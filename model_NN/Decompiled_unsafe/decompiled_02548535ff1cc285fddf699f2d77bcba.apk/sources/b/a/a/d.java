package b.a.a;

import b.a.a.b;
import b.a.i;
import b.v;
import c.e;
import c.f;
import java.io.Closeable;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.Socket;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public final class d implements Closeable {
    static final /* synthetic */ boolean k = (!d.class.desiredAssertionStatus());
    /* access modifiers changed from: private */
    public static final ExecutorService l = new ThreadPoolExecutor(0, Integer.MAX_VALUE, 60, TimeUnit.SECONDS, new SynchronousQueue(), i.a("OkHttp FramedConnection", true));

    /* renamed from: a  reason: collision with root package name */
    final v f262a;

    /* renamed from: b  reason: collision with root package name */
    final boolean f263b;

    /* renamed from: c  reason: collision with root package name */
    long f264c;
    long d;
    n e;
    final n f;
    final p g;
    final Socket h;
    final c i;
    final c j;
    /* access modifiers changed from: private */
    public final b m;
    /* access modifiers changed from: private */
    public final Map<Integer, e> n;
    /* access modifiers changed from: private */
    public final String o;
    /* access modifiers changed from: private */
    public int p;
    /* access modifiers changed from: private */
    public int q;
    /* access modifiers changed from: private */
    public boolean r;
    private long s;
    private final ExecutorService t;
    private Map<Integer, l> u;
    /* access modifiers changed from: private */
    public final m v;
    private int w;
    /* access modifiers changed from: private */
    public boolean x;
    /* access modifiers changed from: private */
    public final Set<Integer> y;

    public static class a {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public Socket f279a;
        /* access modifiers changed from: private */

        /* renamed from: b  reason: collision with root package name */
        public String f280b;
        /* access modifiers changed from: private */

        /* renamed from: c  reason: collision with root package name */
        public e f281c;
        /* access modifiers changed from: private */
        public c.d d;
        /* access modifiers changed from: private */
        public b e = b.f282a;
        /* access modifiers changed from: private */
        public v f = v.SPDY_3;
        /* access modifiers changed from: private */
        public m g = m.f340a;
        /* access modifiers changed from: private */
        public boolean h;

        public a(boolean z) {
            this.h = z;
        }

        public a a(v vVar) {
            this.f = vVar;
            return this;
        }

        public a a(Socket socket, String str, e eVar, c.d dVar) {
            this.f279a = socket;
            this.f280b = str;
            this.f281c = eVar;
            this.d = dVar;
            return this;
        }

        public d a() {
            return new d(this);
        }
    }

    public static abstract class b {

        /* renamed from: a  reason: collision with root package name */
        public static final b f282a = new b() {
            public void a(e eVar) {
                eVar.a(a.REFUSED_STREAM);
            }
        };

        public void a(d dVar) {
        }

        public abstract void a(e eVar);
    }

    class c extends b.a.e implements b.a {

        /* renamed from: a  reason: collision with root package name */
        final b f283a;

        private c(b bVar) {
            super("OkHttp %s", d.this.o);
            this.f283a = bVar;
        }

        private void a(final n nVar) {
            d.l.execute(new b.a.e("OkHttp %s ACK Settings", new Object[]{d.this.o}) {
                public void b() {
                    try {
                        d.this.i.a(nVar);
                    } catch (IOException e) {
                    }
                }
            });
        }

        public void a() {
        }

        public void a(int i, int i2, int i3, boolean z) {
        }

        public void a(int i, int i2, List<f> list) {
            d.this.a(i2, list);
        }

        public void a(int i, long j) {
            if (i == 0) {
                synchronized (d.this) {
                    d.this.d += j;
                    d.this.notifyAll();
                }
                return;
            }
            e a2 = d.this.a(i);
            if (a2 != null) {
                synchronized (a2) {
                    a2.a(j);
                }
            }
        }

        public void a(int i, a aVar) {
            if (d.this.d(i)) {
                d.this.c(i, aVar);
                return;
            }
            e b2 = d.this.b(i);
            if (b2 != null) {
                b2.c(aVar);
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: b.a.a.d.b(b.a.a.d, boolean):boolean
         arg types: [b.a.a.d, int]
         candidates:
          b.a.a.d.b(b.a.a.d, int):int
          b.a.a.d.b(int, b.a.a.a):void
          b.a.a.d.b(b.a.a.d, boolean):boolean */
        public void a(int i, a aVar, f fVar) {
            e[] eVarArr;
            if (fVar.f() > 0) {
            }
            synchronized (d.this) {
                eVarArr = (e[]) d.this.n.values().toArray(new e[d.this.n.size()]);
                boolean unused = d.this.r = true;
            }
            for (e eVar : eVarArr) {
                if (eVar.a() > i && eVar.c()) {
                    eVar.c(a.REFUSED_STREAM);
                    d.this.b(eVar.a());
                }
            }
        }

        public void a(boolean z, int i, int i2) {
            if (z) {
                l c2 = d.this.c(i);
                if (c2 != null) {
                    c2.b();
                    return;
                }
                return;
            }
            d.this.a(true, i, i2, null);
        }

        public void a(boolean z, int i, e eVar, int i2) {
            if (d.this.d(i)) {
                d.this.a(i, eVar, i2, z);
                return;
            }
            e a2 = d.this.a(i);
            if (a2 == null) {
                d.this.a(i, a.INVALID_STREAM);
                eVar.g((long) i2);
                return;
            }
            a2.a(eVar, i2);
            if (z) {
                a2.i();
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: b.a.a.d.a(b.a.a.d, boolean):boolean
         arg types: [b.a.a.d, int]
         candidates:
          b.a.a.d.a(int, java.util.List<b.a.a.f>):void
          b.a.a.d.a(b.a.a.a, b.a.a.a):void
          b.a.a.d.a(b.a.a.d, int):boolean
          b.a.a.d.a(int, long):void
          b.a.a.d.a(int, b.a.a.a):void
          b.a.a.d.a(b.a.a.d, boolean):boolean */
        public void a(boolean z, n nVar) {
            e[] eVarArr;
            long j;
            synchronized (d.this) {
                int f = d.this.f.f(65536);
                if (z) {
                    d.this.f.a();
                }
                d.this.f.a(nVar);
                if (d.this.a() == v.HTTP_2) {
                    a(nVar);
                }
                int f2 = d.this.f.f(65536);
                if (f2 == -1 || f2 == f) {
                    eVarArr = null;
                    j = 0;
                } else {
                    long j2 = (long) (f2 - f);
                    if (!d.this.x) {
                        d.this.a(j2);
                        boolean unused = d.this.x = true;
                    }
                    if (!d.this.n.isEmpty()) {
                        j = j2;
                        eVarArr = (e[]) d.this.n.values().toArray(new e[d.this.n.size()]);
                    } else {
                        j = j2;
                        eVarArr = null;
                    }
                }
                d.l.execute(new b.a.e("OkHttp %s settings", d.this.o) {
                    public void b() {
                        d.this.m.a(d.this);
                    }
                });
            }
            if (eVarArr != null && j != 0) {
                for (e eVar : eVarArr) {
                    synchronized (eVar) {
                        eVar.a(j);
                    }
                }
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:34:0x0092, code lost:
            if (r14.b() == false) goto L_0x00a0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:35:0x0094, code lost:
            r0.b(b.a.a.a.PROTOCOL_ERROR);
            r8.f284c.b(r11);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:36:0x00a0, code lost:
            r0.a(r13, r14);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:37:0x00a3, code lost:
            if (r10 == false) goto L_?;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:38:0x00a5, code lost:
            r0.i();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:45:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:46:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:47:?, code lost:
            return;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void a(boolean r9, boolean r10, int r11, int r12, java.util.List<b.a.a.f> r13, b.a.a.g r14) {
            /*
                r8 = this;
                b.a.a.d r0 = b.a.a.d.this
                boolean r0 = r0.d(r11)
                if (r0 == 0) goto L_0x000e
                b.a.a.d r0 = b.a.a.d.this
                r0.a(r11, r13, r10)
            L_0x000d:
                return
            L_0x000e:
                b.a.a.d r6 = b.a.a.d.this
                monitor-enter(r6)
                b.a.a.d r0 = b.a.a.d.this     // Catch:{ all -> 0x001b }
                boolean r0 = r0.r     // Catch:{ all -> 0x001b }
                if (r0 == 0) goto L_0x001e
                monitor-exit(r6)     // Catch:{ all -> 0x001b }
                goto L_0x000d
            L_0x001b:
                r0 = move-exception
                monitor-exit(r6)     // Catch:{ all -> 0x001b }
                throw r0
            L_0x001e:
                b.a.a.d r0 = b.a.a.d.this     // Catch:{ all -> 0x001b }
                b.a.a.e r0 = r0.a(r11)     // Catch:{ all -> 0x001b }
                if (r0 != 0) goto L_0x008d
                boolean r0 = r14.a()     // Catch:{ all -> 0x001b }
                if (r0 == 0) goto L_0x0035
                b.a.a.d r0 = b.a.a.d.this     // Catch:{ all -> 0x001b }
                b.a.a.a r1 = b.a.a.a.INVALID_STREAM     // Catch:{ all -> 0x001b }
                r0.a(r11, r1)     // Catch:{ all -> 0x001b }
                monitor-exit(r6)     // Catch:{ all -> 0x001b }
                goto L_0x000d
            L_0x0035:
                b.a.a.d r0 = b.a.a.d.this     // Catch:{ all -> 0x001b }
                int r0 = r0.p     // Catch:{ all -> 0x001b }
                if (r11 > r0) goto L_0x003f
                monitor-exit(r6)     // Catch:{ all -> 0x001b }
                goto L_0x000d
            L_0x003f:
                int r0 = r11 % 2
                b.a.a.d r1 = b.a.a.d.this     // Catch:{ all -> 0x001b }
                int r1 = r1.q     // Catch:{ all -> 0x001b }
                int r1 = r1 % 2
                if (r0 != r1) goto L_0x004d
                monitor-exit(r6)     // Catch:{ all -> 0x001b }
                goto L_0x000d
            L_0x004d:
                b.a.a.e r0 = new b.a.a.e     // Catch:{ all -> 0x001b }
                b.a.a.d r2 = b.a.a.d.this     // Catch:{ all -> 0x001b }
                r1 = r11
                r3 = r9
                r4 = r10
                r5 = r13
                r0.<init>(r1, r2, r3, r4, r5)     // Catch:{ all -> 0x001b }
                b.a.a.d r1 = b.a.a.d.this     // Catch:{ all -> 0x001b }
                int unused = r1.p = r11     // Catch:{ all -> 0x001b }
                b.a.a.d r1 = b.a.a.d.this     // Catch:{ all -> 0x001b }
                java.util.Map r1 = r1.n     // Catch:{ all -> 0x001b }
                java.lang.Integer r2 = java.lang.Integer.valueOf(r11)     // Catch:{ all -> 0x001b }
                r1.put(r2, r0)     // Catch:{ all -> 0x001b }
                java.util.concurrent.ExecutorService r1 = b.a.a.d.l     // Catch:{ all -> 0x001b }
                b.a.a.d$c$1 r2 = new b.a.a.d$c$1     // Catch:{ all -> 0x001b }
                java.lang.String r3 = "OkHttp %s stream %d"
                r4 = 2
                java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ all -> 0x001b }
                r5 = 0
                b.a.a.d r7 = b.a.a.d.this     // Catch:{ all -> 0x001b }
                java.lang.String r7 = r7.o     // Catch:{ all -> 0x001b }
                r4[r5] = r7     // Catch:{ all -> 0x001b }
                r5 = 1
                java.lang.Integer r7 = java.lang.Integer.valueOf(r11)     // Catch:{ all -> 0x001b }
                r4[r5] = r7     // Catch:{ all -> 0x001b }
                r2.<init>(r3, r4, r0)     // Catch:{ all -> 0x001b }
                r1.execute(r2)     // Catch:{ all -> 0x001b }
                monitor-exit(r6)     // Catch:{ all -> 0x001b }
                goto L_0x000d
            L_0x008d:
                monitor-exit(r6)     // Catch:{ all -> 0x001b }
                boolean r1 = r14.b()
                if (r1 == 0) goto L_0x00a0
                b.a.a.a r1 = b.a.a.a.PROTOCOL_ERROR
                r0.b(r1)
                b.a.a.d r0 = b.a.a.d.this
                r0.b(r11)
                goto L_0x000d
            L_0x00a0:
                r0.a(r13, r14)
                if (r10 == 0) goto L_0x000d
                r0.i()
                goto L_0x000d
            */
            throw new UnsupportedOperationException("Method not decompiled: b.a.a.d.c.a(boolean, boolean, int, int, java.util.List, b.a.a.g):void");
        }

        /* access modifiers changed from: protected */
        public void b() {
            a aVar;
            Throwable th;
            a aVar2 = a.INTERNAL_ERROR;
            a aVar3 = a.INTERNAL_ERROR;
            try {
                if (!d.this.f263b) {
                    this.f283a.a();
                }
                do {
                } while (this.f283a.a(this));
                try {
                    d.this.a(a.NO_ERROR, a.CANCEL);
                } catch (IOException e) {
                }
                i.a(this.f283a);
            } catch (IOException e2) {
                aVar = a.PROTOCOL_ERROR;
                try {
                    d.this.a(aVar, a.PROTOCOL_ERROR);
                } catch (IOException e3) {
                }
                i.a(this.f283a);
            } catch (Throwable th2) {
                th = th2;
                d.this.a(aVar, aVar3);
                i.a(this.f283a);
                throw th;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: b.a.i.a(java.lang.String, boolean):java.util.concurrent.ThreadFactory
     arg types: [java.lang.String, int]
     candidates:
      b.a.i.a(java.lang.Object[], java.lang.Object[]):java.util.List<T>
      b.a.i.a(java.io.Closeable, java.io.Closeable):void
      b.a.i.a(java.lang.Object, java.lang.Object):boolean
      b.a.i.a(java.lang.String[], java.lang.String):boolean
      b.a.i.a(java.lang.String, boolean):java.util.concurrent.ThreadFactory */
    private d(a aVar) {
        int i2 = 2;
        this.n = new HashMap();
        this.s = System.nanoTime();
        this.f264c = 0;
        this.e = new n();
        this.f = new n();
        this.x = false;
        this.y = new LinkedHashSet();
        this.f262a = aVar.f;
        this.v = aVar.g;
        this.f263b = aVar.h;
        this.m = aVar.e;
        this.q = aVar.h ? 1 : 2;
        if (aVar.h && this.f262a == v.HTTP_2) {
            this.q += 2;
        }
        this.w = aVar.h ? 1 : i2;
        if (aVar.h) {
            this.e.a(7, 0, 16777216);
        }
        this.o = aVar.f280b;
        if (this.f262a == v.HTTP_2) {
            this.g = new i();
            this.t = new ThreadPoolExecutor(0, 1, 60, TimeUnit.SECONDS, new LinkedBlockingQueue(), i.a(String.format("OkHttp %s Push Observer", this.o), true));
            this.f.a(7, 0, 65535);
            this.f.a(5, 0, 16384);
        } else if (this.f262a == v.SPDY_3) {
            this.g = new o();
            this.t = null;
        } else {
            throw new AssertionError(this.f262a);
        }
        this.d = (long) this.f.f(65536);
        this.h = aVar.f279a;
        this.i = this.g.a(aVar.d, this.f263b);
        this.j = new c(this.g.a(aVar.f281c, this.f263b));
        new Thread(this.j).start();
    }

    private e a(int i2, List<f> list, boolean z, boolean z2) {
        int i3;
        e eVar;
        boolean z3 = true;
        boolean z4 = !z;
        if (z2) {
            z3 = false;
        }
        synchronized (this.i) {
            synchronized (this) {
                if (this.r) {
                    throw new IOException("shutdown");
                }
                i3 = this.q;
                this.q += 2;
                eVar = new e(i3, this, z4, z3, list);
                if (eVar.b()) {
                    this.n.put(Integer.valueOf(i3), eVar);
                    a(false);
                }
            }
            if (i2 == 0) {
                this.i.a(z4, z3, i3, i2, list);
            } else if (this.f263b) {
                throw new IllegalArgumentException("client streams shouldn't have associated stream IDs");
            } else {
                this.i.a(i2, i3, list);
            }
        }
        if (!z) {
            this.i.b();
        }
        return eVar;
    }

    /* access modifiers changed from: private */
    public void a(int i2, e eVar, int i3, boolean z) {
        final c.c cVar = new c.c();
        eVar.a((long) i3);
        eVar.a(cVar, (long) i3);
        if (cVar.b() != ((long) i3)) {
            throw new IOException(cVar.b() + " != " + i3);
        }
        final int i4 = i2;
        final int i5 = i3;
        final boolean z2 = z;
        this.t.execute(new b.a.e("OkHttp %s Push Data[%s]", new Object[]{this.o, Integer.valueOf(i2)}) {
            public void b() {
                try {
                    boolean a2 = d.this.v.a(i4, cVar, i5, z2);
                    if (a2) {
                        d.this.i.a(i4, a.CANCEL);
                    }
                    if (a2 || z2) {
                        synchronized (d.this) {
                            d.this.y.remove(Integer.valueOf(i4));
                        }
                    }
                } catch (IOException e2) {
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(int i2, List<f> list) {
        synchronized (this) {
            if (this.y.contains(Integer.valueOf(i2))) {
                a(i2, a.PROTOCOL_ERROR);
                return;
            }
            this.y.add(Integer.valueOf(i2));
            final int i3 = i2;
            final List<f> list2 = list;
            this.t.execute(new b.a.e("OkHttp %s Push Request[%s]", new Object[]{this.o, Integer.valueOf(i2)}) {
                public void b() {
                    if (d.this.v.a(i3, list2)) {
                        try {
                            d.this.i.a(i3, a.CANCEL);
                            synchronized (d.this) {
                                d.this.y.remove(Integer.valueOf(i3));
                            }
                        } catch (IOException e) {
                        }
                    }
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void a(int i2, List<f> list, boolean z) {
        final int i3 = i2;
        final List<f> list2 = list;
        final boolean z2 = z;
        this.t.execute(new b.a.e("OkHttp %s Push Headers[%s]", new Object[]{this.o, Integer.valueOf(i2)}) {
            public void b() {
                boolean a2 = d.this.v.a(i3, list2, z2);
                if (a2) {
                    try {
                        d.this.i.a(i3, a.CANCEL);
                    } catch (IOException e2) {
                        return;
                    }
                }
                if (a2 || z2) {
                    synchronized (d.this) {
                        d.this.y.remove(Integer.valueOf(i3));
                    }
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(a aVar, a aVar2) {
        IOException iOException;
        e[] eVarArr;
        l[] lVarArr;
        if (k || !Thread.holdsLock(this)) {
            try {
                a(aVar);
                iOException = null;
            } catch (IOException e2) {
                iOException = e2;
            }
            synchronized (this) {
                if (!this.n.isEmpty()) {
                    this.n.clear();
                    a(false);
                    eVarArr = (e[]) this.n.values().toArray(new e[this.n.size()]);
                } else {
                    eVarArr = null;
                }
                if (this.u != null) {
                    this.u = null;
                    lVarArr = (l[]) this.u.values().toArray(new l[this.u.size()]);
                } else {
                    lVarArr = null;
                }
            }
            if (eVarArr != null) {
                IOException iOException2 = iOException;
                for (e a2 : eVarArr) {
                    try {
                        a2.a(aVar2);
                    } catch (IOException e3) {
                        if (iOException2 != null) {
                            iOException2 = e3;
                        }
                    }
                }
                iOException = iOException2;
            }
            if (lVarArr != null) {
                for (l c2 : lVarArr) {
                    c2.c();
                }
            }
            try {
                this.i.close();
                e = iOException;
            } catch (IOException e4) {
                e = e4;
                if (iOException != null) {
                    e = iOException;
                }
            }
            try {
                this.h.close();
            } catch (IOException e5) {
                e = e5;
            }
            if (e != null) {
                throw e;
            }
            return;
        }
        throw new AssertionError();
    }

    private synchronized void a(boolean z) {
        this.s = z ? System.nanoTime() : Long.MAX_VALUE;
    }

    /* access modifiers changed from: private */
    public void a(boolean z, int i2, int i3, l lVar) {
        final boolean z2 = z;
        final int i4 = i2;
        final int i5 = i3;
        final l lVar2 = lVar;
        l.execute(new b.a.e("OkHttp %s ping %08x%08x", new Object[]{this.o, Integer.valueOf(i2), Integer.valueOf(i3)}) {
            public void b() {
                try {
                    d.this.b(z2, i4, i5, lVar2);
                } catch (IOException e2) {
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void b(boolean z, int i2, int i3, l lVar) {
        synchronized (this.i) {
            if (lVar != null) {
                lVar.a();
            }
            this.i.a(z, i2, i3);
        }
    }

    /* access modifiers changed from: private */
    public synchronized l c(int i2) {
        return this.u != null ? this.u.remove(Integer.valueOf(i2)) : null;
    }

    /* access modifiers changed from: private */
    public void c(int i2, a aVar) {
        final int i3 = i2;
        final a aVar2 = aVar;
        this.t.execute(new b.a.e("OkHttp %s Push Reset[%s]", new Object[]{this.o, Integer.valueOf(i2)}) {
            public void b() {
                d.this.v.a(i3, aVar2);
                synchronized (d.this) {
                    d.this.y.remove(Integer.valueOf(i3));
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public boolean d(int i2) {
        return this.f262a == v.HTTP_2 && i2 != 0 && (i2 & 1) == 0;
    }

    /* access modifiers changed from: package-private */
    public synchronized e a(int i2) {
        return this.n.get(Integer.valueOf(i2));
    }

    public e a(List<f> list, boolean z, boolean z2) {
        return a(0, list, z, z2);
    }

    public v a() {
        return this.f262a;
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, long j2) {
        final int i3 = i2;
        final long j3 = j2;
        l.execute(new b.a.e("OkHttp Window Update %s stream %d", new Object[]{this.o, Integer.valueOf(i2)}) {
            public void b() {
                try {
                    d.this.i.a(i3, j3);
                } catch (IOException e) {
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, a aVar) {
        final int i3 = i2;
        final a aVar2 = aVar;
        l.submit(new b.a.e("OkHttp %s stream %d", new Object[]{this.o, Integer.valueOf(i2)}) {
            public void b() {
                try {
                    d.this.b(i3, aVar2);
                } catch (IOException e) {
                }
            }
        });
    }

    public void a(int i2, boolean z, c.c cVar, long j2) {
        int min;
        if (j2 == 0) {
            this.i.a(z, i2, cVar, 0);
            return;
        }
        while (j2 > 0) {
            synchronized (this) {
                while (this.d <= 0) {
                    try {
                        if (!this.n.containsKey(Integer.valueOf(i2))) {
                            throw new IOException("stream closed");
                        }
                        wait();
                    } catch (InterruptedException e2) {
                        throw new InterruptedIOException();
                    }
                }
                min = Math.min((int) Math.min(j2, this.d), this.i.c());
                this.d -= (long) min;
            }
            j2 -= (long) min;
            this.i.a(z && j2 == 0, i2, cVar, min);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(long j2) {
        this.d += j2;
        if (j2 > 0) {
            notifyAll();
        }
    }

    public void a(a aVar) {
        synchronized (this.i) {
            synchronized (this) {
                if (!this.r) {
                    this.r = true;
                    int i2 = this.p;
                    this.i.a(i2, aVar, i.f458a);
                }
            }
        }
    }

    public synchronized int b() {
        return this.f.d(Integer.MAX_VALUE);
    }

    /* access modifiers changed from: package-private */
    public synchronized e b(int i2) {
        e remove;
        remove = this.n.remove(Integer.valueOf(i2));
        if (remove != null && this.n.isEmpty()) {
            a(true);
        }
        notifyAll();
        return remove;
    }

    /* access modifiers changed from: package-private */
    public void b(int i2, a aVar) {
        this.i.a(i2, aVar);
    }

    public void c() {
        this.i.b();
    }

    public void close() {
        a(a.NO_ERROR, a.CANCEL);
    }

    public void d() {
        this.i.a();
        this.i.b(this.e);
        int f2 = this.e.f(65536);
        if (f2 != 65536) {
            this.i.a(0, (long) (f2 - 65536));
        }
    }
}
