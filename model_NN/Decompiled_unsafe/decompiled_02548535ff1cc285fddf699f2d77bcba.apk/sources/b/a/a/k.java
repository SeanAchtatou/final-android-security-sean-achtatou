package b.a.a;

import c.c;
import c.e;
import c.f;
import c.h;
import c.l;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;

class k {

    /* renamed from: a  reason: collision with root package name */
    private final c.k f332a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f333b;

    /* renamed from: c  reason: collision with root package name */
    private final e f334c = l.a(this.f332a);

    public k(e eVar) {
        this.f332a = new c.k(new h(eVar) {
            public long a(c cVar, long j) {
                if (k.this.f333b == 0) {
                    return -1;
                }
                long a2 = super.a(cVar, Math.min(j, (long) k.this.f333b));
                if (a2 == -1) {
                    return -1;
                }
                int unused = k.this.f333b = (int) (((long) k.this.f333b) - a2);
                return a2;
            }
        }, new Inflater() {
            public int inflate(byte[] bArr, int i, int i2) {
                int inflate = super.inflate(bArr, i, i2);
                if (inflate != 0 || !needsDictionary()) {
                    return inflate;
                }
                setDictionary(o.f344a);
                return super.inflate(bArr, i, i2);
            }
        });
    }

    private f b() {
        return this.f334c.c((long) this.f334c.j());
    }

    private void c() {
        if (this.f333b > 0) {
            this.f332a.b();
            if (this.f333b != 0) {
                throw new IOException("compressedLimit > 0: " + this.f333b);
            }
        }
    }

    public List<f> a(int i) {
        this.f333b += i;
        int j = this.f334c.j();
        if (j < 0) {
            throw new IOException("numberOfPairs < 0: " + j);
        } else if (j > 1024) {
            throw new IOException("numberOfPairs > 1024: " + j);
        } else {
            ArrayList arrayList = new ArrayList(j);
            for (int i2 = 0; i2 < j; i2++) {
                f e = b().e();
                f b2 = b();
                if (e.f() == 0) {
                    throw new IOException("name.size == 0");
                }
                arrayList.add(new f(e, b2));
            }
            c();
            return arrayList;
        }
    }

    public void a() {
        this.f334c.close();
    }
}
