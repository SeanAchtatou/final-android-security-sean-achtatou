package b.a.a;

import c.c;
import c.e;
import c.f;
import c.l;
import c.r;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

final class h {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final f[] f306a = {new f(f.e, ""), new f(f.f301b, "GET"), new f(f.f301b, "POST"), new f(f.f302c, "/"), new f(f.f302c, "/index.html"), new f(f.d, "http"), new f(f.d, "https"), new f(f.f300a, "200"), new f(f.f300a, "204"), new f(f.f300a, "206"), new f(f.f300a, "304"), new f(f.f300a, "400"), new f(f.f300a, "404"), new f(f.f300a, "500"), new f("accept-charset", ""), new f("accept-encoding", "gzip, deflate"), new f("accept-language", ""), new f("accept-ranges", ""), new f("accept", ""), new f("access-control-allow-origin", ""), new f("age", ""), new f("allow", ""), new f("authorization", ""), new f("cache-control", ""), new f("content-disposition", ""), new f("content-encoding", ""), new f("content-language", ""), new f("content-length", ""), new f("content-location", ""), new f("content-range", ""), new f("content-type", ""), new f("cookie", ""), new f("date", ""), new f("etag", ""), new f("expect", ""), new f("expires", ""), new f("from", ""), new f("host", ""), new f("if-match", ""), new f("if-modified-since", ""), new f("if-none-match", ""), new f("if-range", ""), new f("if-unmodified-since", ""), new f("last-modified", ""), new f("link", ""), new f("location", ""), new f("max-forwards", ""), new f("proxy-authenticate", ""), new f("proxy-authorization", ""), new f("range", ""), new f("referer", ""), new f("refresh", ""), new f("retry-after", ""), new f("server", ""), new f("set-cookie", ""), new f("strict-transport-security", ""), new f("transfer-encoding", ""), new f("user-agent", ""), new f("vary", ""), new f("via", ""), new f("www-authenticate", "")};
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public static final Map<f, Integer> f307b = c();

    static final class a {

        /* renamed from: a  reason: collision with root package name */
        f[] f308a = new f[8];

        /* renamed from: b  reason: collision with root package name */
        int f309b = (this.f308a.length - 1);

        /* renamed from: c  reason: collision with root package name */
        int f310c = 0;
        int d = 0;
        private final List<f> e = new ArrayList();
        private final e f;
        private int g;
        private int h;

        a(int i, r rVar) {
            this.g = i;
            this.h = i;
            this.f = l.a(rVar);
        }

        private void a(int i, f fVar) {
            this.e.add(fVar);
            int i2 = fVar.j;
            if (i != -1) {
                i2 -= this.f308a[d(i)].j;
            }
            if (i2 > this.h) {
                e();
                return;
            }
            int b2 = b((this.d + i2) - this.h);
            if (i == -1) {
                if (this.f310c + 1 > this.f308a.length) {
                    f[] fVarArr = new f[(this.f308a.length * 2)];
                    System.arraycopy(this.f308a, 0, fVarArr, this.f308a.length, this.f308a.length);
                    this.f309b = this.f308a.length - 1;
                    this.f308a = fVarArr;
                }
                int i3 = this.f309b;
                this.f309b = i3 - 1;
                this.f308a[i3] = fVar;
                this.f310c++;
            } else {
                this.f308a[b2 + d(i) + i] = fVar;
            }
            this.d = i2 + this.d;
        }

        private int b(int i) {
            int i2 = 0;
            if (i > 0) {
                int length = this.f308a.length;
                while (true) {
                    length--;
                    if (length < this.f309b || i <= 0) {
                        System.arraycopy(this.f308a, this.f309b + 1, this.f308a, this.f309b + 1 + i2, this.f310c);
                        this.f309b += i2;
                    } else {
                        i -= this.f308a[length].j;
                        this.d -= this.f308a[length].j;
                        this.f310c--;
                        i2++;
                    }
                }
                System.arraycopy(this.f308a, this.f309b + 1, this.f308a, this.f309b + 1 + i2, this.f310c);
                this.f309b += i2;
            }
            return i2;
        }

        private void c(int i) {
            if (h(i)) {
                this.e.add(h.f306a[i]);
                return;
            }
            int d2 = d(i - h.f306a.length);
            if (d2 < 0 || d2 > this.f308a.length - 1) {
                throw new IOException("Header index too large " + (i + 1));
            }
            this.e.add(this.f308a[d2]);
        }

        private int d(int i) {
            return this.f309b + 1 + i;
        }

        private void d() {
            if (this.h >= this.d) {
                return;
            }
            if (this.h == 0) {
                e();
            } else {
                b(this.d - this.h);
            }
        }

        private void e() {
            this.e.clear();
            Arrays.fill(this.f308a, (Object) null);
            this.f309b = this.f308a.length - 1;
            this.f310c = 0;
            this.d = 0;
        }

        private void e(int i) {
            this.e.add(new f(g(i), c()));
        }

        private void f() {
            this.e.add(new f(h.b(c()), c()));
        }

        private void f(int i) {
            a(-1, new f(g(i), c()));
        }

        private f g(int i) {
            return h(i) ? h.f306a[i].h : this.f308a[d(i - h.f306a.length)].h;
        }

        private void g() {
            a(-1, new f(h.b(c()), c()));
        }

        private int h() {
            return this.f.h() & 255;
        }

        private boolean h(int i) {
            return i >= 0 && i <= h.f306a.length + -1;
        }

        /* access modifiers changed from: package-private */
        public int a(int i, int i2) {
            int i3 = i & i2;
            if (i3 < i2) {
                return i3;
            }
            int i4 = 0;
            while (true) {
                int h2 = h();
                if ((h2 & 128) == 0) {
                    return (h2 << i4) + i2;
                }
                i2 += (h2 & 127) << i4;
                i4 += 7;
            }
        }

        /* access modifiers changed from: package-private */
        public void a() {
            while (!this.f.f()) {
                byte h2 = this.f.h() & 255;
                if (h2 == 128) {
                    throw new IOException("index == 0");
                } else if ((h2 & 128) == 128) {
                    c(a(h2, 127) - 1);
                } else if (h2 == 64) {
                    g();
                } else if ((h2 & 64) == 64) {
                    f(a(h2, 63) - 1);
                } else if ((h2 & 32) == 32) {
                    this.h = a(h2, 31);
                    if (this.h < 0 || this.h > this.g) {
                        throw new IOException("Invalid dynamic table size update " + this.h);
                    }
                    d();
                } else if (h2 == 16 || h2 == 0) {
                    f();
                } else {
                    e(a(h2, 15) - 1);
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void a(int i) {
            this.g = i;
            this.h = i;
            d();
        }

        public List<f> b() {
            ArrayList arrayList = new ArrayList(this.e);
            this.e.clear();
            return arrayList;
        }

        /* access modifiers changed from: package-private */
        public f c() {
            int h2 = h();
            boolean z = (h2 & 128) == 128;
            int a2 = a(h2, 127);
            return z ? f.a(j.a().a(this.f.f((long) a2))) : this.f.c((long) a2);
        }
    }

    static final class b {

        /* renamed from: a  reason: collision with root package name */
        private final c f311a;

        b(c cVar) {
            this.f311a = cVar;
        }

        /* access modifiers changed from: package-private */
        public void a(int i, int i2, int i3) {
            if (i < i2) {
                this.f311a.h(i3 | i);
                return;
            }
            this.f311a.h(i3 | i2);
            int i4 = i - i2;
            while (i4 >= 128) {
                this.f311a.h((i4 & 127) | 128);
                i4 >>>= 7;
            }
            this.f311a.h(i4);
        }

        /* access modifiers changed from: package-private */
        public void a(f fVar) {
            a(fVar.f(), 127, 0);
            this.f311a.b(fVar);
        }

        /* access modifiers changed from: package-private */
        public void a(List<f> list) {
            int size = list.size();
            for (int i = 0; i < size; i++) {
                f e = list.get(i).h.e();
                Integer num = (Integer) h.f307b.get(e);
                if (num != null) {
                    a(num.intValue() + 1, 15, 0);
                    a(list.get(i).i);
                } else {
                    this.f311a.h(0);
                    a(e);
                    a(list.get(i).i);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public static f b(f fVar) {
        int i = 0;
        int f = fVar.f();
        while (i < f) {
            byte a2 = fVar.a(i);
            if (a2 < 65 || a2 > 90) {
                i++;
            } else {
                throw new IOException("PROTOCOL_ERROR response malformed: mixed case name: " + fVar.a());
            }
        }
        return fVar;
    }

    private static Map<f, Integer> c() {
        LinkedHashMap linkedHashMap = new LinkedHashMap(f306a.length);
        for (int i = 0; i < f306a.length; i++) {
            if (!linkedHashMap.containsKey(f306a[i].h)) {
                linkedHashMap.put(f306a[i].h, Integer.valueOf(i));
            }
        }
        return Collections.unmodifiableMap(linkedHashMap);
    }
}
