package b.a.a;

import b.a.a.b;
import b.a.i;
import c.c;
import c.d;
import c.e;
import c.f;
import c.g;
import c.l;
import c.q;
import java.io.Closeable;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.ProtocolException;
import java.util.List;
import java.util.zip.Deflater;

public final class o implements p {

    /* renamed from: a  reason: collision with root package name */
    static final byte[] f344a;

    static final class a implements b {

        /* renamed from: a  reason: collision with root package name */
        private final e f345a;

        /* renamed from: b  reason: collision with root package name */
        private final boolean f346b;

        /* renamed from: c  reason: collision with root package name */
        private final k f347c = new k(this.f345a);

        a(e eVar, boolean z) {
            this.f345a = eVar;
            this.f346b = z;
        }

        private static IOException a(String str, Object... objArr) {
            throw new IOException(String.format(str, objArr));
        }

        private void a(b.a aVar, int i, int i2) {
            boolean z = true;
            int j = this.f345a.j() & Integer.MAX_VALUE;
            int j2 = this.f345a.j() & Integer.MAX_VALUE;
            this.f345a.i();
            List<f> a2 = this.f347c.a(i2 - 10);
            boolean z2 = (i & 1) != 0;
            if ((i & 2) == 0) {
                z = false;
            }
            aVar.a(z, z2, j, j2, a2, g.SPDY_SYN_STREAM);
        }

        private void b(b.a aVar, int i, int i2) {
            aVar.a(false, (i & 1) != 0, this.f345a.j() & Integer.MAX_VALUE, -1, this.f347c.a(i2 - 4), g.SPDY_REPLY);
        }

        private void c(b.a aVar, int i, int i2) {
            if (i2 != 8) {
                throw a("TYPE_RST_STREAM length: %d != 8", Integer.valueOf(i2));
            }
            int j = this.f345a.j() & Integer.MAX_VALUE;
            int j2 = this.f345a.j();
            a a2 = a.a(j2);
            if (a2 == null) {
                throw a("TYPE_RST_STREAM unexpected error code: %d", Integer.valueOf(j2));
            } else {
                aVar.a(j, a2);
            }
        }

        private void d(b.a aVar, int i, int i2) {
            aVar.a(false, false, this.f345a.j() & Integer.MAX_VALUE, -1, this.f347c.a(i2 - 4), g.SPDY_HEADERS);
        }

        private void e(b.a aVar, int i, int i2) {
            if (i2 != 8) {
                throw a("TYPE_WINDOW_UPDATE length: %d != 8", Integer.valueOf(i2));
            }
            int j = this.f345a.j() & Integer.MAX_VALUE;
            long j2 = (long) (this.f345a.j() & Integer.MAX_VALUE);
            if (j2 == 0) {
                throw a("windowSizeIncrement was 0", Long.valueOf(j2));
            } else {
                aVar.a(j, j2);
            }
        }

        private void f(b.a aVar, int i, int i2) {
            boolean z = true;
            if (i2 != 4) {
                throw a("TYPE_PING length: %d != 4", Integer.valueOf(i2));
            }
            int j = this.f345a.j();
            if (this.f346b != ((j & 1) == 1)) {
                z = false;
            }
            aVar.a(z, j, 0);
        }

        private void g(b.a aVar, int i, int i2) {
            if (i2 != 8) {
                throw a("TYPE_GOAWAY length: %d != 8", Integer.valueOf(i2));
            }
            int j = this.f345a.j() & Integer.MAX_VALUE;
            int j2 = this.f345a.j();
            a c2 = a.c(j2);
            if (c2 == null) {
                throw a("TYPE_GOAWAY unexpected error code: %d", Integer.valueOf(j2));
            } else {
                aVar.a(j, c2, f.f572b);
            }
        }

        private void h(b.a aVar, int i, int i2) {
            boolean z = true;
            int j = this.f345a.j();
            if (i2 != (j * 8) + 4) {
                throw a("TYPE_SETTINGS length: %d != 4 + 8 * %d", Integer.valueOf(i2), Integer.valueOf(j));
            }
            n nVar = new n();
            for (int i3 = 0; i3 < j; i3++) {
                int j2 = this.f345a.j();
                nVar.a(j2 & 16777215, (-16777216 & j2) >>> 24, this.f345a.j());
            }
            if ((i & 1) == 0) {
                z = false;
            }
            aVar.a(z, nVar);
        }

        public void a() {
        }

        public boolean a(b.a aVar) {
            boolean z = false;
            try {
                int j = this.f345a.j();
                int j2 = this.f345a.j();
                int i = (-16777216 & j2) >>> 24;
                int i2 = j2 & 16777215;
                if ((Integer.MIN_VALUE & j) != 0) {
                    int i3 = (2147418112 & j) >>> 16;
                    int i4 = 65535 & j;
                    if (i3 != 3) {
                        throw new ProtocolException("version != 3: " + i3);
                    }
                    switch (i4) {
                        case 1:
                            a(aVar, i, i2);
                            return true;
                        case 2:
                            b(aVar, i, i2);
                            return true;
                        case 3:
                            c(aVar, i, i2);
                            return true;
                        case 4:
                            h(aVar, i, i2);
                            return true;
                        case 5:
                        default:
                            this.f345a.g((long) i2);
                            return true;
                        case 6:
                            f(aVar, i, i2);
                            return true;
                        case 7:
                            g(aVar, i, i2);
                            return true;
                        case 8:
                            d(aVar, i, i2);
                            return true;
                        case 9:
                            e(aVar, i, i2);
                            return true;
                    }
                } else {
                    int i5 = Integer.MAX_VALUE & j;
                    if ((i & 1) != 0) {
                        z = true;
                    }
                    aVar.a(z, i5, this.f345a, i2);
                    return true;
                }
            } catch (IOException e) {
                return false;
            }
        }

        public void close() {
            this.f347c.a();
        }
    }

    static final class b implements c {

        /* renamed from: a  reason: collision with root package name */
        private final d f348a;

        /* renamed from: b  reason: collision with root package name */
        private final c f349b = new c();

        /* renamed from: c  reason: collision with root package name */
        private final d f350c;
        private final boolean d;
        private boolean e;

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: c.g.<init>(c.q, java.util.zip.Deflater):void
         arg types: [c.c, java.util.zip.Deflater]
         candidates:
          c.g.<init>(c.d, java.util.zip.Deflater):void
          c.g.<init>(c.q, java.util.zip.Deflater):void */
        b(d dVar, boolean z) {
            this.f348a = dVar;
            this.d = z;
            Deflater deflater = new Deflater();
            deflater.setDictionary(o.f344a);
            this.f350c = l.a(new g((q) this.f349b, deflater));
        }

        private void a(List<f> list) {
            this.f350c.f(list.size());
            int size = list.size();
            for (int i = 0; i < size; i++) {
                f fVar = list.get(i).h;
                this.f350c.f(fVar.f());
                this.f350c.b(fVar);
                f fVar2 = list.get(i).i;
                this.f350c.f(fVar2.f());
                this.f350c.b(fVar2);
            }
            this.f350c.flush();
        }

        public synchronized void a() {
        }

        /* access modifiers changed from: package-private */
        public void a(int i, int i2, c cVar, int i3) {
            if (this.e) {
                throw new IOException("closed");
            } else if (((long) i3) > 16777215) {
                throw new IllegalArgumentException("FRAME_TOO_LARGE max size is 16Mib: " + i3);
            } else {
                this.f348a.f(Integer.MAX_VALUE & i);
                this.f348a.f(((i2 & 255) << 24) | (16777215 & i3));
                if (i3 > 0) {
                    this.f348a.a_(cVar, (long) i3);
                }
            }
        }

        public void a(int i, int i2, List<f> list) {
        }

        public synchronized void a(int i, long j) {
            if (this.e) {
                throw new IOException("closed");
            } else if (j == 0 || j > 2147483647L) {
                throw new IllegalArgumentException("windowSizeIncrement must be between 1 and 0x7fffffff: " + j);
            } else {
                this.f348a.f(-2147287031);
                this.f348a.f(8);
                this.f348a.f(i);
                this.f348a.f((int) j);
                this.f348a.flush();
            }
        }

        public synchronized void a(int i, a aVar) {
            if (this.e) {
                throw new IOException("closed");
            } else if (aVar.t == -1) {
                throw new IllegalArgumentException();
            } else {
                this.f348a.f(-2147287037);
                this.f348a.f(8);
                this.f348a.f(Integer.MAX_VALUE & i);
                this.f348a.f(aVar.t);
                this.f348a.flush();
            }
        }

        public synchronized void a(int i, a aVar, byte[] bArr) {
            if (this.e) {
                throw new IOException("closed");
            } else if (aVar.u == -1) {
                throw new IllegalArgumentException("errorCode.spdyGoAwayCode == -1");
            } else {
                this.f348a.f(-2147287033);
                this.f348a.f(8);
                this.f348a.f(i);
                this.f348a.f(aVar.u);
                this.f348a.flush();
            }
        }

        public void a(n nVar) {
        }

        public synchronized void a(boolean z, int i, int i2) {
            boolean z2 = true;
            synchronized (this) {
                if (this.e) {
                    throw new IOException("closed");
                }
                if (this.d == ((i & 1) == 1)) {
                    z2 = false;
                }
                if (z != z2) {
                    throw new IllegalArgumentException("payload != reply");
                }
                this.f348a.f(-2147287034);
                this.f348a.f(4);
                this.f348a.f(i);
                this.f348a.flush();
            }
        }

        public synchronized void a(boolean z, int i, c cVar, int i2) {
            a(i, z ? 1 : 0, cVar, i2);
        }

        public synchronized void a(boolean z, boolean z2, int i, int i2, List<f> list) {
            int i3 = 0;
            synchronized (this) {
                if (this.e) {
                    throw new IOException("closed");
                }
                a(list);
                int b2 = (int) (10 + this.f349b.b());
                int i4 = z ? 1 : 0;
                if (z2) {
                    i3 = 2;
                }
                this.f348a.f(-2147287039);
                this.f348a.f((((i3 | i4) & 255) << 24) | (b2 & 16777215));
                this.f348a.f(i & Integer.MAX_VALUE);
                this.f348a.f(i2 & Integer.MAX_VALUE);
                this.f348a.g(0);
                this.f348a.a(this.f349b);
                this.f348a.flush();
            }
        }

        public synchronized void b() {
            if (this.e) {
                throw new IOException("closed");
            }
            this.f348a.flush();
        }

        public synchronized void b(n nVar) {
            if (this.e) {
                throw new IOException("closed");
            }
            int b2 = nVar.b();
            this.f348a.f(-2147287036);
            this.f348a.f((((b2 * 8) + 4) & 16777215) | 0);
            this.f348a.f(b2);
            for (int i = 0; i <= 10; i++) {
                if (nVar.a(i)) {
                    this.f348a.f(((nVar.c(i) & 255) << 24) | (i & 16777215));
                    this.f348a.f(nVar.b(i));
                }
            }
            this.f348a.flush();
        }

        public int c() {
            return 16383;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: b.a.i.a(java.io.Closeable, java.io.Closeable):void
         arg types: [c.d, c.d]
         candidates:
          b.a.i.a(java.lang.Object[], java.lang.Object[]):java.util.List<T>
          b.a.i.a(java.lang.String, boolean):java.util.concurrent.ThreadFactory
          b.a.i.a(java.lang.Object, java.lang.Object):boolean
          b.a.i.a(java.lang.String[], java.lang.String):boolean
          b.a.i.a(java.io.Closeable, java.io.Closeable):void */
        public synchronized void close() {
            this.e = true;
            i.a((Closeable) this.f348a, (Closeable) this.f350c);
        }
    }

    static {
        try {
            f344a = "\u0000\u0000\u0000\u0007options\u0000\u0000\u0000\u0004head\u0000\u0000\u0000\u0004post\u0000\u0000\u0000\u0003put\u0000\u0000\u0000\u0006delete\u0000\u0000\u0000\u0005trace\u0000\u0000\u0000\u0006accept\u0000\u0000\u0000\u000eaccept-charset\u0000\u0000\u0000\u000faccept-encoding\u0000\u0000\u0000\u000faccept-language\u0000\u0000\u0000\raccept-ranges\u0000\u0000\u0000\u0003age\u0000\u0000\u0000\u0005allow\u0000\u0000\u0000\rauthorization\u0000\u0000\u0000\rcache-control\u0000\u0000\u0000\nconnection\u0000\u0000\u0000\fcontent-base\u0000\u0000\u0000\u0010content-encoding\u0000\u0000\u0000\u0010content-language\u0000\u0000\u0000\u000econtent-length\u0000\u0000\u0000\u0010content-location\u0000\u0000\u0000\u000bcontent-md5\u0000\u0000\u0000\rcontent-range\u0000\u0000\u0000\fcontent-type\u0000\u0000\u0000\u0004date\u0000\u0000\u0000\u0004etag\u0000\u0000\u0000\u0006expect\u0000\u0000\u0000\u0007expires\u0000\u0000\u0000\u0004from\u0000\u0000\u0000\u0004host\u0000\u0000\u0000\bif-match\u0000\u0000\u0000\u0011if-modified-since\u0000\u0000\u0000\rif-none-match\u0000\u0000\u0000\bif-range\u0000\u0000\u0000\u0013if-unmodified-since\u0000\u0000\u0000\rlast-modified\u0000\u0000\u0000\blocation\u0000\u0000\u0000\fmax-forwards\u0000\u0000\u0000\u0006pragma\u0000\u0000\u0000\u0012proxy-authenticate\u0000\u0000\u0000\u0013proxy-authorization\u0000\u0000\u0000\u0005range\u0000\u0000\u0000\u0007referer\u0000\u0000\u0000\u000bretry-after\u0000\u0000\u0000\u0006server\u0000\u0000\u0000\u0002te\u0000\u0000\u0000\u0007trailer\u0000\u0000\u0000\u0011transfer-encoding\u0000\u0000\u0000\u0007upgrade\u0000\u0000\u0000\nuser-agent\u0000\u0000\u0000\u0004vary\u0000\u0000\u0000\u0003via\u0000\u0000\u0000\u0007warning\u0000\u0000\u0000\u0010www-authenticate\u0000\u0000\u0000\u0006method\u0000\u0000\u0000\u0003get\u0000\u0000\u0000\u0006status\u0000\u0000\u0000\u0006200 OK\u0000\u0000\u0000\u0007version\u0000\u0000\u0000\bHTTP/1.1\u0000\u0000\u0000\u0003url\u0000\u0000\u0000\u0006public\u0000\u0000\u0000\nset-cookie\u0000\u0000\u0000\nkeep-alive\u0000\u0000\u0000\u0006origin100101201202205206300302303304305306307402405406407408409410411412413414415416417502504505203 Non-Authoritative Information204 No Content301 Moved Permanently400 Bad Request401 Unauthorized403 Forbidden404 Not Found500 Internal Server Error501 Not Implemented503 Service UnavailableJan Feb Mar Apr May Jun Jul Aug Sept Oct Nov Dec 00:00:00 Mon, Tue, Wed, Thu, Fri, Sat, Sun, GMTchunked,text/html,image/png,image/jpg,image/gif,application/xml,application/xhtml+xml,text/plain,text/javascript,publicprivatemax-age=gzip,deflate,sdchcharset=utf-8charset=iso-8859-1,utf-,*,enq=0.".getBytes(i.f460c.name());
        } catch (UnsupportedEncodingException e) {
            throw new AssertionError();
        }
    }

    public b a(e eVar, boolean z) {
        return new a(eVar, z);
    }

    public c a(d dVar, boolean z) {
        return new b(dVar, z);
    }
}
