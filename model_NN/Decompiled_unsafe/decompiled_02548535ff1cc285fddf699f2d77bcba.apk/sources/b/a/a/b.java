package b.a.a;

import c.e;
import c.f;
import java.io.Closeable;
import java.util.List;

public interface b extends Closeable {

    public interface a {
        void a();

        void a(int i, int i2, int i3, boolean z);

        void a(int i, int i2, List<f> list);

        void a(int i, long j);

        void a(int i, a aVar);

        void a(int i, a aVar, f fVar);

        void a(boolean z, int i, int i2);

        void a(boolean z, int i, e eVar, int i2);

        void a(boolean z, n nVar);

        void a(boolean z, boolean z2, int i, int i2, List<f> list, g gVar);
    }

    void a();

    boolean a(a aVar);
}
