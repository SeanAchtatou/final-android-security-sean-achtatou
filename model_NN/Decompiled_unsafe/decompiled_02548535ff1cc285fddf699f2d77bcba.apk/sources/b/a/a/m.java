package b.a.a;

import c.e;
import java.util.List;

public interface m {

    /* renamed from: a  reason: collision with root package name */
    public static final m f340a = new m() {
        public void a(int i, a aVar) {
        }

        public boolean a(int i, e eVar, int i2, boolean z) {
            eVar.g((long) i2);
            return true;
        }

        public boolean a(int i, List<f> list) {
            return true;
        }

        public boolean a(int i, List<f> list, boolean z) {
            return true;
        }
    };

    void a(int i, a aVar);

    boolean a(int i, e eVar, int i2, boolean z);

    boolean a(int i, List<f> list);

    boolean a(int i, List<f> list, boolean z);
}
