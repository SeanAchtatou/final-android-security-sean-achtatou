package b.a;

public abstract class e implements Runnable {

    /* renamed from: b  reason: collision with root package name */
    protected final String f422b;

    public e(String str, Object... objArr) {
        this.f422b = String.format(str, objArr);
    }

    /* access modifiers changed from: protected */
    public abstract void b();

    public final void run() {
        String name = Thread.currentThread().getName();
        Thread.currentThread().setName(this.f422b);
        try {
            b();
        } finally {
            Thread.currentThread().setName(name);
        }
    }
}
