package b.a;

import b.a;
import b.a.b.r;
import b.a.c.b;
import b.e;
import b.f;
import b.j;
import b.k;
import b.q;
import b.u;
import java.util.logging.Logger;
import javax.net.ssl.SSLSocket;

public abstract class c {

    /* renamed from: a  reason: collision with root package name */
    public static final Logger f412a = Logger.getLogger(u.class.getName());

    /* renamed from: b  reason: collision with root package name */
    public static c f413b;

    public abstract r a(e eVar);

    public abstract b a(j jVar, a aVar, r rVar);

    public abstract d a(u uVar);

    public abstract h a(j jVar);

    public abstract void a(e eVar, f fVar, boolean z);

    public abstract void a(k kVar, SSLSocket sSLSocket, boolean z);

    public abstract void a(q.a aVar, String str);

    public abstract boolean a(j jVar, b bVar);

    public abstract void b(j jVar, b bVar);
}
