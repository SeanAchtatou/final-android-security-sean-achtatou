package b.a.b;

import b.a;
import b.a.c;
import b.a.c.b;
import b.a.h;
import b.a.i;
import b.ab;
import b.j;
import c.q;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.lang.ref.WeakReference;
import java.net.ProtocolException;
import java.net.SocketTimeoutException;
import java.security.cert.CertificateException;
import java.util.concurrent.TimeUnit;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLPeerUnverifiedException;

public final class r {

    /* renamed from: a  reason: collision with root package name */
    public final a f409a;

    /* renamed from: b  reason: collision with root package name */
    private ab f410b;

    /* renamed from: c  reason: collision with root package name */
    private final j f411c;
    private p d;
    private b e;
    private boolean f;
    private boolean g;
    private i h;

    public r(j jVar, a aVar) {
        this.f411c = jVar;
        this.f409a = aVar;
        this.d = new p(aVar, f());
    }

    private b a(int i, int i2, int i3, boolean z) {
        b bVar;
        ab abVar;
        synchronized (this.f411c) {
            if (this.f) {
                throw new IllegalStateException("released");
            } else if (this.h != null) {
                throw new IllegalStateException("stream != null");
            } else if (this.g) {
                throw new IOException("Canceled");
            } else {
                bVar = this.e;
                if (bVar == null || bVar.g) {
                    bVar = c.f413b.a(this.f411c, this.f409a, this);
                    if (bVar != null) {
                        this.e = bVar;
                    } else {
                        ab abVar2 = this.f410b;
                        if (abVar2 == null) {
                            ab b2 = this.d.b();
                            synchronized (this.f411c) {
                                this.f410b = b2;
                            }
                            abVar = b2;
                        } else {
                            abVar = abVar2;
                        }
                        bVar = new b(abVar);
                        a(bVar);
                        synchronized (this.f411c) {
                            c.f413b.b(this.f411c, bVar);
                            this.e = bVar;
                            if (this.g) {
                                throw new IOException("Canceled");
                            }
                        }
                        bVar.a(i, i2, i3, this.f409a.f(), z);
                        f().b(bVar.a());
                    }
                }
            }
        }
        return bVar;
    }

    private void a(boolean z, boolean z2, boolean z3) {
        b bVar = null;
        synchronized (this.f411c) {
            if (z3) {
                this.h = null;
            }
            if (z2) {
                this.f = true;
            }
            if (this.e != null) {
                if (z) {
                    this.e.g = true;
                }
                if (this.h == null && (this.f || this.e.g)) {
                    b(this.e);
                    if (this.e.f.isEmpty()) {
                        this.e.h = System.nanoTime();
                        if (c.f413b.a(this.f411c, this.e)) {
                            bVar = this.e;
                        }
                    }
                    this.e = null;
                }
            }
        }
        if (bVar != null) {
            i.a(bVar.c());
        }
    }

    private b b(int i, int i2, int i3, boolean z, boolean z2) {
        b a2;
        while (true) {
            a2 = a(i, i2, i3, z);
            synchronized (this.f411c) {
                if (a2.f417c != 0) {
                    if (a2.a(z2)) {
                        break;
                    }
                    a(new IOException());
                } else {
                    break;
                }
            }
        }
        return a2;
    }

    private void b(b bVar) {
        int size = bVar.f.size();
        for (int i = 0; i < size; i++) {
            if (bVar.f.get(i).get() == this) {
                bVar.f.remove(i);
                return;
            }
        }
        throw new IllegalStateException();
    }

    private boolean b(IOException iOException) {
        if (iOException instanceof ProtocolException) {
            return false;
        }
        return iOException instanceof InterruptedIOException ? iOException instanceof SocketTimeoutException : (!(iOException instanceof SSLHandshakeException) || !(iOException.getCause() instanceof CertificateException)) && !(iOException instanceof SSLPeerUnverifiedException);
    }

    private h f() {
        return c.f413b.a(this.f411c);
    }

    public i a() {
        i iVar;
        synchronized (this.f411c) {
            iVar = this.h;
        }
        return iVar;
    }

    public i a(int i, int i2, int i3, boolean z, boolean z2) {
        i dVar;
        try {
            b b2 = b(i, i2, i3, z, z2);
            if (b2.f416b != null) {
                dVar = new e(this, b2.f416b);
            } else {
                b2.c().setSoTimeout(i2);
                b2.d.a().a((long) i2, TimeUnit.MILLISECONDS);
                b2.e.a().a((long) i3, TimeUnit.MILLISECONDS);
                dVar = new d(this, b2.d, b2.e);
            }
            synchronized (this.f411c) {
                this.h = dVar;
            }
            return dVar;
        } catch (IOException e2) {
            throw new o(e2);
        }
    }

    public void a(b bVar) {
        bVar.f.add(new WeakReference(this));
    }

    public void a(IOException iOException) {
        synchronized (this.f411c) {
            if (this.e != null && this.e.f417c == 0) {
                if (!(this.f410b == null || iOException == null)) {
                    this.d.a(this.f410b, iOException);
                }
                this.f410b = null;
            }
        }
        a(true, false, true);
    }

    public void a(boolean z, i iVar) {
        synchronized (this.f411c) {
            if (iVar != null) {
                if (iVar == this.h) {
                    if (!z) {
                        this.e.f417c++;
                    }
                }
            }
            throw new IllegalStateException("expected " + this.h + " but was " + iVar);
        }
        a(z, false, true);
    }

    public boolean a(IOException iOException, q qVar) {
        if (this.e != null) {
            a(iOException);
        }
        return (this.d == null || this.d.a()) && b(iOException) && (qVar == null || (qVar instanceof n));
    }

    public synchronized b b() {
        return this.e;
    }

    public void c() {
        a(false, true, false);
    }

    public void d() {
        a(true, false, false);
    }

    public void e() {
        i iVar;
        b bVar;
        synchronized (this.f411c) {
            this.g = true;
            iVar = this.h;
            bVar = this.e;
        }
        if (iVar != null) {
            iVar.a();
        } else if (bVar != null) {
            bVar.b();
        }
    }

    public String toString() {
        return this.f409a.toString();
    }
}
