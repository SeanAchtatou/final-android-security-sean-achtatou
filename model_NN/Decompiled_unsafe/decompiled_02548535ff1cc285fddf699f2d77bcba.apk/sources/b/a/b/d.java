package b.a.b;

import b.aa;
import b.q;
import b.x;
import b.z;
import c.i;
import c.l;
import c.q;
import c.r;
import c.s;
import java.io.EOFException;
import java.io.IOException;
import java.net.ProtocolException;
import java.util.concurrent.TimeUnit;

public final class d implements i {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final r f365a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public final c.e f366b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public final c.d f367c;
    private g d;
    /* access modifiers changed from: private */
    public int e = 0;

    private abstract class a implements r {

        /* renamed from: a  reason: collision with root package name */
        protected final i f368a;

        /* renamed from: b  reason: collision with root package name */
        protected boolean f369b;

        private a() {
            this.f368a = new i(d.this.f366b.a());
        }

        public s a() {
            return this.f368a;
        }

        /* access modifiers changed from: protected */
        public final void a(boolean z) {
            if (d.this.e != 6) {
                if (d.this.e != 5) {
                    throw new IllegalStateException("state: " + d.this.e);
                }
                d.this.a(this.f368a);
                int unused = d.this.e = 6;
                if (d.this.f365a != null) {
                    d.this.f365a.a(!z, d.this);
                }
            }
        }
    }

    private final class b implements q {

        /* renamed from: b  reason: collision with root package name */
        private final i f372b;

        /* renamed from: c  reason: collision with root package name */
        private boolean f373c;

        private b() {
            this.f372b = new i(d.this.f367c.a());
        }

        public s a() {
            return this.f372b;
        }

        public void a_(c.c cVar, long j) {
            if (this.f373c) {
                throw new IllegalStateException("closed");
            } else if (j != 0) {
                d.this.f367c.k(j);
                d.this.f367c.b("\r\n");
                d.this.f367c.a_(cVar, j);
                d.this.f367c.b("\r\n");
            }
        }

        public synchronized void close() {
            if (!this.f373c) {
                this.f373c = true;
                d.this.f367c.b("0\r\n\r\n");
                d.this.a(this.f372b);
                int unused = d.this.e = 3;
            }
        }

        public synchronized void flush() {
            if (!this.f373c) {
                d.this.f367c.flush();
            }
        }
    }

    private class c extends a {
        private long e = -1;
        private boolean f = true;
        private final g g;

        c(g gVar) {
            super();
            this.g = gVar;
        }

        private void b() {
            if (this.e != -1) {
                d.this.f366b.q();
            }
            try {
                this.e = d.this.f366b.n();
                String trim = d.this.f366b.q().trim();
                if (this.e < 0 || (!trim.isEmpty() && !trim.startsWith(";"))) {
                    throw new ProtocolException("expected chunk size and optional extensions but was \"" + this.e + trim + "\"");
                } else if (this.e == 0) {
                    this.f = false;
                    this.g.a(d.this.e());
                    a(true);
                }
            } catch (NumberFormatException e2) {
                throw new ProtocolException(e2.getMessage());
            }
        }

        public long a(c.c cVar, long j) {
            if (j < 0) {
                throw new IllegalArgumentException("byteCount < 0: " + j);
            } else if (this.f369b) {
                throw new IllegalStateException("closed");
            } else if (!this.f) {
                return -1;
            } else {
                if (this.e == 0 || this.e == -1) {
                    b();
                    if (!this.f) {
                        return -1;
                    }
                }
                long a2 = d.this.f366b.a(cVar, Math.min(j, this.e));
                if (a2 == -1) {
                    a(false);
                    throw new ProtocolException("unexpected end of stream");
                }
                this.e -= a2;
                return a2;
            }
        }

        public void close() {
            if (!this.f369b) {
                if (this.f && !b.a.i.a(this, 100, TimeUnit.MILLISECONDS)) {
                    a(false);
                }
                this.f369b = true;
            }
        }
    }

    /* renamed from: b.a.b.d$d  reason: collision with other inner class name */
    private final class C0006d implements q {

        /* renamed from: b  reason: collision with root package name */
        private final i f375b;

        /* renamed from: c  reason: collision with root package name */
        private boolean f376c;
        private long d;

        private C0006d(long j) {
            this.f375b = new i(d.this.f367c.a());
            this.d = j;
        }

        public s a() {
            return this.f375b;
        }

        public void a_(c.c cVar, long j) {
            if (this.f376c) {
                throw new IllegalStateException("closed");
            }
            b.a.i.a(cVar.b(), 0, j);
            if (j > this.d) {
                throw new ProtocolException("expected " + this.d + " bytes but received " + j);
            }
            d.this.f367c.a_(cVar, j);
            this.d -= j;
        }

        public void close() {
            if (!this.f376c) {
                this.f376c = true;
                if (this.d > 0) {
                    throw new ProtocolException("unexpected end of stream");
                }
                d.this.a(this.f375b);
                int unused = d.this.e = 3;
            }
        }

        public void flush() {
            if (!this.f376c) {
                d.this.f367c.flush();
            }
        }
    }

    private class e extends a {
        private long e;

        public e(long j) {
            super();
            this.e = j;
            if (this.e == 0) {
                a(true);
            }
        }

        public long a(c.c cVar, long j) {
            if (j < 0) {
                throw new IllegalArgumentException("byteCount < 0: " + j);
            } else if (this.f369b) {
                throw new IllegalStateException("closed");
            } else if (this.e == 0) {
                return -1;
            } else {
                long a2 = d.this.f366b.a(cVar, Math.min(this.e, j));
                if (a2 == -1) {
                    a(false);
                    throw new ProtocolException("unexpected end of stream");
                }
                this.e -= a2;
                if (this.e == 0) {
                    a(true);
                }
                return a2;
            }
        }

        public void close() {
            if (!this.f369b) {
                if (this.e != 0 && !b.a.i.a(this, 100, TimeUnit.MILLISECONDS)) {
                    a(false);
                }
                this.f369b = true;
            }
        }
    }

    private class f extends a {
        private boolean e;

        private f() {
            super();
        }

        public long a(c.c cVar, long j) {
            if (j < 0) {
                throw new IllegalArgumentException("byteCount < 0: " + j);
            } else if (this.f369b) {
                throw new IllegalStateException("closed");
            } else if (this.e) {
                return -1;
            } else {
                long a2 = d.this.f366b.a(cVar, j);
                if (a2 != -1) {
                    return a2;
                }
                this.e = true;
                a(true);
                return -1;
            }
        }

        public void close() {
            if (!this.f369b) {
                if (!this.e) {
                    a(false);
                }
                this.f369b = true;
            }
        }
    }

    public d(r rVar, c.e eVar, c.d dVar) {
        this.f365a = rVar;
        this.f366b = eVar;
        this.f367c = dVar;
    }

    /* access modifiers changed from: private */
    public void a(i iVar) {
        s a2 = iVar.a();
        iVar.a(s.f602b);
        a2.f();
        a2.d_();
    }

    private r b(z zVar) {
        if (!g.a(zVar)) {
            return b(0);
        }
        if ("chunked".equalsIgnoreCase(zVar.a("Transfer-Encoding"))) {
            return b(this.d);
        }
        long a2 = j.a(zVar);
        return a2 != -1 ? b(a2) : g();
    }

    public aa a(z zVar) {
        return new k(zVar.e(), l.a(b(zVar)));
    }

    public q a(long j) {
        if (this.e != 1) {
            throw new IllegalStateException("state: " + this.e);
        }
        this.e = 2;
        return new C0006d(j);
    }

    public q a(x xVar, long j) {
        if ("chunked".equalsIgnoreCase(xVar.a("Transfer-Encoding"))) {
            return f();
        }
        if (j != -1) {
            return a(j);
        }
        throw new IllegalStateException("Cannot stream a request body without chunked encoding or a known content length!");
    }

    public void a() {
        b.a.c.b b2 = this.f365a.b();
        if (b2 != null) {
            b2.b();
        }
    }

    public void a(g gVar) {
        this.d = gVar;
    }

    public void a(n nVar) {
        if (this.e != 1) {
            throw new IllegalStateException("state: " + this.e);
        }
        this.e = 3;
        nVar.a(this.f367c);
    }

    public void a(b.q qVar, String str) {
        if (this.e != 0) {
            throw new IllegalStateException("state: " + this.e);
        }
        this.f367c.b(str).b("\r\n");
        int a2 = qVar.a();
        for (int i = 0; i < a2; i++) {
            this.f367c.b(qVar.a(i)).b(": ").b(qVar.b(i)).b("\r\n");
        }
        this.f367c.b("\r\n");
        this.e = 1;
    }

    public void a(x xVar) {
        this.d.b();
        a(xVar.c(), m.a(xVar, this.d.d().a().b().type()));
    }

    public z.a b() {
        return d();
    }

    public r b(long j) {
        if (this.e != 4) {
            throw new IllegalStateException("state: " + this.e);
        }
        this.e = 5;
        return new e(j);
    }

    public r b(g gVar) {
        if (this.e != 4) {
            throw new IllegalStateException("state: " + this.e);
        }
        this.e = 5;
        return new c(gVar);
    }

    public void c() {
        this.f367c.flush();
    }

    public z.a d() {
        q a2;
        z.a a3;
        if (this.e == 1 || this.e == 3) {
            do {
                try {
                    a2 = q.a(this.f366b.q());
                    a3 = new z.a().a(a2.f406a).a(a2.f407b).a(a2.f408c).a(e());
                } catch (EOFException e2) {
                    IOException iOException = new IOException("unexpected end of stream on " + this.f365a);
                    iOException.initCause(e2);
                    throw iOException;
                }
            } while (a2.f407b == 100);
            this.e = 4;
            return a3;
        }
        throw new IllegalStateException("state: " + this.e);
    }

    public b.q e() {
        q.a aVar = new q.a();
        while (true) {
            String q = this.f366b.q();
            if (q.length() == 0) {
                return aVar.a();
            }
            b.a.c.f413b.a(aVar, q);
        }
    }

    public c.q f() {
        if (this.e != 1) {
            throw new IllegalStateException("state: " + this.e);
        }
        this.e = 2;
        return new b();
    }

    public r g() {
        if (this.e != 4) {
            throw new IllegalStateException("state: " + this.e);
        } else if (this.f365a == null) {
            throw new IllegalStateException("streamAllocation == null");
        } else {
            this.e = 5;
            this.f365a.d();
            return new f();
        }
    }
}
