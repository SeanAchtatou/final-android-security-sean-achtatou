package b.a.b;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public final class o extends Exception {

    /* renamed from: a  reason: collision with root package name */
    private static final Method f401a;

    /* renamed from: b  reason: collision with root package name */
    private IOException f402b;

    static {
        Method method;
        try {
            method = Throwable.class.getDeclaredMethod("addSuppressed", Throwable.class);
        } catch (Exception e) {
            method = null;
        }
        f401a = method;
    }

    public o(IOException iOException) {
        super(iOException);
        this.f402b = iOException;
    }

    private void a(IOException iOException, IOException iOException2) {
        if (f401a != null) {
            try {
                f401a.invoke(iOException, iOException2);
            } catch (IllegalAccessException | InvocationTargetException e) {
            }
        }
    }

    public IOException a() {
        return this.f402b;
    }

    public void a(IOException iOException) {
        a(iOException, this.f402b);
        this.f402b = iOException;
    }
}
