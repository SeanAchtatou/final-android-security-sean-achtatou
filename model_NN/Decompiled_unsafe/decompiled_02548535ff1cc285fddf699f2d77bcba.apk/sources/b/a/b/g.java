package b.a.b;

import b.a.b.b;
import b.a.j;
import b.aa;
import b.ab;
import b.i;
import b.m;
import b.q;
import b.s;
import b.t;
import b.u;
import b.v;
import b.x;
import b.y;
import b.z;
import c.c;
import c.d;
import c.e;
import c.l;
import c.q;
import c.r;
import java.io.IOException;
import java.net.ProtocolException;
import java.net.Proxy;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSocketFactory;

public final class g {
    private static final aa e = new aa() {
        public t a() {
            return null;
        }

        public long b() {
            return 0;
        }

        public e c() {
            return new c();
        }
    };

    /* renamed from: a  reason: collision with root package name */
    final u f384a;

    /* renamed from: b  reason: collision with root package name */
    public final r f385b;

    /* renamed from: c  reason: collision with root package name */
    long f386c = -1;
    public final boolean d;
    private final z f;
    /* access modifiers changed from: private */
    public i g;
    private boolean h;
    private final x i;
    /* access modifiers changed from: private */
    public x j;
    private z k;
    private z l;
    private q m;
    private d n;
    private final boolean o;
    private final boolean p;
    private a q;
    private b r;

    class a implements s.a {

        /* renamed from: b  reason: collision with root package name */
        private final int f391b;

        /* renamed from: c  reason: collision with root package name */
        private final x f392c;
        private int d;

        a(int i, x xVar) {
            this.f391b = i;
            this.f392c = xVar;
        }

        public i a() {
            return g.this.f385b.b();
        }

        public z a(x xVar) {
            this.d++;
            if (this.f391b > 0) {
                s sVar = g.this.f384a.w().get(this.f391b - 1);
                b.a a2 = a().a().a();
                if (!xVar.a().f().equals(a2.a().f()) || xVar.a().g() != a2.a().g()) {
                    throw new IllegalStateException("network interceptor " + sVar + " must retain the same host and port");
                } else if (this.d > 1) {
                    throw new IllegalStateException("network interceptor " + sVar + " must call proceed() exactly once");
                }
            }
            if (this.f391b < g.this.f384a.w().size()) {
                a aVar = new a(this.f391b + 1, xVar);
                s sVar2 = g.this.f384a.w().get(this.f391b);
                z a3 = sVar2.a(aVar);
                if (aVar.d != 1) {
                    throw new IllegalStateException("network interceptor " + sVar2 + " must call proceed() exactly once");
                } else if (a3 != null) {
                    return a3;
                } else {
                    throw new NullPointerException("network interceptor " + sVar2 + " returned null");
                }
            } else {
                g.this.g.a(xVar);
                x unused = g.this.j = xVar;
                if (g.this.a(xVar) && xVar.d() != null) {
                    d a4 = l.a(g.this.g.a(xVar, xVar.d().b()));
                    xVar.d().a(a4);
                    a4.close();
                }
                z b2 = g.this.m();
                int b3 = b2.b();
                if ((b3 != 204 && b3 != 205) || b2.f().b() <= 0) {
                    return b2;
                }
                throw new ProtocolException("HTTP " + b3 + " had non-zero Content-Length: " + b2.f().b());
            }
        }
    }

    public g(u uVar, x xVar, boolean z, boolean z2, boolean z3, r rVar, n nVar, z zVar) {
        this.f384a = uVar;
        this.i = xVar;
        this.d = z;
        this.o = z2;
        this.p = z3;
        this.f385b = rVar == null ? new r(uVar.o(), a(uVar, xVar)) : rVar;
        this.m = nVar;
        this.f = zVar;
    }

    private static b.a a(u uVar, x xVar) {
        HostnameVerifier hostnameVerifier;
        SSLSocketFactory sSLSocketFactory;
        b.g gVar = null;
        if (xVar.g()) {
            sSLSocketFactory = uVar.j();
            hostnameVerifier = uVar.k();
            gVar = uVar.l();
        } else {
            hostnameVerifier = null;
            sSLSocketFactory = null;
        }
        return new b.a(xVar.a().f(), xVar.a().g(), uVar.h(), uVar.i(), sSLSocketFactory, hostnameVerifier, gVar, uVar.n(), uVar.d(), uVar.t(), uVar.u(), uVar.e());
    }

    private static b.q a(b.q qVar, b.q qVar2) {
        q.a aVar = new q.a();
        int a2 = qVar.a();
        for (int i2 = 0; i2 < a2; i2++) {
            String a3 = qVar.a(i2);
            String b2 = qVar.b(i2);
            if ((!"Warning".equalsIgnoreCase(a3) || !b2.startsWith("1")) && (!j.a(a3) || qVar2.a(a3) == null)) {
                aVar.a(a3, b2);
            }
        }
        int a4 = qVar2.a();
        for (int i3 = 0; i3 < a4; i3++) {
            String a5 = qVar2.a(i3);
            if (!"Content-Length".equalsIgnoreCase(a5) && j.a(a5)) {
                aVar.a(a5, qVar2.b(i3));
            }
        }
        return aVar.a();
    }

    private z a(final a aVar, z zVar) {
        c.q a2;
        if (aVar == null || (a2 = aVar.a()) == null) {
            return zVar;
        }
        final e c2 = zVar.f().c();
        final d a3 = l.a(a2);
        return zVar.g().a(new k(zVar.e(), l.a(new r() {

            /* renamed from: a  reason: collision with root package name */
            boolean f387a;

            public long a(c cVar, long j) {
                try {
                    long a2 = c2.a(cVar, j);
                    if (a2 == -1) {
                        if (!this.f387a) {
                            this.f387a = true;
                            a3.close();
                        }
                        return -1;
                    }
                    cVar.a(a3.c(), cVar.b() - a2, a2);
                    a3.u();
                    return a2;
                } catch (IOException e2) {
                    if (!this.f387a) {
                        this.f387a = true;
                        aVar.b();
                    }
                    throw e2;
                }
            }

            public c.s a() {
                return c2.a();
            }

            public void close() {
                if (!this.f387a && !b.a.i.a(this, 100, TimeUnit.MILLISECONDS)) {
                    this.f387a = true;
                    aVar.b();
                }
                c2.close();
            }
        }))).a();
    }

    private String a(List<b.l> list) {
        StringBuilder sb = new StringBuilder();
        int size = list.size();
        for (int i2 = 0; i2 < size; i2++) {
            if (i2 > 0) {
                sb.append("; ");
            }
            b.l lVar = list.get(i2);
            sb.append(lVar.a()).append('=').append(lVar.b());
        }
        return sb.toString();
    }

    public static boolean a(z zVar) {
        if (zVar.a().b().equals("HEAD")) {
            return false;
        }
        int b2 = zVar.b();
        if ((b2 >= 100 && b2 < 200) || b2 == 204 || b2 == 304) {
            return j.a(zVar) != -1 || "chunked".equalsIgnoreCase(zVar.a("Transfer-Encoding"));
        }
        return true;
    }

    private static boolean a(z zVar, z zVar2) {
        Date b2;
        if (zVar2.b() == 304) {
            return true;
        }
        Date b3 = zVar.e().b("Last-Modified");
        return (b3 == null || (b2 = zVar2.e().b("Last-Modified")) == null || b2.getTime() >= b3.getTime()) ? false : true;
    }

    private x b(x xVar) {
        x.a e2 = xVar.e();
        if (xVar.a("Host") == null) {
            e2.a("Host", b.a.i.a(xVar.a()));
        }
        if (xVar.a("Connection") == null) {
            e2.a("Connection", "Keep-Alive");
        }
        if (xVar.a("Accept-Encoding") == null) {
            this.h = true;
            e2.a("Accept-Encoding", "gzip");
        }
        List<b.l> a2 = this.f384a.f().a(xVar.a());
        if (!a2.isEmpty()) {
            e2.a("Cookie", a(a2));
        }
        if (xVar.a("User-Agent") == null) {
            e2.a("User-Agent", j.a());
        }
        return e2.a();
    }

    private static z b(z zVar) {
        return (zVar == null || zVar.f() == null) ? zVar : zVar.g().a((aa) null).a();
    }

    private z c(z zVar) {
        if (!this.h || !"gzip".equalsIgnoreCase(this.l.a("Content-Encoding")) || zVar.f() == null) {
            return zVar;
        }
        c.j jVar = new c.j(zVar.f().c());
        b.q a2 = zVar.e().b().b("Content-Encoding").b("Content-Length").a();
        return zVar.g().a(a2).a(new k(a2, l.a(jVar))).a();
    }

    private boolean j() {
        return this.o && a(this.j) && this.m == null;
    }

    private i k() {
        return this.f385b.a(this.f384a.a(), this.f384a.b(), this.f384a.c(), this.f384a.r(), !this.j.b().equals("GET"));
    }

    private void l() {
        b.a.d a2 = b.a.c.f413b.a(this.f384a);
        if (a2 != null) {
            if (b.a(this.l, this.j)) {
                this.q = a2.a(b(this.l));
            } else if (h.a(this.j.b())) {
                try {
                    a2.b(this.j);
                } catch (IOException e2) {
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public z m() {
        this.g.c();
        z a2 = this.g.b().a(this.j).a(this.f385b.b().e()).a(j.f394b, Long.toString(this.f386c)).a(j.f395c, Long.toString(System.currentTimeMillis())).a();
        if (!this.p) {
            a2 = a2.g().a(this.g.a(a2)).a();
        }
        if ("close".equalsIgnoreCase(a2.a().a("Connection")) || "close".equalsIgnoreCase(a2.a("Connection"))) {
            this.f385b.d();
        }
        return a2;
    }

    public g a(IOException iOException, c.q qVar) {
        if (!this.f385b.a(iOException, qVar) || !this.f384a.r()) {
            return null;
        }
        return new g(this.f384a, this.i, this.d, this.o, this.p, g(), (n) qVar, this.f);
    }

    public void a() {
        if (this.r == null) {
            if (this.g != null) {
                throw new IllegalStateException();
            }
            x b2 = b(this.i);
            b.a.d a2 = b.a.c.f413b.a(this.f384a);
            z a3 = a2 != null ? a2.a(b2) : null;
            this.r = new b.a(System.currentTimeMillis(), b2, a3).a();
            this.j = this.r.f360a;
            this.k = this.r.f361b;
            if (a2 != null) {
                a2.a(this.r);
            }
            if (a3 != null && this.k == null) {
                b.a.i.a(a3.f());
            }
            if (this.j == null && this.k == null) {
                this.l = new z.a().a(this.i).c(b(this.f)).a(v.HTTP_1_1).a(504).a("Unsatisfiable Request (only-if-cached)").a(e).a();
            } else if (this.j == null) {
                this.l = this.k.g().a(this.i).c(b(this.f)).b(b(this.k)).a();
                this.l = c(this.l);
            } else {
                try {
                    this.g = k();
                    this.g.a(this);
                    if (j()) {
                        long a4 = j.a(b2);
                        if (!this.d) {
                            this.g.a(this.j);
                            this.m = this.g.a(this.j, a4);
                        } else if (a4 > 2147483647L) {
                            throw new IllegalStateException("Use setFixedLengthStreamingMode() or setChunkedStreamingMode() for requests larger than 2 GiB.");
                        } else if (a4 != -1) {
                            this.g.a(this.j);
                            this.m = new n((int) a4);
                        } else {
                            this.m = new n();
                        }
                    }
                } catch (Throwable th) {
                    if (a3 != null) {
                        b.a.i.a(a3.f());
                    }
                    throw th;
                }
            }
        }
    }

    public void a(b.q qVar) {
        if (this.f384a.f() != m.f505a) {
            List<b.l> a2 = b.l.a(this.i.a(), qVar);
            if (!a2.isEmpty()) {
                this.f384a.f().a(this.i.a(), a2);
            }
        }
    }

    public boolean a(b.r rVar) {
        b.r a2 = this.i.a();
        return a2.f().equals(rVar.f()) && a2.g() == rVar.g() && a2.b().equals(rVar.b());
    }

    /* access modifiers changed from: package-private */
    public boolean a(x xVar) {
        return h.c(xVar.b());
    }

    public void b() {
        if (this.f386c != -1) {
            throw new IllegalStateException();
        }
        this.f386c = System.currentTimeMillis();
    }

    public z c() {
        if (this.l != null) {
            return this.l;
        }
        throw new IllegalStateException();
    }

    public i d() {
        return this.f385b.b();
    }

    public void e() {
        this.f385b.c();
    }

    public void f() {
        this.f385b.e();
    }

    public r g() {
        if (this.n != null) {
            b.a.i.a(this.n);
        } else if (this.m != null) {
            b.a.i.a(this.m);
        }
        if (this.l != null) {
            b.a.i.a(this.l.f());
        } else {
            this.f385b.a((IOException) null);
        }
        return this.f385b;
    }

    public void h() {
        z m2;
        if (this.l == null) {
            if (this.j == null && this.k == null) {
                throw new IllegalStateException("call sendRequest() first!");
            } else if (this.j != null) {
                if (this.p) {
                    this.g.a(this.j);
                    m2 = m();
                } else if (!this.o) {
                    m2 = new a(0, this.j).a(this.j);
                } else {
                    if (this.n != null && this.n.c().b() > 0) {
                        this.n.e();
                    }
                    if (this.f386c == -1) {
                        if (j.a(this.j) == -1 && (this.m instanceof n)) {
                            this.j = this.j.e().a("Content-Length", Long.toString(((n) this.m).b())).a();
                        }
                        this.g.a(this.j);
                    }
                    if (this.m != null) {
                        if (this.n != null) {
                            this.n.close();
                        } else {
                            this.m.close();
                        }
                        if (this.m instanceof n) {
                            this.g.a((n) this.m);
                        }
                    }
                    m2 = m();
                }
                a(m2.e());
                if (this.k != null) {
                    if (a(this.k, m2)) {
                        this.l = this.k.g().a(this.i).c(b(this.f)).a(a(this.k.e(), m2.e())).b(b(this.k)).a(b(m2)).a();
                        m2.f().close();
                        e();
                        b.a.d a2 = b.a.c.f413b.a(this.f384a);
                        a2.a();
                        a2.a(this.k, b(this.l));
                        this.l = c(this.l);
                        return;
                    }
                    b.a.i.a(this.k.f());
                }
                this.l = m2.g().a(this.i).c(b(this.f)).b(b(this.k)).a(b(m2)).a();
                if (a(this.l)) {
                    l();
                    this.l = c(a(this.q, this.l));
                }
            }
        }
    }

    public x i() {
        String a2;
        b.r c2;
        if (this.l == null) {
            throw new IllegalStateException();
        }
        b.a.c.b b2 = this.f385b.b();
        ab a3 = b2 != null ? b2.a() : null;
        int b3 = this.l.b();
        String b4 = this.i.b();
        switch (b3) {
            case 300:
            case 301:
            case 302:
            case 303:
                if (this.f384a.q() || (a2 = this.l.a("Location")) == null || (c2 = this.i.a().c(a2)) == null) {
                    return null;
                }
                if (!c2.b().equals(this.i.a().b()) && !this.f384a.p()) {
                    return null;
                }
                x.a e2 = this.i.e();
                if (h.c(b4)) {
                    if (h.d(b4)) {
                        e2.a("GET", (y) null);
                    } else {
                        e2.a(b4, (y) null);
                    }
                    e2.b("Transfer-Encoding");
                    e2.b("Content-Length");
                    e2.b("Content-Type");
                }
                if (!a(c2)) {
                    e2.b("Authorization");
                }
                return e2.a(c2).a();
            case 307:
            case 308:
                if (!b4.equals("GET") && !b4.equals("HEAD")) {
                    return null;
                }
                return this.f384a.q() ? null : null;
            case 401:
                return this.f384a.m().a(a3, this.l);
            case 407:
                if ((a3 != null ? a3.b() : this.f384a.d()).type() != Proxy.Type.HTTP) {
                    throw new ProtocolException("Received HTTP_PROXY_AUTH (407) code while not using proxy");
                }
                return this.f384a.m().a(a3, this.l);
            default:
                return null;
        }
    }
}
