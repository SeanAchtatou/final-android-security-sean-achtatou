package b.a.b;

import b.a.g;
import b.q;
import b.x;
import b.z;

public final class j {

    /* renamed from: a  reason: collision with root package name */
    static final String f393a = g.a().b();

    /* renamed from: b  reason: collision with root package name */
    public static final String f394b = (f393a + "-Sent-Millis");

    /* renamed from: c  reason: collision with root package name */
    public static final String f395c = (f393a + "-Received-Millis");
    public static final String d = (f393a + "-Selected-Protocol");
    public static final String e = (f393a + "-Response-Source");

    public static long a(q qVar) {
        return b(qVar.a("Content-Length"));
    }

    public static long a(x xVar) {
        return a(xVar.c());
    }

    public static long a(z zVar) {
        return a(zVar.e());
    }

    static boolean a(String str) {
        return !"Connection".equalsIgnoreCase(str) && !"Keep-Alive".equalsIgnoreCase(str) && !"Proxy-Authenticate".equalsIgnoreCase(str) && !"Proxy-Authorization".equalsIgnoreCase(str) && !"TE".equalsIgnoreCase(str) && !"Trailers".equalsIgnoreCase(str) && !"Transfer-Encoding".equalsIgnoreCase(str) && !"Upgrade".equalsIgnoreCase(str);
    }

    private static long b(String str) {
        if (str == null) {
            return -1;
        }
        try {
            return Long.parseLong(str);
        } catch (NumberFormatException e2) {
            return -1;
        }
    }
}
