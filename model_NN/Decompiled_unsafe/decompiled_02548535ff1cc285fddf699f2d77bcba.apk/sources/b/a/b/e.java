package b.a.b;

import b.a.a.d;
import b.a.i;
import b.aa;
import b.q;
import b.v;
import b.x;
import b.z;
import c.f;
import c.h;
import c.l;
import c.r;
import java.net.ProtocolException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public final class e implements i {

    /* renamed from: a  reason: collision with root package name */
    private static final f f377a = f.a("connection");

    /* renamed from: b  reason: collision with root package name */
    private static final f f378b = f.a("host");

    /* renamed from: c  reason: collision with root package name */
    private static final f f379c = f.a("keep-alive");
    private static final f d = f.a("proxy-connection");
    private static final f e = f.a("transfer-encoding");
    private static final f f = f.a("te");
    private static final f g = f.a("encoding");
    private static final f h = f.a("upgrade");
    private static final List<f> i = i.a(f377a, f378b, f379c, d, e, b.a.a.f.f301b, b.a.a.f.f302c, b.a.a.f.d, b.a.a.f.e, b.a.a.f.f, b.a.a.f.g);
    private static final List<f> j = i.a(f377a, f378b, f379c, d, e);
    private static final List<f> k = i.a(f377a, f378b, f379c, d, f, e, g, h, b.a.a.f.f301b, b.a.a.f.f302c, b.a.a.f.d, b.a.a.f.e, b.a.a.f.f, b.a.a.f.g);
    private static final List<f> l = i.a(f377a, f378b, f379c, d, f, e, g, h);
    /* access modifiers changed from: private */
    public final r m;
    private final d n;
    private g o;
    private b.a.a.e p;

    class a extends h {
        public a(r rVar) {
            super(rVar);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: b.a.b.r.a(boolean, b.a.b.i):void
         arg types: [int, b.a.b.e]
         candidates:
          b.a.b.r.a(java.io.IOException, c.q):boolean
          b.a.b.r.a(boolean, b.a.b.i):void */
        public void close() {
            e.this.m.a(false, (i) e.this);
            super.close();
        }
    }

    public e(r rVar, d dVar) {
        this.m = rVar;
        this.n = dVar;
    }

    public static z.a a(List<b.a.a.f> list) {
        String str = null;
        String str2 = "HTTP/1.1";
        q.a aVar = new q.a();
        int size = list.size();
        int i2 = 0;
        while (i2 < size) {
            f fVar = list.get(i2).h;
            String a2 = list.get(i2).i.a();
            String str3 = str2;
            int i3 = 0;
            while (i3 < a2.length()) {
                int indexOf = a2.indexOf(0, i3);
                if (indexOf == -1) {
                    indexOf = a2.length();
                }
                String substring = a2.substring(i3, indexOf);
                if (!fVar.equals(b.a.a.f.f300a)) {
                    if (fVar.equals(b.a.a.f.g)) {
                        str3 = substring;
                        substring = str;
                    } else {
                        if (!j.contains(fVar)) {
                            aVar.a(fVar.a(), substring);
                        }
                        substring = str;
                    }
                }
                str = substring;
                i3 = indexOf + 1;
            }
            i2++;
            str2 = str3;
        }
        if (str == null) {
            throw new ProtocolException("Expected ':status' header not present");
        }
        q a3 = q.a(str2 + " " + str);
        return new z.a().a(v.SPDY_3).a(a3.f407b).a(a3.f408c).a(aVar.a());
    }

    private static String a(String str, String str2) {
        return str + 0 + str2;
    }

    public static z.a b(List<b.a.a.f> list) {
        String str = null;
        q.a aVar = new q.a();
        int size = list.size();
        int i2 = 0;
        while (i2 < size) {
            f fVar = list.get(i2).h;
            String a2 = list.get(i2).i.a();
            if (!fVar.equals(b.a.a.f.f300a)) {
                if (!l.contains(fVar)) {
                    aVar.a(fVar.a(), a2);
                }
                a2 = str;
            }
            i2++;
            str = a2;
        }
        if (str == null) {
            throw new ProtocolException("Expected ':status' header not present");
        }
        q a3 = q.a("HTTP/1.1 " + str);
        return new z.a().a(v.HTTP_2).a(a3.f407b).a(a3.f408c).a(aVar.a());
    }

    public static List<b.a.a.f> b(x xVar) {
        q c2 = xVar.c();
        ArrayList arrayList = new ArrayList(c2.a() + 5);
        arrayList.add(new b.a.a.f(b.a.a.f.f301b, xVar.b()));
        arrayList.add(new b.a.a.f(b.a.a.f.f302c, m.a(xVar.a())));
        arrayList.add(new b.a.a.f(b.a.a.f.g, "HTTP/1.1"));
        arrayList.add(new b.a.a.f(b.a.a.f.f, i.a(xVar.a())));
        arrayList.add(new b.a.a.f(b.a.a.f.d, xVar.a().b()));
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        int a2 = c2.a();
        for (int i2 = 0; i2 < a2; i2++) {
            f a3 = f.a(c2.a(i2).toLowerCase(Locale.US));
            if (!i.contains(a3)) {
                String b2 = c2.b(i2);
                if (linkedHashSet.add(a3)) {
                    arrayList.add(new b.a.a.f(a3, b2));
                } else {
                    int i3 = 0;
                    while (true) {
                        if (i3 >= arrayList.size()) {
                            break;
                        } else if (((b.a.a.f) arrayList.get(i3)).h.equals(a3)) {
                            arrayList.set(i3, new b.a.a.f(a3, a(((b.a.a.f) arrayList.get(i3)).i.a(), b2)));
                            break;
                        } else {
                            i3++;
                        }
                    }
                }
            }
        }
        return arrayList;
    }

    public static List<b.a.a.f> c(x xVar) {
        q c2 = xVar.c();
        ArrayList arrayList = new ArrayList(c2.a() + 4);
        arrayList.add(new b.a.a.f(b.a.a.f.f301b, xVar.b()));
        arrayList.add(new b.a.a.f(b.a.a.f.f302c, m.a(xVar.a())));
        arrayList.add(new b.a.a.f(b.a.a.f.e, i.a(xVar.a())));
        arrayList.add(new b.a.a.f(b.a.a.f.d, xVar.a().b()));
        int a2 = c2.a();
        for (int i2 = 0; i2 < a2; i2++) {
            f a3 = f.a(c2.a(i2).toLowerCase(Locale.US));
            if (!k.contains(a3)) {
                arrayList.add(new b.a.a.f(a3, c2.b(i2)));
            }
        }
        return arrayList;
    }

    public aa a(z zVar) {
        return new k(zVar.e(), l.a(new a(this.p.g())));
    }

    public c.q a(x xVar, long j2) {
        return this.p.h();
    }

    public void a() {
        if (this.p != null) {
            this.p.b(b.a.a.a.CANCEL);
        }
    }

    public void a(g gVar) {
        this.o = gVar;
    }

    public void a(n nVar) {
        nVar.a(this.p.h());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: b.a.a.d.a(java.util.List<b.a.a.f>, boolean, boolean):b.a.a.e
     arg types: [java.util.List<b.a.a.f>, boolean, int]
     candidates:
      b.a.a.d.a(int, java.util.List<b.a.a.f>, boolean):void
      b.a.a.d.a(b.a.a.d, int, b.a.a.a):void
      b.a.a.d.a(b.a.a.d, int, java.util.List):void
      b.a.a.d.a(b.a.a.d, b.a.a.a, b.a.a.a):void
      b.a.a.d.a(java.util.List<b.a.a.f>, boolean, boolean):b.a.a.e */
    public void a(x xVar) {
        if (this.p == null) {
            this.o.b();
            this.p = this.n.a(this.n.a() == v.HTTP_2 ? c(xVar) : b(xVar), this.o.a(xVar), true);
            this.p.e().a((long) this.o.f384a.b(), TimeUnit.MILLISECONDS);
            this.p.f().a((long) this.o.f384a.c(), TimeUnit.MILLISECONDS);
        }
    }

    public z.a b() {
        return this.n.a() == v.HTTP_2 ? b(this.p.d()) : a(this.p.d());
    }

    public void c() {
        this.p.h().close();
    }
}
