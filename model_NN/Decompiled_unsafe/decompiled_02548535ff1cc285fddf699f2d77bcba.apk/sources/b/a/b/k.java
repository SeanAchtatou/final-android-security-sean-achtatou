package b.a.b;

import b.aa;
import b.q;
import b.t;
import c.e;

public final class k extends aa {

    /* renamed from: a  reason: collision with root package name */
    private final q f396a;

    /* renamed from: b  reason: collision with root package name */
    private final e f397b;

    public k(q qVar, e eVar) {
        this.f396a = qVar;
        this.f397b = eVar;
    }

    public t a() {
        String a2 = this.f396a.a("Content-Type");
        if (a2 != null) {
            return t.a(a2);
        }
        return null;
    }

    public long b() {
        return j.a(this.f396a);
    }

    public e c() {
        return this.f397b;
    }
}
