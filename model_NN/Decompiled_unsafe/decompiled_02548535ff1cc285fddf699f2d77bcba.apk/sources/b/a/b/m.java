package b.a.b;

import b.r;
import b.x;
import java.net.Proxy;

public final class m {
    public static String a(r rVar) {
        String h = rVar.h();
        String j = rVar.j();
        return j != null ? h + '?' + j : h;
    }

    static String a(x xVar, Proxy.Type type) {
        StringBuilder sb = new StringBuilder();
        sb.append(xVar.b());
        sb.append(' ');
        if (b(xVar, type)) {
            sb.append(xVar.a());
        } else {
            sb.append(a(xVar.a()));
        }
        sb.append(" HTTP/1.1");
        return sb.toString();
    }

    private static boolean b(x xVar, Proxy.Type type) {
        return !xVar.g() && type == Proxy.Type.HTTP;
    }
}
