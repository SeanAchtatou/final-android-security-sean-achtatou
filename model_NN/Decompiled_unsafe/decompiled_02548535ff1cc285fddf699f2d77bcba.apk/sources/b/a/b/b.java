package b.a.b;

import b.d;
import b.q;
import b.x;
import b.z;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public final class b {

    /* renamed from: a  reason: collision with root package name */
    public final x f360a;

    /* renamed from: b  reason: collision with root package name */
    public final z f361b;

    public static class a {

        /* renamed from: a  reason: collision with root package name */
        final long f362a;

        /* renamed from: b  reason: collision with root package name */
        final x f363b;

        /* renamed from: c  reason: collision with root package name */
        final z f364c;
        private Date d;
        private String e;
        private Date f;
        private String g;
        private Date h;
        private long i;
        private long j;
        private String k;
        private int l = -1;

        public a(long j2, x xVar, z zVar) {
            this.f362a = j2;
            this.f363b = xVar;
            this.f364c = zVar;
            if (zVar != null) {
                q e2 = zVar.e();
                int a2 = e2.a();
                for (int i2 = 0; i2 < a2; i2++) {
                    String a3 = e2.a(i2);
                    String b2 = e2.b(i2);
                    if ("Date".equalsIgnoreCase(a3)) {
                        this.d = f.a(b2);
                        this.e = b2;
                    } else if ("Expires".equalsIgnoreCase(a3)) {
                        this.h = f.a(b2);
                    } else if ("Last-Modified".equalsIgnoreCase(a3)) {
                        this.f = f.a(b2);
                        this.g = b2;
                    } else if ("ETag".equalsIgnoreCase(a3)) {
                        this.k = b2;
                    } else if ("Age".equalsIgnoreCase(a3)) {
                        this.l = c.b(b2, -1);
                    } else if (j.f394b.equalsIgnoreCase(a3)) {
                        this.i = Long.parseLong(b2);
                    } else if (j.f395c.equalsIgnoreCase(a3)) {
                        this.j = Long.parseLong(b2);
                    }
                }
            }
        }

        private static boolean a(x xVar) {
            return (xVar.a("If-Modified-Since") == null && xVar.a("If-None-Match") == null) ? false : true;
        }

        private b b() {
            long j2 = 0;
            if (this.f364c == null) {
                return new b(this.f363b, null);
            }
            if (this.f363b.g() && this.f364c.d() == null) {
                return new b(this.f363b, null);
            }
            if (!b.a(this.f364c, this.f363b)) {
                return new b(this.f363b, null);
            }
            d f2 = this.f363b.f();
            if (f2.a() || a(this.f363b)) {
                return new b(this.f363b, null);
            }
            long d2 = d();
            long c2 = c();
            if (f2.c() != -1) {
                c2 = Math.min(c2, TimeUnit.SECONDS.toMillis((long) f2.c()));
            }
            long millis = f2.h() != -1 ? TimeUnit.SECONDS.toMillis((long) f2.h()) : 0;
            d h2 = this.f364c.h();
            if (!h2.f() && f2.g() != -1) {
                j2 = TimeUnit.SECONDS.toMillis((long) f2.g());
            }
            if (h2.a() || d2 + millis >= j2 + c2) {
                x.a e2 = this.f363b.e();
                if (this.k != null) {
                    e2.a("If-None-Match", this.k);
                } else if (this.f != null) {
                    e2.a("If-Modified-Since", this.g);
                } else if (this.d != null) {
                    e2.a("If-Modified-Since", this.e);
                }
                x a2 = e2.a();
                return a(a2) ? new b(a2, this.f364c) : new b(a2, null);
            }
            z.a g2 = this.f364c.g();
            if (millis + d2 >= c2) {
                g2.b("Warning", "110 HttpURLConnection \"Response is stale\"");
            }
            if (d2 > 86400000 && e()) {
                g2.b("Warning", "113 HttpURLConnection \"Heuristic expiration\"");
            }
            return new b(null, g2.a());
        }

        private long c() {
            d h2 = this.f364c.h();
            if (h2.c() != -1) {
                return TimeUnit.SECONDS.toMillis((long) h2.c());
            }
            if (this.h != null) {
                long time = this.h.getTime() - (this.d != null ? this.d.getTime() : this.j);
                if (time <= 0) {
                    time = 0;
                }
                return time;
            } else if (this.f == null || this.f364c.a().a().k() != null) {
                return 0;
            } else {
                long time2 = (this.d != null ? this.d.getTime() : this.i) - this.f.getTime();
                if (time2 > 0) {
                    return time2 / 10;
                }
                return 0;
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.lang.Math.max(long, long):long}
         arg types: [int, long]
         candidates:
          ClspMth{java.lang.Math.max(double, double):double}
          ClspMth{java.lang.Math.max(int, int):int}
          ClspMth{java.lang.Math.max(float, float):float}
          ClspMth{java.lang.Math.max(long, long):long} */
        private long d() {
            long j2 = 0;
            if (this.d != null) {
                j2 = Math.max(0L, this.j - this.d.getTime());
            }
            if (this.l != -1) {
                j2 = Math.max(j2, TimeUnit.SECONDS.toMillis((long) this.l));
            }
            return j2 + (this.j - this.i) + (this.f362a - this.j);
        }

        private boolean e() {
            return this.f364c.h().c() == -1 && this.h == null;
        }

        public b a() {
            b b2 = b();
            return (b2.f360a == null || !this.f363b.f().i()) ? b2 : new b(null, null);
        }
    }

    private b(x xVar, z zVar) {
        this.f360a = xVar;
        this.f361b = zVar;
    }

    public static boolean a(z zVar, x xVar) {
        switch (zVar.b()) {
            case 200:
            case 203:
            case 204:
            case 300:
            case 301:
            case 308:
            case 404:
            case 405:
            case 410:
            case 414:
            case 501:
                break;
            default:
                return false;
            case 302:
            case 307:
                if (zVar.a("Expires") == null && zVar.h().c() == -1 && !zVar.h().e() && !zVar.h().d()) {
                    return false;
                }
        }
        return !zVar.h().b() && !xVar.f().b();
    }
}
