package b.a.b;

import b.a;
import b.a.h;
import b.ab;
import b.r;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.SocketAddress;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;

public final class p {

    /* renamed from: a  reason: collision with root package name */
    private final a f403a;

    /* renamed from: b  reason: collision with root package name */
    private final h f404b;

    /* renamed from: c  reason: collision with root package name */
    private Proxy f405c;
    private InetSocketAddress d;
    private List<Proxy> e = Collections.emptyList();
    private int f;
    private List<InetSocketAddress> g = Collections.emptyList();
    private int h;
    private final List<ab> i = new ArrayList();

    public p(a aVar, h hVar) {
        this.f403a = aVar;
        this.f404b = hVar;
        a(aVar.a(), aVar.h());
    }

    static String a(InetSocketAddress inetSocketAddress) {
        InetAddress address = inetSocketAddress.getAddress();
        return address == null ? inetSocketAddress.getHostName() : address.getHostAddress();
    }

    private void a(r rVar, Proxy proxy) {
        if (proxy != null) {
            this.e = Collections.singletonList(proxy);
        } else {
            this.e = new ArrayList();
            List<Proxy> select = this.f403a.g().select(rVar.a());
            if (select != null) {
                this.e.addAll(select);
            }
            this.e.removeAll(Collections.singleton(Proxy.NO_PROXY));
            this.e.add(Proxy.NO_PROXY);
        }
        this.f = 0;
    }

    private void a(Proxy proxy) {
        int i2;
        String str;
        this.g = new ArrayList();
        if (proxy.type() == Proxy.Type.DIRECT || proxy.type() == Proxy.Type.SOCKS) {
            String f2 = this.f403a.a().f();
            i2 = this.f403a.a().g();
            str = f2;
        } else {
            SocketAddress address = proxy.address();
            if (!(address instanceof InetSocketAddress)) {
                throw new IllegalArgumentException("Proxy.address() is not an InetSocketAddress: " + address.getClass());
            }
            InetSocketAddress inetSocketAddress = (InetSocketAddress) address;
            String a2 = a(inetSocketAddress);
            i2 = inetSocketAddress.getPort();
            str = a2;
        }
        if (i2 < 1 || i2 > 65535) {
            throw new SocketException("No route to " + str + ":" + i2 + "; port is out of range");
        }
        if (proxy.type() == Proxy.Type.SOCKS) {
            this.g.add(InetSocketAddress.createUnresolved(str, i2));
        } else {
            List<InetAddress> a3 = this.f403a.b().a(str);
            int size = a3.size();
            for (int i3 = 0; i3 < size; i3++) {
                this.g.add(new InetSocketAddress(a3.get(i3), i2));
            }
        }
        this.h = 0;
    }

    private boolean c() {
        return this.f < this.e.size();
    }

    private Proxy d() {
        if (!c()) {
            throw new SocketException("No route to " + this.f403a.a().f() + "; exhausted proxy configurations: " + this.e);
        }
        List<Proxy> list = this.e;
        int i2 = this.f;
        this.f = i2 + 1;
        Proxy proxy = list.get(i2);
        a(proxy);
        return proxy;
    }

    private boolean e() {
        return this.h < this.g.size();
    }

    private InetSocketAddress f() {
        if (!e()) {
            throw new SocketException("No route to " + this.f403a.a().f() + "; exhausted inet socket addresses: " + this.g);
        }
        List<InetSocketAddress> list = this.g;
        int i2 = this.h;
        this.h = i2 + 1;
        return list.get(i2);
    }

    private boolean g() {
        return !this.i.isEmpty();
    }

    private ab h() {
        return this.i.remove(0);
    }

    public void a(ab abVar, IOException iOException) {
        if (!(abVar.b().type() == Proxy.Type.DIRECT || this.f403a.g() == null)) {
            this.f403a.g().connectFailed(this.f403a.a().a(), abVar.b().address(), iOException);
        }
        this.f404b.a(abVar);
    }

    public boolean a() {
        return e() || c() || g();
    }

    public ab b() {
        if (!e()) {
            if (c()) {
                this.f405c = d();
            } else if (g()) {
                return h();
            } else {
                throw new NoSuchElementException();
            }
        }
        this.d = f();
        ab abVar = new ab(this.f403a, this.f405c, this.d);
        if (!this.f404b.c(abVar)) {
            return abVar;
        }
        this.i.add(abVar);
        return b();
    }
}
