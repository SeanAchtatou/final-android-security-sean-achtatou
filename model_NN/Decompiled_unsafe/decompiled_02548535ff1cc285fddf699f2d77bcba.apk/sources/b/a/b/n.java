package b.a.b;

import b.a.i;
import c.c;
import c.q;
import c.s;
import java.net.ProtocolException;

public final class n implements q {

    /* renamed from: a  reason: collision with root package name */
    private boolean f398a;

    /* renamed from: b  reason: collision with root package name */
    private final int f399b;

    /* renamed from: c  reason: collision with root package name */
    private final c f400c;

    public n() {
        this(-1);
    }

    public n(int i) {
        this.f400c = new c();
        this.f399b = i;
    }

    public s a() {
        return s.f602b;
    }

    public void a(q qVar) {
        c cVar = new c();
        this.f400c.a(cVar, 0, this.f400c.b());
        qVar.a_(cVar, cVar.b());
    }

    public void a_(c cVar, long j) {
        if (this.f398a) {
            throw new IllegalStateException("closed");
        }
        i.a(cVar.b(), 0, j);
        if (this.f399b == -1 || this.f400c.b() <= ((long) this.f399b) - j) {
            this.f400c.a_(cVar, j);
            return;
        }
        throw new ProtocolException("exceeded content-length limit of " + this.f399b + " bytes");
    }

    public long b() {
        return this.f400c.b();
    }

    public void close() {
        if (!this.f398a) {
            this.f398a = true;
            if (this.f400c.b() < ((long) this.f399b)) {
                throw new ProtocolException("content-length promised " + this.f399b + " bytes, but received " + this.f400c.b());
            }
        }
    }

    public void flush() {
    }
}
