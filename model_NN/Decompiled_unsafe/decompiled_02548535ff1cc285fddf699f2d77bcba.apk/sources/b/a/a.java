package b.a;

import b.k;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.ProtocolException;
import java.net.UnknownServiceException;
import java.security.cert.CertificateException;
import java.util.Arrays;
import java.util.List;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLProtocolException;
import javax.net.ssl.SSLSocket;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private final List<k> f256a;

    /* renamed from: b  reason: collision with root package name */
    private int f257b = 0;

    /* renamed from: c  reason: collision with root package name */
    private boolean f258c;
    private boolean d;

    public a(List<k> list) {
        this.f256a = list;
    }

    private boolean b(SSLSocket sSLSocket) {
        int i = this.f257b;
        while (true) {
            int i2 = i;
            if (i2 >= this.f256a.size()) {
                return false;
            }
            if (this.f256a.get(i2).a(sSLSocket)) {
                return true;
            }
            i = i2 + 1;
        }
    }

    public k a(SSLSocket sSLSocket) {
        k kVar;
        int i = this.f257b;
        int size = this.f256a.size();
        int i2 = i;
        while (true) {
            if (i2 >= size) {
                kVar = null;
                break;
            }
            kVar = this.f256a.get(i2);
            if (kVar.a(sSLSocket)) {
                this.f257b = i2 + 1;
                break;
            }
            i2++;
        }
        if (kVar == null) {
            throw new UnknownServiceException("Unable to find acceptable protocols. isFallback=" + this.d + ", modes=" + this.f256a + ", supported protocols=" + Arrays.toString(sSLSocket.getEnabledProtocols()));
        }
        this.f258c = b(sSLSocket);
        c.f413b.a(kVar, sSLSocket, this.d);
        return kVar;
    }

    public boolean a(IOException iOException) {
        this.d = true;
        if (!this.f258c || (iOException instanceof ProtocolException) || (iOException instanceof InterruptedIOException)) {
            return false;
        }
        if ((!(iOException instanceof SSLHandshakeException) || !(iOException.getCause() instanceof CertificateException)) && !(iOException instanceof SSLPeerUnverifiedException)) {
            return (iOException instanceof SSLHandshakeException) || (iOException instanceof SSLProtocolException);
        }
        return false;
    }
}
