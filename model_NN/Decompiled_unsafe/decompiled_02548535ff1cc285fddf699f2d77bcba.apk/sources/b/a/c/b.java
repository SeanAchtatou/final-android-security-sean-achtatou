package b.a.c;

import b.a.a;
import b.a.a.d;
import b.a.b.r;
import b.a.g;
import b.a.j;
import b.ab;
import b.i;
import b.p;
import b.v;
import b.x;
import c.e;
import c.l;
import java.io.IOException;
import java.lang.ref.Reference;
import java.net.ConnectException;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

public final class b implements i {

    /* renamed from: a  reason: collision with root package name */
    public Socket f415a;

    /* renamed from: b  reason: collision with root package name */
    public volatile d f416b;

    /* renamed from: c  reason: collision with root package name */
    public int f417c;
    public e d;
    public c.d e;
    public final List<Reference<r>> f = new ArrayList();
    public boolean g;
    public long h = Long.MAX_VALUE;
    private final ab i;
    private Socket j;
    private p k;
    private v l;

    public b(ab abVar) {
        this.i = abVar;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x00ca  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x008d A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00aa A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:3:0x0075  */
    private void a(int r9, int r10) {
        /*
            r8 = this;
            b.x r0 = r8.f()
            b.r r1 = r0.a()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "CONNECT "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = r1.f()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = ":"
            java.lang.StringBuilder r2 = r2.append(r3)
            int r1 = r1.g()
            java.lang.StringBuilder r1 = r2.append(r1)
            java.lang.String r2 = " HTTP/1.1"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = r1.toString()
        L_0x0033:
            b.a.b.d r3 = new b.a.b.d
            r1 = 0
            c.e r4 = r8.d
            c.d r5 = r8.e
            r3.<init>(r1, r4, r5)
            c.e r1 = r8.d
            c.s r1 = r1.a()
            long r4 = (long) r9
            java.util.concurrent.TimeUnit r6 = java.util.concurrent.TimeUnit.MILLISECONDS
            r1.a(r4, r6)
            c.d r1 = r8.e
            c.s r1 = r1.a()
            long r4 = (long) r10
            java.util.concurrent.TimeUnit r6 = java.util.concurrent.TimeUnit.MILLISECONDS
            r1.a(r4, r6)
            b.q r1 = r0.c()
            r3.a(r1, r2)
            r3.c()
            b.z$a r1 = r3.d()
            b.z$a r0 = r1.a(r0)
            b.z r4 = r0.a()
            long r0 = b.a.b.j.a(r4)
            r6 = -1
            int r5 = (r0 > r6 ? 1 : (r0 == r6 ? 0 : -1))
            if (r5 != 0) goto L_0x0077
            r0 = 0
        L_0x0077:
            c.r r0 = r3.b(r0)
            r1 = 2147483647(0x7fffffff, float:NaN)
            java.util.concurrent.TimeUnit r3 = java.util.concurrent.TimeUnit.MILLISECONDS
            b.a.i.b(r0, r1, r3)
            r0.close()
            int r0 = r4.b()
            switch(r0) {
                case 200: goto L_0x00aa;
                case 407: goto L_0x00ca;
                default: goto L_0x008d;
            }
        L_0x008d:
            java.io.IOException r0 = new java.io.IOException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Unexpected response code for CONNECT: "
            java.lang.StringBuilder r1 = r1.append(r2)
            int r2 = r4.b()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x00aa:
            c.e r0 = r8.d
            c.c r0 = r0.c()
            boolean r0 = r0.f()
            if (r0 == 0) goto L_0x00c2
            c.d r0 = r8.e
            c.c r0 = r0.c()
            boolean r0 = r0.f()
            if (r0 != 0) goto L_0x00e4
        L_0x00c2:
            java.io.IOException r0 = new java.io.IOException
            java.lang.String r1 = "TLS tunnel buffered too many bytes!"
            r0.<init>(r1)
            throw r0
        L_0x00ca:
            b.ab r0 = r8.i
            b.a r0 = r0.a()
            b.b r0 = r0.d()
            b.ab r1 = r8.i
            b.x r0 = r0.a(r1, r4)
            if (r0 != 0) goto L_0x0033
            java.io.IOException r0 = new java.io.IOException
            java.lang.String r1 = "Failed to authenticate with proxy"
            r0.<init>(r1)
            throw r0
        L_0x00e4:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: b.a.c.b.a(int, int):void");
    }

    private void a(int i2, int i3, int i4, a aVar) {
        this.j.setSoTimeout(i3);
        try {
            g.a().a(this.j, this.i.c(), i2);
            this.d = l.a(l.b(this.j));
            this.e = l.a(l.a(this.j));
            if (this.i.a().i() != null) {
                a(i3, i4, aVar);
            } else {
                this.l = v.HTTP_1_1;
                this.f415a = this.j;
            }
            if (this.l == v.SPDY_3 || this.l == v.HTTP_2) {
                this.f415a.setSoTimeout(0);
                d a2 = new d.a(true).a(this.f415a, this.i.a().a().f(), this.d, this.e).a(this.l).a();
                a2.d();
                this.f416b = a2;
            }
        } catch (ConnectException e2) {
            throw new ConnectException("Failed to connect to " + this.i.c());
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v0, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v1, resolved type: javax.net.ssl.SSLSocket} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v2, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v9, resolved type: javax.net.ssl.SSLSocket} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v4, resolved type: javax.net.ssl.SSLSocket} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v6, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v7, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v20, resolved type: javax.net.ssl.SSLSocket} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v21, resolved type: java.lang.String} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{javax.net.ssl.SSLSocketFactory.createSocket(java.net.Socket, java.lang.String, int, boolean):java.net.Socket throws java.io.IOException}
     arg types: [java.net.Socket, java.lang.String, int, int]
     candidates:
      ClspMth{javax.net.SocketFactory.createSocket(java.lang.String, int, java.net.InetAddress, int):java.net.Socket throws java.io.IOException, java.net.UnknownHostException}
      ClspMth{javax.net.SocketFactory.createSocket(java.net.InetAddress, int, java.net.InetAddress, int):java.net.Socket throws java.io.IOException}
      ClspMth{javax.net.ssl.SSLSocketFactory.createSocket(java.net.Socket, java.lang.String, int, boolean):java.net.Socket throws java.io.IOException} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(int r10, int r11, b.a.a r12) {
        /*
            r9 = this;
            r1 = 0
            b.ab r0 = r9.i
            boolean r0 = r0.d()
            if (r0 == 0) goto L_0x000c
            r9.a(r10, r11)
        L_0x000c:
            b.ab r0 = r9.i
            b.a r2 = r0.a()
            javax.net.ssl.SSLSocketFactory r0 = r2.i()
            java.net.Socket r3 = r9.j     // Catch:{ AssertionError -> 0x0143 }
            b.r r4 = r2.a()     // Catch:{ AssertionError -> 0x0143 }
            java.lang.String r4 = r4.f()     // Catch:{ AssertionError -> 0x0143 }
            b.r r5 = r2.a()     // Catch:{ AssertionError -> 0x0143 }
            int r5 = r5.g()     // Catch:{ AssertionError -> 0x0143 }
            r6 = 1
            java.net.Socket r0 = r0.createSocket(r3, r4, r5, r6)     // Catch:{ AssertionError -> 0x0143 }
            javax.net.ssl.SSLSocket r0 = (javax.net.ssl.SSLSocket) r0     // Catch:{ AssertionError -> 0x0143 }
            b.k r3 = r12.a(r0)     // Catch:{ AssertionError -> 0x00cd, all -> 0x013e }
            boolean r4 = r3.c()     // Catch:{ AssertionError -> 0x00cd, all -> 0x013e }
            if (r4 == 0) goto L_0x004c
            b.a.g r4 = b.a.g.a()     // Catch:{ AssertionError -> 0x00cd, all -> 0x013e }
            b.r r5 = r2.a()     // Catch:{ AssertionError -> 0x00cd, all -> 0x013e }
            java.lang.String r5 = r5.f()     // Catch:{ AssertionError -> 0x00cd, all -> 0x013e }
            java.util.List r6 = r2.e()     // Catch:{ AssertionError -> 0x00cd, all -> 0x013e }
            r4.a(r0, r5, r6)     // Catch:{ AssertionError -> 0x00cd, all -> 0x013e }
        L_0x004c:
            r0.startHandshake()     // Catch:{ AssertionError -> 0x00cd, all -> 0x013e }
            javax.net.ssl.SSLSession r4 = r0.getSession()     // Catch:{ AssertionError -> 0x00cd, all -> 0x013e }
            b.p r4 = b.p.a(r4)     // Catch:{ AssertionError -> 0x00cd, all -> 0x013e }
            javax.net.ssl.HostnameVerifier r5 = r2.j()     // Catch:{ AssertionError -> 0x00cd, all -> 0x013e }
            b.r r6 = r2.a()     // Catch:{ AssertionError -> 0x00cd, all -> 0x013e }
            java.lang.String r6 = r6.f()     // Catch:{ AssertionError -> 0x00cd, all -> 0x013e }
            javax.net.ssl.SSLSession r7 = r0.getSession()     // Catch:{ AssertionError -> 0x00cd, all -> 0x013e }
            boolean r5 = r5.verify(r6, r7)     // Catch:{ AssertionError -> 0x00cd, all -> 0x013e }
            if (r5 != 0) goto L_0x00eb
            java.util.List r1 = r4.b()     // Catch:{ AssertionError -> 0x00cd, all -> 0x013e }
            r3 = 0
            java.lang.Object r1 = r1.get(r3)     // Catch:{ AssertionError -> 0x00cd, all -> 0x013e }
            java.security.cert.X509Certificate r1 = (java.security.cert.X509Certificate) r1     // Catch:{ AssertionError -> 0x00cd, all -> 0x013e }
            javax.net.ssl.SSLPeerUnverifiedException r3 = new javax.net.ssl.SSLPeerUnverifiedException     // Catch:{ AssertionError -> 0x00cd, all -> 0x013e }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ AssertionError -> 0x00cd, all -> 0x013e }
            r4.<init>()     // Catch:{ AssertionError -> 0x00cd, all -> 0x013e }
            java.lang.String r5 = "Hostname "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ AssertionError -> 0x00cd, all -> 0x013e }
            b.r r2 = r2.a()     // Catch:{ AssertionError -> 0x00cd, all -> 0x013e }
            java.lang.String r2 = r2.f()     // Catch:{ AssertionError -> 0x00cd, all -> 0x013e }
            java.lang.StringBuilder r2 = r4.append(r2)     // Catch:{ AssertionError -> 0x00cd, all -> 0x013e }
            java.lang.String r4 = " not verified:"
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ AssertionError -> 0x00cd, all -> 0x013e }
            java.lang.String r4 = "\n    certificate: "
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ AssertionError -> 0x00cd, all -> 0x013e }
            java.lang.String r4 = b.g.a(r1)     // Catch:{ AssertionError -> 0x00cd, all -> 0x013e }
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ AssertionError -> 0x00cd, all -> 0x013e }
            java.lang.String r4 = "\n    DN: "
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ AssertionError -> 0x00cd, all -> 0x013e }
            java.security.Principal r4 = r1.getSubjectDN()     // Catch:{ AssertionError -> 0x00cd, all -> 0x013e }
            java.lang.String r4 = r4.getName()     // Catch:{ AssertionError -> 0x00cd, all -> 0x013e }
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ AssertionError -> 0x00cd, all -> 0x013e }
            java.lang.String r4 = "\n    subjectAltNames: "
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ AssertionError -> 0x00cd, all -> 0x013e }
            java.util.List r1 = b.a.d.b.a(r1)     // Catch:{ AssertionError -> 0x00cd, all -> 0x013e }
            java.lang.StringBuilder r1 = r2.append(r1)     // Catch:{ AssertionError -> 0x00cd, all -> 0x013e }
            java.lang.String r1 = r1.toString()     // Catch:{ AssertionError -> 0x00cd, all -> 0x013e }
            r3.<init>(r1)     // Catch:{ AssertionError -> 0x00cd, all -> 0x013e }
            throw r3     // Catch:{ AssertionError -> 0x00cd, all -> 0x013e }
        L_0x00cd:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
        L_0x00d1:
            boolean r2 = b.a.i.a(r0)     // Catch:{ all -> 0x00dd }
            if (r2 == 0) goto L_0x013d
            java.io.IOException r2 = new java.io.IOException     // Catch:{ all -> 0x00dd }
            r2.<init>(r0)     // Catch:{ all -> 0x00dd }
            throw r2     // Catch:{ all -> 0x00dd }
        L_0x00dd:
            r0 = move-exception
        L_0x00de:
            if (r1 == 0) goto L_0x00e7
            b.a.g r2 = b.a.g.a()
            r2.a(r1)
        L_0x00e7:
            b.a.i.a(r1)
            throw r0
        L_0x00eb:
            b.g r5 = r2.k()     // Catch:{ AssertionError -> 0x00cd, all -> 0x013e }
            b.r r2 = r2.a()     // Catch:{ AssertionError -> 0x00cd, all -> 0x013e }
            java.lang.String r2 = r2.f()     // Catch:{ AssertionError -> 0x00cd, all -> 0x013e }
            java.util.List r6 = r4.b()     // Catch:{ AssertionError -> 0x00cd, all -> 0x013e }
            r5.a(r2, r6)     // Catch:{ AssertionError -> 0x00cd, all -> 0x013e }
            boolean r2 = r3.c()     // Catch:{ AssertionError -> 0x00cd, all -> 0x013e }
            if (r2 == 0) goto L_0x010c
            b.a.g r1 = b.a.g.a()     // Catch:{ AssertionError -> 0x00cd, all -> 0x013e }
            java.lang.String r1 = r1.b(r0)     // Catch:{ AssertionError -> 0x00cd, all -> 0x013e }
        L_0x010c:
            r9.f415a = r0     // Catch:{ AssertionError -> 0x00cd, all -> 0x013e }
            java.net.Socket r2 = r9.f415a     // Catch:{ AssertionError -> 0x00cd, all -> 0x013e }
            c.r r2 = c.l.b(r2)     // Catch:{ AssertionError -> 0x00cd, all -> 0x013e }
            c.e r2 = c.l.a(r2)     // Catch:{ AssertionError -> 0x00cd, all -> 0x013e }
            r9.d = r2     // Catch:{ AssertionError -> 0x00cd, all -> 0x013e }
            java.net.Socket r2 = r9.f415a     // Catch:{ AssertionError -> 0x00cd, all -> 0x013e }
            c.q r2 = c.l.a(r2)     // Catch:{ AssertionError -> 0x00cd, all -> 0x013e }
            c.d r2 = c.l.a(r2)     // Catch:{ AssertionError -> 0x00cd, all -> 0x013e }
            r9.e = r2     // Catch:{ AssertionError -> 0x00cd, all -> 0x013e }
            r9.k = r4     // Catch:{ AssertionError -> 0x00cd, all -> 0x013e }
            if (r1 == 0) goto L_0x013a
            b.v r1 = b.v.a(r1)     // Catch:{ AssertionError -> 0x00cd, all -> 0x013e }
        L_0x012e:
            r9.l = r1     // Catch:{ AssertionError -> 0x00cd, all -> 0x013e }
            if (r0 == 0) goto L_0x0139
            b.a.g r1 = b.a.g.a()
            r1.a(r0)
        L_0x0139:
            return
        L_0x013a:
            b.v r1 = b.v.HTTP_1_1     // Catch:{ AssertionError -> 0x00cd, all -> 0x013e }
            goto L_0x012e
        L_0x013d:
            throw r0     // Catch:{ all -> 0x00dd }
        L_0x013e:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x00de
        L_0x0143:
            r0 = move-exception
            goto L_0x00d1
        */
        throw new UnsupportedOperationException("Method not decompiled: b.a.c.b.a(int, int, b.a.a):void");
    }

    private x f() {
        return new x.a().a(this.i.a().a()).a("Host", b.a.i.a(this.i.a().a())).a("Proxy-Connection", "Keep-Alive").a("User-Agent", j.a()).a();
    }

    public ab a() {
        return this.i;
    }

    /* JADX WARNING: CFG modification limit reached, blocks count: 135 */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x005e A[SYNTHETIC, Splitter:B:15:0x005e] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(int r8, int r9, int r10, java.util.List<b.k> r11, boolean r12) {
        /*
            r7 = this;
            r1 = 0
            b.v r0 = r7.l
            if (r0 == 0) goto L_0x000d
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "already connected"
            r0.<init>(r1)
            throw r0
        L_0x000d:
            b.a.a r3 = new b.a.a
            r3.<init>(r11)
            b.ab r0 = r7.i
            java.net.Proxy r4 = r0.b()
            b.ab r0 = r7.i
            b.a r5 = r0.a()
            b.ab r0 = r7.i
            b.a r0 = r0.a()
            javax.net.ssl.SSLSocketFactory r0 = r0.i()
            if (r0 != 0) goto L_0x00a3
            b.k r0 = b.k.f498c
            boolean r0 = r11.contains(r0)
            if (r0 != 0) goto L_0x00a3
            b.a.b.o r0 = new b.a.b.o
            java.net.UnknownServiceException r1 = new java.net.UnknownServiceException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "CLEARTEXT communication not supported: "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r11)
            java.lang.String r2 = r2.toString()
            r1.<init>(r2)
            r0.<init>(r1)
            throw r0
        L_0x0050:
            java.net.Socket r2 = new java.net.Socket     // Catch:{ IOException -> 0x0077 }
            r2.<init>(r4)     // Catch:{ IOException -> 0x0077 }
        L_0x0055:
            r7.j = r2     // Catch:{ IOException -> 0x0077 }
            r7.a(r8, r9, r10, r3)     // Catch:{ IOException -> 0x0077 }
        L_0x005a:
            b.v r2 = r7.l
            if (r2 != 0) goto L_0x00a2
            java.net.Proxy$Type r2 = r4.type()     // Catch:{ IOException -> 0x0077 }
            java.net.Proxy$Type r6 = java.net.Proxy.Type.DIRECT     // Catch:{ IOException -> 0x0077 }
            if (r2 == r6) goto L_0x006e
            java.net.Proxy$Type r2 = r4.type()     // Catch:{ IOException -> 0x0077 }
            java.net.Proxy$Type r6 = java.net.Proxy.Type.HTTP     // Catch:{ IOException -> 0x0077 }
            if (r2 != r6) goto L_0x0050
        L_0x006e:
            javax.net.SocketFactory r2 = r5.c()     // Catch:{ IOException -> 0x0077 }
            java.net.Socket r2 = r2.createSocket()     // Catch:{ IOException -> 0x0077 }
            goto L_0x0055
        L_0x0077:
            r2 = move-exception
            java.net.Socket r6 = r7.f415a
            b.a.i.a(r6)
            java.net.Socket r6 = r7.j
            b.a.i.a(r6)
            r7.f415a = r1
            r7.j = r1
            r7.d = r1
            r7.e = r1
            r7.k = r1
            r7.l = r1
            if (r0 != 0) goto L_0x009e
            b.a.b.o r0 = new b.a.b.o
            r0.<init>(r2)
        L_0x0095:
            if (r12 == 0) goto L_0x009d
            boolean r2 = r3.a(r2)
            if (r2 != 0) goto L_0x005a
        L_0x009d:
            throw r0
        L_0x009e:
            r0.a(r2)
            goto L_0x0095
        L_0x00a2:
            return
        L_0x00a3:
            r0 = r1
            goto L_0x005a
        */
        throw new UnsupportedOperationException("Method not decompiled: b.a.c.b.a(int, int, int, java.util.List, boolean):void");
    }

    public boolean a(boolean z) {
        int soTimeout;
        if (this.f415a.isClosed() || this.f415a.isInputShutdown() || this.f415a.isOutputShutdown()) {
            return false;
        }
        if (this.f416b != null || !z) {
            return true;
        }
        try {
            soTimeout = this.f415a.getSoTimeout();
            this.f415a.setSoTimeout(1);
            if (this.d.f()) {
                this.f415a.setSoTimeout(soTimeout);
                return false;
            }
            this.f415a.setSoTimeout(soTimeout);
            return true;
        } catch (SocketTimeoutException e2) {
            return true;
        } catch (IOException e3) {
            return false;
        } catch (Throwable th) {
            this.f415a.setSoTimeout(soTimeout);
            throw th;
        }
    }

    public void b() {
        b.a.i.a(this.j);
    }

    public Socket c() {
        return this.f415a;
    }

    public int d() {
        d dVar = this.f416b;
        if (dVar != null) {
            return dVar.b();
        }
        return 1;
    }

    public p e() {
        return this.k;
    }

    public String toString() {
        return "Connection{" + this.i.a().a().f() + ":" + this.i.a().a().g() + ", proxy=" + this.i.b() + " hostAddress=" + this.i.c() + " cipherSuite=" + (this.k != null ? this.k.a() : "none") + " protocol=" + this.l + '}';
    }
}
