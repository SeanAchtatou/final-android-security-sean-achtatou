package b.a;

import c.c;
import c.d;
import c.q;
import c.s;
import java.io.Closeable;
import java.io.File;
import java.io.Flushable;
import java.util.LinkedHashMap;
import java.util.concurrent.Executor;
import java.util.regex.Pattern;

public final class b implements Closeable, Flushable {

    /* renamed from: a  reason: collision with root package name */
    static final Pattern f351a = Pattern.compile("[a-z0-9_-]{1,120}");

    /* renamed from: b  reason: collision with root package name */
    static final /* synthetic */ boolean f352b = (!b.class.desiredAssertionStatus());
    private static final q o = new q() {
        public s a() {
            return s.f602b;
        }

        public void a_(c cVar, long j) {
            cVar.g(j);
        }

        public void close() {
        }

        public void flush() {
        }
    };

    /* renamed from: c  reason: collision with root package name */
    private final b.a.c.a f353c;
    private long d;
    private final int e;
    private long f;
    private d g;
    private final LinkedHashMap<String, C0005b> h;
    private int i;
    private boolean j;
    private boolean k;
    private long l;
    private final Executor m;
    private final Runnable n;

    public final class a {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ b f354a;
        /* access modifiers changed from: private */

        /* renamed from: b  reason: collision with root package name */
        public final C0005b f355b;
        /* access modifiers changed from: private */

        /* renamed from: c  reason: collision with root package name */
        public final boolean[] f356c;
        /* access modifiers changed from: private */
        public boolean d;

        public void a() {
            synchronized (this.f354a) {
                this.f354a.a(this, false);
            }
        }
    }

    /* renamed from: b.a.b$b  reason: collision with other inner class name */
    private final class C0005b {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public final String f357a;
        /* access modifiers changed from: private */

        /* renamed from: b  reason: collision with root package name */
        public final long[] f358b;
        /* access modifiers changed from: private */

        /* renamed from: c  reason: collision with root package name */
        public final File[] f359c;
        /* access modifiers changed from: private */
        public final File[] d;
        /* access modifiers changed from: private */
        public boolean e;
        /* access modifiers changed from: private */
        public a f;
        /* access modifiers changed from: private */
        public long g;

        /* access modifiers changed from: package-private */
        public void a(d dVar) {
            for (long l : this.f358b) {
                dVar.h(32).l(l);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: b.a.b.b.a(b.a.b$b, boolean):boolean
     arg types: [b.a.b$b, int]
     candidates:
      b.a.b.b.a(b.a.b$b, long):long
      b.a.b.b.a(b.a.b$b, b.a.b$a):b.a.b$a
      b.a.b.b.a(b.a.b$b, boolean):boolean */
    /* access modifiers changed from: private */
    public synchronized void a(a aVar, boolean z) {
        synchronized (this) {
            C0005b a2 = aVar.f355b;
            if (a2.f != aVar) {
                throw new IllegalStateException();
            }
            if (z) {
                if (!a2.e) {
                    int i2 = 0;
                    while (true) {
                        if (i2 < this.e) {
                            if (!aVar.f356c[i2]) {
                                aVar.a();
                                throw new IllegalStateException("Newly created entry didn't create value for index " + i2);
                            } else if (!this.f353c.b(a2.d[i2])) {
                                aVar.a();
                                break;
                            } else {
                                i2++;
                            }
                        }
                    }
                }
            }
            for (int i3 = 0; i3 < this.e; i3++) {
                File file = a2.d[i3];
                if (!z) {
                    this.f353c.a(file);
                } else if (this.f353c.b(file)) {
                    File file2 = a2.f359c[i3];
                    this.f353c.a(file, file2);
                    long j2 = a2.f358b[i3];
                    long c2 = this.f353c.c(file2);
                    a2.f358b[i3] = c2;
                    this.f = (this.f - j2) + c2;
                }
            }
            this.i++;
            a unused = a2.f = (a) null;
            if (a2.e || z) {
                boolean unused2 = a2.e = true;
                this.g.b("CLEAN").h(32);
                this.g.b(a2.f357a);
                a2.a(this.g);
                this.g.h(10);
                if (z) {
                    long j3 = this.l;
                    this.l = 1 + j3;
                    long unused3 = a2.g = j3;
                }
            } else {
                this.h.remove(a2.f357a);
                this.g.b("REMOVE").h(32);
                this.g.b(a2.f357a);
                this.g.h(10);
            }
            this.g.flush();
            if (this.f > this.d || b()) {
                this.m.execute(this.n);
            }
        }
    }

    private boolean a(C0005b bVar) {
        if (bVar.f != null) {
            boolean unused = bVar.f.d = true;
        }
        for (int i2 = 0; i2 < this.e; i2++) {
            this.f353c.a(bVar.f359c[i2]);
            this.f -= bVar.f358b[i2];
            bVar.f358b[i2] = 0;
        }
        this.i++;
        this.g.b("REMOVE").h(32).b(bVar.f357a).h(10);
        this.h.remove(bVar.f357a);
        if (b()) {
            this.m.execute(this.n);
        }
        return true;
    }

    private boolean b() {
        return this.i >= 2000 && this.i >= this.h.size();
    }

    private synchronized void c() {
        if (a()) {
            throw new IllegalStateException("cache is closed");
        }
    }

    private void d() {
        while (this.f > this.d) {
            a(this.h.values().iterator().next());
        }
    }

    public synchronized boolean a() {
        return this.k;
    }

    public synchronized void close() {
        if (!this.j || this.k) {
            this.k = true;
        } else {
            for (C0005b bVar : (C0005b[]) this.h.values().toArray(new C0005b[this.h.size()])) {
                if (bVar.f != null) {
                    bVar.f.a();
                }
            }
            d();
            this.g.close();
            this.g = null;
            this.k = true;
        }
    }

    public synchronized void flush() {
        if (this.j) {
            c();
            d();
            this.g.flush();
        }
    }
}
