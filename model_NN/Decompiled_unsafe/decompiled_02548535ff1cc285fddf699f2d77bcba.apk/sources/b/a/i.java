package b.a;

import b.r;
import c.c;
import c.f;
import java.io.Closeable;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.net.IDN;
import java.net.Socket;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

public final class i {

    /* renamed from: a  reason: collision with root package name */
    public static final byte[] f458a = new byte[0];

    /* renamed from: b  reason: collision with root package name */
    public static final String[] f459b = new String[0];

    /* renamed from: c  reason: collision with root package name */
    public static final Charset f460c = Charset.forName("UTF-8");
    public static final TimeZone d = TimeZone.getTimeZone("GMT");
    private static final Pattern e = Pattern.compile("([0-9a-fA-F]*:[0-9a-fA-F:.]*)|([\\d.]+)");

    /* JADX WARNING: Removed duplicated region for block: B:2:0x0003  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int a(java.lang.String r2, int r3, int r4) {
        /*
            r0 = r3
        L_0x0001:
            if (r0 >= r4) goto L_0x000b
            char r1 = r2.charAt(r0)
            switch(r1) {
                case 9: goto L_0x000c;
                case 10: goto L_0x000c;
                case 12: goto L_0x000c;
                case 13: goto L_0x000c;
                case 32: goto L_0x000c;
                default: goto L_0x000a;
            }
        L_0x000a:
            r4 = r0
        L_0x000b:
            return r4
        L_0x000c:
            int r0 = r0 + 1
            goto L_0x0001
        */
        throw new UnsupportedOperationException("Method not decompiled: b.a.i.a(java.lang.String, int, int):int");
    }

    public static int a(String str, int i, int i2, char c2) {
        for (int i3 = i; i3 < i2; i3++) {
            if (str.charAt(i3) == c2) {
                return i3;
            }
        }
        return i2;
    }

    public static int a(String str, int i, int i2, String str2) {
        for (int i3 = i; i3 < i2; i3++) {
            if (str2.indexOf(str.charAt(i3)) != -1) {
                return i3;
            }
        }
        return i2;
    }

    public static f a(f fVar) {
        try {
            return f.a(MessageDigest.getInstance("SHA-1").digest(fVar.g()));
        } catch (NoSuchAlgorithmException e2) {
            throw new AssertionError(e2);
        }
    }

    public static String a(r rVar) {
        return rVar.g() != r.a(rVar.b()) ? rVar.f() + ":" + rVar.g() : rVar.f();
    }

    public static String a(String str) {
        try {
            return f.a(MessageDigest.getInstance("SHA-1").digest(str.getBytes("UTF-8"))).b();
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException e2) {
            throw new AssertionError(e2);
        }
    }

    public static <T> List<T> a(List list) {
        return Collections.unmodifiableList(new ArrayList(list));
    }

    public static <T> List<T> a(Object... objArr) {
        return Collections.unmodifiableList(Arrays.asList((Object[]) objArr.clone()));
    }

    private static <T> List<T> a(Object[] objArr, Object[] objArr2) {
        ArrayList arrayList = new ArrayList();
        for (Object obj : objArr) {
            int length = objArr2.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    break;
                }
                Object obj2 = objArr2[i];
                if (obj.equals(obj2)) {
                    arrayList.add(obj2);
                    break;
                }
                i++;
            }
        }
        return arrayList;
    }

    public static <K, V> Map<K, V> a(Map map) {
        return Collections.unmodifiableMap(new LinkedHashMap(map));
    }

    public static ThreadFactory a(final String str, final boolean z) {
        return new ThreadFactory() {
            public Thread newThread(Runnable runnable) {
                Thread thread = new Thread(runnable, str);
                thread.setDaemon(z);
                return thread;
            }
        };
    }

    public static void a(long j, long j2, long j3) {
        if ((j2 | j3) < 0 || j2 > j || j - j2 < j3) {
            throw new ArrayIndexOutOfBoundsException();
        }
    }

    public static void a(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (RuntimeException e2) {
                throw e2;
            } catch (Exception e3) {
            }
        }
    }

    public static void a(Closeable closeable, Closeable closeable2) {
        Throwable th = null;
        try {
            closeable.close();
        } catch (Throwable th2) {
            th = th2;
        }
        try {
            closeable2.close();
        } catch (Throwable th3) {
            if (th == null) {
                th = th3;
            }
        }
        if (th != null) {
            if (th instanceof IOException) {
                throw ((IOException) th);
            } else if (th instanceof RuntimeException) {
                throw ((RuntimeException) th);
            } else if (th instanceof Error) {
                throw ((Error) th);
            } else {
                throw new AssertionError(th);
            }
        }
    }

    public static void a(Socket socket) {
        if (socket != null) {
            try {
                socket.close();
            } catch (AssertionError e2) {
                if (!a(e2)) {
                    throw e2;
                }
            } catch (RuntimeException e3) {
                throw e3;
            } catch (Exception e4) {
            }
        }
    }

    public static boolean a(c.r rVar, int i, TimeUnit timeUnit) {
        try {
            return b(rVar, i, timeUnit);
        } catch (IOException e2) {
            return false;
        }
    }

    public static boolean a(AssertionError assertionError) {
        return (assertionError.getCause() == null || assertionError.getMessage() == null || !assertionError.getMessage().contains("getsockname failed")) ? false : true;
    }

    public static boolean a(Object obj, Object obj2) {
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }

    public static boolean a(String[] strArr, String str) {
        return Arrays.asList(strArr).contains(str);
    }

    public static <T> T[] a(Class cls, Object[] objArr, Object[] objArr2) {
        List a2 = a(objArr, objArr2);
        return a2.toArray((Object[]) Array.newInstance(cls, a2.size()));
    }

    /* JADX WARNING: Removed duplicated region for block: B:2:0x0004  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int b(java.lang.String r2, int r3, int r4) {
        /*
            int r0 = r4 + -1
        L_0x0002:
            if (r0 < r3) goto L_0x000d
            char r1 = r2.charAt(r0)
            switch(r1) {
                case 9: goto L_0x000e;
                case 10: goto L_0x000e;
                case 12: goto L_0x000e;
                case 13: goto L_0x000e;
                case 32: goto L_0x000e;
                default: goto L_0x000b;
            }
        L_0x000b:
            int r3 = r0 + 1
        L_0x000d:
            return r3
        L_0x000e:
            int r0 = r0 + -1
            goto L_0x0002
        */
        throw new UnsupportedOperationException("Method not decompiled: b.a.i.b(java.lang.String, int, int):int");
    }

    public static String b(String str) {
        try {
            String lowerCase = IDN.toASCII(str).toLowerCase(Locale.US);
            if (!lowerCase.isEmpty() && !d(lowerCase)) {
                return lowerCase;
            }
            return null;
        } catch (IllegalArgumentException e2) {
            return null;
        }
    }

    public static boolean b(c.r rVar, int i, TimeUnit timeUnit) {
        long nanoTime = System.nanoTime();
        long d2 = rVar.a().c_() ? rVar.a().d() - nanoTime : Long.MAX_VALUE;
        rVar.a().a(Math.min(d2, timeUnit.toNanos((long) i)) + nanoTime);
        try {
            c cVar = new c();
            while (rVar.a(cVar, 2048) != -1) {
                cVar.s();
            }
            if (d2 == Long.MAX_VALUE) {
                rVar.a().f();
            } else {
                rVar.a().a(d2 + nanoTime);
            }
            return true;
        } catch (InterruptedIOException e2) {
            if (d2 == Long.MAX_VALUE) {
                rVar.a().f();
            } else {
                rVar.a().a(d2 + nanoTime);
            }
            return false;
        } catch (Throwable th) {
            if (d2 == Long.MAX_VALUE) {
                rVar.a().f();
            } else {
                rVar.a().a(d2 + nanoTime);
            }
            throw th;
        }
    }

    public static String[] b(String[] strArr, String str) {
        String[] strArr2 = new String[(strArr.length + 1)];
        System.arraycopy(strArr, 0, strArr2, 0, strArr.length);
        strArr2[strArr2.length - 1] = str;
        return strArr2;
    }

    public static String c(String str, int i, int i2) {
        int a2 = a(str, i, i2);
        return str.substring(a2, b(str, a2, i2));
    }

    public static boolean c(String str) {
        return e.matcher(str).matches();
    }

    private static boolean d(String str) {
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            if (charAt <= 31 || charAt >= 127) {
                return true;
            }
            if (" #%/:?@[\\]".indexOf(charAt) != -1) {
                return true;
            }
        }
        return false;
    }
}
