package b.a;

import b.ab;
import java.util.LinkedHashSet;
import java.util.Set;

public final class h {

    /* renamed from: a  reason: collision with root package name */
    private final Set<ab> f457a = new LinkedHashSet();

    public synchronized void a(ab abVar) {
        this.f457a.add(abVar);
    }

    public synchronized void b(ab abVar) {
        this.f457a.remove(abVar);
    }

    public synchronized boolean c(ab abVar) {
        return this.f457a.contains(abVar);
    }
}
