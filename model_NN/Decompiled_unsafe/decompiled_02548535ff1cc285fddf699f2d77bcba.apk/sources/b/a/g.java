package b.a;

import b.v;
import java.io.IOException;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import javax.net.ssl.SSLSocket;

public class g {

    /* renamed from: a  reason: collision with root package name */
    private static final g f447a = c();

    private static class a extends g {

        /* renamed from: a  reason: collision with root package name */
        private final f<Socket> f448a;

        /* renamed from: b  reason: collision with root package name */
        private final f<Socket> f449b;

        /* renamed from: c  reason: collision with root package name */
        private final Method f450c;
        private final Method d;
        private final f<Socket> e;
        private final f<Socket> f;

        public a(f<Socket> fVar, f<Socket> fVar2, Method method, Method method2, f<Socket> fVar3, f<Socket> fVar4) {
            this.f448a = fVar;
            this.f449b = fVar2;
            this.f450c = method;
            this.d = method2;
            this.e = fVar3;
            this.f = fVar4;
        }

        public void a(Socket socket, InetSocketAddress inetSocketAddress, int i) {
            try {
                socket.connect(inetSocketAddress, i);
            } catch (AssertionError e2) {
                if (i.a(e2)) {
                    throw new IOException(e2);
                }
                throw e2;
            } catch (SecurityException e3) {
                IOException iOException = new IOException("Exception in connect");
                iOException.initCause(e3);
                throw iOException;
            }
        }

        public void a(SSLSocket sSLSocket, String str, List<v> list) {
            if (str != null) {
                this.f448a.b(sSLSocket, true);
                this.f449b.b(sSLSocket, str);
            }
            if (this.f != null && this.f.a(sSLSocket)) {
                this.f.d(sSLSocket, a(list));
            }
        }

        public String b(SSLSocket sSLSocket) {
            if (this.e == null || !this.e.a(sSLSocket)) {
                return null;
            }
            byte[] bArr = (byte[]) this.e.d(sSLSocket, new Object[0]);
            return bArr != null ? new String(bArr, i.f460c) : null;
        }
    }

    private static class b extends g {

        /* renamed from: a  reason: collision with root package name */
        private final Method f451a;

        /* renamed from: b  reason: collision with root package name */
        private final Method f452b;

        /* renamed from: c  reason: collision with root package name */
        private final Method f453c;
        private final Class<?> d;
        private final Class<?> e;

        public b(Method method, Method method2, Method method3, Class<?> cls, Class<?> cls2) {
            this.f451a = method;
            this.f452b = method2;
            this.f453c = method3;
            this.d = cls;
            this.e = cls2;
        }

        public void a(SSLSocket sSLSocket) {
            try {
                this.f453c.invoke(null, sSLSocket);
            } catch (IllegalAccessException | InvocationTargetException e2) {
                throw new AssertionError();
            }
        }

        public void a(SSLSocket sSLSocket, String str, List<v> list) {
            ArrayList arrayList = new ArrayList(list.size());
            int size = list.size();
            for (int i = 0; i < size; i++) {
                v vVar = list.get(i);
                if (vVar != v.HTTP_1_0) {
                    arrayList.add(vVar.toString());
                }
            }
            try {
                this.f451a.invoke(null, sSLSocket, Proxy.newProxyInstance(g.class.getClassLoader(), new Class[]{this.d, this.e}, new c(arrayList)));
            } catch (IllegalAccessException | InvocationTargetException e2) {
                throw new AssertionError(e2);
            }
        }

        public String b(SSLSocket sSLSocket) {
            try {
                c cVar = (c) Proxy.getInvocationHandler(this.f452b.invoke(null, sSLSocket));
                if (cVar.f455b || cVar.f456c != null) {
                    return cVar.f455b ? null : cVar.f456c;
                }
                c.f412a.log(Level.INFO, "ALPN callback dropped: SPDY and HTTP/2 are disabled. Is alpn-boot on the boot class path?");
                return null;
            } catch (IllegalAccessException | InvocationTargetException e2) {
                throw new AssertionError();
            }
        }
    }

    private static class c implements InvocationHandler {

        /* renamed from: a  reason: collision with root package name */
        private final List<String> f454a;
        /* access modifiers changed from: private */

        /* renamed from: b  reason: collision with root package name */
        public boolean f455b;
        /* access modifiers changed from: private */

        /* renamed from: c  reason: collision with root package name */
        public String f456c;

        public c(List<String> list) {
            this.f454a = list;
        }

        public Object invoke(Object obj, Method method, Object[] objArr) {
            String name = method.getName();
            Class<?> returnType = method.getReturnType();
            if (objArr == null) {
                objArr = i.f459b;
            }
            if (name.equals("supports") && Boolean.TYPE == returnType) {
                return true;
            }
            if (name.equals("unsupported") && Void.TYPE == returnType) {
                this.f455b = true;
                return null;
            } else if (name.equals("protocols") && objArr.length == 0) {
                return this.f454a;
            } else {
                if ((name.equals("selectProtocol") || name.equals("select")) && String.class == returnType && objArr.length == 1 && (objArr[0] instanceof List)) {
                    List list = (List) objArr[0];
                    int size = list.size();
                    for (int i = 0; i < size; i++) {
                        if (this.f454a.contains(list.get(i))) {
                            String str = (String) list.get(i);
                            this.f456c = str;
                            return str;
                        }
                    }
                    String str2 = this.f454a.get(0);
                    this.f456c = str2;
                    return str2;
                } else if ((!name.equals("protocolSelected") && !name.equals("selected")) || objArr.length != 1) {
                    return method.invoke(this, objArr);
                } else {
                    this.f456c = (String) objArr[0];
                    return null;
                }
            }
        }
    }

    public static g a() {
        return f447a;
    }

    static byte[] a(List<v> list) {
        c.c cVar = new c.c();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            v vVar = list.get(i);
            if (vVar != v.HTTP_1_0) {
                cVar.h(vVar.toString().length());
                cVar.b(vVar.toString());
            }
        }
        return cVar.r();
    }

    private static g c() {
        f fVar;
        Method method;
        Method method2;
        f fVar2;
        Method method3;
        f fVar3;
        Method method4;
        Method method5;
        f fVar4;
        f fVar5;
        f fVar6;
        f fVar7;
        try {
            Class.forName("com.android.org.conscrypt.OpenSSLSocketImpl");
        } catch (ClassNotFoundException e) {
            Class.forName("org.apache.harmony.xnet.provider.jsse.OpenSSLSocketImpl");
        }
        try {
            f fVar8 = new f(null, "setUseSessionTickets", Boolean.TYPE);
            f fVar9 = new f(null, "setHostname", String.class);
            try {
                Class<?> cls = Class.forName("android.net.TrafficStats");
                Method method6 = cls.getMethod("tagSocket", Socket.class);
                try {
                    method5 = cls.getMethod("untagSocket", Socket.class);
                    method = method5;
                    fVar3 = fVar5;
                    method3 = method6;
                    fVar2 = fVar6;
                } catch (ClassNotFoundException e2) {
                    method4 = method6;
                    method = null;
                    method2 = method4;
                    fVar = null;
                    fVar2 = null;
                    method3 = method2;
                    fVar3 = fVar;
                    return new a(fVar8, fVar9, method3, method, fVar3, fVar2);
                } catch (NoSuchMethodException e3) {
                    fVar = null;
                    method = null;
                    method2 = method6;
                    fVar2 = null;
                    method3 = method2;
                    fVar3 = fVar;
                    return new a(fVar8, fVar9, method3, method, fVar3, fVar2);
                }
                try {
                    Class.forName("android.net.Network");
                    fVar7 = new f(byte[].class, "getAlpnSelectedProtocol", new Class[0]);
                } catch (ClassNotFoundException e4) {
                    fVar4 = null;
                    fVar5 = fVar4;
                    fVar6 = null;
                    method = method5;
                    fVar3 = fVar5;
                    method3 = method6;
                    fVar2 = fVar6;
                    return new a(fVar8, fVar9, method3, method, fVar3, fVar2);
                } catch (NoSuchMethodException e5) {
                    fVar = null;
                    method = method5;
                    method2 = method6;
                    fVar2 = null;
                    method3 = method2;
                    fVar3 = fVar;
                    return new a(fVar8, fVar9, method3, method, fVar3, fVar2);
                }
                try {
                    fVar6 = new f(null, "setAlpnProtocols", byte[].class);
                    fVar5 = fVar7;
                } catch (ClassNotFoundException e6) {
                    fVar4 = fVar7;
                    fVar5 = fVar4;
                    fVar6 = null;
                    method = method5;
                    fVar3 = fVar5;
                    method3 = method6;
                    fVar2 = fVar6;
                    return new a(fVar8, fVar9, method3, method, fVar3, fVar2);
                } catch (NoSuchMethodException e7) {
                    fVar = fVar7;
                    method = method5;
                    method2 = method6;
                    fVar2 = null;
                    method3 = method2;
                    fVar3 = fVar;
                    return new a(fVar8, fVar9, method3, method, fVar3, fVar2);
                }
            } catch (ClassNotFoundException e8) {
                method4 = null;
                method = null;
                method2 = method4;
                fVar = null;
                fVar2 = null;
                method3 = method2;
                fVar3 = fVar;
                return new a(fVar8, fVar9, method3, method, fVar3, fVar2);
            } catch (NoSuchMethodException e9) {
                fVar = null;
                method = null;
                method2 = null;
                fVar2 = null;
                method3 = method2;
                fVar3 = fVar;
                return new a(fVar8, fVar9, method3, method, fVar3, fVar2);
            }
            return new a(fVar8, fVar9, method3, method, fVar3, fVar2);
        } catch (ClassNotFoundException e10) {
            try {
                Class<?> cls2 = Class.forName("org.eclipse.jetty.alpn.ALPN");
                Class<?> cls3 = Class.forName("org.eclipse.jetty.alpn.ALPN" + "$Provider");
                return new b(cls2.getMethod("put", SSLSocket.class, cls3), cls2.getMethod("get", SSLSocket.class), cls2.getMethod("remove", SSLSocket.class), Class.forName("org.eclipse.jetty.alpn.ALPN" + "$ClientProvider"), Class.forName("org.eclipse.jetty.alpn.ALPN" + "$ServerProvider"));
            } catch (ClassNotFoundException | NoSuchMethodException e11) {
                return new g();
            }
        }
    }

    public void a(Socket socket, InetSocketAddress inetSocketAddress, int i) {
        socket.connect(inetSocketAddress, i);
    }

    public void a(SSLSocket sSLSocket) {
    }

    public void a(SSLSocket sSLSocket, String str, List<v> list) {
    }

    public String b() {
        return "OkHttp";
    }

    public String b(SSLSocket sSLSocket) {
        return null;
    }
}
