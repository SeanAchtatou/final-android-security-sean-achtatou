package b;

import b.a.i;
import b.r;
import java.net.Proxy;
import java.net.ProxySelector;
import java.util.List;
import javax.net.SocketFactory;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSocketFactory;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    final r f253a;

    /* renamed from: b  reason: collision with root package name */
    final o f254b;

    /* renamed from: c  reason: collision with root package name */
    final SocketFactory f255c;
    final b d;
    final List<v> e;
    final List<k> f;
    final ProxySelector g;
    final Proxy h;
    final SSLSocketFactory i;
    final HostnameVerifier j;
    final g k;

    public a(String str, int i2, o oVar, SocketFactory socketFactory, SSLSocketFactory sSLSocketFactory, HostnameVerifier hostnameVerifier, g gVar, b bVar, Proxy proxy, List<v> list, List<k> list2, ProxySelector proxySelector) {
        this.f253a = new r.a().a(sSLSocketFactory != null ? "https" : "http").b(str).a(i2).c();
        if (oVar == null) {
            throw new IllegalArgumentException("dns == null");
        }
        this.f254b = oVar;
        if (socketFactory == null) {
            throw new IllegalArgumentException("socketFactory == null");
        }
        this.f255c = socketFactory;
        if (bVar == null) {
            throw new IllegalArgumentException("proxyAuthenticator == null");
        }
        this.d = bVar;
        if (list == null) {
            throw new IllegalArgumentException("protocols == null");
        }
        this.e = i.a(list);
        if (list2 == null) {
            throw new IllegalArgumentException("connectionSpecs == null");
        }
        this.f = i.a(list2);
        if (proxySelector == null) {
            throw new IllegalArgumentException("proxySelector == null");
        }
        this.g = proxySelector;
        this.h = proxy;
        this.i = sSLSocketFactory;
        this.j = hostnameVerifier;
        this.k = gVar;
    }

    public r a() {
        return this.f253a;
    }

    public o b() {
        return this.f254b;
    }

    public SocketFactory c() {
        return this.f255c;
    }

    public b d() {
        return this.d;
    }

    public List<v> e() {
        return this.e;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof a)) {
            return false;
        }
        a aVar = (a) obj;
        return this.f253a.equals(aVar.f253a) && this.f254b.equals(aVar.f254b) && this.d.equals(aVar.d) && this.e.equals(aVar.e) && this.f.equals(aVar.f) && this.g.equals(aVar.g) && i.a(this.h, aVar.h) && i.a(this.i, aVar.i) && i.a(this.j, aVar.j) && i.a(this.k, aVar.k);
    }

    public List<k> f() {
        return this.f;
    }

    public ProxySelector g() {
        return this.g;
    }

    public Proxy h() {
        return this.h;
    }

    public int hashCode() {
        int i2 = 0;
        int hashCode = ((this.j != null ? this.j.hashCode() : 0) + (((this.i != null ? this.i.hashCode() : 0) + (((this.h != null ? this.h.hashCode() : 0) + ((((((((((((this.f253a.hashCode() + 527) * 31) + this.f254b.hashCode()) * 31) + this.d.hashCode()) * 31) + this.e.hashCode()) * 31) + this.f.hashCode()) * 31) + this.g.hashCode()) * 31)) * 31)) * 31)) * 31;
        if (this.k != null) {
            i2 = this.k.hashCode();
        }
        return hashCode + i2;
    }

    public SSLSocketFactory i() {
        return this.i;
    }

    public HostnameVerifier j() {
        return this.j;
    }

    public g k() {
        return this.k;
    }
}
