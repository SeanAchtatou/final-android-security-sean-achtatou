package b.b;

import b.a.b.r;
import b.a.c;
import b.a.i;
import b.e;
import b.u;
import b.v;
import b.x;
import b.z;
import c.f;
import java.io.IOException;
import java.net.ProtocolException;
import java.security.SecureRandom;
import java.util.Collections;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public final class b {

    /* renamed from: a  reason: collision with root package name */
    private final e f472a;

    /* renamed from: b  reason: collision with root package name */
    private final Random f473b;

    /* renamed from: c  reason: collision with root package name */
    private final String f474c;

    private static class a extends b.a.e.a {

        /* renamed from: c  reason: collision with root package name */
        private final r f477c;
        private final ExecutorService d;

        private a(r rVar, Random random, ExecutorService executorService, c cVar, String str) {
            super(true, rVar.b().d, rVar.b().e, random, executorService, cVar, str);
            this.f477c = rVar;
            this.d = executorService;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: b.a.i.a(java.lang.String, boolean):java.util.concurrent.ThreadFactory
         arg types: [java.lang.String, int]
         candidates:
          b.a.i.a(java.lang.Object[], java.lang.Object[]):java.util.List<T>
          b.a.i.a(java.io.Closeable, java.io.Closeable):void
          b.a.i.a(java.lang.Object, java.lang.Object):boolean
          b.a.i.a(java.lang.String[], java.lang.String):boolean
          b.a.i.a(java.lang.String, boolean):java.util.concurrent.ThreadFactory */
        static b.a.e.a a(r rVar, z zVar, Random random, c cVar) {
            String rVar2 = zVar.a().a().toString();
            ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(1, 1, 1, TimeUnit.SECONDS, new LinkedBlockingDeque(), i.a(String.format("OkHttp %s WebSocket", rVar2), true));
            threadPoolExecutor.allowCoreThreadTimeOut(true);
            return new a(rVar, random, threadPoolExecutor, cVar, rVar2);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: b.a.b.r.a(boolean, b.a.b.i):void
         arg types: [int, b.a.b.i]
         candidates:
          b.a.b.r.a(java.io.IOException, c.q):boolean
          b.a.b.r.a(boolean, b.a.b.i):void */
        /* access modifiers changed from: protected */
        public void b() {
            this.d.shutdown();
            this.f477c.d();
            this.f477c.a(true, this.f477c.a());
        }
    }

    b(u uVar, x xVar) {
        this(uVar, xVar, new SecureRandom());
    }

    b(u uVar, x xVar, Random random) {
        if (!"GET".equals(xVar.b())) {
            throw new IllegalArgumentException("Request must be GET: " + xVar.b());
        }
        this.f473b = random;
        byte[] bArr = new byte[16];
        random.nextBytes(bArr);
        this.f474c = f.a(bArr).b();
        this.f472a = uVar.x().a(Collections.singletonList(v.HTTP_1_1)).a().a(xVar.e().a("Upgrade", "websocket").a("Connection", "Upgrade").a("Sec-WebSocket-Key", this.f474c).a("Sec-WebSocket-Version", "13").a());
    }

    public static b a(u uVar, x xVar) {
        return new b(uVar, xVar);
    }

    /* access modifiers changed from: private */
    public void a(z zVar, c cVar) {
        if (zVar.b() != 101) {
            i.a(zVar.f());
            throw new ProtocolException("Expected HTTP 101 response but was '" + zVar.b() + " " + zVar.c() + "'");
        }
        String a2 = zVar.a("Connection");
        if (!"Upgrade".equalsIgnoreCase(a2)) {
            throw new ProtocolException("Expected 'Connection' header value 'Upgrade' but was '" + a2 + "'");
        }
        String a3 = zVar.a("Upgrade");
        if (!"websocket".equalsIgnoreCase(a3)) {
            throw new ProtocolException("Expected 'Upgrade' header value 'websocket' but was '" + a3 + "'");
        }
        String a4 = zVar.a("Sec-WebSocket-Accept");
        String a5 = i.a(this.f474c + "258EAFA5-E914-47DA-95CA-C5AB0DC85B11");
        if (!a5.equals(a4)) {
            throw new ProtocolException("Expected 'Sec-WebSocket-Accept' header value '" + a5 + "' but was '" + a4 + "'");
        }
        b.a.e.a a6 = a.a(c.f413b.a(this.f472a), zVar, this.f473b, cVar);
        cVar.a(a6, zVar);
        do {
        } while (a6.a());
    }

    public void a() {
        this.f472a.a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: b.a.c.a(b.e, b.f, boolean):void
     arg types: [b.e, b.b.b$1, int]
     candidates:
      b.a.c.a(b.j, b.a, b.a.b.r):b.a.c.b
      b.a.c.a(b.k, javax.net.ssl.SSLSocket, boolean):void
      b.a.c.a(b.e, b.f, boolean):void */
    public void a(final c cVar) {
        c.f413b.a(this.f472a, (b.f) new b.f() {
            public void a(e eVar, z zVar) {
                try {
                    b.this.a(zVar, cVar);
                } catch (IOException e) {
                    cVar.a(e, zVar);
                }
            }

            public void a(e eVar, IOException iOException) {
                cVar.a(iOException, (z) null);
            }
        }, true);
    }
}
