package b;

import b.a.i;
import c.e;
import java.io.Closeable;
import java.io.IOException;
import java.nio.charset.Charset;

public abstract class aa implements Closeable {
    private Charset f() {
        t a2 = a();
        return a2 != null ? a2.a(i.f460c) : i.f460c;
    }

    public abstract t a();

    public abstract long b();

    public abstract e c();

    public void close() {
        i.a(c());
    }

    /* JADX INFO: finally extract failed */
    public final byte[] d() {
        long b2 = b();
        if (b2 > 2147483647L) {
            throw new IOException("Cannot buffer entire body for content length: " + b2);
        }
        e c2 = c();
        try {
            byte[] r = c2.r();
            i.a(c2);
            if (b2 == -1 || b2 == ((long) r.length)) {
                return r;
            }
            throw new IOException("Content-Length and stream length disagree");
        } catch (Throwable th) {
            i.a(c2);
            throw th;
        }
    }

    public final String e() {
        return new String(d(), f().name());
    }
}
