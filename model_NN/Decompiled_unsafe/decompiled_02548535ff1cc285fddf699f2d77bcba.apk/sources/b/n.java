package b;

import b.a.i;
import b.w;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Iterator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public final class n {

    /* renamed from: a  reason: collision with root package name */
    private int f506a = 64;

    /* renamed from: b  reason: collision with root package name */
    private int f507b = 5;

    /* renamed from: c  reason: collision with root package name */
    private ExecutorService f508c;
    private final Deque<w.b> d = new ArrayDeque();
    private final Deque<w.b> e = new ArrayDeque();
    private final Deque<w> f = new ArrayDeque();

    private void b() {
        if (this.e.size() < this.f506a && !this.d.isEmpty()) {
            Iterator<w.b> it = this.d.iterator();
            while (it.hasNext()) {
                w.b next = it.next();
                if (c(next) < this.f507b) {
                    it.remove();
                    this.e.add(next);
                    a().execute(next);
                }
                if (this.e.size() >= this.f506a) {
                    return;
                }
            }
        }
    }

    private int c(w.b bVar) {
        int i = 0;
        Iterator<w.b> it = this.e.iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                return i2;
            }
            i = it.next().a().equals(bVar.a()) ? i2 + 1 : i2;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: b.a.i.a(java.lang.String, boolean):java.util.concurrent.ThreadFactory
     arg types: [java.lang.String, int]
     candidates:
      b.a.i.a(java.lang.Object[], java.lang.Object[]):java.util.List<T>
      b.a.i.a(java.io.Closeable, java.io.Closeable):void
      b.a.i.a(java.lang.Object, java.lang.Object):boolean
      b.a.i.a(java.lang.String[], java.lang.String):boolean
      b.a.i.a(java.lang.String, boolean):java.util.concurrent.ThreadFactory */
    public synchronized ExecutorService a() {
        if (this.f508c == null) {
            this.f508c = new ThreadPoolExecutor(0, Integer.MAX_VALUE, 60, TimeUnit.SECONDS, new SynchronousQueue(), i.a("OkHttp Dispatcher", false));
        }
        return this.f508c;
    }

    /* access modifiers changed from: package-private */
    public synchronized void a(w.b bVar) {
        if (this.e.size() >= this.f506a || c(bVar) >= this.f507b) {
            this.d.add(bVar);
        } else {
            this.e.add(bVar);
            a().execute(bVar);
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void b(w.b bVar) {
        if (!this.e.remove(bVar)) {
            throw new AssertionError("AsyncCall wasn't running!");
        }
        b();
    }
}
