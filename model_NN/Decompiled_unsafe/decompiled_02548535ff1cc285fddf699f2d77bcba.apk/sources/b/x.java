package b;

import b.a.b.h;
import b.q;

public final class x {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final r f545a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public final String f546b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public final q f547c;
    /* access modifiers changed from: private */
    public final y d;
    /* access modifiers changed from: private */
    public final Object e;
    private volatile d f;

    public static class a {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public r f548a;
        /* access modifiers changed from: private */

        /* renamed from: b  reason: collision with root package name */
        public String f549b;
        /* access modifiers changed from: private */

        /* renamed from: c  reason: collision with root package name */
        public q.a f550c;
        /* access modifiers changed from: private */
        public y d;
        /* access modifiers changed from: private */
        public Object e;

        public a() {
            this.f549b = "GET";
            this.f550c = new q.a();
        }

        private a(x xVar) {
            this.f548a = xVar.f545a;
            this.f549b = xVar.f546b;
            this.d = xVar.d;
            this.e = xVar.e;
            this.f550c = xVar.f547c.b();
        }

        public a a(r rVar) {
            if (rVar == null) {
                throw new IllegalArgumentException("url == null");
            }
            this.f548a = rVar;
            return this;
        }

        public a a(String str) {
            if (str == null) {
                throw new IllegalArgumentException("url == null");
            }
            if (str.regionMatches(true, 0, "ws:", 0, 3)) {
                str = "http:" + str.substring(3);
            } else if (str.regionMatches(true, 0, "wss:", 0, 4)) {
                str = "https:" + str.substring(4);
            }
            r e2 = r.e(str);
            if (e2 != null) {
                return a(e2);
            }
            throw new IllegalArgumentException("unexpected url: " + str);
        }

        public a a(String str, y yVar) {
            if (str == null || str.length() == 0) {
                throw new IllegalArgumentException("method == null || method.length() == 0");
            } else if (yVar != null && !h.c(str)) {
                throw new IllegalArgumentException("method " + str + " must not have a request body.");
            } else if (yVar != null || !h.b(str)) {
                this.f549b = str;
                this.d = yVar;
                return this;
            } else {
                throw new IllegalArgumentException("method " + str + " must have a request body.");
            }
        }

        public a a(String str, String str2) {
            this.f550c.c(str, str2);
            return this;
        }

        public x a() {
            if (this.f548a != null) {
                return new x(this);
            }
            throw new IllegalStateException("url == null");
        }

        public a b(String str) {
            this.f550c.b(str);
            return this;
        }

        public a b(String str, String str2) {
            this.f550c.a(str, str2);
            return this;
        }
    }

    private x(a aVar) {
        this.f545a = aVar.f548a;
        this.f546b = aVar.f549b;
        this.f547c = aVar.f550c.a();
        this.d = aVar.d;
        this.e = aVar.e != null ? aVar.e : this;
    }

    public r a() {
        return this.f545a;
    }

    public String a(String str) {
        return this.f547c.a(str);
    }

    public String b() {
        return this.f546b;
    }

    public q c() {
        return this.f547c;
    }

    public y d() {
        return this.d;
    }

    public a e() {
        return new a();
    }

    public d f() {
        d dVar = this.f;
        if (dVar != null) {
            return dVar;
        }
        d a2 = d.a(this.f547c);
        this.f = a2;
        return a2;
    }

    public boolean g() {
        return this.f545a.c();
    }

    public String toString() {
        return "Request{method=" + this.f546b + ", url=" + this.f545a + ", tag=" + (this.e != this ? this.e : null) + '}';
    }
}
