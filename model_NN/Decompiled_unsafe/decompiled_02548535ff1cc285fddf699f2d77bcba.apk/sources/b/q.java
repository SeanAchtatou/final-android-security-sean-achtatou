package b;

import b.a.b.f;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public final class q {

    /* renamed from: a  reason: collision with root package name */
    private final String[] f513a;

    public static final class a {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public final List<String> f514a = new ArrayList(20);

        private void d(String str, String str2) {
            if (str == null) {
                throw new IllegalArgumentException("name == null");
            } else if (str.isEmpty()) {
                throw new IllegalArgumentException("name is empty");
            } else {
                int length = str.length();
                for (int i = 0; i < length; i++) {
                    char charAt = str.charAt(i);
                    if (charAt <= 31 || charAt >= 127) {
                        throw new IllegalArgumentException(String.format("Unexpected char %#04x at %d in header name: %s", Integer.valueOf(charAt), Integer.valueOf(i), str));
                    }
                }
                if (str2 == null) {
                    throw new IllegalArgumentException("value == null");
                }
                int length2 = str2.length();
                for (int i2 = 0; i2 < length2; i2++) {
                    char charAt2 = str2.charAt(i2);
                    if (charAt2 <= 31 || charAt2 >= 127) {
                        throw new IllegalArgumentException(String.format("Unexpected char %#04x at %d in %s value: %s", Integer.valueOf(charAt2), Integer.valueOf(i2), str, str2));
                    }
                }
            }
        }

        /* access modifiers changed from: package-private */
        public a a(String str) {
            int indexOf = str.indexOf(":", 1);
            return indexOf != -1 ? b(str.substring(0, indexOf), str.substring(indexOf + 1)) : str.startsWith(":") ? b("", str.substring(1)) : b("", str);
        }

        public a a(String str, String str2) {
            d(str, str2);
            return b(str, str2);
        }

        public q a() {
            return new q(this);
        }

        public a b(String str) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= this.f514a.size()) {
                    return this;
                }
                if (str.equalsIgnoreCase(this.f514a.get(i2))) {
                    this.f514a.remove(i2);
                    this.f514a.remove(i2);
                    i2 -= 2;
                }
                i = i2 + 2;
            }
        }

        /* access modifiers changed from: package-private */
        public a b(String str, String str2) {
            this.f514a.add(str);
            this.f514a.add(str2.trim());
            return this;
        }

        public a c(String str, String str2) {
            d(str, str2);
            b(str);
            b(str, str2);
            return this;
        }
    }

    private q(a aVar) {
        this.f513a = (String[]) aVar.f514a.toArray(new String[aVar.f514a.size()]);
    }

    private static String a(String[] strArr, String str) {
        for (int length = strArr.length - 2; length >= 0; length -= 2) {
            if (str.equalsIgnoreCase(strArr[length])) {
                return strArr[length + 1];
            }
        }
        return null;
    }

    public int a() {
        return this.f513a.length / 2;
    }

    public String a(int i) {
        return this.f513a[i * 2];
    }

    public String a(String str) {
        return a(this.f513a, str);
    }

    public a b() {
        a aVar = new a();
        Collections.addAll(aVar.f514a, this.f513a);
        return aVar;
    }

    public String b(int i) {
        return this.f513a[(i * 2) + 1];
    }

    public Date b(String str) {
        String a2 = a(str);
        if (a2 != null) {
            return f.a(a2);
        }
        return null;
    }

    public List<String> c(String str) {
        int a2 = a();
        ArrayList arrayList = null;
        for (int i = 0; i < a2; i++) {
            if (str.equalsIgnoreCase(a(i))) {
                if (arrayList == null) {
                    arrayList = new ArrayList(2);
                }
                arrayList.add(b(i));
            }
        }
        return arrayList != null ? Collections.unmodifiableList(arrayList) : Collections.emptyList();
    }

    public Map<String, List<String>> c() {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        int a2 = a();
        for (int i = 0; i < a2; i++) {
            String a3 = a(i);
            Object obj = (List) linkedHashMap.get(a3);
            if (obj == null) {
                obj = new ArrayList(2);
                linkedHashMap.put(a3, obj);
            }
            obj.add(b(i));
        }
        return linkedHashMap;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        int a2 = a();
        for (int i = 0; i < a2; i++) {
            sb.append(a(i)).append(": ").append(b(i)).append("\n");
        }
        return sb.toString();
    }
}
