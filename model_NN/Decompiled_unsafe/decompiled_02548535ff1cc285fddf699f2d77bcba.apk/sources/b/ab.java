package b;

import java.net.InetSocketAddress;
import java.net.Proxy;

public final class ab {

    /* renamed from: a  reason: collision with root package name */
    final a f463a;

    /* renamed from: b  reason: collision with root package name */
    final Proxy f464b;

    /* renamed from: c  reason: collision with root package name */
    final InetSocketAddress f465c;

    public ab(a aVar, Proxy proxy, InetSocketAddress inetSocketAddress) {
        if (aVar == null) {
            throw new NullPointerException("address == null");
        } else if (proxy == null) {
            throw new NullPointerException("proxy == null");
        } else if (inetSocketAddress == null) {
            throw new NullPointerException("inetSocketAddress == null");
        } else {
            this.f463a = aVar;
            this.f464b = proxy;
            this.f465c = inetSocketAddress;
        }
    }

    public a a() {
        return this.f463a;
    }

    public Proxy b() {
        return this.f464b;
    }

    public InetSocketAddress c() {
        return this.f465c;
    }

    public boolean d() {
        return this.f463a.i != null && this.f464b.type() == Proxy.Type.HTTP;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof ab)) {
            return false;
        }
        ab abVar = (ab) obj;
        return this.f463a.equals(abVar.f463a) && this.f464b.equals(abVar.f464b) && this.f465c.equals(abVar.f465c);
    }

    public int hashCode() {
        return ((((this.f463a.hashCode() + 527) * 31) + this.f464b.hashCode()) * 31) + this.f465c.hashCode();
    }
}
