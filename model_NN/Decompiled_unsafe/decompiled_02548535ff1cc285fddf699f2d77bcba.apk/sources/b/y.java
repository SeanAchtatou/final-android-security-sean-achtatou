package b;

import b.a.i;
import c.d;
import java.nio.charset.Charset;

public abstract class y {
    public static y a(t tVar, String str) {
        Charset charset = i.f460c;
        if (tVar != null && (charset = tVar.c()) == null) {
            charset = i.f460c;
            tVar = t.a(tVar + "; charset=utf-8");
        }
        return a(tVar, str.getBytes(charset));
    }

    public static y a(t tVar, byte[] bArr) {
        return a(tVar, bArr, 0, bArr.length);
    }

    public static y a(final t tVar, final byte[] bArr, final int i, final int i2) {
        if (bArr == null) {
            throw new NullPointerException("content == null");
        }
        i.a((long) bArr.length, (long) i, (long) i2);
        return new y() {
            public t a() {
                return tVar;
            }

            public void a(d dVar) {
                dVar.c(bArr, i, i2);
            }

            public long b() {
                return (long) i2;
            }
        };
    }

    public abstract t a();

    public abstract void a(d dVar);

    public long b() {
        return -1;
    }
}
