package b;

import b.a.b.g;
import b.a.b.l;
import b.a.b.o;
import b.a.b.r;
import b.a.c;
import b.a.e;
import b.s;
import b.x;
import c.q;
import java.io.IOException;
import java.net.ProtocolException;
import java.util.logging.Level;

final class w implements e {

    /* renamed from: a  reason: collision with root package name */
    volatile boolean f537a;

    /* renamed from: b  reason: collision with root package name */
    x f538b;

    /* renamed from: c  reason: collision with root package name */
    g f539c;
    /* access modifiers changed from: private */
    public final u d;
    private boolean e;

    class a implements s.a {

        /* renamed from: b  reason: collision with root package name */
        private final int f541b;

        /* renamed from: c  reason: collision with root package name */
        private final x f542c;
        private final boolean d;

        a(int i, x xVar, boolean z) {
            this.f541b = i;
            this.f542c = xVar;
            this.d = z;
        }

        public z a(x xVar) {
            if (this.f541b >= w.this.d.v().size()) {
                return w.this.a(xVar, this.d);
            }
            a aVar = new a(this.f541b + 1, xVar, this.d);
            s sVar = w.this.d.v().get(this.f541b);
            z a2 = sVar.a(aVar);
            if (a2 != null) {
                return a2;
            }
            throw new NullPointerException("application interceptor " + sVar + " returned null");
        }
    }

    final class b extends e {

        /* renamed from: c  reason: collision with root package name */
        private final f f544c;
        private final boolean d;

        private b(f fVar, boolean z) {
            super("OkHttp %s", w.this.f538b.a().toString());
            this.f544c = fVar;
            this.d = z;
        }

        /* access modifiers changed from: package-private */
        public String a() {
            return w.this.f538b.a().f();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
         arg types: [java.util.logging.Level, java.lang.String, java.io.IOException]
         candidates:
          ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
          ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
          ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
          ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
        /* access modifiers changed from: protected */
        public void b() {
            boolean z = true;
            try {
                z a2 = w.this.a(this.d);
                if (w.this.f537a) {
                    try {
                        this.f544c.a(w.this, new IOException("Canceled"));
                    } catch (IOException e) {
                        e = e;
                    }
                } else {
                    this.f544c.a(w.this, a2);
                }
                w.this.d.s().b(this);
            } catch (IOException e2) {
                e = e2;
                z = false;
                if (z) {
                    try {
                        c.f412a.log(Level.INFO, "Callback failure for " + w.this.b(), (Throwable) e);
                    } catch (Throwable th) {
                        w.this.d.s().b(this);
                        throw th;
                    }
                } else {
                    this.f544c.a(w.this, e);
                }
                w.this.d.s().b(this);
            }
        }
    }

    protected w(u uVar, x xVar) {
        this.d = uVar;
        this.f538b = xVar;
    }

    /* access modifiers changed from: private */
    public z a(boolean z) {
        return new a(0, this.f538b, z).a(this.f538b);
    }

    /* access modifiers changed from: private */
    public String b() {
        return (this.f537a ? "canceled call" : "call") + " to " + this.f538b.a().c("/...");
    }

    /* access modifiers changed from: package-private */
    public z a(x xVar, boolean z) {
        x xVar2;
        boolean z2;
        y d2 = xVar.d();
        if (d2 != null) {
            x.a e2 = xVar.e();
            t a2 = d2.a();
            if (a2 != null) {
                e2.a("Content-Type", a2.toString());
            }
            long b2 = d2.b();
            if (b2 != -1) {
                e2.a("Content-Length", Long.toString(b2));
                e2.b("Transfer-Encoding");
            } else {
                e2.a("Transfer-Encoding", "chunked");
                e2.b("Content-Length");
            }
            xVar2 = e2.a();
        } else {
            xVar2 = xVar;
        }
        this.f539c = new g(this.d, xVar2, false, false, z, null, null, null);
        int i = 0;
        while (!this.f537a) {
            try {
                this.f539c.a();
                this.f539c.h();
                z c2 = this.f539c.c();
                x i2 = this.f539c.i();
                if (i2 == null) {
                    if (!z) {
                        this.f539c.e();
                    }
                    return c2;
                }
                r g = this.f539c.g();
                int i3 = i + 1;
                if (i3 > 20) {
                    g.c();
                    throw new ProtocolException("Too many follow-up requests: " + i3);
                }
                if (!this.f539c.a(i2.a())) {
                    g.c();
                    g = null;
                }
                this.f539c = new g(this.d, i2, false, false, z, g, null, c2);
                i = i3;
            } catch (l e3) {
                throw e3.getCause();
            } catch (o e4) {
                g a3 = this.f539c.a(e4.a(), (q) null);
                if (a3 != null) {
                    z2 = false;
                    this.f539c = a3;
                } else {
                    throw e4.a();
                }
            } catch (IOException e5) {
                g a4 = this.f539c.a(e5, (q) null);
                if (a4 != null) {
                    z2 = false;
                    this.f539c = a4;
                } else {
                    throw e5;
                }
            } catch (Throwable th) {
                th = th;
            }
        }
        this.f539c.e();
        throw new IOException("Canceled");
        if (z2) {
            this.f539c.g().c();
        }
        throw th;
    }

    public void a() {
        this.f537a = true;
        if (this.f539c != null) {
            this.f539c.f();
        }
    }

    /* access modifiers changed from: package-private */
    public void a(f fVar, boolean z) {
        synchronized (this) {
            if (this.e) {
                throw new IllegalStateException("Already Executed");
            }
            this.e = true;
        }
        this.d.s().a(new b(fVar, z));
    }
}
