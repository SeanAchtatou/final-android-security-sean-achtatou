package b;

import java.nio.charset.Charset;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class t {

    /* renamed from: a  reason: collision with root package name */
    private static final Pattern f525a = Pattern.compile("([a-zA-Z0-9-!#$%&'*+.^_`{|}~]+)/([a-zA-Z0-9-!#$%&'*+.^_`{|}~]+)");

    /* renamed from: b  reason: collision with root package name */
    private static final Pattern f526b = Pattern.compile(";\\s*(?:([a-zA-Z0-9-!#$%&'*+.^_`{|}~]+)=(?:([a-zA-Z0-9-!#$%&'*+.^_`{|}~]+)|\"([^\"]*)\"))?");

    /* renamed from: c  reason: collision with root package name */
    private final String f527c;
    private final String d;
    private final String e;
    private final String f;

    private t(String str, String str2, String str3, String str4) {
        this.f527c = str;
        this.d = str2;
        this.e = str3;
        this.f = str4;
    }

    public static t a(String str) {
        Matcher matcher = f525a.matcher(str);
        if (!matcher.lookingAt()) {
            return null;
        }
        String lowerCase = matcher.group(1).toLowerCase(Locale.US);
        String lowerCase2 = matcher.group(2).toLowerCase(Locale.US);
        Matcher matcher2 = f526b.matcher(str);
        String str2 = null;
        for (int end = matcher.end(); end < str.length(); end = matcher2.end()) {
            matcher2.region(end, str.length());
            if (!matcher2.lookingAt()) {
                return null;
            }
            String group = matcher2.group(1);
            if (group != null && group.equalsIgnoreCase("charset")) {
                String group2 = matcher2.group(2) != null ? matcher2.group(2) : matcher2.group(3);
                if (str2 == null || group2.equalsIgnoreCase(str2)) {
                    str2 = group2;
                } else {
                    throw new IllegalArgumentException("Multiple different charsets: " + str);
                }
            }
        }
        return new t(str, lowerCase, lowerCase2, str2);
    }

    public String a() {
        return this.d;
    }

    public Charset a(Charset charset) {
        return this.f != null ? Charset.forName(this.f) : charset;
    }

    public String b() {
        return this.e;
    }

    public Charset c() {
        if (this.f != null) {
            return Charset.forName(this.f);
        }
        return null;
    }

    public boolean equals(Object obj) {
        return (obj instanceof t) && ((t) obj).f527c.equals(this.f527c);
    }

    public int hashCode() {
        return this.f527c.hashCode();
    }

    public String toString() {
        return this.f527c;
    }
}
