package ru.jdhndtpk.iypwqbmltdwdk.c;

import android.content.SharedPreferences;
import android.os.Build;
import java.util.ArrayList;
import java.util.List;
import ru.jdhndtpk.iypwqbmltdwdk.YOytzTerr;
import ru.jdhndtpk.iypwqbmltdwdk.a;

public class b {

    /* renamed from: a  reason: collision with root package name */
    public static final String f648a = a.a("oXIwwdxfEhBbaGEZsWmdEoZ");

    /* renamed from: b  reason: collision with root package name */
    public static final String f649b = a.a("UhsskHbfIpAaLZOFQzjuGAz");

    /* renamed from: c  reason: collision with root package name */
    public static final String f650c = a.a("dpTqRYrOmXNMGQrgybhXvPlgOsA");
    public static final String d = a.a("irRODETOjuyGEkGPRN");
    public static final String e = a.a("YStlRHBmKEUuhTJxFMNTrJaPtGS");
    public static final String f = a.a("fVZohfVuuwqxBrQJLcAXEa");
    public static final String g = a.a("hisJqNBnBScwWePCGkVdOoObVUkF");
    public static final String h = a.a("zZpxHWShgXtoCwlPhA");
    public static final String[] i = new String[5];
    public static final List<String> j = new ArrayList();
    public static final b k = new b();
    public static volatile boolean l;
    private static SharedPreferences m;
    private boolean n = m.getBoolean(f648a, false);
    private boolean o = m.getBoolean(f649b, true);
    private boolean p = m.getBoolean(d, false);
    private boolean q = m.getBoolean(h, false);
    private int r;
    private long s;
    private long t;

    static {
        j.add(a.a("shgyDzpPKWKLCxYqlZEISu"));
        j.add(a.a("nNwwXbucJOvcQXtXnOLazaOem"));
        j.add(a.a("vJwxGhCMSJezVDBzOyURQQdf"));
        j.add(a.a("nxOftepkNvIoXvdkAX"));
        j.add(a.a("emuytlIRpGqcmUpiZNYkkUcCrLZswm"));
        j.add(a.a("uWusfeQSAiOiSGyVsjaKp"));
        j.add(a.a("xYhzQfNvXJjmfuUUw"));
        j.add(a.a("TDUFxezSiweDQaIWyIGOgVOwuXPqvf"));
        j.add(a.a("FScFGKjhDZBYfUIhgtsbok"));
        j.add(a.a("URWnSruylLnPfleEOAeXdiewXA"));
        j.add(a.a("UorhsvmETxUIKLypxZaApTk"));
        j.add(a.a("xhlwCDdjOObl"));
        j.add(a.a("LavjJtNNzDx"));
        if (Build.VERSION.SDK_INT >= 23) {
            i[0] = a.a("azYDjMXCnoQ");
            i[1] = a.a("nTPSjXhiHWwKgDXhRdxTabMkvp");
            i[2] = a.a("nPqCaQJWZyDEOXVr");
            i[3] = a.a("HNctFNtcEmKVkghPzrQTpxcKOUag");
            i[4] = a.a("gPwHzWsmYQNqbmYRpvW");
        }
    }

    private b() {
        m = YOytzTerr.a().getSharedPreferences(a.a("UmoNwlsZjOy"), 0);
        l = m.getBoolean(g, false);
        this.r = m.getInt(f650c, 30);
        this.s = m.getLong(e, 0);
        this.t = m.getLong(f, 0);
    }

    private static void a(String str, boolean z) {
        m.edit().putBoolean(str, z).apply();
    }

    public static void a(boolean z) {
        k.p = z;
        a(d, true);
    }

    public static boolean a() {
        return k.o;
    }

    public static boolean b() {
        return k.n;
    }

    public static boolean c() {
        return k.p;
    }

    public static void d() {
        k.n = true;
        a(f648a, true);
    }
}
