package ru.jdhndtpk.iypwqbmltdwdk.b;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import java.util.ArrayList;

public class c extends a {

    /* renamed from: b  reason: collision with root package name */
    public static final String f638b = ru.jdhndtpk.iypwqbmltdwdk.a.a("PBRtOQVGuamReCbbXt");

    /* renamed from: c  reason: collision with root package name */
    public static final String f639c = ru.jdhndtpk.iypwqbmltdwdk.a.a("qWXHzdCfYDeDPJCgT");
    public static final String d = ru.jdhndtpk.iypwqbmltdwdk.a.a("LFPburEjfggoYKpIohVW");
    public static final String e = ru.jdhndtpk.iypwqbmltdwdk.a.a("WLeVKWupvryKDz");
    public static final String f = ru.jdhndtpk.iypwqbmltdwdk.a.a("AznOwCXMvMuJzBOa");

    public class a {

        /* renamed from: a  reason: collision with root package name */
        public long f640a;

        /* renamed from: b  reason: collision with root package name */
        public int f641b;

        /* renamed from: c  reason: collision with root package name */
        public int f642c;
        public String d;

        public a(long j, int i, int i2, String str) {
            this.f640a = j;
            this.f641b = i;
            this.f642c = i2;
            this.d = str;
        }
    }

    public c(Context context) {
        super(context);
    }

    public static String a() {
        return ((((((ru.jdhndtpk.iypwqbmltdwdk.a.a("yqRnxPPEIbwfdSRURUGmyySADcAcPB") + f638b) + ru.jdhndtpk.iypwqbmltdwdk.a.a("gNBPKigYljMiGqfWHMelUUhwh")) + f639c + ru.jdhndtpk.iypwqbmltdwdk.a.a("XsbagSEDsZPOrfkcZaYfgPLnWnokbM")) + d + ru.jdhndtpk.iypwqbmltdwdk.a.a("lcwRKQxNdGdjNzuEHxDAzwnrYr")) + e + ru.jdhndtpk.iypwqbmltdwdk.a.a("lcwRKQxNdGdjNzuEHxDAzwnrYr")) + f + ru.jdhndtpk.iypwqbmltdwdk.a.a("OnwrsFiIbvYUE")) + ru.jdhndtpk.iypwqbmltdwdk.a.a("vnOLSsLgljLCkdMYpUhMoyZoKp");
    }

    public a a(int i, int i2, String str) {
        a(i);
        SQLiteDatabase writableDatabase = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(d, Integer.valueOf(i));
        contentValues.put(e, Integer.valueOf(i2));
        contentValues.put(f, str);
        long insert = writableDatabase.insert(f638b, null, contentValues);
        writableDatabase.close();
        return new a(insert, i, i2, str);
    }

    public void a(int i) {
        SQLiteDatabase writableDatabase = getWritableDatabase();
        writableDatabase.delete(f638b, d + ru.jdhndtpk.iypwqbmltdwdk.a.a("hmkOCvMUrxhNBmQLpveTiqUV"), new String[]{String.valueOf(i)});
        writableDatabase.close();
    }

    public void a(long j) {
        SQLiteDatabase writableDatabase = getWritableDatabase();
        writableDatabase.delete(f638b, f639c + ru.jdhndtpk.iypwqbmltdwdk.a.a("hmkOCvMUrxhNBmQLpveTiqUV"), new String[]{String.valueOf(j)});
        writableDatabase.close();
    }

    public ArrayList<a> b() {
        SQLiteDatabase readableDatabase = getReadableDatabase();
        Cursor query = readableDatabase.query(f638b, new String[]{f639c, d, e, f}, null, null, null, null, null);
        ArrayList<a> arrayList = new ArrayList<>();
        if (query.moveToFirst()) {
            do {
                arrayList.add(new a(query.getLong(query.getColumnIndex(f639c)), query.getInt(query.getColumnIndex(d)), query.getInt(query.getColumnIndex(e)), query.getString(query.getColumnIndex(f))));
            } while (query.moveToNext());
        }
        query.close();
        readableDatabase.close();
        return arrayList;
    }
}
