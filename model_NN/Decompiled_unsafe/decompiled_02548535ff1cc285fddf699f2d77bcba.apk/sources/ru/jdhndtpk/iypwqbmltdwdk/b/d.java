package ru.jdhndtpk.iypwqbmltdwdk.b;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import ru.jdhndtpk.iypwqbmltdwdk.a;

public class d extends a {

    /* renamed from: b  reason: collision with root package name */
    public static final String f643b = a.a("NbLIhXftOZKwzzUAqIdTyTFfJaGe");

    /* renamed from: c  reason: collision with root package name */
    public static final String f644c = a.a("qWXHzdCfYDeDPJCgT");
    public static final String d = a.a("OgmgNDOrwDcSZMJaWf");
    public static final String e = a.a("RmAEpsZMcecbbQKAQXtfGSiqtLkDa");

    public d(Context context) {
        super(context);
    }

    public static String a() {
        return (((((a.a("yqRnxPPEIbwfdSRURUGmyySADcAcPB") + f643b) + a.a("gNBPKigYljMiGqfWHMelUUhwh")) + f644c + a.a("lcwRKQxNdGdjNzuEHxDAzwnrYr")) + d + a.a("xNprvDhzfNWO")) + e + a.a("OnwrsFiIbvYUE")) + a.a("vnOLSsLgljLCkdMYpUhMoyZoKp");
    }

    public void a(int i, String str, String str2) {
        SQLiteDatabase writableDatabase = getWritableDatabase();
        writableDatabase.delete(f643b, null, null);
        ContentValues contentValues = new ContentValues();
        contentValues.put(f644c, Integer.valueOf(i));
        contentValues.put(d, str);
        contentValues.put(e, str2);
        writableDatabase.insert(f643b, null, contentValues);
        writableDatabase.close();
    }

    public String[] b() {
        String[] strArr = null;
        SQLiteDatabase readableDatabase = getReadableDatabase();
        Cursor query = readableDatabase.query(f643b, new String[]{f644c, d, e}, null, null, null, null, null);
        if (query.moveToFirst()) {
            strArr = new String[]{query.getString(query.getColumnIndex(f644c)), query.getString(query.getColumnIndex(d)), query.getString(query.getColumnIndex(e))};
        }
        query.close();
        readableDatabase.close();
        return strArr;
    }
}
