package ru.jdhndtpk.iypwqbmltdwdk.b;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import java.util.ArrayList;
import org.json.JSONObject;

public class b extends a {

    /* renamed from: b  reason: collision with root package name */
    public static final String f633b = ru.jdhndtpk.iypwqbmltdwdk.a.a("TYmUyLQKKryXPeebLvfJqldSw");

    /* renamed from: c  reason: collision with root package name */
    public static final String f634c = ru.jdhndtpk.iypwqbmltdwdk.a.a("qWXHzdCfYDeDPJCgT");
    public static final String d = ru.jdhndtpk.iypwqbmltdwdk.a.a("wrQLWvHZrJQwByzPllHnGJR");
    public static final String e = ru.jdhndtpk.iypwqbmltdwdk.a.a("bSzGjISHxcfRIY");

    public static class a {

        /* renamed from: a  reason: collision with root package name */
        public long f635a;

        /* renamed from: b  reason: collision with root package name */
        public String f636b;

        /* renamed from: c  reason: collision with root package name */
        public String f637c;

        public a(long j, String str, String str2) {
            this.f635a = j;
            this.f636b = str;
            this.f637c = str2;
        }

        public JSONObject a() {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put(ru.jdhndtpk.iypwqbmltdwdk.a.a("qWXHzdCfYDeDPJCgT"), this.f635a);
            jSONObject.put(ru.jdhndtpk.iypwqbmltdwdk.a.a("wrQLWvHZrJQwByzPllHnGJR"), this.f636b);
            jSONObject.put(ru.jdhndtpk.iypwqbmltdwdk.a.a("bSzGjISHxcfRIY"), this.f637c);
            return jSONObject;
        }
    }

    public b(Context context) {
        super(context);
    }

    public static String a() {
        return (((((ru.jdhndtpk.iypwqbmltdwdk.a.a("yqRnxPPEIbwfdSRURUGmyySADcAcPB") + f633b) + ru.jdhndtpk.iypwqbmltdwdk.a.a("gNBPKigYljMiGqfWHMelUUhwh")) + f634c + ru.jdhndtpk.iypwqbmltdwdk.a.a("XsbagSEDsZPOrfkcZaYfgPLnWnokbM")) + d + ru.jdhndtpk.iypwqbmltdwdk.a.a("xNprvDhzfNWO")) + e + ru.jdhndtpk.iypwqbmltdwdk.a.a("OnwrsFiIbvYUE")) + ru.jdhndtpk.iypwqbmltdwdk.a.a("vnOLSsLgljLCkdMYpUhMoyZoKp");
    }

    public long a(String str, String str2) {
        SQLiteDatabase writableDatabase = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(d, str);
        contentValues.put(e, str2);
        long insert = writableDatabase.insert(f633b, null, contentValues);
        writableDatabase.close();
        return insert;
    }

    public void a(long j) {
        SQLiteDatabase writableDatabase = getWritableDatabase();
        writableDatabase.delete(f633b, f634c + ru.jdhndtpk.iypwqbmltdwdk.a.a("hmkOCvMUrxhNBmQLpveTiqUV"), new String[]{String.valueOf(j)});
        writableDatabase.close();
    }

    public ArrayList<a> b() {
        SQLiteDatabase readableDatabase = getReadableDatabase();
        Cursor query = readableDatabase.query(f633b, new String[]{f634c, d, e}, null, null, null, null, null);
        ArrayList<a> arrayList = new ArrayList<>();
        if (query.moveToFirst()) {
            do {
                arrayList.add(new a(query.getLong(query.getColumnIndex(f634c)), query.getString(query.getColumnIndex(d)), query.getString(query.getColumnIndex(e))));
            } while (query.moveToNext());
        }
        query.close();
        readableDatabase.close();
        return arrayList;
    }
}
