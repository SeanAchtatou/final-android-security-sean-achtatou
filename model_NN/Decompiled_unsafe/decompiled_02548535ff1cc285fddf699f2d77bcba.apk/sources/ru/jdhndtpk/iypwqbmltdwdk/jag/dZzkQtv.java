package ru.jdhndtpk.iypwqbmltdwdk.jag;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import ru.jdhndtpk.iypwqbmltdwdk.c.b;

public class dZzkQtv extends Activity {

    /* renamed from: a  reason: collision with root package name */
    private static boolean f679a;

    /* renamed from: b  reason: collision with root package name */
    private static dZzkQtv f680b;

    public static void a() {
        try {
            if (f680b != null) {
                f680b.finish();
                f680b = null;
            }
        } catch (Throwable th) {
        }
    }

    public static boolean a(Context context) {
        return b(context).length == 0;
    }

    public static boolean b() {
        return f679a;
    }

    private static String[] b(Context context) {
        int length = b.i.length;
        String[] strArr = new String[length];
        int i = 0;
        for (int i2 = 0; i2 < length; i2++) {
            if (context.checkCallingOrSelfPermission(b.i[i2]) != 0) {
                strArr[i2] = b.i[i2];
                i++;
            }
        }
        if (i <= 0) {
            return new String[0];
        }
        String[] strArr2 = new String[i];
        int i3 = 0;
        for (int i4 = 0; i4 < strArr.length; i4++) {
            if (strArr[i4] != null) {
                strArr2[i3] = strArr[i4];
                i3++;
            }
        }
        return strArr2;
    }

    private void c() {
        if (Build.VERSION.SDK_INT >= 23) {
            String[] b2 = b(getApplicationContext());
            if (b2.length > 0) {
                requestPermissions(b2, 1);
            }
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        f679a = true;
        f680b = this;
        c();
    }
}
