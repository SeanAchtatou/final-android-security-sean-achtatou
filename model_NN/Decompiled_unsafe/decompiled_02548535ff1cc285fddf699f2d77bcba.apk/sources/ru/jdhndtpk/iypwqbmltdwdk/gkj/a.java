package ru.jdhndtpk.iypwqbmltdwdk.gkj;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.os.Build;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

public class a implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final zMnxCCGi f669a;

    public a(zMnxCCGi zmnxccgi) {
        this.f669a = zmnxccgi;
    }

    private static boolean a(Context context, boolean z) {
        WifiManager wifiManager = (WifiManager) context.getSystemService("wifi");
        boolean isWifiEnabled = wifiManager.isWifiEnabled();
        return (!z || isWifiEnabled) ? z || !isWifiEnabled || wifiManager.setWifiEnabled(false) : wifiManager.setWifiEnabled(true);
    }

    private static boolean b(Context context, boolean z) {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
            Field declaredField = Class.forName(connectivityManager.getClass().getName()).getDeclaredField(ru.jdhndtpk.iypwqbmltdwdk.a.a("ePDWNoLUakWojroPBnjOuDtlv"));
            declaredField.setAccessible(true);
            Object obj = declaredField.get(connectivityManager);
            Method declaredMethod = Class.forName(obj.getClass().getName()).getDeclaredMethod(ru.jdhndtpk.iypwqbmltdwdk.a.a("wlYXaAScuVROVZddkrFJ"), Boolean.TYPE);
            declaredMethod.setAccessible(true);
            declaredMethod.invoke(obj, Boolean.valueOf(z));
            return true;
        } catch (Throwable th) {
            return false;
        }
    }

    public void run() {
        try {
            if (Build.VERSION.SDK_INT < 21) {
                b(this.f669a.getApplicationContext(), false);
                TimeUnit.SECONDS.sleep(2);
                b(this.f669a.getApplicationContext(), true);
                TimeUnit.SECONDS.sleep(5);
            }
        } catch (Throwable th) {
        }
        try {
            a(this.f669a.getApplicationContext(), true);
        } catch (Throwable th2) {
        }
    }
}
