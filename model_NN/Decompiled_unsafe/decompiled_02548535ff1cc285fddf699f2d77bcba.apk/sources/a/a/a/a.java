package a.a.a;

import java.math.BigDecimal;
import java.math.BigInteger;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private long f0a = 100;

    /* renamed from: b  reason: collision with root package name */
    private long f1b = 10000;

    /* renamed from: c  reason: collision with root package name */
    private int f2c = 2;
    private double d;
    private int e;

    public long a() {
        BigInteger valueOf = BigInteger.valueOf(this.f0a);
        BigInteger valueOf2 = BigInteger.valueOf((long) this.f2c);
        int i = this.e;
        this.e = i + 1;
        BigInteger multiply = valueOf.multiply(valueOf2.pow(i));
        if (this.d != 0.0d) {
            double random = Math.random();
            BigInteger bigInteger = BigDecimal.valueOf(random).multiply(BigDecimal.valueOf(this.d)).multiply(new BigDecimal(multiply)).toBigInteger();
            multiply = (((int) Math.floor(random * 10.0d)) & 1) == 0 ? multiply.subtract(bigInteger) : multiply.add(bigInteger);
        }
        return multiply.min(BigInteger.valueOf(this.f1b)).longValue();
    }

    public a a(double d2) {
        this.d = d2;
        return this;
    }

    public a a(long j) {
        this.f0a = j;
        return this;
    }

    public a b(long j) {
        this.f1b = j;
        return this;
    }

    public void b() {
        this.e = 0;
    }

    public int c() {
        return this.e;
    }
}
