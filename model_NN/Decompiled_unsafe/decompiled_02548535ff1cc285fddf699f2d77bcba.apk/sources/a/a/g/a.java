package a.a.g;

import java.util.HashMap;
import java.util.Map;

public class a {
    public static String a(Map<String, String> map) {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry next : map.entrySet()) {
            if (sb.length() > 0) {
                sb.append("&");
            }
            sb.append(a.a.e.a.a((String) next.getKey())).append("=").append(a.a.e.a.a((String) next.getValue()));
        }
        return sb.toString();
    }

    public static Map<String, String> a(String str) {
        HashMap hashMap = new HashMap();
        for (String split : str.split("&")) {
            String[] split2 = split.split("=");
            hashMap.put(a.a.e.a.b(split2[0]), split2.length > 1 ? a.a.e.a.b(split2[1]) : "");
        }
        return hashMap;
    }
}
