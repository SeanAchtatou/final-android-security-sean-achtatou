package a.a.h;

import a.a.h.a;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

public class c {

    /* renamed from: a  reason: collision with root package name */
    public static int f236a = 4;

    /* renamed from: b  reason: collision with root package name */
    public static String[] f237b = {"CONNECT", "DISCONNECT", "EVENT", "ACK", "ERROR", "BINARY_EVENT", "BINARY_ACK"};
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public static final Logger f238c = Logger.getLogger(c.class.getName());

    static class a {

        /* renamed from: a  reason: collision with root package name */
        public b f239a;

        /* renamed from: b  reason: collision with root package name */
        List<byte[]> f240b = new ArrayList();

        a(b bVar) {
            this.f239a = bVar;
        }

        public b a(byte[] bArr) {
            this.f240b.add(bArr);
            if (this.f240b.size() != this.f239a.e) {
                return null;
            }
            b a2 = a.a(this.f239a, (byte[][]) this.f240b.toArray(new byte[this.f240b.size()][]));
            a();
            return a2;
        }

        public void a() {
            this.f239a = null;
            this.f240b = new ArrayList();
        }
    }

    public static class b extends a.a.c.a {

        /* renamed from: a  reason: collision with root package name */
        public static String f241a = "decoded";

        /* renamed from: b  reason: collision with root package name */
        a f242b = null;

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
         arg types: [java.util.logging.Level, java.lang.String, org.json.JSONException]
         candidates:
          ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
          ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
          ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
          ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
        private static b c(String str) {
            int i;
            b bVar = new b();
            int length = str.length();
            bVar.f233a = Character.getNumericValue(str.charAt(0));
            if (bVar.f233a < 0 || bVar.f233a > c.f237b.length - 1) {
                return c.c();
            }
            if (5 != bVar.f233a && 6 != bVar.f233a) {
                i = 0;
            } else if (!str.contains("-") || length <= 1) {
                return c.c();
            } else {
                StringBuilder sb = new StringBuilder();
                i = 0;
                while (true) {
                    i++;
                    if (str.charAt(i) == '-') {
                        break;
                    }
                    sb.append(str.charAt(i));
                }
                bVar.e = Integer.parseInt(sb.toString());
            }
            if (length <= i + 1 || '/' != str.charAt(i + 1)) {
                bVar.f235c = "/";
            } else {
                StringBuilder sb2 = new StringBuilder();
                do {
                    i++;
                    char charAt = str.charAt(i);
                    if (',' == charAt) {
                        break;
                    }
                    sb2.append(charAt);
                } while (i + 1 != length);
                bVar.f235c = sb2.toString();
            }
            if (length > i + 1 && Character.getNumericValue(Character.valueOf(str.charAt(i + 1)).charValue()) > -1) {
                StringBuilder sb3 = new StringBuilder();
                while (true) {
                    i++;
                    char charAt2 = str.charAt(i);
                    if (Character.getNumericValue(charAt2) < 0) {
                        i--;
                        break;
                    }
                    sb3.append(charAt2);
                    

                    /* renamed from: a.a.h.c$c  reason: collision with other inner class name */
                    public static class C0004c {

                        /* renamed from: a.a.h.c$c$a */
                        public interface a {
                            void a(Object[] objArr);
                        }

                        private String a(b bVar) {
                            boolean z;
                            StringBuilder sb = new StringBuilder();
                            sb.append(bVar.f233a);
                            if (5 == bVar.f233a || 6 == bVar.f233a) {
                                sb.append(bVar.e);
                                sb.append("-");
                            }
                            if (bVar.f235c == null || bVar.f235c.length() == 0 || "/".equals(bVar.f235c)) {
                                z = false;
                            } else {
                                sb.append(bVar.f235c);
                                z = true;
                            }
                            if (bVar.f234b >= 0) {
                                if (z) {
                                    sb.append(",");
                                    z = false;
                                }
                                sb.append(bVar.f234b);
                            }
                            if (bVar.d != null) {
                                if (z) {
                                    sb.append(",");
                                }
                                sb.append((Object) bVar.d);
                            }
                            c.f238c.fine(String.format("encoded %s as %s", bVar, sb));
                            return sb.toString();
                        }

                        private void b(b bVar, a aVar) {
                            a.C0003a a2 = a.a(bVar);
                            String a3 = a(a2.f231a);
                            ArrayList arrayList = new ArrayList(Arrays.asList(a2.f232b));
                            arrayList.add(0, a3);
                            aVar.a(arrayList.toArray());
                        }

                        public void a(b bVar, a aVar) {
                            c.f238c.fine(String.format("encoding packet %s", bVar));
                            if (5 == bVar.f233a || 6 == bVar.f233a) {
                                b(bVar, aVar);
                                return;
                            }
                            aVar.a(new String[]{a(bVar)});
                        }
                    }

                    private c() {
                    }

                    /* access modifiers changed from: private */
                    public static b<String> c() {
                        return new b<>(4, "parser error");
                    }
                }
