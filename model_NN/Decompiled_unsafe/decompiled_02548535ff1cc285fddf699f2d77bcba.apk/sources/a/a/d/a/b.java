package a.a.d.a;

import org.json.JSONArray;
import org.json.JSONObject;

public class b {

    /* renamed from: a  reason: collision with root package name */
    public String f143a;

    /* renamed from: b  reason: collision with root package name */
    public String[] f144b;

    /* renamed from: c  reason: collision with root package name */
    public long f145c;
    public long d;

    b(String str) {
        this(new JSONObject(str));
    }

    b(JSONObject jSONObject) {
        JSONArray jSONArray = jSONObject.getJSONArray("upgrades");
        int length = jSONArray.length();
        String[] strArr = new String[length];
        for (int i = 0; i < length; i++) {
            strArr[i] = jSONArray.getString(i);
        }
        this.f143a = jSONObject.getString("sid");
        this.f144b = strArr;
        this.f145c = jSONObject.getLong("pingInterval");
        this.d = jSONObject.getLong("pingTimeout");
    }
}
