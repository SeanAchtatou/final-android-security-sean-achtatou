package a.a.d.a.a;

import a.a.d.a.d;
import a.a.d.b.c;
import b.aa;
import b.b.a;
import b.b.b;
import b.u;
import b.x;
import b.y;
import b.z;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

public class c extends d {
    /* access modifiers changed from: private */
    public static final Logger n = Logger.getLogger(b.class.getName());
    /* access modifiers changed from: private */
    public a o;
    private b p;

    public c(d.a aVar) {
        super(aVar);
        this.f214b = "websocket";
    }

    /* access modifiers changed from: protected */
    public void b(a.a.d.b.b[] bVarArr) {
        this.f213a = false;
        final AnonymousClass2 r1 = new Runnable() {
            public void run() {
                a.a.i.a.b(new Runnable() {
                    public void run() {
                        this.f213a = true;
                        this.a("drain", new Object[0]);
                    }
                });
            }
        };
        final int[] iArr = {bVarArr.length};
        for (a.a.d.b.b a2 : bVarArr) {
            a.a.d.b.c.a(a2, new c.b() {
                public void a(Object obj) {
                    try {
                        if (obj instanceof String) {
                            this.o.a(y.a(a.f470a, (String) obj));
                        } else if (obj instanceof byte[]) {
                            this.o.a(y.a(a.f471b, (byte[]) obj));
                        }
                    } catch (IOException e) {
                        c.n.fine("websocket closed before onclose event");
                    }
                    int[] iArr = iArr;
                    int i = iArr[0] - 1;
                    iArr[0] = i;
                    if (i == 0) {
                        r1.run();
                    }
                }
            });
        }
    }

    /* access modifiers changed from: protected */
    public void d() {
        super.d();
    }

    /* access modifiers changed from: protected */
    public void e() {
        TreeMap treeMap = new TreeMap(String.CASE_INSENSITIVE_ORDER);
        a("requestHeaders", treeMap);
        u.a c2 = new u.a().a(0, TimeUnit.MILLISECONDS).b(0, TimeUnit.MILLISECONDS).c(0, TimeUnit.MILLISECONDS);
        if (this.j != null) {
            c2.a(this.j.getSocketFactory());
        }
        if (this.l != null) {
            c2.a(this.l);
        }
        x.a a2 = new x.a().a(h());
        for (Map.Entry entry : treeMap.entrySet()) {
            for (String b2 : (List) entry.getValue()) {
                a2.b((String) entry.getKey(), b2);
            }
        }
        x a3 = a2.a();
        u a4 = c2.a();
        this.p = b.a(a4, a3);
        this.p.a(new b.b.c() {
            public void a(int i, String str) {
                a.a.i.a.a(new Runnable() {
                    public void run() {
                        this.d();
                    }
                });
            }

            public void a(final aa aaVar) {
                final Object obj = null;
                if (aaVar.a() == a.f470a) {
                    obj = aaVar.e();
                } else if (aaVar.a() == a.f471b) {
                    obj = aaVar.c().r();
                } else {
                    a.a.i.a.a(new Runnable() {
                        public void run() {
                            d unused = this.a("Unknown payload type: " + aaVar.a(), new IllegalStateException());
                        }
                    });
                }
                aaVar.c().close();
                a.a.i.a.a(new Runnable() {
                    public void run() {
                        if (obj != null) {
                            if (obj instanceof String) {
                                this.a((String) obj);
                            } else {
                                this.a((byte[]) obj);
                            }
                        }
                    }
                });
            }

            public void a(a aVar, z zVar) {
                a unused = c.this.o = aVar;
                final Map<String, List<String>> c2 = zVar.e().c();
                a.a.i.a.a(new Runnable() {
                    public void run() {
                        this.a("responseHeaders", c2);
                        this.c();
                    }
                });
            }

            public void a(c.c cVar) {
            }

            public void a(final IOException iOException, z zVar) {
                a.a.i.a.a(new Runnable() {
                    public void run() {
                        d unused = this.a("websocket error", iOException);
                    }
                });
            }
        });
        a4.s().a().shutdown();
    }

    /* access modifiers changed from: protected */
    public void f() {
        if (this.p != null) {
            this.p.a();
        }
        if (this.o != null) {
            try {
                this.o.a(1000, "");
            } catch (IOException | IllegalStateException e) {
            }
        }
    }

    /* access modifiers changed from: protected */
    public String h() {
        Map map = this.f215c;
        if (map == null) {
            map = new HashMap();
        }
        String str = this.d ? "wss" : "ws";
        String str2 = "";
        if (this.f > 0 && (("wss".equals(str) && this.f != 443) || ("ws".equals(str) && this.f != 80))) {
            str2 = ":" + this.f;
        }
        if (this.e) {
            map.put(this.i, a.a.k.a.a());
        }
        String a2 = a.a.g.a.a(map);
        if (a2.length() > 0) {
            a2 = "?" + a2;
        }
        return str + "://" + (this.h.contains(":") ? "[" + this.h + "]" : this.h) + str2 + this.g + a2;
    }
}
