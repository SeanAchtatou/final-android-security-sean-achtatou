package a.a.d.a;

import a.a.c.a;
import a.a.d.a.d;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import org.json.JSONException;

public class c extends a.a.c.a {
    /* access modifiers changed from: private */
    public static final Logger e = Logger.getLogger(c.class.getName());
    private static final Runnable f = new Runnable() {
        public void run() {
        }
    };
    /* access modifiers changed from: private */
    public static boolean g = false;
    private static SSLContext h;
    private static HostnameVerifier i;
    private SSLContext A;
    private HostnameVerifier B;
    /* access modifiers changed from: private */
    public b C;
    private ScheduledExecutorService D;
    private final a.C0001a E;

    /* renamed from: a  reason: collision with root package name */
    int f146a;

    /* renamed from: b  reason: collision with root package name */
    String f147b;

    /* renamed from: c  reason: collision with root package name */
    LinkedList<a.a.d.b.b> f148c;
    d d;
    private boolean j;
    private boolean k;
    private boolean l;
    /* access modifiers changed from: private */
    public boolean m;
    /* access modifiers changed from: private */
    public boolean n;
    private int o;
    private int p;
    private long q;
    /* access modifiers changed from: private */
    public long r;
    private String s;
    private String t;
    private String u;
    /* access modifiers changed from: private */
    public List<String> v;
    private List<String> w;
    private Map<String, String> x;
    private Future y;
    private Future z;

    public static class a extends d.a {
        public String[] i;
        public boolean j = true;
        public boolean k;
        public String l;
        public String m;

        /* access modifiers changed from: private */
        public static a b(URI uri, a aVar) {
            if (aVar == null) {
                aVar = new a();
            }
            aVar.l = uri.getHost();
            aVar.q = "https".equals(uri.getScheme()) || "wss".equals(uri.getScheme());
            aVar.s = uri.getPort();
            String rawQuery = uri.getRawQuery();
            if (rawQuery != null) {
                aVar.m = rawQuery;
            }
            return aVar;
        }
    }

    private enum b {
        OPENING,
        OPEN,
        CLOSING,
        CLOSED;

        public String toString() {
            return super.toString().toLowerCase();
        }
    }

    public c() {
        this(new a());
    }

    public c(a aVar) {
        this.f148c = new LinkedList<>();
        this.E = new a.C0001a() {
            public void a(Object... objArr) {
                c.this.a(objArr.length > 0 ? ((Long) objArr[0]).longValue() : 0);
            }
        };
        if (aVar.l != null) {
            String str = aVar.l;
            if (str.split(":").length > 2) {
                int indexOf = str.indexOf(91);
                str = indexOf != -1 ? str.substring(indexOf + 1) : str;
                int lastIndexOf = str.lastIndexOf(93);
                if (lastIndexOf != -1) {
                    str = str.substring(0, lastIndexOf);
                }
            }
            aVar.n = str;
        }
        this.j = aVar.q;
        if (aVar.s == -1) {
            aVar.s = this.j ? 443 : 80;
        }
        this.A = aVar.v != null ? aVar.v : h;
        this.f147b = aVar.n != null ? aVar.n : "localhost";
        this.f146a = aVar.s;
        this.x = aVar.m != null ? a.a.g.a.a(aVar.m) : new HashMap<>();
        this.k = aVar.j;
        this.t = (aVar.o != null ? aVar.o : "/engine.io").replaceAll("/$", "") + "/";
        this.u = aVar.p != null ? aVar.p : "t";
        this.l = aVar.r;
        this.v = new ArrayList(Arrays.asList(aVar.i != null ? aVar.i : new String[]{"polling", "websocket"}));
        this.o = aVar.t != 0 ? aVar.t : 843;
        this.n = aVar.k;
        this.B = aVar.w != null ? aVar.w : i;
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public c(URI uri, a aVar) {
        this(uri != null ? a.b(uri, aVar) : aVar);
    }

    /* access modifiers changed from: private */
    public void a(long j2) {
        if (this.y != null) {
            this.y.cancel(false);
        }
        if (j2 <= 0) {
            j2 = this.q + this.r;
        }
        this.y = l().schedule(new Runnable() {
            public void run() {
                a.a.i.a.a(new Runnable() {
                    public void run() {
                        if (this.C != b.CLOSED) {
                            this.e("ping timeout");
                        }
                    }
                });
            }
        }, j2, TimeUnit.MILLISECONDS);
    }

    private void a(b bVar) {
        a("handshake", bVar);
        this.s = bVar.f143a;
        this.d.f215c.put("sid", bVar.f143a);
        this.w = a(Arrays.asList(bVar.f144b));
        this.q = bVar.f145c;
        this.r = bVar.d;
        f();
        if (b.CLOSED != this.C) {
            h();
            c("heartbeat", this.E);
            a("heartbeat", this.E);
        }
    }

    /* access modifiers changed from: private */
    public void a(d dVar) {
        e.fine(String.format("setting transport %s", dVar.f214b));
        if (this.d != null) {
            e.fine(String.format("clearing existing transport %s", this.d.f214b));
            this.d.g();
        }
        this.d = dVar;
        dVar.a("drain", new a.C0001a() {
            public void a(Object... objArr) {
                this.j();
            }
        }).a("packet", new a.C0001a() {
            public void a(Object... objArr) {
                this.a(objArr.length > 0 ? (a.a.d.b.b) objArr[0] : null);
            }
        }).a("error", new a.C0001a() {
            public void a(Object... objArr) {
                this.a(objArr.length > 0 ? (Exception) objArr[0] : null);
            }
        }).a("close", new a.C0001a() {
            public void a(Object... objArr) {
                this.e("transport close");
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(a.a.d.b.b bVar) {
        if (this.C == b.OPENING || this.C == b.OPEN) {
            e.fine(String.format("socket received: type '%s', data '%s'", bVar.f223a, bVar.f224b));
            a("packet", bVar);
            a("heartbeat", new Object[0]);
            if ("open".equals(bVar.f223a)) {
                try {
                    a(new b((String) bVar.f224b));
                } catch (JSONException e2) {
                    a("error", new a(e2));
                }
            } else if ("pong".equals(bVar.f223a)) {
                h();
                a("pong", new Object[0]);
            } else if ("error".equals(bVar.f223a)) {
                a aVar = new a("server error");
                aVar.f77b = bVar.f224b;
                a(aVar);
            } else if ("message".equals(bVar.f223a)) {
                a("data", bVar.f224b);
                a("message", bVar.f224b);
            }
        } else {
            e.fine(String.format("packet received with socket readyState '%s'", this.C));
        }
    }

    private void a(a.a.d.b.b bVar, final Runnable runnable) {
        if (b.CLOSING != this.C && b.CLOSED != this.C) {
            a("packetCreate", bVar);
            this.f148c.offer(bVar);
            if (runnable != null) {
                b("flush", new a.C0001a() {
                    public void a(Object... objArr) {
                        runnable.run();
                    }
                });
            }
            k();
        }
    }

    /* access modifiers changed from: private */
    public void a(Exception exc) {
        e.fine(String.format("socket error %s", exc));
        g = false;
        a("error", exc);
        a("transport error", exc);
    }

    private void a(String str, Exception exc) {
        if (b.OPENING == this.C || b.OPEN == this.C || b.CLOSING == this.C) {
            e.fine(String.format("socket close with reason: %s", str));
            if (this.z != null) {
                this.z.cancel(false);
            }
            if (this.y != null) {
                this.y.cancel(false);
            }
            if (this.D != null) {
                this.D.shutdown();
            }
            this.d.b("close");
            this.d.b();
            this.d.g();
            this.C = b.CLOSED;
            this.s = null;
            a("close", str, exc);
            this.f148c.clear();
            this.p = 0;
        }
    }

    /* access modifiers changed from: private */
    public void a(String str, String str2, Runnable runnable) {
        a(new a.a.d.b.b(str, str2), runnable);
    }

    /* access modifiers changed from: private */
    public void a(String str, byte[] bArr, Runnable runnable) {
        a(new a.a.d.b.b(str, bArr), runnable);
    }

    /* access modifiers changed from: private */
    public d c(String str) {
        d bVar;
        e.fine(String.format("creating transport '%s'", str));
        HashMap hashMap = new HashMap(this.x);
        hashMap.put("EIO", String.valueOf(3));
        hashMap.put("transport", str);
        if (this.s != null) {
            hashMap.put("sid", this.s);
        }
        d.a aVar = new d.a();
        aVar.v = this.A;
        aVar.n = this.f147b;
        aVar.s = this.f146a;
        aVar.q = this.j;
        aVar.o = this.t;
        aVar.u = hashMap;
        aVar.r = this.l;
        aVar.p = this.u;
        aVar.t = this.o;
        aVar.x = this;
        aVar.w = this.B;
        if ("websocket".equals(str)) {
            bVar = new a.a.d.a.a.c(aVar);
        } else if ("polling".equals(str)) {
            bVar = new a.a.d.a.a.b(aVar);
        } else {
            throw new RuntimeException();
        }
        a("transport", bVar);
        return bVar;
    }

    /* access modifiers changed from: private */
    public void c(String str, Runnable runnable) {
        a(new a.a.d.b.b(str), runnable);
    }

    private void d(String str) {
        e.fine(String.format("probing transport '%s'", str));
        final d[] dVarArr = {c(str)};
        final boolean[] zArr = {false};
        g = false;
        final String str2 = str;
        AnonymousClass19 r2 = new a.C0001a() {
            public void a(Object... objArr) {
                if (!zArr[0]) {
                    c.e.fine(String.format("probe transport '%s' opened", str2));
                    a.a.d.b.b bVar = new a.a.d.b.b("ping", "probe");
                    dVarArr[0].a(new a.a.d.b.b[]{bVar});
                    dVarArr[0].b("packet", new a.C0001a() {
                        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                         method: a.a.d.a.c.a(a.a.d.a.c, boolean):boolean
                         arg types: [a.a.d.a.c, int]
                         candidates:
                          a.a.d.a.c.a(a.a.d.a.c, a.a.d.a.c$b):a.a.d.a.c$b
                          a.a.d.a.c.a(a.a.d.a.c, java.lang.String):a.a.d.a.d
                          a.a.d.a.c.a(a.a.d.a.c, long):void
                          a.a.d.a.c.a(a.a.d.a.c, a.a.d.a.d):void
                          a.a.d.a.c.a(a.a.d.a.c, a.a.d.b.b):void
                          a.a.d.a.c.a(a.a.d.a.c, java.lang.Exception):void
                          a.a.d.a.c.a(a.a.d.b.b, java.lang.Runnable):void
                          a.a.d.a.c.a(java.lang.String, java.lang.Exception):void
                          a.a.d.a.c.a(java.lang.String, java.lang.Runnable):void
                          a.a.d.a.c.a(byte[], java.lang.Runnable):void
                          a.a.c.a.a(a.a.c.a$a, a.a.c.a$a):boolean
                          a.a.c.a.a(java.lang.String, a.a.c.a$a):a.a.c.a
                          a.a.c.a.a(java.lang.String, java.lang.Object[]):a.a.c.a
                          a.a.d.a.c.a(a.a.d.a.c, boolean):boolean */
                        public void a(Object... objArr) {
                            if (!zArr[0]) {
                                a.a.d.b.b bVar = (a.a.d.b.b) objArr[0];
                                if (!"pong".equals(bVar.f223a) || !"probe".equals(bVar.f224b)) {
                                    c.e.fine(String.format("probe transport '%s' failed", str2));
                                    a aVar = new a("probe error");
                                    aVar.f76a = dVarArr[0].f214b;
                                    this.a("upgradeError", aVar);
                                    return;
                                }
                                c.e.fine(String.format("probe transport '%s' pong", str2));
                                boolean unused = this.m = true;
                                this.a("upgrading", dVarArr[0]);
                                if (dVarArr[0] != null) {
                                    boolean unused2 = c.g = "websocket".equals(dVarArr[0].f214b);
                                    c.e.fine(String.format("pausing current transport '%s'", this.d.f214b));
                                    ((a.a.d.a.a.a) this.d).a((Runnable) new Runnable() {
                                        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                                         method: a.a.d.a.c.a(a.a.d.a.c, boolean):boolean
                                         arg types: [a.a.d.a.c, int]
                                         candidates:
                                          a.a.d.a.c.a(a.a.d.a.c, a.a.d.a.c$b):a.a.d.a.c$b
                                          a.a.d.a.c.a(a.a.d.a.c, java.lang.String):a.a.d.a.d
                                          a.a.d.a.c.a(a.a.d.a.c, long):void
                                          a.a.d.a.c.a(a.a.d.a.c, a.a.d.a.d):void
                                          a.a.d.a.c.a(a.a.d.a.c, a.a.d.b.b):void
                                          a.a.d.a.c.a(a.a.d.a.c, java.lang.Exception):void
                                          a.a.d.a.c.a(a.a.d.b.b, java.lang.Runnable):void
                                          a.a.d.a.c.a(java.lang.String, java.lang.Exception):void
                                          a.a.d.a.c.a(java.lang.String, java.lang.Runnable):void
                                          a.a.d.a.c.a(byte[], java.lang.Runnable):void
                                          a.a.c.a.a(a.a.c.a$a, a.a.c.a$a):boolean
                                          a.a.c.a.a(java.lang.String, a.a.c.a$a):a.a.c.a
                                          a.a.c.a.a(java.lang.String, java.lang.Object[]):a.a.c.a
                                          a.a.d.a.c.a(a.a.d.a.c, boolean):boolean */
                                        public void run() {
                                            if (!zArr[0] && b.CLOSED != this.C) {
                                                c.e.fine("changing transport and sending upgrade packet");
                                                r8[0].run();
                                                this.a(dVarArr[0]);
                                                a.a.d.b.b bVar = new a.a.d.b.b("upgrade");
                                                dVarArr[0].a(new a.a.d.b.b[]{bVar});
                                                this.a("upgrade", dVarArr[0]);
                                                dVarArr[0] = null;
                                                boolean unused = this.m = false;
                                                this.k();
                                            }
                                        }
                                    });
                                }
                            }
                        }
                    });
                }
            }
        };
        final AnonymousClass20 r12 = new a.C0001a() {
            public void a(Object... objArr) {
                if (!zArr[0]) {
                    zArr[0] = true;
                    r8[0].run();
                    dVarArr[0].b();
                    dVarArr[0] = null;
                }
            }
        };
        final d[] dVarArr2 = dVarArr;
        final String str3 = str;
        final AnonymousClass21 r9 = new a.C0001a() {
            public void a(Object... objArr) {
                Object obj = objArr[0];
                a aVar = obj instanceof Exception ? new a("probe error", (Exception) obj) : obj instanceof String ? new a("probe error: " + ((String) obj)) : new a("probe error");
                aVar.f76a = dVarArr2[0].f214b;
                r12.a(new Object[0]);
                c.e.fine(String.format("probe transport \"%s\" failed because of error: %s", str3, obj));
                this.a("upgradeError", aVar);
            }
        };
        final AnonymousClass2 r15 = new a.C0001a() {
            public void a(Object... objArr) {
                r9.a("transport closed");
            }
        };
        final AnonymousClass3 r0 = new a.C0001a() {
            public void a(Object... objArr) {
                r9.a("socket closed");
            }
        };
        final AnonymousClass4 r02 = new a.C0001a() {
            public void a(Object... objArr) {
                d dVar = (d) objArr[0];
                if (dVarArr[0] != null && !dVar.f214b.equals(dVarArr[0].f214b)) {
                    c.e.fine(String.format("'%s' works - aborting '%s'", dVar.f214b, dVarArr[0].f214b));
                    r12.a(new Object[0]);
                }
            }
        };
        final d[] dVarArr3 = dVarArr;
        final AnonymousClass19 r13 = r2;
        final AnonymousClass21 r14 = r9;
        final Runnable[] runnableArr = {new Runnable() {
            public void run() {
                dVarArr3[0].c("open", r13);
                dVarArr3[0].c("error", r14);
                dVarArr3[0].c("close", r15);
                this.c("close", r0);
                this.c("upgrading", r02);
            }
        }};
        dVarArr[0].b("open", r2);
        dVarArr[0].b("error", r9);
        dVarArr[0].b("close", r15);
        b("close", r0);
        b("upgrading", r02);
        dVarArr[0].a();
    }

    /* access modifiers changed from: private */
    public void e(String str) {
        a(str, (Exception) null);
    }

    private void f() {
        e.fine("socket open");
        this.C = b.OPEN;
        g = "websocket".equals(this.d.f214b);
        a("open", new Object[0]);
        k();
        if (this.C == b.OPEN && this.k && (this.d instanceof a.a.d.a.a.a)) {
            e.fine("starting upgrade probes");
            for (String d2 : this.w) {
                d(d2);
            }
        }
    }

    private void h() {
        if (this.z != null) {
            this.z.cancel(false);
        }
        this.z = l().schedule(new Runnable() {
            public void run() {
                a.a.i.a.a(new Runnable() {
                    public void run() {
                        c.e.fine(String.format("writing ping packet - expecting pong within %sms", Long.valueOf(this.r)));
                        this.i();
                        this.a(this.r);
                    }
                });
            }
        }, this.q, TimeUnit.MILLISECONDS);
    }

    /* access modifiers changed from: private */
    public void i() {
        a.a.i.a.a(new Runnable() {
            public void run() {
                c.this.c("ping", new Runnable() {
                    public void run() {
                        c.this.a("ping", new Object[0]);
                    }
                });
            }
        });
    }

    /* access modifiers changed from: private */
    public void j() {
        for (int i2 = 0; i2 < this.p; i2++) {
            this.f148c.poll();
        }
        this.p = 0;
        if (this.f148c.size() == 0) {
            a("drain", new Object[0]);
        } else {
            k();
        }
    }

    /* access modifiers changed from: private */
    public void k() {
        if (this.C != b.CLOSED && this.d.f213a && !this.m && this.f148c.size() != 0) {
            e.fine(String.format("flushing %d packets in socket", Integer.valueOf(this.f148c.size())));
            this.p = this.f148c.size();
            this.d.a((a.a.d.b.b[]) this.f148c.toArray(new a.a.d.b.b[this.f148c.size()]));
            a("flush", new Object[0]);
        }
    }

    private ScheduledExecutorService l() {
        if (this.D == null || this.D.isShutdown()) {
            this.D = Executors.newSingleThreadScheduledExecutor();
        }
        return this.D;
    }

    public c a() {
        a.a.i.a.a(new Runnable() {
            public void run() {
                String str;
                if (c.this.n && c.g && c.this.v.contains("websocket")) {
                    str = "websocket";
                } else if (c.this.v.size() == 0) {
                    final c cVar = c.this;
                    a.a.i.a.b(new Runnable() {
                        public void run() {
                            cVar.a("error", new a("No transports available"));
                        }
                    });
                    return;
                } else {
                    str = (String) c.this.v.get(0);
                }
                b unused = c.this.C = b.OPENING;
                d a2 = c.this.c(str);
                c.this.a(a2);
                a2.a();
            }
        });
        return this;
    }

    /* access modifiers changed from: package-private */
    public List<String> a(List<String> list) {
        ArrayList arrayList = new ArrayList();
        for (String next : list) {
            if (this.v.contains(next)) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    public void a(String str) {
        a(str, (Runnable) null);
    }

    public void a(String str, Runnable runnable) {
        b(str, runnable);
    }

    public void a(byte[] bArr) {
        a(bArr, (Runnable) null);
    }

    public void a(byte[] bArr, Runnable runnable) {
        b(bArr, runnable);
    }

    public c b() {
        a.a.i.a.a(new Runnable() {
            public void run() {
                if (c.this.C == b.OPENING || c.this.C == b.OPEN) {
                    b unused = c.this.C = b.CLOSING;
                    final c cVar = c.this;
                    final AnonymousClass1 r1 = new Runnable() {
                        public void run() {
                            cVar.e("forced close");
                            c.e.fine("socket closing - telling transport to close");
                            cVar.d.b();
                        }
                    };
                    final a.C0001a[] aVarArr = {new a.C0001a() {
                        public void a(Object... objArr) {
                            cVar.c("upgrade", aVarArr[0]);
                            cVar.c("upgradeError", aVarArr[0]);
                            r1.run();
                        }
                    }};
                    final AnonymousClass3 r3 = new Runnable() {
                        public void run() {
                            cVar.b("upgrade", aVarArr[0]);
                            cVar.b("upgradeError", aVarArr[0]);
                        }
                    };
                    if (c.this.f148c.size() > 0) {
                        c.this.b("drain", new a.C0001a() {
                            public void a(Object... objArr) {
                                if (c.this.m) {
                                    r3.run();
                                } else {
                                    r1.run();
                                }
                            }
                        });
                    } else if (c.this.m) {
                        r3.run();
                    } else {
                        r1.run();
                    }
                }
            }
        });
        return this;
    }

    public void b(final String str, final Runnable runnable) {
        a.a.i.a.a(new Runnable() {
            public void run() {
                c.this.a("message", str, runnable);
            }
        });
    }

    public void b(final byte[] bArr, final Runnable runnable) {
        a.a.i.a.a(new Runnable() {
            public void run() {
                c.this.a("message", bArr, runnable);
            }
        });
    }

    public String c() {
        return this.s;
    }
}
