package a.a.d.a.a;

import a.a.c.a;
import a.a.d.a.d;
import a.a.d.b.b;
import a.a.d.b.c;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

public abstract class a extends d {
    /* access modifiers changed from: private */
    public static final Logger n = Logger.getLogger(a.class.getName());
    /* access modifiers changed from: private */
    public boolean o;

    public a(d.a aVar) {
        super(aVar);
        this.f214b = "polling";
    }

    private void a(Object obj) {
        n.fine(String.format("polling got data %s", obj));
        AnonymousClass2 r0 = new c.a() {
            public boolean a(b bVar, int i, int i2) {
                if (this.m == d.b.OPENING) {
                    this.c();
                }
                if ("close".equals(bVar.f223a)) {
                    this.d();
                    return false;
                }
                this.a(bVar);
                return true;
            }
        };
        if (obj instanceof String) {
            c.a((String) obj, r0);
        } else if (obj instanceof byte[]) {
            c.a((byte[]) obj, r0);
        }
        if (this.m != d.b.CLOSED) {
            this.o = false;
            a("pollComplete", new Object[0]);
            if (this.m == d.b.OPEN) {
                k();
                return;
            }
            n.fine(String.format("ignoring poll - transport state '%s'", this.m));
        }
    }

    private void k() {
        n.fine("polling");
        this.o = true;
        i();
        a("poll", new Object[0]);
    }

    public void a(final Runnable runnable) {
        a.a.i.a.a(new Runnable() {
            public void run() {
                final a aVar = a.this;
                d.b unused = a.this.m = d.b.PAUSED;
                final AnonymousClass1 r1 = new Runnable() {
                    public void run() {
                        a.n.fine("paused");
                        d.b unused = aVar.m = d.b.PAUSED;
                        runnable.run();
                    }
                };
                if (a.this.o || !a.this.f213a) {
                    final int[] iArr = {0};
                    if (a.this.o) {
                        a.n.fine("we are currently polling - waiting to pause");
                        iArr[0] = iArr[0] + 1;
                        a.this.b("pollComplete", new a.C0001a() {
                            public void a(Object... objArr) {
                                a.n.fine("pre-pause polling complete");
                                int[] iArr = iArr;
                                int i = iArr[0] - 1;
                                iArr[0] = i;
                                if (i == 0) {
                                    r1.run();
                                }
                            }
                        });
                    }
                    if (!a.this.f213a) {
                        a.n.fine("we are currently writing - waiting to pause");
                        iArr[0] = iArr[0] + 1;
                        a.this.b("drain", new a.C0001a() {
                            public void a(Object... objArr) {
                                a.n.fine("pre-pause writing complete");
                                int[] iArr = iArr;
                                int i = iArr[0] - 1;
                                iArr[0] = i;
                                if (i == 0) {
                                    r1.run();
                                }
                            }
                        });
                        return;
                    }
                    return;
                }
                r1.run();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void a(String str) {
        a((Object) str);
    }

    /* access modifiers changed from: protected */
    public void a(byte[] bArr) {
        a((Object) bArr);
    }

    /* access modifiers changed from: protected */
    public abstract void a(byte[] bArr, Runnable runnable);

    /* access modifiers changed from: protected */
    public void b(b[] bVarArr) {
        this.f213a = false;
        final AnonymousClass4 r0 = new Runnable() {
            public void run() {
                this.f213a = true;
                this.a("drain", new Object[0]);
            }
        };
        c.a(bVarArr, new c.b<byte[]>() {
            public void a(byte[] bArr) {
                this.a(bArr, r0);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void e() {
        k();
    }

    /* access modifiers changed from: protected */
    public void f() {
        AnonymousClass3 r0 = new a.C0001a() {
            public void a(Object... objArr) {
                a.n.fine("writing close packet");
                try {
                    this.b(new b[]{new b("close")});
                } catch (a.a.j.b e) {
                    throw new RuntimeException(e);
                }
            }
        };
        if (this.m == d.b.OPEN) {
            n.fine("transport open - closing");
            r0.a(new Object[0]);
            return;
        }
        n.fine("transport not open - deferring close");
        b("open", r0);
    }

    /* access modifiers changed from: protected */
    public String h() {
        Map map = this.f215c;
        if (map == null) {
            map = new HashMap();
        }
        String str = this.d ? "https" : "http";
        if (this.e) {
            map.put(this.i, a.a.k.a.a());
        }
        String a2 = a.a.g.a.a(map);
        String str2 = (this.f <= 0 || ((!"https".equals(str) || this.f == 443) && (!"http".equals(str) || this.f == 80))) ? "" : ":" + this.f;
        return str + "://" + (this.h.contains(":") ? "[" + this.h + "]" : this.h) + str2 + this.g + (a2.length() > 0 ? "?" + a2 : a2);
    }

    /* access modifiers changed from: protected */
    public abstract void i();
}
