package a.a.d.a;

import a.a.d.b.c;
import java.util.Map;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;

public abstract class d extends a.a.c.a {

    /* renamed from: a  reason: collision with root package name */
    public boolean f213a;

    /* renamed from: b  reason: collision with root package name */
    public String f214b;

    /* renamed from: c  reason: collision with root package name */
    public Map<String, String> f215c;
    protected boolean d;
    protected boolean e;
    protected int f;
    protected String g;
    protected String h;
    protected String i;
    protected SSLContext j;
    protected c k;
    protected HostnameVerifier l;
    /* access modifiers changed from: protected */
    public b m;

    public static class a {
        public String n;
        public String o;
        public String p;
        public boolean q;
        public boolean r;
        public int s = -1;
        public int t = -1;
        public Map<String, String> u;
        public SSLContext v;
        public HostnameVerifier w;
        protected c x;
    }

    protected enum b {
        OPENING,
        OPEN,
        CLOSED,
        PAUSED;

        public String toString() {
            return super.toString().toLowerCase();
        }
    }

    public d(a aVar) {
        this.g = aVar.o;
        this.h = aVar.n;
        this.f = aVar.s;
        this.d = aVar.q;
        this.f215c = aVar.u;
        this.i = aVar.p;
        this.e = aVar.r;
        this.j = aVar.v;
        this.k = aVar.x;
        this.l = aVar.w;
    }

    public d a() {
        a.a.i.a.a(new Runnable() {
            public void run() {
                if (d.this.m == b.CLOSED || d.this.m == null) {
                    d.this.m = b.OPENING;
                    d.this.e();
                }
            }
        });
        return this;
    }

    /* access modifiers changed from: protected */
    public d a(String str, Exception exc) {
        a("error", new a(str, exc));
        return this;
    }

    /* access modifiers changed from: protected */
    public void a(a.a.d.b.b bVar) {
        a("packet", bVar);
    }

    /* access modifiers changed from: protected */
    public void a(String str) {
        a(c.a(str));
    }

    /* access modifiers changed from: protected */
    public void a(byte[] bArr) {
        a(c.a(bArr));
    }

    public void a(final a.a.d.b.b[] bVarArr) {
        a.a.i.a.a(new Runnable() {
            public void run() {
                if (d.this.m == b.OPEN) {
                    try {
                        d.this.b(bVarArr);
                    } catch (a.a.j.b e) {
                        throw new RuntimeException(e);
                    }
                } else {
                    throw new RuntimeException("Transport not open");
                }
            }
        });
    }

    public d b() {
        a.a.i.a.a(new Runnable() {
            public void run() {
                if (d.this.m == b.OPENING || d.this.m == b.OPEN) {
                    d.this.f();
                    d.this.d();
                }
            }
        });
        return this;
    }

    /* access modifiers changed from: protected */
    public abstract void b(a.a.d.b.b[] bVarArr);

    /* access modifiers changed from: protected */
    public void c() {
        this.m = b.OPEN;
        this.f213a = true;
        a("open", new Object[0]);
    }

    /* access modifiers changed from: protected */
    public void d() {
        this.m = b.CLOSED;
        a("close", new Object[0]);
    }

    /* access modifiers changed from: protected */
    public abstract void e();

    /* access modifiers changed from: protected */
    public abstract void f();
}
