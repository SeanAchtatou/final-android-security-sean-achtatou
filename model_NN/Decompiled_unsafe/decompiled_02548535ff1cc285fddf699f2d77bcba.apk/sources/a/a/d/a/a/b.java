package a.a.d.a.a;

import a.a.c.a;
import a.a.d.a.d;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Logger;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;

public class b extends a {
    /* access modifiers changed from: private */
    public static final Logger n = Logger.getLogger(b.class.getName());
    private a o;
    private a p;

    public static class a extends a.a.c.a {

        /* renamed from: a  reason: collision with root package name */
        private String f118a;

        /* renamed from: b  reason: collision with root package name */
        private String f119b;
        /* access modifiers changed from: private */

        /* renamed from: c  reason: collision with root package name */
        public byte[] f120c;
        private SSLContext d;
        /* access modifiers changed from: private */
        public HttpURLConnection e;
        private HostnameVerifier f;

        /* renamed from: a.a.d.a.a.b$a$a  reason: collision with other inner class name */
        public static class C0002a {

            /* renamed from: a  reason: collision with root package name */
            public String f123a;

            /* renamed from: b  reason: collision with root package name */
            public String f124b;

            /* renamed from: c  reason: collision with root package name */
            public byte[] f125c;
            public SSLContext d;
            public HostnameVerifier e;
        }

        public a(C0002a aVar) {
            this.f118a = aVar.f124b != null ? aVar.f124b : "GET";
            this.f119b = aVar.f123a;
            this.f120c = aVar.f125c;
            this.d = aVar.d;
            this.f = aVar.e;
        }

        /* access modifiers changed from: private */
        public void a(Exception exc) {
            a("error", exc);
            c();
        }

        private void a(String str) {
            a("data", str);
            b();
        }

        private void a(Map<String, List<String>> map) {
            a("requestHeaders", map);
        }

        private void a(byte[] bArr) {
            a("data", bArr);
            b();
        }

        private void b() {
            a("success", new Object[0]);
            c();
        }

        /* access modifiers changed from: private */
        public void b(Map<String, List<String>> map) {
            a("responseHeaders", map);
        }

        private void c() {
            if (this.e != null) {
                this.e.disconnect();
                this.e = null;
            }
        }

        /* access modifiers changed from: private */
        /* JADX WARNING: Removed duplicated region for block: B:18:0x0054 A[SYNTHETIC, Splitter:B:18:0x0054] */
        /* JADX WARNING: Removed duplicated region for block: B:21:0x0059 A[SYNTHETIC, Splitter:B:21:0x0059] */
        /* JADX WARNING: Removed duplicated region for block: B:45:0x00a4 A[SYNTHETIC, Splitter:B:45:0x00a4] */
        /* JADX WARNING: Removed duplicated region for block: B:48:0x00a9 A[SYNTHETIC, Splitter:B:48:0x00a9] */
        /* JADX WARNING: Removed duplicated region for block: B:64:? A[RETURN, SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void d() {
            /*
                r10 = this;
                r1 = 0
                r0 = 0
                java.net.HttpURLConnection r2 = r10.e
                java.lang.String r2 = r2.getContentType()
                java.lang.String r3 = "application/octet-stream"
                boolean r2 = r3.equalsIgnoreCase(r2)     // Catch:{ IOException -> 0x00be, all -> 0x00a0 }
                if (r2 == 0) goto L_0x0071
                java.io.BufferedInputStream r2 = new java.io.BufferedInputStream     // Catch:{ IOException -> 0x00be, all -> 0x00a0 }
                java.net.HttpURLConnection r3 = r10.e     // Catch:{ IOException -> 0x00be, all -> 0x00a0 }
                java.io.InputStream r3 = r3.getInputStream()     // Catch:{ IOException -> 0x00be, all -> 0x00a0 }
                r2.<init>(r3)     // Catch:{ IOException -> 0x00be, all -> 0x00a0 }
                java.util.ArrayList r3 = new java.util.ArrayList     // Catch:{ IOException -> 0x004e }
                r3.<init>()     // Catch:{ IOException -> 0x004e }
                r4 = 1024(0x400, float:1.435E-42)
                byte[] r4 = new byte[r4]     // Catch:{ IOException -> 0x004e }
            L_0x0024:
                int r5 = r2.read(r4)     // Catch:{ IOException -> 0x004e }
                if (r5 <= 0) goto L_0x0036
                byte[] r6 = new byte[r5]     // Catch:{ IOException -> 0x004e }
                r7 = 0
                r8 = 0
                java.lang.System.arraycopy(r4, r7, r6, r8, r5)     // Catch:{ IOException -> 0x004e }
                r3.add(r6)     // Catch:{ IOException -> 0x004e }
                int r0 = r0 + r5
                goto L_0x0024
            L_0x0036:
                java.nio.ByteBuffer r4 = java.nio.ByteBuffer.allocate(r0)     // Catch:{ IOException -> 0x004e }
                java.util.Iterator r3 = r3.iterator()     // Catch:{ IOException -> 0x004e }
            L_0x003e:
                boolean r0 = r3.hasNext()     // Catch:{ IOException -> 0x004e }
                if (r0 == 0) goto L_0x005d
                java.lang.Object r0 = r3.next()     // Catch:{ IOException -> 0x004e }
                byte[] r0 = (byte[]) r0     // Catch:{ IOException -> 0x004e }
                r4.put(r0)     // Catch:{ IOException -> 0x004e }
                goto L_0x003e
            L_0x004e:
                r0 = move-exception
            L_0x004f:
                r10.a(r0)     // Catch:{ all -> 0x00b7 }
                if (r2 == 0) goto L_0x0057
                r2.close()     // Catch:{ IOException -> 0x00af }
            L_0x0057:
                if (r1 == 0) goto L_0x005c
                r1.close()     // Catch:{ IOException -> 0x00b1 }
            L_0x005c:
                return
            L_0x005d:
                byte[] r0 = r4.array()     // Catch:{ IOException -> 0x004e }
                r10.a(r0)     // Catch:{ IOException -> 0x004e }
            L_0x0064:
                if (r2 == 0) goto L_0x0069
                r2.close()     // Catch:{ IOException -> 0x00ad }
            L_0x0069:
                if (r1 == 0) goto L_0x005c
                r1.close()     // Catch:{ IOException -> 0x006f }
                goto L_0x005c
            L_0x006f:
                r0 = move-exception
                goto L_0x005c
            L_0x0071:
                java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00be, all -> 0x00a0 }
                r0.<init>()     // Catch:{ IOException -> 0x00be, all -> 0x00a0 }
                java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ IOException -> 0x00be, all -> 0x00a0 }
                java.io.InputStreamReader r3 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x00be, all -> 0x00a0 }
                java.net.HttpURLConnection r4 = r10.e     // Catch:{ IOException -> 0x00be, all -> 0x00a0 }
                java.io.InputStream r4 = r4.getInputStream()     // Catch:{ IOException -> 0x00be, all -> 0x00a0 }
                r3.<init>(r4)     // Catch:{ IOException -> 0x00be, all -> 0x00a0 }
                r2.<init>(r3)     // Catch:{ IOException -> 0x00be, all -> 0x00a0 }
            L_0x0086:
                java.lang.String r3 = r2.readLine()     // Catch:{ IOException -> 0x0090, all -> 0x00b9 }
                if (r3 == 0) goto L_0x0095
                r0.append(r3)     // Catch:{ IOException -> 0x0090, all -> 0x00b9 }
                goto L_0x0086
            L_0x0090:
                r0 = move-exception
                r9 = r2
                r2 = r1
                r1 = r9
                goto L_0x004f
            L_0x0095:
                java.lang.String r0 = r0.toString()     // Catch:{ IOException -> 0x0090, all -> 0x00b9 }
                r10.a(r0)     // Catch:{ IOException -> 0x0090, all -> 0x00b9 }
                r9 = r2
                r2 = r1
                r1 = r9
                goto L_0x0064
            L_0x00a0:
                r0 = move-exception
                r2 = r1
            L_0x00a2:
                if (r2 == 0) goto L_0x00a7
                r2.close()     // Catch:{ IOException -> 0x00b3 }
            L_0x00a7:
                if (r1 == 0) goto L_0x00ac
                r1.close()     // Catch:{ IOException -> 0x00b5 }
            L_0x00ac:
                throw r0
            L_0x00ad:
                r0 = move-exception
                goto L_0x0069
            L_0x00af:
                r0 = move-exception
                goto L_0x0057
            L_0x00b1:
                r0 = move-exception
                goto L_0x005c
            L_0x00b3:
                r2 = move-exception
                goto L_0x00a7
            L_0x00b5:
                r1 = move-exception
                goto L_0x00ac
            L_0x00b7:
                r0 = move-exception
                goto L_0x00a2
            L_0x00b9:
                r0 = move-exception
                r9 = r2
                r2 = r1
                r1 = r9
                goto L_0x00a2
            L_0x00be:
                r0 = move-exception
                r2 = r1
                goto L_0x004f
            */
            throw new UnsupportedOperationException("Method not decompiled: a.a.d.a.a.b.a.d():void");
        }

        public void a() {
            try {
                b.n.fine(String.format("xhr open %s: %s", this.f118a, this.f119b));
                this.e = (HttpURLConnection) new URL(this.f119b).openConnection();
                this.e.setRequestMethod(this.f118a);
                this.e.setConnectTimeout(10000);
                if (this.e instanceof HttpsURLConnection) {
                    if (this.d != null) {
                        ((HttpsURLConnection) this.e).setSSLSocketFactory(this.d.getSocketFactory());
                    }
                    if (this.f != null) {
                        ((HttpsURLConnection) this.e).setHostnameVerifier(this.f);
                    }
                }
                TreeMap treeMap = new TreeMap(String.CASE_INSENSITIVE_ORDER);
                if ("POST".equals(this.f118a)) {
                    this.e.setDoOutput(true);
                    treeMap.put("Content-type", new LinkedList(Arrays.asList("application/octet-stream")));
                }
                a(treeMap);
                for (Map.Entry entry : treeMap.entrySet()) {
                    for (String addRequestProperty : (List) entry.getValue()) {
                        this.e.addRequestProperty((String) entry.getKey(), addRequestProperty);
                    }
                }
                b.n.fine(String.format("sending xhr with url %s | data %s", this.f119b, this.f120c));
                new Thread(new Runnable() {
                    /* JADX WARNING: Removed duplicated region for block: B:27:0x0082 A[SYNTHETIC, Splitter:B:27:0x0082] */
                    /* JADX WARNING: Removed duplicated region for block: B:33:0x008c A[SYNTHETIC, Splitter:B:33:0x008c] */
                    /* JADX WARNING: Removed duplicated region for block: B:46:? A[RETURN, SYNTHETIC] */
                    /* JADX WARNING: Unknown top exception splitter block from list: {B:24:0x007b=Splitter:B:24:0x007b, B:16:0x006c=Splitter:B:16:0x006c} */
                    /* Code decompiled incorrectly, please refer to instructions dump. */
                    public void run() {
                        /*
                            r4 = this;
                            r2 = 0
                            a.a.d.a.a.b$a r0 = r9     // Catch:{ IOException -> 0x0098, NullPointerException -> 0x0079, all -> 0x0088 }
                            byte[] r0 = r0.f120c     // Catch:{ IOException -> 0x0098, NullPointerException -> 0x0079, all -> 0x0088 }
                            if (r0 == 0) goto L_0x009b
                            a.a.d.a.a.b$a r0 = a.a.d.a.a.b.a.this     // Catch:{ IOException -> 0x0098, NullPointerException -> 0x0079, all -> 0x0088 }
                            java.net.HttpURLConnection r0 = r0.e     // Catch:{ IOException -> 0x0098, NullPointerException -> 0x0079, all -> 0x0088 }
                            a.a.d.a.a.b$a r1 = r9     // Catch:{ IOException -> 0x0098, NullPointerException -> 0x0079, all -> 0x0088 }
                            byte[] r1 = r1.f120c     // Catch:{ IOException -> 0x0098, NullPointerException -> 0x0079, all -> 0x0088 }
                            int r1 = r1.length     // Catch:{ IOException -> 0x0098, NullPointerException -> 0x0079, all -> 0x0088 }
                            r0.setFixedLengthStreamingMode(r1)     // Catch:{ IOException -> 0x0098, NullPointerException -> 0x0079, all -> 0x0088 }
                            java.io.BufferedOutputStream r1 = new java.io.BufferedOutputStream     // Catch:{ IOException -> 0x0098, NullPointerException -> 0x0079, all -> 0x0088 }
                            a.a.d.a.a.b$a r0 = a.a.d.a.a.b.a.this     // Catch:{ IOException -> 0x0098, NullPointerException -> 0x0079, all -> 0x0088 }
                            java.net.HttpURLConnection r0 = r0.e     // Catch:{ IOException -> 0x0098, NullPointerException -> 0x0079, all -> 0x0088 }
                            java.io.OutputStream r0 = r0.getOutputStream()     // Catch:{ IOException -> 0x0098, NullPointerException -> 0x0079, all -> 0x0088 }
                            r1.<init>(r0)     // Catch:{ IOException -> 0x0098, NullPointerException -> 0x0079, all -> 0x0088 }
                            a.a.d.a.a.b$a r0 = r9     // Catch:{ IOException -> 0x006b, NullPointerException -> 0x0096 }
                            byte[] r0 = r0.f120c     // Catch:{ IOException -> 0x006b, NullPointerException -> 0x0096 }
                            r1.write(r0)     // Catch:{ IOException -> 0x006b, NullPointerException -> 0x0096 }
                            r1.flush()     // Catch:{ IOException -> 0x006b, NullPointerException -> 0x0096 }
                        L_0x0034:
                            a.a.d.a.a.b$a r0 = a.a.d.a.a.b.a.this     // Catch:{ IOException -> 0x006b, NullPointerException -> 0x0096 }
                            java.net.HttpURLConnection r0 = r0.e     // Catch:{ IOException -> 0x006b, NullPointerException -> 0x0096 }
                            java.util.Map r0 = r0.getHeaderFields()     // Catch:{ IOException -> 0x006b, NullPointerException -> 0x0096 }
                            a.a.d.a.a.b$a r2 = r9     // Catch:{ IOException -> 0x006b, NullPointerException -> 0x0096 }
                            r2.b(r0)     // Catch:{ IOException -> 0x006b, NullPointerException -> 0x0096 }
                            a.a.d.a.a.b$a r0 = a.a.d.a.a.b.a.this     // Catch:{ IOException -> 0x006b, NullPointerException -> 0x0096 }
                            java.net.HttpURLConnection r0 = r0.e     // Catch:{ IOException -> 0x006b, NullPointerException -> 0x0096 }
                            int r0 = r0.getResponseCode()     // Catch:{ IOException -> 0x006b, NullPointerException -> 0x0096 }
                            r2 = 200(0xc8, float:2.8E-43)
                            if (r2 != r0) goto L_0x005c
                            a.a.d.a.a.b$a r0 = r9     // Catch:{ IOException -> 0x006b, NullPointerException -> 0x0096 }
                            r0.d()     // Catch:{ IOException -> 0x006b, NullPointerException -> 0x0096 }
                        L_0x0056:
                            if (r1 == 0) goto L_0x005b
                            r1.close()     // Catch:{ IOException -> 0x0090 }
                        L_0x005b:
                            return
                        L_0x005c:
                            a.a.d.a.a.b$a r2 = r9     // Catch:{ IOException -> 0x006b, NullPointerException -> 0x0096 }
                            java.io.IOException r3 = new java.io.IOException     // Catch:{ IOException -> 0x006b, NullPointerException -> 0x0096 }
                            java.lang.String r0 = java.lang.Integer.toString(r0)     // Catch:{ IOException -> 0x006b, NullPointerException -> 0x0096 }
                            r3.<init>(r0)     // Catch:{ IOException -> 0x006b, NullPointerException -> 0x0096 }
                            r2.a(r3)     // Catch:{ IOException -> 0x006b, NullPointerException -> 0x0096 }
                            goto L_0x0056
                        L_0x006b:
                            r0 = move-exception
                        L_0x006c:
                            a.a.d.a.a.b$a r2 = r9     // Catch:{ all -> 0x0094 }
                            r2.a(r0)     // Catch:{ all -> 0x0094 }
                            if (r1 == 0) goto L_0x005b
                            r1.close()     // Catch:{ IOException -> 0x0077 }
                            goto L_0x005b
                        L_0x0077:
                            r0 = move-exception
                            goto L_0x005b
                        L_0x0079:
                            r0 = move-exception
                            r1 = r2
                        L_0x007b:
                            a.a.d.a.a.b$a r2 = r9     // Catch:{ all -> 0x0094 }
                            r2.a(r0)     // Catch:{ all -> 0x0094 }
                            if (r1 == 0) goto L_0x005b
                            r1.close()     // Catch:{ IOException -> 0x0086 }
                            goto L_0x005b
                        L_0x0086:
                            r0 = move-exception
                            goto L_0x005b
                        L_0x0088:
                            r0 = move-exception
                            r1 = r2
                        L_0x008a:
                            if (r1 == 0) goto L_0x008f
                            r1.close()     // Catch:{ IOException -> 0x0092 }
                        L_0x008f:
                            throw r0
                        L_0x0090:
                            r0 = move-exception
                            goto L_0x005b
                        L_0x0092:
                            r1 = move-exception
                            goto L_0x008f
                        L_0x0094:
                            r0 = move-exception
                            goto L_0x008a
                        L_0x0096:
                            r0 = move-exception
                            goto L_0x007b
                        L_0x0098:
                            r0 = move-exception
                            r1 = r2
                            goto L_0x006c
                        L_0x009b:
                            r1 = r2
                            goto L_0x0034
                        */
                        throw new UnsupportedOperationException("Method not decompiled: a.a.d.a.a.b.a.AnonymousClass1.run():void");
                    }
                }).start();
            } catch (IOException e2) {
                a(e2);
            }
        }
    }

    public b(d.a aVar) {
        super(aVar);
    }

    /* access modifiers changed from: protected */
    public a a(a.C0002a aVar) {
        if (aVar == null) {
            aVar = new a.C0002a();
        }
        aVar.f123a = h();
        aVar.d = this.j;
        aVar.e = this.l;
        a aVar2 = new a(aVar);
        aVar2.a("requestHeaders", new a.C0001a() {
            public void a(Object... objArr) {
                this.a("requestHeaders", objArr[0]);
            }
        }).a("responseHeaders", new a.C0001a() {
            public void a(final Object... objArr) {
                a.a.i.a.a(new Runnable() {
                    public void run() {
                        this.a("responseHeaders", objArr[0]);
                    }
                });
            }
        });
        return aVar2;
    }

    /* access modifiers changed from: protected */
    public void a(byte[] bArr, final Runnable runnable) {
        a.C0002a aVar = new a.C0002a();
        aVar.f124b = "POST";
        aVar.f125c = bArr;
        a a2 = a(aVar);
        a2.a("success", new a.C0001a() {
            public void a(Object... objArr) {
                a.a.i.a.a(new Runnable() {
                    public void run() {
                        runnable.run();
                    }
                });
            }
        });
        a2.a("error", new a.C0001a() {
            public void a(final Object... objArr) {
                a.a.i.a.a(new Runnable() {
                    public void run() {
                        d unused = this.a("xhr post error", (objArr.length <= 0 || !(objArr[0] instanceof Exception)) ? null : (Exception) objArr[0]);
                    }
                });
            }
        });
        a2.a();
        this.o = a2;
    }

    /* access modifiers changed from: protected */
    public void i() {
        n.fine("xhr poll");
        a k = k();
        k.a("data", new a.C0001a() {
            public void a(final Object... objArr) {
                a.a.i.a.a(new Runnable() {
                    /* JADX INFO: additional move instructions added (1) to help type inference */
                    /* JADX WARN: Type inference failed for: r0v7, types: [java.lang.Object[]] */
                    /* JADX WARN: Type inference failed for: r0v8 */
                    /* JADX WARNING: Multi-variable type inference failed */
                    /* Code decompiled incorrectly, please refer to instructions dump. */
                    public void run() {
                        /*
                            r2 = this;
                            java.lang.Object[] r0 = r2
                            int r0 = r0.length
                            if (r0 <= 0) goto L_0x0018
                            java.lang.Object[] r0 = r2
                            r1 = 0
                            r0 = r0[r1]
                        L_0x000a:
                            boolean r1 = r0 instanceof java.lang.String
                            if (r1 == 0) goto L_0x001a
                            a.a.d.a.a.b$5 r1 = a.a.d.a.a.b.AnonymousClass5.this
                            a.a.d.a.a.b r1 = r3
                            java.lang.String r0 = (java.lang.String) r0
                            r1.a(r0)
                        L_0x0017:
                            return
                        L_0x0018:
                            r0 = 0
                            goto L_0x000a
                        L_0x001a:
                            boolean r1 = r0 instanceof byte[]
                            if (r1 == 0) goto L_0x0017
                            a.a.d.a.a.b$5 r1 = a.a.d.a.a.b.AnonymousClass5.this
                            a.a.d.a.a.b r1 = r3
                            byte[] r0 = (byte[]) r0
                            byte[] r0 = (byte[]) r0
                            r1.a(r0)
                            goto L_0x0017
                        */
                        throw new UnsupportedOperationException("Method not decompiled: a.a.d.a.a.b.AnonymousClass5.AnonymousClass1.run():void");
                    }
                });
            }
        });
        k.a("error", new a.C0001a() {
            public void a(final Object... objArr) {
                a.a.i.a.a(new Runnable() {
                    public void run() {
                        d unused = this.a("xhr poll error", (objArr.length <= 0 || !(objArr[0] instanceof Exception)) ? null : (Exception) objArr[0]);
                    }
                });
            }
        });
        k.a();
        this.p = k;
    }

    /* access modifiers changed from: protected */
    public a k() {
        return a((a.C0002a) null);
    }
}
