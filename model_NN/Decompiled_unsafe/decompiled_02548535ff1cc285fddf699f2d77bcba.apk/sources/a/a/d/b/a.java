package a.a.d.b;

import java.nio.ByteBuffer;

class a {
    public static byte[] a(byte[][] bArr) {
        int i = 0;
        for (byte[] length : bArr) {
            i += length.length;
        }
        return a(bArr, i);
    }

    public static byte[] a(byte[][] bArr, int i) {
        if (bArr.length == 0) {
            return new byte[0];
        }
        if (bArr.length == 1) {
            return bArr[0];
        }
        ByteBuffer allocate = ByteBuffer.allocate(i);
        for (byte[] put : bArr) {
            allocate.put(put);
        }
        return allocate.array();
    }
}
