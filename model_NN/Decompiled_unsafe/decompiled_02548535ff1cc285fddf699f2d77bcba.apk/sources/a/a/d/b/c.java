package a.a.d.b;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class c {

    /* renamed from: a  reason: collision with root package name */
    private static final int f225a = String.valueOf(Integer.MAX_VALUE).length();

    /* renamed from: b  reason: collision with root package name */
    private static final Map<String, Integer> f226b = new HashMap<String, Integer>() {
        {
            put("open", 0);
            put("close", 1);
            put("ping", 2);
            put("pong", 3);
            put("message", 4);
            put("upgrade", 5);
            put("noop", 6);
        }
    };

    /* renamed from: c  reason: collision with root package name */
    private static final Map<Integer, String> f227c = new HashMap();
    private static b<String> d = new b<>("error", "parser error");

    public interface a<T> {
        boolean a(b<T> bVar, int i, int i2);
    }

    public interface b<T> {
        void a(Object obj);
    }

    static {
        for (Map.Entry next : f226b.entrySet()) {
            f227c.put(next.getValue(), next.getKey());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.d.b.c.a(java.lang.String, boolean):a.a.d.b.b<java.lang.String>
     arg types: [java.lang.String, int]
     candidates:
      a.a.d.b.c.a(a.a.d.b.b, a.a.d.b.c$b):void
      a.a.d.b.c.a(java.lang.String, a.a.d.b.c$a<java.lang.String>):void
      a.a.d.b.c.a(byte[], a.a.d.b.c$a):void
      a.a.d.b.c.a(a.a.d.b.b[], a.a.d.b.c$b<byte[]>):void
      a.a.d.b.c.a(java.lang.String, boolean):a.a.d.b.b<java.lang.String> */
    public static b<String> a(String str) {
        return a(str, false);
    }

    public static b<String> a(String str, boolean z) {
        int i;
        try {
            i = Character.getNumericValue(str.charAt(0));
        } catch (IndexOutOfBoundsException e) {
            i = -1;
        }
        if (z) {
            try {
                str = a.a.j.a.b(str);
            } catch (a.a.j.b e2) {
                return d;
            }
        }
        return (i < 0 || i >= f227c.size()) ? d : str.length() > 1 ? new b<>(f227c.get(Integer.valueOf(i)), str.substring(1)) : new b<>(f227c.get(Integer.valueOf(i)));
    }

    public static b<byte[]> a(byte[] bArr) {
        byte b2 = bArr[0];
        byte[] bArr2 = new byte[(bArr.length - 1)];
        System.arraycopy(bArr, 1, bArr2, 0, bArr2.length);
        return new b<>(f227c.get(Integer.valueOf(b2)), bArr2);
    }

    public static void a(b bVar, b bVar2) {
        a(bVar, false, bVar2);
    }

    public static void a(b bVar, boolean z, b bVar2) {
        if (bVar.f224b instanceof byte[]) {
            b(bVar, bVar2);
            return;
        }
        String valueOf = String.valueOf(f226b.get(bVar.f223a));
        if (bVar.f224b != null) {
            valueOf = valueOf + (z ? a.a.j.a.a(String.valueOf(bVar.f224b)) : String.valueOf(bVar.f224b));
        }
        bVar2.a(valueOf);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.d.b.c.a(java.lang.String, boolean):a.a.d.b.b<java.lang.String>
     arg types: [java.lang.String, int]
     candidates:
      a.a.d.b.c.a(a.a.d.b.b, a.a.d.b.c$b):void
      a.a.d.b.c.a(java.lang.String, a.a.d.b.c$a<java.lang.String>):void
      a.a.d.b.c.a(byte[], a.a.d.b.c$a):void
      a.a.d.b.c.a(a.a.d.b.b[], a.a.d.b.c$b<byte[]>):void
      a.a.d.b.c.a(java.lang.String, boolean):a.a.d.b.b<java.lang.String> */
    public static void a(String str, a<String> aVar) {
        int i;
        StringBuilder sb;
        if (str == null || str.length() == 0) {
            aVar.a(d, 0, 1);
            return;
        }
        StringBuilder sb2 = new StringBuilder();
        int length = str.length();
        int i2 = 0;
        while (i2 < length) {
            char charAt = str.charAt(i2);
            if (':' != charAt) {
                sb2.append(charAt);
                int i3 = i2;
                sb = sb2;
                i = i3;
            } else {
                try {
                    int parseInt = Integer.parseInt(sb2.toString());
                    try {
                        String substring = str.substring(i2 + 1, i2 + 1 + parseInt);
                        if (substring.length() != 0) {
                            b<String> a2 = a(substring, true);
                            if (d.f223a.equals(a2.f223a) && ((String) d.f224b).equals(a2.f224b)) {
                                aVar.a(d, 0, 1);
                                return;
                            } else if (!aVar.a(a2, i2 + parseInt, length)) {
                                return;
                            }
                        }
                        i = i2 + parseInt;
                        sb = new StringBuilder();
                    } catch (IndexOutOfBoundsException e) {
                        aVar.a(d, 0, 1);
                        return;
                    }
                } catch (NumberFormatException e2) {
                    aVar.a(d, 0, 1);
                    return;
                }
            }
            int i4 = i + 1;
            sb2 = sb;
            i2 = i4;
        }
        if (sb2.length() > 0) {
            aVar.a(d, 0, 1);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.d.b.c.a(java.lang.String, boolean):a.a.d.b.b<java.lang.String>
     arg types: [java.lang.String, int]
     candidates:
      a.a.d.b.c.a(a.a.d.b.b, a.a.d.b.c$b):void
      a.a.d.b.c.a(java.lang.String, a.a.d.b.c$a<java.lang.String>):void
      a.a.d.b.c.a(byte[], a.a.d.b.c$a):void
      a.a.d.b.c.a(a.a.d.b.b[], a.a.d.b.c$b<byte[]>):void
      a.a.d.b.c.a(java.lang.String, boolean):a.a.d.b.b<java.lang.String> */
    public static void a(byte[] bArr, a aVar) {
        boolean z;
        ByteBuffer wrap = ByteBuffer.wrap(bArr);
        ArrayList arrayList = new ArrayList();
        while (wrap.capacity() > 0) {
            StringBuilder sb = new StringBuilder();
            boolean z2 = (wrap.get(0) & 255) == 0;
            int i = 1;
            while (true) {
                byte b2 = wrap.get(i) & 255;
                if (b2 == 255) {
                    z = false;
                    break;
                } else if (sb.length() > f225a) {
                    z = true;
                    break;
                } else {
                    sb.append((int) b2);
                    i++;
                }
            }
            if (z) {
                aVar.a(d, 0, 1);
                return;
            }
            wrap.position(sb.length() + 1);
            ByteBuffer slice = wrap.slice();
            int parseInt = Integer.parseInt(sb.toString());
            slice.position(1);
            slice.limit(parseInt + 1);
            byte[] bArr2 = new byte[slice.remaining()];
            slice.get(bArr2);
            if (z2) {
                arrayList.add(b(bArr2));
            } else {
                arrayList.add(bArr2);
            }
            slice.clear();
            slice.position(parseInt + 1);
            wrap = slice.slice();
        }
        int size = arrayList.size();
        for (int i2 = 0; i2 < size; i2++) {
            Object obj = arrayList.get(i2);
            if (obj instanceof String) {
                aVar.a(a((String) obj, true), i2, size);
            } else if (obj instanceof byte[]) {
                aVar.a(a((byte[]) obj), i2, size);
            }
        }
    }

    public static void a(b[] bVarArr, b<byte[]> bVar) {
        if (bVarArr.length == 0) {
            bVar.a(new byte[0]);
            return;
        }
        final ArrayList arrayList = new ArrayList(bVarArr.length);
        for (b a2 : bVarArr) {
            a(a2, true, new b() {
                public void a(Object obj) {
                    if (obj instanceof String) {
                        String valueOf = String.valueOf(((String) obj).length());
                        byte[] bArr = new byte[(valueOf.length() + 2)];
                        bArr[0] = 0;
                        for (int i = 0; i < valueOf.length(); i++) {
                            bArr[i + 1] = (byte) Character.getNumericValue(valueOf.charAt(i));
                        }
                        bArr[bArr.length - 1] = -1;
                        arrayList.add(a.a(new byte[][]{bArr, c.c((String) obj)}));
                        return;
                    }
                    String valueOf2 = String.valueOf(((byte[]) obj).length);
                    byte[] bArr2 = new byte[(valueOf2.length() + 2)];
                    bArr2[0] = 1;
                    for (int i2 = 0; i2 < valueOf2.length(); i2++) {
                        bArr2[i2 + 1] = (byte) Character.getNumericValue(valueOf2.charAt(i2));
                    }
                    bArr2[bArr2.length - 1] = -1;
                    arrayList.add(a.a(new byte[][]{bArr2, (byte[]) obj}));
                }
            });
        }
        bVar.a(a.a((byte[][]) arrayList.toArray(new byte[arrayList.size()][])));
    }

    private static String b(byte[] bArr) {
        StringBuilder sb = new StringBuilder();
        for (byte b2 : bArr) {
            sb.appendCodePoint(b2 & 255);
        }
        return sb.toString();
    }

    private static void b(b<byte[]> bVar, b<byte[]> bVar2) {
        byte[] bArr = (byte[]) bVar.f224b;
        byte[] bArr2 = new byte[(bArr.length + 1)];
        bArr2[0] = f226b.get(bVar.f223a).byteValue();
        System.arraycopy(bArr, 0, bArr2, 1, bArr.length);
        bVar2.a(bArr2);
    }

    /* access modifiers changed from: private */
    public static byte[] c(String str) {
        int length = str.length();
        byte[] bArr = new byte[length];
        for (int i = 0; i < length; i++) {
            bArr[i] = (byte) Character.codePointAt(str, i);
        }
        return bArr;
    }
}
