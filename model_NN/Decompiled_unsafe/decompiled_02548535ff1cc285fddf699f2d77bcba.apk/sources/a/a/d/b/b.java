package a.a.d.b;

public class b<T> {

    /* renamed from: a  reason: collision with root package name */
    public String f223a;

    /* renamed from: b  reason: collision with root package name */
    public T f224b;

    public b(String str) {
        this(str, null);
    }

    public b(String str, T t) {
        this.f223a = str;
        this.f224b = t;
    }
}
