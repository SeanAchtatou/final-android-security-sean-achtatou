package a.a.c;

import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentMap;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private ConcurrentMap<String, ConcurrentLinkedQueue<C0001a>> f72a = new ConcurrentHashMap();

    /* renamed from: a.a.c.a$a  reason: collision with other inner class name */
    public interface C0001a {
        void a(Object... objArr);
    }

    private class b implements C0001a {

        /* renamed from: a  reason: collision with root package name */
        public final String f73a;

        /* renamed from: b  reason: collision with root package name */
        public final C0001a f74b;

        public b(String str, C0001a aVar) {
            this.f73a = str;
            this.f74b = aVar;
        }

        public void a(Object... objArr) {
            a.this.c(this.f73a, this);
            this.f74b.a(objArr);
        }
    }

    private static boolean a(C0001a aVar, C0001a aVar2) {
        if (aVar.equals(aVar2)) {
            return true;
        }
        if (aVar2 instanceof b) {
            return aVar.equals(((b) aVar2).f74b);
        }
        return false;
    }

    public a a(String str, C0001a aVar) {
        ConcurrentLinkedQueue concurrentLinkedQueue;
        ConcurrentLinkedQueue concurrentLinkedQueue2 = this.f72a.get(str);
        if (concurrentLinkedQueue2 == null && (concurrentLinkedQueue2 = this.f72a.putIfAbsent(str, (concurrentLinkedQueue = new ConcurrentLinkedQueue()))) == null) {
            concurrentLinkedQueue2 = concurrentLinkedQueue;
        }
        concurrentLinkedQueue2.add(aVar);
        return this;
    }

    public a a(String str, Object... objArr) {
        ConcurrentLinkedQueue concurrentLinkedQueue = this.f72a.get(str);
        if (concurrentLinkedQueue != null) {
            Iterator it = concurrentLinkedQueue.iterator();
            while (it.hasNext()) {
                ((C0001a) it.next()).a(objArr);
            }
        }
        return this;
    }

    public a b(String str) {
        this.f72a.remove(str);
        return this;
    }

    public a b(String str, C0001a aVar) {
        a(str, new b(str, aVar));
        return this;
    }

    public a c(String str, C0001a aVar) {
        ConcurrentLinkedQueue concurrentLinkedQueue = this.f72a.get(str);
        if (concurrentLinkedQueue != null) {
            Iterator it = concurrentLinkedQueue.iterator();
            while (true) {
                if (it.hasNext()) {
                    if (a(aVar, (C0001a) it.next())) {
                        it.remove();
                        break;
                    }
                } else {
                    break;
                }
            }
        }
        return this;
    }

    public a g() {
        this.f72a.clear();
        return this;
    }
}
