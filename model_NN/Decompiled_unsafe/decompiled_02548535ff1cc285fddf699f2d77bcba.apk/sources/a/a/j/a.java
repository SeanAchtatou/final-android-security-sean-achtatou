package a.a.j;

import java.util.ArrayList;
import java.util.List;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private static int[] f247a;

    /* renamed from: b  reason: collision with root package name */
    private static int f248b;

    /* renamed from: c  reason: collision with root package name */
    private static int f249c;

    private static int a() {
        if (f249c > f248b) {
            throw new b("Invalid byte index");
        } else if (f249c == f248b) {
            return -1;
        } else {
            int i = f247a[f249c] & 255;
            f249c++;
            if ((i & 128) == 0) {
                return i;
            }
            if ((i & 224) == 192) {
                int b2 = ((i & 31) << 6) | b();
                if (b2 >= 128) {
                    return b2;
                }
                throw new b("Invalid continuation byte");
            } else if ((i & 240) == 224) {
                int b3 = ((i & 15) << 12) | (b() << 6) | b();
                if (b3 >= 2048) {
                    b(b3);
                    return b3;
                }
                throw new b("Invalid continuation byte");
            } else {
                if ((i & 248) == 240) {
                    int b4 = ((i & 15) << 18) | (b() << 12) | (b() << 6) | b();
                    if (b4 >= 65536 && b4 <= 1114111) {
                        return b4;
                    }
                }
                throw new b("Invalid continuation byte");
            }
        }
    }

    private static String a(int i) {
        StringBuilder sb = new StringBuilder();
        if ((i & -128) == 0) {
            return sb.append(Character.toChars(i)).toString();
        }
        if ((i & -2048) == 0) {
            sb.append(Character.toChars(((i >> 6) & 31) | 192));
        } else if ((-65536 & i) == 0) {
            b(i);
            sb.append(Character.toChars(((i >> 12) & 15) | 224));
            sb.append(a(i, 6));
        } else if ((-2097152 & i) == 0) {
            sb.append(Character.toChars(((i >> 18) & 7) | 240));
            sb.append(a(i, 12));
            sb.append(a(i, 6));
        }
        sb.append(Character.toChars((i & 63) | 128));
        return sb.toString();
    }

    public static String a(String str) {
        int[] c2 = c(str);
        int length = c2.length;
        int i = -1;
        StringBuilder sb = new StringBuilder();
        while (true) {
            i++;
            if (i >= length) {
                return sb.toString();
            }
            sb.append(a(c2[i]));
        }
    }

    private static String a(int[] iArr) {
        StringBuilder sb = new StringBuilder();
        for (int appendCodePoint : iArr) {
            sb.appendCodePoint(appendCodePoint);
        }
        return sb.toString();
    }

    private static char[] a(int i, int i2) {
        return Character.toChars(((i >> i2) & 63) | 128);
    }

    private static int[] a(List<Integer> list) {
        int size = list.size();
        int[] iArr = new int[size];
        for (int i = 0; i < size; i++) {
            iArr[i] = list.get(i).intValue();
        }
        return iArr;
    }

    private static int b() {
        if (f249c >= f248b) {
            throw new b("Invalid byte index");
        }
        int i = f247a[f249c] & 255;
        f249c++;
        if ((i & 192) == 128) {
            return i & 63;
        }
        throw new b("Invalid continuation byte");
    }

    public static String b(String str) {
        f247a = c(str);
        f248b = f247a.length;
        f249c = 0;
        ArrayList arrayList = new ArrayList();
        while (true) {
            int a2 = a();
            if (a2 == -1) {
                return a(a(arrayList));
            }
            arrayList.add(Integer.valueOf(a2));
        }
    }

    private static void b(int i) {
        if (i >= 55296 && i <= 57343) {
            throw new b("Lone surrogate U+" + Integer.toHexString(i).toUpperCase() + " is not a scalar value");
        }
    }

    private static int[] c(String str) {
        int i = 0;
        int length = str.length();
        int[] iArr = new int[str.codePointCount(0, length)];
        int i2 = 0;
        while (i < length) {
            int codePointAt = str.codePointAt(i);
            iArr[i2] = codePointAt;
            i += Character.charCount(codePointAt);
            i2++;
        }
        return iArr;
    }
}
