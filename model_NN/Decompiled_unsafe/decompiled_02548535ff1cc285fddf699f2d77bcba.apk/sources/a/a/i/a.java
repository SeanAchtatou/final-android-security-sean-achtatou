package a.a.i;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

public class a extends Thread {

    /* renamed from: a  reason: collision with root package name */
    private static final ThreadFactory f243a = new ThreadFactory() {
        public Thread newThread(Runnable runnable) {
            a unused = a.f244b = new a(runnable);
            a.f244b.setName("EventThread");
            return a.f244b;
        }
    };
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public static a f244b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public static ExecutorService f245c;
    /* access modifiers changed from: private */
    public static int d = 0;

    private a(Runnable runnable) {
        super(runnable);
    }

    public static void a(Runnable runnable) {
        if (a()) {
            runnable.run();
        } else {
            b(runnable);
        }
    }

    public static boolean a() {
        return currentThread() == f244b;
    }

    public static void b(final Runnable runnable) {
        ExecutorService executorService;
        synchronized (a.class) {
            d++;
            if (f245c == null) {
                f245c = Executors.newSingleThreadExecutor(f243a);
            }
            executorService = f245c;
        }
        executorService.execute(new Runnable() {
            public void run() {
                try {
                    runnable.run();
                    synchronized (a.class) {
                        a.c();
                        if (a.d == 0) {
                            a.f245c.shutdown();
                            ExecutorService unused = a.f245c = (ExecutorService) null;
                            a unused2 = a.f244b = (a) null;
                        }
                    }
                } catch (Throwable th) {
                    synchronized (a.class) {
                        a.c();
                        if (a.d == 0) {
                            a.f245c.shutdown();
                            ExecutorService unused3 = a.f245c = (ExecutorService) null;
                            a unused4 = a.f244b = (a) null;
                        }
                        throw th;
                    }
                }
            }
        });
    }

    static /* synthetic */ int c() {
        int i = d;
        d = i - 1;
        return i;
    }
}
