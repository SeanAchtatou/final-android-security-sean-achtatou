package a.a.k;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private static char[] f250a = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-_".toCharArray();

    /* renamed from: b  reason: collision with root package name */
    private static int f251b = f250a.length;

    /* renamed from: c  reason: collision with root package name */
    private static Map<Character, Integer> f252c = new HashMap(f251b);
    private static int d = 0;
    private static String e;

    static {
        for (int i = 0; i < f251b; i++) {
            f252c.put(Character.valueOf(f250a[i]), Integer.valueOf(i));
        }
    }

    public static String a() {
        String a2 = a(new Date().getTime());
        if (!a2.equals(e)) {
            d = 0;
            e = a2;
            return a2;
        }
        StringBuilder append = new StringBuilder().append(a2).append(".");
        int i = d;
        d = i + 1;
        return append.append(a((long) i)).toString();
    }

    public static String a(long j) {
        StringBuilder sb = new StringBuilder();
        do {
            sb.insert(0, f250a[(int) (j % ((long) f251b))]);
            j = (long) Math.floor((double) (j / ((long) f251b)));
        } while (j > 0);
        return sb.toString();
    }
}
