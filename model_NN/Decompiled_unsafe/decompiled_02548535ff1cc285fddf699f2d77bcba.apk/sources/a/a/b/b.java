package a.a.b;

import a.a.b.c;
import a.a.h.c;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;

public class b {

    /* renamed from: a  reason: collision with root package name */
    public static int f3a = c.f236a;

    /* renamed from: b  reason: collision with root package name */
    private static final Logger f4b = Logger.getLogger(b.class.getName());

    /* renamed from: c  reason: collision with root package name */
    private static final ConcurrentHashMap<String, c> f5c = new ConcurrentHashMap<>();

    public static class a extends c.C0000c {

        /* renamed from: a  reason: collision with root package name */
        public boolean f6a;

        /* renamed from: b  reason: collision with root package name */
        public boolean f7b = true;
    }

    private b() {
    }

    public static e a(String str, a aVar) {
        return a(new URI(str), aVar);
    }

    public static e a(URI uri, a aVar) {
        c cVar;
        if (aVar == null) {
            aVar = new a();
        }
        URL a2 = g.a(uri);
        try {
            URI uri2 = a2.toURI();
            String a3 = g.a(a2);
            if (aVar.f6a || !aVar.f7b || (f5c.containsKey(a3) && f5c.get(a3).e.containsKey(a2.getPath()))) {
                f4b.fine(String.format("ignoring socket cache for %s", uri2));
                cVar = new c(uri2, aVar);
            } else {
                if (!f5c.containsKey(a3)) {
                    f4b.fine(String.format("new io instance for %s", uri2));
                    f5c.putIfAbsent(a3, new c(uri2, aVar));
                }
                cVar = f5c.get(a3);
            }
            return cVar.a(a2.getPath());
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }
}
