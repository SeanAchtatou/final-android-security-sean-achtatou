package a.a.b;

import a.a.b.d;
import a.a.c.a;
import a.a.d.a.c;
import a.a.h.c;
import java.net.URI;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;

public class c extends a.a.c.a {

    /* renamed from: a  reason: collision with root package name */
    static SSLContext f8a;

    /* renamed from: b  reason: collision with root package name */
    static HostnameVerifier f9b;
    /* access modifiers changed from: private */
    public static final Logger f = Logger.getLogger(c.class.getName());

    /* renamed from: c  reason: collision with root package name */
    d f10c;
    a.a.d.a.c d;
    ConcurrentHashMap<String, e> e;
    private boolean g;
    /* access modifiers changed from: private */
    public boolean h;
    /* access modifiers changed from: private */
    public boolean i;
    /* access modifiers changed from: private */
    public boolean j;
    private int k;
    private long l;
    private long m;
    private double n;
    /* access modifiers changed from: private */
    public a.a.a.a o;
    /* access modifiers changed from: private */
    public long p;
    /* access modifiers changed from: private */
    public Set<e> q;
    private Date r;
    /* access modifiers changed from: private */
    public URI s;
    private List<a.a.h.b> t;
    /* access modifiers changed from: private */
    public Queue<d.a> u;
    /* access modifiers changed from: private */
    public C0000c v;
    private c.C0004c w;
    private c.b x;

    private static class a extends a.a.d.a.c {
        a(URI uri, c.a aVar) {
            super(uri, aVar);
        }
    }

    public interface b {
        void a(Exception exc);
    }

    /* renamed from: a.a.b.c$c  reason: collision with other inner class name */
    public static class C0000c extends c.a {

        /* renamed from: c  reason: collision with root package name */
        public boolean f45c = true;
        public int d;
        public long e;
        public long f;
        public double g;
        public long h = 20000;
    }

    enum d {
        CLOSED,
        OPENING,
        OPEN
    }

    public c() {
        this(null, null);
    }

    public c(URI uri, C0000c cVar) {
        this.q = new HashSet();
        cVar = cVar == null ? new C0000c() : cVar;
        if (cVar.o == null) {
            cVar.o = "/socket.io";
        }
        if (cVar.v == null) {
            cVar.v = f8a;
        }
        if (cVar.w == null) {
            cVar.w = f9b;
        }
        this.v = cVar;
        this.e = new ConcurrentHashMap<>();
        this.u = new LinkedList();
        a(cVar.f45c);
        a(cVar.d != 0 ? cVar.d : Integer.MAX_VALUE);
        a(cVar.e != 0 ? cVar.e : 1000);
        b(cVar.f != 0 ? cVar.f : 5000);
        a(cVar.g != 0.0d ? cVar.g : 0.5d);
        this.o = new a.a.a.a().a(a()).b(c()).a(b());
        c(cVar.h);
        this.f10c = d.CLOSED;
        this.s = uri;
        this.j = false;
        this.t = new ArrayList();
        this.w = new c.C0004c();
        this.x = new c.b();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.lang.Exception]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    /* access modifiers changed from: private */
    public void a(Exception exc) {
        f.log(Level.FINE, "error", (Throwable) exc);
        b("error", exc);
    }

    /* access modifiers changed from: private */
    public void a(byte[] bArr) {
        this.x.a(bArr);
    }

    /* access modifiers changed from: private */
    public void b(a.a.h.b bVar) {
        a("packet", bVar);
    }

    /* access modifiers changed from: private */
    public void b(String str, Object... objArr) {
        a(str, objArr);
        for (e a2 : this.e.values()) {
            a2.a(str, objArr);
        }
    }

    /* access modifiers changed from: private */
    public void c(String str) {
        this.x.a(str);
    }

    /* access modifiers changed from: private */
    public void d(String str) {
        f.fine("onclose");
        n();
        this.o.b();
        this.f10c = d.CLOSED;
        a("close", str);
        if (this.g && !this.h) {
            o();
        }
    }

    private void h() {
        for (e eVar : this.e.values()) {
            eVar.f54b = this.d.c();
        }
    }

    /* access modifiers changed from: private */
    public void i() {
        if (!this.i && this.g && this.o.c() == 0) {
            o();
        }
    }

    /* access modifiers changed from: private */
    public void j() {
        f.fine("open");
        n();
        this.f10c = d.OPEN;
        a("open", new Object[0]);
        a.a.d.a.c cVar = this.d;
        this.u.add(d.a(cVar, "data", new a.C0001a() {
            public void a(Object... objArr) {
                Object obj = objArr[0];
                if (obj instanceof String) {
                    c.this.c((String) obj);
                } else if (obj instanceof byte[]) {
                    c.this.a((byte[]) obj);
                }
            }
        }));
        this.u.add(d.a(cVar, "ping", new a.C0001a() {
            public void a(Object... objArr) {
                c.this.k();
            }
        }));
        this.u.add(d.a(cVar, "pong", new a.C0001a() {
            public void a(Object... objArr) {
                c.this.l();
            }
        }));
        this.u.add(d.a(cVar, "error", new a.C0001a() {
            public void a(Object... objArr) {
                c.this.a((Exception) objArr[0]);
            }
        }));
        this.u.add(d.a(cVar, "close", new a.C0001a() {
            public void a(Object... objArr) {
                c.this.d((String) objArr[0]);
            }
        }));
        this.u.add(d.a(this.x, c.b.f241a, new a.C0001a() {
            public void a(Object... objArr) {
                c.this.b((a.a.h.b) objArr[0]);
            }
        }));
    }

    /* access modifiers changed from: private */
    public void k() {
        this.r = new Date();
        b("ping", new Object[0]);
    }

    /* access modifiers changed from: private */
    public void l() {
        Object[] objArr = new Object[1];
        objArr[0] = Long.valueOf(this.r != null ? new Date().getTime() - this.r.getTime() : 0);
        b("pong", objArr);
    }

    /* access modifiers changed from: private */
    public void m() {
        if (!this.t.isEmpty() && !this.j) {
            a(this.t.remove(0));
        }
    }

    /* access modifiers changed from: private */
    public void n() {
        f.fine("cleanup");
        while (true) {
            d.a poll = this.u.poll();
            if (poll != null) {
                poll.a();
            } else {
                this.t.clear();
                this.j = false;
                this.r = null;
                this.x.a();
                return;
            }
        }
    }

    /* access modifiers changed from: private */
    public void o() {
        if (!this.i && !this.h) {
            if (this.o.c() >= this.k) {
                f.fine("reconnect failed");
                this.o.b();
                b("reconnect_failed", new Object[0]);
                this.i = false;
                return;
            }
            long a2 = this.o.a();
            f.fine(String.format("will wait %dms before reconnect attempt", Long.valueOf(a2)));
            this.i = true;
            final Timer timer = new Timer();
            timer.schedule(new TimerTask() {
                public void run() {
                    a.a.i.a.a(new Runnable() {
                        public void run() {
                            if (!this.h) {
                                c.f.fine("attempting reconnect");
                                int c2 = this.o.c();
                                this.b("reconnect_attempt", Integer.valueOf(c2));
                                this.b("reconnecting", Integer.valueOf(c2));
                                if (!this.h) {
                                    this.a(new b() {
                                        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                                         method: a.a.b.c.c(a.a.b.c, boolean):boolean
                                         arg types: [a.a.b.c, int]
                                         candidates:
                                          a.a.c.a.c(java.lang.String, a.a.c.a$a):a.a.c.a
                                          a.a.b.c.c(a.a.b.c, boolean):boolean */
                                        public void a(Exception exc) {
                                            if (exc != null) {
                                                c.f.fine("reconnect attempt error");
                                                boolean unused = this.i = false;
                                                this.o();
                                                this.b("reconnect_error", exc);
                                                return;
                                            }
                                            c.f.fine("reconnect success");
                                            this.p();
                                        }
                                    });
                                }
                            }
                        }
                    });
                }
            }, a2);
            this.u.add(new d.a() {
                public void a() {
                    timer.cancel();
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void p() {
        int c2 = this.o.c();
        this.i = false;
        this.o.b();
        h();
        b("reconnect", Integer.valueOf(c2));
    }

    public final long a() {
        return this.l;
    }

    public c a(double d2) {
        this.n = d2;
        if (this.o != null) {
            this.o.a(d2);
        }
        return this;
    }

    public c a(int i2) {
        this.k = i2;
        return this;
    }

    public c a(long j2) {
        this.l = j2;
        if (this.o != null) {
            this.o.a(j2);
        }
        return this;
    }

    public c a(final b bVar) {
        a.a.i.a.a(new Runnable() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: a.a.b.c.a(a.a.b.c, boolean):boolean
             arg types: [a.a.b.c, int]
             candidates:
              a.a.b.c.a(a.a.b.c, a.a.h.b):void
              a.a.b.c.a(a.a.b.c, java.lang.Exception):void
              a.a.b.c.a(a.a.b.c, java.lang.String):void
              a.a.b.c.a(a.a.b.c, byte[]):void
              a.a.c.a.a(a.a.c.a$a, a.a.c.a$a):boolean
              a.a.c.a.a(java.lang.String, a.a.c.a$a):a.a.c.a
              a.a.c.a.a(java.lang.String, java.lang.Object[]):a.a.c.a
              a.a.b.c.a(a.a.b.c, boolean):boolean */
            public void run() {
                c.f.fine(String.format("readyState %s", c.this.f10c));
                if (c.this.f10c != d.OPEN && c.this.f10c != d.OPENING) {
                    c.f.fine(String.format("opening %s", c.this.s));
                    c.this.d = new a(c.this.s, c.this.v);
                    final a.a.d.a.c cVar = c.this.d;
                    final c cVar2 = c.this;
                    c.this.f10c = d.OPENING;
                    boolean unused = c.this.h = false;
                    cVar.a("transport", new a.C0001a() {
                        public void a(Object... objArr) {
                            cVar2.a("transport", objArr);
                        }
                    });
                    final d.a a2 = d.a(cVar, "open", new a.C0001a() {
                        public void a(Object... objArr) {
                            cVar2.j();
                            if (bVar != null) {
                                bVar.a(null);
                            }
                        }
                    });
                    d.a a3 = d.a(cVar, "error", new a.C0001a() {
                        public void a(Object... objArr) {
                            Exception exc = objArr.length > 0 ? objArr[0] : null;
                            c.f.fine("connect_error");
                            cVar2.n();
                            cVar2.f10c = d.CLOSED;
                            cVar2.b("connect_error", exc);
                            if (bVar != null) {
                                bVar.a(new f("Connection error", exc instanceof Exception ? exc : null));
                            } else {
                                cVar2.i();
                            }
                        }
                    });
                    if (c.this.p >= 0) {
                        final long f = c.this.p;
                        c.f.fine(String.format("connection attempt will timeout after %d", Long.valueOf(f)));
                        final Timer timer = new Timer();
                        timer.schedule(new TimerTask() {
                            public void run() {
                                a.a.i.a.a(new Runnable() {
                                    public void run() {
                                        c.f.fine(String.format("connect attempt timed out after %d", Long.valueOf(f)));
                                        a2.a();
                                        cVar.b();
                                        cVar.a("error", new f("timeout"));
                                        cVar2.b("connect_timeout", Long.valueOf(f));
                                    }
                                });
                            }
                        }, f);
                        c.this.u.add(new d.a() {
                            public void a() {
                                timer.cancel();
                            }
                        });
                    }
                    c.this.u.add(a2);
                    c.this.u.add(a3);
                    c.this.d.a();
                }
            }
        });
        return this;
    }

    public c a(boolean z) {
        this.g = z;
        return this;
    }

    public e a(String str) {
        e eVar = this.e.get(str);
        if (eVar != null) {
            return eVar;
        }
        final e eVar2 = new e(this, str);
        e putIfAbsent = this.e.putIfAbsent(str, eVar2);
        if (putIfAbsent != null) {
            return putIfAbsent;
        }
        eVar2.a("connecting", new a.C0001a() {
            public void a(Object... objArr) {
                this.q.add(eVar2);
            }
        });
        eVar2.a("connect", new a.C0001a() {
            public void a(Object... objArr) {
                eVar2.f54b = this.d.c();
            }
        });
        return eVar2;
    }

    /* access modifiers changed from: package-private */
    public void a(e eVar) {
        this.q.remove(eVar);
        if (this.q.isEmpty()) {
            e();
        }
    }

    /* access modifiers changed from: package-private */
    public void a(a.a.h.b bVar) {
        f.fine(String.format("writing packet %s", bVar));
        if (!this.j) {
            this.j = true;
            this.w.a(bVar, new c.C0004c.a() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: a.a.b.c.b(a.a.b.c, boolean):boolean
                 arg types: [a.a.b.c, int]
                 candidates:
                  a.a.b.c.b(a.a.b.c, java.lang.String):void
                  a.a.b.c.b(java.lang.String, java.lang.Object[]):void
                  a.a.c.a.b(java.lang.String, a.a.c.a$a):a.a.c.a
                  a.a.b.c.b(a.a.b.c, boolean):boolean */
                public void a(Object[] objArr) {
                    for (Object obj : objArr) {
                        if (obj instanceof String) {
                            this.d.a((String) obj);
                        } else if (obj instanceof byte[]) {
                            this.d.a((byte[]) obj);
                        }
                    }
                    boolean unused = this.j = false;
                    this.m();
                }
            });
            return;
        }
        this.t.add(bVar);
    }

    public final double b() {
        return this.n;
    }

    public c b(long j2) {
        this.m = j2;
        if (this.o != null) {
            this.o.b(j2);
        }
        return this;
    }

    public final long c() {
        return this.m;
    }

    public c c(long j2) {
        this.p = j2;
        return this;
    }

    public c d() {
        return a((b) null);
    }

    /* access modifiers changed from: package-private */
    public void e() {
        f.fine("disconnect");
        this.h = true;
        this.i = false;
        if (this.f10c != d.OPEN) {
            n();
        }
        this.o.b();
        this.f10c = d.CLOSED;
        if (this.d != null) {
            this.d.b();
        }
    }
}
