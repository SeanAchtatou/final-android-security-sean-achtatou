package a.a.b;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.regex.Pattern;

public class g {

    /* renamed from: a  reason: collision with root package name */
    private static Pattern f70a = Pattern.compile("^http|ws$");

    /* renamed from: b  reason: collision with root package name */
    private static Pattern f71b = Pattern.compile("^(http|ws)s$");

    public static String a(URL url) {
        String protocol = url.getProtocol();
        int port = url.getPort();
        if (port == -1) {
            if (f70a.matcher(protocol).matches()) {
                port = 80;
            } else if (f71b.matcher(protocol).matches()) {
                port = 443;
            }
        }
        return protocol + "://" + url.getHost() + ":" + port;
    }

    public static URL a(URI uri) {
        String scheme = uri.getScheme();
        if (scheme == null || !scheme.matches("^https?|wss?$")) {
            scheme = "https";
        }
        int port = uri.getPort();
        if (port == -1) {
            if (f70a.matcher(scheme).matches()) {
                port = 80;
            } else if (f71b.matcher(scheme).matches()) {
                port = 443;
            }
        }
        String rawPath = uri.getRawPath();
        if (rawPath == null || rawPath.length() == 0) {
            rawPath = "/";
        }
        String rawUserInfo = uri.getRawUserInfo();
        String rawQuery = uri.getRawQuery();
        String rawFragment = uri.getRawFragment();
        try {
            return new URL(scheme + "://" + (rawUserInfo != null ? rawUserInfo + "@" : "") + uri.getHost() + (port != -1 ? ":" + port : "") + rawPath + (rawQuery != null ? "?" + rawQuery : "") + (rawFragment != null ? "#" + rawFragment : ""));
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }
}
