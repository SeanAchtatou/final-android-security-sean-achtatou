package a.a.b;

import a.a.b.c;
import a.a.b.d;
import a.a.c.a;
import a.a.h.b;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class e extends a {

    /* renamed from: a  reason: collision with root package name */
    protected static Map<String, Integer> f52a = new HashMap<String, Integer>() {
        {
            put("connect", 1);
            put("connect_error", 1);
            put("connect_timeout", 1);
            put("connecting", 1);
            put("disconnect", 1);
            put("error", 1);
            put("reconnect", 1);
            put("reconnect_attempt", 1);
            put("reconnect_failed", 1);
            put("reconnect_error", 1);
            put("reconnecting", 1);
            put("ping", 1);
            put("pong", 1);
        }
    };
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public static final Logger f53c = Logger.getLogger(e.class.getName());

    /* renamed from: b  reason: collision with root package name */
    String f54b;
    /* access modifiers changed from: private */
    public volatile boolean d;
    /* access modifiers changed from: private */
    public int e;
    /* access modifiers changed from: private */
    public String f;
    /* access modifiers changed from: private */
    public c g;
    /* access modifiers changed from: private */
    public Map<Integer, a> h = new HashMap();
    private Queue<d.a> i;
    private final Queue<List<Object>> j = new LinkedList();
    /* access modifiers changed from: private */
    public final Queue<b<JSONArray>> k = new LinkedList();

    public e(c cVar, String str) {
        this.g = cVar;
        this.f = str;
    }

    private a a(final int i2) {
        final boolean[] zArr = {false};
        return new a() {
            public void a(final Object... objArr) {
                a.a.i.a.a(new Runnable() {
                    public void run() {
                        if (!zArr[0]) {
                            zArr[0] = true;
                            e.f53c.fine(String.format("sending ack %s", objArr.length != 0 ? objArr : null));
                            JSONArray jSONArray = new JSONArray();
                            for (Object put : objArr) {
                                jSONArray.put(put);
                            }
                            b bVar = new b(a.a.f.a.a(jSONArray) ? (char) 6 : 3, jSONArray);
                            bVar.f234b = i2;
                            this.a(bVar);
                        }
                    }
                });
            }
        };
    }

    /* access modifiers changed from: private */
    public void a(b bVar) {
        bVar.f235c = this.f;
        this.g.a(bVar);
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        f53c.fine(String.format("close (%s)", str));
        this.d = false;
        this.f54b = null;
        a("disconnect", str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, org.json.JSONException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    private static Object[] a(JSONArray jSONArray) {
        Object obj;
        int length = jSONArray.length();
        Object[] objArr = new Object[length];
        for (int i2 = 0; i2 < length; i2++) {
            try {
                obj = jSONArray.get(i2);
            } catch (JSONException e2) {
                f53c.log(Level.WARNING, "An error occured while retrieving data from JSONArray", (Throwable) e2);
                obj = null;
            }
            if (JSONObject.NULL.equals(obj)) {
                obj = null;
            }
            objArr[i2] = obj;
        }
        return objArr;
    }

    /* access modifiers changed from: private */
    public static JSONArray b(JSONArray jSONArray, int i2) {
        Object obj;
        JSONArray jSONArray2 = new JSONArray();
        for (int i3 = 0; i3 < jSONArray.length(); i3++) {
            if (i3 != i2) {
                try {
                    obj = jSONArray.get(i3);
                } catch (JSONException e2) {
                    obj = null;
                }
                jSONArray2.put(obj);
            }
        }
        return jSONArray2;
    }

    /* access modifiers changed from: private */
    public void b(b<?> bVar) {
        if (this.f.equals(bVar.f235c)) {
            switch (bVar.f233a) {
                case 0:
                    i();
                    return;
                case 1:
                    k();
                    return;
                case 2:
                    c((b<JSONArray>) bVar);
                    return;
                case 3:
                    d((b<JSONArray>) bVar);
                    return;
                case 4:
                    a("error", bVar.d);
                    return;
                case 5:
                    c((b<JSONArray>) bVar);
                    return;
                case 6:
                    d((b<JSONArray>) bVar);
                    return;
                default:
                    return;
            }
        }
    }

    private void c(b<JSONArray> bVar) {
        ArrayList arrayList = new ArrayList(Arrays.asList(a((JSONArray) bVar.d)));
        f53c.fine(String.format("emitting event %s", arrayList));
        if (bVar.f234b >= 0) {
            f53c.fine("attaching ack callback to event");
            arrayList.add(a(bVar.f234b));
        }
        if (!this.d) {
            this.j.add(arrayList);
        } else if (!arrayList.isEmpty()) {
            super.a(arrayList.remove(0).toString(), arrayList.toArray());
        }
    }

    private void d(b<JSONArray> bVar) {
        a remove = this.h.remove(Integer.valueOf(bVar.f234b));
        if (remove != null) {
            f53c.fine(String.format("calling ack %s with %s", Integer.valueOf(bVar.f234b), bVar.d));
            remove.a(a((JSONArray) bVar.d));
            return;
        }
        f53c.fine(String.format("bad ack %s", Integer.valueOf(bVar.f234b)));
    }

    /* access modifiers changed from: private */
    public void f() {
        if (this.i == null) {
            final c cVar = this.g;
            this.i = new LinkedList<d.a>() {
                {
                    add(d.a(cVar, "open", new a.C0001a() {
                        public void a(Object... objArr) {
                            e.this.h();
                        }
                    }));
                    add(d.a(cVar, "packet", new a.C0001a() {
                        public void a(Object... objArr) {
                            e.this.b((b) objArr[0]);
                        }
                    }));
                    add(d.a(cVar, "close", new a.C0001a() {
                        public void a(Object... objArr) {
                            e.this.a(objArr.length > 0 ? (String) objArr[0] : null);
                        }
                    }));
                }
            };
        }
    }

    static /* synthetic */ int g(e eVar) {
        int i2 = eVar.e;
        eVar.e = i2 + 1;
        return i2;
    }

    /* access modifiers changed from: private */
    public void h() {
        f53c.fine("transport is open - connecting");
        if (!"/".equals(this.f)) {
            a(new b(0));
        }
    }

    private void i() {
        this.d = true;
        a("connect", new Object[0]);
        j();
    }

    private void j() {
        while (true) {
            List poll = this.j.poll();
            if (poll == null) {
                break;
            }
            super.a((String) poll.get(0), poll.toArray());
        }
        this.j.clear();
        while (true) {
            b poll2 = this.k.poll();
            if (poll2 != null) {
                a(poll2);
            } else {
                this.k.clear();
                return;
            }
        }
    }

    private void k() {
        f53c.fine(String.format("server disconnect (%s)", this.f));
        l();
        a("io server disconnect");
    }

    /* access modifiers changed from: private */
    public void l() {
        if (this.i != null) {
            for (d.a a2 : this.i) {
                a2.a();
            }
            this.i = null;
        }
        this.g.a(this);
    }

    public e a() {
        a.a.i.a.a(new Runnable() {
            public void run() {
                if (!e.this.d) {
                    e.this.f();
                    e.this.g.d();
                    if (c.d.OPEN == e.this.g.f10c) {
                        e.this.h();
                    }
                    e.this.a("connecting", new Object[0]);
                }
            }
        });
        return this;
    }

    public a a(final String str, final Object... objArr) {
        a.a.i.a.a(new Runnable() {
            public void run() {
                if (e.f52a.containsKey(str)) {
                    a unused = e.super.a(str, objArr);
                    return;
                }
                ArrayList<Object> arrayList = new ArrayList<>(objArr.length + 1);
                arrayList.add(str);
                arrayList.addAll(Arrays.asList(objArr));
                JSONArray jSONArray = new JSONArray();
                for (Object put : arrayList) {
                    jSONArray.put(put);
                }
                b bVar = new b(a.a.f.a.a(jSONArray) ? (char) 5 : 2, jSONArray);
                if (arrayList.get(arrayList.size() - 1) instanceof a) {
                    e.f53c.fine(String.format("emitting packet with ack id %d", Integer.valueOf(e.this.e)));
                    e.this.h.put(Integer.valueOf(e.this.e), (a) arrayList.remove(arrayList.size() - 1));
                    bVar.d = e.b(jSONArray, jSONArray.length() - 1);
                    bVar.f234b = e.g(e.this);
                }
                if (e.this.d) {
                    e.this.a(bVar);
                } else {
                    e.this.k.add(bVar);
                }
            }
        });
        return this;
    }

    public e b() {
        return a();
    }

    public e c() {
        a.a.i.a.a(new Runnable() {
            public void run() {
                if (e.this.d) {
                    e.f53c.fine(String.format("performing disconnect (%s)", e.this.f));
                    e.this.a(new b(1));
                }
                e.this.l();
                if (e.this.d) {
                    e.this.a("io client disconnect");
                }
            }
        });
        return this;
    }

    public boolean d() {
        return this.d;
    }
}
