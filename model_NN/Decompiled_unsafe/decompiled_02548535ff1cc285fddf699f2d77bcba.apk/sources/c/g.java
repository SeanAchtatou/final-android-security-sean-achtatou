package c;

import java.util.zip.Deflater;
import org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement;

public final class g implements q {

    /* renamed from: a  reason: collision with root package name */
    private final d f574a;

    /* renamed from: b  reason: collision with root package name */
    private final Deflater f575b;

    /* renamed from: c  reason: collision with root package name */
    private boolean f576c;

    g(d dVar, Deflater deflater) {
        if (dVar == null) {
            throw new IllegalArgumentException("source == null");
        } else if (deflater == null) {
            throw new IllegalArgumentException("inflater == null");
        } else {
            this.f574a = dVar;
            this.f575b = deflater;
        }
    }

    public g(q qVar, Deflater deflater) {
        this(l.a(qVar), deflater);
    }

    @IgnoreJRERequirement
    private void a(boolean z) {
        o e;
        c c2 = this.f574a.c();
        while (true) {
            e = c2.e(1);
            int deflate = z ? this.f575b.deflate(e.f597a, e.f599c, 2048 - e.f599c, 2) : this.f575b.deflate(e.f597a, e.f599c, 2048 - e.f599c);
            if (deflate > 0) {
                e.f599c += deflate;
                c2.f570b += (long) deflate;
                this.f574a.u();
            } else if (this.f575b.needsInput()) {
                break;
            }
        }
        if (e.f598b == e.f599c) {
            c2.f569a = e.a();
            p.a(e);
        }
    }

    public s a() {
        return this.f574a.a();
    }

    public void a_(c cVar, long j) {
        t.a(cVar.f570b, 0, j);
        while (j > 0) {
            o oVar = cVar.f569a;
            int min = (int) Math.min(j, (long) (oVar.f599c - oVar.f598b));
            this.f575b.setInput(oVar.f597a, oVar.f598b, min);
            a(false);
            cVar.f570b -= (long) min;
            oVar.f598b += min;
            if (oVar.f598b == oVar.f599c) {
                cVar.f569a = oVar.a();
                p.a(oVar);
            }
            j -= (long) min;
        }
    }

    /* access modifiers changed from: package-private */
    public void b() {
        this.f575b.finish();
        a(false);
    }

    public void close() {
        if (!this.f576c) {
            Throwable th = null;
            try {
                b();
            } catch (Throwable th2) {
                th = th2;
            }
            try {
                this.f575b.end();
                th = th;
            } catch (Throwable th3) {
                th = th3;
                if (th != null) {
                    th = th;
                }
            }
            try {
                this.f574a.close();
            } catch (Throwable th4) {
                if (th == null) {
                    th = th4;
                }
            }
            this.f576c = true;
            if (th != null) {
                t.a(th);
            }
        }
    }

    public void flush() {
        a(true);
        this.f574a.flush();
    }

    public String toString() {
        return "DeflaterSink(" + this.f574a + ")";
    }
}
