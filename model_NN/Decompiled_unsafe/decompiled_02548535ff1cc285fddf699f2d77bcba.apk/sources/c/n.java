package c;

import java.io.EOFException;

final class n implements e {

    /* renamed from: a  reason: collision with root package name */
    public final c f594a;

    /* renamed from: b  reason: collision with root package name */
    public final r f595b;

    /* renamed from: c  reason: collision with root package name */
    private boolean f596c;

    public n(r rVar) {
        this(rVar, new c());
    }

    public n(r rVar, c cVar) {
        if (rVar == null) {
            throw new IllegalArgumentException("source == null");
        }
        this.f594a = cVar;
        this.f595b = rVar;
    }

    public int a(byte[] bArr, int i, int i2) {
        t.a((long) bArr.length, (long) i, (long) i2);
        if (this.f594a.f570b == 0 && this.f595b.a(this.f594a, 2048) == -1) {
            return -1;
        }
        return this.f594a.a(bArr, i, (int) Math.min((long) i2, this.f594a.f570b));
    }

    public long a(byte b2) {
        return a(b2, 0);
    }

    public long a(byte b2, long j) {
        if (this.f596c) {
            throw new IllegalStateException("closed");
        }
        while (j >= this.f594a.f570b) {
            if (this.f595b.a(this.f594a, 2048) == -1) {
                return -1;
            }
        }
        do {
            long a2 = this.f594a.a(b2, j);
            if (a2 != -1) {
                return a2;
            }
            j = this.f594a.f570b;
        } while (this.f595b.a(this.f594a, 2048) != -1);
        return -1;
    }

    public long a(c cVar, long j) {
        if (cVar == null) {
            throw new IllegalArgumentException("sink == null");
        } else if (j < 0) {
            throw new IllegalArgumentException("byteCount < 0: " + j);
        } else if (this.f596c) {
            throw new IllegalStateException("closed");
        } else if (this.f594a.f570b == 0 && this.f595b.a(this.f594a, 2048) == -1) {
            return -1;
        } else {
            return this.f594a.a(cVar, Math.min(j, this.f594a.f570b));
        }
    }

    public s a() {
        return this.f595b.a();
    }

    public void a(long j) {
        if (!b(j)) {
            throw new EOFException();
        }
    }

    public void a(byte[] bArr) {
        try {
            a((long) bArr.length);
            this.f594a.a(bArr);
        } catch (EOFException e) {
            EOFException eOFException = e;
            int i = 0;
            while (this.f594a.f570b > 0) {
                int a2 = this.f594a.a(bArr, i, (int) this.f594a.f570b);
                if (a2 == -1) {
                    throw new AssertionError();
                }
                i += a2;
            }
            throw eOFException;
        }
    }

    public void b(c cVar, long j) {
        try {
            a(j);
            this.f594a.b(cVar, j);
        } catch (EOFException e) {
            cVar.a(this.f594a);
            throw e;
        }
    }

    public boolean b(long j) {
        if (j < 0) {
            throw new IllegalArgumentException("byteCount < 0: " + j);
        } else if (this.f596c) {
            throw new IllegalStateException("closed");
        } else {
            while (this.f594a.f570b < j) {
                if (this.f595b.a(this.f594a, 2048) == -1) {
                    return false;
                }
            }
            return true;
        }
    }

    public c c() {
        return this.f594a;
    }

    public f c(long j) {
        a(j);
        return this.f594a.c(j);
    }

    public void close() {
        if (!this.f596c) {
            this.f596c = true;
            this.f595b.close();
            this.f594a.s();
        }
    }

    public boolean f() {
        if (!this.f596c) {
            return this.f594a.f() && this.f595b.a(this.f594a, 2048) == -1;
        }
        throw new IllegalStateException("closed");
    }

    public byte[] f(long j) {
        a(j);
        return this.f594a.f(j);
    }

    public void g(long j) {
        if (this.f596c) {
            throw new IllegalStateException("closed");
        }
        while (j > 0) {
            if (this.f594a.f570b == 0 && this.f595b.a(this.f594a, 2048) == -1) {
                throw new EOFException();
            }
            long min = Math.min(j, this.f594a.b());
            this.f594a.g(min);
            j -= min;
        }
    }

    public byte h() {
        a(1);
        return this.f594a.h();
    }

    public short i() {
        a(2);
        return this.f594a.i();
    }

    public int j() {
        a(4);
        return this.f594a.j();
    }

    public long k() {
        a(8);
        return this.f594a.k();
    }

    public short l() {
        a(2);
        return this.f594a.l();
    }

    public int m() {
        a(4);
        return this.f594a.m();
    }

    public long n() {
        a(1);
        int i = 0;
        while (true) {
            if (!b((long) (i + 1))) {
                break;
            }
            byte b2 = this.f594a.b((long) i);
            if ((b2 >= 48 && b2 <= 57) || ((b2 >= 97 && b2 <= 102) || (b2 >= 65 && b2 <= 70))) {
                i++;
            } else if (i == 0) {
                throw new NumberFormatException(String.format("Expected leading [0-9a-fA-F] character but was %#x", Byte.valueOf(b2)));
            }
        }
        return this.f594a.n();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(long, long):long} */
    public String q() {
        long a2 = a((byte) 10);
        if (a2 != -1) {
            return this.f594a.e(a2);
        }
        c cVar = new c();
        this.f594a.a(cVar, 0, Math.min(32L, this.f594a.b()));
        throw new EOFException("\\n not found: size=" + this.f594a.b() + " content=" + cVar.o().d() + "...");
    }

    public byte[] r() {
        this.f594a.a(this.f595b);
        return this.f594a.r();
    }

    public String toString() {
        return "buffer(" + this.f595b + ")";
    }
}
