package c;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class l {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final Logger f585a = Logger.getLogger(l.class.getName());

    private l() {
    }

    public static d a(q qVar) {
        if (qVar != null) {
            return new m(qVar);
        }
        throw new IllegalArgumentException("sink == null");
    }

    public static e a(r rVar) {
        if (rVar != null) {
            return new n(rVar);
        }
        throw new IllegalArgumentException("source == null");
    }

    private static q a(final OutputStream outputStream, final s sVar) {
        if (outputStream == null) {
            throw new IllegalArgumentException("out == null");
        } else if (sVar != null) {
            return new q() {
                public s a() {
                    return sVar;
                }

                public void a_(c cVar, long j) {
                    t.a(cVar.f570b, 0, j);
                    while (j > 0) {
                        sVar.g();
                        o oVar = cVar.f569a;
                        int min = (int) Math.min(j, (long) (oVar.f599c - oVar.f598b));
                        outputStream.write(oVar.f597a, oVar.f598b, min);
                        oVar.f598b += min;
                        j -= (long) min;
                        cVar.f570b -= (long) min;
                        if (oVar.f598b == oVar.f599c) {
                            cVar.f569a = oVar.a();
                            p.a(oVar);
                        }
                    }
                }

                public void close() {
                    outputStream.close();
                }

                public void flush() {
                    outputStream.flush();
                }

                public String toString() {
                    return "sink(" + outputStream + ")";
                }
            };
        } else {
            throw new IllegalArgumentException("timeout == null");
        }
    }

    public static q a(Socket socket) {
        if (socket == null) {
            throw new IllegalArgumentException("socket == null");
        }
        a c2 = c(socket);
        return c2.a(a(socket.getOutputStream(), c2));
    }

    private static r a(final InputStream inputStream, final s sVar) {
        if (inputStream == null) {
            throw new IllegalArgumentException("in == null");
        } else if (sVar != null) {
            return new r() {
                public long a(c cVar, long j) {
                    if (j < 0) {
                        throw new IllegalArgumentException("byteCount < 0: " + j);
                    } else if (j == 0) {
                        return 0;
                    } else {
                        sVar.g();
                        o e = cVar.e(1);
                        int read = inputStream.read(e.f597a, e.f599c, (int) Math.min(j, (long) (2048 - e.f599c)));
                        if (read == -1) {
                            return -1;
                        }
                        e.f599c += read;
                        cVar.f570b += (long) read;
                        return (long) read;
                    }
                }

                public s a() {
                    return sVar;
                }

                public void close() {
                    inputStream.close();
                }

                public String toString() {
                    return "source(" + inputStream + ")";
                }
            };
        } else {
            throw new IllegalArgumentException("timeout == null");
        }
    }

    public static r b(Socket socket) {
        if (socket == null) {
            throw new IllegalArgumentException("socket == null");
        }
        a c2 = c(socket);
        return c2.a(a(socket.getInputStream(), c2));
    }

    private static a c(final Socket socket) {
        return new a() {
            /* access modifiers changed from: protected */
            public IOException a(IOException iOException) {
                SocketTimeoutException socketTimeoutException = new SocketTimeoutException("timeout");
                if (iOException != null) {
                    socketTimeoutException.initCause(iOException);
                }
                return socketTimeoutException;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
             arg types: [java.util.logging.Level, java.lang.String, java.lang.Exception]
             candidates:
              ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
              ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
              ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
              ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
             arg types: [java.util.logging.Level, java.lang.String, java.lang.AssertionError]
             candidates:
              ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
              ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
              ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
              ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
            /* access modifiers changed from: protected */
            public void a() {
                try {
                    socket.close();
                } catch (Exception e) {
                    l.f585a.log(Level.WARNING, "Failed to close timed out socket " + socket, (Throwable) e);
                } catch (AssertionError e2) {
                    if (e2.getCause() == null || e2.getMessage() == null || !e2.getMessage().contains("getsockname failed")) {
                        throw e2;
                    }
                    l.f585a.log(Level.WARNING, "Failed to close timed out socket " + socket, (Throwable) e2);
                }
            }
        };
    }
}
