package c;

import java.io.InterruptedIOException;
import java.util.concurrent.TimeUnit;

public class s {

    /* renamed from: b  reason: collision with root package name */
    public static final s f602b = new s() {
        public s a(long j) {
            return this;
        }

        public s a(long j, TimeUnit timeUnit) {
            return this;
        }

        public void g() {
        }
    };

    /* renamed from: a  reason: collision with root package name */
    private boolean f603a;

    /* renamed from: c  reason: collision with root package name */
    private long f604c;
    private long d;

    public s a(long j) {
        this.f603a = true;
        this.f604c = j;
        return this;
    }

    public s a(long j, TimeUnit timeUnit) {
        if (j < 0) {
            throw new IllegalArgumentException("timeout < 0: " + j);
        } else if (timeUnit == null) {
            throw new IllegalArgumentException("unit == null");
        } else {
            this.d = timeUnit.toNanos(j);
            return this;
        }
    }

    public long b_() {
        return this.d;
    }

    public boolean c_() {
        return this.f603a;
    }

    public long d() {
        if (this.f603a) {
            return this.f604c;
        }
        throw new IllegalStateException("No deadline");
    }

    public s d_() {
        this.d = 0;
        return this;
    }

    public s f() {
        this.f603a = false;
        return this;
    }

    public void g() {
        if (Thread.interrupted()) {
            throw new InterruptedIOException("thread interrupted");
        } else if (this.f603a && this.f604c - System.nanoTime() <= 0) {
            throw new InterruptedIOException("deadline reached");
        }
    }
}
