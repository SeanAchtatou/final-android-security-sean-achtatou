package c;

import java.io.EOFException;
import java.io.IOException;
import java.util.zip.CRC32;
import java.util.zip.Inflater;

public final class j implements r {

    /* renamed from: a  reason: collision with root package name */
    private int f579a = 0;

    /* renamed from: b  reason: collision with root package name */
    private final e f580b;

    /* renamed from: c  reason: collision with root package name */
    private final Inflater f581c;
    private final k d;
    private final CRC32 e = new CRC32();

    public j(r rVar) {
        if (rVar == null) {
            throw new IllegalArgumentException("source == null");
        }
        this.f581c = new Inflater(true);
        this.f580b = l.a(rVar);
        this.d = new k(this.f580b, this.f581c);
    }

    private void a(c cVar, long j, long j2) {
        o oVar = cVar.f569a;
        while (j >= ((long) (oVar.f599c - oVar.f598b))) {
            j -= (long) (oVar.f599c - oVar.f598b);
            oVar = oVar.f;
        }
        while (j2 > 0) {
            int i = (int) (((long) oVar.f598b) + j);
            int min = (int) Math.min((long) (oVar.f599c - i), j2);
            this.e.update(oVar.f597a, i, min);
            j2 -= (long) min;
            oVar = oVar.f;
            j = 0;
        }
    }

    private void a(String str, int i, int i2) {
        if (i2 != i) {
            throw new IOException(String.format("%s: actual 0x%08x != expected 0x%08x", str, Integer.valueOf(i2), Integer.valueOf(i)));
        }
    }

    private void b() {
        this.f580b.a(10);
        byte b2 = this.f580b.c().b(3L);
        boolean z = ((b2 >> 1) & 1) == 1;
        if (z) {
            a(this.f580b.c(), 0, 10);
        }
        a("ID1ID2", 8075, this.f580b.i());
        this.f580b.g(8);
        if (((b2 >> 2) & 1) == 1) {
            this.f580b.a(2);
            if (z) {
                a(this.f580b.c(), 0, 2);
            }
            short l = this.f580b.c().l();
            this.f580b.a((long) l);
            if (z) {
                a(this.f580b.c(), 0, (long) l);
            }
            this.f580b.g((long) l);
        }
        if (((b2 >> 3) & 1) == 1) {
            long a2 = this.f580b.a((byte) 0);
            if (a2 == -1) {
                throw new EOFException();
            }
            if (z) {
                a(this.f580b.c(), 0, 1 + a2);
            }
            this.f580b.g(1 + a2);
        }
        if (((b2 >> 4) & 1) == 1) {
            long a3 = this.f580b.a((byte) 0);
            if (a3 == -1) {
                throw new EOFException();
            }
            if (z) {
                a(this.f580b.c(), 0, 1 + a3);
            }
            this.f580b.g(1 + a3);
        }
        if (z) {
            a("FHCRC", this.f580b.l(), (short) ((int) this.e.getValue()));
            this.e.reset();
        }
    }

    private void c() {
        a("CRC", this.f580b.m(), (int) this.e.getValue());
        a("ISIZE", this.f580b.m(), this.f581c.getTotalOut());
    }

    public long a(c cVar, long j) {
        if (j < 0) {
            throw new IllegalArgumentException("byteCount < 0: " + j);
        } else if (j == 0) {
            return 0;
        } else {
            if (this.f579a == 0) {
                b();
                this.f579a = 1;
            }
            if (this.f579a == 1) {
                long j2 = cVar.f570b;
                long a2 = this.d.a(cVar, j);
                if (a2 != -1) {
                    a(cVar, j2, a2);
                    return a2;
                }
                this.f579a = 2;
            }
            if (this.f579a == 2) {
                c();
                this.f579a = 3;
                if (!this.f580b.f()) {
                    throw new IOException("gzip finished without exhausting source");
                }
            }
            return -1;
        }
    }

    public s a() {
        return this.f580b.a();
    }

    public void close() {
        this.d.close();
    }
}
