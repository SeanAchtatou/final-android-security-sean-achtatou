package c;

import java.io.EOFException;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public final class c implements d, e, Cloneable {

    /* renamed from: c  reason: collision with root package name */
    private static final byte[] f568c = {48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 97, 98, 99, 100, 101, 102};

    /* renamed from: a  reason: collision with root package name */
    o f569a;

    /* renamed from: b  reason: collision with root package name */
    long f570b;

    public int a(byte[] bArr, int i, int i2) {
        t.a((long) bArr.length, (long) i, (long) i2);
        o oVar = this.f569a;
        if (oVar == null) {
            return -1;
        }
        int min = Math.min(i2, oVar.f599c - oVar.f598b);
        System.arraycopy(oVar.f597a, oVar.f598b, bArr, i, min);
        oVar.f598b += min;
        this.f570b -= (long) min;
        if (oVar.f598b != oVar.f599c) {
            return min;
        }
        this.f569a = oVar.a();
        p.a(oVar);
        return min;
    }

    public long a(byte b2) {
        return a(b2, 0);
    }

    public long a(byte b2, long j) {
        if (j < 0) {
            throw new IllegalArgumentException("fromIndex < 0");
        }
        o oVar = this.f569a;
        if (oVar == null) {
            return -1;
        }
        o oVar2 = oVar;
        long j2 = 0;
        do {
            int i = oVar2.f599c - oVar2.f598b;
            if (j >= ((long) i)) {
                j -= (long) i;
            } else {
                byte[] bArr = oVar2.f597a;
                int i2 = oVar2.f599c;
                for (int i3 = (int) (((long) oVar2.f598b) + j); i3 < i2; i3++) {
                    if (bArr[i3] == b2) {
                        return (j2 + ((long) i3)) - ((long) oVar2.f598b);
                    }
                }
                j = 0;
            }
            j2 += (long) i;
            oVar2 = oVar2.f;
        } while (oVar2 != this.f569a);
        return -1;
    }

    public long a(c cVar, long j) {
        if (cVar == null) {
            throw new IllegalArgumentException("sink == null");
        } else if (j < 0) {
            throw new IllegalArgumentException("byteCount < 0: " + j);
        } else if (this.f570b == 0) {
            return -1;
        } else {
            if (j > this.f570b) {
                j = this.f570b;
            }
            cVar.a_(this, j);
            return j;
        }
    }

    public long a(r rVar) {
        if (rVar == null) {
            throw new IllegalArgumentException("source == null");
        }
        long j = 0;
        while (true) {
            long a2 = rVar.a(this, 2048);
            if (a2 == -1) {
                return j;
            }
            j += a2;
        }
    }

    public c a(int i) {
        if (i < 128) {
            h(i);
        } else if (i < 2048) {
            h((i >> 6) | 192);
            h((i & 63) | 128);
        } else if (i < 65536) {
            if (i < 55296 || i > 57343) {
                h((i >> 12) | 224);
                h(((i >> 6) & 63) | 128);
                h((i & 63) | 128);
            } else {
                throw new IllegalArgumentException("Unexpected code point: " + Integer.toHexString(i));
            }
        } else if (i <= 1114111) {
            h((i >> 18) | 240);
            h(((i >> 12) & 63) | 128);
            h(((i >> 6) & 63) | 128);
            h((i & 63) | 128);
        } else {
            throw new IllegalArgumentException("Unexpected code point: " + Integer.toHexString(i));
        }
        return this;
    }

    public c a(c cVar, long j, long j2) {
        if (cVar == null) {
            throw new IllegalArgumentException("out == null");
        }
        t.a(this.f570b, j, j2);
        if (j2 != 0) {
            cVar.f570b += j2;
            o oVar = this.f569a;
            while (j >= ((long) (oVar.f599c - oVar.f598b))) {
                j -= (long) (oVar.f599c - oVar.f598b);
                oVar = oVar.f;
            }
            while (j2 > 0) {
                o oVar2 = new o(oVar);
                oVar2.f598b = (int) (((long) oVar2.f598b) + j);
                oVar2.f599c = Math.min(oVar2.f598b + ((int) j2), oVar2.f599c);
                if (cVar.f569a == null) {
                    oVar2.g = oVar2;
                    oVar2.f = oVar2;
                    cVar.f569a = oVar2;
                } else {
                    cVar.f569a.g.a(oVar2);
                }
                j2 -= (long) (oVar2.f599c - oVar2.f598b);
                oVar = oVar.f;
                j = 0;
            }
        }
        return this;
    }

    /* renamed from: a */
    public c b(f fVar) {
        if (fVar == null) {
            throw new IllegalArgumentException("byteString == null");
        }
        fVar.a(this);
        return this;
    }

    /* renamed from: a */
    public c b(String str) {
        return a(str, 0, str.length());
    }

    /* JADX WARNING: CFG modification limit reached, blocks count: 152 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public c.c a(java.lang.String r10, int r11, int r12) {
        /*
            r9 = this;
            r8 = 57343(0xdfff, float:8.0355E-41)
            r7 = 128(0x80, float:1.794E-43)
            if (r10 != 0) goto L_0x000f
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "string == null"
            r0.<init>(r1)
            throw r0
        L_0x000f:
            if (r11 >= 0) goto L_0x002a
            java.lang.IllegalAccessError r0 = new java.lang.IllegalAccessError
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "beginIndex < 0: "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r11)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x002a:
            if (r12 >= r11) goto L_0x004f
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "endIndex < beginIndex: "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r12)
            java.lang.String r2 = " < "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r11)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x004f:
            int r0 = r10.length()
            if (r12 <= r0) goto L_0x0090
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "endIndex > string.length: "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r12)
            java.lang.String r2 = " > "
            java.lang.StringBuilder r1 = r1.append(r2)
            int r2 = r10.length()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x007c:
            r0 = 0
        L_0x007d:
            r2 = 56319(0xdbff, float:7.892E-41)
            if (r1 > r2) goto L_0x0089
            r2 = 56320(0xdc00, float:7.8921E-41)
            if (r0 < r2) goto L_0x0089
            if (r0 <= r8) goto L_0x0114
        L_0x0089:
            r0 = 63
            r9.h(r0)
            int r11 = r11 + 1
        L_0x0090:
            if (r11 >= r12) goto L_0x0145
            char r1 = r10.charAt(r11)
            if (r1 >= r7) goto L_0x00d2
            r0 = 1
            c.o r2 = r9.e(r0)
            byte[] r3 = r2.f597a
            int r0 = r2.f599c
            int r4 = r0 - r11
            int r0 = 2048 - r4
            int r5 = java.lang.Math.min(r12, r0)
            int r0 = r11 + 1
            int r6 = r4 + r11
            byte r1 = (byte) r1
            r3[r6] = r1
        L_0x00b0:
            if (r0 >= r5) goto L_0x00b8
            char r6 = r10.charAt(r0)
            if (r6 < r7) goto L_0x00ca
        L_0x00b8:
            int r1 = r0 + r4
            int r3 = r2.f599c
            int r1 = r1 - r3
            int r3 = r2.f599c
            int r3 = r3 + r1
            r2.f599c = r3
            long r2 = r9.f570b
            long r4 = (long) r1
            long r2 = r2 + r4
            r9.f570b = r2
        L_0x00c8:
            r11 = r0
            goto L_0x0090
        L_0x00ca:
            int r1 = r0 + 1
            int r0 = r0 + r4
            byte r6 = (byte) r6
            r3[r0] = r6
            r0 = r1
            goto L_0x00b0
        L_0x00d2:
            r0 = 2048(0x800, float:2.87E-42)
            if (r1 >= r0) goto L_0x00e7
            int r0 = r1 >> 6
            r0 = r0 | 192(0xc0, float:2.69E-43)
            r9.h(r0)
            r0 = r1 & 63
            r0 = r0 | 128(0x80, float:1.794E-43)
            r9.h(r0)
            int r0 = r11 + 1
            goto L_0x00c8
        L_0x00e7:
            r0 = 55296(0xd800, float:7.7486E-41)
            if (r1 < r0) goto L_0x00ee
            if (r1 <= r8) goto L_0x0108
        L_0x00ee:
            int r0 = r1 >> 12
            r0 = r0 | 224(0xe0, float:3.14E-43)
            r9.h(r0)
            int r0 = r1 >> 6
            r0 = r0 & 63
            r0 = r0 | 128(0x80, float:1.794E-43)
            r9.h(r0)
            r0 = r1 & 63
            r0 = r0 | 128(0x80, float:1.794E-43)
            r9.h(r0)
            int r0 = r11 + 1
            goto L_0x00c8
        L_0x0108:
            int r0 = r11 + 1
            if (r0 >= r12) goto L_0x007c
            int r0 = r11 + 1
            char r0 = r10.charAt(r0)
            goto L_0x007d
        L_0x0114:
            r2 = 65536(0x10000, float:9.18355E-41)
            r3 = -55297(0xffffffffffff27ff, float:NaN)
            r1 = r1 & r3
            int r1 = r1 << 10
            r3 = -56321(0xffffffffffff23ff, float:NaN)
            r0 = r0 & r3
            r0 = r0 | r1
            int r0 = r0 + r2
            int r1 = r0 >> 18
            r1 = r1 | 240(0xf0, float:3.36E-43)
            r9.h(r1)
            int r1 = r0 >> 12
            r1 = r1 & 63
            r1 = r1 | 128(0x80, float:1.794E-43)
            r9.h(r1)
            int r1 = r0 >> 6
            r1 = r1 & 63
            r1 = r1 | 128(0x80, float:1.794E-43)
            r9.h(r1)
            r0 = r0 & 63
            r0 = r0 | 128(0x80, float:1.794E-43)
            r9.h(r0)
            int r0 = r11 + 2
            goto L_0x00c8
        L_0x0145:
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: c.c.a(java.lang.String, int, int):c.c");
    }

    public s a() {
        return s.f602b;
    }

    public String a(long j, Charset charset) {
        t.a(this.f570b, 0, j);
        if (charset == null) {
            throw new IllegalArgumentException("charset == null");
        } else if (j > 2147483647L) {
            throw new IllegalArgumentException("byteCount > Integer.MAX_VALUE: " + j);
        } else if (j == 0) {
            return "";
        } else {
            o oVar = this.f569a;
            if (((long) oVar.f598b) + j > ((long) oVar.f599c)) {
                return new String(f(j), charset);
            }
            String str = new String(oVar.f597a, oVar.f598b, (int) j, charset);
            oVar.f598b = (int) (((long) oVar.f598b) + j);
            this.f570b -= j;
            if (oVar.f598b != oVar.f599c) {
                return str;
            }
            this.f569a = oVar.a();
            p.a(oVar);
            return str;
        }
    }

    public void a(long j) {
        if (this.f570b < j) {
            throw new EOFException();
        }
    }

    public void a(byte[] bArr) {
        int i = 0;
        while (i < bArr.length) {
            int a2 = a(bArr, i, bArr.length - i);
            if (a2 == -1) {
                throw new EOFException();
            }
            i += a2;
        }
    }

    public void a_(c cVar, long j) {
        if (cVar == null) {
            throw new IllegalArgumentException("source == null");
        } else if (cVar == this) {
            throw new IllegalArgumentException("source == this");
        } else {
            t.a(cVar.f570b, 0, j);
            while (j > 0) {
                if (j < ((long) (cVar.f569a.f599c - cVar.f569a.f598b))) {
                    o oVar = this.f569a != null ? this.f569a.g : null;
                    if (oVar != null && oVar.e) {
                        if ((((long) oVar.f599c) + j) - ((long) (oVar.d ? 0 : oVar.f598b)) <= 2048) {
                            cVar.f569a.a(oVar, (int) j);
                            cVar.f570b -= j;
                            this.f570b += j;
                            return;
                        }
                    }
                    cVar.f569a = cVar.f569a.a((int) j);
                }
                o oVar2 = cVar.f569a;
                long j2 = (long) (oVar2.f599c - oVar2.f598b);
                cVar.f569a = oVar2.a();
                if (this.f569a == null) {
                    this.f569a = oVar2;
                    o oVar3 = this.f569a;
                    o oVar4 = this.f569a;
                    o oVar5 = this.f569a;
                    oVar4.g = oVar5;
                    oVar3.f = oVar5;
                } else {
                    this.f569a.g.a(oVar2).b();
                }
                cVar.f570b -= j2;
                this.f570b += j2;
                j -= j2;
            }
        }
    }

    public byte b(long j) {
        t.a(this.f570b, j, 1);
        o oVar = this.f569a;
        while (true) {
            int i = oVar.f599c - oVar.f598b;
            if (j < ((long) i)) {
                return oVar.f597a[oVar.f598b + ((int) j)];
            }
            j -= (long) i;
            oVar = oVar.f;
        }
    }

    public long b() {
        return this.f570b;
    }

    /* renamed from: b */
    public c h(int i) {
        o e = e(1);
        byte[] bArr = e.f597a;
        int i2 = e.f599c;
        e.f599c = i2 + 1;
        bArr[i2] = (byte) i;
        this.f570b++;
        return this;
    }

    /* renamed from: b */
    public c c(byte[] bArr) {
        if (bArr != null) {
            return c(bArr, 0, bArr.length);
        }
        throw new IllegalArgumentException("source == null");
    }

    /* renamed from: b */
    public c c(byte[] bArr, int i, int i2) {
        if (bArr == null) {
            throw new IllegalArgumentException("source == null");
        }
        t.a((long) bArr.length, (long) i, (long) i2);
        int i3 = i + i2;
        while (i < i3) {
            o e = e(1);
            int min = Math.min(i3 - i, 2048 - e.f599c);
            System.arraycopy(bArr, i, e.f597a, e.f599c, min);
            i += min;
            e.f599c = min + e.f599c;
        }
        this.f570b += (long) i2;
        return this;
    }

    public void b(c cVar, long j) {
        if (this.f570b < j) {
            cVar.a_(this, this.f570b);
            throw new EOFException();
        } else {
            cVar.a_(this, j);
        }
    }

    public c c() {
        return this;
    }

    /* renamed from: c */
    public c g(int i) {
        o e = e(2);
        byte[] bArr = e.f597a;
        int i2 = e.f599c;
        int i3 = i2 + 1;
        bArr[i2] = (byte) ((i >>> 8) & 255);
        bArr[i3] = (byte) (i & 255);
        e.f599c = i3 + 1;
        this.f570b += 2;
        return this;
    }

    public f c(long j) {
        return new f(f(j));
    }

    public void close() {
    }

    /* renamed from: d */
    public c u() {
        return this;
    }

    /* renamed from: d */
    public c f(int i) {
        o e = e(4);
        byte[] bArr = e.f597a;
        int i2 = e.f599c;
        int i3 = i2 + 1;
        bArr[i2] = (byte) ((i >>> 24) & 255);
        int i4 = i3 + 1;
        bArr[i3] = (byte) ((i >>> 16) & 255);
        int i5 = i4 + 1;
        bArr[i4] = (byte) ((i >>> 8) & 255);
        bArr[i5] = (byte) (i & 255);
        e.f599c = i5 + 1;
        this.f570b += 4;
        return this;
    }

    public String d(long j) {
        return a(j, t.f605a);
    }

    public d e() {
        return this;
    }

    /* access modifiers changed from: package-private */
    public o e(int i) {
        if (i < 1 || i > 2048) {
            throw new IllegalArgumentException();
        } else if (this.f569a == null) {
            this.f569a = p.a();
            o oVar = this.f569a;
            o oVar2 = this.f569a;
            o oVar3 = this.f569a;
            oVar2.g = oVar3;
            oVar.f = oVar3;
            return oVar3;
        } else {
            o oVar4 = this.f569a.g;
            return (oVar4.f599c + i > 2048 || !oVar4.e) ? oVar4.a(p.a()) : oVar4;
        }
    }

    /* access modifiers changed from: package-private */
    public String e(long j) {
        if (j <= 0 || b(j - 1) != 13) {
            String d = d(j);
            g(1L);
            return d;
        }
        String d2 = d(j - 1);
        g(2L);
        return d2;
    }

    public boolean equals(Object obj) {
        long j = 0;
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof c)) {
            return false;
        }
        c cVar = (c) obj;
        if (this.f570b != cVar.f570b) {
            return false;
        }
        if (this.f570b == 0) {
            return true;
        }
        o oVar = this.f569a;
        o oVar2 = cVar.f569a;
        int i = oVar.f598b;
        int i2 = oVar2.f598b;
        while (j < this.f570b) {
            long min = (long) Math.min(oVar.f599c - i, oVar2.f599c - i2);
            int i3 = 0;
            while (((long) i3) < min) {
                int i4 = i + 1;
                byte b2 = oVar.f597a[i];
                int i5 = i2 + 1;
                if (b2 != oVar2.f597a[i2]) {
                    return false;
                }
                i3++;
                i2 = i5;
                i = i4;
            }
            if (i == oVar.f599c) {
                oVar = oVar.f;
                i = oVar.f598b;
            }
            if (i2 == oVar2.f599c) {
                oVar2 = oVar2.f;
                i2 = oVar2.f598b;
            }
            j += min;
        }
        return true;
    }

    public boolean f() {
        return this.f570b == 0;
    }

    public byte[] f(long j) {
        t.a(this.f570b, 0, j);
        if (j > 2147483647L) {
            throw new IllegalArgumentException("byteCount > Integer.MAX_VALUE: " + j);
        }
        byte[] bArr = new byte[((int) j)];
        a(bArr);
        return bArr;
    }

    public void flush() {
    }

    public long g() {
        long j = this.f570b;
        if (j == 0) {
            return 0;
        }
        o oVar = this.f569a.g;
        return (oVar.f599c >= 2048 || !oVar.e) ? j : j - ((long) (oVar.f599c - oVar.f598b));
    }

    public void g(long j) {
        while (j > 0) {
            if (this.f569a == null) {
                throw new EOFException();
            }
            int min = (int) Math.min(j, (long) (this.f569a.f599c - this.f569a.f598b));
            this.f570b -= (long) min;
            j -= (long) min;
            o oVar = this.f569a;
            oVar.f598b = min + oVar.f598b;
            if (this.f569a.f598b == this.f569a.f599c) {
                o oVar2 = this.f569a;
                this.f569a = oVar2.a();
                p.a(oVar2);
            }
        }
    }

    public byte h() {
        if (this.f570b == 0) {
            throw new IllegalStateException("size == 0");
        }
        o oVar = this.f569a;
        int i = oVar.f598b;
        int i2 = oVar.f599c;
        int i3 = i + 1;
        byte b2 = oVar.f597a[i];
        this.f570b--;
        if (i3 == i2) {
            this.f569a = oVar.a();
            p.a(oVar);
        } else {
            oVar.f598b = i3;
        }
        return b2;
    }

    /* renamed from: h */
    public c m(long j) {
        o e = e(8);
        byte[] bArr = e.f597a;
        int i = e.f599c;
        int i2 = i + 1;
        bArr[i] = (byte) ((int) ((j >>> 56) & 255));
        int i3 = i2 + 1;
        bArr[i2] = (byte) ((int) ((j >>> 48) & 255));
        int i4 = i3 + 1;
        bArr[i3] = (byte) ((int) ((j >>> 40) & 255));
        int i5 = i4 + 1;
        bArr[i4] = (byte) ((int) ((j >>> 32) & 255));
        int i6 = i5 + 1;
        bArr[i5] = (byte) ((int) ((j >>> 24) & 255));
        int i7 = i6 + 1;
        bArr[i6] = (byte) ((int) ((j >>> 16) & 255));
        int i8 = i7 + 1;
        bArr[i7] = (byte) ((int) ((j >>> 8) & 255));
        bArr[i8] = (byte) ((int) (j & 255));
        e.f599c = i8 + 1;
        this.f570b += 8;
        return this;
    }

    public int hashCode() {
        o oVar = this.f569a;
        if (oVar == null) {
            return 0;
        }
        int i = 1;
        do {
            int i2 = oVar.f598b;
            int i3 = oVar.f599c;
            while (i2 < i3) {
                i2++;
                i = oVar.f597a[i2] + (i * 31);
            }
            oVar = oVar.f;
        } while (oVar != this.f569a);
        return i;
    }

    /* renamed from: i */
    public c l(long j) {
        boolean z;
        long j2;
        if (j == 0) {
            return h(48);
        }
        if (j < 0) {
            j2 = -j;
            if (j2 < 0) {
                return b("-9223372036854775808");
            }
            z = true;
        } else {
            z = false;
            j2 = j;
        }
        int i = j2 < 100000000 ? j2 < 10000 ? j2 < 100 ? j2 < 10 ? 1 : 2 : j2 < 1000 ? 3 : 4 : j2 < 1000000 ? j2 < 100000 ? 5 : 6 : j2 < 10000000 ? 7 : 8 : j2 < 1000000000000L ? j2 < 10000000000L ? j2 < 1000000000 ? 9 : 10 : j2 < 100000000000L ? 11 : 12 : j2 < 1000000000000000L ? j2 < 10000000000000L ? 13 : j2 < 100000000000000L ? 14 : 15 : j2 < 100000000000000000L ? j2 < 10000000000000000L ? 16 : 17 : j2 < 1000000000000000000L ? 18 : 19;
        if (z) {
            i++;
        }
        o e = e(i);
        byte[] bArr = e.f597a;
        int i2 = e.f599c + i;
        while (j2 != 0) {
            i2--;
            bArr[i2] = f568c[(int) (j2 % 10)];
            j2 /= 10;
        }
        if (z) {
            bArr[i2 - 1] = 45;
        }
        e.f599c += i;
        this.f570b = ((long) i) + this.f570b;
        return this;
    }

    public short i() {
        if (this.f570b < 2) {
            throw new IllegalStateException("size < 2: " + this.f570b);
        }
        o oVar = this.f569a;
        int i = oVar.f598b;
        int i2 = oVar.f599c;
        if (i2 - i < 2) {
            return (short) (((h() & 255) << 8) | (h() & 255));
        }
        byte[] bArr = oVar.f597a;
        int i3 = i + 1;
        int i4 = i3 + 1;
        byte b2 = ((bArr[i] & 255) << 8) | (bArr[i3] & 255);
        this.f570b -= 2;
        if (i4 == i2) {
            this.f569a = oVar.a();
            p.a(oVar);
        } else {
            oVar.f598b = i4;
        }
        return (short) b2;
    }

    public int j() {
        if (this.f570b < 4) {
            throw new IllegalStateException("size < 4: " + this.f570b);
        }
        o oVar = this.f569a;
        int i = oVar.f598b;
        int i2 = oVar.f599c;
        if (i2 - i < 4) {
            return ((h() & 255) << 24) | ((h() & 255) << 16) | ((h() & 255) << 8) | (h() & 255);
        }
        byte[] bArr = oVar.f597a;
        int i3 = i + 1;
        int i4 = i3 + 1;
        byte b2 = ((bArr[i] & 255) << 24) | ((bArr[i3] & 255) << 16);
        int i5 = i4 + 1;
        byte b3 = b2 | ((bArr[i4] & 255) << 8);
        int i6 = i5 + 1;
        byte b4 = b3 | (bArr[i5] & 255);
        this.f570b -= 4;
        if (i6 == i2) {
            this.f569a = oVar.a();
            p.a(oVar);
            return b4;
        }
        oVar.f598b = i6;
        return b4;
    }

    /* renamed from: j */
    public c k(long j) {
        if (j == 0) {
            return h(48);
        }
        int numberOfTrailingZeros = (Long.numberOfTrailingZeros(Long.highestOneBit(j)) / 4) + 1;
        o e = e(numberOfTrailingZeros);
        byte[] bArr = e.f597a;
        int i = e.f599c;
        for (int i2 = (e.f599c + numberOfTrailingZeros) - 1; i2 >= i; i2--) {
            bArr[i2] = f568c[(int) (15 & j)];
            j >>>= 4;
        }
        e.f599c += numberOfTrailingZeros;
        this.f570b = ((long) numberOfTrailingZeros) + this.f570b;
        return this;
    }

    public long k() {
        if (this.f570b < 8) {
            throw new IllegalStateException("size < 8: " + this.f570b);
        }
        o oVar = this.f569a;
        int i = oVar.f598b;
        int i2 = oVar.f599c;
        if (i2 - i < 8) {
            return ((((long) j()) & 4294967295L) << 32) | (((long) j()) & 4294967295L);
        }
        byte[] bArr = oVar.f597a;
        int i3 = i + 1;
        int i4 = i3 + 1;
        long j = ((((long) bArr[i3]) & 255) << 48) | ((((long) bArr[i]) & 255) << 56);
        int i5 = i4 + 1;
        int i6 = i5 + 1;
        long j2 = j | ((((long) bArr[i4]) & 255) << 40) | ((((long) bArr[i5]) & 255) << 32);
        int i7 = i6 + 1;
        int i8 = i7 + 1;
        long j3 = j2 | ((((long) bArr[i6]) & 255) << 24) | ((((long) bArr[i7]) & 255) << 16);
        int i9 = i8 + 1;
        int i10 = i9 + 1;
        long j4 = (((long) bArr[i9]) & 255) | j3 | ((((long) bArr[i8]) & 255) << 8);
        this.f570b -= 8;
        if (i10 == i2) {
            this.f569a = oVar.a();
            p.a(oVar);
            return j4;
        }
        oVar.f598b = i10;
        return j4;
    }

    public short l() {
        return t.a(i());
    }

    public int m() {
        return t.a(j());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:30:0x009c, code lost:
        if (r7 != r12) goto L_0x00c9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x009e, code lost:
        r0.f569a = r10.a();
        c.p.a(r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00a9, code lost:
        if (r2 != false) goto L_0x00b1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00c9, code lost:
        r10.f598b = r7;
     */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x009b  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x007e A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long n() {
        /*
            r18 = this;
            r0 = r18
            long r2 = r0.f570b
            r4 = 0
            int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r2 != 0) goto L_0x0012
            java.lang.IllegalStateException r2 = new java.lang.IllegalStateException
            java.lang.String r3 = "size == 0"
            r2.<init>(r3)
            throw r2
        L_0x0012:
            r4 = 0
            r3 = 0
            r2 = 0
        L_0x0016:
            r0 = r18
            c.o r10 = r0.f569a
            byte[] r11 = r10.f597a
            int r6 = r10.f598b
            int r12 = r10.f599c
            r7 = r6
        L_0x0021:
            if (r7 >= r12) goto L_0x009c
            byte r8 = r11[r7]
            r6 = 48
            if (r8 < r6) goto L_0x0062
            r6 = 57
            if (r8 > r6) goto L_0x0062
            int r6 = r8 + -48
        L_0x002f:
            r14 = -1152921504606846976(0xf000000000000000, double:-3.105036184601418E231)
            long r14 = r14 & r4
            r16 = 0
            int r9 = (r14 > r16 ? 1 : (r14 == r16 ? 0 : -1))
            if (r9 == 0) goto L_0x00bd
            c.c r2 = new c.c
            r2.<init>()
            c.c r2 = r2.k(r4)
            c.c r2 = r2.h(r8)
            java.lang.NumberFormatException r3 = new java.lang.NumberFormatException
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "Number too large: "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r2 = r2.p()
            java.lang.StringBuilder r2 = r4.append(r2)
            java.lang.String r2 = r2.toString()
            r3.<init>(r2)
            throw r3
        L_0x0062:
            r6 = 97
            if (r8 < r6) goto L_0x006f
            r6 = 102(0x66, float:1.43E-43)
            if (r8 > r6) goto L_0x006f
            int r6 = r8 + -97
            int r6 = r6 + 10
            goto L_0x002f
        L_0x006f:
            r6 = 65
            if (r8 < r6) goto L_0x007c
            r6 = 70
            if (r8 > r6) goto L_0x007c
            int r6 = r8 + -65
            int r6 = r6 + 10
            goto L_0x002f
        L_0x007c:
            if (r3 != 0) goto L_0x009b
            java.lang.NumberFormatException r2 = new java.lang.NumberFormatException
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Expected leading [0-9a-fA-F] character but was 0x"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r4 = java.lang.Integer.toHexString(r8)
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            r2.<init>(r3)
            throw r2
        L_0x009b:
            r2 = 1
        L_0x009c:
            if (r7 != r12) goto L_0x00c9
            c.o r6 = r10.a()
            r0 = r18
            r0.f569a = r6
            c.p.a(r10)
        L_0x00a9:
            if (r2 != 0) goto L_0x00b1
            r0 = r18
            c.o r6 = r0.f569a
            if (r6 != 0) goto L_0x0016
        L_0x00b1:
            r0 = r18
            long r6 = r0.f570b
            long r2 = (long) r3
            long r2 = r6 - r2
            r0 = r18
            r0.f570b = r2
            return r4
        L_0x00bd:
            r8 = 4
            long r4 = r4 << r8
            long r8 = (long) r6
            long r8 = r8 | r4
            int r4 = r7 + 1
            int r3 = r3 + 1
            r7 = r4
            r4 = r8
            goto L_0x0021
        L_0x00c9:
            r10.f598b = r7
            goto L_0x00a9
        */
        throw new UnsupportedOperationException("Method not decompiled: c.c.n():long");
    }

    public f o() {
        return new f(r());
    }

    public String p() {
        try {
            return a(this.f570b, t.f605a);
        } catch (EOFException e) {
            throw new AssertionError(e);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(long, long):long} */
    public String q() {
        long a2 = a((byte) 10);
        if (a2 != -1) {
            return e(a2);
        }
        c cVar = new c();
        a(cVar, 0, Math.min(32L, this.f570b));
        throw new EOFException("\\n not found: size=" + b() + " content=" + cVar.o().d() + "...");
    }

    public byte[] r() {
        try {
            return f(this.f570b);
        } catch (EOFException e) {
            throw new AssertionError(e);
        }
    }

    public void s() {
        try {
            g(this.f570b);
        } catch (EOFException e) {
            throw new AssertionError(e);
        }
    }

    /* renamed from: t */
    public c clone() {
        c cVar = new c();
        if (this.f570b == 0) {
            return cVar;
        }
        cVar.f569a = new o(this.f569a);
        o oVar = cVar.f569a;
        o oVar2 = cVar.f569a;
        o oVar3 = cVar.f569a;
        oVar2.g = oVar3;
        oVar.f = oVar3;
        for (o oVar4 = this.f569a.f; oVar4 != this.f569a; oVar4 = oVar4.f) {
            cVar.f569a.g.a(new o(oVar4));
        }
        cVar.f570b = this.f570b;
        return cVar;
    }

    public String toString() {
        if (this.f570b == 0) {
            return "Buffer[size=0]";
        }
        if (this.f570b <= 16) {
            return String.format("Buffer[size=%s data=%s]", Long.valueOf(this.f570b), clone().o().d());
        }
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(this.f569a.f597a, this.f569a.f598b, this.f569a.f599c - this.f569a.f598b);
            for (o oVar = this.f569a.f; oVar != this.f569a; oVar = oVar.f) {
                instance.update(oVar.f597a, oVar.f598b, oVar.f599c - oVar.f598b);
            }
            return String.format("Buffer[size=%s md5=%s]", Long.valueOf(this.f570b), f.a(instance.digest()).d());
        } catch (NoSuchAlgorithmException e) {
            throw new AssertionError();
        }
    }
}
