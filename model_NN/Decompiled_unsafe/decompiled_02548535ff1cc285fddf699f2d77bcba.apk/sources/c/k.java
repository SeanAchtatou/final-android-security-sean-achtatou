package c;

import java.io.EOFException;
import java.io.IOException;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;

public final class k implements r {

    /* renamed from: a  reason: collision with root package name */
    private final e f582a;

    /* renamed from: b  reason: collision with root package name */
    private final Inflater f583b;

    /* renamed from: c  reason: collision with root package name */
    private int f584c;
    private boolean d;

    k(e eVar, Inflater inflater) {
        if (eVar == null) {
            throw new IllegalArgumentException("source == null");
        } else if (inflater == null) {
            throw new IllegalArgumentException("inflater == null");
        } else {
            this.f582a = eVar;
            this.f583b = inflater;
        }
    }

    public k(r rVar, Inflater inflater) {
        this(l.a(rVar), inflater);
    }

    private void c() {
        if (this.f584c != 0) {
            int remaining = this.f584c - this.f583b.getRemaining();
            this.f584c -= remaining;
            this.f582a.g((long) remaining);
        }
    }

    public long a(c cVar, long j) {
        boolean b2;
        if (j < 0) {
            throw new IllegalArgumentException("byteCount < 0: " + j);
        } else if (this.d) {
            throw new IllegalStateException("closed");
        } else if (j == 0) {
            return 0;
        } else {
            do {
                b2 = b();
                try {
                    o e = cVar.e(1);
                    int inflate = this.f583b.inflate(e.f597a, e.f599c, 2048 - e.f599c);
                    if (inflate > 0) {
                        e.f599c += inflate;
                        cVar.f570b += (long) inflate;
                        return (long) inflate;
                    } else if (this.f583b.finished() || this.f583b.needsDictionary()) {
                        c();
                        if (e.f598b == e.f599c) {
                            cVar.f569a = e.a();
                            p.a(e);
                        }
                        return -1;
                    }
                } catch (DataFormatException e2) {
                    throw new IOException(e2);
                }
            } while (!b2);
            throw new EOFException("source exhausted prematurely");
        }
    }

    public s a() {
        return this.f582a.a();
    }

    public boolean b() {
        if (!this.f583b.needsInput()) {
            return false;
        }
        c();
        if (this.f583b.getRemaining() != 0) {
            throw new IllegalStateException("?");
        } else if (this.f582a.f()) {
            return true;
        } else {
            o oVar = this.f582a.c().f569a;
            this.f584c = oVar.f599c - oVar.f598b;
            this.f583b.setInput(oVar.f597a, oVar.f598b, this.f584c);
            return false;
        }
    }

    public void close() {
        if (!this.d) {
            this.f583b.end();
            this.d = true;
            this.f582a.close();
        }
    }
}
