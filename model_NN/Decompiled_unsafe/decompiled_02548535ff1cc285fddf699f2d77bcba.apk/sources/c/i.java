package c;

import java.util.concurrent.TimeUnit;

public class i extends s {

    /* renamed from: a  reason: collision with root package name */
    private s f578a;

    public i(s sVar) {
        if (sVar == null) {
            throw new IllegalArgumentException("delegate == null");
        }
        this.f578a = sVar;
    }

    public final i a(s sVar) {
        if (sVar == null) {
            throw new IllegalArgumentException("delegate == null");
        }
        this.f578a = sVar;
        return this;
    }

    public final s a() {
        return this.f578a;
    }

    public s a(long j) {
        return this.f578a.a(j);
    }

    public s a(long j, TimeUnit timeUnit) {
        return this.f578a.a(j, timeUnit);
    }

    public long b_() {
        return this.f578a.b_();
    }

    public boolean c_() {
        return this.f578a.c_();
    }

    public long d() {
        return this.f578a.d();
    }

    public s d_() {
        return this.f578a.d_();
    }

    public s f() {
        return this.f578a.f();
    }

    public void g() {
        this.f578a.g();
    }
}
