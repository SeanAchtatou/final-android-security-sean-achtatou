package c;

import java.io.IOException;
import java.io.InterruptedIOException;

public class a extends s {

    /* renamed from: a  reason: collision with root package name */
    private static a f560a;

    /* renamed from: c  reason: collision with root package name */
    private boolean f561c;
    private a d;
    private long e;

    /* renamed from: c.a$a  reason: collision with other inner class name */
    private static final class C0008a extends Thread {
        public C0008a() {
            super("Okio Watchdog");
            setDaemon(true);
        }

        public void run() {
            while (true) {
                try {
                    a e = a.h();
                    if (e != null) {
                        e.a();
                    }
                } catch (InterruptedException e2) {
                }
            }
        }
    }

    private static synchronized void a(a aVar, long j, boolean z) {
        synchronized (a.class) {
            if (f560a == null) {
                f560a = new a();
                new C0008a().start();
            }
            long nanoTime = System.nanoTime();
            if (j != 0 && z) {
                aVar.e = Math.min(j, aVar.d() - nanoTime) + nanoTime;
            } else if (j != 0) {
                aVar.e = nanoTime + j;
            } else if (z) {
                aVar.e = aVar.d();
            } else {
                throw new AssertionError();
            }
            long b2 = aVar.b(nanoTime);
            a aVar2 = f560a;
            while (aVar2.d != null && b2 >= aVar2.d.b(nanoTime)) {
                aVar2 = aVar2.d;
            }
            aVar.d = aVar2.d;
            aVar2.d = aVar;
            if (aVar2 == f560a) {
                a.class.notify();
            }
        }
    }

    private static synchronized boolean a(a aVar) {
        boolean z;
        synchronized (a.class) {
            a aVar2 = f560a;
            while (true) {
                if (aVar2 == null) {
                    z = true;
                    break;
                } else if (aVar2.d == aVar) {
                    aVar2.d = aVar.d;
                    aVar.d = null;
                    z = false;
                    break;
                } else {
                    aVar2 = aVar2.d;
                }
            }
        }
        return z;
    }

    private long b(long j) {
        return this.e - j;
    }

    /* access modifiers changed from: private */
    public static synchronized a h() {
        a aVar = null;
        synchronized (a.class) {
            a aVar2 = f560a.d;
            if (aVar2 == null) {
                a.class.wait();
            } else {
                long b2 = aVar2.b(System.nanoTime());
                if (b2 > 0) {
                    long j = b2 / 1000000;
                    a.class.wait(j, (int) (b2 - (1000000 * j)));
                } else {
                    f560a.d = aVar2.d;
                    aVar2.d = null;
                    aVar = aVar2;
                }
            }
        }
        return aVar;
    }

    public final q a(final q qVar) {
        return new q() {
            public s a() {
                return a.this;
            }

            public void a_(c cVar, long j) {
                a.this.c();
                try {
                    qVar.a_(cVar, j);
                    a.this.a(true);
                } catch (IOException e) {
                    throw a.this.b(e);
                } catch (Throwable th) {
                    a.this.a(false);
                    throw th;
                }
            }

            public void close() {
                a.this.c();
                try {
                    qVar.close();
                    a.this.a(true);
                } catch (IOException e) {
                    throw a.this.b(e);
                } catch (Throwable th) {
                    a.this.a(false);
                    throw th;
                }
            }

            public void flush() {
                a.this.c();
                try {
                    qVar.flush();
                    a.this.a(true);
                } catch (IOException e) {
                    throw a.this.b(e);
                } catch (Throwable th) {
                    a.this.a(false);
                    throw th;
                }
            }

            public String toString() {
                return "AsyncTimeout.sink(" + qVar + ")";
            }
        };
    }

    public final r a(final r rVar) {
        return new r() {
            public long a(c cVar, long j) {
                a.this.c();
                try {
                    long a2 = rVar.a(cVar, j);
                    a.this.a(true);
                    return a2;
                } catch (IOException e) {
                    throw a.this.b(e);
                } catch (Throwable th) {
                    a.this.a(false);
                    throw th;
                }
            }

            public s a() {
                return a.this;
            }

            public void close() {
                try {
                    rVar.close();
                    a.this.a(true);
                } catch (IOException e) {
                    throw a.this.b(e);
                } catch (Throwable th) {
                    a.this.a(false);
                    throw th;
                }
            }

            public String toString() {
                return "AsyncTimeout.source(" + rVar + ")";
            }
        };
    }

    /* access modifiers changed from: protected */
    public IOException a(IOException iOException) {
        InterruptedIOException interruptedIOException = new InterruptedIOException("timeout");
        if (iOException != null) {
            interruptedIOException.initCause(iOException);
        }
        return interruptedIOException;
    }

    /* access modifiers changed from: protected */
    public void a() {
    }

    /* access modifiers changed from: package-private */
    public final void a(boolean z) {
        if (a_() && z) {
            throw a((IOException) null);
        }
    }

    public final boolean a_() {
        if (!this.f561c) {
            return false;
        }
        this.f561c = false;
        return a(this);
    }

    /* access modifiers changed from: package-private */
    public final IOException b(IOException iOException) {
        return !a_() ? iOException : a(iOException);
    }

    public final void c() {
        if (this.f561c) {
            throw new IllegalStateException("Unbalanced enter/exit");
        }
        long b_ = b_();
        boolean c_ = c_();
        if (b_ != 0 || c_) {
            this.f561c = true;
            a(this, b_, c_);
        }
    }
}
