package c;

import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

public class f implements Serializable, Comparable<f> {

    /* renamed from: a  reason: collision with root package name */
    static final char[] f571a = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

    /* renamed from: b  reason: collision with root package name */
    public static final f f572b = a(new byte[0]);

    /* renamed from: c  reason: collision with root package name */
    final byte[] f573c;
    transient int d;
    transient String e;

    f(byte[] bArr) {
        this.f573c = bArr;
    }

    public static f a(String str) {
        if (str == null) {
            throw new IllegalArgumentException("s == null");
        }
        f fVar = new f(str.getBytes(t.f605a));
        fVar.e = str;
        return fVar;
    }

    public static f a(byte... bArr) {
        if (bArr != null) {
            return new f((byte[]) bArr.clone());
        }
        throw new IllegalArgumentException("data == null");
    }

    private f b(String str) {
        try {
            return a(MessageDigest.getInstance(str).digest(this.f573c));
        } catch (NoSuchAlgorithmException e2) {
            throw new AssertionError(e2);
        }
    }

    public byte a(int i) {
        return this.f573c[i];
    }

    /* renamed from: a */
    public int compareTo(f fVar) {
        int f = f();
        int f2 = fVar.f();
        int min = Math.min(f, f2);
        for (int i = 0; i < min; i++) {
            byte a2 = a(i) & 255;
            byte a3 = fVar.a(i) & 255;
            if (a2 != a3) {
                return a2 < a3 ? -1 : 1;
            }
        }
        if (f == f2) {
            return 0;
        }
        return f >= f2 ? 1 : -1;
    }

    public String a() {
        String str = this.e;
        if (str != null) {
            return str;
        }
        String str2 = new String(this.f573c, t.f605a);
        this.e = str2;
        return str2;
    }

    /* access modifiers changed from: package-private */
    public void a(c cVar) {
        cVar.c(this.f573c, 0, this.f573c.length);
    }

    public boolean a(int i, byte[] bArr, int i2, int i3) {
        return i <= this.f573c.length - i3 && i2 <= bArr.length - i3 && t.a(this.f573c, i, bArr, i2, i3);
    }

    public String b() {
        return b.a(this.f573c);
    }

    public f c() {
        return b("MD5");
    }

    public String d() {
        char[] cArr = new char[(this.f573c.length * 2)];
        int i = 0;
        for (byte b2 : this.f573c) {
            int i2 = i + 1;
            cArr[i] = f571a[(b2 >> 4) & 15];
            i = i2 + 1;
            cArr[i2] = f571a[b2 & 15];
        }
        return new String(cArr);
    }

    public f e() {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.f573c.length) {
                return this;
            }
            byte b2 = this.f573c[i2];
            if (b2 < 65 || b2 > 90) {
                i = i2 + 1;
            } else {
                byte[] bArr = (byte[]) this.f573c.clone();
                bArr[i2] = (byte) (b2 + 32);
                for (int i3 = i2 + 1; i3 < bArr.length; i3++) {
                    byte b3 = bArr[i3];
                    if (b3 >= 65 && b3 <= 90) {
                        bArr[i3] = (byte) (b3 + 32);
                    }
                }
                return new f(bArr);
            }
        }
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        return (obj instanceof f) && ((f) obj).f() == this.f573c.length && ((f) obj).a(0, this.f573c, 0, this.f573c.length);
    }

    public int f() {
        return this.f573c.length;
    }

    public byte[] g() {
        return (byte[]) this.f573c.clone();
    }

    public int hashCode() {
        int i = this.d;
        if (i != 0) {
            return i;
        }
        int hashCode = Arrays.hashCode(this.f573c);
        this.d = hashCode;
        return hashCode;
    }

    public String toString() {
        if (this.f573c.length == 0) {
            return "ByteString[size=0]";
        }
        if (this.f573c.length <= 16) {
            return String.format("ByteString[size=%s data=%s]", Integer.valueOf(this.f573c.length), d());
        }
        return String.format("ByteString[size=%s md5=%s]", Integer.valueOf(this.f573c.length), c().d());
    }
}
