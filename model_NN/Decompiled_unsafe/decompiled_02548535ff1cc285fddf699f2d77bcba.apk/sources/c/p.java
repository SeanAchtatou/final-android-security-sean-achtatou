package c;

final class p {

    /* renamed from: a  reason: collision with root package name */
    static o f600a;

    /* renamed from: b  reason: collision with root package name */
    static long f601b;

    private p() {
    }

    static o a() {
        synchronized (p.class) {
            if (f600a == null) {
                return new o();
            }
            o oVar = f600a;
            f600a = oVar.f;
            oVar.f = null;
            f601b -= 2048;
            return oVar;
        }
    }

    static void a(o oVar) {
        if (oVar.f != null || oVar.g != null) {
            throw new IllegalArgumentException();
        } else if (!oVar.d) {
            synchronized (p.class) {
                if (f601b + 2048 <= 65536) {
                    f601b += 2048;
                    oVar.f = f600a;
                    oVar.f599c = 0;
                    oVar.f598b = 0;
                    f600a = oVar;
                }
            }
        }
    }
}
