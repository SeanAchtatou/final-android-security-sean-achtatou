package c;

public abstract class h implements r {

    /* renamed from: a  reason: collision with root package name */
    private final r f577a;

    public h(r rVar) {
        if (rVar == null) {
            throw new IllegalArgumentException("delegate == null");
        }
        this.f577a = rVar;
    }

    public long a(c cVar, long j) {
        return this.f577a.a(cVar, j);
    }

    public s a() {
        return this.f577a.a();
    }

    public void close() {
        this.f577a.close();
    }

    public String toString() {
        return getClass().getSimpleName() + "(" + this.f577a.toString() + ")";
    }
}
