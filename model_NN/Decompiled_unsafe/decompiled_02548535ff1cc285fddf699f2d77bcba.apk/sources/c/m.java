package c;

final class m implements d {

    /* renamed from: a  reason: collision with root package name */
    public final c f591a;

    /* renamed from: b  reason: collision with root package name */
    public final q f592b;

    /* renamed from: c  reason: collision with root package name */
    private boolean f593c;

    public m(q qVar) {
        this(qVar, new c());
    }

    public m(q qVar, c cVar) {
        if (qVar == null) {
            throw new IllegalArgumentException("sink == null");
        }
        this.f591a = cVar;
        this.f592b = qVar;
    }

    public long a(r rVar) {
        if (rVar == null) {
            throw new IllegalArgumentException("source == null");
        }
        long j = 0;
        while (true) {
            long a2 = rVar.a(this.f591a, 2048);
            if (a2 == -1) {
                return j;
            }
            j += a2;
            u();
        }
    }

    public s a() {
        return this.f592b.a();
    }

    public void a_(c cVar, long j) {
        if (this.f593c) {
            throw new IllegalStateException("closed");
        }
        this.f591a.a_(cVar, j);
        u();
    }

    public d b(f fVar) {
        if (this.f593c) {
            throw new IllegalStateException("closed");
        }
        this.f591a.b(fVar);
        return u();
    }

    public d b(String str) {
        if (this.f593c) {
            throw new IllegalStateException("closed");
        }
        this.f591a.b(str);
        return u();
    }

    public c c() {
        return this.f591a;
    }

    public d c(byte[] bArr) {
        if (this.f593c) {
            throw new IllegalStateException("closed");
        }
        this.f591a.c(bArr);
        return u();
    }

    public d c(byte[] bArr, int i, int i2) {
        if (this.f593c) {
            throw new IllegalStateException("closed");
        }
        this.f591a.c(bArr, i, i2);
        return u();
    }

    public void close() {
        if (!this.f593c) {
            Throwable th = null;
            try {
                if (this.f591a.f570b > 0) {
                    this.f592b.a_(this.f591a, this.f591a.f570b);
                }
            } catch (Throwable th2) {
                th = th2;
            }
            try {
                this.f592b.close();
            } catch (Throwable th3) {
                if (th == null) {
                    th = th3;
                }
            }
            this.f593c = true;
            if (th != null) {
                t.a(th);
            }
        }
    }

    public d e() {
        if (this.f593c) {
            throw new IllegalStateException("closed");
        }
        long b2 = this.f591a.b();
        if (b2 > 0) {
            this.f592b.a_(this.f591a, b2);
        }
        return this;
    }

    public d f(int i) {
        if (this.f593c) {
            throw new IllegalStateException("closed");
        }
        this.f591a.f(i);
        return u();
    }

    public void flush() {
        if (this.f593c) {
            throw new IllegalStateException("closed");
        }
        if (this.f591a.f570b > 0) {
            this.f592b.a_(this.f591a, this.f591a.f570b);
        }
        this.f592b.flush();
    }

    public d g(int i) {
        if (this.f593c) {
            throw new IllegalStateException("closed");
        }
        this.f591a.g(i);
        return u();
    }

    public d h(int i) {
        if (this.f593c) {
            throw new IllegalStateException("closed");
        }
        this.f591a.h(i);
        return u();
    }

    public d k(long j) {
        if (this.f593c) {
            throw new IllegalStateException("closed");
        }
        this.f591a.k(j);
        return u();
    }

    public d l(long j) {
        if (this.f593c) {
            throw new IllegalStateException("closed");
        }
        this.f591a.l(j);
        return u();
    }

    public d m(long j) {
        if (this.f593c) {
            throw new IllegalStateException("closed");
        }
        this.f591a.m(j);
        return u();
    }

    public String toString() {
        return "buffer(" + this.f592b + ")";
    }

    public d u() {
        if (this.f593c) {
            throw new IllegalStateException("closed");
        }
        long g = this.f591a.g();
        if (g > 0) {
            this.f592b.a_(this.f591a, g);
        }
        return this;
    }
}
