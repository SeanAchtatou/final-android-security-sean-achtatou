package c;

final class o {

    /* renamed from: a  reason: collision with root package name */
    final byte[] f597a;

    /* renamed from: b  reason: collision with root package name */
    int f598b;

    /* renamed from: c  reason: collision with root package name */
    int f599c;
    boolean d;
    boolean e;
    o f;
    o g;

    o() {
        this.f597a = new byte[2048];
        this.e = true;
        this.d = false;
    }

    o(o oVar) {
        this(oVar.f597a, oVar.f598b, oVar.f599c);
        oVar.d = true;
    }

    o(byte[] bArr, int i, int i2) {
        this.f597a = bArr;
        this.f598b = i;
        this.f599c = i2;
        this.e = false;
        this.d = true;
    }

    public o a() {
        o oVar = this.f != this ? this.f : null;
        this.g.f = this.f;
        this.f.g = this.g;
        this.f = null;
        this.g = null;
        return oVar;
    }

    public o a(int i) {
        if (i <= 0 || i > this.f599c - this.f598b) {
            throw new IllegalArgumentException();
        }
        o oVar = new o(this);
        oVar.f599c = oVar.f598b + i;
        this.f598b += i;
        this.g.a(oVar);
        return oVar;
    }

    public o a(o oVar) {
        oVar.g = this;
        oVar.f = this.f;
        this.f.g = oVar;
        this.f = oVar;
        return oVar;
    }

    public void a(o oVar, int i) {
        if (!oVar.e) {
            throw new IllegalArgumentException();
        }
        if (oVar.f599c + i > 2048) {
            if (oVar.d) {
                throw new IllegalArgumentException();
            } else if ((oVar.f599c + i) - oVar.f598b > 2048) {
                throw new IllegalArgumentException();
            } else {
                System.arraycopy(oVar.f597a, oVar.f598b, oVar.f597a, 0, oVar.f599c - oVar.f598b);
                oVar.f599c -= oVar.f598b;
                oVar.f598b = 0;
            }
        }
        System.arraycopy(this.f597a, this.f598b, oVar.f597a, oVar.f599c, i);
        oVar.f599c += i;
        this.f598b += i;
    }

    public void b() {
        if (this.g == this) {
            throw new IllegalStateException();
        } else if (this.g.e) {
            int i = this.f599c - this.f598b;
            if (i <= (this.g.d ? 0 : this.g.f598b) + (2048 - this.g.f599c)) {
                a(this.g, i);
                a();
                p.a(this);
            }
        }
    }
}
