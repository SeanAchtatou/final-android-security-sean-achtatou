package android.support.design.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.design.R;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.SnackbarManager;
import android.support.design.widget.SwipeDismissBehavior;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPropertyAnimatorListener;
import android.support.v4.view.ViewPropertyAnimatorListenerAdapter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public final class Snackbar {
    private static final int ANIMATION_DURATION = 250;
    private static final int ANIMATION_FADE_DURATION = 180;
    public static final int LENGTH_INDEFINITE = -2;
    public static final int LENGTH_LONG = 0;
    public static final int LENGTH_SHORT = -1;
    private static final int MSG_DISMISS = 1;
    private static final int MSG_SHOW = 0;
    /* access modifiers changed from: private */
    public static final Handler sHandler;
    /* access modifiers changed from: private */
    public Callback mCallback;
    private final Context mContext;
    private int mDuration;
    /* access modifiers changed from: private */
    public final SnackbarManager.Callback mManagerCallback;
    private final ViewGroup mTargetParent;
    /* access modifiers changed from: private */
    public final SnackbarLayout mView = ((SnackbarLayout) LayoutInflater.from(this.mContext).inflate(R.layout.design_layout_snackbar, this.mTargetParent, false));

    @Retention(RetentionPolicy.SOURCE)
    public @interface Duration {
    }

    public static abstract class Callback {
        public static final int DISMISS_EVENT_ACTION = 1;
        public static final int DISMISS_EVENT_CONSECUTIVE = 4;
        public static final int DISMISS_EVENT_MANUAL = 3;
        public static final int DISMISS_EVENT_SWIPE = 0;
        public static final int DISMISS_EVENT_TIMEOUT = 2;

        @Retention(RetentionPolicy.SOURCE)
        public @interface DismissEvent {
        }

        public void onDismissed(Snackbar snackbar, int i) {
        }

        public void onShown(Snackbar snackbar) {
        }
    }

    static {
        Handler handler;
        Handler.Callback callback;
        new Handler.Callback() {
            public boolean handleMessage(Message message) {
                Message message2 = message;
                switch (message2.what) {
                    case 0:
                        ((Snackbar) message2.obj).showView();
                        return true;
                    case 1:
                        ((Snackbar) message2.obj).hideView(message2.arg1);
                        return true;
                    default:
                        return false;
                }
            }
        };
        new Handler(Looper.getMainLooper(), callback);
        sHandler = handler;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private Snackbar(ViewGroup viewGroup) {
        SnackbarManager.Callback callback;
        ViewGroup viewGroup2 = viewGroup;
        new SnackbarManager.Callback() {
            public void show() {
                boolean sendMessage = Snackbar.sHandler.sendMessage(Snackbar.sHandler.obtainMessage(0, Snackbar.this));
            }

            public void dismiss(int i) {
                boolean sendMessage = Snackbar.sHandler.sendMessage(Snackbar.sHandler.obtainMessage(1, i, 0, Snackbar.this));
            }
        };
        this.mManagerCallback = callback;
        this.mTargetParent = viewGroup2;
        this.mContext = viewGroup2.getContext();
        ThemeUtils.checkAppCompatTheme(this.mContext);
    }

    @NonNull
    public static Snackbar make(@NonNull View view, @NonNull CharSequence charSequence, int i) {
        Snackbar snackbar;
        new Snackbar(findSuitableParent(view));
        Snackbar snackbar2 = snackbar;
        Snackbar text = snackbar2.setText(charSequence);
        Snackbar duration = snackbar2.setDuration(i);
        return snackbar2;
    }

    @NonNull
    public static Snackbar make(@NonNull View view, @StringRes int i, int i2) {
        View view2 = view;
        return make(view2, view2.getResources().getText(i), i2);
    }

    private static ViewGroup findSuitableParent(View view) {
        View view2 = view;
        ViewGroup viewGroup = null;
        while (!(view2 instanceof CoordinatorLayout)) {
            if (view2 instanceof FrameLayout) {
                if (view2.getId() == 16908290) {
                    return (ViewGroup) view2;
                }
                viewGroup = (ViewGroup) view2;
            }
            if (view2 != null) {
                Object parent = view2.getParent();
                view2 = parent instanceof View ? (View) parent : null;
            }
            if (view2 == null) {
                return viewGroup;
            }
        }
        return (ViewGroup) view2;
    }

    @NonNull
    public Snackbar setAction(@StringRes int i, View.OnClickListener onClickListener) {
        return setAction(this.mContext.getText(i), onClickListener);
    }

    @NonNull
    public Snackbar setAction(CharSequence charSequence, View.OnClickListener onClickListener) {
        View.OnClickListener onClickListener2;
        CharSequence charSequence2 = charSequence;
        View.OnClickListener onClickListener3 = onClickListener;
        Button actionView = this.mView.getActionView();
        if (TextUtils.isEmpty(charSequence2) || onClickListener3 == null) {
            actionView.setVisibility(8);
            actionView.setOnClickListener(null);
        } else {
            actionView.setVisibility(0);
            actionView.setText(charSequence2);
            final View.OnClickListener onClickListener4 = onClickListener3;
            new View.OnClickListener() {
                public void onClick(View view) {
                    onClickListener4.onClick(view);
                    Snackbar.this.dispatchDismiss(1);
                }
            };
            actionView.setOnClickListener(onClickListener2);
        }
        return this;
    }

    @NonNull
    public Snackbar setActionTextColor(ColorStateList colorStateList) {
        this.mView.getActionView().setTextColor(colorStateList);
        return this;
    }

    @NonNull
    public Snackbar setActionTextColor(@ColorInt int i) {
        this.mView.getActionView().setTextColor(i);
        return this;
    }

    @NonNull
    public Snackbar setText(@NonNull CharSequence charSequence) {
        this.mView.getMessageView().setText(charSequence);
        return this;
    }

    @NonNull
    public Snackbar setText(@StringRes int i) {
        return setText(this.mContext.getText(i));
    }

    @NonNull
    public Snackbar setDuration(int i) {
        this.mDuration = i;
        return this;
    }

    public int getDuration() {
        return this.mDuration;
    }

    @NonNull
    public View getView() {
        return this.mView;
    }

    public void show() {
        SnackbarManager.getInstance().show(this.mDuration, this.mManagerCallback);
    }

    public void dismiss() {
        dispatchDismiss(3);
    }

    /* access modifiers changed from: private */
    public void dispatchDismiss(int i) {
        SnackbarManager.getInstance().dismiss(this.mManagerCallback, i);
    }

    @NonNull
    public Snackbar setCallback(Callback callback) {
        this.mCallback = callback;
        return this;
    }

    public boolean isShown() {
        return SnackbarManager.getInstance().isCurrent(this.mManagerCallback);
    }

    public boolean isShownOrQueued() {
        return SnackbarManager.getInstance().isCurrentOrNext(this.mManagerCallback);
    }

    /* access modifiers changed from: package-private */
    public final void showView() {
        SnackbarLayout.OnAttachStateChangeListener onAttachStateChangeListener;
        SnackbarLayout.OnLayoutChangeListener onLayoutChangeListener;
        Behavior behavior;
        SwipeDismissBehavior.OnDismissListener onDismissListener;
        if (this.mView.getParent() == null) {
            ViewGroup.LayoutParams layoutParams = this.mView.getLayoutParams();
            if (layoutParams instanceof CoordinatorLayout.LayoutParams) {
                new Behavior();
                Behavior behavior2 = behavior;
                behavior2.setStartAlphaSwipeDistance(0.1f);
                behavior2.setEndAlphaSwipeDistance(0.6f);
                behavior2.setSwipeDirection(0);
                new SwipeDismissBehavior.OnDismissListener() {
                    public void onDismiss(View view) {
                        Snackbar.this.dispatchDismiss(0);
                    }

                    public void onDragStateChanged(int i) {
                        switch (i) {
                            case 0:
                                SnackbarManager.getInstance().restoreTimeout(Snackbar.this.mManagerCallback);
                                return;
                            case 1:
                            case 2:
                                SnackbarManager.getInstance().cancelTimeout(Snackbar.this.mManagerCallback);
                                return;
                            default:
                                return;
                        }
                    }
                };
                behavior2.setListener(onDismissListener);
                ((CoordinatorLayout.LayoutParams) layoutParams).setBehavior(behavior2);
            }
            this.mTargetParent.addView(this.mView);
        }
        new SnackbarLayout.OnAttachStateChangeListener() {
            public void onViewAttachedToWindow(View view) {
            }

            public void onViewDetachedFromWindow(View view) {
                Runnable runnable;
                if (Snackbar.this.isShownOrQueued()) {
                    new Runnable() {
                        public void run() {
                            Snackbar.this.onViewHidden(3);
                        }
                    };
                    boolean post = Snackbar.sHandler.post(runnable);
                }
            }
        };
        this.mView.setOnAttachStateChangeListener(onAttachStateChangeListener);
        if (ViewCompat.isLaidOut(this.mView)) {
            animateViewIn();
            return;
        }
        new SnackbarLayout.OnLayoutChangeListener() {
            public void onLayoutChange(View view, int i, int i2, int i3, int i4) {
                Snackbar.this.animateViewIn();
                Snackbar.this.mView.setOnLayoutChangeListener(null);
            }
        };
        this.mView.setOnLayoutChangeListener(onLayoutChangeListener);
    }

    /* access modifiers changed from: private */
    public void animateViewIn() {
        Animation.AnimationListener animationListener;
        ViewPropertyAnimatorListener viewPropertyAnimatorListener;
        if (Build.VERSION.SDK_INT >= 14) {
            ViewCompat.setTranslationY(this.mView, (float) this.mView.getHeight());
            new ViewPropertyAnimatorListenerAdapter() {
                public void onAnimationStart(View view) {
                    Snackbar.this.mView.animateChildrenIn(70, Snackbar.ANIMATION_FADE_DURATION);
                }

                public void onAnimationEnd(View view) {
                    if (Snackbar.this.mCallback != null) {
                        Snackbar.this.mCallback.onShown(Snackbar.this);
                    }
                    SnackbarManager.getInstance().onShown(Snackbar.this.mManagerCallback);
                }
            };
            ViewCompat.animate(this.mView).translationY(0.0f).setInterpolator(AnimationUtils.FAST_OUT_SLOW_IN_INTERPOLATOR).setDuration(250).setListener(viewPropertyAnimatorListener).start();
            return;
        }
        Animation loadAnimation = AnimationUtils.loadAnimation(this.mView.getContext(), R.anim.design_snackbar_in);
        loadAnimation.setInterpolator(AnimationUtils.FAST_OUT_SLOW_IN_INTERPOLATOR);
        loadAnimation.setDuration(250);
        new Animation.AnimationListener() {
            public void onAnimationEnd(Animation animation) {
                if (Snackbar.this.mCallback != null) {
                    Snackbar.this.mCallback.onShown(Snackbar.this);
                }
                SnackbarManager.getInstance().onShown(Snackbar.this.mManagerCallback);
            }

            public void onAnimationStart(Animation animation) {
            }

            public void onAnimationRepeat(Animation animation) {
            }
        };
        loadAnimation.setAnimationListener(animationListener);
        this.mView.startAnimation(loadAnimation);
    }

    private void animateViewOut(int i) {
        Animation.AnimationListener animationListener;
        ViewPropertyAnimatorListener viewPropertyAnimatorListener;
        int i2 = i;
        if (Build.VERSION.SDK_INT >= 14) {
            final int i3 = i2;
            new ViewPropertyAnimatorListenerAdapter() {
                public void onAnimationStart(View view) {
                    Snackbar.this.mView.animateChildrenOut(0, Snackbar.ANIMATION_FADE_DURATION);
                }

                public void onAnimationEnd(View view) {
                    Snackbar.this.onViewHidden(i3);
                }
            };
            ViewCompat.animate(this.mView).translationY((float) this.mView.getHeight()).setInterpolator(AnimationUtils.FAST_OUT_SLOW_IN_INTERPOLATOR).setDuration(250).setListener(viewPropertyAnimatorListener).start();
            return;
        }
        Animation loadAnimation = AnimationUtils.loadAnimation(this.mView.getContext(), R.anim.design_snackbar_out);
        loadAnimation.setInterpolator(AnimationUtils.FAST_OUT_SLOW_IN_INTERPOLATOR);
        loadAnimation.setDuration(250);
        final int i4 = i2;
        new Animation.AnimationListener() {
            public void onAnimationEnd(Animation animation) {
                Snackbar.this.onViewHidden(i4);
            }

            public void onAnimationStart(Animation animation) {
            }

            public void onAnimationRepeat(Animation animation) {
            }
        };
        loadAnimation.setAnimationListener(animationListener);
        this.mView.startAnimation(loadAnimation);
    }

    /* access modifiers changed from: package-private */
    public final void hideView(int i) {
        int i2 = i;
        if (this.mView.getVisibility() != 0 || isBeingDragged()) {
            onViewHidden(i2);
        } else {
            animateViewOut(i2);
        }
    }

    /* access modifiers changed from: private */
    public void onViewHidden(int i) {
        int i2 = i;
        SnackbarManager.getInstance().onDismissed(this.mManagerCallback);
        if (this.mCallback != null) {
            this.mCallback.onDismissed(this, i2);
        }
        ViewParent parent = this.mView.getParent();
        if (parent instanceof ViewGroup) {
            ((ViewGroup) parent).removeView(this.mView);
        }
    }

    private boolean isBeingDragged() {
        ViewGroup.LayoutParams layoutParams = this.mView.getLayoutParams();
        if (layoutParams instanceof CoordinatorLayout.LayoutParams) {
            CoordinatorLayout.Behavior behavior = ((CoordinatorLayout.LayoutParams) layoutParams).getBehavior();
            if (behavior instanceof SwipeDismissBehavior) {
                return ((SwipeDismissBehavior) behavior).getDragState() != 0;
            }
        }
        return false;
    }

    public static class SnackbarLayout extends LinearLayout {
        private Button mActionView;
        private int mMaxInlineActionWidth;
        private int mMaxWidth;
        private TextView mMessageView;
        private OnAttachStateChangeListener mOnAttachStateChangeListener;
        private OnLayoutChangeListener mOnLayoutChangeListener;

        interface OnAttachStateChangeListener {
            void onViewAttachedToWindow(View view);

            void onViewDetachedFromWindow(View view);
        }

        interface OnLayoutChangeListener {
            void onLayoutChange(View view, int i, int i2, int i3, int i4);
        }

        public SnackbarLayout(Context context) {
            this(context, null);
        }

        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public SnackbarLayout(android.content.Context r9, android.util.AttributeSet r10) {
            /*
                r8 = this;
                r0 = r8
                r1 = r9
                r2 = r10
                r4 = r0
                r5 = r1
                r6 = r2
                r4.<init>(r5, r6)
                r4 = r1
                r5 = r2
                int[] r6 = android.support.design.R.styleable.SnackbarLayout
                android.content.res.TypedArray r4 = r4.obtainStyledAttributes(r5, r6)
                r3 = r4
                r4 = r0
                r5 = r3
                int r6 = android.support.design.R.styleable.SnackbarLayout_android_maxWidth
                r7 = -1
                int r5 = r5.getDimensionPixelSize(r6, r7)
                r4.mMaxWidth = r5
                r4 = r0
                r5 = r3
                int r6 = android.support.design.R.styleable.SnackbarLayout_maxActionInlineWidth
                r7 = -1
                int r5 = r5.getDimensionPixelSize(r6, r7)
                r4.mMaxInlineActionWidth = r5
                r4 = r3
                int r5 = android.support.design.R.styleable.SnackbarLayout_elevation
                boolean r4 = r4.hasValue(r5)
                if (r4 == 0) goto L_0x003e
                r4 = r0
                r5 = r3
                int r6 = android.support.design.R.styleable.SnackbarLayout_elevation
                r7 = 0
                int r5 = r5.getDimensionPixelSize(r6, r7)
                float r5 = (float) r5
                android.support.v4.view.ViewCompat.setElevation(r4, r5)
            L_0x003e:
                r4 = r3
                r4.recycle()
                r4 = r0
                r5 = 1
                r4.setClickable(r5)
                r4 = r1
                android.view.LayoutInflater r4 = android.view.LayoutInflater.from(r4)
                int r5 = android.support.design.R.layout.design_layout_snackbar_include
                r6 = r0
                android.view.View r4 = r4.inflate(r5, r6)
                r4 = r0
                r5 = 1
                android.support.v4.view.ViewCompat.setAccessibilityLiveRegion(r4, r5)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: android.support.design.widget.Snackbar.SnackbarLayout.<init>(android.content.Context, android.util.AttributeSet):void");
        }

        /* access modifiers changed from: protected */
        public void onFinishInflate() {
            super.onFinishInflate();
            this.mMessageView = (TextView) findViewById(R.id.snackbar_text);
            this.mActionView = (Button) findViewById(R.id.snackbar_action);
        }

        /* access modifiers changed from: package-private */
        public TextView getMessageView() {
            return this.mMessageView;
        }

        /* access modifiers changed from: package-private */
        public Button getActionView() {
            return this.mActionView;
        }

        /* access modifiers changed from: protected */
        public void onMeasure(int i, int i2) {
            int i3 = i;
            int i4 = i2;
            super.onMeasure(i3, i4);
            if (this.mMaxWidth > 0 && getMeasuredWidth() > this.mMaxWidth) {
                i3 = View.MeasureSpec.makeMeasureSpec(this.mMaxWidth, 1073741824);
                super.onMeasure(i3, i4);
            }
            int dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.design_snackbar_padding_vertical_2lines);
            int dimensionPixelSize2 = getResources().getDimensionPixelSize(R.dimen.design_snackbar_padding_vertical);
            boolean z = this.mMessageView.getLayout().getLineCount() > 1;
            boolean z2 = false;
            if (!z || this.mMaxInlineActionWidth <= 0 || this.mActionView.getMeasuredWidth() <= this.mMaxInlineActionWidth) {
                int i5 = z ? dimensionPixelSize : dimensionPixelSize2;
                if (updateViewsWithinLayout(0, i5, i5)) {
                    z2 = true;
                }
            } else if (updateViewsWithinLayout(1, dimensionPixelSize, dimensionPixelSize - dimensionPixelSize2)) {
                z2 = true;
            }
            if (z2) {
                super.onMeasure(i3, i4);
            }
        }

        /* access modifiers changed from: package-private */
        public void animateChildrenIn(int i, int i2) {
            int i3 = i;
            int i4 = i2;
            ViewCompat.setAlpha(this.mMessageView, 0.0f);
            ViewCompat.animate(this.mMessageView).alpha(1.0f).setDuration((long) i4).setStartDelay((long) i3).start();
            if (this.mActionView.getVisibility() == 0) {
                ViewCompat.setAlpha(this.mActionView, 0.0f);
                ViewCompat.animate(this.mActionView).alpha(1.0f).setDuration((long) i4).setStartDelay((long) i3).start();
            }
        }

        /* access modifiers changed from: package-private */
        public void animateChildrenOut(int i, int i2) {
            int i3 = i;
            int i4 = i2;
            ViewCompat.setAlpha(this.mMessageView, 1.0f);
            ViewCompat.animate(this.mMessageView).alpha(0.0f).setDuration((long) i4).setStartDelay((long) i3).start();
            if (this.mActionView.getVisibility() == 0) {
                ViewCompat.setAlpha(this.mActionView, 1.0f);
                ViewCompat.animate(this.mActionView).alpha(0.0f).setDuration((long) i4).setStartDelay((long) i3).start();
            }
        }

        /* access modifiers changed from: protected */
        public void onLayout(boolean z, int i, int i2, int i3, int i4) {
            boolean z2 = z;
            int i5 = i;
            int i6 = i2;
            int i7 = i3;
            int i8 = i4;
            super.onLayout(z2, i5, i6, i7, i8);
            if (z2 && this.mOnLayoutChangeListener != null) {
                this.mOnLayoutChangeListener.onLayoutChange(this, i5, i6, i7, i8);
            }
        }

        /* access modifiers changed from: protected */
        public void onAttachedToWindow() {
            super.onAttachedToWindow();
            if (this.mOnAttachStateChangeListener != null) {
                this.mOnAttachStateChangeListener.onViewAttachedToWindow(this);
            }
        }

        /* access modifiers changed from: protected */
        public void onDetachedFromWindow() {
            super.onDetachedFromWindow();
            if (this.mOnAttachStateChangeListener != null) {
                this.mOnAttachStateChangeListener.onViewDetachedFromWindow(this);
            }
        }

        /* access modifiers changed from: package-private */
        public void setOnLayoutChangeListener(OnLayoutChangeListener onLayoutChangeListener) {
            this.mOnLayoutChangeListener = onLayoutChangeListener;
        }

        /* access modifiers changed from: package-private */
        public void setOnAttachStateChangeListener(OnAttachStateChangeListener onAttachStateChangeListener) {
            this.mOnAttachStateChangeListener = onAttachStateChangeListener;
        }

        private boolean updateViewsWithinLayout(int i, int i2, int i3) {
            int i4 = i;
            int i5 = i2;
            int i6 = i3;
            boolean z = false;
            if (i4 != getOrientation()) {
                setOrientation(i4);
                z = true;
            }
            if (!(this.mMessageView.getPaddingTop() == i5 && this.mMessageView.getPaddingBottom() == i6)) {
                updateTopBottomPadding(this.mMessageView, i5, i6);
                z = true;
            }
            return z;
        }

        private static void updateTopBottomPadding(View view, int i, int i2) {
            View view2 = view;
            int i3 = i;
            int i4 = i2;
            if (ViewCompat.isPaddingRelative(view2)) {
                ViewCompat.setPaddingRelative(view2, ViewCompat.getPaddingStart(view2), i3, ViewCompat.getPaddingEnd(view2), i4);
            } else {
                view2.setPadding(view2.getPaddingLeft(), i3, view2.getPaddingRight(), i4);
            }
        }
    }

    final class Behavior extends SwipeDismissBehavior<SnackbarLayout> {
        Behavior() {
        }

        public boolean canSwipeDismissView(View view) {
            return view instanceof SnackbarLayout;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.design.widget.SwipeDismissBehavior.onInterceptTouchEvent(android.support.design.widget.CoordinatorLayout, android.view.View, android.view.MotionEvent):boolean
         arg types: [android.support.design.widget.CoordinatorLayout, android.support.design.widget.Snackbar$SnackbarLayout, android.view.MotionEvent]
         candidates:
          android.support.design.widget.Snackbar.Behavior.onInterceptTouchEvent(android.support.design.widget.CoordinatorLayout, android.support.design.widget.Snackbar$SnackbarLayout, android.view.MotionEvent):boolean
          android.support.design.widget.SwipeDismissBehavior.onInterceptTouchEvent(android.support.design.widget.CoordinatorLayout, android.view.View, android.view.MotionEvent):boolean */
        public boolean onInterceptTouchEvent(CoordinatorLayout coordinatorLayout, SnackbarLayout snackbarLayout, MotionEvent motionEvent) {
            CoordinatorLayout coordinatorLayout2 = coordinatorLayout;
            SnackbarLayout snackbarLayout2 = snackbarLayout;
            MotionEvent motionEvent2 = motionEvent;
            if (coordinatorLayout2.isPointInChildBounds(snackbarLayout2, (int) motionEvent2.getX(), (int) motionEvent2.getY())) {
                switch (motionEvent2.getActionMasked()) {
                    case 0:
                        SnackbarManager.getInstance().cancelTimeout(Snackbar.this.mManagerCallback);
                        break;
                    case 1:
                    case 3:
                        SnackbarManager.getInstance().restoreTimeout(Snackbar.this.mManagerCallback);
                        break;
                }
            }
            return super.onInterceptTouchEvent(coordinatorLayout2, (View) snackbarLayout2, motionEvent2);
        }
    }
}
