package android.support.design.widget;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.ValueAnimatorCompat;
import android.support.v4.os.ParcelableCompat;
import android.support.v4.os.ParcelableCompatCreatorCallbacks;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.WindowInsetsCompat;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Interpolator;
import android.widget.LinearLayout;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.ref.WeakReference;
import java.util.List;

@CoordinatorLayout.DefaultBehavior(Behavior.class)
public class AppBarLayout extends LinearLayout {
    private static final int INVALID_SCROLL_RANGE = -1;
    private static final int PENDING_ACTION_ANIMATE_ENABLED = 4;
    private static final int PENDING_ACTION_COLLAPSED = 2;
    private static final int PENDING_ACTION_EXPANDED = 1;
    private static final int PENDING_ACTION_NONE = 0;
    private int mDownPreScrollRange;
    private int mDownScrollRange;
    boolean mHaveChildWithInterpolator;
    private WindowInsetsCompat mLastInsets;
    /* access modifiers changed from: private */
    public final List<OnOffsetChangedListener> mListeners;
    private int mPendingAction;
    private float mTargetElevation;
    private int mTotalScrollRange;

    public interface OnOffsetChangedListener {
        void onOffsetChanged(AppBarLayout appBarLayout, int i);
    }

    public AppBarLayout(Context context) {
        this(context, null);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AppBarLayout(android.content.Context r11, android.util.AttributeSet r12) {
        /*
            r10 = this;
            r0 = r10
            r1 = r11
            r2 = r12
            r4 = r0
            r5 = r1
            r6 = r2
            r4.<init>(r5, r6)
            r4 = r0
            r5 = -1
            r4.mTotalScrollRange = r5
            r4 = r0
            r5 = -1
            r4.mDownPreScrollRange = r5
            r4 = r0
            r5 = -1
            r4.mDownScrollRange = r5
            r4 = r0
            r5 = 0
            r4.mPendingAction = r5
            r4 = r0
            r5 = 1
            r4.setOrientation(r5)
            r4 = r1
            android.support.design.widget.ThemeUtils.checkAppCompatTheme(r4)
            r4 = r1
            r5 = r2
            int[] r6 = android.support.design.R.styleable.AppBarLayout
            r7 = 0
            int r8 = android.support.design.R.style.Widget_Design_AppBarLayout
            android.content.res.TypedArray r4 = r4.obtainStyledAttributes(r5, r6, r7, r8)
            r3 = r4
            r4 = r0
            r5 = r3
            int r6 = android.support.design.R.styleable.AppBarLayout_elevation
            r7 = 0
            int r5 = r5.getDimensionPixelSize(r6, r7)
            float r5 = (float) r5
            r4.mTargetElevation = r5
            r4 = r0
            r5 = r3
            int r6 = android.support.design.R.styleable.AppBarLayout_android_background
            android.graphics.drawable.Drawable r5 = r5.getDrawable(r6)
            r4.setBackgroundDrawable(r5)
            r4 = r3
            int r5 = android.support.design.R.styleable.AppBarLayout_expanded
            boolean r4 = r4.hasValue(r5)
            if (r4 == 0) goto L_0x005a
            r4 = r0
            r5 = r3
            int r6 = android.support.design.R.styleable.AppBarLayout_expanded
            r7 = 0
            boolean r5 = r5.getBoolean(r6, r7)
            r4.setExpanded(r5)
        L_0x005a:
            r4 = r3
            r4.recycle()
            r4 = r0
            android.support.design.widget.ViewUtils.setBoundsViewOutlineProvider(r4)
            r4 = r0
            java.util.ArrayList r5 = new java.util.ArrayList
            r9 = r5
            r5 = r9
            r6 = r9
            r6.<init>()
            r4.mListeners = r5
            r4 = r0
            r5 = r0
            float r5 = r5.mTargetElevation
            android.support.v4.view.ViewCompat.setElevation(r4, r5)
            r4 = r0
            android.support.design.widget.AppBarLayout$1 r5 = new android.support.design.widget.AppBarLayout$1
            r9 = r5
            r5 = r9
            r6 = r9
            r7 = r0
            r6.<init>()
            android.support.v4.view.ViewCompat.setOnApplyWindowInsetsListener(r4, r5)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.design.widget.AppBarLayout.<init>(android.content.Context, android.util.AttributeSet):void");
    }

    public void addOnOffsetChangedListener(OnOffsetChangedListener onOffsetChangedListener) {
        OnOffsetChangedListener onOffsetChangedListener2 = onOffsetChangedListener;
        if (onOffsetChangedListener2 != null && !this.mListeners.contains(onOffsetChangedListener2)) {
            boolean add = this.mListeners.add(onOffsetChangedListener2);
        }
    }

    public void removeOnOffsetChangedListener(OnOffsetChangedListener onOffsetChangedListener) {
        OnOffsetChangedListener onOffsetChangedListener2 = onOffsetChangedListener;
        if (onOffsetChangedListener2 != null) {
            boolean remove = this.mListeners.remove(onOffsetChangedListener2);
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        invalidateScrollRanges();
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        invalidateScrollRanges();
        this.mHaveChildWithInterpolator = false;
        int childCount = getChildCount();
        for (int i5 = 0; i5 < childCount; i5++) {
            if (((LayoutParams) getChildAt(i5).getLayoutParams()).getScrollInterpolator() != null) {
                this.mHaveChildWithInterpolator = true;
                return;
            }
        }
    }

    private void invalidateScrollRanges() {
        this.mTotalScrollRange = -1;
        this.mDownPreScrollRange = -1;
        this.mDownScrollRange = -1;
    }

    public void setOrientation(int i) {
        Throwable th;
        int i2 = i;
        if (i2 != 1) {
            Throwable th2 = th;
            new IllegalArgumentException("AppBarLayout is always vertical and does not support horizontal orientation");
            throw th2;
        }
        super.setOrientation(i2);
    }

    public void setExpanded(boolean z) {
        setExpanded(z, ViewCompat.isLaidOut(this));
    }

    public void setExpanded(boolean z, boolean z2) {
        this.mPendingAction = (z ? 1 : 2) | (z2 ? 4 : 0);
        requestLayout();
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof LayoutParams;
    }

    /* access modifiers changed from: protected */
    public LayoutParams generateDefaultLayoutParams() {
        LayoutParams layoutParams;
        new LayoutParams(-1, -2);
        return layoutParams;
    }

    public LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        LayoutParams layoutParams;
        new LayoutParams(getContext(), attributeSet);
        return layoutParams;
    }

    /* access modifiers changed from: protected */
    public LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        LayoutParams layoutParams2;
        LayoutParams layoutParams3;
        LayoutParams layoutParams4;
        ViewGroup.LayoutParams layoutParams5 = layoutParams;
        if (layoutParams5 instanceof LinearLayout.LayoutParams) {
            new LayoutParams((LinearLayout.LayoutParams) layoutParams5);
            return layoutParams4;
        } else if (layoutParams5 instanceof ViewGroup.MarginLayoutParams) {
            new LayoutParams((ViewGroup.MarginLayoutParams) layoutParams5);
            return layoutParams3;
        } else {
            new LayoutParams(layoutParams5);
            return layoutParams2;
        }
    }

    /* access modifiers changed from: private */
    public boolean hasChildWithInterpolator() {
        return this.mHaveChildWithInterpolator;
    }

    public final int getTotalScrollRange() {
        if (this.mTotalScrollRange != -1) {
            return this.mTotalScrollRange;
        }
        int i = 0;
        int i2 = 0;
        int childCount = getChildCount();
        while (true) {
            if (i2 >= childCount) {
                break;
            }
            View childAt = getChildAt(i2);
            LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
            int measuredHeight = childAt.getMeasuredHeight();
            int i3 = layoutParams.mScrollFlags;
            if ((i3 & 1) == 0) {
                break;
            }
            i += measuredHeight + layoutParams.topMargin + layoutParams.bottomMargin;
            if ((i3 & 2) != 0) {
                i -= ViewCompat.getMinimumHeight(childAt);
                break;
            }
            i2++;
        }
        int max = Math.max(0, i - getTopInset());
        this.mTotalScrollRange = max;
        return max;
    }

    /* access modifiers changed from: private */
    public boolean hasScrollableChildren() {
        return getTotalScrollRange() != 0;
    }

    /* access modifiers changed from: private */
    public int getUpNestedPreScrollRange() {
        return getTotalScrollRange();
    }

    /* access modifiers changed from: private */
    public int getDownNestedPreScrollRange() {
        if (this.mDownPreScrollRange != -1) {
            return this.mDownPreScrollRange;
        }
        int i = 0;
        for (int childCount = getChildCount() - 1; childCount >= 0; childCount--) {
            View childAt = getChildAt(childCount);
            LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
            int measuredHeight = childAt.getMeasuredHeight();
            int i2 = layoutParams.mScrollFlags;
            if ((i2 & 5) == 5) {
                int i3 = i + layoutParams.topMargin + layoutParams.bottomMargin;
                if ((i2 & 8) != 0) {
                    i = i3 + ViewCompat.getMinimumHeight(childAt);
                } else {
                    i = i3 + measuredHeight;
                }
            } else if (i > 0) {
                break;
            }
        }
        int i4 = i;
        this.mDownPreScrollRange = i4;
        return i4;
    }

    /* access modifiers changed from: private */
    public int getDownNestedScrollRange() {
        if (this.mDownScrollRange != -1) {
            return this.mDownScrollRange;
        }
        int i = 0;
        int i2 = 0;
        int childCount = getChildCount();
        while (true) {
            if (i2 >= childCount) {
                break;
            }
            View childAt = getChildAt(i2);
            LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
            int measuredHeight = childAt.getMeasuredHeight() + layoutParams.topMargin + layoutParams.bottomMargin;
            int i3 = layoutParams.mScrollFlags;
            if ((i3 & 1) == 0) {
                break;
            }
            i += measuredHeight;
            if ((i3 & 2) != 0) {
                i -= ViewCompat.getMinimumHeight(childAt) + getTopInset();
                break;
            }
            i2++;
        }
        int max = Math.max(0, i);
        this.mDownScrollRange = max;
        return max;
    }

    /* access modifiers changed from: package-private */
    public final int getMinimumHeightForVisibleOverlappingContent() {
        int systemWindowInsetTop = this.mLastInsets != null ? this.mLastInsets.getSystemWindowInsetTop() : 0;
        int minimumHeight = ViewCompat.getMinimumHeight(this);
        if (minimumHeight != 0) {
            return (minimumHeight * 2) + systemWindowInsetTop;
        }
        int childCount = getChildCount();
        return childCount >= 1 ? (ViewCompat.getMinimumHeight(getChildAt(childCount - 1)) * 2) + systemWindowInsetTop : 0;
    }

    public void setTargetElevation(float f) {
        this.mTargetElevation = f;
    }

    public float getTargetElevation() {
        return this.mTargetElevation;
    }

    /* access modifiers changed from: private */
    public int getPendingAction() {
        return this.mPendingAction;
    }

    /* access modifiers changed from: private */
    public void resetPendingAction() {
        this.mPendingAction = 0;
    }

    /* access modifiers changed from: private */
    public int getTopInset() {
        return this.mLastInsets != null ? this.mLastInsets.getSystemWindowInsetTop() : 0;
    }

    /* access modifiers changed from: private */
    public void setWindowInsets(WindowInsetsCompat windowInsetsCompat) {
        WindowInsetsCompat windowInsetsCompat2 = windowInsetsCompat;
        this.mTotalScrollRange = -1;
        this.mLastInsets = windowInsetsCompat2;
        int i = 0;
        int childCount = getChildCount();
        while (i < childCount) {
            windowInsetsCompat2 = ViewCompat.dispatchApplyWindowInsets(getChildAt(i), windowInsetsCompat2);
            if (!windowInsetsCompat2.isConsumed()) {
                i++;
            } else {
                return;
            }
        }
    }

    public static class LayoutParams extends LinearLayout.LayoutParams {
        static final int FLAG_QUICK_RETURN = 5;
        static final int FLAG_SNAP = 17;
        public static final int SCROLL_FLAG_ENTER_ALWAYS = 4;
        public static final int SCROLL_FLAG_ENTER_ALWAYS_COLLAPSED = 8;
        public static final int SCROLL_FLAG_EXIT_UNTIL_COLLAPSED = 2;
        public static final int SCROLL_FLAG_SCROLL = 1;
        public static final int SCROLL_FLAG_SNAP = 16;
        int mScrollFlags = 1;
        Interpolator mScrollInterpolator;

        @Retention(RetentionPolicy.SOURCE)
        public @interface ScrollFlags {
        }

        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public LayoutParams(android.content.Context r10, android.util.AttributeSet r11) {
            /*
                r9 = this;
                r0 = r9
                r1 = r10
                r2 = r11
                r5 = r0
                r6 = r1
                r7 = r2
                r5.<init>(r6, r7)
                r5 = r0
                r6 = 1
                r5.mScrollFlags = r6
                r5 = r1
                r6 = r2
                int[] r7 = android.support.design.R.styleable.AppBarLayout_LayoutParams
                android.content.res.TypedArray r5 = r5.obtainStyledAttributes(r6, r7)
                r3 = r5
                r5 = r0
                r6 = r3
                int r7 = android.support.design.R.styleable.AppBarLayout_LayoutParams_layout_scrollFlags
                r8 = 0
                int r6 = r6.getInt(r7, r8)
                r5.mScrollFlags = r6
                r5 = r3
                int r6 = android.support.design.R.styleable.AppBarLayout_LayoutParams_layout_scrollInterpolator
                boolean r5 = r5.hasValue(r6)
                if (r5 == 0) goto L_0x003c
                r5 = r3
                int r6 = android.support.design.R.styleable.AppBarLayout_LayoutParams_layout_scrollInterpolator
                r7 = 0
                int r5 = r5.getResourceId(r6, r7)
                r4 = r5
                r5 = r0
                r6 = r1
                r7 = r4
                android.view.animation.Interpolator r6 = android.view.animation.AnimationUtils.loadInterpolator(r6, r7)
                r5.mScrollInterpolator = r6
            L_0x003c:
                r5 = r3
                r5.recycle()
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: android.support.design.widget.AppBarLayout.LayoutParams.<init>(android.content.Context, android.util.AttributeSet):void");
        }

        public LayoutParams(int i, int i2) {
            super(i, i2);
        }

        public LayoutParams(int i, int i2, float f) {
            super(i, i2, f);
        }

        public LayoutParams(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
        }

        public LayoutParams(ViewGroup.MarginLayoutParams marginLayoutParams) {
            super(marginLayoutParams);
        }

        public LayoutParams(LinearLayout.LayoutParams layoutParams) {
            super(layoutParams);
        }

        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public LayoutParams(android.support.design.widget.AppBarLayout.LayoutParams r5) {
            /*
                r4 = this;
                r0 = r4
                r1 = r5
                r2 = r0
                r3 = r1
                r2.<init>(r3)
                r2 = r0
                r3 = 1
                r2.mScrollFlags = r3
                r2 = r0
                r3 = r1
                int r3 = r3.mScrollFlags
                r2.mScrollFlags = r3
                r2 = r0
                r3 = r1
                android.view.animation.Interpolator r3 = r3.mScrollInterpolator
                r2.mScrollInterpolator = r3
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: android.support.design.widget.AppBarLayout.LayoutParams.<init>(android.support.design.widget.AppBarLayout$LayoutParams):void");
        }

        public void setScrollFlags(int i) {
            this.mScrollFlags = i;
        }

        public int getScrollFlags() {
            return this.mScrollFlags;
        }

        public void setScrollInterpolator(Interpolator interpolator) {
            this.mScrollInterpolator = interpolator;
        }

        public Interpolator getScrollInterpolator() {
            return this.mScrollInterpolator;
        }
    }

    public static class Behavior extends HeaderBehavior<AppBarLayout> {
        private static final int ANIMATE_OFFSET_DIPS_PER_SECOND = 300;
        private static final int INVALID_POSITION = -1;
        private ValueAnimatorCompat mAnimator;
        private WeakReference<View> mLastNestedScrollingChildRef;
        private int mOffsetDelta;
        private int mOffsetToChildIndexOnLayout = -1;
        private boolean mOffsetToChildIndexOnLayoutIsMinHeight;
        private float mOffsetToChildIndexOnLayoutPerc;
        private DragCallback mOnDragCallback;
        private boolean mSkipNestedPreScroll;
        private boolean mWasFlung;

        public static abstract class DragCallback {
            public abstract boolean canDrag(@NonNull AppBarLayout appBarLayout);
        }

        public /* bridge */ /* synthetic */ int getLeftAndRightOffset() {
            return super.getLeftAndRightOffset();
        }

        public /* bridge */ /* synthetic */ int getTopAndBottomOffset() {
            return super.getTopAndBottomOffset();
        }

        public /* bridge */ /* synthetic */ boolean setLeftAndRightOffset(int i) {
            return super.setLeftAndRightOffset(i);
        }

        public /* bridge */ /* synthetic */ boolean setTopAndBottomOffset(int i) {
            return super.setTopAndBottomOffset(i);
        }

        public Behavior() {
        }

        public Behavior(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        public boolean onStartNestedScroll(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout, View view, View view2, int i) {
            AppBarLayout appBarLayout2 = appBarLayout;
            boolean z = (i & 2) != 0 && appBarLayout2.hasScrollableChildren() && coordinatorLayout.getHeight() - view.getHeight() <= appBarLayout2.getHeight();
            if (z && this.mAnimator != null) {
                this.mAnimator.cancel();
            }
            this.mLastNestedScrollingChildRef = null;
            return z;
        }

        public void onNestedPreScroll(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout, View view, int i, int i2, int[] iArr) {
            int i3;
            int i4;
            CoordinatorLayout coordinatorLayout2 = coordinatorLayout;
            AppBarLayout appBarLayout2 = appBarLayout;
            int i5 = i2;
            int[] iArr2 = iArr;
            if (i5 != 0 && !this.mSkipNestedPreScroll) {
                if (i5 < 0) {
                    i3 = -appBarLayout2.getTotalScrollRange();
                    i4 = i3 + appBarLayout2.getDownNestedPreScrollRange();
                } else {
                    i3 = -appBarLayout2.getUpNestedPreScrollRange();
                    i4 = 0;
                }
                iArr2[1] = scroll(coordinatorLayout2, appBarLayout2, i5, i3, i4);
            }
        }

        public void onNestedScroll(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout, View view, int i, int i2, int i3, int i4) {
            CoordinatorLayout coordinatorLayout2 = coordinatorLayout;
            AppBarLayout appBarLayout2 = appBarLayout;
            int i5 = i4;
            if (i5 < 0) {
                int scroll = scroll(coordinatorLayout2, appBarLayout2, i5, -appBarLayout2.getDownNestedScrollRange(), 0);
                this.mSkipNestedPreScroll = true;
                return;
            }
            this.mSkipNestedPreScroll = false;
        }

        public void onStopNestedScroll(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout, View view) {
            WeakReference<View> weakReference;
            CoordinatorLayout coordinatorLayout2 = coordinatorLayout;
            AppBarLayout appBarLayout2 = appBarLayout;
            View view2 = view;
            if (!this.mWasFlung) {
                snapToChildIfNeeded(coordinatorLayout2, appBarLayout2);
            }
            this.mSkipNestedPreScroll = false;
            this.mWasFlung = false;
            new WeakReference<>(view2);
            this.mLastNestedScrollingChildRef = weakReference;
        }

        public boolean onNestedFling(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout, View view, float f, float f2, boolean z) {
            CoordinatorLayout coordinatorLayout2 = coordinatorLayout;
            AppBarLayout appBarLayout2 = appBarLayout;
            float f3 = f2;
            boolean z2 = false;
            if (!z) {
                z2 = fling(coordinatorLayout2, appBarLayout2, -appBarLayout2.getTotalScrollRange(), 0, -f3);
            } else if (f3 < 0.0f) {
                int access$200 = (-appBarLayout2.getTotalScrollRange()) + appBarLayout2.getDownNestedPreScrollRange();
                if (getTopBottomOffsetForScrollingSibling() < access$200) {
                    animateOffsetTo(coordinatorLayout2, appBarLayout2, access$200);
                    z2 = true;
                }
            } else {
                int i = -appBarLayout2.getUpNestedPreScrollRange();
                if (getTopBottomOffsetForScrollingSibling() > i) {
                    animateOffsetTo(coordinatorLayout2, appBarLayout2, i);
                    z2 = true;
                }
            }
            this.mWasFlung = z2;
            return z2;
        }

        public void setDragCallback(@Nullable DragCallback dragCallback) {
            this.mOnDragCallback = dragCallback;
        }

        private void animateOffsetTo(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout, int i) {
            ValueAnimatorCompat.AnimatorUpdateListener animatorUpdateListener;
            CoordinatorLayout coordinatorLayout2 = coordinatorLayout;
            AppBarLayout appBarLayout2 = appBarLayout;
            int i2 = i;
            int topBottomOffsetForScrollingSibling = getTopBottomOffsetForScrollingSibling();
            if (topBottomOffsetForScrollingSibling != i2) {
                if (this.mAnimator == null) {
                    this.mAnimator = ViewUtils.createAnimator();
                    this.mAnimator.setInterpolator(AnimationUtils.DECELERATE_INTERPOLATOR);
                    final CoordinatorLayout coordinatorLayout3 = coordinatorLayout2;
                    final AppBarLayout appBarLayout3 = appBarLayout2;
                    new ValueAnimatorCompat.AnimatorUpdateListener() {
                        public void onAnimationUpdate(ValueAnimatorCompat valueAnimatorCompat) {
                            int headerTopBottomOffset = Behavior.this.setHeaderTopBottomOffset(coordinatorLayout3, appBarLayout3, valueAnimatorCompat.getAnimatedIntValue());
                        }
                    };
                    this.mAnimator.setUpdateListener(animatorUpdateListener);
                } else {
                    this.mAnimator.cancel();
                }
                this.mAnimator.setDuration(Math.round(((((float) Math.abs(topBottomOffsetForScrollingSibling - i2)) / coordinatorLayout2.getResources().getDisplayMetrics().density) * 1000.0f) / 300.0f));
                this.mAnimator.setIntValues(topBottomOffsetForScrollingSibling, i2);
                this.mAnimator.start();
            } else if (this.mAnimator != null && this.mAnimator.isRunning()) {
                this.mAnimator.cancel();
            }
        }

        private View getChildOnOffset(AppBarLayout appBarLayout, int i) {
            AppBarLayout appBarLayout2 = appBarLayout;
            int i2 = i;
            int childCount = appBarLayout2.getChildCount();
            for (int i3 = 0; i3 < childCount; i3++) {
                View childAt = appBarLayout2.getChildAt(i3);
                if (childAt.getTop() <= (-i2) && childAt.getBottom() >= (-i2)) {
                    return childAt;
                }
            }
            return null;
        }

        private void snapToChildIfNeeded(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout) {
            CoordinatorLayout coordinatorLayout2 = coordinatorLayout;
            AppBarLayout appBarLayout2 = appBarLayout;
            int topBottomOffsetForScrollingSibling = getTopBottomOffsetForScrollingSibling();
            View childOnOffset = getChildOnOffset(appBarLayout2, topBottomOffsetForScrollingSibling);
            if (childOnOffset != null) {
                LayoutParams layoutParams = (LayoutParams) childOnOffset.getLayoutParams();
                if ((layoutParams.getScrollFlags() & 17) == 17) {
                    int i = -childOnOffset.getTop();
                    int i2 = -childOnOffset.getBottom();
                    if ((layoutParams.getScrollFlags() & 2) == 2) {
                        i2 += ViewCompat.getMinimumHeight(childOnOffset);
                    }
                    animateOffsetTo(coordinatorLayout2, appBarLayout2, MathUtils.constrain(topBottomOffsetForScrollingSibling < (i2 + i) / 2 ? i2 : i, -appBarLayout2.getTotalScrollRange(), 0));
                }
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.design.widget.ViewOffsetBehavior.onLayoutChild(android.support.design.widget.CoordinatorLayout, android.view.View, int):boolean
         arg types: [android.support.design.widget.CoordinatorLayout, android.support.design.widget.AppBarLayout, int]
         candidates:
          android.support.design.widget.AppBarLayout.Behavior.onLayoutChild(android.support.design.widget.CoordinatorLayout, android.support.design.widget.AppBarLayout, int):boolean
          android.support.design.widget.ViewOffsetBehavior.onLayoutChild(android.support.design.widget.CoordinatorLayout, android.view.View, int):boolean */
        public boolean onLayoutChild(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout, int i) {
            int round;
            CoordinatorLayout coordinatorLayout2 = coordinatorLayout;
            AppBarLayout appBarLayout2 = appBarLayout;
            boolean onLayoutChild = super.onLayoutChild(coordinatorLayout2, (View) appBarLayout2, i);
            int access$500 = appBarLayout2.getPendingAction();
            if (access$500 != 0) {
                boolean z = (access$500 & 4) != 0;
                if ((access$500 & 2) != 0) {
                    int i2 = -appBarLayout2.getUpNestedPreScrollRange();
                    if (z) {
                        animateOffsetTo(coordinatorLayout2, appBarLayout2, i2);
                    } else {
                        int headerTopBottomOffset = setHeaderTopBottomOffset(coordinatorLayout2, appBarLayout2, i2);
                    }
                } else if ((access$500 & 1) != 0) {
                    if (z) {
                        animateOffsetTo(coordinatorLayout2, appBarLayout2, 0);
                    } else {
                        int headerTopBottomOffset2 = setHeaderTopBottomOffset(coordinatorLayout2, appBarLayout2, 0);
                    }
                }
            } else if (this.mOffsetToChildIndexOnLayout >= 0) {
                View childAt = appBarLayout2.getChildAt(this.mOffsetToChildIndexOnLayout);
                int i3 = -childAt.getBottom();
                if (this.mOffsetToChildIndexOnLayoutIsMinHeight) {
                    round = i3 + ViewCompat.getMinimumHeight(childAt);
                } else {
                    round = i3 + Math.round(((float) childAt.getHeight()) * this.mOffsetToChildIndexOnLayoutPerc);
                }
                boolean topAndBottomOffset = setTopAndBottomOffset(round);
            }
            appBarLayout2.resetPendingAction();
            this.mOffsetToChildIndexOnLayout = -1;
            dispatchOffsetUpdates(appBarLayout2);
            return onLayoutChild;
        }

        /* access modifiers changed from: package-private */
        public boolean canDragView(AppBarLayout appBarLayout) {
            AppBarLayout appBarLayout2 = appBarLayout;
            if (this.mOnDragCallback != null) {
                return this.mOnDragCallback.canDrag(appBarLayout2);
            }
            if (this.mLastNestedScrollingChildRef == null) {
                return true;
            }
            View view = this.mLastNestedScrollingChildRef.get();
            return view != null && view.isShown() && !ViewCompat.canScrollVertically(view, -1);
        }

        /* access modifiers changed from: package-private */
        public int getMaxDragOffset(AppBarLayout appBarLayout) {
            return -appBarLayout.getDownNestedScrollRange();
        }

        /* access modifiers changed from: package-private */
        public int getScrollRangeForDragFling(AppBarLayout appBarLayout) {
            return appBarLayout.getTotalScrollRange();
        }

        /* access modifiers changed from: package-private */
        public int setHeaderTopBottomOffset(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout, int i, int i2, int i3) {
            CoordinatorLayout coordinatorLayout2 = coordinatorLayout;
            AppBarLayout appBarLayout2 = appBarLayout;
            int i4 = i;
            int i5 = i2;
            int i6 = i3;
            int topBottomOffsetForScrollingSibling = getTopBottomOffsetForScrollingSibling();
            int i7 = 0;
            if (i5 != 0 && topBottomOffsetForScrollingSibling >= i5 && topBottomOffsetForScrollingSibling <= i6) {
                int constrain = MathUtils.constrain(i4, i5, i6);
                AppBarLayout appBarLayout3 = appBarLayout2;
                if (topBottomOffsetForScrollingSibling != constrain) {
                    int interpolateOffset = appBarLayout3.hasChildWithInterpolator() ? interpolateOffset(appBarLayout3, constrain) : constrain;
                    boolean topAndBottomOffset = setTopAndBottomOffset(interpolateOffset);
                    i7 = topBottomOffsetForScrollingSibling - constrain;
                    this.mOffsetDelta = constrain - interpolateOffset;
                    if (!topAndBottomOffset && appBarLayout3.hasChildWithInterpolator()) {
                        coordinatorLayout2.dispatchDependentViewsChanged(appBarLayout3);
                    }
                    dispatchOffsetUpdates(appBarLayout3);
                }
            }
            return i7;
        }

        private void dispatchOffsetUpdates(AppBarLayout appBarLayout) {
            AppBarLayout appBarLayout2 = appBarLayout;
            List access$800 = appBarLayout2.mListeners;
            int size = access$800.size();
            for (int i = 0; i < size; i++) {
                OnOffsetChangedListener onOffsetChangedListener = (OnOffsetChangedListener) access$800.get(i);
                if (onOffsetChangedListener != null) {
                    onOffsetChangedListener.onOffsetChanged(appBarLayout2, getTopAndBottomOffset());
                }
            }
        }

        private int interpolateOffset(AppBarLayout appBarLayout, int i) {
            AppBarLayout appBarLayout2 = appBarLayout;
            int i2 = i;
            int abs = Math.abs(i2);
            int i3 = 0;
            int childCount = appBarLayout2.getChildCount();
            while (true) {
                if (i3 >= childCount) {
                    break;
                }
                View childAt = appBarLayout2.getChildAt(i3);
                LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                Interpolator scrollInterpolator = layoutParams.getScrollInterpolator();
                if (abs < childAt.getTop() || abs > childAt.getBottom()) {
                    i3++;
                } else if (scrollInterpolator != null) {
                    int i4 = 0;
                    int scrollFlags = layoutParams.getScrollFlags();
                    if ((scrollFlags & 1) != 0) {
                        i4 = 0 + childAt.getHeight() + layoutParams.topMargin + layoutParams.bottomMargin;
                        if ((scrollFlags & 2) != 0) {
                            i4 -= ViewCompat.getMinimumHeight(childAt);
                        }
                    }
                    if (ViewCompat.getFitsSystemWindows(childAt)) {
                        i4 -= appBarLayout2.getTopInset();
                    }
                    if (i4 > 0) {
                        return Integer.signum(i2) * (childAt.getTop() + Math.round(((float) i4) * scrollInterpolator.getInterpolation(((float) (abs - childAt.getTop())) / ((float) i4))));
                    }
                }
            }
            return i2;
        }

        /* access modifiers changed from: package-private */
        public int getTopBottomOffsetForScrollingSibling() {
            return getTopAndBottomOffset() + this.mOffsetDelta;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.design.widget.CoordinatorLayout.Behavior.onSaveInstanceState(android.support.design.widget.CoordinatorLayout, android.view.View):android.os.Parcelable
         arg types: [android.support.design.widget.CoordinatorLayout, android.support.design.widget.AppBarLayout]
         candidates:
          android.support.design.widget.AppBarLayout.Behavior.onSaveInstanceState(android.support.design.widget.CoordinatorLayout, android.support.design.widget.AppBarLayout):android.os.Parcelable
          android.support.design.widget.CoordinatorLayout.Behavior.onSaveInstanceState(android.support.design.widget.CoordinatorLayout, android.view.View):android.os.Parcelable */
        public Parcelable onSaveInstanceState(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout) {
            SavedState savedState;
            AppBarLayout appBarLayout2 = appBarLayout;
            Parcelable onSaveInstanceState = super.onSaveInstanceState(coordinatorLayout, (View) appBarLayout2);
            int topAndBottomOffset = getTopAndBottomOffset();
            int i = 0;
            int childCount = appBarLayout2.getChildCount();
            while (i < childCount) {
                View childAt = appBarLayout2.getChildAt(i);
                int bottom = childAt.getBottom() + topAndBottomOffset;
                if (childAt.getTop() + topAndBottomOffset > 0 || bottom < 0) {
                    i++;
                } else {
                    new SavedState(onSaveInstanceState);
                    SavedState savedState2 = savedState;
                    savedState2.firstVisibleChildIndex = i;
                    savedState2.firstVisibileChildAtMinimumHeight = bottom == ViewCompat.getMinimumHeight(childAt);
                    savedState2.firstVisibileChildPercentageShown = ((float) bottom) / ((float) childAt.getHeight());
                    return savedState2;
                }
            }
            return onSaveInstanceState;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.design.widget.CoordinatorLayout.Behavior.onRestoreInstanceState(android.support.design.widget.CoordinatorLayout, android.view.View, android.os.Parcelable):void
         arg types: [android.support.design.widget.CoordinatorLayout, android.support.design.widget.AppBarLayout, android.os.Parcelable]
         candidates:
          android.support.design.widget.AppBarLayout.Behavior.onRestoreInstanceState(android.support.design.widget.CoordinatorLayout, android.support.design.widget.AppBarLayout, android.os.Parcelable):void
          android.support.design.widget.CoordinatorLayout.Behavior.onRestoreInstanceState(android.support.design.widget.CoordinatorLayout, android.view.View, android.os.Parcelable):void */
        public void onRestoreInstanceState(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout, Parcelable parcelable) {
            CoordinatorLayout coordinatorLayout2 = coordinatorLayout;
            AppBarLayout appBarLayout2 = appBarLayout;
            Parcelable parcelable2 = parcelable;
            if (parcelable2 instanceof SavedState) {
                SavedState savedState = (SavedState) parcelable2;
                super.onRestoreInstanceState(coordinatorLayout2, (View) appBarLayout2, savedState.getSuperState());
                this.mOffsetToChildIndexOnLayout = savedState.firstVisibleChildIndex;
                this.mOffsetToChildIndexOnLayoutPerc = savedState.firstVisibileChildPercentageShown;
                this.mOffsetToChildIndexOnLayoutIsMinHeight = savedState.firstVisibileChildAtMinimumHeight;
                return;
            }
            super.onRestoreInstanceState(coordinatorLayout2, (View) appBarLayout2, parcelable2);
            this.mOffsetToChildIndexOnLayout = -1;
        }

        protected static class SavedState extends View.BaseSavedState {
            public static final Parcelable.Creator<SavedState> CREATOR;
            boolean firstVisibileChildAtMinimumHeight;
            float firstVisibileChildPercentageShown;
            int firstVisibleChildIndex;

            /* JADX WARNING: Illegal instructions before constructor call */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public SavedState(android.os.Parcel r6, java.lang.ClassLoader r7) {
                /*
                    r5 = this;
                    r0 = r5
                    r1 = r6
                    r2 = r7
                    r3 = r0
                    r4 = r1
                    r3.<init>(r4)
                    r3 = r0
                    r4 = r1
                    int r4 = r4.readInt()
                    r3.firstVisibleChildIndex = r4
                    r3 = r0
                    r4 = r1
                    float r4 = r4.readFloat()
                    r3.firstVisibileChildPercentageShown = r4
                    r3 = r0
                    r4 = r1
                    byte r4 = r4.readByte()
                    if (r4 == 0) goto L_0x0024
                    r4 = 1
                L_0x0021:
                    r3.firstVisibileChildAtMinimumHeight = r4
                    return
                L_0x0024:
                    r4 = 0
                    goto L_0x0021
                */
                throw new UnsupportedOperationException("Method not decompiled: android.support.design.widget.AppBarLayout.Behavior.SavedState.<init>(android.os.Parcel, java.lang.ClassLoader):void");
            }

            public SavedState(Parcelable parcelable) {
                super(parcelable);
            }

            public void writeToParcel(Parcel parcel, int i) {
                Parcel parcel2 = parcel;
                super.writeToParcel(parcel2, i);
                parcel2.writeInt(this.firstVisibleChildIndex);
                parcel2.writeFloat(this.firstVisibileChildPercentageShown);
                parcel2.writeByte((byte) (this.firstVisibileChildAtMinimumHeight ? 1 : 0));
            }

            static {
                Object obj;
                new ParcelableCompatCreatorCallbacks<SavedState>() {
                    public SavedState createFromParcel(Parcel parcel, ClassLoader classLoader) {
                        SavedState savedState;
                        new SavedState(parcel, classLoader);
                        return savedState;
                    }

                    public SavedState[] newArray(int i) {
                        return new SavedState[i];
                    }
                };
                CREATOR = ParcelableCompat.newCreator(obj);
            }
        }
    }

    public static class ScrollingViewBehavior extends HeaderScrollingViewBehavior {
        private int mOverlayTop;

        public /* bridge */ /* synthetic */ int getLeftAndRightOffset() {
            return super.getLeftAndRightOffset();
        }

        public /* bridge */ /* synthetic */ int getTopAndBottomOffset() {
            return super.getTopAndBottomOffset();
        }

        public /* bridge */ /* synthetic */ boolean onMeasureChild(CoordinatorLayout coordinatorLayout, View view, int i, int i2, int i3, int i4) {
            return super.onMeasureChild(coordinatorLayout, view, i, i2, i3, i4);
        }

        public /* bridge */ /* synthetic */ boolean setLeftAndRightOffset(int i) {
            return super.setLeftAndRightOffset(i);
        }

        public /* bridge */ /* synthetic */ boolean setTopAndBottomOffset(int i) {
            return super.setTopAndBottomOffset(i);
        }

        public ScrollingViewBehavior() {
        }

        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public ScrollingViewBehavior(android.content.Context r9, android.util.AttributeSet r10) {
            /*
                r8 = this;
                r0 = r8
                r1 = r9
                r2 = r10
                r4 = r0
                r5 = r1
                r6 = r2
                r4.<init>(r5, r6)
                r4 = r1
                r5 = r2
                int[] r6 = android.support.design.R.styleable.ScrollingViewBehavior_Params
                android.content.res.TypedArray r4 = r4.obtainStyledAttributes(r5, r6)
                r3 = r4
                r4 = r0
                r5 = r3
                int r6 = android.support.design.R.styleable.ScrollingViewBehavior_Params_behavior_overlapTop
                r7 = 0
                int r5 = r5.getDimensionPixelSize(r6, r7)
                r4.mOverlayTop = r5
                r4 = r3
                r4.recycle()
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: android.support.design.widget.AppBarLayout.ScrollingViewBehavior.<init>(android.content.Context, android.util.AttributeSet):void");
        }

        public boolean layoutDependsOn(CoordinatorLayout coordinatorLayout, View view, View view2) {
            return view2 instanceof AppBarLayout;
        }

        public boolean onLayoutChild(CoordinatorLayout coordinatorLayout, View view, int i) {
            CoordinatorLayout coordinatorLayout2 = coordinatorLayout;
            View view2 = view;
            boolean onLayoutChild = super.onLayoutChild(coordinatorLayout2, view2, i);
            List<View> dependencies = coordinatorLayout2.getDependencies(view2);
            int i2 = 0;
            int size = dependencies.size();
            while (i2 < size && !updateOffset(coordinatorLayout2, view2, dependencies.get(i2))) {
                i2++;
            }
            return true;
        }

        public boolean onDependentViewChanged(CoordinatorLayout coordinatorLayout, View view, View view2) {
            boolean updateOffset = updateOffset(coordinatorLayout, view, view2);
            return false;
        }

        private boolean updateOffset(CoordinatorLayout coordinatorLayout, View view, View view2) {
            View view3 = view2;
            CoordinatorLayout.Behavior behavior = ((CoordinatorLayout.LayoutParams) view3.getLayoutParams()).getBehavior();
            if (!(behavior instanceof Behavior)) {
                return false;
            }
            int topBottomOffsetForScrollingSibling = ((Behavior) behavior).getTopBottomOffsetForScrollingSibling();
            boolean topAndBottomOffset = setTopAndBottomOffset((view3.getHeight() + topBottomOffsetForScrollingSibling) - getOverlapForOffset(view3, topBottomOffsetForScrollingSibling));
            return true;
        }

        private int getOverlapForOffset(View view, int i) {
            View view2 = view;
            int i2 = i;
            if (this.mOverlayTop != 0 && (view2 instanceof AppBarLayout)) {
                AppBarLayout appBarLayout = (AppBarLayout) view2;
                int totalScrollRange = appBarLayout.getTotalScrollRange();
                int access$200 = appBarLayout.getDownNestedPreScrollRange();
                if (access$200 != 0 && totalScrollRange + i2 <= access$200) {
                    return 0;
                }
                int i3 = totalScrollRange - access$200;
                if (i3 != 0) {
                    return MathUtils.constrain(Math.round((1.0f + (((float) i2) / ((float) i3))) * ((float) this.mOverlayTop)), 0, this.mOverlayTop);
                }
            }
            return this.mOverlayTop;
        }

        public void setOverlayTop(int i) {
            this.mOverlayTop = i;
        }

        public int getOverlayTop() {
            return this.mOverlayTop;
        }

        /* access modifiers changed from: package-private */
        public View findFirstDependency(List<View> list) {
            List<View> list2 = list;
            int size = list2.size();
            for (int i = 0; i < size; i++) {
                View view = list2.get(i);
                if (view instanceof AppBarLayout) {
                    return view;
                }
            }
            return null;
        }

        /* access modifiers changed from: package-private */
        public int getScrollRange(View view) {
            View view2 = view;
            if (view2 instanceof AppBarLayout) {
                return ((AppBarLayout) view2).getTotalScrollRange();
            }
            return super.getScrollRange(view2);
        }
    }
}
