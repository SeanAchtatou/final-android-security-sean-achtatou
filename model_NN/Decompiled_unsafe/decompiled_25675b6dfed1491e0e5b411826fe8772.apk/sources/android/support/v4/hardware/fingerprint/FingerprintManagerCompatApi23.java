package android.support.v4.hardware.fingerprint;

import android.content.Context;
import android.hardware.fingerprint.FingerprintManager;
import android.os.CancellationSignal;
import android.os.Handler;
import java.security.Signature;
import javax.crypto.Cipher;
import javax.crypto.Mac;

public final class FingerprintManagerCompatApi23 {
    private static FingerprintManager getFingerprintManager(Context context) {
        return (FingerprintManager) context.getSystemService(FingerprintManager.class);
    }

    public static boolean hasEnrolledFingerprints(Context context) {
        return getFingerprintManager(context).hasEnrolledFingerprints();
    }

    public static boolean isHardwareDetected(Context context) {
        return getFingerprintManager(context).isHardwareDetected();
    }

    public static void authenticate(Context context, CryptoObject cryptoObject, int i, Object obj, AuthenticationCallback authenticationCallback, Handler handler) {
        getFingerprintManager(context).authenticate(wrapCryptoObject(cryptoObject), (CancellationSignal) obj, i, wrapCallback(authenticationCallback), handler);
    }

    private static FingerprintManager.CryptoObject wrapCryptoObject(CryptoObject cryptoObject) {
        FingerprintManager.CryptoObject cryptoObject2;
        FingerprintManager.CryptoObject cryptoObject3;
        FingerprintManager.CryptoObject cryptoObject4;
        CryptoObject cryptoObject5 = cryptoObject;
        if (cryptoObject5 == null) {
            return null;
        }
        if (cryptoObject5.getCipher() != null) {
            new FingerprintManager.CryptoObject(cryptoObject5.getCipher());
            return cryptoObject4;
        } else if (cryptoObject5.getSignature() != null) {
            new FingerprintManager.CryptoObject(cryptoObject5.getSignature());
            return cryptoObject3;
        } else if (cryptoObject5.getMac() == null) {
            return null;
        } else {
            new FingerprintManager.CryptoObject(cryptoObject5.getMac());
            return cryptoObject2;
        }
    }

    /* access modifiers changed from: private */
    public static CryptoObject unwrapCryptoObject(FingerprintManager.CryptoObject cryptoObject) {
        CryptoObject cryptoObject2;
        CryptoObject cryptoObject3;
        CryptoObject cryptoObject4;
        FingerprintManager.CryptoObject cryptoObject5 = cryptoObject;
        if (cryptoObject5 == null) {
            return null;
        }
        if (cryptoObject5.getCipher() != null) {
            new CryptoObject(cryptoObject5.getCipher());
            return cryptoObject4;
        } else if (cryptoObject5.getSignature() != null) {
            new CryptoObject(cryptoObject5.getSignature());
            return cryptoObject3;
        } else if (cryptoObject5.getMac() == null) {
            return null;
        } else {
            new CryptoObject(cryptoObject5.getMac());
            return cryptoObject2;
        }
    }

    private static FingerprintManager.AuthenticationCallback wrapCallback(AuthenticationCallback authenticationCallback) {
        FingerprintManager.AuthenticationCallback authenticationCallback2;
        final AuthenticationCallback authenticationCallback3 = authenticationCallback;
        new FingerprintManager.AuthenticationCallback() {
            public void onAuthenticationError(int i, CharSequence charSequence) {
                authenticationCallback3.onAuthenticationError(i, charSequence);
            }

            public void onAuthenticationHelp(int i, CharSequence charSequence) {
                authenticationCallback3.onAuthenticationHelp(i, charSequence);
            }

            public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult authenticationResult) {
                AuthenticationResultInternal authenticationResultInternal;
                new AuthenticationResultInternal(FingerprintManagerCompatApi23.unwrapCryptoObject(authenticationResult.getCryptoObject()));
                authenticationCallback3.onAuthenticationSucceeded(authenticationResultInternal);
            }

            public void onAuthenticationFailed() {
                authenticationCallback3.onAuthenticationFailed();
            }
        };
        return authenticationCallback2;
    }

    public static class CryptoObject {
        private final Cipher mCipher;
        private final Mac mMac;
        private final Signature mSignature;

        public CryptoObject(Signature signature) {
            this.mSignature = signature;
            this.mCipher = null;
            this.mMac = null;
        }

        public CryptoObject(Cipher cipher) {
            this.mCipher = cipher;
            this.mSignature = null;
            this.mMac = null;
        }

        public CryptoObject(Mac mac) {
            this.mMac = mac;
            this.mCipher = null;
            this.mSignature = null;
        }

        public Signature getSignature() {
            return this.mSignature;
        }

        public Cipher getCipher() {
            return this.mCipher;
        }

        public Mac getMac() {
            return this.mMac;
        }
    }

    public static final class AuthenticationResultInternal {
        private CryptoObject mCryptoObject;

        public AuthenticationResultInternal(CryptoObject cryptoObject) {
            this.mCryptoObject = cryptoObject;
        }

        public CryptoObject getCryptoObject() {
            return this.mCryptoObject;
        }
    }

    public static abstract class AuthenticationCallback {
        public void onAuthenticationError(int i, CharSequence charSequence) {
        }

        public void onAuthenticationHelp(int i, CharSequence charSequence) {
        }

        public void onAuthenticationSucceeded(AuthenticationResultInternal authenticationResultInternal) {
        }

        public void onAuthenticationFailed() {
        }
    }
}
