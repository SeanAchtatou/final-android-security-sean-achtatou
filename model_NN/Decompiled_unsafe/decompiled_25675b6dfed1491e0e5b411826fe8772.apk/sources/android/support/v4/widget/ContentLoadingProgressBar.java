package android.support.v4.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ProgressBar;

public class ContentLoadingProgressBar extends ProgressBar {
    private static final int MIN_DELAY = 500;
    private static final int MIN_SHOW_TIME = 500;
    private final Runnable mDelayedHide;
    private final Runnable mDelayedShow;
    /* access modifiers changed from: private */
    public boolean mDismissed;
    private boolean mPostedHide;
    private boolean mPostedShow;
    private long mStartTime;

    static /* synthetic */ boolean access$002(ContentLoadingProgressBar contentLoadingProgressBar, boolean z) {
        boolean z2 = z;
        boolean z3 = z2;
        contentLoadingProgressBar.mPostedHide = z3;
        return z2;
    }

    static /* synthetic */ long access$102(ContentLoadingProgressBar contentLoadingProgressBar, long j) {
        long j2 = j;
        long j3 = j2;
        contentLoadingProgressBar.mStartTime = j3;
        return j2;
    }

    static /* synthetic */ boolean access$202(ContentLoadingProgressBar contentLoadingProgressBar, boolean z) {
        boolean z2 = z;
        boolean z3 = z2;
        contentLoadingProgressBar.mPostedShow = z3;
        return z2;
    }

    public ContentLoadingProgressBar(Context context) {
        this(context, null);
    }

    public ContentLoadingProgressBar(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, 0);
        Runnable runnable;
        Runnable runnable2;
        this.mStartTime = -1;
        this.mPostedHide = false;
        this.mPostedShow = false;
        this.mDismissed = false;
        new Runnable() {
            public void run() {
                boolean access$002 = ContentLoadingProgressBar.access$002(ContentLoadingProgressBar.this, false);
                long access$102 = ContentLoadingProgressBar.access$102(ContentLoadingProgressBar.this, -1);
                ContentLoadingProgressBar.this.setVisibility(8);
            }
        };
        this.mDelayedHide = runnable;
        new Runnable() {
            public void run() {
                boolean access$202 = ContentLoadingProgressBar.access$202(ContentLoadingProgressBar.this, false);
                if (!ContentLoadingProgressBar.this.mDismissed) {
                    long access$102 = ContentLoadingProgressBar.access$102(ContentLoadingProgressBar.this, System.currentTimeMillis());
                    ContentLoadingProgressBar.this.setVisibility(0);
                }
            }
        };
        this.mDelayedShow = runnable2;
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        removeCallbacks();
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        removeCallbacks();
    }

    private void removeCallbacks() {
        boolean removeCallbacks = removeCallbacks(this.mDelayedHide);
        boolean removeCallbacks2 = removeCallbacks(this.mDelayedShow);
    }

    public void hide() {
        this.mDismissed = true;
        boolean removeCallbacks = removeCallbacks(this.mDelayedShow);
        long currentTimeMillis = System.currentTimeMillis() - this.mStartTime;
        if (currentTimeMillis >= 500 || this.mStartTime == -1) {
            setVisibility(8);
        } else if (!this.mPostedHide) {
            boolean postDelayed = postDelayed(this.mDelayedHide, 500 - currentTimeMillis);
            this.mPostedHide = true;
        }
    }

    public void show() {
        this.mStartTime = -1;
        this.mDismissed = false;
        boolean removeCallbacks = removeCallbacks(this.mDelayedHide);
        if (!this.mPostedShow) {
            boolean postDelayed = postDelayed(this.mDelayedShow, 500);
            this.mPostedShow = true;
        }
    }
}
