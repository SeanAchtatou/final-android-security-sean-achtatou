package android.support.v4.text;

import android.util.Log;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Locale;

public class ICUCompatApi23 {
    private static final String TAG = "ICUCompatIcs";
    private static Method sAddLikelySubtagsMethod;

    static {
        Throwable th;
        try {
            sAddLikelySubtagsMethod = Class.forName("libcore.icu.ICU").getMethod("addLikelySubtags", Locale.class);
        } catch (Exception e) {
            Exception exc = e;
            Throwable th2 = th;
            new IllegalStateException(exc);
            throw th2;
        }
    }

    public static String maximizeAndGetScript(Locale locale) {
        Locale locale2 = locale;
        try {
            return ((Locale) sAddLikelySubtagsMethod.invoke(null, locale2)).getScript();
        } catch (InvocationTargetException e) {
            int w = Log.w(TAG, e);
            return locale2.getScript();
        } catch (IllegalAccessException e2) {
            int w2 = Log.w(TAG, e2);
            return locale2.getScript();
        }
    }
}
