package android.support.v4.app;

import android.content.Context;
import android.os.Parcelable;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

class NoSaveStateFrameLayout extends FrameLayout {
    static ViewGroup wrap(View view) {
        NoSaveStateFrameLayout noSaveStateFrameLayout;
        ViewGroup.LayoutParams layoutParams;
        View view2 = view;
        new NoSaveStateFrameLayout(view2.getContext());
        NoSaveStateFrameLayout noSaveStateFrameLayout2 = noSaveStateFrameLayout;
        ViewGroup.LayoutParams layoutParams2 = view2.getLayoutParams();
        if (layoutParams2 != null) {
            noSaveStateFrameLayout2.setLayoutParams(layoutParams2);
        }
        new FrameLayout.LayoutParams(-1, -1);
        view2.setLayoutParams(layoutParams);
        noSaveStateFrameLayout2.addView(view2);
        return noSaveStateFrameLayout2;
    }

    public NoSaveStateFrameLayout(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public void dispatchSaveInstanceState(SparseArray<Parcelable> sparseArray) {
        dispatchFreezeSelfOnly(sparseArray);
    }

    /* access modifiers changed from: protected */
    public void dispatchRestoreInstanceState(SparseArray<Parcelable> sparseArray) {
        dispatchThawSelfOnly(sparseArray);
    }
}
