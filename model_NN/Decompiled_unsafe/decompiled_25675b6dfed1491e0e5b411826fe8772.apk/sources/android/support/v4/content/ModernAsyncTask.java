package android.support.v4.content;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.util.Log;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.FutureTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

abstract class ModernAsyncTask<Params, Progress, Result> {
    private static final int CORE_POOL_SIZE = 5;
    private static final int KEEP_ALIVE = 1;
    private static final String LOG_TAG = "AsyncTask";
    private static final int MAXIMUM_POOL_SIZE = 128;
    private static final int MESSAGE_POST_PROGRESS = 2;
    private static final int MESSAGE_POST_RESULT = 1;
    public static final Executor THREAD_POOL_EXECUTOR;
    private static volatile Executor sDefaultExecutor = THREAD_POOL_EXECUTOR;
    private static InternalHandler sHandler;
    private static final BlockingQueue<Runnable> sPoolWorkQueue;
    private static final ThreadFactory sThreadFactory;
    private final FutureTask<Result> mFuture;
    private volatile Status mStatus = Status.PENDING;
    /* access modifiers changed from: private */
    public final AtomicBoolean mTaskInvoked;
    private final WorkerRunnable<Params, Result> mWorker;

    /* access modifiers changed from: protected */
    public abstract Result doInBackground(Object... objArr);

    static {
        ThreadFactory threadFactory;
        BlockingQueue<Runnable> blockingQueue;
        Executor executor;
        new ThreadFactory() {
            private final AtomicInteger mCount;

            {
                AtomicInteger atomicInteger;
                new AtomicInteger(1);
                this.mCount = atomicInteger;
            }

            public Thread newThread(Runnable runnable) {
                Thread thread;
                StringBuilder sb;
                new StringBuilder();
                new Thread(runnable, sb.append("ModernAsyncTask #").append(this.mCount.getAndIncrement()).toString());
                return thread;
            }
        };
        sThreadFactory = threadFactory;
        new LinkedBlockingQueue(10);
        sPoolWorkQueue = blockingQueue;
        new ThreadPoolExecutor(5, 128, 1, TimeUnit.SECONDS, sPoolWorkQueue, sThreadFactory);
        THREAD_POOL_EXECUTOR = executor;
    }

    /* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
    public static final class Status extends Enum<Status> {
        private static final /* synthetic */ Status[] $VALUES;
        public static final Status FINISHED;
        public static final Status PENDING;
        public static final Status RUNNING;

        private Status(String str, int i) {
        }

        public static Status valueOf(String str) {
            return (Status) Enum.valueOf(Status.class, str);
        }

        public static Status[] values() {
            return (Status[]) $VALUES.clone();
        }

        static {
            Status status;
            Status status2;
            Status status3;
            new Status("PENDING", 0);
            PENDING = status;
            new Status("RUNNING", 1);
            RUNNING = status2;
            new Status("FINISHED", 2);
            FINISHED = status3;
            Status[] statusArr = new Status[3];
            statusArr[0] = PENDING;
            Status[] statusArr2 = statusArr;
            statusArr2[1] = RUNNING;
            Status[] statusArr3 = statusArr2;
            statusArr3[2] = FINISHED;
            $VALUES = statusArr3;
        }
    }

    private static Handler getHandler() {
        InternalHandler internalHandler;
        Class<ModernAsyncTask> cls = ModernAsyncTask.class;
        Class<ModernAsyncTask> cls2 = cls;
        synchronized (cls) {
            try {
                if (sHandler == null) {
                    new InternalHandler();
                    sHandler = internalHandler;
                }
                InternalHandler internalHandler2 = sHandler;
                return internalHandler2;
            } catch (Throwable th) {
                Throwable th2 = th;
                Class<ModernAsyncTask> cls3 = cls2;
                throw th2;
            }
        }
    }

    public static void setDefaultExecutor(Executor executor) {
        sDefaultExecutor = executor;
    }

    public ModernAsyncTask() {
        AtomicBoolean atomicBoolean;
        WorkerRunnable<Params, Result> workerRunnable;
        FutureTask<Result> futureTask;
        new AtomicBoolean();
        this.mTaskInvoked = atomicBoolean;
        new WorkerRunnable<Params, Result>() {
            public Result call() throws Exception {
                ModernAsyncTask.this.mTaskInvoked.set(true);
                Process.setThreadPriority(10);
                return ModernAsyncTask.this.postResult(ModernAsyncTask.this.doInBackground(this.mParams));
            }
        };
        this.mWorker = workerRunnable;
        new FutureTask<Result>(this.mWorker) {
            /* access modifiers changed from: protected */
            public void done() {
                Throwable th;
                Throwable th2;
                try {
                    ModernAsyncTask.this.postResultIfNotInvoked(get());
                } catch (InterruptedException e) {
                    int w = Log.w(ModernAsyncTask.LOG_TAG, e);
                } catch (ExecutionException e2) {
                    ExecutionException executionException = e2;
                    Throwable th3 = th2;
                    new RuntimeException("An error occurred while executing doInBackground()", executionException.getCause());
                    throw th3;
                } catch (CancellationException e3) {
                    ModernAsyncTask.this.postResultIfNotInvoked(null);
                } catch (Throwable th4) {
                    Throwable th5 = th4;
                    Throwable th6 = th;
                    new RuntimeException("An error occurred while executing doInBackground()", th5);
                    throw th6;
                }
            }
        };
        this.mFuture = futureTask;
    }

    /* access modifiers changed from: private */
    public void postResultIfNotInvoked(Result result) {
        Result result2 = result;
        if (!this.mTaskInvoked.get()) {
            Object postResult = postResult(result2);
        }
    }

    /* access modifiers changed from: private */
    public Result postResult(Result result) {
        Object obj;
        Result result2 = result;
        Handler handler = getHandler();
        Object obj2 = obj;
        new AsyncTaskResult(this, result2);
        handler.obtainMessage(1, obj2).sendToTarget();
        return result2;
    }

    public final Status getStatus() {
        return this.mStatus;
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Result result) {
    }

    /* access modifiers changed from: protected */
    public void onProgressUpdate(Progress... progressArr) {
    }

    /* access modifiers changed from: protected */
    public void onCancelled(Result result) {
        onCancelled();
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
    }

    public final boolean isCancelled() {
        return this.mFuture.isCancelled();
    }

    public final boolean cancel(boolean z) {
        return this.mFuture.cancel(z);
    }

    public final Result get() throws InterruptedException, ExecutionException {
        return this.mFuture.get();
    }

    public final Result get(long j, TimeUnit timeUnit) throws InterruptedException, ExecutionException, TimeoutException {
        return this.mFuture.get(j, timeUnit);
    }

    public final ModernAsyncTask<Params, Progress, Result> execute(Params... paramsArr) {
        return executeOnExecutor(sDefaultExecutor, paramsArr);
    }

    public final ModernAsyncTask<Params, Progress, Result> executeOnExecutor(Executor executor, Params... paramsArr) {
        Throwable th;
        Throwable th2;
        Executor executor2 = executor;
        Params[] paramsArr2 = paramsArr;
        if (this.mStatus != Status.PENDING) {
            switch (this.mStatus) {
                case Status.RUNNING:
                    Throwable th3 = th2;
                    new IllegalStateException("Cannot execute task: the task is already running.");
                    throw th3;
                case Status.FINISHED:
                    Throwable th4 = th;
                    new IllegalStateException("Cannot execute task: the task has already been executed (a task can be executed only once)");
                    throw th4;
            }
        }
        this.mStatus = Status.RUNNING;
        onPreExecute();
        this.mWorker.mParams = paramsArr2;
        executor2.execute(this.mFuture);
        return this;
    }

    public static void execute(Runnable runnable) {
        sDefaultExecutor.execute(runnable);
    }

    /* access modifiers changed from: protected */
    public final void publishProgress(Progress... progressArr) {
        Object obj;
        Progress[] progressArr2 = progressArr;
        if (!isCancelled()) {
            new AsyncTaskResult(this, progressArr2);
            getHandler().obtainMessage(2, obj).sendToTarget();
        }
    }

    /* access modifiers changed from: private */
    public void finish(Result result) {
        Result result2 = result;
        if (isCancelled()) {
            onCancelled(result2);
        } else {
            onPostExecute(result2);
        }
        this.mStatus = Status.FINISHED;
    }

    private static class InternalHandler extends Handler {
        public InternalHandler() {
            super(Looper.getMainLooper());
        }

        public void handleMessage(Message message) {
            Message message2 = message;
            AsyncTaskResult asyncTaskResult = (AsyncTaskResult) message2.obj;
            switch (message2.what) {
                case 1:
                    asyncTaskResult.mTask.finish(asyncTaskResult.mData[0]);
                    return;
                case 2:
                    asyncTaskResult.mTask.onProgressUpdate(asyncTaskResult.mData);
                    return;
                default:
                    return;
            }
        }
    }

    private static abstract class WorkerRunnable<Params, Result> implements Callable<Result> {
        Params[] mParams;

        private WorkerRunnable() {
        }
    }

    private static class AsyncTaskResult<Data> {
        final Data[] mData;
        final ModernAsyncTask mTask;

        AsyncTaskResult(ModernAsyncTask modernAsyncTask, Data... dataArr) {
            this.mTask = modernAsyncTask;
            this.mData = dataArr;
        }
    }
}
