package android.support.v4.view;

import android.util.Log;
import android.view.View;
import android.view.ViewParent;

class ViewParentCompatLollipop {
    private static final String TAG = "ViewParentCompat";

    ViewParentCompatLollipop() {
    }

    public static boolean onStartNestedScroll(ViewParent viewParent, View view, View view2, int i) {
        StringBuilder sb;
        ViewParent viewParent2 = viewParent;
        try {
            return viewParent2.onStartNestedScroll(view, view2, i);
        } catch (AbstractMethodError e) {
            new StringBuilder();
            int e2 = Log.e(TAG, sb.append("ViewParent ").append(viewParent2).append(" does not implement interface ").append("method onStartNestedScroll").toString(), e);
            return false;
        }
    }

    public static void onNestedScrollAccepted(ViewParent viewParent, View view, View view2, int i) {
        StringBuilder sb;
        ViewParent viewParent2 = viewParent;
        try {
            viewParent2.onNestedScrollAccepted(view, view2, i);
        } catch (AbstractMethodError e) {
            new StringBuilder();
            int e2 = Log.e(TAG, sb.append("ViewParent ").append(viewParent2).append(" does not implement interface ").append("method onNestedScrollAccepted").toString(), e);
        }
    }

    public static void onStopNestedScroll(ViewParent viewParent, View view) {
        StringBuilder sb;
        ViewParent viewParent2 = viewParent;
        try {
            viewParent2.onStopNestedScroll(view);
        } catch (AbstractMethodError e) {
            new StringBuilder();
            int e2 = Log.e(TAG, sb.append("ViewParent ").append(viewParent2).append(" does not implement interface ").append("method onStopNestedScroll").toString(), e);
        }
    }

    public static void onNestedScroll(ViewParent viewParent, View view, int i, int i2, int i3, int i4) {
        StringBuilder sb;
        ViewParent viewParent2 = viewParent;
        try {
            viewParent2.onNestedScroll(view, i, i2, i3, i4);
        } catch (AbstractMethodError e) {
            new StringBuilder();
            int e2 = Log.e(TAG, sb.append("ViewParent ").append(viewParent2).append(" does not implement interface ").append("method onNestedScroll").toString(), e);
        }
    }

    public static void onNestedPreScroll(ViewParent viewParent, View view, int i, int i2, int[] iArr) {
        StringBuilder sb;
        ViewParent viewParent2 = viewParent;
        try {
            viewParent2.onNestedPreScroll(view, i, i2, iArr);
        } catch (AbstractMethodError e) {
            new StringBuilder();
            int e2 = Log.e(TAG, sb.append("ViewParent ").append(viewParent2).append(" does not implement interface ").append("method onNestedPreScroll").toString(), e);
        }
    }

    public static boolean onNestedFling(ViewParent viewParent, View view, float f, float f2, boolean z) {
        StringBuilder sb;
        ViewParent viewParent2 = viewParent;
        try {
            return viewParent2.onNestedFling(view, f, f2, z);
        } catch (AbstractMethodError e) {
            new StringBuilder();
            int e2 = Log.e(TAG, sb.append("ViewParent ").append(viewParent2).append(" does not implement interface ").append("method onNestedFling").toString(), e);
            return false;
        }
    }

    public static boolean onNestedPreFling(ViewParent viewParent, View view, float f, float f2) {
        StringBuilder sb;
        ViewParent viewParent2 = viewParent;
        try {
            return viewParent2.onNestedPreFling(view, f, f2);
        } catch (AbstractMethodError e) {
            new StringBuilder();
            int e2 = Log.e(TAG, sb.append("ViewParent ").append(viewParent2).append(" does not implement interface ").append("method onNestedPreFling").toString(), e);
            return false;
        }
    }
}
