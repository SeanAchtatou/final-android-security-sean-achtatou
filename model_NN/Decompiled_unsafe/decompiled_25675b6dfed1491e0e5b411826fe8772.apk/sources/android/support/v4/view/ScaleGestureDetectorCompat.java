package android.support.v4.view;

import android.os.Build;

public class ScaleGestureDetectorCompat {
    static final ScaleGestureDetectorImpl IMPL;

    interface ScaleGestureDetectorImpl {
        boolean isQuickScaleEnabled(Object obj);

        void setQuickScaleEnabled(Object obj, boolean z);
    }

    private static class BaseScaleGestureDetectorImpl implements ScaleGestureDetectorImpl {
        private BaseScaleGestureDetectorImpl() {
        }

        public void setQuickScaleEnabled(Object obj, boolean z) {
        }

        public boolean isQuickScaleEnabled(Object obj) {
            return false;
        }
    }

    private static class ScaleGestureDetectorCompatKitKatImpl implements ScaleGestureDetectorImpl {
        private ScaleGestureDetectorCompatKitKatImpl() {
        }

        public void setQuickScaleEnabled(Object obj, boolean z) {
            ScaleGestureDetectorCompatKitKat.setQuickScaleEnabled(obj, z);
        }

        public boolean isQuickScaleEnabled(Object obj) {
            return ScaleGestureDetectorCompatKitKat.isQuickScaleEnabled(obj);
        }
    }

    static {
        ScaleGestureDetectorImpl scaleGestureDetectorImpl;
        ScaleGestureDetectorImpl scaleGestureDetectorImpl2;
        if (Build.VERSION.SDK_INT >= 19) {
            new ScaleGestureDetectorCompatKitKatImpl();
            IMPL = scaleGestureDetectorImpl2;
            return;
        }
        new BaseScaleGestureDetectorImpl();
        IMPL = scaleGestureDetectorImpl;
    }

    private ScaleGestureDetectorCompat() {
    }

    public static void setQuickScaleEnabled(Object obj, boolean z) {
        IMPL.setQuickScaleEnabled(obj, z);
    }

    public static boolean isQuickScaleEnabled(Object obj) {
        return IMPL.isQuickScaleEnabled(obj);
    }
}
