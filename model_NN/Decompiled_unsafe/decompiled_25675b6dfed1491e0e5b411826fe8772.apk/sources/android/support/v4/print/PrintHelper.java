package android.support.v4.print;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.support.v4.print.PrintHelperKitkat;
import java.io.FileNotFoundException;

public final class PrintHelper {
    public static final int COLOR_MODE_COLOR = 2;
    public static final int COLOR_MODE_MONOCHROME = 1;
    public static final int ORIENTATION_LANDSCAPE = 1;
    public static final int ORIENTATION_PORTRAIT = 2;
    public static final int SCALE_MODE_FILL = 2;
    public static final int SCALE_MODE_FIT = 1;
    PrintHelperVersionImpl mImpl;

    public interface OnPrintFinishCallback {
        void onFinish();
    }

    interface PrintHelperVersionImpl {
        int getColorMode();

        int getOrientation();

        int getScaleMode();

        void printBitmap(String str, Bitmap bitmap, OnPrintFinishCallback onPrintFinishCallback);

        void printBitmap(String str, Uri uri, OnPrintFinishCallback onPrintFinishCallback) throws FileNotFoundException;

        void setColorMode(int i);

        void setOrientation(int i);

        void setScaleMode(int i);
    }

    public static boolean systemSupportsPrint() {
        if (Build.VERSION.SDK_INT >= 19) {
            return true;
        }
        return false;
    }

    private static final class PrintHelperStubImpl implements PrintHelperVersionImpl {
        int mColorMode;
        int mOrientation;
        int mScaleMode;

        private PrintHelperStubImpl() {
            this.mScaleMode = 2;
            this.mColorMode = 2;
            this.mOrientation = 1;
        }

        public void setScaleMode(int i) {
            this.mScaleMode = i;
        }

        public int getColorMode() {
            return this.mColorMode;
        }

        public void setColorMode(int i) {
            this.mColorMode = i;
        }

        public void setOrientation(int i) {
            this.mOrientation = i;
        }

        public int getOrientation() {
            return this.mOrientation;
        }

        public int getScaleMode() {
            return this.mScaleMode;
        }

        public void printBitmap(String str, Bitmap bitmap, OnPrintFinishCallback onPrintFinishCallback) {
        }

        public void printBitmap(String str, Uri uri, OnPrintFinishCallback onPrintFinishCallback) {
        }
    }

    private static final class PrintHelperKitkatImpl implements PrintHelperVersionImpl {
        private final PrintHelperKitkat mPrintHelper;

        PrintHelperKitkatImpl(Context context) {
            PrintHelperKitkat printHelperKitkat;
            new PrintHelperKitkat(context);
            this.mPrintHelper = printHelperKitkat;
        }

        public void setScaleMode(int i) {
            this.mPrintHelper.setScaleMode(i);
        }

        public int getScaleMode() {
            return this.mPrintHelper.getScaleMode();
        }

        public void setColorMode(int i) {
            this.mPrintHelper.setColorMode(i);
        }

        public int getColorMode() {
            return this.mPrintHelper.getColorMode();
        }

        public void setOrientation(int i) {
            this.mPrintHelper.setOrientation(i);
        }

        public int getOrientation() {
            return this.mPrintHelper.getOrientation();
        }

        public void printBitmap(String str, Bitmap bitmap, OnPrintFinishCallback onPrintFinishCallback) {
            PrintHelperKitkat.OnPrintFinishCallback onPrintFinishCallback2;
            String str2 = str;
            Bitmap bitmap2 = bitmap;
            OnPrintFinishCallback onPrintFinishCallback3 = onPrintFinishCallback;
            PrintHelperKitkat.OnPrintFinishCallback onPrintFinishCallback4 = null;
            if (onPrintFinishCallback3 != null) {
                final OnPrintFinishCallback onPrintFinishCallback5 = onPrintFinishCallback3;
                new PrintHelperKitkat.OnPrintFinishCallback() {
                    public void onFinish() {
                        onPrintFinishCallback5.onFinish();
                    }
                };
                onPrintFinishCallback4 = onPrintFinishCallback2;
            }
            this.mPrintHelper.printBitmap(str2, bitmap2, onPrintFinishCallback4);
        }

        public void printBitmap(String str, Uri uri, OnPrintFinishCallback onPrintFinishCallback) throws FileNotFoundException {
            PrintHelperKitkat.OnPrintFinishCallback onPrintFinishCallback2;
            String str2 = str;
            Uri uri2 = uri;
            OnPrintFinishCallback onPrintFinishCallback3 = onPrintFinishCallback;
            PrintHelperKitkat.OnPrintFinishCallback onPrintFinishCallback4 = null;
            if (onPrintFinishCallback3 != null) {
                final OnPrintFinishCallback onPrintFinishCallback5 = onPrintFinishCallback3;
                new PrintHelperKitkat.OnPrintFinishCallback() {
                    public void onFinish() {
                        onPrintFinishCallback5.onFinish();
                    }
                };
                onPrintFinishCallback4 = onPrintFinishCallback2;
            }
            this.mPrintHelper.printBitmap(str2, uri2, onPrintFinishCallback4);
        }
    }

    public PrintHelper(Context context) {
        PrintHelperVersionImpl printHelperVersionImpl;
        PrintHelperVersionImpl printHelperVersionImpl2;
        Context context2 = context;
        if (systemSupportsPrint()) {
            new PrintHelperKitkatImpl(context2);
            this.mImpl = printHelperVersionImpl2;
            return;
        }
        new PrintHelperStubImpl();
        this.mImpl = printHelperVersionImpl;
    }

    public void setScaleMode(int i) {
        this.mImpl.setScaleMode(i);
    }

    public int getScaleMode() {
        return this.mImpl.getScaleMode();
    }

    public void setColorMode(int i) {
        this.mImpl.setColorMode(i);
    }

    public int getColorMode() {
        return this.mImpl.getColorMode();
    }

    public void setOrientation(int i) {
        this.mImpl.setOrientation(i);
    }

    public int getOrientation() {
        return this.mImpl.getOrientation();
    }

    public void printBitmap(String str, Bitmap bitmap) {
        this.mImpl.printBitmap(str, bitmap, (OnPrintFinishCallback) null);
    }

    public void printBitmap(String str, Bitmap bitmap, OnPrintFinishCallback onPrintFinishCallback) {
        this.mImpl.printBitmap(str, bitmap, onPrintFinishCallback);
    }

    public void printBitmap(String str, Uri uri) throws FileNotFoundException {
        this.mImpl.printBitmap(str, uri, (OnPrintFinishCallback) null);
    }

    public void printBitmap(String str, Uri uri, OnPrintFinishCallback onPrintFinishCallback) throws FileNotFoundException {
        this.mImpl.printBitmap(str, uri, onPrintFinishCallback);
    }
}
