package android.support.v7.graphics.drawable;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorInt;
import android.support.annotation.FloatRange;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.appcompat.R;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class DrawerArrowDrawable extends Drawable {
    public static final int ARROW_DIRECTION_END = 3;
    public static final int ARROW_DIRECTION_LEFT = 0;
    public static final int ARROW_DIRECTION_RIGHT = 1;
    public static final int ARROW_DIRECTION_START = 2;
    private static final float ARROW_HEAD_ANGLE = ((float) Math.toRadians(45.0d));
    private float mArrowHeadLength;
    private float mArrowShaftLength;
    private float mBarGap;
    private float mBarLength;
    private int mDirection = 2;
    private float mMaxCutForBarSize;
    private final Paint mPaint;
    private final Path mPath;
    private float mProgress;
    private final int mSize;
    private boolean mSpin;
    private boolean mVerticalMirror = false;

    @Retention(RetentionPolicy.SOURCE)
    public @interface ArrowDirection {
    }

    public DrawerArrowDrawable(Context context) {
        Paint paint;
        Path path;
        new Paint();
        this.mPaint = paint;
        new Path();
        this.mPath = path;
        this.mPaint.setStyle(Paint.Style.STROKE);
        this.mPaint.setStrokeJoin(Paint.Join.MITER);
        this.mPaint.setStrokeCap(Paint.Cap.BUTT);
        this.mPaint.setAntiAlias(true);
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(null, R.styleable.DrawerArrowToggle, R.attr.drawerArrowStyle, R.style.Base_Widget_AppCompat_DrawerArrowToggle);
        setColor(obtainStyledAttributes.getColor(R.styleable.DrawerArrowToggle_color, 0));
        setBarThickness(obtainStyledAttributes.getDimension(R.styleable.DrawerArrowToggle_thickness, 0.0f));
        setSpinEnabled(obtainStyledAttributes.getBoolean(R.styleable.DrawerArrowToggle_spinBars, true));
        setGapSize((float) Math.round(obtainStyledAttributes.getDimension(R.styleable.DrawerArrowToggle_gapBetweenBars, 0.0f)));
        this.mSize = obtainStyledAttributes.getDimensionPixelSize(R.styleable.DrawerArrowToggle_drawableSize, 0);
        this.mBarLength = (float) Math.round(obtainStyledAttributes.getDimension(R.styleable.DrawerArrowToggle_barLength, 0.0f));
        this.mArrowHeadLength = (float) Math.round(obtainStyledAttributes.getDimension(R.styleable.DrawerArrowToggle_arrowHeadLength, 0.0f));
        this.mArrowShaftLength = obtainStyledAttributes.getDimension(R.styleable.DrawerArrowToggle_arrowShaftLength, 0.0f);
        obtainStyledAttributes.recycle();
    }

    public void setArrowHeadLength(float f) {
        float f2 = f;
        if (this.mArrowHeadLength != f2) {
            this.mArrowHeadLength = f2;
            invalidateSelf();
        }
    }

    public float getArrowHeadLength() {
        return this.mArrowHeadLength;
    }

    public void setArrowShaftLength(float f) {
        float f2 = f;
        if (this.mArrowShaftLength != f2) {
            this.mArrowShaftLength = f2;
            invalidateSelf();
        }
    }

    public float getArrowShaftLength() {
        return this.mArrowShaftLength;
    }

    public float getBarLength() {
        return this.mBarLength;
    }

    public void setBarLength(float f) {
        float f2 = f;
        if (this.mBarLength != f2) {
            this.mBarLength = f2;
            invalidateSelf();
        }
    }

    public void setColor(@ColorInt int i) {
        int i2 = i;
        if (i2 != this.mPaint.getColor()) {
            this.mPaint.setColor(i2);
            invalidateSelf();
        }
    }

    @ColorInt
    public int getColor() {
        return this.mPaint.getColor();
    }

    public void setBarThickness(float f) {
        float f2 = f;
        if (this.mPaint.getStrokeWidth() != f2) {
            this.mPaint.setStrokeWidth(f2);
            this.mMaxCutForBarSize = (float) (((double) (f2 / 2.0f)) * Math.cos((double) ARROW_HEAD_ANGLE));
            invalidateSelf();
        }
    }

    public float getBarThickness() {
        return this.mPaint.getStrokeWidth();
    }

    public float getGapSize() {
        return this.mBarGap;
    }

    public void setGapSize(float f) {
        float f2 = f;
        if (f2 != this.mBarGap) {
            this.mBarGap = f2;
            invalidateSelf();
        }
    }

    public void setDirection(int i) {
        int i2 = i;
        if (i2 != this.mDirection) {
            this.mDirection = i2;
            invalidateSelf();
        }
    }

    public boolean isSpinEnabled() {
        return this.mSpin;
    }

    public void setSpinEnabled(boolean z) {
        boolean z2 = z;
        if (this.mSpin != z2) {
            this.mSpin = z2;
            invalidateSelf();
        }
    }

    public int getDirection() {
        return this.mDirection;
    }

    public void setVerticalMirror(boolean z) {
        boolean z2 = z;
        if (this.mVerticalMirror != z2) {
            this.mVerticalMirror = z2;
            invalidateSelf();
        }
    }

    public void draw(Canvas canvas) {
        boolean z;
        float f;
        Canvas canvas2 = canvas;
        Rect bounds = getBounds();
        switch (this.mDirection) {
            case 0:
                z = false;
                break;
            case 1:
                z = true;
                break;
            case 2:
            default:
                z = DrawableCompat.getLayoutDirection(this) == 1;
                break;
            case 3:
                z = DrawableCompat.getLayoutDirection(this) == 0;
                break;
        }
        float lerp = lerp(this.mBarLength, (float) Math.sqrt((double) (this.mArrowHeadLength * this.mArrowHeadLength * 2.0f)), this.mProgress);
        float lerp2 = lerp(this.mBarLength, this.mArrowShaftLength, this.mProgress);
        float round = (float) Math.round(lerp(0.0f, this.mMaxCutForBarSize, this.mProgress));
        float lerp3 = lerp(0.0f, ARROW_HEAD_ANGLE, this.mProgress);
        float f2 = z ? 0.0f : -180.0f;
        if (z) {
            f = 180.0f;
        } else {
            f = 0.0f;
        }
        float lerp4 = lerp(f2, f, this.mProgress);
        float round2 = (float) Math.round(((double) lerp) * Math.cos((double) lerp3));
        float round3 = (float) Math.round(((double) lerp) * Math.sin((double) lerp3));
        this.mPath.rewind();
        float lerp5 = lerp(this.mBarGap + this.mPaint.getStrokeWidth(), -this.mMaxCutForBarSize, this.mProgress);
        float f3 = (-lerp2) / 2.0f;
        this.mPath.moveTo(f3 + round, 0.0f);
        this.mPath.rLineTo(lerp2 - (round * 2.0f), 0.0f);
        this.mPath.moveTo(f3, lerp5);
        this.mPath.rLineTo(round2, round3);
        this.mPath.moveTo(f3, -lerp5);
        this.mPath.rLineTo(round2, -round3);
        this.mPath.close();
        int save = canvas2.save();
        float strokeWidth = this.mPaint.getStrokeWidth();
        canvas2.translate((float) bounds.centerX(), (float) (((double) ((float) ((((int) ((((float) bounds.height()) - (strokeWidth * 3.0f)) - (this.mBarGap * 2.0f))) / 4) * 2))) + (((double) strokeWidth) * 1.5d) + ((double) this.mBarGap)));
        if (this.mSpin) {
            canvas2.rotate(lerp4 * ((float) (this.mVerticalMirror ^ z ? -1 : 1)));
        } else if (z) {
            canvas2.rotate(180.0f);
        }
        canvas2.drawPath(this.mPath, this.mPaint);
        canvas2.restore();
    }

    public void setAlpha(int i) {
        int i2 = i;
        if (i2 != this.mPaint.getAlpha()) {
            this.mPaint.setAlpha(i2);
            invalidateSelf();
        }
    }

    public void setColorFilter(ColorFilter colorFilter) {
        ColorFilter colorFilter2 = this.mPaint.setColorFilter(colorFilter);
        invalidateSelf();
    }

    public int getIntrinsicHeight() {
        return this.mSize;
    }

    public int getIntrinsicWidth() {
        return this.mSize;
    }

    public int getOpacity() {
        return -3;
    }

    @FloatRange(from = 0.0d, to = 1.0d)
    public float getProgress() {
        return this.mProgress;
    }

    public void setProgress(@FloatRange(from = 0.0d, to = 1.0d) float f) {
        float f2 = f;
        if (this.mProgress != f2) {
            this.mProgress = f2;
            invalidateSelf();
        }
    }

    public final Paint getPaint() {
        return this.mPaint;
    }

    private static float lerp(float f, float f2, float f3) {
        float f4 = f;
        return f4 + ((f2 - f4) * f3);
    }
}
