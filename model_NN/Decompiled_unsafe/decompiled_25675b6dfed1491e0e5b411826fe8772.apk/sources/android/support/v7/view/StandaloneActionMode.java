package android.support.v7.view;

import android.content.Context;
import android.support.v7.view.ActionMode;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuPopupHelper;
import android.support.v7.view.menu.SubMenuBuilder;
import android.support.v7.widget.ActionBarContextView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import java.lang.ref.WeakReference;

public class StandaloneActionMode extends ActionMode implements MenuBuilder.Callback {
    private ActionMode.Callback mCallback;
    private Context mContext;
    private ActionBarContextView mContextView;
    private WeakReference<View> mCustomView;
    private boolean mFinished;
    private boolean mFocusable;
    private MenuBuilder mMenu;

    public StandaloneActionMode(Context context, ActionBarContextView actionBarContextView, ActionMode.Callback callback, boolean z) {
        MenuBuilder menuBuilder;
        ActionBarContextView actionBarContextView2 = actionBarContextView;
        this.mContext = context;
        this.mContextView = actionBarContextView2;
        this.mCallback = callback;
        new MenuBuilder(actionBarContextView2.getContext());
        this.mMenu = menuBuilder.setDefaultShowAsAction(1);
        this.mMenu.setCallback(this);
        this.mFocusable = z;
    }

    public void setTitle(CharSequence charSequence) {
        this.mContextView.setTitle(charSequence);
    }

    public void setSubtitle(CharSequence charSequence) {
        this.mContextView.setSubtitle(charSequence);
    }

    public void setTitle(int i) {
        setTitle(this.mContext.getString(i));
    }

    public void setSubtitle(int i) {
        setSubtitle(this.mContext.getString(i));
    }

    public void setTitleOptionalHint(boolean z) {
        boolean z2 = z;
        super.setTitleOptionalHint(z2);
        this.mContextView.setTitleOptional(z2);
    }

    public boolean isTitleOptional() {
        return this.mContextView.isTitleOptional();
    }

    public void setCustomView(View view) {
        WeakReference<View> weakReference;
        WeakReference<View> weakReference2;
        View view2 = view;
        this.mContextView.setCustomView(view2);
        if (view2 != null) {
            weakReference = weakReference2;
            new WeakReference<>(view2);
        } else {
            weakReference = null;
        }
        this.mCustomView = weakReference;
    }

    public void invalidate() {
        boolean onPrepareActionMode = this.mCallback.onPrepareActionMode(this, this.mMenu);
    }

    public void finish() {
        if (!this.mFinished) {
            this.mFinished = true;
            this.mContextView.sendAccessibilityEvent(32);
            this.mCallback.onDestroyActionMode(this);
        }
    }

    public Menu getMenu() {
        return this.mMenu;
    }

    public CharSequence getTitle() {
        return this.mContextView.getTitle();
    }

    public CharSequence getSubtitle() {
        return this.mContextView.getSubtitle();
    }

    public View getCustomView() {
        return this.mCustomView != null ? this.mCustomView.get() : null;
    }

    public MenuInflater getMenuInflater() {
        MenuInflater menuInflater;
        new MenuInflater(this.mContextView.getContext());
        return menuInflater;
    }

    public boolean onMenuItemSelected(MenuBuilder menuBuilder, MenuItem menuItem) {
        return this.mCallback.onActionItemClicked(this, menuItem);
    }

    public void onCloseMenu(MenuBuilder menuBuilder, boolean z) {
    }

    public boolean onSubMenuSelected(SubMenuBuilder subMenuBuilder) {
        MenuPopupHelper menuPopupHelper;
        SubMenuBuilder subMenuBuilder2 = subMenuBuilder;
        if (!subMenuBuilder2.hasVisibleItems()) {
            return true;
        }
        new MenuPopupHelper(this.mContextView.getContext(), subMenuBuilder2);
        menuPopupHelper.show();
        return true;
    }

    public void onCloseSubMenu(SubMenuBuilder subMenuBuilder) {
    }

    public void onMenuModeChange(MenuBuilder menuBuilder) {
        invalidate();
        boolean showOverflowMenu = this.mContextView.showOverflowMenu();
    }

    public boolean isUiFocusable() {
        return this.mFocusable;
    }
}
