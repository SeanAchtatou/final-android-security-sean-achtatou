package android.support.v7.view.menu;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.view.ViewCompat;
import android.support.v7.appcompat.R;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuView;
import android.support.v7.widget.ActionMenuView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.ListPopupWindow;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

public class ActionMenuItemView extends AppCompatTextView implements MenuView.ItemView, View.OnClickListener, View.OnLongClickListener, ActionMenuView.ActionMenuChildView {
    private static final int MAX_ICON_SIZE = 32;
    private static final String TAG = "ActionMenuItemView";
    private boolean mAllowTextWithIcon;
    private boolean mExpandedFormat;
    private ListPopupWindow.ForwardingListener mForwardingListener;
    private Drawable mIcon;
    /* access modifiers changed from: private */
    public MenuItemImpl mItemData;
    /* access modifiers changed from: private */
    public MenuBuilder.ItemInvoker mItemInvoker;
    private int mMaxIconSize;
    private int mMinWidth;
    /* access modifiers changed from: private */
    public PopupCallback mPopupCallback;
    private int mSavedPaddingLeft;
    private CharSequence mTitle;

    public static abstract class PopupCallback {
        public abstract ListPopupWindow getPopup();
    }

    public ActionMenuItemView(Context context) {
        this(context, null);
    }

    public ActionMenuItemView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public ActionMenuItemView(android.content.Context r13, android.util.AttributeSet r14, int r15) {
        /*
            r12 = this;
            r0 = r12
            r1 = r13
            r2 = r14
            r3 = r15
            r7 = r0
            r8 = r1
            r9 = r2
            r10 = r3
            r7.<init>(r8, r9, r10)
            r7 = r1
            android.content.res.Resources r7 = r7.getResources()
            r4 = r7
            r7 = r0
            r8 = r4
            int r9 = android.support.v7.appcompat.R.bool.abc_config_allowActionMenuItemTextWithIcon
            boolean r8 = r8.getBoolean(r9)
            r7.mAllowTextWithIcon = r8
            r7 = r1
            r8 = r2
            int[] r9 = android.support.v7.appcompat.R.styleable.ActionMenuItemView
            r10 = r3
            r11 = 0
            android.content.res.TypedArray r7 = r7.obtainStyledAttributes(r8, r9, r10, r11)
            r5 = r7
            r7 = r0
            r8 = r5
            int r9 = android.support.v7.appcompat.R.styleable.ActionMenuItemView_android_minWidth
            r10 = 0
            int r8 = r8.getDimensionPixelSize(r9, r10)
            r7.mMinWidth = r8
            r7 = r5
            r7.recycle()
            r7 = r4
            android.util.DisplayMetrics r7 = r7.getDisplayMetrics()
            float r7 = r7.density
            r6 = r7
            r7 = r0
            r8 = 1107296256(0x42000000, float:32.0)
            r9 = r6
            float r8 = r8 * r9
            r9 = 1056964608(0x3f000000, float:0.5)
            float r8 = r8 + r9
            int r8 = (int) r8
            r7.mMaxIconSize = r8
            r7 = r0
            r8 = r0
            r7.setOnClickListener(r8)
            r7 = r0
            r8 = r0
            r7.setOnLongClickListener(r8)
            r7 = r0
            r8 = -1
            r7.mSavedPaddingLeft = r8
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.view.menu.ActionMenuItemView.<init>(android.content.Context, android.util.AttributeSet, int):void");
    }

    public void onConfigurationChanged(Configuration configuration) {
        Configuration configuration2 = configuration;
        if (Build.VERSION.SDK_INT >= 8) {
            super.onConfigurationChanged(configuration2);
        }
        this.mAllowTextWithIcon = getContext().getResources().getBoolean(R.bool.abc_config_allowActionMenuItemTextWithIcon);
        updateTextButtonVisibility();
    }

    public void setPadding(int i, int i2, int i3, int i4) {
        int i5 = i;
        this.mSavedPaddingLeft = i5;
        super.setPadding(i5, i2, i3, i4);
    }

    public MenuItemImpl getItemData() {
        return this.mItemData;
    }

    public void initialize(MenuItemImpl menuItemImpl, int i) {
        ListPopupWindow.ForwardingListener forwardingListener;
        MenuItemImpl menuItemImpl2 = menuItemImpl;
        this.mItemData = menuItemImpl2;
        setIcon(menuItemImpl2.getIcon());
        setTitle(menuItemImpl2.getTitleForItemView(this));
        setId(menuItemImpl2.getItemId());
        setVisibility(menuItemImpl2.isVisible() ? 0 : 8);
        setEnabled(menuItemImpl2.isEnabled());
        if (menuItemImpl2.hasSubMenu() && this.mForwardingListener == null) {
            new ActionMenuItemForwardingListener(this);
            this.mForwardingListener = forwardingListener;
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        MotionEvent motionEvent2 = motionEvent;
        if (!this.mItemData.hasSubMenu() || this.mForwardingListener == null || !this.mForwardingListener.onTouch(this, motionEvent2)) {
            return super.onTouchEvent(motionEvent2);
        }
        return true;
    }

    public void onClick(View view) {
        if (this.mItemInvoker != null) {
            boolean invokeItem = this.mItemInvoker.invokeItem(this.mItemData);
        }
    }

    public void setItemInvoker(MenuBuilder.ItemInvoker itemInvoker) {
        this.mItemInvoker = itemInvoker;
    }

    public void setPopupCallback(PopupCallback popupCallback) {
        this.mPopupCallback = popupCallback;
    }

    public boolean prefersCondensedTitle() {
        return true;
    }

    public void setCheckable(boolean z) {
    }

    public void setChecked(boolean z) {
    }

    public void setExpandedFormat(boolean z) {
        boolean z2 = z;
        if (this.mExpandedFormat != z2) {
            this.mExpandedFormat = z2;
            if (this.mItemData != null) {
                this.mItemData.actionFormatChanged();
            }
        }
    }

    private void updateTextButtonVisibility() {
        setText((!TextUtils.isEmpty(this.mTitle)) & (this.mIcon == null || (this.mItemData.showsTextAsAction() && (this.mAllowTextWithIcon || this.mExpandedFormat))) ? this.mTitle : null);
    }

    public void setIcon(Drawable drawable) {
        Drawable drawable2 = drawable;
        this.mIcon = drawable2;
        if (drawable2 != null) {
            int intrinsicWidth = drawable2.getIntrinsicWidth();
            int intrinsicHeight = drawable2.getIntrinsicHeight();
            if (intrinsicWidth > this.mMaxIconSize) {
                float f = ((float) this.mMaxIconSize) / ((float) intrinsicWidth);
                intrinsicWidth = this.mMaxIconSize;
                intrinsicHeight = (int) (((float) intrinsicHeight) * f);
            }
            if (intrinsicHeight > this.mMaxIconSize) {
                float f2 = ((float) this.mMaxIconSize) / ((float) intrinsicHeight);
                intrinsicHeight = this.mMaxIconSize;
                intrinsicWidth = (int) (((float) intrinsicWidth) * f2);
            }
            drawable2.setBounds(0, 0, intrinsicWidth, intrinsicHeight);
        }
        setCompoundDrawables(drawable2, null, null, null);
        updateTextButtonVisibility();
    }

    public boolean hasText() {
        return !TextUtils.isEmpty(getText());
    }

    public void setShortcut(boolean z, char c) {
    }

    public void setTitle(CharSequence charSequence) {
        this.mTitle = charSequence;
        setContentDescription(this.mTitle);
        updateTextButtonVisibility();
    }

    public boolean showsIcon() {
        return true;
    }

    public boolean needsDividerBefore() {
        return hasText() && this.mItemData.getIcon() == null;
    }

    public boolean needsDividerAfter() {
        return hasText();
    }

    public boolean onLongClick(View view) {
        Rect rect;
        View view2 = view;
        if (hasText()) {
            return false;
        }
        int[] iArr = new int[2];
        new Rect();
        Rect rect2 = rect;
        getLocationOnScreen(iArr);
        getWindowVisibleDisplayFrame(rect2);
        Context context = getContext();
        int width = getWidth();
        int height = getHeight();
        int i = iArr[1] + (height / 2);
        int i2 = iArr[0] + (width / 2);
        if (ViewCompat.getLayoutDirection(view2) == 0) {
            i2 = context.getResources().getDisplayMetrics().widthPixels - i2;
        }
        Toast makeText = Toast.makeText(context, this.mItemData.getTitle(), 0);
        if (i < rect2.height()) {
            makeText.setGravity(8388661, i2, (iArr[1] + height) - rect2.top);
        } else {
            makeText.setGravity(81, 0, height);
        }
        makeText.show();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int i3 = i;
        int i4 = i2;
        boolean hasText = hasText();
        if (hasText && this.mSavedPaddingLeft >= 0) {
            super.setPadding(this.mSavedPaddingLeft, getPaddingTop(), getPaddingRight(), getPaddingBottom());
        }
        super.onMeasure(i3, i4);
        int mode = View.MeasureSpec.getMode(i3);
        int size = View.MeasureSpec.getSize(i3);
        int measuredWidth = getMeasuredWidth();
        int min = mode == Integer.MIN_VALUE ? Math.min(size, this.mMinWidth) : this.mMinWidth;
        if (mode != 1073741824 && this.mMinWidth > 0 && measuredWidth < min) {
            super.onMeasure(View.MeasureSpec.makeMeasureSpec(min, 1073741824), i4);
        }
        if (!hasText && this.mIcon != null) {
            super.setPadding((getMeasuredWidth() - this.mIcon.getBounds().width()) / 2, getPaddingTop(), getPaddingRight(), getPaddingBottom());
        }
    }

    private class ActionMenuItemForwardingListener extends ListPopupWindow.ForwardingListener {
        final /* synthetic */ ActionMenuItemView this$0;

        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public ActionMenuItemForwardingListener(android.support.v7.view.menu.ActionMenuItemView r5) {
            /*
                r4 = this;
                r0 = r4
                r1 = r5
                r2 = r0
                r3 = r1
                r2.this$0 = r3
                r2 = r0
                r3 = r1
                r2.<init>(r3)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: android.support.v7.view.menu.ActionMenuItemView.ActionMenuItemForwardingListener.<init>(android.support.v7.view.menu.ActionMenuItemView):void");
        }

        public ListPopupWindow getPopup() {
            if (this.this$0.mPopupCallback != null) {
                return this.this$0.mPopupCallback.getPopup();
            }
            return null;
        }

        /* access modifiers changed from: protected */
        public boolean onForwardingStarted() {
            if (this.this$0.mItemInvoker == null || !this.this$0.mItemInvoker.invokeItem(this.this$0.mItemData)) {
                return false;
            }
            ListPopupWindow popup = getPopup();
            return popup != null && popup.isShowing();
        }
    }
}
