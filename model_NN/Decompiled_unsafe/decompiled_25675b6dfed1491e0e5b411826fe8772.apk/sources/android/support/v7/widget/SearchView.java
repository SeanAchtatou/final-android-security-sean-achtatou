package android.support.v7.widget;

import android.annotation.TargetApi;
import android.app.PendingIntent;
import android.app.SearchableInfo;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.support.v4.view.KeyEventCompat;
import android.support.v4.widget.CursorAdapter;
import android.support.v7.appcompat.R;
import android.support.v7.view.CollapsibleActionView;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ImageSpan;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.TextView;
import java.lang.reflect.Method;
import java.util.WeakHashMap;

public class SearchView extends LinearLayoutCompat implements CollapsibleActionView {
    private static final boolean DBG = false;
    static final AutoCompleteTextViewReflector HIDDEN_METHOD_INVOKER;
    private static final String IME_OPTION_NO_MICROPHONE = "nm";
    private static final boolean IS_AT_LEAST_FROYO;
    private static final String LOG_TAG = "SearchView";
    private Bundle mAppSearchData;
    private boolean mClearingFocus;
    /* access modifiers changed from: private */
    public final ImageView mCloseButton;
    private final ImageView mCollapsedIcon;
    private int mCollapsedImeOptions;
    private final CharSequence mDefaultQueryHint;
    private final View mDropDownAnchor;
    private boolean mExpandedInActionView;
    /* access modifiers changed from: private */
    public final ImageView mGoButton;
    private boolean mIconified;
    private boolean mIconifiedByDefault;
    private int mMaxWidth;
    private CharSequence mOldQueryText;
    private final View.OnClickListener mOnClickListener;
    private OnCloseListener mOnCloseListener;
    private final TextView.OnEditorActionListener mOnEditorActionListener;
    private final AdapterView.OnItemClickListener mOnItemClickListener;
    private final AdapterView.OnItemSelectedListener mOnItemSelectedListener;
    private OnQueryTextListener mOnQueryChangeListener;
    /* access modifiers changed from: private */
    public View.OnFocusChangeListener mOnQueryTextFocusChangeListener;
    private View.OnClickListener mOnSearchClickListener;
    private OnSuggestionListener mOnSuggestionListener;
    private final WeakHashMap<String, Drawable.ConstantState> mOutsideDrawablesCache;
    private CharSequence mQueryHint;
    private boolean mQueryRefinement;
    private Runnable mReleaseCursorRunnable;
    /* access modifiers changed from: private */
    public final ImageView mSearchButton;
    private final View mSearchEditFrame;
    private final Drawable mSearchHintIcon;
    private final View mSearchPlate;
    /* access modifiers changed from: private */
    public final SearchAutoComplete mSearchSrcTextView;
    /* access modifiers changed from: private */
    public SearchableInfo mSearchable;
    private Runnable mShowImeRunnable;
    private final View mSubmitArea;
    private boolean mSubmitButtonEnabled;
    private final int mSuggestionCommitIconResId;
    private final int mSuggestionRowLayout;
    /* access modifiers changed from: private */
    public CursorAdapter mSuggestionsAdapter;
    View.OnKeyListener mTextKeyListener;
    private TextWatcher mTextWatcher;
    private final TintManager mTintManager;
    private final Runnable mUpdateDrawableStateRunnable;
    private CharSequence mUserQuery;
    private final Intent mVoiceAppSearchIntent;
    /* access modifiers changed from: private */
    public final ImageView mVoiceButton;
    private boolean mVoiceButtonEnabled;
    private final Intent mVoiceWebSearchIntent;

    public interface OnCloseListener {
        boolean onClose();
    }

    public interface OnQueryTextListener {
        boolean onQueryTextChange(String str);

        boolean onQueryTextSubmit(String str);
    }

    public interface OnSuggestionListener {
        boolean onSuggestionClick(int i);

        boolean onSuggestionSelect(int i);
    }

    static {
        boolean z;
        AutoCompleteTextViewReflector autoCompleteTextViewReflector;
        if (Build.VERSION.SDK_INT >= 8) {
            z = true;
        } else {
            z = false;
        }
        IS_AT_LEAST_FROYO = z;
        new AutoCompleteTextViewReflector();
        HIDDEN_METHOD_INVOKER = autoCompleteTextViewReflector;
    }

    public SearchView(Context context) {
        this(context, null);
    }

    public SearchView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, R.attr.searchViewStyle);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.support.v7.widget.SearchView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public SearchView(android.content.Context r18, android.util.AttributeSet r19, int r20) {
        /*
            r17 = this;
            r0 = r17
            r1 = r18
            r2 = r19
            r3 = r20
            r11 = r0
            r12 = r1
            r13 = r2
            r14 = r3
            r11.<init>(r12, r13, r14)
            r11 = r0
            android.support.v7.widget.SearchView$1 r12 = new android.support.v7.widget.SearchView$1
            r16 = r12
            r12 = r16
            r13 = r16
            r14 = r0
            r13.<init>()
            r11.mShowImeRunnable = r12
            r11 = r0
            android.support.v7.widget.SearchView$2 r12 = new android.support.v7.widget.SearchView$2
            r16 = r12
            r12 = r16
            r13 = r16
            r14 = r0
            r13.<init>()
            r11.mUpdateDrawableStateRunnable = r12
            r11 = r0
            android.support.v7.widget.SearchView$3 r12 = new android.support.v7.widget.SearchView$3
            r16 = r12
            r12 = r16
            r13 = r16
            r14 = r0
            r13.<init>()
            r11.mReleaseCursorRunnable = r12
            r11 = r0
            java.util.WeakHashMap r12 = new java.util.WeakHashMap
            r16 = r12
            r12 = r16
            r13 = r16
            r13.<init>()
            r11.mOutsideDrawablesCache = r12
            r11 = r0
            android.support.v7.widget.SearchView$7 r12 = new android.support.v7.widget.SearchView$7
            r16 = r12
            r12 = r16
            r13 = r16
            r14 = r0
            r13.<init>()
            r11.mOnClickListener = r12
            r11 = r0
            android.support.v7.widget.SearchView$8 r12 = new android.support.v7.widget.SearchView$8
            r16 = r12
            r12 = r16
            r13 = r16
            r14 = r0
            r13.<init>()
            r11.mTextKeyListener = r12
            r11 = r0
            android.support.v7.widget.SearchView$9 r12 = new android.support.v7.widget.SearchView$9
            r16 = r12
            r12 = r16
            r13 = r16
            r14 = r0
            r13.<init>()
            r11.mOnEditorActionListener = r12
            r11 = r0
            android.support.v7.widget.SearchView$10 r12 = new android.support.v7.widget.SearchView$10
            r16 = r12
            r12 = r16
            r13 = r16
            r14 = r0
            r13.<init>()
            r11.mOnItemClickListener = r12
            r11 = r0
            android.support.v7.widget.SearchView$11 r12 = new android.support.v7.widget.SearchView$11
            r16 = r12
            r12 = r16
            r13 = r16
            r14 = r0
            r13.<init>()
            r11.mOnItemSelectedListener = r12
            r11 = r0
            android.support.v7.widget.SearchView$12 r12 = new android.support.v7.widget.SearchView$12
            r16 = r12
            r12 = r16
            r13 = r16
            r14 = r0
            r13.<init>()
            r11.mTextWatcher = r12
            r11 = r1
            r12 = r2
            int[] r13 = android.support.v7.appcompat.R.styleable.SearchView
            r14 = r3
            r15 = 0
            android.support.v7.widget.TintTypedArray r11 = android.support.v7.widget.TintTypedArray.obtainStyledAttributes(r11, r12, r13, r14, r15)
            r4 = r11
            r11 = r0
            r12 = r4
            android.support.v7.widget.TintManager r12 = r12.getTintManager()
            r11.mTintManager = r12
            r11 = r1
            android.view.LayoutInflater r11 = android.view.LayoutInflater.from(r11)
            r5 = r11
            r11 = r4
            int r12 = android.support.v7.appcompat.R.styleable.SearchView_layout
            int r13 = android.support.v7.appcompat.R.layout.abc_search_view
            int r11 = r11.getResourceId(r12, r13)
            r6 = r11
            r11 = r5
            r12 = r6
            r13 = r0
            r14 = 1
            android.view.View r11 = r11.inflate(r12, r13, r14)
            r11 = r0
            r12 = r0
            int r13 = android.support.v7.appcompat.R.id.search_src_text
            android.view.View r12 = r12.findViewById(r13)
            android.support.v7.widget.SearchView$SearchAutoComplete r12 = (android.support.v7.widget.SearchView.SearchAutoComplete) r12
            r11.mSearchSrcTextView = r12
            r11 = r0
            android.support.v7.widget.SearchView$SearchAutoComplete r11 = r11.mSearchSrcTextView
            r12 = r0
            r11.setSearchView(r12)
            r11 = r0
            r12 = r0
            int r13 = android.support.v7.appcompat.R.id.search_edit_frame
            android.view.View r12 = r12.findViewById(r13)
            r11.mSearchEditFrame = r12
            r11 = r0
            r12 = r0
            int r13 = android.support.v7.appcompat.R.id.search_plate
            android.view.View r12 = r12.findViewById(r13)
            r11.mSearchPlate = r12
            r11 = r0
            r12 = r0
            int r13 = android.support.v7.appcompat.R.id.submit_area
            android.view.View r12 = r12.findViewById(r13)
            r11.mSubmitArea = r12
            r11 = r0
            r12 = r0
            int r13 = android.support.v7.appcompat.R.id.search_button
            android.view.View r12 = r12.findViewById(r13)
            android.widget.ImageView r12 = (android.widget.ImageView) r12
            r11.mSearchButton = r12
            r11 = r0
            r12 = r0
            int r13 = android.support.v7.appcompat.R.id.search_go_btn
            android.view.View r12 = r12.findViewById(r13)
            android.widget.ImageView r12 = (android.widget.ImageView) r12
            r11.mGoButton = r12
            r11 = r0
            r12 = r0
            int r13 = android.support.v7.appcompat.R.id.search_close_btn
            android.view.View r12 = r12.findViewById(r13)
            android.widget.ImageView r12 = (android.widget.ImageView) r12
            r11.mCloseButton = r12
            r11 = r0
            r12 = r0
            int r13 = android.support.v7.appcompat.R.id.search_voice_btn
            android.view.View r12 = r12.findViewById(r13)
            android.widget.ImageView r12 = (android.widget.ImageView) r12
            r11.mVoiceButton = r12
            r11 = r0
            r12 = r0
            int r13 = android.support.v7.appcompat.R.id.search_mag_icon
            android.view.View r12 = r12.findViewById(r13)
            android.widget.ImageView r12 = (android.widget.ImageView) r12
            r11.mCollapsedIcon = r12
            r11 = r0
            android.view.View r11 = r11.mSearchPlate
            r12 = r4
            int r13 = android.support.v7.appcompat.R.styleable.SearchView_queryBackground
            android.graphics.drawable.Drawable r12 = r12.getDrawable(r13)
            r11.setBackgroundDrawable(r12)
            r11 = r0
            android.view.View r11 = r11.mSubmitArea
            r12 = r4
            int r13 = android.support.v7.appcompat.R.styleable.SearchView_submitBackground
            android.graphics.drawable.Drawable r12 = r12.getDrawable(r13)
            r11.setBackgroundDrawable(r12)
            r11 = r0
            android.widget.ImageView r11 = r11.mSearchButton
            r12 = r4
            int r13 = android.support.v7.appcompat.R.styleable.SearchView_searchIcon
            android.graphics.drawable.Drawable r12 = r12.getDrawable(r13)
            r11.setImageDrawable(r12)
            r11 = r0
            android.widget.ImageView r11 = r11.mGoButton
            r12 = r4
            int r13 = android.support.v7.appcompat.R.styleable.SearchView_goIcon
            android.graphics.drawable.Drawable r12 = r12.getDrawable(r13)
            r11.setImageDrawable(r12)
            r11 = r0
            android.widget.ImageView r11 = r11.mCloseButton
            r12 = r4
            int r13 = android.support.v7.appcompat.R.styleable.SearchView_closeIcon
            android.graphics.drawable.Drawable r12 = r12.getDrawable(r13)
            r11.setImageDrawable(r12)
            r11 = r0
            android.widget.ImageView r11 = r11.mVoiceButton
            r12 = r4
            int r13 = android.support.v7.appcompat.R.styleable.SearchView_voiceIcon
            android.graphics.drawable.Drawable r12 = r12.getDrawable(r13)
            r11.setImageDrawable(r12)
            r11 = r0
            android.widget.ImageView r11 = r11.mCollapsedIcon
            r12 = r4
            int r13 = android.support.v7.appcompat.R.styleable.SearchView_searchIcon
            android.graphics.drawable.Drawable r12 = r12.getDrawable(r13)
            r11.setImageDrawable(r12)
            r11 = r0
            r12 = r4
            int r13 = android.support.v7.appcompat.R.styleable.SearchView_searchHintIcon
            android.graphics.drawable.Drawable r12 = r12.getDrawable(r13)
            r11.mSearchHintIcon = r12
            r11 = r0
            r12 = r4
            int r13 = android.support.v7.appcompat.R.styleable.SearchView_suggestionRowLayout
            int r14 = android.support.v7.appcompat.R.layout.abc_search_dropdown_item_icons_2line
            int r12 = r12.getResourceId(r13, r14)
            r11.mSuggestionRowLayout = r12
            r11 = r0
            r12 = r4
            int r13 = android.support.v7.appcompat.R.styleable.SearchView_commitIcon
            r14 = 0
            int r12 = r12.getResourceId(r13, r14)
            r11.mSuggestionCommitIconResId = r12
            r11 = r0
            android.widget.ImageView r11 = r11.mSearchButton
            r12 = r0
            android.view.View$OnClickListener r12 = r12.mOnClickListener
            r11.setOnClickListener(r12)
            r11 = r0
            android.widget.ImageView r11 = r11.mCloseButton
            r12 = r0
            android.view.View$OnClickListener r12 = r12.mOnClickListener
            r11.setOnClickListener(r12)
            r11 = r0
            android.widget.ImageView r11 = r11.mGoButton
            r12 = r0
            android.view.View$OnClickListener r12 = r12.mOnClickListener
            r11.setOnClickListener(r12)
            r11 = r0
            android.widget.ImageView r11 = r11.mVoiceButton
            r12 = r0
            android.view.View$OnClickListener r12 = r12.mOnClickListener
            r11.setOnClickListener(r12)
            r11 = r0
            android.support.v7.widget.SearchView$SearchAutoComplete r11 = r11.mSearchSrcTextView
            r12 = r0
            android.view.View$OnClickListener r12 = r12.mOnClickListener
            r11.setOnClickListener(r12)
            r11 = r0
            android.support.v7.widget.SearchView$SearchAutoComplete r11 = r11.mSearchSrcTextView
            r12 = r0
            android.text.TextWatcher r12 = r12.mTextWatcher
            r11.addTextChangedListener(r12)
            r11 = r0
            android.support.v7.widget.SearchView$SearchAutoComplete r11 = r11.mSearchSrcTextView
            r12 = r0
            android.widget.TextView$OnEditorActionListener r12 = r12.mOnEditorActionListener
            r11.setOnEditorActionListener(r12)
            r11 = r0
            android.support.v7.widget.SearchView$SearchAutoComplete r11 = r11.mSearchSrcTextView
            r12 = r0
            android.widget.AdapterView$OnItemClickListener r12 = r12.mOnItemClickListener
            r11.setOnItemClickListener(r12)
            r11 = r0
            android.support.v7.widget.SearchView$SearchAutoComplete r11 = r11.mSearchSrcTextView
            r12 = r0
            android.widget.AdapterView$OnItemSelectedListener r12 = r12.mOnItemSelectedListener
            r11.setOnItemSelectedListener(r12)
            r11 = r0
            android.support.v7.widget.SearchView$SearchAutoComplete r11 = r11.mSearchSrcTextView
            r12 = r0
            android.view.View$OnKeyListener r12 = r12.mTextKeyListener
            r11.setOnKeyListener(r12)
            r11 = r0
            android.support.v7.widget.SearchView$SearchAutoComplete r11 = r11.mSearchSrcTextView
            android.support.v7.widget.SearchView$4 r12 = new android.support.v7.widget.SearchView$4
            r16 = r12
            r12 = r16
            r13 = r16
            r14 = r0
            r13.<init>()
            r11.setOnFocusChangeListener(r12)
            r11 = r0
            r12 = r4
            int r13 = android.support.v7.appcompat.R.styleable.SearchView_iconifiedByDefault
            r14 = 1
            boolean r12 = r12.getBoolean(r13, r14)
            r11.setIconifiedByDefault(r12)
            r11 = r4
            int r12 = android.support.v7.appcompat.R.styleable.SearchView_android_maxWidth
            r13 = -1
            int r11 = r11.getDimensionPixelSize(r12, r13)
            r7 = r11
            r11 = r7
            r12 = -1
            if (r11 == r12) goto L_0x0242
            r11 = r0
            r12 = r7
            r11.setMaxWidth(r12)
        L_0x0242:
            r11 = r0
            r12 = r4
            int r13 = android.support.v7.appcompat.R.styleable.SearchView_defaultQueryHint
            java.lang.CharSequence r12 = r12.getText(r13)
            r11.mDefaultQueryHint = r12
            r11 = r0
            r12 = r4
            int r13 = android.support.v7.appcompat.R.styleable.SearchView_queryHint
            java.lang.CharSequence r12 = r12.getText(r13)
            r11.mQueryHint = r12
            r11 = r4
            int r12 = android.support.v7.appcompat.R.styleable.SearchView_android_imeOptions
            r13 = -1
            int r11 = r11.getInt(r12, r13)
            r8 = r11
            r11 = r8
            r12 = -1
            if (r11 == r12) goto L_0x0268
            r11 = r0
            r12 = r8
            r11.setImeOptions(r12)
        L_0x0268:
            r11 = r4
            int r12 = android.support.v7.appcompat.R.styleable.SearchView_android_inputType
            r13 = -1
            int r11 = r11.getInt(r12, r13)
            r9 = r11
            r11 = r9
            r12 = -1
            if (r11 == r12) goto L_0x027a
            r11 = r0
            r12 = r9
            r11.setInputType(r12)
        L_0x027a:
            r11 = 1
            r10 = r11
            r11 = r4
            int r12 = android.support.v7.appcompat.R.styleable.SearchView_android_focusable
            r13 = r10
            boolean r11 = r11.getBoolean(r12, r13)
            r10 = r11
            r11 = r0
            r12 = r10
            r11.setFocusable(r12)
            r11 = r4
            r11.recycle()
            r11 = r0
            android.content.Intent r12 = new android.content.Intent
            r16 = r12
            r12 = r16
            r13 = r16
            java.lang.String r14 = "android.speech.action.WEB_SEARCH"
            r13.<init>(r14)
            r11.mVoiceWebSearchIntent = r12
            r11 = r0
            android.content.Intent r11 = r11.mVoiceWebSearchIntent
            r12 = 268435456(0x10000000, float:2.5243549E-29)
            android.content.Intent r11 = r11.addFlags(r12)
            r11 = r0
            android.content.Intent r11 = r11.mVoiceWebSearchIntent
            java.lang.String r12 = "android.speech.extra.LANGUAGE_MODEL"
            java.lang.String r13 = "web_search"
            android.content.Intent r11 = r11.putExtra(r12, r13)
            r11 = r0
            android.content.Intent r12 = new android.content.Intent
            r16 = r12
            r12 = r16
            r13 = r16
            java.lang.String r14 = "android.speech.action.RECOGNIZE_SPEECH"
            r13.<init>(r14)
            r11.mVoiceAppSearchIntent = r12
            r11 = r0
            android.content.Intent r11 = r11.mVoiceAppSearchIntent
            r12 = 268435456(0x10000000, float:2.5243549E-29)
            android.content.Intent r11 = r11.addFlags(r12)
            r11 = r0
            r12 = r0
            r13 = r0
            android.support.v7.widget.SearchView$SearchAutoComplete r13 = r13.mSearchSrcTextView
            int r13 = r13.getDropDownAnchor()
            android.view.View r12 = r12.findViewById(r13)
            r11.mDropDownAnchor = r12
            r11 = r0
            android.view.View r11 = r11.mDropDownAnchor
            if (r11 == 0) goto L_0x02e9
            int r11 = android.os.Build.VERSION.SDK_INT
            r12 = 11
            if (r11 < r12) goto L_0x02f5
            r11 = r0
            r11.addOnLayoutChangeListenerToDropDownAnchorSDK11()
        L_0x02e9:
            r11 = r0
            r12 = r0
            boolean r12 = r12.mIconifiedByDefault
            r11.updateViewsVisibility(r12)
            r11 = r0
            r11.updateQueryHint()
            return
        L_0x02f5:
            r11 = r0
            r11.addOnLayoutChangeListenerToDropDownAnchorBase()
            goto L_0x02e9
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.SearchView.<init>(android.content.Context, android.util.AttributeSet, int):void");
    }

    @TargetApi(11)
    private void addOnLayoutChangeListenerToDropDownAnchorSDK11() {
        View.OnLayoutChangeListener onLayoutChangeListener;
        new View.OnLayoutChangeListener() {
            public void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
                SearchView.this.adjustDropDownSizeAndPosition();
            }
        };
        this.mDropDownAnchor.addOnLayoutChangeListener(onLayoutChangeListener);
    }

    private void addOnLayoutChangeListenerToDropDownAnchorBase() {
        ViewTreeObserver.OnGlobalLayoutListener onGlobalLayoutListener;
        new ViewTreeObserver.OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                SearchView.this.adjustDropDownSizeAndPosition();
            }
        };
        this.mDropDownAnchor.getViewTreeObserver().addOnGlobalLayoutListener(onGlobalLayoutListener);
    }

    /* access modifiers changed from: package-private */
    public int getSuggestionRowLayout() {
        return this.mSuggestionRowLayout;
    }

    /* access modifiers changed from: package-private */
    public int getSuggestionCommitIconResId() {
        return this.mSuggestionCommitIconResId;
    }

    public void setSearchableInfo(SearchableInfo searchableInfo) {
        this.mSearchable = searchableInfo;
        if (this.mSearchable != null) {
            if (IS_AT_LEAST_FROYO) {
                updateSearchAutoComplete();
            }
            updateQueryHint();
        }
        this.mVoiceButtonEnabled = IS_AT_LEAST_FROYO && hasVoiceSearch();
        if (this.mVoiceButtonEnabled) {
            this.mSearchSrcTextView.setPrivateImeOptions(IME_OPTION_NO_MICROPHONE);
        }
        updateViewsVisibility(isIconified());
    }

    public void setAppSearchData(Bundle bundle) {
        this.mAppSearchData = bundle;
    }

    public void setImeOptions(int i) {
        this.mSearchSrcTextView.setImeOptions(i);
    }

    public int getImeOptions() {
        return this.mSearchSrcTextView.getImeOptions();
    }

    public void setInputType(int i) {
        this.mSearchSrcTextView.setInputType(i);
    }

    public int getInputType() {
        return this.mSearchSrcTextView.getInputType();
    }

    public boolean requestFocus(int i, Rect rect) {
        int i2 = i;
        Rect rect2 = rect;
        if (this.mClearingFocus) {
            return false;
        }
        if (!isFocusable()) {
            return false;
        }
        if (isIconified()) {
            return super.requestFocus(i2, rect2);
        }
        boolean requestFocus = this.mSearchSrcTextView.requestFocus(i2, rect2);
        if (requestFocus) {
            updateViewsVisibility(false);
        }
        return requestFocus;
    }

    public void clearFocus() {
        this.mClearingFocus = true;
        setImeVisibility(false);
        super.clearFocus();
        this.mSearchSrcTextView.clearFocus();
        this.mClearingFocus = false;
    }

    public void setOnQueryTextListener(OnQueryTextListener onQueryTextListener) {
        this.mOnQueryChangeListener = onQueryTextListener;
    }

    public void setOnCloseListener(OnCloseListener onCloseListener) {
        this.mOnCloseListener = onCloseListener;
    }

    public void setOnQueryTextFocusChangeListener(View.OnFocusChangeListener onFocusChangeListener) {
        this.mOnQueryTextFocusChangeListener = onFocusChangeListener;
    }

    public void setOnSuggestionListener(OnSuggestionListener onSuggestionListener) {
        this.mOnSuggestionListener = onSuggestionListener;
    }

    public void setOnSearchClickListener(View.OnClickListener onClickListener) {
        this.mOnSearchClickListener = onClickListener;
    }

    public CharSequence getQuery() {
        return this.mSearchSrcTextView.getText();
    }

    public void setQuery(CharSequence charSequence, boolean z) {
        CharSequence charSequence2 = charSequence;
        boolean z2 = z;
        this.mSearchSrcTextView.setText(charSequence2);
        if (charSequence2 != null) {
            this.mSearchSrcTextView.setSelection(this.mSearchSrcTextView.length());
            this.mUserQuery = charSequence2;
        }
        if (z2 && !TextUtils.isEmpty(charSequence2)) {
            onSubmitQuery();
        }
    }

    public void setQueryHint(CharSequence charSequence) {
        this.mQueryHint = charSequence;
        updateQueryHint();
    }

    public CharSequence getQueryHint() {
        CharSequence charSequence;
        if (this.mQueryHint != null) {
            charSequence = this.mQueryHint;
        } else if (!IS_AT_LEAST_FROYO || this.mSearchable == null || this.mSearchable.getHintId() == 0) {
            charSequence = this.mDefaultQueryHint;
        } else {
            charSequence = getContext().getText(this.mSearchable.getHintId());
        }
        return charSequence;
    }

    public void setIconifiedByDefault(boolean z) {
        boolean z2 = z;
        if (this.mIconifiedByDefault != z2) {
            this.mIconifiedByDefault = z2;
            updateViewsVisibility(z2);
            updateQueryHint();
        }
    }

    public boolean isIconfiedByDefault() {
        return this.mIconifiedByDefault;
    }

    public void setIconified(boolean z) {
        if (z) {
            onCloseClicked();
        } else {
            onSearchClicked();
        }
    }

    public boolean isIconified() {
        return this.mIconified;
    }

    public void setSubmitButtonEnabled(boolean z) {
        this.mSubmitButtonEnabled = z;
        updateViewsVisibility(isIconified());
    }

    public boolean isSubmitButtonEnabled() {
        return this.mSubmitButtonEnabled;
    }

    public void setQueryRefinementEnabled(boolean z) {
        boolean z2 = z;
        this.mQueryRefinement = z2;
        if (this.mSuggestionsAdapter instanceof SuggestionsAdapter) {
            ((SuggestionsAdapter) this.mSuggestionsAdapter).setQueryRefinement(z2 ? 2 : 1);
        }
    }

    public boolean isQueryRefinementEnabled() {
        return this.mQueryRefinement;
    }

    public void setSuggestionsAdapter(CursorAdapter cursorAdapter) {
        this.mSuggestionsAdapter = cursorAdapter;
        this.mSearchSrcTextView.setAdapter(this.mSuggestionsAdapter);
    }

    public CursorAdapter getSuggestionsAdapter() {
        return this.mSuggestionsAdapter;
    }

    public void setMaxWidth(int i) {
        this.mMaxWidth = i;
        requestLayout();
    }

    public int getMaxWidth() {
        return this.mMaxWidth;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int i3 = i;
        int i4 = i2;
        if (isIconified()) {
            super.onMeasure(i3, i4);
            return;
        }
        int mode = View.MeasureSpec.getMode(i3);
        int size = View.MeasureSpec.getSize(i3);
        switch (mode) {
            case Integer.MIN_VALUE:
                if (this.mMaxWidth <= 0) {
                    size = Math.min(getPreferredWidth(), size);
                    break;
                } else {
                    size = Math.min(this.mMaxWidth, size);
                    break;
                }
            case 0:
                size = this.mMaxWidth > 0 ? this.mMaxWidth : getPreferredWidth();
                break;
            case 1073741824:
                if (this.mMaxWidth > 0) {
                    size = Math.min(this.mMaxWidth, size);
                    break;
                }
                break;
        }
        super.onMeasure(View.MeasureSpec.makeMeasureSpec(size, 1073741824), i4);
    }

    private int getPreferredWidth() {
        return getContext().getResources().getDimensionPixelSize(R.dimen.abc_search_view_preferred_width);
    }

    private void updateViewsVisibility(boolean z) {
        int i;
        boolean z2 = z;
        this.mIconified = z2;
        int i2 = z2 ? 0 : 8;
        boolean z3 = !TextUtils.isEmpty(this.mSearchSrcTextView.getText());
        this.mSearchButton.setVisibility(i2);
        updateSubmitButton(z3);
        this.mSearchEditFrame.setVisibility(z2 ? 8 : 0);
        if (this.mCollapsedIcon.getDrawable() == null || this.mIconifiedByDefault) {
            i = 8;
        } else {
            i = 0;
        }
        this.mCollapsedIcon.setVisibility(i);
        updateCloseButton();
        updateVoiceButton(!z3);
        updateSubmitArea();
    }

    @TargetApi(8)
    private boolean hasVoiceSearch() {
        if (this.mSearchable != null && this.mSearchable.getVoiceSearchEnabled()) {
            Intent intent = null;
            if (this.mSearchable.getVoiceSearchLaunchWebSearch()) {
                intent = this.mVoiceWebSearchIntent;
            } else if (this.mSearchable.getVoiceSearchLaunchRecognizer()) {
                intent = this.mVoiceAppSearchIntent;
            }
            if (intent != null) {
                return getContext().getPackageManager().resolveActivity(intent, 65536) != null;
            }
        }
        return false;
    }

    private boolean isSubmitAreaEnabled() {
        return (this.mSubmitButtonEnabled || this.mVoiceButtonEnabled) && !isIconified();
    }

    private void updateSubmitButton(boolean z) {
        boolean z2 = z;
        int i = 8;
        if (this.mSubmitButtonEnabled && isSubmitAreaEnabled() && hasFocus() && (z2 || !this.mVoiceButtonEnabled)) {
            i = 0;
        }
        this.mGoButton.setVisibility(i);
    }

    private void updateSubmitArea() {
        int i = 8;
        if (isSubmitAreaEnabled() && (this.mGoButton.getVisibility() == 0 || this.mVoiceButton.getVisibility() == 0)) {
            i = 0;
        }
        this.mSubmitArea.setVisibility(i);
    }

    private void updateCloseButton() {
        boolean z = !TextUtils.isEmpty(this.mSearchSrcTextView.getText());
        this.mCloseButton.setVisibility(z || (this.mIconifiedByDefault && !this.mExpandedInActionView) ? 0 : 8);
        Drawable drawable = this.mCloseButton.getDrawable();
        if (drawable != null) {
            boolean state = drawable.setState(z ? ENABLED_STATE_SET : EMPTY_STATE_SET);
        }
    }

    private void postUpdateFocusedState() {
        boolean post = post(this.mUpdateDrawableStateRunnable);
    }

    /* access modifiers changed from: private */
    public void updateFocusedState() {
        int[] iArr = this.mSearchSrcTextView.hasFocus() ? FOCUSED_STATE_SET : EMPTY_STATE_SET;
        Drawable background = this.mSearchPlate.getBackground();
        if (background != null) {
            boolean state = background.setState(iArr);
        }
        Drawable background2 = this.mSubmitArea.getBackground();
        if (background2 != null) {
            boolean state2 = background2.setState(iArr);
        }
        invalidate();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        boolean removeCallbacks = removeCallbacks(this.mUpdateDrawableStateRunnable);
        boolean post = post(this.mReleaseCursorRunnable);
        super.onDetachedFromWindow();
    }

    /* access modifiers changed from: private */
    public void setImeVisibility(boolean z) {
        if (z) {
            boolean post = post(this.mShowImeRunnable);
            return;
        }
        boolean removeCallbacks = removeCallbacks(this.mShowImeRunnable);
        InputMethodManager inputMethodManager = (InputMethodManager) getContext().getSystemService("input_method");
        if (inputMethodManager != null) {
            boolean hideSoftInputFromWindow = inputMethodManager.hideSoftInputFromWindow(getWindowToken(), 0);
        }
    }

    /* access modifiers changed from: package-private */
    public void onQueryRefine(CharSequence charSequence) {
        setQuery(charSequence);
    }

    /* access modifiers changed from: private */
    public boolean onSuggestionsKey(View view, int i, KeyEvent keyEvent) {
        int length;
        int i2 = i;
        KeyEvent keyEvent2 = keyEvent;
        if (this.mSearchable == null) {
            return false;
        }
        if (this.mSuggestionsAdapter == null) {
            return false;
        }
        if (keyEvent2.getAction() == 0 && KeyEventCompat.hasNoModifiers(keyEvent2)) {
            if (i2 == 66 || i2 == 84 || i2 == 61) {
                return onItemClicked(this.mSearchSrcTextView.getListSelection(), 0, null);
            }
            if (i2 == 21 || i2 == 22) {
                if (i2 == 21) {
                    length = 0;
                } else {
                    length = this.mSearchSrcTextView.length();
                }
                this.mSearchSrcTextView.setSelection(length);
                this.mSearchSrcTextView.setListSelection(0);
                this.mSearchSrcTextView.clearListSelection();
                HIDDEN_METHOD_INVOKER.ensureImeVisible(this.mSearchSrcTextView, true);
                return true;
            } else if (i2 == 19 && 0 == this.mSearchSrcTextView.getListSelection()) {
                return false;
            }
        }
        return false;
    }

    private CharSequence getDecoratedHint(CharSequence charSequence) {
        SpannableStringBuilder spannableStringBuilder;
        Object obj;
        CharSequence charSequence2 = charSequence;
        if (!this.mIconifiedByDefault || this.mSearchHintIcon == null) {
            return charSequence2;
        }
        int textSize = (int) (((double) this.mSearchSrcTextView.getTextSize()) * 1.25d);
        this.mSearchHintIcon.setBounds(0, 0, textSize, textSize);
        new SpannableStringBuilder("   ");
        SpannableStringBuilder spannableStringBuilder2 = spannableStringBuilder;
        new ImageSpan(this.mSearchHintIcon);
        spannableStringBuilder2.setSpan(obj, 1, 2, 33);
        SpannableStringBuilder append = spannableStringBuilder2.append(charSequence2);
        return spannableStringBuilder2;
    }

    private void updateQueryHint() {
        CharSequence queryHint = getQueryHint();
        this.mSearchSrcTextView.setHint(getDecoratedHint(queryHint == null ? "" : queryHint));
    }

    @TargetApi(8)
    private void updateSearchAutoComplete() {
        CursorAdapter cursorAdapter;
        this.mSearchSrcTextView.setThreshold(this.mSearchable.getSuggestThreshold());
        this.mSearchSrcTextView.setImeOptions(this.mSearchable.getImeOptions());
        int inputType = this.mSearchable.getInputType();
        if ((inputType & 15) == 1) {
            inputType &= -65537;
            if (this.mSearchable.getSuggestAuthority() != null) {
                inputType = inputType | 65536 | 524288;
            }
        }
        this.mSearchSrcTextView.setInputType(inputType);
        if (this.mSuggestionsAdapter != null) {
            this.mSuggestionsAdapter.changeCursor(null);
        }
        if (this.mSearchable.getSuggestAuthority() != null) {
            new SuggestionsAdapter(getContext(), this, this.mSearchable, this.mOutsideDrawablesCache);
            this.mSuggestionsAdapter = cursorAdapter;
            this.mSearchSrcTextView.setAdapter(this.mSuggestionsAdapter);
            ((SuggestionsAdapter) this.mSuggestionsAdapter).setQueryRefinement(this.mQueryRefinement ? 2 : 1);
        }
    }

    private void updateVoiceButton(boolean z) {
        boolean z2 = z;
        int i = 8;
        if (this.mVoiceButtonEnabled && !isIconified() && z2) {
            i = 0;
            this.mGoButton.setVisibility(8);
        }
        this.mVoiceButton.setVisibility(i);
    }

    /* access modifiers changed from: private */
    public void onTextChanged(CharSequence charSequence) {
        CharSequence charSequence2 = charSequence;
        Editable text = this.mSearchSrcTextView.getText();
        this.mUserQuery = text;
        boolean z = !TextUtils.isEmpty(text);
        updateSubmitButton(z);
        updateVoiceButton(!z);
        updateCloseButton();
        updateSubmitArea();
        if (this.mOnQueryChangeListener != null && !TextUtils.equals(charSequence2, this.mOldQueryText)) {
            boolean onQueryTextChange = this.mOnQueryChangeListener.onQueryTextChange(charSequence2.toString());
        }
        this.mOldQueryText = charSequence2.toString();
    }

    /* access modifiers changed from: private */
    public void onSubmitQuery() {
        Editable text = this.mSearchSrcTextView.getText();
        if (text != null && TextUtils.getTrimmedLength(text) > 0) {
            if (this.mOnQueryChangeListener == null || !this.mOnQueryChangeListener.onQueryTextSubmit(text.toString())) {
                if (this.mSearchable != null) {
                    launchQuerySearch(0, null, text.toString());
                }
                setImeVisibility(false);
                dismissSuggestions();
            }
        }
    }

    private void dismissSuggestions() {
        this.mSearchSrcTextView.dismissDropDown();
    }

    /* access modifiers changed from: private */
    public void onCloseClicked() {
        if (!TextUtils.isEmpty(this.mSearchSrcTextView.getText())) {
            this.mSearchSrcTextView.setText("");
            boolean requestFocus = this.mSearchSrcTextView.requestFocus();
            setImeVisibility(true);
        } else if (!this.mIconifiedByDefault) {
        } else {
            if (this.mOnCloseListener == null || !this.mOnCloseListener.onClose()) {
                clearFocus();
                updateViewsVisibility(true);
            }
        }
    }

    /* access modifiers changed from: private */
    public void onSearchClicked() {
        updateViewsVisibility(false);
        boolean requestFocus = this.mSearchSrcTextView.requestFocus();
        setImeVisibility(true);
        if (this.mOnSearchClickListener != null) {
            this.mOnSearchClickListener.onClick(this);
        }
    }

    /* access modifiers changed from: private */
    @TargetApi(8)
    public void onVoiceClicked() {
        if (this.mSearchable != null) {
            SearchableInfo searchableInfo = this.mSearchable;
            try {
                if (searchableInfo.getVoiceSearchLaunchWebSearch()) {
                    getContext().startActivity(createVoiceWebSearchIntent(this.mVoiceWebSearchIntent, searchableInfo));
                } else if (searchableInfo.getVoiceSearchLaunchRecognizer()) {
                    getContext().startActivity(createVoiceAppSearchIntent(this.mVoiceAppSearchIntent, searchableInfo));
                }
            } catch (ActivityNotFoundException e) {
                int w = Log.w(LOG_TAG, "Could not find voice search activity");
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void onTextFocusChanged() {
        updateViewsVisibility(isIconified());
        postUpdateFocusedState();
        if (this.mSearchSrcTextView.hasFocus()) {
            forceSuggestionQuery();
        }
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        postUpdateFocusedState();
    }

    public void onActionViewCollapsed() {
        setQuery("", false);
        clearFocus();
        updateViewsVisibility(true);
        this.mSearchSrcTextView.setImeOptions(this.mCollapsedImeOptions);
        this.mExpandedInActionView = false;
    }

    public void onActionViewExpanded() {
        if (!this.mExpandedInActionView) {
            this.mExpandedInActionView = true;
            this.mCollapsedImeOptions = this.mSearchSrcTextView.getImeOptions();
            this.mSearchSrcTextView.setImeOptions(this.mCollapsedImeOptions | 33554432);
            this.mSearchSrcTextView.setText("");
            setIconified(false);
        }
    }

    /* access modifiers changed from: private */
    public void adjustDropDownSizeAndPosition() {
        Rect rect;
        int i;
        if (this.mDropDownAnchor.getWidth() > 1) {
            Resources resources = getContext().getResources();
            int paddingLeft = this.mSearchPlate.getPaddingLeft();
            new Rect();
            Rect rect2 = rect;
            boolean isLayoutRtl = ViewUtils.isLayoutRtl(this);
            int dimensionPixelSize = this.mIconifiedByDefault ? resources.getDimensionPixelSize(R.dimen.abc_dropdownitem_icon_width) + resources.getDimensionPixelSize(R.dimen.abc_dropdownitem_text_padding_left) : 0;
            boolean padding = this.mSearchSrcTextView.getDropDownBackground().getPadding(rect2);
            if (isLayoutRtl) {
                i = -rect2.left;
            } else {
                i = paddingLeft - (rect2.left + dimensionPixelSize);
            }
            this.mSearchSrcTextView.setDropDownHorizontalOffset(i);
            this.mSearchSrcTextView.setDropDownWidth((((this.mDropDownAnchor.getWidth() + rect2.left) + rect2.right) + dimensionPixelSize) - paddingLeft);
        }
    }

    /* access modifiers changed from: private */
    public boolean onItemClicked(int i, int i2, String str) {
        int i3 = i;
        if (this.mOnSuggestionListener != null && this.mOnSuggestionListener.onSuggestionClick(i3)) {
            return false;
        }
        boolean launchSuggestion = launchSuggestion(i3, 0, null);
        setImeVisibility(false);
        dismissSuggestions();
        return true;
    }

    /* access modifiers changed from: private */
    public boolean onItemSelected(int i) {
        int i2 = i;
        if (this.mOnSuggestionListener != null && this.mOnSuggestionListener.onSuggestionSelect(i2)) {
            return false;
        }
        rewriteQueryFromSuggestion(i2);
        return true;
    }

    private void rewriteQueryFromSuggestion(int i) {
        int i2 = i;
        Editable text = this.mSearchSrcTextView.getText();
        Cursor cursor = this.mSuggestionsAdapter.getCursor();
        if (cursor != null) {
            if (cursor.moveToPosition(i2)) {
                CharSequence convertToString = this.mSuggestionsAdapter.convertToString(cursor);
                if (convertToString != null) {
                    setQuery(convertToString);
                } else {
                    setQuery(text);
                }
            } else {
                setQuery(text);
            }
        }
    }

    private boolean launchSuggestion(int i, int i2, String str) {
        int i3 = i;
        int i4 = i2;
        String str2 = str;
        Cursor cursor = this.mSuggestionsAdapter.getCursor();
        if (cursor == null || !cursor.moveToPosition(i3)) {
            return false;
        }
        launchIntent(createIntentFromSuggestion(cursor, i4, str2));
        return true;
    }

    private void launchIntent(Intent intent) {
        StringBuilder sb;
        Intent intent2 = intent;
        if (intent2 != null) {
            try {
                getContext().startActivity(intent2);
            } catch (RuntimeException e) {
                new StringBuilder();
                int e2 = Log.e(LOG_TAG, sb.append("Failed launch activity: ").append(intent2).toString(), e);
            }
        }
    }

    private void setQuery(CharSequence charSequence) {
        CharSequence charSequence2 = charSequence;
        this.mSearchSrcTextView.setText(charSequence2);
        this.mSearchSrcTextView.setSelection(TextUtils.isEmpty(charSequence2) ? 0 : charSequence2.length());
    }

    /* access modifiers changed from: private */
    public void launchQuerySearch(int i, String str, String str2) {
        getContext().startActivity(createIntent("android.intent.action.SEARCH", null, null, str2, i, str));
    }

    private Intent createIntent(String str, Uri uri, String str2, String str3, int i, String str4) {
        Intent intent;
        Uri uri2 = uri;
        String str5 = str2;
        String str6 = str3;
        int i2 = i;
        String str7 = str4;
        new Intent(str);
        Intent intent2 = intent;
        Intent addFlags = intent2.addFlags(268435456);
        if (uri2 != null) {
            Intent data = intent2.setData(uri2);
        }
        Intent putExtra = intent2.putExtra("user_query", this.mUserQuery);
        if (str6 != null) {
            Intent putExtra2 = intent2.putExtra("query", str6);
        }
        if (str5 != null) {
            Intent putExtra3 = intent2.putExtra("intent_extra_data_key", str5);
        }
        if (this.mAppSearchData != null) {
            Intent putExtra4 = intent2.putExtra("app_data", this.mAppSearchData);
        }
        if (i2 != 0) {
            Intent putExtra5 = intent2.putExtra("action_key", i2);
            Intent putExtra6 = intent2.putExtra("action_msg", str7);
        }
        if (IS_AT_LEAST_FROYO) {
            Intent component = intent2.setComponent(this.mSearchable.getSearchActivity());
        }
        return intent2;
    }

    @TargetApi(8)
    private Intent createVoiceWebSearchIntent(Intent intent, SearchableInfo searchableInfo) {
        Intent intent2;
        new Intent(intent);
        Intent intent3 = intent2;
        ComponentName searchActivity = searchableInfo.getSearchActivity();
        Intent putExtra = intent3.putExtra("calling_package", searchActivity == null ? null : searchActivity.flattenToShortString());
        return intent3;
    }

    @TargetApi(8)
    private Intent createVoiceAppSearchIntent(Intent intent, SearchableInfo searchableInfo) {
        Intent intent2;
        Bundle bundle;
        Intent intent3;
        Intent intent4 = intent;
        SearchableInfo searchableInfo2 = searchableInfo;
        ComponentName searchActivity = searchableInfo2.getSearchActivity();
        new Intent("android.intent.action.SEARCH");
        Intent intent5 = intent2;
        Intent component = intent5.setComponent(searchActivity);
        PendingIntent activity = PendingIntent.getActivity(getContext(), 0, intent5, 1073741824);
        new Bundle();
        Bundle bundle2 = bundle;
        if (this.mAppSearchData != null) {
            bundle2.putParcelable("app_data", this.mAppSearchData);
        }
        new Intent(intent4);
        Intent intent6 = intent3;
        String str = "free_form";
        String str2 = null;
        String str3 = null;
        int i = 1;
        if (Build.VERSION.SDK_INT >= 8) {
            Resources resources = getResources();
            if (searchableInfo2.getVoiceLanguageModeId() != 0) {
                str = resources.getString(searchableInfo2.getVoiceLanguageModeId());
            }
            if (searchableInfo2.getVoicePromptTextId() != 0) {
                str2 = resources.getString(searchableInfo2.getVoicePromptTextId());
            }
            if (searchableInfo2.getVoiceLanguageId() != 0) {
                str3 = resources.getString(searchableInfo2.getVoiceLanguageId());
            }
            if (searchableInfo2.getVoiceMaxResults() != 0) {
                i = searchableInfo2.getVoiceMaxResults();
            }
        }
        Intent putExtra = intent6.putExtra("android.speech.extra.LANGUAGE_MODEL", str);
        Intent putExtra2 = intent6.putExtra("android.speech.extra.PROMPT", str2);
        Intent putExtra3 = intent6.putExtra("android.speech.extra.LANGUAGE", str3);
        Intent putExtra4 = intent6.putExtra("android.speech.extra.MAX_RESULTS", i);
        Intent putExtra5 = intent6.putExtra("calling_package", searchActivity == null ? null : searchActivity.flattenToShortString());
        Intent putExtra6 = intent6.putExtra("android.speech.extra.RESULTS_PENDINGINTENT", activity);
        Intent putExtra7 = intent6.putExtra("android.speech.extra.RESULTS_PENDINGINTENT_BUNDLE", bundle2);
        return intent6;
    }

    private Intent createIntentFromSuggestion(Cursor cursor, int i, String str) {
        int i2;
        StringBuilder sb;
        String columnString;
        StringBuilder sb2;
        Cursor cursor2 = cursor;
        int i3 = i;
        String str2 = str;
        try {
            String columnString2 = SuggestionsAdapter.getColumnString(cursor2, "suggest_intent_action");
            if (columnString2 == null && Build.VERSION.SDK_INT >= 8) {
                columnString2 = this.mSearchable.getSuggestIntentAction();
            }
            if (columnString2 == null) {
                columnString2 = "android.intent.action.SEARCH";
            }
            String columnString3 = SuggestionsAdapter.getColumnString(cursor2, "suggest_intent_data");
            if (IS_AT_LEAST_FROYO && columnString3 == null) {
                columnString3 = this.mSearchable.getSuggestIntentData();
            }
            if (!(columnString3 == null || (columnString = SuggestionsAdapter.getColumnString(cursor2, "suggest_intent_data_id")) == null)) {
                new StringBuilder();
                columnString3 = sb2.append(columnString3).append("/").append(Uri.encode(columnString)).toString();
            }
            return createIntent(columnString2, columnString3 == null ? null : Uri.parse(columnString3), SuggestionsAdapter.getColumnString(cursor2, "suggest_intent_extra_data"), SuggestionsAdapter.getColumnString(cursor2, "suggest_intent_query"), i3, str2);
        } catch (RuntimeException e) {
            RuntimeException runtimeException = e;
            try {
                i2 = cursor2.getPosition();
            } catch (RuntimeException e2) {
                i2 = -1;
            }
            new StringBuilder();
            int w = Log.w(LOG_TAG, sb.append("Search suggestions cursor at row ").append(i2).append(" returned exception.").toString(), runtimeException);
            return null;
        }
    }

    /* access modifiers changed from: private */
    public void forceSuggestionQuery() {
        HIDDEN_METHOD_INVOKER.doBeforeTextChanged(this.mSearchSrcTextView);
        HIDDEN_METHOD_INVOKER.doAfterTextChanged(this.mSearchSrcTextView);
    }

    static boolean isLandscapeMode(Context context) {
        return context.getResources().getConfiguration().orientation == 2;
    }

    public static class SearchAutoComplete extends AppCompatAutoCompleteTextView {
        private SearchView mSearchView;
        private int mThreshold;

        public SearchAutoComplete(Context context) {
            this(context, null);
        }

        public SearchAutoComplete(Context context, AttributeSet attributeSet) {
            this(context, attributeSet, R.attr.autoCompleteTextViewStyle);
        }

        public SearchAutoComplete(Context context, AttributeSet attributeSet, int i) {
            super(context, attributeSet, i);
            this.mThreshold = getThreshold();
        }

        /* access modifiers changed from: package-private */
        public void setSearchView(SearchView searchView) {
            this.mSearchView = searchView;
        }

        public void setThreshold(int i) {
            int i2 = i;
            super.setThreshold(i2);
            this.mThreshold = i2;
        }

        /* access modifiers changed from: private */
        public boolean isEmpty() {
            return TextUtils.getTrimmedLength(getText()) == 0;
        }

        /* access modifiers changed from: protected */
        public void replaceText(CharSequence charSequence) {
        }

        public void performCompletion() {
        }

        public void onWindowFocusChanged(boolean z) {
            boolean z2 = z;
            super.onWindowFocusChanged(z2);
            if (z2 && this.mSearchView.hasFocus() && getVisibility() == 0) {
                boolean showSoftInput = ((InputMethodManager) getContext().getSystemService("input_method")).showSoftInput(this, 0);
                if (SearchView.isLandscapeMode(getContext())) {
                    SearchView.HIDDEN_METHOD_INVOKER.ensureImeVisible(this, true);
                }
            }
        }

        /* access modifiers changed from: protected */
        public void onFocusChanged(boolean z, int i, Rect rect) {
            super.onFocusChanged(z, i, rect);
            this.mSearchView.onTextFocusChanged();
        }

        public boolean enoughToFilter() {
            return this.mThreshold <= 0 || super.enoughToFilter();
        }

        public boolean onKeyPreIme(int i, KeyEvent keyEvent) {
            int i2 = i;
            KeyEvent keyEvent2 = keyEvent;
            if (i2 == 4) {
                if (keyEvent2.getAction() == 0 && keyEvent2.getRepeatCount() == 0) {
                    KeyEvent.DispatcherState keyDispatcherState = getKeyDispatcherState();
                    if (keyDispatcherState != null) {
                        keyDispatcherState.startTracking(keyEvent2, this);
                    }
                    return true;
                } else if (keyEvent2.getAction() == 1) {
                    KeyEvent.DispatcherState keyDispatcherState2 = getKeyDispatcherState();
                    if (keyDispatcherState2 != null) {
                        keyDispatcherState2.handleUpEvent(keyEvent2);
                    }
                    if (keyEvent2.isTracking() && !keyEvent2.isCanceled()) {
                        this.mSearchView.clearFocus();
                        this.mSearchView.setImeVisibility(false);
                        return true;
                    }
                }
            }
            return super.onKeyPreIme(i2, keyEvent2);
        }
    }

    private static class AutoCompleteTextViewReflector {
        private Method doAfterTextChanged;
        private Method doBeforeTextChanged;
        private Method ensureImeVisible;
        private Method showSoftInputUnchecked;

        AutoCompleteTextViewReflector() {
            try {
                this.doBeforeTextChanged = AutoCompleteTextView.class.getDeclaredMethod("doBeforeTextChanged", new Class[0]);
                this.doBeforeTextChanged.setAccessible(true);
            } catch (NoSuchMethodException e) {
            }
            try {
                this.doAfterTextChanged = AutoCompleteTextView.class.getDeclaredMethod("doAfterTextChanged", new Class[0]);
                this.doAfterTextChanged.setAccessible(true);
            } catch (NoSuchMethodException e2) {
            }
            Class<AutoCompleteTextView> cls = AutoCompleteTextView.class;
            try {
                this.ensureImeVisible = cls.getMethod("ensureImeVisible", Boolean.TYPE);
                this.ensureImeVisible.setAccessible(true);
            } catch (NoSuchMethodException e3) {
            }
            Class<InputMethodManager> cls2 = InputMethodManager.class;
            try {
                Class[] clsArr = new Class[2];
                clsArr[0] = Integer.TYPE;
                Class[] clsArr2 = clsArr;
                clsArr2[1] = ResultReceiver.class;
                this.showSoftInputUnchecked = cls2.getMethod("showSoftInputUnchecked", clsArr2);
                this.showSoftInputUnchecked.setAccessible(true);
            } catch (NoSuchMethodException e4) {
            }
        }

        /* access modifiers changed from: package-private */
        public void doBeforeTextChanged(AutoCompleteTextView autoCompleteTextView) {
            AutoCompleteTextView autoCompleteTextView2 = autoCompleteTextView;
            if (this.doBeforeTextChanged != null) {
                try {
                    Object invoke = this.doBeforeTextChanged.invoke(autoCompleteTextView2, new Object[0]);
                } catch (Exception e) {
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void doAfterTextChanged(AutoCompleteTextView autoCompleteTextView) {
            AutoCompleteTextView autoCompleteTextView2 = autoCompleteTextView;
            if (this.doAfterTextChanged != null) {
                try {
                    Object invoke = this.doAfterTextChanged.invoke(autoCompleteTextView2, new Object[0]);
                } catch (Exception e) {
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void ensureImeVisible(AutoCompleteTextView autoCompleteTextView, boolean z) {
            AutoCompleteTextView autoCompleteTextView2 = autoCompleteTextView;
            boolean z2 = z;
            if (this.ensureImeVisible != null) {
                try {
                    Object invoke = this.ensureImeVisible.invoke(autoCompleteTextView2, Boolean.valueOf(z2));
                } catch (Exception e) {
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void showSoftInputUnchecked(InputMethodManager inputMethodManager, View view, int i) {
            InputMethodManager inputMethodManager2 = inputMethodManager;
            View view2 = view;
            int i2 = i;
            if (this.showSoftInputUnchecked != null) {
                try {
                    Object[] objArr = new Object[2];
                    objArr[0] = Integer.valueOf(i2);
                    Object[] objArr2 = objArr;
                    objArr2[1] = null;
                    Object invoke = this.showSoftInputUnchecked.invoke(inputMethodManager2, objArr2);
                    return;
                } catch (Exception e) {
                }
            }
            boolean showSoftInput = inputMethodManager2.showSoftInput(view2, i2);
        }
    }
}
