package android.support.v7.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Build;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPropertyAnimatorCompat;
import android.support.v4.view.ViewPropertyAnimatorListener;
import android.support.v7.appcompat.R;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

abstract class AbsActionBarView extends ViewGroup {
    private static final int FADE_DURATION = 200;
    protected ActionMenuPresenter mActionMenuPresenter;
    protected int mContentHeight;
    private boolean mEatingHover;
    private boolean mEatingTouch;
    protected ActionMenuView mMenuView;
    protected final Context mPopupContext;
    protected final VisibilityAnimListener mVisAnimListener;
    protected ViewPropertyAnimatorCompat mVisibilityAnim;

    AbsActionBarView(Context context) {
        this(context, null);
    }

    AbsActionBarView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    AbsActionBarView(android.content.Context r12, android.util.AttributeSet r13, int r14) {
        /*
            r11 = this;
            r0 = r11
            r1 = r12
            r2 = r13
            r3 = r14
            r5 = r0
            r6 = r1
            r7 = r2
            r8 = r3
            r5.<init>(r6, r7, r8)
            r5 = r0
            android.support.v7.widget.AbsActionBarView$VisibilityAnimListener r6 = new android.support.v7.widget.AbsActionBarView$VisibilityAnimListener
            r10 = r6
            r6 = r10
            r7 = r10
            r8 = r0
            r7.<init>()
            r5.mVisAnimListener = r6
            android.util.TypedValue r5 = new android.util.TypedValue
            r10 = r5
            r5 = r10
            r6 = r10
            r6.<init>()
            r4 = r5
            r5 = r1
            android.content.res.Resources$Theme r5 = r5.getTheme()
            int r6 = android.support.v7.appcompat.R.attr.actionBarPopupTheme
            r7 = r4
            r8 = 1
            boolean r5 = r5.resolveAttribute(r6, r7, r8)
            if (r5 == 0) goto L_0x0044
            r5 = r4
            int r5 = r5.resourceId
            if (r5 == 0) goto L_0x0044
            r5 = r0
            android.view.ContextThemeWrapper r6 = new android.view.ContextThemeWrapper
            r10 = r6
            r6 = r10
            r7 = r10
            r8 = r1
            r9 = r4
            int r9 = r9.resourceId
            r7.<init>(r8, r9)
            r5.mPopupContext = r6
        L_0x0043:
            return
        L_0x0044:
            r5 = r0
            r6 = r1
            r5.mPopupContext = r6
            goto L_0x0043
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.AbsActionBarView.<init>(android.content.Context, android.util.AttributeSet, int):void");
    }

    /* access modifiers changed from: protected */
    public void onConfigurationChanged(Configuration configuration) {
        Configuration configuration2 = configuration;
        if (Build.VERSION.SDK_INT >= 8) {
            super.onConfigurationChanged(configuration2);
        }
        TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(null, R.styleable.ActionBar, R.attr.actionBarStyle, 0);
        setContentHeight(obtainStyledAttributes.getLayoutDimension(R.styleable.ActionBar_height, 0));
        obtainStyledAttributes.recycle();
        if (this.mActionMenuPresenter != null) {
            this.mActionMenuPresenter.onConfigurationChanged(configuration2);
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        MotionEvent motionEvent2 = motionEvent;
        int actionMasked = MotionEventCompat.getActionMasked(motionEvent2);
        if (actionMasked == 0) {
            this.mEatingTouch = false;
        }
        if (!this.mEatingTouch) {
            boolean onTouchEvent = super.onTouchEvent(motionEvent2);
            if (actionMasked == 0 && !onTouchEvent) {
                this.mEatingTouch = true;
            }
        }
        if (actionMasked == 1 || actionMasked == 3) {
            this.mEatingTouch = false;
        }
        return true;
    }

    public boolean onHoverEvent(MotionEvent motionEvent) {
        MotionEvent motionEvent2 = motionEvent;
        int actionMasked = MotionEventCompat.getActionMasked(motionEvent2);
        if (actionMasked == 9) {
            this.mEatingHover = false;
        }
        if (!this.mEatingHover) {
            boolean onHoverEvent = super.onHoverEvent(motionEvent2);
            if (actionMasked == 9 && !onHoverEvent) {
                this.mEatingHover = true;
            }
        }
        if (actionMasked == 10 || actionMasked == 3) {
            this.mEatingHover = false;
        }
        return true;
    }

    public void setContentHeight(int i) {
        this.mContentHeight = i;
        requestLayout();
    }

    public int getContentHeight() {
        return this.mContentHeight;
    }

    public int getAnimatedVisibility() {
        if (this.mVisibilityAnim != null) {
            return this.mVisAnimListener.mFinalVisibility;
        }
        return getVisibility();
    }

    public ViewPropertyAnimatorCompat setupAnimatorToVisibility(int i, long j) {
        int i2 = i;
        long j2 = j;
        if (this.mVisibilityAnim != null) {
            this.mVisibilityAnim.cancel();
        }
        if (i2 == 0) {
            if (getVisibility() != 0) {
                ViewCompat.setAlpha(this, 0.0f);
            }
            ViewPropertyAnimatorCompat alpha = ViewCompat.animate(this).alpha(1.0f);
            ViewPropertyAnimatorCompat duration = alpha.setDuration(j2);
            ViewPropertyAnimatorCompat listener = alpha.setListener(this.mVisAnimListener.withFinalVisibility(alpha, i2));
            return alpha;
        }
        ViewPropertyAnimatorCompat alpha2 = ViewCompat.animate(this).alpha(0.0f);
        ViewPropertyAnimatorCompat duration2 = alpha2.setDuration(j2);
        ViewPropertyAnimatorCompat listener2 = alpha2.setListener(this.mVisAnimListener.withFinalVisibility(alpha2, i2));
        return alpha2;
    }

    public void animateToVisibility(int i) {
        setupAnimatorToVisibility(i, 200).start();
    }

    public void setVisibility(int i) {
        int i2 = i;
        if (i2 != getVisibility()) {
            if (this.mVisibilityAnim != null) {
                this.mVisibilityAnim.cancel();
            }
            super.setVisibility(i2);
        }
    }

    public boolean showOverflowMenu() {
        if (this.mActionMenuPresenter != null) {
            return this.mActionMenuPresenter.showOverflowMenu();
        }
        return false;
    }

    public void postShowOverflowMenu() {
        Runnable runnable;
        new Runnable() {
            public void run() {
                boolean showOverflowMenu = AbsActionBarView.this.showOverflowMenu();
            }
        };
        boolean post = post(runnable);
    }

    public boolean hideOverflowMenu() {
        if (this.mActionMenuPresenter != null) {
            return this.mActionMenuPresenter.hideOverflowMenu();
        }
        return false;
    }

    public boolean isOverflowMenuShowing() {
        if (this.mActionMenuPresenter != null) {
            return this.mActionMenuPresenter.isOverflowMenuShowing();
        }
        return false;
    }

    public boolean isOverflowMenuShowPending() {
        if (this.mActionMenuPresenter != null) {
            return this.mActionMenuPresenter.isOverflowMenuShowPending();
        }
        return false;
    }

    public boolean isOverflowReserved() {
        return this.mActionMenuPresenter != null && this.mActionMenuPresenter.isOverflowReserved();
    }

    public boolean canShowOverflowMenu() {
        return isOverflowReserved() && getVisibility() == 0;
    }

    public void dismissPopupMenus() {
        if (this.mActionMenuPresenter != null) {
            boolean dismissPopupMenus = this.mActionMenuPresenter.dismissPopupMenus();
        }
    }

    /* access modifiers changed from: protected */
    public int measureChildView(View view, int i, int i2, int i3) {
        View view2 = view;
        int i4 = i;
        view2.measure(View.MeasureSpec.makeMeasureSpec(i4, Integer.MIN_VALUE), i2);
        return Math.max(0, (i4 - view2.getMeasuredWidth()) - i3);
    }

    protected static int next(int i, int i2, boolean z) {
        int i3 = i;
        int i4 = i2;
        return z ? i3 - i4 : i3 + i4;
    }

    /* access modifiers changed from: protected */
    public int positionChild(View view, int i, int i2, int i3, boolean z) {
        View view2 = view;
        int i4 = i;
        boolean z2 = z;
        int measuredWidth = view2.getMeasuredWidth();
        int measuredHeight = view2.getMeasuredHeight();
        int i5 = i2 + ((i3 - measuredHeight) / 2);
        if (z2) {
            view2.layout(i4 - measuredWidth, i5, i4, i5 + measuredHeight);
        } else {
            view2.layout(i4, i5, i4 + measuredWidth, i5 + measuredHeight);
        }
        return z2 ? -measuredWidth : measuredWidth;
    }

    protected class VisibilityAnimListener implements ViewPropertyAnimatorListener {
        private boolean mCanceled = false;
        int mFinalVisibility;

        protected VisibilityAnimListener() {
        }

        public VisibilityAnimListener withFinalVisibility(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, int i) {
            AbsActionBarView.this.mVisibilityAnim = viewPropertyAnimatorCompat;
            this.mFinalVisibility = i;
            return this;
        }

        public void onAnimationStart(View view) {
            AbsActionBarView.super.setVisibility(0);
            this.mCanceled = false;
        }

        public void onAnimationEnd(View view) {
            if (!this.mCanceled) {
                AbsActionBarView.this.mVisibilityAnim = null;
                AbsActionBarView.super.setVisibility(this.mFinalVisibility);
            }
        }

        public void onAnimationCancel(View view) {
            this.mCanceled = true;
        }
    }
}
