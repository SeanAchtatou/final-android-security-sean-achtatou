package android.support.v7.widget;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public abstract class SimpleItemAnimator extends RecyclerView.ItemAnimator {
    private static final boolean DEBUG = false;
    private static final String TAG = "SimpleItemAnimator";
    boolean mSupportsChangeAnimations = true;

    public abstract boolean animateAdd(RecyclerView.ViewHolder viewHolder);

    public abstract boolean animateChange(RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder viewHolder2, int i, int i2, int i3, int i4);

    public abstract boolean animateMove(RecyclerView.ViewHolder viewHolder, int i, int i2, int i3, int i4);

    public abstract boolean animateRemove(RecyclerView.ViewHolder viewHolder);

    public boolean getSupportsChangeAnimations() {
        return this.mSupportsChangeAnimations;
    }

    public void setSupportsChangeAnimations(boolean z) {
        this.mSupportsChangeAnimations = z;
    }

    public boolean canReuseUpdatedViewHolder(RecyclerView.ViewHolder viewHolder) {
        return !this.mSupportsChangeAnimations || viewHolder.isInvalid();
    }

    public boolean animateDisappearance(@NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo, @Nullable RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo2) {
        int i;
        RecyclerView.ViewHolder viewHolder2 = viewHolder;
        RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo3 = itemHolderInfo;
        RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo4 = itemHolderInfo2;
        int i2 = itemHolderInfo3.left;
        int i3 = itemHolderInfo3.top;
        View view = viewHolder2.itemView;
        int left = itemHolderInfo4 == null ? view.getLeft() : itemHolderInfo4.left;
        if (itemHolderInfo4 == null) {
            i = view.getTop();
        } else {
            i = itemHolderInfo4.top;
        }
        int i4 = i;
        if (viewHolder2.isRemoved() || (i2 == left && i3 == i4)) {
            return animateRemove(viewHolder2);
        }
        view.layout(left, i4, left + view.getWidth(), i4 + view.getHeight());
        return animateMove(viewHolder2, i2, i3, left, i4);
    }

    public boolean animateAppearance(@NonNull RecyclerView.ViewHolder viewHolder, @Nullable RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo, @NonNull RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo2) {
        RecyclerView.ViewHolder viewHolder2 = viewHolder;
        RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo3 = itemHolderInfo;
        RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo4 = itemHolderInfo2;
        if (itemHolderInfo3 == null || (itemHolderInfo3.left == itemHolderInfo4.left && itemHolderInfo3.top == itemHolderInfo4.top)) {
            return animateAdd(viewHolder2);
        }
        return animateMove(viewHolder2, itemHolderInfo3.left, itemHolderInfo3.top, itemHolderInfo4.left, itemHolderInfo4.top);
    }

    public boolean animatePersistence(@NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo, @NonNull RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo2) {
        RecyclerView.ViewHolder viewHolder2 = viewHolder;
        RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo3 = itemHolderInfo;
        RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo4 = itemHolderInfo2;
        if (itemHolderInfo3.left != itemHolderInfo4.left || itemHolderInfo3.top != itemHolderInfo4.top) {
            return animateMove(viewHolder2, itemHolderInfo3.left, itemHolderInfo3.top, itemHolderInfo4.left, itemHolderInfo4.top);
        }
        dispatchMoveFinished(viewHolder2);
        return false;
    }

    public boolean animateChange(@NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder viewHolder2, @NonNull RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo, @NonNull RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo2) {
        int i;
        int i2;
        RecyclerView.ViewHolder viewHolder3 = viewHolder;
        RecyclerView.ViewHolder viewHolder4 = viewHolder2;
        RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo3 = itemHolderInfo;
        RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo4 = itemHolderInfo2;
        int i3 = itemHolderInfo3.left;
        int i4 = itemHolderInfo3.top;
        if (viewHolder4.shouldIgnore()) {
            i = itemHolderInfo3.left;
            i2 = itemHolderInfo3.top;
        } else {
            i = itemHolderInfo4.left;
            i2 = itemHolderInfo4.top;
        }
        return animateChange(viewHolder3, viewHolder4, i3, i4, i, i2);
    }

    public final void dispatchRemoveFinished(RecyclerView.ViewHolder viewHolder) {
        RecyclerView.ViewHolder viewHolder2 = viewHolder;
        onRemoveFinished(viewHolder2);
        dispatchAnimationFinished(viewHolder2);
    }

    public final void dispatchMoveFinished(RecyclerView.ViewHolder viewHolder) {
        RecyclerView.ViewHolder viewHolder2 = viewHolder;
        onMoveFinished(viewHolder2);
        dispatchAnimationFinished(viewHolder2);
    }

    public final void dispatchAddFinished(RecyclerView.ViewHolder viewHolder) {
        RecyclerView.ViewHolder viewHolder2 = viewHolder;
        onAddFinished(viewHolder2);
        dispatchAnimationFinished(viewHolder2);
    }

    public final void dispatchChangeFinished(RecyclerView.ViewHolder viewHolder, boolean z) {
        RecyclerView.ViewHolder viewHolder2 = viewHolder;
        onChangeFinished(viewHolder2, z);
        dispatchAnimationFinished(viewHolder2);
    }

    public final void dispatchRemoveStarting(RecyclerView.ViewHolder viewHolder) {
        onRemoveStarting(viewHolder);
    }

    public final void dispatchMoveStarting(RecyclerView.ViewHolder viewHolder) {
        onMoveStarting(viewHolder);
    }

    public final void dispatchAddStarting(RecyclerView.ViewHolder viewHolder) {
        onAddStarting(viewHolder);
    }

    public final void dispatchChangeStarting(RecyclerView.ViewHolder viewHolder, boolean z) {
        onChangeStarting(viewHolder, z);
    }

    public void onRemoveStarting(RecyclerView.ViewHolder viewHolder) {
    }

    public void onRemoveFinished(RecyclerView.ViewHolder viewHolder) {
    }

    public void onAddStarting(RecyclerView.ViewHolder viewHolder) {
    }

    public void onAddFinished(RecyclerView.ViewHolder viewHolder) {
    }

    public void onMoveStarting(RecyclerView.ViewHolder viewHolder) {
    }

    public void onMoveFinished(RecyclerView.ViewHolder viewHolder) {
    }

    public void onChangeStarting(RecyclerView.ViewHolder viewHolder, boolean z) {
    }

    public void onChangeFinished(RecyclerView.ViewHolder viewHolder, boolean z) {
    }
}
