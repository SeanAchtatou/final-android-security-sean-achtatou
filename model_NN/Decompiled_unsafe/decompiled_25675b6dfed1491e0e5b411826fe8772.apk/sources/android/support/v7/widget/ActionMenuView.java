package android.support.v7.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.support.v7.view.menu.ActionMenuItemView;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuItemImpl;
import android.support.v7.view.menu.MenuPresenter;
import android.support.v7.view.menu.MenuView;
import android.support.v7.widget.LinearLayoutCompat;
import android.util.AttributeSet;
import android.view.ContextThemeWrapper;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewDebug;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;

public class ActionMenuView extends LinearLayoutCompat implements MenuBuilder.ItemInvoker, MenuView {
    static final int GENERATED_ITEM_PADDING = 4;
    static final int MIN_CELL_SIZE = 56;
    private static final String TAG = "ActionMenuView";
    private MenuPresenter.Callback mActionMenuPresenterCallback;
    private boolean mFormatItems;
    private int mFormatItemsWidth;
    private int mGeneratedItemPadding;
    private MenuBuilder mMenu;
    /* access modifiers changed from: private */
    public MenuBuilder.Callback mMenuBuilderCallback;
    private int mMinCellSize;
    /* access modifiers changed from: private */
    public OnMenuItemClickListener mOnMenuItemClickListener;
    private Context mPopupContext;
    private int mPopupTheme;
    private ActionMenuPresenter mPresenter;
    private boolean mReserveOverflow;

    public interface ActionMenuChildView {
        boolean needsDividerAfter();

        boolean needsDividerBefore();
    }

    public interface OnMenuItemClickListener {
        boolean onMenuItemClick(MenuItem menuItem);
    }

    public ActionMenuView(Context context) {
        this(context, null);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public ActionMenuView(android.content.Context r8, android.util.AttributeSet r9) {
        /*
            r7 = this;
            r0 = r7
            r1 = r8
            r2 = r9
            r4 = r0
            r5 = r1
            r6 = r2
            r4.<init>(r5, r6)
            r4 = r0
            r5 = 0
            r4.setBaselineAligned(r5)
            r4 = r1
            android.content.res.Resources r4 = r4.getResources()
            android.util.DisplayMetrics r4 = r4.getDisplayMetrics()
            float r4 = r4.density
            r3 = r4
            r4 = r0
            r5 = 1113587712(0x42600000, float:56.0)
            r6 = r3
            float r5 = r5 * r6
            int r5 = (int) r5
            r4.mMinCellSize = r5
            r4 = r0
            r5 = 1082130432(0x40800000, float:4.0)
            r6 = r3
            float r5 = r5 * r6
            int r5 = (int) r5
            r4.mGeneratedItemPadding = r5
            r4 = r0
            r5 = r1
            r4.mPopupContext = r5
            r4 = r0
            r5 = 0
            r4.mPopupTheme = r5
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.ActionMenuView.<init>(android.content.Context, android.util.AttributeSet):void");
    }

    public void setPopupTheme(@StyleRes int i) {
        Context context;
        int i2 = i;
        if (this.mPopupTheme != i2) {
            this.mPopupTheme = i2;
            if (i2 == 0) {
                this.mPopupContext = getContext();
                return;
            }
            new ContextThemeWrapper(getContext(), i2);
            this.mPopupContext = context;
        }
    }

    public int getPopupTheme() {
        return this.mPopupTheme;
    }

    public void setPresenter(ActionMenuPresenter actionMenuPresenter) {
        this.mPresenter = actionMenuPresenter;
        this.mPresenter.setMenuView(this);
    }

    public void onConfigurationChanged(Configuration configuration) {
        Configuration configuration2 = configuration;
        if (Build.VERSION.SDK_INT >= 8) {
            super.onConfigurationChanged(configuration2);
        }
        if (this.mPresenter != null) {
            this.mPresenter.updateMenuView(false);
            if (this.mPresenter.isOverflowMenuShowing()) {
                boolean hideOverflowMenu = this.mPresenter.hideOverflowMenu();
                boolean showOverflowMenu = this.mPresenter.showOverflowMenu();
            }
        }
    }

    public void setOnMenuItemClickListener(OnMenuItemClickListener onMenuItemClickListener) {
        this.mOnMenuItemClickListener = onMenuItemClickListener;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int i3 = i;
        int i4 = i2;
        boolean z = this.mFormatItems;
        this.mFormatItems = View.MeasureSpec.getMode(i3) == 1073741824;
        if (z != this.mFormatItems) {
            this.mFormatItemsWidth = 0;
        }
        int size = View.MeasureSpec.getSize(i3);
        if (!(!this.mFormatItems || this.mMenu == null || size == this.mFormatItemsWidth)) {
            this.mFormatItemsWidth = size;
            this.mMenu.onItemsChanged(true);
        }
        int childCount = getChildCount();
        if (!this.mFormatItems || childCount <= 0) {
            for (int i5 = 0; i5 < childCount; i5++) {
                LayoutParams layoutParams = (LayoutParams) getChildAt(i5).getLayoutParams();
                layoutParams.rightMargin = 0;
                layoutParams.leftMargin = 0;
            }
            super.onMeasure(i3, i4);
            return;
        }
        onMeasureExactFormat(i3, i4);
    }

    private void onMeasureExactFormat(int i, int i2) {
        boolean z;
        int i3 = i2;
        int mode = View.MeasureSpec.getMode(i3);
        int size = View.MeasureSpec.getSize(i);
        int size2 = View.MeasureSpec.getSize(i3);
        int paddingLeft = getPaddingLeft() + getPaddingRight();
        int paddingTop = getPaddingTop() + getPaddingBottom();
        int childMeasureSpec = getChildMeasureSpec(i3, paddingTop, -2);
        int i4 = size - paddingLeft;
        int i5 = i4 / this.mMinCellSize;
        int i6 = i4 % this.mMinCellSize;
        if (i5 == 0) {
            setMeasuredDimension(i4, 0);
            return;
        }
        int i7 = this.mMinCellSize + (i6 / i5);
        int i8 = i5;
        int i9 = 0;
        int i10 = 0;
        int i11 = 0;
        int i12 = 0;
        boolean z2 = false;
        long j = 0;
        int childCount = getChildCount();
        for (int i13 = 0; i13 < childCount; i13++) {
            View childAt = getChildAt(i13);
            if (childAt.getVisibility() != 8) {
                boolean z3 = childAt instanceof ActionMenuItemView;
                i12++;
                if (z3) {
                    childAt.setPadding(this.mGeneratedItemPadding, 0, this.mGeneratedItemPadding, 0);
                }
                LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                layoutParams.expanded = false;
                layoutParams.extraPixels = 0;
                layoutParams.cellsUsed = 0;
                layoutParams.expandable = false;
                layoutParams.leftMargin = 0;
                layoutParams.rightMargin = 0;
                layoutParams.preventEdgeOffset = z3 && ((ActionMenuItemView) childAt).hasText();
                int measureChildForCells = measureChildForCells(childAt, i7, layoutParams.isOverflowButton ? 1 : i8, childMeasureSpec, paddingTop);
                i10 = Math.max(i10, measureChildForCells);
                if (layoutParams.expandable) {
                    i11++;
                }
                if (layoutParams.isOverflowButton) {
                    z2 = true;
                }
                i8 -= measureChildForCells;
                i9 = Math.max(i9, childAt.getMeasuredHeight());
                if (measureChildForCells == 1) {
                    j |= (long) (1 << i13);
                }
            }
        }
        boolean z4 = z2 && i12 == 2;
        boolean z5 = false;
        while (true) {
            z = z5;
            if (i11 <= 0 || i8 <= 0) {
                break;
            }
            int i14 = Integer.MAX_VALUE;
            long j2 = 0;
            int i15 = 0;
            for (int i16 = 0; i16 < childCount; i16++) {
                LayoutParams layoutParams2 = (LayoutParams) getChildAt(i16).getLayoutParams();
                if (layoutParams2.expandable) {
                    if (layoutParams2.cellsUsed < i14) {
                        i14 = layoutParams2.cellsUsed;
                        j2 = (long) (1 << i16);
                        i15 = 1;
                    } else if (layoutParams2.cellsUsed == i14) {
                        j2 |= (long) (1 << i16);
                        i15++;
                    }
                }
            }
            j |= j2;
            if (i15 > i8) {
                break;
            }
            int i17 = i14 + 1;
            for (int i18 = 0; i18 < childCount; i18++) {
                View childAt2 = getChildAt(i18);
                LayoutParams layoutParams3 = (LayoutParams) childAt2.getLayoutParams();
                if ((j2 & ((long) (1 << i18))) != 0) {
                    if (z4 && layoutParams3.preventEdgeOffset && i8 == 1) {
                        childAt2.setPadding(this.mGeneratedItemPadding + i7, 0, this.mGeneratedItemPadding, 0);
                    }
                    layoutParams3.cellsUsed++;
                    layoutParams3.expanded = true;
                    i8--;
                } else if (layoutParams3.cellsUsed == i17) {
                    j |= (long) (1 << i18);
                }
            }
            z5 = true;
        }
        boolean z6 = !z2 && i12 == 1;
        if (i8 > 0 && j != 0 && (i8 < i12 - 1 || z6 || i10 > 1)) {
            float bitCount = (float) Long.bitCount(j);
            if (!z6) {
                if ((j & 1) != 0 && !((LayoutParams) getChildAt(0).getLayoutParams()).preventEdgeOffset) {
                    bitCount -= 0.5f;
                }
                if ((j & ((long) (1 << (childCount - 1)))) != 0 && !((LayoutParams) getChildAt(childCount - 1).getLayoutParams()).preventEdgeOffset) {
                    bitCount -= 0.5f;
                }
            }
            int i19 = bitCount > 0.0f ? (int) (((float) (i8 * i7)) / bitCount) : 0;
            for (int i20 = 0; i20 < childCount; i20++) {
                if ((j & ((long) (1 << i20))) != 0) {
                    View childAt3 = getChildAt(i20);
                    LayoutParams layoutParams4 = (LayoutParams) childAt3.getLayoutParams();
                    if (childAt3 instanceof ActionMenuItemView) {
                        layoutParams4.extraPixels = i19;
                        layoutParams4.expanded = true;
                        if (i20 == 0 && !layoutParams4.preventEdgeOffset) {
                            layoutParams4.leftMargin = (-i19) / 2;
                        }
                        z = true;
                    } else if (layoutParams4.isOverflowButton) {
                        layoutParams4.extraPixels = i19;
                        layoutParams4.expanded = true;
                        layoutParams4.rightMargin = (-i19) / 2;
                        z = true;
                    } else {
                        if (i20 != 0) {
                            layoutParams4.leftMargin = i19 / 2;
                        }
                        if (i20 != childCount - 1) {
                            layoutParams4.rightMargin = i19 / 2;
                        }
                    }
                }
            }
        }
        if (z) {
            for (int i21 = 0; i21 < childCount; i21++) {
                View childAt4 = getChildAt(i21);
                LayoutParams layoutParams5 = (LayoutParams) childAt4.getLayoutParams();
                if (layoutParams5.expanded) {
                    childAt4.measure(View.MeasureSpec.makeMeasureSpec((layoutParams5.cellsUsed * i7) + layoutParams5.extraPixels, 1073741824), childMeasureSpec);
                }
            }
        }
        if (mode != 1073741824) {
            size2 = i9;
        }
        setMeasuredDimension(i4, size2);
    }

    static int measureChildForCells(View view, int i, int i2, int i3, int i4) {
        View view2 = view;
        int i5 = i;
        int i6 = i2;
        int i7 = i3;
        LayoutParams layoutParams = (LayoutParams) view2.getLayoutParams();
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(View.MeasureSpec.getSize(i7) - i4, View.MeasureSpec.getMode(i7));
        ActionMenuItemView actionMenuItemView = view2 instanceof ActionMenuItemView ? (ActionMenuItemView) view2 : null;
        boolean z = actionMenuItemView != null && actionMenuItemView.hasText();
        int i8 = 0;
        if (i6 > 0 && (!z || i6 >= 2)) {
            view2.measure(View.MeasureSpec.makeMeasureSpec(i5 * i6, Integer.MIN_VALUE), makeMeasureSpec);
            int measuredWidth = view2.getMeasuredWidth();
            i8 = measuredWidth / i5;
            if (measuredWidth % i5 != 0) {
                i8++;
            }
            if (z && i8 < 2) {
                i8 = 2;
            }
        }
        layoutParams.expandable = !layoutParams.isOverflowButton && z;
        layoutParams.cellsUsed = i8;
        view2.measure(View.MeasureSpec.makeMeasureSpec(i8 * i5, 1073741824), makeMeasureSpec);
        return i8;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int width;
        int i5;
        boolean z2 = z;
        int i6 = i;
        int i7 = i2;
        int i8 = i3;
        int i9 = i4;
        if (!this.mFormatItems) {
            super.onLayout(z2, i6, i7, i8, i9);
            return;
        }
        int childCount = getChildCount();
        int i10 = (i9 - i7) / 2;
        int dividerWidth = getDividerWidth();
        int i11 = 0;
        int i12 = 0;
        int paddingRight = ((i8 - i6) - getPaddingRight()) - getPaddingLeft();
        boolean z3 = false;
        boolean isLayoutRtl = ViewUtils.isLayoutRtl(this);
        for (int i13 = 0; i13 < childCount; i13++) {
            View childAt = getChildAt(i13);
            if (childAt.getVisibility() != 8) {
                LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                if (layoutParams.isOverflowButton) {
                    int measuredWidth = childAt.getMeasuredWidth();
                    if (hasSupportDividerBeforeChildAt(i13)) {
                        measuredWidth += dividerWidth;
                    }
                    int measuredHeight = childAt.getMeasuredHeight();
                    if (isLayoutRtl) {
                        i5 = getPaddingLeft() + layoutParams.leftMargin;
                        width = i5 + measuredWidth;
                    } else {
                        width = (getWidth() - getPaddingRight()) - layoutParams.rightMargin;
                        i5 = width - measuredWidth;
                    }
                    int i14 = i10 - (measuredHeight / 2);
                    childAt.layout(i5, i14, width, i14 + measuredHeight);
                    paddingRight -= measuredWidth;
                    z3 = true;
                } else {
                    int measuredWidth2 = childAt.getMeasuredWidth() + layoutParams.leftMargin + layoutParams.rightMargin;
                    i11 += measuredWidth2;
                    paddingRight -= measuredWidth2;
                    if (hasSupportDividerBeforeChildAt(i13)) {
                        i11 += dividerWidth;
                    }
                    i12++;
                }
            }
        }
        if (childCount != 1 || z3) {
            int i15 = i12 - (z3 ? 0 : 1);
            int max = Math.max(0, i15 > 0 ? paddingRight / i15 : 0);
            if (isLayoutRtl) {
                int width2 = getWidth() - getPaddingRight();
                for (int i16 = 0; i16 < childCount; i16++) {
                    View childAt2 = getChildAt(i16);
                    LayoutParams layoutParams2 = (LayoutParams) childAt2.getLayoutParams();
                    if (childAt2.getVisibility() != 8 && !layoutParams2.isOverflowButton) {
                        int i17 = width2 - layoutParams2.rightMargin;
                        int measuredWidth3 = childAt2.getMeasuredWidth();
                        int measuredHeight2 = childAt2.getMeasuredHeight();
                        int i18 = i10 - (measuredHeight2 / 2);
                        childAt2.layout(i17 - measuredWidth3, i18, i17, i18 + measuredHeight2);
                        width2 = i17 - ((measuredWidth3 + layoutParams2.leftMargin) + max);
                    }
                }
                return;
            }
            int paddingLeft = getPaddingLeft();
            for (int i19 = 0; i19 < childCount; i19++) {
                View childAt3 = getChildAt(i19);
                LayoutParams layoutParams3 = (LayoutParams) childAt3.getLayoutParams();
                if (childAt3.getVisibility() != 8 && !layoutParams3.isOverflowButton) {
                    int i20 = paddingLeft + layoutParams3.leftMargin;
                    int measuredWidth4 = childAt3.getMeasuredWidth();
                    int measuredHeight3 = childAt3.getMeasuredHeight();
                    int i21 = i10 - (measuredHeight3 / 2);
                    childAt3.layout(i20, i21, i20 + measuredWidth4, i21 + measuredHeight3);
                    paddingLeft = i20 + measuredWidth4 + layoutParams3.rightMargin + max;
                }
            }
            return;
        }
        View childAt4 = getChildAt(0);
        int measuredWidth5 = childAt4.getMeasuredWidth();
        int measuredHeight4 = childAt4.getMeasuredHeight();
        int i22 = ((i8 - i6) / 2) - (measuredWidth5 / 2);
        int i23 = i10 - (measuredHeight4 / 2);
        childAt4.layout(i22, i23, i22 + measuredWidth5, i23 + measuredHeight4);
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        dismissPopupMenus();
    }

    public void setOverflowIcon(@Nullable Drawable drawable) {
        Menu menu = getMenu();
        this.mPresenter.setOverflowIcon(drawable);
    }

    @Nullable
    public Drawable getOverflowIcon() {
        Menu menu = getMenu();
        return this.mPresenter.getOverflowIcon();
    }

    public boolean isOverflowReserved() {
        return this.mReserveOverflow;
    }

    public void setOverflowReserved(boolean z) {
        this.mReserveOverflow = z;
    }

    /* access modifiers changed from: protected */
    public LayoutParams generateDefaultLayoutParams() {
        LayoutParams layoutParams;
        new LayoutParams(-2, -2);
        LayoutParams layoutParams2 = layoutParams;
        layoutParams2.gravity = 16;
        return layoutParams2;
    }

    public LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        LayoutParams layoutParams;
        new LayoutParams(getContext(), attributeSet);
        return layoutParams;
    }

    /* access modifiers changed from: protected */
    public LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        LayoutParams layoutParams2;
        LayoutParams layoutParams3;
        LayoutParams layoutParams4;
        ViewGroup.LayoutParams layoutParams5 = layoutParams;
        if (layoutParams5 == null) {
            return generateDefaultLayoutParams();
        }
        if (layoutParams5 instanceof LayoutParams) {
            layoutParams3 = layoutParams4;
            new LayoutParams((LayoutParams) layoutParams5);
        } else {
            layoutParams3 = layoutParams2;
            new LayoutParams(layoutParams5);
        }
        LayoutParams layoutParams6 = layoutParams3;
        if (layoutParams6.gravity <= 0) {
            layoutParams6.gravity = 16;
        }
        return layoutParams6;
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        ViewGroup.LayoutParams layoutParams2 = layoutParams;
        return layoutParams2 != null && (layoutParams2 instanceof LayoutParams);
    }

    public LayoutParams generateOverflowButtonLayoutParams() {
        LayoutParams generateDefaultLayoutParams = generateDefaultLayoutParams();
        generateDefaultLayoutParams.isOverflowButton = true;
        return generateDefaultLayoutParams;
    }

    public boolean invokeItem(MenuItemImpl menuItemImpl) {
        return this.mMenu.performItemAction(menuItemImpl, 0);
    }

    public int getWindowAnimations() {
        return 0;
    }

    public void initialize(MenuBuilder menuBuilder) {
        this.mMenu = menuBuilder;
    }

    public Menu getMenu() {
        MenuBuilder menuBuilder;
        MenuBuilder.Callback callback;
        ActionMenuPresenter actionMenuPresenter;
        MenuPresenter.Callback callback2;
        MenuPresenter.Callback callback3;
        if (this.mMenu == null) {
            Context context = getContext();
            new MenuBuilder(context);
            this.mMenu = menuBuilder;
            new MenuBuilderCallback();
            this.mMenu.setCallback(callback);
            new ActionMenuPresenter(context);
            this.mPresenter = actionMenuPresenter;
            this.mPresenter.setReserveOverflow(true);
            ActionMenuPresenter actionMenuPresenter2 = this.mPresenter;
            if (this.mActionMenuPresenterCallback != null) {
                callback3 = this.mActionMenuPresenterCallback;
            } else {
                callback3 = callback2;
                new ActionMenuPresenterCallback();
            }
            actionMenuPresenter2.setCallback(callback3);
            this.mMenu.addMenuPresenter(this.mPresenter, this.mPopupContext);
            this.mPresenter.setMenuView(this);
        }
        return this.mMenu;
    }

    public void setMenuCallbacks(MenuPresenter.Callback callback, MenuBuilder.Callback callback2) {
        this.mActionMenuPresenterCallback = callback;
        this.mMenuBuilderCallback = callback2;
    }

    public MenuBuilder peekMenu() {
        return this.mMenu;
    }

    public boolean showOverflowMenu() {
        return this.mPresenter != null && this.mPresenter.showOverflowMenu();
    }

    public boolean hideOverflowMenu() {
        return this.mPresenter != null && this.mPresenter.hideOverflowMenu();
    }

    public boolean isOverflowMenuShowing() {
        return this.mPresenter != null && this.mPresenter.isOverflowMenuShowing();
    }

    public boolean isOverflowMenuShowPending() {
        return this.mPresenter != null && this.mPresenter.isOverflowMenuShowPending();
    }

    public void dismissPopupMenus() {
        if (this.mPresenter != null) {
            boolean dismissPopupMenus = this.mPresenter.dismissPopupMenus();
        }
    }

    /* access modifiers changed from: protected */
    public boolean hasSupportDividerBeforeChildAt(int i) {
        int i2 = i;
        if (i2 == 0) {
            return false;
        }
        View childAt = getChildAt(i2 - 1);
        View childAt2 = getChildAt(i2);
        boolean z = false;
        if (i2 < getChildCount() && (childAt instanceof ActionMenuChildView)) {
            z = false | ((ActionMenuChildView) childAt).needsDividerAfter();
        }
        if (i2 > 0 && (childAt2 instanceof ActionMenuChildView)) {
            z |= ((ActionMenuChildView) childAt2).needsDividerBefore();
        }
        return z;
    }

    public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        return false;
    }

    public void setExpandedActionViewsExclusive(boolean z) {
        this.mPresenter.setExpandedActionViewsExclusive(z);
    }

    private class MenuBuilderCallback implements MenuBuilder.Callback {
        private MenuBuilderCallback() {
        }

        public boolean onMenuItemSelected(MenuBuilder menuBuilder, MenuItem menuItem) {
            return ActionMenuView.this.mOnMenuItemClickListener != null && ActionMenuView.this.mOnMenuItemClickListener.onMenuItemClick(menuItem);
        }

        public void onMenuModeChange(MenuBuilder menuBuilder) {
            MenuBuilder menuBuilder2 = menuBuilder;
            if (ActionMenuView.this.mMenuBuilderCallback != null) {
                ActionMenuView.this.mMenuBuilderCallback.onMenuModeChange(menuBuilder2);
            }
        }
    }

    private class ActionMenuPresenterCallback implements MenuPresenter.Callback {
        private ActionMenuPresenterCallback() {
        }

        public void onCloseMenu(MenuBuilder menuBuilder, boolean z) {
        }

        public boolean onOpenSubMenu(MenuBuilder menuBuilder) {
            return false;
        }
    }

    public static class LayoutParams extends LinearLayoutCompat.LayoutParams {
        @ViewDebug.ExportedProperty
        public int cellsUsed;
        @ViewDebug.ExportedProperty
        public boolean expandable;
        boolean expanded;
        @ViewDebug.ExportedProperty
        public int extraPixels;
        @ViewDebug.ExportedProperty
        public boolean isOverflowButton;
        @ViewDebug.ExportedProperty
        public boolean preventEdgeOffset;

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        public LayoutParams(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
        }

        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public LayoutParams(android.support.v7.widget.ActionMenuView.LayoutParams r5) {
            /*
                r4 = this;
                r0 = r4
                r1 = r5
                r2 = r0
                r3 = r1
                r2.<init>(r3)
                r2 = r0
                r3 = r1
                boolean r3 = r3.isOverflowButton
                r2.isOverflowButton = r3
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.ActionMenuView.LayoutParams.<init>(android.support.v7.widget.ActionMenuView$LayoutParams):void");
        }

        public LayoutParams(int i, int i2) {
            super(i, i2);
            this.isOverflowButton = false;
        }

        LayoutParams(int i, int i2, boolean z) {
            super(i, i2);
            this.isOverflowButton = z;
        }
    }
}
