package android.support.v7.widget;

import android.support.v4.animation.AnimatorCompatHelper;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPropertyAnimatorCompat;
import android.support.v4.view.ViewPropertyAnimatorListener;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class DefaultItemAnimator extends SimpleItemAnimator {
    private static final boolean DEBUG = false;
    /* access modifiers changed from: private */
    public ArrayList<RecyclerView.ViewHolder> mAddAnimations;
    /* access modifiers changed from: private */
    public ArrayList<ArrayList<RecyclerView.ViewHolder>> mAdditionsList;
    /* access modifiers changed from: private */
    public ArrayList<RecyclerView.ViewHolder> mChangeAnimations;
    /* access modifiers changed from: private */
    public ArrayList<ArrayList<ChangeInfo>> mChangesList;
    /* access modifiers changed from: private */
    public ArrayList<RecyclerView.ViewHolder> mMoveAnimations;
    /* access modifiers changed from: private */
    public ArrayList<ArrayList<MoveInfo>> mMovesList;
    private ArrayList<RecyclerView.ViewHolder> mPendingAdditions;
    private ArrayList<ChangeInfo> mPendingChanges;
    private ArrayList<MoveInfo> mPendingMoves;
    private ArrayList<RecyclerView.ViewHolder> mPendingRemovals;
    /* access modifiers changed from: private */
    public ArrayList<RecyclerView.ViewHolder> mRemoveAnimations;

    public DefaultItemAnimator() {
        ArrayList<RecyclerView.ViewHolder> arrayList;
        ArrayList<RecyclerView.ViewHolder> arrayList2;
        ArrayList<MoveInfo> arrayList3;
        ArrayList<ChangeInfo> arrayList4;
        ArrayList<ArrayList<RecyclerView.ViewHolder>> arrayList5;
        ArrayList<ArrayList<MoveInfo>> arrayList6;
        ArrayList<ArrayList<ChangeInfo>> arrayList7;
        ArrayList<RecyclerView.ViewHolder> arrayList8;
        ArrayList<RecyclerView.ViewHolder> arrayList9;
        ArrayList<RecyclerView.ViewHolder> arrayList10;
        ArrayList<RecyclerView.ViewHolder> arrayList11;
        new ArrayList<>();
        this.mPendingRemovals = arrayList;
        new ArrayList<>();
        this.mPendingAdditions = arrayList2;
        new ArrayList<>();
        this.mPendingMoves = arrayList3;
        new ArrayList<>();
        this.mPendingChanges = arrayList4;
        new ArrayList<>();
        this.mAdditionsList = arrayList5;
        new ArrayList<>();
        this.mMovesList = arrayList6;
        new ArrayList<>();
        this.mChangesList = arrayList7;
        new ArrayList<>();
        this.mAddAnimations = arrayList8;
        new ArrayList<>();
        this.mMoveAnimations = arrayList9;
        new ArrayList<>();
        this.mRemoveAnimations = arrayList10;
        new ArrayList<>();
        this.mChangeAnimations = arrayList11;
    }

    private static class MoveInfo {
        public int fromX;
        public int fromY;
        public RecyclerView.ViewHolder holder;
        public int toX;
        public int toY;

        private MoveInfo(RecyclerView.ViewHolder viewHolder, int i, int i2, int i3, int i4) {
            this.holder = viewHolder;
            this.fromX = i;
            this.fromY = i2;
            this.toX = i3;
            this.toY = i4;
        }
    }

    private static class ChangeInfo {
        public int fromX;
        public int fromY;
        public RecyclerView.ViewHolder newHolder;
        public RecyclerView.ViewHolder oldHolder;
        public int toX;
        public int toY;

        private ChangeInfo(RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder viewHolder2) {
            this.oldHolder = viewHolder;
            this.newHolder = viewHolder2;
        }

        private ChangeInfo(RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder viewHolder2, int i, int i2, int i3, int i4) {
            this(viewHolder, viewHolder2);
            this.fromX = i;
            this.fromY = i2;
            this.toX = i3;
            this.toY = i4;
        }

        public String toString() {
            StringBuilder sb;
            new StringBuilder();
            return sb.append("ChangeInfo{oldHolder=").append(this.oldHolder).append(", newHolder=").append(this.newHolder).append(", fromX=").append(this.fromX).append(", fromY=").append(this.fromY).append(", toX=").append(this.toX).append(", toY=").append(this.toY).append('}').toString();
        }
    }

    public void runPendingAnimations() {
        ArrayList arrayList;
        Runnable runnable;
        ArrayList arrayList2;
        Runnable runnable2;
        ArrayList arrayList3;
        Runnable runnable3;
        boolean z = !this.mPendingRemovals.isEmpty();
        boolean z2 = !this.mPendingMoves.isEmpty();
        boolean z3 = !this.mPendingChanges.isEmpty();
        boolean z4 = !this.mPendingAdditions.isEmpty();
        if (z || z2 || z4 || z3) {
            Iterator<RecyclerView.ViewHolder> it = this.mPendingRemovals.iterator();
            while (it.hasNext()) {
                animateRemoveImpl(it.next());
            }
            this.mPendingRemovals.clear();
            if (z2) {
                new ArrayList();
                ArrayList arrayList4 = arrayList3;
                boolean addAll = arrayList4.addAll(this.mPendingMoves);
                boolean add = this.mMovesList.add(arrayList4);
                this.mPendingMoves.clear();
                final ArrayList arrayList5 = arrayList4;
                new Runnable() {
                    public void run() {
                        Iterator it = arrayList5.iterator();
                        while (it.hasNext()) {
                            MoveInfo moveInfo = (MoveInfo) it.next();
                            DefaultItemAnimator.this.animateMoveImpl(moveInfo.holder, moveInfo.fromX, moveInfo.fromY, moveInfo.toX, moveInfo.toY);
                        }
                        arrayList5.clear();
                        boolean remove = DefaultItemAnimator.this.mMovesList.remove(arrayList5);
                    }
                };
                Runnable runnable4 = runnable3;
                if (z) {
                    ViewCompat.postOnAnimationDelayed(((MoveInfo) arrayList4.get(0)).holder.itemView, runnable4, getRemoveDuration());
                } else {
                    runnable4.run();
                }
            }
            if (z3) {
                new ArrayList();
                ArrayList arrayList6 = arrayList2;
                boolean addAll2 = arrayList6.addAll(this.mPendingChanges);
                boolean add2 = this.mChangesList.add(arrayList6);
                this.mPendingChanges.clear();
                final ArrayList arrayList7 = arrayList6;
                new Runnable() {
                    public void run() {
                        Iterator it = arrayList7.iterator();
                        while (it.hasNext()) {
                            DefaultItemAnimator.this.animateChangeImpl((ChangeInfo) it.next());
                        }
                        arrayList7.clear();
                        boolean remove = DefaultItemAnimator.this.mChangesList.remove(arrayList7);
                    }
                };
                Runnable runnable5 = runnable2;
                if (z) {
                    ViewCompat.postOnAnimationDelayed(((ChangeInfo) arrayList6.get(0)).oldHolder.itemView, runnable5, getRemoveDuration());
                } else {
                    runnable5.run();
                }
            }
            if (z4) {
                new ArrayList();
                ArrayList arrayList8 = arrayList;
                boolean addAll3 = arrayList8.addAll(this.mPendingAdditions);
                boolean add3 = this.mAdditionsList.add(arrayList8);
                this.mPendingAdditions.clear();
                final ArrayList arrayList9 = arrayList8;
                new Runnable() {
                    public void run() {
                        Iterator it = arrayList9.iterator();
                        while (it.hasNext()) {
                            DefaultItemAnimator.this.animateAddImpl((RecyclerView.ViewHolder) it.next());
                        }
                        arrayList9.clear();
                        boolean remove = DefaultItemAnimator.this.mAdditionsList.remove(arrayList9);
                    }
                };
                Runnable runnable6 = runnable;
                if (z || z2 || z3) {
                    ViewCompat.postOnAnimationDelayed(((RecyclerView.ViewHolder) arrayList8.get(0)).itemView, runnable6, (z ? getRemoveDuration() : 0) + Math.max(z2 ? getMoveDuration() : 0, z3 ? getChangeDuration() : 0));
                } else {
                    runnable6.run();
                }
            }
        }
    }

    public boolean animateRemove(RecyclerView.ViewHolder viewHolder) {
        RecyclerView.ViewHolder viewHolder2 = viewHolder;
        resetAnimation(viewHolder2);
        boolean add = this.mPendingRemovals.add(viewHolder2);
        return true;
    }

    private void animateRemoveImpl(RecyclerView.ViewHolder viewHolder) {
        ViewPropertyAnimatorListener viewPropertyAnimatorListener;
        RecyclerView.ViewHolder viewHolder2 = viewHolder;
        ViewPropertyAnimatorCompat animate = ViewCompat.animate(viewHolder2.itemView);
        boolean add = this.mRemoveAnimations.add(viewHolder2);
        final RecyclerView.ViewHolder viewHolder3 = viewHolder2;
        final ViewPropertyAnimatorCompat viewPropertyAnimatorCompat = animate;
        new VpaListenerAdapter() {
            public void onAnimationStart(View view) {
                DefaultItemAnimator.this.dispatchRemoveStarting(viewHolder3);
            }

            public void onAnimationEnd(View view) {
                ViewPropertyAnimatorCompat listener = viewPropertyAnimatorCompat.setListener(null);
                ViewCompat.setAlpha(view, 1.0f);
                DefaultItemAnimator.this.dispatchRemoveFinished(viewHolder3);
                boolean remove = DefaultItemAnimator.this.mRemoveAnimations.remove(viewHolder3);
                DefaultItemAnimator.this.dispatchFinishedWhenDone();
            }
        };
        animate.setDuration(getRemoveDuration()).alpha(0.0f).setListener(viewPropertyAnimatorListener).start();
    }

    public boolean animateAdd(RecyclerView.ViewHolder viewHolder) {
        RecyclerView.ViewHolder viewHolder2 = viewHolder;
        resetAnimation(viewHolder2);
        ViewCompat.setAlpha(viewHolder2.itemView, 0.0f);
        boolean add = this.mPendingAdditions.add(viewHolder2);
        return true;
    }

    /* access modifiers changed from: private */
    public void animateAddImpl(RecyclerView.ViewHolder viewHolder) {
        ViewPropertyAnimatorListener viewPropertyAnimatorListener;
        RecyclerView.ViewHolder viewHolder2 = viewHolder;
        ViewPropertyAnimatorCompat animate = ViewCompat.animate(viewHolder2.itemView);
        boolean add = this.mAddAnimations.add(viewHolder2);
        final RecyclerView.ViewHolder viewHolder3 = viewHolder2;
        final ViewPropertyAnimatorCompat viewPropertyAnimatorCompat = animate;
        new VpaListenerAdapter() {
            public void onAnimationStart(View view) {
                DefaultItemAnimator.this.dispatchAddStarting(viewHolder3);
            }

            public void onAnimationCancel(View view) {
                ViewCompat.setAlpha(view, 1.0f);
            }

            public void onAnimationEnd(View view) {
                ViewPropertyAnimatorCompat listener = viewPropertyAnimatorCompat.setListener(null);
                DefaultItemAnimator.this.dispatchAddFinished(viewHolder3);
                boolean remove = DefaultItemAnimator.this.mAddAnimations.remove(viewHolder3);
                DefaultItemAnimator.this.dispatchFinishedWhenDone();
            }
        };
        animate.alpha(1.0f).setDuration(getAddDuration()).setListener(viewPropertyAnimatorListener).start();
    }

    public boolean animateMove(RecyclerView.ViewHolder viewHolder, int i, int i2, int i3, int i4) {
        Object obj;
        RecyclerView.ViewHolder viewHolder2 = viewHolder;
        int i5 = i3;
        int i6 = i4;
        View view = viewHolder2.itemView;
        int translationX = (int) (((float) i) + ViewCompat.getTranslationX(viewHolder2.itemView));
        int translationY = (int) (((float) i2) + ViewCompat.getTranslationY(viewHolder2.itemView));
        resetAnimation(viewHolder2);
        int i7 = i5 - translationX;
        int i8 = i6 - translationY;
        if (i7 == 0 && i8 == 0) {
            dispatchMoveFinished(viewHolder2);
            return false;
        }
        if (i7 != 0) {
            ViewCompat.setTranslationX(view, (float) (-i7));
        }
        if (i8 != 0) {
            ViewCompat.setTranslationY(view, (float) (-i8));
        }
        new MoveInfo(viewHolder2, translationX, translationY, i5, i6);
        boolean add = this.mPendingMoves.add(obj);
        return true;
    }

    /* access modifiers changed from: private */
    public void animateMoveImpl(RecyclerView.ViewHolder viewHolder, int i, int i2, int i3, int i4) {
        ViewPropertyAnimatorListener viewPropertyAnimatorListener;
        RecyclerView.ViewHolder viewHolder2 = viewHolder;
        View view = viewHolder2.itemView;
        int i5 = i3 - i;
        int i6 = i4 - i2;
        if (i5 != 0) {
            ViewPropertyAnimatorCompat translationX = ViewCompat.animate(view).translationX(0.0f);
        }
        if (i6 != 0) {
            ViewPropertyAnimatorCompat translationY = ViewCompat.animate(view).translationY(0.0f);
        }
        ViewPropertyAnimatorCompat animate = ViewCompat.animate(view);
        boolean add = this.mMoveAnimations.add(viewHolder2);
        final RecyclerView.ViewHolder viewHolder3 = viewHolder2;
        final int i7 = i5;
        final int i8 = i6;
        final ViewPropertyAnimatorCompat viewPropertyAnimatorCompat = animate;
        new VpaListenerAdapter() {
            public void onAnimationStart(View view) {
                DefaultItemAnimator.this.dispatchMoveStarting(viewHolder3);
            }

            public void onAnimationCancel(View view) {
                View view2 = view;
                if (i7 != 0) {
                    ViewCompat.setTranslationX(view2, 0.0f);
                }
                if (i8 != 0) {
                    ViewCompat.setTranslationY(view2, 0.0f);
                }
            }

            public void onAnimationEnd(View view) {
                ViewPropertyAnimatorCompat listener = viewPropertyAnimatorCompat.setListener(null);
                DefaultItemAnimator.this.dispatchMoveFinished(viewHolder3);
                boolean remove = DefaultItemAnimator.this.mMoveAnimations.remove(viewHolder3);
                DefaultItemAnimator.this.dispatchFinishedWhenDone();
            }
        };
        animate.setDuration(getMoveDuration()).setListener(viewPropertyAnimatorListener).start();
    }

    public boolean animateChange(RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder viewHolder2, int i, int i2, int i3, int i4) {
        Object obj;
        RecyclerView.ViewHolder viewHolder3 = viewHolder;
        RecyclerView.ViewHolder viewHolder4 = viewHolder2;
        int i5 = i;
        int i6 = i2;
        int i7 = i3;
        int i8 = i4;
        if (viewHolder3 == viewHolder4) {
            return animateMove(viewHolder3, i5, i6, i7, i8);
        }
        float translationX = ViewCompat.getTranslationX(viewHolder3.itemView);
        float translationY = ViewCompat.getTranslationY(viewHolder3.itemView);
        float alpha = ViewCompat.getAlpha(viewHolder3.itemView);
        resetAnimation(viewHolder3);
        int i9 = (int) (((float) (i7 - i5)) - translationX);
        int i10 = (int) (((float) (i8 - i6)) - translationY);
        ViewCompat.setTranslationX(viewHolder3.itemView, translationX);
        ViewCompat.setTranslationY(viewHolder3.itemView, translationY);
        ViewCompat.setAlpha(viewHolder3.itemView, alpha);
        if (viewHolder4 != null) {
            resetAnimation(viewHolder4);
            ViewCompat.setTranslationX(viewHolder4.itemView, (float) (-i9));
            ViewCompat.setTranslationY(viewHolder4.itemView, (float) (-i10));
            ViewCompat.setAlpha(viewHolder4.itemView, 0.0f);
        }
        new ChangeInfo(viewHolder3, viewHolder4, i5, i6, i7, i8);
        boolean add = this.mPendingChanges.add(obj);
        return true;
    }

    /* access modifiers changed from: private */
    public void animateChangeImpl(ChangeInfo changeInfo) {
        ViewPropertyAnimatorListener viewPropertyAnimatorListener;
        ViewPropertyAnimatorListener viewPropertyAnimatorListener2;
        ChangeInfo changeInfo2 = changeInfo;
        RecyclerView.ViewHolder viewHolder = changeInfo2.oldHolder;
        View view = viewHolder == null ? null : viewHolder.itemView;
        RecyclerView.ViewHolder viewHolder2 = changeInfo2.newHolder;
        View view2 = viewHolder2 != null ? viewHolder2.itemView : null;
        if (view != null) {
            ViewPropertyAnimatorCompat duration = ViewCompat.animate(view).setDuration(getChangeDuration());
            boolean add = this.mChangeAnimations.add(changeInfo2.oldHolder);
            ViewPropertyAnimatorCompat translationX = duration.translationX((float) (changeInfo2.toX - changeInfo2.fromX));
            ViewPropertyAnimatorCompat translationY = duration.translationY((float) (changeInfo2.toY - changeInfo2.fromY));
            final ChangeInfo changeInfo3 = changeInfo2;
            final ViewPropertyAnimatorCompat viewPropertyAnimatorCompat = duration;
            new VpaListenerAdapter() {
                public void onAnimationStart(View view) {
                    DefaultItemAnimator.this.dispatchChangeStarting(changeInfo3.oldHolder, true);
                }

                public void onAnimationEnd(View view) {
                    View view2 = view;
                    ViewPropertyAnimatorCompat listener = viewPropertyAnimatorCompat.setListener(null);
                    ViewCompat.setAlpha(view2, 1.0f);
                    ViewCompat.setTranslationX(view2, 0.0f);
                    ViewCompat.setTranslationY(view2, 0.0f);
                    DefaultItemAnimator.this.dispatchChangeFinished(changeInfo3.oldHolder, true);
                    boolean remove = DefaultItemAnimator.this.mChangeAnimations.remove(changeInfo3.oldHolder);
                    DefaultItemAnimator.this.dispatchFinishedWhenDone();
                }
            };
            duration.alpha(0.0f).setListener(viewPropertyAnimatorListener2).start();
        }
        if (view2 != null) {
            ViewPropertyAnimatorCompat animate = ViewCompat.animate(view2);
            boolean add2 = this.mChangeAnimations.add(changeInfo2.newHolder);
            final ChangeInfo changeInfo4 = changeInfo2;
            final ViewPropertyAnimatorCompat viewPropertyAnimatorCompat2 = animate;
            final View view3 = view2;
            new VpaListenerAdapter() {
                public void onAnimationStart(View view) {
                    DefaultItemAnimator.this.dispatchChangeStarting(changeInfo4.newHolder, false);
                }

                public void onAnimationEnd(View view) {
                    ViewPropertyAnimatorCompat listener = viewPropertyAnimatorCompat2.setListener(null);
                    ViewCompat.setAlpha(view3, 1.0f);
                    ViewCompat.setTranslationX(view3, 0.0f);
                    ViewCompat.setTranslationY(view3, 0.0f);
                    DefaultItemAnimator.this.dispatchChangeFinished(changeInfo4.newHolder, false);
                    boolean remove = DefaultItemAnimator.this.mChangeAnimations.remove(changeInfo4.newHolder);
                    DefaultItemAnimator.this.dispatchFinishedWhenDone();
                }
            };
            animate.translationX(0.0f).translationY(0.0f).setDuration(getChangeDuration()).alpha(1.0f).setListener(viewPropertyAnimatorListener).start();
        }
    }

    private void endChangeAnimation(List<ChangeInfo> list, RecyclerView.ViewHolder viewHolder) {
        List<ChangeInfo> list2 = list;
        RecyclerView.ViewHolder viewHolder2 = viewHolder;
        for (int size = list2.size() - 1; size >= 0; size--) {
            ChangeInfo changeInfo = list2.get(size);
            if (endChangeAnimationIfNecessary(changeInfo, viewHolder2) && changeInfo.oldHolder == null && changeInfo.newHolder == null) {
                boolean remove = list2.remove(changeInfo);
            }
        }
    }

    private void endChangeAnimationIfNecessary(ChangeInfo changeInfo) {
        ChangeInfo changeInfo2 = changeInfo;
        if (changeInfo2.oldHolder != null) {
            boolean endChangeAnimationIfNecessary = endChangeAnimationIfNecessary(changeInfo2, changeInfo2.oldHolder);
        }
        if (changeInfo2.newHolder != null) {
            boolean endChangeAnimationIfNecessary2 = endChangeAnimationIfNecessary(changeInfo2, changeInfo2.newHolder);
        }
    }

    private boolean endChangeAnimationIfNecessary(ChangeInfo changeInfo, RecyclerView.ViewHolder viewHolder) {
        ChangeInfo changeInfo2 = changeInfo;
        RecyclerView.ViewHolder viewHolder2 = viewHolder;
        boolean z = false;
        if (changeInfo2.newHolder == viewHolder2) {
            changeInfo2.newHolder = null;
        } else if (changeInfo2.oldHolder != viewHolder2) {
            return false;
        } else {
            changeInfo2.oldHolder = null;
            z = true;
        }
        ViewCompat.setAlpha(viewHolder2.itemView, 1.0f);
        ViewCompat.setTranslationX(viewHolder2.itemView, 0.0f);
        ViewCompat.setTranslationY(viewHolder2.itemView, 0.0f);
        dispatchChangeFinished(viewHolder2, z);
        return true;
    }

    public void endAnimation(RecyclerView.ViewHolder viewHolder) {
        RecyclerView.ViewHolder viewHolder2 = viewHolder;
        View view = viewHolder2.itemView;
        ViewCompat.animate(view).cancel();
        for (int size = this.mPendingMoves.size() - 1; size >= 0; size--) {
            if (this.mPendingMoves.get(size).holder == viewHolder2) {
                ViewCompat.setTranslationY(view, 0.0f);
                ViewCompat.setTranslationX(view, 0.0f);
                dispatchMoveFinished(viewHolder2);
                MoveInfo remove = this.mPendingMoves.remove(size);
            }
        }
        endChangeAnimation(this.mPendingChanges, viewHolder2);
        if (this.mPendingRemovals.remove(viewHolder2)) {
            ViewCompat.setAlpha(view, 1.0f);
            dispatchRemoveFinished(viewHolder2);
        }
        if (this.mPendingAdditions.remove(viewHolder2)) {
            ViewCompat.setAlpha(view, 1.0f);
            dispatchAddFinished(viewHolder2);
        }
        for (int size2 = this.mChangesList.size() - 1; size2 >= 0; size2--) {
            ArrayList arrayList = this.mChangesList.get(size2);
            endChangeAnimation(arrayList, viewHolder2);
            if (arrayList.isEmpty()) {
                ArrayList<ChangeInfo> remove2 = this.mChangesList.remove(size2);
            }
        }
        for (int size3 = this.mMovesList.size() - 1; size3 >= 0; size3--) {
            ArrayList arrayList2 = this.mMovesList.get(size3);
            int size4 = arrayList2.size() - 1;
            while (true) {
                if (size4 < 0) {
                    break;
                } else if (((MoveInfo) arrayList2.get(size4)).holder == viewHolder2) {
                    ViewCompat.setTranslationY(view, 0.0f);
                    ViewCompat.setTranslationX(view, 0.0f);
                    dispatchMoveFinished(viewHolder2);
                    Object remove3 = arrayList2.remove(size4);
                    if (arrayList2.isEmpty()) {
                        ArrayList<MoveInfo> remove4 = this.mMovesList.remove(size3);
                    }
                } else {
                    size4--;
                }
            }
        }
        for (int size5 = this.mAdditionsList.size() - 1; size5 >= 0; size5--) {
            ArrayList arrayList3 = this.mAdditionsList.get(size5);
            if (arrayList3.remove(viewHolder2)) {
                ViewCompat.setAlpha(view, 1.0f);
                dispatchAddFinished(viewHolder2);
                if (arrayList3.isEmpty()) {
                    ArrayList<RecyclerView.ViewHolder> remove5 = this.mAdditionsList.remove(size5);
                }
            }
        }
        if (this.mRemoveAnimations.remove(viewHolder2)) {
        }
        if (this.mAddAnimations.remove(viewHolder2)) {
        }
        if (this.mChangeAnimations.remove(viewHolder2)) {
        }
        if (this.mMoveAnimations.remove(viewHolder2)) {
        }
        dispatchFinishedWhenDone();
    }

    private void resetAnimation(RecyclerView.ViewHolder viewHolder) {
        RecyclerView.ViewHolder viewHolder2 = viewHolder;
        AnimatorCompatHelper.clearInterpolator(viewHolder2.itemView);
        endAnimation(viewHolder2);
    }

    public boolean isRunning() {
        return !this.mPendingAdditions.isEmpty() || !this.mPendingChanges.isEmpty() || !this.mPendingMoves.isEmpty() || !this.mPendingRemovals.isEmpty() || !this.mMoveAnimations.isEmpty() || !this.mRemoveAnimations.isEmpty() || !this.mAddAnimations.isEmpty() || !this.mChangeAnimations.isEmpty() || !this.mMovesList.isEmpty() || !this.mAdditionsList.isEmpty() || !this.mChangesList.isEmpty();
    }

    /* access modifiers changed from: private */
    public void dispatchFinishedWhenDone() {
        if (!isRunning()) {
            dispatchAnimationsFinished();
        }
    }

    public void endAnimations() {
        for (int size = this.mPendingMoves.size() - 1; size >= 0; size--) {
            MoveInfo moveInfo = this.mPendingMoves.get(size);
            View view = moveInfo.holder.itemView;
            ViewCompat.setTranslationY(view, 0.0f);
            ViewCompat.setTranslationX(view, 0.0f);
            dispatchMoveFinished(moveInfo.holder);
            MoveInfo remove = this.mPendingMoves.remove(size);
        }
        for (int size2 = this.mPendingRemovals.size() - 1; size2 >= 0; size2--) {
            dispatchRemoveFinished(this.mPendingRemovals.get(size2));
            RecyclerView.ViewHolder remove2 = this.mPendingRemovals.remove(size2);
        }
        for (int size3 = this.mPendingAdditions.size() - 1; size3 >= 0; size3--) {
            RecyclerView.ViewHolder viewHolder = this.mPendingAdditions.get(size3);
            ViewCompat.setAlpha(viewHolder.itemView, 1.0f);
            dispatchAddFinished(viewHolder);
            RecyclerView.ViewHolder remove3 = this.mPendingAdditions.remove(size3);
        }
        for (int size4 = this.mPendingChanges.size() - 1; size4 >= 0; size4--) {
            endChangeAnimationIfNecessary(this.mPendingChanges.get(size4));
        }
        this.mPendingChanges.clear();
        if (isRunning()) {
            for (int size5 = this.mMovesList.size() - 1; size5 >= 0; size5--) {
                ArrayList arrayList = this.mMovesList.get(size5);
                for (int size6 = arrayList.size() - 1; size6 >= 0; size6--) {
                    MoveInfo moveInfo2 = (MoveInfo) arrayList.get(size6);
                    View view2 = moveInfo2.holder.itemView;
                    ViewCompat.setTranslationY(view2, 0.0f);
                    ViewCompat.setTranslationX(view2, 0.0f);
                    dispatchMoveFinished(moveInfo2.holder);
                    Object remove4 = arrayList.remove(size6);
                    if (arrayList.isEmpty()) {
                        boolean remove5 = this.mMovesList.remove(arrayList);
                    }
                }
            }
            for (int size7 = this.mAdditionsList.size() - 1; size7 >= 0; size7--) {
                ArrayList arrayList2 = this.mAdditionsList.get(size7);
                for (int size8 = arrayList2.size() - 1; size8 >= 0; size8--) {
                    RecyclerView.ViewHolder viewHolder2 = (RecyclerView.ViewHolder) arrayList2.get(size8);
                    ViewCompat.setAlpha(viewHolder2.itemView, 1.0f);
                    dispatchAddFinished(viewHolder2);
                    Object remove6 = arrayList2.remove(size8);
                    if (arrayList2.isEmpty()) {
                        boolean remove7 = this.mAdditionsList.remove(arrayList2);
                    }
                }
            }
            for (int size9 = this.mChangesList.size() - 1; size9 >= 0; size9--) {
                ArrayList arrayList3 = this.mChangesList.get(size9);
                for (int size10 = arrayList3.size() - 1; size10 >= 0; size10--) {
                    endChangeAnimationIfNecessary((ChangeInfo) arrayList3.get(size10));
                    if (arrayList3.isEmpty()) {
                        boolean remove8 = this.mChangesList.remove(arrayList3);
                    }
                }
            }
            cancelAll(this.mRemoveAnimations);
            cancelAll(this.mMoveAnimations);
            cancelAll(this.mAddAnimations);
            cancelAll(this.mChangeAnimations);
            dispatchAnimationsFinished();
        }
    }

    /* access modifiers changed from: package-private */
    public void cancelAll(List<RecyclerView.ViewHolder> list) {
        List<RecyclerView.ViewHolder> list2 = list;
        for (int size = list2.size() - 1; size >= 0; size--) {
            ViewCompat.animate(list2.get(size).itemView).cancel();
        }
    }

    private static class VpaListenerAdapter implements ViewPropertyAnimatorListener {
        private VpaListenerAdapter() {
        }

        public void onAnimationStart(View view) {
        }

        public void onAnimationEnd(View view) {
        }

        public void onAnimationCancel(View view) {
        }
    }
}
