package android.support.v7.widget;

import android.content.Context;
import android.support.v7.appcompat.R;
import android.util.AttributeSet;
import android.widget.SeekBar;

public class AppCompatSeekBar extends SeekBar {
    private AppCompatSeekBarHelper mAppCompatSeekBarHelper;
    private TintManager mTintManager;

    public AppCompatSeekBar(Context context) {
        this(context, null);
    }

    public AppCompatSeekBar(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, R.attr.seekBarStyle);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AppCompatSeekBar(android.content.Context r11, android.util.AttributeSet r12, int r13) {
        /*
            r10 = this;
            r0 = r10
            r1 = r11
            r2 = r12
            r3 = r13
            r4 = r0
            r5 = r1
            r6 = r2
            r7 = r3
            r4.<init>(r5, r6, r7)
            r4 = r0
            r5 = r1
            android.support.v7.widget.TintManager r5 = android.support.v7.widget.TintManager.get(r5)
            r4.mTintManager = r5
            r4 = r0
            android.support.v7.widget.AppCompatSeekBarHelper r5 = new android.support.v7.widget.AppCompatSeekBarHelper
            r9 = r5
            r5 = r9
            r6 = r9
            r7 = r0
            r8 = r0
            android.support.v7.widget.TintManager r8 = r8.mTintManager
            r6.<init>(r7, r8)
            r4.mAppCompatSeekBarHelper = r5
            r4 = r0
            android.support.v7.widget.AppCompatSeekBarHelper r4 = r4.mAppCompatSeekBarHelper
            r5 = r2
            r6 = r3
            r4.loadFromAttributes(r5, r6)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.AppCompatSeekBar.<init>(android.content.Context, android.util.AttributeSet, int):void");
    }
}
