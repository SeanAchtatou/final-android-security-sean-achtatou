package android.support.v7.app;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPropertyAnimatorCompat;
import android.support.v4.view.ViewPropertyAnimatorListener;
import android.support.v4.view.ViewPropertyAnimatorListenerAdapter;
import android.support.v4.view.ViewPropertyAnimatorUpdateListener;
import android.support.v7.app.ActionBar;
import android.support.v7.appcompat.R;
import android.support.v7.view.ActionBarPolicy;
import android.support.v7.view.ActionMode;
import android.support.v7.view.SupportMenuInflater;
import android.support.v7.view.ViewPropertyAnimatorCompatSet;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuPopupHelper;
import android.support.v7.view.menu.SubMenuBuilder;
import android.support.v7.widget.ActionBarContainer;
import android.support.v7.widget.ActionBarContextView;
import android.support.v7.widget.ActionBarOverlayLayout;
import android.support.v7.widget.DecorToolbar;
import android.support.v7.widget.ScrollingTabContainerView;
import android.support.v7.widget.TintManager;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.AdapterView;
import android.widget.SpinnerAdapter;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class WindowDecorActionBar extends ActionBar implements ActionBarOverlayLayout.ActionBarVisibilityCallback {
    static final /* synthetic */ boolean $assertionsDisabled = (!WindowDecorActionBar.class.desiredAssertionStatus());
    private static final boolean ALLOW_SHOW_HIDE_ANIMATIONS;
    private static final long FADE_IN_DURATION_MS = 200;
    private static final long FADE_OUT_DURATION_MS = 100;
    private static final int INVALID_POSITION = -1;
    private static final String TAG = "WindowDecorActionBar";
    private static final Interpolator sHideInterpolator;
    private static final Interpolator sShowInterpolator;
    ActionModeImpl mActionMode;
    private Activity mActivity;
    /* access modifiers changed from: private */
    public ActionBarContainer mContainerView;
    /* access modifiers changed from: private */
    public boolean mContentAnimations;
    /* access modifiers changed from: private */
    public View mContentView;
    /* access modifiers changed from: private */
    public Context mContext;
    /* access modifiers changed from: private */
    public ActionBarContextView mContextView;
    private int mCurWindowVisibility;
    private ViewPropertyAnimatorCompatSet mCurrentShowAnim;
    /* access modifiers changed from: private */
    public DecorToolbar mDecorToolbar;
    ActionMode mDeferredDestroyActionMode;
    ActionMode.Callback mDeferredModeDestroyCallback;
    private Dialog mDialog;
    private boolean mDisplayHomeAsUpSet;
    private boolean mHasEmbeddedTabs;
    /* access modifiers changed from: private */
    public boolean mHiddenByApp;
    /* access modifiers changed from: private */
    public boolean mHiddenBySystem;
    final ViewPropertyAnimatorListener mHideListener;
    boolean mHideOnContentScroll;
    private boolean mLastMenuVisibility;
    private ArrayList<ActionBar.OnMenuVisibilityListener> mMenuVisibilityListeners;
    private boolean mNowShowing;
    /* access modifiers changed from: private */
    public ActionBarOverlayLayout mOverlayLayout;
    private int mSavedTabPosition = -1;
    private TabImpl mSelectedTab;
    private boolean mShowHideAnimationEnabled;
    final ViewPropertyAnimatorListener mShowListener;
    private boolean mShowingForMode;
    /* access modifiers changed from: private */
    public ScrollingTabContainerView mTabScrollView;
    private ArrayList<TabImpl> mTabs;
    private Context mThemedContext;
    private TintManager mTintManager;
    final ViewPropertyAnimatorUpdateListener mUpdateListener;

    static {
        Interpolator interpolator;
        Interpolator interpolator2;
        boolean z;
        new AccelerateInterpolator();
        sHideInterpolator = interpolator;
        new DecelerateInterpolator();
        sShowInterpolator = interpolator2;
        if (Build.VERSION.SDK_INT >= 14) {
            z = true;
        } else {
            z = false;
        }
        ALLOW_SHOW_HIDE_ANIMATIONS = z;
    }

    static /* synthetic */ ViewPropertyAnimatorCompatSet access$302(WindowDecorActionBar windowDecorActionBar, ViewPropertyAnimatorCompatSet viewPropertyAnimatorCompatSet) {
        ViewPropertyAnimatorCompatSet viewPropertyAnimatorCompatSet2 = viewPropertyAnimatorCompatSet;
        ViewPropertyAnimatorCompatSet viewPropertyAnimatorCompatSet3 = viewPropertyAnimatorCompatSet2;
        windowDecorActionBar.mCurrentShowAnim = viewPropertyAnimatorCompatSet3;
        return viewPropertyAnimatorCompatSet2;
    }

    public WindowDecorActionBar(Activity activity, boolean z) {
        ArrayList<TabImpl> arrayList;
        ArrayList<ActionBar.OnMenuVisibilityListener> arrayList2;
        ViewPropertyAnimatorListener viewPropertyAnimatorListener;
        ViewPropertyAnimatorListener viewPropertyAnimatorListener2;
        ViewPropertyAnimatorUpdateListener viewPropertyAnimatorUpdateListener;
        Activity activity2 = activity;
        new ArrayList<>();
        this.mTabs = arrayList;
        new ArrayList<>();
        this.mMenuVisibilityListeners = arrayList2;
        this.mCurWindowVisibility = 0;
        this.mContentAnimations = true;
        this.mNowShowing = true;
        new ViewPropertyAnimatorListenerAdapter() {
            public void onAnimationEnd(View view) {
                if (WindowDecorActionBar.this.mContentAnimations && WindowDecorActionBar.this.mContentView != null) {
                    ViewCompat.setTranslationY(WindowDecorActionBar.this.mContentView, 0.0f);
                    ViewCompat.setTranslationY(WindowDecorActionBar.this.mContainerView, 0.0f);
                }
                WindowDecorActionBar.this.mContainerView.setVisibility(8);
                WindowDecorActionBar.this.mContainerView.setTransitioning(false);
                ViewPropertyAnimatorCompatSet access$302 = WindowDecorActionBar.access$302(WindowDecorActionBar.this, null);
                WindowDecorActionBar.this.completeDeferredDestroyActionMode();
                if (WindowDecorActionBar.this.mOverlayLayout != null) {
                    ViewCompat.requestApplyInsets(WindowDecorActionBar.this.mOverlayLayout);
                }
            }
        };
        this.mHideListener = viewPropertyAnimatorListener;
        new ViewPropertyAnimatorListenerAdapter() {
            public void onAnimationEnd(View view) {
                ViewPropertyAnimatorCompatSet access$302 = WindowDecorActionBar.access$302(WindowDecorActionBar.this, null);
                WindowDecorActionBar.this.mContainerView.requestLayout();
            }
        };
        this.mShowListener = viewPropertyAnimatorListener2;
        new ViewPropertyAnimatorUpdateListener() {
            public void onAnimationUpdate(View view) {
                ((View) WindowDecorActionBar.this.mContainerView.getParent()).invalidate();
            }
        };
        this.mUpdateListener = viewPropertyAnimatorUpdateListener;
        this.mActivity = activity2;
        View decorView = activity2.getWindow().getDecorView();
        init(decorView);
        if (!z) {
            this.mContentView = decorView.findViewById(16908290);
        }
    }

    public WindowDecorActionBar(Dialog dialog) {
        ArrayList<TabImpl> arrayList;
        ArrayList<ActionBar.OnMenuVisibilityListener> arrayList2;
        ViewPropertyAnimatorListener viewPropertyAnimatorListener;
        ViewPropertyAnimatorListener viewPropertyAnimatorListener2;
        ViewPropertyAnimatorUpdateListener viewPropertyAnimatorUpdateListener;
        Dialog dialog2 = dialog;
        new ArrayList<>();
        this.mTabs = arrayList;
        new ArrayList<>();
        this.mMenuVisibilityListeners = arrayList2;
        this.mCurWindowVisibility = 0;
        this.mContentAnimations = true;
        this.mNowShowing = true;
        new ViewPropertyAnimatorListenerAdapter() {
            public void onAnimationEnd(View view) {
                if (WindowDecorActionBar.this.mContentAnimations && WindowDecorActionBar.this.mContentView != null) {
                    ViewCompat.setTranslationY(WindowDecorActionBar.this.mContentView, 0.0f);
                    ViewCompat.setTranslationY(WindowDecorActionBar.this.mContainerView, 0.0f);
                }
                WindowDecorActionBar.this.mContainerView.setVisibility(8);
                WindowDecorActionBar.this.mContainerView.setTransitioning(false);
                ViewPropertyAnimatorCompatSet access$302 = WindowDecorActionBar.access$302(WindowDecorActionBar.this, null);
                WindowDecorActionBar.this.completeDeferredDestroyActionMode();
                if (WindowDecorActionBar.this.mOverlayLayout != null) {
                    ViewCompat.requestApplyInsets(WindowDecorActionBar.this.mOverlayLayout);
                }
            }
        };
        this.mHideListener = viewPropertyAnimatorListener;
        new ViewPropertyAnimatorListenerAdapter() {
            public void onAnimationEnd(View view) {
                ViewPropertyAnimatorCompatSet access$302 = WindowDecorActionBar.access$302(WindowDecorActionBar.this, null);
                WindowDecorActionBar.this.mContainerView.requestLayout();
            }
        };
        this.mShowListener = viewPropertyAnimatorListener2;
        new ViewPropertyAnimatorUpdateListener() {
            public void onAnimationUpdate(View view) {
                ((View) WindowDecorActionBar.this.mContainerView.getParent()).invalidate();
            }
        };
        this.mUpdateListener = viewPropertyAnimatorUpdateListener;
        this.mDialog = dialog2;
        init(dialog2.getWindow().getDecorView());
    }

    public WindowDecorActionBar(View view) {
        ArrayList<TabImpl> arrayList;
        ArrayList<ActionBar.OnMenuVisibilityListener> arrayList2;
        ViewPropertyAnimatorListener viewPropertyAnimatorListener;
        ViewPropertyAnimatorListener viewPropertyAnimatorListener2;
        ViewPropertyAnimatorUpdateListener viewPropertyAnimatorUpdateListener;
        Throwable th;
        View view2 = view;
        new ArrayList<>();
        this.mTabs = arrayList;
        new ArrayList<>();
        this.mMenuVisibilityListeners = arrayList2;
        this.mCurWindowVisibility = 0;
        this.mContentAnimations = true;
        this.mNowShowing = true;
        new ViewPropertyAnimatorListenerAdapter() {
            public void onAnimationEnd(View view) {
                if (WindowDecorActionBar.this.mContentAnimations && WindowDecorActionBar.this.mContentView != null) {
                    ViewCompat.setTranslationY(WindowDecorActionBar.this.mContentView, 0.0f);
                    ViewCompat.setTranslationY(WindowDecorActionBar.this.mContainerView, 0.0f);
                }
                WindowDecorActionBar.this.mContainerView.setVisibility(8);
                WindowDecorActionBar.this.mContainerView.setTransitioning(false);
                ViewPropertyAnimatorCompatSet access$302 = WindowDecorActionBar.access$302(WindowDecorActionBar.this, null);
                WindowDecorActionBar.this.completeDeferredDestroyActionMode();
                if (WindowDecorActionBar.this.mOverlayLayout != null) {
                    ViewCompat.requestApplyInsets(WindowDecorActionBar.this.mOverlayLayout);
                }
            }
        };
        this.mHideListener = viewPropertyAnimatorListener;
        new ViewPropertyAnimatorListenerAdapter() {
            public void onAnimationEnd(View view) {
                ViewPropertyAnimatorCompatSet access$302 = WindowDecorActionBar.access$302(WindowDecorActionBar.this, null);
                WindowDecorActionBar.this.mContainerView.requestLayout();
            }
        };
        this.mShowListener = viewPropertyAnimatorListener2;
        new ViewPropertyAnimatorUpdateListener() {
            public void onAnimationUpdate(View view) {
                ((View) WindowDecorActionBar.this.mContainerView.getParent()).invalidate();
            }
        };
        this.mUpdateListener = viewPropertyAnimatorUpdateListener;
        if ($assertionsDisabled || view2.isInEditMode()) {
            init(view2);
            return;
        }
        Throwable th2 = th;
        new AssertionError();
        throw th2;
    }

    private void init(View view) {
        Throwable th;
        StringBuilder sb;
        View view2 = view;
        this.mOverlayLayout = (ActionBarOverlayLayout) view2.findViewById(R.id.decor_content_parent);
        if (this.mOverlayLayout != null) {
            this.mOverlayLayout.setActionBarVisibilityCallback(this);
        }
        this.mDecorToolbar = getDecorToolbar(view2.findViewById(R.id.action_bar));
        this.mContextView = (ActionBarContextView) view2.findViewById(R.id.action_context_bar);
        this.mContainerView = (ActionBarContainer) view2.findViewById(R.id.action_bar_container);
        if (this.mDecorToolbar == null || this.mContextView == null || this.mContainerView == null) {
            Throwable th2 = th;
            new StringBuilder();
            new IllegalStateException(sb.append(getClass().getSimpleName()).append(" can only be used ").append("with a compatible window decor layout").toString());
            throw th2;
        }
        this.mContext = this.mDecorToolbar.getContext();
        boolean z = (this.mDecorToolbar.getDisplayOptions() & 4) != 0;
        if (z) {
            this.mDisplayHomeAsUpSet = true;
        }
        ActionBarPolicy actionBarPolicy = ActionBarPolicy.get(this.mContext);
        setHomeButtonEnabled(actionBarPolicy.enableHomeButtonByDefault() || z);
        setHasEmbeddedTabs(actionBarPolicy.hasEmbeddedTabs());
        TypedArray obtainStyledAttributes = this.mContext.obtainStyledAttributes(null, R.styleable.ActionBar, R.attr.actionBarStyle, 0);
        if (obtainStyledAttributes.getBoolean(R.styleable.ActionBar_hideOnContentScroll, false)) {
            setHideOnContentScrollEnabled(true);
        }
        int dimensionPixelSize = obtainStyledAttributes.getDimensionPixelSize(R.styleable.ActionBar_elevation, 0);
        if (dimensionPixelSize != 0) {
            setElevation((float) dimensionPixelSize);
        }
        obtainStyledAttributes.recycle();
    }

    private DecorToolbar getDecorToolbar(View view) {
        IllegalStateException illegalStateException;
        StringBuilder sb;
        View view2 = view;
        if (view2 instanceof DecorToolbar) {
            return (DecorToolbar) view2;
        }
        if (view2 instanceof Toolbar) {
            return ((Toolbar) view2).getWrapper();
        }
        IllegalStateException illegalStateException2 = illegalStateException;
        new StringBuilder();
        new IllegalStateException(sb.append("Can't make a decor toolbar out of ").append(view2).toString() != null ? view2.getClass().getSimpleName() : "null");
        throw illegalStateException2;
    }

    public void setElevation(float f) {
        ViewCompat.setElevation(this.mContainerView, f);
    }

    public float getElevation() {
        return ViewCompat.getElevation(this.mContainerView);
    }

    public void onConfigurationChanged(Configuration configuration) {
        setHasEmbeddedTabs(ActionBarPolicy.get(this.mContext).hasEmbeddedTabs());
    }

    private void setHasEmbeddedTabs(boolean z) {
        this.mHasEmbeddedTabs = z;
        if (!this.mHasEmbeddedTabs) {
            this.mDecorToolbar.setEmbeddedTabView(null);
            this.mContainerView.setTabContainer(this.mTabScrollView);
        } else {
            this.mContainerView.setTabContainer(null);
            this.mDecorToolbar.setEmbeddedTabView(this.mTabScrollView);
        }
        boolean z2 = getNavigationMode() == 2;
        if (this.mTabScrollView != null) {
            if (z2) {
                this.mTabScrollView.setVisibility(0);
                if (this.mOverlayLayout != null) {
                    ViewCompat.requestApplyInsets(this.mOverlayLayout);
                }
            } else {
                this.mTabScrollView.setVisibility(8);
            }
        }
        this.mDecorToolbar.setCollapsible(!this.mHasEmbeddedTabs && z2);
        this.mOverlayLayout.setHasNonEmbeddedTabs(!this.mHasEmbeddedTabs && z2);
    }

    private void ensureTabsExist() {
        ScrollingTabContainerView scrollingTabContainerView;
        if (this.mTabScrollView == null) {
            new ScrollingTabContainerView(this.mContext);
            ScrollingTabContainerView scrollingTabContainerView2 = scrollingTabContainerView;
            if (this.mHasEmbeddedTabs) {
                scrollingTabContainerView2.setVisibility(0);
                this.mDecorToolbar.setEmbeddedTabView(scrollingTabContainerView2);
            } else {
                if (getNavigationMode() == 2) {
                    scrollingTabContainerView2.setVisibility(0);
                    if (this.mOverlayLayout != null) {
                        ViewCompat.requestApplyInsets(this.mOverlayLayout);
                    }
                } else {
                    scrollingTabContainerView2.setVisibility(8);
                }
                this.mContainerView.setTabContainer(scrollingTabContainerView2);
            }
            this.mTabScrollView = scrollingTabContainerView2;
        }
    }

    /* access modifiers changed from: package-private */
    public void completeDeferredDestroyActionMode() {
        if (this.mDeferredModeDestroyCallback != null) {
            this.mDeferredModeDestroyCallback.onDestroyActionMode(this.mDeferredDestroyActionMode);
            this.mDeferredDestroyActionMode = null;
            this.mDeferredModeDestroyCallback = null;
        }
    }

    public void onWindowVisibilityChanged(int i) {
        this.mCurWindowVisibility = i;
    }

    public void setShowHideAnimationEnabled(boolean z) {
        boolean z2 = z;
        this.mShowHideAnimationEnabled = z2;
        if (!z2 && this.mCurrentShowAnim != null) {
            this.mCurrentShowAnim.cancel();
        }
    }

    public void addOnMenuVisibilityListener(ActionBar.OnMenuVisibilityListener onMenuVisibilityListener) {
        boolean add = this.mMenuVisibilityListeners.add(onMenuVisibilityListener);
    }

    public void removeOnMenuVisibilityListener(ActionBar.OnMenuVisibilityListener onMenuVisibilityListener) {
        boolean remove = this.mMenuVisibilityListeners.remove(onMenuVisibilityListener);
    }

    public void dispatchMenuVisibilityChanged(boolean z) {
        boolean z2 = z;
        if (z2 != this.mLastMenuVisibility) {
            this.mLastMenuVisibility = z2;
            int size = this.mMenuVisibilityListeners.size();
            for (int i = 0; i < size; i++) {
                this.mMenuVisibilityListeners.get(i).onMenuVisibilityChanged(z2);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public void setCustomView(int i) {
        setCustomView(LayoutInflater.from(getThemedContext()).inflate(i, this.mDecorToolbar.getViewGroup(), false));
    }

    public void setDisplayUseLogoEnabled(boolean z) {
        setDisplayOptions(z ? 1 : 0, 1);
    }

    public void setDisplayShowHomeEnabled(boolean z) {
        setDisplayOptions(z ? 2 : 0, 2);
    }

    public void setDisplayHomeAsUpEnabled(boolean z) {
        setDisplayOptions(z ? 4 : 0, 4);
    }

    public void setDisplayShowTitleEnabled(boolean z) {
        setDisplayOptions(z ? 8 : 0, 8);
    }

    public void setDisplayShowCustomEnabled(boolean z) {
        setDisplayOptions(z ? 16 : 0, 16);
    }

    public void setHomeButtonEnabled(boolean z) {
        this.mDecorToolbar.setHomeButtonEnabled(z);
    }

    public void setTitle(int i) {
        setTitle(this.mContext.getString(i));
    }

    public void setSubtitle(int i) {
        setSubtitle(this.mContext.getString(i));
    }

    public void setSelectedNavigationItem(int i) {
        Throwable th;
        int i2 = i;
        switch (this.mDecorToolbar.getNavigationMode()) {
            case 1:
                this.mDecorToolbar.setDropdownSelectedPosition(i2);
                return;
            case 2:
                selectTab(this.mTabs.get(i2));
                return;
            default:
                Throwable th2 = th;
                new IllegalStateException("setSelectedNavigationIndex not valid for current navigation mode");
                throw th2;
        }
    }

    public void removeAllTabs() {
        cleanupTabs();
    }

    private void cleanupTabs() {
        if (this.mSelectedTab != null) {
            selectTab(null);
        }
        this.mTabs.clear();
        if (this.mTabScrollView != null) {
            this.mTabScrollView.removeAllTabs();
        }
        this.mSavedTabPosition = -1;
    }

    public void setTitle(CharSequence charSequence) {
        this.mDecorToolbar.setTitle(charSequence);
    }

    public void setWindowTitle(CharSequence charSequence) {
        this.mDecorToolbar.setWindowTitle(charSequence);
    }

    public void setSubtitle(CharSequence charSequence) {
        this.mDecorToolbar.setSubtitle(charSequence);
    }

    public void setDisplayOptions(int i) {
        int i2 = i;
        if ((i2 & 4) != 0) {
            this.mDisplayHomeAsUpSet = true;
        }
        this.mDecorToolbar.setDisplayOptions(i2);
    }

    public void setDisplayOptions(int i, int i2) {
        int i3 = i;
        int i4 = i2;
        int displayOptions = this.mDecorToolbar.getDisplayOptions();
        if ((i4 & 4) != 0) {
            this.mDisplayHomeAsUpSet = true;
        }
        this.mDecorToolbar.setDisplayOptions((i3 & i4) | (displayOptions & (i4 ^ -1)));
    }

    public void setBackgroundDrawable(Drawable drawable) {
        this.mContainerView.setPrimaryBackground(drawable);
    }

    public void setStackedBackgroundDrawable(Drawable drawable) {
        this.mContainerView.setStackedBackground(drawable);
    }

    public void setSplitBackgroundDrawable(Drawable drawable) {
    }

    public View getCustomView() {
        return this.mDecorToolbar.getCustomView();
    }

    public CharSequence getTitle() {
        return this.mDecorToolbar.getTitle();
    }

    public CharSequence getSubtitle() {
        return this.mDecorToolbar.getSubtitle();
    }

    public int getNavigationMode() {
        return this.mDecorToolbar.getNavigationMode();
    }

    public int getDisplayOptions() {
        return this.mDecorToolbar.getDisplayOptions();
    }

    public ActionMode startActionMode(ActionMode.Callback callback) {
        ActionModeImpl actionModeImpl;
        ActionMode.Callback callback2 = callback;
        if (this.mActionMode != null) {
            this.mActionMode.finish();
        }
        this.mOverlayLayout.setHideOnContentScrollEnabled(false);
        this.mContextView.killMode();
        new ActionModeImpl(this, this.mContextView.getContext(), callback2);
        ActionModeImpl actionModeImpl2 = actionModeImpl;
        if (!actionModeImpl2.dispatchOnCreate()) {
            return null;
        }
        actionModeImpl2.invalidate();
        this.mContextView.initForMode(actionModeImpl2);
        animateToMode(true);
        this.mContextView.sendAccessibilityEvent(32);
        this.mActionMode = actionModeImpl2;
        return actionModeImpl2;
    }

    private void configureTab(ActionBar.Tab tab, int i) {
        Throwable th;
        int i2 = i;
        TabImpl tabImpl = (TabImpl) tab;
        if (tabImpl.getCallback() == null) {
            Throwable th2 = th;
            new IllegalStateException("Action Bar Tab must have a Callback");
            throw th2;
        }
        tabImpl.setPosition(i2);
        this.mTabs.add(i2, tabImpl);
        int size = this.mTabs.size();
        for (int i3 = i2 + 1; i3 < size; i3++) {
            this.mTabs.get(i3).setPosition(i3);
        }
    }

    public void addTab(ActionBar.Tab tab) {
        addTab(tab, this.mTabs.isEmpty());
    }

    public void addTab(ActionBar.Tab tab, int i) {
        addTab(tab, i, this.mTabs.isEmpty());
    }

    public void addTab(ActionBar.Tab tab, boolean z) {
        ActionBar.Tab tab2 = tab;
        boolean z2 = z;
        ensureTabsExist();
        this.mTabScrollView.addTab(tab2, z2);
        configureTab(tab2, this.mTabs.size());
        if (z2) {
            selectTab(tab2);
        }
    }

    public void addTab(ActionBar.Tab tab, int i, boolean z) {
        ActionBar.Tab tab2 = tab;
        int i2 = i;
        boolean z2 = z;
        ensureTabsExist();
        this.mTabScrollView.addTab(tab2, i2, z2);
        configureTab(tab2, i2);
        if (z2) {
            selectTab(tab2);
        }
    }

    public ActionBar.Tab newTab() {
        ActionBar.Tab tab;
        new TabImpl();
        return tab;
    }

    public void removeTab(ActionBar.Tab tab) {
        removeTabAt(tab.getPosition());
    }

    public void removeTabAt(int i) {
        int i2;
        int i3 = i;
        if (this.mTabScrollView != null) {
            if (this.mSelectedTab != null) {
                i2 = this.mSelectedTab.getPosition();
            } else {
                i2 = this.mSavedTabPosition;
            }
            int i4 = i2;
            this.mTabScrollView.removeTabAt(i3);
            TabImpl remove = this.mTabs.remove(i3);
            if (remove != null) {
                remove.setPosition(-1);
            }
            int size = this.mTabs.size();
            for (int i5 = i3; i5 < size; i5++) {
                this.mTabs.get(i5).setPosition(i5);
            }
            if (i4 == i3) {
                selectTab(this.mTabs.isEmpty() ? null : this.mTabs.get(Math.max(0, i3 - 1)));
            }
        }
    }

    public void selectTab(ActionBar.Tab tab) {
        FragmentTransaction fragmentTransaction;
        int i;
        ActionBar.Tab tab2 = tab;
        if (getNavigationMode() != 2) {
            if (tab2 != null) {
                i = tab2.getPosition();
            } else {
                i = -1;
            }
            this.mSavedTabPosition = i;
            return;
        }
        if (!(this.mActivity instanceof FragmentActivity) || this.mDecorToolbar.getViewGroup().isInEditMode()) {
            fragmentTransaction = null;
        } else {
            fragmentTransaction = ((FragmentActivity) this.mActivity).getSupportFragmentManager().beginTransaction().disallowAddToBackStack();
        }
        if (this.mSelectedTab != tab2) {
            this.mTabScrollView.setTabSelected(tab2 != null ? tab2.getPosition() : -1);
            if (this.mSelectedTab != null) {
                this.mSelectedTab.getCallback().onTabUnselected(this.mSelectedTab, fragmentTransaction);
            }
            this.mSelectedTab = (TabImpl) tab2;
            if (this.mSelectedTab != null) {
                this.mSelectedTab.getCallback().onTabSelected(this.mSelectedTab, fragmentTransaction);
            }
        } else if (this.mSelectedTab != null) {
            this.mSelectedTab.getCallback().onTabReselected(this.mSelectedTab, fragmentTransaction);
            this.mTabScrollView.animateToTab(tab2.getPosition());
        }
        if (fragmentTransaction != null && !fragmentTransaction.isEmpty()) {
            int commit = fragmentTransaction.commit();
        }
    }

    public ActionBar.Tab getSelectedTab() {
        return this.mSelectedTab;
    }

    public int getHeight() {
        return this.mContainerView.getHeight();
    }

    public void enableContentAnimations(boolean z) {
        this.mContentAnimations = z;
    }

    public void show() {
        if (this.mHiddenByApp) {
            this.mHiddenByApp = false;
            updateVisibility(false);
        }
    }

    private void showForActionMode() {
        if (!this.mShowingForMode) {
            this.mShowingForMode = true;
            if (this.mOverlayLayout != null) {
                this.mOverlayLayout.setShowingForActionMode(true);
            }
            updateVisibility(false);
        }
    }

    public void showForSystem() {
        if (this.mHiddenBySystem) {
            this.mHiddenBySystem = false;
            updateVisibility(true);
        }
    }

    public void hide() {
        if (!this.mHiddenByApp) {
            this.mHiddenByApp = true;
            updateVisibility(false);
        }
    }

    private void hideForActionMode() {
        if (this.mShowingForMode) {
            this.mShowingForMode = false;
            if (this.mOverlayLayout != null) {
                this.mOverlayLayout.setShowingForActionMode(false);
            }
            updateVisibility(false);
        }
    }

    public void hideForSystem() {
        if (!this.mHiddenBySystem) {
            this.mHiddenBySystem = true;
            updateVisibility(true);
        }
    }

    public void setHideOnContentScrollEnabled(boolean z) {
        Throwable th;
        boolean z2 = z;
        if (!z2 || this.mOverlayLayout.isInOverlayMode()) {
            this.mHideOnContentScroll = z2;
            this.mOverlayLayout.setHideOnContentScrollEnabled(z2);
            return;
        }
        Throwable th2 = th;
        new IllegalStateException("Action bar must be in overlay mode (Window.FEATURE_OVERLAY_ACTION_BAR) to enable hide on content scroll");
        throw th2;
    }

    public boolean isHideOnContentScrollEnabled() {
        return this.mOverlayLayout.isHideOnContentScrollEnabled();
    }

    public int getHideOffset() {
        return this.mOverlayLayout.getActionBarHideOffset();
    }

    public void setHideOffset(int i) {
        Throwable th;
        int i2 = i;
        if (i2 == 0 || this.mOverlayLayout.isInOverlayMode()) {
            this.mOverlayLayout.setActionBarHideOffset(i2);
            return;
        }
        Throwable th2 = th;
        new IllegalStateException("Action bar must be in overlay mode (Window.FEATURE_OVERLAY_ACTION_BAR) to set a non-zero hide offset");
        throw th2;
    }

    /* access modifiers changed from: private */
    public static boolean checkShowingFlags(boolean z, boolean z2, boolean z3) {
        boolean z4 = z;
        boolean z5 = z2;
        if (z3) {
            return true;
        }
        if (z4 || z5) {
            return false;
        }
        return true;
    }

    private void updateVisibility(boolean z) {
        boolean z2 = z;
        if (checkShowingFlags(this.mHiddenByApp, this.mHiddenBySystem, this.mShowingForMode)) {
            if (!this.mNowShowing) {
                this.mNowShowing = true;
                doShow(z2);
            }
        } else if (this.mNowShowing) {
            this.mNowShowing = false;
            doHide(z2);
        }
    }

    public void doShow(boolean z) {
        ViewPropertyAnimatorCompatSet viewPropertyAnimatorCompatSet;
        boolean z2 = z;
        if (this.mCurrentShowAnim != null) {
            this.mCurrentShowAnim.cancel();
        }
        this.mContainerView.setVisibility(0);
        if (this.mCurWindowVisibility != 0 || !ALLOW_SHOW_HIDE_ANIMATIONS || (!this.mShowHideAnimationEnabled && !z2)) {
            ViewCompat.setAlpha(this.mContainerView, 1.0f);
            ViewCompat.setTranslationY(this.mContainerView, 0.0f);
            if (this.mContentAnimations && this.mContentView != null) {
                ViewCompat.setTranslationY(this.mContentView, 0.0f);
            }
            this.mShowListener.onAnimationEnd(null);
        } else {
            ViewCompat.setTranslationY(this.mContainerView, 0.0f);
            float f = (float) (-this.mContainerView.getHeight());
            if (z2) {
                int[] iArr = {0, 0};
                this.mContainerView.getLocationInWindow(iArr);
                f -= (float) iArr[1];
            }
            ViewCompat.setTranslationY(this.mContainerView, f);
            new ViewPropertyAnimatorCompatSet();
            ViewPropertyAnimatorCompatSet viewPropertyAnimatorCompatSet2 = viewPropertyAnimatorCompatSet;
            ViewPropertyAnimatorCompat translationY = ViewCompat.animate(this.mContainerView).translationY(0.0f);
            ViewPropertyAnimatorCompat updateListener = translationY.setUpdateListener(this.mUpdateListener);
            ViewPropertyAnimatorCompatSet play = viewPropertyAnimatorCompatSet2.play(translationY);
            if (this.mContentAnimations && this.mContentView != null) {
                ViewCompat.setTranslationY(this.mContentView, f);
                ViewPropertyAnimatorCompatSet play2 = viewPropertyAnimatorCompatSet2.play(ViewCompat.animate(this.mContentView).translationY(0.0f));
            }
            ViewPropertyAnimatorCompatSet interpolator = viewPropertyAnimatorCompatSet2.setInterpolator(sShowInterpolator);
            ViewPropertyAnimatorCompatSet duration = viewPropertyAnimatorCompatSet2.setDuration(250);
            ViewPropertyAnimatorCompatSet listener = viewPropertyAnimatorCompatSet2.setListener(this.mShowListener);
            this.mCurrentShowAnim = viewPropertyAnimatorCompatSet2;
            viewPropertyAnimatorCompatSet2.start();
        }
        if (this.mOverlayLayout != null) {
            ViewCompat.requestApplyInsets(this.mOverlayLayout);
        }
    }

    public void doHide(boolean z) {
        ViewPropertyAnimatorCompatSet viewPropertyAnimatorCompatSet;
        boolean z2 = z;
        if (this.mCurrentShowAnim != null) {
            this.mCurrentShowAnim.cancel();
        }
        if (this.mCurWindowVisibility != 0 || !ALLOW_SHOW_HIDE_ANIMATIONS || (!this.mShowHideAnimationEnabled && !z2)) {
            this.mHideListener.onAnimationEnd(null);
            return;
        }
        ViewCompat.setAlpha(this.mContainerView, 1.0f);
        this.mContainerView.setTransitioning(true);
        new ViewPropertyAnimatorCompatSet();
        ViewPropertyAnimatorCompatSet viewPropertyAnimatorCompatSet2 = viewPropertyAnimatorCompatSet;
        float f = (float) (-this.mContainerView.getHeight());
        if (z2) {
            int[] iArr = {0, 0};
            this.mContainerView.getLocationInWindow(iArr);
            f -= (float) iArr[1];
        }
        ViewPropertyAnimatorCompat translationY = ViewCompat.animate(this.mContainerView).translationY(f);
        ViewPropertyAnimatorCompat updateListener = translationY.setUpdateListener(this.mUpdateListener);
        ViewPropertyAnimatorCompatSet play = viewPropertyAnimatorCompatSet2.play(translationY);
        if (this.mContentAnimations && this.mContentView != null) {
            ViewPropertyAnimatorCompatSet play2 = viewPropertyAnimatorCompatSet2.play(ViewCompat.animate(this.mContentView).translationY(f));
        }
        ViewPropertyAnimatorCompatSet interpolator = viewPropertyAnimatorCompatSet2.setInterpolator(sHideInterpolator);
        ViewPropertyAnimatorCompatSet duration = viewPropertyAnimatorCompatSet2.setDuration(250);
        ViewPropertyAnimatorCompatSet listener = viewPropertyAnimatorCompatSet2.setListener(this.mHideListener);
        this.mCurrentShowAnim = viewPropertyAnimatorCompatSet2;
        viewPropertyAnimatorCompatSet2.start();
    }

    public boolean isShowing() {
        int height = getHeight();
        return this.mNowShowing && (height == 0 || getHideOffset() < height);
    }

    public void animateToMode(boolean z) {
        ViewPropertyAnimatorCompat viewPropertyAnimatorCompat;
        ViewPropertyAnimatorCompat viewPropertyAnimatorCompat2;
        ViewPropertyAnimatorCompatSet viewPropertyAnimatorCompatSet;
        boolean z2 = z;
        if (z2) {
            showForActionMode();
        } else {
            hideForActionMode();
        }
        if (z2) {
            viewPropertyAnimatorCompat2 = this.mDecorToolbar.setupAnimatorToVisibility(4, FADE_OUT_DURATION_MS);
            viewPropertyAnimatorCompat = this.mContextView.setupAnimatorToVisibility(0, FADE_IN_DURATION_MS);
        } else {
            viewPropertyAnimatorCompat = this.mDecorToolbar.setupAnimatorToVisibility(0, FADE_IN_DURATION_MS);
            viewPropertyAnimatorCompat2 = this.mContextView.setupAnimatorToVisibility(8, FADE_OUT_DURATION_MS);
        }
        new ViewPropertyAnimatorCompatSet();
        ViewPropertyAnimatorCompatSet viewPropertyAnimatorCompatSet2 = viewPropertyAnimatorCompatSet;
        ViewPropertyAnimatorCompatSet playSequentially = viewPropertyAnimatorCompatSet2.playSequentially(viewPropertyAnimatorCompat2, viewPropertyAnimatorCompat);
        viewPropertyAnimatorCompatSet2.start();
    }

    public Context getThemedContext() {
        TypedValue typedValue;
        Context context;
        if (this.mThemedContext == null) {
            new TypedValue();
            TypedValue typedValue2 = typedValue;
            boolean resolveAttribute = this.mContext.getTheme().resolveAttribute(R.attr.actionBarWidgetTheme, typedValue2, true);
            int i = typedValue2.resourceId;
            if (i != 0) {
                new ContextThemeWrapper(this.mContext, i);
                this.mThemedContext = context;
            } else {
                this.mThemedContext = this.mContext;
            }
        }
        return this.mThemedContext;
    }

    public boolean isTitleTruncated() {
        return this.mDecorToolbar != null && this.mDecorToolbar.isTitleTruncated();
    }

    public void setHomeAsUpIndicator(Drawable drawable) {
        this.mDecorToolbar.setNavigationIcon(drawable);
    }

    public void setHomeAsUpIndicator(int i) {
        this.mDecorToolbar.setNavigationIcon(i);
    }

    public void setHomeActionContentDescription(CharSequence charSequence) {
        this.mDecorToolbar.setNavigationContentDescription(charSequence);
    }

    public void setHomeActionContentDescription(int i) {
        this.mDecorToolbar.setNavigationContentDescription(i);
    }

    public void onContentScrollStarted() {
        if (this.mCurrentShowAnim != null) {
            this.mCurrentShowAnim.cancel();
            this.mCurrentShowAnim = null;
        }
    }

    public void onContentScrollStopped() {
    }

    public boolean collapseActionView() {
        if (this.mDecorToolbar == null || !this.mDecorToolbar.hasExpandedActionView()) {
            return false;
        }
        this.mDecorToolbar.collapseActionView();
        return true;
    }

    public class ActionModeImpl extends ActionMode implements MenuBuilder.Callback {
        private final Context mActionModeContext;
        private ActionMode.Callback mCallback;
        private WeakReference<View> mCustomView;
        private final MenuBuilder mMenu;
        final /* synthetic */ WindowDecorActionBar this$0;

        public ActionModeImpl(WindowDecorActionBar windowDecorActionBar, Context context, ActionMode.Callback callback) {
            MenuBuilder menuBuilder;
            Context context2 = context;
            this.this$0 = windowDecorActionBar;
            this.mActionModeContext = context2;
            this.mCallback = callback;
            new MenuBuilder(context2);
            this.mMenu = menuBuilder.setDefaultShowAsAction(1);
            this.mMenu.setCallback(this);
        }

        public MenuInflater getMenuInflater() {
            MenuInflater menuInflater;
            new SupportMenuInflater(this.mActionModeContext);
            return menuInflater;
        }

        public Menu getMenu() {
            return this.mMenu;
        }

        public void finish() {
            if (this.this$0.mActionMode == this) {
                if (!WindowDecorActionBar.checkShowingFlags(this.this$0.mHiddenByApp, this.this$0.mHiddenBySystem, false)) {
                    this.this$0.mDeferredDestroyActionMode = this;
                    this.this$0.mDeferredModeDestroyCallback = this.mCallback;
                } else {
                    this.mCallback.onDestroyActionMode(this);
                }
                this.mCallback = null;
                this.this$0.animateToMode(false);
                this.this$0.mContextView.closeMode();
                this.this$0.mDecorToolbar.getViewGroup().sendAccessibilityEvent(32);
                this.this$0.mOverlayLayout.setHideOnContentScrollEnabled(this.this$0.mHideOnContentScroll);
                this.this$0.mActionMode = null;
            }
        }

        public void invalidate() {
            if (this.this$0.mActionMode == this) {
                this.mMenu.stopDispatchingItemsChanged();
                try {
                    boolean onPrepareActionMode = this.mCallback.onPrepareActionMode(this, this.mMenu);
                    this.mMenu.startDispatchingItemsChanged();
                } catch (Throwable th) {
                    Throwable th2 = th;
                    this.mMenu.startDispatchingItemsChanged();
                    throw th2;
                }
            }
        }

        /* JADX INFO: finally extract failed */
        public boolean dispatchOnCreate() {
            this.mMenu.stopDispatchingItemsChanged();
            try {
                boolean onCreateActionMode = this.mCallback.onCreateActionMode(this, this.mMenu);
                this.mMenu.startDispatchingItemsChanged();
                return onCreateActionMode;
            } catch (Throwable th) {
                Throwable th2 = th;
                this.mMenu.startDispatchingItemsChanged();
                throw th2;
            }
        }

        public void setCustomView(View view) {
            WeakReference<View> weakReference;
            View view2 = view;
            this.this$0.mContextView.setCustomView(view2);
            new WeakReference<>(view2);
            this.mCustomView = weakReference;
        }

        public void setSubtitle(CharSequence charSequence) {
            this.this$0.mContextView.setSubtitle(charSequence);
        }

        public void setTitle(CharSequence charSequence) {
            this.this$0.mContextView.setTitle(charSequence);
        }

        public void setTitle(int i) {
            setTitle(this.this$0.mContext.getResources().getString(i));
        }

        public void setSubtitle(int i) {
            setSubtitle(this.this$0.mContext.getResources().getString(i));
        }

        public CharSequence getTitle() {
            return this.this$0.mContextView.getTitle();
        }

        public CharSequence getSubtitle() {
            return this.this$0.mContextView.getSubtitle();
        }

        public void setTitleOptionalHint(boolean z) {
            boolean z2 = z;
            super.setTitleOptionalHint(z2);
            this.this$0.mContextView.setTitleOptional(z2);
        }

        public boolean isTitleOptional() {
            return this.this$0.mContextView.isTitleOptional();
        }

        public View getCustomView() {
            return this.mCustomView != null ? this.mCustomView.get() : null;
        }

        public boolean onMenuItemSelected(MenuBuilder menuBuilder, MenuItem menuItem) {
            MenuItem menuItem2 = menuItem;
            if (this.mCallback != null) {
                return this.mCallback.onActionItemClicked(this, menuItem2);
            }
            return false;
        }

        public void onCloseMenu(MenuBuilder menuBuilder, boolean z) {
        }

        public boolean onSubMenuSelected(SubMenuBuilder subMenuBuilder) {
            MenuPopupHelper menuPopupHelper;
            SubMenuBuilder subMenuBuilder2 = subMenuBuilder;
            if (this.mCallback == null) {
                return false;
            }
            if (!subMenuBuilder2.hasVisibleItems()) {
                return true;
            }
            new MenuPopupHelper(this.this$0.getThemedContext(), subMenuBuilder2);
            menuPopupHelper.show();
            return true;
        }

        public void onCloseSubMenu(SubMenuBuilder subMenuBuilder) {
        }

        public void onMenuModeChange(MenuBuilder menuBuilder) {
            if (this.mCallback != null) {
                invalidate();
                boolean showOverflowMenu = this.this$0.mContextView.showOverflowMenu();
            }
        }
    }

    public class TabImpl extends ActionBar.Tab {
        private ActionBar.TabListener mCallback;
        private CharSequence mContentDesc;
        private View mCustomView;
        private Drawable mIcon;
        private int mPosition = -1;
        private Object mTag;
        private CharSequence mText;

        public TabImpl() {
        }

        public Object getTag() {
            return this.mTag;
        }

        public ActionBar.Tab setTag(Object obj) {
            this.mTag = obj;
            return this;
        }

        public ActionBar.TabListener getCallback() {
            return this.mCallback;
        }

        public ActionBar.Tab setTabListener(ActionBar.TabListener tabListener) {
            this.mCallback = tabListener;
            return this;
        }

        public View getCustomView() {
            return this.mCustomView;
        }

        public ActionBar.Tab setCustomView(View view) {
            this.mCustomView = view;
            if (this.mPosition >= 0) {
                WindowDecorActionBar.this.mTabScrollView.updateTab(this.mPosition);
            }
            return this;
        }

        public ActionBar.Tab setCustomView(int i) {
            return setCustomView(LayoutInflater.from(WindowDecorActionBar.this.getThemedContext()).inflate(i, (ViewGroup) null));
        }

        public Drawable getIcon() {
            return this.mIcon;
        }

        public int getPosition() {
            return this.mPosition;
        }

        public void setPosition(int i) {
            this.mPosition = i;
        }

        public CharSequence getText() {
            return this.mText;
        }

        public ActionBar.Tab setIcon(Drawable drawable) {
            this.mIcon = drawable;
            if (this.mPosition >= 0) {
                WindowDecorActionBar.this.mTabScrollView.updateTab(this.mPosition);
            }
            return this;
        }

        public ActionBar.Tab setIcon(int i) {
            return setIcon(WindowDecorActionBar.this.getTintManager().getDrawable(i));
        }

        public ActionBar.Tab setText(CharSequence charSequence) {
            this.mText = charSequence;
            if (this.mPosition >= 0) {
                WindowDecorActionBar.this.mTabScrollView.updateTab(this.mPosition);
            }
            return this;
        }

        public ActionBar.Tab setText(int i) {
            return setText(WindowDecorActionBar.this.mContext.getResources().getText(i));
        }

        public void select() {
            WindowDecorActionBar.this.selectTab(this);
        }

        public ActionBar.Tab setContentDescription(int i) {
            return setContentDescription(WindowDecorActionBar.this.mContext.getResources().getText(i));
        }

        public ActionBar.Tab setContentDescription(CharSequence charSequence) {
            this.mContentDesc = charSequence;
            if (this.mPosition >= 0) {
                WindowDecorActionBar.this.mTabScrollView.updateTab(this.mPosition);
            }
            return this;
        }

        public CharSequence getContentDescription() {
            return this.mContentDesc;
        }
    }

    public void setCustomView(View view) {
        this.mDecorToolbar.setCustomView(view);
    }

    public void setCustomView(View view, ActionBar.LayoutParams layoutParams) {
        View view2 = view;
        view2.setLayoutParams(layoutParams);
        this.mDecorToolbar.setCustomView(view2);
    }

    public void setListNavigationCallbacks(SpinnerAdapter spinnerAdapter, ActionBar.OnNavigationListener onNavigationListener) {
        AdapterView.OnItemSelectedListener onItemSelectedListener;
        new NavItemSelectedListener(onNavigationListener);
        this.mDecorToolbar.setDropdownParams(spinnerAdapter, onItemSelectedListener);
    }

    public int getSelectedNavigationIndex() {
        switch (this.mDecorToolbar.getNavigationMode()) {
            case 1:
                return this.mDecorToolbar.getDropdownSelectedPosition();
            case 2:
                return this.mSelectedTab != null ? this.mSelectedTab.getPosition() : -1;
            default:
                return -1;
        }
    }

    public int getNavigationItemCount() {
        switch (this.mDecorToolbar.getNavigationMode()) {
            case 1:
                return this.mDecorToolbar.getDropdownItemCount();
            case 2:
                return this.mTabs.size();
            default:
                return 0;
        }
    }

    public int getTabCount() {
        return this.mTabs.size();
    }

    public void setNavigationMode(int i) {
        int i2 = i;
        int navigationMode = this.mDecorToolbar.getNavigationMode();
        switch (navigationMode) {
            case 2:
                this.mSavedTabPosition = getSelectedNavigationIndex();
                selectTab(null);
                this.mTabScrollView.setVisibility(8);
                break;
        }
        if (!(navigationMode == i2 || this.mHasEmbeddedTabs || this.mOverlayLayout == null)) {
            ViewCompat.requestApplyInsets(this.mOverlayLayout);
        }
        this.mDecorToolbar.setNavigationMode(i2);
        switch (i2) {
            case 2:
                ensureTabsExist();
                this.mTabScrollView.setVisibility(0);
                if (this.mSavedTabPosition != -1) {
                    setSelectedNavigationItem(this.mSavedTabPosition);
                    this.mSavedTabPosition = -1;
                    break;
                }
                break;
        }
        this.mDecorToolbar.setCollapsible(i2 == 2 && !this.mHasEmbeddedTabs);
        this.mOverlayLayout.setHasNonEmbeddedTabs(i2 == 2 && !this.mHasEmbeddedTabs);
    }

    public ActionBar.Tab getTabAt(int i) {
        return this.mTabs.get(i);
    }

    public void setIcon(int i) {
        this.mDecorToolbar.setIcon(i);
    }

    public void setIcon(Drawable drawable) {
        this.mDecorToolbar.setIcon(drawable);
    }

    public boolean hasIcon() {
        return this.mDecorToolbar.hasIcon();
    }

    public void setLogo(int i) {
        this.mDecorToolbar.setLogo(i);
    }

    public void setLogo(Drawable drawable) {
        this.mDecorToolbar.setLogo(drawable);
    }

    public boolean hasLogo() {
        return this.mDecorToolbar.hasLogo();
    }

    public void setDefaultDisplayHomeAsUpEnabled(boolean z) {
        boolean z2 = z;
        if (!this.mDisplayHomeAsUpSet) {
            setDisplayHomeAsUpEnabled(z2);
        }
    }

    /* access modifiers changed from: package-private */
    public TintManager getTintManager() {
        if (this.mTintManager == null) {
            this.mTintManager = TintManager.get(this.mContext);
        }
        return this.mTintManager;
    }
}
