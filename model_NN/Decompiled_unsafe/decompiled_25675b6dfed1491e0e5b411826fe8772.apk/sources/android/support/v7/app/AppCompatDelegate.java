package android.support.v7.app;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

public abstract class AppCompatDelegate {
    public static final int FEATURE_ACTION_MODE_OVERLAY = 10;
    public static final int FEATURE_SUPPORT_ACTION_BAR = 108;
    public static final int FEATURE_SUPPORT_ACTION_BAR_OVERLAY = 109;
    static final String TAG = "AppCompatDelegate";

    public abstract void addContentView(View view, ViewGroup.LayoutParams layoutParams);

    public abstract View createView(View view, String str, @NonNull Context context, @NonNull AttributeSet attributeSet);

    public abstract ActionBarDrawerToggle.Delegate getDrawerToggleDelegate();

    public abstract MenuInflater getMenuInflater();

    public abstract ActionBar getSupportActionBar();

    public abstract boolean hasWindowFeature(int i);

    public abstract void installViewFactory();

    public abstract void invalidateOptionsMenu();

    public abstract boolean isHandleNativeActionModesEnabled();

    public abstract void onConfigurationChanged(Configuration configuration);

    public abstract void onCreate(Bundle bundle);

    public abstract void onDestroy();

    public abstract void onPostCreate(Bundle bundle);

    public abstract void onPostResume();

    public abstract void onStop();

    public abstract boolean requestWindowFeature(int i);

    public abstract void setContentView(@LayoutRes int i);

    public abstract void setContentView(View view);

    public abstract void setContentView(View view, ViewGroup.LayoutParams layoutParams);

    public abstract void setHandleNativeActionModesEnabled(boolean z);

    public abstract void setSupportActionBar(Toolbar toolbar);

    public abstract void setTitle(CharSequence charSequence);

    public abstract ActionMode startSupportActionMode(ActionMode.Callback callback);

    public static AppCompatDelegate create(Activity activity, AppCompatCallback appCompatCallback) {
        Activity activity2 = activity;
        return create(activity2, activity2.getWindow(), appCompatCallback);
    }

    public static AppCompatDelegate create(Dialog dialog, AppCompatCallback appCompatCallback) {
        Dialog dialog2 = dialog;
        return create(dialog2.getContext(), dialog2.getWindow(), appCompatCallback);
    }

    private static AppCompatDelegate create(Context context, Window window, AppCompatCallback appCompatCallback) {
        AppCompatDelegate appCompatDelegate;
        AppCompatDelegate appCompatDelegate2;
        AppCompatDelegate appCompatDelegate3;
        AppCompatDelegate appCompatDelegate4;
        Context context2 = context;
        Window window2 = window;
        AppCompatCallback appCompatCallback2 = appCompatCallback;
        int i = Build.VERSION.SDK_INT;
        if (i >= 23) {
            new AppCompatDelegateImplV23(context2, window2, appCompatCallback2);
            return appCompatDelegate4;
        } else if (i >= 14) {
            new AppCompatDelegateImplV14(context2, window2, appCompatCallback2);
            return appCompatDelegate3;
        } else if (i >= 11) {
            new AppCompatDelegateImplV11(context2, window2, appCompatCallback2);
            return appCompatDelegate2;
        } else {
            new AppCompatDelegateImplV7(context2, window2, appCompatCallback2);
            return appCompatDelegate;
        }
    }

    AppCompatDelegate() {
    }
}
