package mzmr.iiyma.ssrxp;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int ic_fbi = 2130837504;
        public static final int logo = 2130837505;
        public static final int overlay_back = 2130837506;
    }

    public static final class id {
        public static final int webview = 2131165185;
        public static final int webview1 = 2131165184;
    }

    public static final class layout {
        public static final int main = 2130903040;
        public static final int overlay = 2130903041;
    }

    public static final class string {
        public static final int app_name = 2131034112;
        public static final int message_notification = 2131034115;
        public static final int title_notification = 2131034114;
        public static final int xxx = 2131034113;
    }

    public static final class style {
        public static final int AppBaseTheme = 2131099648;
        public static final int AppTheme = 2131099649;
    }

    public static final class xml {
        public static final int device_admin_data = 2130968576;
    }
}
