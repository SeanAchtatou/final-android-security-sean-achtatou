package com.google.devtools.simple.runtime.components.android.util;

import android.os.Handler;
import android.util.Log;
import android.widget.Toast;
import com.google.devtools.simple.runtime.components.android.Form;
import com.google.devtools.simple.runtime.components.android.collect.Lists;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.List;

public class ReplCommController {
    public static final int BLOCKS_EDITOR_PORT = 9997;
    private static final String LOG_TAG = "REPL Controller";
    private REPLServerController blocksEditorReplController;
    private boolean everStarted = false;
    /* access modifiers changed from: private */
    public Form form;
    private final Handler handler;

    public ReplCommController(Form form2) {
        this.form = form2;
        this.handler = new Handler();
        this.blocksEditorReplController = new REPLServerController(BLOCKS_EDITOR_PORT);
    }

    public void stopListening(boolean showAlert) {
    }

    public void destroy() {
        this.blocksEditorReplController.StopServer();
    }

    public void startListening(boolean showAlert) {
        if (!this.everStarted && !this.blocksEditorReplController.ServerRunning()) {
            this.blocksEditorReplController.StartServer();
            this.everStarted = true;
            if (showAlert) {
                ShowAlert("Listening to App Inventor. Click \"Restart app on device\" in the Blocks Editor  if you don't eventually see your components.");
            }
        }
    }

    private void ShowAlert(final String notice) {
        this.handler.post(new Runnable() {
            public void run() {
                Toast.makeText(ReplCommController.this.form, notice, 1).show();
            }
        });
    }

    private class REPLServerController {
        /* access modifiers changed from: private */
        public final Object lock = new Object();
        /* access modifiers changed from: private */
        public List<Socket> openClientSockets;
        /* access modifiers changed from: private */
        public int port;
        /* access modifiers changed from: private */
        public Thread serverThread;
        /* access modifiers changed from: private */
        public ServerSocket socket;

        public REPLServerController(int port2) {
            this.port = port2;
            this.socket = null;
            this.serverThread = null;
            this.openClientSockets = Lists.newArrayList();
        }

        public void StartServer() {
            closeSockets();
            this.serverThread = createServerThread();
            if (this.serverThread != null) {
                this.serverThread.start();
            }
        }

        public void StopServer() {
            Log.d(ReplCommController.LOG_TAG, "Stopping server on port " + this.port);
            this.serverThread = null;
            closeSockets();
        }

        public boolean ServerRunning() {
            return this.serverThread != null && this.serverThread.isAlive();
        }

        private Thread createServerThread() {
            return new Thread(new Runnable() {
                /* JADX WARNING: Removed duplicated region for block: B:41:0x00a9 A[SYNTHETIC] */
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public void run() {
                    /*
                        r11 = this;
                        r10 = 0
                        r2 = 0
                        r4 = 0
                        java.net.ServerSocket r3 = new java.net.ServerSocket     // Catch:{ InterruptedException -> 0x0121, IOException -> 0x011f }
                        r3.<init>()     // Catch:{ InterruptedException -> 0x0121, IOException -> 0x011f }
                        r6 = 1
                        r3.setReuseAddress(r6)     // Catch:{ InterruptedException -> 0x008c, IOException -> 0x00a0, all -> 0x011c }
                        java.net.InetSocketAddress r7 = new java.net.InetSocketAddress     // Catch:{ InterruptedException -> 0x008c, IOException -> 0x00a0, all -> 0x011c }
                        r6 = 0
                        java.net.InetAddress r6 = (java.net.InetAddress) r6     // Catch:{ InterruptedException -> 0x008c, IOException -> 0x00a0, all -> 0x011c }
                        com.google.devtools.simple.runtime.components.android.util.ReplCommController$REPLServerController r8 = com.google.devtools.simple.runtime.components.android.util.ReplCommController.REPLServerController.this     // Catch:{ InterruptedException -> 0x008c, IOException -> 0x00a0, all -> 0x011c }
                        int r8 = r8.port     // Catch:{ InterruptedException -> 0x008c, IOException -> 0x00a0, all -> 0x011c }
                        r7.<init>(r6, r8)     // Catch:{ InterruptedException -> 0x008c, IOException -> 0x00a0, all -> 0x011c }
                        r3.bind(r7)     // Catch:{ InterruptedException -> 0x008c, IOException -> 0x00a0, all -> 0x011c }
                        com.google.devtools.simple.runtime.components.android.util.ReplCommController$REPLServerController r6 = com.google.devtools.simple.runtime.components.android.util.ReplCommController.REPLServerController.this     // Catch:{ InterruptedException -> 0x008c, IOException -> 0x00a0, all -> 0x011c }
                        java.lang.Object r7 = r6.lock     // Catch:{ InterruptedException -> 0x008c, IOException -> 0x00a0, all -> 0x011c }
                        monitor-enter(r7)     // Catch:{ InterruptedException -> 0x008c, IOException -> 0x00a0, all -> 0x011c }
                        com.google.devtools.simple.runtime.components.android.util.ReplCommController$REPLServerController r6 = com.google.devtools.simple.runtime.components.android.util.ReplCommController.REPLServerController.this     // Catch:{ all -> 0x0089 }
                        java.net.ServerSocket unused = r6.socket = r3     // Catch:{ all -> 0x0089 }
                        monitor-exit(r7)     // Catch:{ all -> 0x0089 }
                        java.lang.String r6 = "REPL Controller"
                        java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ InterruptedException -> 0x008c, IOException -> 0x00a0, all -> 0x011c }
                        r7.<init>()     // Catch:{ InterruptedException -> 0x008c, IOException -> 0x00a0, all -> 0x011c }
                        java.lang.String r8 = "Starting a REPL Server thread on port "
                        java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ InterruptedException -> 0x008c, IOException -> 0x00a0, all -> 0x011c }
                        com.google.devtools.simple.runtime.components.android.util.ReplCommController$REPLServerController r8 = com.google.devtools.simple.runtime.components.android.util.ReplCommController.REPLServerController.this     // Catch:{ InterruptedException -> 0x008c, IOException -> 0x00a0, all -> 0x011c }
                        int r8 = r8.port     // Catch:{ InterruptedException -> 0x008c, IOException -> 0x00a0, all -> 0x011c }
                        java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ InterruptedException -> 0x008c, IOException -> 0x00a0, all -> 0x011c }
                        java.lang.String r7 = r7.toString()     // Catch:{ InterruptedException -> 0x008c, IOException -> 0x00a0, all -> 0x011c }
                        android.util.Log.d(r6, r7)     // Catch:{ InterruptedException -> 0x008c, IOException -> 0x00a0, all -> 0x011c }
                        java.lang.Thread r4 = java.lang.Thread.currentThread()     // Catch:{ InterruptedException -> 0x008c, IOException -> 0x00a0, all -> 0x011c }
                        com.google.devtools.simple.runtime.components.android.util.ReplCommController$REPLServerController r6 = com.google.devtools.simple.runtime.components.android.util.ReplCommController.REPLServerController.this     // Catch:{ InterruptedException -> 0x008c, IOException -> 0x00a0, all -> 0x011c }
                        java.lang.Thread r6 = r6.serverThread     // Catch:{ InterruptedException -> 0x008c, IOException -> 0x00a0, all -> 0x011c }
                        if (r6 != r4) goto L_0x0079
                        gnu.expr.ModuleExp.mustNeverCompile()     // Catch:{ InterruptedException -> 0x008c, IOException -> 0x00a0, all -> 0x011c }
                        java.net.Socket r0 = r3.accept()     // Catch:{ InterruptedException -> 0x008c, IOException -> 0x00a0, all -> 0x011c }
                        com.google.devtools.simple.runtime.components.android.util.ReplCommController$REPLServerController r6 = com.google.devtools.simple.runtime.components.android.util.ReplCommController.REPLServerController.this     // Catch:{ InterruptedException -> 0x008c, IOException -> 0x00a0, all -> 0x011c }
                        java.lang.Object r7 = r6.lock     // Catch:{ InterruptedException -> 0x008c, IOException -> 0x00a0, all -> 0x011c }
                        monitor-enter(r7)     // Catch:{ InterruptedException -> 0x008c, IOException -> 0x00a0, all -> 0x011c }
                        com.google.devtools.simple.runtime.components.android.util.ReplCommController$REPLServerController r6 = com.google.devtools.simple.runtime.components.android.util.ReplCommController.REPLServerController.this     // Catch:{ all -> 0x009d }
                        java.util.List r6 = r6.openClientSockets     // Catch:{ all -> 0x009d }
                        r6.add(r0)     // Catch:{ all -> 0x009d }
                        monitor-exit(r7)     // Catch:{ all -> 0x009d }
                        java.lang.String r6 = "scheme"
                        gnu.expr.Language r6 = kawa.standard.Scheme.getInstance(r6)     // Catch:{ InterruptedException -> 0x008c, IOException -> 0x00a0, all -> 0x011c }
                        java.lang.Thread r5 = com.google.devtools.simple.runtime.components.android.util.TelnetRepl.serve(r6, r0)     // Catch:{ InterruptedException -> 0x008c, IOException -> 0x00a0, all -> 0x011c }
                        r5.join()     // Catch:{ InterruptedException -> 0x008c, IOException -> 0x00a0, all -> 0x011c }
                    L_0x0079:
                        com.google.devtools.simple.runtime.components.android.util.ReplCommController$REPLServerController r6 = com.google.devtools.simple.runtime.components.android.util.ReplCommController.REPLServerController.this
                        com.google.devtools.simple.runtime.components.android.util.ReplCommController r6 = com.google.devtools.simple.runtime.components.android.util.ReplCommController.this
                        com.google.devtools.simple.runtime.components.android.Form r6 = r6.form
                        r6.finish()
                        java.lang.System.exit(r10)
                        r2 = r3
                    L_0x0088:
                        return
                    L_0x0089:
                        r6 = move-exception
                        monitor-exit(r7)     // Catch:{ all -> 0x0089 }
                        throw r6     // Catch:{ InterruptedException -> 0x008c, IOException -> 0x00a0, all -> 0x011c }
                    L_0x008c:
                        r6 = move-exception
                        r2 = r3
                    L_0x008e:
                        com.google.devtools.simple.runtime.components.android.util.ReplCommController$REPLServerController r6 = com.google.devtools.simple.runtime.components.android.util.ReplCommController.REPLServerController.this
                        com.google.devtools.simple.runtime.components.android.util.ReplCommController r6 = com.google.devtools.simple.runtime.components.android.util.ReplCommController.this
                        com.google.devtools.simple.runtime.components.android.Form r6 = r6.form
                        r6.finish()
                        java.lang.System.exit(r10)
                        goto L_0x0088
                    L_0x009d:
                        r6 = move-exception
                        monitor-exit(r7)     // Catch:{ all -> 0x009d }
                        throw r6     // Catch:{ InterruptedException -> 0x008c, IOException -> 0x00a0, all -> 0x011c }
                    L_0x00a0:
                        r1 = move-exception
                        r2 = r3
                    L_0x00a2:
                        com.google.devtools.simple.runtime.components.android.util.ReplCommController$REPLServerController r6 = com.google.devtools.simple.runtime.components.android.util.ReplCommController.REPLServerController.this     // Catch:{ all -> 0x010c }
                        java.lang.Object r7 = r6.lock     // Catch:{ all -> 0x010c }
                        monitor-enter(r7)     // Catch:{ all -> 0x010c }
                        com.google.devtools.simple.runtime.components.android.util.ReplCommController$REPLServerController r6 = com.google.devtools.simple.runtime.components.android.util.ReplCommController.REPLServerController.this     // Catch:{ all -> 0x0109 }
                        java.net.ServerSocket r6 = r6.socket     // Catch:{ all -> 0x0109 }
                        if (r6 == 0) goto L_0x00f9
                        com.google.devtools.simple.runtime.components.android.util.ReplCommController$REPLServerController r6 = com.google.devtools.simple.runtime.components.android.util.ReplCommController.REPLServerController.this     // Catch:{ all -> 0x0109 }
                        java.net.ServerSocket r6 = r6.socket     // Catch:{ all -> 0x0109 }
                        if (r6 != r2) goto L_0x00f9
                        java.lang.String r6 = "REPL Controller"
                        java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ all -> 0x0109 }
                        r8.<init>()     // Catch:{ all -> 0x0109 }
                        java.lang.String r9 = "IOException with server socket on port "
                        java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ all -> 0x0109 }
                        com.google.devtools.simple.runtime.components.android.util.ReplCommController$REPLServerController r9 = com.google.devtools.simple.runtime.components.android.util.ReplCommController.REPLServerController.this     // Catch:{ all -> 0x0109 }
                        int r9 = r9.port     // Catch:{ all -> 0x0109 }
                        java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ all -> 0x0109 }
                        java.lang.String r9 = ", closing sockets"
                        java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ all -> 0x0109 }
                        java.lang.String r8 = r8.toString()     // Catch:{ all -> 0x0109 }
                        android.util.Log.d(r6, r8)     // Catch:{ all -> 0x0109 }
                        java.lang.String r6 = "REPL Controller"
                        java.lang.String r8 = android.util.Log.getStackTraceString(r1)     // Catch:{ all -> 0x0109 }
                        android.util.Log.d(r6, r8)     // Catch:{ all -> 0x0109 }
                        com.google.devtools.simple.runtime.components.android.util.ReplCommController$REPLServerController r6 = com.google.devtools.simple.runtime.components.android.util.ReplCommController.REPLServerController.this     // Catch:{ all -> 0x0109 }
                        r6.closeSockets()     // Catch:{ all -> 0x0109 }
                        com.google.devtools.simple.runtime.components.android.util.ReplCommController$REPLServerController r6 = com.google.devtools.simple.runtime.components.android.util.ReplCommController.REPLServerController.this     // Catch:{ all -> 0x0109 }
                        java.lang.Thread r6 = r6.serverThread     // Catch:{ all -> 0x0109 }
                        if (r6 != r4) goto L_0x00f9
                        com.google.devtools.simple.runtime.components.android.util.ReplCommController$REPLServerController r6 = com.google.devtools.simple.runtime.components.android.util.ReplCommController.REPLServerController.this     // Catch:{ all -> 0x0109 }
                        r8 = 0
                        java.lang.Thread unused = r6.serverThread = r8     // Catch:{ all -> 0x0109 }
                    L_0x00f9:
                        monitor-exit(r7)     // Catch:{ all -> 0x0109 }
                        com.google.devtools.simple.runtime.components.android.util.ReplCommController$REPLServerController r6 = com.google.devtools.simple.runtime.components.android.util.ReplCommController.REPLServerController.this
                        com.google.devtools.simple.runtime.components.android.util.ReplCommController r6 = com.google.devtools.simple.runtime.components.android.util.ReplCommController.this
                        com.google.devtools.simple.runtime.components.android.Form r6 = r6.form
                        r6.finish()
                        java.lang.System.exit(r10)
                        goto L_0x0088
                    L_0x0109:
                        r6 = move-exception
                        monitor-exit(r7)     // Catch:{ all -> 0x0109 }
                        throw r6     // Catch:{ all -> 0x010c }
                    L_0x010c:
                        r6 = move-exception
                    L_0x010d:
                        com.google.devtools.simple.runtime.components.android.util.ReplCommController$REPLServerController r7 = com.google.devtools.simple.runtime.components.android.util.ReplCommController.REPLServerController.this
                        com.google.devtools.simple.runtime.components.android.util.ReplCommController r7 = com.google.devtools.simple.runtime.components.android.util.ReplCommController.this
                        com.google.devtools.simple.runtime.components.android.Form r7 = r7.form
                        r7.finish()
                        java.lang.System.exit(r10)
                        throw r6
                    L_0x011c:
                        r6 = move-exception
                        r2 = r3
                        goto L_0x010d
                    L_0x011f:
                        r1 = move-exception
                        goto L_0x00a2
                    L_0x0121:
                        r6 = move-exception
                        goto L_0x008e
                    */
                    throw new UnsupportedOperationException("Method not decompiled: com.google.devtools.simple.runtime.components.android.util.ReplCommController.REPLServerController.AnonymousClass1.run():void");
                }
            });
        }

        /* JADX INFO: finally extract failed */
        /* access modifiers changed from: private */
        public void closeSockets() {
            synchronized (this.lock) {
                if (this.socket != null) {
                    Log.d(ReplCommController.LOG_TAG, "Trying to close server sockets for port " + this.port);
                    try {
                        this.socket.close();
                        this.socket = null;
                        for (Socket openClientSocket : this.openClientSockets) {
                            try {
                                openClientSocket.close();
                                this.openClientSockets = Lists.newArrayList();
                            } catch (IOException e) {
                                Log.d(ReplCommController.LOG_TAG, "IOException closing client socket on port " + this.port);
                                Log.d(ReplCommController.LOG_TAG, Log.getStackTraceString(e));
                                this.openClientSockets = Lists.newArrayList();
                            } catch (Throwable th) {
                                this.openClientSockets = Lists.newArrayList();
                                throw th;
                            }
                        }
                    } catch (IOException e2) {
                        Log.d(ReplCommController.LOG_TAG, "IOException closing server socket on port " + this.port);
                        Log.d(ReplCommController.LOG_TAG, Log.getStackTraceString(e2));
                        this.socket = null;
                        for (Socket openClientSocket2 : this.openClientSockets) {
                            try {
                                openClientSocket2.close();
                                this.openClientSockets = Lists.newArrayList();
                            } catch (IOException e3) {
                                Log.d(ReplCommController.LOG_TAG, "IOException closing client socket on port " + this.port);
                                Log.d(ReplCommController.LOG_TAG, Log.getStackTraceString(e3));
                                this.openClientSockets = Lists.newArrayList();
                            } catch (Throwable th2) {
                                this.openClientSockets = Lists.newArrayList();
                                throw th2;
                            }
                        }
                    } catch (Throwable th3) {
                        this.socket = null;
                        for (Socket openClientSocket3 : this.openClientSockets) {
                            try {
                                openClientSocket3.close();
                                this.openClientSockets = Lists.newArrayList();
                            } catch (IOException e4) {
                                Log.d(ReplCommController.LOG_TAG, "IOException closing client socket on port " + this.port);
                                Log.d(ReplCommController.LOG_TAG, Log.getStackTraceString(e4));
                                this.openClientSockets = Lists.newArrayList();
                            } catch (Throwable th4) {
                                this.openClientSockets = Lists.newArrayList();
                                throw th4;
                            }
                        }
                        throw th3;
                    }
                }
            }
        }
    }
}
