package com.google.devtools.simple.runtime.components.android;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.provider.Contacts;
import android.text.TextUtils;
import android.text.util.Rfc822Token;
import android.view.View;
import android.widget.ResourceCursorAdapter;
import android.widget.TextView;

public class EmailAddressAdapter extends ResourceCursorAdapter {
    public static final int DATA_INDEX = 2;
    public static final int NAME_INDEX = 1;
    private static final String[] PROJECTION = {"_id", "name", "data"};
    private static final String SORT_ORDER = "times_contacted DESC, name";
    private ContentResolver contentResolver;

    public EmailAddressAdapter(Context context) {
        super(context, 17367050, null);
        this.contentResolver = context.getContentResolver();
    }

    public final String convertToString(Cursor cursor) {
        return new Rfc822Token(cursor.getString(1), cursor.getString(2), null).toString();
    }

    private final String makeDisplayString(Cursor cursor) {
        StringBuilder s = new StringBuilder();
        boolean flag = false;
        String name = cursor.getString(1);
        String address = cursor.getString(2);
        if (!TextUtils.isEmpty(name)) {
            s.append(name);
            flag = true;
        }
        if (flag) {
            s.append(" <");
        }
        s.append(address);
        if (flag) {
            s.append(">");
        }
        return s.toString();
    }

    public final void bindView(View view, Context context, Cursor cursor) {
        ((TextView) view).setText(makeDisplayString(cursor));
    }

    public Cursor runQueryOnBackgroundThread(CharSequence constraint) {
        String where = null;
        if (constraint != null) {
            String filter = DatabaseUtils.sqlEscapeString(constraint.toString() + '%');
            where = "(people.name LIKE " + filter + ") OR (contact_methods.data LIKE " + filter + ")";
        }
        return this.contentResolver.query(Contacts.ContactMethods.CONTENT_EMAIL_URI, PROJECTION, where, null, SORT_ORDER);
    }
}
