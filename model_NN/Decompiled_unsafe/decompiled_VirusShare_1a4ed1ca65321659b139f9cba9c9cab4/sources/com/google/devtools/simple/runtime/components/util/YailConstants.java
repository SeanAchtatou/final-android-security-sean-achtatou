package com.google.devtools.simple.runtime.components.util;

import gnu.mapping.SimpleSymbol;

public class YailConstants {
    public static final SimpleSymbol YAIL_HEADER = new SimpleSymbol("*list*");

    private YailConstants() {
    }
}
