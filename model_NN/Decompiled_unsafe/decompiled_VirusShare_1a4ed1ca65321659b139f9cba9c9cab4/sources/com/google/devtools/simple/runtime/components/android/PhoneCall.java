package com.google.devtools.simple.runtime.components.android;

import android.content.Context;
import com.google.devtools.simple.common.ComponentCategory;
import com.google.devtools.simple.common.PropertyCategory;
import com.google.devtools.simple.runtime.annotations.DesignerComponent;
import com.google.devtools.simple.runtime.annotations.DesignerProperty;
import com.google.devtools.simple.runtime.annotations.SimpleFunction;
import com.google.devtools.simple.runtime.annotations.SimpleObject;
import com.google.devtools.simple.runtime.annotations.SimpleProperty;
import com.google.devtools.simple.runtime.annotations.UsesPermissions;
import com.google.devtools.simple.runtime.components.Component;
import com.google.devtools.simple.runtime.components.android.util.PhoneCallUtil;
import gnu.kawa.xml.ElementType;

@SimpleObject
@UsesPermissions(permissionNames = "android.permission.CALL_PHONE")
@DesignerComponent(category = ComponentCategory.SOCIAL, description = "<p>A non-visible component that makes a phone call to the number specified in the <code>PhoneNumber</code> property, which can be set either in the Designer or Blocks Editor. The component has a <code>MakePhoneCall</code> method, enabling the program to launch a phone call.</p><p>Often, this component is used with the <code>ContactPicker</code> component, which lets the user select a contact from the ones stored on the phone and sets the <code>PhoneNumber</code> property to the contact's phone number.</p><p>To directly specify the phone number (e.g., 650-555-1212), set the <code>PhoneNumber</code> property to a Text with the specified digits (e.g., \"6505551212\").  Dashes, dots, and parentheses may be included (e.g., \"(650)-555-1212\") but will be ignored; spaces may not be included.</p>", iconName = "images/phoneCall.png", nonVisible = true, version = 1)
public class PhoneCall extends AndroidNonvisibleComponent implements Component {
    private final Context context;
    private String phoneNumber;

    public PhoneCall(ComponentContainer container) {
        super(container.$form());
        this.context = container.$context();
        PhoneNumber(ElementType.MATCH_ANY_LOCALNAME);
    }

    @SimpleProperty(category = PropertyCategory.BEHAVIOR)
    public String PhoneNumber() {
        return this.phoneNumber;
    }

    @SimpleProperty
    @DesignerProperty(defaultValue = ElementType.MATCH_ANY_LOCALNAME, editorType = DesignerProperty.PROPERTY_TYPE_STRING)
    public void PhoneNumber(String phoneNumber2) {
        this.phoneNumber = phoneNumber2;
    }

    @SimpleFunction
    public void MakePhoneCall() {
        PhoneCallUtil.makePhoneCall(this.context, this.phoneNumber);
    }
}
