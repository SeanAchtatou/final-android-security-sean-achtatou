package com.google.devtools.simple.runtime.components.android;

import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.google.devtools.simple.common.ComponentCategory;
import com.google.devtools.simple.common.PropertyCategory;
import com.google.devtools.simple.runtime.annotations.DesignerComponent;
import com.google.devtools.simple.runtime.annotations.DesignerProperty;
import com.google.devtools.simple.runtime.annotations.SimpleFunction;
import com.google.devtools.simple.runtime.annotations.SimpleObject;
import com.google.devtools.simple.runtime.annotations.SimpleProperty;
import com.google.devtools.simple.runtime.annotations.UsesPermissions;
import gnu.kawa.xml.ElementType;

@SimpleObject
@DesignerComponent(category = ComponentCategory.EXPERIMENTAL, description = "Component for viewing Web pages.  The Home URL can be specified in the Designer or in the Blocks Editor.  The view can be set to follow links when they are tapped, and users can fill in Web forms. Warning: This is not a full browser.  For example, pressing the phone's hardware Back key will exit the app, rather than move back in the browser history.", version = 2)
@UsesPermissions(permissionNames = "android.permission.INTERNET")
public final class WebViewer extends AndroidViewComponent {
    /* access modifiers changed from: private */
    public boolean followLinks = true;
    private String homeUrl;
    private final WebView webview;

    public WebViewer(ComponentContainer container) {
        super(container);
        this.webview = new WebView(container.$context());
        this.webview.setWebViewClient(new WebViewerClient());
        this.webview.getSettings().setJavaScriptEnabled(true);
        this.webview.setFocusable(true);
        this.webview.getSettings().setBuiltInZoomControls(true);
        container.$add(this);
        this.webview.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case 0:
                    case 1:
                        if (v.hasFocus()) {
                            return false;
                        }
                        v.requestFocus();
                        return false;
                    default:
                        return false;
                }
            }
        });
        HomeUrl(ElementType.MATCH_ANY_LOCALNAME);
        Width(-2);
        Height(-2);
    }

    public View getView() {
        return this.webview;
    }

    private class WebViewerClient extends WebViewClient {
        private WebViewerClient() {
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return !WebViewer.this.followLinks;
        }
    }

    @SimpleProperty
    public void Width(int width) {
        if (width == -1) {
            width = -2;
        }
        super.Width(width);
    }

    @SimpleProperty
    public void Height(int height) {
        if (height == -1) {
            height = -2;
        }
        super.Height(height);
    }

    @SimpleProperty(category = PropertyCategory.BEHAVIOR, description = "URL of the page the WebViewer should initially open to.  Setting this will load the page.")
    public String HomeUrl() {
        return this.homeUrl;
    }

    @SimpleProperty
    @DesignerProperty(defaultValue = ElementType.MATCH_ANY_LOCALNAME, editorType = DesignerProperty.PROPERTY_TYPE_STRING)
    public void HomeUrl(String url) {
        this.homeUrl = url;
        this.webview.clearHistory();
        this.webview.loadUrl(this.homeUrl);
    }

    @SimpleProperty(category = PropertyCategory.BEHAVIOR, description = "URL of the page currently viewed.   This could be different from the Home URL if new pages were visited by following links.")
    public String CurrentUrl() {
        return this.webview.getUrl();
    }

    @SimpleProperty(category = PropertyCategory.BEHAVIOR, description = "Title of the page currently viewed")
    public String CurrentPageTitle() {
        return this.webview.getTitle();
    }

    @SimpleProperty(category = PropertyCategory.BEHAVIOR, description = "Determines whether to follow links when they are tapped in the WebViewer.  If you follow links, you can use GoBack and GoForward to navigate the browser history. ")
    public boolean FollowLinks() {
        return this.followLinks;
    }

    @SimpleProperty
    @DesignerProperty(defaultValue = "True", editorType = DesignerProperty.PROPERTY_TYPE_BOOLEAN)
    public void FollowLinks(boolean follow) {
        this.followLinks = follow;
    }

    @SimpleFunction(description = "Loads the home URL page.  This happens automatically when the home URL is changed.")
    public void GoHome() {
        this.webview.loadUrl(this.homeUrl);
    }

    @SimpleFunction(description = "Go back to the previous page in the history list.  Does nothing if there is no previous page.")
    public void GoBack() {
        if (this.webview.canGoBack()) {
            this.webview.goBack();
        }
    }

    @SimpleFunction(description = "Go forward to the next page in the history list.   Does nothing if there is no next page.")
    public void GoForward() {
        if (this.webview.canGoForward()) {
            this.webview.goForward();
        }
    }

    @SimpleFunction(description = "Returns true if the WebViewer can go forward in the history list.")
    public boolean CanGoForward() {
        return this.webview.canGoForward();
    }

    @SimpleFunction(description = "Returns true if the WebViewer can go back in the history list.")
    public boolean CanGoBack() {
        return this.webview.canGoBack();
    }

    @SimpleFunction(description = "Load the page at the given URL.")
    public void GoToUrl(String url) {
        this.webview.loadUrl(url);
    }
}
