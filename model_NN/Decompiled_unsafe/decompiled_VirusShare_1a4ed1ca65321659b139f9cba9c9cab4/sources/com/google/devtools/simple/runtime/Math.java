package com.google.devtools.simple.runtime;

import com.google.devtools.simple.runtime.annotations.SimpleDataElement;
import com.google.devtools.simple.runtime.annotations.SimpleFunction;
import com.google.devtools.simple.runtime.annotations.SimpleObject;
import com.google.devtools.simple.runtime.variants.IntegerVariant;
import com.google.devtools.simple.runtime.variants.Variant;
import java.security.SecureRandom;
import java.util.Random;

@SimpleObject
public final class Math {
    @SimpleDataElement
    public static final double E = 2.718281828459045d;
    @SimpleDataElement
    public static final double PI = 3.141592653589793d;
    private static final Random randomGenerator = new SecureRandom();

    private Math() {
    }

    @SimpleFunction
    public static Variant Abs(Variant v) {
        if (v.cmp(IntegerVariant.getIntegerVariant(0)) < 0) {
            return v.mul(IntegerVariant.getIntegerVariant(-1));
        }
        return v;
    }

    @SimpleFunction
    public static double Atn(double v) {
        return Math.atan(v);
    }

    @SimpleFunction
    public static double Atn2(double y, double x) {
        return Math.atan2(y, x);
    }

    @SimpleFunction
    public static double Cos(double v) {
        return Math.cos(v);
    }

    @SimpleFunction
    public static double Exp(double v) {
        return Math.exp(v);
    }

    @SimpleFunction
    public static long Int(Variant v) {
        return v.getLong();
    }

    @SimpleFunction
    public static double Log(double v) {
        return Math.log(v);
    }

    @SimpleFunction
    public static Variant Max(Variant v1, Variant v2) {
        v1.getDouble();
        v2.getDouble();
        return v1.cmp(v2) < 0 ? v2 : v1;
    }

    @SimpleFunction
    public static Variant Min(Variant v1, Variant v2) {
        v1.getDouble();
        v2.getDouble();
        return v1.cmp(v2) >= 0 ? v2 : v1;
    }

    @SimpleFunction
    public static double Rnd() {
        return randomGenerator.nextDouble();
    }

    @SimpleFunction
    public static double Sin(double v) {
        return Math.sin(v);
    }

    @SimpleFunction
    public static int Sgn(double v) {
        return (int) Math.signum(v);
    }

    @SimpleFunction
    public static double Sqr(double v) {
        return Math.sqrt(v);
    }

    @SimpleFunction
    public static double Tan(double v) {
        return Math.tan(v);
    }

    @SimpleFunction
    public static double DegreesToRadians(double d) {
        return Math.toRadians(d);
    }

    @SimpleFunction
    public static double RadiansToDegrees(double r) {
        return Math.toDegrees(r);
    }
}
