package com.google.devtools.simple.runtime.components.android;

import android.app.Activity;
import android.view.View;
import com.google.android.googlelogin.GoogleLoginServiceConstants;
import com.google.devtools.simple.common.ComponentCategory;
import com.google.devtools.simple.runtime.annotations.DesignerComponent;
import com.google.devtools.simple.runtime.annotations.DesignerProperty;
import com.google.devtools.simple.runtime.annotations.SimpleObject;
import com.google.devtools.simple.runtime.annotations.SimpleProperty;
import com.google.devtools.simple.runtime.components.Component;
import com.google.devtools.simple.runtime.components.android.util.ViewUtil;

@SimpleObject
@DesignerComponent(category = ComponentCategory.ARRANGEMENTS, description = "<p>A formatting element in which to place components that should be displayed in tabular form.</p>", version = 1)
public class TableArrangement extends AndroidViewComponent implements Component, ComponentContainer {
    private final Activity context;
    private final TableLayout viewLayout = new TableLayout(this.context, 2, 2);

    public TableArrangement(ComponentContainer container) {
        super(container);
        this.context = container.$context();
        container.$add(this);
    }

    @SimpleProperty(userVisible = GoogleLoginServiceConstants.PREFER_HOSTED)
    public int Columns() {
        return this.viewLayout.getNumColumns();
    }

    @SimpleProperty(userVisible = GoogleLoginServiceConstants.PREFER_HOSTED)
    @DesignerProperty(defaultValue = "2", editorType = DesignerProperty.PROPERTY_TYPE_NON_NEGATIVE_INTEGER)
    public void Columns(int numColumns) {
        this.viewLayout.setNumColumns(numColumns);
    }

    @SimpleProperty(userVisible = GoogleLoginServiceConstants.PREFER_HOSTED)
    public int Rows() {
        return this.viewLayout.getNumRows();
    }

    @SimpleProperty(userVisible = GoogleLoginServiceConstants.PREFER_HOSTED)
    @DesignerProperty(defaultValue = "2", editorType = DesignerProperty.PROPERTY_TYPE_NON_NEGATIVE_INTEGER)
    public void Rows(int numRows) {
        this.viewLayout.setNumRows(numRows);
    }

    public Activity $context() {
        return this.context;
    }

    public Form $form() {
        return this.container.$form();
    }

    public void $add(AndroidViewComponent component) {
        this.viewLayout.add(component);
    }

    public void setChildWidth(AndroidViewComponent component, int width) {
        ViewUtil.setChildWidthForTableLayout(component.getView(), width);
    }

    public void setChildHeight(AndroidViewComponent component, int height) {
        ViewUtil.setChildHeightForTableLayout(component.getView(), height);
    }

    public View getView() {
        return this.viewLayout.getLayoutManager();
    }
}
