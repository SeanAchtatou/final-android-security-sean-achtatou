package com.google.devtools.simple.runtime.components;

import com.google.devtools.simple.runtime.annotations.SimpleObject;

@SimpleObject
public interface SensorComponent extends Component {
}
