package com.google.devtools.simple.runtime.annotations;

import com.google.devtools.simple.common.ComponentCategory;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface DesignerComponent {
    ComponentCategory category() default ComponentCategory.UNINITIALIZED;

    String description() default "";

    String designerHelpDescription() default "";

    String iconName() default "";

    boolean nonVisible() default false;

    boolean showOnPalette() default true;

    int version();
}
