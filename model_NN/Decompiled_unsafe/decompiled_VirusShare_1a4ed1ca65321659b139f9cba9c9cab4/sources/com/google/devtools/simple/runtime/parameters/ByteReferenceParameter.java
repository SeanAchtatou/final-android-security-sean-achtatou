package com.google.devtools.simple.runtime.parameters;

public final class ByteReferenceParameter extends ReferenceParameter {
    private byte value;

    public ByteReferenceParameter(byte value2) {
        set(value2);
    }

    public byte get() {
        return this.value;
    }

    public void set(byte value2) {
        this.value = value2;
    }
}
