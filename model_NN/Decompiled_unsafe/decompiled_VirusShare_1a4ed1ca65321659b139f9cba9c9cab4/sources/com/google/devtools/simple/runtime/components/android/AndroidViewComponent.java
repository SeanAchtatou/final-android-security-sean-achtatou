package com.google.devtools.simple.runtime.components.android;

import android.view.View;
import com.google.android.googlelogin.GoogleLoginServiceConstants;
import com.google.devtools.simple.common.PropertyCategory;
import com.google.devtools.simple.runtime.annotations.DesignerProperty;
import com.google.devtools.simple.runtime.annotations.SimpleObject;
import com.google.devtools.simple.runtime.annotations.SimpleProperty;
import com.google.devtools.simple.runtime.annotations.SimplePropertyCopier;
import com.google.devtools.simple.runtime.components.HandlesEventDispatching;
import com.google.devtools.simple.runtime.components.VisibleComponent;

@SimpleObject
public abstract class AndroidViewComponent extends VisibleComponent {
    private int column = -1;
    protected final ComponentContainer container;
    private int lastSetHeight = -3;
    private int lastSetWidth = -3;
    private int row = -1;

    public abstract View getView();

    protected AndroidViewComponent(ComponentContainer container2) {
        this.container = container2;
    }

    @SimpleProperty(category = PropertyCategory.APPEARANCE, description = "Whether the component is visible")
    public boolean Visible() {
        return getView().getVisibility() == 0;
    }

    @SimpleProperty
    @DesignerProperty(defaultValue = "True", editorType = DesignerProperty.PROPERTY_TYPE_BOOLEAN)
    public void Visible(boolean visible) {
        getView().setVisibility(visible ? 0 : 8);
    }

    @SimpleProperty
    public int Width() {
        return getView().getWidth();
    }

    @SimpleProperty
    public void Width(int width) {
        this.container.setChildWidth(this, width);
        this.lastSetWidth = width;
    }

    @SimplePropertyCopier
    public void CopyWidth(AndroidViewComponent sourceComponent) {
        Width(sourceComponent.lastSetWidth);
    }

    @SimpleProperty
    public int Height() {
        return getView().getHeight();
    }

    @SimpleProperty
    public void Height(int height) {
        this.container.setChildHeight(this, height);
        this.lastSetHeight = height;
    }

    @SimplePropertyCopier
    public void CopyHeight(AndroidViewComponent sourceComponent) {
        Height(sourceComponent.lastSetHeight);
    }

    @SimpleProperty(userVisible = GoogleLoginServiceConstants.PREFER_HOSTED)
    public int Column() {
        return this.column;
    }

    @SimpleProperty(userVisible = GoogleLoginServiceConstants.PREFER_HOSTED)
    public void Column(int column2) {
        this.column = column2;
    }

    @SimpleProperty(userVisible = GoogleLoginServiceConstants.PREFER_HOSTED)
    public int Row() {
        return this.row;
    }

    @SimpleProperty(userVisible = GoogleLoginServiceConstants.PREFER_HOSTED)
    public void Row(int row2) {
        this.row = row2;
    }

    public HandlesEventDispatching getDispatchDelegate() {
        return this.container.$form();
    }
}
