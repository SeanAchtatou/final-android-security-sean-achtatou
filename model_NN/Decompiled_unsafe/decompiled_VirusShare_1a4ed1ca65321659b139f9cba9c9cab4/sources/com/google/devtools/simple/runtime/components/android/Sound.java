package com.google.devtools.simple.runtime.components.android;

import android.media.SoundPool;
import android.os.Vibrator;
import android.util.Log;
import com.google.android.googlelogin.GoogleLoginServiceConstants;
import com.google.devtools.simple.common.ComponentCategory;
import com.google.devtools.simple.common.PropertyCategory;
import com.google.devtools.simple.runtime.annotations.DesignerComponent;
import com.google.devtools.simple.runtime.annotations.DesignerProperty;
import com.google.devtools.simple.runtime.annotations.SimpleEvent;
import com.google.devtools.simple.runtime.annotations.SimpleFunction;
import com.google.devtools.simple.runtime.annotations.SimpleObject;
import com.google.devtools.simple.runtime.annotations.SimpleProperty;
import com.google.devtools.simple.runtime.annotations.UsesPermissions;
import com.google.devtools.simple.runtime.components.Component;
import com.google.devtools.simple.runtime.components.android.util.MediaUtil;
import com.google.devtools.simple.runtime.components.util.ErrorMessages;
import gnu.kawa.xml.ElementType;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@SimpleObject
@UsesPermissions(permissionNames = "android.permission.VIBRATE, android.permission.INTERNET")
@DesignerComponent(category = ComponentCategory.MEDIA, description = "<p>A multimedia component that plays sound files and optionally vibrates for the number of milliseconds (thousandths of a second) specified in the Blocks Editor.  The name of the sound file to play can be specified either in the Designer or in the Blocks Editor.</p> <p>For legal sound and video formats, see <a href=\"http://developer.android.com/guide/appendix/media-formats.html\" target=\"_blank\">Android Supported Media Formats</a>.</p><p>This component is best for short sound files, such as sound effects, while the <code>Player</code> component is more efficient for longer sounds, such as songs.</p>", iconName = "images/soundEffect.png", nonVisible = true, version = 3)
public class Sound extends AndroidNonvisibleComponent implements Component, OnResumeListener, OnStopListener, OnDestroyListener, Deleteable {
    private static final int LOOP_MODE_NO_LOOP = 0;
    private static final int MAX_STREAMS = 10;
    private static final float PLAYBACK_RATE_NORMAL = 1.0f;
    private static final float VOLUME_FULL = 1.0f;
    private int minimumInterval;
    private int soundId;
    private final Map<String, Integer> soundMap = new HashMap();
    private SoundPool soundPool = new SoundPool(10, 3, 0);
    private String sourcePath = ElementType.MATCH_ANY_LOCALNAME;
    private int streamId;
    private long timeLastPlayed;
    private final Vibrator vibe = ((Vibrator) this.form.getSystemService("vibrator"));

    public Sound(ComponentContainer container) {
        super(container.$form());
        this.form.registerForOnResume(this);
        this.form.registerForOnStop(this);
        this.form.registerForOnDestroy(this);
        this.form.setVolumeControlStream(3);
        MinimumInterval(500);
    }

    @SimpleProperty(category = PropertyCategory.BEHAVIOR, description = "The name of the sound file.  Only <a href=\"http://developer.android.com/guide/appendix/media-formats.html\">certain formats</a> are supported.")
    public String Source() {
        return this.sourcePath;
    }

    @SimpleProperty
    @DesignerProperty(defaultValue = ElementType.MATCH_ANY_LOCALNAME, editorType = DesignerProperty.PROPERTY_TYPE_ASSET)
    public void Source(String path) {
        if (path == null) {
            path = ElementType.MATCH_ANY_LOCALNAME;
        }
        this.sourcePath = path;
        if (this.streamId != 0) {
            this.soundPool.stop(this.streamId);
            this.streamId = 0;
        }
        this.soundId = 0;
        if (this.sourcePath.length() != 0) {
            Integer existingSoundId = this.soundMap.get(this.sourcePath);
            if (existingSoundId != null) {
                this.soundId = existingSoundId.intValue();
                return;
            }
            Log.i("Sound", "No existing sound with path " + this.sourcePath + ".");
            try {
                int newSoundId = MediaUtil.loadSoundPool(this.soundPool, this.form, this.sourcePath);
                if (newSoundId != 0) {
                    this.soundMap.put(this.sourcePath, Integer.valueOf(newSoundId));
                    Log.i("Sound", "Successfully loaded sound: setting soundId to " + newSoundId + ".");
                    this.soundId = newSoundId;
                    return;
                }
                this.form.dispatchErrorOccurredEvent(this, "Source", ErrorMessages.ERROR_UNABLE_TO_LOAD_MEDIA, this.sourcePath);
            } catch (IOException e) {
                this.form.dispatchErrorOccurredEvent(this, "Source", ErrorMessages.ERROR_UNABLE_TO_LOAD_MEDIA, this.sourcePath);
            }
        }
    }

    @SimpleProperty(category = PropertyCategory.BEHAVIOR, description = "The minimum interval...")
    public int MinimumInterval() {
        return this.minimumInterval;
    }

    @SimpleProperty
    @DesignerProperty(defaultValue = "500", editorType = DesignerProperty.PROPERTY_TYPE_NON_NEGATIVE_INTEGER)
    public void MinimumInterval(int interval) {
        this.minimumInterval = interval;
    }

    @SimpleFunction
    public void Play() {
        if (this.soundId != 0) {
            long currentTime = System.currentTimeMillis();
            if (this.timeLastPlayed == 0 || currentTime >= this.timeLastPlayed + ((long) this.minimumInterval)) {
                this.timeLastPlayed = currentTime;
                this.streamId = this.soundPool.play(this.soundId, 1.0f, 1.0f, 0, 0, 1.0f);
                Log.i("Sound", "SoundPool.play returned stream id " + this.streamId);
                if (this.streamId == 0) {
                    this.form.dispatchErrorOccurredEvent(this, "Play", ErrorMessages.ERROR_UNABLE_TO_PLAY_MEDIA, this.sourcePath);
                    return;
                }
                return;
            }
            Log.i("Sound", "Unable to play because MinimumInterval has not elapsed since last play.");
            return;
        }
        Log.i("Sound", "Unable to play. Did you remember to set the Source property?");
    }

    @SimpleFunction
    public void Pause() {
        if (this.streamId != 0) {
            this.soundPool.pause(this.streamId);
        } else {
            Log.i("Sound", "Unable to pause. Did you remember to call the Play function?");
        }
    }

    @SimpleFunction
    public void Resume() {
        if (this.streamId != 0) {
            this.soundPool.resume(this.streamId);
        } else {
            Log.i("Sound", "Unable to resume. Did you remember to call the Play function?");
        }
    }

    @SimpleFunction
    public void Stop() {
        if (this.streamId != 0) {
            this.soundPool.stop(this.streamId);
            this.streamId = 0;
            return;
        }
        Log.i("Sound", "Unable to stop. Did you remember to call the Play function?");
    }

    @SimpleFunction
    public void Vibrate(int millisecs) {
        this.vibe.vibrate((long) millisecs);
    }

    @SimpleEvent(description = "The SoundError event is no longer used. Please use the Screen.ErrorOccurred event instead.", userVisible = GoogleLoginServiceConstants.PREFER_HOSTED)
    public void SoundError(String message) {
    }

    public void onStop() {
        Log.i("Sound", "Got onStop");
        if (this.streamId != 0) {
            this.soundPool.pause(this.streamId);
        }
    }

    public void onResume() {
        Log.i("Sound", "Got onResume");
        if (this.streamId != 0) {
            this.soundPool.resume(this.streamId);
        }
    }

    public void onDestroy() {
        prepareToDie();
    }

    public void onDelete() {
        prepareToDie();
    }

    private void prepareToDie() {
        if (this.streamId != 0) {
            this.soundPool.stop(this.streamId);
            this.soundPool.unload(this.streamId);
        }
        this.soundPool.release();
        this.vibe.cancel();
        this.soundPool = null;
    }
}
