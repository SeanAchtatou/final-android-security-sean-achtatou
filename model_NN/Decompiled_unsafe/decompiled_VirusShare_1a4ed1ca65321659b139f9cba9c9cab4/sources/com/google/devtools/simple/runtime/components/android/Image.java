package com.google.devtools.simple.runtime.components.android;

import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import com.google.devtools.simple.common.ComponentCategory;
import com.google.devtools.simple.common.PropertyCategory;
import com.google.devtools.simple.runtime.annotations.DesignerComponent;
import com.google.devtools.simple.runtime.annotations.DesignerProperty;
import com.google.devtools.simple.runtime.annotations.SimpleObject;
import com.google.devtools.simple.runtime.annotations.SimpleProperty;
import com.google.devtools.simple.runtime.annotations.UsesPermissions;
import com.google.devtools.simple.runtime.components.android.util.AnimationUtil;
import com.google.devtools.simple.runtime.components.android.util.MediaUtil;
import com.google.devtools.simple.runtime.components.android.util.ViewUtil;
import gnu.kawa.xml.ElementType;
import java.io.IOException;

@SimpleObject
@UsesPermissions(permissionNames = "android.permission.INTERNET")
@DesignerComponent(category = ComponentCategory.BASIC, description = "Component for displaying images.  The picture to display, and other aspects of the Image's appearance, can be specified in the Designer or in the Blocks Editor.", version = 1)
public final class Image extends AndroidViewComponent {
    private String picturePath = ElementType.MATCH_ANY_LOCALNAME;
    private final ImageView view;

    public Image(ComponentContainer container) {
        super(container);
        this.view = new ImageView(container.$context()) {
            public boolean verifyDrawable(Drawable dr) {
                super.verifyDrawable(dr);
                return true;
            }
        };
        container.$add(this);
        this.view.setFocusable(true);
    }

    public View getView() {
        return this.view;
    }

    @SimpleProperty(category = PropertyCategory.APPEARANCE)
    public String Picture() {
        return this.picturePath;
    }

    @SimpleProperty
    @DesignerProperty(defaultValue = ElementType.MATCH_ANY_LOCALNAME, editorType = DesignerProperty.PROPERTY_TYPE_ASSET)
    public void Picture(String path) {
        Drawable drawable;
        if (path == null) {
            path = ElementType.MATCH_ANY_LOCALNAME;
        }
        this.picturePath = path;
        try {
            drawable = MediaUtil.getBitmapDrawable(this.container.$form(), this.picturePath);
        } catch (IOException e) {
            Log.e("Image", "Unable to load " + this.picturePath);
            drawable = null;
        }
        ViewUtil.setImage(this.view, drawable);
    }

    @SimpleProperty(category = PropertyCategory.APPEARANCE)
    public void Animation(String animation) {
        AnimationUtil.ApplyAnimation(this.view, animation);
    }
}
