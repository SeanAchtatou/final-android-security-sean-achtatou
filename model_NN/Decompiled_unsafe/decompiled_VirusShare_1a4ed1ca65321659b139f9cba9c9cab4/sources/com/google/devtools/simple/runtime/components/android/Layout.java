package com.google.devtools.simple.runtime.components.android;

import android.view.ViewGroup;

public interface Layout {
    void add(AndroidViewComponent androidViewComponent);

    ViewGroup getLayoutManager();
}
