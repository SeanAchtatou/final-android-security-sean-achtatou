package com.google.devtools.simple.runtime.components;

import com.google.devtools.simple.common.PropertyCategory;
import com.google.devtools.simple.runtime.annotations.SimpleObject;
import com.google.devtools.simple.runtime.annotations.SimpleProperty;

@SimpleObject
public abstract class VisibleComponent implements Component {
    @SimpleProperty(category = PropertyCategory.APPEARANCE)
    public abstract int Height();

    @SimpleProperty
    public abstract void Height(int i);

    @SimpleProperty(category = PropertyCategory.APPEARANCE)
    public abstract int Width();

    @SimpleProperty
    public abstract void Width(int i);

    protected VisibleComponent() {
    }
}
