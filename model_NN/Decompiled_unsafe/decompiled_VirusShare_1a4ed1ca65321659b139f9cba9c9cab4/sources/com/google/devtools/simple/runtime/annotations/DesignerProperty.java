package com.google.devtools.simple.runtime.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface DesignerProperty {
    public static final String PROPERTY_TYPE_ASSET = "asset";
    public static final String PROPERTY_TYPE_BLUETOOTHCLIENT = "BluetoothClient";
    public static final String PROPERTY_TYPE_BOOLEAN = "boolean";
    public static final String PROPERTY_TYPE_COLOR = "color";
    public static final String PROPERTY_TYPE_COMPONENT = "component";
    public static final String PROPERTY_TYPE_FLOAT = "float";
    public static final String PROPERTY_TYPE_INTEGER = "integer";
    public static final String PROPERTY_TYPE_LEGO_NXT_GENERATED_COLOR = "lego_nxt_generated_color";
    public static final String PROPERTY_TYPE_LEGO_NXT_SENSOR_PORT = "lego_nxt_sensor_port";
    public static final String PROPERTY_TYPE_NON_NEGATIVE_FLOAT = "non_negative_float";
    public static final String PROPERTY_TYPE_NON_NEGATIVE_INTEGER = "non_negative_integer";
    public static final String PROPERTY_TYPE_SCREEN_ORIENTATION = "screen_orientation";
    public static final String PROPERTY_TYPE_STRING = "string";
    public static final String PROPERTY_TYPE_TEXT = "text";
    public static final String PROPERTY_TYPE_TEXTALIGNMENT = "textalignment";
    public static final String PROPERTY_TYPE_TYPEFACE = "typeface";

    String defaultValue() default "";

    String editorType() default "text";
}
