package com.google.devtools.simple.runtime.components.android;

public interface OnDestroyListener {
    void onDestroy();
}
