package com.google.devtools.simple.runtime.parameters;

public final class BooleanReferenceParameter extends ReferenceParameter {
    private boolean value;

    public BooleanReferenceParameter(boolean value2) {
        set(value2);
    }

    public boolean get() {
        return this.value;
    }

    public void set(boolean value2) {
        this.value = value2;
    }
}
