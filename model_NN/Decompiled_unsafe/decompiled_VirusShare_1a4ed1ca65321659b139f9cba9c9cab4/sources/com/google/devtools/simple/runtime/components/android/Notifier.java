package com.google.devtools.simple.runtime.components.android;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Handler;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;
import com.google.devtools.simple.common.ComponentCategory;
import com.google.devtools.simple.runtime.annotations.DesignerComponent;
import com.google.devtools.simple.runtime.annotations.SimpleEvent;
import com.google.devtools.simple.runtime.annotations.SimpleFunction;
import com.google.devtools.simple.runtime.annotations.SimpleObject;
import com.google.devtools.simple.runtime.components.Component;
import com.google.devtools.simple.runtime.events.EventDispatcher;

@SimpleObject
@DesignerComponent(category = ComponentCategory.MISC, description = "Component that creates alert messages.", iconName = "images/notifier.png", nonVisible = true, version = 1)
public final class Notifier extends AndroidNonvisibleComponent implements Component {
    private static final String LOG_TAG = "Notifier";
    /* access modifiers changed from: private */
    public final Activity activity;
    private final Handler handler = new Handler();

    public Notifier(ComponentContainer container) {
        super(container.$form());
        this.activity = container.$context();
    }

    @SimpleFunction
    public void ShowMessageDialog(String message, String title, String buttonText) {
        oneButtonAlert(message, title, buttonText);
    }

    private void oneButtonAlert(String message, String title, String buttonText) {
        Log.i(LOG_TAG, "One button alert " + message);
        AlertDialog alertDialog = new AlertDialog.Builder(this.activity).create();
        alertDialog.setTitle(title);
        alertDialog.setCancelable(false);
        alertDialog.setMessage(message);
        alertDialog.setButton(buttonText, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alertDialog.show();
    }

    @SimpleFunction
    public void ShowChooseDialog(String message, String title, String button1Text, String button2Text) {
        twoButtonAlert(message, title, button1Text, button2Text);
    }

    private void twoButtonAlert(String message, String title, final String button1Text, final String button2Text) {
        Log.i(LOG_TAG, "ShowChooseDialog: " + message);
        AlertDialog alertDialog = new AlertDialog.Builder(this.activity).create();
        alertDialog.setTitle(title);
        alertDialog.setCancelable(false);
        alertDialog.setMessage(message);
        alertDialog.setButton(button1Text, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Notifier.this.AfterChoosing(button1Text);
            }
        });
        alertDialog.setButton2(button2Text, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Notifier.this.AfterChoosing(button2Text);
            }
        });
        alertDialog.show();
    }

    @SimpleEvent
    public void AfterChoosing(String choice) {
        EventDispatcher.dispatchEvent(this, "AfterChoosing", choice);
    }

    @SimpleFunction
    public void ShowTextDialog(String message, String title) {
        textInputAlert(message, title);
    }

    private void textInputAlert(String message, String title) {
        Log.i(LOG_TAG, "Text input alert: " + message);
        AlertDialog alertDialog = new AlertDialog.Builder(this.activity).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        final EditText input = new EditText(this.activity);
        alertDialog.setView(input);
        alertDialog.setCancelable(false);
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Notifier.this.AfterTextInput(input.getText().toString());
            }
        });
        alertDialog.show();
    }

    @SimpleEvent
    public void AfterTextInput(String response) {
        EventDispatcher.dispatchEvent(this, "AfterTextInput", response);
    }

    @SimpleFunction
    public void ShowAlert(final String notice) {
        this.handler.post(new Runnable() {
            public void run() {
                Toast.makeText(Notifier.this.activity, notice, 1).show();
            }
        });
    }

    @SimpleFunction
    public void LogError(String message) {
        Log.e(LOG_TAG, message);
    }

    @SimpleFunction
    public void LogWarning(String message) {
        Log.w(LOG_TAG, message);
    }

    @SimpleFunction
    public void LogInfo(String message) {
        Log.i(LOG_TAG, message);
    }
}
