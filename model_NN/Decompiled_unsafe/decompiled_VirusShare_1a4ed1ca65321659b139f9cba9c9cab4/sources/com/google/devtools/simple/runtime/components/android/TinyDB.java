package com.google.devtools.simple.runtime.components.android;

import android.content.SharedPreferences;
import com.google.devtools.simple.common.ComponentCategory;
import com.google.devtools.simple.runtime.annotations.DesignerComponent;
import com.google.devtools.simple.runtime.annotations.SimpleFunction;
import com.google.devtools.simple.runtime.annotations.SimpleObject;
import com.google.devtools.simple.runtime.components.Component;
import com.google.devtools.simple.runtime.components.util.JsonUtil;
import com.google.devtools.simple.runtime.errors.YailRuntimeError;
import gnu.kawa.xml.ElementType;
import org.json.JSONException;

@SimpleObject
@DesignerComponent(category = ComponentCategory.BASIC, description = "Non-visible component that persistently stores values on the phone.", iconName = "images/tinyDB.png", nonVisible = true, version = 1)
public class TinyDB extends AndroidNonvisibleComponent implements Component, Deleteable {
    private SharedPreferences sharedPreferences;

    public TinyDB(ComponentContainer container) {
        super(container.$form());
        this.sharedPreferences = container.$context().getSharedPreferences("TinyDB", 0);
    }

    @SimpleFunction
    public void StoreValue(String tag, Object valueToStore) {
        SharedPreferences.Editor sharedPrefsEditor = this.sharedPreferences.edit();
        try {
            sharedPrefsEditor.putString(tag, JsonUtil.getJsonRepresentation(valueToStore));
            sharedPrefsEditor.commit();
        } catch (JSONException e) {
            throw new YailRuntimeError("Value failed to convert to JSON.", "JSON Creation Error.");
        }
    }

    @SimpleFunction
    public Object GetValue(String tag) {
        try {
            String value = this.sharedPreferences.getString(tag, ElementType.MATCH_ANY_LOCALNAME);
            return value.length() == 0 ? ElementType.MATCH_ANY_LOCALNAME : JsonUtil.getObjectFromJson(value);
        } catch (JSONException e) {
            throw new YailRuntimeError("Value failed to convert from JSON.", "JSON Creation Error.");
        }
    }

    public void onDelete() {
        SharedPreferences.Editor sharedPrefsEditor = this.sharedPreferences.edit();
        sharedPrefsEditor.clear();
        sharedPrefsEditor.commit();
    }
}
