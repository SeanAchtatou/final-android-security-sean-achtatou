package com.google.devtools.simple.runtime.components.android;

public interface OnPauseListener {
    void onPause();
}
