package com.google.devtools.simple.runtime.components.android;

import android.media.MediaPlayer;
import android.util.Log;
import android.view.View;
import android.widget.MediaController;
import android.widget.VideoView;
import com.google.android.googlelogin.GoogleLoginServiceConstants;
import com.google.devtools.simple.common.ComponentCategory;
import com.google.devtools.simple.common.ComponentConstants;
import com.google.devtools.simple.common.PropertyCategory;
import com.google.devtools.simple.runtime.annotations.DesignerComponent;
import com.google.devtools.simple.runtime.annotations.DesignerProperty;
import com.google.devtools.simple.runtime.annotations.SimpleEvent;
import com.google.devtools.simple.runtime.annotations.SimpleFunction;
import com.google.devtools.simple.runtime.annotations.SimpleObject;
import com.google.devtools.simple.runtime.annotations.SimpleProperty;
import com.google.devtools.simple.runtime.annotations.UsesPermissions;
import com.google.devtools.simple.runtime.components.android.util.MediaUtil;
import com.google.devtools.simple.runtime.components.util.ErrorMessages;
import com.google.devtools.simple.runtime.events.EventDispatcher;
import gnu.kawa.xml.ElementType;
import java.io.IOException;

@SimpleObject
@UsesPermissions(permissionNames = "android.permission.INTERNET")
@DesignerComponent(category = ComponentCategory.MEDIA, description = "A multimedia component capable of playing videos. When the application is run, the VideoPlayer will be displayed as a rectangle on-screen.  If the user touches the rectangle, controls will appear to play/pause, skip ahead, and skip backward within the video.  The application can also control behavior by calling the <code>Start</code>, <code>Pause</code>, and <code>SeekTo</code> methods.  <p>Video files should be in Windows Media Video (.wmv) format, 3GPP (.3gp), or MPEG-4 (.mp4).  For more details about legal formats, see <a href=\"http://developer.android.com/guide/appendix/media-formats.html\" target=\"_blank\">Android Supported Media Formats</a>.</p><p>App Inventor for Android only permits video files under 1 MB and limits the total size of an application to 5 MB, not all of which is available for media (video, audio, and sound) files.  If your media files are too large, you may get errors when packaging or installing your application, in which case you should reduce the number of media files or their sizes.  Most video editing software, such as Windows Movie Maker and Apple iMovie, can help you decrease the size of videos by shortening them or re-encoding the video into a more compact format.</p>", version = 3)
public final class VideoPlayer extends AndroidViewComponent implements OnDestroyListener, Deleteable, MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener {
    private String sourcePath = ElementType.MATCH_ANY_LOCALNAME;
    private final VideoView videoView;

    public VideoPlayer(ComponentContainer container) {
        super(container);
        container.$form().registerForOnDestroy(this);
        this.videoView = new VideoView(container.$context());
        this.videoView.setMediaController(new MediaController(container.$context()));
        this.videoView.setOnCompletionListener(this);
        this.videoView.setOnErrorListener(this);
        container.$add(this);
        container.setChildWidth(this, ComponentConstants.VIDEOPLAYER_PREFERRED_WIDTH);
        container.setChildHeight(this, ComponentConstants.VIDEOPLAYER_PREFERRED_HEIGHT);
        container.$form().setVolumeControlStream(3);
    }

    public View getView() {
        return this.videoView;
    }

    @SimpleProperty(category = PropertyCategory.BEHAVIOR, description = "The \"path\" to the video.  Usually, this will be the name of the video file, which should be added in the Designer.")
    @DesignerProperty(defaultValue = ElementType.MATCH_ANY_LOCALNAME, editorType = DesignerProperty.PROPERTY_TYPE_ASSET)
    public void Source(String path) {
        if (path == null) {
            path = ElementType.MATCH_ANY_LOCALNAME;
        }
        this.sourcePath = path;
        if (this.videoView.isPlaying()) {
            this.videoView.stopPlayback();
        }
        this.videoView.setVideoURI(null);
        this.videoView.clearAnimation();
        if (this.sourcePath.length() > 0) {
            Log.i("VideoPlayer", "Source path is " + this.sourcePath);
            try {
                MediaUtil.loadVideoView(this.videoView, this.container.$form(), this.sourcePath);
                Log.i("VideoPlayer", "loading video succeeded");
            } catch (IOException e) {
                this.container.$form().dispatchErrorOccurredEvent(this, "Source", ErrorMessages.ERROR_UNABLE_TO_LOAD_MEDIA, this.sourcePath);
            }
        }
    }

    @SimpleFunction(description = "Starts playback of the video.")
    public void Start() {
        Log.i("VideoPlayer", "Calling Start");
        this.videoView.start();
    }

    @SimpleFunction(description = "Pauses playback of the video.  Playback can be resumed at the same location by calling the <code>Start</code> method.")
    public void Pause() {
        Log.i("VideoPlayer", "Calling Pause");
        this.videoView.pause();
    }

    @SimpleFunction(description = "Seeks to the requested time (specified in milliseconds) in the video. Note that if the video is paused, the frame shown will not be updated by the seek. ")
    public void SeekTo(int ms) {
        Log.i("VideoPlayer", "Calling SeekTo");
        if (ms < 0) {
            ms = 0;
        }
        this.videoView.seekTo(ms);
    }

    @SimpleFunction(description = "Returns duration of the video in milliseconds.")
    public int GetDuration() {
        Log.i("VideoPlayer", "Calling GetDuration");
        return this.videoView.getDuration();
    }

    public void onCompletion(MediaPlayer m) {
        Completed();
    }

    @SimpleEvent
    public void Completed() {
        EventDispatcher.dispatchEvent(this, "Completed", new Object[0]);
    }

    public boolean onError(MediaPlayer m, int what, int extra) {
        Log.e("VideoPlayer", "onError: what is " + what + " 0x" + Integer.toHexString(what) + ", extra is " + extra + " 0x" + Integer.toHexString(extra));
        this.container.$form().dispatchErrorOccurredEvent(this, "Source", ErrorMessages.ERROR_UNABLE_TO_LOAD_MEDIA, this.sourcePath);
        return true;
    }

    @SimpleEvent(description = "The VideoPlayerError event is no longer used. Please use the Screen.ErrorOccurred event instead.", userVisible = GoogleLoginServiceConstants.PREFER_HOSTED)
    public void VideoPlayerError(String message) {
    }

    public void onDestroy() {
        prepareToDie();
    }

    public void onDelete() {
        prepareToDie();
    }

    private void prepareToDie() {
        if (this.videoView.isPlaying()) {
            this.videoView.stopPlayback();
        }
        this.videoView.setVideoURI(null);
        this.videoView.clearAnimation();
    }

    @SimpleProperty(userVisible = GoogleLoginServiceConstants.PREFER_HOSTED)
    public int Width() {
        return super.Width();
    }

    @SimpleProperty(userVisible = GoogleLoginServiceConstants.PREFER_HOSTED)
    public void Width(int width) {
        super.Width(width);
    }

    @SimpleProperty(userVisible = GoogleLoginServiceConstants.PREFER_HOSTED)
    public int Height() {
        return super.Height();
    }

    @SimpleProperty(userVisible = GoogleLoginServiceConstants.PREFER_HOSTED)
    public void Height(int height) {
        super.Height(height);
    }
}
