package com.google.devtools.simple.runtime.errors;

import com.google.devtools.simple.runtime.annotations.SimpleObject;

@SimpleObject
public class AssertionFailure extends RuntimeError {
    public AssertionFailure() {
    }

    public AssertionFailure(String msg) {
        super(msg);
    }
}
