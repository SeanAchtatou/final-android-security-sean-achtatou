package com.google.devtools.simple.runtime.components.android;

import android.content.Intent;
import com.google.devtools.simple.common.ComponentCategory;
import com.google.devtools.simple.common.PropertyCategory;
import com.google.devtools.simple.runtime.annotations.DesignerComponent;
import com.google.devtools.simple.runtime.annotations.DesignerProperty;
import com.google.devtools.simple.runtime.annotations.SimpleObject;
import com.google.devtools.simple.runtime.annotations.SimpleProperty;
import com.google.devtools.simple.runtime.components.util.YailList;
import com.google.devtools.simple.runtime.errors.YailRuntimeError;
import gnu.kawa.xml.ElementType;

@SimpleObject
@DesignerComponent(category = ComponentCategory.BASIC, description = "<p>A button that, when clicked on, displays a list of texts for the user to choose among. The texts can be specified through the Designer or Blocks Editor by setting the <code>ElementsFromString</code> property to their string-separated concatenation (for example, <em>choice 1, choice 2, choice 3</em>) or by setting the <code>Elements</code> property to a List in the Blocks editor.</p><p>Other properties affect the appearance of the button (<code>TextAlignment</code>, <code>BackgroundColor</code>, etc.) and whether it can be clicked on (<code>Enabled</code>).</p>", version = 4)
public class ListPicker extends Picker implements ActivityResultListener, Deleteable {
    static final String LIST_ACTIVITY_ARG_NAME = (LIST_ACTIVITY_CLASS + ".list");
    private static final String LIST_ACTIVITY_CLASS = ListPickerActivity.class.getName();
    static final String LIST_ACTIVITY_RESULT_INDEX = (LIST_ACTIVITY_CLASS + ".index");
    static final String LIST_ACTIVITY_RESULT_NAME = (LIST_ACTIVITY_CLASS + ".selection");
    private YailList items = new YailList();
    private String selection = ElementType.MATCH_ANY_LOCALNAME;
    private int selectionIndex = 0;

    public ListPicker(ComponentContainer container) {
        super(container);
    }

    @SimpleProperty(category = PropertyCategory.BEHAVIOR, description = "<p>The selected item.  When directly changed by the programmer, the SelectionIndex property is also changed to the first item in the ListPicker with the given value.  If the value does not appear, SelectionIndex will be set to 0.</p>")
    public String Selection() {
        return this.selection;
    }

    @SimpleProperty
    @DesignerProperty(defaultValue = ElementType.MATCH_ANY_LOCALNAME, editorType = DesignerProperty.PROPERTY_TYPE_STRING)
    public void Selection(String value) {
        this.selection = value;
        for (int i = 0; i < this.items.size(); i++) {
            if (this.items.getString(i).equals(value)) {
                this.selectionIndex = i + 1;
                return;
            }
        }
        this.selectionIndex = 0;
    }

    @SimpleProperty(category = PropertyCategory.BEHAVIOR, description = "<p>The index of the currently selected item, starting at 1.  If no item is selected, the value will be 0.  If an attempt is made to set this to a number less than 1 or greater than the number of items in the ListPicker, SelectionIndex will be set to 0, and Selection will be set to the empty text.</p>")
    public int SelectionIndex() {
        return this.selectionIndex;
    }

    @SimpleProperty
    public void SelectionIndex(int index) {
        if (index <= 0 || index > this.items.size()) {
            this.selectionIndex = 0;
            this.selection = ElementType.MATCH_ANY_LOCALNAME;
            return;
        }
        this.selectionIndex = index;
        this.selection = this.items.getString(this.selectionIndex - 1);
    }

    @SimpleProperty(category = PropertyCategory.BEHAVIOR)
    public YailList Elements() {
        return this.items;
    }

    @SimpleProperty
    public void Elements(YailList itemList) {
        Object[] objects = itemList.toStringArray();
        for (Object obj : objects) {
            if (!(obj instanceof String)) {
                throw new YailRuntimeError("Items passed to ListPicker must be Strings", "Error");
            }
        }
        this.items = itemList;
    }

    @SimpleProperty(category = PropertyCategory.BEHAVIOR)
    @DesignerProperty(defaultValue = ElementType.MATCH_ANY_LOCALNAME, editorType = DesignerProperty.PROPERTY_TYPE_STRING)
    public void ElementsFromString(String itemstring) {
        if (itemstring.length() == 0) {
            this.items = new YailList();
        } else {
            this.items = YailList.makeList((Object[]) itemstring.split(" *, *"));
        }
    }

    public Intent getIntent() {
        Intent intent = new Intent();
        intent.setClassName(this.container.$context(), LIST_ACTIVITY_CLASS);
        intent.putExtra(LIST_ACTIVITY_ARG_NAME, this.items.toStringArray());
        return intent;
    }

    public void resultReturned(int requestCode, int resultCode, Intent data) {
        if (requestCode == this.requestCode && resultCode == -1) {
            if (data.hasExtra(LIST_ACTIVITY_RESULT_NAME)) {
                this.selection = data.getStringExtra(LIST_ACTIVITY_RESULT_NAME);
            } else {
                this.selection = ElementType.MATCH_ANY_LOCALNAME;
            }
            this.selectionIndex = data.getIntExtra(LIST_ACTIVITY_RESULT_INDEX, 0);
            AfterPicking();
        }
    }

    public void onDelete() {
        this.container.$form().unregisterForActivityResult(this);
    }
}
