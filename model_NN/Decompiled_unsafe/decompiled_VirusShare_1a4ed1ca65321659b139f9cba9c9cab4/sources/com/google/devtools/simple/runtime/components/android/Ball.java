package com.google.devtools.simple.runtime.components.android;

import android.graphics.Canvas;
import android.graphics.Paint;
import com.google.devtools.simple.common.ComponentCategory;
import com.google.devtools.simple.common.PropertyCategory;
import com.google.devtools.simple.runtime.annotations.DesignerComponent;
import com.google.devtools.simple.runtime.annotations.DesignerProperty;
import com.google.devtools.simple.runtime.annotations.SimpleObject;
import com.google.devtools.simple.runtime.annotations.SimpleProperty;
import com.google.devtools.simple.runtime.components.Component;
import com.google.devtools.simple.runtime.components.android.util.PaintUtil;

@SimpleObject
@DesignerComponent(category = ComponentCategory.ANIMATION, description = "<p>A round 'sprite' that can be placed on a <code>Canvas</code>, where it can react to touches and drags, interact with other sprites (<code>ImageSprite</code>s and other <code>Ball</code>s) and the edge of the Canvas, and move according to its property values.</p><p>For example, to have a <code>Ball</code> move 4 pixels toward the top of a <code>Canvas</code> every 500 milliseconds (half second), you would set the <code>Speed</code> property to 4 [pixels], the <code>Interval</code> property to 500 [milliseconds], the <code>Heading</code> property to 90 [degrees], and the <code>Enabled</code> property to <code>True</code>.  These and its other properties can be changed at any time.</p><p>The difference between a Ball and an <code>ImageSprite</code> is that the latter can get its appearance from an image file, while a Ball's appearance can only be changed by varying its <code>PaintColor</code> and <code>Radius</code> properties.</p>", version = 3)
public final class Ball extends Sprite {
    static final int DEFAULT_RADIUS = 5;
    private Paint paint = new Paint();
    private int paintColor;
    private int radius;

    public Ball(ComponentContainer container) {
        super(container);
        PaintColor(Component.COLOR_BLACK);
        Radius(5);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        if (this.visible) {
            canvas.drawCircle(((float) this.xLeft) + ((float) this.radius), ((float) this.yTop) + ((float) this.radius), (float) this.radius, this.paint);
        }
    }

    public int Height() {
        return this.radius * 2;
    }

    public void Height(int height) {
    }

    public int Width() {
        return this.radius * 2;
    }

    public void Width(int width) {
    }

    public boolean containsPoint(double qx, double qy) {
        double xCenter = this.xLeft + ((double) this.radius);
        double yCenter = this.yTop + ((double) this.radius);
        return ((qx - xCenter) * (qx - xCenter)) + ((qy - yCenter) * (qy - yCenter)) <= ((double) (this.radius * this.radius));
    }

    @SimpleProperty(category = PropertyCategory.APPEARANCE)
    @DesignerProperty(defaultValue = "5", editorType = DesignerProperty.PROPERTY_TYPE_NON_NEGATIVE_INTEGER)
    public void Radius(int radius2) {
        this.radius = radius2;
        registerChange();
    }

    @SimpleProperty
    public int Radius() {
        return this.radius;
    }

    @SimpleProperty(category = PropertyCategory.APPEARANCE)
    public int PaintColor() {
        return this.paintColor;
    }

    @SimpleProperty
    @DesignerProperty(defaultValue = Component.DEFAULT_VALUE_COLOR_BLACK, editorType = DesignerProperty.PROPERTY_TYPE_COLOR)
    public void PaintColor(int argb) {
        this.paintColor = argb;
        if (argb != 0) {
            PaintUtil.changePaint(this.paint, argb);
        } else {
            PaintUtil.changePaint(this.paint, Component.COLOR_BLACK);
        }
        registerChange();
    }
}
