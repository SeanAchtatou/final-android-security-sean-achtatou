package com.google.devtools.simple.runtime.parameters;

public final class ObjectReferenceParameter<T> extends ReferenceParameter {
    private T value;

    public ObjectReferenceParameter(T value2) {
        set(value2);
    }

    public T get() {
        return this.value;
    }

    public void set(T value2) {
        this.value = value2;
    }
}
