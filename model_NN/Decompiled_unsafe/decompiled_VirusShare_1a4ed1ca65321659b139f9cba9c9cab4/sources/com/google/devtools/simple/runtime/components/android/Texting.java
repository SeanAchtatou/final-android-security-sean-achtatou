package com.google.devtools.simple.runtime.components.android;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.telephony.gsm.SmsManager;
import android.telephony.gsm.SmsMessage;
import android.util.Log;
import com.google.devtools.simple.common.ComponentCategory;
import com.google.devtools.simple.common.PropertyCategory;
import com.google.devtools.simple.runtime.annotations.DesignerComponent;
import com.google.devtools.simple.runtime.annotations.DesignerProperty;
import com.google.devtools.simple.runtime.annotations.SimpleEvent;
import com.google.devtools.simple.runtime.annotations.SimpleFunction;
import com.google.devtools.simple.runtime.annotations.SimpleObject;
import com.google.devtools.simple.runtime.annotations.SimpleProperty;
import com.google.devtools.simple.runtime.annotations.UsesPermissions;
import com.google.devtools.simple.runtime.components.Component;
import com.google.devtools.simple.runtime.events.EventDispatcher;
import gnu.kawa.xml.ElementType;

@SimpleObject
@DesignerComponent(category = ComponentCategory.SOCIAL, description = "<p>A component that will, when the <code>SendMessage</code>  method is called, send the text message specified in the <code>Message</code> property to the phone number specified in the <code>PhoneNumber</code> property. <p>This component can also receive test messages unless the <code>ReceivingEnabled</code> property is False.  When a message arrives, the <code>MessageReceived</code> event is raised and provides the sending number and message.</p><p>Often, this component is used with the <code>ContactPicker</code> component, which lets the user select a contact from the ones stored on the phone and sets the <code>PhoneNumber</code> property to the contact's phone number.</p><p>To directly specify the phone number (e.g., 650-555-1212), set the <code>PhoneNumber</code> property to a Text with the specified digits (e.g., \"6505551212\").  Dashes, dots, and parentheses may be included (e.g., \"(650)-555-1212\") but will be ignored; spaces may not be included.</p>", iconName = "images/texting.png", nonVisible = true, version = 1)
@UsesPermissions(permissionNames = "android.permission.RECEIVE_SMS, android.permission.SEND_SMS")
public class Texting extends AndroidNonvisibleComponent implements Component, OnDestroyListener, Deleteable {
    private String message;
    private String phoneNumber;
    private boolean receivingEnabled;
    private SmsManager smsManager;
    private SmsReceiver smsReceiver = new SmsReceiver();

    class SmsReceiver extends BroadcastReceiver {
        SmsReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            SmsMessage message = Texting.getMessagesFromIntent(intent)[0];
            if (message != null) {
                Texting.this.MessageReceived(message.getOriginatingAddress(), message.getMessageBody());
                return;
            }
            Log.i("Simple", "Sms message suppposedly received but with no actual content.");
        }
    }

    public Texting(ComponentContainer container) {
        super(container.$form());
        IntentFilter intentFilter = new IntentFilter("android.provider.Telephony.SMS_RECEIVED");
        container.$context().registerReceiver(this.smsReceiver, intentFilter);
        this.form.registerForOnDestroy(this);
        Log.d("Simple", "Texting constructor");
        this.smsManager = SmsManager.getDefault();
        PhoneNumber(ElementType.MATCH_ANY_LOCALNAME);
        this.receivingEnabled = true;
    }

    @SimpleProperty(category = PropertyCategory.BEHAVIOR)
    @DesignerProperty(defaultValue = ElementType.MATCH_ANY_LOCALNAME, editorType = DesignerProperty.PROPERTY_TYPE_STRING)
    public void PhoneNumber(String phoneNumber2) {
        this.phoneNumber = phoneNumber2;
    }

    @SimpleProperty
    public String PhoneNumber() {
        return this.phoneNumber;
    }

    @SimpleProperty(category = PropertyCategory.BEHAVIOR)
    @DesignerProperty(defaultValue = ElementType.MATCH_ANY_LOCALNAME, editorType = DesignerProperty.PROPERTY_TYPE_STRING)
    public void Message(String message2) {
        this.message = message2;
    }

    @SimpleProperty
    public String Message() {
        return this.message;
    }

    @SimpleFunction
    public void SendMessage() {
        this.smsManager.sendTextMessage(this.phoneNumber, null, this.message, null, null);
    }

    @SimpleEvent
    public void MessageReceived(String number, String messageText) {
        if (this.receivingEnabled) {
            Log.d("Simple", "MessageReceived");
            EventDispatcher.dispatchEvent(this, "MessageReceived", number, messageText);
        }
    }

    @SimpleProperty(category = PropertyCategory.BEHAVIOR)
    public boolean ReceivingEnabled() {
        return this.receivingEnabled;
    }

    @SimpleProperty
    @DesignerProperty(defaultValue = "True", editorType = DesignerProperty.PROPERTY_TYPE_BOOLEAN)
    public void ReceivingEnabled(boolean enabled) {
        this.receivingEnabled = enabled;
    }

    public static SmsMessage[] getMessagesFromIntent(Intent intent) {
        Object[] messages = (Object[]) intent.getSerializableExtra("pdus");
        byte[][] pduObjs = new byte[messages.length][];
        for (int i = 0; i < messages.length; i++) {
            pduObjs[i] = (byte[]) messages[i];
        }
        byte[][] pdus = new byte[pduObjs.length][];
        int pduCount = pdus.length;
        SmsMessage[] msgs = new SmsMessage[pduCount];
        for (int i2 = 0; i2 < pduCount; i2++) {
            pdus[i2] = pduObjs[i2];
            msgs[i2] = SmsMessage.createFromPdu(pdus[i2]);
        }
        return msgs;
    }

    public void onDestroy() {
        prepareToDie();
    }

    public void onDelete() {
        prepareToDie();
    }

    private void prepareToDie() {
        this.form.unregisterReceiver(this.smsReceiver);
        this.smsReceiver = null;
    }
}
