package com.google.devtools.simple.runtime.annotations;

import com.google.devtools.simple.common.PropertyCategory;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface SimpleProperty {
    PropertyCategory category() default PropertyCategory.UNSET;

    String description() default "";

    boolean userVisible() default true;
}
