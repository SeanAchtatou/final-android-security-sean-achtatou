package com.google.devtools.simple.runtime.components.android.util;

import android.view.View;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;

public final class AnimationUtil {
    private AnimationUtil() {
    }

    private static void ApplyHorizontalScrollAnimation(View view, boolean left, int speed) {
        float sign = left ? 1.0f : -1.0f;
        AnimationSet animationSet = new AnimationSet(true);
        animationSet.setRepeatCount(-1);
        animationSet.setRepeatMode(1);
        TranslateAnimation move = new TranslateAnimation(2, 0.7f * sign, 2, sign * -0.7f, 2, 0.0f, 2, 0.0f);
        move.setStartOffset(0);
        move.setDuration((long) speed);
        move.setFillAfter(true);
        animationSet.addAnimation(move);
        view.startAnimation(animationSet);
    }

    public static void ApplyAnimation(View view, String animation) {
        if (animation.equals("ScrollRightSlow")) {
            ApplyHorizontalScrollAnimation(view, false, 8000);
        } else if (animation.equals("ScrollRight")) {
            ApplyHorizontalScrollAnimation(view, false, 4000);
        } else if (animation.equals("ScrollRightFast")) {
            ApplyHorizontalScrollAnimation(view, false, 1000);
        } else if (animation.equals("ScrollLeftSlow")) {
            ApplyHorizontalScrollAnimation(view, true, 8000);
        } else if (animation.equals("ScrollLeft")) {
            ApplyHorizontalScrollAnimation(view, true, 4000);
        } else if (animation.equals("ScrollLeftFast")) {
            ApplyHorizontalScrollAnimation(view, true, 1000);
        }
    }
}
