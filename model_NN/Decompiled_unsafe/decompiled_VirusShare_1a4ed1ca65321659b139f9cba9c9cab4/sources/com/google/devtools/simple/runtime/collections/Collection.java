package com.google.devtools.simple.runtime.collections;

import com.google.devtools.simple.runtime.annotations.SimpleFunction;
import com.google.devtools.simple.runtime.annotations.SimpleObject;
import com.google.devtools.simple.runtime.annotations.SimpleProperty;
import com.google.devtools.simple.runtime.variants.Variant;
import java.util.ArrayList;

@SimpleObject
public class Collection {
    private ArrayList<Variant> list = new ArrayList<>();

    @SimpleFunction
    public void Clear() {
        this.list.clear();
    }

    @SimpleFunction
    public void Add(Variant item) {
        this.list.add(item);
    }

    @SimpleFunction
    public Variant Item(int index) {
        return this.list.get(index);
    }

    @SimpleProperty
    public int Count() {
        return this.list.size();
    }

    @SimpleFunction
    public boolean Contains(Variant item) {
        return this.list.contains(item);
    }

    @SimpleFunction
    public void Remove(Variant item) {
        this.list.remove(item);
    }
}
