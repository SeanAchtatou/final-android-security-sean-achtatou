package com.google.devtools.simple.runtime.components.android;

public interface OnResumeListener {
    void onResume();
}
