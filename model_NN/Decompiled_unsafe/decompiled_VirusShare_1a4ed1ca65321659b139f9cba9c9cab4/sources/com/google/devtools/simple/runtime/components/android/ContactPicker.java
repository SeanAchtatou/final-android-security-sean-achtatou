package com.google.devtools.simple.runtime.components.android;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.Contacts;
import android.util.Log;
import com.google.devtools.simple.common.ComponentCategory;
import com.google.devtools.simple.common.PropertyCategory;
import com.google.devtools.simple.runtime.annotations.DesignerComponent;
import com.google.devtools.simple.runtime.annotations.SimpleObject;
import com.google.devtools.simple.runtime.annotations.SimpleProperty;
import com.google.devtools.simple.runtime.annotations.UsesPermissions;
import com.google.devtools.simple.runtime.components.util.ErrorMessages;
import gnu.kawa.xml.ElementType;

@SimpleObject
@UsesPermissions(permissionNames = "android.permission.READ_CONTACTS")
@DesignerComponent(category = ComponentCategory.SOCIAL, description = "<p>A button that, when clicked on, displays a list of the contacts to choose among. After the user has made a selection, the following properties will be set to information about the chosen contact: <ul><li> <code>ContactName</code>: the contact's name </li> <li> <code>EmailAddress</code>: the contact's primary email address </li> <li> <code>Picture</code>: the name of the file containing the contact's image, which can be used as a <code>Picture</code> property value for the <code>Image</code> or <code>ImageSprite</code> component.</li></ul></p><p>Other properties affect the appearance of the button (<code>TextAlignment</code>, <code>BackgroundColor</code>, etc.) and whether it can be clicked on (<code>Enabled</code>).</p>Picking is not supported on all phones.  If it fails, this component will show a notification.  The error behavior can be overridden with the Screen.ErrorOccurred event handler.</p>", version = 3)
public class ContactPicker extends Picker implements ActivityResultListener {
    private static final int EMAIL_INDEX = 1;
    private static final int NAME_INDEX = 0;
    private static final String[] PROJECTION = {"name", "primary_email"};
    protected final Activity activityContext;
    protected String contactName;
    protected String contactPictureUri;
    protected String emailAddress;
    private final Uri intentUri;

    public ContactPicker(ComponentContainer container) {
        this(container, Contacts.People.CONTENT_URI);
    }

    protected ContactPicker(ComponentContainer container, Uri intentUri2) {
        super(container);
        this.activityContext = container.$context();
        this.intentUri = intentUri2;
    }

    @SimpleProperty(category = PropertyCategory.BEHAVIOR)
    public String Picture() {
        return ensureNotNull(this.contactPictureUri);
    }

    @SimpleProperty(category = PropertyCategory.BEHAVIOR)
    public String ContactName() {
        return ensureNotNull(this.contactName);
    }

    @SimpleProperty(category = PropertyCategory.BEHAVIOR)
    public String EmailAddress() {
        return ensureNotNull(this.emailAddress);
    }

    /* access modifiers changed from: protected */
    public Intent getIntent() {
        return new Intent("android.intent.action.PICK", this.intentUri);
    }

    public void resultReturned(int requestCode, int resultCode, Intent data) {
        if (requestCode == this.requestCode && resultCode == -1) {
            Log.i("ContactPicker", "received intent is " + data);
            Uri contactUri = data.getData();
            if (checkContactUri(contactUri, "//contacts/people")) {
                Cursor cursor = null;
                try {
                    cursor = this.activityContext.getContentResolver().query(contactUri, PROJECTION, null, null, null);
                    if (cursor.moveToFirst()) {
                        this.contactName = guardCursorGetString(cursor, 0);
                        this.emailAddress = getEmailAddress(guardCursorGetString(cursor, 1));
                        this.contactPictureUri = contactUri.toString();
                        Log.i("ContactPicker", "Contact name = " + this.contactName + ", email address = " + this.emailAddress + ", contactPhotoUri = " + this.contactPictureUri);
                    }
                } catch (Exception e) {
                    Log.i("ContactPicker", "checkContactUri failed: D");
                    puntContactSelection(ErrorMessages.ERROR_PHONE_UNSUPPORTED_CONTACT_PICKER);
                } finally {
                    cursor.close();
                }
            }
            AfterPicking();
        }
    }

    /* access modifiers changed from: protected */
    public boolean checkContactUri(Uri suspectUri, String requiredPattern) {
        Log.i("ContactPicker", "contactUri is " + suspectUri);
        if (suspectUri == null || !"content".equals(suspectUri.getScheme())) {
            Log.i("ContactPicker", "checkContactUri failed: A");
            puntContactSelection(ErrorMessages.ERROR_PHONE_UNSUPPORTED_CONTACT_PICKER);
            return false;
        }
        String UriSpecific = suspectUri.getSchemeSpecificPart();
        if (UriSpecific.startsWith("//com.android.contacts/contact")) {
            Log.i("ContactPicker", "checkContactUri failed: B");
            puntContactSelection(ErrorMessages.ERROR_PHONE_UNSUPPORTED_SEARCH_IN_CONTACT_PICKING);
            return false;
        } else if (UriSpecific.startsWith(requiredPattern)) {
            return true;
        } else {
            Log.i("ContactPicker", "checkContactUri failed: C");
            Log.i("Contact Picker", suspectUri.getPath());
            puntContactSelection(ErrorMessages.ERROR_PHONE_UNSUPPORTED_CONTACT_PICKER);
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public void puntContactSelection(int errorNumber) {
        this.contactName = ElementType.MATCH_ANY_LOCALNAME;
        this.emailAddress = ElementType.MATCH_ANY_LOCALNAME;
        this.contactPictureUri = ElementType.MATCH_ANY_LOCALNAME;
        this.container.$form().dispatchErrorOccurredEvent(this, ElementType.MATCH_ANY_LOCALNAME, errorNumber, new Object[0]);
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: protected */
    public String getEmailAddress(String emailId) {
        try {
            int id = Integer.parseInt(emailId);
            String data = ElementType.MATCH_ANY_LOCALNAME;
            String[] projection = {"data"};
            Cursor cursor = this.activityContext.getContentResolver().query(Contacts.ContactMethods.CONTENT_EMAIL_URI, projection, "contact_methods._id = " + id, null, null);
            try {
                if (cursor.moveToFirst()) {
                    data = guardCursorGetString(cursor, 0);
                }
                cursor.close();
                return ensureNotNull(data);
            } catch (Throwable th) {
                cursor.close();
                throw th;
            }
        } catch (NumberFormatException e) {
            return ElementType.MATCH_ANY_LOCALNAME;
        }
    }

    /* access modifiers changed from: protected */
    public String guardCursorGetString(Cursor cursor, int index) {
        String result;
        try {
            result = cursor.getString(index);
        } catch (Exception e) {
            result = ElementType.MATCH_ANY_LOCALNAME;
        }
        return ensureNotNull(result);
    }

    /* access modifiers changed from: protected */
    public String ensureNotNull(String value) {
        if (value == null) {
            return ElementType.MATCH_ANY_LOCALNAME;
        }
        return value;
    }
}
