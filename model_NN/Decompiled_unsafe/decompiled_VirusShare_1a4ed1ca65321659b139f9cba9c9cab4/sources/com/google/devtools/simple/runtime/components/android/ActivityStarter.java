package com.google.devtools.simple.runtime.components.android;

import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.util.Log;
import com.google.android.googlelogin.GoogleLoginServiceConstants;
import com.google.devtools.simple.common.ComponentCategory;
import com.google.devtools.simple.common.PropertyCategory;
import com.google.devtools.simple.runtime.annotations.DesignerComponent;
import com.google.devtools.simple.runtime.annotations.DesignerProperty;
import com.google.devtools.simple.runtime.annotations.SimpleEvent;
import com.google.devtools.simple.runtime.annotations.SimpleFunction;
import com.google.devtools.simple.runtime.annotations.SimpleObject;
import com.google.devtools.simple.runtime.annotations.SimpleProperty;
import com.google.devtools.simple.runtime.components.Component;
import com.google.devtools.simple.runtime.components.util.ErrorMessages;
import com.google.devtools.simple.runtime.events.EventDispatcher;
import gnu.kawa.xml.ElementType;

@SimpleObject
@DesignerComponent(category = ComponentCategory.MISC, description = "<p>A component that can launch an activity using the <code>StartActivity</code> method.</p><p>Activities that can be launched include:<ul> <li> Starting another App Inventor for Android app.  To do so, first      find out the <em>class</em> of the other application by      downloading the source code and using a file explorer or unzip      utility to find a file named      \"youngandroidproject/project.properties\".  The first line of      the file will start with \"main=\" and be followed by the class      name; for example,      <code>main=com.gmail.Bitdiddle.Ben.HelloPurr.Screen1</code>.       (The first components indicate that it was created by      Ben.Bitdiddle@gmail.com.)  To make your      <code>ActivityStarter</code> launch this application, set the      following properties: <ul>      <li> <code>ActivityPackage</code> to the class name, dropping the           last component (for example,           <code>com.gmail.Bitdiddle.Ben.HelloPurr</code>)</li>      <li> <code>ActivityClass</code> to the entire class name (for           example,           <code>com.gmail.Bitdiddle.Ben.HelloPurr.Screen1</code>)</li>      </ul></li><li> Starting the camera application by setting the following      properties:<ul>      <li> <code>Action: android.intent.action.MAIN</code> </li>      <li> <code>ActivityPackage: com.android.camera</code> </li>      <li> <code>ActivityClass: com.android.camera.Camera</code></li>      </ul></li><li> Performing web search.  Assuming the term you want to search      for is \"vampire\" (feel free to substitute your own choice),      set the properties to:<blockquote><code>      Action: android.intent.action.WEB_SEARCH<br/>      ExtraKey: query<br/>      ExtraValue: vampire<br/>      ActivityPackage: com.google.android.providers.enhancedgooglesearch<br/>     ActivityClass: com.google.android.providers.enhancedgooglesearch.Launcher<br/>      </code></blockquote></li> <li> Opening a browser to a specified web page.  Assuming the page you      want to go to is \"www.facebook.com\" (feel free to substitute      your own choice), set the properties to: <blockquote><code>      Action: android.intent.action.VIEW <br/>      DataUri: http://www.facebook.com </code> </blockquote> </li> </ul></p>", designerHelpDescription = "<p>A component that can launch an activity using the <code>StartActivity</code> method.</p><p>Activities that can be launched include:<ul> <li> starting other App Inventor for Android apps </li> <li> starting the camera application </li> <li> performing web search </li> <li> opening a browser to a specified web page</li> <li> opening the map application to a specified location</li></ul> You can also launch activities that return text data.  See the documentation on using the Activity Starter for examples.</p>", iconName = "images/activityStarter.png", nonVisible = true, version = 3)
public class ActivityStarter extends AndroidNonvisibleComponent implements ActivityResultListener, Component, Deleteable {
    private String action;
    private String activityClass;
    private String activityPackage;
    private final ComponentContainer container;
    private String dataType;
    private String dataUri;
    private String extraKey;
    private String extraValue;
    private int requestCode;
    private String result = ElementType.MATCH_ANY_LOCALNAME;
    private Intent resultIntent;
    private String resultName;

    public ActivityStarter(ComponentContainer container2) {
        super(container2.$form());
        this.container = container2;
        Action("android.intent.action.MAIN");
        ActivityPackage(ElementType.MATCH_ANY_LOCALNAME);
        ActivityClass(ElementType.MATCH_ANY_LOCALNAME);
        DataUri(ElementType.MATCH_ANY_LOCALNAME);
        DataType(ElementType.MATCH_ANY_LOCALNAME);
        ExtraKey(ElementType.MATCH_ANY_LOCALNAME);
        ExtraValue(ElementType.MATCH_ANY_LOCALNAME);
        ResultName(ElementType.MATCH_ANY_LOCALNAME);
    }

    @SimpleProperty(category = PropertyCategory.BEHAVIOR)
    public String Action() {
        return this.action;
    }

    @SimpleProperty
    @DesignerProperty(defaultValue = ElementType.MATCH_ANY_LOCALNAME, editorType = DesignerProperty.PROPERTY_TYPE_STRING)
    public void Action(String action2) {
        this.action = action2.trim();
    }

    @SimpleProperty(category = PropertyCategory.BEHAVIOR)
    public String ExtraKey() {
        return this.extraKey;
    }

    @SimpleProperty
    @DesignerProperty(defaultValue = ElementType.MATCH_ANY_LOCALNAME, editorType = DesignerProperty.PROPERTY_TYPE_STRING)
    public void ExtraKey(String extraKey2) {
        this.extraKey = extraKey2.trim();
    }

    @SimpleProperty(category = PropertyCategory.BEHAVIOR)
    public String ExtraValue() {
        return this.extraValue;
    }

    @SimpleProperty
    @DesignerProperty(defaultValue = ElementType.MATCH_ANY_LOCALNAME, editorType = DesignerProperty.PROPERTY_TYPE_STRING)
    public void ExtraValue(String extraValue2) {
        this.extraValue = extraValue2.trim();
    }

    @SimpleProperty(category = PropertyCategory.BEHAVIOR)
    public String ResultName() {
        return this.resultName;
    }

    @SimpleProperty
    @DesignerProperty(defaultValue = ElementType.MATCH_ANY_LOCALNAME, editorType = DesignerProperty.PROPERTY_TYPE_STRING)
    public void ResultName(String resultName2) {
        this.resultName = resultName2.trim();
    }

    @SimpleProperty(category = PropertyCategory.BEHAVIOR)
    public String Result() {
        return this.result;
    }

    @SimpleProperty(category = PropertyCategory.BEHAVIOR)
    public String DataUri() {
        return this.dataUri;
    }

    @SimpleProperty
    @DesignerProperty(defaultValue = ElementType.MATCH_ANY_LOCALNAME, editorType = DesignerProperty.PROPERTY_TYPE_STRING)
    public void DataUri(String dataUri2) {
        this.dataUri = dataUri2.trim();
    }

    @SimpleProperty(category = PropertyCategory.BEHAVIOR)
    public String DataType() {
        return this.dataType;
    }

    @SimpleProperty
    @DesignerProperty(defaultValue = ElementType.MATCH_ANY_LOCALNAME, editorType = DesignerProperty.PROPERTY_TYPE_STRING)
    public void DataType(String dataType2) {
        this.dataType = dataType2.trim();
    }

    @SimpleProperty(category = PropertyCategory.BEHAVIOR)
    public String ActivityPackage() {
        return this.activityPackage;
    }

    @SimpleProperty
    @DesignerProperty(defaultValue = ElementType.MATCH_ANY_LOCALNAME, editorType = DesignerProperty.PROPERTY_TYPE_STRING)
    public void ActivityPackage(String activityPackage2) {
        this.activityPackage = activityPackage2.trim();
    }

    @SimpleProperty(category = PropertyCategory.BEHAVIOR)
    public String ActivityClass() {
        return this.activityClass;
    }

    @SimpleProperty
    @DesignerProperty(defaultValue = ElementType.MATCH_ANY_LOCALNAME, editorType = DesignerProperty.PROPERTY_TYPE_STRING)
    public void ActivityClass(String activityClass2) {
        this.activityClass = activityClass2.trim();
    }

    @SimpleEvent(description = "Event raised after this ActivityStarter returns.")
    public void AfterActivity(String result2) {
        EventDispatcher.dispatchEvent(this, "AfterActivity", result2);
    }

    @SimpleProperty(category = PropertyCategory.BEHAVIOR)
    public String ResultType() {
        String resultType;
        return (this.resultIntent == null || (resultType = this.resultIntent.getType()) == null) ? ElementType.MATCH_ANY_LOCALNAME : resultType;
    }

    @SimpleProperty(category = PropertyCategory.BEHAVIOR)
    public String ResultUri() {
        String resultUri;
        return (this.resultIntent == null || (resultUri = this.resultIntent.getDataString()) == null) ? ElementType.MATCH_ANY_LOCALNAME : resultUri;
    }

    @SimpleFunction(description = "Returns the name of the activity that corresponds to this ActivityStarer, or an empty string if no corresponding activity can be found.")
    public String ResolveActivity() {
        ResolveInfo resolveInfo = this.container.$context().getPackageManager().resolveActivity(buildActivityIntent(), 0);
        if (resolveInfo == null || resolveInfo.activityInfo == null) {
            return ElementType.MATCH_ANY_LOCALNAME;
        }
        return resolveInfo.activityInfo.name;
    }

    @SimpleFunction(description = "Start the activity corresponding to this ActivityStarter.")
    public void StartActivity() {
        this.resultIntent = null;
        this.result = ElementType.MATCH_ANY_LOCALNAME;
        Intent intent = buildActivityIntent();
        if (this.requestCode == 0) {
            this.requestCode = this.form.registerForActivityResult(this);
        }
        try {
            this.container.$context().startActivityForResult(intent, this.requestCode);
        } catch (ActivityNotFoundException e) {
            this.form.dispatchErrorOccurredEvent(this, "StartActivity", ErrorMessages.ERROR_ACTIVITY_STARTER_NO_CORRESPONDING_ACTIVITY, new Object[0]);
        }
    }

    private Intent buildActivityIntent() {
        Uri uri = this.dataUri.length() != 0 ? Uri.parse(this.dataUri) : null;
        Intent intent = uri != null ? new Intent(this.action, uri) : new Intent(this.action);
        if (this.dataType.length() != 0) {
            if (uri != null) {
                intent.setDataAndType(uri, this.dataType);
            } else {
                intent.setType(this.dataType);
            }
        }
        if (!(this.activityPackage.length() == 0 && this.activityClass.length() == 0)) {
            intent.setComponent(new ComponentName(this.activityPackage, this.activityClass));
        }
        if (!(this.extraKey.length() == 0 || this.extraValue.length() == 0)) {
            intent.putExtra(this.extraKey, this.extraValue);
        }
        return intent;
    }

    public void resultReturned(int requestCode2, int resultCode, Intent data) {
        if (requestCode2 == this.requestCode) {
            Log.i("ActivityStarter", "resultReturned - resultCode = " + resultCode);
            if (resultCode == -1) {
                this.resultIntent = data;
                if (this.resultName.length() == 0 || this.resultIntent == null || !this.resultIntent.hasExtra(this.resultName)) {
                    this.result = ElementType.MATCH_ANY_LOCALNAME;
                } else {
                    this.result = this.resultIntent.getStringExtra(this.resultName);
                }
                AfterActivity(this.result);
            }
        }
    }

    @SimpleEvent(description = "The ActivityError event is no longer used. Please use the Screen.ErrorOccurred event instead.", userVisible = GoogleLoginServiceConstants.PREFER_HOSTED)
    public void ActivityError(String message) {
    }

    public void onDelete() {
        this.form.unregisterForActivityResult(this);
    }
}
