package com.google.devtools.simple.runtime.components.android.util;

import android.app.Activity;
import android.speech.tts.TextToSpeech;
import com.google.devtools.simple.runtime.components.android.util.ITextToSpeech;
import java.util.HashMap;
import java.util.Locale;

public class InternalTextToSpeech implements ITextToSpeech {
    /* access modifiers changed from: private */
    public final Activity activity;
    /* access modifiers changed from: private */
    public final ITextToSpeech.TextToSpeechCallback callback;
    /* access modifiers changed from: private */
    public volatile boolean isTtsInitialized;
    private int nextUtteranceId = 1;
    private TextToSpeech tts;

    public InternalTextToSpeech(Activity activity2, ITextToSpeech.TextToSpeechCallback callback2) {
        this.activity = activity2;
        this.callback = callback2;
        initializeTts();
    }

    private void initializeTts() {
        if (this.tts == null) {
            this.tts = new TextToSpeech(this.activity, new TextToSpeech.OnInitListener() {
                public void onInit(int status) {
                    if (status == 0) {
                        boolean unused = InternalTextToSpeech.this.isTtsInitialized = true;
                    }
                }
            });
        }
    }

    public void speak(String message, Locale loc) {
        if (this.isTtsInitialized) {
            this.tts.setLanguage(loc);
            this.tts.setOnUtteranceCompletedListener(new TextToSpeech.OnUtteranceCompletedListener() {
                public void onUtteranceCompleted(String utteranceId) {
                    InternalTextToSpeech.this.activity.runOnUiThread(new Runnable() {
                        public void run() {
                            InternalTextToSpeech.this.callback.onSuccess();
                        }
                    });
                }
            });
            HashMap<String, String> params = new HashMap<>();
            int i = this.nextUtteranceId;
            this.nextUtteranceId = i + 1;
            params.put("utteranceId", Integer.toString(i));
            TextToSpeech textToSpeech = this.tts;
            TextToSpeech textToSpeech2 = this.tts;
            if (textToSpeech.speak(message, 0, params) == -1) {
                this.callback.onFailure();
                return;
            }
            return;
        }
        this.callback.onFailure();
    }

    public void onStop() {
        if (this.tts != null) {
            this.tts.shutdown();
            this.isTtsInitialized = false;
            this.tts = null;
        }
    }

    public void onResume() {
        initializeTts();
    }
}
