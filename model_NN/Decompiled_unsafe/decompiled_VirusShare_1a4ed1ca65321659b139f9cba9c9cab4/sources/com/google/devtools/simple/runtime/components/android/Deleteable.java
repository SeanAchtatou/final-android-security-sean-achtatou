package com.google.devtools.simple.runtime.components.android;

public interface Deleteable {
    void onDelete();
}
