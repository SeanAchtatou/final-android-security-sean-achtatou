package com.google.devtools.simple.runtime.components.android;

import android.os.Handler;
import com.google.devtools.simple.common.ComponentCategory;
import com.google.devtools.simple.common.PropertyCategory;
import com.google.devtools.simple.runtime.annotations.DesignerComponent;
import com.google.devtools.simple.runtime.annotations.DesignerProperty;
import com.google.devtools.simple.runtime.annotations.SimpleEvent;
import com.google.devtools.simple.runtime.annotations.SimpleFunction;
import com.google.devtools.simple.runtime.annotations.SimpleObject;
import com.google.devtools.simple.runtime.annotations.SimpleProperty;
import com.google.devtools.simple.runtime.annotations.UsesPermissions;
import com.google.devtools.simple.runtime.components.Component;
import com.google.devtools.simple.runtime.components.android.collect.Lists;
import com.google.devtools.simple.runtime.components.android.util.AsyncCallbackPair;
import com.google.devtools.simple.runtime.components.android.util.AsynchUtil;
import com.google.devtools.simple.runtime.components.android.util.WebServiceUtil;
import com.google.devtools.simple.runtime.components.util.JsonUtil;
import com.google.devtools.simple.runtime.errors.YailRuntimeError;
import com.google.devtools.simple.runtime.events.EventDispatcher;
import gnu.kawa.xml.ElementType;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;

@SimpleObject
@UsesPermissions(permissionNames = "android.permission.INTERNET")
@DesignerComponent(category = ComponentCategory.MISC, description = "Non-visible component that communicates with a Web service to store and retrieve information.", iconName = "images/tinyWebDB.png", nonVisible = true, version = 2)
public class TinyWebDB extends AndroidNonvisibleComponent implements Component {
    private static final String GETVALUE_COMMAND = "getvalue";
    private static final String LOG_TAG = "TinyWebDB";
    private static final String STOREAVALUE_COMMAND = "storeavalue";
    private static final String TAG_PARAMETER = "tag";
    private static final String VALUE_PARAMETER = "value";
    /* access modifiers changed from: private */
    public Handler androidUIHandler = new Handler();
    private String serviceURL = "http://appinvtinywebdb.appspot.com/";

    public TinyWebDB(ComponentContainer container) {
        super(container.$form());
    }

    @SimpleProperty(category = PropertyCategory.BEHAVIOR)
    public String ServiceURL() {
        return this.serviceURL;
    }

    @SimpleProperty
    @DesignerProperty(defaultValue = "http://appinvtinywebdb.appspot.com", editorType = DesignerProperty.PROPERTY_TYPE_STRING)
    public void ServiceURL(String url) {
        this.serviceURL = url;
    }

    @SimpleFunction
    public void StoreValue(final String tag, final Object valueToStore) {
        AsynchUtil.runAsynchronously(new Runnable() {
            public void run() {
                TinyWebDB.this.postStoreValue(tag, valueToStore);
            }
        });
    }

    /* access modifiers changed from: private */
    public void postStoreValue(String tag, Object valueToStore) {
        AsyncCallbackPair<String> myCallback = new AsyncCallbackPair<String>() {
            public void onSuccess(String response) {
                TinyWebDB.this.androidUIHandler.post(new Runnable() {
                    public void run() {
                        TinyWebDB.this.ValueStored();
                    }
                });
            }

            public void onFailure(final String message) {
                TinyWebDB.this.androidUIHandler.post(new Runnable() {
                    public void run() {
                        TinyWebDB.this.WebServiceError(message);
                    }
                });
            }
        };
        try {
            WebServiceUtil.getInstance().postCommand(this.serviceURL, STOREAVALUE_COMMAND, Lists.newArrayList(new BasicNameValuePair((String) TAG_PARAMETER, tag), new BasicNameValuePair((String) VALUE_PARAMETER, JsonUtil.getJsonRepresentation(valueToStore))), myCallback);
        } catch (JSONException e) {
            throw new YailRuntimeError("Value failed to convert to JSON.", "JSON Creation Error.");
        }
    }

    @SimpleEvent
    public void ValueStored() {
        EventDispatcher.dispatchEvent(this, "ValueStored", new Object[0]);
    }

    @SimpleFunction
    public void GetValue(final String tag) {
        AsynchUtil.runAsynchronously(new Runnable() {
            public void run() {
                TinyWebDB.this.postGetValue(tag);
            }
        });
    }

    /* access modifiers changed from: private */
    public void postGetValue(final String tag) {
        AsyncCallbackPair<JSONArray> myCallback = new AsyncCallbackPair<JSONArray>() {
            public void onSuccess(JSONArray result) {
                if (result == null) {
                    TinyWebDB.this.androidUIHandler.post(new Runnable() {
                        public void run() {
                            TinyWebDB.this.WebServiceError("The Web server did not respond to the get value request for the tag " + tag + ".");
                        }
                    });
                    return;
                }
                try {
                    final String tagFromWebDB = result.getString(1);
                    String value = result.getString(2);
                    final Object valueFromWebDB = value.length() == 0 ? ElementType.MATCH_ANY_LOCALNAME : JsonUtil.getObjectFromJson(value);
                    TinyWebDB.this.androidUIHandler.post(new Runnable() {
                        public void run() {
                            TinyWebDB.this.GotValue(tagFromWebDB, valueFromWebDB);
                        }
                    });
                } catch (JSONException e) {
                    TinyWebDB.this.androidUIHandler.post(new Runnable() {
                        public void run() {
                            TinyWebDB.this.WebServiceError("The Web server returned a garbled value for the tag " + tag + ".");
                        }
                    });
                }
            }

            public void onFailure(final String message) {
                TinyWebDB.this.androidUIHandler.post(new Runnable() {
                    public void run() {
                        TinyWebDB.this.WebServiceError(message);
                    }
                });
            }
        };
        WebServiceUtil.getInstance().postCommandReturningArray(this.serviceURL, GETVALUE_COMMAND, Lists.newArrayList(new BasicNameValuePair((String) TAG_PARAMETER, tag)), myCallback);
    }

    @SimpleEvent
    public void GotValue(String tagFromWebDB, Object valueFromWebDB) {
        EventDispatcher.dispatchEvent(this, "GotValue", tagFromWebDB, valueFromWebDB);
    }

    @SimpleEvent
    public void WebServiceError(String message) {
        EventDispatcher.dispatchEvent(this, "WebServiceError", message);
    }
}
