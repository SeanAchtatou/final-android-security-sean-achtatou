package com.google.devtools.simple.runtime.components.android.util;

public interface AsyncCallbackPair<T> {
    void onFailure(String str);

    void onSuccess(Object obj);
}
