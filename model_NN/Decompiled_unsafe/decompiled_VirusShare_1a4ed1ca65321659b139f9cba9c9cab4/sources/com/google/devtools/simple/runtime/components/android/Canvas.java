package com.google.devtools.simple.runtime.components.android;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import com.google.android.googlelogin.GoogleLoginServiceConstants;
import com.google.devtools.simple.common.ComponentCategory;
import com.google.devtools.simple.common.PropertyCategory;
import com.google.devtools.simple.runtime.annotations.DesignerComponent;
import com.google.devtools.simple.runtime.annotations.DesignerProperty;
import com.google.devtools.simple.runtime.annotations.SimpleEvent;
import com.google.devtools.simple.runtime.annotations.SimpleFunction;
import com.google.devtools.simple.runtime.annotations.SimpleObject;
import com.google.devtools.simple.runtime.annotations.SimpleProperty;
import com.google.devtools.simple.runtime.annotations.UsesPermissions;
import com.google.devtools.simple.runtime.components.Component;
import com.google.devtools.simple.runtime.components.android.util.FileUtil;
import com.google.devtools.simple.runtime.components.android.util.MediaUtil;
import com.google.devtools.simple.runtime.components.android.util.PaintUtil;
import com.google.devtools.simple.runtime.components.util.BoundingBox;
import com.google.devtools.simple.runtime.components.util.ErrorMessages;
import com.google.devtools.simple.runtime.events.EventDispatcher;
import gnu.kawa.xml.ElementType;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

@SimpleObject
@DesignerComponent(category = ComponentCategory.BASIC, description = "<p>A two-dimensional touch-sensitive rectangular panel on which drawing can be done and sprites can be moved.</p> <p>The <code>BackgroundColor</code>, <code>PaintColor</code>, <code>BackgroundImage</code>, <code>Width</code>, and <code>Height</code> of the Canvas can be set in either the Designer or in the Blocks Editor.  The <code>Width</code> and <code>Height</code> are measured in pixels and must be positive.</p><p>Any location on the Canvas can be specified as a pair of (X, Y) values, where <ul> <li>X is the number of pixels away from the left edge of the Canvas</li><li>Y is the number of pixels away from the top edge of the Canvas</li></ul></p> <p>There are events to tell when and where a Canvas has been touched or a <code>Sprite</code> (<code>ImageSprite</code> or <code>Ball</code>) has been dragged.  There are also methods for drawing points, lines, and circles.</p>", version = 5)
@UsesPermissions(permissionNames = "android.permission.INTERNET,android.permission.WRITE_EXTERNAL_STORAGE")
public final class Canvas extends AndroidViewComponent implements ComponentContainer {
    private static final int DEFAULT_BACKGROUND_COLOR = -1;
    private static final float DEFAULT_LINE_WIDTH = 2.0f;
    private static final int DEFAULT_PAINT_COLOR = -16777216;
    private static final String LOG_TAG = "Canvas";
    /* access modifiers changed from: private */
    public int backgroundColor;
    /* access modifiers changed from: private */
    public String backgroundImagePath = ElementType.MATCH_ANY_LOCALNAME;
    private final Activity context;
    /* access modifiers changed from: private */
    public boolean drawn;
    /* access modifiers changed from: private */
    public final MotionEventParser motionEventParser;
    /* access modifiers changed from: private */
    public final Paint paint;
    private int paintColor;
    /* access modifiers changed from: private */
    public final List<Sprite> sprites;
    private int textAlignment;
    private final CanvasView view;

    class MotionEventParser {
        public static final int FINGER_HEIGHT = 24;
        public static final int FINGER_WIDTH = 24;
        private static final int HALF_FINGER_HEIGHT = 12;
        private static final int HALF_FINGER_WIDTH = 12;
        public static final int TAP_THRESHOLD = 30;
        private static final int UNSET = -1;
        private boolean drag = false;
        private final List<Sprite> draggedSprites = new ArrayList();
        private boolean isDrag = false;
        private float lastX = -1.0f;
        private float lastY = -1.0f;
        private float startX = -1.0f;
        private float startY = -1.0f;

        MotionEventParser() {
        }

        /* access modifiers changed from: package-private */
        public void parse(MotionEvent event) {
            int width = Canvas.this.Width();
            int height = Canvas.this.Height();
            float x = (float) Math.max(0, (int) event.getX());
            float y = (float) Math.max(0, (int) event.getY());
            BoundingBox rect = new BoundingBox((double) Math.max(0, ((int) x) - 12), (double) Math.max(0, ((int) y) - 12), (double) Math.min(width - 1, ((int) x) + 12), (double) Math.min(height - 1, ((int) y) + 12));
            switch (event.getAction()) {
                case 0:
                    this.draggedSprites.clear();
                    this.startX = x;
                    this.startY = y;
                    this.lastX = x;
                    this.lastY = y;
                    this.drag = false;
                    this.isDrag = false;
                    for (Sprite sprite : Canvas.this.sprites) {
                        if (sprite.Enabled() && sprite.Visible() && sprite.intersectsWith(rect)) {
                            this.draggedSprites.add(sprite);
                        }
                    }
                    return;
                case 1:
                    if (!this.drag) {
                        boolean handled = false;
                        for (Sprite sprite2 : this.draggedSprites) {
                            if (sprite2.Enabled() && sprite2.Visible()) {
                                sprite2.Touched(this.startX, this.startY);
                                handled = true;
                            }
                        }
                        Canvas.this.Touched(this.startX, this.startY, handled);
                    }
                    this.drag = false;
                    this.startX = -1.0f;
                    this.startY = -1.0f;
                    this.lastX = -1.0f;
                    this.lastY = -1.0f;
                    return;
                case 2:
                    if (this.startX == -1.0f || this.startY == -1.0f || this.lastX == -1.0f || this.lastY == -1.0f) {
                        Log.w(Canvas.LOG_TAG, "In Canvas.MotionEventParser.parse(), an ACTION_MOVE was passed without a preceding ACTION_DOWN: " + event);
                    }
                    if (this.isDrag || Math.abs(x - this.startX) >= 30.0f || Math.abs(y - this.startY) >= 30.0f) {
                        this.isDrag = true;
                        this.drag = true;
                        for (Sprite sprite3 : Canvas.this.sprites) {
                            if (!this.draggedSprites.contains(sprite3) && sprite3.Enabled() && sprite3.Visible() && sprite3.intersectsWith(rect)) {
                                this.draggedSprites.add(sprite3);
                            }
                        }
                        boolean handled2 = false;
                        for (Sprite sprite4 : this.draggedSprites) {
                            if (sprite4.Enabled() && sprite4.Visible()) {
                                sprite4.Dragged(this.startX, this.startY, this.lastX, this.lastY, x, y);
                                handled2 = true;
                            }
                        }
                        Canvas.this.Dragged(this.startX, this.startY, this.lastX, this.lastY, x, y, handled2);
                        this.lastX = x;
                        this.lastY = y;
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    private final class CanvasView extends View {
        private BitmapDrawable backgroundDrawable;
        private Bitmap bitmap = Bitmap.createBitmap(32, 48, Bitmap.Config.ARGB_8888);
        /* access modifiers changed from: private */
        public android.graphics.Canvas canvas = new android.graphics.Canvas(this.bitmap);
        /* access modifiers changed from: private */
        public Bitmap completeCache;
        private Bitmap scaledBackgroundBitmap;

        public CanvasView(Context context) {
            super(context);
        }

        /* access modifiers changed from: private */
        public Bitmap buildCache() {
            setDrawingCacheEnabled(true);
            destroyDrawingCache();
            Bitmap cache = getDrawingCache();
            if (cache != null) {
                return cache;
            }
            int width = getWidth();
            int height = getHeight();
            Bitmap cache2 = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            android.graphics.Canvas c = new android.graphics.Canvas(cache2);
            layout(0, 0, width, height);
            draw(c);
            return cache2;
        }

        public void onDraw(android.graphics.Canvas canvas0) {
            this.completeCache = null;
            super.onDraw(canvas0);
            canvas0.drawBitmap(this.bitmap, 0.0f, 0.0f, (Paint) null);
            for (Sprite sprite : Canvas.this.sprites) {
                sprite.onDraw(canvas0);
            }
            boolean unused = Canvas.this.drawn = true;
        }

        /* access modifiers changed from: protected */
        public void onSizeChanged(int w, int h, int oldW, int oldH) {
            int oldBitmapWidth = this.bitmap.getWidth();
            int oldBitmapHeight = this.bitmap.getHeight();
            if (w != oldBitmapWidth || h != oldBitmapHeight) {
                Bitmap oldBitmap = this.bitmap;
                Bitmap scaledBitmap = Bitmap.createScaledBitmap(oldBitmap, w, h, false);
                if (scaledBitmap.isMutable()) {
                    this.bitmap = scaledBitmap;
                    this.canvas = new android.graphics.Canvas(this.bitmap);
                } else {
                    this.bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
                    this.canvas = new android.graphics.Canvas(this.bitmap);
                    this.canvas.drawBitmap(oldBitmap, new Rect(0, 0, oldBitmapWidth, oldBitmapHeight), new RectF(0.0f, 0.0f, (float) w, (float) h), (Paint) null);
                }
                this.scaledBackgroundBitmap = null;
            }
        }

        /* access modifiers changed from: protected */
        public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            int preferredWidth;
            int preferredHeight;
            if (this.backgroundDrawable != null) {
                Bitmap bitmap2 = this.backgroundDrawable.getBitmap();
                preferredWidth = bitmap2.getWidth();
                preferredHeight = bitmap2.getHeight();
            } else {
                preferredWidth = 32;
                preferredHeight = 48;
            }
            setMeasuredDimension(getSize(widthMeasureSpec, preferredWidth), getSize(heightMeasureSpec, preferredHeight));
        }

        private int getSize(int measureSpec, int preferredSize) {
            int specMode = View.MeasureSpec.getMode(measureSpec);
            int specSize = View.MeasureSpec.getSize(measureSpec);
            if (specMode == 1073741824) {
                return specSize;
            }
            int result = preferredSize;
            if (specMode == Integer.MIN_VALUE) {
                return Math.min(result, specSize);
            }
            return result;
        }

        public boolean onTouchEvent(MotionEvent event) {
            Canvas.this.container.$form().dontGrabTouchEventsForComponent();
            Canvas.this.motionEventParser.parse(event);
            return true;
        }

        /* access modifiers changed from: package-private */
        public void setBackgroundImage(String path) {
            Canvas canvas2 = Canvas.this;
            if (path == null) {
                path = ElementType.MATCH_ANY_LOCALNAME;
            }
            String unused = canvas2.backgroundImagePath = path;
            this.backgroundDrawable = null;
            this.scaledBackgroundBitmap = null;
            if (!TextUtils.isEmpty(Canvas.this.backgroundImagePath)) {
                try {
                    this.backgroundDrawable = MediaUtil.getBitmapDrawable(Canvas.this.container.$form(), Canvas.this.backgroundImagePath);
                } catch (IOException e) {
                    Log.e(Canvas.LOG_TAG, "Unable to load " + Canvas.this.backgroundImagePath);
                }
            }
            setBackgroundDrawable(this.backgroundDrawable);
            if (this.backgroundDrawable == null) {
                super.setBackgroundColor(Canvas.this.backgroundColor);
            }
            clearDrawingLayer();
        }

        /* access modifiers changed from: private */
        public void clearDrawingLayer() {
            this.canvas.drawColor(0, PorterDuff.Mode.CLEAR);
            invalidate();
        }

        public void setBackgroundColor(int color) {
            int unused = Canvas.this.backgroundColor = color;
            if (this.backgroundDrawable == null) {
                super.setBackgroundColor(color);
            }
            clearDrawingLayer();
        }

        /* access modifiers changed from: private */
        public void drawTextAtAngle(String text, int x, int y, float angle) {
            this.canvas.save();
            this.canvas.rotate(-angle, (float) x, (float) y);
            this.canvas.drawText(text, (float) x, (float) y, Canvas.this.paint);
            this.canvas.restore();
            invalidate();
        }

        /* access modifiers changed from: private */
        public int getBackgroundPixelColor(int x, int y) {
            if (x < 0 || x >= this.bitmap.getWidth() || y < 0 || y >= this.bitmap.getHeight()) {
                return Component.COLOR_NONE;
            }
            try {
                int color = this.bitmap.getPixel(x, y);
                if (color != 0) {
                    return color;
                }
                if (this.backgroundDrawable == null) {
                    return Color.alpha(Canvas.this.backgroundColor) != 0 ? Canvas.this.backgroundColor : Component.COLOR_NONE;
                }
                if (this.scaledBackgroundBitmap == null) {
                    this.scaledBackgroundBitmap = Bitmap.createScaledBitmap(this.backgroundDrawable.getBitmap(), this.bitmap.getWidth(), this.bitmap.getHeight(), false);
                }
                return this.scaledBackgroundBitmap.getPixel(x, y);
            } catch (IllegalArgumentException e) {
                Log.e(Canvas.LOG_TAG, String.format("Returning COLOR_NONE (exception) from getBackgroundPixelColor.", new Object[0]));
                return Component.COLOR_NONE;
            }
        }

        /* access modifiers changed from: private */
        public int getPixelColor(int x, int y) {
            if (x < 0 || x >= this.bitmap.getWidth() || y < 0 || y >= this.bitmap.getHeight()) {
                return Component.COLOR_NONE;
            }
            if (this.completeCache == null) {
                boolean anySpritesVisible = false;
                Iterator i$ = Canvas.this.sprites.iterator();
                while (true) {
                    if (i$.hasNext()) {
                        if (((Sprite) i$.next()).Visible()) {
                            anySpritesVisible = true;
                            break;
                        }
                    } else {
                        break;
                    }
                }
                if (!anySpritesVisible) {
                    return getBackgroundPixelColor(x, y);
                }
                this.completeCache = buildCache();
            }
            try {
                return this.completeCache.getPixel(x, y);
            } catch (IllegalArgumentException e) {
                Log.e(Canvas.LOG_TAG, String.format("Returning COLOR_NONE (exception) from getPixelColor.", new Object[0]));
                return Component.COLOR_NONE;
            }
        }
    }

    public Canvas(ComponentContainer container) {
        super(container);
        this.context = container.$context();
        this.view = new CanvasView(this.context);
        container.$add(this);
        this.paint = new Paint();
        this.paint.setStrokeWidth(DEFAULT_LINE_WIDTH);
        PaintColor(-16777216);
        BackgroundColor(-1);
        TextAlignment(0);
        FontSize(14.0f);
        this.sprites = new LinkedList();
        this.motionEventParser = new MotionEventParser();
    }

    public View getView() {
        return this.view;
    }

    public boolean ready() {
        return this.drawn;
    }

    /* access modifiers changed from: package-private */
    public void addSprite(Sprite sprite) {
        for (int i = 0; i < this.sprites.size(); i++) {
            if (this.sprites.get(i).Z() > sprite.Z()) {
                this.sprites.add(i, sprite);
                return;
            }
        }
        this.sprites.add(sprite);
    }

    /* access modifiers changed from: package-private */
    public void removeSprite(Sprite sprite) {
        this.sprites.remove(sprite);
    }

    /* access modifiers changed from: package-private */
    public void changeSpriteLayer(Sprite sprite) {
        removeSprite(sprite);
        addSprite(sprite);
        this.view.invalidate();
    }

    public Activity $context() {
        return this.context;
    }

    public Form $form() {
        return this.container.$form();
    }

    public void $add(AndroidViewComponent component) {
        throw new UnsupportedOperationException("Canvas.$add() called");
    }

    public void setChildWidth(AndroidViewComponent component, int width) {
        throw new UnsupportedOperationException("Canvas.setChildWidth() called");
    }

    public void setChildHeight(AndroidViewComponent component, int height) {
        throw new UnsupportedOperationException("Canvas.setChildHeight() called");
    }

    /* access modifiers changed from: package-private */
    public void registerChange(Sprite sprite) {
        this.view.invalidate();
        findSpriteCollisions(sprite);
    }

    /* access modifiers changed from: protected */
    public void findSpriteCollisions(Sprite movedSprite) {
        for (Sprite sprite : this.sprites) {
            if (sprite != movedSprite) {
                if (movedSprite.CollidingWith(sprite)) {
                    if (!movedSprite.Visible() || !movedSprite.Enabled() || !sprite.Visible() || !sprite.Enabled() || !Sprite.colliding(sprite, movedSprite)) {
                        movedSprite.NoLongerCollidingWith(sprite);
                        sprite.NoLongerCollidingWith(movedSprite);
                    }
                } else if (movedSprite.Visible() && movedSprite.Enabled() && sprite.Visible() && sprite.Enabled() && Sprite.colliding(sprite, movedSprite)) {
                    movedSprite.CollidedWith(sprite);
                    sprite.CollidedWith(movedSprite);
                }
            }
        }
    }

    @SimpleProperty(category = PropertyCategory.APPEARANCE, description = "The color of the canvas background.")
    public int BackgroundColor() {
        return this.backgroundColor;
    }

    @SimpleProperty
    @DesignerProperty(defaultValue = Component.DEFAULT_VALUE_COLOR_WHITE, editorType = DesignerProperty.PROPERTY_TYPE_COLOR)
    public void BackgroundColor(int argb) {
        this.view.setBackgroundColor(argb);
    }

    @SimpleProperty(category = PropertyCategory.APPEARANCE, description = "The name of a file containing the background image for the canvas")
    public String BackgroundImage() {
        return this.backgroundImagePath;
    }

    @SimpleProperty
    @DesignerProperty(defaultValue = ElementType.MATCH_ANY_LOCALNAME, editorType = DesignerProperty.PROPERTY_TYPE_ASSET)
    public void BackgroundImage(String path) {
        this.view.setBackgroundImage(path);
    }

    @SimpleProperty(category = PropertyCategory.APPEARANCE, description = "The color in which lines are drawn")
    public int PaintColor() {
        return this.paintColor;
    }

    @SimpleProperty
    @DesignerProperty(defaultValue = Component.DEFAULT_VALUE_COLOR_BLACK, editorType = DesignerProperty.PROPERTY_TYPE_COLOR)
    public void PaintColor(int argb) {
        this.paintColor = argb;
        changePaint(this.paint, argb);
    }

    private void changePaint(Paint paint2, int argb) {
        if (argb == 0) {
            PaintUtil.changePaint(paint2, -16777216);
        } else if (argb == 16777215) {
            PaintUtil.changePaintTransparent(paint2);
        } else {
            PaintUtil.changePaint(paint2, argb);
        }
    }

    @SimpleProperty(category = PropertyCategory.APPEARANCE, description = "The font size of text drawn on the canvas.")
    public float FontSize() {
        return this.paint.getTextSize();
    }

    @SimpleProperty
    @DesignerProperty(defaultValue = "14.0", editorType = DesignerProperty.PROPERTY_TYPE_NON_NEGATIVE_FLOAT)
    public void FontSize(float size) {
        this.paint.setTextSize(size);
    }

    @SimpleProperty(category = PropertyCategory.APPEARANCE, description = "The width of lines drawn on the canvas.")
    public float LineWidth() {
        return this.paint.getStrokeWidth();
    }

    @SimpleProperty
    @DesignerProperty(defaultValue = "2.0", editorType = DesignerProperty.PROPERTY_TYPE_NON_NEGATIVE_FLOAT)
    public void LineWidth(float width) {
        this.paint.setStrokeWidth(width);
    }

    @SimpleProperty(category = PropertyCategory.APPEARANCE, userVisible = GoogleLoginServiceConstants.PREFER_HOSTED)
    public int TextAlignment() {
        return this.textAlignment;
    }

    @SimpleProperty(userVisible = GoogleLoginServiceConstants.PREFER_HOSTED)
    @DesignerProperty(defaultValue = "1", editorType = DesignerProperty.PROPERTY_TYPE_TEXTALIGNMENT)
    public void TextAlignment(int alignment) {
        this.textAlignment = alignment;
        switch (alignment) {
            case 0:
                this.paint.setTextAlign(Paint.Align.LEFT);
                return;
            case 1:
                this.paint.setTextAlign(Paint.Align.CENTER);
                return;
            case 2:
                this.paint.setTextAlign(Paint.Align.RIGHT);
                return;
            default:
                return;
        }
    }

    @SimpleEvent
    public void Touched(float x, float y, boolean touchedSprite) {
        EventDispatcher.dispatchEvent(this, "Touched", Float.valueOf(x), Float.valueOf(y), Boolean.valueOf(touchedSprite));
    }

    @SimpleEvent
    public void Dragged(float startX, float startY, float prevX, float prevY, float currentX, float currentY, boolean draggedSprite) {
        EventDispatcher.dispatchEvent(this, "Dragged", Float.valueOf(startX), Float.valueOf(startY), Float.valueOf(prevX), Float.valueOf(prevY), Float.valueOf(currentX), Float.valueOf(currentY), Boolean.valueOf(draggedSprite));
    }

    @SimpleFunction(description = "Clears anything drawn on this Canvas but not any background color or image.")
    public void Clear() {
        this.view.clearDrawingLayer();
    }

    @SimpleFunction
    public void DrawPoint(int x, int y) {
        this.view.canvas.drawPoint((float) x, (float) y, this.paint);
        this.view.invalidate();
    }

    @SimpleFunction
    public void DrawCircle(int x, int y, float r) {
        this.view.canvas.drawCircle((float) x, (float) y, r, this.paint);
        this.view.invalidate();
    }

    @SimpleFunction
    public void DrawLine(int x1, int y1, int x2, int y2) {
        this.view.canvas.drawLine((float) x1, (float) y1, (float) x2, (float) y2, this.paint);
        this.view.invalidate();
    }

    @SimpleFunction
    public void DrawText(String text, int x, int y) {
        this.view.canvas.drawText(text, (float) x, (float) y, this.paint);
        this.view.invalidate();
    }

    @SimpleFunction
    public void DrawTextAtAngle(String text, int x, int y, float angle) {
        this.view.drawTextAtAngle(text, x, y, angle);
    }

    @SimpleFunction(description = "Gets the color of the specified point. This includes the background and any drawn points, lines, or circles but not sprites.")
    public int GetBackgroundPixelColor(int x, int y) {
        return this.view.getBackgroundPixelColor(x, y);
    }

    @SimpleFunction(description = "Sets the color of the specified point. This differs from DrawPoint by having an argument for color.")
    public void SetBackgroundPixelColor(int x, int y, int color) {
        Paint pixelPaint = new Paint();
        PaintUtil.changePaint(pixelPaint, color);
        this.view.canvas.drawPoint((float) x, (float) y, pixelPaint);
        this.view.invalidate();
    }

    @SimpleFunction(description = "Gets the color of the specified point.")
    public int GetPixelColor(int x, int y) {
        return this.view.getPixelColor(x, y);
    }

    @SimpleFunction
    public String Save() {
        try {
            return saveFile(FileUtil.getPictureFile("png"), Bitmap.CompressFormat.PNG, "Save");
        } catch (IOException e) {
            this.container.$form().dispatchErrorOccurredEvent(this, "Save", ErrorMessages.ERROR_MEDIA_FILE_ERROR, e.getMessage());
        } catch (FileUtil.FileException e2) {
            this.container.$form().dispatchErrorOccurredEvent(this, "Save", e2.getErrorMessageNumber(), new Object[0]);
        }
        return ElementType.MATCH_ANY_LOCALNAME;
    }

    @SimpleFunction
    public String SaveAs(String fileName) {
        Bitmap.CompressFormat format;
        if (fileName.endsWith(".jpg") || fileName.endsWith(".jpeg")) {
            format = Bitmap.CompressFormat.JPEG;
        } else if (fileName.endsWith(".png")) {
            format = Bitmap.CompressFormat.PNG;
        } else if (!fileName.contains(".")) {
            fileName = fileName + ".png";
            format = Bitmap.CompressFormat.PNG;
        } else {
            this.container.$form().dispatchErrorOccurredEvent(this, "SaveAs", ErrorMessages.ERROR_MEDIA_IMAGE_FILE_FORMAT, new Object[0]);
            return ElementType.MATCH_ANY_LOCALNAME;
        }
        try {
            return saveFile(FileUtil.getExternalFile(fileName), format, "SaveAs");
        } catch (IOException e) {
            this.container.$form().dispatchErrorOccurredEvent(this, "SaveAs", ErrorMessages.ERROR_MEDIA_FILE_ERROR, e.getMessage());
        } catch (FileUtil.FileException e2) {
            this.container.$form().dispatchErrorOccurredEvent(this, "SaveAs", e2.getErrorMessageNumber(), new Object[0]);
        }
        return ElementType.MATCH_ANY_LOCALNAME;
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String saveFile(java.io.File r10, android.graphics.Bitmap.CompressFormat r11, java.lang.String r12) {
        /*
            r9 = this;
            r7 = 1
            r8 = 0
            r3 = 0
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x0032, IOException -> 0x0058 }
            r2.<init>(r10)     // Catch:{ FileNotFoundException -> 0x0032, IOException -> 0x0058 }
            com.google.devtools.simple.runtime.components.android.Canvas$CanvasView r4 = r9.view     // Catch:{ FileNotFoundException -> 0x0032, IOException -> 0x0058 }
            android.graphics.Bitmap r4 = r4.completeCache     // Catch:{ FileNotFoundException -> 0x0032, IOException -> 0x0058 }
            if (r4 != 0) goto L_0x0026
            com.google.devtools.simple.runtime.components.android.Canvas$CanvasView r4 = r9.view     // Catch:{ FileNotFoundException -> 0x0032, IOException -> 0x0058 }
            android.graphics.Bitmap r0 = r4.buildCache()     // Catch:{ FileNotFoundException -> 0x0032, IOException -> 0x0058 }
        L_0x0016:
            r4 = 100
            boolean r3 = r0.compress(r11, r4, r2)     // Catch:{ all -> 0x002d }
            r2.close()     // Catch:{ FileNotFoundException -> 0x0032, IOException -> 0x0058 }
            if (r3 == 0) goto L_0x0049
            java.lang.String r4 = r10.getAbsolutePath()     // Catch:{ FileNotFoundException -> 0x0032, IOException -> 0x0058 }
        L_0x0025:
            return r4
        L_0x0026:
            com.google.devtools.simple.runtime.components.android.Canvas$CanvasView r4 = r9.view     // Catch:{ FileNotFoundException -> 0x0032, IOException -> 0x0058 }
            android.graphics.Bitmap r0 = r4.completeCache     // Catch:{ FileNotFoundException -> 0x0032, IOException -> 0x0058 }
            goto L_0x0016
        L_0x002d:
            r4 = move-exception
            r2.close()     // Catch:{ FileNotFoundException -> 0x0032, IOException -> 0x0058 }
            throw r4     // Catch:{ FileNotFoundException -> 0x0032, IOException -> 0x0058 }
        L_0x0032:
            r1 = move-exception
            com.google.devtools.simple.runtime.components.android.ComponentContainer r4 = r9.container
            com.google.devtools.simple.runtime.components.android.Form r4 = r4.$form()
            r5 = 707(0x2c3, float:9.91E-43)
            java.lang.Object[] r6 = new java.lang.Object[r7]
            java.lang.String r7 = r10.getAbsolutePath()
            r6[r8] = r7
            r4.dispatchErrorOccurredEvent(r9, r12, r5, r6)
        L_0x0046:
            java.lang.String r4 = ""
            goto L_0x0025
        L_0x0049:
            com.google.devtools.simple.runtime.components.android.ComponentContainer r4 = r9.container     // Catch:{ FileNotFoundException -> 0x0032, IOException -> 0x0058 }
            com.google.devtools.simple.runtime.components.android.Form r4 = r4.$form()     // Catch:{ FileNotFoundException -> 0x0032, IOException -> 0x0058 }
            r5 = 1001(0x3e9, float:1.403E-42)
            r6 = 0
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ FileNotFoundException -> 0x0032, IOException -> 0x0058 }
            r4.dispatchErrorOccurredEvent(r9, r12, r5, r6)     // Catch:{ FileNotFoundException -> 0x0032, IOException -> 0x0058 }
            goto L_0x0046
        L_0x0058:
            r1 = move-exception
            com.google.devtools.simple.runtime.components.android.ComponentContainer r4 = r9.container
            com.google.devtools.simple.runtime.components.android.Form r4 = r4.$form()
            r5 = 708(0x2c4, float:9.92E-43)
            java.lang.Object[] r6 = new java.lang.Object[r7]
            java.lang.String r7 = r1.getMessage()
            r6[r8] = r7
            r4.dispatchErrorOccurredEvent(r9, r12, r5, r6)
            goto L_0x0046
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.devtools.simple.runtime.components.android.Canvas.saveFile(java.io.File, android.graphics.Bitmap$CompressFormat, java.lang.String):java.lang.String");
    }
}
