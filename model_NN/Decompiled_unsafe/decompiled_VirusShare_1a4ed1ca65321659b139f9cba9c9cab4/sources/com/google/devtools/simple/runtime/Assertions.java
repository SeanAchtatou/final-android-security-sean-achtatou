package com.google.devtools.simple.runtime;

import com.google.devtools.simple.runtime.annotations.SimpleFunction;
import com.google.devtools.simple.runtime.annotations.SimpleObject;
import com.google.devtools.simple.runtime.errors.AssertionFailure;
import com.google.devtools.simple.runtime.variants.Variant;

@SimpleObject
public class Assertions {
    private Assertions() {
    }

    @SimpleFunction
    public static void AssertTrue(Variant expression) {
        if (!expression.getBoolean()) {
            throw new AssertionFailure();
        }
    }

    @SimpleFunction
    public static void AssertFalse(Variant expression) {
        if (expression.getBoolean()) {
            throw new AssertionFailure();
        }
    }
}
