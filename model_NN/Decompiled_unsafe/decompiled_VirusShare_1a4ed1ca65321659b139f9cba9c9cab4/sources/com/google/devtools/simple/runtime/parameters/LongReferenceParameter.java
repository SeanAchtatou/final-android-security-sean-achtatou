package com.google.devtools.simple.runtime.parameters;

public final class LongReferenceParameter extends ReferenceParameter {
    private long value;

    public LongReferenceParameter(long value2) {
        set(value2);
    }

    public long get() {
        return this.value;
    }

    public void set(long value2) {
        this.value = value2;
    }
}
