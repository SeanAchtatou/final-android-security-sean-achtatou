package com.google.devtools.simple.runtime.components.android;

interface BluetoothConnectionListener {
    void afterConnect(BluetoothConnectionBase bluetoothConnectionBase);

    void beforeDisconnect(BluetoothConnectionBase bluetoothConnectionBase);
}
