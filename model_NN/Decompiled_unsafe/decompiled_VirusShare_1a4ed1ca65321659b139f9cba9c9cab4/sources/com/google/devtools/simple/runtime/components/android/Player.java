package com.google.devtools.simple.runtime.components.android;

import android.media.MediaPlayer;
import android.os.Vibrator;
import android.util.Log;
import com.google.android.googlelogin.GoogleLoginServiceConstants;
import com.google.devtools.simple.common.ComponentCategory;
import com.google.devtools.simple.common.PropertyCategory;
import com.google.devtools.simple.runtime.annotations.DesignerComponent;
import com.google.devtools.simple.runtime.annotations.DesignerProperty;
import com.google.devtools.simple.runtime.annotations.SimpleEvent;
import com.google.devtools.simple.runtime.annotations.SimpleFunction;
import com.google.devtools.simple.runtime.annotations.SimpleObject;
import com.google.devtools.simple.runtime.annotations.SimpleProperty;
import com.google.devtools.simple.runtime.annotations.UsesPermissions;
import com.google.devtools.simple.runtime.components.Component;
import com.google.devtools.simple.runtime.components.android.util.MediaUtil;
import com.google.devtools.simple.runtime.components.util.ErrorMessages;
import gnu.kawa.xml.ElementType;
import java.io.IOException;

@SimpleObject
@UsesPermissions(permissionNames = "android.permission.VIBRATE, android.permission.INTERNET")
@DesignerComponent(category = ComponentCategory.MEDIA, description = "<p>Multimedia component that plays audio or video and controls phone vibration.  The name of a multimedia field is specified in the <code>Source</code> property, which can be set in the Designer or in the Blocks Editor.  The length of time for a vibration is specified in the Blocks Editor in milliseconds (thousandths of a second).</p><p>For legal sound and video formats, see <a href=\"http://developer.android.com/guide/appendix/media-formats.html\" target=\"_blank\">Android Supported Media Formats</a>.</p><p>If you will only be playing sound files and vibrating, not using video, this component is best for long sound files, such as songs, while the <code>Sound</code> component is more efficient for short files, such as sound effects.</p>", iconName = "images/player.png", nonVisible = true, version = 3)
public final class Player extends AndroidNonvisibleComponent implements Component, OnDestroyListener, Deleteable {
    private MediaPlayer mp;
    private int playerState;
    private String sourcePath = ElementType.MATCH_ANY_LOCALNAME;
    private final Vibrator vibe = ((Vibrator) this.form.getSystemService("vibrator"));

    public Player(ComponentContainer container) {
        super(container.$form());
        this.form.registerForOnDestroy(this);
        this.form.setVolumeControlStream(3);
    }

    @SimpleProperty(category = PropertyCategory.BEHAVIOR)
    public String Source() {
        return this.sourcePath;
    }

    @SimpleProperty
    @DesignerProperty(defaultValue = ElementType.MATCH_ANY_LOCALNAME, editorType = DesignerProperty.PROPERTY_TYPE_ASSET)
    public void Source(String path) {
        if (path == null) {
            path = ElementType.MATCH_ANY_LOCALNAME;
        }
        this.sourcePath = path;
        if (this.playerState == 1 || this.playerState == 2) {
            this.mp.stop();
        }
        this.playerState = 0;
        if (this.mp != null) {
            this.mp.release();
            this.mp = null;
        }
        if (this.sourcePath.length() > 0) {
            Log.i("Player", "Source path is " + this.sourcePath);
            this.mp = new MediaPlayer();
            try {
                MediaUtil.loadMediaPlayer(this.mp, this.form, this.sourcePath);
                this.mp.setAudioStreamType(3);
                Log.i("Player", "Successfully loaded source path " + this.sourcePath);
                prepare();
            } catch (IOException e) {
                this.mp.release();
                this.mp = null;
                this.form.dispatchErrorOccurredEvent(this, "Source", ErrorMessages.ERROR_UNABLE_TO_LOAD_MEDIA, this.sourcePath);
            }
        }
    }

    @SimpleFunction
    public void Start() {
        Log.i("Player", "Calling Start -- State=" + this.playerState);
        if (this.playerState == 1 || this.playerState == 2) {
            this.mp.start();
            this.playerState = 2;
        }
    }

    @SimpleFunction
    public void Pause() {
        Log.i("Player", "Calling Pause -- State=" + this.playerState);
        if (this.playerState == 2) {
            this.mp.pause();
            this.playerState = 2;
        }
    }

    @SimpleFunction
    public void Stop() {
        Log.i("Player", "Calling Stop -- State=" + this.playerState);
        if (this.playerState == 1 || this.playerState == 2) {
            this.mp.stop();
            prepare();
        }
    }

    @SimpleFunction
    public void Vibrate(long milliseconds) {
        this.vibe.vibrate(milliseconds);
    }

    @SimpleEvent(description = "The PlayerError event is no longer used. Please use the Screen.ErrorOccurred event instead.", userVisible = GoogleLoginServiceConstants.PREFER_HOSTED)
    public void PlayerError(String message) {
    }

    private void prepare() {
        try {
            this.mp.prepare();
            this.playerState = 1;
            Log.i("Player", "Successfully prepared");
        } catch (IOException e) {
            this.mp.release();
            this.mp = null;
            this.playerState = 0;
            this.form.dispatchErrorOccurredEvent(this, "Source", ErrorMessages.ERROR_UNABLE_TO_PREPARE_MEDIA, this.sourcePath);
        }
    }

    public void onDestroy() {
        prepareToDie();
    }

    public void onDelete() {
        prepareToDie();
    }

    private void prepareToDie() {
        if (this.playerState == 1 || this.playerState == 2) {
            this.mp.stop();
        }
        this.playerState = 0;
        if (this.mp != null) {
            this.mp.release();
            this.mp = null;
        }
        this.vibe.cancel();
    }
}
