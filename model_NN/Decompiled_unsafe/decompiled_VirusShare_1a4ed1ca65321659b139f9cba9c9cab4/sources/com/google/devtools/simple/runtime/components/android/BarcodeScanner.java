package com.google.devtools.simple.runtime.components.android;

import android.content.Intent;
import android.util.Log;
import com.google.devtools.simple.common.ComponentCategory;
import com.google.devtools.simple.common.PropertyCategory;
import com.google.devtools.simple.runtime.annotations.DesignerComponent;
import com.google.devtools.simple.runtime.annotations.SimpleEvent;
import com.google.devtools.simple.runtime.annotations.SimpleFunction;
import com.google.devtools.simple.runtime.annotations.SimpleObject;
import com.google.devtools.simple.runtime.annotations.SimpleProperty;
import com.google.devtools.simple.runtime.components.Component;
import com.google.devtools.simple.runtime.events.EventDispatcher;
import gnu.kawa.xml.ElementType;

@SimpleObject
@DesignerComponent(category = ComponentCategory.MISC, description = "Component for using the Barcode Scanner to read a barcode", iconName = "images/barcodeScanner.png", nonVisible = true, version = 1)
public class BarcodeScanner extends AndroidNonvisibleComponent implements ActivityResultListener, Component {
    private static final String SCANNER_RESULT_NAME = "SCAN_RESULT";
    private static final String SCAN_INTENT = "com.google.zxing.client.android.SCAN";
    private final ComponentContainer container;
    private int requestCode;
    private String result = ElementType.MATCH_ANY_LOCALNAME;

    public BarcodeScanner(ComponentContainer container2) {
        super(container2.$form());
        this.container = container2;
    }

    @SimpleProperty(category = PropertyCategory.BEHAVIOR)
    public String Result() {
        return this.result;
    }

    @SimpleFunction
    public void DoScan() {
        Intent intent = new Intent(SCAN_INTENT);
        if (this.requestCode == 0) {
            this.requestCode = this.form.registerForActivityResult(this);
        }
        this.container.$context().startActivityForResult(intent, this.requestCode);
    }

    public void resultReturned(int requestCode2, int resultCode, Intent data) {
        Log.i("BarcodeScanner", "Returning result. Request code = " + requestCode2 + ", result code = " + resultCode);
        if (requestCode2 == this.requestCode && resultCode == -1) {
            if (data.hasExtra(SCANNER_RESULT_NAME)) {
                this.result = data.getStringExtra(SCANNER_RESULT_NAME);
            } else {
                this.result = ElementType.MATCH_ANY_LOCALNAME;
            }
            AfterScan(this.result);
        }
    }

    @SimpleEvent
    public void AfterScan(String result2) {
        EventDispatcher.dispatchEvent(this, "AfterScan", result2);
    }
}
