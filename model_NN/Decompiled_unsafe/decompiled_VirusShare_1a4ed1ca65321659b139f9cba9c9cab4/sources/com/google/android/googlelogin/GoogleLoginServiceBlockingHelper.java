package com.google.android.googlelogin;

import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import com.google.android.googleapps.GoogleLoginCredentialsResult;
import com.google.android.googleapps.IGoogleLoginService;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class GoogleLoginServiceBlockingHelper {
    private static final String TAG = "GoogleLoginServiceBlockingHelper";
    private final double mBackoffFactor = 2.0d;
    /* access modifiers changed from: private */
    public Condition mBindWaitCondition = this.mGoogleLoginServiceLock.newCondition();
    /* access modifiers changed from: private */
    public final Context mContext;
    private int mDelay = this.mMinDelaySecs;
    /* access modifiers changed from: private */
    public boolean mGlsVerified;
    /* access modifiers changed from: private */
    public volatile IGoogleLoginService mGoogleLoginService = null;
    /* access modifiers changed from: private */
    public Lock mGoogleLoginServiceLock = new ReentrantLock();
    private final int mMaxDelaySecs = 300;
    private final int mMinDelaySecs = 5;
    private ServiceConnection mServiceConnection;
    /* access modifiers changed from: private */
    public Thread mServiceThread = null;

    public class AuthenticationException extends Exception {
        public AuthenticationException() {
        }

        public AuthenticationException(String message) {
            super(message);
        }

        public AuthenticationException(String message, Throwable cause) {
            super(message, cause);
        }

        public AuthenticationException(Throwable cause) {
            super(cause);
        }
    }

    public static String getOneAuthToken(Context context, String username, String service) throws AuthenticationException, GoogleLoginServiceNotFoundException {
        GoogleLoginServiceBlockingHelper loginHelper = new GoogleLoginServiceBlockingHelper(context);
        try {
            return loginHelper.getAuthToken(username, service);
        } finally {
            loginHelper.close();
        }
    }

    public GoogleLoginServiceBlockingHelper(Context context) throws GoogleLoginServiceNotFoundException {
        this.mContext = context;
        if (!GoogleAppsVerifier.isServiceAvailable(context, GoogleLoginServiceConstants.FULLY_QUALIFIED_SERVICE_NAME)) {
            throw new GoogleLoginServiceNotFoundException(0);
        }
        this.mGoogleLoginServiceLock.lock();
        try {
            this.mServiceConnection = new ServiceConnection() {
                public void onServiceConnected(ComponentName className, IBinder service) {
                    try {
                        GoogleLoginServiceBlockingHelper.this.mGoogleLoginServiceLock.lock();
                        boolean unused = GoogleLoginServiceBlockingHelper.this.mGlsVerified = GoogleAppsVerifier.isGoogleAppsVerified(GoogleLoginServiceBlockingHelper.this.mContext);
                        Thread unused2 = GoogleLoginServiceBlockingHelper.this.mServiceThread = Thread.currentThread();
                        IGoogleLoginService unused3 = GoogleLoginServiceBlockingHelper.this.mGoogleLoginService = IGoogleLoginService.Stub.asInterface(service);
                        GoogleLoginServiceBlockingHelper.this.mBindWaitCondition.signalAll();
                    } finally {
                        GoogleLoginServiceBlockingHelper.this.mGoogleLoginServiceLock.unlock();
                    }
                }

                public void onServiceDisconnected(ComponentName className) {
                    GoogleLoginServiceBlockingHelper.this.mGoogleLoginServiceLock.lock();
                    IGoogleLoginService unused = GoogleLoginServiceBlockingHelper.this.mGoogleLoginService = null;
                    GoogleLoginServiceBlockingHelper.this.mGoogleLoginServiceLock.unlock();
                }
            };
            this.mContext.bindService(GoogleLoginServiceConstants.SERVICE_INTENT, this.mServiceConnection, 1);
        } finally {
            this.mGoogleLoginServiceLock.unlock();
        }
    }

    public void close() {
        this.mGoogleLoginServiceLock.lock();
        try {
            if (this.mServiceConnection != null) {
                this.mContext.unbindService(this.mServiceConnection);
                this.mServiceConnection = null;
                this.mGoogleLoginService = null;
            }
        } finally {
            this.mGoogleLoginServiceLock.unlock();
        }
    }

    private void delay() {
        try {
            Thread.sleep(((long) this.mDelay) * 1000);
        } catch (InterruptedException e) {
        }
        this.mDelay = (int) (((double) this.mDelay) * this.mBackoffFactor);
        if (this.mDelay > this.mMaxDelaySecs) {
            this.mDelay = this.mMaxDelaySecs;
        }
    }

    private void resetDelay() {
        this.mDelay = this.mMinDelaySecs;
    }

    public IGoogleLoginService getLoginService() throws GoogleLoginServiceNotFoundException {
        try {
            this.mGoogleLoginServiceLock.lock();
            if (this.mServiceThread == null || Thread.currentThread() != this.mServiceThread) {
                while (this.mGoogleLoginService == null) {
                    try {
                        this.mBindWaitCondition.await();
                    } catch (InterruptedException e) {
                    }
                }
                checkGoogleLoginServiceVerificationLocked();
                return this.mGoogleLoginService;
            }
            throw new IllegalStateException("calling GoogleLoginServiceBlockingHelper methods from your main thread can lead to deadlock");
        } finally {
            this.mGoogleLoginServiceLock.unlock();
        }
    }

    private void checkGoogleLoginServiceVerificationLocked() throws GoogleLoginServiceNotFoundException {
        if (this.mGoogleLoginService != null && !this.mGlsVerified) {
            throw new GoogleLoginServiceNotFoundException(1);
        }
    }

    public static void invalidateAuthToken(Context context, String authToken) throws GoogleLoginServiceNotFoundException {
        GoogleLoginServiceBlockingHelper h = new GoogleLoginServiceBlockingHelper(context);
        try {
            h.invalidateAuthToken(authToken);
        } finally {
            h.close();
        }
    }

    public void invalidateAuthToken(String authToken) throws GoogleLoginServiceNotFoundException {
        resetDelay();
        while (true) {
            try {
                getLoginService().invalidateAuthToken(authToken);
                break;
            } catch (RemoteException e) {
                delay();
            }
        }
    }

    public static String[] getAccounts(Context context) throws GoogleLoginServiceNotFoundException {
        GoogleLoginServiceBlockingHelper h = new GoogleLoginServiceBlockingHelper(context);
        try {
            return h.getAccounts();
        } finally {
            h.close();
        }
    }

    public String[] getAccounts() throws GoogleLoginServiceNotFoundException {
        resetDelay();
        while (true) {
            try {
                break;
            } catch (RemoteException e) {
                delay();
            }
        }
        return getLoginService().getAccounts();
    }

    public static String getAccount(Context context, boolean requiresGoogle) throws GoogleLoginServiceNotFoundException {
        GoogleLoginServiceBlockingHelper h = new GoogleLoginServiceBlockingHelper(context);
        try {
            return h.getAccount(requiresGoogle);
        } finally {
            h.close();
        }
    }

    public String getAccount(boolean requiresGoogle) throws GoogleLoginServiceNotFoundException {
        resetDelay();
        while (true) {
            try {
                break;
            } catch (RemoteException e) {
                delay();
            }
        }
        return getLoginService().getAccount(requiresGoogle);
    }

    public static String getAuthToken(Context context, String username, String service) throws AuthenticationException, GoogleLoginServiceNotFoundException {
        GoogleLoginServiceBlockingHelper h = new GoogleLoginServiceBlockingHelper(context);
        try {
            return h.getAuthToken(username, service);
        } finally {
            h.close();
        }
    }

    public String getAuthToken(String username, String service) throws AuthenticationException, GoogleLoginServiceNotFoundException {
        resetDelay();
        GoogleLoginCredentialsResult result = getCredentials(username, service, true);
        if (result.getCredentialsString() != null) {
            return result.getCredentialsString();
        }
        throw new AuthenticationException("unable to find auth token for account");
    }

    public GoogleLoginCredentialsResult getCredentials(String username, String service, boolean notifyAuthFailure) throws GoogleLoginServiceNotFoundException {
        resetDelay();
        while (true) {
            try {
                break;
            } catch (RemoteException e) {
                delay();
            }
        }
        return getLoginService().blockingGetCredentials(username, service, notifyAuthFailure);
    }

    public String peekCredentials(String username, String service) throws GoogleLoginServiceNotFoundException {
        resetDelay();
        while (true) {
            try {
                break;
            } catch (RemoteException e) {
                delay();
            }
        }
        return getLoginService().peekCredentials(username, service);
    }

    public static long getAndroidId(Context context) throws GoogleLoginServiceNotFoundException {
        GoogleLoginServiceBlockingHelper h = new GoogleLoginServiceBlockingHelper(context);
        try {
            return h.getAndroidId();
        } finally {
            h.close();
        }
    }

    public long getAndroidId() throws GoogleLoginServiceNotFoundException {
        resetDelay();
        while (true) {
            try {
                break;
            } catch (RemoteException e) {
                delay();
            }
        }
        return getLoginService().getAndroidId();
    }
}
