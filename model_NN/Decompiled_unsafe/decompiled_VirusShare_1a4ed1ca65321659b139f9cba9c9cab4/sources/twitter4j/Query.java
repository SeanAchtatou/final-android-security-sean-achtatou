package twitter4j;

import java.util.ArrayList;
import java.util.List;
import twitter4j.http.PostParameter;

public class Query {
    public static final String KILOMETERS = "km";
    public static final String MILES = "mi";
    private String geocode = null;
    private String lang = null;
    private int page = -1;
    private String query = null;
    private int rpp = -1;
    private long sinceId = -1;

    public Query() {
    }

    public Query(String query2) {
        this.query = query2;
    }

    public String getQuery() {
        return this.query;
    }

    public void setQuery(String query2) {
        this.query = query2;
    }

    public String getLang() {
        return this.lang;
    }

    public void setLang(String lang2) {
        this.lang = lang2;
    }

    public int getRpp() {
        return this.rpp;
    }

    public void setRpp(int rpp2) {
        this.rpp = rpp2;
    }

    public int getPage() {
        return this.page;
    }

    public void setPage(int page2) {
        this.page = page2;
    }

    public long getSinceId() {
        return this.sinceId;
    }

    public void setSinceId(long sinceId2) {
        this.sinceId = sinceId2;
    }

    public String getGeocode() {
        return this.geocode;
    }

    public void setGeoCode(double latitude, double longtitude, double radius, String unit) {
        this.geocode = new StringBuffer().append(latitude).append(",").append(longtitude).append(",").append(radius).append(unit).toString();
    }

    public PostParameter[] asPostParameters() {
        ArrayList<PostParameter> params = new ArrayList<>();
        appendParameter("q", this.query, params);
        appendParameter("lang", this.lang, params);
        appendParameter("rpp", (long) this.rpp, params);
        appendParameter("page", (long) this.page, params);
        appendParameter("since_id", this.sinceId, params);
        appendParameter("geocode", this.geocode, params);
        return (PostParameter[]) params.toArray(new PostParameter[params.size()]);
    }

    private void appendParameter(String name, String value, List<PostParameter> params) {
        if (value != null) {
            params.add(new PostParameter(name, value));
        }
    }

    private void appendParameter(String name, long value, List<PostParameter> params) {
        if (0 <= value) {
            params.add(new PostParameter(name, String.valueOf(value)));
        }
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Query query1 = (Query) o;
        if (this.page != query1.page) {
            return false;
        }
        if (this.rpp != query1.rpp) {
            return false;
        }
        if (this.sinceId != query1.sinceId) {
            return false;
        }
        if (this.geocode == null ? query1.geocode != null : !this.geocode.equals(query1.geocode)) {
            return false;
        }
        if (this.lang == null ? query1.lang != null : !this.lang.equals(query1.lang)) {
            return false;
        }
        if (this.query != null) {
            if (this.query.equals(query1.query)) {
                return true;
            }
        } else if (query1.query == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int result;
        int i;
        int i2 = 0;
        if (this.query != null) {
            result = this.query.hashCode();
        } else {
            result = 0;
        }
        int i3 = result * 31;
        if (this.lang != null) {
            i = this.lang.hashCode();
        } else {
            i = 0;
        }
        int i4 = (((((((i3 + i) * 31) + this.rpp) * 31) + this.page) * 31) + ((int) (this.sinceId ^ (this.sinceId >>> 32)))) * 31;
        if (this.geocode != null) {
            i2 = this.geocode.hashCode();
        }
        return i4 + i2;
    }

    public String toString() {
        return new StringBuffer().append("Query{query='").append(this.query).append('\'').append(", lang='").append(this.lang).append('\'').append(", rpp=").append(this.rpp).append(", page=").append(this.page).append(", sinceId=").append(this.sinceId).append(", geocode='").append(this.geocode).append('\'').append('}').toString();
    }
}
