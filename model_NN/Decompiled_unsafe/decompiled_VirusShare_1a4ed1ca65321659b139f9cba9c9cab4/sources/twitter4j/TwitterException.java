package twitter4j;

public class TwitterException extends Exception {
    private static final long serialVersionUID = -2623309261327598087L;
    private int statusCode = -1;

    public TwitterException(String msg) {
        super(msg);
    }

    public TwitterException(Exception cause) {
        super(cause);
    }

    public TwitterException(String msg, int statusCode2) {
        super(msg);
        this.statusCode = statusCode2;
    }

    public TwitterException(String msg, Exception cause) {
        super(msg, cause);
    }

    public TwitterException(String msg, Exception cause, int statusCode2) {
        super(msg, cause);
        this.statusCode = statusCode2;
    }

    public int getStatusCode() {
        return this.statusCode;
    }
}
