package twitter4j;

import com.google.devtools.simple.runtime.components.android.LocationSensor;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import twitter4j.http.PostParameter;

public class TwitterStream extends TwitterSupport {
    private static final boolean DEBUG = Configuration.getDebug();
    private StreamHandlingThread handler = null;
    private int retryPerMinutes = 1;
    private StatusListener statusListener;

    static int access$000(TwitterStream x0) {
        return x0.retryPerMinutes;
    }

    static void access$100(TwitterStream x0, String x1, String x2) {
        x0.log(x1, x2);
    }

    static StatusListener access$200(TwitterStream x0) {
        return x0.statusListener;
    }

    static void access$300(TwitterStream x0, String x1) {
        x0.log(x1);
    }

    public void forceUsePost(boolean x0) {
        super.forceUsePost(x0);
    }

    public String getClientURL() {
        return super.getClientURL();
    }

    public String getClientVersion() {
        return super.getClientVersion();
    }

    public String getPassword() {
        return super.getPassword();
    }

    public String getSource() {
        return super.getSource();
    }

    public String getUserAgent() {
        return super.getUserAgent();
    }

    public String getUserId() {
        return super.getUserId();
    }

    public boolean isUsePostForced() {
        return super.isUsePostForced();
    }

    public void setClientURL(String x0) {
        super.setClientURL(x0);
    }

    public void setClientVersion(String x0) {
        super.setClientVersion(x0);
    }

    public void setHttpConnectionTimeout(int x0) {
        super.setHttpConnectionTimeout(x0);
    }

    public void setHttpProxy(String x0, int x1) {
        super.setHttpProxy(x0, x1);
    }

    public void setHttpProxyAuth(String x0, String x1) {
        super.setHttpProxyAuth(x0, x1);
    }

    public void setHttpReadTimeout(int x0) {
        super.setHttpReadTimeout(x0);
    }

    public void setPassword(String x0) {
        super.setPassword(x0);
    }

    public void setRequestHeader(String x0, String x1) {
        super.setRequestHeader(x0, x1);
    }

    public void setRetryCount(int x0) {
        super.setRetryCount(x0);
    }

    public void setRetryIntervalSecs(int x0) {
        super.setRetryIntervalSecs(x0);
    }

    public void setSource(String x0) {
        super.setSource(x0);
    }

    public void setUserAgent(String x0) {
        super.setUserAgent(x0);
    }

    public void setUserId(String x0) {
        super.setUserId(x0);
    }

    public TwitterStream() {
    }

    public TwitterStream(String userId, String password) {
        super(userId, password);
    }

    public TwitterStream(String userId, String password, StatusListener listener) {
        super(userId, password);
        this.statusListener = listener;
    }

    public void firehose(int count) throws TwitterException {
        startHandler(new StreamHandlingThread(this, new Object[]{new Integer(count)}) {
            private final TwitterStream this$0;

            {
                this.this$0 = r1;
            }

            public StatusStream getStream() throws TwitterException {
                return this.this$0.getFirehoseStream(((Integer) this.args[0]).intValue());
            }
        });
    }

    public StatusStream getFirehoseStream(int count) throws TwitterException {
        try {
            return new StatusStream(this.http.post(new StringBuffer().append(getStreamBaseURL()).append("1/statuses/firehose.json").toString(), new PostParameter[]{new PostParameter("count", String.valueOf(count))}, true));
        } catch (IOException e) {
            throw new TwitterException(e);
        }
    }

    public void retweet() throws TwitterException {
        startHandler(new StreamHandlingThread(this, new Object[0]) {
            private final TwitterStream this$0;

            {
                this.this$0 = r1;
            }

            public StatusStream getStream() throws TwitterException {
                return this.this$0.getRetweetStream();
            }
        });
    }

    public StatusStream getRetweetStream() throws TwitterException {
        try {
            return new StatusStream(this.http.post(new StringBuffer().append(getStreamBaseURL()).append("1/statuses/retweet.json").toString(), new PostParameter[0], true));
        } catch (IOException e) {
            throw new TwitterException(e);
        }
    }

    public void sample() throws TwitterException {
        startHandler(new StreamHandlingThread(this, null) {
            private final TwitterStream this$0;

            {
                this.this$0 = r1;
            }

            public StatusStream getStream() throws TwitterException {
                return this.this$0.getSampleStream();
            }
        });
    }

    public StatusStream getSampleStream() throws TwitterException {
        try {
            return new StatusStream(this.http.get(new StringBuffer().append(getStreamBaseURL()).append("1/statuses/sample.json").toString(), true));
        } catch (IOException e) {
            throw new TwitterException(e);
        }
    }

    public void filter(int count, int[] follow, String[] track) throws TwitterException {
        startHandler(new StreamHandlingThread(this, new Object[]{new Integer(count), follow, track}) {
            private final TwitterStream this$0;

            {
                this.this$0 = r1;
            }

            public StatusStream getStream() throws TwitterException {
                return this.this$0.getFilterStream(((Integer) this.args[0]).intValue(), (int[]) this.args[1], (String[]) this.args[2]);
            }
        });
    }

    public StatusStream getFilterStream(int count, int[] follow, String[] track) throws TwitterException {
        List<PostParameter> postparams = new ArrayList<>();
        postparams.add(new PostParameter("count", count));
        if (follow != null && follow.length > 0) {
            postparams.add(new PostParameter("follow", toFollowString(follow)));
        }
        if (track != null && track.length > 0) {
            postparams.add(new PostParameter("track", toTrackString(track)));
        }
        try {
            return new StatusStream(this.http.post(new StringBuffer().append(getStreamBaseURL()).append("1/statuses/filter.json").toString(), (PostParameter[]) postparams.toArray(new PostParameter[0]), true));
        } catch (IOException e) {
            throw new TwitterException(e);
        }
    }

    public void gardenhose() throws TwitterException {
        startHandler(new StreamHandlingThread(this, null) {
            private final TwitterStream this$0;

            {
                this.this$0 = r1;
            }

            public StatusStream getStream() throws TwitterException {
                return this.this$0.getGardenhoseStream();
            }
        });
    }

    public StatusStream getGardenhoseStream() throws TwitterException {
        return getSampleStream();
    }

    public void spritzer() throws TwitterException {
        startHandler(new StreamHandlingThread(this, null) {
            private final TwitterStream this$0;

            {
                this.this$0 = r1;
            }

            public StatusStream getStream() throws TwitterException {
                return this.this$0.getSpritzerStream();
            }
        });
    }

    public StatusStream getSpritzerStream() throws TwitterException {
        return getSampleStream();
    }

    public void birddog(int count, int[] follow) throws TwitterException {
        startHandler(new StreamHandlingThread(this, new Object[]{new Integer(count), follow}) {
            private final TwitterStream this$0;

            {
                this.this$0 = r1;
            }

            public StatusStream getStream() throws TwitterException {
                return this.this$0.getBirddogStream(((Integer) this.args[0]).intValue(), (int[]) this.args[1]);
            }
        });
    }

    public StatusStream getBirddogStream(int count, int[] follow) throws TwitterException {
        return getFilterStream(count, follow, null);
    }

    public void shadow(int count, int[] follow) throws TwitterException {
        startHandler(new StreamHandlingThread(this, new Object[]{new Integer(count), follow}) {
            private final TwitterStream this$0;

            {
                this.this$0 = r1;
            }

            public StatusStream getStream() throws TwitterException {
                return this.this$0.getShadowStream(((Integer) this.args[0]).intValue(), (int[]) this.args[1]);
            }
        });
    }

    public StatusStream getShadowStream(int count, int[] follow) throws TwitterException {
        return getFilterStream(count, follow, null);
    }

    public void follow(int[] follow) throws TwitterException {
        startHandler(new StreamHandlingThread(this, new Object[]{follow}) {
            private final TwitterStream this$0;

            {
                this.this$0 = r1;
            }

            public StatusStream getStream() throws TwitterException {
                return this.this$0.getFollowStream((int[]) this.args[0]);
            }
        });
    }

    public StatusStream getFollowStream(int[] follow) throws TwitterException {
        return getFilterStream(0, follow, null);
    }

    private String toFollowString(int[] follows) {
        StringBuffer buf = new StringBuffer(follows.length * 11);
        for (int follow : follows) {
            if (buf.length() != 0) {
                buf.append(",");
            }
            buf.append(follow);
        }
        return buf.toString();
    }

    public void track(String[] keywords) throws TwitterException {
        startHandler(new StreamHandlingThread(this, null, keywords) {
            private final TwitterStream this$0;
            private final String[] val$keywords;

            {
                this.this$0 = r1;
                this.val$keywords = r3;
            }

            public StatusStream getStream() throws TwitterException {
                return this.this$0.getTrackStream(this.val$keywords);
            }
        });
    }

    public StatusStream getTrackStream(String[] keywords) throws TwitterException {
        return getFilterStream(0, null, keywords);
    }

    private String toTrackString(String[] keywords) {
        StringBuffer buf = new StringBuffer(keywords.length * 20 * 4);
        for (String keyword : keywords) {
            if (buf.length() != 0) {
                buf.append(",");
            }
            buf.append(keyword);
        }
        return buf.toString();
    }

    private synchronized void startHandler(StreamHandlingThread handler2) throws TwitterException {
        cleanup();
        if (this.statusListener == null) {
            throw new IllegalStateException("StatusListener is not set.");
        }
        this.handler = handler2;
        this.handler.start();
    }

    public synchronized void cleanup() {
        if (this.handler != null) {
            try {
                this.handler.close();
            } catch (IOException e) {
            }
        }
    }

    public StatusListener getStatusListener() {
        return this.statusListener;
    }

    public void setStatusListener(StatusListener statusListener2) {
        this.statusListener = statusListener2;
    }

    abstract class StreamHandlingThread extends Thread {
        private static final String NAME = "Twitter Stream Handling Thread";
        Object[] args;
        private boolean closed = false;
        private List<Long> retryHistory;
        StatusStream stream = null;
        private final TwitterStream this$0;

        /* access modifiers changed from: package-private */
        public abstract StatusStream getStream() throws TwitterException;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        StreamHandlingThread(TwitterStream twitterStream, Object[] args2) {
            super("Twitter Stream Handling Thread[initializing]");
            this.this$0 = twitterStream;
            this.args = args2;
            this.retryHistory = new ArrayList(TwitterStream.access$000(twitterStream));
        }

        public void run() {
            Status status;
            while (!this.closed) {
                try {
                    if (this.retryHistory.size() > 0 && System.currentTimeMillis() - this.retryHistory.get(0).longValue() > LocationSensor.MIN_TIME_INTERVAL) {
                        this.retryHistory.remove(0);
                    }
                    if (this.retryHistory.size() < TwitterStream.access$000(this.this$0)) {
                        setStatus("[establishing connection]");
                        while (!this.closed && this.stream == null) {
                            if (this.retryHistory.size() < TwitterStream.access$000(this.this$0)) {
                                this.retryHistory.add(new Long(System.currentTimeMillis()));
                                this.stream = getStream();
                            }
                        }
                    } else {
                        long timeToSleep = LocationSensor.MIN_TIME_INTERVAL - (System.currentTimeMillis() - this.retryHistory.get(this.retryHistory.size() - 1).longValue());
                        setStatus(new StringBuffer().append("[retry limit reached. sleeping for ").append(timeToSleep / 1000).append(" secs]").toString());
                        try {
                            Thread.sleep(timeToSleep);
                        } catch (InterruptedException e) {
                        }
                    }
                    if (this.stream != null) {
                        setStatus("[receiving stream]");
                        while (!this.closed && (status = this.stream.next()) != null) {
                            TwitterStream.access$100(this.this$0, "received:", status.toString());
                            if (TwitterStream.access$200(this.this$0) != null) {
                                TwitterStream.access$200(this.this$0).onStatus(status);
                            }
                        }
                    }
                } catch (TwitterException te) {
                    this.stream = null;
                    te.printStackTrace();
                    TwitterStream.access$300(this.this$0, te.getMessage());
                    TwitterStream.access$200(this.this$0).onException(te);
                }
            }
        }

        public synchronized void close() throws IOException {
            setStatus("[disposing thread]");
            if (this.stream != null) {
                this.stream.close();
                this.closed = true;
            }
        }

        private void setStatus(String message) {
            String actualMessage = new StringBuffer().append(NAME).append(message).toString();
            setName(actualMessage);
            TwitterStream.access$300(this.this$0, actualMessage);
        }
    }

    private String getStreamBaseURL() {
        return this.USE_SSL ? "https://stream.twitter.com/" : "http://stream.twitter.com/";
    }

    private void log(String message) {
        if (DEBUG) {
            System.out.println(new StringBuffer().append("[").append(new Date()).append("]").append(message).toString());
        }
    }

    private void log(String message, String message2) {
        if (DEBUG) {
            log(new StringBuffer().append(message).append(message2).toString());
        }
    }
}
