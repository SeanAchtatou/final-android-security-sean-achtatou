package twitter4j;

import com.google.devtools.simple.runtime.annotations.DesignerProperty;
import gnu.kawa.xml.ElementType;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import twitter4j.http.AccessToken;
import twitter4j.http.HttpClient;
import twitter4j.http.PostParameter;
import twitter4j.http.RequestToken;
import twitter4j.http.Response;

public class Twitter extends TwitterSupport implements Serializable {
    public static final Device IM = new Device("im");
    public static final Device NONE = new Device("none");
    public static final Device SMS = new Device("sms");
    private static final long serialVersionUID = -1486360080128882436L;
    private String baseURL;
    private SimpleDateFormat format;
    private String searchBaseURL;

    public void forceUsePost(boolean x0) {
        super.forceUsePost(x0);
    }

    public String getClientURL() {
        return super.getClientURL();
    }

    public String getClientVersion() {
        return super.getClientVersion();
    }

    public String getPassword() {
        return super.getPassword();
    }

    public String getSource() {
        return super.getSource();
    }

    public String getUserAgent() {
        return super.getUserAgent();
    }

    public String getUserId() {
        return super.getUserId();
    }

    public boolean isUsePostForced() {
        return super.isUsePostForced();
    }

    public void setClientURL(String x0) {
        super.setClientURL(x0);
    }

    public void setClientVersion(String x0) {
        super.setClientVersion(x0);
    }

    public void setHttpConnectionTimeout(int x0) {
        super.setHttpConnectionTimeout(x0);
    }

    public void setHttpProxy(String x0, int x1) {
        super.setHttpProxy(x0, x1);
    }

    public void setHttpProxyAuth(String x0, String x1) {
        super.setHttpProxyAuth(x0, x1);
    }

    public void setHttpReadTimeout(int x0) {
        super.setHttpReadTimeout(x0);
    }

    public void setPassword(String x0) {
        super.setPassword(x0);
    }

    public void setRequestHeader(String x0, String x1) {
        super.setRequestHeader(x0, x1);
    }

    public void setRetryCount(int x0) {
        super.setRetryCount(x0);
    }

    public void setRetryIntervalSecs(int x0) {
        super.setRetryIntervalSecs(x0);
    }

    public void setSource(String x0) {
        super.setSource(x0);
    }

    public void setUserAgent(String x0) {
        super.setUserAgent(x0);
    }

    public void setUserId(String x0) {
        super.setUserId(x0);
    }

    public Twitter() {
        this.baseURL = new StringBuffer().append(Configuration.getScheme()).append("twitter.com/").toString();
        this.searchBaseURL = new StringBuffer().append(Configuration.getScheme()).append("search.twitter.com/").toString();
        this.format = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss z", Locale.ENGLISH);
        this.format.setTimeZone(TimeZone.getTimeZone("GMT"));
        this.http.setRequestTokenURL(new StringBuffer().append(Configuration.getScheme()).append("twitter.com/oauth/request_token").toString());
        this.http.setAuthorizationURL(new StringBuffer().append(Configuration.getScheme()).append("twitter.com/oauth/authorize").toString());
        this.http.setAccessTokenURL(new StringBuffer().append(Configuration.getScheme()).append("twitter.com/oauth/access_token").toString());
    }

    public Twitter(String baseURL2) {
        this();
        this.baseURL = baseURL2;
    }

    public Twitter(String id, String password) {
        this();
        setUserId(id);
        setPassword(password);
    }

    public Twitter(String id, String password, String baseURL2) {
        this();
        setUserId(id);
        setPassword(password);
        this.baseURL = baseURL2;
    }

    public void setBaseURL(String baseURL2) {
        this.baseURL = baseURL2;
    }

    public String getBaseURL() {
        return this.baseURL;
    }

    public void setSearchBaseURL(String searchBaseURL2) {
        this.searchBaseURL = searchBaseURL2;
    }

    public String getSearchBaseURL() {
        return this.searchBaseURL;
    }

    public synchronized void setOAuthConsumer(String consumerKey, String consumerSecret) {
        this.http.setOAuthConsumer(consumerKey, consumerSecret);
    }

    public RequestToken getOAuthRequestToken() throws TwitterException {
        return this.http.getOAuthRequestToken();
    }

    public RequestToken getOAuthRequestToken(String callback_url) throws TwitterException {
        return this.http.getOauthRequestToken(callback_url);
    }

    public synchronized AccessToken getOAuthAccessToken(RequestToken requestToken) throws TwitterException {
        return this.http.getOAuthAccessToken(requestToken);
    }

    public synchronized AccessToken getOAuthAccessToken(RequestToken requestToken, String pin) throws TwitterException {
        AccessToken accessToken;
        accessToken = this.http.getOAuthAccessToken(requestToken, pin);
        setUserId(accessToken.getScreenName());
        return accessToken;
    }

    public synchronized AccessToken getOAuthAccessToken(String token, String tokenSecret) throws TwitterException {
        AccessToken accessToken;
        accessToken = this.http.getOAuthAccessToken(token, tokenSecret);
        setUserId(accessToken.getScreenName());
        return accessToken;
    }

    public synchronized AccessToken getOAuthAccessToken(String token, String tokenSecret, String oauth_verifier) throws TwitterException {
        return this.http.getOAuthAccessToken(token, tokenSecret, oauth_verifier);
    }

    public void setOAuthAccessToken(AccessToken accessToken) {
        this.http.setOAuthAccessToken(accessToken);
    }

    public void setOAuthAccessToken(String token, String tokenSecret) {
        setOAuthAccessToken(new AccessToken(token, tokenSecret));
    }

    private Response get(String url, boolean authenticate) throws TwitterException {
        return get(url, null, authenticate);
    }

    /* access modifiers changed from: protected */
    public Response get(String url, String name1, String value1, boolean authenticate) throws TwitterException {
        return get(url, new PostParameter[]{new PostParameter(name1, value1)}, authenticate);
    }

    /* access modifiers changed from: protected */
    public Response get(String url, String name1, String value1, String name2, String value2, boolean authenticate) throws TwitterException {
        return get(url, new PostParameter[]{new PostParameter(name1, value1), new PostParameter(name2, value2)}, authenticate);
    }

    /* access modifiers changed from: protected */
    public Response get(String url, PostParameter[] params, boolean authenticate) throws TwitterException {
        if (params != null && params.length > 0) {
            url = new StringBuffer().append(url).append("?").append(HttpClient.encodeParameters(params)).toString();
        }
        return this.http.get(url, authenticate);
    }

    /* access modifiers changed from: protected */
    public Response get(String url, PostParameter[] params, Paging paging, boolean authenticate) throws TwitterException {
        if (paging == null) {
            return get(url, params, authenticate);
        }
        List<PostParameter> pagingParams = new ArrayList<>(4);
        if (-1 != paging.getMaxId()) {
            pagingParams.add(new PostParameter("max_id", String.valueOf(paging.getMaxId())));
        }
        if (-1 != paging.getSinceId()) {
            pagingParams.add(new PostParameter("since_id", String.valueOf(paging.getSinceId())));
        }
        if (-1 != paging.getPage()) {
            pagingParams.add(new PostParameter("page", String.valueOf(paging.getPage())));
        }
        if (-1 != paging.getCount()) {
            if (-1 != url.indexOf("search")) {
                pagingParams.add(new PostParameter("rpp", String.valueOf(paging.getCount())));
            } else {
                pagingParams.add(new PostParameter("count", String.valueOf(paging.getCount())));
            }
        }
        PostParameter[] newparams = null;
        PostParameter[] arrayPagingParams = (PostParameter[]) pagingParams.toArray(new PostParameter[pagingParams.size()]);
        if (params != null) {
            newparams = new PostParameter[(params.length + pagingParams.size())];
            System.arraycopy(params, 0, newparams, 0, params.length);
            System.arraycopy(arrayPagingParams, 0, newparams, params.length, pagingParams.size());
        } else if (arrayPagingParams.length != 0) {
            String encodedParams = HttpClient.encodeParameters(arrayPagingParams);
            if (-1 != url.indexOf("?")) {
                url = new StringBuffer().append(url).append("&").append(encodedParams).toString();
            } else {
                url = new StringBuffer().append(url).append("?").append(encodedParams).toString();
            }
        }
        return get(url, newparams, authenticate);
    }

    public QueryResult search(Query query) throws TwitterException {
        try {
            return new QueryResult(get(new StringBuffer().append(this.searchBaseURL).append("search.json").toString(), query.asPostParameters(), false), this);
        } catch (TwitterException te) {
            if (404 == te.getStatusCode()) {
                return new QueryResult(query);
            }
            throw te;
        }
    }

    public Trends getTrends() throws TwitterException {
        return Trends.constructTrends(get(new StringBuffer().append(this.searchBaseURL).append("trends.json").toString(), false));
    }

    public Trends getCurrentTrends() throws TwitterException {
        return Trends.constructTrendsList(get(new StringBuffer().append(this.searchBaseURL).append("trends/current.json").toString(), false)).get(0);
    }

    public Trends getCurrentTrends(boolean excludeHashTags) throws TwitterException {
        return Trends.constructTrendsList(get(new StringBuffer().append(this.searchBaseURL).append("trends/current.json").append(excludeHashTags ? "?exclude=hashtags" : ElementType.MATCH_ANY_LOCALNAME).toString(), false)).get(0);
    }

    public List<Trends> getDailyTrends() throws TwitterException {
        return Trends.constructTrendsList(get(new StringBuffer().append(this.searchBaseURL).append("trends/daily.json").toString(), false));
    }

    public List<Trends> getDailyTrends(Date date, boolean excludeHashTags) throws TwitterException {
        return Trends.constructTrendsList(get(new StringBuffer().append(this.searchBaseURL).append("trends/daily.json?date=").append(toDateStr(date)).append(excludeHashTags ? "&exclude=hashtags" : ElementType.MATCH_ANY_LOCALNAME).toString(), false));
    }

    private String toDateStr(Date date) {
        if (date == null) {
            date = new Date();
        }
        return new SimpleDateFormat("yyyy-MM-dd").format(date);
    }

    public List<Trends> getWeeklyTrends() throws TwitterException {
        return Trends.constructTrendsList(get(new StringBuffer().append(this.searchBaseURL).append("trends/weekly.json").toString(), false));
    }

    public List<Trends> getWeeklyTrends(Date date, boolean excludeHashTags) throws TwitterException {
        return Trends.constructTrendsList(get(new StringBuffer().append(this.searchBaseURL).append("trends/weekly.json?date=").append(toDateStr(date)).append(excludeHashTags ? "&exclude=hashtags" : ElementType.MATCH_ANY_LOCALNAME).toString(), false));
    }

    public List<Status> getPublicTimeline() throws TwitterException {
        return Status.constructStatuses(get(new StringBuffer().append(getBaseURL()).append("statuses/public_timeline.xml").toString(), false), this);
    }

    public List<Status> getPublicTimeline(int sinceID) throws TwitterException {
        return getPublicTimeline((long) sinceID);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: twitter4j.Twitter.get(java.lang.String, twitter4j.http.PostParameter[], twitter4j.Paging, boolean):twitter4j.http.Response
     arg types: [java.lang.String, ?[OBJECT, ARRAY], twitter4j.Paging, int]
     candidates:
      twitter4j.Twitter.get(java.lang.String, java.lang.String, java.lang.String, boolean):twitter4j.http.Response
      twitter4j.Twitter.get(java.lang.String, twitter4j.http.PostParameter[], twitter4j.Paging, boolean):twitter4j.http.Response */
    public List<Status> getPublicTimeline(long sinceID) throws TwitterException {
        return Status.constructStatuses(get(new StringBuffer().append(getBaseURL()).append("statuses/public_timeline.xml").toString(), (PostParameter[]) null, new Paging(sinceID), false), this);
    }

    public List<Status> getHomeTimeline() throws TwitterException {
        return Status.constructStatuses(get(new StringBuffer().append(getBaseURL()).append("statuses/home_timeline.xml").toString(), true), this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: twitter4j.Twitter.get(java.lang.String, twitter4j.http.PostParameter[], twitter4j.Paging, boolean):twitter4j.http.Response
     arg types: [java.lang.String, ?[OBJECT, ARRAY], twitter4j.Paging, int]
     candidates:
      twitter4j.Twitter.get(java.lang.String, java.lang.String, java.lang.String, boolean):twitter4j.http.Response
      twitter4j.Twitter.get(java.lang.String, twitter4j.http.PostParameter[], twitter4j.Paging, boolean):twitter4j.http.Response */
    public List<Status> getHomeTimeline(Paging paging) throws TwitterException {
        return Status.constructStatuses(get(new StringBuffer().append(getBaseURL()).append("statuses/home_timeline.xml").toString(), (PostParameter[]) null, paging, true), this);
    }

    public List<Status> getFriendsTimeline() throws TwitterException {
        return Status.constructStatuses(get(new StringBuffer().append(getBaseURL()).append("statuses/friends_timeline.xml").toString(), true), this);
    }

    public List<Status> getFriendsTimelineByPage(int page) throws TwitterException {
        return getFriendsTimeline(new Paging(page));
    }

    public List<Status> getFriendsTimeline(int page) throws TwitterException {
        return getFriendsTimeline(new Paging(page));
    }

    public List<Status> getFriendsTimeline(long sinceId, int page) throws TwitterException {
        return getFriendsTimeline(new Paging(page).sinceId(sinceId));
    }

    public List<Status> getFriendsTimeline(String id) throws TwitterException {
        throw new IllegalStateException("The Twitter API is not supporting this method anymore");
    }

    public List<Status> getFriendsTimelineByPage(String id, int page) throws TwitterException {
        throw new IllegalStateException("The Twitter API is not supporting this method anymore");
    }

    public List<Status> getFriendsTimeline(String id, int page) throws TwitterException {
        throw new IllegalStateException("The Twitter API is not supporting this method anymore");
    }

    public List<Status> getFriendsTimeline(long sinceId, String id, int page) throws TwitterException {
        throw new IllegalStateException("The Twitter API is not supporting this method anymore");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: twitter4j.Twitter.get(java.lang.String, twitter4j.http.PostParameter[], twitter4j.Paging, boolean):twitter4j.http.Response
     arg types: [java.lang.String, ?[OBJECT, ARRAY], twitter4j.Paging, int]
     candidates:
      twitter4j.Twitter.get(java.lang.String, java.lang.String, java.lang.String, boolean):twitter4j.http.Response
      twitter4j.Twitter.get(java.lang.String, twitter4j.http.PostParameter[], twitter4j.Paging, boolean):twitter4j.http.Response */
    public List<Status> getFriendsTimeline(Paging paging) throws TwitterException {
        return Status.constructStatuses(get(new StringBuffer().append(getBaseURL()).append("statuses/friends_timeline.xml").toString(), (PostParameter[]) null, paging, true), this);
    }

    public List<Status> getFriendsTimeline(String id, Paging paging) throws TwitterException {
        throw new IllegalStateException("The Twitter API is not supporting this method anymore");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: twitter4j.Twitter.get(java.lang.String, java.lang.String, java.lang.String, boolean):twitter4j.http.Response
     arg types: [java.lang.String, java.lang.String, java.lang.String, int]
     candidates:
      twitter4j.Twitter.get(java.lang.String, twitter4j.http.PostParameter[], twitter4j.Paging, boolean):twitter4j.http.Response
      twitter4j.Twitter.get(java.lang.String, java.lang.String, java.lang.String, boolean):twitter4j.http.Response */
    public List<Status> getFriendsTimeline(Date since) throws TwitterException {
        return Status.constructStatuses(get(new StringBuffer().append(getBaseURL()).append("statuses/friends_timeline.xml").toString(), "since", this.format.format(since), true), this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: twitter4j.Twitter.get(java.lang.String, java.lang.String, java.lang.String, boolean):twitter4j.http.Response
     arg types: [java.lang.String, java.lang.String, java.lang.String, int]
     candidates:
      twitter4j.Twitter.get(java.lang.String, twitter4j.http.PostParameter[], twitter4j.Paging, boolean):twitter4j.http.Response
      twitter4j.Twitter.get(java.lang.String, java.lang.String, java.lang.String, boolean):twitter4j.http.Response */
    public List<Status> getFriendsTimeline(long sinceId) throws TwitterException {
        return Status.constructStatuses(get(new StringBuffer().append(getBaseURL()).append("statuses/friends_timeline.xml").toString(), "since_id", String.valueOf(sinceId), true), this);
    }

    public List<Status> getFriendsTimeline(String id, Date since) throws TwitterException {
        throw new IllegalStateException("The Twitter API is not supporting this method anymore");
    }

    public List<Status> getFriendsTimeline(String id, long sinceId) throws TwitterException {
        throw new IllegalStateException("The Twitter API is not supporting this method anymore");
    }

    public List<Status> getUserTimeline(String id, int count, Date since) throws TwitterException {
        return Status.constructStatuses(get(new StringBuffer().append(getBaseURL()).append("statuses/user_timeline/").append(id).append(".xml").toString(), "since", this.format.format(since), "count", String.valueOf(count), this.http.isAuthenticationEnabled()), this);
    }

    public List<Status> getUserTimeline(String id, int count, long sinceId) throws TwitterException {
        return getUserTimeline(id, new Paging(sinceId).count(count));
    }

    public List<Status> getUserTimeline(String id, Paging paging) throws TwitterException {
        return Status.constructStatuses(get(new StringBuffer().append(getBaseURL()).append("statuses/user_timeline/").append(id).append(".xml").toString(), (PostParameter[]) null, paging, this.http.isAuthenticationEnabled()), this);
    }

    public List<Status> getUserTimeline(String id, Date since) throws TwitterException {
        return Status.constructStatuses(get(new StringBuffer().append(getBaseURL()).append("statuses/user_timeline/").append(id).append(".xml").toString(), "since", this.format.format(since), this.http.isAuthenticationEnabled()), this);
    }

    public List<Status> getUserTimeline(String id, int count) throws TwitterException {
        return Status.constructStatuses(get(new StringBuffer().append(getBaseURL()).append("statuses/user_timeline/").append(id).append(".xml").toString(), "count", String.valueOf(count), this.http.isAuthenticationEnabled()), this);
    }

    public List<Status> getUserTimeline(int count, Date since) throws TwitterException {
        return Status.constructStatuses(get(new StringBuffer().append(getBaseURL()).append("statuses/user_timeline.xml").toString(), "since", this.format.format(since), "count", String.valueOf(count), true), this);
    }

    public List<Status> getUserTimeline(int count, long sinceId) throws TwitterException {
        return getUserTimeline(new Paging(sinceId).count(count));
    }

    public List<Status> getUserTimeline(String id) throws TwitterException {
        return Status.constructStatuses(get(new StringBuffer().append(getBaseURL()).append("statuses/user_timeline/").append(id).append(".xml").toString(), this.http.isAuthenticationEnabled()), this);
    }

    public List<Status> getUserTimeline(String id, long sinceId) throws TwitterException {
        return getUserTimeline(id, new Paging(sinceId));
    }

    public List<Status> getUserTimeline() throws TwitterException {
        return Status.constructStatuses(get(new StringBuffer().append(getBaseURL()).append("statuses/user_timeline.xml").toString(), true), this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: twitter4j.Twitter.get(java.lang.String, twitter4j.http.PostParameter[], twitter4j.Paging, boolean):twitter4j.http.Response
     arg types: [java.lang.String, ?[OBJECT, ARRAY], twitter4j.Paging, int]
     candidates:
      twitter4j.Twitter.get(java.lang.String, java.lang.String, java.lang.String, boolean):twitter4j.http.Response
      twitter4j.Twitter.get(java.lang.String, twitter4j.http.PostParameter[], twitter4j.Paging, boolean):twitter4j.http.Response */
    public List<Status> getUserTimeline(Paging paging) throws TwitterException {
        return Status.constructStatuses(get(new StringBuffer().append(getBaseURL()).append("statuses/user_timeline.xml").toString(), (PostParameter[]) null, paging, true), this);
    }

    public List<Status> getUserTimeline(long sinceId) throws TwitterException {
        return getUserTimeline(new Paging(sinceId));
    }

    public List<Status> getReplies() throws TwitterException {
        return Status.constructStatuses(get(new StringBuffer().append(getBaseURL()).append("statuses/replies.xml").toString(), true), this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: twitter4j.Twitter.get(java.lang.String, java.lang.String, java.lang.String, boolean):twitter4j.http.Response
     arg types: [java.lang.String, java.lang.String, java.lang.String, int]
     candidates:
      twitter4j.Twitter.get(java.lang.String, twitter4j.http.PostParameter[], twitter4j.Paging, boolean):twitter4j.http.Response
      twitter4j.Twitter.get(java.lang.String, java.lang.String, java.lang.String, boolean):twitter4j.http.Response */
    public List<Status> getReplies(long sinceId) throws TwitterException {
        return Status.constructStatuses(get(new StringBuffer().append(getBaseURL()).append("statuses/replies.xml").toString(), "since_id", String.valueOf(sinceId), true), this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: twitter4j.Twitter.get(java.lang.String, java.lang.String, java.lang.String, boolean):twitter4j.http.Response
     arg types: [java.lang.String, java.lang.String, java.lang.String, int]
     candidates:
      twitter4j.Twitter.get(java.lang.String, twitter4j.http.PostParameter[], twitter4j.Paging, boolean):twitter4j.http.Response
      twitter4j.Twitter.get(java.lang.String, java.lang.String, java.lang.String, boolean):twitter4j.http.Response */
    public List<Status> getRepliesByPage(int page) throws TwitterException {
        if (page >= 1) {
            return Status.constructStatuses(get(new StringBuffer().append(getBaseURL()).append("statuses/replies.xml").toString(), "page", String.valueOf(page), true), this);
        }
        throw new IllegalArgumentException(new StringBuffer().append("page should be positive integer. passed:").append(page).toString());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: twitter4j.Twitter.get(java.lang.String, java.lang.String, java.lang.String, boolean):twitter4j.http.Response
     arg types: [java.lang.String, java.lang.String, java.lang.String, int]
     candidates:
      twitter4j.Twitter.get(java.lang.String, twitter4j.http.PostParameter[], twitter4j.Paging, boolean):twitter4j.http.Response
      twitter4j.Twitter.get(java.lang.String, java.lang.String, java.lang.String, boolean):twitter4j.http.Response */
    public List<Status> getReplies(int page) throws TwitterException {
        if (page >= 1) {
            return Status.constructStatuses(get(new StringBuffer().append(getBaseURL()).append("statuses/replies.xml").toString(), "page", String.valueOf(page), true), this);
        }
        throw new IllegalArgumentException(new StringBuffer().append("page should be positive integer. passed:").append(page).toString());
    }

    public List<Status> getReplies(long sinceId, int page) throws TwitterException {
        if (page >= 1) {
            return Status.constructStatuses(get(new StringBuffer().append(getBaseURL()).append("statuses/replies.xml").toString(), "since_id", String.valueOf(sinceId), "page", String.valueOf(page), true), this);
        }
        throw new IllegalArgumentException(new StringBuffer().append("page should be positive integer. passed:").append(page).toString());
    }

    public List<Status> getMentions() throws TwitterException {
        return Status.constructStatuses(get(new StringBuffer().append(getBaseURL()).append("statuses/mentions.xml").toString(), null, true), this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: twitter4j.Twitter.get(java.lang.String, twitter4j.http.PostParameter[], twitter4j.Paging, boolean):twitter4j.http.Response
     arg types: [java.lang.String, ?[OBJECT, ARRAY], twitter4j.Paging, int]
     candidates:
      twitter4j.Twitter.get(java.lang.String, java.lang.String, java.lang.String, boolean):twitter4j.http.Response
      twitter4j.Twitter.get(java.lang.String, twitter4j.http.PostParameter[], twitter4j.Paging, boolean):twitter4j.http.Response */
    public List<Status> getMentions(Paging paging) throws TwitterException {
        return Status.constructStatuses(get(new StringBuffer().append(getBaseURL()).append("statuses/mentions.xml").toString(), (PostParameter[]) null, paging, true), this);
    }

    public List<Status> getRetweetedByMe() throws TwitterException {
        return Status.constructStatuses(get(new StringBuffer().append(getBaseURL()).append("statuses/retweeted_by_me.xml").toString(), null, true), this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: twitter4j.Twitter.get(java.lang.String, twitter4j.http.PostParameter[], twitter4j.Paging, boolean):twitter4j.http.Response
     arg types: [java.lang.String, ?[OBJECT, ARRAY], twitter4j.Paging, int]
     candidates:
      twitter4j.Twitter.get(java.lang.String, java.lang.String, java.lang.String, boolean):twitter4j.http.Response
      twitter4j.Twitter.get(java.lang.String, twitter4j.http.PostParameter[], twitter4j.Paging, boolean):twitter4j.http.Response */
    public List<Status> getRetweetedByMe(Paging paging) throws TwitterException {
        return Status.constructStatuses(get(new StringBuffer().append(getBaseURL()).append("statuses/retweeted_by_me.xml").toString(), (PostParameter[]) null, paging, true), this);
    }

    public List<Status> getRetweetedToMe() throws TwitterException {
        return Status.constructStatuses(get(new StringBuffer().append(getBaseURL()).append("statuses/retweeted_to_me.xml").toString(), null, true), this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: twitter4j.Twitter.get(java.lang.String, twitter4j.http.PostParameter[], twitter4j.Paging, boolean):twitter4j.http.Response
     arg types: [java.lang.String, ?[OBJECT, ARRAY], twitter4j.Paging, int]
     candidates:
      twitter4j.Twitter.get(java.lang.String, java.lang.String, java.lang.String, boolean):twitter4j.http.Response
      twitter4j.Twitter.get(java.lang.String, twitter4j.http.PostParameter[], twitter4j.Paging, boolean):twitter4j.http.Response */
    public List<Status> getRetweetedToMe(Paging paging) throws TwitterException {
        return Status.constructStatuses(get(new StringBuffer().append(getBaseURL()).append("statuses/retweeted_to_me.xml").toString(), (PostParameter[]) null, paging, true), this);
    }

    public List<Status> getRetweetsOfMe() throws TwitterException {
        return Status.constructStatuses(get(new StringBuffer().append(getBaseURL()).append("statuses/retweets_of_me.xml").toString(), null, true), this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: twitter4j.Twitter.get(java.lang.String, twitter4j.http.PostParameter[], twitter4j.Paging, boolean):twitter4j.http.Response
     arg types: [java.lang.String, ?[OBJECT, ARRAY], twitter4j.Paging, int]
     candidates:
      twitter4j.Twitter.get(java.lang.String, java.lang.String, java.lang.String, boolean):twitter4j.http.Response
      twitter4j.Twitter.get(java.lang.String, twitter4j.http.PostParameter[], twitter4j.Paging, boolean):twitter4j.http.Response */
    public List<Status> getRetweetsOfMe(Paging paging) throws TwitterException {
        return Status.constructStatuses(get(new StringBuffer().append(getBaseURL()).append("statuses/retweets_of_me.xml").toString(), (PostParameter[]) null, paging, true), this);
    }

    public Status show(int id) throws TwitterException {
        return showStatus((long) id);
    }

    public Status show(long id) throws TwitterException {
        return new Status(get(new StringBuffer().append(getBaseURL()).append("statuses/show/").append(id).append(".xml").toString(), false), this);
    }

    public Status showStatus(long id) throws TwitterException {
        return new Status(get(new StringBuffer().append(getBaseURL()).append("statuses/show/").append(id).append(".xml").toString(), false), this);
    }

    public Status update(String status) throws TwitterException {
        return updateStatus(status);
    }

    public Status updateStatus(String status) throws TwitterException {
        return new Status(this.http.post(new StringBuffer().append(getBaseURL()).append("statuses/update.xml").toString(), new PostParameter[]{new PostParameter("status", status), new PostParameter("source", this.source)}, true), this);
    }

    public Status updateStatus(String status, double latitude, double longitude) throws TwitterException {
        return new Status(this.http.post(new StringBuffer().append(getBaseURL()).append("statuses/update.xml").toString(), new PostParameter[]{new PostParameter("status", status), new PostParameter("lat", latitude), new PostParameter("long", longitude), new PostParameter("source", this.source)}, true), this);
    }

    public Status update(String status, long inReplyToStatusId) throws TwitterException {
        return updateStatus(status, inReplyToStatusId);
    }

    public Status updateStatus(String status, long inReplyToStatusId) throws TwitterException {
        return new Status(this.http.post(new StringBuffer().append(getBaseURL()).append("statuses/update.xml").toString(), new PostParameter[]{new PostParameter("status", status), new PostParameter("in_reply_to_status_id", String.valueOf(inReplyToStatusId)), new PostParameter("source", this.source)}, true), this);
    }

    public Status updateStatus(String status, long inReplyToStatusId, double latitude, double longitude) throws TwitterException {
        return new Status(this.http.post(new StringBuffer().append(getBaseURL()).append("statuses/update.xml").toString(), new PostParameter[]{new PostParameter("status", status), new PostParameter("lat", latitude), new PostParameter("long", longitude), new PostParameter("in_reply_to_status_id", String.valueOf(inReplyToStatusId)), new PostParameter("source", this.source)}, true), this);
    }

    public Status destroyStatus(long statusId) throws TwitterException {
        return new Status(this.http.post(new StringBuffer().append(getBaseURL()).append("statuses/destroy/").append(statusId).append(".xml").toString(), new PostParameter[0], true), this);
    }

    public Status retweetStatus(long statusId) throws TwitterException {
        return new Status(this.http.post(new StringBuffer().append(getBaseURL()).append("statuses/retweet/").append(statusId).append(".xml").toString(), new PostParameter[0], true), this);
    }

    public List<RetweetDetails> getRetweets(long statusId) throws TwitterException {
        return RetweetDetails.createRetweetDetails(get(new StringBuffer().append(getBaseURL()).append("statuses/retweets/").append(statusId).append(".xml").toString(), true), this);
    }

    public User getUserDetail(String id) throws TwitterException {
        return showUser(id);
    }

    public User showUser(String id) throws TwitterException {
        return new User(get(new StringBuffer().append(getBaseURL()).append("users/show/").append(id).append(".xml").toString(), this.http.isAuthenticationEnabled()), this);
    }

    public List<User> getFriends() throws TwitterException {
        return getFriendsStatuses();
    }

    public List<User> getFriendsStatuses() throws TwitterException {
        return User.constructUsers(get(new StringBuffer().append(getBaseURL()).append("statuses/friends.xml").toString(), true), this);
    }

    public List<User> getFriends(Paging paging) throws TwitterException {
        return getFriendsStatuses(paging);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: twitter4j.Twitter.get(java.lang.String, twitter4j.http.PostParameter[], twitter4j.Paging, boolean):twitter4j.http.Response
     arg types: [java.lang.String, ?[OBJECT, ARRAY], twitter4j.Paging, int]
     candidates:
      twitter4j.Twitter.get(java.lang.String, java.lang.String, java.lang.String, boolean):twitter4j.http.Response
      twitter4j.Twitter.get(java.lang.String, twitter4j.http.PostParameter[], twitter4j.Paging, boolean):twitter4j.http.Response */
    public List<User> getFriendsStatuses(Paging paging) throws TwitterException {
        return User.constructUsers(get(new StringBuffer().append(getBaseURL()).append("statuses/friends.xml").toString(), (PostParameter[]) null, paging, true), this);
    }

    public List<User> getFriends(int page) throws TwitterException {
        return getFriendsStatuses(new Paging(page));
    }

    public List<User> getFriends(String id) throws TwitterException {
        return getFriendsStatuses(id);
    }

    public List<User> getFriendsStatuses(String id) throws TwitterException {
        return User.constructUsers(get(new StringBuffer().append(getBaseURL()).append("statuses/friends/").append(id).append(".xml").toString(), false), this);
    }

    public List<User> getFriends(String id, Paging paging) throws TwitterException {
        return getFriendsStatuses(id, paging);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: twitter4j.Twitter.get(java.lang.String, twitter4j.http.PostParameter[], twitter4j.Paging, boolean):twitter4j.http.Response
     arg types: [java.lang.String, ?[OBJECT, ARRAY], twitter4j.Paging, int]
     candidates:
      twitter4j.Twitter.get(java.lang.String, java.lang.String, java.lang.String, boolean):twitter4j.http.Response
      twitter4j.Twitter.get(java.lang.String, twitter4j.http.PostParameter[], twitter4j.Paging, boolean):twitter4j.http.Response */
    public List<User> getFriendsStatuses(String id, Paging paging) throws TwitterException {
        return User.constructUsers(get(new StringBuffer().append(getBaseURL()).append("statuses/friends/").append(id).append(".xml").toString(), (PostParameter[]) null, paging, false), this);
    }

    public List<User> getFriends(String id, int page) throws TwitterException {
        return getFriendsStatuses(id, new Paging(page));
    }

    public List<User> getFollowers() throws TwitterException {
        return getFollowersStatuses();
    }

    public List<User> getFollowersStatuses() throws TwitterException {
        return User.constructUsers(get(new StringBuffer().append(getBaseURL()).append("statuses/followers.xml").toString(), true), this);
    }

    public List<User> getFollowers(Paging paging) throws TwitterException {
        return getFollowersStatuses(paging);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: twitter4j.Twitter.get(java.lang.String, twitter4j.http.PostParameter[], twitter4j.Paging, boolean):twitter4j.http.Response
     arg types: [java.lang.String, ?[OBJECT, ARRAY], twitter4j.Paging, int]
     candidates:
      twitter4j.Twitter.get(java.lang.String, java.lang.String, java.lang.String, boolean):twitter4j.http.Response
      twitter4j.Twitter.get(java.lang.String, twitter4j.http.PostParameter[], twitter4j.Paging, boolean):twitter4j.http.Response */
    public List<User> getFollowersStatuses(Paging paging) throws TwitterException {
        return User.constructUsers(get(new StringBuffer().append(getBaseURL()).append("statuses/followers.xml").toString(), (PostParameter[]) null, paging, true), this);
    }

    public List<User> getFollowers(int page) throws TwitterException {
        return getFollowersStatuses(new Paging(page));
    }

    public List<User> getFollowers(String id) throws TwitterException {
        return getFollowersStatuses(id);
    }

    public List<User> getFollowersStatuses(String id) throws TwitterException {
        return User.constructUsers(get(new StringBuffer().append(getBaseURL()).append("statuses/followers/").append(id).append(".xml").toString(), true), this);
    }

    public List<User> getFollowers(String id, Paging paging) throws TwitterException {
        return getFollowersStatuses(id, paging);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: twitter4j.Twitter.get(java.lang.String, twitter4j.http.PostParameter[], twitter4j.Paging, boolean):twitter4j.http.Response
     arg types: [java.lang.String, ?[OBJECT, ARRAY], twitter4j.Paging, int]
     candidates:
      twitter4j.Twitter.get(java.lang.String, java.lang.String, java.lang.String, boolean):twitter4j.http.Response
      twitter4j.Twitter.get(java.lang.String, twitter4j.http.PostParameter[], twitter4j.Paging, boolean):twitter4j.http.Response */
    public List<User> getFollowersStatuses(String id, Paging paging) throws TwitterException {
        return User.constructUsers(get(new StringBuffer().append(getBaseURL()).append("statuses/followers/").append(id).append(".xml").toString(), (PostParameter[]) null, paging, true), this);
    }

    public List<User> getFollowers(String id, int page) throws TwitterException {
        return getFollowersStatuses(id, new Paging(page));
    }

    public List<User> getFeatured() throws TwitterException {
        return User.constructUsers(get(new StringBuffer().append(getBaseURL()).append("statuses/featured.xml").toString(), true), this);
    }

    public List<DirectMessage> getDirectMessages() throws TwitterException {
        return DirectMessage.constructDirectMessages(get(new StringBuffer().append(getBaseURL()).append("direct_messages.xml").toString(), true), this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: twitter4j.Twitter.get(java.lang.String, twitter4j.http.PostParameter[], twitter4j.Paging, boolean):twitter4j.http.Response
     arg types: [java.lang.String, ?[OBJECT, ARRAY], twitter4j.Paging, int]
     candidates:
      twitter4j.Twitter.get(java.lang.String, java.lang.String, java.lang.String, boolean):twitter4j.http.Response
      twitter4j.Twitter.get(java.lang.String, twitter4j.http.PostParameter[], twitter4j.Paging, boolean):twitter4j.http.Response */
    public List<DirectMessage> getDirectMessages(Paging paging) throws TwitterException {
        return DirectMessage.constructDirectMessages(get(new StringBuffer().append(getBaseURL()).append("direct_messages.xml").toString(), (PostParameter[]) null, paging, true), this);
    }

    public List<DirectMessage> getDirectMessagesByPage(int page) throws TwitterException {
        return getDirectMessages(new Paging(page));
    }

    public List<DirectMessage> getDirectMessages(int page, int sinceId) throws TwitterException {
        return getDirectMessages(new Paging(page).sinceId(sinceId));
    }

    public List<DirectMessage> getDirectMessages(int sinceId) throws TwitterException {
        return getDirectMessages(new Paging((long) sinceId));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: twitter4j.Twitter.get(java.lang.String, java.lang.String, java.lang.String, boolean):twitter4j.http.Response
     arg types: [java.lang.String, java.lang.String, java.lang.String, int]
     candidates:
      twitter4j.Twitter.get(java.lang.String, twitter4j.http.PostParameter[], twitter4j.Paging, boolean):twitter4j.http.Response
      twitter4j.Twitter.get(java.lang.String, java.lang.String, java.lang.String, boolean):twitter4j.http.Response */
    public List<DirectMessage> getDirectMessages(Date since) throws TwitterException {
        return DirectMessage.constructDirectMessages(get(new StringBuffer().append(getBaseURL()).append("direct_messages.xml").toString(), "since", this.format.format(since), true), this);
    }

    public List<DirectMessage> getSentDirectMessages() throws TwitterException {
        return DirectMessage.constructDirectMessages(get(new StringBuffer().append(getBaseURL()).append("direct_messages/sent.xml").toString(), new PostParameter[0], true), this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: twitter4j.Twitter.get(java.lang.String, twitter4j.http.PostParameter[], twitter4j.Paging, boolean):twitter4j.http.Response
     arg types: [java.lang.String, twitter4j.http.PostParameter[], twitter4j.Paging, int]
     candidates:
      twitter4j.Twitter.get(java.lang.String, java.lang.String, java.lang.String, boolean):twitter4j.http.Response
      twitter4j.Twitter.get(java.lang.String, twitter4j.http.PostParameter[], twitter4j.Paging, boolean):twitter4j.http.Response */
    public List<DirectMessage> getSentDirectMessages(Paging paging) throws TwitterException {
        return DirectMessage.constructDirectMessages(get(new StringBuffer().append(getBaseURL()).append("direct_messages/sent.xml").toString(), new PostParameter[0], paging, true), this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: twitter4j.Twitter.get(java.lang.String, java.lang.String, java.lang.String, boolean):twitter4j.http.Response
     arg types: [java.lang.String, java.lang.String, java.lang.String, int]
     candidates:
      twitter4j.Twitter.get(java.lang.String, twitter4j.http.PostParameter[], twitter4j.Paging, boolean):twitter4j.http.Response
      twitter4j.Twitter.get(java.lang.String, java.lang.String, java.lang.String, boolean):twitter4j.http.Response */
    public List<DirectMessage> getSentDirectMessages(Date since) throws TwitterException {
        return DirectMessage.constructDirectMessages(get(new StringBuffer().append(getBaseURL()).append("direct_messages/sent.xml").toString(), "since", this.format.format(since), true), this);
    }

    public List<DirectMessage> getSentDirectMessages(int sinceId) throws TwitterException {
        return getSentDirectMessages(new Paging((long) sinceId));
    }

    public List<DirectMessage> getSentDirectMessages(int page, int sinceId) throws TwitterException {
        return getSentDirectMessages(new Paging(page, (long) sinceId));
    }

    public DirectMessage sendDirectMessage(String id, String text) throws TwitterException {
        return new DirectMessage(this.http.post(new StringBuffer().append(getBaseURL()).append("direct_messages/new.xml").toString(), new PostParameter[]{new PostParameter("user", id), new PostParameter((String) DesignerProperty.PROPERTY_TYPE_TEXT, text)}, true), this);
    }

    public DirectMessage deleteDirectMessage(int id) throws TwitterException {
        return destroyDirectMessage(id);
    }

    public DirectMessage destroyDirectMessage(int id) throws TwitterException {
        return new DirectMessage(this.http.post(new StringBuffer().append(getBaseURL()).append("direct_messages/destroy/").append(id).append(".xml").toString(), new PostParameter[0], true), this);
    }

    public User create(String id) throws TwitterException {
        return createFriendship(id);
    }

    public User createFriendship(String id) throws TwitterException {
        return new User(this.http.post(new StringBuffer().append(getBaseURL()).append("friendships/create/").append(id).append(".xml").toString(), new PostParameter[0], true), this);
    }

    public User createFriendship(String id, boolean follow) throws TwitterException {
        return new User(this.http.post(new StringBuffer().append(getBaseURL()).append("friendships/create/").append(id).append(".xml").toString(), new PostParameter[]{new PostParameter("follow", String.valueOf(follow))}, true), this);
    }

    public User destroy(String id) throws TwitterException {
        return destroyFriendship(id);
    }

    public User destroyFriendship(String id) throws TwitterException {
        return new User(this.http.post(new StringBuffer().append(getBaseURL()).append("friendships/destroy/").append(id).append(".xml").toString(), new PostParameter[0], true), this);
    }

    public boolean exists(String userA, String userB) throws TwitterException {
        return existsFriendship(userA, userB);
    }

    public boolean existsFriendship(String userA, String userB) throws TwitterException {
        return -1 != get(new StringBuffer().append(getBaseURL()).append("friendships/exists.xml").toString(), "user_a", userA, "user_b", userB, true).asString().indexOf("true");
    }

    public IDs getFriendsIDs() throws TwitterException {
        return getFriendsIDs(-1L);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: twitter4j.Twitter.get(java.lang.String, twitter4j.http.PostParameter[], twitter4j.Paging, boolean):twitter4j.http.Response
     arg types: [java.lang.String, ?[OBJECT, ARRAY], twitter4j.Paging, int]
     candidates:
      twitter4j.Twitter.get(java.lang.String, java.lang.String, java.lang.String, boolean):twitter4j.http.Response
      twitter4j.Twitter.get(java.lang.String, twitter4j.http.PostParameter[], twitter4j.Paging, boolean):twitter4j.http.Response */
    public IDs getFriendsIDs(Paging paging) throws TwitterException {
        return new IDs(get(new StringBuffer().append(getBaseURL()).append("friends/ids.xml").toString(), (PostParameter[]) null, paging, true));
    }

    public IDs getFriendsIDs(long cursor) throws TwitterException {
        return new IDs(get(new StringBuffer().append(getBaseURL()).append("friends/ids.xml?cursor=").append(cursor).toString(), true));
    }

    public IDs getFriendsIDs(int userId) throws TwitterException {
        return getFriendsIDs(userId, -1);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: twitter4j.Twitter.get(java.lang.String, twitter4j.http.PostParameter[], twitter4j.Paging, boolean):twitter4j.http.Response
     arg types: [java.lang.String, ?[OBJECT, ARRAY], twitter4j.Paging, int]
     candidates:
      twitter4j.Twitter.get(java.lang.String, java.lang.String, java.lang.String, boolean):twitter4j.http.Response
      twitter4j.Twitter.get(java.lang.String, twitter4j.http.PostParameter[], twitter4j.Paging, boolean):twitter4j.http.Response */
    public IDs getFriendsIDs(int userId, Paging paging) throws TwitterException {
        return new IDs(get(new StringBuffer().append(getBaseURL()).append("friends/ids.xml?user_id=").append(userId).toString(), (PostParameter[]) null, paging, true));
    }

    public IDs getFriendsIDs(int userId, long cursor) throws TwitterException {
        return new IDs(get(new StringBuffer().append(getBaseURL()).append("friends/ids.xml?user_id=").append(userId).append("&cursor=").append(cursor).toString(), true));
    }

    public IDs getFriendsIDs(String screenName) throws TwitterException {
        return getFriendsIDs(screenName, -1);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: twitter4j.Twitter.get(java.lang.String, twitter4j.http.PostParameter[], twitter4j.Paging, boolean):twitter4j.http.Response
     arg types: [java.lang.String, ?[OBJECT, ARRAY], twitter4j.Paging, int]
     candidates:
      twitter4j.Twitter.get(java.lang.String, java.lang.String, java.lang.String, boolean):twitter4j.http.Response
      twitter4j.Twitter.get(java.lang.String, twitter4j.http.PostParameter[], twitter4j.Paging, boolean):twitter4j.http.Response */
    public IDs getFriendsIDs(String screenName, Paging paging) throws TwitterException {
        return new IDs(get(new StringBuffer().append(getBaseURL()).append("friends/ids.xml?screen_name=").append(screenName).toString(), (PostParameter[]) null, paging, true));
    }

    public IDs getFriendsIDs(String screenName, long cursor) throws TwitterException {
        return new IDs(get(new StringBuffer().append(getBaseURL()).append("friends/ids.xml?screen_name=").append(screenName).append("&cursor=").append(cursor).toString(), true));
    }

    public IDs getFollowersIDs() throws TwitterException {
        return getFollowersIDs(-1L);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: twitter4j.Twitter.get(java.lang.String, twitter4j.http.PostParameter[], twitter4j.Paging, boolean):twitter4j.http.Response
     arg types: [java.lang.String, ?[OBJECT, ARRAY], twitter4j.Paging, int]
     candidates:
      twitter4j.Twitter.get(java.lang.String, java.lang.String, java.lang.String, boolean):twitter4j.http.Response
      twitter4j.Twitter.get(java.lang.String, twitter4j.http.PostParameter[], twitter4j.Paging, boolean):twitter4j.http.Response */
    public IDs getFollowersIDs(Paging paging) throws TwitterException {
        return new IDs(get(new StringBuffer().append(getBaseURL()).append("followers/ids.xml").toString(), (PostParameter[]) null, paging, true));
    }

    public IDs getFollowersIDs(long cursor) throws TwitterException {
        return new IDs(get(new StringBuffer().append(getBaseURL()).append("followers/ids.xml?cursor=").append(cursor).toString(), true));
    }

    public IDs getFollowersIDs(int userId) throws TwitterException {
        return getFollowersIDs(userId, -1);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: twitter4j.Twitter.get(java.lang.String, twitter4j.http.PostParameter[], twitter4j.Paging, boolean):twitter4j.http.Response
     arg types: [java.lang.String, ?[OBJECT, ARRAY], twitter4j.Paging, int]
     candidates:
      twitter4j.Twitter.get(java.lang.String, java.lang.String, java.lang.String, boolean):twitter4j.http.Response
      twitter4j.Twitter.get(java.lang.String, twitter4j.http.PostParameter[], twitter4j.Paging, boolean):twitter4j.http.Response */
    public IDs getFollowersIDs(int userId, Paging paging) throws TwitterException {
        return new IDs(get(new StringBuffer().append(getBaseURL()).append("followers/ids.xml?user_id=").append(userId).toString(), (PostParameter[]) null, paging, true));
    }

    public IDs getFollowersIDs(int userId, long cursor) throws TwitterException {
        return new IDs(get(new StringBuffer().append(getBaseURL()).append("followers/ids.xml?user_id=").append(userId).append("&cursor=").append(cursor).toString(), true));
    }

    public IDs getFollowersIDs(String screenName) throws TwitterException {
        return getFollowersIDs(screenName, -1);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: twitter4j.Twitter.get(java.lang.String, twitter4j.http.PostParameter[], twitter4j.Paging, boolean):twitter4j.http.Response
     arg types: [java.lang.String, ?[OBJECT, ARRAY], twitter4j.Paging, int]
     candidates:
      twitter4j.Twitter.get(java.lang.String, java.lang.String, java.lang.String, boolean):twitter4j.http.Response
      twitter4j.Twitter.get(java.lang.String, twitter4j.http.PostParameter[], twitter4j.Paging, boolean):twitter4j.http.Response */
    public IDs getFollowersIDs(String screenName, Paging paging) throws TwitterException {
        return new IDs(get(new StringBuffer().append(getBaseURL()).append("followers/ids.xml?screen_name=").append(screenName).toString(), (PostParameter[]) null, paging, true));
    }

    public IDs getFollowersIDs(String screenName, long cursor) throws TwitterException {
        return new IDs(get(new StringBuffer().append(getBaseURL()).append("followers/ids.xml?screen_name=").append(screenName).append("&cursor=").append(cursor).toString(), true));
    }

    public User verifyCredentials() throws TwitterException {
        return new User(get(new StringBuffer().append(getBaseURL()).append("account/verify_credentials.xml").toString(), true), this);
    }

    public User updateLocation(String location) throws TwitterException {
        return new User(this.http.post(new StringBuffer().append(getBaseURL()).append("account/update_location.xml").toString(), new PostParameter[]{new PostParameter("location", location)}, true), this);
    }

    public User updateProfile(String name, String email, String url, String location, String description) throws TwitterException {
        List<PostParameter> profile = new ArrayList<>(5);
        addParameterToList(profile, "name", name);
        addParameterToList(profile, "email", email);
        addParameterToList(profile, "url", url);
        addParameterToList(profile, "location", location);
        addParameterToList(profile, "description", description);
        return new User(this.http.post(new StringBuffer().append(getBaseURL()).append("account/update_profile.xml").toString(), (PostParameter[]) profile.toArray(new PostParameter[profile.size()]), true), this);
    }

    public RateLimitStatus rateLimitStatus() throws TwitterException {
        return new RateLimitStatus(this.http.get(new StringBuffer().append(getBaseURL()).append("account/rate_limit_status.xml").toString(), (getUserId() == null || getPassword() == null) ? false : true));
    }

    static class Device {
        final String DEVICE;

        public Device(String device) {
            this.DEVICE = device;
        }
    }

    public User updateDeliverlyDevice(Device device) throws TwitterException {
        return new User(this.http.post(new StringBuffer().append(getBaseURL()).append("account/update_delivery_device.xml").toString(), new PostParameter[]{new PostParameter("device", device.DEVICE)}, true), this);
    }

    public User updateProfileColors(String profileBackgroundColor, String profileTextColor, String profileLinkColor, String profileSidebarFillColor, String profileSidebarBorderColor) throws TwitterException {
        List<PostParameter> colors = new ArrayList<>(5);
        addParameterToList(colors, "profile_background_color", profileBackgroundColor);
        addParameterToList(colors, "profile_text_color", profileTextColor);
        addParameterToList(colors, "profile_link_color", profileLinkColor);
        addParameterToList(colors, "profile_sidebar_fill_color", profileSidebarFillColor);
        addParameterToList(colors, "profile_sidebar_border_color", profileSidebarBorderColor);
        return new User(this.http.post(new StringBuffer().append(getBaseURL()).append("account/update_profile_colors.xml").toString(), (PostParameter[]) colors.toArray(new PostParameter[colors.size()]), true), this);
    }

    private void addParameterToList(List<PostParameter> colors, String paramName, String color) {
        if (color != null) {
            colors.add(new PostParameter(paramName, color));
        }
    }

    public List<Status> favorites() throws TwitterException {
        return getFavorites();
    }

    public List<Status> getFavorites() throws TwitterException {
        return Status.constructStatuses(get(new StringBuffer().append(getBaseURL()).append("favorites.xml").toString(), new PostParameter[0], true), this);
    }

    public List<Status> favorites(int page) throws TwitterException {
        return getFavorites(page);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: twitter4j.Twitter.get(java.lang.String, java.lang.String, java.lang.String, boolean):twitter4j.http.Response
     arg types: [java.lang.String, java.lang.String, java.lang.String, int]
     candidates:
      twitter4j.Twitter.get(java.lang.String, twitter4j.http.PostParameter[], twitter4j.Paging, boolean):twitter4j.http.Response
      twitter4j.Twitter.get(java.lang.String, java.lang.String, java.lang.String, boolean):twitter4j.http.Response */
    public List<Status> getFavorites(int page) throws TwitterException {
        return Status.constructStatuses(get(new StringBuffer().append(getBaseURL()).append("favorites.xml").toString(), "page", String.valueOf(page), true), this);
    }

    public List<Status> favorites(String id) throws TwitterException {
        return getFavorites(id);
    }

    public List<Status> getFavorites(String id) throws TwitterException {
        return Status.constructStatuses(get(new StringBuffer().append(getBaseURL()).append("favorites/").append(id).append(".xml").toString(), new PostParameter[0], true), this);
    }

    public List<Status> favorites(String id, int page) throws TwitterException {
        return getFavorites(id, page);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: twitter4j.Twitter.get(java.lang.String, java.lang.String, java.lang.String, boolean):twitter4j.http.Response
     arg types: [java.lang.String, java.lang.String, java.lang.String, int]
     candidates:
      twitter4j.Twitter.get(java.lang.String, twitter4j.http.PostParameter[], twitter4j.Paging, boolean):twitter4j.http.Response
      twitter4j.Twitter.get(java.lang.String, java.lang.String, java.lang.String, boolean):twitter4j.http.Response */
    public List<Status> getFavorites(String id, int page) throws TwitterException {
        return Status.constructStatuses(get(new StringBuffer().append(getBaseURL()).append("favorites/").append(id).append(".xml").toString(), "page", String.valueOf(page), true), this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: twitter4j.http.HttpClient.post(java.lang.String, boolean):twitter4j.http.Response
     arg types: [java.lang.String, int]
     candidates:
      twitter4j.http.HttpClient.post(java.lang.String, twitter4j.http.PostParameter[]):twitter4j.http.Response
      twitter4j.http.HttpClient.post(java.lang.String, boolean):twitter4j.http.Response */
    public Status createFavorite(long id) throws TwitterException {
        return new Status(this.http.post(new StringBuffer().append(getBaseURL()).append("favorites/create/").append(id).append(".xml").toString(), true), this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: twitter4j.http.HttpClient.post(java.lang.String, boolean):twitter4j.http.Response
     arg types: [java.lang.String, int]
     candidates:
      twitter4j.http.HttpClient.post(java.lang.String, twitter4j.http.PostParameter[]):twitter4j.http.Response
      twitter4j.http.HttpClient.post(java.lang.String, boolean):twitter4j.http.Response */
    public Status destroyFavorite(long id) throws TwitterException {
        return new Status(this.http.post(new StringBuffer().append(getBaseURL()).append("favorites/destroy/").append(id).append(".xml").toString(), true), this);
    }

    public User follow(String id) throws TwitterException {
        return enableNotification(id);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: twitter4j.http.HttpClient.post(java.lang.String, boolean):twitter4j.http.Response
     arg types: [java.lang.String, int]
     candidates:
      twitter4j.http.HttpClient.post(java.lang.String, twitter4j.http.PostParameter[]):twitter4j.http.Response
      twitter4j.http.HttpClient.post(java.lang.String, boolean):twitter4j.http.Response */
    public User enableNotification(String id) throws TwitterException {
        return new User(this.http.post(new StringBuffer().append(getBaseURL()).append("notifications/follow/").append(id).append(".xml").toString(), true), this);
    }

    public User leave(String id) throws TwitterException {
        return disableNotification(id);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: twitter4j.http.HttpClient.post(java.lang.String, boolean):twitter4j.http.Response
     arg types: [java.lang.String, int]
     candidates:
      twitter4j.http.HttpClient.post(java.lang.String, twitter4j.http.PostParameter[]):twitter4j.http.Response
      twitter4j.http.HttpClient.post(java.lang.String, boolean):twitter4j.http.Response */
    public User disableNotification(String id) throws TwitterException {
        return new User(this.http.post(new StringBuffer().append(getBaseURL()).append("notifications/leave/").append(id).append(".xml").toString(), true), this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: twitter4j.http.HttpClient.post(java.lang.String, boolean):twitter4j.http.Response
     arg types: [java.lang.String, int]
     candidates:
      twitter4j.http.HttpClient.post(java.lang.String, twitter4j.http.PostParameter[]):twitter4j.http.Response
      twitter4j.http.HttpClient.post(java.lang.String, boolean):twitter4j.http.Response */
    public User block(String id) throws TwitterException {
        return new User(this.http.post(new StringBuffer().append(getBaseURL()).append("blocks/create/").append(id).append(".xml").toString(), true), this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: twitter4j.http.HttpClient.post(java.lang.String, boolean):twitter4j.http.Response
     arg types: [java.lang.String, int]
     candidates:
      twitter4j.http.HttpClient.post(java.lang.String, twitter4j.http.PostParameter[]):twitter4j.http.Response
      twitter4j.http.HttpClient.post(java.lang.String, boolean):twitter4j.http.Response */
    public User createBlock(String id) throws TwitterException {
        return new User(this.http.post(new StringBuffer().append(getBaseURL()).append("blocks/create/").append(id).append(".xml").toString(), true), this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: twitter4j.http.HttpClient.post(java.lang.String, boolean):twitter4j.http.Response
     arg types: [java.lang.String, int]
     candidates:
      twitter4j.http.HttpClient.post(java.lang.String, twitter4j.http.PostParameter[]):twitter4j.http.Response
      twitter4j.http.HttpClient.post(java.lang.String, boolean):twitter4j.http.Response */
    public User unblock(String id) throws TwitterException {
        return new User(this.http.post(new StringBuffer().append(getBaseURL()).append("blocks/destroy/").append(id).append(".xml").toString(), true), this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: twitter4j.http.HttpClient.post(java.lang.String, boolean):twitter4j.http.Response
     arg types: [java.lang.String, int]
     candidates:
      twitter4j.http.HttpClient.post(java.lang.String, twitter4j.http.PostParameter[]):twitter4j.http.Response
      twitter4j.http.HttpClient.post(java.lang.String, boolean):twitter4j.http.Response */
    public User destroyBlock(String id) throws TwitterException {
        return new User(this.http.post(new StringBuffer().append(getBaseURL()).append("blocks/destroy/").append(id).append(".xml").toString(), true), this);
    }

    public boolean existsBlock(String id) throws TwitterException {
        try {
            if (-1 == get(new StringBuffer().append(getBaseURL()).append("blocks/exists/").append(id).append(".xml").toString(), true).asString().indexOf("<error>You are not blocking this user.</error>")) {
                return true;
            }
            return false;
        } catch (TwitterException te) {
            if (te.getStatusCode() == 404) {
                return false;
            }
            throw te;
        }
    }

    public List<User> getBlockingUsers() throws TwitterException {
        return User.constructUsers(get(new StringBuffer().append(getBaseURL()).append("blocks/blocking.xml").toString(), true), this);
    }

    public List<User> getBlockingUsers(int page) throws TwitterException {
        return User.constructUsers(get(new StringBuffer().append(getBaseURL()).append("blocks/blocking.xml?page=").append(page).toString(), true), this);
    }

    public IDs getBlockingUsersIDs() throws TwitterException {
        return new IDs(get(new StringBuffer().append(getBaseURL()).append("blocks/blocking/ids.xml").toString(), true));
    }

    public List<SavedSearch> getSavedSearches() throws TwitterException {
        return SavedSearch.constructSavedSearches(get(new StringBuffer().append(getBaseURL()).append("saved_searches.json").toString(), true));
    }

    public SavedSearch showSavedSearch(int id) throws TwitterException {
        return new SavedSearch(get(new StringBuffer().append(getBaseURL()).append("saved_searches/show/").append(id).append(".json").toString(), true));
    }

    public SavedSearch createSavedSearch(String query) throws TwitterException {
        return new SavedSearch(this.http.post(new StringBuffer().append(getBaseURL()).append("saved_searches/create.json").toString(), new PostParameter[]{new PostParameter("query", query)}, true));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: twitter4j.http.HttpClient.post(java.lang.String, boolean):twitter4j.http.Response
     arg types: [java.lang.String, int]
     candidates:
      twitter4j.http.HttpClient.post(java.lang.String, twitter4j.http.PostParameter[]):twitter4j.http.Response
      twitter4j.http.HttpClient.post(java.lang.String, boolean):twitter4j.http.Response */
    public SavedSearch destroySavedSearch(int id) throws TwitterException {
        return new SavedSearch(this.http.post(new StringBuffer().append(getBaseURL()).append("saved_searches/destroy/").append(id).append(".json").toString(), true));
    }

    public boolean test() throws TwitterException {
        return -1 != get(new StringBuffer().append(getBaseURL()).append("help/test.xml").toString(), false).asString().indexOf("ok");
    }

    public User getAuthenticatedUser() throws TwitterException {
        return new User(get(new StringBuffer().append(getBaseURL()).append("account/verify_credentials.xml").toString(), true), this);
    }

    public String getDowntimeSchedule() throws TwitterException {
        throw new TwitterException("this method is not supported by the Twitter API anymore", new NoSuchMethodException("this method is not supported by the Twitter API anymore"));
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Twitter twitter = (Twitter) o;
        if (!this.baseURL.equals(twitter.baseURL)) {
            return false;
        }
        if (!this.format.equals(twitter.format)) {
            return false;
        }
        if (!this.http.equals(twitter.http)) {
            return false;
        }
        if (!this.searchBaseURL.equals(twitter.searchBaseURL)) {
            return false;
        }
        if (!this.source.equals(twitter.source)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return (((((((this.http.hashCode() * 31) + this.baseURL.hashCode()) * 31) + this.searchBaseURL.hashCode()) * 31) + this.source.hashCode()) * 31) + this.format.hashCode();
    }

    public String toString() {
        return new StringBuffer().append("Twitter{http=").append(this.http).append(", baseURL='").append(this.baseURL).append('\'').append(", searchBaseURL='").append(this.searchBaseURL).append('\'').append(", source='").append(this.source).append('\'').append(", format=").append(this.format).append('}').toString();
    }
}
