package twitter4j;

import java.util.Date;
import org.w3c.dom.Element;
import twitter4j.http.Response;

public class RateLimitStatus extends TwitterResponse {
    private static final long serialVersionUID = 933996804168952707L;
    private int hourlyLimit;
    private int remainingHits;
    private Date resetTime;
    private int resetTimeInSeconds;

    RateLimitStatus(Response res) throws TwitterException {
        super(res);
        Element elem = res.asDocument().getDocumentElement();
        this.remainingHits = getChildInt("remaining-hits", elem);
        this.hourlyLimit = getChildInt("hourly-limit", elem);
        this.resetTimeInSeconds = getChildInt("reset-time-in-seconds", elem);
        this.resetTime = getChildDate("reset-time", elem, "yyyy-M-d'T'HH:mm:ss+00:00");
    }

    public int getRemainingHits() {
        return this.remainingHits;
    }

    public int getHourlyLimit() {
        return this.hourlyLimit;
    }

    public int getResetTimeInSeconds() {
        return this.resetTimeInSeconds;
    }

    public Date getDateTime() {
        return this.resetTime;
    }

    public Date getResetTime() {
        return this.resetTime;
    }
}
