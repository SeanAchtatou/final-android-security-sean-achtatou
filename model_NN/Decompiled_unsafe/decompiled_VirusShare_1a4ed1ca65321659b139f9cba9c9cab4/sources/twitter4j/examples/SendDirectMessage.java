package twitter4j.examples;

import twitter4j.Twitter;
import twitter4j.TwitterException;

public class SendDirectMessage {
    public static void main(String[] args) {
        if (args.length < 4) {
            System.out.println("No TwitterID/Password specified.");
            System.out.println("Usage: java twitter4j.examples.DirectMessage senderID senderPassword message recipientId");
            System.exit(-1);
        }
        try {
            System.out.println(new StringBuffer().append("Direct message successfully sent to ").append(new Twitter(args[0], args[1]).sendDirectMessage(args[2], args[3]).getRecipientScreenName()).toString());
            System.exit(0);
        } catch (TwitterException te) {
            System.out.println(new StringBuffer().append("Failed to send message: ").append(te.getMessage()).toString());
            System.exit(-1);
        }
    }
}
