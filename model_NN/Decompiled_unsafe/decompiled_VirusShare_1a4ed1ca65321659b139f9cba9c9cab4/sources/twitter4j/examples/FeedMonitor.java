package twitter4j.examples;

import com.google.devtools.simple.common.ComponentConstants;
import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.io.FeedException;
import com.sun.syndication.io.SyndFeedInput;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.http.HttpClient;

public class FeedMonitor {
    static Class class$twitter4j$examples$FeedMonitor;
    static Logger log;
    private String feedurl;
    private String fileName;
    private HttpClient http = new HttpClient();
    private Date lastUpdate;
    private Properties prop = new Properties();
    private Twitter twitter;

    static {
        Class cls;
        if (class$twitter4j$examples$FeedMonitor == null) {
            cls = class$("twitter4j.examples.FeedMonitor");
            class$twitter4j$examples$FeedMonitor = cls;
        } else {
            cls = class$twitter4j$examples$FeedMonitor;
        }
        log = LoggerFactory.getLogger(cls);
    }

    static Class class$(String x0) {
        try {
            return Class.forName(x0);
        } catch (ClassNotFoundException x1) {
            throw new NoClassDefFoundError().initCause(x1);
        }
    }

    /* JADX INFO: Multiple debug info for r2v2 java.util.Iterator: [D('i$' java.util.Iterator), D('i$' int)] */
    public static void main(String[] args) {
        int interval = 10;
        List<FeedMonitor> list = new ArrayList<>();
        for (String arg : args) {
            try {
                interval = Integer.parseInt(arg);
            } catch (NumberFormatException e) {
                list.add(new FeedMonitor(arg));
            }
        }
        if (list.size() == 0) {
            list.add(new FeedMonitor("feedmonitor.properties"));
        }
        while (true) {
            for (FeedMonitor monitor : list) {
                monitor.check();
            }
            try {
                log.info(new StringBuffer().append("Sleeping ").append(interval).append(" minutes.").toString());
                Thread.sleep((long) (interval * 60 * 1000));
            } catch (InterruptedException e2) {
            }
        }
    }

    public FeedMonitor(String fileName2) {
        this.fileName = fileName2;
        log.info(new StringBuffer().append("Loading properties from ").append(fileName2).toString());
        try {
            this.prop.load(new FileInputStream(fileName2));
        } catch (IOException ex) {
            log.error(new StringBuffer().append("Configuration file not found:").append(ex.getMessage()).toString());
            System.exit(-1);
        }
        this.twitter = new Twitter(this.prop.getProperty("id"), this.prop.getProperty("password"));
        this.feedurl = this.prop.getProperty("feedurl");
        this.lastUpdate = new Date(Long.valueOf(this.prop.getProperty("lastUpdate", "0")).longValue());
    }

    private void check() {
        log.info("Checking feed from {}", this.feedurl);
        Date latestEntry = this.lastUpdate;
        log.info("Last update is {}", this.lastUpdate);
        try {
            List<SyndEntry> entries = new SyndFeedInput().build(this.http.get(this.feedurl).asDocument()).getEntries();
            Collections.sort(entries, new Comparator<SyndEntry>(this) {
                private final FeedMonitor this$0;

                {
                    this.this$0 = r1;
                }

                public int compare(Object x0, Object x1) {
                    return compare((SyndEntry) x0, (SyndEntry) x1);
                }

                public int compare(SyndEntry o1, SyndEntry o2) {
                    return o1.getPublishedDate().compareTo(o2.getPublishedDate());
                }
            });
            for (SyndEntry entry : entries) {
                if (this.lastUpdate.before(entry.getPublishedDate())) {
                    if (latestEntry.before(entry.getPublishedDate())) {
                        latestEntry = entry.getPublishedDate();
                    }
                    String title = entry.getTitle().split("\n")[0];
                    String link = entry.getLink();
                    log.info("New entry \"{}\" published at {}", title, entry.getPublishedDate());
                    String status = new StringBuffer().append(title).append(" ").append(link).toString();
                    if (status.length() > 160) {
                        if (link.length() > 160) {
                            status = new StringBuffer().append(title.substring(0, title.length() - (status.length() - 159))).append(" ").append(link).toString();
                        } else if (title.length() > 160) {
                            status = title.substring(0, ComponentConstants.TEXTBOX_PREFERRED_WIDTH);
                        } else {
                            status = title;
                        }
                    }
                    log.info("Updating Twitter.");
                    this.twitter.updateStatus(status);
                    log.info("Done.");
                }
            }
            if (!this.lastUpdate.equals(latestEntry)) {
                log.info("Updating last update.");
                this.prop.setProperty("lastUpdate", String.valueOf(latestEntry.getTime()));
                try {
                    this.prop.store(new FileOutputStream(this.fileName), "FeedMonitor");
                } catch (IOException ex1) {
                    log.error(new StringBuffer().append("Failed to save configuration file:").append(ex1.getMessage()).toString());
                }
            } else {
                log.info("No new entry found.");
            }
        } catch (TwitterException te) {
            log.info(new StringBuffer().append("Failed to fetch the feed:").append(te.getMessage()).toString());
        } catch (FeedException fe) {
            log.info(new StringBuffer().append("Failed to parse the feed:").append(fe.getMessage()).toString());
        }
    }
}
