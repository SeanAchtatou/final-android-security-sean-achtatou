package twitter4j.examples;

import twitter4j.Twitter;
import twitter4j.TwitterException;

public class Update {
    public static void main(String[] args) throws TwitterException {
        if (args.length < 3) {
            System.out.println("Usage: java twitter4j.examples.Update ID Password text");
            System.exit(-1);
        }
        System.out.println(new StringBuffer().append("Successfully updated the status to [").append(new Twitter(args[0], args[1]).updateStatus(args[2]).getText()).append("].").toString());
    }
}
