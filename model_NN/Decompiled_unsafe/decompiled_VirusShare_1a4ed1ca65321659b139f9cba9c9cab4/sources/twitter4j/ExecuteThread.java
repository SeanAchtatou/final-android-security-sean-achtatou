package twitter4j;

/* compiled from: Dispatcher */
class ExecuteThread extends Thread {
    private boolean alive = true;
    Dispatcher q;

    ExecuteThread(String name, Dispatcher q2, int index) {
        super(new StringBuffer().append(name).append("[").append(index).append("]").toString());
        this.q = q2;
    }

    public void shutdown() {
        this.alive = false;
    }

    public void run() {
        while (this.alive) {
            Runnable task = this.q.poll();
            if (task != null) {
                try {
                    task.run();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
    }
}
