package twitter4j;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import twitter4j.http.Response;

public class StatusStream {
    private BufferedReader br;
    private InputStream is;
    private Response response;
    private boolean streamAlive;

    StatusStream(InputStream stream) throws IOException {
        this.streamAlive = true;
        this.is = stream;
        this.br = new BufferedReader(new InputStreamReader(stream, "UTF-8"));
    }

    StatusStream(Response response2) throws IOException {
        this(response2.asStream());
        this.response = response2;
    }

    /* JADX WARNING: CFG modification limit reached, blocks count: 126 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public twitter4j.Status next() throws twitter4j.TwitterException {
        /*
            r4 = this;
            boolean r2 = r4.streamAlive
            if (r2 != 0) goto L_0x000d
            java.lang.IllegalStateException r2 = new java.lang.IllegalStateException
            java.lang.String r3 = "Stream already closed."
            r2.<init>(r3)
            throw r2
        L_0x000c:
            r2 = move-exception
        L_0x000d:
            boolean r2 = r4.streamAlive     // Catch:{ IOException -> 0x002d }
            if (r2 == 0) goto L_0x0025
            java.io.BufferedReader r2 = r4.br     // Catch:{ IOException -> 0x002d }
            java.lang.String r1 = r2.readLine()     // Catch:{ IOException -> 0x002d }
            if (r1 == 0) goto L_0x000d
            int r2 = r1.length()     // Catch:{ IOException -> 0x002d }
            if (r2 <= 0) goto L_0x000d
            twitter4j.Status r2 = new twitter4j.Status     // Catch:{ JSONException -> 0x000c }
            r2.<init>(r1)     // Catch:{ JSONException -> 0x000c }
            return r2
        L_0x0025:
            twitter4j.TwitterException r2 = new twitter4j.TwitterException     // Catch:{ IOException -> 0x002d }
            java.lang.String r3 = "Stream closed."
            r2.<init>(r3)     // Catch:{ IOException -> 0x002d }
            throw r2     // Catch:{ IOException -> 0x002d }
        L_0x002d:
            r0 = move-exception
            java.io.InputStream r2 = r4.is     // Catch:{ IOException -> 0x003e }
            r2.close()     // Catch:{ IOException -> 0x003e }
        L_0x0033:
            r2 = 0
            r4.streamAlive = r2
            twitter4j.TwitterException r2 = new twitter4j.TwitterException
            java.lang.String r3 = "Stream closed."
            r2.<init>(r3, r0)
            throw r2
        L_0x003e:
            r2 = move-exception
            goto L_0x0033
        */
        throw new UnsupportedOperationException("Method not decompiled: twitter4j.StatusStream.next():twitter4j.Status");
    }

    public void close() throws IOException {
        this.is.close();
        this.br.close();
        if (this.response != null) {
            this.response.disconnect();
        }
    }
}
