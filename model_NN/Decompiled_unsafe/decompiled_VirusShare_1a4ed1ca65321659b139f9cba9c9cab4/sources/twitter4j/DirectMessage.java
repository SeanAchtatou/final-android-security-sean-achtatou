package twitter4j;

import com.google.devtools.simple.runtime.annotations.DesignerProperty;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import twitter4j.http.Response;

public class DirectMessage extends TwitterResponse implements Serializable {
    private static final long serialVersionUID = -3253021825891789737L;
    private Date created_at;
    private int id;
    private User recipient;
    private int recipient_id;
    private String recipient_screen_name;
    private User sender;
    private int sender_id;
    private String sender_screen_name;
    private String text;

    DirectMessage(Response res, Twitter twitter) throws TwitterException {
        super(res);
        init(res, res.asDocument().getDocumentElement(), twitter);
    }

    DirectMessage(Response res, Element elem, Twitter twitter) throws TwitterException {
        super(res);
        init(res, elem, twitter);
    }

    private void init(Response res, Element elem, Twitter twitter) throws TwitterException {
        ensureRootNodeNameIs("direct_message", elem);
        this.sender = new User(res, (Element) elem.getElementsByTagName("sender").item(0), twitter);
        this.recipient = new User(res, (Element) elem.getElementsByTagName("recipient").item(0), twitter);
        this.id = getChildInt("id", elem);
        this.text = getChildText(DesignerProperty.PROPERTY_TYPE_TEXT, elem);
        this.sender_id = getChildInt("sender_id", elem);
        this.recipient_id = getChildInt("recipient_id", elem);
        this.created_at = getChildDate("created_at", elem);
        this.sender_screen_name = getChildText("sender_screen_name", elem);
        this.recipient_screen_name = getChildText("recipient_screen_name", elem);
    }

    public int getId() {
        return this.id;
    }

    public String getText() {
        return this.text;
    }

    public int getSenderId() {
        return this.sender_id;
    }

    public int getRecipientId() {
        return this.recipient_id;
    }

    public Date getCreatedAt() {
        return this.created_at;
    }

    public String getSenderScreenName() {
        return this.sender_screen_name;
    }

    public String getRecipientScreenName() {
        return this.recipient_screen_name;
    }

    public User getSender() {
        return this.sender;
    }

    public User getRecipient() {
        return this.recipient;
    }

    static List<DirectMessage> constructDirectMessages(Response res, Twitter twitter) throws TwitterException {
        Document doc = res.asDocument();
        if (isRootNodeNilClasses(doc)) {
            return new ArrayList(0);
        }
        try {
            ensureRootNodeNameIs("direct-messages", doc);
            NodeList list = doc.getDocumentElement().getElementsByTagName("direct_message");
            int size = list.getLength();
            List<DirectMessage> messages = new ArrayList<>(size);
            for (int i = 0; i < size; i++) {
                messages.add(new DirectMessage(res, (Element) list.item(i), twitter));
            }
            return messages;
        } catch (TwitterException te) {
            if (isRootNodeNilClasses(doc)) {
                return new ArrayList(0);
            }
            throw te;
        }
    }

    public int hashCode() {
        return this.id;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof DirectMessage) || ((DirectMessage) obj).id != this.id) {
            return false;
        }
        return true;
    }

    public String toString() {
        return new StringBuffer().append("DirectMessage{id=").append(this.id).append(", text='").append(this.text).append('\'').append(", sender_id=").append(this.sender_id).append(", recipient_id=").append(this.recipient_id).append(", created_at=").append(this.created_at).append(", sender_screen_name='").append(this.sender_screen_name).append('\'').append(", recipient_screen_name='").append(this.recipient_screen_name).append('\'').append(", sender=").append(this.sender).append(", recipient=").append(this.recipient).append('}').toString();
    }
}
