package twitter4j;

import java.util.LinkedList;
import java.util.List;

class Dispatcher {
    private boolean active;
    private List<Runnable> q;
    private ExecuteThread[] threads;
    Object ticket;

    static boolean access$000(Dispatcher x0) {
        return x0.active;
    }

    public Dispatcher(String name) {
        this(name, 1);
    }

    public Dispatcher(String name, int threadcount) {
        this.q = new LinkedList();
        this.ticket = new Object();
        this.active = true;
        this.threads = new ExecuteThread[threadcount];
        for (int i = 0; i < this.threads.length; i++) {
            this.threads[i] = new ExecuteThread(name, this, i);
            this.threads[i].setDaemon(true);
            this.threads[i].start();
        }
        Runtime.getRuntime().addShutdownHook(new Thread(this) {
            private final Dispatcher this$0;

            {
                this.this$0 = r1;
            }

            public void run() {
                if (Dispatcher.access$000(this.this$0)) {
                    this.this$0.shutdown();
                }
            }
        });
    }

    public synchronized void invokeLater(Runnable task) {
        synchronized (this.q) {
            this.q.add(task);
        }
        synchronized (this.ticket) {
            this.ticket.notify();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001d, code lost:
        r2 = r4.ticket;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001f, code lost:
        monitor-enter(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        r4.ticket.wait();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Runnable poll() {
        /*
            r4 = this;
        L_0x0000:
            boolean r1 = r4.active
            if (r1 == 0) goto L_0x002d
            java.util.List<java.lang.Runnable> r2 = r4.q
            monitor-enter(r2)
            java.util.List<java.lang.Runnable> r1 = r4.q     // Catch:{ all -> 0x002a }
            int r1 = r1.size()     // Catch:{ all -> 0x002a }
            if (r1 <= 0) goto L_0x001c
            java.util.List<java.lang.Runnable> r1 = r4.q     // Catch:{ all -> 0x002a }
            r3 = 0
            java.lang.Object r0 = r1.remove(r3)     // Catch:{ all -> 0x002a }
            java.lang.Runnable r0 = (java.lang.Runnable) r0     // Catch:{ all -> 0x002a }
            if (r0 == 0) goto L_0x001c
            monitor-exit(r2)     // Catch:{ all -> 0x002a }
        L_0x001b:
            return r0
        L_0x001c:
            monitor-exit(r2)     // Catch:{ all -> 0x002a }
            java.lang.Object r2 = r4.ticket
            monitor-enter(r2)
            java.lang.Object r1 = r4.ticket     // Catch:{ InterruptedException -> 0x002f }
            r1.wait()     // Catch:{ InterruptedException -> 0x002f }
        L_0x0025:
            monitor-exit(r2)     // Catch:{ all -> 0x0027 }
            goto L_0x0000
        L_0x0027:
            r1 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0027 }
            throw r1
        L_0x002a:
            r1 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x002a }
            throw r1
        L_0x002d:
            r0 = 0
            goto L_0x001b
        L_0x002f:
            r1 = move-exception
            goto L_0x0025
        */
        throw new UnsupportedOperationException("Method not decompiled: twitter4j.Dispatcher.poll():java.lang.Runnable");
    }

    public synchronized void shutdown() {
        if (this.active) {
            this.active = false;
            for (ExecuteThread thread : this.threads) {
                thread.shutdown();
            }
            synchronized (this.ticket) {
                this.ticket.notify();
            }
        } else {
            throw new IllegalStateException("Already shutdown");
        }
    }
}
