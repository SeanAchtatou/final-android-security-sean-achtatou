package twitter4j;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import twitter4j.http.Response;
import twitter4j.org.json.JSONArray;
import twitter4j.org.json.JSONException;
import twitter4j.org.json.JSONObject;

public class Trends extends TwitterResponse implements Comparable<Trends> {
    private static final long serialVersionUID = -7151479143843312309L;
    private Date asOf;
    private Date trendAt;
    private Trend[] trends;

    public int compareTo(Object x0) {
        return compareTo((Trends) x0);
    }

    public int compareTo(Trends that) {
        return this.trendAt.compareTo(that.trendAt);
    }

    Trends(Response res, Date asOf2, Date trendAt2, Trend[] trends2) throws TwitterException {
        super(res);
        this.asOf = asOf2;
        this.trendAt = trendAt2;
        this.trends = trends2;
    }

    static List<Trends> constructTrendsList(Response res) throws TwitterException {
        JSONObject json = res.asJSONObject();
        try {
            Date asOf2 = parseDate(json.getString("as_of"));
            JSONObject trendsJson = json.getJSONObject("trends");
            List<Trends> trends2 = new ArrayList<>(trendsJson.length());
            Iterator ite = trendsJson.keys();
            while (ite.hasNext()) {
                String key = (String) ite.next();
                Trend[] trendsArray = jsonArrayToTrendArray(trendsJson.getJSONArray(key));
                if (key.length() == 19) {
                    trends2.add(new Trends(res, asOf2, parseDate(key, "yyyy-MM-dd HH:mm:ss"), trendsArray));
                } else if (key.length() == 16) {
                    trends2.add(new Trends(res, asOf2, parseDate(key, "yyyy-MM-dd HH:mm"), trendsArray));
                } else if (key.length() == 10) {
                    trends2.add(new Trends(res, asOf2, parseDate(key, "yyyy-MM-dd"), trendsArray));
                }
            }
            Collections.sort(trends2);
            return trends2;
        } catch (JSONException jsone) {
            throw new TwitterException(new StringBuffer().append(jsone.getMessage()).append(":").append(res.asString()).toString(), jsone);
        }
    }

    static Trends constructTrends(Response res) throws TwitterException {
        JSONObject json = res.asJSONObject();
        try {
            Date asOf2 = parseDate(json.getString("as_of"));
            return new Trends(res, asOf2, asOf2, jsonArrayToTrendArray(json.getJSONArray("trends")));
        } catch (JSONException jsone) {
            throw new TwitterException(new StringBuffer().append(jsone.getMessage()).append(":").append(res.asString()).toString(), jsone);
        }
    }

    private static Date parseDate(String asOfStr) throws TwitterException {
        if (asOfStr.length() == 10) {
            return new Date(Long.parseLong(asOfStr) * 1000);
        }
        return TwitterResponse.parseDate(asOfStr, "EEE, d MMM yyyy HH:mm:ss z");
    }

    private static Trend[] jsonArrayToTrendArray(JSONArray array) throws JSONException {
        Trend[] trends2 = new Trend[array.length()];
        for (int i = 0; i < array.length(); i++) {
            trends2[i] = new Trend(array.getJSONObject(i));
        }
        return trends2;
    }

    public Trend[] getTrends() {
        return this.trends;
    }

    public Date getAsOf() {
        return this.asOf;
    }

    public Date getTrendAt() {
        return this.trendAt;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Trends)) {
            return false;
        }
        Trends trends1 = (Trends) o;
        if (this.asOf == null ? trends1.asOf != null : !this.asOf.equals(trends1.asOf)) {
            return false;
        }
        if (this.trendAt == null ? trends1.trendAt != null : !this.trendAt.equals(trends1.trendAt)) {
            return false;
        }
        if (!Arrays.equals(this.trends, trends1.trends)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int result;
        int i;
        int i2 = 0;
        if (this.asOf != null) {
            result = this.asOf.hashCode();
        } else {
            result = 0;
        }
        int i3 = result * 31;
        if (this.trendAt != null) {
            i = this.trendAt.hashCode();
        } else {
            i = 0;
        }
        int i4 = (i3 + i) * 31;
        if (this.trends != null) {
            i2 = Arrays.hashCode(this.trends);
        }
        return i4 + i2;
    }

    public String toString() {
        return new StringBuffer().append("Trends{asOf=").append(this.asOf).append(", trendAt=").append(this.trendAt).append(", trends=").append(this.trends == null ? null : Arrays.asList(this.trends)).append('}').toString();
    }
}
