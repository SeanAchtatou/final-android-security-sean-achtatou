package twitter4j;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.security.AccessControlException;
import java.util.Properties;

public class Configuration {
    private static boolean DALVIK;
    static Class class$twitter4j$Configuration;
    private static Properties defaultProperty;

    static {
        init();
    }

    static void init() {
        Class cls;
        Class cls2;
        defaultProperty = new Properties();
        defaultProperty.setProperty("twitter4j.debug", "false");
        defaultProperty.setProperty("twitter4j.source", "Twitter4J");
        defaultProperty.setProperty("twitter4j.clientURL", "http://yusuke.homeip.net/twitter4j/en/twitter4j-{twitter4j.clientVersion}.xml");
        defaultProperty.setProperty("twitter4j.http.userAgent", "twitter4j http://yusuke.homeip.net/twitter4j/ /{twitter4j.clientVersion}");
        defaultProperty.setProperty("twitter4j.http.useSSL", "false");
        defaultProperty.setProperty("twitter4j.http.proxyHost.fallback", "http.proxyHost");
        defaultProperty.setProperty("twitter4j.http.proxyPort.fallback", "http.proxyPort");
        defaultProperty.setProperty("twitter4j.http.connectionTimeout", "20000");
        defaultProperty.setProperty("twitter4j.http.readTimeout", "120000");
        defaultProperty.setProperty("twitter4j.http.retryCount", "3");
        defaultProperty.setProperty("twitter4j.http.retryIntervalSecs", "10");
        defaultProperty.setProperty("twitter4j.async.numThreads", "1");
        defaultProperty.setProperty("twitter4j.clientVersion", Version.getVersion());
        try {
            Class.forName("dalvik.system.VMRuntime");
            defaultProperty.setProperty("twitter4j.dalvik", "true");
        } catch (ClassNotFoundException e) {
            defaultProperty.setProperty("twitter4j.dalvik", "false");
        }
        DALVIK = getBoolean("twitter4j.dalvik");
        if (!loadProperties(defaultProperty, new StringBuffer().append(".").append(File.separatorChar).append("twitter4j.properties").toString())) {
            Properties properties = defaultProperty;
            if (class$twitter4j$Configuration == null) {
                cls = class$("twitter4j.Configuration");
                class$twitter4j$Configuration = cls;
            } else {
                cls = class$twitter4j$Configuration;
            }
            if (!loadProperties(properties, cls.getResourceAsStream(new StringBuffer().append("/WEB-INF/").append("twitter4j.properties").toString()))) {
                Properties properties2 = defaultProperty;
                if (class$twitter4j$Configuration == null) {
                    cls2 = class$("twitter4j.Configuration");
                    class$twitter4j$Configuration = cls2;
                } else {
                    cls2 = class$twitter4j$Configuration;
                }
                if (!loadProperties(properties2, cls2.getResourceAsStream(new StringBuffer().append("/").append("twitter4j.properties").toString()))) {
                }
            }
        }
    }

    static Class class$(String x0) {
        try {
            return Class.forName(x0);
        } catch (ClassNotFoundException x1) {
            throw new NoClassDefFoundError().initCause(x1);
        }
    }

    private static boolean loadProperties(Properties props, String path) {
        try {
            File file = new File(path);
            if (file.exists() && file.isFile()) {
                props.load(new FileInputStream(file));
                return true;
            }
        } catch (Exception e) {
        }
        return false;
    }

    private static boolean loadProperties(Properties props, InputStream is) {
        try {
            props.load(is);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean isDalvik() {
        return DALVIK;
    }

    public static boolean useSSL() {
        return getBoolean("twitter4j.http.useSSL");
    }

    public static String getScheme() {
        return useSSL() ? "https://" : "http://";
    }

    public static String getCilentVersion() {
        return getProperty("twitter4j.clientVersion");
    }

    public static String getCilentVersion(String clientVersion) {
        return getProperty("twitter4j.clientVersion", clientVersion);
    }

    public static String getSource() {
        return getProperty("twitter4j.source");
    }

    public static String getSource(String source) {
        return getProperty("twitter4j.source", source);
    }

    public static String getProxyHost() {
        return getProperty("twitter4j.http.proxyHost");
    }

    public static String getProxyHost(String proxyHost) {
        return getProperty("twitter4j.http.proxyHost", proxyHost);
    }

    public static String getProxyUser() {
        return getProperty("twitter4j.http.proxyUser");
    }

    public static String getProxyUser(String user) {
        return getProperty("twitter4j.http.proxyUser", user);
    }

    public static String getClientURL() {
        return getProperty("twitter4j.clientURL");
    }

    public static String getClientURL(String clientURL) {
        return getProperty("twitter4j.clientURL", clientURL);
    }

    public static String getProxyPassword() {
        return getProperty("twitter4j.http.proxyPassword");
    }

    public static String getProxyPassword(String password) {
        return getProperty("twitter4j.http.proxyPassword", password);
    }

    public static int getProxyPort() {
        return getIntProperty("twitter4j.http.proxyPort");
    }

    public static int getProxyPort(int port) {
        return getIntProperty("twitter4j.http.proxyPort", port);
    }

    public static int getConnectionTimeout() {
        return getIntProperty("twitter4j.http.connectionTimeout");
    }

    public static int getConnectionTimeout(int connectionTimeout) {
        return getIntProperty("twitter4j.http.connectionTimeout", connectionTimeout);
    }

    public static int getReadTimeout() {
        return getIntProperty("twitter4j.http.readTimeout");
    }

    public static int getReadTimeout(int readTimeout) {
        return getIntProperty("twitter4j.http.readTimeout", readTimeout);
    }

    public static int getRetryCount() {
        return getIntProperty("twitter4j.http.retryCount");
    }

    public static int getRetryCount(int retryCount) {
        return getIntProperty("twitter4j.http.retryCount", retryCount);
    }

    public static int getRetryIntervalSecs() {
        return getIntProperty("twitter4j.http.retryIntervalSecs");
    }

    public static int getRetryIntervalSecs(int retryIntervalSecs) {
        return getIntProperty("twitter4j.http.retryIntervalSecs", retryIntervalSecs);
    }

    public static String getUser() {
        return getProperty("twitter4j.user");
    }

    public static String getUser(String userId) {
        return getProperty("twitter4j.user", userId);
    }

    public static String getPassword() {
        return getProperty("twitter4j.password");
    }

    public static String getPassword(String password) {
        return getProperty("twitter4j.password", password);
    }

    public static String getUserAgent() {
        return getProperty("twitter4j.http.userAgent");
    }

    public static String getUserAgent(String userAgent) {
        return getProperty("twitter4j.http.userAgent", userAgent);
    }

    public static String getOAuthConsumerKey() {
        return getProperty("twitter4j.oauth.consumerKey");
    }

    public static String getOAuthConsumerKey(String consumerKey) {
        return getProperty("twitter4j.oauth.consumerKey", consumerKey);
    }

    public static String getOAuthConsumerSecret() {
        return getProperty("twitter4j.oauth.consumerSecret");
    }

    public static String getOAuthConsumerSecret(String consumerSecret) {
        return getProperty("twitter4j.oauth.consumerSecret", consumerSecret);
    }

    public static boolean getBoolean(String name) {
        return Boolean.valueOf(getProperty(name)).booleanValue();
    }

    public static int getIntProperty(String name) {
        try {
            return Integer.parseInt(getProperty(name));
        } catch (NumberFormatException e) {
            return -1;
        }
    }

    public static int getIntProperty(String name, int fallbackValue) {
        try {
            return Integer.parseInt(getProperty(name, String.valueOf(fallbackValue)));
        } catch (NumberFormatException e) {
            return -1;
        }
    }

    public static long getLongProperty(String name) {
        try {
            return Long.parseLong(getProperty(name));
        } catch (NumberFormatException e) {
            return -1;
        }
    }

    public static String getProperty(String name) {
        return getProperty(name, null);
    }

    public static String getProperty(String name, String fallbackValue) {
        String value;
        String fallback;
        try {
            value = System.getProperty(name, fallbackValue);
            if (value == null) {
                value = defaultProperty.getProperty(name);
            }
            if (value == null && (fallback = defaultProperty.getProperty(new StringBuffer().append(name).append(".fallback").toString())) != null) {
                value = System.getProperty(fallback);
            }
        } catch (AccessControlException e) {
            value = fallbackValue;
        }
        return replace(value);
    }

    private static String replace(String value) {
        int closeBrace;
        if (value == null) {
            return value;
        }
        String newValue = value;
        int openBrace = value.indexOf("{", 0);
        if (-1 != openBrace && (closeBrace = value.indexOf("}", openBrace)) > openBrace + 1) {
            String name = value.substring(openBrace + 1, closeBrace);
            if (name.length() > 0) {
                newValue = new StringBuffer().append(value.substring(0, openBrace)).append(getProperty(name)).append(value.substring(closeBrace + 1)).toString();
            }
        }
        return !newValue.equals(value) ? replace(newValue) : value;
    }

    public static int getNumberOfAsyncThreads() {
        return getIntProperty("twitter4j.async.numThreads");
    }

    public static boolean getDebug() {
        return getBoolean("twitter4j.debug");
    }
}
