package twitter4j;

import java.util.List;

public class TwitterAdapter implements TwitterListener {
    public void gotHomeTimeline(List<Status> list) {
    }

    public void gotPublicTimeline(List<Status> list) {
    }

    public void gotFriendsTimeline(List<Status> list) {
    }

    public void gotUserTimeline(List<Status> list) {
    }

    public void gotShow(Status statuses) {
    }

    public void gotShowStatus(Status statuses) {
    }

    public void updated(Status statuses) {
    }

    public void updatedStatus(Status statuses) {
    }

    public void gotReplies(List<Status> list) {
    }

    public void gotMentions(List<Status> list) {
    }

    public void gotRetweetedByMe(List<Status> list) {
    }

    public void gotRetweetedToMe(List<Status> list) {
    }

    public void gotRetweetsOfMe(List<Status> list) {
    }

    public void destroyedStatus(Status destroyedStatus) {
    }

    public void retweetedStatus(Status retweetedStatus) {
    }

    public void gotFriends(List<User> list) {
    }

    public void gotFollowers(List<User> list) {
    }

    public void gotFeatured(List<User> list) {
    }

    public void gotUserDetail(User user) {
    }

    public void gotDirectMessages(List<DirectMessage> list) {
    }

    public void gotSentDirectMessages(List<DirectMessage> list) {
    }

    public void sentDirectMessage(DirectMessage message) {
    }

    public void deletedDirectMessage(DirectMessage message) {
    }

    public void destroyedDirectMessage(DirectMessage message) {
    }

    public void gotFriendsIDs(IDs ids) {
    }

    public void gotFollowersIDs(IDs ids) {
    }

    public void created(User user) {
    }

    public void createdFriendship(User user) {
    }

    public void destroyed(User user) {
    }

    public void destroyedFriendship(User user) {
    }

    public void gotExists(boolean exists) {
    }

    public void gotExistsFriendship(boolean exists) {
    }

    public void updatedLocation(User user) {
    }

    public void updatedProfile(User user) {
    }

    public void updatedProfileColors(User user) {
    }

    public void gotRateLimitStatus(RateLimitStatus status) {
    }

    public void updatedDeliverlyDevice(User user) {
    }

    public void gotFavorites(List<Status> list) {
    }

    public void createdFavorite(Status status) {
    }

    public void destroyedFavorite(Status status) {
    }

    public void followed(User user) {
    }

    public void enabledNotification(User user) {
    }

    public void left(User user) {
    }

    public void disabledNotification(User user) {
    }

    public void blocked(User user) {
    }

    public void createdBlock(User user) {
    }

    public void unblocked(User user) {
    }

    public void destroyedBlock(User user) {
    }

    public void gotExistsBlock(boolean blockExists) {
    }

    public void gotBlockingUsers(List<User> list) {
    }

    public void gotBlockingUsersIDs(IDs blockingUsersIDs) {
    }

    public void tested(boolean test) {
    }

    public void gotDowntimeSchedule(String schedule) {
    }

    public void searched(QueryResult result) {
    }

    public void gotTrends(Trends trends) {
    }

    public void gotCurrentTrends(Trends trends) {
    }

    public void gotDailyTrends(List<Trends> list) {
    }

    public void gotWeeklyTrends(List<Trends> list) {
    }

    public void onException(TwitterException ex, int method) {
    }
}
