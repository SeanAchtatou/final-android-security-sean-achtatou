package twitter4j;

public class Version {
    private static final String TITLE = "Twitter4J";
    private static final String VERSION = "2.0.10";

    public static String getVersion() {
        return VERSION;
    }

    public static void main(String[] args) {
        System.out.println("Twitter4J 2.0.10");
    }
}
