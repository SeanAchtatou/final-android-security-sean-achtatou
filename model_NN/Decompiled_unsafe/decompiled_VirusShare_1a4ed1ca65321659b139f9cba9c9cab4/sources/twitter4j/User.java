package twitter4j;

import com.google.devtools.simple.runtime.annotations.DesignerProperty;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import twitter4j.http.Response;
import twitter4j.org.json.JSONException;
import twitter4j.org.json.JSONObject;

public class User extends TwitterResponse implements Serializable {
    static final String[] POSSIBLE_ROOT_NAMES = {"user", "sender", "recipient", "retweeting_user"};
    private static final long serialVersionUID = -6345893237975349030L;
    private Date createdAt;
    private String description;
    private int favouritesCount;
    private int followersCount;
    private boolean following;
    private int friendsCount;
    private boolean geoEnabled;
    private int id;
    private boolean isProtected;
    private String location;
    private String name;
    private boolean notificationEnabled;
    private String profileBackgroundColor;
    private String profileBackgroundImageUrl;
    private String profileBackgroundTile;
    private String profileImageUrl;
    private String profileLinkColor;
    private String profileSidebarBorderColor;
    private String profileSidebarFillColor;
    private String profileTextColor;
    private String screenName;
    private Date statusCreatedAt;
    private boolean statusFavorited = false;
    private long statusId = -1;
    private String statusInReplyToScreenName = null;
    private long statusInReplyToStatusId = -1;
    private int statusInReplyToUserId = -1;
    private String statusSource = null;
    private String statusText = null;
    private boolean statusTruncated = false;
    private int statusesCount;
    private String timeZone;
    private Twitter twitter;
    private String url;
    private int utcOffset;
    private boolean verified;

    User(Response res, Twitter twitter2) throws TwitterException {
        super(res);
        init(res.asDocument().getDocumentElement(), twitter2);
    }

    User(Response res, Element elem, Twitter twitter2) throws TwitterException {
        super(res);
        init(elem, twitter2);
    }

    User(JSONObject json) throws TwitterException {
        init(json);
    }

    private void init(JSONObject json) throws TwitterException {
        try {
            this.id = json.getInt("id");
            this.name = json.getString("name");
            this.screenName = json.getString("screen_name");
            this.location = json.getString("location");
            this.description = json.getString("description");
            this.profileImageUrl = json.getString("profile_image_url");
            this.url = json.getString("url");
            this.isProtected = json.getBoolean("protected");
            this.followersCount = json.getInt("followers_count");
            this.profileBackgroundColor = json.getString("profile_background_color");
            this.profileTextColor = json.getString("profile_text_color");
            this.profileLinkColor = json.getString("profile_link_color");
            this.profileSidebarFillColor = json.getString("profile_sidebar_fill_color");
            this.profileSidebarBorderColor = json.getString("profile_sidebar_border_color");
            this.friendsCount = json.getInt("friends_count");
            this.createdAt = parseDate(json.getString("created_at"), "EEE MMM dd HH:mm:ss z yyyy");
            this.favouritesCount = json.getInt("favourites_count");
            this.utcOffset = getInt("utc_offset", json);
            this.timeZone = json.getString("time_zone");
            this.profileBackgroundImageUrl = json.getString("profile_background_image_url");
            this.profileBackgroundTile = json.getString("profile_background_tile");
            this.following = getBoolean("following", json);
            this.notificationEnabled = getBoolean("notifications", json);
            this.statusesCount = json.getInt("statuses_count");
            if (!json.isNull("status")) {
                JSONObject status = json.getJSONObject("status");
                this.statusCreatedAt = parseDate(status.getString("created_at"), "EEE MMM dd HH:mm:ss z yyyy");
                this.statusId = status.getLong("id");
                this.statusText = status.getString(DesignerProperty.PROPERTY_TYPE_TEXT);
                this.statusSource = status.getString("source");
                this.statusTruncated = status.getBoolean("truncated");
                this.statusInReplyToStatusId = status.getLong("in_reply_to_status_id");
                this.statusInReplyToUserId = status.getInt("in_reply_to_user_id");
                this.statusFavorited = status.getBoolean("favorited");
                this.statusInReplyToScreenName = status.getString("in_reply_to_screen_name");
            }
        } catch (JSONException jsone) {
            throw new TwitterException(new StringBuffer().append(jsone.getMessage()).append(":").append(json.toString()).toString(), jsone);
        }
    }

    private void init(Element elem, Twitter twitter2) throws TwitterException {
        this.twitter = twitter2;
        ensureRootNodeNameIs(POSSIBLE_ROOT_NAMES, elem);
        this.id = getChildInt("id", elem);
        this.name = getChildText("name", elem);
        this.screenName = getChildText("screen_name", elem);
        this.location = getChildText("location", elem);
        this.description = getChildText("description", elem);
        this.profileImageUrl = getChildText("profile_image_url", elem);
        this.url = getChildText("url", elem);
        this.isProtected = getChildBoolean("protected", elem);
        this.followersCount = getChildInt("followers_count", elem);
        this.profileBackgroundColor = getChildText("profile_background_color", elem);
        this.profileTextColor = getChildText("profile_text_color", elem);
        this.profileLinkColor = getChildText("profile_link_color", elem);
        this.profileSidebarFillColor = getChildText("profile_sidebar_fill_color", elem);
        this.profileSidebarBorderColor = getChildText("profile_sidebar_border_color", elem);
        this.friendsCount = getChildInt("friends_count", elem);
        this.createdAt = getChildDate("created_at", elem);
        this.favouritesCount = getChildInt("favourites_count", elem);
        this.utcOffset = getChildInt("utc_offset", elem);
        this.timeZone = getChildText("time_zone", elem);
        this.profileBackgroundImageUrl = getChildText("profile_background_image_url", elem);
        this.profileBackgroundTile = getChildText("profile_background_tile", elem);
        this.following = getChildBoolean("following", elem);
        this.notificationEnabled = getChildBoolean("notifications", elem);
        this.statusesCount = getChildInt("statuses_count", elem);
        this.geoEnabled = getChildBoolean("geo_enabled", elem);
        this.verified = getChildBoolean("verified", elem);
        NodeList statuses = elem.getElementsByTagName("status");
        if (statuses.getLength() != 0) {
            Element status = (Element) statuses.item(0);
            this.statusCreatedAt = getChildDate("created_at", status);
            this.statusId = getChildLong("id", status);
            this.statusText = getChildText(DesignerProperty.PROPERTY_TYPE_TEXT, status);
            this.statusSource = getChildText("source", status);
            this.statusTruncated = getChildBoolean("truncated", status);
            this.statusInReplyToStatusId = getChildLong("in_reply_to_status_id", status);
            this.statusInReplyToUserId = getChildInt("in_reply_to_user_id", status);
            this.statusFavorited = getChildBoolean("favorited", status);
            this.statusInReplyToScreenName = getChildText("in_reply_to_screen_name", status);
        }
    }

    public int getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getScreenName() {
        return this.screenName;
    }

    public String getLocation() {
        return this.location;
    }

    public String getDescription() {
        return this.description;
    }

    public URL getProfileImageURL() {
        try {
            return new URL(this.profileImageUrl);
        } catch (MalformedURLException e) {
            return null;
        }
    }

    public URL getURL() {
        try {
            return new URL(this.url);
        } catch (MalformedURLException e) {
            return null;
        }
    }

    public boolean isProtected() {
        return this.isProtected;
    }

    public int getFollowersCount() {
        return this.followersCount;
    }

    public DirectMessage sendDirectMessage(String text) throws TwitterException {
        return this.twitter.sendDirectMessage(getName(), text);
    }

    public static List<User> constructUsers(Response res, Twitter twitter2) throws TwitterException {
        Document doc = res.asDocument();
        if (isRootNodeNilClasses(doc)) {
            return new ArrayList(0);
        }
        try {
            ensureRootNodeNameIs("users", doc);
            NodeList list = doc.getDocumentElement().getElementsByTagName("user");
            int size = list.getLength();
            List<User> users = new ArrayList<>(size);
            for (int i = 0; i < size; i++) {
                users.add(new User(res, (Element) list.item(i), twitter2));
            }
            return users;
        } catch (TwitterException te) {
            if (isRootNodeNilClasses(doc)) {
                return new ArrayList(0);
            }
            throw te;
        }
    }

    public Date getStatusCreatedAt() {
        return this.statusCreatedAt;
    }

    public long getStatusId() {
        return this.statusId;
    }

    public String getStatusText() {
        return this.statusText;
    }

    public String getStatusSource() {
        return this.statusSource;
    }

    public boolean isStatusTruncated() {
        return this.statusTruncated;
    }

    public long getStatusInReplyToStatusId() {
        return this.statusInReplyToStatusId;
    }

    public int getStatusInReplyToUserId() {
        return this.statusInReplyToUserId;
    }

    public boolean isStatusFavorited() {
        return this.statusFavorited;
    }

    public String getStatusInReplyToScreenName() {
        if (-1 != this.statusInReplyToUserId) {
            return this.statusInReplyToScreenName;
        }
        return null;
    }

    public String getProfileBackgroundColor() {
        return this.profileBackgroundColor;
    }

    public String getProfileTextColor() {
        return this.profileTextColor;
    }

    public String getProfileLinkColor() {
        return this.profileLinkColor;
    }

    public String getProfileSidebarFillColor() {
        return this.profileSidebarFillColor;
    }

    public String getProfileSidebarBorderColor() {
        return this.profileSidebarBorderColor;
    }

    public int getFriendsCount() {
        return this.friendsCount;
    }

    public Date getCreatedAt() {
        return this.createdAt;
    }

    public int getFavouritesCount() {
        return this.favouritesCount;
    }

    public int getUtcOffset() {
        return this.utcOffset;
    }

    public String getTimeZone() {
        return this.timeZone;
    }

    public String getProfileBackgroundImageUrl() {
        return this.profileBackgroundImageUrl;
    }

    public String getProfileBackgroundTile() {
        return this.profileBackgroundTile;
    }

    public boolean isFollowing() {
        return this.following;
    }

    public boolean isNotifications() {
        return this.notificationEnabled;
    }

    public boolean isNotificationEnabled() {
        return this.notificationEnabled;
    }

    public int getStatusesCount() {
        return this.statusesCount;
    }

    public boolean isGeoEnabled() {
        return this.geoEnabled;
    }

    public boolean isVerified() {
        return this.verified;
    }

    public int hashCode() {
        return this.id;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof User) || ((User) obj).id != this.id) {
            return false;
        }
        return true;
    }

    public String toString() {
        return new StringBuffer().append("User{twitter=").append(this.twitter).append(", id=").append(this.id).append(", name='").append(this.name).append('\'').append(", screenName='").append(this.screenName).append('\'').append(", location='").append(this.location).append('\'').append(", description='").append(this.description).append('\'').append(", profileImageUrl='").append(this.profileImageUrl).append('\'').append(", url='").append(this.url).append('\'').append(", isProtected=").append(this.isProtected).append(", followersCount=").append(this.followersCount).append(", statusCreatedAt=").append(this.statusCreatedAt).append(", statusId=").append(this.statusId).append(", statusText='").append(this.statusText).append('\'').append(", statusSource='").append(this.statusSource).append('\'').append(", statusTruncated=").append(this.statusTruncated).append(", statusInReplyToStatusId=").append(this.statusInReplyToStatusId).append(", statusInReplyToUserId=").append(this.statusInReplyToUserId).append(", statusFavorited=").append(this.statusFavorited).append(", statusInReplyToScreenName='").append(this.statusInReplyToScreenName).append('\'').append(", profileBackgroundColor='").append(this.profileBackgroundColor).append('\'').append(", profileTextColor='").append(this.profileTextColor).append('\'').append(", profileLinkColor='").append(this.profileLinkColor).append('\'').append(", profileSidebarFillColor='").append(this.profileSidebarFillColor).append('\'').append(", profileSidebarBorderColor='").append(this.profileSidebarBorderColor).append('\'').append(", friendsCount=").append(this.friendsCount).append(", createdAt=").append(this.createdAt).append(", favouritesCount=").append(this.favouritesCount).append(", utcOffset=").append(this.utcOffset).append(", timeZone='").append(this.timeZone).append('\'').append(", profileBackgroundImageUrl='").append(this.profileBackgroundImageUrl).append('\'').append(", profileBackgroundTile='").append(this.profileBackgroundTile).append('\'').append(", following=").append(this.following).append(", notificationEnabled=").append(this.notificationEnabled).append(", statusesCount=").append(this.statusesCount).append(", geoEnabled=").append(this.geoEnabled).append(", verified=").append(this.verified).append('}').toString();
    }
}
