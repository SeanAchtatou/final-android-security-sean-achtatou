package twitter4j.org.json;

import com.google.devtools.simple.runtime.components.util.ErrorMessages;
import gnu.kawa.xml.ElementType;
import java.io.IOException;
import java.io.Writer;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeSet;

public class JSONObject {
    public static final Object NULL = new Null(null);
    static Class class$java$lang$Boolean;
    static Class class$java$lang$Byte;
    static Class class$java$lang$Character;
    static Class class$java$lang$Double;
    static Class class$java$lang$Float;
    static Class class$java$lang$Integer;
    static Class class$java$lang$Long;
    static Class class$java$lang$Short;
    static Class class$java$lang$String;
    private Map map;

    /* renamed from: twitter4j.org.json.JSONObject$1  reason: invalid class name */
    static class AnonymousClass1 {
    }

    private static final class Null {
        private Null() {
        }

        Null(AnonymousClass1 x0) {
            this();
        }

        /* access modifiers changed from: protected */
        public final Object clone() {
            return this;
        }

        public boolean equals(Object object) {
            return object == null || object == this;
        }

        public String toString() {
            return "null";
        }
    }

    public JSONObject() {
        this.map = new HashMap();
    }

    public JSONObject(JSONObject jo, String[] names) throws JSONException {
        this();
        for (int i = 0; i < names.length; i++) {
            putOnce(names[i], jo.opt(names[i]));
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x004f A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0069 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001c  */
    public JSONObject(twitter4j.org.json.JSONTokener r5) throws twitter4j.org.json.JSONException {
        /*
            r4 = this;
            r4.<init>()
            char r2 = r5.nextClean()
            r3 = 123(0x7b, float:1.72E-43)
            if (r2 == r3) goto L_0x0015
            java.lang.String r2 = "A JSONObject text must begin with '{'"
            twitter4j.org.json.JSONException r2 = r5.syntaxError(r2)
            throw r2
        L_0x0012:
            r5.back()
        L_0x0015:
            char r0 = r5.nextClean()
            switch(r0) {
                case 0: goto L_0x004f;
                case 125: goto L_0x0069;
                default: goto L_0x001c;
            }
        L_0x001c:
            r5.back()
            java.lang.Object r2 = r5.nextValue()
            java.lang.String r1 = r2.toString()
            char r0 = r5.nextClean()
            r2 = 61
            if (r0 != r2) goto L_0x0056
            char r2 = r5.next()
            r3 = 62
            if (r2 == r3) goto L_0x003a
            r5.back()
        L_0x003a:
            java.lang.Object r2 = r5.nextValue()
            r4.putOnce(r1, r2)
            char r2 = r5.nextClean()
            switch(r2) {
                case 44: goto L_0x0061;
                case 59: goto L_0x0061;
                case 125: goto L_0x0069;
                default: goto L_0x0048;
            }
        L_0x0048:
            java.lang.String r2 = "Expected a ',' or '}'"
            twitter4j.org.json.JSONException r2 = r5.syntaxError(r2)
            throw r2
        L_0x004f:
            java.lang.String r2 = "A JSONObject text must end with '}'"
            twitter4j.org.json.JSONException r2 = r5.syntaxError(r2)
            throw r2
        L_0x0056:
            r2 = 58
            if (r0 == r2) goto L_0x003a
            java.lang.String r2 = "Expected a ':' after a key"
            twitter4j.org.json.JSONException r2 = r5.syntaxError(r2)
            throw r2
        L_0x0061:
            char r2 = r5.nextClean()
            r3 = 125(0x7d, float:1.75E-43)
            if (r2 != r3) goto L_0x0012
        L_0x0069:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: twitter4j.org.json.JSONObject.<init>(twitter4j.org.json.JSONTokener):void");
    }

    public JSONObject(Map map2) {
        this.map = map2 == null ? new HashMap() : map2;
    }

    public JSONObject(Map map2, boolean includeSuperClass) {
        this.map = new HashMap();
        if (map2 != null) {
            for (Map.Entry e : map2.entrySet()) {
                this.map.put(e.getKey(), new JSONObject(e.getValue(), includeSuperClass));
            }
        }
    }

    public JSONObject(Object bean) {
        this();
        populateInternalMap(bean, false);
    }

    public JSONObject(Object bean, boolean includeSuperClass) {
        this();
        populateInternalMap(bean, includeSuperClass);
    }

    private void populateInternalMap(Object bean, boolean includeSuperClass) {
        Class klass = bean.getClass();
        if (klass.getClassLoader() == null) {
            includeSuperClass = false;
        }
        Method[] methods = includeSuperClass ? klass.getMethods() : klass.getDeclaredMethods();
        int i = 0;
        while (i < methods.length) {
            try {
                Method method = methods[i];
                String name = method.getName();
                String key = ElementType.MATCH_ANY_LOCALNAME;
                if (name.startsWith("get")) {
                    key = name.substring(3);
                } else if (name.startsWith("is")) {
                    key = name.substring(2);
                }
                if (key.length() > 0 && Character.isUpperCase(key.charAt(0)) && method.getParameterTypes().length == 0) {
                    if (key.length() == 1) {
                        key = key.toLowerCase();
                    } else if (!Character.isUpperCase(key.charAt(1))) {
                        key = new StringBuffer().append(key.substring(0, 1).toLowerCase()).append(key.substring(1)).toString();
                    }
                    Object result = method.invoke(bean, null);
                    if (result == null) {
                        this.map.put(key, NULL);
                    } else if (result.getClass().isArray()) {
                        this.map.put(key, new JSONArray(result, includeSuperClass));
                    } else if (result instanceof Collection) {
                        this.map.put(key, new JSONArray((Collection) result, includeSuperClass));
                    } else if (result instanceof Map) {
                        this.map.put(key, new JSONObject((Map) result, includeSuperClass));
                    } else if (isStandardProperty(result.getClass())) {
                        this.map.put(key, result);
                    } else if (result.getClass().getPackage().getName().startsWith("java") || result.getClass().getClassLoader() == null) {
                        this.map.put(key, result.toString());
                    } else {
                        this.map.put(key, new JSONObject(result, includeSuperClass));
                    }
                }
                i++;
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    private boolean isStandardProperty(Class clazz) {
        Class cls;
        Class cls2;
        Class cls3;
        Class cls4;
        Class cls5;
        Class cls6;
        Class cls7;
        Class cls8;
        Class cls9;
        if (!clazz.isPrimitive()) {
            if (class$java$lang$Byte == null) {
                cls = class$("java.lang.Byte");
                class$java$lang$Byte = cls;
            } else {
                cls = class$java$lang$Byte;
            }
            if (!clazz.isAssignableFrom(cls)) {
                if (class$java$lang$Short == null) {
                    cls2 = class$("java.lang.Short");
                    class$java$lang$Short = cls2;
                } else {
                    cls2 = class$java$lang$Short;
                }
                if (!clazz.isAssignableFrom(cls2)) {
                    if (class$java$lang$Integer == null) {
                        cls3 = class$("java.lang.Integer");
                        class$java$lang$Integer = cls3;
                    } else {
                        cls3 = class$java$lang$Integer;
                    }
                    if (!clazz.isAssignableFrom(cls3)) {
                        if (class$java$lang$Long == null) {
                            cls4 = class$("java.lang.Long");
                            class$java$lang$Long = cls4;
                        } else {
                            cls4 = class$java$lang$Long;
                        }
                        if (!clazz.isAssignableFrom(cls4)) {
                            if (class$java$lang$Float == null) {
                                cls5 = class$("java.lang.Float");
                                class$java$lang$Float = cls5;
                            } else {
                                cls5 = class$java$lang$Float;
                            }
                            if (!clazz.isAssignableFrom(cls5)) {
                                if (class$java$lang$Double == null) {
                                    cls6 = class$("java.lang.Double");
                                    class$java$lang$Double = cls6;
                                } else {
                                    cls6 = class$java$lang$Double;
                                }
                                if (!clazz.isAssignableFrom(cls6)) {
                                    if (class$java$lang$Character == null) {
                                        cls7 = class$("java.lang.Character");
                                        class$java$lang$Character = cls7;
                                    } else {
                                        cls7 = class$java$lang$Character;
                                    }
                                    if (!clazz.isAssignableFrom(cls7)) {
                                        if (class$java$lang$String == null) {
                                            cls8 = class$("java.lang.String");
                                            class$java$lang$String = cls8;
                                        } else {
                                            cls8 = class$java$lang$String;
                                        }
                                        if (!clazz.isAssignableFrom(cls8)) {
                                            if (class$java$lang$Boolean == null) {
                                                cls9 = class$("java.lang.Boolean");
                                                class$java$lang$Boolean = cls9;
                                            } else {
                                                cls9 = class$java$lang$Boolean;
                                            }
                                            return clazz.isAssignableFrom(cls9);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    static Class class$(String x0) {
        try {
            return Class.forName(x0);
        } catch (ClassNotFoundException x1) {
            throw new NoClassDefFoundError().initCause(x1);
        }
    }

    public JSONObject(Object object, String[] names) {
        this();
        Class c = object.getClass();
        for (String name : names) {
            try {
                putOpt(name, c.getField(name).get(object));
            } catch (Exception e) {
            }
        }
    }

    public JSONObject(String source) throws JSONException {
        this(new JSONTokener(source));
    }

    public JSONObject accumulate(String key, Object value) throws JSONException {
        testValidity(value);
        Object o = opt(key);
        if (o == null) {
            if (value instanceof JSONArray) {
                value = new JSONArray().put(value);
            }
            put(key, value);
        } else if (o instanceof JSONArray) {
            ((JSONArray) o).put(value);
        } else {
            put(key, new JSONArray().put(o).put(value));
        }
        return this;
    }

    public JSONObject append(String key, Object value) throws JSONException {
        testValidity(value);
        Object o = opt(key);
        if (o == null) {
            put(key, new JSONArray().put(value));
        } else if (o instanceof JSONArray) {
            put(key, ((JSONArray) o).put(value));
        } else {
            throw new JSONException(new StringBuffer().append("JSONObject[").append(key).append("] is not a JSONArray.").toString());
        }
        return this;
    }

    public static String doubleToString(double d) {
        if (Double.isInfinite(d) || Double.isNaN(d)) {
            return "null";
        }
        String s = Double.toString(d);
        if (s.indexOf(46) <= 0 || s.indexOf((int) ErrorMessages.ERROR_LOCATION_SENSOR_LATITUDE_NOT_FOUND) >= 0 || s.indexOf(69) >= 0) {
            return s;
        }
        while (s.endsWith("0")) {
            s = s.substring(0, s.length() - 1);
        }
        if (s.endsWith(".")) {
            return s.substring(0, s.length() - 1);
        }
        return s;
    }

    public Object get(String key) throws JSONException {
        Object o = opt(key);
        if (o != null) {
            return o;
        }
        throw new JSONException(new StringBuffer().append("JSONObject[").append(quote(key)).append("] not found.").toString());
    }

    public boolean getBoolean(String key) throws JSONException {
        Object o = get(key);
        if (o.equals(Boolean.FALSE) || ((o instanceof String) && ((String) o).equalsIgnoreCase("false"))) {
            return false;
        }
        if (o.equals(Boolean.TRUE) || ((o instanceof String) && ((String) o).equalsIgnoreCase("true"))) {
            return true;
        }
        throw new JSONException(new StringBuffer().append("JSONObject[").append(quote(key)).append("] is not a Boolean.").toString());
    }

    public double getDouble(String key) throws JSONException {
        Object o = get(key);
        try {
            return o instanceof Number ? ((Number) o).doubleValue() : Double.valueOf((String) o).doubleValue();
        } catch (Exception e) {
            throw new JSONException(new StringBuffer().append("JSONObject[").append(quote(key)).append("] is not a number.").toString());
        }
    }

    public int getInt(String key) throws JSONException {
        Object o = get(key);
        return o instanceof Number ? ((Number) o).intValue() : (int) getDouble(key);
    }

    public JSONArray getJSONArray(String key) throws JSONException {
        Object o = get(key);
        if (o instanceof JSONArray) {
            return (JSONArray) o;
        }
        throw new JSONException(new StringBuffer().append("JSONObject[").append(quote(key)).append("] is not a JSONArray.").toString());
    }

    public JSONObject getJSONObject(String key) throws JSONException {
        Object o = get(key);
        if (o instanceof JSONObject) {
            return (JSONObject) o;
        }
        throw new JSONException(new StringBuffer().append("JSONObject[").append(quote(key)).append("] is not a JSONObject.").toString());
    }

    public long getLong(String key) throws JSONException {
        Object o = get(key);
        return o instanceof Number ? ((Number) o).longValue() : (long) getDouble(key);
    }

    public static String[] getNames(JSONObject jo) {
        int length = jo.length();
        if (length == 0) {
            return null;
        }
        Iterator i = jo.keys();
        String[] names = new String[length];
        int j = 0;
        while (i.hasNext()) {
            names[j] = (String) i.next();
            j++;
        }
        return names;
    }

    public static String[] getNames(Object object) {
        Field[] fields;
        int length;
        String[] names = null;
        if (!(object == null || (length = (fields = object.getClass().getFields()).length) == 0)) {
            names = new String[length];
            for (int i = 0; i < length; i++) {
                names[i] = fields[i].getName();
            }
        }
        return names;
    }

    public String getString(String key) throws JSONException {
        return get(key).toString();
    }

    public boolean has(String key) {
        return this.map.containsKey(key);
    }

    public boolean isNull(String key) {
        return NULL.equals(opt(key));
    }

    public Iterator keys() {
        return this.map.keySet().iterator();
    }

    public int length() {
        return this.map.size();
    }

    public JSONArray names() {
        JSONArray ja = new JSONArray();
        Iterator keys = keys();
        while (keys.hasNext()) {
            ja.put(keys.next());
        }
        if (ja.length() == 0) {
            return null;
        }
        return ja;
    }

    public static String numberToString(Number n) throws JSONException {
        if (n == null) {
            throw new JSONException("Null pointer");
        }
        testValidity(n);
        String s = n.toString();
        if (s.indexOf(46) <= 0 || s.indexOf((int) ErrorMessages.ERROR_LOCATION_SENSOR_LATITUDE_NOT_FOUND) >= 0 || s.indexOf(69) >= 0) {
            return s;
        }
        while (s.endsWith("0")) {
            s = s.substring(0, s.length() - 1);
        }
        if (s.endsWith(".")) {
            return s.substring(0, s.length() - 1);
        }
        return s;
    }

    public Object opt(String key) {
        if (key == null) {
            return null;
        }
        return this.map.get(key);
    }

    public boolean optBoolean(String key) {
        return optBoolean(key, false);
    }

    public boolean optBoolean(String key, boolean defaultValue) {
        try {
            return getBoolean(key);
        } catch (Exception e) {
            return defaultValue;
        }
    }

    public JSONObject put(String key, Collection value) throws JSONException {
        put(key, new JSONArray(value));
        return this;
    }

    public double optDouble(String key) {
        return optDouble(key, Double.NaN);
    }

    public double optDouble(String key, double defaultValue) {
        try {
            Object o = opt(key);
            return o instanceof Number ? ((Number) o).doubleValue() : new Double((String) o).doubleValue();
        } catch (Exception e) {
            return defaultValue;
        }
    }

    public int optInt(String key) {
        return optInt(key, 0);
    }

    public int optInt(String key, int defaultValue) {
        try {
            return getInt(key);
        } catch (Exception e) {
            return defaultValue;
        }
    }

    public JSONArray optJSONArray(String key) {
        Object o = opt(key);
        if (o instanceof JSONArray) {
            return (JSONArray) o;
        }
        return null;
    }

    public JSONObject optJSONObject(String key) {
        Object o = opt(key);
        if (o instanceof JSONObject) {
            return (JSONObject) o;
        }
        return null;
    }

    public long optLong(String key) {
        return optLong(key, 0);
    }

    public long optLong(String key, long defaultValue) {
        try {
            return getLong(key);
        } catch (Exception e) {
            return defaultValue;
        }
    }

    public String optString(String key) {
        return optString(key, ElementType.MATCH_ANY_LOCALNAME);
    }

    public String optString(String key, String defaultValue) {
        Object o = opt(key);
        return o != null ? o.toString() : defaultValue;
    }

    public JSONObject put(String key, boolean value) throws JSONException {
        put(key, value ? Boolean.TRUE : Boolean.FALSE);
        return this;
    }

    public JSONObject put(String key, double value) throws JSONException {
        put(key, new Double(value));
        return this;
    }

    public JSONObject put(String key, int value) throws JSONException {
        put(key, new Integer(value));
        return this;
    }

    public JSONObject put(String key, long value) throws JSONException {
        put(key, new Long(value));
        return this;
    }

    public JSONObject put(String key, Map value) throws JSONException {
        put(key, new JSONObject(value));
        return this;
    }

    public JSONObject put(String key, Object value) throws JSONException {
        if (key == null) {
            throw new JSONException("Null key.");
        }
        if (value != null) {
            testValidity(value);
            this.map.put(key, value);
        } else {
            remove(key);
        }
        return this;
    }

    public JSONObject putOnce(String key, Object value) throws JSONException {
        if (!(key == null || value == null)) {
            if (opt(key) != null) {
                throw new JSONException(new StringBuffer().append("Duplicate key \"").append(key).append("\"").toString());
            }
            put(key, value);
        }
        return this;
    }

    public JSONObject putOpt(String key, Object value) throws JSONException {
        if (!(key == null || value == null)) {
            put(key, value);
        }
        return this;
    }

    public static String quote(String string) {
        if (string == null || string.length() == 0) {
            return "\"\"";
        }
        char c = 0;
        int len = string.length();
        StringBuffer sb = new StringBuffer(len + 4);
        sb.append('\"');
        for (int i = 0; i < len; i++) {
            char b = c;
            c = string.charAt(i);
            switch (c) {
                case 8:
                    sb.append("\\b");
                    break;
                case 9:
                    sb.append("\\t");
                    break;
                case 10:
                    sb.append("\\n");
                    break;
                case 12:
                    sb.append("\\f");
                    break;
                case 13:
                    sb.append("\\r");
                    break;
                case '\"':
                case '\\':
                    sb.append('\\');
                    sb.append(c);
                    break;
                case '/':
                    if (b == '<') {
                        sb.append('\\');
                    }
                    sb.append(c);
                    break;
                default:
                    if (c >= ' ' && ((c < 128 || c >= 160) && (c < 8192 || c >= 8448))) {
                        sb.append(c);
                        break;
                    } else {
                        String t = new StringBuffer().append("000").append(Integer.toHexString(c)).toString();
                        sb.append(new StringBuffer().append("\\u").append(t.substring(t.length() - 4)).toString());
                        break;
                    }
            }
        }
        sb.append('\"');
        return sb.toString();
    }

    public Object remove(String key) {
        return this.map.remove(key);
    }

    public Iterator sortedKeys() {
        return new TreeSet(this.map.keySet()).iterator();
    }

    public static Object stringToValue(String s) {
        if (s.equals(ElementType.MATCH_ANY_LOCALNAME)) {
            return s;
        }
        if (s.equalsIgnoreCase("true")) {
            return Boolean.TRUE;
        }
        if (s.equalsIgnoreCase("false")) {
            return Boolean.FALSE;
        }
        if (s.equalsIgnoreCase("null")) {
            return NULL;
        }
        char b = s.charAt(0);
        if ((b < '0' || b > '9') && b != '.' && b != '-' && b != '+') {
            return s;
        }
        if (b == '0') {
            if (s.length() <= 2 || !(s.charAt(1) == 'x' || s.charAt(1) == 'X')) {
                try {
                    return new Integer(Integer.parseInt(s, 8));
                } catch (Exception e) {
                }
            } else {
                try {
                    return new Integer(Integer.parseInt(s.substring(2), 16));
                } catch (Exception e2) {
                }
            }
        }
        try {
            return new Integer(s);
        } catch (Exception e3) {
            try {
                return new Long(s);
            } catch (Exception e4) {
                try {
                    return new Double(s);
                } catch (Exception e5) {
                    return s;
                }
            }
        }
    }

    static void testValidity(Object o) throws JSONException {
        if (o == null) {
            return;
        }
        if (o instanceof Double) {
            if (((Double) o).isInfinite() || ((Double) o).isNaN()) {
                throw new JSONException("JSON does not allow non-finite numbers.");
            }
        } else if (!(o instanceof Float)) {
        } else {
            if (((Float) o).isInfinite() || ((Float) o).isNaN()) {
                throw new JSONException("JSON does not allow non-finite numbers.");
            }
        }
    }

    public JSONArray toJSONArray(JSONArray names) throws JSONException {
        if (names == null || names.length() == 0) {
            return null;
        }
        JSONArray ja = new JSONArray();
        for (int i = 0; i < names.length(); i++) {
            ja.put(opt(names.getString(i)));
        }
        return ja;
    }

    public String toString() {
        try {
            Iterator keys = keys();
            StringBuffer sb = new StringBuffer("{");
            while (keys.hasNext()) {
                if (sb.length() > 1) {
                    sb.append(',');
                }
                Object o = keys.next();
                sb.append(quote(o.toString()));
                sb.append(':');
                sb.append(valueToString(this.map.get(o)));
            }
            sb.append('}');
            return sb.toString();
        } catch (Exception e) {
            return null;
        }
    }

    public String toString(int indentFactor) throws JSONException {
        return toString(indentFactor, 0);
    }

    /* access modifiers changed from: package-private */
    public String toString(int indentFactor, int indent) throws JSONException {
        int n = length();
        if (n == 0) {
            return "{}";
        }
        Iterator keys = sortedKeys();
        StringBuffer sb = new StringBuffer("{");
        int newindent = indent + indentFactor;
        if (n == 1) {
            Object o = keys.next();
            sb.append(quote(o.toString()));
            sb.append(": ");
            sb.append(valueToString(this.map.get(o), indentFactor, indent));
        } else {
            while (keys.hasNext()) {
                Object o2 = keys.next();
                if (sb.length() > 1) {
                    sb.append(",\n");
                } else {
                    sb.append(10);
                }
                for (int j = 0; j < newindent; j++) {
                    sb.append(' ');
                }
                sb.append(quote(o2.toString()));
                sb.append(": ");
                sb.append(valueToString(this.map.get(o2), indentFactor, newindent));
            }
            if (sb.length() > 1) {
                sb.append(10);
                for (int j2 = 0; j2 < indent; j2++) {
                    sb.append(' ');
                }
            }
        }
        sb.append('}');
        return sb.toString();
    }

    static String valueToString(Object value) throws JSONException {
        if (value == null || value.equals(null)) {
            return "null";
        }
        if (value instanceof JSONString) {
            try {
                String o = ((JSONString) value).toJSONString();
                if (o instanceof String) {
                    return o;
                }
                throw new JSONException(new StringBuffer().append("Bad value from toJSONString: ").append((Object) o).toString());
            } catch (Exception e) {
                throw new JSONException(e);
            }
        } else if (value instanceof Number) {
            return numberToString((Number) value);
        } else {
            if ((value instanceof Boolean) || (value instanceof JSONObject) || (value instanceof JSONArray)) {
                return value.toString();
            }
            if (value instanceof Map) {
                return new JSONObject((Map) value).toString();
            }
            if (value instanceof Collection) {
                return new JSONArray((Collection) value).toString();
            }
            if (value.getClass().isArray()) {
                return new JSONArray(value).toString();
            }
            return quote(value.toString());
        }
    }

    static String valueToString(Object value, int indentFactor, int indent) throws JSONException {
        if (value == null || value.equals(null)) {
            return "null";
        }
        try {
            if (value instanceof JSONString) {
                String o = ((JSONString) value).toJSONString();
                if (o instanceof String) {
                    return o;
                }
            }
        } catch (Exception e) {
        }
        if (value instanceof Number) {
            return numberToString((Number) value);
        }
        if (value instanceof Boolean) {
            return value.toString();
        }
        if (value instanceof JSONObject) {
            return ((JSONObject) value).toString(indentFactor, indent);
        }
        if (value instanceof JSONArray) {
            return ((JSONArray) value).toString(indentFactor, indent);
        }
        if (value instanceof Map) {
            return new JSONObject((Map) value).toString(indentFactor, indent);
        }
        if (value instanceof Collection) {
            return new JSONArray((Collection) value).toString(indentFactor, indent);
        }
        if (value.getClass().isArray()) {
            return new JSONArray(value).toString(indentFactor, indent);
        }
        return quote(value.toString());
    }

    public Writer write(Writer writer) throws JSONException {
        boolean b = false;
        try {
            Iterator keys = keys();
            writer.write(123);
            while (keys.hasNext()) {
                if (b) {
                    writer.write(44);
                }
                Object k = keys.next();
                writer.write(quote(k.toString()));
                writer.write(58);
                Object v = this.map.get(k);
                if (v instanceof JSONObject) {
                    ((JSONObject) v).write(writer);
                } else if (v instanceof JSONArray) {
                    ((JSONArray) v).write(writer);
                } else {
                    writer.write(valueToString(v));
                }
                b = true;
            }
            writer.write(125);
            return writer;
        } catch (IOException e) {
            throw new JSONException(e);
        }
    }
}
