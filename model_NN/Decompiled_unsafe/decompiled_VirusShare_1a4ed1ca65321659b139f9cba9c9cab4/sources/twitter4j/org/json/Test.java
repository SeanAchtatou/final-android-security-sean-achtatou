package twitter4j.org.json;

import com.google.devtools.simple.runtime.annotations.DesignerProperty;
import gnu.kawa.xml.ElementType;
import java.io.StringWriter;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

public class Test {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: twitter4j.org.json.JSONObject.put(java.lang.String, double):twitter4j.org.json.JSONObject
     arg types: [java.lang.String, int]
     candidates:
      twitter4j.org.json.JSONObject.put(java.lang.String, int):twitter4j.org.json.JSONObject
      twitter4j.org.json.JSONObject.put(java.lang.String, long):twitter4j.org.json.JSONObject
      twitter4j.org.json.JSONObject.put(java.lang.String, java.lang.Object):twitter4j.org.json.JSONObject
      twitter4j.org.json.JSONObject.put(java.lang.String, java.util.Collection):twitter4j.org.json.JSONObject
      twitter4j.org.json.JSONObject.put(java.lang.String, java.util.Map):twitter4j.org.json.JSONObject
      twitter4j.org.json.JSONObject.put(java.lang.String, boolean):twitter4j.org.json.JSONObject
      twitter4j.org.json.JSONObject.put(java.lang.String, double):twitter4j.org.json.JSONObject */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: twitter4j.org.json.JSONObject.put(java.lang.String, boolean):twitter4j.org.json.JSONObject
     arg types: [java.lang.String, int]
     candidates:
      twitter4j.org.json.JSONObject.put(java.lang.String, double):twitter4j.org.json.JSONObject
      twitter4j.org.json.JSONObject.put(java.lang.String, int):twitter4j.org.json.JSONObject
      twitter4j.org.json.JSONObject.put(java.lang.String, long):twitter4j.org.json.JSONObject
      twitter4j.org.json.JSONObject.put(java.lang.String, java.lang.Object):twitter4j.org.json.JSONObject
      twitter4j.org.json.JSONObject.put(java.lang.String, java.util.Collection):twitter4j.org.json.JSONObject
      twitter4j.org.json.JSONObject.put(java.lang.String, java.util.Map):twitter4j.org.json.JSONObject
      twitter4j.org.json.JSONObject.put(java.lang.String, boolean):twitter4j.org.json.JSONObject */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: twitter4j.org.json.JSONObject.put(java.lang.String, long):twitter4j.org.json.JSONObject
     arg types: [java.lang.String, int]
     candidates:
      twitter4j.org.json.JSONObject.put(java.lang.String, double):twitter4j.org.json.JSONObject
      twitter4j.org.json.JSONObject.put(java.lang.String, int):twitter4j.org.json.JSONObject
      twitter4j.org.json.JSONObject.put(java.lang.String, java.lang.Object):twitter4j.org.json.JSONObject
      twitter4j.org.json.JSONObject.put(java.lang.String, java.util.Collection):twitter4j.org.json.JSONObject
      twitter4j.org.json.JSONObject.put(java.lang.String, java.util.Map):twitter4j.org.json.JSONObject
      twitter4j.org.json.JSONObject.put(java.lang.String, boolean):twitter4j.org.json.JSONObject
      twitter4j.org.json.JSONObject.put(java.lang.String, long):twitter4j.org.json.JSONObject */
    public static void main(String[] args) {
        JSONArray a;
        JSONObject j;
        JSONObject j2;
        AnonymousClass1Obj r0 = new JSONString("A beany object", 42.0d, true) {
            public boolean aBoolean;
            public double aNumber;
            public String aString;

            {
                this.aString = string;
                this.aNumber = n;
                this.aBoolean = b;
            }

            public double getNumber() {
                return this.aNumber;
            }

            public String getString() {
                return this.aString;
            }

            public boolean isBoolean() {
                return this.aBoolean;
            }

            public String getBENT() {
                return "All uppercase key";
            }

            public String getX() {
                return "x";
            }

            public String toJSONString() {
                return new StringBuffer().append("{").append(JSONObject.quote(this.aString)).append(":").append(JSONObject.doubleToString(this.aNumber)).append("}").toString();
            }

            public String toString() {
                return new StringBuffer().append(getString()).append(" ").append(getNumber()).append(" ").append(isBoolean()).append(".").append(getBENT()).append(" ").append(getX()).toString();
            }
        };
        System.out.println(XML.toJSONObject("<![CDATA[This is a collection of test patterns and examples for org.json.]]>  Ignore the stuff past the end.  ").toString());
        JSONObject j3 = new JSONObject("{     \"list of lists\" : [         [1, 2, 3],         [4, 5, 6],     ] }");
        System.out.println(j3.toString(4));
        System.out.println(XML.toString(j3));
        System.out.println(XML.toJSONObject("<recipe name=\"bread\" prep_time=\"5 mins\" cook_time=\"3 hours\"> <title>Basic bread</title> <ingredient amount=\"8\" unit=\"dL\">Flour</ingredient> <ingredient amount=\"10\" unit=\"grams\">Yeast</ingredient> <ingredient amount=\"4\" unit=\"dL\" state=\"warm\">Water</ingredient> <ingredient amount=\"1\" unit=\"teaspoon\">Salt</ingredient> <instructions> <step>Mix all ingredients together.</step> <step>Knead thoroughly.</step> <step>Cover with a cloth, and leave for one hour in warm room.</step> <step>Knead again.</step> <step>Place in a bread baking tin.</step> <step>Cover with a cloth, and leave for one hour in warm room.</step> <step>Bake in the oven at 180(degrees)C for 30 minutes.</step> </instructions> </recipe> ").toString(4));
        System.out.println();
        JSONObject j4 = JSONML.toJSONObject("<recipe name=\"bread\" prep_time=\"5 mins\" cook_time=\"3 hours\"> <title>Basic bread</title> <ingredient amount=\"8\" unit=\"dL\">Flour</ingredient> <ingredient amount=\"10\" unit=\"grams\">Yeast</ingredient> <ingredient amount=\"4\" unit=\"dL\" state=\"warm\">Water</ingredient> <ingredient amount=\"1\" unit=\"teaspoon\">Salt</ingredient> <instructions> <step>Mix all ingredients together.</step> <step>Knead thoroughly.</step> <step>Cover with a cloth, and leave for one hour in warm room.</step> <step>Knead again.</step> <step>Place in a bread baking tin.</step> <step>Cover with a cloth, and leave for one hour in warm room.</step> <step>Bake in the oven at 180(degrees)C for 30 minutes.</step> </instructions> </recipe> ");
        System.out.println(j4.toString());
        System.out.println(JSONML.toString(j4));
        System.out.println();
        JSONArray a2 = JSONML.toJSONArray("<recipe name=\"bread\" prep_time=\"5 mins\" cook_time=\"3 hours\"> <title>Basic bread</title> <ingredient amount=\"8\" unit=\"dL\">Flour</ingredient> <ingredient amount=\"10\" unit=\"grams\">Yeast</ingredient> <ingredient amount=\"4\" unit=\"dL\" state=\"warm\">Water</ingredient> <ingredient amount=\"1\" unit=\"teaspoon\">Salt</ingredient> <instructions> <step>Mix all ingredients together.</step> <step>Knead thoroughly.</step> <step>Cover with a cloth, and leave for one hour in warm room.</step> <step>Knead again.</step> <step>Place in a bread baking tin.</step> <step>Cover with a cloth, and leave for one hour in warm room.</step> <step>Bake in the oven at 180(degrees)C for 30 minutes.</step> </instructions> </recipe> ");
        System.out.println(a2.toString(4));
        System.out.println(JSONML.toString(a2));
        System.out.println();
        JSONObject j5 = JSONML.toJSONObject("<div id=\"demo\" class=\"JSONML\"><p>JSONML is a transformation between <b>JSON</b> and <b>XML</b> that preserves ordering of document features.</p><p>JSONML can work with JSON arrays or JSON objects.</p><p>Three<br/>little<br/>words</p></div>");
        System.out.println(j5.toString(4));
        System.out.println(JSONML.toString(j5));
        System.out.println();
        JSONArray a3 = JSONML.toJSONArray("<div id=\"demo\" class=\"JSONML\"><p>JSONML is a transformation between <b>JSON</b> and <b>XML</b> that preserves ordering of document features.</p><p>JSONML can work with JSON arrays or JSON objects.</p><p>Three<br/>little<br/>words</p></div>");
        System.out.println(a3.toString(4));
        System.out.println(JSONML.toString(a3));
        System.out.println();
        System.out.println(XML.toJSONObject("<person created=\"2006-11-11T19:23\" modified=\"2006-12-31T23:59\">\n <firstName>Robert</firstName>\n <lastName>Smith</lastName>\n <address type=\"home\">\n <street>12345 Sixth Ave</street>\n <city>Anytown</city>\n <state>CA</state>\n <postalCode>98765-4321</postalCode>\n </address>\n </person>").toString(4));
        System.out.println(new JSONObject(r0).toString());
        System.out.println(new JSONObject("{ \"entity\": { \"imageURL\": \"\", \"name\": \"IXXXXXXXXXXXXX\", \"id\": 12336, \"ratingCount\": null, \"averageRating\": null } }").toString(2));
        System.out.println(new JSONStringer().object().key("single").value("MARIE HAA'S").key("Johnny").value("MARIE HAA\\'S").key("foo").value("bar").key("baz").array().object().key("quux").value("Thanks, Josh!").endObject().endArray().key("obj keys").value(JSONObject.getNames(r0)).endObject().toString());
        System.out.println(new JSONStringer().object().key("a").array().array().array().value("b").endArray().endArray().endArray().endObject().toString());
        JSONStringer jj = new JSONStringer();
        jj.array();
        jj.value(1L);
        jj.array();
        jj.value((Object) null);
        jj.array();
        jj.object();
        jj.key("empty-array").array().endArray();
        jj.key("answer").value(42L);
        jj.key("null").value((Object) null);
        jj.key("false").value(false);
        jj.key("true").value(true);
        jj.key("big").value(1.23456789E96d);
        jj.key("small").value(1.23456789E-80d);
        jj.key("empty-object").object().endObject();
        jj.key("long");
        jj.value(Long.MAX_VALUE);
        jj.endObject();
        jj.value("two");
        jj.endArray();
        jj.value(true);
        jj.endArray();
        jj.value(98.6d);
        jj.value(-100.0d);
        jj.object();
        jj.endObject();
        jj.object();
        jj.key("one");
        jj.value(1.0d);
        jj.endObject();
        jj.value(r0);
        jj.endArray();
        System.out.println(jj.toString());
        System.out.println(new JSONArray(jj.toString()).toString(4));
        System.out.println(new JSONArray(new int[]{1, 2, 3}).toString());
        JSONObject j6 = new JSONObject(r0, new String[]{"aString", "aNumber", "aBoolean"});
        j6.put("Testing JSONString interface", r0);
        System.out.println(j6.toString(4));
        JSONObject j7 = new JSONObject("{slashes: '///', closetag: '</script>', backslash:'\\\\', ei: {quotes: '\"\\''},eo: {a: '\"quoted\"', b:\"don't\"}, quotes: [\"'\", '\"']}");
        System.out.println(j7.toString(2));
        System.out.println(XML.toString(j7));
        System.out.println(ElementType.MATCH_ANY_LOCALNAME);
        JSONObject j8 = new JSONObject("{foo: [true, false,9876543210,    0.0, 1.00000001,  1.000000000001, 1.00000000000000001, .00000000000000001, 2.00, 0.1, 2e100, -32,[],{}, \"string\"],   to   : null, op : 'Good',ten:10} postfix comment");
        j8.put("String", "98.6");
        j8.put("JSONObject", new JSONObject());
        j8.put("JSONArray", new JSONArray());
        j8.put("int", 57);
        j8.put("double", 1.2345678901234568E29d);
        j8.put("true", true);
        j8.put("false", false);
        j8.put("null", JSONObject.NULL);
        j8.put("bool", "true");
        j8.put("zero", -0.0d);
        j8.put("\\u2028", " ");
        j8.put("\\u2029", " ");
        JSONArray a4 = j8.getJSONArray("foo");
        a4.put(666);
        a4.put(2001.99d);
        a4.put("so \"fine\".");
        a4.put("so <fine>.");
        a4.put(true);
        a4.put(false);
        a4.put(new JSONArray());
        a4.put(new JSONObject());
        j8.put("keys", JSONObject.getNames(j8));
        System.out.println(j8.toString(4));
        System.out.println(XML.toString(j8));
        System.out.println(new StringBuffer().append("String: ").append(j8.getDouble("String")).toString());
        System.out.println(new StringBuffer().append("  bool: ").append(j8.getBoolean("bool")).toString());
        System.out.println(new StringBuffer().append("    to: ").append(j8.getString("to")).toString());
        System.out.println(new StringBuffer().append("  true: ").append(j8.getString("true")).toString());
        System.out.println(new StringBuffer().append("   foo: ").append(j8.getJSONArray("foo")).toString());
        System.out.println(new StringBuffer().append("    op: ").append(j8.getString("op")).toString());
        System.out.println(new StringBuffer().append("   ten: ").append(j8.getInt("ten")).toString());
        System.out.println(new StringBuffer().append("  oops: ").append(j8.optBoolean("oops")).toString());
        JSONObject j9 = XML.toJSONObject("<xml one = 1 two=' \"2\" '><five></five>First \t&lt;content&gt;<five></five> This is \"content\". <three>  3  </three>JSON does not preserve the sequencing of elements and contents.<three>  III  </three>  <three>  T H R E E</three><four/>Content text is an implied structure in XML. <six content=\"6\"/>JSON does not have implied structure:<seven>7</seven>everything is explicit.<![CDATA[CDATA blocks<are><supported>!]]></xml>");
        System.out.println(j9.toString(2));
        System.out.println(XML.toString(j9));
        System.out.println(ElementType.MATCH_ANY_LOCALNAME);
        JSONArray ja = JSONML.toJSONArray("<xml one = 1 two=' \"2\" '><five></five>First \t&lt;content&gt;<five></five> This is \"content\". <three>  3  </three>JSON does not preserve the sequencing of elements and contents.<three>  III  </three>  <three>  T H R E E</three><four/>Content text is an implied structure in XML. <six content=\"6\"/>JSON does not have implied structure:<seven>7</seven>everything is explicit.<![CDATA[CDATA blocks<are><supported>!]]></xml>");
        System.out.println(ja.toString(4));
        System.out.println(JSONML.toString(ja));
        System.out.println(ElementType.MATCH_ANY_LOCALNAME);
        JSONArray ja2 = JSONML.toJSONArray("<xml do='0'>uno<a re='1' mi='2'>dos<b fa='3'/>tres<c>true</c>quatro</a>cinqo<d>seis<e/></d></xml>");
        System.out.println(ja2.toString(4));
        System.out.println(JSONML.toString(ja2));
        System.out.println(ElementType.MATCH_ANY_LOCALNAME);
        JSONObject j10 = XML.toJSONObject("<mapping><empty/>   <class name = \"Customer\">      <field name = \"ID\" type = \"string\">         <bind-xml name=\"ID\" node=\"attribute\"/>      </field>      <field name = \"FirstName\" type = \"FirstName\"/>      <field name = \"MI\" type = \"MI\"/>      <field name = \"LastName\" type = \"LastName\"/>   </class>   <class name = \"FirstName\">      <field name = \"text\">         <bind-xml name = \"text\" node = \"text\"/>      </field>   </class>   <class name = \"MI\">      <field name = \"text\">         <bind-xml name = \"text\" node = \"text\"/>      </field>   </class>   <class name = \"LastName\">      <field name = \"text\">         <bind-xml name = \"text\" node = \"text\"/>      </field>   </class></mapping>");
        System.out.println(j10.toString(2));
        System.out.println(XML.toString(j10));
        System.out.println(ElementType.MATCH_ANY_LOCALNAME);
        JSONArray ja3 = JSONML.toJSONArray("<mapping><empty/>   <class name = \"Customer\">      <field name = \"ID\" type = \"string\">         <bind-xml name=\"ID\" node=\"attribute\"/>      </field>      <field name = \"FirstName\" type = \"FirstName\"/>      <field name = \"MI\" type = \"MI\"/>      <field name = \"LastName\" type = \"LastName\"/>   </class>   <class name = \"FirstName\">      <field name = \"text\">         <bind-xml name = \"text\" node = \"text\"/>      </field>   </class>   <class name = \"MI\">      <field name = \"text\">         <bind-xml name = \"text\" node = \"text\"/>      </field>   </class>   <class name = \"LastName\">      <field name = \"text\">         <bind-xml name = \"text\" node = \"text\"/>      </field>   </class></mapping>");
        System.out.println(ja3.toString(4));
        System.out.println(JSONML.toString(ja3));
        System.out.println(ElementType.MATCH_ANY_LOCALNAME);
        JSONObject j11 = XML.toJSONObject("<?xml version=\"1.0\" ?><Book Author=\"Anonymous\"><Title>Sample Book</Title><Chapter id=\"1\">This is chapter 1. It is not very long or interesting.</Chapter><Chapter id=\"2\">This is chapter 2. Although it is longer than chapter 1, it is not any more interesting.</Chapter></Book>");
        System.out.println(j11.toString(2));
        System.out.println(XML.toString(j11));
        System.out.println(ElementType.MATCH_ANY_LOCALNAME);
        JSONObject j12 = XML.toJSONObject("<!DOCTYPE bCard 'http://www.cs.caltech.edu/~adam/schemas/bCard'><bCard><?xml default bCard        firstname = ''        lastname  = '' company   = '' email = '' homepage  = ''?><bCard        firstname = 'Rohit'        lastname  = 'Khare'        company   = 'MCI'        email     = 'khare@mci.net'        homepage  = 'http://pest.w3.org/'/><bCard        firstname = 'Adam'        lastname  = 'Rifkin'        company   = 'Caltech Infospheres Project'        email     = 'adam@cs.caltech.edu'        homepage  = 'http://www.cs.caltech.edu/~adam/'/></bCard>");
        System.out.println(j12.toString(2));
        System.out.println(XML.toString(j12));
        System.out.println(ElementType.MATCH_ANY_LOCALNAME);
        JSONObject j13 = XML.toJSONObject("<?xml version=\"1.0\"?><customer>    <firstName>        <text>Fred</text>    </firstName>    <ID>fbs0001</ID>    <lastName> <text>Scerbo</text>    </lastName>    <MI>        <text>B</text>    </MI></customer>");
        System.out.println(j13.toString(2));
        System.out.println(XML.toString(j13));
        System.out.println(ElementType.MATCH_ANY_LOCALNAME);
        JSONObject j14 = XML.toJSONObject("<!ENTITY tp-address PUBLIC '-//ABC University::Special Collections Library//TEXT (titlepage: name and address)//EN' 'tpspcoll.sgm'><list type='simple'><head>Repository Address </head><item>Special Collections Library</item><item>ABC University</item><item>Main Library, 40 Circle Drive</item><item>Ourtown, Pennsylvania</item><item>17654 USA</item></list>");
        System.out.println(j14.toString());
        System.out.println(XML.toString(j14));
        System.out.println(ElementType.MATCH_ANY_LOCALNAME);
        JSONObject j15 = XML.toJSONObject("<test intertag status=ok><empty/>deluxe<blip sweet=true>&amp;&quot;toot&quot;&toot;&#x41;</blip><x>eks</x><w>bonus</w><w>bonus2</w></test>");
        System.out.println(j15.toString(2));
        System.out.println(XML.toString(j15));
        System.out.println(ElementType.MATCH_ANY_LOCALNAME);
        JSONObject j16 = HTTP.toJSONObject("GET / HTTP/1.0\nAccept: image/gif, image/x-xbitmap, image/jpeg, image/pjpeg, application/vnd.ms-powerpoint, application/vnd.ms-excel, application/msword, */*\nAccept-Language: en-us\nUser-Agent: Mozilla/4.0 (compatible; MSIE 5.5; Windows 98; Win 9x 4.90; T312461; Q312461)\nHost: www.nokko.com\nConnection: keep-alive\nAccept-encoding: gzip, deflate\n");
        System.out.println(j16.toString(2));
        System.out.println(HTTP.toString(j16));
        System.out.println(ElementType.MATCH_ANY_LOCALNAME);
        JSONObject j17 = HTTP.toJSONObject("HTTP/1.1 200 Oki Doki\nDate: Sun, 26 May 2002 17:38:52 GMT\nServer: Apache/1.3.23 (Unix) mod_perl/1.26\nKeep-Alive: timeout=15, max=100\nConnection: Keep-Alive\nTransfer-Encoding: chunked\nContent-Type: text/html\n");
        System.out.println(j17.toString(2));
        System.out.println(HTTP.toString(j17));
        System.out.println(ElementType.MATCH_ANY_LOCALNAME);
        JSONObject j18 = new JSONObject("{nix: null, nux: false, null: 'null', 'Request-URI': '/', Method: 'GET', 'HTTP-Version': 'HTTP/1.0'}");
        System.out.println(j18.toString(2));
        System.out.println(new StringBuffer().append("isNull: ").append(j18.isNull("nix")).toString());
        System.out.println(new StringBuffer().append("   has: ").append(j18.has("nix")).toString());
        System.out.println(XML.toString(j18));
        System.out.println(HTTP.toString(j18));
        System.out.println(ElementType.MATCH_ANY_LOCALNAME);
        JSONObject j19 = XML.toJSONObject("<?xml version='1.0' encoding='UTF-8'?>\n\n<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/1999/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/1999/XMLSchema\"><SOAP-ENV:Body><ns1:doGoogleSearch xmlns:ns1=\"urn:GoogleSearch\" SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\"><key xsi:type=\"xsd:string\">GOOGLEKEY</key> <q xsi:type=\"xsd:string\">'+search+'</q> <start xsi:type=\"xsd:int\">0</start> <maxResults xsi:type=\"xsd:int\">10</maxResults> <filter xsi:type=\"xsd:boolean\">true</filter> <restrict xsi:type=\"xsd:string\"></restrict> <safeSearch xsi:type=\"xsd:boolean\">false</safeSearch> <lr xsi:type=\"xsd:string\"></lr> <ie xsi:type=\"xsd:string\">latin1</ie> <oe xsi:type=\"xsd:string\">latin1</oe></ns1:doGoogleSearch></SOAP-ENV:Body></SOAP-ENV:Envelope>");
        System.out.println(j19.toString(2));
        System.out.println(XML.toString(j19));
        System.out.println(ElementType.MATCH_ANY_LOCALNAME);
        JSONObject j20 = new JSONObject("{Envelope: {Body: {\"ns1:doGoogleSearch\": {oe: \"latin1\", filter: true, q: \"'+search+'\", key: \"GOOGLEKEY\", maxResults: 10, \"SOAP-ENV:encodingStyle\": \"http://schemas.xmlsoap.org/soap/encoding/\", start: 0, ie: \"latin1\", safeSearch:false, \"xmlns:ns1\": \"urn:GoogleSearch\"}}}}");
        System.out.println(j20.toString(2));
        System.out.println(XML.toString(j20));
        System.out.println(ElementType.MATCH_ANY_LOCALNAME);
        JSONObject j21 = CookieList.toJSONObject("  f%oo = b+l=ah  ; o;n%40e = t.wo ");
        System.out.println(j21.toString(2));
        System.out.println(CookieList.toString(j21));
        System.out.println(ElementType.MATCH_ANY_LOCALNAME);
        JSONObject j22 = Cookie.toJSONObject("f%oo=blah; secure ;expires = April 24, 2002");
        System.out.println(j22.toString(2));
        System.out.println(Cookie.toString(j22));
        System.out.println(ElementType.MATCH_ANY_LOCALNAME);
        System.out.println(new JSONObject("{script: 'It is not allowed in HTML to send a close script tag in a string<script>because it confuses browsers</script>so we insert a backslash before the /'}").toString());
        System.out.println(ElementType.MATCH_ANY_LOCALNAME);
        JSONTokener jSONTokener = new JSONTokener("{op:'test', to:'session', pre:1}{op:'test', to:'session', pre:2}");
        JSONObject j23 = new JSONObject(jSONTokener);
        System.out.println(j23.toString());
        System.out.println(new StringBuffer().append("pre: ").append(j23.optInt("pre")).toString());
        System.out.println(jSONTokener.skipTo('{'));
        System.out.println(new JSONObject(jSONTokener).toString());
        System.out.println(ElementType.MATCH_ANY_LOCALNAME);
        JSONArray a5 = CDL.toJSONArray("No quotes, 'Single Quotes', \"Double Quotes\"\n1,'2',\"3\"\n,'It is \"good,\"', \"It works.\"\n\n");
        System.out.println(CDL.toString(a5));
        System.out.println(ElementType.MATCH_ANY_LOCALNAME);
        System.out.println(a5.toString(4));
        System.out.println(ElementType.MATCH_ANY_LOCALNAME);
        JSONArray a6 = new JSONArray(" [\"<escape>\", next is an implied null , , ok,] ");
        System.out.println(a6.toString());
        System.out.println(ElementType.MATCH_ANY_LOCALNAME);
        System.out.println(XML.toString(a6));
        System.out.println(ElementType.MATCH_ANY_LOCALNAME);
        JSONObject j24 = new JSONObject("{ fun => with non-standard forms ; forgiving => This package can be used to parse formats that are similar to but not stricting conforming to JSON; why=To make it easier to migrate existing data to JSON,one = [[1.00]]; uno=[[{1=>1}]];'+':+6e66 ;pluses=+++;empty = '' , 'double':0.666,true: TRUE, false: FALSE, null=NULL;[true] = [[!,@;*]]; string=>  o. k. ; \r oct=0666; hex=0x666; dec=666; o=0999; noh=0x0x}");
        System.out.println(j24.toString(4));
        System.out.println(ElementType.MATCH_ANY_LOCALNAME);
        if (j24.getBoolean("true") && !j24.getBoolean("false")) {
            System.out.println("It's all good");
        }
        System.out.println(ElementType.MATCH_ANY_LOCALNAME);
        JSONObject j25 = new JSONObject(j24, new String[]{"dec", "oct", "hex", "missing"});
        System.out.println(j25.toString(4));
        System.out.println(ElementType.MATCH_ANY_LOCALNAME);
        System.out.println(new JSONStringer().array().value(a6).value(j25).endArray());
        JSONObject j26 = new JSONObject("{string: \"98.6\", long: 2147483648, int: 2147483647, longer: 9223372036854775807, double: 9223372036854775808}");
        System.out.println(j26.toString(4));
        System.out.println("\ngetInt");
        System.out.println(new StringBuffer().append("int    ").append(j26.getInt("int")).toString());
        System.out.println(new StringBuffer().append("long   ").append(j26.getInt("long")).toString());
        System.out.println(new StringBuffer().append("longer ").append(j26.getInt("longer")).toString());
        System.out.println(new StringBuffer().append("double ").append(j26.getInt("double")).toString());
        System.out.println(new StringBuffer().append("string ").append(j26.getInt(DesignerProperty.PROPERTY_TYPE_STRING)).toString());
        System.out.println("\ngetLong");
        System.out.println(new StringBuffer().append("int    ").append(j26.getLong("int")).toString());
        System.out.println(new StringBuffer().append("long   ").append(j26.getLong("long")).toString());
        System.out.println(new StringBuffer().append("longer ").append(j26.getLong("longer")).toString());
        System.out.println(new StringBuffer().append("double ").append(j26.getLong("double")).toString());
        System.out.println(new StringBuffer().append("string ").append(j26.getLong(DesignerProperty.PROPERTY_TYPE_STRING)).toString());
        System.out.println("\ngetDouble");
        System.out.println(new StringBuffer().append("int    ").append(j26.getDouble("int")).toString());
        System.out.println(new StringBuffer().append("long   ").append(j26.getDouble("long")).toString());
        System.out.println(new StringBuffer().append("longer ").append(j26.getDouble("longer")).toString());
        System.out.println(new StringBuffer().append("double ").append(j26.getDouble("double")).toString());
        System.out.println(new StringBuffer().append("string ").append(j26.getDouble(DesignerProperty.PROPERTY_TYPE_STRING)).toString());
        j26.put("good sized", Long.MAX_VALUE);
        System.out.println(j26.toString(4));
        System.out.println(new JSONArray("[2147483647, 2147483648, 9223372036854775807, 9223372036854775808]").toString(4));
        System.out.println("\nKeys: ");
        Iterator it = j26.keys();
        while (it.hasNext()) {
            String s = (String) it.next();
            System.out.println(new StringBuffer().append(s).append(": ").append(j26.getString(s)).toString());
        }
        System.out.println("\naccumulate: ");
        JSONObject j27 = new JSONObject();
        j27.accumulate("stooge", "Curly");
        j27.accumulate("stooge", "Larry");
        j27.accumulate("stooge", "Moe");
        j27.getJSONArray("stooge").put(5, "Shemp");
        System.out.println(j27.toString(4));
        System.out.println("\nwrite:");
        System.out.println(j27.write(new StringWriter()));
        JSONObject j28 = XML.toJSONObject("<xml empty><a></a><a>1</a><a>22</a><a>333</a></xml>");
        System.out.println(j28.toString(4));
        System.out.println(XML.toString(j28));
        JSONObject j29 = XML.toJSONObject("<book><chapter>Content of the first chapter</chapter><chapter>Content of the second chapter      <chapter>Content of the first subchapter</chapter>      <chapter>Content of the second subchapter</chapter></chapter><chapter>Third Chapter</chapter></book>");
        System.out.println(j29.toString(4));
        System.out.println(XML.toString(j29));
        JSONArray a7 = JSONML.toJSONArray("<book><chapter>Content of the first chapter</chapter><chapter>Content of the second chapter      <chapter>Content of the first subchapter</chapter>      <chapter>Content of the second subchapter</chapter></chapter><chapter>Third Chapter</chapter></book>");
        System.out.println(a7.toString(4));
        System.out.println(JSONML.toString(a7));
        JSONObject j30 = new JSONObject((Map) null);
        JSONArray a8 = new JSONArray((Collection) null);
        j30.append("stooge", "Joe DeRita");
        j30.append("stooge", "Shemp");
        j30.accumulate("stooges", "Curly");
        j30.accumulate("stooges", "Larry");
        j30.accumulate("stooges", "Moe");
        j30.accumulate("stoogearray", j30.get("stooges"));
        j30.put("map", (Map) null);
        j30.put("collection", (Collection) null);
        j30.put("array", a8);
        a8.put((Map) null);
        a8.put((Collection) null);
        System.out.println(j30.toString(4));
        System.out.println(new JSONObject("{plist=Apple; AnimalSmells = { pig = piggish; lamb = lambish; worm = wormy; }; AnimalSounds = { pig = oink; lamb = baa; worm = baa;  Lisa = \"Why is the worm talking like a lamb?\" } ; AnimalColors = { pig = pink; lamb = black; worm = pink; } } ").toString(4));
        JSONArray a9 = new JSONArray(" (\"San Francisco\", \"New York\", \"Seoul\", \"London\", \"Seattle\", \"Shanghai\")");
        System.out.println(a9.toString());
        JSONObject j31 = XML.toJSONObject("<a ichi='1' ni='2'><b>The content of b</b> and <c san='3'>The content of c</c><d>do</d><e></e><d>re</d><f/><d>mi</d></a>");
        System.out.println(j31.toString(2));
        System.out.println(XML.toString(j31));
        System.out.println(ElementType.MATCH_ANY_LOCALNAME);
        JSONArray ja4 = JSONML.toJSONArray("<a ichi='1' ni='2'><b>The content of b</b> and <c san='3'>The content of c</c><d>do</d><e></e><d>re</d><f/><d>mi</d></a>");
        System.out.println(ja4.toString(4));
        System.out.println(JSONML.toString(ja4));
        System.out.println(ElementType.MATCH_ANY_LOCALNAME);
        System.out.println("\nTesting Exceptions: ");
        System.out.print("Exception: ");
        try {
            a = new JSONArray();
            try {
                a.put(Double.NEGATIVE_INFINITY);
                a.put(Double.NaN);
                System.out.println(a.toString());
            } catch (Exception e) {
                e = e;
                a9 = a;
                try {
                    System.out.println(e);
                    a = a9;
                    System.out.print("Exception: ");
                    System.out.println(j31.getDouble("stooge"));
                    System.out.print("Exception: ");
                    System.out.println(j31.getDouble("howard"));
                    System.out.print("Exception: ");
                    System.out.println(j31.put((String) null, "howard"));
                    System.out.print("Exception: ");
                    System.out.println(a.getDouble(0));
                    System.out.print("Exception: ");
                    System.out.println(a.get(-1));
                    System.out.print("Exception: ");
                    System.out.println(a.put(Double.NaN));
                    System.out.print("Exception: ");
                    j31 = XML.toJSONObject("<a><b>    ");
                    System.out.print("Exception: ");
                    j31 = XML.toJSONObject("<a></b>    ");
                    System.out.print("Exception: ");
                    j = XML.toJSONObject("<a></a    ");
                    System.out.print("Exception: ");
                    System.out.println(new JSONArray(new Object()).toString());
                    System.out.print("Exception: ");
                    System.out.println(new JSONArray("[)").toString());
                    System.out.print("Exception: ");
                    System.out.println(JSONML.toJSONArray("<xml").toString(4));
                    System.out.print("Exception: ");
                    System.out.println(JSONML.toJSONArray("<right></wrong>").toString(4));
                    System.out.print("Exception: ");
                    j2 = new JSONObject("{\"koda\": true, \"koda\": true}");
                    try {
                        System.out.println(j2.toString(4));
                    } catch (Exception e2) {
                        e = e2;
                        System.out.println(e);
                        System.out.print("Exception: ");
                        String s2 = new JSONStringer().object().key("bosanda").value("MARIE HAA'S").key("bosanda").value("MARIE HAA\\'S").endObject().toString();
                        System.out.println(j2.toString(4));
                    }
                    System.out.print("Exception: ");
                    try {
                        String s22 = new JSONStringer().object().key("bosanda").value("MARIE HAA'S").key("bosanda").value("MARIE HAA\\'S").endObject().toString();
                        System.out.println(j2.toString(4));
                    } catch (Exception e3) {
                        e = e3;
                        System.out.println(e);
                    }
                } catch (Exception e4) {
                    System.out.println(e4.toString());
                    return;
                }
            }
        } catch (Exception e5) {
            e = e5;
            System.out.println(e);
            a = a9;
            System.out.print("Exception: ");
            System.out.println(j31.getDouble("stooge"));
            System.out.print("Exception: ");
            System.out.println(j31.getDouble("howard"));
            System.out.print("Exception: ");
            System.out.println(j31.put((String) null, "howard"));
            System.out.print("Exception: ");
            System.out.println(a.getDouble(0));
            System.out.print("Exception: ");
            System.out.println(a.get(-1));
            System.out.print("Exception: ");
            System.out.println(a.put(Double.NaN));
            System.out.print("Exception: ");
            j31 = XML.toJSONObject("<a><b>    ");
            System.out.print("Exception: ");
            j31 = XML.toJSONObject("<a></b>    ");
            System.out.print("Exception: ");
            j = XML.toJSONObject("<a></a    ");
            System.out.print("Exception: ");
            System.out.println(new JSONArray(new Object()).toString());
            System.out.print("Exception: ");
            System.out.println(new JSONArray("[)").toString());
            System.out.print("Exception: ");
            System.out.println(JSONML.toJSONArray("<xml").toString(4));
            System.out.print("Exception: ");
            System.out.println(JSONML.toJSONArray("<right></wrong>").toString(4));
            System.out.print("Exception: ");
            j2 = new JSONObject("{\"koda\": true, \"koda\": true}");
            System.out.println(j2.toString(4));
            System.out.print("Exception: ");
            String s222 = new JSONStringer().object().key("bosanda").value("MARIE HAA'S").key("bosanda").value("MARIE HAA\\'S").endObject().toString();
            System.out.println(j2.toString(4));
        }
        System.out.print("Exception: ");
        try {
            System.out.println(j31.getDouble("stooge"));
        } catch (Exception e6) {
            System.out.println(e6);
        }
        System.out.print("Exception: ");
        try {
            System.out.println(j31.getDouble("howard"));
        } catch (Exception e7) {
            System.out.println(e7);
        }
        System.out.print("Exception: ");
        try {
            System.out.println(j31.put((String) null, "howard"));
        } catch (Exception e8) {
            System.out.println(e8);
        }
        System.out.print("Exception: ");
        try {
            System.out.println(a.getDouble(0));
        } catch (Exception e9) {
            System.out.println(e9);
        }
        System.out.print("Exception: ");
        try {
            System.out.println(a.get(-1));
        } catch (Exception e10) {
            System.out.println(e10);
        }
        System.out.print("Exception: ");
        try {
            System.out.println(a.put(Double.NaN));
        } catch (Exception e11) {
            System.out.println(e11);
        }
        System.out.print("Exception: ");
        try {
            j31 = XML.toJSONObject("<a><b>    ");
        } catch (Exception e12) {
            System.out.println(e12);
        }
        System.out.print("Exception: ");
        try {
            j31 = XML.toJSONObject("<a></b>    ");
        } catch (Exception e13) {
            System.out.println(e13);
        }
        System.out.print("Exception: ");
        try {
            j = XML.toJSONObject("<a></a    ");
        } catch (Exception e14) {
            System.out.println(e14);
            j = j31;
        }
        System.out.print("Exception: ");
        try {
            try {
                System.out.println(new JSONArray(new Object()).toString());
            } catch (Exception e15) {
                e = e15;
                System.out.println(e);
                System.out.print("Exception: ");
                System.out.println(new JSONArray("[)").toString());
                System.out.print("Exception: ");
                System.out.println(JSONML.toJSONArray("<xml").toString(4));
                System.out.print("Exception: ");
                System.out.println(JSONML.toJSONArray("<right></wrong>").toString(4));
                System.out.print("Exception: ");
                j2 = new JSONObject("{\"koda\": true, \"koda\": true}");
                System.out.println(j2.toString(4));
                System.out.print("Exception: ");
                String s2222 = new JSONStringer().object().key("bosanda").value("MARIE HAA'S").key("bosanda").value("MARIE HAA\\'S").endObject().toString();
                System.out.println(j2.toString(4));
            }
        } catch (Exception e16) {
            e = e16;
            System.out.println(e);
            System.out.print("Exception: ");
            System.out.println(new JSONArray("[)").toString());
            System.out.print("Exception: ");
            System.out.println(JSONML.toJSONArray("<xml").toString(4));
            System.out.print("Exception: ");
            System.out.println(JSONML.toJSONArray("<right></wrong>").toString(4));
            System.out.print("Exception: ");
            j2 = new JSONObject("{\"koda\": true, \"koda\": true}");
            System.out.println(j2.toString(4));
            System.out.print("Exception: ");
            String s22222 = new JSONStringer().object().key("bosanda").value("MARIE HAA'S").key("bosanda").value("MARIE HAA\\'S").endObject().toString();
            System.out.println(j2.toString(4));
        }
        System.out.print("Exception: ");
        try {
            try {
                System.out.println(new JSONArray("[)").toString());
            } catch (Exception e17) {
                e = e17;
                System.out.println(e);
                System.out.print("Exception: ");
                System.out.println(JSONML.toJSONArray("<xml").toString(4));
                System.out.print("Exception: ");
                System.out.println(JSONML.toJSONArray("<right></wrong>").toString(4));
                System.out.print("Exception: ");
                j2 = new JSONObject("{\"koda\": true, \"koda\": true}");
                System.out.println(j2.toString(4));
                System.out.print("Exception: ");
                String s222222 = new JSONStringer().object().key("bosanda").value("MARIE HAA'S").key("bosanda").value("MARIE HAA\\'S").endObject().toString();
                System.out.println(j2.toString(4));
            }
        } catch (Exception e18) {
            e = e18;
            JSONArray jSONArray = a;
            System.out.println(e);
            System.out.print("Exception: ");
            System.out.println(JSONML.toJSONArray("<xml").toString(4));
            System.out.print("Exception: ");
            System.out.println(JSONML.toJSONArray("<right></wrong>").toString(4));
            System.out.print("Exception: ");
            j2 = new JSONObject("{\"koda\": true, \"koda\": true}");
            System.out.println(j2.toString(4));
            System.out.print("Exception: ");
            String s2222222 = new JSONStringer().object().key("bosanda").value("MARIE HAA'S").key("bosanda").value("MARIE HAA\\'S").endObject().toString();
            System.out.println(j2.toString(4));
        }
        System.out.print("Exception: ");
        try {
            System.out.println(JSONML.toJSONArray("<xml").toString(4));
        } catch (Exception e19) {
            System.out.println(e19);
        }
        System.out.print("Exception: ");
        try {
            System.out.println(JSONML.toJSONArray("<right></wrong>").toString(4));
        } catch (Exception e20) {
            System.out.println(e20);
        }
        System.out.print("Exception: ");
        try {
            j2 = new JSONObject("{\"koda\": true, \"koda\": true}");
            System.out.println(j2.toString(4));
        } catch (Exception e21) {
            e = e21;
            j2 = j;
            System.out.println(e);
            System.out.print("Exception: ");
            String s22222222 = new JSONStringer().object().key("bosanda").value("MARIE HAA'S").key("bosanda").value("MARIE HAA\\'S").endObject().toString();
            System.out.println(j2.toString(4));
        }
        System.out.print("Exception: ");
        try {
            String s222222222 = new JSONStringer().object().key("bosanda").value("MARIE HAA'S").key("bosanda").value("MARIE HAA\\'S").endObject().toString();
            System.out.println(j2.toString(4));
        } catch (Exception e22) {
            e = e22;
            System.out.println(e);
        }
    }
}
