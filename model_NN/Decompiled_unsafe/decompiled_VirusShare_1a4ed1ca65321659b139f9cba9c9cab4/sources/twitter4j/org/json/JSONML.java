package twitter4j.org.json;

import java.util.Iterator;

public class JSONML {
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x014a, code lost:
        throw r12.syntaxError("Reserved attribute.");
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.Object parse(twitter4j.org.json.XMLTokener r12, boolean r13, twitter4j.org.json.JSONArray r14) throws twitter4j.org.json.JSONException {
        /*
            r11 = 91
            r10 = 45
            r2 = 0
            r4 = 0
            r5 = 0
            r6 = 0
        L_0x0008:
            java.lang.Object r7 = r12.nextContent()
            java.lang.Character r8 = twitter4j.org.json.XML.LT
            if (r7 != r8) goto L_0x01d2
            java.lang.Object r7 = r12.nextToken()
            boolean r8 = r7 instanceof java.lang.Character
            if (r8 == 0) goto L_0x00c1
            java.lang.Character r8 = twitter4j.org.json.XML.SLASH
            if (r7 != r8) goto L_0x0052
            java.lang.Object r7 = r12.nextToken()
            boolean r8 = r7 instanceof java.lang.String
            if (r8 != 0) goto L_0x0043
            twitter4j.org.json.JSONException r8 = new twitter4j.org.json.JSONException
            java.lang.StringBuffer r9 = new java.lang.StringBuffer
            r9.<init>()
            java.lang.String r10 = "Expected a closing name instead of '"
            java.lang.StringBuffer r9 = r9.append(r10)
            java.lang.StringBuffer r9 = r9.append(r7)
            java.lang.String r10 = "'."
            java.lang.StringBuffer r9 = r9.append(r10)
            java.lang.String r9 = r9.toString()
            r8.<init>(r9)
            throw r8
        L_0x0043:
            java.lang.Object r8 = r12.nextToken()
            java.lang.Character r9 = twitter4j.org.json.XML.GT
            if (r8 == r9) goto L_0x0178
            java.lang.String r8 = "Misshaped close tag"
            twitter4j.org.json.JSONException r8 = r12.syntaxError(r8)
            throw r8
        L_0x0052:
            java.lang.Character r8 = twitter4j.org.json.XML.BANG
            if (r7 != r8) goto L_0x00af
            char r1 = r12.next()
            if (r1 != r10) goto L_0x006b
            char r8 = r12.next()
            if (r8 != r10) goto L_0x0067
            java.lang.String r8 = "-->"
            r12.skipPast(r8)
        L_0x0067:
            r12.back()
            goto L_0x0008
        L_0x006b:
            if (r1 != r11) goto L_0x0090
            java.lang.Object r7 = r12.nextToken()
            java.lang.String r8 = "CDATA"
            boolean r8 = r7.equals(r8)
            if (r8 == 0) goto L_0x0089
            char r8 = r12.next()
            if (r8 != r11) goto L_0x0089
            if (r14 == 0) goto L_0x0008
            java.lang.String r8 = r12.nextCDATA()
            r14.put(r8)
            goto L_0x0008
        L_0x0089:
            java.lang.String r8 = "Expected 'CDATA['"
            twitter4j.org.json.JSONException r8 = r12.syntaxError(r8)
            throw r8
        L_0x0090:
            r3 = 1
        L_0x0091:
            java.lang.Object r7 = r12.nextMeta()
            if (r7 != 0) goto L_0x009e
            java.lang.String r8 = "Missing '>' after '<!'."
            twitter4j.org.json.JSONException r8 = r12.syntaxError(r8)
            throw r8
        L_0x009e:
            java.lang.Character r8 = twitter4j.org.json.XML.LT
            if (r7 != r8) goto L_0x00a8
            int r3 = r3 + 1
        L_0x00a4:
            if (r3 > 0) goto L_0x0091
            goto L_0x0008
        L_0x00a8:
            java.lang.Character r8 = twitter4j.org.json.XML.GT
            if (r7 != r8) goto L_0x00a4
            int r3 = r3 + -1
            goto L_0x00a4
        L_0x00af:
            java.lang.Character r8 = twitter4j.org.json.XML.QUEST
            if (r7 != r8) goto L_0x00ba
            java.lang.String r8 = "?>"
            r12.skipPast(r8)
            goto L_0x0008
        L_0x00ba:
            java.lang.String r8 = "Misshaped tag"
            twitter4j.org.json.JSONException r8 = r12.syntaxError(r8)
            throw r8
        L_0x00c1:
            boolean r8 = r7 instanceof java.lang.String
            if (r8 != 0) goto L_0x00e3
            java.lang.StringBuffer r8 = new java.lang.StringBuffer
            r8.<init>()
            java.lang.String r9 = "Bad tagName '"
            java.lang.StringBuffer r8 = r8.append(r9)
            java.lang.StringBuffer r8 = r8.append(r7)
            java.lang.String r9 = "'."
            java.lang.StringBuffer r8 = r8.append(r9)
            java.lang.String r8 = r8.toString()
            twitter4j.org.json.JSONException r8 = r12.syntaxError(r8)
            throw r8
        L_0x00e3:
            r6 = r7
            java.lang.String r6 = (java.lang.String) r6
            twitter4j.org.json.JSONArray r4 = new twitter4j.org.json.JSONArray
            r4.<init>()
            twitter4j.org.json.JSONObject r5 = new twitter4j.org.json.JSONObject
            r5.<init>()
            if (r13 == 0) goto L_0x010b
            r4.put(r6)
            if (r14 == 0) goto L_0x00fa
            r14.put(r4)
        L_0x00fa:
            r7 = 0
        L_0x00fb:
            if (r7 != 0) goto L_0x01e3
            java.lang.Object r7 = r12.nextToken()
            r0 = r7
        L_0x0102:
            if (r0 != 0) goto L_0x0116
            java.lang.String r8 = "Misshaped tag"
            twitter4j.org.json.JSONException r8 = r12.syntaxError(r8)
            throw r8
        L_0x010b:
            java.lang.String r8 = "tagName"
            r5.put(r8, r6)
            if (r14 == 0) goto L_0x00fa
            r14.put(r5)
            goto L_0x00fa
        L_0x0116:
            boolean r8 = r0 instanceof java.lang.String
            if (r8 != 0) goto L_0x0138
            if (r13 == 0) goto L_0x0125
            int r8 = r5.length()
            if (r8 <= 0) goto L_0x0125
            r4.put(r5)
        L_0x0125:
            java.lang.Character r8 = twitter4j.org.json.XML.SLASH
            if (r0 != r8) goto L_0x017b
            java.lang.Object r8 = r12.nextToken()
            java.lang.Character r9 = twitter4j.org.json.XML.GT
            if (r8 == r9) goto L_0x0173
            java.lang.String r8 = "Misshaped tag"
            twitter4j.org.json.JSONException r8 = r12.syntaxError(r8)
            throw r8
        L_0x0138:
            java.lang.String r0 = (java.lang.String) r0
            if (r13 != 0) goto L_0x014b
            java.lang.String r8 = "tagName"
            if (r0 == r8) goto L_0x0144
            java.lang.String r8 = "childNode"
            if (r0 != r8) goto L_0x014b
        L_0x0144:
            java.lang.String r8 = "Reserved attribute."
            twitter4j.org.json.JSONException r8 = r12.syntaxError(r8)
            throw r8
        L_0x014b:
            java.lang.Object r7 = r12.nextToken()
            java.lang.Character r8 = twitter4j.org.json.XML.EQ
            if (r7 != r8) goto L_0x016d
            java.lang.Object r7 = r12.nextToken()
            boolean r8 = r7 instanceof java.lang.String
            if (r8 != 0) goto L_0x0162
            java.lang.String r8 = "Missing value"
            twitter4j.org.json.JSONException r8 = r12.syntaxError(r8)
            throw r8
        L_0x0162:
            java.lang.String r7 = (java.lang.String) r7
            java.lang.Object r8 = twitter4j.org.json.JSONObject.stringToValue(r7)
            r5.accumulate(r0, r8)
            r7 = 0
            goto L_0x00fb
        L_0x016d:
            java.lang.String r8 = ""
            r5.accumulate(r0, r8)
            goto L_0x00fb
        L_0x0173:
            if (r14 != 0) goto L_0x0008
            if (r13 == 0) goto L_0x0179
            r7 = r4
        L_0x0178:
            return r7
        L_0x0179:
            r7 = r5
            goto L_0x0178
        L_0x017b:
            java.lang.Character r8 = twitter4j.org.json.XML.GT
            if (r0 == r8) goto L_0x0186
            java.lang.String r8 = "Misshaped tag"
            twitter4j.org.json.JSONException r8 = r12.syntaxError(r8)
            throw r8
        L_0x0186:
            java.lang.Object r2 = parse(r12, r13, r4)
            java.lang.String r2 = (java.lang.String) r2
            if (r2 == 0) goto L_0x0008
            boolean r8 = r2.equals(r6)
            if (r8 != 0) goto L_0x01bc
            java.lang.StringBuffer r8 = new java.lang.StringBuffer
            r8.<init>()
            java.lang.String r9 = "Mismatched '"
            java.lang.StringBuffer r8 = r8.append(r9)
            java.lang.StringBuffer r8 = r8.append(r6)
            java.lang.String r9 = "' and '"
            java.lang.StringBuffer r8 = r8.append(r9)
            java.lang.StringBuffer r8 = r8.append(r2)
            java.lang.String r9 = "'"
            java.lang.StringBuffer r8 = r8.append(r9)
            java.lang.String r8 = r8.toString()
            twitter4j.org.json.JSONException r8 = r12.syntaxError(r8)
            throw r8
        L_0x01bc:
            r6 = 0
            if (r13 != 0) goto L_0x01ca
            int r8 = r4.length()
            if (r8 <= 0) goto L_0x01ca
            java.lang.String r8 = "childNodes"
            r5.put(r8, r4)
        L_0x01ca:
            if (r14 != 0) goto L_0x0008
            if (r13 == 0) goto L_0x01d0
            r7 = r4
            goto L_0x0178
        L_0x01d0:
            r7 = r5
            goto L_0x0178
        L_0x01d2:
            if (r14 == 0) goto L_0x0008
            boolean r8 = r7 instanceof java.lang.String
            if (r8 == 0) goto L_0x01de
            java.lang.String r7 = (java.lang.String) r7
            java.lang.Object r7 = twitter4j.org.json.JSONObject.stringToValue(r7)
        L_0x01de:
            r14.put(r7)
            goto L_0x0008
        L_0x01e3:
            r0 = r7
            goto L_0x0102
        */
        throw new UnsupportedOperationException("Method not decompiled: twitter4j.org.json.JSONML.parse(twitter4j.org.json.XMLTokener, boolean, twitter4j.org.json.JSONArray):java.lang.Object");
    }

    public static JSONArray toJSONArray(String string) throws JSONException {
        return toJSONArray(new XMLTokener(string));
    }

    public static JSONArray toJSONArray(XMLTokener x) throws JSONException {
        return (JSONArray) parse(x, true, null);
    }

    public static JSONObject toJSONObject(XMLTokener x) throws JSONException {
        return (JSONObject) parse(x, false, null);
    }

    public static JSONObject toJSONObject(String string) throws JSONException {
        return toJSONObject(new XMLTokener(string));
    }

    public static String toString(JSONArray ja) throws JSONException {
        int i;
        StringBuffer sb = new StringBuffer();
        String tagName = ja.getString(0);
        XML.noSpace(tagName);
        String tagName2 = XML.escape(tagName);
        sb.append('<');
        sb.append(tagName2);
        Object e = ja.opt(1);
        if (e instanceof JSONObject) {
            i = 2;
            JSONObject jo = (JSONObject) e;
            Iterator keys = jo.keys();
            while (keys.hasNext()) {
                String k = keys.next().toString();
                XML.noSpace(k);
                String v = jo.optString(k);
                if (v != null) {
                    sb.append(' ');
                    sb.append(XML.escape(k));
                    sb.append('=');
                    sb.append('\"');
                    sb.append(XML.escape(v));
                    sb.append('\"');
                }
            }
        } else {
            i = 1;
        }
        int length = ja.length();
        if (i >= length) {
            sb.append('/');
            sb.append('>');
        } else {
            sb.append('>');
            do {
                Object e2 = ja.get(i);
                i++;
                if (e2 != null) {
                    if (e2 instanceof String) {
                        sb.append(XML.escape(e2.toString()));
                        continue;
                    } else if (e2 instanceof JSONObject) {
                        sb.append(toString((JSONObject) e2));
                        continue;
                    } else if (e2 instanceof JSONArray) {
                        sb.append(toString((JSONArray) e2));
                        continue;
                    } else {
                        continue;
                    }
                }
            } while (i < length);
            sb.append('<');
            sb.append('/');
            sb.append(tagName2);
            sb.append('>');
        }
        return sb.toString();
    }

    public static String toString(JSONObject jo) throws JSONException {
        StringBuffer sb = new StringBuffer();
        String tagName = jo.optString("tagName");
        if (tagName == null) {
            return XML.escape(jo.toString());
        }
        XML.noSpace(tagName);
        String tagName2 = XML.escape(tagName);
        sb.append('<');
        sb.append(tagName2);
        Iterator keys = jo.keys();
        while (keys.hasNext()) {
            String k = keys.next().toString();
            if (!k.equals("tagName") && !k.equals("childNodes")) {
                XML.noSpace(k);
                String v = jo.optString(k);
                if (v != null) {
                    sb.append(' ');
                    sb.append(XML.escape(k));
                    sb.append('=');
                    sb.append('\"');
                    sb.append(XML.escape(v));
                    sb.append('\"');
                }
            }
        }
        JSONArray ja = jo.optJSONArray("childNodes");
        if (ja == null) {
            sb.append('/');
            sb.append('>');
        } else {
            sb.append('>');
            int len = ja.length();
            for (int i = 0; i < len; i++) {
                Object e = ja.get(i);
                if (e != null) {
                    if (e instanceof String) {
                        sb.append(XML.escape(e.toString()));
                    } else if (e instanceof JSONObject) {
                        sb.append(toString((JSONObject) e));
                    } else if (e instanceof JSONArray) {
                        sb.append(toString((JSONArray) e));
                    }
                }
            }
            sb.append('<');
            sb.append('/');
            sb.append(tagName2);
            sb.append('>');
        }
        return sb.toString();
    }
}
