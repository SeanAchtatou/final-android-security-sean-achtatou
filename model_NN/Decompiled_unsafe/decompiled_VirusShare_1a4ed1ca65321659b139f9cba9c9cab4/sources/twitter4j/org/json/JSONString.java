package twitter4j.org.json;

public interface JSONString {
    String toJSONString();
}
