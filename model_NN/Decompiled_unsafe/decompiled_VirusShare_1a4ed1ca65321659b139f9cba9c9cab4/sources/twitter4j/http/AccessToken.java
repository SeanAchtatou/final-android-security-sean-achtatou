package twitter4j.http;

import twitter4j.TwitterException;

public class AccessToken extends OAuthToken {
    private static final long serialVersionUID = -8344528374458826291L;
    private String screenName;
    private int userId;

    public boolean equals(Object x0) {
        return super.equals(x0);
    }

    public String getParameter(String x0) {
        return super.getParameter(x0);
    }

    public String getToken() {
        return super.getToken();
    }

    public String getTokenSecret() {
        return super.getTokenSecret();
    }

    public int hashCode() {
        return super.hashCode();
    }

    public String toString() {
        return super.toString();
    }

    AccessToken(Response res) throws TwitterException {
        this(res.asString());
    }

    AccessToken(String str) {
        super(str);
        this.screenName = getParameter("screen_name");
        String sUserId = getParameter("user_id");
        if (sUserId != null) {
            this.userId = Integer.parseInt(sUserId);
        }
    }

    public AccessToken(String token, String tokenSecret) {
        super(token, tokenSecret);
    }

    public String getScreenName() {
        return this.screenName;
    }

    public int getUserId() {
        return this.userId;
    }
}
