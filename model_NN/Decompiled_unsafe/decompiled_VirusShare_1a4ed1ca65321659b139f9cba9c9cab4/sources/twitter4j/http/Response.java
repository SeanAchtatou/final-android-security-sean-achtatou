package twitter4j.http;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import twitter4j.Configuration;
import twitter4j.TwitterException;
import twitter4j.org.json.JSONArray;
import twitter4j.org.json.JSONException;
import twitter4j.org.json.JSONObject;

public class Response {
    private static final boolean DEBUG = Configuration.getDebug();
    private static ThreadLocal<DocumentBuilder> builders = new ThreadLocal<DocumentBuilder>() {
        /* access modifiers changed from: protected */
        public Object initialValue() {
            return m2initialValue();
        }

        /* access modifiers changed from: protected */
        /* renamed from: initialValue  reason: collision with other method in class */
        public DocumentBuilder m2initialValue() {
            try {
                return DocumentBuilderFactory.newInstance().newDocumentBuilder();
            } catch (ParserConfigurationException ex) {
                throw new ExceptionInInitializerError(ex);
            }
        }
    };
    private static Pattern escaped = Pattern.compile("&#([0-9]{3,5});");
    private HttpURLConnection con;
    private InputStream is;
    private Document responseAsDocument = null;
    private String responseAsString = null;
    private int statusCode;
    private boolean streamConsumed = false;

    public Response(HttpURLConnection con2) throws IOException {
        this.con = con2;
        this.statusCode = con2.getResponseCode();
        InputStream errorStream = con2.getErrorStream();
        this.is = errorStream;
        if (errorStream == null) {
            this.is = con2.getInputStream();
        }
        if (this.is != null && "gzip".equals(con2.getContentEncoding())) {
            this.is = new GZIPInputStream(this.is);
        }
    }

    Response(String content) {
        this.responseAsString = content;
    }

    public int getStatusCode() {
        return this.statusCode;
    }

    public String getResponseHeader(String name) {
        return this.con.getHeaderField(name);
    }

    public InputStream asStream() {
        if (!this.streamConsumed) {
            return this.is;
        }
        throw new IllegalStateException("Stream has already been consumed.");
    }

    public String asString() throws TwitterException {
        if (this.responseAsString == null) {
            try {
                InputStream stream = asStream();
                if (stream == null) {
                    return null;
                }
                BufferedReader br = new BufferedReader(new InputStreamReader(stream, "UTF-8"));
                StringBuffer buf = new StringBuffer();
                while (true) {
                    String line = br.readLine();
                    if (line == null) {
                        break;
                    }
                    buf.append(line).append("\n");
                }
                this.responseAsString = buf.toString();
                if (Configuration.isDalvik()) {
                    this.responseAsString = unescape(this.responseAsString);
                }
                log(this.responseAsString);
                stream.close();
                this.con.disconnect();
                this.streamConsumed = true;
            } catch (NullPointerException npe) {
                throw new TwitterException(npe.getMessage(), npe);
            } catch (IOException ioe) {
                throw new TwitterException(ioe.getMessage(), ioe);
            }
        }
        return this.responseAsString;
    }

    public Document asDocument() throws TwitterException {
        if (this.responseAsDocument == null) {
            try {
                this.responseAsDocument = builders.get().parse(new ByteArrayInputStream(asString().getBytes("UTF-8")));
            } catch (SAXException saxe) {
                throw new TwitterException(new StringBuffer().append("The response body was not well-formed:\n").append(this.responseAsString).toString(), saxe);
            } catch (IOException ioe) {
                throw new TwitterException("There's something with the connection.", ioe);
            }
        }
        return this.responseAsDocument;
    }

    public JSONObject asJSONObject() throws TwitterException {
        try {
            return new JSONObject(asString());
        } catch (JSONException jsone) {
            throw new TwitterException(new StringBuffer().append(jsone.getMessage()).append(":").append(this.responseAsString).toString(), jsone);
        }
    }

    public JSONArray asJSONArray() throws TwitterException {
        try {
            return new JSONArray(asString());
        } catch (JSONException jsone) {
            throw new TwitterException(new StringBuffer().append(jsone.getMessage()).append(":").append(this.responseAsString).toString(), jsone);
        }
    }

    public InputStreamReader asReader() {
        try {
            return new InputStreamReader(this.is, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return new InputStreamReader(this.is);
        }
    }

    public void disconnect() {
        this.con.disconnect();
    }

    public static String unescape(String original) {
        Matcher mm = escaped.matcher(original);
        StringBuffer unescaped = new StringBuffer();
        while (mm.find()) {
            mm.appendReplacement(unescaped, Character.toString((char) Integer.parseInt(mm.group(1), 10)));
        }
        mm.appendTail(unescaped);
        return unescaped.toString();
    }

    public String toString() {
        if (this.responseAsString != null) {
            return this.responseAsString;
        }
        return new StringBuffer().append("Response{statusCode=").append(this.statusCode).append(", response=").append(this.responseAsDocument).append(", responseString='").append(this.responseAsString).append('\'').append(", is=").append(this.is).append(", con=").append(this.con).append('}').toString();
    }

    private void log(String message) {
        if (DEBUG) {
            System.out.println(new StringBuffer().append("[").append(new Date()).append("]").append(message).toString());
        }
    }

    private void log(String message, String message2) {
        if (DEBUG) {
            log(new StringBuffer().append(message).append(message2).toString());
        }
    }
}
