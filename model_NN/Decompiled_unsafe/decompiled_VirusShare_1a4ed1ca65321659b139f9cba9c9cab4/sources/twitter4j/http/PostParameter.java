package twitter4j.http;

import java.io.Serializable;

public class PostParameter implements Serializable, Comparable {
    private static final long serialVersionUID = -8708108746980739212L;
    String name;
    String value;

    public PostParameter(String name2, String value2) {
        this.name = name2;
        this.value = value2;
    }

    public PostParameter(String name2, double value2) {
        this.name = name2;
        this.value = String.valueOf(value2);
    }

    public PostParameter(String name2, int value2) {
        this.name = name2;
        this.value = String.valueOf(value2);
    }

    public String getName() {
        return this.name;
    }

    public String getValue() {
        return this.value;
    }

    public int hashCode() {
        return (this.name.hashCode() * 31) + this.value.hashCode();
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof PostParameter)) {
            return false;
        }
        PostParameter that = (PostParameter) obj;
        if (!this.name.equals(that.name) || !this.value.equals(that.value)) {
            z = false;
        }
        return z;
    }

    public int compareTo(Object o) {
        PostParameter that = (PostParameter) o;
        int compared = this.name.compareTo(that.name);
        if (compared == 0) {
            return this.value.compareTo(that.value);
        }
        return compared;
    }
}
