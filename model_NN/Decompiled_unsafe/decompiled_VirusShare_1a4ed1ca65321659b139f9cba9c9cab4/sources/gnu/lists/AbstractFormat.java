package gnu.lists;

import gnu.mapping.CharArrayOutPort;
import gnu.mapping.OutPort;
import java.io.Writer;
import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;

public abstract class AbstractFormat extends Format {
    public abstract void writeObject(Object obj, Consumer consumer);

    /* access modifiers changed from: protected */
    public void write(String str, Consumer out) {
        out.write(str);
    }

    public void write(int v, Consumer out) {
        out.write(v);
    }

    public void writeLong(long v, Consumer out) {
        out.writeLong(v);
    }

    public void writeInt(int i, Consumer out) {
        writeLong((long) i, out);
    }

    public void writeBoolean(boolean v, Consumer out) {
        out.writeBoolean(v);
    }

    public void startElement(Object type, Consumer out) {
        write("(", out);
        write(type.toString(), out);
        write(" ", out);
    }

    public void endElement(Consumer out) {
        write(")", out);
    }

    public void startAttribute(Object attrType, Consumer out) {
        write(attrType.toString(), out);
        write(": ", out);
    }

    public void endAttribute(Consumer out) {
        write(" ", out);
    }

    /* JADX INFO: finally extract failed */
    public void format(Object value, Consumer out) {
        if (out instanceof OutPort) {
            OutPort pout = (OutPort) out;
            AbstractFormat saveFormat = pout.objectFormat;
            try {
                pout.objectFormat = this;
                out.writeObject(value);
                pout.objectFormat = saveFormat;
            } catch (Throwable th) {
                pout.objectFormat = saveFormat;
                throw th;
            }
        } else {
            out.writeObject(value);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: gnu.lists.AbstractFormat.writeObject(java.lang.Object, gnu.lists.Consumer):void
     arg types: [java.lang.Object, gnu.lists.PrintConsumer]
     candidates:
      gnu.lists.AbstractFormat.writeObject(java.lang.Object, gnu.lists.PrintConsumer):void
      gnu.lists.AbstractFormat.writeObject(java.lang.Object, java.io.Writer):void
      gnu.lists.AbstractFormat.writeObject(java.lang.Object, gnu.lists.Consumer):void */
    public final void writeObject(Object obj, PrintConsumer out) {
        writeObject(obj, (Consumer) out);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: gnu.mapping.OutPort.<init>(java.io.Writer, boolean, boolean):void
     arg types: [java.io.Writer, int, int]
     candidates:
      gnu.mapping.OutPort.<init>(java.io.Writer, gnu.text.PrettyWriter, boolean):void
      gnu.mapping.OutPort.<init>(java.io.Writer, boolean, gnu.text.Path):void
      gnu.mapping.OutPort.<init>(java.io.Writer, boolean, boolean):void */
    public final void writeObject(Object obj, Writer out) {
        if (out instanceof Consumer) {
            writeObject(obj, (Consumer) out);
            return;
        }
        OutPort port = new OutPort(out, false, true);
        writeObject(obj, (Consumer) out);
        port.close();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: gnu.lists.AbstractFormat.writeObject(java.lang.Object, gnu.lists.PrintConsumer):void
     arg types: [java.lang.Object, gnu.mapping.CharArrayOutPort]
     candidates:
      gnu.lists.AbstractFormat.writeObject(java.lang.Object, gnu.lists.Consumer):void
      gnu.lists.AbstractFormat.writeObject(java.lang.Object, java.io.Writer):void
      gnu.lists.AbstractFormat.writeObject(java.lang.Object, gnu.lists.PrintConsumer):void */
    public StringBuffer format(Object val, StringBuffer sbuf, FieldPosition fpos) {
        CharArrayOutPort out = new CharArrayOutPort();
        writeObject(val, (PrintConsumer) out);
        sbuf.append(out.toCharArray());
        out.close();
        return sbuf;
    }

    public Object parseObject(String text, ParsePosition status) {
        throw new Error(getClass().getName() + ".parseObject - not implemented");
    }
}
