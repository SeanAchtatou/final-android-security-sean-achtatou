package gnu.kawa.functions;

import gnu.expr.Language;
import gnu.mapping.Procedure;
import gnu.mapping.Procedure1;
import gnu.mapping.PropertySet;

public class Not extends Procedure1 {
    Language language;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: gnu.mapping.LazyPropertyKey.set(gnu.mapping.PropertySet, java.lang.String):void
     arg types: [gnu.kawa.functions.Not, java.lang.String]
     candidates:
      gnu.mapping.PropertyKey.set(gnu.mapping.PropertySet, ?):void
      gnu.mapping.LazyPropertyKey.set(gnu.mapping.PropertySet, java.lang.String):void */
    public Not(Language language2) {
        this.language = language2;
        setProperty(Procedure.validateApplyKey, "gnu.kawa.functions.CompileMisc:validateApplyNot");
        Procedure.compilerKey.set((PropertySet) this, "*gnu.kawa.functions.CompileMisc:forNot");
    }

    public Not(Language language2, String name) {
        this(language2);
        setName(name);
    }

    public Object apply1(Object arg1) {
        return this.language.booleanObject(!this.language.isTrue(arg1));
    }
}
