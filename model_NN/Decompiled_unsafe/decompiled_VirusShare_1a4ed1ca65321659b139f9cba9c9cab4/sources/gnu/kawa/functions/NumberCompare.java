package gnu.kawa.functions;

import gnu.bytecode.CodeAttr;
import gnu.bytecode.Label;
import gnu.bytecode.Type;
import gnu.expr.ApplyExp;
import gnu.expr.Compilation;
import gnu.expr.ConditionalTarget;
import gnu.expr.Expression;
import gnu.expr.IfExp;
import gnu.expr.Inlineable;
import gnu.expr.Language;
import gnu.expr.PrimProcedure;
import gnu.expr.QuoteExp;
import gnu.expr.ReferenceExp;
import gnu.expr.StackTarget;
import gnu.expr.Target;
import gnu.mapping.Procedure;
import gnu.mapping.ProcedureN;
import gnu.math.IntNum;

public class NumberCompare extends ProcedureN implements Inlineable {
    static final int RESULT_EQU = 0;
    static final int RESULT_GRT = 1;
    static final int RESULT_LSS = -1;
    static final int RESULT_NAN = -2;
    static final int RESULT_NEQ = -3;
    public static final int TRUE_IF_EQU = 8;
    public static final int TRUE_IF_GRT = 16;
    public static final int TRUE_IF_LSS = 4;
    public static final int TRUE_IF_NAN = 2;
    public static final int TRUE_IF_NEQ = 1;
    int flags;
    Language language;

    public int numArgs() {
        return -4094;
    }

    public static boolean $Eq(Object arg1, Object arg2) {
        return apply2(8, arg1, arg2);
    }

    public static boolean $Gr(Object arg1, Object arg2) {
        return apply2(16, arg1, arg2);
    }

    public static boolean $Gr$Eq(Object arg1, Object arg2) {
        return apply2(24, arg1, arg2);
    }

    public static boolean $Ls(Object arg1, Object arg2) {
        return apply2(4, arg1, arg2);
    }

    public static boolean $Ls$Eq(Object arg1, Object arg2) {
        return apply2(12, arg1, arg2);
    }

    public static boolean $Eq$V(Object arg1, Object arg2, Object arg3, Object[] rest) {
        if (!$Eq(arg1, arg2) || !$Eq(arg2, arg3)) {
            return false;
        }
        return rest.length == 0 || ($Eq(arg3, rest[0]) && applyN(8, rest));
    }

    public static boolean $Gr$V(Object arg1, Object arg2, Object arg3, Object[] rest) {
        if (!$Gr(arg1, arg2) || !$Gr(arg2, arg3)) {
            return false;
        }
        return rest.length == 0 || ($Gr(arg3, rest[0]) && applyN(16, rest));
    }

    public static boolean $Gr$Eq$V(Object arg1, Object arg2, Object arg3, Object[] rest) {
        if (!$Gr$Eq(arg1, arg2) || !$Gr$Eq(arg2, arg3)) {
            return false;
        }
        return rest.length == 0 || ($Gr$Eq(arg3, rest[0]) && applyN(24, rest));
    }

    public static boolean $Ls$V(Object arg1, Object arg2, Object arg3, Object[] rest) {
        if (!$Ls(arg1, arg2) || !$Ls(arg2, arg3)) {
            return false;
        }
        return rest.length == 0 || ($Ls(arg3, rest[0]) && applyN(4, rest));
    }

    public static boolean $Ls$Eq$V(Object arg1, Object arg2, Object arg3, Object[] rest) {
        if (!$Ls$Eq(arg1, arg2) || !$Ls$Eq(arg2, arg3)) {
            return false;
        }
        return rest.length == 0 || ($Ls$Eq(arg3, rest[0]) && applyN(12, rest));
    }

    public static NumberCompare make(Language language2, String name, int flags2) {
        NumberCompare proc = new NumberCompare();
        proc.language = language2;
        proc.setName(name);
        proc.flags = flags2;
        proc.setProperty(Procedure.validateApplyKey, "gnu.kawa.functions.CompileArith:validateApplyNumberCompare");
        return proc;
    }

    /* access modifiers changed from: protected */
    public final Language getLanguage() {
        return this.language;
    }

    public Object apply2(Object arg1, Object arg2) {
        return getLanguage().booleanObject(apply2(this.flags, arg1, arg2));
    }

    public static boolean apply2(int flags2, Object arg1, Object arg2) {
        return ((1 << (compare(arg1, arg2, true) + 3)) & flags2) != 0;
    }

    public static boolean checkCompareCode(int code, int flags2) {
        return ((1 << (code + 3)) & flags2) != 0;
    }

    public static boolean applyWithPromotion(int flags2, Object arg1, Object arg2) {
        return checkCompareCode(compare(arg1, arg2, false), flags2);
    }

    public static int compare(Object arg1, Object arg2, boolean exact) {
        return compare(arg1, Arithmetic.classifyValue(arg1), arg2, Arithmetic.classifyValue(arg2), exact);
    }

    /* JADX WARNING: Removed duplicated region for block: B:48:0x00ca A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x00cd  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int compare(java.lang.Object r24, int r25, java.lang.Object r26, int r27, boolean r28) {
        /*
            if (r25 < 0) goto L_0x0004
            if (r27 >= 0) goto L_0x0006
        L_0x0004:
            r7 = -3
        L_0x0005:
            return r7
        L_0x0006:
            r0 = r25
            r1 = r27
            if (r0 >= r1) goto L_0x001e
            r6 = r27
        L_0x000e:
            switch(r6) {
                case 1: goto L_0x0021;
                case 2: goto L_0x0033;
                case 3: goto L_0x0049;
                case 4: goto L_0x0056;
                case 5: goto L_0x0063;
                case 6: goto L_0x0070;
                case 7: goto L_0x007d;
                case 8: goto L_0x00ac;
                case 9: goto L_0x00ac;
                default: goto L_0x0011;
            }
        L_0x0011:
            gnu.math.Numeric r20 = gnu.kawa.functions.Arithmetic.asNumeric(r24)
            gnu.math.Numeric r21 = gnu.kawa.functions.Arithmetic.asNumeric(r26)
            int r7 = r20.compare(r21)
            goto L_0x0005
        L_0x001e:
            r6 = r25
            goto L_0x000e
        L_0x0021:
            int r14 = gnu.kawa.functions.Arithmetic.asInt(r24)
            int r15 = gnu.kawa.functions.Arithmetic.asInt(r26)
            if (r14 >= r15) goto L_0x002d
            r7 = -1
        L_0x002c:
            goto L_0x0005
        L_0x002d:
            if (r14 <= r15) goto L_0x0031
            r7 = 1
            goto L_0x002c
        L_0x0031:
            r7 = 0
            goto L_0x002c
        L_0x0033:
            long r16 = gnu.kawa.functions.Arithmetic.asLong(r24)
            long r18 = gnu.kawa.functions.Arithmetic.asLong(r26)
            int r22 = (r16 > r18 ? 1 : (r16 == r18 ? 0 : -1))
            if (r22 >= 0) goto L_0x0041
            r7 = -1
        L_0x0040:
            goto L_0x0005
        L_0x0041:
            int r22 = (r16 > r18 ? 1 : (r16 == r18 ? 0 : -1))
            if (r22 <= 0) goto L_0x0047
            r7 = 1
            goto L_0x0040
        L_0x0047:
            r7 = 0
            goto L_0x0040
        L_0x0049:
            java.math.BigInteger r4 = gnu.kawa.functions.Arithmetic.asBigInteger(r24)
            java.math.BigInteger r5 = gnu.kawa.functions.Arithmetic.asBigInteger(r26)
            int r7 = r4.compareTo(r5)
            goto L_0x0005
        L_0x0056:
            gnu.math.IntNum r22 = gnu.kawa.functions.Arithmetic.asIntNum(r24)
            gnu.math.IntNum r23 = gnu.kawa.functions.Arithmetic.asIntNum(r26)
            int r7 = gnu.math.IntNum.compare(r22, r23)
            goto L_0x0005
        L_0x0063:
            java.math.BigDecimal r2 = gnu.kawa.functions.Arithmetic.asBigDecimal(r24)
            java.math.BigDecimal r3 = gnu.kawa.functions.Arithmetic.asBigDecimal(r26)
            int r7 = r2.compareTo(r3)
            goto L_0x0005
        L_0x0070:
            gnu.math.RatNum r22 = gnu.kawa.functions.Arithmetic.asRatNum(r24)
            gnu.math.RatNum r23 = gnu.kawa.functions.Arithmetic.asRatNum(r26)
            int r7 = gnu.math.RatNum.compare(r22, r23)
            goto L_0x0005
        L_0x007d:
            if (r28 == 0) goto L_0x008f
            r22 = 6
            r0 = r25
            r1 = r22
            if (r0 <= r1) goto L_0x00ac
            r22 = 6
            r0 = r27
            r1 = r22
            if (r0 <= r1) goto L_0x00ac
        L_0x008f:
            float r12 = gnu.kawa.functions.Arithmetic.asFloat(r24)
            float r13 = gnu.kawa.functions.Arithmetic.asFloat(r26)
            int r22 = (r12 > r13 ? 1 : (r12 == r13 ? 0 : -1))
            if (r22 <= 0) goto L_0x009e
            r7 = 1
        L_0x009c:
            goto L_0x0005
        L_0x009e:
            int r22 = (r12 > r13 ? 1 : (r12 == r13 ? 0 : -1))
            if (r22 >= 0) goto L_0x00a4
            r7 = -1
            goto L_0x009c
        L_0x00a4:
            int r22 = (r12 > r13 ? 1 : (r12 == r13 ? 0 : -1))
            if (r22 != 0) goto L_0x00aa
            r7 = 0
            goto L_0x009c
        L_0x00aa:
            r7 = -2
            goto L_0x009c
        L_0x00ac:
            if (r28 == 0) goto L_0x00be
            r22 = 6
            r0 = r25
            r1 = r22
            if (r0 <= r1) goto L_0x0011
            r22 = 6
            r0 = r27
            r1 = r22
            if (r0 <= r1) goto L_0x0011
        L_0x00be:
            double r8 = gnu.kawa.functions.Arithmetic.asDouble(r24)
            double r10 = gnu.kawa.functions.Arithmetic.asDouble(r26)
            int r22 = (r8 > r10 ? 1 : (r8 == r10 ? 0 : -1))
            if (r22 <= 0) goto L_0x00cd
            r7 = 1
        L_0x00cb:
            goto L_0x0005
        L_0x00cd:
            int r22 = (r8 > r10 ? 1 : (r8 == r10 ? 0 : -1))
            if (r22 >= 0) goto L_0x00d3
            r7 = -1
            goto L_0x00cb
        L_0x00d3:
            int r22 = (r8 > r10 ? 1 : (r8 == r10 ? 0 : -1))
            if (r22 != 0) goto L_0x00d9
            r7 = 0
            goto L_0x00cb
        L_0x00d9:
            r7 = -2
            goto L_0x00cb
        */
        throw new UnsupportedOperationException("Method not decompiled: gnu.kawa.functions.NumberCompare.compare(java.lang.Object, int, java.lang.Object, int, boolean):int");
    }

    static boolean applyN(int flags2, Object[] args) {
        for (int i = 0; i < args.length - 1; i++) {
            if (!apply2(flags2, args[i], args[i + 1])) {
                return false;
            }
        }
        return true;
    }

    public Object applyN(Object[] args) {
        return getLanguage().booleanObject(applyN(this.flags, args));
    }

    public void compile(ApplyExp exp, Compilation comp, Target target) {
        Type commonType;
        int opcode;
        Expression[] args = exp.getArgs();
        if (args.length == 2) {
            Expression arg0 = args[0];
            Expression arg1 = args[1];
            int kind0 = classify(arg0);
            int kind1 = classify(arg1);
            CodeAttr code = comp.getCode();
            if (kind0 > 0 && kind1 > 0 && kind0 <= 10 && kind1 <= 10 && !(kind0 == 6 && kind1 == 6)) {
                if (!(target instanceof ConditionalTarget)) {
                    IfExp.compile(exp, QuoteExp.trueExp, QuoteExp.falseExp, comp, target);
                    return;
                }
                int mask = this.flags;
                if (mask == 1) {
                    mask = 20;
                }
                if (kind0 <= 4 && kind1 <= 4 && (kind0 > 2 || kind1 > 2)) {
                    Type[] ctypes = new Type[2];
                    ctypes[0] = Arithmetic.typeIntNum;
                    if (kind1 <= 2) {
                        ctypes[1] = Type.longType;
                    } else if (kind0 > 2 || (!(arg0 instanceof QuoteExp) && !(arg1 instanceof QuoteExp) && !(arg0 instanceof ReferenceExp) && !(arg1 instanceof ReferenceExp))) {
                        ctypes[1] = Arithmetic.typeIntNum;
                    } else {
                        ctypes[1] = Type.longType;
                        args = new Expression[]{arg1, arg0};
                        if (!(mask == 8 || mask == 20)) {
                            mask ^= 20;
                        }
                    }
                    arg0 = new ApplyExp(new PrimProcedure(Arithmetic.typeIntNum.getMethod("compare", ctypes)), args);
                    arg1 = new QuoteExp(IntNum.zero());
                    kind1 = 1;
                    kind0 = 1;
                }
                if (kind0 <= 1 && kind1 <= 1) {
                    commonType = Type.intType;
                } else if (kind0 > 2 || kind1 > 2) {
                    commonType = Type.doubleType;
                } else {
                    commonType = Type.longType;
                }
                StackTarget stackTarget = new StackTarget(commonType);
                ConditionalTarget ctarget = (ConditionalTarget) target;
                if ((arg0 instanceof QuoteExp) && !(arg1 instanceof QuoteExp)) {
                    Expression tmp = arg1;
                    arg1 = arg0;
                    arg0 = tmp;
                    if (!(mask == 8 || mask == 20)) {
                        mask ^= 20;
                    }
                }
                Label label1 = ctarget.trueBranchComesFirst ? ctarget.ifFalse : ctarget.ifTrue;
                if (ctarget.trueBranchComesFirst) {
                    mask ^= 28;
                }
                switch (mask) {
                    case 4:
                        opcode = 155;
                        break;
                    case 8:
                        opcode = 153;
                        break;
                    case 12:
                        opcode = 158;
                        break;
                    case 16:
                        opcode = 157;
                        break;
                    case 20:
                        opcode = 154;
                        break;
                    case 24:
                        opcode = 156;
                        break;
                    default:
                        opcode = 0;
                        break;
                }
                arg0.compile(comp, stackTarget);
                if (kind0 <= 1 && kind1 <= 1 && (arg1 instanceof QuoteExp)) {
                    Object value = ((QuoteExp) arg1).getValue();
                    if ((value instanceof IntNum) && ((IntNum) value).isZero()) {
                        code.emitGotoIfCompare1(label1, opcode);
                        ctarget.emitGotoFirstBranch(code);
                        return;
                    }
                }
                arg1.compile(comp, stackTarget);
                code.emitGotoIfCompare2(label1, opcode);
                ctarget.emitGotoFirstBranch(code);
                return;
            }
        }
        ApplyExp.compile(exp, comp, target);
    }

    static int classify(Expression exp) {
        int kind = Arithmetic.classifyType(exp.getType());
        if (kind != 4 || !(exp instanceof QuoteExp)) {
            return kind;
        }
        Object value = ((QuoteExp) exp).getValue();
        if (!(value instanceof IntNum)) {
            return kind;
        }
        int ilength = ((IntNum) value).intLength();
        if (ilength < 32) {
            return 1;
        }
        if (ilength < 64) {
            return 2;
        }
        return kind;
    }

    public Type getReturnType(Expression[] args) {
        return Type.booleanType;
    }
}
