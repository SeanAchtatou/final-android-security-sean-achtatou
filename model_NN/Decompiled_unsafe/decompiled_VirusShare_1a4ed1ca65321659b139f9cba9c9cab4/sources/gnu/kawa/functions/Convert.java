package gnu.kawa.functions;

import gnu.bytecode.Type;
import gnu.mapping.Procedure;
import gnu.mapping.Procedure2;
import gnu.mapping.PropertySet;

public class Convert extends Procedure2 {
    public static final Convert as = new Convert();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: gnu.mapping.LazyPropertyKey.set(gnu.mapping.PropertySet, java.lang.String):void
     arg types: [gnu.kawa.functions.Convert, java.lang.String]
     candidates:
      gnu.mapping.PropertyKey.set(gnu.mapping.PropertySet, ?):void
      gnu.mapping.LazyPropertyKey.set(gnu.mapping.PropertySet, java.lang.String):void */
    static {
        as.setName("as");
        as.setProperty(Procedure.validateApplyKey, "gnu.kawa.functions.CompileMisc:validateApplyConvert");
        Procedure.compilerKey.set((PropertySet) as, "*gnu.kawa.functions.CompileMisc:forConvert");
    }

    public static Convert getInstance() {
        return as;
    }

    public Object apply2(Object arg1, Object arg2) {
        Type type;
        if (arg1 instanceof Class) {
            type = Type.make((Class) arg1);
        } else {
            type = (Type) arg1;
        }
        return type.coerceFromObject(arg2);
    }
}
