package gnu.kawa.servlet;

import gnu.expr.Compilation;
import gnu.expr.Language;
import gnu.expr.ModuleBody;
import gnu.expr.ModuleContext;
import gnu.expr.ModuleExp;
import gnu.expr.ModuleInfo;
import gnu.expr.ModuleManager;
import gnu.kawa.xml.ElementType;
import gnu.mapping.CallContext;
import gnu.mapping.Environment;
import gnu.mapping.InPort;
import gnu.text.Path;
import gnu.text.SourceMessages;
import gnu.text.SyntaxException;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.Hashtable;
import twitter4j.org.json.HTTP;

public class KawaAutoHandler {
    static final String MODULE_MAP_ATTRIBUTE = "gnu.kawa.module-map";

    public static void run(HttpRequestContext hctx, CallContext ctx) throws Throwable {
        Object mod = getModule(hctx, ctx, hctx.getRequestParameter("qexo-save-class") != null);
        if (mod instanceof ModuleBody) {
            ((ModuleBody) mod).run(ctx);
        }
    }

    public static Object getModule(HttpRequestContext hctx, CallContext ctx, boolean saveClass) throws Exception {
        URL url;
        Compilation comp;
        String path = hctx.getRequestPath().substring(hctx.getContextPath().length() - 1);
        Hashtable mmap = (Hashtable) hctx.getAttribute(MODULE_MAP_ATTRIBUTE);
        if (mmap == null) {
            mmap = new Hashtable();
            hctx.setAttribute(MODULE_MAP_ATTRIBUTE, mmap);
        }
        ModuleContext mcontext = (ModuleContext) hctx.getAttribute("gnu.kawa.module-context");
        if (mcontext == null) {
            mcontext = ModuleContext.getContext();
        }
        mcontext.addFlags(ModuleContext.IN_HTTP_SERVER);
        if (hctx.getClass().getName().endsWith("KawaServlet$Context")) {
            mcontext.addFlags(ModuleContext.IN_SERVLET);
        }
        ModuleInfo minfo = (ModuleInfo) mmap.get(path);
        long now = System.currentTimeMillis();
        ModuleManager mmanager = mcontext.getManager();
        if (minfo != null && now - minfo.lastCheckedTime < mmanager.lastModifiedCacheTime) {
            return mcontext.findInstance(minfo);
        }
        int plen = path.length();
        if (plen == 0 || path.charAt(plen - 1) == '/') {
            url = null;
        } else {
            url = hctx.getResourceURL(path);
        }
        String upath = path;
        if (url == null) {
            String xpath = path;
            while (true) {
                int sl = xpath.lastIndexOf(47);
                if (sl >= 0) {
                    xpath = xpath.substring(0, sl);
                    upath = xpath + "/+default+";
                    url = hctx.getResourceURL(upath);
                    if (url != null) {
                        hctx.setScriptAndLocalPath(path.substring(1, sl + 1), path.substring(sl + 1));
                        break;
                    }
                } else {
                    break;
                }
            }
        } else {
            hctx.setScriptAndLocalPath(path, ElementType.MATCH_ANY_LOCALNAME);
        }
        if (url == null) {
            byte[] bmsg = ("The requested URL " + path + " was not found on this server." + " res/:" + hctx.getResourceURL("/") + HTTP.CRLF).getBytes();
            hctx.sendResponseHeaders(404, null, (long) bmsg.length);
            try {
                hctx.getResponseStream().write(bmsg);
                return null;
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        } else {
            String urlString = url.toExternalForm();
            if (minfo == null || !urlString.equals(minfo.getSourceAbsPathname())) {
                minfo = mmanager.findWithURL(url);
            }
            if (minfo.checkCurrent(mmanager, now)) {
                return mcontext.findInstance(minfo);
            }
            mmap.put(path, minfo);
            Path absPath = minfo.getSourceAbsPath();
            InputStream resourceStream = absPath.openInputStream();
            if (!(resourceStream instanceof BufferedInputStream)) {
                resourceStream = new BufferedInputStream(resourceStream);
            }
            Language language = Language.getInstanceFromFilenameExtension(path);
            if (language != null) {
                hctx.log("Compile " + path + " - a " + language.getName() + " source file (based on extension)");
            } else {
                language = Language.detect(resourceStream);
                if (language != null) {
                    hctx.log("Compile " + path + " - a " + language.getName() + " source file (detected from content)");
                } else if (path != upath) {
                    byte[] bmsg2 = ("The requested URL " + path + " was not found on this server." + " upath=" + upath + ".\r\n").getBytes();
                    hctx.sendResponseHeaders(404, null, (long) bmsg2.length);
                    try {
                        hctx.getResponseStream().write(bmsg2);
                        return null;
                    } catch (IOException ex2) {
                        throw new RuntimeException(ex2);
                    }
                } else {
                    hctx.sendResponseHeaders(HttpRequestContext.HTTP_OK, null, absPath.getContentLength());
                    OutputStream out = hctx.getResponseStream();
                    byte[] buffer = new byte[4096];
                    while (true) {
                        int n = resourceStream.read(buffer);
                        if (n < 0) {
                            resourceStream.close();
                            out.close();
                            return null;
                        }
                        out.write(buffer, 0, n);
                    }
                }
            }
            InPort inPort = new InPort(resourceStream, absPath);
            Language.setCurrentLanguage(language);
            SourceMessages messages = new SourceMessages();
            try {
                comp = language.parse(inPort, messages, 9, minfo);
            } catch (SyntaxException ex3) {
                if (ex3.getMessages() != messages) {
                    throw ex3;
                }
                comp = null;
            }
            Class cl = null;
            if (!messages.seenErrors()) {
                ModuleExp module = comp.getModule();
                cl = (Class) ModuleExp.evalModule1(Environment.getCurrent(), comp, url, null);
            }
            if (messages.seenErrors()) {
                ((ServletPrinter) ctx.consumer).addHeader("Content-type", "text/plain");
                hctx.sendResponseHeaders(500, "Syntax errors", -1);
                ctx.consumer.write("script syntax error:\n" + messages.toString(20));
                minfo.cleanupAfterCompilation();
                return null;
            }
            minfo.setModuleClass(cl);
            return mcontext.findInstance(minfo);
        }
    }
}
