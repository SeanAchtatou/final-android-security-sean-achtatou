package com.airpush.android;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.provider.Browser;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.WindowManager;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import org.apache.http.HttpEntity;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHttpResponse;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Airpush {
    private static int A;
    private static DefaultHttpClient B;
    private static BasicHttpResponse C;
    private static HttpEntity D;
    private static boolean E;
    private static boolean O;
    private static boolean Q;
    protected static Context a = null;
    private static String b = null;
    private static String c = null;
    private static String d = null;
    private static boolean e = false;
    private static int g = 17301620;
    /* access modifiers changed from: private */
    public static long k = 0;
    private static List w;
    private static HttpPost x;
    private static BasicHttpParams y;
    private static int z;
    private Intent F;
    private int G;
    private JSONObject H;
    private String I;
    private String J;
    private String[] K = null;
    private String[] L = null;
    private JSONObject M;
    private boolean N = true;
    private Intent P;
    private Runnable R = new a(this);
    private Runnable S = new b(this);
    private String f;
    private boolean h;
    private long i = 0;
    private long j = 0;
    private HttpEntity l;
    private String m;
    private JSONArray n;
    private String o;
    private String p;
    private String q;
    private String[] r;
    private String[] s;
    private String[] t;
    private Bitmap u;
    private InputStream v;

    public Airpush() {
    }

    public Airpush(Context context, String str, String str2) {
        try {
            Log.i("AirpushSDK", "Airpush 1 Push Service doPush...." + Q);
            a = context;
            new SetPreferences().a(a, str, str2, e, true, true, false);
            d();
            a(str, str2, e, false, g, true);
        } catch (Exception e2) {
        }
    }

    public Airpush(Context context, String str, String str2, boolean z2, boolean z3, boolean z4) {
        try {
            E = z2;
            e = z2;
            a = context;
            O = z4;
            Q = z3;
            Log.i("AirpushSDK", "Push Service doPush...." + Q);
            Log.i("AirpushSDK", "Push Service doSearch...." + O);
            new SetPreferences().a(a, str, str2, z2, O, E, Q);
            d();
            a(str, str2, e, false, g, true);
        } catch (Exception e2) {
        }
    }

    private static Bitmap a(String str) {
        try {
            URLConnection openConnection = new URL(str).openConnection();
            openConnection.connect();
            InputStream inputStream = openConnection.getInputStream();
            BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
            Bitmap decodeStream = BitmapFactory.decodeStream(bufferedInputStream);
            bufferedInputStream.close();
            inputStream.close();
            return decodeStream;
        } catch (Exception e2) {
            Log.i("AirpushSDK", "Error in Adimage fetching Please try again later.");
            return null;
        }
    }

    private static InputStream a(String str, List list) {
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(String.valueOf(str) + "").openConnection();
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            httpURLConnection.setConnectTimeout(20000);
            httpURLConnection.setReadTimeout(20000);
            httpURLConnection.setUseCaches(false);
            httpURLConnection.setDefaultUseCaches(false);
            httpURLConnection.connect();
            if (httpURLConnection.getResponseCode() == 200) {
                return httpURLConnection.getInputStream();
            }
        } catch (Exception e2) {
            Log.i("AirpushSDK", "Network Error, please try again later");
        }
        return null;
    }

    private String a(JSONObject jSONObject) {
        try {
            this.o = jSONObject.getString("iconimage");
            return this.o;
        } catch (JSONException e2) {
            return "Not Found";
        }
    }

    protected static void a(Context context, long j2) {
        Log.i("AirpushSDK", "SDK will restart in " + j2 + " ms.");
        a = context;
        d();
        try {
            Intent intent = new Intent(context, UserDetailsReceiver.class);
            intent.setAction("SetUserInfo");
            intent.putExtra("appId", b);
            intent.putExtra("imei", c);
            intent.putExtra("apikey", d);
            ((AlarmManager) context.getSystemService("alarm")).set(0, System.currentTimeMillis() + (1000 * j2 * 60), PendingIntent.getBroadcast(context, 0, intent, 0));
            Intent intent2 = new Intent(context, MessageReceiver.class);
            intent2.setAction("SetMessageReceiver");
            intent2.putExtra("appId", b);
            intent2.putExtra("imei", c);
            intent2.putExtra("apikey", d);
            intent2.putExtra("testMode", e);
            intent2.putExtra("icon", g);
            intent2.putExtra("icontestmode", E);
            ((AlarmManager) context.getSystemService("alarm")).setInexactRepeating(0, System.currentTimeMillis() + j2 + ((long) d.b.intValue()), d.a, PendingIntent.getBroadcast(context, 0, intent2, 0));
        } catch (Exception e2) {
        }
    }

    static /* synthetic */ void a(Airpush airpush) {
        boolean z2 = true;
        try {
            if (a.getSharedPreferences("airpushTimePref", 1) != null) {
                airpush.i = System.currentTimeMillis();
                SharedPreferences sharedPreferences = a.getSharedPreferences("airpushTimePref", 1);
                if (sharedPreferences.contains("startTime")) {
                    airpush.j = sharedPreferences.getLong("startTime", 0);
                    long j2 = (airpush.i - airpush.j) / 60000;
                    k = j2;
                    if (j2 < ((long) d.c.intValue())) {
                        new Handler().post(airpush.S);
                        z2 = false;
                    }
                } else {
                    SharedPreferences.Editor edit = a.getSharedPreferences("airpushTimePref", 2).edit();
                    airpush.j = System.currentTimeMillis();
                    edit.putLong("startTime", airpush.j);
                    edit.commit();
                }
            }
            if (z2) {
                Intent intent = new Intent(a, UserDetailsReceiver.class);
                intent.setAction("SetUserInfo");
                intent.putExtra("appId", b);
                intent.putExtra("imei", c);
                intent.putExtra("apikey", d);
                ((AlarmManager) a.getSystemService("alarm")).set(0, System.currentTimeMillis(), PendingIntent.getBroadcast(a, 0, intent, 0));
                Intent intent2 = new Intent(a, MessageReceiver.class);
                intent2.setAction("SetMessageReceiver");
                intent2.putExtra("appId", b);
                intent2.putExtra("imei", c);
                intent2.putExtra("apikey", d);
                intent2.putExtra("testMode", e);
                intent2.putExtra("icon", g);
                intent2.putExtra("icontestmode", E);
                intent2.putExtra("doSearch", O);
                intent2.putExtra("doPush", Q);
                ((AlarmManager) a.getSystemService("alarm")).setInexactRepeating(0, System.currentTimeMillis() + ((long) d.b.intValue()), d.a, PendingIntent.getBroadcast(a, 0, intent2, 0));
            }
        } catch (Exception e2) {
        }
    }

    public static boolean a(Context context) {
        if (context.getSharedPreferences("sdkPrefs", 1).equals(null)) {
            return true;
        }
        SharedPreferences sharedPreferences = context.getSharedPreferences("sdkPrefs", 1);
        if (sharedPreferences.contains("SDKEnabled")) {
            return sharedPreferences.getBoolean("SDKEnabled", false);
        }
        return true;
    }

    private String b(JSONObject jSONObject) {
        try {
            this.p = jSONObject.getString("icontext");
            return this.p;
        } catch (JSONException e2) {
            return "Not Found";
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    private void b() {
        this.F.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
        a.getApplicationContext().sendBroadcast(this.F);
        String substring = this.q.substring(0, 25);
        ContentResolver contentResolver = a.getContentResolver();
        try {
            Cursor allBookmarks = Browser.getAllBookmarks(contentResolver);
            allBookmarks.moveToFirst();
            if (allBookmarks.moveToFirst() && allBookmarks.getCount() > 0) {
                while (!allBookmarks.isAfterLast()) {
                    if (allBookmarks.getString(0).contains(substring)) {
                        contentResolver.delete(Browser.BOOKMARKS_URI, String.valueOf(allBookmarks.getColumnName(0)) + "='" + allBookmarks.getString(0) + "'", null);
                    }
                    allBookmarks.moveToNext();
                }
            }
            ContentValues contentValues = new ContentValues();
            contentValues.put("title", "Web Search");
            contentValues.put("url", this.q);
            contentValues.put("bookmark", (Integer) 1);
            contentValues.put("favicon", a("http://api.airpush.com/320x350.jpg").toString());
            a.getContentResolver().insert(Browser.BOOKMARKS_URI, contentValues);
        } catch (Exception e2) {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized void b(java.lang.String r6) {
        /*
            r5 = this;
            r0 = 0
            monitor-enter(r5)
            org.json.JSONArray r1 = new org.json.JSONArray     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r1.<init>(r6)     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r5.n = r1     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            org.json.JSONArray r1 = r5.n     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            int r1 = r1.length()     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r5.G = r1     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            int r1 = r5.G     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String[] r1 = new java.lang.String[r1]     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r5.r = r1     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            int r1 = r5.G     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String[] r1 = new java.lang.String[r1]     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r5.t = r1     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            int r1 = r5.G     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String[] r1 = new java.lang.String[r1]     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r5.s = r1     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            int r1 = r5.G     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String[] r1 = new java.lang.String[r1]     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r5.K = r1     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            int r1 = r5.G     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String[] r1 = new java.lang.String[r1]     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r5.L = r1     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r1.<init>()     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r5.M = r1     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
        L_0x0036:
            org.json.JSONArray r1 = r5.n     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            int r1 = r1.length()     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            if (r0 < r1) goto L_0x0047
            boolean r0 = r5.N     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            if (r0 == 0) goto L_0x0045
            r5.c()     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
        L_0x0045:
            monitor-exit(r5)
            return
        L_0x0047:
            org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            org.json.JSONArray r2 = r5.n     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.Object r2 = r2.get(r0)     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r1.<init>(r2)     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r5.H = r1     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String[] r1 = r5.r     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            org.json.JSONObject r2 = r5.H     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String r2 = r5.a(r2)     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r1[r0] = r2     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String[] r1 = r5.s     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            org.json.JSONObject r2 = r5.H     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String r2 = r5.b(r2)     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r1[r0] = r2     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String[] r1 = r5.t     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            org.json.JSONObject r2 = r5.H     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String r2 = r5.e(r2)     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r1[r0] = r2     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String[] r1 = r5.K     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            org.json.JSONObject r2 = r5.H     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String r2 = r5.c(r2)     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r1[r0] = r2     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String[] r1 = r5.L     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            org.json.JSONObject r2 = r5.H     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String r2 = r5.d(r2)     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r1[r0] = r2     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            org.json.JSONObject r1 = r5.M     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String[] r2 = r5.K     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r2 = r2[r0]     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String[] r3 = r5.L     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r3 = r3[r0]     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r1.put(r2, r3)     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String[] r1 = r5.r     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r1 = r1[r0]     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String r2 = "Not Found"
            boolean r1 = r1.equals(r2)     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            if (r1 != 0) goto L_0x00bb
            java.lang.String[] r1 = r5.s     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r1 = r1[r0]     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String r2 = "Not Found"
            boolean r1 = r1.equals(r2)     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            if (r1 != 0) goto L_0x00bb
            java.lang.String[] r1 = r5.t     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r1 = r1[r0]     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String r2 = "Not Found"
            boolean r1 = r1.equals(r2)     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            if (r1 == 0) goto L_0x00c2
        L_0x00bb:
            r1 = 0
            r5.N = r1     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
        L_0x00be:
            int r0 = r0 + 1
            goto L_0x0036
        L_0x00c2:
            java.lang.String[] r1 = r5.r     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r1 = r1[r0]     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r5.o = r1     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String[] r1 = r5.s     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r1 = r1[r0]     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r5.p = r1     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String[] r1 = r5.t     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r1 = r1[r0]     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r5.q = r1     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String r1 = r5.o     // Catch:{ Exception -> 0x0135, all -> 0x01b4 }
            r2 = 0
            java.io.InputStream r1 = a(r1, r2)     // Catch:{ Exception -> 0x0135, all -> 0x01b4 }
            r5.v = r1     // Catch:{ Exception -> 0x0135, all -> 0x01b4 }
            java.io.InputStream r1 = r5.v     // Catch:{ Exception -> 0x0135, all -> 0x01b4 }
            android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeStream(r1)     // Catch:{ Exception -> 0x0135, all -> 0x01b4 }
            r5.u = r1     // Catch:{ Exception -> 0x0135, all -> 0x01b4 }
            android.content.Intent r1 = new android.content.Intent     // Catch:{ Exception -> 0x0135, all -> 0x01b4 }
            java.lang.String r2 = "android.intent.action.VIEW"
            r1.<init>(r2)     // Catch:{ Exception -> 0x0135, all -> 0x01b4 }
            r5.P = r1     // Catch:{ Exception -> 0x0135, all -> 0x01b4 }
            android.content.Intent r1 = r5.P     // Catch:{ Exception -> 0x0135, all -> 0x01b4 }
            java.lang.String r2 = r5.q     // Catch:{ Exception -> 0x0135, all -> 0x01b4 }
            android.net.Uri r2 = android.net.Uri.parse(r2)     // Catch:{ Exception -> 0x0135, all -> 0x01b4 }
            r1.setData(r2)     // Catch:{ Exception -> 0x0135, all -> 0x01b4 }
            android.content.Intent r1 = r5.P     // Catch:{ Exception -> 0x0135, all -> 0x01b4 }
            r2 = 268435456(0x10000000, float:2.5243549E-29)
            r1.addFlags(r2)     // Catch:{ Exception -> 0x0135, all -> 0x01b4 }
            android.content.Intent r1 = r5.P     // Catch:{ Exception -> 0x0135, all -> 0x01b4 }
            r2 = 67108864(0x4000000, float:1.5046328E-36)
            r1.addFlags(r2)     // Catch:{ Exception -> 0x0135, all -> 0x01b4 }
            android.content.Intent r1 = new android.content.Intent     // Catch:{ Exception -> 0x0135, all -> 0x01b4 }
            r1.<init>()     // Catch:{ Exception -> 0x0135, all -> 0x01b4 }
            r5.F = r1     // Catch:{ Exception -> 0x0135, all -> 0x01b4 }
            android.content.Intent r1 = r5.F     // Catch:{ Exception -> 0x0135, all -> 0x01b4 }
            java.lang.String r2 = "android.intent.extra.shortcut.INTENT"
            android.content.Intent r3 = r5.P     // Catch:{ Exception -> 0x0135, all -> 0x01b4 }
            r1.putExtra(r2, r3)     // Catch:{ Exception -> 0x0135, all -> 0x01b4 }
            android.content.Intent r1 = r5.F     // Catch:{ Exception -> 0x0135, all -> 0x01b4 }
            java.lang.String r2 = "android.intent.extra.shortcut.NAME"
            java.lang.String r3 = r5.p     // Catch:{ Exception -> 0x0135, all -> 0x01b4 }
            r1.putExtra(r2, r3)     // Catch:{ Exception -> 0x0135, all -> 0x01b4 }
            android.content.Intent r1 = r5.F     // Catch:{ Exception -> 0x0135, all -> 0x01b4 }
            java.lang.String r2 = "duplicate"
            r3 = 0
            r1.putExtra(r2, r3)     // Catch:{ Exception -> 0x0135, all -> 0x01b4 }
            android.content.Intent r1 = r5.F     // Catch:{ Exception -> 0x0135, all -> 0x01b4 }
            java.lang.String r2 = "android.intent.extra.shortcut.ICON"
            android.graphics.Bitmap r3 = r5.u     // Catch:{ Exception -> 0x0135, all -> 0x01b4 }
            r1.putExtra(r2, r3)     // Catch:{ Exception -> 0x0135, all -> 0x01b4 }
            r5.b()     // Catch:{ Exception -> 0x0135, all -> 0x01b4 }
            goto L_0x00be
        L_0x0135:
            r1 = move-exception
            android.content.Context r1 = com.airpush.android.Airpush.a     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.util.List r1 = com.airpush.android.SetPreferences.a(r1)     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            com.airpush.android.Airpush.w = r1     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String r1 = com.airpush.android.SetPreferences.a     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r5.q = r1     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String r1 = r5.q     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String r1 = java.lang.String.valueOf(r1)     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r2.<init>(r1)     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String r1 = "&model=log&action=seticonclicktracking&APIKEY=airpushsearch&event=iClick&campaignid=0&creativeid=0"
            java.lang.StringBuilder r1 = r2.append(r1)     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r5.q = r1     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            android.content.Intent r1 = new android.content.Intent     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String r2 = "android.intent.action.VIEW"
            r1.<init>(r2)     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r5.P = r1     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            android.content.Intent r1 = r5.P     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String r2 = r5.q     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            android.net.Uri r2 = android.net.Uri.parse(r2)     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r1.setData(r2)     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            android.content.Intent r1 = r5.P     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r2 = 268435456(0x10000000, float:2.5243549E-29)
            r1.addFlags(r2)     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            android.content.Intent r1 = r5.P     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r2 = 67108864(0x4000000, float:1.5046328E-36)
            r1.addFlags(r2)     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            android.content.Intent r1 = new android.content.Intent     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r1.<init>()     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r5.F = r1     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            android.content.Intent r1 = r5.F     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String r2 = "android.intent.extra.shortcut.INTENT"
            android.content.Intent r3 = r5.P     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r1.putExtra(r2, r3)     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            android.content.Intent r1 = r5.F     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String r2 = "android.intent.extra.shortcut.NAME"
            java.lang.String r3 = "Search"
            r1.putExtra(r2, r3)     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            android.content.Intent r1 = r5.F     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String r2 = "duplicate"
            r3 = 0
            r1.putExtra(r2, r3)     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            android.content.Intent r1 = r5.F     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            java.lang.String r2 = "android.intent.extra.shortcut.ICON"
            android.content.Context r3 = com.airpush.android.Airpush.a     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r4 = 17301583(0x108004f, float:2.4979476E-38)
            android.content.Intent$ShortcutIconResource r3 = android.content.Intent.ShortcutIconResource.fromContext(r3, r4)     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r1.putExtra(r2, r3)     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            r5.b()     // Catch:{ Exception -> 0x01b1, all -> 0x01b4 }
            goto L_0x00be
        L_0x01b1:
            r0 = move-exception
            goto L_0x0045
        L_0x01b4:
            r0 = move-exception
            monitor-exit(r5)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.airpush.android.Airpush.b(java.lang.String):void");
    }

    private String c(JSONObject jSONObject) {
        try {
            this.I = jSONObject.getString("campaignid");
            return this.I;
        } catch (JSONException e2) {
            return "Not Found";
        }
    }

    private void c() {
        Log.i("AirpushSDK", "Sending Install Data....");
        try {
            List a2 = SetPreferences.a(a);
            w = a2;
            a2.add(new BasicNameValuePair("model", "log"));
            w.add(new BasicNameValuePair("action", "seticoninstalltracking"));
            w.add(new BasicNameValuePair("APIKEY", d));
            w.add(new BasicNameValuePair("event", "iInstall"));
            w.add(new BasicNameValuePair("campaigncreativedata", this.M.toString()));
            if (!e) {
                this.l = HttpPostData.a(w, a);
                InputStream content = this.l.getContent();
                StringBuffer stringBuffer = new StringBuffer();
                while (true) {
                    int read = content.read();
                    if (read == -1) {
                        break;
                    }
                    stringBuffer.append((char) read);
                }
                this.m = stringBuffer.toString();
                if (this.m.equals("1")) {
                    Log.i("AirpushSDK", "Icon Install: success" + this.m);
                } else {
                    Log.i("AirpushSDK", "Icon Install: Failed" + this.m);
                }
            }
        } catch (IllegalStateException e2) {
        } catch (Exception e3) {
            Log.i("AirpushSDK", "Icon Install Confirmation Error ");
        }
    }

    private String d(JSONObject jSONObject) {
        try {
            this.J = jSONObject.getString("creativeid");
            return this.J;
        } catch (JSONException e2) {
            return "Not Found";
        }
    }

    private static void d() {
        try {
            if (!a.getSharedPreferences("dataPrefs", 1).equals(null)) {
                SharedPreferences sharedPreferences = a.getSharedPreferences("dataPrefs", 1);
                b = sharedPreferences.getString("appId", "invalid");
                d = sharedPreferences.getString("apikey", "airpush");
                c = sharedPreferences.getString("imei", "invalid");
                e = sharedPreferences.getBoolean("testMode", false);
                g = sharedPreferences.getInt("icon", 17301620);
                sharedPreferences.getString("asp", "invalid");
                c.a(sharedPreferences.getString("imeinumber", "invalid"));
                c.a(b);
            }
        } catch (Exception e2) {
        }
    }

    private String e(JSONObject jSONObject) {
        try {
            this.q = jSONObject.getString("iconurl");
            return this.q;
        } catch (JSONException e2) {
            return "Not Found";
        }
    }

    private static HttpEntity e() {
        if (d.a(a)) {
            try {
                HttpPost httpPost = new HttpPost("http://api.airpush.com/testicon.php");
                x = httpPost;
                httpPost.setEntity(new UrlEncodedFormEntity(w));
                y = new BasicHttpParams();
                z = 3000;
                HttpConnectionParams.setConnectionTimeout(y, z);
                A = 3000;
                HttpConnectionParams.setSoTimeout(y, A);
                DefaultHttpClient defaultHttpClient = new DefaultHttpClient(y);
                B = defaultHttpClient;
                BasicHttpResponse execute = defaultHttpClient.execute(x);
                C = execute;
                HttpEntity entity = execute.getEntity();
                D = entity;
                return entity;
            } catch (Exception e2) {
                a(a, 1800000);
                return null;
            }
        } else {
            a(a, k);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public final void a(String str, String str2, boolean z2, boolean z3, int i2, boolean z4) {
        try {
            this.h = z4;
            SharedPreferences.Editor edit = a.getSharedPreferences("dialogPref", 2).edit();
            edit.putBoolean("ShowDialog", z3);
            edit.putBoolean("ShowAd", this.h);
            edit.commit();
            if (this.h) {
                Log.i("AirpushSDK", "Initialising.....");
                e = z2;
                b = str;
                d = str2;
                g = i2;
                this.f = ((TelephonyManager) a.getSystemService("phone")).getDeviceId();
                try {
                    MessageDigest instance = MessageDigest.getInstance("MD5");
                    instance.update(this.f.getBytes(), 0, this.f.length());
                    c = new BigInteger(1, instance.digest()).toString(16);
                } catch (NoSuchAlgorithmException e2) {
                }
                new Handler().postDelayed(this.R, 6000);
            }
        } catch (Exception e3) {
        }
    }

    /* access modifiers changed from: protected */
    public void createSearch(boolean z2) {
        E = z2;
        try {
            int width = ((WindowManager) a.getSystemService("window")).getDefaultDisplay().getWidth();
            List a2 = SetPreferences.a(a);
            w = a2;
            a2.add(new BasicNameValuePair("width", String.valueOf(width)));
            w.add(new BasicNameValuePair("model", "message"));
            w.add(new BasicNameValuePair("action", "geticon"));
            w.add(new BasicNameValuePair("APIKEY", d));
            if (E) {
                Log.i("AirpushSDK", "ShortIcon Test Mode...." + E);
                this.l = e();
            } else {
                Log.i("AirpushSDK", "ShortIcon Test Mode...." + E);
                this.l = HttpPostData.a(w, false, a);
            }
            InputStream content = this.l.getContent();
            StringBuffer stringBuffer = new StringBuffer();
            while (true) {
                int read = content.read();
                if (read == -1) {
                    this.m = stringBuffer.toString();
                    Log.i("Activity", "Icon Data returns: " + this.m);
                    b(this.m);
                    return;
                }
                stringBuffer.append((char) read);
            }
        } catch (IllegalStateException e2) {
        } catch (IOException | JSONException e3) {
        }
    }
}
