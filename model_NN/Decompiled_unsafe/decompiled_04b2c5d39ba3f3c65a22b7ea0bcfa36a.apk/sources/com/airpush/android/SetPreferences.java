package com.airpush.android;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.webkit.WebView;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

public class SetPreferences {
    /* access modifiers changed from: private */
    public static String A = "0";
    private static Context B;
    private static List C;
    private static String D;
    protected static String a;
    private static JSONObject b = null;
    private static int e;
    private static String j;
    private static String k;
    private static String l;
    private static boolean m;
    private static String n = "4.01";
    private static String o = "0";
    private static String p = "00";
    private static String q = "invalid";
    private static String r = "0";
    private static String s = "0";
    private static String t = "0";
    private static String u = "0";
    private static String v = "0";
    private static String w = "0";
    private static String x = "0";
    private static String y = "0";
    /* access modifiers changed from: private */
    public static String z = "0";
    private String c = "0";
    private boolean d;
    private String f;
    private boolean g;
    private boolean h;
    private boolean i;

    protected static List a(Context context) {
        try {
            if (!context.getSharedPreferences("dataPrefs", 1).equals(null)) {
                SharedPreferences sharedPreferences = context.getSharedPreferences("dataPrefs", 1);
                w = sharedPreferences.getString("appId", "invalid");
                x = sharedPreferences.getString("apikey", "airpush");
                o = sharedPreferences.getString("imei", "invalid");
                y = sharedPreferences.getString("token", "invalid");
                p = new Date().toString();
                q = sharedPreferences.getString("packageName", "invalid");
                r = sharedPreferences.getString("version", "invalid");
                s = sharedPreferences.getString("carrier", "invalid");
                t = sharedPreferences.getString("networkOperator", "invalid");
                u = sharedPreferences.getString("phoneModel", "invalid");
                v = sharedPreferences.getString("manufacturer", "invalid");
                z = sharedPreferences.getString("longitude", "invalid");
                A = sharedPreferences.getString("latitude", "invalid");
                n = sharedPreferences.getString("sdkversion", "4.01");
                l = sharedPreferences.getString("connectionType", "0");
                m = sharedPreferences.getBoolean("testMode", false);
                k = sharedPreferences.getString("useragent", "Default");
                e = sharedPreferences.getInt("icon", 17301514);
                j = sharedPreferences.getString("android_id", "Android_id");
            } else {
                q = B.getPackageName();
                String a2 = HttpPostData.a("http://api.airpush.com/model/user/getappinfo.php?packageName=" + q, B);
                D = a2;
                w = c(a2);
                x = d(D);
            }
        } catch (Exception e2) {
        }
        try {
            ArrayList arrayList = new ArrayList();
            C = arrayList;
            arrayList.add(new BasicNameValuePair("apikey", x));
            C.add(new BasicNameValuePair("appId", w));
            C.add(new BasicNameValuePair("imei", o));
            C.add(new BasicNameValuePair("token", y));
            C.add(new BasicNameValuePair("request_timestamp", p));
            C.add(new BasicNameValuePair("packageName", q));
            C.add(new BasicNameValuePair("version", r));
            C.add(new BasicNameValuePair("carrier", s));
            C.add(new BasicNameValuePair("networkOperator", t));
            C.add(new BasicNameValuePair("phoneModel", u));
            C.add(new BasicNameValuePair("manufacturer", v));
            C.add(new BasicNameValuePair("longitude", z));
            C.add(new BasicNameValuePair("latitude", A));
            C.add(new BasicNameValuePair("sdkversion", n));
            C.add(new BasicNameValuePair("wifi", l));
            C.add(new BasicNameValuePair("useragent", k));
            C.add(new BasicNameValuePair("android_id", j));
            a = "apikey=" + x + "&appId=" + w + "&imei=" + o + "&token=" + y + "&request_timestamp=" + p + "&packageName=" + q + "&version=" + r + "&carrier=" + s + "&networkOperator=" + t + "&phoneModel=" + u + "&manufacturer=" + v + "&longitude=" + z + "&latitude=" + A + "&sdkversion=" + n + "&wifi=" + l + "&useragent=" + k;
        } catch (Exception e3) {
        }
        return C;
    }

    public static boolean b(Context context) {
        if (context.getSharedPreferences("sdkPrefs", 1).equals(null)) {
            return true;
        }
        SharedPreferences sharedPreferences = context.getSharedPreferences("sdkPrefs", 1);
        if (sharedPreferences.contains("SDKEnabled")) {
            return sharedPreferences.getBoolean("SDKEnabled", false);
        }
        return true;
    }

    private static String c(String str) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            b = jSONObject;
            return jSONObject.getString("appid");
        } catch (JSONException e2) {
            return "invalid Id";
        }
    }

    private static String d(String str) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            b = jSONObject;
            return jSONObject.getString("authkey");
        } catch (JSONException e2) {
            return "invalid key";
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Context context, String str, String str2, boolean z2, boolean z3, boolean z4, boolean z5) {
        B = context;
        w = str;
        x = str2;
        this.g = z5;
        this.h = z3;
        this.i = z4;
        m = z2;
        k = new WebView(B).getSettings().getUserAgentString();
        Log.i("User Agent", "User Agent : " + this.g);
        l = ((ConnectivityManager) B.getSystemService("connectivity")).getActiveNetworkInfo().getTypeName().equals("WIFI") ? "1" : "0";
        Context context2 = B;
        if (context2.getPackageManager().checkPermission("android.permission.ACCESS_COARSE_LOCATION", context2.getPackageName()) == 0 && context2.getPackageManager().checkPermission("android.permission.ACCESS_FINE_LOCATION", context2.getPackageName()) == 0) {
            LocationManager locationManager = (LocationManager) context2.getSystemService("location");
            Location lastKnownLocation = locationManager.getLastKnownLocation("network");
            if (lastKnownLocation == null) {
                locationManager.requestLocationUpdates("network", 0, 0.0f, new v(this));
            } else {
                z = String.valueOf(lastKnownLocation.getLongitude());
                A = String.valueOf(lastKnownLocation.getLatitude());
            }
        }
        TelephonyManager telephonyManager = (TelephonyManager) B.getSystemService("phone");
        this.c = telephonyManager.getDeviceId();
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(this.c.getBytes(), 0, this.c.length());
            o = new BigInteger(1, instance.digest()).toString(16);
            p = new Date().toString();
            u = Build.MODEL;
            v = Build.MANUFACTURER;
            t = telephonyManager.getNetworkOperatorName();
            s = telephonyManager.getSimOperatorName();
            r = Build.VERSION.SDK;
            j = Settings.Secure.getString(B.getContentResolver(), "android_id");
            q = B.getPackageName();
            y = String.valueOf(o) + w + p;
            MessageDigest instance2 = MessageDigest.getInstance("MD5");
            instance2.update(y.getBytes(), 0, y.length());
            y = new BigInteger(1, instance2.digest()).toString(16);
            try {
                p = new Date().toString();
                SharedPreferences.Editor edit = B.getSharedPreferences("dataPrefs", 2).edit();
                edit.putString("apikey", x);
                edit.putString("appId", w);
                edit.putString("imei", o);
                edit.putString("connectionType", l);
                edit.putString("token", y);
                edit.putString("request_timestamp", p);
                edit.putString("packageName", q);
                edit.putString("version", r);
                edit.putString("carrier", s);
                edit.putString("networkOperator", t);
                edit.putString("phoneModel", u);
                edit.putString("manufacturer", v);
                edit.putString("longitude", z);
                edit.putString("latitude", A);
                edit.putString("sdkversion", "4.01");
                edit.putString("android_id", j);
                edit.putBoolean("showDialog", this.d);
                edit.putBoolean("showAd", false);
                edit.putBoolean("testMode", m);
                edit.putBoolean("doPush", this.g);
                edit.putBoolean("doSearch", this.h);
                edit.putBoolean("searchIconTestMode", this.i);
                edit.putInt("icon", e);
                edit.putString("useragent", k);
                this.f = c.a(String.valueOf(w) + this.c + l + y + p + q + r + s + t + u + v + z + A + k);
                edit.putString("asp", this.f);
                edit.putString("imeinumber", this.c);
                edit.commit();
            } catch (Exception e2) {
            }
        } catch (NoSuchAlgorithmException e3) {
            Log.i("AirpushSDK", "IMEI conversion Error ");
        }
    }
}
