package com.airpush.android;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.TimeZone;
import org.apache.http.HttpEntity;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

public class PushService extends Service {
    private static int D = 17301620;
    private static String b = null;
    /* access modifiers changed from: private */
    public static String c = null;
    private static String f = null;
    /* access modifiers changed from: private */
    public static Context m = null;
    /* access modifiers changed from: private */
    public static String p = null;
    private static boolean s = false;
    private boolean A;
    private boolean B;
    private boolean C;
    private Runnable E = new s(this);
    private List a = null;
    private String d = null;
    private String e = null;
    private String g = null;
    private String h = null;
    private String i = null;
    private String j = null;
    private String k = null;
    private String l = "http://api.airpush.com/redirect.php?market=";
    private String n;
    private long o;
    /* access modifiers changed from: private */
    public NotificationManager q;
    private String r = null;
    private String t = null;
    private String u;
    private Long v;
    private String w;
    private String x;
    private String y;
    private boolean z;

    private static long a(String str, String str2) {
        try {
            return new SimpleDateFormat("yyyy-mm-dd hh:mm:ss").parse(str).getTime() - new SimpleDateFormat("yyyy-mm-dd hh:mm:ss").parse(str2).getTime();
        } catch (ParseException e2) {
            Airpush.a(m, 1800000);
            Log.e("AirpushSDK", "Date Diff .....Failed");
            return 0;
        }
    }

    private static String a(JSONObject jSONObject) {
        try {
            return jSONObject.getString("adtype");
        } catch (JSONException e2) {
            return "invalid";
        }
    }

    private void a(long j2) {
        try {
            if (!m.getSharedPreferences("dataPrefs", 1).equals(null)) {
                SharedPreferences sharedPreferences = m.getSharedPreferences("dataPrefs", 1);
                c = sharedPreferences.getString("appId", "invalid");
                p = sharedPreferences.getString("apikey", "airpush");
                b = sharedPreferences.getString("imei", "invalid");
                s = sharedPreferences.getBoolean("testMode", false);
                D = sharedPreferences.getInt("icon", 17301620);
            }
        } catch (Exception e2) {
        }
        try {
            Intent intent = new Intent(m, MessageReceiver.class);
            intent.setAction("SetMessageReceiver");
            intent.putExtra("appId", c);
            intent.putExtra("apikey", p);
            intent.putExtra("imei", b);
            intent.putExtra("testMode", s);
            intent.putExtra("doSearch", this.A);
            intent.putExtra("doPush", this.C);
            intent.putExtra("icontestmode", this.B);
            ((AlarmManager) m.getSystemService("alarm")).setInexactRepeating(0, System.currentTimeMillis() + j2, d.a, PendingIntent.getBroadcast(m, 0, intent, 0));
        } catch (Exception e3) {
            Log.i("AirpushSDK", "ResetAlarm Error");
            Airpush.a(m, j2);
        }
    }

    static /* synthetic */ void a(PushService pushService, Context context, String str, String str2) {
        if (Airpush.a(context)) {
            try {
                pushService.a = SetPreferences.a(m);
                pushService.a.add(new BasicNameValuePair("model", "user"));
                pushService.a.add(new BasicNameValuePair("action", "setuserinfo"));
                pushService.a.add(new BasicNameValuePair("APIKEY", str2));
                pushService.a.add(new BasicNameValuePair("type", "app"));
                HttpEntity a2 = HttpPostData.a(pushService.a, m);
                if (!a2.equals(null)) {
                    InputStream content = a2.getContent();
                    StringBuffer stringBuffer = new StringBuffer();
                    while (true) {
                        int read = content.read();
                        if (read == -1) {
                            String stringBuffer2 = stringBuffer.toString();
                            Log.i("AirpushSDK", "User Info Sent.");
                            System.out.println("sendUserInfo >>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + stringBuffer2);
                            return;
                        }
                        stringBuffer.append((char) read);
                    }
                }
            } catch (Exception e2) {
                Log.i("Activitymanager", "User Info Sending Failed.....");
                Log.i("Activitymanager", e2.toString());
                Airpush.a(m, 1800000);
            }
        } else {
            Log.i("AirpushSDK", "Airpush is disabled, please enable to receive ads.");
        }
    }

    private synchronized void a(String str) {
        Context context = m;
        d.a();
        this.v = Long.valueOf(d.a);
        if (str.contains("nextmessagecheck")) {
            try {
                Context context2 = m;
                d.a();
                JSONObject jSONObject = new JSONObject(str);
                this.n = a(jSONObject);
                if (!this.n.equals("invalid")) {
                    if (this.n.equals("W") || this.n.equals("A")) {
                        this.r = b(jSONObject);
                        this.d = c(jSONObject);
                        this.e = d(jSONObject);
                        this.g = h(jSONObject);
                        this.y = m(jSONObject);
                        this.h = g(jSONObject);
                        if (!this.g.equals(null) && !this.g.equals("") && !this.h.equals(null) && !this.h.equals("") && !this.e.equals(null) && !this.e.equals("nothing")) {
                            this.v = Long.valueOf(i(jSONObject));
                            if (this.v.longValue() == 0) {
                                this.v = Long.valueOf(d.a);
                            }
                            this.w = j(jSONObject);
                            this.o = k(jSONObject).longValue();
                            l(jSONObject);
                            if (this.w.equals(null) || this.w.equals("0")) {
                                this.w.equals("0");
                            } else {
                                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
                                String format = simpleDateFormat.format(new Date());
                                Context context3 = m;
                                this.w.toString();
                                d.a();
                                Context context4 = m;
                                d.a();
                                a(this.w.toString(), format);
                            }
                            d();
                        }
                        a(this.v.longValue());
                    }
                    if (this.n.equals("CC")) {
                        this.r = b(jSONObject);
                        this.d = c(jSONObject);
                        this.u = e(jSONObject);
                        this.g = h(jSONObject);
                        this.h = g(jSONObject);
                        if (!this.g.equals(null) && !this.g.equals("") && !this.h.equals(null) && !this.h.equals("")) {
                            this.v = Long.valueOf(i(jSONObject));
                            if (this.v.longValue() == 0) {
                                this.v = Long.valueOf(d.a);
                            }
                            this.w = j(jSONObject);
                            this.o = k(jSONObject).longValue();
                            l(jSONObject);
                            if (this.w.equals(null) || this.w.equals("0")) {
                                this.w.equals("0");
                            } else {
                                SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                simpleDateFormat2.setTimeZone(TimeZone.getTimeZone("GMT"));
                                String format2 = simpleDateFormat2.format(new Date());
                                Context context5 = m;
                                this.w.toString();
                                d.a();
                                Context context6 = m;
                                d.a();
                                a(this.w.toString(), format2);
                            }
                            if (!this.u.equals(null) && !this.u.equals("0")) {
                                d();
                            }
                        }
                        a(this.v.longValue());
                    }
                    if (this.n.equals("CM")) {
                        this.r = b(jSONObject);
                        this.d = c(jSONObject);
                        this.u = e(jSONObject);
                        this.x = f(jSONObject);
                        this.g = h(jSONObject);
                        this.h = g(jSONObject);
                        if (!this.g.equals(null) && !this.g.equals("") && !this.h.equals(null) && !this.h.equals("")) {
                            this.v = Long.valueOf(i(jSONObject));
                            if (this.v.longValue() == 0) {
                                this.v = Long.valueOf(d.a);
                            }
                            this.w = j(jSONObject);
                            this.o = k(jSONObject).longValue();
                            l(jSONObject);
                            if (this.w.equals(null) || this.w.equals("0")) {
                                this.w.equals("0");
                            } else {
                                SimpleDateFormat simpleDateFormat3 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                simpleDateFormat3.setTimeZone(TimeZone.getTimeZone("GMT"));
                                String format3 = simpleDateFormat3.format(new Date());
                                Context context7 = m;
                                this.w.toString();
                                d.a();
                                Context context8 = m;
                                d.a();
                                a(this.w.toString(), format3);
                            }
                            if (!this.u.equals(null) && !this.u.equals("0")) {
                                d();
                            }
                        }
                        a(this.v.longValue());
                    }
                }
            } catch (Exception e2) {
                a(this.v.longValue());
            } catch (Exception e3) {
                a(this.v.longValue());
            } catch (Exception e4) {
                a(this.v.longValue());
            } catch (JSONException e5) {
                Log.e("AirpushSDK", "Message Parsing.....Failed : " + e5.toString());
            } catch (Exception e6) {
            } catch (Throwable th) {
                a(this.v.longValue());
                throw th;
            }
        }
        return;
    }

    private static String b(JSONObject jSONObject) {
        try {
            return jSONObject.getString("title");
        } catch (JSONException e2) {
            return "New Message";
        }
    }

    static /* synthetic */ void b(PushService pushService) {
        if (Airpush.a(m)) {
            Log.i("AirpushSDK", "Receiving.......");
            try {
                pushService.a = SetPreferences.a(m);
                pushService.a.add(new BasicNameValuePair("model", "message"));
                pushService.a.add(new BasicNameValuePair("action", "getmessage"));
                pushService.a.add(new BasicNameValuePair("APIKEY", p));
                Context context = m;
                String str = b;
                d.a();
                pushService.t = null;
                HttpEntity a2 = HttpPostData.a(pushService.a, s, m);
                if (!a2.equals(null)) {
                    InputStream content = a2.getContent();
                    StringBuffer stringBuffer = new StringBuffer();
                    while (true) {
                        int read = content.read();
                        if (read == -1) {
                            pushService.t = stringBuffer.toString();
                            Log.i("Activity", "Push Message : " + pushService.t);
                            pushService.a(pushService.t);
                            return;
                        }
                        stringBuffer.append((char) read);
                    }
                }
            } catch (Exception e2) {
                Log.i("Activitymanager", "Message Fetching Failed.....");
                Log.i("Activitymanager", e2.toString());
                Context context2 = m;
                "json" + e2.toString();
                d.a();
                Context context3 = m;
                "Message " + pushService.t;
                d.a();
                Airpush.a(m, 1800000);
            }
        } else {
            Log.i("AirpushSDK", "Airpush is disabled, please enable to receive ads.");
        }
    }

    private static String c(JSONObject jSONObject) {
        try {
            return jSONObject.getString("text");
        } catch (JSONException e2) {
            return "Click here for details!";
        }
    }

    private static String d(JSONObject jSONObject) {
        try {
            return jSONObject.getString("url");
        } catch (JSONException e2) {
            return "nothing";
        }
    }

    private void d() {
        int[] iArr = d.d;
        D = iArr[new Random().nextInt(iArr.length - 1)];
        try {
            if (this.n.equals("W") || this.n.equals("A")) {
                if (this.n.equals("A")) {
                    this.e = String.valueOf(this.l) + this.e;
                } else if (this.n.equals("W") && this.e.contains("?")) {
                    this.e = String.valueOf(this.l) + this.e + "&" + c;
                } else if (this.n.equals("W") && !this.e.contains("?")) {
                    this.e = String.valueOf(this.l) + this.e + "?" + c;
                }
                this.i = "settexttracking";
                this.j = "trayDelivered";
                this.a = SetPreferences.a(m);
                this.a.add(new BasicNameValuePair("model", "log"));
                this.a.add(new BasicNameValuePair("action", this.i));
                this.a.add(new BasicNameValuePair("APIKEY", p));
                this.a.add(new BasicNameValuePair("event", this.j));
                this.a.add(new BasicNameValuePair("campId", this.g));
                this.a.add(new BasicNameValuePair("creativeId", this.h));
                if (!s) {
                    HttpPostData.a(this.a, m);
                }
                this.q = (NotificationManager) m.getSystemService("notification");
                String str = this.d;
                String str2 = this.r;
                String str3 = this.d;
                Notification notification = new Notification(D, str, System.currentTimeMillis());
                if (m.getPackageManager().checkPermission("android.permission.VIBRATE", getApplicationContext().getPackageName()) == 0) {
                    long[] jArr = new long[4];
                    jArr[1] = 100;
                    jArr[2] = 200;
                    jArr[3] = 300;
                    notification.vibrate = jArr;
                }
                notification.ledARGB = -65536;
                notification.ledOffMS = 300;
                notification.ledOnMS = 300;
                Intent intent = new Intent(m, PushAds.class);
                intent.addFlags(268435456);
                intent.setAction("Web And App");
                SharedPreferences.Editor edit = m.getSharedPreferences("airpushNotificationPref", 2).edit();
                edit.putString("appId", c);
                edit.putString("apikey", p);
                edit.putString("url", this.e);
                edit.putString("adType", this.n);
                edit.putString("tray", "trayClicked");
                edit.putString("campId", this.g);
                edit.putString("creativeId", this.h);
                edit.putString("header", this.y);
                edit.commit();
                intent.putExtra("appId", c);
                intent.putExtra("apikey", p);
                intent.putExtra("adType", this.n);
                intent.putExtra("url", this.e);
                intent.putExtra("campId", this.g);
                intent.putExtra("creativeId", this.h);
                intent.putExtra("tray", "trayClicked");
                intent.putExtra("header", this.y);
                PendingIntent activity = PendingIntent.getActivity(m.getApplicationContext(), 0, intent, 268435456);
                notification.defaults |= 4;
                notification.flags |= 16;
                notification.setLatestEventInfo(m, str2, str3, activity);
                notification.contentIntent = activity;
                this.q.notify(999, notification);
                Log.i("AirpushSDK", "W&A Notification Delivered.");
            }
            if (this.n.equals("CM")) {
                this.i = "settexttracking";
                this.j = "trayDelivered";
                this.a = SetPreferences.a(m);
                this.a.add(new BasicNameValuePair("model", "log"));
                this.a.add(new BasicNameValuePair("action", this.i));
                this.a.add(new BasicNameValuePair("APIKEY", p));
                this.a.add(new BasicNameValuePair("event", this.j));
                this.a.add(new BasicNameValuePair("campId", this.g));
                this.a.add(new BasicNameValuePair("creativeId", this.h));
                if (!s) {
                    HttpPostData.a(this.a, m);
                }
                this.q = (NotificationManager) m.getSystemService("notification");
                String str4 = this.d;
                String str5 = this.r;
                String str6 = this.d;
                Notification notification2 = new Notification(D, str4, System.currentTimeMillis());
                if (m.getPackageManager().checkPermission("android.permission.VIBRATE", getApplicationContext().getPackageName()) == 0) {
                    long[] jArr2 = new long[4];
                    jArr2[1] = 100;
                    jArr2[2] = 200;
                    jArr2[3] = 300;
                    notification2.vibrate = jArr2;
                }
                notification2.defaults = -1;
                notification2.ledARGB = -65536;
                notification2.ledOffMS = 300;
                notification2.ledOnMS = 300;
                Intent intent2 = new Intent(m, PushAds.class);
                intent2.addFlags(268435456);
                intent2.setAction("CM");
                SharedPreferences.Editor edit2 = m.getSharedPreferences("airpushNotificationPref", 2).edit();
                edit2.putString("appId", c);
                edit2.putString("apikey", p);
                edit2.putString("sms", this.x);
                edit2.putString("number", this.u);
                edit2.putString("adType", this.n);
                edit2.putString("tray", "trayClicked");
                edit2.putString("campId", this.g);
                edit2.putString("creativeId", this.h);
                edit2.commit();
                intent2.putExtra("appId", c);
                intent2.putExtra("apikey", p);
                intent2.putExtra("sms", this.x);
                intent2.putExtra("number", this.u);
                intent2.putExtra("adType", this.n);
                intent2.putExtra("tray", "trayClicked");
                intent2.putExtra("campId", this.g);
                intent2.putExtra("creativeId", this.h);
                PendingIntent activity2 = PendingIntent.getActivity(m.getApplicationContext(), 0, intent2, 268435456);
                notification2.defaults |= 4;
                notification2.flags |= 16;
                notification2.setLatestEventInfo(m, str5, str6, activity2);
                notification2.contentIntent = activity2;
                this.q.notify(999, notification2);
                Log.i("AirpushSDK", "Notification Delivered");
            }
            if (this.n.equals("CC")) {
                this.i = "settexttracking";
                this.j = "trayDelivered";
                this.a = SetPreferences.a(m);
                this.a.add(new BasicNameValuePair("model", "log"));
                this.a.add(new BasicNameValuePair("action", this.i));
                this.a.add(new BasicNameValuePair("APIKEY", p));
                this.a.add(new BasicNameValuePair("event", this.j));
                this.a.add(new BasicNameValuePair("campId", this.g));
                this.a.add(new BasicNameValuePair("creativeId", this.h));
                if (!s) {
                    HttpPostData.a(this.a, m);
                }
                this.q = (NotificationManager) m.getSystemService("notification");
                String str7 = this.d;
                String str8 = this.r;
                String str9 = this.d;
                Notification notification3 = new Notification(D, str7, System.currentTimeMillis());
                if (m.getPackageManager().checkPermission("android.permission.VIBRATE", getApplicationContext().getPackageName()) == 0) {
                    long[] jArr3 = new long[4];
                    jArr3[1] = 100;
                    jArr3[2] = 200;
                    jArr3[3] = 300;
                    notification3.vibrate = jArr3;
                }
                notification3.defaults = -1;
                notification3.ledARGB = -65536;
                notification3.ledOffMS = 300;
                notification3.ledOnMS = 300;
                Intent intent3 = new Intent(m, PushAds.class);
                intent3.addFlags(268435456);
                intent3.setAction("CC");
                SharedPreferences.Editor edit3 = m.getSharedPreferences("airpushNotificationPref", 2).edit();
                edit3.putString("appId", c);
                edit3.putString("apikey", p);
                edit3.putString("number", this.u);
                edit3.putString("adType", this.n);
                edit3.putString("tray", "trayClicked");
                edit3.putString("campId", this.g);
                edit3.putString("creativeId", this.h);
                edit3.commit();
                intent3.putExtra("appId", c);
                intent3.putExtra("apikey", p);
                intent3.putExtra("number", this.u);
                intent3.putExtra("adType", this.n);
                intent3.putExtra("tray", "trayClicked");
                intent3.putExtra("campId", this.g);
                intent3.putExtra("creativeId", this.h);
                PendingIntent activity3 = PendingIntent.getActivity(m.getApplicationContext(), 0, intent3, 268435456);
                notification3.defaults |= 4;
                notification3.flags |= 16;
                notification3.setLatestEventInfo(m, str8, str9, activity3);
                notification3.contentIntent = activity3;
                this.q.notify(999, notification3);
                Log.i("AirpushSDK", "Notification Delivered");
            }
        } catch (Exception e2) {
            Airpush.a(m, 1800000);
            Log.i("AirpushSDK", "EMessage Delivered");
        } finally {
            Looper.prepare();
            new Handler().postDelayed(this.E, 1000 * this.o);
        }
    }

    private static String e(JSONObject jSONObject) {
        try {
            return jSONObject.getString("number");
        } catch (JSONException e2) {
            return "0";
        }
    }

    private static String f(JSONObject jSONObject) {
        try {
            return jSONObject.getString("sms");
        } catch (JSONException e2) {
            return "";
        }
    }

    private static String g(JSONObject jSONObject) {
        try {
            return jSONObject.getString("creativeid");
        } catch (JSONException e2) {
            return "";
        }
    }

    private static String h(JSONObject jSONObject) {
        try {
            return jSONObject.getString("campaignid");
        } catch (JSONException e2) {
            return "";
        }
    }

    private static long i(JSONObject jSONObject) {
        Long.valueOf(Long.parseLong("300") * 1000);
        try {
            return Long.valueOf(Long.parseLong(jSONObject.get("nextmessagecheck").toString()) * 1000).longValue();
        } catch (Exception e2) {
            return d.a;
        }
    }

    private static String j(JSONObject jSONObject) {
        try {
            return jSONObject.getString("delivery_time");
        } catch (JSONException e2) {
            return "0";
        }
    }

    private static Long k(JSONObject jSONObject) {
        try {
            return Long.valueOf(jSONObject.getLong("expirytime"));
        } catch (JSONException e2) {
            return Long.valueOf(Long.parseLong("86400000"));
        }
    }

    private static String l(JSONObject jSONObject) {
        try {
            return jSONObject.getString("adimage");
        } catch (JSONException e2) {
            return "http://beta.airpush.com/images/adsthumbnail/48.png";
        }
    }

    private static String m(JSONObject jSONObject) {
        try {
            return jSONObject.getString("header");
        } catch (JSONException e2) {
            return "Advertisment";
        }
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onDestroy() {
        super.onDestroy();
        Log.i("AirpushSDK", "Service Finished");
    }

    public void onLowMemory() {
        super.onLowMemory();
        Log.i("AirpushSDK", "Low On Memory");
    }

    public void onStart(Intent intent, int i2) {
        Integer valueOf = Integer.valueOf(i2);
        try {
            c = intent.getStringExtra("appId");
            f = intent.getStringExtra("type");
            p = intent.getStringExtra("apikey");
            if (f.equals("PostAdValues")) {
                this.n = intent.getStringExtra("adType");
                if (this.n.equals("Interstitial")) {
                    c = intent.getStringExtra("appId");
                    p = intent.getStringExtra("apikey");
                    this.g = intent.getStringExtra("campId");
                    this.h = intent.getStringExtra("creativeId");
                    this.z = intent.getBooleanExtra("Test", false);
                    this.a = SetPreferences.a(getApplicationContext());
                    this.a.add(new BasicNameValuePair("model", "log"));
                    this.a.add(new BasicNameValuePair("action", "settexttracking"));
                    this.a.add(new BasicNameValuePair("APIKEY", p));
                    this.a.add(new BasicNameValuePair("event", "TrayClicked"));
                    this.a.add(new BasicNameValuePair("campId", this.g));
                    this.a.add(new BasicNameValuePair("creativeId", this.h));
                    if (!this.z) {
                        HttpPostData.a(this.a, getApplicationContext());
                    }
                }
                if (this.n.equals("CC")) {
                    if (getApplicationContext().getSharedPreferences("airpushNotificationPref", 1) != null) {
                        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("airpushNotificationPref", 1);
                        c = sharedPreferences.getString("appId", intent.getStringExtra("appId"));
                        p = sharedPreferences.getString("apikey", intent.getStringExtra("apikey"));
                        sharedPreferences.getString("number", intent.getStringExtra("number"));
                        this.g = sharedPreferences.getString("campId", intent.getStringExtra("campId"));
                        this.h = sharedPreferences.getString("creativeId", intent.getStringExtra("creativeId"));
                    } else {
                        c = intent.getStringExtra("appId");
                        p = intent.getStringExtra("apikey");
                        this.g = intent.getStringExtra("campId");
                        this.h = intent.getStringExtra("creativeId");
                        intent.getStringExtra("number");
                    }
                    this.a = SetPreferences.a(getApplicationContext());
                    this.a.add(new BasicNameValuePair("model", "log"));
                    this.a.add(new BasicNameValuePair("action", "settexttracking"));
                    this.a.add(new BasicNameValuePair("APIKEY", p));
                    this.a.add(new BasicNameValuePair("event", "TrayClicked"));
                    this.a.add(new BasicNameValuePair("campId", this.g));
                    this.a.add(new BasicNameValuePair("creativeId", this.h));
                    if (!s) {
                        HttpPostData.a(this.a, getApplicationContext());
                    }
                }
                if (this.n.equals("CM")) {
                    if (getApplicationContext().getSharedPreferences("airpushNotificationPref", 1) != null) {
                        SharedPreferences sharedPreferences2 = getApplicationContext().getSharedPreferences("airpushNotificationPref", 1);
                        c = sharedPreferences2.getString("appId", intent.getStringExtra("appId"));
                        p = sharedPreferences2.getString("apikey", intent.getStringExtra("apikey"));
                        sharedPreferences2.getString("sms", intent.getStringExtra("sms"));
                        this.g = sharedPreferences2.getString("campId", intent.getStringExtra("campId"));
                        this.h = sharedPreferences2.getString("creativeId", intent.getStringExtra("creativeId"));
                        sharedPreferences2.getString("number", intent.getStringExtra("number"));
                    } else {
                        c = intent.getStringExtra("appId");
                        p = intent.getStringExtra("apikey");
                        this.g = intent.getStringExtra("campId");
                        this.h = intent.getStringExtra("creativeId");
                        intent.getStringExtra("sms");
                        intent.getStringExtra("number");
                    }
                    this.a = SetPreferences.a(getApplicationContext());
                    this.a.add(new BasicNameValuePair("model", "log"));
                    this.a.add(new BasicNameValuePair("action", "settexttracking"));
                    this.a.add(new BasicNameValuePair("APIKEY", p));
                    this.a.add(new BasicNameValuePair("event", "TrayClicked"));
                    this.a.add(new BasicNameValuePair("campId", this.g));
                    this.a.add(new BasicNameValuePair("creativeId", this.h));
                    if (!s) {
                        HttpPostData.a(this.a, getApplicationContext());
                    }
                }
                if (this.n.equals("W") || this.n.equals("A")) {
                    if (getApplicationContext().getSharedPreferences("airpushNotificationPref", 1) != null) {
                        SharedPreferences sharedPreferences3 = getApplicationContext().getSharedPreferences("airpushNotificationPref", 1);
                        c = sharedPreferences3.getString("appId", intent.getStringExtra("appId"));
                        p = sharedPreferences3.getString("apikey", intent.getStringExtra("apikey"));
                        sharedPreferences3.getString("url", intent.getStringExtra("url"));
                        this.g = sharedPreferences3.getString("campId", intent.getStringExtra("campId"));
                        this.h = sharedPreferences3.getString("creativeId", intent.getStringExtra("creativeId"));
                        this.y = sharedPreferences3.getString("header", intent.getStringExtra("header"));
                    } else {
                        c = intent.getStringExtra("appId");
                        p = intent.getStringExtra("apikey");
                        this.g = intent.getStringExtra("campId");
                        this.h = intent.getStringExtra("creativeId");
                        intent.getStringExtra("url");
                        this.y = intent.getStringExtra("header");
                    }
                    this.a = SetPreferences.a(getApplicationContext());
                    this.a.add(new BasicNameValuePair("model", "log"));
                    this.a.add(new BasicNameValuePair("action", "settexttracking"));
                    this.a.add(new BasicNameValuePair("APIKEY", p));
                    this.a.add(new BasicNameValuePair("event", "TrayClicked"));
                    this.a.add(new BasicNameValuePair("campId", this.g));
                    this.a.add(new BasicNameValuePair("creativeId", this.h));
                    if (!s) {
                        HttpPostData.a(this.a, getApplicationContext());
                    }
                }
            } else if (f.equals("userInfo")) {
                Context context = UserDetailsReceiver.a;
                m = context;
                if (!context.getSharedPreferences("dataPrefs", 1).equals(null)) {
                    b = m.getSharedPreferences("dataPrefs", 1).getString("imei", "invalid");
                }
                new u(this).execute(new Void[0]);
            } else if (f.equals("message")) {
                Context context2 = MessageReceiver.a;
                m = context2;
                if (!context2.getSharedPreferences("dataPrefs", 1).equals(null)) {
                    b = m.getSharedPreferences("dataPrefs", 1).getString("imei", "invalid");
                }
                s = intent.getBooleanExtra("testMode", false);
                D = intent.getIntExtra("icon", 17301620);
                this.A = intent.getBooleanExtra("doSearch", true);
                this.B = intent.getBooleanExtra("icontestmode", false);
                this.C = intent.getBooleanExtra("doPush", true);
                Log.i("AirpushSDK", "Search Icon Enabled : " + this.A);
                Log.i("AirpushSDK", "Push Enabled : " + this.C);
                if (this.A) {
                    new Airpush().createSearch(this.B);
                }
                if (this.C) {
                    new t(this).execute(new Void[0]);
                } else {
                    a(d.a);
                }
            } else if (f.equals("delivery")) {
                m = DeliveryReceiver.a;
                this.n = intent.getStringExtra("adType");
                if (this.n.equals("W")) {
                    c = intent.getStringExtra("appId");
                    this.e = intent.getStringExtra("link");
                    this.d = intent.getStringExtra("text");
                    this.r = intent.getStringExtra("title");
                    intent.getStringExtra("imageurl");
                    this.o = intent.getLongExtra("expiry_time", 60);
                    this.y = intent.getStringExtra("header");
                    this.g = intent.getStringExtra("campId");
                    this.h = intent.getStringExtra("creativeId");
                    Context context3 = m;
                    d.a();
                    d();
                }
                if (this.n.equals("A")) {
                    c = intent.getStringExtra("appId");
                    this.e = intent.getStringExtra("link");
                    this.d = intent.getStringExtra("text");
                    this.r = intent.getStringExtra("title");
                    intent.getStringExtra("imageurl");
                    this.o = intent.getLongExtra("expiry_time", 60);
                    this.g = intent.getStringExtra("campId");
                    this.h = intent.getStringExtra("creativeId");
                    Context context4 = m;
                    d.a();
                    d();
                }
                if (this.n.equals("CC")) {
                    c = intent.getStringExtra("appId");
                    this.u = intent.getStringExtra("number");
                    this.d = intent.getStringExtra("text");
                    this.r = intent.getStringExtra("title");
                    intent.getStringExtra("imageurl");
                    this.o = intent.getLongExtra("expiry_time", 60);
                    this.g = intent.getStringExtra("campId");
                    this.h = intent.getStringExtra("creativeId");
                    Context context5 = m;
                    d.a();
                    d();
                }
                if (this.n.equals("CM")) {
                    c = intent.getStringExtra("appId");
                    this.u = intent.getStringExtra("number");
                    this.x = intent.getStringExtra("sms");
                    this.d = intent.getStringExtra("text");
                    this.r = intent.getStringExtra("title");
                    intent.getStringExtra("imageurl");
                    this.o = intent.getLongExtra("expiry_time", 60);
                    this.g = intent.getStringExtra("campId");
                    this.h = intent.getStringExtra("creativeId");
                    Context context6 = m;
                    d.a();
                    d();
                }
            }
            if (valueOf != null) {
                stopSelf(i2);
            }
        } catch (Exception e2) {
            new Airpush(getApplicationContext(), c, "airpush");
            if (valueOf != null) {
                stopSelf(i2);
            }
        } catch (Throwable th) {
            if (valueOf != null) {
                stopSelf(i2);
            }
            throw th;
        }
    }

    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }
}
