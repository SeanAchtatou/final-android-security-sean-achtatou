package com.airpush.android;

import android.os.Handler;
import android.util.Log;
import android.view.View;
import org.apache.http.message.BasicNameValuePair;

final class m implements View.OnClickListener {
    private /* synthetic */ PushAds a;

    m(PushAds pushAds) {
        this.a = pushAds;
    }

    public final void onClick(View view) {
        try {
            PushAds.a(this.a, PushAds.b(this.a));
            PushAds.e = SetPreferences.a(this.a.getApplicationContext());
            PushAds.e.add(new BasicNameValuePair("model", "log"));
            PushAds.e.add(new BasicNameValuePair("action", "setfptracking"));
            PushAds.e.add(new BasicNameValuePair("APIKEY", this.a.j));
            PushAds.e.add(new BasicNameValuePair("event", "fclick"));
            PushAds.e.add(new BasicNameValuePair("campId", PushAds.d(this.a)));
            PushAds.e.add(new BasicNameValuePair("creativeId", this.a.c));
            new Handler().postDelayed(this.a.A, 5000);
        } catch (Exception e) {
            Log.i("AirpushSDK", "Display Ad Network Error, please try again later. ");
        }
    }
}
