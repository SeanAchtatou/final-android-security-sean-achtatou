package com.airpush.android;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import bys.widgets.srihanuman.R;

public class Main extends Activity {
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        new Airpush(this, "20422", "airpush", false, false, false);
        setContentView((int) R.layout.audio_list_layout);
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4) {
            return super.onKeyDown(i, keyEvent);
        }
        moveTaskToBack(true);
        return true;
    }
}
