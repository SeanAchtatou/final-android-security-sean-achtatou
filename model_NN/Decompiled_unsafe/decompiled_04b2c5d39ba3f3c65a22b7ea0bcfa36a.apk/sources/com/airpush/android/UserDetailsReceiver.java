package com.airpush.android;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

public class UserDetailsReceiver extends BroadcastReceiver {
    protected static Context a = null;
    private String b = "Invalid";
    private String c = "airpush";
    private String d;
    private String e;
    private JSONObject f;

    private String a(String str) {
        try {
            this.f = new JSONObject(str);
            return this.f.getString("appid");
        } catch (JSONException e2) {
            return "invalid Id";
        }
    }

    private void a() {
        try {
            if (!a.getSharedPreferences("dataPrefs", 1).equals(null)) {
                SharedPreferences sharedPreferences = a.getSharedPreferences("dataPrefs", 1);
                this.b = sharedPreferences.getString("appId", "invalid");
                this.c = sharedPreferences.getString("apikey", "airpush");
                sharedPreferences.getString("imei", "invalid");
                sharedPreferences.getBoolean("testMode", false);
                sharedPreferences.getInt("icon", 17301620);
                return;
            }
            this.e = a.getPackageName();
            this.d = HttpPostData.a("http://api.airpush.com/model/user/getappinfo.php?packageName=" + this.e, a);
            this.b = a(this.d);
            this.c = b(this.d);
        } catch (Exception e2) {
            this.e = a.getPackageName();
            this.d = HttpPostData.a("http://api.airpush.com/model/user/getappinfo.php?packageName=" + this.e, a);
            this.b = a(this.d);
            this.c = b(this.d);
            new Airpush(a, this.b, "airpush");
        }
    }

    private String b(String str) {
        try {
            this.f = new JSONObject(str);
            return this.f.getString("authkey");
        } catch (JSONException e2) {
            return "invalid key";
        }
    }

    public void onReceive(Context context, Intent intent) {
        a = context;
        if (SetPreferences.b(context)) {
            try {
                if (d.a(a)) {
                    if (intent.getAction().equals("SetUserInfo")) {
                        a();
                    }
                    Log.i("AirpushSDK", "Sending User Info....");
                    "airpushAppid " + this.b;
                    d.a();
                    Intent intent2 = new Intent();
                    intent2.setAction("com.airpush.android.PushServiceStart" + this.b);
                    intent2.putExtra("appId", this.b);
                    intent2.putExtra("type", "userInfo");
                    intent2.putExtra("apikey", this.c);
                    if (!intent2.equals(null)) {
                        a.startService(intent2);
                        return;
                    }
                    a();
                    new Airpush(a, this.b, "airpush");
                    return;
                }
                Airpush.a(a, 1800000);
            } catch (Exception e2) {
                a();
                new Airpush(a, this.b, "airpush");
                Log.i("AirpushSDK", "Sending User Info failed");
            }
        } else {
            Log.i("AirpushSDK", "SDK is disabled, please enable to receive Ads !");
        }
    }
}
