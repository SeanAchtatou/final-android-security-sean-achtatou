package com.airpush.android;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.SystemClock;
import android.util.Log;

public final class d {
    protected static long a = 14400000;
    protected static final Integer b = 6000;
    protected static final Integer c = 240;
    protected static int[] d = {17301620, 17301547, 17301611};

    static {
        Integer.valueOf(20000);
        SystemClock.elapsedRealtime();
    }

    protected static void a() {
    }

    protected static boolean a(Context context) {
        new String(new String("ABC"));
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
            if (connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isAvailable() && connectivityManager.getActiveNetworkInfo().isConnected()) {
                return true;
            }
            Log.i("AirpushSDK", "Internet Connection Not Found");
            Log.i("AirpushSDK", "Internet Error: SDK will retry after " + HttpPostData.a + " ms");
            return false;
        } catch (Exception e) {
            return false;
        }
    }
}
