package com.airpush.android;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import java.io.InputStream;
import java.util.List;
import org.apache.http.HttpEntity;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHttpResponse;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;

public class PushAds extends Activity implements View.OnClickListener {
    private static String a = null;
    /* access modifiers changed from: private */
    public static List e = null;
    private static Context l;
    private static HttpPost t;
    private static BasicHttpParams u;
    private static int v;
    private static int w;
    private static DefaultHttpClient x;
    private static BasicHttpResponse y;
    private static HttpEntity z;
    /* access modifiers changed from: private */
    public Runnable A;
    private String b = null;
    /* access modifiers changed from: private */
    public String c = null;
    private String d = null;
    private String f;
    private String g;
    private String h;
    private String i;
    /* access modifiers changed from: private */
    public String j = null;
    private String k;
    /* access modifiers changed from: private */
    public boolean m = true;
    private boolean n = false;
    private int o = 17301620;
    /* access modifiers changed from: private */
    public boolean p = true;
    private Intent q;
    /* access modifiers changed from: private */
    public HttpEntity r;
    private String s;

    public PushAds() {
        new k(this);
        new l(this);
        new m(this);
        new n(this);
        this.A = new o(this);
        new p(this);
    }

    private static HttpEntity a(List list, Context context) {
        if (d.a(context)) {
            try {
                HttpPost httpPost = new HttpPost("http://api.airpush.com/v2/api.php");
                t = httpPost;
                httpPost.setEntity(new UrlEncodedFormEntity(list));
                u = new BasicHttpParams();
                v = 3000;
                HttpConnectionParams.setConnectionTimeout(u, v);
                w = 3000;
                HttpConnectionParams.setSoTimeout(u, w);
                DefaultHttpClient defaultHttpClient = new DefaultHttpClient(u);
                x = defaultHttpClient;
                BasicHttpResponse execute = defaultHttpClient.execute(t);
                y = execute;
                HttpEntity entity = execute.getEntity();
                z = entity;
                return entity;
            } catch (Exception e2) {
                Airpush.a(context, 1800000);
                return null;
            }
        } else {
            Airpush.a(context, 3600000);
            return null;
        }
    }

    static /* synthetic */ void a(PushAds pushAds, String str) {
        Log.i("AirpushSDK", "Displaying Ad.");
        CustomWebView customWebView = new CustomWebView(pushAds);
        customWebView.loadUrl(str);
        pushAds.setContentView(customWebView);
    }

    static /* synthetic */ String b(PushAds pushAds) {
        return null;
    }

    static /* synthetic */ String d(PushAds pushAds) {
        return null;
    }

    static /* synthetic */ boolean g(PushAds pushAds) {
        return false;
    }

    static /* synthetic */ void h(PushAds pushAds) {
        List a2 = SetPreferences.a(l);
        e = a2;
        a2.add(new BasicNameValuePair("model", "log"));
        e.add(new BasicNameValuePair("action", "settexttracking"));
        e.add(new BasicNameValuePair("APIKEY", pushAds.j));
        e.add(new BasicNameValuePair("event", "trayDelivered"));
        e.add(new BasicNameValuePair("campId", pushAds.b));
        e.add(new BasicNameValuePair("creativeId", pushAds.c));
        pushAds.r = a(e, pushAds.getApplicationContext());
        StringBuffer stringBuffer = new StringBuffer();
        try {
            InputStream content = pushAds.r.getContent();
            while (true) {
                int read = content.read();
                if (read == -1) {
                    break;
                }
                stringBuffer.append((char) read);
            }
        } catch (Exception e2) {
        }
        stringBuffer.toString();
    }

    static /* synthetic */ void i(PushAds pushAds) {
        pushAds.r = HttpPostData.a(e, pushAds.getApplicationContext());
        StringBuffer stringBuffer = new StringBuffer();
        try {
            InputStream content = pushAds.r.getContent();
            while (true) {
                int read = content.read();
                if (read == -1) {
                    break;
                }
                stringBuffer.append((char) read);
            }
        } catch (Exception e2) {
        }
        stringBuffer.toString();
    }

    static /* synthetic */ void j(PushAds pushAds) {
        Airpush airpush = new Airpush();
        Context context = l;
        airpush.a(pushAds.d, pushAds.j, pushAds.n, pushAds.m, pushAds.o, pushAds.p);
    }

    public void onClick(View view) {
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        l = getApplicationContext();
        this.q = getIntent();
        this.s = this.q.getAction();
        getWindowManager().getDefaultDisplay();
        this.f = this.q.getStringExtra("adType");
        if (this.f.equals("searchad")) {
            Log.i("AirpushSDK", "Search Clicked");
            return;
        }
        if (this.f.equals("ShoWDialog")) {
            this.d = this.q.getStringExtra("appId");
            this.j = this.q.getStringExtra("apikey");
            this.n = this.q.getBooleanExtra("test", false);
            this.o = this.q.getIntExtra("icon", 17301620);
            try {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setCancelable(true);
                builder.setMessage("Support the App developer by enabling ads in the notification tray, limited to 1 per day.");
                builder.setPositiveButton("I Agree", new q(this));
                builder.setNegativeButton("No", new r(this));
                builder.create();
                builder.show();
            } catch (Exception e2) {
            }
        }
        if (this.s.equals("CC")) {
            if (this.f.equals("CC")) {
                Log.i("AirpushSDK", "Pushing Ads.....");
                if (l.getSharedPreferences("airpushNotificationPref", 1) != null) {
                    SharedPreferences sharedPreferences = l.getSharedPreferences("airpushNotificationPref", 1);
                    this.d = sharedPreferences.getString("appId", this.q.getStringExtra("appId"));
                    this.j = sharedPreferences.getString("apikey", this.q.getStringExtra("apikey"));
                    this.i = sharedPreferences.getString("number", this.q.getStringExtra("number"));
                    this.b = sharedPreferences.getString("campId", this.q.getStringExtra("campId"));
                    this.c = sharedPreferences.getString("creativeId", this.q.getStringExtra("creativeId"));
                } else {
                    this.d = this.q.getStringExtra("appId");
                    this.j = this.q.getStringExtra("apikey");
                    this.b = this.q.getStringExtra("campId");
                    this.c = this.q.getStringExtra("creativeId");
                    this.i = this.q.getStringExtra("number");
                }
                Intent intent = new Intent();
                intent.setAction("com.airpush.android.PushServiceStart" + this.d);
                intent.putExtra("type", "PostAdValues");
                intent.putExtras(this.q);
                startService(intent);
                Log.i("AirpushSDK", "Pushing CC Ads.....");
                startActivity(new Intent("android.intent.action.DIAL", Uri.parse("tel:" + this.i)));
            }
        } else if (this.s.equals("CM")) {
            if (this.f.equals("CM")) {
                Log.i("AirpushSDK", "Pushing Ads.....");
                if (l.getSharedPreferences("airpushNotificationPref", 1) != null) {
                    SharedPreferences sharedPreferences2 = l.getSharedPreferences("airpushNotificationPref", 1);
                    this.d = sharedPreferences2.getString("appId", this.q.getStringExtra("appId"));
                    this.j = sharedPreferences2.getString("apikey", this.q.getStringExtra("apikey"));
                    this.g = sharedPreferences2.getString("sms", this.q.getStringExtra("sms"));
                    this.b = sharedPreferences2.getString("campId", this.q.getStringExtra("campId"));
                    this.c = sharedPreferences2.getString("creativeId", this.q.getStringExtra("creativeId"));
                    this.h = sharedPreferences2.getString("number", this.q.getStringExtra("number"));
                } else {
                    this.d = this.q.getStringExtra("appId");
                    this.j = this.q.getStringExtra("apikey");
                    this.b = this.q.getStringExtra("campId");
                    this.c = this.q.getStringExtra("creativeId");
                    this.g = this.q.getStringExtra("sms");
                    this.h = this.q.getStringExtra("number");
                }
                Intent intent2 = new Intent();
                intent2.setAction("com.airpush.android.PushServiceStart" + this.d);
                intent2.putExtra("type", "PostAdValues");
                intent2.putExtras(this.q);
                startService(intent2);
                Log.i("AirpushSDK", "Pushing CM Ads.....");
                Intent intent3 = new Intent("android.intent.action.SENDTO", Uri.parse("smsto:" + this.h));
                intent3.putExtra("sms_body", this.g);
                startActivity(intent3);
            }
        } else if (!this.s.equals("Web And App")) {
        } else {
            if (this.f.equals("W") || this.f.equals("A")) {
                Log.i("AirpushSDK", "Pushing Ads.....");
                if (l.getSharedPreferences("airpushNotificationPref", 1) != null) {
                    SharedPreferences sharedPreferences3 = l.getSharedPreferences("airpushNotificationPref", 1);
                    this.d = sharedPreferences3.getString("appId", this.q.getStringExtra("appId"));
                    this.j = sharedPreferences3.getString("apikey", this.q.getStringExtra("apikey"));
                    a = sharedPreferences3.getString("url", this.q.getStringExtra("url"));
                    this.b = sharedPreferences3.getString("campId", this.q.getStringExtra("campId"));
                    this.c = sharedPreferences3.getString("creativeId", this.q.getStringExtra("creativeId"));
                    this.k = sharedPreferences3.getString("header", this.q.getStringExtra("header"));
                } else {
                    this.d = this.q.getStringExtra("appId");
                    this.j = this.q.getStringExtra("apikey");
                    this.b = this.q.getStringExtra("campId");
                    this.c = this.q.getStringExtra("creativeId");
                    a = this.q.getStringExtra("url");
                    this.k = this.q.getStringExtra("header");
                }
                Intent intent4 = new Intent();
                intent4.setAction("com.airpush.android.PushServiceStart" + this.d);
                intent4.putExtra("type", "PostAdValues");
                intent4.putExtras(this.q);
                startService(intent4);
                setTitle(this.k);
                String str = a;
                Log.i("AirpushSDK", "Pushing Web and App Ads.....");
                CustomWebView customWebView = new CustomWebView(this);
                customWebView.loadUrl(str);
                setContentView(customWebView);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        finish();
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4) {
            return super.onKeyDown(i2, keyEvent);
        }
        finish();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        finish();
    }
}
