package com.airpush.android;

public final class c {
    private static final char[] a = new char[64];
    private static final byte[] b = new byte[128];

    static {
        System.getProperty("line.separator");
        char c = 'A';
        int i = 0;
        while (c <= 'Z') {
            a[i] = c;
            c = (char) (c + 1);
            i++;
        }
        char c2 = 'a';
        while (c2 <= 'z') {
            a[i] = c2;
            c2 = (char) (c2 + 1);
            i++;
        }
        char c3 = '0';
        while (c3 <= '9') {
            a[i] = c3;
            c3 = (char) (c3 + 1);
            i++;
        }
        a[i] = '+';
        a[i + 1] = '/';
        for (int i2 = 0; i2 < b.length; i2++) {
            b[i2] = -1;
        }
        for (int i3 = 0; i3 < 64; i3++) {
            b[a[i3]] = (byte) i3;
        }
    }

    private c() {
    }

    public static String a(String str) {
        byte[] bytes = str.getBytes();
        return new String(a(bytes, 0, bytes.length));
    }

    private static char[] a(byte[] bArr, int i, int i2) {
        byte b2;
        byte b3;
        int i3 = ((i2 * 4) + 2) / 3;
        char[] cArr = new char[(((i2 + 2) / 3) * 4)];
        int i4 = i2 + 0;
        int i5 = 0;
        int i6 = 0;
        while (i6 < i4) {
            int i7 = i6 + 1;
            byte b4 = bArr[i6] & 255;
            if (i7 < i4) {
                b2 = bArr[i7] & 255;
                i7++;
            } else {
                b2 = 0;
            }
            if (i7 < i4) {
                i6 = i7 + 1;
                b3 = bArr[i7] & 255;
            } else {
                i6 = i7;
                b3 = 0;
            }
            int i8 = b4 >>> 2;
            int i9 = ((b4 & 3) << 4) | (b2 >>> 4);
            int i10 = ((b2 & 15) << 2) | (b3 >>> 6);
            byte b5 = b3 & 63;
            int i11 = i5 + 1;
            cArr[i5] = a[i8];
            int i12 = i11 + 1;
            cArr[i11] = a[i9];
            cArr[i12] = i12 < i3 ? a[i10] : '=';
            int i13 = i12 + 1;
            cArr[i13] = i13 < i3 ? a[b5] : '=';
            i5 = i13 + 1;
        }
        return cArr;
    }
}
