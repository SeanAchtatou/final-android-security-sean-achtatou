package com.airpush.android;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

public class MessageReceiver extends BroadcastReceiver {
    protected static Context a;
    private static String b = "Invalid";
    private String c = null;
    private boolean d;
    private int e;
    private JSONObject f;
    private String g;
    private String h;
    private Runnable i = new j(this);
    private boolean j;
    private boolean k;
    private boolean l;

    private String a(String str) {
        try {
            this.f = new JSONObject(str);
            return this.f.getString("appid");
        } catch (JSONException e2) {
            return "invalid Id";
        }
    }

    /* access modifiers changed from: private */
    public void a() {
        try {
            if (!a.getSharedPreferences("dataPrefs", 1).equals(null)) {
                SharedPreferences sharedPreferences = a.getSharedPreferences("dataPrefs", 1);
                b = sharedPreferences.getString("appId", "invalid");
                this.c = sharedPreferences.getString("apikey", "airpush");
                sharedPreferences.getString("imei", "invalid");
                this.d = sharedPreferences.getBoolean("testMode", false);
                this.l = sharedPreferences.getBoolean("doPush", true);
                this.k = sharedPreferences.getBoolean("doSearch", true);
                this.j = sharedPreferences.getBoolean("searchIconTestMode", false);
                this.e = sharedPreferences.getInt("icon", 17301620);
                return;
            }
            this.g = a.getPackageName();
            this.h = HttpPostData.a("http://api.airpush.com/model/user/getappinfo.php?packageName=" + this.g, a);
            b = a(this.h);
            this.c = b(this.h);
        } catch (Exception e2) {
            this.g = a.getPackageName();
            this.h = HttpPostData.a("http://api.airpush.com/model/user/getappinfo.php?packageName=" + this.g, a);
            b = a(this.h);
            this.c = b(this.h);
            new Airpush(a, b, "airpush");
        }
    }

    private String b(String str) {
        try {
            this.f = new JSONObject(str);
            return this.f.getString("authkey");
        } catch (JSONException e2) {
            return "invalid key";
        }
    }

    public void onReceive(Context context, Intent intent) {
        a = context;
        if (SetPreferences.b(context)) {
            try {
                if (d.a(a)) {
                    Log.i("AirpushSDK", "Receiving Message.....");
                    if (intent.getAction().equals("SetMessageReceiver")) {
                        a();
                    }
                    Intent intent2 = new Intent();
                    intent2.setAction("com.airpush.android.PushServiceStart" + b);
                    intent2.putExtra("appId", b);
                    intent2.putExtra("type", "message");
                    intent2.putExtra("apikey", this.c);
                    intent2.putExtra("testMode", this.d);
                    intent2.putExtra("icon", this.e);
                    intent2.putExtra("icontestmode", this.j);
                    intent2.putExtra("doSearch", this.k);
                    intent2.putExtra("doPush", this.l);
                    if (!intent2.equals(null)) {
                        context.startService(intent2);
                        return;
                    }
                    a();
                    if (b.equals("invalid") || b.equals(null)) {
                        new Handler().postDelayed(this.i, 1800000);
                    }
                    new Airpush(a, b, "airpush");
                }
            } catch (Exception e2) {
                a();
                new Airpush(a, b, "airpush");
            }
        } else {
            Log.i("AirpushSDK", "SDK is disabled, please enable to receive Ads !");
        }
    }
}
