package com.senddroid;

import android.content.Context;
import android.content.SharedPreferences;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

public class AdRequest {
    private static final String PARAMETER_CONNECTION_SPEED = "connection_speed";
    public static final String PARAMETER_DEVICE_ID = "udid";
    protected static final String PARAMETER_HOUROFDAY = "hour";
    protected static final String PARAMETER_INSTALLED = "inst";
    protected static final String PARAMETER_LASTACTIVITY = "act";
    protected static final String PARAMETER_LASTAD = "last";
    protected static final String PARAMETER_LASTICON = "icon";
    private static final String PARAMETER_LATITUDE = "lat";
    private static final String PARAMETER_LONGITUDE = "long";
    private static final String PARAMETER_USER_AGENT = "ua";
    private static final String PARAMETER_ZONE = "app";
    public static final String TAG = "AdRequest";
    private AdLog adLog;
    private String adserverURL = "http://push.senddroid.com/ad.php";
    private Hashtable<String, String> customParameters;
    private Map<String, String> parameters = Collections.synchronizedMap(new HashMap());

    public AdRequest(AdLog adLog2) {
        this.adLog = adLog2;
    }

    public AdRequest(String zone) {
        setZone(zone);
    }

    public void initDefaultParameters(Context context) {
        String deviceIdMD5 = Utils.getDeviceIdMD5(context);
        this.adLog.log(2, 3, "deviceIdMD5", deviceIdMD5);
        if (deviceIdMD5 != null && deviceIdMD5.length() > 0) {
            this.parameters.put(PARAMETER_DEVICE_ID, deviceIdMD5);
        }
        SharedPreferences settings = context.getSharedPreferences("sendDroidSettings", 0);
        this.parameters.put(PARAMETER_INSTALLED, Long.toString(settings.getLong(context.getPackageName() + " installed", 0)));
        this.parameters.put(PARAMETER_LASTACTIVITY, Long.toString(settings.getLong(context.getPackageName() + " lastActivity", 0)));
        this.parameters.put(PARAMETER_LASTAD, Long.toString(settings.getLong(context.getPackageName() + " lastAd", 0)));
        this.parameters.put(PARAMETER_LASTICON, Long.toString(settings.getLong(context.getPackageName() + " lastIcon", 0)));
        this.parameters.put(PARAMETER_HOUROFDAY, new SimpleDateFormat("HH").format(Calendar.getInstance().getTime()));
    }

    public synchronized String getAdserverURL() {
        return this.adserverURL;
    }

    public synchronized void setAdserverURL(String adserverURL2) {
        if (adserverURL2 != null) {
            if (adserverURL2.length() > 0) {
                this.adserverURL = adserverURL2;
            }
        }
    }

    public AdRequest setUa(String ua) {
        if (ua != null) {
            synchronized (this.parameters) {
                this.parameters.put(PARAMETER_USER_AGENT, ua);
            }
        }
        return this;
    }

    public AdRequest setZone(String zone) {
        if (zone != null) {
            synchronized (this.parameters) {
                this.parameters.put(PARAMETER_ZONE, zone);
            }
        }
        return this;
    }

    public AdRequest setLatitude(String latitude) {
        if (latitude != null) {
            synchronized (this.parameters) {
                this.parameters.put(PARAMETER_LATITUDE, latitude);
            }
        }
        return this;
    }

    public AdRequest setLongitude(String longitude) {
        if (longitude != null) {
            synchronized (this.parameters) {
                this.parameters.put(PARAMETER_LONGITUDE, longitude);
            }
        }
        return this;
    }

    public AdRequest setConnectionSpeed(Integer connectionSpeed) {
        if (connectionSpeed != null) {
            synchronized (this.parameters) {
                this.parameters.put(PARAMETER_CONNECTION_SPEED, String.valueOf(connectionSpeed));
            }
        }
        return this;
    }

    public String getUa() {
        String str;
        synchronized (this.parameters) {
            str = this.parameters.get(PARAMETER_USER_AGENT);
        }
        return str;
    }

    public String getZone() {
        String str;
        synchronized (this.parameters) {
            str = this.parameters.get(PARAMETER_ZONE);
        }
        return str;
    }

    public String getLatitude() {
        String str;
        synchronized (this.parameters) {
            str = this.parameters.get(PARAMETER_LATITUDE);
        }
        return str;
    }

    public String getLongitude() {
        String str;
        synchronized (this.parameters) {
            str = this.parameters.get(PARAMETER_LONGITUDE);
        }
        return str;
    }

    public Integer getConnectionSpeed() {
        Integer intParameter;
        synchronized (this.parameters) {
            intParameter = getIntParameter(this.parameters.get(PARAMETER_CONNECTION_SPEED));
        }
        return intParameter;
    }

    public void setCustomParameters(Hashtable<String, String> customParameters2) {
        this.customParameters = customParameters2;
    }

    public Hashtable<String, String> getCustomParameters() {
        return this.customParameters;
    }

    private Integer getIntParameter(String stringValue) {
        if (stringValue != null) {
            return Integer.valueOf(Integer.parseInt(stringValue));
        }
        return null;
    }

    public synchronized String createURL() throws IllegalStateException {
        return toString();
    }

    public synchronized String createIconURL() throws IllegalStateException {
        return toIconString();
    }

    public synchronized String toString() {
        StringBuilder builderToString;
        builderToString = new StringBuilder();
        builderToString.append(this.adserverURL + "?adtype=4");
        appendParameters(builderToString, this.parameters);
        appendParameters(builderToString, this.customParameters);
        return builderToString.toString();
    }

    public synchronized String toIconString() {
        StringBuilder builderToString;
        builderToString = new StringBuilder();
        builderToString.append(this.adserverURL + "?adtype=9");
        appendParameters(builderToString, this.parameters);
        appendParameters(builderToString, this.customParameters);
        return builderToString.toString();
    }

    private void appendParameters(StringBuilder builderToString, Map<String, String> parameters2) {
        if (parameters2 != null) {
            synchronized (parameters2) {
                for (String param : parameters2.keySet()) {
                    String value = parameters2.get(param);
                    if (value != null) {
                        try {
                            builderToString.append("&" + URLEncoder.encode(param, "UTF-8") + "=" + URLEncoder.encode(value, "UTF-8"));
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }
}
