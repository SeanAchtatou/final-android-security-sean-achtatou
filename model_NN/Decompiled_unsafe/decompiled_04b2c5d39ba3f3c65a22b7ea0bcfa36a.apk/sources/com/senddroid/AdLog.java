package com.senddroid;

import android.util.Log;
import java.io.File;
import java.io.IOException;

public class AdLog {
    public static final int LOG_LEVEL_1 = 1;
    public static final int LOG_LEVEL_2 = 2;
    public static final int LOG_LEVEL_3 = 3;
    public static final int LOG_LEVEL_NONE = 0;
    public static final int LOG_TYPE_ERROR = 1;
    public static final int LOG_TYPE_INFO = 3;
    public static final int LOG_TYPE_WARNING = 2;
    private static int defaultLevel = 0;
    private int currentLogLevel = 0;
    private Object object;

    public static void setDefaultLogLevel(int logLevel) {
        defaultLevel = logLevel;
    }

    public static void setFileLog(String fileName) {
        try {
            File filename = new File(fileName);
            if (filename.exists()) {
                filename.delete();
            }
            filename.createNewFile();
            Runtime.getRuntime().exec("logcat -v time -f " + filename.getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public AdLog(Object object2) {
        this.object = object2;
        setLogLevel(defaultLevel);
    }

    public void log(int Level, int Type, String tag, String msg) {
        String resultTag = "[" + Integer.toHexString(this.object.hashCode()) + "]" + tag;
        if (Level <= this.currentLogLevel) {
            switch (Type) {
                case 1:
                    Log.e(resultTag, msg + ' ');
                    return;
                case 2:
                    Log.w(resultTag, msg + ' ');
                    return;
                default:
                    Log.i(resultTag, msg + ' ');
                    return;
            }
        }
    }

    public void setLogLevel(int logLevel) {
        this.currentLogLevel = logLevel;
        switch (logLevel) {
            case 1:
                log(1, 3, "SetLogLevel", "LOG_LEVEL_1");
                return;
            case 2:
                log(1, 3, "SetLogLevel", "LOG_LEVEL_2");
                return;
            case 3:
                log(1, 3, "SetLogLevel", "LOG_LEVEL_3");
                return;
            default:
                log(1, 3, "SetLogLevel", "LOG_LEVEL_NONE");
                return;
        }
    }
}
