package com.senddroid;

public class AutoDetectedParametersSet {
    private static AutoDetectedParametersSet instance;
    private Integer connectionSpeed;
    private String latitude;
    private String longitude;
    private String ua;

    private AutoDetectedParametersSet() {
    }

    public static synchronized AutoDetectedParametersSet getInstance() {
        AutoDetectedParametersSet autoDetectedParametersSet;
        synchronized (AutoDetectedParametersSet.class) {
            if (instance == null) {
                instance = new AutoDetectedParametersSet();
            }
            autoDetectedParametersSet = instance;
        }
        return autoDetectedParametersSet;
    }

    public String getLatitude() {
        return this.latitude;
    }

    public void setLatitude(String latitude2) {
        this.latitude = latitude2;
    }

    public String getLongitude() {
        return this.longitude;
    }

    public void setLongitude(String longitude2) {
        this.longitude = longitude2;
    }

    public String getUa() {
        return this.ua;
    }

    public void setUa(String ua2) {
        this.ua = ua2;
    }

    public Integer getConnectionSpeed() {
        return this.connectionSpeed;
    }

    public void setConnectionSpeed(Integer connectionSpeed2) {
        this.connectionSpeed = connectionSpeed2;
    }
}
