package com.senddroid;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;
import android.os.PowerManager;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import org.json.JSONException;
import org.json.JSONObject;

public class AdService extends Service {
    public static final String TAG = "AdService";
    private static final long WAIT_TIMEOUT = 30000;
    /* access modifiers changed from: private */
    public AdLog adLog = new AdLog(this);
    /* access modifiers changed from: private */
    public AdRequest adRequest;
    /* access modifiers changed from: private */
    public WhereamiLocationListener listener;
    /* access modifiers changed from: private */
    public LocationManager locationManager;
    private PowerManager pm;
    PowerManager.WakeLock wl;

    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        if (this.pm == null) {
            this.pm = (PowerManager) getSystemService("power");
        }
        if (this.wl == null) {
            this.wl = this.pm.newWakeLock(1, "My Tag");
        }
        this.wl.acquire();
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String appId = preferences.getString("app_id", "");
        int logLevel = preferences.getInt("log_level", -1);
        if (logLevel >= 0) {
            this.adLog.setLogLevel(logLevel);
        }
        this.adLog.log(2, 3, TAG, "AdService.onStart #" + String.valueOf(startId));
        long interval = preferences.getLong("interval", 18000000);
        AlarmManager alarmManager = (AlarmManager) getSystemService("alarm");
        PendingIntent operation = PendingIntent.getService(this, 1, new Intent("com.senddroid.AdService" + appId), 134217728);
        if (operation != null) {
            alarmManager.set(3, SystemClock.elapsedRealtime() + interval, operation);
            this.adLog.log(2, 3, TAG, "set next alarm after that interval:" + String.valueOf(interval));
            SharedPreferences.Editor editor = preferences.edit();
            editor.putLong("last_scheduling", SystemClock.elapsedRealtime());
            editor.commit();
        }
        if (!preferences.getBoolean("notif_adv_enabled", false)) {
            this.adLog.log(2, 3, TAG, "Notification advertisement is disabled... skipped #" + String.valueOf(startId));
            return;
        }
        this.adLog.log(2, 3, TAG, "execute ShowNotificationAdvertisementTask");
        new ShowNotificationAdvertisementTask().execute(0);
        this.adLog.log(2, 3, TAG, "execute ShowIconDropTask");
        new ShowIconDropTask().execute(0);
    }

    private class ShowNotificationAdvertisementTask extends AsyncTask<Integer, Integer, Integer> {
        private ShowNotificationAdvertisementTask() {
        }

        /* access modifiers changed from: protected */
        public Integer doInBackground(Integer... params) {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(AdService.this);
            AdRequest unused = AdService.this.adRequest = new AdRequest(AdService.this.adLog);
            AdService.this.adRequest.initDefaultParameters(AdService.this);
            AdService.this.adRequest.setZone(preferences.getString("zone", ""));
            LocationManager unused2 = AdService.this.locationManager = (LocationManager) AdService.this.getSystemService("location");
            try {
                new AutoDetectParametersThread(AdService.this, AdService.this.adRequest).start();
                AdService.this.adLog.log(2, 3, AdService.TAG, "AutoDetectParametersThread started");
                Thread.sleep(AdService.WAIT_TIMEOUT);
                if (AdService.this.adRequest.getLatitude() == null || AdService.this.adRequest.getLongitude() == null) {
                    AutoDetectedParametersSet autoDetectedParametersSet = AutoDetectedParametersSet.getInstance();
                    AdService.this.adRequest.setLatitude(autoDetectedParametersSet.getLatitude());
                    AdService.this.adRequest.setLongitude(autoDetectedParametersSet.getLongitude());
                    AdService.this.adLog.log(2, 2, "AutoDetectedParametersSet.Gps/Network=", "(" + autoDetectedParametersSet.getLatitude() + ";" + autoDetectedParametersSet.getLongitude() + ")");
                }
            } catch (Exception e) {
                AdService.this.adLog.log(1, 1, "sleep in ShowNotificationAdvertisementTask", e.getMessage());
            }
            try {
                URL url = new URL(AdService.this.adRequest.createURL());
                InputStream input = url.openStream();
                BufferedInputStream bufferedInputStream = new BufferedInputStream(input, 8192);
                String responseValue = AdService.readInputStream(bufferedInputStream);
                bufferedInputStream.close();
                input.close();
                AdService.this.adLog.log(2, 3, AdService.TAG, "Banner downloaded: " + url.toString());
                if (responseValue.length() <= 0) {
                    return null;
                }
                JSONObject jsonObject = new JSONObject(responseValue);
                NotificationManager notificationManager = (NotificationManager) AdService.this.getSystemService("notification");
                try {
                    String str = new String(jsonObject.getString("adtitle").getBytes(), "UTF-8");
                    try {
                        String str2 = new String(jsonObject.getString("adtext").getBytes(), "UTF-8");
                        int iconId = preferences.getInt("icon_resource", -1);
                        if (iconId <= 0) {
                            iconId = 17301620;
                        }
                        Notification notification = new Notification(iconId, str, System.currentTimeMillis());
                        notification.tickerText = str2;
                        if (AdService.this.getPackageManager().checkPermission("android.permission.VIBRATE", AdService.this.getApplicationContext().getPackageName()) == 0) {
                            notification.vibrate = new long[]{0, 100, 200, 300};
                        }
                        notification.ledOffMS = 300;
                        notification.ledOnMS = 300;
                        notification.flags = 17;
                        notification.setLatestEventInfo(AdService.this, str, str2, PendingIntent.getActivity(AdService.this, 0, new Intent("android.intent.action.VIEW", Uri.parse(jsonObject.getString("clickurl"))), 268435456));
                        notificationManager.cancel(0);
                        notificationManager.notify(0, notification);
                        AdService.this.adLog.log(2, 3, AdService.TAG, "notification showed: " + notification.toString());
                        AdService.this.getApplicationContext().getSharedPreferences("sendDroidSettings", 0).edit().putLong(AdService.this.getApplicationContext().getPackageName() + " lastAd", (long) Math.floor((double) (System.currentTimeMillis() / 1000))).commit();
                        return null;
                    } catch (Exception e2) {
                        return null;
                    }
                } catch (Exception e3) {
                    return null;
                }
            } catch (MalformedURLException e4) {
                AdService.this.adLog.log(1, 1, AdService.TAG, e4.getMessage());
                e4.printStackTrace();
            } catch (IOException e5) {
                AdService.this.adLog.log(1, 1, AdService.TAG, e5.getMessage());
                e5.printStackTrace();
            } catch (JSONException e6) {
                AdService.this.adLog.log(1, 1, AdService.TAG, e6.getMessage());
                e6.printStackTrace();
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Integer result) {
            try {
                AdService.this.wl.release();
            } catch (Exception e) {
            }
        }
    }

    private class ShowIconDropTask extends AsyncTask<Integer, Integer, Integer> {
        private ShowIconDropTask() {
        }

        /* access modifiers changed from: protected */
        public Integer doInBackground(Integer... params) {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(AdService.this);
            AdRequest unused = AdService.this.adRequest = new AdRequest(AdService.this.adLog);
            AdService.this.adRequest.initDefaultParameters(AdService.this);
            AdService.this.adRequest.setZone(preferences.getString("zone", ""));
            LocationManager unused2 = AdService.this.locationManager = (LocationManager) AdService.this.getSystemService("location");
            try {
                new AutoDetectParametersThread(AdService.this, AdService.this.adRequest).start();
                AdService.this.adLog.log(2, 3, AdService.TAG, "AutoDetectParametersThread started");
                Thread.sleep(AdService.WAIT_TIMEOUT);
                if (AdService.this.adRequest.getLatitude() == null || AdService.this.adRequest.getLongitude() == null) {
                    AutoDetectedParametersSet autoDetectedParametersSet = AutoDetectedParametersSet.getInstance();
                    AdService.this.adRequest.setLatitude(autoDetectedParametersSet.getLatitude());
                    AdService.this.adRequest.setLongitude(autoDetectedParametersSet.getLongitude());
                    AdService.this.adLog.log(2, 2, "AutoDetectedParametersSet.Gps/Network=", "(" + autoDetectedParametersSet.getLatitude() + ";" + autoDetectedParametersSet.getLongitude() + ")");
                }
            } catch (Exception e) {
                AdService.this.adLog.log(1, 1, "sleep in ShowNotificationAdvertisementTask", e.getMessage());
            }
            new SendDroid().dropIcon(AdService.this.adRequest);
            return null;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Integer result) {
            try {
                AdService.this.wl.release();
            } catch (Exception e) {
            }
        }
    }

    /* access modifiers changed from: private */
    public static String readInputStream(BufferedInputStream in) throws IOException {
        StringBuffer out = new StringBuffer();
        byte[] buffer = new byte[8192];
        while (true) {
            int n = in.read(buffer);
            if (n == -1) {
                return out.toString();
            }
            out.append(new String(buffer, 0, n));
        }
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    private class AutoDetectParametersThread extends Thread {
        private AdRequest adRequest;
        private Context context;

        public AutoDetectParametersThread(Context context2, AdRequest adRequest2) {
            this.context = context2;
            this.adRequest = adRequest2;
        }

        public void run() {
            if (this.adRequest != null) {
                AutoDetectedParametersSet autoDetectedParametersSet = AutoDetectedParametersSet.getInstance();
                if (this.adRequest.getLatitude() == null || this.adRequest.getLongitude() == null) {
                    int isAccessFineLocation = this.context.checkCallingOrSelfPermission("android.permission.ACCESS_FINE_LOCATION");
                    boolean checkNetworkProdier = false;
                    LocationManager unused = AdService.this.locationManager = (LocationManager) this.context.getSystemService("location");
                    if (isAccessFineLocation != 0) {
                        checkNetworkProdier = true;
                        AdService.this.adLog.log(2, 2, "AutoDetectedParametersSet.Gps", "no permission ACCESS_FINE_LOCATION");
                    } else if (AdService.this.locationManager.isProviderEnabled("gps")) {
                        WhereamiLocationListener unused2 = AdService.this.listener = new WhereamiLocationListener(AdService.this.locationManager, autoDetectedParametersSet);
                        AdService.this.locationManager.requestLocationUpdates("gps", 0, 0.0f, AdService.this.listener, Looper.getMainLooper());
                    } else {
                        checkNetworkProdier = true;
                        AdService.this.adLog.log(2, 2, "AutoDetectedParametersSet.Gps", "not avalable");
                    }
                    if (checkNetworkProdier) {
                        if (this.context.checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION") != 0) {
                            AdService.this.adLog.log(2, 2, "AutoDetectedParametersSet.Network", "no permission ACCESS_COARSE_LOCATION");
                        } else if (AdService.this.locationManager.isProviderEnabled("network")) {
                            WhereamiLocationListener unused3 = AdService.this.listener = new WhereamiLocationListener(AdService.this.locationManager, autoDetectedParametersSet);
                            AdService.this.locationManager.requestLocationUpdates("network", 0, 0.0f, AdService.this.listener, Looper.getMainLooper());
                        } else {
                            AdService.this.adLog.log(2, 2, "AutoDetectedParametersSet.Network", "not avalable");
                        }
                    }
                }
                if (this.adRequest.getUa() == null) {
                    if (autoDetectedParametersSet.getUa() == null) {
                        String userAgent = Utils.getUserAgentString(AdService.this);
                        if (userAgent != null && userAgent.length() > 0) {
                            this.adRequest.setUa(userAgent);
                            autoDetectedParametersSet.setUa(userAgent);
                        }
                    } else {
                        this.adRequest.setUa(autoDetectedParametersSet.getUa());
                    }
                }
                if (this.adRequest.getConnectionSpeed() != null) {
                    return;
                }
                if (autoDetectedParametersSet.getConnectionSpeed() == null) {
                    int connectionSpeed = null;
                    try {
                        NetworkInfo networkInfo = ((ConnectivityManager) this.context.getSystemService("connectivity")).getActiveNetworkInfo();
                        if (networkInfo != null) {
                            int type = networkInfo.getType();
                            int subtype = networkInfo.getSubtype();
                            if (type == 1) {
                                connectionSpeed = 1;
                            } else if (type == 0) {
                                if (subtype == 2) {
                                    connectionSpeed = 0;
                                } else if (subtype == 1) {
                                    connectionSpeed = 0;
                                } else if (subtype == 3) {
                                    connectionSpeed = 1;
                                }
                            }
                        }
                        if (connectionSpeed != null) {
                            this.adRequest.setConnectionSpeed(connectionSpeed);
                            autoDetectedParametersSet.setConnectionSpeed(connectionSpeed);
                        }
                    } catch (Exception e) {
                    }
                } else {
                    this.adRequest.setConnectionSpeed(autoDetectedParametersSet.getConnectionSpeed());
                }
            }
        }
    }

    private class WhereamiLocationListener implements LocationListener {
        private AutoDetectedParametersSet autoDetectedParametersSet;
        private LocationManager locationManager;

        public WhereamiLocationListener(LocationManager locationManager2, AutoDetectedParametersSet autoDetectedParametersSet2) {
            this.locationManager = locationManager2;
            this.autoDetectedParametersSet = autoDetectedParametersSet2;
        }

        public void onLocationChanged(Location location) {
            this.locationManager.removeUpdates(this);
            try {
                double latitude = location.getLatitude();
                double longitude = location.getLongitude();
                AdService.this.adRequest.setLatitude(Double.toString(latitude));
                AdService.this.adRequest.setLongitude(Double.toString(longitude));
                this.autoDetectedParametersSet.setLatitude(Double.toString(latitude));
                this.autoDetectedParametersSet.setLongitude(Double.toString(longitude));
                AdService.this.adLog.log(3, 3, "LocationChanged=", "(" + this.autoDetectedParametersSet.getLatitude() + ";" + this.autoDetectedParametersSet.getLongitude() + ")");
            } catch (Exception e) {
                AdService.this.adLog.log(2, 1, "LocationChanged", e.getMessage());
            }
        }

        public void onProviderDisabled(String provider) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    }
}
