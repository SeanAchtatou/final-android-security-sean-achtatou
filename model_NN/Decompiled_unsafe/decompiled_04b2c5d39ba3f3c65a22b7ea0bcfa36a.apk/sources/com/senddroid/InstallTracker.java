package com.senddroid;

import android.content.Context;
import android.util.Log;
import java.io.IOException;
import java.net.URLEncoder;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

public class InstallTracker {
    /* access modifiers changed from: private */
    public static String TRACK_HANDLER = "/install.php";
    /* access modifiers changed from: private */
    public static String TRACK_HOST = "push.senddroid.com";
    private static InstallTracker mInstance = null;
    /* access modifiers changed from: private */
    public AdLog adLog = new AdLog(this);
    /* access modifiers changed from: private */
    public String mAppId;
    /* access modifiers changed from: private */
    public Context mContext;
    /* access modifiers changed from: private */
    public String mPackageName;
    private Runnable mTrackInstall = new Runnable() {
        public void run() {
            StringBuilder sz = new StringBuilder("http://" + InstallTracker.TRACK_HOST + InstallTracker.TRACK_HANDLER);
            sz.append("?pkg=" + InstallTracker.this.mPackageName);
            if (InstallTracker.this.mAppId != null) {
                try {
                    sz.append("&app=" + URLEncoder.encode(InstallTracker.this.mAppId, "UTF-8"));
                } catch (Exception e) {
                }
            }
            sz.append("&udid=" + Utils.getDeviceIdMD5(InstallTracker.this.mContext));
            if (InstallTracker.this.ua != null) {
                try {
                    sz.append("&ua=" + URLEncoder.encode(InstallTracker.this.ua, "UTF-8"));
                } catch (Exception e2) {
                }
            }
            String url = sz.toString();
            InstallTracker.this.adLog.log(3, 3, "InstallTracker", "Install track: " + url);
            try {
                HttpResponse response = new DefaultHttpClient().execute(new HttpGet(url));
                if (response.getStatusLine().getStatusCode() != 200) {
                    Log.i("InstallTracker", "Install track failed: Status code " + response.getStatusLine().getStatusCode() + " != 200");
                    return;
                }
                Log.i("InstallTracker", "Install track successful");
                InstallTracker.this.mContext.getSharedPreferences("sendDroidSettings", 0).edit().putLong(InstallTracker.this.mPackageName + " installed", (long) Math.floor((double) (System.currentTimeMillis() / 1000))).commit();
            } catch (ClientProtocolException e3) {
                Log.i("InstallTracker", "Install track failed: ClientProtocolException (no signal?)");
            } catch (IOException e4) {
                Log.i("InstallTracker", "Install track failed: IOException (no signal?)");
            }
        }
    };
    /* access modifiers changed from: private */
    public String ua = null;

    private InstallTracker() {
    }

    public static InstallTracker getInstance() {
        if (mInstance == null) {
            mInstance = new InstallTracker();
        }
        return mInstance;
    }

    public void reportInstall(Context context) {
        reportInstall(context, null);
    }

    public void reportInstall(Context context, String appId) {
        if (context != null) {
            this.mContext = context;
            this.mAppId = appId;
            this.mPackageName = this.mContext.getPackageName();
            if (this.mContext.getSharedPreferences("sendDroidSettings", 0).getLong(this.mPackageName + " installed", 0) == 0) {
                this.ua = Utils.getUserAgentString(this.mContext);
                new Thread(this.mTrackInstall).start();
                return;
            }
            Log.i("InstallTracker", "Install already tracked");
        }
    }

    public void setLogLevel(int logLevel) {
        this.adLog.setLogLevel(logLevel);
    }
}
