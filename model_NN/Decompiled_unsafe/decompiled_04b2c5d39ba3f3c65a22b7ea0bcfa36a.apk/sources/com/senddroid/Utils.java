package com.senddroid;

import android.content.Context;
import android.telephony.TelephonyManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.lang.reflect.Constructor;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

public class Utils {
    private static final String INSTALLATION = "INSTALLATION";
    private static String sID = null;
    private static String userAgent = null;

    public static String scrape(String resp, String start, String stop) {
        int len;
        int offset = resp.indexOf(start);
        if (offset >= 0 && (len = resp.indexOf(stop, start.length() + offset)) >= 0) {
            return resp.substring(start.length() + offset, len);
        }
        return "";
    }

    public static String md5(String data) {
        try {
            MessageDigest digester = MessageDigest.getInstance("MD5");
            digester.update(data.getBytes());
            return byteArrayToHexString(digester.digest());
        } catch (NoSuchAlgorithmException e) {
            return null;
        }
    }

    public static String byteArrayToHexString(byte[] array) {
        StringBuffer hexString = new StringBuffer();
        for (byte b : array) {
            int intVal = b & 255;
            if (intVal < 16) {
                hexString.append("0");
            }
            hexString.append(Integer.toHexString(intVal));
        }
        return hexString.toString();
    }

    public static String getUserAgentString(Context context) {
        Constructor<WebSettings> constructor;
        if (userAgent == null) {
            try {
                constructor = WebSettings.class.getDeclaredConstructor(Context.class, WebView.class);
                constructor.setAccessible(true);
                userAgent = constructor.newInstance(context, null).getUserAgentString();
                constructor.setAccessible(false);
            } catch (Exception e) {
                return null;
            } catch (Throwable th) {
                constructor.setAccessible(false);
                throw th;
            }
        }
        return userAgent;
    }

    public static synchronized String id(Context context) {
        String str;
        synchronized (Utils.class) {
            if (sID == null) {
                File installation = new File(context.getFilesDir(), INSTALLATION);
                try {
                    if (!installation.exists()) {
                        writeInstallationFile(installation);
                    }
                    sID = readInstallationFile(installation);
                } catch (Exception e) {
                    sID = "1234567890";
                }
            }
            str = sID;
        }
        return str;
    }

    private static String readInstallationFile(File installation) throws IOException {
        RandomAccessFile f = new RandomAccessFile(installation, "r");
        byte[] bytes = new byte[((int) f.length())];
        f.readFully(bytes);
        f.close();
        return new String(bytes);
    }

    private static void writeInstallationFile(File installation) throws IOException {
        FileOutputStream out = new FileOutputStream(installation);
        out.write(UUID.randomUUID().toString().getBytes());
        out.close();
    }

    public static String getDeviceIdMD5(Context context) {
        String deviceId = ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
        if (deviceId == null) {
            deviceId = id(context);
        }
        return md5(deviceId);
    }
}
