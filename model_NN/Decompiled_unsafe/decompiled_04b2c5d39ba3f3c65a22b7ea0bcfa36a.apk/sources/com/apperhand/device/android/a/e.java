package com.apperhand.device.android.a;

import android.content.Context;
import android.content.SharedPreferences;

/* compiled from: AndroidTerminateDMA */
public final class e implements com.apperhand.device.a.a.e {
    private Context a;

    public e(Context context) {
        this.a = context;
    }

    public final void a() {
        SharedPreferences.Editor edit = this.a.getSharedPreferences("com.apperhand.global", 0).edit();
        edit.putBoolean("TERMINATE", true);
        edit.commit();
    }
}
