package com.apperhand.device.android.c;

import org.apache.http.HttpResponse;
import org.apache.http.message.AbstractHttpMessage;

/* compiled from: NetworkUtils */
public final class c {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.apperhand.device.android.c.c.a(java.lang.String, org.apache.http.message.AbstractHttpMessage, org.apache.http.HttpResponse):java.lang.StringBuilder
     arg types: [java.lang.String, org.apache.http.client.methods.HttpPost, org.apache.http.HttpResponse]
     candidates:
      com.apperhand.device.android.c.c.a(java.lang.String, byte[], java.util.List<org.apache.http.Header>):java.lang.String
      com.apperhand.device.android.c.c.a(java.lang.String, org.apache.http.message.AbstractHttpMessage, org.apache.http.HttpResponse):java.lang.StringBuilder */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:60:0x0165=Splitter:B:60:0x0165, B:23:0x00af=Splitter:B:23:0x00af} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String a(java.lang.String r9, byte[] r10, java.util.List<org.apache.http.Header> r11) throws com.apperhand.device.a.d.f {
        /*
            r2 = 60000(0xea60, float:8.4078E-41)
            r1 = 0
            r4 = 0
            org.apache.http.params.BasicHttpParams r0 = new org.apache.http.params.BasicHttpParams
            r0.<init>()
            org.apache.http.params.HttpConnectionParams.setConnectionTimeout(r0, r2)
            org.apache.http.params.HttpConnectionParams.setSoTimeout(r0, r2)
            org.apache.http.impl.client.DefaultHttpClient r5 = new org.apache.http.impl.client.DefaultHttpClient
            r5.<init>(r0)
            com.apperhand.device.android.c.c$1 r0 = new com.apperhand.device.android.c.c$1
            r0.<init>()
            r5.setHttpRequestRetryHandler(r0)
            org.apache.http.client.methods.HttpPost r6 = new org.apache.http.client.methods.HttpPost
            r6.<init>(r9)
            if (r11 == 0) goto L_0x0038
            java.util.Iterator r2 = r11.iterator()
        L_0x0028:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x0038
            java.lang.Object r0 = r2.next()
            org.apache.http.Header r0 = (org.apache.http.Header) r0
            r6.setHeader(r0)
            goto L_0x0028
        L_0x0038:
            org.apache.http.params.HttpParams r0 = r6.getParams()
            java.lang.String r2 = "http.protocol.expect-continue"
            r0.setBooleanParameter(r2, r4)
            java.lang.String r0 = "Content-Type"
            java.lang.String r2 = "application/json"
            r6.setHeader(r0, r2)
            java.lang.String r0 = "Accept-Encoding"
            java.lang.String r2 = "gzip"
            r6.setHeader(r0, r2)
            java.lang.String r0 = "Accept"
            java.lang.String r2 = "application/json"
            r6.setHeader(r0, r2)
            int r0 = r10.length     // Catch:{ IOException -> 0x0104 }
            r2 = 2048(0x800, float:2.87E-42)
            if (r0 >= r2) goto L_0x00e4
            org.apache.http.entity.ByteArrayEntity r0 = new org.apache.http.entity.ByteArrayEntity     // Catch:{ IOException -> 0x0104 }
            r0.<init>(r10)     // Catch:{ IOException -> 0x0104 }
        L_0x0060:
            r6.setEntity(r0)     // Catch:{ IOException -> 0x0104 }
        L_0x0063:
            org.apache.http.HttpResponse r3 = r5.execute(r6)     // Catch:{ IOException -> 0x01ab, RuntimeException -> 0x01a8 }
            org.apache.http.HttpEntity r0 = r3.getEntity()     // Catch:{ IOException -> 0x00ad, RuntimeException -> 0x0164 }
            org.apache.http.StatusLine r2 = r3.getStatusLine()     // Catch:{ IOException -> 0x00ad, RuntimeException -> 0x0164 }
            int r2 = r2.getStatusCode()     // Catch:{ IOException -> 0x00ad, RuntimeException -> 0x0164 }
            r7 = 200(0xc8, float:2.8E-43)
            if (r2 == r7) goto L_0x010f
            if (r0 == 0) goto L_0x007c
            r0.consumeContent()     // Catch:{ IOException -> 0x00ad, RuntimeException -> 0x0164 }
        L_0x007c:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00ad, RuntimeException -> 0x0164 }
            r0.<init>()     // Catch:{ IOException -> 0x00ad, RuntimeException -> 0x0164 }
            java.lang.String r1 = "Status code is "
            java.lang.StringBuilder r1 = r0.append(r1)     // Catch:{ IOException -> 0x00ad, RuntimeException -> 0x0164 }
            org.apache.http.StatusLine r2 = r3.getStatusLine()     // Catch:{ IOException -> 0x00ad, RuntimeException -> 0x0164 }
            int r2 = r2.getStatusCode()     // Catch:{ IOException -> 0x00ad, RuntimeException -> 0x0164 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ IOException -> 0x00ad, RuntimeException -> 0x0164 }
            java.lang.String r2 = ", "
            r1.append(r2)     // Catch:{ IOException -> 0x00ad, RuntimeException -> 0x0164 }
            java.lang.StringBuilder r1 = a(r9, r6, r3)     // Catch:{ IOException -> 0x00ad, RuntimeException -> 0x0164 }
            r0.append(r1)     // Catch:{ IOException -> 0x00ad, RuntimeException -> 0x0164 }
            com.apperhand.device.a.d.f r1 = new com.apperhand.device.a.d.f     // Catch:{ IOException -> 0x00ad, RuntimeException -> 0x0164 }
            com.apperhand.device.a.d.f$a r2 = com.apperhand.device.a.d.f.a.GENERAL_ERROR     // Catch:{ IOException -> 0x00ad, RuntimeException -> 0x0164 }
            java.lang.String r0 = r0.toString()     // Catch:{ IOException -> 0x00ad, RuntimeException -> 0x0164 }
            r4 = 0
            r7 = 0
            r1.<init>(r2, r0, r4, r7)     // Catch:{ IOException -> 0x00ad, RuntimeException -> 0x0164 }
            throw r1     // Catch:{ IOException -> 0x00ad, RuntimeException -> 0x0164 }
        L_0x00ad:
            r0 = move-exception
            r1 = r3
        L_0x00af:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x00db }
            r2.<init>()     // Catch:{ all -> 0x00db }
            java.lang.String r3 = "Error execute Exception "
            java.lang.StringBuilder r3 = r2.append(r3)     // Catch:{ all -> 0x00db }
            java.lang.String r4 = r0.getMessage()     // Catch:{ all -> 0x00db }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x00db }
            java.lang.String r4 = ", "
            r3.append(r4)     // Catch:{ all -> 0x00db }
            java.lang.StringBuilder r1 = a(r9, r6, r1)     // Catch:{ all -> 0x00db }
            r2.append(r1)     // Catch:{ all -> 0x00db }
            com.apperhand.device.a.d.f r1 = new com.apperhand.device.a.d.f     // Catch:{ all -> 0x00db }
            com.apperhand.device.a.d.f$a r3 = com.apperhand.device.a.d.f.a.GENERAL_ERROR     // Catch:{ all -> 0x00db }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x00db }
            r4 = 0
            r1.<init>(r3, r2, r0, r4)     // Catch:{ all -> 0x00db }
            throw r1     // Catch:{ all -> 0x00db }
        L_0x00db:
            r0 = move-exception
            org.apache.http.conn.ClientConnectionManager r1 = r5.getConnectionManager()
            r1.shutdown()
            throw r0
        L_0x00e4:
            java.io.ByteArrayOutputStream r2 = new java.io.ByteArrayOutputStream     // Catch:{ IOException -> 0x0104 }
            r2.<init>()     // Catch:{ IOException -> 0x0104 }
            java.util.zip.GZIPOutputStream r0 = new java.util.zip.GZIPOutputStream     // Catch:{ IOException -> 0x0104 }
            r0.<init>(r2)     // Catch:{ IOException -> 0x0104 }
            r0.write(r10)     // Catch:{ IOException -> 0x0104 }
            r0.close()     // Catch:{ IOException -> 0x0104 }
            org.apache.http.entity.ByteArrayEntity r0 = new org.apache.http.entity.ByteArrayEntity     // Catch:{ IOException -> 0x0104 }
            byte[] r2 = r2.toByteArray()     // Catch:{ IOException -> 0x0104 }
            r0.<init>(r2)     // Catch:{ IOException -> 0x0104 }
            java.lang.String r2 = "gzip"
            r0.setContentEncoding(r2)     // Catch:{ IOException -> 0x0104 }
            goto L_0x0060
        L_0x0104:
            r0 = move-exception
            org.apache.http.entity.ByteArrayEntity r0 = new org.apache.http.entity.ByteArrayEntity
            r0.<init>(r10)
            r6.setEntity(r0)
            goto L_0x0063
        L_0x010f:
            if (r0 == 0) goto L_0x01b3
            java.io.InputStream r2 = r0.getContent()     // Catch:{ IOException -> 0x00ad, RuntimeException -> 0x0164 }
            org.apache.http.Header r0 = r0.getContentEncoding()     // Catch:{ all -> 0x01ae }
            if (r0 == 0) goto L_0x0139
            if (r2 == 0) goto L_0x0139
            org.apache.http.HeaderElement[] r7 = r0.getElements()     // Catch:{ all -> 0x01ae }
            r0 = r4
        L_0x0122:
            int r4 = r7.length     // Catch:{ all -> 0x01ae }
            if (r0 >= r4) goto L_0x0139
            r4 = r7[r0]     // Catch:{ all -> 0x01ae }
            java.lang.String r4 = r4.getName()     // Catch:{ all -> 0x01ae }
            java.lang.String r8 = "gzip"
            boolean r4 = r4.equalsIgnoreCase(r8)     // Catch:{ all -> 0x01ae }
            if (r4 == 0) goto L_0x0194
            java.util.zip.GZIPInputStream r0 = new java.util.zip.GZIPInputStream     // Catch:{ all -> 0x01ae }
            r0.<init>(r2)     // Catch:{ all -> 0x01ae }
            r2 = r0
        L_0x0139:
            if (r2 == 0) goto L_0x01b1
            java.io.StringWriter r0 = new java.io.StringWriter     // Catch:{ all -> 0x015c }
            r0.<init>()     // Catch:{ all -> 0x015c }
            r1 = 1024(0x400, float:1.435E-42)
            char[] r1 = new char[r1]     // Catch:{ all -> 0x015c }
            java.io.BufferedReader r4 = new java.io.BufferedReader     // Catch:{ all -> 0x015c }
            java.io.InputStreamReader r7 = new java.io.InputStreamReader     // Catch:{ all -> 0x015c }
            java.lang.String r8 = "UTF-8"
            r7.<init>(r2, r8)     // Catch:{ all -> 0x015c }
            r4.<init>(r7)     // Catch:{ all -> 0x015c }
        L_0x0150:
            int r7 = r4.read(r1)     // Catch:{ all -> 0x015c }
            r8 = -1
            if (r7 == r8) goto L_0x0197
            r8 = 0
            r0.write(r1, r8, r7)     // Catch:{ all -> 0x015c }
            goto L_0x0150
        L_0x015c:
            r0 = move-exception
            r1 = r2
        L_0x015e:
            if (r1 == 0) goto L_0x0163
            r1.close()     // Catch:{ IOException -> 0x00ad, RuntimeException -> 0x0164 }
        L_0x0163:
            throw r0     // Catch:{ IOException -> 0x00ad, RuntimeException -> 0x0164 }
        L_0x0164:
            r0 = move-exception
        L_0x0165:
            r6.abort()     // Catch:{ all -> 0x00db }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x00db }
            r1.<init>()     // Catch:{ all -> 0x00db }
            java.lang.String r2 = "Error execute Exception "
            java.lang.StringBuilder r2 = r1.append(r2)     // Catch:{ all -> 0x00db }
            java.lang.String r4 = r0.getMessage()     // Catch:{ all -> 0x00db }
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ all -> 0x00db }
            java.lang.String r4 = ", "
            r2.append(r4)     // Catch:{ all -> 0x00db }
            java.lang.StringBuilder r2 = a(r9, r6, r3)     // Catch:{ all -> 0x00db }
            r1.append(r2)     // Catch:{ all -> 0x00db }
            com.apperhand.device.a.d.f r2 = new com.apperhand.device.a.d.f     // Catch:{ all -> 0x00db }
            com.apperhand.device.a.d.f$a r3 = com.apperhand.device.a.d.f.a.GENERAL_ERROR     // Catch:{ all -> 0x00db }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x00db }
            r4 = 0
            r2.<init>(r3, r1, r0, r4)     // Catch:{ all -> 0x00db }
            throw r2     // Catch:{ all -> 0x00db }
        L_0x0194:
            int r0 = r0 + 1
            goto L_0x0122
        L_0x0197:
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x015c }
        L_0x019b:
            if (r2 == 0) goto L_0x01a0
            r2.close()     // Catch:{ IOException -> 0x00ad, RuntimeException -> 0x0164 }
        L_0x01a0:
            org.apache.http.conn.ClientConnectionManager r1 = r5.getConnectionManager()
            r1.shutdown()
            return r0
        L_0x01a8:
            r0 = move-exception
            r3 = r1
            goto L_0x0165
        L_0x01ab:
            r0 = move-exception
            goto L_0x00af
        L_0x01ae:
            r0 = move-exception
            r1 = r2
            goto L_0x015e
        L_0x01b1:
            r0 = r1
            goto L_0x019b
        L_0x01b3:
            r0 = r1
            goto L_0x01a0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.apperhand.device.android.c.c.a(java.lang.String, byte[], java.util.List):java.lang.String");
    }

    private static StringBuilder a(String str, AbstractHttpMessage abstractHttpMessage, HttpResponse httpResponse) {
        StringBuilder sb = new StringBuilder();
        if (str != null) {
            sb.append("address = [").append(str).append("],");
        }
        if (!(abstractHttpMessage == null || abstractHttpMessage.getAllHeaders() == null)) {
            sb.append("Headers = [").append(abstractHttpMessage.getAllHeaders()).append("],");
        }
        if (!(httpResponse == null || httpResponse.getStatusLine() == null)) {
            sb.append("statusLine = [").append(httpResponse.getStatusLine()).append("]");
        }
        return sb;
    }
}
