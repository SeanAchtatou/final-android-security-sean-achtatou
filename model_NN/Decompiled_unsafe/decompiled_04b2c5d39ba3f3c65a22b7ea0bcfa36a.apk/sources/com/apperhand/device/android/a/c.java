package com.apperhand.device.android.a;

import android.content.Context;
import com.apperhand.common.dto.Homepage;
import com.apperhand.device.android.a.a.a;
import java.util.List;

/* compiled from: AndroidHomepageDMA */
public final class c implements com.apperhand.device.a.a.c {
    private Context a;

    public c(Context context) {
        this.a = context;
    }

    public final boolean a(Homepage homepage) {
        boolean z;
        boolean z2 = false;
        try {
            Context context = this.a;
            List<a> a2 = a.C0002a.a();
            if (a2 != null && a2.size() > 0) {
                for (a a3 : a2) {
                    try {
                        if (a3.a(this.a, homepage)) {
                            z = true;
                        } else {
                            z = z2;
                        }
                        z2 = z;
                    } catch (Throwable th) {
                    }
                }
            }
        } catch (Throwable th2) {
        }
        return z2;
    }
}
