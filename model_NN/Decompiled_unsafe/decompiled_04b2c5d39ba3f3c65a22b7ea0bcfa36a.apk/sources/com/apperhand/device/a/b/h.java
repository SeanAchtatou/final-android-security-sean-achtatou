package com.apperhand.device.a.b;

import com.apperhand.common.dto.Command;
import com.apperhand.common.dto.CommandStatus;
import com.apperhand.common.dto.Shortcut;
import com.apperhand.common.dto.protocol.BaseResponse;
import com.apperhand.common.dto.protocol.CommandStatusRequest;
import com.apperhand.common.dto.protocol.ShortcutRequest;
import com.apperhand.common.dto.protocol.ShortcutResponse;
import com.apperhand.device.a.a;
import com.apperhand.device.a.a.d;
import com.apperhand.device.a.b;
import com.apperhand.device.a.d.c;
import com.apperhand.device.a.d.f;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* compiled from: ShortcutsService */
public final class h extends b {
    private d g;
    private String h = "";
    private String i = null;
    private boolean j = false;

    public h(b bVar, a aVar, String str, Command command) {
        super(bVar, aVar, str, command.getCommand());
        this.g = aVar.e();
    }

    /* access modifiers changed from: protected */
    public final Map<String, Object> a(BaseResponse baseResponse) throws f {
        List<Shortcut> shortcutList = ((ShortcutResponse) baseResponse).getShortcutList();
        if (shortcutList == null) {
            return null;
        }
        for (Shortcut next : shortcutList) {
            switch (next.getStatus()) {
                case ADD:
                    try {
                        if (!this.g.a(next)) {
                            this.g.b(next);
                        } else {
                            this.j = true;
                        }
                    } catch (f e) {
                        this.h = e.getMessage();
                        this.g.b(next);
                    }
                    this.i = next.getName();
                    break;
                default:
                    this.b.a(c.a.ERROR, this.a, String.format("Unknown action %s for shortcut %s", next.getStatus(), next.toString()));
                    break;
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public final BaseResponse a() throws f {
        this.g.a();
        ShortcutRequest shortcutRequest = new ShortcutRequest();
        shortcutRequest.setApplicationDetails(this.e.i());
        shortcutRequest.setSupportLauncher(Boolean.valueOf(this.g.c()));
        return a(shortcutRequest);
    }

    private BaseResponse a(ShortcutRequest shortcutRequest) {
        try {
            return (ShortcutResponse) this.e.b().a(shortcutRequest, Command.Commands.SHORTCUTS, ShortcutResponse.class);
        } catch (f e) {
            this.e.a().a(c.a.DEBUG, this.a, "Unable to handle Shortcut command!!!!", e);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Map<String, Object> map) throws f {
        a(b());
    }

    /* access modifiers changed from: protected */
    public final CommandStatusRequest b() throws f {
        boolean z;
        HashMap hashMap;
        List<CommandStatus> a;
        CommandStatusRequest b = super.b();
        if (!this.j) {
            z = true;
        } else {
            z = false;
        }
        if (!this.g.c()) {
            a = a(Command.Commands.SHORTCUTS, CommandStatus.Status.SUCCESS_WITH_WARNING, "Trying to used the following : [" + this.g.b() + "]", null);
        } else {
            if (this.i == null || !z) {
                hashMap = null;
            } else {
                hashMap = new HashMap();
                hashMap.put("PARAMETER", String.valueOf(this.g.a(this.i, 5)));
            }
            a = a(Command.Commands.SHORTCUTS, z ? CommandStatus.Status.SUCCESS : CommandStatus.Status.FAILURE, "Sababa!!!" + String.format(", used [%s] as launcher", this.g.b()), hashMap);
        }
        b.setStatuses(a);
        return b;
    }
}
