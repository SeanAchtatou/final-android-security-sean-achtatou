package com.apperhand.device.a;

import com.apperhand.common.dto.Command;
import com.apperhand.common.dto.protocol.CommandsRequest;
import com.apperhand.common.dto.protocol.CommandsResponse;
import com.apperhand.device.a.b.g;
import com.apperhand.device.a.d.c;
import com.apperhand.device.a.d.e;
import java.util.Collection;
import java.util.List;

/* compiled from: SDKManager */
public abstract class b {
    protected static final String c = b.class.getSimpleName();
    private long a;
    private String b = null;
    private boolean d;
    private boolean e;
    private a f;

    /* access modifiers changed from: protected */
    public abstract void a(String str);

    /* access modifiers changed from: protected */
    public abstract String b();

    /* access modifiers changed from: protected */
    public abstract void c();

    public b(a aVar, boolean z) {
        this.f = aVar;
        this.a = 60;
        this.d = z;
        this.e = true;
    }

    public void a() {
        List<Command> commands;
        boolean z = false;
        try {
            this.b = b();
            CommandsRequest commandsRequest = new CommandsRequest();
            Collection<String> b2 = this.f.h().b();
            if (!Boolean.valueOf(this.f.h().a("ACTIVATED", "false")).booleanValue() || (b2 != null && b2.size() > 0)) {
                z = true;
            }
            commandsRequest.setNeedSpecificParameters(z);
            commandsRequest.setInitiationType(this.d ? "first time" : "schedule");
            commandsRequest.setApplicationDetails(this.f.i());
            CommandsResponse commandsResponse = (CommandsResponse) this.f.b().a(commandsRequest, Command.Commands.COMMANDS, CommandsResponse.class);
            if (!commandsResponse.isValidResponse()) {
                a(86400);
                this.f.a().a(c.a.ERROR, c, "Server Error in getCommands. Next command = [86400] seconds");
                commands = null;
            } else {
                a(commandsResponse.getCommandsInterval());
                b(e.a(commandsResponse));
                commands = commandsResponse.getCommands();
            }
            if (commands != null) {
                c();
                for (Command next : commands) {
                    com.apperhand.device.a.b.b a2 = g.a(this, next, this.f);
                    if (a2 != null) {
                        a2.c();
                    } else {
                        this.f.a().a(c.a.DEBUG, c, String.format("Uknown command [command = %s] !!!", next));
                    }
                }
            }
        } catch (Throwable th) {
            this.f.a().a(c.a.ERROR, c, "Error handling unexpected error!!!", th);
        }
    }

    public final void a(long j) {
        if (j > 0) {
            this.a = j;
        }
    }

    public final long d() {
        return this.a;
    }

    public final String e() {
        return this.b;
    }

    public final void b(String str) {
        if (str != null) {
            if (str.length() <= 0) {
                str = null;
            }
            this.b = str;
            a(this.b);
        }
    }

    public final void f() {
        this.e = false;
    }

    public final boolean g() {
        return this.e;
    }
}
