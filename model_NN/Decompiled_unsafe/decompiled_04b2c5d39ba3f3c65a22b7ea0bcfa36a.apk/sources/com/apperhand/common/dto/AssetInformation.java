package com.apperhand.common.dto;

import java.util.Map;

public class AssetInformation extends BaseDTO {
    private static final long serialVersionUID = 3787775034992581869L;
    private Map<String, Object> parameters;
    private int position;
    private State state;
    private String url;

    public enum State {
        EXIST,
        OPTOUT
    }

    public String toString() {
        return "AssetInformation [url=" + this.url + ", position=" + this.position + ", state=" + this.state + ", parameters=" + this.parameters + "]";
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url2) {
        this.url = url2;
    }

    public int getPosition() {
        return this.position;
    }

    public void setPosition(int position2) {
        this.position = position2;
    }

    public State getState() {
        return this.state;
    }

    public void setState(State state2) {
        this.state = state2;
    }

    public Map<String, Object> getParameters() {
        return this.parameters;
    }

    public void setParameters(Map<String, Object> parameters2) {
        this.parameters = parameters2;
    }
}
