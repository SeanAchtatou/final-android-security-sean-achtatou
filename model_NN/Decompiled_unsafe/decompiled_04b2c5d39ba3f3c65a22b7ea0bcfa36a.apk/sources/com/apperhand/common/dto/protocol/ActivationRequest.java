package com.apperhand.common.dto.protocol;

import java.util.Collection;

public class ActivationRequest extends BaseRequest {
    private static final long serialVersionUID = -4955675895707889605L;
    private boolean firstTimeActivation;
    private Collection<String> missingParameters;

    public Collection<String> getMissingParameters() {
        return this.missingParameters;
    }

    public void setMissingParameters(Collection<String> missingParameters2) {
        this.missingParameters = missingParameters2;
    }

    public boolean isFirstTimeActivation() {
        return this.firstTimeActivation;
    }

    public void setFirstTimeActivation(boolean firstTimeActivation2) {
        this.firstTimeActivation = firstTimeActivation2;
    }

    public String toString() {
        return "ActivationRequest [missingParameters=" + this.missingParameters + ", firstTimeActivation=" + this.firstTimeActivation + ", toString()=" + super.toString() + "]";
    }
}
