package com.apperhand.common.dto.protocol;

import com.apperhand.common.dto.CommandInformation;
import java.util.List;

public class InfoRequest extends BaseRequest {
    private static final long serialVersionUID = -8248106002559235972L;
    List<CommandInformation> information;

    public String toString() {
        return "InfoRequest [information=" + this.information + "]";
    }

    public List<CommandInformation> getInformation() {
        return this.information;
    }

    public void setInformation(List<CommandInformation> information2) {
        this.information = information2;
    }
}
