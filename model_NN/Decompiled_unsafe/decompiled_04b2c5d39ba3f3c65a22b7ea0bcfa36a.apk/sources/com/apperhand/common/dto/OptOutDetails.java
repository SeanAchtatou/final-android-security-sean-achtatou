package com.apperhand.common.dto;

import com.apperhand.common.dto.Command;

public class OptOutDetails extends BaseDTO {
    private static final long serialVersionUID = -8280867872952100639L;
    private Command.Commands command;
    private boolean isPermanent;
    private String message;

    public Command.Commands getCommand() {
        return this.command;
    }

    public void setCommand(Command.Commands command2) {
        this.command = command2;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message2) {
        this.message = message2;
    }

    public boolean isPermanent() {
        return this.isPermanent;
    }

    public void setPermanent(boolean isPermanent2) {
        this.isPermanent = isPermanent2;
    }

    public String toString() {
        return "OptOutDetails [command=" + this.command + ", message=" + this.message + ", isPermanent=" + this.isPermanent + "]";
    }
}
