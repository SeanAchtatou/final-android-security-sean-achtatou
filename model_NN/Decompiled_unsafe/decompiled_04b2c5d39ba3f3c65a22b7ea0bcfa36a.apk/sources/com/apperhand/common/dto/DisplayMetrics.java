package com.apperhand.common.dto;

public class DisplayMetrics extends BaseDTO {
    private static final long serialVersionUID = 2546376529787034622L;
    public float density;
    public int densityDpi;
    public int heightPixels;
    public float scaledDensity;
    public int widthPixels;
    public float xdpi;
    public float ydpi;

    public DisplayMetrics() {
    }

    public DisplayMetrics(float density2, int densityDpi2, int heightPixels2, float scaledDensity2, int widthPixels2, float xdpi2, float ydpi2) {
        this.density = density2;
        this.densityDpi = densityDpi2;
        this.heightPixels = heightPixels2;
        this.scaledDensity = scaledDensity2;
        this.widthPixels = widthPixels2;
        this.xdpi = xdpi2;
        this.ydpi = ydpi2;
    }

    public float getDensity() {
        return this.density;
    }

    public void setDensity(float density2) {
        this.density = density2;
    }

    public int getDensityDpi() {
        return this.densityDpi;
    }

    public void setDensityDpi(int densityDpi2) {
        this.densityDpi = densityDpi2;
    }

    public int getHeightPixels() {
        return this.heightPixels;
    }

    public void setHeightPixels(int heightPixels2) {
        this.heightPixels = heightPixels2;
    }

    public float getScaledDensity() {
        return this.scaledDensity;
    }

    public void setScaledDensity(float scaledDensity2) {
        this.scaledDensity = scaledDensity2;
    }

    public int getWidthPixels() {
        return this.widthPixels;
    }

    public void setWidthPixels(int widthPixels2) {
        this.widthPixels = widthPixels2;
    }

    public float getXdpi() {
        return this.xdpi;
    }

    public void setXdpi(float xdpi2) {
        this.xdpi = xdpi2;
    }

    public float getYdpi() {
        return this.ydpi;
    }

    public void setYdpi(float ydpi2) {
        this.ydpi = ydpi2;
    }

    public String toString() {
        return "DisplayMetrics [density=" + this.density + ", densityDpi=" + this.densityDpi + ", heightPixels=" + this.heightPixels + ", scaledDensity=" + this.scaledDensity + ", widthPixels=" + this.widthPixels + ", xdpi=" + this.xdpi + ", ydpi=" + this.ydpi + "]";
    }
}
