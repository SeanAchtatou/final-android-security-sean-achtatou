package org.codehaus.jackson.impl;

import java.io.IOException;
import java.io.Reader;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonToken;
import org.codehaus.jackson.io.IOContext;

public abstract class ReaderBasedNumericParser extends ReaderBasedParserBase {
    public ReaderBasedNumericParser(IOContext pc, int features, Reader r) {
        super(pc, features, r);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0017  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final org.codehaus.jackson.JsonToken parseNumberText(int r15) throws java.io.IOException, org.codehaus.jackson.JsonParseException {
        /*
            r14 = this;
            r13 = 45
            r12 = 57
            r11 = 48
            if (r15 != r13) goto L_0x0020
            r5 = 1
        L_0x0009:
            int r6 = r14._inputPtr
            int r8 = r6 + -1
            int r2 = r14._inputEnd
            if (r5 == 0) goto L_0x0032
            int r9 = r14._inputEnd
            if (r6 < r9) goto L_0x0022
        L_0x0015:
            if (r5 == 0) goto L_0x0019
            int r8 = r8 + 1
        L_0x0019:
            r14._inputPtr = r8
            org.codehaus.jackson.JsonToken r9 = r14.parseNumberText2(r5)
        L_0x001f:
            return r9
        L_0x0020:
            r5 = 0
            goto L_0x0009
        L_0x0022:
            char[] r9 = r14._inputBuffer
            int r7 = r6 + 1
            char r15 = r9[r6]
            if (r15 > r12) goto L_0x002c
            if (r15 >= r11) goto L_0x0031
        L_0x002c:
            java.lang.String r9 = "expected digit (0-9) to follow minus sign, for valid numeric value"
            r14.reportUnexpectedNumberChar(r15, r9)
        L_0x0031:
            r6 = r7
        L_0x0032:
            r3 = 1
        L_0x0033:
            int r9 = r14._inputEnd
            if (r6 >= r9) goto L_0x0015
            char[] r9 = r14._inputBuffer
            int r7 = r6 + 1
            char r15 = r9[r6]
            if (r15 < r11) goto L_0x0041
            if (r15 <= r12) goto L_0x004a
        L_0x0041:
            r1 = 0
            r9 = 46
            if (r15 != r9) goto L_0x0070
        L_0x0046:
            if (r7 < r2) goto L_0x005e
            r6 = r7
            goto L_0x0015
        L_0x004a:
            int r3 = r3 + 1
            r9 = 2
            if (r3 != r9) goto L_0x00c5
            char[] r9 = r14._inputBuffer
            int r10 = r7 + -2
            char r9 = r9[r10]
            if (r9 != r11) goto L_0x00c5
            java.lang.String r9 = "Leading zeroes not allowed"
            r14.reportInvalidNumber(r9)
            r6 = r7
            goto L_0x0033
        L_0x005e:
            char[] r9 = r14._inputBuffer
            int r6 = r7 + 1
            char r15 = r9[r7]
            if (r15 < r11) goto L_0x0068
            if (r15 <= r12) goto L_0x007d
        L_0x0068:
            if (r1 != 0) goto L_0x006f
            java.lang.String r9 = "Decimal point not followed by a digit"
            r14.reportUnexpectedNumberChar(r15, r9)
        L_0x006f:
            r7 = r6
        L_0x0070:
            r0 = 0
            r9 = 101(0x65, float:1.42E-43)
            if (r15 == r9) goto L_0x0079
            r9 = 69
            if (r15 != r9) goto L_0x00af
        L_0x0079:
            if (r7 < r2) goto L_0x0081
            r6 = r7
            goto L_0x0015
        L_0x007d:
            int r1 = r1 + 1
            r7 = r6
            goto L_0x0046
        L_0x0081:
            char[] r9 = r14._inputBuffer
            int r6 = r7 + 1
            char r15 = r9[r7]
            if (r15 == r13) goto L_0x008d
            r9 = 43
            if (r15 != r9) goto L_0x00c3
        L_0x008d:
            if (r6 >= r2) goto L_0x0015
            char[] r9 = r14._inputBuffer
            int r7 = r6 + 1
            char r15 = r9[r6]
        L_0x0095:
            if (r15 > r12) goto L_0x00a8
            if (r15 < r11) goto L_0x00a8
            int r0 = r0 + 1
            if (r7 < r2) goto L_0x00a0
            r6 = r7
            goto L_0x0015
        L_0x00a0:
            char[] r9 = r14._inputBuffer
            int r6 = r7 + 1
            char r15 = r9[r7]
            r7 = r6
            goto L_0x0095
        L_0x00a8:
            if (r0 != 0) goto L_0x00af
            java.lang.String r9 = "Exponent indicator not followed by a digit"
            r14.reportUnexpectedNumberChar(r15, r9)
        L_0x00af:
            r6 = r7
            int r6 = r6 + -1
            r14._inputPtr = r6
            int r4 = r6 - r8
            org.codehaus.jackson.util.TextBuffer r9 = r14._textBuffer
            char[] r10 = r14._inputBuffer
            r9.resetWithShared(r10, r8, r4)
            org.codehaus.jackson.JsonToken r9 = r14.reset(r5, r3, r1, r0)
            goto L_0x001f
        L_0x00c3:
            r7 = r6
            goto L_0x0095
        L_0x00c5:
            r6 = r7
            goto L_0x0033
        */
        throw new UnsupportedOperationException("Method not decompiled: org.codehaus.jackson.impl.ReaderBasedNumericParser.parseNumberText(int):org.codehaus.jackson.JsonToken");
    }

    private final JsonToken parseNumberText2(boolean negative) throws IOException, JsonParseException {
        char c;
        int outPtr;
        char c2;
        char c3;
        int outPtr2;
        char[] outBuf = this._textBuffer.emptyAndGetCurrentSegment();
        int outPtr3 = 0;
        if (negative) {
            outBuf[0] = '-';
            outPtr3 = 0 + 1;
        }
        int intLen = 0;
        boolean eof = false;
        while (true) {
            if (this._inputPtr >= this._inputEnd && !loadMore()) {
                c = 0;
                eof = true;
                break;
            }
            char[] cArr = this._inputBuffer;
            int i = this._inputPtr;
            this._inputPtr = i + 1;
            c = cArr[i];
            if (c < '0' || c > '9') {
                break;
            }
            intLen++;
            if (intLen == 2 && outBuf[outPtr3 - 1] == '0') {
                reportInvalidNumber("Leading zeroes not allowed");
            }
            if (outPtr3 >= outBuf.length) {
                outBuf = this._textBuffer.finishCurrentSegment();
                outPtr3 = 0;
            }
            outBuf[outPtr3] = c;
            outPtr3++;
        }
        if (intLen == 0) {
            reportInvalidNumber("Missing integer part (next char " + _getCharDesc(c) + ")");
        }
        int fractLen = 0;
        if (c == '.') {
            int outPtr4 = outPtr3 + 1;
            outBuf[outPtr3] = c;
            while (true) {
                outPtr3 = outPtr4;
                if (this._inputPtr >= this._inputEnd && !loadMore()) {
                    eof = true;
                    break;
                }
                char[] cArr2 = this._inputBuffer;
                int i2 = this._inputPtr;
                this._inputPtr = i2 + 1;
                c = cArr2[i2];
                if (c < '0' || c > '9') {
                    break;
                }
                fractLen++;
                if (outPtr3 >= outBuf.length) {
                    outBuf = this._textBuffer.finishCurrentSegment();
                    outPtr3 = 0;
                }
                outPtr4 = outPtr3 + 1;
                outBuf[outPtr3] = c;
            }
            if (fractLen == 0) {
                reportUnexpectedNumberChar(c, "Decimal point not followed by a digit");
            }
        }
        int expLen = 0;
        if (c == 'e' || c == 'E') {
            if (outPtr >= outBuf.length) {
                outBuf = this._textBuffer.finishCurrentSegment();
                outPtr = 0;
            }
            int outPtr5 = outPtr + 1;
            outBuf[outPtr] = c;
            if (this._inputPtr < this._inputEnd) {
                char[] cArr3 = this._inputBuffer;
                int i3 = this._inputPtr;
                this._inputPtr = i3 + 1;
                c2 = cArr3[i3];
            } else {
                c2 = getNextChar("expected a digit for number exponent");
            }
            if (c3 == '-' || c3 == '+') {
                if (outPtr5 >= outBuf.length) {
                    outBuf = this._textBuffer.finishCurrentSegment();
                    outPtr2 = 0;
                } else {
                    outPtr2 = outPtr5;
                }
                int outPtr6 = outPtr2 + 1;
                outBuf[outPtr2] = c3;
                if (this._inputPtr < this._inputEnd) {
                    char[] cArr4 = this._inputBuffer;
                    int i4 = this._inputPtr;
                    this._inputPtr = i4 + 1;
                    c3 = cArr4[i4];
                } else {
                    c3 = getNextChar("expected a digit for number exponent");
                }
                outPtr = outPtr6;
            } else {
                outPtr = outPtr5;
            }
            while (true) {
                if (c3 > '9' || c3 < '0') {
                    break;
                }
                expLen++;
                if (outPtr >= outBuf.length) {
                    outBuf = this._textBuffer.finishCurrentSegment();
                    outPtr = 0;
                }
                int outPtr7 = outPtr + 1;
                outBuf[outPtr] = c3;
                if (this._inputPtr >= this._inputEnd && !loadMore()) {
                    eof = true;
                    outPtr = outPtr7;
                    break;
                }
                char[] cArr5 = this._inputBuffer;
                int i5 = this._inputPtr;
                this._inputPtr = i5 + 1;
                c3 = cArr5[i5];
                outPtr = outPtr7;
            }
            if (expLen == 0) {
                reportUnexpectedNumberChar(c3, "Exponent indicator not followed by a digit");
            }
        }
        if (!eof) {
            this._inputPtr--;
        }
        this._textBuffer.setCurrentLength(outPtr);
        return reset(negative, intLen, fractLen, expLen);
    }
}
