package org.codehaus.jackson.node;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.JsonToken;
import org.codehaus.jackson.map.SerializerProvider;
import org.codehaus.jackson.node.ContainerNode;

public class ObjectNode extends ContainerNode {
    protected LinkedHashMap<String, JsonNode> _children = null;

    public ObjectNode(JsonNodeFactory nc) {
        super(nc);
    }

    public JsonToken asToken() {
        return JsonToken.START_OBJECT;
    }

    public boolean isObject() {
        return true;
    }

    public int size() {
        if (this._children == null) {
            return 0;
        }
        return this._children.size();
    }

    public Iterator<JsonNode> getElements() {
        return this._children == null ? ContainerNode.NoNodesIterator.instance() : this._children.values().iterator();
    }

    public JsonNode get(int index) {
        return null;
    }

    public JsonNode get(String fieldName) {
        if (this._children != null) {
            return this._children.get(fieldName);
        }
        return null;
    }

    public Iterator<String> getFieldNames() {
        return this._children == null ? ContainerNode.NoStringsIterator.instance() : this._children.keySet().iterator();
    }

    public JsonNode path(int index) {
        return MissingNode.getInstance();
    }

    public JsonNode path(String fieldName) {
        JsonNode n;
        return (this._children == null || (n = this._children.get(fieldName)) == null) ? MissingNode.getInstance() : n;
    }

    public JsonNode findValue(String fieldName) {
        if (this._children != null) {
            for (Map.Entry<String, JsonNode> entry : this._children.entrySet()) {
                if (fieldName.equals(entry.getKey())) {
                    return (JsonNode) entry.getValue();
                }
                JsonNode value = ((JsonNode) entry.getValue()).findValue(fieldName);
                if (value != null) {
                    return value;
                }
            }
        }
        return null;
    }

    public List<JsonNode> findValues(String fieldName, List<JsonNode> foundSoFar) {
        if (this._children != null) {
            for (Map.Entry<String, JsonNode> entry : this._children.entrySet()) {
                if (fieldName.equals(entry.getKey())) {
                    if (foundSoFar == null) {
                        foundSoFar = new ArrayList<>();
                    }
                    foundSoFar.add(entry.getValue());
                } else {
                    foundSoFar = ((JsonNode) entry.getValue()).findValues(fieldName, foundSoFar);
                }
            }
        }
        return foundSoFar;
    }

    public List<String> findValuesAsText(String fieldName, List<String> foundSoFar) {
        if (this._children != null) {
            for (Map.Entry<String, JsonNode> entry : this._children.entrySet()) {
                if (fieldName.equals(entry.getKey())) {
                    if (foundSoFar == null) {
                        foundSoFar = new ArrayList<>();
                    }
                    foundSoFar.add(((JsonNode) entry.getValue()).getValueAsText());
                } else {
                    foundSoFar = ((JsonNode) entry.getValue()).findValuesAsText(fieldName, foundSoFar);
                }
            }
        }
        return foundSoFar;
    }

    /* Debug info: failed to restart local var, previous not found, register: 4 */
    public ObjectNode findParent(String fieldName) {
        if (this._children != null) {
            for (Map.Entry<String, JsonNode> entry : this._children.entrySet()) {
                if (fieldName.equals(entry.getKey())) {
                    return this;
                }
                JsonNode value = ((JsonNode) entry.getValue()).findParent(fieldName);
                if (value != null) {
                    return (ObjectNode) value;
                }
            }
        }
        return null;
    }

    public List<JsonNode> findParents(String fieldName, List<JsonNode> foundSoFar) {
        if (this._children != null) {
            for (Map.Entry<String, JsonNode> entry : this._children.entrySet()) {
                if (fieldName.equals(entry.getKey())) {
                    if (foundSoFar == null) {
                        foundSoFar = new ArrayList<>();
                    }
                    foundSoFar.add(this);
                } else {
                    foundSoFar = ((JsonNode) entry.getValue()).findParents(fieldName, foundSoFar);
                }
            }
        }
        return foundSoFar;
    }

    public final void serialize(JsonGenerator jg, SerializerProvider provider) throws IOException, JsonProcessingException {
        jg.writeStartObject();
        if (this._children != null) {
            for (Map.Entry<String, JsonNode> en : this._children.entrySet()) {
                jg.writeFieldName((String) en.getKey());
                ((BaseJsonNode) en.getValue()).serialize(jg, provider);
            }
        }
        jg.writeEndObject();
    }

    public Iterator<Map.Entry<String, JsonNode>> getFields() {
        if (this._children == null) {
            return NoFieldsIterator.instance;
        }
        return this._children.entrySet().iterator();
    }

    public JsonNode put(String fieldName, JsonNode value) {
        if (value == null) {
            value = nullNode();
        }
        return _put(fieldName, value);
    }

    public JsonNode remove(String fieldName) {
        if (this._children != null) {
            return this._children.remove(fieldName);
        }
        return null;
    }

    public ObjectNode remove(Collection<String> fieldNames) {
        if (this._children != null) {
            for (String fieldName : fieldNames) {
                this._children.remove(fieldName);
            }
        }
        return this;
    }

    public ObjectNode removeAll() {
        this._children = null;
        return this;
    }

    public JsonNode putAll(Map<String, JsonNode> properties) {
        if (this._children == null) {
            this._children = new LinkedHashMap<>(properties);
        } else {
            for (Map.Entry<String, JsonNode> en : properties.entrySet()) {
                JsonNode n = en.getValue();
                if (n == null) {
                    n = nullNode();
                }
                this._children.put(en.getKey(), n);
            }
        }
        return this;
    }

    public JsonNode putAll(ObjectNode other) {
        int len = other.size();
        if (len > 0) {
            if (this._children == null) {
                this._children = new LinkedHashMap<>(len);
            }
            other.putContentsTo(this._children);
        }
        return this;
    }

    public ObjectNode retain(Collection<String> fieldNames) {
        if (this._children != null) {
            Iterator<Map.Entry<String, JsonNode>> entries = this._children.entrySet().iterator();
            while (entries.hasNext()) {
                if (!fieldNames.contains(entries.next().getKey())) {
                    entries.remove();
                }
            }
        }
        return this;
    }

    public ObjectNode retain(String... fieldNames) {
        return retain(Arrays.asList(fieldNames));
    }

    public ArrayNode putArray(String fieldName) {
        ArrayNode n = arrayNode();
        _put(fieldName, n);
        return n;
    }

    public ObjectNode putObject(String fieldName) {
        ObjectNode n = objectNode();
        _put(fieldName, n);
        return n;
    }

    public void putPOJO(String fieldName, Object pojo) {
        _put(fieldName, POJONode(pojo));
    }

    public void putNull(String fieldName) {
        _put(fieldName, nullNode());
    }

    public void put(String fieldName, int v) {
        _put(fieldName, numberNode(v));
    }

    public void put(String fieldName, long v) {
        _put(fieldName, numberNode(v));
    }

    public void put(String fieldName, float v) {
        _put(fieldName, numberNode(v));
    }

    public void put(String fieldName, double v) {
        _put(fieldName, numberNode(v));
    }

    public void put(String fieldName, BigDecimal v) {
        if (v == null) {
            putNull(fieldName);
        } else {
            _put(fieldName, numberNode(v));
        }
    }

    public void put(String fieldName, String v) {
        if (v == null) {
            putNull(fieldName);
        } else {
            _put(fieldName, textNode(v));
        }
    }

    public void put(String fieldName, boolean v) {
        _put(fieldName, booleanNode(v));
    }

    public void put(String fieldName, byte[] v) {
        if (v == null) {
            putNull(fieldName);
        } else {
            _put(fieldName, binaryNode(v));
        }
    }

    /* access modifiers changed from: protected */
    public void putContentsTo(Map<String, JsonNode> dst) {
        if (this._children != null) {
            for (Map.Entry<String, JsonNode> en : this._children.entrySet()) {
                dst.put(en.getKey(), en.getValue());
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0038  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean equals(java.lang.Object r11) {
        /*
            r10 = this;
            r6 = 1
            r7 = 0
            if (r11 != r10) goto L_0x0005
        L_0x0004:
            return r6
        L_0x0005:
            if (r11 != 0) goto L_0x0009
            r6 = r7
            goto L_0x0004
        L_0x0009:
            java.lang.Class r8 = r11.getClass()
            java.lang.Class r9 = r10.getClass()
            if (r8 == r9) goto L_0x0015
            r6 = r7
            goto L_0x0004
        L_0x0015:
            r3 = r11
            org.codehaus.jackson.node.ObjectNode r3 = (org.codehaus.jackson.node.ObjectNode) r3
            int r8 = r3.size()
            int r9 = r10.size()
            if (r8 == r9) goto L_0x0024
            r6 = r7
            goto L_0x0004
        L_0x0024:
            java.util.LinkedHashMap<java.lang.String, org.codehaus.jackson.JsonNode> r8 = r10._children
            if (r8 == 0) goto L_0x0004
            java.util.LinkedHashMap<java.lang.String, org.codehaus.jackson.JsonNode> r8 = r10._children
            java.util.Set r8 = r8.entrySet()
            java.util.Iterator r1 = r8.iterator()
        L_0x0032:
            boolean r8 = r1.hasNext()
            if (r8 == 0) goto L_0x0004
            java.lang.Object r0 = r1.next()
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0
            java.lang.Object r2 = r0.getKey()
            java.lang.String r2 = (java.lang.String) r2
            java.lang.Object r5 = r0.getValue()
            org.codehaus.jackson.JsonNode r5 = (org.codehaus.jackson.JsonNode) r5
            org.codehaus.jackson.JsonNode r4 = r3.get(r2)
            if (r4 == 0) goto L_0x0056
            boolean r8 = r4.equals(r5)
            if (r8 != 0) goto L_0x0032
        L_0x0056:
            r6 = r7
            goto L_0x0004
        */
        throw new UnsupportedOperationException("Method not decompiled: org.codehaus.jackson.node.ObjectNode.equals(java.lang.Object):boolean");
    }

    public int hashCode() {
        if (this._children == null) {
            return -1;
        }
        return this._children.hashCode();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder((size() << 4) + 32);
        sb.append("{");
        if (this._children != null) {
            int count = 0;
            for (Map.Entry<String, JsonNode> en : this._children.entrySet()) {
                if (count > 0) {
                    sb.append(",");
                }
                count++;
                TextNode.appendQuoted(sb, (String) en.getKey());
                sb.append(':');
                sb.append(((JsonNode) en.getValue()).toString());
            }
        }
        sb.append("}");
        return sb.toString();
    }

    private final JsonNode _put(String fieldName, JsonNode value) {
        if (this._children == null) {
            this._children = new LinkedHashMap<>();
        }
        return this._children.put(fieldName, value);
    }

    protected static class NoFieldsIterator implements Iterator<Map.Entry<String, JsonNode>> {
        static final NoFieldsIterator instance = new NoFieldsIterator();

        private NoFieldsIterator() {
        }

        public boolean hasNext() {
            return false;
        }

        public Map.Entry<String, JsonNode> next() {
            throw new NoSuchElementException();
        }

        public void remove() {
            throw new IllegalStateException();
        }
    }
}
