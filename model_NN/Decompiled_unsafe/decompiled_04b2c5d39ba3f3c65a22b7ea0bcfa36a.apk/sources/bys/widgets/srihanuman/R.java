package bys.widgets.srihanuman;

public final class R {

    public static final class attr {
        public static final int adSize = 2130771968;
        public static final int adUnitId = 2130771969;
        public static final int screenId = 2130771970;
    }

    public static final class color {
        public static final int dark_brown = 2131230720;
        public static final int grey = 2131230721;
        public static final int rose_pink = 2131230722;
    }

    public static final class drawable {
        public static final int audio = 2130837504;
        public static final int audio_list_item_background = 2130837505;
        public static final int audio_seek = 2130837506;
        public static final int bronze_statues_temple_bell_40 = 2130837507;
        public static final int candle_flame = 2130837508;
        public static final int classic_clock = 2130837509;
        public static final int dia_pot = 2130837510;
        public static final int dia_stand = 2130837511;
        public static final int download_48 = 2130837512;
        public static final int file = 2130837513;
        public static final int flower1 = 2130837514;
        public static final int flower2 = 2130837515;
        public static final int flower3 = 2130837516;
        public static final int flower4 = 2130837517;
        public static final int flower5 = 2130837518;
        public static final int flower6 = 2130837519;
        public static final int folder = 2130837520;
        public static final int glowing_ohm = 2130837521;
        public static final int god_icon = 2130837522;
        public static final int god_imagd_00 = 2130837523;
        public static final int god_image_00 = 2130837524;
        public static final int god_image_01 = 2130837525;
        public static final int god_image_02 = 2130837526;
        public static final int god_image_03 = 2130837527;
        public static final int god_image_04 = 2130837528;
        public static final int god_image_05 = 2130837529;
        public static final int god_image_06 = 2130837530;
        public static final int god_image_07 = 2130837531;
        public static final int god_image_08 = 2130837532;
        public static final int god_image_09 = 2130837533;
        public static final int god_image_10 = 2130837534;
        public static final int god_image_11 = 2130837535;
        public static final int god_image_12 = 2130837536;
        public static final int god_image_13 = 2130837537;
        public static final int god_image_14 = 2130837538;
        public static final int god_image_15 = 2130837539;
        public static final int god_image_16 = 2130837540;
        public static final int god_image_17 = 2130837541;
        public static final int god_image_18 = 2130837542;
        public static final int god_image_19 = 2130837543;
        public static final int god_image_20 = 2130837544;
        public static final int god_image_21 = 2130837545;
        public static final int god_image_22 = 2130837546;
        public static final int god_image_23 = 2130837547;
        public static final int god_image_24 = 2130837548;
        public static final int god_image_25 = 2130837549;
        public static final int god_image_26 = 2130837550;
        public static final int god_image_27 = 2130837551;
        public static final int god_image_28 = 2130837552;
        public static final int god_image_29 = 2130837553;
        public static final int god_image_30 = 2130837554;
        public static final int god_image_31 = 2130837555;
        public static final int god_image_32 = 2130837556;
        public static final int god_image_33 = 2130837557;
        public static final int god_image_34 = 2130837558;
        public static final int gold_plate_with_flower = 2130837559;
        public static final int help_us = 2130837560;
        public static final int home_32 = 2130837561;
        public static final int menu_how_to = 2130837562;
        public static final int more_audio = 2130837563;
        public static final int not_glowing_ohm = 2130837564;
        public static final int pause_48 = 2130837565;
        public static final int picture_frame = 2130837566;
        public static final int plate = 2130837567;
        public static final int play_48 = 2130837568;
        public static final int play_next = 2130837569;
        public static final int play_prev = 2130837570;
        public static final int progress_bar = 2130837571;
        public static final int puja_plate_small = 2130837572;
        public static final int sdcard_32 = 2130837573;
        public static final int shankh = 2130837574;
        public static final int sheet_bright = 2130837575;
        public static final int sheet_dark = 2130837576;
        public static final int side_curtain = 2130837577;
        public static final int sub_form_background = 2130837578;
        public static final int sub_form_dip_background = 2130837579;
        public static final int top_curtain = 2130837580;
        public static final int up_arrow = 2130837581;
    }

    public static final class id {
        public static final int ActivityGodImage = 2131165267;
        public static final int AudioPlayer = 2131165200;
        public static final int BANNER = 2131165184;
        public static final int FrameAndImageHolder = 2131165266;
        public static final int IAB_BANNER = 2131165186;
        public static final int IAB_LEADERBOARD = 2131165187;
        public static final int IAB_MRECT = 2131165185;
        public static final int LinearLayout01 = 2131165192;
        public static final int LinearLayout02 = 2131165191;
        public static final int LinearLayout03 = 2131165260;
        public static final int LinearLayout04 = 2131165196;
        public static final int LinearLayout05 = 2131165242;
        public static final int LinearToScroll = 2131165240;
        public static final int TextView01 = 2131165251;
        public static final int TextView02 = 2131165254;
        public static final int TextView03 = 2131165248;
        public static final int WebView01 = 2131165188;
        public static final int WidgetGodImage160 = 2131165217;
        public static final int WidgetGodImage240 = 2131165218;
        public static final int WidgetGodImage80 = 2131165219;
        public static final int ad = 2131165279;
        public static final int ad2 = 2131165189;
        public static final int ad3 = 2131165226;
        public static final int ad4 = 2131165202;
        public static final int adMob = 2131165212;
        public static final int audioPlayerView = 2131165273;
        public static final int btnAccept = 2131165227;
        public static final int btnChangeAlarmTriggerTime = 2131165253;
        public static final int btnCheckOurOtherApps = 2131165224;
        public static final int btnDecline = 2131165228;
        public static final int btnDonate = 2131165225;
        public static final int btnDownload = 2131165238;
        public static final int btnGiveFiveStar = 2131165222;
        public static final int btnSankh = 2131165268;
        public static final int btnSelectAudio = 2131165280;
        public static final int btnSuggestImprovement = 2131165221;
        public static final int btnTellOthersEmail = 2131165223;
        public static final int chkBellAsRingtone = 2131165256;
        public static final int chkChantingAsRingtone = 2131165258;
        public static final int chkDisableBannerAds = 2131165265;
        public static final int chkDisableBellVibration = 2131165263;
        public static final int chkDisableNotificationAds = 2131165264;
        public static final int chkDisableShankhnad = 2131165261;
        public static final int chkDisableStartupBell = 2131165262;
        public static final int chkEnableAlarm = 2131165250;
        public static final int chkEnableRepeat = 2131165244;
        public static final int chkSelect = 2131165235;
        public static final int chkSelectAll = 2131165237;
        public static final int chkShankhAsRingtone = 2131165257;
        public static final int chkTurnShuffleOn = 2131165243;
        public static final int edtRepeatCount = 2131165247;
        public static final int file_picker_image = 2131165215;
        public static final int gridAudioList = 2131165201;
        public static final int gridPlayList = 2131165236;
        public static final int imgAppIcon = 2131165232;
        public static final int imgBellLeft = 2131165271;
        public static final int imgBellRight = 2131165272;
        public static final int imgDownload = 2131165198;
        public static final int imgPause = 2131165209;
        public static final int imgPlay = 2131165197;
        public static final int imgPlayNext = 2131165210;
        public static final int imgPlayPrev = 2131165208;
        public static final int imgSideLeftCurtain = 2131165274;
        public static final int imgSideRightCurtain = 2131165275;
        public static final int imgStylus = 2131165205;
        public static final int layoutAlarmSetting = 2131165249;
        public static final int layoutBuffering = 2131165204;
        public static final int layoutFilePickerRoot = 2131165214;
        public static final int layoutLyricViewHolder = 2131165276;
        public static final int layoutLyricViewMain = 2131165229;
        public static final int layoutProgress = 2131165213;
        public static final int layoutRepeatAudio = 2131165241;
        public static final int layoutRingtone = 2131165255;
        public static final int layoutShankhnad = 2131165259;
        public static final int linearLayout1 = 2131165193;
        public static final int linearTapeAndStylus = 2131165203;
        public static final int lyricDownlaoderView = 2131165277;
        public static final int lyricView = 2131165278;
        public static final int mnuAudioListContextMenu_AddMyAudio = 2131165283;
        public static final int mnuAudioListContextMenu_Delete = 2131165281;
        public static final int mnuAudioListContextMenu_SetAsRingtone = 2131165282;
        public static final int mnuChangeAudioOptionMenu_PlayDefault = 2131165284;
        public static final int mnuHowTo = 2131165286;
        public static final int mnuMoreAudio = 2131165287;
        public static final int mnuPlayList = 2131165288;
        public static final int mnuSearchAudioContextMenu_Download = 2131165291;
        public static final int mnuSearchAudioContextMenu_Play = 2131165292;
        public static final int mnuSearchAudioContextMenu_Preview = 2131165290;
        public static final int mnuSearchAudioContextMenu_SetAsRingtone = 2131165294;
        public static final int mnuSearchAudioContextMenu_Stop = 2131165293;
        public static final int mnuSetAsAlarm = 2131165285;
        public static final int mnuTellOthers = 2131165289;
        public static final int rdbRepeatContinuosly = 2131165245;
        public static final int rdbRepeatNTimes = 2131165246;
        public static final int scrollLayout1 = 2131165239;
        public static final int txtAlarmTriggerTime = 2131165252;
        public static final int txtBuffering = 2131165206;
        public static final int txtDesc = 2131165195;
        public static final int txtElapseTime = 2131165207;
        public static final int txtFileName = 2131165216;
        public static final int txtIntroduction = 2131165220;
        public static final int txtNotiDesc = 2131165234;
        public static final int txtNotiTitle = 2131165233;
        public static final int txtSize = 2131165194;
        public static final int txtTitle = 2131165199;
        public static final int txtTitleEnglish = 2131165230;
        public static final int txtTitleLocal = 2131165231;
        public static final int txtTotalTime = 2131165211;
        public static final int viewAarti = 2131165270;
        public static final int viewDownloader = 2131165190;
        public static final int viewFlowerShower = 2131165269;
    }

    public static final class layout {
        public static final int about = 2130903040;
        public static final int audio_list_item = 2130903041;
        public static final int audio_list_layout = 2130903042;
        public static final int audio_player_view_layout = 2130903043;
        public static final int bys_adview_layout = 2130903044;
        public static final int downloader_view_layout = 2130903045;
        public static final int file_picker_list_item = 2130903046;
        public static final int god_widget_layout_160_200 = 2130903047;
        public static final int god_widget_layout_240_300 = 2130903048;
        public static final int god_widget_layout_80_100 = 2130903049;
        public static final int help_us_layout = 2130903050;
        public static final int license_agreement_layout = 2130903051;
        public static final int lyric_view_layout = 2130903052;
        public static final int notification_bar_layout = 2130903053;
        public static final int play_list_item = 2130903054;
        public static final int play_list_maker_layout = 2130903055;
        public static final int settings_layout = 2130903056;
        public static final int temple_audio_player_view_layout = 2130903057;
        public static final int temple_main_layout = 2130903058;
    }

    public static final class menu {
        public static final int audio_list_context_menu = 2131296256;
        public static final int change_audio_option_menu = 2131296257;
        public static final int option_menu = 2131296258;
        public static final int search_audio_context_menu = 2131296259;
    }

    public static final class raw {
        public static final int bell = 2131034112;
        public static final int shankh = 2131034113;
    }

    public static final class string {
        public static final int About = 2131099668;
        public static final int AdMobId = 2131099649;
        public static final int AddMyAudio = 2131099689;
        public static final int AirPushApiKey = 2131099651;
        public static final int AirPushAppId = 2131099650;
        public static final int Amazon = 2131099695;
        public static final int AudioFile = 2131099691;
        public static final int BaseUrl = 2131099704;
        public static final int CheckOurOtherApps = 2131099681;
        public static final int Continuously = 2131099657;
        public static final int Delete = 2131099670;
        public static final int DisableBannerAds = 2131099662;
        public static final int DisableBellVibration = 2131099661;
        public static final int DisableNotificationAds = 2131099663;
        public static final int DisableShankhnad = 2131099659;
        public static final int DisableShankhnadNote = 2131099658;
        public static final int DisableStartupBell = 2131099660;
        public static final int Donate = 2131099682;
        public static final int Download = 2131099673;
        public static final int DownloadFromNet = 2131099693;
        public static final int EnableAlarm = 2131099665;
        public static final int FeedbackEmail = 2131099705;
        public static final int HelpUs = 2131099676;
        public static final int Home = 2131099696;
        public static final int InMobiId = 2131099648;
        public static final int Introduction = 2131099683;
        public static final int LicenseAgreement = 2131099703;
        public static final int MoreAudio = 2131099669;
        public static final int Play = 2131099674;
        public static final int PlayDefaultAudio = 2131099699;
        public static final int PlayList = 2131099688;
        public static final int Preview = 2131099672;
        public static final int PublisherName = 2131099706;
        public static final int Rate5Star = 2131099678;
        public static final int Repeat = 2131099656;
        public static final int RepeatMantra = 2131099664;
        public static final int Root = 2131099698;
        public static final int Select = 2131099690;
        public static final int SelectAll = 2131099707;
        public static final int SelectAudio = 2131099694;
        public static final int SelectAudioFile = 2131099692;
        public static final int SendroidAppId = 2131099654;
        public static final int SetAsAlarm = 2131099667;
        public static final int SetAsRingtone = 2131099671;
        public static final int SetAudioAsRingtone = 2131099702;
        public static final int SetBellAsRingTOne = 2131099700;
        public static final int SetShankhAsRingTOne = 2131099701;
        public static final int Stop = 2131099675;
        public static final int SuggestImprovement = 2131099677;
        public static final int TellOthersEmail = 2131099680;
        public static final int TellOthersSMS = 2131099679;
        public static final int Time = 2131099666;
        public static final int Times = 2131099655;
        public static final int TurnShuffleOn = 2131099708;
        public static final int Up = 2131099697;
        public static final int app_name = 2131099684;
        public static final int startapp_appid = 2131099653;
        public static final int startapp_devid = 2131099652;
        public static final int widget_name_160_200 = 2131099686;
        public static final int widget_name_240_300 = 2131099687;
        public static final int widget_name_80_100 = 2131099685;
    }

    public static final class styleable {
        public static final int[] bys_customviews_audioplayer = {R.attr.screenId};
        public static final int bys_customviews_audioplayer_screenId = 0;
        public static final int[] com_google_ads_AdView = {R.attr.adSize, R.attr.adUnitId};
        public static final int com_google_ads_AdView_adSize = 0;
        public static final int com_google_ads_AdView_adUnitId = 1;
    }

    public static final class xml {
        public static final int god_widget_info_160_200 = 2130968576;
        public static final int god_widget_info_240_300 = 2130968577;
        public static final int god_widget_info_80_100 = 2130968578;
    }
}
