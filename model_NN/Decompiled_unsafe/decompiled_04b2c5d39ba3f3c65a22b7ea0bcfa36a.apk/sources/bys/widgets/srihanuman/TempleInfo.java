package bys.widgets.srihanuman;

public final class TempleInfo {
    public static final int intImageCount = 36;
    public static final int intResIdFirstImage = 2130837523;
    public static final String strIdentifier = "ShriHanumanTemple";
    public static final String strPaypalUrl = "https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=paypal%40backyardsoft%2ecom&lc=US&item_name=Hanuman%20Temple&currency_code=USD&bn=PP%2dDonationsBF%3abtn_donateCC_LG%2egif%3aNonHosted";
}
