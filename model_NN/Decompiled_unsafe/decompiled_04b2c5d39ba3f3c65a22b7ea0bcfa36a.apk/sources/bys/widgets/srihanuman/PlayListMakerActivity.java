package bys.widgets.srihanuman;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Log;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;
import bys.apps.tools.lyricserializer.PlayListManager;
import bys.libs.utils.ringtonemanager.RingToneManager;
import bys.libs.utils.storage.StorageUtil;
import java.io.File;

public class PlayListMakerActivity extends Activity {
    public static final String EXTRA_CURRENT_AUDIO_FILE_PATH = "Current Audio Path";
    private CheckBox chkSelectAll = null;
    /* access modifiers changed from: private */
    public PlayListAdapter gcaPlayListAdaptor = null;
    private GridView gridPlayListView = null;
    Activity mActivty = null;
    /* access modifiers changed from: private */
    public AudioListItemView mSelectedRowView = null;
    /* access modifiers changed from: private */
    public PlayListManager mplstManager = null;
    private String mstrCurrentAudioPath = "";

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        if (this.mSelectedRowView != null) {
            super.onCreateContextMenu(menu, v, menuInfo);
            getMenuInflater().inflate(R.menu.audio_list_context_menu, menu);
        }
    }

    public boolean onContextItemSelected(MenuItem item) {
        File file = this.mSelectedRowView.audioFile;
        Log.v("", "onContextItemSelected : File Absolute Path" + file.getAbsolutePath());
        switch (item.getItemId()) {
            case R.id.mnuAudioListContextMenu_Delete:
                if (file != null && !file.delete()) {
                    Log.v("", "Failed to delete file" + file.getAbsolutePath());
                }
                refreshFilesList();
                this.gcaPlayListAdaptor.notifyDataSetChanged();
                break;
            case R.id.mnuAudioListContextMenu_SetAsRingtone:
                new RingToneManager(getApplicationContext()).setRingTone(file.getAbsolutePath(), this.mSelectedRowView.txtFileName.getText().toString());
                break;
            case R.id.mnuAudioListContextMenu_AddMyAudio:
                Toast.makeText(getApplicationContext(), "Place your audio file in \"" + getString(R.string.app_name) + "\" folder created in SD Card.", 1).show();
                break;
        }
        return super.onContextItemSelected(item);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            this.mplstManager.notifyAudioSelectionChange();
            SharedPreferences.Editor editor = getSharedPreferences(TempleInfo.strIdentifier, 0).edit();
            editor.putString("Key_CurrentAudioPath", "~");
            editor.commit();
            editor.putString("Key_CurrentAudioPath", this.mstrCurrentAudioPath);
            editor.commit();
        }
        return super.onKeyDown(keyCode, event);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        refreshFilesList();
        if (this.gcaPlayListAdaptor != null) {
            this.gcaPlayListAdaptor.notifyDataSetChanged();
        }
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    private void refreshFilesList() {
        this.mplstManager.prapareFreshPlayList();
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.play_list_maker_layout);
        this.gridPlayListView = (GridView) findViewById(R.id.gridPlayList);
        this.chkSelectAll = (CheckBox) findViewById(R.id.chkSelectAll);
        Button btnDownload = (Button) findViewById(R.id.btnDownload);
        if (getIntent().hasExtra(EXTRA_CURRENT_AUDIO_FILE_PATH)) {
            this.mstrCurrentAudioPath = getIntent().getStringExtra(EXTRA_CURRENT_AUDIO_FILE_PATH);
        }
        btnDownload.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (PlayListMakerActivity.this.getSharedPreferences(TempleInfo.strIdentifier, 0).getBoolean(LicenseAgreementActivity.KEY_AGREEMENT_ACCEPTANCE, false)) {
                    PlayListMakerActivity.this.startActivity(new Intent(PlayListMakerActivity.this.getApplicationContext(), AudioListActivity.class));
                    return;
                }
                PlayListMakerActivity.this.startActivity(new Intent(PlayListMakerActivity.this.getApplicationContext(), LicenseAgreementActivity.class));
            }
        });
        this.mplstManager = new PlayListManager(getApplicationContext(), StorageUtil.makeFolderInSDCard(getResources().getString(R.string.app_name)));
        this.chkSelectAll.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                PlayListMakerActivity.this.mplstManager.SelectAll(isChecked);
                PlayListMakerActivity.this.gcaPlayListAdaptor.notifyDataSetChanged();
            }
        });
        this.mActivty = this;
        refreshFilesList();
        this.gcaPlayListAdaptor = new PlayListAdapter(this);
        try {
            this.gridPlayListView.setAdapter((ListAdapter) this.gcaPlayListAdaptor);
        } catch (Exception e) {
            e.printStackTrace();
        }
        registerForContextMenu(this.gridPlayListView);
        this.gridPlayListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                String strAudioPath = PlayListMakerActivity.this.mplstManager.getAudioFilePath(arg2);
                Log.v("", "onItemClick : File Absolute Path" + strAudioPath);
                PlayListMakerActivity.this.mplstManager.setSelectedFlag(arg2, true);
                PlayListMakerActivity.this.mplstManager.notifyAudioSelectionChange();
                SharedPreferences.Editor editor = PlayListMakerActivity.this.getSharedPreferences(TempleInfo.strIdentifier, 0).edit();
                editor.putString("Key_CurrentAudioPath", "~");
                editor.commit();
                editor.putString("Key_CurrentAudioPath", strAudioPath);
                editor.commit();
                PlayListMakerActivity.this.finish();
            }
        });
    }

    private class PlayListAdapter extends BaseAdapter {
        private Context mContext;

        public PlayListAdapter(Context c) {
            this.mContext = c;
        }

        public int getCount() {
            return PlayListMakerActivity.this.mplstManager.getAudioListSize();
        }

        public Object getItem(int position) {
            return 0;
        }

        public long getItemId(int position) {
            return 0;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            AudioListItemView viewAudioItem = new AudioListItemView(this.mContext, null);
            File fileAudio = new File(PlayListMakerActivity.this.mplstManager.getAudioFilePath(position));
            if (fileAudio.exists()) {
                viewAudioItem.txtFileName.setText(fileAudio.getName());
                viewAudioItem.position = position;
                viewAudioItem.chkSelected.setChecked(PlayListMakerActivity.this.mplstManager.getSelectedFlag(position));
                viewAudioItem.audioFile = fileAudio;
                viewAudioItem.setOnTouchListener(new View.OnTouchListener() {
                    public boolean onTouch(View v, MotionEvent event) {
                        if (event.getAction() == 0) {
                            PlayListMakerActivity.this.mSelectedRowView = (AudioListItemView) v;
                        }
                        if (event.getAction() != 1) {
                            return false;
                        }
                        PlayListMakerActivity.this.mSelectedRowView = null;
                        return false;
                    }
                });
            }
            return viewAudioItem;
        }
    }

    private class AudioListItemView extends LinearLayout {
        File audioFile = null;
        CheckBox chkSelected = null;
        int position;
        TextView txtFileName = null;

        public AudioListItemView(Context context, AttributeSet attrs) {
            super(context, attrs);
            ((LayoutInflater) context.getSystemService("layout_inflater")).inflate((int) R.layout.play_list_item, this);
            this.txtFileName = (TextView) findViewById(R.id.txtFileName);
            this.chkSelected = (CheckBox) findViewById(R.id.chkSelect);
            this.chkSelected.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    PlayListMakerActivity.this.mplstManager.setSelectedFlag(AudioListItemView.this.position, isChecked);
                }
            });
        }
    }
}
