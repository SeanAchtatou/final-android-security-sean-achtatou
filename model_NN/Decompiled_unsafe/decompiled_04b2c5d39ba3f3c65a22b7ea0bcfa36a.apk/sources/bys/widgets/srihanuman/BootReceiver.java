package bys.widgets.srihanuman;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import com.airpush.android.Airpush;
import com.senddroid.SendDroid;

public class BootReceiver extends BroadcastReceiver {
    public void onReceive(Context arg0, Intent arg1) {
        if (!arg0.getSharedPreferences(TempleInfo.strIdentifier, 0).getBoolean("Key_DisableNotificationAds", false) && Integer.parseInt(Build.VERSION.SDK) > 3) {
            new Airpush(arg0, arg0.getResources().getString(R.string.AirPushAppId), arg0.getResources().getString(R.string.AirPushApiKey), false, true, true);
            new SendDroid(arg0, arg0.getResources().getString(R.string.SendroidAppId), arg0.getPackageName(), true);
        }
    }
}
