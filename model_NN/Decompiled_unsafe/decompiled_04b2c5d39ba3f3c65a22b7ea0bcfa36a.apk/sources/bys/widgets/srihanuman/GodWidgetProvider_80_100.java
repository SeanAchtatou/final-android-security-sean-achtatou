package bys.widgets.srihanuman;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;
import android.widget.RemoteViews;
import java.util.Random;

public class GodWidgetProvider_80_100 extends AppWidgetProvider {
    Handler handle = new Handler();
    Context m_Context = null;
    AppWidgetManager m_appWidgetManager;
    RemoteViews m_remoteViews = null;
    Runnable m_taskImageChanger;
    int mintGodImageIndex = this.randGen.nextInt(0, 35);
    RandomGenerator randGen = new RandomGenerator(this, null);

    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        this.m_Context = context;
        Log.v(TempleInfo.strIdentifier, "onUpdate = " + appWidgetIds.length);
        this.m_remoteViews = new RemoteViews(context.getPackageName(), (int) R.layout.god_widget_layout_80_100);
        this.m_appWidgetManager = appWidgetManager;
        for (int appWidgetId : appWidgetIds) {
            this.m_remoteViews.setOnClickPendingIntent(R.id.WidgetGodImage80, PendingIntent.getActivity(context, 0, new Intent(context, TempleMainActivity.class), 0));
            RemoteViews remoteViews = this.m_remoteViews;
            int i = this.mintGodImageIndex;
            this.mintGodImageIndex = i + 1;
            remoteViews.setImageViewResource(R.id.WidgetGodImage80, R.drawable.god_imagd_00 + i);
            if (this.mintGodImageIndex >= 36) {
                this.mintGodImageIndex = 0;
            }
            this.m_appWidgetManager.updateAppWidget(appWidgetId, this.m_remoteViews);
        }
        this.m_remoteViews = new RemoteViews(this.m_Context.getPackageName(), (int) R.layout.god_widget_layout_80_100);
    }

    public void onDeleted(Context context, int[] appWidgetIds) {
        super.onDeleted(context, appWidgetIds);
    }

    public void onDisabled(Context context) {
        this.handle.removeCallbacks(this.m_taskImageChanger);
        super.onDisabled(context);
    }

    public void onEnabled(Context context) {
        super.onEnabled(context);
    }

    private class RandomGenerator extends Random {
        private static final long serialVersionUID = 1;

        private RandomGenerator() {
        }

        /* synthetic */ RandomGenerator(GodWidgetProvider_80_100 godWidgetProvider_80_100, RandomGenerator randomGenerator) {
            this();
        }

        public int nextInt(int L_limit, int U_limit) {
            return nextInt((U_limit - L_limit) + 1) + L_limit;
        }
    }
}
