package bys.widgets.srihanuman;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import bys.customviews.adview.BYSAdView;

public class HelpUsActivity extends Activity {
    private Button btnCheckOurOtherApps = null;
    private Button btnGiveFiveStar = null;
    private Button btnSuggestImprovement = null;
    private Button btnTellOthersEmail = null;
    /* access modifiers changed from: private */
    public String mstrAppIdentifier = TempleInfo.strIdentifier;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.help_us_layout);
        this.mstrAppIdentifier = getString(R.string.app_name);
        this.btnSuggestImprovement = (Button) findViewById(R.id.btnSuggestImprovement);
        this.btnGiveFiveStar = (Button) findViewById(R.id.btnGiveFiveStar);
        this.btnTellOthersEmail = (Button) findViewById(R.id.btnTellOthersEmail);
        this.btnCheckOurOtherApps = (Button) findViewById(R.id.btnCheckOurOtherApps);
        if (getResources().getString(R.string.Amazon).compareTo("Y") == 0) {
            this.btnGiveFiveStar.setVisibility(8);
        }
        ((BYSAdView) findViewById(R.id.ad3)).LoadAd(this, 3, 1000);
        this.btnSuggestImprovement.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HelpUsActivity.this.SendAsEmail(new String[]{HelpUsActivity.this.getString(R.string.FeedbackEmail)}, "My Suggestions for " + HelpUsActivity.this.mstrAppIdentifier, "");
            }
        });
        this.btnGiveFiveStar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent("android.intent.action.VIEW");
                intent.setData(Uri.parse("market://details?id=" + HelpUsActivity.this.getPackageName()));
                HelpUsActivity.this.startActivity(intent);
            }
        });
        this.btnTellOthersEmail.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HelpUsActivity.this.SendAsEmail(new String[]{""}, "~~:" + HelpUsActivity.this.mstrAppIdentifier + ":~~", HelpUsActivity.this.GetMessageBody());
            }
        });
        this.btnCheckOurOtherApps.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent("android.intent.action.VIEW");
                if (HelpUsActivity.this.getResources().getString(R.string.Amazon).compareTo("Y") == 0) {
                    intent.setData(Uri.parse("http://www.amazon.com/gp/mas/dl/android?p=[packagename]&showAll=1"));
                } else {
                    intent.setData(Uri.parse("market://search?q=pub:" + HelpUsActivity.this.getString(R.string.PublisherName)));
                }
                HelpUsActivity.this.startActivity(intent);
            }
        });
    }

    /* access modifiers changed from: private */
    public String GetMessageBody() {
        String lstrUrl;
        if (getResources().getString(R.string.Amazon).compareTo("Y") == 0) {
            lstrUrl = "http://www.amazon.com/gp/mas/dl/android?p=" + getPackageName();
        } else {
            lstrUrl = "https://market.android.com/details?id=" + getPackageName();
        }
        return "Check this out.\n\r" + lstrUrl + "\n\r" + "I found this android application very interesting.\n\r";
    }

    /* access modifiers changed from: private */
    public void SendAsEmail(String[] strEmailAddress, String strSubject, String strMessage) {
        Intent i = new Intent("android.intent.action.SEND");
        i.setType("message/rfc822");
        i.putExtra("android.intent.extra.EMAIL", strEmailAddress);
        i.putExtra("android.intent.extra.SUBJECT", strSubject);
        i.putExtra("android.intent.extra.TEXT", strMessage);
        startActivity(Intent.createChooser(i, "Select email application."));
    }
}
