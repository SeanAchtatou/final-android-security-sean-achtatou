package bys.widgets.srihanuman;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;
import bys.apps.components.audiodownloader.AudioListManager;
import bys.apps.components.audiodownloader.AudioListUpdateListeners;
import bys.customviews.adview.BYSAdView;
import bys.customviews.audioplayer.AudioPlayerListeners;
import bys.customviews.audioplayer.AudioPlayerView;
import bys.libs.customuiwidgets.downloaderview.DownloadViewListeners;
import bys.libs.customuiwidgets.downloaderview.FileDownloaderView;
import bys.libs.utils.ringtonemanager.RingToneManager;
import bys.libs.utils.storage.StorageUtil;
import java.io.File;
import java.util.List;
import org.codehaus.jackson.impl.JsonWriteContext;
import org.codehaus.jackson.util.MinimalPrettyPrinter;

public class AudioListActivity extends Activity {
    BYSAdView adView = null;
    /* access modifiers changed from: private */
    public AudioListManager audioListManger = null;
    AudioPlayerView audioPlayerView = null;
    AudioListRowAdapter gcaMessageListAdaptor;
    GridView gridAudioListView = null;
    Handler handler = new Handler();
    Activity mMainActivity = null;
    /* access modifiers changed from: private */
    public AudioListItemView mSelectedRowView = null;
    Runnable m_taskFetchAudioList = null;
    Runnable m_taskRefreshAudioList = null;
    /* access modifiers changed from: private */
    public boolean mblnDownloadInProgress = false;
    /* access modifiers changed from: private */
    public boolean mblnDownloadInitiated = false;
    File mfileRootDir = null;
    /* access modifiers changed from: private */
    public int mintSearchResponseId = 0;
    /* access modifiers changed from: private */
    public String mstrDownloadStatus = null;
    /* access modifiers changed from: private */
    public String mstrNetworkError = null;

    public void DoCleanup() {
        if (this.gcaMessageListAdaptor != null) {
            for (int i = 0; i < this.gcaMessageListAdaptor.getCount(); i++) {
                AudioListItemView pliv = (AudioListItemView) this.gcaMessageListAdaptor.getItem(i);
                Log.v("", "StopDownload pliv = " + pliv);
                if (pliv != null) {
                    pliv.stopDownload();
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        Log.v(TempleInfo.strIdentifier, "AudioListItemView:onDestroy");
        this.audioPlayerView.Destroy();
        this.audioPlayerView = null;
        DoCleanup();
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        Log.v(TempleInfo.strIdentifier, "AudioListItemView:onResume");
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        Log.v(TempleInfo.strIdentifier, "AudioListItemView:onStop");
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        Log.v(TempleInfo.strIdentifier, "AudioListItemView:onPause");
        super.onPause();
    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        if (this.mSelectedRowView != null) {
            super.onCreateContextMenu(menu, v, menuInfo);
            getMenuInflater().inflate(R.menu.search_audio_context_menu, menu);
            if (!this.mSelectedRowView.fileAudio.exists()) {
                if (this.mblnDownloadInProgress) {
                    menu.getItem(1).setVisible(false);
                    menu.getItem(0).setVisible(false);
                } else {
                    menu.getItem(1).setVisible(true);
                    menu.getItem(0).setVisible(true);
                }
                menu.getItem(2).setVisible(false);
                menu.getItem(3).setVisible(false);
                menu.getItem(4).setVisible(false);
                if (this.audioPlayerView != null && this.audioPlayerView.IsPlaying()) {
                    menu.getItem(0).setVisible(false);
                    menu.getItem(2).setVisible(false);
                    menu.getItem(3).setVisible(true);
                    return;
                }
                return;
            }
            menu.getItem(0).setVisible(false);
            menu.getItem(1).setVisible(false);
            menu.getItem(2).setVisible(true);
            menu.getItem(3).setVisible(false);
            menu.getItem(4).setVisible(true);
            if (this.audioPlayerView != null && this.audioPlayerView.IsPlaying()) {
                menu.getItem(2).setVisible(false);
                menu.getItem(3).setVisible(true);
            }
        }
    }

    public boolean onContextItemSelected(MenuItem item) {
        File file = this.mSelectedRowView.fileAudio;
        switch (item.getItemId()) {
            case R.id.mnuSearchAudioContextMenu_Preview /*2131165290*/:
                if (this.audioPlayerView != null) {
                    this.audioPlayerView.Play(this.mSelectedRowView.lstrAudioUrl, this.mSelectedRowView.txtTitle.getText().toString());
                    break;
                }
                break;
            case R.id.mnuSearchAudioContextMenu_Download /*2131165291*/:
                this.mSelectedRowView.beginDownload();
                break;
            case R.id.mnuSearchAudioContextMenu_Play /*2131165292*/:
                if (this.audioPlayerView != null) {
                    this.audioPlayerView.Play(file.getAbsolutePath(), this.mSelectedRowView.txtTitle.getText().toString());
                    break;
                }
                break;
            case R.id.mnuSearchAudioContextMenu_Stop /*2131165293*/:
                this.audioPlayerView.Stop();
                break;
            case R.id.mnuSearchAudioContextMenu_SetAsRingtone /*2131165294*/:
                new RingToneManager(getApplicationContext()).setRingTone(file.getAbsolutePath(), this.mSelectedRowView.txtTitle.getText().toString());
                break;
        }
        return super.onContextItemSelected(item);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(5);
        try {
            setContentView((int) R.layout.audio_list_layout);
            this.adView = (BYSAdView) findViewById(R.id.ad4);
            setProgressBarIndeterminateVisibility(true);
            this.mMainActivity = this;
            try {
                this.mfileRootDir = new File(Environment.getExternalStorageDirectory(), "/" + getString(R.string.app_name));
                this.mfileRootDir.mkdirs();
            } catch (Exception e) {
                e.printStackTrace();
            }
            this.gridAudioListView = (GridView) findViewById(R.id.gridAudioList);
            this.gridAudioListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                    AudioListActivity.this.mSelectedRowView = (AudioListItemView) arg1;
                    if (AudioListActivity.this.mSelectedRowView.imgDownlaod.getVisibility() == 0) {
                        AudioListActivity.this.mSelectedRowView.beginDownload();
                    } else if (AudioListActivity.this.audioPlayerView != null) {
                        AudioListActivity.this.audioPlayerView.Play(AudioListActivity.this.mSelectedRowView.fileAudio.getAbsolutePath(), AudioListActivity.this.mSelectedRowView.txtTitle.getText().toString());
                    }
                }
            });
            this.audioPlayerView = (AudioPlayerView) findViewById(R.id.AudioPlayer);
            this.audioPlayerView.initializePlayer(R.layout.audio_player_view_layout);
            this.audioPlayerView.setAudioPlayerListener(new AudioPlayerListeners() {
                public void onPlayDone() {
                    AudioListActivity.this.adView.setVisibility(0);
                    AudioListActivity.this.adView.LoadAd(AudioListActivity.this.mMainActivity, 3, 1000);
                }

                public void onError(int intErrorStringId) {
                }

                public void onPause() {
                    AudioListActivity.this.adView.setVisibility(0);
                    AudioListActivity.this.adView.LoadAd(AudioListActivity.this.mMainActivity, 3, 1000);
                }

                public void onPositionChanged(int intAudioPosition) {
                }

                public void onPlayResumed() {
                    AudioListActivity.this.adView.setVisibility(8);
                }

                public void onStop() {
                    AudioListActivity.this.adView.setVisibility(0);
                    AudioListActivity.this.adView.LoadAd(AudioListActivity.this.mMainActivity, 3, 1000);
                }

                public void onCurrentAudioPlayDone() {
                    AudioListActivity.this.adView.setVisibility(0);
                    AudioListActivity.this.adView.LoadAd(AudioListActivity.this.mMainActivity, 3, 1000);
                }

                public void onPlayStarted(String mstrAudioSource) {
                }
            });
            registerForContextMenu(this.gridAudioListView);
            this.gcaMessageListAdaptor = new AudioListRowAdapter(this);
            try {
                this.gridAudioListView.setAdapter((ListAdapter) this.gcaMessageListAdaptor);
            } catch (Exception e2) {
                Log.v("IncommingMessageActivity: ", "Setting AudioListRowAdapter Failed!!! " + this.gridAudioListView);
            }
        } catch (Exception exp) {
            exp.printStackTrace();
        }
        try {
            this.gridAudioListView.setAdapter((ListAdapter) this.gcaMessageListAdaptor);
        } catch (Exception e3) {
            Log.v("IncommingMessageActivity: ", "Setting AudioListRowAdapter Failed!!! " + this.gridAudioListView);
        }
        this.m_taskRefreshAudioList = new Runnable() {
            public void run() {
                AudioListActivity.this.setProgressBarIndeterminateVisibility(false);
                switch (AudioListActivity.this.mintSearchResponseId) {
                    case 0:
                        AudioListActivity.this.mMainActivity.setTitle("Search Completed");
                        break;
                    case 1:
                        AudioListActivity.this.mMainActivity.setTitle(AudioListActivity.this.mstrNetworkError);
                        break;
                    case 2:
                        AudioListActivity.this.mMainActivity.setTitle("Search Failed...");
                        Toast.makeText(AudioListActivity.this.getApplicationContext(), "Search Failed...  \n\nCheck network connection and try again", 1).show();
                        break;
                }
                AudioListActivity.this.gcaMessageListAdaptor.setAudioInfoList(AudioListActivity.this.audioListManger.getAudioInfoList());
                AudioListActivity.this.gcaMessageListAdaptor.notifyDataSetChanged();
            }
        };
        this.audioListManger = new AudioListManager(this);
        this.audioListManger.setInfcAudioListUpdateListeners(new AudioListUpdateListeners() {
            public void onUpdateAvailable() {
                Log.v("", "onUpdateAvailable");
                AudioListActivity.this.mintSearchResponseId = 0;
                if (!AudioListActivity.this.mblnDownloadInitiated) {
                    AudioListActivity.this.handler.post(AudioListActivity.this.m_taskRefreshAudioList);
                }
            }

            public void onNeworkError(String error) {
                Log.v("", "onNeworkError");
                AudioListActivity.this.mintSearchResponseId = 1;
                AudioListActivity.this.mstrNetworkError = error;
                AudioListActivity.this.handler.post(AudioListActivity.this.m_taskRefreshAudioList);
            }

            public void onSearchFailed() {
                AudioListActivity.this.mintSearchResponseId = 2;
                AudioListActivity.this.handler.post(AudioListActivity.this.m_taskRefreshAudioList);
            }
        });
        this.audioListManger.CheckForUpdates();
    }

    public class AudioListRowAdapter extends BaseAdapter {
        private Context mContext;
        AudioListItemView[] marrAudioItemView = null;
        List<String[]> mlistAudioInfo = null;

        public AudioListRowAdapter(Context c) {
            this.mContext = c;
        }

        public void setAudioInfoList(List<String[]> audioInfoList) {
            this.mlistAudioInfo = audioInfoList;
            this.marrAudioItemView = new AudioListItemView[this.mlistAudioInfo.size()];
        }

        public int getCount() {
            if (this.mlistAudioInfo != null) {
                return this.mlistAudioInfo.size();
            }
            return 0;
        }

        public Object getItem(int position) {
            return this.marrAudioItemView[position];
        }

        public long getItemId(int position) {
            return 0;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            if (this.marrAudioItemView[position] == null) {
                AudioListItemView mRowView = new AudioListItemView(this.mContext, null);
                Log.v("", "AudioListActivity:getView (" + position + ")" + mRowView);
                mRowView.setPosition(position);
                if (this.mlistAudioInfo != null && this.mlistAudioInfo.size() > position) {
                    String[] audioInfo = this.mlistAudioInfo.get(position);
                    try {
                        mRowView.setAudioTitle(audioInfo[0]);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        mRowView.txtSize.setText(audioInfo[3]);
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                    try {
                        mRowView.setAudioUrl(audioInfo[2]);
                    } catch (Exception e3) {
                        e3.printStackTrace();
                    }
                }
                mRowView.setOnTouchListener(new View.OnTouchListener() {
                    public boolean onTouch(View v, MotionEvent event) {
                        if (event.getAction() == 0) {
                            AudioListActivity.this.mSelectedRowView = (AudioListItemView) v;
                        }
                        if (event.getAction() != 1) {
                            return false;
                        }
                        AudioListActivity.this.mSelectedRowView = null;
                        return false;
                    }
                });
                this.marrAudioItemView[position] = mRowView;
            }
            return this.marrAudioItemView[position];
        }
    }

    public class AudioListItemView extends LinearLayout {
        File fileAudio;
        ImageView imgDownlaod;
        ImageView imgPlay;
        int intAudioId = 0;
        int intAudioState = 0;
        int intPosition;
        String lstrAudioFilePath = "";
        String lstrAudioUrl = "";
        private View mView;
        int m_intViewRefreshCode = 0;
        Runnable m_taskViewRefresher = null;
        String mstrAudioFileName;
        String mstrDownloadError = "";
        TextView txtDesc;
        TextView txtSize;
        TextView txtTitle;
        FileDownloaderView viewDownloader;

        public AudioListItemView(Context context, AttributeSet attrs) {
            super(context, attrs);
            ((LayoutInflater) context.getSystemService("layout_inflater")).inflate((int) R.layout.audio_list_item, this);
            this.txtTitle = (TextView) findViewById(R.id.txtTitle);
            this.txtDesc = (TextView) findViewById(R.id.txtDesc);
            this.txtSize = (TextView) findViewById(R.id.txtSize);
            this.imgDownlaod = (ImageView) findViewById(R.id.imgDownload);
            this.imgPlay = (ImageView) findViewById(R.id.imgPlay);
            this.viewDownloader = (FileDownloaderView) findViewById(R.id.viewDownloader);
            this.m_taskViewRefresher = new Runnable() {
                public void run() {
                    switch (AudioListItemView.this.m_intViewRefreshCode) {
                        case 1:
                            AudioListItemView.this.imgDownlaod.setEnabled(false);
                            AudioListItemView.this.txtDesc.setText("Downloading...");
                            AudioListItemView.this.viewDownloader.setVisibility(0);
                            return;
                        case 2:
                            AudioListItemView.this.txtDesc.setText(AudioListItemView.this.mstrDownloadError);
                            AudioListItemView.this.imgDownlaod.setEnabled(true);
                            AudioListItemView.this.viewDownloader.setVisibility(8);
                            return;
                        case 3:
                            AudioListItemView.this.imgPlay.setVisibility(0);
                            AudioListItemView.this.imgDownlaod.setVisibility(8);
                            AudioListItemView.this.viewDownloader.setVisibility(8);
                            AudioListItemView.this.txtDesc.setText("Download done.");
                            AudioListActivity.this.adView.setVisibility(0);
                            AudioListActivity.this.adView.LoadAd(AudioListActivity.this.mMainActivity, 3, 1000);
                            return;
                        case JsonWriteContext.STATUS_EXPECT_VALUE /*4*/:
                            AudioListItemView.this.txtDesc.setText(AudioListActivity.this.mstrDownloadStatus);
                            return;
                        default:
                            return;
                    }
                }
            };
            this.viewDownloader.setOnDownloadDoneListener(new DownloadViewListeners() {
                public void onDownloadDone(String strFileName) {
                    AudioListItemView.this.m_intViewRefreshCode = 3;
                    AudioListActivity.this.mblnDownloadInProgress = false;
                    AudioListActivity.this.handler.post(AudioListItemView.this.m_taskViewRefresher);
                    SharedPreferences.Editor editor = AudioListActivity.this.getSharedPreferences(TempleInfo.strIdentifier, 0).edit();
                    editor.remove("Key_AudioFileSize_" + AudioListItemView.this.viewDownloader.getFileName());
                    editor.commit();
                }

                public void onDownloadStarted(int intFileSize) {
                    AudioListItemView.this.m_intViewRefreshCode = 1;
                    AudioListActivity.this.handler.post(AudioListItemView.this.m_taskViewRefresher);
                    AudioListActivity.this.mblnDownloadInProgress = true;
                    SharedPreferences.Editor editor = AudioListActivity.this.getSharedPreferences(TempleInfo.strIdentifier, 0).edit();
                    editor.putInt("Key_AudioFileSize_" + AudioListItemView.this.viewDownloader.getFileName(), intFileSize);
                    editor.commit();
                }

                public void onError(String strError) {
                    AudioListItemView.this.m_intViewRefreshCode = 2;
                    AudioListItemView.this.mstrDownloadError = strError;
                    AudioListActivity.this.handler.post(AudioListItemView.this.m_taskViewRefresher);
                    AudioListActivity.this.mblnDownloadInProgress = false;
                }

                public void onUpdateStatus(String string) {
                    AudioListItemView.this.m_intViewRefreshCode = 2;
                    AudioListItemView.this.mstrDownloadError = string;
                    AudioListActivity.this.handler.post(AudioListItemView.this.m_taskViewRefresher);
                }

                public void OnProgressUpdate(int fltPercent) {
                    AudioListItemView.this.m_intViewRefreshCode = 4;
                    AudioListActivity.this.mstrDownloadStatus = "Downloading... " + fltPercent + "% Done";
                    AudioListActivity.this.handler.post(AudioListItemView.this.m_taskViewRefresher);
                }
            });
            this.imgPlay.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    if (AudioListActivity.this.audioPlayerView != null) {
                        Log.v("", "Play " + AudioListItemView.this.fileAudio);
                        AudioListActivity.this.audioPlayerView.Play(AudioListItemView.this.fileAudio.getAbsolutePath(), AudioListItemView.this.txtTitle.getText().toString());
                    }
                }
            });
            this.imgDownlaod.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    AudioListItemView.this.beginDownload();
                }
            });
        }

        public void beginDownload() {
            Log.v("", "beginDownload " + this.fileAudio);
            if (!AudioListActivity.this.mblnDownloadInProgress) {
                AudioListActivity.this.mblnDownloadInitiated = true;
                if (this.viewDownloader.IsDownloadStopped()) {
                    int intExternalStorageError = StorageUtil.checkExternalStorage();
                    if (intExternalStorageError > 0) {
                        this.txtDesc.setText(StorageUtil.getErrorString(intExternalStorageError));
                        Log.v("", "checkExternalStorage error id: " + intExternalStorageError);
                        return;
                    }
                    this.viewDownloader.SetLocalFile(AudioListActivity.this.mfileRootDir, this.mstrAudioFileName);
                    this.viewDownloader.StartDownloading(this.lstrAudioUrl);
                    return;
                }
                Log.v("", "Download already started " + this.fileAudio);
                return;
            }
            Toast.makeText(AudioListActivity.this.getApplicationContext(), "Please wait.. Audio download is in progress", 1).show();
        }

        public void stopDownload() {
            Log.v("Audio List Item View", "StopDownload");
            this.viewDownloader.StopDownloading();
        }

        public void setAudioTitle(String strTitle) {
            this.txtTitle.setText(strTitle);
            this.mstrAudioFileName = String.valueOf(this.txtTitle.getText().toString().toLowerCase().replace(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR, "_")) + ".mp3";
            this.mstrAudioFileName = this.mstrAudioFileName.replace(":", "_");
            this.fileAudio = new File(AudioListActivity.this.mfileRootDir.getAbsoluteFile() + "/" + this.mstrAudioFileName);
            if (this.fileAudio.exists()) {
                this.imgPlay.setVisibility(0);
                this.imgDownlaod.setVisibility(8);
                this.txtDesc.setText("Audio available in SD card");
                return;
            }
            int intTotalFileSize = AudioListActivity.this.getSharedPreferences(TempleInfo.strIdentifier, 0).getInt("Key_AudioFileSize_" + this.fileAudio.getName(), 0);
            int intPartaillyDownloadedSize = this.viewDownloader.checkIfPartiallyDownloaded(this.fileAudio, intTotalFileSize);
            if (intPartaillyDownloadedSize == 0) {
                this.txtDesc.setText("Tap globe icon to download");
                return;
            }
            this.viewDownloader.setVisibility(0);
            if (intTotalFileSize > 0) {
                this.txtDesc.setText("Partially downloaded (" + ((intPartaillyDownloadedSize * 100) / intTotalFileSize) + "%)");
            }
        }

        public void setAudioUrl(String url) {
            this.lstrAudioUrl = url;
        }

        public void setPosition(int position) {
            this.intPosition = position;
        }

        public void setView(View mView2) {
            this.mView = mView2;
        }

        public View getView() {
            return this.mView;
        }
    }
}
