package bys.widgets.srihanuman;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class LicenseAgreementActivity extends Activity {
    public static String KEY_AGREEMENT_ACCEPTANCE = "Key_AgreementAccepted";
    private Button btnAccept = null;
    private Button btnDecline = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.license_agreement_layout);
        this.btnAccept = (Button) findViewById(R.id.btnAccept);
        this.btnDecline = (Button) findViewById(R.id.btnDecline);
        this.btnAccept.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SharedPreferences.Editor editor = LicenseAgreementActivity.this.getSharedPreferences(TempleInfo.strIdentifier, 0).edit();
                editor.putBoolean(LicenseAgreementActivity.KEY_AGREEMENT_ACCEPTANCE, true);
                editor.commit();
                LicenseAgreementActivity.this.startActivity(new Intent(LicenseAgreementActivity.this.getApplicationContext(), AudioListActivity.class));
                LicenseAgreementActivity.this.finish();
            }
        });
        this.btnDecline.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SharedPreferences.Editor editor = LicenseAgreementActivity.this.getSharedPreferences(TempleInfo.strIdentifier, 0).edit();
                editor.putBoolean(LicenseAgreementActivity.KEY_AGREEMENT_ACCEPTANCE, false);
                editor.commit();
                LicenseAgreementActivity.this.finish();
            }
        });
    }
}
