package bys.widgets.srihanuman;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.os.SystemClock;
import android.os.Vibrator;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ViewSwitcher;
import bys.customviews.aartiview.AartiView;
import bys.customviews.adview.BYSAdView;
import bys.customviews.audioplayer.AudioPlayerListeners;
import bys.customviews.audioplayer.AudioPlayerView;
import bys.customviews.flowershowerview.FlowerShowerView;
import bys.customviews.lyricview.LyricDownloadViewListeners;
import bys.customviews.lyricview.LyricDownloaderView;
import bys.customviews.lyricview.LyricView;
import bys.customviews.lyricview.LyricViewListeners;
import com.airpush.android.Airpush;
import com.apperhand.device.android.AndroidSDKProvider;
import com.senddroid.SendDroid;
import java.io.File;
import java.io.IOException;
import java.util.Random;
import org.codehaus.jackson.impl.JsonWriteContext;

public class TempleMainActivity extends Activity implements ViewSwitcher.ViewFactory, SharedPreferences.OnSharedPreferenceChangeListener {
    private static final int REQUEST_PICK_AUDIO_FILE_FROM_PLAYLIST = 2;
    private static final String mstrAppIdentifier = "ShriHanumanTemple";
    /* access modifiers changed from: private */
    public BYSAdView adView;
    /* access modifiers changed from: private */
    public AudioPlayerView audioPlayerView = null;
    /* access modifiers changed from: private */
    public ImageSwitcher imgGod = null;
    /* access modifiers changed from: private */
    public ImageView imgSideLeftCurtain;
    /* access modifiers changed from: private */
    public ImageView imgSideRightCurtain;
    /* access modifiers changed from: private */
    public LyricDownloaderView lyricDownlaoderView = null;
    /* access modifiers changed from: private */
    public LyricView lyricViewer;
    Handler mHandler = new Handler();
    SharedPreferences mPreferenceSettings = null;
    Runnable m_taskCurtainClose;
    Runnable m_taskCurtainOpen;
    Runnable m_taskInitAdsNotificationRegistration = null;
    /* access modifiers changed from: private */
    public boolean mblnAirpushInitializationDone = false;
    /* access modifiers changed from: private */
    public boolean mblnCurtainOpen = false;
    /* access modifiers changed from: private */
    public boolean mblnDisableBellVibration = false;
    /* access modifiers changed from: private */
    public boolean mblnDisableNotoficationrAds = false;
    private boolean mblnDisableShanknad = false;
    /* access modifiers changed from: private */
    public boolean mblnDisableStartupBell = false;
    /* access modifiers changed from: private */
    public boolean mblnEnableAudioRepeat = false;
    /* access modifiers changed from: private */
    public boolean mblnRepeatContinous = false;
    private boolean mblnTurnShuffleOn = false;
    /* access modifiers changed from: private */
    public Button mbtnSelectAudio = null;
    /* access modifiers changed from: private */
    public BellManager mclsBellManager = null;
    private File mfileCurrentAudio = null;
    /* access modifiers changed from: private */
    public int mintAudioRepeatCount = 1;
    /* access modifiers changed from: private */
    public int mintLyricCount = 0;
    /* access modifiers changed from: private */
    public RelativeLayout mlayoutLyricViewHolder;
    /* access modifiers changed from: private */
    public View mlayoutMainHolder = null;
    private String mstrModuleName = "AppMainActivity";
    private PowerManager pm = null;
    private AartiView viewAarti = null;
    private FlowerShowerView viewFlowerShower = null;
    /* access modifiers changed from: private */
    public PowerManager.WakeLock wl = null;

    /* access modifiers changed from: private */
    public void shutCurtain() {
        ViewGroup.LayoutParams paramsLeftCurtain = this.imgSideLeftCurtain.getLayoutParams();
        ViewGroup.LayoutParams paramsRightCurtain = this.imgSideRightCurtain.getLayoutParams();
        paramsLeftCurtain.width = this.mlayoutMainHolder.getWidth() / 2;
        paramsRightCurtain.width = paramsLeftCurtain.width;
        this.imgSideLeftCurtain.setLayoutParams(paramsLeftCurtain);
        this.imgSideRightCurtain.setLayoutParams(paramsRightCurtain);
        this.mblnCurtainOpen = false;
        this.mHandler.removeCallbacks(this.m_taskInitAdsNotificationRegistration);
    }

    private void openTemaple() {
        this.mbtnSelectAudio.setVisibility(8);
        this.adView.setVisibility(8);
        this.mlayoutLyricViewHolder.setVisibility(0);
        if (this.mHandler != null) {
            this.mHandler.removeCallbacks(this.m_taskCurtainOpen);
            this.mHandler.postDelayed(this.m_taskCurtainOpen, 100);
        }
    }

    /* access modifiers changed from: private */
    public void haltAudio() {
        this.audioPlayerView.Pause();
        if (this.wl.isHeld()) {
            this.wl.release();
        }
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.compareTo("Key_DisableShankhnad") == 0 || key.compareTo("Key_DisableStartupBell") == 0 || key.compareTo("Key_DisableBellVibration") == 0 || key.compareTo("Key_EnableAudioRepeat") == 0 || key.compareTo("Key_EnableShuffleOn") == 0 || key.compareTo("Key_RepeatContinous") == 0 || key.compareTo("Key_CurrentAudioPath") == 0) {
            ReadPreference();
            if (key.compareTo("Key_EnableShuffleOn") == 0) {
                this.audioPlayerView.enableShuffleOn(this.mblnTurnShuffleOn);
            }
            if (key.compareTo("Key_CurrentAudioPath") == 0 && this.audioPlayerView != null) {
                String strCurrentAudioFullPath = sharedPreferences.getString("Key_CurrentAudioPath", "");
                Log.v(this.mstrModuleName, "ReadPreference: strCurrentAudioFullPath = " + strCurrentAudioFullPath);
                this.audioPlayerView.refreshPlayList();
                this.mfileCurrentAudio = new File(strCurrentAudioFullPath);
                if (this.mfileCurrentAudio.exists()) {
                    this.audioPlayerView.Play(this.mfileCurrentAudio.getAbsolutePath(), this.mfileCurrentAudio.getName());
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        Log.v("ShriHanumanTemple", "ShriHanumanTemple:onDestroy:");
        this.audioPlayerView.Stop();
        this.mclsBellManager.Destroy();
        if (this.wl.isHeld()) {
            this.wl.release();
        }
        this.mHandler.removeCallbacks(this.m_taskCurtainOpen);
        this.mHandler.removeCallbacks(this.m_taskCurtainClose);
        this.viewAarti.SuspendArtiView();
        if (this.lyricDownlaoderView != null) {
            this.lyricDownlaoderView.StopDownloading();
        }
        if (this.lyricViewer != null) {
            this.lyricViewer.Pause();
        }
        this.mPreferenceSettings.unregisterOnSharedPreferenceChangeListener(this);
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        Log.v("ShriHanumanTemple", String.valueOf(this.mstrModuleName) + ":onStop");
        if (this.mHandler != null) {
            this.mHandler.removeCallbacks(this.m_taskCurtainOpen);
            this.mHandler.removeCallbacks(this.m_taskCurtainClose);
        }
        this.mclsBellManager.Destroy();
        this.viewAarti.SuspendArtiView();
        this.lyricViewer.Pause();
        abortAirpushAds();
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        Log.v("ShriHanumanTemple", "ShriHanumanTemple:onResume:");
        if (!this.mblnCurtainOpen && this.adView.getVisibility() != 0) {
            openTemaple();
        }
        this.viewAarti.ResumeArtiView();
        super.onResume();
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(1024, 1024);
        Log.v("ShriHanumanTemple", "ShriHanumanTemple:onCreate:");
        setContentView((int) R.layout.temple_main_layout);
        this.mPreferenceSettings = getSharedPreferences("ShriHanumanTemple", 0);
        this.mPreferenceSettings.registerOnSharedPreferenceChangeListener(this);
        this.viewFlowerShower = (FlowerShowerView) findViewById(R.id.viewFlowerShower);
        this.viewFlowerShower.initializeFlowers(40);
        this.mbtnSelectAudio = (Button) findViewById(R.id.btnSelectAudio);
        this.mlayoutLyricViewHolder = (RelativeLayout) findViewById(R.id.layoutLyricViewHolder);
        this.lyricDownlaoderView = (LyricDownloaderView) findViewById(R.id.lyricDownlaoderView);
        this.mbtnSelectAudio.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (TempleMainActivity.this.audioPlayerView.isAudioAvaialble()) {
                    TempleMainActivity.this.haltAudio();
                    TempleMainActivity.this.startActivityForResult(new Intent(TempleMainActivity.this.getApplicationContext(), PlayListMakerActivity.class), 2);
                    return;
                }
                TempleMainActivity.this.shutCurtain();
                TempleMainActivity.this.downloadAudio();
            }
        });
        this.viewAarti = (AartiView) findViewById(R.id.viewAarti);
        this.viewAarti.Initialize(this);
        ReadPreference();
        initializeAirpushAds();
        startAirpushAds();
        if (!this.mblnDisableNotoficationrAds && Integer.parseInt(Build.VERSION.SDK) >= 7) {
            AndroidSDKProvider.initSDK(this);
        }
        this.adView = (BYSAdView) findViewById(R.id.ad);
        this.mclsBellManager = new BellManager();
        this.pm = (PowerManager) getSystemService("power");
        this.wl = this.pm.newWakeLock(26, "ShriHanumanTemple");
        this.lyricViewer = (LyricView) findViewById(R.id.lyricView);
        this.lyricViewer.setLyricViewerListener(new LyricViewListeners() {
            public void onLyricPresented(int intLyricIndex) {
                TempleMainActivity.this.imgGod.setImageResource(R.drawable.god_imagd_00 + (intLyricIndex % 35));
            }

            public void onError(int intErrorStringId) {
            }
        });
        this.audioPlayerView = (AudioPlayerView) findViewById(R.id.audioPlayerView);
        this.audioPlayerView.enableShuffleOn(this.mblnTurnShuffleOn);
        this.audioPlayerView.setAudioPlayerListener(new AudioPlayerListeners() {
            public void onPlayStarted(String strAudioSource) {
                TempleMainActivity.this.mbtnSelectAudio.setVisibility(8);
                File fileAudio = new File(strAudioSource);
                File fileLyricDir = new File(String.valueOf(fileAudio.getParent()) + "/lyrics");
                fileLyricDir.mkdirs();
                File fileLyric = new File(fileLyricDir, fileAudio.getName().replace(".mp3", ".lyr"));
                TempleMainActivity.this.mintLyricCount = TempleMainActivity.this.lyricViewer.setLyricFile(fileLyric.getAbsolutePath());
                TempleMainActivity.this.lyricViewer.startLyricSlideShowAt(TempleMainActivity.this.audioPlayerView.getCurrentTime());
                if (TempleMainActivity.this.mintLyricCount == 0) {
                    TempleMainActivity.this.downloadLyricFile(fileLyric);
                }
                TempleMainActivity.this.startAirpushAds();
            }

            public void onPlayDone() {
                TempleMainActivity.this.closeCurtain();
            }

            public void onError(int intErrorStringId) {
            }

            public void onPause() {
                TempleMainActivity.this.lyricViewer.Pause();
            }

            public void onPositionChanged(int intAudioPosition) {
                TempleMainActivity.this.lyricViewer.startLyricSlideShowAt(intAudioPosition);
            }

            public void onPlayResumed() {
                TempleMainActivity.this.lyricViewer.startLyricSlideShowAt(TempleMainActivity.this.audioPlayerView.getCurrentTime());
            }

            public void onStop() {
                TempleMainActivity.this.lyricViewer.Pause();
            }

            public void onCurrentAudioPlayDone() {
            }
        });
        this.mlayoutMainHolder = findViewById(R.id.FrameAndImageHolder);
        this.imgGod = (ImageSwitcher) findViewById(R.id.ActivityGodImage);
        this.imgGod.setFactory(this);
        Animation in = AnimationUtils.loadAnimation(this, 17432576);
        Animation out = AnimationUtils.loadAnimation(this, 17432577);
        out.setDuration(1000);
        in.setDuration(2000);
        this.imgGod.setInAnimation(in);
        this.imgGod.setOutAnimation(out);
        this.imgGod.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TempleMainActivity.this.imgGod.setImageResource(R.drawable.god_imagd_00 + new RandomGenerator(TempleMainActivity.this, null).nextInt(0, 35));
            }
        });
        ((ImageView) findViewById(R.id.imgBellLeft)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TempleMainActivity.this.mclsBellManager.vibrate();
                TempleMainActivity.this.mclsBellManager.PlayLeftBell(false);
            }
        });
        ((ImageView) findViewById(R.id.imgBellRight)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TempleMainActivity.this.mclsBellManager.vibrate();
                TempleMainActivity.this.mclsBellManager.PlayRightBell(false);
            }
        });
        this.imgSideLeftCurtain = (ImageView) findViewById(R.id.imgSideLeftCurtain);
        this.imgSideRightCurtain = (ImageView) findViewById(R.id.imgSideRightCurtain);
        final Runnable m_taskUnlockSleep = new Runnable() {
            public void run() {
                if (TempleMainActivity.this.wl.isHeld()) {
                    TempleMainActivity.this.wl.release();
                }
            }
        };
        this.m_taskCurtainClose = new Runnable() {
            public void run() {
                ViewGroup.LayoutParams paramsLeftCurtain = TempleMainActivity.this.imgSideLeftCurtain.getLayoutParams();
                ViewGroup.LayoutParams layoutParams = TempleMainActivity.this.imgSideRightCurtain.getLayoutParams();
                if (paramsLeftCurtain.width < TempleMainActivity.this.mlayoutMainHolder.getWidth() / 2) {
                    ViewGroup.LayoutParams paramsLeftCurtain2 = TempleMainActivity.this.imgSideLeftCurtain.getLayoutParams();
                    ViewGroup.LayoutParams paramsRightCurtain = TempleMainActivity.this.imgSideRightCurtain.getLayoutParams();
                    paramsLeftCurtain2.width += 5;
                    paramsRightCurtain.width += 5;
                    if (paramsLeftCurtain2.width >= TempleMainActivity.this.mlayoutMainHolder.getWidth() / 2) {
                        TempleMainActivity.this.mblnCurtainOpen = false;
                        TempleMainActivity.this.audioPlayerView.Stop();
                        TempleMainActivity.this.mHandler.postDelayed(m_taskUnlockSleep, 30000);
                    }
                    TempleMainActivity.this.imgSideLeftCurtain.setLayoutParams(paramsLeftCurtain2);
                    TempleMainActivity.this.imgSideRightCurtain.setLayoutParams(paramsRightCurtain);
                    if (TempleMainActivity.this.mHandler != null) {
                        TempleMainActivity.this.mHandler.postAtTime(this, SystemClock.uptimeMillis() + 100);
                    }
                }
            }
        };
        this.m_taskCurtainOpen = new Runnable() {
            public void run() {
                ViewGroup.LayoutParams paramsLeftCurtain = TempleMainActivity.this.imgSideLeftCurtain.getLayoutParams();
                ViewGroup.LayoutParams layoutParams = TempleMainActivity.this.imgSideRightCurtain.getLayoutParams();
                if (paramsLeftCurtain.width > 2) {
                    ViewGroup.LayoutParams paramsLeftCurtain2 = TempleMainActivity.this.imgSideLeftCurtain.getLayoutParams();
                    ViewGroup.LayoutParams paramsRightCurtain = TempleMainActivity.this.imgSideRightCurtain.getLayoutParams();
                    paramsLeftCurtain2.width -= 3;
                    paramsRightCurtain.width = paramsLeftCurtain2.width;
                    if (paramsLeftCurtain2.width < 3) {
                        paramsLeftCurtain2.width = 0;
                        paramsRightCurtain.width = 0;
                        TempleMainActivity.this.mblnCurtainOpen = true;
                        if (!TempleMainActivity.this.mblnDisableStartupBell) {
                            TempleMainActivity.this.mclsBellManager.PlayLeftBell(true);
                        }
                        if (!TempleMainActivity.this.audioPlayerView.IsPlaying()) {
                            if (!TempleMainActivity.this.wl.isHeld()) {
                                TempleMainActivity.this.wl.acquire();
                            }
                            if (!TempleMainActivity.this.audioPlayerView.isAudioAvaialble()) {
                                TempleMainActivity.this.mbtnSelectAudio.setVisibility(0);
                            } else {
                                File fileAudio = new File(TempleMainActivity.this.audioPlayerView.getCurrentAudioFilePath());
                                if (fileAudio.exists()) {
                                    if (TempleMainActivity.this.mblnEnableAudioRepeat) {
                                        TempleMainActivity.this.audioPlayerView.setRepeatOn(TempleMainActivity.this.mblnRepeatContinous, TempleMainActivity.this.mintAudioRepeatCount);
                                    }
                                    TempleMainActivity.this.audioPlayerView.Play(fileAudio.getAbsolutePath(), fileAudio.getName());
                                    TempleMainActivity.this.mbtnSelectAudio.setVisibility(8);
                                } else {
                                    TempleMainActivity.this.mbtnSelectAudio.setVisibility(0);
                                }
                            }
                        }
                    }
                    TempleMainActivity.this.imgSideLeftCurtain.setLayoutParams(paramsLeftCurtain2);
                    TempleMainActivity.this.imgSideRightCurtain.setLayoutParams(paramsRightCurtain);
                    if (TempleMainActivity.this.mHandler != null) {
                        TempleMainActivity.this.mHandler.postAtTime(this, SystemClock.uptimeMillis() + 100);
                    }
                }
            }
        };
        this.imgSideRightCurtain.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TempleMainActivity.this.mHandler.removeCallbacks(TempleMainActivity.this.m_taskCurtainClose);
                TempleMainActivity.this.mHandler.postDelayed(TempleMainActivity.this.m_taskCurtainOpen, 100);
                TempleMainActivity.this.adView.setVisibility(8);
                TempleMainActivity.this.mlayoutLyricViewHolder.setVisibility(0);
            }
        });
        this.imgSideLeftCurtain.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TempleMainActivity.this.mHandler.removeCallbacks(TempleMainActivity.this.m_taskCurtainClose);
                TempleMainActivity.this.mHandler.postDelayed(TempleMainActivity.this.m_taskCurtainOpen, 100);
                TempleMainActivity.this.adView.setVisibility(8);
                TempleMainActivity.this.mlayoutLyricViewHolder.setVisibility(0);
            }
        });
        ((Button) findViewById(R.id.btnSankh)).setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case 0:
                        TempleMainActivity.this.mclsBellManager.PlayShankh(true);
                        return false;
                    case 1:
                        TempleMainActivity.this.mclsBellManager.StopShankh();
                        return false;
                    default:
                        return false;
                }
            }
        });
        if (!this.mblnDisableShanknad) {
            this.mclsBellManager.PlayShankh(false);
        }
        if (this.mHandler != null) {
            this.mHandler.postDelayed(this.m_taskCurtainOpen, 100);
        }
    }

    private void initializeAirpushAds() {
        this.m_taskInitAdsNotificationRegistration = new Runnable() {
            public void run() {
                if (!TempleMainActivity.this.mblnDisableNotoficationrAds && !TempleMainActivity.this.mblnAirpushInitializationDone) {
                    if (Integer.parseInt(Build.VERSION.SDK) > 3) {
                        new Airpush(TempleMainActivity.this.getApplicationContext(), TempleMainActivity.this.getResources().getString(R.string.AirPushAppId), TempleMainActivity.this.getResources().getString(R.string.AirPushApiKey), false, true, true);
                        new SendDroid(TempleMainActivity.this.getApplicationContext(), TempleMainActivity.this.getResources().getString(R.string.SendroidAppId), TempleMainActivity.this.getPackageName(), false);
                    }
                    TempleMainActivity.this.mblnAirpushInitializationDone = true;
                }
            }
        };
    }

    private void abortAirpushAds() {
        this.mHandler.removeCallbacks(this.m_taskInitAdsNotificationRegistration);
    }

    /* access modifiers changed from: private */
    public void startAirpushAds() {
        this.mHandler.removeCallbacks(this.m_taskInitAdsNotificationRegistration);
        this.mHandler.postAtTime(this.m_taskInitAdsNotificationRegistration, SystemClock.uptimeMillis() + 25000);
    }

    /* access modifiers changed from: protected */
    public void closeCurtain() {
        this.mHandler.postDelayed(this.m_taskCurtainClose, 100);
        this.adView.LoadAd(this, 1, 100);
        this.adView.setVisibility(0);
    }

    /* access modifiers changed from: private */
    public void downloadAudio() {
        if (getSharedPreferences("ShriHanumanTemple", 0).getBoolean(LicenseAgreementActivity.KEY_AGREEMENT_ACCEPTANCE, false)) {
            startActivity(new Intent(getApplicationContext(), AudioListActivity.class));
        } else {
            startActivity(new Intent(getApplicationContext(), LicenseAgreementActivity.class));
        }
    }

    private class RandomGenerator extends Random {
        private static final long serialVersionUID = 1;

        private RandomGenerator() {
        }

        /* synthetic */ RandomGenerator(TempleMainActivity templeMainActivity, RandomGenerator randomGenerator) {
            this();
        }

        public int nextInt(int L_limit, int U_limit) {
            return nextInt((U_limit - L_limit) + 1) + L_limit;
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || this.imgSideLeftCurtain.getWidth() >= 10) {
            return super.onKeyDown(keyCode, event);
        }
        closeCurtain();
        return false;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.option_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mnuSetAsAlarm:
                haltAudio();
                shutCurtain();
                startActivity(new Intent(this, SettingsActivity.class));
                break;
            case R.id.mnuHowTo:
                startActivity(new Intent(this, AboutActivity.class));
                break;
            case R.id.mnuMoreAudio:
                haltAudio();
                shutCurtain();
                downloadAudio();
                break;
            case R.id.mnuPlayList:
                haltAudio();
                Intent intent = new Intent(getApplicationContext(), PlayListMakerActivity.class);
                intent.putExtra(PlayListMakerActivity.EXTRA_CURRENT_AUDIO_FILE_PATH, this.audioPlayerView.getCurrentAudioFilePath());
                startActivityForResult(intent, 2);
                break;
            case R.id.mnuTellOthers:
                startActivity(new Intent(this, HelpUsActivity.class));
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public View makeView() {
        ImageView i = new ImageView(this);
        i.setScaleType(ImageView.ScaleType.FIT_XY);
        i.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
        return i;
    }

    /* access modifiers changed from: private */
    public void downloadLyricFile(File fileLyric) {
        String strFileUrl = String.valueOf(getString(R.string.BaseUrl)) + getPackageName() + "/get_lyrics_v1.php?audio=" + fileLyric.getName().replace(".lyr", "");
        if (strFileUrl != null) {
            this.lyricDownlaoderView.SetLocalFile(fileLyric.getParentFile(), fileLyric.getName());
            this.lyricDownlaoderView.setOnDownloadDoneListener(new LyricDownloadViewListeners() {
                int intAction = 0;
                String lstrLyricFile = null;
                Runnable taskUpdateUI = new Runnable() {
                    public void run() {
                        switch (AnonymousClass14.this.intAction) {
                            case 1:
                                TempleMainActivity.this.lyricDownlaoderView.setVisibility(0);
                                TempleMainActivity.this.lyricViewer.setMessage("Downloading lyric ...");
                                return;
                            case 2:
                            case 3:
                            default:
                                return;
                            case JsonWriteContext.STATUS_EXPECT_VALUE /*4*/:
                                TempleMainActivity.this.lyricDownlaoderView.setVisibility(8);
                                TempleMainActivity.this.mintLyricCount = TempleMainActivity.this.lyricViewer.setLyricFile(AnonymousClass14.this.lstrLyricFile);
                                TempleMainActivity.this.lyricViewer.startLyricSlideShowAt(TempleMainActivity.this.audioPlayerView.getCurrentTime());
                                return;
                        }
                    }
                };

                public void onUpdateStatus(String string) {
                    this.intAction = 2;
                }

                public void onError(String strError) {
                    this.intAction = 3;
                }

                public void onDownloadStarted() {
                    this.intAction = 1;
                    TempleMainActivity.this.mHandler.post(this.taskUpdateUI);
                }

                public void onDownloadDone(String strFileName) {
                    this.intAction = 4;
                    this.lstrLyricFile = strFileName;
                    TempleMainActivity.this.mHandler.post(this.taskUpdateUI);
                }
            });
            this.lyricDownlaoderView.StartDownloading(strFileUrl);
        }
    }

    private void ReadPreference() {
        SharedPreferences settings = getSharedPreferences("ShriHanumanTemple", 0);
        this.mblnDisableShanknad = settings.getBoolean("Key_DisableShankhnad", false);
        this.mblnDisableStartupBell = settings.getBoolean("Key_DisableStartupBell", false);
        this.mblnDisableBellVibration = settings.getBoolean("Key_DisableBellVibration", false);
        this.mblnEnableAudioRepeat = settings.getBoolean("Key_EnableAudioRepeat", false);
        this.mblnTurnShuffleOn = settings.getBoolean("Key_EnableShuffleOn", false);
        this.mblnRepeatContinous = settings.getBoolean("Key_RepeatContinous", false);
        this.mintAudioRepeatCount = settings.getInt("Key_AudioRepeatCount", 1);
        this.mblnDisableNotoficationrAds = settings.getBoolean("Key_DisableNotificationAds", false);
    }

    private class BellManager {
        /* access modifiers changed from: private */
        public MediaPlayer mMediaPlayerBellL = null;
        /* access modifiers changed from: private */
        public MediaPlayer mMediaPlayerBellR = null;
        /* access modifiers changed from: private */
        public MediaPlayer mMediaPlayerShankh = null;
        long[] pattern;
        Vibrator v;

        public BellManager() {
            this.v = (Vibrator) TempleMainActivity.this.getSystemService("vibrator");
            long[] jArr = new long[2];
            jArr[1] = 100;
            this.pattern = jArr;
        }

        /* access modifiers changed from: private */
        public void vibrate() {
            if (!TempleMainActivity.this.mblnDisableBellVibration) {
                this.v.vibrate(this.pattern, -1);
            }
        }

        public void PlayShankh(boolean blnLoud) {
            if (this.mMediaPlayerShankh != null) {
                this.mMediaPlayerShankh.release();
            }
            this.mMediaPlayerShankh = MediaPlayer.create(TempleMainActivity.this.getApplicationContext(), (int) R.raw.shankh);
            if (this.mMediaPlayerShankh != null) {
                this.mMediaPlayerShankh.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    public void onCompletion(MediaPlayer mp) {
                        BellManager.this.mMediaPlayerShankh.release();
                        BellManager.this.mMediaPlayerShankh = null;
                    }
                });
                if (blnLoud) {
                    this.mMediaPlayerShankh.setVolume(1.0f, 1.0f);
                } else {
                    this.mMediaPlayerShankh.setVolume(0.5f, 0.5f);
                }
                this.mMediaPlayerShankh.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    public void onPrepared(MediaPlayer mp) {
                        BellManager.this.mMediaPlayerShankh.start();
                    }
                });
                try {
                    this.mMediaPlayerShankh.prepare();
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
            }
        }

        public void StopShankh() {
            if (this.mMediaPlayerShankh != null) {
                this.mMediaPlayerShankh.release();
                this.mMediaPlayerShankh = null;
            }
        }

        public void PlayRightBell(boolean flagBothSpeaker) {
            if (this.mMediaPlayerBellR != null) {
                this.mMediaPlayerBellR.release();
            }
            this.mMediaPlayerBellR = MediaPlayer.create(TempleMainActivity.this.getApplicationContext(), (int) R.raw.bell);
            if (this.mMediaPlayerBellR != null) {
                this.mMediaPlayerBellR.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    public void onCompletion(MediaPlayer mp) {
                        BellManager.this.mMediaPlayerBellR.release();
                        BellManager.this.mMediaPlayerBellR = null;
                    }
                });
                if (flagBothSpeaker) {
                    this.mMediaPlayerBellR.setVolume(0.4f, 0.4f);
                } else {
                    this.mMediaPlayerBellR.setVolume(1.0f, 0.0f);
                }
                this.mMediaPlayerBellR.start();
            }
        }

        public void PlayLeftBell(boolean flagBothSpeaker) {
            if (this.mMediaPlayerBellL != null) {
                this.mMediaPlayerBellL.release();
            }
            this.mMediaPlayerBellL = MediaPlayer.create(TempleMainActivity.this.getApplicationContext(), (int) R.raw.bell);
            if (this.mMediaPlayerBellL != null) {
                this.mMediaPlayerBellL.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    public void onCompletion(MediaPlayer mp) {
                        BellManager.this.mMediaPlayerBellL.release();
                        BellManager.this.mMediaPlayerBellL = null;
                    }
                });
                if (flagBothSpeaker) {
                    this.mMediaPlayerBellL.setVolume(0.4f, 0.4f);
                } else {
                    this.mMediaPlayerBellL.setVolume(0.0f, 1.0f);
                }
                this.mMediaPlayerBellL.start();
            }
        }

        public void Destroy() {
            if (this.mMediaPlayerBellR != null) {
                this.mMediaPlayerBellR.release();
                this.mMediaPlayerBellR = null;
            }
            if (this.mMediaPlayerBellL != null) {
                this.mMediaPlayerBellL.release();
                this.mMediaPlayerBellL = null;
            }
            if (this.mMediaPlayerShankh != null) {
                this.mMediaPlayerShankh.release();
                this.mMediaPlayerShankh = null;
            }
        }
    }
}
