package bys.widgets.srihanuman;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;
import bys.customviews.adview.BYSAdView;

public class AboutActivity extends Activity {
    BYSAdView adView;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.about);
        WebView webView = (WebView) findViewById(R.id.WebView01);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.loadUrl("file:///android_asset/about.html");
        this.adView = (BYSAdView) findViewById(R.id.ad2);
        this.adView.LoadAd(this, 2, 1000);
    }
}
