package bys.widgets.srihanuman;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.PowerManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import bys.libs.utils.ringtonemanager.RingToneManager;
import java.util.Calendar;
import org.codehaus.jackson.util.MinimalPrettyPrinter;

public class SettingsActivity extends Activity {
    private static final int DIALOG_TIME_PICK = 1;
    private static final String mstrAppIdentifier = "ShriHanumanTemple";
    AlarmManager am = null;
    CheckBox chkBellAsRingTone = null;
    CheckBox chkDisableBannerAds = null;
    CheckBox chkDisableBellVibration = null;
    CheckBox chkDisableNotificationAds = null;
    CheckBox chkDisableShankhnad = null;
    CheckBox chkDisableStartupBell = null;
    CheckBox chkEnableAlarm = null;
    CheckBox chkEnableRepeat = null;
    CheckBox chkShankhAsRingTone = null;
    CheckBox chkTurnShuffleOn = null;
    EditText edtRepeatCount = null;
    boolean mblnDisableBannerAds = false;
    boolean mblnDisableBellVibration = false;
    boolean mblnDisableNotificationAds = false;
    boolean mblnDisableShanknad = false;
    boolean mblnDisableStartupBell = false;
    boolean mblnEnableAudioRepeat = false;
    boolean mblnRepeatContinous = false;
    boolean mblnTurnShiffleOn = false;
    int mintAudioRepeatCount = 1;
    int mintSavedAlarmHourTime;
    int mintSavedAlarmMinuteTime;
    PendingIntent pi = null;
    PowerManager pm = null;
    RadioButton rdbRepeatContinuosly = null;
    RadioButton rdbRepeatNTimes = null;
    RingToneManager rtManager = null;
    TextView txtTime = null;

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 1:
                return new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        SettingsActivity.this.mintSavedAlarmHourTime = hourOfDay;
                        SettingsActivity.this.mintSavedAlarmMinuteTime = minute;
                        SettingsActivity.this.SetUserSpecifiedAlarmTime();
                    }
                }, this.mintSavedAlarmHourTime, this.mintSavedAlarmMinuteTime, false);
            default:
                return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        try {
            this.mintAudioRepeatCount = new Integer(this.edtRepeatCount.getText().toString()).intValue();
        } catch (Exception e) {
            this.mintAudioRepeatCount = 1;
            e.printStackTrace();
        }
        SavePreference();
        super.onDestroy();
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(1024, 1024);
        getWindow().setSoftInputMode(3);
        setContentView((int) R.layout.settings_layout);
        this.rtManager = new RingToneManager(getApplicationContext());
        this.am = (AlarmManager) getSystemService("alarm");
        this.pi = PendingIntent.getBroadcast(getApplicationContext(), 0, new Intent("INTENT_ShriHanumanTemple"), 0);
        this.chkEnableAlarm = (CheckBox) findViewById(R.id.chkEnableAlarm);
        this.chkEnableRepeat = (CheckBox) findViewById(R.id.chkEnableRepeat);
        this.chkDisableShankhnad = (CheckBox) findViewById(R.id.chkDisableShankhnad);
        this.chkDisableStartupBell = (CheckBox) findViewById(R.id.chkDisableStartupBell);
        this.chkDisableBellVibration = (CheckBox) findViewById(R.id.chkDisableBellVibration);
        this.chkTurnShuffleOn = (CheckBox) findViewById(R.id.chkTurnShuffleOn);
        this.edtRepeatCount = (EditText) findViewById(R.id.edtRepeatCount);
        this.rdbRepeatContinuosly = (RadioButton) findViewById(R.id.rdbRepeatContinuosly);
        this.rdbRepeatNTimes = (RadioButton) findViewById(R.id.rdbRepeatNTimes);
        this.chkBellAsRingTone = (CheckBox) findViewById(R.id.chkBellAsRingtone);
        this.chkDisableNotificationAds = (CheckBox) findViewById(R.id.chkDisableNotificationAds);
        this.chkDisableBannerAds = (CheckBox) findViewById(R.id.chkDisableBannerAds);
        this.chkShankhAsRingTone = (CheckBox) findViewById(R.id.chkShankhAsRingtone);
        ReadPreference();
        ApplyPreferenceOnUI();
        this.chkDisableNotificationAds.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SettingsActivity.this.mblnDisableNotificationAds = SettingsActivity.this.chkDisableNotificationAds.isChecked();
            }
        });
        this.chkDisableBannerAds.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SettingsActivity.this.mblnDisableBannerAds = SettingsActivity.this.chkDisableBannerAds.isChecked();
            }
        });
        this.chkTurnShuffleOn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SettingsActivity.this.mblnTurnShiffleOn = SettingsActivity.this.chkTurnShuffleOn.isChecked();
            }
        });
        this.chkBellAsRingTone.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                boolean isChecked = SettingsActivity.this.chkBellAsRingTone.isChecked();
                Log.v("chkBellAsRingTone onClick", "flagChecked : " + isChecked);
                SettingsActivity.this.rtManager.setBellAsRingTone(isChecked);
                SettingsActivity.this.ApplyRingtoneSetStatus();
            }
        });
        this.chkShankhAsRingTone.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                boolean isChecked = SettingsActivity.this.chkShankhAsRingTone.isChecked();
                Log.v("chkShankhAsRingTone onClick", "flagChecked : " + isChecked);
                SettingsActivity.this.rtManager.setShankhAsRingTone(isChecked);
                SettingsActivity.this.ApplyRingtoneSetStatus();
            }
        });
        this.chkEnableRepeat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SettingsActivity.this.mblnEnableAudioRepeat = isChecked;
                if (!isChecked) {
                    SettingsActivity.this.rdbRepeatContinuosly.setChecked(false);
                    SettingsActivity.this.rdbRepeatNTimes.setChecked(false);
                    SettingsActivity.this.rdbRepeatContinuosly.setEnabled(false);
                    SettingsActivity.this.rdbRepeatNTimes.setEnabled(false);
                    SettingsActivity.this.edtRepeatCount.setEnabled(false);
                    return;
                }
                SettingsActivity.this.rdbRepeatContinuosly.setEnabled(true);
                SettingsActivity.this.rdbRepeatNTimes.setEnabled(true);
                if (SettingsActivity.this.mblnRepeatContinous) {
                    SettingsActivity.this.rdbRepeatContinuosly.setChecked(true);
                    SettingsActivity.this.rdbRepeatNTimes.setChecked(false);
                    SettingsActivity.this.edtRepeatCount.setEnabled(false);
                    return;
                }
                SettingsActivity.this.rdbRepeatContinuosly.setChecked(false);
                SettingsActivity.this.rdbRepeatNTimes.setChecked(true);
                SettingsActivity.this.edtRepeatCount.setEnabled(true);
            }
        });
        this.chkDisableShankhnad.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SettingsActivity.this.mblnDisableShanknad = isChecked;
            }
        });
        this.chkDisableStartupBell.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SettingsActivity.this.mblnDisableStartupBell = isChecked;
            }
        });
        this.chkDisableBellVibration.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SettingsActivity.this.mblnDisableBellVibration = isChecked;
            }
        });
        this.rdbRepeatContinuosly.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SettingsActivity.this.mblnRepeatContinous = true;
                SettingsActivity.this.rdbRepeatNTimes.setChecked(false);
                SettingsActivity.this.edtRepeatCount.setEnabled(false);
            }
        });
        this.rdbRepeatNTimes.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SettingsActivity.this.mblnRepeatContinous = false;
                SettingsActivity.this.rdbRepeatContinuosly.setChecked(false);
                SettingsActivity.this.edtRepeatCount.setEnabled(true);
            }
        });
        this.txtTime = (TextView) findViewById(R.id.txtAlarmTriggerTime);
        GetSavedAlarmTime();
        DisplayAlarmTime();
        ((Button) findViewById(R.id.btnChangeAlarmTriggerTime)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SettingsActivity.this.showDialog(1);
            }
        });
        this.chkEnableAlarm.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SettingsActivity.this.EnableDisableAlarm();
                SettingsActivity.this.SaveAlarmTime();
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void SetUserSpecifiedAlarmTime() {
        SaveAlarmTime();
        DisplayAlarmTime();
        EnableDisableAlarm();
    }

    /* access modifiers changed from: package-private */
    public String GetAlarmTimeDisplayString() {
        String lstrMinute;
        String lstrHour = new StringBuilder().append(this.mintSavedAlarmHourTime > 11 ? this.mintSavedAlarmHourTime - 12 : this.mintSavedAlarmHourTime).toString();
        if (this.mintSavedAlarmMinuteTime > 9) {
            lstrMinute = new StringBuilder().append(this.mintSavedAlarmMinuteTime).toString();
        } else {
            lstrMinute = "0" + this.mintSavedAlarmMinuteTime;
        }
        return String.valueOf(lstrHour) + ":" + lstrMinute + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + ((this.mintSavedAlarmHourTime * 60) + this.mintSavedAlarmMinuteTime < 720 ? "AM" : "PM");
    }

    /* access modifiers changed from: package-private */
    public void DisplayAlarmTime() {
        this.txtTime.setText(GetAlarmTimeDisplayString());
    }

    /* access modifiers changed from: package-private */
    public void EnableDisableAlarm() {
        Calendar calAlarmTrigger = Calendar.getInstance();
        if (this.chkEnableAlarm.isChecked()) {
            Calendar calNow = Calendar.getInstance();
            if ((calNow.get(11) * 60) + calNow.get(12) > (this.mintSavedAlarmHourTime * 60) + this.mintSavedAlarmMinuteTime) {
                calAlarmTrigger.set(calNow.get(1), calNow.get(2), calNow.get(5) + 1, this.mintSavedAlarmHourTime, this.mintSavedAlarmMinuteTime);
            } else {
                calAlarmTrigger.set(calNow.get(1), calNow.get(2), calNow.get(5), this.mintSavedAlarmHourTime, this.mintSavedAlarmMinuteTime);
            }
            Log.v("ShriHanumanTemple:EnableDisableAlarm", "Activating Alarm " + calAlarmTrigger.toString());
            this.am.cancel(this.pi);
            this.am.setRepeating(0, calAlarmTrigger.getTimeInMillis(), 86400000, this.pi);
            Toast.makeText(getApplicationContext(), "Alarm set @ " + GetAlarmTimeDisplayString(), 1).show();
            return;
        }
        Log.v("ShriHanumanTemple:EnableDisableAlarm", "Disabling Alarm ");
        this.am.cancel(this.pi);
    }

    private void ApplyPreferenceOnUI() {
        this.chkEnableRepeat.setChecked(this.mblnEnableAudioRepeat);
        this.chkDisableShankhnad.setChecked(this.mblnDisableShanknad);
        this.chkDisableStartupBell.setChecked(this.mblnDisableStartupBell);
        this.chkDisableBellVibration.setChecked(this.mblnDisableBellVibration);
        this.chkTurnShuffleOn.setChecked(this.mblnTurnShiffleOn);
        this.chkDisableBannerAds.setChecked(this.mblnDisableBannerAds);
        this.chkDisableNotificationAds.setChecked(this.mblnDisableNotificationAds);
        this.edtRepeatCount.setText(new StringBuilder().append(this.mintAudioRepeatCount).toString());
        if (this.mblnEnableAudioRepeat) {
            this.rdbRepeatContinuosly.setEnabled(true);
            this.rdbRepeatNTimes.setEnabled(true);
            if (this.mblnRepeatContinous) {
                this.rdbRepeatContinuosly.setChecked(true);
                this.rdbRepeatNTimes.setChecked(false);
                this.edtRepeatCount.setEnabled(false);
            } else {
                this.rdbRepeatContinuosly.setChecked(false);
                this.rdbRepeatNTimes.setChecked(true);
                this.edtRepeatCount.setEnabled(true);
            }
        } else {
            this.rdbRepeatContinuosly.setChecked(false);
            this.rdbRepeatNTimes.setChecked(false);
            this.rdbRepeatContinuosly.setEnabled(false);
            this.rdbRepeatNTimes.setEnabled(false);
            this.edtRepeatCount.setEnabled(false);
        }
        ApplyRingtoneSetStatus();
    }

    /* access modifiers changed from: private */
    public void ApplyRingtoneSetStatus() {
        this.chkBellAsRingTone.setChecked(this.rtManager.isBellSetAsRingTone());
        this.chkShankhAsRingTone.setChecked(this.rtManager.isShankhSetAsRingTone());
    }

    private void ReadPreference() {
        SharedPreferences settings = getSharedPreferences("ShriHanumanTemple", 0);
        this.mblnDisableShanknad = settings.getBoolean("Key_DisableShankhnad", false);
        this.mblnDisableStartupBell = settings.getBoolean("Key_DisableStartupBell", false);
        this.mblnDisableBellVibration = settings.getBoolean("Key_DisableBellVibration", false);
        this.mblnTurnShiffleOn = settings.getBoolean("Key_EnableShuffleOn", false);
        this.mblnDisableBannerAds = settings.getBoolean("Key_DisableBannerAds", false);
        this.mblnDisableNotificationAds = settings.getBoolean("Key_DisableNotificationAds", false);
        this.mblnEnableAudioRepeat = settings.getBoolean("Key_EnableAudioRepeat", false);
        this.mblnRepeatContinous = settings.getBoolean("Key_RepeatContinous", false);
        this.mintAudioRepeatCount = settings.getInt("Key_AudioRepeatCount", 1);
    }

    private void SavePreference() {
        SharedPreferences.Editor editor = getSharedPreferences("ShriHanumanTemple", 0).edit();
        editor.putBoolean("Key_DisableShankhnad", this.mblnDisableShanknad);
        editor.putBoolean("Key_DisableStartupBell", this.mblnDisableStartupBell);
        editor.putBoolean("Key_DisableBellVibration", this.mblnDisableBellVibration);
        editor.putBoolean("Key_EnableShuffleOn", this.mblnTurnShiffleOn);
        editor.putBoolean("Key_EnableAudioRepeat", this.mblnEnableAudioRepeat);
        editor.putBoolean("Key_RepeatContinous", this.mblnRepeatContinous);
        editor.putInt("Key_AudioRepeatCount", this.mintAudioRepeatCount);
        editor.putBoolean("Key_DisableBannerAds", this.mblnDisableBannerAds);
        editor.putBoolean("Key_DisableNotificationAds", this.mblnDisableNotificationAds);
        editor.commit();
    }

    private void GetSavedAlarmTime() {
        SharedPreferences settings = getSharedPreferences("ShriHanumanTemple", 0);
        this.mintSavedAlarmHourTime = settings.getInt("WakeUpHourTime", 6);
        this.mintSavedAlarmMinuteTime = settings.getInt("WakeUpMinuteTime", 0);
        this.chkEnableAlarm.setChecked(settings.getBoolean("AlarmEnabled", false));
    }

    /* access modifiers changed from: private */
    public void SaveAlarmTime() {
        SharedPreferences.Editor editor = getSharedPreferences("ShriHanumanTemple", 0).edit();
        editor.putInt("WakeUpHourTime", this.mintSavedAlarmHourTime);
        editor.putInt("WakeUpMinuteTime", this.mintSavedAlarmMinuteTime);
        editor.putBoolean("AlarmEnabled", this.chkEnableAlarm.isChecked());
        editor.commit();
    }
}
