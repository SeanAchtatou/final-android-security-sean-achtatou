package bys.libs.customuiwidgets.downloaderview;

import android.content.Context;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class CustomDownloaderService {
    static final int mintThreadCount = 4;
    FragmentDownloader[] fragmentDownloader = new FragmentDownloader[4];
    Handler mHandler = new Handler();
    /* access modifiers changed from: private */
    public URL mUrl = null;
    Runnable m_taskFileDownloader = null;
    Runnable m_taskRetryDownloading = null;
    boolean mblnDownloading = false;
    File mdirDownloadDir = null;
    int mintFragmentCompletedCount = 0;
    int mintFragmentFailedCount = 0;
    int mintRetryCount = 0;
    long mintStartMiliSecond = 0;
    int mintTotalFileSize = -1;
    private String mstrFileUrl;
    /* access modifiers changed from: private */
    public String mstrLocalFileName;
    DownloadViewListeners onDownloadViewListener = null;

    private interface FragmentDownloaderListener {
        void OnConnectionFailure(int i);

        void OnDownloadDone(int i);

        void OnFatalError(int i);
    }

    public CustomDownloaderService(Context context) {
    }

    public void setOnDownloadDoneListener(DownloadViewListeners listener) {
        this.onDownloadViewListener = listener;
    }

    public void stopDownload() {
        for (int i = 0; i < this.fragmentDownloader.length; i++) {
            Log.v("File Downloader View", "StopDownload " + this.fragmentDownloader.length);
            if (this.fragmentDownloader[i] != null) {
                this.fragmentDownloader[i].AbortDownnload();
            }
        }
        this.mHandler.removeCallbacks(this.m_taskFileDownloader);
        this.mHandler.removeCallbacks(this.m_taskRetryDownloading);
    }

    public void setLocalFile(File dirAppDirectory, String strLocalFileName) {
        this.mstrLocalFileName = strLocalFileName;
        Log.i("Local filename:", this.mstrLocalFileName);
        this.mdirDownloadDir = dirAppDirectory;
    }

    public boolean isDownloading() {
        return this.mblnDownloading;
    }

    public void startDownload(String strFileUrl) {
        this.mstrFileUrl = strFileUrl;
        this.mblnDownloading = true;
        this.m_taskFileDownloader = new Runnable() {
            public void run() {
                CustomDownloaderService.this.mintStartMiliSecond = SystemClock.uptimeMillis();
                CustomDownloaderService.this.StartFragmnetsDownload();
            }
        };
        new Thread(this.m_taskFileDownloader).start();
    }

    /* access modifiers changed from: private */
    public HttpURLConnection EstablishUrlConnection(URL url, long intStartOffset, long intEndOffset) {
        try {
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            try {
                urlConnection.setRequestMethod("GET");
                urlConnection.setDoOutput(true);
                if (intStartOffset >= 0 && intEndOffset > 0) {
                    urlConnection.setRequestProperty("Range", "bytes=" + intStartOffset + "-" + intEndOffset);
                } else if (intStartOffset > 0 && intEndOffset == 0) {
                    urlConnection.setRequestProperty("Range", "bytes=" + intStartOffset + "-");
                }
                try {
                    urlConnection.setConnectTimeout(30000);
                    urlConnection.connect();
                    return urlConnection;
                } catch (IOException e) {
                    Log.e("", "Failed to Connect: StartOffset = " + intStartOffset + ", End Offset" + intEndOffset);
                    e.printStackTrace();
                    return null;
                }
            } catch (ProtocolException e2) {
                Log.e("", "Failed to set Get Request StartOffset = " + intStartOffset + "End Offset" + intEndOffset);
                e2.printStackTrace();
                return null;
            }
        } catch (IOException e3) {
            Log.e("", "Failed to open Connection StartOffset = " + intStartOffset + "End Offset = " + intEndOffset);
            e3.printStackTrace();
            return null;
        }
    }

    private long showFragmentsSize() {
        for (int i = 0; i < 4; i++) {
            if (this.fragmentDownloader[i] != null) {
                Log.v("", "Fragment (" + i + ") Size = " + this.fragmentDownloader[i].getFragmentCurrentSize());
            }
        }
        return (long) 0;
    }

    private void TryDownloadingAgain() {
        this.mblnDownloading = true;
        this.m_taskRetryDownloading = new Runnable() {
            public void run() {
                CustomDownloaderService.this.StartFragmnetsDownload();
            }
        };
        this.mHandler.postAtTime(this.m_taskRetryDownloading, SystemClock.uptimeMillis() + 10000);
    }

    /* access modifiers changed from: private */
    public void ActionAfterFragmentsReady(int intThreadIndex) {
        Log.i("ActionAfterFragmentsReady", "Download Done (" + intThreadIndex + ") :  " + "mintDownloadedSize = " + this.fragmentDownloader[intThreadIndex].getFragmentCurrentSize() + ", mintFragmentCompletedCount = " + this.mintFragmentCompletedCount + ", mintFragmentfailedCount = " + this.mintFragmentFailedCount + ", mintTotalFileSize = " + this.mintTotalFileSize);
        if (this.mintFragmentCompletedCount + this.mintFragmentFailedCount == 4) {
            this.mblnDownloading = false;
            if (this.mintFragmentFailedCount <= 0) {
                String strConcatenatedFileName = String.valueOf(this.mdirDownloadDir.getAbsolutePath()) + "/" + this.mstrLocalFileName;
                ByteBuffer[] buffers = new ByteBuffer[4];
                int i = 0;
                while (i < 4) {
                    try {
                        RandomAccessFile raf = new RandomAccessFile(String.valueOf(this.mdirDownloadDir.getAbsolutePath()) + "/" + this.mstrLocalFileName.replace(".mp3", ".frag") + i, "r");
                        try {
                            FileChannel channel = raf.getChannel();
                            buffers[i] = channel.map(FileChannel.MapMode.READ_ONLY, 0, raf.length());
                            channel.close();
                            i++;
                        } catch (IOException e) {
                            e.printStackTrace();
                            return;
                        }
                    } catch (FileNotFoundException e2) {
                        e2.printStackTrace();
                        return;
                    }
                }
                try {
                    FileChannel out = new FileOutputStream(strConcatenatedFileName).getChannel();
                    Log.i("", "Concatenating... ");
                    try {
                        out.write(buffers);
                        try {
                            out.close();
                            int i2 = 0;
                            while (i2 < 4) {
                                try {
                                    new File(String.valueOf(this.mdirDownloadDir.getAbsolutePath()) + "/" + this.mstrLocalFileName.replace(".mp3", ".frag") + i2).delete();
                                    i2++;
                                } catch (Exception e3) {
                                    e3.printStackTrace();
                                    return;
                                }
                            }
                            if (getCurrentDownlaodedSize() == this.mintTotalFileSize) {
                                this.onDownloadViewListener.onDownloadDone(strConcatenatedFileName);
                                Log.i("", "Download Done = Downlaod Time : " + (SystemClock.uptimeMillis() - this.mintStartMiliSecond));
                                return;
                            }
                            Log.i("", "There is error in downloaded file. Deleteing..  " + strConcatenatedFileName);
                            Log.i("Failed fragments", "Failed fragment Count =  " + this.mintFragmentFailedCount + ", Completed  fragment Count  =  " + this.mintFragmentCompletedCount);
                            showFragmentsSize();
                            new File(strConcatenatedFileName).delete();
                        } catch (IOException e4) {
                            e4.printStackTrace();
                        }
                    } catch (IOException e5) {
                        e5.printStackTrace();
                    }
                } catch (FileNotFoundException e6) {
                    e6.printStackTrace();
                }
            } else if (this.mintRetryCount < 10) {
                this.mintRetryCount = this.mintRetryCount + 1;
                TryDownloadingAgain();
            } else {
                this.onDownloadViewListener.onError("Error while downloading. Try again...");
            }
        }
    }

    private boolean startDownloadSetup() {
        try {
            this.onDownloadViewListener.onUpdateStatus("Connecting...");
            this.mUrl = new URL(this.mstrFileUrl);
            HttpURLConnection urlConnection = EstablishUrlConnection(this.mUrl, 0, 0);
            this.mintTotalFileSize = -1;
            if (urlConnection != null) {
                this.mintTotalFileSize = urlConnection.getContentLength();
                if (this.mintTotalFileSize == -1) {
                    this.onDownloadViewListener.onError("Server busy... Try Later");
                    return false;
                } else if (this.mintTotalFileSize < 100000) {
                    this.onDownloadViewListener.onError("File not accessible");
                    return false;
                } else {
                    urlConnection.disconnect();
                    if (this.onDownloadViewListener != null) {
                        this.onDownloadViewListener.onDownloadStarted(this.mintTotalFileSize);
                    }
                    Log.i("", "Total Download Size = " + this.mintTotalFileSize);
                    return true;
                }
            } else {
                this.onDownloadViewListener.onError("Failed to connect. Try Again");
                return false;
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
            if (this.onDownloadViewListener != null) {
                this.onDownloadViewListener.onError("Invalid URL");
            }
            return false;
        }
    }

    /* access modifiers changed from: private */
    public void StartFragmnetsDownload() {
        int lngEndOffset;
        this.mintFragmentCompletedCount = 0;
        this.mintFragmentFailedCount = 0;
        this.mintRetryCount = 0;
        if (!startDownloadSetup()) {
            this.mblnDownloading = false;
            return;
        }
        int lngFragmentedFileSize = this.mintTotalFileSize / 4;
        Log.v("", "Fragement Size = " + lngFragmentedFileSize + "*" + 3 + " and " + (this.mintTotalFileSize - (lngFragmentedFileSize * 3)));
        for (int intThreadIndex = 0; intThreadIndex < 4; intThreadIndex++) {
            int lngFragmentedFileCheckSize = lngFragmentedFileSize;
            if (intThreadIndex == 3) {
                lngFragmentedFileCheckSize = this.mintTotalFileSize - (lngFragmentedFileSize * 3);
                lngEndOffset = 0;
            } else {
                lngEndOffset = ((intThreadIndex + 1) * lngFragmentedFileSize) - 1;
            }
            this.fragmentDownloader[intThreadIndex] = new FragmentDownloader(intThreadIndex, (long) (intThreadIndex * lngFragmentedFileSize), (long) lngEndOffset);
            this.fragmentDownloader[intThreadIndex].setCalculatedFragmentSize(lngFragmentedFileCheckSize);
            this.fragmentDownloader[intThreadIndex].setDownloadListener(new FragmentDownloaderListener() {
                public void OnDownloadDone(int intThreadIndex) {
                    CustomDownloaderService.this.mintFragmentCompletedCount++;
                    CustomDownloaderService.this.ActionAfterFragmentsReady(intThreadIndex);
                }

                public void OnConnectionFailure(int intThreadIndex) {
                    CustomDownloaderService.this.mintFragmentFailedCount++;
                    if (CustomDownloaderService.this.mintFragmentCompletedCount + CustomDownloaderService.this.mintFragmentFailedCount == 4) {
                        Log.e("OnConnectionFailure", "Error while downloading. Trying again...");
                        CustomDownloaderService.this.fragmentDownloader[intThreadIndex].StartDownnload();
                    }
                }

                public void OnFatalError(int intThreadIndex) {
                    CustomDownloaderService.this.mintFragmentFailedCount++;
                    CustomDownloaderService.this.ActionAfterFragmentsReady(intThreadIndex);
                    Log.e("OnFatalError", "Error while downloading. Trying again...");
                }
            });
            Log.i("", "Starting Fragment  " + intThreadIndex + " download");
            this.fragmentDownloader[intThreadIndex].StartDownnload();
        }
    }

    private class FragmentDownloader {
        private boolean blnIsDownloading = false;
        boolean blnStop = false;
        FragmentDownloaderListener downloadListener = null;
        File file = null;
        FileOutputStream fileOutput = null;
        InputStream inputStream = null;
        long intEndOffset = 0;
        long intStartOffset = 0;
        int intThreadIndex = 0;
        Integer lngFragmentCurrentSize = 0;
        int lngFragmentPredownloadedSize = 0;
        int lngFragmentTotalSize = 0;
        Runnable m_taskFileFragmentDownloader = null;
        HttpURLConnection urlConnection = null;

        public int getFragmentCurrentSize() {
            int intValue;
            synchronized (this.lngFragmentCurrentSize) {
                intValue = this.lngFragmentCurrentSize.intValue() + this.lngFragmentPredownloadedSize;
            }
            return intValue;
        }

        public void setCalculatedFragmentSize(int lngFragmentedFileCheckSize) {
            this.lngFragmentTotalSize = lngFragmentedFileCheckSize;
        }

        private void addToFragmentCurrentSize(int alngFragmentCurrentSize) {
            synchronized (this.lngFragmentCurrentSize) {
                this.lngFragmentCurrentSize = Integer.valueOf(this.lngFragmentCurrentSize.intValue() + alngFragmentCurrentSize);
            }
        }

        public void setDownloadListener(FragmentDownloaderListener downloadListener2) {
            this.downloadListener = downloadListener2;
        }

        public FragmentDownloader(int aintThreadIndex, long aintStartOffset, long aintEndOffset) {
            this.intThreadIndex = aintThreadIndex;
            this.intStartOffset = aintStartOffset;
            this.intEndOffset = aintEndOffset;
            this.lngFragmentCurrentSize = 0;
            this.lngFragmentPredownloadedSize = 0;
            this.file = new File(CustomDownloaderService.this.mdirDownloadDir, String.valueOf(CustomDownloaderService.this.mstrLocalFileName.replace(".mp3", ".frag")) + this.intThreadIndex);
            if (this.file.exists()) {
                this.lngFragmentPredownloadedSize = (int) this.file.length();
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
         arg types: [java.io.File, int]
         candidates:
          ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
          ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
        public void StartDownnload() {
            if (this.lngFragmentTotalSize < this.lngFragmentPredownloadedSize) {
                Log.e("Fatal Error: 1", " Fragment(" + this.intThreadIndex + ") " + "Something wrong with the download. Deleting this fragment");
                this.file.delete();
                this.downloadListener.OnFatalError(this.intThreadIndex);
            } else if (this.lngFragmentTotalSize == this.lngFragmentPredownloadedSize) {
                Log.v("Fragment already downloaded", " Fragment(" + this.intThreadIndex + ") " + "Fragment already downloaded");
                this.downloadListener.OnDownloadDone(this.intThreadIndex);
            } else {
                this.urlConnection = CustomDownloaderService.this.EstablishUrlConnection(CustomDownloaderService.this.mUrl, this.intStartOffset + ((long) this.lngFragmentPredownloadedSize), this.intEndOffset);
                if (this.urlConnection == null) {
                    this.downloadListener.OnConnectionFailure(this.intThreadIndex);
                } else if (this.lngFragmentTotalSize != this.lngFragmentPredownloadedSize + this.urlConnection.getContentLength()) {
                    Log.v("Fatal Error: 2", " Fragment(" + this.intThreadIndex + ") " + "Something wrong with the download. Deleting this fragment");
                    this.file.delete();
                    this.downloadListener.OnFatalError(this.intThreadIndex);
                } else {
                    try {
                        this.inputStream = this.urlConnection.getInputStream();
                        try {
                            this.fileOutput = new FileOutputStream(this.file, true);
                            this.m_taskFileFragmentDownloader = new Runnable() {
                                public void run() {
                                    Log.v("Fragment Downloader ", "Calling.. StartDownloadThread " + FragmentDownloader.this.intThreadIndex);
                                    FragmentDownloader.this.StartDownloadThread();
                                }
                            };
                            new Thread(this.m_taskFileFragmentDownloader).start();
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                            this.downloadListener.OnConnectionFailure(this.intThreadIndex);
                        }
                    } catch (IOException e1) {
                        e1.printStackTrace();
                        this.downloadListener.OnConnectionFailure(this.intThreadIndex);
                    }
                }
            }
        }

        /* access modifiers changed from: private */
        public void StartDownloadThread() {
            this.blnStop = false;
            byte[] buffer = new byte[1024];
            if (this.downloadListener != null) {
                try {
                    this.blnIsDownloading = true;
                    while (true) {
                        int bufferLength = this.inputStream.read(buffer);
                        if (bufferLength <= 0 || this.blnStop) {
                            this.blnIsDownloading = false;
                        } else {
                            this.fileOutput.write(buffer, 0, bufferLength);
                            addToFragmentCurrentSize(bufferLength);
                        }
                    }
                    this.blnIsDownloading = false;
                    if (!this.blnStop) {
                        Log.v("Fragment Download done ", " Thread(" + this.intThreadIndex + ")" + this.lngFragmentCurrentSize);
                        if (this.lngFragmentTotalSize == this.lngFragmentCurrentSize.intValue() + this.lngFragmentPredownloadedSize) {
                            this.downloadListener.OnDownloadDone(this.intThreadIndex);
                        } else if (this.lngFragmentTotalSize < this.lngFragmentCurrentSize.intValue() + this.lngFragmentPredownloadedSize) {
                            Log.v("Incomplete Download ", " Fragment(" + this.intThreadIndex + ") " + "Download Stopped in middle");
                            this.downloadListener.OnConnectionFailure(this.intThreadIndex);
                        } else {
                            Log.v("Fatal Error: 3", " Fragment(" + this.intThreadIndex + ") " + "Something wrong with the download. Deleting this fragment");
                            this.file.delete();
                            this.downloadListener.OnFatalError(this.intThreadIndex);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    this.blnIsDownloading = false;
                }
            }
        }

        public void AbortDownnload() {
            Log.v("Fragment Downloader ", "StopDownload");
            this.blnStop = true;
        }
    }

    public int getPartiallyDownloaded(File fileAudio, int intTotalFileSize) {
        int intPartiallyDownloadedSize = 0;
        String strAbsoluteFilePath = fileAudio.getAbsolutePath();
        for (int i = 0; i < 4; i++) {
            File fileFragment = new File(String.valueOf(strAbsoluteFilePath.replace(".mp3", ".frag")) + i);
            if (fileFragment.exists()) {
                intPartiallyDownloadedSize = (int) (((long) intPartiallyDownloadedSize) + fileFragment.length());
            }
        }
        return intPartiallyDownloadedSize;
    }

    public String getFileName() {
        return this.mstrLocalFileName;
    }

    public int getTotalFileSize() {
        return this.mintTotalFileSize;
    }

    public int getCurrentDownlaodedSize() {
        int intDownloadedSize = 0;
        for (int i = 0; i < 4; i++) {
            if (this.fragmentDownloader[i] != null) {
                intDownloadedSize += this.fragmentDownloader[i].getFragmentCurrentSize();
            }
        }
        return intDownloadedSize;
    }
}
