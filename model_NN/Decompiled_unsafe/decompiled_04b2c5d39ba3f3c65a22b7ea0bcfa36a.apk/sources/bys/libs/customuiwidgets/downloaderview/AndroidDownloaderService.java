package bys.libs.customuiwidgets.downloaderview;

import android.app.DownloadManager;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import java.io.File;
import org.codehaus.jackson.impl.JsonWriteContext;
import org.codehaus.jackson.util.TokenBuffer;

public class AndroidDownloaderService {
    /* access modifiers changed from: private */
    public DownloadManager dm;
    DownloadManager.Query dmQuery = new DownloadManager.Query();
    private long downloadRequestId;
    private Context mContext;
    /* access modifiers changed from: private */
    public Handler mHandler = new Handler();
    private Runnable m_taskWatchForDownloadStarted = null;
    /* access modifiers changed from: private */
    public boolean mblnDownloadStopped = true;
    /* access modifiers changed from: private */
    public boolean mblnDownloading;
    File mdirDownloadDir = null;
    /* access modifiers changed from: private */
    public int mintTotalFileSize = 0;
    private String mstrLocalFileName;
    DownloadViewListeners onDownloadViewListener = null;

    public void startDownload(String strUrl) {
        this.mblnDownloadStopped = false;
        this.mblnDownloading = false;
        this.mintTotalFileSize = -1;
        this.dm = (DownloadManager) this.mContext.getSystemService("download");
        try {
            DownloadManager.Request request = new DownloadManager.Request(Uri.parse(strUrl));
            request.setTitle(this.mstrLocalFileName);
            request.setDestinationUri(Uri.fromFile(new File(this.mdirDownloadDir, this.mstrLocalFileName)));
            this.downloadRequestId = this.dm.enqueue(request);
            this.dmQuery.setFilterById(this.downloadRequestId);
            this.m_taskWatchForDownloadStarted = new Runnable() {
                public void run() {
                    Cursor cursor = AndroidDownloaderService.this.dm.query(AndroidDownloaderService.this.dmQuery);
                    cursor.moveToFirst();
                    if (AndroidDownloaderService.this.mintTotalFileSize < 0) {
                        AndroidDownloaderService.this.mintTotalFileSize = cursor.getInt(cursor.getColumnIndex("total_size"));
                        if (AndroidDownloaderService.this.mintTotalFileSize > 0) {
                            AndroidDownloaderService.this.onDownloadViewListener.onDownloadStarted(AndroidDownloaderService.this.mintTotalFileSize);
                            AndroidDownloaderService.this.mblnDownloading = true;
                            Log.v("", "onDownloadStarted " + AndroidDownloaderService.this.mintTotalFileSize);
                        }
                    }
                    switch (cursor.getInt(cursor.getColumnIndex("status"))) {
                        case 1:
                        case 2:
                        case 8:
                        default:
                            cursor.close();
                            if (AndroidDownloaderService.this.mintTotalFileSize == -1) {
                                AndroidDownloaderService.this.mHandler.postAtTime(this, SystemClock.uptimeMillis() + 500);
                                return;
                            }
                            return;
                        case JsonWriteContext.STATUS_EXPECT_VALUE /*4*/:
                            AndroidDownloaderService.this.mblnDownloading = false;
                            AndroidDownloaderService.this.mblnDownloadStopped = true;
                            return;
                        case TokenBuffer.Segment.TOKENS_PER_SEGMENT /*16*/:
                            AndroidDownloaderService.this.onDownloadViewListener.onUpdateStatus("Download Failed!!!");
                            AndroidDownloaderService.this.mblnDownloading = false;
                            AndroidDownloaderService.this.mblnDownloadStopped = true;
                            return;
                    }
                }
            };
            this.mHandler.postAtTime(this.m_taskWatchForDownloadStarted, SystemClock.uptimeMillis() + 500);
        } catch (Exception e) {
            this.onDownloadViewListener.onError("Failed to download");
            this.mblnDownloading = false;
            e.printStackTrace();
        }
    }

    public AndroidDownloaderService(Context context) {
        this.mContext = context;
    }

    public void setOnDownloadDoneListener(DownloadViewListeners listener) {
        this.onDownloadViewListener = listener;
    }

    public void setLocalFile(File dirAppDirectory, String strLocalFileName) {
        this.mdirDownloadDir = dirAppDirectory;
        this.mstrLocalFileName = strLocalFileName;
    }

    public boolean isDownloading() {
        return this.mblnDownloading;
    }

    public boolean isDownloadStopped() {
        return this.mblnDownloadStopped;
    }

    public void stopDownload() {
    }

    public String getFileName() {
        return this.mstrLocalFileName;
    }

    public int getCurrentDownlaodedSize() {
        Cursor cursor = this.dm.query(this.dmQuery);
        cursor.moveToFirst();
        int lngDownloadedSize = cursor.getInt(cursor.getColumnIndex("bytes_so_far"));
        switch (cursor.getInt(cursor.getColumnIndex("status"))) {
            case JsonWriteContext.STATUS_EXPECT_VALUE /*4*/:
                this.mblnDownloading = false;
                this.mblnDownloadStopped = true;
                break;
            case 8:
                this.onDownloadViewListener.onDownloadDone(this.mstrLocalFileName);
                this.mblnDownloading = false;
                this.mblnDownloadStopped = true;
                break;
            case TokenBuffer.Segment.TOKENS_PER_SEGMENT /*16*/:
                this.onDownloadViewListener.onError("Download interrupted !!!");
                this.mblnDownloading = false;
                this.mblnDownloadStopped = true;
                break;
        }
        cursor.close();
        return lngDownloadedSize;
    }

    public int getTotalFileSize() {
        return this.mintTotalFileSize;
    }
}
