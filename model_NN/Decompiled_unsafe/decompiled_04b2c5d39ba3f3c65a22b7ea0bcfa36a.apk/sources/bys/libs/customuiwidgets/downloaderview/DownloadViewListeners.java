package bys.libs.customuiwidgets.downloaderview;

public interface DownloadViewListeners {
    void OnProgressUpdate(int i);

    void onDownloadDone(String str);

    void onDownloadStarted(int i);

    void onError(String str);

    void onUpdateStatus(String str);
}
