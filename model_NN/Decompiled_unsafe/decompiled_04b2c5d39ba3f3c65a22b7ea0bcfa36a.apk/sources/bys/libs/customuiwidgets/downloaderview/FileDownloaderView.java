package bys.libs.customuiwidgets.downloaderview;

import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.WindowManager;
import android.widget.LinearLayout;
import bys.widgets.srihanuman.R;
import java.io.File;

public class FileDownloaderView extends LinearLayout {
    private AndroidDownloaderService ads;
    private CustomDownloaderService cds;
    private Display display;
    /* access modifiers changed from: private */
    public LinearLayout layoutProgress;
    Handler mHandler = new Handler();
    Runnable m_taskPrgressBarUpdater = null;
    private float mfltScale;
    private int mintMaxAllowedVersion = 8;
    private int mintSDKVersion = Integer.parseInt(Build.VERSION.SDK);
    private String mstrLocalFileName;
    DownloadViewListeners onDownloadViewListener = null;
    /* access modifiers changed from: private */
    public LinearLayout thisView;

    public FileDownloaderView(Context context, AttributeSet attrs) {
        super(context, attrs);
        ((LayoutInflater) context.getSystemService("layout_inflater")).inflate((int) R.layout.downloader_view_layout, this);
        this.layoutProgress = (LinearLayout) findViewById(R.id.layoutProgress);
        this.thisView = this;
        this.display = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
        this.mfltScale = context.getResources().getDisplayMetrics().density;
        if (this.mintSDKVersion > this.mintMaxAllowedVersion) {
            this.ads = new AndroidDownloaderService(context);
        } else {
            this.cds = new CustomDownloaderService(context);
        }
        this.m_taskPrgressBarUpdater = new Runnable() {
            public void run() {
                long lngDownloadedSize = (long) FileDownloaderView.this.getCurrentDownlaodedSize();
                long intTotalFileSize = (long) FileDownloaderView.this.getTotalFileSize();
                Log.i("", "Progress: " + lngDownloadedSize + " (" + intTotalFileSize + ")");
                LinearLayout.LayoutParams lparamsPot = (LinearLayout.LayoutParams) FileDownloaderView.this.layoutProgress.getLayoutParams();
                if (intTotalFileSize != 0 && !FileDownloaderView.this.IsDownloadStopped()) {
                    lparamsPot.width = (int) (((float) FileDownloaderView.this.thisView.getWidth()) * (((float) lngDownloadedSize) / ((float) intTotalFileSize)));
                    FileDownloaderView.this.layoutProgress.setLayoutParams(lparamsPot);
                    int intPercentProgress = (int) ((100 * lngDownloadedSize) / intTotalFileSize);
                    if (intPercentProgress >= 0 && intPercentProgress < 100) {
                        FileDownloaderView.this.onDownloadViewListener.OnProgressUpdate(intPercentProgress);
                    }
                }
                if (!FileDownloaderView.this.IsDownloadStopped()) {
                    FileDownloaderView.this.mHandler.postAtTime(this, SystemClock.uptimeMillis() + 750);
                }
            }
        };
    }

    public void setOnDownloadDoneListener(DownloadViewListeners listener) {
        this.onDownloadViewListener = listener;
        if (this.mintSDKVersion > this.mintMaxAllowedVersion) {
            this.ads.setOnDownloadDoneListener(listener);
        } else {
            this.cds.setOnDownloadDoneListener(listener);
        }
    }

    public void StopDownloading() {
        this.mHandler.removeCallbacks(this.m_taskPrgressBarUpdater);
        if (this.mintSDKVersion > this.mintMaxAllowedVersion) {
            this.ads.stopDownload();
        } else {
            this.cds.stopDownload();
        }
    }

    public void SetLocalFile(File dirAppDirectory, String strLocalFileName) {
        this.mstrLocalFileName = strLocalFileName;
        if (this.mintSDKVersion > this.mintMaxAllowedVersion) {
            this.ads.setLocalFile(dirAppDirectory, strLocalFileName);
        } else {
            this.cds.setLocalFile(dirAppDirectory, strLocalFileName);
        }
    }

    public boolean IsDownloading() {
        if (this.mintSDKVersion > this.mintMaxAllowedVersion) {
            return this.ads.isDownloading();
        }
        return this.cds.isDownloading();
    }

    public boolean IsDownloadStopped() {
        if (this.mintSDKVersion > this.mintMaxAllowedVersion) {
            return this.ads.isDownloadStopped();
        }
        return !this.cds.isDownloading();
    }

    public void StartDownloading(String strFileUrl) {
        if (this.mintSDKVersion > this.mintMaxAllowedVersion) {
            this.ads.startDownload(strFileUrl);
        } else {
            this.cds.startDownload(strFileUrl);
        }
        this.mHandler.postAtTime(this.m_taskPrgressBarUpdater, SystemClock.uptimeMillis() + 500);
    }

    public int checkIfPartiallyDownloaded(File fileAudio, int intTotalFileSize) {
        int intPartiallyDownloadedSize = this.cds.getPartiallyDownloaded(fileAudio, intTotalFileSize);
        LinearLayout.LayoutParams lparamsPot = (LinearLayout.LayoutParams) this.layoutProgress.getLayoutParams();
        if (intTotalFileSize != 0) {
            lparamsPot.width = (int) (((float) (this.display.getWidth() - Math.round(10.0f * this.mfltScale))) * (((float) intPartiallyDownloadedSize) / ((float) intTotalFileSize)));
            this.layoutProgress.setLayoutParams(lparamsPot);
        }
        return intPartiallyDownloadedSize;
    }

    public String getFileName() {
        return this.mstrLocalFileName;
    }

    /* access modifiers changed from: private */
    public int getTotalFileSize() {
        if (this.mintSDKVersion > this.mintMaxAllowedVersion) {
            return this.ads.getTotalFileSize();
        }
        return this.cds.getTotalFileSize();
    }

    /* access modifiers changed from: private */
    public int getCurrentDownlaodedSize() {
        if (this.mintSDKVersion > this.mintMaxAllowedVersion) {
            return this.ads.getCurrentDownlaodedSize();
        }
        return this.cds.getCurrentDownlaodedSize();
    }
}
