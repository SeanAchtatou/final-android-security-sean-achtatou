package bys.libs.utils.xmldownloader;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class XMLTableAdaptor {
    XMLTableReceiveListener itfcXMLTable = null;
    /* access modifiers changed from: private */
    public String[] mstrFields = null;
    /* access modifiers changed from: private */
    public String mstrObjectName = null;
    /* access modifiers changed from: private */
    public String strTableUrl = null;

    public void setOnTableReceiveListener(XMLTableReceiveListener onXMLTableReceiveListener) {
        this.itfcXMLTable = onXMLTableReceiveListener;
    }

    public void setExpectedFields(String strObjectName, String[] strFields) {
        this.mstrFields = strFields;
        this.mstrObjectName = strObjectName;
    }

    public void FetchTable(String strUrl) {
        this.strTableUrl = strUrl;
        if (this.mstrFields != null) {
            new Thread(new Runnable() {
                public void run() {
                    List<String[]> table = new DomFeedParser(XMLTableAdaptor.this.strTableUrl).parse();
                    if (XMLTableAdaptor.this.itfcXMLTable != null) {
                        XMLTableAdaptor.this.itfcXMLTable.OnTableReceived(XMLTableAdaptor.this.mstrObjectName, table);
                    }
                }
            }).start();
        } else if (this.itfcXMLTable != null) {
            this.itfcXMLTable.OnTableReceived("", null);
        }
    }

    private class DomFeedParser {
        private final URL feedUrl;
        InputStream xmlInputStream = null;

        protected DomFeedParser(String feedUrl2) {
            try {
                this.feedUrl = new URL(feedUrl2);
            } catch (MalformedURLException e) {
                throw new RuntimeException(e);
            }
        }

        public List<String[]> parse() {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            ArrayList arrayList = new ArrayList();
            try {
                this.xmlInputStream = this.feedUrl.openConnection().getInputStream();
                try {
                    NodeList items = factory.newDocumentBuilder().parse(this.xmlInputStream).getDocumentElement().getElementsByTagName(XMLTableAdaptor.this.mstrObjectName);
                    for (int i = 0; i < items.getLength(); i++) {
                        NodeList rowFields = items.item(i).getChildNodes();
                        int intTotalFieldCount = rowFields.getLength();
                        int intRealFieldCount = XMLTableAdaptor.this.mstrFields.length;
                        String[] strRowValues = new String[intRealFieldCount];
                        for (int j = 0; j < intTotalFieldCount; j++) {
                            Node rowField = rowFields.item(j);
                            for (int k = 0; k < intRealFieldCount; k++) {
                                if (rowField.getNodeName().equalsIgnoreCase(XMLTableAdaptor.this.mstrFields[k])) {
                                    strRowValues[k] = rowField.getFirstChild().getNodeValue();
                                }
                            }
                        }
                        arrayList.add(strRowValues);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (IOException e2) {
                e2.printStackTrace();
            }
            return arrayList;
        }
    }
}
