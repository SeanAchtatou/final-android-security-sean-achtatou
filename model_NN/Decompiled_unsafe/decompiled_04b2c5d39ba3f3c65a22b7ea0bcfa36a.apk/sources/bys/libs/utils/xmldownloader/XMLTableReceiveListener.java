package bys.libs.utils.xmldownloader;

import java.util.List;

public interface XMLTableReceiveListener {
    void OnError(String str);

    void OnTableReceived(String str, List<String[]> list);
}
