package bys.libs.utils.ringtonemanager;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.Toast;
import bys.libs.utils.storage.StorageUtil;
import bys.widgets.srihanuman.R;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class RingToneManager {
    static final String BELL_AUDIO_TITLE = "Temple Bell";
    static final String EXTERNAL_RINGTONE_DIR = "/media/audio/ringtones/";
    static final String SHANKH_AUDIO_TITLE = "Temple Shankh";
    Context mContext = null;
    private String mstrAppName = null;

    public RingToneManager(Context context) {
        this.mContext = context;
        this.mstrAppName = this.mContext.getResources().getString(R.string.app_name);
    }

    public boolean setRingTone(String strAudioPath, String strTitle) {
        return SetOrRemoveAsRingtone(new File(strAudioPath), strTitle, true);
    }

    public boolean setRingTone(String strAudioDir, String strAudioFile, String strTitle) {
        return SetOrRemoveAsRingtone(new File(strAudioDir, strAudioFile), strTitle, true);
    }

    private String getAbsolutePathForGivenUri(Uri contentUri) {
        Cursor cursor = this.mContext.getContentResolver().query(contentUri, new String[]{"_data"}, null, null, null);
        if (cursor == null || cursor.getCount() <= 0) {
            return null;
        }
        int column_index = cursor.getColumnIndexOrThrow("_data");
        cursor.moveToFirst();
        String lstrReturn = cursor.getString(column_index);
        cursor.close();
        return lstrReturn;
    }

    private Uri getRingtoneUriForGivenAudioTitle(String astrAudioTitle) {
        Uri parcialUri = Uri.parse("content://media/external/audio/media");
        Uri finalSuccessfulUri = null;
        Cursor cursor = new RingtoneManager(this.mContext).getCursor();
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            while (true) {
                if (cursor.isAfterLast()) {
                    break;
                } else if (astrAudioTitle.compareToIgnoreCase(cursor.getString(cursor.getColumnIndex("title"))) == 0) {
                    finalSuccessfulUri = Uri.withAppendedPath(parcialUri, new StringBuilder().append(cursor.getInt(cursor.getColumnIndex("_id"))).toString());
                    break;
                } else {
                    cursor.moveToNext();
                }
            }
            cursor.close();
        }
        return finalSuccessfulUri;
    }

    public boolean isBellSetAsRingTone() {
        return isThisSetAsRingTone("Temple Bell.mp3");
    }

    public boolean isShankhSetAsRingTone() {
        return isThisSetAsRingTone("Temple Shankh.mp3");
    }

    public boolean isThisSetAsRingTone(String strAudioFileName) {
        boolean retValue = false;
        Uri uriDefault = RingtoneManager.getActualDefaultRingtoneUri(this.mContext, 1);
        if (StorageUtil.checkExternalStorage() != 0) {
            return false;
        }
        File fileRoot = Environment.getExternalStorageDirectory();
        String lstrAbsoluteAudioPathToVerify = String.valueOf(fileRoot.getAbsolutePath()) + EXTERNAL_RINGTONE_DIR + strAudioFileName;
        Log.v("isThisSetAsRingTone", "fileRoot = " + fileRoot);
        Log.v("isThisSetAsRingTone", "Default Ringtone Uri: = " + uriDefault);
        Log.v("isThisSetAsRingTone", "lstrAbsoluteAudioPathToVerify : = " + lstrAbsoluteAudioPathToVerify);
        if (uriDefault != null) {
            String lstrSetRingtoneAudioPath = getAbsolutePathForGivenUri(uriDefault);
            Log.v("isThisSetAsRingTone", "Set Ringtone audio : = " + lstrSetRingtoneAudioPath);
            if (lstrSetRingtoneAudioPath == null) {
                return false;
            }
            String lstrSetRingtoneAudioPath2 = lstrSetRingtoneAudioPath.replace("/_ExternalSD", "");
            File fileDefaultRingtoneAudio = new File(lstrSetRingtoneAudioPath2);
            if (!fileDefaultRingtoneAudio.exists()) {
                Uri uri = MediaStore.Audio.Media.getContentUriForPath(lstrSetRingtoneAudioPath2);
                this.mContext.getContentResolver().delete(uri, null, null);
                Log.v("isThisSetAsRingTone", "Deleting uri: = " + uri);
                return false;
            }
            Log.v("isThisSetAsRingTone", "fileDefaultRingtoneAudio.exists() = true");
            if (fileDefaultRingtoneAudio.getAbsolutePath().compareTo(lstrAbsoluteAudioPathToVerify) == 0) {
                Log.v("isThisSetAsRingTone", "fileDefaultRingtoneAudio.getAbsolutePath(): = " + fileDefaultRingtoneAudio.getAbsolutePath());
                retValue = true;
            }
        }
        return retValue;
    }

    public void setBellAsRingTone(boolean isChecked) {
        setRawAudioAsRingtone(Uri.parse("android.resource://" + this.mContext.getPackageName() + "/raw/bell"), BELL_AUDIO_TITLE, isChecked);
    }

    public void setShankhAsRingTone(boolean isChecked) {
        setRawAudioAsRingtone(Uri.parse("android.resource://" + this.mContext.getPackageName() + "/raw/shankh"), SHANKH_AUDIO_TITLE, isChecked);
    }

    private boolean setRawAudioAsRingtone(Uri uriRawFolderAudio, String strAudioTitle, boolean blnInsertOrRemove) {
        File fileAudio = copyAudioToSDCard(uriRawFolderAudio, String.valueOf(strAudioTitle) + ".mp3");
        if (fileAudio != null) {
            return SetOrRemoveAsRingtone(fileAudio, strAudioTitle, blnInsertOrRemove);
        }
        Toast.makeText(this.mContext, "Failed: SD card not found !", 1).show();
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void} */
    /* access modifiers changed from: package-private */
    public boolean SetOrRemoveAsRingtone(File fileAudio, String strAudioTitle, boolean blnInsertOrRemove) {
        Uri uri = MediaStore.Audio.Media.getContentUriForPath(fileAudio.getAbsolutePath());
        if (blnInsertOrRemove) {
            ContentValues values = new ContentValues();
            values.put("_data", fileAudio.getAbsolutePath());
            values.put("title", strAudioTitle);
            values.put("mime_type", "audio/mp3");
            values.put("is_ringtone", (Boolean) true);
            values.put("is_notification", (Boolean) false);
            values.put("is_alarm", (Boolean) false);
            values.put("is_music", (Boolean) false);
            Uri newUri = getRingtoneUriForGivenAudioTitle(strAudioTitle);
            Log.v("setRawAudioAsRingtone", "Already inserted newUri : " + newUri);
            if (newUri == null) {
                newUri = this.mContext.getContentResolver().insert(uri, values);
                Log.v("setRawAudioAsRingtone", "After insert newUri : " + newUri);
            }
            if (newUri != null) {
                RingtoneManager.setActualDefaultRingtoneUri(this.mContext, 1, newUri);
                Toast.makeText(this.mContext, String.valueOf(strAudioTitle) + " has been set as ringtone", 1).show();
            } else {
                Toast.makeText(this.mContext, "Failed set " + strAudioTitle + " as ringtone", 1).show();
            }
        } else {
            this.mContext.getContentResolver().delete(uri, null, null);
        }
        return true;
    }

    private File copyAudioToSDCard(Uri aUri, String strRingtoneAudioFile) {
        AssetFileDescriptor soundFile;
        File fileRingtoneDir = StorageUtil.makeFolderInSDCard(EXTERNAL_RINGTONE_DIR);
        if (fileRingtoneDir == null) {
            Log.v("", "fileRingtoneDir " + fileRingtoneDir);
            return null;
        }
        File newSoundFile = new File(fileRingtoneDir, strRingtoneAudioFile);
        if (newSoundFile.exists()) {
            return newSoundFile;
        }
        ContentResolver mCr = this.mContext.getContentResolver();
        try {
            Log.v("", "aUri: " + aUri);
            soundFile = mCr.openAssetFileDescriptor(aUri, "r");
            Log.v("", "soundFile: " + soundFile);
        } catch (FileNotFoundException e) {
            soundFile = null;
        }
        try {
            byte[] readData = new byte[1024];
            FileInputStream fis = soundFile.createInputStream();
            FileOutputStream fos = new FileOutputStream(newSoundFile);
            for (int i = fis.read(readData); i != -1; i = fis.read(readData)) {
                fos.write(readData, 0, i);
            }
            fos.close();
            return newSoundFile;
        } catch (IOException e2) {
            return newSoundFile;
        }
    }
}
