package bys.libs.utils.storage;

import android.os.Environment;
import java.io.File;

public class StorageUtil {
    public static String getErrorString(int intErrorIndex) {
        switch (intErrorIndex) {
            case 1:
                return "SD Card is not mounted";
            case 2:
                return "SD Card is read only";
            default:
                return null;
        }
    }

    public static int checkExternalStorage() {
        String state = Environment.getExternalStorageState();
        if (!"mounted".equals(state)) {
            return 1;
        }
        if ("mounted_ro".equals(state)) {
            return 2;
        }
        return 0;
    }

    public static File makeFolderInSDCard(String strFolderPath) {
        if (checkExternalStorage() != 0) {
            return null;
        }
        File fileCreatedDir = new File(Environment.getExternalStorageDirectory(), strFolderPath);
        fileCreatedDir.mkdirs();
        return fileCreatedDir;
    }
}
