package bys.apps.components.audiodownloader;

public interface AudioListUpdateListeners {
    void onNeworkError(String str);

    void onSearchFailed();

    void onUpdateAvailable();
}
