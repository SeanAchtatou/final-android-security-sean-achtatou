package bys.apps.components.audiodownloader;

import android.content.Context;
import android.os.Build;
import android.util.Log;
import bys.libs.utils.xmldownloader.XMLTableAdaptor;
import bys.libs.utils.xmldownloader.XMLTableReceiveListener;
import bys.widgets.srihanuman.R;
import java.util.List;

public class AudioListManager {
    static final String strTag = "AudioListManager";
    List<String[]> listAudioInfo;
    Context mContext;
    AudioListUpdateListeners mInfcAudioListUpdateListeners;
    Runnable m_taskCheckForUpdates;
    XMLTableAdaptor xmlTableAdaptor;

    public List<String[]> getAudioInfoList() {
        return this.listAudioInfo;
    }

    public void setInfcAudioListUpdateListeners(AudioListUpdateListeners infcAudioListUpdateListeners) {
        this.mInfcAudioListUpdateListeners = infcAudioListUpdateListeners;
    }

    public AudioListManager(Context context) {
        this.listAudioInfo = null;
        this.xmlTableAdaptor = null;
        this.mInfcAudioListUpdateListeners = null;
        this.mContext = null;
        this.m_taskCheckForUpdates = null;
        this.xmlTableAdaptor = new XMLTableAdaptor();
        this.mContext = context;
        this.xmlTableAdaptor.setOnTableReceiveListener(new XMLTableReceiveListener() {
            public void OnTableReceived(String table_name, List<String[]> table) {
                AudioListManager.this.listAudioInfo = table;
                int lintAudioCount = AudioListManager.this.listAudioInfo.size();
                Log.v(AudioListManager.strTag, "Fetch Successful: Count = " + lintAudioCount);
                if (lintAudioCount > 0) {
                    AudioListManager.this.mInfcAudioListUpdateListeners.onUpdateAvailable();
                } else {
                    AudioListManager.this.mInfcAudioListUpdateListeners.onSearchFailed();
                }
            }

            public void OnError(String error) {
                AudioListManager.this.mInfcAudioListUpdateListeners.onNeworkError(error);
            }
        });
        this.m_taskCheckForUpdates = new Runnable() {
            public void run() {
                AudioListManager.this.xmlTableAdaptor.setExpectedFields("audio-info", new String[]{"title", "desc", "url", "size", "version"});
                AudioListManager.this.xmlTableAdaptor.FetchTable(String.valueOf(AudioListManager.this.mContext.getString(R.string.BaseUrl)) + AudioListManager.this.mContext.getPackageName() + "/search_audio_hybrid.php?version=" + Build.VERSION.SDK);
            }
        };
    }

    public void CheckForUpdates() {
        new Thread(this.m_taskCheckForUpdates).start();
    }
}
