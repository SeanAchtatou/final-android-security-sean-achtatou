package bys.apps.tools.lyricserializer;

import android.content.Context;
import android.util.Log;
import bys.apps.tools.lyricserializer.SerializerClass;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class PlayListManager {
    private ArrayList<AudioInfo> marrAudioPlayList = new ArrayList<>();
    private ArrayList<String> marrSelectedAudioList = new ArrayList<>();
    private ArrayList<String> marrUnSelectedAudioList = new ArrayList<>();
    private boolean mblnShuffle = false;
    private File mdirAudioFolder;
    private File mfileDeafultPlayList = null;
    private int mintCurrentAudioIndex = 0;
    private String mstrModuleName = getClass().getSimpleName();
    private String mstrPlayListFileName;

    public PlayListManager(Context context, File dirAppFolder) {
        this.mdirAudioFolder = dirAppFolder;
        if (this.mdirAudioFolder != null) {
            File filePlayListDir = new File(String.valueOf(this.mdirAudioFolder.getAbsolutePath()) + "/playlists");
            filePlayListDir.mkdirs();
            this.mfileDeafultPlayList = new File(filePlayListDir, "default_playlist.plt");
            if (this.mfileDeafultPlayList.exists()) {
                readUnslectedAudioListFromFile(this.mfileDeafultPlayList.getAbsolutePath());
            }
            this.marrAudioPlayList = preparePlayListFromAllAvailableAudio();
            prepareSelectedAudioList();
        }
    }

    public void prapareFreshPlayList() {
        if (this.mfileDeafultPlayList != null && this.mfileDeafultPlayList.exists()) {
            readUnslectedAudioListFromFile(this.mfileDeafultPlayList.getAbsolutePath());
        }
        this.marrAudioPlayList = preparePlayListFromAllAvailableAudio();
        prepareSelectedAudioList();
        if (this.mblnShuffle) {
            shuffleetSelectedAudioList();
        }
        Log.v(this.mstrModuleName, "prapareFreshPlayList: Selected Audio List Size = " + this.marrSelectedAudioList.size());
    }

    public void enableShuffleOn(boolean flag) {
        this.mblnShuffle = flag;
        if (this.mblnShuffle) {
            shuffleetSelectedAudioList();
        } else {
            prepareSelectedAudioList();
        }
    }

    public int readUnslectedAudioListFromFile(String strLyricFile) {
        FileInputStream in = null;
        this.mstrPlayListFileName = strLyricFile;
        int intAudioCount = 0;
        try {
            in = new FileInputStream(this.mstrPlayListFileName);
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        }
        if (in == null) {
            return 0;
        }
        try {
            this.marrUnSelectedAudioList = readPlayList(in);
            if (this.marrUnSelectedAudioList != null) {
                intAudioCount = this.marrUnSelectedAudioList.size();
                if (intAudioCount > 0) {
                    int i = intAudioCount - 1;
                    while (i >= 0) {
                        if (!new File(this.marrUnSelectedAudioList.get(i)).exists()) {
                            this.marrUnSelectedAudioList.remove(i);
                            i--;
                        }
                        i--;
                    }
                    intAudioCount = this.marrUnSelectedAudioList.size();
                }
            } else {
                File f = new File(strLyricFile);
                if (f.exists()) {
                    f.deleteOnExit();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            in.close();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        return intAudioCount;
    }

    public void notifyAudioSelectionChange() {
        prepareSelectedAudioList();
        saveUnselectedAudioListToFile();
    }

    public void saveUnselectedAudioListToFile() {
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(this.mfileDeafultPlayList.getAbsolutePath());
            prepareUnslectedAudioList();
            byte[] bytAudioInfoSerializedBytes = SerializerClass.serializeObject(this.marrUnSelectedAudioList);
            try {
                fileOutputStream.write(SerializerClass.serializeObject(new SerializerClass.FileHeader("bys.plt", bytAudioInfoSerializedBytes.length)));
                fileOutputStream.write(bytAudioInfoSerializedBytes);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            try {
                fileOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e2) {
            e2.printStackTrace();
        }
    }

    public boolean setSelectedFlag(int intAudioIndex, boolean flag) {
        if (this.marrAudioPlayList.size() <= intAudioIndex) {
            return false;
        }
        this.marrAudioPlayList.get(intAudioIndex).blnSelected = flag;
        return true;
    }

    public boolean getSelectedFlag(int intAudioIndex) {
        if (this.marrAudioPlayList.size() > intAudioIndex) {
            return this.marrAudioPlayList.get(intAudioIndex).blnSelected;
        }
        return false;
    }

    private boolean isUnselected(String strAudioName) {
        if (this.marrUnSelectedAudioList == null) {
            return true;
        }
        for (int i = 0; i < this.marrUnSelectedAudioList.size(); i++) {
            if (this.marrUnSelectedAudioList.get(i).compareTo(strAudioName) == 0) {
                return true;
            }
        }
        return false;
    }

    private void prepareUnslectedAudioList() {
        this.marrUnSelectedAudioList = new ArrayList<>();
        if (this.marrAudioPlayList != null && this.marrAudioPlayList.size() > 0) {
            for (int i = 0; i < this.marrAudioPlayList.size(); i++) {
                if (!this.marrAudioPlayList.get(i).blnSelected) {
                    this.marrUnSelectedAudioList.add(this.marrAudioPlayList.get(i).strAudioPath);
                }
            }
        }
    }

    private ArrayList<AudioInfo> preparePlayListFromAllAvailableAudio() {
        File[] lFiles;
        ArrayList<AudioInfo> arrAudioPlayList = new ArrayList<>();
        if (!(this.mdirAudioFolder == null || (lFiles = this.mdirAudioFolder.listFiles()) == null || lFiles.length <= 0)) {
            try {
                Arrays.sort(lFiles);
            } catch (Exception e) {
            }
            for (File f : lFiles) {
                if (f.isFile() && f.getName().contains(".mp3")) {
                    AudioInfo ai = new AudioInfo(f.getAbsolutePath());
                    if (isUnselected(ai.strAudioPath)) {
                        ai.blnSelected = false;
                    }
                    arrAudioPlayList.add(ai);
                }
            }
        }
        return arrAudioPlayList;
    }

    public String getCurrentAudioPath() {
        if (this.marrSelectedAudioList.size() <= 0) {
            Log.v(this.mstrModuleName, "getCurrentAudioPath = " + "");
            prapareFreshPlayList();
        }
        if (this.marrSelectedAudioList.size() <= 0) {
            Log.v(this.mstrModuleName, "getCurrentAudioPath = " + "");
            return "";
        }
        if (this.marrSelectedAudioList.size() <= this.mintCurrentAudioIndex) {
            this.mintCurrentAudioIndex = 0;
        }
        String lstrAudioPath = this.marrSelectedAudioList.get(this.mintCurrentAudioIndex);
        Log.v(this.mstrModuleName, "getCurrentAudioPath = " + lstrAudioPath);
        return lstrAudioPath;
    }

    public boolean isLastAudio() {
        if (this.mintCurrentAudioIndex == this.marrSelectedAudioList.size() - 1) {
            return true;
        }
        return false;
    }

    public String getNextAudioPath() {
        if (this.marrSelectedAudioList.size() <= 0) {
            Log.v(this.mstrModuleName, "getNextAudioPath = " + "");
            return "";
        }
        this.mintCurrentAudioIndex++;
        if (this.marrSelectedAudioList.size() <= this.mintCurrentAudioIndex) {
            this.mintCurrentAudioIndex = 0;
        }
        String lstrAudioPath = this.marrSelectedAudioList.get(this.mintCurrentAudioIndex);
        Log.v(this.mstrModuleName, "getNextAudioPath = " + lstrAudioPath);
        return lstrAudioPath;
    }

    public String getPrevAudioPath() {
        String lstrAudioPath = "";
        if (this.marrSelectedAudioList.size() <= 0) {
            Log.v(this.mstrModuleName, "getPrevAudioPath = " + lstrAudioPath);
            return lstrAudioPath;
        }
        if (this.mintCurrentAudioIndex == 0) {
            this.mintCurrentAudioIndex = this.marrSelectedAudioList.size() - 1;
        } else {
            this.mintCurrentAudioIndex--;
        }
        if (this.marrSelectedAudioList.size() > this.mintCurrentAudioIndex) {
            lstrAudioPath = this.marrSelectedAudioList.get(this.mintCurrentAudioIndex);
        } else {
            this.mintCurrentAudioIndex = 0;
        }
        Log.v(this.mstrModuleName, "getPrevAudioPath = " + lstrAudioPath);
        return lstrAudioPath;
    }

    public String getFirstAudioPath() {
        String lstrAudioPath = "";
        if (this.marrSelectedAudioList.size() > 0) {
            lstrAudioPath = this.marrSelectedAudioList.get(0);
        }
        Log.v(this.mstrModuleName, "getFirstAudioPath = " + lstrAudioPath);
        return lstrAudioPath;
    }

    public void shuffleetSelectedAudioList() {
        ArrayList<String> arrShuffledAudioList = new ArrayList<>();
        RandomGenerator randGen = new RandomGenerator(this, null);
        if (this.marrSelectedAudioList.size() != 0) {
            while (this.marrSelectedAudioList.size() > 1) {
                int intRandomIndex = randGen.nextInt(0, this.marrSelectedAudioList.size() - 1);
                arrShuffledAudioList.add(this.marrSelectedAudioList.get(intRandomIndex));
                this.marrSelectedAudioList.remove(intRandomIndex);
            }
            arrShuffledAudioList.add(this.marrSelectedAudioList.get(0));
            this.marrSelectedAudioList = arrShuffledAudioList;
        }
    }

    public String getLastAudioPath() {
        String lstrAudioPath = "";
        if (this.marrSelectedAudioList.size() > 0) {
            lstrAudioPath = this.marrSelectedAudioList.get(this.marrSelectedAudioList.size() - 1);
        }
        Log.v(this.mstrModuleName, "getLastAudioPath = " + lstrAudioPath);
        return lstrAudioPath;
    }

    private class RandomGenerator extends Random {
        private static final long serialVersionUID = 1;

        private RandomGenerator() {
        }

        /* synthetic */ RandomGenerator(PlayListManager playListManager, RandomGenerator randomGenerator) {
            this();
        }

        public int nextInt(int L_limit, int U_limit) {
            return nextInt((U_limit - L_limit) + 1) + L_limit;
        }
    }

    private class AudioInfo implements Serializable {
        private static final long serialVersionUID = 2;
        public boolean blnSelected = true;
        public String strAudioPath;

        public AudioInfo(String lstrAudioPath) {
            this.strAudioPath = lstrAudioPath;
        }

        private void writeObject(ObjectOutputStream out) throws IOException {
            out.writeObject(this.strAudioPath);
            out.writeBoolean(this.blnSelected);
        }

        private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
            this.strAudioPath = (String) in.readObject();
            this.blnSelected = in.readBoolean();
        }
    }

    private ArrayList<String> readPlayList(FileInputStream in) throws IOException {
        byte[] bytSignature = new byte[150];
        Log.v(this.mstrModuleName, "byteRead = " + in.read(bytSignature, 0, 150));
        try {
            SerializerClass.FileHeader aHeaderAfterDeserialize = (SerializerClass.FileHeader) SerializerClass.deserializeObject(bytSignature);
            if (aHeaderAfterDeserialize.strSignature.compareTo("bys.plt") != 0) {
                Log.v(this.mstrModuleName, "Invalid Playlist File");
                return null;
            }
            Log.v(this.mstrModuleName, "aHeaderAfterDeserialize.intPartOneLength = " + aHeaderAfterDeserialize.intPartOneLength);
            byte[] bytInfoList = new byte[aHeaderAfterDeserialize.intPartOneLength];
            Log.v("readHeader", "byteRead = " + in.read(bytInfoList));
            Log.v("readHeader", "bytSignature = " + bytSignature.length);
            Log.v("readHeader", "bytInfoList = " + bytInfoList.length);
            ArrayList<String> strUnselectedAudioList = (ArrayList) SerializerClass.deserializeObject(bytInfoList);
            Log.v(this.mstrModuleName, "AudioInfo List Size = " + strUnselectedAudioList.size());
            return strUnselectedAudioList;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public int getAudioListSize() {
        if (this.marrAudioPlayList.size() == 0) {
            this.marrAudioPlayList = preparePlayListFromAllAvailableAudio();
        }
        return this.marrAudioPlayList.size();
    }

    public String getAudioFilePath(int position) {
        if (this.marrAudioPlayList == null || this.marrAudioPlayList.size() <= position) {
            return null;
        }
        return this.marrAudioPlayList.get(position).strAudioPath;
    }

    public void SelectAll(boolean isChecked) {
        if (this.marrAudioPlayList != null && this.marrAudioPlayList.size() > 0) {
            for (int i = 0; i < this.marrAudioPlayList.size(); i++) {
                this.marrAudioPlayList.get(i).blnSelected = isChecked;
            }
        }
    }

    private void prepareSelectedAudioList() {
        this.marrSelectedAudioList.clear();
        if (this.marrAudioPlayList != null && this.marrAudioPlayList.size() > 0) {
            for (int i = 0; i < this.marrAudioPlayList.size(); i++) {
                if (this.marrAudioPlayList.get(i).blnSelected) {
                    this.marrSelectedAudioList.add(this.marrAudioPlayList.get(i).strAudioPath);
                }
            }
        }
    }

    private void prepareSelectedAudioList(String strAudioFullPath) {
        this.marrSelectedAudioList.clear();
        if (this.marrAudioPlayList != null && this.marrAudioPlayList.size() > 0) {
            for (int i = 0; i < this.marrAudioPlayList.size(); i++) {
                if (this.marrAudioPlayList.get(i).blnSelected) {
                    String lstrAudioPath = this.marrAudioPlayList.get(i).strAudioPath;
                    if (lstrAudioPath.compareTo(strAudioFullPath) == 0) {
                        Log.v(this.mstrModuleName, "prepareSelectedAudioList = " + strAudioFullPath);
                        Log.v(this.mstrModuleName, "mintCurrentAudioIndex = " + this.mintCurrentAudioIndex);
                        this.mintCurrentAudioIndex = this.marrSelectedAudioList.size();
                    }
                    this.marrSelectedAudioList.add(lstrAudioPath);
                }
            }
        }
    }

    public void setCurrentAudioPath(String strAudioFullPath) {
        Log.v(this.mstrModuleName, "setCurrentAudioPath = " + strAudioFullPath);
        if (this.marrAudioPlayList != null && this.marrAudioPlayList.size() > 0) {
            int i = 0;
            while (true) {
                if (i >= this.marrAudioPlayList.size()) {
                    break;
                } else if (this.marrAudioPlayList.get(i).strAudioPath.compareTo(strAudioFullPath) == 0) {
                    this.marrAudioPlayList.get(i).blnSelected = true;
                    break;
                } else {
                    i++;
                }
            }
            prepareSelectedAudioList(strAudioFullPath);
        }
    }
}
