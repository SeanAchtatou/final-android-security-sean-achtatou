package bys.apps.tools.lyricserializer;

import android.util.Log;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class SerializerClass {
    public static byte[] serializeObject(Object o) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            ObjectOutput out = new ObjectOutputStream(bos);
            out.writeObject(o);
            out.close();
            return bos.toByteArray();
        } catch (IOException ioe) {
            Log.e("serializeObject", "error", ioe);
            return null;
        }
    }

    public static Object deserializeObject(byte[] b) {
        try {
            ObjectInputStream in = new ObjectInputStream(new ByteArrayInputStream(b));
            Object object = in.readObject();
            in.close();
            return object;
        } catch (ClassNotFoundException cnfe) {
            Log.e("deserializeObject", "class not found error", cnfe);
            return null;
        } catch (IOException ioe) {
            Log.e("deserializeObject", "io error", ioe);
            return null;
        }
    }

    public static class FileHeader implements Serializable {
        private static final long serialVersionUID = 11;
        public int intPartOneLength;
        public String strSignature;

        public FileHeader(String astrSignature, int aintPartOneLength) {
            this.strSignature = astrSignature;
            this.intPartOneLength = aintPartOneLength;
        }

        private void writeObject(ObjectOutputStream out) throws IOException {
            out.writeObject(this.strSignature);
            out.writeInt(this.intPartOneLength);
        }

        private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
            this.strSignature = (String) in.readObject();
            this.intPartOneLength = in.readInt();
        }
    }

    public static class PartOneItem implements Serializable {
        private static final long serialVersionUID = 12;
        public int intDuration;
        public String strTextEnglish;
        public String strTextLocal;

        public PartOneItem(int aintDuration, String astrTextEnglish, String astrTextLocal) {
            this.intDuration = aintDuration;
            this.strTextLocal = astrTextLocal;
            this.strTextEnglish = astrTextEnglish;
        }

        private void writeObject(ObjectOutputStream out) throws IOException {
            out.writeInt(this.intDuration);
            out.writeObject(this.strTextEnglish);
            out.writeObject(this.strTextLocal);
        }

        private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
            this.intDuration = in.readInt();
            this.strTextEnglish = (String) in.readObject();
            this.strTextLocal = (String) in.readObject();
        }
    }
}
