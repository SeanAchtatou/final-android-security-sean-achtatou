package bys.customviews.candleflameview;

import android.content.Context;
import android.os.Handler;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import bys.widgets.srihanuman.R;
import java.util.Random;

public class CandleFlameView extends RelativeLayout {
    Handler handle = new Handler();
    ImageView imgDiya = null;
    ImageView imgFlame = null;
    Context mContext = null;
    Runnable m_taskAnimation = null;
    boolean mblnStopAnimation = true;
    int mintMaxHeight;
    int mintMaxHeightChange = 10;
    int mintMaxWidth;
    int mintMaxWidthChange = 4;
    int mintMinHeight;
    int mintMinWidth;
    RandomGenerator randGen = new RandomGenerator(this, null);

    private int getPixelFromDip(float dips) {
        return Math.round(dips * this.mContext.getResources().getDisplayMetrics().density);
    }

    private void createFlameView(Context context) {
        this.mContext = context;
        this.imgDiya = new ImageView(this.mContext);
        this.imgFlame = new ImageView(this.mContext);
        this.imgFlame.setBackgroundResource(R.drawable.candle_flame);
        this.imgDiya.setBackgroundResource(R.drawable.dia_pot);
        this.mintMaxHeightChange = getPixelFromDip(10.0f);
        this.mintMaxWidthChange = getPixelFromDip(4.0f);
        addView(this.imgDiya);
        addView(this.imgFlame);
        RelativeLayout.LayoutParams lparamsDiya = (RelativeLayout.LayoutParams) this.imgDiya.getLayoutParams();
        lparamsDiya.addRule(12);
        lparamsDiya.addRule(14);
        this.imgDiya.setLayoutParams(lparamsDiya);
        RelativeLayout.LayoutParams lparamsFlame = (RelativeLayout.LayoutParams) this.imgFlame.getLayoutParams();
        lparamsFlame.height = getPixelFromDip(40.0f);
        lparamsFlame.width = getPixelFromDip(10.0f);
        lparamsFlame.bottomMargin = getPixelFromDip(38.0f);
        lparamsFlame.addRule(12);
        lparamsFlame.addRule(14);
        this.imgFlame.setLayoutParams(lparamsFlame);
    }

    public CandleFlameView(Context context) {
        super(context);
        createFlameView(context);
    }

    public CandleFlameView(Context context, AttributeSet attrs) {
        super(context, attrs);
        createFlameView(context);
    }

    public void Initialize(Context context) {
        this.mContext = context;
        RelativeLayout.LayoutParams lparamsFlame = (RelativeLayout.LayoutParams) this.imgFlame.getLayoutParams();
        this.mintMaxHeight = lparamsFlame.height + (this.mintMaxHeightChange / 2);
        this.mintMinHeight = lparamsFlame.height - (this.mintMaxHeightChange / 2);
        this.mintMaxWidth = lparamsFlame.width + (this.mintMaxWidthChange / 2);
        this.mintMinWidth = lparamsFlame.width - (this.mintMaxWidthChange / 2);
        this.m_taskAnimation = new Runnable() {
            public void run() {
                RelativeLayout.LayoutParams lparamsFlame = (RelativeLayout.LayoutParams) CandleFlameView.this.imgFlame.getLayoutParams();
                if (lparamsFlame.height >= CandleFlameView.this.mintMaxHeight) {
                    lparamsFlame.height--;
                } else if (lparamsFlame.height <= CandleFlameView.this.mintMinHeight) {
                    lparamsFlame.height++;
                } else {
                    lparamsFlame.height += CandleFlameView.this.randGen.nextInt(-1, 1);
                }
                if (lparamsFlame.width >= CandleFlameView.this.mintMaxWidth) {
                    lparamsFlame.width--;
                } else if (lparamsFlame.width <= CandleFlameView.this.mintMinWidth) {
                    lparamsFlame.width++;
                } else {
                    lparamsFlame.width += CandleFlameView.this.randGen.nextInt(-1, 1);
                }
                CandleFlameView.this.imgFlame.setLayoutParams(lparamsFlame);
                if (!CandleFlameView.this.mblnStopAnimation) {
                    CandleFlameView.this.handle.postAtTime(this, SystemClock.uptimeMillis() + 200);
                }
            }
        };
    }

    public void StartFlameAnimation() {
        this.mblnStopAnimation = false;
        if (this.handle != null) {
            this.handle.removeCallbacks(this.m_taskAnimation);
            this.handle.post(this.m_taskAnimation);
        }
    }

    public void StopFlameAnimation() {
        this.mblnStopAnimation = true;
    }

    private class RandomGenerator extends Random {
        private static final long serialVersionUID = 1;

        private RandomGenerator() {
        }

        /* synthetic */ RandomGenerator(CandleFlameView candleFlameView, RandomGenerator randomGenerator) {
            this();
        }

        public int nextInt(int L_limit, int U_limit) {
            return nextInt((U_limit - L_limit) + 1) + L_limit;
        }
    }
}
