package bys.customviews.flowershowerview;

import android.content.Context;
import android.os.Handler;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import bys.widgets.srihanuman.R;
import java.util.Random;

public class FlowerShowerView extends RelativeLayout {
    ImageView[] arrImages;
    Handler handle = new Handler();
    ImageView imgFlowerPot = null;
    RelativeLayout layoutHolder;
    Context mContext = null;
    Runnable m_taskAnimation = null;
    boolean mblnAllFallen = false;
    private int mintFlowerCount = 0;
    int mintIterationCount = 0;
    int mintPlateBottomMargin = 0;
    int mintPlateWidth = 0;
    /* access modifiers changed from: private */
    public int[] mintVx;
    /* access modifiers changed from: private */
    public int[] mintVy;
    int mintWindowWidth = 0;
    FlowerShowerActionListeners onFlowerShowerActionListener = null;
    RandomGenerator randGen = new RandomGenerator(this, null);

    private int getPixelFromDip(float dips) {
        return Math.round(dips * this.mContext.getResources().getDisplayMetrics().density);
    }

    public int getPlateWidth() {
        return this.mintPlateWidth;
    }

    public void setPlateWidth(int mintPlateWidth2) {
        this.mintPlateWidth = mintPlateWidth2;
    }

    public void setOnFlowerShowerActionListener(FlowerShowerActionListeners onFlowerShowerActionListener2) {
        this.onFlowerShowerActionListener = onFlowerShowerActionListener2;
    }

    private void createAnimationTask(Context context) {
        this.layoutHolder = this;
        this.mContext = context;
        this.mintPlateWidth = getPixelFromDip(52.0f);
        this.mintPlateBottomMargin = getPixelFromDip(100.0f);
        this.mintWindowWidth = ((WindowManager) this.mContext.getSystemService("window")).getDefaultDisplay().getWidth();
        this.m_taskAnimation = new Runnable() {
            public void run() {
                FlowerShowerView.this.mblnAllFallen = true;
                FlowerShowerView.this.mintIterationCount++;
                for (int i = 0; i < FlowerShowerView.this.arrImages.length; i++) {
                    RelativeLayout.LayoutParams lparamsBall = (RelativeLayout.LayoutParams) FlowerShowerView.this.arrImages[i].getLayoutParams();
                    if (lparamsBall.rightMargin < FlowerShowerView.this.mintWindowWidth - FlowerShowerView.this.arrImages[i].getWidth() && lparamsBall.bottomMargin > FlowerShowerView.this.arrImages[i].getHeight()) {
                        lparamsBall.rightMargin = FlowerShowerView.this.mintVx[i] * FlowerShowerView.this.mintIterationCount;
                        FlowerShowerView.this.mblnAllFallen = false;
                    }
                    if (lparamsBall.bottomMargin > FlowerShowerView.this.arrImages[i].getHeight()) {
                        lparamsBall.bottomMargin = (FlowerShowerView.this.mintPlateBottomMargin + (FlowerShowerView.this.mintVy[i] * FlowerShowerView.this.mintIterationCount)) - ((int) (0.98d * ((double) ((float) (FlowerShowerView.this.mintIterationCount * FlowerShowerView.this.mintIterationCount)))));
                        FlowerShowerView.this.mblnAllFallen = false;
                    } else {
                        lparamsBall.bottomMargin = FlowerShowerView.this.arrImages[i].getHeight();
                    }
                    FlowerShowerView.this.arrImages[i].setLayoutParams(lparamsBall);
                }
                if (!FlowerShowerView.this.mblnAllFallen) {
                    FlowerShowerView.this.handle.postAtTime(this, SystemClock.uptimeMillis() + 30);
                } else if (FlowerShowerView.this.onFlowerShowerActionListener != null) {
                    FlowerShowerView.this.onFlowerShowerActionListener.onAllFallen();
                }
            }
        };
    }

    public FlowerShowerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        createAnimationTask(context);
    }

    public void initializeFlowers(int aintFlowerCount) {
        this.mintFlowerCount = aintFlowerCount;
        this.imgFlowerPot = new ImageView(this.mContext);
        this.arrImages = new ImageView[this.mintFlowerCount];
        this.mintVx = new int[this.mintFlowerCount];
        this.mintVy = new int[this.mintFlowerCount];
        addView(this.imgFlowerPot);
        for (int i = 0; i < this.mintFlowerCount; i++) {
            this.arrImages[i] = new ImageView(this.mContext);
            this.arrImages[i].setBackgroundResource(R.drawable.flower1 + this.randGen.nextInt(0, 5));
            addView(this.arrImages[i]);
            RelativeLayout.LayoutParams lparamsBall = (RelativeLayout.LayoutParams) this.arrImages[i].getLayoutParams();
            lparamsBall.height = this.randGen.nextInt(getPixelFromDip(5.0f), getPixelFromDip(20.0f));
            lparamsBall.width = lparamsBall.height;
            lparamsBall.addRule(11);
            lparamsBall.addRule(12);
            this.arrImages[i].setLayoutParams(lparamsBall);
        }
        this.imgFlowerPot.setImageResource(R.drawable.puja_plate_small);
        RelativeLayout.LayoutParams lparamsPot = (RelativeLayout.LayoutParams) this.imgFlowerPot.getLayoutParams();
        lparamsPot.bottomMargin = this.mintPlateBottomMargin;
        lparamsPot.height = 30;
        lparamsPot.width = this.mintPlateWidth;
        lparamsPot.addRule(11);
        lparamsPot.addRule(12);
        this.imgFlowerPot.setLayoutParams(lparamsPot);
        this.imgFlowerPot.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (FlowerShowerView.this.mblnAllFallen) {
                    FlowerShowerView.this.moveFlowersToPlate();
                } else {
                    FlowerShowerView.this.animateShowerFlowers();
                }
            }
        });
        moveFlowersToPlate();
    }

    public void animateDisappearPlate() {
    }

    public void animateAppearPlate() {
    }

    public void animateShowerFlowers() {
        for (int i = 0; i < this.arrImages.length; i++) {
            this.mintVx[i] = this.randGen.nextInt(2, 8);
            this.mintVy[i] = this.randGen.nextInt(2, 35);
        }
        this.mintIterationCount = 0;
        this.handle.post(this.m_taskAnimation);
    }

    public void moveFlowersToPlate() {
        int intFlowerOffsetFromPlateBottom = getPixelFromDip(7.0f);
        for (int i = 0; i < this.arrImages.length; i++) {
            RelativeLayout.LayoutParams lparamsBall = (RelativeLayout.LayoutParams) this.arrImages[i].getLayoutParams();
            int intHeightFromPlate = this.randGen.nextInt(getPixelFromDip(10.0f), getPixelFromDip(18.0f));
            lparamsBall.bottomMargin = this.mintPlateBottomMargin + intHeightFromPlate;
            lparamsBall.rightMargin = this.randGen.nextInt(intHeightFromPlate - intFlowerOffsetFromPlateBottom, (this.mintPlateWidth - intHeightFromPlate) - intFlowerOffsetFromPlateBottom);
            this.arrImages[i].setLayoutParams(lparamsBall);
        }
        this.mblnAllFallen = false;
    }

    private class RandomGenerator extends Random {
        private static final long serialVersionUID = 1;

        private RandomGenerator() {
        }

        /* synthetic */ RandomGenerator(FlowerShowerView flowerShowerView, RandomGenerator randomGenerator) {
            this();
        }

        public int nextInt(int L_limit, int U_limit) {
            return nextInt((U_limit - L_limit) + 1) + L_limit;
        }
    }
}
