package bys.customviews.audioplayer;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.SystemClock;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RemoteViews;
import android.widget.TextView;
import bys.apps.tools.lyricserializer.PlayListManager;
import bys.libs.utils.storage.StorageUtil;
import bys.widgets.srihanuman.AudioListActivity;
import bys.widgets.srihanuman.R;
import bys.widgets.srihanuman.TempleMainActivity;
import java.io.File;

public class AudioPlayerView extends LinearLayout {
    boolean blnPasuedByIncommingCall = false;
    Handler handle = new Handler();
    /* access modifiers changed from: private */
    public ImageView imgPause = null;
    /* access modifiers changed from: private */
    public ImageView imgPlay = null;
    private ImageView imgPlayNext = null;
    private ImageView imgPlayPrev = null;
    /* access modifiers changed from: private */
    public ImageView imgStylus = null;
    AudioPlayerListeners infcAudioPlayerListener = null;
    private LinearLayout layoutBuffering = null;
    RelativeLayout layoutHolder;
    MediaPlayer mAudioPlayer = new MediaPlayer();
    Context mContext = null;
    /* access modifiers changed from: private */
    public NotificationManager mNotificationManager;
    Runnable m_taskPreview = null;
    Runnable m_taskProgress = null;
    /* access modifiers changed from: private */
    public boolean mblnRepeatContinous = false;
    int mintAudioPlayerState = AudioPlayerState.IDLE;
    /* access modifiers changed from: private */
    public int mintAudioRepeatCount = 0;
    /* access modifiers changed from: private */
    public int mintAudioRepeatIndex = 0;
    /* access modifiers changed from: private */
    public int mintDuration = 0;
    /* access modifiers changed from: private */
    public PlayListManager mplstManager = null;
    /* access modifiers changed from: private */
    public String mstrAudioSource = null;
    private String mstrModuleName = "AudioPlayerView";
    /* access modifiers changed from: private */
    public String mstrScreenId = "0";
    /* access modifiers changed from: private */
    public String mstrTitle = "";
    /* access modifiers changed from: private */
    public Notification notifyDetails;
    /* access modifiers changed from: private */
    public TextView txtBuffering = null;
    private TextView txtElapseTime = null;
    /* access modifiers changed from: private */
    public TextView txtTitle = null;
    /* access modifiers changed from: private */
    public TextView txtTotalTime = null;

    private static class AudioPlayerState {
        static int END = 128;
        static int IDLE = 0;
        static int INITIALIZED = 2;
        static int PAUSED = 16;
        static int PLAYBACK_COMPLETED = 32;
        static int PREPARED = 4;
        static int STARTED = 8;
        static int STOPPED = 64;

        private AudioPlayerState() {
        }
    }

    /* access modifiers changed from: private */
    public void updatePreogres() {
        if (this.mintDuration > 0) {
            int intElapseTime = this.mAudioPlayer.getCurrentPosition();
            int intStylusMargin = ((this.txtTitle.getWidth() - this.imgStylus.getWidth()) * intElapseTime) / this.mintDuration;
            RelativeLayout.LayoutParams lparamsBall = (RelativeLayout.LayoutParams) this.imgStylus.getLayoutParams();
            lparamsBall.leftMargin = intStylusMargin;
            this.imgStylus.setLayoutParams(lparamsBall);
            this.txtElapseTime.setText(convertToMMSS(intElapseTime));
        }
    }

    /* access modifiers changed from: private */
    public CharSequence convertToMMSS(int mintDuration2) {
        return String.format("%2d:%2d", Integer.valueOf((mintDuration2 / 60000) % 60), Integer.valueOf((mintDuration2 / 1000) % 60));
    }

    public void setAudioPlayerListener(AudioPlayerListeners infcAudioPlayerListener2) {
        this.infcAudioPlayerListener = infcAudioPlayerListener2;
    }

    /* access modifiers changed from: private */
    public void showBufferingStatus(int intPercent) {
        int intMargin = (this.txtTitle.getWidth() * (100 - intPercent)) / 100;
        RelativeLayout.LayoutParams lparamsBall = (RelativeLayout.LayoutParams) this.layoutBuffering.getLayoutParams();
        lparamsBall.rightMargin = intMargin;
        this.layoutBuffering.setLayoutParams(lparamsBall);
    }

    /* access modifiers changed from: package-private */
    public void hideNotification() {
        this.mNotificationManager.cancel(1);
    }

    /* access modifiers changed from: package-private */
    public void showNotification() {
        this.handle.postAtTime(new Runnable() {
            public void run() {
                if (AudioPlayerView.this.mintAudioPlayerState == AudioPlayerState.STARTED) {
                    AudioPlayerView.this.initNotification();
                    AudioPlayerView.this.mNotificationManager.cancel(1);
                    AudioPlayerView.this.notifyDetails.contentView.setTextViewText(R.id.txtNotiDesc, AudioPlayerView.this.mstrTitle);
                    AudioPlayerView.this.mNotificationManager.notify(1, AudioPlayerView.this.notifyDetails);
                }
            }
        }, SystemClock.uptimeMillis() + 1500);
    }

    /* access modifiers changed from: package-private */
    public void initNotification() {
        Intent notifyIntent;
        this.mNotificationManager = (NotificationManager) this.mContext.getSystemService("notification");
        this.notifyDetails = new Notification(R.drawable.god_icon, this.mContext.getString(R.string.app_name), System.currentTimeMillis());
        if (this.mstrScreenId.compareTo("0") == 0) {
            notifyIntent = new Intent(this.mContext, TempleMainActivity.class);
        } else {
            notifyIntent = new Intent(this.mContext, AudioListActivity.class);
        }
        RemoteViews contentView = new RemoteViews(this.mContext.getPackageName(), (int) R.layout.notification_bar_layout);
        this.notifyDetails.contentView = contentView;
        PendingIntent intent = PendingIntent.getActivity(this.mContext, 0, notifyIntent, 67108864);
        this.notifyDetails.contentIntent = intent;
        this.notifyDetails.flags = 2;
        contentView.setOnClickPendingIntent(R.id.imgAppIcon, intent);
    }

    private class MyPhoneStateListener extends PhoneStateListener {
        private MyPhoneStateListener() {
        }

        /* synthetic */ MyPhoneStateListener(AudioPlayerView audioPlayerView, MyPhoneStateListener myPhoneStateListener) {
            this();
        }

        public void onCallStateChanged(int state, String incomingNumber) {
            switch (state) {
                case 0:
                    if (AudioPlayerView.this.blnPasuedByIncommingCall) {
                        AudioPlayerView.this.Start();
                        AudioPlayerView.this.blnPasuedByIncommingCall = false;
                        return;
                    }
                    return;
                case 1:
                    try {
                        if (AudioPlayerView.this.mintAudioPlayerState == AudioPlayerState.STARTED) {
                            AudioPlayerView.this.blnPasuedByIncommingCall = true;
                            AudioPlayerView.this.Pause();
                            return;
                        }
                        return;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                default:
                    return;
            }
            e.printStackTrace();
        }
    }

    public void initializePlayer(int inLayoutResourceId) {
        ((LayoutInflater) this.mContext.getSystemService("layout_inflater")).inflate(inLayoutResourceId, this);
        this.txtElapseTime = (TextView) findViewById(R.id.txtElapseTime);
        this.imgPlay = (ImageView) findViewById(R.id.imgPlay);
        this.imgPause = (ImageView) findViewById(R.id.imgPause);
        this.imgPlayNext = (ImageView) findViewById(R.id.imgPlayNext);
        this.imgPlayPrev = (ImageView) findViewById(R.id.imgPlayPrev);
        this.txtTotalTime = (TextView) findViewById(R.id.txtTotalTime);
        this.txtTitle = (TextView) findViewById(R.id.txtTitle);
        this.txtBuffering = (TextView) findViewById(R.id.txtBuffering);
        this.imgStylus = (ImageView) findViewById(R.id.imgStylus);
        this.layoutBuffering = (LinearLayout) findViewById(R.id.layoutBuffering);
        this.mplstManager = new PlayListManager(this.mContext, StorageUtil.makeFolderInSDCard(this.mContext.getString(R.string.app_name)));
        this.mstrAudioSource = this.mplstManager.getCurrentAudioPath();
        initNotification();
        ((TelephonyManager) this.mContext.getSystemService("phone")).listen(new MyPhoneStateListener(this, null), 32);
        this.txtTitle.setOnTouchListener(new View.OnTouchListener() {
            RelativeLayout.LayoutParams lparamsStylus = null;

            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case 0:
                        int intTocuhX = (int) event.getX();
                        AudioPlayerView.this.handle.removeCallbacks(AudioPlayerView.this.m_taskProgress);
                        this.lparamsStylus = (RelativeLayout.LayoutParams) AudioPlayerView.this.imgStylus.getLayoutParams();
                        return intTocuhX > this.lparamsStylus.leftMargin + -15 && intTocuhX < (this.lparamsStylus.leftMargin + 15) + AudioPlayerView.this.imgStylus.getWidth();
                    case 1:
                        this.lparamsStylus = (RelativeLayout.LayoutParams) AudioPlayerView.this.imgStylus.getLayoutParams();
                        AudioPlayerView.this.mAudioPlayer.seekTo((AudioPlayerView.this.mintDuration * this.lparamsStylus.leftMargin) / AudioPlayerView.this.txtTitle.getWidth());
                        AudioPlayerView.this.m_taskProgress.run();
                        break;
                    case 2:
                        int intStylusMargin = (int) event.getX();
                        this.lparamsStylus = (RelativeLayout.LayoutParams) AudioPlayerView.this.imgStylus.getLayoutParams();
                        this.lparamsStylus.leftMargin = intStylusMargin;
                        AudioPlayerView.this.imgStylus.setLayoutParams(this.lparamsStylus);
                        break;
                }
                return false;
            }
        });
        this.imgPlay.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AudioPlayerView.this.Start();
            }
        });
        this.imgPlayNext.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AudioPlayerView.this.playNextAudio();
            }
        });
        this.imgPlayPrev.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (AudioPlayerView.this.mplstManager != null) {
                    File filAudio = new File(AudioPlayerView.this.mplstManager.getPrevAudioPath());
                    if (filAudio.exists()) {
                        AudioPlayerView.this.Play(filAudio.getAbsolutePath(), filAudio.getName());
                    }
                }
            }
        });
        this.imgPause.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AudioPlayerView.this.Pause();
            }
        });
        this.mAudioPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            public void onPrepared(MediaPlayer mp) {
                AudioPlayerView.this.mintAudioPlayerState = AudioPlayerState.PREPARED;
                AudioPlayerView.this.imgPause.setVisibility(0);
                AudioPlayerView.this.imgPlay.setVisibility(8);
                AudioPlayerView.this.mintDuration = AudioPlayerView.this.mAudioPlayer.getDuration();
                AudioPlayerView.this.txtTotalTime.setText(AudioPlayerView.this.convertToMMSS(AudioPlayerView.this.mintDuration));
                AudioPlayerView.this.txtTitle.setText(AudioPlayerView.this.mstrTitle);
                AudioPlayerView.this.mAudioPlayer.start();
                AudioPlayerView.this.mintAudioPlayerState = AudioPlayerState.STARTED;
                AudioPlayerView.this.showNotification();
                AudioPlayerView.this.handle.post(AudioPlayerView.this.m_taskProgress);
                AudioPlayerView.this.showBufferingStatus(100);
                AudioPlayerView.this.infcAudioPlayerListener.onPlayStarted(AudioPlayerView.this.mstrAudioSource);
            }
        });
        this.mAudioPlayer.setOnBufferingUpdateListener(new MediaPlayer.OnBufferingUpdateListener() {
            public void onBufferingUpdate(MediaPlayer mp, int percent) {
                AudioPlayerView.this.txtBuffering.setText("Buffering...(" + percent + "%) ");
                AudioPlayerView.this.showBufferingStatus(percent);
                if (percent == 100) {
                    AudioPlayerView.this.txtBuffering.setText("");
                }
            }
        });
        this.mAudioPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                AudioPlayerView.this.mintAudioPlayerState = AudioPlayerState.PLAYBACK_COMPLETED;
                if (AudioPlayerView.this.mplstManager == null) {
                    return;
                }
                if (!AudioPlayerView.this.mplstManager.isLastAudio()) {
                    AudioPlayerView.this.playNextAudio();
                    AudioPlayerView.this.infcAudioPlayerListener.onCurrentAudioPlayDone();
                } else if (AudioPlayerView.this.mblnRepeatContinous) {
                    AudioPlayerView.this.playNextAudio();
                } else if (AudioPlayerView.this.mintAudioRepeatIndex < AudioPlayerView.this.mintAudioRepeatCount) {
                    AudioPlayerView audioPlayerView = AudioPlayerView.this;
                    audioPlayerView.mintAudioRepeatIndex = audioPlayerView.mintAudioRepeatIndex + 1;
                    AudioPlayerView.this.playNextAudio();
                } else {
                    AudioPlayerView.this.infcAudioPlayerListener.onPlayDone();
                    AudioPlayerView.this.updatePreogres();
                    AudioPlayerView.this.imgPlay.setVisibility(0);
                    AudioPlayerView.this.imgPause.setVisibility(8);
                }
            }
        });
        this.mAudioPlayer.setOnSeekCompleteListener(new MediaPlayer.OnSeekCompleteListener() {
            public void onSeekComplete(MediaPlayer mp) {
                AudioPlayerView.this.infcAudioPlayerListener.onPositionChanged(AudioPlayerView.this.mAudioPlayer.getCurrentPosition());
            }
        });
        this.mAudioPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            public boolean onError(MediaPlayer mp, int what, int extra) {
                if (AudioPlayerView.this.mstrScreenId.compareTo("1") != 0) {
                    return true;
                }
                AudioPlayerView.this.txtTitle.setText("Network Error");
                AudioPlayerView.this.mAudioPlayer.reset();
                AudioPlayerView.this.mintAudioPlayerState = AudioPlayerState.IDLE;
                return true;
            }
        });
        this.m_taskProgress = new Runnable() {
            public void run() {
                if (AudioPlayerView.this.mintAudioPlayerState == AudioPlayerState.STARTED) {
                    AudioPlayerView.this.updatePreogres();
                    AudioPlayerView.this.handle.postAtTime(this, SystemClock.uptimeMillis() + 1000);
                }
            }
        };
        this.m_taskPreview = new Runnable() {
            public void run() {
                AudioPlayerView.this.Play(AudioPlayerView.this.mstrAudioSource, AudioPlayerView.this.mstrTitle);
            }
        };
    }

    public void enableShuffleOn(boolean flag) {
        if (this.mplstManager != null) {
            this.mplstManager.enableShuffleOn(flag);
        }
    }

    /* access modifiers changed from: private */
    public void playNextAudio() {
        File filAudio = new File(this.mplstManager.getNextAudioPath());
        if (filAudio.exists()) {
            Play(filAudio.getAbsolutePath(), filAudio.getName());
        }
    }

    public AudioPlayerView(Context context) {
        super(context);
        this.mContext = context;
    }

    public AudioPlayerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        this.mstrScreenId = getContext().obtainStyledAttributes(attrs, R.styleable.bys_customviews_audioplayer).getString(0);
        if (this.mstrScreenId.compareTo("0") == 0) {
            initializePlayer(R.layout.temple_audio_player_view_layout);
        } else {
            initializePlayer(R.layout.audio_player_view_layout);
        }
    }

    public void Stop() {
        if (AudioPlayerState.PREPARED == this.mintAudioPlayerState || AudioPlayerState.STARTED == this.mintAudioPlayerState || AudioPlayerState.PAUSED == this.mintAudioPlayerState || AudioPlayerState.STOPPED == this.mintAudioPlayerState || AudioPlayerState.PLAYBACK_COMPLETED == this.mintAudioPlayerState) {
            this.handle.removeCallbacks(this.m_taskProgress);
            this.mAudioPlayer.stop();
            Log.v(this.mstrModuleName, "Playing Stopped");
            this.mintAudioPlayerState = AudioPlayerState.STOPPED;
            hideNotification();
            this.infcAudioPlayerListener.onStop();
        }
    }

    public void Pause() {
        if (AudioPlayerState.STARTED == this.mintAudioPlayerState) {
            this.mAudioPlayer.pause();
            Log.v(this.mstrModuleName, "Playing Paused");
            this.mintAudioPlayerState = AudioPlayerState.PAUSED;
            hideNotification();
            this.handle.removeCallbacks(this.m_taskProgress);
            this.imgPause.setVisibility(8);
            this.imgPlay.setVisibility(0);
        }
    }

    /* access modifiers changed from: private */
    public void Start() {
        if (AudioPlayerState.PREPARED == this.mintAudioPlayerState || AudioPlayerState.PAUSED == this.mintAudioPlayerState || AudioPlayerState.PLAYBACK_COMPLETED == this.mintAudioPlayerState) {
            Log.v(this.mstrModuleName, "Playing from OTHER STATE");
            this.mAudioPlayer.start();
            this.mintAudioPlayerState = AudioPlayerState.STARTED;
            showNotification();
            this.handle.post(this.m_taskProgress);
            this.infcAudioPlayerListener.onPositionChanged(this.mAudioPlayer.getCurrentPosition());
            this.imgPause.setVisibility(0);
            this.imgPlay.setVisibility(8);
        } else if (AudioPlayerState.IDLE == this.mintAudioPlayerState || AudioPlayerState.STOPPED == this.mintAudioPlayerState) {
            Log.v(this.mstrModuleName, "Playing from IDLE STATE");
            Play(this.mstrAudioSource, this.mstrTitle);
        } else {
            Log.v(this.mstrModuleName, "Playing from STATE = " + this.mintAudioPlayerState);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:?, code lost:
        r0.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0039, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x003a, code lost:
        r1.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0043, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0044, code lost:
        r1.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0034, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0039 A[ExcHandler: IllegalArgumentException (r1v2 'e1' java.lang.IllegalArgumentException A[CUSTOM_DECLARE]), Splitter:B:1:0x0008] */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0043 A[ExcHandler: IOException (r1v0 'e1' java.io.IOException A[CUSTOM_DECLARE]), Splitter:B:1:0x0008] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void Play(java.lang.String r5, java.lang.String r6) {
        /*
            r4 = this;
            r4.mstrTitle = r6
            r4.mstrAudioSource = r5
            r2 = 0
            r4.setVisibility(r2)
            android.media.MediaPlayer r2 = r4.mAudioPlayer     // Catch:{ IllegalArgumentException -> 0x0039, IllegalStateException -> 0x003e, IOException -> 0x0043 }
            r2.reset()     // Catch:{ IllegalArgumentException -> 0x0039, IllegalStateException -> 0x003e, IOException -> 0x0043 }
            java.lang.String r2 = r4.mstrScreenId     // Catch:{ IllegalArgumentException -> 0x0039, IllegalStateException -> 0x003e, IOException -> 0x0043 }
            java.lang.String r3 = "1"
            int r2 = r2.compareTo(r3)     // Catch:{ IllegalArgumentException -> 0x0039, IllegalStateException -> 0x003e, IOException -> 0x0043 }
            if (r2 != 0) goto L_0x0025
            android.widget.TextView r2 = r4.txtTitle     // Catch:{ IllegalArgumentException -> 0x0039, IllegalStateException -> 0x003e, IOException -> 0x0043 }
            java.lang.String r3 = "Connecting ..."
            r2.setText(r3)     // Catch:{ IllegalArgumentException -> 0x0039, IllegalStateException -> 0x003e, IOException -> 0x0043 }
            android.widget.TextView r2 = r4.txtBuffering     // Catch:{ IllegalArgumentException -> 0x0039, IllegalStateException -> 0x003e, IOException -> 0x0043 }
            java.lang.String r3 = ""
            r2.setText(r3)     // Catch:{ IllegalArgumentException -> 0x0039, IllegalStateException -> 0x003e, IOException -> 0x0043 }
        L_0x0025:
            android.media.MediaPlayer r2 = r4.mAudioPlayer     // Catch:{ IllegalArgumentException -> 0x0039, IllegalStateException -> 0x003e, IOException -> 0x0043 }
            r2.setDataSource(r5)     // Catch:{ IllegalArgumentException -> 0x0039, IllegalStateException -> 0x003e, IOException -> 0x0043 }
            int r2 = bys.customviews.audioplayer.AudioPlayerView.AudioPlayerState.INITIALIZED     // Catch:{ IllegalArgumentException -> 0x0039, IllegalStateException -> 0x003e, IOException -> 0x0043 }
            r4.mintAudioPlayerState = r2     // Catch:{ IllegalArgumentException -> 0x0039, IllegalStateException -> 0x003e, IOException -> 0x0043 }
            android.media.MediaPlayer r2 = r4.mAudioPlayer     // Catch:{ IllegalStateException -> 0x0034, IllegalArgumentException -> 0x0039, IOException -> 0x0043 }
            r2.prepareAsync()     // Catch:{ IllegalStateException -> 0x0034, IllegalArgumentException -> 0x0039, IOException -> 0x0043 }
        L_0x0033:
            return
        L_0x0034:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ IllegalArgumentException -> 0x0039, IllegalStateException -> 0x003e, IOException -> 0x0043 }
            goto L_0x0033
        L_0x0039:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0033
        L_0x003e:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0033
        L_0x0043:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0033
        */
        throw new UnsupportedOperationException("Method not decompiled: bys.customviews.audioplayer.AudioPlayerView.Play(java.lang.String, java.lang.String):void");
    }

    public boolean IsPlaying() {
        if (this.mintAudioPlayerState == AudioPlayerState.STARTED) {
            return true;
        }
        return false;
    }

    public void Destroy() {
        if (AudioPlayerState.END != this.mintAudioPlayerState) {
            this.mAudioPlayer.stop();
            hideNotification();
            this.mAudioPlayer.release();
            this.mintAudioPlayerState = AudioPlayerState.END;
            this.handle.removeCallbacks(this.m_taskProgress);
            this.handle.removeCallbacks(this.m_taskPreview);
        }
    }

    public int getCurrentTime() {
        return this.mAudioPlayer.getCurrentPosition();
    }

    public void setRepeatOn(boolean blnRepeatContinous, int intAudioRepeatCount) {
        this.mblnRepeatContinous = blnRepeatContinous;
        this.mintAudioRepeatCount = intAudioRepeatCount;
    }

    public boolean isAudioAvaialble() {
        if (this.mplstManager.getAudioListSize() > 0) {
            return true;
        }
        return false;
    }

    public String getCurrentAudioFilePath() {
        return this.mplstManager.getCurrentAudioPath();
    }

    public void setCurrentAudio(String strAudioFullPath) {
        Log.v(this.mstrModuleName, "setCurrentAudio = " + strAudioFullPath);
        this.mplstManager.prapareFreshPlayList();
        this.mplstManager.setCurrentAudioPath(strAudioFullPath);
    }

    public void refreshPlayList() {
        this.mplstManager.prapareFreshPlayList();
    }
}
