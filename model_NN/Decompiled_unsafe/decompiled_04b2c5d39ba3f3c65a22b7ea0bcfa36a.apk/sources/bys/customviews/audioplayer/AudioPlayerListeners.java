package bys.customviews.audioplayer;

public interface AudioPlayerListeners {
    void onCurrentAudioPlayDone();

    void onError(int i);

    void onPause();

    void onPlayDone();

    void onPlayResumed();

    void onPlayStarted(String str);

    void onPositionChanged(int i);

    void onStop();
}
