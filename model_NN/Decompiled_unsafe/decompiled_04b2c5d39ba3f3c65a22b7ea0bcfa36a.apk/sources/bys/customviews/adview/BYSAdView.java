package bys.customviews.adview;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import bys.widgets.srihanuman.R;
import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import com.google.ads.AdView;

public class BYSAdView extends LinearLayout {
    /* access modifiers changed from: private */
    public AdView adMob;
    Activity mActvity = null;
    Context mContext = null;
    Handler mHandler = new Handler();
    Runnable m_taskShowAd = null;
    private boolean mblnDisableBannerAds = false;
    boolean mblnStopAdRequest = false;
    int mintAdLoadCount = 0;
    int mintAdProviderId = 0;
    int mintAdRequestAttempt = 0;
    String mstrAdProviderSequence = "12";

    public BYSAdView(Context context, AttributeSet attrs) {
        super(context, attrs);
        ((LayoutInflater) context.getSystemService("layout_inflater")).inflate((int) R.layout.bys_adview_layout, this);
        this.mContext = context;
        this.adMob = (AdView) findViewById(R.id.adMob);
        this.adMob.setAdListener(new AdListener() {
            public void onReceiveAd(Ad arg0) {
                Log.v("", "BYSAdView : onReceiveAd " + arg0);
                BYSAdView.this.adMob.setVisibility(0);
            }

            public void onPresentScreen(Ad arg0) {
                Log.v("", "BYSAdView : onPresentScreen " + arg0);
            }

            public void onLeaveApplication(Ad arg0) {
                Log.v("", "BYSAdView: onLeaveApplication " + arg0);
            }

            public void onFailedToReceiveAd(Ad arg0, AdRequest.ErrorCode arg1) {
                if (BYSAdView.this.mstrAdProviderSequence.length() > 1 && !BYSAdView.this.mblnStopAdRequest) {
                    BYSAdView.this.mintAdProviderId = 2;
                    BYSAdView.this.mHandler.postAtTime(BYSAdView.this.m_taskShowAd, SystemClock.uptimeMillis() + 5000);
                    Log.v("", "BYSAdView: AsMob: onFailedToReceiveAd " + arg0);
                }
            }

            public void onDismissScreen(Ad arg0) {
                Log.v("", "BYSAdView: onDismissScreen " + arg0);
            }
        });
    }

    public void LoadAd(Activity activity, int intScreen, int delay) {
        this.mActvity = activity;
        if (!this.mblnDisableBannerAds) {
            this.adMob.loadAd(new AdRequest());
        }
    }
}
