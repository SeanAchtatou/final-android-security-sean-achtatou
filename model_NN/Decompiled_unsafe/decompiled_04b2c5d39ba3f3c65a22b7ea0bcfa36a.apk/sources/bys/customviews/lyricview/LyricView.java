package bys.customviews.lyricview;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Handler;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import bys.widgets.srihanuman.R;
import org.codehaus.jackson.util.MinimalPrettyPrinter;

public class LyricView extends LinearLayout {
    private static final long FIXED_SLIDE_SHOW_DURATION = 20000;
    Handler handler = new Handler();
    LyricViewListeners infcLyricViewerListener = null;
    LyricFileReaderUtil lfrUtil = new LyricFileReaderUtil();
    Context mContext = null;
    Runnable m_taskSlideShow = null;
    /* access modifiers changed from: private */
    public boolean mblnShowEnglishText = true;
    /* access modifiers changed from: private */
    public int mintCurrentLyricCount = 0;
    /* access modifiers changed from: private */
    public int mintCurrentLyricIndex;
    private LinearLayout mlayoutLyricViewMain;
    /* access modifiers changed from: private */
    public String mstrCurrentLyricEnglishText;
    private String mstrCurrentLyricLocalText;
    /* access modifiers changed from: private */
    public TextView mtxtTitleEnglish = null;
    /* access modifiers changed from: private */
    public TextView mtxtTitleLocal = null;

    public void setLyricViewerListener(LyricViewListeners infcLyricViewerListener2) {
        this.infcLyricViewerListener = infcLyricViewerListener2;
    }

    public void DestroyLyricView() {
        this.handler.removeCallbacks(this.m_taskSlideShow);
    }

    public LyricView(Context context) {
        super(context);
        this.mContext = context;
        GetPreference();
    }

    /* access modifiers changed from: private */
    public void SavePreference() {
        SharedPreferences.Editor editor = this.mContext.getSharedPreferences(this.mContext.getString(R.string.app_name), 0).edit();
        editor.putBoolean("Show_Text_Lyric", this.mblnShowEnglishText);
        editor.commit();
    }

    private void GetPreference() {
        this.mblnShowEnglishText = this.mContext.getSharedPreferences(this.mContext.getString(R.string.app_name), 0).getBoolean("Show_Text_Lyric", true);
        if (this.mblnShowEnglishText) {
            this.mtxtTitleLocal.setVisibility(8);
            this.mtxtTitleEnglish.setVisibility(0);
            return;
        }
        this.mtxtTitleLocal.setVisibility(0);
        this.mtxtTitleEnglish.setVisibility(8);
    }

    public LyricView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        ((LayoutInflater) this.mContext.getSystemService("layout_inflater")).inflate((int) R.layout.lyric_view_layout, this);
        this.mtxtTitleEnglish = (TextView) findViewById(R.id.txtTitleEnglish);
        this.mtxtTitleLocal = (TextView) findViewById(R.id.txtTitleLocal);
        this.mlayoutLyricViewMain = (LinearLayout) findViewById(R.id.layoutLyricViewMain);
        this.mtxtTitleLocal.setTypeface(Typeface.createFromAsset(context.getAssets(), "hin_glyph.dat"));
        GetPreference();
        this.mtxtTitleLocal.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                LyricView.this.mtxtTitleLocal.setVisibility(8);
                LyricView.this.mtxtTitleEnglish.setVisibility(0);
                if (LyricView.this.mtxtTitleEnglish.getLineCount() > 2) {
                    LyricView.this.mtxtTitleEnglish.setText(LyricView.this.mstrCurrentLyricEnglishText.replace("\n", MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR));
                }
                LyricView.this.mblnShowEnglishText = true;
                LyricView.this.SavePreference();
            }
        });
        this.mtxtTitleEnglish.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                LyricView.this.mtxtTitleLocal.setVisibility(0);
                LyricView.this.mtxtTitleEnglish.setVisibility(8);
                LyricView.this.mblnShowEnglishText = false;
                LyricView.this.SavePreference();
            }
        });
        this.m_taskSlideShow = new Runnable() {
            public void run() {
                LyricView lyricView = LyricView.this;
                lyricView.mintCurrentLyricIndex = lyricView.mintCurrentLyricIndex + 1;
                LyricView.this.infcLyricViewerListener.onLyricPresented(LyricView.this.mintCurrentLyricIndex);
                if (LyricView.this.mintCurrentLyricCount == 0) {
                    LyricView.this.handler.postAtTime(this, SystemClock.uptimeMillis() + LyricView.FIXED_SLIDE_SHOW_DURATION);
                    return;
                }
                LyricView.this.setLyricIndex(LyricView.this.mintCurrentLyricIndex);
                int intLyricDuration = LyricView.this.lfrUtil.getLyricDuration(LyricView.this.mintCurrentLyricIndex);
                if (LyricView.this.mintCurrentLyricIndex < LyricView.this.mintCurrentLyricCount - 1) {
                    LyricView.this.handler.postAtTime(this, SystemClock.uptimeMillis() + ((long) intLyricDuration));
                }
            }
        };
    }

    public int setLyricFile(String strLyricFile) {
        this.mintCurrentLyricCount = this.lfrUtil.readLyricFile(strLyricFile);
        if (this.mintCurrentLyricCount == 0) {
            this.mlayoutLyricViewMain.setBackgroundResource(R.drawable.gold_plate_with_flower);
            this.mtxtTitleEnglish.setText("");
            this.mtxtTitleLocal.setText("");
        } else {
            this.mlayoutLyricViewMain.setBackgroundResource(R.drawable.plate);
        }
        return this.mintCurrentLyricCount;
    }

    public void Destroy() {
        this.handler.removeCallbacks(this.m_taskSlideShow);
    }

    public void Pause() {
        this.handler.removeCallbacks(this.m_taskSlideShow);
    }

    public void setLyricIndex(int index) {
        this.mintCurrentLyricIndex = index;
        if (this.mintCurrentLyricIndex < this.mintCurrentLyricCount) {
            this.mstrCurrentLyricEnglishText = this.lfrUtil.getLyricEnglishText(this.mintCurrentLyricIndex);
            this.mstrCurrentLyricLocalText = this.lfrUtil.getLyricLocalText(this.mintCurrentLyricIndex);
            this.mtxtTitleEnglish.setText(this.mstrCurrentLyricEnglishText);
            if (this.mtxtTitleEnglish.getLineCount() > 2) {
                this.mtxtTitleEnglish.setText(this.mstrCurrentLyricEnglishText.replace("\n", MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR));
            }
            this.mstrCurrentLyricLocalText = this.mstrCurrentLyricLocalText.replace("﻿", "");
            this.mtxtTitleLocal.setText(this.mstrCurrentLyricLocalText);
        }
    }

    public void startLyricSlideShowAt(int iAudioPosition) {
        if (this.mintCurrentLyricCount > 0) {
            this.mintCurrentLyricIndex = this.lfrUtil.getLyricIndexAtAudioPostion(iAudioPosition);
            this.infcLyricViewerListener.onLyricPresented(this.mintCurrentLyricIndex);
            long intLyricEndTime = (long) this.lfrUtil.getLyricEndTime(this.mintCurrentLyricIndex);
            setLyricIndex(this.mintCurrentLyricIndex);
            this.handler.removeCallbacks(this.m_taskSlideShow);
            this.handler.postAtTime(this.m_taskSlideShow, SystemClock.uptimeMillis() + ((long) ((int) (intLyricEndTime - ((long) iAudioPosition)))));
            return;
        }
        this.handler.removeCallbacks(this.m_taskSlideShow);
        this.handler.postAtTime(this.m_taskSlideShow, SystemClock.uptimeMillis() + FIXED_SLIDE_SHOW_DURATION);
    }

    public void setMessage(String string) {
        this.mlayoutLyricViewMain.setBackgroundResource(R.drawable.plate);
        this.mtxtTitleLocal.setVisibility(8);
        this.mtxtTitleEnglish.setVisibility(0);
        this.mtxtTitleEnglish.setText(string);
    }
}
