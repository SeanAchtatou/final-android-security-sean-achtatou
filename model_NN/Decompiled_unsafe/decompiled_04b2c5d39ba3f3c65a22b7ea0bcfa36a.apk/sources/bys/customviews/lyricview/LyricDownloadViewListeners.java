package bys.customviews.lyricview;

public interface LyricDownloadViewListeners {
    void onDownloadDone(String str);

    void onDownloadStarted();

    void onError(String str);

    void onUpdateStatus(String str);
}
