package bys.customviews.lyricview;

public interface LyricViewListeners {
    void onError(int i);

    void onLyricPresented(int i);
}
