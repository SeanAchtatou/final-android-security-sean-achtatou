package bys.customviews.lyricview;

import android.util.Log;
import bys.apps.tools.lyricserializer.SerializerClass;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

public class LyricFileReaderUtil {
    ArrayList<SerializerClass.PartOneItem> mPartOneItemList = null;
    private String mstrLyricFileName = null;

    public int getLyricIndexAtAudioPostion(int iAudioPosition) {
        if (this.mPartOneItemList == null) {
            return -1;
        }
        int intAudioPosition = 0;
        int intLyricIndex = 0;
        while (intLyricIndex < this.mPartOneItemList.size() && (intAudioPosition = intAudioPosition + this.mPartOneItemList.get(intLyricIndex).intDuration) < iAudioPosition) {
            intLyricIndex++;
        }
        return intLyricIndex;
    }

    public int getLyricEndTime(int mintCurrentLyricIndex) {
        int intLyricStartTime = 0;
        if (this.mPartOneItemList == null) {
            return 0;
        }
        for (int i = 0; i <= mintCurrentLyricIndex; i++) {
            if (this.mPartOneItemList.size() > i) {
                intLyricStartTime += this.mPartOneItemList.get(i).intDuration;
            }
        }
        Log.v("Lyric[" + mintCurrentLyricIndex + "]", " Lyric End Time = " + intLyricStartTime);
        return intLyricStartTime;
    }

    public int getLyricDuration(int mintCurrentLyricIndex) {
        if (this.mPartOneItemList == null || mintCurrentLyricIndex >= this.mPartOneItemList.size()) {
            return 0;
        }
        return this.mPartOneItemList.get(mintCurrentLyricIndex).intDuration;
    }

    public String getLyricEnglishText(int mintCurrentLyricIndex) {
        if (this.mPartOneItemList == null || mintCurrentLyricIndex >= this.mPartOneItemList.size()) {
            return null;
        }
        return this.mPartOneItemList.get(mintCurrentLyricIndex).strTextEnglish;
    }

    public String getLyricLocalText(int mintCurrentLyricIndex) {
        if (this.mPartOneItemList == null || mintCurrentLyricIndex >= this.mPartOneItemList.size()) {
            return null;
        }
        return this.mPartOneItemList.get(mintCurrentLyricIndex).strTextLocal;
    }

    private ArrayList<SerializerClass.PartOneItem> readHeader(FileInputStream in) throws IOException {
        byte[] bytSignature = new byte[150];
        Log.v("readHeader", "byteRead = " + in.read(bytSignature, 0, 150));
        try {
            SerializerClass.FileHeader aHeaderAfterDeserialize = (SerializerClass.FileHeader) SerializerClass.deserializeObject(bytSignature);
            if (aHeaderAfterDeserialize.strSignature.compareTo("bys.lyr") != 0) {
                Log.v("LyricFileReader", "Invalid Lyric File");
                return null;
            }
            Log.v("readHeader", "aHeaderAfterDeserialize.intPartOneLength = " + aHeaderAfterDeserialize.intPartOneLength);
            byte[] bytInfoList = new byte[aHeaderAfterDeserialize.intPartOneLength];
            Log.v("readHeader", "byteRead = " + in.read(bytInfoList));
            Log.v("readHeader", "bytSignature = " + bytSignature.length);
            Log.v("readHeader", "bytInfoList = " + bytInfoList.length);
            ArrayList<SerializerClass.PartOneItem> partOne = (ArrayList) SerializerClass.deserializeObject(bytInfoList);
            Log.v("readHeader", "partOne List Size = " + partOne.size());
            return partOne;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public int readLyricFile(String strLyricFile) {
        int intLyricCount = 0;
        FileInputStream in = null;
        this.mstrLyricFileName = strLyricFile;
        try {
            in = new FileInputStream(this.mstrLyricFileName);
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        }
        if (in == null) {
            return 0;
        }
        try {
            this.mPartOneItemList = readHeader(in);
            if (this.mPartOneItemList != null) {
                intLyricCount = this.mPartOneItemList.size();
            } else {
                File f = new File(strLyricFile);
                if (f.exists()) {
                    f.deleteOnExit();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            in.close();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        return intLyricCount;
    }
}
