package bys.customviews.lyricview;

import android.content.Context;
import android.os.Handler;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import bys.widgets.srihanuman.R;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import org.codehaus.jackson.util.MinimalPrettyPrinter;

public class LyricDownloaderView extends LinearLayout {
    FragmentDownloader fragmentDownloader = null;
    /* access modifiers changed from: private */
    public LinearLayout layoutProgress;
    LyricDownloadViewListeners mDownloadViewListener = null;
    Handler mHandler = new Handler();
    /* access modifiers changed from: private */
    public URL mUrl = null;
    Runnable m_taskFileDownloader = null;
    Runnable m_taskPrgressBarUpdater = null;
    Runnable m_taskRetryDownloading = null;
    boolean mblnDownloading = false;
    File mdirDownloadDir = null;
    int mintFragmentCompletedCount = 0;
    int mintFragmentFailedCount = 0;
    int mintRetryCount = 0;
    long mintStartMiliSecond = 0;
    long mintTotalFileSize = 0;
    int mintViewWidth = 0;
    private String mstrFileUrl;
    /* access modifiers changed from: private */
    public String mstrLocalFileName;
    /* access modifiers changed from: private */
    public LinearLayout thisView;

    public LyricDownloaderView(Context context) {
        super(context);
    }

    public LyricDownloaderView(Context context, AttributeSet attrs) {
        super(context, attrs);
        ((LayoutInflater) context.getSystemService("layout_inflater")).inflate((int) R.layout.downloader_view_layout, this);
        this.layoutProgress = (LinearLayout) findViewById(R.id.layoutProgress);
        this.thisView = this;
    }

    /* access modifiers changed from: private */
    public HttpURLConnection EstablishUrlConnection(URL url, long intStartOffset, long intEndOffset) {
        try {
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            try {
                urlConnection.setRequestMethod("GET");
                urlConnection.setDoOutput(true);
                if (intStartOffset >= 0 && intEndOffset > 0) {
                    urlConnection.setRequestProperty("Range", "bytes=" + intStartOffset + "-" + intEndOffset);
                } else if (intStartOffset > 0 && intEndOffset == 0) {
                    urlConnection.setRequestProperty("Range", "bytes=" + intStartOffset + "-");
                }
                try {
                    urlConnection.setConnectTimeout(30000);
                    urlConnection.connect();
                    return urlConnection;
                } catch (IOException e) {
                    Log.e("", "Failed to Connect: StartOffset = " + intStartOffset + ", End Offset" + intEndOffset);
                    e.printStackTrace();
                    return null;
                }
            } catch (ProtocolException e2) {
                Log.e("", "Failed to set Get Request StartOffset = " + intStartOffset + "End Offset" + intEndOffset);
                e2.printStackTrace();
                return null;
            }
        } catch (IOException e3) {
            Log.e("", "Failed to open Connection StartOffset = " + intStartOffset + "End Offset = " + intEndOffset);
            e3.printStackTrace();
            return null;
        }
    }

    public void setOnDownloadDoneListener(LyricDownloadViewListeners listener) {
        this.mDownloadViewListener = listener;
    }

    public void StopDownloading() {
        if (this.fragmentDownloader != null) {
            this.fragmentDownloader.AbortDownnload();
        }
        this.mHandler.removeCallbacks(this.m_taskFileDownloader);
        this.mHandler.removeCallbacks(this.m_taskRetryDownloading);
        this.mHandler.removeCallbacks(this.m_taskPrgressBarUpdater);
    }

    public void SetLocalFile(File dirAppDirectory, String strLocalFileName) {
        this.mstrLocalFileName = strLocalFileName;
        Log.i("Local filename:", this.mstrLocalFileName);
        this.mdirDownloadDir = dirAppDirectory;
    }

    public boolean IsDownloading() {
        return this.mblnDownloading;
    }

    public void StartDownloading(String strFileUrl) {
        this.mstrFileUrl = strFileUrl;
        this.mblnDownloading = true;
        Log.i("", "Downloading From: " + strFileUrl);
        this.m_taskFileDownloader = new Runnable() {
            public void run() {
                LyricDownloaderView.this.mintStartMiliSecond = SystemClock.uptimeMillis();
                LyricDownloaderView.this.StartFragmnetsDownload();
            }
        };
        new Thread(this.m_taskFileDownloader).start();
    }

    private boolean startDownloadSetup() {
        try {
            this.mDownloadViewListener.onUpdateStatus("Connecting...");
            this.mUrl = new URL(this.mstrFileUrl);
            HttpURLConnection urlConnection = EstablishUrlConnection(this.mUrl, 0, 0);
            Log.i("", "Connection Established: " + urlConnection);
            this.mintTotalFileSize = 0;
            if (urlConnection != null) {
                this.mintTotalFileSize = (long) urlConnection.getContentLength();
                if (this.mintTotalFileSize == -1) {
                    this.mDownloadViewListener.onError("Weak / No Internet connection");
                    return false;
                } else if (this.mintTotalFileSize < 500) {
                    this.mDownloadViewListener.onError("File not accessible");
                    return false;
                } else {
                    urlConnection.disconnect();
                    if (this.mDownloadViewListener != null) {
                        this.mDownloadViewListener.onDownloadStarted();
                    }
                    Log.i("", "Total Download Size = " + this.mintTotalFileSize);
                    return true;
                }
            } else {
                this.mDownloadViewListener.onError("Failed to connect");
                return false;
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
            if (this.mDownloadViewListener != null) {
                this.mDownloadViewListener.onError("Invalid URL");
            }
            return false;
        }
    }

    /* access modifiers changed from: private */
    public void StartFragmnetsDownload() {
        this.mintFragmentCompletedCount = 0;
        this.mintFragmentFailedCount = 0;
        this.mintRetryCount = 0;
        if (!startDownloadSetup()) {
            this.mblnDownloading = false;
            return;
        }
        this.m_taskPrgressBarUpdater = new Runnable() {
            public void run() {
                long lngDownloadedSize = LyricDownloaderView.this.fragmentDownloader.getFragmentCurrentSize();
                LinearLayout.LayoutParams lparamsPot = (LinearLayout.LayoutParams) LyricDownloaderView.this.layoutProgress.getLayoutParams();
                if (LyricDownloaderView.this.mintTotalFileSize != 0) {
                    lparamsPot.width = (int) (((float) LyricDownloaderView.this.thisView.getWidth()) * (((float) lngDownloadedSize) / ((float) LyricDownloaderView.this.mintTotalFileSize)));
                    LyricDownloaderView.this.layoutProgress.setLayoutParams(lparamsPot);
                }
                LyricDownloaderView.this.mHandler.postAtTime(this, SystemClock.uptimeMillis() + 750);
            }
        };
        long lngFragmentedFileCheckSize = this.mintTotalFileSize;
        this.fragmentDownloader = new FragmentDownloader();
        this.fragmentDownloader.setCalculatedFragmentSize(lngFragmentedFileCheckSize);
        this.fragmentDownloader.StartDownnload();
    }

    private class FragmentDownloader {
        boolean blnStop;
        File file;
        FileOutputStream fileOutput;
        InputStream inputStream;
        long lngFragmentCurrentSize;
        long lngFragmentPredownloadedSize;
        long lngFragmentTotalSize;
        Runnable m_taskFileFragmentDownloader;
        HttpURLConnection urlConnection;

        public synchronized long getFragmentCurrentSize() {
            return this.lngFragmentCurrentSize + this.lngFragmentPredownloadedSize;
        }

        public void setCalculatedFragmentSize(long lngSize) {
            this.lngFragmentTotalSize = lngSize;
        }

        public synchronized void addToFragmentCurrentSize(long alngFragmentCurrentSize) {
            this.lngFragmentCurrentSize += alngFragmentCurrentSize;
        }

        public FragmentDownloader() {
            this.urlConnection = null;
            this.file = null;
            this.blnStop = false;
            this.fileOutput = null;
            this.inputStream = null;
            this.m_taskFileFragmentDownloader = null;
            this.lngFragmentPredownloadedSize = 0;
            this.lngFragmentCurrentSize = 0;
            this.lngFragmentTotalSize = 0;
            this.lngFragmentCurrentSize = 0;
            this.lngFragmentPredownloadedSize = 0;
            this.file = new File(LyricDownloaderView.this.mdirDownloadDir, String.valueOf(LyricDownloaderView.this.mstrLocalFileName) + ".tmp");
            if (this.file.exists()) {
                this.lngFragmentPredownloadedSize = this.file.length();
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
         arg types: [java.io.File, int]
         candidates:
          ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
          ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
        public void StartDownnload() {
            if (this.lngFragmentTotalSize < this.lngFragmentPredownloadedSize) {
                Log.e("Fatal Error: 1", "Something wrong with the download. Deleting this fragment");
                this.file.delete();
                LyricDownloaderView.this.mDownloadViewListener.onError("Fator Error");
            } else if (this.lngFragmentTotalSize == this.lngFragmentPredownloadedSize) {
                Log.v("Fragment already downloaded", "File already downloaded " + this.lngFragmentPredownloadedSize);
                LyricDownloaderView.this.mDownloadViewListener.onDownloadDone(this.file.getAbsolutePath());
            } else {
                this.urlConnection = LyricDownloaderView.this.EstablishUrlConnection(LyricDownloaderView.this.mUrl, this.lngFragmentPredownloadedSize, 0);
                if (this.urlConnection == null) {
                    LyricDownloaderView.this.mDownloadViewListener.onError("Connection Failure");
                } else if (this.lngFragmentTotalSize != this.lngFragmentPredownloadedSize + ((long) this.urlConnection.getContentLength())) {
                    Log.v("Fatal Error: 2", "Something wrong with the download. Deleting this fragment");
                    this.file.delete();
                    LyricDownloaderView.this.mDownloadViewListener.onError("FatalError");
                } else {
                    try {
                        this.inputStream = this.urlConnection.getInputStream();
                        try {
                            this.fileOutput = new FileOutputStream(this.file, true);
                            this.m_taskFileFragmentDownloader = new Runnable() {
                                public void run() {
                                    Log.v("Fragment Downloader ", "Calling.. StartDownloadThread ");
                                    FragmentDownloader.this.StartDownloadThread();
                                }
                            };
                            new Thread(this.m_taskFileFragmentDownloader).start();
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                            LyricDownloaderView.this.mDownloadViewListener.onError("Connection Failure");
                        }
                    } catch (IOException e1) {
                        e1.printStackTrace();
                        LyricDownloaderView.this.mDownloadViewListener.onError("Connection Failure");
                    }
                }
            }
        }

        /* access modifiers changed from: private */
        public void StartDownloadThread() {
            this.blnStop = false;
            byte[] buffer = new byte[1024];
            LyricDownloaderView.this.mHandler.postAtTime(LyricDownloaderView.this.m_taskPrgressBarUpdater, SystemClock.uptimeMillis() + 500);
            while (true) {
                try {
                    int bufferLength = this.inputStream.read(buffer);
                    if (bufferLength <= 0 || this.blnStop) {
                        this.fileOutput.flush();
                        this.fileOutput.close();
                    } else {
                        this.fileOutput.write(buffer, 0, bufferLength);
                        addToFragmentCurrentSize((long) bufferLength);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    LyricDownloaderView.this.mHandler.removeCallbacks(LyricDownloaderView.this.m_taskPrgressBarUpdater);
                    return;
                }
            }
            this.fileOutput.flush();
            this.fileOutput.close();
            LyricDownloaderView.this.mHandler.removeCallbacks(LyricDownloaderView.this.m_taskPrgressBarUpdater);
            if (!this.blnStop) {
                Log.v("Download done ", MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + this.lngFragmentCurrentSize);
                if (this.lngFragmentTotalSize == this.lngFragmentCurrentSize + this.lngFragmentPredownloadedSize) {
                    File renameFile = new File(this.file.getAbsolutePath().replace(".tmp", ""));
                    this.file.renameTo(renameFile);
                    LyricDownloaderView.this.mDownloadViewListener.onDownloadDone(renameFile.getAbsolutePath());
                } else if (this.lngFragmentTotalSize < this.lngFragmentCurrentSize + this.lngFragmentPredownloadedSize) {
                    Log.v("Incomplete Download ", "Download Stopped in middle");
                    LyricDownloaderView.this.mDownloadViewListener.onError("Connection Failure");
                } else {
                    Log.v("Fatal Error: 3", "Something wrong with the download. Deleting this fragment");
                    this.file.delete();
                    LyricDownloaderView.this.mDownloadViewListener.onError("FatalError");
                }
            }
        }

        public void AbortDownnload() {
            Log.v("Fragment Downloader ", "StopDownload");
            this.blnStop = true;
        }
    }
}
