package bys.customviews.aartiview;

import android.content.Context;
import android.os.Handler;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import bys.customviews.candleflameview.CandleFlameView;
import bys.widgets.srihanuman.R;

public class AartiView extends RelativeLayout {
    float fltCosTheta = 0.0f;
    float fltSinTheta = 0.0f;
    Handler handle = new Handler();
    ImageView imgDiyaStand = null;
    Context mContext = null;
    Runnable m_taskAnimation = null;
    boolean mblnCLockWise = true;
    boolean mblnStopAnimation = true;
    int mintAartiCenterBottomMargin = 55;
    int mintAartiCenterLeftMargin = 0;
    int mintAartiCount = 0;
    int mintAartiRadius = 75;
    int mintDiyaHeight = 85;
    int mintDiyaWidth = 50;
    int mintInitialBottomMargin = 100;
    int mintInitialLeftMargin = 0;
    int mintTheta = 0;
    CandleFlameView viewDiya = null;

    private int getPixelFromDip(float dips) {
        return Math.round(dips * this.mContext.getResources().getDisplayMetrics().density);
    }

    private void initializeParameters() {
        this.mintAartiCenterBottomMargin = getPixelFromDip(55.0f);
        this.mintInitialBottomMargin = getPixelFromDip(100.0f);
        this.mintDiyaWidth = getPixelFromDip(50.0f);
        this.mintDiyaHeight = getPixelFromDip(85.0f);
        this.mintAartiRadius = getPixelFromDip(75.0f);
    }

    private void createAartiView(Context context) {
        this.mContext = context;
        this.viewDiya = new CandleFlameView(this.mContext);
        this.imgDiyaStand = new ImageView(this.mContext);
        this.imgDiyaStand.setBackgroundResource(R.drawable.dia_stand);
        initializeParameters();
        addView(this.imgDiyaStand);
        addView(this.viewDiya);
        RelativeLayout.LayoutParams lparamsDiyaStand = (RelativeLayout.LayoutParams) this.imgDiyaStand.getLayoutParams();
        lparamsDiyaStand.height = getPixelFromDip(100.0f);
        lparamsDiyaStand.width = getPixelFromDip(56.0f);
        lparamsDiyaStand.addRule(12);
        this.imgDiyaStand.setLayoutParams(lparamsDiyaStand);
        RelativeLayout.LayoutParams lparamsDiya = (RelativeLayout.LayoutParams) this.viewDiya.getLayoutParams();
        lparamsDiya.height = this.mintDiyaHeight;
        lparamsDiya.width = this.mintDiyaWidth;
        lparamsDiya.bottomMargin = this.mintInitialBottomMargin;
        lparamsDiya.addRule(12);
        this.viewDiya.setLayoutParams(lparamsDiya);
        this.viewDiya.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (AartiView.this.mblnStopAnimation) {
                    AartiView.this.StartAartiAnimation();
                } else {
                    AartiView.this.StopAartiAnimation();
                }
            }
        });
        this.imgDiyaStand.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (AartiView.this.mblnStopAnimation) {
                    AartiView.this.StartAartiAnimation();
                } else {
                    AartiView.this.StopAartiAnimation();
                }
            }
        });
    }

    public AartiView(Context context) {
        super(context);
        createAartiView(context);
    }

    public AartiView(Context context, AttributeSet attrs) {
        super(context, attrs);
        createAartiView(context);
    }

    public void Initialize(Context context) {
        this.mintAartiCenterLeftMargin = (((WindowManager) context.getSystemService("window")).getDefaultDisplay().getWidth() - this.mintDiyaWidth) / 2;
        this.m_taskAnimation = new Runnable() {
            public void run() {
                RelativeLayout.LayoutParams lparamsDiya = (RelativeLayout.LayoutParams) AartiView.this.viewDiya.getLayoutParams();
                AartiView.this.fltCosTheta = (float) Math.cos((((double) AartiView.this.mintTheta) * 3.14d) / 180.0d);
                AartiView.this.fltSinTheta = (float) Math.sin((((double) AartiView.this.mintTheta) * 3.14d) / 180.0d);
                lparamsDiya.leftMargin = AartiView.this.mintAartiCenterLeftMargin + ((int) (((float) AartiView.this.mintAartiRadius) * AartiView.this.fltCosTheta));
                lparamsDiya.bottomMargin = AartiView.this.mintAartiCenterBottomMargin + ((int) (((float) AartiView.this.mintAartiRadius) * AartiView.this.fltSinTheta));
                AartiView.this.viewDiya.setLayoutParams(lparamsDiya);
                AartiView aartiView = AartiView.this;
                aartiView.mintTheta -= 6;
                if (AartiView.this.mintTheta < -360) {
                    AartiView.this.mintTheta = 0;
                }
                if (!AartiView.this.mblnStopAnimation) {
                    AartiView.this.handle.postAtTime(this, SystemClock.uptimeMillis() + 100);
                    return;
                }
                lparamsDiya.leftMargin = AartiView.this.mintInitialLeftMargin;
                lparamsDiya.bottomMargin = AartiView.this.mintInitialBottomMargin;
                AartiView.this.viewDiya.setLayoutParams(lparamsDiya);
            }
        };
        this.viewDiya.Initialize(context);
        this.viewDiya.StartFlameAnimation();
    }

    public void StartAartiAnimation() {
        this.mblnStopAnimation = false;
        if (this.handle != null) {
            this.mintTheta = 180;
            this.handle.removeCallbacks(this.m_taskAnimation);
            this.handle.post(this.m_taskAnimation);
        }
    }

    public void StopAartiAnimation() {
        this.mblnStopAnimation = true;
    }

    public void SuspendArtiView() {
        StopAartiAnimation();
        this.viewDiya.StopFlameAnimation();
    }

    public void ResumeArtiView() {
        this.viewDiya.StartFlameAnimation();
    }
}
