package android.support.v4.media.session;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.media.session.PlaybackStateCompat;

final class e8kxjqktk9t implements Parcelable.Creator {
    e8kxjqktk9t() {
    }

    /* renamed from: ttmhx7 */
    public PlaybackStateCompat.CustomAction createFromParcel(Parcel parcel) {
        return new PlaybackStateCompat.CustomAction(parcel, null);
    }

    /* renamed from: ttmhx7 */
    public PlaybackStateCompat.CustomAction[] newArray(int i) {
        return new PlaybackStateCompat.CustomAction[i];
    }
}
