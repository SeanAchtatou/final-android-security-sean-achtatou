package android.support.v4.c;

import java.util.Collection;
import java.util.Iterator;

final class iux03f6yieb implements Collection {
    final /* synthetic */ fxug2rdnfo ttmhx7;

    iux03f6yieb(fxug2rdnfo fxug2rdnfo) {
        this.ttmhx7 = fxug2rdnfo;
    }

    public boolean add(Object obj) {
        throw new UnsupportedOperationException();
    }

    public boolean addAll(Collection collection) {
        throw new UnsupportedOperationException();
    }

    public void clear() {
        this.ttmhx7.cehyzt7dw();
    }

    public boolean contains(Object obj) {
        return this.ttmhx7.ozpoxuz523b2(obj) >= 0;
    }

    public boolean containsAll(Collection collection) {
        for (Object contains : collection) {
            if (!contains(contains)) {
                return false;
            }
        }
        return true;
    }

    public boolean isEmpty() {
        return this.ttmhx7.ttmhx7() == 0;
    }

    public Iterator iterator() {
        return new e8kxjqktk9t(this.ttmhx7, 1);
    }

    public boolean remove(Object obj) {
        int ozpoxuz523b2 = this.ttmhx7.ozpoxuz523b2(obj);
        if (ozpoxuz523b2 < 0) {
            return false;
        }
        this.ttmhx7.ttmhx7(ozpoxuz523b2);
        return true;
    }

    public boolean removeAll(Collection collection) {
        int i = 0;
        int ttmhx72 = this.ttmhx7.ttmhx7();
        boolean z = false;
        while (i < ttmhx72) {
            if (collection.contains(this.ttmhx7.ttmhx7(i, 1))) {
                this.ttmhx7.ttmhx7(i);
                i--;
                ttmhx72--;
                z = true;
            }
            i++;
        }
        return z;
    }

    public boolean retainAll(Collection collection) {
        int i = 0;
        int ttmhx72 = this.ttmhx7.ttmhx7();
        boolean z = false;
        while (i < ttmhx72) {
            if (!collection.contains(this.ttmhx7.ttmhx7(i, 1))) {
                this.ttmhx7.ttmhx7(i);
                i--;
                ttmhx72--;
                z = true;
            }
            i++;
        }
        return z;
    }

    public int size() {
        return this.ttmhx7.ttmhx7();
    }

    public Object[] toArray() {
        return this.ttmhx7.ozpoxuz523b2(1);
    }

    public Object[] toArray(Object[] objArr) {
        return this.ttmhx7.ttmhx7(objArr, 1);
    }
}
