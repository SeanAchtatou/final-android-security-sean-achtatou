package android.support.v4.app;

import android.os.Parcel;
import android.os.Parcelable;

final class ol99ycz2wbkd implements Parcelable.Creator {
    ol99ycz2wbkd() {
    }

    /* renamed from: ttmhx7 */
    public FragmentManagerState createFromParcel(Parcel parcel) {
        return new FragmentManagerState(parcel);
    }

    /* renamed from: ttmhx7 */
    public FragmentManagerState[] newArray(int i) {
        return new FragmentManagerState[i];
    }
}
