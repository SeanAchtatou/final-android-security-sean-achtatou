package android.support.v4.app;

import android.os.Parcel;
import android.os.Parcelable;

final class FragmentManagerState implements Parcelable {
    public static final Parcelable.Creator CREATOR = new ol99ycz2wbkd();
    BackStackState[] cehyzt7dw;
    int[] ozpoxuz523b2;
    FragmentState[] ttmhx7;

    public FragmentManagerState() {
    }

    public FragmentManagerState(Parcel parcel) {
        this.ttmhx7 = (FragmentState[]) parcel.createTypedArray(FragmentState.CREATOR);
        this.ozpoxuz523b2 = parcel.createIntArray();
        this.cehyzt7dw = (BackStackState[]) parcel.createTypedArray(BackStackState.CREATOR);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedArray(this.ttmhx7, i);
        parcel.writeIntArray(this.ozpoxuz523b2);
        parcel.writeTypedArray(this.cehyzt7dw, i);
    }
}
