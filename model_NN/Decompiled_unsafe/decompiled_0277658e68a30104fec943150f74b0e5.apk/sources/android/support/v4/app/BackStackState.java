package android.support.v4.app;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;
import java.util.ArrayList;

final class BackStackState implements Parcelable {
    public static final Parcelable.Creator CREATOR = new b5zlaptmyxarl();
    final ArrayList b5zlaptmyxarl;
    final int cehyzt7dw;
    final CharSequence e8kxjqktk9t;
    final CharSequence ef5tn1cvshg414;
    final int fxug2rdnfo;
    final ArrayList iux03f6yieb;
    final int lg71ytkvzw;
    final int ozpoxuz523b2;
    final int[] ttmhx7;
    final String uin6g3d5rqgcbs;
    final int usuayu88rw4;

    public BackStackState(Parcel parcel) {
        this.ttmhx7 = parcel.createIntArray();
        this.ozpoxuz523b2 = parcel.readInt();
        this.cehyzt7dw = parcel.readInt();
        this.uin6g3d5rqgcbs = parcel.readString();
        this.usuayu88rw4 = parcel.readInt();
        this.fxug2rdnfo = parcel.readInt();
        this.e8kxjqktk9t = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.lg71ytkvzw = parcel.readInt();
        this.ef5tn1cvshg414 = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.b5zlaptmyxarl = parcel.createStringArrayList();
        this.iux03f6yieb = parcel.createStringArrayList();
    }

    public BackStackState(rulrdod1midre rulrdod1midre, uin6g3d5rqgcbs uin6g3d5rqgcbs2) {
        int i = 0;
        for (lg71ytkvzw lg71ytkvzw2 = uin6g3d5rqgcbs2.ozpoxuz523b2; lg71ytkvzw2 != null; lg71ytkvzw2 = lg71ytkvzw2.ttmhx7) {
            if (lg71ytkvzw2.ef5tn1cvshg414 != null) {
                i += lg71ytkvzw2.ef5tn1cvshg414.size();
            }
        }
        this.ttmhx7 = new int[(i + (uin6g3d5rqgcbs2.uin6g3d5rqgcbs * 7))];
        if (!uin6g3d5rqgcbs2.iux03f6yieb) {
            throw new IllegalStateException("Not on back stack");
        }
        int i2 = 0;
        for (lg71ytkvzw lg71ytkvzw3 = uin6g3d5rqgcbs2.ozpoxuz523b2; lg71ytkvzw3 != null; lg71ytkvzw3 = lg71ytkvzw3.ttmhx7) {
            int i3 = i2 + 1;
            this.ttmhx7[i2] = lg71ytkvzw3.cehyzt7dw;
            int i4 = i3 + 1;
            this.ttmhx7[i3] = lg71ytkvzw3.uin6g3d5rqgcbs != null ? lg71ytkvzw3.uin6g3d5rqgcbs.e8kxjqktk9t : -1;
            int i5 = i4 + 1;
            this.ttmhx7[i4] = lg71ytkvzw3.usuayu88rw4;
            int i6 = i5 + 1;
            this.ttmhx7[i5] = lg71ytkvzw3.fxug2rdnfo;
            int i7 = i6 + 1;
            this.ttmhx7[i6] = lg71ytkvzw3.e8kxjqktk9t;
            int i8 = i7 + 1;
            this.ttmhx7[i7] = lg71ytkvzw3.lg71ytkvzw;
            if (lg71ytkvzw3.ef5tn1cvshg414 != null) {
                int size = lg71ytkvzw3.ef5tn1cvshg414.size();
                int i9 = i8 + 1;
                this.ttmhx7[i8] = size;
                int i10 = 0;
                while (i10 < size) {
                    this.ttmhx7[i9] = ((Fragment) lg71ytkvzw3.ef5tn1cvshg414.get(i10)).e8kxjqktk9t;
                    i10++;
                    i9++;
                }
                i2 = i9;
            } else {
                i2 = i8 + 1;
                this.ttmhx7[i8] = 0;
            }
        }
        this.ozpoxuz523b2 = uin6g3d5rqgcbs2.ef5tn1cvshg414;
        this.cehyzt7dw = uin6g3d5rqgcbs2.b5zlaptmyxarl;
        this.uin6g3d5rqgcbs = uin6g3d5rqgcbs2.mhtc4dliin7r;
        this.usuayu88rw4 = uin6g3d5rqgcbs2.bpogj6;
        this.fxug2rdnfo = uin6g3d5rqgcbs2.ca2ssr26fefu;
        this.e8kxjqktk9t = uin6g3d5rqgcbs2.flawb66z00q;
        this.lg71ytkvzw = uin6g3d5rqgcbs2.k3jokks5k5;
        this.ef5tn1cvshg414 = uin6g3d5rqgcbs2.rulrdod1midre;
        this.b5zlaptmyxarl = uin6g3d5rqgcbs2.cpgyvt8o4r3;
        this.iux03f6yieb = uin6g3d5rqgcbs2.mqnmk83l0o;
    }

    public int describeContents() {
        return 0;
    }

    public uin6g3d5rqgcbs ttmhx7(rulrdod1midre rulrdod1midre) {
        uin6g3d5rqgcbs uin6g3d5rqgcbs2 = new uin6g3d5rqgcbs(rulrdod1midre);
        int i = 0;
        int i2 = 0;
        while (i2 < this.ttmhx7.length) {
            lg71ytkvzw lg71ytkvzw2 = new lg71ytkvzw();
            int i3 = i2 + 1;
            lg71ytkvzw2.cehyzt7dw = this.ttmhx7[i2];
            if (rulrdod1midre.ttmhx7) {
                Log.v("FragmentManager", "Instantiate " + uin6g3d5rqgcbs2 + " op #" + i + " base fragment #" + this.ttmhx7[i3]);
            }
            int i4 = i3 + 1;
            int i5 = this.ttmhx7[i3];
            if (i5 >= 0) {
                lg71ytkvzw2.uin6g3d5rqgcbs = (Fragment) rulrdod1midre.fxug2rdnfo.get(i5);
            } else {
                lg71ytkvzw2.uin6g3d5rqgcbs = null;
            }
            int i6 = i4 + 1;
            lg71ytkvzw2.usuayu88rw4 = this.ttmhx7[i4];
            int i7 = i6 + 1;
            lg71ytkvzw2.fxug2rdnfo = this.ttmhx7[i6];
            int i8 = i7 + 1;
            lg71ytkvzw2.e8kxjqktk9t = this.ttmhx7[i7];
            int i9 = i8 + 1;
            lg71ytkvzw2.lg71ytkvzw = this.ttmhx7[i8];
            int i10 = i9 + 1;
            int i11 = this.ttmhx7[i9];
            if (i11 > 0) {
                lg71ytkvzw2.ef5tn1cvshg414 = new ArrayList(i11);
                int i12 = 0;
                while (i12 < i11) {
                    if (rulrdod1midre.ttmhx7) {
                        Log.v("FragmentManager", "Instantiate " + uin6g3d5rqgcbs2 + " set remove fragment #" + this.ttmhx7[i10]);
                    }
                    lg71ytkvzw2.ef5tn1cvshg414.add((Fragment) rulrdod1midre.fxug2rdnfo.get(this.ttmhx7[i10]));
                    i12++;
                    i10++;
                }
            }
            uin6g3d5rqgcbs2.ttmhx7(lg71ytkvzw2);
            i++;
            i2 = i10;
        }
        uin6g3d5rqgcbs2.ef5tn1cvshg414 = this.ozpoxuz523b2;
        uin6g3d5rqgcbs2.b5zlaptmyxarl = this.cehyzt7dw;
        uin6g3d5rqgcbs2.mhtc4dliin7r = this.uin6g3d5rqgcbs;
        uin6g3d5rqgcbs2.bpogj6 = this.usuayu88rw4;
        uin6g3d5rqgcbs2.iux03f6yieb = true;
        uin6g3d5rqgcbs2.ca2ssr26fefu = this.fxug2rdnfo;
        uin6g3d5rqgcbs2.flawb66z00q = this.e8kxjqktk9t;
        uin6g3d5rqgcbs2.k3jokks5k5 = this.lg71ytkvzw;
        uin6g3d5rqgcbs2.rulrdod1midre = this.ef5tn1cvshg414;
        uin6g3d5rqgcbs2.cpgyvt8o4r3 = this.b5zlaptmyxarl;
        uin6g3d5rqgcbs2.mqnmk83l0o = this.iux03f6yieb;
        uin6g3d5rqgcbs2.ttmhx7(1);
        return uin6g3d5rqgcbs2;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeIntArray(this.ttmhx7);
        parcel.writeInt(this.ozpoxuz523b2);
        parcel.writeInt(this.cehyzt7dw);
        parcel.writeString(this.uin6g3d5rqgcbs);
        parcel.writeInt(this.usuayu88rw4);
        parcel.writeInt(this.fxug2rdnfo);
        TextUtils.writeToParcel(this.e8kxjqktk9t, parcel, 0);
        parcel.writeInt(this.lg71ytkvzw);
        TextUtils.writeToParcel(this.ef5tn1cvshg414, parcel, 0);
        parcel.writeStringList(this.b5zlaptmyxarl);
        parcel.writeStringList(this.iux03f6yieb);
    }
}
