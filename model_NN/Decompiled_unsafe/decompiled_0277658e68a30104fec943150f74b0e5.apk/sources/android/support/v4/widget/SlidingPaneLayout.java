package android.support.v4.widget;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.view.ijavw7l1x;
import android.support.v4.view.rulrdod1midre;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;

public class SlidingPaneLayout extends ViewGroup {
    static final lqwegpi5 ttmhx7;
    private boolean ay6ebym1yp0qgk;
    private float b5zlaptmyxarl;
    private float bpogj6;
    private cpgyvt8o4r3 ca2ssr26fefu;
    private int cehyzt7dw;
    private final Rect cpgyvt8o4r3;
    private boolean e8kxjqktk9t;
    private float ef5tn1cvshg414;
    private final aecbla89ntoa8 flawb66z00q;
    private final int fxug2rdnfo;
    private int iux03f6yieb;
    private boolean k3jokks5k5;
    private View lg71ytkvzw;
    private int mhtc4dliin7r;
    /* access modifiers changed from: private */
    public final ArrayList mqnmk83l0o;
    private float oc9mgl157cp;
    private int ozpoxuz523b2;
    private boolean rulrdod1midre;
    private Drawable uin6g3d5rqgcbs;
    private Drawable usuayu88rw4;

    class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator CREATOR = new mqnmk83l0o();
        boolean ttmhx7;

        private SavedState(Parcel parcel) {
            super(parcel);
            this.ttmhx7 = parcel.readInt() != 0;
        }

        SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.ttmhx7 ? 1 : 0);
        }
    }

    static {
        int i = Build.VERSION.SDK_INT;
        if (i >= 17) {
            ttmhx7 = new sgnd7s4();
        } else if (i >= 16) {
            ttmhx7 = new eyli1ymagd3o();
        } else {
            ttmhx7 = new ol99ycz2wbkd();
        }
    }

    private static boolean cehyzt7dw(View view) {
        if (ijavw7l1x.usuayu88rw4(view)) {
            return true;
        }
        if (Build.VERSION.SDK_INT >= 18) {
            return false;
        }
        Drawable background = view.getBackground();
        if (background != null) {
            return background.getOpacity() == -1;
        }
        return false;
    }

    private boolean fxug2rdnfo() {
        return ijavw7l1x.uin6g3d5rqgcbs(this) == 1;
    }

    private boolean ozpoxuz523b2(View view, int i) {
        if (!this.rulrdod1midre && !ttmhx7(1.0f, i)) {
            return false;
        }
        this.k3jokks5k5 = true;
        return true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:8:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void ttmhx7(float r10) {
        /*
            r9 = this;
            r1 = 0
            r8 = 1065353216(0x3f800000, float:1.0)
            boolean r3 = r9.fxug2rdnfo()
            android.view.View r0 = r9.lg71ytkvzw
            android.view.ViewGroup$LayoutParams r0 = r0.getLayoutParams()
            android.support.v4.widget.rulrdod1midre r0 = (android.support.v4.widget.rulrdod1midre) r0
            boolean r2 = r0.cehyzt7dw
            if (r2 == 0) goto L_0x0030
            if (r3 == 0) goto L_0x002d
            int r0 = r0.rightMargin
        L_0x0017:
            if (r0 > 0) goto L_0x0030
            r0 = 1
        L_0x001a:
            int r4 = r9.getChildCount()
            r2 = r1
        L_0x001f:
            if (r2 >= r4) goto L_0x005d
            android.view.View r5 = r9.getChildAt(r2)
            android.view.View r1 = r9.lg71ytkvzw
            if (r5 != r1) goto L_0x0032
        L_0x0029:
            int r1 = r2 + 1
            r2 = r1
            goto L_0x001f
        L_0x002d:
            int r0 = r0.leftMargin
            goto L_0x0017
        L_0x0030:
            r0 = r1
            goto L_0x001a
        L_0x0032:
            float r1 = r9.b5zlaptmyxarl
            float r1 = r8 - r1
            int r6 = r9.mhtc4dliin7r
            float r6 = (float) r6
            float r1 = r1 * r6
            int r1 = (int) r1
            r9.b5zlaptmyxarl = r10
            float r6 = r8 - r10
            int r7 = r9.mhtc4dliin7r
            float r7 = (float) r7
            float r6 = r6 * r7
            int r6 = (int) r6
            int r1 = r1 - r6
            if (r3 == 0) goto L_0x0048
            int r1 = -r1
        L_0x0048:
            r5.offsetLeftAndRight(r1)
            if (r0 == 0) goto L_0x0029
            if (r3 == 0) goto L_0x0058
            float r1 = r9.b5zlaptmyxarl
            float r1 = r1 - r8
        L_0x0052:
            int r6 = r9.cehyzt7dw
            r9.ttmhx7(r5, r1, r6)
            goto L_0x0029
        L_0x0058:
            float r1 = r9.b5zlaptmyxarl
            float r1 = r8 - r1
            goto L_0x0052
        L_0x005d:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.widget.SlidingPaneLayout.ttmhx7(float):void");
    }

    private void ttmhx7(View view, float f, int i) {
        rulrdod1midre rulrdod1midre2 = (rulrdod1midre) view.getLayoutParams();
        if (f > 0.0f && i != 0) {
            int i2 = (((int) (((float) ((-16777216 & i) >>> 24)) * f)) << 24) | (16777215 & i);
            if (rulrdod1midre2.uin6g3d5rqgcbs == null) {
                rulrdod1midre2.uin6g3d5rqgcbs = new Paint();
            }
            rulrdod1midre2.uin6g3d5rqgcbs.setColorFilter(new PorterDuffColorFilter(i2, PorterDuff.Mode.SRC_OVER));
            if (ijavw7l1x.cehyzt7dw(view) != 2) {
                ijavw7l1x.ttmhx7(view, 2, rulrdod1midre2.uin6g3d5rqgcbs);
            }
            uin6g3d5rqgcbs(view);
        } else if (ijavw7l1x.cehyzt7dw(view) != 0) {
            if (rulrdod1midre2.uin6g3d5rqgcbs != null) {
                rulrdod1midre2.uin6g3d5rqgcbs.setColorFilter(null);
            }
            k3jokks5k5 k3jokks5k52 = new k3jokks5k5(this, view);
            this.mqnmk83l0o.add(k3jokks5k52);
            ijavw7l1x.ttmhx7(this, k3jokks5k52);
        }
    }

    private boolean ttmhx7(View view, int i) {
        if (!this.rulrdod1midre && !ttmhx7(0.0f, i)) {
            return false;
        }
        this.k3jokks5k5 = false;
        return true;
    }

    /* access modifiers changed from: private */
    public void uin6g3d5rqgcbs(View view) {
        ttmhx7.ttmhx7(this, view);
    }

    public boolean cehyzt7dw() {
        return ttmhx7(this.lg71ytkvzw, 0);
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return (layoutParams instanceof rulrdod1midre) && super.checkLayoutParams(layoutParams);
    }

    public void computeScroll() {
        if (!this.flawb66z00q.ttmhx7(true)) {
            return;
        }
        if (!this.e8kxjqktk9t) {
            this.flawb66z00q.fxug2rdnfo();
        } else {
            ijavw7l1x.ozpoxuz523b2(this);
        }
    }

    public void draw(Canvas canvas) {
        int left;
        int i;
        super.draw(canvas);
        Drawable drawable = fxug2rdnfo() ? this.usuayu88rw4 : this.uin6g3d5rqgcbs;
        View childAt = getChildCount() > 1 ? getChildAt(1) : null;
        if (childAt != null && drawable != null) {
            int top = childAt.getTop();
            int bottom = childAt.getBottom();
            int intrinsicWidth = drawable.getIntrinsicWidth();
            if (fxug2rdnfo()) {
                i = childAt.getRight();
                left = i + intrinsicWidth;
            } else {
                left = childAt.getLeft();
                i = left - intrinsicWidth;
            }
            drawable.setBounds(i, top, left, bottom);
            drawable.draw(canvas);
        }
    }

    /* access modifiers changed from: protected */
    public boolean drawChild(Canvas canvas, View view, long j) {
        boolean drawChild;
        rulrdod1midre rulrdod1midre2 = (rulrdod1midre) view.getLayoutParams();
        int save = canvas.save(2);
        if (this.e8kxjqktk9t && !rulrdod1midre2.ozpoxuz523b2 && this.lg71ytkvzw != null) {
            canvas.getClipBounds(this.cpgyvt8o4r3);
            if (fxug2rdnfo()) {
                this.cpgyvt8o4r3.left = Math.max(this.cpgyvt8o4r3.left, this.lg71ytkvzw.getRight());
            } else {
                this.cpgyvt8o4r3.right = Math.min(this.cpgyvt8o4r3.right, this.lg71ytkvzw.getLeft());
            }
            canvas.clipRect(this.cpgyvt8o4r3);
        }
        if (Build.VERSION.SDK_INT >= 11) {
            drawChild = super.drawChild(canvas, view, j);
        } else if (!rulrdod1midre2.cehyzt7dw || this.ef5tn1cvshg414 <= 0.0f) {
            if (view.isDrawingCacheEnabled()) {
                view.setDrawingCacheEnabled(false);
            }
            drawChild = super.drawChild(canvas, view, j);
        } else {
            if (!view.isDrawingCacheEnabled()) {
                view.setDrawingCacheEnabled(true);
            }
            Bitmap drawingCache = view.getDrawingCache();
            if (drawingCache != null) {
                canvas.drawBitmap(drawingCache, (float) view.getLeft(), (float) view.getTop(), rulrdod1midre2.uin6g3d5rqgcbs);
                drawChild = false;
            } else {
                Log.e("SlidingPaneLayout", "drawChild: child view " + view + " returned null drawing cache");
                drawChild = super.drawChild(canvas, view, j);
            }
        }
        canvas.restoreToCount(save);
        return drawChild;
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new rulrdod1midre();
    }

    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new rulrdod1midre(getContext(), attributeSet);
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof ViewGroup.MarginLayoutParams ? new rulrdod1midre((ViewGroup.MarginLayoutParams) layoutParams) : new rulrdod1midre(layoutParams);
    }

    public int getCoveredFadeColor() {
        return this.cehyzt7dw;
    }

    public int getParallaxDistance() {
        return this.mhtc4dliin7r;
    }

    public int getSliderFadeColor() {
        return this.ozpoxuz523b2;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.rulrdod1midre = true;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.rulrdod1midre = true;
        int size = this.mqnmk83l0o.size();
        for (int i = 0; i < size; i++) {
            ((k3jokks5k5) this.mqnmk83l0o.get(i)).run();
        }
        this.mqnmk83l0o.clear();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        boolean z;
        View childAt;
        int ttmhx72 = rulrdod1midre.ttmhx7(motionEvent);
        if (!this.e8kxjqktk9t && ttmhx72 == 0 && getChildCount() > 1 && (childAt = getChildAt(1)) != null) {
            this.k3jokks5k5 = !this.flawb66z00q.ozpoxuz523b2(childAt, (int) motionEvent.getX(), (int) motionEvent.getY());
        }
        if (!this.e8kxjqktk9t || (this.ay6ebym1yp0qgk && ttmhx72 != 0)) {
            this.flawb66z00q.usuayu88rw4();
            return super.onInterceptTouchEvent(motionEvent);
        } else if (ttmhx72 == 3 || ttmhx72 == 1) {
            this.flawb66z00q.usuayu88rw4();
            return false;
        } else {
            switch (ttmhx72) {
                case 0:
                    this.ay6ebym1yp0qgk = false;
                    float x = motionEvent.getX();
                    float y = motionEvent.getY();
                    this.oc9mgl157cp = x;
                    this.bpogj6 = y;
                    if (this.flawb66z00q.ozpoxuz523b2(this.lg71ytkvzw, (int) x, (int) y) && ozpoxuz523b2(this.lg71ytkvzw)) {
                        z = true;
                        break;
                    }
                    z = false;
                    break;
                case 1:
                default:
                    z = false;
                    break;
                case 2:
                    float x2 = motionEvent.getX();
                    float y2 = motionEvent.getY();
                    float abs = Math.abs(x2 - this.oc9mgl157cp);
                    float abs2 = Math.abs(y2 - this.bpogj6);
                    if (abs > ((float) this.flawb66z00q.uin6g3d5rqgcbs()) && abs2 > abs) {
                        this.flawb66z00q.usuayu88rw4();
                        this.ay6ebym1yp0qgk = true;
                        return false;
                    }
                    z = false;
                    break;
            }
            return this.flawb66z00q.ttmhx7(motionEvent) || z;
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int i5;
        int i6;
        int i7;
        int i8;
        int width;
        int i9;
        boolean fxug2rdnfo2 = fxug2rdnfo();
        if (fxug2rdnfo2) {
            this.flawb66z00q.ttmhx7(2);
        } else {
            this.flawb66z00q.ttmhx7(1);
        }
        int i10 = i3 - i;
        int paddingRight = fxug2rdnfo2 ? getPaddingRight() : getPaddingLeft();
        int paddingLeft = fxug2rdnfo2 ? getPaddingLeft() : getPaddingRight();
        int paddingTop = getPaddingTop();
        int childCount = getChildCount();
        if (this.rulrdod1midre) {
            this.ef5tn1cvshg414 = (!this.e8kxjqktk9t || !this.k3jokks5k5) ? 0.0f : 1.0f;
        }
        int i11 = 0;
        int i12 = paddingRight;
        while (i11 < childCount) {
            View childAt = getChildAt(i11);
            if (childAt.getVisibility() == 8) {
                width = paddingRight;
                i9 = i12;
            } else {
                rulrdod1midre rulrdod1midre2 = (rulrdod1midre) childAt.getLayoutParams();
                int measuredWidth = childAt.getMeasuredWidth();
                if (rulrdod1midre2.ozpoxuz523b2) {
                    int min = (Math.min(paddingRight, (i10 - paddingLeft) - this.fxug2rdnfo) - i12) - (rulrdod1midre2.leftMargin + rulrdod1midre2.rightMargin);
                    this.iux03f6yieb = min;
                    int i13 = fxug2rdnfo2 ? rulrdod1midre2.rightMargin : rulrdod1midre2.leftMargin;
                    rulrdod1midre2.cehyzt7dw = ((i12 + i13) + min) + (measuredWidth / 2) > i10 - paddingLeft;
                    int i14 = (int) (((float) min) * this.ef5tn1cvshg414);
                    i6 = i12 + i13 + i14;
                    this.ef5tn1cvshg414 = ((float) i14) / ((float) this.iux03f6yieb);
                    i5 = 0;
                } else if (!this.e8kxjqktk9t || this.mhtc4dliin7r == 0) {
                    i5 = 0;
                    i6 = paddingRight;
                } else {
                    i5 = (int) ((1.0f - this.ef5tn1cvshg414) * ((float) this.mhtc4dliin7r));
                    i6 = paddingRight;
                }
                if (fxug2rdnfo2) {
                    i8 = (i10 - i6) + i5;
                    i7 = i8 - measuredWidth;
                } else {
                    i7 = i6 - i5;
                    i8 = i7 + measuredWidth;
                }
                childAt.layout(i7, paddingTop, i8, childAt.getMeasuredHeight() + paddingTop);
                width = childAt.getWidth() + paddingRight;
                i9 = i6;
            }
            i11++;
            paddingRight = width;
            i12 = i9;
        }
        if (this.rulrdod1midre) {
            if (this.e8kxjqktk9t) {
                if (this.mhtc4dliin7r != 0) {
                    ttmhx7(this.ef5tn1cvshg414);
                }
                if (((rulrdod1midre) this.lg71ytkvzw.getLayoutParams()).cehyzt7dw) {
                    ttmhx7(this.lg71ytkvzw, this.ef5tn1cvshg414, this.ozpoxuz523b2);
                }
            } else {
                for (int i15 = 0; i15 < childCount; i15++) {
                    ttmhx7(getChildAt(i15), 0.0f, this.ozpoxuz523b2);
                }
            }
            ttmhx7(this.lg71ytkvzw);
        }
        this.rulrdod1midre = false;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int i3;
        int i4;
        int i5;
        int i6;
        int paddingTop;
        int i7;
        int i8;
        boolean z;
        float f;
        int mode = View.MeasureSpec.getMode(i);
        int size = View.MeasureSpec.getSize(i);
        int mode2 = View.MeasureSpec.getMode(i2);
        int size2 = View.MeasureSpec.getSize(i2);
        if (mode == 1073741824) {
            if (mode2 == 0) {
                if (!isInEditMode()) {
                    throw new IllegalStateException("Height must not be UNSPECIFIED");
                } else if (mode2 == 0) {
                    i3 = Integer.MIN_VALUE;
                    i4 = size;
                    i5 = 300;
                }
            }
            i3 = mode2;
            i4 = size;
            i5 = size2;
        } else if (!isInEditMode()) {
            throw new IllegalStateException("Width must have an exact value or MATCH_PARENT");
        } else if (mode == Integer.MIN_VALUE) {
            i3 = mode2;
            i4 = size;
            i5 = size2;
        } else {
            if (mode == 0) {
                i3 = mode2;
                i4 = 300;
                i5 = size2;
            }
            i3 = mode2;
            i4 = size;
            i5 = size2;
        }
        switch (i3) {
            case Integer.MIN_VALUE:
                i6 = 0;
                paddingTop = (i5 - getPaddingTop()) - getPaddingBottom();
                break;
            case 1073741824:
                i6 = (i5 - getPaddingTop()) - getPaddingBottom();
                paddingTop = i6;
                break;
            default:
                i6 = 0;
                paddingTop = -1;
                break;
        }
        boolean z2 = false;
        int paddingLeft = (i4 - getPaddingLeft()) - getPaddingRight();
        int childCount = getChildCount();
        if (childCount > 2) {
            Log.e("SlidingPaneLayout", "onMeasure: More than two child views are not supported.");
        }
        this.lg71ytkvzw = null;
        int i9 = 0;
        int i10 = paddingLeft;
        int i11 = i6;
        float f2 = 0.0f;
        while (i9 < childCount) {
            View childAt = getChildAt(i9);
            rulrdod1midre rulrdod1midre2 = (rulrdod1midre) childAt.getLayoutParams();
            if (childAt.getVisibility() == 8) {
                rulrdod1midre2.cehyzt7dw = false;
                i7 = i10;
                f = f2;
                i8 = i11;
                z = z2;
            } else {
                if (rulrdod1midre2.ttmhx7 > 0.0f) {
                    f2 += rulrdod1midre2.ttmhx7;
                    if (rulrdod1midre2.width == 0) {
                        i7 = i10;
                        f = f2;
                        i8 = i11;
                        z = z2;
                    }
                }
                int i12 = rulrdod1midre2.leftMargin + rulrdod1midre2.rightMargin;
                childAt.measure(rulrdod1midre2.width == -2 ? View.MeasureSpec.makeMeasureSpec(paddingLeft - i12, Integer.MIN_VALUE) : rulrdod1midre2.width == -1 ? View.MeasureSpec.makeMeasureSpec(paddingLeft - i12, 1073741824) : View.MeasureSpec.makeMeasureSpec(rulrdod1midre2.width, 1073741824), rulrdod1midre2.height == -2 ? View.MeasureSpec.makeMeasureSpec(paddingTop, Integer.MIN_VALUE) : rulrdod1midre2.height == -1 ? View.MeasureSpec.makeMeasureSpec(paddingTop, 1073741824) : View.MeasureSpec.makeMeasureSpec(rulrdod1midre2.height, 1073741824));
                int measuredWidth = childAt.getMeasuredWidth();
                int measuredHeight = childAt.getMeasuredHeight();
                if (i3 == Integer.MIN_VALUE && measuredHeight > i11) {
                    i11 = Math.min(measuredHeight, paddingTop);
                }
                int i13 = i10 - measuredWidth;
                boolean z3 = i13 < 0;
                rulrdod1midre2.ozpoxuz523b2 = z3;
                boolean z4 = z3 | z2;
                if (rulrdod1midre2.ozpoxuz523b2) {
                    this.lg71ytkvzw = childAt;
                }
                i7 = i13;
                i8 = i11;
                float f3 = f2;
                z = z4;
                f = f3;
            }
            i9++;
            z2 = z;
            i11 = i8;
            f2 = f;
            i10 = i7;
        }
        if (z2 || f2 > 0.0f) {
            int i14 = paddingLeft - this.fxug2rdnfo;
            for (int i15 = 0; i15 < childCount; i15++) {
                View childAt2 = getChildAt(i15);
                if (childAt2.getVisibility() != 8) {
                    rulrdod1midre rulrdod1midre3 = (rulrdod1midre) childAt2.getLayoutParams();
                    if (childAt2.getVisibility() != 8) {
                        boolean z5 = rulrdod1midre3.width == 0 && rulrdod1midre3.ttmhx7 > 0.0f;
                        int measuredWidth2 = z5 ? 0 : childAt2.getMeasuredWidth();
                        if (!z2 || childAt2 == this.lg71ytkvzw) {
                            if (rulrdod1midre3.ttmhx7 > 0.0f) {
                                int makeMeasureSpec = rulrdod1midre3.width == 0 ? rulrdod1midre3.height == -2 ? View.MeasureSpec.makeMeasureSpec(paddingTop, Integer.MIN_VALUE) : rulrdod1midre3.height == -1 ? View.MeasureSpec.makeMeasureSpec(paddingTop, 1073741824) : View.MeasureSpec.makeMeasureSpec(rulrdod1midre3.height, 1073741824) : View.MeasureSpec.makeMeasureSpec(childAt2.getMeasuredHeight(), 1073741824);
                                if (z2) {
                                    int i16 = paddingLeft - (rulrdod1midre3.rightMargin + rulrdod1midre3.leftMargin);
                                    int makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(i16, 1073741824);
                                    if (measuredWidth2 != i16) {
                                        childAt2.measure(makeMeasureSpec2, makeMeasureSpec);
                                    }
                                } else {
                                    childAt2.measure(View.MeasureSpec.makeMeasureSpec(((int) ((rulrdod1midre3.ttmhx7 * ((float) Math.max(0, i10))) / f2)) + measuredWidth2, 1073741824), makeMeasureSpec);
                                }
                            }
                        } else if (rulrdod1midre3.width < 0 && (measuredWidth2 > i14 || rulrdod1midre3.ttmhx7 > 0.0f)) {
                            childAt2.measure(View.MeasureSpec.makeMeasureSpec(i14, 1073741824), z5 ? rulrdod1midre3.height == -2 ? View.MeasureSpec.makeMeasureSpec(paddingTop, Integer.MIN_VALUE) : rulrdod1midre3.height == -1 ? View.MeasureSpec.makeMeasureSpec(paddingTop, 1073741824) : View.MeasureSpec.makeMeasureSpec(rulrdod1midre3.height, 1073741824) : View.MeasureSpec.makeMeasureSpec(childAt2.getMeasuredHeight(), 1073741824));
                        }
                    }
                }
            }
        }
        setMeasuredDimension(i4, getPaddingTop() + i11 + getPaddingBottom());
        this.e8kxjqktk9t = z2;
        if (this.flawb66z00q.ttmhx7() != 0 && !z2) {
            this.flawb66z00q.fxug2rdnfo();
        }
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        if (savedState.ttmhx7) {
            ozpoxuz523b2();
        } else {
            cehyzt7dw();
        }
        this.k3jokks5k5 = savedState.ttmhx7;
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.ttmhx7 = usuayu88rw4() ? uin6g3d5rqgcbs() : this.k3jokks5k5;
        return savedState;
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        if (i != i3) {
            this.rulrdod1midre = true;
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (!this.e8kxjqktk9t) {
            return super.onTouchEvent(motionEvent);
        }
        this.flawb66z00q.ozpoxuz523b2(motionEvent);
        switch (motionEvent.getAction() & 255) {
            case 0:
                float x = motionEvent.getX();
                float y = motionEvent.getY();
                this.oc9mgl157cp = x;
                this.bpogj6 = y;
                return true;
            case 1:
                if (!ozpoxuz523b2(this.lg71ytkvzw)) {
                    return true;
                }
                float x2 = motionEvent.getX();
                float y2 = motionEvent.getY();
                float f = x2 - this.oc9mgl157cp;
                float f2 = y2 - this.bpogj6;
                int uin6g3d5rqgcbs2 = this.flawb66z00q.uin6g3d5rqgcbs();
                if ((f * f) + (f2 * f2) >= ((float) (uin6g3d5rqgcbs2 * uin6g3d5rqgcbs2)) || !this.flawb66z00q.ozpoxuz523b2(this.lg71ytkvzw, (int) x2, (int) y2)) {
                    return true;
                }
                ttmhx7(this.lg71ytkvzw, 0);
                return true;
            default:
                return true;
        }
    }

    public boolean ozpoxuz523b2() {
        return ozpoxuz523b2(this.lg71ytkvzw, 0);
    }

    /* access modifiers changed from: package-private */
    public boolean ozpoxuz523b2(View view) {
        if (view == null) {
            return false;
        }
        return this.e8kxjqktk9t && ((rulrdod1midre) view.getLayoutParams()).cehyzt7dw && this.ef5tn1cvshg414 > 0.0f;
    }

    public void requestChildFocus(View view, View view2) {
        super.requestChildFocus(view, view2);
        if (!isInTouchMode() && !this.e8kxjqktk9t) {
            this.k3jokks5k5 = view == this.lg71ytkvzw;
        }
    }

    public void setCoveredFadeColor(int i) {
        this.cehyzt7dw = i;
    }

    public void setPanelSlideListener(cpgyvt8o4r3 cpgyvt8o4r32) {
        this.ca2ssr26fefu = cpgyvt8o4r32;
    }

    public void setParallaxDistance(int i) {
        this.mhtc4dliin7r = i;
        requestLayout();
    }

    @Deprecated
    public void setShadowDrawable(Drawable drawable) {
        setShadowDrawableLeft(drawable);
    }

    public void setShadowDrawableLeft(Drawable drawable) {
        this.uin6g3d5rqgcbs = drawable;
    }

    public void setShadowDrawableRight(Drawable drawable) {
        this.usuayu88rw4 = drawable;
    }

    @Deprecated
    public void setShadowResource(int i) {
        setShadowDrawable(getResources().getDrawable(i));
    }

    public void setShadowResourceLeft(int i) {
        setShadowDrawableLeft(getResources().getDrawable(i));
    }

    public void setShadowResourceRight(int i) {
        setShadowDrawableRight(getResources().getDrawable(i));
    }

    public void setSliderFadeColor(int i) {
        this.ozpoxuz523b2 = i;
    }

    /* access modifiers changed from: package-private */
    public void ttmhx7() {
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            if (childAt.getVisibility() == 4) {
                childAt.setVisibility(0);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void ttmhx7(View view) {
        int i;
        int i2;
        int i3;
        int i4;
        boolean fxug2rdnfo2 = fxug2rdnfo();
        int width = fxug2rdnfo2 ? getWidth() - getPaddingRight() : getPaddingLeft();
        int paddingLeft = fxug2rdnfo2 ? getPaddingLeft() : getWidth() - getPaddingRight();
        int paddingTop = getPaddingTop();
        int height = getHeight() - getPaddingBottom();
        if (view == null || !cehyzt7dw(view)) {
            i = 0;
            i2 = 0;
            i3 = 0;
            i4 = 0;
        } else {
            i4 = view.getLeft();
            i3 = view.getRight();
            i2 = view.getTop();
            i = view.getBottom();
        }
        int childCount = getChildCount();
        int i5 = 0;
        while (i5 < childCount) {
            View childAt = getChildAt(i5);
            if (childAt != view) {
                childAt.setVisibility((Math.max(fxug2rdnfo2 ? paddingLeft : width, childAt.getLeft()) < i4 || Math.max(paddingTop, childAt.getTop()) < i2 || Math.min(fxug2rdnfo2 ? width : paddingLeft, childAt.getRight()) > i3 || Math.min(height, childAt.getBottom()) > i) ? 0 : 4);
                i5++;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean ttmhx7(float f, int i) {
        int paddingLeft;
        if (!this.e8kxjqktk9t) {
            return false;
        }
        boolean fxug2rdnfo2 = fxug2rdnfo();
        rulrdod1midre rulrdod1midre2 = (rulrdod1midre) this.lg71ytkvzw.getLayoutParams();
        if (fxug2rdnfo2) {
            paddingLeft = (int) (((float) getWidth()) - ((((float) (rulrdod1midre2.rightMargin + getPaddingRight())) + (((float) this.iux03f6yieb) * f)) + ((float) this.lg71ytkvzw.getWidth())));
        } else {
            paddingLeft = (int) (((float) (rulrdod1midre2.leftMargin + getPaddingLeft())) + (((float) this.iux03f6yieb) * f));
        }
        if (!this.flawb66z00q.ttmhx7(this.lg71ytkvzw, paddingLeft, this.lg71ytkvzw.getTop())) {
            return false;
        }
        ttmhx7();
        ijavw7l1x.ozpoxuz523b2(this);
        return true;
    }

    public boolean uin6g3d5rqgcbs() {
        return !this.e8kxjqktk9t || this.ef5tn1cvshg414 == 1.0f;
    }

    public boolean usuayu88rw4() {
        return this.e8kxjqktk9t;
    }
}
