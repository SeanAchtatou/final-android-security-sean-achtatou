package android.support.v4.view;

import android.database.DataSetObserver;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.b.ttmhx7;
import android.support.v4.widget.b5zlaptmyxarl;
import android.util.AttributeSet;
import android.util.Log;
import android.view.FocusFinder;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SoundEffectConstants;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.animation.Interpolator;
import android.widget.Scroller;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class ViewPager extends ViewGroup {
    private static final Comparator cehyzt7dw = new i422h07p3ed();
    /* access modifiers changed from: private */
    public static final int[] ttmhx7 = {16842931};
    private static final j4ksty ub3me3t9yzv = new j4ksty();
    private static final Interpolator uin6g3d5rqgcbs = new fx9gujks();
    private olqqn4x4 a5uhwh1;
    private int aecbla89ntoa8;
    private ClassLoader ay6ebym1yp0qgk;
    private int b5zlaptmyxarl;
    private float b6p1j7hoons8;
    private int bh8ldx;
    private float bjcn50q4e9;
    private int bpogj6;
    private Drawable ca2ssr26fefu;
    private int ck0x6f;
    private float cpgyvt8o4r3;
    private final Rect e8kxjqktk9t;
    private int ef5tn1cvshg414;
    private boolean eyli1ymagd3o;
    private int flawb66z00q;
    private int ftlyjgoncub6q;
    private int fx9gujks;
    private final a5uhwh1 fxug2rdnfo;
    private olqqn4x4 hajwjku;
    private boolean i422h07p3ed;
    private int ijavw7l1x;
    private Parcelable iux03f6yieb;
    private final Runnable j4ksty;
    private b5zlaptmyxarl j72htm5;
    private ArrayList ja66gqu;
    private float jaztd5t99d;
    private int k3jokks5k5;
    private int kld4qxthnxo5uo;
    private eyli1ymagd3o lg71ytkvzw;
    private int lqwegpi5;
    private Scroller mhtc4dliin7r;
    private int mqnmk83l0o;
    private bh8ldx o9ph3xbm2yk6g;
    private ja66gqu oc9mgl157cp;
    private boolean ol99ycz2wbkd;
    private Method olqqn4x4;
    private int oziax9tu6k;
    private int ozpoxuz523b2;
    private boolean rob6sujr97;
    private float rulrdod1midre;
    private int s6o869vduri;
    private boolean sgnd7s4;
    private VelocityTracker ty7df019s;
    private int ug2s4y;
    private final ArrayList usuayu88rw4;
    private o9ph3xbm2yk6g w3g96lv8;
    private b5zlaptmyxarl w5mzcxwa3kamml;
    private float wg4f90m80dyc0s;
    private boolean xbcow1jyae;
    private boolean xf1q4c;
    private int yry0gjw5rm0;
    private int yxqgts35zbsb;
    private boolean zk18i66egwxe;
    private boolean zs1ge47fq1dgv5;

    public class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator CREATOR = ttmhx7.ttmhx7(new ub3me3t9yzv());
        ClassLoader cehyzt7dw;
        Parcelable ozpoxuz523b2;
        int ttmhx7;

        SavedState(Parcel parcel, ClassLoader classLoader) {
            super(parcel);
            classLoader = classLoader == null ? getClass().getClassLoader() : classLoader;
            this.ttmhx7 = parcel.readInt();
            this.ozpoxuz523b2 = parcel.readParcelable(classLoader);
            this.cehyzt7dw = classLoader;
        }

        public SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public String toString() {
            return "FragmentPager.SavedState{" + Integer.toHexString(System.identityHashCode(this)) + " position=" + this.ttmhx7 + "}";
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.ttmhx7);
            parcel.writeParcelable(this.ozpoxuz523b2, i);
        }
    }

    private void cehyzt7dw(boolean z) {
        ViewParent parent = getParent();
        if (parent != null) {
            parent.requestDisallowInterceptTouchEvent(z);
        }
    }

    private void e8kxjqktk9t() {
        if (this.bh8ldx != 0) {
            if (this.ja66gqu == null) {
                this.ja66gqu = new ArrayList();
            } else {
                this.ja66gqu.clear();
            }
            int childCount = getChildCount();
            for (int i = 0; i < childCount; i++) {
                this.ja66gqu.add(getChildAt(i));
            }
            Collections.sort(this.ja66gqu, ub3me3t9yzv);
        }
    }

    private void ef5tn1cvshg414() {
        this.zs1ge47fq1dgv5 = false;
        this.xbcow1jyae = false;
        if (this.ty7df019s != null) {
            this.ty7df019s.recycle();
            this.ty7df019s = null;
        }
    }

    private void fxug2rdnfo() {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < getChildCount()) {
                if (!((w3g96lv8) getChildAt(i2).getLayoutParams()).ttmhx7) {
                    removeViewAt(i2);
                    i2--;
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    private int getClientWidth() {
        return (getMeasuredWidth() - getPaddingLeft()) - getPaddingRight();
    }

    private a5uhwh1 lg71ytkvzw() {
        int i;
        a5uhwh1 a5uhwh12;
        int clientWidth = getClientWidth();
        float scrollX = clientWidth > 0 ? ((float) getScrollX()) / ((float) clientWidth) : 0.0f;
        float f = clientWidth > 0 ? ((float) this.bpogj6) / ((float) clientWidth) : 0.0f;
        float f2 = 0.0f;
        float f3 = 0.0f;
        int i2 = -1;
        int i3 = 0;
        boolean z = true;
        a5uhwh1 a5uhwh13 = null;
        while (i3 < this.usuayu88rw4.size()) {
            a5uhwh1 a5uhwh14 = (a5uhwh1) this.usuayu88rw4.get(i3);
            if (z || a5uhwh14.ozpoxuz523b2 == i2 + 1) {
                a5uhwh1 a5uhwh15 = a5uhwh14;
                i = i3;
                a5uhwh12 = a5uhwh15;
            } else {
                a5uhwh1 a5uhwh16 = this.fxug2rdnfo;
                a5uhwh16.usuayu88rw4 = f2 + f3 + f;
                a5uhwh16.ozpoxuz523b2 = i2 + 1;
                a5uhwh16.uin6g3d5rqgcbs = this.lg71ytkvzw.ttmhx7(a5uhwh16.ozpoxuz523b2);
                a5uhwh1 a5uhwh17 = a5uhwh16;
                i = i3 - 1;
                a5uhwh12 = a5uhwh17;
            }
            float f4 = a5uhwh12.usuayu88rw4;
            float f5 = a5uhwh12.uin6g3d5rqgcbs + f4 + f;
            if (!z && scrollX < f4) {
                return a5uhwh13;
            }
            if (scrollX < f5 || i == this.usuayu88rw4.size() - 1) {
                return a5uhwh12;
            }
            f3 = f4;
            i2 = a5uhwh12.ozpoxuz523b2;
            z = false;
            f2 = a5uhwh12.uin6g3d5rqgcbs;
            a5uhwh13 = a5uhwh12;
            i3 = i + 1;
        }
        return a5uhwh13;
    }

    private void ozpoxuz523b2(boolean z) {
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            ijavw7l1x.ttmhx7(getChildAt(i), z ? 2 : 0, null);
        }
    }

    private boolean ozpoxuz523b2(float f) {
        boolean z;
        float f2;
        boolean z2 = true;
        boolean z3 = false;
        this.wg4f90m80dyc0s = f;
        float scrollX = ((float) getScrollX()) + (this.wg4f90m80dyc0s - f);
        int clientWidth = getClientWidth();
        float f3 = ((float) clientWidth) * this.rulrdod1midre;
        float f4 = ((float) clientWidth) * this.cpgyvt8o4r3;
        a5uhwh1 a5uhwh12 = (a5uhwh1) this.usuayu88rw4.get(0);
        a5uhwh1 a5uhwh13 = (a5uhwh1) this.usuayu88rw4.get(this.usuayu88rw4.size() - 1);
        if (a5uhwh12.ozpoxuz523b2 != 0) {
            f3 = a5uhwh12.usuayu88rw4 * ((float) clientWidth);
            z = false;
        } else {
            z = true;
        }
        if (a5uhwh13.ozpoxuz523b2 != this.lg71ytkvzw.ttmhx7() - 1) {
            f2 = a5uhwh13.usuayu88rw4 * ((float) clientWidth);
            z2 = false;
        } else {
            f2 = f4;
        }
        if (scrollX < f3) {
            if (z) {
                z3 = this.w5mzcxwa3kamml.ttmhx7(Math.abs(f3 - scrollX) / ((float) clientWidth));
            }
        } else if (scrollX > f2) {
            if (z2) {
                z3 = this.j72htm5.ttmhx7(Math.abs(scrollX - f2) / ((float) clientWidth));
            }
            f3 = f2;
        } else {
            f3 = scrollX;
        }
        this.wg4f90m80dyc0s += f3 - ((float) ((int) f3));
        scrollTo((int) f3, getScrollY());
        uin6g3d5rqgcbs((int) f3);
        return z3;
    }

    private void setScrollState(int i) {
        if (this.yry0gjw5rm0 != i) {
            this.yry0gjw5rm0 = i;
            if (this.o9ph3xbm2yk6g != null) {
                ozpoxuz523b2(i != 0);
            }
            if (this.hajwjku != null) {
                this.hajwjku.ozpoxuz523b2(i);
            }
        }
    }

    private void setScrollingCacheEnabled(boolean z) {
        if (this.eyli1ymagd3o != z) {
            this.eyli1ymagd3o = z;
        }
    }

    private int ttmhx7(int i, float f, int i2, int i3) {
        if (Math.abs(i3) <= this.ck0x6f || Math.abs(i2) <= this.kld4qxthnxo5uo) {
            i = (int) ((i >= this.ef5tn1cvshg414 ? 0.4f : 0.6f) + ((float) i) + f);
        } else if (i2 <= 0) {
            i++;
        }
        return this.usuayu88rw4.size() > 0 ? Math.max(((a5uhwh1) this.usuayu88rw4.get(0)).ozpoxuz523b2, Math.min(i, ((a5uhwh1) this.usuayu88rw4.get(this.usuayu88rw4.size() - 1)).ozpoxuz523b2)) : i;
    }

    private Rect ttmhx7(Rect rect, View view) {
        Rect rect2 = rect == null ? new Rect() : rect;
        if (view == null) {
            rect2.set(0, 0, 0, 0);
            return rect2;
        }
        rect2.left = view.getLeft();
        rect2.right = view.getRight();
        rect2.top = view.getTop();
        rect2.bottom = view.getBottom();
        ViewParent parent = view.getParent();
        while ((parent instanceof ViewGroup) && parent != this) {
            ViewGroup viewGroup = (ViewGroup) parent;
            rect2.left += viewGroup.getLeft();
            rect2.right += viewGroup.getRight();
            rect2.top += viewGroup.getTop();
            rect2.bottom += viewGroup.getBottom();
            parent = viewGroup.getParent();
        }
        return rect2;
    }

    private void ttmhx7(int i, int i2, int i3, int i4) {
        if (i2 <= 0 || this.usuayu88rw4.isEmpty()) {
            a5uhwh1 ozpoxuz523b22 = ozpoxuz523b2(this.ef5tn1cvshg414);
            int min = (int) ((ozpoxuz523b22 != null ? Math.min(ozpoxuz523b22.usuayu88rw4, this.cpgyvt8o4r3) : 0.0f) * ((float) ((i - getPaddingLeft()) - getPaddingRight())));
            if (min != getScrollX()) {
                ttmhx7(false);
                scrollTo(min, getScrollY());
                return;
            }
            return;
        }
        int paddingLeft = (int) (((float) (((i - getPaddingLeft()) - getPaddingRight()) + i3)) * (((float) getScrollX()) / ((float) (((i2 - getPaddingLeft()) - getPaddingRight()) + i4))));
        scrollTo(paddingLeft, getScrollY());
        if (!this.mhtc4dliin7r.isFinished()) {
            this.mhtc4dliin7r.startScroll(paddingLeft, 0, (int) (ozpoxuz523b2(this.ef5tn1cvshg414).usuayu88rw4 * ((float) i)), 0, this.mhtc4dliin7r.getDuration() - this.mhtc4dliin7r.timePassed());
        }
    }

    private void ttmhx7(int i, boolean z, int i2, boolean z2) {
        int i3;
        a5uhwh1 ozpoxuz523b22 = ozpoxuz523b2(i);
        if (ozpoxuz523b22 != null) {
            i3 = (int) (Math.max(this.rulrdod1midre, Math.min(ozpoxuz523b22.usuayu88rw4, this.cpgyvt8o4r3)) * ((float) getClientWidth()));
        } else {
            i3 = 0;
        }
        if (z) {
            ttmhx7(i3, 0, i2);
            if (z2 && this.hajwjku != null) {
                this.hajwjku.ttmhx7(i);
            }
            if (z2 && this.a5uhwh1 != null) {
                this.a5uhwh1.ttmhx7(i);
                return;
            }
            return;
        }
        if (z2 && this.hajwjku != null) {
            this.hajwjku.ttmhx7(i);
        }
        if (z2 && this.a5uhwh1 != null) {
            this.a5uhwh1.ttmhx7(i);
        }
        ttmhx7(false);
        scrollTo(i3, 0);
        uin6g3d5rqgcbs(i3);
    }

    private void ttmhx7(a5uhwh1 a5uhwh12, int i, a5uhwh1 a5uhwh13) {
        a5uhwh1 a5uhwh14;
        a5uhwh1 a5uhwh15;
        int ttmhx72 = this.lg71ytkvzw.ttmhx7();
        int clientWidth = getClientWidth();
        float f = clientWidth > 0 ? ((float) this.bpogj6) / ((float) clientWidth) : 0.0f;
        if (a5uhwh13 != null) {
            int i2 = a5uhwh13.ozpoxuz523b2;
            if (i2 < a5uhwh12.ozpoxuz523b2) {
                float f2 = a5uhwh13.usuayu88rw4 + a5uhwh13.uin6g3d5rqgcbs + f;
                int i3 = i2 + 1;
                int i4 = 0;
                while (i3 <= a5uhwh12.ozpoxuz523b2 && i4 < this.usuayu88rw4.size()) {
                    Object obj = this.usuayu88rw4.get(i4);
                    while (true) {
                        a5uhwh15 = (a5uhwh1) obj;
                        if (i3 > a5uhwh15.ozpoxuz523b2 && i4 < this.usuayu88rw4.size() - 1) {
                            i4++;
                            obj = this.usuayu88rw4.get(i4);
                        }
                    }
                    while (i3 < a5uhwh15.ozpoxuz523b2) {
                        f2 += this.lg71ytkvzw.ttmhx7(i3) + f;
                        i3++;
                    }
                    a5uhwh15.usuayu88rw4 = f2;
                    f2 += a5uhwh15.uin6g3d5rqgcbs + f;
                    i3++;
                }
            } else if (i2 > a5uhwh12.ozpoxuz523b2) {
                int size = this.usuayu88rw4.size() - 1;
                float f3 = a5uhwh13.usuayu88rw4;
                int i5 = i2 - 1;
                while (i5 >= a5uhwh12.ozpoxuz523b2 && size >= 0) {
                    Object obj2 = this.usuayu88rw4.get(size);
                    while (true) {
                        a5uhwh14 = (a5uhwh1) obj2;
                        if (i5 < a5uhwh14.ozpoxuz523b2 && size > 0) {
                            size--;
                            obj2 = this.usuayu88rw4.get(size);
                        }
                    }
                    while (i5 > a5uhwh14.ozpoxuz523b2) {
                        f3 -= this.lg71ytkvzw.ttmhx7(i5) + f;
                        i5--;
                    }
                    f3 -= a5uhwh14.uin6g3d5rqgcbs + f;
                    a5uhwh14.usuayu88rw4 = f3;
                    i5--;
                }
            }
        }
        int size2 = this.usuayu88rw4.size();
        float f4 = a5uhwh12.usuayu88rw4;
        int i6 = a5uhwh12.ozpoxuz523b2 - 1;
        this.rulrdod1midre = a5uhwh12.ozpoxuz523b2 == 0 ? a5uhwh12.usuayu88rw4 : -3.4028235E38f;
        this.cpgyvt8o4r3 = a5uhwh12.ozpoxuz523b2 == ttmhx72 + -1 ? (a5uhwh12.usuayu88rw4 + a5uhwh12.uin6g3d5rqgcbs) - 1.0f : Float.MAX_VALUE;
        for (int i7 = i - 1; i7 >= 0; i7--) {
            a5uhwh1 a5uhwh16 = (a5uhwh1) this.usuayu88rw4.get(i7);
            float f5 = f4;
            while (i6 > a5uhwh16.ozpoxuz523b2) {
                f5 -= this.lg71ytkvzw.ttmhx7(i6) + f;
                i6--;
            }
            f4 = f5 - (a5uhwh16.uin6g3d5rqgcbs + f);
            a5uhwh16.usuayu88rw4 = f4;
            if (a5uhwh16.ozpoxuz523b2 == 0) {
                this.rulrdod1midre = f4;
            }
            i6--;
        }
        float f6 = a5uhwh12.usuayu88rw4 + a5uhwh12.uin6g3d5rqgcbs + f;
        int i8 = a5uhwh12.ozpoxuz523b2 + 1;
        for (int i9 = i + 1; i9 < size2; i9++) {
            a5uhwh1 a5uhwh17 = (a5uhwh1) this.usuayu88rw4.get(i9);
            float f7 = f6;
            while (i8 < a5uhwh17.ozpoxuz523b2) {
                f7 = this.lg71ytkvzw.ttmhx7(i8) + f + f7;
                i8++;
            }
            if (a5uhwh17.ozpoxuz523b2 == ttmhx72 - 1) {
                this.cpgyvt8o4r3 = (a5uhwh17.uin6g3d5rqgcbs + f7) - 1.0f;
            }
            a5uhwh17.usuayu88rw4 = f7;
            f6 = f7 + a5uhwh17.uin6g3d5rqgcbs + f;
            i8++;
        }
        this.zk18i66egwxe = false;
    }

    private void ttmhx7(MotionEvent motionEvent) {
        int ozpoxuz523b22 = rulrdod1midre.ozpoxuz523b2(motionEvent);
        if (rulrdod1midre.ozpoxuz523b2(motionEvent, ozpoxuz523b22) == this.oziax9tu6k) {
            int i = ozpoxuz523b22 == 0 ? 1 : 0;
            this.wg4f90m80dyc0s = rulrdod1midre.cehyzt7dw(motionEvent, i);
            this.oziax9tu6k = rulrdod1midre.ozpoxuz523b2(motionEvent, i);
            if (this.ty7df019s != null) {
                this.ty7df019s.clear();
            }
        }
    }

    private void ttmhx7(boolean z) {
        boolean z2 = this.yry0gjw5rm0 == 2;
        if (z2) {
            setScrollingCacheEnabled(false);
            this.mhtc4dliin7r.abortAnimation();
            int scrollX = getScrollX();
            int scrollY = getScrollY();
            int currX = this.mhtc4dliin7r.getCurrX();
            int currY = this.mhtc4dliin7r.getCurrY();
            if (!(scrollX == currX && scrollY == currY)) {
                scrollTo(currX, currY);
            }
        }
        this.sgnd7s4 = false;
        boolean z3 = z2;
        for (int i = 0; i < this.usuayu88rw4.size(); i++) {
            a5uhwh1 a5uhwh12 = (a5uhwh1) this.usuayu88rw4.get(i);
            if (a5uhwh12.cehyzt7dw) {
                a5uhwh12.cehyzt7dw = false;
                z3 = true;
            }
        }
        if (!z3) {
            return;
        }
        if (z) {
            ijavw7l1x.ttmhx7(this, this.j4ksty);
        } else {
            this.j4ksty.run();
        }
    }

    private boolean ttmhx7(float f, float f2) {
        return (f < ((float) this.ijavw7l1x) && f2 > 0.0f) || (f > ((float) (getWidth() - this.ijavw7l1x)) && f2 < 0.0f);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.ttmhx7(int, float, int):void
     arg types: [int, int, int]
     candidates:
      android.support.v4.view.ViewPager.ttmhx7(android.support.v4.view.a5uhwh1, int, android.support.v4.view.a5uhwh1):void
      android.support.v4.view.ViewPager.ttmhx7(int, int, int):void
      android.support.v4.view.ViewPager.ttmhx7(int, boolean, boolean):void
      android.support.v4.view.ViewPager.ttmhx7(int, float, int):void */
    private boolean uin6g3d5rqgcbs(int i) {
        if (this.usuayu88rw4.size() == 0) {
            this.i422h07p3ed = false;
            ttmhx7(0, 0.0f, 0);
            if (this.i422h07p3ed) {
                return false;
            }
            throw new IllegalStateException("onPageScrolled did not call superclass implementation");
        }
        a5uhwh1 lg71ytkvzw2 = lg71ytkvzw();
        int clientWidth = getClientWidth();
        int i2 = this.bpogj6 + clientWidth;
        float f = ((float) this.bpogj6) / ((float) clientWidth);
        int i3 = lg71ytkvzw2.ozpoxuz523b2;
        float f2 = ((((float) i) / ((float) clientWidth)) - lg71ytkvzw2.usuayu88rw4) / (lg71ytkvzw2.uin6g3d5rqgcbs + f);
        this.i422h07p3ed = false;
        ttmhx7(i3, f2, (int) (((float) i2) * f2));
        if (this.i422h07p3ed) {
            return true;
        }
        throw new IllegalStateException("onPageScrolled did not call superclass implementation");
    }

    public void addFocusables(ArrayList arrayList, int i, int i2) {
        a5uhwh1 ttmhx72;
        int size = arrayList.size();
        int descendantFocusability = getDescendantFocusability();
        if (descendantFocusability != 393216) {
            for (int i3 = 0; i3 < getChildCount(); i3++) {
                View childAt = getChildAt(i3);
                if (childAt.getVisibility() == 0 && (ttmhx72 = ttmhx7(childAt)) != null && ttmhx72.ozpoxuz523b2 == this.ef5tn1cvshg414) {
                    childAt.addFocusables(arrayList, i, i2);
                }
            }
        }
        if ((descendantFocusability == 262144 && size != arrayList.size()) || !isFocusable()) {
            return;
        }
        if (((i2 & 1) != 1 || !isInTouchMode() || isFocusableInTouchMode()) && arrayList != null) {
            arrayList.add(this);
        }
    }

    public void addTouchables(ArrayList arrayList) {
        a5uhwh1 ttmhx72;
        for (int i = 0; i < getChildCount(); i++) {
            View childAt = getChildAt(i);
            if (childAt.getVisibility() == 0 && (ttmhx72 = ttmhx7(childAt)) != null && ttmhx72.ozpoxuz523b2 == this.ef5tn1cvshg414) {
                childAt.addTouchables(arrayList);
            }
        }
    }

    public void addView(View view, int i, ViewGroup.LayoutParams layoutParams) {
        ViewGroup.LayoutParams generateLayoutParams = !checkLayoutParams(layoutParams) ? generateLayoutParams(layoutParams) : layoutParams;
        w3g96lv8 w3g96lv82 = (w3g96lv8) generateLayoutParams;
        w3g96lv82.ttmhx7 |= view instanceof hajwjku;
        if (!this.ol99ycz2wbkd) {
            super.addView(view, i, generateLayoutParams);
        } else if (w3g96lv82 == null || !w3g96lv82.ttmhx7) {
            w3g96lv82.uin6g3d5rqgcbs = true;
            addViewInLayout(view, i, generateLayoutParams);
        } else {
            throw new IllegalStateException("Cannot add pager decor view during layout");
        }
    }

    public boolean canScrollHorizontally(int i) {
        boolean z = true;
        if (this.lg71ytkvzw == null) {
            return false;
        }
        int clientWidth = getClientWidth();
        int scrollX = getScrollX();
        if (i < 0) {
            if (scrollX <= ((int) (((float) clientWidth) * this.rulrdod1midre))) {
                z = false;
            }
            return z;
        } else if (i <= 0) {
            return false;
        } else {
            if (scrollX >= ((int) (((float) clientWidth) * this.cpgyvt8o4r3))) {
                z = false;
            }
            return z;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.ttmhx7(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.view.ViewPager.ttmhx7(android.graphics.Rect, android.view.View):android.graphics.Rect
      android.support.v4.view.ViewPager.ttmhx7(float, float):boolean
      android.support.v4.view.ViewPager.ttmhx7(int, int):android.support.v4.view.a5uhwh1
      android.support.v4.view.ViewPager.ttmhx7(int, boolean):void */
    /* access modifiers changed from: package-private */
    public boolean cehyzt7dw() {
        if (this.ef5tn1cvshg414 <= 0) {
            return false;
        }
        ttmhx7(this.ef5tn1cvshg414 - 1, true);
        return true;
    }

    public boolean cehyzt7dw(int i) {
        View view;
        boolean z;
        boolean z2;
        View findFocus = findFocus();
        if (findFocus == this) {
            view = null;
        } else {
            if (findFocus != null) {
                ViewParent parent = findFocus.getParent();
                while (true) {
                    if (!(parent instanceof ViewGroup)) {
                        z = false;
                        break;
                    } else if (parent == this) {
                        z = true;
                        break;
                    } else {
                        parent = parent.getParent();
                    }
                }
                if (!z) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(findFocus.getClass().getSimpleName());
                    for (ViewParent parent2 = findFocus.getParent(); parent2 instanceof ViewGroup; parent2 = parent2.getParent()) {
                        sb.append(" => ").append(parent2.getClass().getSimpleName());
                    }
                    Log.e("ViewPager", "arrowScroll tried to find focus based on non-child current focused view " + sb.toString());
                    view = null;
                }
            }
            view = findFocus;
        }
        View findNextFocus = FocusFinder.getInstance().findNextFocus(this, view, i);
        if (findNextFocus == null || findNextFocus == view) {
            if (i == 17 || i == 1) {
                z2 = cehyzt7dw();
            } else {
                if (i == 66 || i == 2) {
                    z2 = uin6g3d5rqgcbs();
                }
                z2 = false;
            }
        } else if (i == 17) {
            z2 = (view == null || ttmhx7(this.e8kxjqktk9t, findNextFocus).left < ttmhx7(this.e8kxjqktk9t, view).left) ? findNextFocus.requestFocus() : cehyzt7dw();
        } else {
            if (i == 66) {
                z2 = (view == null || ttmhx7(this.e8kxjqktk9t, findNextFocus).left > ttmhx7(this.e8kxjqktk9t, view).left) ? findNextFocus.requestFocus() : uin6g3d5rqgcbs();
            }
            z2 = false;
        }
        if (z2) {
            playSoundEffect(SoundEffectConstants.getContantForFocusDirection(i));
        }
        return z2;
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return (layoutParams instanceof w3g96lv8) && super.checkLayoutParams(layoutParams);
    }

    public void computeScroll() {
        if (this.mhtc4dliin7r.isFinished() || !this.mhtc4dliin7r.computeScrollOffset()) {
            ttmhx7(true);
            return;
        }
        int scrollX = getScrollX();
        int scrollY = getScrollY();
        int currX = this.mhtc4dliin7r.getCurrX();
        int currY = this.mhtc4dliin7r.getCurrY();
        if (!(scrollX == currX && scrollY == currY)) {
            scrollTo(currX, currY);
            if (!uin6g3d5rqgcbs(currX)) {
                this.mhtc4dliin7r.abortAnimation();
                scrollTo(0, currY);
            }
        }
        ijavw7l1x.ozpoxuz523b2(this);
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        return super.dispatchKeyEvent(keyEvent) || ttmhx7(keyEvent);
    }

    public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        a5uhwh1 ttmhx72;
        if (accessibilityEvent.getEventType() == 4096) {
            return super.dispatchPopulateAccessibilityEvent(accessibilityEvent);
        }
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            if (childAt.getVisibility() == 0 && (ttmhx72 = ttmhx7(childAt)) != null && ttmhx72.ozpoxuz523b2 == this.ef5tn1cvshg414 && childAt.dispatchPopulateAccessibilityEvent(accessibilityEvent)) {
                return true;
            }
        }
        return false;
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        boolean z = false;
        int ttmhx72 = ijavw7l1x.ttmhx7(this);
        if (ttmhx72 == 0 || (ttmhx72 == 1 && this.lg71ytkvzw != null && this.lg71ytkvzw.ttmhx7() > 1)) {
            if (!this.w5mzcxwa3kamml.ttmhx7()) {
                int save = canvas.save();
                int height = (getHeight() - getPaddingTop()) - getPaddingBottom();
                int width = getWidth();
                canvas.rotate(270.0f);
                canvas.translate((float) ((-height) + getPaddingTop()), this.rulrdod1midre * ((float) width));
                this.w5mzcxwa3kamml.ttmhx7(height, width);
                z = false | this.w5mzcxwa3kamml.ttmhx7(canvas);
                canvas.restoreToCount(save);
            }
            if (!this.j72htm5.ttmhx7()) {
                int save2 = canvas.save();
                int width2 = getWidth();
                int height2 = (getHeight() - getPaddingTop()) - getPaddingBottom();
                canvas.rotate(90.0f);
                canvas.translate((float) (-getPaddingTop()), (-(this.cpgyvt8o4r3 + 1.0f)) * ((float) width2));
                this.j72htm5.ttmhx7(height2, width2);
                z |= this.j72htm5.ttmhx7(canvas);
                canvas.restoreToCount(save2);
            }
        } else {
            this.w5mzcxwa3kamml.ozpoxuz523b2();
            this.j72htm5.ozpoxuz523b2();
        }
        if (z) {
            ijavw7l1x.ozpoxuz523b2(this);
        }
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        Drawable drawable = this.ca2ssr26fefu;
        if (drawable != null && drawable.isStateful()) {
            drawable.setState(getDrawableState());
        }
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new w3g96lv8();
    }

    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new w3g96lv8(getContext(), attributeSet);
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return generateDefaultLayoutParams();
    }

    public eyli1ymagd3o getAdapter() {
        return this.lg71ytkvzw;
    }

    /* access modifiers changed from: protected */
    public int getChildDrawingOrder(int i, int i2) {
        if (this.bh8ldx == 2) {
            i2 = (i - 1) - i2;
        }
        return ((w3g96lv8) ((View) this.ja66gqu.get(i2)).getLayoutParams()).fxug2rdnfo;
    }

    public int getCurrentItem() {
        return this.ef5tn1cvshg414;
    }

    public int getOffscreenPageLimit() {
        return this.aecbla89ntoa8;
    }

    public int getPageMargin() {
        return this.bpogj6;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.xf1q4c = true;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        removeCallbacks(this.j4ksty);
        super.onDetachedFromWindow();
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        float f;
        super.onDraw(canvas);
        if (this.bpogj6 > 0 && this.ca2ssr26fefu != null && this.usuayu88rw4.size() > 0 && this.lg71ytkvzw != null) {
            int scrollX = getScrollX();
            int width = getWidth();
            float f2 = ((float) this.bpogj6) / ((float) width);
            a5uhwh1 a5uhwh12 = (a5uhwh1) this.usuayu88rw4.get(0);
            float f3 = a5uhwh12.usuayu88rw4;
            int size = this.usuayu88rw4.size();
            int i = a5uhwh12.ozpoxuz523b2;
            int i2 = ((a5uhwh1) this.usuayu88rw4.get(size - 1)).ozpoxuz523b2;
            int i3 = 0;
            int i4 = i;
            while (i4 < i2) {
                while (i4 > a5uhwh12.ozpoxuz523b2 && i3 < size) {
                    i3++;
                    a5uhwh12 = (a5uhwh1) this.usuayu88rw4.get(i3);
                }
                if (i4 == a5uhwh12.ozpoxuz523b2) {
                    f = (a5uhwh12.usuayu88rw4 + a5uhwh12.uin6g3d5rqgcbs) * ((float) width);
                    f3 = a5uhwh12.usuayu88rw4 + a5uhwh12.uin6g3d5rqgcbs + f2;
                } else {
                    float ttmhx72 = this.lg71ytkvzw.ttmhx7(i4);
                    f = (f3 + ttmhx72) * ((float) width);
                    f3 += ttmhx72 + f2;
                }
                if (((float) this.bpogj6) + f > ((float) scrollX)) {
                    this.ca2ssr26fefu.setBounds((int) f, this.flawb66z00q, (int) (((float) this.bpogj6) + f + 0.5f), this.k3jokks5k5);
                    this.ca2ssr26fefu.draw(canvas);
                }
                if (f <= ((float) (scrollX + width))) {
                    i4++;
                } else {
                    return;
                }
            }
        }
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction() & 255;
        if (action == 3 || action == 1) {
            this.zs1ge47fq1dgv5 = false;
            this.xbcow1jyae = false;
            this.oziax9tu6k = -1;
            if (this.ty7df019s == null) {
                return false;
            }
            this.ty7df019s.recycle();
            this.ty7df019s = null;
            return false;
        }
        if (action != 0) {
            if (this.zs1ge47fq1dgv5) {
                return true;
            }
            if (this.xbcow1jyae) {
                return false;
            }
        }
        switch (action) {
            case 0:
                float x = motionEvent.getX();
                this.bjcn50q4e9 = x;
                this.wg4f90m80dyc0s = x;
                float y = motionEvent.getY();
                this.b6p1j7hoons8 = y;
                this.jaztd5t99d = y;
                this.oziax9tu6k = rulrdod1midre.ozpoxuz523b2(motionEvent, 0);
                this.xbcow1jyae = false;
                this.mhtc4dliin7r.computeScrollOffset();
                if (this.yry0gjw5rm0 == 2 && Math.abs(this.mhtc4dliin7r.getFinalX() - this.mhtc4dliin7r.getCurrX()) > this.ug2s4y) {
                    this.mhtc4dliin7r.abortAnimation();
                    this.sgnd7s4 = false;
                    ozpoxuz523b2();
                    this.zs1ge47fq1dgv5 = true;
                    cehyzt7dw(true);
                    setScrollState(1);
                    break;
                } else {
                    ttmhx7(false);
                    this.zs1ge47fq1dgv5 = false;
                    break;
                }
                break;
            case 2:
                int i = this.oziax9tu6k;
                if (i != -1) {
                    int ttmhx72 = rulrdod1midre.ttmhx7(motionEvent, i);
                    float cehyzt7dw2 = rulrdod1midre.cehyzt7dw(motionEvent, ttmhx72);
                    float f = cehyzt7dw2 - this.wg4f90m80dyc0s;
                    float abs = Math.abs(f);
                    float uin6g3d5rqgcbs2 = rulrdod1midre.uin6g3d5rqgcbs(motionEvent, ttmhx72);
                    float abs2 = Math.abs(uin6g3d5rqgcbs2 - this.b6p1j7hoons8);
                    if (f == 0.0f || ttmhx7(this.wg4f90m80dyc0s, f) || !ttmhx7(this, false, (int) f, (int) cehyzt7dw2, (int) uin6g3d5rqgcbs2)) {
                        if (abs > ((float) this.s6o869vduri) && 0.5f * abs > abs2) {
                            this.zs1ge47fq1dgv5 = true;
                            cehyzt7dw(true);
                            setScrollState(1);
                            this.wg4f90m80dyc0s = f > 0.0f ? this.bjcn50q4e9 + ((float) this.s6o869vduri) : this.bjcn50q4e9 - ((float) this.s6o869vduri);
                            this.jaztd5t99d = uin6g3d5rqgcbs2;
                            setScrollingCacheEnabled(true);
                        } else if (abs2 > ((float) this.s6o869vduri)) {
                            this.xbcow1jyae = true;
                        }
                        if (this.zs1ge47fq1dgv5 && ozpoxuz523b2(cehyzt7dw2)) {
                            ijavw7l1x.ozpoxuz523b2(this);
                            break;
                        }
                    } else {
                        this.wg4f90m80dyc0s = cehyzt7dw2;
                        this.jaztd5t99d = uin6g3d5rqgcbs2;
                        this.xbcow1jyae = true;
                        return false;
                    }
                }
                break;
            case 6:
                ttmhx7(motionEvent);
                break;
        }
        if (this.ty7df019s == null) {
            this.ty7df019s = VelocityTracker.obtain();
        }
        this.ty7df019s.addMovement(motionEvent);
        return this.zs1ge47fq1dgv5;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.ttmhx7(int, boolean, int, boolean):void
     arg types: [int, int, int, int]
     candidates:
      android.support.v4.view.ViewPager.ttmhx7(int, float, int, int):int
      android.support.v4.view.ViewPager.ttmhx7(int, int, int, int):void
      android.support.v4.view.ViewPager.ttmhx7(int, boolean, boolean, int):void
      android.support.v4.view.ViewPager.ttmhx7(int, boolean, int, boolean):void */
    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        a5uhwh1 ttmhx72;
        int i5;
        int i6;
        int i7;
        int measuredHeight;
        int i8;
        int i9;
        int childCount = getChildCount();
        int i10 = i3 - i;
        int i11 = i4 - i2;
        int paddingLeft = getPaddingLeft();
        int paddingTop = getPaddingTop();
        int paddingRight = getPaddingRight();
        int paddingBottom = getPaddingBottom();
        int scrollX = getScrollX();
        int i12 = 0;
        int i13 = 0;
        while (i13 < childCount) {
            View childAt = getChildAt(i13);
            if (childAt.getVisibility() != 8) {
                w3g96lv8 w3g96lv82 = (w3g96lv8) childAt.getLayoutParams();
                if (w3g96lv82.ttmhx7) {
                    int i14 = w3g96lv82.ozpoxuz523b2 & 7;
                    int i15 = w3g96lv82.ozpoxuz523b2 & 112;
                    switch (i14) {
                        case 1:
                            i7 = Math.max((i10 - childAt.getMeasuredWidth()) / 2, paddingLeft);
                            break;
                        case 2:
                        case 4:
                        default:
                            i7 = paddingLeft;
                            break;
                        case 3:
                            i7 = paddingLeft;
                            paddingLeft = childAt.getMeasuredWidth() + paddingLeft;
                            break;
                        case 5:
                            int measuredWidth = (i10 - paddingRight) - childAt.getMeasuredWidth();
                            paddingRight += childAt.getMeasuredWidth();
                            i7 = measuredWidth;
                            break;
                    }
                    switch (i15) {
                        case 16:
                            measuredHeight = Math.max((i11 - childAt.getMeasuredHeight()) / 2, paddingTop);
                            int i16 = paddingBottom;
                            i8 = paddingTop;
                            i9 = i16;
                            break;
                        case 48:
                            int measuredHeight2 = childAt.getMeasuredHeight() + paddingTop;
                            int i17 = paddingTop;
                            i9 = paddingBottom;
                            i8 = measuredHeight2;
                            measuredHeight = i17;
                            break;
                        case 80:
                            measuredHeight = (i11 - paddingBottom) - childAt.getMeasuredHeight();
                            int measuredHeight3 = paddingBottom + childAt.getMeasuredHeight();
                            i8 = paddingTop;
                            i9 = measuredHeight3;
                            break;
                        default:
                            measuredHeight = paddingTop;
                            int i18 = paddingBottom;
                            i8 = paddingTop;
                            i9 = i18;
                            break;
                    }
                    int i19 = i7 + scrollX;
                    childAt.layout(i19, measuredHeight, childAt.getMeasuredWidth() + i19, childAt.getMeasuredHeight() + measuredHeight);
                    i5 = i12 + 1;
                    i6 = i8;
                    paddingBottom = i9;
                    i13++;
                    paddingLeft = paddingLeft;
                    paddingRight = paddingRight;
                    paddingTop = i6;
                    i12 = i5;
                }
            }
            i5 = i12;
            i6 = paddingTop;
            i13++;
            paddingLeft = paddingLeft;
            paddingRight = paddingRight;
            paddingTop = i6;
            i12 = i5;
        }
        int i20 = (i10 - paddingLeft) - paddingRight;
        for (int i21 = 0; i21 < childCount; i21++) {
            View childAt2 = getChildAt(i21);
            if (childAt2.getVisibility() != 8) {
                w3g96lv8 w3g96lv83 = (w3g96lv8) childAt2.getLayoutParams();
                if (!w3g96lv83.ttmhx7 && (ttmhx72 = ttmhx7(childAt2)) != null) {
                    int i22 = ((int) (ttmhx72.usuayu88rw4 * ((float) i20))) + paddingLeft;
                    if (w3g96lv83.uin6g3d5rqgcbs) {
                        w3g96lv83.uin6g3d5rqgcbs = false;
                        childAt2.measure(View.MeasureSpec.makeMeasureSpec((int) (w3g96lv83.cehyzt7dw * ((float) i20)), 1073741824), View.MeasureSpec.makeMeasureSpec((i11 - paddingTop) - paddingBottom, 1073741824));
                    }
                    childAt2.layout(i22, paddingTop, childAt2.getMeasuredWidth() + i22, childAt2.getMeasuredHeight() + paddingTop);
                }
            }
        }
        this.flawb66z00q = paddingTop;
        this.k3jokks5k5 = i11 - paddingBottom;
        this.fx9gujks = i12;
        if (this.xf1q4c) {
            ttmhx7(this.ef5tn1cvshg414, false, 0, false);
        }
        this.xf1q4c = false;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00a0  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00b4  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onMeasure(int r14, int r15) {
        /*
            r13 = this;
            r0 = 0
            int r0 = getDefaultSize(r0, r14)
            r1 = 0
            int r1 = getDefaultSize(r1, r15)
            r13.setMeasuredDimension(r0, r1)
            int r0 = r13.getMeasuredWidth()
            int r1 = r0 / 10
            int r2 = r13.ftlyjgoncub6q
            int r1 = java.lang.Math.min(r1, r2)
            r13.ijavw7l1x = r1
            int r1 = r13.getPaddingLeft()
            int r0 = r0 - r1
            int r1 = r13.getPaddingRight()
            int r3 = r0 - r1
            int r0 = r13.getMeasuredHeight()
            int r1 = r13.getPaddingTop()
            int r0 = r0 - r1
            int r1 = r13.getPaddingBottom()
            int r5 = r0 - r1
            int r9 = r13.getChildCount()
            r0 = 0
            r8 = r0
        L_0x003b:
            if (r8 >= r9) goto L_0x00bc
            android.view.View r10 = r13.getChildAt(r8)
            int r0 = r10.getVisibility()
            r1 = 8
            if (r0 == r1) goto L_0x00a5
            android.view.ViewGroup$LayoutParams r0 = r10.getLayoutParams()
            android.support.v4.view.w3g96lv8 r0 = (android.support.v4.view.w3g96lv8) r0
            if (r0 == 0) goto L_0x00a5
            boolean r1 = r0.ttmhx7
            if (r1 == 0) goto L_0x00a5
            int r1 = r0.ozpoxuz523b2
            r6 = r1 & 7
            int r1 = r0.ozpoxuz523b2
            r4 = r1 & 112(0x70, float:1.57E-43)
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r1 = -2147483648(0xffffffff80000000, float:-0.0)
            r7 = 48
            if (r4 == r7) goto L_0x0069
            r7 = 80
            if (r4 != r7) goto L_0x00a9
        L_0x0069:
            r4 = 1
            r7 = r4
        L_0x006b:
            r4 = 3
            if (r6 == r4) goto L_0x0071
            r4 = 5
            if (r6 != r4) goto L_0x00ac
        L_0x0071:
            r4 = 1
            r6 = r4
        L_0x0073:
            if (r7 == 0) goto L_0x00af
            r2 = 1073741824(0x40000000, float:2.0)
        L_0x0077:
            int r4 = r0.width
            r11 = -2
            if (r4 == r11) goto L_0x010f
            r4 = 1073741824(0x40000000, float:2.0)
            int r2 = r0.width
            r11 = -1
            if (r2 == r11) goto L_0x010c
            int r2 = r0.width
        L_0x0085:
            int r11 = r0.height
            r12 = -2
            if (r11 == r12) goto L_0x010a
            r1 = 1073741824(0x40000000, float:2.0)
            int r11 = r0.height
            r12 = -1
            if (r11 == r12) goto L_0x010a
            int r0 = r0.height
        L_0x0093:
            int r2 = android.view.View.MeasureSpec.makeMeasureSpec(r2, r4)
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r0, r1)
            r10.measure(r2, r0)
            if (r7 == 0) goto L_0x00b4
            int r0 = r10.getMeasuredHeight()
            int r5 = r5 - r0
        L_0x00a5:
            int r0 = r8 + 1
            r8 = r0
            goto L_0x003b
        L_0x00a9:
            r4 = 0
            r7 = r4
            goto L_0x006b
        L_0x00ac:
            r4 = 0
            r6 = r4
            goto L_0x0073
        L_0x00af:
            if (r6 == 0) goto L_0x0077
            r1 = 1073741824(0x40000000, float:2.0)
            goto L_0x0077
        L_0x00b4:
            if (r6 == 0) goto L_0x00a5
            int r0 = r10.getMeasuredWidth()
            int r3 = r3 - r0
            goto L_0x00a5
        L_0x00bc:
            r0 = 1073741824(0x40000000, float:2.0)
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r3, r0)
            r13.mqnmk83l0o = r0
            r0 = 1073741824(0x40000000, float:2.0)
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r5, r0)
            r13.lqwegpi5 = r0
            r0 = 1
            r13.ol99ycz2wbkd = r0
            r13.ozpoxuz523b2()
            r0 = 0
            r13.ol99ycz2wbkd = r0
            int r2 = r13.getChildCount()
            r0 = 0
            r1 = r0
        L_0x00db:
            if (r1 >= r2) goto L_0x0109
            android.view.View r4 = r13.getChildAt(r1)
            int r0 = r4.getVisibility()
            r5 = 8
            if (r0 == r5) goto L_0x0105
            android.view.ViewGroup$LayoutParams r0 = r4.getLayoutParams()
            android.support.v4.view.w3g96lv8 r0 = (android.support.v4.view.w3g96lv8) r0
            if (r0 == 0) goto L_0x00f5
            boolean r5 = r0.ttmhx7
            if (r5 != 0) goto L_0x0105
        L_0x00f5:
            float r5 = (float) r3
            float r0 = r0.cehyzt7dw
            float r0 = r0 * r5
            int r0 = (int) r0
            r5 = 1073741824(0x40000000, float:2.0)
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r0, r5)
            int r5 = r13.lqwegpi5
            r4.measure(r0, r5)
        L_0x0105:
            int r0 = r1 + 1
            r1 = r0
            goto L_0x00db
        L_0x0109:
            return
        L_0x010a:
            r0 = r5
            goto L_0x0093
        L_0x010c:
            r2 = r3
            goto L_0x0085
        L_0x010f:
            r4 = r2
            r2 = r3
            goto L_0x0085
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.view.ViewPager.onMeasure(int, int):void");
    }

    /* access modifiers changed from: protected */
    public boolean onRequestFocusInDescendants(int i, Rect rect) {
        int i2;
        a5uhwh1 ttmhx72;
        int i3 = -1;
        int childCount = getChildCount();
        if ((i & 2) != 0) {
            i3 = 1;
            i2 = 0;
        } else {
            i2 = childCount - 1;
            childCount = -1;
        }
        while (i2 != childCount) {
            View childAt = getChildAt(i2);
            if (childAt.getVisibility() == 0 && (ttmhx72 = ttmhx7(childAt)) != null && ttmhx72.ozpoxuz523b2 == this.ef5tn1cvshg414 && childAt.requestFocus(i, rect)) {
                return true;
            }
            i2 += i3;
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.ttmhx7(int, boolean, boolean):void
     arg types: [int, int, int]
     candidates:
      android.support.v4.view.ViewPager.ttmhx7(android.support.v4.view.a5uhwh1, int, android.support.v4.view.a5uhwh1):void
      android.support.v4.view.ViewPager.ttmhx7(int, float, int):void
      android.support.v4.view.ViewPager.ttmhx7(int, int, int):void
      android.support.v4.view.ViewPager.ttmhx7(int, boolean, boolean):void */
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        if (this.lg71ytkvzw != null) {
            this.lg71ytkvzw.ttmhx7(savedState.ozpoxuz523b2, savedState.cehyzt7dw);
            ttmhx7(savedState.ttmhx7, false, true);
            return;
        }
        this.b5zlaptmyxarl = savedState.ttmhx7;
        this.iux03f6yieb = savedState.ozpoxuz523b2;
        this.ay6ebym1yp0qgk = savedState.cehyzt7dw;
    }

    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.ttmhx7 = this.ef5tn1cvshg414;
        if (this.lg71ytkvzw != null) {
            savedState.ozpoxuz523b2 = this.lg71ytkvzw.ozpoxuz523b2();
        }
        return savedState;
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        if (i != i3) {
            ttmhx7(i, i3, this.bpogj6, this.bpogj6);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.ttmhx7(int, boolean, boolean, int):void
     arg types: [int, int, int, int]
     candidates:
      android.support.v4.view.ViewPager.ttmhx7(int, float, int, int):int
      android.support.v4.view.ViewPager.ttmhx7(int, int, int, int):void
      android.support.v4.view.ViewPager.ttmhx7(int, boolean, int, boolean):void
      android.support.v4.view.ViewPager.ttmhx7(int, boolean, boolean, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.ttmhx7(int, boolean, int, boolean):void
     arg types: [int, int, int, int]
     candidates:
      android.support.v4.view.ViewPager.ttmhx7(int, float, int, int):int
      android.support.v4.view.ViewPager.ttmhx7(int, int, int, int):void
      android.support.v4.view.ViewPager.ttmhx7(int, boolean, boolean, int):void
      android.support.v4.view.ViewPager.ttmhx7(int, boolean, int, boolean):void */
    public boolean onTouchEvent(MotionEvent motionEvent) {
        boolean z = false;
        if (this.rob6sujr97) {
            return true;
        }
        if (motionEvent.getAction() == 0 && motionEvent.getEdgeFlags() != 0) {
            return false;
        }
        if (this.lg71ytkvzw == null || this.lg71ytkvzw.ttmhx7() == 0) {
            return false;
        }
        if (this.ty7df019s == null) {
            this.ty7df019s = VelocityTracker.obtain();
        }
        this.ty7df019s.addMovement(motionEvent);
        switch (motionEvent.getAction() & 255) {
            case 0:
                this.mhtc4dliin7r.abortAnimation();
                this.sgnd7s4 = false;
                ozpoxuz523b2();
                float x = motionEvent.getX();
                this.bjcn50q4e9 = x;
                this.wg4f90m80dyc0s = x;
                float y = motionEvent.getY();
                this.b6p1j7hoons8 = y;
                this.jaztd5t99d = y;
                this.oziax9tu6k = rulrdod1midre.ozpoxuz523b2(motionEvent, 0);
                break;
            case 1:
                if (this.zs1ge47fq1dgv5) {
                    VelocityTracker velocityTracker = this.ty7df019s;
                    velocityTracker.computeCurrentVelocity(1000, (float) this.yxqgts35zbsb);
                    int ttmhx72 = (int) sgnd7s4.ttmhx7(velocityTracker, this.oziax9tu6k);
                    this.sgnd7s4 = true;
                    int clientWidth = getClientWidth();
                    int scrollX = getScrollX();
                    a5uhwh1 lg71ytkvzw2 = lg71ytkvzw();
                    ttmhx7(ttmhx7(lg71ytkvzw2.ozpoxuz523b2, ((((float) scrollX) / ((float) clientWidth)) - lg71ytkvzw2.usuayu88rw4) / lg71ytkvzw2.uin6g3d5rqgcbs, ttmhx72, (int) (rulrdod1midre.cehyzt7dw(motionEvent, rulrdod1midre.ttmhx7(motionEvent, this.oziax9tu6k)) - this.bjcn50q4e9)), true, true, ttmhx72);
                    this.oziax9tu6k = -1;
                    ef5tn1cvshg414();
                    z = this.j72htm5.cehyzt7dw() | this.w5mzcxwa3kamml.cehyzt7dw();
                    break;
                }
                break;
            case 2:
                if (!this.zs1ge47fq1dgv5) {
                    int ttmhx73 = rulrdod1midre.ttmhx7(motionEvent, this.oziax9tu6k);
                    float cehyzt7dw2 = rulrdod1midre.cehyzt7dw(motionEvent, ttmhx73);
                    float abs = Math.abs(cehyzt7dw2 - this.wg4f90m80dyc0s);
                    float uin6g3d5rqgcbs2 = rulrdod1midre.uin6g3d5rqgcbs(motionEvent, ttmhx73);
                    float abs2 = Math.abs(uin6g3d5rqgcbs2 - this.jaztd5t99d);
                    if (abs > ((float) this.s6o869vduri) && abs > abs2) {
                        this.zs1ge47fq1dgv5 = true;
                        cehyzt7dw(true);
                        this.wg4f90m80dyc0s = cehyzt7dw2 - this.bjcn50q4e9 > 0.0f ? this.bjcn50q4e9 + ((float) this.s6o869vduri) : this.bjcn50q4e9 - ((float) this.s6o869vduri);
                        this.jaztd5t99d = uin6g3d5rqgcbs2;
                        setScrollState(1);
                        setScrollingCacheEnabled(true);
                        ViewParent parent = getParent();
                        if (parent != null) {
                            parent.requestDisallowInterceptTouchEvent(true);
                        }
                    }
                }
                if (this.zs1ge47fq1dgv5) {
                    z = false | ozpoxuz523b2(rulrdod1midre.cehyzt7dw(motionEvent, rulrdod1midre.ttmhx7(motionEvent, this.oziax9tu6k)));
                    break;
                }
                break;
            case 3:
                if (this.zs1ge47fq1dgv5) {
                    ttmhx7(this.ef5tn1cvshg414, true, 0, false);
                    this.oziax9tu6k = -1;
                    ef5tn1cvshg414();
                    z = this.j72htm5.cehyzt7dw() | this.w5mzcxwa3kamml.cehyzt7dw();
                    break;
                }
                break;
            case 5:
                int ozpoxuz523b22 = rulrdod1midre.ozpoxuz523b2(motionEvent);
                this.wg4f90m80dyc0s = rulrdod1midre.cehyzt7dw(motionEvent, ozpoxuz523b22);
                this.oziax9tu6k = rulrdod1midre.ozpoxuz523b2(motionEvent, ozpoxuz523b22);
                break;
            case 6:
                ttmhx7(motionEvent);
                this.wg4f90m80dyc0s = rulrdod1midre.cehyzt7dw(motionEvent, rulrdod1midre.ttmhx7(motionEvent, this.oziax9tu6k));
                break;
        }
        if (z) {
            ijavw7l1x.ozpoxuz523b2(this);
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public a5uhwh1 ozpoxuz523b2(int i) {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= this.usuayu88rw4.size()) {
                return null;
            }
            a5uhwh1 a5uhwh12 = (a5uhwh1) this.usuayu88rw4.get(i3);
            if (a5uhwh12.ozpoxuz523b2 == i) {
                return a5uhwh12;
            }
            i2 = i3 + 1;
        }
    }

    /* access modifiers changed from: package-private */
    public a5uhwh1 ozpoxuz523b2(View view) {
        while (true) {
            ViewParent parent = view.getParent();
            if (parent == this) {
                return ttmhx7(view);
            }
            if (parent == null || !(parent instanceof View)) {
                return null;
            }
            view = (View) parent;
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public void ozpoxuz523b2() {
        ttmhx7(this.ef5tn1cvshg414);
    }

    public void removeView(View view) {
        if (this.ol99ycz2wbkd) {
            removeViewInLayout(view);
        } else {
            super.removeView(view);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.eyli1ymagd3o.ttmhx7(android.view.ViewGroup, int, java.lang.Object):void
     arg types: [android.support.v4.view.ViewPager, int, java.lang.Object]
     candidates:
      android.support.v4.view.eyli1ymagd3o.ttmhx7(android.view.View, int, java.lang.Object):void
      android.support.v4.view.eyli1ymagd3o.ttmhx7(android.view.ViewGroup, int, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.ttmhx7(int, boolean, boolean):void
     arg types: [int, int, int]
     candidates:
      android.support.v4.view.ViewPager.ttmhx7(android.support.v4.view.a5uhwh1, int, android.support.v4.view.a5uhwh1):void
      android.support.v4.view.ViewPager.ttmhx7(int, float, int):void
      android.support.v4.view.ViewPager.ttmhx7(int, int, int):void
      android.support.v4.view.ViewPager.ttmhx7(int, boolean, boolean):void */
    public void setAdapter(eyli1ymagd3o eyli1ymagd3o2) {
        if (this.lg71ytkvzw != null) {
            this.lg71ytkvzw.ozpoxuz523b2(this.oc9mgl157cp);
            this.lg71ytkvzw.ttmhx7((ViewGroup) this);
            for (int i = 0; i < this.usuayu88rw4.size(); i++) {
                a5uhwh1 a5uhwh12 = (a5uhwh1) this.usuayu88rw4.get(i);
                this.lg71ytkvzw.ttmhx7((ViewGroup) this, a5uhwh12.ozpoxuz523b2, a5uhwh12.ttmhx7);
            }
            this.lg71ytkvzw.ozpoxuz523b2((ViewGroup) this);
            this.usuayu88rw4.clear();
            fxug2rdnfo();
            this.ef5tn1cvshg414 = 0;
            scrollTo(0, 0);
        }
        eyli1ymagd3o eyli1ymagd3o3 = this.lg71ytkvzw;
        this.lg71ytkvzw = eyli1ymagd3o2;
        this.ozpoxuz523b2 = 0;
        if (this.lg71ytkvzw != null) {
            if (this.oc9mgl157cp == null) {
                this.oc9mgl157cp = new ja66gqu(this, null);
            }
            this.lg71ytkvzw.ttmhx7((DataSetObserver) this.oc9mgl157cp);
            this.sgnd7s4 = false;
            boolean z = this.xf1q4c;
            this.xf1q4c = true;
            this.ozpoxuz523b2 = this.lg71ytkvzw.ttmhx7();
            if (this.b5zlaptmyxarl >= 0) {
                this.lg71ytkvzw.ttmhx7(this.iux03f6yieb, this.ay6ebym1yp0qgk);
                ttmhx7(this.b5zlaptmyxarl, false, true);
                this.b5zlaptmyxarl = -1;
                this.iux03f6yieb = null;
                this.ay6ebym1yp0qgk = null;
            } else if (!z) {
                ozpoxuz523b2();
            } else {
                requestLayout();
            }
        }
        if (this.w3g96lv8 != null && eyli1ymagd3o3 != eyli1ymagd3o2) {
            this.w3g96lv8.ttmhx7(eyli1ymagd3o3, eyli1ymagd3o2);
        }
    }

    /* access modifiers changed from: package-private */
    public void setChildrenDrawingOrderEnabledCompat(boolean z) {
        if (Build.VERSION.SDK_INT >= 7) {
            if (this.olqqn4x4 == null) {
                Class<ViewGroup> cls = ViewGroup.class;
                try {
                    this.olqqn4x4 = cls.getDeclaredMethod("setChildrenDrawingOrderEnabled", Boolean.TYPE);
                } catch (NoSuchMethodException e) {
                    Log.e("ViewPager", "Can't find setChildrenDrawingOrderEnabled", e);
                }
            }
            try {
                this.olqqn4x4.invoke(this, Boolean.valueOf(z));
            } catch (Exception e2) {
                Log.e("ViewPager", "Error changing children drawing order", e2);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.ttmhx7(int, boolean, boolean):void
     arg types: [int, boolean, int]
     candidates:
      android.support.v4.view.ViewPager.ttmhx7(android.support.v4.view.a5uhwh1, int, android.support.v4.view.a5uhwh1):void
      android.support.v4.view.ViewPager.ttmhx7(int, float, int):void
      android.support.v4.view.ViewPager.ttmhx7(int, int, int):void
      android.support.v4.view.ViewPager.ttmhx7(int, boolean, boolean):void */
    public void setCurrentItem(int i) {
        this.sgnd7s4 = false;
        ttmhx7(i, !this.xf1q4c, false);
    }

    public void setOffscreenPageLimit(int i) {
        if (i < 1) {
            Log.w("ViewPager", "Requested offscreen page limit " + i + " too small; defaulting to " + 1);
            i = 1;
        }
        if (i != this.aecbla89ntoa8) {
            this.aecbla89ntoa8 = i;
            ozpoxuz523b2();
        }
    }

    /* access modifiers changed from: package-private */
    public void setOnAdapterChangeListener(o9ph3xbm2yk6g o9ph3xbm2yk6g2) {
        this.w3g96lv8 = o9ph3xbm2yk6g2;
    }

    public void setOnPageChangeListener(olqqn4x4 olqqn4x42) {
        this.hajwjku = olqqn4x42;
    }

    public void setPageMargin(int i) {
        int i2 = this.bpogj6;
        this.bpogj6 = i;
        int width = getWidth();
        ttmhx7(width, width, i, i2);
        requestLayout();
    }

    public void setPageMarginDrawable(int i) {
        setPageMarginDrawable(getContext().getResources().getDrawable(i));
    }

    public void setPageMarginDrawable(Drawable drawable) {
        this.ca2ssr26fefu = drawable;
        if (drawable != null) {
            refreshDrawableState();
        }
        setWillNotDraw(drawable == null);
        invalidate();
    }

    /* access modifiers changed from: package-private */
    public float ttmhx7(float f) {
        return (float) Math.sin((double) ((float) (((double) (f - 0.5f)) * 0.4712389167638204d)));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.eyli1ymagd3o.ttmhx7(android.view.ViewGroup, int):java.lang.Object
     arg types: [android.support.v4.view.ViewPager, int]
     candidates:
      android.support.v4.view.eyli1ymagd3o.ttmhx7(android.view.View, int):java.lang.Object
      android.support.v4.view.eyli1ymagd3o.ttmhx7(android.os.Parcelable, java.lang.ClassLoader):void
      android.support.v4.view.eyli1ymagd3o.ttmhx7(android.view.View, java.lang.Object):boolean
      android.support.v4.view.eyli1ymagd3o.ttmhx7(android.view.ViewGroup, int):java.lang.Object */
    /* access modifiers changed from: package-private */
    public a5uhwh1 ttmhx7(int i, int i2) {
        a5uhwh1 a5uhwh12 = new a5uhwh1();
        a5uhwh12.ozpoxuz523b2 = i;
        a5uhwh12.ttmhx7 = this.lg71ytkvzw.ttmhx7((ViewGroup) this, i);
        a5uhwh12.uin6g3d5rqgcbs = this.lg71ytkvzw.ttmhx7(i);
        if (i2 < 0 || i2 >= this.usuayu88rw4.size()) {
            this.usuayu88rw4.add(a5uhwh12);
        } else {
            this.usuayu88rw4.add(i2, a5uhwh12);
        }
        return a5uhwh12;
    }

    /* access modifiers changed from: package-private */
    public a5uhwh1 ttmhx7(View view) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.usuayu88rw4.size()) {
                return null;
            }
            a5uhwh1 a5uhwh12 = (a5uhwh1) this.usuayu88rw4.get(i2);
            if (this.lg71ytkvzw.ttmhx7(view, a5uhwh12.ttmhx7)) {
                return a5uhwh12;
            }
            i = i2 + 1;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.eyli1ymagd3o.ttmhx7(android.view.ViewGroup, int, java.lang.Object):void
     arg types: [android.support.v4.view.ViewPager, int, java.lang.Object]
     candidates:
      android.support.v4.view.eyli1ymagd3o.ttmhx7(android.view.View, int, java.lang.Object):void
      android.support.v4.view.eyli1ymagd3o.ttmhx7(android.view.ViewGroup, int, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.ttmhx7(int, boolean, boolean):void
     arg types: [int, int, int]
     candidates:
      android.support.v4.view.ViewPager.ttmhx7(android.support.v4.view.a5uhwh1, int, android.support.v4.view.a5uhwh1):void
      android.support.v4.view.ViewPager.ttmhx7(int, float, int):void
      android.support.v4.view.ViewPager.ttmhx7(int, int, int):void
      android.support.v4.view.ViewPager.ttmhx7(int, boolean, boolean):void */
    /* access modifiers changed from: package-private */
    public void ttmhx7() {
        int i;
        boolean z;
        int i2;
        boolean z2;
        int ttmhx72 = this.lg71ytkvzw.ttmhx7();
        this.ozpoxuz523b2 = ttmhx72;
        boolean z3 = this.usuayu88rw4.size() < (this.aecbla89ntoa8 * 2) + 1 && this.usuayu88rw4.size() < ttmhx72;
        boolean z4 = false;
        int i3 = this.ef5tn1cvshg414;
        boolean z5 = z3;
        int i4 = 0;
        while (i4 < this.usuayu88rw4.size()) {
            a5uhwh1 a5uhwh12 = (a5uhwh1) this.usuayu88rw4.get(i4);
            int ttmhx73 = this.lg71ytkvzw.ttmhx7(a5uhwh12.ttmhx7);
            if (ttmhx73 == -1) {
                i = i4;
                z = z4;
                i2 = i3;
                z2 = z5;
            } else if (ttmhx73 == -2) {
                this.usuayu88rw4.remove(i4);
                int i5 = i4 - 1;
                if (!z4) {
                    this.lg71ytkvzw.ttmhx7((ViewGroup) this);
                    z4 = true;
                }
                this.lg71ytkvzw.ttmhx7((ViewGroup) this, a5uhwh12.ozpoxuz523b2, a5uhwh12.ttmhx7);
                if (this.ef5tn1cvshg414 == a5uhwh12.ozpoxuz523b2) {
                    i = i5;
                    z = z4;
                    i2 = Math.max(0, Math.min(this.ef5tn1cvshg414, ttmhx72 - 1));
                    z2 = true;
                } else {
                    i = i5;
                    z = z4;
                    i2 = i3;
                    z2 = true;
                }
            } else if (a5uhwh12.ozpoxuz523b2 != ttmhx73) {
                if (a5uhwh12.ozpoxuz523b2 == this.ef5tn1cvshg414) {
                    i3 = ttmhx73;
                }
                a5uhwh12.ozpoxuz523b2 = ttmhx73;
                i = i4;
                z = z4;
                i2 = i3;
                z2 = true;
            } else {
                i = i4;
                z = z4;
                i2 = i3;
                z2 = z5;
            }
            z5 = z2;
            i3 = i2;
            z4 = z;
            i4 = i + 1;
        }
        if (z4) {
            this.lg71ytkvzw.ozpoxuz523b2((ViewGroup) this);
        }
        Collections.sort(this.usuayu88rw4, cehyzt7dw);
        if (z5) {
            int childCount = getChildCount();
            for (int i6 = 0; i6 < childCount; i6++) {
                w3g96lv8 w3g96lv82 = (w3g96lv8) getChildAt(i6).getLayoutParams();
                if (!w3g96lv82.ttmhx7) {
                    w3g96lv82.cehyzt7dw = 0.0f;
                }
            }
            ttmhx7(i3, false, true);
            requestLayout();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.eyli1ymagd3o.ozpoxuz523b2(android.view.ViewGroup, int, java.lang.Object):void
     arg types: [android.support.v4.view.ViewPager, int, java.lang.Object]
     candidates:
      android.support.v4.view.eyli1ymagd3o.ozpoxuz523b2(android.view.View, int, java.lang.Object):void
      android.support.v4.view.eyli1ymagd3o.ozpoxuz523b2(android.view.ViewGroup, int, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.eyli1ymagd3o.ttmhx7(android.view.ViewGroup, int, java.lang.Object):void
     arg types: [android.support.v4.view.ViewPager, int, java.lang.Object]
     candidates:
      android.support.v4.view.eyli1ymagd3o.ttmhx7(android.view.View, int, java.lang.Object):void
      android.support.v4.view.eyli1ymagd3o.ttmhx7(android.view.ViewGroup, int, java.lang.Object):void */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00ff, code lost:
        if (r2.ozpoxuz523b2 == r0.ef5tn1cvshg414) goto L_0x0101;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void ttmhx7(int r19) {
        /*
            r18 = this;
            r3 = 0
            r2 = 2
            r0 = r18
            int r4 = r0.ef5tn1cvshg414
            r0 = r19
            if (r4 == r0) goto L_0x033f
            r0 = r18
            int r2 = r0.ef5tn1cvshg414
            r0 = r19
            if (r2 >= r0) goto L_0x0030
            r2 = 66
        L_0x0014:
            r0 = r18
            int r3 = r0.ef5tn1cvshg414
            r0 = r18
            android.support.v4.view.a5uhwh1 r3 = r0.ozpoxuz523b2(r3)
            r0 = r19
            r1 = r18
            r1.ef5tn1cvshg414 = r0
            r4 = r3
            r3 = r2
        L_0x0026:
            r0 = r18
            android.support.v4.view.eyli1ymagd3o r2 = r0.lg71ytkvzw
            if (r2 != 0) goto L_0x0033
            r18.e8kxjqktk9t()
        L_0x002f:
            return
        L_0x0030:
            r2 = 17
            goto L_0x0014
        L_0x0033:
            r0 = r18
            boolean r2 = r0.sgnd7s4
            if (r2 == 0) goto L_0x003d
            r18.e8kxjqktk9t()
            goto L_0x002f
        L_0x003d:
            android.os.IBinder r2 = r18.getWindowToken()
            if (r2 == 0) goto L_0x002f
            r0 = r18
            android.support.v4.view.eyli1ymagd3o r2 = r0.lg71ytkvzw
            r0 = r18
            r2.ttmhx7(r0)
            r0 = r18
            int r2 = r0.aecbla89ntoa8
            r5 = 0
            r0 = r18
            int r6 = r0.ef5tn1cvshg414
            int r6 = r6 - r2
            int r11 = java.lang.Math.max(r5, r6)
            r0 = r18
            android.support.v4.view.eyli1ymagd3o r5 = r0.lg71ytkvzw
            int r12 = r5.ttmhx7()
            int r5 = r12 + -1
            r0 = r18
            int r6 = r0.ef5tn1cvshg414
            int r2 = r2 + r6
            int r13 = java.lang.Math.min(r5, r2)
            r0 = r18
            int r2 = r0.ozpoxuz523b2
            if (r12 == r2) goto L_0x00da
            android.content.res.Resources r2 = r18.getResources()     // Catch:{ NotFoundException -> 0x00d0 }
            int r3 = r18.getId()     // Catch:{ NotFoundException -> 0x00d0 }
            java.lang.String r2 = r2.getResourceName(r3)     // Catch:{ NotFoundException -> 0x00d0 }
        L_0x007f:
            java.lang.IllegalStateException r3 = new java.lang.IllegalStateException
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "The application's PagerAdapter changed the adapter's contents without calling PagerAdapter#notifyDataSetChanged! Expected adapter item count: "
            java.lang.StringBuilder r4 = r4.append(r5)
            r0 = r18
            int r5 = r0.ozpoxuz523b2
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r5 = ", found: "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r4 = r4.append(r12)
            java.lang.String r5 = " Pager id: "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r2 = r4.append(r2)
            java.lang.String r4 = " Pager class: "
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.Class r4 = r18.getClass()
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r4 = " Problematic adapter: "
            java.lang.StringBuilder r2 = r2.append(r4)
            r0 = r18
            android.support.v4.view.eyli1ymagd3o r4 = r0.lg71ytkvzw
            java.lang.Class r4 = r4.getClass()
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r2 = r2.toString()
            r3.<init>(r2)
            throw r3
        L_0x00d0:
            r2 = move-exception
            int r2 = r18.getId()
            java.lang.String r2 = java.lang.Integer.toHexString(r2)
            goto L_0x007f
        L_0x00da:
            r6 = 0
            r2 = 0
            r5 = r2
        L_0x00dd:
            r0 = r18
            java.util.ArrayList r2 = r0.usuayu88rw4
            int r2 = r2.size()
            if (r5 >= r2) goto L_0x033c
            r0 = r18
            java.util.ArrayList r2 = r0.usuayu88rw4
            java.lang.Object r2 = r2.get(r5)
            android.support.v4.view.a5uhwh1 r2 = (android.support.v4.view.a5uhwh1) r2
            int r7 = r2.ozpoxuz523b2
            r0 = r18
            int r8 = r0.ef5tn1cvshg414
            if (r7 < r8) goto L_0x01cf
            int r7 = r2.ozpoxuz523b2
            r0 = r18
            int r8 = r0.ef5tn1cvshg414
            if (r7 != r8) goto L_0x033c
        L_0x0101:
            if (r2 != 0) goto L_0x0339
            if (r12 <= 0) goto L_0x0339
            r0 = r18
            int r2 = r0.ef5tn1cvshg414
            r0 = r18
            android.support.v4.view.a5uhwh1 r2 = r0.ttmhx7(r2, r5)
            r10 = r2
        L_0x0110:
            if (r10 == 0) goto L_0x0180
            r9 = 0
            int r8 = r5 + -1
            if (r8 < 0) goto L_0x01d4
            r0 = r18
            java.util.ArrayList r2 = r0.usuayu88rw4
            java.lang.Object r2 = r2.get(r8)
            android.support.v4.view.a5uhwh1 r2 = (android.support.v4.view.a5uhwh1) r2
        L_0x0121:
            int r14 = r18.getClientWidth()
            if (r14 > 0) goto L_0x01d7
            r6 = 0
        L_0x0128:
            r0 = r18
            int r7 = r0.ef5tn1cvshg414
            int r7 = r7 + -1
            r16 = r7
            r7 = r9
            r9 = r16
            r17 = r8
            r8 = r5
            r5 = r17
        L_0x0138:
            if (r9 < 0) goto L_0x0142
            int r15 = (r7 > r6 ? 1 : (r7 == r6 ? 0 : -1))
            if (r15 < 0) goto L_0x0216
            if (r9 >= r11) goto L_0x0216
            if (r2 != 0) goto L_0x01e6
        L_0x0142:
            float r6 = r10.uin6g3d5rqgcbs
            int r9 = r8 + 1
            r2 = 1073741824(0x40000000, float:2.0)
            int r2 = (r6 > r2 ? 1 : (r6 == r2 ? 0 : -1))
            if (r2 >= 0) goto L_0x017b
            r0 = r18
            java.util.ArrayList r2 = r0.usuayu88rw4
            int r2 = r2.size()
            if (r9 >= r2) goto L_0x024c
            r0 = r18
            java.util.ArrayList r2 = r0.usuayu88rw4
            java.lang.Object r2 = r2.get(r9)
            android.support.v4.view.a5uhwh1 r2 = (android.support.v4.view.a5uhwh1) r2
            r7 = r2
        L_0x0161:
            if (r14 > 0) goto L_0x024f
            r2 = 0
            r5 = r2
        L_0x0165:
            r0 = r18
            int r2 = r0.ef5tn1cvshg414
            int r2 = r2 + 1
            r16 = r2
            r2 = r7
            r7 = r9
            r9 = r16
        L_0x0171:
            if (r9 >= r12) goto L_0x017b
            int r11 = (r6 > r5 ? 1 : (r6 == r5 ? 0 : -1))
            if (r11 < 0) goto L_0x029a
            if (r9 <= r13) goto L_0x029a
            if (r2 != 0) goto L_0x025c
        L_0x017b:
            r0 = r18
            r0.ttmhx7(r10, r8, r4)
        L_0x0180:
            r0 = r18
            android.support.v4.view.eyli1ymagd3o r4 = r0.lg71ytkvzw
            r0 = r18
            int r5 = r0.ef5tn1cvshg414
            if (r10 == 0) goto L_0x02e8
            java.lang.Object r2 = r10.ttmhx7
        L_0x018c:
            r0 = r18
            r4.ozpoxuz523b2(r0, r5, r2)
            r0 = r18
            android.support.v4.view.eyli1ymagd3o r2 = r0.lg71ytkvzw
            r0 = r18
            r2.ozpoxuz523b2(r0)
            int r5 = r18.getChildCount()
            r2 = 0
            r4 = r2
        L_0x01a0:
            if (r4 >= r5) goto L_0x02eb
            r0 = r18
            android.view.View r6 = r0.getChildAt(r4)
            android.view.ViewGroup$LayoutParams r2 = r6.getLayoutParams()
            android.support.v4.view.w3g96lv8 r2 = (android.support.v4.view.w3g96lv8) r2
            r2.fxug2rdnfo = r4
            boolean r7 = r2.ttmhx7
            if (r7 != 0) goto L_0x01cb
            float r7 = r2.cehyzt7dw
            r8 = 0
            int r7 = (r7 > r8 ? 1 : (r7 == r8 ? 0 : -1))
            if (r7 != 0) goto L_0x01cb
            r0 = r18
            android.support.v4.view.a5uhwh1 r6 = r0.ttmhx7(r6)
            if (r6 == 0) goto L_0x01cb
            float r7 = r6.uin6g3d5rqgcbs
            r2.cehyzt7dw = r7
            int r6 = r6.ozpoxuz523b2
            r2.usuayu88rw4 = r6
        L_0x01cb:
            int r2 = r4 + 1
            r4 = r2
            goto L_0x01a0
        L_0x01cf:
            int r2 = r5 + 1
            r5 = r2
            goto L_0x00dd
        L_0x01d4:
            r2 = 0
            goto L_0x0121
        L_0x01d7:
            r6 = 1073741824(0x40000000, float:2.0)
            float r7 = r10.uin6g3d5rqgcbs
            float r6 = r6 - r7
            int r7 = r18.getPaddingLeft()
            float r7 = (float) r7
            float r15 = (float) r14
            float r7 = r7 / r15
            float r6 = r6 + r7
            goto L_0x0128
        L_0x01e6:
            int r15 = r2.ozpoxuz523b2
            if (r9 != r15) goto L_0x0210
            boolean r15 = r2.cehyzt7dw
            if (r15 != 0) goto L_0x0210
            r0 = r18
            java.util.ArrayList r15 = r0.usuayu88rw4
            r15.remove(r5)
            r0 = r18
            android.support.v4.view.eyli1ymagd3o r15 = r0.lg71ytkvzw
            java.lang.Object r2 = r2.ttmhx7
            r0 = r18
            r15.ttmhx7(r0, r9, r2)
            int r5 = r5 + -1
            int r8 = r8 + -1
            if (r5 < 0) goto L_0x0214
            r0 = r18
            java.util.ArrayList r2 = r0.usuayu88rw4
            java.lang.Object r2 = r2.get(r5)
            android.support.v4.view.a5uhwh1 r2 = (android.support.v4.view.a5uhwh1) r2
        L_0x0210:
            int r9 = r9 + -1
            goto L_0x0138
        L_0x0214:
            r2 = 0
            goto L_0x0210
        L_0x0216:
            if (r2 == 0) goto L_0x0230
            int r15 = r2.ozpoxuz523b2
            if (r9 != r15) goto L_0x0230
            float r2 = r2.uin6g3d5rqgcbs
            float r7 = r7 + r2
            int r5 = r5 + -1
            if (r5 < 0) goto L_0x022e
            r0 = r18
            java.util.ArrayList r2 = r0.usuayu88rw4
            java.lang.Object r2 = r2.get(r5)
            android.support.v4.view.a5uhwh1 r2 = (android.support.v4.view.a5uhwh1) r2
            goto L_0x0210
        L_0x022e:
            r2 = 0
            goto L_0x0210
        L_0x0230:
            int r2 = r5 + 1
            r0 = r18
            android.support.v4.view.a5uhwh1 r2 = r0.ttmhx7(r9, r2)
            float r2 = r2.uin6g3d5rqgcbs
            float r7 = r7 + r2
            int r8 = r8 + 1
            if (r5 < 0) goto L_0x024a
            r0 = r18
            java.util.ArrayList r2 = r0.usuayu88rw4
            java.lang.Object r2 = r2.get(r5)
            android.support.v4.view.a5uhwh1 r2 = (android.support.v4.view.a5uhwh1) r2
            goto L_0x0210
        L_0x024a:
            r2 = 0
            goto L_0x0210
        L_0x024c:
            r7 = 0
            goto L_0x0161
        L_0x024f:
            int r2 = r18.getPaddingRight()
            float r2 = (float) r2
            float r5 = (float) r14
            float r2 = r2 / r5
            r5 = 1073741824(0x40000000, float:2.0)
            float r2 = r2 + r5
            r5 = r2
            goto L_0x0165
        L_0x025c:
            int r11 = r2.ozpoxuz523b2
            if (r9 != r11) goto L_0x0332
            boolean r11 = r2.cehyzt7dw
            if (r11 != 0) goto L_0x0332
            r0 = r18
            java.util.ArrayList r11 = r0.usuayu88rw4
            r11.remove(r7)
            r0 = r18
            android.support.v4.view.eyli1ymagd3o r11 = r0.lg71ytkvzw
            java.lang.Object r2 = r2.ttmhx7
            r0 = r18
            r11.ttmhx7(r0, r9, r2)
            r0 = r18
            java.util.ArrayList r2 = r0.usuayu88rw4
            int r2 = r2.size()
            if (r7 >= r2) goto L_0x0298
            r0 = r18
            java.util.ArrayList r2 = r0.usuayu88rw4
            java.lang.Object r2 = r2.get(r7)
            android.support.v4.view.a5uhwh1 r2 = (android.support.v4.view.a5uhwh1) r2
        L_0x028a:
            r16 = r6
            r6 = r2
            r2 = r16
        L_0x028f:
            int r9 = r9 + 1
            r16 = r2
            r2 = r6
            r6 = r16
            goto L_0x0171
        L_0x0298:
            r2 = 0
            goto L_0x028a
        L_0x029a:
            if (r2 == 0) goto L_0x02c1
            int r11 = r2.ozpoxuz523b2
            if (r9 != r11) goto L_0x02c1
            float r2 = r2.uin6g3d5rqgcbs
            float r6 = r6 + r2
            int r7 = r7 + 1
            r0 = r18
            java.util.ArrayList r2 = r0.usuayu88rw4
            int r2 = r2.size()
            if (r7 >= r2) goto L_0x02bf
            r0 = r18
            java.util.ArrayList r2 = r0.usuayu88rw4
            java.lang.Object r2 = r2.get(r7)
            android.support.v4.view.a5uhwh1 r2 = (android.support.v4.view.a5uhwh1) r2
        L_0x02b9:
            r16 = r6
            r6 = r2
            r2 = r16
            goto L_0x028f
        L_0x02bf:
            r2 = 0
            goto L_0x02b9
        L_0x02c1:
            r0 = r18
            android.support.v4.view.a5uhwh1 r2 = r0.ttmhx7(r9, r7)
            int r7 = r7 + 1
            float r2 = r2.uin6g3d5rqgcbs
            float r6 = r6 + r2
            r0 = r18
            java.util.ArrayList r2 = r0.usuayu88rw4
            int r2 = r2.size()
            if (r7 >= r2) goto L_0x02e6
            r0 = r18
            java.util.ArrayList r2 = r0.usuayu88rw4
            java.lang.Object r2 = r2.get(r7)
            android.support.v4.view.a5uhwh1 r2 = (android.support.v4.view.a5uhwh1) r2
        L_0x02e0:
            r16 = r6
            r6 = r2
            r2 = r16
            goto L_0x028f
        L_0x02e6:
            r2 = 0
            goto L_0x02e0
        L_0x02e8:
            r2 = 0
            goto L_0x018c
        L_0x02eb:
            r18.e8kxjqktk9t()
            boolean r2 = r18.hasFocus()
            if (r2 == 0) goto L_0x002f
            android.view.View r2 = r18.findFocus()
            if (r2 == 0) goto L_0x0330
            r0 = r18
            android.support.v4.view.a5uhwh1 r2 = r0.ozpoxuz523b2(r2)
        L_0x0300:
            if (r2 == 0) goto L_0x030a
            int r2 = r2.ozpoxuz523b2
            r0 = r18
            int r4 = r0.ef5tn1cvshg414
            if (r2 == r4) goto L_0x002f
        L_0x030a:
            r2 = 0
        L_0x030b:
            int r4 = r18.getChildCount()
            if (r2 >= r4) goto L_0x002f
            r0 = r18
            android.view.View r4 = r0.getChildAt(r2)
            r0 = r18
            android.support.v4.view.a5uhwh1 r5 = r0.ttmhx7(r4)
            if (r5 == 0) goto L_0x032d
            int r5 = r5.ozpoxuz523b2
            r0 = r18
            int r6 = r0.ef5tn1cvshg414
            if (r5 != r6) goto L_0x032d
            boolean r4 = r4.requestFocus(r3)
            if (r4 != 0) goto L_0x002f
        L_0x032d:
            int r2 = r2 + 1
            goto L_0x030b
        L_0x0330:
            r2 = 0
            goto L_0x0300
        L_0x0332:
            r16 = r6
            r6 = r2
            r2 = r16
            goto L_0x028f
        L_0x0339:
            r10 = r2
            goto L_0x0110
        L_0x033c:
            r2 = r6
            goto L_0x0101
        L_0x033f:
            r4 = r3
            r3 = r2
            goto L_0x0026
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.view.ViewPager.ttmhx7(int):void");
    }

    /* access modifiers changed from: protected */
    public void ttmhx7(int i, float f, int i2) {
        int measuredWidth;
        int i3;
        int i4;
        if (this.fx9gujks > 0) {
            int scrollX = getScrollX();
            int paddingLeft = getPaddingLeft();
            int paddingRight = getPaddingRight();
            int width = getWidth();
            int childCount = getChildCount();
            int i5 = 0;
            while (i5 < childCount) {
                View childAt = getChildAt(i5);
                w3g96lv8 w3g96lv82 = (w3g96lv8) childAt.getLayoutParams();
                if (!w3g96lv82.ttmhx7) {
                    int i6 = paddingRight;
                    i3 = paddingLeft;
                    i4 = i6;
                } else {
                    switch (w3g96lv82.ozpoxuz523b2 & 7) {
                        case 1:
                            measuredWidth = Math.max((width - childAt.getMeasuredWidth()) / 2, paddingLeft);
                            int i7 = paddingRight;
                            i3 = paddingLeft;
                            i4 = i7;
                            break;
                        case 2:
                        case 4:
                        default:
                            measuredWidth = paddingLeft;
                            int i8 = paddingRight;
                            i3 = paddingLeft;
                            i4 = i8;
                            break;
                        case 3:
                            int width2 = childAt.getWidth() + paddingLeft;
                            int i9 = paddingLeft;
                            i4 = paddingRight;
                            i3 = width2;
                            measuredWidth = i9;
                            break;
                        case 5:
                            measuredWidth = (width - paddingRight) - childAt.getMeasuredWidth();
                            int measuredWidth2 = paddingRight + childAt.getMeasuredWidth();
                            i3 = paddingLeft;
                            i4 = measuredWidth2;
                            break;
                    }
                    int left = (measuredWidth + scrollX) - childAt.getLeft();
                    if (left != 0) {
                        childAt.offsetLeftAndRight(left);
                    }
                }
                i5++;
                int i10 = i4;
                paddingLeft = i3;
                paddingRight = i10;
            }
        }
        if (this.hajwjku != null) {
            this.hajwjku.ttmhx7(i, f, i2);
        }
        if (this.a5uhwh1 != null) {
            this.a5uhwh1.ttmhx7(i, f, i2);
        }
        if (this.o9ph3xbm2yk6g != null) {
            int scrollX2 = getScrollX();
            int childCount2 = getChildCount();
            for (int i11 = 0; i11 < childCount2; i11++) {
                View childAt2 = getChildAt(i11);
                if (!((w3g96lv8) childAt2.getLayoutParams()).ttmhx7) {
                    this.o9ph3xbm2yk6g.ttmhx7(childAt2, ((float) (childAt2.getLeft() - scrollX2)) / ((float) getClientWidth()));
                }
            }
        }
        this.i422h07p3ed = true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    /* access modifiers changed from: package-private */
    public void ttmhx7(int i, int i2, int i3) {
        int abs;
        if (getChildCount() == 0) {
            setScrollingCacheEnabled(false);
            return;
        }
        int scrollX = getScrollX();
        int scrollY = getScrollY();
        int i4 = i - scrollX;
        int i5 = i2 - scrollY;
        if (i4 == 0 && i5 == 0) {
            ttmhx7(false);
            ozpoxuz523b2();
            setScrollState(0);
            return;
        }
        setScrollingCacheEnabled(true);
        setScrollState(2);
        int clientWidth = getClientWidth();
        int i6 = clientWidth / 2;
        float ttmhx72 = (((float) i6) * ttmhx7(Math.min(1.0f, (((float) Math.abs(i4)) * 1.0f) / ((float) clientWidth)))) + ((float) i6);
        int abs2 = Math.abs(i3);
        if (abs2 > 0) {
            abs = Math.round(1000.0f * Math.abs(ttmhx72 / ((float) abs2))) * 4;
        } else {
            abs = (int) (((((float) Math.abs(i4)) / ((((float) clientWidth) * this.lg71ytkvzw.ttmhx7(this.ef5tn1cvshg414)) + ((float) this.bpogj6))) + 1.0f) * 100.0f);
        }
        this.mhtc4dliin7r.startScroll(scrollX, scrollY, i4, i5, Math.min(abs, 600));
        ijavw7l1x.ozpoxuz523b2(this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.ttmhx7(int, boolean, boolean):void
     arg types: [int, boolean, int]
     candidates:
      android.support.v4.view.ViewPager.ttmhx7(android.support.v4.view.a5uhwh1, int, android.support.v4.view.a5uhwh1):void
      android.support.v4.view.ViewPager.ttmhx7(int, float, int):void
      android.support.v4.view.ViewPager.ttmhx7(int, int, int):void
      android.support.v4.view.ViewPager.ttmhx7(int, boolean, boolean):void */
    public void ttmhx7(int i, boolean z) {
        this.sgnd7s4 = false;
        ttmhx7(i, z, false);
    }

    /* access modifiers changed from: package-private */
    public void ttmhx7(int i, boolean z, boolean z2) {
        ttmhx7(i, z, z2, 0);
    }

    /* access modifiers changed from: package-private */
    public void ttmhx7(int i, boolean z, boolean z2, int i2) {
        boolean z3 = false;
        if (this.lg71ytkvzw == null || this.lg71ytkvzw.ttmhx7() <= 0) {
            setScrollingCacheEnabled(false);
        } else if (z2 || this.ef5tn1cvshg414 != i || this.usuayu88rw4.size() == 0) {
            if (i < 0) {
                i = 0;
            } else if (i >= this.lg71ytkvzw.ttmhx7()) {
                i = this.lg71ytkvzw.ttmhx7() - 1;
            }
            int i3 = this.aecbla89ntoa8;
            if (i > this.ef5tn1cvshg414 + i3 || i < this.ef5tn1cvshg414 - i3) {
                for (int i4 = 0; i4 < this.usuayu88rw4.size(); i4++) {
                    ((a5uhwh1) this.usuayu88rw4.get(i4)).cehyzt7dw = true;
                }
            }
            if (this.ef5tn1cvshg414 != i) {
                z3 = true;
            }
            if (this.xf1q4c) {
                this.ef5tn1cvshg414 = i;
                if (z3 && this.hajwjku != null) {
                    this.hajwjku.ttmhx7(i);
                }
                if (z3 && this.a5uhwh1 != null) {
                    this.a5uhwh1.ttmhx7(i);
                }
                requestLayout();
                return;
            }
            ttmhx7(i);
            ttmhx7(i, z, i2, z3);
        } else {
            setScrollingCacheEnabled(false);
        }
    }

    public boolean ttmhx7(KeyEvent keyEvent) {
        if (keyEvent.getAction() != 0) {
            return false;
        }
        switch (keyEvent.getKeyCode()) {
            case 21:
                return cehyzt7dw(17);
            case 22:
                return cehyzt7dw(66);
            case 61:
                if (Build.VERSION.SDK_INT < 11) {
                    return false;
                }
                if (ay6ebym1yp0qgk.ttmhx7(keyEvent)) {
                    return cehyzt7dw(2);
                }
                if (ay6ebym1yp0qgk.ttmhx7(keyEvent, 1)) {
                    return cehyzt7dw(1);
                }
                return false;
            default:
                return false;
        }
    }

    /* access modifiers changed from: protected */
    public boolean ttmhx7(View view, boolean z, int i, int i2, int i3) {
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            int scrollX = view.getScrollX();
            int scrollY = view.getScrollY();
            for (int childCount = viewGroup.getChildCount() - 1; childCount >= 0; childCount--) {
                View childAt = viewGroup.getChildAt(childCount);
                if (i2 + scrollX >= childAt.getLeft() && i2 + scrollX < childAt.getRight() && i3 + scrollY >= childAt.getTop() && i3 + scrollY < childAt.getBottom()) {
                    if (ttmhx7(childAt, true, i, (i2 + scrollX) - childAt.getLeft(), (i3 + scrollY) - childAt.getTop())) {
                        return true;
                    }
                }
            }
        }
        return z && ijavw7l1x.ttmhx7(view, -i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.ViewPager.ttmhx7(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.view.ViewPager.ttmhx7(android.graphics.Rect, android.view.View):android.graphics.Rect
      android.support.v4.view.ViewPager.ttmhx7(float, float):boolean
      android.support.v4.view.ViewPager.ttmhx7(int, int):android.support.v4.view.a5uhwh1
      android.support.v4.view.ViewPager.ttmhx7(int, boolean):void */
    /* access modifiers changed from: package-private */
    public boolean uin6g3d5rqgcbs() {
        if (this.lg71ytkvzw == null || this.ef5tn1cvshg414 >= this.lg71ytkvzw.ttmhx7() - 1) {
            return false;
        }
        ttmhx7(this.ef5tn1cvshg414 + 1, true);
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean verifyDrawable(Drawable drawable) {
        return super.verifyDrawable(drawable) || drawable == this.ca2ssr26fefu;
    }
}
