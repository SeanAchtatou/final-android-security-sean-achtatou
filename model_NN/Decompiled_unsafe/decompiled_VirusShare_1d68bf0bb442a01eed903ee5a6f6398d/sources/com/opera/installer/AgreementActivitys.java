package com.opera.installer;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Button;

public class AgreementActivitys extends Activity {
    private Activity a = this;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.agreements);
        ((Button) findViewById(R.id.disagr)).setOnClickListener(new g(this));
        ((Button) findViewById(R.id.agr)).setOnClickListener(new i(this));
    }
}
