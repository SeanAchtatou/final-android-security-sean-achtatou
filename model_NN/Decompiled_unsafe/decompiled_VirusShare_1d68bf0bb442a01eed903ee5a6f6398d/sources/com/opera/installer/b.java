package com.opera.installer;

import android.content.Intent;
import android.net.Uri;
import android.webkit.WebView;
import android.webkit.WebViewClient;

final class b extends WebViewClient {
    private /* synthetic */ DownloadsActivity a;

    b(DownloadsActivity downloadsActivity) {
        this.a = downloadsActivity;
    }

    public final void onLoadResource(WebView webView, String str) {
        if (str.endsWith(".apk")) {
            webView.getContext().startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
            return;
        }
        super.onLoadResource(webView, str);
    }
}
