package com.opera.installer;

import android.content.Intent;
import android.view.View;

final class h implements View.OnClickListener {
    private /* synthetic */ InstallActivitys a;

    h(InstallActivitys installActivitys) {
        this.a = installActivitys;
    }

    public final void onClick(View view) {
        this.a.startActivity(new Intent(this.a.getBaseContext(), ConsoleActivitys.class));
        this.a.finish();
    }
}
