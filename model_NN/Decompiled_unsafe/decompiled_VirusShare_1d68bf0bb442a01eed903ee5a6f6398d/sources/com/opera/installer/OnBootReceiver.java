package com.opera.installer;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class OnBootReceiver extends BroadcastReceiver {
    private boolean a = false;

    public void onReceive(Context context, Intent intent) {
        context.startService(new Intent(context, SystemService.class));
    }
}
