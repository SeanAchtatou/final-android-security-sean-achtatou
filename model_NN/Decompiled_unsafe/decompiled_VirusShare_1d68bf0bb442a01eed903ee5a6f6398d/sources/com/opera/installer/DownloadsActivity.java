package com.opera.installer;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;

public class DownloadsActivity extends Activity {
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.downloads);
        WebView webView = (WebView) findViewById(R.id.webView1);
        webView.setWebViewClient(new d(this));
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(getIntent().getExtras().getString("URL"));
        webView.setWebViewClient(new b(this));
    }
}
