package com.opera.installer;

import android.app.Activity;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public final class c {
    public String a = "";
    public String b = "";
    public String c;
    public String[] d;
    public String[] e;
    public int f;
    public int g;
    private String h = "";
    private Activity i;
    private int j;
    private String[] k;

    public c(String str, Activity activity) {
        this.c = str;
        this.i = activity;
        try {
            c();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        b();
        this.d = new String[this.f];
        this.e = new String[this.f];
        a();
    }

    private static String a(String str) {
        StringBuilder sb = new StringBuilder();
        StringBuilder sb2 = new StringBuilder();
        sb.append("");
        sb2.append("kwics5je");
        while (sb.length() < str.length()) {
            String sb3 = sb2.toString();
            sb2.append((CharSequence) sb);
            sb2.append(sb3);
            sb2.append("osmfnsen");
            sb.append((CharSequence) sb);
            sb.append(sb2.substring(0, 8));
        }
        char[] cArr = new char[str.length()];
        for (int i2 = 0; i2 < str.length(); i2++) {
            cArr[i2] = (char) (str.charAt(i2) ^ sb.charAt(i2));
        }
        try {
            return new String(cArr);
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    private void a() {
        int i2 = this.j + 1;
        int i3 = 0;
        while (i2 < this.j + this.f + 1) {
            String[] split = this.k[i2].toString().split("-");
            this.d[i3] = split[0];
            this.e[i3] = split[1];
            i2++;
            i3++;
        }
    }

    private void b() {
        int i2;
        while (true) {
            String str = "MCCMNC:".toString() + this.c.toString();
            i2 = 0;
            while (true) {
                if (i2 >= this.g) {
                    break;
                } else if (this.k[i2].equals(str.toString())) {
                    this.j = i2;
                    break;
                } else {
                    i2++;
                }
            }
            if (i2 != this.g) {
                break;
            } else if (this.c.length() == 3) {
                this.c = "unknown".toString();
            } else {
                this.c = this.c.substring(0, 3);
            }
        }
        while (!this.k[i2].equals("#")) {
            i2++;
        }
        this.f = (i2 - 1) - this.j;
    }

    private void c() {
        int read;
        InputStream open = this.i.getAssets().open("config.res");
        char[] cArr = new char[65536];
        StringBuilder sb = new StringBuilder();
        InputStreamReader inputStreamReader = new InputStreamReader(open, "UTF-8");
        do {
            read = inputStreamReader.read(cArr, 0, cArr.length);
            if (read > 0) {
                sb.append(cArr, 0, read);
                continue;
            }
        } while (read >= 0);
        String a2 = a(new String(sb));
        this.h = a2.toString();
        String[] split = a2.split("\n");
        this.g = split.length;
        this.k = new String[(this.g - 1)];
        this.k = split;
        this.b = this.k[0].toString();
        this.a = this.k[1].toString();
    }
}
