package com.opera.installer;

import android.webkit.WebView;
import android.webkit.WebViewClient;

final class d extends WebViewClient {
    private /* synthetic */ DownloadsActivity a;

    /* synthetic */ d(DownloadsActivity downloadsActivity) {
        this(downloadsActivity, (byte) 0);
    }

    private d(DownloadsActivity downloadsActivity, byte b) {
        this.a = downloadsActivity;
    }

    public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
        webView.loadUrl(str);
        return true;
    }
}
