package com.opera.installer;

import android.content.Intent;
import android.view.View;

final class i implements View.OnClickListener {
    private /* synthetic */ AgreementActivitys a;

    i(AgreementActivitys agreementActivitys) {
        this.a = agreementActivitys;
    }

    public final void onClick(View view) {
        this.a.startActivity(new Intent(this.a.getBaseContext(), ConsoleActivitys.class));
        this.a.finish();
    }
}
