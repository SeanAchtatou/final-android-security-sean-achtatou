package com.android.x5a807058;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class q {
    ByteArrayOutputStream a;
    boolean b;
    boolean c;
    private String d;

    public q() {
        this.d = null;
        this.a = new ByteArrayOutputStream();
        this.b = false;
        this.c = false;
        this.d = s.a(30);
    }

    public void a() {
        if (!this.c) {
            try {
                this.a.write(("--" + this.d + "\r\n").getBytes());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        this.c = true;
    }

    public void a(OutputStream outputStream) {
        outputStream.write(this.a.toByteArray());
    }

    public void a(String str, String str2, InputStream inputStream, String str3, boolean z) {
        a();
        try {
            this.a.write(("Content-Disposition: form-data; name=\"" + str + "\"; filename=\"" + str2 + "\"\r\n").getBytes());
            this.a.write(("Content-Type: " + str3 + "\r\n").getBytes());
            this.a.write("Content-Transfer-Encoding: binary\r\n\r\n".getBytes());
            byte[] bArr = new byte[4096];
            while (true) {
                int read = inputStream.read(bArr);
                if (read == -1) {
                    break;
                }
                this.a.write(bArr, 0, read);
            }
            if (!z) {
                this.a.write(("\r\n--" + this.d + "\r\n").getBytes());
            }
            this.a.flush();
            try {
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException e2) {
            e2.printStackTrace();
            try {
                inputStream.close();
            } catch (IOException e3) {
                e3.printStackTrace();
            }
        } catch (Throwable th) {
            try {
                inputStream.close();
            } catch (IOException e4) {
                e4.printStackTrace();
            }
            throw th;
        }
    }

    public void b() {
        if (!this.b) {
            try {
                this.a.write(("\r\n--" + this.d + "--\r\n").getBytes());
            } catch (IOException e) {
                e.printStackTrace();
            }
            this.b = true;
        }
    }

    public long c() {
        b();
        return (long) this.a.toByteArray().length;
    }

    public String d() {
        return "multipart/form-data; boundary=" + this.d;
    }
}
