package com.android.x5a807058;

class l extends h {
    final Runnable b;
    final Runnable c;
    final /* synthetic */ f d;
    /* access modifiers changed from: private */
    public boolean e;
    /* access modifiers changed from: private */
    public boolean f;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    private l(f fVar) {
        super(fVar);
        this.d = fVar;
        this.b = new m(this);
        this.c = new n(this);
    }

    /* access modifiers changed from: package-private */
    public void a() {
        ZActivity.a().runOnUiThread(this.b);
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z) {
        if (z && !this.f) {
            this.e = !this.e;
        }
    }

    /* access modifiers changed from: package-private */
    public void b() {
        ZActivity.a().runOnUiThread(this.c);
    }
}
