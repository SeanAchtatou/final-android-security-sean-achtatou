package com.android.x5a807058;

import java.util.Iterator;
import java.util.LinkedList;

public class f {
    private static int a = 524288000;
    private boolean b;
    /* access modifiers changed from: private */
    public final LinkedList c = new LinkedList();
    private final h[] d;
    private h e;
    /* access modifiers changed from: private */
    public final ae f;

    public f(ae aeVar) {
        this.f = aeVar;
        this.d = new h[4];
        this.d[0] = new o(this);
        this.d[1] = new j(this);
        this.d[2] = new l(this);
        this.d[3] = new p(this);
        b();
    }

    private int a(i iVar) {
        int a2 = iVar.a();
        return a2 + String.valueOf(a2).length() + 1;
    }

    private void a(i iVar, StringBuilder sb) {
        sb.append(iVar.a()).append(' ');
        iVar.a(sb);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void b(com.android.x5a807058.i r2) {
        /*
            r1 = this;
            monitor-enter(r1)
            com.android.x5a807058.h r0 = r1.e     // Catch:{ all -> 0x0017 }
            if (r0 != 0) goto L_0x0007
            monitor-exit(r1)     // Catch:{ all -> 0x0017 }
        L_0x0006:
            return
        L_0x0007:
            java.util.LinkedList r0 = r1.c     // Catch:{ all -> 0x0017 }
            r0.add(r2)     // Catch:{ all -> 0x0017 }
            boolean r0 = r1.b     // Catch:{ all -> 0x0017 }
            if (r0 != 0) goto L_0x0015
            com.android.x5a807058.h r0 = r1.e     // Catch:{ all -> 0x0017 }
            r0.a()     // Catch:{ all -> 0x0017 }
        L_0x0015:
            monitor-exit(r1)     // Catch:{ all -> 0x0017 }
            goto L_0x0006
        L_0x0017:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0017 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.x5a807058.f.b(com.android.x5a807058.i):void");
    }

    /* access modifiers changed from: private */
    public String c() {
        String sb;
        synchronized (this) {
            if (this.c.size() == 0) {
                sb = null;
            } else {
                Iterator it = this.c.iterator();
                int i = 0;
                int i2 = 0;
                while (it.hasNext()) {
                    int a2 = ((i) it.next()).a() + 50;
                    if (i > 0 && i2 + a2 > a && a > 0) {
                        break;
                    }
                    i2 += a2;
                    i++;
                }
                boolean z = i == this.c.size();
                StringBuilder sb2 = new StringBuilder((z ? 0 : 100) + i2);
                for (int i3 = 0; i3 < i; i3++) {
                    i iVar = (i) this.c.removeFirst();
                    if (!z || i3 + 1 != i) {
                        sb2.append("try{");
                        iVar.b(sb2);
                        sb2.append("}finally{");
                    } else {
                        iVar.b(sb2);
                    }
                }
                if (!z) {
                    sb2.append("window.setTimeout(function(){cordova.require('cordova/plugin/android/polling').pollOnce();},0);");
                }
                for (int i4 = z ? 1 : 0; i4 < i; i4++) {
                    sb2.append('}');
                }
                sb = sb2.toString();
            }
        }
        return sb;
    }

    public String a(boolean z) {
        String str = null;
        synchronized (this) {
            if (this.e != null) {
                this.e.a(z);
                if (!this.c.isEmpty()) {
                    Iterator it = this.c.iterator();
                    int i = 0;
                    int i2 = 0;
                    while (it.hasNext()) {
                        int a2 = a((i) it.next());
                        if (i > 0 && i2 + a2 > a && a > 0) {
                            break;
                        }
                        i2 += a2;
                        i++;
                    }
                    StringBuilder sb = new StringBuilder(i2);
                    for (int i3 = 0; i3 < i; i3++) {
                        a((i) this.c.removeFirst(), sb);
                    }
                    if (!this.c.isEmpty()) {
                        sb.append('*');
                    }
                    str = sb.toString();
                }
            }
        }
        return str;
    }

    public void a(int i) {
        if (i >= -1 && i < this.d.length) {
            h hVar = i < 0 ? null : this.d[i];
            if (hVar != this.e) {
                synchronized (this) {
                    this.e = hVar;
                    if (hVar != null) {
                        hVar.b();
                        if (!this.b && !this.c.isEmpty()) {
                            hVar.a();
                        }
                    }
                }
            }
        }
    }

    public void a(d dVar, String str) {
        if (str != null) {
            boolean z = dVar.a() == e.NO_RESULT.ordinal();
            boolean e2 = dVar.e();
            if (!z || !e2) {
                b(new i(dVar, str));
            }
        }
    }

    public void a(String str) {
        b(new i(str));
    }

    public boolean a() {
        return this.e != null;
    }

    public void b() {
        synchronized (this) {
            this.c.clear();
            a(-1);
        }
    }

    public void b(boolean z) {
        if (!this.b || z) {
        }
        this.b = z;
        if (!z) {
            synchronized (this) {
                if (!this.c.isEmpty() && this.e != null) {
                    this.e.a();
                }
            }
        }
    }
}
