package com.android.x5a807058;

class i {
    final String a;
    final d b;

    i(d dVar, String str) {
        if (str == null || dVar == null) {
            throw new NullPointerException();
        }
        this.a = str;
        this.b = dVar;
    }

    i(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.a = str;
        this.b = null;
    }

    /* access modifiers changed from: package-private */
    public int a() {
        if (this.b == null) {
            return this.a.length() + 1;
        }
        int length = String.valueOf(this.b.a()).length() + 2 + 1 + this.a.length() + 1;
        switch (this.b.b()) {
            case 1:
                return length + this.b.d().length() + 1;
            case 2:
            case 4:
            case 5:
            default:
                return length + this.b.c().length();
            case 3:
                return length + this.b.c().length() + 1;
            case 6:
                return length + this.b.c().length() + 1;
            case 7:
                return length + this.b.c().length() + 1;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(StringBuilder sb) {
        boolean z = true;
        if (this.b == null) {
            sb.append('J').append(this.a);
            return;
        }
        int a2 = this.b.a();
        boolean z2 = a2 == e.NO_RESULT.ordinal();
        if (a2 != e.OK.ordinal()) {
            z = false;
        }
        sb.append((z2 || z) ? 'S' : 'F').append(this.b.e() ? '1' : '0').append(a2).append(' ').append(this.a).append(' ');
        switch (this.b.b()) {
            case 1:
                sb.append('s');
                sb.append(this.b.d());
                return;
            case 2:
            case 4:
            case 5:
            default:
                sb.append(this.b.c());
                return;
            case 3:
                sb.append('n').append(this.b.c());
                return;
            case 6:
                sb.append('A');
                sb.append(this.b.c());
                return;
            case 7:
                sb.append('S');
                sb.append(this.b.c());
                return;
        }
    }

    /* access modifiers changed from: package-private */
    public void b(StringBuilder sb) {
        if (this.b == null) {
            sb.append(this.a);
            return;
        }
        int a2 = this.b.a();
        sb.append("cordova.callbackFromNative('").append(this.a).append("',").append(a2 == e.OK.ordinal() || a2 == e.NO_RESULT.ordinal()).append(",").append(a2).append(",[");
        switch (this.b.b()) {
            case 6:
                sb.append("cordova.require('cordova/base64').toArrayBuffer('").append(this.b.c()).append("')");
                break;
            case 7:
                sb.append("atob('").append(this.b.c()).append("')");
                break;
            default:
                sb.append(this.b.c());
                break;
        }
        sb.append("],").append(this.b.e()).append(");");
    }
}
