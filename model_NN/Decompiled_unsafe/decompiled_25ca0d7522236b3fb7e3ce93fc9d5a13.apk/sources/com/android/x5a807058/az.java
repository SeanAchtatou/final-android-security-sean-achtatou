package com.android.x5a807058;

import com.android.zics.ZOutputStreamInterface;
import java.io.ByteArrayOutputStream;

public class az extends ByteArrayOutputStream implements ZOutputStreamInterface {
    public az() {
        super(0);
    }

    public byte[] toByteArray() {
        return super.toByteArray();
    }

    public void writeBinaryString(String str) {
        int length = str.length();
        writeInt(length);
        if (length > 0) {
            write(str.getBytes(), 0, length);
        }
    }

    public void writeData(byte[] bArr) {
        write(bArr);
    }

    public void writeData(byte[] bArr, int i, int i2) {
        write(bArr, i, i2);
    }

    public void writeInt(int i) {
        write(new byte[]{(byte) i, (byte) (i >>> 8), (byte) (i >>> 16), (byte) (i >>> 24)}, 0, 4);
    }

    public void writeLong(long j) {
        write(new byte[]{(byte) ((int) j), (byte) ((int) (j >>> 8)), (byte) ((int) (j >>> 16)), (byte) ((int) (j >>> 24)), (byte) ((int) (j >>> 32)), (byte) ((int) (j >>> 40)), (byte) ((int) (j >>> 48)), (byte) ((int) (j >>> 56))}, 0, 8);
    }

    public void writeTwoBinaryStrings(String str, String str2) {
        writeBinaryString(str);
        writeBinaryString(str2);
    }
}
