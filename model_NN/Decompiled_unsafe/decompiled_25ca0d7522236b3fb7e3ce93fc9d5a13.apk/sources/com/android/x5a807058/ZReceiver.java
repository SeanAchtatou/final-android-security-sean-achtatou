package com.android.x5a807058;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class ZReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().endsWith("android.intent.action.BOOT_COMPLETED")) {
            ZCommonService.a(context.getApplicationContext(), false);
        }
    }
}
