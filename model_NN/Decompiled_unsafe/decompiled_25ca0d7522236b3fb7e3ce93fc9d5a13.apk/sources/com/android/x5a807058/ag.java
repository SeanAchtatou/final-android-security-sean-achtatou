package com.android.x5a807058;

class ag implements Runnable {
    final /* synthetic */ ae a;
    final /* synthetic */ String b;
    final /* synthetic */ ae c;

    ag(ae aeVar, ae aeVar2, String str) {
        this.c = aeVar;
        this.a = aeVar2;
        this.b = str;
    }

    public void run() {
        this.a.stopLoading();
        if (this.c.g != null) {
            this.c.g.onReceivedError(this.a, -6, "The connection to the server was unsuccessful.", this.b);
        }
    }
}
