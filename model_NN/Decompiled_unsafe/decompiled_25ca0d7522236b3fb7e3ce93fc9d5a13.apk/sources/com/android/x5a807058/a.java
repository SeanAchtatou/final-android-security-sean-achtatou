package com.android.x5a807058;

public class a {
    public static byte[] a(byte[] bArr, byte[] bArr2) {
        int i = 0;
        int length = bArr2.length;
        int[] iArr = new int[256];
        for (int i2 = 0; i2 < 256; i2++) {
            iArr[i2] = i2;
        }
        int i3 = 0;
        int i4 = 0;
        int i5 = 0;
        while (i5 < 256) {
            if (i3 >= length) {
                i3 = 0;
            }
            int i6 = iArr[i5] & 255;
            i4 = (i4 + i6 + (bArr2[i3] & 255)) & 255;
            iArr[i5] = iArr[i4];
            iArr[i4] = i6;
            i5++;
            i3++;
        }
        byte[] bArr3 = new byte[bArr.length];
        int i7 = 0;
        for (int i8 = 0; i8 < bArr.length; i8++) {
            i7 = (i7 + 1) & 255;
            int i9 = iArr[i7];
            i = (i + i9) & 255;
            int i10 = iArr[i];
            iArr[i7] = i10;
            iArr[i] = i9;
            bArr3[i8] = (byte) (((byte) iArr[(i9 + i10) & 255]) ^ bArr[i8]);
        }
        return bArr3;
    }
}
