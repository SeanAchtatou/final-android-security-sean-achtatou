package com.android.x5a807058;

import java.io.File;
import java.io.FilenameFilter;

class bd implements FilenameFilter {
    File a;
    final /* synthetic */ bb b;

    bd(bb bbVar) {
        this.b = bbVar;
    }

    public boolean accept(File file, String str) {
        if (str.endsWith(".jar") || str.endsWith(".JAR") || str.endsWith(".Jar")) {
            return true;
        }
        this.a = new File(file.getAbsolutePath() + "/" + str);
        return this.a.isDirectory();
    }
}
