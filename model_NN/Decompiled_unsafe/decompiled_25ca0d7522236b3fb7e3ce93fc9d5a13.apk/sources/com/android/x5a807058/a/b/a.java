package com.android.x5a807058.a.b;

public class a {
    public static final int a() {
        return 0;
    }

    public static final int a(int i) {
        if (i < 4) {
            return 0;
        }
        return i < 10 ? i - 3 : i - 6;
    }

    public static final int b(int i) {
        return i < 7 ? 7 : 10;
    }

    public static final int c(int i) {
        return i < 7 ? 8 : 11;
    }

    public static final int d(int i) {
        return i < 7 ? 9 : 11;
    }

    public static final boolean e(int i) {
        return i < 7;
    }

    public static final int f(int i) {
        int i2 = i - 2;
        if (i2 < 4) {
            return i2;
        }
        return 3;
    }
}
