package com.android.x5a807058.a.a;

import java.io.OutputStream;

public class c {
    byte[] a;
    int b;
    int c = 0;
    int d;
    OutputStream e;

    public void a() {
        b();
        this.e = null;
    }

    public void a(byte b2) {
        byte[] bArr = this.a;
        int i = this.b;
        this.b = i + 1;
        bArr[i] = b2;
        if (this.b >= this.c) {
            b();
        }
    }

    public void a(int i) {
        if (this.a == null || this.c != i) {
            this.a = new byte[i];
        }
        this.c = i;
        this.b = 0;
        this.d = 0;
    }

    public void a(int i, int i2) {
        int i3 = (this.b - i) - 1;
        if (i3 < 0) {
            i3 += this.c;
        }
        while (i2 != 0) {
            if (i3 >= this.c) {
                i3 = 0;
            }
            byte[] bArr = this.a;
            int i4 = this.b;
            this.b = i4 + 1;
            int i5 = i3 + 1;
            bArr[i4] = this.a[i3];
            if (this.b >= this.c) {
                b();
            }
            i2--;
            i3 = i5;
        }
    }

    public void a(OutputStream outputStream) {
        a();
        this.e = outputStream;
    }

    public void a(boolean z) {
        if (!z) {
            this.d = 0;
            this.b = 0;
        }
    }

    public byte b(int i) {
        int i2 = (this.b - i) - 1;
        if (i2 < 0) {
            i2 += this.c;
        }
        return this.a[i2];
    }

    public void b() {
        int i = this.b - this.d;
        if (i != 0) {
            this.e.write(this.a, this.d, i);
            if (this.b >= this.c) {
                this.b = 0;
            }
            this.d = this.b;
        }
    }
}
