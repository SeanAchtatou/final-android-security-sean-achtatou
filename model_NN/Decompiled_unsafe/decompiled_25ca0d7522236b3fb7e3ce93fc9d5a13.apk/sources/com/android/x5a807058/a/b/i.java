package com.android.x5a807058.a.b;

class i {
    j[] a;
    int b;
    int c;
    int d;
    final /* synthetic */ f e;

    i(f fVar) {
        this.e = fVar;
    }

    public j a(int i, byte b2) {
        return this.a[((this.d & i) << this.b) + ((b2 & 255) >>> (8 - this.b))];
    }

    public void a() {
        int i = 1 << (this.b + this.c);
        for (int i2 = 0; i2 < i; i2++) {
            this.a[i2].a();
        }
    }

    public void a(int i, int i2) {
        if (this.a == null || this.b != i2 || this.c != i) {
            this.c = i;
            this.d = (1 << i) - 1;
            this.b = i2;
            int i3 = 1 << (this.b + this.c);
            this.a = new j[i3];
            for (int i4 = 0; i4 < i3; i4++) {
                this.a[i4] = new j(this);
            }
        }
    }
}
