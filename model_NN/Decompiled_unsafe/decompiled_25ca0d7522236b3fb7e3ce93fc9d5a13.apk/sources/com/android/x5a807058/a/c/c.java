package com.android.x5a807058.a.c;

import java.io.InputStream;

public class c {
    int a;
    int b;
    InputStream c;

    public static void a(short[] sArr) {
        for (int i = 0; i < sArr.length; i++) {
            sArr[i] = 1024;
        }
    }

    public final int a(int i) {
        int i2 = 0;
        while (i != 0) {
            this.a >>>= 1;
            int i3 = (this.b - this.a) >>> 31;
            this.b -= this.a & (i3 - 1);
            i2 = (i2 << 1) | (1 - i3);
            if ((this.a & -16777216) == 0) {
                this.b = (this.b << 8) | this.c.read();
                this.a <<= 8;
            }
            i--;
        }
        return i2;
    }

    public int a(short[] sArr, int i) {
        short s = sArr[i];
        int i2 = (this.a >>> 11) * s;
        if ((this.b ^ Integer.MIN_VALUE) < (Integer.MIN_VALUE ^ i2)) {
            this.a = i2;
            sArr[i] = (short) (s + ((2048 - s) >>> 5));
            if ((this.a & -16777216) == 0) {
                this.b = (this.b << 8) | this.c.read();
                this.a <<= 8;
            }
            return 0;
        }
        this.a -= i2;
        this.b -= i2;
        sArr[i] = (short) (s - (s >>> 5));
        if ((this.a & -16777216) == 0) {
            this.b = (this.b << 8) | this.c.read();
            this.a <<= 8;
        }
        return 1;
    }

    public final void a() {
        this.c = null;
    }

    public final void a(InputStream inputStream) {
        this.c = inputStream;
    }

    public final void b() {
        this.b = 0;
        this.a = -1;
        for (int i = 0; i < 5; i++) {
            this.b = (this.b << 8) | this.c.read();
        }
    }
}
