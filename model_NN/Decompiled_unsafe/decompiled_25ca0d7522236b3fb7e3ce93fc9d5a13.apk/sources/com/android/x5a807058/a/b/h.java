package com.android.x5a807058.a.b;

import com.android.x5a807058.a.c.d;

class h extends g {
    int[] f = new int[4352];
    int g;
    int[] h = new int[16];
    final /* synthetic */ f i;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    h(f fVar) {
        super(fVar);
        this.i = fVar;
    }

    public int a(int i2, int i3) {
        return this.f[(i3 * 272) + i2];
    }

    public void a(d dVar, int i2, int i3) {
        super.a(dVar, i2, i3);
        int[] iArr = this.h;
        int i4 = iArr[i3] - 1;
        iArr[i3] = i4;
        if (i4 == 0) {
            c(i3);
        }
    }

    public void b(int i2) {
        this.g = i2;
    }

    /* access modifiers changed from: package-private */
    public void c(int i2) {
        a(i2, this.g, this.f, i2 * 272);
        this.h[i2] = this.g;
    }

    public void d(int i2) {
        for (int i3 = 0; i3 < i2; i3++) {
            c(i3);
        }
    }
}
