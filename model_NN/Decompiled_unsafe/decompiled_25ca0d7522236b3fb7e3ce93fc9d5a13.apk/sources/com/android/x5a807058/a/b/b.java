package com.android.x5a807058.a.b;

import com.android.x5a807058.a.a.c;
import com.android.x5a807058.a.c.a;

public class b {
    c a = new c();
    com.android.x5a807058.a.c.c b = new com.android.x5a807058.a.c.c();
    short[] c = new short[192];
    short[] d = new short[12];
    short[] e = new short[12];
    short[] f = new short[12];
    short[] g = new short[12];
    short[] h = new short[192];
    a[] i = new a[4];
    short[] j = new short[114];
    a k = new a(4);
    c l = new c(this);
    c m = new c(this);
    d n = new d(this);
    int o = -1;
    int p = -1;
    int q;

    public b() {
        for (int i2 = 0; i2 < 4; i2++) {
            this.i[i2] = new a(6);
        }
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.a.a(false);
        com.android.x5a807058.a.c.c.a(this.c);
        com.android.x5a807058.a.c.c.a(this.h);
        com.android.x5a807058.a.c.c.a(this.d);
        com.android.x5a807058.a.c.c.a(this.e);
        com.android.x5a807058.a.c.c.a(this.f);
        com.android.x5a807058.a.c.c.a(this.g);
        com.android.x5a807058.a.c.c.a(this.j);
        this.n.a();
        for (int i2 = 0; i2 < 4; i2++) {
            this.i[i2].a();
        }
        this.l.a();
        this.m.a();
        this.k.a();
        this.b.b();
    }

    /* access modifiers changed from: package-private */
    public boolean a(int i2) {
        if (i2 < 0) {
            return false;
        }
        if (this.o == i2) {
            return true;
        }
        this.o = i2;
        this.p = Math.max(this.o, 1);
        this.a.a(Math.max(this.p, 4096));
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean a(int i2, int i3, int i4) {
        if (i2 > 8 || i3 > 4 || i4 > 4) {
            return false;
        }
        this.n.a(i3, i2);
        int i5 = 1 << i4;
        this.l.a(i5);
        this.m.a(i5);
        this.q = i5 - 1;
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0135, code lost:
        r15.a.b();
        r15.a.a();
        r15.b.a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:?, code lost:
        return true;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(java.io.InputStream r16, java.io.OutputStream r17, long r18) {
        /*
            r15 = this;
            com.android.x5a807058.a.c.c r1 = r15.b
            r0 = r16
            r1.a(r0)
            com.android.x5a807058.a.a.c r1 = r15.a
            r0 = r17
            r1.a(r0)
            r15.a()
            int r8 = com.android.x5a807058.a.b.a.a()
            r7 = 0
            r6 = 0
            r5 = 0
            r4 = 0
            r2 = 0
            r1 = 0
            r9 = r2
            r3 = r4
            r2 = r5
            r4 = r6
        L_0x0020:
            r5 = 0
            int r5 = (r18 > r5 ? 1 : (r18 == r5 ? 0 : -1))
            if (r5 < 0) goto L_0x002a
            int r5 = (r9 > r18 ? 1 : (r9 == r18 ? 0 : -1))
            if (r5 >= 0) goto L_0x0135
        L_0x002a:
            int r5 = (int) r9
            int r6 = r15.q
            r6 = r6 & r5
            com.android.x5a807058.a.c.c r5 = r15.b
            short[] r11 = r15.c
            int r12 = r8 << 4
            int r12 = r12 + r6
            int r5 = r5.a(r11, r12)
            if (r5 != 0) goto L_0x0071
            com.android.x5a807058.a.b.d r5 = r15.n
            int r6 = (int) r9
            com.android.x5a807058.a.b.e r1 = r5.a(r6, r1)
            boolean r5 = com.android.x5a807058.a.b.a.e(r8)
            if (r5 != 0) goto L_0x006a
            com.android.x5a807058.a.c.c r5 = r15.b
            com.android.x5a807058.a.a.c r6 = r15.a
            byte r6 = r6.b(r7)
            byte r1 = r1.a(r5, r6)
        L_0x0054:
            com.android.x5a807058.a.a.c r5 = r15.a
            r5.a(r1)
            int r8 = com.android.x5a807058.a.b.a.a(r8)
            r5 = 1
            long r5 = r5 + r9
            r13 = r5
            r5 = r2
            r6 = r4
            r4 = r3
            r2 = r13
        L_0x0065:
            r9 = r2
            r3 = r4
            r2 = r5
            r4 = r6
            goto L_0x0020
        L_0x006a:
            com.android.x5a807058.a.c.c r5 = r15.b
            byte r1 = r1.a(r5)
            goto L_0x0054
        L_0x0071:
            com.android.x5a807058.a.c.c r1 = r15.b
            short[] r5 = r15.d
            int r1 = r1.a(r5, r8)
            r5 = 1
            if (r1 != r5) goto L_0x00dd
            r5 = 0
            com.android.x5a807058.a.c.c r1 = r15.b
            short[] r11 = r15.e
            int r1 = r1.a(r11, r8)
            if (r1 != 0) goto L_0x00bd
            com.android.x5a807058.a.c.c r1 = r15.b
            short[] r11 = r15.h
            int r12 = r8 << 4
            int r12 = r12 + r6
            int r1 = r1.a(r11, r12)
            if (r1 != 0) goto L_0x0168
            int r8 = com.android.x5a807058.a.b.a.d(r8)
            r1 = 1
            r13 = r3
            r3 = r7
            r7 = r4
            r4 = r2
            r2 = r13
        L_0x009e:
            if (r1 != 0) goto L_0x0162
            com.android.x5a807058.a.b.c r1 = r15.m
            com.android.x5a807058.a.c.c r5 = r15.b
            int r1 = r1.a(r5, r6)
            int r1 = r1 + 2
            int r8 = com.android.x5a807058.a.b.a.c(r8)
            r5 = r4
            r6 = r7
            r4 = r2
            r7 = r3
        L_0x00b2:
            long r2 = (long) r7
            int r2 = (r2 > r9 ? 1 : (r2 == r9 ? 0 : -1))
            if (r2 >= 0) goto L_0x00bb
            int r2 = r15.p
            if (r7 < r2) goto L_0x0151
        L_0x00bb:
            r1 = 0
        L_0x00bc:
            return r1
        L_0x00bd:
            com.android.x5a807058.a.c.c r1 = r15.b
            short[] r11 = r15.f
            int r1 = r1.a(r11, r8)
            if (r1 != 0) goto L_0x00cd
            r1 = r4
        L_0x00c8:
            r4 = r2
            r2 = r3
            r3 = r1
            r1 = r5
            goto L_0x009e
        L_0x00cd:
            com.android.x5a807058.a.c.c r1 = r15.b
            short[] r11 = r15.g
            int r1 = r1.a(r11, r8)
            if (r1 != 0) goto L_0x00da
            r1 = r2
        L_0x00d8:
            r2 = r4
            goto L_0x00c8
        L_0x00da:
            r1 = r3
            r3 = r2
            goto L_0x00d8
        L_0x00dd:
            com.android.x5a807058.a.b.c r1 = r15.l
            com.android.x5a807058.a.c.c r3 = r15.b
            int r1 = r1.a(r3, r6)
            int r3 = r1 + 2
            int r8 = com.android.x5a807058.a.b.a.b(r8)
            com.android.x5a807058.a.c.a[] r1 = r15.i
            int r5 = com.android.x5a807058.a.b.a.f(r3)
            r1 = r1[r5]
            com.android.x5a807058.a.c.c r5 = r15.b
            int r1 = r1.a(r5)
            r5 = 4
            if (r1 < r5) goto L_0x014a
            int r5 = r1 >> 1
            int r5 = r5 + -1
            r6 = r1 & 1
            r6 = r6 | 2
            int r6 = r6 << r5
            r11 = 14
            if (r1 >= r11) goto L_0x011c
            short[] r11 = r15.j
            int r1 = r6 - r1
            int r1 = r1 + -1
            com.android.x5a807058.a.c.c r12 = r15.b
            int r1 = com.android.x5a807058.a.c.a.a(r11, r1, r12, r5)
            int r1 = r1 + r6
        L_0x0116:
            r5 = r4
            r6 = r7
            r4 = r2
            r7 = r1
            r1 = r3
            goto L_0x00b2
        L_0x011c:
            com.android.x5a807058.a.c.c r1 = r15.b
            int r5 = r5 + -4
            int r1 = r1.a(r5)
            int r1 = r1 << 4
            int r1 = r1 + r6
            com.android.x5a807058.a.c.a r5 = r15.k
            com.android.x5a807058.a.c.c r6 = r15.b
            int r5 = r5.b(r6)
            int r1 = r1 + r5
            if (r1 >= 0) goto L_0x0116
            r2 = -1
            if (r1 != r2) goto L_0x0147
        L_0x0135:
            com.android.x5a807058.a.a.c r1 = r15.a
            r1.b()
            com.android.x5a807058.a.a.c r1 = r15.a
            r1.a()
            com.android.x5a807058.a.c.c r1 = r15.b
            r1.a()
            r1 = 1
            goto L_0x00bc
        L_0x0147:
            r1 = 0
            goto L_0x00bc
        L_0x014a:
            r5 = r4
            r6 = r7
            r4 = r2
            r7 = r1
            r1 = r3
            goto L_0x00b2
        L_0x0151:
            com.android.x5a807058.a.a.c r2 = r15.a
            r2.a(r7, r1)
            long r1 = (long) r1
            long r2 = r9 + r1
            com.android.x5a807058.a.a.c r1 = r15.a
            r9 = 0
            byte r1 = r1.b(r9)
            goto L_0x0065
        L_0x0162:
            r5 = r4
            r6 = r7
            r4 = r2
            r7 = r3
            goto L_0x00b2
        L_0x0168:
            r1 = r5
            r13 = r3
            r3 = r7
            r7 = r4
            r4 = r2
            r2 = r13
            goto L_0x009e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.x5a807058.a.b.b.a(java.io.InputStream, java.io.OutputStream, long):boolean");
    }

    public boolean a(byte[] bArr) {
        if (bArr.length < 5) {
            return false;
        }
        byte b2 = bArr[0] & 255;
        int i2 = b2 % 9;
        int i3 = b2 / 9;
        int i4 = i3 % 5;
        int i5 = i3 / 5;
        int i6 = 0;
        for (int i7 = 0; i7 < 4; i7++) {
            i6 += (bArr[i7 + 1] & 255) << (i7 * 8);
        }
        if (a(i2, i4, i5)) {
            return a(i6);
        }
        return false;
    }
}
