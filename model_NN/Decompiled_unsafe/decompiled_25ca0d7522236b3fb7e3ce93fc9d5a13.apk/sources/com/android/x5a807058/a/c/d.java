package com.android.x5a807058.a.c;

import java.io.OutputStream;

public class d {
    private static int[] g = new int[512];
    OutputStream a;
    long b;
    int c;
    int d;
    int e;
    long f;

    static {
        for (int i = 8; i >= 0; i--) {
            int i2 = 1 << (9 - i);
            for (int i3 = 1 << ((9 - i) - 1); i3 < i2; i3++) {
                g[i3] = (i << 6) + (((i2 - i3) << 6) >>> ((9 - i) - 1));
            }
        }
    }

    public static int a(int i) {
        return g[i >>> 2];
    }

    public static void a(short[] sArr) {
        for (int i = 0; i < sArr.length; i++) {
            sArr[i] = 1024;
        }
    }

    public static int b(int i) {
        return g[(2048 - i) >>> 2];
    }

    public static int b(int i, int i2) {
        return g[(((i - i2) ^ (-i2)) & 2047) >>> 2];
    }

    public void a() {
        this.a = null;
    }

    public void a(int i, int i2) {
        for (int i3 = i2 - 1; i3 >= 0; i3--) {
            this.c >>>= 1;
            if (((i >>> i3) & 1) == 1) {
                this.b += (long) this.c;
            }
            if ((this.c & -16777216) == 0) {
                this.c <<= 8;
                e();
            }
        }
    }

    public void a(OutputStream outputStream) {
        this.a = outputStream;
    }

    public void a(short[] sArr, int i, int i2) {
        short s = sArr[i];
        int i3 = (this.c >>> 11) * s;
        if (i2 == 0) {
            this.c = i3;
            sArr[i] = (short) (s + ((2048 - s) >>> 5));
        } else {
            this.b += ((long) i3) & 4294967295L;
            this.c -= i3;
            sArr[i] = (short) (s - (s >>> 5));
        }
        if ((this.c & -16777216) == 0) {
            this.c <<= 8;
            e();
        }
    }

    public void b() {
        this.f = 0;
        this.b = 0;
        this.c = -1;
        this.d = 1;
        this.e = 0;
    }

    public void c() {
        for (int i = 0; i < 5; i++) {
            e();
        }
    }

    public void d() {
        this.a.flush();
    }

    public void e() {
        int i;
        int i2 = (int) (this.b >>> 32);
        if (i2 != 0 || this.b < 4278190080L) {
            this.f += (long) this.d;
            int i3 = this.e;
            do {
                this.a.write(i3 + i2);
                i3 = 255;
                i = this.d - 1;
                this.d = i;
            } while (i != 0);
            this.e = ((int) this.b) >>> 24;
        }
        this.d++;
        this.b = (this.b & 16777215) << 8;
    }

    public long f() {
        return ((long) this.d) + this.f + 4;
    }
}
