package com.android.x5a807058.a.a;

import java.io.InputStream;

public class b {
    public byte[] m;
    InputStream n;
    int o;
    boolean p;
    int q;
    public int r;
    public int s;
    public int t;
    int u;
    int v;
    public int w;

    public void a() {
        this.r = 0;
        this.t = 0;
        this.w = 0;
        this.p = false;
        e();
    }

    public void a(int i, int i2, int i3) {
        this.u = i;
        this.v = i2;
        int i4 = i + i2 + i3;
        if (this.m == null || this.s != i4) {
            f();
            this.s = i4;
            this.m = new byte[this.s];
        }
        this.q = this.s - i2;
    }

    public void a(InputStream inputStream) {
        this.n = inputStream;
    }

    public int b(int i, int i2, int i3) {
        if (this.p && this.t + i + i3 > this.w) {
            i3 = this.w - (this.t + i);
        }
        int i4 = i2 + 1;
        int i5 = this.r + this.t + i;
        int i6 = 0;
        while (i6 < i3 && this.m[i5 + i6] == this.m[(i5 + i6) - i4]) {
            i6++;
        }
        return i6;
    }

    public void b() {
        this.t++;
        if (this.t > this.o) {
            if (this.r + this.t > this.q) {
                d();
            }
            e();
        }
    }

    public byte c(int i) {
        return this.m[this.r + this.t + i];
    }

    public void d() {
        int i = (this.r + this.t) - this.u;
        if (i > 0) {
            i--;
        }
        int i2 = (this.r + this.w) - i;
        for (int i3 = 0; i3 < i2; i3++) {
            this.m[i3] = this.m[i + i3];
        }
        this.r -= i;
    }

    public void d(int i) {
        this.r += i;
        this.o -= i;
        this.t -= i;
        this.w -= i;
    }

    /* JADX WARNING: CFG modification limit reached, blocks count: 118 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void e() {
        /*
            r5 = this;
            boolean r0 = r5.p
            if (r0 == 0) goto L_0x001a
        L_0x0004:
            return
        L_0x0005:
            int r1 = r5.w
            int r0 = r0 + r1
            r5.w = r0
            int r0 = r5.w
            int r1 = r5.t
            int r2 = r5.v
            int r1 = r1 + r2
            if (r0 < r1) goto L_0x001a
            int r0 = r5.w
            int r1 = r5.v
            int r0 = r0 - r1
            r5.o = r0
        L_0x001a:
            int r0 = r5.r
            int r0 = 0 - r0
            int r1 = r5.s
            int r0 = r0 + r1
            int r1 = r5.w
            int r0 = r0 - r1
            if (r0 == 0) goto L_0x0004
            java.io.InputStream r1 = r5.n
            byte[] r2 = r5.m
            int r3 = r5.r
            int r4 = r5.w
            int r3 = r3 + r4
            int r0 = r1.read(r2, r3, r0)
            r1 = -1
            if (r0 != r1) goto L_0x0005
            int r0 = r5.w
            r5.o = r0
            int r0 = r5.r
            int r1 = r5.o
            int r0 = r0 + r1
            int r1 = r5.q
            if (r0 <= r1) goto L_0x004a
            int r0 = r5.q
            int r1 = r5.r
            int r0 = r0 - r1
            r5.o = r0
        L_0x004a:
            r0 = 1
            r5.p = r0
            goto L_0x0004
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.x5a807058.a.a.b.e():void");
    }

    /* access modifiers changed from: package-private */
    public void f() {
        this.m = null;
    }

    public void g() {
        this.n = null;
    }

    public int h() {
        return this.w - this.t;
    }
}
