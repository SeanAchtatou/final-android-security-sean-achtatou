package com.android.x5a807058.a.c;

public class b {
    short[] a;
    int b;

    public b(int i) {
        this.b = i;
        this.a = new short[(1 << i)];
    }

    public static int a(short[] sArr, int i, int i2, int i3) {
        int i4 = 0;
        int i5 = 1;
        while (i2 != 0) {
            int i6 = i3 & 1;
            i3 >>>= 1;
            i4 += d.b(sArr[i + i5], i6);
            i5 = (i5 << 1) | i6;
            i2--;
        }
        return i4;
    }

    public static void a(short[] sArr, int i, d dVar, int i2, int i3) {
        int i4 = 1;
        for (int i5 = 0; i5 < i2; i5++) {
            int i6 = i3 & 1;
            dVar.a(sArr, i + i4, i6);
            i4 = (i4 << 1) | i6;
            i3 >>= 1;
        }
    }

    public int a(int i) {
        int i2 = 0;
        int i3 = 1;
        int i4 = this.b;
        while (i4 != 0) {
            i4--;
            int i5 = (i >>> i4) & 1;
            i2 += d.b(this.a[i3], i5);
            i3 = (i3 << 1) + i5;
        }
        return i2;
    }

    public void a() {
        c.a(this.a);
    }

    public void a(d dVar, int i) {
        int i2 = 1;
        int i3 = this.b;
        while (i3 != 0) {
            i3--;
            int i4 = (i >>> i3) & 1;
            dVar.a(this.a, i2, i4);
            i2 = (i2 << 1) | i4;
        }
    }

    public int b(int i) {
        int i2 = 0;
        int i3 = 1;
        for (int i4 = this.b; i4 != 0; i4--) {
            int i5 = i & 1;
            i >>>= 1;
            i2 += d.b(this.a[i3], i5);
            i3 = (i3 << 1) | i5;
        }
        return i2;
    }

    public void b(d dVar, int i) {
        int i2 = 1;
        for (int i3 = 0; i3 < this.b; i3++) {
            int i4 = i & 1;
            dVar.a(this.a, i2, i4);
            i2 = (i2 << 1) | i4;
            i >>= 1;
        }
    }
}
