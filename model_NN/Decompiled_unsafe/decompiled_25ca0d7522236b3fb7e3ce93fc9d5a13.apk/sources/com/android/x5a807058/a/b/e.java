package com.android.x5a807058.a.b;

import com.android.x5a807058.a.c.c;

class e {
    short[] a = new short[768];
    final /* synthetic */ d b;

    e(d dVar) {
        this.b = dVar;
    }

    public byte a(c cVar) {
        int i = 1;
        do {
            i = cVar.a(this.a, i) | (i << 1);
        } while (i < 256);
        return (byte) i;
    }

    public byte a(c cVar, byte b2) {
        int i = 1;
        while (true) {
            int i2 = (b2 >> 7) & 1;
            b2 = (byte) (b2 << 1);
            int a2 = cVar.a(this.a, ((i2 + 1) << 8) + i);
            i = (i << 1) | a2;
            if (i2 == a2) {
                if (i >= 256) {
                    break;
                }
            } else {
                while (i < 256) {
                    i = cVar.a(this.a, i) | (i << 1);
                }
            }
        }
        return (byte) i;
    }

    public void a() {
        c.a(this.a);
    }
}
