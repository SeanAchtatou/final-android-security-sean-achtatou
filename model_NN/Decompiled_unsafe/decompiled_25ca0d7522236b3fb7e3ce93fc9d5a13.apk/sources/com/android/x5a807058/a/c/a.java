package com.android.x5a807058.a.c;

public class a {
    short[] a;
    int b;

    public a(int i) {
        this.b = i;
        this.a = new short[(1 << i)];
    }

    public static int a(short[] sArr, int i, c cVar, int i2) {
        int i3 = 1;
        int i4 = 0;
        for (int i5 = 0; i5 < i2; i5++) {
            int a2 = cVar.a(sArr, i + i3);
            i3 = (i3 << 1) + a2;
            i4 |= a2 << i5;
        }
        return i4;
    }

    public int a(c cVar) {
        int i = 1;
        for (int i2 = this.b; i2 != 0; i2--) {
            i = cVar.a(this.a, i) + (i << 1);
        }
        return i - (1 << this.b);
    }

    public void a() {
        c.a(this.a);
    }

    public int b(c cVar) {
        int i = 1;
        int i2 = 0;
        for (int i3 = 0; i3 < this.b; i3++) {
            int a2 = cVar.a(this.a, i);
            i = (i << 1) + a2;
            i2 |= a2 << i3;
        }
        return i2;
    }
}
