package com.android.x5a807058.a.b;

import com.android.x5a807058.a.c.b;
import com.android.x5a807058.a.c.d;

class g {
    short[] a = new short[2];
    b[] b = new b[16];
    b[] c = new b[16];
    b d = new b(8);
    final /* synthetic */ f e;

    public g(f fVar) {
        this.e = fVar;
        for (int i = 0; i < 16; i++) {
            this.b[i] = new b(3);
            this.c[i] = new b(3);
        }
    }

    public void a(int i) {
        d.a(this.a);
        for (int i2 = 0; i2 < i; i2++) {
            this.b[i2].a();
            this.c[i2].a();
        }
        this.d.a();
    }

    public void a(int i, int i2, int[] iArr, int i3) {
        int i4 = 0;
        int a2 = d.a(this.a[0]);
        int b2 = d.b(this.a[0]);
        int a3 = d.a(this.a[1]) + b2;
        int b3 = b2 + d.b(this.a[1]);
        while (i4 < 8) {
            if (i4 < i2) {
                iArr[i3 + i4] = this.b[i].a(i4) + a2;
                i4++;
            } else {
                return;
            }
        }
        while (i4 < 16) {
            if (i4 < i2) {
                iArr[i3 + i4] = this.c[i].a(i4 - 8) + a3;
                i4++;
            } else {
                return;
            }
        }
        while (i4 < i2) {
            iArr[i3 + i4] = this.d.a((i4 - 8) - 8) + b3;
            i4++;
        }
    }

    public void a(d dVar, int i, int i2) {
        if (i < 8) {
            dVar.a(this.a, 0, 0);
            this.b[i2].a(dVar, i);
            return;
        }
        int i3 = i - 8;
        dVar.a(this.a, 0, 1);
        if (i3 < 8) {
            dVar.a(this.a, 1, 0);
            this.c[i2].a(dVar, i3);
            return;
        }
        dVar.a(this.a, 1, 1);
        this.d.a(dVar, i3 - 8);
    }
}
