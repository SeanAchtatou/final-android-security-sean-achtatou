package com.android.x5a807058.a.a;

public class a extends b {
    private static final int[] x = new int[256];
    int a;
    int b = 0;
    int c;
    int[] d;
    int[] e;
    int f = 255;
    int g;
    int h = 0;
    boolean i = true;
    int j = 0;
    int k = 4;
    int l = 66560;

    static {
        for (int i2 = 0; i2 < 256; i2++) {
            int i3 = i2;
            for (int i4 = 0; i4 < 8; i4++) {
                i3 = (i3 & 1) != 0 ? (i3 >>> 1) ^ -306674912 : i3 >>> 1;
            }
            x[i2] = i3;
        }
    }

    public int a(int[] iArr) {
        int i2;
        byte b2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        int i9;
        int i10;
        if (this.t + this.c <= this.w) {
            i2 = this.c;
        } else {
            i2 = this.w - this.t;
            if (i2 < this.k) {
                b();
                return 0;
            }
        }
        int i11 = 0;
        int i12 = this.t > this.b ? this.t - this.b : 0;
        int i13 = this.r + this.t;
        int i14 = 1;
        byte b3 = 0;
        byte b4 = 0;
        if (this.i) {
            byte b5 = x[this.m[i13] & 255] ^ (this.m[i13 + 1] & 255);
            b3 = b5 & 1023;
            byte b6 = ((this.m[i13 + 2] & 255) << 8) ^ b5;
            b4 = 65535 & b6;
            b2 = (b6 ^ (x[this.m[i13 + 3] & 255] << 5)) & this.g;
        } else {
            b2 = (this.m[i13] & 255) ^ ((this.m[i13 + 1] & 255) << 8);
        }
        int i15 = this.e[this.l + b2];
        if (this.i) {
            int i16 = this.e[b3];
            int i17 = this.e[b4 + 1024];
            this.e[b3] = this.t;
            this.e[b4 + 1024] = this.t;
            if (i16 > i12 && this.m[this.r + i16] == this.m[i13]) {
                i14 = 2;
                iArr[0] = 2;
                i11 = 2;
                iArr[1] = (this.t - i16) - 1;
            }
            int i18 = i14;
            int i19 = i11;
            if (i17 <= i12 || this.m[this.r + i17] != this.m[i13]) {
                i10 = i16;
                int i20 = i18;
                i4 = i19;
                i3 = i20;
            } else {
                if (i17 == i16) {
                    i19 -= 2;
                }
                int i21 = i19 + 1;
                iArr[i19] = 3;
                int i22 = i21 + 1;
                iArr[i21] = (this.t - i17) - 1;
                i10 = i17;
                i4 = i22;
                i3 = 3;
            }
            if (i4 != 0 && i10 == i15) {
                i4 -= 2;
                i3 = 1;
            }
        } else {
            i3 = 1;
            i4 = 0;
        }
        this.e[b2 + this.l] = this.t;
        int i23 = (this.a << 1) + 1;
        int i24 = this.a << 1;
        int i25 = this.j;
        if (!(this.j == 0 || i15 <= i12 || this.m[this.r + i15 + this.j] == this.m[this.j + i13])) {
            int i26 = i4 + 1;
            i3 = this.j;
            iArr[i4] = i3;
            i4 = i26 + 1;
            iArr[i26] = (this.t - i15) - 1;
        }
        int i27 = i25;
        int i28 = i15;
        int i29 = i4;
        int i30 = i3;
        int i31 = this.f;
        while (true) {
            if (i28 <= i12) {
                break;
            }
            int i32 = i31 - 1;
            if (i31 == 0) {
                break;
            }
            int i33 = this.t - i28;
            int i34 = (i33 <= this.a ? this.a - i33 : (this.a - i33) + this.b) << 1;
            int i35 = this.r + i28;
            int min = Math.min(i27, i25);
            if (this.m[i35 + min] == this.m[i13 + min]) {
                do {
                    min++;
                    if (min == i2) {
                        break;
                    }
                } while (this.m[i35 + min] == this.m[i13 + min]);
                if (i30 < min) {
                    int i36 = i29 + 1;
                    iArr[i29] = min;
                    i5 = i36 + 1;
                    iArr[i36] = i33 - 1;
                    if (min == i2) {
                        this.d[i24] = this.d[i34];
                        this.d[i23] = this.d[i34 + 1];
                        break;
                    }
                    i29 = i5;
                    i30 = min;
                }
            }
            if ((this.m[i35 + min] & 255) < (this.m[i13 + min] & 255)) {
                this.d[i24] = i28;
                int i37 = i34 + 1;
                int i38 = this.d[i37];
                int i39 = i27;
                i7 = i37;
                i9 = i39;
                int i40 = i23;
                i6 = i38;
                i8 = i40;
            } else {
                this.d[i23] = i28;
                i6 = this.d[i34];
                i7 = i24;
                i8 = i34;
                int i41 = min;
                min = i25;
                i9 = i41;
            }
            i28 = i6;
            i23 = i8;
            i24 = i7;
            i27 = i9;
            i25 = min;
            i31 = i32;
        }
        int[] iArr2 = this.d;
        this.d[i24] = 0;
        iArr2[i23] = 0;
        i5 = i29;
        b();
        return i5;
    }

    public void a() {
        super.a();
        for (int i2 = 0; i2 < this.h; i2++) {
            this.e[i2] = 0;
        }
        this.a = 0;
        d(-1);
    }

    public void a(int i2) {
        this.i = i2 > 2;
        if (this.i) {
            this.j = 0;
            this.k = 4;
            this.l = 66560;
            return;
        }
        this.j = 2;
        this.k = 3;
        this.l = 0;
    }

    /* access modifiers changed from: package-private */
    public void a(int[] iArr, int i2, int i3) {
        for (int i4 = 0; i4 < i2; i4++) {
            int i5 = iArr[i4];
            iArr[i4] = i5 <= i3 ? 0 : i5 - i3;
        }
    }

    public boolean a(int i2, int i3, int i4, int i5) {
        if (i2 > 1073741567) {
            return false;
        }
        this.f = (i4 >> 1) + 16;
        super.a(i2 + i3, i4 + i5, ((((i2 + i3) + i4) + i5) / 2) + 256);
        this.c = i4;
        int i6 = i2 + 1;
        if (this.b != i6) {
            this.b = i6;
            this.d = new int[(i6 * 2)];
        }
        int i7 = 65536;
        if (this.i) {
            int i8 = i2 - 1;
            int i9 = i8 | (i8 >> 1);
            int i10 = i9 | (i9 >> 2);
            int i11 = i10 | (i10 >> 4);
            int i12 = ((i11 | (i11 >> 8)) >> 1) | 65535;
            if (i12 > 16777216) {
                i12 >>= 1;
            }
            this.g = i12;
            i7 = i12 + 1 + this.l;
        }
        if (i7 != this.h) {
            this.h = i7;
            this.e = new int[i7];
        }
        return true;
    }

    public void b() {
        int i2 = this.a + 1;
        this.a = i2;
        if (i2 >= this.b) {
            this.a = 0;
        }
        super.b();
        if (this.t == 1073741823) {
            c();
        }
    }

    public void b(int i2) {
        int i3;
        byte b2;
        int i4;
        int i5;
        int i6;
        int i7;
        do {
            if (this.t + this.c <= this.w) {
                i3 = this.c;
            } else {
                i3 = this.w - this.t;
                if (i3 < this.k) {
                    b();
                    i2--;
                }
            }
            int i8 = this.t > this.b ? this.t - this.b : 0;
            int i9 = this.r + this.t;
            if (this.i) {
                byte b3 = x[this.m[i9] & 255] ^ (this.m[i9 + 1] & 255);
                this.e[b3 & 1023] = this.t;
                byte b4 = b3 ^ ((this.m[i9 + 2] & 255) << 8);
                this.e[(65535 & b4) + 1024] = this.t;
                b2 = (b4 ^ (x[this.m[i9 + 3] & 255] << 5)) & this.g;
            } else {
                b2 = (this.m[i9] & 255) ^ ((this.m[i9 + 1] & 255) << 8);
            }
            int i10 = this.e[this.l + b2];
            this.e[b2 + this.l] = this.t;
            int i11 = (this.a << 1) + 1;
            int i12 = this.a << 1;
            int i13 = this.j;
            int i14 = this.f;
            int i15 = i10;
            int i16 = i13;
            while (true) {
                if (i15 <= i8) {
                    break;
                }
                int i17 = i14 - 1;
                if (i14 == 0) {
                    break;
                }
                int i18 = this.t - i15;
                int i19 = (i18 <= this.a ? this.a - i18 : (this.a - i18) + this.b) << 1;
                int i20 = this.r + i15;
                int min = Math.min(i16, i13);
                if (this.m[i20 + min] == this.m[i9 + min]) {
                    do {
                        min++;
                        if (min == i3) {
                            break;
                        }
                    } while (this.m[i20 + min] == this.m[i9 + min]);
                    if (min == i3) {
                        this.d[i12] = this.d[i19];
                        this.d[i11] = this.d[i19 + 1];
                        break;
                    }
                }
                if ((this.m[i20 + min] & 255) < (this.m[i9 + min] & 255)) {
                    this.d[i12] = i15;
                    int i21 = i19 + 1;
                    int i22 = this.d[i21];
                    int i23 = i16;
                    i5 = i21;
                    i7 = i23;
                    int i24 = i11;
                    i4 = i22;
                    i6 = i24;
                } else {
                    this.d[i11] = i15;
                    i4 = this.d[i19];
                    i5 = i12;
                    i6 = i19;
                    int i25 = min;
                    min = i13;
                    i7 = i25;
                }
                i15 = i4;
                i11 = i6;
                i12 = i5;
                i16 = i7;
                i13 = min;
                i14 = i17;
            }
            int[] iArr = this.d;
            this.d[i12] = 0;
            iArr[i11] = 0;
            b();
            i2--;
        } while (i2 != 0);
    }

    /* access modifiers changed from: package-private */
    public void c() {
        int i2 = this.t - this.b;
        a(this.d, this.b * 2, i2);
        a(this.e, this.h, i2);
        d(i2);
    }
}
