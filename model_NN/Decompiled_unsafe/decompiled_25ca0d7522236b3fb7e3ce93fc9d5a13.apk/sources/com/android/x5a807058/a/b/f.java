package com.android.x5a807058.a.b;

import com.android.x5a807058.a.a.a;
import com.android.x5a807058.a.c.b;
import com.android.x5a807058.a.c.d;
import java.io.InputStream;
import java.io.OutputStream;

public class f {
    static byte[] a = new byte[2048];
    boolean A;
    int[] B = new int[256];
    int[] C = new int[512];
    int[] D = new int[16];
    int E;
    int F = 44;
    int G = 2;
    int H = 3;
    int I = 0;
    int J = 3;
    int K = 4194304;
    int L = -1;
    int M = -1;
    long N;
    boolean O;
    InputStream P;
    int Q = 1;
    boolean R = false;
    int[] S = new int[4];
    int[] T = new int[4];
    int U;
    long[] V = new long[1];
    long[] W = new long[1];
    boolean[] X = new boolean[1];
    byte[] Y = new byte[5];
    int[] Z = new int[128];
    int aa;
    int b = a.a();
    byte c;
    int[] d = new int[4];
    k[] e = new k[4096];
    a f = null;
    d g = new d();
    short[] h = new short[192];
    short[] i = new short[12];
    short[] j = new short[12];
    short[] k = new short[12];
    short[] l = new short[12];
    short[] m = new short[192];
    b[] n = new b[4];
    short[] o = new short[114];
    b p = new b(4);
    h q = new h(this);
    h r = new h(this);
    i s = new i(this);
    int[] t = new int[548];
    int u = 32;
    int v;
    int w;
    int x;
    int y;
    int z;

    static {
        int i2 = 2;
        a[0] = 0;
        a[1] = 1;
        int i3 = 2;
        while (i3 < 22) {
            int i4 = 1 << ((i3 >> 1) - 1);
            int i5 = i2;
            int i6 = 0;
            while (i6 < i4) {
                a[i5] = (byte) i3;
                i6++;
                i5++;
            }
            i3++;
            i2 = i5;
        }
    }

    public f() {
        for (int i2 = 0; i2 < 4096; i2++) {
            this.e[i2] = new k(this);
        }
        for (int i3 = 0; i3 < 4; i3++) {
            this.n[i3] = new b(6);
        }
    }

    static int a(int i2) {
        return i2 < 2048 ? a[i2] : i2 < 2097152 ? a[i2 >> 10] + 20 : a[i2 >> 20] + 40;
    }

    static int b(int i2) {
        return i2 < 131072 ? a[i2 >> 6] + 12 : i2 < 134217728 ? a[i2 >> 16] + 32 : a[i2 >> 26] + 52;
    }

    /* access modifiers changed from: package-private */
    public int a(int i2, int i3) {
        return d.a(this.j[i2]) + d.a(this.m[(i2 << 4) + i3]);
    }

    /* access modifiers changed from: package-private */
    public int a(int i2, int i3, int i4) {
        if (i2 == 0) {
            return d.a(this.j[i3]) + d.b(this.m[(i3 << 4) + i4]);
        }
        int b2 = d.b(this.j[i3]);
        return i2 == 1 ? b2 + d.a(this.k[i3]) : b2 + d.b(this.k[i3]) + d.b(this.l[i3], i2 - 2);
    }

    /* access modifiers changed from: package-private */
    public int a(int i2, int i3, int i4, int i5) {
        return this.r.a(i3 - 2, i5) + a(i2, i4, i5);
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.b = a.a();
        this.c = 0;
        for (int i2 = 0; i2 < 4; i2++) {
            this.d[i2] = 0;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(InputStream inputStream, OutputStream outputStream, long j2, long j3) {
        this.P = inputStream;
        this.O = false;
        b();
        a(outputStream);
        c();
        h();
        i();
        this.q.b((this.u + 1) - 2);
        this.q.d(1 << this.G);
        this.r.b((this.u + 1) - 2);
        this.r.d(1 << this.G);
        this.N = 0;
    }

    /* access modifiers changed from: package-private */
    public void a(OutputStream outputStream) {
        this.g.a(outputStream);
    }

    public void a(long[] jArr, long[] jArr2, boolean[] zArr) {
        jArr[0] = 0;
        jArr2[0] = 0;
        zArr[0] = true;
        if (this.P != null) {
            this.f.a(this.P);
            this.f.a();
            this.R = true;
            this.P = null;
        }
        if (!this.O) {
            this.O = true;
            long j2 = this.N;
            if (this.N == 0) {
                if (this.f.h() == 0) {
                    g((int) this.N);
                    return;
                }
                d();
                this.g.a(this.h, (((int) this.N) & this.H) + (this.b << 4), 0);
                this.b = a.a(this.b);
                byte c2 = this.f.c(0 - this.x);
                this.s.a((int) this.N, this.c).a(this.g, c2);
                this.c = c2;
                this.x--;
                this.N++;
            }
            if (this.f.h() == 0) {
                g((int) this.N);
                return;
            }
            while (true) {
                int e2 = e((int) this.N);
                int i2 = this.U;
                int i3 = ((int) this.N) & this.H;
                int i4 = (this.b << 4) + i3;
                if (e2 == 1 && i2 == -1) {
                    this.g.a(this.h, i4, 0);
                    byte c3 = this.f.c(0 - this.x);
                    j a2 = this.s.a((int) this.N, this.c);
                    if (!a.e(this.b)) {
                        a2.a(this.g, this.f.c(((0 - this.d[0]) - 1) - this.x), c3);
                    } else {
                        a2.a(this.g, c3);
                    }
                    this.c = c3;
                    this.b = a.a(this.b);
                } else {
                    this.g.a(this.h, i4, 1);
                    if (i2 < 4) {
                        this.g.a(this.i, this.b, 1);
                        if (i2 == 0) {
                            this.g.a(this.j, this.b, 0);
                            if (e2 == 1) {
                                this.g.a(this.m, i4, 0);
                            } else {
                                this.g.a(this.m, i4, 1);
                            }
                        } else {
                            this.g.a(this.j, this.b, 1);
                            if (i2 == 1) {
                                this.g.a(this.k, this.b, 0);
                            } else {
                                this.g.a(this.k, this.b, 1);
                                this.g.a(this.l, this.b, i2 - 2);
                            }
                        }
                        if (e2 == 1) {
                            this.b = a.d(this.b);
                        } else {
                            this.r.a(this.g, e2 - 2, i3);
                            this.b = a.c(this.b);
                        }
                        int i5 = this.d[i2];
                        if (i2 != 0) {
                            while (i2 >= 1) {
                                this.d[i2] = this.d[i2 - 1];
                                i2--;
                            }
                            this.d[0] = i5;
                        }
                    } else {
                        this.g.a(this.i, this.b, 0);
                        this.b = a.b(this.b);
                        this.q.a(this.g, e2 - 2, i3);
                        int i6 = i2 - 4;
                        int a3 = a(i6);
                        this.n[a.f(e2)].a(this.g, a3);
                        if (a3 >= 4) {
                            int i7 = (a3 >> 1) - 1;
                            int i8 = ((a3 & 1) | 2) << i7;
                            int i9 = i6 - i8;
                            if (a3 < 14) {
                                b.a(this.o, (i8 - a3) - 1, this.g, i7, i9);
                            } else {
                                this.g.a(i9 >> 4, i7 - 4);
                                this.p.b(this.g, i9 & 15);
                                this.E++;
                            }
                        }
                        for (int i10 = 3; i10 >= 1; i10--) {
                            this.d[i10] = this.d[i10 - 1];
                        }
                        this.d[0] = i6;
                        this.aa++;
                    }
                    this.c = this.f.c((e2 - 1) - this.x);
                }
                this.x -= e2;
                this.N += (long) e2;
                if (this.x == 0) {
                    if (this.aa >= 128) {
                        h();
                    }
                    if (this.E >= 16) {
                        i();
                    }
                    jArr[0] = this.N;
                    jArr2[0] = this.g.f();
                    if (this.f.h() == 0) {
                        g((int) this.N);
                        return;
                    } else if (this.N - j2 >= 4096) {
                        this.O = false;
                        zArr[0] = false;
                        return;
                    }
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public int b(int i2, int i3, int i4) {
        int f2 = a.f(i3);
        return (i2 < 128 ? this.C[(f2 * 128) + i2] : this.B[(f2 << 6) + b(i2)] + this.D[i2 & 15]) + this.q.a(i3 - 2, i4);
    }

    /* access modifiers changed from: package-private */
    public void b() {
        if (this.f == null) {
            a aVar = new a();
            int i2 = 4;
            if (this.Q == 0) {
                i2 = 2;
            }
            aVar.a(i2);
            this.f = aVar;
        }
        this.s.a(this.I, this.J);
        if (this.K != this.L || this.M != this.u) {
            this.f.a(this.K, 4096, this.u, 274);
            this.L = this.K;
            this.M = this.u;
        }
    }

    public void b(InputStream inputStream, OutputStream outputStream, long j2, long j3) {
        this.R = false;
        try {
            a(inputStream, outputStream, j2, j3);
            do {
                a(this.V, this.W, this.X);
            } while (!this.X[0]);
        } finally {
            g();
        }
    }

    public void b(OutputStream outputStream) {
        this.Y[0] = (byte) ((((this.G * 5) + this.I) * 9) + this.J);
        for (int i2 = 0; i2 < 4; i2++) {
            this.Y[i2 + 1] = (byte) (this.K >> (i2 * 8));
        }
        outputStream.write(this.Y, 0, 5);
    }

    /* access modifiers changed from: package-private */
    public void c() {
        a();
        this.g.b();
        d.a(this.h);
        d.a(this.m);
        d.a(this.i);
        d.a(this.j);
        d.a(this.k);
        d.a(this.l);
        d.a(this.o);
        this.s.a();
        for (int i2 = 0; i2 < 4; i2++) {
            this.n[i2].a();
        }
        this.q.a(1 << this.G);
        this.r.a(1 << this.G);
        this.p.a();
        this.A = false;
        this.y = 0;
        this.z = 0;
        this.x = 0;
    }

    /* access modifiers changed from: package-private */
    public void c(int i2) {
        if (i2 > 0) {
            this.f.b(i2);
            this.x += i2;
        }
    }

    /* access modifiers changed from: package-private */
    public int d() {
        int i2 = 0;
        this.w = this.f.a(this.t);
        if (this.w > 0 && (i2 = this.t[this.w - 2]) == this.u) {
            i2 += this.f.b(i2 - 1, this.t[this.w - 1], 273 - i2);
        }
        this.x++;
        return i2;
    }

    /* access modifiers changed from: package-private */
    public int d(int i2) {
        this.y = i2;
        int i3 = this.e[i2].g;
        int i4 = this.e[i2].h;
        int i5 = i3;
        while (true) {
            if (this.e[i2].b) {
                this.e[i5].a();
                this.e[i5].g = i5 - 1;
                if (this.e[i2].c) {
                    this.e[i5 - 1].b = false;
                    this.e[i5 - 1].g = this.e[i2].d;
                    this.e[i5 - 1].h = this.e[i2].e;
                }
            }
            int i6 = this.e[i5].h;
            int i7 = this.e[i5].g;
            this.e[i5].h = i4;
            this.e[i5].g = i2;
            if (i5 <= 0) {
                this.U = this.e[0].h;
                this.z = this.e[0].g;
                return this.z;
            }
            i4 = i6;
            i2 = i5;
            i5 = i7;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.android.x5a807058.a.b.j.a(boolean, byte, byte):int
     arg types: [int, byte, byte]
     candidates:
      com.android.x5a807058.a.b.j.a(com.android.x5a807058.a.c.d, byte, byte):void
      com.android.x5a807058.a.b.j.a(boolean, byte, byte):int */
    /* access modifiers changed from: package-private */
    public int e(int i2) {
        int i3;
        int i4;
        int i5;
        int c2;
        int i6;
        int i7;
        int b2;
        int b3;
        int b4;
        int a2;
        int i8;
        int a3;
        if (this.y != this.z) {
            int i9 = this.e[this.z].g - this.z;
            this.U = this.e[this.z].h;
            this.z = this.e[this.z].g;
            return i9;
        }
        this.y = 0;
        this.z = 0;
        if (!this.A) {
            i3 = d();
        } else {
            i3 = this.v;
            this.A = false;
        }
        int i10 = this.w;
        int h2 = this.f.h() + 1;
        if (h2 < 2) {
            this.U = -1;
            return 1;
        }
        if (h2 > 273) {
        }
        int i11 = 0;
        for (int i12 = 0; i12 < 4; i12++) {
            this.S[i12] = this.d[i12];
            this.T[i12] = this.f.b(-1, this.S[i12], 273);
            if (this.T[i12] > this.T[i11]) {
                i11 = i12;
            }
        }
        if (this.T[i11] >= this.u) {
            this.U = i11;
            int i13 = this.T[i11];
            c(i13 - 1);
            return i13;
        } else if (i3 >= this.u) {
            this.U = this.t[i10 - 1] + 4;
            c(i3 - 1);
            return i3;
        } else {
            byte c3 = this.f.c(-1);
            byte c4 = this.f.c(((0 - this.d[0]) - 1) - 1);
            if (i3 >= 2 || c3 == c4 || this.T[i11] >= 2) {
                this.e[0].a = this.b;
                int i14 = i2 & this.H;
                this.e[1].f = this.s.a(i2, this.c).a(!a.e(this.b), c4, c3) + d.a(this.h[(this.b << 4) + i14]);
                this.e[1].a();
                int b5 = d.b(this.h[(this.b << 4) + i14]);
                int b6 = b5 + d.b(this.i[this.b]);
                if (c4 == c3 && (a3 = a(this.b, i14) + b6) < this.e[1].f) {
                    this.e[1].f = a3;
                    this.e[1].b();
                }
                int i15 = i3 >= this.T[i11] ? i3 : this.T[i11];
                if (i15 < 2) {
                    this.U = this.e[1].h;
                    return 1;
                }
                this.e[1].g = 0;
                this.e[0].i = this.S[0];
                this.e[0].j = this.S[1];
                this.e[0].k = this.S[2];
                this.e[0].l = this.S[3];
                int i16 = i15;
                while (true) {
                    int i17 = i16 - 1;
                    this.e[i16].f = 268435455;
                    if (i17 < 2) {
                        break;
                    }
                    i16 = i17;
                }
                int i18 = 0;
                while (true) {
                    int i19 = i18;
                    if (i19 >= 4) {
                        break;
                    }
                    int i20 = this.T[i19];
                    if (i20 >= 2) {
                        int a4 = a(i19, this.b, i14) + b6;
                        do {
                            int a5 = this.r.a(i20 - 2, i14) + a4;
                            k kVar = this.e[i20];
                            if (a5 < kVar.f) {
                                kVar.f = a5;
                                kVar.g = 0;
                                kVar.h = i19;
                                kVar.b = false;
                            }
                            i20--;
                        } while (i20 >= 2);
                    }
                    i18 = i19 + 1;
                }
                int a6 = b5 + d.a(this.i[this.b]);
                int i21 = this.T[0] >= 2 ? this.T[0] + 1 : 2;
                if (i21 <= i3) {
                    int i22 = 0;
                    while (i21 > this.t[i22]) {
                        i22 += 2;
                    }
                    while (true) {
                        int i23 = this.t[i22 + 1];
                        int b7 = b(i23, i21, i14) + a6;
                        k kVar2 = this.e[i21];
                        if (b7 < kVar2.f) {
                            kVar2.f = b7;
                            kVar2.g = 0;
                            kVar2.h = i23 + 4;
                            kVar2.b = false;
                        }
                        if (i21 == this.t[i22] && (i22 = i22 + 2) == i10) {
                            break;
                        }
                        i21++;
                    }
                }
                int i24 = 0;
                int i25 = i15;
                while (true) {
                    int i26 = i24 + 1;
                    if (i26 == i25) {
                        return d(i26);
                    }
                    int d2 = d();
                    int i27 = this.w;
                    if (d2 >= this.u) {
                        this.v = d2;
                        this.A = true;
                        return d(i26);
                    }
                    i2++;
                    int i28 = this.e[i26].g;
                    if (this.e[i26].b) {
                        i28--;
                        if (this.e[i26].c) {
                            int i29 = this.e[this.e[i26].d].a;
                            i8 = this.e[i26].e < 4 ? a.c(i29) : a.b(i29);
                        } else {
                            i8 = this.e[i28].a;
                        }
                        i4 = a.a(i8);
                    } else {
                        i4 = this.e[i28].a;
                    }
                    if (i28 == i26 - 1) {
                        c2 = this.e[i26].c() ? a.d(i4) : a.a(i4);
                    } else {
                        if (!this.e[i26].b || !this.e[i26].c) {
                            i5 = this.e[i26].h;
                            c2 = i5 < 4 ? a.c(i4) : a.b(i4);
                        } else {
                            i28 = this.e[i26].d;
                            i5 = this.e[i26].e;
                            c2 = a.c(i4);
                        }
                        k kVar3 = this.e[i28];
                        if (i5 >= 4) {
                            this.S[0] = i5 - 4;
                            this.S[1] = kVar3.i;
                            this.S[2] = kVar3.j;
                            this.S[3] = kVar3.k;
                        } else if (i5 == 0) {
                            this.S[0] = kVar3.i;
                            this.S[1] = kVar3.j;
                            this.S[2] = kVar3.k;
                            this.S[3] = kVar3.l;
                        } else if (i5 == 1) {
                            this.S[0] = kVar3.j;
                            this.S[1] = kVar3.i;
                            this.S[2] = kVar3.k;
                            this.S[3] = kVar3.l;
                        } else if (i5 == 2) {
                            this.S[0] = kVar3.k;
                            this.S[1] = kVar3.i;
                            this.S[2] = kVar3.j;
                            this.S[3] = kVar3.l;
                        } else {
                            this.S[0] = kVar3.l;
                            this.S[1] = kVar3.i;
                            this.S[2] = kVar3.j;
                            this.S[3] = kVar3.k;
                        }
                    }
                    this.e[i26].a = c2;
                    this.e[i26].i = this.S[0];
                    this.e[i26].j = this.S[1];
                    this.e[i26].k = this.S[2];
                    this.e[i26].l = this.S[3];
                    int i30 = this.e[i26].f;
                    byte c5 = this.f.c(-1);
                    byte c6 = this.f.c(((0 - this.S[0]) - 1) - 1);
                    int i31 = i2 & this.H;
                    int a7 = i30 + d.a(this.h[(c2 << 4) + i31]) + this.s.a(i2, this.f.c(-2)).a(!a.e(c2), c6, c5);
                    k kVar4 = this.e[i26 + 1];
                    boolean z2 = false;
                    if (a7 < kVar4.f) {
                        kVar4.f = a7;
                        kVar4.g = i26;
                        kVar4.a();
                        z2 = true;
                    }
                    int b8 = i30 + d.b(this.h[(c2 << 4) + i31]);
                    int b9 = b8 + d.b(this.i[c2]);
                    if (c6 == c5 && ((kVar4.g >= i26 || kVar4.h != 0) && (a2 = a(c2, i31) + b9) <= kVar4.f)) {
                        kVar4.f = a2;
                        kVar4.g = i26;
                        kVar4.b();
                        z2 = true;
                    }
                    int min = Math.min(4095 - i26, this.f.h() + 1);
                    if (min < 2) {
                        i24 = i26;
                    } else {
                        int i32 = min > this.u ? this.u : min;
                        if (z2 || c6 == c5 || (b4 = this.f.b(0, this.S[0], Math.min(min - 1, this.u))) < 2) {
                            i6 = i25;
                        } else {
                            int a8 = a.a(c2);
                            int i33 = this.H & (i2 + 1);
                            int b10 = d.b(this.i[a8]) + d.b(this.h[(a8 << 4) + i33]) + a7;
                            int i34 = i26 + 1 + b4;
                            i6 = i25;
                            while (i6 < i34) {
                                i6++;
                                this.e[i6].f = 268435455;
                            }
                            int a9 = a(0, b4, a8, i33) + b10;
                            k kVar5 = this.e[i34];
                            if (a9 < kVar5.f) {
                                kVar5.f = a9;
                                kVar5.g = i26 + 1;
                                kVar5.h = 0;
                                kVar5.b = true;
                                kVar5.c = false;
                            }
                        }
                        int i35 = i6;
                        int i36 = 2;
                        for (int i37 = 0; i37 < 4; i37++) {
                            int b11 = this.f.b(-1, this.S[i37], i32);
                            if (b11 >= 2) {
                                int i38 = b11;
                                while (true) {
                                    if (i35 < i26 + i38) {
                                        i35++;
                                        this.e[i35].f = 268435455;
                                    } else {
                                        int a10 = a(i37, i38, c2, i31) + b9;
                                        k kVar6 = this.e[i26 + i38];
                                        if (a10 < kVar6.f) {
                                            kVar6.f = a10;
                                            kVar6.g = i26;
                                            kVar6.h = i37;
                                            kVar6.b = false;
                                        }
                                        i38--;
                                        if (i38 < 2) {
                                            break;
                                        }
                                    }
                                }
                                if (i37 == 0) {
                                    i36 = b11 + 1;
                                }
                                if (b11 < min && (b3 = this.f.b(b11, this.S[i37], Math.min((min - 1) - b11, this.u))) >= 2) {
                                    int c7 = a.c(c2);
                                    int a11 = d.a(this.h[((i2 + b11) & this.H) + (c7 << 4)]) + a(i37, b11, c2, i31) + b9 + this.s.a(i2 + b11, this.f.c((b11 - 1) - 1)).a(true, this.f.c((b11 - 1) - (this.S[i37] + 1)), this.f.c(b11 - 1));
                                    int a12 = a.a(c7);
                                    int i39 = (i2 + b11 + 1) & this.H;
                                    int b12 = a11 + d.b(this.h[(a12 << 4) + i39]) + d.b(this.i[a12]);
                                    int i40 = b11 + 1 + b3;
                                    while (i35 < i26 + i40) {
                                        i35++;
                                        this.e[i35].f = 268435455;
                                    }
                                    int a13 = a(0, b3, a12, i39) + b12;
                                    k kVar7 = this.e[i26 + i40];
                                    if (a13 < kVar7.f) {
                                        kVar7.f = a13;
                                        kVar7.g = i26 + b11 + 1;
                                        kVar7.h = 0;
                                        kVar7.b = true;
                                        kVar7.c = true;
                                        kVar7.d = i26;
                                        kVar7.e = i37;
                                    }
                                }
                            }
                        }
                        if (d2 > i32) {
                            int i41 = 0;
                            while (i32 > this.t[i41]) {
                                i41 += 2;
                            }
                            this.t[i41] = i32;
                            i7 = i41 + 2;
                        } else {
                            i32 = d2;
                            i7 = i27;
                        }
                        if (i32 >= i36) {
                            int a14 = b8 + d.a(this.i[c2]);
                            i25 = i35;
                            while (i25 < i26 + i32) {
                                i25++;
                                this.e[i25].f = 268435455;
                            }
                            int i42 = 0;
                            while (i36 > this.t[i42]) {
                                i42 += 2;
                            }
                            while (true) {
                                int i43 = this.t[i42 + 1];
                                int b13 = b(i43, i36, i31) + a14;
                                k kVar8 = this.e[i26 + i36];
                                if (b13 < kVar8.f) {
                                    kVar8.f = b13;
                                    kVar8.g = i26;
                                    kVar8.h = i43 + 4;
                                    kVar8.b = false;
                                }
                                if (i36 == this.t[i42]) {
                                    if (i36 < min && (b2 = this.f.b(i36, i43, Math.min((min - 1) - i36, this.u))) >= 2) {
                                        int b14 = a.b(c2);
                                        int a15 = b13 + d.a(this.h[((i2 + i36) & this.H) + (b14 << 4)]) + this.s.a(i2 + i36, this.f.c((i36 - 1) - 1)).a(true, this.f.c((i36 - (i43 + 1)) - 1), this.f.c(i36 - 1));
                                        int a16 = a.a(b14);
                                        int i44 = (i2 + i36 + 1) & this.H;
                                        int b15 = a15 + d.b(this.h[(a16 << 4) + i44]) + d.b(this.i[a16]);
                                        int i45 = i36 + 1 + b2;
                                        while (i25 < i26 + i45) {
                                            i25++;
                                            this.e[i25].f = 268435455;
                                        }
                                        int a17 = b15 + a(0, b2, a16, i44);
                                        k kVar9 = this.e[i26 + i45];
                                        if (a17 < kVar9.f) {
                                            kVar9.f = a17;
                                            kVar9.g = i26 + i36 + 1;
                                            kVar9.h = 0;
                                            kVar9.b = true;
                                            kVar9.c = true;
                                            kVar9.d = i26;
                                            kVar9.e = i43 + 4;
                                        }
                                    }
                                    i42 += 2;
                                    if (i42 == i7) {
                                        break;
                                    }
                                }
                                i36++;
                            }
                        } else {
                            i25 = i35;
                        }
                        i24 = i26;
                    }
                }
            } else {
                this.U = -1;
                return 1;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void e() {
        if (this.f != null && this.R) {
            this.f.g();
            this.f = null;
            this.R = false;
        }
    }

    /* access modifiers changed from: package-private */
    public void f() {
        this.g.a();
    }

    /* access modifiers changed from: package-private */
    public void f(int i2) {
        this.g.a(this.h, (this.b << 4) + i2, 1);
        this.g.a(this.i, this.b, 0);
        this.b = a.b(this.b);
        this.q.a(this.g, 0, i2);
        this.n[a.f(2)].a(this.g, 63);
        this.g.a(67108863, 26);
        this.p.b(this.g, 15);
    }

    /* access modifiers changed from: package-private */
    public void g() {
        e();
        f();
    }

    /* access modifiers changed from: package-private */
    public void g(int i2) {
        e();
        f(this.H & i2);
        this.g.c();
        this.g.d();
    }

    /* access modifiers changed from: package-private */
    public void h() {
        for (int i2 = 4; i2 < 128; i2++) {
            int a2 = a(i2);
            int i3 = (a2 >> 1) - 1;
            int i4 = ((a2 & 1) | 2) << i3;
            this.Z[i2] = b.a(this.o, (i4 - a2) - 1, i3, i2 - i4);
        }
        for (int i5 = 0; i5 < 4; i5++) {
            b bVar = this.n[i5];
            int i6 = i5 << 6;
            for (int i7 = 0; i7 < this.F; i7++) {
                this.B[i6 + i7] = bVar.a(i7);
            }
            for (int i8 = 14; i8 < this.F; i8++) {
                int[] iArr = this.B;
                int i9 = i6 + i8;
                iArr[i9] = iArr[i9] + ((((i8 >> 1) - 1) - 4) << 6);
            }
            int i10 = i5 * 128;
            int i11 = 0;
            while (i11 < 4) {
                this.C[i10 + i11] = this.B[i6 + i11];
                i11++;
            }
            while (i11 < 128) {
                this.C[i10 + i11] = this.B[a(i11) + i6] + this.Z[i11];
                i11++;
            }
        }
        this.aa = 0;
    }

    public boolean h(int i2) {
        int i3 = 0;
        if (i2 < 1 || i2 > 536870912) {
            return false;
        }
        this.K = i2;
        while (i2 > (1 << i3)) {
            i3++;
        }
        this.F = i3 * 2;
        return true;
    }

    /* access modifiers changed from: package-private */
    public void i() {
        for (int i2 = 0; i2 < 16; i2++) {
            this.D[i2] = this.p.b(i2);
        }
        this.E = 0;
    }
}
