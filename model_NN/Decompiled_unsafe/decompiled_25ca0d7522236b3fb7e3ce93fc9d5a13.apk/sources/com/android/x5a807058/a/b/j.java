package com.android.x5a807058.a.b;

import com.android.x5a807058.a.c.d;

class j {
    short[] a = new short[768];
    final /* synthetic */ i b;

    j(i iVar) {
        this.b = iVar;
    }

    public int a(boolean z, byte b2, byte b3) {
        int i = 0;
        int i2 = 1;
        int i3 = 7;
        if (z) {
            while (true) {
                if (i3 < 0) {
                    break;
                }
                int i4 = (b2 >> i3) & 1;
                int i5 = (b3 >> i3) & 1;
                i += d.b(this.a[((i4 + 1) << 8) + i2], i5);
                i2 = (i2 << 1) | i5;
                if (i4 != i5) {
                    i3--;
                    break;
                }
                i3--;
            }
        }
        while (i3 >= 0) {
            int i6 = (b3 >> i3) & 1;
            i += d.b(this.a[i2], i6);
            i2 = (i2 << 1) | i6;
            i3--;
        }
        return i;
    }

    public void a() {
        d.a(this.a);
    }

    public void a(d dVar, byte b2) {
        int i = 1;
        for (int i2 = 7; i2 >= 0; i2--) {
            int i3 = (b2 >> i2) & 1;
            dVar.a(this.a, i, i3);
            i = (i << 1) | i3;
        }
    }

    /* JADX WARN: Type inference failed for: r0v6, types: [int] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(com.android.x5a807058.a.c.d r9, byte r10, byte r11) {
        /*
            r8 = this;
            r1 = 1
            r0 = 7
            r4 = r0
            r3 = r1
            r0 = r1
        L_0x0005:
            if (r4 < 0) goto L_0x002c
            int r2 = r11 >> r4
            r5 = r2 & 1
            if (r0 == 0) goto L_0x002d
            int r0 = r10 >> r4
            r0 = r0 & 1
            int r2 = r0 + 1
            int r2 = r2 << 8
            int r2 = r2 + r3
            if (r0 != r5) goto L_0x002a
            r0 = r1
        L_0x0019:
            r7 = r2
            r2 = r0
            r0 = r7
        L_0x001c:
            short[] r6 = r8.a
            r9.a(r6, r0, r5)
            int r0 = r3 << 1
            r3 = r0 | r5
            int r0 = r4 + -1
            r4 = r0
            r0 = r2
            goto L_0x0005
        L_0x002a:
            r0 = 0
            goto L_0x0019
        L_0x002c:
            return
        L_0x002d:
            r2 = r0
            r0 = r3
            goto L_0x001c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.x5a807058.a.b.j.a(com.android.x5a807058.a.c.d, byte, byte):void");
    }
}
