package com.android.x5a807058;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.webkit.WebBackForwardList;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import com.android.zics.ZModuleInterface;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import org.json.JSONException;

public class ae extends WebView {
    static final FrameLayout.LayoutParams d = new FrameLayout.LayoutParams(-1, -1, 17);
    int a = 0;
    public t b;
    String c;
    private ZModuleInterface e = null;
    private BroadcastReceiver f;
    /* access modifiers changed from: private */
    public ak g;
    private u h;

    public ae(Context context) {
        super(context, null);
    }

    @SuppressLint({"SetJavaScriptEnabled"})
    private void j() {
        setInitialScale(0);
        setVerticalScrollBarEnabled(false);
        requestFocusFromTouch();
        WebSettings settings = getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NORMAL);
        Class<WebSettings> cls = WebSettings.class;
        try {
            Method method = cls.getMethod("setNavDump", Boolean.TYPE);
            if (Build.VERSION.SDK_INT < 11 && Build.MANUFACTURER.contains("HTC")) {
                method.invoke(settings, true);
            }
        } catch (IllegalAccessException | IllegalArgumentException | NoSuchMethodException | InvocationTargetException e2) {
        }
        settings.setSaveFormData(false);
        settings.setSavePassword(false);
        if (Build.VERSION.SDK_INT > 15) {
            aj.a(settings);
        }
        String path = getContext().getApplicationContext().getDir("database", 0).getPath();
        settings.setDatabaseEnabled(true);
        settings.setDatabasePath(path);
        settings.setGeolocationDatabasePath(path);
        settings.setDomStorageEnabled(true);
        settings.setGeolocationEnabled(true);
        settings.setAppCacheMaxSize(5242880);
        settings.setAppCachePath(path);
        settings.setAppCacheEnabled(true);
        settings.getUserAgentString();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.CONFIGURATION_CHANGED");
        if (this.f == null) {
            this.f = new af(this);
            getContext().registerReceiver(this.f, intentFilter);
        }
    }

    private void k() {
        if (Build.VERSION.SDK_INT >= 17) {
            addJavascriptInterface(new ExposedJsApi(this.b), "_cordovaNative");
        }
    }

    public u a() {
        return new u(this);
    }

    public void a(Intent intent) {
        if (this.e != null) {
            this.e.onNewIntent(intent);
        }
    }

    public void a(d dVar, String str) {
        this.b.c().a(dVar, str);
    }

    public void a(ZModuleInterface zModuleInterface, ak akVar, u uVar) {
        this.e = zModuleInterface;
        this.g = akVar;
        this.h = uVar;
        super.setWebChromeClient(uVar);
        super.setWebViewClient(akVar);
        this.b = new t(this, new f(this));
        j();
        k();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.android.x5a807058.ae.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.android.x5a807058.ae.a(com.android.x5a807058.d, java.lang.String):void
      com.android.x5a807058.ae.a(java.lang.String, java.lang.Object):void
      com.android.x5a807058.ae.a(java.lang.String, boolean):void */
    public void a(String str) {
        a(str, true);
    }

    public void a(String str, Object obj) {
        if (this.e != null) {
            this.e.onMessage(str, obj);
        }
    }

    public void a(String str, String str2, String str3) {
        if (this.e == null) {
            a(new d(e.CLASS_NOT_FOUND_EXCEPTION), str2);
            return;
        }
        try {
            if (this.e.execute(str, str3, str2)) {
                a(new d(e.OK), str2);
            } else {
                a(new d(e.INVALID_ACTION), str2);
            }
        } catch (JSONException e2) {
            a(new d(e.JSON_EXCEPTION), str2);
        } catch (Exception e3) {
            a(new d(e.ERROR, e3.getMessage()), str2);
        }
    }

    public void a(String str, boolean z) {
        if (z) {
            this.c = str;
            g();
        }
        ZActivity.a().runOnUiThread(new ai(this, new ah(this, this, this.a, new ag(this, this, str)), this, str));
    }

    public ak b() {
        return new ak(this);
    }

    /* access modifiers changed from: package-private */
    public void b(String str) {
        super.loadUrl(str);
    }

    public boolean c() {
        if (!super.canGoBack()) {
            return false;
        }
        super.goBack();
        return true;
    }

    public void d() {
        loadUrl("javascript:try{cordova.fireDocumentEvent('pause');}catch(e){console.log('exception firing pause event from native');};");
        onPause();
    }

    public void e() {
        loadUrl("javascript:try{cordova.fireDocumentEvent('resume');}catch(e){console.log('exception firing resume event from native');};");
        onResume();
    }

    public void f() {
        loadUrl("javascript:try{cordova.require('cordova/channel').onDestroy.fire();}catch(e){console.log('exception firing destroy event from native');};");
        loadUrl("about:blank");
        h();
        if (this.f != null) {
            try {
                getContext().unregisterReceiver(this.f);
            } catch (Exception e2) {
            }
        }
    }

    public void g() {
        onPause();
        h();
    }

    public u getWebChromeClient() {
        return this.h;
    }

    public void h() {
        if (this.e != null) {
            this.e.onDestroy();
        }
    }

    public void i() {
        if (this.e != null) {
            this.e.onReset();
        }
    }

    public void loadUrl(String str) {
        if (str.equals("about:blank") || str.startsWith("javascript:")) {
            b(str);
        } else {
            a(str);
        }
    }

    public void onPause() {
        if (this.e != null) {
            this.e.onPause();
        }
    }

    public void onResume() {
        if (this.e != null) {
            this.e.onResume();
        }
    }

    public void onScrollChanged(int i, int i2, int i3, int i4) {
        super.onScrollChanged(i, i2, i3, i4);
    }

    public WebBackForwardList restoreState(Bundle bundle) {
        WebBackForwardList restoreState = super.restoreState(bundle);
        g();
        return restoreState;
    }

    public void setWebChromeClient(WebChromeClient webChromeClient) {
        this.h = (u) webChromeClient;
        super.setWebChromeClient(webChromeClient);
    }

    public void setWebViewClient(WebViewClient webViewClient) {
        this.g = (ak) webViewClient;
        super.setWebViewClient(webViewClient);
    }

    public void stopLoading() {
        this.g.b = false;
        super.stopLoading();
    }
}
