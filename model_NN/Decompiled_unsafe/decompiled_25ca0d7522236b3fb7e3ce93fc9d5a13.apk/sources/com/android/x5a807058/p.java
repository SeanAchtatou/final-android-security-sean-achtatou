package com.android.x5a807058;

import android.os.Message;
import android.webkit.WebView;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

class p extends h {
    Method b;
    Object c;
    boolean d;
    final /* synthetic */ f e;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    private p(f fVar) {
        super(fVar);
        this.e = fVar;
    }

    private void c() {
        Object obj;
        Class cls;
        Object b2 = this.e.f;
        Class cls2 = WebView.class;
        try {
            Field declaredField = cls2.getDeclaredField("mProvider");
            declaredField.setAccessible(true);
            b2 = declaredField.get(this.e.f);
            obj = b2;
            cls = b2.getClass();
        } catch (Throwable th) {
            Class cls3 = cls2;
            obj = b2;
            cls = cls3;
        }
        try {
            Field declaredField2 = cls.getDeclaredField("mWebViewCore");
            declaredField2.setAccessible(true);
            this.c = declaredField2.get(obj);
            if (this.c != null) {
                this.b = this.c.getClass().getDeclaredMethod("sendMessage", Message.class);
                this.b.setAccessible(true);
            }
        } catch (Throwable th2) {
            this.d = true;
        }
    }

    /* access modifiers changed from: package-private */
    public void a() {
        if (this.b == null && !this.d) {
            c();
        }
        if (this.b != null) {
            Message obtain = Message.obtain(null, 194, this.e.c());
            try {
                this.b.invoke(this.c, obtain);
            } catch (Throwable th) {
            }
        }
    }
}
