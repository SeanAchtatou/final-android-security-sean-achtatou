package com.android.x5a807058;

import android.content.DialogInterface;
import android.webkit.JsPromptResult;
import android.widget.EditText;

class ac implements DialogInterface.OnClickListener {
    final /* synthetic */ EditText a;
    final /* synthetic */ JsPromptResult b;
    final /* synthetic */ u c;

    ac(u uVar, EditText editText, JsPromptResult jsPromptResult) {
        this.c = uVar;
        this.a = editText;
        this.b = jsPromptResult;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        this.b.confirm(this.a.getText().toString());
    }
}
