package com.android.x5a807058;

import android.webkit.JavascriptInterface;

class ExposedJsApi {
    private t _bridge;

    public ExposedJsApi(t tVar) {
        this._bridge = tVar;
    }

    @JavascriptInterface
    public String exec(int i, String str, String str2, String str3) {
        return this._bridge.a(i, str, str2, str3);
    }

    @JavascriptInterface
    public String retrieveJsMessages(int i, boolean z) {
        return this._bridge.a(i, z);
    }

    @JavascriptInterface
    public void setNativeToJsBridgeMode(int i, int i2) {
        this._bridge.a(i, i2);
    }
}
