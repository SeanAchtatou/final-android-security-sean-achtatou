package com.android.x5a807058;

import android.graphics.Bitmap;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import org.json.JSONException;
import org.json.JSONObject;

public class ak extends WebViewClient {
    ae a;
    boolean b;
    private boolean c = false;

    public ak(ae aeVar) {
        this.a = aeVar;
    }

    public void onPageFinished(WebView webView, String str) {
        super.onPageFinished(webView, str);
        if (this.b) {
            this.b = false;
            if (this.c) {
                webView.clearHistory();
                this.c = false;
            }
            this.a.a++;
            this.a.a("onPageFinished", str);
            if (this.a.getVisibility() == 4) {
                new Thread(new al(this)).start();
            }
            if (str.equals("about:blank")) {
                this.a.a("exit", (Object) null);
            }
        }
    }

    public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        super.onPageStarted(webView, str, bitmap);
        this.b = true;
        this.a.b.a(str);
        this.a.a("onPageStarted", str);
        this.a.i();
    }

    public void onReceivedError(WebView webView, int i, String str, String str2) {
        if (this.b) {
            this.a.a++;
            if (i == -10) {
                if (webView.canGoBack()) {
                    webView.goBack();
                    return;
                }
                super.onReceivedError(webView, i, str, str2);
            }
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put("errorCode", i);
                jSONObject.put("description", str);
                jSONObject.put("url", str2);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            this.a.a("onReceivedError", jSONObject);
        }
    }
}
