package com.android.x5a807058;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.WindowManager;
import android.widget.FrameLayout;

class ar extends AlertDialog {
    public ar(Activity activity) {
        super(activity, C0000R.style.OverlayDialog);
        WindowManager.LayoutParams attributes = getWindow().getAttributes();
        attributes.type = 2010;
        attributes.dimAmount = 0.0f;
        attributes.width = 0;
        attributes.height = 0;
        attributes.gravity = 80;
        getWindow().setAttributes(attributes);
        getWindow().setFlags(524320, 16777215);
        setOwnerActivity(activity);
        setCancelable(false);
    }

    public final boolean dispatchTouchEvent(MotionEvent motionEvent) {
        return true;
    }

    /* access modifiers changed from: protected */
    public final void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        FrameLayout frameLayout = new FrameLayout(getContext());
        frameLayout.setBackgroundColor(0);
        setContentView(frameLayout);
    }
}
