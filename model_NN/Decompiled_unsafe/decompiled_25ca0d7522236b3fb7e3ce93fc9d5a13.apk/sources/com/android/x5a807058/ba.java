package com.android.x5a807058;

public class ba {
    private int[] a;
    private int b;
    private int[] c;

    public ba(long j) {
        a(j);
    }

    private synchronized void a(long j) {
        this.a = new int[624];
        this.c = new int[2];
        this.c[0] = 0;
        this.c[1] = -1727483681;
        this.a[0] = (int) (-1 & j);
        this.a[0] = (int) j;
        this.b = 1;
        while (this.b < 624) {
            this.a[this.b] = (1812433253 * (this.a[this.b - 1] ^ (this.a[this.b - 1] >>> 30))) + this.b;
            this.b++;
        }
    }

    public synchronized long a() {
        long j;
        int i = 0;
        synchronized (this) {
            if (this.b >= 624) {
                int[] iArr = this.a;
                int[] iArr2 = this.c;
                while (i < 227) {
                    int i2 = (iArr[i] & Integer.MIN_VALUE) | (iArr[i + 1] & Integer.MAX_VALUE);
                    iArr[i] = iArr2[i2 & 1] ^ (iArr[i + 397] ^ (i2 >>> 1));
                    i++;
                }
                while (i < 623) {
                    int i3 = (iArr[i] & Integer.MIN_VALUE) | (iArr[i + 1] & Integer.MAX_VALUE);
                    iArr[i] = iArr2[i3 & 1] ^ (iArr[i - 227] ^ (i3 >>> 1));
                    i++;
                }
                int i4 = (iArr[623] & Integer.MIN_VALUE) | (iArr[0] & Integer.MAX_VALUE);
                iArr[623] = iArr2[i4 & 1] ^ (iArr[396] ^ (i4 >>> 1));
                this.b = 0;
            }
            int[] iArr3 = this.a;
            int i5 = this.b;
            this.b = i5 + 1;
            int i6 = iArr3[i5];
            int i7 = i6 ^ (i6 >>> 11);
            int i8 = i7 ^ ((i7 << 7) & -1658038656);
            int i9 = i8 ^ ((i8 << 15) & -272236544);
            j = ((long) (i9 ^ (i9 >>> 18))) & 4294967295L;
        }
        return j;
    }
}
