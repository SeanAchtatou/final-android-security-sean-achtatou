package com.android.x5a807058;

public class r {
    private long a;
    private long b;
    private long c;

    /* JADX INFO: additional move instructions added (1) to help type inference */
    private long a(byte[] bArr, int i) {
        byte b2 = bArr[i];
        byte b3 = bArr[i + 1];
        byte b4 = bArr[i + 2];
        byte b5 = bArr[i + 3];
        byte b6 = b2 & true;
        int i2 = b2;
        if (b6 == true) {
            i2 = (b2 & Byte.MAX_VALUE) + 128;
        }
        byte b7 = b3 & true;
        int i3 = b3;
        if (b7 == true) {
            i3 = (b3 & Byte.MAX_VALUE) + 128;
        }
        byte b8 = b4 & true;
        int i4 = b4;
        if (b8 == true) {
            i4 = (b4 & Byte.MAX_VALUE) + 128;
        }
        byte b9 = b5 & true;
        int i5 = b5;
        if (b9 == true) {
            i5 = (b5 & Byte.MAX_VALUE) + 128;
        }
        return (((long) i2) << 24) + (((long) i3) << 16) + (((long) i4) << 8) + ((long) i5);
    }

    private void a(byte[] bArr, int i, long j) {
        long j2 = j / 1000;
        long j3 = j2 + 2208988800L;
        int i2 = i + 1;
        bArr[i] = (byte) ((int) (j3 >> 24));
        int i3 = i2 + 1;
        bArr[i2] = (byte) ((int) (j3 >> 16));
        int i4 = i3 + 1;
        bArr[i3] = (byte) ((int) (j3 >> 8));
        int i5 = i4 + 1;
        bArr[i4] = (byte) ((int) (j3 >> 0));
        long j4 = (4294967296L * (j - (1000 * j2))) / 1000;
        int i6 = i5 + 1;
        bArr[i5] = (byte) ((int) (j4 >> 24));
        int i7 = i6 + 1;
        bArr[i6] = (byte) ((int) (j4 >> 16));
        int i8 = i7 + 1;
        bArr[i7] = (byte) ((int) (j4 >> 8));
        int i9 = i8 + 1;
        bArr[i8] = (byte) ((int) (Math.random() * 255.0d));
    }

    private long b(byte[] bArr, int i) {
        return ((a(bArr, i) - 2208988800L) * 1000) + ((a(bArr, i + 4) * 1000) / 4294967296L);
    }

    public long a() {
        return this.a;
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x007f  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0086  */
    /* JADX WARNING: Removed duplicated region for block: B:21:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(java.lang.String r19, int r20) {
        /*
            r18 = this;
            r2 = 0
            java.net.DatagramSocket r1 = new java.net.DatagramSocket     // Catch:{ Exception -> 0x007b, all -> 0x0083 }
            r1.<init>()     // Catch:{ Exception -> 0x007b, all -> 0x0083 }
            r0 = r20
            r1.setSoTimeout(r0)     // Catch:{ Exception -> 0x0091, all -> 0x008a }
            java.net.InetAddress r2 = java.net.InetAddress.getByName(r19)     // Catch:{ Exception -> 0x0091, all -> 0x008a }
            r3 = 48
            byte[] r3 = new byte[r3]     // Catch:{ Exception -> 0x0091, all -> 0x008a }
            java.net.DatagramPacket r4 = new java.net.DatagramPacket     // Catch:{ Exception -> 0x0091, all -> 0x008a }
            int r5 = r3.length     // Catch:{ Exception -> 0x0091, all -> 0x008a }
            r6 = 123(0x7b, float:1.72E-43)
            r4.<init>(r3, r5, r2, r6)     // Catch:{ Exception -> 0x0091, all -> 0x008a }
            r2 = 0
            r5 = 27
            r3[r2] = r5     // Catch:{ Exception -> 0x0091, all -> 0x008a }
            long r5 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0091, all -> 0x008a }
            long r7 = android.os.SystemClock.elapsedRealtime()     // Catch:{ Exception -> 0x0091, all -> 0x008a }
            r2 = 40
            r0 = r18
            r0.a(r3, r2, r5)     // Catch:{ Exception -> 0x0091, all -> 0x008a }
            r1.send(r4)     // Catch:{ Exception -> 0x0091, all -> 0x008a }
            java.net.DatagramPacket r2 = new java.net.DatagramPacket     // Catch:{ Exception -> 0x0091, all -> 0x008a }
            int r4 = r3.length     // Catch:{ Exception -> 0x0091, all -> 0x008a }
            r2.<init>(r3, r4)     // Catch:{ Exception -> 0x0091, all -> 0x008a }
            r1.receive(r2)     // Catch:{ Exception -> 0x0091, all -> 0x008a }
            long r9 = android.os.SystemClock.elapsedRealtime()     // Catch:{ Exception -> 0x0091, all -> 0x008a }
            long r11 = r9 - r7
            long r4 = r5 + r11
            r2 = 24
            r0 = r18
            long r11 = r0.b(r3, r2)     // Catch:{ Exception -> 0x0091, all -> 0x008a }
            r2 = 32
            r0 = r18
            long r13 = r0.b(r3, r2)     // Catch:{ Exception -> 0x0091, all -> 0x008a }
            r2 = 40
            r0 = r18
            long r2 = r0.b(r3, r2)     // Catch:{ Exception -> 0x0091, all -> 0x008a }
            long r6 = r9 - r7
            long r15 = r2 - r13
            long r6 = r6 - r15
            long r11 = r13 - r11
            long r2 = r2 - r4
            long r2 = r2 + r11
            r11 = 2
            long r2 = r2 / r11
            long r2 = r2 + r4
            r0 = r18
            r0.a = r2     // Catch:{ Exception -> 0x0091, all -> 0x008a }
            r0 = r18
            r0.b = r9     // Catch:{ Exception -> 0x0091, all -> 0x008a }
            r0 = r18
            r0.c = r6     // Catch:{ Exception -> 0x0091, all -> 0x008a }
            if (r1 == 0) goto L_0x0079
            r1.close()
        L_0x0079:
            r1 = 1
        L_0x007a:
            return r1
        L_0x007b:
            r1 = move-exception
        L_0x007c:
            r1 = 0
            if (r2 == 0) goto L_0x007a
            r2.close()
            goto L_0x007a
        L_0x0083:
            r1 = move-exception
        L_0x0084:
            if (r2 == 0) goto L_0x0089
            r2.close()
        L_0x0089:
            throw r1
        L_0x008a:
            r2 = move-exception
            r17 = r2
            r2 = r1
            r1 = r17
            goto L_0x0084
        L_0x0091:
            r2 = move-exception
            r2 = r1
            goto L_0x007c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.x5a807058.r.a(java.lang.String, int):boolean");
    }

    public long b() {
        return this.b;
    }
}
