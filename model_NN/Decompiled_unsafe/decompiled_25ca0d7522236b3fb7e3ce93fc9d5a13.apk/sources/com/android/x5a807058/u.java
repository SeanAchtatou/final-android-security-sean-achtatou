package com.android.x5a807058;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.view.View;
import android.webkit.ConsoleMessage;
import android.webkit.GeolocationPermissions;
import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebStorage;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

public class u extends WebChromeClient {
    protected ae a;
    private long b = 104857600;
    private View c;

    public u(ae aeVar) {
        this.a = aeVar;
    }

    public View getVideoLoadingProgressView() {
        if (this.c == null) {
            LinearLayout linearLayout = new LinearLayout(this.a.getContext());
            linearLayout.setOrientation(1);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams.addRule(13);
            linearLayout.setLayoutParams(layoutParams);
            ProgressBar progressBar = new ProgressBar(this.a.getContext());
            LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-2, -2);
            layoutParams2.gravity = 17;
            progressBar.setLayoutParams(layoutParams2);
            linearLayout.addView(progressBar);
            this.c = linearLayout;
        }
        return this.c;
    }

    @TargetApi(8)
    public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
        return super.onConsoleMessage(consoleMessage);
    }

    public void onExceededDatabaseQuota(String str, String str2, long j, long j2, long j3, WebStorage.QuotaUpdater quotaUpdater) {
        quotaUpdater.updateQuota(this.b);
    }

    public void onGeolocationPermissionsShowPrompt(String str, GeolocationPermissions.Callback callback) {
        super.onGeolocationPermissionsShowPrompt(str, callback);
        callback.invoke(str, true, false);
    }

    public boolean onJsAlert(WebView webView, String str, String str2, JsResult jsResult) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ZActivity.a());
        builder.setMessage(str2);
        builder.setTitle("Alert");
        builder.setCancelable(true);
        builder.setPositiveButton(17039370, new v(this, jsResult));
        builder.setOnCancelListener(new w(this, jsResult));
        builder.setOnKeyListener(new x(this, jsResult));
        builder.show();
        return true;
    }

    public boolean onJsConfirm(WebView webView, String str, String str2, JsResult jsResult) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ZActivity.a());
        builder.setMessage(str2);
        builder.setTitle("Confirm");
        builder.setCancelable(true);
        builder.setPositiveButton(17039370, new y(this, jsResult));
        builder.setNegativeButton(17039360, new z(this, jsResult));
        builder.setOnCancelListener(new aa(this, jsResult));
        builder.setOnKeyListener(new ab(this, jsResult));
        builder.show();
        return true;
    }

    public boolean onJsPrompt(WebView webView, String str, String str2, String str3, JsPromptResult jsPromptResult) {
        String a2 = this.a.b.a(str, str2, str3);
        if (a2 != null) {
            jsPromptResult.confirm(a2);
            return true;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(ZActivity.a());
        builder.setMessage(str2);
        EditText editText = new EditText(ZActivity.a());
        if (str3 != null) {
            editText.setText(str3);
        }
        builder.setView(editText);
        builder.setCancelable(false);
        builder.setPositiveButton(17039370, new ac(this, editText, jsPromptResult));
        builder.setNegativeButton(17039360, new ad(this, jsPromptResult));
        builder.show();
        return true;
    }
}
