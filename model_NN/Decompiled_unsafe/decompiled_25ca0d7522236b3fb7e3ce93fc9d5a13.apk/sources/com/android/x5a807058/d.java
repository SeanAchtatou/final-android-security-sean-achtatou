package com.android.x5a807058;

import org.json.JSONObject;

public class d {
    public static String[] a = {"No result", "OK", "Class not found", "Illegal access", "Instantiation error", "Malformed url", "IO error", "Invalid action", "JSON error", "Error"};
    private final int b;
    private final int c;
    private boolean d;
    private String e;
    private String f;

    public d(e eVar) {
        this(eVar, a[eVar.ordinal()]);
    }

    public d(e eVar, String str) {
        this.d = false;
        this.b = eVar.ordinal();
        this.c = 1;
        this.e = str;
    }

    public int a() {
        return this.b;
    }

    public int b() {
        return this.c;
    }

    public String c() {
        if (this.f == null) {
            this.f = JSONObject.quote(this.e);
        }
        return this.f;
    }

    public String d() {
        return this.e;
    }

    public boolean e() {
        return this.d;
    }
}
