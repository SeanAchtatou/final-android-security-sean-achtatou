package com.android.x5a807058;

import com.android.zics.ZModuleInterface;
import java.util.Comparator;

class bg implements Comparator {
    final /* synthetic */ bb a;

    private bg(bb bbVar) {
        this.a = bbVar;
    }

    /* synthetic */ bg(bb bbVar, bc bcVar) {
        this(bbVar);
    }

    /* renamed from: a */
    public int compare(ZModuleInterface zModuleInterface, ZModuleInterface zModuleInterface2) {
        return Integer.valueOf(this.a.getModulePriority(zModuleInterface.getHash())).compareTo(Integer.valueOf(this.a.getModulePriority(zModuleInterface2.getHash())));
    }
}
