package com.android.x5a807058;

import com.android.zics.ZInputStreamInterface;
import java.io.InputStream;

public class ay implements ZInputStreamInterface {
    private InputStream a;

    public void close() {
        this.a.close();
    }

    public void init(InputStream inputStream) {
        this.a = inputStream;
    }

    public int read(byte[] bArr) {
        return this.a.read(bArr);
    }

    public int read(byte[] bArr, int i, int i2) {
        return this.a.read(bArr, i, i2);
    }

    public String readBinaryString() {
        byte[] bArr = new byte[readInt()];
        this.a.read(bArr);
        return new String(bArr);
    }

    public int readInt() {
        byte[] bArr = new byte[4];
        this.a.read(bArr);
        int i = 0;
        for (int i2 = 0; i2 < bArr.length; i2++) {
            i |= (bArr[i2] & 255) << (i2 * 8);
        }
        return i;
    }

    public long readLong() {
        long j = 0;
        byte[] bArr = new byte[8];
        this.a.read(bArr);
        for (int i = 0; i < bArr.length; i++) {
            j |= (((long) bArr[i]) & 255) << (i * 8);
        }
        return j;
    }
}
