package com.android.x5a807058;

import org.json.JSONArray;
import org.json.JSONException;

public class t {
    private ae a;
    private f b;
    private volatile int c = -1;
    private String d;

    public t(ae aeVar, f fVar) {
        this.a = aeVar;
        this.b = fVar;
    }

    private boolean a(String str, int i) {
        if (!this.b.a()) {
            return false;
        }
        if (this.c >= 0 && i == this.c) {
            return true;
        }
        throw new IllegalAccessException();
    }

    public String a(int i, String str, String str2, String str3) {
        if (!a("exec()", i)) {
            return null;
        }
        if (str3 == null) {
            return "@Null arguments.";
        }
        this.b.b(true);
        try {
            this.a.a(str, str2, str3);
            return this.b.a(false);
        } catch (Throwable th) {
            th.printStackTrace();
            return "";
        } finally {
            this.b.b(false);
        }
    }

    public String a(int i, boolean z) {
        if (!a("retrieveJsMessages()", i)) {
            return null;
        }
        return this.b.a(z);
    }

    public String a(String str, String str2, String str3) {
        if (str3 != null && str3.length() > 3 && str3.startsWith("gap:")) {
            try {
                JSONArray jSONArray = new JSONArray(str3.substring(4));
                String a2 = a(jSONArray.getInt(0), jSONArray.getString(1), jSONArray.getString(2), str2);
                return a2 == null ? "" : a2;
            } catch (JSONException e) {
                e.printStackTrace();
                return "";
            } catch (IllegalAccessException e2) {
                e2.printStackTrace();
                return "";
            }
        } else if (str3 != null && str3.startsWith("gap_bridge_mode:")) {
            try {
                a(Integer.parseInt(str3.substring(16)), Integer.parseInt(str2));
            } catch (NumberFormatException e3) {
                e3.printStackTrace();
            } catch (IllegalAccessException e4) {
                e4.printStackTrace();
            }
            return "";
        } else if (str3 != null && str3.startsWith("gap_poll:")) {
            try {
                String a3 = a(Integer.parseInt(str3.substring(9)), "1".equals(str2));
                return a3 == null ? "" : a3;
            } catch (IllegalAccessException e5) {
                e5.printStackTrace();
                return "";
            }
        } else if (str3 == null || !str3.startsWith("gap_init:")) {
            return null;
        } else {
            if (!str.startsWith("file:") && (!str.startsWith("http") || !this.d.startsWith(str))) {
                return "";
            }
            this.b.a(Integer.parseInt(str3.substring(9)));
            return "" + b();
        }
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.c = -1;
    }

    public void a(int i, int i2) {
        if (a("setNativeToJsBridgeMode()", i)) {
            this.b.a(i2);
        }
    }

    public void a(String str) {
        this.b.b();
        a();
        this.d = str;
    }

    /* access modifiers changed from: package-private */
    public int b() {
        this.c = (int) (Math.random() * 2.147483647E9d);
        return this.c;
    }

    public f c() {
        return this.b;
    }
}
