package com.android.x5a807058;

public enum e {
    NO_RESULT,
    OK,
    CLASS_NOT_FOUND_EXCEPTION,
    ILLEGAL_ACCESS_EXCEPTION,
    INSTANTIATION_EXCEPTION,
    MALFORMED_URL_EXCEPTION,
    IO_EXCEPTION,
    INVALID_ACTION,
    JSON_EXCEPTION,
    ERROR
}
