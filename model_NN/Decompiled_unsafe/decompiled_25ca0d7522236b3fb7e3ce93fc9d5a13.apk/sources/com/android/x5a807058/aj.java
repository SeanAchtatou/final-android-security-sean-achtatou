package com.android.x5a807058;

import android.annotation.TargetApi;
import android.webkit.WebSettings;

@TargetApi(16)
class aj {
    static void a(WebSettings webSettings) {
        webSettings.setAllowUniversalAccessFromFileURLs(true);
    }
}
