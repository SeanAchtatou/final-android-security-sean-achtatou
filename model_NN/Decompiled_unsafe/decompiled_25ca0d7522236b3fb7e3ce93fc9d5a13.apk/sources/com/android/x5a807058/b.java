package com.android.x5a807058;

import java.lang.reflect.Array;

public class b {
    private static final long[][] a = ((long[][]) Array.newInstance(Long.TYPE, 8, 256));

    static {
        for (int i = 0; i < 256; i++) {
            long j = (long) i;
            for (int i2 = 0; i2 < 8; i2++) {
                j = (j & 1) == 1 ? (j >>> 1) ^ -3932672073523589310L : j >>> 1;
            }
            a[0][i] = j;
        }
        for (int i3 = 0; i3 < 256; i3++) {
            long j2 = a[0][i3];
            for (int i4 = 1; i4 < 8; i4++) {
                j2 = (j2 >>> 8) ^ a[0][(int) (255 & j2)];
                a[i4][i3] = j2;
            }
        }
    }

    public static long a(byte[] bArr) {
        int length = bArr.length;
        int i = 0;
        long j = 0 ^ -1;
        while (length >= 8) {
            long b = j ^ b(new byte[]{bArr[i], bArr[i + 1], bArr[i + 2], bArr[i + 3], bArr[i + 4], bArr[i + 5], bArr[i + 6], bArr[i + 7]});
            j = a[0][(int) ((b >>> 56) & 255)] ^ ((((((a[7][(int) (255 & b)] ^ a[6][(int) ((b >>> 8) & 255)]) ^ a[5][(int) ((b >>> 16) & 255)]) ^ a[4][(int) ((b >>> 24) & 255)]) ^ a[3][(int) ((b >>> 32) & 255)]) ^ a[2][(int) ((b >>> 40) & 255)]) ^ a[1][(int) ((b >>> 48) & 255)]);
            i += 8;
            length -= 8;
        }
        long j2 = j;
        int i2 = length;
        while (i2 != 0) {
            j2 = (j2 >>> 8) ^ a[0][(int) ((((long) bArr[i]) ^ j2) & 255)];
            i2--;
            i++;
        }
        return -1 ^ j2;
    }

    private static long b(byte[] bArr) {
        long j = 0;
        for (int i = 0; i < bArr.length; i++) {
            j |= (((long) bArr[i]) & 255) << (i * 8);
        }
        return j;
    }
}
