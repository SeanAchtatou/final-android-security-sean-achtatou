package com.android.zics;

public interface ZCtrlRequestInterface {
    ZInputStreamInterface doRequest();

    ZModuleInterface getModuleOwner();

    int getRequestHash();

    ZOutputStreamInterface getRequestStream();

    ZInputStreamInterface getResponseStream();

    void init(ZRuntimeInterface zRuntimeInterface, int i);

    void setModuleOwner(ZModuleInterface zModuleInterface);
}
