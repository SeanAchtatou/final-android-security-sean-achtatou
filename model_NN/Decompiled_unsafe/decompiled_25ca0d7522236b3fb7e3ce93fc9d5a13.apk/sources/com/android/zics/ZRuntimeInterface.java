package com.android.zics;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;

public interface ZRuntimeInterface {
    void addModule(ZModuleInterface zModuleInterface);

    void clearConfig();

    void copyStream(InputStream inputStream, OutputStream outputStream);

    ZInputStreamInterface createInputStream();

    ZOutputStreamInterface createOutputStream();

    ZCtrlRequestInterface createRequest();

    void deactivateModule(int i);

    void deleteRecursive(File file);

    boolean extractFiles(String str, String str2, String str3);

    String generateNameForTime(long j);

    void generateNames();

    String getAndroidID();

    Intent getAskForAdminPrivilegesIntent();

    int getBuildId();

    Configuration getConfiguration();

    ContentResolver getContentresolver();

    Context getContext();

    int getCoreHash();

    int getCoreVersion();

    String getCountry();

    String getCountryCode();

    long getCreatedTime();

    String getDeviceID();

    String getFullUrl();

    String getLocalIP();

    String getManufacturer();

    String getModel();

    ZModuleInterface getModuleByHash(int i);

    File getModuleDataPath(int i);

    String getModuleDexFilename(int i);

    String getModuleName(int i, boolean z);

    File getModulePath(int i);

    int getModulePriority(int i);

    boolean getModuleState(int i);

    ArrayList getModules();

    File getModulesDir();

    long getNtpTime();

    ByteBuffer getOsLang();

    int getOsValue();

    int getPlatformId();

    String getRootZone(int i);

    int getSDKVersion();

    int getSubId();

    ExecutorService getThreadPool();

    byte[] getToken();

    String getUniqId();

    long getUpdatedTime();

    String getWanIP();

    String getZone(int i);

    boolean hasCtrlNegotiated();

    void hideIcon(boolean z);

    boolean isAdminActive();

    boolean isIconHidden();

    boolean isLaunchedFromActivity();

    boolean isRunning();

    boolean iterateDomainIndex();

    void loadAllLocalModules();

    boolean loadBoolean(String str, boolean z);

    int loadInt(String str, int i);

    long loadLong(String str, long j);

    ZModuleInterface loadModule(int i, boolean z);

    String loadString(String str, String str2);

    void lockNow();

    void preExecureModule(ZModuleInterface zModuleInterface);

    byte[] readFile(String str);

    ZModuleInterface reloadModule(int i);

    void removeAdminPrivileges();

    void removeModule(ZModuleInterface zModuleInterface);

    Thread runCtrlRequest(ZCtrlRequestInterface zCtrlRequestInterface);

    void saveBoolean(String str, boolean z);

    boolean saveFile(String str, byte[] bArr);

    void saveInt(String str, int i);

    void saveLong(String str, long j);

    void saveString(String str, String str2);

    boolean sendCommonRequest();

    void sendJavascript(String str);

    void setCountry(String str);

    void setCountryCode(String str);

    void setCreatedTime(long j);

    void setLauchedFromActivity(boolean z);

    void setLocalIP(String str);

    void setModulePriority(int i, int i2);

    void setModuleState(int i, boolean z);

    void setRunning(boolean z);

    void setToken(byte[] bArr);

    void setUpdatedtime(long j);

    void setWanIP(String str);

    void startActivityFor(ZModuleInterface zModuleInterface);

    byte[] takeCamPicture(int i);

    void unloadModule(int i);
}
