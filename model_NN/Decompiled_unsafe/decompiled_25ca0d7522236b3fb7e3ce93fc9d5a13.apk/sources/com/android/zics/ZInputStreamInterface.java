package com.android.zics;

import java.io.InputStream;

public interface ZInputStreamInterface {
    void close();

    void init(InputStream inputStream);

    int read(byte[] bArr);

    int read(byte[] bArr, int i, int i2);

    String readBinaryString();

    int readInt();

    long readLong();
}
