package com.android.zics;

import android.content.Intent;

public interface ZModuleInterface {
    void done();

    boolean execute(long j);

    boolean execute(String str, String str2, String str3);

    int getHash();

    int getVersion();

    void init(ZRuntimeInterface zRuntimeInterface);

    String[] listForExtract();

    boolean needFinishActivity();

    void onCtrlResponse(ZCtrlRequestInterface zCtrlRequestInterface);

    void onDestroy();

    void onMessage(String str, Object obj);

    void onNewIntent(Intent intent);

    void onPause();

    void onReset();

    void onResume();

    void preExecute();
}
