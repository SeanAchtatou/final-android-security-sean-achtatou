package com.android.zics;

public interface ZOutputStreamInterface {
    byte[] toByteArray();

    void writeBinaryString(String str);

    void writeData(byte[] bArr);

    void writeData(byte[] bArr, int i, int i2);

    void writeInt(int i);

    void writeLong(long j);

    void writeTwoBinaryStrings(String str, String str2);
}
