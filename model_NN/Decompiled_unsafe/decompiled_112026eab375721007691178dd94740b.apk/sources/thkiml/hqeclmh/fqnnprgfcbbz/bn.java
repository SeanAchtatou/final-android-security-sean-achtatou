package thkiml.hqeclmh.fqnnprgfcbbz;

import android.widget.RelativeLayout;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class bn {
    RelativeLayout a;

    public bn(RelativeLayout relativeLayout) {
        this.a = relativeLayout;
    }

    public void a(int i) {
        Class<RelativeLayout> cls = RelativeLayout.class;
        try {
            Method method = cls.getMethod("setVisibility", Integer.TYPE);
            try {
                method.invoke(this.a, Integer.valueOf(i));
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e2) {
                e2.printStackTrace();
            } catch (InvocationTargetException e3) {
                e3.printStackTrace();
            }
        } catch (NoSuchMethodException e4) {
            e4.printStackTrace();
        }
    }
}
