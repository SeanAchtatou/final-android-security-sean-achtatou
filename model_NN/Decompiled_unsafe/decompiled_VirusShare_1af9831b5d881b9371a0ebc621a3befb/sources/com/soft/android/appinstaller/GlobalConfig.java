package com.soft.android.appinstaller;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;

public final class GlobalConfig {
    private static GlobalConfig instance = null;
    private static final String tag = "GlobalConfig";
    Context con;
    private HashMap<String, String> config;
    private ActivityTexts finishTexts = null;
    private ActivityTexts firstActivityTexts = null;
    private HashMap<String, String> globalVars;
    private boolean isInitialized = false;
    private Resources res;
    private ActivityTexts rulesTexts = null;

    private GlobalConfig() {
        Log.v(tag, "Creating instance...");
        this.globalVars = new HashMap<>();
    }

    public static GlobalConfig getInstance() {
        if (instance == null) {
            instance = new GlobalConfig();
        }
        return instance;
    }

    private void parseConfigLine(String s) {
        int div = s.indexOf(32);
        if (div > 0) {
            this.config.put(s.substring(0, div), s.substring(div + 1));
        }
    }

    public void init(Context con2) {
        if (!this.isInitialized) {
            this.con = con2;
            this.isInitialized = true;
            this.config = new HashMap<>();
            this.res = con2.getResources();
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(this.res.openRawResource(R.raw.config), "UTF-8"));
                while (true) {
                    String line = br.readLine();
                    if (line != null) {
                        parseConfigLine(line);
                    } else {
                        return;
                    }
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }
    }

    public String getPrefsName() {
        return "prefs" + getValue("id");
    }

    public String getValue(String key) {
        String s;
        return (this.config == null || (s = this.config.get(key)) == null) ? "" : s;
    }

    public String getValue(String key, String defaultValue) {
        if (this.config == null) {
            return defaultValue;
        }
        String s = this.config.get(key);
        if (s == null) {
            return defaultValue;
        }
        return s;
    }

    public boolean getValueNull(String key) {
        if (this.config == null) {
            return true;
        }
        if (this.config.get(key) == null) {
            return true;
        }
        return false;
    }

    private static String replace(String _text, String _searchStr, String _replacementStr) {
        StringBuffer sb = new StringBuffer();
        int searchStringPos = _text.indexOf(_searchStr);
        int startPos = 0;
        int searchStringLength = _searchStr.length();
        while (searchStringPos != -1) {
            sb.append(_text.substring(startPos, searchStringPos)).append(_replacementStr);
            startPos = searchStringPos + searchStringLength;
            searchStringPos = _text.indexOf(_searchStr, startPos);
        }
        sb.append(_text.substring(startPos, _text.length()));
        return sb.toString();
    }

    private ActivityTexts readFile(int id, ArrayList<ReplaceItem> replaces) {
        InputStream is = this.res.openRawResource(id);
        StringBuilder text = new StringBuilder();
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            String title = null;
            boolean firstLine = true;
            while (true) {
                String line = br.readLine();
                if (line == null) {
                    return new ActivityTexts(title, text.toString());
                }
                for (int i = 0; i < replaces.size(); i++) {
                    ReplaceItem r = replaces.get(i);
                    line = replace(line, r.getFrom(), r.getTo());
                }
                if (firstLine) {
                    title = line;
                    firstLine = false;
                } else {
                    text.append(line);
                    text.append(10);
                }
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return new ActivityTexts("Error", "Resource error(UnsupportedEncodingException)", true);
        } catch (IOException e2) {
            e2.printStackTrace();
            return new ActivityTexts("Error", "Resource error (IOException)", true);
        }
    }

    private ActivityTexts readFile(String asset, ArrayList<ReplaceItem> replaces) {
        Log.v(tag, asset);
        try {
            InputStream is = this.res.getAssets().open(asset);
            StringBuilder text = new StringBuilder();
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                String title = null;
                boolean firstLine = true;
                while (true) {
                    String line = br.readLine();
                    if (line == null) {
                        return new ActivityTexts(title, text.toString());
                    }
                    for (int i = 0; i < replaces.size(); i++) {
                        ReplaceItem r = replaces.get(i);
                        line = replace(line, r.getFrom(), r.getTo());
                    }
                    if (firstLine) {
                        title = line;
                        firstLine = false;
                    } else {
                        text.append(line);
                        text.append(10);
                    }
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                return new ActivityTexts("Error", "Resource error(UnsupportedEncodingException)", true);
            } catch (IOException e2) {
                e2.printStackTrace();
                return new ActivityTexts("Error", "Resource error (IOException)", true);
            }
        } catch (IOException e1) {
            Log.v("error", e1.getMessage());
            return new ActivityTexts("Error", "Resource error", true);
        }
    }

    private ArrayList<ReplaceItem> getReplaces() {
        ArrayList<ReplaceItem> replaces = new ArrayList<>();
        replaces.add(new ReplaceItem("%id%", getValue("id")));
        replaces.add(new ReplaceItem("%midname%", getValue("midname")));
        replaces.add(new ReplaceItem("%dfrom%", getValue("dfrom")));
        replaces.add(new ReplaceItem("%megafonRules%", getValue("megafonRules")));
        return replaces;
    }

    public ActivityTexts getFirstActivityTexts() {
        if (this.firstActivityTexts == null) {
            this.firstActivityTexts = getTextForLocation("first_activity.txt");
        }
        return this.firstActivityTexts;
    }

    public ActivityTexts getRulesTexts() {
        if (this.rulesTexts == null) {
            this.rulesTexts = getTextForLocation("rules_activity.txt");
        }
        return this.rulesTexts;
    }

    public ActivityTexts getFinishTexts() {
        if (this.finishTexts == null) {
            this.finishTexts = getTextForLocation("finish_activity.txt");
        }
        return this.finishTexts;
    }

    public ActivityTexts getTextForLocation(String fileName) {
        OpInfo.getInstance().init(this.con);
        ArrayList<String> loc = OpInfo.getInstance().getLoc();
        ActivityTexts t = new ActivityTexts("Error", "Resource not found", true);
        for (int i = 0; i < loc.size(); i++) {
            t = readFile("texts/" + loc.get(i) + "/" + fileName, getReplaces());
            if (!t.isError()) {
                return t;
            }
        }
        return t;
    }

    public void setRuntimeValue(String key, String value) {
        this.globalVars.put(key, value);
    }

    public String getRuntimeValue(String key) {
        return this.globalVars.get(key);
    }

    public boolean existsRuntimeValue(String key) {
        return this.globalVars.containsKey(key);
    }
}
