package eu.inmite.android.lib.dialogs;

import android.view.View;

/* compiled from: SimpleDialogFragment */
class e implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SimpleDialogFragment f2561a;

    e(SimpleDialogFragment simpleDialogFragment) {
        this.f2561a = simpleDialogFragment;
    }

    public void onClick(View view) {
        c e = this.f2561a.e();
        if (e != null) {
            e.onNegativeButtonClicked(this.f2561a.e);
        }
        this.f2561a.dismiss();
    }
}
