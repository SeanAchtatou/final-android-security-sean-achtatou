package eu.inmite.android.lib.dialogs;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import com.baidu.mobads.openad.d.b;
import eu.inmite.android.lib.dialogs.BaseDialogFragment;

public class SimpleDialogFragment extends BaseDialogFragment {

    /* renamed from: a  reason: collision with root package name */
    protected static String f2558a = b.EVENT_MESSAGE;
    protected static String b = "title";
    protected static String c = "positive_button";
    protected static String d = "negative_button";
    protected int e;

    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        if (getTargetFragment() != null) {
            this.e = getTargetRequestCode();
            return;
        }
        Bundle arguments = getArguments();
        if (arguments != null) {
            this.e = arguments.getInt(a.f2559a, 0);
        }
    }

    /* access modifiers changed from: protected */
    public BaseDialogFragment.a a(BaseDialogFragment.a aVar) {
        String b2 = b();
        if (!TextUtils.isEmpty(b2)) {
            aVar.a(b2);
        }
        CharSequence a2 = a();
        if (!TextUtils.isEmpty(a2)) {
            aVar.b(a2);
        }
        String c2 = c();
        if (!TextUtils.isEmpty(c2)) {
            aVar.a(c2, new d(this));
        }
        String d2 = d();
        if (!TextUtils.isEmpty(d2)) {
            aVar.b(d2, new e(this));
        }
        return aVar;
    }

    /* access modifiers changed from: protected */
    public CharSequence a() {
        return getArguments().getCharSequence(f2558a);
    }

    /* access modifiers changed from: protected */
    public String b() {
        return getArguments().getString(b);
    }

    /* access modifiers changed from: protected */
    public String c() {
        return getArguments().getString(c);
    }

    /* access modifiers changed from: protected */
    public String d() {
        return getArguments().getString(d);
    }

    public void onCancel(DialogInterface dialogInterface) {
        super.onCancel(dialogInterface);
        b f = f();
        if (f != null) {
            f.onCancelled(this.e);
        }
    }

    /* access modifiers changed from: protected */
    public c e() {
        Fragment targetFragment = getTargetFragment();
        if (targetFragment != null) {
            if (targetFragment instanceof c) {
                return (c) targetFragment;
            }
        } else if (getParentFragment() != null && (getParentFragment() instanceof c)) {
            return (c) getParentFragment();
        } else {
            if (getActivity() instanceof c) {
                return (c) getActivity();
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public b f() {
        Fragment targetFragment = getTargetFragment();
        if (targetFragment != null) {
            if (targetFragment instanceof b) {
                return (b) targetFragment;
            }
        } else if (getParentFragment() != null && (getParentFragment() instanceof b)) {
            return (b) getParentFragment();
        } else {
            if (getActivity() instanceof b) {
                return (b) getActivity();
            }
        }
        return null;
    }
}
