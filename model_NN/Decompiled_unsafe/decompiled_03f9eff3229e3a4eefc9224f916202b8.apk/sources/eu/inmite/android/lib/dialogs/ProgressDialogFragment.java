package eu.inmite.android.lib.dialogs;

import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import cn.a.a.a;
import com.baidu.mobads.openad.d.b;
import eu.inmite.android.lib.dialogs.BaseDialogFragment;

public class ProgressDialogFragment extends BaseDialogFragment {

    /* renamed from: a  reason: collision with root package name */
    protected static String f2557a = b.EVENT_MESSAGE;
    protected static String b = "title";
    protected int c;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: protected */
    public BaseDialogFragment.a a(BaseDialogFragment.a aVar) {
        int color = getResources().getColor(a.c.sdl_message_text_dark);
        TypedArray obtainStyledAttributes = getActivity().getTheme().obtainStyledAttributes(null, a.j.DialogStyle, a.b.sdlDialogStyle, 0);
        int color2 = obtainStyledAttributes.getColor(3, color);
        obtainStyledAttributes.recycle();
        View inflate = aVar.a().inflate(a.g.dialog_part_progress, (ViewGroup) null, false);
        TextView textView = (TextView) inflate.findViewById(a.f.sdl__message);
        textView.setText(getArguments().getString(f2557a));
        textView.setTextColor(color2);
        aVar.a(inflate);
        aVar.a(getArguments().getString(b));
        return aVar;
    }

    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        if (getArguments() == null) {
            throw new IllegalArgumentException("use ProgressDialogBuilder to construct this dialog");
        }
        this.c = getTargetFragment() != null ? getTargetRequestCode() : getArguments().getInt(a.f2559a, 0);
    }

    public void onCancel(DialogInterface dialogInterface) {
        super.onCancel(dialogInterface);
        b a2 = a();
        if (a2 != null) {
            a2.onCancelled(this.c);
        }
    }

    /* access modifiers changed from: protected */
    public b a() {
        Fragment targetFragment = getTargetFragment();
        if (targetFragment != null) {
            if (targetFragment instanceof b) {
                return (b) targetFragment;
            }
        } else if (getActivity() instanceof b) {
            return (b) getActivity();
        }
        return null;
    }
}
