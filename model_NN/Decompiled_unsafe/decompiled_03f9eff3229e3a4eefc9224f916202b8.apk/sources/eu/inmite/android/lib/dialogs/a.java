package eu.inmite.android.lib.dialogs;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import eu.inmite.android.lib.dialogs.a;

/* compiled from: BaseDialogBuilder */
public abstract class a<T extends a<T>> {

    /* renamed from: a  reason: collision with root package name */
    public static String f2559a = "request_code";
    public static String b = "cancelable_oto";
    public static String c = "simple_dialog";
    public static int d = -42;
    protected final Context e;
    protected final FragmentManager f;
    protected final Class<? extends BaseDialogFragment> g;
    private Fragment h;
    private boolean i = true;
    private boolean j = true;
    private String k = c;
    private int l = d;

    /* access modifiers changed from: protected */
    public abstract Bundle b();

    /* access modifiers changed from: protected */
    public abstract T c();

    public a(Context context, FragmentManager fragmentManager, Class<? extends BaseDialogFragment> cls) {
        this.f = fragmentManager;
        this.e = context.getApplicationContext();
        this.g = cls;
    }

    public T a(boolean z) {
        this.i = z;
        return c();
    }

    public T f(int i2) {
        this.l = i2;
        return c();
    }

    public DialogFragment d() {
        Bundle b2 = b();
        BaseDialogFragment baseDialogFragment = (BaseDialogFragment) Fragment.instantiate(this.e, this.g.getName(), b2);
        b2.putBoolean(b, this.j);
        if (this.h != null) {
            baseDialogFragment.setTargetFragment(this.h, this.l);
        } else {
            b2.putInt(f2559a, this.l);
        }
        baseDialogFragment.setCancelable(this.i);
        FragmentTransaction beginTransaction = this.f.beginTransaction();
        beginTransaction.add(baseDialogFragment, this.k);
        beginTransaction.commitAllowingStateLoss();
        return baseDialogFragment;
    }
}
