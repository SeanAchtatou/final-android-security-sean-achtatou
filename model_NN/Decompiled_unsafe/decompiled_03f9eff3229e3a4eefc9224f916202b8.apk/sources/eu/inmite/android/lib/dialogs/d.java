package eu.inmite.android.lib.dialogs;

import android.view.View;

/* compiled from: SimpleDialogFragment */
class d implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SimpleDialogFragment f2560a;

    d(SimpleDialogFragment simpleDialogFragment) {
        this.f2560a = simpleDialogFragment;
    }

    public void onClick(View view) {
        c e = this.f2560a.e();
        if (e != null) {
            e.onPositiveButtonClicked(this.f2560a.e);
        }
        this.f2560a.dismiss();
    }
}
