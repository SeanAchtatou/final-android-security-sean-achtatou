package org.a;

import java.util.Map;
import org.a.a.f;
import org.a.a.i;
import org.a.c.a;

/* compiled from: MDC */
public class d {

    /* renamed from: a  reason: collision with root package name */
    static a f2569a;

    static {
        try {
            f2569a = org.a.b.d.f2567a.a();
        } catch (NoClassDefFoundError e) {
            f2569a = new f();
            String message = e.getMessage();
            if (message == null || message.indexOf("org/slf4j/impl/StaticMDCBinder") == -1) {
                throw e;
            }
            i.a("Failed to load class \"org.slf4j.impl.StaticMDCBinder\".");
            i.a("Defaulting to no-operation MDCAdapter implementation.");
            i.a("See http://www.slf4j.org/codes.html#no_static_mdc_binder for further details.");
        } catch (Exception e2) {
            i.a("MDC binding unsuccessful.", e2);
        }
    }

    public static void a(String str, String str2) throws IllegalArgumentException {
        if (str == null) {
            throw new IllegalArgumentException("key parameter cannot be null");
        } else if (f2569a == null) {
            throw new IllegalStateException("MDCAdapter cannot be null. See also http://www.slf4j.org/codes.html#null_MDCA");
        } else {
            f2569a.a(str, str2);
        }
    }

    public static String a(String str) throws IllegalArgumentException {
        if (str == null) {
            throw new IllegalArgumentException("key parameter cannot be null");
        } else if (f2569a != null) {
            return f2569a.a(str);
        } else {
            throw new IllegalStateException("MDCAdapter cannot be null. See also http://www.slf4j.org/codes.html#null_MDCA");
        }
    }

    public static void b(String str) throws IllegalArgumentException {
        if (str == null) {
            throw new IllegalArgumentException("key parameter cannot be null");
        } else if (f2569a == null) {
            throw new IllegalStateException("MDCAdapter cannot be null. See also http://www.slf4j.org/codes.html#null_MDCA");
        } else {
            f2569a.b(str);
        }
    }

    public static Map a() {
        if (f2569a != null) {
            return f2569a.a();
        }
        throw new IllegalStateException("MDCAdapter cannot be null. See also http://www.slf4j.org/codes.html#null_MDCA");
    }
}
