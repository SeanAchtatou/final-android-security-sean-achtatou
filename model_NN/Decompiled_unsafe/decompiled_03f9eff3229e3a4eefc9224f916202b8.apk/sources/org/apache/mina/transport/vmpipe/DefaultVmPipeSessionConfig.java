package org.apache.mina.transport.vmpipe;

import org.apache.mina.core.session.AbstractIoSessionConfig;
import org.apache.mina.core.session.IoSessionConfig;

class DefaultVmPipeSessionConfig extends AbstractIoSessionConfig implements VmPipeSessionConfig {
    DefaultVmPipeSessionConfig() {
    }

    /* access modifiers changed from: protected */
    public void doSetAll(IoSessionConfig ioSessionConfig) {
    }
}
