package org.apache.mina.filter.ssl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLSession;
import org.a.b;
import org.a.c;
import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.filterchain.IoFilter;
import org.apache.mina.core.filterchain.IoFilterAdapter;
import org.apache.mina.core.filterchain.IoFilterChain;
import org.apache.mina.core.future.DefaultWriteFuture;
import org.apache.mina.core.future.IoFuture;
import org.apache.mina.core.future.IoFutureListener;
import org.apache.mina.core.future.WriteFuture;
import org.apache.mina.core.service.IoAcceptor;
import org.apache.mina.core.session.AttributeKey;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.core.write.WriteRequest;
import org.apache.mina.core.write.WriteRequestWrapper;
import org.apache.mina.core.write.WriteToClosedSessionException;

public class SslFilter extends IoFilterAdapter {
    public static final AttributeKey DISABLE_ENCRYPTION_ONCE = new AttributeKey(SslFilter.class, "disableOnce");
    private static final b LOGGER = c.a(SslFilter.class);
    private static final AttributeKey NEXT_FILTER = new AttributeKey(SslFilter.class, "nextFilter");
    public static final AttributeKey PEER_ADDRESS = new AttributeKey(SslFilter.class, "peerAddress");
    public static final SslFilterMessage SESSION_SECURED = new SslFilterMessage("SESSION_SECURED");
    public static final SslFilterMessage SESSION_UNSECURED = new SslFilterMessage("SESSION_UNSECURED");
    private static final AttributeKey SSL_HANDLER = new AttributeKey(SslFilter.class, "handler");
    public static final AttributeKey SSL_SESSION = new AttributeKey(SslFilter.class, "session");
    private static final boolean START_HANDSHAKE = true;
    public static final AttributeKey USE_NOTIFICATION = new AttributeKey(SslFilter.class, "useNotification");
    private final boolean autoStart;
    private boolean client;
    private String[] enabledCipherSuites;
    private String[] enabledProtocols;
    private boolean needClientAuth;
    final SSLContext sslContext;
    private boolean wantClientAuth;

    public SslFilter(SSLContext sSLContext) {
        this(sSLContext, true);
    }

    public SslFilter(SSLContext sSLContext, boolean z) {
        if (sSLContext == null) {
            throw new IllegalArgumentException("sslContext");
        }
        this.sslContext = sSLContext;
        this.autoStart = z;
    }

    public SSLSession getSslSession(IoSession ioSession) {
        return (SSLSession) ioSession.getAttribute(SSL_SESSION);
    }

    public boolean startSsl(IoSession ioSession) throws SSLException {
        boolean z;
        SslHandler sslSessionHandler = getSslSessionHandler(ioSession);
        synchronized (sslSessionHandler) {
            if (sslSessionHandler.isOutboundDone()) {
                sslSessionHandler.destroy();
                sslSessionHandler.init();
                sslSessionHandler.handshake((IoFilter.NextFilter) ioSession.getAttribute(NEXT_FILTER));
                z = true;
            } else {
                z = false;
            }
        }
        sslSessionHandler.flushScheduledEvents();
        return z;
    }

    /* access modifiers changed from: package-private */
    public String getSessionInfo(IoSession ioSession) {
        StringBuilder sb = new StringBuilder();
        if (ioSession.getService() instanceof IoAcceptor) {
            sb.append("Session Server");
        } else {
            sb.append("Session Client");
        }
        sb.append('[').append(ioSession.getId()).append(']');
        SslHandler sslHandler = (SslHandler) ioSession.getAttribute(SSL_HANDLER);
        if (sslHandler == null) {
            sb.append("(no sslEngine)");
        } else if (isSslStarted(ioSession)) {
            if (sslHandler.isHandshakeComplete()) {
                sb.append("(SSL)");
            } else {
                sb.append("(ssl...)");
            }
        }
        return sb.toString();
    }

    public boolean isSslStarted(IoSession ioSession) {
        boolean z = false;
        SslHandler sslHandler = (SslHandler) ioSession.getAttribute(SSL_HANDLER);
        if (sslHandler == null) {
            return false;
        }
        synchronized (sslHandler) {
            if (!sslHandler.isOutboundDone()) {
                z = true;
            }
        }
        return z;
    }

    public WriteFuture stopSsl(IoSession ioSession) throws SSLException {
        WriteFuture initiateClosure;
        SslHandler sslSessionHandler = getSslSessionHandler(ioSession);
        IoFilter.NextFilter nextFilter = (IoFilter.NextFilter) ioSession.getAttribute(NEXT_FILTER);
        synchronized (sslSessionHandler) {
            initiateClosure = initiateClosure(nextFilter, ioSession);
        }
        sslSessionHandler.flushScheduledEvents();
        return initiateClosure;
    }

    public boolean isUseClientMode() {
        return this.client;
    }

    public void setUseClientMode(boolean z) {
        this.client = z;
    }

    public boolean isNeedClientAuth() {
        return this.needClientAuth;
    }

    public void setNeedClientAuth(boolean z) {
        this.needClientAuth = z;
    }

    public boolean isWantClientAuth() {
        return this.wantClientAuth;
    }

    public void setWantClientAuth(boolean z) {
        this.wantClientAuth = z;
    }

    public String[] getEnabledCipherSuites() {
        return this.enabledCipherSuites;
    }

    public void setEnabledCipherSuites(String[] strArr) {
        this.enabledCipherSuites = strArr;
    }

    public String[] getEnabledProtocols() {
        return this.enabledProtocols;
    }

    public void setEnabledProtocols(String[] strArr) {
        this.enabledProtocols = strArr;
    }

    public void onPreAdd(IoFilterChain ioFilterChain, String str, IoFilter.NextFilter nextFilter) throws SSLException {
        if (ioFilterChain.contains(SslFilter.class)) {
            LOGGER.e("Only one SSL filter is permitted in a chain.");
            throw new IllegalStateException("Only one SSL filter is permitted in a chain.");
        }
        LOGGER.b("Adding the SSL Filter {} to the chain", str);
        IoSession session = ioFilterChain.getSession();
        session.setAttribute(NEXT_FILTER, nextFilter);
        SslHandler sslHandler = new SslHandler(this, session);
        sslHandler.init();
        session.setAttribute(SSL_HANDLER, sslHandler);
    }

    public void onPostAdd(IoFilterChain ioFilterChain, String str, IoFilter.NextFilter nextFilter) throws SSLException {
        if (this.autoStart) {
            initiateHandshake(nextFilter, ioFilterChain.getSession());
        }
    }

    public void onPreRemove(IoFilterChain ioFilterChain, String str, IoFilter.NextFilter nextFilter) throws SSLException {
        IoSession session = ioFilterChain.getSession();
        stopSsl(session);
        session.removeAttribute(NEXT_FILTER);
        session.removeAttribute(SSL_HANDLER);
    }

    public void sessionClosed(IoFilter.NextFilter nextFilter, IoSession ioSession) throws SSLException {
        SslHandler sslSessionHandler = getSslSessionHandler(ioSession);
        try {
            synchronized (sslSessionHandler) {
                sslSessionHandler.destroy();
            }
            sslSessionHandler.flushScheduledEvents();
        } finally {
            nextFilter.sessionClosed(ioSession);
        }
    }

    public void messageReceived(IoFilter.NextFilter nextFilter, IoSession ioSession, Object obj) throws SSLException {
        SSLException sSLException;
        if (LOGGER.a()) {
            LOGGER.a("{}: Message received : {}", getSessionInfo(ioSession), obj);
        }
        SslHandler sslSessionHandler = getSslSessionHandler(ioSession);
        synchronized (sslSessionHandler) {
            if (isSslStarted(ioSession) || !sslSessionHandler.isInboundDone()) {
                IoBuffer ioBuffer = (IoBuffer) obj;
                try {
                    sslSessionHandler.messageReceived(nextFilter, ioBuffer.buf());
                    handleSslData(nextFilter, sslSessionHandler);
                    if (sslSessionHandler.isInboundDone()) {
                        if (sslSessionHandler.isOutboundDone()) {
                            sslSessionHandler.destroy();
                        } else {
                            initiateClosure(nextFilter, ioSession);
                        }
                        if (ioBuffer.hasRemaining()) {
                            sslSessionHandler.scheduleMessageReceived(nextFilter, ioBuffer);
                        }
                    }
                } catch (SSLException e) {
                    if (!sslSessionHandler.isHandshakeComplete()) {
                        sSLException = new SSLHandshakeException("SSL handshake failed.");
                        sSLException.initCause(e);
                    } else {
                        sSLException = e;
                    }
                    throw sSLException;
                }
            } else {
                sslSessionHandler.scheduleMessageReceived(nextFilter, obj);
            }
        }
        sslSessionHandler.flushScheduledEvents();
    }

    public void messageSent(IoFilter.NextFilter nextFilter, IoSession ioSession, WriteRequest writeRequest) {
        if (writeRequest instanceof EncryptedWriteRequest) {
            nextFilter.messageSent(ioSession, ((EncryptedWriteRequest) writeRequest).getParentRequest());
        }
    }

    public void exceptionCaught(IoFilter.NextFilter nextFilter, IoSession ioSession, Throwable th) throws Exception {
        boolean z;
        if (th instanceof WriteToClosedSessionException) {
            List<WriteRequest> requests = ((WriteToClosedSessionException) th).getRequests();
            Iterator<WriteRequest> it = requests.iterator();
            while (true) {
                if (it.hasNext()) {
                    if (isCloseNotify(it.next().getMessage())) {
                        z = true;
                        break;
                    }
                } else {
                    z = false;
                    break;
                }
            }
            if (z) {
                if (requests.size() != 1) {
                    ArrayList arrayList = new ArrayList(requests.size() - 1);
                    for (WriteRequest next : requests) {
                        if (!isCloseNotify(next.getMessage())) {
                            arrayList.add(next);
                        }
                    }
                    if (!arrayList.isEmpty()) {
                        th = new WriteToClosedSessionException(arrayList, th.getMessage(), th.getCause());
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            }
        }
        nextFilter.exceptionCaught(ioSession, th);
    }

    private boolean isCloseNotify(Object obj) {
        boolean z = true;
        if (!(obj instanceof IoBuffer)) {
            return false;
        }
        IoBuffer ioBuffer = (IoBuffer) obj;
        int position = ioBuffer.position();
        if (!(ioBuffer.get(position + 0) == 21 && ioBuffer.get(position + 1) == 3 && ((ioBuffer.get(position + 2) == 0 || ioBuffer.get(position + 2) == 1 || ioBuffer.get(position + 2) == 2 || ioBuffer.get(position + 2) == 3) && ioBuffer.get(position + 3) == 0))) {
            z = false;
        }
        return z;
    }

    public void filterWrite(IoFilter.NextFilter nextFilter, IoSession ioSession, WriteRequest writeRequest) throws SSLException {
        boolean z;
        if (LOGGER.a()) {
            LOGGER.a("{}: Writing Message : {}", getSessionInfo(ioSession), writeRequest);
        }
        SslHandler sslSessionHandler = getSslSessionHandler(ioSession);
        synchronized (sslSessionHandler) {
            if (!isSslStarted(ioSession)) {
                sslSessionHandler.scheduleFilterWrite(nextFilter, writeRequest);
                z = true;
            } else if (ioSession.containsAttribute(DISABLE_ENCRYPTION_ONCE)) {
                ioSession.removeAttribute(DISABLE_ENCRYPTION_ONCE);
                sslSessionHandler.scheduleFilterWrite(nextFilter, writeRequest);
                z = true;
            } else {
                IoBuffer ioBuffer = (IoBuffer) writeRequest.getMessage();
                if (sslSessionHandler.isWritingEncryptedData()) {
                    sslSessionHandler.scheduleFilterWrite(nextFilter, writeRequest);
                    z = true;
                } else if (sslSessionHandler.isHandshakeComplete()) {
                    int position = ioBuffer.position();
                    sslSessionHandler.encrypt(ioBuffer.buf());
                    ioBuffer.position(position);
                    sslSessionHandler.scheduleFilterWrite(nextFilter, new EncryptedWriteRequest(writeRequest, sslSessionHandler.fetchOutNetBuffer()));
                    z = true;
                } else {
                    if (ioSession.isConnected()) {
                        sslSessionHandler.schedulePreHandshakeWriteRequest(nextFilter, writeRequest);
                    }
                    z = false;
                }
            }
        }
        if (z) {
            sslSessionHandler.flushScheduledEvents();
        }
    }

    public void filterClose(final IoFilter.NextFilter nextFilter, final IoSession ioSession) throws SSLException {
        SslHandler sslHandler = (SslHandler) ioSession.getAttribute(SSL_HANDLER);
        if (sslHandler == null) {
            nextFilter.filterClose(ioSession);
            return;
        }
        WriteFuture writeFuture = null;
        try {
            synchronized (sslHandler) {
                if (isSslStarted(ioSession)) {
                    writeFuture = initiateClosure(nextFilter, ioSession);
                    writeFuture.addListener((IoFutureListener<?>) new IoFutureListener<IoFuture>() {
                        public void operationComplete(IoFuture ioFuture) {
                            nextFilter.filterClose(ioSession);
                        }
                    });
                }
            }
            sslHandler.flushScheduledEvents();
        } finally {
            if (writeFuture == null) {
                nextFilter.filterClose(ioSession);
            }
        }
    }

    private void initiateHandshake(IoFilter.NextFilter nextFilter, IoSession ioSession) throws SSLException {
        LOGGER.b("{} : Starting the first handshake", getSessionInfo(ioSession));
        SslHandler sslSessionHandler = getSslSessionHandler(ioSession);
        synchronized (sslSessionHandler) {
            sslSessionHandler.handshake(nextFilter);
        }
        sslSessionHandler.flushScheduledEvents();
    }

    private WriteFuture initiateClosure(IoFilter.NextFilter nextFilter, IoSession ioSession) throws SSLException {
        SslHandler sslSessionHandler = getSslSessionHandler(ioSession);
        if (!sslSessionHandler.closeOutbound()) {
            return DefaultWriteFuture.newNotWrittenFuture(ioSession, new IllegalStateException("SSL session is shut down already."));
        }
        WriteFuture writeNetBuffer = sslSessionHandler.writeNetBuffer(nextFilter);
        if (writeNetBuffer == null) {
            writeNetBuffer = DefaultWriteFuture.newWrittenFuture(ioSession);
        }
        if (sslSessionHandler.isInboundDone()) {
            sslSessionHandler.destroy();
        }
        if (!ioSession.containsAttribute(USE_NOTIFICATION)) {
            return writeNetBuffer;
        }
        sslSessionHandler.scheduleMessageReceived(nextFilter, SESSION_UNSECURED);
        return writeNetBuffer;
    }

    private void handleSslData(IoFilter.NextFilter nextFilter, SslHandler sslHandler) throws SSLException {
        if (LOGGER.a()) {
            LOGGER.b("{}: Processing the SSL Data ", getSessionInfo(sslHandler.getSession()));
        }
        if (sslHandler.isHandshakeComplete()) {
            sslHandler.flushPreHandshakeEvents();
        }
        sslHandler.writeNetBuffer(nextFilter);
        handleAppDataRead(nextFilter, sslHandler);
    }

    private void handleAppDataRead(IoFilter.NextFilter nextFilter, SslHandler sslHandler) {
        IoBuffer fetchAppBuffer = sslHandler.fetchAppBuffer();
        if (fetchAppBuffer.hasRemaining()) {
            sslHandler.scheduleMessageReceived(nextFilter, fetchAppBuffer);
        }
    }

    private SslHandler getSslSessionHandler(IoSession ioSession) {
        SslHandler sslHandler = (SslHandler) ioSession.getAttribute(SSL_HANDLER);
        if (sslHandler == null) {
            throw new IllegalStateException();
        } else if (sslHandler.getSslFilter() == this) {
            return sslHandler;
        } else {
            throw new IllegalArgumentException("Not managed by this filter.");
        }
    }

    public static class SslFilterMessage {
        private final String name;

        private SslFilterMessage(String str) {
            this.name = str;
        }

        public String toString() {
            return this.name;
        }
    }

    private static class EncryptedWriteRequest extends WriteRequestWrapper {
        private final IoBuffer encryptedMessage;

        private EncryptedWriteRequest(WriteRequest writeRequest, IoBuffer ioBuffer) {
            super(writeRequest);
            this.encryptedMessage = ioBuffer;
        }

        public Object getMessage() {
            return this.encryptedMessage;
        }
    }
}
