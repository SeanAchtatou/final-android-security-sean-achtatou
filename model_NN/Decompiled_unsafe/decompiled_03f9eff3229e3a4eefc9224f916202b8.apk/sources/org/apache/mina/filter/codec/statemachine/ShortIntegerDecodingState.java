package org.apache.mina.filter.codec.statemachine;

import org.apache.mina.filter.codec.ProtocolDecoderException;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;

public abstract class ShortIntegerDecodingState implements DecodingState {
    private int counter;
    private int highByte;

    /* access modifiers changed from: protected */
    public abstract DecodingState finishDecode(short s, ProtocolDecoderOutput protocolDecoderOutput) throws Exception;

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:2:0x0006  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public org.apache.mina.filter.codec.statemachine.DecodingState decode(org.apache.mina.core.buffer.IoBuffer r3, org.apache.mina.filter.codec.ProtocolDecoderOutput r4) throws java.lang.Exception {
        /*
            r2 = this;
        L_0x0000:
            boolean r0 = r3.hasRemaining()
            if (r0 == 0) goto L_0x002f
            int r0 = r2.counter
            switch(r0) {
                case 0: goto L_0x0011;
                case 1: goto L_0x001e;
                default: goto L_0x000b;
            }
        L_0x000b:
            java.lang.InternalError r0 = new java.lang.InternalError
            r0.<init>()
            throw r0
        L_0x0011:
            short r0 = r3.getUnsigned()
            r2.highByte = r0
            int r0 = r2.counter
            int r0 = r0 + 1
            r2.counter = r0
            goto L_0x0000
        L_0x001e:
            r0 = 0
            r2.counter = r0
            int r0 = r2.highByte
            int r0 = r0 << 8
            short r1 = r3.getUnsigned()
            r0 = r0 | r1
            short r0 = (short) r0
            org.apache.mina.filter.codec.statemachine.DecodingState r2 = r2.finishDecode(r0, r4)
        L_0x002f:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.mina.filter.codec.statemachine.ShortIntegerDecodingState.decode(org.apache.mina.core.buffer.IoBuffer, org.apache.mina.filter.codec.ProtocolDecoderOutput):org.apache.mina.filter.codec.statemachine.DecodingState");
    }

    public DecodingState finishDecode(ProtocolDecoderOutput protocolDecoderOutput) throws Exception {
        throw new ProtocolDecoderException("Unexpected end of session while waiting for a short integer.");
    }
}
