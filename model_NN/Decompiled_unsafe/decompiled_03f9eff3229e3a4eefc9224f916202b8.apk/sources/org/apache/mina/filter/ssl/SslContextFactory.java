package org.apache.mina.filter.ssl;

import java.security.KeyStore;
import java.security.SecureRandom;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.ManagerFactoryParameters;
import javax.net.ssl.TrustManagerFactory;

public class SslContextFactory {
    private int clientSessionCacheSize = -1;
    private int clientSessionTimeout = -1;
    private KeyManagerFactory keyManagerFactory = null;
    private String keyManagerFactoryAlgorithm = null;
    private boolean keyManagerFactoryAlgorithmUseDefault = true;
    private KeyStore keyManagerFactoryKeyStore = null;
    private char[] keyManagerFactoryKeyStorePassword = null;
    private String keyManagerFactoryProvider = null;
    private String protocol = "TLS";
    private String provider = null;
    private SecureRandom secureRandom = null;
    private int serverSessionCacheSize = -1;
    private int serverSessionTimeout = -1;
    private TrustManagerFactory trustManagerFactory = null;
    private String trustManagerFactoryAlgorithm = null;
    private boolean trustManagerFactoryAlgorithmUseDefault = true;
    private KeyStore trustManagerFactoryKeyStore = null;
    private ManagerFactoryParameters trustManagerFactoryParameters = null;
    private String trustManagerFactoryProvider = null;

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0020  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0047  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0058  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0067  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0074  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0081  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x008e  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x00af  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x00b8  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public javax.net.ssl.SSLContext newInstance() throws java.lang.Exception {
        /*
            r5 = this;
            r0 = 0
            javax.net.ssl.KeyManagerFactory r2 = r5.keyManagerFactory
            javax.net.ssl.TrustManagerFactory r1 = r5.trustManagerFactory
            if (r2 != 0) goto L_0x00bd
            java.lang.String r3 = r5.keyManagerFactoryAlgorithm
            if (r3 != 0) goto L_0x0013
            boolean r4 = r5.keyManagerFactoryAlgorithmUseDefault
            if (r4 == 0) goto L_0x0013
            java.lang.String r3 = javax.net.ssl.KeyManagerFactory.getDefaultAlgorithm()
        L_0x0013:
            if (r3 == 0) goto L_0x00bd
            java.lang.String r2 = r5.keyManagerFactoryProvider
            if (r2 != 0) goto L_0x0098
            javax.net.ssl.KeyManagerFactory r2 = javax.net.ssl.KeyManagerFactory.getInstance(r3)
            r4 = r2
        L_0x001e:
            if (r1 != 0) goto L_0x00ba
            java.lang.String r2 = r5.trustManagerFactoryAlgorithm
            if (r2 != 0) goto L_0x002c
            boolean r3 = r5.trustManagerFactoryAlgorithmUseDefault
            if (r3 == 0) goto L_0x002c
            java.lang.String r2 = javax.net.ssl.TrustManagerFactory.getDefaultAlgorithm()
        L_0x002c:
            if (r2 == 0) goto L_0x00ba
            java.lang.String r1 = r5.trustManagerFactoryProvider
            if (r1 != 0) goto L_0x00a1
            javax.net.ssl.TrustManagerFactory r1 = javax.net.ssl.TrustManagerFactory.getInstance(r2)
            r3 = r1
        L_0x0037:
            if (r4 == 0) goto L_0x00b8
            java.security.KeyStore r1 = r5.keyManagerFactoryKeyStore
            char[] r2 = r5.keyManagerFactoryKeyStorePassword
            r4.init(r1, r2)
            javax.net.ssl.KeyManager[] r1 = r4.getKeyManagers()
            r2 = r1
        L_0x0045:
            if (r3 == 0) goto L_0x0054
            javax.net.ssl.ManagerFactoryParameters r0 = r5.trustManagerFactoryParameters
            if (r0 == 0) goto L_0x00a9
            javax.net.ssl.ManagerFactoryParameters r0 = r5.trustManagerFactoryParameters
            r3.init(r0)
        L_0x0050:
            javax.net.ssl.TrustManager[] r0 = r3.getTrustManagers()
        L_0x0054:
            java.lang.String r1 = r5.provider
            if (r1 != 0) goto L_0x00af
            java.lang.String r1 = r5.protocol
            javax.net.ssl.SSLContext r1 = javax.net.ssl.SSLContext.getInstance(r1)
        L_0x005e:
            java.security.SecureRandom r3 = r5.secureRandom
            r1.init(r2, r0, r3)
            int r0 = r5.clientSessionCacheSize
            if (r0 < 0) goto L_0x0070
            javax.net.ssl.SSLSessionContext r0 = r1.getClientSessionContext()
            int r2 = r5.clientSessionCacheSize
            r0.setSessionCacheSize(r2)
        L_0x0070:
            int r0 = r5.clientSessionTimeout
            if (r0 < 0) goto L_0x007d
            javax.net.ssl.SSLSessionContext r0 = r1.getClientSessionContext()
            int r2 = r5.clientSessionTimeout
            r0.setSessionTimeout(r2)
        L_0x007d:
            int r0 = r5.serverSessionCacheSize
            if (r0 < 0) goto L_0x008a
            javax.net.ssl.SSLSessionContext r0 = r1.getServerSessionContext()
            int r2 = r5.serverSessionCacheSize
            r0.setSessionCacheSize(r2)
        L_0x008a:
            int r0 = r5.serverSessionTimeout
            if (r0 < 0) goto L_0x0097
            javax.net.ssl.SSLSessionContext r0 = r1.getServerSessionContext()
            int r2 = r5.serverSessionTimeout
            r0.setSessionTimeout(r2)
        L_0x0097:
            return r1
        L_0x0098:
            java.lang.String r2 = r5.keyManagerFactoryProvider
            javax.net.ssl.KeyManagerFactory r2 = javax.net.ssl.KeyManagerFactory.getInstance(r3, r2)
            r4 = r2
            goto L_0x001e
        L_0x00a1:
            java.lang.String r1 = r5.trustManagerFactoryProvider
            javax.net.ssl.TrustManagerFactory r1 = javax.net.ssl.TrustManagerFactory.getInstance(r2, r1)
            r3 = r1
            goto L_0x0037
        L_0x00a9:
            java.security.KeyStore r0 = r5.trustManagerFactoryKeyStore
            r3.init(r0)
            goto L_0x0050
        L_0x00af:
            java.lang.String r1 = r5.protocol
            java.lang.String r3 = r5.provider
            javax.net.ssl.SSLContext r1 = javax.net.ssl.SSLContext.getInstance(r1, r3)
            goto L_0x005e
        L_0x00b8:
            r2 = r0
            goto L_0x0045
        L_0x00ba:
            r3 = r1
            goto L_0x0037
        L_0x00bd:
            r4 = r2
            goto L_0x001e
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.mina.filter.ssl.SslContextFactory.newInstance():javax.net.ssl.SSLContext");
    }

    public void setProvider(String str) {
        this.provider = str;
    }

    public void setProtocol(String str) {
        if (str == null) {
            throw new IllegalArgumentException("protocol");
        }
        this.protocol = str;
    }

    public void setKeyManagerFactoryAlgorithmUseDefault(boolean z) {
        this.keyManagerFactoryAlgorithmUseDefault = z;
    }

    public void setTrustManagerFactoryAlgorithmUseDefault(boolean z) {
        this.trustManagerFactoryAlgorithmUseDefault = z;
    }

    public void setKeyManagerFactory(KeyManagerFactory keyManagerFactory2) {
        this.keyManagerFactory = keyManagerFactory2;
    }

    public void setKeyManagerFactoryAlgorithm(String str) {
        this.keyManagerFactoryAlgorithm = str;
    }

    public void setKeyManagerFactoryProvider(String str) {
        this.keyManagerFactoryProvider = str;
    }

    public void setKeyManagerFactoryKeyStore(KeyStore keyStore) {
        this.keyManagerFactoryKeyStore = keyStore;
    }

    public void setKeyManagerFactoryKeyStorePassword(String str) {
        if (str != null) {
            this.keyManagerFactoryKeyStorePassword = str.toCharArray();
        } else {
            this.keyManagerFactoryKeyStorePassword = null;
        }
    }

    public void setTrustManagerFactory(TrustManagerFactory trustManagerFactory2) {
        this.trustManagerFactory = trustManagerFactory2;
    }

    public void setTrustManagerFactoryAlgorithm(String str) {
        this.trustManagerFactoryAlgorithm = str;
    }

    public void setTrustManagerFactoryKeyStore(KeyStore keyStore) {
        this.trustManagerFactoryKeyStore = keyStore;
    }

    public void setTrustManagerFactoryParameters(ManagerFactoryParameters managerFactoryParameters) {
        this.trustManagerFactoryParameters = managerFactoryParameters;
    }

    public void setTrustManagerFactoryProvider(String str) {
        this.trustManagerFactoryProvider = str;
    }

    public void setSecureRandom(SecureRandom secureRandom2) {
        this.secureRandom = secureRandom2;
    }

    public void setClientSessionCacheSize(int i) {
        this.clientSessionCacheSize = i;
    }

    public void setClientSessionTimeout(int i) {
        this.clientSessionTimeout = i;
    }

    public void setServerSessionCacheSize(int i) {
        this.serverSessionCacheSize = i;
    }

    public void setServerSessionTimeout(int i) {
        this.serverSessionTimeout = i;
    }
}
