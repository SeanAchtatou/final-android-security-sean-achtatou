package org.apache.mina.filter.executor;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import org.apache.mina.core.session.IoEvent;

public class UnorderedThreadPoolExecutor extends ThreadPoolExecutor {
    /* access modifiers changed from: private */
    public static final Runnable EXIT_SIGNAL = new Runnable() {
        public void run() {
            throw new Error("This method shouldn't be called. Please file a bug report.");
        }
    };
    private long completedTaskCount;
    /* access modifiers changed from: private */
    public volatile int corePoolSize;
    /* access modifiers changed from: private */
    public final AtomicInteger idleWorkers;
    private volatile int largestPoolSize;
    private volatile int maximumPoolSize;
    /* access modifiers changed from: private */
    public final IoEventQueueHandler queueHandler;
    private volatile boolean shutdown;
    /* access modifiers changed from: private */
    public final Set<Worker> workers;

    static /* synthetic */ long access$714(UnorderedThreadPoolExecutor unorderedThreadPoolExecutor, long j) {
        long j2 = unorderedThreadPoolExecutor.completedTaskCount + j;
        unorderedThreadPoolExecutor.completedTaskCount = j2;
        return j2;
    }

    public UnorderedThreadPoolExecutor() {
        this(16);
    }

    public UnorderedThreadPoolExecutor(int i) {
        this(0, i);
    }

    public UnorderedThreadPoolExecutor(int i, int i2) {
        this(i, i2, 30, TimeUnit.SECONDS);
    }

    public UnorderedThreadPoolExecutor(int i, int i2, long j, TimeUnit timeUnit) {
        this(i, i2, j, timeUnit, Executors.defaultThreadFactory());
    }

    public UnorderedThreadPoolExecutor(int i, int i2, long j, TimeUnit timeUnit, IoEventQueueHandler ioEventQueueHandler) {
        this(i, i2, j, timeUnit, Executors.defaultThreadFactory(), ioEventQueueHandler);
    }

    public UnorderedThreadPoolExecutor(int i, int i2, long j, TimeUnit timeUnit, ThreadFactory threadFactory) {
        this(i, i2, j, timeUnit, threadFactory, null);
    }

    public UnorderedThreadPoolExecutor(int i, int i2, long j, TimeUnit timeUnit, ThreadFactory threadFactory, IoEventQueueHandler ioEventQueueHandler) {
        super(0, 1, j, timeUnit, new LinkedBlockingQueue(), threadFactory, new ThreadPoolExecutor.AbortPolicy());
        this.workers = new HashSet();
        this.idleWorkers = new AtomicInteger();
        if (i < 0) {
            throw new IllegalArgumentException("corePoolSize: " + i);
        } else if (i2 == 0 || i2 < i) {
            throw new IllegalArgumentException("maximumPoolSize: " + i2);
        } else {
            ioEventQueueHandler = ioEventQueueHandler == null ? IoEventQueueHandler.NOOP : ioEventQueueHandler;
            this.corePoolSize = i;
            this.maximumPoolSize = i2;
            this.queueHandler = ioEventQueueHandler;
        }
    }

    public IoEventQueueHandler getQueueHandler() {
        return this.queueHandler;
    }

    public void setRejectedExecutionHandler(RejectedExecutionHandler rejectedExecutionHandler) {
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void addWorker() {
        /*
            r4 = this;
            java.util.Set<org.apache.mina.filter.executor.UnorderedThreadPoolExecutor$Worker> r1 = r4.workers
            monitor-enter(r1)
            java.util.Set<org.apache.mina.filter.executor.UnorderedThreadPoolExecutor$Worker> r0 = r4.workers     // Catch:{ all -> 0x003e }
            int r0 = r0.size()     // Catch:{ all -> 0x003e }
            int r2 = r4.maximumPoolSize     // Catch:{ all -> 0x003e }
            if (r0 < r2) goto L_0x000f
            monitor-exit(r1)     // Catch:{ all -> 0x003e }
        L_0x000e:
            return
        L_0x000f:
            org.apache.mina.filter.executor.UnorderedThreadPoolExecutor$Worker r0 = new org.apache.mina.filter.executor.UnorderedThreadPoolExecutor$Worker     // Catch:{ all -> 0x003e }
            r2 = 0
            r0.<init>()     // Catch:{ all -> 0x003e }
            java.util.concurrent.ThreadFactory r2 = r4.getThreadFactory()     // Catch:{ all -> 0x003e }
            java.lang.Thread r2 = r2.newThread(r0)     // Catch:{ all -> 0x003e }
            java.util.concurrent.atomic.AtomicInteger r3 = r4.idleWorkers     // Catch:{ all -> 0x003e }
            r3.incrementAndGet()     // Catch:{ all -> 0x003e }
            r2.start()     // Catch:{ all -> 0x003e }
            java.util.Set<org.apache.mina.filter.executor.UnorderedThreadPoolExecutor$Worker> r2 = r4.workers     // Catch:{ all -> 0x003e }
            r2.add(r0)     // Catch:{ all -> 0x003e }
            java.util.Set<org.apache.mina.filter.executor.UnorderedThreadPoolExecutor$Worker> r0 = r4.workers     // Catch:{ all -> 0x003e }
            int r0 = r0.size()     // Catch:{ all -> 0x003e }
            int r2 = r4.largestPoolSize     // Catch:{ all -> 0x003e }
            if (r0 <= r2) goto L_0x003c
            java.util.Set<org.apache.mina.filter.executor.UnorderedThreadPoolExecutor$Worker> r0 = r4.workers     // Catch:{ all -> 0x003e }
            int r0 = r0.size()     // Catch:{ all -> 0x003e }
            r4.largestPoolSize = r0     // Catch:{ all -> 0x003e }
        L_0x003c:
            monitor-exit(r1)     // Catch:{ all -> 0x003e }
            goto L_0x000e
        L_0x003e:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x003e }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.mina.filter.executor.UnorderedThreadPoolExecutor.addWorker():void");
    }

    private void addWorkerIfNecessary() {
        if (this.idleWorkers.get() == 0) {
            synchronized (this.workers) {
                if (this.workers.isEmpty() || this.idleWorkers.get() == 0) {
                    addWorker();
                }
            }
        }
    }

    private void removeWorker() {
        synchronized (this.workers) {
            if (this.workers.size() > this.corePoolSize) {
                getQueue().offer(EXIT_SIGNAL);
            }
        }
    }

    public int getMaximumPoolSize() {
        return this.maximumPoolSize;
    }

    public void setMaximumPoolSize(int i) {
        if (i <= 0 || i < this.corePoolSize) {
            throw new IllegalArgumentException("maximumPoolSize: " + i);
        }
        synchronized (this.workers) {
            this.maximumPoolSize = i;
            for (int size = this.workers.size() - i; size > 0; size--) {
                removeWorker();
            }
        }
    }

    public boolean awaitTermination(long j, TimeUnit timeUnit) throws InterruptedException {
        long currentTimeMillis = System.currentTimeMillis() + timeUnit.toMillis(j);
        synchronized (this.workers) {
            while (!isTerminated()) {
                long currentTimeMillis2 = currentTimeMillis - System.currentTimeMillis();
                if (currentTimeMillis2 <= 0) {
                    break;
                }
                this.workers.wait(currentTimeMillis2);
            }
        }
        return isTerminated();
    }

    public boolean isShutdown() {
        return this.shutdown;
    }

    public boolean isTerminated() {
        boolean isEmpty;
        if (!this.shutdown) {
            return false;
        }
        synchronized (this.workers) {
            isEmpty = this.workers.isEmpty();
        }
        return isEmpty;
    }

    public void shutdown() {
        if (!this.shutdown) {
            this.shutdown = true;
            synchronized (this.workers) {
                for (int size = this.workers.size(); size > 0; size--) {
                    getQueue().offer(EXIT_SIGNAL);
                }
            }
        }
    }

    public List<Runnable> shutdownNow() {
        shutdown();
        ArrayList arrayList = new ArrayList();
        while (true) {
            Runnable poll = getQueue().poll();
            if (poll == null) {
                return arrayList;
            }
            if (poll == EXIT_SIGNAL) {
                getQueue().offer(EXIT_SIGNAL);
                Thread.yield();
            } else {
                getQueueHandler().polled(this, (IoEvent) poll);
                arrayList.add(poll);
            }
        }
    }

    public void execute(Runnable runnable) {
        if (this.shutdown) {
            rejectTask(runnable);
        }
        checkTaskType(runnable);
        IoEvent ioEvent = (IoEvent) runnable;
        boolean accept = this.queueHandler.accept(this, ioEvent);
        if (accept) {
            getQueue().offer(ioEvent);
        }
        addWorkerIfNecessary();
        if (accept) {
            this.queueHandler.offered(this, ioEvent);
        }
    }

    private void rejectTask(Runnable runnable) {
        getRejectedExecutionHandler().rejectedExecution(runnable, this);
    }

    private void checkTaskType(Runnable runnable) {
        if (!(runnable instanceof IoEvent)) {
            throw new IllegalArgumentException("task must be an IoEvent or its subclass.");
        }
    }

    public int getActiveCount() {
        int size;
        synchronized (this.workers) {
            size = this.workers.size() - this.idleWorkers.get();
        }
        return size;
    }

    public long getCompletedTaskCount() {
        long j;
        synchronized (this.workers) {
            long j2 = this.completedTaskCount;
            j = j2;
            for (Worker access$100 : this.workers) {
                j = access$100.completedTaskCount + j;
            }
        }
        return j;
    }

    public int getLargestPoolSize() {
        return this.largestPoolSize;
    }

    public int getPoolSize() {
        int size;
        synchronized (this.workers) {
            size = this.workers.size();
        }
        return size;
    }

    public long getTaskCount() {
        return getCompletedTaskCount();
    }

    public boolean isTerminating() {
        boolean z;
        synchronized (this.workers) {
            z = isShutdown() && !isTerminated();
        }
        return z;
    }

    public int prestartAllCoreThreads() {
        int i = 0;
        synchronized (this.workers) {
            for (int size = this.corePoolSize - this.workers.size(); size > 0; size--) {
                addWorker();
                i++;
            }
        }
        return i;
    }

    public boolean prestartCoreThread() {
        boolean z;
        synchronized (this.workers) {
            if (this.workers.size() < this.corePoolSize) {
                addWorker();
                z = true;
            } else {
                z = false;
            }
        }
        return z;
    }

    public void purge() {
    }

    public boolean remove(Runnable runnable) {
        boolean remove = super.remove(runnable);
        if (remove) {
            getQueueHandler().polled(this, (IoEvent) runnable);
        }
        return remove;
    }

    public int getCorePoolSize() {
        return this.corePoolSize;
    }

    public void setCorePoolSize(int i) {
        if (i < 0) {
            throw new IllegalArgumentException("corePoolSize: " + i);
        } else if (i > this.maximumPoolSize) {
            throw new IllegalArgumentException("corePoolSize exceeds maximumPoolSize");
        } else {
            synchronized (this.workers) {
                if (this.corePoolSize > i) {
                    for (int i2 = this.corePoolSize - i; i2 > 0; i2--) {
                        removeWorker();
                    }
                }
                this.corePoolSize = i;
            }
        }
    }

    private class Worker implements Runnable {
        /* access modifiers changed from: private */
        public volatile long completedTaskCount;
        private Thread thread;

        private Worker() {
        }

        public void run() {
            this.thread = Thread.currentThread();
            while (true) {
                Runnable fetchTask = fetchTask();
                UnorderedThreadPoolExecutor.this.idleWorkers.decrementAndGet();
                if (fetchTask == null) {
                    synchronized (UnorderedThreadPoolExecutor.this.workers) {
                        if (UnorderedThreadPoolExecutor.this.workers.size() > UnorderedThreadPoolExecutor.this.corePoolSize) {
                            UnorderedThreadPoolExecutor.this.workers.remove(this);
                            break;
                        }
                    }
                }
                if (fetchTask == UnorderedThreadPoolExecutor.EXIT_SIGNAL) {
                    break;
                }
                if (fetchTask != null) {
                    try {
                        UnorderedThreadPoolExecutor.this.queueHandler.polled(UnorderedThreadPoolExecutor.this, (IoEvent) fetchTask);
                        runTask(fetchTask);
                    } catch (Throwable th) {
                        synchronized (UnorderedThreadPoolExecutor.this.workers) {
                            UnorderedThreadPoolExecutor.this.workers.remove(this);
                            UnorderedThreadPoolExecutor.access$714(UnorderedThreadPoolExecutor.this, this.completedTaskCount);
                            UnorderedThreadPoolExecutor.this.workers.notifyAll();
                            throw th;
                        }
                    }
                }
                UnorderedThreadPoolExecutor.this.idleWorkers.incrementAndGet();
            }
            synchronized (UnorderedThreadPoolExecutor.this.workers) {
                UnorderedThreadPoolExecutor.this.workers.remove(this);
                UnorderedThreadPoolExecutor.access$714(UnorderedThreadPoolExecutor.this, this.completedTaskCount);
                UnorderedThreadPoolExecutor.this.workers.notifyAll();
            }
        }

        private Runnable fetchTask() {
            long j;
            Runnable runnable = null;
            long currentTimeMillis = System.currentTimeMillis();
            long keepAliveTime = currentTimeMillis + UnorderedThreadPoolExecutor.this.getKeepAliveTime(TimeUnit.MILLISECONDS);
            while (true) {
                long j2 = keepAliveTime - currentTimeMillis;
                if (j2 <= 0) {
                    return runnable;
                }
                try {
                    Runnable poll = UnorderedThreadPoolExecutor.this.getQueue().poll(j2, TimeUnit.MILLISECONDS);
                    if (poll != null) {
                        return poll;
                    }
                    try {
                        System.currentTimeMillis();
                        return poll;
                    } catch (InterruptedException e) {
                        runnable = poll;
                    }
                } catch (InterruptedException e2) {
                    currentTimeMillis = j;
                } catch (Throwable th) {
                    Throwable th2 = th;
                    if (runnable == null) {
                        try {
                            j = System.currentTimeMillis();
                        } catch (InterruptedException e3) {
                        }
                    } else {
                        j = currentTimeMillis;
                    }
                    throw th2;
                }
            }
        }

        private void runTask(Runnable runnable) {
            UnorderedThreadPoolExecutor.this.beforeExecute(this.thread, runnable);
            boolean z = false;
            try {
                runnable.run();
                z = true;
                UnorderedThreadPoolExecutor.this.afterExecute(runnable, null);
                this.completedTaskCount++;
            } catch (RuntimeException e) {
                if (!z) {
                    UnorderedThreadPoolExecutor.this.afterExecute(runnable, e);
                }
                throw e;
            }
        }
    }
}
