package org.apache.mina.filter.reqres;

import com.baidu.mobads.openad.d.b;
import com.tencent.open.SocialConstants;

public class Response {
    private final Object message;
    private final Request request;
    private final ResponseType type;

    public Response(Request request2, Object obj, ResponseType responseType) {
        if (request2 == null) {
            throw new IllegalArgumentException(SocialConstants.TYPE_REQUEST);
        } else if (obj == null) {
            throw new IllegalArgumentException(b.EVENT_MESSAGE);
        } else if (responseType == null) {
            throw new IllegalArgumentException("type");
        } else {
            this.request = request2;
            this.type = responseType;
            this.message = obj;
        }
    }

    public Request getRequest() {
        return this.request;
    }

    public ResponseType getType() {
        return this.type;
    }

    public Object getMessage() {
        return this.message;
    }

    public int hashCode() {
        return getRequest().getId().hashCode();
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || !(obj instanceof Response)) {
            return false;
        }
        Response response = (Response) obj;
        if (getRequest().equals(response.getRequest())) {
            return getType().equals(response.getType());
        }
        return false;
    }

    public String toString() {
        return "response: { requestId=" + getRequest().getId() + ", type=" + getType() + ", message=" + getMessage() + " }";
    }
}
