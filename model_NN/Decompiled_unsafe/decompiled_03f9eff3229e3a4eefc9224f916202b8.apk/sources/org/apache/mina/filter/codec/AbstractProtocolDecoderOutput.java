package org.apache.mina.filter.codec;

import com.baidu.mobads.openad.d.b;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public abstract class AbstractProtocolDecoderOutput implements ProtocolDecoderOutput {
    private final Queue<Object> messageQueue = new ConcurrentLinkedQueue();

    public Queue<Object> getMessageQueue() {
        return this.messageQueue;
    }

    public void write(Object obj) {
        if (obj == null) {
            throw new IllegalArgumentException(b.EVENT_MESSAGE);
        }
        this.messageQueue.add(obj);
    }
}
