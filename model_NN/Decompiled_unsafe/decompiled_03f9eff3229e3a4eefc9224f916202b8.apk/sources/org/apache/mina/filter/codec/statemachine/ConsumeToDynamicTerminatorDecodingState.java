package org.apache.mina.filter.codec.statemachine;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;

public abstract class ConsumeToDynamicTerminatorDecodingState implements DecodingState {
    private IoBuffer buffer;

    /* access modifiers changed from: protected */
    public abstract DecodingState finishDecode(IoBuffer ioBuffer, ProtocolDecoderOutput protocolDecoderOutput) throws Exception;

    /* access modifiers changed from: protected */
    public abstract boolean isTerminator(byte b);

    public DecodingState decode(IoBuffer ioBuffer, ProtocolDecoderOutput protocolDecoderOutput) throws Exception {
        IoBuffer flip;
        int position = ioBuffer.position();
        int i = -1;
        int limit = ioBuffer.limit();
        int i2 = position;
        while (true) {
            if (i2 >= limit) {
                break;
            } else if (isTerminator(ioBuffer.get(i2))) {
                i = i2;
                break;
            } else {
                i2++;
            }
        }
        if (i >= 0) {
            if (position < i) {
                ioBuffer.limit(i);
                if (this.buffer == null) {
                    flip = ioBuffer.slice();
                } else {
                    this.buffer.put(ioBuffer);
                    flip = this.buffer.flip();
                    this.buffer = null;
                }
                ioBuffer.limit(limit);
            } else if (this.buffer == null) {
                flip = IoBuffer.allocate(0);
            } else {
                flip = this.buffer.flip();
                this.buffer = null;
            }
            ioBuffer.position(i + 1);
            return finishDecode(flip, protocolDecoderOutput);
        }
        if (this.buffer == null) {
            this.buffer = IoBuffer.allocate(ioBuffer.remaining());
            this.buffer.setAutoExpand(true);
        }
        this.buffer.put(ioBuffer);
        return this;
    }

    public DecodingState finishDecode(ProtocolDecoderOutput protocolDecoderOutput) throws Exception {
        IoBuffer flip;
        if (this.buffer == null) {
            flip = IoBuffer.allocate(0);
        } else {
            flip = this.buffer.flip();
            this.buffer = null;
        }
        return finishDecode(flip, protocolDecoderOutput);
    }
}
