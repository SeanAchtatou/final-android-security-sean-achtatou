package org.apache.mina.filter.reqres;

import com.baidu.mobads.openad.d.b;
import java.util.NoSuchElementException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class Request {
    private volatile boolean endOfResponses;
    private final Object id;
    private final Object message;
    private final BlockingQueue<Object> responses;
    private volatile ScheduledFuture<?> timeoutFuture;
    private final long timeoutMillis;
    private volatile Runnable timeoutTask;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.mina.filter.reqres.Request.<init>(java.lang.Object, java.lang.Object, boolean, long):void
     arg types: [java.lang.Object, java.lang.Object, int, long]
     candidates:
      org.apache.mina.filter.reqres.Request.<init>(java.lang.Object, java.lang.Object, long, java.util.concurrent.TimeUnit):void
      org.apache.mina.filter.reqres.Request.<init>(java.lang.Object, java.lang.Object, boolean, long):void */
    public Request(Object obj, Object obj2, long j) {
        this(obj, obj2, true, j);
    }

    public Request(Object obj, Object obj2, boolean z, long j) {
        this(obj, obj2, z, j, TimeUnit.MILLISECONDS);
    }

    public Request(Object obj, Object obj2, long j, TimeUnit timeUnit) {
        this(obj, obj2, true, j, timeUnit);
    }

    public Request(Object obj, Object obj2, boolean z, long j, TimeUnit timeUnit) {
        if (obj == null) {
            throw new IllegalArgumentException("id");
        } else if (obj2 == null) {
            throw new IllegalArgumentException(b.EVENT_MESSAGE);
        } else if (j < 0) {
            throw new IllegalArgumentException("timeout: " + j + " (expected: 0+)");
        } else {
            j = j == 0 ? Long.MAX_VALUE : j;
            if (timeUnit == null) {
                throw new IllegalArgumentException("unit");
            }
            this.id = obj;
            this.message = obj2;
            this.responses = z ? new LinkedBlockingQueue() : null;
            this.timeoutMillis = timeUnit.toMillis(j);
        }
    }

    public Object getId() {
        return this.id;
    }

    public Object getMessage() {
        return this.message;
    }

    public long getTimeoutMillis() {
        return this.timeoutMillis;
    }

    public boolean isUseResponseQueue() {
        return this.responses != null;
    }

    public boolean hasResponse() {
        checkUseResponseQueue();
        return !this.responses.isEmpty();
    }

    public Response awaitResponse() throws RequestTimeoutException, InterruptedException {
        checkUseResponseQueue();
        chechEndOfResponses();
        return convertToResponse(this.responses.take());
    }

    public Response awaitResponse(long j, TimeUnit timeUnit) throws RequestTimeoutException, InterruptedException {
        checkUseResponseQueue();
        chechEndOfResponses();
        return convertToResponse(this.responses.poll(j, timeUnit));
    }

    private Response convertToResponse(Object obj) {
        if (obj instanceof Response) {
            return (Response) obj;
        }
        if (obj == null) {
            return null;
        }
        throw ((RequestTimeoutException) obj);
    }

    public Response awaitResponseUninterruptibly() throws RequestTimeoutException {
        while (true) {
            try {
                break;
            } catch (InterruptedException e) {
            }
        }
        return awaitResponse();
    }

    private void chechEndOfResponses() {
        if (this.responses != null && this.endOfResponses && this.responses.isEmpty()) {
            throw new NoSuchElementException("All responses has been retrieved already.");
        }
    }

    private void checkUseResponseQueue() {
        if (this.responses == null) {
            throw new UnsupportedOperationException("Response queue is not available; useResponseQueue is false.");
        }
    }

    /* access modifiers changed from: package-private */
    public void signal(Response response) {
        signal0(response);
        if (response.getType() != ResponseType.PARTIAL) {
            this.endOfResponses = true;
        }
    }

    /* access modifiers changed from: package-private */
    public void signal(RequestTimeoutException requestTimeoutException) {
        signal0(requestTimeoutException);
        this.endOfResponses = true;
    }

    private void signal0(Object obj) {
        if (this.responses != null) {
            this.responses.add(obj);
        }
    }

    public int hashCode() {
        return getId().hashCode();
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || !(obj instanceof Request)) {
            return false;
        }
        return getId().equals(((Request) obj).getId());
    }

    public String toString() {
        return "request: { id=" + getId() + ", timeout=" + (getTimeoutMillis() == Long.MAX_VALUE ? "max" : String.valueOf(getTimeoutMillis())) + ", message=" + getMessage() + " }";
    }

    /* access modifiers changed from: package-private */
    public Runnable getTimeoutTask() {
        return this.timeoutTask;
    }

    /* access modifiers changed from: package-private */
    public void setTimeoutTask(Runnable runnable) {
        this.timeoutTask = runnable;
    }

    /* access modifiers changed from: package-private */
    public ScheduledFuture<?> getTimeoutFuture() {
        return this.timeoutFuture;
    }

    /* access modifiers changed from: package-private */
    public void setTimeoutFuture(ScheduledFuture<?> scheduledFuture) {
        this.timeoutFuture = scheduledFuture;
    }
}
