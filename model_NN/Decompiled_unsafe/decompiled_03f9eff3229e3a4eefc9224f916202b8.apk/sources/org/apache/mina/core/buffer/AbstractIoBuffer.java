package org.apache.mina.core.buffer;

import android.support.v4.internal.view.SupportMenu;
import android.support.v4.view.MotionEventCompat;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectStreamClass;
import java.io.OutputStream;
import java.io.Serializable;
import java.io.StreamCorruptedException;
import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.CharBuffer;
import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.LongBuffer;
import java.nio.ShortBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.CoderResult;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.Set;
import org.apache.mina.proxy.handlers.socks.SocksProxyConstants;

public abstract class AbstractIoBuffer extends IoBuffer {
    private static final long BYTE_MASK = 255;
    private static final long INT_MASK = 4294967295L;
    private static final long SHORT_MASK = 65535;
    private boolean autoExpand;
    private boolean autoShrink;
    private final boolean derived;
    private int mark = -1;
    private int minimumCapacity;
    private boolean recapacityAllowed = true;

    /* access modifiers changed from: protected */
    public abstract IoBuffer asReadOnlyBuffer0();

    /* access modifiers changed from: protected */
    public abstract void buf(ByteBuffer byteBuffer);

    /* access modifiers changed from: protected */
    public abstract IoBuffer duplicate0();

    /* access modifiers changed from: protected */
    public abstract IoBuffer slice0();

    protected AbstractIoBuffer(IoBufferAllocator ioBufferAllocator, int i) {
        setAllocator(ioBufferAllocator);
        this.recapacityAllowed = true;
        this.derived = false;
        this.minimumCapacity = i;
    }

    protected AbstractIoBuffer(AbstractIoBuffer abstractIoBuffer) {
        setAllocator(getAllocator());
        this.recapacityAllowed = false;
        this.derived = true;
        this.minimumCapacity = abstractIoBuffer.minimumCapacity;
    }

    public final boolean isDirect() {
        return buf().isDirect();
    }

    public final boolean isReadOnly() {
        return buf().isReadOnly();
    }

    public final int minimumCapacity() {
        return this.minimumCapacity;
    }

    public final IoBuffer minimumCapacity(int i) {
        if (i < 0) {
            throw new IllegalArgumentException("minimumCapacity: " + i);
        }
        this.minimumCapacity = i;
        return this;
    }

    public final int capacity() {
        return buf().capacity();
    }

    public final IoBuffer capacity(int i) {
        if (!this.recapacityAllowed) {
            throw new IllegalStateException("Derived buffers and their parent can't be expanded.");
        }
        if (i > capacity()) {
            int position = position();
            int limit = limit();
            ByteOrder order = order();
            ByteBuffer buf = buf();
            ByteBuffer allocateNioBuffer = getAllocator().allocateNioBuffer(i, isDirect());
            buf.clear();
            allocateNioBuffer.put(buf);
            buf(allocateNioBuffer);
            buf().limit(limit);
            if (this.mark >= 0) {
                buf().position(this.mark);
                buf().mark();
            }
            buf().position(position);
            buf().order(order);
        }
        return this;
    }

    public final boolean isAutoExpand() {
        return this.autoExpand && this.recapacityAllowed;
    }

    public final boolean isAutoShrink() {
        return this.autoShrink && this.recapacityAllowed;
    }

    public final boolean isDerived() {
        return this.derived;
    }

    public final IoBuffer setAutoExpand(boolean z) {
        if (!this.recapacityAllowed) {
            throw new IllegalStateException("Derived buffers and their parent can't be expanded.");
        }
        this.autoExpand = z;
        return this;
    }

    public final IoBuffer setAutoShrink(boolean z) {
        if (!this.recapacityAllowed) {
            throw new IllegalStateException("Derived buffers and their parent can't be shrinked.");
        }
        this.autoShrink = z;
        return this;
    }

    public final IoBuffer expand(int i) {
        return expand(position(), i, false);
    }

    private IoBuffer expand(int i, boolean z) {
        return expand(position(), i, z);
    }

    public final IoBuffer expand(int i, int i2) {
        return expand(i, i2, false);
    }

    private IoBuffer expand(int i, int i2, boolean z) {
        int i3;
        if (!this.recapacityAllowed) {
            throw new IllegalStateException("Derived buffers and their parent can't be expanded.");
        }
        int i4 = i + i2;
        if (z) {
            i3 = IoBuffer.normalizeCapacity(i4);
        } else {
            i3 = i4;
        }
        if (i3 > capacity()) {
            capacity(i3);
        }
        if (i4 > limit()) {
            buf().limit(i4);
        }
        return this;
    }

    public final IoBuffer shrink() {
        if (!this.recapacityAllowed) {
            throw new IllegalStateException("Derived buffers and their parent can't be expanded.");
        }
        int position = position();
        int capacity = capacity();
        int limit = limit();
        if (capacity != limit) {
            int max = Math.max(this.minimumCapacity, limit);
            int i = capacity;
            while ((i >>> 1) >= max) {
                i >>>= 1;
            }
            int max2 = Math.max(max, i);
            if (max2 != capacity) {
                ByteOrder order = order();
                ByteBuffer buf = buf();
                ByteBuffer allocateNioBuffer = getAllocator().allocateNioBuffer(max2, isDirect());
                buf.position(0);
                buf.limit(limit);
                allocateNioBuffer.put(buf);
                buf(allocateNioBuffer);
                buf().position(position);
                buf().limit(limit);
                buf().order(order);
                this.mark = -1;
            }
        }
        return this;
    }

    public final int position() {
        return buf().position();
    }

    public final IoBuffer position(int i) {
        autoExpand(i, 0);
        buf().position(i);
        if (this.mark > i) {
            this.mark = -1;
        }
        return this;
    }

    public final int limit() {
        return buf().limit();
    }

    public final IoBuffer limit(int i) {
        autoExpand(i, 0);
        buf().limit(i);
        if (this.mark > i) {
            this.mark = -1;
        }
        return this;
    }

    public final IoBuffer mark() {
        ByteBuffer buf = buf();
        buf.mark();
        this.mark = buf.position();
        return this;
    }

    public final int markValue() {
        return this.mark;
    }

    public final IoBuffer reset() {
        buf().reset();
        return this;
    }

    public final IoBuffer clear() {
        buf().clear();
        this.mark = -1;
        return this;
    }

    public final IoBuffer sweep() {
        clear();
        return fillAndReset(remaining());
    }

    public final IoBuffer sweep(byte b) {
        clear();
        return fillAndReset(b, remaining());
    }

    public final IoBuffer flip() {
        buf().flip();
        this.mark = -1;
        return this;
    }

    public final IoBuffer rewind() {
        buf().rewind();
        this.mark = -1;
        return this;
    }

    public final int remaining() {
        ByteBuffer buf = buf();
        return buf.limit() - buf.position();
    }

    public final boolean hasRemaining() {
        ByteBuffer buf = buf();
        return buf.limit() > buf.position();
    }

    public final byte get() {
        return buf().get();
    }

    public final short getUnsigned() {
        return (short) (get() & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD);
    }

    public final IoBuffer put(byte b) {
        autoExpand(1);
        buf().put(b);
        return this;
    }

    public IoBuffer putUnsigned(byte b) {
        autoExpand(1);
        buf().put((byte) (b & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD));
        return this;
    }

    public IoBuffer putUnsigned(int i, byte b) {
        autoExpand(i, 1);
        buf().put(i, (byte) (b & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD));
        return this;
    }

    public IoBuffer putUnsigned(short s) {
        autoExpand(1);
        buf().put((byte) (s & 255));
        return this;
    }

    public IoBuffer putUnsigned(int i, short s) {
        autoExpand(i, 1);
        buf().put(i, (byte) (s & 255));
        return this;
    }

    public IoBuffer putUnsigned(int i) {
        autoExpand(1);
        buf().put((byte) (i & MotionEventCompat.ACTION_MASK));
        return this;
    }

    public IoBuffer putUnsigned(int i, int i2) {
        autoExpand(i, 1);
        buf().put(i, (byte) (i2 & MotionEventCompat.ACTION_MASK));
        return this;
    }

    public IoBuffer putUnsigned(long j) {
        autoExpand(1);
        buf().put((byte) ((int) (BYTE_MASK & j)));
        return this;
    }

    public IoBuffer putUnsigned(int i, long j) {
        autoExpand(i, 1);
        buf().put(i, (byte) ((int) (BYTE_MASK & j)));
        return this;
    }

    public final byte get(int i) {
        return buf().get(i);
    }

    public final short getUnsigned(int i) {
        return (short) (get(i) & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD);
    }

    public final IoBuffer put(int i, byte b) {
        autoExpand(i, 1);
        buf().put(i, b);
        return this;
    }

    public final IoBuffer get(byte[] bArr, int i, int i2) {
        buf().get(bArr, i, i2);
        return this;
    }

    public final IoBuffer put(ByteBuffer byteBuffer) {
        autoExpand(byteBuffer.remaining());
        buf().put(byteBuffer);
        return this;
    }

    public final IoBuffer put(byte[] bArr, int i, int i2) {
        autoExpand(i2);
        buf().put(bArr, i, i2);
        return this;
    }

    public final IoBuffer compact() {
        int remaining = remaining();
        int capacity = capacity();
        if (capacity != 0) {
            if (!isAutoShrink() || remaining > (capacity >>> 2) || capacity <= this.minimumCapacity) {
                buf().compact();
            } else {
                int max = Math.max(this.minimumCapacity, remaining << 1);
                int i = capacity;
                while ((i >>> 1) >= max) {
                    i >>>= 1;
                }
                int max2 = Math.max(max, i);
                if (max2 != capacity) {
                    ByteOrder order = order();
                    if (remaining > max2) {
                        throw new IllegalStateException("The amount of the remaining bytes is greater than the new capacity.");
                    }
                    ByteBuffer buf = buf();
                    ByteBuffer allocateNioBuffer = getAllocator().allocateNioBuffer(max2, isDirect());
                    allocateNioBuffer.put(buf);
                    buf(allocateNioBuffer);
                    buf().order(order);
                }
            }
            this.mark = -1;
        }
        return this;
    }

    public final ByteOrder order() {
        return buf().order();
    }

    public final IoBuffer order(ByteOrder byteOrder) {
        buf().order(byteOrder);
        return this;
    }

    public final char getChar() {
        return buf().getChar();
    }

    public final IoBuffer putChar(char c) {
        autoExpand(2);
        buf().putChar(c);
        return this;
    }

    public final char getChar(int i) {
        return buf().getChar(i);
    }

    public final IoBuffer putChar(int i, char c) {
        autoExpand(i, 2);
        buf().putChar(i, c);
        return this;
    }

    public final CharBuffer asCharBuffer() {
        return buf().asCharBuffer();
    }

    public final short getShort() {
        return buf().getShort();
    }

    public final IoBuffer putShort(short s) {
        autoExpand(2);
        buf().putShort(s);
        return this;
    }

    public final short getShort(int i) {
        return buf().getShort(i);
    }

    public final IoBuffer putShort(int i, short s) {
        autoExpand(i, 2);
        buf().putShort(i, s);
        return this;
    }

    public final ShortBuffer asShortBuffer() {
        return buf().asShortBuffer();
    }

    public final int getInt() {
        return buf().getInt();
    }

    public final IoBuffer putInt(int i) {
        autoExpand(4);
        buf().putInt(i);
        return this;
    }

    public final IoBuffer putUnsignedInt(byte b) {
        autoExpand(4);
        buf().putInt(b & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD);
        return this;
    }

    public final IoBuffer putUnsignedInt(int i, byte b) {
        autoExpand(i, 4);
        buf().putInt(i, b & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD);
        return this;
    }

    public final IoBuffer putUnsignedInt(short s) {
        autoExpand(4);
        buf().putInt(65535 & s);
        return this;
    }

    public final IoBuffer putUnsignedInt(int i, short s) {
        autoExpand(i, 4);
        buf().putInt(i, 65535 & s);
        return this;
    }

    public final IoBuffer putUnsignedInt(int i) {
        autoExpand(4);
        buf().putInt(i);
        return this;
    }

    public final IoBuffer putUnsignedInt(int i, int i2) {
        autoExpand(i, 4);
        buf().putInt(i, i2);
        return this;
    }

    public final IoBuffer putUnsignedInt(long j) {
        autoExpand(4);
        buf().putInt((int) (-1 & j));
        return this;
    }

    public final IoBuffer putUnsignedInt(int i, long j) {
        autoExpand(i, 4);
        buf().putInt(i, (int) (INT_MASK & j));
        return this;
    }

    public final IoBuffer putUnsignedShort(byte b) {
        autoExpand(2);
        buf().putShort((short) (b & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD));
        return this;
    }

    public final IoBuffer putUnsignedShort(int i, byte b) {
        autoExpand(i, 2);
        buf().putShort(i, (short) (b & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD));
        return this;
    }

    public final IoBuffer putUnsignedShort(short s) {
        autoExpand(2);
        buf().putShort(s);
        return this;
    }

    public final IoBuffer putUnsignedShort(int i, short s) {
        autoExpand(i, 2);
        buf().putShort(i, s);
        return this;
    }

    public final IoBuffer putUnsignedShort(int i) {
        autoExpand(2);
        buf().putShort((short) i);
        return this;
    }

    public final IoBuffer putUnsignedShort(int i, int i2) {
        autoExpand(i, 2);
        buf().putShort(i, (short) i2);
        return this;
    }

    public final IoBuffer putUnsignedShort(long j) {
        autoExpand(2);
        buf().putShort((short) ((int) j));
        return this;
    }

    public final IoBuffer putUnsignedShort(int i, long j) {
        autoExpand(i, 2);
        buf().putShort(i, (short) ((int) j));
        return this;
    }

    public final int getInt(int i) {
        return buf().getInt(i);
    }

    public final IoBuffer putInt(int i, int i2) {
        autoExpand(i, 4);
        buf().putInt(i, i2);
        return this;
    }

    public final IntBuffer asIntBuffer() {
        return buf().asIntBuffer();
    }

    public final long getLong() {
        return buf().getLong();
    }

    public final IoBuffer putLong(long j) {
        autoExpand(8);
        buf().putLong(j);
        return this;
    }

    public final long getLong(int i) {
        return buf().getLong(i);
    }

    public final IoBuffer putLong(int i, long j) {
        autoExpand(i, 8);
        buf().putLong(i, j);
        return this;
    }

    public final LongBuffer asLongBuffer() {
        return buf().asLongBuffer();
    }

    public final float getFloat() {
        return buf().getFloat();
    }

    public final IoBuffer putFloat(float f) {
        autoExpand(4);
        buf().putFloat(f);
        return this;
    }

    public final float getFloat(int i) {
        return buf().getFloat(i);
    }

    public final IoBuffer putFloat(int i, float f) {
        autoExpand(i, 4);
        buf().putFloat(i, f);
        return this;
    }

    public final FloatBuffer asFloatBuffer() {
        return buf().asFloatBuffer();
    }

    public final double getDouble() {
        return buf().getDouble();
    }

    public final IoBuffer putDouble(double d) {
        autoExpand(8);
        buf().putDouble(d);
        return this;
    }

    public final double getDouble(int i) {
        return buf().getDouble(i);
    }

    public final IoBuffer putDouble(int i, double d) {
        autoExpand(i, 8);
        buf().putDouble(i, d);
        return this;
    }

    public final DoubleBuffer asDoubleBuffer() {
        return buf().asDoubleBuffer();
    }

    public final IoBuffer asReadOnlyBuffer() {
        this.recapacityAllowed = false;
        return asReadOnlyBuffer0();
    }

    public final IoBuffer duplicate() {
        this.recapacityAllowed = false;
        return duplicate0();
    }

    public final IoBuffer slice() {
        this.recapacityAllowed = false;
        return slice0();
    }

    public final IoBuffer getSlice(int i, int i2) {
        if (i2 < 0) {
            throw new IllegalArgumentException("length: " + i2);
        }
        int position = position();
        int limit = limit();
        if (i > limit) {
            throw new IllegalArgumentException("index: " + i);
        }
        int i3 = i + i2;
        if (i3 > limit) {
            throw new IndexOutOfBoundsException("index + length (" + i3 + ") is greater " + "than limit (" + limit + ").");
        }
        clear();
        position(i);
        limit(i3);
        IoBuffer slice = slice();
        position(position);
        limit(limit);
        return slice;
    }

    public final IoBuffer getSlice(int i) {
        if (i < 0) {
            throw new IllegalArgumentException("length: " + i);
        }
        int position = position();
        int limit = limit();
        int i2 = position + i;
        if (limit < i2) {
            throw new IndexOutOfBoundsException("position + length (" + i2 + ") is greater " + "than limit (" + limit + ").");
        }
        limit(position + i);
        IoBuffer slice = slice();
        position(i2);
        limit(limit);
        return slice;
    }

    public int hashCode() {
        int i = 1;
        int position = position();
        for (int limit = limit() - 1; limit >= position; limit--) {
            i = (i * 31) + get(limit);
        }
        return i;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof IoBuffer)) {
            return false;
        }
        IoBuffer ioBuffer = (IoBuffer) obj;
        if (remaining() != ioBuffer.remaining()) {
            return false;
        }
        int position = position();
        int limit = limit() - 1;
        int limit2 = ioBuffer.limit() - 1;
        while (limit >= position) {
            if (get(limit) != ioBuffer.get(limit2)) {
                return false;
            }
            limit--;
            limit2--;
        }
        return true;
    }

    public int compareTo(IoBuffer ioBuffer) {
        int position = position() + Math.min(remaining(), ioBuffer.remaining());
        int position2 = position();
        int position3 = ioBuffer.position();
        while (position2 < position) {
            byte b = get(position2);
            byte b2 = ioBuffer.get(position3);
            if (b == b2) {
                position2++;
                position3++;
            } else if (b < b2) {
                return -1;
            } else {
                return 1;
            }
        }
        return remaining() - ioBuffer.remaining();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (isDirect()) {
            sb.append("DirectBuffer");
        } else {
            sb.append("HeapBuffer");
        }
        sb.append("[pos=");
        sb.append(position());
        sb.append(" lim=");
        sb.append(limit());
        sb.append(" cap=");
        sb.append(capacity());
        sb.append(": ");
        sb.append(getHexDump(16));
        sb.append(']');
        return sb.toString();
    }

    public IoBuffer get(byte[] bArr) {
        return get(bArr, 0, bArr.length);
    }

    public IoBuffer put(IoBuffer ioBuffer) {
        return put(ioBuffer.buf());
    }

    public IoBuffer put(byte[] bArr) {
        return put(bArr, 0, bArr.length);
    }

    public int getUnsignedShort() {
        return getShort() & 65535;
    }

    public int getUnsignedShort(int i) {
        return getShort(i) & 65535;
    }

    public long getUnsignedInt() {
        return ((long) getInt()) & INT_MASK;
    }

    public int getMediumInt() {
        byte b = get();
        byte b2 = get();
        byte b3 = get();
        if (ByteOrder.BIG_ENDIAN.equals(order())) {
            return getMediumInt(b, b2, b3);
        }
        return getMediumInt(b3, b2, b);
    }

    public int getUnsignedMediumInt() {
        short unsigned = getUnsigned();
        short unsigned2 = getUnsigned();
        short unsigned3 = getUnsigned();
        if (ByteOrder.BIG_ENDIAN.equals(order())) {
            return (unsigned << 16) | (unsigned2 << 8) | unsigned3;
        }
        return unsigned | (unsigned2 << 8) | (unsigned3 << 16);
    }

    public int getMediumInt(int i) {
        byte b = get(i);
        byte b2 = get(i + 1);
        byte b3 = get(i + 2);
        if (ByteOrder.BIG_ENDIAN.equals(order())) {
            return getMediumInt(b, b2, b3);
        }
        return getMediumInt(b3, b2, b);
    }

    public int getUnsignedMediumInt(int i) {
        short unsigned = getUnsigned(i);
        short unsigned2 = getUnsigned(i + 1);
        short unsigned3 = getUnsigned(i + 2);
        if (ByteOrder.BIG_ENDIAN.equals(order())) {
            return (unsigned << 16) | (unsigned2 << 8) | unsigned3;
        }
        return unsigned | (unsigned2 << 8) | (unsigned3 << 16);
    }

    private int getMediumInt(byte b, byte b2, byte b3) {
        byte b4 = ((b << 16) & 16711680) | ((b2 << 8) & 65280) | (b3 & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD);
        if ((b & 128) == 128) {
            return b4 | -16777216;
        }
        return b4;
    }

    public IoBuffer putMediumInt(int i) {
        byte b = (byte) (i >> 16);
        byte b2 = (byte) (i >> 8);
        byte b3 = (byte) i;
        if (ByteOrder.BIG_ENDIAN.equals(order())) {
            put(b).put(b2).put(b3);
        } else {
            put(b3).put(b2).put(b);
        }
        return this;
    }

    public IoBuffer putMediumInt(int i, int i2) {
        byte b = (byte) (i2 >> 16);
        byte b2 = (byte) (i2 >> 8);
        byte b3 = (byte) i2;
        if (ByteOrder.BIG_ENDIAN.equals(order())) {
            put(i, b).put(i + 1, b2).put(i + 2, b3);
        } else {
            put(i, b3).put(i + 1, b2).put(i + 2, b);
        }
        return this;
    }

    public long getUnsignedInt(int i) {
        return ((long) getInt(i)) & INT_MASK;
    }

    public InputStream asInputStream() {
        return new InputStream() {
            public int available() {
                return AbstractIoBuffer.this.remaining();
            }

            public synchronized void mark(int i) {
                AbstractIoBuffer.this.mark();
            }

            public boolean markSupported() {
                return true;
            }

            public int read() {
                if (AbstractIoBuffer.this.hasRemaining()) {
                    return AbstractIoBuffer.this.get() & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD;
                }
                return -1;
            }

            public int read(byte[] bArr, int i, int i2) {
                int remaining = AbstractIoBuffer.this.remaining();
                if (remaining <= 0) {
                    return -1;
                }
                int min = Math.min(remaining, i2);
                AbstractIoBuffer.this.get(bArr, i, min);
                return min;
            }

            public synchronized void reset() {
                AbstractIoBuffer.this.reset();
            }

            public long skip(long j) {
                int min;
                if (j > 2147483647L) {
                    min = AbstractIoBuffer.this.remaining();
                } else {
                    min = Math.min(AbstractIoBuffer.this.remaining(), (int) j);
                }
                AbstractIoBuffer.this.skip(min);
                return (long) min;
            }
        };
    }

    public OutputStream asOutputStream() {
        return new OutputStream() {
            public void write(byte[] bArr, int i, int i2) {
                AbstractIoBuffer.this.put(bArr, i, i2);
            }

            public void write(int i) {
                AbstractIoBuffer.this.put((byte) i);
            }
        };
    }

    public String getHexDump() {
        return getHexDump(Integer.MAX_VALUE);
    }

    public String getHexDump(int i) {
        return IoBufferHexDumper.getHexdump(this, i);
    }

    public String getString(CharsetDecoder charsetDecoder) throws CharacterCodingException {
        int i;
        CoderResult flush;
        if (!hasRemaining()) {
            return "";
        }
        boolean startsWith = charsetDecoder.charset().name().startsWith("UTF-16");
        int position = position();
        int limit = limit();
        int i2 = -1;
        if (!startsWith) {
            i2 = indexOf((byte) 0);
            if (i2 < 0) {
                i = limit;
                i2 = limit;
            } else {
                i = i2 + 1;
            }
        } else {
            int i3 = position;
            while (true) {
                boolean z = get(i3) == 0;
                i3++;
                if (i3 >= limit) {
                    break;
                } else if (get(i3) != 0) {
                    i3++;
                    if (i3 >= limit) {
                        break;
                    }
                } else if (z) {
                    i2 = i3 - 1;
                    break;
                }
            }
            if (i2 < 0) {
                int i4 = ((limit - position) & -2) + position;
                i = i4;
                i2 = i4;
            } else {
                i = i2 + 2 <= limit ? i2 + 2 : i2;
            }
        }
        if (position == i2) {
            position(i);
            return "";
        }
        limit(i2);
        charsetDecoder.reset();
        int remaining = ((int) (((float) remaining()) * charsetDecoder.averageCharsPerByte())) + 1;
        CharBuffer allocate = CharBuffer.allocate(remaining);
        while (true) {
            if (hasRemaining()) {
                flush = charsetDecoder.decode(buf(), allocate, true);
            } else {
                flush = charsetDecoder.flush(allocate);
            }
            if (flush.isUnderflow()) {
                limit(limit);
                position(i);
                return allocate.flip().toString();
            } else if (flush.isOverflow()) {
                CharBuffer allocate2 = CharBuffer.allocate(allocate.capacity() + remaining);
                allocate.flip();
                allocate2.put(allocate);
                allocate = allocate2;
            } else if (flush.isError()) {
                limit(limit);
                position(position);
                flush.throwException();
            }
        }
    }

    public String getString(int i, CharsetDecoder charsetDecoder) throws CharacterCodingException {
        CoderResult flush;
        checkFieldSize(i);
        if (i == 0 || !hasRemaining()) {
            return "";
        }
        boolean startsWith = charsetDecoder.charset().name().startsWith("UTF-16");
        if (!startsWith || (i & 1) == 0) {
            int position = position();
            int limit = limit();
            int i2 = position + i;
            if (limit < i2) {
                throw new BufferUnderflowException();
            }
            if (!startsWith) {
                int i3 = position;
                while (i3 < i2 && get(i3) != 0) {
                    i3++;
                }
                if (i3 == i2) {
                    limit(i2);
                } else {
                    limit(i3);
                }
            } else {
                int i4 = position;
                while (i4 < i2 && (get(i4) != 0 || get(i4 + 1) != 0)) {
                    i4 += 2;
                }
                if (i4 == i2) {
                    limit(i2);
                } else {
                    limit(i4);
                }
            }
            if (!hasRemaining()) {
                limit(limit);
                position(i2);
                return "";
            }
            charsetDecoder.reset();
            int remaining = ((int) (((float) remaining()) * charsetDecoder.averageCharsPerByte())) + 1;
            CharBuffer allocate = CharBuffer.allocate(remaining);
            while (true) {
                if (hasRemaining()) {
                    flush = charsetDecoder.decode(buf(), allocate, true);
                } else {
                    flush = charsetDecoder.flush(allocate);
                }
                if (flush.isUnderflow()) {
                    limit(limit);
                    position(i2);
                    return allocate.flip().toString();
                } else if (flush.isOverflow()) {
                    CharBuffer allocate2 = CharBuffer.allocate(allocate.capacity() + remaining);
                    allocate.flip();
                    allocate2.put(allocate);
                    allocate = allocate2;
                } else if (flush.isError()) {
                    limit(limit);
                    position(position);
                    flush.throwException();
                }
            }
        } else {
            throw new IllegalArgumentException("fieldSize is not even.");
        }
    }

    public IoBuffer putString(CharSequence charSequence, CharsetEncoder charsetEncoder) throws CharacterCodingException {
        CoderResult flush;
        if (charSequence.length() != 0) {
            CharBuffer wrap = CharBuffer.wrap(charSequence);
            charsetEncoder.reset();
            int i = 0;
            while (true) {
                if (wrap.hasRemaining()) {
                    flush = charsetEncoder.encode(wrap, buf(), true);
                } else {
                    flush = charsetEncoder.flush(buf());
                }
                if (!flush.isUnderflow()) {
                    if (!flush.isOverflow()) {
                        i = 0;
                    } else if (isAutoExpand()) {
                        switch (i) {
                            case 0:
                                autoExpand((int) Math.ceil((double) (((float) wrap.remaining()) * charsetEncoder.averageBytesPerChar())));
                                i++;
                                continue;
                            case 1:
                                autoExpand((int) Math.ceil((double) (((float) wrap.remaining()) * charsetEncoder.maxBytesPerChar())));
                                i++;
                                continue;
                            default:
                                throw new RuntimeException("Expanded by " + ((int) Math.ceil((double) (((float) wrap.remaining()) * charsetEncoder.maxBytesPerChar()))) + " but that wasn't enough for '" + ((Object) charSequence) + "'");
                        }
                    }
                    flush.throwException();
                }
            }
        }
        return this;
    }

    public IoBuffer putString(CharSequence charSequence, int i, CharsetEncoder charsetEncoder) throws CharacterCodingException {
        CoderResult flush;
        checkFieldSize(i);
        if (i != 0) {
            autoExpand(i);
            boolean startsWith = charsetEncoder.charset().name().startsWith("UTF-16");
            if (!startsWith || (i & 1) == 0) {
                int limit = limit();
                int position = position() + i;
                if (limit < position) {
                    throw new BufferOverflowException();
                } else if (charSequence.length() == 0) {
                    if (!startsWith) {
                        put((byte) 0);
                    } else {
                        put((byte) 0);
                        put((byte) 0);
                    }
                    position(position);
                } else {
                    CharBuffer wrap = CharBuffer.wrap(charSequence);
                    limit(position);
                    charsetEncoder.reset();
                    while (true) {
                        if (wrap.hasRemaining()) {
                            flush = charsetEncoder.encode(wrap, buf(), true);
                        } else {
                            flush = charsetEncoder.flush(buf());
                        }
                        if (flush.isUnderflow() || flush.isOverflow()) {
                            limit(limit);
                        } else {
                            flush.throwException();
                        }
                    }
                    limit(limit);
                    if (position() < position) {
                        if (!startsWith) {
                            put((byte) 0);
                        } else {
                            put((byte) 0);
                            put((byte) 0);
                        }
                    }
                    position(position);
                }
            } else {
                throw new IllegalArgumentException("fieldSize is not even.");
            }
        }
        return this;
    }

    public String getPrefixedString(CharsetDecoder charsetDecoder) throws CharacterCodingException {
        return getPrefixedString(2, charsetDecoder);
    }

    public String getPrefixedString(int i, CharsetDecoder charsetDecoder) throws CharacterCodingException {
        CoderResult flush;
        if (!prefixedDataAvailable(i)) {
            throw new BufferUnderflowException();
        }
        int i2 = 0;
        switch (i) {
            case 1:
                i2 = getUnsigned();
                break;
            case 2:
                i2 = getUnsignedShort();
                break;
            case 4:
                i2 = getInt();
                break;
        }
        if (i2 == 0) {
            return "";
        }
        if (!charsetDecoder.charset().name().startsWith("UTF-16") || (i2 & 1) == 0) {
            int limit = limit();
            int position = position() + i2;
            if (limit < position) {
                throw new BufferUnderflowException();
            }
            limit(position);
            charsetDecoder.reset();
            int remaining = ((int) (((float) remaining()) * charsetDecoder.averageCharsPerByte())) + 1;
            CharBuffer allocate = CharBuffer.allocate(remaining);
            while (true) {
                if (hasRemaining()) {
                    flush = charsetDecoder.decode(buf(), allocate, true);
                } else {
                    flush = charsetDecoder.flush(allocate);
                }
                if (flush.isUnderflow()) {
                    limit(limit);
                    position(position);
                    return allocate.flip().toString();
                } else if (flush.isOverflow()) {
                    CharBuffer allocate2 = CharBuffer.allocate(allocate.capacity() + remaining);
                    allocate.flip();
                    allocate2.put(allocate);
                    allocate = allocate2;
                } else {
                    flush.throwException();
                }
            }
        } else {
            throw new BufferDataException("fieldSize is not even for a UTF-16 string.");
        }
    }

    public IoBuffer putPrefixedString(CharSequence charSequence, CharsetEncoder charsetEncoder) throws CharacterCodingException {
        return putPrefixedString(charSequence, 2, 0, charsetEncoder);
    }

    public IoBuffer putPrefixedString(CharSequence charSequence, int i, CharsetEncoder charsetEncoder) throws CharacterCodingException {
        return putPrefixedString(charSequence, i, 0, charsetEncoder);
    }

    public IoBuffer putPrefixedString(CharSequence charSequence, int i, int i2, CharsetEncoder charsetEncoder) throws CharacterCodingException {
        return putPrefixedString(charSequence, i, i2, (byte) 0, charsetEncoder);
    }

    public IoBuffer putPrefixedString(CharSequence charSequence, int i, int i2, byte b, CharsetEncoder charsetEncoder) throws CharacterCodingException {
        int i3;
        int i4;
        CoderResult flush;
        switch (i) {
            case 1:
                i3 = MotionEventCompat.ACTION_MASK;
                break;
            case 2:
                i3 = SupportMenu.USER_MASK;
                break;
            case 3:
            default:
                throw new IllegalArgumentException("prefixLength: " + i);
            case 4:
                i3 = Integer.MAX_VALUE;
                break;
        }
        if (charSequence.length() > i3) {
            throw new IllegalArgumentException("The specified string is too long.");
        }
        if (charSequence.length() == 0) {
            switch (i) {
                case 1:
                    put((byte) 0);
                    break;
                case 2:
                    putShort(0);
                    break;
                case 4:
                    putInt(0);
                    break;
            }
        } else {
            switch (i2) {
                case 0:
                case 1:
                    i4 = 0;
                    break;
                case 2:
                    i4 = 1;
                    break;
                case 3:
                default:
                    throw new IllegalArgumentException("padding: " + i2);
                case 4:
                    i4 = 3;
                    break;
            }
            CharBuffer wrap = CharBuffer.wrap(charSequence);
            skip(i);
            int position = position();
            charsetEncoder.reset();
            int i5 = 0;
            while (true) {
                if (wrap.hasRemaining()) {
                    flush = charsetEncoder.encode(wrap, buf(), true);
                } else {
                    flush = charsetEncoder.flush(buf());
                }
                if (position() - position > i3) {
                    throw new IllegalArgumentException("The specified string is too long.");
                } else if (flush.isUnderflow()) {
                    fill(b, i2 - ((position() - position) & i4));
                    int position2 = position() - position;
                    switch (i) {
                        case 1:
                            put(position - 1, (byte) position2);
                            break;
                        case 2:
                            putShort(position - 2, (short) position2);
                            break;
                        case 4:
                            putInt(position - 4, position2);
                            break;
                    }
                } else {
                    if (!flush.isOverflow()) {
                        i5 = 0;
                    } else if (isAutoExpand()) {
                        switch (i5) {
                            case 0:
                                autoExpand((int) Math.ceil((double) (((float) wrap.remaining()) * charsetEncoder.averageBytesPerChar())));
                                i5++;
                                continue;
                            case 1:
                                autoExpand((int) Math.ceil((double) (((float) wrap.remaining()) * charsetEncoder.maxBytesPerChar())));
                                i5++;
                                continue;
                            default:
                                throw new RuntimeException("Expanded by " + ((int) Math.ceil((double) (((float) wrap.remaining()) * charsetEncoder.maxBytesPerChar()))) + " but that wasn't enough for '" + ((Object) charSequence) + "'");
                        }
                    }
                    flush.throwException();
                }
            }
        }
        return this;
    }

    public Object getObject() throws ClassNotFoundException {
        return getObject(Thread.currentThread().getContextClassLoader());
    }

    public Object getObject(final ClassLoader classLoader) throws ClassNotFoundException {
        if (!prefixedDataAvailable(4)) {
            throw new BufferUnderflowException();
        }
        int i = getInt();
        if (i <= 4) {
            throw new BufferDataException("Object length should be greater than 4: " + i);
        }
        int limit = limit();
        limit(i + position());
        try {
            Object readObject = new ObjectInputStream(asInputStream()) {
                /* access modifiers changed from: protected */
                public ObjectStreamClass readClassDescriptor() throws IOException, ClassNotFoundException {
                    int read = read();
                    if (read < 0) {
                        throw new EOFException();
                    }
                    switch (read) {
                        case 0:
                            return super.readClassDescriptor();
                        case 1:
                            return ObjectStreamClass.lookup(Class.forName(readUTF(), true, classLoader));
                        default:
                            throw new StreamCorruptedException("Unexpected class descriptor type: " + read);
                    }
                }

                /* access modifiers changed from: protected */
                public Class<?> resolveClass(ObjectStreamClass objectStreamClass) throws IOException, ClassNotFoundException {
                    try {
                        return Class.forName(objectStreamClass.getName(), false, classLoader);
                    } catch (ClassNotFoundException e) {
                        return super.resolveClass(objectStreamClass);
                    }
                }
            }.readObject();
            limit(limit);
            return readObject;
        } catch (IOException e) {
            throw new BufferDataException(e);
        } catch (Throwable th) {
            limit(limit);
            throw th;
        }
    }

    public IoBuffer putObject(Object obj) {
        int position = position();
        skip(4);
        try {
            AnonymousClass4 r1 = new ObjectOutputStream(asOutputStream()) {
                /* access modifiers changed from: protected */
                public void writeClassDescriptor(ObjectStreamClass objectStreamClass) throws IOException {
                    try {
                        if (!Serializable.class.isAssignableFrom(Class.forName(objectStreamClass.getName()))) {
                            write(0);
                            super.writeClassDescriptor(objectStreamClass);
                            return;
                        }
                        write(1);
                        writeUTF(objectStreamClass.getName());
                    } catch (ClassNotFoundException e) {
                        write(0);
                        super.writeClassDescriptor(objectStreamClass);
                    }
                }
            };
            r1.writeObject(obj);
            r1.flush();
            int position2 = position();
            position(position);
            putInt((position2 - position) - 4);
            position(position2);
            return this;
        } catch (IOException e) {
            throw new BufferDataException(e);
        }
    }

    public boolean prefixedDataAvailable(int i) {
        return prefixedDataAvailable(i, Integer.MAX_VALUE);
    }

    public boolean prefixedDataAvailable(int i, int i2) {
        int i3;
        if (remaining() < i) {
            return false;
        }
        switch (i) {
            case 1:
                i3 = getUnsigned(position());
                break;
            case 2:
                i3 = getUnsignedShort(position());
                break;
            case 3:
            default:
                throw new IllegalArgumentException("prefixLength: " + i);
            case 4:
                i3 = getInt(position());
                break;
        }
        if (i3 < 0 || i3 > i2) {
            throw new BufferDataException("dataLength: " + i3);
        } else if (remaining() - i >= i3) {
            return true;
        } else {
            return false;
        }
    }

    public int indexOf(byte b) {
        if (hasArray()) {
            int arrayOffset = arrayOffset();
            int limit = limit() + arrayOffset;
            byte[] array = array();
            for (int position = position() + arrayOffset; position < limit; position++) {
                if (array[position] == b) {
                    return position - arrayOffset;
                }
            }
        } else {
            int limit2 = limit();
            for (int position2 = position(); position2 < limit2; position2++) {
                if (get(position2) == b) {
                    return position2;
                }
            }
        }
        return -1;
    }

    public IoBuffer skip(int i) {
        autoExpand(i);
        return position(position() + i);
    }

    public IoBuffer fill(byte b, int i) {
        autoExpand(i);
        int i2 = i >>> 3;
        int i3 = i & 7;
        if (i2 > 0) {
            byte b2 = (b << 8) | b | (b << 16) | (b << 24);
            long j = ((long) b2) | (((long) b2) << 32);
            while (i2 > 0) {
                putLong(j);
                i2--;
            }
        }
        int i4 = i3 >>> 2;
        int i5 = i3 & 3;
        if (i4 > 0) {
            putInt((b << 8) | b | (b << 16) | (b << 24));
        }
        int i6 = i5 >> 1;
        int i7 = i5 & 1;
        if (i6 > 0) {
            putShort((short) ((b << 8) | b));
        }
        if (i7 > 0) {
            put(b);
        }
        return this;
    }

    public IoBuffer fillAndReset(byte b, int i) {
        autoExpand(i);
        int position = position();
        try {
            fill(b, i);
            return this;
        } finally {
            position(position);
        }
    }

    public IoBuffer fill(int i) {
        autoExpand(i);
        int i2 = i & 7;
        for (int i3 = i >>> 3; i3 > 0; i3--) {
            putLong(0);
        }
        int i4 = i2 >>> 2;
        int i5 = i2 & 3;
        if (i4 > 0) {
            putInt(0);
        }
        int i6 = i5 >> 1;
        int i7 = i5 & 1;
        if (i6 > 0) {
            putShort(0);
        }
        if (i7 > 0) {
            put((byte) 0);
        }
        return this;
    }

    public IoBuffer fillAndReset(int i) {
        autoExpand(i);
        int position = position();
        try {
            fill(i);
            return this;
        } finally {
            position(position);
        }
    }

    public <E extends Enum<E>> E getEnum(Class<E> cls) {
        return (Enum) toEnum(cls, getUnsigned());
    }

    public <E extends Enum<E>> E getEnum(int i, Class<E> cls) {
        return (Enum) toEnum(cls, getUnsigned(i));
    }

    public <E extends Enum<E>> E getEnumShort(Class<E> cls) {
        return (Enum) toEnum(cls, getUnsignedShort());
    }

    public <E extends Enum<E>> E getEnumShort(int i, Class<E> cls) {
        return (Enum) toEnum(cls, getUnsignedShort(i));
    }

    public <E extends Enum<E>> E getEnumInt(Class<E> cls) {
        return (Enum) toEnum(cls, getInt());
    }

    public <E extends Enum<E>> E getEnumInt(int i, Class<E> cls) {
        return (Enum) toEnum(cls, getInt(i));
    }

    public IoBuffer putEnum(Enum<?> enumR) {
        if (((long) enumR.ordinal()) <= BYTE_MASK) {
            return put((byte) enumR.ordinal());
        }
        throw new IllegalArgumentException(enumConversionErrorMessage(enumR, "byte"));
    }

    public IoBuffer putEnum(int i, Enum<?> enumR) {
        if (((long) enumR.ordinal()) <= BYTE_MASK) {
            return put(i, (byte) enumR.ordinal());
        }
        throw new IllegalArgumentException(enumConversionErrorMessage(enumR, "byte"));
    }

    public IoBuffer putEnumShort(Enum<?> enumR) {
        if (((long) enumR.ordinal()) <= SHORT_MASK) {
            return putShort((short) enumR.ordinal());
        }
        throw new IllegalArgumentException(enumConversionErrorMessage(enumR, "short"));
    }

    public IoBuffer putEnumShort(int i, Enum<?> enumR) {
        if (((long) enumR.ordinal()) <= SHORT_MASK) {
            return putShort(i, (short) enumR.ordinal());
        }
        throw new IllegalArgumentException(enumConversionErrorMessage(enumR, "short"));
    }

    public IoBuffer putEnumInt(Enum<?> enumR) {
        return putInt(enumR.ordinal());
    }

    public IoBuffer putEnumInt(int i, Enum<?> enumR) {
        return putInt(i, enumR.ordinal());
    }

    private <E> E toEnum(Class<E> cls, int i) {
        E[] enumConstants = cls.getEnumConstants();
        if (i <= enumConstants.length) {
            return enumConstants[i];
        }
        throw new IndexOutOfBoundsException(String.format("%d is too large of an ordinal to convert to the enum %s", Integer.valueOf(i), cls.getName()));
    }

    private String enumConversionErrorMessage(Enum<?> enumR, String str) {
        return String.format("%s.%s has an ordinal value too large for a %s", enumR.getClass().getName(), enumR.name(), str);
    }

    public <E extends Enum<E>> EnumSet<E> getEnumSet(Class<E> cls) {
        return toEnumSet(cls, ((long) get()) & BYTE_MASK);
    }

    public <E extends Enum<E>> EnumSet<E> getEnumSet(int i, Class<E> cls) {
        return toEnumSet(cls, ((long) get(i)) & BYTE_MASK);
    }

    public <E extends Enum<E>> EnumSet<E> getEnumSetShort(Class<E> cls) {
        return toEnumSet(cls, ((long) getShort()) & SHORT_MASK);
    }

    public <E extends Enum<E>> EnumSet<E> getEnumSetShort(int i, Class<E> cls) {
        return toEnumSet(cls, ((long) getShort(i)) & SHORT_MASK);
    }

    public <E extends Enum<E>> EnumSet<E> getEnumSetInt(Class<E> cls) {
        return toEnumSet(cls, ((long) getInt()) & INT_MASK);
    }

    public <E extends Enum<E>> EnumSet<E> getEnumSetInt(int i, Class<E> cls) {
        return toEnumSet(cls, ((long) getInt(i)) & INT_MASK);
    }

    public <E extends Enum<E>> EnumSet<E> getEnumSetLong(Class<E> cls) {
        return toEnumSet(cls, getLong());
    }

    public <E extends Enum<E>> EnumSet<E> getEnumSetLong(int i, Class<E> cls) {
        return toEnumSet(cls, getLong(i));
    }

    private <E extends Enum<E>> EnumSet<E> toEnumSet(Class<E> cls, long j) {
        EnumSet<E> noneOf = EnumSet.noneOf(cls);
        long j2 = 1;
        for (Enum enumR : (Enum[]) cls.getEnumConstants()) {
            if ((j2 & j) == j2) {
                noneOf.add(enumR);
            }
            j2 <<= 1;
        }
        return noneOf;
    }

    public <E extends Enum<E>> IoBuffer putEnumSet(Set<E> set) {
        long j = toLong(set);
        if ((-256 & j) == 0) {
            return put((byte) ((int) j));
        }
        throw new IllegalArgumentException("The enum set is too large to fit in a byte: " + set);
    }

    public <E extends Enum<E>> IoBuffer putEnumSet(int i, Set<E> set) {
        long j = toLong(set);
        if ((-256 & j) == 0) {
            return put(i, (byte) ((int) j));
        }
        throw new IllegalArgumentException("The enum set is too large to fit in a byte: " + set);
    }

    public <E extends Enum<E>> IoBuffer putEnumSetShort(Set<E> set) {
        long j = toLong(set);
        if ((-65536 & j) == 0) {
            return putShort((short) ((int) j));
        }
        throw new IllegalArgumentException("The enum set is too large to fit in a short: " + set);
    }

    public <E extends Enum<E>> IoBuffer putEnumSetShort(int i, Set<E> set) {
        long j = toLong(set);
        if ((-65536 & j) == 0) {
            return putShort(i, (short) ((int) j));
        }
        throw new IllegalArgumentException("The enum set is too large to fit in a short: " + set);
    }

    public <E extends Enum<E>> IoBuffer putEnumSetInt(Set<E> set) {
        long j = toLong(set);
        if ((-4294967296L & j) == 0) {
            return putInt((int) j);
        }
        throw new IllegalArgumentException("The enum set is too large to fit in an int: " + set);
    }

    public <E extends Enum<E>> IoBuffer putEnumSetInt(int i, Set<E> set) {
        long j = toLong(set);
        if ((-4294967296L & j) == 0) {
            return putInt(i, (int) j);
        }
        throw new IllegalArgumentException("The enum set is too large to fit in an int: " + set);
    }

    public <E extends Enum<E>> IoBuffer putEnumSetLong(Set<E> set) {
        return putLong(toLong(set));
    }

    public <E extends Enum<E>> IoBuffer putEnumSetLong(int i, Set<E> set) {
        return putLong(i, toLong(set));
    }

    private <E extends Enum<E>> long toLong(Set<E> set) {
        long j = 0;
        Iterator<E> it = set.iterator();
        while (true) {
            long j2 = j;
            if (!it.hasNext()) {
                return j2;
            }
            Enum enumR = (Enum) it.next();
            if (enumR.ordinal() >= 64) {
                throw new IllegalArgumentException("The enum set is too large to fit in a bit vector: " + set);
            }
            j = (1 << enumR.ordinal()) | j2;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.mina.core.buffer.AbstractIoBuffer.expand(int, boolean):org.apache.mina.core.buffer.IoBuffer
     arg types: [int, int]
     candidates:
      org.apache.mina.core.buffer.AbstractIoBuffer.expand(int, int):org.apache.mina.core.buffer.IoBuffer
      org.apache.mina.core.buffer.IoBuffer.expand(int, int):org.apache.mina.core.buffer.IoBuffer
      org.apache.mina.core.buffer.AbstractIoBuffer.expand(int, boolean):org.apache.mina.core.buffer.IoBuffer */
    private IoBuffer autoExpand(int i) {
        if (isAutoExpand()) {
            expand(i, true);
        }
        return this;
    }

    private IoBuffer autoExpand(int i, int i2) {
        if (isAutoExpand()) {
            expand(i, i2, true);
        }
        return this;
    }

    private static void checkFieldSize(int i) {
        if (i < 0) {
            throw new IllegalArgumentException("fieldSize cannot be negative: " + i);
        }
    }
}
