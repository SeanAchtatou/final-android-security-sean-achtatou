package org.apache.mina.core.future;

import com.baidu.mobads.openad.d.b;
import java.io.IOException;
import org.apache.mina.core.RuntimeIoException;
import org.apache.mina.core.session.IoSession;

public class DefaultReadFuture extends DefaultIoFuture implements ReadFuture {
    private static final Object CLOSED = new Object();

    public DefaultReadFuture(IoSession ioSession) {
        super(ioSession);
    }

    public Object getMessage() {
        if (!isDone()) {
            return null;
        }
        Object value = getValue();
        if (value == CLOSED) {
            return null;
        }
        if (!(value instanceof ExceptionHolder)) {
            return value;
        }
        Throwable access$000 = ((ExceptionHolder) value).exception;
        if (access$000 instanceof RuntimeException) {
            throw ((RuntimeException) access$000);
        } else if (access$000 instanceof Error) {
            throw ((Error) access$000);
        } else if (!(access$000 instanceof IOException) && !(access$000 instanceof Exception)) {
            return access$000;
        } else {
            throw new RuntimeIoException((Exception) access$000);
        }
    }

    public boolean isRead() {
        Object value;
        if (!isDone() || (value = getValue()) == CLOSED || (value instanceof ExceptionHolder)) {
            return false;
        }
        return true;
    }

    public boolean isClosed() {
        if (!isDone() || getValue() != CLOSED) {
            return false;
        }
        return true;
    }

    public Throwable getException() {
        if (isDone()) {
            Object value = getValue();
            if (value instanceof ExceptionHolder) {
                return ((ExceptionHolder) value).exception;
            }
        }
        return null;
    }

    public void setClosed() {
        setValue(CLOSED);
    }

    public void setRead(Object obj) {
        if (obj == null) {
            throw new IllegalArgumentException(b.EVENT_MESSAGE);
        }
        setValue(obj);
    }

    public void setException(Throwable th) {
        if (th == null) {
            throw new IllegalArgumentException("exception");
        }
        setValue(new ExceptionHolder(th));
    }

    public ReadFuture await() throws InterruptedException {
        return (ReadFuture) super.await();
    }

    public ReadFuture awaitUninterruptibly() {
        return (ReadFuture) super.awaitUninterruptibly();
    }

    public ReadFuture addListener(IoFutureListener<?> ioFutureListener) {
        return (ReadFuture) super.addListener(ioFutureListener);
    }

    public ReadFuture removeListener(IoFutureListener<?> ioFutureListener) {
        return (ReadFuture) super.removeListener(ioFutureListener);
    }

    private static class ExceptionHolder {
        /* access modifiers changed from: private */
        public final Throwable exception;

        private ExceptionHolder(Throwable th) {
            this.exception = th;
        }
    }
}
