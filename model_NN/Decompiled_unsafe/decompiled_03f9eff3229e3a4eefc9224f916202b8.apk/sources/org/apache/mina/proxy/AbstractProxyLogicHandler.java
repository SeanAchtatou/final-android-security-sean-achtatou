package org.apache.mina.proxy;

import java.util.LinkedList;
import java.util.Queue;
import org.a.b;
import org.a.c;
import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.filterchain.IoFilter;
import org.apache.mina.core.future.DefaultWriteFuture;
import org.apache.mina.core.future.WriteFuture;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.core.write.DefaultWriteRequest;
import org.apache.mina.core.write.WriteRequest;
import org.apache.mina.proxy.filter.ProxyFilter;
import org.apache.mina.proxy.filter.ProxyHandshakeIoBuffer;
import org.apache.mina.proxy.session.ProxyIoSession;

public abstract class AbstractProxyLogicHandler implements ProxyLogicHandler {
    private static final b LOGGER = c.a(AbstractProxyLogicHandler.class);
    private boolean handshakeComplete = false;
    private ProxyIoSession proxyIoSession;
    private Queue<Event> writeRequestQueue = null;

    public AbstractProxyLogicHandler(ProxyIoSession proxyIoSession2) {
        this.proxyIoSession = proxyIoSession2;
    }

    /* access modifiers changed from: protected */
    public ProxyFilter getProxyFilter() {
        return this.proxyIoSession.getProxyFilter();
    }

    /* access modifiers changed from: protected */
    public IoSession getSession() {
        return this.proxyIoSession.getSession();
    }

    public ProxyIoSession getProxyIoSession() {
        return this.proxyIoSession;
    }

    /* access modifiers changed from: protected */
    public WriteFuture writeData(IoFilter.NextFilter nextFilter, IoBuffer ioBuffer) {
        ProxyHandshakeIoBuffer proxyHandshakeIoBuffer = new ProxyHandshakeIoBuffer(ioBuffer);
        LOGGER.b("   session write: {}", proxyHandshakeIoBuffer);
        DefaultWriteFuture defaultWriteFuture = new DefaultWriteFuture(getSession());
        getProxyFilter().writeData(nextFilter, getSession(), new DefaultWriteRequest(proxyHandshakeIoBuffer, defaultWriteFuture), true);
        return defaultWriteFuture;
    }

    public boolean isHandshakeComplete() {
        boolean z;
        synchronized (this) {
            z = this.handshakeComplete;
        }
        return z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.a.b.e(java.lang.String, java.lang.Throwable):void
     arg types: [java.lang.String, java.lang.Exception]
     candidates:
      org.a.b.e(java.lang.String, java.lang.Object):void
      org.a.b.e(java.lang.String, java.lang.Throwable):void */
    /* access modifiers changed from: protected */
    public final void setHandshakeComplete() {
        synchronized (this) {
            this.handshakeComplete = true;
        }
        ProxyIoSession proxyIoSession2 = getProxyIoSession();
        proxyIoSession2.getConnector().fireConnected(proxyIoSession2.getSession()).awaitUninterruptibly();
        LOGGER.b("  handshake completed");
        try {
            proxyIoSession2.getEventQueue().flushPendingSessionEvents();
            flushPendingWriteRequests();
        } catch (Exception e) {
            LOGGER.e("Unable to flush pending write requests", (Throwable) e);
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        r4.writeRequestQueue = null;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void flushPendingWriteRequests() throws java.lang.Exception {
        /*
            r4 = this;
            monitor-enter(r4)
            org.a.b r0 = org.apache.mina.proxy.AbstractProxyLogicHandler.LOGGER     // Catch:{ all -> 0x0039 }
            java.lang.String r1 = " flushPendingWriteRequests()"
            r0.b(r1)     // Catch:{ all -> 0x0039 }
            java.util.Queue<org.apache.mina.proxy.AbstractProxyLogicHandler$Event> r0 = r4.writeRequestQueue     // Catch:{ all -> 0x0039 }
            if (r0 != 0) goto L_0x000e
        L_0x000c:
            monitor-exit(r4)
            return
        L_0x000e:
            java.util.Queue<org.apache.mina.proxy.AbstractProxyLogicHandler$Event> r0 = r4.writeRequestQueue     // Catch:{ all -> 0x0039 }
            java.lang.Object r0 = r0.poll()     // Catch:{ all -> 0x0039 }
            org.apache.mina.proxy.AbstractProxyLogicHandler$Event r0 = (org.apache.mina.proxy.AbstractProxyLogicHandler.Event) r0     // Catch:{ all -> 0x0039 }
            if (r0 == 0) goto L_0x003c
            org.a.b r1 = org.apache.mina.proxy.AbstractProxyLogicHandler.LOGGER     // Catch:{ all -> 0x0039 }
            java.lang.String r2 = " Flushing buffered write request: {}"
            java.lang.Object r3 = r0.data     // Catch:{ all -> 0x0039 }
            r1.b(r2, r3)     // Catch:{ all -> 0x0039 }
            org.apache.mina.proxy.filter.ProxyFilter r1 = r4.getProxyFilter()     // Catch:{ all -> 0x0039 }
            org.apache.mina.core.filterchain.IoFilter$NextFilter r2 = r0.nextFilter     // Catch:{ all -> 0x0039 }
            org.apache.mina.core.session.IoSession r3 = r4.getSession()     // Catch:{ all -> 0x0039 }
            java.lang.Object r0 = r0.data     // Catch:{ all -> 0x0039 }
            org.apache.mina.core.write.WriteRequest r0 = (org.apache.mina.core.write.WriteRequest) r0     // Catch:{ all -> 0x0039 }
            r1.filterWrite(r2, r3, r0)     // Catch:{ all -> 0x0039 }
            goto L_0x000e
        L_0x0039:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        L_0x003c:
            r0 = 0
            r4.writeRequestQueue = r0     // Catch:{ all -> 0x0039 }
            goto L_0x000c
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.mina.proxy.AbstractProxyLogicHandler.flushPendingWriteRequests():void");
    }

    public synchronized void enqueueWriteRequest(IoFilter.NextFilter nextFilter, WriteRequest writeRequest) {
        if (this.writeRequestQueue == null) {
            this.writeRequestQueue = new LinkedList();
        }
        this.writeRequestQueue.offer(new Event(nextFilter, writeRequest));
    }

    /* access modifiers changed from: protected */
    public void closeSession(String str, Throwable th) {
        if (th != null) {
            LOGGER.e(str, th);
            this.proxyIoSession.setAuthenticationFailed(true);
        } else {
            LOGGER.e(str);
        }
        getSession().close(true);
    }

    /* access modifiers changed from: protected */
    public void closeSession(String str) {
        closeSession(str, null);
    }

    private static final class Event {
        /* access modifiers changed from: private */
        public final Object data;
        /* access modifiers changed from: private */
        public final IoFilter.NextFilter nextFilter;

        Event(IoFilter.NextFilter nextFilter2, Object obj) {
            this.nextFilter = nextFilter2;
            this.data = obj;
        }

        public Object getData() {
            return this.data;
        }

        public IoFilter.NextFilter getNextFilter() {
            return this.nextFilter;
        }
    }
}
