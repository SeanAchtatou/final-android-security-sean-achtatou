package org.apache.mina.proxy.handlers.http.digest;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.StringTokenizer;
import org.a.b;
import org.a.c;
import org.apache.mina.core.filterchain.IoFilter;
import org.apache.mina.proxy.ProxyAuthException;
import org.apache.mina.proxy.handlers.http.AbstractAuthLogicHandler;
import org.apache.mina.proxy.handlers.http.HttpProxyConstants;
import org.apache.mina.proxy.handlers.http.HttpProxyRequest;
import org.apache.mina.proxy.handlers.http.HttpProxyResponse;
import org.apache.mina.proxy.session.ProxyIoSession;
import org.apache.mina.proxy.utils.StringUtilities;
import org.apache.mina.util.Base64;

public class HttpDigestAuthLogicHandler extends AbstractAuthLogicHandler {
    private static final b logger = c.a(HttpDigestAuthLogicHandler.class);
    private static SecureRandom rnd;
    private HashMap<String, String> directives = null;
    private HttpProxyResponse response;

    static {
        try {
            rnd = SecureRandom.getInstance("SHA1PRNG");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    public HttpDigestAuthLogicHandler(ProxyIoSession proxyIoSession) throws ProxyAuthException {
        super(proxyIoSession);
        ((HttpProxyRequest) this.request).checkRequiredProperties(HttpProxyConstants.USER_PROPERTY, HttpProxyConstants.PWD_PROPERTY);
    }

    /* JADX WARN: Type inference failed for: r1v3, types: [java.lang.Throwable, org.apache.mina.proxy.ProxyAuthException] */
    /* JADX WARN: Type inference failed for: r0v37, types: [java.lang.Throwable, org.apache.mina.proxy.ProxyAuthException] */
    /* JADX WARN: Type inference failed for: r1v6, types: [java.lang.Throwable, org.apache.mina.proxy.ProxyAuthException] */
    /* JADX WARN: Type inference failed for: r0v48, types: [java.lang.Throwable, org.apache.mina.proxy.ProxyAuthException] */
    /* JADX WARN: Type inference failed for: r0v51, types: [java.lang.Throwable, org.apache.mina.proxy.ProxyAuthException] */
    public void doHandshake(IoFilter.NextFilter nextFilter) throws ProxyAuthException {
        Map hashMap;
        boolean z;
        logger.b(" doHandshake()");
        if (this.step <= 0 || this.directives != null) {
            HttpProxyRequest httpProxyRequest = (HttpProxyRequest) this.request;
            if (httpProxyRequest.getHeaders() != null) {
                hashMap = httpProxyRequest.getHeaders();
            } else {
                hashMap = new HashMap();
            }
            if (this.step > 0) {
                logger.b("  sending DIGEST challenge response");
                HashMap hashMap2 = new HashMap();
                hashMap2.put("username", httpProxyRequest.getProperties().get(HttpProxyConstants.USER_PROPERTY));
                StringUtilities.copyDirective(this.directives, hashMap2, "realm");
                StringUtilities.copyDirective(this.directives, hashMap2, "uri");
                StringUtilities.copyDirective(this.directives, hashMap2, "opaque");
                StringUtilities.copyDirective(this.directives, hashMap2, "nonce");
                String copyDirective = StringUtilities.copyDirective(this.directives, hashMap2, "algorithm");
                if (copyDirective == null || "md5".equalsIgnoreCase(copyDirective) || "md5-sess".equalsIgnoreCase(copyDirective)) {
                    String str = this.directives.get("qop");
                    if (str != null) {
                        StringTokenizer stringTokenizer = new StringTokenizer(str, ",");
                        String str2 = null;
                        while (stringTokenizer.hasMoreTokens()) {
                            String nextToken = stringTokenizer.nextToken();
                            if ("auth".equalsIgnoreCase(str2)) {
                                break;
                            }
                            if (Arrays.binarySearch(DigestUtilities.SUPPORTED_QOPS, nextToken) <= -1) {
                                nextToken = str2;
                            }
                            str2 = nextToken;
                        }
                        if (str2 != null) {
                            hashMap2.put("qop", str2);
                            byte[] bArr = new byte[8];
                            rnd.nextBytes(bArr);
                            try {
                                hashMap2.put("cnonce", new String(Base64.encodeBase64(bArr), this.proxyIoSession.getCharsetName()));
                            } catch (UnsupportedEncodingException e) {
                                throw new ProxyAuthException("Unable to encode cnonce", e);
                            }
                        } else {
                            throw new ProxyAuthException("No supported qop option available");
                        }
                    }
                    hashMap2.put("nc", "00000001");
                    hashMap2.put("uri", httpProxyRequest.getHttpURI());
                    try {
                        hashMap2.put("response", DigestUtilities.computeResponseValue(this.proxyIoSession.getSession(), hashMap2, httpProxyRequest.getHttpVerb().toUpperCase(), httpProxyRequest.getProperties().get(HttpProxyConstants.PWD_PROPERTY), this.proxyIoSession.getCharsetName(), this.response.getBody()));
                        StringBuilder sb = new StringBuilder("Digest ");
                        boolean z2 = false;
                        for (String str3 : hashMap2.keySet()) {
                            if (z2) {
                                sb.append(", ");
                            } else {
                                z2 = true;
                            }
                            if ("qop".equals(str3) || "nc".equals(str3)) {
                                z = false;
                            } else {
                                z = true;
                            }
                            sb.append(str3);
                            if (z) {
                                sb.append("=\"").append((String) hashMap2.get(str3)).append('\"');
                            } else {
                                sb.append('=').append((String) hashMap2.get(str3));
                            }
                        }
                        StringUtilities.addValueToHeader(hashMap, "Proxy-Authorization", sb.toString(), true);
                    } catch (Exception e2) {
                        throw new ProxyAuthException("Digest response computing failed", e2);
                    }
                } else {
                    throw new ProxyAuthException("Unknown algorithm required by server");
                }
            }
            addKeepAliveHeaders(hashMap);
            httpProxyRequest.setHeaders(hashMap);
            writeRequest(nextFilter, httpProxyRequest);
            this.step++;
            return;
        }
        throw new ProxyAuthException("Authentication challenge not received");
    }

    /* JADX WARN: Type inference failed for: r0v1, types: [java.lang.Throwable, org.apache.mina.proxy.ProxyAuthException] */
    /* JADX WARN: Type inference failed for: r1v9, types: [java.lang.Throwable, org.apache.mina.proxy.ProxyAuthException] */
    /* JADX WARN: Type inference failed for: r0v14, types: [java.lang.Throwable, org.apache.mina.proxy.ProxyAuthException] */
    /* JADX WARN: Type inference failed for: r0v18, types: [java.lang.Throwable, org.apache.mina.proxy.ProxyAuthException] */
    public void handleResponse(HttpProxyResponse httpProxyResponse) throws ProxyAuthException {
        String str;
        this.response = httpProxyResponse;
        if (this.step != 0) {
            throw new ProxyAuthException("Received unexpected response code (" + httpProxyResponse.getStatusLine() + ").");
        } else if (httpProxyResponse.getStatusCode() == 401 || httpProxyResponse.getStatusCode() == 407) {
            Iterator it = httpProxyResponse.getHeaders().get("Proxy-Authenticate").iterator();
            while (true) {
                if (!it.hasNext()) {
                    str = null;
                    break;
                }
                str = (String) it.next();
                if (str.startsWith("Digest")) {
                    break;
                }
            }
            if (str == null) {
                throw new ProxyAuthException("Server doesn't support digest authentication method !");
            }
            try {
                this.directives = StringUtilities.parseDirectives(str.substring(7).getBytes(this.proxyIoSession.getCharsetName()));
                this.step = 1;
            } catch (Exception e) {
                throw new ProxyAuthException("Parsing of server digest directives failed", e);
            }
        } else {
            throw new ProxyAuthException("Received unexpected response code (" + httpProxyResponse.getStatusLine() + ").");
        }
    }
}
