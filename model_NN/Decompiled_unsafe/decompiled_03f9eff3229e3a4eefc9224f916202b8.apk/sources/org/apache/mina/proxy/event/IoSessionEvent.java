package org.apache.mina.proxy.event;

import org.a.b;
import org.a.c;
import org.apache.mina.core.filterchain.IoFilter;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.core.session.IoSession;

public class IoSessionEvent {
    private static final b logger = c.a(IoSessionEvent.class);
    private final IoFilter.NextFilter nextFilter;
    private final IoSession session;
    private IdleStatus status;
    private final IoSessionEventType type;

    public IoSessionEvent(IoFilter.NextFilter nextFilter2, IoSession ioSession, IoSessionEventType ioSessionEventType) {
        this.nextFilter = nextFilter2;
        this.session = ioSession;
        this.type = ioSessionEventType;
    }

    public IoSessionEvent(IoFilter.NextFilter nextFilter2, IoSession ioSession, IdleStatus idleStatus) {
        this(nextFilter2, ioSession, IoSessionEventType.IDLE);
        this.status = idleStatus;
    }

    public void deliverEvent() {
        logger.b("Delivering event {}", this);
        deliverEvent(this.nextFilter, this.session, this.type, this.status);
    }

    private static void deliverEvent(IoFilter.NextFilter nextFilter2, IoSession ioSession, IoSessionEventType ioSessionEventType, IdleStatus idleStatus) {
        switch (ioSessionEventType) {
            case CREATED:
                nextFilter2.sessionCreated(ioSession);
                return;
            case OPENED:
                nextFilter2.sessionOpened(ioSession);
                return;
            case IDLE:
                nextFilter2.sessionIdle(ioSession, idleStatus);
                return;
            case CLOSED:
                nextFilter2.sessionClosed(ioSession);
                return;
            default:
                return;
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(IoSessionEvent.class.getSimpleName());
        sb.append('@');
        sb.append(Integer.toHexString(hashCode()));
        sb.append(" - [ ").append(this.session);
        sb.append(", ").append(this.type);
        sb.append(']');
        return sb.toString();
    }

    public IdleStatus getStatus() {
        return this.status;
    }

    public IoFilter.NextFilter getNextFilter() {
        return this.nextFilter;
    }

    public IoSession getSession() {
        return this.session;
    }

    public IoSessionEventType getType() {
        return this.type;
    }
}
