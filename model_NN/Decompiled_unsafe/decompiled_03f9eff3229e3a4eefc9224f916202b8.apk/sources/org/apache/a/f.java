package org.apache.a;

public class f extends Exception {
    public f() {
    }

    public f(String str) {
        super(str);
    }

    public f(String str, Throwable th) {
        super(str, th);
    }

    public f(Throwable th) {
        super(th);
    }
}
