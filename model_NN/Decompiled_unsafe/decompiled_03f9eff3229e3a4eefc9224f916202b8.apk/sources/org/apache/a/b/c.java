package org.apache.a.b;

public class c {

    /* renamed from: a  reason: collision with root package name */
    public final String f2581a;
    public final byte b;
    public final short c;

    public c() {
        this("", (byte) 0, 0);
    }

    public c(String str, byte b2, short s) {
        this.f2581a = str;
        this.b = b2;
        this.c = s;
    }

    public String toString() {
        return "<TField name:'" + this.f2581a + "' type:" + ((int) this.b) + " field-id:" + ((int) this.c) + ">";
    }
}
