package org.apache.a.b;

import android.support.v4.media.TransportMediator;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import org.apache.a.c.d;
import org.apache.a.f;

public final class b extends f {
    private static final k d = new k("");
    private static final c f = new c("", (byte) 0, 0);
    private static final byte[] g = new byte[16];

    /* renamed from: a  reason: collision with root package name */
    byte[] f2580a = new byte[5];
    byte[] b = new byte[10];
    byte[] c = new byte[1];
    private org.apache.a.a h = new org.apache.a.a(15);
    private short i = 0;
    private c j = null;
    private Boolean k = null;
    private byte[] l = new byte[1];

    public static class a implements h {
        public f a(d dVar) {
            return new b(dVar);
        }
    }

    static {
        g[0] = 0;
        g[2] = 1;
        g[3] = 3;
        g[6] = 4;
        g[8] = 5;
        g[10] = 6;
        g[4] = 7;
        g[11] = 8;
        g[15] = 9;
        g[14] = 10;
        g[13] = 11;
        g[12] = 12;
    }

    public b(d dVar) {
        super(dVar);
    }

    private long A() {
        int i2 = 0;
        long j2 = 0;
        if (this.e.c() >= 10) {
            byte[] a2 = this.e.a();
            int b2 = this.e.b();
            int i3 = 0;
            while (true) {
                byte b3 = a2[b2 + i2];
                j2 |= ((long) (b3 & Byte.MAX_VALUE)) << i3;
                if ((b3 & 128) != 128) {
                    break;
                }
                i3 += 7;
                i2++;
            }
            this.e.a(i2 + 1);
        } else {
            while (true) {
                byte r = r();
                j2 |= ((long) (r & Byte.MAX_VALUE)) << i2;
                if ((r & 128) != 128) {
                    break;
                }
                i2 += 7;
            }
        }
        return j2;
    }

    private long a(byte[] bArr) {
        return ((((long) bArr[7]) & 255) << 56) | ((((long) bArr[6]) & 255) << 48) | ((((long) bArr[5]) & 255) << 40) | ((((long) bArr[4]) & 255) << 32) | ((((long) bArr[3]) & 255) << 24) | ((((long) bArr[2]) & 255) << 16) | ((((long) bArr[1]) & 255) << 8) | (((long) bArr[0]) & 255);
    }

    private void a(c cVar, byte b2) {
        if (b2 == -1) {
            b2 = e(cVar.b);
        }
        if (cVar.c <= this.i || cVar.c - this.i > 15) {
            b(b2);
            a(cVar.c);
        } else {
            d((int) (((cVar.c - this.i) << 4) | b2));
        }
        this.i = cVar.c;
    }

    private void a(byte[] bArr, int i2, int i3) {
        b(i3);
        this.e.b(bArr, i2, i3);
    }

    private void b(byte b2) {
        this.l[0] = b2;
        this.e.b(this.l);
    }

    private void b(int i2) {
        int i3 = 0;
        while ((i2 & -128) != 0) {
            this.f2580a[i3] = (byte) ((i2 & TransportMediator.KEYCODE_MEDIA_PAUSE) | 128);
            i2 >>>= 7;
            i3++;
        }
        this.f2580a[i3] = (byte) i2;
        this.e.b(this.f2580a, 0, i3 + 1);
    }

    private void b(long j2) {
        int i2 = 0;
        while ((-128 & j2) != 0) {
            this.b[i2] = (byte) ((int) ((127 & j2) | 128));
            j2 >>>= 7;
            i2++;
        }
        this.b[i2] = (byte) ((int) j2);
        this.e.b(this.b, 0, i2 + 1);
    }

    private int c(int i2) {
        return (i2 << 1) ^ (i2 >> 31);
    }

    private long c(long j2) {
        return (j2 << 1) ^ (j2 >> 63);
    }

    private boolean c(byte b2) {
        byte b3 = b2 & 15;
        return b3 == 1 || b3 == 2;
    }

    private byte d(byte b2) {
        switch ((byte) (b2 & 15)) {
            case 0:
                return 0;
            case 1:
            case 2:
                return 2;
            case 3:
                return 3;
            case 4:
                return 6;
            case 5:
                return 8;
            case 6:
                return 10;
            case 7:
                return 4;
            case 8:
                return 11;
            case 9:
                return 15;
            case 10:
                return 14;
            case 11:
                return 13;
            case 12:
                return 12;
            default:
                throw new g("don't know what type: " + ((int) ((byte) (b2 & 15))));
        }
    }

    private long d(long j2) {
        return (j2 >>> 1) ^ (-(1 & j2));
    }

    private void d(int i2) {
        b((byte) i2);
    }

    private byte e(byte b2) {
        return g[b2];
    }

    private byte[] e(int i2) {
        if (i2 == 0) {
            return new byte[0];
        }
        byte[] bArr = new byte[i2];
        this.e.d(bArr, 0, i2);
        return bArr;
    }

    private int f(int i2) {
        return (i2 >>> 1) ^ (-(i2 & 1));
    }

    private int z() {
        int i2 = 0;
        if (this.e.c() >= 5) {
            byte[] a2 = this.e.a();
            int b2 = this.e.b();
            int i3 = 0;
            int i4 = 0;
            while (true) {
                byte b3 = a2[b2 + i2];
                i4 |= (b3 & Byte.MAX_VALUE) << i3;
                if ((b3 & 128) != 128) {
                    this.e.a(i2 + 1);
                    return i4;
                }
                i3 += 7;
                i2++;
            }
        } else {
            int i5 = 0;
            while (true) {
                byte r = r();
                i5 |= (r & Byte.MAX_VALUE) << i2;
                if ((r & 128) != 128) {
                    return i5;
                }
                i2 += 7;
            }
        }
    }

    public void a() {
        this.i = this.h.a();
    }

    public void a(byte b2) {
        b(b2);
    }

    /* access modifiers changed from: protected */
    public void a(byte b2, int i2) {
        if (i2 <= 14) {
            d((int) ((i2 << 4) | e(b2)));
            return;
        }
        d((int) (e(b2) | 240));
        b(i2);
    }

    public void a(int i2) {
        b(c(i2));
    }

    public void a(long j2) {
        b(c(j2));
    }

    public void a(String str) {
        try {
            byte[] bytes = str.getBytes("UTF-8");
            a(bytes, 0, bytes.length);
        } catch (UnsupportedEncodingException e) {
            throw new f("UTF-8 not supported!");
        }
    }

    public void a(ByteBuffer byteBuffer) {
        a(byteBuffer.array(), byteBuffer.position() + byteBuffer.arrayOffset(), (byteBuffer.limit() - byteBuffer.position()) - byteBuffer.arrayOffset());
    }

    public void a(c cVar) {
        if (cVar.b == 2) {
            this.j = cVar;
        } else {
            a(cVar, (byte) -1);
        }
    }

    public void a(d dVar) {
        a(dVar.f2582a, dVar.b);
    }

    public void a(e eVar) {
        if (eVar.c == 0) {
            d(0);
            return;
        }
        b(eVar.c);
        d((int) ((e(eVar.f2583a) << 4) | e(eVar.b)));
    }

    public void a(j jVar) {
        a(jVar.f2586a, jVar.b);
    }

    public void a(k kVar) {
        this.h.a(this.i);
        this.i = 0;
    }

    public void a(short s) {
        b(c((int) s));
    }

    public void a(boolean z) {
        byte b2 = 1;
        if (this.j != null) {
            c cVar = this.j;
            if (!z) {
                b2 = 2;
            }
            a(cVar, b2);
            this.j = null;
            return;
        }
        if (!z) {
            b2 = 2;
        }
        b(b2);
    }

    public void b() {
    }

    public void c() {
        b((byte) 0);
    }

    public void d() {
    }

    public void e() {
    }

    public void f() {
    }

    public k g() {
        this.h.a(this.i);
        this.i = 0;
        return d;
    }

    public void h() {
        this.i = this.h.a();
    }

    public c i() {
        byte r = r();
        if (r == 0) {
            return f;
        }
        short s = (short) ((r & 240) >> 4);
        c cVar = new c("", d((byte) (r & 15)), s == 0 ? s() : (short) (s + this.i));
        if (c(r)) {
            this.k = ((byte) (r & 15)) == 1 ? Boolean.TRUE : Boolean.FALSE;
        }
        this.i = cVar.c;
        return cVar;
    }

    public void j() {
    }

    public e k() {
        int z = z();
        byte r = z == 0 ? 0 : r();
        return new e(d((byte) (r >> 4)), d((byte) (r & 15)), z);
    }

    public void l() {
    }

    public d m() {
        byte r = r();
        int i2 = (r >> 4) & 15;
        if (i2 == 15) {
            i2 = z();
        }
        return new d(d(r), i2);
    }

    public void n() {
    }

    public j o() {
        return new j(m());
    }

    public void p() {
    }

    public boolean q() {
        if (this.k == null) {
            return r() == 1;
        }
        boolean booleanValue = this.k.booleanValue();
        this.k = null;
        return booleanValue;
    }

    public byte r() {
        if (this.e.c() > 0) {
            byte b2 = this.e.a()[this.e.b()];
            this.e.a(1);
            return b2;
        }
        this.e.d(this.c, 0, 1);
        return this.c[0];
    }

    public short s() {
        return (short) f(z());
    }

    public int t() {
        return f(z());
    }

    public long u() {
        return d(A());
    }

    public double v() {
        byte[] bArr = new byte[8];
        this.e.d(bArr, 0, 8);
        return Double.longBitsToDouble(a(bArr));
    }

    public String w() {
        int z = z();
        if (z == 0) {
            return "";
        }
        try {
            if (this.e.c() < z) {
                return new String(e(z), "UTF-8");
            }
            String str = new String(this.e.a(), this.e.b(), z, "UTF-8");
            this.e.a(z);
            return str;
        } catch (UnsupportedEncodingException e) {
            throw new f("UTF-8 not supported!");
        }
    }

    public ByteBuffer x() {
        int z = z();
        if (z == 0) {
            return ByteBuffer.wrap(new byte[0]);
        }
        byte[] bArr = new byte[z];
        this.e.d(bArr, 0, z);
        return ByteBuffer.wrap(bArr);
    }

    public void y() {
        this.h.b();
        this.i = 0;
    }
}
