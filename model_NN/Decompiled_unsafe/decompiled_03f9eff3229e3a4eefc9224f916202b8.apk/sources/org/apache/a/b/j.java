package org.apache.a.b;

public final class j {

    /* renamed from: a  reason: collision with root package name */
    public final byte f2586a;
    public final int b;

    public j() {
        this((byte) 0, 0);
    }

    public j(byte b2, int i) {
        this.f2586a = b2;
        this.b = i;
    }

    public j(d dVar) {
        this(dVar.f2582a, dVar.b);
    }
}
