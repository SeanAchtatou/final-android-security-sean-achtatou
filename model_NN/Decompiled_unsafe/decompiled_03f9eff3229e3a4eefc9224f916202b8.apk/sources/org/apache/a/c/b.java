package org.apache.a.c;

import org.apache.a.d;

public class b extends d {

    /* renamed from: a  reason: collision with root package name */
    private d f2590a;
    private int b;

    public b(int i) {
        this.f2590a = new d(i);
    }

    public int a(byte[] bArr, int i, int i2) {
        byte[] a2 = this.f2590a.a();
        if (i2 > this.f2590a.b() - this.b) {
            i2 = this.f2590a.b() - this.b;
        }
        if (i2 > 0) {
            System.arraycopy(a2, this.b, bArr, i, i2);
            this.b += i2;
        }
        return i2;
    }

    public void b(byte[] bArr, int i, int i2) {
        this.f2590a.write(bArr, i, i2);
    }

    public int b_() {
        return this.f2590a.size();
    }
}
