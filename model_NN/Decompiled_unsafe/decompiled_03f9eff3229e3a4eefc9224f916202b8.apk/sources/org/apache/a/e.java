package org.apache.a;

import org.apache.a.b.a;
import org.apache.a.b.f;
import org.apache.a.b.h;
import org.apache.a.c.c;

public class e {

    /* renamed from: a  reason: collision with root package name */
    private final f f2593a;
    private final c b;

    public e() {
        this(new a.C0061a());
    }

    public e(h hVar) {
        this.b = new c();
        this.f2593a = hVar.a(this.b);
    }

    public void a(b bVar, byte[] bArr) {
        try {
            this.b.a(bArr);
            bVar.a(this.f2593a);
        } finally {
            this.f2593a.y();
        }
    }
}
