package org.apache.a;

import java.io.ByteArrayOutputStream;
import org.apache.a.b.a;
import org.apache.a.b.f;
import org.apache.a.b.h;
import org.apache.a.c.a;

public class g {

    /* renamed from: a  reason: collision with root package name */
    private final ByteArrayOutputStream f2594a;
    private final a b;
    private f c;

    public g() {
        this(new a.C0061a());
    }

    public g(h hVar) {
        this.f2594a = new ByteArrayOutputStream();
        this.b = new org.apache.a.c.a(this.f2594a);
        this.c = hVar.a(this.b);
    }

    public byte[] a(b bVar) {
        this.f2594a.reset();
        bVar.b(this.c);
        return this.f2594a.toByteArray();
    }
}
