package org.apache.a.a;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class b implements Serializable {
    private static Map<Class<? extends org.apache.a.b>, Map<?, b>> d = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public final String f2572a;
    public final byte b;
    public final c c;

    public b(String str, byte b2, c cVar) {
        this.f2572a = str;
        this.b = b2;
        this.c = cVar;
    }

    public static void a(Class<? extends org.apache.a.b> cls, Map<?, b> map) {
        d.put(cls, map);
    }
}
