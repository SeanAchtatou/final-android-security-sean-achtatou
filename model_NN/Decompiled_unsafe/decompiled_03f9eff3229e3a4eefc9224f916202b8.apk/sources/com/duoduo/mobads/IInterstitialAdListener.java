package com.duoduo.mobads;

public interface IInterstitialAdListener {
    void onAdClick(IInterstitialAd iInterstitialAd);

    void onAdDismissed();

    void onAdFailed(String str);

    void onAdPresent();

    void onAdReady();
}
