package com.duoduo.cmmusic.init;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import com.sina.weibo.sdk.exception.WeiboAuthException;
import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

public class GetAppInfo {
    public static String getAppid(Context context) {
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            if (applicationInfo != null) {
                return applicationInfo.metaData.getString("appid").substring(6, 24);
            }
            return "";
        } catch (PackageManager.NameNotFoundException e) {
            PackageManager.NameNotFoundException nameNotFoundException = e;
            String str = "";
            PackageManager.NameNotFoundException nameNotFoundException2 = nameNotFoundException;
            Log.e("SDK_LW_CMM", nameNotFoundException2.getMessage(), nameNotFoundException2);
            return str;
        }
    }

    public static String getexCode(Context context) {
        PackageManager.NameNotFoundException e;
        String str;
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            if (applicationInfo == null) {
                return "";
            }
            str = applicationInfo.metaData.getString("excode");
            if (str == null) {
                return str;
            }
            try {
                if (str.length() <= 0) {
                    return str;
                }
                Log.d("SDK_LW_CMM", str);
                return str.substring(7, 11);
            } catch (PackageManager.NameNotFoundException e2) {
                e = e2;
                Log.e("SDK_LW_CMM", e.getMessage(), e);
                return str;
            }
        } catch (PackageManager.NameNotFoundException e3) {
            PackageManager.NameNotFoundException nameNotFoundException = e3;
            str = "";
            e = nameNotFoundException;
            Log.e("SDK_LW_CMM", e.getMessage(), e);
            return str;
        }
    }

    public static String getSign(Context context) {
        byte[] bArr = null;
        try {
            bArr = getSignInfo(context).getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            Log.e("SDK_LW_CMM", e.getMessage(), e);
        }
        return MD5.bytes2hex(MD5.md5(bArr));
    }

    public static String getSignInfo(Context context) {
        String str = "";
        try {
            str = parseSignature(context.getPackageManager().getPackageInfo(getPackageName(context), 64).signatures[0].toByteArray());
            return str.toLowerCase();
        } catch (Exception e) {
            Exception exc = e;
            String str2 = str;
            Exception exc2 = exc;
            Log.e("SDK_LW_CMM", exc2.getMessage(), exc2);
            return str2;
        }
    }

    public static String parseSignature(byte[] bArr) {
        try {
            String lowerCase = Utils.subString(((X509Certificate) CertificateFactory.getInstance("X.509").generateCertificate(new ByteArrayInputStream(bArr))).getPublicKey().toString()).replace(",", "").toLowerCase();
            return lowerCase.substring(lowerCase.indexOf("modulus") + 8, lowerCase.indexOf("publicexponent"));
        } catch (CertificateException e) {
            Log.e("SDK_LW_CMM", e.getMessage(), e);
            return "";
        }
    }

    public static String getPackageName(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).packageName;
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("SDK_LW_CMM", e.getMessage(), e);
            return "";
        }
    }

    public static String getSDKVersion() {
        return "S2.1";
    }

    public static String getIMSI(String str, Context context) {
        byte[] bArr = null;
        if (str != null) {
            try {
                if (!"".equals(str)) {
                    if (str.length() != 15) {
                        str = DeviceUuidFactory.getInstance().getUuid(context);
                    }
                    bArr = str.getBytes("UTF-8");
                    return MD5.bytes2hex(MD5.md5(bArr));
                }
            } catch (UnsupportedEncodingException e) {
                Log.e("SDK_LW_CMM", e.getMessage(), e);
            }
        }
        return "";
    }

    public static String getIMSI(Context context) {
        String subscriberId = ((TelephonyManager) context.getSystemService("phone")).getSubscriberId();
        byte[] bArr = null;
        if (subscriberId != null) {
            try {
                if (!"".equals(subscriberId)) {
                    if (subscriberId.length() != 15) {
                        subscriberId = DeviceUuidFactory.getInstance().getUuid(context);
                    }
                    bArr = subscriberId.getBytes("UTF-8");
                    return MD5.bytes2hex(MD5.md5(bArr));
                }
            } catch (UnsupportedEncodingException e) {
                Log.e("SDK_LW_CMM", e.getMessage(), e);
            }
        }
        return "";
    }

    public static String getIMSIbyFile(Context context) {
        String fromZIP = XZip.fromZIP(context);
        if (!fromZIP.equalsIgnoreCase(WeiboAuthException.DEFAULT_AUTH_ERROR_CODE) && fromZIP.trim().length() > 0) {
            return fromZIP;
        }
        String fromZIP2 = XZip.fromZIP(context, 0);
        return (fromZIP2.equalsIgnoreCase(WeiboAuthException.DEFAULT_AUTH_ERROR_CODE) || fromZIP2.trim().length() <= 0) ? "" : fromZIP2;
    }

    public static String getToken(Context context) {
        return PreferenceUtil.getToken(context);
    }
}
