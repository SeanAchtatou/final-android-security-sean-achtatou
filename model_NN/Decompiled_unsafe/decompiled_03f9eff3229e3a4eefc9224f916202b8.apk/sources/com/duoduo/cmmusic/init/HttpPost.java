package com.duoduo.cmmusic.init;

import android.content.Context;
import android.util.Log;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import org.apache.mina.proxy.handlers.http.HttpProxyConstants;

public final class HttpPost {
    public static String httpConnection(Context context, String str, String str2, ArrayList<String> arrayList) throws IOException {
        Log.d("requestString", str2);
        byte[] bytes = str2.getBytes("UTF-8");
        Log.i("httpConnection", "address----" + str);
        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
        httpURLConnection.setDoOutput(true);
        httpURLConnection.setDoInput(true);
        httpURLConnection.setConnectTimeout(30000);
        httpURLConnection.setReadTimeout(30000);
        httpURLConnection.setUseCaches(false);
        httpURLConnection.setRequestMethod("POST");
        if (isTokenExist(context)) {
            setTokenAuthorization(context, httpURLConnection);
        } else if (GetAppInfo.getIMSIbyFile(context).trim().length() != 0) {
            setIMSIAuthorization(context, httpURLConnection);
        } else {
            setAuthorization(context, httpURLConnection);
        }
        httpURLConnection.setRequestProperty("Content-length", new StringBuilder().append(bytes.length).toString());
        httpURLConnection.setRequestProperty("Accept", "*/*");
        httpURLConnection.setRequestProperty("Content-Type", "*/*");
        httpURLConnection.setRequestProperty("Connection", "Keep-Alive");
        httpURLConnection.setRequestProperty("Charset", "UTF-8");
        httpURLConnection.setRequestProperty("Token", PreferenceUtil.getToken(context));
        httpURLConnection.setRequestProperty("excode", GetAppInfo.getexCode(context));
        Log.d("Authorization", httpURLConnection.getRequestProperty("Authorization"));
        Log.i("httpConnection", "output before");
        OutputStream outputStream = httpURLConnection.getOutputStream();
        outputStream.write(bytes);
        outputStream.flush();
        Log.i("httpConnection", "output flush");
        outputStream.close();
        int responseCode = httpURLConnection.getResponseCode();
        Log.i("httpConnection", "responseCode-----" + responseCode);
        if (200 != responseCode) {
            return null;
        }
        InputStream inputStream = httpURLConnection.getInputStream();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] bArr = new byte[1024];
        while (true) {
            int read = inputStream.read(bArr, 0, 1024);
            if (read == -1) {
                byteArrayOutputStream.toByteArray();
                Log.d(str, "responseBody------------\r\n" + new String(byteArrayOutputStream.toByteArray(), "UTF-8"));
                return new String(byteArrayOutputStream.toByteArray(), "UTF-8");
            }
            byteArrayOutputStream.write(bArr, 0, read);
        }
    }

    static InputStream httpConnection1(Context context, String str, String str2) throws IOException {
        Log.d("requestString", str2);
        byte[] bytes = str2.getBytes("UTF-8");
        Log.i("httpConnection", "address----" + str);
        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
        httpURLConnection.setDoOutput(true);
        httpURLConnection.setDoInput(true);
        httpURLConnection.setConnectTimeout(30000);
        httpURLConnection.setReadTimeout(30000);
        httpURLConnection.setUseCaches(false);
        httpURLConnection.setRequestMethod("POST");
        if (isTokenExist(context)) {
            setTokenAuthorization(context, httpURLConnection);
        } else if (GetAppInfo.getIMSIbyFile(context).trim().length() != 0) {
            setIMSIAuthorization(context, httpURLConnection);
        } else {
            setAuthorization(context, httpURLConnection);
        }
        httpURLConnection.setRequestProperty("Content-length", new StringBuilder().append(bytes.length).toString());
        httpURLConnection.setRequestProperty("Accept", "*/*");
        httpURLConnection.setRequestProperty("Content-Type", "*/*");
        httpURLConnection.setRequestProperty("Connection", "Keep-Alive");
        httpURLConnection.setRequestProperty("Charset", "UTF-8");
        httpURLConnection.setRequestProperty("Token", PreferenceUtil.getToken(context));
        httpURLConnection.setRequestProperty("excode", GetAppInfo.getexCode(context));
        Log.d("excode", "excode = " + httpURLConnection.getRequestProperty("excode"));
        Log.d("Authorization", httpURLConnection.getRequestProperty("Authorization"));
        Log.i("httpConnection", "output before");
        OutputStream outputStream = httpURLConnection.getOutputStream();
        outputStream.write(bytes);
        outputStream.flush();
        Log.i("httpConnection", "output flush");
        outputStream.close();
        int responseCode = httpURLConnection.getResponseCode();
        Log.i("httpConnection", "responseCode-----" + responseCode);
        if (200 != responseCode) {
            return null;
        }
        InputStream inputStream = httpURLConnection.getInputStream();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] bArr = new byte[1024];
        while (true) {
            int read = inputStream.read(bArr, 0, 1024);
            if (read == -1) {
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                Log.d(str, "responseBody------------\r\n" + new String(byteArrayOutputStream.toByteArray(), "UTF-8"));
                InputStream byte2InputStream = PullXMLTool.byte2InputStream(byteArray);
                new String(byteArrayOutputStream.toByteArray(), "UTF-8");
                return byte2InputStream;
            }
            byteArrayOutputStream.write(bArr, 0, read);
        }
    }

    private static void setIMSIAuthorization(Context context, HttpURLConnection httpURLConnection) {
        httpURLConnection.addRequestProperty("Authorization", "OEPAUTH realm=\"OEP\",IMSI=\"" + GetAppInfo.getIMSIbyFile(context) + "\",appID=\"" + GetAppInfoInterface.getAppid(context) + "\",pubKey=\"" + GetAppInfoInterface.getSign(context) + "\",netMode=\"" + GetAppInfoInterface.getNetMode(context) + "\",packageName=\"" + GetAppInfoInterface.getPackageName(context) + "\",version=\"" + "S2.1" + "\",excode=\"" + GetAppInfo.getexCode(context) + "\"");
        System.out.println("===============================================================");
        System.out.println("imsi\r\n" + GetAppInfoInterface.getIMSI(context) + HttpProxyConstants.CRLF);
        System.out.println("sign\r\n" + GetAppInfoInterface.getSign(context) + HttpProxyConstants.CRLF);
        System.out.println("appid\r\n" + GetAppInfoInterface.getAppid(context) + HttpProxyConstants.CRLF);
        System.out.println("netmode\r\n" + GetAppInfoInterface.getNetMode(context) + HttpProxyConstants.CRLF);
        System.out.println("packagename\r\n" + GetAppInfoInterface.getPackageName(context) + HttpProxyConstants.CRLF);
        System.out.println("sdkversion\r\nS2.1\r\n");
        System.out.println("excode\r\n" + GetAppInfo.getexCode(context) + HttpProxyConstants.CRLF);
        System.out.println("===============================================================");
    }

    private static void setAuthorization(Context context, HttpURLConnection httpURLConnection) {
        httpURLConnection.addRequestProperty("Authorization", "OEPAUTH realm=\"OEP\",IMSI=\"\",appID=\"" + GetAppInfoInterface.getAppid(context) + "\",pubKey=\"" + GetAppInfoInterface.getSign(context) + "\",netMode=\"" + GetAppInfoInterface.getNetMode(context) + "\",packageName=\"" + GetAppInfoInterface.getPackageName(context) + "\",version=\"" + "S2.1" + "\",excode=\"" + GetAppInfo.getexCode(context) + "\"");
        System.out.println("===============================================================");
        System.out.println("sign\r\n" + GetAppInfoInterface.getSign(context) + HttpProxyConstants.CRLF);
        System.out.println("appid\r\n" + GetAppInfoInterface.getAppid(context) + HttpProxyConstants.CRLF);
        System.out.println("netmode\r\n" + GetAppInfoInterface.getNetMode(context) + HttpProxyConstants.CRLF);
        System.out.println("packagename\r\n" + GetAppInfoInterface.getPackageName(context) + HttpProxyConstants.CRLF);
        System.out.println("sdkversion\r\nS2.1\r\n");
        System.out.println("excode\r\n" + GetAppInfo.getexCode(context) + HttpProxyConstants.CRLF);
        System.out.println("===============================================================");
    }

    private static void setTokenAuthorization(Context context, HttpURLConnection httpURLConnection) {
        httpURLConnection.addRequestProperty("Authorization", "OEPAUTH realm=\"OEP\",Token=\"" + PreferenceUtil.getToken(context) + "\",appID=\"" + GetAppInfoInterface.getAppid(context) + "\",pubKey=\"" + GetAppInfoInterface.getSign(context) + "\",netMode=\"" + GetAppInfoInterface.getNetMode(context) + "\",packageName=\"" + GetAppInfoInterface.getPackageName(context) + "\",version=\"" + "S2.1" + "\",excode=\"" + GetAppInfo.getexCode(context) + "\"");
        System.out.println("===============================================================");
        System.out.println("token\r\n" + PreferenceUtil.getToken(context) + HttpProxyConstants.CRLF);
        System.out.println("sign\r\n" + GetAppInfoInterface.getSign(context) + HttpProxyConstants.CRLF);
        System.out.println("appid\r\n" + GetAppInfoInterface.getAppid(context) + HttpProxyConstants.CRLF);
        System.out.println("netmode\r\n" + GetAppInfoInterface.getNetMode(context) + HttpProxyConstants.CRLF);
        System.out.println("packagename\r\n" + GetAppInfoInterface.getPackageName(context) + HttpProxyConstants.CRLF);
        System.out.println("sdkversion\r\nS2.1\r\n");
        System.out.println("excode\r\n" + GetAppInfo.getexCode(context) + HttpProxyConstants.CRLF);
        System.out.println("===============================================================");
    }

    static boolean isTokenExist(Context context) {
        return PreferenceUtil.getToken(context).length() > 0;
    }
}
