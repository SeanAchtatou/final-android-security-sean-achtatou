package com.duoduo.cmmusic.init;

import android.content.Context;
import android.util.Log;
import cn.banshenggua.aichang.utils.StringUtil;
import com.sina.weibo.sdk.exception.WeiboAuthException;
import com.tencent.open.SocialConstants;
import java.io.IOException;
import java.util.Hashtable;
import org.xmlpull.v1.XmlPullParserException;

public class InitCmmInterface {
    public static Hashtable<String, String> initCmmEnv(Context context) {
        long currentTimeMillis = System.currentTimeMillis();
        long time = PreferenceUtil.getTime(context);
        long cycleBeginTime = PreferenceUtil.getCycleBeginTime(context);
        if (cycleBeginTime != 0 && currentTimeMillis - cycleBeginTime >= LogBuilder.MAX_INTERVAL) {
            PreferenceUtil.saveLimitTim(context, 0);
            PreferenceUtil.saveCycleBeginTim(context, 0);
            Constants.countMap.put("initCount", 0);
        }
        if (time == 0 || currentTimeMillis - time >= StatisticConfig.MIN_UPLOAD_INTERVAL) {
            PreferenceUtil.saveTime(context, currentTimeMillis);
            try {
                Log.i("SDK_LW_CMM", "init 22");
                return InitCmm3.initCmm(context);
            } catch (Throwable th) {
                Log.i("SDK_LW_CMM", "init 11 exception");
                Hashtable<String, String> hashtable = new Hashtable<>();
                hashtable.put("code", WeiboAuthException.DEFAULT_AUTH_ERROR_CODE);
                hashtable.put(SocialConstants.PARAM_APP_DESC, "未知错误");
                return hashtable;
            }
        } else {
            Hashtable<String, String> hashtable2 = new Hashtable<>();
            hashtable2.put("code", "5");
            hashtable2.put(SocialConstants.PARAM_APP_DESC, "初始化函数调用间隔不能小于30s");
            return hashtable2;
        }
    }

    public static boolean initCheck(Context context) {
        try {
            Log.i("SDK_LW_CMM", "check 22");
            return InitCmm3.initCheck(context);
        } catch (Throwable th) {
            Log.i("SDK_LW_CMM", "check 11 exception");
            return false;
        }
    }

    public static int simWhichConnected(Context context) {
        try {
            Log.i("SDK_LW_CMM", "simWhichConnected 22");
            return NetMode.simWhichConnected(context);
        } catch (Throwable th) {
            Log.i("SDK_LW_CMM", "simWhichConnected 11");
            return 0;
        }
    }

    public static Result getValidateCode(Context context, String str) throws IOException, XmlPullParserException {
        return PullXMLTool.getResult(HttpPost.httpConnection1(context, "http://218.200.227.123:90/wapServer/1.0/crbt/getValidateCode", Utils.buildRequsetXml("<MSISDN>" + str + "</MSISDN>")));
    }

    public static Result smsLoginAuth(Context context, String str, String str2) throws IOException, XmlPullParserException {
        SmsLoginAuthResult smsLoginAuthResult = PullXMLTool.getSmsLoginAuthResult(HttpPost.httpConnection1(context, "http://218.200.227.123:90/wapServer/1.0/crbt/smsLoginAuth", Utils.buildRequsetXml("<MSISDN>" + str + "</MSISDN><smsCode>" + str2 + "</smsCode>")));
        if (smsLoginAuthResult != null && "000000".equals(smsLoginAuthResult.getResCode())) {
            PreferenceUtil.saveToken(context, new String(smsLoginAuthResult.getToken().getBytes(), StringUtil.Encoding));
            Result result = new Result();
            result.setResCode(smsLoginAuthResult.getResCode());
            result.setResMsg(smsLoginAuthResult.getResMsg());
            return result;
        } else if (smsLoginAuthResult == null) {
            return null;
        } else {
            Result result2 = new Result();
            result2.setResCode(smsLoginAuthResult.getResCode());
            result2.setResMsg(smsLoginAuthResult.getResMsg());
            return result2;
        }
    }
}
