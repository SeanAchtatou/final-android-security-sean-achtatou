package com.duoduo.cmmusic.init;

import android.content.Context;
import android.util.Log;
import java.util.regex.Pattern;

class Utils {
    Utils() {
    }

    static String subString(String str) {
        return Pattern.compile("\\s*|\t|\r|\n").matcher(str).replaceAll("");
    }

    static void smsCount(Context context) {
        int intValue = Constants.countMap.get("initCount").intValue() + 1;
        Log.i("SDK_LW_CMM", "---------------------------------one cycle SMS send count=" + intValue);
        if (intValue == 1) {
            PreferenceUtil.saveCycleBeginTim(context, System.currentTimeMillis());
        }
        Constants.countMap.put("initCount", Integer.valueOf(intValue));
    }

    public static String buildRequsetXml(String str) {
        return "<?xml version='1.0' encoding='UTF-8'?><request>" + str + "</request>";
    }
}
