package com.duoduo.cmmusic.init;

import android.content.Context;
import android.content.SharedPreferences;
import java.util.UUID;

public class DeviceUuidFactory {
    protected static final String PREFS_DEVICE_ID = "device_id";
    protected static final String PREFS_FILE = "device_id.xml";
    protected static UUID uuid;
    protected static DeviceUuidFactory uuidFactory;
    protected String strUUID;

    public static DeviceUuidFactory getInstance() {
        if (uuidFactory != null) {
            return uuidFactory;
        }
        DeviceUuidFactory deviceUuidFactory = new DeviceUuidFactory();
        uuidFactory = deviceUuidFactory;
        return deviceUuidFactory;
    }

    public String getUuid(Context context) {
        if (uuid == null) {
            synchronized (DeviceUuidFactory.class) {
                if (uuid == null) {
                    SharedPreferences sharedPreferences = context.getSharedPreferences(PREFS_FILE, 0);
                    String string = sharedPreferences.getString(PREFS_DEVICE_ID, null);
                    if (string != null) {
                        uuid = UUID.fromString(string);
                    } else {
                        uuid = UUID.randomUUID();
                        sharedPreferences.edit().putString(PREFS_DEVICE_ID, uuid.toString()).commit();
                    }
                }
            }
        }
        this.strUUID = uuid.toString().replace("-", "");
        return this.strUUID;
    }
}
