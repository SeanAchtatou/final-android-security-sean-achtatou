package com.duoduo.cmmusic.init;

import android.app.PendingIntent;
import android.content.Context;
import android.net.ConnectivityManager;
import android.provider.Settings;
import android.telephony.TelephonyManager;

public class DualSimUtils {
    public static String getImsi(Context context, int i) throws NoSuchMethodException {
        String str;
        boolean z = false;
        String str2 = null;
        boolean z2 = true;
        try {
            z = true;
            str2 = (String) Class.forName("android.telephony.TelephonyManager").getMethod("getSubscriberIdGemini", Integer.TYPE).invoke((TelephonyManager) context.getSystemService("phone"), Integer.valueOf(i));
        } catch (Error | Exception e) {
        }
        if (str2 == null) {
            try {
                Class<?> cls = Class.forName("android.telephony.MSimTelephonyManager");
                z = true;
                str2 = (String) cls.getMethod("getSubscriberId", Integer.TYPE).invoke(cls.getMethod("getDefault", null).invoke(null, null), Integer.valueOf(i));
            } catch (Error | Exception e2) {
            }
        }
        if (str2 == null) {
            try {
                Class<?> cls2 = Class.forName("com.mediatek.telephony.TelephonyManagerEx");
                z = true;
                str2 = (String) cls2.getMethod("getSubscriberId", Integer.TYPE).invoke(cls2.getMethod("getDefault", null).invoke(null, null), Integer.valueOf(i));
            } catch (Error | Exception e3) {
            }
        }
        if (str2 == null) {
            try {
                Class<?> cls3 = Class.forName("android.telephony.TelephonyManager");
                str = (String) cls3.getMethod("getSubscriberId", null).invoke(cls3.getMethod("getDefault", Integer.TYPE).invoke(null, Integer.valueOf(i)), null);
            } catch (Exception e4) {
                z2 = z;
                str = str2;
            } catch (Error e5) {
                z2 = z;
                str = str2;
            }
        } else {
            z2 = z;
            str = str2;
        }
        if (str == null) {
            str = "";
        }
        if (z2) {
            return str;
        }
        throw new NoSuchMethodException();
    }

    public static int getDefaultSim(Context context) throws NoSuchMethodException {
        try {
            return ((Integer) Class.forName("android.net.NetworkInfo").getMethod("getSimId", null).invoke(((ConnectivityManager) context.getSystemService("connectivity")).getNetworkInfo(0), null)).intValue();
        } catch (Error | Exception e) {
            try {
                Class<?> cls = Class.forName("android.telephony.MSimTelephonyManager");
                return ((Integer) cls.getMethod("getPreferredDataSubscription", null).invoke(cls.getMethod("getDefault", null).invoke(null, null), null)).intValue();
            } catch (Error | Exception e2) {
                try {
                    Class<?> cls2 = Class.forName("android.telephony.MSimTelephonyManager");
                    return ((Integer) cls2.getMethod("getDefaultSubscription", null).invoke(cls2.getMethod("getDefault", null).invoke(null, null), null)).intValue();
                } catch (Error | Exception e3) {
                    try {
                        Class<?> cls3 = Class.forName("android.telephony.TelephonyManager");
                        Object invoke = cls3.getMethod("getDefault", new Class[0]).invoke(null, null);
                        return ((Integer) cls3.getMethod("getDefaultSim", Context.class, Integer.TYPE).invoke(invoke, context, 1)).intValue();
                    } catch (Error | Exception e4) {
                        try {
                            Class<?> cls4 = Class.forName("android.telephony.TelephonyManager");
                            return ((Integer) cls4.getMethod("getSmsDefaultSim", null).invoke(cls4.getMethod("getDefault", new Class[0]).invoke(null, null), null)).intValue();
                        } catch (Error | Exception e5) {
                            try {
                                if (Settings.System.getInt(context.getContentResolver(), "gprs_connection_setting", -4) >= 0) {
                                }
                                return 0;
                            } catch (Error | Exception e6) {
                                throw new NoSuchMethodException();
                            }
                        }
                    }
                }
            }
        }
    }

    public static void sendTextMessage(String str, String str2, String str3, PendingIntent pendingIntent, PendingIntent pendingIntent2, int i) throws NoSuchMethodException {
        try {
            Class<?> cls = Class.forName("android.telephony.gemini.GeminiSmsManager");
            cls.getMethod("sendTextMessageGemini", String.class, String.class, String.class, Integer.TYPE, PendingIntent.class, PendingIntent.class).invoke(cls, str, str2, str3, Integer.valueOf(i), pendingIntent, pendingIntent2);
        } catch (Error | Exception e) {
            try {
                Class<?> cls2 = Class.forName("android.telephony.MSimSmsManager");
                Object invoke = cls2.getMethod("getDefault", null).invoke(null, null);
                cls2.getMethod("sendTextMessage", String.class, String.class, String.class, PendingIntent.class, PendingIntent.class, Integer.TYPE).invoke(invoke, str, str2, str3, pendingIntent, pendingIntent2, Integer.valueOf(i));
            } catch (Error | Exception e2) {
                try {
                    Class<?> cls3 = Class.forName("android.telephony.MSimSmsManager");
                    cls3.getMethod("sendTextMessage", String.class, String.class, String.class, PendingIntent.class, PendingIntent.class, Integer.TYPE).invoke(cls3.newInstance(), str, str2, str3, pendingIntent, pendingIntent2, Integer.valueOf(i));
                } catch (Error | Exception e3) {
                    try {
                        Class<?> cls4 = Class.forName("android.telephony.SmsManager");
                        Object invoke2 = cls4.getDeclaredMethod("getDefault", Integer.TYPE).invoke(null, Integer.valueOf(i));
                        cls4.getMethod("sendTextMessage", String.class, String.class, String.class, PendingIntent.class, PendingIntent.class).invoke(invoke2, str, str2, str3, pendingIntent, pendingIntent2);
                    } catch (Error | Exception e4) {
                        try {
                            Class<?> cls5 = Class.forName("com.mediatek.telephony.SmsManager");
                            cls5.getMethod("sendTextMessage", String.class, String.class, String.class, Integer.TYPE, PendingIntent.class, PendingIntent.class).invoke(cls5.newInstance(), str, str2, str3, Integer.valueOf(i), pendingIntent, pendingIntent2);
                        } catch (Error | Exception e5) {
                            try {
                                Class<?> cls6 = Class.forName("com.mediatek.telephony.SmsManagerEx");
                                Object invoke3 = cls6.getMethod("getDefault", null).invoke(null, null);
                                cls6.getMethod("sendTextMessage", String.class, String.class, String.class, PendingIntent.class, PendingIntent.class, Integer.TYPE).invoke(invoke3, str, str2, str3, pendingIntent, pendingIntent2, Integer.valueOf(i));
                            } catch (Error | Exception e6) {
                                throw new NoSuchMethodException();
                            }
                        }
                    }
                }
            }
        }
    }
}
