package com.duoduo.cmmusic.init;

public class Result {
    private String resCode;
    private String resMsg;

    public String getResCode() {
        return this.resCode;
    }

    public void setResCode(String str) {
        this.resCode = str;
    }

    public String getResMsg() {
        return this.resMsg;
    }

    public void setResMsg(String str) {
        this.resMsg = str;
    }

    public String toString() {
        return "Result [resCode=" + this.resCode + ", resMsg=" + this.resMsg + "]";
    }
}
