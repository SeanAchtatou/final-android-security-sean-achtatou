package com.duoduo.cmmusic.init;

import android.content.Context;
import android.os.Build;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import com.tencent.connect.common.Constants;
import com.tencent.open.SocialConstants;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.util.Hashtable;

class InitCmm1 {
    static int counter;
    static boolean flag;

    InitCmm1() {
    }

    static boolean initCheck(Context context) {
        File file = new File("/data/data/" + GetAppInfo.getPackageName(context) + "/" + "cmsc.si");
        String imsi = GetAppInfo.getIMSI(context);
        if (imsi == null || "".equals(imsi)) {
            Log.i("SDK_LW_CMM", "no sim");
            return false;
        } else if (file.exists()) {
            if (GetAppInfo.getIMSI(context).equals(XZip.fromZIP(context))) {
                Log.i("SDK_LW_CMM", "not need initialize ...");
                return true;
            }
            Log.i("SDK_LW_CMM", "sim is changed");
            return false;
        } else if ("0".equals(httpUrlConnection(context, NetMode.WIFIorMOBILE(context), "http://218.200.227.123:90/wapServer/checksmsinitreturn").get("code"))) {
            XZip.toZIP(context, GetAppInfo.getIMSI(context));
            Log.i("SDK_LW_CMM", "server have pid");
            return true;
        } else {
            Log.i("SDK_LW_CMM", "server have no pid");
            return false;
        }
    }

    static Hashtable<String, String> initCmm(Context context) {
        String str = Build.MODEL;
        String str2 = Build.VERSION.RELEASE;
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        String deviceId = telephonyManager.getDeviceId();
        String subscriberId = telephonyManager.getSubscriberId();
        Log.i("SDK_LW_CMM", "===========CMO_S_lightweight Version_1.0.0_20130108.sc===========");
        Log.i("SDK_LW_CMM", "initCmm calling");
        Log.i("SDK_LW_CMM", "appID=" + GetAppInfo.getAppid(context));
        Log.i("SDK_LW_CMM", "devicemodel=" + str + ", deviceID=" + deviceId + ", release=" + str2 + ", subscriberID=" + subscriberId);
        if ("sdk".equals(str)) {
            Log.i("SDK_LW_CMM", "google_sdk...模拟器运行...not apn setting");
        }
        if (subscriberId == null || "".equals(subscriberId)) {
            Hashtable<String, String> hashtable = new Hashtable<>();
            hashtable.put("code", "4");
            hashtable.put(SocialConstants.PARAM_APP_DESC, "无SIM卡，双卡手机请将中国移动sim插入主卡槽");
            return hashtable;
        }
        String substring = subscriberId.substring(3, 5);
        if (!"00".equals(substring) && !"02".equals(substring) && !"07".equals(substring)) {
            Hashtable<String, String> hashtable2 = new Hashtable<>();
            hashtable2.put("code", "3");
            hashtable2.put(SocialConstants.PARAM_APP_DESC, "请使用中国移动SIM卡，双卡手机请将中国移动sim插入主卡槽");
            return hashtable2;
        } else if (!new File("/data/data/" + GetAppInfo.getPackageName(context) + "/" + "cmsc.si").exists()) {
            Hashtable<String, String> httpUrlConnection = httpUrlConnection(context, NetMode.WIFIorMOBILE(context), "http://218.200.227.123:90/wapServer/checksmsinitreturn");
            if ("0".equals(httpUrlConnection.get("code"))) {
                Log.i("SDK_LW_CMM", "server heve pid");
                XZip.toZIP(context, GetAppInfo.getIMSI(context));
                return httpUrlConnection;
            }
            Log.i("SDK_LW_CMM", "server heve no pid, initiating");
            Hashtable<String, String> init2 = init2(context);
            if (!"0".equals(init2.get("code"))) {
                return init2;
            }
            XZip.toZIP(context, GetAppInfo.getIMSI(context));
            Log.i("SDK_LW_CMM", "init success");
            return init2;
        } else if (GetAppInfo.getIMSI(context).equals(XZip.fromZIP(context))) {
            Log.i("SDK_LW_CMM", "the same file");
            Log.i("SDK_LW_CMM", "init success");
            Hashtable<String, String> hashtable3 = new Hashtable<>();
            hashtable3.put("code", "0");
            hashtable3.put(SocialConstants.PARAM_APP_DESC, "初始化成功");
            return hashtable3;
        } else {
            Log.i("SDK_LW_CMM", "difference file");
            Log.i("SDK_LW_CMM", "sim is changed, initiating");
            Hashtable<String, String> init22 = init2(context);
            if (!"0".equals(init22.get("code"))) {
                return init22;
            }
            XZip.toZIP(context, GetAppInfo.getIMSI(context));
            Log.i("SDK_LW_CMM", "init success");
            return init22;
        }
    }

    static Hashtable<String, String> init2(Context context) {
        String WIFIorMOBILE = NetMode.WIFIorMOBILE(context);
        Hashtable<String, String> hashtable = new Hashtable<>();
        if ("CMWAP".equals(WIFIorMOBILE)) {
            Log.i("SDK_LW_CMM", "netmode cmwap");
            return initCMWAP(context, WIFIorMOBILE);
        } else if ("CMNET".equals(WIFIorMOBILE)) {
            Log.i("SDK_LW_CMM", "netmode cmnet ");
            Hashtable<String, String> initCMNETWAP = initCMNETWAP(context, WIFIorMOBILE);
            if (initCMNETWAP == null || !"0".equals(initCMNETWAP.get("code"))) {
                return initSMS(context, WIFIorMOBILE);
            }
            return initCMNETWAP;
        } else if ("WIFI".equals(WIFIorMOBILE)) {
            Log.i("SDK_LW_CMM", "netmode wifi");
            return initSMS(context, WIFIorMOBILE);
        } else {
            Log.i("SDK_LW_CMM", "netmode--" + WIFIorMOBILE);
            hashtable.put("code", "2");
            hashtable.put(SocialConstants.PARAM_APP_DESC, "请检查网络连接是否可以访问公网或是cmwap/cmnet/wifi方式联网");
            return hashtable;
        }
    }

    static Hashtable<String, String> initCMNETWAP(Context context, String str) {
        return cmnetHttpUrlConnectionwap(context, str, "http://218.200.227.123:90/wapServer/wapinit2");
    }

    static Hashtable<String, String> initCMWAP(Context context, String str) {
        return httpUrlConnectionwap(context, str, "http://218.200.227.123:90/wapServer/wapinit2");
    }

    static Hashtable<String, String> initSMS(Context context, String str) {
        if (Constants.countMap.get("initCount").intValue() >= 3) {
            Hashtable<String, String> hashtable = new Hashtable<>();
            hashtable.put("code", Constants.VIA_SHARE_TYPE_INFO);
            hashtable.put(SocialConstants.PARAM_APP_DESC, "在24小时内短信初始化调用次数不能超过3次。");
            return hashtable;
        }
        SmsManager.getDefault().sendTextMessage("1065843601", null, "CMO_S=" + GetAppInfo.getIMSI(context) + "@" + GetAppInfo.getAppid(context) + "@" + GetAppInfo.getSDKVersion() + "@" + GetAppInfo.getexCode(context), null, null);
        Utils.smsCount(context);
        Log.i("SDK_LW_CMM", "sendSMS sleep");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            Log.e("SDK_LW_CMM", e.getMessage(), e);
        }
        flag = false;
        counter = 0;
        Hashtable<String, String> hashtable2 = new Hashtable<>();
        while (!flag) {
            counter++;
            Log.i("SDK_LW_CMM", "initSMS " + counter);
            Hashtable<String, String> httpUrlConnection = httpUrlConnection(context, str, "http://218.200.227.123:90/wapServer/checksmsinitreturn");
            if ("0".equals(httpUrlConnection.get("code"))) {
                flag = true;
                return httpUrlConnection;
            } else if (counter >= 3) {
                flag = true;
                return httpUrlConnection;
            } else {
                try {
                    Thread.sleep(5000);
                    hashtable2 = httpUrlConnection;
                } catch (InterruptedException e2) {
                    Log.e("SDK_LW_CMM", e2.getMessage(), e2);
                    hashtable2 = httpUrlConnection;
                }
            }
        }
        return hashtable2;
    }

    static Hashtable<String, String> httpUrlConnection(Context context, String str, String str2) {
        Hashtable<String, String> hashtable = new Hashtable<>();
        try {
            Log.i("SDK_LW_CMM", "url------------" + str2);
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str2).openConnection();
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            httpURLConnection.setConnectTimeout(30000);
            httpURLConnection.setReadTimeout(30000);
            httpURLConnection.setUseCaches(false);
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.addRequestProperty("Authorization", "OEPAUTH realm=\"OEP\",IMSI=\"" + GetAppInfo.getIMSI(context) + "\",appID=\"" + GetAppInfo.getAppid(context) + "\",pubKey=\"" + GetAppInfo.getSign(context) + "\",netMode=\"" + str + "\",packageName=\"" + GetAppInfo.getPackageName(context) + "\",version=\"" + GetAppInfo.getSDKVersion() + "\",excode=\"" + GetAppInfo.getexCode(context) + "\"");
            byte[] bytes = "<?xml version='1.0' encoding='UTF-8'?><request><request>".getBytes("UTF-8");
            httpURLConnection.setRequestProperty("Content-length", new StringBuilder().append(bytes.length).toString());
            httpURLConnection.setRequestProperty("Accept", "*/*");
            httpURLConnection.setRequestProperty("Content-Type", "*/*");
            httpURLConnection.setRequestProperty("Connection", "Keep-Alive");
            httpURLConnection.setRequestProperty("Accept-Charset", "UTF-8");
            OutputStream outputStream = httpURLConnection.getOutputStream();
            outputStream.write(bytes);
            outputStream.flush();
            int responseCode = httpURLConnection.getResponseCode();
            Log.i("SDK_LW_CMM", "-----" + responseCode);
            if (200 == responseCode) {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                byte[] bArr = new byte[1024];
                while (true) {
                    int read = httpURLConnection.getInputStream().read(bArr, 0, 1024);
                    if (read == -1) {
                        break;
                    }
                    byteArrayOutputStream.write(bArr, 0, read);
                }
                String str3 = new String(byteArrayOutputStream.toByteArray(), "UTF-8");
                String pull2Result = PullXMLTool.pull2Result(PullXMLTool.byte2InputStream(str3.getBytes("UTF-8")));
                String pull2ResultDesc = PullXMLTool.pull2ResultDesc(PullXMLTool.byte2InputStream(str3.getBytes("UTF-8")));
                hashtable.put("code", pull2Result);
                hashtable.put(SocialConstants.PARAM_APP_DESC, pull2ResultDesc);
            }
            return hashtable;
        } catch (Exception e) {
            Log.e("SDK_LW_CMM", e.getMessage(), e);
            hashtable.put("code", "-2");
            hashtable.put(SocialConstants.PARAM_APP_DESC, "connection timeout and so on（网络不通，初始化失败，稍后再试）");
            return hashtable;
        }
    }

    static Hashtable<String, String> httpUrlConnectionwap(Context context, String str, String str2) {
        Hashtable<String, String> hashtable = new Hashtable<>();
        Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("10.0.0.172", 80));
        try {
            Log.i("SDK_LW_CMM", "url------------" + str2);
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str2).openConnection(proxy);
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            httpURLConnection.setConnectTimeout(30000);
            httpURLConnection.setReadTimeout(30000);
            httpURLConnection.setUseCaches(false);
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.addRequestProperty("Authorization", "OEPAUTH realm=\"OEP\",IMSI=\"" + GetAppInfo.getIMSI(context) + "\",appID=\"" + GetAppInfo.getAppid(context) + "\",pubKey=\"" + GetAppInfo.getSign(context) + "\",netMode=\"" + str + "\",packageName=\"" + GetAppInfo.getPackageName(context) + "\",version=\"" + GetAppInfo.getSDKVersion() + "\",excode=\"" + GetAppInfo.getexCode(context) + "\"");
            byte[] bytes = "<?xml version='1.0' encoding='UTF-8'?><request><request>".getBytes("UTF-8");
            httpURLConnection.setRequestProperty("Content-length", new StringBuilder().append(bytes.length).toString());
            httpURLConnection.setRequestProperty("Accept", "*/*");
            httpURLConnection.setRequestProperty("Content-Type", "*/*");
            httpURLConnection.setRequestProperty("Connection", "Keep-Alive");
            httpURLConnection.setRequestProperty("Accept-Charset", "UTF-8");
            OutputStream outputStream = httpURLConnection.getOutputStream();
            outputStream.write(bytes);
            outputStream.flush();
            int responseCode = httpURLConnection.getResponseCode();
            Log.i("SDK_LW_CMM", "-----" + responseCode);
            if (200 == responseCode) {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                byte[] bArr = new byte[1024];
                while (true) {
                    int read = httpURLConnection.getInputStream().read(bArr, 0, 1024);
                    if (read == -1) {
                        break;
                    }
                    byteArrayOutputStream.write(bArr, 0, read);
                }
                String str3 = new String(byteArrayOutputStream.toByteArray(), "UTF-8");
                String pull2Result = PullXMLTool.pull2Result(PullXMLTool.byte2InputStream(str3.getBytes("UTF-8")));
                String pull2ResultDesc = PullXMLTool.pull2ResultDesc(PullXMLTool.byte2InputStream(str3.getBytes("UTF-8")));
                hashtable.put("code", pull2Result);
                hashtable.put(SocialConstants.PARAM_APP_DESC, pull2ResultDesc);
            }
            return hashtable;
        } catch (Exception e) {
            Log.e("SDK_LW_CMM", e.getMessage(), e);
            hashtable.put("code", "-2");
            hashtable.put(SocialConstants.PARAM_APP_DESC, "connection timeout and so on（网络不通，初始化失败，稍后再试）");
            return hashtable;
        }
    }

    static Hashtable<String, String> cmnetHttpUrlConnectionwap(Context context, String str, String str2) {
        Hashtable<String, String> hashtable = new Hashtable<>();
        try {
            Log.i("SDK_LW_CMM", "url------------" + str2);
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str2).openConnection();
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            httpURLConnection.setConnectTimeout(30000);
            httpURLConnection.setReadTimeout(30000);
            httpURLConnection.setUseCaches(false);
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.addRequestProperty("Authorization", "OEPAUTH realm=\"OEP\",IMSI=\"" + GetAppInfo.getIMSI(context) + "\",appID=\"" + GetAppInfo.getAppid(context) + "\",pubKey=\"" + GetAppInfo.getSign(context) + "\",netMode=\"" + str + "\",packageName=\"" + GetAppInfo.getPackageName(context) + "\",version=\"" + GetAppInfo.getSDKVersion() + "\",excode=\"" + GetAppInfo.getexCode(context) + "\"");
            byte[] bytes = "<?xml version='1.0' encoding='UTF-8'?><request><request>".getBytes("UTF-8");
            httpURLConnection.setRequestProperty("Content-length", new StringBuilder().append(bytes.length).toString());
            httpURLConnection.setRequestProperty("Accept", "*/*");
            httpURLConnection.setRequestProperty("Content-Type", "*/*");
            httpURLConnection.setRequestProperty("Connection", "Keep-Alive");
            httpURLConnection.setRequestProperty("Accept-Charset", "UTF-8");
            OutputStream outputStream = httpURLConnection.getOutputStream();
            outputStream.write(bytes);
            outputStream.flush();
            int responseCode = httpURLConnection.getResponseCode();
            Log.i("SDK_LW_CMM", "-----" + responseCode);
            if (200 == responseCode) {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                byte[] bArr = new byte[1024];
                while (true) {
                    int read = httpURLConnection.getInputStream().read(bArr, 0, 1024);
                    if (read == -1) {
                        break;
                    }
                    byteArrayOutputStream.write(bArr, 0, read);
                }
                String str3 = new String(byteArrayOutputStream.toByteArray(), "UTF-8");
                String pull2Result = PullXMLTool.pull2Result(PullXMLTool.byte2InputStream(str3.getBytes("UTF-8")));
                String pull2ResultDesc = PullXMLTool.pull2ResultDesc(PullXMLTool.byte2InputStream(str3.getBytes("UTF-8")));
                hashtable.put("code", pull2Result);
                hashtable.put(SocialConstants.PARAM_APP_DESC, pull2ResultDesc);
            }
            return hashtable;
        } catch (Exception e) {
            Log.e("SDK_LW_CMM", e.getMessage(), e);
            hashtable.put("code", "-2");
            hashtable.put(SocialConstants.PARAM_APP_DESC, "connection timeout and so on（网络不通，初始化失败，稍后再试）");
            return hashtable;
        }
    }
}
