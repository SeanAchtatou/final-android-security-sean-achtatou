package com.duoduo.cmmusic.init;

import java.security.MessageDigest;

class MD5 {
    private static MessageDigest md5;

    MD5() {
    }

    static {
        md5 = null;
        try {
            md5 = MessageDigest.getInstance("MD5");
        } catch (Exception e) {
        }
    }

    static byte[] md5(byte[] bArr) {
        byte[] digest;
        synchronized (md5) {
            digest = md5.digest(bArr);
        }
        return digest;
    }

    static String bytes2hex(byte[] bArr) {
        StringBuffer stringBuffer = new StringBuffer(bArr.length * 2);
        for (byte hexString : bArr) {
            String upperCase = Integer.toHexString(hexString).toUpperCase();
            if (upperCase.length() > 2) {
                upperCase = upperCase.substring(6, 8);
            } else if (upperCase.length() == 1) {
                upperCase = "0" + upperCase;
            }
            stringBuffer.append(upperCase);
            stringBuffer.append("");
        }
        return stringBuffer.toString();
    }
}
