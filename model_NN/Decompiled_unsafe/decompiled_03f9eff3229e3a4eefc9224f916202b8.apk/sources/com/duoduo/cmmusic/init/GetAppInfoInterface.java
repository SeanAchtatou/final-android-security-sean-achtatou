package com.duoduo.cmmusic.init;

import android.content.Context;

public class GetAppInfoInterface {
    public static String getAppid(Context context) {
        return GetAppInfo.getAppid(context);
    }

    public static String getSign(Context context) {
        return GetAppInfo.getSign(context);
    }

    public static String getPackageName(Context context) {
        return GetAppInfo.getPackageName(context);
    }

    public static String getSDKVersion() {
        return GetAppInfo.getSDKVersion();
    }

    public static String getNetMode(Context context) {
        return NetMode.WIFIorMOBILE(context);
    }

    public static String getIMSI(Context context) {
        return GetAppInfo.getIMSI(context);
    }

    public static String getIMSI2(String str, Context context) {
        return GetAppInfo.getIMSI(str, context);
    }

    public static String getToken(Context context) {
        return GetAppInfo.getToken(context);
    }
}
