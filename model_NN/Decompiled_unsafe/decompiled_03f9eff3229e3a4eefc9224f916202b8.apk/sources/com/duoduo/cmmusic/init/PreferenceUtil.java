package com.duoduo.cmmusic.init;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

final class PreferenceUtil {
    private static final String CYCLE_BEGIN_TIME = "CYCLE_BEGIN_TIME";
    private static final String INIT_TIME = "INIT_TIME";
    private static final String LIMIT_TIME = "LIMIT_TIME";
    private static final String TOKEN = "TOKEN";
    private static final String TOKEN_TIME = "TOKEN_TIME";

    PreferenceUtil() {
    }

    private static SharedPreferences getPreferences(Context context) {
        return context.getSharedPreferences(context.getApplicationInfo().name, 0);
    }

    public static long getTokenTime(Context context) {
        SharedPreferences preferences = getPreferences(context);
        if (preferences == null) {
            return 0;
        }
        return preferences.getLong(TOKEN_TIME, 0);
    }

    public static void saveTokenTime(Context context, long j) {
        SharedPreferences preferences = getPreferences(context);
        if (preferences != null) {
            SharedPreferences.Editor edit = preferences.edit();
            edit.putLong(TOKEN_TIME, j);
            edit.commit();
        }
    }

    static String getToken(Context context) {
        long currentTimeMillis = System.currentTimeMillis();
        long tokenTime = getTokenTime(context);
        if (tokenTime == 0 || currentTimeMillis - tokenTime <= LogBuilder.MAX_INTERVAL) {
            SharedPreferences preferences = getPreferences(context);
            if (preferences == null) {
                return "";
            }
            return preferences.getString(TOKEN, "");
        }
        Log.i("TAG", "preTime too long");
        return "";
    }

    static void saveToken(Context context, String str) {
        saveTokenTime(context, System.currentTimeMillis());
        SharedPreferences preferences = getPreferences(context);
        if (preferences != null) {
            SharedPreferences.Editor edit = preferences.edit();
            edit.putString(TOKEN, str);
            edit.commit();
        }
    }

    static long getTime(Context context) {
        SharedPreferences preferences = getPreferences(context);
        if (preferences == null) {
            return 0;
        }
        return preferences.getLong(INIT_TIME, 0);
    }

    static void saveTime(Context context, long j) {
        SharedPreferences preferences = getPreferences(context);
        if (preferences != null) {
            SharedPreferences.Editor edit = preferences.edit();
            edit.putLong(INIT_TIME, j);
            edit.commit();
        }
    }

    static long getLimitTime(Context context) {
        SharedPreferences preferences = getPreferences(context);
        if (preferences == null) {
            return 0;
        }
        return preferences.getLong(LIMIT_TIME, 0);
    }

    static void saveLimitTim(Context context, long j) {
        SharedPreferences preferences = getPreferences(context);
        if (preferences != null) {
            SharedPreferences.Editor edit = preferences.edit();
            edit.putLong(LIMIT_TIME, j);
            edit.commit();
        }
    }

    static long getCycleBeginTime(Context context) {
        SharedPreferences preferences = getPreferences(context);
        if (preferences == null) {
            return 0;
        }
        return preferences.getLong(CYCLE_BEGIN_TIME, 0);
    }

    static void saveCycleBeginTim(Context context, long j) {
        SharedPreferences preferences = getPreferences(context);
        if (preferences != null) {
            SharedPreferences.Editor edit = preferences.edit();
            edit.putLong(CYCLE_BEGIN_TIME, j);
            edit.commit();
        }
    }
}
