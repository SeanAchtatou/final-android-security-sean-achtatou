package com.duoduo.cmmusic.init;

import android.content.Context;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.Log;
import com.tencent.connect.common.Constants;
import com.tencent.open.SocialConstants;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.util.Hashtable;

class InitCmm3 {
    static int counter;
    static boolean flag;

    InitCmm3() {
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x008d  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00d6  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00e3  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static boolean initCheck(android.content.Context r12) throws java.lang.IllegalArgumentException, java.lang.SecurityException, java.lang.IllegalAccessException, java.lang.reflect.InvocationTargetException, java.lang.NoSuchMethodException {
        /*
            r11 = 5
            r10 = 3
            r2 = 1
            r1 = 0
            java.lang.String r0 = "phone"
            java.lang.Object r0 = r12.getSystemService(r0)
            android.telephony.TelephonyManager r0 = (android.telephony.TelephonyManager) r0
            boolean r7 = com.duoduo.cmmusic.init.NetMode.simInserted(r12, r1)
            boolean r9 = com.duoduo.cmmusic.init.NetMode.simInserted(r12, r2)
            java.lang.String r0 = "SDK_LW_CMM"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "isInserted sim0:"
            r3.<init>(r4)
            java.lang.StringBuilder r3 = r3.append(r7)
            java.lang.String r4 = "_sim1:"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r3 = r3.append(r9)
            java.lang.String r3 = r3.toString()
            android.util.Log.i(r0, r3)
            if (r7 != 0) goto L_0x003c
            if (r9 != 0) goto L_0x003c
            java.lang.NoSuchMethodException r0 = new java.lang.NoSuchMethodException
            r0.<init>()
            throw r0
        L_0x003c:
            java.lang.String r6 = ""
            java.lang.String r4 = ""
            java.lang.String r5 = ""
            java.lang.String r3 = ""
            java.lang.String r0 = ""
            if (r7 == 0) goto L_0x0167
            java.lang.String r6 = com.duoduo.cmmusic.init.DualSimUtils.getImsi(r12, r1)
            if (r6 == 0) goto L_0x0052
            java.lang.String r0 = r6.substring(r10, r11)
        L_0x0052:
            java.lang.String r7 = "00"
            boolean r7 = r7.equals(r0)
            if (r7 != 0) goto L_0x006a
            java.lang.String r7 = "02"
            boolean r7 = r7.equals(r0)
            if (r7 != 0) goto L_0x006a
            java.lang.String r7 = "07"
            boolean r7 = r7.equals(r0)
            if (r7 == 0) goto L_0x0167
        L_0x006a:
            java.lang.String r5 = "SDK_LW_CMM"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            java.lang.String r8 = "initCheck sim"
            r7.<init>(r8)
            java.lang.StringBuilder r7 = r7.append(r1)
            java.lang.String r8 = "_imsi:"
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.StringBuilder r7 = r7.append(r6)
            java.lang.String r7 = r7.toString()
            android.util.Log.i(r5, r7)
            r7 = r6
            r8 = r6
            r6 = r2
        L_0x008b:
            if (r9 == 0) goto L_0x0161
            java.lang.String r4 = com.duoduo.cmmusic.init.DualSimUtils.getImsi(r12, r2)
            if (r4 == 0) goto L_0x0097
            java.lang.String r0 = r4.substring(r10, r11)
        L_0x0097:
            java.lang.String r5 = "00"
            boolean r5 = r5.equals(r0)
            if (r5 != 0) goto L_0x00af
            java.lang.String r5 = "02"
            boolean r5 = r5.equals(r0)
            if (r5 != 0) goto L_0x00af
            java.lang.String r5 = "07"
            boolean r0 = r5.equals(r0)
            if (r0 == 0) goto L_0x0161
        L_0x00af:
            java.lang.String r0 = "SDK_LW_CMM"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r5 = "initCheck sim"
            r3.<init>(r5)
            java.lang.StringBuilder r3 = r3.append(r2)
            java.lang.String r5 = "_imsi:"
            java.lang.StringBuilder r3 = r3.append(r5)
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            android.util.Log.i(r0, r3)
            r0 = r2
            r3 = r2
            r5 = r4
        L_0x00d0:
            boolean r5 = r8.equals(r5)
            if (r5 == 0) goto L_0x00e3
            java.lang.String r0 = "SDK_LW_CMM"
            java.lang.String r1 = "sima equals simb conver to singlesim!"
            android.util.Log.i(r0, r1)
            java.lang.NoSuchMethodException r0 = new java.lang.NoSuchMethodException
            r0.<init>()
            throw r0
        L_0x00e3:
            if (r6 != 0) goto L_0x00f4
            if (r0 != 0) goto L_0x00f4
            java.lang.String r0 = "SDK_LW_CMM"
            java.lang.String r1 = "no CM_SIM"
            android.util.Log.i(r0, r1)
            java.lang.NoSuchMethodException r0 = new java.lang.NoSuchMethodException
            r0.<init>()
            throw r0
        L_0x00f4:
            java.lang.String r0 = "SDK_LW_CMM"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            java.lang.String r6 = "which_"
            r5.<init>(r6)
            java.lang.StringBuilder r5 = r5.append(r1)
            java.lang.String r6 = ";imsi:"
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.StringBuilder r5 = r5.append(r7)
            java.lang.String r5 = r5.toString()
            android.util.Log.i(r0, r5)
            boolean r0 = initCheck1(r12, r1, r7)
            java.lang.String r1 = "SDK_LW_CMM"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            java.lang.String r6 = "which_"
            r5.<init>(r6)
            java.lang.StringBuilder r5 = r5.append(r3)
            java.lang.String r6 = ";imsi:"
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.StringBuilder r5 = r5.append(r4)
            java.lang.String r5 = r5.toString()
            android.util.Log.i(r1, r5)
            boolean r1 = initCheck1(r12, r3, r4)
            java.lang.String r3 = "SDK_LW_CMM"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r5 = "initCheck result: sim0_"
            r4.<init>(r5)
            java.lang.StringBuilder r4 = r4.append(r0)
            java.lang.String r5 = "-----sim1_"
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r4 = r4.append(r1)
            java.lang.String r4 = r4.toString()
            android.util.Log.i(r3, r4)
            if (r0 != 0) goto L_0x0160
            if (r1 != 0) goto L_0x0160
            java.lang.NoSuchMethodException r0 = new java.lang.NoSuchMethodException
            r0.<init>()
            throw r0
        L_0x0160:
            return r2
        L_0x0161:
            r0 = r1
            r5 = r4
            r4 = r3
            r3 = r1
            goto L_0x00d0
        L_0x0167:
            r7 = r5
            r8 = r6
            r6 = r1
            goto L_0x008b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.duoduo.cmmusic.init.InitCmm3.initCheck(android.content.Context):boolean");
    }

    static boolean initCheck1(Context context, int i, String str) {
        File file;
        if ("".equals(str)) {
            return false;
        }
        if (i == 0) {
            file = new File("/data/data/" + GetAppInfo.getPackageName(context) + "/" + "cmsc.si");
        } else {
            file = new File("/data/data/" + GetAppInfo.getPackageName(context) + "/" + "cmsc.si");
        }
        if (file.exists()) {
            if (GetAppInfo.getIMSI(str, context).equals(XZip.fromZIP(context, i))) {
                Log.i("SDK_LW_CMM", "not need initialize ...");
                return true;
            }
            Log.i("SDK_LW_CMM", "sim is changed");
            return false;
        } else if ("0".equals(httpUrlConnection(context, NetMode.WIFIorMOBILE(context), "http://218.200.227.123:90/wapServer/checksmsinitreturn", str).get("code"))) {
            XZip.toZIP(context, GetAppInfo.getIMSI(str, context), i);
            Log.i("SDK_LW_CMM", "server have pid");
            return true;
        } else {
            Log.i("SDK_LW_CMM", "server have no pid");
            return false;
        }
    }

    static Hashtable<String, String> initCmm(Context context) throws IllegalArgumentException, SecurityException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        String str = Build.MODEL;
        String str2 = Build.VERSION.RELEASE;
        String deviceId = ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
        boolean simInserted = NetMode.simInserted(context, 0);
        boolean simInserted2 = NetMode.simInserted(context, 1);
        String str3 = "";
        String str4 = "";
        if (simInserted && (str3 = DualSimUtils.getImsi(context, 0)) == null) {
            Log.i("SDK_LW_CMM", "sim1 exist, but null");
            str3 = "";
        }
        if (simInserted2 && (str4 = DualSimUtils.getImsi(context, 1)) == null) {
            Log.i("SDK_LW_CMM", "sim2 exist, but null");
            str4 = "";
        }
        Log.i("SDK_LW_CMM", "===========CMO_S_lightweight_doublesim Version_1.0.0_20130218.sc===========");
        Log.i("SDK_LW_CMM", "initCmm calling");
        Log.i("SDK_LW_CMM", "appID=" + GetAppInfo.getAppid(context));
        Log.i("SDK_LW_CMM", "devicemodel=" + str + ", deviceID=" + deviceId + ", release=" + str2 + ", subscriberID=" + str3 + "_" + str4);
        if ("sdk".equals(str)) {
            Log.i("SDK_LW_CMM", "google_sdk...模拟器运行...not apn setting");
        }
        if ("".equals(str3) && "".equals(str4)) {
            throw new NoSuchMethodException();
        } else if (!str3.equals(str4)) {
            return init1(context, str3, str4);
        } else {
            Log.i("SDK_LW_CMM", "sima equals simb conver to singlesim!");
            throw new NoSuchMethodException();
        }
    }

    static Hashtable<String, String> init1(Context context, String str, String str2) throws IllegalArgumentException, SecurityException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        if (!"".equals(str)) {
            if (!"".equals(str2)) {
                Log.i("SDK_LW_CMM", "sim sim");
                boolean isCmCard = isCmCard(str);
                boolean isCmCard2 = isCmCard(str2);
                if (!isCmCard || !isCmCard2) {
                    Hashtable<String, String> init15 = init15(context, str, 0);
                    Hashtable<String, String> init152 = init15(context, str2, 1);
                    Log.i("SDK_LW_CMM", "code-0:" + init15.get("code"));
                    Log.i("SDK_LW_CMM", "code-1:" + init152.get("code"));
                    if ("0".equals(init15.get("code"))) {
                        init15.put("detail", String.valueOf(init15.get("code")) + " " + init152.get("code"));
                        return init15;
                    } else if ("0".equals(init152.get("code"))) {
                        init152.put("detail", String.valueOf(init15.get("code")) + " " + init152.get("code"));
                        return init152;
                    } else {
                        Hashtable<String, String> hashtable = new Hashtable<>();
                        hashtable.put("code", Constants.VIA_REPORT_TYPE_WPA_STATE);
                        hashtable.put("detail", String.valueOf(init15.get("code")) + " " + init152.get("code"));
                        hashtable.put(SocialConstants.PARAM_APP_DESC, "双卡槽初始化都失败（可从detail字段查询每个卡槽失败的code）");
                        return hashtable;
                    }
                } else {
                    Hashtable<String, String> hashtable2 = new Hashtable<>();
                    hashtable2.put("code", Constants.VIA_REPORT_TYPE_START_WAP);
                    hashtable2.put(SocialConstants.PARAM_APP_DESC, "您有两张移动SIM卡，请选择其中一张进行初始化！");
                    return hashtable2;
                }
            } else {
                Log.i("SDK_LW_CMM", "sim null");
                return init15(context, str, 0);
            }
        } else if (!"".equals(str2)) {
            Log.i("SDK_LW_CMM", "null sim");
            return init15(context, str2, 1);
        } else {
            Log.i("SDK_LW_CMM", "null null");
            Hashtable<String, String> hashtable3 = new Hashtable<>();
            hashtable3.put("code", "4");
            hashtable3.put(SocialConstants.PARAM_APP_DESC, "无sim卡");
            return hashtable3;
        }
    }

    static Hashtable<String, String> init15(Context context, String str, int i) throws IllegalArgumentException, SecurityException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        String substring = str.substring(3, 5);
        if ("00".equals(substring) || "02".equals(substring) || "07".equals(substring)) {
            File file = null;
            if (i == 0) {
                file = new File("/data/data/" + GetAppInfo.getPackageName(context) + "/" + "cmsc.si");
            }
            if (i == 1) {
                file = new File("/data/data/" + GetAppInfo.getPackageName(context) + "/" + "cmsc.si");
            }
            if (!file.exists()) {
                Hashtable<String, String> httpUrlConnection = httpUrlConnection(context, NetMode.WIFIorMOBILE(context), "http://218.200.227.123:90/wapServer/checksmsinitreturn", str);
                if ("0".equals(httpUrlConnection.get("code"))) {
                    Log.i("SDK_LW_CMM", "server heve pid");
                    XZip.toZIP(context, GetAppInfo.getIMSI(str, context), i);
                    return httpUrlConnection;
                }
                Log.i("SDK_LW_CMM", "server heve no pid, initiating");
                Hashtable<String, String> init2 = init2(context, str, i);
                if (!"0".equals(init2.get("code"))) {
                    return init2;
                }
                XZip.toZIP(context, GetAppInfo.getIMSI(str, context), i);
                Log.i("SDK_LW_CMM", "init success");
                return init2;
            } else if (GetAppInfo.getIMSI(str, context).equals(XZip.fromZIP(context, i))) {
                Log.i("SDK_LW_CMM", "the same file");
                Log.i("SDK_LW_CMM", "init success");
                Hashtable<String, String> hashtable = new Hashtable<>();
                hashtable.put("code", "0");
                hashtable.put(SocialConstants.PARAM_APP_DESC, "初始化成功");
                return hashtable;
            } else {
                Log.i("SDK_LW_CMM", "difference file");
                Log.i("SDK_LW_CMM", "sim is changed, initiating");
                Hashtable<String, String> init22 = init2(context, str, i);
                if (!"0".equals(init22.get("code"))) {
                    return init22;
                }
                XZip.toZIP(context, GetAppInfo.getIMSI(str, context), i);
                Log.i("SDK_LW_CMM", "init success");
                return init22;
            }
        } else {
            Hashtable<String, String> hashtable2 = new Hashtable<>();
            hashtable2.put("code", "3");
            hashtable2.put(SocialConstants.PARAM_APP_DESC, "非中国移动SIM卡");
            return hashtable2;
        }
    }

    static Hashtable<String, String> init2(Context context, String str, int i) throws IllegalArgumentException, SecurityException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        String WIFIorMOBILE = NetMode.WIFIorMOBILE(context);
        Hashtable<String, String> hashtable = new Hashtable<>();
        if ("CMWAP".equals(WIFIorMOBILE)) {
            if (NetMode.simWhichConnected(context) == i) {
                Log.i("SDK_LW_CMM", "netmode cmwap on_" + i);
                return initCMWAP(context, WIFIorMOBILE, str);
            }
            Log.i("SDK_LW_CMM", "netmode cmwap");
            return initSMS(context, WIFIorMOBILE, str, i);
        } else if ("CMNET".equals(WIFIorMOBILE)) {
            Log.i("SDK_LW_CMM", "netmode cmnet");
            if (NetMode.simWhichConnected(context) != i) {
                return initSMS(context, WIFIorMOBILE, str, i);
            }
            Log.i("SDK_LW_CMM", "netmode cmnet on_" + i);
            Hashtable<String, String> initCMNETWAP = initCMNETWAP(context, WIFIorMOBILE, str);
            if (initCMNETWAP == null || !"0".equals(initCMNETWAP.get("code"))) {
                return initSMS(context, WIFIorMOBILE, str, i);
            }
            return initCMNETWAP;
        } else if ("WIFI".equals(WIFIorMOBILE) || "OTHER".equals(WIFIorMOBILE)) {
            Log.i("SDK_LW_CMM", "netmode wifi other");
            return initSMS(context, WIFIorMOBILE, str, i);
        } else {
            Log.i("SDK_LW_CMM", "netmode--" + WIFIorMOBILE);
            hashtable.put("code", "2");
            hashtable.put(SocialConstants.PARAM_APP_DESC, "请检查网络连接");
            return hashtable;
        }
    }

    static Hashtable<String, String> initCMNETWAP(Context context, String str, String str2) {
        return cmnetHttpUrlConnectionwap(context, str, "http://218.200.227.123:90/wapServer/wapinit2", str2);
    }

    static Hashtable<String, String> initCMWAP(Context context, String str, String str2) {
        return httpUrlConnectionwap(context, str, "http://218.200.227.123:90/wapServer/wapinit2", str2);
    }

    static Hashtable<String, String> initSMS(Context context, String str, String str2, int i) throws NoSuchMethodException {
        if (Constants.countMap.get("initCount").intValue() >= 3) {
            Hashtable<String, String> hashtable = new Hashtable<>();
            hashtable.put("code", Constants.VIA_SHARE_TYPE_INFO);
            hashtable.put(SocialConstants.PARAM_APP_DESC, "在24小时内短信初始化调用次数不能超过3次。");
            return hashtable;
        }
        DualSimUtils.sendTextMessage("1065843601", null, "CMO_S=" + GetAppInfo.getIMSI(str2, context) + "@" + GetAppInfo.getAppid(context) + "@" + GetAppInfo.getSDKVersion() + "@" + GetAppInfo.getexCode(context), null, null, i);
        Utils.smsCount(context);
        Log.i("SDK_LW_CMM", "sendSMS sleep");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            Log.e("SDK_LW_CMM", e.getMessage(), e);
        }
        flag = false;
        counter = 0;
        Hashtable<String, String> hashtable2 = new Hashtable<>();
        while (!flag) {
            counter++;
            Log.i("SDK_LW_CMM", "initSMS " + counter);
            Hashtable<String, String> httpUrlConnection = httpUrlConnection(context, str, "http://218.200.227.123:90/wapServer/checksmsinitreturn", str2);
            if ("0".equals(httpUrlConnection.get("code"))) {
                flag = true;
                return httpUrlConnection;
            } else if (counter >= 3) {
                flag = true;
                return httpUrlConnection;
            } else {
                try {
                    Thread.sleep(5000);
                    hashtable2 = httpUrlConnection;
                } catch (InterruptedException e2) {
                    Log.e("SDK_LW_CMM", e2.getMessage(), e2);
                    hashtable2 = httpUrlConnection;
                }
            }
        }
        return hashtable2;
    }

    static Hashtable<String, String> httpUrlConnection(Context context, String str, String str2, String str3) {
        Hashtable<String, String> hashtable = new Hashtable<>();
        try {
            Log.i("SDK_LW_CMM", "url------------" + str2);
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str2).openConnection();
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            httpURLConnection.setConnectTimeout(30000);
            httpURLConnection.setReadTimeout(30000);
            httpURLConnection.setUseCaches(false);
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.addRequestProperty("Authorization", "OEPAUTH realm=\"OEP\",IMSI=\"" + GetAppInfo.getIMSI(str3, context) + "\",appID=\"" + GetAppInfo.getAppid(context) + "\",pubKey=\"" + GetAppInfo.getSign(context) + "\",netMode=\"" + str + "\",packageName=\"" + GetAppInfo.getPackageName(context) + "\",version=\"" + GetAppInfo.getSDKVersion() + "\",excode=\"" + GetAppInfo.getexCode(context) + "\"");
            byte[] bytes = "<?xml version='1.0' encoding='UTF-8'?><request><request>".getBytes("UTF-8");
            httpURLConnection.setRequestProperty("Content-length", new StringBuilder().append(bytes.length).toString());
            httpURLConnection.setRequestProperty("Accept", "*/*");
            httpURLConnection.setRequestProperty("Content-Type", "*/*");
            httpURLConnection.setRequestProperty("Connection", "Keep-Alive");
            httpURLConnection.setRequestProperty("Accept-Charset", "UTF-8");
            OutputStream outputStream = httpURLConnection.getOutputStream();
            outputStream.write(bytes);
            outputStream.flush();
            int responseCode = httpURLConnection.getResponseCode();
            Log.i("SDK_LW_CMM", "-----" + responseCode);
            if (200 == responseCode) {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                byte[] bArr = new byte[1024];
                while (true) {
                    int read = httpURLConnection.getInputStream().read(bArr, 0, 1024);
                    if (read == -1) {
                        break;
                    }
                    byteArrayOutputStream.write(bArr, 0, read);
                }
                String str4 = new String(byteArrayOutputStream.toByteArray(), "UTF-8");
                String pull2Result = PullXMLTool.pull2Result(PullXMLTool.byte2InputStream(str4.getBytes("UTF-8")));
                String pull2ResultDesc = PullXMLTool.pull2ResultDesc(PullXMLTool.byte2InputStream(str4.getBytes("UTF-8")));
                hashtable.put("code", pull2Result);
                hashtable.put(SocialConstants.PARAM_APP_DESC, pull2ResultDesc);
            }
            return hashtable;
        } catch (Exception e) {
            Log.e("SDK_LW_CMM", e.getMessage(), e);
            hashtable.put("code", "-2");
            hashtable.put(SocialConstants.PARAM_APP_DESC, "connection timeout and so on（网络不通，初始化失败，稍后再试）");
            return hashtable;
        }
    }

    static Hashtable<String, String> httpUrlConnectionwap(Context context, String str, String str2, String str3) {
        Hashtable<String, String> hashtable = new Hashtable<>();
        Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("10.0.0.172", 80));
        try {
            Log.i("SDK_LW_CMM", "url------------" + str2);
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str2).openConnection(proxy);
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            httpURLConnection.setConnectTimeout(30000);
            httpURLConnection.setReadTimeout(30000);
            httpURLConnection.setUseCaches(false);
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.addRequestProperty("Authorization", "OEPAUTH realm=\"OEP\",IMSI=\"" + GetAppInfo.getIMSI(str3, context) + "\",appID=\"" + GetAppInfo.getAppid(context) + "\",pubKey=\"" + GetAppInfo.getSign(context) + "\",netMode=\"" + str + "\",packageName=\"" + GetAppInfo.getPackageName(context) + "\",version=\"" + GetAppInfo.getSDKVersion() + "\",excode=\"" + GetAppInfo.getexCode(context) + "\"");
            byte[] bytes = "<?xml version='1.0' encoding='UTF-8'?><request><request>".getBytes("UTF-8");
            httpURLConnection.setRequestProperty("Content-length", new StringBuilder().append(bytes.length).toString());
            httpURLConnection.setRequestProperty("Accept", "*/*");
            httpURLConnection.setRequestProperty("Content-Type", "*/*");
            httpURLConnection.setRequestProperty("Connection", "Keep-Alive");
            httpURLConnection.setRequestProperty("Accept-Charset", "UTF-8");
            OutputStream outputStream = httpURLConnection.getOutputStream();
            outputStream.write(bytes);
            outputStream.flush();
            int responseCode = httpURLConnection.getResponseCode();
            Log.i("SDK_LW_CMM", "-----" + responseCode);
            if (200 == responseCode) {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                byte[] bArr = new byte[1024];
                while (true) {
                    int read = httpURLConnection.getInputStream().read(bArr, 0, 1024);
                    if (read == -1) {
                        break;
                    }
                    byteArrayOutputStream.write(bArr, 0, read);
                }
                String str4 = new String(byteArrayOutputStream.toByteArray(), "UTF-8");
                String pull2Result = PullXMLTool.pull2Result(PullXMLTool.byte2InputStream(str4.getBytes("UTF-8")));
                String pull2ResultDesc = PullXMLTool.pull2ResultDesc(PullXMLTool.byte2InputStream(str4.getBytes("UTF-8")));
                hashtable.put("code", pull2Result);
                hashtable.put(SocialConstants.PARAM_APP_DESC, pull2ResultDesc);
            }
            return hashtable;
        } catch (Exception e) {
            Log.e("SDK_LW_CMM", e.getMessage(), e);
            hashtable.put("code", "-2");
            hashtable.put(SocialConstants.PARAM_APP_DESC, "connection timeout and so on（网络不通，初始化失败，稍后再试）");
            return hashtable;
        }
    }

    static Hashtable<String, String> cmnetHttpUrlConnectionwap(Context context, String str, String str2, String str3) {
        Hashtable<String, String> hashtable = new Hashtable<>();
        try {
            Log.i("SDK_LW_CMM", "url------------" + str2);
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str2).openConnection();
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            httpURLConnection.setConnectTimeout(30000);
            httpURLConnection.setReadTimeout(30000);
            httpURLConnection.setUseCaches(false);
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.addRequestProperty("Authorization", "OEPAUTH realm=\"OEP\",IMSI=\"" + GetAppInfo.getIMSI(str3, context) + "\",appID=\"" + GetAppInfo.getAppid(context) + "\",pubKey=\"" + GetAppInfo.getSign(context) + "\",netMode=\"" + str + "\",packageName=\"" + GetAppInfo.getPackageName(context) + "\",version=\"" + GetAppInfo.getSDKVersion() + "\",excode=\"" + GetAppInfo.getexCode(context) + "\"");
            byte[] bytes = "<?xml version='1.0' encoding='UTF-8'?><request><request>".getBytes("UTF-8");
            httpURLConnection.setRequestProperty("Content-length", new StringBuilder().append(bytes.length).toString());
            httpURLConnection.setRequestProperty("Accept", "*/*");
            httpURLConnection.setRequestProperty("Content-Type", "*/*");
            httpURLConnection.setRequestProperty("Connection", "Keep-Alive");
            httpURLConnection.setRequestProperty("Accept-Charset", "UTF-8");
            OutputStream outputStream = httpURLConnection.getOutputStream();
            outputStream.write(bytes);
            outputStream.flush();
            int responseCode = httpURLConnection.getResponseCode();
            Log.i("SDK_LW_CMM", "-----" + responseCode);
            if (200 == responseCode) {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                byte[] bArr = new byte[1024];
                while (true) {
                    int read = httpURLConnection.getInputStream().read(bArr, 0, 1024);
                    if (read == -1) {
                        break;
                    }
                    byteArrayOutputStream.write(bArr, 0, read);
                }
                String str4 = new String(byteArrayOutputStream.toByteArray(), "UTF-8");
                String pull2Result = PullXMLTool.pull2Result(PullXMLTool.byte2InputStream(str4.getBytes("UTF-8")));
                String pull2ResultDesc = PullXMLTool.pull2ResultDesc(PullXMLTool.byte2InputStream(str4.getBytes("UTF-8")));
                hashtable.put("code", pull2Result);
                hashtable.put(SocialConstants.PARAM_APP_DESC, pull2ResultDesc);
            }
            return hashtable;
        } catch (Exception e) {
            Log.e("SDK_LW_CMM", e.getMessage(), e);
            hashtable.put("code", "-2");
            hashtable.put(SocialConstants.PARAM_APP_DESC, "connection timeout and so on（网络不通，初始化失败，稍后再试）");
            return hashtable;
        }
    }

    private static boolean isCmCard(String str) {
        String substring = str.substring(3, 5);
        if ("00".equals(substring) || "02".equals(substring) || "07".equals(substring)) {
            return true;
        }
        return false;
    }
}
