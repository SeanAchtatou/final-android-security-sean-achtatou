package com.duoduo.cmmusic.init;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class Constants {
    static final String CHECK_VALIDATE_CODE_URL = "http://218.200.227.123:90/wapServer/1.0/crbt/smsLoginAuth";
    static final String CMNET = "CMNET";
    static final String CMWAP = "CMWAP";
    static final long CYCLE = 86400000;
    static final boolean DEBUG = true;
    static final String ENABLER_URL_DOMAIN = "http://218.200.227.123:90/wapServer/1.0";
    static final String ENCODE = "UTF-8";
    static final String FN = "cmsc.si";
    static final String FN0 = "cmsc.si";
    static final String FN1 = "cmsc.si";
    static final String GET_VALIDATE_CODE_URL = "http://218.200.227.123:90/wapServer/1.0/crbt/getValidateCode";
    static final String INITCOUNT = "initCount";
    static final String NIISNULL = "NIISNUll";
    static final String NOWM = "NOWM";
    static final String OTHER = "OTHER";
    static final String ROOT = "/data/data/";
    static final String SDK_VERSION = "S2.1";
    static final String SMSCODE = "1065843601";
    static final String TAG = "SDK_LW_CMM";
    static final String URL_SMSCHECK = "http://218.200.227.123:90/wapServer/checksmsinitreturn";
    static final String URL_WAP = "http://218.200.227.123:90/wapServer/wapinit2";
    static final String WIFI = "WIFI";
    static Map<String, Integer> countMap = Collections.synchronizedMap(new HashMap());
    public static SmsStage smsStage = SmsStage.Original;

    public enum SmsStage {
        Original,
        Send
    }

    static {
        countMap.put(INITCOUNT, 0);
    }
}
