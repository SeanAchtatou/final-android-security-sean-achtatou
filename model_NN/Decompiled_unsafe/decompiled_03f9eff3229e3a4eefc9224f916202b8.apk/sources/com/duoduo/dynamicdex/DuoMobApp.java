package com.duoduo.dynamicdex;

import android.app.Application;

public enum DuoMobApp {
    Ins;
    
    private Application mApp = null;

    public void init(Application application) {
        this.mApp = application;
    }

    public void onDestroy() {
    }

    public Application getApp() {
        return this.mApp;
    }
}
