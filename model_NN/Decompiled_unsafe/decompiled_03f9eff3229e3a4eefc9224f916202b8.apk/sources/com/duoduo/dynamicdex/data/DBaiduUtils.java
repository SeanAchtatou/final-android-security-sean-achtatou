package com.duoduo.dynamicdex.data;

import android.content.Context;
import android.view.ViewGroup;
import com.duoduo.dynamicdex.DuoMobApp;
import com.duoduo.dynamicdex.utils.AppSPUtils;
import com.duoduo.mobads.IAdView;
import com.duoduo.mobads.IAdViewListener;
import com.duoduo.mobads.IBaiduNative;
import com.duoduo.mobads.IBaiduNativeNetworkListener;
import com.duoduo.mobads.IInterstitialAd;
import com.duoduo.mobads.ISplashAdListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.lang.reflect.Constructor;

public class DBaiduUtils implements IAdUtils {
    private static final String KEY_SP_FILE_NAME = "key_sp_file_name";
    private Class<?> mAdViewClazz = null;
    private Class<?> mBaiduNativeClazz = null;
    private boolean mCopySuc = false;
    private Class<?> mInterstitialAdClazz = null;
    private Class<?> mSplashAdClazz = null;

    public boolean isEmpty() {
        return this.mBaiduNativeClazz == null && this.mSplashAdClazz == null && this.mAdViewClazz == null && this.mInterstitialAdClazz == null;
    }

    public void load(String str, ClassLoader classLoader) {
        if (classLoader != null) {
            try {
                this.mBaiduNativeClazz = classLoader.loadClass("com.duoduo.mobads.wrapper.BaiduNativeWrapper");
            } catch (Exception e) {
            }
            try {
                this.mSplashAdClazz = classLoader.loadClass("com.duoduo.mobads.wrapper.SplashAdWrapper");
            } catch (Exception e2) {
            }
            try {
                this.mAdViewClazz = classLoader.loadClass("com.duoduo.mobads.wrapper.AdViewWrapper");
            } catch (Exception e3) {
            }
            try {
                this.mInterstitialAdClazz = classLoader.loadClass("com.duoduo.mobads.wrapper.InterstitialAdWrapper");
            } catch (Exception e4) {
            }
            try {
                this.mCopySuc = copyJar(str, classLoader);
            } catch (Exception e5) {
            }
        }
    }

    private boolean copyJar(String str, ClassLoader classLoader) {
        boolean z;
        try {
            if (this.mBaiduNativeClazz == null) {
                return false;
            }
            File file = new File(String.valueOf(DuoMobApp.Ins.getApp().getDir("baidu_ad_sdk", 0).getAbsolutePath()) + File.separator + "__xadsdk__remote__final__builtin__.jar");
            if (str == null || !str.equals(AppSPUtils.loadPrefString(KEY_SP_FILE_NAME, ""))) {
                z = false;
            } else {
                z = true;
            }
            if (z && file.exists() && file.length() > 0) {
                return true;
            }
            InputStream resourceAsStream = this.mBaiduNativeClazz.getResourceAsStream("/assets/__xadsdk__remote__final__.jar");
            if (resourceAsStream == null) {
                return false;
            }
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            byte[] bArr = new byte[1024];
            while (true) {
                int read = resourceAsStream.read(bArr);
                if (read <= 0) {
                    resourceAsStream.close();
                    fileOutputStream.flush();
                    fileOutputStream.close();
                    AppSPUtils.savePrefString(KEY_SP_FILE_NAME, str);
                    return true;
                }
                fileOutputStream.write(bArr, 0, read);
            }
        } catch (Exception e) {
            return false;
        }
    }

    public IBaiduNative getNativeAdIns(Context context, String str, String str2, IBaiduNativeNetworkListener iBaiduNativeNetworkListener) {
        if (!isReady(this.mBaiduNativeClazz)) {
            return null;
        }
        try {
            Constructor<?> constructor = this.mBaiduNativeClazz.getConstructor(Context.class, String.class, String.class, IBaiduNativeNetworkListener.class);
            if (constructor != null) {
                return (IBaiduNative) constructor.newInstance(context, str, str2, iBaiduNativeNetworkListener);
            }
        } catch (Exception e) {
        }
        return null;
    }

    public Object getSplashAd(Context context, ViewGroup viewGroup, ISplashAdListener iSplashAdListener, String str, String str2, boolean z) {
        if (!isReady(this.mSplashAdClazz)) {
            return null;
        }
        try {
            Constructor<?> constructor = this.mSplashAdClazz.getConstructor(Context.class, ViewGroup.class, ISplashAdListener.class, String.class, String.class, Boolean.TYPE);
            if (constructor != null) {
                return constructor.newInstance(context, viewGroup, iSplashAdListener, str, str2, Boolean.valueOf(z));
            }
        } catch (Exception e) {
        }
        return null;
    }

    public IAdView getAdViewIns(Context context, String str, String str2, IAdViewListener iAdViewListener) {
        if (!isReady(this.mAdViewClazz)) {
            return null;
        }
        try {
            Constructor<?> constructor = this.mAdViewClazz.getConstructor(Context.class, IAdViewListener.class, String.class, String.class);
            if (constructor != null) {
                return (IAdView) constructor.newInstance(context, iAdViewListener, str, str2);
            }
        } catch (Exception e) {
        }
        return null;
    }

    public IInterstitialAd getInterstitialAdIns(Context context, String str, String str2) {
        if (!isReady(this.mInterstitialAdClazz)) {
            return null;
        }
        try {
            Constructor<?> constructor = this.mInterstitialAdClazz.getConstructor(Context.class, String.class, String.class);
            if (constructor != null) {
                return (IInterstitialAd) constructor.newInstance(context, str, str2);
            }
        } catch (Exception e) {
        }
        return null;
    }

    private boolean isReady(Class<?> cls) {
        return this.mCopySuc && cls != null;
    }
}
