package com.duoduo.dynamicdex.data;

import android.app.Activity;
import android.view.ViewGroup;
import com.duoduo.mobads.gdt.IGdtNativeAd;
import com.duoduo.mobads.gdt.IGdtNativeAdListener;
import com.duoduo.mobads.gdt.IGdtSplashAdListener;
import java.lang.reflect.Constructor;

public class DGdtUtils implements IAdUtils {
    private Class<?> mGdtNativeClazz = null;
    private Class<?> mSplashAdClazz = null;

    public boolean isEmpty() {
        return this.mGdtNativeClazz == null && this.mSplashAdClazz == null;
    }

    public void load(String str, ClassLoader classLoader) {
        if (classLoader != null) {
            try {
                this.mGdtNativeClazz = classLoader.loadClass("com.duoduo.mobads.wrapper.gdt.GdtNativeAdWrapper");
            } catch (Exception e) {
            }
            try {
                this.mSplashAdClazz = classLoader.loadClass("com.duoduo.mobads.wrapper.gdt.GdtAdSplashWrapper");
            } catch (Exception e2) {
            }
        }
    }

    public IGdtNativeAd getNativeAd(Activity activity, String str, String str2, IGdtNativeAdListener iGdtNativeAdListener) {
        if (this.mGdtNativeClazz == null) {
            return null;
        }
        try {
            Constructor<?> constructor = this.mGdtNativeClazz.getConstructor(Activity.class, String.class, String.class, IGdtNativeAdListener.class);
            if (constructor != null) {
                return (IGdtNativeAd) constructor.newInstance(activity, str, str2, iGdtNativeAdListener);
            }
        } catch (Exception e) {
        }
        return null;
    }

    public Object getSplashAd(Activity activity, ViewGroup viewGroup, String str, String str2, IGdtSplashAdListener iGdtSplashAdListener) {
        if (this.mSplashAdClazz == null) {
            return null;
        }
        try {
            Constructor<?> constructor = this.mSplashAdClazz.getConstructor(Activity.class, ViewGroup.class, String.class, String.class, IGdtSplashAdListener.class);
            if (constructor != null) {
                return constructor.newInstance(activity, viewGroup, str, str2, iGdtSplashAdListener);
            }
        } catch (Exception e) {
        }
        return null;
    }
}
