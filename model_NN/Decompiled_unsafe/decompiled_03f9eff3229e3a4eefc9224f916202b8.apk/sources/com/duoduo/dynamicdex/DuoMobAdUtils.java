package com.duoduo.dynamicdex;

import com.duoduo.dynamicdex.DDexLoader;
import com.duoduo.dynamicdex.data.DBaiduUtils;
import com.duoduo.dynamicdex.data.DGdtUtils;
import com.duoduo.dynamicdex.data.DToutiaoUtils;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.lang.reflect.Field;

public enum DuoMobAdUtils {
    Ins;
    
    public static final int ALL_CHANNEL = 7;
    public static final int CHANNEL_BAIDU = 1;
    public static final int CHANNEL_GDT = 4;
    public static final int CHANNEL_TOUTIAO = 2;
    public static final int ERROR_CLASS_NOT_FOUND = -2002;
    public static final int ERROR_FILE_NOT_FOUND = -2001;
    public static final int ERROR_LOAD_CLASS_FAILED = -2006;
    public static final int ERROR_MV_ASSETS_FAILED = -2005;
    public static final int ERROR_MV_BDF_FAILED = -2003;
    public static final int ERROR_ONLINE_NULL = -2004;
    public DBaiduUtils BaiduIns = new DBaiduUtils();
    public DGdtUtils GdtIns = new DGdtUtils();
    private int INIT_CHANNEL = 7;
    private String INNER_FILE_JAR = "duobaidu.jar";
    public DToutiaoUtils ToutiaoIns = new DToutiaoUtils();
    private final String VERSION = "0.0.8";
    private String mInnerSdkVer = "unknown";
    private DDexLoader.DexLoadListener mLoadListener = new DDexLoader.DexLoadListener() {
        public void loadFailed(int i) {
            if (DuoMobAdUtils.access$2(DuoMobAdUtils.this) != null) {
                DuoMobAdUtils.access$2(DuoMobAdUtils.this).loadFailed(i);
            }
        }

        public void loaded(ClassLoader classLoader) {
            boolean z;
            boolean z2;
            boolean z3 = false;
            if (classLoader == null) {
                DuoMobAdUtils.access$2(DuoMobAdUtils.this).loadFailed(DuoMobAdUtils.ERROR_CLASS_NOT_FOUND);
                return;
            }
            try {
                Class<?> loadClass = classLoader.loadClass("com.duoduo.mobads.wrapper.Constants");
                Field field = loadClass.getField("SDK_VER");
                if (field != null) {
                    DuoMobAdUtils.access$3(DuoMobAdUtils.this, field.get(loadClass).toString());
                }
            } catch (Exception e) {
            }
            if ((DuoMobAdUtils.access$4(DuoMobAdUtils.this) & 1) != 0) {
                DuoMobAdUtils.this.BaiduIns.load(DuoMobAdUtils.access$5(DuoMobAdUtils.this), classLoader);
                z = (!DuoMobAdUtils.this.BaiduIns.isEmpty()) & true;
            } else {
                z = true;
            }
            if ((DuoMobAdUtils.access$4(DuoMobAdUtils.this) & 2) != 0) {
                DuoMobAdUtils.this.ToutiaoIns.load(DuoMobAdUtils.access$5(DuoMobAdUtils.this), classLoader);
                if (DuoMobAdUtils.this.ToutiaoIns.isEmpty()) {
                    z2 = false;
                } else {
                    z2 = true;
                }
                z &= z2;
            }
            if ((DuoMobAdUtils.access$4(DuoMobAdUtils.this) & 4) != 0) {
                DuoMobAdUtils.this.GdtIns.load(DuoMobAdUtils.access$5(DuoMobAdUtils.this), classLoader);
                if (!DuoMobAdUtils.this.GdtIns.isEmpty()) {
                    z3 = true;
                }
                z &= z3;
            }
            if (!z) {
                if (DuoMobAdUtils.access$2(DuoMobAdUtils.this) != null) {
                    DuoMobAdUtils.access$2(DuoMobAdUtils.this).loadFailed(DuoMobAdUtils.ERROR_LOAD_CLASS_FAILED);
                }
                try {
                    new File(String.valueOf(DuoMobApp.Ins.getApp().getDir("duo_jar", 0).getAbsolutePath()) + File.separator + DuoMobAdUtils.access$5(DuoMobAdUtils.this)).delete();
                } catch (Exception e2) {
                }
            } else if (DuoMobAdUtils.access$2(DuoMobAdUtils.this) != null) {
                DuoMobAdUtils.access$2(DuoMobAdUtils.this).loaded();
            }
        }
    };
    private DuoMobAdPrepareListener mPrepareListener = null;

    public interface DuoMobAdPrepareListener {
        void loadFailed(int i);

        void loaded();
    }

    public String getJarVer() {
        return this.mInnerSdkVer;
    }

    public String getVer() {
        return "0.0.8";
    }

    public void prepareFmAssert(String str, String str2, DuoMobAdPrepareListener duoMobAdPrepareListener) {
        prepareFmAssert(7, str, str2, duoMobAdPrepareListener);
    }

    public void prepareFmAssert(int i, String str, String str2, DuoMobAdPrepareListener duoMobAdPrepareListener) {
        this.INIT_CHANNEL = i;
        try {
            this.INNER_FILE_JAR = str;
            String str3 = String.valueOf(DuoMobApp.Ins.getApp().getDir("duo_jar", 0).getAbsolutePath()) + File.separator + this.INNER_FILE_JAR;
            File file = new File(str3);
            if (!file.exists() || file.length() <= 0) {
                InputStream open = DuoMobApp.Ins.getApp().getAssets().open(str);
                if (open == null) {
                    if (duoMobAdPrepareListener != null) {
                        duoMobAdPrepareListener.loadFailed(ERROR_FILE_NOT_FOUND);
                    }
                    prepareFromOnline(str2, duoMobAdPrepareListener);
                    return;
                }
                FileOutputStream fileOutputStream = new FileOutputStream(file);
                byte[] bArr = new byte[1024];
                while (true) {
                    int read = open.read(bArr);
                    if (read <= 0) {
                        open.close();
                        fileOutputStream.flush();
                        fileOutputStream.close();
                        prepare(str3, duoMobAdPrepareListener);
                        return;
                    }
                    fileOutputStream.write(bArr, 0, read);
                }
            } else {
                prepare(str3, duoMobAdPrepareListener);
            }
        } catch (Exception e) {
            if (duoMobAdPrepareListener != null) {
                duoMobAdPrepareListener.loadFailed(ERROR_MV_ASSETS_FAILED);
            }
            prepareFromOnline(str2, duoMobAdPrepareListener);
        }
    }

    private void prepareFromOnline(String str, DuoMobAdPrepareListener duoMobAdPrepareListener) {
        if (str != null && str != "") {
            this.mPrepareListener = duoMobAdPrepareListener;
            DDexLoader.Ins.load(str, this.INNER_FILE_JAR, this.mLoadListener);
        } else if (duoMobAdPrepareListener != null) {
            duoMobAdPrepareListener.loadFailed(ERROR_ONLINE_NULL);
        }
    }

    public void prepare(String str, DuoMobAdPrepareListener duoMobAdPrepareListener) {
        this.mPrepareListener = duoMobAdPrepareListener;
        DDexLoader.Ins.loadFmStorage(str, this.mLoadListener);
    }
}
