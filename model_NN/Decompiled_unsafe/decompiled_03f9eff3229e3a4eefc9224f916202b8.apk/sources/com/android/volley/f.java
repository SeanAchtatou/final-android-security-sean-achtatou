package com.android.volley;

import android.os.Handler;
import java.util.concurrent.Executor;

/* compiled from: ExecutorDelivery */
public class f implements s {

    /* renamed from: a  reason: collision with root package name */
    private final Executor f132a;

    public f(Handler handler) {
        this.f132a = new g(this, handler);
    }

    public void a(n<?> nVar, r<?> rVar) {
        a(nVar, rVar, null);
    }

    public void a(n<?> nVar, r<?> rVar, Runnable runnable) {
        nVar.v();
        nVar.a("post-response");
        this.f132a.execute(new a(nVar, rVar, runnable));
    }

    public void a(n<?> nVar, w wVar) {
        nVar.a("post-error");
        this.f132a.execute(new a(nVar, r.a(wVar), null));
    }

    /* compiled from: ExecutorDelivery */
    private class a implements Runnable {
        private final n b;
        private final r c;
        private final Runnable d;

        public a(n nVar, r rVar, Runnable runnable) {
            this.b = nVar;
            this.c = rVar;
            this.d = runnable;
        }

        public void run() {
            if (this.b.h()) {
                this.b.b("canceled-at-delivery");
                return;
            }
            if (this.c.a()) {
                this.b.b((Object) this.c.f142a);
            } else {
                this.b.b(this.c.c);
            }
            if (this.c.d) {
                this.b.a("intermediate-response");
            } else {
                this.b.b("done");
            }
            if (this.d != null) {
                this.d.run();
            }
        }
    }
}
