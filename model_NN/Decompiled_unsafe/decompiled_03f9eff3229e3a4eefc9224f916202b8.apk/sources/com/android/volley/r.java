package com.android.volley;

import com.android.volley.b;

/* compiled from: Response */
public class r<T> {

    /* renamed from: a  reason: collision with root package name */
    public final T f142a;
    public final b.a b;
    public final w c;
    public boolean d;

    /* compiled from: Response */
    public interface a {
        void onErrorResponse(w wVar);
    }

    /* compiled from: Response */
    public interface b<T> {
        void onResponse(Object obj);
    }

    public static <T> r<T> a(T t, b.a aVar) {
        return new r<>(t, aVar);
    }

    public static <T> r<T> a(w wVar) {
        return new r<>(wVar);
    }

    public boolean a() {
        return this.c == null;
    }

    private r(T t, b.a aVar) {
        this.d = false;
        this.f142a = t;
        this.b = aVar;
        this.c = null;
    }

    private r(w wVar) {
        this.d = false;
        this.f142a = null;
        this.b = null;
        this.c = wVar;
    }
}
