package com.android.volley;

import android.os.Handler;
import android.os.Looper;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

/* compiled from: RequestQueue */
public class p {

    /* renamed from: a  reason: collision with root package name */
    private AtomicInteger f140a;
    private final Map<String, Queue<n>> b;
    private final Set<n> c;
    private final PriorityBlockingQueue<n> d;
    private final PriorityBlockingQueue<n> e;
    private final b f;
    private final h g;
    private final s h;
    private i[] i;
    private c j;

    /* compiled from: RequestQueue */
    public interface a {
        boolean a(n<?> nVar);
    }

    public p(b bVar, h hVar, int i2, s sVar) {
        this.f140a = new AtomicInteger();
        this.b = new HashMap();
        this.c = new HashSet();
        this.d = new PriorityBlockingQueue<>();
        this.e = new PriorityBlockingQueue<>();
        this.f = bVar;
        this.g = hVar;
        this.i = new i[i2];
        this.h = sVar;
    }

    public p(b bVar, h hVar, int i2) {
        this(bVar, hVar, i2, new f(new Handler(Looper.getMainLooper())));
    }

    public p(b bVar, h hVar) {
        this(bVar, hVar, 4);
    }

    public void a() {
        b();
        this.j = new c(this.d, this.e, this.f, this.h);
        this.j.start();
        for (int i2 = 0; i2 < this.i.length; i2++) {
            i iVar = new i(this.e, this.g, this.f, this.h);
            this.i[i2] = iVar;
            iVar.start();
        }
    }

    public void b() {
        if (this.j != null) {
            this.j.a();
        }
        for (int i2 = 0; i2 < this.i.length; i2++) {
            if (this.i[i2] != null) {
                this.i[i2].a();
            }
        }
    }

    public int c() {
        return this.f140a.incrementAndGet();
    }

    public void a(a aVar) {
        synchronized (this.c) {
            for (n next : this.c) {
                if (aVar.a(next)) {
                    next.g();
                }
            }
        }
    }

    public void a(Object obj) {
        if (obj == null) {
            throw new IllegalArgumentException("Cannot cancelAll with a null tag");
        }
        a((a) new q(this, obj));
    }

    public n a(n nVar) {
        nVar.a(this);
        synchronized (this.c) {
            this.c.add(nVar);
        }
        nVar.a(c());
        nVar.a("add-to-queue");
        if (!nVar.r()) {
            this.e.add(nVar);
        } else {
            synchronized (this.b) {
                String e2 = nVar.e();
                if (this.b.containsKey(e2)) {
                    Object obj = this.b.get(e2);
                    if (obj == null) {
                        obj = new LinkedList();
                    }
                    obj.add(nVar);
                    this.b.put(e2, obj);
                    if (x.b) {
                        x.a("Request for cacheKey=%s is in flight, putting on hold.", e2);
                    }
                } else {
                    this.b.put(e2, null);
                    this.d.add(nVar);
                }
            }
        }
        return nVar;
    }

    /* access modifiers changed from: package-private */
    public void b(n nVar) {
        synchronized (this.c) {
            this.c.remove(nVar);
        }
        if (nVar.r()) {
            synchronized (this.b) {
                String e2 = nVar.e();
                Queue remove = this.b.remove(e2);
                if (remove != null) {
                    if (x.b) {
                        x.a("Releasing %d waiting requests for cacheKey=%s.", Integer.valueOf(remove.size()), e2);
                    }
                    this.d.addAll(remove);
                }
            }
        }
    }
}
