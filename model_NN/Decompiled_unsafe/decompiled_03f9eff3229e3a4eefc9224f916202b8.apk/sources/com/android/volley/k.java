package com.android.volley;

import java.util.Map;

/* compiled from: NetworkResponse */
public class k {

    /* renamed from: a  reason: collision with root package name */
    public final int f136a;
    public final byte[] b;
    public final Map<String, String> c;
    public final boolean d;

    public k(int i, byte[] bArr, Map<String, String> map, boolean z) {
        this.f136a = i;
        this.b = bArr;
        this.c = map;
        this.d = z;
    }

    public k(byte[] bArr, Map<String, String> map) {
        this(200, bArr, map, false);
    }
}
