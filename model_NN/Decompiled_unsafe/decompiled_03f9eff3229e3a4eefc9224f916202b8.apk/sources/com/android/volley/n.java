package com.android.volley;

import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.text.TextUtils;
import com.android.volley.b;
import com.android.volley.r;
import com.android.volley.x;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Collections;
import java.util.Map;

/* compiled from: Request */
public abstract class n<T> implements Comparable<n<T>> {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final x.a f137a;
    private final int b;
    private final String c;
    private final int d;
    private final r.a e;
    private Integer f;
    private p g;
    private boolean h;
    private boolean i;
    private boolean j;
    private long k;
    private t l;
    private b.a m;
    private Object n;

    /* compiled from: Request */
    public enum a {
        LOW,
        NORMAL,
        HIGH,
        IMMEDIATE
    }

    /* access modifiers changed from: protected */
    public abstract r<T> a(k kVar);

    /* access modifiers changed from: protected */
    public abstract void b(T t);

    public n(int i2, String str, r.a aVar) {
        x.a aVar2;
        if (x.a.f164a) {
            aVar2 = new x.a();
        } else {
            aVar2 = null;
        }
        this.f137a = aVar2;
        this.h = true;
        this.i = false;
        this.j = false;
        this.k = 0;
        this.m = null;
        this.b = i2;
        this.c = str;
        this.e = aVar;
        a((t) new e());
        this.d = TextUtils.isEmpty(str) ? 0 : Uri.parse(str).getHost().hashCode();
    }

    public int a() {
        return this.b;
    }

    public void a(Object obj) {
        this.n = obj;
    }

    public Object b() {
        return this.n;
    }

    public int c() {
        return this.d;
    }

    public void a(t tVar) {
        this.l = tVar;
    }

    public void a(String str) {
        if (x.a.f164a) {
            this.f137a.a(str, Thread.currentThread().getId());
        } else if (this.k == 0) {
            this.k = SystemClock.elapsedRealtime();
        }
    }

    /* access modifiers changed from: package-private */
    public void b(String str) {
        if (this.g != null) {
            this.g.b(this);
        }
        if (x.a.f164a) {
            long id = Thread.currentThread().getId();
            if (Looper.myLooper() != Looper.getMainLooper()) {
                new Handler(Looper.getMainLooper()).post(new o(this, str, id));
                return;
            }
            this.f137a.a(str, id);
            this.f137a.a(toString());
            return;
        }
        long elapsedRealtime = SystemClock.elapsedRealtime() - this.k;
        if (elapsedRealtime >= 3000) {
            x.b("%d ms: %s", Long.valueOf(elapsedRealtime), toString());
        }
    }

    public void a(p pVar) {
        this.g = pVar;
    }

    public final void a(int i2) {
        this.f = Integer.valueOf(i2);
    }

    public String d() {
        return this.c;
    }

    public String e() {
        return d();
    }

    public void a(b.a aVar) {
        this.m = aVar;
    }

    public b.a f() {
        return this.m;
    }

    public void g() {
        this.i = true;
    }

    public boolean h() {
        return this.i;
    }

    public Map<String, String> i() throws a {
        return Collections.emptyMap();
    }

    /* access modifiers changed from: protected */
    public Map<String, String> j() throws a {
        return n();
    }

    /* access modifiers changed from: protected */
    public String k() {
        return o();
    }

    public String l() {
        return p();
    }

    public byte[] m() throws a {
        Map<String, String> j2 = j();
        if (j2 == null || j2.size() <= 0) {
            return null;
        }
        return a(j2, k());
    }

    /* access modifiers changed from: protected */
    public Map<String, String> n() throws a {
        return null;
    }

    /* access modifiers changed from: protected */
    public String o() {
        return "UTF-8";
    }

    public String p() {
        return "application/x-www-form-urlencoded; charset=" + o();
    }

    public byte[] q() throws a {
        Map<String, String> n2 = n();
        if (n2 == null || n2.size() <= 0) {
            return null;
        }
        return a(n2, o());
    }

    private byte[] a(Map<String, String> map, String str) {
        StringBuilder sb = new StringBuilder();
        try {
            for (Map.Entry next : map.entrySet()) {
                sb.append(URLEncoder.encode((String) next.getKey(), str));
                sb.append('=');
                sb.append(URLEncoder.encode((String) next.getValue(), str));
                sb.append('&');
            }
            return sb.toString().getBytes(str);
        } catch (UnsupportedEncodingException e2) {
            throw new RuntimeException("Encoding not supported: " + str, e2);
        }
    }

    public final void a(boolean z) {
        this.h = z;
    }

    public final boolean r() {
        return this.h;
    }

    public a s() {
        return a.NORMAL;
    }

    public final int t() {
        return this.l.a();
    }

    public t u() {
        return this.l;
    }

    public void v() {
        this.j = true;
    }

    public boolean w() {
        return this.j;
    }

    /* access modifiers changed from: protected */
    public w a(w wVar) {
        return wVar;
    }

    public void b(w wVar) {
        if (this.e != null) {
            this.e.onErrorResponse(wVar);
        }
    }

    /* renamed from: a */
    public int compareTo(n nVar) {
        a s = s();
        a s2 = nVar.s();
        if (s == s2) {
            return this.f.intValue() - nVar.f.intValue();
        }
        return s2.ordinal() - s.ordinal();
    }

    public String toString() {
        return String.valueOf(this.i ? "[X] " : "[ ] ") + d() + " " + ("0x" + Integer.toHexString(c())) + " " + s() + " " + this.f;
    }
}
