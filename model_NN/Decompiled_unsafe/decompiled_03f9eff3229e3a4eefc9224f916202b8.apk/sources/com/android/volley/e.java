package com.android.volley;

/* compiled from: DefaultRetryPolicy */
public class e implements t {

    /* renamed from: a  reason: collision with root package name */
    private int f131a;
    private int b;
    private final int c;
    private final float d;

    public e() {
        this(2500, 1, 1.0f);
    }

    public e(int i, int i2, float f) {
        this.f131a = i;
        this.c = i2;
        this.d = f;
    }

    public int a() {
        return this.f131a;
    }

    public int b() {
        return this.b;
    }

    public void a(w wVar) throws w {
        this.b++;
        this.f131a = (int) (((float) this.f131a) + (((float) this.f131a) * this.d));
        if (!c()) {
            throw wVar;
        }
    }

    /* access modifiers changed from: protected */
    public boolean c() {
        return this.b <= this.c;
    }
}
