package com.android.volley;

import android.os.SystemClock;
import android.util.Log;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/* compiled from: VolleyLog */
public class x {

    /* renamed from: a  reason: collision with root package name */
    public static String f163a = "Volley";
    public static boolean b = Log.isLoggable(f163a, 2);

    public static void a(String str, Object... objArr) {
        if (b) {
            Log.v(f163a, e(str, objArr));
        }
    }

    public static void b(String str, Object... objArr) {
        Log.d(f163a, e(str, objArr));
    }

    public static void c(String str, Object... objArr) {
        Log.e(f163a, e(str, objArr));
    }

    public static void a(Throwable th, String str, Object... objArr) {
        Log.e(f163a, e(str, objArr), th);
    }

    public static void d(String str, Object... objArr) {
        Log.wtf(f163a, e(str, objArr));
    }

    private static String e(String str, Object... objArr) {
        String str2;
        if (objArr != null) {
            str = String.format(Locale.US, str, objArr);
        }
        StackTraceElement[] stackTrace = new Throwable().fillInStackTrace().getStackTrace();
        int i = 2;
        while (true) {
            if (i >= stackTrace.length) {
                str2 = "<unknown>";
                break;
            } else if (!stackTrace[i].getClass().equals(x.class)) {
                String className = stackTrace[i].getClassName();
                String substring = className.substring(className.lastIndexOf(46) + 1);
                str2 = String.valueOf(substring.substring(substring.lastIndexOf(36) + 1)) + "." + stackTrace[i].getMethodName();
                break;
            } else {
                i++;
            }
        }
        return String.format(Locale.US, "[%d] %s: %s", Long.valueOf(Thread.currentThread().getId()), str2, str);
    }

    /* compiled from: VolleyLog */
    static class a {

        /* renamed from: a  reason: collision with root package name */
        public static final boolean f164a = x.b;
        private final List<C0002a> b = new ArrayList();
        private boolean c = false;

        a() {
        }

        /* renamed from: com.android.volley.x$a$a  reason: collision with other inner class name */
        /* compiled from: VolleyLog */
        private static class C0002a {

            /* renamed from: a  reason: collision with root package name */
            public final String f165a;
            public final long b;
            public final long c;

            public C0002a(String str, long j, long j2) {
                this.f165a = str;
                this.b = j;
                this.c = j2;
            }
        }

        public synchronized void a(String str, long j) {
            if (this.c) {
                throw new IllegalStateException("Marker added to finished log");
            }
            this.b.add(new C0002a(str, j, SystemClock.elapsedRealtime()));
        }

        public synchronized void a(String str) {
            this.c = true;
            long a2 = a();
            if (a2 > 0) {
                long j = this.b.get(0).c;
                x.b("(%-4d ms) %s", Long.valueOf(a2), str);
                long j2 = j;
                for (C0002a next : this.b) {
                    long j3 = next.c;
                    x.b("(+%-4d) [%2d] %s", Long.valueOf(j3 - j2), Long.valueOf(next.b), next.f165a);
                    j2 = j3;
                }
            }
        }

        /* access modifiers changed from: protected */
        public void finalize() throws Throwable {
            if (!this.c) {
                a("Request on the loose");
                x.c("Marker log finalized without finish() - uncaught exit point for request", new Object[0]);
            }
        }

        private long a() {
            if (this.b.size() == 0) {
                return 0;
            }
            return this.b.get(this.b.size() - 1).c - this.b.get(0).c;
        }
    }
}
