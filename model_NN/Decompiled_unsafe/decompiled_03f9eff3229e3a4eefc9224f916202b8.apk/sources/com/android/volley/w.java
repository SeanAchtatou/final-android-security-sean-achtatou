package com.android.volley;

/* compiled from: VolleyError */
public class w extends Exception {

    /* renamed from: a  reason: collision with root package name */
    public final k f162a;

    public w() {
        this.f162a = null;
    }

    public w(k kVar) {
        this.f162a = kVar;
    }

    public w(Throwable th) {
        super(th);
        this.f162a = null;
    }
}
