package com.android.volley;

import android.os.Handler;
import java.util.concurrent.Executor;

/* compiled from: ExecutorDelivery */
class g implements Executor {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ f f134a;
    private final /* synthetic */ Handler b;

    g(f fVar, Handler handler) {
        this.f134a = fVar;
        this.b = handler;
    }

    public void execute(Runnable runnable) {
        this.b.post(runnable);
    }
}
