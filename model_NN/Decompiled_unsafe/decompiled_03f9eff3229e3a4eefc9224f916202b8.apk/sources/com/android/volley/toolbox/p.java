package com.android.volley.toolbox;

import com.android.volley.toolbox.i;
import com.android.volley.w;

/* compiled from: NetworkImageView */
class p implements i.d {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ NetworkImageView f159a;
    private final /* synthetic */ boolean b;

    p(NetworkImageView networkImageView, boolean z) {
        this.f159a = networkImageView;
        this.b = z;
    }

    public void onErrorResponse(w wVar) {
        if (this.f159a.c != 0) {
            this.f159a.setImageResource(this.f159a.c);
        }
    }

    public void a(i.c cVar, boolean z) {
        if (z && this.b) {
            this.f159a.post(new q(this, cVar));
        } else if (cVar.b() != null) {
            this.f159a.setImageBitmap(cVar.b());
        } else if (this.f159a.b != 0) {
            this.f159a.setImageResource(this.f159a.b);
        }
    }
}
