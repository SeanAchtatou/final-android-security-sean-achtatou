package com.android.volley.toolbox;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import cn.banshenggua.aichang.utils.Constants;
import com.android.volley.e;
import com.android.volley.k;
import com.android.volley.n;
import com.android.volley.r;
import com.android.volley.t;
import com.android.volley.x;

/* compiled from: ImageRequest */
public class m extends n<Bitmap> {
    private static final Object e = new Object();

    /* renamed from: a  reason: collision with root package name */
    private final r.b<Bitmap> f157a;
    private final Bitmap.Config b;
    private final int c;
    private final int d;

    public m(String str, r.b<Bitmap> bVar, int i, int i2, Bitmap.Config config, r.a aVar) {
        super(0, str, aVar);
        a((t) new e(Constants.CLEARIMGED, 2, 2.0f));
        this.f157a = bVar;
        this.b = config;
        this.c = i;
        this.d = i2;
    }

    public n.a s() {
        return n.a.LOW;
    }

    private static int b(int i, int i2, int i3, int i4) {
        if (i == 0 && i2 == 0) {
            return i3;
        }
        if (i == 0) {
            return (int) ((((double) i2) / ((double) i4)) * ((double) i3));
        }
        if (i2 == 0) {
            return i;
        }
        double d2 = ((double) i4) / ((double) i3);
        if (((double) i) * d2 > ((double) i2)) {
            return (int) (((double) i2) / d2);
        }
        return i;
    }

    /* access modifiers changed from: protected */
    public r<Bitmap> a(k kVar) {
        r<Bitmap> a2;
        synchronized (e) {
            try {
                a2 = b(kVar);
            } catch (OutOfMemoryError e2) {
                x.c("Caught OOM for %d byte image, url=%s", Integer.valueOf(kVar.b.length), d());
                a2 = r.a(new com.android.volley.m(e2));
            }
        }
        return a2;
    }

    private r<Bitmap> b(k kVar) {
        Bitmap bitmap;
        byte[] bArr = kVar.b;
        BitmapFactory.Options options = new BitmapFactory.Options();
        if (this.c == 0 && this.d == 0) {
            options.inPreferredConfig = this.b;
            bitmap = BitmapFactory.decodeByteArray(bArr, 0, bArr.length, options);
        } else {
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeByteArray(bArr, 0, bArr.length, options);
            int i = options.outWidth;
            int i2 = options.outHeight;
            int b2 = b(this.c, this.d, i, i2);
            int b3 = b(this.d, this.c, i2, i);
            options.inJustDecodeBounds = false;
            options.inSampleSize = a(i, i2, b2, b3);
            Bitmap decodeByteArray = BitmapFactory.decodeByteArray(bArr, 0, bArr.length, options);
            if (decodeByteArray == null || (decodeByteArray.getWidth() <= b2 && decodeByteArray.getHeight() <= b3)) {
                bitmap = decodeByteArray;
            } else {
                bitmap = Bitmap.createScaledBitmap(decodeByteArray, b2, b3, true);
                decodeByteArray.recycle();
            }
        }
        if (bitmap == null) {
            return r.a(new com.android.volley.m(kVar));
        }
        return r.a(bitmap, f.a(kVar));
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void b(Bitmap bitmap) {
        this.f157a.onResponse(bitmap);
    }

    static int a(int i, int i2, int i3, int i4) {
        float f = 1.0f;
        while (((double) (f * 2.0f)) <= Math.min(((double) i) / ((double) i3), ((double) i2) / ((double) i4))) {
            f *= 2.0f;
        }
        return (int) f;
    }
}
