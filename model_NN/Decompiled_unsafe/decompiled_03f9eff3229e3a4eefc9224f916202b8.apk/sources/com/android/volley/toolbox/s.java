package com.android.volley.toolbox;

import android.content.Context;
import android.content.pm.PackageManager;
import android.net.http.AndroidHttpClient;
import android.os.Build;
import com.android.volley.p;
import java.io.File;

/* compiled from: Volley */
public class s {
    public static p a(Context context, g gVar) {
        File file = new File(context.getCacheDir(), "volley");
        String str = "volley/0";
        try {
            String packageName = context.getPackageName();
            str = String.valueOf(packageName) + "/" + context.getPackageManager().getPackageInfo(packageName, 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
        }
        if (gVar == null) {
            if (Build.VERSION.SDK_INT >= 9) {
                gVar = new h();
            } else {
                gVar = new e(AndroidHttpClient.newInstance(str));
            }
        }
        p pVar = new p(new d(file), new a(gVar));
        pVar.a();
        return pVar;
    }

    public static p a(Context context) {
        return a(context, null);
    }
}
