package com.android.volley.toolbox;

import com.android.volley.a;
import com.android.volley.n;
import java.io.IOException;
import java.util.Map;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

/* compiled from: HttpClientStack */
public class e implements g {

    /* renamed from: a  reason: collision with root package name */
    protected final HttpClient f149a;

    public e(HttpClient httpClient) {
        this.f149a = httpClient;
    }

    private static void a(HttpUriRequest httpUriRequest, Map<String, String> map) {
        for (String next : map.keySet()) {
            httpUriRequest.setHeader(next, map.get(next));
        }
    }

    public HttpResponse a(n<?> nVar, Map<String, String> map) throws IOException, a {
        HttpUriRequest b = b(nVar, map);
        a(b, map);
        a(b, nVar.i());
        a(b);
        HttpParams params = b.getParams();
        int t = nVar.t();
        HttpConnectionParams.setConnectionTimeout(params, 5000);
        HttpConnectionParams.setSoTimeout(params, t);
        return this.f149a.execute(b);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.android.volley.toolbox.e.a(org.apache.http.client.methods.HttpEntityEnclosingRequestBase, com.android.volley.n<?>):void
     arg types: [org.apache.http.client.methods.HttpPost, com.android.volley.n<?>]
     candidates:
      com.android.volley.toolbox.e.a(org.apache.http.client.methods.HttpUriRequest, java.util.Map<java.lang.String, java.lang.String>):void
      com.android.volley.toolbox.e.a(com.android.volley.n<?>, java.util.Map<java.lang.String, java.lang.String>):org.apache.http.HttpResponse
      com.android.volley.toolbox.g.a(com.android.volley.n<?>, java.util.Map<java.lang.String, java.lang.String>):org.apache.http.HttpResponse
      com.android.volley.toolbox.e.a(org.apache.http.client.methods.HttpEntityEnclosingRequestBase, com.android.volley.n<?>):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.android.volley.toolbox.e.a(org.apache.http.client.methods.HttpEntityEnclosingRequestBase, com.android.volley.n<?>):void
     arg types: [org.apache.http.client.methods.HttpPut, com.android.volley.n<?>]
     candidates:
      com.android.volley.toolbox.e.a(org.apache.http.client.methods.HttpUriRequest, java.util.Map<java.lang.String, java.lang.String>):void
      com.android.volley.toolbox.e.a(com.android.volley.n<?>, java.util.Map<java.lang.String, java.lang.String>):org.apache.http.HttpResponse
      com.android.volley.toolbox.g.a(com.android.volley.n<?>, java.util.Map<java.lang.String, java.lang.String>):org.apache.http.HttpResponse
      com.android.volley.toolbox.e.a(org.apache.http.client.methods.HttpEntityEnclosingRequestBase, com.android.volley.n<?>):void */
    static HttpUriRequest b(n<?> nVar, Map<String, String> map) throws a {
        switch (nVar.a()) {
            case -1:
                byte[] m = nVar.m();
                if (m == null) {
                    return new HttpGet(nVar.d());
                }
                HttpPost httpPost = new HttpPost(nVar.d());
                httpPost.addHeader("Content-Type", nVar.l());
                httpPost.setEntity(new ByteArrayEntity(m));
                return httpPost;
            case 0:
                return new HttpGet(nVar.d());
            case 1:
                HttpPost httpPost2 = new HttpPost(nVar.d());
                httpPost2.addHeader("Content-Type", nVar.p());
                a((HttpEntityEnclosingRequestBase) httpPost2, nVar);
                return httpPost2;
            case 2:
                HttpPut httpPut = new HttpPut(nVar.d());
                httpPut.addHeader("Content-Type", nVar.p());
                a((HttpEntityEnclosingRequestBase) httpPut, nVar);
                return httpPut;
            case 3:
                return new HttpDelete(nVar.d());
            default:
                throw new IllegalStateException("Unknown request method.");
        }
    }

    private static void a(HttpEntityEnclosingRequestBase httpEntityEnclosingRequestBase, n<?> nVar) throws a {
        byte[] q = nVar.q();
        if (q != null) {
            httpEntityEnclosingRequestBase.setEntity(new ByteArrayEntity(q));
        }
    }

    /* access modifiers changed from: protected */
    public void a(HttpUriRequest httpUriRequest) throws IOException {
    }
}
