package com.android.volley.toolbox;

import cn.banshenggua.aichang.utils.StringUtil;
import com.android.volley.k;
import com.android.volley.n;
import com.android.volley.r;
import com.android.volley.x;
import java.io.UnsupportedEncodingException;

/* compiled from: JsonRequest */
public abstract class o<T> extends n<T> {

    /* renamed from: a  reason: collision with root package name */
    private static final String f158a = String.format("application/json; charset=%s", StringUtil.Encoding);
    private final r.b<T> b;
    private final String c;

    /* access modifiers changed from: protected */
    public abstract r<T> a(k kVar);

    public o(int i, String str, String str2, r.b<T> bVar, r.a aVar) {
        super(i, str, aVar);
        this.b = bVar;
        this.c = str2;
    }

    /* access modifiers changed from: protected */
    public void b(T t) {
        this.b.onResponse(t);
    }

    public String l() {
        return p();
    }

    public byte[] m() {
        return q();
    }

    public String p() {
        return f158a;
    }

    public byte[] q() {
        try {
            if (this.c == null) {
                return null;
            }
            return this.c.getBytes(StringUtil.Encoding);
        } catch (UnsupportedEncodingException e) {
            x.d("Unsupported Encoding while trying to get the bytes of %s using %s", this.c, StringUtil.Encoding);
            return null;
        }
    }
}
