package com.android.volley.toolbox;

import cn.banshenggua.aichang.room.SimpleLiveRoomActivity;
import com.android.volley.b;
import com.android.volley.h;
import com.android.volley.n;
import com.android.volley.t;
import com.android.volley.u;
import com.android.volley.w;
import com.android.volley.x;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.StatusLine;
import org.apache.http.impl.cookie.DateUtils;

/* compiled from: BasicNetwork */
public class a implements h {

    /* renamed from: a  reason: collision with root package name */
    protected static final boolean f144a = x.b;
    private static int d = SimpleLiveRoomActivity.DONWLOAD_START;
    private static int e = 4096;
    protected final g b;
    protected final b c;

    public a(g gVar) {
        this(gVar, new b(e));
    }

    public a(g gVar, b bVar) {
        this.b = gVar;
        this.c = bVar;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0068, code lost:
        a("socket", r13, new com.android.volley.v());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x007f, code lost:
        a("connection", r13, new com.android.volley.v());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x008b, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00a4, code lost:
        throw new java.lang.RuntimeException("Bad URL " + r13.d(), r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00a5, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00a9, code lost:
        r0 = r2.getStatusLine().getStatusCode();
        com.android.volley.x.c("Unexpected response code %d for %s", java.lang.Integer.valueOf(r0), r13.d());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00c7, code lost:
        if (0 != 0) goto L_0x00c9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00c9, code lost:
        r2 = new com.android.volley.k(r0, null, r1, false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00d1, code lost:
        if (r0 == 401) goto L_0x00d7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00d7, code lost:
        a("auth", r13, new com.android.volley.a(r2));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00e8, code lost:
        throw new com.android.volley.l(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00ee, code lost:
        throw new com.android.volley.u(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00f4, code lost:
        throw new com.android.volley.j((com.android.volley.k) null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00f8, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00f9, code lost:
        r1 = r7;
        r2 = r8;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0067 A[ExcHandler: SocketTimeoutException (e java.net.SocketTimeoutException), Splitter:B:2:0x000b] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x007e A[ExcHandler: ConnectTimeoutException (e org.apache.http.conn.ConnectTimeoutException), Splitter:B:2:0x000b] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x008b A[ExcHandler: MalformedURLException (r0v7 'e' java.net.MalformedURLException A[CUSTOM_DECLARE]), Splitter:B:2:0x000b] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00a9  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x00e3 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.android.volley.k a(com.android.volley.n<?> r13) throws com.android.volley.w {
        /*
            r12 = this;
            long r10 = android.os.SystemClock.elapsedRealtime()
        L_0x0004:
            r2 = 0
            r5 = 0
            java.util.HashMap r1 = new java.util.HashMap
            r1.<init>()
            java.util.HashMap r0 = new java.util.HashMap     // Catch:{ SocketTimeoutException -> 0x0067, ConnectTimeoutException -> 0x007e, MalformedURLException -> 0x008b, IOException -> 0x00a5 }
            r0.<init>()     // Catch:{ SocketTimeoutException -> 0x0067, ConnectTimeoutException -> 0x007e, MalformedURLException -> 0x008b, IOException -> 0x00a5 }
            com.android.volley.b$a r3 = r13.f()     // Catch:{ SocketTimeoutException -> 0x0067, ConnectTimeoutException -> 0x007e, MalformedURLException -> 0x008b, IOException -> 0x00a5 }
            r12.a(r0, r3)     // Catch:{ SocketTimeoutException -> 0x0067, ConnectTimeoutException -> 0x007e, MalformedURLException -> 0x008b, IOException -> 0x00a5 }
            com.android.volley.toolbox.g r3 = r12.b     // Catch:{ SocketTimeoutException -> 0x0067, ConnectTimeoutException -> 0x007e, MalformedURLException -> 0x008b, IOException -> 0x00a5 }
            org.apache.http.HttpResponse r8 = r3.a(r13, r0)     // Catch:{ SocketTimeoutException -> 0x0067, ConnectTimeoutException -> 0x007e, MalformedURLException -> 0x008b, IOException -> 0x00a5 }
            org.apache.http.StatusLine r6 = r8.getStatusLine()     // Catch:{ SocketTimeoutException -> 0x0067, ConnectTimeoutException -> 0x007e, MalformedURLException -> 0x008b, IOException -> 0x00f5 }
            int r9 = r6.getStatusCode()     // Catch:{ SocketTimeoutException -> 0x0067, ConnectTimeoutException -> 0x007e, MalformedURLException -> 0x008b, IOException -> 0x00f5 }
            org.apache.http.Header[] r0 = r8.getAllHeaders()     // Catch:{ SocketTimeoutException -> 0x0067, ConnectTimeoutException -> 0x007e, MalformedURLException -> 0x008b, IOException -> 0x00f5 }
            java.util.Map r7 = a(r0)     // Catch:{ SocketTimeoutException -> 0x0067, ConnectTimeoutException -> 0x007e, MalformedURLException -> 0x008b, IOException -> 0x00f5 }
            r0 = 304(0x130, float:4.26E-43)
            if (r9 != r0) goto L_0x0040
            com.android.volley.k r0 = new com.android.volley.k     // Catch:{ SocketTimeoutException -> 0x0067, ConnectTimeoutException -> 0x007e, MalformedURLException -> 0x008b, IOException -> 0x00f8 }
            r1 = 304(0x130, float:4.26E-43)
            com.android.volley.b$a r2 = r13.f()     // Catch:{ SocketTimeoutException -> 0x0067, ConnectTimeoutException -> 0x007e, MalformedURLException -> 0x008b, IOException -> 0x00f8 }
            byte[] r2 = r2.f128a     // Catch:{ SocketTimeoutException -> 0x0067, ConnectTimeoutException -> 0x007e, MalformedURLException -> 0x008b, IOException -> 0x00f8 }
            r3 = 1
            r0.<init>(r1, r2, r7, r3)     // Catch:{ SocketTimeoutException -> 0x0067, ConnectTimeoutException -> 0x007e, MalformedURLException -> 0x008b, IOException -> 0x00f8 }
        L_0x003f:
            return r0
        L_0x0040:
            org.apache.http.HttpEntity r0 = r8.getEntity()     // Catch:{ SocketTimeoutException -> 0x0067, ConnectTimeoutException -> 0x007e, MalformedURLException -> 0x008b, IOException -> 0x00f8 }
            if (r0 == 0) goto L_0x0073
            org.apache.http.HttpEntity r0 = r8.getEntity()     // Catch:{ SocketTimeoutException -> 0x0067, ConnectTimeoutException -> 0x007e, MalformedURLException -> 0x008b, IOException -> 0x00f8 }
            byte[] r5 = r12.a(r0)     // Catch:{ SocketTimeoutException -> 0x0067, ConnectTimeoutException -> 0x007e, MalformedURLException -> 0x008b, IOException -> 0x00f8 }
        L_0x004e:
            long r0 = android.os.SystemClock.elapsedRealtime()     // Catch:{ SocketTimeoutException -> 0x0067, ConnectTimeoutException -> 0x007e, MalformedURLException -> 0x008b, IOException -> 0x00f8 }
            long r2 = r0 - r10
            r1 = r12
            r4 = r13
            r1.a(r2, r4, r5, r6)     // Catch:{ SocketTimeoutException -> 0x0067, ConnectTimeoutException -> 0x007e, MalformedURLException -> 0x008b, IOException -> 0x00f8 }
            r0 = 200(0xc8, float:2.8E-43)
            if (r9 < r0) goto L_0x0061
            r0 = 299(0x12b, float:4.19E-43)
            if (r9 <= r0) goto L_0x0077
        L_0x0061:
            java.io.IOException r0 = new java.io.IOException     // Catch:{ SocketTimeoutException -> 0x0067, ConnectTimeoutException -> 0x007e, MalformedURLException -> 0x008b, IOException -> 0x00f8 }
            r0.<init>()     // Catch:{ SocketTimeoutException -> 0x0067, ConnectTimeoutException -> 0x007e, MalformedURLException -> 0x008b, IOException -> 0x00f8 }
            throw r0     // Catch:{ SocketTimeoutException -> 0x0067, ConnectTimeoutException -> 0x007e, MalformedURLException -> 0x008b, IOException -> 0x00f8 }
        L_0x0067:
            r0 = move-exception
            java.lang.String r0 = "socket"
            com.android.volley.v r1 = new com.android.volley.v
            r1.<init>()
            a(r0, r13, r1)
            goto L_0x0004
        L_0x0073:
            r0 = 0
            byte[] r5 = new byte[r0]     // Catch:{ SocketTimeoutException -> 0x0067, ConnectTimeoutException -> 0x007e, MalformedURLException -> 0x008b, IOException -> 0x00f8 }
            goto L_0x004e
        L_0x0077:
            com.android.volley.k r0 = new com.android.volley.k     // Catch:{ SocketTimeoutException -> 0x0067, ConnectTimeoutException -> 0x007e, MalformedURLException -> 0x008b, IOException -> 0x00f8 }
            r1 = 0
            r0.<init>(r9, r5, r7, r1)     // Catch:{ SocketTimeoutException -> 0x0067, ConnectTimeoutException -> 0x007e, MalformedURLException -> 0x008b, IOException -> 0x00f8 }
            goto L_0x003f
        L_0x007e:
            r0 = move-exception
            java.lang.String r0 = "connection"
            com.android.volley.v r1 = new com.android.volley.v
            r1.<init>()
            a(r0, r13, r1)
            goto L_0x0004
        L_0x008b:
            r0 = move-exception
            java.lang.RuntimeException r1 = new java.lang.RuntimeException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "Bad URL "
            r2.<init>(r3)
            java.lang.String r3 = r13.d()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            r1.<init>(r2, r0)
            throw r1
        L_0x00a5:
            r0 = move-exception
        L_0x00a6:
            r3 = 0
            if (r2 == 0) goto L_0x00e3
            org.apache.http.StatusLine r0 = r2.getStatusLine()
            int r0 = r0.getStatusCode()
            java.lang.String r2 = "Unexpected response code %d for %s"
            r4 = 2
            java.lang.Object[] r4 = new java.lang.Object[r4]
            r6 = 0
            java.lang.Integer r7 = java.lang.Integer.valueOf(r0)
            r4[r6] = r7
            r6 = 1
            java.lang.String r7 = r13.d()
            r4[r6] = r7
            com.android.volley.x.c(r2, r4)
            if (r5 == 0) goto L_0x00ef
            com.android.volley.k r2 = new com.android.volley.k
            r3 = 0
            r2.<init>(r0, r5, r1, r3)
            r1 = 401(0x191, float:5.62E-43)
            if (r0 == r1) goto L_0x00d7
            r1 = 403(0x193, float:5.65E-43)
            if (r0 != r1) goto L_0x00e9
        L_0x00d7:
            java.lang.String r0 = "auth"
            com.android.volley.a r1 = new com.android.volley.a
            r1.<init>(r2)
            a(r0, r13, r1)
            goto L_0x0004
        L_0x00e3:
            com.android.volley.l r1 = new com.android.volley.l
            r1.<init>(r0)
            throw r1
        L_0x00e9:
            com.android.volley.u r0 = new com.android.volley.u
            r0.<init>(r2)
            throw r0
        L_0x00ef:
            com.android.volley.j r0 = new com.android.volley.j
            r0.<init>(r3)
            throw r0
        L_0x00f5:
            r0 = move-exception
            r2 = r8
            goto L_0x00a6
        L_0x00f8:
            r0 = move-exception
            r1 = r7
            r2 = r8
            goto L_0x00a6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.volley.toolbox.a.a(com.android.volley.n):com.android.volley.k");
    }

    private void a(long j, n<?> nVar, byte[] bArr, StatusLine statusLine) {
        if (f144a || j > ((long) d)) {
            Object[] objArr = new Object[5];
            objArr[0] = nVar;
            objArr[1] = Long.valueOf(j);
            objArr[2] = bArr != null ? Integer.valueOf(bArr.length) : "null";
            objArr[3] = Integer.valueOf(statusLine.getStatusCode());
            objArr[4] = Integer.valueOf(nVar.u().b());
            x.b("HTTP response for request=<%s> [lifetime=%d], [size=%s], [rc=%d], [retryCount=%s]", objArr);
        }
    }

    private static void a(String str, n<?> nVar, w wVar) throws w {
        t u = nVar.u();
        int t = nVar.t();
        try {
            u.a(wVar);
            nVar.a(String.format("%s-retry [timeout=%s]", str, Integer.valueOf(t)));
        } catch (w e2) {
            nVar.a(String.format("%s-timeout-giveup [timeout=%s]", str, Integer.valueOf(t)));
            throw e2;
        }
    }

    private void a(Map<String, String> map, b.a aVar) {
        if (aVar != null) {
            if (aVar.b != null) {
                map.put("If-None-Match", aVar.b);
            }
            if (aVar.c > 0) {
                map.put("If-Modified-Since", DateUtils.formatDate(new Date(aVar.c)));
            }
        }
    }

    private byte[] a(HttpEntity httpEntity) throws IOException, u {
        r rVar = new r(this.c, (int) httpEntity.getContentLength());
        byte[] bArr = null;
        try {
            InputStream content = httpEntity.getContent();
            if (content == null) {
                throw new u();
            }
            bArr = this.c.a(1024);
            while (true) {
                int read = content.read(bArr);
                if (read == -1) {
                    break;
                }
                rVar.write(bArr, 0, read);
            }
            byte[] byteArray = rVar.toByteArray();
            try {
            } catch (IOException e2) {
                x.a("Error occured when calling consumingContent", new Object[0]);
            }
            return byteArray;
        } finally {
            try {
                httpEntity.consumeContent();
            } catch (IOException e3) {
                x.a("Error occured when calling consumingContent", new Object[0]);
            }
            this.c.a(bArr);
            rVar.close();
        }
    }

    private static Map<String, String> a(Header[] headerArr) {
        HashMap hashMap = new HashMap();
        for (int i = 0; i < headerArr.length; i++) {
            hashMap.put(headerArr[i].getName(), headerArr[i].getValue());
        }
        return hashMap;
    }
}
