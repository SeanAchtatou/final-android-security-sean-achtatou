package com.android.volley.toolbox;

import com.android.volley.k;
import com.android.volley.m;
import com.android.volley.r;
import java.io.UnsupportedEncodingException;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: JsonObjectRequest */
public class n extends o<JSONObject> {
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public n(int i, String str, JSONObject jSONObject, r.b<JSONObject> bVar, r.a aVar) {
        super(i, str, jSONObject == null ? null : jSONObject.toString(), bVar, aVar);
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public n(String str, JSONObject jSONObject, r.b<JSONObject> bVar, r.a aVar) {
        this(jSONObject == null ? 0 : 1, str, jSONObject, bVar, aVar);
    }

    /* access modifiers changed from: protected */
    public r<JSONObject> a(k kVar) {
        try {
            return r.a(new JSONObject(new String(kVar.b, f.a(kVar.c))), f.a(kVar));
        } catch (UnsupportedEncodingException e) {
            return r.a(new m(e));
        } catch (JSONException e2) {
            return r.a(new m(e2));
        }
    }
}
