package com.android.volley.toolbox;

import com.android.volley.toolbox.i;
import java.util.Iterator;

/* compiled from: ImageLoader */
class l implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ i f156a;

    l(i iVar) {
        this.f156a = iVar;
    }

    public void run() {
        for (i.a aVar : this.f156a.e.values()) {
            Iterator it = aVar.e.iterator();
            while (it.hasNext()) {
                i.c cVar = (i.c) it.next();
                if (cVar.c != null) {
                    if (aVar.a() == null) {
                        cVar.b = aVar.c;
                        cVar.c.a(cVar, false);
                    } else {
                        cVar.c.onErrorResponse(aVar.a());
                    }
                }
            }
        }
        this.f156a.e.clear();
        this.f156a.g = (Runnable) null;
    }
}
