package com.android.volley.toolbox;

import java.util.Comparator;

/* compiled from: ByteArrayPool */
class c implements Comparator<byte[]> {
    c() {
    }

    /* renamed from: a */
    public int compare(byte[] bArr, byte[] bArr2) {
        return bArr.length - bArr2.length;
    }
}
