package com.android.volley.toolbox;

import android.graphics.Bitmap;
import com.android.volley.r;

/* compiled from: ImageLoader */
class j implements r.b<Bitmap> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ i f154a;
    private final /* synthetic */ String b;

    j(i iVar, String str) {
        this.f154a = iVar;
        this.b = str;
    }

    /* renamed from: a */
    public void onResponse(Bitmap bitmap) {
        this.f154a.a(this.b, bitmap);
    }
}
