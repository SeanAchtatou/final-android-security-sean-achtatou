package com.android.volley;

import android.content.Intent;

/* compiled from: AuthFailureError */
public class a extends w {
    private Intent b;

    public a() {
    }

    public a(k kVar) {
        super(kVar);
    }

    public String getMessage() {
        if (this.b != null) {
            return "User needs to (re)enter credentials.";
        }
        return super.getMessage();
    }
}
