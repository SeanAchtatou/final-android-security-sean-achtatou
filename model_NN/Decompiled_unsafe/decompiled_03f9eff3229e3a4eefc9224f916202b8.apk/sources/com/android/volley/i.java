package com.android.volley;

import android.net.TrafficStats;
import android.os.Build;
import android.os.Process;
import java.util.concurrent.BlockingQueue;

/* compiled from: NetworkDispatcher */
public class i extends Thread {

    /* renamed from: a  reason: collision with root package name */
    private final BlockingQueue<n> f135a;
    private final h b;
    private final b c;
    private final s d;
    private volatile boolean e = false;

    public i(BlockingQueue<n> blockingQueue, h hVar, b bVar, s sVar) {
        this.f135a = blockingQueue;
        this.b = hVar;
        this.c = bVar;
        this.d = sVar;
    }

    public void a() {
        this.e = true;
        interrupt();
    }

    public void run() {
        Process.setThreadPriority(10);
        while (true) {
            try {
                n take = this.f135a.take();
                try {
                    take.a("network-queue-take");
                    if (take.h()) {
                        take.b("network-discard-cancelled");
                    } else {
                        if (Build.VERSION.SDK_INT >= 14) {
                            TrafficStats.setThreadStatsTag(take.c());
                        }
                        k a2 = this.b.a(take);
                        take.a("network-http-complete");
                        if (!a2.d || !take.w()) {
                            r a3 = take.a(a2);
                            take.a("network-parse-complete");
                            if (take.r() && a3.b != null) {
                                this.c.a(take.e(), a3.b);
                                take.a("network-cache-written");
                            }
                            take.v();
                            this.d.a(take, a3);
                        } else {
                            take.b("not-modified");
                        }
                    }
                } catch (w e2) {
                    a(take, e2);
                } catch (Exception e3) {
                    x.a(e3, "Unhandled exception %s", e3.toString());
                    this.d.a(take, new w(e3));
                }
            } catch (InterruptedException e4) {
                if (this.e) {
                    return;
                }
            }
        }
    }

    private void a(n<?> nVar, w wVar) {
        this.d.a(nVar, nVar.a(wVar));
    }
}
