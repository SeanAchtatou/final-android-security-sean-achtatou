package com.sina.weibo.sdk.utils;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.StateSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;
import org.apache.http.util.EncodingUtils;

public class ResourceManager {
    private static final String DRAWABLE = "drawable";
    private static final String DRAWABLE_HDPI = "drawable-hdpi";
    private static final String DRAWABLE_LDPI = "drawable-ldpi";
    private static final String DRAWABLE_MDPI = "drawable-mdpi";
    private static final String DRAWABLE_XHDPI = "drawable-xhdpi";
    private static final String DRAWABLE_XXHDPI = "drawable-xxhdpi";
    private static final String[] PRE_INSTALL_DRAWBLE_PATHS = {DRAWABLE_XXHDPI, DRAWABLE_XHDPI, DRAWABLE_HDPI, DRAWABLE_MDPI, DRAWABLE_LDPI, DRAWABLE};
    private static final String TAG = ResourceManager.class.getName();

    public static String getString(Context context, String str, String str2, String str3) {
        Locale language = getLanguage();
        if (Locale.SIMPLIFIED_CHINESE.equals(language)) {
            return str2;
        }
        if (Locale.TRADITIONAL_CHINESE.equals(language)) {
            return str3;
        }
        return str;
    }

    public static Drawable getDrawable(Context context, String str) {
        return getDrawableFromAssert(context, getAppropriatePathOfDrawable(context, str), false);
    }

    public static Drawable getNinePatchDrawable(Context context, String str) {
        return getDrawableFromAssert(context, getAppropriatePathOfDrawable(context, str), true);
    }

    public static Locale getLanguage() {
        Locale locale = Locale.getDefault();
        return (Locale.SIMPLIFIED_CHINESE.equals(locale) || Locale.TRADITIONAL_CHINESE.equals(locale)) ? locale : Locale.ENGLISH;
    }

    private static String getAppropriatePathOfDrawable(Context context, String str) {
        if (TextUtils.isEmpty(str)) {
            LogUtil.e(TAG, "id is NOT correct!");
            return null;
        }
        String currentDpiFolder = getCurrentDpiFolder(context);
        LogUtil.d(TAG, "find Appropriate path...");
        int i = 0;
        int i2 = -1;
        int i3 = -1;
        while (true) {
            if (i >= PRE_INSTALL_DRAWBLE_PATHS.length) {
                i = -1;
                break;
            }
            if (PRE_INSTALL_DRAWBLE_PATHS[i].equals(currentDpiFolder)) {
                i2 = i;
            }
            String str2 = String.valueOf(PRE_INSTALL_DRAWBLE_PATHS[i]) + "/" + str;
            if (isFileExisted(context, str2)) {
                if (i2 != i) {
                    if (i2 >= 0) {
                        break;
                    }
                    i3 = i;
                } else {
                    return str2;
                }
            }
            i++;
        }
        if (i3 <= 0 || i <= 0) {
            if (i3 <= 0 || i >= 0) {
                if (i3 >= 0 || i <= 0) {
                    LogUtil.e(TAG, "Not find the appropriate path for drawable");
                    i3 = -1;
                } else {
                    i3 = i;
                }
            }
        } else if (Math.abs(i2 - i) <= Math.abs(i2 - i3)) {
            i3 = i;
        }
        if (i3 >= 0) {
            return String.valueOf(PRE_INSTALL_DRAWBLE_PATHS[i3]) + "/" + str;
        }
        LogUtil.e(TAG, "Not find the appropriate path for drawable");
        return null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x0067 A[SYNTHETIC, Splitter:B:26:0x0067] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static android.graphics.drawable.Drawable getDrawableFromAssert(android.content.Context r11, java.lang.String r12, boolean r13) {
        /*
            r6 = 0
            android.content.res.AssetManager r0 = r11.getAssets()
            java.io.InputStream r7 = r0.open(r12)     // Catch:{ IOException -> 0x007a, all -> 0x0063 }
            if (r7 == 0) goto L_0x007f
            android.graphics.Bitmap r2 = android.graphics.BitmapFactory.decodeStream(r7)     // Catch:{ IOException -> 0x0052, all -> 0x0075 }
            android.content.res.Resources r0 = r11.getResources()     // Catch:{ IOException -> 0x0052, all -> 0x0075 }
            android.util.DisplayMetrics r0 = r0.getDisplayMetrics()     // Catch:{ IOException -> 0x0052, all -> 0x0075 }
            if (r13 == 0) goto L_0x0043
            android.content.res.Resources r1 = r11.getResources()     // Catch:{ IOException -> 0x0052, all -> 0x0075 }
            android.content.res.Configuration r3 = r1.getConfiguration()     // Catch:{ IOException -> 0x0052, all -> 0x0075 }
            android.content.res.Resources r1 = new android.content.res.Resources     // Catch:{ IOException -> 0x0052, all -> 0x0075 }
            android.content.res.AssetManager r4 = r11.getAssets()     // Catch:{ IOException -> 0x0052, all -> 0x0075 }
            r1.<init>(r4, r0, r3)     // Catch:{ IOException -> 0x0052, all -> 0x0075 }
            android.graphics.drawable.NinePatchDrawable r0 = new android.graphics.drawable.NinePatchDrawable     // Catch:{ IOException -> 0x0052, all -> 0x0075 }
            byte[] r3 = r2.getNinePatchChunk()     // Catch:{ IOException -> 0x0052, all -> 0x0075 }
            android.graphics.Rect r4 = new android.graphics.Rect     // Catch:{ IOException -> 0x0052, all -> 0x0075 }
            r5 = 0
            r8 = 0
            r9 = 0
            r10 = 0
            r4.<init>(r5, r8, r9, r10)     // Catch:{ IOException -> 0x0052, all -> 0x0075 }
            r5 = 0
            r0.<init>(r1, r2, r3, r4, r5)     // Catch:{ IOException -> 0x0052, all -> 0x0075 }
        L_0x003d:
            if (r7 == 0) goto L_0x0042
            r7.close()     // Catch:{ IOException -> 0x0070 }
        L_0x0042:
            return r0
        L_0x0043:
            int r0 = r0.densityDpi     // Catch:{ IOException -> 0x0052, all -> 0x0075 }
            r2.setDensity(r0)     // Catch:{ IOException -> 0x0052, all -> 0x0075 }
            android.graphics.drawable.BitmapDrawable r0 = new android.graphics.drawable.BitmapDrawable     // Catch:{ IOException -> 0x0052, all -> 0x0075 }
            android.content.res.Resources r1 = r11.getResources()     // Catch:{ IOException -> 0x0052, all -> 0x0075 }
            r0.<init>(r1, r2)     // Catch:{ IOException -> 0x0052, all -> 0x0075 }
            goto L_0x003d
        L_0x0052:
            r0 = move-exception
            r1 = r7
        L_0x0054:
            r0.printStackTrace()     // Catch:{ all -> 0x0077 }
            if (r1 == 0) goto L_0x007d
            r1.close()     // Catch:{ IOException -> 0x005e }
        L_0x005c:
            r0 = r6
            goto L_0x0042
        L_0x005e:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x005c
        L_0x0063:
            r0 = move-exception
            r7 = r6
        L_0x0065:
            if (r7 == 0) goto L_0x006a
            r7.close()     // Catch:{ IOException -> 0x006b }
        L_0x006a:
            throw r0
        L_0x006b:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x006a
        L_0x0070:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0042
        L_0x0075:
            r0 = move-exception
            goto L_0x0065
        L_0x0077:
            r0 = move-exception
            r7 = r1
            goto L_0x0065
        L_0x007a:
            r0 = move-exception
            r1 = r6
            goto L_0x0054
        L_0x007d:
            r0 = r6
            goto L_0x0042
        L_0x007f:
            r0 = r6
            goto L_0x003d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sina.weibo.sdk.utils.ResourceManager.getDrawableFromAssert(android.content.Context, java.lang.String, boolean):android.graphics.drawable.Drawable");
    }

    private static boolean isFileExisted(Context context, String str) {
        if (context == null || TextUtils.isEmpty(str)) {
            return false;
        }
        InputStream inputStream = null;
        try {
            inputStream = context.getAssets().open(str);
            LogUtil.d(TAG, "file [" + str + "] existed");
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return true;
        } catch (IOException e2) {
            LogUtil.d(TAG, "file [" + str + "] NOT existed");
            if (inputStream == null) {
                return false;
            }
            try {
                inputStream.close();
                return false;
            } catch (IOException e3) {
                e3.printStackTrace();
                return false;
            }
        } catch (Throwable th) {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e4) {
                    e4.printStackTrace();
                }
            }
            throw th;
        }
    }

    private static String getCurrentDpiFolder(Context context) {
        int i = context.getResources().getDisplayMetrics().densityDpi;
        if (i <= 120) {
            return DRAWABLE_LDPI;
        }
        if (i > 120 && i <= 160) {
            return DRAWABLE_MDPI;
        }
        if (i > 160 && i <= 240) {
            return DRAWABLE_HDPI;
        }
        if (i <= 240 || i > 320) {
            return DRAWABLE_XXHDPI;
        }
        return DRAWABLE_XHDPI;
    }

    private static View extractView(Context context, String str, ViewGroup viewGroup) throws Exception {
        return ((LayoutInflater) context.getSystemService("layout_inflater")).inflate(context.getAssets().openXmlResourceParser(str), viewGroup);
    }

    private static Drawable extractDrawable(Context context, String str) throws Exception {
        InputStream open = context.getAssets().open(str);
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        TypedValue typedValue = new TypedValue();
        typedValue.density = displayMetrics.densityDpi;
        Drawable createFromResourceStream = Drawable.createFromResourceStream(context.getResources(), typedValue, open, str);
        open.close();
        return createFromResourceStream;
    }

    public static int dp2px(Context context, int i) {
        return (int) (((double) (context.getResources().getDisplayMetrics().density * ((float) i))) + 0.5d);
    }

    public static ColorStateList createColorStateList(int i, int i2) {
        int[] iArr = {i2, i2, i2, i};
        return new ColorStateList(new int[][]{new int[]{16842919}, new int[]{16842913}, new int[]{16842908}, StateSet.WILD_CARD}, iArr);
    }

    public static StateListDrawable createStateListDrawable(Context context, String str, String str2) {
        Drawable drawable;
        Drawable drawable2;
        if (str.indexOf(".9") > -1) {
            drawable = getNinePatchDrawable(context, str);
        } else {
            drawable = getDrawable(context, str);
        }
        if (str2.indexOf(".9") > -1) {
            drawable2 = getNinePatchDrawable(context, str2);
        } else {
            drawable2 = getDrawable(context, str2);
        }
        StateListDrawable stateListDrawable = new StateListDrawable();
        stateListDrawable.addState(new int[]{16842919}, drawable2);
        stateListDrawable.addState(new int[]{16842913}, drawable2);
        stateListDrawable.addState(new int[]{16842908}, drawable2);
        stateListDrawable.addState(StateSet.WILD_CARD, drawable);
        return stateListDrawable;
    }

    public static StateListDrawable createStateListDrawable(Context context, String str, String str2, String str3) {
        Drawable drawable;
        Drawable drawable2;
        Drawable drawable3;
        if (str.indexOf(".9") > -1) {
            drawable = getNinePatchDrawable(context, str);
        } else {
            drawable = getDrawable(context, str);
        }
        if (str3.indexOf(".9") > -1) {
            drawable2 = getNinePatchDrawable(context, str3);
        } else {
            drawable2 = getDrawable(context, str3);
        }
        if (str2.indexOf(".9") > -1) {
            drawable3 = getNinePatchDrawable(context, str2);
        } else {
            drawable3 = getDrawable(context, str2);
        }
        StateListDrawable stateListDrawable = new StateListDrawable();
        stateListDrawable.addState(new int[]{16842919}, drawable3);
        stateListDrawable.addState(new int[]{16842913}, drawable3);
        stateListDrawable.addState(new int[]{16842908}, drawable3);
        stateListDrawable.addState(new int[]{16842766}, drawable2);
        stateListDrawable.addState(StateSet.WILD_CARD, drawable);
        return stateListDrawable;
    }

    public static String readCountryFromAsset(Context context, String str) {
        try {
            InputStream open = context.getAssets().open(str);
            if (open == null) {
                return "";
            }
            DataInputStream dataInputStream = new DataInputStream(open);
            byte[] bArr = new byte[dataInputStream.available()];
            dataInputStream.read(bArr);
            String string = EncodingUtils.getString(bArr, "UTF-8");
            open.close();
            return string;
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }
}
