package com.sina.weibo.sdk.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class ImageUtils {
    private static void revitionImageSizeHD(String str, int i, int i2) throws IOException {
        float f;
        Bitmap createBitmap;
        if (i <= 0) {
            throw new IllegalArgumentException("size must be greater than 0!");
        } else if (!isFileExisted(str)) {
            if (str == null) {
                str = "null";
            }
            throw new FileNotFoundException(str);
        } else if (!BitmapHelper.verifyBitmap(str)) {
            throw new IOException("");
        } else {
            int i3 = i * 2;
            FileInputStream fileInputStream = new FileInputStream(str);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(fileInputStream, null, options);
            try {
                fileInputStream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            int i4 = 0;
            while (true) {
                if ((options.outWidth >> i4) <= i3 && (options.outHeight >> i4) <= i3) {
                    break;
                }
                i4++;
            }
            options.inSampleSize = (int) Math.pow(2.0d, (double) i4);
            options.inJustDecodeBounds = false;
            Bitmap safeDecodeBimtapFile = safeDecodeBimtapFile(str, options);
            if (safeDecodeBimtapFile == null) {
                throw new IOException("Bitmap decode error!");
            }
            deleteDependon(str);
            makesureFileExist(str);
            float width = ((float) i) / ((float) (safeDecodeBimtapFile.getWidth() > safeDecodeBimtapFile.getHeight() ? safeDecodeBimtapFile.getWidth() : safeDecodeBimtapFile.getHeight()));
            if (width < 1.0f) {
                while (true) {
                    try {
                        f = width;
                        createBitmap = Bitmap.createBitmap((int) (((float) safeDecodeBimtapFile.getWidth()) * f), (int) (((float) safeDecodeBimtapFile.getHeight()) * f), Bitmap.Config.ARGB_8888);
                        break;
                    } catch (OutOfMemoryError e2) {
                        System.gc();
                        width = (float) (((double) f) * 0.8d);
                    }
                }
                if (createBitmap == null) {
                    safeDecodeBimtapFile.recycle();
                }
                Canvas canvas = new Canvas(createBitmap);
                Matrix matrix = new Matrix();
                matrix.setScale(f, f);
                canvas.drawBitmap(safeDecodeBimtapFile, matrix, new Paint());
                safeDecodeBimtapFile.recycle();
                safeDecodeBimtapFile = createBitmap;
            }
            FileOutputStream fileOutputStream = new FileOutputStream(str);
            if (options == null || options.outMimeType == null || !options.outMimeType.contains("png")) {
                safeDecodeBimtapFile.compress(Bitmap.CompressFormat.JPEG, i2, fileOutputStream);
            } else {
                safeDecodeBimtapFile.compress(Bitmap.CompressFormat.PNG, i2, fileOutputStream);
            }
            try {
                fileOutputStream.close();
            } catch (Exception e3) {
                e3.printStackTrace();
            }
            safeDecodeBimtapFile.recycle();
        }
    }

    private static void revitionImageSize(String str, int i, int i2) throws IOException {
        if (i <= 0) {
            throw new IllegalArgumentException("size must be greater than 0!");
        } else if (!isFileExisted(str)) {
            if (str == null) {
                str = "null";
            }
            throw new FileNotFoundException(str);
        } else if (!BitmapHelper.verifyBitmap(str)) {
            throw new IOException("");
        } else {
            FileInputStream fileInputStream = new FileInputStream(str);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(fileInputStream, null, options);
            try {
                fileInputStream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            int i3 = 0;
            while (true) {
                if ((options.outWidth >> i3) <= i && (options.outHeight >> i3) <= i) {
                    break;
                }
                i3++;
            }
            options.inSampleSize = (int) Math.pow(2.0d, (double) i3);
            options.inJustDecodeBounds = false;
            Bitmap safeDecodeBimtapFile = safeDecodeBimtapFile(str, options);
            if (safeDecodeBimtapFile == null) {
                throw new IOException("Bitmap decode error!");
            }
            deleteDependon(str);
            makesureFileExist(str);
            FileOutputStream fileOutputStream = new FileOutputStream(str);
            if (options == null || options.outMimeType == null || !options.outMimeType.contains("png")) {
                safeDecodeBimtapFile.compress(Bitmap.CompressFormat.JPEG, i2, fileOutputStream);
            } else {
                safeDecodeBimtapFile.compress(Bitmap.CompressFormat.PNG, i2, fileOutputStream);
            }
            try {
                fileOutputStream.close();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            safeDecodeBimtapFile.recycle();
        }
    }

    public static boolean revitionPostImageSize(Context context, String str) {
        try {
            if (NetworkHelper.isWifiValid(context)) {
                revitionImageSizeHD(str, 1600, 75);
            } else {
                revitionImageSize(str, 1024, 75);
            }
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0028, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0029, code lost:
        r6 = r2;
        r2 = r3;
        r3 = r1;
        r1 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:?, code lost:
        return r1;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0044 A[ExcHandler: FileNotFoundException (e java.io.FileNotFoundException), Splitter:B:7:0x0013] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static android.graphics.Bitmap safeDecodeBimtapFile(java.lang.String r7, android.graphics.BitmapFactory.Options r8) {
        /*
            r4 = 0
            if (r8 != 0) goto L_0x004c
            android.graphics.BitmapFactory$Options r0 = new android.graphics.BitmapFactory$Options
            r0.<init>()
            r1 = 1
            r0.inSampleSize = r1
        L_0x000b:
            r1 = 0
            r5 = r1
            r1 = r4
        L_0x000e:
            r2 = 5
            if (r5 < r2) goto L_0x0013
            r0 = r1
        L_0x0012:
            return r0
        L_0x0013:
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ OutOfMemoryError -> 0x0047, FileNotFoundException -> 0x0044 }
            r3.<init>(r7)     // Catch:{ OutOfMemoryError -> 0x0047, FileNotFoundException -> 0x0044 }
            r2 = 0
            android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeStream(r3, r2, r8)     // Catch:{ OutOfMemoryError -> 0x0028, FileNotFoundException -> 0x0044 }
            r3.close()     // Catch:{ IOException -> 0x0022 }
            r0 = r1
            goto L_0x0012
        L_0x0022:
            r2 = move-exception
            r2.printStackTrace()     // Catch:{ OutOfMemoryError -> 0x0028, FileNotFoundException -> 0x0044 }
            r0 = r1
            goto L_0x0012
        L_0x0028:
            r2 = move-exception
            r6 = r2
            r2 = r3
            r3 = r1
            r1 = r6
        L_0x002d:
            r1.printStackTrace()
            int r1 = r0.inSampleSize
            int r1 = r1 * 2
            r0.inSampleSize = r1
            r2.close()     // Catch:{ IOException -> 0x003f }
        L_0x0039:
            int r1 = r5 + 1
            r5 = r1
            r4 = r2
            r1 = r3
            goto L_0x000e
        L_0x003f:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0039
        L_0x0044:
            r0 = move-exception
            r0 = r1
            goto L_0x0012
        L_0x0047:
            r2 = move-exception
            r3 = r1
            r1 = r2
            r2 = r4
            goto L_0x002d
        L_0x004c:
            r0 = r8
            goto L_0x000b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sina.weibo.sdk.utils.ImageUtils.safeDecodeBimtapFile(java.lang.String, android.graphics.BitmapFactory$Options):android.graphics.Bitmap");
    }

    private static void delete(File file) {
        if (file != null && file.exists() && !file.delete()) {
            throw new RuntimeException(String.valueOf(file.getAbsolutePath()) + " doesn't be deleted!");
        }
    }

    private static boolean deleteDependon(String str) {
        boolean z = false;
        if (!TextUtils.isEmpty(str)) {
            File file = new File(str);
            int i = 1;
            if (file != null) {
                while (!z && i <= 5 && file.isFile() && file.exists()) {
                    z = file.delete();
                    if (!z) {
                        i++;
                    }
                }
            }
        }
        return z;
    }

    private static boolean isFileExisted(String str) {
        File file;
        if (!TextUtils.isEmpty(str) && (file = new File(str)) != null && file.exists()) {
            return true;
        }
        return false;
    }

    private static boolean isParentExist(File file) {
        File parentFile;
        if (file == null || (parentFile = file.getParentFile()) == null || parentFile.exists()) {
            return false;
        }
        if (file.exists() || file.mkdirs()) {
            return true;
        }
        return false;
    }

    private static void makesureFileExist(String str) {
        File file;
        if (str != null && (file = new File(str)) != null && !file.exists() && isParentExist(file)) {
            if (file.exists()) {
                delete(file);
            }
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static boolean isWifi(Context context) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo == null || activeNetworkInfo.getType() != 1) {
            return false;
        }
        return true;
    }
}
