package com.alimama.mobile.pluginframework.core;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import com.alimama.mobile.sdk.config.system.bridge.CMPluginBridge;

public class PluginServiceAgent {
    public ProviderShell provider;

    public PluginServiceAgent(ProviderShell providerShell) {
        this.provider = providerShell;
    }

    public static class ProviderShell {
        Object provider;

        public ProviderShell(Object obj) {
            this.provider = obj;
        }

        public IBinder onBind(Intent intent) {
            try {
                return (IBinder) CMPluginBridge.DownloadProvider_onBind.invoke(this.provider, intent);
            } catch (Exception e) {
                Log.e("PluginServiceContext", "invoke onBind error", e);
                return null;
            }
        }

        public int onStartCommand(Intent intent, int i, int i2) {
            try {
                return ((Integer) CMPluginBridge.DownloadProvider_onStartCommand.invoke(this.provider, intent, Integer.valueOf(i), Integer.valueOf(i2))).intValue();
            } catch (Exception e) {
                Log.e("PluginServiceContext", "invoke onStartCommand error", e);
                return 0;
            }
        }

        public void onCreate() {
            try {
                CMPluginBridge.DownloadProvider_onCreate.invoke(this.provider, new Object[0]);
            } catch (Exception e) {
                Log.e("PluginServiceContext", "invoke onCreate error", e);
            }
        }

        public void onDestroy() {
            try {
                CMPluginBridge.DownloadProvider_onDestroy.invoke(this.provider, new Object[0]);
            } catch (Exception e) {
                Log.e("PluginServiceContext", "invoke onDestroy error", e);
            }
        }

        public void setHostService(Service service) {
            try {
                CMPluginBridge.DownloadProvider_setHostService.invoke(this.provider, service);
            } catch (Exception e) {
                Log.e("PluginServiceContext", "invoke onDestroy error", e);
            }
        }
    }
}
