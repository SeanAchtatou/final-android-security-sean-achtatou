package com.alimama.mobile.pluginframework.core;

import android.content.pm.ApplicationInfo;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.util.Log;
import com.alimama.mobile.sdk.config.system.bridge.RuntimeBridge;
import com.alimama.mobile.sdk.hack.Hack;
import dalvik.system.DexClassLoader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

public class PluginFramework {
    public static String FRAMEWORK_PLUGIN_NAME = "FrameworkPlugin";
    private static String FRAMEWORK_PLUGIN_PREFIX = (FRAMEWORK_PLUGIN_NAME + "-");
    public static final String KEY_UPDATE_ACCESS = "access";
    public static final String KEY_UPDATE_DEVICEID = "deviceid";
    public static final String KEY_UPDATE_LAYOUTTYPE = "layouttype";
    public static final String KEY_UPDATE_OS = "os";
    public static final String KEY_UPDATE_OSV = "osv";
    public static final String KEY_UPDATE_SDKV = "sdkv";
    public static final String KEY_UPDATE_SLOTID = "slotid";
    public static String OPTIMIZED_DIR = "opt";
    public static String PLUGIN_DIR = "plugins";
    public static String PLUGIN_ROOT = "mmplugins";
    private static Class<?> frameworkLoader = null;
    private static AtomicBoolean inited = new AtomicBoolean(false);
    private static Set<String> mLoadedPlugins = new HashSet();

    public static Set<String> init(ApplicationInfo applicationInfo, AssetManager assetManager, String str, Resources resources, String str2) throws Exception {
        String str3;
        String str4;
        if (!inited.compareAndSet(false, true)) {
            return mLoadedPlugins;
        }
        try {
            File file = new File(str, PLUGIN_ROOT);
            File file2 = new File(file, PLUGIN_DIR);
            file.mkdir();
            file2.mkdir();
            File[] listFiles = file2.listFiles();
            int length = listFiles.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    str3 = null;
                    break;
                }
                File file3 = listFiles[i];
                if (file3.getName().startsWith(FRAMEWORK_PLUGIN_PREFIX)) {
                    str3 = file3.getAbsolutePath();
                    break;
                }
                i++;
            }
            String[] list = assetManager.list(str2);
            int length2 = list.length;
            int i2 = 0;
            while (true) {
                if (i2 >= length2) {
                    str4 = null;
                    break;
                }
                str4 = list[i2];
                Log.i("munion", "path = " + str4);
                if (str4.startsWith(FRAMEWORK_PLUGIN_PREFIX)) {
                    break;
                }
                i2++;
            }
            if (str3 == null || compareLessFrameworkPlugin(str3, str4)) {
                if (str4 == null) {
                    throw new Exception("plugin framework init fail. framework apk not found.");
                }
                InputStream open = assetManager.open(str2 + "/" + str4);
                byte[] bArr = new byte[open.available()];
                open.read(bArr);
                open.close();
                File file4 = new File(file2, str4);
                FileOutputStream fileOutputStream = new FileOutputStream(file4);
                fileOutputStream.write(bArr);
                fileOutputStream.close();
                str3 = file4.getAbsolutePath();
            }
            if (str3 == null) {
                throw new Exception("plugin framework init fail. framework apk not found.");
            }
            File file5 = new File(file, OPTIMIZED_DIR);
            file5.mkdir();
            Log.i("wt", "loadAPK:" + str3);
            DexClassLoader dexClassLoader = new DexClassLoader(str3, file5.getAbsolutePath(), null, PluginFramework.class.getClassLoader());
            frameworkLoader = dexClassLoader.loadClass("com.alimama.mobile.plugin.framework.FrameworkLoader");
            mLoadedPlugins.add(FRAMEWORK_PLUGIN_NAME);
            mLoadedPlugins.addAll((Set) RuntimeBridge.FrameworkLoader_invoke(frameworkLoader.getClassLoader(), applicationInfo, assetManager, str, resources, str2, dexClassLoader));
            return mLoadedPlugins;
        } catch (Exception e) {
            deletePlugins(str);
            throw e;
        } catch (Hack.HackDeclaration.HackAssertionException e2) {
            Log.e("wl", "加载插件失败。", e2);
            mLoadedPlugins.add(FRAMEWORK_PLUGIN_NAME);
            deletePlugins(str);
            return mLoadedPlugins;
        }
    }

    public static boolean compareLessFrameworkPlugin(String str, String str2) {
        String str3;
        if (str != null) {
            try {
                if (!"".equals(str)) {
                    String[] split = str.split("\\/");
                    String str4 = split[split.length - 1].split("-")[0];
                    String str5 = split[split.length - 1].split("-")[1];
                    if (str5 == null || "".equals(str5) || !str5.contains(".apk")) {
                        str3 = str5;
                    } else {
                        str3 = str5.substring(0, str5.lastIndexOf("."));
                    }
                    if (str2 != null && !"".equals(str2)) {
                        String str6 = str2.split("-")[0];
                        String str7 = str2.split("-")[1];
                        if (str7 != null && !"".equals(str7) && str7.contains(".apk")) {
                            str7 = str7.substring(0, str7.lastIndexOf("."));
                        }
                        if (str6.equals(str4) && str7.compareTo(str3) > 0) {
                            File file = new File(str);
                            if (file.exists()) {
                                file.delete();
                                return true;
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    private static void deletePlugins(String str) {
        File file = new File(new File(str, PLUGIN_ROOT), PLUGIN_DIR);
        if (file != null) {
            file.delete();
        }
    }

    public static AssetManager getPluginAssetManager() throws Exception {
        if (frameworkLoader != null) {
            return (AssetManager) frameworkLoader.getMethod("getPluginAssetManager", new Class[0]).invoke(frameworkLoader, new Object[0]);
        }
        throw new Exception("plugin framework not init...");
    }

    public static ClassLoader getPluginClassLoader() throws Exception {
        if (frameworkLoader != null) {
            return (ClassLoader) frameworkLoader.getMethod("getPluginClassLoader", new Class[0]).invoke(frameworkLoader, new Object[0]);
        }
        Log.i("wt", "==null");
        throw new Exception("plugin framework not init...");
    }

    public static Resources getPluginResources() throws Exception {
        if (frameworkLoader != null) {
            return (Resources) frameworkLoader.getMethod("getPluginResources", new Class[0]).invoke(frameworkLoader, new Object[0]);
        }
        throw new Exception("plugin framework not init...");
    }
}
