package com.alimama.mobile.pluginframework.core;

import android.app.Service;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.util.Log;
import com.alimama.mobile.sdk.config.system.bridge.AndroidBridgeHacks;
import com.alimama.mobile.sdk.config.system.bridge.FrameworkBridge;

public abstract class PluginService extends Service {
    protected PluginServiceAgent pluginServiceAgent = null;

    public abstract PluginServiceAgent findProvider();

    public void onCreate() {
        try {
            this.pluginServiceAgent = findProvider();
            this.pluginServiceAgent.provider.setHostService(this);
            super.onCreate();
        } catch (Exception e) {
            Log.e("Download", "", e);
        }
    }

    public AssetManager getAssets() {
        if (this.pluginServiceAgent == null) {
            return super.getAssets();
        }
        try {
            return (AssetManager) FrameworkBridge.FrameworkLoader_getPluginAssetManager.invoke(null, new Object[0]);
        } catch (Exception e) {
            return super.getAssets();
        }
    }

    public Resources getResources() {
        if (this.pluginServiceAgent == null) {
            return super.getResources();
        }
        try {
            return (Resources) FrameworkBridge.FrameworkLoader_getPluginResources.invoke(null, new Object[0]);
        } catch (Exception e) {
            return super.getResources();
        }
    }

    public Resources.Theme getTheme() {
        Resources.Theme theme = super.getTheme();
        try {
            AndroidBridgeHacks.THeme_mAssets.on(theme).set(getAssets());
        } catch (Exception e) {
            Log.e("wt", "", e);
        }
        return theme;
    }

    public ClassLoader getClassLoader() {
        if (this.pluginServiceAgent == null) {
            return super.getClassLoader();
        }
        try {
            return (ClassLoader) FrameworkBridge.FrameworkLoader_getPluginClassLoader.invoke(null, new Object[0]);
        } catch (Exception e) {
            return super.getClassLoader();
        }
    }
}
