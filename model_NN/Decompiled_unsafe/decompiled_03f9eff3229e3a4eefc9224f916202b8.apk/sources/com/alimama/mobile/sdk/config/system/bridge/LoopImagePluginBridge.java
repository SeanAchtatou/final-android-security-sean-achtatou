package com.alimama.mobile.sdk.config.system.bridge;

import android.content.Context;
import android.util.Log;
import android.view.ViewGroup;
import com.alimama.mobile.pluginframework.core.PluginFramework;
import com.alimama.mobile.sdk.config.LoopImageController;
import com.alimama.mobile.sdk.config.system.BridgeSystem;
import com.alimama.mobile.sdk.hack.AssertionArrayException;
import com.alimama.mobile.sdk.hack.Hack;
import java.lang.reflect.InvocationTargetException;

public class LoopImagePluginBridge implements BridgeSystem.HackCollection, Hack.AssertionFailureHandler {
    private static LoopImagePluginBridge mBridge;
    private Hack.HackedClass<Object> LargeImage;
    private Hack.HackedClass<Object> LargeImageManager;
    private Hack.HackedMethod LargeImageManager_getProduct;
    private Hack.HackedMethod LargeImageManager_hasProduct;
    private Hack.HackedMethod LargeImageManager_incubate;
    private Hack.HackedMethod LargeImageManager_init;
    private Hack.HackedConstructor LargeImageManager_obj;
    private Hack.HackedMethod LargeImageManager_setIncubatedListener;
    private Hack.HackedMethod LargeImage_existCachePromoter;
    private Hack.HackedMethod LargeImage_existOnlinePromoter;
    private Hack.HackedMethod LargeImage_getDataService;
    private Hack.HackedMethod LargeImage_getPromoters;
    private Hack.HackedMethod LoopImageReflect_setTitle;
    private Hack.HackedClass<Object> loopImageReflect;
    private AssertionArrayException mExceptionArray = null;
    private Object mLargeImageManager;

    private LoopImagePluginBridge() {
    }

    public boolean hasInitManager() {
        if (this.mLargeImageManager != null) {
            return true;
        }
        return false;
    }

    public void initLargeImageManager() {
        this.mLargeImageManager = this.LargeImageManager_obj.getInstance(CMPluginBridge.getAppContext());
    }

    public void invoke_LargeImageManager_init(String str) {
        try {
            this.LargeImageManager_init.invoke(this.mLargeImageManager, str);
        } catch (IllegalArgumentException e) {
            throw new RuntimeException("Hack调用失败", e);
        } catch (InvocationTargetException e2) {
            throw new RuntimeException("Hack调用失败", e2);
        }
    }

    public void invoke_LargeImageManager_incubate() {
        try {
            this.LargeImageManager_incubate.invoke(this.mLargeImageManager, new Object[0]);
        } catch (IllegalArgumentException e) {
            throw new RuntimeException("Hack调用失败", e);
        } catch (InvocationTargetException e2) {
            throw new RuntimeException("Hack调用失败", e2);
        }
    }

    public void invoke_LargeImageManager_setIncubatedListener(LoopImageController.IncubatedListener incubatedListener) {
        try {
            this.LargeImageManager_setIncubatedListener.invoke(this.mLargeImageManager, incubatedListener);
        } catch (IllegalArgumentException e) {
            throw new RuntimeException("Hack调用失败", e);
        } catch (InvocationTargetException e2) {
            throw new RuntimeException("Hack调用失败", e2);
        }
    }

    public Object invoke_LargeImageManager_hasProduct() {
        try {
            return this.LargeImageManager_hasProduct.invoke(this.mLargeImageManager, new Object[0]);
        } catch (IllegalArgumentException e) {
            throw new RuntimeException("Hack调用失败", e);
        } catch (InvocationTargetException e2) {
            throw new RuntimeException("Hack调用失败", e2);
        }
    }

    public Object invoke_LargeImageManager_getProduct() {
        try {
            return this.LargeImageManager_getProduct.invoke(this.mLargeImageManager, new Object[0]);
        } catch (IllegalArgumentException e) {
            throw new RuntimeException("Hack调用失败", e);
        } catch (InvocationTargetException e2) {
            throw new RuntimeException("Hack调用失败", e2);
        }
    }

    public Object invoke_LargeImage_existOnlinePromoter(Object obj) {
        try {
            return this.LargeImage_existOnlinePromoter.invoke(obj, new Object[0]);
        } catch (IllegalArgumentException e) {
            throw new RuntimeException("Hack调用失败", e);
        } catch (InvocationTargetException e2) {
            throw new RuntimeException("Hack调用失败", e2);
        }
    }

    public Object invoke_LargeImage_existCachePromoter(Object obj) {
        try {
            return this.LargeImage_existCachePromoter.invoke(obj, new Object[0]);
        } catch (IllegalArgumentException e) {
            throw new RuntimeException("Hack调用失败", e);
        } catch (InvocationTargetException e2) {
            throw new RuntimeException("Hack调用失败", e2);
        }
    }

    public Object invoke_LargeImage_getPromoters(Object obj) {
        try {
            return this.LargeImage_getPromoters.invoke(obj, new Object[0]);
        } catch (IllegalArgumentException e) {
            throw new RuntimeException("Hack调用失败", e);
        } catch (InvocationTargetException e2) {
            throw new RuntimeException("Hack调用失败", e2);
        }
    }

    public Object invoke_LargeImage_getDataService(Object obj, Context context) {
        try {
            return this.LargeImage_getDataService.invoke(obj, context);
        } catch (IllegalArgumentException e) {
            throw new RuntimeException("Hack调用失败", e);
        } catch (InvocationTargetException e2) {
            throw new RuntimeException("Hack调用失败", e2);
        }
    }

    public static LoopImagePluginBridge getInstance() {
        boolean z;
        if (mBridge == null) {
            LoopImagePluginBridge loopImagePluginBridge = new LoopImagePluginBridge();
            try {
                Hack.setAssertionFailureHandler(loopImagePluginBridge);
                loopImagePluginBridge.allClasses();
                loopImagePluginBridge.allMethods();
                loopImagePluginBridge.allFields();
                if (loopImagePluginBridge.mExceptionArray != null) {
                    z = false;
                } else {
                    z = true;
                }
                Hack.setAssertionFailureHandler(null);
                if (z) {
                    mBridge = loopImagePluginBridge;
                } else if (mBridge != null) {
                    Log.e("Hack", "Create loopImagePluginBridge failed.", mBridge.mExceptionArray);
                } else if (loopImagePluginBridge != null) {
                    Log.e("Hack", "Create loopImagePluginBridge failed.", loopImagePluginBridge.mExceptionArray);
                }
            } catch (Hack.HackDeclaration.HackAssertionException e) {
                Log.e("Hack", "HackAssertionException", e);
                Hack.setAssertionFailureHandler(null);
                if (mBridge != null) {
                    Log.e("Hack", "Create loopImagePluginBridge failed.", mBridge.mExceptionArray);
                } else if (loopImagePluginBridge != null) {
                    Log.e("Hack", "Create loopImagePluginBridge failed.", loopImagePluginBridge.mExceptionArray);
                }
            } catch (Throwable th) {
                Hack.setAssertionFailureHandler(null);
                if (mBridge != null) {
                    Log.e("Hack", "Create loopImagePluginBridge failed.", mBridge.mExceptionArray);
                } else if (loopImagePluginBridge != null) {
                    Log.e("Hack", "Create loopImagePluginBridge failed.", loopImagePluginBridge.mExceptionArray);
                }
                throw th;
            }
        }
        return mBridge;
    }

    private static ClassLoader getClassLoader() {
        try {
            return PluginFramework.getPluginClassLoader();
        } catch (Exception e) {
            Log.e("wt", "", e);
            return null;
        }
    }

    public void allClasses() throws Hack.HackDeclaration.HackAssertionException {
        this.loopImageReflect = Hack.into(getClassLoader(), "com.taobao.newxp.view.largeimage.LoopImageReflect");
        this.LargeImageManager = Hack.into(getClassLoader(), "com.taobao.newxp.view.largeimage.LargeImageManager");
        this.LargeImage = Hack.into(getClassLoader(), "com.taobao.newxp.view.largeimage.LargeImage");
    }

    public void allMethods() throws Hack.HackDeclaration.HackAssertionException {
        this.LoopImageReflect_setTitle = this.loopImageReflect.staticMethod("setTitle", ViewGroup.class, String.class);
        this.LargeImageManager_init = this.LargeImageManager.method("init", String.class);
        this.LargeImageManager_incubate = this.LargeImageManager.method("incubate", new Class[0]);
        this.LargeImageManager_getProduct = this.LargeImageManager.method("getProduct", new Class[0]);
        this.LargeImageManager_hasProduct = this.LargeImageManager.method("hasProduct", new Class[0]);
        this.LargeImageManager_setIncubatedListener = this.LargeImageManager.method("setIncubatedListener", LoopImageController.IncubatedListener.class);
        this.LargeImageManager_obj = this.LargeImageManager.constructor(Context.class);
        this.LargeImage_existOnlinePromoter = this.LargeImage.method("existOnlinePromoter", new Class[0]);
        this.LargeImage_existCachePromoter = this.LargeImage.method("existCachePromoter", new Class[0]);
        this.LargeImage_getPromoters = this.LargeImage.method("getPromoters", new Class[0]);
        this.LargeImage_getDataService = this.LargeImage.method("getDataService", Context.class);
    }

    public void allFields() throws Hack.HackDeclaration.HackAssertionException {
    }

    public void invoke_setTitle(ViewGroup viewGroup, String str) {
        try {
            if (this.LoopImageReflect_setTitle != null) {
                this.LoopImageReflect_setTitle.invoke(this.loopImageReflect, viewGroup, str);
            }
        } catch (InvocationTargetException e) {
            throw new RuntimeException("Hack调用失败", e);
        }
    }

    public boolean onAssertionFailure(Hack.HackDeclaration.HackAssertionException hackAssertionException) {
        if (this.mExceptionArray == null) {
            Log.i("LoopImagePluginBridge", "onassertionFailure is null");
            this.mExceptionArray = new AssertionArrayException("loopImagePluginBridge hack failed");
        }
        this.mExceptionArray.addException(hackAssertionException);
        Log.i("LoopImagePluginBridge", "onassertionFailure in loopimagepluginbridge");
        return true;
    }
}
