package com.alimama.mobile.sdk.config.system;

import android.app.Activity;
import android.content.res.AssetManager;
import com.alimama.mobile.pluginframework.core.PluginFramework;
import com.alimama.mobile.sdk.MmuSDK;
import com.alimama.mobile.sdk.config.ContainerProperties;
import com.alimama.mobile.sdk.config.MmuProperties;
import com.alimama.mobile.sdk.config.MmuSDKFactory;
import com.alimama.mobile.sdk.config.system.MMLog;
import java.io.IOException;
import java.util.Arrays;

public class STSystemImpl implements STSystem {
    public boolean stManifest() {
        return true;
    }

    public boolean stAssetPlugin(AssetManager assetManager) {
        try {
            for (String startsWith : assetManager.list("mu")) {
                if (startsWith.startsWith("FrameworkPlugin")) {
                    return true;
                }
            }
        } catch (IOException e) {
            MMLog.e("", e);
        }
        return false;
    }

    public boolean stLoadedCommonPlugin() {
        return ((MmuSDKImpl) MmuSDKFactory.getMmuSDK()).loadplugin("CommonPlugin");
    }

    public boolean stLoadedFrameWorkPlugin() {
        return ((MmuSDKImpl) MmuSDKFactory.getMmuSDK()).loadplugin(PluginFramework.FRAMEWORK_PLUGIN_NAME);
    }

    public <T extends MmuProperties> boolean stPlugin(T t) {
        MMLog.MarkerLog markerLog = null;
        String str = "";
        if (MMLog.DEBUG) {
            markerLog = new MMLog.MarkerLog();
        }
        boolean stPlugin_featureploginLoad = stPlugin_featureploginLoad(t);
        if (markerLog != null) {
            markerLog.add(String.format("[%s]检查必要的Feature插件是否加载成功,{ContainerProperties=%s}", Boolean.valueOf(stPlugin_featureploginLoad), Arrays.toString(t.getPluginNames())), Thread.currentThread().getId());
        }
        if (stPlugin_featureploginLoad) {
            stPlugin_featureploginLoad = stPlugin_loadComplete();
            if (markerLog != null) {
                str = "Container check";
                markerLog.add(String.format("[%s]检查必要插件[CommonPlugin,FrameworkPlugin]是否加载成功", Boolean.valueOf(stPlugin_featureploginLoad), Arrays.toString(t.getPluginNames())), Thread.currentThread().getId());
            }
        }
        if (t instanceof ContainerProperties) {
            ContainerProperties containerProperties = (ContainerProperties) t;
            if (stPlugin_featureploginLoad) {
                stPlugin_featureploginLoad = stPlugin_containsHookActivity(containerProperties.activity);
                if (markerLog != null) {
                    markerLog.add(String.format("[%s]检查是否完成宿主Activity初始化,[activity=%s]", Boolean.valueOf(stPlugin_featureploginLoad), containerProperties.activity.getClass().getCanonicalName()), Thread.currentThread().getId());
                }
            }
        }
        if (markerLog != null) {
            markerLog.finish(str);
        }
        return stPlugin_featureploginLoad;
    }

    private boolean stPlugin_loadComplete() {
        return MmuSDKFactory.getMmuSDK().getStatus() == MmuSDK.PLUGIN_LOAD_STATUS.COMPLETED;
    }

    private boolean stPlugin_containsHookActivity(Activity activity) {
        return HookManager.containsHookActivity(activity);
    }

    private boolean stPlugin_featureploginLoad(MmuProperties mmuProperties) {
        String[] pluginNames = mmuProperties.getPluginNames();
        if (pluginNames == null || pluginNames.length == 0) {
            MMLog.i("MmuProperties[%s] 没有设置必要的Feature插件检查.", mmuProperties.getClass().getCanonicalName());
            return true;
        }
        MmuSDKImpl mmuSDKImpl = (MmuSDKImpl) MmuSDKFactory.getMmuSDK();
        for (String loadplugin : pluginNames) {
            if (!mmuSDKImpl.loadplugin(loadplugin)) {
                return false;
            }
        }
        return true;
    }
}
