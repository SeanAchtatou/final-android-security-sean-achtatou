package com.alimama.mobile.sdk.hack;

import com.alimama.mobile.sdk.hack.Hack;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class AssertionArrayException extends Exception {
    private static final long serialVersionUID = 1;
    private List<Hack.HackDeclaration.HackAssertionException> mAssertionErr = new ArrayList();

    public AssertionArrayException(String str) {
        super(str);
    }

    public void addException(Hack.HackDeclaration.HackAssertionException hackAssertionException) {
        this.mAssertionErr.add(hackAssertionException);
    }

    public void addException(List<Hack.HackDeclaration.HackAssertionException> list) {
        this.mAssertionErr.addAll(list);
    }

    public List<Hack.HackDeclaration.HackAssertionException> getExceptions() {
        return this.mAssertionErr;
    }

    public static AssertionArrayException mergeException(AssertionArrayException assertionArrayException, AssertionArrayException assertionArrayException2) {
        if (assertionArrayException == null) {
            return assertionArrayException2;
        }
        if (assertionArrayException2 == null) {
            return assertionArrayException;
        }
        AssertionArrayException assertionArrayException3 = new AssertionArrayException(assertionArrayException.getMessage() + ";" + assertionArrayException2.getMessage());
        assertionArrayException3.addException(assertionArrayException.getExceptions());
        assertionArrayException3.addException(assertionArrayException2.getExceptions());
        return assertionArrayException3;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Hack.HackDeclaration.HackAssertionException next : this.mAssertionErr) {
            sb.append(next.toString()).append(";");
            try {
                if (next.getCause() instanceof NoSuchFieldException) {
                    Field[] declaredFields = next.getHackedClass().getDeclaredFields();
                    sb.append(next.getHackedClass().getName()).append(".").append(next.getHackedFieldName()).append(";");
                    for (Field name : declaredFields) {
                        sb.append(name.getName()).append("/");
                    }
                } else if (next.getCause() instanceof NoSuchMethodException) {
                    Method[] declaredMethods = next.getHackedClass().getDeclaredMethods();
                    sb.append(next.getHackedClass().getName()).append("->").append(next.getHackedMethodName()).append(";");
                    for (int i = 0; i < declaredMethods.length; i++) {
                        if (next.getHackedMethodName().equals(declaredMethods[i].getName())) {
                            sb.append(declaredMethods[i].toGenericString()).append("/");
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            sb.append("@@@@");
        }
        return sb.toString();
    }
}
