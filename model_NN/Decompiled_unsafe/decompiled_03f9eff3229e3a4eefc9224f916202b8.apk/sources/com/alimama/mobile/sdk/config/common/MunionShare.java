package com.alimama.mobile.sdk.config.common;

import android.app.Activity;
import cn.banshenggua.aichang.utils.Constants;
import com.alimama.mobile.sdk.config.system.bridge.CMPluginBridge;
import com.alimama.mobile.sdk.hack.Hack;

public abstract class MunionShare {
    public static MunionShare DEFAULT_SHARE = null;

    public enum SHARE_PLATFORM {
        WEIXIN_TIMELINE {
            public String toString() {
                return "wxtimeline";
            }
        },
        WEIXIN_FRIEND {
            public String toString() {
                return "wxsession";
            }
        },
        SINA_WEIBO {
            public String toString() {
                return Constants.WEIBO;
            }
        },
        MOMO {
            public String toString() {
                return "momo";
            }
        },
        FACEBOOK {
            public String toString() {
                return "facebook";
            }
        },
        TWITTER {
            public String toString() {
                return "twitter";
            }
        },
        MAIL {
            public String toString() {
                return "mail";
            }
        },
        SMS {
            public String toString() {
                return "sms";
            }
        },
        OTHER {
            public String toString() {
                return "other";
            }
        }
    }

    public abstract void shareUI(Activity activity, String str, String str2, String str3);

    public final void sendPlatform(SHARE_PLATFORM share_platform, boolean z) {
        int i = 1;
        try {
            Hack.HackedMethod hackedMethod = CMPluginBridge.ShareUtils_sendPlatformClick;
            Object[] objArr = new Object[3];
            objArr[0] = "publisher";
            objArr[1] = share_platform.toString();
            if (!z) {
                i = 0;
            }
            objArr[2] = Integer.valueOf(i);
            hackedMethod.invoke(null, objArr);
        } catch (Exception e) {
        }
    }
}
