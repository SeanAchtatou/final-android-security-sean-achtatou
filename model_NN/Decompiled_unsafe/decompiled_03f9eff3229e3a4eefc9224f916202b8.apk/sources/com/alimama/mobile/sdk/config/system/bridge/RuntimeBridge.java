package com.alimama.mobile.sdk.config.system.bridge;

import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.res.AssetManager;
import android.content.res.Resources;
import com.alimama.mobile.sdk.hack.Hack;
import dalvik.system.DexClassLoader;
import java.lang.reflect.InvocationTargetException;

public class RuntimeBridge {
    private static final String AlimamaJSdkController = "com.taobao.munion.view.webview.windvane.jsdk.AlimamaJSdkController";
    private static final String FrameworkLoader = "com.alimama.mobile.plugin.framework.FrameworkLoader";
    private static final String WVCamera = "com.taobao.munion.view.webview.windvane.jsdk.WVCamera";

    public static Object FrameworkLoader_invoke(ClassLoader classLoader, ApplicationInfo applicationInfo, AssetManager assetManager, String str, Resources resources, String str2, DexClassLoader dexClassLoader) throws Hack.HackDeclaration.HackAssertionException, InvocationTargetException {
        return Hack.into(classLoader, FrameworkLoader).method("init", ApplicationInfo.class, AssetManager.class, String.class, Resources.class, String.class, ClassLoader.class).invoke(null, applicationInfo, assetManager, str, resources, str2, dexClassLoader);
    }

    public static void alimamaJSdkController_onResume(ClassLoader classLoader) throws Hack.HackDeclaration.HackAssertionException, InvocationTargetException {
        Hack.HackedClass into = Hack.into(classLoader, AlimamaJSdkController);
        into.method("onResume", new Class[0]).invoke(into.method("getInstance", new Class[0]).invoke(null, new Object[0]), new Object[0]);
    }

    public static void alimamaJSdkController_onPause(ClassLoader classLoader) throws Hack.HackDeclaration.HackAssertionException, InvocationTargetException {
        Hack.HackedClass into = Hack.into(classLoader, AlimamaJSdkController);
        into.method("onPause", new Class[0]).invoke(into.method("getInstance", new Class[0]).invoke(null, new Object[0]), new Object[0]);
    }

    public static void alimamaJSdkController_onDestroy(ClassLoader classLoader) throws Hack.HackDeclaration.HackAssertionException, InvocationTargetException {
        Hack.HackedClass into = Hack.into(classLoader, AlimamaJSdkController);
        into.method("onDestroy", new Class[0]).invoke(into.method("getInstance", new Class[0]).invoke(null, new Object[0]), new Object[0]);
    }

    public static void alimamaJSdkController_onActivityResult(ClassLoader classLoader, int i, int i2, Intent intent) throws Hack.HackDeclaration.HackAssertionException, InvocationTargetException {
        Hack.HackedClass into = Hack.into(classLoader, WVCamera);
        Object invoke = into.method("getInstance", new Class[0]).invoke(null, new Object[0]);
        into.method("onActivityResult", Integer.TYPE, Integer.TYPE, Intent.class).invoke(invoke, Integer.valueOf(i), Integer.valueOf(i2), intent);
    }
}
