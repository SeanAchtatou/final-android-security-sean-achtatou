package com.alimama.mobile.sdk.config.system;

import android.content.res.AssetManager;
import com.alimama.mobile.sdk.config.MmuProperties;

interface STSystem {

    public static final class STSException extends Exception {
        private static final long serialVersionUID = 1;
    }

    boolean stAssetPlugin(AssetManager assetManager);

    boolean stLoadedCommonPlugin();

    boolean stLoadedFrameWorkPlugin();

    boolean stManifest();

    <T extends MmuProperties> boolean stPlugin(T t);
}
