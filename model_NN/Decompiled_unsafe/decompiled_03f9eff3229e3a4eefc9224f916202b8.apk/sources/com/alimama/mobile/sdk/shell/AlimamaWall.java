package com.alimama.mobile.sdk.shell;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.KeyEvent;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.alimama.mobile.sdk.config.MmuSDKFactory;
import com.alimama.mobile.sdk.config.system.MMLog;
import com.alimama.mobile.sdk.config.system.MmuSDKImpl;
import com.alimama.mobile.sdk.config.system.bridge.CMPluginBridge;
import java.util.HashMap;
import java.util.Map;

public class AlimamaWall extends FragmentActivity {
    public static Object extra = null;
    Fragment mFragment;
    Map<String, Object> map;
    private MmuSDKImpl mmuSDK;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        Bundle bundleExtra = getIntent().getBundleExtra("msg");
        this.map = new HashMap();
        this.map.put("handler", bundleExtra);
        if (extra != null) {
            this.map.put("preload", extra);
            extra = null;
        }
        try {
            super.onCreate(bundle);
        } catch (Fragment.InstantiationException e) {
            MMLog.e(e, "", new Object[0]);
            finish();
        }
        try {
            FrameLayout frameLayout = new FrameLayout(this);
            frameLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
            frameLayout.setId(16908300);
            requestWindowFeature(1);
            setContentView(frameLayout);
            this.mmuSDK = (MmuSDKImpl) MmuSDKFactory.getMmuSDK();
            this.mFragment = this.mmuSDK.getApfSystem().findFragment(this.map);
            FragmentTransaction beginTransaction = getSupportFragmentManager().beginTransaction();
            beginTransaction.add(16908300, this.mFragment);
            beginTransaction.commit();
        } catch (Exception e2) {
            Log.e("AlimamaWall", "", e2);
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        if (!this.mmuSDK.accountServiceHandleResult(i, i2, intent, this)) {
            super.onActivityResult(i, i2, intent);
        }
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        try {
            if (((Boolean) CMPluginBridge.BaseFragment_dispatchKeyEvent.invoke(this.mFragment, keyEvent)).booleanValue()) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return super.dispatchKeyEvent(keyEvent);
    }
}
