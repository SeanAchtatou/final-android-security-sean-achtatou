package com.alimama.mobile.sdk.config;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.util.Log;
import android.view.View;
import com.alimama.mobile.sdk.config.system.bridge.CMPluginBridge;
import com.alimama.mobile.sdk.config.system.bridge.FeedPluginBridge;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

public class FeedController {

    public enum FEED_STYLE {
        SINGLE_BIGIMG,
        SINGLE_APP,
        SINGLE_TUAN,
        GROUP_ICON_APP,
        GROUP_ICON_TB,
        TEXT_ICON,
        TEXT_BANNER,
        CUSTOM_STYLE_APP,
        CUSTOM_STYLE_TB
    }

    public interface IncubatedListener {
        public static final int STATUS_FAIL = 0;
        public static final int STATUS_SUCCESS = 1;
        public static final int STATUS_SUCCESS_CACHE = 2;

        void onClick();

        void onComplete(int i, String str);
    }

    public interface LazyDataCallback {
        void onReviced(MMFeed mMFeed);
    }

    public MMFeed getProduct(String str) {
        try {
            Object invoke_getProduct = FeedPluginBridge.getInstance().invoke_getProduct(str);
            if (invoke_getProduct != null) {
                return new MMFeed(invoke_getProduct);
            }
            return null;
        } catch (Exception e) {
            Log.e(ExchangeConstants.LOG_TAG, "", e);
            return null;
        }
    }

    public void setIncubatedListener(IncubatedListener incubatedListener) {
        try {
            FeedPluginBridge.getInstance().invoke_setIncubatedListener(incubatedListener);
        } catch (Exception e) {
            Log.e(ExchangeConstants.LOG_TAG, "", e);
        }
    }

    public View getFeedView(Activity activity, MMFeed mMFeed) {
        return FeedPluginBridge.getInstance().invoke_getFeedView(activity, mMFeed);
    }

    public void setLazyDataCallback(MMFeed mMFeed, LazyDataCallback lazyDataCallback) {
        FeedPluginBridge.getInstance().invoke_setLazyDataCallback(mMFeed, lazyDataCallback);
    }

    public void updateLocation(Location location) {
        ExchangeConstants.setManuallocation(location);
    }

    public static class MMFeed {
        protected Object feed;
        private Object mService;

        public MMFeed(Object obj) {
            this.feed = obj;
        }

        public void reportImpression(Activity activity, MMPromoter... mMPromoterArr) {
            try {
                if (this.mService == null) {
                    this.mService = FeedPluginBridge.getInstance().Feed_getDataService.invoke(this.feed, CMPluginBridge.getAppContext());
                }
                ArrayList arrayList = null;
                if (mMPromoterArr != null) {
                    arrayList = new ArrayList();
                    for (MMPromoter mMPromoter : mMPromoterArr) {
                        arrayList.add(mMPromoter.promoter);
                    }
                }
                CMPluginBridge.ExchangeDataService_reportImpression.invoke(this.mService, arrayList);
            } catch (InvocationTargetException e) {
                throw new RuntimeException("Hack调用失败", e);
            }
        }

        public void clickOnPromoter(Activity activity, MMPromoter mMPromoter) {
            try {
                if (this.mService == null) {
                    Context appContext = CMPluginBridge.getAppContext();
                    this.mService = FeedPluginBridge.getInstance().Feed_getDataService.invoke(this.feed, appContext);
                }
                CMPluginBridge.ExchangeDataService_clickOnPromoter.invoke(this.mService, activity, mMPromoter.promoter);
            } catch (InvocationTargetException e) {
                throw new RuntimeException("Hack调用失败", e);
            }
        }

        public void cleanReportFlag() {
            try {
                FeedPluginBridge.getInstance().Feed_cleanReportFlag.invoke(this.feed, new Object[0]);
            } catch (InvocationTargetException e) {
                throw new RuntimeException("Hack调用失败", e);
            }
        }

        public FEED_STYLE getStyle() {
            try {
                return FEED_STYLE.valueOf(FeedPluginBridge.getInstance().Feed_getStyle.invoke(this.feed, new Object[0]).toString());
            } catch (InvocationTargetException e) {
                throw new RuntimeException("Hack调用失败", e);
            }
        }

        public boolean existOnlineFeed() {
            try {
                return ((Boolean) FeedPluginBridge.getInstance().Feed_existOnlineFeed.invoke(this.feed, new Object[0])).booleanValue();
            } catch (InvocationTargetException e) {
                throw new RuntimeException("Hack调用失败", e);
            }
        }

        public boolean existCacheFeed() {
            try {
                return ((Boolean) FeedPluginBridge.getInstance().Feed_existCacheFeed.invoke(this.feed, new Object[0])).booleanValue();
            } catch (InvocationTargetException e) {
                throw new RuntimeException("Hack调用失败", e);
            }
        }

        public List<MMPromoter> getPromoters() {
            try {
                List<Object> list = (List) FeedPluginBridge.getInstance().Feed_getPromoters.invoke(this.feed, new Object[0]);
                if (list == null) {
                    return null;
                }
                ArrayList arrayList = new ArrayList();
                for (Object mMPromoter : list) {
                    arrayList.add(new MMPromoter(mMPromoter));
                }
                return arrayList;
            } catch (InvocationTargetException e) {
                throw new RuntimeException("Hack调用失败", e);
            }
        }

        public List<MMPromoter> getExtraPromoters() {
            try {
                List<Object> list = (List) FeedPluginBridge.getInstance().Feed_getExtraPromoters.invoke(this.feed, new Object[0]);
                if (list == null) {
                    return null;
                }
                ArrayList arrayList = new ArrayList();
                for (Object mMPromoter : list) {
                    arrayList.add(new MMPromoter(mMPromoter));
                }
                return arrayList;
            } catch (InvocationTargetException e) {
                throw new RuntimeException("Hack调用失败", e);
            }
        }

        public boolean isReady() {
            try {
                return ((Boolean) FeedPluginBridge.getInstance().Feed_isReady.invoke(this.feed, new Object[0])).booleanValue();
            } catch (InvocationTargetException e) {
                throw new RuntimeException("Hack调用失败", e);
            }
        }

        public void reflushData() {
            try {
                FeedPluginBridge.getInstance().Feed_reflushData.invoke(FeedPluginBridge.getInstance().MMFeed_feed.on(this).get(), new Object[0]);
            } catch (InvocationTargetException e) {
                throw new RuntimeException("Hack调用失败", e);
            }
        }

        public Object getTag() {
            try {
                return FeedPluginBridge.getInstance().Feed_getTag.invoke(FeedPluginBridge.getInstance().MMFeed_feed.on(this).get(), new Object[0]);
            } catch (InvocationTargetException e) {
                throw new RuntimeException("Hack调用失败", e);
            }
        }

        public void setObj(Object obj) {
            try {
                FeedPluginBridge.getInstance().Feed_obj.on(FeedPluginBridge.getInstance().MMFeed_feed.on(this).get()).set(obj);
            } catch (Exception e) {
                throw new RuntimeException("Hack调用失败", e);
            }
        }

        public Object getObj() {
            try {
                return FeedPluginBridge.getInstance().Feed_obj.on(FeedPluginBridge.getInstance().MMFeed_feed.on(this).get()).get();
            } catch (Exception e) {
                throw new RuntimeException("Hack调用失败", e);
            }
        }

        public String getSlotId() {
            try {
                return (String) FeedPluginBridge.getInstance().Feed_getSlotId.invoke(FeedPluginBridge.getInstance().MMFeed_feed.on(this).get(), new Object[0]);
            } catch (Exception e) {
                throw new RuntimeException("Hack调用失败", e);
            }
        }

        public String toString() {
            Object[] objArr = new Object[9];
            objArr[0] = getSlotId();
            objArr[1] = getStyle().toString();
            objArr[2] = existOnlineFeed() ? "True" : "False";
            objArr[3] = existCacheFeed() ? "True" : "False";
            objArr[4] = getPromoters() == null ? "0" : Integer.valueOf(getPromoters().size());
            objArr[5] = getExtraPromoters() == null ? "0" : Integer.valueOf(getExtraPromoters().size());
            objArr[6] = isReady() ? "True" : "Fasle";
            objArr[7] = getTag() == null ? "null" : getTag().toString();
            objArr[8] = getObj() == null ? "null" : getObj().toString();
            return String.format("MMFeed:\r{\r     Slot[%s]\r     Style[%s]\r     existOnlineFeed[%s]\r     existCacheFeed[%s]\r     Promoter[%s]\r     ExtraPromoter[%s]\r     isReady[%s]\r     Tag[%s]\r     Obj[%s]\r}", objArr);
        }
    }
}
