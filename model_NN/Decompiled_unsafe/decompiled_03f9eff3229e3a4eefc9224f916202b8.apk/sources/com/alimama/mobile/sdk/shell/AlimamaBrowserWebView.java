package com.alimama.mobile.sdk.shell;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.alimama.mobile.sdk.config.MmuSDKFactory;
import com.alimama.mobile.sdk.config.system.bridge.CMPluginBridge;

public class AlimamaBrowserWebView extends FragmentActivity {
    private Fragment mFragment;
    String url = "";

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        String str;
        String str2;
        boolean z = true;
        super.onCreate(bundle);
        try {
            MmuSDKFactory.getMmuSDK().accountServiceInit(this);
            FrameLayout frameLayout = new FrameLayout(this);
            frameLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
            frameLayout.setId(16908300);
            requestWindowFeature(1);
            setContentView(frameLayout);
            if (getIntent().hasExtra("url")) {
                this.url = getIntent().getExtras().getString("url");
            }
            if (getIntent().hasExtra("referer")) {
                str = getIntent().getExtras().getString("referer");
            } else {
                str = "";
            }
            if (getIntent().hasExtra("interceptUrl")) {
                str2 = getIntent().getExtras().getString("interceptUrl");
            } else {
                str2 = "";
            }
            String str3 = "";
            if (getIntent().hasExtra("onUrlInterceptCallBack")) {
                str3 = getIntent().getExtras().getString("onUrlInterceptCallBack");
            }
            if (getIntent().hasExtra("report")) {
                z = getIntent().getBooleanExtra("report", true);
            }
            FragmentTransaction beginTransaction = getSupportFragmentManager().beginTransaction();
            this.mFragment = CMPluginBridge.getBrowserFragment();
            Bundle bundle2 = new Bundle();
            bundle2.putString("url", this.url);
            if (!TextUtils.isEmpty(str)) {
                bundle2.putString("referer", str);
            }
            if (!TextUtils.isEmpty(str2)) {
                bundle2.putString("interceptUrl", str2);
            }
            if (!TextUtils.isEmpty(str3)) {
                bundle2.putString("onUrlInterceptCallBack", str3);
            }
            if (getIntent().hasExtra("promoter")) {
                bundle2.putParcelable("promoter", getIntent().getExtras().getParcelable("promoter"));
            }
            if (getIntent().hasExtra("transinfo")) {
                bundle2.putString("transinfo", getIntent().getExtras().getString("transinfo"));
            }
            bundle2.putBoolean("report", z);
            this.mFragment.setArguments(bundle2);
            if (this.mFragment != null) {
                beginTransaction.add(16908300, this.mFragment);
                beginTransaction.commit();
                return;
            }
            finish();
        } catch (Exception e) {
            Log.e("AlimamaWall", "", e);
        }
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        try {
            if (((Boolean) CMPluginBridge.BaseFragment_dispatchKeyEvent.invoke(this.mFragment, keyEvent)).booleanValue()) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return super.dispatchKeyEvent(keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        MmuSDKFactory.getMmuSDK().alimamaJsSdkOnResume();
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        MmuSDKFactory.getMmuSDK().alimamaJsSdkOnDestroy();
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        MmuSDKFactory.getMmuSDK().alimamaJsSdkOnPause();
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        if (!MmuSDKFactory.getMmuSDK().accountServiceHandleResult(i, i2, intent, this)) {
            MmuSDKFactory.getMmuSDK().alimamaJsSdkOnActivityResult(i, i2, intent);
            if (!CMPluginBridge.BrowserFragmentHandleResult(i, i2, intent, this.mFragment)) {
                super.onActivityResult(i, i2, intent);
            }
        }
    }
}
