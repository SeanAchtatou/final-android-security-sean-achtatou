package com.alimama.mobile.sdk.config;

import android.app.Activity;
import android.view.ViewGroup;
import com.alimama.mobile.sdk.MmuSDK;
import com.alimama.mobile.sdk.config.system.bridge.CMPluginBridge;
import com.alimama.mobile.sdk.config.system.bridge.LoopImagePluginBridge;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

public class LoopImageController<T> implements MmuController<T> {
    private LoopImagePluginBridge instance;
    private T mInstanceView;

    public interface IncubatedListener {
        public static final int STATUS_FAIL = 0;
        public static final int STATUS_SUCCESS = 1;
        public static final int STATUS_SUCCESS_CACHE = 2;

        void onComplete(int i, String str);
    }

    private LoopImageController() {
    }

    public static LoopImageController newInstance() {
        if (MmuSDK.PLUGIN_LOAD_STATUS.COMPLETED == MmuSDKFactory.getMmuSDK().getStatus()) {
            return new LoopImageController();
        }
        return null;
    }

    public T getInstanceView() {
        return this.mInstanceView;
    }

    public void setInstanceView(T t) {
        this.mInstanceView = t;
    }

    public MMLargeImage getProduct() {
        Object invoke_LargeImageManager_getProduct;
        if (this.instance == null || !this.instance.hasInitManager() || (invoke_LargeImageManager_getProduct = LoopImagePluginBridge.getInstance().invoke_LargeImageManager_getProduct()) == null) {
            return null;
        }
        return new MMLargeImage(invoke_LargeImageManager_getProduct);
    }

    public void setIncubatedListener(IncubatedListener incubatedListener) {
        if (this.instance != null && this.instance.hasInitManager()) {
            LoopImagePluginBridge.getInstance().invoke_LargeImageManager_setIncubatedListener(incubatedListener);
        }
    }

    public void init(String str) {
        this.instance = LoopImagePluginBridge.getInstance();
        if (this.instance != null) {
            if (!this.instance.hasInitManager()) {
                this.instance.initLargeImageManager();
            }
            this.instance.invoke_LargeImageManager_init(str);
        }
    }

    public void incubate() {
        if (this.instance != null && this.instance.hasInitManager()) {
            LoopImagePluginBridge.getInstance().invoke_LargeImageManager_incubate();
        }
    }

    public boolean hasProduct() {
        Object invoke_LargeImageManager_hasProduct;
        if (this.instance == null || !this.instance.hasInitManager() || (invoke_LargeImageManager_hasProduct = LoopImagePluginBridge.getInstance().invoke_LargeImageManager_hasProduct()) == null || !(invoke_LargeImageManager_hasProduct instanceof Boolean)) {
            return false;
        }
        return ((Boolean) invoke_LargeImageManager_hasProduct).booleanValue();
    }

    public void setTitle(ViewGroup viewGroup, String str) {
        LoopImagePluginBridge.getInstance().invoke_setTitle(viewGroup, str);
    }

    public void close() {
    }

    public static class MMLargeImage {
        protected Object largeImage;
        private Object mService;

        protected MMLargeImage(Object obj) {
            this.largeImage = obj;
        }

        public void reportImpression(Activity activity, MMPromoter... mMPromoterArr) {
            try {
                if (this.mService == null) {
                    this.mService = LoopImagePluginBridge.getInstance().invoke_LargeImage_getDataService(this.largeImage, CMPluginBridge.getAppContext());
                }
                ArrayList arrayList = null;
                if (mMPromoterArr != null) {
                    arrayList = new ArrayList();
                    for (MMPromoter mMPromoter : mMPromoterArr) {
                        arrayList.add(mMPromoter.promoter);
                    }
                }
                CMPluginBridge.ExchangeDataService_reportImpression.invoke(this.mService, arrayList);
            } catch (InvocationTargetException e) {
                throw new RuntimeException("Hack调用失败", e);
            }
        }

        public void clickOnPromoter(Activity activity, MMPromoter mMPromoter) {
            try {
                if (this.mService == null) {
                    this.mService = LoopImagePluginBridge.getInstance().invoke_LargeImage_getDataService(this.largeImage, CMPluginBridge.getAppContext());
                }
                CMPluginBridge.ExchangeDataService_clickOnPromoter.invoke(this.mService, activity, mMPromoter.promoter);
            } catch (InvocationTargetException e) {
                throw new RuntimeException("Hack调用失败", e);
            }
        }

        public boolean existOnlineFeed() {
            try {
                return ((Boolean) LoopImagePluginBridge.getInstance().invoke_LargeImage_existOnlinePromoter(this.largeImage)).booleanValue();
            } catch (Exception e) {
                throw new RuntimeException("Hack调用失败", e);
            }
        }

        public boolean existCacheFeed() {
            try {
                return ((Boolean) LoopImagePluginBridge.getInstance().invoke_LargeImage_existCachePromoter(this.largeImage)).booleanValue();
            } catch (Exception e) {
                throw new RuntimeException("Hack调用失败", e);
            }
        }

        public List<MMPromoter> getPromoters() {
            try {
                List<Object> list = (List) LoopImagePluginBridge.getInstance().invoke_LargeImage_getPromoters(this.largeImage);
                if (list == null) {
                    return null;
                }
                ArrayList arrayList = new ArrayList();
                for (Object mMPromoter : list) {
                    arrayList.add(new MMPromoter(mMPromoter));
                }
                return arrayList;
            } catch (Exception e) {
                throw new RuntimeException("Hack调用失败", e);
            }
        }

        public String toString() {
            Object[] objArr = new Object[3];
            objArr[0] = existOnlineFeed() ? "True" : "False";
            objArr[1] = existCacheFeed() ? "True" : "False";
            objArr[2] = getPromoters() == null ? "0" : Integer.valueOf(getPromoters().size());
            return String.format("MMFeed:\r{\r     Style[%s]\r     existOnlineFeed[%s]\r     existCacheFeed[%s]\r     Promoter[%s]\r     ExtraPromoter[%s]\r     isReady[%s]", objArr);
        }
    }
}
