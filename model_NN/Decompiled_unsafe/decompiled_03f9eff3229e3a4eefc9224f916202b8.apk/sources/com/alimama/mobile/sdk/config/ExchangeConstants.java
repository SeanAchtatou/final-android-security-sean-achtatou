package com.alimama.mobile.sdk.config;

import android.location.Location;
import android.support.v4.view.MotionEventCompat;

public class ExchangeConstants {
    public static boolean ALLOW_SHOW_HANDLERLIST_HEADER = true;
    public static String APPKEY = "";
    public static String[] BASE_URL_LIST = {"http://ex.puata.info", "http://ex.umengcloud.com", "http://ex.mobmore.com"};
    public static boolean BROWSER_GRADUAL_BACK = true;
    public static final String CACHE_PATH = "/download/.alimunion";
    public static String CHANNEL = null;
    public static boolean CONTAINER_AUTOEXPANDED = true;
    public static int CONTAINER_HEIGHT = 55;
    public static int CONTAINER_LIST_COUNT = 7;
    public static int CustomBrowserLayout = 0;
    public static boolean DEBUG_MODE = false;
    public static String DEFAULT_HANDLE_APP_WALL_TITLE = "精彩推荐";
    public static boolean DETAIL_PAGE = true;
    public static boolean IGNORE_SERVER_INTERVAL = false;
    public static String LOG_TAG = "com.taobao.munion";
    public static String MTOP_APPKEY = "";
    public static String MTOP_APP_SECRET = "";
    public static String MTOP_APP_SIGNATURE = "";
    public static boolean ONLY_CHINESE = false;
    public static int PRELOAD_REPEAT_COUNT = 1;
    public static int REFRESH_INTERVAL = 30000;
    public static boolean RICH_NOTIFICATION = true;
    public static boolean ROUND_ICON = true;
    public static String SDK_CHANNEL = "None";
    public static String WEB_CACHE_NAME = "/download/.web_cache/";
    public static boolean WELCOME_COUNTDOWN = true;
    public static int banner_alpha = MotionEventCompat.ACTION_MASK;
    public static boolean full_screen = false;
    public static boolean handler_auto_expand = true;
    public static boolean handler_left = true;
    private static Location manuallocation = null;
    public static String no = "否";
    public static String protocol_version = "6.1.20150717";
    public static String sdk_version = "8.3.2.20150717";
    public static boolean show_size = false;
    @Deprecated
    public static String text_color = "#000000";
    public static final int type_container = 8;
    public static final int type_feed_stream = 12;
    public static final int type_hypertextlink_banner = 13;
    public static final int type_large_image = 43;
    public static final int type_list_curtain = 7;
    public static final int type_standalone_handler = 6;
    public static final int type_suppose = 16;
    public static final int type_welcome_image = 9;

    public static int definePageLevel(int i) {
        switch (i) {
            case 7:
                return 2;
            case 8:
                return 1;
            default:
                return 3;
        }
    }

    public static Location getManuallocation() {
        return manuallocation;
    }

    public static void setManuallocation(Location location) {
        if (location.getTime() <= 0) {
            location.setTime(System.currentTimeMillis());
        }
        location.setProvider("manual");
        manuallocation = location;
    }
}
