package com.alimama.mobile.sdk.config.system.bridge;

import android.content.res.AssetManager;
import android.content.res.Resources;
import com.alimama.mobile.sdk.config.system.BridgeSystem;
import com.alimama.mobile.sdk.hack.Hack;

@BridgeSystem.GROUP(TYPE = BridgeSystem.GroupType.OS)
public class AndroidBridgeHacks implements BridgeSystem.HackCollection {
    public static Hack.HackedClass<Resources.Theme> THeme;
    public static Hack.HackedField<Resources.Theme, AssetManager> THeme_mAssets;

    public void allClasses() throws Hack.HackDeclaration.HackAssertionException {
        THeme = Hack.into(Resources.Theme.class);
    }

    public void allMethods() throws Hack.HackDeclaration.HackAssertionException {
    }

    public void allFields() throws Hack.HackDeclaration.HackAssertionException {
        THeme_mAssets = THeme.field("mAssets").ofType(AssetManager.class);
    }
}
