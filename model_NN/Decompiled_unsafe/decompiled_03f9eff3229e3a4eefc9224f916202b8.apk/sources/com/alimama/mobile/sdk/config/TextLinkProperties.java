package com.alimama.mobile.sdk.config;

import android.view.ViewGroup;

public class TextLinkProperties extends MmuProperties {
    public static final int TYPE = 13;
    private ViewGroup container;

    public TextLinkProperties(String str, ViewGroup viewGroup) {
        super(str, 13);
        this.container = viewGroup;
    }

    public ViewGroup getContainer() {
        return this.container;
    }

    public String[] getPluginNames() {
        return new String[]{"TextLinkPlugin"};
    }
}
