package com.alimama.mobile.sdk.config;

import java.util.HashMap;
import java.util.Map;

public class MmuProperties {
    private Map<String, Object> extra;
    private int layoutType;
    protected MmuController<?> mmuController;
    private String slot_id;
    public Object tag;

    public MmuProperties(String str, int i) {
        this.slot_id = str;
        this.layoutType = i;
    }

    public Object getTag() {
        return this.tag;
    }

    public void setTag(Object obj) {
        this.tag = obj;
    }

    public String getSlot_id() {
        return this.slot_id;
    }

    public int getLayoutType() {
        return this.layoutType;
    }

    public boolean containsExtraKey(String str) {
        if (this.extra == null || !this.extra.containsKey(str)) {
            return false;
        }
        return true;
    }

    public Object getExtra(String str) {
        if (this.extra != null) {
            return this.extra.get(str);
        }
        return null;
    }

    public void putExtra(String str, Object obj) {
        if (this.extra == null) {
            this.extra = new HashMap();
        }
        this.extra.put(str, obj);
    }

    public String[] getPluginNames() {
        return null;
    }
}
