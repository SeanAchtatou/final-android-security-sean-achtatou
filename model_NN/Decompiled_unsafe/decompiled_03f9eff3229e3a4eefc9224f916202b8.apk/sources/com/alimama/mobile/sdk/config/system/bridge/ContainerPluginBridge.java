package com.alimama.mobile.sdk.config.system.bridge;

import android.content.Context;
import android.util.Log;
import android.view.View;
import com.alimama.mobile.pluginframework.core.PluginFramework;
import com.alimama.mobile.sdk.config.system.BridgeSystem;
import com.alimama.mobile.sdk.hack.AssertionArrayException;
import com.alimama.mobile.sdk.hack.Hack;

public class ContainerPluginBridge implements BridgeSystem.HackCollection, Hack.AssertionFailureHandler {
    private static ContainerPluginBridge mBridge;
    private Hack.HackedClass<Object> MunionContainerView;
    private AssertionArrayException mExceptionArray = null;

    private ContainerPluginBridge() {
    }

    public View invoke_create_containerview(Context context) {
        try {
            return (View) this.MunionContainerView.constructor(Context.class).getInstance(context);
        } catch (Hack.HackDeclaration.HackAssertionException e) {
            throw new RuntimeException("Hack调用失败", e);
        }
    }

    public static ContainerPluginBridge getInstance() {
        boolean z;
        if (mBridge == null) {
            ContainerPluginBridge containerPluginBridge = new ContainerPluginBridge();
            try {
                Hack.setAssertionFailureHandler(containerPluginBridge);
                containerPluginBridge.allClasses();
                containerPluginBridge.allMethods();
                containerPluginBridge.allFields();
                if (containerPluginBridge.mExceptionArray != null) {
                    z = false;
                } else {
                    z = true;
                }
                Hack.setAssertionFailureHandler(null);
                if (z) {
                    mBridge = containerPluginBridge;
                } else if (mBridge != null) {
                    Log.e("Hack", "Create FeedPluginBridge failed.", mBridge.mExceptionArray);
                } else if (containerPluginBridge != null) {
                    Log.e("Hack", "Create FeedPluginBridge failed.", containerPluginBridge.mExceptionArray);
                }
            } catch (Hack.HackDeclaration.HackAssertionException e) {
                Log.e("Hack", "HackAssertionException", e);
                Hack.setAssertionFailureHandler(null);
                if (mBridge != null) {
                    Log.e("Hack", "Create FeedPluginBridge failed.", mBridge.mExceptionArray);
                } else if (containerPluginBridge != null) {
                    Log.e("Hack", "Create FeedPluginBridge failed.", containerPluginBridge.mExceptionArray);
                }
            } catch (Throwable th) {
                Hack.setAssertionFailureHandler(null);
                if (mBridge != null) {
                    Log.e("Hack", "Create FeedPluginBridge failed.", mBridge.mExceptionArray);
                } else if (containerPluginBridge != null) {
                    Log.e("Hack", "Create FeedPluginBridge failed.", containerPluginBridge.mExceptionArray);
                }
                throw th;
            }
        }
        return mBridge;
    }

    private static ClassLoader getClassLoader() {
        try {
            return PluginFramework.getPluginClassLoader();
        } catch (Exception e) {
            Log.e("wt", "", e);
            return null;
        }
    }

    public void allClasses() throws Hack.HackDeclaration.HackAssertionException {
        this.MunionContainerView = Hack.into(getClassLoader(), "com.taobao.newxp.view.container.MunionContainerView");
    }

    public void allMethods() throws Hack.HackDeclaration.HackAssertionException {
    }

    public void allFields() throws Hack.HackDeclaration.HackAssertionException {
    }

    public boolean onAssertionFailure(Hack.HackDeclaration.HackAssertionException hackAssertionException) {
        if (this.mExceptionArray == null) {
            this.mExceptionArray = new AssertionArrayException("FeedPluginBridge hack failed");
        }
        this.mExceptionArray.addException(hackAssertionException);
        return true;
    }
}
