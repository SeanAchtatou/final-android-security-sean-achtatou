package com.alimama.mobile.sdk.lab;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.app.Fragment;
import android.app.Instrumentation;
import android.app.UiAutomation;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.IBinder;
import android.view.KeyEvent;
import android.view.MotionEvent;
import com.alimama.mobile.sdk.config.system.HookManager;
import com.alimama.mobile.sdk.config.system.MMLog;
import com.alimama.mobile.sdk.config.system.bridge.FrameworkBridge;
import com.alimama.mobile.sdk.config.system.bridge.GodModeHacks;
import java.lang.reflect.InvocationTargetException;

public class InstrumentationHook extends Instrumentation {
    private Context context;
    /* access modifiers changed from: private */
    public Instrumentation mBase;

    private interface ExecStartActivityCallback {
        Instrumentation.ActivityResult execStartActivity();
    }

    public InstrumentationHook(Instrumentation instrumentation, Context context2) {
        this.context = context2;
        this.mBase = instrumentation;
    }

    public Instrumentation.ActivityResult execStartActivity(Context context2, IBinder iBinder, IBinder iBinder2, Activity activity, Intent intent, int i) {
        final Context context3 = context2;
        final IBinder iBinder3 = iBinder;
        final IBinder iBinder4 = iBinder2;
        final Activity activity2 = activity;
        final Intent intent2 = intent;
        final int i2 = i;
        return execStartActivityInternal(this.context, intent, new ExecStartActivityCallback() {
            public Instrumentation.ActivityResult execStartActivity() {
                return InstrumentationHook.this.mBase.execStartActivity(context3, iBinder3, iBinder4, activity2, intent2, i2);
            }
        });
    }

    @TargetApi(16)
    public Instrumentation.ActivityResult execStartActivity(Context context2, IBinder iBinder, IBinder iBinder2, Activity activity, Intent intent, int i, Bundle bundle) {
        final Context context3 = context2;
        final IBinder iBinder3 = iBinder;
        final IBinder iBinder4 = iBinder2;
        final Activity activity2 = activity;
        final Intent intent2 = intent;
        final int i2 = i;
        final Bundle bundle2 = bundle;
        return execStartActivityInternal(this.context, intent, new ExecStartActivityCallback() {
            public Instrumentation.ActivityResult execStartActivity() {
                return InstrumentationHook.this.mBase.execStartActivity(context3, iBinder3, iBinder4, activity2, intent2, i2, bundle2);
            }
        });
    }

    @TargetApi(14)
    public Instrumentation.ActivityResult execStartActivity(Context context2, IBinder iBinder, IBinder iBinder2, Fragment fragment, Intent intent, int i) {
        final Context context3 = context2;
        final IBinder iBinder3 = iBinder;
        final IBinder iBinder4 = iBinder2;
        final Fragment fragment2 = fragment;
        final Intent intent2 = intent;
        final int i2 = i;
        return execStartActivityInternal(this.context, intent, new ExecStartActivityCallback() {
            public Instrumentation.ActivityResult execStartActivity() {
                return InstrumentationHook.this.mBase.execStartActivity(context3, iBinder3, iBinder4, fragment2, intent2, i2);
            }
        });
    }

    @TargetApi(16)
    public Instrumentation.ActivityResult execStartActivity(Context context2, IBinder iBinder, IBinder iBinder2, Fragment fragment, Intent intent, int i, Bundle bundle) {
        final Context context3 = context2;
        final IBinder iBinder3 = iBinder;
        final IBinder iBinder4 = iBinder2;
        final Fragment fragment2 = fragment;
        final Intent intent2 = intent;
        final int i2 = i;
        final Bundle bundle2 = bundle;
        return execStartActivityInternal(this.context, intent, new ExecStartActivityCallback() {
            public Instrumentation.ActivityResult execStartActivity() {
                return InstrumentationHook.this.mBase.execStartActivity(context3, iBinder3, iBinder4, fragment2, intent2, i2, bundle2);
            }
        });
    }

    private Instrumentation.ActivityResult execStartActivityInternal(Context context2, Intent intent, ExecStartActivityCallback execStartActivityCallback) {
        return execStartActivityCallback.execStartActivity();
    }

    public Activity newActivity(Class<?> cls, Context context2, IBinder iBinder, Application application, Intent intent, ActivityInfo activityInfo, CharSequence charSequence, Activity activity, String str, Object obj) throws InstantiationException, IllegalAccessException {
        Activity newActivity = this.mBase.newActivity(cls, context2, iBinder, application, intent, activityInfo, charSequence, activity, str, obj);
        hookInjectResource(cls.getCanonicalName(), newActivity);
        return newActivity;
    }

    public Activity newActivity(ClassLoader classLoader, String str, Intent intent) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
        Activity newActivity = this.mBase.newActivity(classLoader, str, intent);
        hookInjectResource(str, newActivity);
        return newActivity;
    }

    private void hookInjectResource(String str, Activity activity) {
        if (HookManager.containsRegActivity(str)) {
            try {
                Resources resources = (Resources) FrameworkBridge.FrameworkLoader_getPluginResources.invoke(null, new Object[0]);
                if (GodModeHacks.ContextThemeWrapper_mResources != null) {
                    GodModeHacks.ContextThemeWrapper_mResources.on(activity).set(resources);
                }
                HookManager.addHookActivity(activity);
                MMLog.i("Inject successfully.[Activity=%s]", str);
            } catch (InvocationTargetException e) {
                MMLog.e(e, "Inject ContextThemeWapper error.", new Object[0]);
            }
        } else {
            MMLog.i("The Actvity[%s] is no in HookMangager", str);
        }
    }

    public void callActivityOnCreate(Activity activity, Bundle bundle) {
        this.mBase.callActivityOnCreate(activity, bundle);
    }

    public void callActivityOnResume(Activity activity) {
        this.mBase.callActivityOnResume(activity);
    }

    @TargetApi(18)
    public UiAutomation getUiAutomation() {
        return this.mBase.getUiAutomation();
    }

    public void onCreate(Bundle bundle) {
        this.mBase.onCreate(bundle);
    }

    public void start() {
        this.mBase.start();
    }

    public void onStart() {
        this.mBase.onStart();
    }

    public boolean onException(Object obj, Throwable th) {
        return this.mBase.onException(obj, th);
    }

    public void sendStatus(int i, Bundle bundle) {
        this.mBase.sendStatus(i, bundle);
    }

    public void finish(int i, Bundle bundle) {
        this.mBase.finish(i, bundle);
    }

    public void setAutomaticPerformanceSnapshots() {
        this.mBase.setAutomaticPerformanceSnapshots();
    }

    public void startPerformanceSnapshot() {
        this.mBase.startPerformanceSnapshot();
    }

    public void endPerformanceSnapshot() {
        this.mBase.endPerformanceSnapshot();
    }

    public void onDestroy() {
        this.mBase.onDestroy();
    }

    public Context getContext() {
        return this.mBase.getContext();
    }

    public ComponentName getComponentName() {
        return this.mBase.getComponentName();
    }

    public Context getTargetContext() {
        return this.mBase.getTargetContext();
    }

    public boolean isProfiling() {
        return this.mBase.isProfiling();
    }

    public void startProfiling() {
        this.mBase.startProfiling();
    }

    public void stopProfiling() {
        this.mBase.stopProfiling();
    }

    public void setInTouchMode(boolean z) {
        this.mBase.setInTouchMode(z);
    }

    public void waitForIdle(Runnable runnable) {
        this.mBase.waitForIdle(runnable);
    }

    public void waitForIdleSync() {
        this.mBase.waitForIdleSync();
    }

    public void runOnMainSync(Runnable runnable) {
        this.mBase.runOnMainSync(runnable);
    }

    public Activity startActivitySync(Intent intent) {
        return this.mBase.startActivitySync(intent);
    }

    public void addMonitor(Instrumentation.ActivityMonitor activityMonitor) {
        this.mBase.addMonitor(activityMonitor);
    }

    public Instrumentation.ActivityMonitor addMonitor(IntentFilter intentFilter, Instrumentation.ActivityResult activityResult, boolean z) {
        return this.mBase.addMonitor(intentFilter, activityResult, z);
    }

    public Instrumentation.ActivityMonitor addMonitor(String str, Instrumentation.ActivityResult activityResult, boolean z) {
        return this.mBase.addMonitor(str, activityResult, z);
    }

    public boolean checkMonitorHit(Instrumentation.ActivityMonitor activityMonitor, int i) {
        return this.mBase.checkMonitorHit(activityMonitor, i);
    }

    public Activity waitForMonitor(Instrumentation.ActivityMonitor activityMonitor) {
        return this.mBase.waitForMonitor(activityMonitor);
    }

    public Activity waitForMonitorWithTimeout(Instrumentation.ActivityMonitor activityMonitor, long j) {
        return this.mBase.waitForMonitorWithTimeout(activityMonitor, j);
    }

    public void removeMonitor(Instrumentation.ActivityMonitor activityMonitor) {
        this.mBase.removeMonitor(activityMonitor);
    }

    public boolean invokeMenuActionSync(Activity activity, int i, int i2) {
        return this.mBase.invokeMenuActionSync(activity, i, i2);
    }

    public boolean invokeContextMenuAction(Activity activity, int i, int i2) {
        return this.mBase.invokeContextMenuAction(activity, i, i2);
    }

    public void sendStringSync(String str) {
        this.mBase.sendStringSync(str);
    }

    public void sendKeySync(KeyEvent keyEvent) {
        this.mBase.sendKeySync(keyEvent);
    }

    public void sendKeyDownUpSync(int i) {
        this.mBase.sendKeyDownUpSync(i);
    }

    public void sendCharacterSync(int i) {
        this.mBase.sendCharacterSync(i);
    }

    public void sendPointerSync(MotionEvent motionEvent) {
        this.mBase.sendPointerSync(motionEvent);
    }

    public void sendTrackballEventSync(MotionEvent motionEvent) {
        this.mBase.sendTrackballEventSync(motionEvent);
    }

    public Application newApplication(ClassLoader classLoader, String str, Context context2) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
        return this.mBase.newApplication(classLoader, str, context2);
    }

    public void callApplicationOnCreate(Application application) {
        this.mBase.callApplicationOnCreate(application);
    }

    public void callActivityOnDestroy(Activity activity) {
        this.mBase.callActivityOnDestroy(activity);
    }

    public void callActivityOnRestoreInstanceState(Activity activity, Bundle bundle) {
        this.mBase.callActivityOnRestoreInstanceState(activity, bundle);
    }

    public void callActivityOnPostCreate(Activity activity, Bundle bundle) {
        this.mBase.callActivityOnPostCreate(activity, bundle);
    }

    public void callActivityOnNewIntent(Activity activity, Intent intent) {
        this.mBase.callActivityOnNewIntent(activity, intent);
    }

    public void callActivityOnStart(Activity activity) {
        this.mBase.callActivityOnStart(activity);
    }

    public void callActivityOnRestart(Activity activity) {
        this.mBase.callActivityOnRestart(activity);
    }

    public void callActivityOnStop(Activity activity) {
        this.mBase.callActivityOnStop(activity);
    }

    public void callActivityOnSaveInstanceState(Activity activity, Bundle bundle) {
        this.mBase.callActivityOnSaveInstanceState(activity, bundle);
    }

    public void callActivityOnPause(Activity activity) {
        this.mBase.callActivityOnPause(activity);
    }

    public void callActivityOnUserLeaving(Activity activity) {
        this.mBase.callActivityOnUserLeaving(activity);
    }

    public void startAllocCounting() {
        this.mBase.startAllocCounting();
    }

    public void stopAllocCounting() {
        this.mBase.stopAllocCounting();
    }

    public Bundle getAllocCounts() {
        return this.mBase.getAllocCounts();
    }

    public Bundle getBinderCounts() {
        return this.mBase.getBinderCounts();
    }
}
