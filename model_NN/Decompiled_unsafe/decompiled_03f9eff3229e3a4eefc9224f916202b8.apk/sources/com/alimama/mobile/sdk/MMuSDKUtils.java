package com.alimama.mobile.sdk;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import com.alimama.mobile.pluginframework.core.PluginFramework;
import com.alimama.mobile.sdk.config.ExchangeConstants;
import com.baidu.mobads.interfaces.utils.IXAdSystemUtils;
import java.util.Map;

public class MMuSDKUtils {
    private static final String OS = "android";

    public static Map<String, Object> addExtraParams(Map<String, Object> map, Context context) {
        if (!map.containsKey(PluginFramework.KEY_UPDATE_SLOTID) || !map.containsKey(PluginFramework.KEY_UPDATE_LAYOUTTYPE)) {
            Log.e("Update", "无法添加插件更新参数，slotid或layout_type无法获得.");
        } else {
            map.put("os", OS);
            map.put("osv", Build.VERSION.RELEASE);
            map.put(PluginFramework.KEY_UPDATE_SDKV, ExchangeConstants.sdk_version);
            map.put(PluginFramework.KEY_UPDATE_ACCESS, getAccess(context));
            map.put(PluginFramework.KEY_UPDATE_DEVICEID, getDeviceId(context));
        }
        return map;
    }

    public static String getDeviceId(Context context) {
        TelephonyManager telephonyManager;
        String str;
        if (context == null || (telephonyManager = (TelephonyManager) context.getSystemService("phone")) == null) {
            return "";
        }
        try {
            str = checkPermission(context, "android.permission.READ_PHONE_STATE") ? telephonyManager.getDeviceId() : "";
        } catch (Exception e) {
            str = "";
        }
        if (!TextUtils.isEmpty(str)) {
            return str;
        }
        String mac = getMac(context);
        if (TextUtils.isEmpty(mac)) {
            return Settings.Secure.getString(context.getContentResolver(), "android_id");
        }
        return mac;
    }

    public static String getMac(Context context) {
        String str;
        if (context == null) {
            return null;
        }
        try {
            WifiManager wifiManager = (WifiManager) context.getSystemService(IXAdSystemUtils.NT_WIFI);
            if (checkPermission(context, "android.permission.ACCESS_WIFI_STATE")) {
                str = wifiManager.getConnectionInfo().getMacAddress();
            } else {
                str = null;
            }
            return str;
        } catch (Exception e) {
            return null;
        }
    }

    public static String getAccess(Context context) {
        if (context != null) {
            try {
                if (!checkPermission(context, "android.permission.ACCESS_NETWORK_STATE")) {
                    return "unknown";
                }
                ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
                if (connectivityManager == null) {
                    return "unknown";
                }
                if (connectivityManager.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED) {
                    return IXAdSystemUtils.NT_WIFI;
                }
                if (connectivityManager.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTED) {
                    return "2G/3G";
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return "unknown";
    }

    public static boolean checkPermission(Context context, String str) {
        return context.getPackageManager().checkPermission(str, context.getPackageName()) == 0;
    }
}
