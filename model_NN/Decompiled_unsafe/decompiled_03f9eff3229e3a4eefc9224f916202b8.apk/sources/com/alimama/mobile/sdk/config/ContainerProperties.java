package com.alimama.mobile.sdk.config;

import android.app.Activity;
import android.view.ViewGroup;

public class ContainerProperties extends MmuProperties {
    public static final int TYPE = 8;
    public final Activity activity;
    private final ViewGroup container;
    private int listitem = 0;

    public ContainerProperties(Activity activity2, String str, ViewGroup viewGroup) {
        super(str, 8);
        this.activity = activity2;
        this.container = viewGroup;
    }

    public ContainerProperties setListItemResource(int i) {
        this.listitem = i;
        return this;
    }

    public int getListitemResource() {
        return this.listitem;
    }

    public ViewGroup getContainer() {
        return this.container;
    }

    public String[] getPluginNames() {
        return new String[]{"ContainerPlugin"};
    }
}
