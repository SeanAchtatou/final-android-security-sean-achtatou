package com.alimama.mobile.sdk.config.system.bridge;

import android.support.v4.app.Fragment;
import android.util.Log;
import com.alimama.mobile.pluginframework.core.PluginFramework;
import com.alimama.mobile.sdk.config.system.BridgeSystem;
import com.alimama.mobile.sdk.config.system.MMLog;
import com.alimama.mobile.sdk.hack.AssertionArrayException;
import com.alimama.mobile.sdk.hack.Hack;

public class TaePluginBridge implements BridgeSystem.HackCollection, Hack.AssertionFailureHandler {
    private static TaePluginBridge mBridge;
    private Hack.HackedClass<Fragment> LoginWebViewFragment;
    private AssertionArrayException mExceptionArray = null;

    private TaePluginBridge() {
    }

    public static TaePluginBridge getInstance() {
        boolean z;
        if (mBridge == null) {
            TaePluginBridge taePluginBridge = new TaePluginBridge();
            try {
                Hack.setAssertionFailureHandler(taePluginBridge);
                taePluginBridge.allClasses();
                taePluginBridge.allMethods();
                taePluginBridge.allFields();
                if (taePluginBridge.mExceptionArray != null) {
                    z = false;
                } else {
                    z = true;
                }
                Hack.setAssertionFailureHandler(null);
                if (z) {
                    mBridge = taePluginBridge;
                } else if (mBridge != null) {
                    Log.e("Hack", "Create FeedPluginBridge failed.", mBridge.mExceptionArray);
                } else if (taePluginBridge != null) {
                    Log.e("Hack", "Create FeedPluginBridge failed.", taePluginBridge.mExceptionArray);
                }
            } catch (Hack.HackDeclaration.HackAssertionException e) {
                Log.e("Hack", "HackAssertionException", e);
                Hack.setAssertionFailureHandler(null);
                if (mBridge != null) {
                    Log.e("Hack", "Create FeedPluginBridge failed.", mBridge.mExceptionArray);
                } else if (taePluginBridge != null) {
                    Log.e("Hack", "Create FeedPluginBridge failed.", taePluginBridge.mExceptionArray);
                }
            } catch (Throwable th) {
                Hack.setAssertionFailureHandler(null);
                if (mBridge != null) {
                    Log.e("Hack", "Create FeedPluginBridge failed.", mBridge.mExceptionArray);
                } else if (taePluginBridge != null) {
                    Log.e("Hack", "Create FeedPluginBridge failed.", taePluginBridge.mExceptionArray);
                }
                throw th;
            }
        }
        return mBridge;
    }

    public Fragment getLoginWebViewFragment() {
        try {
            return (Fragment) this.LoginWebViewFragment.constructor(new Class[0]).getInstance(new Object[0]);
        } catch (Hack.HackDeclaration.HackAssertionException e) {
            MMLog.e(e, "", new Object[0]);
            return null;
        }
    }

    private static ClassLoader getClassLoader() {
        try {
            return PluginFramework.getPluginClassLoader();
        } catch (Exception e) {
            Log.e("wt", "", e);
            return null;
        }
    }

    public void allClasses() throws Hack.HackDeclaration.HackAssertionException {
        this.LoginWebViewFragment = Hack.into(getClassLoader(), "com.alibaba.sdk.android.login.ui.LoginWebViewFragment");
    }

    public void allMethods() throws Hack.HackDeclaration.HackAssertionException {
    }

    public void allFields() throws Hack.HackDeclaration.HackAssertionException {
    }

    public boolean onAssertionFailure(Hack.HackDeclaration.HackAssertionException hackAssertionException) {
        if (this.mExceptionArray == null) {
            this.mExceptionArray = new AssertionArrayException("FeedPluginBridge hack failed");
        }
        this.mExceptionArray.addException(hackAssertionException);
        return true;
    }
}
