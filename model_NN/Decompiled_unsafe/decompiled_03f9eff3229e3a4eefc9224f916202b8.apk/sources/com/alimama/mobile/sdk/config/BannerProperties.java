package com.alimama.mobile.sdk.config;

import android.view.ViewGroup;
import com.alimama.mobile.sdk.config.BannerController;
import com.alimama.mobile.sdk.config.MmuController;

public class BannerProperties extends MmuProperties {
    public static final int TYPE = 6;
    private ViewGroup container;
    private final BannerController mController = new BannerController();
    private boolean stretch = false;

    public BannerProperties(String str, ViewGroup viewGroup) {
        super(str, 6);
        this.container = viewGroup;
    }

    public ViewGroup getContainer() {
        return this.container;
    }

    public BannerController getMmuController() {
        return this.mController;
    }

    public void setClickCallBackListener(MmuController.ClickCallBackListener clickCallBackListener) {
        this.mController.setClickCallBackListener(clickCallBackListener);
    }

    public void setOnStateChangeCallBackListener(BannerController.OnStateChangeCallBackListener onStateChangeCallBackListener) {
        this.mController.setOnStateChangeCallBackListener(onStateChangeCallBackListener);
    }

    public String[] getPluginNames() {
        return new String[]{"BannerPlugin"};
    }

    public boolean isStretch() {
        return this.stretch;
    }

    public void setStretch(boolean z) {
        this.stretch = z;
    }
}
