package com.alimama.mobile.sdk.config.system.bridge;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.View;
import com.alimama.mobile.pluginframework.core.PluginFramework;
import com.alimama.mobile.sdk.config.FeedController;
import com.alimama.mobile.sdk.config.FeedProperties;
import com.alimama.mobile.sdk.config.system.BridgeSystem;
import com.alimama.mobile.sdk.hack.AssertionArrayException;
import com.alimama.mobile.sdk.hack.Hack;
import java.lang.reflect.InvocationTargetException;

public class FeedPluginBridge implements BridgeSystem.HackCollection, Hack.AssertionFailureHandler {
    private static Object feedMgr = null;
    private static FeedPluginBridge mBridge;
    public Hack.HackedClass<Object> Feed;
    private Hack.HackedClass<Object> FeedViewFactory;
    public Hack.HackedMethod FeedViewFactory_getFeedView;
    public Hack.HackedMethod Feed_cleanReportFlag;
    public Hack.HackedMethod Feed_existCacheFeed;
    public Hack.HackedMethod Feed_existOnlineFeed;
    public Hack.HackedMethod Feed_getDataService;
    public Hack.HackedMethod Feed_getExtraPromoters;
    public Hack.HackedMethod Feed_getPromoters;
    public Hack.HackedMethod Feed_getSlotId;
    public Hack.HackedMethod Feed_getStyle;
    public Hack.HackedMethod Feed_getTag;
    public Hack.HackedMethod Feed_isReady;
    public Hack.HackedField<Object, Object> Feed_obj;
    public Hack.HackedMethod Feed_reflushData;
    public Hack.HackedMethod Feed_setLazyDataCallback;
    public Hack.HackedMethod Feed_setTag;
    private Hack.HackedClass<Object> FeedsManager;
    private Hack.HackedMethod FeedsManager_addMaterial;
    private Hack.HackedMethod FeedsManager_getProduct;
    private Hack.HackedMethod FeedsManager_setIncubatedListener;
    private Hack.HackedConstructor FeedsManger_obj;
    public Hack.HackedClass<FeedController.MMFeed> MMFeed;
    public Hack.HackedField<FeedController.MMFeed, Object> MMFeed_feed;
    private AssertionArrayException mExceptionArray = null;

    private FeedPluginBridge() {
    }

    public static FeedPluginBridge getInstance() {
        boolean z;
        if (mBridge == null) {
            FeedPluginBridge feedPluginBridge = new FeedPluginBridge();
            try {
                Hack.setAssertionFailureHandler(feedPluginBridge);
                feedPluginBridge.allClasses();
                feedPluginBridge.allMethods();
                feedPluginBridge.allFields();
                if (feedPluginBridge.mExceptionArray != null) {
                    z = false;
                } else {
                    z = true;
                }
                Hack.setAssertionFailureHandler(null);
                if (z) {
                    mBridge = feedPluginBridge;
                    feedMgr = mBridge.FeedsManger_obj.getInstance(CMPluginBridge.getAppContext());
                } else if (mBridge != null) {
                    Log.e("Hack", "Create FeedPluginBridge failed.", mBridge.mExceptionArray);
                } else if (feedPluginBridge != null) {
                    Log.e("Hack", "Create FeedPluginBridge failed.", feedPluginBridge.mExceptionArray);
                }
            } catch (Hack.HackDeclaration.HackAssertionException e) {
                Log.e("Hack", "HackAssertionException", e);
                Hack.setAssertionFailureHandler(null);
                if (mBridge != null) {
                    Log.e("Hack", "Create FeedPluginBridge failed.", mBridge.mExceptionArray);
                } else if (feedPluginBridge != null) {
                    Log.e("Hack", "Create FeedPluginBridge failed.", feedPluginBridge.mExceptionArray);
                }
            } catch (Throwable th) {
                Hack.setAssertionFailureHandler(null);
                if (mBridge != null) {
                    Log.e("Hack", "Create FeedPluginBridge failed.", mBridge.mExceptionArray);
                } else if (feedPluginBridge != null) {
                    Log.e("Hack", "Create FeedPluginBridge failed.", feedPluginBridge.mExceptionArray);
                }
                throw th;
            }
        }
        return mBridge;
    }

    private static ClassLoader getClassLoader() {
        try {
            return PluginFramework.getPluginClassLoader();
        } catch (Exception e) {
            Log.e("wt", "", e);
            return null;
        }
    }

    public void invoke_addMaterial(FeedProperties feedProperties) {
        try {
            this.FeedsManager_addMaterial.invoke(feedMgr, feedProperties);
        } catch (InvocationTargetException e) {
            throw new RuntimeException("Hack调用失败", e);
        }
    }

    public View invoke_getFeedView(Activity activity, FeedController.MMFeed mMFeed) {
        try {
            return (View) this.FeedViewFactory_getFeedView.invoke(null, activity, mMFeed);
        } catch (InvocationTargetException e) {
            throw new RuntimeException("Hack调用失败", e);
        }
    }

    public void invoke_setIncubatedListener(FeedController.IncubatedListener incubatedListener) {
        try {
            this.FeedsManager_setIncubatedListener.invoke(feedMgr, incubatedListener);
        } catch (InvocationTargetException e) {
            throw new RuntimeException("Hack调用失败", e);
        }
    }

    public void invoke_setLazyDataCallback(FeedController.MMFeed mMFeed, FeedController.LazyDataCallback lazyDataCallback) {
        try {
            Object obj = this.MMFeed_feed.on(mMFeed).get();
            this.Feed_setLazyDataCallback.invoke(obj, lazyDataCallback);
        } catch (InvocationTargetException e) {
            throw new RuntimeException("Hack调用失败", e);
        }
    }

    public Object invoke_getProduct(String str) {
        try {
            return this.FeedsManager_getProduct.invoke(feedMgr, str);
        } catch (InvocationTargetException e) {
            throw new RuntimeException("Hack调用失败", e);
        }
    }

    public void allClasses() throws Hack.HackDeclaration.HackAssertionException {
        this.FeedsManager = Hack.into(getClassLoader(), "com.taobao.newxp.view.feed.FeedsManager");
        this.Feed = Hack.into(getClassLoader(), "com.taobao.newxp.view.feed.Feed");
        this.FeedViewFactory = Hack.into(getClassLoader(), "com.taobao.newxp.view.feed.FeedViewFactory");
        this.MMFeed = Hack.into(FeedController.MMFeed.class);
    }

    public void allMethods() throws Hack.HackDeclaration.HackAssertionException {
        this.FeedsManager_addMaterial = this.FeedsManager.method("addMaterial", FeedProperties.class);
        this.FeedsManager_getProduct = this.FeedsManager.method("getProduct", String.class);
        this.FeedsManager_setIncubatedListener = this.FeedsManager.method("setIncubatedListener", FeedController.IncubatedListener.class);
        this.FeedsManger_obj = this.FeedsManager.constructor(Context.class);
        this.Feed_cleanReportFlag = this.Feed.method("cleanReportFlag", new Class[0]);
        this.Feed_getStyle = this.Feed.method("getStyle", new Class[0]);
        this.Feed_existOnlineFeed = this.Feed.method("existOnlineFeed", new Class[0]);
        this.Feed_existCacheFeed = this.Feed.method("existCacheFeed", new Class[0]);
        this.Feed_getPromoters = this.Feed.method("getPromoters", new Class[0]);
        this.Feed_getExtraPromoters = this.Feed.method("getExtraPromoters", new Class[0]);
        this.Feed_isReady = this.Feed.method("isReady", new Class[0]);
        this.Feed_setLazyDataCallback = this.Feed.method("setLazyDataCallback", FeedController.LazyDataCallback.class);
        this.Feed_getDataService = this.Feed.method("getDataService", Context.class);
        this.FeedViewFactory_getFeedView = this.FeedViewFactory.method("getFeedView", Activity.class, FeedController.MMFeed.class);
        this.Feed_reflushData = this.Feed.method("reflushData", new Class[0]);
        this.Feed_setTag = this.Feed.method("setTag", Object.class);
        this.Feed_getTag = this.Feed.method("getTag", new Class[0]);
        this.Feed_getSlotId = this.Feed.method("getSlotId", new Class[0]);
    }

    public void allFields() throws Hack.HackDeclaration.HackAssertionException {
        this.MMFeed_feed = this.MMFeed.field("feed");
        this.Feed_obj = this.Feed.field("obj");
    }

    public boolean onAssertionFailure(Hack.HackDeclaration.HackAssertionException hackAssertionException) {
        if (this.mExceptionArray == null) {
            this.mExceptionArray = new AssertionArrayException("FeedPluginBridge hack failed");
        }
        this.mExceptionArray.addException(hackAssertionException);
        return true;
    }
}
