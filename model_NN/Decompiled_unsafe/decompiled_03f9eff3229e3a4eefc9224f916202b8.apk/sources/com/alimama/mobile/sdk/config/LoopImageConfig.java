package com.alimama.mobile.sdk.config;

import android.view.View;
import android.view.ViewGroup;

public class LoopImageConfig {
    private LargeGalleryBindListener bindListener;
    private onPageChangedListener mPageChangedListener;
    private View parent;

    public interface BindDrawableListener {
        void onEnd(STATUS status);

        void onStart(BindMode bindMode);
    }

    public enum BindMode {
        BIND_FORM_CACHE,
        BIND_FROM_NET
    }

    public interface LargeGalleryBindListener {
        void onEnd(STATUS status, ViewGroup viewGroup);

        void onStart(BindMode bindMode, ViewGroup viewGroup);
    }

    public enum STATUS {
        SUCCESS,
        FAIL
    }

    public interface onPageChangedListener {
        void onPageChanged(int i, MMPromoter mMPromoter, View view);
    }

    public LoopImageConfig setBindListener(LargeGalleryBindListener largeGalleryBindListener) {
        this.bindListener = largeGalleryBindListener;
        return this;
    }

    public LargeGalleryBindListener getBindListener() {
        return this.bindListener;
    }

    public onPageChangedListener getPageChangedListener() {
        return this.mPageChangedListener;
    }

    public void setPageChangedListener(onPageChangedListener onpagechangedlistener) {
        this.mPageChangedListener = onpagechangedlistener;
    }

    public View getParent() {
        return this.parent;
    }

    public void setParent(View view) {
        this.parent = view;
    }
}
