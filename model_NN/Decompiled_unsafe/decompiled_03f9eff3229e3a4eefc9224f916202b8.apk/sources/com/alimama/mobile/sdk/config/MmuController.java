package com.alimama.mobile.sdk.config;

public interface MmuController<T> {

    public interface ClickCallBackListener {
        void onClick();
    }

    public interface InitAsyncComplete {
        void onComplete(boolean z);
    }

    void close();

    T getInstanceView();

    void setInstanceView(T t);
}
