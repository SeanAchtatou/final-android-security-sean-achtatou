package com.alimama.mobile.sdk.config.system;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.ViewGroup;
import com.alimama.mobile.pluginframework.core.PluginFramework;
import com.alimama.mobile.pluginframework.core.PluginServiceAgent;
import com.alimama.mobile.sdk.config.FeedProperties;
import com.alimama.mobile.sdk.config.system.bridge.CMPluginBridge;
import com.alimama.mobile.sdk.config.system.bridge.FeedPluginBridge;
import com.alimama.mobile.sdk.config.system.bridge.FrameworkBridge;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class APFSystem {
    public Set<String> init(Context context) {
        try {
            return PluginFramework.init(context.getApplicationInfo(), context.getAssets(), context.getFilesDir().getAbsolutePath(), context.getResources(), "mu");
        } catch (Exception e) {
            Log.e("E", "", e);
            return null;
        }
    }

    public Fragment findFragment(Map<String, Object> map) throws Exception {
        return (Fragment) CMPluginBridge.Router_getFragment.invoke(null, map);
    }

    public PluginServiceAgent findProvider() throws Exception {
        HashMap hashMap = new HashMap();
        hashMap.put("Download", "com.taobao.munion.base.download.DownloadProvider");
        return new PluginServiceAgent(new PluginServiceAgent.ProviderShell(CMPluginBridge.Router_getService.invoke(null, hashMap)));
    }

    public void inset(ViewGroup viewGroup, Map<String, Object> map) throws Exception {
        if (map == null || !map.containsKey(PluginFramework.KEY_UPDATE_SLOTID) || !map.containsKey(PluginFramework.KEY_UPDATE_LAYOUTTYPE)) {
            throw new RuntimeException("AFPSystem无法获取slot_id.");
        }
        CMPluginBridge.Router_insetView.invoke(null, viewGroup, map);
    }

    public void normalView(Context context, Map<String, Object> map) throws Exception {
        if (map == null || !map.containsKey(PluginFramework.KEY_UPDATE_SLOTID) || !map.containsKey(PluginFramework.KEY_UPDATE_LAYOUTTYPE)) {
            throw new RuntimeException("AFPSystem无法获取slot_id.");
        }
        CMPluginBridge.Router_normalView.invoke(null, context, map);
    }

    public void updateSDK(Map<String, Object> map) {
        try {
            FrameworkBridge.FrameworkLoader_update.invoke(null, map);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("E", "com.alimama.mobile.plugin.framework.FrameworkLoader not found");
        }
    }

    public void addFeedMaterial(FeedProperties feedProperties) {
        FeedPluginBridge.getInstance().invoke_addMaterial(feedProperties);
    }

    public static String getVersion() {
        try {
            Object invoke = FrameworkBridge.FrameworkLoader_getVersion.invoke(null, null);
            if (invoke != null) {
                return invoke.toString();
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("E", "com.alimama.mobile.plugin.framework.FrameworkLoader not found");
            return null;
        }
    }
}
