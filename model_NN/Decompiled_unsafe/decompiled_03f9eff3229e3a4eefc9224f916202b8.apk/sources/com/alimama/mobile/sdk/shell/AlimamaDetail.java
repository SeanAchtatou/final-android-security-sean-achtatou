package com.alimama.mobile.sdk.shell;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.alimama.mobile.sdk.config.MmuSDKFactory;
import com.alimama.mobile.sdk.config.system.MMLog;
import com.alimama.mobile.sdk.config.system.MmuSDKImpl;
import java.util.HashMap;
import java.util.Map;

public class AlimamaDetail extends FragmentActivity {
    Map<String, Object> map = null;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.map = new HashMap();
            this.map.put("alimamaDetail", extras);
        }
        try {
            super.onCreate(bundle);
        } catch (Fragment.InstantiationException e) {
            MMLog.e(e, "", new Object[0]);
            finish();
        }
        requestWindowFeature(1);
        try {
            FrameLayout frameLayout = new FrameLayout(this);
            frameLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
            frameLayout.setId(16908300);
            setContentView(frameLayout);
            Fragment findFragment = ((MmuSDKImpl) MmuSDKFactory.getMmuSDK()).getApfSystem().findFragment(this.map);
            FragmentTransaction beginTransaction = getSupportFragmentManager().beginTransaction();
            beginTransaction.add(16908300, findFragment);
            beginTransaction.commit();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void finish() {
        super.finish();
    }
}
