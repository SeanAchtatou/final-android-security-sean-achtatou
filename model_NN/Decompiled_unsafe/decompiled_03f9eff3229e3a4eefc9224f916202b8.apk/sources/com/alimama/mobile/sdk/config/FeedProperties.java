package com.alimama.mobile.sdk.config;

import com.alimama.mobile.sdk.MmuSDK;

public class FeedProperties extends MmuProperties {
    public static final int TYPE = 12;
    private static final FeedController mController = new FeedController();
    public boolean scrollAble = true;

    public FeedProperties(String str) {
        super(str, 12);
    }

    public static FeedController getMmuController() {
        if (MmuSDK.PLUGIN_LOAD_STATUS.COMPLETED == MmuSDKFactory.getMmuSDK().getStatus()) {
            return mController;
        }
        return null;
    }

    public String[] getPluginNames() {
        return new String[]{"FeedPlugin"};
    }
}
