package com.alimama.mobile.sdk.config.system.bridge;

import android.app.Instrumentation;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.util.Log;
import android.view.ContextThemeWrapper;
import com.alimama.mobile.sdk.config.ExchangeConstants;
import com.alimama.mobile.sdk.config.system.BridgeSystem;
import com.alimama.mobile.sdk.hack.AssertionArrayException;
import com.alimama.mobile.sdk.hack.Hack;
import com.alimama.mobile.sdk.lab.InstrumentationHook;
import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;

@BridgeSystem.GROUP(TYPE = BridgeSystem.GroupType.OS)
public class GodModeHacks implements BridgeSystem.HackCollection {
    public static Hack.HackedClass<Object> ActivityThread;
    public static Hack.HackedMethod ActivityThread_currentActivityThread;
    public static Hack.HackedField<Object, Instrumentation> ActivityThread_mInstrumentation;
    public static Hack.HackedField<Object, Map> ActivityThread_mPackages;
    public static Hack.HackedClass<Object> ContextImpl;
    public static Hack.HackedField<Object, Resources> ContextImpl_mResources;
    public static Hack.HackedField<Object, Resources.Theme> ContextImpl_mTheme;
    public static Hack.HackedClass<ContextThemeWrapper> ContextThemeWrapper;
    public static Hack.HackedField<ContextThemeWrapper, Resources> ContextThemeWrapper_mResources;
    public static Hack.HackedClass<Object> LoadedApk;
    public static Hack.HackedField<Object, ClassLoader> LoadedApk_mClassLoader;
    public static Hack.HackedField<Object, Resources> LoadedApk_mResources;
    public static Hack.HackedClass<Resources> Resources;
    public static Hack.HackedField<Resources, Configuration> Resources_mConfiguration;
    private AssertionArrayException mExceptionArray = null;

    public static void inject(Context context) {
        try {
            Resources resources = (Resources) FrameworkBridge.FrameworkLoader_getPluginResources.invoke(null, new Object[0]);
            ClassLoader classLoader = (ClassLoader) FrameworkBridge.FrameworkLoader_getPluginClassLoader.invoke(null, new Object[0]);
            Resources_mConfiguration.on(resources).set(context.getApplicationContext().getResources().getConfiguration());
            Object invoke = ActivityThread_currentActivityThread.invoke(null, new Object[0]);
            Map map = ActivityThread_mPackages.on(invoke).get();
            for (Object obj : map.keySet()) {
                Object obj2 = ((WeakReference) map.get(obj)).get();
                if (obj2 != null) {
                    LoadedApk_mResources.on(obj2).set(resources);
                    LoadedApk_mClassLoader.on(obj2).set(classLoader);
                }
            }
            ContextImpl_mResources.on(context).set(resources);
            ContextImpl_mTheme.on(context).set(null);
            ActivityThread_mInstrumentation.on(invoke).set(new InstrumentationHook(ActivityThread_mInstrumentation.on(invoke).get(), context));
        } catch (InvocationTargetException e) {
            Log.e(ExchangeConstants.LOG_TAG, "插件环境初始化失败", e);
        }
    }

    public void allClasses() throws Hack.HackDeclaration.HackAssertionException {
        if (Build.VERSION.SDK_INT <= 8) {
            LoadedApk = Hack.into("android.app.ActivityThread$PackageInfo");
        } else {
            LoadedApk = Hack.into("android.app.LoadedApk");
        }
        ActivityThread = Hack.into("android.app.ActivityThread");
        ContextImpl = Hack.into("android.app.ContextImpl");
        ContextThemeWrapper = Hack.into(ContextThemeWrapper.class);
        Resources = Hack.into(Resources.class);
    }

    public void allFields() throws Hack.HackDeclaration.HackAssertionException {
        ActivityThread_mPackages = ActivityThread.field("mPackages").ofType(Map.class);
        ActivityThread_mInstrumentation = ActivityThread.field("mInstrumentation").ofType(Instrumentation.class);
        LoadedApk_mResources = LoadedApk.field("mResources").ofType(Resources.class);
        LoadedApk_mClassLoader = LoadedApk.field("mClassLoader").ofType(ClassLoader.class);
        ContextImpl_mResources = ContextImpl.field("mResources").ofType(Resources.class);
        ContextImpl_mTheme = ContextImpl.field("mTheme").ofType(Resources.Theme.class);
        Resources_mConfiguration = Resources.field("mConfiguration").ofType(Configuration.class);
        try {
            if (Build.VERSION.SDK_INT >= 17 && ContextThemeWrapper.getmClass().getDeclaredField("mResources") != null) {
                ContextThemeWrapper_mResources = ContextThemeWrapper.field("mResources").ofType(Resources.class);
            }
        } catch (NoSuchFieldException e) {
            Log.w("Hack", "Not found ContextThemeWrapper.mResources on VERSION " + Build.VERSION.SDK_INT);
        }
    }

    public void allMethods() throws Hack.HackDeclaration.HackAssertionException {
        ActivityThread_currentActivityThread = ActivityThread.method("currentActivityThread", new Class[0]);
    }
}
