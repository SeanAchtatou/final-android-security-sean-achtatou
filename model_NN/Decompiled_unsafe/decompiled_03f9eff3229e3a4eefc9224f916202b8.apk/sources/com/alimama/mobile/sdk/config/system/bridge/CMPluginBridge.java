package com.alimama.mobile.sdk.config.system.bridge;

import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.ViewGroup;
import com.alimama.mobile.pluginframework.core.PluginFramework;
import com.alimama.mobile.sdk.config.system.BridgeSystem;
import com.alimama.mobile.sdk.config.system.MMLog;
import com.alimama.mobile.sdk.hack.Hack;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.Map;

@BridgeSystem.GROUP(TYPE = BridgeSystem.GroupType.COMMON)
public class CMPluginBridge implements BridgeSystem.HackCollection {
    public static Hack.HackedClass<Object> AccountService;
    public static Hack.HackedMethod AccountService_getDefault;
    public static Hack.HackedMethod AccountService_handleResult;
    public static Hack.HackedMethod AccountService_init;
    public static Hack.HackedClass<Object> AlimmContext;
    public static Hack.HackedMethod AlimmContext_getAliContext;
    public static Hack.HackedMethod AlimmContext_getAppContext;
    public static Hack.HackedMethod AlimmContext_getAppUtils;
    public static Hack.HackedMethod AlimmContext_init;
    public static Hack.HackedClass<Object> AppUtils;
    public static Hack.HackedMethod AppUtils_getCurrentNetowrk;
    public static Hack.HackedMethod AppUtils_getDeviceID;
    public static Hack.HackedClass<Object> BaseFragment;
    public static Hack.HackedMethod BaseFragment_dispatchKeyEvent;
    public static Hack.HackedClass<Fragment> BrowserFragment;
    public static Hack.HackedMethod BrowserFragmentOnFragmentResult;
    public static Hack.HackedClass<Object> DownloadProvider;
    public static Hack.HackedMethod DownloadProvider_onBind;
    public static Hack.HackedMethod DownloadProvider_onCreate;
    public static Hack.HackedMethod DownloadProvider_onDestroy;
    public static Hack.HackedMethod DownloadProvider_onStartCommand;
    public static Hack.HackedMethod DownloadProvider_setHostService;
    public static Hack.HackedClass<Object> ExchangeDataService;
    public static Hack.HackedMethod ExchangeDataService_clickOnPromoter;
    public static Hack.HackedMethod ExchangeDataService_reportImpression;
    public static Hack.HackedClass<Object> Promoter;
    public static Hack.HackedField<Object, String> Promoter_ad_words;
    public static Hack.HackedField<Object, String> Promoter_description;
    public static Hack.HackedField<Object, String> Promoter_icon;
    public static Hack.HackedField<Object, String> Promoter_img;
    public static Hack.HackedField<Object, String> Promoter_price;
    public static Hack.HackedField<Object, Object> Promoter_promoterPrice;
    public static Hack.HackedField<Object, String> Promoter_title;
    public static Hack.HackedClass<Object> Router;
    public static Hack.HackedMethod Router_getFragment;
    public static Hack.HackedMethod Router_getService;
    public static Hack.HackedMethod Router_insetView;
    public static Hack.HackedMethod Router_normalView;
    public static Hack.HackedClass<Object> ShareUtils;
    public static Hack.HackedMethod ShareUtils_sendPlatformClick;

    public static ClassLoader getClassLoader() {
        try {
            return PluginFramework.getPluginClassLoader();
        } catch (Exception e) {
            Log.e("wt", "", e);
            return null;
        }
    }

    public static String getDeviceID() {
        try {
            return AppUtils_getDeviceID.invoke(AlimmContext_getAppUtils.invoke(AlimmContext_getAliContext.invoke(null, new Object[0]), new Object[0]), new Object[0]).toString();
        } catch (InvocationTargetException e) {
            throw new RuntimeException("Hack调用失败", e);
        }
    }

    public static Fragment getBrowserFragment() {
        try {
            return (Fragment) BrowserFragment.constructor(new Class[0]).getInstance(new Object[0]);
        } catch (Hack.HackDeclaration.HackAssertionException e) {
            MMLog.e(e, "", new Object[0]);
            return null;
        }
    }

    public static Context getAppContext() {
        try {
            return (Context) AlimmContext_getAppContext.invoke(AlimmContext_getAliContext.invoke(null, new Object[0]), new Object[0]);
        } catch (InvocationTargetException e) {
            throw new RuntimeException("Hack调用失败", e);
        }
    }

    public static String getCurrentNetwork() {
        try {
            return ((Object[]) AppUtils_getCurrentNetowrk.invoke(AlimmContext_getAppUtils.invoke(AlimmContext_getAliContext.invoke(null, new Object[0]), new Object[0]), new Object[0]))[0].toString();
        } catch (InvocationTargetException e) {
            throw new RuntimeException("Hack调用失败", e);
        }
    }

    public static void accountServiceInit(Context context) {
        try {
            Object invoke = AccountService_getDefault.invoke(null, new Object[0]);
            AccountService_init.invoke(invoke, context);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean accountServiceHandleResult(int i, int i2, Intent intent, Activity activity) {
        try {
            Object invoke = AccountService_getDefault.invoke(null, new Object[0]);
            return ((Boolean) AccountService_handleResult.invoke(invoke, Integer.valueOf(i), Integer.valueOf(i2), intent, activity)).booleanValue();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean BrowserFragmentHandleResult(int i, int i2, Intent intent, Fragment fragment) {
        try {
            return ((Boolean) BrowserFragmentOnFragmentResult.invoke(fragment, Integer.valueOf(i), Integer.valueOf(i2), intent)).booleanValue();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public void allClasses() throws Hack.HackDeclaration.HackAssertionException {
        AlimmContext = Hack.into(getClassLoader(), "com.taobao.newxp.common.AlimmContext");
        AppUtils = Hack.into(getClassLoader(), "com.taobao.munion.base.AppUtils");
        Router = Hack.into(getClassLoader(), "com.alimama.mobile.plugin.common.Router");
        DownloadProvider = Hack.into(getClassLoader(), "com.taobao.munion.base.download.DownloadProvider");
        BaseFragment = Hack.into(getClassLoader(), "com.taobao.newxp.view.BaseFragment");
        ExchangeDataService = Hack.into(getClassLoader(), "com.taobao.newxp.controller.ExchangeDataService");
        Promoter = Hack.into(getClassLoader(), "com.taobao.newxp.Promoter");
        AccountService = Hack.into(getClassLoader(), "com.taobao.newxp.controller.AccountService");
        BrowserFragment = Hack.into(getClassLoader(), "com.taobao.newxp.view.common.BrowserFragment");
        ShareUtils = Hack.into(getClassLoader(), "com.alimama.mobile.share.ShareUtils");
    }

    public void allMethods() throws Hack.HackDeclaration.HackAssertionException {
        AlimmContext_getAliContext = AlimmContext.method("getAliContext", new Class[0]);
        AlimmContext_init = AlimmContext.method("init", Context.class);
        AlimmContext_getAppContext = AlimmContext.method("getAppContext", new Class[0]);
        AlimmContext_getAppUtils = AlimmContext.method("getAppUtils", new Class[0]);
        AppUtils_getCurrentNetowrk = AppUtils.method("getCurrentNetowrk", new Class[0]);
        AppUtils_getDeviceID = AppUtils.method("getDeviceID", new Class[0]);
        Router_getFragment = Router.method("getFragment", Map.class);
        Router_getService = Router.method("getService", Map.class);
        Router_insetView = Router.method("insetView", ViewGroup.class, Map.class);
        Router_normalView = Router.method("normalView", Context.class, Map.class);
        DownloadProvider_onBind = DownloadProvider.method("onBind", Intent.class);
        DownloadProvider_onStartCommand = DownloadProvider.method("onStartCommand", Intent.class, Integer.TYPE, Integer.TYPE);
        DownloadProvider_onCreate = DownloadProvider.method("onCreate", new Class[0]);
        DownloadProvider_onDestroy = DownloadProvider.method("onDestroy", new Class[0]);
        DownloadProvider_setHostService = DownloadProvider.method("setHostService", Service.class);
        BaseFragment_dispatchKeyEvent = BaseFragment.method("dispatchKeyEvent", KeyEvent.class);
        ExchangeDataService_reportImpression = ExchangeDataService.method("reportImpression", Collection.class);
        ExchangeDataService_clickOnPromoter = ExchangeDataService.method("clickOnPromoter", Activity.class, Promoter.getmClass());
        AccountService_getDefault = AccountService.staticMethod("getDefault", new Class[0]);
        AccountService_init = AccountService.method("init", Context.class);
        AccountService_handleResult = AccountService.method("handleResult", Integer.TYPE, Integer.TYPE, Intent.class, Activity.class);
        BrowserFragmentOnFragmentResult = BrowserFragment.method("onFragmentResult", Integer.TYPE, Integer.TYPE, Intent.class);
        ShareUtils_sendPlatformClick = ShareUtils.method("sendPlatformClick", String.class, String.class, Integer.TYPE);
    }

    public void allFields() throws Hack.HackDeclaration.HackAssertionException {
        Promoter_title = Promoter.field("title").ofType(String.class);
        Promoter_ad_words = Promoter.field("ad_words").ofType(String.class);
        Promoter_description = Promoter.field("description").ofType(String.class);
        Promoter_icon = Promoter.field("icon").ofType(String.class);
        Promoter_img = Promoter.field("img").ofType(String.class);
        Promoter_price = Promoter.field("price").ofType(String.class);
        Promoter_promoterPrice = Promoter.field("promoterPrice");
    }
}
