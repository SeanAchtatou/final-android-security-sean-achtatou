package com.alimama.mobile.sdk.config;

import java.io.UnsupportedEncodingException;

public interface IAdBaseInfo {

    public static class Type {
        public static final int AD_BASED = 0;
        public static final int AD_DEVICE = 2;
        public static final int AD_VIDEO = 1;
    }

    String encodeModel() throws UnsupportedEncodingException;

    String getId();

    int getType();
}
