package com.alimama.mobile.sdk.config;

import com.alimama.mobile.sdk.config.system.bridge.CMPluginBridge;

public class MMPromoter {
    protected Object promoter;

    public MMPromoter(Object obj) {
        this.promoter = obj;
    }

    public String getTitle() {
        return CMPluginBridge.Promoter_title.on(this.promoter).get();
    }

    public String getAdWords() {
        return CMPluginBridge.Promoter_ad_words.on(this.promoter).get();
    }

    public String getDescription() {
        return CMPluginBridge.Promoter_description.on(this.promoter).get();
    }

    public String getPrice() {
        return CMPluginBridge.Promoter_price.on(this.promoter).get();
    }

    public double getPromoterPrice() {
        return Double.parseDouble(CMPluginBridge.Promoter_promoterPrice.on(this.promoter).get().toString());
    }

    public String getIcon() {
        return CMPluginBridge.Promoter_icon.on(this.promoter).get();
    }

    public String getImg() {
        return CMPluginBridge.Promoter_img.on(this.promoter).get();
    }

    public Object getField(String str) {
        try {
            return this.promoter.getClass().getField(str).get(this.promoter);
        } catch (Exception e) {
            return null;
        }
    }

    public String toString() {
        return "MMPromoter:\r{\r     title=" + getTitle() + "\r     adWords=" + getAdWords() + "\r     description=" + getDescription() + "\r     price=" + getPrice() + "\r     promoterPrice=" + getPromoterPrice() + "\r     icon=" + getIcon() + "\r     img=" + getImg() + '}';
    }
}
