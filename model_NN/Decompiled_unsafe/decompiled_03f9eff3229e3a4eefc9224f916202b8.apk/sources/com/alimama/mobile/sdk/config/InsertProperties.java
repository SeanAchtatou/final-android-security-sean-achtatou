package com.alimama.mobile.sdk.config;

import android.view.ViewGroup;
import com.alimama.mobile.sdk.config.InsertController;
import com.alimama.mobile.sdk.config.MmuController;

public class InsertProperties extends MmuProperties {
    public static final int TYPE = 15;
    private ViewGroup container;
    private final InsertController mController = new InsertController();
    private boolean videoInsert = false;

    public InsertProperties(String str, ViewGroup viewGroup) {
        super(str, 15);
        this.container = viewGroup;
    }

    public ViewGroup getContainer() {
        return this.container;
    }

    public boolean getVideoInsert() {
        return this.videoInsert;
    }

    public void setVideoInsert(boolean z) {
        this.videoInsert = z;
    }

    public InsertController getMmuController() {
        return this.mController;
    }

    public void setClickCallBackListener(MmuController.ClickCallBackListener clickCallBackListener) {
        this.mController.setClickCallBackListener(clickCallBackListener);
    }

    public void setOnStateChangeCallBackListener(InsertController.OnStateChangeCallBackListener onStateChangeCallBackListener) {
        this.mController.setOnStateChangeCallBackListener(onStateChangeCallBackListener);
    }

    public String[] getPluginNames() {
        return new String[]{"InsertPlugin"};
    }
}
