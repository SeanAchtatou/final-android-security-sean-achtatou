package com.alimama.mobile.sdk.config;

import android.view.ViewGroup;

public class LoopImageProperties extends MmuProperties {
    public static final int TYPE = 43;
    private LoopImageConfig config;
    private ViewGroup container;
    private LoopImageController loopImageController;

    public LoopImageProperties(String str, ViewGroup viewGroup) {
        super(str, 43);
        this.container = viewGroup;
    }

    public ViewGroup getContainer() {
        return this.container;
    }

    public MmuController getMmuController() {
        if (this.loopImageController == null) {
            this.loopImageController = LoopImageController.newInstance();
        }
        return this.loopImageController;
    }

    public LoopImageConfig getConfig() {
        return this.config;
    }

    public void setConfig(LoopImageConfig loopImageConfig) {
        this.config = loopImageConfig;
    }

    public String[] getPluginNames() {
        return new String[]{"LoopImagePlugin"};
    }
}
