package com.alimama.mobile.sdk.shell;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.alimama.mobile.sdk.config.system.bridge.TaePluginBridge;

public class TaeLoginWebView extends FragmentActivity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        try {
            FrameLayout frameLayout = new FrameLayout(this);
            frameLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
            frameLayout.setId(16908300);
            requestWindowFeature(1);
            setContentView(frameLayout);
            FragmentTransaction beginTransaction = getSupportFragmentManager().beginTransaction();
            Fragment loginWebViewFragment = TaePluginBridge.getInstance().getLoginWebViewFragment();
            if (loginWebViewFragment != null) {
                beginTransaction.add(16908300, loginWebViewFragment);
                beginTransaction.commit();
                return;
            }
            finish();
        } catch (Exception e) {
            Log.e("AlimamaWall", "", e);
        }
    }
}
