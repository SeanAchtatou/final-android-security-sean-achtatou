package com.alimama.mobile.sdk.config.system;

import android.app.Activity;
import java.lang.ref.WeakReference;
import java.util.HashSet;
import java.util.Set;

public class HookManager {
    private static final Set<WeakReference<Activity>> hookActivitys = new HashSet();
    private static final Set<String> regActivitys = new HashSet();

    public static void registerAcvitity(Class<? extends Activity> cls) {
        regActivitys.add(cls.getCanonicalName());
    }

    public static boolean containsRegActivity(String str) {
        if (str.startsWith("com.alimama.mobile.sdk.shell")) {
            return true;
        }
        return regActivitys.contains(str);
    }

    public static void addHookActivity(Activity activity) {
        hookActivitys.add(new WeakReference(activity));
    }

    public static boolean containsHookActivity(Activity activity) {
        if (activity == null) {
            return false;
        }
        for (WeakReference<Activity> weakReference : hookActivitys) {
            if (weakReference.get() == activity) {
                return true;
            }
        }
        return false;
    }
}
