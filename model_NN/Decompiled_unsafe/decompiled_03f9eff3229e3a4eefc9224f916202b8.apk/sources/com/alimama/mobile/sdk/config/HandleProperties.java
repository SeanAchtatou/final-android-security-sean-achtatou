package com.alimama.mobile.sdk.config;

import android.view.ViewGroup;
import com.baidu.mobads.interfaces.IXAdRequestInfo;

public class HandleProperties extends MmuProperties {
    public static final int TYPE = 7;
    private ViewGroup container;
    private ViewGroup handleContainer;

    public HandleProperties(String str, ViewGroup viewGroup) {
        super(str, 7);
        this.container = viewGroup;
    }

    public ViewGroup getContainer() {
        return this.container;
    }

    public ViewGroup getHandleContainer() {
        return this.handleContainer;
    }

    public HandleProperties setHandleContainer(ViewGroup viewGroup) {
        if (viewGroup != null) {
            viewGroup.setTag(IXAdRequestInfo.CS);
            this.handleContainer = viewGroup;
        }
        return this;
    }

    public String[] getPluginNames() {
        return new String[]{"HandWallPlugin"};
    }
}
