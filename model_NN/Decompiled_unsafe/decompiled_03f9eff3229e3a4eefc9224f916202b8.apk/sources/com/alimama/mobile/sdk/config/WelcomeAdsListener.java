package com.alimama.mobile.sdk.config;

import android.view.View;

public interface WelcomeAdsListener {
    void onCountdown(int i);

    void onDataReviced(MMPromoter mMPromoter);

    void onError(String str);

    void onFinish();

    void onShow(View view);
}
