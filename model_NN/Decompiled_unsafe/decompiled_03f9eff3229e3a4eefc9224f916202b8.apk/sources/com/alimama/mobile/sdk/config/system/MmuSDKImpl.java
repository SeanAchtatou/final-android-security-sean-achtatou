package com.alimama.mobile.sdk.config.system;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.ViewGroup;
import com.alimama.mobile.pluginframework.core.PluginFramework;
import com.alimama.mobile.sdk.MMuSDKUtils;
import com.alimama.mobile.sdk.MmuSDK;
import com.alimama.mobile.sdk.config.BannerProperties;
import com.alimama.mobile.sdk.config.ContainerProperties;
import com.alimama.mobile.sdk.config.ExchangeConstants;
import com.alimama.mobile.sdk.config.FeedProperties;
import com.alimama.mobile.sdk.config.HandleProperties;
import com.alimama.mobile.sdk.config.InsertProperties;
import com.alimama.mobile.sdk.config.LoopImageProperties;
import com.alimama.mobile.sdk.config.MmuController;
import com.alimama.mobile.sdk.config.MmuProperties;
import com.alimama.mobile.sdk.config.TextLinkProperties;
import com.alimama.mobile.sdk.config.WelcomeAdsListener;
import com.alimama.mobile.sdk.config.WelcomeProperties;
import com.alimama.mobile.sdk.config.system.BridgeSystem;
import com.alimama.mobile.sdk.config.system.bridge.CMPluginBridge;
import com.alimama.mobile.sdk.config.system.bridge.GodModeHacks;
import com.alimama.mobile.sdk.config.system.bridge.RuntimeBridge;
import com.alimama.mobile.sdk.hack.Hack;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class MmuSDKImpl implements MmuSDK {
    private static final Lock initLock = new ReentrantReadWriteLock().writeLock();
    /* access modifiers changed from: private */
    public volatile MmuSDK.PLUGIN_LOAD_STATUS STATUS = MmuSDK.PLUGIN_LOAD_STATUS.INITIAL;
    /* access modifiers changed from: private */
    public APFSystem apfSystem = new APFSystem();
    /* access modifiers changed from: private */
    public Application application;
    /* access modifiers changed from: private */
    public MmuController.InitAsyncComplete initAsyncCompleteListener;
    /* access modifiers changed from: private */
    public Set<String> loadedPlugins;
    /* access modifiers changed from: private */
    public STSystem stSystem = new STSystemImpl();

    public APFSystem getApfSystem() {
        return this.apfSystem;
    }

    public void init(Application application2) {
        initLock.lock();
        this.application = application2;
        if (this.STATUS == MmuSDK.PLUGIN_LOAD_STATUS.INITIAL) {
            try {
                if (!this.stSystem.stAssetPlugin(application2.getApplicationContext().getAssets())) {
                    Log.e(ExchangeConstants.LOG_TAG, "请检查是否添加FrameworkPlugin插件到 asset/mu/ 目录下。");
                    return;
                } else if (!this.stSystem.stManifest()) {
                    Log.e(ExchangeConstants.LOG_TAG, "Manifest完整性检查失败。");
                    return;
                } else {
                    BridgeSystem.defineAndVerify(BridgeSystem.GroupType.OS);
                    BridgeSystem.defineAndVerify(BridgeSystem.GroupType.APP);
                    this.loadedPlugins = this.apfSystem.init(application2);
                    if (this.stSystem.stLoadedFrameWorkPlugin()) {
                        BridgeSystem.defineAndVerify(BridgeSystem.GroupType.FRAMEWORK);
                        if (!this.stSystem.stLoadedCommonPlugin()) {
                            Log.w(ExchangeConstants.LOG_TAG, "无法完成初始化，CommonPlugin未加载成功。");
                            this.STATUS = MmuSDK.PLUGIN_LOAD_STATUS.INCOMPLETED;
                            return;
                        }
                        BridgeSystem.defineAndVerify(BridgeSystem.GroupType.COMMON);
                        Object invoke = CMPluginBridge.AlimmContext_getAliContext.invoke(null, new Object[0]);
                        CMPluginBridge.AlimmContext_init.invoke(invoke, application2);
                        GodModeHacks.inject(application2.getBaseContext());
                        this.STATUS = MmuSDK.PLUGIN_LOAD_STATUS.COMPLETED;
                    } else {
                        Log.w(ExchangeConstants.LOG_TAG, "Framework加载失败，无法完成初始化。");
                        return;
                    }
                }
            } catch (Exception e) {
                Log.e(ExchangeConstants.LOG_TAG, "无法初始化MMSDK", e);
                this.STATUS = MmuSDK.PLUGIN_LOAD_STATUS.INCOMPLETED;
            }
        }
        initLock.unlock();
    }

    public void initAsync(final Application application2) {
        initLock.lock();
        new AsyncTask<String, String, Boolean>() {
            /* access modifiers changed from: protected */
            public Boolean doInBackground(String... strArr) {
                if (MmuSDKImpl.this.STATUS == MmuSDK.PLUGIN_LOAD_STATUS.INITIAL) {
                    try {
                        if (!MmuSDKImpl.this.stSystem.stAssetPlugin(application2.getApplicationContext().getAssets())) {
                            Log.e(ExchangeConstants.LOG_TAG, "请检查是否添加FrameworkPlugin插件到 asset/mu/ 目录下。");
                            return false;
                        } else if (!MmuSDKImpl.this.stSystem.stManifest()) {
                            Log.e(ExchangeConstants.LOG_TAG, "Manifest完整性检查失败。");
                            return false;
                        } else {
                            BridgeSystem.defineAndVerify(BridgeSystem.GroupType.OS);
                            BridgeSystem.defineAndVerify(BridgeSystem.GroupType.APP);
                            Set unused = MmuSDKImpl.this.loadedPlugins = MmuSDKImpl.this.apfSystem.init(application2);
                            if (MmuSDKImpl.this.stSystem.stLoadedFrameWorkPlugin()) {
                                BridgeSystem.defineAndVerify(BridgeSystem.GroupType.FRAMEWORK);
                                if (!MmuSDKImpl.this.stSystem.stLoadedCommonPlugin()) {
                                    Log.w(ExchangeConstants.LOG_TAG, "无法完成初始化，CommonPlugin未加载成功。");
                                    MmuSDK.PLUGIN_LOAD_STATUS unused2 = MmuSDKImpl.this.STATUS = MmuSDK.PLUGIN_LOAD_STATUS.INCOMPLETED;
                                    return false;
                                }
                                BridgeSystem.defineAndVerify(BridgeSystem.GroupType.COMMON);
                            } else {
                                Log.w(ExchangeConstants.LOG_TAG, "Framework加载失败，无法完成初始化。");
                                return false;
                            }
                        }
                    } catch (Exception e) {
                        Log.e(ExchangeConstants.LOG_TAG, "无法初始化MMSDK", e);
                        MmuSDK.PLUGIN_LOAD_STATUS unused3 = MmuSDKImpl.this.STATUS = MmuSDK.PLUGIN_LOAD_STATUS.INCOMPLETED;
                    }
                }
                return true;
            }

            /* access modifiers changed from: protected */
            public void onPostExecute(Boolean bool) {
                if (bool.booleanValue() == Boolean.TRUE.booleanValue()) {
                    try {
                        Object invoke = CMPluginBridge.AlimmContext_getAliContext.invoke(null, new Object[0]);
                        CMPluginBridge.AlimmContext_init.invoke(invoke, application2);
                        GodModeHacks.inject(application2.getBaseContext());
                        MmuSDK.PLUGIN_LOAD_STATUS unused = MmuSDKImpl.this.STATUS = MmuSDK.PLUGIN_LOAD_STATUS.COMPLETED;
                    } catch (Exception e) {
                        Log.w(ExchangeConstants.LOG_TAG, "无法完成初始化，alimmContext未初始化成功。");
                    }
                }
                if (MmuSDKImpl.this.initAsyncCompleteListener != null) {
                    MmuSDKImpl.this.initAsyncCompleteListener.onComplete(bool.booleanValue());
                }
                super.onPostExecute((Object) bool);
            }

            /* access modifiers changed from: protected */
            public void onPreExecute() {
                super.onPreExecute();
                Application unused = MmuSDKImpl.this.application = application2;
            }
        }.execute(new String[0]);
        initLock.unlock();
    }

    public MmuSDK.PLUGIN_LOAD_STATUS getStatus() {
        return this.STATUS;
    }

    public <T extends MmuProperties> boolean attach(T t) {
        HashMap hashMap = new HashMap();
        hashMap.put(PluginFramework.KEY_UPDATE_SLOTID, t.getSlot_id());
        hashMap.put("properties", t);
        hashMap.put(PluginFramework.KEY_UPDATE_LAYOUTTYPE, Integer.valueOf(t.getLayoutType()));
        if (this.application != null) {
            this.apfSystem.updateSDK(MMuSDKUtils.addExtraParams(hashMap, this.application.getApplicationContext()));
        } else {
            MMLog.w("无法更新插件，application is null.", new Object[0]);
        }
        if (!this.stSystem.stPlugin(t)) {
            Log.w(ExchangeConstants.LOG_TAG, "MMSDK 插件加载环境检查失败，无法启动样式。");
            return false;
        }
        if (t instanceof HandleProperties) {
            try {
                this.apfSystem.inset(((HandleProperties) t).getContainer(), hashMap);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (t instanceof ContainerProperties) {
            try {
                this.apfSystem.inset(((ContainerProperties) t).getContainer(), hashMap);
            } catch (Exception e2) {
            }
        } else if (t instanceof BannerProperties) {
            BannerProperties bannerProperties = (BannerProperties) t;
            hashMap.put("controller", bannerProperties.getMmuController());
            hashMap.put("isstretch", Boolean.valueOf(bannerProperties.isStretch()));
            try {
                this.apfSystem.inset(bannerProperties.getContainer(), hashMap);
            } catch (Exception e3) {
                e3.printStackTrace();
            }
        } else if (t instanceof InsertProperties) {
            InsertProperties insertProperties = (InsertProperties) t;
            hashMap.put("isvideo", Boolean.valueOf(insertProperties.getVideoInsert()));
            hashMap.put("controller", insertProperties.getMmuController());
            try {
                this.apfSystem.inset(insertProperties.getContainer(), hashMap);
            } catch (Exception e4) {
                e4.printStackTrace();
            }
        } else if (t instanceof FeedProperties) {
            FeedProperties feedProperties = (FeedProperties) t;
            feedProperties.getSlot_id();
            Object obj = feedProperties.tag;
            boolean z = feedProperties.scrollAble;
            this.apfSystem.addFeedMaterial(feedProperties);
        } else if (t instanceof TextLinkProperties) {
            try {
                this.apfSystem.inset(((TextLinkProperties) t).getContainer(), hashMap);
            } catch (Exception e5) {
                e5.printStackTrace();
            }
        } else if (t instanceof WelcomeProperties) {
            WelcomeProperties welcomeProperties = (WelcomeProperties) t;
            long minDelay = welcomeProperties.getMinDelay();
            long maxDelay = welcomeProperties.getMaxDelay();
            ViewGroup container = welcomeProperties.getContainer();
            if (container != null) {
                hashMap.put("container", container);
            }
            Context context = welcomeProperties.getContext();
            WelcomeAdsListener welcomeAdsListener = welcomeProperties.getWelcomeAdsListener();
            hashMap.put("minDelay", Long.valueOf(minDelay));
            hashMap.put("maxDelay", Long.valueOf(maxDelay));
            if (welcomeAdsListener != null) {
                hashMap.put("welcomeListener", welcomeAdsListener);
            }
            try {
                this.apfSystem.normalView(context, hashMap);
            } catch (Exception e6) {
                e6.printStackTrace();
            }
        } else if (t instanceof LoopImageProperties) {
            LoopImageProperties loopImageProperties = (LoopImageProperties) t;
            hashMap.put("controller", loopImageProperties.getMmuController());
            if (loopImageProperties.getConfig() != null) {
                hashMap.put("loopImageConfig", loopImageProperties.getConfig());
            }
            try {
                this.apfSystem.inset(loopImageProperties.getContainer(), hashMap);
            } catch (Exception e7) {
                e7.printStackTrace();
            }
        }
        return true;
    }

    public <T extends MmuProperties> Fragment findFragment(T t) {
        return null;
    }

    public boolean loadplugin(String str) {
        if (this.loadedPlugins == null) {
            return false;
        }
        return this.loadedPlugins.contains(str);
    }

    public void accountServiceInit(Context context) {
        CMPluginBridge.accountServiceInit(context);
    }

    public boolean accountServiceHandleResult(int i, int i2, Intent intent, Activity activity) {
        return CMPluginBridge.accountServiceHandleResult(i, i2, intent, activity);
    }

    public void alimamaJsSdkOnDestroy() {
        try {
            RuntimeBridge.alimamaJSdkController_onDestroy(CMPluginBridge.getClassLoader());
        } catch (InvocationTargetException e) {
            Log.e("wt", "Hack 调用失败", e);
        } catch (Hack.HackDeclaration.HackAssertionException e2) {
            Log.e("wt", "Hack 调用失败", e2);
        }
    }

    public void alimamaJsSdkOnResume() {
        try {
            RuntimeBridge.alimamaJSdkController_onResume(CMPluginBridge.getClassLoader());
        } catch (InvocationTargetException e) {
            Log.e("wt", "Hack 调用失败", e);
        } catch (Hack.HackDeclaration.HackAssertionException e2) {
            Log.e("wt", "Hack 调用失败", e2);
        }
    }

    public void alimamaJsSdkOnPause() {
        try {
            RuntimeBridge.alimamaJSdkController_onPause(CMPluginBridge.getClassLoader());
        } catch (InvocationTargetException e) {
            Log.e("wt", "Hack 调用失败", e);
        } catch (Hack.HackDeclaration.HackAssertionException e2) {
            Log.e("wt", "Hack 调用失败", e2);
        }
    }

    public void alimamaJsSdkOnActivityResult(int i, int i2, Intent intent) {
        try {
            RuntimeBridge.alimamaJSdkController_onActivityResult(CMPluginBridge.getClassLoader(), i, i2, intent);
        } catch (InvocationTargetException e) {
            Log.e("wt", "Hack 调用失败", e);
        } catch (Hack.HackDeclaration.HackAssertionException e2) {
            Log.e("wt", "Hack 调用失败", e2);
        }
    }

    public void setInitAsyncListener(MmuController.InitAsyncComplete initAsyncComplete) {
        this.initAsyncCompleteListener = initAsyncComplete;
    }
}
