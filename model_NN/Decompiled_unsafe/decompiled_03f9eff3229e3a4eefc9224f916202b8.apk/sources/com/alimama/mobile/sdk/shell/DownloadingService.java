package com.alimama.mobile.sdk.shell;

import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import com.alimama.mobile.pluginframework.core.PluginService;
import com.alimama.mobile.pluginframework.core.PluginServiceAgent;
import com.alimama.mobile.sdk.config.MmuSDKFactory;
import com.alimama.mobile.sdk.config.system.MmuSDKImpl;

public class DownloadingService extends PluginService {
    public void onCreate() {
        super.onCreate();
        this.pluginServiceAgent.provider.onCreate();
    }

    public PluginServiceAgent findProvider() {
        try {
            return ((MmuSDKImpl) MmuSDKFactory.getMmuSDK()).getApfSystem().findProvider();
        } catch (Exception e) {
            Log.e("Download", "Find Provider Error", e);
            return null;
        }
    }

    public IBinder onBind(Intent intent) {
        return this.pluginServiceAgent.provider.onBind(intent);
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        return this.pluginServiceAgent.provider.onStartCommand(intent, i, i2);
    }

    public void onDestroy() {
        this.pluginServiceAgent.provider.onDestroy();
    }
}
