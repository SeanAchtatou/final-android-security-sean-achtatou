package com.alimama.mobile.sdk.config;

import android.app.Activity;
import com.alimama.mobile.sdk.MmuSDK;
import com.alimama.mobile.sdk.config.system.HookManager;
import com.alimama.mobile.sdk.config.system.MmuSDKImpl;

public final class MmuSDKFactory {
    private static MmuSDK mSdk;

    private MmuSDKFactory() {
    }

    public static MmuSDK getMmuSDK() {
        if (mSdk == null) {
            mSdk = new MmuSDKImpl();
        }
        return mSdk;
    }

    public static void registerAcvitity(Class<? extends Activity> cls) {
        HookManager.registerAcvitity(cls);
    }
}
