package com.alimama.mobile.sdk.config;

import android.content.Context;
import android.view.ViewGroup;
import com.baidu.mobads.interfaces.IXAdRequestInfo;

public class WelcomeProperties extends MmuProperties {
    public static final int TYPE = 9;
    private ViewGroup container;
    private Context context;
    private long maxDelay = 3000;
    private long minDelay = 1000;
    private WelcomeAdsListener welcomeAdsListener;
    private ViewGroup welcomeContainer;

    public WelcomeProperties(Context context2, String str) {
        super(str, 9);
        setContext(context2);
    }

    public WelcomeProperties(Context context2, String str, long j, long j2, WelcomeAdsListener welcomeAdsListener2) {
        super(str, 9);
        setContext(context2);
        this.minDelay = j;
        this.maxDelay = j2;
        this.welcomeAdsListener = welcomeAdsListener2;
    }

    public ViewGroup getContainer() {
        return this.welcomeContainer;
    }

    public WelcomeAdsListener getWelcomeAdsListener() {
        return this.welcomeAdsListener;
    }

    public void setWelcomeAdsListener(WelcomeAdsListener welcomeAdsListener2) {
        this.welcomeAdsListener = welcomeAdsListener2;
    }

    public long getMaxDelay() {
        return this.maxDelay;
    }

    public void setMaxDelay(long j) {
        this.maxDelay = j;
    }

    public long getMinDelay() {
        return this.minDelay;
    }

    public void setMinDelay(long j) {
        this.minDelay = j;
    }

    public Context getContext() {
        return this.context;
    }

    public void setContext(Context context2) {
        this.context = context2;
    }

    public ViewGroup getWelcomeContainer() {
        return this.welcomeContainer;
    }

    public void setWelcomeContainer(ViewGroup viewGroup) {
        if (viewGroup != null) {
            viewGroup.setTag(IXAdRequestInfo.CS);
            this.welcomeContainer = viewGroup;
        }
    }

    public String[] getPluginNames() {
        return new String[]{"WelcomePlugin"};
    }
}
