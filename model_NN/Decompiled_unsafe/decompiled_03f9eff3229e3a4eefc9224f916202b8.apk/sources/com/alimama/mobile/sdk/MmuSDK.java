package com.alimama.mobile.sdk;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import com.alimama.mobile.sdk.config.MmuController;
import com.alimama.mobile.sdk.config.MmuProperties;

public interface MmuSDK {

    public enum PLUGIN_LOAD_STATUS {
        INITIAL,
        INCOMPLETED,
        COMPLETED
    }

    boolean accountServiceHandleResult(int i, int i2, Intent intent, Activity activity);

    void accountServiceInit(Context context);

    void alimamaJsSdkOnActivityResult(int i, int i2, Intent intent);

    void alimamaJsSdkOnDestroy();

    void alimamaJsSdkOnPause();

    void alimamaJsSdkOnResume();

    <T extends MmuProperties> boolean attach(T t);

    <T extends MmuProperties> Fragment findFragment(T t);

    PLUGIN_LOAD_STATUS getStatus();

    void init(Application application);

    void initAsync(Application application);

    void setInitAsyncListener(MmuController.InitAsyncComplete initAsyncComplete);
}
