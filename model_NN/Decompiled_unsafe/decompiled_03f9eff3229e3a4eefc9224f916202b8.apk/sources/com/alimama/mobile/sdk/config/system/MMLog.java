package com.alimama.mobile.sdk.config.system;

import android.os.SystemClock;
import android.util.Log;
import java.util.ArrayList;
import java.util.List;

public class MMLog {
    public static boolean DEBUG = false;
    public static String TAG = "Munion";

    public static void setTag(String str) {
        d("Changing log tag to %s", str);
        TAG = str;
    }

    public static void v(String str, Object... objArr) {
        if (DEBUG) {
            Log.v(TAG, buildMessage(str, objArr));
        }
    }

    public static void d(String str, Object... objArr) {
        if (DEBUG) {
            Log.d(TAG, buildMessage(str, objArr));
        }
    }

    public static void i(String str, Object... objArr) {
        if (DEBUG) {
            Log.i(TAG, buildMessage(str, objArr));
        }
    }

    public static void e(String str, Object... objArr) {
        Log.e(TAG, buildMessage(str, objArr));
    }

    public static void e(Throwable th, String str, Object... objArr) {
        Log.e(TAG, buildMessage(str, objArr), th);
    }

    public static void w(String str, Object... objArr) {
        Log.w(TAG, buildMessage(str, objArr));
    }

    public static void w(Throwable th, String str, Object... objArr) {
        Log.w(TAG, buildMessage(str, objArr), th);
    }

    public static void wtf(String str, Object... objArr) {
        Log.wtf(TAG, buildMessage(str, objArr));
    }

    public static void wtf(Throwable th, String str, Object... objArr) {
        Log.wtf(TAG, buildMessage(str, objArr), th);
    }

    private static String buildMessage(String str, Object... objArr) {
        String str2;
        if (!(objArr == null || objArr.length == 0)) {
            str = String.format(str, objArr);
        }
        StackTraceElement[] stackTrace = new Throwable().fillInStackTrace().getStackTrace();
        int i = 2;
        while (true) {
            if (i >= stackTrace.length) {
                str2 = "<unknown>";
                break;
            } else if (!stackTrace[i].getClass().equals(MMLog.class)) {
                String className = stackTrace[i].getClassName();
                String substring = className.substring(className.lastIndexOf(46) + 1);
                str2 = substring.substring(substring.lastIndexOf(36) + 1) + "." + stackTrace[i].getMethodName();
                break;
            } else {
                i++;
            }
        }
        return String.format("[%d] %s: %s", Long.valueOf(Thread.currentThread().getId()), str2, str);
    }

    public static class MarkerLog {
        public static final boolean ENABLED = MMLog.DEBUG;
        private static final long MIN_DURATION_FOR_LOGGING_MS = 0;
        private boolean mFinished = false;
        private final List<Marker> mMarkers = new ArrayList();

        public synchronized void add(String str, long j) {
            if (!this.mFinished) {
                this.mMarkers.add(new Marker(str, j, SystemClock.elapsedRealtime()));
            }
        }

        public synchronized void finish(String str) {
            this.mFinished = true;
            long totalDuration = getTotalDuration();
            if (this.mMarkers.size() > 0) {
                long j = this.mMarkers.get(0).time;
                MMLog.d("<%s>(%-4d ms) %s", str, Long.valueOf(totalDuration), "BEGIN.");
                long j2 = j;
                for (Marker next : this.mMarkers) {
                    long j3 = next.time;
                    MMLog.d("<%s>(+%-4d) [%2d] %s", str, Long.valueOf(j3 - j2), Long.valueOf(next.thread), next.name);
                    j2 = j3;
                }
            }
            MMLog.d("<%s>(end.   ) %s", str, "END.");
        }

        /* access modifiers changed from: protected */
        public void finalize() throws Throwable {
            if (!this.mFinished) {
                finish("Request on the loose");
                MMLog.e("Marker log finalized without finish() - uncaught exit point for request", new Object[0]);
            }
        }

        private long getTotalDuration() {
            if (this.mMarkers.size() == 0) {
                return 0;
            }
            return this.mMarkers.get(this.mMarkers.size() - 1).time - this.mMarkers.get(0).time;
        }

        private static class Marker {
            public final String name;
            public final long thread;
            public final long time;

            public Marker(String str, long j, long j2) {
                this.name = str;
                this.thread = j;
                this.time = j2;
            }
        }
    }
}
