package com.alimama.mobile.sdk.config;

import com.alimama.mobile.sdk.config.MmuController;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class InsertController<T> implements MmuController<T> {
    private T mInstanceView;

    public enum InterstitialState {
        CLOSE,
        READY,
        ERROR,
        SHOW
    }

    public interface OnStateChangeCallBackListener {
        void onStateChanged(InterstitialState interstitialState);
    }

    public void load(String str) {
        if (this.mInstanceView != null) {
            try {
                Method declaredMethod = this.mInstanceView.getClass().getDeclaredMethod("load", String.class);
                declaredMethod.setAccessible(true);
                declaredMethod.invoke(this.mInstanceView, str);
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e2) {
                e2.printStackTrace();
            } catch (IllegalArgumentException e3) {
                e3.printStackTrace();
            } catch (InvocationTargetException e4) {
                e4.printStackTrace();
            }
        }
    }

    public void load(AdBaseInfo adBaseInfo) {
        if (this.mInstanceView != null) {
            try {
                Method declaredMethod = this.mInstanceView.getClass().getDeclaredMethod("load", AdBaseInfo.class);
                declaredMethod.setAccessible(true);
                declaredMethod.invoke(this.mInstanceView, adBaseInfo);
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e2) {
                e2.printStackTrace();
            } catch (IllegalArgumentException e3) {
                e3.printStackTrace();
            } catch (InvocationTargetException e4) {
                e4.printStackTrace();
            }
        }
    }

    public void close() {
        if (this.mInstanceView != null) {
            try {
                Method declaredMethod = this.mInstanceView.getClass().getDeclaredMethod("close", null);
                declaredMethod.setAccessible(true);
                declaredMethod.invoke(this.mInstanceView, null);
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e2) {
                e2.printStackTrace();
            } catch (IllegalArgumentException e3) {
                e3.printStackTrace();
            } catch (InvocationTargetException e4) {
                e4.printStackTrace();
            }
        }
    }

    public boolean onBackPressed() {
        if (this.mInstanceView != null) {
            try {
                Method declaredMethod = this.mInstanceView.getClass().getDeclaredMethod("onBackPressed", null);
                declaredMethod.setAccessible(true);
                return ((Boolean) declaredMethod.invoke(this.mInstanceView, null)).booleanValue();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e2) {
                e2.printStackTrace();
            } catch (IllegalArgumentException e3) {
                e3.printStackTrace();
            } catch (InvocationTargetException e4) {
                e4.printStackTrace();
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void setClickCallBackListener(MmuController.ClickCallBackListener clickCallBackListener) {
        if (this.mInstanceView != null) {
            try {
                Method declaredMethod = this.mInstanceView.getClass().getDeclaredMethod("setClickCallBackListener", MmuController.ClickCallBackListener.class);
                declaredMethod.setAccessible(true);
                declaredMethod.invoke(this.mInstanceView, clickCallBackListener);
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e2) {
                e2.printStackTrace();
            } catch (IllegalArgumentException e3) {
                e3.printStackTrace();
            } catch (InvocationTargetException e4) {
                e4.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void setOnStateChangeCallBackListener(OnStateChangeCallBackListener onStateChangeCallBackListener) {
        if (this.mInstanceView != null) {
            try {
                Method declaredMethod = this.mInstanceView.getClass().getDeclaredMethod("setOnStateChangeCallBackListener", OnStateChangeCallBackListener.class);
                declaredMethod.setAccessible(true);
                declaredMethod.invoke(this.mInstanceView, onStateChangeCallBackListener);
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e2) {
                e2.printStackTrace();
            } catch (IllegalArgumentException e3) {
                e3.printStackTrace();
            } catch (InvocationTargetException e4) {
                e4.printStackTrace();
            }
        }
    }

    public T getInstanceView() {
        return this.mInstanceView;
    }

    public void setInstanceView(T t) {
        this.mInstanceView = t;
    }
}
