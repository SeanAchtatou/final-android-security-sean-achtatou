package com.alimama.mobile.sdk.hack;

import com.alimama.mobile.sdk.hack.Interception;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Hack {
    private static AssertionFailureHandler sFailureHandler;

    public interface AssertionFailureHandler {
        boolean onAssertionFailure(HackDeclaration.HackAssertionException hackAssertionException);
    }

    public static abstract class HackDeclaration {

        public static class HackAssertionException extends Throwable {
            private static final long serialVersionUID = 1;
            private Class<?> mHackedClass;
            private String mHackedFieldName;
            private String mHackedMethodName;

            public HackAssertionException(String str) {
                super(str);
            }

            public HackAssertionException(Exception exc) {
                super(exc);
            }

            public String toString() {
                return getCause() != null ? getClass().getName() + ": " + getCause() : super.toString();
            }

            public Class<?> getHackedClass() {
                return this.mHackedClass;
            }

            public void setHackedClass(Class<?> cls) {
                this.mHackedClass = cls;
            }

            public String getHackedMethodName() {
                return this.mHackedMethodName;
            }

            public void setHackedMethodName(String str) {
                this.mHackedMethodName = str;
            }

            public String getHackedFieldName() {
                return this.mHackedFieldName;
            }

            public void setHackedFieldName(String str) {
                this.mHackedFieldName = str;
            }
        }
    }

    public static class HackedField<C, T> {
        private final Field mField;
        private Object mObject;

        public <T2> HackedField<C, T2> ofGenericType(Class<?> cls) throws HackDeclaration.HackAssertionException {
            if (this.mField != null && !cls.isAssignableFrom(this.mField.getType())) {
                Hack.fail(new HackDeclaration.HackAssertionException(new ClassCastException(this.mField + " is not of type " + cls)));
            }
            return this;
        }

        public <T2> HackedField<C, T2> ofType(Class cls) throws HackDeclaration.HackAssertionException {
            if (this.mField != null && !cls.isAssignableFrom(this.mField.getType())) {
                Hack.fail(new HackDeclaration.HackAssertionException(new ClassCastException(this.mField + " is not of type " + cls)));
            }
            return this;
        }

        public HackedField<C, T> ofType(String str) throws HackDeclaration.HackAssertionException {
            try {
                return ofType(Class.forName(str));
            } catch (ClassNotFoundException e) {
                Hack.fail(new HackDeclaration.HackAssertionException(e));
                return this;
            }
        }

        public T get() {
            try {
                T t = this.mField.get(this.mObject);
                this.mObject = null;
                return t;
            } catch (IllegalAccessException e) {
                e.printStackTrace();
                return null;
            }
        }

        public void set(Object obj) {
            try {
                this.mField.set(this.mObject, obj);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }

        public void hijack(Interception.InterceptionHandler<?> interceptionHandler) {
            Object obj = get();
            if (obj == null) {
                throw new IllegalStateException("Cannot hijack null");
            }
            set(Interception.proxy(obj, interceptionHandler, obj.getClass().getInterfaces()));
        }

        public HackedField<C, T> on(C c) {
            this.mObject = c;
            return this;
        }

        HackedField(Class<C> cls, String str, int i) throws HackDeclaration.HackAssertionException {
            Field field = null;
            if (cls == null) {
                this.mField = field;
                return;
            }
            try {
                this.mObject = null;
                field = cls.getDeclaredField(str);
                if (i > 0 && (field.getModifiers() & i) != i) {
                    Hack.fail(new HackDeclaration.HackAssertionException(field + " does not match modifiers: " + i));
                }
                field.setAccessible(true);
            } catch (NoSuchFieldException e) {
                HackDeclaration.HackAssertionException hackAssertionException = new HackDeclaration.HackAssertionException(e);
                hackAssertionException.setHackedClass(cls);
                hackAssertionException.setHackedFieldName(str);
                Hack.fail(hackAssertionException);
            } finally {
                this.mField = field;
            }
        }

        public Field getField() {
            return this.mField;
        }
    }

    public static class HackedMethod {
        protected final Method mMethod;

        public Object invoke(Object obj, Object... objArr) throws IllegalArgumentException, InvocationTargetException {
            try {
                return this.mMethod.invoke(obj, objArr);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
                return null;
            }
        }

        HackedMethod(Class<?> cls, String str, Class<?>[] clsArr, int i) throws HackDeclaration.HackAssertionException {
            Method method = null;
            if (cls == null) {
                this.mMethod = method;
                return;
            }
            try {
                method = cls.getDeclaredMethod(str, clsArr);
                if (i > 0 && (method.getModifiers() & i) != i) {
                    Hack.fail(new HackDeclaration.HackAssertionException(method + " does not match modifiers: " + i));
                }
                method.setAccessible(true);
            } catch (NoSuchMethodException e) {
                HackDeclaration.HackAssertionException hackAssertionException = new HackDeclaration.HackAssertionException(e);
                hackAssertionException.setHackedClass(cls);
                hackAssertionException.setHackedMethodName(str);
                Hack.fail(hackAssertionException);
            } finally {
                this.mMethod = method;
            }
        }

        public Method getMethod() {
            return this.mMethod;
        }
    }

    public static class HackedConstructor {
        protected Constructor<?> mConstructor;

        HackedConstructor(Class<?> cls, Class<?>[] clsArr) throws HackDeclaration.HackAssertionException {
            if (cls != null) {
                try {
                    this.mConstructor = cls.getDeclaredConstructor(clsArr);
                } catch (NoSuchMethodException e) {
                    HackDeclaration.HackAssertionException hackAssertionException = new HackDeclaration.HackAssertionException(e);
                    hackAssertionException.setHackedClass(cls);
                    Hack.fail(hackAssertionException);
                }
            }
        }

        public Object getInstance(Object... objArr) throws IllegalArgumentException {
            this.mConstructor.setAccessible(true);
            try {
                return this.mConstructor.newInstance(objArr);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    public static class HackedClass<C> {
        protected Class<C> mClass;

        public HackedField<C, Object> staticField(String str) throws HackDeclaration.HackAssertionException {
            return new HackedField<>(this.mClass, str, 8);
        }

        public HackedField<C, Object> field(String str) throws HackDeclaration.HackAssertionException {
            return new HackedField<>(this.mClass, str, 0);
        }

        public HackedMethod staticMethod(String str, Class<?>... clsArr) throws HackDeclaration.HackAssertionException {
            return new HackedMethod(this.mClass, str, clsArr, 8);
        }

        public HackedMethod method(String str, Class<?>... clsArr) throws HackDeclaration.HackAssertionException {
            return new HackedMethod(this.mClass, str, clsArr, 0);
        }

        public HackedConstructor constructor(Class<?>... clsArr) throws HackDeclaration.HackAssertionException {
            return new HackedConstructor(this.mClass, clsArr);
        }

        public HackedClass(Class<C> cls) {
            this.mClass = cls;
        }

        public Class<C> getmClass() {
            return this.mClass;
        }
    }

    public static <T> HackedClass<T> into(Class<T> cls) {
        return new HackedClass<>(cls);
    }

    public static <T> HackedClass<T> into(String str) throws HackDeclaration.HackAssertionException {
        try {
            return new HackedClass<>(Class.forName(str));
        } catch (ClassNotFoundException e) {
            fail(new HackDeclaration.HackAssertionException(e));
            return new HackedClass<>(null);
        }
    }

    public static <T> HackedClass<T> into(ClassLoader classLoader, String str) throws HackDeclaration.HackAssertionException {
        try {
            return new HackedClass<>(classLoader.loadClass(str));
        } catch (Exception e) {
            fail(new HackDeclaration.HackAssertionException(e));
            return new HackedClass<>(null);
        }
    }

    /* access modifiers changed from: private */
    public static void fail(HackDeclaration.HackAssertionException hackAssertionException) throws HackDeclaration.HackAssertionException {
        if (sFailureHandler == null || !sFailureHandler.onAssertionFailure(hackAssertionException)) {
            throw hackAssertionException;
        }
    }

    public static void setAssertionFailureHandler(AssertionFailureHandler assertionFailureHandler) {
        sFailureHandler = assertionFailureHandler;
    }

    private Hack() {
    }
}
