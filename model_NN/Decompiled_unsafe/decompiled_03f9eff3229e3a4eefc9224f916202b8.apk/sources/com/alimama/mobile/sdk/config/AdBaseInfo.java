package com.alimama.mobile.sdk.config;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Iterator;

public class AdBaseInfo implements IAdBaseInfo {
    private HashMap<String, String> baseInfo = new HashMap<>(10);

    private static class Protocol {
        static final String KEY_ID = "pid";
        static final String KEY_SIZE = "size";
        static final String KEY_VIDEO_CAT = "ca";
        static final String KEY_VIDEO_DUR = "dur";
        static final String KEY_VIDEO_SSD = "sd";
        static final String KEY_VIDEO_TITLE = "tt";
        static final String KEY_VIEW_TYPE = "vt";
        static final String PARAM_AND = "&";
        static final String PARAM_EQUAL = "=";
        static final String PARAM_STAR = "*";
        static final String PARAM_VT_INTERSTITIAL = "107";
        static final String PARAM_VT_PATCH = "106";

        private Protocol() {
        }
    }

    private AdBaseInfo(String str) {
        this.baseInfo.put("pid", str);
    }

    public static AdBaseInfo getVideoInstance(String str, int i, int i2, String str2, String str3, String str4, String str5) {
        AdBaseInfo adBaseInfo = new AdBaseInfo(str);
        adBaseInfo.baseInfo.put("size", String.valueOf(i) + "*" + String.valueOf(i2));
        adBaseInfo.baseInfo.put("tt", str2);
        adBaseInfo.baseInfo.put("dur", str3);
        adBaseInfo.baseInfo.put("ca", str4);
        adBaseInfo.baseInfo.put("sd", str5);
        adBaseInfo.baseInfo.put("vt", "106");
        return adBaseInfo;
    }

    public static AdBaseInfo getInsVideoInstance(String str, String str2, String str3, String str4) {
        AdBaseInfo adBaseInfo = new AdBaseInfo(str);
        adBaseInfo.baseInfo.put("tt", str2);
        adBaseInfo.baseInfo.put("dur", str3);
        adBaseInfo.baseInfo.put("ca", str4);
        adBaseInfo.baseInfo.put("vt", "107");
        return adBaseInfo;
    }

    public int getType() {
        return 0;
    }

    public String getId() {
        return this.baseInfo.get("pid");
    }

    public void setId(String str) {
        HashMap<String, String> hashMap = this.baseInfo;
        if (str == null) {
            str = "";
        }
        hashMap.put("pid", str);
    }

    public int[] getViewSize() {
        String str = this.baseInfo.get("size");
        if (str != null) {
            int[] iArr = new int[2];
            String[] split = str.split("*");
            if (split != null && split.length == 2) {
                iArr[0] = Integer.valueOf(split[0]).intValue();
                iArr[1] = Integer.valueOf(split[1]).intValue();
                return iArr;
            }
        }
        return null;
    }

    public void setViewSize(int i, int i2) {
        this.baseInfo.put("size", String.valueOf(i) + "*" + String.valueOf(i2));
    }

    public String getV_title() {
        return this.baseInfo.get("tt");
    }

    public void setV_title(String str) {
        HashMap<String, String> hashMap = this.baseInfo;
        if (str == null) {
            str = "";
        }
        hashMap.put("tt", str);
    }

    public String getV_dur() {
        return this.baseInfo.get("dur");
    }

    public void setV_dur(String str) {
        HashMap<String, String> hashMap = this.baseInfo;
        if (str == null) {
            str = "";
        }
        hashMap.put("dur", str);
    }

    public String getV_cat() {
        return this.baseInfo.get("ca");
    }

    public void setV_cat(String str) {
        HashMap<String, String> hashMap = this.baseInfo;
        if (str == null) {
            str = "";
        }
        hashMap.put("ca", str);
    }

    public String getV_ssd() {
        return this.baseInfo.get("sd");
    }

    public void setV_ssd(String str) {
        HashMap<String, String> hashMap = this.baseInfo;
        if (str == null) {
            str = "";
        }
        hashMap.put("sd", str);
    }

    public String encodeModel() throws UnsupportedEncodingException {
        StringBuilder sb = new StringBuilder();
        Iterator<String> it = this.baseInfo.keySet().iterator();
        if (it == null) {
            return "";
        }
        while (it.hasNext()) {
            String next = it.next();
            sb.append(next);
            sb.append("=");
            sb.append(this.baseInfo.get(next));
            sb.append("&");
        }
        return sb.substring(0, sb.length() - 1);
    }
}
