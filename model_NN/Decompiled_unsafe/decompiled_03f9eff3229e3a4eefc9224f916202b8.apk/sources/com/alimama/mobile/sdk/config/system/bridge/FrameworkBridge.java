package com.alimama.mobile.sdk.config.system.bridge;

import android.util.Log;
import com.alimama.mobile.pluginframework.core.PluginFramework;
import com.alimama.mobile.sdk.config.system.BridgeSystem;
import com.alimama.mobile.sdk.hack.Hack;
import java.util.Map;

@BridgeSystem.GROUP(TYPE = BridgeSystem.GroupType.FRAMEWORK)
public class FrameworkBridge implements BridgeSystem.HackCollection {
    public static Hack.HackedClass<Object> FrameworkLoader;
    public static Hack.HackedMethod FrameworkLoader_getPluginAssetManager;
    public static Hack.HackedMethod FrameworkLoader_getPluginClassLoader;
    public static Hack.HackedMethod FrameworkLoader_getPluginResources;
    public static Hack.HackedMethod FrameworkLoader_getVersion;
    public static Hack.HackedMethod FrameworkLoader_update;

    private static ClassLoader getClassLoader() {
        try {
            return PluginFramework.getPluginClassLoader();
        } catch (Exception e) {
            Log.e("wt", "", e);
            return null;
        }
    }

    public void allClasses() throws Hack.HackDeclaration.HackAssertionException {
        FrameworkLoader = Hack.into(getClassLoader(), "com.alimama.mobile.plugin.framework.FrameworkLoader");
    }

    public void allMethods() throws Hack.HackDeclaration.HackAssertionException {
        FrameworkLoader_getPluginResources = FrameworkLoader.method("getPluginResources", new Class[0]);
        FrameworkLoader_getPluginClassLoader = FrameworkLoader.method("getPluginClassLoader", new Class[0]);
        FrameworkLoader_getPluginAssetManager = FrameworkLoader.method("getPluginAssetManager", new Class[0]);
        FrameworkLoader_update = FrameworkLoader.method("update", Map.class);
        FrameworkLoader_getVersion = FrameworkLoader.method("getVersion", new Class[0]);
    }

    public void allFields() throws Hack.HackDeclaration.HackAssertionException {
    }
}
