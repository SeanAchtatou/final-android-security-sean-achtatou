package com.alimama.mobile.sdk.shell;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.alimama.mobile.sdk.config.system.MMLog;
import com.alimama.mobile.sdk.config.system.bridge.ContainerPluginBridge;

public class AlimmContainerView extends FrameLayout {
    private View container;
    boolean init = false;

    public AlimmContainerView(Context context) {
        super(context);
    }

    public AlimmContainerView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public AlimmContainerView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public boolean init() {
        if (this.init) {
            return this.init;
        }
        try {
            this.container = ContainerPluginBridge.getInstance().invoke_create_containerview(getContext());
            addView(this.container, new ViewGroup.LayoutParams(-1, -1));
            this.init = true;
            return true;
        } catch (Exception e) {
            MMLog.e(e, "", new Object[0]);
            return false;
        }
    }

    public View getContainer() {
        return this.container;
    }
}
