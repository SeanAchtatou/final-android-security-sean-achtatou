package com.alimama.mobile.sdk.config.system;

import android.os.Build;
import android.util.Log;
import com.alimama.mobile.sdk.config.system.bridge.AndroidBridgeHacks;
import com.alimama.mobile.sdk.config.system.bridge.CMPluginBridge;
import com.alimama.mobile.sdk.config.system.bridge.FrameworkBridge;
import com.alimama.mobile.sdk.config.system.bridge.GodModeHacks;
import com.alimama.mobile.sdk.hack.AssertionArrayException;
import com.alimama.mobile.sdk.hack.Hack;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;

public class BridgeSystem extends Hack.HackDeclaration implements Hack.AssertionFailureHandler {
    private static final Vector<HackCollection> collections = new Vector<>();
    public static boolean sIsIgnoreFailure = false;
    private static final Map<GroupType, Boolean> sIsTypeChecked = new HashMap();
    private AssertionArrayException mExceptionArray = null;

    @Target({ElementType.TYPE})
    @Retention(RetentionPolicy.RUNTIME)
    public @interface GROUP {
        GroupType TYPE();
    }

    public enum GroupType {
        OS,
        APP,
        FRAMEWORK,
        COMMON
    }

    public interface HackCollection {
        void allClasses() throws Hack.HackDeclaration.HackAssertionException;

        void allFields() throws Hack.HackDeclaration.HackAssertionException;

        void allMethods() throws Hack.HackDeclaration.HackAssertionException;
    }

    static {
        collections.add(new FrameworkBridge());
        collections.add(new CMPluginBridge());
        collections.add(new AndroidBridgeHacks());
        collections.add(new GodModeHacks());
    }

    /* JADX INFO: finally extract failed */
    public static boolean defineAndVerify(GroupType groupType) throws AssertionArrayException {
        if (sIsTypeChecked.containsKey(groupType)) {
            return sIsTypeChecked.get(groupType).booleanValue();
        }
        try {
            BridgeSystem bridgeSystem = new BridgeSystem();
            Hack.setAssertionFailureHandler(bridgeSystem);
            if (Build.VERSION.SDK_INT == 11) {
                bridgeSystem.onAssertionFailure(new Hack.HackDeclaration.HackAssertionException("Hack Assertion Failed: Android OS Version 11"));
            }
            Iterator<HackCollection> it = collections.iterator();
            while (it.hasNext()) {
                HackCollection next = it.next();
                if (groupType == ((GROUP) next.getClass().getAnnotation(GROUP.class)).TYPE()) {
                    next.allClasses();
                    next.allFields();
                    next.allMethods();
                }
            }
            if (bridgeSystem.mExceptionArray != null) {
                throw bridgeSystem.mExceptionArray;
            }
            Hack.setAssertionFailureHandler(null);
            sIsTypeChecked.put(groupType, true);
            return true;
        } catch (Hack.HackDeclaration.HackAssertionException e) {
            Log.e("Hack", "HackAssertionException", e);
            Hack.setAssertionFailureHandler(null);
            sIsTypeChecked.put(groupType, false);
            return false;
        } catch (Throwable th) {
            Hack.setAssertionFailureHandler(null);
            sIsTypeChecked.put(groupType, false);
            throw th;
        }
    }

    public boolean isTypeAvailable(GroupType groupType) {
        if (sIsTypeChecked.containsKey(groupType)) {
            return sIsTypeChecked.get(groupType).booleanValue();
        }
        return false;
    }

    public boolean isTypeChecked(GroupType groupType) {
        return sIsTypeChecked.containsKey(groupType);
    }

    public boolean onAssertionFailure(Hack.HackDeclaration.HackAssertionException hackAssertionException) {
        if (!sIsIgnoreFailure) {
            if (this.mExceptionArray == null) {
                this.mExceptionArray = new AssertionArrayException("BridgeSystem hack failed");
            }
            this.mExceptionArray.addException(hackAssertionException);
        }
        return true;
    }
}
