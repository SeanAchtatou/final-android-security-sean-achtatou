package com.umeng.fb;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.umeng.fb.c.c;
import com.umeng.fb.c.d;
import com.umeng.fb.c.e;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ContactActivity extends Activity {

    /* renamed from: a  reason: collision with root package name */
    private ImageView f2159a;
    private ImageView b;
    /* access modifiers changed from: private */
    public EditText c;
    /* access modifiers changed from: private */
    public k d;
    private TextView e;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(d.a(this));
        this.d = new k(this);
        this.f2159a = (ImageView) findViewById(c.d(this));
        this.b = (ImageView) findViewById(c.g(this));
        this.c = (EditText) findViewById(c.h(this));
        this.e = (TextView) findViewById(c.i(this));
        try {
            String str = this.d.c().b().get("plain");
            this.c.setText(str);
            long d2 = this.d.d();
            if (d2 > 0) {
                Date date = new Date(d2);
                this.e.setText(getResources().getString(e.a(this)) + SimpleDateFormat.getDateTimeInstance().format(date));
                this.e.setVisibility(0);
            } else {
                this.e.setVisibility(8);
            }
            if (TextUtils.isEmpty(str)) {
                this.c.requestFocus();
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService("input_method");
                if (inputMethodManager != null) {
                    inputMethodManager.toggleSoftInput(2, 0);
                }
            }
        } catch (NullPointerException e2) {
            e2.printStackTrace();
        }
        this.f2159a.setOnClickListener(new a(this));
        this.b.setOnClickListener(new b(this));
    }

    /* access modifiers changed from: package-private */
    @SuppressLint({"NewApi"})
    public void a() {
        finish();
        if (Build.VERSION.SDK_INT > 4) {
            new c(this).a(this);
        }
    }
}
