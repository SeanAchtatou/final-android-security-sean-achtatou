package com.umeng.fb.c;

import android.content.Context;
import com.umeng.fb.d.d;

/* compiled from: AnimMapper */
public class a {
    public static int a(Context context) {
        return d.a(context).a("umeng_fb_slide_in_from_left");
    }

    public static int b(Context context) {
        return d.a(context).a("umeng_fb_slide_in_from_right");
    }

    public static int c(Context context) {
        return d.a(context).a("umeng_fb_slide_out_from_left");
    }

    public static int d(Context context) {
        return d.a(context).a("umeng_fb_slide_out_from_right");
    }
}
