package com.umeng.fb;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.view.View;

/* compiled from: ConversationActivity */
class d implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ConversationActivity f2180a;

    d(ConversationActivity conversationActivity) {
        this.f2180a = conversationActivity;
    }

    @SuppressLint({"NewApi"})
    public void onClick(View view) {
        Intent intent = new Intent();
        intent.setClass(this.f2180a, ContactActivity.class);
        this.f2180a.startActivity(intent);
        if (Build.VERSION.SDK_INT > 4) {
            new e(this).a(this.f2180a);
        }
    }
}
