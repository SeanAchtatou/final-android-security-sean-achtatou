package com.umeng.fb;

import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

/* compiled from: ConversationActivity */
class g implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ConversationActivity f2187a;

    g(ConversationActivity conversationActivity) {
        this.f2187a = conversationActivity;
    }

    public void onClick(View view) {
        String trim = this.f2187a.d.getEditableText().toString().trim();
        if (!TextUtils.isEmpty(trim)) {
            this.f2187a.d.getEditableText().clear();
            this.f2187a.g.a(trim);
            this.f2187a.a();
            InputMethodManager inputMethodManager = (InputMethodManager) this.f2187a.getSystemService("input_method");
            if (inputMethodManager != null) {
                inputMethodManager.hideSoftInputFromWindow(this.f2187a.d.getWindowToken(), 0);
            }
        }
    }
}
