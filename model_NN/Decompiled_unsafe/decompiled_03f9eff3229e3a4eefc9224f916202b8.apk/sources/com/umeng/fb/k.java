package com.umeng.fb;

import android.content.Context;
import android.content.Intent;
import com.umeng.fb.a.a;
import com.umeng.fb.a.e;
import com.umeng.fb.a.f;
import com.umeng.fb.d.c;
import java.util.List;

/* compiled from: FeedbackAgent */
public class k {

    /* renamed from: a  reason: collision with root package name */
    private static final String f2191a = k.class.getName();
    private Context b;
    private e c = e.a(this.b);

    public k(Context context) {
        this.b = context;
    }

    public List<String> a() {
        return this.c.d();
    }

    public a a(String str) {
        return this.c.a(str);
    }

    public a b() {
        List<String> a2 = a();
        if (a2 == null || a2.size() < 1) {
            c.c(f2191a, "getDefaultConversation: No conversation saved locally. Create a new one.");
            return new a(this.b);
        }
        c.c(f2191a, "getDefaultConversation: There are " + a2.size() + " saved locally, use the first one by default.");
        return a(a2.get(0));
    }

    public f c() {
        return this.c.a();
    }

    public void a(f fVar) {
        this.c.a(fVar);
    }

    public long d() {
        return this.c.b();
    }

    public void e() {
        try {
            Intent intent = new Intent();
            intent.setClass(this.b, ConversationActivity.class);
            this.b.startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
