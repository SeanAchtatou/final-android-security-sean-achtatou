package com.umeng.fb;

import android.view.MotionEvent;
import android.view.View;

/* compiled from: ConversationActivity */
class h implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ConversationActivity f2188a;

    h(ConversationActivity conversationActivity) {
        this.f2188a = conversationActivity;
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (this.f2188a.i.getAdapter().getCount() >= 2) {
            switch (motionEvent.getAction()) {
                case 0:
                    int unused = this.f2188a.j = (int) motionEvent.getY();
                    break;
                case 1:
                    if (this.f2188a.i.getFirstVisiblePosition() == 0) {
                        if (this.f2188a.f2160a.getBottom() < this.f2188a.b + 20 && this.f2188a.f2160a.getTop() <= 0) {
                            this.f2188a.i.setSelection(1);
                            this.f2188a.f2160a.setVisibility(8);
                            this.f2188a.f2160a.setPadding(this.f2188a.f2160a.getPaddingLeft(), -this.f2188a.b, this.f2188a.f2160a.getPaddingRight(), this.f2188a.f2160a.getPaddingBottom());
                            break;
                        } else {
                            this.f2188a.f2160a.setVisibility(0);
                            this.f2188a.f2160a.setPadding(this.f2188a.f2160a.getPaddingLeft(), this.f2188a.c, this.f2188a.f2160a.getPaddingRight(), this.f2188a.f2160a.getPaddingBottom());
                            break;
                        }
                    }
                    break;
                case 2:
                    a(motionEvent);
                    break;
            }
        }
        return false;
    }

    private void a(MotionEvent motionEvent) {
        int historySize = motionEvent.getHistorySize();
        for (int i = 0; i < historySize; i++) {
            if (this.f2188a.i.getFirstVisiblePosition() == 0) {
                int historicalY = (int) (((double) ((((int) motionEvent.getHistoricalY(i)) - this.f2188a.j) - this.f2188a.b)) / 1.7d);
                this.f2188a.f2160a.setVisibility(0);
                this.f2188a.f2160a.setPadding(this.f2188a.f2160a.getPaddingLeft(), historicalY, this.f2188a.f2160a.getPaddingRight(), this.f2188a.f2160a.getPaddingBottom());
            }
        }
    }
}
