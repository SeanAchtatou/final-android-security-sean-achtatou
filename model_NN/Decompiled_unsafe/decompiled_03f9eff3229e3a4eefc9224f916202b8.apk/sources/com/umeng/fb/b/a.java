package com.umeng.fb.b;

import android.content.Context;
import android.text.TextUtils;
import cn.banshenggua.aichang.utils.Constants;
import com.umeng.fb.a.d;
import com.umeng.fb.a.e;
import com.umeng.fb.a.g;
import com.umeng.fb.a.h;
import com.umeng.fb.d.b;
import com.umeng.fb.d.c;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: FbClient */
public class a {

    /* renamed from: a  reason: collision with root package name */
    private static final String f2176a = a.class.getName();
    private Context b;

    public a(Context context) {
        this.b = context;
    }

    public c a(b bVar) {
        HttpUriRequest httpGet;
        int nextInt = new Random().nextInt(Constants.CLEARIMGED);
        String str = bVar.c;
        String str2 = bVar.f2177a;
        JSONObject jSONObject = bVar.b;
        if (!(bVar instanceof b)) {
            c.b(f2176a, "request type error, request must be type of FbReportRequest");
            return null;
        } else if (str.length() <= 1) {
            c.b(f2176a, nextInt + ":\tInvalid baseUrl.");
            return null;
        } else {
            if (str2 != null) {
                c.a(f2176a, nextInt + ": post: " + str + " " + jSONObject.toString());
                ArrayList arrayList = new ArrayList(1);
                arrayList.add(new BasicNameValuePair(str2, jSONObject.toString()));
                try {
                    UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(arrayList, "UTF-8");
                    HttpPost httpPost = new HttpPost(str);
                    httpPost.addHeader(urlEncodedFormEntity.getContentType());
                    httpPost.setEntity(urlEncodedFormEntity);
                    httpGet = httpPost;
                } catch (UnsupportedEncodingException e) {
                    throw new AssertionError(e);
                }
            } else {
                c.a(f2176a, nextInt + ":\tget: " + str);
                httpGet = new HttpGet(str);
            }
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
            HttpParams params = defaultHttpClient.getParams();
            HttpConnectionParams.setConnectionTimeout(params, 30000);
            HttpConnectionParams.setSoTimeout(params, 30000);
            ConnManagerParams.setTimeout(params, (long) StatisticConfig.MIN_UPLOAD_INTERVAL);
            try {
                HttpResponse execute = defaultHttpClient.execute(httpGet);
                if (execute.getStatusLine().getStatusCode() != 200) {
                    return null;
                }
                String entityUtils = EntityUtils.toString(execute.getEntity());
                c.a(f2176a, "res :" + entityUtils);
                return new c(new JSONObject(entityUtils));
            } catch (ClientProtocolException e2) {
                c.c(f2176a, nextInt + ":\tClientProtocolException,Failed to send message." + str, e2);
                return null;
            } catch (Exception e3) {
                c.c(f2176a, nextInt + ":\tIOException,Failed to send message." + str, e3);
                return null;
            }
        }
    }

    public List<com.umeng.fb.a.c> a(List<String> list, String str, String str2) {
        if (list == null || list.size() == 0 || TextUtils.isEmpty(str2)) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        for (String next : list) {
            if (!TextUtils.isEmpty(next)) {
                sb.append(next);
                sb.append(",");
            }
        }
        if (sb.length() > 1) {
            sb.replace(sb.length() - 1, sb.length(), "");
        }
        StringBuilder sb2 = new StringBuilder("http://feedback.umeng.com/feedback/reply");
        sb2.append("?appkey=" + str2);
        sb2.append("&feedback_id=" + ((Object) sb));
        if (!TextUtils.isEmpty(str)) {
            sb2.append("&startkey=" + str);
        }
        c.c(f2176a, "getDevReply url: " + ((Object) sb2));
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
        HttpParams params = defaultHttpClient.getParams();
        HttpConnectionParams.setConnectionTimeout(params, 30000);
        HttpConnectionParams.setSoTimeout(params, 30000);
        ConnManagerParams.setTimeout(params, (long) StatisticConfig.MIN_UPLOAD_INTERVAL);
        try {
            HttpResponse execute = defaultHttpClient.execute(new HttpGet(sb2.toString()));
            if (execute.getStatusLine().getStatusCode() == 200) {
                String entityUtils = EntityUtils.toString(execute.getEntity());
                c.c(f2176a, "getDevReply resp: " + entityUtils);
                JSONArray jSONArray = new JSONArray(entityUtils);
                ArrayList arrayList = new ArrayList();
                for (int i = 0; i < jSONArray.length(); i++) {
                    try {
                        JSONArray jSONArray2 = jSONArray.getJSONArray(i);
                        for (int i2 = 0; i2 < jSONArray2.length(); i2++) {
                            try {
                                arrayList.add(new com.umeng.fb.a.c(jSONArray2.getJSONObject(i2)));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    } catch (JSONException e2) {
                        e2.printStackTrace();
                    }
                }
                return arrayList;
            }
        } catch (Exception e3) {
            e3.printStackTrace();
        }
        return null;
    }

    public boolean a(d dVar) throws IllegalArgumentException {
        if (dVar == null) {
            return true;
        }
        if (dVar instanceof g) {
            return a((g) dVar);
        }
        if (dVar instanceof h) {
            return a((h) dVar);
        }
        throw new IllegalArgumentException("Illegal argument: " + dVar.getClass().getName() + ". reply must be " + g.class.getName() + " or " + h.class.getName() + ".");
    }

    private boolean a(g gVar) {
        try {
            JSONObject a2 = gVar.a();
            a(a2);
            b(a2);
            c a3 = a(new b("reply", a2, "http://feedback.umeng.com/feedback/reply"));
            if (a3 == null || !"ok".equalsIgnoreCase(a3.a().get("state").toString())) {
                return false;
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean a(h hVar) {
        try {
            JSONObject a2 = hVar.a();
            a(a2);
            b(a2);
            c a3 = a(new b("feedback", a2, "http://feedback.umeng.com/feedback/feedbacks"));
            if (a3 == null || !"ok".equalsIgnoreCase(a3.a().get("state").toString())) {
                return false;
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void a(JSONObject jSONObject) {
        try {
            JSONObject b2 = b.b(this.b);
            c.c(f2176a, "addRequestHeader: " + b2.toString());
            Iterator<String> keys = b2.keys();
            while (keys.hasNext()) {
                String next = keys.next();
                jSONObject.put(next, b2.get(next));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void b(JSONObject jSONObject) {
        try {
            long c = e.a(this.b).c();
            long b2 = e.a(this.b).b();
            c.c(f2176a, "addUserInfoIfNotSynced: last_sync_at=" + c + " last_update_at=" + b2);
            if (c < b2) {
                jSONObject.put("userinfo", e.a(this.b).a().a());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
