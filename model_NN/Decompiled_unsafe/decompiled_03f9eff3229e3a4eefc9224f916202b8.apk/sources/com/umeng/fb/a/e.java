package com.umeng.fb.a;

import android.content.Context;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: Store */
public class e {

    /* renamed from: a  reason: collision with root package name */
    private static final String f2172a = e.class.getName();
    private static e b;
    private Context c;

    private e(Context context) {
        this.c = context.getApplicationContext();
    }

    public static e a(Context context) {
        if (b == null) {
            b = new e(context);
        }
        return b;
    }

    public void a(a aVar) {
        this.c.getSharedPreferences("umeng_feedback_conversations", 0).edit().putString(aVar.c(), aVar.b().toString()).commit();
    }

    public void a(f fVar) {
        this.c.getSharedPreferences("umeng_feedback_user_info", 0).edit().putString("user", fVar.a().toString()).putLong("last_update_at", System.currentTimeMillis()).commit();
    }

    public f a() {
        String string = this.c.getSharedPreferences("umeng_feedback_user_info", 0).getString("user", "");
        if ("".equals(string)) {
            return null;
        }
        try {
            return new f(new JSONObject(string));
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public long b() {
        return this.c.getSharedPreferences("umeng_feedback_user_info", 0).getLong("last_update_at", 0);
    }

    public long c() {
        return this.c.getSharedPreferences("umeng_feedback_user_info", 0).getLong("last_sync_at", 0);
    }

    public a a(String str) {
        try {
            return new a(str, new JSONArray(this.c.getSharedPreferences("umeng_feedback_conversations", 0).getString(str, "")), this.c);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<String> d() {
        Map<String, ?> all = this.c.getSharedPreferences("umeng_feedback_conversations", 0).getAll();
        ArrayList arrayList = new ArrayList();
        for (String add : all.keySet()) {
            arrayList.add(add);
        }
        return arrayList;
    }
}
