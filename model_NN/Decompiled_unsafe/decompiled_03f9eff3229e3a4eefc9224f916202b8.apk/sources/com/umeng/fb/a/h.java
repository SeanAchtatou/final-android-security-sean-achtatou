package com.umeng.fb.a;

import com.umeng.fb.a.d;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: UserTitleReply */
public class h extends d {

    /* renamed from: a  reason: collision with root package name */
    protected String f2174a;

    public h(String str, String str2, String str3, String str4) {
        super(str, str2, str3, str4, d.b.NEW_FEEDBACK);
        this.f2174a = str;
    }

    h(JSONObject jSONObject) throws JSONException {
        super(jSONObject);
        if (this.g != d.b.NEW_FEEDBACK) {
            throw new JSONException(h.class.getName() + ".type must be " + d.b.NEW_FEEDBACK);
        }
        this.f2174a = jSONObject.optString("thread");
    }

    public JSONObject a() {
        JSONObject a2 = super.a();
        try {
            a2.put("thread", this.f2174a);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return a2;
    }
}
