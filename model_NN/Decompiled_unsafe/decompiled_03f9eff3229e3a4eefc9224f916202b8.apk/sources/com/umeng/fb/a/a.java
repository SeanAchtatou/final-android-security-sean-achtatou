package com.umeng.fb.a;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import com.umeng.fb.a.d;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: Conversation */
public class a {
    private static final String b = a.class.getName();
    private static ExecutorService d = Executors.newSingleThreadExecutor();

    /* renamed from: a  reason: collision with root package name */
    List<d> f2164a = new ArrayList();
    /* access modifiers changed from: private */
    public Context c;
    /* access modifiers changed from: private */
    public String e;
    private String f;
    /* access modifiers changed from: private */
    public String g;
    /* access modifiers changed from: private */
    public Map<String, d> h;

    /* compiled from: Conversation */
    public interface c {
        void a(List<d> list);

        void b(List<c> list);
    }

    public synchronized List<d> a() {
        this.f2164a.clear();
        this.f2164a.addAll(this.h.values());
        Collections.sort(this.f2164a);
        return this.f2164a;
    }

    public a(Context context) {
        this.c = context;
        this.g = com.umeng.fb.d.a.i(this.c);
        this.e = com.umeng.fb.d.b.a(this.c);
        this.f = com.umeng.fb.d.a.d(this.c);
        this.h = new ConcurrentHashMap();
    }

    a(String str, JSONArray jSONArray, Context context) throws JSONException {
        this.c = context;
        this.g = com.umeng.fb.d.a.i(this.c);
        this.e = str;
        this.f = com.umeng.fb.d.a.d(this.c);
        this.h = new HashMap();
        if (jSONArray != null && jSONArray.length() >= 1) {
            for (int i = 0; i < jSONArray.length(); i++) {
                JSONObject jSONObject = jSONArray.getJSONObject(i);
                String string = jSONObject.getString("type");
                d dVar = null;
                if (d.b.NEW_FEEDBACK.toString().equals(string)) {
                    dVar = new h(jSONObject);
                } else if (d.b.USER_REPLY.toString().equals(string)) {
                    dVar = new g(jSONObject);
                } else if (d.b.DEV_REPLY.toString().equals(string)) {
                    dVar = new c(jSONObject);
                }
                if (dVar == null) {
                    throw new JSONException("Failed to create Conversation using given JSONArray: " + jSONArray + " at element " + i + ": " + jSONObject);
                }
                if (!this.h.containsKey(dVar.c)) {
                    this.h.put(dVar.c, dVar);
                }
            }
            d();
        }
    }

    public void a(String str) {
        d gVar;
        if (this.h.size() < 1) {
            gVar = new h(str, this.g, this.f, this.e);
        } else {
            gVar = new g(str, this.g, this.f, this.e);
        }
        if (!this.h.containsKey(gVar.c)) {
            this.h.put(gVar.c, gVar);
        }
        d();
    }

    /* access modifiers changed from: private */
    public void d() {
        e.a(this.c).a(this);
    }

    /* access modifiers changed from: package-private */
    public JSONArray b() {
        JSONArray jSONArray = new JSONArray();
        for (Map.Entry<String, d> value : this.h.entrySet()) {
            jSONArray.put(((d) value.getValue()).a());
        }
        return jSONArray;
    }

    public String c() {
        return this.e;
    }

    public void a(c cVar) {
        d.execute(new b(this, new b(cVar)));
    }

    /* compiled from: Conversation */
    class b extends Handler {

        /* renamed from: a  reason: collision with root package name */
        c f2166a;

        public b(c cVar) {
            this.f2166a = cVar;
        }

        public void handleMessage(Message message) {
            boolean z = true;
            if (message.what == 2) {
                d dVar = (d) message.obj;
                if (message.arg1 != 1) {
                    z = false;
                }
                if (z) {
                    dVar.i = d.a.SENT;
                }
            } else if (message.what == 1) {
                C0047a aVar = (C0047a) message.obj;
                List<c> list = aVar.b;
                List<d> list2 = aVar.f2165a;
                if (list != null) {
                    Iterator<c> it = list.iterator();
                    while (it.hasNext()) {
                        d next = it.next();
                        if (a.this.h.containsKey(next.c)) {
                            it.remove();
                        } else {
                            a.this.h.put(next.c, next);
                        }
                    }
                }
                a.this.d();
                if (this.f2166a != null) {
                    this.f2166a.b(list);
                    this.f2166a.a(list2);
                }
            }
        }
    }

    /* renamed from: com.umeng.fb.a.a$a  reason: collision with other inner class name */
    /* compiled from: Conversation */
    static class C0047a {

        /* renamed from: a  reason: collision with root package name */
        List<d> f2165a;
        List<c> b;

        C0047a() {
        }
    }
}
