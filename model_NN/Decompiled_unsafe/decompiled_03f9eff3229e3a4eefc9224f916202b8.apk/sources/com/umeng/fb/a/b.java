package com.umeng.fb.a;

import android.os.Handler;
import android.os.Message;
import com.umeng.fb.a.a;
import com.umeng.fb.a.d;
import com.umeng.fb.b.a;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/* compiled from: Conversation */
class b implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Handler f2167a;
    final /* synthetic */ a b;

    b(a aVar, Handler handler) {
        this.b = aVar;
        this.f2167a = handler;
    }

    public void run() {
        String str;
        Date date;
        ArrayList<d> arrayList = new ArrayList<>();
        ArrayList arrayList2 = new ArrayList();
        Date date2 = null;
        String str2 = "";
        for (Map.Entry value : this.b.h.entrySet()) {
            d dVar = (d) value.getValue();
            if ((dVar instanceof g) || (dVar instanceof h)) {
                if (dVar.i == d.a.NOT_SENT) {
                    arrayList.add(dVar);
                    str = str2;
                    date = date2;
                }
                str = str2;
                date = date2;
            } else {
                if ((dVar instanceof c) && (date2 == null || date2.compareTo(dVar.c()) < 0)) {
                    date = dVar.c();
                    str = dVar.c;
                }
                str = str2;
                date = date2;
            }
            date2 = date;
            str2 = str;
        }
        arrayList2.add(this.b.e);
        for (d dVar2 : arrayList) {
            boolean a2 = new a(this.b.c).a(dVar2);
            if (a2) {
                Message obtain = Message.obtain();
                obtain.what = 2;
                obtain.obj = dVar2;
                obtain.arg1 = a2 ? 1 : 0;
                this.f2167a.sendMessage(obtain);
            }
        }
        List<c> a3 = new a(this.b.c).a(arrayList2, str2, this.b.g);
        Message obtain2 = Message.obtain();
        obtain2.what = 1;
        a.C0047a aVar = new a.C0047a();
        aVar.b = a3;
        aVar.f2165a = arrayList;
        obtain2.obj = aVar;
        this.f2167a.sendMessage(obtain2);
    }
}
