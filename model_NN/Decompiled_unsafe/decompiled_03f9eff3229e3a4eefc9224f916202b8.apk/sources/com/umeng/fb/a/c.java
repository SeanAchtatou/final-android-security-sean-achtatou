package com.umeng.fb.a;

import com.umeng.fb.a.d;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: DevReply */
public class c extends d {

    /* renamed from: a  reason: collision with root package name */
    protected String f2168a;

    public c(JSONObject jSONObject) throws JSONException {
        super(jSONObject);
        if (this.g != d.b.DEV_REPLY) {
            throw new JSONException(c.class.getName() + ".type must be " + d.b.DEV_REPLY);
        }
        this.f2168a = jSONObject.optString("user_name", "");
    }

    public JSONObject a() {
        JSONObject a2 = super.a();
        if (a2 != null) {
            try {
                a2.put("user_name", this.f2168a);
                return a2;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
