package com.umeng.fb.a;

import com.umeng.fb.d.c;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: Reply */
public abstract class d implements Comparable<d> {

    /* renamed from: a  reason: collision with root package name */
    private static final String f2169a = d.class.getName();
    protected String b;
    protected String c;
    protected String d;
    protected String e;
    protected String f;
    protected b g;
    protected Date h;
    protected a i;

    d(String str, String str2, String str3, String str4, b bVar) {
        this.b = str;
        this.c = com.umeng.fb.d.b.a();
        this.d = str2;
        this.e = str3;
        this.f = str4;
        this.g = bVar;
        this.h = new Date();
        this.i = a.NOT_SENT;
    }

    d(JSONObject jSONObject) throws JSONException {
        this.b = jSONObject.optString("content", "");
        this.c = jSONObject.optString("reply_id", "");
        this.d = jSONObject.optString(LogBuilder.KEY_APPKEY, "");
        this.e = jSONObject.optString("user_id", "");
        this.f = jSONObject.optString("feedback_id", "");
        try {
            this.g = b.a(jSONObject.getString("type"));
            try {
                this.h = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(jSONObject.getString("datetime"));
            } catch (ParseException e2) {
                try {
                    this.h = new SimpleDateFormat().parse(jSONObject.getString("datetime"));
                } catch (ParseException e3) {
                    e3.printStackTrace();
                    c.b(f2169a, "Reply(JSONObject json): error parsing datetime from json " + jSONObject.optString("datetime", "") + ", using current Date instead.");
                    this.h = new Date();
                }
            }
            this.i = a.a(jSONObject.optString("status", a.NOT_SENT.toString()));
        } catch (Exception e4) {
            throw new JSONException(e4.getMessage());
        }
    }

    public JSONObject a() {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("content", this.b);
            jSONObject.put("reply_id", this.c);
            jSONObject.put(LogBuilder.KEY_APPKEY, this.d);
            jSONObject.put("user_id", this.e);
            jSONObject.put("feedback_id", this.f);
            jSONObject.put("type", this.g);
            jSONObject.put("datetime", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US).format(this.h));
            jSONObject.put("status", this.i.toString());
            return jSONObject;
        } catch (JSONException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    /* renamed from: a */
    public int compareTo(d dVar) {
        return this.h.compareTo(dVar.h);
    }

    public String b() {
        return this.b;
    }

    public Date c() {
        return this.h;
    }

    /* compiled from: Reply */
    public enum b {
        NEW_FEEDBACK("new_feedback"),
        DEV_REPLY("dev_reply"),
        USER_REPLY("user_reply");
        
        private final String d;

        private b(String str) {
            this.d = str;
        }

        public String toString() {
            return this.d;
        }

        public static b a(String str) {
            if (NEW_FEEDBACK.toString().equals(str)) {
                return NEW_FEEDBACK;
            }
            if (DEV_REPLY.toString().equals(str)) {
                return DEV_REPLY;
            }
            if (USER_REPLY.toString().equals(str)) {
                return USER_REPLY;
            }
            throw new RuntimeException(str + "Cannot convert " + str + " to enum " + b.class.getName());
        }
    }

    /* compiled from: Reply */
    public enum a {
        SENDING("sending"),
        NOT_SENT("not_sent"),
        SENT("sent");
        
        private final String d;

        private a(String str) {
            this.d = str;
        }

        public String toString() {
            return this.d;
        }

        public static a a(String str) {
            if (SENDING.toString().equals(str)) {
                return SENDING;
            }
            if (NOT_SENT.toString().equals(str)) {
                return NOT_SENT;
            }
            if (SENT.toString().equals(str)) {
                return SENT;
            }
            throw new RuntimeException(str + "Cannot convert " + str + " to enum " + a.class.getName());
        }
    }
}
