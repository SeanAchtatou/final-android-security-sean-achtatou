package com.umeng.fb.a;

import com.umeng.fb.a.d;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: UserReply */
public class g extends d {
    public g(String str, String str2, String str3, String str4) {
        super(str, str2, str3, str4, d.b.USER_REPLY);
    }

    g(JSONObject jSONObject) throws JSONException {
        super(jSONObject);
        if (this.g != d.b.USER_REPLY) {
            throw new JSONException(g.class.getName() + ".type must be " + d.b.USER_REPLY);
        }
    }

    public JSONObject a() {
        return super.a();
    }
}
