package com.umeng.fb.a;

import android.text.TextUtils;
import com.umeng.fb.d.c;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: UserInfo */
public class f {
    private static final String e = f.class.getName();

    /* renamed from: a  reason: collision with root package name */
    int f2173a;
    String b;
    Map<String, String> c;
    Map<String, String> d;

    public f() {
        this.f2173a = -1;
        this.b = "";
        this.c = new HashMap();
        this.d = new HashMap();
    }

    f(JSONObject jSONObject) throws JSONException {
        this.f2173a = -1;
        this.b = "";
        this.f2173a = jSONObject.optInt("age_group", -1);
        this.b = jSONObject.optString("gender", "");
        this.c = new HashMap();
        this.d = new HashMap();
        JSONObject optJSONObject = jSONObject.optJSONObject("contact");
        if (optJSONObject != null) {
            Iterator<String> keys = optJSONObject.keys();
            while (keys.hasNext()) {
                String next = keys.next();
                this.c.put(next, optJSONObject.getString(next));
            }
        }
        JSONObject optJSONObject2 = jSONObject.optJSONObject("remark");
        c.c(e, "" + optJSONObject2);
        if (optJSONObject2 != null) {
            Iterator<String> keys2 = optJSONObject2.keys();
            while (keys2.hasNext()) {
                String next2 = keys2.next();
                this.d.put(next2, optJSONObject2.getString(next2));
            }
        }
    }

    public JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            if (this.f2173a > -1) {
                jSONObject.put("age_group", this.f2173a);
            }
            if (!TextUtils.isEmpty(this.b)) {
                jSONObject.put("gender", this.b);
            }
            if (this.c != null && this.c.size() > 0) {
                JSONObject jSONObject2 = new JSONObject();
                for (Map.Entry next : this.c.entrySet()) {
                    jSONObject2.put((String) next.getKey(), next.getValue());
                }
                jSONObject.put("contact", jSONObject2);
            }
            if (this.d != null && this.d.size() > 0) {
                JSONObject jSONObject3 = new JSONObject();
                for (Map.Entry next2 : this.d.entrySet()) {
                    jSONObject3.put((String) next2.getKey(), next2.getValue());
                }
                jSONObject.put("remark", jSONObject3);
            }
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
        return jSONObject;
    }

    public Map<String, String> b() {
        return this.c;
    }

    public void a(Map<String, String> map) {
        this.c = map;
    }
}
