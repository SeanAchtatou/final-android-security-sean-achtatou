package com.umeng.fb;

import android.view.View;
import com.umeng.fb.a.f;
import java.util.HashMap;
import java.util.Map;

/* compiled from: ContactActivity */
class b implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ContactActivity f2175a;

    b(ContactActivity contactActivity) {
        this.f2175a = contactActivity;
    }

    public void onClick(View view) {
        f fVar;
        try {
            f c = this.f2175a.d.c();
            if (c == null) {
                fVar = new f();
            } else {
                fVar = c;
            }
            Map b = fVar.b();
            if (b == null) {
                b = new HashMap();
            }
            b.put("plain", this.f2175a.c.getEditableText().toString());
            fVar.a(b);
            this.f2175a.d.a(fVar);
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.f2175a.a();
    }
}
