package com.umeng.fb;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.umeng.fb.c.b;
import com.umeng.fb.c.c;
import com.umeng.fb.c.d;
import java.text.SimpleDateFormat;
import java.util.List;
import org.apache.mina.proxy.handlers.http.ntlm.NTLMConstants;

public class ConversationActivity extends Activity {
    private static final String e = ConversationActivity.class.getName();

    /* renamed from: a  reason: collision with root package name */
    RelativeLayout f2160a;
    int b;
    int c;
    EditText d;
    private k f;
    /* access modifiers changed from: private */
    public com.umeng.fb.a.a g;
    /* access modifiers changed from: private */
    public a h;
    /* access modifiers changed from: private */
    public ListView i;
    /* access modifiers changed from: private */
    public int j;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(d.b(this));
        try {
            this.f = new k(this);
            this.g = this.f.b();
            this.i = (ListView) findViewById(c.a(this));
            b();
            this.h = new a(this);
            this.i.setAdapter((ListAdapter) this.h);
            a();
            View findViewById = findViewById(c.c(this));
            findViewById.setOnClickListener(new d(this));
            if (this.f.d() > 0) {
                findViewById.setVisibility(8);
            }
            findViewById(c.d(this)).setOnClickListener(new f(this));
            this.d = (EditText) findViewById(c.b(this));
            findViewById(c.e(this)).setOnClickListener(new g(this));
        } catch (Exception e2) {
            e2.printStackTrace();
            finish();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.widget.ListView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void b() {
        this.f2160a = (RelativeLayout) ((LayoutInflater) getSystemService("layout_inflater")).inflate(d.d(this), (ViewGroup) this.i, false);
        this.i.addHeaderView(this.f2160a);
        a(this.f2160a);
        this.b = this.f2160a.getMeasuredHeight();
        this.c = this.f2160a.getPaddingTop();
        this.f2160a.setPadding(this.f2160a.getPaddingLeft(), -this.b, this.f2160a.getPaddingRight(), this.f2160a.getPaddingBottom());
        this.f2160a.setVisibility(8);
        this.i.setOnTouchListener(new h(this));
        this.i.setOnScrollListener(new i(this));
    }

    private void a(View view) {
        int makeMeasureSpec;
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (layoutParams == null) {
            layoutParams = new ViewGroup.LayoutParams(-1, -2);
        }
        int childMeasureSpec = ViewGroup.getChildMeasureSpec(0, 0, layoutParams.width);
        int i2 = layoutParams.height;
        if (i2 > 0) {
            makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(i2, NTLMConstants.FLAG_NEGOTIATE_KEY_EXCHANGE);
        } else {
            makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
        }
        view.measure(childMeasureSpec, makeMeasureSpec);
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.g.a(new j(this));
    }

    class a extends BaseAdapter {

        /* renamed from: a  reason: collision with root package name */
        Context f2161a;
        LayoutInflater b = LayoutInflater.from(this.f2161a);

        public a(Context context) {
            this.f2161a = context;
        }

        public int getCount() {
            List<com.umeng.fb.a.d> a2 = ConversationActivity.this.g.a();
            if (a2 == null) {
                return 0;
            }
            return a2.size();
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            C0046a aVar;
            if (view == null) {
                view = this.b.inflate(d.c(this.f2161a), (ViewGroup) null);
                aVar = new C0046a();
                aVar.f2162a = (TextView) view.findViewById(c.f(this.f2161a));
                aVar.b = (TextView) view.findViewById(c.b(this.f2161a));
                view.setTag(aVar);
            } else {
                aVar = (C0046a) view.getTag();
            }
            com.umeng.fb.a.d dVar = ConversationActivity.this.g.a().get(i);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
            if (dVar instanceof com.umeng.fb.a.c) {
                layoutParams.addRule(9);
                aVar.b.setLayoutParams(layoutParams);
                aVar.b.setBackgroundResource(b.b(this.f2161a));
            } else {
                layoutParams.addRule(11);
                aVar.b.setLayoutParams(layoutParams);
                aVar.b.setBackgroundResource(b.a(this.f2161a));
            }
            aVar.f2162a.setText(SimpleDateFormat.getDateTimeInstance().format(dVar.c()));
            aVar.b.setText(dVar.b());
            return view;
        }

        public Object getItem(int i) {
            return ConversationActivity.this.g.a().get(i);
        }

        public long getItemId(int i) {
            return (long) i;
        }

        /* renamed from: com.umeng.fb.ConversationActivity$a$a  reason: collision with other inner class name */
        class C0046a {

            /* renamed from: a  reason: collision with root package name */
            TextView f2162a;
            TextView b;

            C0046a() {
            }
        }
    }
}
