package com.umeng.fb.d;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import com.baidu.mobads.interfaces.utils.IXAdSystemUtils;
import java.lang.reflect.Field;
import java.util.Calendar;
import java.util.Locale;

/* compiled from: DeviceConfig */
public class a {

    /* renamed from: a  reason: collision with root package name */
    protected static final String f2181a = a.class.getName();

    public static String a(Context context) {
        try {
            return String.valueOf(context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode);
        } catch (PackageManager.NameNotFoundException e) {
            return "Unknown";
        }
    }

    public static String b(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            return "Unknown";
        }
    }

    public static boolean a(Context context, String str) {
        if (context.getPackageManager().checkPermission(str, context.getPackageName()) != 0) {
            return false;
        }
        return true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:9:0x001d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String a() {
        /*
            r0 = 0
            java.io.FileReader r1 = new java.io.FileReader     // Catch:{ FileNotFoundException -> 0x0044 }
            java.lang.String r2 = "/proc/cpuinfo"
            r1.<init>(r2)     // Catch:{ FileNotFoundException -> 0x0044 }
            if (r1 == 0) goto L_0x001b
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ IOException -> 0x002e }
            r3 = 1024(0x400, float:1.435E-42)
            r2.<init>(r1, r3)     // Catch:{ IOException -> 0x002e }
            java.lang.String r0 = r2.readLine()     // Catch:{ IOException -> 0x002e }
            r2.close()     // Catch:{ IOException -> 0x002e }
            r1.close()     // Catch:{ IOException -> 0x002e }
        L_0x001b:
            if (r0 == 0) goto L_0x0029
            r1 = 58
            int r1 = r0.indexOf(r1)
            int r1 = r1 + 1
            java.lang.String r0 = r0.substring(r1)
        L_0x0029:
            java.lang.String r0 = r0.trim()
            return r0
        L_0x002e:
            r1 = move-exception
            java.lang.String r2 = com.umeng.fb.d.a.f2181a     // Catch:{ FileNotFoundException -> 0x0037 }
            java.lang.String r3 = "Could not read from file /proc/cpuinfo"
            com.umeng.fb.d.c.b(r2, r3, r1)     // Catch:{ FileNotFoundException -> 0x0037 }
            goto L_0x001b
        L_0x0037:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
        L_0x003b:
            java.lang.String r2 = com.umeng.fb.d.a.f2181a
            java.lang.String r3 = "Could not open file /proc/cpuinfo"
            com.umeng.fb.d.c.b(r2, r3, r0)
            r0 = r1
            goto L_0x001b
        L_0x0044:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x003b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.umeng.fb.d.a.a():java.lang.String");
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0025  */
    /* JADX WARNING: Removed duplicated region for block: B:17:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String c(android.content.Context r4) {
        /*
            java.lang.String r0 = "phone"
            java.lang.Object r0 = r4.getSystemService(r0)
            android.telephony.TelephonyManager r0 = (android.telephony.TelephonyManager) r0
            if (r0 != 0) goto L_0x0011
            java.lang.String r1 = com.umeng.fb.d.a.f2181a
            java.lang.String r2 = "No IMEI."
            com.umeng.fb.d.c.d(r1, r2)
        L_0x0011:
            java.lang.String r1 = ""
            java.lang.String r2 = "android.permission.READ_PHONE_STATE"
            boolean r2 = a(r4, r2)     // Catch:{ Exception -> 0x0060 }
            if (r2 == 0) goto L_0x0068
            java.lang.String r0 = r0.getDeviceId()     // Catch:{ Exception -> 0x0060 }
        L_0x001f:
            boolean r1 = android.text.TextUtils.isEmpty(r0)
            if (r1 == 0) goto L_0x005f
            java.lang.String r0 = com.umeng.fb.d.a.f2181a
            java.lang.String r1 = "No IMEI."
            com.umeng.fb.d.c.d(r0, r1)
            java.lang.String r0 = j(r4)
            boolean r1 = android.text.TextUtils.isEmpty(r0)
            if (r1 == 0) goto L_0x005f
            java.lang.String r0 = com.umeng.fb.d.a.f2181a
            java.lang.String r1 = "Failed to take mac as IMEI. Try to use Secure.ANDROID_ID instead."
            com.umeng.fb.d.c.d(r0, r1)
            android.content.ContentResolver r0 = r4.getContentResolver()
            java.lang.String r1 = "android_id"
            java.lang.String r0 = android.provider.Settings.Secure.getString(r0, r1)
            java.lang.String r1 = com.umeng.fb.d.a.f2181a
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "getDeviceId: Secure.ANDROID_ID: "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r0)
            java.lang.String r2 = r2.toString()
            com.umeng.fb.d.c.a(r1, r2)
        L_0x005f:
            return r0
        L_0x0060:
            r0 = move-exception
            java.lang.String r2 = com.umeng.fb.d.a.f2181a
            java.lang.String r3 = "No IMEI."
            com.umeng.fb.d.c.d(r2, r3, r0)
        L_0x0068:
            r0 = r1
            goto L_0x001f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.umeng.fb.d.a.c(android.content.Context):java.lang.String");
    }

    public static String d(Context context) {
        return b.a(c(context));
    }

    public static String e(Context context) {
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
            if (telephonyManager == null) {
                return "Unknown";
            }
            return telephonyManager.getNetworkOperatorName();
        } catch (Exception e) {
            e.printStackTrace();
            return "Unknown";
        }
    }

    public static String[] f(Context context) {
        String[] strArr = {"Unknown", "Unknown"};
        try {
            if (context.getPackageManager().checkPermission("android.permission.ACCESS_NETWORK_STATE", context.getPackageName()) != 0) {
                strArr[0] = "Unknown";
                return strArr;
            }
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
            if (connectivityManager == null) {
                strArr[0] = "Unknown";
                return strArr;
            } else if (connectivityManager.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED) {
                strArr[0] = "Wi-Fi";
                return strArr;
            } else {
                NetworkInfo networkInfo = connectivityManager.getNetworkInfo(0);
                if (networkInfo.getState() == NetworkInfo.State.CONNECTED) {
                    strArr[0] = "2G/3G";
                    strArr[1] = networkInfo.getSubtypeName();
                    return strArr;
                }
                return strArr;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static int g(Context context) {
        try {
            Calendar instance = Calendar.getInstance(n(context));
            if (instance != null) {
                return instance.getTimeZone().getRawOffset() / 3600000;
            }
        } catch (Exception e) {
            c.a(f2181a, "error in getTimeZone", e);
        }
        return 8;
    }

    public static String[] h(Context context) {
        String[] strArr = new String[2];
        try {
            Locale n = n(context);
            if (n != null) {
                strArr[0] = n.getCountry();
                strArr[1] = n.getLanguage();
            }
            if (TextUtils.isEmpty(strArr[0])) {
                strArr[0] = "Unknown";
            }
            if (TextUtils.isEmpty(strArr[1])) {
                strArr[1] = "Unknown";
            }
        } catch (Exception e) {
            c.b(f2181a, "error in getLocaleInfo", e);
        }
        return strArr;
    }

    private static Locale n(Context context) {
        Locale locale = null;
        try {
            Configuration configuration = new Configuration();
            Settings.System.getConfiguration(context.getContentResolver(), configuration);
            if (configuration != null) {
                locale = configuration.locale;
            }
        } catch (Exception e) {
            c.b(f2181a, "fail to read user config locale");
        }
        if (locale == null) {
            return Locale.getDefault();
        }
        return locale;
    }

    public static String i(Context context) {
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            if (applicationInfo != null) {
                String string = applicationInfo.metaData.getString("UMENG_APPKEY");
                if (string != null) {
                    return string.trim();
                }
                c.b(f2181a, "Could not read UMENG_APPKEY meta-data from AndroidManifest.xml.");
            }
        } catch (Exception e) {
            c.b(f2181a, "Could not read UMENG_APPKEY meta-data from AndroidManifest.xml.", e);
        }
        return null;
    }

    public static String j(Context context) {
        try {
            WifiManager wifiManager = (WifiManager) context.getSystemService(IXAdSystemUtils.NT_WIFI);
            if (a(context, "android.permission.ACCESS_WIFI_STATE")) {
                return wifiManager.getConnectionInfo().getMacAddress();
            }
            c.d(f2181a, "Could not get mac address.[no permission android.permission.ACCESS_WIFI_STATE");
            return "";
        } catch (Exception e) {
            c.d(f2181a, "Could not get mac address." + e.toString());
        }
    }

    public static String k(Context context) {
        int i;
        int i2;
        try {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
            if ((context.getApplicationInfo().flags & 8192) == 0) {
                i2 = a(displayMetrics, "noncompatWidthPixels");
                i = a(displayMetrics, "noncompatHeightPixels");
            } else {
                i = -1;
                i2 = -1;
            }
            if (i2 == -1 || i == -1) {
                i2 = displayMetrics.widthPixels;
                i = displayMetrics.heightPixels;
            }
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(i2);
            stringBuffer.append("*");
            stringBuffer.append(i);
            return stringBuffer.toString();
        } catch (Exception e) {
            c.b(f2181a, "read resolution fail", e);
            return "Unknown";
        }
    }

    private static int a(Object obj, String str) {
        try {
            Field declaredField = DisplayMetrics.class.getDeclaredField(str);
            declaredField.setAccessible(true);
            return declaredField.getInt(obj);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    public static String l(Context context) {
        Object obj;
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            if (!(applicationInfo == null || applicationInfo.metaData == null || (obj = applicationInfo.metaData.get("UMENG_CHANNEL")) == null)) {
                String obj2 = obj.toString();
                if (obj2 != null) {
                    return obj2;
                }
                c.a(f2181a, "Could not read UMENG_CHANNEL meta-data from AndroidManifest.xml.");
            }
        } catch (Exception e) {
            c.a(f2181a, "Could not read UMENG_CHANNEL meta-data from AndroidManifest.xml.");
            e.printStackTrace();
        }
        return "Unknown";
    }

    public static String m(Context context) {
        return context.getPackageName();
    }
}
