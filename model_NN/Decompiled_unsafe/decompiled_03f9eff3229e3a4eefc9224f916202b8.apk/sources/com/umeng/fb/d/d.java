package com.umeng.fb.d;

import android.content.Context;

/* compiled from: Res */
public class d {

    /* renamed from: a  reason: collision with root package name */
    private static final String f2184a = d.class.getName();
    private static d b = null;
    private static String c = null;
    private static Class d = null;
    private static Class e = null;
    private static Class f = null;
    private static Class g = null;
    private static Class h = null;
    private static Class i = null;
    private static Class j = null;

    private d(String str) {
        try {
            e = Class.forName(str + ".R$drawable");
        } catch (ClassNotFoundException e2) {
            c.b(f2184a, e2.getMessage());
        }
        try {
            f = Class.forName(str + ".R$layout");
        } catch (ClassNotFoundException e3) {
            c.b(f2184a, e3.getMessage());
        }
        try {
            d = Class.forName(str + ".R$id");
        } catch (ClassNotFoundException e4) {
            c.b(f2184a, e4.getMessage());
        }
        try {
            g = Class.forName(str + ".R$anim");
        } catch (ClassNotFoundException e5) {
            c.b(f2184a, e5.getMessage());
        }
        try {
            h = Class.forName(str + ".R$style");
        } catch (ClassNotFoundException e6) {
            c.b(f2184a, e6.getMessage());
        }
        try {
            i = Class.forName(str + ".R$string");
        } catch (ClassNotFoundException e7) {
            c.b(f2184a, e7.getMessage());
        }
        try {
            j = Class.forName(str + ".R$array");
        } catch (ClassNotFoundException e8) {
            c.b(f2184a, e8.getMessage());
        }
    }

    public static synchronized d a(Context context) {
        d dVar;
        synchronized (d.class) {
            if (b == null) {
                c = c != null ? c : context.getPackageName();
                b = new d(c);
            }
            dVar = b;
        }
        return dVar;
    }

    public int a(String str) {
        return a(g, str);
    }

    public int b(String str) {
        return a(d, str);
    }

    public int c(String str) {
        return a(e, str);
    }

    public int d(String str) {
        return a(f, str);
    }

    public int e(String str) {
        return a(i, str);
    }

    private int a(Class<?> cls, String str) {
        if (cls == null) {
            c.b(f2184a, "getRes(null," + str + ")");
            throw new IllegalArgumentException("ResClass is not initialized. Please make sure you have added necessary resources. Also make sure you have " + c + ".R$* configured in obfuscation. field=" + str);
        }
        try {
            return cls.getField(str).getInt(str);
        } catch (Exception e2) {
            c.b(f2184a, "getRes(" + cls.getName() + ", " + str + ")");
            c.b(f2184a, "Error getting resource. Make sure you have copied all resources (res/) from SDK to your project. ");
            c.b(f2184a, e2.getMessage());
            return -1;
        }
    }
}
