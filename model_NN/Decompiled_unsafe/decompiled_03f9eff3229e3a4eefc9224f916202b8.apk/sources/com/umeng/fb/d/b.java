package com.umeng.fb.d;

import android.content.Context;
import android.os.Build;
import com.alimama.mobile.pluginframework.core.PluginFramework;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import org.apache.mina.proxy.handlers.socks.SocksProxyConstants;
import org.json.JSONObject;

/* compiled from: Helper */
public class b {

    /* renamed from: a  reason: collision with root package name */
    public static final String f2182a = System.getProperty("line.separator");
    private static final String b = b.class.getName();

    public static String a(Context context) {
        return "FB[" + a.i(context) + "_" + a.d(context) + "]" + String.valueOf(System.currentTimeMillis()) + String.valueOf((int) (1000.0d + (Math.random() * 9000.0d)));
    }

    public static String a() {
        return "RP" + String.valueOf(System.currentTimeMillis()) + String.valueOf((int) (1000.0d + (Math.random() * 9000.0d)));
    }

    public static JSONObject b(Context context) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("device_id", a.c(context));
            jSONObject.put("idmd5", a.d(context));
            jSONObject.put("device_model", Build.MODEL);
            jSONObject.put(LogBuilder.KEY_APPKEY, a.i(context));
            jSONObject.put(LogBuilder.KEY_CHANNEL, a.l(context));
            jSONObject.put("app_version", a.b(context));
            jSONObject.put("version_code", a.a(context));
            jSONObject.put("sdk_type", "Android");
            jSONObject.put("sdk_version", "4.3.2.20140520");
            jSONObject.put("os", "Android");
            jSONObject.put("os_version", Build.VERSION.RELEASE);
            jSONObject.put("country", a.h(context)[0]);
            jSONObject.put("language", a.h(context)[1]);
            jSONObject.put("timezone", a.g(context));
            jSONObject.put("resolution", a.k(context));
            jSONObject.put(PluginFramework.KEY_UPDATE_ACCESS, a.f(context)[0]);
            jSONObject.put("access_subtype", a.f(context)[1]);
            jSONObject.put("carrier", a.e(context));
            jSONObject.put("cpu", a.a());
            jSONObject.put("package", a.m(context));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jSONObject;
    }

    public static String a(String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(str.getBytes());
            byte[] digest = instance.digest();
            StringBuffer stringBuffer = new StringBuffer();
            for (byte b2 : digest) {
                stringBuffer.append(Integer.toHexString(b2 & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD));
            }
            return stringBuffer.toString();
        } catch (NoSuchAlgorithmException e) {
            c.a(b, "getMD5 error", e);
            return "";
        }
    }
}
