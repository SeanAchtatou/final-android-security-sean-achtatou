package com.umeng.analytics;

import android.content.Context;
import android.text.TextUtils;

/* compiled from: InternalConfig */
public class j {

    /* renamed from: a  reason: collision with root package name */
    private static String[] f2154a = new String[2];

    public static String[] a(Context context) {
        String[] a2;
        if (!TextUtils.isEmpty(f2154a[0]) && !TextUtils.isEmpty(f2154a[1])) {
            return f2154a;
        }
        if (context == null || (a2 = m.a(context).a()) == null) {
            return null;
        }
        f2154a[0] = a2[0];
        f2154a[1] = a2[1];
        return f2154a;
    }
}
