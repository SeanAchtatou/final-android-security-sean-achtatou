package com.umeng.analytics;

import a.a.Cdo;
import a.a.bn;
import a.a.ds;
import a.a.dt;
import a.a.dz;
import a.a.e;
import a.a.eb;
import a.a.ee;
import android.content.Context;
import android.text.TextUtils;
import com.umeng.analytics.a.a;
import com.umeng.analytics.a.b;
import java.util.Map;

/* compiled from: InternalAgent */
public class g implements dz {

    /* renamed from: a  reason: collision with root package name */
    private final b f2151a = new b();
    private Context b = null;
    private f c;
    private ds d = new ds();
    private e e = new e();
    private ee f = new ee();
    private dt g;
    private Cdo h;
    private boolean i = false;

    g() {
        this.d.a(this);
    }

    private void e(Context context) {
        if (!this.i) {
            this.b = context.getApplicationContext();
            this.g = new dt(this.b);
            this.h = Cdo.a(this.b);
            this.i = true;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        if (!a.h) {
            try {
                this.e.a(str);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void b(String str) {
        if (!a.h) {
            try {
                this.e.b(str);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Context context) {
        if (context == null) {
            bn.b("MobclickAgent", "unexpected null context in onResume");
            return;
        }
        this.f2151a.a(context);
        try {
            Cdo.a(context).a(this.f2151a);
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: package-private */
    public void a(a aVar) {
        this.f2151a.a(aVar);
    }

    /* access modifiers changed from: package-private */
    public void b(Context context) {
        if (context == null) {
            bn.b("MobclickAgent", "unexpected null context in onResume");
            return;
        }
        if (a.h) {
            this.e.a(context.getClass().getName());
        }
        try {
            if (!this.i) {
                e(context);
            }
            k.a(new h(this, context));
        } catch (Exception e2) {
            bn.b("MobclickAgent", "Exception occurred in Mobclick.onResume(). ", e2);
        }
    }

    /* access modifiers changed from: package-private */
    public void c(Context context) {
        if (context == null) {
            bn.b("MobclickAgent", "unexpected null context in onPause");
            return;
        }
        if (a.h) {
            this.e.b(context.getClass().getName());
        }
        try {
            if (!this.i) {
                e(context);
            }
            k.a(new i(this, context));
        } catch (Exception e2) {
            bn.b("MobclickAgent", "Exception occurred in Mobclick.onRause(). ", e2);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Context context, String str) {
        if (!TextUtils.isEmpty(str)) {
            if (context == null) {
                bn.b("MobclickAgent", "unexpected null context in reportError");
                return;
            }
            try {
                if (!this.i) {
                    e(context);
                }
                this.h.a(new a.a.g(str).a(false));
            } catch (Exception e2) {
                bn.b("MobclickAgent", "", e2);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Context context, Throwable th) {
        if (context != null && th != null) {
            try {
                if (!this.i) {
                    e(context);
                }
                this.h.a(new a.a.g(th).a(false));
            } catch (Exception e2) {
                bn.b("MobclickAgent", "", e2);
            }
        }
    }

    /* access modifiers changed from: private */
    public void f(Context context) {
        this.f.c(context);
        if (this.c != null) {
            this.c.a();
        }
    }

    /* access modifiers changed from: private */
    public void g(Context context) {
        this.f.d(context);
        this.e.a(context);
        if (this.c != null) {
            this.c.b();
        }
        this.h.b();
    }

    public void a(Context context, String str, String str2, long j, int i2) {
        try {
            if (!this.i) {
                e(context);
            }
            this.g.a(str, str2, j, i2);
        } catch (Exception e2) {
            bn.b("MobclickAgent", "", e2);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Context context, String str, Map<String, Object> map, long j) {
        try {
            if (!this.i) {
                e(context);
            }
            this.g.a(str, map, j);
        } catch (Exception e2) {
            bn.b("MobclickAgent", "", e2);
        }
    }

    /* access modifiers changed from: package-private */
    public void d(Context context) {
        try {
            this.e.a();
            g(context);
            eb.a(context).edit().commit();
            k.a();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void a(Throwable th) {
        try {
            this.e.a();
            if (this.b != null) {
                if (!(th == null || this.h == null)) {
                    this.h.b(new a.a.g(th));
                }
                g(this.b);
                eb.a(this.b).edit().commit();
            }
            k.a();
        } catch (Exception e2) {
            bn.a("MobclickAgent", "Exception in onAppCrash", e2);
        }
    }
}
