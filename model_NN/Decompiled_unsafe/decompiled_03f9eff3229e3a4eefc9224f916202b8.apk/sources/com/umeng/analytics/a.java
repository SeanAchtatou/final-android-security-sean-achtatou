package com.umeng.analytics;

import a.a.bm;
import android.content.Context;

/* compiled from: AnalyticsConfig */
public class a {

    /* renamed from: a  reason: collision with root package name */
    public static String f2136a = null;
    public static String b = null;
    public static int c;
    public static String d = "";
    public static String e = "";
    public static boolean f = false;
    public static int g;
    public static boolean h = true;
    public static boolean i = true;
    public static boolean j = true;
    public static boolean k = true;
    public static long l = StatisticConfig.MIN_UPLOAD_INTERVAL;
    private static String m = null;
    private static String n = null;
    private static double[] o = null;

    public static String a(Context context) {
        if (m == null) {
            m = bm.j(context);
        }
        return m;
    }

    public static String b(Context context) {
        if (n == null) {
            n = bm.n(context);
        }
        return n;
    }

    public static String a() {
        if (c == 1) {
            return "5.5.3.0";
        }
        return "5.5.3";
    }

    public static double[] b() {
        return o;
    }
}
