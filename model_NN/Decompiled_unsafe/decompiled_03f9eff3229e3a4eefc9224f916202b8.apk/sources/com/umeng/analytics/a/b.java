package com.umeng.analytics.a;

import a.a.bm;
import a.a.bn;
import a.a.bo;
import a.a.bp;
import a.a.bq;
import a.a.bt;
import android.content.Context;
import android.content.SharedPreferences;
import com.umeng.analytics.m;
import java.util.Iterator;
import org.json.JSONObject;

/* compiled from: OnlineConfigAgent */
public class b {

    /* renamed from: a  reason: collision with root package name */
    private final String f2137a = "last_config_time";
    private final String b = "report_policy";
    private final String c = "online_config";
    private a d = null;
    /* access modifiers changed from: private */
    public d e = null;

    public void a(Context context) {
        if (context == null) {
            try {
                bn.b("MobclickAgent", "unexpected null context in updateOnlineConfig");
            } catch (Exception e2) {
                bn.b("MobclickAgent", "exception in updateOnlineConfig");
            }
        } else {
            new Thread(new C0044b(context.getApplicationContext())).start();
        }
    }

    public void a(a aVar) {
        this.d = aVar;
    }

    public void a(d dVar) {
        this.e = dVar;
    }

    /* access modifiers changed from: private */
    public void a(JSONObject jSONObject) {
        if (this.d != null) {
            this.d.a(jSONObject);
        }
    }

    /* access modifiers changed from: private */
    public long b(Context context) {
        return m.a(context).i().getLong("oc_mdf_told", 0);
    }

    /* access modifiers changed from: private */
    public void a(Context context, c cVar) {
        if (cVar.c != -1) {
            m.a(context).a(cVar.c, cVar.d);
        }
    }

    /* access modifiers changed from: private */
    public void b(Context context, c cVar) {
        if (cVar.f2142a != null && cVar.f2142a.length() != 0) {
            SharedPreferences.Editor edit = m.a(context).i().edit();
            try {
                JSONObject jSONObject = cVar.f2142a;
                Iterator<String> keys = jSONObject.keys();
                while (keys.hasNext()) {
                    String next = keys.next();
                    edit.putString(next, jSONObject.getString(next));
                }
                edit.commit();
                bn.a("MobclickAgent", "get online setting params: " + jSONObject);
            } catch (Exception e2) {
                bn.c("MobclickAgent", "save online config params", e2);
            }
        }
    }

    /* renamed from: com.umeng.analytics.a.b$b  reason: collision with other inner class name */
    /* compiled from: OnlineConfigAgent */
    public class C0044b extends bo implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        Context f2139a;

        public C0044b(Context context) {
            this.f2139a = context.getApplicationContext();
        }

        public void run() {
            try {
                if (c()) {
                    b();
                }
            } catch (Exception e) {
                b.this.a((JSONObject) null);
                bn.c("MobclickAgent", "request online config error", e);
            }
        }

        public boolean a() {
            return true;
        }

        private void b() {
            c cVar = (c) a(new a(this.f2139a), c.class);
            if (cVar == null) {
                b.this.a((JSONObject) null);
            } else if (cVar.b) {
                if (b.this.e != null) {
                    b.this.e.a(cVar.c, (long) cVar.d);
                }
                b.this.a(this.f2139a, cVar);
                b.this.b(this.f2139a, cVar);
                b.this.a(cVar.f2142a);
            } else {
                b.this.a((JSONObject) null);
            }
        }

        /* JADX WARNING: Removed duplicated region for block: B:20:0x008f  */
        /* JADX WARNING: Removed duplicated region for block: B:23:0x009c  */
        /* JADX WARNING: Removed duplicated region for block: B:26:0x00b7  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private boolean c() {
            /*
                r14 = this;
                r1 = 1
                r12 = 0
                r2 = 0
                android.content.Context r0 = r14.f2139a
                java.lang.String r0 = com.umeng.analytics.a.a(r0)
                boolean r0 = android.text.TextUtils.isEmpty(r0)
                if (r0 == 0) goto L_0x0018
                java.lang.String r0 = "MobclickAgent"
                java.lang.String r1 = "Appkey is missing ,Please check AndroidManifest.xml"
                a.a.bn.b(r0, r1)
            L_0x0017:
                return r2
            L_0x0018:
                boolean r0 = a.a.bn.f43a
                if (r0 == 0) goto L_0x00b4
                android.content.Context r0 = r14.f2139a
                boolean r0 = a.a.bm.q(r0)
                if (r0 == 0) goto L_0x00b4
                r3 = r1
            L_0x0025:
                if (r3 != 0) goto L_0x00b9
                android.content.Context r0 = r14.f2139a
                com.umeng.analytics.m r0 = com.umeng.analytics.m.a(r0)
                android.content.SharedPreferences r0 = r0.i()
                java.lang.String r4 = "last_test_t"
                long r4 = r0.getLong(r4, r12)
                long r6 = java.lang.System.currentTimeMillis()
                long r4 = r6 - r4
                java.lang.String r8 = "oc_req_i"
                r10 = 600000(0x927c0, double:2.964394E-318)
                long r8 = r0.getLong(r8, r10)
                int r4 = (r4 > r8 ? 1 : (r4 == r8 ? 0 : -1))
                if (r4 <= 0) goto L_0x00b9
                android.content.SharedPreferences$Editor r0 = r0.edit()
                java.lang.String r4 = "last_test_t"
                android.content.SharedPreferences$Editor r0 = r0.putLong(r4, r6)
                r0.commit()
                r0 = r1
            L_0x0058:
                if (r3 != 0) goto L_0x005c
                if (r0 == 0) goto L_0x0017
            L_0x005c:
                com.umeng.analytics.a.b$c r0 = new com.umeng.analytics.a.b$c
                com.umeng.analytics.a.b r3 = com.umeng.analytics.a.b.this
                android.content.Context r4 = r14.f2139a
                r0.<init>(r4)
                java.lang.Class<com.umeng.analytics.a.b$d> r3 = com.umeng.analytics.a.b.d.class
                a.a.bq r0 = r14.a(r0, r3)
                com.umeng.analytics.a.b$d r0 = (com.umeng.analytics.a.b.d) r0
                if (r0 == 0) goto L_0x0017
                android.content.Context r3 = r14.f2139a
                com.umeng.analytics.m r3 = com.umeng.analytics.m.a(r3)
                android.content.SharedPreferences r3 = r3.i()
                long r4 = r0.f2141a
                java.lang.String r6 = "oc_mdf_t"
                long r6 = r3.getLong(r6, r12)
                int r4 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
                if (r4 <= 0) goto L_0x00b7
            L_0x0085:
                android.content.SharedPreferences$Editor r2 = r3.edit()
                long r4 = r0.b
                int r4 = (r4 > r12 ? 1 : (r4 == r12 ? 0 : -1))
                if (r4 < 0) goto L_0x0096
                java.lang.String r4 = "oc_req_i"
                long r6 = r0.b
                r2.putLong(r4, r6)
            L_0x0096:
                long r4 = r0.f2141a
                int r4 = (r4 > r12 ? 1 : (r4 == r12 ? 0 : -1))
                if (r4 < 0) goto L_0x00ae
                java.lang.String r4 = "oc_mdf_told"
                java.lang.String r5 = "oc_mdf_t"
                long r6 = r3.getLong(r5, r12)
                r2.putLong(r4, r6)
                java.lang.String r3 = "oc_mdf_t"
                long r4 = r0.f2141a
                r2.putLong(r3, r4)
            L_0x00ae:
                r2.commit()
                r2 = r1
                goto L_0x0017
            L_0x00b4:
                r3 = r2
                goto L_0x0025
            L_0x00b7:
                r1 = r2
                goto L_0x0085
            L_0x00b9:
                r0 = r2
                goto L_0x0058
            */
            throw new UnsupportedOperationException("Method not decompiled: com.umeng.analytics.a.b.C0044b.c():boolean");
        }
    }

    /* compiled from: OnlineConfigAgent */
    class a extends bp {
        private final String e = "http://oc.umeng.com/v2/check_config_update";
        private JSONObject f;

        public a(Context context) {
            super(null);
            this.d = "http://oc.umeng.com/v2/check_config_update";
            this.f = a(context);
        }

        public JSONObject a() {
            return this.f;
        }

        public String b() {
            return this.d;
        }

        private JSONObject a(Context context) {
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put("type", "online_config");
                jSONObject.put(LogBuilder.KEY_APPKEY, com.umeng.analytics.a.a(context));
                jSONObject.put("version_code", bm.a(context));
                jSONObject.put("package", bm.o(context));
                jSONObject.put("sdk_version", com.umeng.analytics.a.a());
                jSONObject.put("idmd5", bt.b(bm.c(context)));
                jSONObject.put(LogBuilder.KEY_CHANNEL, com.umeng.analytics.a.b(context));
                jSONObject.put("report_policy", m.a(context).b()[0]);
                jSONObject.put("last_config_time", b.this.b(context));
                return jSONObject;
            } catch (Exception e2) {
                bn.b("MobclickAgent", "exception in onlineConfigInternal");
                return null;
            }
        }
    }

    /* compiled from: OnlineConfigAgent */
    class c extends bp {
        private final String e = "http://oc.umeng.com/v2/get_update_time";
        private JSONObject f;

        public c(Context context) {
            super(null);
            this.d = "http://oc.umeng.com/v2/get_update_time";
            this.f = a(context);
        }

        public JSONObject a() {
            return this.f;
        }

        private JSONObject a(Context context) {
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put(LogBuilder.KEY_APPKEY, com.umeng.analytics.a.a(context));
                jSONObject.put("version_code", bm.a(context));
                return jSONObject;
            } catch (Exception e2) {
                bn.b("MobclickAgent", "exception in onlineConfigInternal");
                return null;
            }
        }

        public String b() {
            return this.d;
        }
    }

    /* compiled from: OnlineConfigAgent */
    public static class d extends bq {

        /* renamed from: a  reason: collision with root package name */
        public long f2141a = -1;
        public long b = -1;

        public d(JSONObject jSONObject) {
            super(jSONObject);
            a(jSONObject);
        }

        private void a(JSONObject jSONObject) {
            if (jSONObject != null) {
                try {
                    this.f2141a = jSONObject.optLong("last_config_time", -1);
                    this.b = jSONObject.optLong("oc_interval", -1) * 60 * 1000;
                } catch (Exception e) {
                    bn.d("MobclickAgent", "fail to parce online config response", e);
                }
            }
        }
    }
}
