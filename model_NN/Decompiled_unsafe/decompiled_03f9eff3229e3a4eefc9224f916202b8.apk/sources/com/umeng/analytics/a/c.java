package com.umeng.analytics.a;

import a.a.bn;
import a.a.bq;
import cn.banshenggua.aichang.utils.Constants;
import java.util.Locale;
import org.json.JSONObject;

/* compiled from: OnlineConfigResponse */
public class c extends bq {

    /* renamed from: a  reason: collision with root package name */
    public JSONObject f2142a = null;
    boolean b = false;
    int c = -1;
    int d = -1;
    private final String e = "config_update";
    private final String f = "report_policy";
    private final String g = "online_params";
    private final String h = "report_interval";

    public c(JSONObject jSONObject) {
        super(jSONObject);
        if (jSONObject != null) {
            a(jSONObject);
            a();
        }
    }

    private void a(JSONObject jSONObject) {
        try {
            if (jSONObject.has("config_update") && !jSONObject.getString("config_update").toLowerCase(Locale.US).equals("no")) {
                if (jSONObject.has("report_policy")) {
                    this.c = jSONObject.getInt("report_policy");
                    this.d = jSONObject.optInt("report_interval") * Constants.CLEARIMGED;
                } else {
                    bn.d("MobclickAgent", " online config fetch no report policy");
                }
                this.f2142a = jSONObject.optJSONObject("online_params");
                this.b = true;
            }
        } catch (Exception e2) {
            bn.d("MobclickAgent", "fail to parce online config response", e2);
        }
    }

    private void a() {
        if (this.c < 0 || this.c > 6) {
            this.c = 1;
        }
    }
}
