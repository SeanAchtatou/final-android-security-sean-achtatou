package com.umeng.analytics;

import a.a.bm;
import a.a.bn;
import a.a.bt;
import android.content.Context;
import android.content.SharedPreferences;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.util.Arrays;
import java.util.Locale;

/* compiled from: StoreHelper */
public final class m {

    /* renamed from: a  reason: collision with root package name */
    private static m f2156a = null;
    private static Context b;
    private static String c;
    private static long e = 1209600000;
    private static long f = 2097152;
    private a d;

    /* compiled from: StoreHelper */
    public interface b {
        void a(File file);

        boolean b(File file);

        void c(File file);
    }

    public m(Context context) {
        this.d = new a(context);
        b = context.getApplicationContext();
        c = context.getPackageName();
    }

    public static synchronized m a(Context context) {
        m mVar;
        synchronized (m.class) {
            if (f2156a == null) {
                f2156a = new m(context);
            }
            mVar = f2156a;
        }
        return mVar;
    }

    private static boolean a(File file) {
        long length = file.length();
        if (!file.exists() || length <= f) {
            return false;
        }
        return true;
    }

    public String[] a() {
        SharedPreferences j = j();
        String string = j.getString("au_p", null);
        String string2 = j.getString("au_u", null);
        if (string == null || string2 == null) {
            return null;
        }
        return new String[]{string, string2};
    }

    public int[] b() {
        SharedPreferences i = i();
        int[] iArr = new int[2];
        if (i.getInt("umeng_net_report_policy", -1) != -1) {
            iArr[0] = i.getInt("umeng_net_report_policy", 1);
            iArr[1] = (int) i.getLong("umeng_net_report_interval", 0);
        } else {
            iArr[0] = i.getInt("umeng_local_report_policy", 1);
            iArr[1] = (int) i.getLong("umeng_local_report_interval", 0);
        }
        return iArr;
    }

    public void a(int i) {
        SharedPreferences i2;
        if (i >= 0 && i <= 3 && (i2 = i()) != null) {
            i2.edit().putInt("oc_dc", i).commit();
        }
    }

    public int c() {
        SharedPreferences i = i();
        if (i != null) {
            return i.getInt("oc_dc", 0);
        }
        return 0;
    }

    public void b(int i) {
        SharedPreferences i2;
        if (i > 0 && (i2 = i()) != null) {
            i2.edit().putInt("oc_lt", i).commit();
        }
    }

    public int d() {
        SharedPreferences i = i();
        if (i != null) {
            return i.getInt("oc_lt", 0);
        }
        return 0;
    }

    public void c(int i) {
        SharedPreferences i2 = i();
        if (i2 != null) {
            i2.edit().putInt("oc_ec", i).commit();
        }
    }

    public int d(int i) {
        SharedPreferences i2 = i();
        if (i2 != null) {
            return i2.getInt("oc_ec", i);
        }
        return i;
    }

    public void a(int i, int i2) {
        SharedPreferences.Editor edit = a(b).i().edit();
        edit.putInt("umeng_net_report_policy", i);
        edit.putLong("umeng_net_report_interval", (long) i2);
        edit.commit();
    }

    public byte[] e() {
        FileInputStream fileInputStream;
        Throwable th;
        byte[] bArr = null;
        String l = l();
        File file = new File(b.getFilesDir(), l);
        if (a(file)) {
            file.delete();
        } else if (file.exists()) {
            try {
                fileInputStream = b.openFileInput(l);
                try {
                    bArr = bt.b(fileInputStream);
                    bt.c(fileInputStream);
                } catch (Exception e2) {
                    e = e2;
                    try {
                        e.printStackTrace();
                        bt.c(fileInputStream);
                        return bArr;
                    } catch (Throwable th2) {
                        th = th2;
                        bt.c(fileInputStream);
                        throw th;
                    }
                }
            } catch (Exception e3) {
                e = e3;
                fileInputStream = null;
            } catch (Throwable th3) {
                fileInputStream = null;
                th = th3;
                bt.c(fileInputStream);
                throw th;
            }
        }
        return bArr;
    }

    public void a(byte[] bArr) {
        try {
            bt.a(new File(b.getFilesDir(), l()), bArr);
        } catch (Exception e2) {
            bn.b("MobclickAgent", e2.getMessage());
        }
    }

    public void f() {
        b.deleteFile(k());
        b.deleteFile(l());
    }

    public void b(byte[] bArr) {
        this.d.a(bArr);
    }

    public boolean g() {
        return this.d.a();
    }

    public a h() {
        return this.d;
    }

    private SharedPreferences j() {
        return b.getSharedPreferences("mobclick_agent_user_" + c, 0);
    }

    public SharedPreferences i() {
        return b.getSharedPreferences("mobclick_agent_online_setting_" + c, 0);
    }

    private String k() {
        return "mobclick_agent_header_" + c;
    }

    private String l() {
        return "mobclick_agent_cached_" + c + bm.a(b);
    }

    /* compiled from: StoreHelper */
    public static class a {

        /* renamed from: a  reason: collision with root package name */
        private final int f2157a;
        private File b;
        private FilenameFilter c;

        public a(Context context) {
            this(context, ".um");
        }

        public a(Context context, String str) {
            this.f2157a = 10;
            this.c = new n(this);
            this.b = new File(context.getFilesDir(), str);
            if (!this.b.exists() || !this.b.isDirectory()) {
                this.b.mkdir();
            }
        }

        public boolean a() {
            File[] listFiles = this.b.listFiles();
            if (listFiles == null || listFiles.length <= 0) {
                return false;
            }
            return true;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:12:0x0027, code lost:
            r1[r0].delete();
         */
        /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void a(com.umeng.analytics.m.b r5) {
            /*
                r4 = this;
                java.io.File r0 = r4.b
                java.io.FilenameFilter r1 = r4.c
                java.io.File[] r1 = r0.listFiles(r1)
                if (r1 == 0) goto L_0x0034
                int r0 = r1.length
                if (r0 <= 0) goto L_0x0034
                java.io.File r0 = r4.b
                r5.a(r0)
                int r2 = r1.length
                r0 = 0
            L_0x0014:
                if (r0 >= r2) goto L_0x002f
                r3 = r1[r0]     // Catch:{ Throwable -> 0x0026, all -> 0x002d }
                boolean r3 = r5.b(r3)     // Catch:{ Throwable -> 0x0026, all -> 0x002d }
                if (r3 == 0) goto L_0x0023
                r3 = r1[r0]
                r3.delete()
            L_0x0023:
                int r0 = r0 + 1
                goto L_0x0014
            L_0x0026:
                r3 = move-exception
                r3 = r1[r0]
                r3.delete()
                goto L_0x0023
            L_0x002d:
                r0 = move-exception
                throw r0
            L_0x002f:
                java.io.File r0 = r4.b
                r5.c(r0)
            L_0x0034:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.umeng.analytics.m.a.a(com.umeng.analytics.m$b):void");
        }

        public void a(byte[] bArr) {
            if (bArr != null && bArr.length != 0) {
                try {
                    bt.a(new File(this.b, String.format(Locale.US, "um_cache_%d.env", Long.valueOf(System.currentTimeMillis()))), bArr);
                } catch (Exception e) {
                }
                File[] listFiles = this.b.listFiles(this.c);
                if (listFiles != null && listFiles.length >= 10) {
                    Arrays.sort(listFiles);
                    int length = listFiles.length - 10;
                    for (int i = 0; i < length; i++) {
                        listFiles[i].delete();
                    }
                }
            }
        }
    }
}
