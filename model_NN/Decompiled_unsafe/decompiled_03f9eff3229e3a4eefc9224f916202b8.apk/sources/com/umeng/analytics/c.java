package com.umeng.analytics;

import a.a.bm;
import a.a.j;
import android.content.Context;

/* compiled from: ReportPolicy */
public class c {

    /* compiled from: ReportPolicy */
    public static class f {
        public boolean a(boolean z) {
            return true;
        }

        public boolean a() {
            return true;
        }
    }

    /* renamed from: com.umeng.analytics.c$c  reason: collision with other inner class name */
    /* compiled from: ReportPolicy */
    public static class C0045c extends f {
        public boolean a(boolean z) {
            return z;
        }
    }

    /* compiled from: ReportPolicy */
    public static class d extends f {

        /* renamed from: a  reason: collision with root package name */
        private long f2146a = 90000;
        private long b;
        private a.a.b c;

        public d(a.a.b bVar, long j) {
            this.c = bVar;
            this.b = j < this.f2146a ? this.f2146a : j;
        }

        public boolean a(boolean z) {
            if (System.currentTimeMillis() - this.c.c >= this.b) {
                return true;
            }
            return false;
        }
    }

    /* compiled from: ReportPolicy */
    public static class e extends f {

        /* renamed from: a  reason: collision with root package name */
        private long f2147a = LogBuilder.MAX_INTERVAL;
        private a.a.b b;

        public e(a.a.b bVar) {
            this.b = bVar;
        }

        public boolean a(boolean z) {
            if (System.currentTimeMillis() - this.b.c >= this.f2147a) {
                return true;
            }
            return false;
        }
    }

    /* compiled from: ReportPolicy */
    public static class g extends f {

        /* renamed from: a  reason: collision with root package name */
        private Context f2148a = null;

        public g(Context context) {
            this.f2148a = context;
        }

        public boolean a(boolean z) {
            return bm.f(this.f2148a);
        }
    }

    /* compiled from: ReportPolicy */
    public static class a extends f {

        /* renamed from: a  reason: collision with root package name */
        private j f2144a;
        private a.a.b b;

        public a(a.a.b bVar, j jVar) {
            this.b = bVar;
            this.f2144a = jVar;
        }

        public boolean a(boolean z) {
            long currentTimeMillis = System.currentTimeMillis();
            if (currentTimeMillis - this.b.c >= this.f2144a.a()) {
                return true;
            }
            return false;
        }

        public boolean a() {
            return this.f2144a.b();
        }
    }

    /* compiled from: ReportPolicy */
    public static class b extends f {

        /* renamed from: a  reason: collision with root package name */
        private long f2145a;
        private long b = 0;

        public b(int i) {
            this.f2145a = (long) i;
            this.b = System.currentTimeMillis();
        }

        public boolean a(boolean z) {
            if (System.currentTimeMillis() - this.b >= this.f2145a) {
                return true;
            }
            return false;
        }

        public boolean a() {
            return System.currentTimeMillis() - this.b < this.f2145a;
        }
    }
}
