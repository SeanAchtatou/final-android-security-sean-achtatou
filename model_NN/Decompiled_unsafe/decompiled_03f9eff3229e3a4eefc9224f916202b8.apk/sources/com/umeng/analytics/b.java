package com.umeng.analytics;

import a.a.bn;
import android.content.Context;
import android.text.TextUtils;
import com.umeng.analytics.a.a;
import java.util.HashMap;
import java.util.Map;

/* compiled from: MobclickAgent */
public class b {

    /* renamed from: a  reason: collision with root package name */
    private static final g f2143a = new g();

    public static void a(a aVar) {
        f2143a.a(aVar);
    }

    public static g a() {
        return f2143a;
    }

    public static void a(boolean z) {
        a.h = z;
    }

    public static void a(String str) {
        if (!TextUtils.isEmpty(str)) {
            f2143a.a(str);
        } else {
            bn.b("MobclickAgent", "pageName is null or empty");
        }
    }

    public static void b(String str) {
        if (!TextUtils.isEmpty(str)) {
            f2143a.b(str);
        } else {
            bn.b("MobclickAgent", "pageName is null or empty");
        }
    }

    public static void a(Context context) {
        f2143a.c(context);
    }

    public static void b(Context context) {
        if (context == null) {
            bn.b("MobclickAgent", "unexpected null context in onResume");
        } else {
            f2143a.b(context);
        }
    }

    public static void a(Context context, String str) {
        f2143a.a(context, str);
    }

    public static void a(Context context, Throwable th) {
        f2143a.a(context, th);
    }

    public static void b(Context context, String str) {
        f2143a.a(context, str, null, -1, 1);
    }

    public static void a(Context context, String str, Map<String, String> map) {
        if (map == null) {
            bn.b("MobclickAgent", "input map is null");
            return;
        }
        f2143a.a(context, str, new HashMap(map), -1);
    }

    public static void a(Context context, String str, Map<String, String> map, int i) {
        HashMap hashMap;
        if (map == null) {
            hashMap = new HashMap();
        } else {
            hashMap = new HashMap(map);
        }
        hashMap.put("__ct__", Integer.valueOf(i));
        f2143a.a(context, str, hashMap, -1);
    }

    public static String c(Context context, String str) {
        return m.a(context).i().getString(str, "");
    }

    public static void c(Context context) {
        f2143a.a(context);
    }

    public static void d(Context context) {
        f2143a.d(context);
    }
}
