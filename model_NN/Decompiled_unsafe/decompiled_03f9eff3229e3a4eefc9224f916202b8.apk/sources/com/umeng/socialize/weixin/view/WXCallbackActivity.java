package com.umeng.socialize.weixin.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import com.tencent.mm.sdk.modelbase.BaseReq;
import com.tencent.mm.sdk.modelbase.BaseResp;
import com.tencent.mm.sdk.openapi.IWXAPIEventHandler;
import com.umeng.socialize.PlatformConfig;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.c.b;
import com.umeng.socialize.handler.UMWXHandler;
import com.umeng.socialize.utils.g;

public abstract class WXCallbackActivity extends Activity implements IWXAPIEventHandler {

    /* renamed from: a  reason: collision with root package name */
    protected UMWXHandler f2324a = null;
    private final String b = WXCallbackActivity.class.getSimpleName();

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        g.a("create wx callback activity");
        this.f2324a = (UMWXHandler) UMShareAPI.get(getApplicationContext()).getHandler(b.WEIXIN);
        g.b("xxxx wxhandler=" + this.f2324a);
        this.f2324a.a(getApplicationContext(), PlatformConfig.getPlatform(b.WEIXIN));
        this.f2324a.e().handleIntent(getIntent(), this);
    }

    /* access modifiers changed from: protected */
    public final void onNewIntent(Intent intent) {
        g.c(this.b, "### WXCallbackActivity   onNewIntent");
        super.onNewIntent(intent);
        setIntent(intent);
        this.f2324a = (UMWXHandler) UMShareAPI.get(getApplicationContext()).getHandler(b.WEIXIN);
        this.f2324a.a(getApplicationContext(), PlatformConfig.getPlatform(b.WEIXIN));
        this.f2324a.e().handleIntent(intent, this);
    }

    public void onResp(BaseResp baseResp) {
        if (!(this.f2324a == null || baseResp == null)) {
            try {
                this.f2324a.d().onResp(baseResp);
            } catch (Exception e) {
            }
        }
        finish();
    }

    public void onReq(BaseReq baseReq) {
        if (this.f2324a != null) {
            this.f2324a.d().onReq(baseReq);
        }
        finish();
    }
}
