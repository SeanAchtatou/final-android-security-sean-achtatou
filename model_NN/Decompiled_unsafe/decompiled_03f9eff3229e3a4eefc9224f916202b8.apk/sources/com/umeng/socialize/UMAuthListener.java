package com.umeng.socialize;

import com.umeng.socialize.c.b;
import java.util.Map;

public interface UMAuthListener {
    public static final int ACTION_AUTHORIZE = 0;
    public static final int ACTION_DELETE = 1;
    public static final int ACTION_GET_PROFILE = 2;
    public static final UMAuthListener dummy = new d();

    void onCancel(b bVar, int i);

    void onComplete(b bVar, int i, Map<String, String> map);

    void onError(b bVar, int i, Throwable th);
}
