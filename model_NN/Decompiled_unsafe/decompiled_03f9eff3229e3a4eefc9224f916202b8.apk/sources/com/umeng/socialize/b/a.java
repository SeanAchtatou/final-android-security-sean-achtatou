package com.umeng.socialize.b;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Pair;
import com.tencent.connect.common.Constants;
import com.umeng.socialize.Config;
import com.umeng.socialize.PlatformConfig;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMAuthListener;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.a.c;
import com.umeng.socialize.c.b;
import com.umeng.socialize.d.o;
import com.umeng.socialize.d.p;
import com.umeng.socialize.handler.UMSSOHandler;
import com.umeng.socialize.utils.g;
import com.umeng.socialize.view.UMFriendListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* compiled from: SocialRouter */
public final class a {

    /* renamed from: a  reason: collision with root package name */
    private int f2201a = 0;
    private final Map<b, UMSSOHandler> b = new HashMap();
    private final List<Pair<b, String>> c = new ArrayList();
    private C0048a d;
    private Context e;

    public a(Context context) {
        List<Pair<b, String>> list = this.c;
        list.add(new Pair(b.LAIWANG, "com.umeng.socialize.handler.UMLWHandler"));
        list.add(new Pair(b.LAIWANG_DYNAMIC, "com.umeng.socialize.handler.UMLWHandler"));
        list.add(new Pair(b.SINA, "com.umeng.socialize.handler.SinaSsoHandler"));
        list.add(new Pair(b.PINTEREST, "com.umeng.socialize.handler.UMPinterestHandler"));
        list.add(new Pair(b.QZONE, "com.umeng.socialize.handler.QZoneSsoHandler"));
        list.add(new Pair(b.QQ, "com.umeng.socialize.handler.UMQQSsoHandler"));
        list.add(new Pair(b.RENREN, "com.umeng.socialize.handler.RenrenSsoHandler"));
        list.add(new Pair(b.TENCENT, "com.umeng.socialize.handler.QQwbHandler"));
        list.add(new Pair(b.WEIXIN, "com.umeng.socialize.handler.UMWXHandler"));
        list.add(new Pair(b.WEIXIN_CIRCLE, "com.umeng.socialize.handler.UMWXHandler"));
        list.add(new Pair(b.WEIXIN_FAVORITE, "com.umeng.socialize.handler.UMWXHandler"));
        list.add(new Pair(b.YIXIN, "com.umeng.socialize.handler.UMYXHandler"));
        list.add(new Pair(b.YIXIN_CIRCLE, "com.umeng.socialize.handler.UMYXHandler"));
        list.add(new Pair(b.EMAIL, "com.umeng.socialize.handler.EmailHandler"));
        list.add(new Pair(b.EVERNOTE, "com.umeng.socialize.handler.UMEvernoteHandler"));
        list.add(new Pair(b.FACEBOOK, "com.umeng.socialize.handler.UMFacebookHandler"));
        list.add(new Pair(b.FLICKR, "com.umeng.socialize.handler.UMFlickrHandler"));
        list.add(new Pair(b.FOURSQUARE, "com.umeng.socialize.handler.UMFourSquareHandler"));
        list.add(new Pair(b.GOOGLEPLUS, "com.umeng.socialize.handler.UMGooglePlusHandler"));
        list.add(new Pair(b.INSTAGRAM, "com.umeng.socialize.handler.UMInstagramHandler"));
        list.add(new Pair(b.KAKAO, "com.umeng.socialize.handler.UMKakaoHandler"));
        list.add(new Pair(b.LINE, "com.umeng.socialize.handler.UMLineHandler"));
        list.add(new Pair(b.LINKEDIN, "com.umeng.socialize.handler.UMLinkedInHandler"));
        list.add(new Pair(b.POCKET, "com.umeng.socialize.handler.UMPocketHandler"));
        list.add(new Pair(b.WHATSAPP, "com.umeng.socialize.handler.UMWhatsAppHandler"));
        list.add(new Pair(b.YNOTE, "com.umeng.socialize.handler.UMYNoteHandler"));
        list.add(new Pair(b.SMS, "com.umeng.socialize.handler.SmsHandler"));
        list.add(new Pair(b.DOUBAN, "com.umeng.socialize.handler.DoubanHandler"));
        list.add(new Pair(b.TUMBLR, "com.umeng.socialize.handler.UMTumblrHandler"));
        list.add(new Pair(b.TWITTER, "com.umeng.socialize.handler.TwitterHandler"));
        list.add(new Pair(b.ALIPAY, "com.umeng.socialize.handler.AlipayHandler"));
        this.d = new C0048a(this.b);
        this.e = null;
        this.e = context;
        a();
    }

    private void a() {
        UMSSOHandler uMSSOHandler;
        for (Pair next : this.c) {
            if (next.first == b.WEIXIN_CIRCLE || next.first == b.WEIXIN_FAVORITE) {
                uMSSOHandler = this.b.get(b.WEIXIN);
            } else if (next.first == b.YIXIN_CIRCLE) {
                uMSSOHandler = this.b.get(b.YIXIN);
            } else if (next.first == b.LAIWANG_DYNAMIC) {
                uMSSOHandler = this.b.get(b.LAIWANG);
            } else if (next.first != b.TENCENT) {
                uMSSOHandler = a((String) next.second);
            } else if (!Config.WBBYQQ) {
                uMSSOHandler = a("com.umeng.socialize.handler.TencentWBSsoHandler");
            } else {
                uMSSOHandler = a((String) next.second);
            }
            this.b.put(next.first, uMSSOHandler);
        }
    }

    private UMSSOHandler a(String str) {
        try {
            return (UMSSOHandler) Class.forName(str).newInstance();
        } catch (Exception e2) {
            g.d("xxxx", "ignore=" + e2);
            return null;
        }
    }

    public UMSSOHandler a(b bVar) {
        UMSSOHandler uMSSOHandler = this.b.get(bVar);
        if (uMSSOHandler != null) {
            uMSSOHandler.a(this.e, PlatformConfig.getPlatform(bVar));
        }
        return uMSSOHandler;
    }

    public void a(int i, int i2, Intent intent) {
        UMSSOHandler a2 = a(i);
        if (a2 != null) {
            a2.a(i, i2, intent);
        }
    }

    private UMSSOHandler a(int i) {
        int i2;
        int i3 = Constants.REQUEST_QQ_SHARE;
        if (!(i == 10103 || i == 11101)) {
            i3 = i;
        }
        if (i == 64207 || i == 64206) {
            i3 = 64206;
        }
        if (i == 32973 || i == 765) {
            i2 = 5659;
        } else {
            i2 = i3;
        }
        for (UMSSOHandler next : this.b.values()) {
            if (next != null && i2 == next.b()) {
                return next;
            }
        }
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.umeng.socialize.handler.UMSSOHandler.a(android.content.Context, com.umeng.socialize.UMAuthListener):void
     arg types: [android.app.Activity, com.umeng.socialize.UMAuthListener]
     candidates:
      com.umeng.socialize.handler.UMSSOHandler.a(android.app.Activity, com.umeng.socialize.UMAuthListener):void
      com.umeng.socialize.handler.UMSSOHandler.a(android.app.Activity, com.umeng.socialize.view.UMFriendListener):void
      com.umeng.socialize.handler.UMSSOHandler.a(android.content.Context, com.umeng.socialize.PlatformConfig$Platform):void
      com.umeng.socialize.handler.UMSSOHandler.a(android.content.Context, com.umeng.socialize.UMAuthListener):void */
    public void a(Activity activity, b bVar, UMAuthListener uMAuthListener) {
        if (this.d.a(activity, bVar)) {
            if (uMAuthListener == null) {
                uMAuthListener = new b(this);
            }
            this.b.get(bVar).a(activity, PlatformConfig.getPlatform(bVar));
            this.b.get(bVar).a((Context) activity, uMAuthListener);
        }
    }

    public void b(Activity activity, b bVar, UMAuthListener uMAuthListener) {
        if (this.d.a(activity, bVar)) {
            if (uMAuthListener == null) {
                uMAuthListener = new c(this);
            }
            this.b.get(bVar).a(activity, PlatformConfig.getPlatform(bVar));
            this.b.get(bVar).b(activity, uMAuthListener);
        }
    }

    public void a(Activity activity, b bVar, UMFriendListener uMFriendListener) {
        if (this.d.a(activity, bVar)) {
            if (uMFriendListener == null) {
                uMFriendListener = new d(this);
            }
            this.b.get(bVar).a(activity, PlatformConfig.getPlatform(bVar));
            this.b.get(bVar).a(activity, uMFriendListener);
        }
    }

    public boolean a(Activity activity, b bVar) {
        if (!this.d.a(activity, bVar)) {
            return false;
        }
        this.b.get(bVar).a(activity, PlatformConfig.getPlatform(bVar));
        return this.b.get(bVar).a(activity);
    }

    public boolean b(Activity activity, b bVar) {
        if (!this.d.a(activity, bVar)) {
            return false;
        }
        this.b.get(bVar).a(activity, PlatformConfig.getPlatform(bVar));
        return this.b.get(bVar).b(activity);
    }

    public void c(Activity activity, b bVar, UMAuthListener uMAuthListener) {
        if (this.d.a(activity, bVar)) {
            UMSSOHandler uMSSOHandler = this.b.get(bVar);
            uMSSOHandler.a(activity, PlatformConfig.getPlatform(bVar));
            uMSSOHandler.a(activity, uMAuthListener);
        }
    }

    public void a(Activity activity, ShareAction shareAction, UMShareListener uMShareListener) {
        if (this.d.a(activity, shareAction)) {
            if (uMShareListener == null) {
                uMShareListener = new e(this);
            }
            b platform = shareAction.getPlatform();
            UMSSOHandler uMSSOHandler = this.b.get(platform);
            uMSSOHandler.a(shareAction.getFrom());
            uMSSOHandler.a(activity, PlatformConfig.getPlatform(platform));
            if (!platform.toString().equals("TENCENT") && !platform.toString().equals("RENREN") && !platform.toString().equals("DOUBAN")) {
                if (platform.toString().equals("WEIXIN")) {
                    c.a(activity, "wxsession", shareAction.getShareContent().mText, shareAction.getShareContent().mMedia);
                } else if (platform.toString().equals("WEIXIN_CIRCLE")) {
                    c.a(activity, "wxtimeline", shareAction.getShareContent().mText, shareAction.getShareContent().mMedia);
                } else if (platform.toString().equals("WEIXIN_FAVORITE")) {
                    c.a(activity, "wxfavorite", shareAction.getShareContent().mText, shareAction.getShareContent().mMedia);
                } else {
                    c.a(activity, platform.toString().toLowerCase(), shareAction.getShareContent().mText, shareAction.getShareContent().mMedia);
                }
            }
            if (platform.toString().equals("TENCENT") && Config.WBBYQQ) {
                c.a(activity, platform.toString().toLowerCase(), shareAction.getShareContent().mText, shareAction.getShareContent().mMedia);
            }
            if (Config.isloadUrl) {
                a(activity, shareAction);
            }
            uMSSOHandler.a(activity, shareAction.getShareContent(), uMShareListener);
        }
    }

    private void a(Activity activity, ShareAction shareAction) {
        String lowerCase;
        String str = shareAction.getShareContent().mTargetUrl;
        if (!TextUtils.isEmpty(str)) {
            if (shareAction.getPlatform().toString().equals("WEIXIN")) {
                lowerCase = "wxsession";
            } else if (shareAction.getPlatform().toString().equals("")) {
                lowerCase = "wxtimeline";
            } else {
                lowerCase = shareAction.getPlatform().toString().toLowerCase();
            }
            p a2 = com.umeng.socialize.d.g.a(new o(activity, lowerCase, str));
            g.b("xxxxxx resp" + a2);
            if (a2 == null || a2.l != 200) {
                g.b("upload url fail ");
            } else {
                shareAction.withTargetUrl(a2.f2239a);
            }
        }
    }

    /* renamed from: com.umeng.socialize.b.a$a  reason: collision with other inner class name */
    /* compiled from: SocialRouter */
    static class C0048a {

        /* renamed from: a  reason: collision with root package name */
        private Map<b, UMSSOHandler> f2202a;

        public C0048a(Map<b, UMSSOHandler> map) {
            this.f2202a = map;
        }

        public boolean a(Context context, b bVar) {
            if (!a(context)) {
                return false;
            }
            if (!a(bVar)) {
                return false;
            }
            if (this.f2202a.get(bVar).a()) {
                return true;
            }
            g.e(bVar.toString() + "平台不支持授权,无法完成操作");
            return false;
        }

        public boolean a(Activity activity, ShareAction shareAction) {
            b platform;
            if (a(activity) && (platform = shareAction.getPlatform()) != null && a(platform)) {
                return true;
            }
            return false;
        }

        private boolean a(Context context) {
            if (context != null) {
                return true;
            }
            g.b("Context is null");
            return false;
        }

        private boolean a(b bVar) {
            PlatformConfig.Platform platform = PlatformConfig.configs.get(bVar);
            if (platform != null && !platform.isConfigured()) {
                g.b(bVar + ": 没有配置相关的Appkey、Secret");
                return false;
            } else if (this.f2202a.get(bVar) != null) {
                return true;
            } else {
                g.b("没有配置 " + bVar + " 的jar包");
                return false;
            }
        }
    }
}
