package com.umeng.socialize.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.res.Resources;
import android.net.Uri;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;
import com.umeng.socialize.common.g;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;
import org.json.JSONObject;

/* compiled from: SocializeUtils */
public class h {

    /* renamed from: a  reason: collision with root package name */
    public static Set<Uri> f2319a = new HashSet();
    private static Pattern b = null;
    private static int c = 0;

    public static String a(Context context) {
        String str = com.umeng.socialize.common.h.g;
        if (!TextUtils.isEmpty(str)) {
            return str;
        }
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            if (applicationInfo == null) {
                return str;
            }
            Object obj = applicationInfo.metaData.get("UMENG_APPKEY");
            if (obj != null) {
                return obj.toString();
            }
            g.a("com.umeng.socialize", "Could not read UMENG_APPKEY meta-data from AndroidManifest.xml.");
            return str;
        } catch (Exception e) {
            g.a("com.umeng.socialize", "Could not read UMENG_APPKEY meta-data from AndroidManifest.xml.", e);
            return str;
        }
    }

    public static void a(Dialog dialog) {
        Activity ownerActivity;
        if (dialog != null) {
            try {
                if (dialog.isShowing() && (ownerActivity = dialog.getOwnerActivity()) != null && !ownerActivity.isFinishing()) {
                    dialog.dismiss();
                }
            } catch (WindowManager.BadTokenException e) {
                g.b("SocializeUtils", "dialog dismiss error", e);
            }
        }
    }

    public static void b(Dialog dialog) {
        Activity ownerActivity;
        if (dialog != null) {
            try {
                if (!dialog.isShowing() && (ownerActivity = dialog.getOwnerActivity()) != null && !ownerActivity.isFinishing()) {
                    dialog.show();
                }
            } catch (WindowManager.BadTokenException e) {
                g.b("SocializeUtils", "dialog show error", e);
            }
        }
    }

    public static int a(String str) {
        String trim = str.trim();
        int i = 0;
        while (a().matcher(trim).find()) {
            i++;
        }
        int length = trim.length() - i;
        if (length % 2 != 0) {
            return i + ((length + 1) / 2);
        }
        return i + (length / 2);
    }

    private static Pattern a() {
        if (b == null) {
            b = Pattern.compile("[^\\x00-\\xff]");
        }
        return b;
    }

    public static int[] b(Context context) {
        g a2 = g.a(context);
        Resources resources = context.getResources();
        return new int[]{(int) resources.getDimension(a2.f("umeng_socialize_pad_window_width")), (int) resources.getDimension(a2.f("umeng_socialize_pad_window_height"))};
    }

    public static boolean c(Context context) {
        if (com.umeng.socialize.common.h.c) {
            if (c == 0) {
                WindowManager windowManager = (WindowManager) context.getSystemService("window");
                Display defaultDisplay = windowManager.getDefaultDisplay();
                int width = defaultDisplay.getWidth();
                int height = defaultDisplay.getHeight();
                if (width <= height) {
                    height = width;
                }
                DisplayMetrics displayMetrics = new DisplayMetrics();
                windowManager.getDefaultDisplay().getMetrics(displayMetrics);
                c = (int) ((((float) height) / displayMetrics.density) + 0.5f);
            }
            if ((context.getResources().getConfiguration().screenLayout & 15) < 3 || c < 550) {
                return false;
            }
            return true;
        }
        return false;
    }

    public static Map<String, String> b(String str) {
        HashMap hashMap = new HashMap();
        try {
            JSONObject jSONObject = new JSONObject(str);
            Iterator<String> keys = jSONObject.keys();
            while (keys.hasNext()) {
                String next = keys.next();
                hashMap.put(next, jSONObject.get(next) + "");
            }
        } catch (Exception e) {
            g.b("weixin", "jsontomap fail=" + e);
        }
        return hashMap;
    }
}
