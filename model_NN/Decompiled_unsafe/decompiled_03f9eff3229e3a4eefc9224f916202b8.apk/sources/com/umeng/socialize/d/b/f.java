package com.umeng.socialize.d.b;

import android.text.TextUtils;
import com.umeng.socialize.utils.g;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.HttpURLConnection;
import java.util.Map;
import org.apache.mina.proxy.handlers.http.HttpProxyConstants;
import org.json.JSONObject;

/* compiled from: UClient */
public class f {

    /* renamed from: a  reason: collision with root package name */
    private Map<String, String> f2232a;
    private StringBuilder b;

    public <T extends h> T a(g gVar, Class cls) {
        JSONObject jSONObject;
        gVar.a();
        String trim = gVar.g().trim();
        a(trim);
        this.b = new StringBuilder();
        if (g.e.equals(trim)) {
            jSONObject = a(gVar);
        } else if (g.d.equals(trim)) {
            jSONObject = a(gVar.f, gVar);
        } else {
            jSONObject = null;
        }
        if (jSONObject == null) {
            return null;
        }
        try {
            return (h) cls.getConstructor(JSONObject.class).newInstance(jSONObject);
        } catch (SecurityException e) {
            g.b("UClient", "SecurityException", e);
        } catch (NoSuchMethodException e2) {
            g.b("UClient", "NoSuchMethodException", e2);
        } catch (IllegalArgumentException e3) {
            g.b("UClient", "IllegalArgumentException", e3);
        } catch (InstantiationException e4) {
            g.b("UClient", "InstantiationException", e4);
        } catch (IllegalAccessException e5) {
            g.b("UClient", "IllegalAccessException", e5);
        } catch (InvocationTargetException e6) {
            g.b("UClient", "InvocationTargetException", e6);
        }
        return null;
    }

    /* JADX WARN: Type inference failed for: r1v18, types: [java.net.URLConnection] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x013c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private org.json.JSONObject a(java.lang.String r14, com.umeng.socialize.d.b.g r15) {
        /*
            r13 = this;
            r8 = 0
            r10 = 1
            org.json.JSONObject r1 = r15.e()
            if (r1 != 0) goto L_0x00dc
            java.lang.String r1 = ""
            r2 = r1
        L_0x000b:
            java.util.Random r1 = new java.util.Random
            r1.<init>()
            r3 = 1000(0x3e8, float:1.401E-42)
            int r1 = r1.nextInt(r3)
            java.lang.String r3 = "line.separator"
            java.lang.String r3 = java.lang.System.getProperty(r3)
            java.lang.String r4 = "UClient"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.StringBuilder r1 = r5.append(r1)
            java.lang.String r5 = ":\trequest: "
            java.lang.StringBuilder r1 = r1.append(r5)
            java.lang.StringBuilder r1 = r1.append(r14)
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            com.umeng.socialize.utils.g.a(r4, r1)
            java.util.UUID r1 = java.util.UUID.randomUUID()
            java.lang.String r5 = r1.toString()
            java.lang.String r1 = "xxxxx"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "url="
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r3 = r3.append(r14)
            java.lang.String r3 = r3.toString()
            com.umeng.socialize.utils.g.b(r1, r3)
            r3 = 0
            java.net.URL r1 = new java.net.URL     // Catch:{ IOException -> 0x018c, all -> 0x0189 }
            r1.<init>(r14)     // Catch:{ IOException -> 0x018c, all -> 0x0189 }
            java.net.URLConnection r1 = r1.openConnection()     // Catch:{ IOException -> 0x018c, all -> 0x0189 }
            r0 = r1
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ IOException -> 0x018c, all -> 0x0189 }
            r7 = r0
            int r1 = com.umeng.socialize.Config.connectionTimeOut     // Catch:{ IOException -> 0x00d4, all -> 0x0139 }
            r7.setConnectTimeout(r1)     // Catch:{ IOException -> 0x00d4, all -> 0x0139 }
            int r1 = com.umeng.socialize.Config.readSocketTimeOut     // Catch:{ IOException -> 0x00d4, all -> 0x0139 }
            r7.setReadTimeout(r1)     // Catch:{ IOException -> 0x00d4, all -> 0x0139 }
            java.lang.String r1 = "POST"
            r7.setRequestMethod(r1)     // Catch:{ IOException -> 0x00d4, all -> 0x0139 }
            r1 = 1
            r7.setDoOutput(r1)     // Catch:{ IOException -> 0x00d4, all -> 0x0139 }
            r1 = 1
            r7.setDoInput(r1)     // Catch:{ IOException -> 0x00d4, all -> 0x0139 }
            java.lang.String r1 = "Content-Type"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00d4, all -> 0x0139 }
            r4.<init>()     // Catch:{ IOException -> 0x00d4, all -> 0x0139 }
            java.lang.String r6 = "multipart/form-data; boundary="
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ IOException -> 0x00d4, all -> 0x0139 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ IOException -> 0x00d4, all -> 0x0139 }
            java.lang.String r4 = r4.toString()     // Catch:{ IOException -> 0x00d4, all -> 0x0139 }
            r7.setRequestProperty(r1, r4)     // Catch:{ IOException -> 0x00d4, all -> 0x0139 }
            java.util.Map r4 = r15.d()     // Catch:{ IOException -> 0x00d4, all -> 0x0139 }
            if (r4 == 0) goto L_0x014f
            int r1 = r4.size()     // Catch:{ IOException -> 0x00d4, all -> 0x0139 }
            if (r1 <= 0) goto L_0x014f
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00d4, all -> 0x0139 }
            r2.<init>()     // Catch:{ IOException -> 0x00d4, all -> 0x0139 }
            java.util.Set r1 = r4.keySet()     // Catch:{ IOException -> 0x00d4, all -> 0x0139 }
            java.util.Iterator r6 = r1.iterator()     // Catch:{ IOException -> 0x00d4, all -> 0x0139 }
        L_0x00b6:
            boolean r1 = r6.hasNext()     // Catch:{ IOException -> 0x00d4, all -> 0x0139 }
            if (r1 == 0) goto L_0x00e7
            java.lang.Object r1 = r6.next()     // Catch:{ IOException -> 0x00d4, all -> 0x0139 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ IOException -> 0x00d4, all -> 0x0139 }
            java.lang.Object r9 = r4.get(r1)     // Catch:{ IOException -> 0x00d4, all -> 0x0139 }
            if (r9 == 0) goto L_0x00b6
            java.lang.Object r9 = r4.get(r1)     // Catch:{ IOException -> 0x00d4, all -> 0x0139 }
            java.lang.String r9 = r9.toString()     // Catch:{ IOException -> 0x00d4, all -> 0x0139 }
            r13.a(r2, r1, r9, r5)     // Catch:{ IOException -> 0x00d4, all -> 0x0139 }
            goto L_0x00b6
        L_0x00d4:
            r1 = move-exception
        L_0x00d5:
            if (r7 == 0) goto L_0x00da
            r7.disconnect()
        L_0x00da:
            r1 = r8
        L_0x00db:
            return r1
        L_0x00dc:
            org.json.JSONObject r1 = r15.e()
            java.lang.String r1 = r1.toString()
            r2 = r1
            goto L_0x000b
        L_0x00e7:
            int r1 = r2.length()     // Catch:{ IOException -> 0x00d4, all -> 0x0139 }
            if (r1 <= 0) goto L_0x0190
            java.io.DataOutputStream r6 = new java.io.DataOutputStream     // Catch:{ IOException -> 0x00d4, all -> 0x0139 }
            java.io.OutputStream r1 = r7.getOutputStream()     // Catch:{ IOException -> 0x00d4, all -> 0x0139 }
            r6.<init>(r1)     // Catch:{ IOException -> 0x00d4, all -> 0x0139 }
            java.lang.String r1 = r2.toString()     // Catch:{ IOException -> 0x00d4, all -> 0x0139 }
            byte[] r1 = r1.getBytes()     // Catch:{ IOException -> 0x00d4, all -> 0x0139 }
            r6.write(r1)     // Catch:{ IOException -> 0x00d4, all -> 0x0139 }
            r9 = r10
        L_0x0102:
            java.util.Map r11 = r15.c()     // Catch:{ IOException -> 0x00d4, all -> 0x0139 }
            if (r11 == 0) goto L_0x0140
            int r1 = r11.size()     // Catch:{ IOException -> 0x00d4, all -> 0x0139 }
            if (r1 <= 0) goto L_0x0140
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00d4, all -> 0x0139 }
            r2.<init>()     // Catch:{ IOException -> 0x00d4, all -> 0x0139 }
            java.util.Set r1 = r11.keySet()     // Catch:{ IOException -> 0x00d4, all -> 0x0139 }
            java.util.Iterator r12 = r1.iterator()     // Catch:{ IOException -> 0x00d4, all -> 0x0139 }
        L_0x011b:
            boolean r1 = r12.hasNext()     // Catch:{ IOException -> 0x00d4, all -> 0x0139 }
            if (r1 == 0) goto L_0x0140
            java.lang.Object r3 = r12.next()     // Catch:{ IOException -> 0x00d4, all -> 0x0139 }
            java.lang.String r3 = (java.lang.String) r3     // Catch:{ IOException -> 0x00d4, all -> 0x0139 }
            java.lang.Object r1 = r11.get(r3)     // Catch:{ IOException -> 0x00d4, all -> 0x0139 }
            com.umeng.socialize.d.b.g$a r1 = (com.umeng.socialize.d.b.g.a) r1     // Catch:{ IOException -> 0x00d4, all -> 0x0139 }
            byte[] r4 = r1.b     // Catch:{ IOException -> 0x00d4, all -> 0x0139 }
            if (r4 == 0) goto L_0x011b
            int r1 = r4.length     // Catch:{ IOException -> 0x00d4, all -> 0x0139 }
            if (r1 < r10) goto L_0x011b
            r1 = r13
            r1.a(r2, r3, r4, r5, r6)     // Catch:{ IOException -> 0x00d4, all -> 0x0139 }
            goto L_0x011b
        L_0x0139:
            r1 = move-exception
        L_0x013a:
            if (r7 == 0) goto L_0x013f
            r7.disconnect()
        L_0x013f:
            throw r1
        L_0x0140:
            if (r9 == 0) goto L_0x0145
            r13.a(r6, r5)     // Catch:{ IOException -> 0x00d4, all -> 0x0139 }
        L_0x0145:
            org.json.JSONObject r1 = r13.a(r7)     // Catch:{ IOException -> 0x00d4, all -> 0x0139 }
            if (r7 == 0) goto L_0x00db
            r7.disconnect()
            goto L_0x00db
        L_0x014f:
            java.lang.String r1 = "Content-Type"
            java.lang.String r3 = "application/x-www-form-urlencoded"
            r7.setRequestProperty(r1, r3)     // Catch:{ IOException -> 0x00d4, all -> 0x0139 }
            android.net.Uri$Builder r1 = new android.net.Uri$Builder     // Catch:{ IOException -> 0x00d4, all -> 0x0139 }
            r1.<init>()     // Catch:{ IOException -> 0x00d4, all -> 0x0139 }
            java.lang.String r3 = "content"
            r1.appendQueryParameter(r3, r2)     // Catch:{ IOException -> 0x00d4, all -> 0x0139 }
            android.net.Uri r1 = r1.build()     // Catch:{ IOException -> 0x00d4, all -> 0x0139 }
            java.lang.String r1 = r1.getEncodedQuery()     // Catch:{ IOException -> 0x00d4, all -> 0x0139 }
            java.io.DataOutputStream r2 = new java.io.DataOutputStream     // Catch:{ IOException -> 0x00d4, all -> 0x0139 }
            java.io.OutputStream r3 = r7.getOutputStream()     // Catch:{ IOException -> 0x00d4, all -> 0x0139 }
            r2.<init>(r3)     // Catch:{ IOException -> 0x00d4, all -> 0x0139 }
            byte[] r1 = r1.getBytes()     // Catch:{ IOException -> 0x00d4, all -> 0x0139 }
            r2.write(r1)     // Catch:{ IOException -> 0x00d4, all -> 0x0139 }
            r2.flush()     // Catch:{ IOException -> 0x00d4, all -> 0x0139 }
            r2.close()     // Catch:{ IOException -> 0x00d4, all -> 0x0139 }
            org.json.JSONObject r1 = r13.a(r7)     // Catch:{ IOException -> 0x00d4, all -> 0x0139 }
            if (r7 == 0) goto L_0x00db
            r7.disconnect()
            goto L_0x00db
        L_0x0189:
            r1 = move-exception
            r7 = r8
            goto L_0x013a
        L_0x018c:
            r1 = move-exception
            r7 = r8
            goto L_0x00d5
        L_0x0190:
            r9 = r3
            r6 = r8
            goto L_0x0102
        */
        throw new UnsupportedOperationException("Method not decompiled: com.umeng.socialize.d.b.f.a(java.lang.String, com.umeng.socialize.d.b.g):org.json.JSONObject");
    }

    private JSONObject a(HttpURLConnection httpURLConnection) throws IOException {
        if (httpURLConnection.getResponseCode() != 200) {
            return null;
        }
        String a2 = a(new BufferedInputStream(httpURLConnection.getInputStream()));
        try {
            return new JSONObject(a2);
        } catch (Exception e) {
            try {
                return new JSONObject(a.b(a2, "UTF-8").trim());
            } catch (Exception e2) {
                return null;
            }
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r0v10, types: [org.json.JSONObject] */
    /* JADX WARN: Type inference failed for: r0v16, types: [org.json.JSONObject] */
    /* JADX WARN: Type inference failed for: r1v16 */
    /* JADX WARN: Type inference failed for: r1v18, types: [java.io.InputStream] */
    /* JADX WARN: Type inference failed for: r0v18, types: [org.json.JSONObject] */
    /* JADX WARN: Type inference failed for: r0v22, types: [org.json.JSONObject] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00a6  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x00fa  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private org.json.JSONObject a(com.umeng.socialize.d.b.g r8) {
        /*
            r7 = this;
            r3 = 0
            java.util.Random r0 = new java.util.Random
            r0.<init>()
            r1 = 1000(0x3e8, float:1.401E-42)
            int r0 = r0.nextInt(r1)
            java.lang.String r1 = r8.f()
            int r1 = r1.length()     // Catch:{ Exception -> 0x0118, all -> 0x0113 }
            r2 = 1
            if (r1 > r2) goto L_0x0036
            java.lang.String r1 = "UClient"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0118, all -> 0x0113 }
            r2.<init>()     // Catch:{ Exception -> 0x0118, all -> 0x0113 }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ Exception -> 0x0118, all -> 0x0113 }
            java.lang.String r2 = ":\tInvalid baseUrl."
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x0118, all -> 0x0113 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0118, all -> 0x0113 }
            com.umeng.socialize.utils.g.b(r1, r0)     // Catch:{ Exception -> 0x0118, all -> 0x0113 }
            if (r3 == 0) goto L_0x0034
            r3.disconnect()
        L_0x0034:
            r0 = r3
        L_0x0035:
            return r0
        L_0x0036:
            java.net.URL r0 = new java.net.URL     // Catch:{ Exception -> 0x0118, all -> 0x0113 }
            java.lang.String r1 = r8.f()     // Catch:{ Exception -> 0x0118, all -> 0x0113 }
            r0.<init>(r1)     // Catch:{ Exception -> 0x0118, all -> 0x0113 }
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ Exception -> 0x0118, all -> 0x0113 }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x0118, all -> 0x0113 }
            java.lang.String r1 = "Accept-Encoding"
            java.lang.String r2 = "gzip"
            r0.setRequestProperty(r1, r2)     // Catch:{ Exception -> 0x0084, all -> 0x00f5 }
            int r1 = com.umeng.socialize.Config.connectionTimeOut     // Catch:{ Exception -> 0x0084, all -> 0x00f5 }
            r0.setConnectTimeout(r1)     // Catch:{ Exception -> 0x0084, all -> 0x00f5 }
            int r1 = com.umeng.socialize.Config.readSocketTimeOut     // Catch:{ Exception -> 0x0084, all -> 0x00f5 }
            r0.setReadTimeout(r1)     // Catch:{ Exception -> 0x0084, all -> 0x00f5 }
            java.util.Map<java.lang.String, java.lang.String> r1 = r7.f2232a     // Catch:{ Exception -> 0x0084, all -> 0x00f5 }
            if (r1 == 0) goto L_0x00ab
            java.util.Map<java.lang.String, java.lang.String> r1 = r7.f2232a     // Catch:{ Exception -> 0x0084, all -> 0x00f5 }
            int r1 = r1.size()     // Catch:{ Exception -> 0x0084, all -> 0x00f5 }
            if (r1 <= 0) goto L_0x00ab
            java.util.Map<java.lang.String, java.lang.String> r1 = r7.f2232a     // Catch:{ Exception -> 0x0084, all -> 0x00f5 }
            java.util.Set r1 = r1.keySet()     // Catch:{ Exception -> 0x0084, all -> 0x00f5 }
            java.util.Iterator r4 = r1.iterator()     // Catch:{ Exception -> 0x0084, all -> 0x00f5 }
        L_0x006c:
            boolean r1 = r4.hasNext()     // Catch:{ Exception -> 0x0084, all -> 0x00f5 }
            if (r1 == 0) goto L_0x00ab
            java.lang.Object r1 = r4.next()     // Catch:{ Exception -> 0x0084, all -> 0x00f5 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ Exception -> 0x0084, all -> 0x00f5 }
            java.util.Map<java.lang.String, java.lang.String> r2 = r7.f2232a     // Catch:{ Exception -> 0x0084, all -> 0x00f5 }
            java.lang.Object r2 = r2.get(r1)     // Catch:{ Exception -> 0x0084, all -> 0x00f5 }
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ Exception -> 0x0084, all -> 0x00f5 }
            r0.setRequestProperty(r1, r2)     // Catch:{ Exception -> 0x0084, all -> 0x00f5 }
            goto L_0x006c
        L_0x0084:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
        L_0x0088:
            java.lang.String r2 = "UMhttprequest"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0115 }
            r4.<init>()     // Catch:{ all -> 0x0115 }
            java.lang.String r5 = "error:"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0115 }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x0115 }
            java.lang.StringBuilder r0 = r4.append(r0)     // Catch:{ all -> 0x0115 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0115 }
            com.umeng.socialize.utils.g.b(r2, r0)     // Catch:{ all -> 0x0115 }
            if (r1 == 0) goto L_0x00a9
            r1.disconnect()
        L_0x00a9:
            r0 = r3
            goto L_0x0035
        L_0x00ab:
            r0.connect()     // Catch:{ Exception -> 0x0084, all -> 0x00f5 }
            int r1 = r0.getResponseCode()     // Catch:{ Exception -> 0x0084, all -> 0x00f5 }
            r2 = 200(0xc8, float:2.8E-43)
            if (r1 != r2) goto L_0x010b
            java.lang.String r1 = r0.getContentEncoding()     // Catch:{ Exception -> 0x0084, all -> 0x00f5 }
            java.lang.String r2 = "gzip"
            boolean r2 = r1.equalsIgnoreCase(r2)     // Catch:{ Exception -> 0x0084, all -> 0x00f5 }
            if (r2 == 0) goto L_0x00e3
            java.util.zip.GZIPInputStream r1 = new java.util.zip.GZIPInputStream     // Catch:{ Exception -> 0x0084, all -> 0x00f5 }
            java.io.InputStream r2 = r0.getInputStream()     // Catch:{ Exception -> 0x0084, all -> 0x00f5 }
            r1.<init>(r2)     // Catch:{ Exception -> 0x0084, all -> 0x00f5 }
        L_0x00cb:
            java.lang.String r1 = a(r1)     // Catch:{ Exception -> 0x0084, all -> 0x00f5 }
            java.lang.String r2 = "UTF-8"
            java.lang.String r1 = com.umeng.socialize.d.b.a.b(r1, r2)     // Catch:{ Exception -> 0x0084, all -> 0x00f5 }
            java.lang.String r2 = r1.trim()     // Catch:{ Exception -> 0x0084, all -> 0x00f5 }
            if (r2 != 0) goto L_0x00fe
            if (r0 == 0) goto L_0x00e0
            r0.disconnect()
        L_0x00e0:
            r0 = r3
            goto L_0x0035
        L_0x00e3:
            java.lang.String r2 = "deflate"
            boolean r1 = r1.equalsIgnoreCase(r2)     // Catch:{ Exception -> 0x0084, all -> 0x00f5 }
            if (r1 == 0) goto L_0x011c
            java.util.zip.InflaterInputStream r1 = new java.util.zip.InflaterInputStream     // Catch:{ Exception -> 0x0084, all -> 0x00f5 }
            java.io.InputStream r2 = r0.getInputStream()     // Catch:{ Exception -> 0x0084, all -> 0x00f5 }
            r1.<init>(r2)     // Catch:{ Exception -> 0x0084, all -> 0x00f5 }
            goto L_0x00cb
        L_0x00f5:
            r1 = move-exception
            r3 = r0
            r0 = r1
        L_0x00f8:
            if (r3 == 0) goto L_0x00fd
            r3.disconnect()
        L_0x00fd:
            throw r0
        L_0x00fe:
            org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ Exception -> 0x0084, all -> 0x00f5 }
            r1.<init>(r2)     // Catch:{ Exception -> 0x0084, all -> 0x00f5 }
            if (r0 == 0) goto L_0x0108
            r0.disconnect()
        L_0x0108:
            r0 = r1
            goto L_0x0035
        L_0x010b:
            if (r0 == 0) goto L_0x0110
            r0.disconnect()
        L_0x0110:
            r0 = r3
            goto L_0x0035
        L_0x0113:
            r0 = move-exception
            goto L_0x00f8
        L_0x0115:
            r0 = move-exception
            r3 = r1
            goto L_0x00f8
        L_0x0118:
            r0 = move-exception
            r1 = r3
            goto L_0x0088
        L_0x011c:
            r1 = r3
            goto L_0x00cb
        */
        throw new UnsupportedOperationException("Method not decompiled: com.umeng.socialize.d.b.f.a(com.umeng.socialize.d.b.g):org.json.JSONObject");
    }

    private void a(String str) {
        if (TextUtils.isEmpty(str) || !(g.e.equals(str.trim()) ^ g.d.equals(str.trim()))) {
            throw new RuntimeException("验证请求方式失败[" + str + "]");
        }
    }

    private static String a(InputStream inputStream) {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream), 8192);
        StringBuilder sb = new StringBuilder();
        while (true) {
            try {
                String readLine = bufferedReader.readLine();
                if (readLine != null) {
                    sb.append(readLine + "\n");
                } else {
                    try {
                        inputStream.close();
                        return sb.toString();
                    } catch (IOException e) {
                        g.b("UClient", "Caught IOException in convertStreamToString()", e);
                        return null;
                    }
                }
            } catch (IOException e2) {
                g.b("UClient", "Caught IOException in convertStreamToString()", e2);
                try {
                    inputStream.close();
                    return null;
                } catch (IOException e3) {
                    g.b("UClient", "Caught IOException in convertStreamToString()", e3);
                    return null;
                }
            } catch (Throwable th) {
                try {
                    inputStream.close();
                    throw th;
                } catch (IOException e4) {
                    g.b("UClient", "Caught IOException in convertStreamToString()", e4);
                    return null;
                }
            }
        }
    }

    private void a(StringBuilder sb, String str, String str2, String str3) {
        sb.append("--").append(str3).append(HttpProxyConstants.CRLF).append("Content-Disposition: form-data; name=\"").append(str).append("\"").append(HttpProxyConstants.CRLF).append("Content-Type: text/plain; charset=").append("UTF-8").append(HttpProxyConstants.CRLF).append(HttpProxyConstants.CRLF).append(str2).append(HttpProxyConstants.CRLF);
    }

    private void a(StringBuilder sb, String str, byte[] bArr, String str2, OutputStream outputStream) throws IOException {
        sb.append("--").append(str2).append(HttpProxyConstants.CRLF).append("Content-Disposition: form-data; name=\"").append(str).append("\"; filename=\"").append(str).append("\"").append(HttpProxyConstants.CRLF).append("Content-Type: ").append("application/octet-stream").append(HttpProxyConstants.CRLF).append("Content-Transfer-Encoding: binary").append(HttpProxyConstants.CRLF).append(HttpProxyConstants.CRLF);
        outputStream.write(sb.toString().getBytes());
        outputStream.write(bArr);
        outputStream.write(HttpProxyConstants.CRLF.getBytes());
    }

    private void a(OutputStream outputStream, String str) throws IOException {
        outputStream.write(HttpProxyConstants.CRLF.getBytes());
        outputStream.write(("--" + str + "--").getBytes());
        outputStream.write(HttpProxyConstants.CRLF.getBytes());
        outputStream.flush();
        outputStream.close();
    }
}
