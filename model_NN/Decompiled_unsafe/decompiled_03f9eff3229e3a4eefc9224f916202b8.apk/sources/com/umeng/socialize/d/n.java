package com.umeng.socialize.d;

import com.umeng.socialize.d.a.f;
import org.json.JSONObject;

/* compiled from: UploadImageResponse */
public class n extends f {

    /* renamed from: a  reason: collision with root package name */
    public String f2238a;
    public String b;

    public n(JSONObject jSONObject) {
        super(jSONObject);
    }

    public void a() {
        super.a();
        JSONObject jSONObject = this.j;
        if (jSONObject != null) {
            this.f2238a = jSONObject.optString("large_url");
            this.b = jSONObject.optString("small_url");
        }
    }
}
