package com.umeng.socialize.d;

import android.content.Context;
import com.umeng.socialize.d.a.b;

/* compiled from: ShareFriendsRequest */
public class h extends b {
    private String g;
    private com.umeng.socialize.c.b h;

    public h(Context context, com.umeng.socialize.c.b bVar, String str) {
        super(context, "", i.class, 14, b.C0050b.GET);
        this.b = context;
        this.g = str;
        this.h = bVar;
    }

    public void a() {
        a("to", this.h.toString().toLowerCase());
    }

    /* access modifiers changed from: protected */
    public String b() {
        return "/share/friends/" + com.umeng.socialize.utils.h.a(this.b) + "/" + this.g + "/";
    }
}
