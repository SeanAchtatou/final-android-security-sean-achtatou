package com.umeng.socialize.d.b;

import android.support.v4.view.MotionEventCompat;
import org.apache.mina.proxy.handlers.socks.SocksProxyConstants;

/* compiled from: Base64 */
public class b extends c {

    /* renamed from: a  reason: collision with root package name */
    static final byte[] f2229a = {13, 10};
    private static final byte[] h = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, SocksProxyConstants.V4_REPLY_REQUEST_GRANTED, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 43, 47};
    private static final byte[] i = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, SocksProxyConstants.V4_REPLY_REQUEST_GRANTED, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 45, 95};
    private static final byte[] j = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 62, -1, 62, -1, 63, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -1, -1, -1, -1, -1, -1, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -1, -1, -1, -1, 63, -1, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51};
    private final byte[] k;
    private final byte[] l;
    private final byte[] m;
    private final int n;
    private final int o;
    private int p;

    public b() {
        this(0);
    }

    public b(boolean z) {
        this(76, f2229a, z);
    }

    public b(int i2) {
        this(i2, f2229a);
    }

    public b(int i2, byte[] bArr) {
        this(i2, bArr, false);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public b(int r6, byte[] r7, boolean r8) {
        /*
            r5 = this;
            r4 = 0
            r3 = 4
            r1 = 0
            r2 = 3
            if (r7 != 0) goto L_0x0039
            r0 = r1
        L_0x0007:
            r5.<init>(r2, r3, r6, r0)
            byte[] r0 = com.umeng.socialize.d.b.b.j
            r5.l = r0
            if (r7 == 0) goto L_0x005f
            boolean r0 = r5.d(r7)
            if (r0 == 0) goto L_0x003b
            java.lang.String r0 = com.umeng.socialize.d.b.a.a(r7)
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "lineSeparator must not contain base64 characters: ["
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r2 = "]"
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
            r1.<init>(r0)
            throw r1
        L_0x0039:
            int r0 = r7.length
            goto L_0x0007
        L_0x003b:
            if (r6 <= 0) goto L_0x005a
            int r0 = r7.length
            int r0 = r0 + 4
            r5.o = r0
            int r0 = r7.length
            byte[] r0 = new byte[r0]
            r5.m = r0
            byte[] r0 = r5.m
            int r2 = r7.length
            java.lang.System.arraycopy(r7, r1, r0, r1, r2)
        L_0x004d:
            int r0 = r5.o
            int r0 = r0 + -1
            r5.n = r0
            if (r8 == 0) goto L_0x0064
            byte[] r0 = com.umeng.socialize.d.b.b.i
        L_0x0057:
            r5.k = r0
            return
        L_0x005a:
            r5.o = r3
            r5.m = r4
            goto L_0x004d
        L_0x005f:
            r5.o = r3
            r5.m = r4
            goto L_0x004d
        L_0x0064:
            byte[] r0 = com.umeng.socialize.d.b.b.h
            goto L_0x0057
        */
        throw new UnsupportedOperationException("Method not decompiled: com.umeng.socialize.d.b.b.<init>(int, byte[], boolean):void");
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v6, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v34, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v35, resolved type: byte} */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(byte[] r8, int r9, int r10) {
        /*
            r7 = this;
            r6 = 61
            r2 = 0
            boolean r0 = r7.e
            if (r0 == 0) goto L_0x0008
        L_0x0007:
            return
        L_0x0008:
            if (r10 >= 0) goto L_0x00d8
            r0 = 1
            r7.e = r0
            int r0 = r7.g
            if (r0 != 0) goto L_0x0015
            int r0 = r7.b
            if (r0 == 0) goto L_0x0007
        L_0x0015:
            int r0 = r7.o
            r7.a(r0)
            int r0 = r7.d
            int r1 = r7.g
            switch(r1) {
                case 1: goto L_0x0047;
                case 2: goto L_0x008a;
                default: goto L_0x0021;
            }
        L_0x0021:
            int r1 = r7.f
            int r3 = r7.d
            int r0 = r3 - r0
            int r0 = r0 + r1
            r7.f = r0
            int r0 = r7.b
            if (r0 <= 0) goto L_0x0007
            int r0 = r7.f
            if (r0 <= 0) goto L_0x0007
            byte[] r0 = r7.m
            byte[] r1 = r7.c
            int r3 = r7.d
            byte[] r4 = r7.m
            int r4 = r4.length
            java.lang.System.arraycopy(r0, r2, r1, r3, r4)
            int r0 = r7.d
            byte[] r1 = r7.m
            int r1 = r1.length
            int r0 = r0 + r1
            r7.d = r0
            goto L_0x0007
        L_0x0047:
            byte[] r1 = r7.c
            int r3 = r7.d
            int r4 = r3 + 1
            r7.d = r4
            byte[] r4 = r7.k
            int r5 = r7.p
            int r5 = r5 >> 2
            r5 = r5 & 63
            byte r4 = r4[r5]
            r1[r3] = r4
            byte[] r1 = r7.c
            int r3 = r7.d
            int r4 = r3 + 1
            r7.d = r4
            byte[] r4 = r7.k
            int r5 = r7.p
            int r5 = r5 << 4
            r5 = r5 & 63
            byte r4 = r4[r5]
            r1[r3] = r4
            byte[] r1 = r7.k
            byte[] r3 = com.umeng.socialize.d.b.b.h
            if (r1 != r3) goto L_0x0021
            byte[] r1 = r7.c
            int r3 = r7.d
            int r4 = r3 + 1
            r7.d = r4
            r1[r3] = r6
            byte[] r1 = r7.c
            int r3 = r7.d
            int r4 = r3 + 1
            r7.d = r4
            r1[r3] = r6
            goto L_0x0021
        L_0x008a:
            byte[] r1 = r7.c
            int r3 = r7.d
            int r4 = r3 + 1
            r7.d = r4
            byte[] r4 = r7.k
            int r5 = r7.p
            int r5 = r5 >> 10
            r5 = r5 & 63
            byte r4 = r4[r5]
            r1[r3] = r4
            byte[] r1 = r7.c
            int r3 = r7.d
            int r4 = r3 + 1
            r7.d = r4
            byte[] r4 = r7.k
            int r5 = r7.p
            int r5 = r5 >> 4
            r5 = r5 & 63
            byte r4 = r4[r5]
            r1[r3] = r4
            byte[] r1 = r7.c
            int r3 = r7.d
            int r4 = r3 + 1
            r7.d = r4
            byte[] r4 = r7.k
            int r5 = r7.p
            int r5 = r5 << 2
            r5 = r5 & 63
            byte r4 = r4[r5]
            r1[r3] = r4
            byte[] r1 = r7.k
            byte[] r3 = com.umeng.socialize.d.b.b.h
            if (r1 != r3) goto L_0x0021
            byte[] r1 = r7.c
            int r3 = r7.d
            int r4 = r3 + 1
            r7.d = r4
            r1[r3] = r6
            goto L_0x0021
        L_0x00d8:
            r1 = r2
        L_0x00d9:
            if (r1 >= r10) goto L_0x0007
            int r0 = r7.o
            r7.a(r0)
            int r0 = r7.g
            int r0 = r0 + 1
            int r0 = r0 % 3
            r7.g = r0
            int r3 = r9 + 1
            byte r0 = r8[r9]
            if (r0 >= 0) goto L_0x00f0
            int r0 = r0 + 256
        L_0x00f0:
            int r4 = r7.p
            int r4 = r4 << 8
            int r0 = r0 + r4
            r7.p = r0
            int r0 = r7.g
            if (r0 != 0) goto L_0x016f
            byte[] r0 = r7.c
            int r4 = r7.d
            int r5 = r4 + 1
            r7.d = r5
            byte[] r5 = r7.k
            int r6 = r7.p
            int r6 = r6 >> 18
            r6 = r6 & 63
            byte r5 = r5[r6]
            r0[r4] = r5
            byte[] r0 = r7.c
            int r4 = r7.d
            int r5 = r4 + 1
            r7.d = r5
            byte[] r5 = r7.k
            int r6 = r7.p
            int r6 = r6 >> 12
            r6 = r6 & 63
            byte r5 = r5[r6]
            r0[r4] = r5
            byte[] r0 = r7.c
            int r4 = r7.d
            int r5 = r4 + 1
            r7.d = r5
            byte[] r5 = r7.k
            int r6 = r7.p
            int r6 = r6 >> 6
            r6 = r6 & 63
            byte r5 = r5[r6]
            r0[r4] = r5
            byte[] r0 = r7.c
            int r4 = r7.d
            int r5 = r4 + 1
            r7.d = r5
            byte[] r5 = r7.k
            int r6 = r7.p
            r6 = r6 & 63
            byte r5 = r5[r6]
            r0[r4] = r5
            int r0 = r7.f
            int r0 = r0 + 4
            r7.f = r0
            int r0 = r7.b
            if (r0 <= 0) goto L_0x016f
            int r0 = r7.b
            int r4 = r7.f
            if (r0 > r4) goto L_0x016f
            byte[] r0 = r7.m
            byte[] r4 = r7.c
            int r5 = r7.d
            byte[] r6 = r7.m
            int r6 = r6.length
            java.lang.System.arraycopy(r0, r2, r4, r5, r6)
            int r0 = r7.d
            byte[] r4 = r7.m
            int r4 = r4.length
            int r0 = r0 + r4
            r7.d = r0
            r7.f = r2
        L_0x016f:
            int r0 = r1 + 1
            r1 = r0
            r9 = r3
            goto L_0x00d9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.umeng.socialize.d.b.b.a(byte[], int, int):void");
    }

    /* access modifiers changed from: package-private */
    public void b(byte[] bArr, int i2, int i3) {
        byte b;
        if (!this.e) {
            if (i3 < 0) {
                this.e = true;
            }
            int i4 = 0;
            while (true) {
                if (i4 >= i3) {
                    break;
                }
                a(this.n);
                int i5 = i2 + 1;
                byte b2 = bArr[i2];
                if (b2 == 61) {
                    this.e = true;
                    break;
                }
                if (b2 >= 0 && b2 < j.length && (b = j[b2]) >= 0) {
                    this.g = (this.g + 1) % 4;
                    this.p = b + (this.p << 6);
                    if (this.g == 0) {
                        byte[] bArr2 = this.c;
                        int i6 = this.d;
                        this.d = i6 + 1;
                        bArr2[i6] = (byte) ((this.p >> 16) & MotionEventCompat.ACTION_MASK);
                        byte[] bArr3 = this.c;
                        int i7 = this.d;
                        this.d = i7 + 1;
                        bArr3[i7] = (byte) ((this.p >> 8) & MotionEventCompat.ACTION_MASK);
                        byte[] bArr4 = this.c;
                        int i8 = this.d;
                        this.d = i8 + 1;
                        bArr4[i8] = (byte) (this.p & MotionEventCompat.ACTION_MASK);
                    }
                }
                i4++;
                i2 = i5;
            }
            if (this.e && this.g != 0) {
                a(this.n);
                switch (this.g) {
                    case 2:
                        this.p >>= 4;
                        byte[] bArr5 = this.c;
                        int i9 = this.d;
                        this.d = i9 + 1;
                        bArr5[i9] = (byte) (this.p & MotionEventCompat.ACTION_MASK);
                        return;
                    case 3:
                        this.p >>= 2;
                        byte[] bArr6 = this.c;
                        int i10 = this.d;
                        this.d = i10 + 1;
                        bArr6[i10] = (byte) ((this.p >> 8) & MotionEventCompat.ACTION_MASK);
                        byte[] bArr7 = this.c;
                        int i11 = this.d;
                        this.d = i11 + 1;
                        bArr7[i11] = (byte) (this.p & MotionEventCompat.ACTION_MASK);
                        return;
                    default:
                        return;
                }
            }
        }
    }

    public static String a(byte[] bArr) {
        return a.a(a(bArr, false));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.umeng.socialize.d.b.b.a(byte[], boolean, boolean):byte[]
     arg types: [byte[], boolean, int]
     candidates:
      com.umeng.socialize.d.b.b.a(byte[], int, int):void
      com.umeng.socialize.d.b.c.a(byte[], int, int):void
      com.umeng.socialize.d.b.b.a(byte[], boolean, boolean):byte[] */
    public static byte[] a(byte[] bArr, boolean z) {
        return a(bArr, z, false);
    }

    public static byte[] a(byte[] bArr, boolean z, boolean z2) {
        return a(bArr, z, z2, Integer.MAX_VALUE);
    }

    public static byte[] a(byte[] bArr, boolean z, boolean z2, int i2) {
        if (bArr == null || bArr.length == 0) {
            return bArr;
        }
        b bVar = z ? new b(z2) : new b(0, f2229a, z2);
        long e = bVar.e(bArr);
        if (e <= ((long) i2)) {
            return bVar.c(bArr);
        }
        throw new IllegalArgumentException("Input array too big, the output array would be bigger (" + e + ") than the specified maximum size of " + i2);
    }

    public static byte[] a(String str) {
        return new b().b(str);
    }

    /* access modifiers changed from: protected */
    public boolean a(byte b) {
        return b >= 0 && b < this.l.length && this.l[b] != -1;
    }
}
