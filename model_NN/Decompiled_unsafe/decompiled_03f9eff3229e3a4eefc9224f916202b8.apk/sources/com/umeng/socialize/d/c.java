package com.umeng.socialize.d;

import android.content.Context;
import com.umeng.socialize.d.a.b;
import com.umeng.socialize.utils.h;

/* compiled from: GetPlatformKeyRequest */
public class c extends b {
    public c(Context context) {
        super(context, "", d.class, 20, b.C0050b.GET);
        this.b = context;
    }

    /* access modifiers changed from: protected */
    public String b() {
        return "/share/keysecret/" + h.a(this.b) + "/";
    }
}
