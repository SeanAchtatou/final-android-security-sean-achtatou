package com.umeng.socialize.d;

import android.text.TextUtils;
import com.sina.weibo.sdk.register.mobile.SelectCountryActivity;
import com.umeng.socialize.c.c;
import com.umeng.socialize.d.a.f;
import com.umeng.socialize.utils.g;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONObject;

/* compiled from: ShareFriendsResponse */
public class i extends f {

    /* renamed from: a  reason: collision with root package name */
    public List<c> f2236a;

    public i(JSONObject jSONObject) {
        super(jSONObject);
    }

    public void a() {
        JSONObject jSONObject = this.j;
        if (jSONObject == null) {
            g.b("SocializeReseponse", "data json is null....");
            return;
        }
        this.f2236a = new ArrayList();
        Iterator<String> keys = jSONObject.keys();
        while (keys.hasNext()) {
            try {
                String obj = keys.next().toString();
                JSONObject jSONObject2 = (JSONObject) this.j.get(obj);
                if (jSONObject2.has(SelectCountryActivity.EXTRA_COUNTRY_NAME)) {
                    String string = jSONObject2.getString(SelectCountryActivity.EXTRA_COUNTRY_NAME);
                    if (!TextUtils.isEmpty(obj) && !TextUtils.isEmpty(string)) {
                        c cVar = new c();
                        cVar.b(obj);
                        cVar.c(string);
                        String optString = jSONObject2.optString("link_name", "");
                        if (!TextUtils.isEmpty(optString)) {
                            string = optString;
                        }
                        cVar.a(string);
                        String optString2 = jSONObject2.optString("pinyin", "");
                        if (!TextUtils.isEmpty(optString2)) {
                            c.a aVar = new c.a();
                            aVar.b = String.valueOf(a(optString2.charAt(0)));
                            aVar.f2211a = optString2;
                            cVar.a(aVar);
                        }
                        if (jSONObject2.has("profile_image_url")) {
                            cVar.d(jSONObject2.getString("profile_image_url"));
                        }
                        this.f2236a.add(cVar);
                    }
                }
            } catch (Exception e) {
                g.b("SocializeReseponse", "Parse friend data error", e);
            }
        }
    }

    public static char a(char c) {
        if (c < 'a' || c > 'z') {
            return c;
        }
        return (char) (c - ' ');
    }
}
