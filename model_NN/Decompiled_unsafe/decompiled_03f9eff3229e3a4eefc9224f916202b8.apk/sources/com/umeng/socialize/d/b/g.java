package com.umeng.socialize.d.b;

import java.util.Map;
import org.json.JSONObject;

/* compiled from: URequest */
public abstract class g {
    /* access modifiers changed from: protected */
    public static String d = "POST";
    /* access modifiers changed from: protected */
    public static String e = "GET";
    protected String f;

    public abstract JSONObject e();

    public abstract String f();

    /* access modifiers changed from: protected */
    public String g() {
        return d;
    }

    public g(String str) {
        this.f = str;
    }

    public void b(String str) {
        this.f = str;
    }

    public String j() {
        return this.f;
    }

    public Map<String, Object> d() {
        return null;
    }

    public Map<String, a> c() {
        return null;
    }

    public void a() {
    }

    /* compiled from: URequest */
    public static class a {

        /* renamed from: a  reason: collision with root package name */
        String f2233a;
        byte[] b;

        public a(String str, byte[] bArr) {
            this.f2233a = str;
            this.b = bArr;
        }
    }
}
