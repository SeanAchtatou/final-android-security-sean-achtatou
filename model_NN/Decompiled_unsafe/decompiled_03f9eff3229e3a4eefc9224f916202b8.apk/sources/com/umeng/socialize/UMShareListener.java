package com.umeng.socialize;

import com.umeng.socialize.c.b;

public interface UMShareListener {
    void onCancel(b bVar);

    void onError(b bVar, Throwable th);

    void onResult(b bVar);
}
