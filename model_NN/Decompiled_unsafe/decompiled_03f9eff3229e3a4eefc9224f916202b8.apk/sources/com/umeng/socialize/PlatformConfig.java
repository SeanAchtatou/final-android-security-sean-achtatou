package com.umeng.socialize;

import android.content.Context;
import android.text.TextUtils;
import com.umeng.socialize.c.b;
import com.umeng.socialize.d.c;
import com.umeng.socialize.d.d;
import com.umeng.socialize.d.g;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

public class PlatformConfig {
    public static Map<b, Platform> configs = new HashMap();

    public interface Platform {
        b getName();

        boolean isAuthrized();

        boolean isConfigured();

        void parse(JSONObject jSONObject);
    }

    static {
        configs.put(b.QQ, new QQZone(b.QQ));
        configs.put(b.QZONE, new QQZone(b.QZONE));
        configs.put(b.WEIXIN, new Weixin(b.WEIXIN));
        configs.put(b.WEIXIN_CIRCLE, new Weixin(b.WEIXIN_CIRCLE));
        configs.put(b.WEIXIN_FAVORITE, new Weixin(b.WEIXIN_FAVORITE));
        configs.put(b.DOUBAN, new Douban());
        configs.put(b.LAIWANG, new Laiwang(b.LAIWANG));
        configs.put(b.LAIWANG_DYNAMIC, new Laiwang(b.LAIWANG_DYNAMIC));
        configs.put(b.YIXIN, new Yixin(b.YIXIN));
        configs.put(b.YIXIN_CIRCLE, new Yixin(b.YIXIN_CIRCLE));
        configs.put(b.SINA, new SinaWeibo());
        configs.put(b.TENCENT, new QQZone(b.TENCENT));
        configs.put(b.ALIPAY, new Alipay());
        configs.put(b.RENREN, new Renren());
        configs.put(b.GOOGLEPLUS, new GooglePlus());
        configs.put(b.FACEBOOK, new CustomPlatform(b.FACEBOOK));
        configs.put(b.TWITTER, new Twitter(b.TWITTER));
        configs.put(b.TUMBLR, new CustomPlatform(b.TUMBLR));
        configs.put(b.PINTEREST, new Pinterest());
        configs.put(b.POCKET, new CustomPlatform(b.POCKET));
        configs.put(b.WHATSAPP, new CustomPlatform(b.WHATSAPP));
        configs.put(b.EMAIL, new CustomPlatform(b.EMAIL));
        configs.put(b.SMS, new CustomPlatform(b.SMS));
        configs.put(b.LINKEDIN, new CustomPlatform(b.LINKEDIN));
        configs.put(b.LINE, new CustomPlatform(b.LINE));
        configs.put(b.FLICKR, new CustomPlatform(b.FLICKR));
        configs.put(b.EVERNOTE, new CustomPlatform(b.EVERNOTE));
        configs.put(b.FOURSQUARE, new CustomPlatform(b.FOURSQUARE));
        configs.put(b.YNOTE, new CustomPlatform(b.YNOTE));
        configs.put(b.KAKAO, new CustomPlatform(b.KAKAO));
        configs.put(b.INSTAGRAM, new CustomPlatform(b.INSTAGRAM));
    }

    public static void setQQZone(String str, String str2) {
        QQZone qQZone = (QQZone) configs.get(b.QZONE);
        qQZone.appId = str;
        qQZone.appKey = str2;
        QQZone qQZone2 = (QQZone) configs.get(b.QQ);
        qQZone2.appId = str;
        qQZone2.appKey = str2;
        QQZone qQZone3 = (QQZone) configs.get(b.TENCENT);
        qQZone3.appId = str;
        qQZone3.appKey = str2;
    }

    public static void setTwitter(String str, String str2) {
        Twitter twitter = (Twitter) configs.get(b.TWITTER);
        twitter.appKey = str;
        twitter.appSecret = str2;
    }

    public static void setAlipay(String str) {
        ((Alipay) configs.get(b.ALIPAY)).id = str;
    }

    public static void setTencentWB(String str, String str2) {
        TencentWeibo tencentWeibo = (TencentWeibo) configs.get(b.TENCENT);
        tencentWeibo.appKey = str;
        tencentWeibo.appSecret = str2;
    }

    public static void setSinaWeibo(String str, String str2) {
        SinaWeibo sinaWeibo = (SinaWeibo) configs.get(b.SINA);
        sinaWeibo.appKey = str;
        sinaWeibo.appSecret = str2;
    }

    public static void setTencentWeibo(String str, String str2) {
        TencentWeibo tencentWeibo = (TencentWeibo) configs.get(b.TENCENT);
        tencentWeibo.appKey = str;
        tencentWeibo.appSecret = str2;
    }

    private void a(String str, String str2, String str3) {
        Renren renren = (Renren) configs.get(b.RENREN);
        renren.appId = str;
        renren.appkey = str2;
        renren.appSecret = str3;
    }

    private static void a(String str, String str2) {
        Douban douban = (Douban) configs.get(b.DOUBAN);
        douban.appKey = str;
        douban.appSecret = str2;
    }

    public static void setWeixin(String str, String str2) {
        Weixin weixin = (Weixin) configs.get(b.WEIXIN);
        weixin.appId = str;
        weixin.appSecret = str2;
        Weixin weixin2 = (Weixin) configs.get(b.WEIXIN_CIRCLE);
        weixin2.appId = str;
        weixin2.appSecret = str2;
        Weixin weixin3 = (Weixin) configs.get(b.WEIXIN_FAVORITE);
        weixin3.appId = str;
        weixin3.appSecret = str2;
    }

    public static void setLaiwang(String str, String str2) {
        Laiwang laiwang = (Laiwang) configs.get(b.LAIWANG);
        laiwang.appToken = str;
        laiwang.appSecret = str2;
        Laiwang laiwang2 = (Laiwang) configs.get(b.LAIWANG_DYNAMIC);
        laiwang2.appToken = str;
        laiwang2.appSecret = str2;
    }

    public static void setYixin(String str) {
        ((Yixin) configs.get(b.YIXIN)).yixinId = str;
        ((Yixin) configs.get(b.YIXIN_CIRCLE)).yixinId = str;
    }

    public static void setPinterest(String str) {
        ((Pinterest) configs.get(b.PINTEREST)).appId = str;
    }

    public static Platform getPlatform(b bVar) {
        return configs.get(bVar);
    }

    /* JADX WARNING: Removed duplicated region for block: B:3:0x0010  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void updateConfig(android.content.Context r2) {
        /*
            java.util.Map<com.umeng.socialize.c.b, com.umeng.socialize.PlatformConfig$Platform> r0 = com.umeng.socialize.PlatformConfig.configs
            java.util.Collection r0 = r0.values()
            java.util.Iterator r1 = r0.iterator()
        L_0x000a:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x001c
            java.lang.Object r0 = r1.next()
            com.umeng.socialize.PlatformConfig$Platform r0 = (com.umeng.socialize.PlatformConfig.Platform) r0
            boolean r0 = r0.isConfigured()
            if (r0 != 0) goto L_0x000a
        L_0x001c:
            boolean r0 = a(r2)
            if (r0 == 0) goto L_0x0023
        L_0x0022:
            return
        L_0x0023:
            boolean r0 = b(r2)
            if (r0 == 0) goto L_0x0022
            goto L_0x0022
        */
        throw new UnsupportedOperationException("Method not decompiled: com.umeng.socialize.PlatformConfig.updateConfig(android.content.Context):void");
    }

    private static boolean a(Context context) {
        return false;
    }

    private static boolean b(Context context) {
        d a2 = g.a(new c(context));
        if (a2 == null || !a2.b()) {
            return false;
        }
        JSONObject c = a2.c();
        try {
            for (Map.Entry next : configs.entrySet()) {
                ((Platform) next.getValue()).parse(c.getJSONObject(((b) next.getKey()).toString()));
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static class QQZone implements Platform {
        public String appId = null;
        public String appKey = null;
        private final b media;

        public QQZone(b bVar) {
            this.media = bVar;
        }

        public b getName() {
            return this.media;
        }

        public void parse(JSONObject jSONObject) {
            this.appId = jSONObject.optString("key");
            this.appKey = jSONObject.optString("secret");
        }

        public boolean isConfigured() {
            return !TextUtils.isEmpty(this.appId) && !TextUtils.isEmpty(this.appKey);
        }

        public boolean isAuthrized() {
            return false;
        }
    }

    public static class Twitter implements Platform {
        public String appKey = null;
        public String appSecret = null;
        private final b media;

        public Twitter(b bVar) {
            this.media = bVar;
        }

        public b getName() {
            return this.media;
        }

        public void parse(JSONObject jSONObject) {
            this.appKey = jSONObject.optString("key");
            this.appSecret = jSONObject.optString("secret");
        }

        public boolean isConfigured() {
            return !TextUtils.isEmpty(this.appSecret) && !TextUtils.isEmpty(this.appKey);
        }

        public boolean isAuthrized() {
            return false;
        }
    }

    public static class SinaWeibo implements Platform {
        public String appKey = null;
        public String appSecret = null;

        public b getName() {
            return b.SINA;
        }

        public void parse(JSONObject jSONObject) {
            this.appKey = jSONObject.optString("key");
            this.appSecret = jSONObject.optString("secret");
        }

        public boolean isConfigured() {
            return !TextUtils.isEmpty(this.appKey) && !TextUtils.isEmpty(this.appSecret);
        }

        public boolean isAuthrized() {
            return false;
        }
    }

    public static class TencentWeibo implements Platform {
        public static final String Name = "tencent";
        public String appKey = null;
        public String appSecret = null;

        public b getName() {
            return b.TENCENT;
        }

        public void parse(JSONObject jSONObject) {
            this.appKey = jSONObject.optString("key");
            this.appSecret = jSONObject.optString("secret");
        }

        public boolean isConfigured() {
            return true;
        }

        public boolean isAuthrized() {
            return false;
        }
    }

    public static class Alipay implements Platform {
        public static final String Name = "alipay";
        public String id = null;

        public b getName() {
            return b.ALIPAY;
        }

        public void parse(JSONObject jSONObject) {
            this.id = jSONObject.optString("id");
        }

        public boolean isConfigured() {
            return !TextUtils.isEmpty(this.id);
        }

        public boolean isAuthrized() {
            return false;
        }
    }

    public static class Renren implements Platform {
        public static final String Name = "renren";
        public String appId = "201874";
        public String appSecret = "3bf66e42db1e4fa9829b955cc300b737";
        public String appkey = "28401c0964f04a72a14c812d6132fcef";

        public b getName() {
            return b.RENREN;
        }

        public void parse(JSONObject jSONObject) {
            this.appId = jSONObject.optString("id");
            this.appkey = jSONObject.optString("key");
            this.appSecret = jSONObject.optString("secret");
        }

        public boolean isConfigured() {
            boolean z;
            boolean z2;
            boolean z3;
            if (!TextUtils.isEmpty(this.appkey)) {
                z = true;
            } else {
                z = false;
            }
            if (!TextUtils.isEmpty(this.appSecret)) {
                z2 = true;
            } else {
                z2 = false;
            }
            if (!TextUtils.isEmpty(this.appId)) {
                z3 = true;
            } else {
                z3 = false;
            }
            if (!z || !z2 || !z3) {
                return false;
            }
            return true;
        }

        public boolean isAuthrized() {
            return false;
        }
    }

    public static class Facebook implements Platform {
        public b getName() {
            return b.FACEBOOK;
        }

        public void parse(JSONObject jSONObject) {
        }

        public boolean isConfigured() {
            return false;
        }

        public boolean isAuthrized() {
            return false;
        }
    }

    public static class Douban implements Platform {
        public String appKey = null;
        public String appSecret = null;

        public b getName() {
            return b.DOUBAN;
        }

        public void parse(JSONObject jSONObject) {
            this.appKey = jSONObject.optString("key");
            this.appSecret = jSONObject.optString("secret");
        }

        public boolean isConfigured() {
            return true;
        }

        public boolean isAuthrized() {
            return false;
        }
    }

    public static class Weixin implements Platform {
        public String appId = null;
        public String appSecret = null;
        private final b media;

        public b getName() {
            return this.media;
        }

        public Weixin(b bVar) {
            this.media = bVar;
        }

        public void parse(JSONObject jSONObject) {
        }

        public boolean isConfigured() {
            return !TextUtils.isEmpty(this.appId) && !TextUtils.isEmpty(this.appSecret);
        }

        public boolean isAuthrized() {
            return false;
        }
    }

    public static class Pinterest implements Platform {
        public String appId = null;

        public b getName() {
            return b.PINTEREST;
        }

        public void parse(JSONObject jSONObject) {
        }

        public boolean isConfigured() {
            return !TextUtils.isEmpty(this.appId);
        }

        public boolean isAuthrized() {
            return false;
        }
    }

    public static class Laiwang implements Platform {
        public String appSecret = null;
        public String appToken = null;
        private final b media;

        public Laiwang(b bVar) {
            this.media = bVar;
        }

        public b getName() {
            return this.media;
        }

        public void parse(JSONObject jSONObject) {
        }

        public boolean isConfigured() {
            return !TextUtils.isEmpty(this.appToken) || !TextUtils.isEmpty(this.appSecret);
        }

        public boolean isAuthrized() {
            return false;
        }
    }

    public static class Yixin implements Platform {
        private final b media;
        public String yixinId = null;

        public Yixin(b bVar) {
            this.media = bVar;
        }

        public b getName() {
            return this.media;
        }

        public void parse(JSONObject jSONObject) {
        }

        public boolean isConfigured() {
            return !TextUtils.isEmpty(this.yixinId);
        }

        public boolean isAuthrized() {
            return false;
        }
    }

    public static class GooglePlus implements Platform {
        public static final String Name = "g+";
        public String appId = null;
        public String appSecret = null;
        public String appkey = null;

        public b getName() {
            return b.GOOGLEPLUS;
        }

        public void parse(JSONObject jSONObject) {
        }

        public boolean isConfigured() {
            return true;
        }

        public boolean isAuthrized() {
            return false;
        }
    }

    public static class CustomPlatform implements Platform {
        public static final String Name = "g+";
        public String appId = null;
        public String appSecret = null;
        public String appkey = null;
        private b p;

        public CustomPlatform(b bVar) {
            this.p = bVar;
        }

        public b getName() {
            return this.p;
        }

        public void parse(JSONObject jSONObject) {
        }

        public boolean isConfigured() {
            return true;
        }

        public boolean isAuthrized() {
            return false;
        }
    }
}
