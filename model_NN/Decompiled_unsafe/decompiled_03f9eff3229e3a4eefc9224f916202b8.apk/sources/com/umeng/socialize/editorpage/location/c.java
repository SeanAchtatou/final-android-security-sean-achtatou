package com.umeng.socialize.editorpage.location;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

/* compiled from: SocializeLocationListener */
public class c implements LocationListener {

    /* renamed from: a  reason: collision with root package name */
    private a f2253a;

    public void onStatusChanged(String str, int i, Bundle bundle) {
    }

    public void onProviderEnabled(String str) {
    }

    public void onProviderDisabled(String str) {
    }

    public void onLocationChanged(Location location) {
        if (this.f2253a != null) {
            this.f2253a.a(location);
            this.f2253a.c().a(this);
        }
    }
}
