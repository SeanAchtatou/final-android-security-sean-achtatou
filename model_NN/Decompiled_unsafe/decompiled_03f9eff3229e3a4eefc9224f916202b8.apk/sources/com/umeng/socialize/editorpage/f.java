package com.umeng.socialize.editorpage;

import android.location.Location;
import android.widget.Toast;
import com.umeng.socialize.c.d;
import com.umeng.socialize.editorpage.location.a;
import com.umeng.socialize.editorpage.location.b;
import com.umeng.socialize.utils.g;

/* compiled from: ShareActivity */
class f extends b {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ShareActivity f2249a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    f(ShareActivity shareActivity, a aVar) {
        super(aVar);
        this.f2249a = shareActivity;
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        super.onPreExecute();
        this.f2249a.a(true);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(Location location) {
        super.onPostExecute(location);
        g.b("xxxxx", "result = " + location);
        d unused = this.f2249a.y = d.a(location);
        this.f2249a.a(false);
        if (location == null && !this.f2249a.isFinishing()) {
            Toast.makeText(this.f2249a.u, "获取地理位置失败，请稍候重试.", 0).show();
        }
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        super.onCancelled();
        this.f2249a.a(false);
    }
}
