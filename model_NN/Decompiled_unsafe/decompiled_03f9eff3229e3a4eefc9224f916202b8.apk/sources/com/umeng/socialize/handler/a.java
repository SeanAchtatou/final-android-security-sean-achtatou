package com.umeng.socialize.handler;

import android.os.Bundle;
import com.umeng.socialize.handler.UMAPIShareHandler;
import com.umeng.socialize.utils.g;

/* compiled from: UMAPIShareHandler */
class a implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ UMAPIShareHandler.a f2295a;
    final /* synthetic */ Bundle b;
    final /* synthetic */ UMAPIShareHandler c;

    a(UMAPIShareHandler uMAPIShareHandler, UMAPIShareHandler.a aVar, Bundle bundle) {
        this.c = uMAPIShareHandler;
        this.f2295a = aVar;
        this.b = bundle;
    }

    public void run() {
        this.c.a(this.c.a(this.f2295a.f2279a, this.b), this.f2295a.b);
        g.c("act", "sent share request");
    }
}
