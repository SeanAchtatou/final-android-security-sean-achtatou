package com.umeng.socialize.handler;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import com.sina.weibo.sdk.auth.Oauth2AccessToken;
import java.util.HashMap;
import java.util.Map;

public class WeixinPreferences {

    /* renamed from: a  reason: collision with root package name */
    private SharedPreferences f2294a = null;
    private String b;
    private String c;
    private long d;
    private String e;
    private long f;
    private long g;

    public WeixinPreferences(Context context, String str) {
        this.f2294a = context.getSharedPreferences(str, 0);
        this.b = this.f2294a.getString("openid", null);
        this.c = this.f2294a.getString("access_token", null);
        this.d = this.f2294a.getLong("expires_in", 0);
        this.e = this.f2294a.getString(Oauth2AccessToken.KEY_REFRESH_TOKEN, null);
        this.f = this.f2294a.getLong("rt_expires_in", 0);
        this.g = this.f2294a.getLong("expires_in", 0);
    }

    public WeixinPreferences a(Bundle bundle) {
        this.b = bundle.getString("openid");
        this.c = bundle.getString("access_token");
        this.e = bundle.getString(Oauth2AccessToken.KEY_REFRESH_TOKEN);
        String string = bundle.getString("expires_in");
        if (!TextUtils.isEmpty(string)) {
            this.g = (Long.valueOf(string).longValue() * 1000) + System.currentTimeMillis();
        }
        String string2 = bundle.getString("expires_in");
        if (!TextUtils.isEmpty(string2)) {
            this.d = (Long.valueOf(string2).longValue() * 1000) + System.currentTimeMillis();
        }
        String string3 = bundle.getString("rt_expires_in");
        if (!TextUtils.isEmpty(string3)) {
            this.f = (Long.valueOf(string3).longValue() * 1000) + System.currentTimeMillis();
        }
        i();
        return this;
    }

    public String a() {
        return this.b;
    }

    public String b() {
        return this.e;
    }

    public Map<String, String> c() {
        HashMap hashMap = new HashMap();
        hashMap.put("access_token", this.c);
        hashMap.put("openid", this.b);
        hashMap.put(Oauth2AccessToken.KEY_REFRESH_TOKEN, this.e);
        return hashMap;
    }

    public boolean d() {
        boolean z;
        boolean isEmpty = TextUtils.isEmpty(this.c);
        if (this.g - System.currentTimeMillis() <= 0) {
            z = true;
        } else {
            z = false;
        }
        if (isEmpty || z) {
            return false;
        }
        return true;
    }

    public String e() {
        return this.c;
    }

    public boolean f() {
        boolean z;
        boolean isEmpty = TextUtils.isEmpty(this.e);
        if (this.f - System.currentTimeMillis() <= 0) {
            z = true;
        } else {
            z = false;
        }
        if (isEmpty || z) {
            return false;
        }
        return true;
    }

    public boolean g() {
        return !TextUtils.isEmpty(e());
    }

    public void h() {
        this.f2294a.edit().clear().commit();
    }

    public void i() {
        this.f2294a.edit().putString("openid", this.b).putString("access_token", this.c).putLong("expires_in", this.d).putString(Oauth2AccessToken.KEY_REFRESH_TOKEN, this.e).putLong("rt_expires_in", this.f).putLong("expires_in", this.g).commit();
    }
}
