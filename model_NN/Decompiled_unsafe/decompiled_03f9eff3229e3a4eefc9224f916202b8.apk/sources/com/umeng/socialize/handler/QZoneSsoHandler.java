package com.umeng.socialize.handler;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Toast;
import com.qq.e.comm.constants.Constants;
import com.sina.weibo.sdk.auth.Oauth2AccessToken;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.Tencent;
import com.tencent.tauth.UiError;
import com.umeng.socialize.Config;
import com.umeng.socialize.PlatformConfig;
import com.umeng.socialize.ShareContent;
import com.umeng.socialize.SocializeException;
import com.umeng.socialize.UMAuthListener;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.c.b;
import com.umeng.socialize.d.e;
import com.umeng.socialize.handler.UMTencentSSOHandler;
import com.umeng.socialize.media.UMediaObject;
import com.umeng.socialize.media.c;
import com.umeng.socialize.utils.g;
import com.umeng.socialize.utils.h;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.json.JSONObject;

public class QZoneSsoHandler extends UMTencentSSOHandler {
    private Activity i;
    /* access modifiers changed from: private */
    public c j;
    /* access modifiers changed from: private */
    public QQPreferences k;
    private IUiListener l = new IUiListener() {
        public void onError(UiError uiError) {
            QZoneSsoHandler.this.f.onError(b.QZONE, 0, null);
        }

        public void onCancel() {
            QZoneSsoHandler.this.f.onCancel(b.QZONE, 0);
        }

        public void onComplete(Object obj) {
            QZoneSsoHandler.this.f.onComplete(b.QZONE, 0, null);
        }
    };

    public void a(Context context, PlatformConfig.Platform platform) {
        super.a(context, platform);
        this.k = new QQPreferences(context, b.QQ.toString());
    }

    public boolean a(Activity activity, ShareContent shareContent, UMShareListener uMShareListener) {
        if (uMShareListener != null) {
            this.h = uMShareListener;
        }
        if (b(activity, j())) {
            this.j = new c(shareContent);
            a(activity, new c(shareContent));
        }
        return false;
    }

    private void a(Activity activity, c cVar) {
        if (activity == null) {
            g.c("UMError", "QZone share activity is null");
            return;
        }
        Bundle e = this.j.e();
        e.putString("appName", c());
        int i2 = e.getInt("req_type");
        ArrayList<String> stringArrayList = e.getStringArrayList("imageUrl");
        String str = null;
        if (stringArrayList != null && stringArrayList.size() > 0) {
            str = stringArrayList.get(0);
        }
        if (a(str, i2)) {
            a(activity, a(e, new com.umeng.socialize.media.g(activity, str), activity));
        } else {
            a(e, activity);
        }
    }

    public void a(Context context, UMAuthListener uMAuthListener) {
        this.g.logout(context);
        if (this.k != null) {
            this.k.g();
        }
        uMAuthListener.onComplete(b.QZONE, 1, null);
    }

    private UMAuthListener a(final Bundle bundle, final com.umeng.socialize.media.g gVar, final Activity activity) {
        return new UMAuthListener() {
            public void onComplete(b bVar, int i, Map<String, String> map) {
                if (map != null && map.containsKey("uid")) {
                    QZoneSsoHandler.this.a(gVar, map.get("uid"), new UMTencentSSOHandler.ObtainImageUrlListener() {
                        public void a(String str) {
                            if (!TextUtils.isEmpty(str)) {
                                ArrayList arrayList = new ArrayList();
                                bundle.remove("imageUrl");
                                arrayList.add(str);
                                bundle.putStringArrayList("imageUrl", arrayList);
                                QZoneSsoHandler.this.a(bundle, activity);
                                return;
                            }
                            QZoneSsoHandler.this.a(bundle, activity);
                            UMediaObject d = QZoneSsoHandler.this.j.d();
                            int i = bundle.getInt("req_type");
                            if (!QZoneSsoHandler.this.d(activity) && d != null) {
                                if (d.g() == UMediaObject.a.VEDIO || d.g() == UMediaObject.a.MUSIC || i == 1) {
                                    g.b("QZoneSsoHandler", "QQ空间上传图片失败将导致无客户端分享失败，请设置缩略图为url类型或者较小的本地图片...");
                                }
                            }
                        }
                    });
                }
            }

            public void onError(b bVar, int i, Throwable th) {
            }

            public void onCancel(b bVar, int i) {
            }
        };
    }

    private boolean b(Context context, PlatformConfig.Platform platform) {
        if (this.g.isSupportSSOLogin((Activity) context)) {
            return true;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("请安装");
        sb.append("qq");
        sb.append("客户端");
        g.d(sb.toString());
        if (Config.IsToastTip) {
            Toast.makeText(context, sb, 1).show();
        }
        return false;
    }

    public boolean a() {
        return true;
    }

    public void a(Activity activity, UMAuthListener uMAuthListener) {
        if (activity == null) {
            g.c("UMError", "QZone share activity is null");
            return;
        }
        this.i = activity;
        if (b(activity, j())) {
            this.f = uMAuthListener;
            e();
        }
    }

    private void e() {
        g.a("QZoneSsoHandler", "QQ oauth login...");
        this.g.login(this.i, "all", a(this.f));
    }

    private IUiListener a(UMAuthListener uMAuthListener) {
        return new IUiListener() {
            public void onError(UiError uiError) {
                if (uiError != null) {
                    g.c("QZoneSsoHandler", "授权失败! ==> errorCode = " + uiError.errorCode + ", errorMsg = " + uiError.errorMessage + ", detail = " + uiError.errorDetail);
                }
                QZoneSsoHandler.this.f.onError(b.QQ, 0, new Throwable("授权失败! ==> errorCode = " + uiError.errorCode + ", errorMsg = " + uiError.errorMessage + ", detail = " + uiError.errorDetail));
            }

            public void onCancel() {
                QZoneSsoHandler.this.f.onCancel(b.QQ, 0);
            }

            public void onComplete(Object obj) {
                h.a(QZoneSsoHandler.this.b);
                Bundle a2 = QZoneSsoHandler.this.a(obj);
                QZoneSsoHandler.this.k.a(a2).f();
                QZoneSsoHandler.this.a((JSONObject) obj);
                if (QZoneSsoHandler.this.f != null) {
                    QZoneSsoHandler.this.f.onComplete(b.QQ, 0, QZoneSsoHandler.this.b(a2));
                }
                QZoneSsoHandler.this.a(a2);
                if (a2 == null || TextUtils.isEmpty(a2.getString(Constants.KEYS.RET))) {
                }
            }
        };
    }

    /* access modifiers changed from: private */
    public void a(final Bundle bundle) throws SocializeException {
        new Thread(new Runnable() {
            public void run() {
                e eVar = new e(QZoneSsoHandler.this.i());
                eVar.a("to", "qq");
                eVar.a("usid", bundle.getString("uid"));
                eVar.a("access_token", bundle.getString("access_token"));
                eVar.a(Oauth2AccessToken.KEY_REFRESH_TOKEN, bundle.getString(Oauth2AccessToken.KEY_REFRESH_TOKEN));
                eVar.a("expires_in", bundle.getString("expires_in"));
                eVar.a("app_id", QZoneSsoHandler.this.e.appId);
                eVar.a("app_secret", QZoneSsoHandler.this.e.appKey);
                g.b("upload token resp = " + com.umeng.socialize.d.g.a(eVar));
            }
        }).start();
    }

    /* access modifiers changed from: private */
    public Map<String, String> b(Bundle bundle) {
        if (bundle == null || bundle.isEmpty()) {
            return null;
        }
        Set<String> keySet = bundle.keySet();
        HashMap hashMap = new HashMap();
        for (String next : keySet) {
            hashMap.put(next, bundle.getString(next));
        }
        return hashMap;
    }

    public void a(JSONObject jSONObject) {
        try {
            String string = jSONObject.getString("access_token");
            String string2 = jSONObject.getString("expires_in");
            String string3 = jSONObject.getString("openid");
            if (!TextUtils.isEmpty(string) && !TextUtils.isEmpty(string2) && !TextUtils.isEmpty(string3)) {
                this.g.setAccessToken(string, string2);
                this.g.setOpenId(string3);
            }
        } catch (Exception e) {
        }
    }

    public int b() {
        return com.tencent.connect.common.Constants.REQUEST_QZONE_SHARE;
    }

    public void a(int i2, int i3, Intent intent) {
        if (i2 == 10104) {
            Tencent.onActivityResultData(i2, i3, intent, a(this.h));
        }
        if (i2 == 11101) {
            Tencent.onActivityResultData(i2, i3, intent, a(this.f));
        }
    }

    public IUiListener a(final UMShareListener uMShareListener) {
        return new IUiListener() {
            public void onComplete(Object obj) {
                uMShareListener.onResult(b.QZONE);
            }

            public void onError(UiError uiError) {
                uMShareListener.onError(b.QZONE, null);
            }

            public void onCancel() {
                uMShareListener.onCancel(b.QZONE);
            }
        };
    }

    /* access modifiers changed from: private */
    public void a(final Bundle bundle, final Activity activity) {
        g.c("QZoneSsoHandler", "invoke Tencent.shareToQzone method...");
        if (bundle != null) {
            com.umeng.socialize.common.b.a(new Runnable() {
                public void run() {
                    QZoneSsoHandler.this.g.shareToQzone(activity, bundle, QZoneSsoHandler.this.a(QZoneSsoHandler.this.h));
                }
            });
        }
    }
}
