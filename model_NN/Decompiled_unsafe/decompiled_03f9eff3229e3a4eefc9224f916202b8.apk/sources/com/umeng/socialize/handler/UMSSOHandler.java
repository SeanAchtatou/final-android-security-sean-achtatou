package com.umeng.socialize.handler;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import com.umeng.socialize.PlatformConfig;
import com.umeng.socialize.ShareContent;
import com.umeng.socialize.UMAuthListener;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.utils.g;
import com.umeng.socialize.view.UMFriendListener;

public abstract class UMSSOHandler {

    /* renamed from: a  reason: collision with root package name */
    protected int f2286a = 32768;
    private Context b = null;
    private PlatformConfig.Platform c = null;
    private String d;

    public abstract boolean a(Activity activity, ShareContent shareContent, UMShareListener uMShareListener);

    public void a(Context context, PlatformConfig.Platform platform) {
        this.b = context.getApplicationContext();
        this.c = platform;
    }

    public void a(String str) {
        this.d = str;
    }

    public Context i() {
        return this.b;
    }

    public PlatformConfig.Platform j() {
        return this.c;
    }

    public void a(Activity activity, UMAuthListener uMAuthListener) {
    }

    public void a(Context context, UMAuthListener uMAuthListener) {
    }

    public void a(Activity activity, UMFriendListener uMFriendListener) {
    }

    public void a(int i, int i2, Intent intent) {
    }

    public void b(Activity activity, UMAuthListener uMAuthListener) {
        g.c("'getPlatformInfo', it works!");
    }

    public boolean a(Context context) {
        g.b("该平台不支持查询");
        return true;
    }

    public boolean b(Context context) {
        g.b("该平台不支持查询");
        return true;
    }

    public int b() {
        return 0;
    }

    public boolean a() {
        return false;
    }
}
