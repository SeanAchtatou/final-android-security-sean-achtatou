package com.umeng.socialize.handler;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.umeng.socialize.Config;
import com.umeng.socialize.PlatformConfig;
import com.umeng.socialize.ShareContent;
import com.umeng.socialize.SocializeException;
import com.umeng.socialize.UMAuthListener;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.c.b;
import com.umeng.socialize.d.a.f;
import com.umeng.socialize.d.j;
import com.umeng.socialize.d.k;
import com.umeng.socialize.d.l;
import com.umeng.socialize.editorpage.IEditor;
import com.umeng.socialize.editorpage.ShareActivity;
import com.umeng.socialize.utils.e;
import com.umeng.socialize.utils.g;
import java.util.Stack;

public abstract class UMAPIShareHandler extends UMSSOHandler implements IEditor {
    private Stack<a> b = new Stack<>();

    public abstract void b(int i, int i2, Intent intent);

    public abstract b e();

    public abstract String f();

    public void a(Context context, PlatformConfig.Platform platform) {
        super.a(context, platform);
    }

    public boolean d() {
        g.b("该平台不支持授权查询");
        return false;
    }

    public void a(int i, int i2, Intent intent) {
        a pop;
        if (i == b()) {
            if (i2 == 1000 && (pop = this.b.pop()) != null) {
                pop.b.onCancel(e());
            }
            if (intent == null || !intent.hasExtra("txt")) {
                b(i, i2, intent);
            } else if (!this.b.empty()) {
                a pop2 = this.b.pop();
                Bundle extras = intent == null ? null : intent.getExtras();
                if (i2 == -1) {
                    com.umeng.socialize.common.b.b(new a(this, pop2, extras));
                } else if (pop2.b != null) {
                    pop2.b.onCancel(e());
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void h() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.umeng.socialize.handler.UMSSOHandler.a(android.app.Activity, com.umeng.socialize.UMAuthListener):void
     arg types: [android.app.Activity, com.umeng.socialize.handler.b]
     candidates:
      com.umeng.socialize.handler.UMAPIShareHandler.a(com.umeng.socialize.ShareContent, com.umeng.socialize.UMShareListener):void
      com.umeng.socialize.handler.UMAPIShareHandler.a(android.content.Context, com.umeng.socialize.PlatformConfig$Platform):void
      com.umeng.socialize.handler.UMSSOHandler.a(android.app.Activity, com.umeng.socialize.view.UMFriendListener):void
      com.umeng.socialize.handler.UMSSOHandler.a(android.content.Context, com.umeng.socialize.PlatformConfig$Platform):void
      com.umeng.socialize.handler.UMSSOHandler.a(android.content.Context, com.umeng.socialize.UMAuthListener):void
      com.umeng.socialize.editorpage.IEditor.a(com.umeng.socialize.ShareContent, android.os.Bundle):com.umeng.socialize.ShareContent
      com.umeng.socialize.handler.UMSSOHandler.a(android.app.Activity, com.umeng.socialize.UMAuthListener):void */
    public boolean a(Activity activity, ShareContent shareContent, UMShareListener uMShareListener) {
        UMShareListener uMShareListener2 = (UMShareListener) e.a(UMShareListener.class, uMShareListener);
        if (d()) {
            b(activity, shareContent, uMShareListener2);
            return false;
        }
        a(activity, (UMAuthListener) new b(this, activity, shareContent, uMShareListener2));
        return false;
    }

    /* access modifiers changed from: private */
    public void b(Activity activity, ShareContent shareContent, UMShareListener uMShareListener) {
        if (Config.OpenEditor) {
            a aVar = new a();
            aVar.f2279a = shareContent;
            aVar.b = uMShareListener;
            this.b.push(aVar);
            Intent intent = new Intent(activity, ShareActivity.class);
            intent.putExtras(a(shareContent));
            activity.startActivityForResult(intent, b());
            return;
        }
        a(shareContent, uMShareListener);
    }

    /* access modifiers changed from: private */
    public void a(ShareContent shareContent, UMShareListener uMShareListener) {
        b e = e();
        String lowerCase = e.toString().toLowerCase();
        String f = f();
        l lVar = new l(i(), lowerCase, f, shareContent);
        lVar.a(0);
        f a2 = com.umeng.socialize.d.g.a(lVar);
        g.b("xxxx  platform= " + lowerCase + "  uid=" + f + "   share=" + shareContent);
        if (a2 == null) {
            uMShareListener.onError(e, new SocializeException("response is null"));
            g.b("xxxx error!!! = response is null");
        } else if (!a2.b()) {
            uMShareListener.onError(e, new SocializeException(a2.l, a2.k));
            g.b("xxxx error!!! = " + a2.k + "   " + a2.l);
        } else {
            uMShareListener.onResult(e);
            g.b("xxxx error!!! = noerror");
        }
        if (shareContent.mFollow != null) {
            k a3 = com.umeng.socialize.d.g.a(new j(i(), lowerCase, f, shareContent.mFollow));
            if (a3 == null) {
                g.b("follow", "resp = null");
            } else if (!a3.b()) {
                g.b("follow", "follow fail e =" + a3.k);
            } else {
                h();
            }
        }
    }

    static class a {

        /* renamed from: a  reason: collision with root package name */
        public ShareContent f2279a;
        public UMShareListener b;

        a() {
        }
    }
}
