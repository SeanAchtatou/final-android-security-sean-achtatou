package com.umeng.socialize;

import android.app.Activity;
import android.content.Context;
import com.umeng.socialize.b.a;
import com.umeng.socialize.common.b;

/* compiled from: UMShareAPI */
class e extends b.a<Void> {
    final /* synthetic */ Activity c;
    final /* synthetic */ com.umeng.socialize.c.b d;
    final /* synthetic */ UMAuthListener e;
    final /* synthetic */ UMShareAPI f;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    e(UMShareAPI uMShareAPI, Context context, Activity activity, com.umeng.socialize.c.b bVar, UMAuthListener uMAuthListener) {
        super(context);
        this.f = uMShareAPI;
        this.c = activity;
        this.d = bVar;
        this.e = uMAuthListener;
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public Void c() {
        if (this.f.f2195a != null) {
            this.f.f2195a.c(this.c, this.d, this.e);
            return null;
        }
        new a(this.c);
        this.f.f2195a.c(this.c, this.d, this.e);
        return null;
    }
}
