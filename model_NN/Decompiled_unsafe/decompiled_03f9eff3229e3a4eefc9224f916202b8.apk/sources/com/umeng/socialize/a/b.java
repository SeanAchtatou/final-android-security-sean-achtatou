package com.umeng.socialize.a;

import com.umeng.socialize.d.a.f;
import java.util.Map;
import org.json.JSONObject;

/* compiled from: AnalyticsResponse */
public class b extends f {

    /* renamed from: a  reason: collision with root package name */
    public Map<com.umeng.socialize.c.b, Integer> f2198a;
    public String b;

    public b(JSONObject jSONObject) {
        super(jSONObject);
    }

    public String toString() {
        return "ShareMultiResponse [mInfoMap=" + this.f2198a + ", mWeiboId=" + this.b + ", mMsg=" + this.k + ", mStCode=" + this.l + "]";
    }
}
