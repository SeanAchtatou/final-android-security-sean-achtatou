package com.umeng.socialize.a;

import android.content.Context;
import com.umeng.socialize.d.a.a;
import com.umeng.socialize.d.a.b;
import com.umeng.socialize.media.UMediaObject;
import com.umeng.socialize.utils.g;

/* compiled from: SocialAnalytics */
public class c {

    /* renamed from: a  reason: collision with root package name */
    private static a f2199a = new a();

    public static void a(Context context, String str, String str2, UMediaObject uMediaObject) {
        g.d("xxxxxx log=" + str2);
        a aVar = new a(context, str, str2);
        aVar.a("normal");
        aVar.a(uMediaObject);
        b bVar = (b) f2199a.a((b) aVar);
        if (bVar == null || !bVar.b()) {
            g.c("xxxxxx fail to send log");
        } else {
            g.c("xxxxxx send log succeed");
        }
    }
}
