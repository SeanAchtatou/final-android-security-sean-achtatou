package com.umeng.socialize;

public class SocializeException extends RuntimeException {
    private static final long b = 1;

    /* renamed from: a  reason: collision with root package name */
    protected int f2194a = 5000;
    private String c = "";

    public int getErrorCode() {
        return this.f2194a;
    }

    public SocializeException(int i, String str) {
        super(str);
        this.f2194a = i;
        this.c = str;
    }

    public SocializeException(String str, Throwable th) {
        super(str, th);
        this.c = str;
    }

    public SocializeException(String str) {
        super(str);
        this.c = str;
    }

    public String getMessage() {
        return this.c;
    }
}
