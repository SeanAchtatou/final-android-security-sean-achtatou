package com.umeng.socialize.media;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import com.sina.weibo.sdk.api.ImageObject;
import com.sina.weibo.sdk.api.MusicObject;
import com.sina.weibo.sdk.api.TextObject;
import com.sina.weibo.sdk.api.VideoObject;
import com.sina.weibo.sdk.api.WebpageObject;
import com.sina.weibo.sdk.api.WeiboMultiMessage;
import com.sina.weibo.sdk.component.ShareRequestParam;
import com.sina.weibo.sdk.utils.Utility;
import com.umeng.socialize.ShareContent;
import com.umeng.socialize.utils.c;
import com.umeng.socialize.utils.g;
import java.io.ByteArrayOutputStream;

/* compiled from: SinaShareContent */
public class e extends d {

    /* renamed from: a  reason: collision with root package name */
    private final int f2303a = 24576;
    private Context b;
    private g c;

    public e(ShareContent shareContent) {
        super(shareContent);
        if (shareContent.mMedia != null && (shareContent.mMedia instanceof h)) {
            a((h) shareContent.mMedia);
        }
        if (shareContent.mMedia != null && (shareContent.mMedia instanceof p)) {
            a((p) shareContent.mMedia);
        }
        if (shareContent.mExtra != null) {
            this.c = (g) shareContent.mExtra;
        }
    }

    public void a(Context context) {
        this.b = context;
    }

    public WeiboMultiMessage a() {
        WeiboMultiMessage weiboMultiMessage = new WeiboMultiMessage();
        weiboMultiMessage.textObject = b();
        if (h() != null) {
            weiboMultiMessage.imageObject = c();
        }
        if (!TextUtils.isEmpty(i())) {
            weiboMultiMessage.mediaObject = d();
        }
        if (j() != null) {
            weiboMultiMessage.mediaObject = e();
            g.c("media", "share music");
        }
        if (k() != null) {
            weiboMultiMessage.mediaObject = l();
            g.c("media", "share video");
        }
        return weiboMultiMessage;
    }

    private TextObject b() {
        TextObject textObject = new TextObject();
        textObject.text = g();
        return textObject;
    }

    private ImageObject c() {
        ImageObject imageObject = new ImageObject();
        if (h().m() != null) {
            imageObject.setImageObject(h().m());
        }
        return imageObject;
    }

    private WebpageObject d() {
        Bitmap decodeResource;
        WebpageObject webpageObject = new WebpageObject();
        webpageObject.identify = Utility.generateGUID();
        if (TextUtils.isEmpty(f())) {
            webpageObject.title = "分享链接";
        } else {
            webpageObject.title = f();
        }
        webpageObject.description = g();
        if (this.c != null) {
            byte[] l = this.c.l();
            if (this.c.l().length > 24576) {
                decodeResource = BitmapFactory.decodeByteArray(a(l, 24576), 0, a(l, 24576).length);
            } else {
                decodeResource = this.c.m();
            }
        } else {
            decodeResource = BitmapFactory.decodeResource(this.b.getResources(), com.umeng.socialize.common.g.a(this.b, "drawable", "sina_web_default"));
        }
        webpageObject.setThumbImage(decodeResource);
        webpageObject.actionUrl = i();
        webpageObject.defaultText = g();
        g.c("share", "args check:" + webpageObject.checkArgs());
        return webpageObject;
    }

    private MusicObject e() {
        MusicObject musicObject = new MusicObject();
        musicObject.identify = Utility.generateGUID();
        if (TextUtils.isEmpty(f())) {
            musicObject.title = "分享音乐";
        } else {
            musicObject.title = f();
        }
        musicObject.description = j().f2300a;
        Bitmap bitmap = null;
        if (j().n() != null) {
            byte[] a2 = a(j().n().l(), 24576);
            if (a2 != null) {
                bitmap = BitmapFactory.decodeByteArray(a2, 0, a2.length);
            }
        } else if (!TextUtils.isEmpty(j().e())) {
            byte[] a3 = a(new g(c.a(), j().e()).l(), 24576);
            if (a3 != null) {
                bitmap = BitmapFactory.decodeByteArray(a3, 0, a3.length);
                g.c("UM", "get thumb bitmap");
            }
        } else {
            bitmap = BitmapFactory.decodeResource(this.b.getResources(), com.umeng.socialize.common.g.a(this.b, "drawable", "ic_logo"));
        }
        musicObject.setThumbImage(bitmap);
        musicObject.actionUrl = j().b();
        if (!TextUtils.isEmpty(j().o())) {
            musicObject.dataUrl = j().o();
        }
        if (!TextUtils.isEmpty(j().l())) {
            musicObject.dataHdUrl = j().l();
        }
        if (!TextUtils.isEmpty(j().m())) {
            musicObject.h5Url = j().m();
        }
        if (j().j() > 0) {
            musicObject.duration = j().j();
        } else {
            musicObject.duration = 10;
        }
        if (!TextUtils.isEmpty(j().a())) {
            musicObject.description = j().a();
        }
        if (!TextUtils.isEmpty(g())) {
            musicObject.defaultText = g();
        }
        return musicObject;
    }

    private VideoObject l() {
        VideoObject videoObject = new VideoObject();
        videoObject.identify = Utility.generateGUID();
        if (TextUtils.isEmpty(f())) {
            videoObject.title = "分享视频";
        } else {
            videoObject.title = f();
        }
        videoObject.description = g();
        Bitmap bitmap = null;
        if (k().o() != null) {
            byte[] a2 = a(k().o().l(), 24576);
            if (a2 != null) {
                bitmap = BitmapFactory.decodeByteArray(a2, 0, a2.length);
            }
        } else if (!TextUtils.isEmpty(k().e())) {
            byte[] l = new g(c.a(), k().e()).l();
            if (l != null) {
                bitmap = BitmapFactory.decodeByteArray(l, 0, l.length);
            }
        } else {
            bitmap = BitmapFactory.decodeResource(this.b.getResources(), com.umeng.socialize.common.g.a(this.b, "drawable", "ic_logo"));
        }
        videoObject.setThumbImage(bitmap);
        videoObject.actionUrl = k().b();
        if (!TextUtils.isEmpty(k().l())) {
            videoObject.dataUrl = k().l();
        }
        if (!TextUtils.isEmpty(k().m())) {
            videoObject.dataHdUrl = k().m();
        }
        if (!TextUtils.isEmpty(k().n())) {
            videoObject.h5Url = k().n();
        }
        if (k().j() > 0) {
            videoObject.duration = k().j();
        } else {
            videoObject.duration = 10;
        }
        if (!TextUtils.isEmpty(k().a())) {
            videoObject.description = k().a();
        }
        videoObject.defaultText = "Video 分享视频";
        return videoObject;
    }

    private byte[] a(byte[] bArr, int i) {
        boolean z = false;
        if (bArr != null && bArr.length >= i) {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            Bitmap decodeByteArray = BitmapFactory.decodeByteArray(bArr, 0, bArr.length);
            int i2 = 1;
            while (!z && i2 <= 20) {
                decodeByteArray.compress(Bitmap.CompressFormat.JPEG, (int) (Math.pow(0.8d, (double) i2) * 100.0d), byteArrayOutputStream);
                if (byteArrayOutputStream == null || byteArrayOutputStream.size() >= i) {
                    byteArrayOutputStream.reset();
                    i2++;
                } else {
                    z = true;
                }
            }
            if (byteArrayOutputStream != null) {
                bArr = byteArrayOutputStream.toByteArray();
                if (!decodeByteArray.isRecycled()) {
                    decodeByteArray.recycle();
                }
                if (bArr == null || bArr.length <= 0) {
                }
                return bArr;
            }
        }
        g.c(ShareRequestParam.RESP_UPLOAD_PIC_PARAM_DATA, "weibo data size:" + bArr.length);
        return bArr;
    }
}
