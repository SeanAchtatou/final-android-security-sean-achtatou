package com.umeng.socialize.media;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Environment;
import android.text.TextUtils;
import com.umeng.socialize.media.UMediaObject;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

/* compiled from: UMImage */
public class g extends a {
    private c j = null;
    private WeakReference<Bitmap> k = new WeakReference<>(null);

    /* compiled from: UMImage */
    interface f {
        File a();

        String b();

        byte[] c();

        Bitmap d();
    }

    public g(Context context, File file) {
        a(context, file);
    }

    public g(Context context, String str) {
        super(str);
        a((Context) new WeakReference(context).get(), str);
    }

    private void a(Context context, Object obj) {
        if (obj instanceof File) {
            this.j = new e((File) obj);
        } else if (obj instanceof String) {
            this.j = new h((String) obj);
        } else if (obj instanceof Integer) {
            this.j = new C0051g(context, ((Integer) obj).intValue());
        } else if (obj instanceof byte[]) {
            this.j = new a((byte[]) obj);
        } else if (obj instanceof Bitmap) {
            this.j = new b((Bitmap) obj);
        } else {
            throw new RuntimeException("Don't support type");
        }
        this.j.a(new d(context));
    }

    public byte[] i() {
        return l();
    }

    public final Map<String, Object> h() {
        HashMap hashMap = new HashMap();
        if (c()) {
            hashMap.put(com.umeng.socialize.d.b.e.e, this.b);
            hashMap.put(com.umeng.socialize.d.b.e.f, g());
        }
        return hashMap;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x002d A[SYNTHETIC, Splitter:B:14:0x002d] */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0039 A[SYNTHETIC, Splitter:B:20:0x0039] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static byte[] b(byte[] r6) {
        /*
            r0 = 0
            android.graphics.BitmapFactory$Options r1 = com.umeng.socialize.utils.a.a(r6)     // Catch:{ Exception -> 0x0029, all -> 0x0033 }
            r2 = 0
            int r3 = r6.length     // Catch:{ Exception -> 0x0029, all -> 0x0033 }
            android.graphics.Bitmap r2 = android.graphics.BitmapFactory.decodeByteArray(r6, r2, r3, r1)     // Catch:{ Exception -> 0x0029, all -> 0x0033 }
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x0029, all -> 0x0033 }
            r1.<init>()     // Catch:{ Exception -> 0x0029, all -> 0x0033 }
            if (r2 == 0) goto L_0x001f
            android.graphics.Bitmap$CompressFormat r3 = android.graphics.Bitmap.CompressFormat.PNG     // Catch:{ Exception -> 0x0043, all -> 0x0041 }
            r4 = 100
            r2.compress(r3, r4, r1)     // Catch:{ Exception -> 0x0043, all -> 0x0041 }
            r2.recycle()     // Catch:{ Exception -> 0x0043, all -> 0x0041 }
            java.lang.System.gc()     // Catch:{ Exception -> 0x0043, all -> 0x0041 }
        L_0x001f:
            byte[] r0 = r1.toByteArray()     // Catch:{ Exception -> 0x0043, all -> 0x0041 }
            if (r1 == 0) goto L_0x0028
            r1.close()     // Catch:{ IOException -> 0x003d }
        L_0x0028:
            return r0
        L_0x0029:
            r1 = move-exception
            r1 = r0
        L_0x002b:
            if (r1 == 0) goto L_0x0028
            r1.close()     // Catch:{ IOException -> 0x0031 }
            goto L_0x0028
        L_0x0031:
            r1 = move-exception
            goto L_0x0028
        L_0x0033:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x0037:
            if (r1 == 0) goto L_0x003c
            r1.close()     // Catch:{ IOException -> 0x003f }
        L_0x003c:
            throw r0
        L_0x003d:
            r1 = move-exception
            goto L_0x0028
        L_0x003f:
            r1 = move-exception
            goto L_0x003c
        L_0x0041:
            r0 = move-exception
            goto L_0x0037
        L_0x0043:
            r2 = move-exception
            goto L_0x002b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.umeng.socialize.media.g.b(byte[]):byte[]");
    }

    public UMediaObject.a g() {
        return UMediaObject.a.IMAGE;
    }

    public File j() {
        com.umeng.socialize.utils.g.b("xxxxx convor=" + this.j.a());
        if (this.j == null) {
            return null;
        }
        return this.j.a();
    }

    public String k() {
        if (this.j == null) {
            return null;
        }
        return this.j.b();
    }

    public byte[] l() {
        if (this.j == null) {
            return null;
        }
        return this.j.c();
    }

    public Bitmap m() {
        if (this.j == null) {
            return null;
        }
        return this.j.d();
    }

    /* compiled from: UMImage */
    static class b extends c {
        private Bitmap b;

        public b(Bitmap bitmap) {
            this.b = bitmap;
        }

        /* JADX WARNING: Removed duplicated region for block: B:19:0x00e5 A[SYNTHETIC, Splitter:B:19:0x00e5] */
        /* JADX WARNING: Removed duplicated region for block: B:25:0x00ee A[SYNTHETIC, Splitter:B:25:0x00ee] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.io.File a() {
            /*
                r9 = this;
                r1 = 0
                long r4 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x00c1, all -> 0x00ea }
                com.umeng.socialize.media.g$d r0 = r9.f2304a     // Catch:{ Exception -> 0x00c1, all -> 0x00ea }
                android.graphics.Bitmap r2 = r9.b     // Catch:{ Exception -> 0x00c1, all -> 0x00ea }
                java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x00c1, all -> 0x00ea }
                java.lang.String r2 = com.umeng.socialize.d.b.a.c(r2)     // Catch:{ Exception -> 0x00c1, all -> 0x00ea }
                java.io.File r0 = r0.a(r2)     // Catch:{ Exception -> 0x00c1, all -> 0x00ea }
                java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x00c1, all -> 0x00ea }
                r2.<init>(r0)     // Catch:{ Exception -> 0x00c1, all -> 0x00ea }
                android.graphics.Bitmap r3 = r9.b     // Catch:{ Exception -> 0x00fa }
                int r3 = r3.getRowBytes()     // Catch:{ Exception -> 0x00fa }
                android.graphics.Bitmap r6 = r9.b     // Catch:{ Exception -> 0x00fa }
                int r6 = r6.getHeight()     // Catch:{ Exception -> 0x00fa }
                int r3 = r3 * r6
                int r6 = r3 / 1024
                java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00fa }
                r3.<init>()     // Catch:{ Exception -> 0x00fa }
                java.lang.String r7 = "### bitmap size = "
                java.lang.StringBuilder r3 = r3.append(r7)     // Catch:{ Exception -> 0x00fa }
                java.lang.StringBuilder r3 = r3.append(r6)     // Catch:{ Exception -> 0x00fa }
                java.lang.String r7 = " KB"
                java.lang.StringBuilder r3 = r3.append(r7)     // Catch:{ Exception -> 0x00fa }
                java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x00fa }
                com.umeng.socialize.utils.g.c(r3)     // Catch:{ Exception -> 0x00fa }
                r3 = 100
                float r7 = (float) r6     // Catch:{ Exception -> 0x00fa }
                com.umeng.socialize.media.g$d r8 = r9.f2304a     // Catch:{ Exception -> 0x00fa }
                float r8 = r8.f2305a     // Catch:{ Exception -> 0x00fa }
                int r7 = (r7 > r8 ? 1 : (r7 == r8 ? 0 : -1))
                if (r7 <= 0) goto L_0x005a
                com.umeng.socialize.media.g$d r7 = r9.f2304a     // Catch:{ Exception -> 0x00fa }
                float r7 = r7.f2305a     // Catch:{ Exception -> 0x00fa }
                float r6 = (float) r6     // Catch:{ Exception -> 0x00fa }
                float r6 = r7 / r6
                float r3 = (float) r3     // Catch:{ Exception -> 0x00fa }
                float r3 = r3 * r6
                int r3 = (int) r3     // Catch:{ Exception -> 0x00fa }
            L_0x005a:
                java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00fa }
                r6.<init>()     // Catch:{ Exception -> 0x00fa }
                java.lang.String r7 = "### 压缩质量 : "
                java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x00fa }
                java.lang.StringBuilder r6 = r6.append(r3)     // Catch:{ Exception -> 0x00fa }
                java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x00fa }
                com.umeng.socialize.utils.g.c(r6)     // Catch:{ Exception -> 0x00fa }
                android.graphics.Bitmap r6 = r9.b     // Catch:{ Exception -> 0x00fa }
                boolean r6 = r6.isRecycled()     // Catch:{ Exception -> 0x00fa }
                if (r6 != 0) goto L_0x007f
                android.graphics.Bitmap r6 = r9.b     // Catch:{ Exception -> 0x00fa }
                android.graphics.Bitmap$CompressFormat r7 = android.graphics.Bitmap.CompressFormat.JPEG     // Catch:{ Exception -> 0x00fa }
                r6.compress(r7, r3, r2)     // Catch:{ Exception -> 0x00fa }
            L_0x007f:
                java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00fa }
                r3.<init>()     // Catch:{ Exception -> 0x00fa }
                java.lang.String r6 = "##save bitmap "
                java.lang.StringBuilder r3 = r3.append(r6)     // Catch:{ Exception -> 0x00fa }
                java.lang.String r6 = r0.getAbsolutePath()     // Catch:{ Exception -> 0x00fa }
                java.lang.StringBuilder r3 = r3.append(r6)     // Catch:{ Exception -> 0x00fa }
                java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x00fa }
                com.umeng.socialize.utils.g.c(r3)     // Catch:{ Exception -> 0x00fa }
                long r6 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x00fa }
                java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00fa }
                r3.<init>()     // Catch:{ Exception -> 0x00fa }
                java.lang.String r8 = "#### 图片序列化耗时 : "
                java.lang.StringBuilder r3 = r3.append(r8)     // Catch:{ Exception -> 0x00fa }
                long r4 = r6 - r4
                java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x00fa }
                java.lang.String r4 = " ms."
                java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x00fa }
                java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x00fa }
                com.umeng.socialize.utils.g.c(r3)     // Catch:{ Exception -> 0x00fa }
                if (r2 == 0) goto L_0x00c0
                r2.close()     // Catch:{ IOException -> 0x00f2 }
            L_0x00c0:
                return r0
            L_0x00c1:
                r0 = move-exception
                r2 = r1
            L_0x00c3:
                java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x00f8 }
                r3.<init>()     // Catch:{ all -> 0x00f8 }
                java.lang.String r4 = "Sorry cannot setImage..["
                java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x00f8 }
                java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00f8 }
                java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ all -> 0x00f8 }
                java.lang.String r3 = "]"
                java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ all -> 0x00f8 }
                java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00f8 }
                com.umeng.socialize.utils.g.b(r0)     // Catch:{ all -> 0x00f8 }
                if (r2 == 0) goto L_0x00e8
                r2.close()     // Catch:{ IOException -> 0x00f4 }
            L_0x00e8:
                r0 = r1
                goto L_0x00c0
            L_0x00ea:
                r0 = move-exception
                r2 = r1
            L_0x00ec:
                if (r2 == 0) goto L_0x00f1
                r2.close()     // Catch:{ IOException -> 0x00f6 }
            L_0x00f1:
                throw r0
            L_0x00f2:
                r1 = move-exception
                goto L_0x00c0
            L_0x00f4:
                r0 = move-exception
                goto L_0x00e8
            L_0x00f6:
                r1 = move-exception
                goto L_0x00f1
            L_0x00f8:
                r0 = move-exception
                goto L_0x00ec
            L_0x00fa:
                r0 = move-exception
                goto L_0x00c3
            */
            throw new UnsupportedOperationException("Method not decompiled: com.umeng.socialize.media.g.b.a():java.io.File");
        }

        public String b() {
            return null;
        }

        public byte[] c() {
            return com.umeng.socialize.utils.a.a(this.b);
        }

        public Bitmap d() {
            return this.b;
        }
    }

    /* compiled from: UMImage */
    static class e extends c {
        private File b;

        public e(File file) {
            this.b = file;
        }

        public File a() {
            return this.b;
        }

        public String b() {
            return null;
        }

        public byte[] c() {
            return a(this.b);
        }

        public Bitmap d() {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(this.b.toString(), options);
            options.inSampleSize = com.umeng.socialize.utils.a.a(options, com.umeng.socialize.utils.a.b, com.umeng.socialize.utils.a.c);
            options.inJustDecodeBounds = false;
            return BitmapFactory.decodeFile(this.b.getAbsolutePath(), options);
        }

        private byte[] a(File file) {
            if (file == null || !file.getAbsoluteFile().exists()) {
                return null;
            }
            byte[] b2 = b(file);
            if (b2 == null || b2.length <= 0) {
                return null;
            }
            if (!com.umeng.socialize.common.a.f2214a[1].equals(com.umeng.socialize.common.a.a(b2))) {
                return g.b(b2);
            }
            return b2;
        }

        /* JADX WARNING: Removed duplicated region for block: B:15:0x0021 A[SYNTHETIC, Splitter:B:15:0x0021] */
        /* JADX WARNING: Removed duplicated region for block: B:18:0x0026 A[Catch:{ IOException -> 0x0052 }] */
        /* JADX WARNING: Removed duplicated region for block: B:31:0x0041 A[SYNTHETIC, Splitter:B:31:0x0041] */
        /* JADX WARNING: Removed duplicated region for block: B:34:0x0046 A[Catch:{ IOException -> 0x004a }] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private static byte[] b(java.io.File r6) {
            /*
                r0 = 0
                java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0054, all -> 0x003b }
                r3.<init>(r6)     // Catch:{ Exception -> 0x0054, all -> 0x003b }
                java.io.ByteArrayOutputStream r2 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x0058, all -> 0x004c }
                r2.<init>()     // Catch:{ Exception -> 0x0058, all -> 0x004c }
                r1 = 4096(0x1000, float:5.74E-42)
                byte[] r1 = new byte[r1]     // Catch:{ Exception -> 0x001b }
            L_0x000f:
                int r4 = r3.read(r1)     // Catch:{ Exception -> 0x001b }
                r5 = -1
                if (r4 == r5) goto L_0x002a
                r5 = 0
                r2.write(r1, r5, r4)     // Catch:{ Exception -> 0x001b }
                goto L_0x000f
            L_0x001b:
                r1 = move-exception
            L_0x001c:
                r1.printStackTrace()     // Catch:{ all -> 0x0050 }
                if (r3 == 0) goto L_0x0024
                r3.close()     // Catch:{ IOException -> 0x0052 }
            L_0x0024:
                if (r2 == 0) goto L_0x0029
                r2.close()     // Catch:{ IOException -> 0x0052 }
            L_0x0029:
                return r0
            L_0x002a:
                byte[] r0 = r2.toByteArray()     // Catch:{ Exception -> 0x001b }
                if (r3 == 0) goto L_0x0033
                r3.close()     // Catch:{ IOException -> 0x0039 }
            L_0x0033:
                if (r2 == 0) goto L_0x0029
                r2.close()     // Catch:{ IOException -> 0x0039 }
                goto L_0x0029
            L_0x0039:
                r1 = move-exception
                goto L_0x0029
            L_0x003b:
                r1 = move-exception
                r2 = r0
                r3 = r0
                r0 = r1
            L_0x003f:
                if (r3 == 0) goto L_0x0044
                r3.close()     // Catch:{ IOException -> 0x004a }
            L_0x0044:
                if (r2 == 0) goto L_0x0049
                r2.close()     // Catch:{ IOException -> 0x004a }
            L_0x0049:
                throw r0
            L_0x004a:
                r1 = move-exception
                goto L_0x0049
            L_0x004c:
                r1 = move-exception
                r2 = r0
                r0 = r1
                goto L_0x003f
            L_0x0050:
                r0 = move-exception
                goto L_0x003f
            L_0x0052:
                r1 = move-exception
                goto L_0x0029
            L_0x0054:
                r1 = move-exception
                r2 = r0
                r3 = r0
                goto L_0x001c
            L_0x0058:
                r1 = move-exception
                r2 = r0
                goto L_0x001c
            */
            throw new UnsupportedOperationException("Method not decompiled: com.umeng.socialize.media.g.e.b(java.io.File):byte[]");
        }
    }

    /* compiled from: UMImage */
    static class h extends c {
        private String b = null;

        public h(String str) {
            this.b = str;
        }

        public File a() {
            Exception e;
            File file;
            try {
                file = this.f2304a.a(com.umeng.socialize.d.b.a.c(this.b));
                try {
                    FileOutputStream fileOutputStream = new FileOutputStream(file);
                    fileOutputStream.write(c());
                    fileOutputStream.flush();
                    fileOutputStream.close();
                } catch (Exception e2) {
                    e = e2;
                    e.printStackTrace();
                    return file;
                }
            } catch (Exception e3) {
                Exception exc = e3;
                file = null;
                e = exc;
            }
            return file;
        }

        public String b() {
            return this.b;
        }

        public byte[] c() {
            return com.umeng.socialize.d.b.d.a(this.b);
        }

        public Bitmap d() {
            byte[] c = c();
            if (c != null) {
                return BitmapFactory.decodeByteArray(c, 0, c.length);
            }
            return null;
        }
    }

    /* compiled from: UMImage */
    static class a extends c {
        private d b = new d();
        private byte[] c;

        public a(byte[] bArr) {
            this.c = bArr;
        }

        public File a() {
            try {
                return a(this.c, this.b.a(e()));
            } catch (IOException e) {
                com.umeng.socialize.utils.g.b("Sorry cannot setImage..[" + e.toString() + "]");
                return null;
            }
        }

        public String b() {
            return null;
        }

        public byte[] c() {
            return this.c;
        }

        public Bitmap d() {
            if (this.c != null) {
                return BitmapFactory.decodeByteArray(this.c, 0, this.c.length);
            }
            return null;
        }

        public String e() {
            return com.umeng.socialize.d.b.a.c(String.valueOf(System.currentTimeMillis()));
        }

        /* JADX WARNING: Removed duplicated region for block: B:14:0x001b A[SYNTHETIC, Splitter:B:14:0x001b] */
        /* JADX WARNING: Removed duplicated region for block: B:19:0x0024 A[SYNTHETIC, Splitter:B:19:0x0024] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private java.io.File a(byte[] r4, java.io.File r5) {
            /*
                r3 = this;
                r2 = 0
                java.io.FileOutputStream r0 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0014, all -> 0x0021 }
                r0.<init>(r5)     // Catch:{ Exception -> 0x0014, all -> 0x0021 }
                java.io.BufferedOutputStream r1 = new java.io.BufferedOutputStream     // Catch:{ Exception -> 0x0014, all -> 0x0021 }
                r1.<init>(r0)     // Catch:{ Exception -> 0x0014, all -> 0x0021 }
                r1.write(r4)     // Catch:{ Exception -> 0x002f }
                if (r1 == 0) goto L_0x0013
                r1.close()     // Catch:{ IOException -> 0x0028 }
            L_0x0013:
                return r5
            L_0x0014:
                r0 = move-exception
                r1 = r2
            L_0x0016:
                r0.printStackTrace()     // Catch:{ all -> 0x002c }
                if (r1 == 0) goto L_0x0013
                r1.close()     // Catch:{ IOException -> 0x001f }
                goto L_0x0013
            L_0x001f:
                r0 = move-exception
                goto L_0x0013
            L_0x0021:
                r0 = move-exception
            L_0x0022:
                if (r2 == 0) goto L_0x0027
                r2.close()     // Catch:{ IOException -> 0x002a }
            L_0x0027:
                throw r0
            L_0x0028:
                r0 = move-exception
                goto L_0x0013
            L_0x002a:
                r1 = move-exception
                goto L_0x0027
            L_0x002c:
                r0 = move-exception
                r2 = r1
                goto L_0x0022
            L_0x002f:
                r0 = move-exception
                goto L_0x0016
            */
            throw new UnsupportedOperationException("Method not decompiled: com.umeng.socialize.media.g.a.a(byte[], java.io.File):java.io.File");
        }
    }

    /* renamed from: com.umeng.socialize.media.g$g  reason: collision with other inner class name */
    /* compiled from: UMImage */
    static class C0051g extends c {
        private Context b;
        private int c = 0;

        public C0051g(Context context, int i) {
            this.b = context;
            this.c = i;
        }

        /* JADX WARNING: Removed duplicated region for block: B:15:0x0039 A[SYNTHETIC, Splitter:B:15:0x0039] */
        /* JADX WARNING: Removed duplicated region for block: B:18:0x003e A[Catch:{ IOException -> 0x0056 }] */
        /* JADX WARNING: Removed duplicated region for block: B:34:0x005f A[SYNTHETIC, Splitter:B:34:0x005f] */
        /* JADX WARNING: Removed duplicated region for block: B:37:0x0064 A[Catch:{ IOException -> 0x0068 }] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.io.File a() {
            /*
                r7 = this;
                r1 = 0
                android.content.Context r0 = r7.b     // Catch:{ IOException -> 0x0072, all -> 0x005b }
                android.content.res.Resources r0 = r0.getResources()     // Catch:{ IOException -> 0x0072, all -> 0x005b }
                int r2 = r7.c     // Catch:{ IOException -> 0x0072, all -> 0x005b }
                android.content.res.AssetFileDescriptor r0 = r0.openRawResourceFd(r2)     // Catch:{ IOException -> 0x0072, all -> 0x005b }
                java.io.FileInputStream r2 = r0.createInputStream()     // Catch:{ IOException -> 0x0072, all -> 0x005b }
                com.umeng.socialize.media.g$d r0 = r7.f2304a     // Catch:{ IOException -> 0x0076, all -> 0x006d }
                java.lang.String r3 = r2.toString()     // Catch:{ IOException -> 0x0076, all -> 0x006d }
                java.lang.String r3 = com.umeng.socialize.d.b.a.c(r3)     // Catch:{ IOException -> 0x0076, all -> 0x006d }
                java.io.File r0 = r0.a(r3)     // Catch:{ IOException -> 0x0076, all -> 0x006d }
                java.io.FileOutputStream r3 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x0076, all -> 0x006d }
                r3.<init>(r0)     // Catch:{ IOException -> 0x0076, all -> 0x006d }
                r4 = 4096(0x1000, float:5.74E-42)
                byte[] r4 = new byte[r4]     // Catch:{ IOException -> 0x0033 }
            L_0x0028:
                int r5 = r2.read(r4)     // Catch:{ IOException -> 0x0033 }
                r6 = -1
                if (r5 == r6) goto L_0x0043
                r3.write(r4)     // Catch:{ IOException -> 0x0033 }
                goto L_0x0028
            L_0x0033:
                r0 = move-exception
            L_0x0034:
                r0.printStackTrace()     // Catch:{ all -> 0x006f }
                if (r2 == 0) goto L_0x003c
                r2.close()     // Catch:{ IOException -> 0x0056 }
            L_0x003c:
                if (r3 == 0) goto L_0x0041
                r3.close()     // Catch:{ IOException -> 0x0056 }
            L_0x0041:
                r0 = r1
            L_0x0042:
                return r0
            L_0x0043:
                r3.flush()     // Catch:{ IOException -> 0x0033 }
                if (r2 == 0) goto L_0x004b
                r2.close()     // Catch:{ IOException -> 0x0051 }
            L_0x004b:
                if (r3 == 0) goto L_0x0042
                r3.close()     // Catch:{ IOException -> 0x0051 }
                goto L_0x0042
            L_0x0051:
                r1 = move-exception
                r1.printStackTrace()
                goto L_0x0042
            L_0x0056:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x0041
            L_0x005b:
                r0 = move-exception
                r2 = r1
            L_0x005d:
                if (r2 == 0) goto L_0x0062
                r2.close()     // Catch:{ IOException -> 0x0068 }
            L_0x0062:
                if (r1 == 0) goto L_0x0067
                r1.close()     // Catch:{ IOException -> 0x0068 }
            L_0x0067:
                throw r0
            L_0x0068:
                r1 = move-exception
                r1.printStackTrace()
                goto L_0x0067
            L_0x006d:
                r0 = move-exception
                goto L_0x005d
            L_0x006f:
                r0 = move-exception
                r1 = r3
                goto L_0x005d
            L_0x0072:
                r0 = move-exception
                r2 = r1
                r3 = r1
                goto L_0x0034
            L_0x0076:
                r0 = move-exception
                r3 = r1
                goto L_0x0034
            */
            throw new UnsupportedOperationException("Method not decompiled: com.umeng.socialize.media.g.C0051g.a():java.io.File");
        }

        public String b() {
            return null;
        }

        public byte[] c() {
            Drawable drawable;
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            Resources resources = this.b.getResources();
            if (Build.VERSION.SDK_INT >= 21) {
                drawable = resources.getDrawable(this.c, null);
            } else {
                drawable = resources.getDrawable(this.c);
            }
            g.a(drawable).compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
            return byteArrayOutputStream.toByteArray();
        }

        public Bitmap d() {
            return BitmapFactory.decodeResource(this.b.getResources(), this.c);
        }
    }

    /* compiled from: UMImage */
    static abstract class c implements f {

        /* renamed from: a  reason: collision with root package name */
        public d f2304a = null;

        c() {
        }

        public void a(d dVar) {
            this.f2304a = dVar;
        }
    }

    /* compiled from: UMImage */
    static class d {

        /* renamed from: a  reason: collision with root package name */
        public float f2305a = 2048.0f;
        private String b = "";

        public d(Context context) {
            try {
                this.b = context.getCacheDir().getCanonicalPath();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public d() {
        }

        public File a() throws IOException {
            String str;
            if (com.umeng.socialize.utils.d.b()) {
                str = Environment.getExternalStorageDirectory().getCanonicalPath();
            } else if (!TextUtils.isEmpty(this.b)) {
                str = this.b;
            } else {
                throw new IOException("dirpath is unknow");
            }
            File file = new File(str + "/umeng_cache/");
            if (file != null && !file.exists()) {
                file.mkdirs();
            }
            return file;
        }

        public File a(String str) throws IOException {
            com.umeng.socialize.utils.a.b();
            File file = new File(a(), str);
            if (file.exists()) {
                file.delete();
            }
            file.createNewFile();
            return file;
        }
    }

    static Bitmap a(Drawable drawable) {
        int intrinsicWidth = drawable.getIntrinsicWidth();
        int intrinsicHeight = drawable.getIntrinsicHeight();
        Bitmap createBitmap = Bitmap.createBitmap(intrinsicWidth, intrinsicHeight, drawable.getOpacity() != -1 ? Bitmap.Config.ARGB_8888 : Bitmap.Config.RGB_565);
        Canvas canvas = new Canvas(createBitmap);
        drawable.setBounds(0, 0, intrinsicWidth, intrinsicHeight);
        drawable.draw(canvas);
        return createBitmap;
    }
}
