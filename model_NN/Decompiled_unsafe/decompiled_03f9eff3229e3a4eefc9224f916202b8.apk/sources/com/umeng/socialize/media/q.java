package com.umeng.socialize.media;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import com.sina.weibo.sdk.constant.WBConstants;
import com.tencent.mm.sdk.modelmsg.WXEmojiObject;
import com.tencent.mm.sdk.modelmsg.WXImageObject;
import com.tencent.mm.sdk.modelmsg.WXMediaMessage;
import com.tencent.mm.sdk.modelmsg.WXMusicObject;
import com.tencent.mm.sdk.modelmsg.WXTextObject;
import com.tencent.mm.sdk.modelmsg.WXVideoObject;
import com.tencent.mm.sdk.modelmsg.WXWebpageObject;
import com.umeng.socialize.ShareContent;
import com.umeng.socialize.utils.a;
import com.umeng.socialize.utils.c;
import com.umeng.socialize.utils.g;
import java.io.ByteArrayOutputStream;
import java.io.File;

/* compiled from: WeiXinShareContent */
public class q {

    /* renamed from: a  reason: collision with root package name */
    public String f2306a;
    private final String b = "分享到微信";
    private WXMediaMessage c = null;
    private final int d = 150;
    private final int e = 32768;
    private final int f = 512;
    private final int g = 1024;
    private ShareContent h;
    private String i;
    private String j;
    private final int k = 1;
    private final int l = 2;
    private String m;
    private UMediaObject n;

    public q(ShareContent shareContent) {
        this.h = shareContent;
        this.i = shareContent.mTitle;
        this.m = shareContent.mText;
        this.n = shareContent.mMedia;
        this.j = shareContent.mTargetUrl;
    }

    public void a() {
        if (!TextUtils.isEmpty(this.m) && this.n == null) {
            this.f2306a = "text";
        } else if (this.n != null && (this.n instanceof f)) {
            this.f2306a = "emoji";
        } else if (TextUtils.isEmpty(this.m) && this.n != null && (this.n instanceof g)) {
            this.f2306a = WBConstants.GAME_PARAMS_GAME_IMAGE_URL;
        } else if (this.n != null && (this.n instanceof p)) {
            this.f2306a = "music";
        } else if (this.n != null && (this.n instanceof h)) {
            this.f2306a = "video";
        } else if (!TextUtils.isEmpty(this.m) && this.n != null && (this.n instanceof g)) {
            this.f2306a = "text_image";
        }
    }

    public WXMediaMessage b() {
        WXMediaMessage wXMediaMessage = null;
        if (this.h.mMedia == null) {
            if (!TextUtils.isEmpty(this.h.mText)) {
                g.a("--->", "text share..");
                wXMediaMessage = e();
            }
        } else if (this.h.mMedia instanceof f) {
            wXMediaMessage = c();
        } else if (TextUtils.isEmpty(this.h.mText) && (this.h.mMedia instanceof g)) {
            g.c("weixin", "picture share");
            wXMediaMessage = f();
        } else if (this.h.mMedia instanceof p) {
            wXMediaMessage = d();
        } else if (this.h.mMedia instanceof h) {
            wXMediaMessage = g();
        } else if (!TextUtils.isEmpty(this.h.mText) && (this.h.mMedia instanceof g)) {
            g.c("图文分享..");
            wXMediaMessage = h();
        }
        if (wXMediaMessage != null) {
            byte[] bArr = wXMediaMessage.thumbData;
            if (bArr != null && bArr.length > 32768) {
                wXMediaMessage.thumbData = a(bArr, 32768);
                g.c("压缩之后缩略图大小 : " + (wXMediaMessage.thumbData.length / 1024) + " KB.");
            }
            if (TextUtils.isEmpty(wXMediaMessage.title) || wXMediaMessage.title.getBytes().length < 512) {
                this.i = "分享到微信";
            } else {
                wXMediaMessage.title = new String(wXMediaMessage.title.getBytes(), 0, 512);
            }
            if (!TextUtils.isEmpty(wXMediaMessage.description) && wXMediaMessage.description.getBytes().length >= 1024) {
                wXMediaMessage.description = new String(wXMediaMessage.description.getBytes(), 0, 1024);
            }
        }
        return wXMediaMessage;
    }

    private byte[] a(byte[] bArr, int i2) {
        boolean z = false;
        if (bArr != null && bArr.length >= i2) {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            Bitmap decodeByteArray = BitmapFactory.decodeByteArray(bArr, 0, bArr.length);
            int i3 = 1;
            while (!z && i3 <= 10) {
                int pow = (int) (Math.pow(0.8d, (double) i3) * 100.0d);
                g.c("quality = " + pow);
                decodeByteArray.compress(Bitmap.CompressFormat.JPEG, pow, byteArrayOutputStream);
                g.c("WeiXin Thumb Size = " + (byteArrayOutputStream.toByteArray().length / 1024) + " KB");
                if (byteArrayOutputStream == null || byteArrayOutputStream.size() >= i2) {
                    byteArrayOutputStream.reset();
                    i3++;
                } else {
                    z = true;
                }
            }
            if (byteArrayOutputStream != null) {
                bArr = byteArrayOutputStream.toByteArray();
                if (!decodeByteArray.isRecycled()) {
                    decodeByteArray.recycle();
                }
                if (bArr != null && bArr.length <= 0) {
                    g.b("### 您的原始图片太大,导致缩略图压缩过后还大于32KB,请将分享到微信的图片进行适当缩小.");
                }
            }
        }
        return bArr;
    }

    private WXMediaMessage c() {
        f fVar = (f) this.h.mMedia;
        g gVar = fVar.k;
        String file = gVar.j().toString();
        WXEmojiObject wXEmojiObject = new WXEmojiObject();
        if (fVar.k.c()) {
            file = a.d(gVar.b());
            if (!new File(file).exists()) {
                a.a(gVar.b(), 150, 150);
            }
        }
        wXEmojiObject.emojiPath = file;
        WXMediaMessage wXMediaMessage = new WXMediaMessage();
        wXMediaMessage.mediaObject = wXEmojiObject;
        if (fVar.j() != null) {
            wXMediaMessage.thumbData = fVar.j.i();
        } else if (!TextUtils.isEmpty(fVar.e())) {
            Bitmap a2 = a.a(fVar.e(), 150, 150);
            wXMediaMessage.thumbData = a.a(a2);
            a2.recycle();
        } else {
            wXMediaMessage.thumbData = fVar.k.i();
        }
        wXMediaMessage.title = this.i;
        wXMediaMessage.description = this.h.mText;
        return wXMediaMessage;
    }

    private WXMediaMessage d() {
        byte[] l2;
        p pVar = (p) this.h.mMedia;
        WXMusicObject wXMusicObject = new WXMusicObject();
        if (!TextUtils.isEmpty(pVar.f())) {
            wXMusicObject.musicUrl = pVar.f();
        } else if (TextUtils.isEmpty(this.h.mTargetUrl)) {
            wXMusicObject.musicUrl = "http://dev.umeng.com";
        } else {
            wXMusicObject.musicUrl = this.h.mTargetUrl;
        }
        wXMusicObject.musicDataUrl = pVar.b();
        if (!TextUtils.isEmpty(pVar.o())) {
            wXMusicObject.musicLowBandDataUrl = pVar.o();
        }
        if (!TextUtils.isEmpty(pVar.k())) {
            wXMusicObject.musicLowBandUrl = pVar.k();
        }
        WXMediaMessage i2 = i();
        i2.mediaObject = wXMusicObject;
        if (!TextUtils.isEmpty(pVar.d())) {
            i2.title = pVar.d();
        } else if (TextUtils.isEmpty(this.h.mTitle)) {
            i2.title = "分享音频";
        } else {
            i2.title = this.h.mTitle;
        }
        i2.description = this.h.mText;
        i2.mediaObject = wXMusicObject;
        if (pVar.e() != null && (!"".equals(pVar.e()) || pVar.e() != null)) {
            if (pVar.n() != null) {
                l2 = pVar.n().l();
            } else {
                l2 = !TextUtils.isEmpty(pVar.e()) ? new g(c.a(), pVar.e()).l() : null;
            }
            if (l2 != null) {
                g.c("share with thumb");
                i2.thumbData = l2;
            }
        }
        return i2;
    }

    private WXMediaMessage e() {
        WXTextObject wXTextObject = new WXTextObject();
        wXTextObject.text = this.h.mText;
        WXMediaMessage wXMediaMessage = new WXMediaMessage();
        wXMediaMessage.mediaObject = wXTextObject;
        wXMediaMessage.description = this.h.mText;
        wXMediaMessage.title = this.i;
        return wXMediaMessage;
    }

    private WXMediaMessage f() {
        g gVar = (g) this.h.mMedia;
        WXImageObject wXImageObject = new WXImageObject();
        WXMediaMessage i2 = i();
        if (gVar.c()) {
            wXImageObject.imageUrl = gVar.k();
        } else if (gVar.j() != null) {
            Bitmap m2 = gVar.m();
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            if (m2 == null) {
                return i2;
            }
            m2.compress(Bitmap.CompressFormat.JPEG, 80, byteArrayOutputStream);
            wXImageObject.imageData = byteArrayOutputStream.toByteArray();
            i2.mediaObject = wXImageObject;
            if (m2 != null && !m2.isRecycled()) {
                m2.recycle();
            }
            return i2;
        }
        wXImageObject.imageData = gVar.l();
        i2.mediaObject = wXImageObject;
        return i2;
    }

    private WXMediaMessage g() {
        byte[] l2;
        h hVar = (h) this.h.mMedia;
        WXVideoObject wXVideoObject = new WXVideoObject();
        wXVideoObject.videoUrl = hVar.b();
        if (!TextUtils.isEmpty(hVar.k())) {
            wXVideoObject.videoLowBandUrl = hVar.k();
        }
        WXMediaMessage i2 = i();
        i2.mediaObject = wXVideoObject;
        if (TextUtils.isEmpty(this.h.mTitle)) {
            i2.title = "分享视频";
        } else {
            i2.title = this.h.mTitle;
        }
        i2.description = this.h.mText;
        if (!TextUtils.isEmpty(hVar.e())) {
            l2 = new g(c.a(), hVar.e()).l();
        } else {
            l2 = hVar.o() != null ? hVar.o().l() : null;
        }
        if (l2 != null && l2.length > 0) {
            i2.thumbData = l2;
        }
        return i2;
    }

    private WXMediaMessage h() {
        g gVar = (g) this.h.mMedia;
        if (TextUtils.isEmpty(this.j)) {
            this.j = "http://www.umeng.com";
        }
        WXWebpageObject wXWebpageObject = new WXWebpageObject();
        wXWebpageObject.webpageUrl = this.j;
        WXMediaMessage i2 = i();
        i2.title = this.i;
        i2.description = this.h.mText;
        i2.mediaObject = wXWebpageObject;
        return i2;
    }

    private WXMediaMessage i() {
        String str;
        g n2;
        String k2;
        String str2 = null;
        if (this.h.mMedia instanceof g) {
            g gVar = (g) this.h.mMedia;
            if (gVar.j() != null) {
                String file = gVar.j().toString();
                g.c("localPath", file);
                str2 = file;
                k2 = null;
            } else {
                k2 = gVar.k();
            }
            String str3 = k2;
            str = str2;
            str2 = str3;
        } else {
            if (this.h.mMedia instanceof h) {
                g o = ((h) this.h.mMedia).o();
                if (o != null) {
                    if (o == null || o.j() == null) {
                        String k3 = o.k();
                        str = null;
                        str2 = k3;
                    } else {
                        str = o.j().toString();
                    }
                }
            } else if ((this.h.mMedia instanceof p) && (n2 = ((p) this.h.mMedia).n()) != null) {
                if (n2 == null || n2.j() == null) {
                    String k4 = n2.k();
                    str = null;
                    str2 = k4;
                } else {
                    str = n2.j().toString();
                }
            }
            str = null;
        }
        WXMediaMessage wXMediaMessage = new WXMediaMessage();
        if (!TextUtils.isEmpty(str2)) {
            wXMediaMessage.thumbData = a.a(a.a(str2, 150, 150));
        } else if (!TextUtils.isEmpty(str)) {
            Bitmap a2 = a(str);
            g.c("localBitmap", a2 + "");
            wXMediaMessage.setThumbImage(a2);
            if (a2 != null && !a2.isRecycled()) {
                a2.recycle();
            }
        }
        return wXMediaMessage;
    }

    private Bitmap a(String str) {
        g.c("imagePath", str);
        if (!a.b(str)) {
            return null;
        }
        g.c("imagePath", "iamge exist:" + str);
        if (a.a(str, 32768)) {
            Bitmap b2 = a.b(str, 150, 150);
            g.c("imagePath", "bitmap exist resize:" + b2);
            return b2;
        }
        Bitmap c2 = a.c(str);
        g.c("imagePath", "bitmap exist:" + c2);
        return c2;
    }
}
