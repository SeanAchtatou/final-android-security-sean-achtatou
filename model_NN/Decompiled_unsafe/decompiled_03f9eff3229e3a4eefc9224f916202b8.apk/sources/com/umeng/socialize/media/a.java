package com.umeng.socialize.media;

import android.text.TextUtils;

/* compiled from: BaseMediaObject */
public abstract class a implements UMediaObject {

    /* renamed from: a  reason: collision with root package name */
    public String f2300a = null;
    protected String b = "";
    protected String c = "";
    protected String d = "";
    protected String e = "";
    protected String f = "";
    protected String g = "";
    protected int h = 0;
    protected String i = "";

    public a() {
    }

    public a(String str) {
        this.b = str;
    }

    public String a() {
        return this.i;
    }

    public String b() {
        return this.b;
    }

    public boolean c() {
        if (TextUtils.isEmpty(this.b)) {
            return false;
        }
        return true;
    }

    public String d() {
        return this.c;
    }

    public void a(String str) {
        this.c = str;
    }

    public String e() {
        return this.d;
    }

    public void b(String str) {
        this.e = str;
    }

    public String f() {
        return this.e;
    }

    public String toString() {
        return "BaseMediaObject [media_url=" + this.b + ", qzone_title=" + this.c + ", qzone_thumb=" + this.d + "]";
    }
}
