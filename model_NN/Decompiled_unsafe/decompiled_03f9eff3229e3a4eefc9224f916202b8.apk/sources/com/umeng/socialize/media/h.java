package com.umeng.socialize.media;

import com.umeng.socialize.d.b.e;
import com.umeng.socialize.media.UMediaObject;
import java.util.HashMap;
import java.util.Map;

/* compiled from: UMVideo */
public class h extends a {
    private g j;
    private String k;
    private String l;
    private String m;
    private String n;
    private int o;

    public int j() {
        return this.o;
    }

    public String k() {
        return this.k;
    }

    public String l() {
        return this.l;
    }

    public String m() {
        return this.m;
    }

    public String n() {
        return this.n;
    }

    public UMediaObject.a g() {
        return UMediaObject.a.VEDIO;
    }

    public final Map<String, Object> h() {
        HashMap hashMap = new HashMap();
        if (c()) {
            hashMap.put(e.e, this.b);
            hashMap.put(e.f, g());
        }
        return hashMap;
    }

    public byte[] i() {
        if (this.j != null) {
            return this.j.i();
        }
        return null;
    }

    public String toString() {
        return "UMVedio [media_url=" + this.b + ", qzone_title=" + this.c + ", qzone_thumb=" + this.d + "media_url=" + this.b + ", qzone_title=" + this.c + ", qzone_thumb=" + this.d + "]";
    }

    public g o() {
        return this.j;
    }
}
