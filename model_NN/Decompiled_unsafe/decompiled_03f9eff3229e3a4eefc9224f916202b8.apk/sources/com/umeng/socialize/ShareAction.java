package com.umeng.socialize;

import android.app.Activity;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.view.View;
import com.umeng.socialize.c.b;
import com.umeng.socialize.media.f;
import com.umeng.socialize.media.g;
import com.umeng.socialize.media.h;
import com.umeng.socialize.media.p;
import com.umeng.socialize.shareboard.a;
import com.umeng.socialize.utils.ShareBoardlistener;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class ShareAction {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public ShareContent f2193a = new ShareContent();
    private String b = null;
    private b c = null;
    /* access modifiers changed from: private */
    public UMShareListener d = null;
    private ShareBoardlistener e = null;
    private Activity f;
    /* access modifiers changed from: private */
    public List<b> g = null;
    private List<a> h = new ArrayList();
    /* access modifiers changed from: private */
    public List<ShareContent> i = new ArrayList();
    /* access modifiers changed from: private */
    public List<UMShareListener> j = new ArrayList();
    private int k = 80;
    private View l = null;
    private ShareBoardlistener m = new b(this);
    private ShareBoardlistener n = new c(this);

    public ShareAction(Activity activity) {
        if (activity != null) {
            this.f = (Activity) new WeakReference(activity).get();
        }
    }

    public void openBoard() {
    }

    public ShareContent getShareContent() {
        return this.f2193a;
    }

    public String getFrom() {
        return this.b;
    }

    public b getPlatform() {
        return this.c;
    }

    public ShareAction setPlatform(b bVar) {
        this.c = bVar;
        return this;
    }

    public ShareAction setCallback(UMShareListener uMShareListener) {
        this.d = uMShareListener;
        return this;
    }

    public ShareAction setShareboardclickCallback(ShareBoardlistener shareBoardlistener) {
        this.e = shareBoardlistener;
        return this;
    }

    public ShareAction setShareContent(ShareContent shareContent) {
        this.f2193a = shareContent;
        return this;
    }

    public ShareAction setDisplayList(b... bVarArr) {
        this.g = Arrays.asList(bVarArr);
        this.h.clear();
        for (b a2 : this.g) {
            this.h.add(a2.a());
        }
        return this;
    }

    public ShareAction setListenerList(UMShareListener... uMShareListenerArr) {
        this.j = Arrays.asList(uMShareListenerArr);
        return this;
    }

    public ShareAction setContentList(ShareContent... shareContentArr) {
        if (shareContentArr == null || Arrays.asList(shareContentArr).size() == 0) {
            ShareContent shareContent = new ShareContent();
            shareContent.mText = "友盟分享";
            this.i.add(shareContent);
        } else {
            this.i = Arrays.asList(shareContentArr);
        }
        return this;
    }

    public ShareAction addButton(String str, String str2, String str3, String str4) {
        this.h.add(b.a(str, str2, str3, str4, 0));
        return this;
    }

    public ShareAction withText(String str) {
        this.f2193a.mText = str;
        return this;
    }

    public ShareAction withTitle(String str) {
        this.f2193a.mTitle = str;
        return this;
    }

    public ShareAction withTargetUrl(String str) {
        this.f2193a.mTargetUrl = str;
        return this;
    }

    public ShareAction withMedia(g gVar) {
        this.f2193a.mMedia = gVar;
        return this;
    }

    public ShareAction withMedia(f fVar) {
        this.f2193a.mMedia = fVar;
        return this;
    }

    public ShareAction withFollow(String str) {
        this.f2193a.mFollow = str;
        return this;
    }

    public ShareAction withExtra(g gVar) {
        this.f2193a.mExtra = gVar;
        return this;
    }

    public ShareAction withMedia(p pVar) {
        this.f2193a.mMedia = pVar;
        return this;
    }

    public ShareAction withMedia(h hVar) {
        this.f2193a.mMedia = hVar;
        return this;
    }

    public ShareAction withShareBoardDirection(View view, int i2) {
        this.k = i2;
        this.l = view;
        return this;
    }

    public void share() {
        UMShareAPI.get(this.f).doShare(this.f, this, this.d);
    }

    public void open() {
        if (this.h.size() != 0) {
            HashMap hashMap = new HashMap();
            hashMap.put("listener", this.d);
            hashMap.put("content", this.f2193a);
            com.umeng.socialize.shareboard.b bVar = new com.umeng.socialize.shareboard.b(this.f, this.h);
            if (this.e == null) {
                bVar.a(this.n);
            } else {
                bVar.a(this.e);
            }
            bVar.setFocusable(true);
            bVar.setBackgroundDrawable(new BitmapDrawable());
            if (this.l == null) {
                this.l = this.f.getWindow().getDecorView();
            }
            bVar.showAtLocation(this.l, this.k, 0, 0);
            return;
        }
        this.h.add(b.WEIXIN.a());
        this.h.add(b.WEIXIN_CIRCLE.a());
        this.h.add(b.SINA.a());
        this.h.add(b.QQ.a());
        HashMap hashMap2 = new HashMap();
        hashMap2.put("listener", this.d);
        hashMap2.put("content", this.f2193a);
        com.umeng.socialize.shareboard.b bVar2 = new com.umeng.socialize.shareboard.b(this.f, this.h);
        if (this.i.size() == 0) {
            if (this.e == null) {
                bVar2.a(this.m);
            } else {
                bVar2.a(this.e);
            }
        } else if (this.e == null) {
            bVar2.a(this.n);
        } else {
            bVar2.a(this.e);
        }
        bVar2.setFocusable(true);
        bVar2.setBackgroundDrawable(new BitmapDrawable());
        if (this.l == null) {
            this.l = this.f.getWindow().getDecorView();
        }
        bVar2.showAtLocation(this.l, 80, 0, 0);
    }

    public static Rect locateView(View view) {
        int[] iArr = new int[2];
        if (view == null) {
            return null;
        }
        try {
            view.getLocationOnScreen(iArr);
            Rect rect = new Rect();
            rect.left = iArr[0];
            rect.top = iArr[1];
            rect.right = rect.left + view.getWidth();
            rect.bottom = rect.top + view.getHeight();
            return rect;
        } catch (NullPointerException e2) {
            return null;
        }
    }
}
