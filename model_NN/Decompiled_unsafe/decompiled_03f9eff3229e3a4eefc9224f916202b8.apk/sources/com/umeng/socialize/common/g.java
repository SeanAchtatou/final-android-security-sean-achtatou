package com.umeng.socialize.common;

import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import java.util.HashMap;
import java.util.Map;

/* compiled from: ResContainer */
public final class g {

    /* renamed from: a  reason: collision with root package name */
    private static g f2221a = null;
    private static String d = "";
    private Map<String, Integer> b = new HashMap();
    private Context c = null;

    private g(Context context) {
        this.c = context.getApplicationContext();
    }

    public static synchronized g a(Context context) {
        g gVar;
        synchronized (g.class) {
            if (f2221a == null) {
                f2221a = new g(context);
            }
            gVar = f2221a;
        }
        return gVar;
    }

    public int a(String str) {
        return a(this.c, "layout", str);
    }

    public int b(String str) {
        return a(this.c, "id", str);
    }

    public int c(String str) {
        return a(this.c, "drawable", str);
    }

    public int d(String str) {
        return a(this.c, "style", str);
    }

    public int e(String str) {
        return a(this.c, "color", str);
    }

    public int f(String str) {
        return a(this.c, "dimen", str);
    }

    public static int a(Context context, String str, String str2) {
        Resources resources = context.getResources();
        if (TextUtils.isEmpty(d)) {
            d = context.getPackageName();
        }
        int identifier = resources.getIdentifier(str2, str, d);
        if (identifier > 0) {
            return identifier;
        }
        throw new RuntimeException("获取资源ID失败:(packageName=" + d + " type=" + str + " name=" + str2);
    }

    public static String a(Context context, String str) {
        return context.getString(a(context, "string", str));
    }
}
