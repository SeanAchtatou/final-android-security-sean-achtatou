package com.umeng.socialize.common;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import com.umeng.socialize.Config;
import com.umeng.socialize.utils.h;

/* compiled from: QueuedWork */
public class b {

    /* renamed from: a  reason: collision with root package name */
    private static Handler f2215a = new Handler(Looper.getMainLooper());

    public static void a(Runnable runnable) {
        f2215a.post(runnable);
    }

    public static void b(Runnable runnable) {
        HandlerThread handlerThread = new HandlerThread("umengsocial", 10);
        handlerThread.start();
        new Handler(handlerThread.getLooper()).post(runnable);
    }

    public static void c(Runnable runnable) {
    }

    /* compiled from: QueuedWork */
    public static abstract class a<T> extends C0049b {

        /* renamed from: a  reason: collision with root package name */
        Dialog f2216a = null;

        public a(Context context) {
            if ((context instanceof Activity) && Config.dialogSwitch) {
                if (Config.dialog != null) {
                    this.f2216a = Config.dialog;
                } else {
                    this.f2216a = new ProgressDialog(context);
                }
                this.f2216a.setOwnerActivity((Activity) context);
                this.f2216a.setOnKeyListener(new c(this));
            }
        }

        /* access modifiers changed from: protected */
        public void a(Object obj) {
            super.a(obj);
            h.a(this.f2216a);
        }

        /* access modifiers changed from: protected */
        public void a_() {
            super.a_();
            h.b(this.f2216a);
        }
    }

    /* renamed from: com.umeng.socialize.common.b$b  reason: collision with other inner class name */
    /* compiled from: QueuedWork */
    public static abstract class C0049b<Result> {
        protected Runnable b;

        /* access modifiers changed from: protected */
        public abstract Result c();

        /* access modifiers changed from: protected */
        public void a_() {
        }

        /* access modifiers changed from: protected */
        public void a(Result result) {
        }

        public final C0049b<Result> d() {
            this.b = new d(this);
            b.a(new f(this));
            b.b(this.b);
            return this;
        }
    }
}
