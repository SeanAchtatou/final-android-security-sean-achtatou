package com.umeng.socialize.common;

import android.content.DialogInterface;
import android.view.KeyEvent;
import com.umeng.socialize.common.b;

/* compiled from: QueuedWork */
class c implements DialogInterface.OnKeyListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ b.a f2217a;

    c(b.a aVar) {
        this.f2217a = aVar;
    }

    public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
        if (i != 4 || keyEvent.getRepeatCount() != 0) {
            return false;
        }
        b.c(this.f2217a.b);
        return false;
    }
}
