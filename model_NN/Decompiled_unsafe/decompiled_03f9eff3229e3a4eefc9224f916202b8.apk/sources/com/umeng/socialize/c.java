package com.umeng.socialize;

import com.umeng.socialize.c.b;
import com.umeng.socialize.shareboard.a;
import com.umeng.socialize.utils.ShareBoardlistener;

/* compiled from: ShareAction */
class c implements ShareBoardlistener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ShareAction f2207a;

    c(ShareAction shareAction) {
        this.f2207a = shareAction;
    }

    public void a(a aVar, b bVar) {
        ShareContent shareContent;
        int indexOf = this.f2207a.g.indexOf(bVar);
        int size = this.f2207a.i.size();
        if (size != 0) {
            if (indexOf < size) {
                shareContent = (ShareContent) this.f2207a.i.get(indexOf);
            } else {
                shareContent = (ShareContent) this.f2207a.i.get(size - 1);
            }
            ShareContent unused = this.f2207a.f2193a = shareContent;
        }
        int size2 = this.f2207a.j.size();
        if (size2 != 0) {
            if (indexOf < size2) {
                UMShareListener unused2 = this.f2207a.d = (UMShareListener) this.f2207a.j.get(indexOf);
            } else {
                UMShareListener unused3 = this.f2207a.d = (UMShareListener) this.f2207a.j.get(size2 - 1);
            }
        }
        this.f2207a.setPlatform(bVar);
        this.f2207a.share();
    }
}
