package com.umeng.socialize.view;

import com.umeng.socialize.c.b;
import java.util.Map;

public interface UMFriendListener {
    public static final UMFriendListener b = new b();

    void a(b bVar, int i, Throwable th);

    void a(b bVar, int i, Map<String, Object> map);
}
