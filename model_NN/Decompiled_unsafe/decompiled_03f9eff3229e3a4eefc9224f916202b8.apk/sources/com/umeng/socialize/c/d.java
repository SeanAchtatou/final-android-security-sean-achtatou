package com.umeng.socialize.c;

import android.location.Location;
import java.io.Serializable;

/* compiled from: UMLocation */
public class d implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private double f2212a;
    private double b;

    public d(double d, double d2) {
        this.f2212a = d;
        this.b = d2;
    }

    public String toString() {
        return "(" + this.b + "," + this.f2212a + ")";
    }

    public static d a(Location location) {
        try {
            if (!(location.getLatitude() == 0.0d || location.getLongitude() == 0.0d)) {
                return new d(location.getLatitude(), location.getLongitude());
            }
        } catch (Exception e) {
        }
        return null;
    }
}
