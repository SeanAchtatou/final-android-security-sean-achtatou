package com.umeng.socialize;

import android.app.Dialog;

public class Config {
    public static String Descriptor = "com.umeng.share";
    public static String EntityKey = null;
    public static String EntityName = null;
    public static boolean IsToastTip = true;
    public static int LinkedInProfileScope = 0;
    public static int LinkedInShareCode = 0;
    public static boolean OpenEditor = true;
    public static String QQAPPNAME = "";
    public static int QQWITHQZONE = 0;
    public static String REDIRECT_URL = "http://sns.whalecloud.com";
    public static String SessionId = null;
    public static boolean ShareLocation = true;
    public static String UID = null;
    public static int UseCocos = 0;
    public static boolean WBBYQQ = true;

    /* renamed from: a  reason: collision with root package name */
    private static String f2192a = "";
    public static String appName = null;
    private static String b = "";
    public static int connectionTimeOut = 30000;
    public static Dialog dialog = null;
    public static boolean dialogSwitch = true;
    public static float imageSize = 3072.0f;
    public static boolean isIntentShareFB = false;
    public static boolean isloadUrl = false;
    public static final boolean mEncrypt = true;
    public static int readSocketTimeOut = 30000;
    public static boolean showShareBoardOnTop = false;

    public static String getAdapterSDKVersion() {
        return b;
    }

    public static String getAdapterSDK() {
        return f2192a;
    }

    public static void setAdapterSDKInfo(String str, String str2) {
        f2192a = str;
        b = str2;
    }
}
