package com.c.a.c.a;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/* compiled from: CompatibleAsyncTask */
class b implements ThreadFactory {

    /* renamed from: a  reason: collision with root package name */
    private final AtomicInteger f451a = new AtomicInteger(1);

    b() {
    }

    public Thread newThread(Runnable runnable) {
        return new Thread(runnable, "CompatibleAsyncTask #" + this.f451a.getAndIncrement());
    }
}
