package com.c.a.c;

import java.util.ArrayList;
import java.util.List;

/* compiled from: CharsetUtils */
public class a {

    /* renamed from: a  reason: collision with root package name */
    public static final List<String> f444a = new ArrayList();

    public static String a(String str, String str2, int i) {
        try {
            return new String(str.getBytes(a(str, i)), str2);
        } catch (Throwable th) {
            c.a(th);
            return str;
        }
    }

    public static String a(String str, int i) {
        for (String next : f444a) {
            if (b(str, next, i)) {
                return next;
            }
        }
        return "ISO-8859-1";
    }

    public static boolean b(String str, String str2, int i) {
        try {
            if (str.length() > i) {
                str = str.substring(0, i);
            }
            return str.equals(new String(str.getBytes(str2), str2));
        } catch (Throwable th) {
            return false;
        }
    }

    static {
        f444a.add("ISO-8859-1");
        f444a.add("GB2312");
        f444a.add("GBK");
        f444a.add("GB18030");
        f444a.add("US-ASCII");
        f444a.add("ASCII");
        f444a.add("ISO-2022-KR");
        f444a.add("ISO-8859-2");
        f444a.add("ISO-2022-JP");
        f444a.add("ISO-2022-JP-2");
        f444a.add("UTF-8");
    }
}
