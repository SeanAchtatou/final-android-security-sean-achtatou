package com.c.a.c.a;

import android.os.Process;
import com.c.a.c.a.a;

/* compiled from: CompatibleAsyncTask */
class c extends a.e<Params, Result> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f452a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    c(a aVar) {
        super(null);
        this.f452a = aVar;
    }

    public Result call() throws Exception {
        this.f452a.k.set(true);
        Process.setThreadPriority(10);
        return this.f452a.d(this.f452a.c(this.b));
    }
}
