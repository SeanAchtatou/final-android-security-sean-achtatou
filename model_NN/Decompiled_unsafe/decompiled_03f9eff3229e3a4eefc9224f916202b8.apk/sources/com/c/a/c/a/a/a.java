package com.c.a.c.a.a;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.AbstractCollection;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;

/* compiled from: ArrayDeque */
public class a<E> extends AbstractCollection<E> implements b<E>, Serializable, Cloneable {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public transient Object[] f447a = new Object[16];
    /* access modifiers changed from: private */
    public transient int b;
    /* access modifiers changed from: private */
    public transient int c;

    private void f() {
        int i = this.b;
        int length = this.f447a.length;
        int i2 = length - i;
        int i3 = length << 1;
        if (i3 < 0) {
            throw new IllegalStateException("Sorry, deque too big");
        }
        Object[] objArr = new Object[i3];
        System.arraycopy(this.f447a, i, objArr, 0, i2);
        System.arraycopy(this.f447a, 0, objArr, i2, i);
        this.f447a = objArr;
        this.b = 0;
        this.c = length;
    }

    private <T> T[] a(T[] tArr) {
        if (this.b < this.c) {
            System.arraycopy(this.f447a, this.b, tArr, 0, size());
        } else if (this.b > this.c) {
            int length = this.f447a.length - this.b;
            System.arraycopy(this.f447a, this.b, tArr, 0, length);
            System.arraycopy(this.f447a, 0, tArr, length, this.c);
        }
        return tArr;
    }

    public void a(E e) {
        if (e == null) {
            throw new NullPointerException("e == null");
        }
        this.f447a[this.c] = e;
        int length = (this.c + 1) & (this.f447a.length - 1);
        this.c = length;
        if (length == this.b) {
            f();
        }
    }

    public boolean b(E e) {
        a((Object) e);
        return true;
    }

    public E a() {
        E b2 = b();
        if (b2 != null) {
            return b2;
        }
        throw new NoSuchElementException();
    }

    public E b() {
        int i = this.b;
        E e = this.f447a[i];
        if (e == null) {
            return null;
        }
        this.f447a[i] = null;
        this.b = (i + 1) & (this.f447a.length - 1);
        return e;
    }

    public E c() {
        E e = this.f447a[this.b];
        if (e != null) {
            return e;
        }
        throw new NoSuchElementException();
    }

    public E d() {
        return this.f447a[this.b];
    }

    public boolean c(Object obj) {
        if (obj == null) {
            return false;
        }
        int length = this.f447a.length - 1;
        int i = this.b;
        while (true) {
            Object obj2 = this.f447a[i];
            if (obj2 == null) {
                return false;
            }
            if (obj.equals(obj2)) {
                a(i);
                return true;
            }
            i = (i + 1) & length;
        }
    }

    public boolean add(E e) {
        a((Object) e);
        return true;
    }

    public boolean offer(E e) {
        return b((Object) e);
    }

    public E remove() {
        return a();
    }

    public E poll() {
        return b();
    }

    public E element() {
        return c();
    }

    public E peek() {
        return d();
    }

    /* access modifiers changed from: private */
    public boolean a(int i) {
        Object[] objArr = this.f447a;
        int length = objArr.length - 1;
        int i2 = this.b;
        int i3 = this.c;
        int i4 = (i - i2) & length;
        int i5 = (i3 - i) & length;
        if (i4 >= ((i3 - i2) & length)) {
            throw new ConcurrentModificationException();
        } else if (i4 < i5) {
            if (i2 <= i) {
                System.arraycopy(objArr, i2, objArr, i2 + 1, i4);
            } else {
                System.arraycopy(objArr, 0, objArr, 1, i);
                objArr[0] = objArr[length];
                System.arraycopy(objArr, i2, objArr, i2 + 1, length - i2);
            }
            objArr[i2] = null;
            this.b = (i2 + 1) & length;
            return false;
        } else {
            if (i < i3) {
                System.arraycopy(objArr, i + 1, objArr, i, i5);
                this.c = i3 - 1;
            } else {
                System.arraycopy(objArr, i + 1, objArr, i, length - i);
                objArr[length] = objArr[0];
                System.arraycopy(objArr, 1, objArr, 0, i3);
                this.c = (i3 - 1) & length;
            }
            return true;
        }
    }

    public int size() {
        return (this.c - this.b) & (this.f447a.length - 1);
    }

    public boolean isEmpty() {
        return this.b == this.c;
    }

    public Iterator<E> iterator() {
        return new C0008a(this, null);
    }

    /* renamed from: com.c.a.c.a.a.a$a  reason: collision with other inner class name */
    /* compiled from: ArrayDeque */
    private class C0008a implements Iterator<E> {
        private int b;
        private int c;
        private int d;

        private C0008a() {
            this.b = a.this.b;
            this.c = a.this.c;
            this.d = -1;
        }

        /* synthetic */ C0008a(a aVar, C0008a aVar2) {
            this();
        }

        public boolean hasNext() {
            return this.b != this.c;
        }

        public E next() {
            if (this.b == this.c) {
                throw new NoSuchElementException();
            }
            E e = a.this.f447a[this.b];
            if (a.this.c != this.c || e == null) {
                throw new ConcurrentModificationException();
            }
            this.d = this.b;
            this.b = (this.b + 1) & (a.this.f447a.length - 1);
            return e;
        }

        public void remove() {
            if (this.d < 0) {
                throw new IllegalStateException();
            }
            if (a.this.a(this.d)) {
                this.b = (this.b - 1) & (a.this.f447a.length - 1);
                this.c = a.this.c;
            }
            this.d = -1;
        }
    }

    public boolean contains(Object obj) {
        if (obj == null) {
            return false;
        }
        int length = this.f447a.length - 1;
        int i = this.b;
        while (true) {
            Object obj2 = this.f447a[i];
            if (obj2 == null) {
                return false;
            }
            if (obj.equals(obj2)) {
                return true;
            }
            i = (i + 1) & length;
        }
    }

    public boolean remove(Object obj) {
        return c(obj);
    }

    public void clear() {
        int i = this.b;
        int i2 = this.c;
        if (i != i2) {
            this.c = 0;
            this.b = 0;
            int length = this.f447a.length - 1;
            do {
                this.f447a[i] = null;
                i = (i + 1) & length;
            } while (i != i2);
        }
    }

    public Object[] toArray() {
        return a(new Object[size()]);
    }

    public <T> T[] toArray(T[] tArr) {
        T[] tArr2;
        int size = size();
        if (tArr.length < size) {
            tArr2 = (Object[]) Array.newInstance(tArr.getClass().getComponentType(), size);
        } else {
            tArr2 = tArr;
        }
        a((Object[]) tArr2);
        if (tArr2.length > size) {
            tArr2[size] = null;
        }
        return tArr2;
    }

    /* renamed from: e */
    public a<E> clone() {
        try {
            a<E> aVar = (a) super.clone();
            aVar.f447a = a(this.f447a, 0, this.f447a.length);
            return aVar;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError();
        }
    }

    private static <T> T[] a(T[] tArr, int i, int i2) {
        int length = tArr.length;
        if (i > i2) {
            throw new IllegalArgumentException("start > end");
        } else if (i < 0 || i > length) {
            throw new ArrayIndexOutOfBoundsException();
        } else {
            int i3 = i2 - i;
            int min = Math.min(i3, length - i);
            T[] tArr2 = (Object[]) Array.newInstance(tArr.getClass().getComponentType(), i3);
            System.arraycopy(tArr, i, tArr2, 0, min);
            return tArr2;
        }
    }
}
