package com.c.a.c.a;

import com.c.a.c.a.a;

/* compiled from: CompatibleAsyncTask */
class e implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a.c f454a;
    private final /* synthetic */ Runnable b;

    e(a.c cVar, Runnable runnable) {
        this.f454a = cVar;
        this.b = runnable;
    }

    public void run() {
        try {
            this.b.run();
        } finally {
            this.f454a.a();
        }
    }
}
