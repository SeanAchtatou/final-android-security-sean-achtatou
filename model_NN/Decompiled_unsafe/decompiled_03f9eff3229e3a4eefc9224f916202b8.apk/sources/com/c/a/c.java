package com.c.a;

import java.io.IOException;
import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.protocol.HttpContext;

/* compiled from: HttpUtils */
class c implements HttpRequestInterceptor {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f443a;

    c(a aVar) {
        this.f443a = aVar;
    }

    public void process(HttpRequest httpRequest, HttpContext httpContext) throws HttpException, IOException {
        if (!httpRequest.containsHeader("Accept-Encoding")) {
            httpRequest.addHeader("Accept-Encoding", "gzip");
        }
    }
}
