package com.c.a;

import com.c.a.b.a.b;
import com.c.a.b.b.c;
import com.c.a.b.b.d;
import com.c.a.b.c;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import org.apache.http.HttpVersion;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.conn.params.ConnPerRouteBean;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;

/* compiled from: HttpUtils */
public class a {

    /* renamed from: a  reason: collision with root package name */
    public static final com.c.a.b.a f417a = new com.c.a.b.a();
    private static final ThreadFactory g = new b();
    private static int h = 3;
    private static Executor i = Executors.newFixedThreadPool(h, g);
    private final DefaultHttpClient b;
    private final HttpContext c;
    private b d;
    private String e;
    private long f;

    public a() {
        this(15000);
    }

    public a(int i2) {
        this.c = new BasicHttpContext();
        this.e = "UTF-8";
        this.f = com.c.a.b.a.a();
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        ConnManagerParams.setTimeout(basicHttpParams, (long) i2);
        HttpConnectionParams.setSoTimeout(basicHttpParams, i2);
        HttpConnectionParams.setConnectionTimeout(basicHttpParams, i2);
        ConnManagerParams.setMaxConnectionsPerRoute(basicHttpParams, new ConnPerRouteBean(10));
        ConnManagerParams.setMaxTotalConnections(basicHttpParams, 10);
        HttpConnectionParams.setTcpNoDelay(basicHttpParams, true);
        HttpConnectionParams.setSocketBufferSize(basicHttpParams, 8192);
        HttpProtocolParams.setVersion(basicHttpParams, HttpVersion.HTTP_1_1);
        SchemeRegistry schemeRegistry = new SchemeRegistry();
        schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
        schemeRegistry.register(new Scheme("https", com.c.a.b.b.a.a(), 443));
        this.b = new DefaultHttpClient(new ThreadSafeClientConnManager(basicHttpParams, schemeRegistry), basicHttpParams);
        this.b.setHttpRequestRetryHandler(new d(5));
        this.b.addRequestInterceptor(new c(this));
        this.b.addResponseInterceptor(new d(this));
    }

    public <T> c<T> a(c.a aVar, String str, com.c.a.b.d dVar, com.c.a.b.a.c<T> cVar) {
        if (str != null) {
            return a(new com.c.a.b.b.c(aVar, str), dVar, cVar);
        }
        throw new IllegalArgumentException("url may not be null");
    }

    private <T> com.c.a.b.c<T> a(com.c.a.b.b.c cVar, com.c.a.b.d dVar, com.c.a.b.a.c<T> cVar2) {
        com.c.a.b.c<T> cVar3 = new com.c.a.b.c<>(this.b, this.c, this.e, cVar2);
        cVar3.a(this.f);
        cVar3.a(this.d);
        cVar.a(dVar, cVar3);
        cVar3.a(i, cVar);
        return cVar3;
    }
}
