package com.c.a.b.a;

import com.c.a.c.b;
import com.c.a.c.d;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import org.apache.http.HttpEntity;

/* compiled from: StringDownloadHandler */
public class e {
    public String a(HttpEntity httpEntity, d dVar, String str) throws IOException {
        if (httpEntity == null) {
            return null;
        }
        long j = 0;
        long contentLength = httpEntity.getContentLength();
        if (dVar != null && !dVar.a(contentLength, 0, true)) {
            return null;
        }
        InputStream inputStream = null;
        StringBuilder sb = new StringBuilder();
        try {
            InputStream content = httpEntity.getContent();
            try {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(content, str));
                while (true) {
                    String readLine = bufferedReader.readLine();
                    if (readLine != null) {
                        sb.append(readLine).append(10);
                        j += d.a(readLine, str);
                        if (dVar != null && !dVar.a(contentLength, j, false)) {
                            break;
                        }
                    } else {
                        break;
                    }
                }
                if (dVar != null) {
                    dVar.a(contentLength, j, true);
                }
                b.a(content);
                return sb.toString().trim();
            } catch (Throwable th) {
                th = th;
                inputStream = content;
                b.a(inputStream);
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
        }
    }
}
