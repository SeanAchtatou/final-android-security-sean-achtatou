package com.c.a.b.b;

import com.c.a.c.c;
import java.io.IOException;
import java.net.Socket;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import org.apache.http.conn.ssl.SSLSocketFactory;

/* compiled from: DefaultSSLSocketFactory */
public class a extends SSLSocketFactory {
    private static KeyStore b;
    private static a c;

    /* renamed from: a  reason: collision with root package name */
    private SSLContext f422a = SSLContext.getInstance("TLS");

    static {
        try {
            b = KeyStore.getInstance(KeyStore.getDefaultType());
            b.load(null, null);
        } catch (Throwable th) {
            c.a(th.getMessage(), th);
        }
    }

    public static a a() {
        if (c == null) {
            try {
                c = new a();
            } catch (Throwable th) {
                c.a(th.getMessage(), th);
            }
        }
        return c;
    }

    private a() throws UnrecoverableKeyException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
        super(b);
        b bVar = new b(this);
        this.f422a.init(null, new TrustManager[]{bVar}, null);
        setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
    }

    public Socket createSocket(Socket socket, String str, int i, boolean z) throws IOException {
        return this.f422a.getSocketFactory().createSocket(socket, str, i, z);
    }

    public Socket createSocket() throws IOException {
        return this.f422a.getSocketFactory().createSocket();
    }
}
