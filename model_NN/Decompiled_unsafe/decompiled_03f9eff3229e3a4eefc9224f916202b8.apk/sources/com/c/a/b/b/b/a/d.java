package com.c.a.b.b.b.a;

import cn.banshenggua.aichang.player.PlayerService;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

/* compiled from: StringBody */
public class d extends a {
    private final byte[] b;
    private final Charset c;

    public d(String str, String str2, Charset charset) throws UnsupportedEncodingException {
        super(str2);
        if (str == null) {
            throw new IllegalArgumentException("Text may not be null");
        }
        charset = charset == null ? Charset.forName("UTF-8") : charset;
        this.b = str.getBytes(charset.name());
        this.c = charset;
    }

    public d(String str) throws UnsupportedEncodingException {
        this(str, "text/plain", null);
    }

    public void a(OutputStream outputStream) throws IOException {
        if (outputStream == null) {
            throw new IllegalArgumentException("Output stream may not be null");
        }
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(this.b);
        byte[] bArr = new byte[4096];
        do {
            int read = byteArrayInputStream.read(bArr);
            if (read == -1) {
                outputStream.flush();
                return;
            }
            outputStream.write(bArr, 0, read);
            this.f426a.d += (long) read;
        } while (this.f426a.a(false));
        throw new InterruptedIOException(PlayerService.ACTION_STOP);
    }

    public String d() {
        return "8bit";
    }

    public String c() {
        return this.c.name();
    }

    public long e() {
        return (long) this.b.length;
    }

    public String b() {
        return null;
    }
}
