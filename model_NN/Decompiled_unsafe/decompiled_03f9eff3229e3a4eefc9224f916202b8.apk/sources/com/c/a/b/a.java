package com.c.a.b;

import android.text.TextUtils;
import com.c.a.b.b.c;
import com.c.a.c.a.g;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: HttpCache */
public class a {
    private static long c = 60000;
    private static final ConcurrentHashMap<String, Boolean> d = new ConcurrentHashMap<>(10);

    /* renamed from: a  reason: collision with root package name */
    private final g<String, String> f420a;
    private int b;

    static {
        d.put(c.a.GET.toString(), true);
    }

    public a() {
        this(102400, 60000);
    }

    public a(int i, long j) {
        this.b = 102400;
        this.b = i;
        c = j;
        this.f420a = new b(this, this.b);
    }

    public static long a() {
        return c;
    }

    public void a(String str, String str2, long j) {
        if (str != null && str2 != null && j >= 1) {
            this.f420a.a(str, str2, System.currentTimeMillis() + j);
        }
    }

    public String a(String str) {
        if (str != null) {
            return this.f420a.a(str);
        }
        return null;
    }

    public boolean b(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        Boolean bool = d.get(str.toUpperCase());
        return bool == null ? false : bool.booleanValue();
    }
}
