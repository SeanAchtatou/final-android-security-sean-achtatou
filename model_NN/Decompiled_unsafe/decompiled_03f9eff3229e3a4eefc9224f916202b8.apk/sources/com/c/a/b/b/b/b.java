package com.c.a.b.b.b;

import cn.banshenggua.aichang.player.PlayerService;
import com.c.a.b.b.b.g;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.http.util.ByteArrayBuffer;
import org.apache.mina.proxy.handlers.http.HttpProxyConstants;

/* compiled from: HttpMultipart */
class b {

    /* renamed from: a  reason: collision with root package name */
    private static final ByteArrayBuffer f427a = a(d.f429a, ": ");
    private static final ByteArrayBuffer b = a(d.f429a, HttpProxyConstants.CRLF);
    private static final ByteArrayBuffer c = a(d.f429a, "--");
    private static /* synthetic */ int[] i;
    private String d;
    private final Charset e;
    private final String f;
    private final List<a> g;
    private final c h;

    static /* synthetic */ int[] d() {
        int[] iArr = i;
        if (iArr == null) {
            iArr = new int[c.values().length];
            try {
                iArr[c.BROWSER_COMPATIBLE.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[c.STRICT.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            i = iArr;
        }
        return iArr;
    }

    private static ByteArrayBuffer a(Charset charset, String str) {
        ByteBuffer encode = charset.encode(CharBuffer.wrap(str));
        ByteArrayBuffer byteArrayBuffer = new ByteArrayBuffer(encode.remaining());
        byteArrayBuffer.append(encode.array(), encode.position(), encode.remaining());
        return byteArrayBuffer;
    }

    private static void a(ByteArrayBuffer byteArrayBuffer, OutputStream outputStream) throws IOException {
        outputStream.write(byteArrayBuffer.buffer(), 0, byteArrayBuffer.length());
        outputStream.flush();
    }

    private static void a(String str, Charset charset, OutputStream outputStream) throws IOException {
        a(a(charset, str), outputStream);
    }

    private static void a(String str, OutputStream outputStream) throws IOException {
        a(a(d.f429a, str), outputStream);
    }

    private static void a(e eVar, OutputStream outputStream) throws IOException {
        a(eVar.a(), outputStream);
        a(f427a, outputStream);
        a(eVar.b(), outputStream);
        a(b, outputStream);
    }

    private static void a(e eVar, Charset charset, OutputStream outputStream) throws IOException {
        a(eVar.a(), charset, outputStream);
        a(f427a, outputStream);
        a(eVar.b(), charset, outputStream);
        a(b, outputStream);
    }

    public b(String str, Charset charset, String str2, c cVar) {
        if (str == null) {
            throw new IllegalArgumentException("Multipart subtype may not be null");
        } else if (str2 == null) {
            throw new IllegalArgumentException("Multipart boundary may not be null");
        } else {
            this.d = str;
            this.e = charset == null ? d.f429a : charset;
            this.f = str2;
            this.g = new ArrayList();
            this.h = cVar;
        }
    }

    public List<a> a() {
        return this.g;
    }

    public void a(a aVar) {
        if (aVar != null) {
            this.g.add(aVar);
        }
    }

    public String b() {
        return this.f;
    }

    private void a(c cVar, OutputStream outputStream, boolean z) throws IOException {
        a(cVar, outputStream, g.a.f433a, z);
    }

    private void a(c cVar, OutputStream outputStream, g.a aVar, boolean z) throws IOException {
        aVar.d = 0;
        ByteArrayBuffer a2 = a(this.e, b());
        for (a next : this.g) {
            if (!aVar.a(true)) {
                throw new InterruptedIOException(PlayerService.ACTION_STOP);
            }
            a(c, outputStream);
            aVar.d += (long) c.length();
            a(a2, outputStream);
            aVar.d += (long) a2.length();
            a(b, outputStream);
            aVar.d += (long) b.length();
            f c2 = next.c();
            switch (d()[cVar.ordinal()]) {
                case 1:
                    Iterator<e> it = c2.iterator();
                    while (it.hasNext()) {
                        e next2 = it.next();
                        a(next2, outputStream);
                        aVar.d += (long) (a(d.f429a, String.valueOf(next2.a()) + next2.b()).length() + f427a.length() + b.length());
                    }
                    break;
                case 2:
                    e a3 = c2.a("Content-Disposition");
                    a(a3, this.e, outputStream);
                    aVar.d = ((long) (a(this.e, String.valueOf(a3.a()) + a3.b()).length() + f427a.length() + b.length())) + aVar.d;
                    if (next.b().b() != null) {
                        e a4 = c2.a("Content-Type");
                        a(a4, this.e, outputStream);
                        aVar.d += (long) (a(this.e, String.valueOf(a4.a()) + a4.b()).length() + f427a.length() + b.length());
                        break;
                    }
                    break;
            }
            a(b, outputStream);
            aVar.d += (long) b.length();
            if (z) {
                com.c.a.b.b.b.a.b b2 = next.b();
                b2.a(aVar);
                b2.a(outputStream);
            }
            a(b, outputStream);
            aVar.d += (long) b.length();
        }
        a(c, outputStream);
        aVar.d += (long) c.length();
        a(a2, outputStream);
        aVar.d += (long) a2.length();
        a(c, outputStream);
        aVar.d += (long) c.length();
        a(b, outputStream);
        aVar.d += (long) b.length();
        aVar.a(true);
    }

    public void a(OutputStream outputStream, g.a aVar) throws IOException {
        a(this.h, outputStream, aVar, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.c.a.b.b.b.b.a(com.c.a.b.b.b.c, java.io.OutputStream, boolean):void
     arg types: [com.c.a.b.b.b.c, java.io.ByteArrayOutputStream, int]
     candidates:
      com.c.a.b.b.b.b.a(com.c.a.b.b.b.e, java.nio.charset.Charset, java.io.OutputStream):void
      com.c.a.b.b.b.b.a(java.lang.String, java.nio.charset.Charset, java.io.OutputStream):void
      com.c.a.b.b.b.b.a(com.c.a.b.b.b.c, java.io.OutputStream, boolean):void */
    public long c() {
        long j = 0;
        for (a b2 : this.g) {
            long e2 = b2.b().e();
            if (e2 < 0) {
                return -1;
            }
            j = e2 + j;
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            a(this.h, (OutputStream) byteArrayOutputStream, false);
            return ((long) byteArrayOutputStream.toByteArray().length) + j;
        } catch (Throwable th) {
            return -1;
        }
    }
}
