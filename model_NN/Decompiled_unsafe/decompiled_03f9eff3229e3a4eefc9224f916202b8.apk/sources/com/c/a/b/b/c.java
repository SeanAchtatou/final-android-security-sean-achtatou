package com.c.a.b.b;

import com.c.a.b.b.a.b;
import com.c.a.b.b.c.a;
import com.c.a.b.d;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.List;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.CloneUtils;
import org.apache.mina.proxy.handlers.http.HttpProxyConstants;

/* compiled from: HttpRequest */
public class c extends HttpRequestBase implements HttpEntityEnclosingRequest {

    /* renamed from: a  reason: collision with root package name */
    private HttpEntity f434a;
    private a b;
    private a c;
    private Charset d;

    public c(a aVar, String str) {
        this.b = aVar;
        a(str);
    }

    public c a(List<NameValuePair> list) {
        if (list != null) {
            for (NameValuePair next : list) {
                this.c.a(next.getName(), next.getValue());
            }
        }
        return this;
    }

    public void a(d dVar, com.c.a.b.a.d dVar2) {
        if (dVar != null) {
            if (this.d == null) {
                this.d = Charset.forName(dVar.a());
            }
            List<d.a> d2 = dVar.d();
            if (d2 != null) {
                for (d.a next : d2) {
                    if (next.f441a) {
                        setHeader(next.b);
                    } else {
                        addHeader(next.b);
                    }
                }
            }
            a(dVar.c());
            b b2 = dVar.b();
            if (b2 != null) {
                if (b2 instanceof b) {
                    b2.a(dVar2);
                }
                setEntity(b2);
            }
        }
    }

    public URI getURI() {
        try {
            if (this.d == null) {
                this.d = com.c.a.c.d.a((HttpRequestBase) this);
            }
            if (this.d == null) {
                this.d = Charset.forName("UTF-8");
            }
            return this.c.a(this.d);
        } catch (URISyntaxException e) {
            com.c.a.c.c.a(e.getMessage(), e);
            return null;
        }
    }

    public void setURI(URI uri) {
        this.c = new a(uri);
    }

    public void a(String str) {
        this.c = new a(str);
    }

    public String getMethod() {
        return this.b.toString();
    }

    public HttpEntity getEntity() {
        return this.f434a;
    }

    public void setEntity(HttpEntity httpEntity) {
        this.f434a = httpEntity;
    }

    public boolean expectContinue() {
        Header firstHeader = getFirstHeader("Expect");
        return firstHeader != null && "100-continue".equalsIgnoreCase(firstHeader.getValue());
    }

    public Object clone() throws CloneNotSupportedException {
        c cVar = (c) c.super.clone();
        if (this.f434a != null) {
            cVar.f434a = (HttpEntity) CloneUtils.clone(this.f434a);
        }
        return cVar;
    }

    /* compiled from: HttpRequest */
    public enum a {
        GET("GET"),
        POST("POST"),
        PUT(HttpProxyConstants.PUT),
        HEAD("HEAD"),
        MOVE("MOVE"),
        COPY("COPY"),
        DELETE("DELETE"),
        OPTIONS("OPTIONS"),
        TRACE("TRACE"),
        CONNECT(HttpProxyConstants.CONNECT);
        
        private final String k;

        private a(String str) {
            this.k = str;
        }

        public String toString() {
            return this.k;
        }
    }
}
