package com.c.a;

import java.io.IOException;
import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.HttpEntity;
import org.apache.http.HttpException;
import org.apache.http.HttpResponse;
import org.apache.http.HttpResponseInterceptor;
import org.apache.http.protocol.HttpContext;

/* compiled from: HttpUtils */
class d implements HttpResponseInterceptor {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f457a;

    d(a aVar) {
        this.f457a = aVar;
    }

    public void process(HttpResponse httpResponse, HttpContext httpContext) throws HttpException, IOException {
        Header contentEncoding;
        HttpEntity entity = httpResponse.getEntity();
        if (entity != null && (contentEncoding = entity.getContentEncoding()) != null) {
            HeaderElement[] elements = contentEncoding.getElements();
            int length = elements.length;
            int i = 0;
            while (i < length && !elements[i].getName().equalsIgnoreCase("gzip")) {
                i++;
            }
        }
    }
}
