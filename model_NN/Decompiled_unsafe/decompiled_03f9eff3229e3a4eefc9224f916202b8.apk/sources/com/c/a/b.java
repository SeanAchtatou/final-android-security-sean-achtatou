package com.c.a;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/* compiled from: HttpUtils */
class b implements ThreadFactory {

    /* renamed from: a  reason: collision with root package name */
    private final AtomicInteger f419a = new AtomicInteger(1);

    b() {
    }

    public Thread newThread(Runnable runnable) {
        Thread thread = new Thread(runnable, "HttpUtils #" + this.f419a.getAndIncrement());
        thread.setPriority(4);
        return thread;
    }
}
