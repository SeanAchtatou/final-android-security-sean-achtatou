package com.tencent.wxop.stat;

import android.app.ListActivity;

public class EasyListActivity extends ListActivity {
    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        e.m(this);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        e.l(this);
    }
}
