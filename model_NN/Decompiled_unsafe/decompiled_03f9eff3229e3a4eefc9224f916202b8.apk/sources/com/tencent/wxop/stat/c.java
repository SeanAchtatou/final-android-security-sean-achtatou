package com.tencent.wxop.stat;

import android.content.Context;
import com.tencent.a.a.a.a.g;
import com.tencent.stat.common.StatConstants;
import com.tencent.wxop.stat.b.b;
import com.tencent.wxop.stat.b.l;
import com.tencent.wxop.stat.b.q;
import com.tencent.wxop.stat.b.r;
import java.net.URI;
import java.util.Iterator;
import org.json.JSONException;
import org.json.JSONObject;

public final class c {
    static String M = "__MTA_KILL__";
    private static b N = l.av();
    static ah O = new ah(2);
    static ah P = new ah(1);
    private static d Q = d.APP_LAUNCH;
    private static boolean R = false;
    private static boolean S = true;
    private static int T = 30000;
    private static int U = 100000;
    private static int V = 30;
    static String W = "__HIBERNATE__TIME";
    private static String X = null;
    private static String Y;
    private static String Z;
    static int aA = 512;
    private static String aa = "mta_channel";
    static String ab = "";
    private static int ac = 180;
    static boolean ad = false;
    static int ae = 100;
    static long af = 10000;
    private static int ag = 1024;
    static boolean ah = true;
    private static long ai = 0;
    private static long aj = 300000;
    public static boolean ak = true;
    static volatile String al = StatConstants.MTA_SERVER;
    private static volatile String am = "http://pingma.qq.com:80/mstat/report";
    private static int an = 0;
    private static volatile int ao = 0;
    private static int ap = 20;
    private static int aq = 0;
    private static boolean ar = false;
    private static int as = 4096;
    private static boolean at = false;
    private static String au = null;
    private static boolean av = false;
    private static ai aw = null;
    static boolean ax = true;
    static int ay = 0;
    static long az = 10000;
    static String c = "__HIBERNATE__";
    private static int w = 10;
    private static int x = 100;
    private static int y = 30;
    private static int z = 1;

    public static int A() {
        return ap;
    }

    static void B() {
        aq++;
    }

    static void C() {
        aq = 0;
    }

    static int D() {
        return aq;
    }

    public static boolean E() {
        return at;
    }

    public static ai F() {
        return aw;
    }

    static void a(Context context, ah ahVar) {
        if (ahVar.aI == P.aI) {
            P = ahVar;
            a(ahVar.df);
            if (!P.df.isNull("iplist")) {
                g.r(context).b(P.df.getString("iplist"));
            }
        } else if (ahVar.aI == O.aI) {
            O = ahVar;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:109:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:110:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0044, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0045, code lost:
        com.tencent.wxop.stat.c.N.b((java.lang.Throwable) r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x005c, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x005d, code lost:
        com.tencent.wxop.stat.c.N.b(r0);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x005c A[ExcHandler: Throwable (r0v0 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:1:0x0002] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void a(android.content.Context r10, com.tencent.wxop.stat.ah r11, org.json.JSONObject r12) {
        /*
            r2 = 0
            r1 = 1
            java.util.Iterator r4 = r12.keys()     // Catch:{ JSONException -> 0x0044, Throwable -> 0x005c }
            r3 = r2
        L_0x0007:
            boolean r0 = r4.hasNext()     // Catch:{ JSONException -> 0x0044, Throwable -> 0x005c }
            if (r0 == 0) goto L_0x0063
            java.lang.Object r0 = r4.next()     // Catch:{ JSONException -> 0x0044, Throwable -> 0x005c }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ JSONException -> 0x0044, Throwable -> 0x005c }
            java.lang.String r5 = "v"
            boolean r5 = r0.equalsIgnoreCase(r5)     // Catch:{ JSONException -> 0x0044, Throwable -> 0x005c }
            if (r5 == 0) goto L_0x0028
            int r5 = r12.getInt(r0)     // Catch:{ JSONException -> 0x0044, Throwable -> 0x005c }
            int r0 = r11.L     // Catch:{ JSONException -> 0x0044, Throwable -> 0x005c }
            if (r0 == r5) goto L_0x02a0
            r0 = r1
        L_0x0024:
            r11.L = r5     // Catch:{ JSONException -> 0x0044, Throwable -> 0x005c }
            r3 = r0
            goto L_0x0007
        L_0x0028:
            java.lang.String r5 = "c"
            boolean r5 = r0.equalsIgnoreCase(r5)     // Catch:{ JSONException -> 0x0044, Throwable -> 0x005c }
            if (r5 == 0) goto L_0x004b
            java.lang.String r0 = "c"
            java.lang.String r0 = r12.getString(r0)     // Catch:{ JSONException -> 0x0044, Throwable -> 0x005c }
            int r5 = r0.length()     // Catch:{ JSONException -> 0x0044, Throwable -> 0x005c }
            if (r5 <= 0) goto L_0x0007
            org.json.JSONObject r5 = new org.json.JSONObject     // Catch:{ JSONException -> 0x0044, Throwable -> 0x005c }
            r5.<init>(r0)     // Catch:{ JSONException -> 0x0044, Throwable -> 0x005c }
            r11.df = r5     // Catch:{ JSONException -> 0x0044, Throwable -> 0x005c }
            goto L_0x0007
        L_0x0044:
            r0 = move-exception
            com.tencent.wxop.stat.b.b r1 = com.tencent.wxop.stat.c.N
            r1.b(r0)
        L_0x004a:
            return
        L_0x004b:
            java.lang.String r5 = "m"
            boolean r0 = r0.equalsIgnoreCase(r5)     // Catch:{ JSONException -> 0x0044, Throwable -> 0x005c }
            if (r0 == 0) goto L_0x0007
            java.lang.String r0 = "m"
            java.lang.String r0 = r12.getString(r0)     // Catch:{ JSONException -> 0x0044, Throwable -> 0x005c }
            r11.c = r0     // Catch:{ JSONException -> 0x0044, Throwable -> 0x005c }
            goto L_0x0007
        L_0x005c:
            r0 = move-exception
            com.tencent.wxop.stat.b.b r1 = com.tencent.wxop.stat.c.N
            r1.b(r0)
            goto L_0x004a
        L_0x0063:
            if (r3 != r1) goto L_0x0089
            android.content.Context r0 = com.tencent.wxop.stat.ak.aB()     // Catch:{ JSONException -> 0x0044, Throwable -> 0x005c }
            com.tencent.wxop.stat.t r0 = com.tencent.wxop.stat.t.s(r0)     // Catch:{ JSONException -> 0x0044, Throwable -> 0x005c }
            if (r0 == 0) goto L_0x0072
            r0.b(r11)     // Catch:{ JSONException -> 0x0044, Throwable -> 0x005c }
        L_0x0072:
            int r0 = r11.aI     // Catch:{ JSONException -> 0x0044, Throwable -> 0x005c }
            com.tencent.wxop.stat.ah r3 = com.tencent.wxop.stat.c.P     // Catch:{ JSONException -> 0x0044, Throwable -> 0x005c }
            int r3 = r3.aI     // Catch:{ JSONException -> 0x0044, Throwable -> 0x005c }
            if (r0 != r3) goto L_0x0089
            org.json.JSONObject r0 = r11.df     // Catch:{ JSONException -> 0x0044, Throwable -> 0x005c }
            a(r0)     // Catch:{ JSONException -> 0x0044, Throwable -> 0x005c }
            org.json.JSONObject r3 = r11.df     // Catch:{ JSONException -> 0x0044, Throwable -> 0x005c }
            if (r3 == 0) goto L_0x0089
            int r0 = r3.length()     // Catch:{ JSONException -> 0x0044, Throwable -> 0x005c }
            if (r0 != 0) goto L_0x008d
        L_0x0089:
            a(r10, r11)     // Catch:{ JSONException -> 0x0044, Throwable -> 0x005c }
            goto L_0x004a
        L_0x008d:
            android.content.Context r4 = com.tencent.wxop.stat.ak.aB()     // Catch:{ JSONException -> 0x00de, Throwable -> 0x005c }
            java.lang.String r0 = com.tencent.wxop.stat.c.M     // Catch:{ Exception -> 0x0282 }
            java.lang.String r0 = r3.optString(r0)     // Catch:{ Exception -> 0x0282 }
            boolean r5 = com.tencent.wxop.stat.b.l.e(r0)     // Catch:{ Exception -> 0x0282 }
            if (r5 == 0) goto L_0x00a8
            org.json.JSONObject r5 = new org.json.JSONObject     // Catch:{ Exception -> 0x0282 }
            r5.<init>(r0)     // Catch:{ Exception -> 0x0282 }
            int r0 = r5.length()     // Catch:{ Exception -> 0x0282 }
            if (r0 != 0) goto L_0x00e7
        L_0x00a8:
            java.lang.String r0 = com.tencent.wxop.stat.c.c     // Catch:{ JSONException -> 0x00de, Throwable -> 0x005c }
            java.lang.String r0 = r3.getString(r0)     // Catch:{ JSONException -> 0x00de, Throwable -> 0x005c }
            boolean r1 = com.tencent.wxop.stat.c.R     // Catch:{ JSONException -> 0x00de, Throwable -> 0x005c }
            if (r1 == 0) goto L_0x00cc
            com.tencent.wxop.stat.b.b r1 = com.tencent.wxop.stat.c.N     // Catch:{ JSONException -> 0x00de, Throwable -> 0x005c }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x00de, Throwable -> 0x005c }
            java.lang.String r3 = "hibernateVer:"
            r2.<init>(r3)     // Catch:{ JSONException -> 0x00de, Throwable -> 0x005c }
            java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ JSONException -> 0x00de, Throwable -> 0x005c }
            java.lang.String r3 = ", current version:2.0.3"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ JSONException -> 0x00de, Throwable -> 0x005c }
            java.lang.String r2 = r2.toString()     // Catch:{ JSONException -> 0x00de, Throwable -> 0x005c }
            r1.e(r2)     // Catch:{ JSONException -> 0x00de, Throwable -> 0x005c }
        L_0x00cc:
            long r0 = com.tencent.wxop.stat.b.l.u(r0)     // Catch:{ JSONException -> 0x00de, Throwable -> 0x005c }
            java.lang.String r2 = "2.0.3"
            long r2 = com.tencent.wxop.stat.b.l.u(r2)     // Catch:{ JSONException -> 0x00de, Throwable -> 0x005c }
            int r2 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
            if (r2 > 0) goto L_0x0089
            b(r0)     // Catch:{ JSONException -> 0x00de, Throwable -> 0x005c }
            goto L_0x0089
        L_0x00de:
            r0 = move-exception
            com.tencent.wxop.stat.b.b r0 = com.tencent.wxop.stat.c.N     // Catch:{ JSONException -> 0x0044, Throwable -> 0x005c }
            java.lang.String r1 = "__HIBERNATE__ not found."
            r0.e(r1)     // Catch:{ JSONException -> 0x0044, Throwable -> 0x005c }
            goto L_0x0089
        L_0x00e7:
            java.lang.String r0 = "sm"
            boolean r0 = r5.isNull(r0)     // Catch:{ Exception -> 0x0282 }
            if (r0 != 0) goto L_0x0139
            java.lang.String r0 = "sm"
            java.lang.Object r0 = r5.get(r0)     // Catch:{ Exception -> 0x0282 }
            boolean r6 = r0 instanceof java.lang.Integer     // Catch:{ Exception -> 0x0282 }
            if (r6 == 0) goto L_0x028a
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ Exception -> 0x0282 }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x0282 }
        L_0x00ff:
            if (r0 <= 0) goto L_0x0139
            boolean r6 = com.tencent.wxop.stat.c.R     // Catch:{ Exception -> 0x0282 }
            if (r6 == 0) goto L_0x011f
            com.tencent.wxop.stat.b.b r6 = com.tencent.wxop.stat.c.N     // Catch:{ Exception -> 0x0282 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0282 }
            java.lang.String r8 = "match sleepTime:"
            r7.<init>(r8)     // Catch:{ Exception -> 0x0282 }
            java.lang.StringBuilder r7 = r7.append(r0)     // Catch:{ Exception -> 0x0282 }
            java.lang.String r8 = " minutes"
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Exception -> 0x0282 }
            java.lang.String r7 = r7.toString()     // Catch:{ Exception -> 0x0282 }
            r6.b(r7)     // Catch:{ Exception -> 0x0282 }
        L_0x011f:
            long r6 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0282 }
            int r0 = r0 * 60
            int r0 = r0 * 1000
            long r8 = (long) r0     // Catch:{ Exception -> 0x0282 }
            long r6 = r6 + r8
            java.lang.String r0 = com.tencent.wxop.stat.c.W     // Catch:{ Exception -> 0x0282 }
            com.tencent.wxop.stat.b.q.a(r4, r0, r6)     // Catch:{ Exception -> 0x0282 }
            r0 = 0
            a(r0)     // Catch:{ Exception -> 0x0282 }
            com.tencent.wxop.stat.b.b r0 = com.tencent.wxop.stat.c.N     // Catch:{ Exception -> 0x0282 }
            java.lang.String r6 = "MTA is disable for current SDK version"
            r0.warn(r6)     // Catch:{ Exception -> 0x0282 }
        L_0x0139:
            java.lang.String r0 = "sv"
            java.lang.String r6 = "2.0.3"
            boolean r0 = b(r5, r0, r6)     // Catch:{ Exception -> 0x0282 }
            if (r0 == 0) goto L_0x029a
            com.tencent.wxop.stat.b.b r0 = com.tencent.wxop.stat.c.N     // Catch:{ Exception -> 0x0282 }
            java.lang.String r2 = "match sdk version:2.0.3"
            r0.b(r2)     // Catch:{ Exception -> 0x0282 }
            r0 = r1
        L_0x014b:
            java.lang.String r2 = "md"
            java.lang.String r6 = android.os.Build.MODEL     // Catch:{ Exception -> 0x0282 }
            boolean r2 = b(r5, r2, r6)     // Catch:{ Exception -> 0x0282 }
            if (r2 == 0) goto L_0x016c
            com.tencent.wxop.stat.b.b r0 = com.tencent.wxop.stat.c.N     // Catch:{ Exception -> 0x0282 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0282 }
            java.lang.String r6 = "match MODEL:"
            r2.<init>(r6)     // Catch:{ Exception -> 0x0282 }
            java.lang.String r6 = android.os.Build.MODEL     // Catch:{ Exception -> 0x0282 }
            java.lang.StringBuilder r2 = r2.append(r6)     // Catch:{ Exception -> 0x0282 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0282 }
            r0.b(r2)     // Catch:{ Exception -> 0x0282 }
            r0 = r1
        L_0x016c:
            java.lang.String r2 = "av"
            java.lang.String r6 = com.tencent.wxop.stat.b.l.D(r4)     // Catch:{ Exception -> 0x0282 }
            boolean r2 = b(r5, r2, r6)     // Catch:{ Exception -> 0x0282 }
            if (r2 == 0) goto L_0x0191
            com.tencent.wxop.stat.b.b r0 = com.tencent.wxop.stat.c.N     // Catch:{ Exception -> 0x0282 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0282 }
            java.lang.String r6 = "match app version:"
            r2.<init>(r6)     // Catch:{ Exception -> 0x0282 }
            java.lang.String r6 = com.tencent.wxop.stat.b.l.D(r4)     // Catch:{ Exception -> 0x0282 }
            java.lang.StringBuilder r2 = r2.append(r6)     // Catch:{ Exception -> 0x0282 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0282 }
            r0.b(r2)     // Catch:{ Exception -> 0x0282 }
            r0 = r1
        L_0x0191:
            java.lang.String r2 = "mf"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0282 }
            r6.<init>()     // Catch:{ Exception -> 0x0282 }
            java.lang.String r7 = android.os.Build.MANUFACTURER     // Catch:{ Exception -> 0x0282 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x0282 }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x0282 }
            boolean r2 = b(r5, r2, r6)     // Catch:{ Exception -> 0x0282 }
            if (r2 == 0) goto L_0x01bf
            com.tencent.wxop.stat.b.b r0 = com.tencent.wxop.stat.c.N     // Catch:{ Exception -> 0x0282 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0282 }
            java.lang.String r6 = "match MANUFACTURER:"
            r2.<init>(r6)     // Catch:{ Exception -> 0x0282 }
            java.lang.String r6 = android.os.Build.MANUFACTURER     // Catch:{ Exception -> 0x0282 }
            java.lang.StringBuilder r2 = r2.append(r6)     // Catch:{ Exception -> 0x0282 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0282 }
            r0.b(r2)     // Catch:{ Exception -> 0x0282 }
            r0 = r1
        L_0x01bf:
            java.lang.String r2 = "osv"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0282 }
            r6.<init>()     // Catch:{ Exception -> 0x0282 }
            int r7 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x0282 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x0282 }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x0282 }
            boolean r2 = b(r5, r2, r6)     // Catch:{ Exception -> 0x0282 }
            if (r2 == 0) goto L_0x01ed
            com.tencent.wxop.stat.b.b r0 = com.tencent.wxop.stat.c.N     // Catch:{ Exception -> 0x0282 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0282 }
            java.lang.String r6 = "match android SDK version:"
            r2.<init>(r6)     // Catch:{ Exception -> 0x0282 }
            int r6 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x0282 }
            java.lang.StringBuilder r2 = r2.append(r6)     // Catch:{ Exception -> 0x0282 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0282 }
            r0.b(r2)     // Catch:{ Exception -> 0x0282 }
            r0 = r1
        L_0x01ed:
            java.lang.String r2 = "ov"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0282 }
            r6.<init>()     // Catch:{ Exception -> 0x0282 }
            int r7 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x0282 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x0282 }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x0282 }
            boolean r2 = b(r5, r2, r6)     // Catch:{ Exception -> 0x0282 }
            if (r2 == 0) goto L_0x021b
            com.tencent.wxop.stat.b.b r0 = com.tencent.wxop.stat.c.N     // Catch:{ Exception -> 0x0282 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0282 }
            java.lang.String r6 = "match android SDK version:"
            r2.<init>(r6)     // Catch:{ Exception -> 0x0282 }
            int r6 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x0282 }
            java.lang.StringBuilder r2 = r2.append(r6)     // Catch:{ Exception -> 0x0282 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0282 }
            r0.b(r2)     // Catch:{ Exception -> 0x0282 }
            r0 = r1
        L_0x021b:
            java.lang.String r2 = "ui"
            com.tencent.wxop.stat.t r6 = com.tencent.wxop.stat.t.s(r4)     // Catch:{ Exception -> 0x0282 }
            com.tencent.wxop.stat.b.c r6 = r6.t(r4)     // Catch:{ Exception -> 0x0282 }
            java.lang.String r6 = r6.b()     // Catch:{ Exception -> 0x0282 }
            boolean r2 = b(r5, r2, r6)     // Catch:{ Exception -> 0x0282 }
            if (r2 == 0) goto L_0x0250
            com.tencent.wxop.stat.b.b r0 = com.tencent.wxop.stat.c.N     // Catch:{ Exception -> 0x0282 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0282 }
            java.lang.String r6 = "match imei:"
            r2.<init>(r6)     // Catch:{ Exception -> 0x0282 }
            com.tencent.wxop.stat.t r6 = com.tencent.wxop.stat.t.s(r4)     // Catch:{ Exception -> 0x0282 }
            com.tencent.wxop.stat.b.c r6 = r6.t(r4)     // Catch:{ Exception -> 0x0282 }
            java.lang.String r6 = r6.b()     // Catch:{ Exception -> 0x0282 }
            java.lang.StringBuilder r2 = r2.append(r6)     // Catch:{ Exception -> 0x0282 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0282 }
            r0.b(r2)     // Catch:{ Exception -> 0x0282 }
            r0 = r1
        L_0x0250:
            java.lang.String r2 = "mid"
            java.lang.String r6 = h(r4)     // Catch:{ Exception -> 0x0282 }
            boolean r2 = b(r5, r2, r6)     // Catch:{ Exception -> 0x0282 }
            if (r2 == 0) goto L_0x0275
            com.tencent.wxop.stat.b.b r0 = com.tencent.wxop.stat.c.N     // Catch:{ Exception -> 0x0282 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0282 }
            java.lang.String r5 = "match mid:"
            r2.<init>(r5)     // Catch:{ Exception -> 0x0282 }
            java.lang.String r4 = h(r4)     // Catch:{ Exception -> 0x0282 }
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x0282 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0282 }
            r0.b(r2)     // Catch:{ Exception -> 0x0282 }
            r0 = r1
        L_0x0275:
            if (r0 == 0) goto L_0x00a8
            java.lang.String r0 = "2.0.3"
            long r0 = com.tencent.wxop.stat.b.l.u(r0)     // Catch:{ Exception -> 0x0282 }
            b(r0)     // Catch:{ Exception -> 0x0282 }
            goto L_0x00a8
        L_0x0282:
            r0 = move-exception
            com.tencent.wxop.stat.b.b r1 = com.tencent.wxop.stat.c.N     // Catch:{ JSONException -> 0x00de, Throwable -> 0x005c }
            r1.b(r0)     // Catch:{ JSONException -> 0x00de, Throwable -> 0x005c }
            goto L_0x00a8
        L_0x028a:
            boolean r6 = r0 instanceof java.lang.String     // Catch:{ Exception -> 0x0282 }
            if (r6 == 0) goto L_0x029d
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x0282 }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ Exception -> 0x0282 }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x0282 }
            goto L_0x00ff
        L_0x029a:
            r0 = r2
            goto L_0x014b
        L_0x029d:
            r0 = r2
            goto L_0x00ff
        L_0x02a0:
            r0 = r3
            goto L_0x0024
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.wxop.stat.c.a(android.content.Context, com.tencent.wxop.stat.ah, org.json.JSONObject):void");
    }

    static void a(Context context, JSONObject jSONObject) {
        try {
            Iterator<String> keys = jSONObject.keys();
            while (keys.hasNext()) {
                String next = keys.next();
                if (next.equalsIgnoreCase(Integer.toString(P.aI))) {
                    a(context, P, jSONObject.getJSONObject(next));
                } else if (next.equalsIgnoreCase(Integer.toString(O.aI))) {
                    a(context, O, jSONObject.getJSONObject(next));
                } else if (next.equalsIgnoreCase("rs")) {
                    d a2 = d.a(jSONObject.getInt(next));
                    if (a2 != null) {
                        Q = a2;
                        if (R) {
                            N.e("Change to ReportStrategy:" + a2.name());
                        }
                    }
                } else {
                    return;
                }
            }
        } catch (JSONException e) {
            N.b((Throwable) e);
        }
    }

    public static void a(d dVar) {
        Q = dVar;
        if (dVar != d.PERIOD) {
            e.aZ = 0;
        }
        if (R) {
            N.e("Change to statSendStrategy: " + dVar);
        }
    }

    private static void a(JSONObject jSONObject) {
        try {
            d a2 = d.a(jSONObject.getInt("rs"));
            if (a2 != null) {
                a(a2);
            }
        } catch (JSONException e) {
            if (R) {
                N.b("rs not found.");
            }
        }
    }

    public static void a(boolean z2) {
        S = z2;
        if (!z2) {
            N.warn("!!!!!!MTA StatService has been disabled!!!!!!");
        }
    }

    private static void b(long j) {
        q.a(ak.aB(), c, j);
        a(false);
        N.warn("MTA is disable for current SDK version");
    }

    public static void b(Context context, String str) {
        String str2;
        if (context == null) {
            N.error("ctx in StatConfig.setAppKey() is null");
        } else if (str == null || str.length() > 256) {
            N.error("appkey in StatConfig.setAppKey() is null or exceed 256 bytes");
        } else {
            if (Y == null) {
                Y = r.t(q.b(context, "_mta_ky_tag_", (String) null));
            }
            if ((m(str) || m(l.z(context))) && (str2 = Y) != null) {
                q.c(context, "_mta_ky_tag_", r.q(str2));
            }
        }
    }

    private static boolean b(JSONObject jSONObject, String str, String str2) {
        if (!jSONObject.isNull(str)) {
            String optString = jSONObject.optString(str);
            return l.e(str2) && l.e(optString) && str2.equalsIgnoreCase(optString);
        }
    }

    public static void c(Context context, String str) {
        if (str.length() > 128) {
            N.error("the length of installChannel can not exceed the range of 128 bytes.");
            return;
        }
        Z = str;
        q.c(context, aa, str);
    }

    public static synchronized String d(Context context) {
        String str;
        synchronized (c.class) {
            if (Y != null) {
                str = Y;
            } else {
                if (context != null) {
                    if (Y == null) {
                        Y = l.z(context);
                    }
                }
                if (Y == null || Y.trim().length() == 0) {
                    N.error("AppKey can not be null or empty, please read Developer's Guide first!");
                }
                str = Y;
            }
        }
        return str;
    }

    public static synchronized String e(Context context) {
        String str;
        synchronized (c.class) {
            if (Z != null) {
                str = Z;
            } else {
                String b = q.b(context, aa, "");
                Z = b;
                if (b == null || Z.trim().length() == 0) {
                    Z = l.A(context);
                }
                if (Z == null || Z.trim().length() == 0) {
                    N.c("installChannel can not be null or empty, please read Developer's Guide first!");
                }
                str = Z;
            }
        }
        return str;
    }

    public static String f(Context context) {
        return q.b(context, "mta.acc.qq", ab);
    }

    public static String g(Context context) {
        if (context == null) {
            N.error("Context for getCustomUid is null.");
            return null;
        }
        if (au == null) {
            au = q.b(context, "MTA_CUSTOM_UID", "");
        }
        return au;
    }

    public static String h(Context context) {
        return context != null ? g.a(context).f().c() : "0";
    }

    public static d j() {
        return Q;
    }

    public static boolean k() {
        return R;
    }

    static String l(String str) {
        try {
            String string = P.df.getString(str);
            if (string != null) {
                return string;
            }
            return null;
        } catch (Throwable th) {
            N.c("can't find custom key:" + str);
        }
    }

    public static boolean l() {
        return S;
    }

    public static int m() {
        return T;
    }

    private static boolean m(String str) {
        if (str == null) {
            return false;
        }
        if (Y == null) {
            Y = str;
            return true;
        } else if (Y.contains(str)) {
            return false;
        } else {
            Y += "|" + str;
            return true;
        }
    }

    public static int n() {
        return x;
    }

    public static void n(String str) {
        if (str.length() > 128) {
            N.error("the length of installChannel can not exceed the range of 128 bytes.");
        } else {
            Z = str;
        }
    }

    public static int o() {
        return y;
    }

    public static void o(String str) {
        if (str == null || str.length() == 0) {
            N.error("statReportUrl cannot be null or empty.");
            return;
        }
        am = str;
        try {
            al = new URI(am).getHost();
        } catch (Exception e) {
            N.c(e);
        }
        if (R) {
            N.b("url:" + am + ", domain:" + al);
        }
    }

    public static int p() {
        return w;
    }

    public static int q() {
        return z;
    }

    static int r() {
        return V;
    }

    public static int s() {
        return U;
    }

    public static void t() {
        ac = 60;
    }

    public static int u() {
        return ac;
    }

    public static int v() {
        return ag;
    }

    public static void w() {
        ah = true;
    }

    public static boolean x() {
        return ak;
    }

    public static String y() {
        return am;
    }

    static synchronized void z() {
        synchronized (c.class) {
            ao = 0;
        }
    }
}
