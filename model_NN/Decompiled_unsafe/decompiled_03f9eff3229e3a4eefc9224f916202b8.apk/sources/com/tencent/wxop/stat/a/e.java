package com.tencent.wxop.stat.a;

import cn.banshenggua.aichang.utils.Constants;
import com.baidu.mobads.CpuInfoManager;

public enum e {
    PAGE_VIEW(1),
    SESSION_ENV(2),
    ERROR(3),
    CUSTOM(Constants.CLEARIMGED),
    ADDITION(1001),
    MONITOR_STAT(1002),
    MTA_GAME_USER(CpuInfoManager.CHANNEL_PICTURE),
    NETWORK_MONITOR(1004),
    NETWORK_DETECTOR(CpuInfoManager.CHANNEL_MOBILE);
    
    private int bG;

    private e(int i) {
        this.bG = i;
    }

    public final int r() {
        return this.bG;
    }
}
