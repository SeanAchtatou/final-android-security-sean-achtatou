package com.tencent.weibo.sdk.android.api.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.weibo.sdk.android.api.util.BackGroudSeletor;
import java.util.List;

public class ConversationAdapter extends BaseAdapter {
    private Context ct;
    private List<String> cvlist;

    public ConversationAdapter(Context context, List<String> list) {
        this.ct = context;
        this.cvlist = list;
    }

    public int getCount() {
        return this.cvlist.size();
    }

    public Object getItem(int i) {
        return this.cvlist.get(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public List<String> getCvlist() {
        return this.cvlist;
    }

    public void setCvlist(List<String> list) {
        this.cvlist = list;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            LinearLayout linearLayout = new LinearLayout(this.ct);
            new LinearLayout.LayoutParams(-1, -2);
            linearLayout.setBackgroundDrawable(BackGroudSeletor.getdrawble("topic_", this.ct));
            linearLayout.setGravity(16);
            linearLayout.setPadding(10, 0, 10, 0);
            TextView textView = new TextView(this.ct);
            textView.setTextColor(Color.parseColor("#108ece"));
            textView.setText(getItem(i).toString());
            textView.setGravity(16);
            textView.setTextSize(18.0f);
            linearLayout.addView(textView);
            linearLayout.setTag(textView);
            return linearLayout;
        }
        ((TextView) view.getTag()).setText(getItem(i).toString());
        return view;
    }
}
