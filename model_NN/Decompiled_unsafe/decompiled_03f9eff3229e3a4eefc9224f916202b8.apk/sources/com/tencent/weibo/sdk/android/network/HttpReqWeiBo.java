package com.tencent.weibo.sdk.android.network;

import android.content.Context;
import android.util.Log;
import cn.banshenggua.aichang.utils.StringUtil;
import com.qq.e.comm.constants.Constants;
import com.tencent.weibo.sdk.android.api.util.JsonUtil;
import com.tencent.weibo.sdk.android.model.BaseVO;
import com.tencent.weibo.sdk.android.model.ModelResult;
import com.tencent.weibo.sdk.android.network.HttpReq;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.methods.ByteArrayRequestEntity;
import org.apache.commons.httpclient.methods.PostMethod;
import org.json.JSONArray;
import org.json.JSONObject;

public class HttpReqWeiBo extends HttpReq {
    private Context mContext;
    private Integer mResultType = 0;
    private Class<? extends BaseVO> mTargetClass;
    private Class<? extends BaseVO> mTargetClass2;

    public HttpReqWeiBo(Context context, String str, HttpCallback httpCallback, Class<? extends BaseVO> cls, String str2, Integer num) {
        this.mContext = context;
        this.mHost = HttpConfig.CRM_SERVER_NAME;
        this.mPort = HttpConfig.CRM_SERVER_PORT;
        this.mUrl = str;
        this.mCallBack = httpCallback;
        this.mTargetClass = cls;
        this.mResultType = num;
        this.mMethod = str2;
    }

    public void setmTargetClass(Class<? extends BaseVO> cls) {
        this.mTargetClass = cls;
    }

    public void setmTargetClass2(Class<? extends BaseVO> cls) {
        this.mTargetClass2 = cls;
    }

    public void setmResultType(Integer num) {
        this.mResultType = num;
    }

    /* access modifiers changed from: protected */
    public Object processResponse(InputStream inputStream) throws Exception {
        int intValue;
        int intValue2;
        int i = 1;
        ModelResult modelResult = new ModelResult();
        if (inputStream != null) {
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            StringBuffer stringBuffer = new StringBuffer();
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine == null) {
                    break;
                }
                stringBuffer.append(readLine);
            }
            bufferedReader.close();
            inputStreamReader.close();
            Log.d("relst", stringBuffer.toString());
            if (stringBuffer.toString().indexOf("errcode") != -1 || stringBuffer.toString().indexOf("access_token") == -1) {
                JSONObject jSONObject = new JSONObject(stringBuffer.toString());
                BaseVO baseVO = null;
                if (this.mTargetClass != null) {
                    baseVO = (BaseVO) this.mTargetClass.newInstance();
                }
                String string = jSONObject.getString("errcode");
                String string2 = jSONObject.getString("msg");
                if (string != null && "0".equals(string)) {
                    modelResult.setSuccess(true);
                    switch (this.mResultType.intValue()) {
                        case 0:
                            BaseVO jsonToObject = JsonUtil.jsonToObject(this.mTargetClass, jSONObject);
                            ArrayList arrayList = new ArrayList();
                            arrayList.add(jsonToObject);
                            modelResult.setList(arrayList);
                            break;
                        case 1:
                            Map<String, Object> analyseHead = baseVO.analyseHead(jSONObject);
                            List<BaseVO> jsonToList = JsonUtil.jsonToList(this.mTargetClass, (JSONArray) analyseHead.get("array"));
                            if (analyseHead.get("total") == null) {
                                intValue = 0;
                            } else {
                                intValue = ((Integer) analyseHead.get("total")).intValue();
                            }
                            Integer valueOf = Integer.valueOf(intValue);
                            if (analyseHead.get("p") == null) {
                                intValue2 = 1;
                            } else {
                                intValue2 = ((Integer) analyseHead.get("p")).intValue();
                            }
                            Integer valueOf2 = Integer.valueOf(intValue2);
                            if (analyseHead.get(Constants.KEYS.PLACEMENTS) != null) {
                                i = ((Integer) analyseHead.get(Constants.KEYS.PLACEMENTS)).intValue();
                            }
                            Integer valueOf3 = Integer.valueOf(i);
                            boolean booleanValue = ((Boolean) analyseHead.get("isLastPage")).booleanValue();
                            modelResult.setList(jsonToList);
                            modelResult.setTotal(valueOf.intValue());
                            modelResult.setP(valueOf2.intValue());
                            modelResult.setPs(valueOf3.intValue());
                            modelResult.setLastPage(booleanValue);
                            break;
                        case 2:
                            modelResult.setObj(JsonUtil.jsonToObject(this.mTargetClass, jSONObject));
                            break;
                        case 3:
                            BaseVO jsonToObject2 = JsonUtil.jsonToObject(this.mTargetClass, jSONObject);
                            List<BaseVO> jsonToList2 = JsonUtil.jsonToList(this.mTargetClass2, jSONObject.getJSONArray("result_list"));
                            modelResult.setObj(jsonToObject2);
                            modelResult.setList(jsonToList2);
                            break;
                        case 4:
                            modelResult.setObj(jSONObject);
                            break;
                    }
                } else {
                    modelResult.setSuccess(false);
                    modelResult.setError_message(string2);
                }
            } else {
                modelResult.setObj(stringBuffer.toString());
                return modelResult;
            }
        }
        return modelResult;
    }

    /* access modifiers changed from: protected */
    public void setReq(HttpMethod httpMethod) throws Exception {
        if ("POST".equals(this.mMethod)) {
            PostMethod postMethod = (PostMethod) httpMethod;
            this.mParam.toString();
            postMethod.addParameter("Connection", "Keep-Alive");
            postMethod.addParameter("Charset", "UTF-8");
            postMethod.setRequestEntity(new ByteArrayRequestEntity(this.mParam.toString().getBytes(StringUtil.Encoding)));
        }
    }

    public void setReq(String str) throws Exception {
        if ("POST".equals(this.mMethod)) {
            HttpReq.UTF8PostMethod uTF8PostMethod = new HttpReq.UTF8PostMethod(this.mUrl);
            this.mParam.toString();
            uTF8PostMethod.setRequestEntity(new ByteArrayRequestEntity(this.mParam.toString().getBytes(StringUtil.Encoding)));
        }
    }
}
