package com.tencent.weibo.sdk.android.network;

import android.os.AsyncTask;
import android.util.Log;
import cn.banshenggua.aichang.room.message.SocketMessage;
import cn.banshenggua.aichang.utils.StringUtil;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.zip.GZIPInputStream;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.mina.proxy.handlers.http.HttpProxyConstants;

public abstract class HttpReq extends AsyncTask<Void, Integer, Object> {
    private final String GET = "GET";
    private final String POST = "POST";
    protected HttpCallback mCallBack = null;
    protected String mHost = null;
    protected String mMethod = null;
    protected ReqParam mParam = new ReqParam();
    protected int mPort = HttpConfig.CRM_SERVER_PORT;
    private int mServiceTag = -1;
    protected String mUrl = null;

    /* access modifiers changed from: protected */
    public abstract Object processResponse(InputStream inputStream) throws Exception;

    /* access modifiers changed from: protected */
    public abstract void setReq(HttpMethod httpMethod) throws Exception;

    public void setServiceTag(int i) {
        this.mServiceTag = i;
    }

    public int getServiceTag() {
        return this.mServiceTag;
    }

    /* access modifiers changed from: protected */
    public HttpCallback getCallBack() {
        return this.mCallBack;
    }

    public void setParam(ReqParam reqParam) {
        this.mParam = reqParam;
    }

    public void addParam(String str, String str2) {
        this.mParam.addParam(str, str2);
    }

    public void addParam(String str, Object obj) {
        this.mParam.addParam(str, obj);
    }

    public Object runReq() throws Exception {
        GetMethod uTF8PostMethod;
        HttpClient httpClient = new HttpClient();
        if (this.mMethod.equals("GET")) {
            this.mUrl = String.valueOf(this.mUrl) + "?" + this.mParam.toString().substring(0, this.mParam.toString().length() - 1);
            uTF8PostMethod = new GetMethod(this.mUrl);
        } else if (!this.mMethod.equals("POST")) {
            throw new Exception("unrecognized http method");
        } else if (this.mParam.getmParams().get("pic") != null) {
            return processResponse(picMethod());
        } else {
            uTF8PostMethod = new UTF8PostMethod(this.mUrl);
        }
        httpClient.getHostConfiguration().setHost(this.mHost, this.mPort, "https");
        uTF8PostMethod.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        setReq(uTF8PostMethod);
        int executeMethod = httpClient.executeMethod(uTF8PostMethod);
        Log.d(SocketMessage.MSG_RESULE_KEY, new StringBuilder(String.valueOf(executeMethod)).toString());
        if (executeMethod != 200) {
            return null;
        }
        return processResponse(uTF8PostMethod.getResponseBodyAsStream());
    }

    private InputStream picMethod() {
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(this.mUrl);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        String reqParam = this.mParam.toString();
        httpPost.setHeader("Content-Type", String.valueOf("multipart/form-data") + "; boundary=" + "---------------------------7da2137580612");
        if (reqParam != null) {
            try {
                if (!reqParam.equals("")) {
                    for (String str : reqParam.split("&")) {
                        if (str != null && !str.equals("") && str.indexOf("=") > -1) {
                            String[] split = str.split("=");
                            String decode = split.length == 2 ? decode(split[1]) : "";
                            StringBuilder sb = new StringBuilder();
                            sb.append("-----------------------------7da2137580612\r\n");
                            sb.append("Content-Disposition:form-data; name=\"" + split[0] + "\"" + HttpProxyConstants.CRLF);
                            sb.append(HttpProxyConstants.CRLF);
                            sb.append(decode);
                            sb.append(HttpProxyConstants.CRLF);
                            byteArrayOutputStream.write(sb.toString().getBytes(StringUtil.Encoding));
                        }
                    }
                    byteArrayOutputStream.write(("-----------------------------7da2137580612\r\n" + "Content-Disposition:form-data; name=\"pic\"; filename=\"123456.jpg\"\r\n" + "Content-Type:image/jpeg\r\n\r\n").getBytes(StringUtil.Encoding));
                    char[] charArray = this.mParam.getmParams().get("pic").toCharArray();
                    byte[] bArr = new byte[charArray.length];
                    for (int i = 0; i < charArray.length; i++) {
                        bArr[i] = (byte) charArray[i];
                    }
                    byteArrayOutputStream.write(bArr);
                    byteArrayOutputStream.write("---------------------------7da2137580612\r\n".getBytes(StringUtil.Encoding));
                }
            } catch (IOException e) {
                return null;
            }
        }
        byteArrayOutputStream.write("-----------------------------7da2137580612--\r\n".getBytes(StringUtil.Encoding));
        httpPost.setEntity(new ByteArrayEntity(byteArrayOutputStream.toByteArray()));
        byteArrayOutputStream.close();
        HttpResponse execute = defaultHttpClient.execute(httpPost);
        if (execute.getStatusLine().getStatusCode() == 200) {
            return readHttpResponse(execute);
        }
        readHttpResponse(execute);
        return null;
    }

    public static String decode(String str) {
        if (str == null) {
            return "";
        }
        try {
            return URLDecoder.decode(str, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    private static InputStream readHttpResponse(HttpResponse httpResponse) {
        try {
            InputStream content = httpResponse.getEntity().getContent();
            try {
                Header firstHeader = httpResponse.getFirstHeader("Content-Encoding");
                if (firstHeader == null || firstHeader.getValue().toLowerCase().indexOf("gzip") <= -1) {
                    return content;
                }
                return new GZIPInputStream(content);
            } catch (IllegalStateException e) {
                return content;
            } catch (IOException e2) {
                return content;
            }
        } catch (IllegalStateException e3) {
            return null;
        } catch (IOException e4) {
            return null;
        }
    }

    public static class UTF8PostMethod extends PostMethod {
        public UTF8PostMethod(String str) {
            super(str);
        }

        public String getRequestCharSet() {
            return "UTF-8";
        }
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        super.onPreExecute();
    }

    /* access modifiers changed from: protected */
    public Object doInBackground(Void... voidArr) {
        try {
            return runReq();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Object obj) {
        if (this.mCallBack != null) {
            this.mCallBack.onResult(obj);
        }
        HttpService.getInstance().onReqFinish(this);
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        if (this.mCallBack != null) {
            this.mCallBack.onResult(null);
        }
        HttpService.getInstance().onReqFinish(this);
    }
}
