package com.tencent.weibo.sdk.android.component;

import android.app.Activity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.widget.TextView;
import com.sina.weibo.sdk.component.ShareRequestParam;

public class GeneralDataShowActivity extends Activity {
    private TextView tv;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.tv = new TextView(this);
        this.tv.setText(getIntent().getExtras().getString(ShareRequestParam.RESP_UPLOAD_PIC_PARAM_DATA));
        this.tv.setMovementMethod(ScrollingMovementMethod.getInstance());
        setContentView(this.tv);
    }
}
