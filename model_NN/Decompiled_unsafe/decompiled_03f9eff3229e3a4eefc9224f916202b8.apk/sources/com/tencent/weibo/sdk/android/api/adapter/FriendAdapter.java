package com.tencent.weibo.sdk.android.api.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.weibo.sdk.android.api.util.BackGroudSeletor;
import com.tencent.weibo.sdk.android.model.Firend;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Map;

public class FriendAdapter extends BaseExpandableListAdapter {
    private Map<String, List<Firend>> child;
    /* access modifiers changed from: private */
    public Context ctx;
    private List<String> group;

    public FriendAdapter(Context context, List<String> list, Map<String, List<Firend>> map) {
        this.ctx = context;
        this.group = list;
        this.child = map;
    }

    public Object getChild(int i, int i2) {
        return this.child.get(getGroup(i)).get(i2);
    }

    public long getChildId(int i, int i2) {
        return (long) i2;
    }

    public List<String> getGroup() {
        return this.group;
    }

    public void setGroup(List<String> list) {
        this.group = list;
    }

    public Map<String, List<Firend>> getChild() {
        return this.child;
    }

    public void setChild(Map<String, List<Firend>> map) {
        this.child = map;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    public View getChildView(final int i, final int i2, boolean z, View view, ViewGroup viewGroup) {
        final LinearLayout linearLayout;
        if (view == null) {
            LinearLayout linearLayout2 = new LinearLayout(this.ctx);
            new LinearLayout.LayoutParams(-1, -2);
            linearLayout2.setOrientation(0);
            linearLayout2.setGravity(16);
            linearLayout2.setPadding(15, 0, 10, 0);
            linearLayout2.setBackgroundDrawable(BackGroudSeletor.getdrawble("childbg_", this.ctx));
            ImageView imageView = new ImageView(this.ctx);
            imageView.setId(4502);
            imageView.setLayoutParams(new LinearLayout.LayoutParams(45, 45));
            imageView.setImageDrawable(BackGroudSeletor.getdrawble("logo", this.ctx));
            LinearLayout linearLayout3 = new LinearLayout(this.ctx);
            linearLayout3.setPadding(10, 0, 0, 0);
            linearLayout3.setGravity(16);
            linearLayout3.setOrientation(1);
            TextView textView = new TextView(this.ctx);
            TextView textView2 = new TextView(this.ctx);
            textView.setTextSize(18.0f);
            textView.setId(4500);
            textView.setTextColor(Color.parseColor("#32769b"));
            textView2.setTextSize(12.0f);
            textView2.setId(4501);
            textView2.setTextColor(Color.parseColor("#a6c6d5"));
            textView.setText(((Firend) getChild(i, i2)).getNick());
            textView2.setText(((Firend) getChild(i, i2)).getName());
            linearLayout3.addView(textView);
            linearLayout3.addView(textView2);
            linearLayout2.setBackgroundDrawable(BackGroudSeletor.getdrawble("childbg_", this.ctx));
            linearLayout2.addView(imageView);
            linearLayout2.addView(linearLayout3);
            linearLayout2.setTag(linearLayout2);
            linearLayout = linearLayout2;
        } else {
            LinearLayout linearLayout4 = (LinearLayout) view.getTag();
            ((ImageView) ((LinearLayout) linearLayout4.getTag()).findViewById(4502)).setImageDrawable(BackGroudSeletor.getdrawble("logo", this.ctx));
            ((TextView) linearLayout4.findViewById(4500)).setText(((Firend) getChild(i, i2)).getNick());
            ((TextView) linearLayout4.findViewById(4501)).setText(((Firend) getChild(i, i2)).getName());
            linearLayout = view;
        }
        if (((Firend) getChild(i, i2)).getHeadurl() != null) {
            new AsyncTask<String, Integer, Bitmap>() {
                /* access modifiers changed from: protected */
                public Bitmap doInBackground(String... strArr) {
                    try {
                        Log.d("image urel", new StringBuilder(String.valueOf(((Firend) FriendAdapter.this.getChild(i, i2)).getHeadurl())).toString());
                        URL url = new URL(((Firend) FriendAdapter.this.getChild(i, i2)).getHeadurl());
                        String headerField = url.openConnection().getHeaderField(0);
                        if (headerField.indexOf("200") < 0) {
                            Log.d("图片文件不存在或路径错误，错误代码：", headerField);
                        }
                        return BitmapFactory.decodeStream(url.openStream());
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                        return null;
                    } catch (IOException e2) {
                        e2.printStackTrace();
                        return null;
                    }
                }

                /* access modifiers changed from: protected */
                public void onPreExecute() {
                    super.onPreExecute();
                }

                /* access modifiers changed from: protected */
                public void onPostExecute(Bitmap bitmap) {
                    ImageView imageView = (ImageView) ((LinearLayout) linearLayout.getTag()).findViewById(4502);
                    if (bitmap == null) {
                        imageView.setImageDrawable(BackGroudSeletor.getdrawble("logo", FriendAdapter.this.ctx));
                    } else {
                        imageView.setImageBitmap(bitmap);
                    }
                    super.onPostExecute((Object) bitmap);
                }
            }.execute(((Firend) getChild(i, i2)).getHeadurl());
        } else {
            ((ImageView) ((LinearLayout) linearLayout.getTag()).findViewById(4502)).setImageDrawable(BackGroudSeletor.getdrawble("logo", this.ctx));
        }
        return linearLayout;
    }

    public int getChildrenCount(int i) {
        return this.child.get(this.group.get(i)).size();
    }

    public Object getGroup(int i) {
        return this.group.get(i);
    }

    public int getGroupCount() {
        return this.group.size();
    }

    public long getGroupId(int i) {
        return (long) i;
    }

    public View getGroupView(int i, boolean z, View view, ViewGroup viewGroup) {
        if (view == null) {
            LinearLayout linearLayout = new LinearLayout(this.ctx);
            TextView textView = new TextView(this.ctx);
            linearLayout.setPadding(30, 0, 0, 0);
            linearLayout.setGravity(16);
            textView.setTextSize(18.0f);
            textView.setTextColor(-1);
            textView.setWidth(40);
            textView.setText(getGroup(i).toString().toUpperCase());
            linearLayout.addView(textView);
            linearLayout.setBackgroundDrawable(BackGroudSeletor.getdrawble("groupbg_", this.ctx));
            textView.setTag(Integer.valueOf(i));
            linearLayout.setTag(textView);
            return linearLayout;
        }
        ((TextView) view.getTag()).setText(getGroup(i).toString().toUpperCase());
        return view;
    }

    public boolean hasStableIds() {
        return true;
    }

    public boolean isChildSelectable(int i, int i2) {
        return true;
    }
}
