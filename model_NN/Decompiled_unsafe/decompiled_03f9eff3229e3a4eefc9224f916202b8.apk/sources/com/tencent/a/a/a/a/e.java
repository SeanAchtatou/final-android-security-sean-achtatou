package com.tencent.a.a.a.a;

import android.content.Context;
import android.provider.Settings;
import android.util.Log;

public final class e extends f {
    public e(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public final boolean a() {
        return h.a(this.e, "android.permission.WRITE_SETTINGS");
    }

    /* access modifiers changed from: protected */
    public final String b() {
        String string;
        synchronized (this) {
            Log.i("MID", "read mid from Settings.System");
            string = Settings.System.getString(this.e.getContentResolver(), h.f("4kU71lN96TJUomD1vOU9lgj9Tw=="));
        }
        return string;
    }

    /* access modifiers changed from: protected */
    public final void b(String str) {
        synchronized (this) {
            Log.i("MID", "write mid to Settings.System");
            Settings.System.putString(this.e.getContentResolver(), h.f("4kU71lN96TJUomD1vOU9lgj9Tw=="), str);
        }
    }
}
