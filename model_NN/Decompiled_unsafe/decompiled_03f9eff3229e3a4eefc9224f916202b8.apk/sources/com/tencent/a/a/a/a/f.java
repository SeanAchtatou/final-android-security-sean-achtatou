package com.tencent.a.a.a.a;

import android.content.Context;

public abstract class f {
    protected Context e = null;

    protected f(Context context) {
        this.e = context;
    }

    public final void a(c cVar) {
        if (cVar != null) {
            String cVar2 = cVar.toString();
            if (a()) {
                b(h.g(cVar2));
            }
        }
    }

    /* access modifiers changed from: protected */
    public abstract boolean a();

    /* access modifiers changed from: protected */
    public abstract String b();

    /* access modifiers changed from: protected */
    public abstract void b(String str);

    public final c e() {
        String f = a() ? h.f(b()) : null;
        if (f != null) {
            return c.c(f);
        }
        return null;
    }
}
