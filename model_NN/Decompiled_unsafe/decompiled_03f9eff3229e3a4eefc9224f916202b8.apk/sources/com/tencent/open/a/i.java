package com.tencent.open.a;

import com.tencent.open.a.d;

/* compiled from: ProGuard */
public abstract class i {

    /* renamed from: a  reason: collision with root package name */
    private volatile int f2013a;
    private volatile boolean b;
    private h c;

    /* access modifiers changed from: protected */
    public abstract void a(int i, Thread thread, long j, String str, String str2, Throwable th);

    public i() {
        this(c.f2007a, true, h.f2012a);
    }

    public i(int i, boolean z, h hVar) {
        this.f2013a = c.f2007a;
        this.b = true;
        this.c = h.f2012a;
        a(i);
        a(z);
        a(hVar);
    }

    public void b(int i, Thread thread, long j, String str, String str2, Throwable th) {
        if (d() && d.a.a(this.f2013a, i)) {
            a(i, thread, j, str, str2, th);
        }
    }

    public void a(int i) {
        this.f2013a = i;
    }

    public boolean d() {
        return this.b;
    }

    public void a(boolean z) {
        this.b = z;
    }

    public h e() {
        return this.c;
    }

    public void a(h hVar) {
        this.c = hVar;
    }
}
