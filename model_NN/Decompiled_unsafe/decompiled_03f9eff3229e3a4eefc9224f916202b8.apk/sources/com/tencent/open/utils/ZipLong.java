package com.tencent.open.utils;

import android.support.v4.view.MotionEventCompat;
import org.apache.mina.proxy.handlers.socks.SocksProxyConstants;

/* compiled from: ProGuard */
public final class ZipLong implements Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private long f2051a;

    public ZipLong(byte[] bArr) {
        this(bArr, 0);
    }

    public ZipLong(byte[] bArr, int i) {
        this.f2051a = ((long) (bArr[i + 3] << 24)) & 4278190080L;
        this.f2051a += (long) ((bArr[i + 2] << 16) & 16711680);
        this.f2051a += (long) ((bArr[i + 1] << 8) & MotionEventCompat.ACTION_POINTER_INDEX_MASK);
        this.f2051a += (long) (bArr[i] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD);
    }

    public ZipLong(long j) {
        this.f2051a = j;
    }

    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof ZipLong) || this.f2051a != ((ZipLong) obj).getValue()) {
            return false;
        }
        return true;
    }

    public byte[] getBytes() {
        return new byte[]{(byte) ((int) (this.f2051a & 255)), (byte) ((int) ((this.f2051a & 65280) >> 8)), (byte) ((int) ((this.f2051a & 16711680) >> 16)), (byte) ((int) ((this.f2051a & 4278190080L) >> 24))};
    }

    public long getValue() {
        return this.f2051a;
    }

    public int hashCode() {
        return (int) this.f2051a;
    }
}
