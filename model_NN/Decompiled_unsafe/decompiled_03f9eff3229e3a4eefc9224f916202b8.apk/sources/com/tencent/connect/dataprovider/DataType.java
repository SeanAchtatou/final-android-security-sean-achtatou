package com.tencent.connect.dataprovider;

import android.os.Parcel;
import android.os.Parcelable;

/* compiled from: ProGuard */
public final class DataType {
    public static final int CONTENT_AND_IMAGE_PATH = 1;
    public static final int CONTENT_AND_VIDEO_PATH = 2;
    public static final int CONTENT_ONLY = 4;

    /* compiled from: ProGuard */
    public static class TextAndMediaPath implements Parcelable {
        public static final Parcelable.Creator<TextAndMediaPath> CREATOR = new Parcelable.Creator<TextAndMediaPath>() {
            public TextAndMediaPath createFromParcel(Parcel parcel) {
                return new TextAndMediaPath(parcel);
            }

            public TextAndMediaPath[] newArray(int i) {
                return new TextAndMediaPath[i];
            }
        };

        /* renamed from: a  reason: collision with root package name */
        private String f1923a;
        private String b;

        public TextAndMediaPath(String str, String str2) {
            this.f1923a = str;
            this.b = str2;
        }

        public String getText() {
            return this.f1923a;
        }

        public String getMediaPath() {
            return this.b;
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(this.f1923a);
            parcel.writeString(this.b);
        }

        private TextAndMediaPath(Parcel parcel) {
            this.f1923a = parcel.readString();
            this.b = parcel.readString();
        }
    }

    /* compiled from: ProGuard */
    public static class TextOnly implements Parcelable {
        public static final Parcelable.Creator<TextOnly> CREATOR = new Parcelable.Creator<TextOnly>() {
            public TextOnly createFromParcel(Parcel parcel) {
                return new TextOnly(parcel);
            }

            public TextOnly[] newArray(int i) {
                return new TextOnly[i];
            }
        };

        /* renamed from: a  reason: collision with root package name */
        private String f1924a;

        public TextOnly(String str) {
            this.f1924a = str;
        }

        public String getText() {
            return this.f1924a;
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(this.f1924a);
        }

        private TextOnly(Parcel parcel) {
            this.f1924a = parcel.readString();
        }
    }
}
