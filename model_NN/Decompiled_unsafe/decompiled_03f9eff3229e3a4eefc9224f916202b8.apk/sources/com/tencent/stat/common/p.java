package com.tencent.stat.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class p {

    /* renamed from: a  reason: collision with root package name */
    private static SharedPreferences f2101a = null;

    public static int a(Context context, String str, int i) {
        return a(context).getInt(k.b(context, "" + str), i);
    }

    public static long a(Context context, String str, long j) {
        return a(context).getLong(k.b(context, "" + str), j);
    }

    static synchronized SharedPreferences a(Context context) {
        SharedPreferences sharedPreferences;
        synchronized (p.class) {
            if (f2101a == null) {
                f2101a = PreferenceManager.getDefaultSharedPreferences(context);
            }
            sharedPreferences = f2101a;
        }
        return sharedPreferences;
    }

    public static String a(Context context, String str, String str2) {
        return a(context).getString(k.b(context, "" + str), str2);
    }

    public static void b(Context context, String str, int i) {
        String b = k.b(context, "" + str);
        SharedPreferences.Editor edit = a(context).edit();
        edit.putInt(b, i);
        edit.commit();
    }

    public static void b(Context context, String str, long j) {
        String b = k.b(context, "" + str);
        SharedPreferences.Editor edit = a(context).edit();
        edit.putLong(b, j);
        edit.commit();
    }

    public static void b(Context context, String str, String str2) {
        String b = k.b(context, "" + str);
        SharedPreferences.Editor edit = a(context).edit();
        edit.putString(b, str2);
        edit.commit();
    }
}
