package com.tencent.stat;

public class StatGameUser implements Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private String f2073a = "";
    private String b = "";
    private String c = "";

    public StatGameUser clone() {
        try {
            return (StatGameUser) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

    public String getAccount() {
        return this.b;
    }

    public String getLevel() {
        return this.c;
    }

    public String getWorldName() {
        return this.f2073a;
    }

    public void setAccount(String str) {
        this.b = str;
    }

    public void setLevel(String str) {
        this.c = str;
    }

    public void setWorldName(String str) {
        this.f2073a = str;
    }
}
