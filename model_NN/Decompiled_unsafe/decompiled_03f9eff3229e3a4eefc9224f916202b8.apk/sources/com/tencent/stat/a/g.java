package com.tencent.stat.a;

import android.content.Context;
import com.tencent.stat.StatGameUser;
import com.tencent.stat.common.k;
import org.json.JSONObject;

public class g extends e {

    /* renamed from: a  reason: collision with root package name */
    private StatGameUser f2085a = null;

    public g(Context context, int i, StatGameUser statGameUser) {
        super(context, i);
        this.f2085a = statGameUser.clone();
    }

    public f a() {
        return f.MTA_GAME_USER;
    }

    public boolean a(JSONObject jSONObject) {
        if (this.f2085a == null) {
            return false;
        }
        k.a(jSONObject, "wod", this.f2085a.getWorldName());
        k.a(jSONObject, "gid", this.f2085a.getAccount());
        k.a(jSONObject, "lev", this.f2085a.getLevel());
        return true;
    }
}
