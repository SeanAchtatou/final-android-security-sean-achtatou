package com.tencent.stat.a;

import cn.banshenggua.aichang.utils.Constants;
import com.baidu.mobads.CpuInfoManager;

public enum f {
    PAGE_VIEW(1),
    SESSION_ENV(2),
    ERROR(3),
    CUSTOM(Constants.CLEARIMGED),
    ADDITION(1001),
    MONITOR_STAT(1002),
    MTA_GAME_USER(CpuInfoManager.CHANNEL_PICTURE),
    NETWORK_MONITOR(1004);
    
    private int i;

    private f(int i2) {
        this.i = i2;
    }

    public int a() {
        return this.i;
    }
}
