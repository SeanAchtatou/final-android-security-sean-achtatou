package com.tencent.stat.a;

import android.content.Context;
import com.tencent.stat.StatConfig;
import com.tencent.stat.common.k;
import java.io.PrintWriter;
import java.io.StringWriter;
import org.json.JSONObject;

public class d extends e {

    /* renamed from: a  reason: collision with root package name */
    private String f2082a;
    private int l;
    private int m = 100;

    public d(Context context, int i, int i2, Throwable th) {
        super(context, i);
        if (th != null) {
            Throwable th2 = new Throwable(th);
            try {
                StackTraceElement[] stackTrace = th2.getStackTrace();
                if (stackTrace != null && stackTrace.length > this.m) {
                    StackTraceElement[] stackTraceElementArr = new StackTraceElement[this.m];
                    for (int i3 = 0; i3 < this.m; i3++) {
                        stackTraceElementArr[i3] = stackTrace[i3];
                    }
                    th2.setStackTrace(stackTraceElementArr);
                }
            } catch (Throwable th3) {
                th3.printStackTrace();
            }
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            th2.printStackTrace(printWriter);
            this.f2082a = stringWriter.toString();
            this.l = i2;
            printWriter.close();
        }
    }

    public d(Context context, int i, String str, int i2, int i3) {
        super(context, i);
        if (str != null) {
            i3 = i3 <= 0 ? StatConfig.getMaxReportEventLength() : i3;
            if (str.length() <= i3) {
                this.f2082a = str;
            } else {
                this.f2082a = str.substring(0, i3);
            }
        }
        this.l = i2;
    }

    public f a() {
        return f.ERROR;
    }

    public void a(long j) {
        this.c = j;
    }

    public boolean a(JSONObject jSONObject) {
        k.a(jSONObject, "er", this.f2082a);
        jSONObject.put("ea", this.l);
        return true;
    }
}
