package com.tencent.map.b;

import android.content.Context;
import android.location.GpsSatellite;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import java.util.Iterator;

public final class e {
    private static LocationManager b = null;
    private static float d = 0.0f;

    /* renamed from: a  reason: collision with root package name */
    private Context f1950a = null;
    private a c = null;
    /* access modifiers changed from: private */
    public c e = null;
    /* access modifiers changed from: private */
    public b f = null;
    private boolean g = false;
    private byte[] h = new byte[0];
    /* access modifiers changed from: private */
    public int i = 1024;
    /* access modifiers changed from: private */
    public long j = 0;
    private boolean k = false;
    /* access modifiers changed from: private */
    public int l = 0;
    /* access modifiers changed from: private */
    public int m = 0;

    class a implements GpsStatus.Listener, LocationListener {
        private a() {
        }

        /* synthetic */ a(e eVar, byte b) {
            this();
        }

        public final void onGpsStatusChanged(int i) {
            switch (i) {
                case 1:
                    e.a(e.this, 1);
                    break;
                case 2:
                    e.a(e.this, 0);
                    break;
                case 3:
                    e.a(e.this, 2);
                    break;
            }
            e.this.b();
        }

        public final void onLocationChanged(Location location) {
            boolean z = false;
            if (location != null) {
                double latitude = location.getLatitude();
                double longitude = location.getLongitude();
                if (latitude != 29.999998211860657d && longitude != 103.99999916553497d && Math.abs(latitude) >= 1.0E-8d && Math.abs(longitude) >= 1.0E-8d && latitude >= -90.0d && latitude <= 90.0d && longitude >= -180.0d && longitude <= 180.0d) {
                    z = true;
                }
                if (z) {
                    long unused = e.this.j = System.currentTimeMillis();
                    e.this.b();
                    e.a(e.this, 2);
                    b unused2 = e.this.f = new b(e.this, location, e.this.l, e.this.m, e.this.i, e.this.j);
                    if (e.this.e != null) {
                        e.this.e.a(e.this.f);
                    }
                }
            }
        }

        public final void onProviderDisabled(String str) {
            if (str != null) {
                try {
                    if (str.equals("gps")) {
                        int unused = e.this.l = e.this.m = 0;
                        int unused2 = e.this.i = 0;
                        if (e.this.e != null) {
                            e.this.e.a(e.this.i);
                        }
                    }
                } catch (Exception e) {
                }
            }
        }

        public final void onProviderEnabled(String str) {
            if (str != null) {
                try {
                    if (str.equals("gps")) {
                        int unused = e.this.i = 4;
                        if (e.this.e != null) {
                            e.this.e.a(e.this.i);
                        }
                    }
                } catch (Exception e) {
                }
            }
        }

        public final void onStatusChanged(String str, int i, Bundle bundle) {
        }
    }

    public class b implements Cloneable {

        /* renamed from: a  reason: collision with root package name */
        private Location f1952a = null;
        private long b = 0;
        private int c = 0;

        public b(e eVar, Location location, int i, int i2, int i3, long j) {
            if (location != null) {
                this.f1952a = new Location(location);
                this.c = i2;
                this.b = j;
            }
        }

        public final boolean a() {
            if (this.f1952a == null) {
                return false;
            }
            return (this.c <= 0 || this.c >= 3) && System.currentTimeMillis() - this.b <= StatisticConfig.MIN_UPLOAD_INTERVAL;
        }

        public final Location b() {
            return this.f1952a;
        }

        public final Object clone() {
            b bVar;
            try {
                bVar = (b) super.clone();
            } catch (Exception e) {
                bVar = null;
            }
            if (this.f1952a != null) {
                bVar.f1952a = new Location(this.f1952a);
            }
            return bVar;
        }
    }

    public interface c {
        void a(int i);

        void a(b bVar);
    }

    static /* synthetic */ int a(e eVar, int i2) {
        int i3 = eVar.i | i2;
        eVar.i = i3;
        return i3;
    }

    /* access modifiers changed from: private */
    public void b() {
        this.m = 0;
        this.l = 0;
        GpsStatus gpsStatus = b.getGpsStatus(null);
        if (gpsStatus != null) {
            int maxSatellites = gpsStatus.getMaxSatellites();
            Iterator<GpsSatellite> it = gpsStatus.getSatellites().iterator();
            if (it != null) {
                while (it.hasNext() && this.l <= maxSatellites) {
                    this.l++;
                    if (it.next().usedInFix()) {
                        this.m++;
                    }
                }
            }
        }
    }

    public final void a() {
        synchronized (this.h) {
            if (this.g) {
                if (!(b == null || this.c == null)) {
                    b.removeGpsStatusListener(this.c);
                    b.removeUpdates(this.c);
                }
                this.g = false;
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:43:?, code lost:
        return false;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(com.tencent.map.b.e.c r9, android.content.Context r10) {
        /*
            r8 = this;
            r0 = 1
            r6 = 0
            byte[] r7 = r8.h
            monitor-enter(r7)
            boolean r1 = r8.g     // Catch:{ all -> 0x0068 }
            if (r1 == 0) goto L_0x000b
            monitor-exit(r7)     // Catch:{ all -> 0x0068 }
        L_0x000a:
            return r0
        L_0x000b:
            if (r10 == 0) goto L_0x000f
            if (r9 != 0) goto L_0x0012
        L_0x000f:
            monitor-exit(r7)
            r0 = r6
            goto L_0x000a
        L_0x0012:
            r8.f1950a = r10     // Catch:{ all -> 0x0068 }
            r8.e = r9     // Catch:{ all -> 0x0068 }
            android.content.Context r0 = r8.f1950a     // Catch:{ Exception -> 0x0035 }
            java.lang.String r1 = "location"
            java.lang.Object r0 = r0.getSystemService(r1)     // Catch:{ Exception -> 0x0035 }
            android.location.LocationManager r0 = (android.location.LocationManager) r0     // Catch:{ Exception -> 0x0035 }
            com.tencent.map.b.e.b = r0     // Catch:{ Exception -> 0x0035 }
            com.tencent.map.b.e$a r0 = new com.tencent.map.b.e$a     // Catch:{ Exception -> 0x0035 }
            r1 = 0
            r0.<init>(r8, r1)     // Catch:{ Exception -> 0x0035 }
            r8.c = r0     // Catch:{ Exception -> 0x0035 }
            android.location.LocationManager r0 = com.tencent.map.b.e.b     // Catch:{ Exception -> 0x0035 }
            if (r0 == 0) goto L_0x0032
            com.tencent.map.b.e$a r0 = r8.c     // Catch:{ Exception -> 0x0035 }
            if (r0 != 0) goto L_0x0039
        L_0x0032:
            monitor-exit(r7)     // Catch:{ all -> 0x0068 }
            r0 = r6
            goto L_0x000a
        L_0x0035:
            r0 = move-exception
            monitor-exit(r7)
            r0 = r6
            goto L_0x000a
        L_0x0039:
            android.location.LocationManager r0 = com.tencent.map.b.e.b     // Catch:{ Exception -> 0x0064 }
            java.lang.String r1 = "gps"
            r2 = 1000(0x3e8, double:4.94E-321)
            r4 = 0
            com.tencent.map.b.e$a r5 = r8.c     // Catch:{ Exception -> 0x0064 }
            r0.requestLocationUpdates(r1, r2, r4, r5)     // Catch:{ Exception -> 0x0064 }
            android.location.LocationManager r0 = com.tencent.map.b.e.b     // Catch:{ Exception -> 0x0064 }
            com.tencent.map.b.e$a r1 = r8.c     // Catch:{ Exception -> 0x0064 }
            r0.addGpsStatusListener(r1)     // Catch:{ Exception -> 0x0064 }
            android.location.LocationManager r0 = com.tencent.map.b.e.b     // Catch:{ Exception -> 0x0064 }
            java.lang.String r1 = "gps"
            boolean r0 = r0.isProviderEnabled(r1)     // Catch:{ Exception -> 0x0064 }
            if (r0 == 0) goto L_0x0060
            r0 = 4
            r8.i = r0     // Catch:{ Exception -> 0x0064 }
        L_0x0059:
            r0 = 1
            r8.g = r0     // Catch:{ all -> 0x0068 }
            monitor-exit(r7)     // Catch:{ all -> 0x0068 }
            boolean r0 = r8.g
            goto L_0x000a
        L_0x0060:
            r0 = 0
            r8.i = r0     // Catch:{ Exception -> 0x0064 }
            goto L_0x0059
        L_0x0064:
            r0 = move-exception
            monitor-exit(r7)     // Catch:{ all -> 0x0068 }
            r0 = r6
            goto L_0x000a
        L_0x0068:
            r0 = move-exception
            monitor-exit(r7)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.map.b.e.a(com.tencent.map.b.e$c, android.content.Context):boolean");
    }
}
