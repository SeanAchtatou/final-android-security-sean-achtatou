package com.tencent.map.b;

import android.support.v4.view.MotionEventCompat;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private static a f1939a = null;

    /* renamed from: com.tencent.map.b.a$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ a f1940a;

        private AnonymousClass1(a aVar) {
            this.f1940a = aVar;
        }

        /* synthetic */ AnonymousClass1(a aVar, byte b) {
            this(aVar);
        }

        public boolean a(String str, String str2) {
            int a2 = a.a(this.f1940a, str);
            if (str2.charAt(4) != i.f1964a.charAt(((((a2 * 9) + 10) / 3) + 36) & 31)) {
                return false;
            }
            if (str2.charAt(7) != i.f1964a.charAt((((a2 * 5) + 11) / 5) & 31)) {
                return false;
            }
            if (str2.charAt(12) != i.f1964a.charAt((((a2 + 10) / 3) << 3) & 31)) {
                return false;
            }
            if (str2.charAt(14) != i.f1964a.charAt((((a2 * 3) + 19) / 9) & 31)) {
                return false;
            }
            if (str2.charAt(19) != i.f1964a.charAt((((a2 * 3) + 39) / 8) & 31)) {
                return false;
            }
            if (str2.charAt(21) != i.f1964a.charAt((((a2 / 23) + 67) / 7) & 31)) {
                return false;
            }
            if (str2.charAt(26) != i.f1964a.charAt(((((a2 + 23) / 6) + 3) * 7) & 31)) {
                return false;
            }
            int i = 0;
            for (int i2 = 0; i2 < str.length(); i2++) {
                i = i.b[(i ^ i.a(str.charAt(i2))) & MotionEventCompat.ACTION_MASK] ^ ((i >> 8) & MotionEventCompat.ACTION_MASK);
            }
            if (str2.charAt(0) != i.f1964a.charAt(i & 31)) {
                return false;
            }
            return str2.charAt(1) == i.f1964a.charAt((i >> 5) & 31);
        }
    }

    private a() {
    }

    static /* synthetic */ int a(a aVar, String str) {
        int length = str.length();
        int i = 0;
        for (int i2 = 0; i2 < length; i2++) {
            i += i.a(str.charAt(i2));
        }
        return ((length << 7) + length) ^ i;
    }

    public static synchronized a a() {
        a aVar;
        synchronized (a.class) {
            if (f1939a == null) {
                f1939a = new a();
            }
            aVar = f1939a;
        }
        return aVar;
    }

    public final boolean a(String str, String str2) {
        boolean z;
        if (!i.a(str) || !i.b(str2) || !new AnonymousClass1(this, (byte) 0).a(str, str2)) {
            return false;
        }
        int i = 0;
        for (int i2 = 0; i2 < 27; i2++) {
            i = i.b[(i ^ i.a(str2.charAt(i2))) & MotionEventCompat.ACTION_MASK] ^ ((i >> 8) & MotionEventCompat.ACTION_MASK);
        }
        if (str2.charAt(27) != i.f1964a.charAt(i & 31)) {
            z = false;
        } else {
            z = str2.charAt(28) == i.f1964a.charAt((i >> 5) & 31);
        }
        return z;
    }
}
