package com.pocketmusic.kshare.requestobjs;

import cn.banshenggua.aichang.api.APIKey;
import cn.banshenggua.aichang.api.KURL;
import cn.banshenggua.aichang.room.message.SocketMessage;
import com.sina.weibo.sdk.constant.WBPageConstants;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: WeiBoList */
public class x extends m implements Serializable {
    private static /* synthetic */ int[] p;
    private static /* synthetic */ int[] q;

    /* renamed from: a  reason: collision with root package name */
    public int f639a = 0;
    public int b = 0;
    public int c = 1;
    public int d = 0;
    public int e = 20;
    public String f = null;
    public String g = null;
    public b h;
    public boolean i = true;
    public String j = null;
    public String k = null;
    public String l = "";
    public a m = a.FIRST;
    public Map<String, String> n = new HashMap();
    private List<w> o = new ArrayList();

    /* compiled from: WeiBoList */
    public enum a {
        FIRST,
        FORWARD,
        REPLY
    }

    /* compiled from: WeiBoList */
    public enum b {
        TodaySelected,
        HMSelected
    }

    static /* synthetic */ int[] l() {
        int[] iArr = p;
        if (iArr == null) {
            iArr = new int[b.values().length];
            try {
                iArr[b.HMSelected.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[b.TodaySelected.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            p = iArr;
        }
        return iArr;
    }

    static /* synthetic */ int[] m() {
        int[] iArr = q;
        if (iArr == null) {
            iArr = new int[APIKey.values().length];
            try {
                iArr[APIKey.APIKEY_CrashLog.ordinal()] = 135;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[APIKey.APIKEY_GetUserAlbums.ordinal()] = 74;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[APIKey.APIKEY_LikeFanchang.ordinal()] = 60;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[APIKey.APIKEY_Modify_UserZoneHomePic.ordinal()] = 43;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[APIKey.APIKEY_SendLog.ordinal()] = 134;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[APIKey.APIKEY_UploadUserAlbums.ordinal()] = 75;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[APIKey.APIKey_ACTIVATE.ordinal()] = 152;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[APIKey.APIKey_AccountUpdateNotify.ordinal()] = 145;
            } catch (NoSuchFieldError e9) {
            }
            try {
                iArr[APIKey.APIKey_AccountUpdateSnsNotify.ordinal()] = 146;
            } catch (NoSuchFieldError e10) {
            }
            try {
                iArr[APIKey.APIKey_AddTo_Club.ordinal()] = 217;
            } catch (NoSuchFieldError e11) {
            }
            try {
                iArr[APIKey.APIKey_Add_Vice_Admin.ordinal()] = 224;
            } catch (NoSuchFieldError e12) {
            }
            try {
                iArr[APIKey.APIKey_BanZouUrl.ordinal()] = 136;
            } catch (NoSuchFieldError e13) {
            }
            try {
                iArr[APIKey.APIKey_BindSNS.ordinal()] = 141;
            } catch (NoSuchFieldError e14) {
            }
            try {
                iArr[APIKey.APIKey_Bind_Phone.ordinal()] = 178;
            } catch (NoSuchFieldError e15) {
            }
            try {
                iArr[APIKey.APIKey_BuyGift.ordinal()] = 195;
            } catch (NoSuchFieldError e16) {
            }
            try {
                iArr[APIKey.APIKey_COMMON_DISCOVERY.ordinal()] = 235;
            } catch (NoSuchFieldError e17) {
            }
            try {
                iArr[APIKey.APIKey_Category_AOD.ordinal()] = 66;
            } catch (NoSuchFieldError e18) {
            }
            try {
                iArr[APIKey.APIKey_Category_AOD_LIVE.ordinal()] = 67;
            } catch (NoSuchFieldError e19) {
            }
            try {
                iArr[APIKey.APIKey_Category_AOD_YQ.ordinal()] = 68;
            } catch (NoSuchFieldError e20) {
            }
            try {
                iArr[APIKey.APIKey_Category_TopList.ordinal()] = 72;
            } catch (NoSuchFieldError e21) {
            }
            try {
                iArr[APIKey.APIKey_Change_Phone.ordinal()] = 177;
            } catch (NoSuchFieldError e22) {
            }
            try {
                iArr[APIKey.APIKey_Channel.ordinal()] = 112;
            } catch (NoSuchFieldError e23) {
            }
            try {
                iArr[APIKey.APIKey_Channel_ByAll.ordinal()] = 115;
            } catch (NoSuchFieldError e24) {
            }
            try {
                iArr[APIKey.APIKey_Channel_ByPinyin.ordinal()] = 116;
            } catch (NoSuchFieldError e25) {
            }
            try {
                iArr[APIKey.APIKey_Channel_BySinger.ordinal()] = 113;
            } catch (NoSuchFieldError e26) {
            }
            try {
                iArr[APIKey.APIKey_Channel_BySongName.ordinal()] = 114;
            } catch (NoSuchFieldError e27) {
            }
            try {
                iArr[APIKey.APIKey_Channel_ByTopbanzou.ordinal()] = 117;
            } catch (NoSuchFieldError e28) {
            }
            try {
                iArr[APIKey.APIKey_Channel_Rank.ordinal()] = 150;
            } catch (NoSuchFieldError e29) {
            }
            try {
                iArr[APIKey.APIKey_CheckNickName.ordinal()] = 3;
            } catch (NoSuchFieldError e30) {
            }
            try {
                iArr[APIKey.APIKey_CheckUserName.ordinal()] = 2;
            } catch (NoSuchFieldError e31) {
            }
            try {
                iArr[APIKey.APIKey_Club_Add.ordinal()] = 207;
            } catch (NoSuchFieldError e32) {
            }
            try {
                iArr[APIKey.APIKey_Club_Apply_Add.ordinal()] = 210;
            } catch (NoSuchFieldError e33) {
            }
            try {
                iArr[APIKey.APIKey_Club_Apply_Del.ordinal()] = 211;
            } catch (NoSuchFieldError e34) {
            }
            try {
                iArr[APIKey.APIKey_Club_Apply_List.ordinal()] = 212;
            } catch (NoSuchFieldError e35) {
            }
            try {
                iArr[APIKey.APIKey_Club_Del.ordinal()] = 208;
            } catch (NoSuchFieldError e36) {
            }
            try {
                iArr[APIKey.APIKey_Club_Edit.ordinal()] = 209;
            } catch (NoSuchFieldError e37) {
            }
            try {
                iArr[APIKey.APIKey_Club_Relation.ordinal()] = 219;
            } catch (NoSuchFieldError e38) {
            }
            try {
                iArr[APIKey.APIKey_Club_User_List.ordinal()] = 213;
            } catch (NoSuchFieldError e39) {
            }
            try {
                iArr[APIKey.APIKey_Common_Property.ordinal()] = 88;
            } catch (NoSuchFieldError e40) {
            }
            try {
                iArr[APIKey.APIKey_DAY_EVENT.ordinal()] = 155;
            } catch (NoSuchFieldError e41) {
            }
            try {
                iArr[APIKey.APIKey_Default.ordinal()] = 1;
            } catch (NoSuchFieldError e42) {
            }
            try {
                iArr[APIKey.APIKey_DelTo_Club.ordinal()] = 218;
            } catch (NoSuchFieldError e43) {
            }
            try {
                iArr[APIKey.APIKey_Del_Vice_Admin.ordinal()] = 225;
            } catch (NoSuchFieldError e44) {
            }
            try {
                iArr[APIKey.APIKey_DeleteUserPics.ordinal()] = 76;
            } catch (NoSuchFieldError e45) {
            }
            try {
                iArr[APIKey.APIKey_DeleteUserWeibo.ordinal()] = 49;
            } catch (NoSuchFieldError e46) {
            }
            try {
                iArr[APIKey.APIKey_EVENT_SEND.ordinal()] = 151;
            } catch (NoSuchFieldError e47) {
            }
            try {
                iArr[APIKey.APIKey_Exchange.ordinal()] = 137;
            } catch (NoSuchFieldError e48) {
            }
            try {
                iArr[APIKey.APIKey_FanChang_Add_Cover.ordinal()] = 121;
            } catch (NoSuchFieldError e49) {
            }
            try {
                iArr[APIKey.APIKey_FanChang_Cover.ordinal()] = 119;
            } catch (NoSuchFieldError e50) {
            }
            try {
                iArr[APIKey.APIKey_FanChang_Cover_Data.ordinal()] = 120;
            } catch (NoSuchFieldError e51) {
            }
            try {
                iArr[APIKey.APIKey_FanChang_Info.ordinal()] = 118;
            } catch (NoSuchFieldError e52) {
            }
            try {
                iArr[APIKey.APIKey_FollowRoomList.ordinal()] = 80;
            } catch (NoSuchFieldError e53) {
            }
            try {
                iArr[APIKey.APIKey_Friends_In_ROOM.ordinal()] = 205;
            } catch (NoSuchFieldError e54) {
            }
            try {
                iArr[APIKey.APIKey_Friends_ROOM.ordinal()] = 203;
            } catch (NoSuchFieldError e55) {
            }
            try {
                iArr[APIKey.APIKey_Friends_Recommend_ROOM.ordinal()] = 204;
            } catch (NoSuchFieldError e56) {
            }
            try {
                iArr[APIKey.APIKey_GET_BALANCE.ordinal()] = 156;
            } catch (NoSuchFieldError e57) {
            }
            try {
                iArr[APIKey.APIKey_GET_FACE_SYSTEM.ordinal()] = 179;
            } catch (NoSuchFieldError e58) {
            }
            try {
                iArr[APIKey.APIKey_GUACHANG.ordinal()] = 63;
            } catch (NoSuchFieldError e59) {
            }
            try {
                iArr[APIKey.APIKey_GUACHANGList.ordinal()] = 147;
            } catch (NoSuchFieldError e60) {
            }
            try {
                iArr[APIKey.APIKey_GUACHANG_ITEM.ordinal()] = 64;
            } catch (NoSuchFieldError e61) {
            }
            try {
                iArr[APIKey.APIKey_GetAPK.ordinal()] = 95;
            } catch (NoSuchFieldError e62) {
            }
            try {
                iArr[APIKey.APIKey_GetAlbums.ordinal()] = 139;
            } catch (NoSuchFieldError e63) {
            }
            try {
                iArr[APIKey.APIKey_GetAllOrders.ordinal()] = 200;
            } catch (NoSuchFieldError e64) {
            }
            try {
                iArr[APIKey.APIKey_GetConsumeList.ordinal()] = 202;
            } catch (NoSuchFieldError e65) {
            }
            try {
                iArr[APIKey.APIKey_GetDialogUserInfo.ordinal()] = 7;
            } catch (NoSuchFieldError e66) {
            }
            try {
                iArr[APIKey.APIKey_GetFanChangURL.ordinal()] = 133;
            } catch (NoSuchFieldError e67) {
            }
            try {
                iArr[APIKey.APIKey_GetFixedRoomRank.ordinal()] = 82;
            } catch (NoSuchFieldError e68) {
            }
            try {
                iArr[APIKey.APIKey_GetLevelImgUrl.ordinal()] = 12;
            } catch (NoSuchFieldError e69) {
            }
            try {
                iArr[APIKey.APIKey_GetLiveDown.ordinal()] = 171;
            } catch (NoSuchFieldError e70) {
            }
            try {
                iArr[APIKey.APIKey_GetLiveUp.ordinal()] = 170;
            } catch (NoSuchFieldError e71) {
            }
            try {
                iArr[APIKey.APIKey_GetLyrcURL.ordinal()] = 126;
            } catch (NoSuchFieldError e72) {
            }
            try {
                iArr[APIKey.APIKey_GetMainCategory.ordinal()] = 65;
            } catch (NoSuchFieldError e73) {
            }
            try {
                iArr[APIKey.APIKey_GetMic_RoomList.ordinal()] = 226;
            } catch (NoSuchFieldError e74) {
            }
            try {
                iArr[APIKey.APIKey_GetNetLyrcURL.ordinal()] = 132;
            } catch (NoSuchFieldError e75) {
            }
            try {
                iArr[APIKey.APIKey_GetOrderInfo.ordinal()] = 197;
            } catch (NoSuchFieldError e76) {
            }
            try {
                iArr[APIKey.APIKey_GetOrderStatus.ordinal()] = 198;
            } catch (NoSuchFieldError e77) {
            }
            try {
                iArr[APIKey.APIKey_GetPayEventNotice.ordinal()] = 199;
            } catch (NoSuchFieldError e78) {
            }
            try {
                iArr[APIKey.APIKey_GetRoomList.ordinal()] = 77;
            } catch (NoSuchFieldError e79) {
            }
            try {
                iArr[APIKey.APIKey_GetRoomSongScore.ordinal()] = 83;
            } catch (NoSuchFieldError e80) {
            }
            try {
                iArr[APIKey.APIKey_GetSingerList.ordinal()] = 131;
            } catch (NoSuchFieldError e81) {
            }
            try {
                iArr[APIKey.APIKey_GetSingerPicURL.ordinal()] = 130;
            } catch (NoSuchFieldError e82) {
            }
            try {
                iArr[APIKey.APIKey_GetSmsCode.ordinal()] = 174;
            } catch (NoSuchFieldError e83) {
            }
            try {
                iArr[APIKey.APIKey_GetSongPicURL.ordinal()] = 129;
            } catch (NoSuchFieldError e84) {
            }
            try {
                iArr[APIKey.APIKey_GetSongURL.ordinal()] = 128;
            } catch (NoSuchFieldError e85) {
            }
            try {
                iArr[APIKey.APIKey_GetTradePriceList.ordinal()] = 201;
            } catch (NoSuchFieldError e86) {
            }
            try {
                iArr[APIKey.APIKey_GetUpdateInfo.ordinal()] = 138;
            } catch (NoSuchFieldError e87) {
            }
            try {
                iArr[APIKey.APIKey_GetUserFace.ordinal()] = 11;
            } catch (NoSuchFieldError e88) {
            }
            try {
                iArr[APIKey.APIKey_GetUserInfo.ordinal()] = 5;
            } catch (NoSuchFieldError e89) {
            }
            try {
                iArr[APIKey.APIKey_GetUserInfoItem.ordinal()] = 6;
            } catch (NoSuchFieldError e90) {
            }
            try {
                iArr[APIKey.APIKey_GetUserList.ordinal()] = 44;
            } catch (NoSuchFieldError e91) {
            }
            try {
                iArr[APIKey.APIKey_GetValidCode.ordinal()] = 172;
            } catch (NoSuchFieldError e92) {
            }
            try {
                iArr[APIKey.APIKey_GetVipList.ordinal()] = 227;
            } catch (NoSuchFieldError e93) {
            }
            try {
                iArr[APIKey.APIKey_Get_Animation_Config.ordinal()] = 161;
            } catch (NoSuchFieldError e94) {
            }
            try {
                iArr[APIKey.APIKey_Get_Club_Info.ordinal()] = 216;
            } catch (NoSuchFieldError e95) {
            }
            try {
                iArr[APIKey.APIKey_Get_Config.ordinal()] = 158;
            } catch (NoSuchFieldError e96) {
            }
            try {
                iArr[APIKey.APIKey_Get_My_Club.ordinal()] = 214;
            } catch (NoSuchFieldError e97) {
            }
            try {
                iArr[APIKey.APIKey_Get_My_Club_Tuijian.ordinal()] = 215;
            } catch (NoSuchFieldError e98) {
            }
            try {
                iArr[APIKey.APIKey_Get_Start_Pic.ordinal()] = 162;
            } catch (NoSuchFieldError e99) {
            }
            try {
                iArr[APIKey.APIKey_Get_Super_Gift_Fans.ordinal()] = 206;
            } catch (NoSuchFieldError e100) {
            }
            try {
                iArr[APIKey.APIKey_Get_Third_Config.ordinal()] = 160;
            } catch (NoSuchFieldError e101) {
            }
            try {
                iArr[APIKey.APIKey_GiftList.ordinal()] = 196;
            } catch (NoSuchFieldError e102) {
            }
            try {
                iArr[APIKey.APIKey_HallOfflineMessage.ordinal()] = 92;
            } catch (NoSuchFieldError e103) {
            }
            try {
                iArr[APIKey.APIKey_Home_Page.ordinal()] = 159;
            } catch (NoSuchFieldError e104) {
            }
            try {
                iArr[APIKey.APIKey_HotHMWeiBoSelect.ordinal()] = 97;
            } catch (NoSuchFieldError e105) {
            }
            try {
                iArr[APIKey.APIKey_HotRoomList.ordinal()] = 78;
            } catch (NoSuchFieldError e106) {
            }
            try {
                iArr[APIKey.APIKey_HotWeiBoSelect.ordinal()] = 96;
            } catch (NoSuchFieldError e107) {
            }
            try {
                iArr[APIKey.APIKey_Hot_Clubs.ordinal()] = 222;
            } catch (NoSuchFieldError e108) {
            }
            try {
                iArr[APIKey.APIKey_Hot_Program.ordinal()] = 70;
            } catch (NoSuchFieldError e109) {
            }
            try {
                iArr[APIKey.APIKey_Hot_Program_Thumb.ordinal()] = 71;
            } catch (NoSuchFieldError e110) {
            }
            try {
                iArr[APIKey.APIKey_Hot_Room_Page_AD.ordinal()] = 69;
            } catch (NoSuchFieldError e111) {
            }
            try {
                iArr[APIKey.APIKey_Hot_Today_Selected.ordinal()] = 228;
            } catch (NoSuchFieldError e112) {
            }
            try {
                iArr[APIKey.APIKey_Kick_User.ordinal()] = 223;
            } catch (NoSuchFieldError e113) {
            }
            try {
                iArr[APIKey.APIKey_Level_Img.ordinal()] = 186;
            } catch (NoSuchFieldError e114) {
            }
            try {
                iArr[APIKey.APIKey_Level_RoomDetail.ordinal()] = 184;
            } catch (NoSuchFieldError e115) {
            }
            try {
                iArr[APIKey.APIKey_Level_UserDetail.ordinal()] = 185;
            } catch (NoSuchFieldError e116) {
            }
            try {
                iArr[APIKey.APIKey_LibCheck.ordinal()] = 94;
            } catch (NoSuchFieldError e117) {
            }
            try {
                iArr[APIKey.APIKey_ListPeopleHeChang.ordinal()] = 144;
            } catch (NoSuchFieldError e118) {
            }
            try {
                iArr[APIKey.APIKey_LiveServer.ordinal()] = 90;
            } catch (NoSuchFieldError e119) {
            }
            try {
                iArr[APIKey.APIKey_LiveServerHall.ordinal()] = 91;
            } catch (NoSuchFieldError e120) {
            }
            try {
                iArr[APIKey.APIKey_Login.ordinal()] = 38;
            } catch (NoSuchFieldError e121) {
            }
            try {
                iArr[APIKey.APIKey_Logout.ordinal()] = 39;
            } catch (NoSuchFieldError e122) {
            }
            try {
                iArr[APIKey.APIKey_LyrcPath.ordinal()] = 127;
            } catch (NoSuchFieldError e123) {
            }
            try {
                iArr[APIKey.APIKey_Machine_Config.ordinal()] = 157;
            } catch (NoSuchFieldError e124) {
            }
            try {
                iArr[APIKey.APIKey_Modify_MyFace.ordinal()] = 42;
            } catch (NoSuchFieldError e125) {
            }
            try {
                iArr[APIKey.APIKey_Modify_PassWord.ordinal()] = 41;
            } catch (NoSuchFieldError e126) {
            }
            try {
                iArr[APIKey.APIKey_MyGiftByUser.ordinal()] = 194;
            } catch (NoSuchFieldError e127) {
            }
            try {
                iArr[APIKey.APIKey_MyGiftGroup.ordinal()] = 193;
            } catch (NoSuchFieldError e128) {
            }
            try {
                iArr[APIKey.APIKey_NEW_GUANGCHANG.ordinal()] = 62;
            } catch (NoSuchFieldError e129) {
            }
            try {
                iArr[APIKey.APIKey_Quit_Club.ordinal()] = 220;
            } catch (NoSuchFieldError e130) {
            }
            try {
                iArr[APIKey.APIKey_ReGetSmsCode.ordinal()] = 176;
            } catch (NoSuchFieldError e131) {
            }
            try {
                iArr[APIKey.APIKey_RecentLike.ordinal()] = 143;
            } catch (NoSuchFieldError e132) {
            }
            try {
                iArr[APIKey.APIKey_Register.ordinal()] = 45;
            } catch (NoSuchFieldError e133) {
            }
            try {
                iArr[APIKey.APIKey_Report.ordinal()] = 166;
            } catch (NoSuchFieldError e134) {
            }
            try {
                iArr[APIKey.APIKey_Report_Banzou_Lowquality.ordinal()] = 169;
            } catch (NoSuchFieldError e135) {
            }
            try {
                iArr[APIKey.APIKey_Report_Lyrc_Error.ordinal()] = 167;
            } catch (NoSuchFieldError e136) {
            }
            try {
                iArr[APIKey.APIKey_Report_Lyrc_Unalign.ordinal()] = 168;
            } catch (NoSuchFieldError e137) {
            }
            try {
                iArr[APIKey.APIKey_Rondom_Room_Info.ordinal()] = 85;
            } catch (NoSuchFieldError e138) {
            }
            try {
                iArr[APIKey.APIKey_RoomBuyVip.ordinal()] = 190;
            } catch (NoSuchFieldError e139) {
            }
            try {
                iArr[APIKey.APIKey_RoomGiftSpendTop.ordinal()] = 189;
            } catch (NoSuchFieldError e140) {
            }
            try {
                iArr[APIKey.APIKey_RoomHanHua.ordinal()] = 191;
            } catch (NoSuchFieldError e141) {
            }
            try {
                iArr[APIKey.APIKey_RoomParam.ordinal()] = 93;
            } catch (NoSuchFieldError e142) {
            }
            try {
                iArr[APIKey.APIKey_RoomRank.ordinal()] = 81;
            } catch (NoSuchFieldError e143) {
            }
            try {
                iArr[APIKey.APIKey_RoomVipList.ordinal()] = 188;
            } catch (NoSuchFieldError e144) {
            }
            try {
                iArr[APIKey.APIKey_Room_Create.ordinal()] = 87;
            } catch (NoSuchFieldError e145) {
            }
            try {
                iArr[APIKey.APIKey_Room_Edit.ordinal()] = 89;
            } catch (NoSuchFieldError e146) {
            }
            try {
                iArr[APIKey.APIKey_Room_GetMices.ordinal()] = 181;
            } catch (NoSuchFieldError e147) {
            }
            try {
                iArr[APIKey.APIKey_Room_GetMicesWhiteList.ordinal()] = 182;
            } catch (NoSuchFieldError e148) {
            }
            try {
                iArr[APIKey.APIKey_Room_GetUsers.ordinal()] = 180;
            } catch (NoSuchFieldError e149) {
            }
            try {
                iArr[APIKey.APIKey_Room_Info.ordinal()] = 84;
            } catch (NoSuchFieldError e150) {
            }
            try {
                iArr[APIKey.APIKey_Room_Info_Item.ordinal()] = 86;
            } catch (NoSuchFieldError e151) {
            }
            try {
                iArr[APIKey.APIKey_Room_Multi_GetMices.ordinal()] = 183;
            } catch (NoSuchFieldError e152) {
            }
            try {
                iArr[APIKey.APIKey_SHARE_LOG.ordinal()] = 153;
            } catch (NoSuchFieldError e153) {
            }
            try {
                iArr[APIKey.APIKey_SHARE_SUCCESS_LOG.ordinal()] = 154;
            } catch (NoSuchFieldError e154) {
            }
            try {
                iArr[APIKey.APIKey_SMS_ADD.ordinal()] = 101;
            } catch (NoSuchFieldError e155) {
            }
            try {
                iArr[APIKey.APIKey_SMS_ALL.ordinal()] = 102;
            } catch (NoSuchFieldError e156) {
            }
            try {
                iArr[APIKey.APIKey_SMS_DEL_LIST.ordinal()] = 105;
            } catch (NoSuchFieldError e157) {
            }
            try {
                iArr[APIKey.APIKey_SMS_DEL_ONE.ordinal()] = 104;
            } catch (NoSuchFieldError e158) {
            }
            try {
                iArr[APIKey.APIKey_SMS_GET_NEW.ordinal()] = 106;
            } catch (NoSuchFieldError e159) {
            }
            try {
                iArr[APIKey.APIKey_SMS_PEOPLE.ordinal()] = 103;
            } catch (NoSuchFieldError e160) {
            }
            try {
                iArr[APIKey.APIKey_SearchRoom.ordinal()] = 79;
            } catch (NoSuchFieldError e161) {
            }
            try {
                iArr[APIKey.APIKey_SearchUser.ordinal()] = 4;
            } catch (NoSuchFieldError e162) {
            }
            try {
                iArr[APIKey.APIKey_Search_Club.ordinal()] = 221;
            } catch (NoSuchFieldError e163) {
            }
            try {
                iArr[APIKey.APIKey_Search_From_QQ_Weibo.ordinal()] = 165;
            } catch (NoSuchFieldError e164) {
            }
            try {
                iArr[APIKey.APIKey_Search_From_Sina.ordinal()] = 164;
            } catch (NoSuchFieldError e165) {
            }
            try {
                iArr[APIKey.APIKey_Search_From_Third.ordinal()] = 163;
            } catch (NoSuchFieldError e166) {
            }
            try {
                iArr[APIKey.APIKey_SetAuth.ordinal()] = 46;
            } catch (NoSuchFieldError e167) {
            }
            try {
                iArr[APIKey.APIKey_Song_AddImage.ordinal()] = 99;
            } catch (NoSuchFieldError e168) {
            }
            try {
                iArr[APIKey.APIKey_Song_Albums.ordinal()] = 98;
            } catch (NoSuchFieldError e169) {
            }
            try {
                iArr[APIKey.APIKey_Song_DelImage.ordinal()] = 100;
            } catch (NoSuchFieldError e170) {
            }
            try {
                iArr[APIKey.APIKey_Song_Download.ordinal()] = 123;
            } catch (NoSuchFieldError e171) {
            }
            try {
                iArr[APIKey.APIKey_Song_Info.ordinal()] = 122;
            } catch (NoSuchFieldError e172) {
            }
            try {
                iArr[APIKey.APIKey_Song_Upload.ordinal()] = 124;
            } catch (NoSuchFieldError e173) {
            }
            try {
                iArr[APIKey.APIKey_Song_Upload_Slice.ordinal()] = 125;
            } catch (NoSuchFieldError e174) {
            }
            try {
                iArr[APIKey.APIKey_SystemUserList_Short.ordinal()] = 35;
            } catch (NoSuchFieldError e175) {
            }
            try {
                iArr[APIKey.APIKey_UNBindSNS.ordinal()] = 142;
            } catch (NoSuchFieldError e176) {
            }
            try {
                iArr[APIKey.APIKey_USER_DISABLE_RECEIVE_SUBSCRIPTION.ordinal()] = 233;
            } catch (NoSuchFieldError e177) {
            }
            try {
                iArr[APIKey.APIKey_USER_ENABLE_RECEIVE_SUBSCRIPTION.ordinal()] = 232;
            } catch (NoSuchFieldError e178) {
            }
            try {
                iArr[APIKey.APIKey_USER_FRIENDS.ordinal()] = 107;
            } catch (NoSuchFieldError e179) {
            }
            try {
                iArr[APIKey.APIKey_USER_SUBSCRIBE.ordinal()] = 229;
            } catch (NoSuchFieldError e180) {
            }
            try {
                iArr[APIKey.APIKey_USER_SUBSCRIPTION.ordinal()] = 234;
            } catch (NoSuchFieldError e181) {
            }
            try {
                iArr[APIKey.APIKey_USER_THIRDCHANGE.ordinal()] = 237;
            } catch (NoSuchFieldError e182) {
            }
            try {
                iArr[APIKey.APIKey_USER_TUISONG_SETTING.ordinal()] = 236;
            } catch (NoSuchFieldError e183) {
            }
            try {
                iArr[APIKey.APIKey_USER_UNSUBSCRIBE.ordinal()] = 230;
            } catch (NoSuchFieldError e184) {
            }
            try {
                iArr[APIKey.APIKey_USER_UPDATE_GETUI_ID.ordinal()] = 231;
            } catch (NoSuchFieldError e185) {
            }
            try {
                iArr[APIKey.APIKey_USE_DEAL_WITH.ordinal()] = 187;
            } catch (NoSuchFieldError e186) {
            }
            try {
                iArr[APIKey.APIKey_UserBlack_Add.ordinal()] = 17;
            } catch (NoSuchFieldError e187) {
            }
            try {
                iArr[APIKey.APIKey_UserBlack_Check.ordinal()] = 19;
            } catch (NoSuchFieldError e188) {
            }
            try {
                iArr[APIKey.APIKey_UserBlack_Del.ordinal()] = 18;
            } catch (NoSuchFieldError e189) {
            }
            try {
                iArr[APIKey.APIKey_UserBlack_List.ordinal()] = 23;
            } catch (NoSuchFieldError e190) {
            }
            try {
                iArr[APIKey.APIKey_UserBlack_ShortList.ordinal()] = 24;
            } catch (NoSuchFieldError e191) {
            }
            try {
                iArr[APIKey.APIKey_UserFans.ordinal()] = 8;
            } catch (NoSuchFieldError e192) {
            }
            try {
                iArr[APIKey.APIKey_UserFollowList_Short.ordinal()] = 34;
            } catch (NoSuchFieldError e193) {
            }
            try {
                iArr[APIKey.APIKey_UserFollowers.ordinal()] = 9;
            } catch (NoSuchFieldError e194) {
            }
            try {
                iArr[APIKey.APIKey_UserFriends.ordinal()] = 10;
            } catch (NoSuchFieldError e195) {
            }
            try {
                iArr[APIKey.APIKey_UserGift.ordinal()] = 192;
            } catch (NoSuchFieldError e196) {
            }
            try {
                iArr[APIKey.APIKey_UserList_FavoriteMe.ordinal()] = 33;
            } catch (NoSuchFieldError e197) {
            }
            try {
                iArr[APIKey.APIKey_UserModify_Profile.ordinal()] = 40;
            } catch (NoSuchFieldError e198) {
            }
            try {
                iArr[APIKey.APIKey_UserRank.ordinal()] = 148;
            } catch (NoSuchFieldError e199) {
            }
            try {
                iArr[APIKey.APIKey_UserRelationship.ordinal()] = 13;
            } catch (NoSuchFieldError e200) {
            }
            try {
                iArr[APIKey.APIKey_UserRelationship_GetRelation.ordinal()] = 14;
            } catch (NoSuchFieldError e201) {
            }
            try {
                iArr[APIKey.APIKey_UserRelationship_SetFollow.ordinal()] = 15;
            } catch (NoSuchFieldError e202) {
            }
            try {
                iArr[APIKey.APIKey_UserRelationship_SetUnfollow.ordinal()] = 16;
            } catch (NoSuchFieldError e203) {
            }
            try {
                iArr[APIKey.APIKey_UserTop.ordinal()] = 47;
            } catch (NoSuchFieldError e204) {
            }
            try {
                iArr[APIKey.APIKey_UserWhite_Add.ordinal()] = 20;
            } catch (NoSuchFieldError e205) {
            }
            try {
                iArr[APIKey.APIKey_UserWhite_Check.ordinal()] = 22;
            } catch (NoSuchFieldError e206) {
            }
            try {
                iArr[APIKey.APIKey_UserWhite_Del.ordinal()] = 21;
            } catch (NoSuchFieldError e207) {
            }
            try {
                iArr[APIKey.APIKey_ValidDate.ordinal()] = 173;
            } catch (NoSuchFieldError e208) {
            }
            try {
                iArr[APIKey.APIKey_VerifySmsCode.ordinal()] = 175;
            } catch (NoSuchFieldError e209) {
            }
            try {
                iArr[APIKey.APIKey_WeiBoRank.ordinal()] = 149;
            } catch (NoSuchFieldError e210) {
            }
            try {
                iArr[APIKey.APIKey_WeiboList_AtMe.ordinal()] = 28;
            } catch (NoSuchFieldError e211) {
            }
            try {
                iArr[APIKey.APIKey_WeiboList_CommentMe.ordinal()] = 27;
            } catch (NoSuchFieldError e212) {
            }
            try {
                iArr[APIKey.APIKey_WeiboList_Favorite.ordinal()] = 31;
            } catch (NoSuchFieldError e213) {
            }
            try {
                iArr[APIKey.APIKey_WeiboList_Favorite_Short.ordinal()] = 32;
            } catch (NoSuchFieldError e214) {
            }
            try {
                iArr[APIKey.APIKey_WeiboList_Forward.ordinal()] = 37;
            } catch (NoSuchFieldError e215) {
            }
            try {
                iArr[APIKey.APIKey_WeiboList_Friends.ordinal()] = 29;
            } catch (NoSuchFieldError e216) {
            }
            try {
                iArr[APIKey.APIKey_WeiboList_Friends_Original.ordinal()] = 30;
            } catch (NoSuchFieldError e217) {
            }
            try {
                iArr[APIKey.APIKey_WeiboList_HotRecordHistory.ordinal()] = 111;
            } catch (NoSuchFieldError e218) {
            }
            try {
                iArr[APIKey.APIKey_WeiboList_HotRecordToday.ordinal()] = 110;
            } catch (NoSuchFieldError e219) {
            }
            try {
                iArr[APIKey.APIKey_WeiboList_MyComment.ordinal()] = 26;
            } catch (NoSuchFieldError e220) {
            }
            try {
                iArr[APIKey.APIKey_WeiboList_Post.ordinal()] = 25;
            } catch (NoSuchFieldError e221) {
            }
            try {
                iArr[APIKey.APIKey_WeiboList_Public.ordinal()] = 108;
            } catch (NoSuchFieldError e222) {
            }
            try {
                iArr[APIKey.APIKey_WeiboList_Reply.ordinal()] = 36;
            } catch (NoSuchFieldError e223) {
            }
            try {
                iArr[APIKey.APIKey_WeiboList_Top.ordinal()] = 109;
            } catch (NoSuchFieldError e224) {
            }
            try {
                iArr[APIKey.APIKey_Weibo_Checkfavorite.ordinal()] = 58;
            } catch (NoSuchFieldError e225) {
            }
            try {
                iArr[APIKey.APIKey_Weibo_Favorite.ordinal()] = 56;
            } catch (NoSuchFieldError e226) {
            }
            try {
                iArr[APIKey.APIKey_Weibo_Forward.ordinal()] = 54;
            } catch (NoSuchFieldError e227) {
            }
            try {
                iArr[APIKey.APIKey_Weibo_Like.ordinal()] = 59;
            } catch (NoSuchFieldError e228) {
            }
            try {
                iArr[APIKey.APIKey_Weibo_Listen.ordinal()] = 61;
            } catch (NoSuchFieldError e229) {
            }
            try {
                iArr[APIKey.APIKey_Weibo_Post.ordinal()] = 48;
            } catch (NoSuchFieldError e230) {
            }
            try {
                iArr[APIKey.APIKey_Weibo_Refresh.ordinal()] = 50;
            } catch (NoSuchFieldError e231) {
            }
            try {
                iArr[APIKey.APIKey_Weibo_Reply.ordinal()] = 53;
            } catch (NoSuchFieldError e232) {
            }
            try {
                iArr[APIKey.APIKey_Weibo_ReplyForward.ordinal()] = 55;
            } catch (NoSuchFieldError e233) {
            }
            try {
                iArr[APIKey.APIKey_Weibo_Show.ordinal()] = 51;
            } catch (NoSuchFieldError e234) {
            }
            try {
                iArr[APIKey.APIKey_Weibo_Show_Item.ordinal()] = 52;
            } catch (NoSuchFieldError e235) {
            }
            try {
                iArr[APIKey.APIKey_Weibo_Unfavorite.ordinal()] = 57;
            } catch (NoSuchFieldError e236) {
            }
            try {
                iArr[APIKey.APIkey_ListHeChang.ordinal()] = 140;
            } catch (NoSuchFieldError e237) {
            }
            try {
                iArr[APIKey.APKKey_Category_Match.ordinal()] = 73;
            } catch (NoSuchFieldError e238) {
            }
            q = iArr;
        }
        return iArr;
    }

    public x(b bVar, int i2) {
        this.h = bVar;
        this.e = i2;
    }

    public void a() {
        this.i = false;
        this.c = 1;
        b();
    }

    public void b() {
        f();
        KURL kurl = new KURL();
        HashMap hashMap = new HashMap();
        if (this.i && this.f != null) {
            hashMap.put("id_max", this.f);
        } else if (this.g != null) {
            hashMap.put("id_min", this.g);
        }
        hashMap.put(WBPageConstants.ParamKey.COUNT, String.valueOf(this.e));
        switch (l()[this.h.ordinal()]) {
            case 1:
                kurl.baseURL = s.a(APIKey.APIKey_HotWeiBoSelect);
                hashMap.put(WBPageConstants.ParamKey.PAGE, String.valueOf(this.c));
                kurl.getParameter.putAll(hashMap);
                a(kurl, this.ar, APIKey.APIKey_HotWeiBoSelect);
                return;
            case 2:
                kurl.baseURL = s.a(APIKey.APIKey_HotHMWeiBoSelect);
                hashMap.put(WBPageConstants.ParamKey.PAGE, String.valueOf(this.c));
                kurl.getParameter.putAll(hashMap);
                a(kurl, this.ar, APIKey.APIKey_HotHMWeiBoSelect);
                return;
            default:
                return;
        }
    }

    public void c() {
        this.i = true;
        b();
    }

    public void d() {
        this.i = false;
        this.c = 1;
        b();
    }

    public boolean e() {
        if (this.f639a <= this.e || this.b >= this.c) {
            return false;
        }
        return true;
    }

    public void a(w wVar) {
        this.o.add(wVar);
    }

    public void f() {
        this.o.clear();
    }

    public List<w> g() {
        return this.o;
    }

    public w a(int i2) {
        return this.o.get(i2);
    }

    public w h() {
        return this.o.get(this.o.size() - 1);
    }

    public int k() {
        return this.o.size();
    }

    public void a(JSONObject jSONObject) {
        JSONObject optJSONObject;
        if (jSONObject != null && !b(jSONObject) && (optJSONObject = jSONObject.optJSONObject(SocketMessage.MSG_RESULE_KEY)) != null && optJSONObject.length() != 0) {
            JSONArray optJSONArray = optJSONObject.optJSONArray("topics");
            if ((optJSONArray != null && this.aq != APIKey.APIKey_WeiboList_Reply) || ((optJSONArray = optJSONObject.optJSONArray("comments")) != null && optJSONArray.length() != 0)) {
                JSONArray jSONArray = optJSONArray;
                if (this.aq == APIKey.APIKey_WeiboList_HotRecordHistory || this.aq == APIKey.APIKey_WeiboList_HotRecordToday || this.aq == APIKey.APIKey_WeiBoRank || this.aq == APIKey.APIKey_WeiboList_Favorite || this.aq == APIKey.APIKey_Hot_Today_Selected || this.aq == APIKey.APIKey_HotWeiBoSelect || this.aq == APIKey.APIKey_HotHMWeiBoSelect) {
                    this.c = optJSONObject.optInt("page_next");
                    this.b = optJSONObject.optInt(WBPageConstants.ParamKey.PAGE);
                    this.d = optJSONObject.optInt("page_count");
                }
                this.e = optJSONObject.optInt("limit", this.e);
                if (this.i) {
                    this.f639a = optJSONObject.optInt("total");
                } else if (this.f639a > 0) {
                    this.f639a = this.e + 1;
                } else {
                    this.f639a = optJSONObject.optInt("total");
                }
                JSONArray optJSONArray2 = optJSONObject.optJSONArray("extension");
                if (jSONArray != null) {
                    try {
                        if (jSONArray.length() > 0) {
                            for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                                if (!(jSONArray.get(i2) == null || jSONArray.get(i2) == JSONObject.NULL)) {
                                    w wVar = new w();
                                    if (this.aq == APIKey.APIKey_WeiboList_Reply) {
                                        JSONObject jSONObject2 = (JSONObject) jSONArray.get(i2);
                                        if (jSONObject2.opt(SocketMessage.MSG_ERROR_KEY) != null) {
                                            wVar.b(optJSONObject.optInt("code"));
                                        } else if (jSONObject2 != null) {
                                            this.n.put(jSONObject2.optString("tid"), jSONObject2.optString("nickname"));
                                        }
                                    }
                                    if (this.aq != APIKey.APIKey_WeiboList_Favorite_Short || !(jSONArray.get(i2) instanceof String)) {
                                        wVar.aq = this.aq;
                                        wVar.a(jSONArray.optJSONObject(i2));
                                        if (optJSONArray2 != null) {
                                            if (wVar.C == null) {
                                                wVar.J = optJSONArray2.getJSONObject(i2).optString("liked_count", "0");
                                                wVar.I = optJSONArray2.getJSONObject(i2).optString("listened_count", "0");
                                            } else {
                                                wVar.C.J = optJSONArray2.getJSONObject(i2).optString("liked_count", "0");
                                                wVar.C.I = optJSONArray2.getJSONObject(i2).optString("listened_count", "0");
                                            }
                                        }
                                        a(wVar);
                                    } else {
                                        wVar.f635a = (String) jSONArray.get(i2);
                                        a(wVar);
                                    }
                                }
                            }
                        }
                    } catch (JSONException e2) {
                    }
                }
                switch (m()[this.aq.ordinal()]) {
                    case D.QHINTERSTITIALAD_setAdEventListener /*25*/:
                    case 27:
                    case D.QHNATIVEAD_onAdClicked /*28*/:
                    case D.QHNATIVEADLISTENER_onNativeAdLoadSucceeded /*29*/:
                    case D.QHNATIVEADLISTENER_onNativeAdLoadFailed /*30*/:
                    case D.QHNATIVEADLOADER_setAdAttributes /*36*/:
                    case D.QHNATIVEADLOADER_clearAdAttributes /*37*/:
                    case 108:
                    case 140:
                    case 144:
                    case 149:
                        if (k() > 0) {
                            if (this.f == null || this.i) {
                                try {
                                    this.f = String.valueOf(Integer.valueOf(h().f635a).intValue() - 1);
                                } catch (Exception e3) {
                                }
                            }
                            if (this.g == null || !this.i) {
                                try {
                                    this.g = String.valueOf(Integer.valueOf(a(0).f635a).intValue());
                                    return;
                                } catch (Exception e4) {
                                    return;
                                }
                            } else {
                                return;
                            }
                        } else {
                            return;
                        }
                    default:
                        return;
                }
            }
        }
    }
}
