package com.pocketmusic.kshare.requestobjs;

import cn.banshenggua.aichang.api.APIKey;
import cn.banshenggua.aichang.api.KURL;
import com.tencent.open.wpa.WPA;
import java.util.HashMap;

/* compiled from: Level */
public class h {
    private static /* synthetic */ int[] d;
    private static /* synthetic */ int[] e;

    /* renamed from: a  reason: collision with root package name */
    public a f619a = a.SIM;
    public b b = b.Level;
    public int c = 0;

    /* compiled from: Level */
    public enum a {
        BIG,
        MID,
        SIM
    }

    /* compiled from: Level */
    public enum b {
        Level,
        Group
    }

    static /* synthetic */ int[] a() {
        int[] iArr = d;
        if (iArr == null) {
            iArr = new int[a.values().length];
            try {
                iArr[a.BIG.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[a.MID.ordinal()] = 2;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[a.SIM.ordinal()] = 3;
            } catch (NoSuchFieldError e4) {
            }
            d = iArr;
        }
        return iArr;
    }

    static /* synthetic */ int[] b() {
        int[] iArr = e;
        if (iArr == null) {
            iArr = new int[b.values().length];
            try {
                iArr[b.Group.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[b.Level.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            e = iArr;
        }
        return iArr;
    }

    public h(int i) {
        this.c = i;
    }

    public String a(a aVar) {
        this.f619a = aVar;
        HashMap hashMap = new HashMap();
        hashMap.put("lid", new StringBuilder().append(this.c).toString());
        switch (a()[this.f619a.ordinal()]) {
            case 1:
                hashMap.put("size", "b");
                break;
            case 2:
                hashMap.put("size", "m");
                break;
            case 3:
                hashMap.put("size", "s");
                break;
        }
        switch (b()[this.b.ordinal()]) {
            case 1:
                hashMap.put("type", "level");
                break;
            case 2:
                hashMap.put("type", WPA.CHAT_TYPE_GROUP);
                break;
        }
        return KURL.urlEncode(s.a(APIKey.APIKey_GetLevelImgUrl), hashMap, false);
    }

    public static String a(a aVar, int i) {
        return a(aVar, b.Level, i);
    }

    public static String a(a aVar, b bVar, int i) {
        h hVar = new h(i);
        hVar.b = bVar;
        return hVar.a(aVar);
    }
}
