package com.pocketmusic.kshare.requestobjs;

import cn.banshenggua.aichang.api.APIKey;
import cn.banshenggua.aichang.api.KURL;
import org.json.JSONObject;

/* compiled from: LibCheck */
public class i extends m {

    /* renamed from: a  reason: collision with root package name */
    public String f622a;
    public long b;
    public boolean c = false;
    public long d;

    public void a() {
        KURL kurl = new KURL();
        kurl.baseURL = s.a(APIKey.APIKey_LibCheck);
        a(kurl, this.ar, APIKey.APIKey_LibCheck);
    }

    public void a(JSONObject jSONObject) {
        boolean z = false;
        if (jSONObject != null) {
            this.f622a = jSONObject.optString("file");
            this.b = jSONObject.optLong("size", 0);
            this.d = jSONObject.optLong("osize", 0);
            if (jSONObject.optInt("zip", 0) != 0) {
                z = true;
            }
            this.c = z;
        }
    }
}
