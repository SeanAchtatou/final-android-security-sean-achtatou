package com.pocketmusic.kshare.requestobjs;

import android.text.TextUtils;
import cn.banshenggua.aichang.room.message.User;
import cn.banshenggua.aichang.utils.CommonUtil;
import cn.banshenggua.aichang.utils.Constants;
import com.pocketmusic.kshare.requestobjs.h;
import com.sina.weibo.sdk.exception.WeiboAuthException;
import com.sina.weibo.sdk.register.mobile.SelectCountryActivity;
import java.io.Serializable;
import java.util.ArrayList;
import org.json.JSONObject;

/* compiled from: Gift */
public class e implements Serializable {
    public String A = "";
    public String B = "";
    public String C = "";
    public String D = "";
    public int E = 20;
    public String F = "";
    public double G = 0.0d;
    public int H = 0;
    public String I = "0";
    public String[] J = null;
    public boolean K = true;
    public ArrayList<C0014e> L = new ArrayList<>();
    public b M = b.ToOne;
    public d N = d.All;
    public c O = c.Normal;
    public a P = a.Normal;

    /* renamed from: a  reason: collision with root package name */
    public String f610a = "";
    public String b = "";
    public String c = "0";
    public String d = "";
    public String e = "";
    public String f = "";
    public String g = "";
    public String h = "";
    public String i = "";
    public int j = Constants.CLEARIMGED;
    public int k = 1;
    public String l = "gif";
    public String m = "";
    public String n = "";
    public String o = WeiboAuthException.DEFAULT_AUTH_ERROR_CODE;
    public String p = "";
    public C0014e q = null;
    public C0014e r = null;
    public int s = 100;
    public int t = 100;
    public int u = 0;
    public int v = -1;
    public String w = "";
    public String x = "";
    public String y = "";
    public String z = "";

    /* compiled from: Gift */
    public enum b {
        ToOne("0"),
        ToAll("1");
        
        private String c = "0";

        private b(String str) {
            this.c = str;
        }

        public static b a(String str) {
            for (b bVar : values()) {
                if (bVar.c.equalsIgnoreCase(str)) {
                    return bVar;
                }
            }
            return ToOne;
        }
    }

    /* compiled from: Gift */
    public enum d {
        All("0"),
        OnlyRoom("1"),
        OnlyPlayer("2");
        
        private String d = "0";

        private d(String str) {
            this.d = str;
        }

        public static d a(String str) {
            for (d dVar : values()) {
                if (dVar.d.equalsIgnoreCase(str)) {
                    return dVar;
                }
            }
            return All;
        }
    }

    /* compiled from: Gift */
    public enum c {
        Normal("normal"),
        SaQian("saqian"),
        HanHua("hanhua"),
        GuiBin("guibin");
        
        private String e = "normal";

        private c(String str) {
            this.e = str;
        }

        public static c a(String str) {
            if (TextUtils.isEmpty(str)) {
                str = "normal";
            }
            for (c cVar : values()) {
                if (cVar.e.equalsIgnoreCase(str)) {
                    return cVar;
                }
            }
            return Normal;
        }
    }

    /* compiled from: Gift */
    public enum a {
        Normal("normal"),
        Level("level"),
        VIP("vip");
        
        private String d = "normal";

        private a(String str) {
            this.d = str;
        }

        public static a a(String str) {
            if (TextUtils.isEmpty(str)) {
                str = "normal";
            }
            for (a aVar : values()) {
                if (aVar.d.equalsIgnoreCase(str)) {
                    return aVar;
                }
            }
            return Normal;
        }
    }

    /* renamed from: com.pocketmusic.kshare.requestobjs.e$e  reason: collision with other inner class name */
    /* compiled from: Gift */
    public static class C0014e {

        /* renamed from: a  reason: collision with root package name */
        public String f615a;
        public String b;
        public String c;
        public int d = 0;
        public String e;
        public String f = "";
        public String g;
        public int h = 0;
        public String i = "";
        public String j = "";
        public String k = "";
        public boolean l = false;
        public String m = null;

        public String toString() {
            return String.valueOf(String.valueOf(String.valueOf("") + "uid: " + this.f615a) + "; username: " + this.b) + "; nickname: " + this.c;
        }

        public String a() {
            if (TextUtils.isEmpty(this.g)) {
                return this.c;
            }
            return this.g;
        }

        public void a(String str) {
            this.g = str;
        }

        public String a(User.FACE_SIZE face_size) {
            if (TextUtils.isEmpty(this.e)) {
                this.e = User.getFace(this.f615a, face_size);
            }
            return this.e;
        }
    }

    public static C0014e a(User user) {
        C0014e eVar = new C0014e();
        if (user != null) {
            eVar.f615a = user.mUid;
            eVar.g = user.getFullName();
            eVar.c = user.mNickname;
            eVar.d = user.mGender;
            eVar.e = user.mFace;
            eVar.h = user.mLevel;
            eVar.k = user.mLevelImage;
            eVar.j = user.auth_info;
            eVar.i = user.authIcon;
            eVar.m = user.mVip;
        }
        return eVar;
    }

    public String toString() {
        return String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf("") + "gid: " + this.b) + "; price: " + this.c) + "; name: " + this.d) + "; icon: " + this.e) + "; count: " + this.g) + "; giftValue: " + this.p) + "; user: { " + this.q + "}";
    }

    public void a(JSONObject jSONObject) {
        boolean z2 = true;
        if (jSONObject != null) {
            this.f610a = jSONObject.optString("id", "0");
            this.b = jSONObject.optString("gid", "0");
            this.g = jSONObject.optString("giftcount", "0");
            this.c = jSONObject.optString("price", "0");
            this.d = jSONObject.optString(SelectCountryActivity.EXTRA_COUNTRY_NAME, "");
            this.e = jSONObject.optString("iconpath_b", "");
            this.f = jSONObject.optString("iconpath_s", "");
            this.p = jSONObject.optString("giftvalue", "0");
            this.i = jSONObject.optString("recvtime", "0");
            this.j = jSONObject.optInt("playtime", 1) * Constants.CLEARIMGED;
            this.k = jSONObject.optInt("repeats", 1);
            this.l = jSONObject.optString("filetype", "");
            this.m = jSONObject.optString("begintime", "0");
            this.n = jSONObject.optString(LogBuilder.KEY_END_TIME, "0");
            this.o = jSONObject.optString("stock", WeiboAuthException.DEFAULT_AUTH_ERROR_CODE);
            this.t = CommonUtil.dipToPixel(jSONObject.optInt("height", 100));
            this.s = CommonUtil.dipToPixel(jSONObject.optInt("width", 100));
            this.u = jSONObject.optInt("ifprecious", 0);
            this.w = jSONObject.optString("sound", "");
            this.x = jSONObject.optString("soundpath", "");
            this.N = d.a(jSONObject.optString("viewtype", ""));
            this.O = c.a(jSONObject.optString("type", ""));
            this.M = b.a(jSONObject.optString("totype", "0"));
            this.P = a.a(jSONObject.optString("privilege", ""));
            this.C = jSONObject.optString("abbr", "");
            this.D = jSONObject.optString("title", "");
            this.E = jSONObject.optInt("wordcount", 50);
            this.G = jSONObject.optDouble("exchange", 0.0d);
            this.H = jSONObject.optInt("freecount", 0);
            this.I = jSONObject.optString("global", "0");
            if (jSONObject.has("uid")) {
                this.q = new C0014e();
                this.q.f615a = jSONObject.optString("uid", "0");
                this.q.b = jSONObject.optString("username", "");
                this.q.c = jSONObject.optString("nickname", "");
                this.q.d = jSONObject.optInt("gender");
                this.q.e = jSONObject.optString("face", "");
                this.q.h = jSONObject.optInt("level", 0);
                this.q.i = jSONObject.optString("icon", "");
                this.q.a(jSONObject.optString("full", this.q.c));
                if (this.q.h > 0) {
                    this.q.k = h.a(h.a.SIM, this.q.h);
                }
                this.q.j = jSONObject.optString("auth", "");
                C0014e eVar = this.q;
                if (!jSONObject.optString("isonline", "0").equalsIgnoreCase("1")) {
                    z2 = false;
                }
                eVar.l = z2;
                this.q.m = jSONObject.optString("vip", null);
            }
        }
    }

    public e a() {
        e eVar = new e();
        eVar.f610a = this.f610a;
        eVar.b = this.b;
        eVar.c = this.c;
        eVar.d = this.d;
        eVar.e = this.e;
        eVar.f = this.f;
        eVar.g = this.g;
        eVar.h = this.h;
        eVar.i = this.i;
        eVar.j = this.j;
        eVar.k = this.k;
        eVar.l = this.l;
        eVar.m = this.m;
        eVar.n = this.n;
        eVar.o = this.o;
        eVar.p = this.p;
        eVar.q = this.q;
        eVar.r = this.r;
        eVar.s = this.s;
        eVar.t = this.t;
        eVar.u = this.u;
        eVar.v = this.v;
        eVar.w = this.w;
        eVar.x = this.x;
        eVar.y = this.y;
        eVar.z = this.z;
        eVar.A = this.A;
        eVar.O = this.O;
        eVar.M = this.M;
        eVar.P = this.P;
        eVar.N = this.N;
        eVar.E = this.E;
        eVar.C = this.C;
        eVar.D = this.D;
        eVar.I = this.I;
        eVar.K = this.K;
        return eVar;
    }
}
