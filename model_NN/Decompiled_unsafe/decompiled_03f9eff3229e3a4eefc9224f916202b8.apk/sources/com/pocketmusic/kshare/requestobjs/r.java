package com.pocketmusic.kshare.requestobjs;

import android.os.Parcel;
import android.os.Parcelable;

/* compiled from: Song */
class r implements Parcelable.Creator<Song> {
    r() {
    }

    /* renamed from: a */
    public Song createFromParcel(Parcel parcel) {
        Song song = new Song();
        song.b = parcel.readString();
        song.c = parcel.readString();
        song.d = parcel.readString();
        song.f = parcel.readString();
        song.g = parcel.readString();
        song.h = parcel.readString();
        song.i = parcel.readString();
        song.j = parcel.readString();
        song.k = parcel.readString();
        song.l = parcel.readString();
        song.n = parcel.readString();
        song.o = parcel.readString();
        song.u = parcel.readString();
        song.v = parcel.readString();
        song.w = parcel.readString();
        if (parcel.readInt() == 0) {
            song.m = false;
        } else {
            song.m = true;
        }
        song.q = parcel.readString();
        song.r = parcel.readString();
        if (parcel.readInt() == 0) {
            song.p = false;
        } else {
            song.p = true;
        }
        int readInt = parcel.readInt();
        int readInt2 = parcel.readInt();
        int readInt3 = parcel.readInt();
        int readInt4 = parcel.readInt();
        int readInt5 = parcel.readInt();
        if (readInt == 0) {
            song.B = false;
        } else {
            song.B = true;
        }
        if (readInt2 == 0) {
            song.C = false;
        } else {
            song.C = true;
        }
        if (readInt3 == 0) {
            song.D = false;
        } else {
            song.D = true;
        }
        if (readInt4 == 0) {
            song.E = false;
        } else {
            song.E = true;
        }
        if (readInt5 == 0) {
            song.I = false;
        } else {
            song.I = true;
        }
        song.z = parcel.readString();
        if (parcel.readInt() == 0) {
            song.x = false;
        } else {
            song.x = true;
        }
        song.J = parcel.readLong();
        int readInt6 = parcel.readInt();
        int readInt7 = parcel.readInt();
        int readInt8 = parcel.readInt();
        int readInt9 = parcel.readInt();
        if (readInt6 == 0) {
            song.F = false;
        } else {
            song.F = true;
        }
        if (readInt7 == 0) {
            song.G = false;
        } else {
            song.G = true;
        }
        if (readInt8 == 0) {
            song.H = false;
        } else {
            song.H = true;
        }
        if (readInt9 == 0) {
            song.N = false;
        } else {
            song.N = true;
        }
        song.O = parcel.readString();
        song.s = parcel.readString();
        song.R = parcel.readLong();
        return song;
    }

    /* renamed from: a */
    public Song[] newArray(int i) {
        return new Song[i];
    }
}
