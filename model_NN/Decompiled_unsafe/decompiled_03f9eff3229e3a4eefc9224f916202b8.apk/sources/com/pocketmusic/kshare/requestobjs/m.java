package com.pocketmusic.kshare.requestobjs;

import android.text.TextUtils;
import cn.banshenggua.aichang.api.APIKey;
import cn.banshenggua.aichang.api.KURL;
import cn.banshenggua.aichang.app.KShareApplication;
import cn.banshenggua.aichang.room.message.SocketMessage;
import cn.banshenggua.aichang.sdk.ACException;
import cn.banshenggua.aichang.utils.ULog;
import com.android.volley.toolbox.n;
import org.json.JSONObject;

/* compiled from: RequestObj */
public class m {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public String f624a = getClass().getSimpleName();
    public int ao = -1000;
    public d ap = null;
    public APIKey aq = null;
    public q ar = null;
    private q b = new n(this);

    public int i() {
        return this.ao;
    }

    public void b(int i) {
        this.ao = i;
    }

    public d j() {
        return this.ap;
    }

    public void a(d dVar) {
        this.ap = dVar;
    }

    public boolean b(JSONObject jSONObject) {
        b(-1000);
        a((d) null);
        if (jSONObject == null || !jSONObject.has(SocketMessage.MSG_ERROR_KEY) || !jSONObject.has("code")) {
            return false;
        }
        b(jSONObject.optInt("code", -1000));
        String optString = jSONObject.optString("errmsg", "");
        if (!TextUtils.isEmpty(optString)) {
            a(new d(i(), optString));
        }
        return true;
    }

    public void a(q qVar) {
        this.ar = qVar;
    }

    public void a(KURL kurl, q qVar, APIKey aPIKey) {
        if (kurl != null) {
            if (qVar == null) {
                qVar = this.b;
            }
            n nVar = new n(kurl.urlEncode(), null, qVar, qVar);
            if (aPIKey == null) {
                aPIKey = APIKey.APIKey_Default;
            }
            this.aq = aPIKey;
            ULog.d(this.f624a, "apikey: " + aPIKey);
            ULog.d(this.f624a, "url: " + kurl);
            nVar.a(false);
            try {
                KShareApplication.getInstance().addToRequestQueue(nVar, aPIKey.getKey());
            } catch (ACException e) {
                e.printStackTrace();
            }
        }
    }
}
