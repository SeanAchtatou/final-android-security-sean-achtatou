package com.pocketmusic.kshare.requestobjs;

import android.text.TextUtils;
import cn.banshenggua.aichang.room.message.UserList;
import com.pocketmusic.kshare.requestobjs.g;
import com.sina.weibo.sdk.exception.WeiboAuthException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/* compiled from: Account */
public class a extends m implements Serializable, Cloneable {

    /* renamed from: a  reason: collision with root package name */
    public static String f604a = "weixin";
    private static /* synthetic */ int[] at;
    public String A = "0";
    public String B = "";
    public String C = "";
    public String D = "";
    public String E = "";
    public k F = null;
    public k G = null;
    public k H = null;
    public k I = null;
    public k J = null;
    public String K = null;
    public String L = null;
    public String M = null;
    public int N = 0;
    public int O = 0;
    public String P = "";
    public String Q = "";
    public String R = "";
    public String S = "";
    public UserList T = null;
    public o U;
    public b V = null;
    public int W = 0;
    public String X;
    public String Y = "";
    public int Z = 0;
    public boolean aa = false;
    public String ab = "";
    public String ac = "";
    public g.a ad = null;
    public String ae = "";
    public String af = "";
    public String ag = null;
    public C0012a ah = C0012a.Default;
    public String ai = "";
    public String aj = "";
    public String ak = "";
    public String al = "";
    public String am = "";
    public boolean an = false;
    private String as = "";
    public String b = WeiboAuthException.DEFAULT_AUTH_ERROR_CODE;
    public String c = "";
    public String d = "";
    public String e = "";
    public Integer f = 0;
    public String g = null;
    public String h = null;
    public String i = "";
    public String j = "";
    public String k = "";
    public String l = "";
    public String m = "";
    public String n = "";
    public String o = "";
    public String p = "0";
    public String q = "0";
    public String r = "0";
    public String s = "0";
    public String t = "";
    public List<v> u = new ArrayList();
    public String v = "";
    public String w = "0";
    public String x = "0";
    public String y = "0";
    public String z = "";

    /* renamed from: com.pocketmusic.kshare.requestobjs.a$a  reason: collision with other inner class name */
    /* compiled from: Account */
    public enum C0012a {
        Default,
        InfoItem
    }

    static /* synthetic */ int[] e() {
        int[] iArr = at;
        if (iArr == null) {
            iArr = new int[C0012a.values().length];
            try {
                iArr[C0012a.Default.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[C0012a.InfoItem.ordinal()] = 2;
            } catch (NoSuchFieldError e3) {
            }
            at = iArr;
        }
        return iArr;
    }

    public String a() {
        if (TextUtils.isEmpty(this.as)) {
            return this.d;
        }
        return this.as;
    }

    public void a(String str) {
        this.as = str;
    }

    public boolean equals(Object obj) {
        a aVar = (a) obj;
        if (!aVar.c.equals(this.c) || !aVar.i.equals(this.i)) {
            return false;
        }
        return true;
    }

    public a() {
    }

    public a(String str) {
        this.c = str;
    }

    public void b() {
        this.u.clear();
    }

    public void c() {
        if (this.ah != null) {
            switch (e()[this.ah.ordinal()]) {
                case 2:
                default:
                    return;
            }
        }
    }

    public Object clone() throws CloneNotSupportedException {
        return (a) super.clone();
    }

    public boolean d() {
        return WeiboAuthException.DEFAULT_AUTH_ERROR_CODE.equals(this.b);
    }

    public String toString() {
        return "usename:" + this.c + " nickname:" + this.d + " uid:" + this.b + " password:" + this.i + " authString: " + this.Q;
    }
}
