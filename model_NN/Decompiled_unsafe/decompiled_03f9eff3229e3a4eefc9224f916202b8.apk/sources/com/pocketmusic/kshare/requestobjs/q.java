package com.pocketmusic.kshare.requestobjs;

import cn.banshenggua.aichang.utils.Toaster;
import com.android.volley.a;
import com.android.volley.l;
import com.android.volley.m;
import com.android.volley.r;
import com.android.volley.u;
import com.android.volley.v;
import com.android.volley.w;
import java.io.Serializable;
import org.json.JSONObject;

/* compiled from: SimpleRequestListener */
public class q implements r.a, r.b<JSONObject>, Serializable {
    private static final long serialVersionUID = 14807546712L;

    public void onErrorResponse(w wVar) {
        try {
            if (wVar instanceof l) {
                Toaster.showLongToast("手机未连接到网络");
            } else if (wVar instanceof u) {
                Toaster.showLongToast("服务器内部错误，错误码:" + wVar.f162a.f136a);
            } else if (!(wVar instanceof a) && !(wVar instanceof m)) {
                if (wVar instanceof v) {
                    Toaster.showLongToast("网路连接超时，请稍后再试");
                } else {
                    Toaster.showLongToast(wVar.getMessage());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onResponse(JSONObject jSONObject) {
    }
}
