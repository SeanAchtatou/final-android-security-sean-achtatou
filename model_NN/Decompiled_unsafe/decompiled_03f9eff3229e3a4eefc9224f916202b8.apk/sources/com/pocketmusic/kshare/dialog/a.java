package com.pocketmusic.kshare.dialog;

import android.view.View;
import eu.inmite.android.lib.dialogs.c;

/* compiled from: MyDialogFragment */
class a implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ MyDialogFragment f599a;

    a(MyDialogFragment myDialogFragment) {
        this.f599a = myDialogFragment;
    }

    public void onClick(View view) {
        c f = this.f599a.f();
        if (f != null) {
            f.onPositiveButtonClicked(this.f599a.f);
        }
        this.f599a.dismiss();
    }
}
