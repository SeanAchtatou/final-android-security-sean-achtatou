package com.pocketmusic.kshare.dialog;

import android.view.View;
import eu.inmite.android.lib.dialogs.c;

/* compiled from: MyDialogFragment */
class b implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ MyDialogFragment f600a;

    b(MyDialogFragment myDialogFragment) {
        this.f600a = myDialogFragment;
    }

    public void onClick(View view) {
        c f = this.f600a.f();
        if (f != null) {
            f.onNegativeButtonClicked(this.f600a.f);
        }
        this.f600a.dismiss();
    }
}
