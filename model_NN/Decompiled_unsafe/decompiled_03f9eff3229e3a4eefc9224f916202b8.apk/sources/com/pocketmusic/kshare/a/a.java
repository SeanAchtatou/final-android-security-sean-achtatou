package com.pocketmusic.kshare.a;

import android.text.TextUtils;
import cn.banshenggua.aichang.room.message.ContextError;
import cn.banshenggua.aichang.utils.Constants;
import com.pocketmusic.kshare.requestobjs.d;
import com.qq.e.comm.constants.ErrorCode;
import java.util.HashMap;
import java.util.Map;

/* compiled from: ContextError */
public class a {

    /* renamed from: a  reason: collision with root package name */
    public static final Map<Integer, String> f597a = new HashMap();

    static {
        f597a.put(-13, "昵称过长");
        f597a.put(-12, "签名过长");
        f597a.put(-11, "歌曲校验失败");
        f597a.put(-10, "该账号已被其他爱唱账号绑定");
        f597a.put(-9, "该昵称已经存在");
        f597a.put(-8, "昵称包含禁用敏感文字");
        f597a.put(-7, "只能用汉字,英文,数字和下划线");
        f597a.put(-6, "Email已经存在");
        f597a.put(-5, "Email不允许注册");
        f597a.put(-4, "Email不合法");
        f597a.put(-3, "该用户名已经存在");
        f597a.put(-2, "该用户名不允许注册");
        f597a.put(-1, "只能用汉字,英文,数字和下划线");
        f597a.put(0, " 注册的时候出错了！");
        f597a.put(1, "模块错误");
        f597a.put(10, "内容输入为空");
        f597a.put(11, "连接出问题了");
        f597a.put(12, "你被禁止访问");
        f597a.put(13, "AppKey或AppSecret错误");
        f597a.put(14, "应用正在审核中");
        f597a.put(15, "用户名或密码错误");
        f597a.put(16, "当天请求数超过限制");
        f597a.put(17, "对不起，出错了");
        f597a.put(20, "至少绑定一个帐号");
        f597a.put(21, "你被管理员禁言");
        f597a.put(22, "你的设备被禁止使用");
        f597a.put(23, "为了您的帐号安全，请再次登录。");
        f597a.put(24, " 为了您的帐号安全，请输入验证码。");
        f597a.put(25, "输入验证码验证错误");
        f597a.put(26, "重新发送过快，请一分钟以后再试");
        f597a.put(27, "输入短信验证码不正确，请重新输入");
        f597a.put(28, "手机短信验证码失效");
        f597a.put(29, "手机没有与该用户绑定");
        f597a.put(30, "登录密码错误");
        f597a.put(31, "手机号输入错误");
        f597a.put(39, "取消收藏失败");
        f597a.put(40, "微博ID为空");
        f597a.put(41, "该帖已不存在");
        f597a.put(42, "没有权限删除该帖子");
        f597a.put(100, "用户ID为空");
        f597a.put(Integer.valueOf((int) Constants.RESULT_OK), "用户ID错误");
        f597a.put(Integer.valueOf((int) Constants.REGISTER_OK), "ID为空");
        f597a.put(103, "ID错误");
        f597a.put(104, "微博内容不能为空");
        f597a.put(105, "对不起,原始微博已删除,不能评论或转发");
        f597a.put(106, "微博删除失败");
        f597a.put(107, "source_id为空");
        f597a.put(108, "source_id 错误");
        f597a.put(109, "target_id 为空");
        f597a.put(110, "target_id 错误");
        f597a.put(111, "私信内容不能为空");
        f597a.put(112, "私信接收人为空");
        f597a.put(113, "私信接收人错误");
        f597a.put(114, "没找到这个新浪或者QQ用户");
        f597a.put(115, "参数错误");
        f597a.put(116, "缺少上传文件");
        f597a.put(117, "文件类型错误");
        f597a.put(118, "上传歌曲不合法");
        f597a.put(119, "此用户已绑定");
        f597a.put(120, "还未绑定此用户");
        f597a.put(121, "已关注此用户");
        f597a.put(122, "尚未关注此用户");
        f597a.put(200, "数据库操作错误");
        f597a.put(201, "图片转换错误");
        f597a.put(202, "API访问错误");
        f597a.put(203, "您还没有绑定新浪微博");
        f597a.put(204, "您还没有绑定腾讯微博");
        f597a.put(205, "您还没有绑定QQ空间");
        f597a.put(Integer.valueOf((int) ErrorCode.InitError.INIT_ADMANGER_ERROR), "用户搜索的字串少于三个");
        f597a.put(Integer.valueOf((int) ErrorCode.InitError.INIT_PLUGIN_ERROR), "含有禁止使用的字符");
        f597a.put(Integer.valueOf((int) ErrorCode.NetWorkError.STUB_NETWORK_ERROR), "错误的请求");
        f597a.put(Integer.valueOf((int) ErrorCode.NetWorkError.QUEUE_FULL_ERROR), "用户认证错误");
        f597a.put(Integer.valueOf((int) ErrorCode.NetWorkError.HTTP_STATUS_ERROR), "禁止访问");
        f597a.put(Integer.valueOf((int) ErrorCode.NetWorkError.TIME_OUT_ERROR), "未找到");
        f597a.put(Integer.valueOf((int) ErrorCode.NetWorkError.RESOURCE_LOAD_FAIL_ERROR), "方法不被允许");
        f597a.put(500, "网络访问超时");
        f597a.put(501, "网络访问异常");
        f597a.put(503, "你无权进入此房间");
        f597a.put(Integer.valueOf((int) ContextError.Room_MUTED), "该房间已被封");
        f597a.put(536, "您等级不够，暂时无法赠送该礼物");
        f597a.put(Integer.valueOf((int) ErrorCode.OtherError.NETWORK_TYPE_ERROR), "拉黑时发生错误");
        f597a.put(Integer.valueOf((int) ErrorCode.OtherError.ANDROID_PERMMISON_ERROR), "取消拉黑时发生错误");
        f597a.put(Integer.valueOf((int) ErrorCode.OtherError.GET_PARAS_FROM_JS_ERROR), "无权进行此操作");
        f597a.put(Integer.valueOf((int) ErrorCode.OtherError.GET_PARAS_FROM_NATIVE_ERROR), "该用户在我的黑名单里面");
        f597a.put(Integer.valueOf((int) ErrorCode.OtherError.UNKNOWN_ERROR), "不需要添加");
        f597a.put(614, "房间不存在");
        f597a.put(617, "礼物已不存在, 请刷新");
        f597a.put(618, "礼物已经过期");
        f597a.put(619, "礼物卖光了");
        f597a.put(620, "礼物还没有开始卖");
        f597a.put(621, "爱币余额不足以致赠送失败，请充值");
        f597a.put(622, "没有修改任何信息");
        f597a.put(631, "订单号错误");
        f597a.put(640, "家族不存在");
        f597a.put(641, "该用户已加入其他家族");
        f597a.put(642, "你已经申请过了哦");
        f597a.put(643, "您没有权限");
        f597a.put(644, "家族重名");
        f597a.put(645, "家族满员");
        f597a.put(646, "家族名只能使用汉字，英文字母，数字和下划线");
        f597a.put(Integer.valueOf((int) Constants.CLEARIMGED), "内部错误");
        f597a.put(1001, "添加关注失败");
        f597a.put(1002, "取消关注失败");
        f597a.put(2001, "文件读取异常");
    }

    public static String a(int i, d dVar) {
        if (i == -1000) {
            return "";
        }
        String str = f597a.get(Integer.valueOf(i));
        if (str == null) {
            str = "未知错误: " + i;
        }
        if (dVar == null || TextUtils.isEmpty(dVar.b)) {
            return str;
        }
        return dVar.b;
    }
}
