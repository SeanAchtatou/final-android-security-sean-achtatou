package com.renn.rennsdk;

/* compiled from: AccessToken */
public class a {

    /* renamed from: a  reason: collision with root package name */
    public C0018a f701a;
    public String b;
    public String c;
    public String d;
    public String e;
    public String f;
    public long g;
    public long h;

    /* renamed from: com.renn.rennsdk.a$a  reason: collision with other inner class name */
    /* compiled from: AccessToken */
    public enum C0018a {
        Bearer,
        MAC
    }

    public String toString() {
        return "AccessToken [type=" + this.f701a + ", accessToken=" + this.b + ", refreshToken=" + this.c + ", macKey=" + this.d + ", macAlgorithm=" + this.e + "accessScope=" + this.f + ", expiresIn=" + this.g + "requestTime=" + this.h + "]";
    }
}
