package com.renn.rennsdk;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.renn.rennsdk.oauth.j;
import com.renn.rennsdk.oauth.r;

/* compiled from: RennClient */
public class b {
    private static b c;

    /* renamed from: a  reason: collision with root package name */
    private a f703a;
    private String b;
    private r d;
    private j e;

    /* compiled from: RennClient */
    public interface a {
        void a();

        void b();
    }

    public b(Context context) {
        this.e = j.a(context);
        this.d = r.a(context);
        if (a()) {
            this.f703a = new a();
            this.f703a.f701a = this.d.c("rr_renn_tokenType");
            this.f703a.b = this.d.a("rr_renn_accessToken");
            this.f703a.c = this.d.a("rr_renn_refreshToken");
            this.f703a.d = this.d.a("rr_renn_macKey");
            this.f703a.e = this.d.a("rr_renn_macAlgorithm");
            this.f703a.f = this.d.a("rr_renn_accessScope");
            this.f703a.g = this.d.b("rr_renn_expiresIn").longValue();
            this.f703a.h = this.d.b("rr_renn_requestTime").longValue();
            this.b = this.d.a("rr_renn_uid");
        }
    }

    public static synchronized b a(Context context) {
        b bVar;
        synchronized (b.class) {
            if (c == null) {
                c = new b(context);
            }
            bVar = c;
        }
        return bVar;
    }

    public boolean a(int i, int i2, Intent intent) {
        if (this.e != null) {
            return this.e.a(i, i2, intent);
        }
        return false;
    }

    public void a(a aVar) {
        this.f703a = aVar;
    }

    public void a(String str) {
        this.b = str;
    }

    public boolean a() {
        if (TextUtils.isEmpty(this.d.a("rr_renn_accessToken"))) {
            return false;
        }
        return true;
    }
}
