package com.renn.rennsdk.oauth;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.Toast;

/* compiled from: RegisterLayout */
class i implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ g f714a;

    i(g gVar) {
        this.f714a = gVar;
    }

    public void onClick(View view) {
        try {
            Intent intent = new Intent();
            intent.setAction("android.intent.action.SENDTO");
            intent.setData(Uri.parse("smsto:106900867742"));
            this.f714a.getContext().startActivity(intent);
        } catch (ActivityNotFoundException e) {
            g.b("Send Message Activity Not Found!");
            Toast.makeText(this.f714a.getContext(), this.f714a.getResources().getString(l.a(this.f714a.getContext(), "renren_function_sms_not_support")), 0).show();
        }
    }
}
