package com.renn.rennsdk.oauth;

import android.os.Handler;
import com.renn.rennsdk.oauth.k;

/* compiled from: SSO */
class n implements k.d {
    /* access modifiers changed from: package-private */

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ m f722a;
    private final /* synthetic */ k b;

    n(m mVar, k kVar) {
        this.f722a = mVar;
        this.b = kVar;
    }

    public void a(boolean z) {
        this.f722a.a(this.b.a(), this.b.b(), this.b.c(), this.b.d(), this.b.e(), this.b.f());
    }

    public void a(k.e eVar) {
        if (this.f722a.d != null) {
            new Handler(this.f722a.f721a.getMainLooper()).post(new o(this));
        }
    }
}
