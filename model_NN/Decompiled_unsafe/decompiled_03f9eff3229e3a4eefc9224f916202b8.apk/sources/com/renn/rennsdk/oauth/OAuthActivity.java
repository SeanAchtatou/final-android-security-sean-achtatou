package com.renn.rennsdk.oauth;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Proxy;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;
import android.view.KeyEvent;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.renn.rennsdk.b;

public class OAuthActivity extends Activity {
    private static final Object l = "a";
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public WebView f704a;
    private String b;
    private String c;
    /* access modifiers changed from: private */
    public String d;
    /* access modifiers changed from: private */
    public ProgressDialog e;
    private boolean f;
    /* access modifiers changed from: private */
    public g g;
    /* access modifiers changed from: private */
    public String h;
    private a i;
    /* access modifiers changed from: private */
    public boolean j = false;
    /* access modifiers changed from: private */
    public boolean k = true;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        Intent intent = getIntent();
        this.f = intent.getBooleanExtra("registe", false);
        if (!this.f) {
            this.b = intent.getStringExtra("apikey");
            this.c = intent.getStringExtra("scope");
            this.d = intent.getStringExtra("token_type");
            a();
            b();
            return;
        }
        if (this.g == null) {
            this.g = new g(this, true);
        }
        setContentView(this.g);
    }

    @SuppressLint({"SetJavaScriptEnabled"})
    private void a() {
        this.f704a = new WebView(this);
        setContentView(this.f704a);
        this.f704a.getSettings().setJavaScriptEnabled(true);
        this.i = new a(this, null);
        this.f704a.setWebViewClient(this.i);
        this.f704a.addJavascriptInterface(new c(this), "androidoauth");
        this.f704a.addJavascriptInterface(new d(this), "androidutil");
        this.e = new ProgressDialog(this);
        this.e.setMessage("Loading...");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 == 4) {
            if (this.f704a.canGoBack() && this.k) {
                this.f704a.goBack();
            }
            if (getIntent().getBooleanExtra("fromoauth", false)) {
                getIntent().putExtra("fromoauth", false);
                getIntent().putExtra("registe", false);
                Intent intent = getIntent();
                this.b = intent.getStringExtra("apikey");
                this.c = intent.getStringExtra("scope");
                a();
                b();
                return true;
            }
        }
        return super.onKeyDown(i2, keyEvent);
    }

    private void b() {
        if (c()) {
            new e(this).start();
            return;
        }
        this.f704a.loadUrl("https://graph.renren.com/oauth/v2/wap/authorize?" + d());
    }

    private boolean c() {
        NetworkInfo activeNetworkInfo;
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService("connectivity");
        if (!(connectivityManager == null || (activeNetworkInfo = connectivityManager.getActiveNetworkInfo()) == null || activeNetworkInfo.getType() == 1)) {
            String defaultHost = Proxy.getDefaultHost();
            int defaultPort = Proxy.getDefaultPort();
            if (!(defaultHost == null || defaultPort == 0)) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (!this.j) {
            b.a(this).a(1, 0, new Intent());
        }
        super.onDestroy();
        if (this.e != null) {
            this.e.dismiss();
            this.e = null;
        }
    }

    /* access modifiers changed from: private */
    public String d() {
        StringBuilder sb = new StringBuilder();
        sb.append("client_id=").append(this.b).append("&").append("redirect_uri=").append("http://graph.renren.com/oauth/login_success.html").append("&").append("scope=").append(this.c).append("&").append("token_type=").append(this.d);
        return sb.toString();
    }

    private class a extends WebViewClient {
        private a() {
        }

        /* synthetic */ a(OAuthActivity oAuthActivity, a aVar) {
            this();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
         arg types: [int, int]
         candidates:
          ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
          ClspMth{java.lang.String.replace(char, char):java.lang.String} */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.renn.rennsdk.oauth.OAuthActivity.a(com.renn.rennsdk.oauth.OAuthActivity, boolean):void
         arg types: [com.renn.rennsdk.oauth.OAuthActivity, int]
         candidates:
          com.renn.rennsdk.oauth.OAuthActivity.a(com.renn.rennsdk.oauth.OAuthActivity, com.renn.rennsdk.oauth.g):void
          com.renn.rennsdk.oauth.OAuthActivity.a(com.renn.rennsdk.oauth.OAuthActivity, java.lang.String):void
          com.renn.rennsdk.oauth.OAuthActivity.a(com.renn.rennsdk.oauth.OAuthActivity, boolean):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
         arg types: [java.lang.String, int]
         candidates:
          ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
        public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
            String replace = str.replace('#', '?');
            if (OAuthActivity.this.e != null) {
                OAuthActivity.this.e.show();
            }
            if (replace.startsWith("http://graph.renren.com/oauth/login_success.html")) {
                Uri parse = Uri.parse(replace);
                String queryParameter = parse.getQueryParameter("access_token");
                String queryParameter2 = parse.getQueryParameter("scope");
                String queryParameter3 = parse.getQueryParameter("mac_key");
                String queryParameter4 = parse.getQueryParameter("mac_algorithm");
                String queryParameter5 = parse.getQueryParameter("expires_in");
                OAuthActivity.this.f704a.stopLoading();
                Intent intent = new Intent();
                intent.putExtra("access_token", queryParameter);
                intent.putExtra("scope", queryParameter2);
                intent.putExtra("mac_key", queryParameter3);
                intent.putExtra("mac_algorithm", queryParameter4);
                intent.putExtra("token_type", OAuthActivity.this.d);
                if (queryParameter5 == null) {
                    intent.putExtra("expired_in", 0);
                } else {
                    intent.putExtra("expired_in", Long.valueOf(queryParameter5));
                }
                OAuthActivity.this.j = true;
                b.a(OAuthActivity.this).a(1, -1, intent);
                OAuthActivity.this.setResult(-1, intent);
                OAuthActivity.this.finish();
            } else if (replace.startsWith("http-renren://cancel.tk/")) {
                OAuthActivity.this.f704a.stopLoading();
                OAuthActivity.this.finish();
            } else if (replace.startsWith("http://mreg.renren.com/reg/wljump.do")) {
                OAuthActivity.this.getIntent().putExtra("registe", true);
                OAuthActivity.this.getIntent().putExtra("fromoauth", true);
                if (OAuthActivity.this.g == null) {
                    OAuthActivity.this.g = new g(OAuthActivity.this, true);
                }
                OAuthActivity.this.f704a.stopLoading();
                OAuthActivity.this.setContentView(OAuthActivity.this.g);
            } else if (replace.startsWith("data:text")) {
                OAuthActivity.this.k = false;
            }
            super.onPageStarted(webView, replace, bitmap);
        }

        public void onReceivedSslError(WebView webView, SslErrorHandler sslErrorHandler, SslError sslError) {
            sslErrorHandler.proceed();
        }

        public void onPageFinished(WebView webView, String str) {
            super.onPageFinished(webView, str);
            if (OAuthActivity.this.e != null) {
                OAuthActivity.this.e.dismiss();
                OAuthActivity.this.f704a.invalidate();
            }
        }

        public void onReceivedError(WebView webView, int i, String str, String str2) {
            OAuthActivity.this.h = str2;
            a(webView, "网络连接错误", "重试", "关闭");
            super.onReceivedError(webView, i, str, str2);
        }

        private void a(WebView webView, String str, String str2, String str3) {
            webView.stopLoading();
            webView.clearView();
            webView.getSettings().setDefaultTextEncodingName("UTF-8");
            webView.loadData("", "text/html", "UTF-8");
            if (str == null) {
                str = "";
            }
            if (str2 == null) {
            }
            if (str3 == null) {
            }
            webView.loadData(str, "text/html; charset=UTF-8", null);
        }
    }
}
