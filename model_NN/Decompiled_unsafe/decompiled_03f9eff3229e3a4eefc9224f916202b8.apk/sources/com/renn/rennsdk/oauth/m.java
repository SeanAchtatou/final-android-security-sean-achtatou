package com.renn.rennsdk.oauth;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import com.renn.rennsdk.a;
import com.renn.rennsdk.b;

/* compiled from: SSO */
public class m {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Context f721a;
    private b b;
    private int c;
    /* access modifiers changed from: private */
    public b.a d;

    public m(Context context, b bVar) {
        this.f721a = context;
        this.b = bVar;
    }

    public void a(int i) {
        this.c = i;
    }

    public void a(b.a aVar) {
        this.d = aVar;
    }

    public boolean a(int i, int i2, Intent intent) {
        if (i == this.c) {
            switch (i2) {
                case -1:
                    if (intent != null) {
                        a(intent.getStringExtra("access_token"), intent.getStringExtra("scope"), intent.getLongExtra("expired_in", 0), intent.getStringExtra("token_type"), intent.getStringExtra("mac_key"), intent.getStringExtra("mac_algorithm"));
                        return true;
                    }
                    break;
                case 0:
                    if (this.d != null) {
                        this.d.b();
                        break;
                    }
                    break;
            }
        }
        return false;
    }

    public boolean a(int i, String str, String str2, String str3, String str4) {
        this.c = i;
        if (this.f721a instanceof Activity) {
            k kVar = new k((Activity) this.f721a, str, str3, a.a(this.f721a).e(), str4);
            if (Boolean.valueOf(kVar.a(new n(this, kVar))).booleanValue()) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public void a(String str, String str2, long j, String str3, String str4, String str5) {
        a.C0018a aVar = a.C0018a.Bearer;
        String str6 = null;
        if (str3.equals("mac")) {
            aVar = a.C0018a.MAC;
            String substring = str.substring(0, str.lastIndexOf("."));
            str6 = substring.substring(substring.lastIndexOf(".") + 1);
        } else if (str3.equals("bearer")) {
            aVar = a.C0018a.Bearer;
            str6 = str.substring(str.lastIndexOf("-") + 1);
        }
        a aVar2 = new a();
        aVar2.f701a = aVar;
        aVar2.b = str;
        aVar2.c = "";
        aVar2.d = str4;
        aVar2.e = str5;
        aVar2.f = str2;
        aVar2.g = j;
        aVar2.h = System.currentTimeMillis();
        r a2 = r.a(this.f721a);
        a2.a("rr_renn_tokenType", aVar2.f701a);
        a2.a("rr_renn_accessToken", aVar2.b);
        a2.a("rr_renn_refreshToken", aVar2.c);
        a2.a("rr_renn_macKey", aVar2.d);
        a2.a("rr_renn_macAlgorithm", aVar2.e);
        a2.a("rr_renn_accessScope", aVar2.f);
        a2.a("rr_renn_expiresIn", Long.valueOf(aVar2.g));
        a2.a("rr_renn_requestTime", Long.valueOf(aVar2.h));
        a2.a("rr_renn_uid", str6);
        b.a(this.f721a).a(aVar2);
        b.a(this.f721a).a(str6);
        new p(this).start();
    }
}
