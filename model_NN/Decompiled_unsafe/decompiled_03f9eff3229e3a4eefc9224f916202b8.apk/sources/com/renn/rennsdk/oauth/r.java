package com.renn.rennsdk.oauth;

import android.content.Context;
import android.content.SharedPreferences;
import com.renn.rennsdk.a;

/* compiled from: ValueStorage */
public class r {
    private static r c;

    /* renamed from: a  reason: collision with root package name */
    private SharedPreferences f726a;
    private SharedPreferences.Editor b = this.f726a.edit();

    public r(Context context) {
        this.f726a = context.getSharedPreferences(context.getPackageName(), 0);
    }

    public static synchronized r a(Context context) {
        r rVar;
        synchronized (r.class) {
            if (c == null) {
                c = new r(context);
            }
            rVar = c;
        }
        return rVar;
    }

    public void a(String str, String str2) {
        this.b.putString(str, str2);
        this.b.commit();
    }

    public String a(String str) {
        return this.f726a.getString(str, "");
    }

    public void a(String str, Long l) {
        this.b.putLong(str, l.longValue());
        this.b.commit();
    }

    public Long b(String str) {
        return Long.valueOf(this.f726a.getLong(str, 0));
    }

    public void a(String str, a.C0018a aVar) {
        if (aVar == a.C0018a.Bearer) {
            this.b.putInt(str, 0);
        } else if (aVar == a.C0018a.MAC) {
            this.b.putInt(str, 1);
        }
        this.b.commit();
    }

    public a.C0018a c(String str) {
        if (this.f726a.getInt(str, 0) == 1) {
            return a.C0018a.MAC;
        }
        return a.C0018a.Bearer;
    }
}
