package com.renn.rennsdk.oauth;

import android.app.Activity;
import android.view.View;

/* compiled from: RegisterLayout */
class h implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ g f713a;

    h(g gVar) {
        this.f713a = gVar;
    }

    public void onClick(View view) {
        if (this.f713a.e != null) {
            this.f713a.e.b();
        }
        if (this.f713a.d instanceof Activity) {
            ((Activity) this.f713a.d).finish();
        }
    }
}
