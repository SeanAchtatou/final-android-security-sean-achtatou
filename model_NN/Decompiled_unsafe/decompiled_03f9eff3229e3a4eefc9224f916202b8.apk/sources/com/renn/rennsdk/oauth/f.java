package com.renn.rennsdk.oauth;

/* compiled from: RRException */
public class f extends Exception {

    /* renamed from: a  reason: collision with root package name */
    private int f711a;
    private String b;
    private String c;

    public f(String str) {
        super(str);
        this.b = str;
    }

    public String toString() {
        return "RRException [mExceptionCode=" + this.f711a + ", mExceptionMsg=" + this.b + ", mExceptionDescription=" + this.c + "]";
    }
}
