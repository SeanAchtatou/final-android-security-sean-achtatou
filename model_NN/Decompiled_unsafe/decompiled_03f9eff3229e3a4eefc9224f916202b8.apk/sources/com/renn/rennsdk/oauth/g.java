package com.renn.rennsdk.oauth;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.text.SpannableStringBuilder;
import android.text.style.TextAppearanceSpan;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.renn.rennsdk.b;
import java.lang.reflect.Array;

/* compiled from: RegisterLayout */
class g extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    private float f712a;
    private RelativeLayout b;
    private Object c;
    /* access modifiers changed from: private */
    public Context d;
    /* access modifiers changed from: private */
    public b.a e;

    public g(Context context, boolean z) {
        super(context);
        a(context);
        setBackgroundColor(17170444);
        this.d = context;
        int i = context.getResources().getConfiguration().orientation;
        this.f712a = context.getResources().getDisplayMetrics().density;
        switch (i) {
            case 0:
            case 1:
            case 3:
                b();
                return;
            case 2:
                a();
                return;
            default:
                return;
        }
    }

    public void a(Object obj) {
        this.c = obj;
    }

    private void a() {
        a(true);
        addView(this.b);
    }

    private void b() {
        a(false);
        addView(this.b);
    }

    private void a(boolean z) {
        String replace;
        RelativeLayout.LayoutParams layoutParams;
        int i;
        int indexOf;
        boolean z2;
        boolean equals = "000000000000000".equals(a.a(this.d).d());
        Drawable drawable = getResources().getDrawable(l.b(getContext(), "renren_login_background"));
        Bitmap bitmap = ((BitmapDrawable) getResources().getDrawable(l.b(getContext(), "renren_logo_land"))).getBitmap();
        Drawable drawable2 = getResources().getDrawable(l.b(getContext(), "renren_topbar"));
        TextView textView = new TextView(getContext());
        textView.setBackgroundResource(l.b(getContext(), "renren_registe_tips"));
        if (!equals) {
            replace = getResources().getString(l.a(getContext(), "renren_register_tip_str_with_sim"));
        } else {
            replace = getResources().getString(l.a(getContext(), "renren_register_tip_str_without_sim")).replace("#", "\n");
        }
        String replace2 = replace.replace("^ ", " ");
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        boolean z3 = true;
        int i2 = 0;
        int indexOf2 = replace2.indexOf(36);
        int i3 = 0;
        while (indexOf2 != -1) {
            if (z3) {
                spannableStringBuilder.append(replace2.subSequence(i3, indexOf2));
                SpannableStringBuilder spannableStringBuilder2 = spannableStringBuilder;
                spannableStringBuilder2.setSpan(new TextAppearanceSpan(null, 0, (int) (14.0f * this.f712a), new ColorStateList((int[][]) Array.newInstance(Integer.TYPE, 1, 0), new int[]{-12699079}), null), i3 - i2, indexOf2 - i2, 33);
                int i4 = indexOf2 + 1;
                i = i4;
                indexOf = replace2.indexOf(36, i4);
            } else {
                spannableStringBuilder.append(replace2.subSequence(i3, indexOf2));
                SpannableStringBuilder spannableStringBuilder3 = spannableStringBuilder;
                spannableStringBuilder3.setSpan(new TextAppearanceSpan(null, 0, (int) (22.0f * this.f712a), new ColorStateList((int[][]) Array.newInstance(Integer.TYPE, 1, 0), new int[]{-13602375}), null), i3 - i2, indexOf2 - i2, 33);
                int i5 = indexOf2 + 1;
                i = i5;
                indexOf = replace2.indexOf(36, i5);
            }
            int i6 = i2 + 1;
            if (z3) {
                z2 = false;
            } else {
                z2 = true;
            }
            z3 = z2;
            i2 = i6;
            indexOf2 = indexOf;
            i3 = i;
        }
        int length = replace2.length();
        if (z3) {
            spannableStringBuilder.append(replace2.subSequence(i3, length));
            SpannableStringBuilder spannableStringBuilder4 = spannableStringBuilder;
            spannableStringBuilder4.setSpan(new TextAppearanceSpan(null, 0, (int) (14.0f * this.f712a), new ColorStateList((int[][]) Array.newInstance(Integer.TYPE, 1, 0), new int[]{-12699079}), null), i3 - i2, length - i2, 33);
        } else {
            spannableStringBuilder.append(replace2.subSequence(i3, length));
            SpannableStringBuilder spannableStringBuilder5 = spannableStringBuilder;
            spannableStringBuilder5.setSpan(new TextAppearanceSpan(null, 0, (int) (22.0f * this.f712a), new ColorStateList((int[][]) Array.newInstance(Integer.TYPE, 1, 0), new int[]{-13602375}), null), i3 - i2, length - i2, 33);
        }
        textView.setText(spannableStringBuilder, TextView.BufferType.SPANNABLE);
        textView.setPadding((int) (10.0f * this.f712a), (int) (10.0f * this.f712a), (int) (10.0f * this.f712a), (int) (10.0f * this.f712a));
        this.b = new RelativeLayout(getContext());
        this.b.setBackgroundDrawable(drawable);
        this.b.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        int i7 = (int) (10.0f * this.f712a);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, (int) (44.0f * this.f712a));
        layoutParams2.addRule(10);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams((int) (65.0f * this.f712a), (int) (34.0f * this.f712a));
        layoutParams3.rightMargin = (int) (7.0f * this.f712a);
        layoutParams3.addRule(11);
        layoutParams3.addRule(15);
        RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams((int) (54.0f * this.f712a), (int) (34.0f * this.f712a));
        layoutParams4.leftMargin = (int) (7.0f * this.f712a);
        layoutParams4.addRule(9);
        layoutParams4.addRule(15);
        if (!z) {
            layoutParams = new RelativeLayout.LayoutParams((int) (220.0f * this.f712a), -2);
            layoutParams.topMargin = (int) (55.0f * this.f712a);
        } else {
            layoutParams = new RelativeLayout.LayoutParams((int) (350.0f * this.f712a), -2);
            layoutParams.topMargin = i7;
        }
        layoutParams.addRule(14);
        RelativeLayout.LayoutParams layoutParams5 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams5.leftMargin = i7 * 3;
        layoutParams5.rightMargin = i7 * 3;
        if (!z) {
            layoutParams5.topMargin = (int) (30.0f * this.f712a);
        } else {
            layoutParams5.topMargin = i7;
        }
        layoutParams5.addRule(14);
        RelativeLayout relativeLayout = new RelativeLayout(getContext());
        relativeLayout.setLayoutParams(layoutParams2);
        relativeLayout.setBackgroundDrawable(drawable2);
        relativeLayout.setId(2001);
        Button button = new Button(getContext());
        button.setLayoutParams(layoutParams3);
        button.setBackgroundResource(l.b(getContext(), "renren_action_btn"));
        button.setText(getResources().getString(l.a(getContext(), "renren_send_reg_sms_cancal_button_title")));
        button.setTextSize(15.0f);
        button.setTextColor(-1);
        button.setVisibility(8);
        ImageView imageView = new ImageView(getContext());
        imageView.setLayoutParams(layoutParams4);
        imageView.setImageBitmap(bitmap);
        layoutParams.addRule(3, 2001);
        textView.setLayoutParams(layoutParams);
        textView.setId(2002);
        layoutParams5.addRule(3, 2002);
        Button button2 = new Button(getContext());
        button2.setLayoutParams(layoutParams5);
        button2.setBackgroundResource(l.b(getContext(), "renren_action_btn"));
        button2.setTextColor(-855310);
        button2.setTextSize(18.0f);
        button2.setPadding(10, i7 >> 1, 10, i7 >> 1);
        button2.setText(String.valueOf(getResources().getString(l.a(getContext(), "renren_register_sent_sms_button_title"))) + "106900867742");
        if (equals) {
            drawable.setVisible(false, true);
        }
        relativeLayout.addView(button);
        relativeLayout.addView(imageView);
        this.b.addView(relativeLayout);
        this.b.addView(textView);
        this.b.addView(button2);
        button.setOnClickListener(new h(this));
        button2.setOnClickListener(new i(this));
    }

    /* access modifiers changed from: private */
    public static final void b(String str) {
    }
}
