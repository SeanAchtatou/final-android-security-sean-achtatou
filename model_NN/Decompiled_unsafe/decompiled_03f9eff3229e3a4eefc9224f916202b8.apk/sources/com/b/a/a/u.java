package com.b.a.a;

import java.io.IOException;
import java.io.Writer;

public class u extends i {

    /* renamed from: a  reason: collision with root package name */
    private StringBuffer f193a;

    public u(String str) {
        this.f193a = new StringBuffer(str);
    }

    public String a() {
        return this.f193a.toString();
    }

    /* access modifiers changed from: package-private */
    public void a(Writer writer) throws IOException {
        writer.write(this.f193a.toString());
    }

    public void a(char[] cArr, int i, int i2) {
        this.f193a.append(cArr, i, i2);
        b();
    }

    /* access modifiers changed from: package-private */
    public void b(Writer writer) throws IOException {
        String stringBuffer = this.f193a.toString();
        if (stringBuffer.length() < 50) {
            i.a(writer, stringBuffer);
            return;
        }
        writer.write("<![CDATA[");
        writer.write(stringBuffer);
        writer.write("]]>");
    }

    /* access modifiers changed from: protected */
    public int c() {
        return this.f193a.toString().hashCode();
    }

    public Object clone() {
        return new u(this.f193a.toString());
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof u)) {
            return false;
        }
        return this.f193a.toString().equals(((u) obj).f193a.toString());
    }
}
