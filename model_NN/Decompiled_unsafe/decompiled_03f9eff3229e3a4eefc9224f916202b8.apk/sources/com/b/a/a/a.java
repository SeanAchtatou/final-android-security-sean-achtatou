package com.b.a.a;

class a implements e, n {
    private final o c;
    private f d;
    private final d e;
    private p f;

    public a() {
        this(null);
    }

    public a(o oVar) {
        this.d = null;
        this.e = new d();
        this.f = null;
        this.c = oVar == null ? p.f190a : oVar;
    }

    public d a() {
        return this.e;
    }

    public void a(f fVar) {
        if (this.d == null) {
            this.e.a(fVar);
        } else {
            this.d.b((i) fVar);
        }
        this.d = fVar;
    }

    public void a(p pVar) {
        this.f = pVar;
        this.e.a(pVar.toString());
    }

    public void a(char[] cArr, int i, int i2) {
        f fVar = this.d;
        if (fVar.e() instanceof u) {
            ((u) fVar.e()).a(cArr, i, i2);
        } else {
            fVar.a(new u(new String(cArr, i, i2)));
        }
    }

    public void b() {
    }

    public void b(f fVar) {
        this.d = this.d.g();
    }

    public void c() {
    }

    public String toString() {
        if (this.f != null) {
            return new StringBuffer().append("BuildDoc: ").append(this.f.toString()).toString();
        }
        return null;
    }
}
