package com.b.a.a;

public class m extends Exception {

    /* renamed from: a  reason: collision with root package name */
    private int f189a;
    private Throwable b;

    public m(l lVar, char c, char c2) {
        this(lVar, new StringBuffer().append("got '").append(c).append("' instead of expected '").append(c2).append("'").toString());
    }

    public m(l lVar, char c, String str) {
        this(lVar, new StringBuffer().append("got '").append(c).append("' instead of ").append(str).append(" as expected").toString());
    }

    public m(l lVar, char c, char[] cArr) {
        this(lVar, new StringBuffer().append("got '").append(c).append("' instead of ").append(a(cArr)).toString());
    }

    public m(l lVar, String str) {
        this(lVar.e(), lVar.a(), lVar.b(), lVar.c(), lVar.d(), str);
    }

    public m(l lVar, String str, String str2) {
        this(lVar, new StringBuffer().append("got \"").append(str).append("\" instead of \"").append(str2).append("\" as expected").toString());
    }

    public m(l lVar, String str, char[] cArr) {
        this(lVar, str, new String(cArr));
    }

    public m(o oVar, String str, int i, int i2, String str2, String str3) {
        this(str, i, i2, str2, str3);
        oVar.a(str3, str, i);
    }

    public m(String str, int i, int i2, String str2, String str3) {
        super(a(str, i, i2, str2, str3));
        this.f189a = -1;
        this.b = null;
        this.f189a = i;
    }

    public m(String str, Throwable th) {
        super(new StringBuffer().append(str).append(" ").append(th).toString());
        this.f189a = -1;
        this.b = null;
        this.b = th;
    }

    static String a(int i) {
        return i == -1 ? "EOF" : new StringBuffer().append("").append((char) i).toString();
    }

    private static String a(String str, int i, int i2, String str2, String str3) {
        return new StringBuffer().append(str).append("(").append(i).append("): \n").append(str2).append("\nLast character read was '").append(a(i2)).append("'\n").append(str3).toString();
    }

    private static String a(char[] cArr) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(cArr[0]);
        for (int i = 1; i < cArr.length; i++) {
            stringBuffer.append(new StringBuffer().append("or ").append(cArr[i]).toString());
        }
        return stringBuffer.toString();
    }

    public Throwable getCause() {
        return this.b;
    }
}
