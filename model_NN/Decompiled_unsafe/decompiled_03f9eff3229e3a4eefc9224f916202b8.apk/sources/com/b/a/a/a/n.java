package com.b.a.a.a;

import com.sina.weibo.sdk.register.mobile.SelectCountryActivity;
import java.io.IOException;

public class n {
    static k a(ac acVar, s sVar) throws ad, IOException {
        int i;
        int i2;
        switch (sVar.f177a) {
            case -3:
                if (!sVar.c.equals("text")) {
                    throw new ad(acVar, "at beginning of expression", sVar, "text()");
                } else if (sVar.a() != 40) {
                    throw new ad(acVar, "after text", sVar, "(");
                } else if (sVar.a() != 41) {
                    throw new ad(acVar, "after text(", sVar, ")");
                } else {
                    switch (sVar.a()) {
                        case 33:
                            sVar.a();
                            if (sVar.f177a != 61) {
                                throw new ad(acVar, "after !", sVar, "=");
                            }
                            sVar.a();
                            if (sVar.f177a == 34 || sVar.f177a == 39) {
                                String str = sVar.c;
                                sVar.a();
                                return new x(str);
                            }
                            throw new ad(acVar, "right hand side of !=", sVar, "quoted string");
                        case 61:
                            sVar.a();
                            if (sVar.f177a == 34 || sVar.f177a == 39) {
                                String str2 = sVar.c;
                                sVar.a();
                                return new v(str2);
                            }
                            throw new ad(acVar, "right hand side of equals", sVar, "quoted string");
                        default:
                            return w.f180a;
                    }
                }
            case -2:
                int i3 = sVar.b;
                sVar.a();
                return new r(i3);
            case 64:
                if (sVar.a() != -3) {
                    throw new ad(acVar, "after @", sVar, SelectCountryActivity.EXTRA_COUNTRY_NAME);
                }
                String str3 = sVar.c;
                switch (sVar.a()) {
                    case 33:
                        sVar.a();
                        if (sVar.f177a != 61) {
                            throw new ad(acVar, "after !", sVar, "=");
                        }
                        sVar.a();
                        if (sVar.f177a == 34 || sVar.f177a == 39) {
                            String str4 = sVar.c;
                            sVar.a();
                            return new h(str3, str4);
                        }
                        throw new ad(acVar, "right hand side of !=", sVar, "quoted string");
                    case 60:
                        sVar.a();
                        if (sVar.f177a == 34 || sVar.f177a == 39) {
                            i2 = Integer.parseInt(sVar.c);
                        } else if (sVar.f177a == -2) {
                            i2 = sVar.b;
                        } else {
                            throw new ad(acVar, "right hand side of less-than", sVar, "quoted string or number");
                        }
                        sVar.a();
                        return new g(str3, i2);
                    case 61:
                        sVar.a();
                        if (sVar.f177a == 34 || sVar.f177a == 39) {
                            String str5 = sVar.c;
                            sVar.a();
                            return new c(str3, str5);
                        }
                        throw new ad(acVar, "right hand side of equals", sVar, "quoted string");
                    case 62:
                        sVar.a();
                        if (sVar.f177a == 34 || sVar.f177a == 39) {
                            i = Integer.parseInt(sVar.c);
                        } else if (sVar.f177a == -2) {
                            i = sVar.b;
                        } else {
                            throw new ad(acVar, "right hand side of greater-than", sVar, "quoted string or number");
                        }
                        sVar.a();
                        return new f(str3, i);
                    default:
                        return new d(str3);
                }
            default:
                throw new ad(acVar, "at beginning of expression", sVar, "@, number, or text()");
        }
    }
}
