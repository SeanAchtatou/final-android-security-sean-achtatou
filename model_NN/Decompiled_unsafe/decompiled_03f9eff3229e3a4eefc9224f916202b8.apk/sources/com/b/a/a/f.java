package com.b.a.a;

import com.b.a.a.a.ac;
import com.b.a.a.a.ad;
import java.io.IOException;
import java.io.Writer;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

public class f extends i {

    /* renamed from: a  reason: collision with root package name */
    private i f185a = null;
    private i b = null;
    private Hashtable c = null;
    private Vector d = null;
    private String e = null;

    f() {
    }

    public f(String str) {
        this.e = r.a(str);
    }

    private v a(String str, boolean z) throws ad {
        ac a2 = ac.a(str);
        if (a2.b() == z) {
            return new v(this, a2);
        }
        throw new ad(a2, new StringBuffer().append("\"").append(a2).append("\" evaluates to ").append(z ? "evaluates to element not string" : "evaluates to string not element").toString());
    }

    private boolean e(i iVar) {
        int i = 0;
        for (i iVar2 = this.f185a; iVar2 != null; iVar2 = iVar2.i()) {
            if (iVar2.equals(iVar)) {
                if (this.f185a == iVar2) {
                    this.f185a = iVar2.i();
                }
                if (this.b == iVar2) {
                    this.b = iVar2.h();
                }
                iVar2.j();
                iVar2.b((f) null);
                iVar2.a((d) null);
                return true;
            }
            i++;
        }
        return false;
    }

    public f a(boolean z) {
        f fVar = new f(this.e);
        if (this.d != null) {
            Enumeration elements = this.d.elements();
            while (elements.hasMoreElements()) {
                String str = (String) elements.nextElement();
                fVar.a(str, (String) this.c.get(str));
            }
        }
        if (z) {
            for (i iVar = this.f185a; iVar != null; iVar = iVar.i()) {
                fVar.b((i) iVar.clone());
            }
        }
        return fVar;
    }

    public String a() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public void a(i iVar) {
        f g = iVar.g();
        if (g != null) {
            g.e(iVar);
        }
        iVar.d(this.b);
        if (this.f185a == null) {
            this.f185a = iVar;
        }
        iVar.b(this);
        this.b = iVar;
        iVar.a(f());
    }

    /* access modifiers changed from: package-private */
    public void a(Writer writer) throws IOException {
        for (i iVar = this.f185a; iVar != null; iVar = iVar.i()) {
            iVar.a(writer);
        }
    }

    public void a(String str) {
        this.e = r.a(str);
        b();
    }

    public void a(String str, String str2) {
        if (this.c == null) {
            this.c = new Hashtable();
            this.d = new Vector();
        }
        if (this.c.get(str) == null) {
            this.d.addElement(str);
        }
        this.c.put(str, str2);
        b();
    }

    public String b(String str) {
        if (this.c == null) {
            return null;
        }
        return (String) this.c.get(str);
    }

    public void b(i iVar) {
        a(!c(iVar) ? (f) iVar.clone() : iVar);
        b();
    }

    public void b(Writer writer) throws IOException {
        writer.write(new StringBuffer().append("<").append(this.e).toString());
        if (this.d != null) {
            Enumeration elements = this.d.elements();
            while (elements.hasMoreElements()) {
                String str = (String) elements.nextElement();
                writer.write(new StringBuffer().append(" ").append(str).append("=\"").toString());
                i.a(writer, (String) this.c.get(str));
                writer.write("\"");
            }
        }
        if (this.f185a == null) {
            writer.write("/>");
            return;
        }
        writer.write(">");
        for (i iVar = this.f185a; iVar != null; iVar = iVar.i()) {
            iVar.b(writer);
        }
        writer.write(new StringBuffer().append("</").append(this.e).append(">").toString());
    }

    /* access modifiers changed from: protected */
    public int c() {
        int i;
        int hashCode = this.e.hashCode();
        if (this.c != null) {
            Enumeration keys = this.c.keys();
            while (true) {
                i = hashCode;
                if (!keys.hasMoreElements()) {
                    break;
                }
                String str = (String) keys.nextElement();
                int hashCode2 = (i * 31) + str.hashCode();
                hashCode = ((String) this.c.get(str)).hashCode() + (hashCode2 * 31);
            }
        } else {
            i = hashCode;
        }
        for (i iVar = this.f185a; iVar != null; iVar = iVar.i()) {
            i = (i * 31) + iVar.hashCode();
        }
        return i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.b.a.a.f.a(java.lang.String, boolean):com.b.a.a.v
     arg types: [java.lang.String, int]
     candidates:
      com.b.a.a.f.a(java.lang.String, java.lang.String):void
      com.b.a.a.i.a(java.io.Writer, java.lang.String):void
      com.b.a.a.f.a(java.lang.String, boolean):com.b.a.a.v */
    public String c(String str) throws m {
        try {
            return a(str, true).b();
        } catch (ad e2) {
            throw new m("XPath problem", e2);
        }
    }

    /* access modifiers changed from: package-private */
    public boolean c(i iVar) {
        if (iVar == this) {
            return false;
        }
        f g = g();
        if (g == null) {
            return true;
        }
        return g.c(iVar);
    }

    public Object clone() {
        return a(true);
    }

    public i d() {
        return this.f185a;
    }

    public i e() {
        return this.b;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof f)) {
            return false;
        }
        f fVar = (f) obj;
        if (!this.e.equals(fVar.e)) {
            return false;
        }
        if ((this.c == null ? 0 : this.c.size()) != (fVar.c == null ? 0 : fVar.c.size())) {
            return false;
        }
        if (this.c != null) {
            Enumeration keys = this.c.keys();
            while (keys.hasMoreElements()) {
                String str = (String) keys.nextElement();
                if (!((String) this.c.get(str)).equals((String) fVar.c.get(str))) {
                    return false;
                }
            }
        }
        i iVar = this.f185a;
        i iVar2 = fVar.f185a;
        while (iVar != null) {
            if (!iVar.equals(iVar2)) {
                return false;
            }
            iVar = iVar.i();
            iVar2 = iVar2.i();
        }
        return true;
    }
}
