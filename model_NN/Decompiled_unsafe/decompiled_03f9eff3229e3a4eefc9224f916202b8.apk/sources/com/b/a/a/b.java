package com.b.a.a;

class b {

    /* renamed from: a  reason: collision with root package name */
    private final int[] f183a;
    private int b;
    private int c;

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer((this.f183a.length * 11) / 10);
        for (int length = this.c < this.f183a.length ? this.f183a.length - this.c : 0; length < this.f183a.length; length++) {
            int i = this.f183a[(this.b + length) % this.f183a.length];
            if (i < 65536) {
                stringBuffer.append((char) i);
            } else {
                stringBuffer.append(Integer.toString(i - 65536));
            }
        }
        return stringBuffer.toString();
    }
}
