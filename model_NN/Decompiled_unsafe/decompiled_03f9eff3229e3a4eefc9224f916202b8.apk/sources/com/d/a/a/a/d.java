package com.d.a.a.a;

import java.io.File;

/* compiled from: LimitedDiscCache */
class d implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ c f461a;

    d(c cVar) {
        this.f461a = cVar;
    }

    public void run() {
        File[] listFiles = this.f461a.f460a.listFiles();
        if (listFiles != null) {
            int i = 0;
            for (File file : listFiles) {
                i += this.f461a.a(file);
                this.f461a.d.put(file, Long.valueOf(file.lastModified()));
            }
            this.f461a.b.set(i);
        }
    }
}
