package com.d.a.a.a;

import java.io.File;

/* compiled from: BaseDiscCache */
public abstract class a implements b {

    /* renamed from: a  reason: collision with root package name */
    protected File f460a;
    private com.d.a.a.a.b.a b;

    public a(File file, com.d.a.a.a.b.a aVar) {
        if (file == null) {
            throw new IllegalArgumentException(String.format("\"%s\" argument must be not null", "cacheDir"));
        } else if (aVar == null) {
            throw new IllegalArgumentException(String.format("\"%s\" argument must be not null", "fileNameGenerator"));
        } else {
            this.f460a = file;
            this.b = aVar;
        }
    }

    public File a(String str) {
        return new File(this.f460a, this.b.generate(str));
    }

    public void a() {
        File[] listFiles = this.f460a.listFiles();
        if (listFiles != null) {
            for (File delete : listFiles) {
                delete.delete();
            }
        }
    }
}
