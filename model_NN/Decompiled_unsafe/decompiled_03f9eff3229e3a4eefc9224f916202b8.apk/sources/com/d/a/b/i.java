package com.d.a.b;

import android.graphics.Bitmap;
import android.os.Handler;
import com.d.a.b.a.c;
import com.d.a.b.a.e;
import com.d.a.b.a.f;
import com.d.a.b.a.g;
import com.d.a.b.a.h;
import com.d.a.b.a.o;
import com.d.a.b.c;
import com.d.a.b.d.b;
import com.d.a.c.b;
import com.d.a.c.c;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;

/* compiled from: LoadAndDisplayImageTask */
final class i implements b.a, Runnable {

    /* renamed from: a  reason: collision with root package name */
    final String f504a;
    final com.d.a.b.e.a b;
    final c c;
    final e d;
    final f e;
    private final f f;
    private final h g;
    private final Handler h;
    /* access modifiers changed from: private */
    public final e i;
    private final com.d.a.b.d.b j;
    private final com.d.a.b.d.b k;
    private final com.d.a.b.d.b l;
    private final com.d.a.b.b.b m;
    private final boolean n;
    private final String o;
    private final h p;
    private com.d.a.b.a.i q = com.d.a.b.a.i.NETWORK;

    public i(f fVar, h hVar, Handler handler) {
        this.f = fVar;
        this.g = hVar;
        this.h = handler;
        this.i = fVar.f501a;
        this.j = this.i.r;
        this.k = this.i.w;
        this.l = this.i.x;
        this.m = this.i.s;
        this.n = this.i.u;
        this.f504a = hVar.f503a;
        this.o = hVar.b;
        this.b = hVar.c;
        this.p = hVar.d;
        this.c = hVar.e;
        this.d = hVar.f;
        this.e = hVar.g;
    }

    public void run() {
        if (!b() && !c()) {
            ReentrantLock reentrantLock = this.g.h;
            b("Start display image task [%s]");
            if (reentrantLock.isLocked()) {
                b("Image already is loading. Waiting... [%s]");
            }
            reentrantLock.lock();
            try {
                h();
                Bitmap a2 = this.i.p.a(this.o);
                if (a2 == null) {
                    a2 = d();
                    if (a2 != null) {
                        h();
                        n();
                        if (this.c.d()) {
                            b("PreProcess image before caching in memory [%s]");
                            a2 = this.c.o().a(a2);
                            if (a2 == null) {
                                c.d("Pre-processor returned null [%s]", this.o);
                            }
                        }
                        if (a2 != null && this.c.h()) {
                            b("Cache image in memory [%s]");
                            this.i.p.a(this.o, a2);
                        }
                    } else {
                        return;
                    }
                } else {
                    this.q = com.d.a.b.a.i.MEMORY_CACHE;
                    b("...Get cached bitmap from memory after waiting. [%s]");
                }
                if (a2 != null && this.c.e()) {
                    b("PostProcess image before displaying [%s]");
                    a2 = this.c.p().a(a2);
                    if (a2 == null) {
                        c.d("Post-processor returned null [%s]", this.o);
                    }
                }
                h();
                n();
                reentrantLock.unlock();
                b bVar = new b(a2, this.g, this.f, this.q);
                bVar.a(this.n);
                if (this.c.s()) {
                    bVar.run();
                } else {
                    this.h.post(bVar);
                }
            } catch (a e2) {
                f();
            } finally {
                reentrantLock.unlock();
            }
        }
    }

    private boolean b() {
        AtomicBoolean c2 = this.f.c();
        if (c2.get()) {
            synchronized (this.f.d()) {
                if (c2.get()) {
                    b("ImageLoader is paused. Waiting...  [%s]");
                    try {
                        this.f.d().wait();
                        b(".. Resume loading [%s]");
                    } catch (InterruptedException e2) {
                        c.d("Task was interrupted [%s]", this.o);
                        return true;
                    }
                }
            }
        }
        return i();
    }

    private boolean c() {
        if (!this.c.f()) {
            return false;
        }
        a("Delay %d ms before loading...  [%s]", Integer.valueOf(this.c.l()), this.o);
        try {
            Thread.sleep((long) this.c.l());
            return i();
        } catch (InterruptedException e2) {
            c.d("Task was interrupted [%s]", this.o);
            return true;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0031, code lost:
        if (r0.getHeight() > 0) goto L_0x0065;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x006a, code lost:
        r0 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0071, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0072, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0073, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0074, code lost:
        r5 = r0;
        r0 = null;
        r1 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0085, code lost:
        r3.delete();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0089, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x008a, code lost:
        r5 = r0;
        r0 = null;
        r1 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0096, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0097, code lost:
        r5 = r0;
        r0 = null;
        r1 = r5;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0071 A[ExcHandler: a (r0v9 'e' com.d.a.b.i$a A[CUSTOM_DECLARE]), Splitter:B:1:0x0005] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0085  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private android.graphics.Bitmap d() throws com.d.a.b.i.a {
        /*
            r6 = this;
            r1 = 0
            java.io.File r3 = r6.e()
            com.d.a.b.d.b$a r0 = com.d.a.b.d.b.a.FILE     // Catch:{ IllegalStateException -> 0x0069, a -> 0x0071, IOException -> 0x0073, OutOfMemoryError -> 0x0089, Throwable -> 0x0096 }
            java.lang.String r2 = r3.getAbsolutePath()     // Catch:{ IllegalStateException -> 0x0069, a -> 0x0071, IOException -> 0x0073, OutOfMemoryError -> 0x0089, Throwable -> 0x0096 }
            java.lang.String r2 = r0.b(r2)     // Catch:{ IllegalStateException -> 0x0069, a -> 0x0071, IOException -> 0x0073, OutOfMemoryError -> 0x0089, Throwable -> 0x0096 }
            boolean r0 = r3.exists()     // Catch:{ IllegalStateException -> 0x0069, a -> 0x0071, IOException -> 0x0073, OutOfMemoryError -> 0x0089, Throwable -> 0x0096 }
            if (r0 == 0) goto L_0x00ab
            java.lang.String r0 = "Load image from disc cache [%s]"
            r6.b(r0)     // Catch:{ IllegalStateException -> 0x0069, a -> 0x0071, IOException -> 0x0073, OutOfMemoryError -> 0x0089, Throwable -> 0x0096 }
            com.d.a.b.a.i r0 = com.d.a.b.a.i.DISC_CACHE     // Catch:{ IllegalStateException -> 0x0069, a -> 0x0071, IOException -> 0x0073, OutOfMemoryError -> 0x0089, Throwable -> 0x0096 }
            r6.q = r0     // Catch:{ IllegalStateException -> 0x0069, a -> 0x0071, IOException -> 0x0073, OutOfMemoryError -> 0x0089, Throwable -> 0x0096 }
            r6.h()     // Catch:{ IllegalStateException -> 0x0069, a -> 0x0071, IOException -> 0x0073, OutOfMemoryError -> 0x0089, Throwable -> 0x0096 }
            android.graphics.Bitmap r0 = r6.a(r2)     // Catch:{ IllegalStateException -> 0x0069, a -> 0x0071, IOException -> 0x0073, OutOfMemoryError -> 0x0089, Throwable -> 0x0096 }
        L_0x0025:
            if (r0 == 0) goto L_0x0033
            int r4 = r0.getWidth()     // Catch:{ IllegalStateException -> 0x00a9, a -> 0x0071, IOException -> 0x00a7, OutOfMemoryError -> 0x00a5, Throwable -> 0x00a3 }
            if (r4 <= 0) goto L_0x0033
            int r4 = r0.getHeight()     // Catch:{ IllegalStateException -> 0x00a9, a -> 0x0071, IOException -> 0x00a7, OutOfMemoryError -> 0x00a5, Throwable -> 0x00a3 }
            if (r4 > 0) goto L_0x0065
        L_0x0033:
            java.lang.String r4 = "Load image from network [%s]"
            r6.b(r4)     // Catch:{ IllegalStateException -> 0x00a9, a -> 0x0071, IOException -> 0x00a7, OutOfMemoryError -> 0x00a5, Throwable -> 0x00a3 }
            com.d.a.b.a.i r4 = com.d.a.b.a.i.NETWORK     // Catch:{ IllegalStateException -> 0x00a9, a -> 0x0071, IOException -> 0x00a7, OutOfMemoryError -> 0x00a5, Throwable -> 0x00a3 }
            r6.q = r4     // Catch:{ IllegalStateException -> 0x00a9, a -> 0x0071, IOException -> 0x00a7, OutOfMemoryError -> 0x00a5, Throwable -> 0x00a3 }
            com.d.a.b.c r4 = r6.c     // Catch:{ IllegalStateException -> 0x00a9, a -> 0x0071, IOException -> 0x00a7, OutOfMemoryError -> 0x00a5, Throwable -> 0x00a3 }
            boolean r4 = r4.i()     // Catch:{ IllegalStateException -> 0x00a9, a -> 0x0071, IOException -> 0x00a7, OutOfMemoryError -> 0x00a5, Throwable -> 0x00a3 }
            if (r4 == 0) goto L_0x0066
            boolean r4 = r6.a(r3)     // Catch:{ IllegalStateException -> 0x00a9, a -> 0x0071, IOException -> 0x00a7, OutOfMemoryError -> 0x00a5, Throwable -> 0x00a3 }
            if (r4 == 0) goto L_0x0066
        L_0x004a:
            r6.h()     // Catch:{ IllegalStateException -> 0x00a9, a -> 0x0071, IOException -> 0x00a7, OutOfMemoryError -> 0x00a5, Throwable -> 0x00a3 }
            android.graphics.Bitmap r0 = r6.a(r2)     // Catch:{ IllegalStateException -> 0x00a9, a -> 0x0071, IOException -> 0x00a7, OutOfMemoryError -> 0x00a5, Throwable -> 0x00a3 }
            if (r0 == 0) goto L_0x005f
            int r2 = r0.getWidth()     // Catch:{ IllegalStateException -> 0x00a9, a -> 0x0071, IOException -> 0x00a7, OutOfMemoryError -> 0x00a5, Throwable -> 0x00a3 }
            if (r2 <= 0) goto L_0x005f
            int r2 = r0.getHeight()     // Catch:{ IllegalStateException -> 0x00a9, a -> 0x0071, IOException -> 0x00a7, OutOfMemoryError -> 0x00a5, Throwable -> 0x00a3 }
            if (r2 > 0) goto L_0x0065
        L_0x005f:
            com.d.a.b.a.c$a r2 = com.d.a.b.a.c.a.DECODING_ERROR     // Catch:{ IllegalStateException -> 0x00a9, a -> 0x0071, IOException -> 0x00a7, OutOfMemoryError -> 0x00a5, Throwable -> 0x00a3 }
            r4 = 0
            r6.a(r2, r4)     // Catch:{ IllegalStateException -> 0x00a9, a -> 0x0071, IOException -> 0x00a7, OutOfMemoryError -> 0x00a5, Throwable -> 0x00a3 }
        L_0x0065:
            return r0
        L_0x0066:
            java.lang.String r2 = r6.f504a     // Catch:{ IllegalStateException -> 0x00a9, a -> 0x0071, IOException -> 0x00a7, OutOfMemoryError -> 0x00a5, Throwable -> 0x00a3 }
            goto L_0x004a
        L_0x0069:
            r0 = move-exception
            r0 = r1
        L_0x006b:
            com.d.a.b.a.c$a r2 = com.d.a.b.a.c.a.NETWORK_DENIED
            r6.a(r2, r1)
            goto L_0x0065
        L_0x0071:
            r0 = move-exception
            throw r0
        L_0x0073:
            r0 = move-exception
            r5 = r0
            r0 = r1
            r1 = r5
        L_0x0077:
            com.d.a.c.c.a(r1)
            com.d.a.b.a.c$a r2 = com.d.a.b.a.c.a.IO_ERROR
            r6.a(r2, r1)
            boolean r1 = r3.exists()
            if (r1 == 0) goto L_0x0065
            r3.delete()
            goto L_0x0065
        L_0x0089:
            r0 = move-exception
            r5 = r0
            r0 = r1
            r1 = r5
        L_0x008d:
            com.d.a.c.c.a(r1)
            com.d.a.b.a.c$a r2 = com.d.a.b.a.c.a.OUT_OF_MEMORY
            r6.a(r2, r1)
            goto L_0x0065
        L_0x0096:
            r0 = move-exception
            r5 = r0
            r0 = r1
            r1 = r5
        L_0x009a:
            com.d.a.c.c.a(r1)
            com.d.a.b.a.c$a r2 = com.d.a.b.a.c.a.UNKNOWN
            r6.a(r2, r1)
            goto L_0x0065
        L_0x00a3:
            r1 = move-exception
            goto L_0x009a
        L_0x00a5:
            r1 = move-exception
            goto L_0x008d
        L_0x00a7:
            r1 = move-exception
            goto L_0x0077
        L_0x00a9:
            r2 = move-exception
            goto L_0x006b
        L_0x00ab:
            r0 = r1
            goto L_0x0025
        */
        throw new UnsupportedOperationException("Method not decompiled: com.d.a.b.i.d():android.graphics.Bitmap");
    }

    private File e() {
        File parentFile;
        File a2 = this.i.q.a(this.f504a);
        File parentFile2 = a2.getParentFile();
        if ((parentFile2 == null || (!parentFile2.exists() && !parentFile2.mkdirs())) && (parentFile = (a2 = this.i.v.a(this.f504a)).getParentFile()) != null && !parentFile.exists()) {
            parentFile.mkdirs();
        }
        return a2;
    }

    private Bitmap a(String str) throws IOException {
        String str2 = str;
        return this.m.a(new com.d.a.b.b.c(this.o, str2, this.p, this.b.c(), g(), this.c));
    }

    private boolean a(File file) throws a {
        IOException e2;
        boolean z;
        b("Cache image on disc [%s]");
        try {
            z = b(file);
            if (z) {
                try {
                    int i2 = this.i.d;
                    int i3 = this.i.e;
                    if (i2 > 0 || i3 > 0) {
                        b("Resize image in disc cache [%s]");
                        z = a(file, i2, i3);
                    }
                    this.i.q.a(this.f504a, file);
                } catch (IOException e3) {
                    e2 = e3;
                    c.a(e2);
                    if (file.exists()) {
                        file.delete();
                    }
                    return z;
                }
            }
        } catch (IOException e4) {
            IOException iOException = e4;
            z = false;
            e2 = iOException;
        }
        return z;
    }

    private boolean b(File file) throws IOException {
        BufferedOutputStream a2 = g().a(this.f504a, this.c.n());
        try {
            a2 = new BufferedOutputStream(new FileOutputStream(file), 32768);
            return b.a(a2, a2, this);
        } catch (Throwable th) {
            throw th;
        } finally {
            b.a(a2);
        }
    }

    /* JADX INFO: finally extract failed */
    private boolean a(File file, int i2, int i3) throws IOException {
        Bitmap a2 = this.m.a(new com.d.a.b.b.c(this.o, b.a.FILE.b(file.getAbsolutePath()), new h(i2, i3), o.FIT_INSIDE, g(), new c.a().a(this.c).a(g.IN_SAMPLE_INT).c()));
        if (!(a2 == null || this.i.h == null)) {
            b("Process image before cache on disc [%s]");
            a2 = this.i.h.a(a2);
            if (a2 == null) {
                com.d.a.c.c.d("Bitmap processor for disc cache returned null [%s]", this.o);
            }
        }
        if (a2 != null) {
            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(file), 32768);
            try {
                a2.compress(this.i.f, this.i.g, bufferedOutputStream);
                com.d.a.c.b.a(bufferedOutputStream);
                a2.recycle();
            } catch (Throwable th) {
                com.d.a.c.b.a(bufferedOutputStream);
                throw th;
            }
        }
        return true;
    }

    public boolean a(int i2, int i3) {
        return this.e == null || b(i2, i3);
    }

    private boolean b(int i2, int i3) {
        if (this.c.s() || o() || i()) {
            return false;
        }
        this.h.post(new j(this, i2, i3));
        return true;
    }

    private void a(c.a aVar, Throwable th) {
        if (!this.c.s() && !o() && !i()) {
            this.h.post(new k(this, aVar, th));
        }
    }

    private void f() {
        if (!this.c.s() && !o()) {
            this.h.post(new l(this));
        }
    }

    private com.d.a.b.d.b g() {
        if (this.f.e()) {
            return this.k;
        }
        if (this.f.f()) {
            return this.l;
        }
        return this.j;
    }

    private void h() throws a {
        j();
        l();
    }

    private boolean i() {
        return k() || m();
    }

    private void j() throws a {
        if (k()) {
            throw new a();
        }
    }

    private boolean k() {
        if (!this.b.e()) {
            return false;
        }
        b("ImageAware was collected by GC. Task is cancelled. [%s]");
        return true;
    }

    private void l() throws a {
        if (m()) {
            throw new a();
        }
    }

    private boolean m() {
        if (!(!this.o.equals(this.f.a(this.b)))) {
            return false;
        }
        b("ImageAware is reused for another image. Task is cancelled. [%s]");
        return true;
    }

    private void n() throws a {
        if (o()) {
            throw new a();
        }
    }

    private boolean o() {
        if (!Thread.interrupted()) {
            return false;
        }
        b("Task was interrupted [%s]");
        return true;
    }

    /* access modifiers changed from: package-private */
    public String a() {
        return this.f504a;
    }

    private void b(String str) {
        if (this.n) {
            com.d.a.c.c.a(str, this.o);
        }
    }

    private void a(String str, Object... objArr) {
        if (this.n) {
            com.d.a.c.c.a(str, objArr);
        }
    }

    /* compiled from: LoadAndDisplayImageTask */
    class a extends Exception {
        a() {
        }
    }
}
