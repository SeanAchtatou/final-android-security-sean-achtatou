package com.d.a.b;

/* compiled from: ImageLoaderEngine */
class g implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ i f502a;
    final /* synthetic */ f b;

    g(f fVar, i iVar) {
        this.b = fVar;
        this.f502a = iVar;
    }

    public void run() {
        boolean exists = this.b.f501a.q.a(this.f502a.a()).exists();
        this.b.g();
        if (exists) {
            this.b.c.execute(this.f502a);
        } else {
            this.b.b.execute(this.f502a);
        }
    }
}
