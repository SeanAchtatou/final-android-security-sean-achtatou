package com.d.a.b.a;

import android.graphics.Bitmap;
import com.d.a.a.b.b;
import java.util.ArrayList;
import java.util.Comparator;

/* compiled from: MemoryCacheUtil */
public final class j {
    public static String a(String str, h hVar) {
        return str + "_" + hVar.a() + "x" + hVar.b();
    }

    public static Comparator<String> a() {
        return new k();
    }

    public static void a(String str, b<String, Bitmap> bVar) {
        ArrayList<String> arrayList = new ArrayList<>();
        for (String next : bVar.a()) {
            if (next.startsWith(str)) {
                arrayList.add(next);
            }
        }
        for (String b : arrayList) {
            bVar.b(b);
        }
    }
}
