package com.d.a.b;

import android.content.Context;
import android.graphics.Bitmap;
import com.d.a.a.a.b.b;
import com.d.a.b.a.a.c;
import com.d.a.b.a.m;
import com.d.a.c.d;
import java.io.File;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/* compiled from: DefaultConfigurationFactory */
public class a {
    public static Executor a(int i, int i2, m mVar) {
        return new ThreadPoolExecutor(i, i, 0, TimeUnit.MILLISECONDS, mVar == m.LIFO ? new c() : new LinkedBlockingQueue(), b(i2));
    }

    public static com.d.a.a.a.b.a a() {
        return new b();
    }

    public static com.d.a.a.a.b a(Context context, com.d.a.a.a.b.a aVar, int i, int i2) {
        if (i > 0) {
            return new com.d.a.a.a.a.c(d.b(context), aVar, i);
        }
        if (i2 > 0) {
            return new com.d.a.a.a.a.a(d.b(context), aVar, i2);
        }
        return new com.d.a.a.a.a.d(d.a(context), aVar);
    }

    public static com.d.a.a.a.b a(File file) {
        File file2 = new File(file, "uil-images");
        if (file2.exists() || file2.mkdir()) {
            file = file2;
        }
        return new com.d.a.a.a.a.c(file, 2097152);
    }

    public static com.d.a.a.b.b<String, Bitmap> a(int i) {
        if (i == 0) {
            i = (int) (Runtime.getRuntime().maxMemory() / 8);
        }
        return new com.d.a.a.b.a.b(i);
    }

    public static com.d.a.b.d.b a(Context context) {
        return new com.d.a.b.d.a(context);
    }

    public static com.d.a.b.b.b a(boolean z) {
        return new com.d.a.b.b.a(z);
    }

    public static com.d.a.b.c.a b() {
        return new com.d.a.b.c.d();
    }

    private static ThreadFactory b(int i) {
        return new C0009a(i);
    }

    /* renamed from: com.d.a.b.a$a  reason: collision with other inner class name */
    /* compiled from: DefaultConfigurationFactory */
    private static class C0009a implements ThreadFactory {

        /* renamed from: a  reason: collision with root package name */
        private static final AtomicInteger f465a = new AtomicInteger(1);
        private final ThreadGroup b;
        private final AtomicInteger c = new AtomicInteger(1);
        private final String d;
        private final int e;

        C0009a(int i) {
            this.e = i;
            SecurityManager securityManager = System.getSecurityManager();
            this.b = securityManager != null ? securityManager.getThreadGroup() : Thread.currentThread().getThreadGroup();
            this.d = "uil-pool-" + f465a.getAndIncrement() + "-thread-";
        }

        public Thread newThread(Runnable runnable) {
            Thread thread = new Thread(this.b, runnable, this.d + this.c.getAndIncrement(), 0);
            if (thread.isDaemon()) {
                thread.setDaemon(false);
            }
            thread.setPriority(this.e);
            return thread;
        }
    }
}
