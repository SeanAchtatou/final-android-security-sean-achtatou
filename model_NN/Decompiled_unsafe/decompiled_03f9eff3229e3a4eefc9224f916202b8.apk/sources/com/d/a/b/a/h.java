package com.d.a.b.a;

/* compiled from: ImageSize */
public class h {

    /* renamed from: a  reason: collision with root package name */
    private final int f473a;
    private final int b;

    public h(int i, int i2) {
        this.f473a = i;
        this.b = i2;
    }

    public h(int i, int i2, int i3) {
        if (i3 % 180 == 0) {
            this.f473a = i;
            this.b = i2;
            return;
        }
        this.f473a = i2;
        this.b = i;
    }

    public int a() {
        return this.f473a;
    }

    public int b() {
        return this.b;
    }

    public h a(int i) {
        return new h(this.f473a / i, this.b / i);
    }

    public h a(float f) {
        return new h((int) (((float) this.f473a) * f), (int) (((float) this.b) * f));
    }

    public String toString() {
        return new StringBuilder(9).append(this.f473a).append("x").append(this.b).toString();
    }
}
