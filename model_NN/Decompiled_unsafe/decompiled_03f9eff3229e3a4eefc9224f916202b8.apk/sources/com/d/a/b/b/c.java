package com.d.a.b.b;

import android.annotation.TargetApi;
import android.graphics.BitmapFactory;
import android.os.Build;
import com.d.a.b.a.g;
import com.d.a.b.a.h;
import com.d.a.b.a.o;
import com.d.a.b.d.b;

/* compiled from: ImageDecodingInfo */
public class c {

    /* renamed from: a  reason: collision with root package name */
    private final String f483a;
    private final String b;
    private final h c;
    private final g d;
    private final o e;
    private final b f;
    private final Object g;
    private final boolean h;
    private final BitmapFactory.Options i = new BitmapFactory.Options();

    public c(String str, String str2, h hVar, o oVar, b bVar, com.d.a.b.c cVar) {
        this.f483a = str;
        this.b = str2;
        this.c = hVar;
        this.d = cVar.j();
        this.e = oVar;
        this.f = bVar;
        this.g = cVar.n();
        this.h = cVar.m();
        a(cVar.k(), this.i);
    }

    private void a(BitmapFactory.Options options, BitmapFactory.Options options2) {
        options2.inDensity = options.inDensity;
        options2.inDither = options.inDither;
        options2.inInputShareable = options.inInputShareable;
        options2.inJustDecodeBounds = options.inJustDecodeBounds;
        options2.inPreferredConfig = options.inPreferredConfig;
        options2.inPurgeable = options.inPurgeable;
        options2.inSampleSize = options.inSampleSize;
        options2.inScaled = options.inScaled;
        options2.inScreenDensity = options.inScreenDensity;
        options2.inTargetDensity = options.inTargetDensity;
        options2.inTempStorage = options.inTempStorage;
        if (Build.VERSION.SDK_INT >= 10) {
            b(options, options2);
        }
        if (Build.VERSION.SDK_INT >= 11) {
            c(options, options2);
        }
    }

    @TargetApi(10)
    private void b(BitmapFactory.Options options, BitmapFactory.Options options2) {
        options2.inPreferQualityOverSpeed = options.inPreferQualityOverSpeed;
    }

    @TargetApi(11)
    private void c(BitmapFactory.Options options, BitmapFactory.Options options2) {
        options2.inBitmap = options.inBitmap;
        options2.inMutable = options.inMutable;
    }

    public String a() {
        return this.f483a;
    }

    public String b() {
        return this.b;
    }

    public h c() {
        return this.c;
    }

    public g d() {
        return this.d;
    }

    public o e() {
        return this.e;
    }

    public b f() {
        return this.f;
    }

    public Object g() {
        return this.g;
    }

    public boolean h() {
        return this.h;
    }

    public BitmapFactory.Options i() {
        return this.i;
    }
}
