package com.d.a.b.b;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.Build;
import com.d.a.b.a.g;
import com.d.a.b.a.h;
import com.d.a.b.d.b;
import com.d.a.c.c;
import java.io.IOException;
import java.io.InputStream;

/* compiled from: BaseImageDecoder */
public class a implements b {

    /* renamed from: a  reason: collision with root package name */
    protected final boolean f480a;

    public a(boolean z) {
        this.f480a = z;
    }

    public Bitmap a(c cVar) throws IOException {
        InputStream b2 = b(cVar);
        try {
            b a2 = a(b2, cVar);
            b2 = b(b2, cVar);
            Bitmap decodeStream = BitmapFactory.decodeStream(b2, null, a(a2.f482a, cVar));
            if (decodeStream != null) {
                return a(decodeStream, cVar, a2.b.f481a, a2.b.b);
            }
            c.d("Image can't be decoded [%s]", cVar.a());
            return decodeStream;
        } finally {
            com.d.a.c.b.a(b2);
        }
    }

    /* access modifiers changed from: protected */
    public InputStream b(c cVar) throws IOException {
        return cVar.f().a(cVar.b(), cVar.g());
    }

    /* access modifiers changed from: protected */
    public b a(InputStream inputStream, c cVar) throws IOException {
        C0010a aVar;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(inputStream, null, options);
        String b2 = cVar.b();
        if (!cVar.h() || !a(b2, options.outMimeType)) {
            aVar = new C0010a();
        } else {
            aVar = a(b2);
        }
        return new b(new h(options.outWidth, options.outHeight, aVar.f481a), aVar);
    }

    private boolean a(String str, String str2) {
        return Build.VERSION.SDK_INT >= 5 && "image/jpeg".equalsIgnoreCase(str2) && b.a.a(str) == b.a.FILE;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    public C0010a a(String str) {
        int i = 0;
        boolean z = true;
        try {
            switch (new ExifInterface(b.a.FILE.c(str)).getAttributeInt("Orientation", 1)) {
                case 1:
                default:
                    z = false;
                    break;
                case 2:
                    break;
                case 3:
                    z = false;
                    i = 180;
                    break;
                case 4:
                    i = 180;
                    break;
                case 5:
                    i = 270;
                    break;
                case 6:
                    z = false;
                    i = 90;
                    break;
                case 7:
                    i = 90;
                    break;
                case 8:
                    z = false;
                    i = 270;
                    break;
            }
        } catch (IOException e) {
            c.c("Can't read EXIF tags from file [%s]", str);
        }
        return new C0010a(i, z);
    }

    /* access modifiers changed from: protected */
    public BitmapFactory.Options a(h hVar, c cVar) {
        boolean z;
        int a2;
        g d = cVar.d();
        if (d == g.NONE) {
            a2 = com.d.a.c.a.a(hVar);
        } else {
            h c = cVar.c();
            if (d == g.IN_SAMPLE_POWER_OF_2) {
                z = true;
            } else {
                z = false;
            }
            a2 = com.d.a.c.a.a(hVar, c, cVar.e(), z);
        }
        if (a2 > 1 && this.f480a) {
            c.a("Subsample original image (%1$s) to %2$s (scale = %3$d) [%4$s]", hVar, hVar.a(a2), Integer.valueOf(a2), cVar.a());
        }
        BitmapFactory.Options i = cVar.i();
        i.inSampleSize = a2;
        return i;
    }

    /* access modifiers changed from: protected */
    public InputStream b(InputStream inputStream, c cVar) throws IOException {
        try {
            inputStream.reset();
            return inputStream;
        } catch (IOException e) {
            com.d.a.c.b.a(inputStream);
            return b(cVar);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    /* access modifiers changed from: protected */
    public Bitmap a(Bitmap bitmap, c cVar, int i, boolean z) {
        Matrix matrix = new Matrix();
        g d = cVar.d();
        if (d == g.EXACTLY || d == g.EXACTLY_STRETCHED) {
            h hVar = new h(bitmap.getWidth(), bitmap.getHeight(), i);
            float b2 = com.d.a.c.a.b(hVar, cVar.c(), cVar.e(), d == g.EXACTLY_STRETCHED);
            if (Float.compare(b2, 1.0f) != 0) {
                matrix.setScale(b2, b2);
                if (this.f480a) {
                    c.a("Scale subsampled image (%1$s) to %2$s (scale = %3$.5f) [%4$s]", hVar, hVar.a(b2), Float.valueOf(b2), cVar.a());
                }
            }
        }
        if (z) {
            matrix.postScale(-1.0f, 1.0f);
            if (this.f480a) {
                c.a("Flip image horizontally [%s]", cVar.a());
            }
        }
        if (i != 0) {
            matrix.postRotate((float) i);
            if (this.f480a) {
                c.a("Rotate image on %1$d° [%2$s]", Integer.valueOf(i), cVar.a());
            }
        }
        Bitmap createBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        if (createBitmap != bitmap) {
            bitmap.recycle();
        }
        return createBitmap;
    }

    /* renamed from: com.d.a.b.b.a$a  reason: collision with other inner class name */
    /* compiled from: BaseImageDecoder */
    protected static class C0010a {

        /* renamed from: a  reason: collision with root package name */
        public final int f481a;
        public final boolean b;

        protected C0010a() {
            this.f481a = 0;
            this.b = false;
        }

        protected C0010a(int i, boolean z) {
            this.f481a = i;
            this.b = z;
        }
    }

    /* compiled from: BaseImageDecoder */
    protected static class b {

        /* renamed from: a  reason: collision with root package name */
        public final h f482a;
        public final C0010a b;

        protected b(h hVar, C0010a aVar) {
            this.f482a = hVar;
            this.b = aVar;
        }
    }
}
