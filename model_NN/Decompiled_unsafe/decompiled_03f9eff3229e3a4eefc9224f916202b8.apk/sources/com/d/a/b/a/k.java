package com.d.a.b.a;

import java.util.Comparator;

/* compiled from: MemoryCacheUtil */
final class k implements Comparator<String> {
    k() {
    }

    /* renamed from: a */
    public int compare(String str, String str2) {
        return str.substring(0, str.lastIndexOf("_")).compareTo(str2.substring(0, str2.lastIndexOf("_")));
    }
}
