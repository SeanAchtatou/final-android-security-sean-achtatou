package com.d.a.b.a;

import java.io.File;

/* compiled from: DiscCacheUtil */
public final class b {
    public static File a(String str, com.d.a.a.a.b bVar) {
        File a2 = bVar.a(str);
        if (a2.exists()) {
            return a2;
        }
        return null;
    }

    public static boolean b(String str, com.d.a.a.a.b bVar) {
        return bVar.a(str).delete();
    }
}
