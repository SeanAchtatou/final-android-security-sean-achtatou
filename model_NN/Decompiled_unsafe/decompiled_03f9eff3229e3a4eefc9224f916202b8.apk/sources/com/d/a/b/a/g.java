package com.d.a.b.a;

/* compiled from: ImageScaleType */
public enum g {
    NONE,
    IN_SAMPLE_POWER_OF_2,
    IN_SAMPLE_INT,
    EXACTLY,
    EXACTLY_STRETCHED
}
