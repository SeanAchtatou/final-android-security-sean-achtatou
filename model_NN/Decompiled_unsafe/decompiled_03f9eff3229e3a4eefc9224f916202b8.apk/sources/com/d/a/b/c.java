package com.d.a.b;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Looper;
import com.d.a.b.a.g;
import com.d.a.b.c.a;

/* compiled from: DisplayImageOptions */
public final class c {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final int f484a;
    /* access modifiers changed from: private */
    public final int b;
    /* access modifiers changed from: private */
    public final int c;
    /* access modifiers changed from: private */
    public final Drawable d;
    /* access modifiers changed from: private */
    public final Drawable e;
    /* access modifiers changed from: private */
    public final Drawable f;
    /* access modifiers changed from: private */
    public final boolean g;
    /* access modifiers changed from: private */
    public final boolean h;
    /* access modifiers changed from: private */
    public final boolean i;
    /* access modifiers changed from: private */
    public final g j;
    /* access modifiers changed from: private */
    public final BitmapFactory.Options k;
    /* access modifiers changed from: private */
    public final int l;
    /* access modifiers changed from: private */
    public final boolean m;
    /* access modifiers changed from: private */
    public final Object n;
    /* access modifiers changed from: private */
    public final com.d.a.b.f.a o;
    /* access modifiers changed from: private */
    public final com.d.a.b.f.a p;
    /* access modifiers changed from: private */
    public final a q;
    /* access modifiers changed from: private */
    public final Handler r;
    /* access modifiers changed from: private */
    public final boolean s;

    private c(a aVar) {
        this.f484a = aVar.f485a;
        this.b = aVar.b;
        this.c = aVar.c;
        this.d = aVar.d;
        this.e = aVar.e;
        this.f = aVar.f;
        this.g = aVar.g;
        this.h = aVar.h;
        this.i = aVar.i;
        this.j = aVar.j;
        this.k = aVar.k;
        this.l = aVar.l;
        this.m = aVar.m;
        this.n = aVar.n;
        this.o = aVar.o;
        this.p = aVar.p;
        this.q = aVar.q;
        this.r = aVar.r;
        this.s = aVar.s;
    }

    public boolean a() {
        return (this.d == null && this.f484a == 0) ? false : true;
    }

    public boolean b() {
        return (this.e == null && this.b == 0) ? false : true;
    }

    public boolean c() {
        return (this.f == null && this.c == 0) ? false : true;
    }

    public boolean d() {
        return this.o != null;
    }

    public boolean e() {
        return this.p != null;
    }

    public boolean f() {
        return this.l > 0;
    }

    public Drawable a(Resources resources) {
        return this.f484a != 0 ? resources.getDrawable(this.f484a) : this.d;
    }

    public Drawable b(Resources resources) {
        return this.b != 0 ? resources.getDrawable(this.b) : this.e;
    }

    public Drawable c(Resources resources) {
        return this.c != 0 ? resources.getDrawable(this.c) : this.f;
    }

    public boolean g() {
        return this.g;
    }

    public boolean h() {
        return this.h;
    }

    public boolean i() {
        return this.i;
    }

    public g j() {
        return this.j;
    }

    public BitmapFactory.Options k() {
        return this.k;
    }

    public int l() {
        return this.l;
    }

    public boolean m() {
        return this.m;
    }

    public Object n() {
        return this.n;
    }

    public com.d.a.b.f.a o() {
        return this.o;
    }

    public com.d.a.b.f.a p() {
        return this.p;
    }

    public a q() {
        return this.q;
    }

    public Handler r() {
        if (this.s) {
            return null;
        }
        if (this.r != null) {
            return this.r;
        }
        if (Looper.myLooper() == Looper.getMainLooper()) {
            return new Handler();
        }
        throw new IllegalStateException("ImageLoader.displayImage(...) must be invoked from the main thread or from Looper thread");
    }

    /* access modifiers changed from: package-private */
    public boolean s() {
        return this.s;
    }

    /* compiled from: DisplayImageOptions */
    public static class a {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public int f485a = 0;
        /* access modifiers changed from: private */
        public int b = 0;
        /* access modifiers changed from: private */
        public int c = 0;
        /* access modifiers changed from: private */
        public Drawable d = null;
        /* access modifiers changed from: private */
        public Drawable e = null;
        /* access modifiers changed from: private */
        public Drawable f = null;
        /* access modifiers changed from: private */
        public boolean g = false;
        /* access modifiers changed from: private */
        public boolean h = false;
        /* access modifiers changed from: private */
        public boolean i = false;
        /* access modifiers changed from: private */
        public g j = g.IN_SAMPLE_POWER_OF_2;
        /* access modifiers changed from: private */
        public BitmapFactory.Options k = new BitmapFactory.Options();
        /* access modifiers changed from: private */
        public int l = 0;
        /* access modifiers changed from: private */
        public boolean m = false;
        /* access modifiers changed from: private */
        public Object n = null;
        /* access modifiers changed from: private */
        public com.d.a.b.f.a o = null;
        /* access modifiers changed from: private */
        public com.d.a.b.f.a p = null;
        /* access modifiers changed from: private */
        public com.d.a.b.c.a q = a.b();
        /* access modifiers changed from: private */
        public Handler r = null;
        /* access modifiers changed from: private */
        public boolean s = false;

        public a() {
            this.k.inPurgeable = true;
            this.k.inInputShareable = true;
        }

        @Deprecated
        public a a(int i2) {
            this.f485a = i2;
            return this;
        }

        public a b(int i2) {
            this.f485a = i2;
            return this;
        }

        public a c(int i2) {
            this.b = i2;
            return this;
        }

        public a d(int i2) {
            this.c = i2;
            return this;
        }

        public a a() {
            this.h = true;
            return this;
        }

        public a a(boolean z) {
            this.h = z;
            return this;
        }

        public a b() {
            this.i = true;
            return this;
        }

        public a b(boolean z) {
            this.i = z;
            return this;
        }

        public a a(g gVar) {
            this.j = gVar;
            return this;
        }

        public a a(Bitmap.Config config) {
            if (config == null) {
                throw new IllegalArgumentException("bitmapConfig can't be null");
            }
            this.k.inPreferredConfig = config;
            return this;
        }

        public a c(boolean z) {
            this.m = z;
            return this;
        }

        public a a(com.d.a.b.c.a aVar) {
            if (aVar == null) {
                throw new IllegalArgumentException("displayer can't be null");
            }
            this.q = aVar;
            return this;
        }

        public a a(c cVar) {
            this.f485a = cVar.f484a;
            this.b = cVar.b;
            this.c = cVar.c;
            this.d = cVar.d;
            this.e = cVar.e;
            this.f = cVar.f;
            this.g = cVar.g;
            this.h = cVar.h;
            this.i = cVar.i;
            this.j = cVar.j;
            this.k = cVar.k;
            this.l = cVar.l;
            this.m = cVar.m;
            this.n = cVar.n;
            this.o = cVar.o;
            this.p = cVar.p;
            this.q = cVar.q;
            this.r = cVar.r;
            this.s = cVar.s;
            return this;
        }

        public c c() {
            return new c(this);
        }
    }

    public static c t() {
        return new a().c();
    }
}
