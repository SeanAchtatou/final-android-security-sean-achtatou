package com.d.a.b;

import android.graphics.Bitmap;
import android.os.Handler;
import com.d.a.b.a.i;
import com.d.a.c.c;

/* compiled from: ProcessAndDisplayImageTask */
class m implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final f f509a;
    private final Bitmap b;
    private final h c;
    private final Handler d;

    public m(f fVar, Bitmap bitmap, h hVar, Handler handler) {
        this.f509a = fVar;
        this.b = bitmap;
        this.c = hVar;
        this.d = handler;
    }

    public void run() {
        if (this.f509a.f501a.u) {
            c.a("PostProcess image before displaying [%s]", this.c.b);
        }
        b bVar = new b(this.c.e.p().a(this.b), this.c, this.f509a, i.MEMORY_CACHE);
        bVar.a(this.f509a.f501a.u);
        if (this.c.e.s()) {
            bVar.run();
        } else {
            this.d.post(bVar);
        }
    }
}
