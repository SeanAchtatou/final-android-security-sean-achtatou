package com.d.a.b.a.a;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.AbstractQueue;
import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/* compiled from: LinkedBlockingDeque */
public class d<E> extends AbstractQueue<E> implements a<E>, Serializable {

    /* renamed from: a  reason: collision with root package name */
    transient c<E> f467a;
    transient c<E> b;
    final ReentrantLock c;
    private transient int d;
    private final int e;
    private final Condition f;
    private final Condition g;

    /* compiled from: LinkedBlockingDeque */
    static final class c<E> {

        /* renamed from: a  reason: collision with root package name */
        E f469a;
        c<E> b;
        c<E> c;

        c(E e) {
            this.f469a = e;
        }
    }

    public d() {
        this(Integer.MAX_VALUE);
    }

    public d(int i) {
        this.c = new ReentrantLock();
        this.f = this.c.newCondition();
        this.g = this.c.newCondition();
        if (i <= 0) {
            throw new IllegalArgumentException();
        }
        this.e = i;
    }

    private boolean b(c cVar) {
        if (this.d >= this.e) {
            return false;
        }
        c<E> cVar2 = this.f467a;
        cVar.c = cVar2;
        this.f467a = cVar;
        if (this.b == null) {
            this.b = cVar;
        } else {
            cVar2.b = cVar;
        }
        this.d++;
        this.f.signal();
        return true;
    }

    private boolean c(c<E> cVar) {
        if (this.d >= this.e) {
            return false;
        }
        c<E> cVar2 = this.b;
        cVar.b = cVar2;
        this.b = cVar;
        if (this.f467a == null) {
            this.f467a = cVar;
        } else {
            cVar2.c = cVar;
        }
        this.d++;
        this.f.signal();
        return true;
    }

    private E f() {
        c<E> cVar = this.f467a;
        if (cVar == null) {
            return null;
        }
        c<E> cVar2 = cVar.c;
        E e2 = cVar.f469a;
        cVar.f469a = null;
        cVar.c = cVar;
        this.f467a = cVar2;
        if (cVar2 == null) {
            this.b = null;
        } else {
            cVar2.b = null;
        }
        this.d--;
        this.g.signal();
        return e2;
    }

    private E g() {
        c<E> cVar = this.b;
        if (cVar == null) {
            return null;
        }
        c<E> cVar2 = cVar.b;
        E e2 = cVar.f469a;
        cVar.f469a = null;
        cVar.b = cVar;
        this.b = cVar2;
        if (cVar2 == null) {
            this.f467a = null;
        } else {
            cVar2.c = null;
        }
        this.d--;
        this.g.signal();
        return e2;
    }

    /* access modifiers changed from: package-private */
    public void a(c<E> cVar) {
        c<E> cVar2 = cVar.b;
        c<E> cVar3 = cVar.c;
        if (cVar2 == null) {
            f();
        } else if (cVar3 == null) {
            g();
        } else {
            cVar2.c = cVar3;
            cVar3.b = cVar2;
            cVar.f469a = null;
            this.d--;
            this.g.signal();
        }
    }

    public void a(E e2) {
        if (!c((Object) e2)) {
            throw new IllegalStateException("Deque full");
        }
    }

    public boolean b(Object obj) {
        if (obj == null) {
            throw new NullPointerException();
        }
        c cVar = new c(obj);
        ReentrantLock reentrantLock = this.c;
        reentrantLock.lock();
        try {
            return b(cVar);
        } finally {
            reentrantLock.unlock();
        }
    }

    public boolean c(E e2) {
        if (e2 == null) {
            throw new NullPointerException();
        }
        c cVar = new c(e2);
        ReentrantLock reentrantLock = this.c;
        reentrantLock.lock();
        try {
            return c(cVar);
        } finally {
            reentrantLock.unlock();
        }
    }

    public void d(E e2) throws InterruptedException {
        if (e2 == null) {
            throw new NullPointerException();
        }
        c cVar = new c(e2);
        ReentrantLock reentrantLock = this.c;
        reentrantLock.lock();
        while (!c(cVar)) {
            try {
                this.g.await();
            } finally {
                reentrantLock.unlock();
            }
        }
    }

    public boolean a(E e2, long j, TimeUnit timeUnit) throws InterruptedException {
        if (e2 == null) {
            throw new NullPointerException();
        }
        c cVar = new c(e2);
        long nanos = timeUnit.toNanos(j);
        ReentrantLock reentrantLock = this.c;
        reentrantLock.lockInterruptibly();
        while (!c(cVar)) {
            try {
                if (nanos <= 0) {
                    return false;
                }
                nanos = this.g.awaitNanos(nanos);
            } finally {
                reentrantLock.unlock();
            }
        }
        reentrantLock.unlock();
        return true;
    }

    public E a() {
        E b2 = b();
        if (b2 != null) {
            return b2;
        }
        throw new NoSuchElementException();
    }

    public E b() {
        ReentrantLock reentrantLock = this.c;
        reentrantLock.lock();
        try {
            return f();
        } finally {
            reentrantLock.unlock();
        }
    }

    public E c() throws InterruptedException {
        ReentrantLock reentrantLock = this.c;
        reentrantLock.lock();
        while (true) {
            try {
                E f2 = f();
                if (f2 != null) {
                    return f2;
                }
                this.f.await();
            } finally {
                reentrantLock.unlock();
            }
        }
    }

    public E a(long j, TimeUnit timeUnit) throws InterruptedException {
        long nanos = timeUnit.toNanos(j);
        ReentrantLock reentrantLock = this.c;
        reentrantLock.lockInterruptibly();
        while (true) {
            try {
                long j2 = nanos;
                E f2 = f();
                if (f2 != null) {
                    reentrantLock.unlock();
                    return f2;
                } else if (j2 <= 0) {
                    return null;
                } else {
                    nanos = this.f.awaitNanos(j2);
                }
            } finally {
                reentrantLock.unlock();
            }
        }
    }

    public E d() {
        E e2 = e();
        if (e2 != null) {
            return e2;
        }
        throw new NoSuchElementException();
    }

    public E e() {
        ReentrantLock reentrantLock = this.c;
        reentrantLock.lock();
        try {
            return this.f467a == null ? null : this.f467a.f469a;
        } finally {
            reentrantLock.unlock();
        }
    }

    public boolean e(Object obj) {
        if (obj == null) {
            return false;
        }
        ReentrantLock reentrantLock = this.c;
        reentrantLock.lock();
        try {
            for (c<E> cVar = this.f467a; cVar != null; cVar = cVar.c) {
                if (obj.equals(cVar.f469a)) {
                    a((c) cVar);
                    return true;
                }
            }
            reentrantLock.unlock();
            return false;
        } finally {
            reentrantLock.unlock();
        }
    }

    public boolean add(E e2) {
        a((Object) e2);
        return true;
    }

    public boolean offer(E e2) {
        return c((Object) e2);
    }

    public void put(E e2) throws InterruptedException {
        d(e2);
    }

    public boolean offer(E e2, long j, TimeUnit timeUnit) throws InterruptedException {
        return a(e2, j, timeUnit);
    }

    public E remove() {
        return a();
    }

    public E poll() {
        return b();
    }

    public E take() throws InterruptedException {
        return c();
    }

    public E poll(long j, TimeUnit timeUnit) throws InterruptedException {
        return a(j, timeUnit);
    }

    public E element() {
        return d();
    }

    public E peek() {
        return e();
    }

    public int remainingCapacity() {
        ReentrantLock reentrantLock = this.c;
        reentrantLock.lock();
        try {
            return this.e - this.d;
        } finally {
            reentrantLock.unlock();
        }
    }

    public int drainTo(Collection<? super E> collection) {
        return drainTo(collection, Integer.MAX_VALUE);
    }

    public int drainTo(Collection<? super E> collection, int i) {
        if (collection == null) {
            throw new NullPointerException();
        } else if (collection == this) {
            throw new IllegalArgumentException();
        } else {
            ReentrantLock reentrantLock = this.c;
            reentrantLock.lock();
            try {
                int min = Math.min(i, this.d);
                for (int i2 = 0; i2 < min; i2++) {
                    collection.add(this.f467a.f469a);
                    f();
                }
                return min;
            } finally {
                reentrantLock.unlock();
            }
        }
    }

    public boolean remove(Object obj) {
        return e(obj);
    }

    public int size() {
        ReentrantLock reentrantLock = this.c;
        reentrantLock.lock();
        try {
            return this.d;
        } finally {
            reentrantLock.unlock();
        }
    }

    public boolean contains(Object obj) {
        if (obj == null) {
            return false;
        }
        ReentrantLock reentrantLock = this.c;
        reentrantLock.lock();
        try {
            for (c<E> cVar = this.f467a; cVar != null; cVar = cVar.c) {
                if (obj.equals(cVar.f469a)) {
                    return true;
                }
            }
            reentrantLock.unlock();
            return false;
        } finally {
            reentrantLock.unlock();
        }
    }

    public Object[] toArray() {
        ReentrantLock reentrantLock = this.c;
        reentrantLock.lock();
        try {
            Object[] objArr = new Object[this.d];
            int i = 0;
            c<E> cVar = this.f467a;
            while (cVar != null) {
                int i2 = i + 1;
                objArr[i] = cVar.f469a;
                cVar = cVar.c;
                i = i2;
            }
            return objArr;
        } finally {
            reentrantLock.unlock();
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    public <T> T[] toArray(T[] tArr) {
        ReentrantLock reentrantLock = this.c;
        reentrantLock.lock();
        try {
            int length = tArr.length;
            T[] tArr2 = tArr;
            if (length < this.d) {
                tArr2 = (Object[]) Array.newInstance(tArr.getClass().getComponentType(), this.d);
            }
            int i = 0;
            c<E> cVar = this.f467a;
            while (cVar != null) {
                tArr2[i] = cVar.f469a;
                cVar = cVar.c;
                i++;
            }
            if (tArr2.length > i) {
                tArr2[i] = null;
            }
            return tArr2;
        } finally {
            reentrantLock.unlock();
        }
    }

    public String toString() {
        String sb;
        ReentrantLock reentrantLock = this.c;
        reentrantLock.lock();
        try {
            c<E> cVar = this.f467a;
            if (cVar == null) {
                sb = "[]";
            } else {
                StringBuilder sb2 = new StringBuilder();
                sb2.append('[');
                while (true) {
                    c<E> cVar2 = cVar;
                    E e2 = cVar2.f469a;
                    if (e2 == this) {
                        e2 = "(this Collection)";
                    }
                    sb2.append((Object) e2);
                    cVar = cVar2.c;
                    if (cVar == null) {
                        break;
                    }
                    sb2.append(',').append(' ');
                }
                sb = sb2.append(']').toString();
                reentrantLock.unlock();
            }
            return sb;
        } finally {
            reentrantLock.unlock();
        }
    }

    public void clear() {
        ReentrantLock reentrantLock = this.c;
        reentrantLock.lock();
        try {
            c<E> cVar = this.f467a;
            while (cVar != null) {
                cVar.f469a = null;
                c<E> cVar2 = cVar.c;
                cVar.b = null;
                cVar.c = null;
                cVar = cVar2;
            }
            this.b = null;
            this.f467a = null;
            this.d = 0;
            this.g.signalAll();
        } finally {
            reentrantLock.unlock();
        }
    }

    public Iterator<E> iterator() {
        return new b();
    }

    /* compiled from: LinkedBlockingDeque */
    private abstract class a implements Iterator<E> {

        /* renamed from: a  reason: collision with root package name */
        c<E> f468a;
        E b;
        private c<E> d;

        /* access modifiers changed from: package-private */
        public abstract c<E> a();

        /* access modifiers changed from: package-private */
        public abstract c<E> a(c<E> cVar);

        a() {
            ReentrantLock reentrantLock = d.this.c;
            reentrantLock.lock();
            try {
                this.f468a = a();
                this.b = this.f468a == null ? null : this.f468a.f469a;
            } finally {
                reentrantLock.unlock();
            }
        }

        private c<E> b(c<E> cVar) {
            while (true) {
                c<E> a2 = a(cVar);
                if (a2 == null) {
                    return null;
                }
                if (a2.f469a != null) {
                    return a2;
                }
                if (a2 == cVar) {
                    return a();
                }
                cVar = a2;
            }
        }

        /* access modifiers changed from: package-private */
        public void b() {
            ReentrantLock reentrantLock = d.this.c;
            reentrantLock.lock();
            try {
                this.f468a = b(this.f468a);
                this.b = this.f468a == null ? null : this.f468a.f469a;
            } finally {
                reentrantLock.unlock();
            }
        }

        public boolean hasNext() {
            return this.f468a != null;
        }

        public E next() {
            if (this.f468a == null) {
                throw new NoSuchElementException();
            }
            this.d = this.f468a;
            E e = this.b;
            b();
            return e;
        }

        public void remove() {
            c<E> cVar = this.d;
            if (cVar == null) {
                throw new IllegalStateException();
            }
            this.d = null;
            ReentrantLock reentrantLock = d.this.c;
            reentrantLock.lock();
            try {
                if (cVar.f469a != null) {
                    d.this.a((c) cVar);
                }
            } finally {
                reentrantLock.unlock();
            }
        }
    }

    /* compiled from: LinkedBlockingDeque */
    private class b extends a {
        private b() {
            super();
        }

        /* access modifiers changed from: package-private */
        public c<E> a() {
            return d.this.f467a;
        }

        /* access modifiers changed from: package-private */
        public c<E> a(c<E> cVar) {
            return cVar.c;
        }
    }
}
