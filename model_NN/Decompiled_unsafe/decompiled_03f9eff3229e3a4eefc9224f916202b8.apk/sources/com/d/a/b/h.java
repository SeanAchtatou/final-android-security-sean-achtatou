package com.d.a.b;

import com.d.a.b.a.e;
import com.d.a.b.a.f;
import com.d.a.b.e.a;
import java.util.concurrent.locks.ReentrantLock;

/* compiled from: ImageLoadingInfo */
final class h {

    /* renamed from: a  reason: collision with root package name */
    final String f503a;
    final String b;
    final a c;
    final com.d.a.b.a.h d;
    final c e;
    final e f;
    final f g;
    final ReentrantLock h;

    public h(String str, a aVar, com.d.a.b.a.h hVar, String str2, c cVar, e eVar, f fVar, ReentrantLock reentrantLock) {
        this.f503a = str;
        this.c = aVar;
        this.d = hVar;
        this.e = cVar;
        this.f = eVar;
        this.g = fVar;
        this.h = reentrantLock;
        this.b = str2;
    }
}
