package com.d.a.b.a;

import java.io.IOException;
import java.io.InputStream;

/* compiled from: ContentLengthInputStream */
public class a extends InputStream {

    /* renamed from: a  reason: collision with root package name */
    private final InputStream f466a;
    private final long b;
    private long c;

    public a(InputStream inputStream, long j) {
        this.f466a = inputStream;
        this.b = j;
    }

    public synchronized int available() {
        return (int) (this.b - this.c);
    }

    public void close() throws IOException {
        this.f466a.close();
    }

    public void mark(int i) {
        this.c = (long) i;
        this.f466a.mark(i);
    }

    public int read() throws IOException {
        this.c++;
        return this.f466a.read();
    }

    public int read(byte[] bArr) throws IOException {
        return read(bArr, 0, bArr.length);
    }

    public int read(byte[] bArr, int i, int i2) throws IOException {
        this.c += (long) i2;
        return this.f466a.read(bArr, i, i2);
    }

    public synchronized void reset() throws IOException {
        this.c = 0;
        this.f466a.reset();
    }

    public long skip(long j) throws IOException {
        this.c += j;
        return this.f466a.skip(j);
    }
}
