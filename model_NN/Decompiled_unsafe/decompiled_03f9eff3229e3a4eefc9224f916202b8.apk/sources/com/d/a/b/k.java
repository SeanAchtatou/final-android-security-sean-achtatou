package com.d.a.b;

import com.d.a.b.a.c;

/* compiled from: LoadAndDisplayImageTask */
class k implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ c.a f507a;
    final /* synthetic */ Throwable b;
    final /* synthetic */ i c;

    k(i iVar, c.a aVar, Throwable th) {
        this.c = iVar;
        this.f507a = aVar;
        this.b = th;
    }

    public void run() {
        if (this.c.c.c()) {
            this.c.b.a(this.c.c.c(this.c.i.f497a));
        }
        this.c.d.onLoadingFailed(this.c.f504a, this.c.b.d(), new c(this.f507a, this.b));
    }
}
