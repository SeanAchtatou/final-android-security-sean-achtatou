package com.d.a.b;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.widget.ImageView;
import com.d.a.b.a.e;
import com.d.a.b.a.f;
import com.d.a.b.a.h;
import com.d.a.b.a.i;
import com.d.a.b.a.j;
import com.d.a.b.a.n;
import com.d.a.b.a.o;
import com.d.a.b.e.a;
import com.d.a.b.e.b;
import com.d.a.c.c;

/* compiled from: ImageLoader */
public class d {

    /* renamed from: a  reason: collision with root package name */
    public static final String f489a = d.class.getSimpleName();
    private static volatile d e;
    private e b;
    private f c;
    private final e d = new n();

    public static d a() {
        if (e == null) {
            synchronized (d.class) {
                if (e == null) {
                    e = new d();
                }
            }
        }
        return e;
    }

    protected d() {
    }

    public synchronized void a(e eVar) {
        if (eVar == null) {
            throw new IllegalArgumentException("ImageLoader configuration can not be initialized with null");
        } else if (this.b == null) {
            if (eVar.u) {
                c.a("Initialize ImageLoader with configuration", new Object[0]);
            }
            this.c = new f(eVar);
            this.b = eVar;
        } else {
            c.c("Try to initialize ImageLoader which had already been initialized before. To re-init ImageLoader with new configuration call ImageLoader.destroy() at first.", new Object[0]);
        }
    }

    public boolean b() {
        return this.b != null;
    }

    public void a(String str, a aVar, c cVar, e eVar, f fVar) {
        e eVar2;
        c cVar2;
        h();
        if (aVar == null) {
            throw new IllegalArgumentException("Wrong arguments were passed to displayImage() method (ImageView reference must not be null)");
        }
        if (eVar == null) {
            eVar2 = this.d;
        } else {
            eVar2 = eVar;
        }
        if (cVar == null) {
            cVar2 = this.b.t;
        } else {
            cVar2 = cVar;
        }
        if (TextUtils.isEmpty(str)) {
            this.c.b(aVar);
            eVar2.onLoadingStarted(str, aVar.d());
            if (cVar2.b()) {
                aVar.a(cVar2.b(this.b.f497a));
            } else {
                aVar.a((Drawable) null);
            }
            eVar2.onLoadingComplete(str, aVar.d(), null);
            return;
        }
        h a2 = com.d.a.c.a.a(aVar, this.b.a());
        String a3 = j.a(str, a2);
        this.c.a(aVar, a3);
        eVar2.onLoadingStarted(str, aVar.d());
        Bitmap a4 = this.b.p.a(a3);
        if (a4 == null || a4.isRecycled()) {
            if (cVar2.a()) {
                aVar.a(cVar2.a(this.b.f497a));
            } else if (cVar2.g()) {
                aVar.a((Drawable) null);
            }
            i iVar = new i(this.c, new h(str, aVar, a2, a3, cVar2, eVar2, fVar, this.c.a(str)), cVar2.r());
            if (cVar2.s()) {
                iVar.run();
            } else {
                this.c.a(iVar);
            }
        } else {
            if (this.b.u) {
                c.a("Load image from memory cache [%s]", a3);
            }
            if (cVar2.e()) {
                m mVar = new m(this.c, a4, new h(str, aVar, a2, a3, cVar2, eVar2, fVar, this.c.a(str)), cVar2.r());
                if (cVar2.s()) {
                    mVar.run();
                } else {
                    this.c.a(mVar);
                }
            } else {
                cVar2.q().display(a4, aVar, i.MEMORY_CACHE);
                eVar2.onLoadingComplete(str, aVar.d(), a4);
            }
        }
    }

    public void a(String str, ImageView imageView) {
        a(str, new com.d.a.b.e.c(imageView), (c) null, (e) null, (f) null);
    }

    public void a(String str, ImageView imageView, c cVar) {
        a(str, new com.d.a.b.e.c(imageView), cVar, (e) null, (f) null);
    }

    public void a(String str, ImageView imageView, c cVar, e eVar, f fVar) {
        a(str, new com.d.a.b.e.c(imageView), cVar, eVar, fVar);
    }

    public void a(String str, e eVar) {
        a(str, (h) null, (c) null, eVar, (f) null);
    }

    public void a(String str, c cVar, e eVar) {
        a(str, (h) null, cVar, eVar, (f) null);
    }

    public void a(String str, h hVar, c cVar, e eVar, f fVar) {
        c cVar2;
        h();
        if (hVar == null) {
            hVar = this.b.a();
        }
        if (cVar == null) {
            cVar2 = this.b.t;
        } else {
            cVar2 = cVar;
        }
        a(str, new b(str, hVar, o.CROP), cVar2, eVar, fVar);
    }

    private void h() {
        if (this.b == null) {
            throw new IllegalStateException("ImageLoader must be init with configuration before using");
        }
    }

    public com.d.a.a.b.b<String, Bitmap> c() {
        h();
        return this.b.p;
    }

    public com.d.a.a.a.b d() {
        h();
        return this.b.q;
    }

    public void e() {
        h();
        this.b.q.a();
    }

    public void f() {
        this.c.a();
    }

    public void g() {
        this.c.b();
    }
}
