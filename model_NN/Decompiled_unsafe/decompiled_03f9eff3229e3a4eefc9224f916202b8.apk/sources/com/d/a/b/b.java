package com.d.a.b;

import android.graphics.Bitmap;
import com.d.a.b.a.e;
import com.d.a.b.a.i;
import com.d.a.b.e.a;
import com.d.a.c.c;

/* compiled from: DisplayBitmapTask */
final class b implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final Bitmap f479a;
    private final String b;
    private final a c;
    private final String d;
    private final com.d.a.b.c.a e;
    private final e f;
    private final f g;
    private final i h;
    private boolean i;

    public b(Bitmap bitmap, h hVar, f fVar, i iVar) {
        this.f479a = bitmap;
        this.b = hVar.f503a;
        this.c = hVar.c;
        this.d = hVar.b;
        this.e = hVar.e.q();
        this.f = hVar.f;
        this.g = fVar;
        this.h = iVar;
    }

    public void run() {
        if (this.c.e()) {
            if (this.i) {
                c.a("ImageAware was collected by GC. Task is cancelled. [%s]", this.d);
            }
            this.f.onLoadingCancelled(this.b, this.c.d());
        } else if (a()) {
            if (this.i) {
                c.a("ImageAware is reused for another image. Task is cancelled. [%s]", this.d);
            }
            this.f.onLoadingCancelled(this.b, this.c.d());
        } else {
            if (this.i) {
                c.a("Display image in ImageAware (loaded from %1$s) [%2$s]", this.h, this.d);
            }
            this.e.display(this.f479a, this.c, this.h);
            this.f.onLoadingComplete(this.b, this.c.d(), this.f479a);
            this.g.b(this.c);
        }
    }

    private boolean a() {
        return !this.d.equals(this.g.a(this.c));
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z) {
        this.i = z;
    }
}
