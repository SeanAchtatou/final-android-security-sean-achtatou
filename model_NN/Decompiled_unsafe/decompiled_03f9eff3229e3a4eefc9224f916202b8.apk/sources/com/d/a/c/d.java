package com.d.a.c;

import android.content.Context;
import android.os.Environment;
import com.sina.weibo.sdk.component.ShareRequestParam;
import java.io.File;
import java.io.IOException;

/* compiled from: StorageUtils */
public final class d {
    public static File a(Context context) {
        return a(context, true);
    }

    public static File a(Context context, boolean z) {
        File file = null;
        if (z && "mounted".equals(Environment.getExternalStorageState()) && d(context)) {
            file = c(context);
        }
        if (file == null) {
            file = context.getCacheDir();
        }
        if (file != null) {
            return file;
        }
        String str = "/data/data/" + context.getPackageName() + "/cache/";
        c.c("Can't define system cache directory! '%s' will be used.", str);
        return new File(str);
    }

    public static File b(Context context) {
        File a2 = a(context);
        File file = new File(a2, "uil-images");
        return (file.exists() || file.mkdir()) ? file : a2;
    }

    private static File c(Context context) {
        File file = new File(new File(new File(new File(Environment.getExternalStorageDirectory(), "Android"), ShareRequestParam.RESP_UPLOAD_PIC_PARAM_DATA), context.getPackageName()), "cache");
        if (file.exists()) {
            return file;
        }
        if (!file.mkdirs()) {
            c.c("Unable to create external cache directory", new Object[0]);
            return null;
        }
        try {
            new File(file, ".nomedia").createNewFile();
            return file;
        } catch (IOException e) {
            c.b("Can't create \".nomedia\" file in application external cache directory", new Object[0]);
            return file;
        }
    }

    private static boolean d(Context context) {
        return context.checkCallingOrSelfPermission("android.permission.WRITE_EXTERNAL_STORAGE") == 0;
    }
}
