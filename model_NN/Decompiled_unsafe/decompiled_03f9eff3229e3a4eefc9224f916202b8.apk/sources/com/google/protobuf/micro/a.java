package com.google.protobuf.micro;

import java.io.InputStream;
import java.util.Vector;
import org.apache.mina.proxy.handlers.http.ntlm.NTLMConstants;
import org.apache.mina.proxy.handlers.socks.SocksProxyConstants;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private final byte[] f594a;
    private int b;
    private int c;
    private int d;
    private final InputStream e;
    private int f;
    private int g;
    private int h;
    private int i;
    private int j;

    private a(InputStream inputStream) {
        this.h = Integer.MAX_VALUE;
        this.i = 64;
        this.j = NTLMConstants.FLAG_UNIDENTIFIED_9;
        this.f594a = new byte[4096];
        this.b = 0;
        this.d = 0;
        this.e = inputStream;
    }

    private a(byte[] bArr, int i2, int i3) {
        this.h = Integer.MAX_VALUE;
        this.i = 64;
        this.j = NTLMConstants.FLAG_UNIDENTIFIED_9;
        this.f594a = bArr;
        this.b = i2 + i3;
        this.d = i2;
        this.e = null;
    }

    public static a a(InputStream inputStream) {
        return new a(inputStream);
    }

    public static a a(byte[] bArr, int i2, int i3) {
        return new a(bArr, i2, i3);
    }

    private boolean a(boolean z) {
        if (this.d < this.b) {
            throw new IllegalStateException("refillBuffer() called when buffer wasn't empty.");
        } else if (this.g + this.b != this.h) {
            this.g += this.b;
            this.d = 0;
            this.b = this.e == null ? -1 : this.e.read(this.f594a);
            if (this.b == 0 || this.b < -1) {
                throw new IllegalStateException("InputStream#read(byte[]) returned invalid result: " + this.b + "\nThe InputStream implementation is buggy.");
            } else if (this.b == -1) {
                this.b = 0;
                if (!z) {
                    return false;
                }
                throw c.a();
            } else {
                l();
                int i2 = this.g + this.b + this.c;
                if (i2 <= this.j && i2 >= 0) {
                    return true;
                }
                throw c.g();
            }
        } else if (!z) {
            return false;
        } else {
            throw c.a();
        }
    }

    private void l() {
        this.b += this.c;
        int i2 = this.g + this.b;
        if (i2 > this.h) {
            this.c = i2 - this.h;
            this.b -= this.c;
            return;
        }
        this.c = 0;
    }

    public int a() {
        if (j()) {
            this.f = 0;
            return 0;
        }
        this.f = g();
        if (this.f != 0) {
            return this.f;
        }
        throw c.d();
    }

    public void a(int i2) {
        if (this.f != i2) {
            throw c.e();
        }
    }

    public void b() {
        int a2;
        do {
            a2 = a();
            if (a2 == 0) {
                return;
            }
        } while (b(a2));
    }

    public boolean b(int i2) {
        switch (e.a(i2)) {
            case 0:
                c();
                return true;
            case 1:
                i();
                return true;
            case 2:
                d(g());
                return true;
            case 3:
                b();
                a(e.a(e.b(i2), 4));
                return true;
            case 4:
                return false;
            case 5:
                h();
                return true;
            default:
                throw c.f();
        }
    }

    public int c() {
        return g();
    }

    public byte[] c(int i2) {
        if (i2 < 0) {
            throw c.b();
        } else if (this.g + this.d + i2 > this.h) {
            d((this.h - this.g) - this.d);
            throw c.a();
        } else if (i2 <= this.b - this.d) {
            byte[] bArr = new byte[i2];
            System.arraycopy(this.f594a, this.d, bArr, 0, i2);
            this.d += i2;
            return bArr;
        } else if (i2 < 4096) {
            byte[] bArr2 = new byte[i2];
            int i3 = this.b - this.d;
            System.arraycopy(this.f594a, this.d, bArr2, 0, i3);
            this.d = this.b;
            a(true);
            while (i2 - i3 > this.b) {
                System.arraycopy(this.f594a, 0, bArr2, i3, this.b);
                i3 += this.b;
                this.d = this.b;
                a(true);
            }
            System.arraycopy(this.f594a, 0, bArr2, i3, i2 - i3);
            this.d = i2 - i3;
            return bArr2;
        } else {
            int i4 = this.d;
            int i5 = this.b;
            this.g += this.b;
            this.d = 0;
            this.b = 0;
            Vector vector = new Vector();
            int i6 = i2 - (i5 - i4);
            while (i6 > 0) {
                byte[] bArr3 = new byte[Math.min(i6, 4096)];
                int i7 = 0;
                while (i7 < bArr3.length) {
                    int read = this.e == null ? -1 : this.e.read(bArr3, i7, bArr3.length - i7);
                    if (read == -1) {
                        throw c.a();
                    }
                    this.g += read;
                    i7 += read;
                }
                vector.addElement(bArr3);
                i6 -= bArr3.length;
            }
            byte[] bArr4 = new byte[i2];
            int i8 = i5 - i4;
            System.arraycopy(this.f594a, i4, bArr4, 0, i8);
            int i9 = i8;
            for (int i10 = 0; i10 < vector.size(); i10++) {
                byte[] bArr5 = (byte[]) vector.elementAt(i10);
                System.arraycopy(bArr5, 0, bArr4, i9, bArr5.length);
                i9 += bArr5.length;
            }
            return bArr4;
        }
    }

    public void d(int i2) {
        if (i2 < 0) {
            throw c.b();
        } else if (this.g + this.d + i2 > this.h) {
            d((this.h - this.g) - this.d);
            throw c.a();
        } else if (i2 <= this.b - this.d) {
            this.d += i2;
        } else {
            int i3 = this.b - this.d;
            this.g += this.b;
            this.d = 0;
            this.b = 0;
            int i4 = i3;
            while (i4 < i2) {
                int skip = this.e == null ? -1 : (int) this.e.skip((long) (i2 - i4));
                if (skip <= 0) {
                    throw c.a();
                }
                i4 += skip;
                this.g = skip + this.g;
            }
        }
    }

    public boolean d() {
        return g() != 0;
    }

    public String e() {
        int g2 = g();
        if (g2 > this.b - this.d || g2 <= 0) {
            return new String(c(g2), "UTF-8");
        }
        String str = new String(this.f594a, this.d, g2, "UTF-8");
        this.d = g2 + this.d;
        return str;
    }

    public int f() {
        return g();
    }

    public int g() {
        byte k = k();
        if (k >= 0) {
            return k;
        }
        byte b2 = k & Byte.MAX_VALUE;
        byte k2 = k();
        if (k2 >= 0) {
            return b2 | (k2 << 7);
        }
        byte b3 = b2 | ((k2 & Byte.MAX_VALUE) << 7);
        byte k3 = k();
        if (k3 >= 0) {
            return b3 | (k3 << 14);
        }
        byte b4 = b3 | ((k3 & Byte.MAX_VALUE) << 14);
        byte k4 = k();
        if (k4 >= 0) {
            return b4 | (k4 << 21);
        }
        byte b5 = b4 | ((k4 & Byte.MAX_VALUE) << 21);
        byte k5 = k();
        byte b6 = b5 | (k5 << 28);
        if (k5 >= 0) {
            return b6;
        }
        for (int i2 = 0; i2 < 5; i2++) {
            if (k() >= 0) {
                return b6;
            }
        }
        throw c.c();
    }

    public int h() {
        return (k() & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) | ((k() & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) << 8) | ((k() & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) << 16) | ((k() & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) << 24);
    }

    public long i() {
        byte k = k();
        byte k2 = k();
        return ((((long) k2) & 255) << 8) | (((long) k) & 255) | ((((long) k()) & 255) << 16) | ((((long) k()) & 255) << 24) | ((((long) k()) & 255) << 32) | ((((long) k()) & 255) << 40) | ((((long) k()) & 255) << 48) | ((((long) k()) & 255) << 56);
    }

    public boolean j() {
        return this.d == this.b && !a(false);
    }

    public byte k() {
        if (this.d == this.b) {
            a(true);
        }
        byte[] bArr = this.f594a;
        int i2 = this.d;
        this.d = i2 + 1;
        return bArr[i2];
    }
}
