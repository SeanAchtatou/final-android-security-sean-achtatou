package com.google.a;

import java.lang.reflect.Field;
import java.util.Locale;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* compiled from: FieldNamingPolicy */
final class h extends c {
    h(String str, int i) {
        super(str, i, null);
    }

    public String a(Field field) {
        return a(field.getName(), "-").toLowerCase(Locale.ENGLISH);
    }
}
