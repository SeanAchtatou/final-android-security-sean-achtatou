package com.google.a.b.a;

import com.google.a.a.c;
import com.google.a.af;
import com.google.a.ah;
import com.google.a.d.d;
import com.google.a.u;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.URI;
import java.net.URL;
import java.util.BitSet;
import java.util.Calendar;
import java.util.Currency;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicIntegerArray;

/* compiled from: TypeAdapters */
public final class v {
    public static final af<String> A = new ad();
    public static final af<BigDecimal> B = new ae();
    public static final af<BigInteger> C = new af();
    public static final ah D = a(String.class, A);
    public static final af<StringBuilder> E = new ag();
    public static final ah F = a(StringBuilder.class, E);
    public static final af<StringBuffer> G = new ai();
    public static final ah H = a(StringBuffer.class, G);
    public static final af<URL> I = new aj();
    public static final ah J = a(URL.class, I);
    public static final af<URI> K = new ak();
    public static final ah L = a(URI.class, K);
    public static final af<InetAddress> M = new al();
    public static final ah N = b(InetAddress.class, M);
    public static final af<UUID> O = new am();
    public static final ah P = a(UUID.class, O);
    public static final af<Currency> Q = new an().a();
    public static final ah R = a(Currency.class, Q);
    public static final ah S = new ao();
    public static final af<Calendar> T = new aq();
    public static final ah U = b(Calendar.class, GregorianCalendar.class, T);
    public static final af<Locale> V = new ar();
    public static final ah W = a(Locale.class, V);
    public static final af<u> X = new as();
    public static final ah Y = b(u.class, X);
    public static final ah Z = new au();

    /* renamed from: a  reason: collision with root package name */
    public static final af<Class> f542a = new w();
    public static final ah b = a(Class.class, f542a);
    public static final af<BitSet> c = new ah();
    public static final ah d = a(BitSet.class, c);
    public static final af<Boolean> e = new at();
    public static final af<Boolean> f = new bb();
    public static final ah g = a(Boolean.TYPE, Boolean.class, e);
    public static final af<Number> h = new bc();
    public static final ah i = a(Byte.TYPE, Byte.class, h);
    public static final af<Number> j = new bd();
    public static final ah k = a(Short.TYPE, Short.class, j);
    public static final af<Number> l = new be();
    public static final ah m = a(Integer.TYPE, Integer.class, l);
    public static final af<AtomicInteger> n = new bf().a();
    public static final ah o = a(AtomicInteger.class, n);
    public static final af<AtomicBoolean> p = new bg().a();
    public static final ah q = a(AtomicBoolean.class, p);
    public static final af<AtomicIntegerArray> r = new x().a();
    public static final ah s = a(AtomicIntegerArray.class, r);
    public static final af<Number> t = new y();
    public static final af<Number> u = new z();
    public static final af<Number> v = new aa();
    public static final af<Number> w = new ab();
    public static final ah x = a(Number.class, w);
    public static final af<Character> y = new ac();
    public static final ah z = a(Character.TYPE, Character.class, y);

    /* compiled from: TypeAdapters */
    private static final class a<T extends Enum<T>> extends af<T> {

        /* renamed from: a  reason: collision with root package name */
        private final Map<String, T> f543a = new HashMap();
        private final Map<T, String> b = new HashMap();

        public a(Class<T> cls) {
            try {
                for (Enum enumR : (Enum[]) cls.getEnumConstants()) {
                    String name = enumR.name();
                    c cVar = (c) cls.getField(name).getAnnotation(c.class);
                    if (cVar != null) {
                        name = cVar.a();
                        for (String put : cVar.b()) {
                            this.f543a.put(put, enumR);
                        }
                    }
                    String str = name;
                    this.f543a.put(str, enumR);
                    this.b.put(enumR, str);
                }
            } catch (NoSuchFieldException e) {
                throw new AssertionError("Missing field in " + cls.getName(), e);
            }
        }

        /* renamed from: a */
        public T b(com.google.a.d.a aVar) throws IOException {
            if (aVar.f() != com.google.a.d.c.NULL) {
                return (Enum) this.f543a.get(aVar.h());
            }
            aVar.j();
            return null;
        }

        public void a(d dVar, T t) throws IOException {
            dVar.b(t == null ? null : this.b.get(t));
        }
    }

    public static <TT> ah a(Class<TT> cls, af<TT> afVar) {
        return new av(cls, afVar);
    }

    public static <TT> ah a(Class<TT> cls, Class<TT> cls2, af<? super TT> afVar) {
        return new aw(cls, cls2, afVar);
    }

    public static <TT> ah b(Class<TT> cls, Class<? extends TT> cls2, af<? super TT> afVar) {
        return new ax(cls, cls2, afVar);
    }

    public static <T1> ah b(Class<T1> cls, af<T1> afVar) {
        return new ay(cls, afVar);
    }
}
