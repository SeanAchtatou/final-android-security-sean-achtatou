package com.google.a.b.a;

import com.google.a.af;
import com.google.a.b.a.o;
import com.google.a.c.a;
import com.google.a.d.d;
import com.google.a.j;
import java.io.IOException;
import java.lang.reflect.Field;

/* compiled from: ReflectiveTypeAdapterFactory */
class p extends o.b {

    /* renamed from: a  reason: collision with root package name */
    final af<?> f538a = this.f.a(this.b, this.c, this.d);
    final /* synthetic */ j b;
    final /* synthetic */ Field c;
    final /* synthetic */ a d;
    final /* synthetic */ boolean e;
    final /* synthetic */ o f;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    p(o oVar, String str, boolean z, boolean z2, j jVar, Field field, a aVar, boolean z3) {
        super(str, z, z2);
        this.f = oVar;
        this.b = jVar;
        this.c = field;
        this.d = aVar;
        this.e = z3;
    }

    /* access modifiers changed from: package-private */
    public void a(d dVar, Object obj) throws IOException, IllegalAccessException {
        new u(this.b, this.f538a, this.d.b()).a(dVar, this.c.get(obj));
    }

    /* access modifiers changed from: package-private */
    public void a(com.google.a.d.a aVar, Object obj) throws IOException, IllegalAccessException {
        Object b2 = this.f538a.b(aVar);
        if (b2 != null || !this.e) {
            this.c.set(obj, b2);
        }
    }

    public boolean a(Object obj) throws IOException, IllegalAccessException {
        if (this.h && this.c.get(obj) != obj) {
            return true;
        }
        return false;
    }
}
