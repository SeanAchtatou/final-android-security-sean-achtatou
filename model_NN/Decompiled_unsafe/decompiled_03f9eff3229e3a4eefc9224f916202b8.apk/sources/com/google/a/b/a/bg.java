package com.google.a.b.a;

import com.google.a.af;
import com.google.a.d.a;
import com.google.a.d.d;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;

/* compiled from: TypeAdapters */
final class bg extends af<AtomicBoolean> {
    bg() {
    }

    /* renamed from: a */
    public AtomicBoolean b(a aVar) throws IOException {
        return new AtomicBoolean(aVar.i());
    }

    public void a(d dVar, AtomicBoolean atomicBoolean) throws IOException {
        dVar.a(atomicBoolean.get());
    }
}
