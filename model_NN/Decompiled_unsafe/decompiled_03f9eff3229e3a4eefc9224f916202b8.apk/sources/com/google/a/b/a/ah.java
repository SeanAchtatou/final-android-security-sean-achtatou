package com.google.a.b.a;

import com.google.a.ab;
import com.google.a.af;
import com.google.a.d.a;
import com.google.a.d.c;
import com.google.a.d.d;
import java.io.IOException;
import java.util.BitSet;

/* compiled from: TypeAdapters */
final class ah extends af<BitSet> {
    ah() {
    }

    /* renamed from: a */
    public BitSet b(a aVar) throws IOException {
        boolean z;
        if (aVar.f() == c.NULL) {
            aVar.j();
            return null;
        }
        BitSet bitSet = new BitSet();
        aVar.a();
        c f = aVar.f();
        int i = 0;
        while (f != c.END_ARRAY) {
            switch (f) {
                case NUMBER:
                    if (aVar.m() == 0) {
                        z = false;
                        break;
                    } else {
                        z = true;
                        break;
                    }
                case BOOLEAN:
                    z = aVar.i();
                    break;
                case STRING:
                    String h = aVar.h();
                    try {
                        if (Integer.parseInt(h) == 0) {
                            z = false;
                            break;
                        } else {
                            z = true;
                            break;
                        }
                    } catch (NumberFormatException e) {
                        throw new ab("Error: Expecting: bitset number value (1, 0), Found: " + h);
                    }
                default:
                    throw new ab("Invalid bitset value type: " + f);
            }
            if (z) {
                bitSet.set(i);
            }
            i++;
            f = aVar.f();
        }
        aVar.b();
        return bitSet;
    }

    public void a(d dVar, BitSet bitSet) throws IOException {
        int i;
        if (bitSet == null) {
            dVar.f();
            return;
        }
        dVar.b();
        for (int i2 = 0; i2 < bitSet.length(); i2++) {
            if (bitSet.get(i2)) {
                i = 1;
            } else {
                i = 0;
            }
            dVar.a((long) i);
        }
        dVar.c();
    }
}
