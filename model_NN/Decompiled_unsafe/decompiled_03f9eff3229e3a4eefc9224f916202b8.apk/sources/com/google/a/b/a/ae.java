package com.google.a.b.a;

import com.google.a.ab;
import com.google.a.af;
import com.google.a.d.a;
import com.google.a.d.c;
import com.google.a.d.d;
import java.io.IOException;
import java.math.BigDecimal;

/* compiled from: TypeAdapters */
final class ae extends af<BigDecimal> {
    ae() {
    }

    /* renamed from: a */
    public BigDecimal b(a aVar) throws IOException {
        if (aVar.f() == c.NULL) {
            aVar.j();
            return null;
        }
        try {
            return new BigDecimal(aVar.h());
        } catch (NumberFormatException e) {
            throw new ab(e);
        }
    }

    public void a(d dVar, BigDecimal bigDecimal) throws IOException {
        dVar.a(bigDecimal);
    }
}
