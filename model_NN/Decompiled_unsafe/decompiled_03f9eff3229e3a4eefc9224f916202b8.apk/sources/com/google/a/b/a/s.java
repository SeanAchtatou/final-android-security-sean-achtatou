package com.google.a.b.a;

import com.google.a.ab;
import com.google.a.af;
import com.google.a.ah;
import com.google.a.d.a;
import com.google.a.d.c;
import com.google.a.d.d;
import java.io.IOException;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/* compiled from: TimeTypeAdapter */
public final class s extends af<Time> {

    /* renamed from: a  reason: collision with root package name */
    public static final ah f540a = new t();
    private final DateFormat b = new SimpleDateFormat("hh:mm:ss a");

    /* renamed from: a */
    public synchronized Time b(a aVar) throws IOException {
        Time time;
        if (aVar.f() == c.NULL) {
            aVar.j();
            time = null;
        } else {
            try {
                time = new Time(this.b.parse(aVar.h()).getTime());
            } catch (ParseException e) {
                throw new ab(e);
            }
        }
        return time;
    }

    public synchronized void a(d dVar, Time time) throws IOException {
        dVar.b(time == null ? null : this.b.format(time));
    }
}
