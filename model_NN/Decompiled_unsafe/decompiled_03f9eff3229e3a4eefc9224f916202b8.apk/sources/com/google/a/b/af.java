package com.google.a.b;

import java.lang.reflect.Method;

/* compiled from: UnsafeAllocator */
final class af extends ac {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Method f547a;

    af(Method method) {
        this.f547a = method;
    }

    public <T> T a(Class<T> cls) throws Exception {
        return this.f547a.invoke(null, cls, Object.class);
    }
}
