package com.google.a.b.a;

import com.google.a.ab;
import com.google.a.af;
import com.google.a.d.a;
import com.google.a.d.d;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

/* compiled from: TypeAdapters */
final class bf extends af<AtomicInteger> {
    bf() {
    }

    /* renamed from: a */
    public AtomicInteger b(a aVar) throws IOException {
        try {
            return new AtomicInteger(aVar.m());
        } catch (NumberFormatException e) {
            throw new ab(e);
        }
    }

    public void a(d dVar, AtomicInteger atomicInteger) throws IOException {
        dVar.a((long) atomicInteger.get());
    }
}
