package com.google.a.b.a;

import com.google.a.ab;
import com.google.a.af;
import com.google.a.ah;
import com.google.a.b.aa;
import com.google.a.b.c;
import com.google.a.b.r;
import com.google.a.b.z;
import com.google.a.d.d;
import com.google.a.i;
import com.google.a.j;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/* compiled from: ReflectiveTypeAdapterFactory */
public final class o implements ah {

    /* renamed from: a  reason: collision with root package name */
    private final c f536a;
    private final i b;
    private final r c;

    public o(c cVar, i iVar, r rVar) {
        this.f536a = cVar;
        this.b = iVar;
        this.c = rVar;
    }

    public boolean a(Field field, boolean z) {
        return a(field, z, this.c);
    }

    static boolean a(Field field, boolean z, r rVar) {
        return !rVar.a(field.getType(), z) && !rVar.a(field, z);
    }

    private List<String> a(Field field) {
        return a(this.b, field);
    }

    static List<String> a(i iVar, Field field) {
        com.google.a.a.c cVar = (com.google.a.a.c) field.getAnnotation(com.google.a.a.c.class);
        LinkedList linkedList = new LinkedList();
        if (cVar == null) {
            linkedList.add(iVar.a(field));
        } else {
            linkedList.add(cVar.a());
            for (String add : cVar.b()) {
                linkedList.add(add);
            }
        }
        return linkedList;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.a.b.a.o.a(com.google.a.j, com.google.a.c.a<?>, java.lang.Class<?>):java.util.Map<java.lang.String, com.google.a.b.a.o$b>
     arg types: [com.google.a.j, com.google.a.c.a<T>, java.lang.Class<? super T>]
     candidates:
      com.google.a.b.a.o.a(java.lang.reflect.Field, boolean, com.google.a.b.r):boolean
      com.google.a.b.a.o.a(com.google.a.j, java.lang.reflect.Field, com.google.a.c.a<?>):com.google.a.af<?>
      com.google.a.b.a.o.a(com.google.a.j, com.google.a.c.a<?>, java.lang.Class<?>):java.util.Map<java.lang.String, com.google.a.b.a.o$b> */
    public <T> af<T> a(j jVar, com.google.a.c.a<T> aVar) {
        Class<? super T> a2 = aVar.a();
        if (!Object.class.isAssignableFrom(a2)) {
            return null;
        }
        return new a(this.f536a.a(aVar), a(jVar, (com.google.a.c.a<?>) aVar, (Class<?>) a2));
    }

    private b a(j jVar, Field field, String str, com.google.a.c.a<?> aVar, boolean z, boolean z2) {
        return new p(this, str, z, z2, jVar, field, aVar, aa.a((Type) aVar.a()));
    }

    /* access modifiers changed from: package-private */
    public af<?> a(j jVar, Field field, com.google.a.c.a<?> aVar) {
        af<?> a2;
        com.google.a.a.b bVar = (com.google.a.a.b) field.getAnnotation(com.google.a.a.b.class);
        return (bVar == null || (a2 = f.a(this.f536a, jVar, aVar, bVar)) == null) ? jVar.a(aVar) : a2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.a.b.a.o.a(java.lang.reflect.Field, boolean):boolean
     arg types: [java.lang.reflect.Field, int]
     candidates:
      com.google.a.b.a.o.a(com.google.a.i, java.lang.reflect.Field):java.util.List<java.lang.String>
      com.google.a.b.a.o.a(com.google.a.j, com.google.a.c.a):com.google.a.af<T>
      com.google.a.ah.a(com.google.a.j, com.google.a.c.a):com.google.a.af<T>
      com.google.a.b.a.o.a(java.lang.reflect.Field, boolean):boolean */
    private Map<String, b> a(j jVar, com.google.a.c.a<?> aVar, Class<?> cls) {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        if (cls.isInterface()) {
            return linkedHashMap;
        }
        Type b2 = aVar.b();
        while (cls != Object.class) {
            for (Field field : cls.getDeclaredFields()) {
                boolean a2 = a(field, true);
                boolean a3 = a(field, false);
                if (a2 || a3) {
                    field.setAccessible(true);
                    Type a4 = com.google.a.b.b.a(aVar.b(), cls, field.getGenericType());
                    List<String> a5 = a(field);
                    b bVar = null;
                    int i = 0;
                    while (i < a5.size()) {
                        String str = a5.get(i);
                        if (i != 0) {
                            a2 = false;
                        }
                        b bVar2 = (b) linkedHashMap.put(str, a(jVar, field, str, com.google.a.c.a.a(a4), a2, a3));
                        if (bVar != null) {
                            bVar2 = bVar;
                        }
                        i++;
                        bVar = bVar2;
                    }
                    if (bVar != null) {
                        throw new IllegalArgumentException(b2 + " declares multiple JSON fields named " + bVar.g);
                    }
                }
            }
            aVar = com.google.a.c.a.a(com.google.a.b.b.a(aVar.b(), cls, cls.getGenericSuperclass()));
            cls = aVar.a();
        }
        return linkedHashMap;
    }

    /* compiled from: ReflectiveTypeAdapterFactory */
    static abstract class b {
        final String g;
        final boolean h;
        final boolean i;

        /* access modifiers changed from: package-private */
        public abstract void a(com.google.a.d.a aVar, Object obj) throws IOException, IllegalAccessException;

        /* access modifiers changed from: package-private */
        public abstract void a(d dVar, Object obj) throws IOException, IllegalAccessException;

        /* access modifiers changed from: package-private */
        public abstract boolean a(Object obj) throws IOException, IllegalAccessException;

        protected b(String str, boolean z, boolean z2) {
            this.g = str;
            this.h = z;
            this.i = z2;
        }
    }

    /* compiled from: ReflectiveTypeAdapterFactory */
    public static final class a<T> extends af<T> {

        /* renamed from: a  reason: collision with root package name */
        private final z<T> f537a;
        private final Map<String, b> b;

        a(z<T> zVar, Map<String, b> map) {
            this.f537a = zVar;
            this.b = map;
        }

        public T b(com.google.a.d.a aVar) throws IOException {
            if (aVar.f() == com.google.a.d.c.NULL) {
                aVar.j();
                return null;
            }
            T a2 = this.f537a.a();
            try {
                aVar.c();
                while (aVar.e()) {
                    b bVar = this.b.get(aVar.g());
                    if (bVar == null || !bVar.i) {
                        aVar.n();
                    } else {
                        bVar.a(aVar, a2);
                    }
                }
                aVar.d();
                return a2;
            } catch (IllegalStateException e) {
                throw new ab(e);
            } catch (IllegalAccessException e2) {
                throw new AssertionError(e2);
            }
        }

        public void a(d dVar, T t) throws IOException {
            if (t == null) {
                dVar.f();
                return;
            }
            dVar.d();
            try {
                for (b next : this.b.values()) {
                    if (next.a(t)) {
                        dVar.a(next.g);
                        next.a(dVar, t);
                    }
                }
                dVar.e();
            } catch (IllegalAccessException e) {
                throw new AssertionError(e);
            }
        }
    }
}
