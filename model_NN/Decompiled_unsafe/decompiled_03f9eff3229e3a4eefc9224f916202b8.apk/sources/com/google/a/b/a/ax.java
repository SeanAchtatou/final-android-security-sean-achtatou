package com.google.a.b.a;

import com.google.a.af;
import com.google.a.ah;
import com.google.a.c.a;
import com.google.a.j;

/* compiled from: TypeAdapters */
final class ax implements ah {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Class f523a;
    final /* synthetic */ Class b;
    final /* synthetic */ af c;

    ax(Class cls, Class cls2, af afVar) {
        this.f523a = cls;
        this.b = cls2;
        this.c = afVar;
    }

    public <T> af<T> a(j jVar, a<T> aVar) {
        Class<? super T> a2 = aVar.a();
        if (a2 == this.f523a || a2 == this.b) {
            return this.c;
        }
        return null;
    }

    public String toString() {
        return "Factory[type=" + this.f523a.getName() + "+" + this.b.getName() + ",adapter=" + this.c + "]";
    }
}
