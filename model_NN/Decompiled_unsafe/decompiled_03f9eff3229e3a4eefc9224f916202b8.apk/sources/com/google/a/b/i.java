package com.google.a.b;

import java.lang.reflect.Type;

/* compiled from: ConstructorConstructor */
class i implements z<T> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Class f558a;
    final /* synthetic */ Type b;
    final /* synthetic */ c c;
    private final ac d = ac.a();

    i(c cVar, Class cls, Type type) {
        this.c = cVar;
        this.f558a = cls;
        this.b = type;
    }

    public T a() {
        try {
            return this.d.a(this.f558a);
        } catch (Exception e) {
            throw new RuntimeException("Unable to invoke no-args constructor for " + this.b + ". " + "Register an InstanceCreator with Gson for this type may fix this problem.", e);
        }
    }
}
