package com.google.a.b.a;

import com.google.a.a.b;
import com.google.a.af;
import com.google.a.ah;
import com.google.a.b.c;
import com.google.a.c.a;
import com.google.a.j;

/* compiled from: JsonAdapterAnnotationTypeAdapterFactory */
public final class f implements ah {

    /* renamed from: a  reason: collision with root package name */
    private final c f530a;

    public f(c cVar) {
        this.f530a = cVar;
    }

    /* JADX WARN: Type inference failed for: r4v0, types: [com.google.a.c.a<T>, com.google.a.c.a] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public <T> com.google.a.af<T> a(com.google.a.j r3, com.google.a.c.a<T> r4) {
        /*
            r2 = this;
            java.lang.Class r0 = r4.a()
            java.lang.Class<com.google.a.a.b> r1 = com.google.a.a.b.class
            java.lang.annotation.Annotation r0 = r0.getAnnotation(r1)
            com.google.a.a.b r0 = (com.google.a.a.b) r0
            if (r0 != 0) goto L_0x0010
            r0 = 0
        L_0x000f:
            return r0
        L_0x0010:
            com.google.a.b.c r1 = r2.f530a
            com.google.a.af r0 = a(r1, r3, r4, r0)
            goto L_0x000f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.a.b.a.f.a(com.google.a.j, com.google.a.c.a):com.google.a.af");
    }

    static af<?> a(c cVar, j jVar, a<?> aVar, b bVar) {
        af<?> a2;
        Class<?> a3 = bVar.a();
        if (af.class.isAssignableFrom(a3)) {
            a2 = (af) cVar.a(a.b(a3)).a();
        } else if (ah.class.isAssignableFrom(a3)) {
            a2 = ((ah) cVar.a(a.b(a3)).a()).a(jVar, aVar);
        } else {
            throw new IllegalArgumentException("@JsonAdapter value must be TypeAdapter or TypeAdapterFactory reference.");
        }
        if (a2 != null) {
            return a2.a();
        }
        return a2;
    }
}
