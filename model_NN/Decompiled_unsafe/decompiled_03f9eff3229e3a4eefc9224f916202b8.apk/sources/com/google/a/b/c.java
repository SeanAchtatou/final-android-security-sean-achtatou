package com.google.a.b;

import com.google.a.c.a;
import com.google.a.r;
import java.lang.reflect.Constructor;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.EnumSet;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentNavigableMap;

/* compiled from: ConstructorConstructor */
public final class c {

    /* renamed from: a  reason: collision with root package name */
    private final Map<Type, r<?>> f552a;

    public c(Map<Type, r<?>> map) {
        this.f552a = map;
    }

    public <T> z<T> a(a aVar) {
        Type b = aVar.b();
        Class a2 = aVar.a();
        r rVar = this.f552a.get(b);
        if (rVar != null) {
            return new d(this, rVar, b);
        }
        r rVar2 = this.f552a.get(a2);
        if (rVar2 != null) {
            return new j(this, rVar2, b);
        }
        z<T> a3 = a(a2);
        if (a3 != null) {
            return a3;
        }
        z<T> a4 = a(b, a2);
        return a4 == null ? b(b, a2) : a4;
    }

    private <T> z<T> a(Class cls) {
        try {
            Constructor declaredConstructor = cls.getDeclaredConstructor(new Class[0]);
            if (!declaredConstructor.isAccessible()) {
                declaredConstructor.setAccessible(true);
            }
            return new k(this, declaredConstructor);
        } catch (NoSuchMethodException e) {
            return null;
        }
    }

    private <T> z<T> a(Type type, Class<? super T> cls) {
        if (Collection.class.isAssignableFrom(cls)) {
            if (SortedSet.class.isAssignableFrom(cls)) {
                return new l(this);
            }
            if (EnumSet.class.isAssignableFrom(cls)) {
                return new m(this, type);
            }
            if (Set.class.isAssignableFrom(cls)) {
                return new n(this);
            }
            if (Queue.class.isAssignableFrom(cls)) {
                return new o(this);
            }
            return new p(this);
        } else if (!Map.class.isAssignableFrom(cls)) {
            return null;
        } else {
            if (ConcurrentNavigableMap.class.isAssignableFrom(cls)) {
                return new q(this);
            }
            if (ConcurrentMap.class.isAssignableFrom(cls)) {
                return new e(this);
            }
            if (SortedMap.class.isAssignableFrom(cls)) {
                return new f(this);
            }
            if (!(type instanceof ParameterizedType) || String.class.isAssignableFrom(a.a(((ParameterizedType) type).getActualTypeArguments()[0]).a())) {
                return new h(this);
            }
            return new g(this);
        }
    }

    private <T> z<T> b(Type type, Class<? super T> cls) {
        return new i(this, cls, type);
    }

    public String toString() {
        return this.f552a.toString();
    }
}
