package com.google.a.b;

import java.math.BigDecimal;

/* compiled from: LazilyParsedNumber */
public final class u extends Number {

    /* renamed from: a  reason: collision with root package name */
    private final String f570a;

    public u(String str) {
        this.f570a = str;
    }

    public int intValue() {
        try {
            return Integer.parseInt(this.f570a);
        } catch (NumberFormatException e) {
            try {
                return (int) Long.parseLong(this.f570a);
            } catch (NumberFormatException e2) {
                return new BigDecimal(this.f570a).intValue();
            }
        }
    }

    public long longValue() {
        try {
            return Long.parseLong(this.f570a);
        } catch (NumberFormatException e) {
            return new BigDecimal(this.f570a).longValue();
        }
    }

    public float floatValue() {
        return Float.parseFloat(this.f570a);
    }

    public double doubleValue() {
        return Double.parseDouble(this.f570a);
    }

    public String toString() {
        return this.f570a;
    }

    public int hashCode() {
        return this.f570a.hashCode();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof u)) {
            return false;
        }
        u uVar = (u) obj;
        if (this.f570a == uVar.f570a || this.f570a.equals(uVar.f570a)) {
            return true;
        }
        return false;
    }
}
