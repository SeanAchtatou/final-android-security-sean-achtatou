package com.google.a.b.a;

import com.google.a.af;
import com.google.a.d.a;
import com.google.a.d.c;
import com.google.a.d.d;
import java.io.IOException;
import java.net.URL;

/* compiled from: TypeAdapters */
final class aj extends af<URL> {
    aj() {
    }

    /* renamed from: a */
    public URL b(a aVar) throws IOException {
        if (aVar.f() == c.NULL) {
            aVar.j();
            return null;
        }
        String h = aVar.h();
        if (!"null".equals(h)) {
            return new URL(h);
        }
        return null;
    }

    public void a(d dVar, URL url) throws IOException {
        dVar.b(url == null ? null : url.toExternalForm());
    }
}
