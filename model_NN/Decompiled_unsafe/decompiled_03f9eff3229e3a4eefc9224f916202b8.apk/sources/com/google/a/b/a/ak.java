package com.google.a.b.a;

import com.google.a.af;
import com.google.a.d.a;
import com.google.a.d.c;
import com.google.a.d.d;
import com.google.a.v;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

/* compiled from: TypeAdapters */
final class ak extends af<URI> {
    ak() {
    }

    /* renamed from: a */
    public URI b(a aVar) throws IOException {
        if (aVar.f() == c.NULL) {
            aVar.j();
            return null;
        }
        try {
            String h = aVar.h();
            if (!"null".equals(h)) {
                return new URI(h);
            }
            return null;
        } catch (URISyntaxException e) {
            throw new v(e);
        }
    }

    public void a(d dVar, URI uri) throws IOException {
        dVar.b(uri == null ? null : uri.toASCIIString());
    }
}
