package com.google.a.b.a;

import com.google.a.ab;
import com.google.a.d.a;
import com.google.a.d.c;
import com.google.a.d.d;
import java.io.IOException;
import java.math.BigInteger;

/* compiled from: TypeAdapters */
final class af extends com.google.a.af<BigInteger> {
    af() {
    }

    /* renamed from: a */
    public BigInteger b(a aVar) throws IOException {
        if (aVar.f() == c.NULL) {
            aVar.j();
            return null;
        }
        try {
            return new BigInteger(aVar.h());
        } catch (NumberFormatException e) {
            throw new ab(e);
        }
    }

    public void a(d dVar, BigInteger bigInteger) throws IOException {
        dVar.a(bigInteger);
    }
}
