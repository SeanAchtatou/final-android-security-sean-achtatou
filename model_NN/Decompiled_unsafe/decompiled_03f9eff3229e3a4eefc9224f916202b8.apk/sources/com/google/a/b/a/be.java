package com.google.a.b.a;

import com.google.a.ab;
import com.google.a.af;
import com.google.a.d.a;
import com.google.a.d.c;
import com.google.a.d.d;
import java.io.IOException;

/* compiled from: TypeAdapters */
final class be extends af<Number> {
    be() {
    }

    /* renamed from: a */
    public Number b(a aVar) throws IOException {
        if (aVar.f() == c.NULL) {
            aVar.j();
            return null;
        }
        try {
            return Integer.valueOf(aVar.m());
        } catch (NumberFormatException e) {
            throw new ab(e);
        }
    }

    public void a(d dVar, Number number) throws IOException {
        dVar.a(number);
    }
}
