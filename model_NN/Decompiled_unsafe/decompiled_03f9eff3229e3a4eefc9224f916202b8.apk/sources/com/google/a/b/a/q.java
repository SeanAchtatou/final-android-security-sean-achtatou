package com.google.a.b.a;

import com.google.a.ab;
import com.google.a.af;
import com.google.a.ah;
import com.google.a.d.a;
import com.google.a.d.c;
import com.google.a.d.d;
import java.io.IOException;
import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/* compiled from: SqlDateTypeAdapter */
public final class q extends af<Date> {

    /* renamed from: a  reason: collision with root package name */
    public static final ah f539a = new r();
    private final DateFormat b = new SimpleDateFormat("MMM d, yyyy");

    /* renamed from: a */
    public synchronized Date b(a aVar) throws IOException {
        Date date;
        if (aVar.f() == c.NULL) {
            aVar.j();
            date = null;
        } else {
            try {
                date = new Date(this.b.parse(aVar.h()).getTime());
            } catch (ParseException e) {
                throw new ab(e);
            }
        }
        return date;
    }

    public synchronized void a(d dVar, Date date) throws IOException {
        dVar.b(date == null ? null : this.b.format(date));
    }
}
