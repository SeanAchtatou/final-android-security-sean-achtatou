package com.google.a.b.a;

import com.google.a.af;
import com.google.a.d.a;
import com.google.a.d.c;
import com.google.a.d.d;
import java.io.IOException;

/* compiled from: TypeAdapters */
final class aa extends af<Number> {
    aa() {
    }

    /* renamed from: a */
    public Number b(a aVar) throws IOException {
        if (aVar.f() != c.NULL) {
            return Double.valueOf(aVar.k());
        }
        aVar.j();
        return null;
    }

    public void a(d dVar, Number number) throws IOException {
        dVar.a(number);
    }
}
