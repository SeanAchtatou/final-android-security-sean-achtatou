package com.google.a.b.a;

import com.google.a.af;
import com.google.a.ah;
import com.google.a.d.c;
import com.google.a.d.d;
import com.google.a.j;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;

/* compiled from: ArrayTypeAdapter */
public final class a<E> extends af<Object> {

    /* renamed from: a  reason: collision with root package name */
    public static final ah f518a = new b();
    private final Class<E> b;
    private final af<E> c;

    public a(j jVar, af<E> afVar, Class<E> cls) {
        this.c = new u(jVar, afVar, cls);
        this.b = cls;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int):java.lang.Object throws java.lang.NegativeArraySizeException}
     arg types: [java.lang.Class<E>, int]
     candidates:
      ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int[]):java.lang.Object VARARG throws java.lang.IllegalArgumentException, java.lang.NegativeArraySizeException}
      ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int):java.lang.Object throws java.lang.NegativeArraySizeException} */
    public Object b(com.google.a.d.a aVar) throws IOException {
        if (aVar.f() == c.NULL) {
            aVar.j();
            return null;
        }
        ArrayList arrayList = new ArrayList();
        aVar.a();
        while (aVar.e()) {
            arrayList.add(this.c.b(aVar));
        }
        aVar.b();
        Object newInstance = Array.newInstance((Class<?>) this.b, arrayList.size());
        for (int i = 0; i < arrayList.size(); i++) {
            Array.set(newInstance, i, arrayList.get(i));
        }
        return newInstance;
    }

    public void a(d dVar, Object obj) throws IOException {
        if (obj == null) {
            dVar.f();
            return;
        }
        dVar.b();
        int length = Array.getLength(obj);
        for (int i = 0; i < length; i++) {
            this.c.a(dVar, Array.get(obj, i));
        }
        dVar.c();
    }
}
