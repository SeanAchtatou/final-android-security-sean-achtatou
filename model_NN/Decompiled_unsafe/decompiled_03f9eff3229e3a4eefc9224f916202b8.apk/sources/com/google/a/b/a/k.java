package com.google.a.b.a;

import com.google.a.ab;
import com.google.a.af;
import com.google.a.ah;
import com.google.a.b.b;
import com.google.a.b.c;
import com.google.a.b.t;
import com.google.a.b.z;
import com.google.a.d.d;
import com.google.a.j;
import com.google.a.u;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Map;

/* compiled from: MapTypeAdapterFactory */
public final class k implements ah {

    /* renamed from: a  reason: collision with root package name */
    final boolean f532a;
    private final c b;

    public k(c cVar, boolean z) {
        this.b = cVar;
        this.f532a = z;
    }

    public <T> af<T> a(j jVar, com.google.a.c.a<T> aVar) {
        Type b2 = aVar.b();
        if (!Map.class.isAssignableFrom(aVar.a())) {
            return null;
        }
        Type[] b3 = b.b(b2, b.e(b2));
        return new a(jVar, b3[0], a(jVar, b3[0]), b3[1], jVar.a(com.google.a.c.a.a(b3[1])), this.b.a(aVar));
    }

    private af<?> a(j jVar, Type type) {
        if (type == Boolean.TYPE || type == Boolean.class) {
            return v.f;
        }
        return jVar.a(com.google.a.c.a.a(type));
    }

    /* compiled from: MapTypeAdapterFactory */
    private final class a<K, V> extends af<Map<K, V>> {
        private final af<K> b;
        private final af<V> c;
        private final z<? extends Map<K, V>> d;

        public a(j jVar, Type type, af<K> afVar, Type type2, af<V> afVar2, z<? extends Map<K, V>> zVar) {
            this.b = new u(jVar, afVar, type);
            this.c = new u(jVar, afVar2, type2);
            this.d = zVar;
        }

        /* renamed from: a */
        public Map<K, V> b(com.google.a.d.a aVar) throws IOException {
            com.google.a.d.c f = aVar.f();
            if (f == com.google.a.d.c.NULL) {
                aVar.j();
                return null;
            }
            Map<K, V> map = (Map) this.d.a();
            if (f == com.google.a.d.c.BEGIN_ARRAY) {
                aVar.a();
                while (aVar.e()) {
                    aVar.a();
                    K b2 = this.b.b(aVar);
                    if (map.put(b2, this.c.b(aVar)) != null) {
                        throw new ab("duplicate key: " + ((Object) b2));
                    }
                    aVar.b();
                }
                aVar.b();
                return map;
            }
            aVar.c();
            while (aVar.e()) {
                t.f569a.a(aVar);
                K b3 = this.b.b(aVar);
                if (map.put(b3, this.c.b(aVar)) != null) {
                    throw new ab("duplicate key: " + ((Object) b3));
                }
            }
            aVar.d();
            return map;
        }

        public void a(d dVar, Map<K, V> map) throws IOException {
            boolean z;
            int i = 0;
            if (map == null) {
                dVar.f();
            } else if (!k.this.f532a) {
                dVar.d();
                for (Map.Entry next : map.entrySet()) {
                    dVar.a(String.valueOf(next.getKey()));
                    this.c.a(dVar, next.getValue());
                }
                dVar.e();
            } else {
                ArrayList arrayList = new ArrayList(map.size());
                ArrayList arrayList2 = new ArrayList(map.size());
                boolean z2 = false;
                for (Map.Entry next2 : map.entrySet()) {
                    u a2 = this.b.a(next2.getKey());
                    arrayList.add(a2);
                    arrayList2.add(next2.getValue());
                    if (a2.g() || a2.h()) {
                        z = true;
                    } else {
                        z = false;
                    }
                    z2 = z | z2;
                }
                if (z2) {
                    dVar.b();
                    while (i < arrayList.size()) {
                        dVar.b();
                        com.google.a.b.ab.a((u) arrayList.get(i), dVar);
                        this.c.a(dVar, arrayList2.get(i));
                        dVar.c();
                        i++;
                    }
                    dVar.c();
                    return;
                }
                dVar.d();
                while (i < arrayList.size()) {
                    dVar.a(a((u) arrayList.get(i)));
                    this.c.a(dVar, arrayList2.get(i));
                    i++;
                }
                dVar.e();
            }
        }

        private String a(u uVar) {
            if (uVar.i()) {
                com.google.a.z m = uVar.m();
                if (m.p()) {
                    return String.valueOf(m.a());
                }
                if (m.o()) {
                    return Boolean.toString(m.f());
                }
                if (m.q()) {
                    return m.b();
                }
                throw new AssertionError();
            } else if (uVar.j()) {
                return "null";
            } else {
                throw new AssertionError();
            }
        }
    }
}
