package com.google.a.b.a;

import com.google.a.d.d;
import com.google.a.s;
import com.google.a.u;
import com.google.a.w;
import com.google.a.x;
import com.google.a.z;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

/* compiled from: JsonTreeWriter */
public final class i extends d {

    /* renamed from: a  reason: collision with root package name */
    private static final Writer f531a = new j();
    private static final z b = new z("closed");
    private final List<u> c = new ArrayList();
    private String d;
    private u e = w.f591a;

    public i() {
        super(f531a);
    }

    public u a() {
        if (this.c.isEmpty()) {
            return this.e;
        }
        throw new IllegalStateException("Expected one JSON element but was " + this.c);
    }

    private u i() {
        return this.c.get(this.c.size() - 1);
    }

    private void a(u uVar) {
        if (this.d != null) {
            if (!uVar.j() || h()) {
                ((x) i()).a(this.d, uVar);
            }
            this.d = null;
        } else if (this.c.isEmpty()) {
            this.e = uVar;
        } else {
            u i = i();
            if (i instanceof s) {
                ((s) i).a(uVar);
                return;
            }
            throw new IllegalStateException();
        }
    }

    public d b() throws IOException {
        s sVar = new s();
        a(sVar);
        this.c.add(sVar);
        return this;
    }

    public d c() throws IOException {
        if (this.c.isEmpty() || this.d != null) {
            throw new IllegalStateException();
        } else if (i() instanceof s) {
            this.c.remove(this.c.size() - 1);
            return this;
        } else {
            throw new IllegalStateException();
        }
    }

    public d d() throws IOException {
        x xVar = new x();
        a(xVar);
        this.c.add(xVar);
        return this;
    }

    public d e() throws IOException {
        if (this.c.isEmpty() || this.d != null) {
            throw new IllegalStateException();
        } else if (i() instanceof x) {
            this.c.remove(this.c.size() - 1);
            return this;
        } else {
            throw new IllegalStateException();
        }
    }

    public d a(String str) throws IOException {
        if (this.c.isEmpty() || this.d != null) {
            throw new IllegalStateException();
        } else if (i() instanceof x) {
            this.d = str;
            return this;
        } else {
            throw new IllegalStateException();
        }
    }

    public d b(String str) throws IOException {
        if (str == null) {
            return f();
        }
        a(new z(str));
        return this;
    }

    public d f() throws IOException {
        a(w.f591a);
        return this;
    }

    public d a(boolean z) throws IOException {
        a(new z(Boolean.valueOf(z)));
        return this;
    }

    public d a(long j) throws IOException {
        a(new z(Long.valueOf(j)));
        return this;
    }

    public d a(Number number) throws IOException {
        if (number == null) {
            return f();
        }
        if (!g()) {
            double doubleValue = number.doubleValue();
            if (Double.isNaN(doubleValue) || Double.isInfinite(doubleValue)) {
                throw new IllegalArgumentException("JSON forbids NaN and infinities: " + number);
            }
        }
        a(new z(number));
        return this;
    }

    public void flush() throws IOException {
    }

    public void close() throws IOException {
        if (!this.c.isEmpty()) {
            throw new IOException("Incomplete document");
        }
        this.c.add(b);
    }
}
