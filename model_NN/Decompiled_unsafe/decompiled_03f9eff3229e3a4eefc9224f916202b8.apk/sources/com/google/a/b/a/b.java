package com.google.a.b.a;

import com.google.a.af;
import com.google.a.ah;
import com.google.a.c.a;
import com.google.a.j;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.Type;

/* compiled from: ArrayTypeAdapter */
final class b implements ah {
    b() {
    }

    public <T> af<T> a(j jVar, a<T> aVar) {
        Type b = aVar.b();
        if (!(b instanceof GenericArrayType) && (!(b instanceof Class) || !((Class) b).isArray())) {
            return null;
        }
        Type g = com.google.a.b.b.g(b);
        return new a(jVar, jVar.a(a.a(g)), com.google.a.b.b.e(g));
    }
}
