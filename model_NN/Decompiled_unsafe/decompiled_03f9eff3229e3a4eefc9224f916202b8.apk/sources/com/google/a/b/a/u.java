package com.google.a.b.a;

import com.google.a.af;
import com.google.a.b.a.o;
import com.google.a.d.a;
import com.google.a.d.d;
import com.google.a.j;
import java.io.IOException;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;

/* compiled from: TypeAdapterRuntimeTypeWrapper */
final class u<T> extends af<T> {

    /* renamed from: a  reason: collision with root package name */
    private final j f541a;
    private final af<T> b;
    private final Type c;

    u(j jVar, af<T> afVar, Type type) {
        this.f541a = jVar;
        this.b = afVar;
        this.c = type;
    }

    public T b(a aVar) throws IOException {
        return this.b.b(aVar);
    }

    public void a(d dVar, T t) throws IOException {
        af<T> afVar = this.b;
        Type a2 = a(this.c, t);
        if (a2 != this.c) {
            afVar = this.f541a.a(com.google.a.c.a.a(a2));
            if ((afVar instanceof o.a) && !(this.b instanceof o.a)) {
                afVar = this.b;
            }
        }
        afVar.a(dVar, t);
    }

    private Type a(Type type, Object obj) {
        if (obj == null) {
            return type;
        }
        if (type == Object.class || (type instanceof TypeVariable) || (type instanceof Class)) {
            return obj.getClass();
        }
        return type;
    }
}
