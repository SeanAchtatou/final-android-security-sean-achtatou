package com.google.a.b.a;

import com.google.a.af;
import com.google.a.d.a;
import com.google.a.d.c;
import com.google.a.d.d;
import java.io.IOException;
import java.net.InetAddress;

/* compiled from: TypeAdapters */
final class al extends af<InetAddress> {
    al() {
    }

    /* renamed from: a */
    public InetAddress b(a aVar) throws IOException {
        if (aVar.f() != c.NULL) {
            return InetAddress.getByName(aVar.h());
        }
        aVar.j();
        return null;
    }

    public void a(d dVar, InetAddress inetAddress) throws IOException {
        dVar.b(inetAddress == null ? null : inetAddress.getHostAddress());
    }
}
