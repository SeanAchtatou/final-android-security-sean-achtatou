package com.google.a.b.a;

import com.google.a.af;
import com.google.a.d.a;
import com.google.a.d.c;
import com.google.a.d.d;
import java.io.IOException;

/* compiled from: TypeAdapters */
final class z extends af<Number> {
    z() {
    }

    /* renamed from: a */
    public Number b(a aVar) throws IOException {
        if (aVar.f() != c.NULL) {
            return Float.valueOf((float) aVar.k());
        }
        aVar.j();
        return null;
    }

    public void a(d dVar, Number number) throws IOException {
        dVar.a(number);
    }
}
