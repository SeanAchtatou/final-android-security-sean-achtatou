package com.google.a.b.a;

import com.google.a.af;
import com.google.a.d.a;
import com.google.a.d.c;
import com.google.a.d.d;
import java.io.IOException;
import java.util.UUID;

/* compiled from: TypeAdapters */
final class am extends af<UUID> {
    am() {
    }

    /* renamed from: a */
    public UUID b(a aVar) throws IOException {
        if (aVar.f() != c.NULL) {
            return UUID.fromString(aVar.h());
        }
        aVar.j();
        return null;
    }

    public void a(d dVar, UUID uuid) throws IOException {
        dVar.b(uuid == null ? null : uuid.toString());
    }
}
