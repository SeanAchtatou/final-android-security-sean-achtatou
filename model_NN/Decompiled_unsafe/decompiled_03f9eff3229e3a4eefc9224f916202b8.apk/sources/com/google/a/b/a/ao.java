package com.google.a.b.a;

import com.google.a.af;
import com.google.a.ah;
import com.google.a.c.a;
import com.google.a.j;
import java.sql.Timestamp;
import java.util.Date;

/* compiled from: TypeAdapters */
final class ao implements ah {
    ao() {
    }

    public <T> af<T> a(j jVar, a<T> aVar) {
        if (aVar.a() != Timestamp.class) {
            return null;
        }
        return new ap(this, jVar.a(Date.class));
    }
}
