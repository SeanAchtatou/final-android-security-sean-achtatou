package com.google.a.b.a;

import com.google.a.d.a;
import com.google.a.d.c;
import com.google.a.s;
import com.google.a.w;
import com.google.a.x;
import com.google.a.z;
import java.io.IOException;
import java.io.Reader;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* compiled from: JsonTreeReader */
public final class g extends a {
    private static final Reader b = new h();
    private static final Object c = new Object();
    private final List<Object> d;

    public void a() throws IOException {
        a(c.BEGIN_ARRAY);
        this.d.add(((s) u()).iterator());
    }

    public void b() throws IOException {
        a(c.END_ARRAY);
        v();
        v();
    }

    public void c() throws IOException {
        a(c.BEGIN_OBJECT);
        this.d.add(((x) u()).o().iterator());
    }

    public void d() throws IOException {
        a(c.END_OBJECT);
        v();
        v();
    }

    public boolean e() throws IOException {
        c f = f();
        return (f == c.END_OBJECT || f == c.END_ARRAY) ? false : true;
    }

    public c f() throws IOException {
        if (this.d.isEmpty()) {
            return c.END_DOCUMENT;
        }
        Object u = u();
        if (u instanceof Iterator) {
            boolean z = this.d.get(this.d.size() - 2) instanceof x;
            Iterator it = (Iterator) u;
            if (!it.hasNext()) {
                return z ? c.END_OBJECT : c.END_ARRAY;
            }
            if (z) {
                return c.NAME;
            }
            this.d.add(it.next());
            return f();
        } else if (u instanceof x) {
            return c.BEGIN_OBJECT;
        } else {
            if (u instanceof s) {
                return c.BEGIN_ARRAY;
            }
            if (u instanceof z) {
                z zVar = (z) u;
                if (zVar.q()) {
                    return c.STRING;
                }
                if (zVar.o()) {
                    return c.BOOLEAN;
                }
                if (zVar.p()) {
                    return c.NUMBER;
                }
                throw new AssertionError();
            } else if (u instanceof w) {
                return c.NULL;
            } else {
                if (u == c) {
                    throw new IllegalStateException("JsonReader is closed");
                }
                throw new AssertionError();
            }
        }
    }

    private Object u() {
        return this.d.get(this.d.size() - 1);
    }

    private Object v() {
        return this.d.remove(this.d.size() - 1);
    }

    private void a(c cVar) throws IOException {
        if (f() != cVar) {
            throw new IllegalStateException("Expected " + cVar + " but was " + f());
        }
    }

    public String g() throws IOException {
        a(c.NAME);
        Map.Entry entry = (Map.Entry) ((Iterator) u()).next();
        this.d.add(entry.getValue());
        return (String) entry.getKey();
    }

    public String h() throws IOException {
        c f = f();
        if (f == c.STRING || f == c.NUMBER) {
            return ((z) v()).b();
        }
        throw new IllegalStateException("Expected " + c.STRING + " but was " + f);
    }

    public boolean i() throws IOException {
        a(c.BOOLEAN);
        return ((z) v()).f();
    }

    public void j() throws IOException {
        a(c.NULL);
        v();
    }

    public double k() throws IOException {
        c f = f();
        if (f == c.NUMBER || f == c.STRING) {
            double c2 = ((z) u()).c();
            if (p() || (!Double.isNaN(c2) && !Double.isInfinite(c2))) {
                v();
                return c2;
            }
            throw new NumberFormatException("JSON forbids NaN and infinities: " + c2);
        }
        throw new IllegalStateException("Expected " + c.NUMBER + " but was " + f);
    }

    public long l() throws IOException {
        c f = f();
        if (f == c.NUMBER || f == c.STRING) {
            long d2 = ((z) u()).d();
            v();
            return d2;
        }
        throw new IllegalStateException("Expected " + c.NUMBER + " but was " + f);
    }

    public int m() throws IOException {
        c f = f();
        if (f == c.NUMBER || f == c.STRING) {
            int e = ((z) u()).e();
            v();
            return e;
        }
        throw new IllegalStateException("Expected " + c.NUMBER + " but was " + f);
    }

    public void close() throws IOException {
        this.d.clear();
        this.d.add(c);
    }

    public void n() throws IOException {
        if (f() == c.NAME) {
            g();
        } else {
            v();
        }
    }

    public String toString() {
        return getClass().getSimpleName();
    }

    public void o() throws IOException {
        a(c.NAME);
        Map.Entry entry = (Map.Entry) ((Iterator) u()).next();
        this.d.add(entry.getValue());
        this.d.add(new z((String) entry.getKey()));
    }
}
