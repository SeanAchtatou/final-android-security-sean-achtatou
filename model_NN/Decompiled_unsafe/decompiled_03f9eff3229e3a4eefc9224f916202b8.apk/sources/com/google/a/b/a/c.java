package com.google.a.b.a;

import com.google.a.af;
import com.google.a.ah;
import com.google.a.b.b;
import com.google.a.b.z;
import com.google.a.d.d;
import com.google.a.j;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Collection;

/* compiled from: CollectionTypeAdapterFactory */
public final class c implements ah {

    /* renamed from: a  reason: collision with root package name */
    private final com.google.a.b.c f527a;

    public c(com.google.a.b.c cVar) {
        this.f527a = cVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.a.b.b.a(java.lang.reflect.Type, java.lang.Class<?>):java.lang.reflect.Type
     arg types: [java.lang.reflect.Type, java.lang.Class<? super T>]
     candidates:
      com.google.a.b.b.a(java.lang.Object[], java.lang.Object):int
      com.google.a.b.b.a(java.lang.Object, java.lang.Object):boolean
      com.google.a.b.b.a(java.lang.reflect.Type, java.lang.reflect.Type):boolean
      com.google.a.b.b.a(java.lang.reflect.Type, java.lang.Class<?>):java.lang.reflect.Type */
    public <T> af<T> a(j jVar, com.google.a.c.a<T> aVar) {
        Type b = aVar.b();
        Class<? super T> a2 = aVar.a();
        if (!Collection.class.isAssignableFrom(a2)) {
            return null;
        }
        Type a3 = b.a(b, (Class<?>) a2);
        return new a(jVar, a3, jVar.a(com.google.a.c.a.a(a3)), this.f527a.a(aVar));
    }

    /* compiled from: CollectionTypeAdapterFactory */
    private static final class a<E> extends af<Collection<E>> {

        /* renamed from: a  reason: collision with root package name */
        private final af<E> f528a;
        private final z<? extends Collection<E>> b;

        public a(j jVar, Type type, af<E> afVar, z<? extends Collection<E>> zVar) {
            this.f528a = new u(jVar, afVar, type);
            this.b = zVar;
        }

        /* renamed from: a */
        public Collection<E> b(com.google.a.d.a aVar) throws IOException {
            if (aVar.f() == com.google.a.d.c.NULL) {
                aVar.j();
                return null;
            }
            Collection<E> collection = (Collection) this.b.a();
            aVar.a();
            while (aVar.e()) {
                collection.add(this.f528a.b(aVar));
            }
            aVar.b();
            return collection;
        }

        public void a(d dVar, Collection<E> collection) throws IOException {
            if (collection == null) {
                dVar.f();
                return;
            }
            dVar.b();
            for (E a2 : collection) {
                this.f528a.a(dVar, a2);
            }
            dVar.c();
        }
    }
}
