package com.google.a.b;

import com.google.a.a;
import com.google.a.a.d;
import com.google.a.a.e;
import com.google.a.af;
import com.google.a.ah;
import com.google.a.b;
import com.google.a.j;
import java.lang.reflect.Field;
import java.util.Collections;
import java.util.List;

/* compiled from: Excluder */
public final class r implements ah, Cloneable {

    /* renamed from: a  reason: collision with root package name */
    public static final r f567a = new r();
    private double b = -1.0d;
    private int c = 136;
    private boolean d = true;
    private boolean e;
    private List<a> f = Collections.emptyList();
    private List<a> g = Collections.emptyList();

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public r clone() {
        try {
            return (r) super.clone();
        } catch (CloneNotSupportedException e2) {
            throw new AssertionError(e2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.a.b.r.a(java.lang.Class<?>, boolean):boolean
     arg types: [java.lang.Class<? super T>, int]
     candidates:
      com.google.a.b.r.a(com.google.a.a.d, com.google.a.a.e):boolean
      com.google.a.b.r.a(com.google.a.j, com.google.a.c.a):com.google.a.af<T>
      com.google.a.b.r.a(java.lang.reflect.Field, boolean):boolean
      com.google.a.ah.a(com.google.a.j, com.google.a.c.a):com.google.a.af<T>
      com.google.a.b.r.a(java.lang.Class<?>, boolean):boolean */
    public <T> af<T> a(j jVar, com.google.a.c.a<T> aVar) {
        Class<? super T> a2 = aVar.a();
        boolean a3 = a((Class<?>) a2, true);
        boolean a4 = a((Class<?>) a2, false);
        if (a3 || a4) {
            return new s(this, a4, a3, jVar, aVar);
        }
        return null;
    }

    public boolean a(Field field, boolean z) {
        com.google.a.a.a aVar;
        if ((this.c & field.getModifiers()) != 0) {
            return true;
        }
        if (this.b != -1.0d && !a((d) field.getAnnotation(d.class), (e) field.getAnnotation(e.class))) {
            return true;
        }
        if (field.isSynthetic()) {
            return true;
        }
        if (this.e && ((aVar = (com.google.a.a.a) field.getAnnotation(com.google.a.a.a.class)) == null || (!z ? !aVar.b() : !aVar.a()))) {
            return true;
        }
        if (!this.d && b(field.getType())) {
            return true;
        }
        if (a(field.getType())) {
            return true;
        }
        List<a> list = z ? this.f : this.g;
        if (!list.isEmpty()) {
            b bVar = new b(field);
            for (a a2 : list) {
                if (a2.a(bVar)) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean a(Class<?> cls, boolean z) {
        if (this.b != -1.0d && !a((d) cls.getAnnotation(d.class), (e) cls.getAnnotation(e.class))) {
            return true;
        }
        if (!this.d && b(cls)) {
            return true;
        }
        if (a(cls)) {
            return true;
        }
        for (a a2 : z ? this.f : this.g) {
            if (a2.a(cls)) {
                return true;
            }
        }
        return false;
    }

    private boolean a(Class<?> cls) {
        return !Enum.class.isAssignableFrom(cls) && (cls.isAnonymousClass() || cls.isLocalClass());
    }

    private boolean b(Class<?> cls) {
        return cls.isMemberClass() && !c(cls);
    }

    private boolean c(Class<?> cls) {
        return (cls.getModifiers() & 8) != 0;
    }

    private boolean a(d dVar, e eVar) {
        return a(dVar) && a(eVar);
    }

    private boolean a(d dVar) {
        if (dVar == null || dVar.a() <= this.b) {
            return true;
        }
        return false;
    }

    private boolean a(e eVar) {
        if (eVar == null || eVar.a() > this.b) {
            return true;
        }
        return false;
    }
}
