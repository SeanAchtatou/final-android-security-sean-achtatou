package com.google.a.b;

import java.lang.reflect.Method;

/* compiled from: UnsafeAllocator */
final class ad extends ac {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Method f545a;
    final /* synthetic */ Object b;

    ad(Method method, Object obj) {
        this.f545a = method;
        this.b = obj;
    }

    public <T> T a(Class<T> cls) throws Exception {
        return this.f545a.invoke(this.b, cls);
    }
}
