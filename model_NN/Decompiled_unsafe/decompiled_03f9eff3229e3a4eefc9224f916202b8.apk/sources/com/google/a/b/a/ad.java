package com.google.a.b.a;

import com.google.a.af;
import com.google.a.d.a;
import com.google.a.d.c;
import com.google.a.d.d;
import java.io.IOException;

/* compiled from: TypeAdapters */
final class ad extends af<String> {
    ad() {
    }

    /* renamed from: a */
    public String b(a aVar) throws IOException {
        c f = aVar.f();
        if (f == c.NULL) {
            aVar.j();
            return null;
        } else if (f == c.BOOLEAN) {
            return Boolean.toString(aVar.i());
        } else {
            return aVar.h();
        }
    }

    public void a(d dVar, String str) throws IOException {
        dVar.b(str);
    }
}
