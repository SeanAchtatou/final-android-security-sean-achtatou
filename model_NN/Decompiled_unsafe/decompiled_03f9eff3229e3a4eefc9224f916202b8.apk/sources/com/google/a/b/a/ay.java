package com.google.a.b.a;

import com.google.a.af;
import com.google.a.ah;
import com.google.a.c.a;
import com.google.a.j;

/* compiled from: TypeAdapters */
final class ay implements ah {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Class f524a;
    final /* synthetic */ af b;

    ay(Class cls, af afVar) {
        this.f524a = cls;
        this.b = afVar;
    }

    public <T2> af<T2> a(j jVar, a<T2> aVar) {
        Class<? super T2> a2 = aVar.a();
        if (!this.f524a.isAssignableFrom(a2)) {
            return null;
        }
        return new az(this, a2);
    }

    public String toString() {
        return "Factory[typeHierarchy=" + this.f524a.getName() + ",adapter=" + this.b + "]";
    }
}
