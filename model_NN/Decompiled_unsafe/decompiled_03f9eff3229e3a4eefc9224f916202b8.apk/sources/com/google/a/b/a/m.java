package com.google.a.b.a;

import com.google.a.af;
import com.google.a.ah;
import com.google.a.c.a;
import com.google.a.j;

/* compiled from: ObjectTypeAdapter */
final class m implements ah {
    m() {
    }

    public <T> af<T> a(j jVar, a<T> aVar) {
        if (aVar.a() == Object.class) {
            return new l(jVar);
        }
        return null;
    }
}
