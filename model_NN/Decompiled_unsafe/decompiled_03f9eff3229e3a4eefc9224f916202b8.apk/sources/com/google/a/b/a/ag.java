package com.google.a.b.a;

import com.google.a.af;
import com.google.a.d.a;
import com.google.a.d.c;
import com.google.a.d.d;
import java.io.IOException;

/* compiled from: TypeAdapters */
final class ag extends af<StringBuilder> {
    ag() {
    }

    /* renamed from: a */
    public StringBuilder b(a aVar) throws IOException {
        if (aVar.f() != c.NULL) {
            return new StringBuilder(aVar.h());
        }
        aVar.j();
        return null;
    }

    public void a(d dVar, StringBuilder sb) throws IOException {
        dVar.b(sb == null ? null : sb.toString());
    }
}
