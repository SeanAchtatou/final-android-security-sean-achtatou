package com.google.a.b.a;

import com.google.a.af;
import com.google.a.d.a;
import com.google.a.d.c;
import com.google.a.d.d;
import java.io.IOException;
import java.util.Locale;
import java.util.StringTokenizer;

/* compiled from: TypeAdapters */
final class ar extends af<Locale> {
    ar() {
    }

    /* renamed from: a */
    public Locale b(a aVar) throws IOException {
        String str;
        String str2;
        String str3;
        if (aVar.f() == c.NULL) {
            aVar.j();
            return null;
        }
        StringTokenizer stringTokenizer = new StringTokenizer(aVar.h(), "_");
        if (stringTokenizer.hasMoreElements()) {
            str = stringTokenizer.nextToken();
        } else {
            str = null;
        }
        if (stringTokenizer.hasMoreElements()) {
            str2 = stringTokenizer.nextToken();
        } else {
            str2 = null;
        }
        if (stringTokenizer.hasMoreElements()) {
            str3 = stringTokenizer.nextToken();
        } else {
            str3 = null;
        }
        if (str2 == null && str3 == null) {
            return new Locale(str);
        }
        if (str3 == null) {
            return new Locale(str, str2);
        }
        return new Locale(str, str2, str3);
    }

    public void a(d dVar, Locale locale) throws IOException {
        dVar.b(locale == null ? null : locale.toString());
    }
}
