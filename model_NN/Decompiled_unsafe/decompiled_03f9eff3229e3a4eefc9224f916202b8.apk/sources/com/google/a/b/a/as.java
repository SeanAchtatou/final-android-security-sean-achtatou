package com.google.a.b.a;

import com.google.a.af;
import com.google.a.d.a;
import com.google.a.d.d;
import com.google.a.s;
import com.google.a.u;
import com.google.a.w;
import com.google.a.x;
import com.google.a.z;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

/* compiled from: TypeAdapters */
final class as extends af<u> {
    as() {
    }

    /* renamed from: a */
    public u b(a aVar) throws IOException {
        switch (aVar.f()) {
            case NUMBER:
                return new z(new com.google.a.b.u(aVar.h()));
            case BOOLEAN:
                return new z(Boolean.valueOf(aVar.i()));
            case STRING:
                return new z(aVar.h());
            case NULL:
                aVar.j();
                return w.f591a;
            case BEGIN_ARRAY:
                s sVar = new s();
                aVar.a();
                while (aVar.e()) {
                    sVar.a(b(aVar));
                }
                aVar.b();
                return sVar;
            case BEGIN_OBJECT:
                x xVar = new x();
                aVar.c();
                while (aVar.e()) {
                    xVar.a(aVar.g(), b(aVar));
                }
                aVar.d();
                return xVar;
            default:
                throw new IllegalArgumentException();
        }
    }

    public void a(d dVar, u uVar) throws IOException {
        if (uVar == null || uVar.j()) {
            dVar.f();
        } else if (uVar.i()) {
            z m = uVar.m();
            if (m.p()) {
                dVar.a(m.a());
            } else if (m.o()) {
                dVar.a(m.f());
            } else {
                dVar.b(m.b());
            }
        } else if (uVar.g()) {
            dVar.b();
            Iterator<u> it = uVar.l().iterator();
            while (it.hasNext()) {
                a(dVar, it.next());
            }
            dVar.c();
        } else if (uVar.h()) {
            dVar.d();
            for (Map.Entry next : uVar.k().o()) {
                dVar.a((String) next.getKey());
                a(dVar, (u) next.getValue());
            }
            dVar.e();
        } else {
            throw new IllegalArgumentException("Couldn't write " + uVar.getClass());
        }
    }
}
