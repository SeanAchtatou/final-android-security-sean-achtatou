package com.google.a.b.a;

import com.google.a.af;
import com.google.a.d.a;
import com.google.a.d.c;
import com.google.a.d.d;
import java.io.IOException;

/* compiled from: TypeAdapters */
final class at extends af<Boolean> {
    at() {
    }

    /* renamed from: a */
    public Boolean b(a aVar) throws IOException {
        if (aVar.f() == c.NULL) {
            aVar.j();
            return null;
        } else if (aVar.f() == c.STRING) {
            return Boolean.valueOf(Boolean.parseBoolean(aVar.h()));
        } else {
            return Boolean.valueOf(aVar.i());
        }
    }

    public void a(d dVar, Boolean bool) throws IOException {
        if (bool == null) {
            dVar.f();
        } else {
            dVar.a(bool.booleanValue());
        }
    }
}
