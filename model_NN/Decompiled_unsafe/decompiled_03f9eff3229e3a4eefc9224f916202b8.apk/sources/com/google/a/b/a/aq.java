package com.google.a.b.a;

import com.google.a.af;
import com.google.a.d.a;
import com.google.a.d.c;
import com.google.a.d.d;
import java.io.IOException;
import java.util.Calendar;
import java.util.GregorianCalendar;

/* compiled from: TypeAdapters */
final class aq extends af<Calendar> {
    aq() {
    }

    /* renamed from: a */
    public Calendar b(a aVar) throws IOException {
        int i = 0;
        if (aVar.f() == c.NULL) {
            aVar.j();
            return null;
        }
        aVar.c();
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        int i5 = 0;
        int i6 = 0;
        while (aVar.f() != c.END_OBJECT) {
            String g = aVar.g();
            int m = aVar.m();
            if ("year".equals(g)) {
                i6 = m;
            } else if ("month".equals(g)) {
                i5 = m;
            } else if ("dayOfMonth".equals(g)) {
                i4 = m;
            } else if ("hourOfDay".equals(g)) {
                i3 = m;
            } else if ("minute".equals(g)) {
                i2 = m;
            } else if ("second".equals(g)) {
                i = m;
            }
        }
        aVar.d();
        return new GregorianCalendar(i6, i5, i4, i3, i2, i);
    }

    public void a(d dVar, Calendar calendar) throws IOException {
        if (calendar == null) {
            dVar.f();
            return;
        }
        dVar.d();
        dVar.a("year");
        dVar.a((long) calendar.get(1));
        dVar.a("month");
        dVar.a((long) calendar.get(2));
        dVar.a("dayOfMonth");
        dVar.a((long) calendar.get(5));
        dVar.a("hourOfDay");
        dVar.a((long) calendar.get(11));
        dVar.a("minute");
        dVar.a((long) calendar.get(12));
        dVar.a("second");
        dVar.a((long) calendar.get(13));
        dVar.e();
    }
}
