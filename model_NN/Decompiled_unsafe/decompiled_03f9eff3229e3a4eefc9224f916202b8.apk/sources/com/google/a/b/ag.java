package com.google.a.b;

/* compiled from: UnsafeAllocator */
final class ag extends ac {
    ag() {
    }

    public <T> T a(Class<T> cls) {
        throw new UnsupportedOperationException("Cannot allocate " + cls);
    }
}
