package com.google.a.b.a;

import com.google.a.af;
import com.google.a.d.a;
import com.google.a.d.c;
import com.google.a.d.d;
import java.io.IOException;

/* compiled from: TypeAdapters */
final class ai extends af<StringBuffer> {
    ai() {
    }

    /* renamed from: a */
    public StringBuffer b(a aVar) throws IOException {
        if (aVar.f() != c.NULL) {
            return new StringBuffer(aVar.h());
        }
        aVar.j();
        return null;
    }

    public void a(d dVar, StringBuffer stringBuffer) throws IOException {
        dVar.b(stringBuffer == null ? null : stringBuffer.toString());
    }
}
