package com.google.a.b.a;

import com.google.a.af;
import com.google.a.b.u;
import com.google.a.d.a;
import com.google.a.d.c;
import com.google.a.d.d;
import java.io.IOException;

/* compiled from: TypeAdapters */
final class ab extends af<Number> {
    ab() {
    }

    /* renamed from: a */
    public Number b(a aVar) throws IOException {
        c f = aVar.f();
        switch (ba.f526a[f.ordinal()]) {
            case 1:
                return new u(aVar.h());
            case 2:
            case 3:
            default:
                throw new com.google.a.ab("Expecting number, got: " + f);
            case 4:
                aVar.j();
                return null;
        }
    }

    public void a(d dVar, Number number) throws IOException {
        dVar.a(number);
    }
}
