package com.google.a.b.a;

import com.google.a.af;
import com.google.a.ah;
import com.google.a.b.a.v;
import com.google.a.c.a;
import com.google.a.j;

/* compiled from: TypeAdapters */
final class au implements ah {
    au() {
    }

    public <T> af<T> a(j jVar, a<T> aVar) {
        Class<? super T> a2 = aVar.a();
        if (!Enum.class.isAssignableFrom(a2) || a2 == Enum.class) {
            return null;
        }
        if (!a2.isEnum()) {
            a2 = a2.getSuperclass();
        }
        return new v.a(a2);
    }
}
