package com.google.a.b.a;

import com.google.a.af;
import com.google.a.ah;
import com.google.a.c.a;
import com.google.a.j;
import java.util.Date;

/* compiled from: DateTypeAdapter */
final class e implements ah {
    e() {
    }

    public <T> af<T> a(j jVar, a<T> aVar) {
        if (aVar.a() == Date.class) {
            return new d();
        }
        return null;
    }
}
