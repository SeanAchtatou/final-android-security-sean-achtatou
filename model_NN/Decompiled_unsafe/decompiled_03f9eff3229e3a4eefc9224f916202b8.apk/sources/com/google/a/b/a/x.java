package com.google.a.b.a;

import com.google.a.ab;
import com.google.a.af;
import com.google.a.d.a;
import com.google.a.d.d;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicIntegerArray;

/* compiled from: TypeAdapters */
final class x extends af<AtomicIntegerArray> {
    x() {
    }

    /* renamed from: a */
    public AtomicIntegerArray b(a aVar) throws IOException {
        ArrayList arrayList = new ArrayList();
        aVar.a();
        while (aVar.e()) {
            try {
                arrayList.add(Integer.valueOf(aVar.m()));
            } catch (NumberFormatException e) {
                throw new ab(e);
            }
        }
        aVar.b();
        int size = arrayList.size();
        AtomicIntegerArray atomicIntegerArray = new AtomicIntegerArray(size);
        for (int i = 0; i < size; i++) {
            atomicIntegerArray.set(i, ((Integer) arrayList.get(i)).intValue());
        }
        return atomicIntegerArray;
    }

    public void a(d dVar, AtomicIntegerArray atomicIntegerArray) throws IOException {
        dVar.b();
        int length = atomicIntegerArray.length();
        for (int i = 0; i < length; i++) {
            dVar.a((long) atomicIntegerArray.get(i));
        }
        dVar.c();
    }
}
