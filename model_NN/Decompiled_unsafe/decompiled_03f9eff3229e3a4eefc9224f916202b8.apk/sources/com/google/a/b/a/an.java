package com.google.a.b.a;

import com.google.a.af;
import com.google.a.d.a;
import com.google.a.d.d;
import java.io.IOException;
import java.util.Currency;

/* compiled from: TypeAdapters */
final class an extends af<Currency> {
    an() {
    }

    /* renamed from: a */
    public Currency b(a aVar) throws IOException {
        return Currency.getInstance(aVar.h());
    }

    public void a(d dVar, Currency currency) throws IOException {
        dVar.b(currency.getCurrencyCode());
    }
}
