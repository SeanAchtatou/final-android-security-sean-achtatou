package com.google.a.b;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/* compiled from: ConstructorConstructor */
class k implements z<T> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Constructor f560a;
    final /* synthetic */ c b;

    k(c cVar, Constructor constructor) {
        this.b = cVar;
        this.f560a = constructor;
    }

    public T a() {
        try {
            return this.f560a.newInstance(null);
        } catch (InstantiationException e) {
            throw new RuntimeException("Failed to invoke " + this.f560a + " with no args", e);
        } catch (InvocationTargetException e2) {
            throw new RuntimeException("Failed to invoke " + this.f560a + " with no args", e2.getTargetException());
        } catch (IllegalAccessException e3) {
            throw new AssertionError(e3);
        }
    }
}
