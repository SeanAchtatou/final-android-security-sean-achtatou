package com.google.a.b;

import java.lang.reflect.Method;

/* compiled from: UnsafeAllocator */
final class ae extends ac {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Method f546a;
    final /* synthetic */ int b;

    ae(Method method, int i) {
        this.f546a = method;
        this.b = i;
    }

    public <T> T a(Class<T> cls) throws Exception {
        return this.f546a.invoke(null, cls, Integer.valueOf(this.b));
    }
}
