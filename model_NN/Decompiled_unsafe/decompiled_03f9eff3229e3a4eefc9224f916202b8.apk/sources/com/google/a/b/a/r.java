package com.google.a.b.a;

import com.google.a.af;
import com.google.a.ah;
import com.google.a.c.a;
import com.google.a.j;
import java.sql.Date;

/* compiled from: SqlDateTypeAdapter */
final class r implements ah {
    r() {
    }

    public <T> af<T> a(j jVar, a<T> aVar) {
        if (aVar.a() == Date.class) {
            return new q();
        }
        return null;
    }
}
