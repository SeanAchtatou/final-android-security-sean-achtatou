package com.google.a.b.a;

import com.google.a.ab;
import com.google.a.af;
import com.google.a.d.a;
import com.google.a.d.c;
import com.google.a.d.d;
import java.io.IOException;

/* compiled from: TypeAdapters */
final class ac extends af<Character> {
    ac() {
    }

    /* renamed from: a */
    public Character b(a aVar) throws IOException {
        if (aVar.f() == c.NULL) {
            aVar.j();
            return null;
        }
        String h = aVar.h();
        if (h.length() == 1) {
            return Character.valueOf(h.charAt(0));
        }
        throw new ab("Expecting character, got: " + h);
    }

    public void a(d dVar, Character ch) throws IOException {
        dVar.b(ch == null ? null : String.valueOf(ch));
    }
}
