package com.google.a.b.a;

import com.google.a.ab;
import com.google.a.af;
import com.google.a.d.a;
import com.google.a.d.d;
import java.io.IOException;

/* compiled from: TypeAdapters */
class az extends af<T1> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Class f525a;
    final /* synthetic */ ay b;

    az(ay ayVar, Class cls) {
        this.b = ayVar;
        this.f525a = cls;
    }

    public void a(d dVar, T1 t1) throws IOException {
        this.b.b.a(dVar, t1);
    }

    public T1 b(a aVar) throws IOException {
        T1 b2 = this.b.b.b(aVar);
        if (b2 == null || this.f525a.isInstance(b2)) {
            return b2;
        }
        throw new ab("Expected a " + this.f525a.getName() + " but was " + b2.getClass().getName());
    }
}
