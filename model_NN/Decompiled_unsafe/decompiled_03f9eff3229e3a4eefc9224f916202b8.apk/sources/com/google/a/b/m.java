package com.google.a.b;

import com.google.a.v;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.EnumSet;

/* compiled from: ConstructorConstructor */
class m implements z<T> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Type f562a;
    final /* synthetic */ c b;

    m(c cVar, Type type) {
        this.b = cVar;
        this.f562a = type;
    }

    public T a() {
        if (this.f562a instanceof ParameterizedType) {
            Type type = ((ParameterizedType) this.f562a).getActualTypeArguments()[0];
            if (type instanceof Class) {
                return EnumSet.noneOf((Class) type);
            }
            throw new v("Invalid EnumSet type: " + this.f562a.toString());
        }
        throw new v("Invalid EnumSet type: " + this.f562a.toString());
    }
}
