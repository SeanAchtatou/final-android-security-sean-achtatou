package com.google.a.b.a;

import com.google.a.af;
import com.google.a.ah;
import com.google.a.b.v;
import com.google.a.d.a;
import com.google.a.d.d;
import com.google.a.j;
import java.io.IOException;
import java.util.ArrayList;

/* compiled from: ObjectTypeAdapter */
public final class l extends af<Object> {

    /* renamed from: a  reason: collision with root package name */
    public static final ah f534a = new m();
    private final j b;

    l(j jVar) {
        this.b = jVar;
    }

    public Object b(a aVar) throws IOException {
        switch (n.f535a[aVar.f().ordinal()]) {
            case 1:
                ArrayList arrayList = new ArrayList();
                aVar.a();
                while (aVar.e()) {
                    arrayList.add(b(aVar));
                }
                aVar.b();
                return arrayList;
            case 2:
                v vVar = new v();
                aVar.c();
                while (aVar.e()) {
                    vVar.put(aVar.g(), b(aVar));
                }
                aVar.d();
                return vVar;
            case 3:
                return aVar.h();
            case 4:
                return Double.valueOf(aVar.k());
            case 5:
                return Boolean.valueOf(aVar.i());
            case 6:
                aVar.j();
                return null;
            default:
                throw new IllegalStateException();
        }
    }

    public void a(d dVar, Object obj) throws IOException {
        if (obj == null) {
            dVar.f();
            return;
        }
        af a2 = this.b.a(obj.getClass());
        if (a2 instanceof l) {
            dVar.d();
            dVar.e();
            return;
        }
        a2.a(dVar, obj);
    }
}
