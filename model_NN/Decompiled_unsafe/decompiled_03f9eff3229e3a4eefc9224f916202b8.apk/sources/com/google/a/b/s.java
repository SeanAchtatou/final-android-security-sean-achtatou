package com.google.a.b;

import com.google.a.af;
import com.google.a.c.a;
import com.google.a.d.d;
import com.google.a.j;
import java.io.IOException;

/* compiled from: Excluder */
class s extends af<T> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ boolean f568a;
    final /* synthetic */ boolean b;
    final /* synthetic */ j c;
    final /* synthetic */ a d;
    final /* synthetic */ r e;
    private af<T> f;

    s(r rVar, boolean z, boolean z2, j jVar, a aVar) {
        this.e = rVar;
        this.f568a = z;
        this.b = z2;
        this.c = jVar;
        this.d = aVar;
    }

    public T b(com.google.a.d.a aVar) throws IOException {
        if (!this.f568a) {
            return b().b(aVar);
        }
        aVar.n();
        return null;
    }

    public void a(d dVar, T t) throws IOException {
        if (this.b) {
            dVar.f();
        } else {
            b().a(dVar, t);
        }
    }

    private af<T> b() {
        af<T> afVar = this.f;
        if (afVar != null) {
            return afVar;
        }
        af<T> a2 = this.c.a(this.e, this.d);
        this.f = a2;
        return a2;
    }
}
