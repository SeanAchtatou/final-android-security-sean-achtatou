package com.google.a.b.a;

import com.google.a.af;
import com.google.a.d.a;
import com.google.a.d.d;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;

/* compiled from: TypeAdapters */
class ap extends af<Timestamp> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ af f520a;
    final /* synthetic */ ao b;

    ap(ao aoVar, af afVar) {
        this.b = aoVar;
        this.f520a = afVar;
    }

    /* renamed from: a */
    public Timestamp b(a aVar) throws IOException {
        Date date = (Date) this.f520a.b(aVar);
        if (date != null) {
            return new Timestamp(date.getTime());
        }
        return null;
    }

    public void a(d dVar, Timestamp timestamp) throws IOException {
        this.f520a.a(dVar, timestamp);
    }
}
