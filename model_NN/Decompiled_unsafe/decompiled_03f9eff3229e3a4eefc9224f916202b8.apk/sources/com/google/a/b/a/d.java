package com.google.a.b.a;

import com.google.a.ab;
import com.google.a.af;
import com.google.a.ah;
import com.google.a.d.a;
import com.google.a.d.c;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.util.Date;
import java.util.Locale;

/* compiled from: DateTypeAdapter */
public final class d extends af<Date> {

    /* renamed from: a  reason: collision with root package name */
    public static final ah f529a = new e();
    private final DateFormat b = DateFormat.getDateTimeInstance(2, 2, Locale.US);
    private final DateFormat c = DateFormat.getDateTimeInstance(2, 2);

    /* renamed from: a */
    public Date b(a aVar) throws IOException {
        if (aVar.f() != c.NULL) {
            return a(aVar.h());
        }
        aVar.j();
        return null;
    }

    private synchronized Date a(String str) {
        Date a2;
        try {
            a2 = this.c.parse(str);
        } catch (ParseException e) {
            try {
                a2 = this.b.parse(str);
            } catch (ParseException e2) {
                try {
                    a2 = com.google.a.b.a.a.a.a(str, new ParsePosition(0));
                } catch (ParseException e3) {
                    throw new ab(str, e3);
                }
            }
        }
        return a2;
    }

    public synchronized void a(com.google.a.d.d dVar, Date date) throws IOException {
        if (date == null) {
            dVar.f();
        } else {
            dVar.b(this.b.format(date));
        }
    }
}
