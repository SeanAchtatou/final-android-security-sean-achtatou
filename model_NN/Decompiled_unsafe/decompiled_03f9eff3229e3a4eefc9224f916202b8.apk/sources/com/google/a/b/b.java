package com.google.a.b;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Properties;

/* compiled from: $Gson$Types */
public final class b {

    /* renamed from: a  reason: collision with root package name */
    static final Type[] f548a = new Type[0];

    public static ParameterizedType a(Type type, Type type2, Type... typeArr) {
        return new C0011b(type, type2, typeArr);
    }

    public static GenericArrayType a(Type type) {
        return new a(type);
    }

    public static WildcardType b(Type type) {
        return new c(new Type[]{type}, f548a);
    }

    public static WildcardType c(Type type) {
        return new c(new Type[]{Object.class}, new Type[]{type});
    }

    public static Type d(Type type) {
        Type type2;
        if (type instanceof Class) {
            Class cls = (Class) type;
            if (cls.isArray()) {
                type2 = new a(d(cls.getComponentType()));
            } else {
                type2 = cls;
            }
            return type2;
        } else if (type instanceof ParameterizedType) {
            ParameterizedType parameterizedType = (ParameterizedType) type;
            return new C0011b(parameterizedType.getOwnerType(), parameterizedType.getRawType(), parameterizedType.getActualTypeArguments());
        } else if (type instanceof GenericArrayType) {
            return new a(((GenericArrayType) type).getGenericComponentType());
        } else {
            if (!(type instanceof WildcardType)) {
                return type;
            }
            WildcardType wildcardType = (WildcardType) type;
            return new c(wildcardType.getUpperBounds(), wildcardType.getLowerBounds());
        }
    }

    public static Class<?> e(Type type) {
        if (type instanceof Class) {
            return (Class) type;
        }
        if (type instanceof ParameterizedType) {
            Type rawType = ((ParameterizedType) type).getRawType();
            a.a(rawType instanceof Class);
            return (Class) rawType;
        } else if (type instanceof GenericArrayType) {
            return Array.newInstance(e(((GenericArrayType) type).getGenericComponentType()), 0).getClass();
        } else {
            if (type instanceof TypeVariable) {
                return Object.class;
            }
            if (type instanceof WildcardType) {
                return e(((WildcardType) type).getUpperBounds()[0]);
            }
            throw new IllegalArgumentException("Expected a Class, ParameterizedType, or GenericArrayType, but <" + type + "> is of type " + (type == null ? "null" : type.getClass().getName()));
        }
    }

    static boolean a(Object obj, Object obj2) {
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.a.b.b.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.reflect.Type, java.lang.reflect.Type]
     candidates:
      com.google.a.b.b.a(java.lang.Object[], java.lang.Object):int
      com.google.a.b.b.a(java.lang.reflect.Type, java.lang.Class<?>):java.lang.reflect.Type
      com.google.a.b.b.a(java.lang.reflect.Type, java.lang.reflect.Type):boolean
      com.google.a.b.b.a(java.lang.Object, java.lang.Object):boolean */
    public static boolean a(Type type, Type type2) {
        boolean z = true;
        if (type == type2) {
            return true;
        }
        if (type instanceof Class) {
            return type.equals(type2);
        }
        if (type instanceof ParameterizedType) {
            if (!(type2 instanceof ParameterizedType)) {
                return false;
            }
            ParameterizedType parameterizedType = (ParameterizedType) type;
            ParameterizedType parameterizedType2 = (ParameterizedType) type2;
            if (!a((Object) parameterizedType.getOwnerType(), (Object) parameterizedType2.getOwnerType()) || !parameterizedType.getRawType().equals(parameterizedType2.getRawType()) || !Arrays.equals(parameterizedType.getActualTypeArguments(), parameterizedType2.getActualTypeArguments())) {
                z = false;
            }
            return z;
        } else if (type instanceof GenericArrayType) {
            if (type2 instanceof GenericArrayType) {
                return a(((GenericArrayType) type).getGenericComponentType(), ((GenericArrayType) type2).getGenericComponentType());
            }
            return false;
        } else if (type instanceof WildcardType) {
            if (!(type2 instanceof WildcardType)) {
                return false;
            }
            WildcardType wildcardType = (WildcardType) type;
            WildcardType wildcardType2 = (WildcardType) type2;
            if (!Arrays.equals(wildcardType.getUpperBounds(), wildcardType2.getUpperBounds()) || !Arrays.equals(wildcardType.getLowerBounds(), wildcardType2.getLowerBounds())) {
                z = false;
            }
            return z;
        } else if (!(type instanceof TypeVariable) || !(type2 instanceof TypeVariable)) {
            return false;
        } else {
            TypeVariable typeVariable = (TypeVariable) type;
            TypeVariable typeVariable2 = (TypeVariable) type2;
            if (typeVariable.getGenericDeclaration() != typeVariable2.getGenericDeclaration() || !typeVariable.getName().equals(typeVariable2.getName())) {
                z = false;
            }
            return z;
        }
    }

    static int a(Object obj) {
        if (obj != null) {
            return obj.hashCode();
        }
        return 0;
    }

    public static String f(Type type) {
        return type instanceof Class ? ((Class) type).getName() : type.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.a.b.b.a(java.lang.reflect.Type, java.lang.Class<?>, java.lang.Class<?>):java.lang.reflect.Type
     arg types: [java.lang.reflect.Type, java.lang.Class<? super ?>, java.lang.Class<?>]
     candidates:
      com.google.a.b.b.a(java.lang.reflect.Type, java.lang.reflect.Type, java.lang.reflect.Type[]):java.lang.reflect.ParameterizedType
      com.google.a.b.b.a(java.lang.reflect.Type, java.lang.Class<?>, java.lang.reflect.Type):java.lang.reflect.Type
      com.google.a.b.b.a(java.lang.reflect.Type, java.lang.Class<?>, java.lang.reflect.TypeVariable<?>):java.lang.reflect.Type
      com.google.a.b.b.a(java.lang.reflect.Type, java.lang.Class<?>, java.lang.Class<?>):java.lang.reflect.Type */
    static Type a(Type type, Class<?> cls, Class<?> cls2) {
        if (cls2 == cls) {
            return type;
        }
        if (cls2.isInterface()) {
            Class<?>[] interfaces = cls.getInterfaces();
            int length = interfaces.length;
            for (int i = 0; i < length; i++) {
                if (interfaces[i] == cls2) {
                    return cls.getGenericInterfaces()[i];
                }
                if (cls2.isAssignableFrom(interfaces[i])) {
                    return a(cls.getGenericInterfaces()[i], interfaces[i], cls2);
                }
            }
        }
        if (!cls.isInterface()) {
            while (cls != Object.class) {
                Class<? super Object> superclass = cls.getSuperclass();
                if (superclass == cls2) {
                    return cls.getGenericSuperclass();
                }
                if (cls2.isAssignableFrom(superclass)) {
                    return a(cls.getGenericSuperclass(), (Class<?>) superclass, cls2);
                }
                cls = superclass;
            }
        }
        return cls2;
    }

    static Type b(Type type, Class<?> cls, Class<?> cls2) {
        a.a(cls2.isAssignableFrom(cls));
        return a(type, cls, a(type, cls, cls2));
    }

    public static Type g(Type type) {
        if (type instanceof GenericArrayType) {
            return ((GenericArrayType) type).getGenericComponentType();
        }
        return ((Class) type).getComponentType();
    }

    public static Type a(Type type, Class<?> cls) {
        Type b = b(type, cls, Collection.class);
        if (b instanceof WildcardType) {
            b = ((WildcardType) b).getUpperBounds()[0];
        }
        if (b instanceof ParameterizedType) {
            return ((ParameterizedType) b).getActualTypeArguments()[0];
        }
        return Object.class;
    }

    public static Type[] b(Type type, Class<?> cls) {
        if (type == Properties.class) {
            return new Type[]{String.class, String.class};
        }
        Type b = b(type, cls, Map.class);
        if (b instanceof ParameterizedType) {
            return ((ParameterizedType) b).getActualTypeArguments();
        }
        return new Type[]{Object.class, Object.class};
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.a.b.b.a(java.lang.reflect.Type, java.lang.Class<?>, java.lang.reflect.Type):java.lang.reflect.Type
     arg types: [java.lang.reflect.Type, java.lang.Class<?>, java.lang.Class<?>]
     candidates:
      com.google.a.b.b.a(java.lang.reflect.Type, java.lang.reflect.Type, java.lang.reflect.Type[]):java.lang.reflect.ParameterizedType
      com.google.a.b.b.a(java.lang.reflect.Type, java.lang.Class<?>, java.lang.Class<?>):java.lang.reflect.Type
      com.google.a.b.b.a(java.lang.reflect.Type, java.lang.Class<?>, java.lang.reflect.TypeVariable<?>):java.lang.reflect.Type
      com.google.a.b.b.a(java.lang.reflect.Type, java.lang.Class<?>, java.lang.reflect.Type):java.lang.reflect.Type */
    public static Type a(Type type, Class<?> cls, Type type2) {
        Type a2;
        boolean z;
        while (true) {
            Type type3 = type2;
            if (type3 instanceof TypeVariable) {
                TypeVariable typeVariable = (TypeVariable) type3;
                type2 = a(type, cls, (TypeVariable<?>) typeVariable);
                if (type2 == typeVariable) {
                    return type2;
                }
            } else if ((type3 instanceof Class) && ((Class) type3).isArray()) {
                Class cls2 = (Class) type3;
                Class<?> componentType = cls2.getComponentType();
                Type a3 = a(type, cls, (Type) componentType);
                if (componentType != a3) {
                    return a(a3);
                }
                return cls2;
            } else if (type3 instanceof GenericArrayType) {
                GenericArrayType genericArrayType = (GenericArrayType) type3;
                Type genericComponentType = genericArrayType.getGenericComponentType();
                Type a4 = a(type, cls, genericComponentType);
                if (genericComponentType != a4) {
                    return a(a4);
                }
                return genericArrayType;
            } else if (type3 instanceof ParameterizedType) {
                ParameterizedType parameterizedType = (ParameterizedType) type3;
                Type ownerType = parameterizedType.getOwnerType();
                Type a5 = a(type, cls, ownerType);
                if (a5 != ownerType) {
                    z = true;
                } else {
                    z = false;
                }
                Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
                int length = actualTypeArguments.length;
                boolean z2 = z;
                Type[] typeArr = actualTypeArguments;
                for (int i = 0; i < length; i++) {
                    Type a6 = a(type, cls, typeArr[i]);
                    if (a6 != typeArr[i]) {
                        if (!z2) {
                            typeArr = (Type[]) typeArr.clone();
                            z2 = true;
                        }
                        typeArr[i] = a6;
                    }
                }
                if (z2) {
                    return a(a5, parameterizedType.getRawType(), typeArr);
                }
                return parameterizedType;
            } else if (!(type3 instanceof WildcardType)) {
                return type3;
            } else {
                WildcardType wildcardType = (WildcardType) type3;
                Type[] lowerBounds = wildcardType.getLowerBounds();
                Type[] upperBounds = wildcardType.getUpperBounds();
                if (lowerBounds.length == 1) {
                    Type a7 = a(type, cls, lowerBounds[0]);
                    if (a7 != lowerBounds[0]) {
                        return c(a7);
                    }
                    return wildcardType;
                } else if (upperBounds.length != 1 || (a2 = a(type, cls, upperBounds[0])) == upperBounds[0]) {
                    return wildcardType;
                } else {
                    return b(a2);
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.a.b.b.a(java.lang.Object[], java.lang.Object):int
     arg types: [java.lang.reflect.TypeVariable[], java.lang.reflect.TypeVariable<?>]
     candidates:
      com.google.a.b.b.a(java.lang.reflect.Type, java.lang.Class<?>):java.lang.reflect.Type
      com.google.a.b.b.a(java.lang.Object, java.lang.Object):boolean
      com.google.a.b.b.a(java.lang.reflect.Type, java.lang.reflect.Type):boolean
      com.google.a.b.b.a(java.lang.Object[], java.lang.Object):int */
    static Type a(Type type, Class<?> cls, TypeVariable<?> typeVariable) {
        Class<?> a2 = a(typeVariable);
        if (a2 == null) {
            return typeVariable;
        }
        Type a3 = a(type, cls, a2);
        if (!(a3 instanceof ParameterizedType)) {
            return typeVariable;
        }
        return ((ParameterizedType) a3).getActualTypeArguments()[a((Object[]) a2.getTypeParameters(), (Object) typeVariable)];
    }

    private static int a(Object[] objArr, Object obj) {
        for (int i = 0; i < objArr.length; i++) {
            if (obj.equals(objArr[i])) {
                return i;
            }
        }
        throw new NoSuchElementException();
    }

    private static Class<?> a(TypeVariable<?> typeVariable) {
        Object genericDeclaration = typeVariable.getGenericDeclaration();
        if (genericDeclaration instanceof Class) {
            return (Class) genericDeclaration;
        }
        return null;
    }

    static void h(Type type) {
        a.a(!(type instanceof Class) || !((Class) type).isPrimitive());
    }

    /* renamed from: com.google.a.b.b$b  reason: collision with other inner class name */
    /* compiled from: $Gson$Types */
    private static final class C0011b implements Serializable, ParameterizedType {

        /* renamed from: a  reason: collision with root package name */
        private final Type f550a;
        private final Type b;
        private final Type[] c;

        public C0011b(Type type, Type type2, Type... typeArr) {
            boolean z;
            if (type2 instanceof Class) {
                Class cls = (Class) type2;
                boolean z2 = Modifier.isStatic(cls.getModifiers()) || cls.getEnclosingClass() == null;
                if (type != null || z2) {
                    z = true;
                } else {
                    z = false;
                }
                a.a(z);
            }
            this.f550a = type == null ? null : b.d(type);
            this.b = b.d(type2);
            this.c = (Type[]) typeArr.clone();
            for (int i = 0; i < this.c.length; i++) {
                a.a(this.c[i]);
                b.h(this.c[i]);
                this.c[i] = b.d(this.c[i]);
            }
        }

        public Type[] getActualTypeArguments() {
            return (Type[]) this.c.clone();
        }

        public Type getRawType() {
            return this.b;
        }

        public Type getOwnerType() {
            return this.f550a;
        }

        public boolean equals(Object obj) {
            return (obj instanceof ParameterizedType) && b.a(this, (ParameterizedType) obj);
        }

        public int hashCode() {
            return (Arrays.hashCode(this.c) ^ this.b.hashCode()) ^ b.a((Object) this.f550a);
        }

        public String toString() {
            StringBuilder sb = new StringBuilder((this.c.length + 1) * 30);
            sb.append(b.f(this.b));
            if (this.c.length == 0) {
                return sb.toString();
            }
            sb.append("<").append(b.f(this.c[0]));
            for (int i = 1; i < this.c.length; i++) {
                sb.append(", ").append(b.f(this.c[i]));
            }
            return sb.append(">").toString();
        }
    }

    /* compiled from: $Gson$Types */
    private static final class a implements Serializable, GenericArrayType {

        /* renamed from: a  reason: collision with root package name */
        private final Type f549a;

        public a(Type type) {
            this.f549a = b.d(type);
        }

        public Type getGenericComponentType() {
            return this.f549a;
        }

        public boolean equals(Object obj) {
            return (obj instanceof GenericArrayType) && b.a(this, (GenericArrayType) obj);
        }

        public int hashCode() {
            return this.f549a.hashCode();
        }

        public String toString() {
            return b.f(this.f549a) + "[]";
        }
    }

    /* compiled from: $Gson$Types */
    private static final class c implements Serializable, WildcardType {

        /* renamed from: a  reason: collision with root package name */
        private final Type f551a;
        private final Type b;

        public c(Type[] typeArr, Type[] typeArr2) {
            boolean z;
            boolean z2 = true;
            a.a(typeArr2.length <= 1);
            if (typeArr.length == 1) {
                z = true;
            } else {
                z = false;
            }
            a.a(z);
            if (typeArr2.length == 1) {
                a.a(typeArr2[0]);
                b.h(typeArr2[0]);
                a.a(typeArr[0] != Object.class ? false : z2);
                this.b = b.d(typeArr2[0]);
                this.f551a = Object.class;
                return;
            }
            a.a(typeArr[0]);
            b.h(typeArr[0]);
            this.b = null;
            this.f551a = b.d(typeArr[0]);
        }

        public Type[] getUpperBounds() {
            return new Type[]{this.f551a};
        }

        public Type[] getLowerBounds() {
            if (this.b == null) {
                return b.f548a;
            }
            return new Type[]{this.b};
        }

        public boolean equals(Object obj) {
            return (obj instanceof WildcardType) && b.a(this, (WildcardType) obj);
        }

        public int hashCode() {
            return (this.b != null ? this.b.hashCode() + 31 : 1) ^ (this.f551a.hashCode() + 31);
        }

        public String toString() {
            if (this.b != null) {
                return "? super " + b.f(this.b);
            }
            if (this.f551a == Object.class) {
                return "?";
            }
            return "? extends " + b.f(this.f551a);
        }
    }
}
