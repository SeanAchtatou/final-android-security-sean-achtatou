package com.google.a.b.a;

import com.google.a.af;
import com.google.a.ah;
import com.google.a.c.a;
import com.google.a.j;

/* compiled from: TypeAdapters */
final class av implements ah {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Class f521a;
    final /* synthetic */ af b;

    av(Class cls, af afVar) {
        this.f521a = cls;
        this.b = afVar;
    }

    public <T> af<T> a(j jVar, a<T> aVar) {
        if (aVar.a() == this.f521a) {
            return this.b;
        }
        return null;
    }

    public String toString() {
        return "Factory[type=" + this.f521a.getName() + ",adapter=" + this.b + "]";
    }
}
