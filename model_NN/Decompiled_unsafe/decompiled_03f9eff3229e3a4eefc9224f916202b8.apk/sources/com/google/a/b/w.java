package com.google.a.b;

import java.util.Comparator;

/* compiled from: LinkedTreeMap */
final class w implements Comparator<Comparable> {
    w() {
    }

    /* renamed from: a */
    public int compare(Comparable comparable, Comparable comparable2) {
        return comparable.compareTo(comparable2);
    }
}
