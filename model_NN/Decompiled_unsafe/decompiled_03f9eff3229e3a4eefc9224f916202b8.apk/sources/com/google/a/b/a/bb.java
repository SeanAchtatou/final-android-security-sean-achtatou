package com.google.a.b.a;

import com.google.a.af;
import com.google.a.d.a;
import com.google.a.d.c;
import com.google.a.d.d;
import java.io.IOException;

/* compiled from: TypeAdapters */
final class bb extends af<Boolean> {
    bb() {
    }

    /* renamed from: a */
    public Boolean b(a aVar) throws IOException {
        if (aVar.f() != c.NULL) {
            return Boolean.valueOf(aVar.h());
        }
        aVar.j();
        return null;
    }

    public void a(d dVar, Boolean bool) throws IOException {
        dVar.b(bool == null ? "null" : bool.toString());
    }
}
