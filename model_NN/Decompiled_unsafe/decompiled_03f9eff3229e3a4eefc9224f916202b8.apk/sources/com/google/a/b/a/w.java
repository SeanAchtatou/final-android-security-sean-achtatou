package com.google.a.b.a;

import com.google.a.af;
import com.google.a.d.a;
import com.google.a.d.c;
import com.google.a.d.d;
import java.io.IOException;

/* compiled from: TypeAdapters */
final class w extends af<Class> {
    w() {
    }

    public void a(d dVar, Class cls) throws IOException {
        if (cls == null) {
            dVar.f();
            return;
        }
        throw new UnsupportedOperationException("Attempted to serialize java.lang.Class: " + cls.getName() + ". Forgot to register a type adapter?");
    }

    /* renamed from: a */
    public Class b(a aVar) throws IOException {
        if (aVar.f() == c.NULL) {
            aVar.j();
            return null;
        }
        throw new UnsupportedOperationException("Attempted to deserialize a java.lang.Class. Forgot to register a type adapter?");
    }
}
