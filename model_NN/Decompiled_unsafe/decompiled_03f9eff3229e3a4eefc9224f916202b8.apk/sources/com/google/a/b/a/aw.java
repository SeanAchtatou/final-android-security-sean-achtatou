package com.google.a.b.a;

import com.google.a.af;
import com.google.a.ah;
import com.google.a.c.a;
import com.google.a.j;

/* compiled from: TypeAdapters */
final class aw implements ah {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Class f522a;
    final /* synthetic */ Class b;
    final /* synthetic */ af c;

    aw(Class cls, Class cls2, af afVar) {
        this.f522a = cls;
        this.b = cls2;
        this.c = afVar;
    }

    public <T> af<T> a(j jVar, a<T> aVar) {
        Class<? super T> a2 = aVar.a();
        if (a2 == this.f522a || a2 == this.b) {
            return this.c;
        }
        return null;
    }

    public String toString() {
        return "Factory[type=" + this.b.getName() + "+" + this.f522a.getName() + ",adapter=" + this.c + "]";
    }
}
