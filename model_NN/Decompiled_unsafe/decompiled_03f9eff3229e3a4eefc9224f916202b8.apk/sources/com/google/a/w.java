package com.google.a;

/* compiled from: JsonNull */
public final class w extends u {

    /* renamed from: a  reason: collision with root package name */
    public static final w f591a = new w();

    public int hashCode() {
        return w.class.hashCode();
    }

    public boolean equals(Object obj) {
        return this == obj || (obj instanceof w);
    }
}
