package com.google.a;

import com.google.a.b.a.i;
import com.google.a.d.a;
import com.google.a.d.d;
import java.io.IOException;

/* compiled from: TypeAdapter */
public abstract class af<T> {
    public abstract void a(d dVar, T t) throws IOException;

    public abstract T b(a aVar) throws IOException;

    public final af<T> a() {
        return new ag(this);
    }

    public final u a(T t) {
        try {
            i iVar = new i();
            a(iVar, t);
            return iVar.a();
        } catch (IOException e) {
            throw new v(e);
        }
    }
}
