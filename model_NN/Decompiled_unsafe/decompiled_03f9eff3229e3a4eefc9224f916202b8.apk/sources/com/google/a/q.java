package com.google.a;

import com.google.a.d.a;
import com.google.a.d.d;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicLongArray;

/* compiled from: Gson */
final class q extends af<AtomicLongArray> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ af f589a;

    q(af afVar) {
        this.f589a = afVar;
    }

    public void a(d dVar, AtomicLongArray atomicLongArray) throws IOException {
        dVar.b();
        int length = atomicLongArray.length();
        for (int i = 0; i < length; i++) {
            this.f589a.a(dVar, Long.valueOf(atomicLongArray.get(i)));
        }
        dVar.c();
    }

    /* renamed from: a */
    public AtomicLongArray b(a aVar) throws IOException {
        ArrayList arrayList = new ArrayList();
        aVar.a();
        while (aVar.e()) {
            arrayList.add(Long.valueOf(((Number) this.f589a.b(aVar)).longValue()));
        }
        aVar.b();
        int size = arrayList.size();
        AtomicLongArray atomicLongArray = new AtomicLongArray(size);
        for (int i = 0; i < size; i++) {
            atomicLongArray.set(i, ((Long) arrayList.get(i)).longValue());
        }
        return atomicLongArray;
    }
}
