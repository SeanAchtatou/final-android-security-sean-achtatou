package com.google.a;

import com.google.a.d.a;
import com.google.a.d.c;
import com.google.a.d.d;
import java.io.IOException;

/* compiled from: Gson */
class n extends af<Number> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ j f587a;

    n(j jVar) {
        this.f587a = jVar;
    }

    /* renamed from: a */
    public Float b(a aVar) throws IOException {
        if (aVar.f() != c.NULL) {
            return Float.valueOf((float) aVar.k());
        }
        aVar.j();
        return null;
    }

    public void a(d dVar, Number number) throws IOException {
        if (number == null) {
            dVar.f();
            return;
        }
        j.a((double) number.floatValue());
        dVar.a(number);
    }
}
