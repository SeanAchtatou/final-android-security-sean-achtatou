package com.google.a;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: JsonArray */
public final class s extends u implements Iterable<u> {

    /* renamed from: a  reason: collision with root package name */
    private final List<u> f590a = new ArrayList();

    public void a(u uVar) {
        if (uVar == null) {
            uVar = w.f591a;
        }
        this.f590a.add(uVar);
    }

    public Iterator<u> iterator() {
        return this.f590a.iterator();
    }

    public Number a() {
        if (this.f590a.size() == 1) {
            return this.f590a.get(0).a();
        }
        throw new IllegalStateException();
    }

    public String b() {
        if (this.f590a.size() == 1) {
            return this.f590a.get(0).b();
        }
        throw new IllegalStateException();
    }

    public double c() {
        if (this.f590a.size() == 1) {
            return this.f590a.get(0).c();
        }
        throw new IllegalStateException();
    }

    public long d() {
        if (this.f590a.size() == 1) {
            return this.f590a.get(0).d();
        }
        throw new IllegalStateException();
    }

    public int e() {
        if (this.f590a.size() == 1) {
            return this.f590a.get(0).e();
        }
        throw new IllegalStateException();
    }

    public boolean f() {
        if (this.f590a.size() == 1) {
            return this.f590a.get(0).f();
        }
        throw new IllegalStateException();
    }

    public boolean equals(Object obj) {
        return obj == this || ((obj instanceof s) && ((s) obj).f590a.equals(this.f590a));
    }

    public int hashCode() {
        return this.f590a.hashCode();
    }
}
