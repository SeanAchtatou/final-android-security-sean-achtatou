package com.google.a.d;

import com.google.a.b.a.g;
import com.google.a.b.t;
import java.io.IOException;

/* compiled from: JsonReader */
final class b extends t {
    b() {
    }

    public void a(a aVar) throws IOException {
        if (aVar instanceof g) {
            ((g) aVar).o();
            return;
        }
        int i = aVar.f579a;
        if (i == 0) {
            i = aVar.q();
        }
        if (i == 13) {
            aVar.f579a = 9;
        } else if (i == 12) {
            aVar.f579a = 8;
        } else if (i == 14) {
            aVar.f579a = 10;
        } else {
            throw new IllegalStateException("Expected a name but was " + aVar.f() + " " + " at line " + aVar.r() + " column " + aVar.s() + " path " + aVar.t());
        }
    }
}
