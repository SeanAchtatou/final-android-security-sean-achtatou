package com.google.a.d;

import cn.banshenggua.aichang.utils.Constants;
import com.google.a.b.t;
import java.io.Closeable;
import java.io.EOFException;
import java.io.IOException;
import java.io.Reader;

/* compiled from: JsonReader */
public class a implements Closeable {
    private static final char[] b = ")]}'\n".toCharArray();

    /* renamed from: a  reason: collision with root package name */
    int f579a = 0;
    private final Reader c;
    private boolean d = false;
    private final char[] e = new char[1024];
    private int f = 0;
    private int g = 0;
    private int h = 0;
    private int i = 0;
    private long j;
    private int k;
    private String l;
    private int[] m = new int[32];
    private int n = 0;
    private String[] o;
    private int[] p;

    static {
        t.f569a = new b();
    }

    public a(Reader reader) {
        int[] iArr = this.m;
        int i2 = this.n;
        this.n = i2 + 1;
        iArr[i2] = 6;
        this.o = new String[32];
        this.p = new int[32];
        if (reader == null) {
            throw new NullPointerException("in == null");
        }
        this.c = reader;
    }

    public final void a(boolean z) {
        this.d = z;
    }

    public final boolean p() {
        return this.d;
    }

    public void a() throws IOException {
        int i2 = this.f579a;
        if (i2 == 0) {
            i2 = q();
        }
        if (i2 == 3) {
            a(1);
            this.p[this.n - 1] = 0;
            this.f579a = 0;
            return;
        }
        throw new IllegalStateException("Expected BEGIN_ARRAY but was " + f() + " at line " + r() + " column " + s() + " path " + t());
    }

    public void b() throws IOException {
        int i2 = this.f579a;
        if (i2 == 0) {
            i2 = q();
        }
        if (i2 == 4) {
            this.n--;
            int[] iArr = this.p;
            int i3 = this.n - 1;
            iArr[i3] = iArr[i3] + 1;
            this.f579a = 0;
            return;
        }
        throw new IllegalStateException("Expected END_ARRAY but was " + f() + " at line " + r() + " column " + s() + " path " + t());
    }

    public void c() throws IOException {
        int i2 = this.f579a;
        if (i2 == 0) {
            i2 = q();
        }
        if (i2 == 1) {
            a(3);
            this.f579a = 0;
            return;
        }
        throw new IllegalStateException("Expected BEGIN_OBJECT but was " + f() + " at line " + r() + " column " + s() + " path " + t());
    }

    public void d() throws IOException {
        int i2 = this.f579a;
        if (i2 == 0) {
            i2 = q();
        }
        if (i2 == 2) {
            this.n--;
            this.o[this.n] = null;
            int[] iArr = this.p;
            int i3 = this.n - 1;
            iArr[i3] = iArr[i3] + 1;
            this.f579a = 0;
            return;
        }
        throw new IllegalStateException("Expected END_OBJECT but was " + f() + " at line " + r() + " column " + s() + " path " + t());
    }

    public boolean e() throws IOException {
        int i2 = this.f579a;
        if (i2 == 0) {
            i2 = q();
        }
        return (i2 == 2 || i2 == 4) ? false : true;
    }

    public c f() throws IOException {
        int i2 = this.f579a;
        if (i2 == 0) {
            i2 = q();
        }
        switch (i2) {
            case 1:
                return c.BEGIN_OBJECT;
            case 2:
                return c.END_OBJECT;
            case 3:
                return c.BEGIN_ARRAY;
            case 4:
                return c.END_ARRAY;
            case 5:
            case 6:
                return c.BOOLEAN;
            case 7:
                return c.NULL;
            case 8:
            case 9:
            case 10:
            case 11:
                return c.STRING;
            case 12:
            case 13:
            case 14:
                return c.NAME;
            case 15:
            case 16:
                return c.NUMBER;
            case 17:
                return c.END_DOCUMENT;
            default:
                throw new AssertionError();
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: package-private */
    public int q() throws IOException {
        int i2 = this.m[this.n - 1];
        if (i2 == 1) {
            this.m[this.n - 1] = 2;
        } else if (i2 == 2) {
            switch (b(true)) {
                case D.QHVIDEOADONCLICKLISTENER_onDownloadCancelled /*44*/:
                    break;
                case 59:
                    x();
                    break;
                case 93:
                    this.f579a = 4;
                    return 4;
                default:
                    throw b("Unterminated array");
            }
        } else if (i2 == 3 || i2 == 5) {
            this.m[this.n - 1] = 4;
            if (i2 == 5) {
                switch (b(true)) {
                    case D.QHVIDEOADONCLICKLISTENER_onDownloadCancelled /*44*/:
                        break;
                    case 59:
                        x();
                        break;
                    case 125:
                        this.f579a = 2;
                        return 2;
                    default:
                        throw b("Unterminated object");
                }
            }
            int b2 = b(true);
            switch (b2) {
                case D.QHNATIVEADLOADER_setKeywords /*34*/:
                    this.f579a = 13;
                    return 13;
                case D.QHVIDEOAD_onAdPlayStarted /*39*/:
                    x();
                    this.f579a = 12;
                    return 12;
                case 125:
                    if (i2 != 5) {
                        this.f579a = 2;
                        return 2;
                    }
                    throw b("Expected name");
                default:
                    x();
                    this.f--;
                    if (a((char) b2)) {
                        this.f579a = 14;
                        return 14;
                    }
                    throw b("Expected name");
            }
        } else if (i2 == 4) {
            this.m[this.n - 1] = 5;
            switch (b(true)) {
                case 58:
                    break;
                case 59:
                case 60:
                default:
                    throw b("Expected ':'");
                case 61:
                    x();
                    if ((this.f < this.g || b(1)) && this.e[this.f] == '>') {
                        this.f++;
                        break;
                    }
            }
        } else if (i2 == 6) {
            if (this.d) {
                A();
            }
            this.m[this.n - 1] = 7;
        } else if (i2 == 7) {
            if (b(false) == -1) {
                this.f579a = 17;
                return 17;
            }
            x();
            this.f--;
        } else if (i2 == 8) {
            throw new IllegalStateException("JsonReader is closed");
        }
        switch (b(true)) {
            case D.QHNATIVEADLOADER_setKeywords /*34*/:
                this.f579a = 9;
                return 9;
            case D.QHVIDEOAD_onAdPlayStarted /*39*/:
                x();
                this.f579a = 8;
                return 8;
            case D.QHVIDEOADONCLICKLISTENER_onDownloadCancelled /*44*/:
            case 59:
                break;
            case 91:
                this.f579a = 3;
                return 3;
            case 93:
                if (i2 == 1) {
                    this.f579a = 4;
                    return 4;
                }
                break;
            case 123:
                this.f579a = 1;
                return 1;
            default:
                this.f--;
                int o2 = o();
                if (o2 != 0) {
                    return o2;
                }
                int u = u();
                if (u != 0) {
                    return u;
                }
                if (!a(this.e[this.f])) {
                    throw b("Expected value");
                }
                x();
                this.f579a = 10;
                return 10;
        }
        if (i2 == 1 || i2 == 2) {
            x();
            this.f--;
            this.f579a = 7;
            return 7;
        }
        throw b("Unexpected value");
    }

    private int o() throws IOException {
        String str;
        String str2;
        int i2;
        char c2 = this.e[this.f];
        if (c2 == 't' || c2 == 'T') {
            str = "true";
            str2 = "TRUE";
            i2 = 5;
        } else if (c2 == 'f' || c2 == 'F') {
            str = "false";
            str2 = "FALSE";
            i2 = 6;
        } else if (c2 != 'n' && c2 != 'N') {
            return 0;
        } else {
            str = "null";
            str2 = "NULL";
            i2 = 7;
        }
        int length = str.length();
        for (int i3 = 1; i3 < length; i3++) {
            if (this.f + i3 >= this.g && !b(i3 + 1)) {
                return 0;
            }
            char c3 = this.e[this.f + i3];
            if (c3 != str.charAt(i3) && c3 != str2.charAt(i3)) {
                return 0;
            }
        }
        if ((this.f + length < this.g || b(length + 1)) && a(this.e[this.f + length])) {
            return 0;
        }
        this.f += length;
        this.f579a = i2;
        return i2;
    }

    /* JADX WARN: Type inference failed for: r2v5 */
    /* JADX WARN: Type inference failed for: r2v7 */
    /* JADX WARN: Type inference failed for: r2v10 */
    /* JADX WARN: Type inference failed for: r2v11 */
    /* JADX WARN: Type inference failed for: r2v13 */
    /* JADX WARN: Type inference failed for: r2v14 */
    /* JADX WARN: Type inference failed for: r2v17 */
    /* JADX WARN: Type inference failed for: r2v20 */
    /* JADX WARN: Type inference failed for: r2v22 */
    /* JADX WARN: Type inference failed for: r2v23 */
    /* JADX WARN: Type inference failed for: r2v28 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int u() throws java.io.IOException {
        /*
            r15 = this;
            char[] r11 = r15.e
            int r2 = r15.f
            int r1 = r15.g
            r6 = 0
            r5 = 0
            r4 = 1
            r3 = 0
            r0 = 0
            r10 = r0
            r0 = r1
            r1 = r2
        L_0x000f:
            int r2 = r1 + r10
            if (r2 != r0) goto L_0x003f
            int r0 = r11.length
            if (r10 != r0) goto L_0x0018
            r0 = 0
        L_0x0017:
            return r0
        L_0x0018:
            int r0 = r10 + 1
            boolean r0 = r15.b(r0)
            if (r0 != 0) goto L_0x003b
        L_0x0020:
            r0 = 2
            if (r3 != r0) goto L_0x00df
            if (r4 == 0) goto L_0x00df
            r0 = -9223372036854775808
            int r0 = (r6 > r0 ? 1 : (r6 == r0 ? 0 : -1))
            if (r0 != 0) goto L_0x002d
            if (r5 == 0) goto L_0x00df
        L_0x002d:
            if (r5 == 0) goto L_0x00dc
        L_0x002f:
            r15.j = r6
            int r0 = r15.f
            int r0 = r0 + r10
            r15.f = r0
            r0 = 15
            r15.f579a = r0
            goto L_0x0017
        L_0x003b:
            int r1 = r15.f
            int r0 = r15.g
        L_0x003f:
            int r2 = r1 + r10
            char r2 = r11[r2]
            switch(r2) {
                case 43: goto L_0x006d;
                case 45: goto L_0x0056;
                case 46: goto L_0x0082;
                case 69: goto L_0x0076;
                case 101: goto L_0x0076;
                default: goto L_0x0046;
            }
        L_0x0046:
            r8 = 48
            if (r2 < r8) goto L_0x004e
            r8 = 57
            if (r2 <= r8) goto L_0x008b
        L_0x004e:
            boolean r0 = r15.a(r2)
            if (r0 == 0) goto L_0x0020
            r0 = 0
            goto L_0x0017
        L_0x0056:
            if (r3 != 0) goto L_0x0064
            r3 = 1
            r2 = 1
            r14 = r4
            r4 = r3
            r3 = r14
        L_0x005d:
            int r5 = r10 + 1
            r10 = r5
            r5 = r4
            r4 = r3
            r3 = r2
            goto L_0x000f
        L_0x0064:
            r2 = 5
            if (r3 != r2) goto L_0x006b
            r2 = 6
            r3 = r4
            r4 = r5
            goto L_0x005d
        L_0x006b:
            r0 = 0
            goto L_0x0017
        L_0x006d:
            r2 = 5
            if (r3 != r2) goto L_0x0074
            r2 = 6
            r3 = r4
            r4 = r5
            goto L_0x005d
        L_0x0074:
            r0 = 0
            goto L_0x0017
        L_0x0076:
            r2 = 2
            if (r3 == r2) goto L_0x007c
            r2 = 4
            if (r3 != r2) goto L_0x0080
        L_0x007c:
            r2 = 5
            r3 = r4
            r4 = r5
            goto L_0x005d
        L_0x0080:
            r0 = 0
            goto L_0x0017
        L_0x0082:
            r2 = 2
            if (r3 != r2) goto L_0x0089
            r2 = 3
            r3 = r4
            r4 = r5
            goto L_0x005d
        L_0x0089:
            r0 = 0
            goto L_0x0017
        L_0x008b:
            r8 = 1
            if (r3 == r8) goto L_0x0090
            if (r3 != 0) goto L_0x0098
        L_0x0090:
            int r2 = r2 + -48
            int r2 = -r2
            long r6 = (long) r2
            r2 = 2
            r3 = r4
            r4 = r5
            goto L_0x005d
        L_0x0098:
            r8 = 2
            if (r3 != r8) goto L_0x00cb
            r8 = 0
            int r8 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r8 != 0) goto L_0x00a4
            r0 = 0
            goto L_0x0017
        L_0x00a4:
            r8 = 10
            long r8 = r8 * r6
            int r2 = r2 + -48
            long r12 = (long) r2
            long r8 = r8 - r12
            r12 = -922337203685477580(0xf333333333333334, double:-8.390303882365713E246)
            int r2 = (r6 > r12 ? 1 : (r6 == r12 ? 0 : -1))
            if (r2 > 0) goto L_0x00c1
            r12 = -922337203685477580(0xf333333333333334, double:-8.390303882365713E246)
            int r2 = (r6 > r12 ? 1 : (r6 == r12 ? 0 : -1))
            if (r2 != 0) goto L_0x00c9
            int r2 = (r8 > r6 ? 1 : (r8 == r6 ? 0 : -1))
            if (r2 >= 0) goto L_0x00c9
        L_0x00c1:
            r2 = 1
        L_0x00c2:
            r2 = r2 & r4
            r4 = r5
            r6 = r8
            r14 = r3
            r3 = r2
            r2 = r14
            goto L_0x005d
        L_0x00c9:
            r2 = 0
            goto L_0x00c2
        L_0x00cb:
            r2 = 3
            if (r3 != r2) goto L_0x00d2
            r2 = 4
            r3 = r4
            r4 = r5
            goto L_0x005d
        L_0x00d2:
            r2 = 5
            if (r3 == r2) goto L_0x00d8
            r2 = 6
            if (r3 != r2) goto L_0x00f3
        L_0x00d8:
            r2 = 7
            r3 = r4
            r4 = r5
            goto L_0x005d
        L_0x00dc:
            long r6 = -r6
            goto L_0x002f
        L_0x00df:
            r0 = 2
            if (r3 == r0) goto L_0x00e8
            r0 = 4
            if (r3 == r0) goto L_0x00e8
            r0 = 7
            if (r3 != r0) goto L_0x00f0
        L_0x00e8:
            r15.k = r10
            r0 = 16
            r15.f579a = r0
            goto L_0x0017
        L_0x00f0:
            r0 = 0
            goto L_0x0017
        L_0x00f3:
            r2 = r3
            r3 = r4
            r4 = r5
            goto L_0x005d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.a.d.a.u():int");
    }

    private boolean a(char c2) throws IOException {
        switch (c2) {
            case 9:
            case 10:
            case 12:
            case 13:
            case ' ':
            case D.QHVIDEOADONCLICKLISTENER_onDownloadCancelled /*44*/:
            case ':':
            case '[':
            case ']':
            case '{':
            case '}':
                break;
            default:
                return true;
            case D.QHNATIVEADLOADER_clearKeywords /*35*/:
            case D.QHLANDINGPAGEVIEW_open /*47*/:
            case ';':
            case '=':
            case '\\':
                x();
                break;
        }
        return false;
    }

    public String g() throws IOException {
        String b2;
        int i2 = this.f579a;
        if (i2 == 0) {
            i2 = q();
        }
        if (i2 == 14) {
            b2 = v();
        } else if (i2 == 12) {
            b2 = b('\'');
        } else if (i2 == 13) {
            b2 = b('\"');
        } else {
            throw new IllegalStateException("Expected a name but was " + f() + " at line " + r() + " column " + s() + " path " + t());
        }
        this.f579a = 0;
        this.o[this.n - 1] = b2;
        return b2;
    }

    public String h() throws IOException {
        String str;
        int i2 = this.f579a;
        if (i2 == 0) {
            i2 = q();
        }
        if (i2 == 10) {
            str = v();
        } else if (i2 == 8) {
            str = b('\'');
        } else if (i2 == 9) {
            str = b('\"');
        } else if (i2 == 11) {
            str = this.l;
            this.l = null;
        } else if (i2 == 15) {
            str = Long.toString(this.j);
        } else if (i2 == 16) {
            str = new String(this.e, this.f, this.k);
            this.f += this.k;
        } else {
            throw new IllegalStateException("Expected a string but was " + f() + " at line " + r() + " column " + s() + " path " + t());
        }
        this.f579a = 0;
        int[] iArr = this.p;
        int i3 = this.n - 1;
        iArr[i3] = iArr[i3] + 1;
        return str;
    }

    public boolean i() throws IOException {
        int i2 = this.f579a;
        if (i2 == 0) {
            i2 = q();
        }
        if (i2 == 5) {
            this.f579a = 0;
            int[] iArr = this.p;
            int i3 = this.n - 1;
            iArr[i3] = iArr[i3] + 1;
            return true;
        } else if (i2 == 6) {
            this.f579a = 0;
            int[] iArr2 = this.p;
            int i4 = this.n - 1;
            iArr2[i4] = iArr2[i4] + 1;
            return false;
        } else {
            throw new IllegalStateException("Expected a boolean but was " + f() + " at line " + r() + " column " + s() + " path " + t());
        }
    }

    public void j() throws IOException {
        int i2 = this.f579a;
        if (i2 == 0) {
            i2 = q();
        }
        if (i2 == 7) {
            this.f579a = 0;
            int[] iArr = this.p;
            int i3 = this.n - 1;
            iArr[i3] = iArr[i3] + 1;
            return;
        }
        throw new IllegalStateException("Expected null but was " + f() + " at line " + r() + " column " + s() + " path " + t());
    }

    public double k() throws IOException {
        int i2 = this.f579a;
        if (i2 == 0) {
            i2 = q();
        }
        if (i2 == 15) {
            this.f579a = 0;
            int[] iArr = this.p;
            int i3 = this.n - 1;
            iArr[i3] = iArr[i3] + 1;
            return (double) this.j;
        }
        if (i2 == 16) {
            this.l = new String(this.e, this.f, this.k);
            this.f += this.k;
        } else if (i2 == 8 || i2 == 9) {
            this.l = b(i2 == 8 ? '\'' : '\"');
        } else if (i2 == 10) {
            this.l = v();
        } else if (i2 != 11) {
            throw new IllegalStateException("Expected a double but was " + f() + " at line " + r() + " column " + s() + " path " + t());
        }
        this.f579a = 11;
        double parseDouble = Double.parseDouble(this.l);
        if (this.d || (!Double.isNaN(parseDouble) && !Double.isInfinite(parseDouble))) {
            this.l = null;
            this.f579a = 0;
            int[] iArr2 = this.p;
            int i4 = this.n - 1;
            iArr2[i4] = iArr2[i4] + 1;
            return parseDouble;
        }
        throw new e("JSON forbids NaN and infinities: " + parseDouble + " at line " + r() + " column " + s() + " path " + t());
    }

    public long l() throws IOException {
        int i2 = this.f579a;
        if (i2 == 0) {
            i2 = q();
        }
        if (i2 == 15) {
            this.f579a = 0;
            int[] iArr = this.p;
            int i3 = this.n - 1;
            iArr[i3] = iArr[i3] + 1;
            return this.j;
        }
        if (i2 == 16) {
            this.l = new String(this.e, this.f, this.k);
            this.f += this.k;
        } else if (i2 == 8 || i2 == 9) {
            this.l = b(i2 == 8 ? '\'' : '\"');
            try {
                long parseLong = Long.parseLong(this.l);
                this.f579a = 0;
                int[] iArr2 = this.p;
                int i4 = this.n - 1;
                iArr2[i4] = iArr2[i4] + 1;
                return parseLong;
            } catch (NumberFormatException e2) {
            }
        } else {
            throw new IllegalStateException("Expected a long but was " + f() + " at line " + r() + " column " + s() + " path " + t());
        }
        this.f579a = 11;
        double parseDouble = Double.parseDouble(this.l);
        long j2 = (long) parseDouble;
        if (((double) j2) != parseDouble) {
            throw new NumberFormatException("Expected a long but was " + this.l + " at line " + r() + " column " + s() + " path " + t());
        }
        this.l = null;
        this.f579a = 0;
        int[] iArr3 = this.p;
        int i5 = this.n - 1;
        iArr3[i5] = iArr3[i5] + 1;
        return j2;
    }

    private String b(char c2) throws IOException {
        char[] cArr = this.e;
        StringBuilder sb = new StringBuilder();
        do {
            int i2 = this.f;
            int i3 = this.g;
            int i4 = i2;
            while (i4 < i3) {
                int i5 = i4 + 1;
                char c3 = cArr[i4];
                if (c3 == c2) {
                    this.f = i5;
                    sb.append(cArr, i2, (i5 - i2) - 1);
                    return sb.toString();
                }
                if (c3 == '\\') {
                    this.f = i5;
                    sb.append(cArr, i2, (i5 - i2) - 1);
                    sb.append(z());
                    i2 = this.f;
                    i3 = this.g;
                    i5 = i2;
                } else if (c3 == 10) {
                    this.h++;
                    this.i = i5;
                }
                i4 = i5;
            }
            sb.append(cArr, i2, i4 - i2);
            this.f = i4;
        } while (b(1));
        throw b("Unterminated string");
    }

    private String v() throws IOException {
        String sb;
        StringBuilder sb2 = null;
        int i2 = 0;
        while (true) {
            if (this.f + i2 < this.g) {
                switch (this.e[this.f + i2]) {
                    case 9:
                    case 10:
                    case 12:
                    case 13:
                    case ' ':
                    case D.QHVIDEOADONCLICKLISTENER_onDownloadCancelled /*44*/:
                    case ':':
                    case '[':
                    case ']':
                    case '{':
                    case '}':
                        break;
                    case D.QHNATIVEADLOADER_clearKeywords /*35*/:
                    case D.QHLANDINGPAGEVIEW_open /*47*/:
                    case ';':
                    case '=':
                    case '\\':
                        x();
                        break;
                    default:
                        i2++;
                }
            } else if (i2 >= this.e.length) {
                if (sb2 == null) {
                    sb2 = new StringBuilder();
                }
                sb2.append(this.e, this.f, i2);
                this.f = i2 + this.f;
                if (!b(1)) {
                    i2 = 0;
                } else {
                    i2 = 0;
                }
            } else if (b(i2 + 1)) {
            }
        }
        if (sb2 == null) {
            sb = new String(this.e, this.f, i2);
        } else {
            sb2.append(this.e, this.f, i2);
            sb = sb2.toString();
        }
        this.f = i2 + this.f;
        return sb;
    }

    private void c(char c2) throws IOException {
        char[] cArr = this.e;
        do {
            int i2 = this.f;
            int i3 = this.g;
            int i4 = i2;
            while (i4 < i3) {
                int i5 = i4 + 1;
                char c3 = cArr[i4];
                if (c3 == c2) {
                    this.f = i5;
                    return;
                }
                if (c3 == '\\') {
                    this.f = i5;
                    z();
                    i5 = this.f;
                    i3 = this.g;
                } else if (c3 == 10) {
                    this.h++;
                    this.i = i5;
                }
                i4 = i5;
            }
            this.f = i4;
        } while (b(1));
        throw b("Unterminated string");
    }

    private void w() throws IOException {
        do {
            int i2 = 0;
            while (this.f + i2 < this.g) {
                switch (this.e[this.f + i2]) {
                    case 9:
                    case 10:
                    case 12:
                    case 13:
                    case ' ':
                    case D.QHVIDEOADONCLICKLISTENER_onDownloadCancelled /*44*/:
                    case ':':
                    case '[':
                    case ']':
                    case '{':
                    case '}':
                        this.f = i2 + this.f;
                        return;
                    case D.QHNATIVEADLOADER_clearKeywords /*35*/:
                    case D.QHLANDINGPAGEVIEW_open /*47*/:
                    case ';':
                    case '=':
                    case '\\':
                        x();
                        this.f = i2 + this.f;
                        return;
                    default:
                        i2++;
                }
            }
            this.f = i2 + this.f;
        } while (b(1));
    }

    public int m() throws IOException {
        int i2 = this.f579a;
        if (i2 == 0) {
            i2 = q();
        }
        if (i2 == 15) {
            int i3 = (int) this.j;
            if (this.j != ((long) i3)) {
                throw new NumberFormatException("Expected an int but was " + this.j + " at line " + r() + " column " + s() + " path " + t());
            }
            this.f579a = 0;
            int[] iArr = this.p;
            int i4 = this.n - 1;
            iArr[i4] = iArr[i4] + 1;
            return i3;
        }
        if (i2 == 16) {
            this.l = new String(this.e, this.f, this.k);
            this.f += this.k;
        } else if (i2 == 8 || i2 == 9) {
            this.l = b(i2 == 8 ? '\'' : '\"');
            try {
                int parseInt = Integer.parseInt(this.l);
                this.f579a = 0;
                int[] iArr2 = this.p;
                int i5 = this.n - 1;
                iArr2[i5] = iArr2[i5] + 1;
                return parseInt;
            } catch (NumberFormatException e2) {
            }
        } else {
            throw new IllegalStateException("Expected an int but was " + f() + " at line " + r() + " column " + s() + " path " + t());
        }
        this.f579a = 11;
        double parseDouble = Double.parseDouble(this.l);
        int i6 = (int) parseDouble;
        if (((double) i6) != parseDouble) {
            throw new NumberFormatException("Expected an int but was " + this.l + " at line " + r() + " column " + s() + " path " + t());
        }
        this.l = null;
        this.f579a = 0;
        int[] iArr3 = this.p;
        int i7 = this.n - 1;
        iArr3[i7] = iArr3[i7] + 1;
        return i6;
    }

    public void close() throws IOException {
        this.f579a = 0;
        this.m[0] = 8;
        this.n = 1;
        this.c.close();
    }

    public void n() throws IOException {
        int i2 = 0;
        do {
            int i3 = this.f579a;
            if (i3 == 0) {
                i3 = q();
            }
            if (i3 == 3) {
                a(1);
                i2++;
            } else if (i3 == 1) {
                a(3);
                i2++;
            } else if (i3 == 4) {
                this.n--;
                i2--;
            } else if (i3 == 2) {
                this.n--;
                i2--;
            } else if (i3 == 14 || i3 == 10) {
                w();
            } else if (i3 == 8 || i3 == 12) {
                c('\'');
            } else if (i3 == 9 || i3 == 13) {
                c('\"');
            } else if (i3 == 16) {
                this.f += this.k;
            }
            this.f579a = 0;
        } while (i2 != 0);
        int[] iArr = this.p;
        int i4 = this.n - 1;
        iArr[i4] = iArr[i4] + 1;
        this.o[this.n - 1] = "null";
    }

    private void a(int i2) {
        if (this.n == this.m.length) {
            int[] iArr = new int[(this.n * 2)];
            int[] iArr2 = new int[(this.n * 2)];
            String[] strArr = new String[(this.n * 2)];
            System.arraycopy(this.m, 0, iArr, 0, this.n);
            System.arraycopy(this.p, 0, iArr2, 0, this.n);
            System.arraycopy(this.o, 0, strArr, 0, this.n);
            this.m = iArr;
            this.p = iArr2;
            this.o = strArr;
        }
        int[] iArr3 = this.m;
        int i3 = this.n;
        this.n = i3 + 1;
        iArr3[i3] = i2;
    }

    private boolean b(int i2) throws IOException {
        char[] cArr = this.e;
        this.i -= this.f;
        if (this.g != this.f) {
            this.g -= this.f;
            System.arraycopy(cArr, this.f, cArr, 0, this.g);
        } else {
            this.g = 0;
        }
        this.f = 0;
        do {
            int read = this.c.read(cArr, this.g, cArr.length - this.g);
            if (read == -1) {
                return false;
            }
            this.g = read + this.g;
            if (this.h == 0 && this.i == 0 && this.g > 0 && cArr[0] == 65279) {
                this.f++;
                this.i++;
                i2++;
            }
        } while (this.g < i2);
        return true;
    }

    /* access modifiers changed from: package-private */
    public int r() {
        return this.h + 1;
    }

    /* access modifiers changed from: package-private */
    public int s() {
        return (this.f - this.i) + 1;
    }

    private int b(boolean z) throws IOException {
        char[] cArr = this.e;
        int i2 = this.f;
        int i3 = this.g;
        while (true) {
            if (i2 == i3) {
                this.f = i2;
                if (b(1)) {
                    i2 = this.f;
                    i3 = this.g;
                } else if (!z) {
                    return -1;
                } else {
                    throw new EOFException("End of input at line " + r() + " column " + s());
                }
            }
            int i4 = i2 + 1;
            char c2 = cArr[i2];
            if (c2 == 10) {
                this.h++;
                this.i = i4;
                i2 = i4;
            } else if (c2 == ' ' || c2 == 13) {
                i2 = i4;
            } else if (c2 == 9) {
                i2 = i4;
            } else if (c2 == '/') {
                this.f = i4;
                if (i4 == i3) {
                    this.f--;
                    boolean b2 = b(2);
                    this.f++;
                    if (!b2) {
                        return c2;
                    }
                }
                x();
                switch (cArr[this.f]) {
                    case D.QHVIDEOAD_onAdClicked /*42*/:
                        this.f++;
                        if (!a("*/")) {
                            throw b("Unterminated comment");
                        }
                        i2 = this.f + 2;
                        i3 = this.g;
                        continue;
                    case D.QHLANDINGPAGEVIEW_open /*47*/:
                        this.f++;
                        y();
                        i2 = this.f;
                        i3 = this.g;
                        continue;
                    default:
                        return c2;
                }
            } else if (c2 == '#') {
                this.f = i4;
                x();
                y();
                i2 = this.f;
                i3 = this.g;
            } else {
                this.f = i4;
                return c2;
            }
        }
    }

    private void x() throws IOException {
        if (!this.d) {
            throw b("Use JsonReader.setLenient(true) to accept malformed JSON");
        }
    }

    private void y() throws IOException {
        char c2;
        do {
            if (this.f < this.g || b(1)) {
                char[] cArr = this.e;
                int i2 = this.f;
                this.f = i2 + 1;
                c2 = cArr[i2];
                if (c2 == 10) {
                    this.h++;
                    this.i = this.f;
                    return;
                }
            } else {
                return;
            }
        } while (c2 != 13);
    }

    private boolean a(String str) throws IOException {
        while (true) {
            if (this.f + str.length() > this.g && !b(str.length())) {
                return false;
            }
            if (this.e[this.f] == 10) {
                this.h++;
                this.i = this.f + 1;
            } else {
                int i2 = 0;
                while (i2 < str.length()) {
                    if (this.e[this.f + i2] == str.charAt(i2)) {
                        i2++;
                    }
                }
                return true;
            }
            this.f++;
        }
    }

    public String toString() {
        return getClass().getSimpleName() + " at line " + r() + " column " + s();
    }

    public String t() {
        StringBuilder append = new StringBuilder().append('$');
        int i2 = this.n;
        for (int i3 = 0; i3 < i2; i3++) {
            switch (this.m[i3]) {
                case 1:
                case 2:
                    append.append('[').append(this.p[i3]).append(']');
                    break;
                case 3:
                case 4:
                case 5:
                    append.append('.');
                    if (this.o[i3] == null) {
                        break;
                    } else {
                        append.append(this.o[i3]);
                        break;
                    }
            }
        }
        return append.toString();
    }

    private char z() throws IOException {
        int i2;
        if (this.f != this.g || b(1)) {
            char[] cArr = this.e;
            int i3 = this.f;
            this.f = i3 + 1;
            char c2 = cArr[i3];
            switch (c2) {
                case 10:
                    this.h++;
                    this.i = this.f;
                    return c2;
                case 'b':
                    return 8;
                case Constants.REGISTER_OK:
                    return 12;
                case 'n':
                    return 10;
                case 'r':
                    return 13;
                case 't':
                    return 9;
                case 'u':
                    if (this.f + 4 <= this.g || b(4)) {
                        int i4 = this.f;
                        int i5 = i4 + 4;
                        int i6 = i4;
                        char c3 = 0;
                        for (int i7 = i6; i7 < i5; i7++) {
                            char c4 = this.e[i7];
                            char c5 = (char) (c3 << 4);
                            if (c4 >= '0' && c4 <= '9') {
                                i2 = c4 - '0';
                            } else if (c4 >= 'a' && c4 <= 'f') {
                                i2 = (c4 - 'a') + 10;
                            } else if (c4 < 'A' || c4 > 'F') {
                                throw new NumberFormatException("\\u" + new String(this.e, this.f, 4));
                            } else {
                                i2 = (c4 - 'A') + 10;
                            }
                            c3 = (char) (c5 + i2);
                        }
                        this.f += 4;
                        return c3;
                    }
                    throw b("Unterminated escape sequence");
                default:
                    return c2;
            }
        } else {
            throw b("Unterminated escape sequence");
        }
    }

    private IOException b(String str) throws IOException {
        throw new e(str + " at line " + r() + " column " + s() + " path " + t());
    }

    private void A() throws IOException {
        b(true);
        this.f--;
        if (this.f + b.length <= this.g || b(b.length)) {
            int i2 = 0;
            while (i2 < b.length) {
                if (this.e[this.f + i2] == b[i2]) {
                    i2++;
                } else {
                    return;
                }
            }
            this.f += b.length;
        }
    }
}
