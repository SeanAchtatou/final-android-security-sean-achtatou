package com.google.a;

import com.google.a.d.a;
import com.google.a.d.d;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicLong;

/* compiled from: Gson */
final class p extends af<AtomicLong> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ af f588a;

    p(af afVar) {
        this.f588a = afVar;
    }

    public void a(d dVar, AtomicLong atomicLong) throws IOException {
        this.f588a.a(dVar, Long.valueOf(atomicLong.get()));
    }

    /* renamed from: a */
    public AtomicLong b(a aVar) throws IOException {
        return new AtomicLong(((Number) this.f588a.b(aVar)).longValue());
    }
}
