package com.google.a;

import com.google.a.d.a;
import com.google.a.d.c;
import com.google.a.d.d;
import java.io.IOException;

/* compiled from: Gson */
class m extends af<Number> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ j f586a;

    m(j jVar) {
        this.f586a = jVar;
    }

    /* renamed from: a */
    public Double b(a aVar) throws IOException {
        if (aVar.f() != c.NULL) {
            return Double.valueOf(aVar.k());
        }
        aVar.j();
        return null;
    }

    public void a(d dVar, Number number) throws IOException {
        if (number == null) {
            dVar.f();
            return;
        }
        j.a(number.doubleValue());
        dVar.a(number);
    }
}
