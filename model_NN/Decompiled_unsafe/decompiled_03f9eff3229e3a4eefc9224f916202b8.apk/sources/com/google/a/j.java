package com.google.a;

import com.google.a.b.a.d;
import com.google.a.b.a.f;
import com.google.a.b.a.k;
import com.google.a.b.a.l;
import com.google.a.b.a.o;
import com.google.a.b.a.q;
import com.google.a.b.a.s;
import com.google.a.b.a.v;
import com.google.a.b.aa;
import com.google.a.b.c;
import com.google.a.b.r;
import com.google.a.d.e;
import java.io.EOFException;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicLongArray;

/* compiled from: Gson */
public final class j {

    /* renamed from: a  reason: collision with root package name */
    final t f582a;
    final aa b;
    private final ThreadLocal<Map<com.google.a.c.a<?>, a<?>>> c;
    private final Map<com.google.a.c.a<?>, af<?>> d;
    private final List<ah> e;
    private final c f;
    private final boolean g;
    private final boolean h;
    private final boolean i;
    private final boolean j;
    private final boolean k;

    public j() {
        this(r.f567a, c.IDENTITY, Collections.emptyMap(), false, false, false, true, false, false, false, ac.DEFAULT, Collections.emptyList());
    }

    j(r rVar, i iVar, Map<Type, r<?>> map, boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7, ac acVar, List<ah> list) {
        this.c = new ThreadLocal<>();
        this.d = Collections.synchronizedMap(new HashMap());
        this.f582a = new k(this);
        this.b = new l(this);
        this.f = new c(map);
        this.g = z;
        this.i = z3;
        this.h = z4;
        this.j = z5;
        this.k = z6;
        ArrayList arrayList = new ArrayList();
        arrayList.add(v.Y);
        arrayList.add(l.f534a);
        arrayList.add(rVar);
        arrayList.addAll(list);
        arrayList.add(v.D);
        arrayList.add(v.m);
        arrayList.add(v.g);
        arrayList.add(v.i);
        arrayList.add(v.k);
        af<Number> a2 = a(acVar);
        arrayList.add(v.a(Long.TYPE, Long.class, a2));
        arrayList.add(v.a(Double.TYPE, Double.class, a(z7)));
        arrayList.add(v.a(Float.TYPE, Float.class, b(z7)));
        arrayList.add(v.x);
        arrayList.add(v.o);
        arrayList.add(v.q);
        arrayList.add(v.a(AtomicLong.class, a(a2)));
        arrayList.add(v.a(AtomicLongArray.class, b(a2)));
        arrayList.add(v.s);
        arrayList.add(v.z);
        arrayList.add(v.F);
        arrayList.add(v.H);
        arrayList.add(v.a(BigDecimal.class, v.B));
        arrayList.add(v.a(BigInteger.class, v.C));
        arrayList.add(v.J);
        arrayList.add(v.L);
        arrayList.add(v.P);
        arrayList.add(v.R);
        arrayList.add(v.W);
        arrayList.add(v.N);
        arrayList.add(v.d);
        arrayList.add(d.f529a);
        arrayList.add(v.U);
        arrayList.add(s.f540a);
        arrayList.add(q.f539a);
        arrayList.add(v.S);
        arrayList.add(com.google.a.b.a.a.f518a);
        arrayList.add(v.b);
        arrayList.add(new com.google.a.b.a.c(this.f));
        arrayList.add(new k(this.f, z2));
        arrayList.add(new f(this.f));
        arrayList.add(v.Z);
        arrayList.add(new o(this.f, iVar, rVar));
        this.e = Collections.unmodifiableList(arrayList);
    }

    private af<Number> a(boolean z) {
        if (z) {
            return v.v;
        }
        return new m(this);
    }

    private af<Number> b(boolean z) {
        if (z) {
            return v.u;
        }
        return new n(this);
    }

    static void a(double d2) {
        if (Double.isNaN(d2) || Double.isInfinite(d2)) {
            throw new IllegalArgumentException(d2 + " is not a valid double value as per JSON specification. To override this" + " behavior, use GsonBuilder.serializeSpecialFloatingPointValues() method.");
        }
    }

    private static af<Number> a(ac acVar) {
        if (acVar == ac.DEFAULT) {
            return v.t;
        }
        return new o();
    }

    private static af<AtomicLong> a(af<Number> afVar) {
        return new p(afVar).a();
    }

    private static af<AtomicLongArray> b(af<Number> afVar) {
        return new q(afVar).a();
    }

    public <T> af<T> a(com.google.a.c.a<T> aVar) {
        HashMap hashMap;
        af<T> afVar = this.d.get(aVar);
        if (afVar == null) {
            Map map = this.c.get();
            boolean z = false;
            if (map == null) {
                HashMap hashMap2 = new HashMap();
                this.c.set(hashMap2);
                hashMap = hashMap2;
                z = true;
            } else {
                hashMap = map;
            }
            afVar = (a) hashMap.get(aVar);
            if (afVar == null) {
                try {
                    a aVar2 = new a();
                    hashMap.put(aVar, aVar2);
                    for (ah a2 : this.e) {
                        afVar = a2.a(this, aVar);
                        if (afVar != null) {
                            aVar2.a((af) afVar);
                            this.d.put(aVar, afVar);
                            hashMap.remove(aVar);
                            if (z) {
                                this.c.remove();
                            }
                        }
                    }
                    throw new IllegalArgumentException("GSON cannot handle " + aVar);
                } catch (Throwable th) {
                    hashMap.remove(aVar);
                    if (z) {
                        this.c.remove();
                    }
                    throw th;
                }
            }
        }
        return afVar;
    }

    public <T> af<T> a(ah ahVar, com.google.a.c.a aVar) {
        boolean z = false;
        if (!this.e.contains(ahVar)) {
            z = true;
        }
        boolean z2 = z;
        for (ah next : this.e) {
            if (z2) {
                af<T> a2 = next.a(this, aVar);
                if (a2 != null) {
                    return a2;
                }
            } else if (next == ahVar) {
                z2 = true;
            }
        }
        throw new IllegalArgumentException("GSON cannot serialize " + aVar);
    }

    public <T> af<T> a(Class<T> cls) {
        return a(com.google.a.c.a.b(cls));
    }

    public com.google.a.d.a a(Reader reader) {
        com.google.a.d.a aVar = new com.google.a.d.a(reader);
        aVar.a(this.k);
        return aVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.a.j.a(java.lang.String, java.lang.reflect.Type):T
     arg types: [java.lang.String, java.lang.Class]
     candidates:
      com.google.a.j.a(java.lang.Object, com.google.a.d.a):void
      com.google.a.j.a(com.google.a.ah, com.google.a.c.a):com.google.a.af<T>
      com.google.a.j.a(com.google.a.d.a, java.lang.reflect.Type):T
      com.google.a.j.a(java.io.Reader, java.lang.reflect.Type):T
      com.google.a.j.a(java.lang.String, java.lang.Class):T
      com.google.a.j.a(java.lang.String, java.lang.reflect.Type):T */
    public <T> T a(String str, Class cls) throws ab {
        return aa.a(cls).cast(a(str, (Type) cls));
    }

    public <T> T a(String str, Type type) throws ab {
        if (str == null) {
            return null;
        }
        return a(new StringReader(str), type);
    }

    public <T> T a(Reader reader, Type type) throws v, ab {
        com.google.a.d.a a2 = a(reader);
        T a3 = a(a2, type);
        a(a3, a2);
        return a3;
    }

    private static void a(Object obj, com.google.a.d.a aVar) {
        if (obj != null) {
            try {
                if (aVar.f() != com.google.a.d.c.END_DOCUMENT) {
                    throw new v("JSON document was not fully consumed.");
                }
            } catch (e e2) {
                throw new ab(e2);
            } catch (IOException e3) {
                throw new v(e3);
            }
        }
    }

    public <T> T a(com.google.a.d.a aVar, Type type) throws v, ab {
        boolean z = true;
        boolean p = aVar.p();
        aVar.a(true);
        try {
            aVar.f();
            z = false;
            T b2 = a(com.google.a.c.a.a(type)).b(aVar);
            aVar.a(p);
            return b2;
        } catch (EOFException e2) {
            if (z) {
                aVar.a(p);
                return null;
            }
            throw new ab(e2);
        } catch (IllegalStateException e3) {
            throw new ab(e3);
        } catch (IOException e4) {
            throw new ab(e4);
        } catch (Throwable th) {
            aVar.a(p);
            throw th;
        }
    }

    /* compiled from: Gson */
    static class a<T> extends af<T> {

        /* renamed from: a  reason: collision with root package name */
        private af<T> f583a;

        a() {
        }

        public void a(af<T> afVar) {
            if (this.f583a != null) {
                throw new AssertionError();
            }
            this.f583a = afVar;
        }

        public T b(com.google.a.d.a aVar) throws IOException {
            if (this.f583a != null) {
                return this.f583a.b(aVar);
            }
            throw new IllegalStateException();
        }

        public void a(com.google.a.d.d dVar, T t) throws IOException {
            if (this.f583a == null) {
                throw new IllegalStateException();
            }
            this.f583a.a(dVar, t);
        }
    }

    public String toString() {
        return "{serializeNulls:" + this.g + "factories:" + this.e + ",instanceCreators:" + this.f + "}";
    }
}
