package com.google.a;

import com.google.a.b.ab;
import com.google.a.d.d;
import java.io.IOException;
import java.io.StringWriter;

/* compiled from: JsonElement */
public abstract class u {
    public boolean g() {
        return this instanceof s;
    }

    public boolean h() {
        return this instanceof x;
    }

    public boolean i() {
        return this instanceof z;
    }

    public boolean j() {
        return this instanceof w;
    }

    public x k() {
        if (h()) {
            return (x) this;
        }
        throw new IllegalStateException("Not a JSON Object: " + this);
    }

    public s l() {
        if (g()) {
            return (s) this;
        }
        throw new IllegalStateException("This is not a JSON Array.");
    }

    public z m() {
        if (i()) {
            return (z) this;
        }
        throw new IllegalStateException("This is not a JSON Primitive.");
    }

    public boolean f() {
        throw new UnsupportedOperationException(getClass().getSimpleName());
    }

    /* access modifiers changed from: package-private */
    public Boolean n() {
        throw new UnsupportedOperationException(getClass().getSimpleName());
    }

    public Number a() {
        throw new UnsupportedOperationException(getClass().getSimpleName());
    }

    public String b() {
        throw new UnsupportedOperationException(getClass().getSimpleName());
    }

    public double c() {
        throw new UnsupportedOperationException(getClass().getSimpleName());
    }

    public long d() {
        throw new UnsupportedOperationException(getClass().getSimpleName());
    }

    public int e() {
        throw new UnsupportedOperationException(getClass().getSimpleName());
    }

    public String toString() {
        try {
            StringWriter stringWriter = new StringWriter();
            d dVar = new d(stringWriter);
            dVar.b(true);
            ab.a(this, dVar);
            return stringWriter.toString();
        } catch (IOException e) {
            throw new AssertionError(e);
        }
    }
}
