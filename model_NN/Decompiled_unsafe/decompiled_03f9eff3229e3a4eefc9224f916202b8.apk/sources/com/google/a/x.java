package com.google.a;

import com.google.a.b.v;
import java.util.Map;
import java.util.Set;

/* compiled from: JsonObject */
public final class x extends u {

    /* renamed from: a  reason: collision with root package name */
    private final v<String, u> f592a = new v<>();

    public void a(String str, u uVar) {
        if (uVar == null) {
            uVar = w.f591a;
        }
        this.f592a.put(str, uVar);
    }

    public Set<Map.Entry<String, u>> o() {
        return this.f592a.entrySet();
    }

    public boolean equals(Object obj) {
        return obj == this || ((obj instanceof x) && ((x) obj).f592a.equals(this.f592a));
    }

    public int hashCode() {
        return this.f592a.hashCode();
    }
}
