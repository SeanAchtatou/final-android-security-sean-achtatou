package com.google.a;

import com.google.a.d.a;
import com.google.a.d.c;
import com.google.a.d.d;
import java.io.IOException;

/* compiled from: TypeAdapter */
class ag extends af<T> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ af f516a;

    ag(af afVar) {
        this.f516a = afVar;
    }

    public void a(d dVar, T t) throws IOException {
        if (t == null) {
            dVar.f();
        } else {
            this.f516a.a(dVar, t);
        }
    }

    public T b(a aVar) throws IOException {
        if (aVar.f() != c.NULL) {
            return this.f516a.b(aVar);
        }
        aVar.j();
        return null;
    }
}
