package com.baidu.mobads;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.baidu.mobads.interfaces.utils.IXAdURIUitls;
import com.baidu.mobads.j.m;
import com.baidu.mobads.openad.e.a;
import com.baidu.mobads.openad.e.d;

class h extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppActivity f293a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    h(AppActivity appActivity, Looper looper) {
        super(looper);
        this.f293a = appActivity;
    }

    public void handleMessage(Message message) {
        m.a().m();
        IXAdURIUitls i = m.a().i();
        StringBuilder sb = new StringBuilder("type=" + message.what + "&");
        String str = "";
        if (message.what == AppActivity.G) {
            str = this.f293a.A.toString();
        }
        try {
            d dVar = new d(i.addParameters("http://mobads-logs.baidu.com/dz.zb?" + sb.append(str).toString(), null), "");
            dVar.e = 1;
            new a().a(dVar);
        } catch (Exception e) {
            this.f293a.D.d(AppActivity.o, e.getMessage());
        }
    }
}
