package com.baidu.mobads.production.c;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Rect;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.webkit.CookieManager;
import com.baidu.mobads.interfaces.IXAdRequestInfo;
import com.baidu.mobads.interfaces.utils.IXAdCommonUtils;
import com.baidu.mobads.interfaces.utils.IXAdSystemUtils;
import com.baidu.mobads.j.m;
import java.util.HashSet;
import java.util.Set;

public class b {

    /* renamed from: a  reason: collision with root package name */
    private Set<String> f373a;
    private Set<String> b;
    private Set<String> c;
    private CookieManager d;
    private IXAdSystemUtils e = m.a().n();
    private IXAdCommonUtils f = m.a().m();
    private Context g;
    private int h;
    private String i;

    public b(Context context, int i2, String str) {
        this.g = context;
        this.h = i2;
        this.i = str;
        this.d = CookieManager.getInstance();
        this.d.setAcceptCookie(true);
        b();
    }

    public String a() {
        c();
        return "http://cpu.baidu.com/" + this.h + "/" + this.i;
    }

    private void b() {
        this.f373a = new HashSet();
        this.f373a.add("46000");
        this.f373a.add("46002");
        this.f373a.add("46007");
        this.b = new HashSet();
        this.b.add("46001");
        this.b.add("46006");
        this.c = new HashSet();
        this.c.add("46003");
        this.c.add("46005");
    }

    private void c() {
        int i2;
        String str = null;
        int i3 = 0;
        Rect screenRect = this.f.getScreenRect(this.g);
        int height = screenRect.height();
        int width = screenRect.width();
        boolean d2 = d();
        String a2 = d2 ? a(g()) : null;
        if (d2) {
            i2 = e();
        } else {
            i2 = 0;
        }
        if (d2) {
            str = f();
        }
        if (d2) {
            i3 = 1;
        }
        String cuid = this.e.getCUID(this.g);
        a(IXAdRequestInfo.V, h());
        a(IXAdRequestInfo.IMSI, this.e.getIMEI(this.g));
        a("aid", this.e.getAndroidId(this.g));
        a("m", a(this.e.getMacAddress(this.g)));
        a("cuid", cuid);
        a("ct", Integer.valueOf(a.a(this.g)));
        a("oi", Integer.valueOf(i()));
        a("src", 1);
        a(IXAdRequestInfo.HEIGHT, Integer.valueOf(height));
        a(IXAdRequestInfo.WIDTH, Integer.valueOf(width));
        a("apm", a2);
        a("rssi", Integer.valueOf(i2));
        a("apn", str);
        a("isc", Integer.valueOf(i3));
    }

    private void a(String str, Object obj) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(str);
        stringBuffer.append("=");
        stringBuffer.append(obj);
        stringBuffer.append(";");
        this.d.setCookie("http://cpu.baidu.com/", stringBuffer.toString());
    }

    private boolean d() {
        try {
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) this.g.getSystemService("connectivity")).getActiveNetworkInfo();
            if (activeNetworkInfo == null || activeNetworkInfo.getType() != 1) {
                return false;
            }
            return true;
        } catch (Exception e2) {
            e2.printStackTrace();
            return false;
        }
    }

    private int e() {
        try {
            WifiInfo connectionInfo = ((WifiManager) this.g.getSystemService(IXAdSystemUtils.NT_WIFI)).getConnectionInfo();
            if (connectionInfo == null) {
                return 0;
            }
            return connectionInfo.getRssi();
        } catch (Exception e2) {
            e2.printStackTrace();
            return 0;
        }
    }

    private String f() {
        try {
            WifiInfo connectionInfo = ((WifiManager) this.g.getSystemService(IXAdSystemUtils.NT_WIFI)).getConnectionInfo();
            String ssid = connectionInfo == null ? "" : connectionInfo.getSSID();
            if (ssid.startsWith("\"") && ssid.endsWith("\"")) {
                return ssid.substring(1, ssid.length() - 1);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return "";
    }

    private String g() {
        try {
            WifiInfo connectionInfo = ((WifiManager) this.g.getSystemService(IXAdSystemUtils.NT_WIFI)).getConnectionInfo();
            if (connectionInfo == null) {
                return null;
            }
            return connectionInfo.getMacAddress();
        } catch (Exception e2) {
            e2.printStackTrace();
            return "";
        }
    }

    private String h() {
        try {
            PackageInfo packageInfo = this.g.getPackageManager().getPackageInfo(this.g.getPackageName(), 0);
            String str = packageInfo == null ? null : packageInfo.versionName;
            if (str != null) {
                return str.replace(".", "-");
            }
            return null;
        } catch (PackageManager.NameNotFoundException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    private int i() {
        String networkOperator = this.e.getNetworkOperator(this.g);
        if (networkOperator == null) {
            return 0;
        }
        if (this.f373a.contains(networkOperator)) {
            return 1;
        }
        if (this.c.contains(networkOperator)) {
            return 2;
        }
        if (this.b.contains(networkOperator)) {
            return 3;
        }
        return 99;
    }

    private String a(String str) {
        if (str == null) {
            return null;
        }
        return str.replace(":", "-");
    }
}
