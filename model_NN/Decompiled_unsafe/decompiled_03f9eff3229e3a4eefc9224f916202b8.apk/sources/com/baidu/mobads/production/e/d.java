package com.baidu.mobads.production.e;

import android.view.View;
import android.widget.RelativeLayout;

class d implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RelativeLayout.LayoutParams f378a;
    final /* synthetic */ View b;
    final /* synthetic */ c c;

    d(c cVar, RelativeLayout.LayoutParams layoutParams, View view) {
        this.c = cVar;
        this.f378a = layoutParams;
        this.b = view;
    }

    public void run() {
        this.f378a.addRule(11);
        this.f378a.rightMargin = 0;
        this.f378a.topMargin = this.b.getTop();
        this.c.g.setLayoutParams(this.f378a);
    }
}
