package com.baidu.mobads.production.f;

import android.view.ViewGroup;

class d implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ b f381a;

    d(b bVar) {
        this.f381a = bVar;
    }

    public void run() {
        this.f381a.x.d("remote Interstitial.removeAd");
        try {
            if (this.f381a.e.getParent() != null) {
                ((ViewGroup) this.f381a.e.getParent()).removeView(this.f381a.e);
            }
            this.f381a.e.removeAllViews();
        } catch (Exception e) {
            this.f381a.x.d("Interstitial.removeAd", e);
        }
    }
}
