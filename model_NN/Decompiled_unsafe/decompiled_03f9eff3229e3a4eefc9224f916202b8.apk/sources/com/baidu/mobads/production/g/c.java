package com.baidu.mobads.production.g;

import android.view.ViewGroup;

class c implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ b f385a;

    c(b bVar) {
        this.f385a = bVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.mobads.production.g.b.a(com.baidu.mobads.production.g.b, boolean):boolean
     arg types: [com.baidu.mobads.production.g.b, int]
     candidates:
      com.baidu.mobads.production.g.b.a(com.baidu.mobads.production.g.b, android.content.Context):android.view.ViewGroup
      com.baidu.mobads.production.g.b.a(int, int):void
      com.baidu.mobads.production.g.b.a(android.app.Activity, android.widget.RelativeLayout):void
      com.baidu.mobads.production.a.a(com.baidu.mobads.interfaces.error.XAdErrorCode, java.lang.String):void
      com.baidu.mobads.production.a.a(android.content.Context, java.lang.String):void
      com.baidu.mobads.production.a.a(com.baidu.mobads.interfaces.IXAdContainer, java.util.HashMap<java.lang.String, java.lang.Object>):void
      com.baidu.mobads.production.a.a(com.baidu.mobads.interfaces.IXAdResponseInfo, com.baidu.mobads.interfaces.IXAdInstanceInfo):void
      com.baidu.mobads.production.a.a(int, android.view.KeyEvent):boolean
      com.baidu.mobads.production.g.a.a(int, int):void
      com.baidu.mobads.production.g.a.a(android.app.Activity, android.widget.RelativeLayout):void
      com.baidu.mobads.production.g.a.a(int, android.view.KeyEvent):boolean
      com.baidu.mobads.production.g.b.a(com.baidu.mobads.production.g.b, boolean):boolean */
    public void run() {
        this.f385a.x.d("remote Interstitial.removeAd");
        boolean unused = this.f385a.B = false;
        try {
            this.f385a.e.removeAllViews();
            ViewGroup a2 = this.f385a.c(this.f385a.e.getContext());
            this.f385a.D.removeAllViews();
            a2.removeView(this.f385a.D);
        } catch (Exception e) {
            this.f385a.x.d("Interstitial.removeAd", e);
        }
    }
}
