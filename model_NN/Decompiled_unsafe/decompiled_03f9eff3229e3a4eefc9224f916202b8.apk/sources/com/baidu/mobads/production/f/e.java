package com.baidu.mobads.production.f;

import android.os.CountDownTimer;

class e extends CountDownTimer {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ b f382a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    e(b bVar, long j, long j2) {
        super(j, j2);
        this.f382a = bVar;
    }

    public void onTick(long j) {
        int i = 5;
        int i2 = (int) (j / 1000);
        if (i2 <= 5) {
            i = i2;
        }
        this.f382a.z.setText(String.valueOf(i));
    }

    public void onFinish() {
        this.f382a.x.d("CountDownTimer finished");
        this.f382a.q();
        this.f382a.h.stop();
    }
}
