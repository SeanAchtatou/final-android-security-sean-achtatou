package com.baidu.mobads.production.f;

import android.graphics.Color;

class c implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ b f380a;

    c(b bVar) {
        this.f380a = bVar;
    }

    public void run() {
        if (this.f380a.o()) {
            this.f380a.e.setBackgroundColor(Color.argb(51, 0, 0, 0));
        }
        if (this.f380a.h.getAdView() != null) {
            this.f380a.h.getAdView().setVisibility(0);
        }
        if (this.f380a.s()) {
            this.f380a.x.d("add countdown view");
            this.f380a.u();
            this.f380a.e.addView(this.f380a.t(), this.f380a.v());
        }
    }
}
