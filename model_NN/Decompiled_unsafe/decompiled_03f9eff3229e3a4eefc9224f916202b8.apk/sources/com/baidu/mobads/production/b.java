package com.baidu.mobads.production;

import com.baidu.mobads.c.a;
import com.baidu.mobads.j.m;
import com.baidu.mobads.openad.interfaces.event.IOAdEvent;
import com.baidu.mobads.openad.interfaces.event.IOAdEventListener;
import com.baidu.mobads.vo.c;
import org.json.JSONException;

class b implements IOAdEventListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f371a;

    b(a aVar) {
        this.f371a = aVar;
    }

    public void run(IOAdEvent iOAdEvent) {
        this.f371a.g();
        if ("URLLoader.Load.Complete".equals(iOAdEvent.getType())) {
            try {
                this.f371a.setAdResponseInfo(new c((String) iOAdEvent.getData().get(com.baidu.mobads.openad.d.b.EVENT_MESSAGE)));
                if (this.f371a.getAdResponseInfo().getAdInstanceList().size() > 0) {
                    this.f371a.b = true;
                    this.f371a.a("XAdMouldeLoader ad-server requesting success");
                    return;
                }
                m.a().q().printErrorMessage(this.f371a.getAdResponseInfo().getErrorCode(), this.f371a.getAdResponseInfo().getErrorMessage(), "");
                this.f371a.b(this.f371a.getAdResponseInfo().getErrorMessage());
            } catch (JSONException e) {
                m.a().q().printErrorMessage("", "response json parsing error", "");
                this.f371a.b("response json parsing error");
                a.a().a("response json parsing error");
            }
        } else {
            m.a().q().printErrorMessage("", "request ad-server error, io_err/timeout", "");
            this.f371a.b("request ad-server error, io_err/timeout");
            a.a().a("request ad-server error, io_err/timeout");
        }
    }
}
