package com.baidu.mobads.production.i;

import android.app.Activity;
import android.content.Context;
import com.baidu.mobads.AdSize;
import com.baidu.mobads.interfaces.IXAdConstants4PDK;
import com.baidu.mobads.interfaces.IXAdProd;
import com.baidu.mobads.interfaces.IXAdRequestInfo;
import com.baidu.mobads.vo.d;
import com.tencent.connect.common.Constants;
import java.util.HashMap;

public class a extends d {

    /* renamed from: a  reason: collision with root package name */
    protected IXAdProd f391a;

    public a(Context context, Activity activity, IXAdConstants4PDK.SlotType slotType, IXAdProd iXAdProd) {
        super(context, activity, slotType);
        this.b = "http://mobads.baidu.com/cpro/ui/mads.php";
        this.f391a = iXAdProd;
    }

    /* access modifiers changed from: protected */
    public HashMap<String, String> a() {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("slottype", this.f.getValue());
        hashMap.put("mimetype", "video/mp4,image/jpg,image/gif,image/png");
        hashMap.put("prod", "video");
        hashMap.put(IXAdRequestInfo.FET, "ANTI,HTML,MSSP,VIDEO,NMON");
        hashMap.put(IXAdRequestInfo.AD_TYPE, Constants.VIA_REPORT_TYPE_SHARE_TO_QQ);
        hashMap.put(IXAdRequestInfo.AD_COUNT, "1");
        if (this.f391a.getProdBase() == null && getApt() != AdSize.PrerollVideoNative.getValue()) {
            hashMap.put(IXAdRequestInfo.QUERY_WIDTH, "640");
            hashMap.put(IXAdRequestInfo.QUERY_HEIGHT, "480");
        }
        return hashMap;
    }

    public String b() {
        return super.b();
    }
}
