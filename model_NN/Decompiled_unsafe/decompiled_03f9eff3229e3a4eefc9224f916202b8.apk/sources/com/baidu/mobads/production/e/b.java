package com.baidu.mobads.production.e;

import android.app.Activity;
import android.graphics.Rect;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import com.baidu.mobads.interfaces.IXAdConstants4PDK;
import com.baidu.mobads.interfaces.IXAdContainer;
import com.baidu.mobads.interfaces.utils.IXAdCommonUtils;
import com.baidu.mobads.interfaces.utils.IXAdLogger;
import com.baidu.mobads.j.m;
import com.baidu.mobads.openad.e.d;
import com.baidu.mobads.production.t;
import java.util.HashMap;

public class b extends com.baidu.mobads.production.a {
    private boolean A = true;
    private double B = 0.5d;
    private int C = 0;
    private int D = 0;
    /* access modifiers changed from: private */
    public int E = 0;
    private ViewGroup F;
    private RelativeLayout G;
    protected final IXAdLogger w = m.a().f();
    IXAdCommonUtils x;
    private a y;
    /* access modifiers changed from: private */
    public RelativeLayout z;

    public b(Activity activity, String str, boolean z2, double d) {
        super(activity, str, IXAdConstants4PDK.SlotType.SLOT_TYPE_FRONTLINK);
        setActivity(activity);
        this.x = m.a().m();
        this.C = this.x.getPixel(activity, 80);
        this.D = this.x.getPixel(activity, 80);
        this.E = this.x.getStatusBarHeight(activity);
        this.B = d;
        this.A = z2;
        this.y = new a(getApplicationContext());
        this.y.d(str);
        getAdRequestInfo().a(this.D);
        getAdRequestInfo().b(this.C);
        a(activity);
        request();
    }

    /* access modifiers changed from: protected */
    public void d() {
        this.n = 4200;
    }

    public void request() {
        a(this.y);
    }

    /* access modifiers changed from: protected */
    public void a(d dVar, t tVar, int i) {
        tVar.a(dVar, (double) i);
    }

    /* access modifiers changed from: protected */
    public void c(IXAdContainer iXAdContainer, HashMap<String, Object> hashMap) {
        start();
    }

    /* access modifiers changed from: protected */
    public void d(IXAdContainer iXAdContainer, HashMap<String, Object> hashMap) {
        View adView = iXAdContainer.getAdView();
        a aVar = new a();
        aVar.b = adView;
        aVar.f376a = getActivity();
        aVar.d = 80;
        aVar.c = 80;
        a(aVar);
    }

    /* access modifiers changed from: protected */
    public void e(IXAdContainer iXAdContainer, HashMap<String, Object> hashMap) {
        super.e(iXAdContainer, hashMap);
        if (this.F != null && this.G != null) {
            this.F.removeView(this.G);
            this.F = null;
            this.G = null;
            l();
        }
    }

    public void l() {
        if (this.F != null && this.G != null) {
            this.F.removeView(this.G);
            this.F = null;
            this.G = null;
            super.l();
        }
    }

    static class a {

        /* renamed from: a  reason: collision with root package name */
        Activity f376a;
        View b;
        int c;
        int d;

        a() {
        }
    }

    private void a(Activity activity) {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(this.D, this.C);
        if (this.A) {
            layoutParams.addRule(9);
        } else {
            layoutParams.addRule(11);
        }
        int height = this.x.getScreenRect(activity).height();
        int i = (int) (((double) height) * this.B);
        int i2 = (height - this.C) - this.E;
        if (i <= i2) {
            i2 = i;
        }
        layoutParams.topMargin = i2;
        this.G = new RelativeLayout(activity);
        this.z = new RelativeLayout(activity);
        this.z.setBackgroundColor(0);
        setAdSlotBase(this.z);
        this.G.addView(this.z, layoutParams);
        this.G.setBackgroundColor(0);
        this.F = (ViewGroup) activity.getWindow().getDecorView();
        this.F.addView(this.G, new ViewGroup.LayoutParams(-1, -1));
    }

    /* access modifiers changed from: package-private */
    public void a(a aVar) {
        Activity activity = aVar.f376a;
        RelativeLayout relativeLayout = this.z;
        com.baidu.mobads.j.d m = m.a().m();
        Rect screenRect = m.getScreenRect(activity);
        this.h.getAdView().setOnTouchListener(new c(this, screenRect.width(), screenRect.height(), relativeLayout, m.getPixel(activity, aVar.c), m.getPixel(activity, aVar.d)));
    }

    /* renamed from: m */
    public com.baidu.mobads.vo.d getAdRequestInfo() {
        return this.y;
    }

    public void c() {
        if (this.h != null) {
            this.h.load();
        } else {
            this.w.e("container is null");
        }
    }
}
