package com.baidu.mobads.production.a;

import android.content.Context;
import android.webkit.WebView;
import android.widget.RelativeLayout;
import com.baidu.mobads.AdSize;
import com.baidu.mobads.interfaces.IXAdConstants4PDK;
import com.baidu.mobads.interfaces.IXAdContainer;
import com.baidu.mobads.interfaces.IXAdInstanceInfo;
import com.baidu.mobads.interfaces.IXNonLinearAdSlot;
import com.baidu.mobads.interfaces.event.IXAdEvent;
import com.baidu.mobads.j.m;
import com.baidu.mobads.production.t;
import com.baidu.mobads.vo.b;
import com.baidu.mobads.vo.c;
import com.baidu.mobads.vo.d;
import java.util.HashMap;
import org.json.JSONException;
import org.json.JSONObject;

public class a extends com.baidu.mobads.production.a implements IXNonLinearAdSlot {
    private c w = new c(getApplicationContext(), getActivity(), this.p);

    public a(Context context, RelativeLayout relativeLayout, String str, boolean z) {
        super(context);
        setId(str);
        setActivity(context);
        setAdSlotBase(relativeLayout);
        this.p = IXAdConstants4PDK.SlotType.SLOT_TYPE_BANNER;
        m.a().p();
        this.w.c(AdSize.Banner.getValue());
        this.w.d(str);
        b bVar = (b) this.w.d();
        bVar.a(z);
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("ABILITY", "BANNER_CLOSE,");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        bVar.a(jSONObject);
        c(str);
    }

    public void c() {
        load();
    }

    /* access modifiers changed from: protected */
    public void d() {
        this.n = 10000;
    }

    public void request() {
        a(this.w);
        try {
            new WebView(getActivity());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void b(d dVar) {
        this.l = dVar;
        g();
        a((com.baidu.mobads.openad.e.d) null, (t) null, 5000);
    }

    /* access modifiers changed from: protected */
    public void a(com.baidu.mobads.openad.e.d dVar, t tVar, int i) {
        String str = "{'ad':[{'id':99999999,'url':'" + this.w.b() + "', type='" + IXAdInstanceInfo.CreativeType.HTML.getValue() + "'}],'n':1}";
        this.b = true;
        try {
            setAdResponseInfo(new c(str));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        a("XAdMouldeLoader ad-server requesting success");
    }

    /* access modifiers changed from: protected */
    public void c(IXAdContainer iXAdContainer, HashMap<String, Object> hashMap) {
        start();
    }

    /* access modifiers changed from: protected */
    public void d(IXAdContainer iXAdContainer, HashMap<String, Object> hashMap) {
    }

    /* renamed from: m */
    public d getAdRequestInfo() {
        return this.w;
    }

    /* access modifiers changed from: protected */
    public void a() {
        new Thread(new b(this)).start();
    }

    /* access modifiers changed from: protected */
    public void e(IXAdContainer iXAdContainer, HashMap<String, Object> hashMap) {
        super.l();
        dispatchEvent(new com.baidu.mobads.f.a(IXAdEvent.AD_USER_CLOSE));
    }
}
