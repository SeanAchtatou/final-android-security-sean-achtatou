package com.baidu.mobads.production;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import com.baidu.mobads.interfaces.IXAdContainer;
import com.baidu.mobads.interfaces.IXAdContainerContext;
import com.baidu.mobads.interfaces.IXAdContainerEventListener;
import com.baidu.mobads.interfaces.IXAdInstanceInfo;
import com.baidu.mobads.interfaces.IXAdResource;
import com.baidu.mobads.interfaces.event.IXAdEvent;
import com.baidu.mobads.interfaces.utils.IXAdConstants;
import com.baidu.mobads.interfaces.utils.IXAdPackageUtils;
import com.baidu.mobads.j.d;
import com.baidu.mobads.j.m;
import com.baidu.mobads.openad.e.a;
import com.baidu.mobads.vo.XAdInstanceInfo;
import com.sina.weibo.sdk.constant.WBPageConstants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONObject;

public class o implements IXAdContainerEventListener {

    /* renamed from: a  reason: collision with root package name */
    private Context f397a;
    /* access modifiers changed from: private */
    public final a b;
    private AtomicBoolean c;
    private AtomicBoolean d;
    private AtomicBoolean e;
    private AtomicBoolean f;
    /* access modifiers changed from: private */
    public int g = 0;
    /* access modifiers changed from: private */
    public int h = 2;
    /* access modifiers changed from: private */
    public int i = 15;
    /* access modifiers changed from: private */
    public int j = 0;
    /* access modifiers changed from: private */
    public int k = 2;
    /* access modifiers changed from: private */
    public int l = 15;

    static /* synthetic */ int d(o oVar) {
        int i2 = oVar.g;
        oVar.g = i2 + 1;
        return i2;
    }

    static /* synthetic */ int h(o oVar) {
        int i2 = oVar.j;
        oVar.j = i2 + 1;
        return i2;
    }

    public o(Context context, a aVar) {
        this.f397a = context;
        this.b = aVar;
        this.c = new AtomicBoolean(false);
        this.d = new AtomicBoolean(false);
        this.e = new AtomicBoolean(false);
        this.f = new AtomicBoolean(false);
    }

    private void a(Context context, String str, String str2) {
        try {
            this.g = 0;
            Timer timer = new Timer();
            timer.schedule(new p(this, m.a().l(), context, str2, timer, str), 0, 1000);
        } catch (Exception e2) {
        }
    }

    private void a(Context context, String str) {
        try {
            this.j = 0;
            Timer timer = new Timer();
            timer.schedule(new q(this, m.a().l(), context, str, timer), 0, 1000);
        } catch (Exception e2) {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.mobads.openad.e.a.a(com.baidu.mobads.openad.e.d, java.lang.Boolean):void
     arg types: [com.baidu.mobads.openad.e.d, int]
     candidates:
      com.baidu.mobads.openad.e.a.a(com.baidu.mobads.openad.e.a, java.net.HttpURLConnection):java.net.HttpURLConnection
      com.baidu.mobads.openad.e.a.a(com.baidu.mobads.openad.e.d, double):void
      com.baidu.mobads.openad.e.a.a(com.baidu.mobads.openad.e.d, java.lang.Boolean):void */
    public void onAdClicked(IXAdContainer iXAdContainer, IXAdInstanceInfo iXAdInstanceInfo, Boolean bool, HashMap<String, Object> hashMap) {
        Boolean bool2;
        boolean z;
        JSONObject jSONObject;
        d m = m.a().m();
        IXAdConstants p = m.a().p();
        IXAdPackageUtils l2 = m.a().l();
        a aVar = this.b;
        IXAdResource adResource = iXAdContainer.getAdContainerContext().getAdResource();
        boolean z2 = false;
        String clickThroughUrl = iXAdInstanceInfo.getClickThroughUrl();
        int actionType = iXAdInstanceInfo.getActionType();
        ArrayList arrayList = new ArrayList();
        List<String> thirdClickTrackingUrls = iXAdInstanceInfo.getThirdClickTrackingUrls();
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= thirdClickTrackingUrls.size()) {
                break;
            }
            arrayList.add(thirdClickTrackingUrls.get(i3).replaceAll("\\$\\{PROGRESS\\}", String.valueOf((int) iXAdContainer.getPlayheadTime())));
            i2 = i3 + 1;
        }
        HashSet hashSet = new HashSet();
        hashSet.addAll(arrayList);
        a(hashSet);
        if (actionType == p.getActTypeOpenExternalApp()) {
            try {
                jSONObject = new JSONObject(clickThroughUrl);
            } catch (Exception e2) {
                try {
                    JSONObject jSONObject2 = new JSONObject(iXAdInstanceInfo.getAppOpenStrs());
                    a aVar2 = new a();
                    com.baidu.mobads.openad.e.d dVar = new com.baidu.mobads.openad.e.d(clickThroughUrl, "");
                    dVar.e = 1;
                    aVar2.a(dVar, (Boolean) true);
                    jSONObject = jSONObject2;
                } catch (Exception e3) {
                    return;
                }
            }
            String optString = jSONObject.optString(WBPageConstants.ParamKey.PAGE, "");
            if (!l2.sendAPOInfo(aVar.getApplicationContext(), optString, iXAdInstanceInfo.getAppPackageName(), 366, jSONObject.optInt("fb_act", 0))) {
                int optInt = jSONObject.optInt("fb_act", 0);
                XAdInstanceInfo xAdInstanceInfo = new XAdInstanceInfo(new JSONObject());
                if (optInt == p.getActTypeLandingPage()) {
                    xAdInstanceInfo.setActionType(p.getActTypeLandingPage());
                    xAdInstanceInfo.setClickThroughUrl(jSONObject.optString("fallback", ""));
                    xAdInstanceInfo.setTitle(iXAdInstanceInfo.getTitle());
                    xAdInstanceInfo.setInapp(true);
                    onAdClicked(iXAdContainer, xAdInstanceInfo, bool, hashMap);
                } else if (optInt == p.getActTypeDownload()) {
                    xAdInstanceInfo.setActionType(p.getActTypeDownload());
                    xAdInstanceInfo.setClickThroughUrl(jSONObject.optString("fallback", ""));
                    xAdInstanceInfo.setTitle(iXAdInstanceInfo.getTitle());
                    xAdInstanceInfo.setInapp(true);
                    xAdInstanceInfo.setAPOOpen(true);
                    xAdInstanceInfo.setPage(optString);
                    xAdInstanceInfo.setAppPackageName(iXAdInstanceInfo.getAppPackageName());
                    onAdClicked(iXAdContainer, xAdInstanceInfo, bool, hashMap);
                }
            } else if (bool.booleanValue()) {
                new com.baidu.mobads.command.b.a(aVar, iXAdInstanceInfo, adResource, optString).a();
            }
            a(aVar.getApplicationContext(), optString, iXAdInstanceInfo.getAppPackageName());
            bool2 = true;
        } else if (actionType == p.getActTypeDownload()) {
            bool2 = false;
            if (bool.booleanValue()) {
                new com.baidu.mobads.command.a.a(aVar, iXAdInstanceInfo, adResource).a();
            }
        } else if (actionType == p.getActTypeLandingPage() || actionType == p.getActTypeOpenMap()) {
            if (this.b.getProdInfo().getProdType() != p.getProductionTypeSplash()) {
                bool2 = true;
            } else {
                bool2 = false;
            }
            if (bool.booleanValue()) {
                if (iXAdInstanceInfo.isInapp()) {
                    new com.baidu.mobads.command.c.a(aVar, iXAdInstanceInfo, adResource, clickThroughUrl).a();
                } else {
                    m.browserOutside(iXAdContainer.getAdContainerContext().getApplicationContext(), clickThroughUrl);
                }
            }
        } else {
            if (actionType == p.getActTypeMakeCall() || actionType == p.getActTypeSendSMS() || actionType == p.getActTypeSendMail()) {
                z2 = true;
                if (bool.booleanValue()) {
                    new com.baidu.mobads.command.b.a(aVar, iXAdInstanceInfo, adResource, clickThroughUrl).a();
                }
                if (actionType == p.getActTypeMakeCall()) {
                    PackageManager packageManager = aVar.getApplicationContext().getPackageManager();
                    Intent intent = new Intent("android.intent.action.VIEW");
                    intent.setData(Uri.parse(clickThroughUrl));
                    List<ResolveInfo> queryIntentActivities = packageManager.queryIntentActivities(intent, 64);
                    if (queryIntentActivities != null && queryIntentActivities.size() > 0) {
                        int i4 = 0;
                        String str = null;
                        while (true) {
                            int i5 = i4;
                            if (i5 < queryIntentActivities.size()) {
                                if (i5 >= 1 && !str.equals(queryIntentActivities.get(i5).activityInfo.processName)) {
                                    z = false;
                                    break;
                                } else {
                                    str = queryIntentActivities.get(i5).activityInfo.processName;
                                    i4 = i5 + 1;
                                }
                            } else {
                                z = true;
                                break;
                            }
                        }
                        if (z) {
                            a(aVar.getApplicationContext(), str);
                        }
                    }
                    bool2 = true;
                }
            } else if (actionType == p.getActTypeNothing2Do()) {
                bool2 = false;
            } else if (actionType == p.getActTypeRichMedia()) {
            }
            bool2 = z2;
        }
        if (bool2.booleanValue()) {
            this.b.dispatchEvent(new com.baidu.mobads.f.a(IXAdEvent.AD_CLICK_THRU));
        }
        this.b.dispatchEvent(new com.baidu.mobads.f.a("AdUserClick"));
    }

    public void onAdLoaded(IXAdContainer iXAdContainer, IXAdInstanceInfo iXAdInstanceInfo, Boolean bool, HashMap<String, Object> hashMap) {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            this.b.a(iXAdContainer, hashMap);
        } else {
            new Handler(this.f397a.getMainLooper()).post(new r(this, iXAdContainer, hashMap));
        }
    }

    public void onAdStarted(IXAdContainer iXAdContainer, IXAdInstanceInfo iXAdInstanceInfo, Boolean bool, HashMap<String, Object> hashMap) {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            this.b.b(iXAdContainer, hashMap);
        } else {
            new Handler(this.f397a.getMainLooper()).post(new s(this, iXAdContainer, hashMap));
        }
    }

    public void onAdImpression(IXAdContainer iXAdContainer, IXAdInstanceInfo iXAdInstanceInfo, Boolean bool, HashMap<String, Object> hashMap) {
        a(iXAdInstanceInfo.getImpressionUrls());
    }

    public void onAdStoped(IXAdContainer iXAdContainer, IXAdInstanceInfo iXAdInstanceInfo, Boolean bool, Boolean bool2, HashMap<String, Object> hashMap) {
        if (iXAdInstanceInfo != null) {
            HashSet hashSet = new HashSet();
            hashSet.addAll(iXAdInstanceInfo.getCloseTrackers());
            a(hashSet);
        }
        if (bool2.booleanValue()) {
            IXAdContainerContext adContainerContext = iXAdContainer.getAdContainerContext();
            this.b.a(adContainerContext.getAdResponseInfo(), adContainerContext.getAdInstanceInfo());
            return;
        }
        this.b.e(iXAdContainer, hashMap);
        this.b.dispatchEvent(new com.baidu.mobads.f.a(IXAdEvent.AD_STOPPED));
    }

    public void onAdError(IXAdContainer iXAdContainer, IXAdInstanceInfo iXAdInstanceInfo, Boolean bool, HashMap<String, Object> hashMap) {
        if (!this.e.get()) {
            if (hashMap != null) {
                IXAdConstants p = m.a().p();
                com.baidu.mobads.c.a.a().a(hashMap.get(p.getInfoKeyErrorCode()) + "," + hashMap.get(p.getInfoKeyErrorMessage()) + "," + hashMap.get(p.getInfoKeyErrorModule()));
            }
            this.e.set(true);
            this.b.dispatchEvent(new com.baidu.mobads.f.a(IXAdEvent.AD_ERROR, hashMap));
        }
    }

    public void onAdPlaying(IXAdContainer iXAdContainer, Boolean bool, HashMap<String, Object> hashMap) {
        this.b.dispatchEvent(new com.baidu.mobads.f.a(IXAdEvent.AD_PLAYING));
    }

    public void onAdPaused(IXAdContainer iXAdContainer, Boolean bool, HashMap<String, Object> hashMap) {
        this.b.dispatchEvent(new com.baidu.mobads.f.a(IXAdEvent.AD_PAUSED));
    }

    public void onAdLinearChange(IXAdContainer iXAdContainer, Boolean bool, HashMap<String, Object> hashMap) {
        if (this.e.get()) {
        }
    }

    public void onAdExpandedChange(IXAdContainer iXAdContainer, Boolean bool, HashMap<String, Object> hashMap) {
        if (this.e.get()) {
        }
    }

    public void onAdUserClosed(IXAdContainer iXAdContainer, Boolean bool, HashMap<String, Object> hashMap) {
        this.b.dispatchEvent(new com.baidu.mobads.f.a(IXAdEvent.AD_USER_CLOSE));
    }

    public void onAdDurationChange(IXAdContainer iXAdContainer, Boolean bool, HashMap<String, Object> hashMap) {
        if (this.e.get()) {
        }
    }

    public void onAdRemainingTimeChange(IXAdContainer iXAdContainer, Boolean bool, HashMap<String, Object> hashMap) {
        if (this.e.get()) {
        }
    }

    public void onAdVolumeChange(IXAdContainer iXAdContainer, Boolean bool, HashMap<String, Object> hashMap) {
        if (this.e.get()) {
        }
    }

    public void onAdSizeChange(IXAdContainer iXAdContainer, Boolean bool, HashMap<String, Object> hashMap) {
        if (this.e.get()) {
        }
    }

    public void onAdSkippableStateChange(IXAdContainer iXAdContainer, Boolean bool, HashMap<String, Object> hashMap) {
        if (this.e.get()) {
        }
    }

    public void onAdSkipped(IXAdContainer iXAdContainer, Boolean bool, HashMap<String, Object> hashMap) {
        if (this.e.get()) {
        }
    }

    public void onAdInteraction(IXAdContainer iXAdContainer, Boolean bool, HashMap<String, Object> hashMap) {
        if (this.e.get()) {
        }
    }

    public void onAdUserAcceptInvitation(IXAdContainer iXAdContainer, Boolean bool, HashMap<String, Object> hashMap) {
        if (this.e.get()) {
        }
    }

    public void onAdUserMinimize(IXAdContainer iXAdContainer, Boolean bool, HashMap<String, Object> hashMap) {
        if (this.e.get()) {
        }
    }

    public void onAdVideoStart(IXAdContainer iXAdContainer, Boolean bool, HashMap<String, Object> hashMap) {
        if (this.e.get()) {
        }
    }

    public void onAdVideoFirstQuartile(IXAdContainer iXAdContainer, Boolean bool, HashMap<String, Object> hashMap) {
        if (this.e.get()) {
        }
    }

    public void onAdVideoMidpoint(IXAdContainer iXAdContainer, Boolean bool, HashMap<String, Object> hashMap) {
        if (this.e.get()) {
        }
    }

    public void onAdVideoThirdQuartile(IXAdContainer iXAdContainer, Boolean bool, HashMap<String, Object> hashMap) {
        if (this.e.get()) {
        }
    }

    public void onAdVideoComplete(IXAdContainer iXAdContainer, Boolean bool, HashMap<String, Object> hashMap) {
        if (this.e.get()) {
        }
    }

    public void onAdCustomEvent(String str, IXAdContainer iXAdContainer, Boolean bool, HashMap<String, Object> hashMap) {
        if (this.e.get()) {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.mobads.openad.e.a.a(com.baidu.mobads.openad.e.d, java.lang.Boolean):void
     arg types: [com.baidu.mobads.openad.e.d, int]
     candidates:
      com.baidu.mobads.openad.e.a.a(com.baidu.mobads.openad.e.a, java.net.HttpURLConnection):java.net.HttpURLConnection
      com.baidu.mobads.openad.e.a.a(com.baidu.mobads.openad.e.d, double):void
      com.baidu.mobads.openad.e.a.a(com.baidu.mobads.openad.e.d, java.lang.Boolean):void */
    private void a(Set<String> set) {
        a aVar = new a();
        for (String dVar : set) {
            com.baidu.mobads.openad.e.d dVar2 = new com.baidu.mobads.openad.e.d(dVar, "");
            dVar2.e = 1;
            aVar.a(dVar2, (Boolean) true);
        }
    }
}
