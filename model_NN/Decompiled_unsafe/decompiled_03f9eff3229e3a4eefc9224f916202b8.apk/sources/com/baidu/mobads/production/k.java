package com.baidu.mobads.production;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.baidu.mobads.interfaces.IXAdInstanceInfo;

class k extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ IXAdInstanceInfo f393a;
    final /* synthetic */ a b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    k(a aVar, Looper looper, IXAdInstanceInfo iXAdInstanceInfo) {
        super(looper);
        this.b = aVar;
        this.f393a = iXAdInstanceInfo;
    }

    public void handleMessage(Message message) {
        if (message.getData().getBoolean("caching_result")) {
            this.f393a.setLocalCreativeURL(message.getData().getString("local_creative_url"));
        } else {
            this.f393a.setLocalCreativeURL(null);
        }
    }
}
