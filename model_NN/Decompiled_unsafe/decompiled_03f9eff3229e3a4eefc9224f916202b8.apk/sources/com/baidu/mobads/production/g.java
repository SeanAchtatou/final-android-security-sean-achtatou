package com.baidu.mobads.production;

import com.baidu.mobads.interfaces.download.activate.IXMonitorActivation;

class g implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ f f384a;

    g(f fVar) {
        this.f384a = fVar;
    }

    public void run() {
        try {
            if (a.f369a != null) {
                IXMonitorActivation xMonitorActivation = a.f369a.getXMonitorActivation(this.f384a.f379a, this.f384a.b.s);
                xMonitorActivation.setIXActivateListener(new h(this));
                xMonitorActivation.startMonitor();
            }
        } catch (Exception e) {
            this.f384a.b.s.e(e);
        }
    }
}
