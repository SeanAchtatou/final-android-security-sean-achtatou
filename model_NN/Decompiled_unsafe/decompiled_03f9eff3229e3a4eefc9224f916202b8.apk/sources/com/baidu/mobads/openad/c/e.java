package com.baidu.mobads.openad.c;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.baidu.mobads.command.a;
import com.baidu.mobads.j.m;
import com.baidu.mobads.openad.interfaces.download.IOAdDownloader;
import com.baidu.mobads.openad.interfaces.event.IOAdEvent;
import com.baidu.mobads.openad.interfaces.event.IOAdEventListener;
import java.util.ArrayList;

class e implements IOAdEventListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ d f354a;

    e(d dVar) {
        this.f354a = dVar;
    }

    public void run(IOAdEvent iOAdEvent) {
        a a2;
        try {
            m.a().f().d("OAdDownloadManager", "网络状态已经改变");
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) this.f354a.f353a.getSystemService("connectivity")).getActiveNetworkInfo();
            if (activeNetworkInfo == null || !activeNetworkInfo.isConnected()) {
                m.a().f().d("OAdDownloadManager", "没有可用网络");
                return;
            }
            String typeName = activeNetworkInfo.getTypeName();
            int type = activeNetworkInfo.getType();
            m.a().f().d("OAdDownloadManager", "当前网络名称：" + typeName + "; 网络类型：" + type);
            ArrayList<IOAdDownloader> allAdsApkDownloaderes = this.f354a.getAllAdsApkDownloaderes();
            if (allAdsApkDownloaderes != null) {
                for (IOAdDownloader next : allAdsApkDownloaderes) {
                    if (type == 1) {
                        if (next.getState() == IOAdDownloader.DownloadStatus.ERROR || next.getState() == IOAdDownloader.DownloadStatus.PAUSED) {
                            try {
                                next.resume();
                            } catch (Exception e) {
                                m.a().f().d("OAdDownloadManager", e);
                            }
                        }
                    } else if (type == 0) {
                        m.a().f().d("OAdDownloadManager", "mobile net work");
                        b a3 = b.a(next.getPackageName());
                        if (!(a3 == null || (a2 = a3.a()) == null)) {
                            if (!a2.r) {
                                try {
                                    next.pause();
                                } catch (Exception e2) {
                                    m.a().f().d("OAdDownloadManager", e2);
                                }
                            } else if (next.getState() == IOAdDownloader.DownloadStatus.ERROR || next.getState() == IOAdDownloader.DownloadStatus.PAUSED) {
                                try {
                                    next.resume();
                                } catch (Exception e3) {
                                    m.a().f().d("OAdDownloadManager", e3);
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e4) {
            m.a().f().d("OAdDownloadManager", e4);
            com.baidu.mobads.c.a.a().a("create apk downloader failed: " + e4.toString());
        }
    }
}
