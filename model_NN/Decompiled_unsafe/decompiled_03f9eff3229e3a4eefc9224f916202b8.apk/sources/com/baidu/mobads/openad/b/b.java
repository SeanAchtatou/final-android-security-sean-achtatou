package com.baidu.mobads.openad.b;

import android.content.Context;
import android.os.Build;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import com.baidu.mobads.openad.e.a;

public class b implements a {

    /* renamed from: a  reason: collision with root package name */
    public static Context f347a;

    public static void a(Context context) {
        if (f347a == null) {
            f347a = context;
            CookieSyncManager.createInstance(f347a);
            if (Build.VERSION.SDK_INT < 21) {
                new Thread(new c()).start();
            }
        }
    }

    public b() {
        Class.forName("android.webkit.CookieManager", true, a.class.getClassLoader());
    }

    public void a(String str, String str2) {
        CookieManager.getInstance().setCookie(str, str2);
        CookieSyncManager.getInstance().sync();
    }

    public void a() {
        CookieManager.getInstance().removeExpiredCookie();
    }

    public String a(String str) {
        return CookieManager.getInstance().getCookie(str);
    }
}
