package com.baidu.mobads.openad.c;

import com.baidu.mobads.j.m;
import com.baidu.mobads.openad.interfaces.download.IOAdDownloader;

class c implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ b f352a;

    c(b bVar) {
        this.f352a = bVar;
    }

    public void run() {
        try {
            if (this.f352a.c.g == IOAdDownloader.DownloadStatus.CANCELLED) {
                b.f351a.cancel(this.f352a.c.f);
                return;
            }
            b.f351a.notify(this.f352a.c.f, this.f352a.d());
            if (this.f352a.c.g == IOAdDownloader.DownloadStatus.ERROR) {
                m.a().f().d("OAdApkDownloaderObserver", "status >> error");
            } else if (this.f352a.c.g == IOAdDownloader.DownloadStatus.INITING && this.f352a.c.q == 1) {
                this.f352a.d("开始下载 " + this.f352a.c.f277a);
            }
        } catch (Exception e) {
            m.a().f().d("OAdApkDownloaderObserver", e);
        }
    }
}
