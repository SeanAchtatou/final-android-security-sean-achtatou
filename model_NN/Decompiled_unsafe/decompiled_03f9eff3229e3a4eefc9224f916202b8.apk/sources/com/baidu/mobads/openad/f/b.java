package com.baidu.mobads.openad.f;

import java.util.TimerTask;

class b extends TimerTask {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f366a;

    b(a aVar) {
        this.f366a = aVar;
    }

    public void run() {
        if (this.f366a.h.get() == 0) {
            if (this.f366a.b != null) {
                int unused = this.f366a.f = this.f366a.d - this.f366a.e;
                this.f366a.b.onTimer(this.f366a.f);
            }
            if (this.f366a.e > 0) {
                a.f(this.f366a);
                return;
            }
            this.f366a.stop();
            if (this.f366a.b != null) {
                this.f366a.b.onTimerComplete();
            }
        }
    }
}
