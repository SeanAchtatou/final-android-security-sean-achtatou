package com.baidu.mobads.openad.e;

import android.net.Uri;
import com.baidu.mobads.j.m;

public class d {

    /* renamed from: a  reason: collision with root package name */
    public String f364a;
    public String b;
    public long c = 0;
    public String d = "text/plain";
    public int e = 1;
    private Uri.Builder f;

    public d(String str, String str2) {
        this.f364a = str;
        this.b = str2;
    }

    public String a() {
        return m.a().i().getFixedString(this.f364a);
    }

    public Uri.Builder b() {
        return this.f;
    }

    public void a(Uri.Builder builder) {
        this.f = builder;
    }

    public void a(int i) {
        this.e = i;
    }
}
