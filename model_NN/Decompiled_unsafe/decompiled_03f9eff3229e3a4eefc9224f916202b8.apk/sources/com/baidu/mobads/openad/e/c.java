package com.baidu.mobads.openad.e;

import com.baidu.mobads.j.j;
import java.net.HttpURLConnection;

class c implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f363a;

    c(a aVar) {
        this.f363a = aVar;
    }

    public void run() {
        try {
            if (this.f363a.g != null && this.f363a.e.getAndSet(false)) {
                this.f363a.g.disconnect();
                HttpURLConnection unused = this.f363a.g = (HttpURLConnection) null;
            }
        } catch (Exception e) {
            j.a().e(e);
        }
    }
}
