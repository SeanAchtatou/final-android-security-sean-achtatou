package com.baidu.mobads.vo;

import android.os.Parcel;
import android.os.Parcelable;

final class a implements Parcelable.Creator<XAdInstanceInfo> {
    a() {
    }

    /* renamed from: a */
    public XAdInstanceInfo createFromParcel(Parcel parcel) {
        return new XAdInstanceInfo(parcel, null);
    }

    /* renamed from: a */
    public XAdInstanceInfo[] newArray(int i) {
        return new XAdInstanceInfo[i];
    }
}
