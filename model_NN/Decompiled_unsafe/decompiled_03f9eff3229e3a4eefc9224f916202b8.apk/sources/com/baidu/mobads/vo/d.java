package com.baidu.mobads.vo;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.os.Build;
import android.util.DisplayMetrics;
import cn.banshenggua.aichang.utils.StringUtil;
import com.baidu.mobads.a.a;
import com.baidu.mobads.interfaces.IXAdConstants4PDK;
import com.baidu.mobads.interfaces.IXAdProdInfo;
import com.baidu.mobads.interfaces.IXAdRequestInfo;
import com.baidu.mobads.interfaces.utils.IXAdConstants;
import com.baidu.mobads.interfaces.utils.IXAdSystemUtils;
import com.baidu.mobads.j.m;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;

public abstract class d implements IXAdRequestInfo {

    /* renamed from: a  reason: collision with root package name */
    private String f412a = "android";
    protected String b = "";
    protected String c = "TODO";
    protected Context d;
    protected Activity e;
    protected IXAdConstants4PDK.SlotType f;
    protected IXAdProdInfo g;
    protected IXAdConstants h = m.a().p();
    private String i = "";
    private int j;
    private int k;
    private int l = m.a().p().getAdCreativeTypeImage();
    private String m = "LP,DL";
    private String n = "";
    private int o;
    private int p = 0;
    private int q;
    private String r = "";
    private String s = "";
    private String t = "";
    private boolean u = true;
    private long v = System.currentTimeMillis();

    /* access modifiers changed from: protected */
    public abstract HashMap<String, String> a();

    public d(Context context, Activity activity, IXAdConstants4PDK.SlotType slotType) {
        Activity activity2 = context instanceof Activity ? (Activity) context : null;
        this.e = activity2;
        this.d = activity2 != null ? activity2.getApplicationContext() : context;
        if (this.e == null && activity != null) {
            this.e = activity;
        }
        this.f = slotType;
        this.g = new b(this, this.f);
        c(this.f.getValue());
    }

    public IXAdProdInfo d() {
        return this.g;
    }

    /* access modifiers changed from: protected */
    @TargetApi(4)
    public HashMap<String, String> e() {
        String str;
        String str2;
        String str3;
        IXAdSystemUtils n2 = m.a().n();
        com.baidu.mobads.j.d m2 = m.a().m();
        HashMap<String, String> hashMap = new HashMap<>();
        try {
            hashMap.put("net", "" + n2.getNetworkCatagory(this.d));
            hashMap.put("u", "default");
            hashMap.put("ie", "1");
            hashMap.put(IXAdRequestInfo.AD_COUNT, "" + getN());
            hashMap.put(IXAdRequestInfo.MAX_TITLE_LENGTH, "512");
            hashMap.put(IXAdRequestInfo.MAX_CONTENT_LENGTH, "512");
            hashMap.put(IXAdRequestInfo.TEST_MODE, "1");
            hashMap.put(IXAdRequestInfo.AD_TYPE, "" + getAt());
            hashMap.put(IXAdRequestInfo.V, f() + "_" + a.c + "_" + "4.1.30");
            hashMap.put(IXAdRequestInfo.CS, "");
            hashMap.put(IXAdRequestInfo.PACKAGE, m2.getAppPackage(this.d));
            hashMap.put(IXAdRequestInfo.SDK_VALID, "sdk_8.25");
            String appId = m2.getAppId(this.d);
            hashMap.put("q", appId + "_cpr");
            hashMap.put("appid", appId);
            hashMap.put(IXAdRequestInfo.PHONE_TYPE, Build.MODEL);
            hashMap.put(IXAdRequestInfo.BRAND, n2.getPhoneOSBrand());
            DisplayMetrics displayMetrics = m2.getDisplayMetrics(this.d);
            hashMap.put(IXAdRequestInfo.DENSITY, "" + displayMetrics.density);
            hashMap.put(IXAdRequestInfo.WIDTH, "" + getW());
            hashMap.put(IXAdRequestInfo.HEIGHT, "" + getH());
            Rect screenRect = m2.getScreenRect(this.d);
            hashMap.put(IXAdRequestInfo.SCREEN_WIDTH, "" + screenRect.width());
            hashMap.put(IXAdRequestInfo.SCREEN_HEIGHT, "" + screenRect.height());
            hashMap.put(IXAdRequestInfo.QUERY_WIDTH, String.valueOf(Math.round(((float) getW()) / displayMetrics.density)));
            hashMap.put(IXAdRequestInfo.QUERY_HEIGHT, String.valueOf(Math.round(((float) getH()) / displayMetrics.density)));
            hashMap.put(IXAdRequestInfo.SN, n2.getSn(this.d));
            String str4 = "";
            try {
                List<String[]> cell = n2.getCell(this.d);
                if (cell.size() > 0) {
                    StringBuilder sb = new StringBuilder();
                    for (int i2 = 0; i2 < cell.size(); i2++) {
                        String[] strArr = cell.get(i2);
                        sb.append(String.format("%s_%s_%s|", strArr[0], strArr[1], strArr[2]));
                    }
                    str4 = sb.substring(0, sb.length() - 1);
                }
            } catch (Exception e2) {
                str4 = "";
            }
            hashMap.put(IXAdRequestInfo.CELL_ID, str4);
            hashMap.put(IXAdRequestInfo.NETWORK_OPERATOR, n2.getNetworkOperator(this.d));
            hashMap.put(IXAdRequestInfo.IMSI, m2.getSubscriberId(this.d));
            try {
                double[] gps = n2.getGPS(this.d);
                String str5 = "";
                if (gps != null) {
                    System.currentTimeMillis();
                    str5 = String.format("%s_%s_%s", Double.valueOf(gps[0]), Double.valueOf(gps[1]), Double.valueOf(gps[2]));
                }
                str = str5;
            } catch (Exception e3) {
                str = "";
            }
            hashMap.put(IXAdRequestInfo.GPS, str);
            try {
                List<String[]> wifi = n2.getWIFI(this.d);
                if (wifi.size() > 0) {
                    StringBuilder sb2 = new StringBuilder();
                    for (int i3 = 0; i3 < wifi.size(); i3++) {
                        String[] strArr2 = wifi.get(i3);
                        sb2.append(String.format("%s_%s|", strArr2[0], strArr2[1]));
                    }
                    str2 = sb2.substring(0, sb2.length() - 1);
                } else {
                    str2 = "";
                }
            } catch (Exception e4) {
                str2 = "";
            }
            hashMap.put(IXAdRequestInfo.WIFI, str2);
            hashMap.put("swi", "" + (IXAdSystemUtils.NT_WIFI.equals(n2.getNetworkType(this.d)) ? 1 : 0));
            hashMap.put("tel", "");
            try {
                hashMap.put("uk", URLEncoder.encode(getUk(), StringUtil.Encoding));
                hashMap.put("sex", URLEncoder.encode(getSex(), StringUtil.Encoding));
                hashMap.put("zip", URLEncoder.encode(getZip(), StringUtil.Encoding));
            } catch (Exception e5) {
            }
            hashMap.put("tab", n2.isTablet(this.d) ? "1" : "0");
            hashMap.put("sdc", n2.getAppSDC() + "," + n2.getMem());
            hashMap.put("act", getAct());
            hashMap.put("prod", getProd());
            hashMap.put("os", "android");
            hashMap.put("osv", Build.VERSION.RELEASE);
            hashMap.put(IXAdRequestInfo.BDR, "" + Build.VERSION.SDK_INT);
            hashMap.put("apinfo", m2.getBaiduMapsInfo(this.d));
            hashMap.put("apid", getApid());
            hashMap.put("chid", m2.getChannelId());
            hashMap.put("apt", "0");
            hashMap.put("ap", "" + getAp());
            hashMap.put("nt", n2.getNetType(this.d));
            hashMap.put("udid", "");
            hashMap.put("ses", "" + getSes());
            hashMap.put("android_id", n2.getAndroidId(this.d));
            hashMap.put("imei", n2.getIMEI(this.d));
            hashMap.put("mac", n2.getMacAddress(this.d));
            hashMap.put("cuid", n2.getCUID(this.d));
            hashMap.put(IXAdRequestInfo.P_VER, "8.25");
            hashMap.put("req_id", m2.createRequestId(this.d, getApid()));
            if (n2.isWifiConnected(this.d).booleanValue()) {
                str3 = n2.getWifiConnected(this.d);
            } else {
                str3 = "";
            }
            hashMap.put("cssid", str3);
        } catch (Exception e6) {
        }
        return hashMap;
    }

    public String b() {
        HashMap<String, String> e2 = e();
        e2.putAll(a());
        return m.a().i().getRequestAdUrl(this.b, e2);
    }

    public boolean isCanClick() {
        return this.u;
    }

    public void a(boolean z) {
        this.u = z;
    }

    public String f() {
        return this.f412a;
    }

    public void a(String str) {
        this.f412a = str;
    }

    public int getW() {
        return this.j;
    }

    public void a(int i2) {
        this.j = i2;
    }

    public int getH() {
        return this.k;
    }

    public void b(int i2) {
        this.k = i2;
    }

    public String getAct() {
        return this.m;
    }

    public void b(String str) {
        this.m = str;
    }

    public String getProd() {
        return this.n;
    }

    public void c(String str) {
        this.n = str;
    }

    public int getApt() {
        return this.o;
    }

    public void c(int i2) {
        this.o = i2;
    }

    public int getN() {
        return this.q;
    }

    public void d(int i2) {
        this.q = i2;
    }

    public String getUk() {
        return this.r;
    }

    public String getSex() {
        return this.s;
    }

    public String getZip() {
        return this.t;
    }

    public long getSes() {
        return this.v;
    }

    public int getAp() {
        return this.p;
    }

    public void e(int i2) {
        this.p = i2;
    }

    public String getApid() {
        return this.i;
    }

    public void d(String str) {
        this.i = str;
    }

    public int getAt() {
        return this.l;
    }

    public void f(int i2) {
        this.l = i2;
    }
}
