package com.baidu.mobads;

import com.baidu.mobads.interfaces.event.IXAdEvent;
import com.baidu.mobads.j.m;
import com.baidu.mobads.openad.interfaces.event.IOAdEvent;

class ai implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ IOAdEvent f254a;
    final /* synthetic */ ah b;

    ai(ah ahVar, IOAdEvent iOAdEvent) {
        this.b = ahVar;
        this.f254a = iOAdEvent;
    }

    public void run() {
        if (IXAdEvent.AD_LOADED.equals(this.f254a.getType())) {
            m.a().f().i(this.f254a);
        } else if (IXAdEvent.AD_STARTED.equals(this.f254a.getType())) {
            this.b.f253a.c.onAdPresent();
        } else if ("AdUserClick".equals(this.f254a.getType())) {
            this.b.f253a.c.onAdClick();
        } else if (IXAdEvent.AD_STOPPED.equals(this.f254a.getType())) {
            this.b.f253a.f238a.removeAllListeners();
            this.b.f253a.c.onAdDismissed();
        } else if (IXAdEvent.AD_ERROR.equals(this.f254a.getType())) {
            this.b.f253a.f238a.removeAllListeners();
            this.b.f253a.c.onAdFailed(m.a().q().getMessage(this.f254a.getData()));
        }
    }
}
