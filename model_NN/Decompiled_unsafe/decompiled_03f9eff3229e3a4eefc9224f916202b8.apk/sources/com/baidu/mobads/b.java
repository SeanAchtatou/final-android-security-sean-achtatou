package com.baidu.mobads;

import android.view.View;
import com.baidu.mobads.interfaces.event.IXAdEvent;
import com.baidu.mobads.j.m;
import com.baidu.mobads.openad.interfaces.event.IOAdEvent;
import org.json.JSONObject;

class b implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ IOAdEvent f270a;
    final /* synthetic */ a b;

    b(a aVar, IOAdEvent iOAdEvent) {
        this.b = aVar;
        this.f270a = iOAdEvent;
    }

    public void run() {
        if (IXAdEvent.AD_LOADED.equals(this.f270a.getType())) {
            this.b.f244a.d.onAdReady(this.b.f244a);
        } else if (IXAdEvent.AD_STARTED.equals(this.f270a.getType())) {
            this.b.f244a.d.onAdSwitch();
            this.b.f244a.d.onAdShow(new JSONObject());
        } else if (IXAdEvent.AD_ERROR.equals(this.f270a.getType())) {
            this.b.f244a.d.onAdFailed(m.a().q().getMessage(this.f270a.getData()));
        } else if ("AdUserClick".equals(this.f270a.getType())) {
            this.b.f244a.d.onAdClick(new JSONObject());
        } else if (IXAdEvent.AD_USER_CLOSE.equals(this.f270a.getType())) {
            m.a().m().a((View) this.b.f244a);
            this.b.f244a.d.onAdClose(new JSONObject());
        }
    }
}
