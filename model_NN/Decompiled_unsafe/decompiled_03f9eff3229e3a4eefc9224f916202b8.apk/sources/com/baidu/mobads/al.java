package com.baidu.mobads;

import com.baidu.mobads.interfaces.event.IXAdEvent;
import com.baidu.mobads.openad.d.b;
import com.baidu.mobads.openad.interfaces.event.IOAdEvent;

class al implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ IOAdEvent f257a;
    final /* synthetic */ ak b;

    al(ak akVar, IOAdEvent iOAdEvent) {
        this.b = akVar;
        this.f257a = iOAdEvent;
    }

    public void run() {
        if (IXAdEvent.AD_LOADED.equals(this.f257a.getType())) {
            this.b.f256a.c.onVideoPrepared();
        }
        if (IXAdEvent.AD_STARTED.equals(this.f257a.getType())) {
            this.b.f256a.c.onVideoStart();
        }
        if (IXAdEvent.AD_CLICK_THRU.equals(this.f257a.getType())) {
            this.b.f256a.c.onVideoClickAd();
        }
        if (IXAdEvent.AD_STOPPED.equals(this.f257a.getType())) {
            this.b.f256a.c.onVideoFinish();
        }
        if (IXAdEvent.AD_ERROR.equals(this.f257a.getType())) {
            String str = (String) this.f257a.getData().get(b.EVENT_MESSAGE);
            this.b.f256a.c.onVideoError();
        }
    }
}
