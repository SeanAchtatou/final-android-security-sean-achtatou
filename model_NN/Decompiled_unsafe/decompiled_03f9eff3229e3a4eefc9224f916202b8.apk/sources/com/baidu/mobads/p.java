package com.baidu.mobads;

import android.view.ViewTreeObserver;

class p implements ViewTreeObserver.OnPreDrawListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppActivity f368a;

    p(AppActivity appActivity) {
        this.f368a = appActivity;
    }

    public boolean onPreDraw() {
        this.f368a.d.getViewTreeObserver().removeOnPreDrawListener(this);
        this.f368a.a(this.f368a.d);
        return true;
    }
}
