package com.baidu.mobads;

import java.util.Calendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AdSettings {

    /* renamed from: a  reason: collision with root package name */
    private static HashSet<String> f225a = new HashSet<>();
    private static JSONArray b = new JSONArray();
    private static String c;
    private static String d;
    private static String e;
    private static String f;
    private static String g;
    private static String h;
    private static String i;
    private static HashSet<String> j = new HashSet<>();
    private static JSONArray k = new JSONArray();
    private static JSONObject l = new JSONObject();

    public enum c {
        MALE(0),
        FEMALE(1);
        
        private int c;

        private c(int i) {
            this.c = i;
        }

        public int a() {
            return this.c;
        }
    }

    public enum a {
        PRIMARY(0),
        JUNIOR(1),
        SENIOR(2),
        SPECIALTY(3),
        BACHELOR(4),
        MASTER(5),
        DOCTOR(6);
        
        private int h;

        private a(int i2) {
            this.h = i2;
        }

        public int a() {
            return this.h;
        }
    }

    public enum b {
        F0T1k(0),
        F1kT2k(1),
        F2kT3k(2),
        F3kT4k(3),
        F4kT5k(4),
        F5kT6k(5),
        F6kT7k(6),
        F7kT8k(7),
        F8kT9k(8),
        F9kT10k(9),
        F10kT15k(10),
        F15kT20k(11),
        F20(12);
        
        private int n;

        private b(int i) {
            this.n = i;
        }

        public int a() {
            return this.n;
        }
    }

    protected static JSONObject getAttr() {
        JSONObject jSONObject = new JSONObject();
        Iterator<String> it = f225a.iterator();
        b = new JSONArray();
        while (it.hasNext()) {
            b.put(it.next());
        }
        Iterator<String> it2 = j.iterator();
        k = new JSONArray();
        while (it2.hasNext()) {
            k.put(it2.next());
        }
        try {
            jSONObject.putOpt("KEY", b);
            jSONObject.putOpt("SEX", c);
            jSONObject.putOpt("BIR", d);
            jSONObject.putOpt("CITY", e);
            jSONObject.putOpt("ZIP", f);
            jSONObject.putOpt("JOB", g);
            jSONObject.putOpt("EDU", h);
            jSONObject.putOpt("SAL", i);
            jSONObject.putOpt("HOB", k);
            jSONObject.putOpt("R", l);
        } catch (Exception e2) {
        }
        return jSONObject;
    }

    public static void setKey(String[] strArr) {
        for (String add : strArr) {
            f225a.add(add);
        }
    }

    public static void setKey(List<String> list) {
        f225a.addAll(list);
    }

    public static void setSex(c cVar) {
        if (cVar != null) {
            c = cVar.a() + "";
        }
    }

    public static void setBirthday(Calendar calendar) {
        if (calendar != null) {
            int i2 = calendar.get(1);
            int i3 = calendar.get(2) + 1;
            int i4 = calendar.get(5);
            d = i2 + "";
            if (i3 <= 0 || i3 >= 10) {
                d += i3;
            } else {
                d += "0" + i3;
            }
            if (i4 <= 0 || i4 >= 10) {
                d += i4;
            } else {
                d += "0" + i4;
            }
        }
    }

    public static void setCity(String str) {
        e = str;
    }

    public static void setZip(String str) {
        f = str;
    }

    public static void setJob(String str) {
        g = str;
    }

    public static void setEducation(a aVar) {
        if (aVar != null) {
            h = aVar.a() + "";
        }
    }

    public static void setSalary(b bVar) {
        if (bVar != null) {
            i = bVar.a() + "";
        }
    }

    public static void setHob(String[] strArr) {
        for (String add : strArr) {
            j.add(add);
        }
    }

    public static void setHob(List<String> list) {
        j.addAll(list);
    }

    public static void setUserAttr(String str, String str2) {
        try {
            l.put(str, str2);
        } catch (JSONException e2) {
        }
    }
}
