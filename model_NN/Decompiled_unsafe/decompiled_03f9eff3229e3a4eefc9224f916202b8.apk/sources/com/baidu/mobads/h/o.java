package com.baidu.mobads.h;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import com.baidu.mobads.j.m;
import com.baidu.mobads.openad.interfaces.download.IOAdDownloader;
import java.net.URL;
import java.util.Observable;
import java.util.Observer;

public class o implements Observer {

    /* renamed from: a  reason: collision with root package name */
    private Context f307a;
    private URL b = null;
    private String c = null;
    private final e d;
    private a e;
    private SharedPreferences f;
    private SharedPreferences.OnSharedPreferenceChangeListener g = new p(this);

    public interface a {
        void a(e eVar);

        void b(e eVar);
    }

    public o(Context context, URL url, e eVar, a aVar) {
        this.b = url;
        this.d = eVar;
        a(context, aVar);
    }

    public o(Context context, String str, e eVar, a aVar) {
        this.c = str;
        this.d = eVar;
        a(context, aVar);
    }

    private void a(Context context, a aVar) {
        this.f307a = context;
        this.e = aVar;
        this.f = this.f307a.getSharedPreferences("__xadsdk_downloaded__version__", 0);
        this.f.registerOnSharedPreferenceChangeListener(this.g);
    }

    public void a(String str, String str2) {
        IOAdDownloader createSimpleFileDownloader = m.a().b(this.f307a).createSimpleFileDownloader(this.c != null ? new URL(this.c) : this.b, str, str2, false);
        createSimpleFileDownloader.addObserver(this);
        createSimpleFileDownloader.start();
        SharedPreferences.Editor edit = this.f.edit();
        edit.putString("version", this.d.toString());
        if (Build.VERSION.SDK_INT >= 9) {
            edit.apply();
        } else {
            edit.commit();
        }
    }

    public void update(Observable observable, Object obj) {
        IOAdDownloader iOAdDownloader = (IOAdDownloader) observable;
        if (iOAdDownloader.getState() == IOAdDownloader.DownloadStatus.COMPLETED) {
            this.e.a(new e(this.d, iOAdDownloader.getOutputPath(), true));
        }
        if (iOAdDownloader.getState() == IOAdDownloader.DownloadStatus.ERROR) {
            this.e.b(new e(this.d, iOAdDownloader.getOutputPath(), false));
        }
    }
}
