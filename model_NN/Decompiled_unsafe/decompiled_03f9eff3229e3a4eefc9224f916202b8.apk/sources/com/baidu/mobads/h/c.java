package com.baidu.mobads.h;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import com.baidu.mobads.h.g;
import com.baidu.mobads.h.o;
import com.baidu.mobads.interfaces.utils.IXAdLogger;
import com.baidu.mobads.j.m;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.UUID;

public class c extends Thread {
    /* access modifiers changed from: private */
    public static volatile c f;

    /* renamed from: a  reason: collision with root package name */
    o.a f297a = new d(this);
    private volatile String b;
    private String c = null;
    private double d;
    private Handler e;
    private final Context g;
    private o h = null;
    private final e i;
    private IXAdLogger j = m.a().f();

    public static c a(Context context, e eVar, String str, Handler handler) {
        if (f == null) {
            f = new c(context, eVar, str, handler);
        }
        return f;
    }

    private c(Context context, e eVar, String str, Handler handler) {
        this.g = context;
        this.i = eVar;
        a(eVar.c());
        this.e = handler;
        this.c = str;
    }

    public void a(String str) {
        this.b = str;
        interrupt();
    }

    public void run() {
        try {
            if (b()) {
                a();
                this.j.d("XAdApkDownloadThread", "download apk successfully, downloader exit");
                f = null;
                this.j.d("XAdApkDownloadThread", "no newer apk, downloader exit");
                f = null;
            }
        } catch (IOException e2) {
            this.j.e("XAdApkDownloadThread", "create File or HTTP Get failed, exception: " + e2.getMessage());
        } catch (Throwable th) {
        }
    }

    /* access modifiers changed from: private */
    public void a(String str, e eVar, String str2) {
        if (str.equals("OK") || str.equals("ERROR")) {
            Message obtainMessage = this.e.obtainMessage();
            Bundle bundle = new Bundle();
            bundle.putParcelable("APK_INFO", eVar);
            bundle.putString("CODE", str);
            obtainMessage.setData(bundle);
            this.e.sendMessage(obtainMessage);
        }
    }

    private String a() {
        String str = "__xadsdk__remote__final__" + UUID.randomUUID().toString() + ".jar";
        String str2 = this.c + str;
        File file = new File(str2);
        try {
            file.createNewFile();
            this.h.a(this.c, str);
            return str2;
        } catch (IOException e2) {
            file.delete();
            throw e2;
        }
    }

    private boolean b() {
        double d2;
        try {
            this.h = new o(this.g, new URL(this.b), this.i, this.f297a);
        } catch (MalformedURLException e2) {
            try {
                this.h = new o(this.g, this.b, this.i, this.f297a);
            } catch (Exception e3) {
                String str = "parse apk failed, error:" + e3.toString();
                this.j.e("XAdApkDownloadThread", str);
                throw new g.a(str);
            }
        }
        if (g.c != null) {
            d2 = g.c.f294a;
        } else if (g.b == null) {
            d2 = 0.0d;
        } else if (g.b.f294a > 0.0d) {
            d2 = g.b.f294a;
        } else {
            d2 = g.b.f294a;
        }
        this.j.d("XAdApkDownloadThread", "isNewApkAvailable: local apk version is: " + d2 + ", remote apk version: " + this.i.b());
        if (d2 > 0.0d) {
            if (this.i.b() > 0.0d) {
                this.j.e("XAdApkDownloadThread", "remote not null, local apk version is null, force upgrade");
                this.d = this.i.b();
                return true;
            }
            this.j.e("XAdApkDownloadThread", "remote is null, local apk version is null, do not upgrade");
            return false;
        } else if (this.i.b() <= 0.0d) {
            this.j.e("XAdApkDownloadThread", "remote apk version is: null, local apk version is: " + d2 + ", do not upgrade");
            return false;
        } else if (this.i.b() <= d2) {
            return false;
        } else {
            this.d = this.i.b();
            return true;
        }
    }
}
