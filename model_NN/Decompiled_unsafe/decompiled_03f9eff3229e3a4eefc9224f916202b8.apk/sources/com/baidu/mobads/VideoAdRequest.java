package com.baidu.mobads;

import com.baidu.mobads.VideoAdView;

public class VideoAdRequest {

    /* renamed from: a  reason: collision with root package name */
    private VideoAdView.VideoDuration f239a;
    private boolean b;
    private VideoAdView.VideoSize c;

    private VideoAdRequest(Builder builder) {
        this.f239a = builder.f240a;
        this.b = builder.b;
        this.c = builder.c;
    }

    /* access modifiers changed from: protected */
    public int getVideoDuration() {
        if (this.f239a == null) {
            return VideoAdView.VideoDuration.DURATION_15_SECONDS.getValue();
        }
        return this.f239a.getValue();
    }

    /* access modifiers changed from: protected */
    public boolean isShowCountdown() {
        return this.b;
    }

    /* access modifiers changed from: protected */
    public int getVideoWidth() {
        if (this.c == null) {
            this.c = VideoAdView.VideoSize.SIZE_16x9;
        }
        return this.c.getWidth();
    }

    /* access modifiers changed from: protected */
    public int getVideoHeight() {
        if (this.c == null) {
            this.c = VideoAdView.VideoSize.SIZE_16x9;
        }
        return this.c.getHeight();
    }

    public static class Builder {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public VideoAdView.VideoDuration f240a;
        /* access modifiers changed from: private */
        public boolean b = false;
        /* access modifiers changed from: private */
        public VideoAdView.VideoSize c;

        public Builder setVideoDuration(VideoAdView.VideoDuration videoDuration) {
            this.f240a = videoDuration;
            return this;
        }

        public Builder isShowCountdown(boolean z) {
            this.b = z;
            return this;
        }

        public Builder setVideoSize(VideoAdView.VideoSize videoSize) {
            this.c = videoSize;
            return this;
        }

        public VideoAdRequest build() {
            return new VideoAdRequest(this);
        }
    }
}
