package com.baidu.mobads.c;

import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import com.baidu.mobads.interfaces.IXAdRequestInfo;
import com.baidu.mobads.interfaces.download.activate.IXAppInfo;
import com.baidu.mobads.interfaces.utils.IXAdConstants;
import com.baidu.mobads.interfaces.utils.IXAdLogger;
import com.baidu.mobads.j.d;
import com.baidu.mobads.j.m;
import com.baidu.mobads.vo.a.b;
import com.tencent.connect.common.Constants;
import com.tencent.stat.DeviceInfo;
import java.util.Map;

public class a {
    public static volatile String b = "";
    public static volatile String c = "";
    private static a d = new a();
    private static boolean f = false;

    /* renamed from: a  reason: collision with root package name */
    protected final IXAdLogger f274a = m.a().f();
    private Context e;

    public static a a() {
        return d;
    }

    private a() {
        new Handler(Looper.getMainLooper()).postDelayed(new b(this), 2000);
    }

    public void a(Context context) {
        if (this.e == null) {
            this.e = context;
        }
    }

    public void a(Context context, com.baidu.mobads.command.a aVar) {
        a(context, "9", aVar);
    }

    public void a(com.baidu.mobads.command.a aVar) {
    }

    public void b(Context context, com.baidu.mobads.command.a aVar) {
        a(context, Constants.VIA_REPORT_TYPE_SHARE_TO_QZONE, aVar);
    }

    public void a(Context context, IXAppInfo iXAppInfo) {
        a(context, Constants.VIA_REPORT_TYPE_SHARE_TO_QQ, iXAppInfo);
    }

    public void a(String str) {
        if (TextUtils.isEmpty(str) || !str.contains("temp_for_feed_response_html")) {
            a(str, "400", (Uri.Builder) null);
        } else if (!f) {
            a("temp_for_feed_response_html", "405", b + "___" + c);
            f = true;
        }
    }

    public void a(String str, String str2, String str3) {
        Uri.Builder appendQueryParameter = new Uri.Builder().appendQueryParameter("stacktrace", str2);
        appendQueryParameter.appendQueryParameter("ad", str3);
        a(str, "404", appendQueryParameter);
    }

    private void a(String str, String str2, Uri.Builder builder) {
        IXAdConstants p = m.a().p();
        d m = m.a().m();
        if (builder == null) {
            builder = new Uri.Builder();
        }
        try {
            builder.appendQueryParameter("type", str2).appendQueryParameter(IXAdRequestInfo.P_VER, "8.25").appendQueryParameter("appsid", p.getAppSid()).appendQueryParameter(IXAdRequestInfo.V, "android_" + com.baidu.mobads.a.a.c + "_" + "4.1.30").appendQueryParameter("reason", str).appendQueryParameter("osv", Build.VERSION.RELEASE).appendQueryParameter(IXAdRequestInfo.BDR, "" + Build.VERSION.SDK_INT).appendQueryParameter(IXAdRequestInfo.BRAND, "" + m.getTextEncoder(Build.BRAND)).appendQueryParameter("pack", p.getAppPackageNameOfPublisher());
        } catch (Exception e2) {
            m.a().f().e(e2);
        }
        com.baidu.mobads.openad.e.d dVar = new com.baidu.mobads.openad.e.d("http://mobads-logs.baidu.com/brwhis.log", "");
        dVar.a(builder);
        dVar.a(0);
        new com.baidu.mobads.openad.e.a().a(dVar);
    }

    private void a(Context context, String str, com.baidu.mobads.command.a aVar) {
        IXAppInfo a2 = com.baidu.mobads.command.a.a.a(aVar);
        if (a2 != null) {
            a(context, str, a2);
        }
    }

    private void a(Context context, String str, IXAppInfo iXAppInfo) {
        b bVar = new b(context, iXAppInfo);
        bVar.b = iXAppInfo.getAdId();
        b(a(context, str, bVar.c()));
    }

    private void b(String str) {
        a(1, str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.mobads.openad.e.a.a(com.baidu.mobads.openad.e.d, java.lang.Boolean):void
     arg types: [com.baidu.mobads.openad.e.d, int]
     candidates:
      com.baidu.mobads.openad.e.a.a(com.baidu.mobads.openad.e.a, java.net.HttpURLConnection):java.net.HttpURLConnection
      com.baidu.mobads.openad.e.a.a(com.baidu.mobads.openad.e.d, double):void
      com.baidu.mobads.openad.e.a.a(com.baidu.mobads.openad.e.d, java.lang.Boolean):void */
    private void a(int i, String str) {
        com.baidu.mobads.openad.e.a aVar = new com.baidu.mobads.openad.e.a();
        com.baidu.mobads.openad.e.d dVar = new com.baidu.mobads.openad.e.d(str, "");
        dVar.e = i;
        aVar.a(dVar, (Boolean) true);
    }

    private String a(Context context, String str, Map<String, String> map) {
        try {
            StringBuilder sb = new StringBuilder("type=" + str + "&");
            StringBuilder sb2 = new StringBuilder();
            map.put(DeviceInfo.TAG_TIMESTAMPS, System.currentTimeMillis() + "");
            d m = m.a().m();
            for (String next : map.keySet()) {
                String str2 = map.get(next);
                if (!(next == null || str2 == null)) {
                    String encodeURIComponent = m.encodeURIComponent(next);
                    String encodeURIComponent2 = m.encodeURIComponent(str2);
                    sb.append(encodeURIComponent);
                    sb.append("=");
                    sb.append(encodeURIComponent2);
                    sb.append("&");
                    sb2.append(encodeURIComponent2);
                    sb2.append(",");
                }
            }
            sb2.append("mobads,");
            String md5 = m.getMD5(sb2.toString());
            this.f274a.d("ExtraQuery.allValue:" + ((Object) sb2));
            sb.append("vd=" + md5 + "&");
            this.f274a.d("ExtraQuery.params:" + ((Object) sb));
            return "http://mobads-logs.baidu.com/dz.zb" + "?" + sb.toString();
        } catch (Exception e2) {
            this.f274a.d(e2);
            return "";
        }
    }
}
