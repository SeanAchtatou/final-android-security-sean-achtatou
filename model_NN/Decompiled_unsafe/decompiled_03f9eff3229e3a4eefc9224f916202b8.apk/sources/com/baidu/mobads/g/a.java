package com.baidu.mobads.g;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.v4.view.MotionEventCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.baidu.mobads.AppActivity;
import com.baidu.mobads.interfaces.utils.IXAdCommonUtils;
import com.baidu.mobads.j.m;

public class a extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    protected c f284a;
    Paint b;
    int c;
    int d;
    private Context e;
    /* access modifiers changed from: private */
    public IXAdCommonUtils f;
    private AppActivity.ActionBarColorTheme g;
    private TextView h;

    public interface c {
        void a();

        void b();
    }

    public a(Context context) {
        super(context);
        this.b = new Paint();
        this.c = 0;
        this.d = 0;
        this.e = context;
    }

    public a(Context context, AppActivity.ActionBarColorTheme actionBarColorTheme) {
        this(context);
        this.g = actionBarColorTheme;
        setBackgroundColor(this.g.getBackgroundColor());
        this.f = m.a().m();
        a();
    }

    public void a(String str) {
        if (this.h != null) {
            this.h.setText(str);
            this.h.invalidate();
        }
    }

    public void a(c cVar) {
        this.f284a = cVar;
    }

    /* access modifiers changed from: protected */
    public void a() {
        int pixel = this.f.getPixel(this.e, 52);
        b bVar = new b(this.e, this.g.getCloseColor());
        bVar.setId(132343242);
        addView(bVar, new RelativeLayout.LayoutParams(pixel, -1));
        bVar.setOnClickListener(new b(this));
        d dVar = new d(this.e, this.g.getCloseColor());
        dVar.setId(132343243);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(pixel, -1);
        layoutParams.addRule(11);
        dVar.setOnClickListener(new c(this));
        addView(dVar, layoutParams);
        this.h = new TextView(this.e);
        this.h.setTextSize(1, 16.0f);
        this.h.setLines(1);
        this.h.setEllipsize(TextUtils.TruncateAt.END);
        this.h.setGravity(16);
        this.h.setTextColor(this.g.getTitleColor());
        this.h.setText("");
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(this.f.getScreenRect(this.e).width() - (pixel * 2), -1);
        layoutParams2.addRule(14);
        addView(this.h, layoutParams2);
    }

    /* renamed from: com.baidu.mobads.g.a$a  reason: collision with other inner class name */
    public class C0004a extends View {
        public C0004a(Context context) {
            super(context);
        }
    }

    private class b extends C0004a {
        private Paint c;
        private int d;

        public b(Context context, int i) {
            super(context);
            this.d = i;
        }

        private Paint a() {
            if (this.c == null) {
                this.c = new Paint();
                this.c.setStyle(Paint.Style.STROKE);
                this.c.setColor(this.d);
                this.c.setAlpha(MotionEventCompat.ACTION_MASK);
                this.c.setAntiAlias(true);
                this.c.setStrokeWidth((float) ((int) a.this.f.getScreenDensity(getContext())));
            }
            return this.c;
        }

        /* access modifiers changed from: protected */
        public void onDraw(Canvas canvas) {
            super.onDraw(canvas);
            canvas.drawLine((float) a.this.f.getPixel(getContext(), 18), (float) a.this.f.getPixel(getContext(), 15), (float) a.this.f.getPixel(getContext(), 34), (float) a.this.f.getPixel(getContext(), 31), a());
            canvas.drawLine((float) a.this.f.getPixel(getContext(), 18), (float) a.this.f.getPixel(getContext(), 31), (float) a.this.f.getPixel(getContext(), 34), (float) a.this.f.getPixel(getContext(), 15), a());
        }
    }

    private class d extends C0004a {
        private Paint c;
        private int d;

        public d(Context context, int i) {
            super(context);
            this.d = i;
        }

        private Paint a() {
            if (this.c == null) {
                this.c = new Paint();
                this.c.setColor(this.d);
                this.c.setAlpha(MotionEventCompat.ACTION_MASK);
                this.c.setAntiAlias(true);
            }
            return this.c;
        }

        /* access modifiers changed from: protected */
        public void onDraw(Canvas canvas) {
            super.onDraw(canvas);
            a(canvas, 31);
            a(canvas, 15);
            a(canvas, 23);
        }

        private void a(Canvas canvas, int i) {
            canvas.drawCircle((float) a.this.f.getPixel(getContext(), 26), (float) a.this.f.getPixel(getContext(), i), (float) ((int) (a.this.f.getScreenDensity(getContext()) * 1.0f)), a());
        }
    }

    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.g.equals(AppActivity.ActionBarColorTheme.ACTION_BAR_WHITE_THEME)) {
            this.b.setColor(-5592406);
            this.b.setStyle(Paint.Style.STROKE);
            this.b.setStrokeWidth((float) this.f.getPixel(this.e, 1));
            canvas.drawLine(0.0f, (float) this.d, (float) this.c, (float) this.d, this.b);
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        this.c = i3 - i;
        this.d = i4 - i2;
    }
}
