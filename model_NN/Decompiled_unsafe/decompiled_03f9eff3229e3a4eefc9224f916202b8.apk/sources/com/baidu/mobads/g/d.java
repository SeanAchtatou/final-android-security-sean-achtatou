package com.baidu.mobads.g;

import android.content.Context;
import android.support.v4.view.ViewCompat;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.baidu.mobads.interfaces.utils.IXAdCommonUtils;
import com.baidu.mobads.j.m;

public class d extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    protected b f288a;
    /* access modifiers changed from: private */
    public Context b;
    /* access modifiers changed from: private */
    public IXAdCommonUtils c = m.a().m();

    public interface b {
        void a();

        void b();

        void c();
    }

    public d(Context context) {
        super(context);
        this.b = context;
        setBackgroundColor(-2236963);
        setOrientation(1);
        a aVar = new a(context, "刷新");
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) aVar.getLayoutParams();
        layoutParams.bottomMargin = this.c.getPixel(this.b, 2);
        addView(aVar, layoutParams);
        a aVar2 = new a(context, "复制网址");
        LinearLayout.LayoutParams layoutParams2 = (LinearLayout.LayoutParams) aVar2.getLayoutParams();
        layoutParams2.bottomMargin = this.c.getPixel(this.b, 4);
        addView(aVar2, layoutParams2);
        a aVar3 = new a(context, "取消");
        addView(aVar3);
        aVar.setOnClickListener(new e(this));
        aVar2.setOnClickListener(new f(this));
        aVar3.setOnClickListener(new g(this));
    }

    public void a(b bVar) {
        this.f288a = bVar;
    }

    class a extends TextView {
        public a(Context context, String str) {
            super(context);
            a(str);
        }

        private void a(String str) {
            setText(str);
            setTextColor((int) ViewCompat.MEASURED_STATE_MASK);
            setGravity(17);
            setBackgroundColor(-1);
            setTextSize(19.0f);
            setLayoutParams(new LinearLayout.LayoutParams(-1, d.this.c.getPixel(d.this.b, 50)));
        }
    }
}
