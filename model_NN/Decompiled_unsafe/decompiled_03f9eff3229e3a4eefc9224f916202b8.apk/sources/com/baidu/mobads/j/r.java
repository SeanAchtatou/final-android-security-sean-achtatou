package com.baidu.mobads.j;

import android.annotation.SuppressLint;
import android.net.Uri;
import android.os.Build;
import com.baidu.mobads.interfaces.utils.IXAdURIUitls;
import com.baidu.mobads.openad.b.b;
import com.baidu.mobads.openad.e.a;
import com.baidu.mobads.openad.e.d;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

class r implements IXAdURIUitls {

    /* renamed from: a  reason: collision with root package name */
    private b f339a;

    r() {
    }

    public HashMap<String, String> getAllQueryParameters(String str) {
        HashMap<String, String> hashMap = new HashMap<>();
        Uri parse = Uri.parse(str);
        for (String next : getQueryParameterNames(parse)) {
            if (next != null && next.length() > 0) {
                hashMap.put(next, parse.getQueryParameter(next));
            }
        }
        return hashMap;
    }

    @SuppressLint({"NewApi"})
    public Set<String> getQueryParameterNames(Uri uri) {
        try {
            if (Build.VERSION.SDK_INT >= 11) {
                return uri.getQueryParameterNames();
            }
            String encodedQuery = uri.getEncodedQuery();
            if (encodedQuery == null) {
                return Collections.emptySet();
            }
            LinkedHashSet linkedHashSet = new LinkedHashSet();
            int i = 0;
            while (true) {
                int i2 = i;
                int indexOf = encodedQuery.indexOf(38, i2);
                if (indexOf == -1) {
                    indexOf = encodedQuery.length();
                }
                int indexOf2 = encodedQuery.indexOf(61, i2);
                if (indexOf2 > indexOf || indexOf2 == -1) {
                    indexOf2 = indexOf;
                }
                linkedHashSet.add(Uri.decode(encodedQuery.substring(i2, indexOf2)));
                i = indexOf + 1;
                if (i >= encodedQuery.length()) {
                    return Collections.unmodifiableSet(linkedHashSet);
                }
            }
        } catch (Exception e) {
            return new HashSet();
        }
    }

    public String getRequestAdUrl(String str, HashMap<String, String> hashMap) {
        m a2 = m.a();
        StringBuilder sb = new StringBuilder();
        if (hashMap != null) {
            int i = 0;
            for (String next : hashMap.keySet()) {
                int i2 = i + 1;
                String str2 = hashMap.get(next);
                if (i2 == 1) {
                    sb.append(next).append("=").append(str2);
                } else {
                    sb.append("&").append(next).append("=").append(str2);
                }
                i = i2;
            }
        }
        if (!com.baidu.mobads.a.b.f246a.booleanValue()) {
            return str + "?code2=" + a2.e().encode(sb.toString());
        }
        if (hashMap != null) {
            for (String next2 : hashMap.keySet()) {
                try {
                    String str3 = hashMap.get(next2);
                    if (str3 != null) {
                        hashMap.put(next2, URLEncoder.encode(str3, "UTF-8"));
                    }
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        }
        return a2.i().addParameters(str, hashMap);
    }

    @Deprecated
    public String addParameter(String str, String str2, String str3) {
        String str4;
        String fixedString = getFixedString(str);
        String queryString = getQueryString(str);
        if (!m.a().m().isStringAvailable(queryString)) {
            str4 = str2 + "=" + str3;
        } else {
            str4 = queryString + "&" + str2 + "=" + str3;
        }
        return fixedString + "?" + str4;
    }

    public String addParameters(String str, HashMap<String, String> hashMap) {
        StringBuilder sb = new StringBuilder(str);
        if (hashMap == null || hashMap.isEmpty()) {
            return sb.toString();
        }
        sb.append("?");
        for (Map.Entry next : hashMap.entrySet()) {
            try {
                sb.append((String) next.getKey());
                sb.append("=");
                sb.append((String) next.getValue());
                sb.append("&");
            } catch (Exception e) {
                j.a().e(e);
            }
        }
        String sb2 = sb.toString();
        return sb2.substring(0, sb2.length() - 1);
    }

    public String encodeUrl(String str) {
        try {
            return URLEncoder.encode(str, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public Boolean isHttpProtocol(String str) {
        return a(str, "http:");
    }

    public Boolean isHttpsProtocol(String str) {
        return a(str, "https:");
    }

    public Boolean a(String str) {
        return Boolean.valueOf(a(str, "sms:").booleanValue() || a(str, "smsto:").booleanValue() || a(str, "mms:").booleanValue());
    }

    private Boolean a(String str, String str2) {
        boolean z = false;
        if (str != null && str.trim().toLowerCase(Locale.getDefault()).indexOf(str2) == 0) {
            z = true;
        }
        return Boolean.valueOf(z);
    }

    public String getFileName(String str) {
        try {
            String path = new URI(str).getPath();
            return path.substring(path.lastIndexOf(47) + 1, path.length());
        } catch (URISyntaxException e) {
            return "";
        }
    }

    public HttpURLConnection getHttpURLConnection(URL url) {
        String a2;
        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
        b a3 = a();
        if (!(a3 == null || (a2 = a3.a(url.toString())) == null || a2.length() <= 0)) {
            httpURLConnection.setRequestProperty("Cookie", a2);
        }
        return httpURLConnection;
    }

    private b a() {
        if (this.f339a == null) {
            try {
                this.f339a = new b();
            } catch (ClassNotFoundException e) {
            }
        }
        return this.f339a;
    }

    public String getFixedString(String str) {
        if (str == null) {
            return null;
        }
        return (isHttpProtocol(str).booleanValue() || isHttpsProtocol(str).booleanValue()) ? str.split("\\?")[0] : str;
    }

    public String getQueryString(String str) {
        String[] strArr;
        if (str == null) {
            return null;
        }
        if (isHttpProtocol(str).booleanValue() || isHttpsProtocol(str).booleanValue()) {
            strArr = str.split("\\?");
        } else {
            strArr = null;
        }
        if (strArr == null || strArr.length < 2) {
            return null;
        }
        return strArr[1];
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.mobads.openad.e.a.a(com.baidu.mobads.openad.e.d, java.lang.Boolean):void
     arg types: [com.baidu.mobads.openad.e.d, int]
     candidates:
      com.baidu.mobads.openad.e.a.a(com.baidu.mobads.openad.e.a, java.net.HttpURLConnection):java.net.HttpURLConnection
      com.baidu.mobads.openad.e.a.a(com.baidu.mobads.openad.e.d, double):void
      com.baidu.mobads.openad.e.a.a(com.baidu.mobads.openad.e.d, java.lang.Boolean):void */
    public void pintHttpInNewThread(String str) {
        a aVar = new a();
        d dVar = new d(str, "");
        dVar.e = 1;
        aVar.a(dVar, (Boolean) true);
    }
}
