package com.baidu.mobads.j;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import cn.banshenggua.aichang.room.message.SocketMessage;
import com.baidu.mobads.command.a;
import com.baidu.mobads.interfaces.utils.IXAdCommonUtils;
import com.baidu.mobads.interfaces.utils.IXAdSystemUtils;
import com.sina.weibo.sdk.register.mobile.SelectCountryActivity;
import com.tencent.stat.DeviceInfo;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicLong;
import org.apache.mina.proxy.handlers.http.ntlm.NTLMConstants;
import org.apache.mina.proxy.handlers.socks.SocksProxyConstants;
import org.json.JSONArray;
import org.json.JSONObject;

public class d implements IXAdCommonUtils {

    /* renamed from: a  reason: collision with root package name */
    private static String f327a;
    private static String b;
    private static String c;
    private static String d;
    private final String e = "_cpr";
    private final AtomicLong f = new AtomicLong(1);
    private Method g = null;
    private String h = null;
    private HashMap<String, Object> i = new HashMap<>();

    public int getApkDownloadStatus(Context context, String str, String str2) {
        if (str == null || "".equals(str)) {
            return -1;
        }
        try {
            JSONObject optJSONObject = new JSONObject(context.getSharedPreferences(IXAdCommonUtils.PKGS_PREF_ACTIVATION, 0).getString(IXAdCommonUtils.PKGS_PREF_DOWNLOAD_KEY, "{}")).optJSONObject(str);
            if (optJSONObject != null && optJSONObject.optBoolean("a", false)) {
                return 100;
            }
        } catch (Exception e2) {
        }
        try {
            SharedPreferences sharedPreferences = context.getSharedPreferences(IXAdCommonUtils.PKGS_PREF_DOWNLOAD, 0);
            JSONObject jSONObject = new JSONObject(sharedPreferences.getString(str + "#$#" + a.b(), "{}"));
            if (jSONObject == null) {
                return -1;
            }
            int i2 = jSONObject.getInt(IXAdCommonUtils.PKGS_PREF_DOWNLOAD_STATUS);
            String optString = jSONObject.optString(SelectCountryActivity.EXTRA_COUNTRY_NAME, null);
            if (optString == null) {
                return i2;
            }
            File file = new File(m.a().k().getStoreagePath(context) + optString);
            if (i2 != 3) {
                return i2;
            }
            boolean z = false;
            if (!file.exists() || file.length() == 0) {
                z = true;
            } else {
                long optLong = jSONObject.optLong("contentLength", -1);
                if (optLong != -1 && Math.abs(optLong - file.length()) >= 2) {
                    z = true;
                }
            }
            if (!z) {
                return i2;
            }
            jSONObject.put(IXAdCommonUtils.PKGS_PREF_DOWNLOAD_STATUS, 5);
            SharedPreferences.Editor edit = sharedPreferences.edit();
            edit.putString(str, jSONObject.toString());
            if (Build.VERSION.SDK_INT >= 9) {
                edit.apply();
                return 5;
            }
            edit.commit();
            return 5;
        } catch (Exception e3) {
            return -1;
        }
    }

    public String getStatusStr(Context context, String str, String str2) {
        try {
            int apkDownloadStatus = getApkDownloadStatus(context, str, str2);
            boolean isInstalled = m.a().l().isInstalled(context, str);
            switch (apkDownloadStatus) {
                case 0:
                case 1:
                    if (isInstalled) {
                        return "INSTALLED_BY_OTHER";
                    }
                    return "DOWNLOADING";
                case 2:
                case 4:
                    if (isInstalled) {
                        return "INSTALLED_BY_OTHER";
                    }
                    return "DOWNLOAD_FAILED";
                case 3:
                    if (isInstalled) {
                        return "INSTALLED";
                    }
                    return "DOWNLOADED";
                case 5:
                    if (isInstalled) {
                        return "DONE";
                    }
                    return "NONE";
                case 100:
                    return "DONE";
                default:
                    if (isInstalled) {
                        return "INSTALLED_BY_OTHER";
                    }
                    return "NONE";
            }
        } catch (Exception e2) {
            return "NONE";
        }
    }

    public String getMD5(String str) {
        byte[] bytes = str.getBytes();
        char[] cArr = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(bytes);
            byte[] digest = instance.digest();
            char[] cArr2 = new char[32];
            int i2 = 0;
            for (int i3 = 0; i3 < 16; i3++) {
                byte b2 = digest[i3];
                int i4 = i2 + 1;
                cArr2[i2] = cArr[(b2 >>> 4) & 15];
                i2 = i4 + 1;
                cArr2[i4] = cArr[b2 & 15];
            }
            return new String(cArr2);
        } catch (NoSuchAlgorithmException e2) {
            m.a().f().e("AdUtil.getMD5", "", e2);
            return null;
        }
    }

    private String c(String str) {
        return getMD5(str);
    }

    private String a(Context context) {
        StringBuilder sb = new StringBuilder();
        try {
            String file = context.getFilesDir().toString();
            sb.append(file.toString().substring(0, file.toString().lastIndexOf(File.separator)));
        } catch (Exception e2) {
        }
        sb.append(File.separator);
        sb.append("bddownload");
        return sb.toString();
    }

    public String getFileLocalFullPath(Context context, String str) {
        try {
            return a(context) + File.separator + c(str);
        } catch (Exception e2) {
            return "";
        }
    }

    public boolean isStringAvailable(String str) {
        return str != null && str.length() > 0;
    }

    public void makeCall(Context context, String str) {
        try {
            if (TextUtils.isEmpty(str)) {
                Intent intent = new Intent("android.intent.action.CALL", Uri.parse(("tel:" + str).toString()));
                intent.addFlags(NTLMConstants.FLAG_UNIDENTIFIED_11);
                a(context, intent);
            }
        } catch (Exception e2) {
            m.a().f().d(e2);
        }
    }

    public void sendSMS(Context context, String str, String str2) {
        try {
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.putExtra("address", str);
            intent.putExtra("sms_body", str2);
            intent.setType("vnd.android-dir/mms-sms");
            intent.addFlags(NTLMConstants.FLAG_UNIDENTIFIED_11);
            a(context, intent);
        } catch (Exception e2) {
            m.a().f().d(e2);
        }
    }

    @TargetApi(4)
    private void a(Context context, Intent intent) {
        try {
            if (Build.VERSION.SDK_INT < 19) {
                context.startActivity(intent);
            } else {
                new Handler(context.getMainLooper()).post(new e(this, context, intent));
            }
        } catch (Exception e2) {
            m.a().f().d(e2);
        }
    }

    public String getDebugToken(Context context) {
        try {
            if (b == null) {
                b = a(context, IXAdCommonUtils.DEBUG_TOKEN);
            }
            return b;
        } catch (Exception e2) {
            return "";
        }
    }

    public String getAppId(Context context) {
        try {
            if (f327a == null) {
                f327a = a(context, IXAdCommonUtils.APPSID);
            }
            return f327a;
        } catch (Exception e2) {
            return "";
        }
    }

    public String getAppSec(Context context) {
        if (c == null || c.length() == 0 || c.startsWith("null")) {
            setAppSec(getAppId(context));
        }
        return c;
    }

    public void setAppSec(String str) {
        c = str + "_cpr";
    }

    private String a(Context context, String str) {
        String str2 = SocketMessage.MSG_ERROR_KEY;
        try {
            str2 = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData.get(str) + "";
            if (str2.trim().equals("")) {
                throw new Exception();
            }
        } catch (Exception e2) {
            String.format("Could not read %s meta-data from AndroidManifest.xml", str);
        }
        return str2;
    }

    public String md5(String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(str.getBytes());
            byte[] digest = instance.digest();
            StringBuffer stringBuffer = new StringBuffer();
            for (byte b2 : digest) {
                String hexString = Integer.toHexString(b2 & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD);
                while (hexString.length() < 2) {
                    hexString = "0" + hexString;
                }
                stringBuffer.append(hexString);
            }
            return stringBuffer.toString();
        } catch (NoSuchAlgorithmException e2) {
            return "";
        }
    }

    public long generateUniqueId() {
        long j;
        long j2;
        do {
            j = this.f.get();
            j2 = j + 1;
            if (j2 > 9223372036854775806L) {
                j2 = 1;
            }
        } while (!this.f.compareAndSet(j, j2));
        return j;
    }

    public boolean bitMaskContainsFlag(int i2, int i3) {
        return (i2 & i3) != 0;
    }

    @TargetApi(17)
    public Rect getScreenRect(Context context) {
        DisplayMetrics displayMetrics = getDisplayMetrics(context);
        try {
            if (displayMetrics.widthPixels > displayMetrics.heightPixels) {
                return new Rect(0, 0, displayMetrics.heightPixels, displayMetrics.widthPixels);
            }
            return new Rect(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        } catch (Exception e2) {
            return null;
        }
    }

    public Rect getWindowRect(Context context) {
        DisplayMetrics displayMetrics = getDisplayMetrics(context);
        return new Rect(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
    }

    @TargetApi(4)
    public float getScreenDensity(Context context) {
        return getDisplayMetrics(context).density;
    }

    @TargetApi(17)
    public DisplayMetrics getDisplayMetrics(Context context) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        if (Build.VERSION.SDK_INT >= 17) {
            ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getRealMetrics(displayMetrics);
        } else {
            ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
        }
        return displayMetrics;
    }

    public int getLogicalPixel(Context context, int i2) {
        try {
            return (int) (((float) i2) / getScreenDensity(context));
        } catch (Exception e2) {
            return i2;
        }
    }

    public int getPixel(Context context, int i2) {
        try {
            return (int) (((float) i2) * getScreenDensity(context));
        } catch (Exception e2) {
            return i2;
        }
    }

    public String getTextEncoder(String str) {
        if (str == null || "".equals(str)) {
            return "";
        }
        try {
            return URLEncoder.encode(str, "UTF-8");
        } catch (UnsupportedEncodingException | NullPointerException e2) {
            return str;
        }
    }

    public String getSubscriberId(Context context) {
        if (this.h == null) {
            try {
                if (hasPermission(context, "android.permission.READ_PHONE_STATE")) {
                    this.h = ((TelephonyManager) context.getSystemService("phone")).getSubscriberId();
                }
            } catch (Exception e2) {
            }
        }
        return b(this.h);
    }

    public String getAppPackage(Context context) {
        return context.getPackageName();
    }

    public boolean hasPermission(Context context, String str) {
        try {
            return context.checkCallingOrSelfPermission(str) == 0;
        } catch (Exception e2) {
            j.a().e(e2);
            return false;
        }
    }

    public String encodeURIComponent(String str) {
        try {
            return URLEncoder.encode(str, "UTF-8").replaceAll("\\+", "%20").replaceAll("\\%21", "!").replaceAll("\\%27", "'").replaceAll("\\%28", "(").replaceAll("\\%29", ")").replaceAll("\\%7E", "~");
        } catch (Exception e2) {
            return str;
        }
    }

    public String decodeURIComponent(String str) {
        if (str == null) {
            return null;
        }
        try {
            return URLDecoder.decode(str, "UTF-8");
        } catch (Exception e2) {
            return str;
        }
    }

    public String vdUrl(String str, int i2) {
        m.a().i();
        JSONObject jSONObject = new JSONObject();
        String[] split = str.substring(str.indexOf("?") + 1).split("&");
        for (int i3 = 0; i3 < split.length; i3++) {
            try {
                String[] split2 = split[i3].split("=");
                if (split2.length > 1 && !split2[0].equals("type")) {
                    jSONObject.putOpt(split2[0], split2[1]);
                }
            } catch (Exception e2) {
            }
        }
        StringBuilder sb = new StringBuilder("type=" + i2 + "&");
        TreeMap treeMap = new TreeMap();
        StringBuilder sb2 = new StringBuilder();
        try {
            Iterator<String> keys = jSONObject.keys();
            while (keys.hasNext()) {
                try {
                    String next = keys.next();
                    if (next != null && !next.equals("")) {
                        treeMap.put(next, jSONObject.optString(next));
                    }
                } catch (Exception e3) {
                }
            }
        } catch (Exception e4) {
        }
        treeMap.put(DeviceInfo.TAG_TIMESTAMPS, System.currentTimeMillis() + "");
        for (String str2 : treeMap.keySet()) {
            String str3 = (String) treeMap.get(str2);
            if (!(str2 == null || str3 == null)) {
                if (!str2.equals("targetscheme")) {
                    str2 = encodeURIComponent(str2);
                    str3 = encodeURIComponent(str3);
                }
                sb.append(str2 + "=" + str3 + "&");
                sb2.append(str3 + ",");
            }
        }
        sb2.append("mobads,");
        sb.append("vd=" + getMD5(sb2.toString()) + "&");
        return "http://mobads-logs.baidu.com/dz.zb?" + sb.toString();
    }

    public String getChannelId() {
        return d;
    }

    public void setChannelId(String str) {
        d = str;
    }

    public String getBaiduMapsInfo(Context context) {
        Object a2 = a("mapinfo");
        if (a2 != null) {
            return String.valueOf(a2);
        }
        String str = "";
        try {
            str = new com.baidu.mobads.i.a(context).a();
        } catch (Exception e2) {
            j.a().e(e2);
        }
        a("mapinfo", str);
        return str;
    }

    public void a(String str, Object obj) {
        this.i.put(str + "_E", Long.valueOf(System.currentTimeMillis() + 5000));
        this.i.put(str + "_V", obj);
    }

    public Object a(String str) {
        try {
            Object obj = this.i.get(str + "_E");
            if (obj != null) {
                if (System.currentTimeMillis() < ((Long) obj).longValue()) {
                    return this.i.get(str + "_V");
                }
            }
        } catch (Exception e2) {
            j.a().e(e2);
        }
        return null;
    }

    public void setAppId(String str) {
        f327a = str;
    }

    @TargetApi(3)
    private static String b(Context context) {
        int i2;
        String[] supportedBrowsers = m.a().p().getSupportedBrowsers();
        try {
            PackageManager packageManager = context.getPackageManager();
            ArrayList arrayList = new ArrayList();
            ArrayList<ComponentName> arrayList2 = new ArrayList<>();
            packageManager.getPreferredActivities(arrayList, arrayList2, null);
            for (ComponentName componentName : arrayList2) {
                int i3 = 0;
                while (true) {
                    if (i3 < supportedBrowsers.length) {
                        String str = supportedBrowsers[i3];
                        if (str.equals(componentName.getPackageName())) {
                            m.a().f().d(str, "规则1 hit!");
                            return str;
                        }
                        i3++;
                    }
                }
            }
            try {
                i2 = -1;
                for (ActivityManager.RunningAppProcessInfo next : ((ActivityManager) context.getSystemService("activity")).getRunningAppProcesses()) {
                    try {
                        if (!(packageManager.getLaunchIntentForPackage(next.processName) == null || packageManager.getApplicationInfo(next.processName, 128) == null)) {
                            for (int i4 = 0; i4 < supportedBrowsers.length; i4++) {
                                if (next.processName.equals(supportedBrowsers[i4])) {
                                    if (i2 == -1) {
                                        i2 = i4;
                                    } else if (i4 < i2) {
                                        i2 = i4;
                                    }
                                }
                            }
                        }
                    } catch (Exception e2) {
                    }
                }
            } catch (Exception e3) {
                i2 = -1;
            }
            if (i2 != -1) {
                return supportedBrowsers[i2];
            }
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setData(Uri.parse("http://m.baidu.com"));
            List<ResolveInfo> queryIntentActivities = packageManager.queryIntentActivities(intent, 64);
            if (queryIntentActivities != null) {
                for (int i5 = 0; i5 < supportedBrowsers.length; i5++) {
                    for (int i6 = 0; i6 < queryIntentActivities.size(); i6++) {
                        String str2 = queryIntentActivities.get(i6).activityInfo.packageName;
                        String str3 = supportedBrowsers[i5];
                        if (str3.equals(str2)) {
                            m.a().f().d(str3, "规则2 hit!");
                            return str3;
                        }
                    }
                }
            }
            if (queryIntentActivities != null) {
                if (queryIntentActivities.size() > 0) {
                    return queryIntentActivities.get(0).activityInfo.packageName;
                }
            }
            return "";
        } catch (Exception e4) {
            com.baidu.mobads.c.a.a().a("open browser outside failed: " + e4.toString());
        }
    }

    @TargetApi(3)
    public void browserOutside(Context context, String str) {
        Intent intent;
        if (str.startsWith("wtai://wp/mc;")) {
            str = "tel:" + str.substring("wtai://wp/mc;".length());
        }
        try {
            Intent intent2 = new Intent("android.intent.action.VIEW", Uri.parse(str));
            if (m.a().i().isHttpProtocol(str).booleanValue()) {
                String b2 = b(context);
                m.a().f().d("Utils", "AdUtil.browserOutside pkgOfBrowser=" + b2);
                if (!b2.equals("")) {
                    intent = context.getPackageManager().getLaunchIntentForPackage(b2);
                    intent.setData(Uri.parse(str));
                    intent.setAction("android.intent.action.VIEW");
                } else {
                    intent = intent2;
                }
                intent2 = intent;
            } else if (((r) m.a().i()).a(str).booleanValue()) {
                intent2.setType("vnd.android-dir/mms-sms");
                String substring = str.substring(4, str.indexOf(63) > 0 ? str.indexOf(63) : str.length());
                intent2.putExtra("address", substring);
                int length = "body=".length() + str.indexOf("body=");
                String str2 = "";
                if (length > "body=".length()) {
                    int indexOf = str.indexOf(38, length);
                    if (indexOf <= 0) {
                        indexOf = str.length();
                    }
                    str2 = str.substring(length, indexOf);
                    intent2.putExtra("sms_body", Uri.decode(str2));
                }
                m.a().f().d(substring, str2);
            }
            if (context.getPackageManager().resolveActivity(intent2, 65536) != null) {
                context.startActivity(intent2);
            }
        } catch (Exception e2) {
            m.a().f().d("XAdCommonUtils.browserOutside 1", str, e2);
            try {
                Intent intent3 = new Intent("android.intent.action.VIEW", Uri.parse(str));
                intent3.addFlags(NTLMConstants.FLAG_UNIDENTIFIED_11);
                context.startActivity(intent3);
            } catch (Exception e3) {
                m.a().f().d("XAdCommonUtils.browserOutside 2", str, e3);
            }
        }
    }

    public String a() {
        return "android_8.25_4.0.0";
    }

    public boolean hasSupportedApps(Context context, int i2) {
        Intent intent;
        try {
            IXAdSystemUtils n = m.a().n();
            switch (i2) {
                case 0:
                    intent = new Intent("android.intent.action.SENDTO");
                    intent.setData(Uri.parse("mailto:baidumobadstest@baidu.com"));
                    break;
                case 1:
                    intent = new Intent("android.intent.action.SENDTO");
                    intent.setData(Uri.parse("sms:12345678"));
                    break;
                case 2:
                    return m.a().m().hasPermission(context, "android.permission.ACCESS_WIFI_STATE") && m.a().m().hasPermission(context, "android.permission.CHANGE_WIFI_STATE");
                case 3:
                    return n.canSupportSdcardStroage(context);
                case 4:
                    intent = new Intent("android.intent.action.EDIT");
                    intent.setType("vnd.android.cursor.item/event");
                    break;
                default:
                    return false;
            }
            new ArrayList();
            List<ResolveInfo> queryIntentActivities = context.getPackageManager().queryIntentActivities(intent, 64);
            if (queryIntentActivities == null || queryIntentActivities.size() <= 0 || queryIntentActivities.get(0).activityInfo.packageName.equals("com.android.fallback")) {
                return false;
            }
            return true;
        } catch (Exception e2) {
            return false;
        }
    }

    public JSONArray list2Json(List<String[]> list) {
        JSONArray jSONArray = new JSONArray();
        int i2 = 0;
        while (i2 < list.size()) {
            try {
                JSONArray jSONArray2 = new JSONArray();
                for (String put : list.get(i2)) {
                    jSONArray2.put(put);
                }
                jSONArray.put(jSONArray2);
                i2++;
            } catch (Exception e2) {
                m.a().f().d(e2);
            }
        }
        return jSONArray;
    }

    public String base64Encode(String str) {
        return m.a().e().encode(str);
    }

    public JSONArray array2Json(double[] dArr) {
        Exception e2;
        JSONArray jSONArray;
        if (dArr == null) {
            return null;
        }
        try {
            jSONArray = new JSONArray();
            int i2 = 0;
            while (i2 < dArr.length) {
                try {
                    jSONArray.put(dArr[i2]);
                    i2++;
                } catch (Exception e3) {
                    e2 = e3;
                    m.a().f().d(e2);
                    return jSONArray;
                }
            }
            return jSONArray;
        } catch (Exception e4) {
            Exception exc = e4;
            jSONArray = null;
            e2 = exc;
            m.a().f().d(e2);
            return jSONArray;
        }
    }

    public String getLocationInfo(Context context) {
        return getBaiduMapsInfo(context);
    }

    public String getApkFileLocalPath(Context context, String str) {
        try {
            String string = context.getSharedPreferences(IXAdCommonUtils.PKGS_PREF_DOWNLOAD, 0).getString(str + "#$#" + a.b(), "");
            if (!TextUtils.isEmpty(string)) {
                JSONObject jSONObject = new JSONObject(string);
                String optString = jSONObject.optString("folder");
                String optString2 = jSONObject.optString("filename");
                if (!TextUtils.isEmpty(optString) && !TextUtils.isEmpty(optString2)) {
                    return optString + optString2;
                }
            }
        } catch (Exception e2) {
            m.a().f().d(e2);
        }
        return "";
    }

    public void installApp(Context context, String str, File file, boolean z) {
        try {
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.addCategory("android.intent.category.DEFAULT");
            intent.addFlags(NTLMConstants.FLAG_UNIDENTIFIED_11);
            intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
            context.startActivity(intent);
            if (!z || str != null) {
            }
        } catch (Exception e2) {
            m.a().f().d(e2);
        }
    }

    public boolean isOldPermissionModel() {
        return Build.VERSION.SDK_INT < 23;
    }

    public boolean checkSelfPermission(Context context, String str) {
        try {
            if (((Integer) Context.class.getMethod("checkSelfPermission", String.class).invoke(context, str)).intValue() == 0) {
                return true;
            }
            return false;
        } catch (Exception e2) {
            m.a().f().d(e2);
            return true;
        }
    }

    public void a(Context context, String[] strArr, int i2) {
        Class<Activity> cls = Activity.class;
        try {
            cls.getMethod("requestPermissions", String[].class, Integer.TYPE).invoke(context, strArr, Integer.valueOf(i2));
        } catch (Exception e2) {
            m.a().f().d(e2);
        }
    }

    public String createRequestId(Context context, String str) {
        return getMD5(m.a().n().getIMEI(context) + getAppId(context) + str + System.currentTimeMillis());
    }

    public String b(String str) {
        return str == null ? "" : str;
    }

    public void a(Runnable runnable) {
        if (runnable != null) {
            try {
                if (Looper.myLooper() == Looper.getMainLooper()) {
                    runnable.run();
                } else {
                    new Handler(Looper.getMainLooper()).post(new f(this, runnable));
                }
            } catch (Exception e2) {
                m.a().f().d(e2);
            }
        }
    }

    public int getStatusBarHeight(Activity activity) {
        try {
            Rect rect = new Rect();
            activity.getWindow().getDecorView().getWindowVisibleDisplayFrame(rect);
            return rect.top;
        } catch (Exception e2) {
            return 0;
        }
    }

    public Class<?> a(Object obj) {
        try {
            return Class.forName(obj.getClass().getName());
        } catch (Exception e2) {
            m.a().f().d(e2);
            return null;
        }
    }

    public Method a(Object obj, String str, Class<?>... clsArr) {
        try {
            Method declaredMethod = a(obj).getDeclaredMethod(str, clsArr);
            declaredMethod.setAccessible(true);
            return declaredMethod;
        } catch (Exception e2) {
            m.a().f().d(e2);
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.mobads.j.d.a(java.lang.Object, java.lang.String, java.lang.Class<?>[]):java.lang.reflect.Method
     arg types: [java.lang.Object, java.lang.String, java.lang.Class[]]
     candidates:
      com.baidu.mobads.j.d.a(java.lang.Object, java.lang.String, java.lang.Object[]):java.lang.Object
      com.baidu.mobads.j.d.a(android.content.Context, java.lang.String[], int):void
      com.baidu.mobads.j.d.a(java.lang.Object, java.lang.String, java.lang.Class<?>[]):java.lang.reflect.Method */
    public Object a(Object obj, String str, Object... objArr) {
        try {
            Class[] clsArr = new Class[objArr.length];
            for (int i2 = 0; i2 < clsArr.length; i2++) {
                clsArr[i2] = objArr[i2].getClass();
            }
            return a(obj, str, (Class<?>[]) clsArr).invoke(obj, objArr);
        } catch (Exception e2) {
            m.a().f().d(e2);
            return null;
        }
    }

    public void a(View view) {
        if (view != null) {
            try {
                if (view.getParent() != null) {
                    ((ViewGroup) view.getParent()).removeView(view);
                }
            } catch (Exception e2) {
                m.a().f().d(e2);
            }
        }
    }
}
