package com.baidu.mobads.j;

import android.net.wifi.ScanResult;
import java.util.Comparator;

class p implements Comparator<ScanResult> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ n f337a;

    p(n nVar) {
        this.f337a = nVar;
    }

    /* renamed from: a */
    public int compare(ScanResult scanResult, ScanResult scanResult2) {
        return scanResult2.level - scanResult.level;
    }
}
