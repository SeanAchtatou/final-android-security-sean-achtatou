package com.baidu.mobads.j;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import com.baidu.mobads.interfaces.utils.IXAdActivityUtils;

public class b implements IXAdActivityUtils {
    public Boolean isFullScreen(Activity activity) {
        boolean z;
        if (activity == null) {
            return false;
        }
        try {
            if ((activity.getWindow().getAttributes().flags & 1024) == 1024) {
                z = true;
            } else {
                z = false;
            }
            return Boolean.valueOf(z);
        } catch (Exception e) {
            return false;
        }
    }

    public void showAlertDialog(Activity activity, String str, String str2, String str3, String str4, boolean z, DialogInterface.OnClickListener onClickListener, DialogInterface.OnClickListener onClickListener2) {
        if (activity != null) {
            try {
                new AlertDialog.Builder(activity).setCancelable(z).setTitle(str).setMessage(str2).setPositiveButton(str3, onClickListener).setNegativeButton(str4, onClickListener2).create().show();
            } catch (Exception e) {
                m.a().f().d(e);
            }
        }
    }
}
