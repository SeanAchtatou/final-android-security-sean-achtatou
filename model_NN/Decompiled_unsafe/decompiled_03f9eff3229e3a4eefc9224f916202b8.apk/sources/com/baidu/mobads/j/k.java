package com.baidu.mobads.j;

import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import com.baidu.mobads.a.a;
import com.baidu.mobads.interfaces.utils.IBase64;
import com.baidu.mobads.interfaces.utils.IXAdPackageUtils;
import com.baidu.mobads.interfaces.utils.IXAdSystemUtils;
import com.baidu.mobads.interfaces.utils.IXAdURIUitls;
import com.baidu.mobads.openad.e.d;
import java.io.File;
import java.util.List;
import org.apache.mina.proxy.handlers.http.ntlm.NTLMConstants;

public class k implements IXAdPackageUtils {
    public boolean isInstalled(Context context, String str) {
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(str, 0);
            if (applicationInfo == null || !str.equals(applicationInfo.packageName)) {
                return false;
            }
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public boolean isSystemPackage(PackageInfo packageInfo) {
        return (packageInfo.applicationInfo.flags & 1) != 0;
    }

    @TargetApi(3)
    public void openApp(Context context, String str) {
        try {
            Intent launchIntentForPackage = context.getPackageManager().getLaunchIntentForPackage(str);
            launchIntentForPackage.addFlags(NTLMConstants.FLAG_UNIDENTIFIED_11);
            context.startActivity(launchIntentForPackage);
        } catch (Exception e) {
        }
    }

    public Intent getInstallIntent(String str) {
        try {
            Uri fromFile = Uri.fromFile(new File(str));
            Intent intent = new Intent("android.intent.action.VIEW");
            try {
                intent.addCategory("android.intent.category.DEFAULT");
                intent.addFlags(NTLMConstants.FLAG_UNIDENTIFIED_11);
                intent.setDataAndType(fromFile, "application/vnd.android.package-archive");
                return intent;
            } catch (Exception e) {
                return intent;
            }
        } catch (Exception e2) {
            return null;
        }
    }

    public int getAppVersion(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 16384).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            return 0;
        }
    }

    public IXAdPackageUtils.ApkInfo getLocalApkFileInfo(Context context, String str) {
        try {
            PackageInfo packageArchiveInfo = context.getPackageManager().getPackageArchiveInfo(str, 1);
            if (packageArchiveInfo != null) {
                return new IXAdPackageUtils.ApkInfo(context, packageArchiveInfo);
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    public boolean isForeground(Context context, String str) {
        try {
            for (ActivityManager.RunningAppProcessInfo next : ((ActivityManager) context.getSystemService("activity")).getRunningAppProcesses()) {
                if (next.processName.equals(str)) {
                    if (next.importance == 100) {
                        return true;
                    }
                    return false;
                }
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    public void sendAPOIsSuccess(Context context, boolean z, int i, String str, String str2) {
        d m = m.a().m();
        IXAdURIUitls i2 = m.a().i();
        IXAdSystemUtils n = m.a().n();
        IBase64 e = m.a().e();
        String encodeUrl = i2.encodeUrl(str);
        StringBuilder sb = new StringBuilder();
        sb.append("aposuccess=" + z);
        if (!z) {
            sb.append("&failtime=" + i);
        }
        sb.append("&sn=" + n.getEncodedSN(context));
        sb.append("&mac=" + e.encode(n.getMacAddress(context)));
        sb.append("&cuid=" + n.getCUID(context));
        sb.append("&pack=" + context.getPackageName());
        sb.append("&v=" + ("android_" + a.c + "_" + "4.1.30"));
        sb.append("&targetscheme=" + encodeUrl);
        sb.append("&pk=" + str2);
        try {
            d dVar = new d(i2.addParameters(m.vdUrl(sb.toString(), 369), null), "");
            dVar.e = 1;
            new com.baidu.mobads.openad.e.a().a(dVar);
        } catch (Exception e2) {
            m.a().f().d("XAdPackageUtils", e2.getMessage());
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    public boolean sendAPOInfo(Context context, String str, String str2, int i, int i2) {
        String str3;
        boolean z = false;
        PackageManager packageManager = context.getPackageManager();
        IXAdSystemUtils n = m.a().n();
        IXAdURIUitls i3 = m.a().i();
        d m = m.a().m();
        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
        intent.addFlags(NTLMConstants.FLAG_UNIDENTIFIED_11);
        String encodeUrl = i3.encodeUrl(str);
        List<ResolveInfo> queryIntentActivities = packageManager.queryIntentActivities(intent, 65536);
        String str4 = "&sn=" + n.getEncodedSN(context) + "&fb_act=" + i2 + "&pack=" + context.getPackageName() + "&v=" + ("android_" + a.c + "_" + "4.1.30") + "&targetscheme=" + encodeUrl + "&pk=" + str2;
        if (queryIntentActivities.size() > 0) {
            str3 = ("&open=" + "true") + "&n=" + queryIntentActivities.size();
            while (true) {
                int i4 = z;
                if (i4 >= queryIntentActivities.size()) {
                    break;
                }
                ResolveInfo resolveInfo = queryIntentActivities.get(i4);
                if (i4 == 0) {
                    str3 = str3 + "&p=" + resolveInfo.activityInfo.packageName;
                } else {
                    str3 = str3 + "," + resolveInfo.activityInfo.packageName;
                }
                z = i4 + 1;
            }
            z = true;
        } else {
            str3 = "&open=" + "false";
        }
        try {
            d dVar = new d(i3.addParameters(m.vdUrl(str4 + str3, i), null), "");
            dVar.e = 1;
            new com.baidu.mobads.openad.e.a().a(dVar);
        } catch (Exception e) {
            m.a().f().d("XAdPackageUtils", e.getMessage());
        }
        return z;
    }

    public void sendDialerIsSuccess(Context context, boolean z, int i, String str) {
        d m = m.a().m();
        IXAdURIUitls i2 = m.a().i();
        IXAdSystemUtils n = m.a().n();
        IBase64 e = m.a().e();
        StringBuilder sb = new StringBuilder();
        sb.append("callstate=" + z);
        if (!z) {
            sb.append("&duration=" + i);
        }
        sb.append("&sn=" + n.getEncodedSN(context));
        sb.append("&mac=" + e.encode(n.getMacAddress(context)));
        sb.append("&bdr=" + Build.VERSION.SDK_INT);
        sb.append("&cuid=" + n.getCUID(context));
        sb.append("&pack=" + context.getPackageName());
        sb.append("&v=" + ("android_" + a.c + "_" + "4.1.30"));
        sb.append("&pk=" + str);
        try {
            d dVar = new d(i2.addParameters(m.vdUrl(sb.toString(), 372), null), "");
            dVar.e = 1;
            new com.baidu.mobads.openad.e.a().a(dVar);
        } catch (Exception e2) {
            m.a().f().d("XAdPackageUtils", e2.getMessage());
        }
    }
}
