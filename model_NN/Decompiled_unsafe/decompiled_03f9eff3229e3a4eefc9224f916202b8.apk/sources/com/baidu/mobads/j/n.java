package com.baidu.mobads.j;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;
import android.os.Process;
import android.os.StatFs;
import android.provider.Settings;
import android.telephony.CellLocation;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.text.TextUtils;
import android.text.format.Formatter;
import cn.banshenggua.aichang.utils.Constants;
import com.baidu.mobads.interfaces.utils.IXAdLogger;
import com.baidu.mobads.interfaces.utils.IXAdSystemUtils;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;
import org.json.JSONArray;

public class n implements IXAdSystemUtils {
    private static String b;

    /* renamed from: a  reason: collision with root package name */
    public JSONArray f335a = new JSONArray();
    private String c;
    private String d;
    private String e;
    private String f;
    private String g;
    private int h = -1;
    private String i;
    private String j;
    private String k;
    private String l;

    @TargetApi(4)
    public boolean isTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout & 15) >= 3;
    }

    public String getIMEI(Context context) {
        if (TextUtils.isEmpty(this.c) && context != null) {
            SharedPreferences sharedPreferences = context.getSharedPreferences("__x_adsdk_agent_header__", 0);
            String string = sharedPreferences.getString(m.a().e().decodeStr("pyd-pifb"), "");
            if (!TextUtils.isEmpty(string)) {
                this.c = string;
            } else {
                try {
                    String decodeStr = m.a().e().decodeStr("uvNYwANvpyP-iyfb");
                    String str = (String) m.a().m().a((TelephonyManager) context.getApplicationContext().getSystemService("phone"), decodeStr, new Object[0]);
                    if (!TextUtils.isEmpty(str)) {
                        new Thread(new o(this, sharedPreferences, str)).start();
                        this.c = str;
                    }
                } catch (Exception e2) {
                    j.a().d(e2);
                }
            }
        }
        return m.a().m().b(this.c);
    }

    public String getSn(Context context) {
        try {
            if (TextUtils.isEmpty(this.d)) {
                String imei = getIMEI(context);
                if (TextUtils.isEmpty(imei)) {
                    imei = getMacAddress(context);
                }
                this.d = m.a().m().b(imei);
            }
            return this.d;
        } catch (Exception e2) {
            return "";
        }
    }

    public String getCUID(Context context) {
        String string;
        try {
            if (TextUtils.isEmpty(b) && (string = Settings.System.getString(context.getContentResolver(), "com.baidu.deviceid")) != null && !string.equals("")) {
                String string2 = Settings.System.getString(context.getContentResolver(), "bd_setting_i");
                if (TextUtils.isEmpty(string2)) {
                    string2 = "0";
                }
                b = string + "|" + new StringBuffer(string2).reverse().toString();
            }
            return m.a().m().b(b);
        } catch (Exception e2) {
            return m.a().m().b(b);
        }
    }

    public double[] getGPS(Context context) {
        double[] dArr;
        try {
            Object a2 = m.a().m().a("SYSGPS");
            if (a2 != null) {
                return (double[]) a2;
            }
        } catch (Exception e2) {
            j.a().e(e2);
        }
        if (m.a().m().hasPermission(context, "android.permission.ACCESS_FINE_LOCATION")) {
            try {
                Location lastKnownLocation = ((LocationManager) context.getSystemService("location")).getLastKnownLocation("gps");
                if (lastKnownLocation != null) {
                    dArr = new double[3];
                    try {
                        dArr[0] = (double) lastKnownLocation.getTime();
                        dArr[1] = lastKnownLocation.getLongitude();
                        dArr[2] = lastKnownLocation.getLatitude();
                    } catch (SecurityException e3) {
                    }
                    m.a().m().a("SYSGPS", dArr);
                    return dArr;
                }
            } catch (SecurityException e4) {
                dArr = null;
            }
        }
        dArr = null;
        m.a().m().a("SYSGPS", dArr);
        return dArr;
    }

    public String getGUID(Context context) {
        try {
            if (this.e != null || context == null) {
                return this.e;
            }
            d m = m.a().m();
            this.e = context.getSharedPreferences("__x_adsdk_agent_header__", 0).getString("guid", "");
            if (this.e == null || this.e.length() <= 0) {
                this.e = m.md5(getMacAddress(context) + "&" + getIMEI(context) + "&" + "&");
                if (this.e == null || this.e.length() <= 0) {
                    return "";
                }
                context.getSharedPreferences("__x_adsdk_agent_header__", 0).edit().putString("guid", this.e).commit();
            }
            return this.e;
        } catch (Exception e2) {
            return "";
        }
    }

    public String getAndroidId(Context context) {
        try {
            if (TextUtils.isEmpty(this.f)) {
                this.f = m.a().m().b(Settings.Secure.getString(context.getContentResolver(), "android_id"));
            }
            return this.f;
        } catch (Exception e2) {
            return "";
        }
    }

    public String getAppSDC() {
        try {
            Object a2 = m.a().m().a("sysSdc");
            if (a2 != null) {
                return (String) a2;
            }
        } catch (Exception e2) {
            j.a().e(e2);
        }
        if (!Environment.getExternalStorageState().equals("mounted")) {
            return "0,0";
        }
        String str = "";
        try {
            str = getAvailableExternalMemorySize() + "," + getAllExternalMemorySize();
            m.a().m().a("sysSdc", str);
            return str;
        } catch (Exception e3) {
            return str;
        }
    }

    public String getMem() {
        try {
            Object a2 = m.a().m().a("sysMem");
            if (a2 != null) {
                return (String) a2;
            }
        } catch (Exception e2) {
            j.a().e(e2);
        }
        String str = "";
        try {
            str = getAvailableInternalMemorySize() + "," + getAllInternalMemorySize();
            m.a().m().a("sysMem", str);
            return str;
        } catch (Exception e3) {
            return str;
        }
    }

    public long getAllExternalMemorySize() {
        try {
            if (Environment.getExternalStorageState().equals("mounted")) {
                return a(Environment.getExternalStorageDirectory());
            }
            return -1;
        } catch (Exception e2) {
            return -1;
        }
    }

    public long getAllInternalMemorySize() {
        try {
            return a(Environment.getDataDirectory());
        } catch (Exception e2) {
            return -1;
        }
    }

    public long getAvailableExternalMemorySize() {
        try {
            if (Environment.getExternalStorageState().equals("mounted")) {
                return b(Environment.getExternalStorageDirectory());
            }
            return -1;
        } catch (Exception e2) {
            return -1;
        }
    }

    public long getAvailableInternalMemorySize() {
        try {
            return b(Environment.getDataDirectory());
        } catch (Exception e2) {
            return -1;
        }
    }

    @TargetApi(18)
    private long a(File file) {
        try {
            StatFs statFs = new StatFs(file.getPath());
            return (long) (((statFs.getBlockSize() * statFs.getBlockCount()) / 1024) / 1024);
        } catch (Exception e2) {
            return -1;
        }
    }

    @TargetApi(18)
    private long b(File file) {
        try {
            StatFs statFs = new StatFs(file.getPath());
            return (long) (((statFs.getBlockSize() * statFs.getAvailableBlocks()) / 1024) / 1024);
        } catch (Exception e2) {
            return -1;
        }
    }

    public String getMacAddress(Context context) {
        if (TextUtils.isEmpty(this.g)) {
            IXAdLogger f2 = m.a().f();
            d m = m.a().m();
            try {
                WifiManager wifiManager = (WifiManager) context.getSystemService(IXAdSystemUtils.NT_WIFI);
                if (m.hasPermission(context, "android.permission.ACCESS_WIFI_STATE")) {
                    this.g = m.a().m().b(wifiManager.getConnectionInfo().getMacAddress());
                } else {
                    f2.e("", "Could not get mac address. no android.permission.ACCESS_WIFI_STATE");
                }
            } catch (Exception e2) {
                f2.e("", "Could not get mac address." + e2.toString());
            }
        }
        return this.g;
    }

    @TargetApi(3)
    public String getIp(Context context) {
        String str = "";
        if (((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo() == null) {
            return "";
        }
        try {
            str = Formatter.formatIpAddress(((WifiManager) context.getSystemService(IXAdSystemUtils.NT_WIFI)).getConnectionInfo().getIpAddress());
            if (!TextUtils.isEmpty(str)) {
                return "0.0.0.0".equals(str) ? "" : str;
            }
            Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
            while (networkInterfaces.hasMoreElements()) {
                Enumeration<InetAddress> inetAddresses = networkInterfaces.nextElement().getInetAddresses();
                while (true) {
                    if (inetAddresses.hasMoreElements()) {
                        InetAddress nextElement = inetAddresses.nextElement();
                        if (!nextElement.isLoopbackAddress() && !nextElement.isLinkLocalAddress() && nextElement.isSiteLocalAddress() && (nextElement instanceof Inet4Address)) {
                            return nextElement.getHostAddress();
                        }
                    }
                }
            }
            return str;
        } catch (Exception | SocketException e2) {
        }
    }

    public String getMaxCpuFreq() {
        FileReader fileReader;
        BufferedReader bufferedReader;
        IXAdLogger iXAdLogger;
        FileReader fileReader2 = null;
        if (this.h < 0) {
            try {
                fileReader = new FileReader("/sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_max_freq");
                try {
                    bufferedReader = new BufferedReader(fileReader);
                } catch (Exception e2) {
                    e = e2;
                    bufferedReader = null;
                    fileReader2 = fileReader;
                    try {
                        m.a().f().d(e);
                        try {
                            fileReader2.close();
                            bufferedReader.close();
                        } catch (IOException e3) {
                            e = e3;
                            iXAdLogger = m.a().f();
                        }
                        return this.h + "";
                    } catch (Throwable th) {
                        th = th;
                        fileReader = fileReader2;
                        try {
                            fileReader.close();
                            bufferedReader.close();
                        } catch (IOException e4) {
                            m.a().f().d(e4);
                        }
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    bufferedReader = null;
                    fileReader.close();
                    bufferedReader.close();
                    throw th;
                }
                try {
                    this.h = Integer.parseInt(bufferedReader.readLine().trim()) / Constants.CLEARIMGED;
                    try {
                        fileReader.close();
                        bufferedReader.close();
                    } catch (IOException e5) {
                        e = e5;
                        iXAdLogger = m.a().f();
                        iXAdLogger.d(e);
                        return this.h + "";
                    }
                } catch (Exception e6) {
                    e = e6;
                    fileReader2 = fileReader;
                    m.a().f().d(e);
                    fileReader2.close();
                    bufferedReader.close();
                    return this.h + "";
                } catch (Throwable th3) {
                    th = th3;
                    fileReader.close();
                    bufferedReader.close();
                    throw th;
                }
            } catch (Exception e7) {
                e = e7;
                bufferedReader = null;
                m.a().f().d(e);
                fileReader2.close();
                bufferedReader.close();
                return this.h + "";
            } catch (Throwable th4) {
                th = th4;
                bufferedReader = null;
                fileReader = null;
                fileReader.close();
                bufferedReader.close();
                throw th;
            }
        }
        return this.h + "";
    }

    public String getNetworkOperatorName(Context context) {
        if (TextUtils.isEmpty(this.i)) {
            try {
                d m = m.a().m();
                TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
                String simOperatorName = telephonyManager.getSimOperatorName();
                StringBuilder sb = new StringBuilder();
                if (!TextUtils.isEmpty(simOperatorName)) {
                    sb.append(simOperatorName);
                } else {
                    String networkOperatorName = telephonyManager.getNetworkOperatorName();
                    if (TextUtils.isEmpty(networkOperatorName)) {
                        return "";
                    }
                    sb.append(networkOperatorName);
                }
                sb.append("_");
                String simOperator = telephonyManager.getSimOperator();
                if (!TextUtils.isEmpty(simOperator)) {
                    sb.append(simOperator);
                }
                if (sb.length() > 1) {
                    this.i = m.getTextEncoder(sb.toString());
                }
            } catch (Exception e2) {
                m.a().f().e("Get operator failed", "");
            }
        }
        return this.i;
    }

    public String getNetworkOperator(Context context) {
        try {
            if (TextUtils.isEmpty(this.j)) {
                this.j = m.a().m().b(((TelephonyManager) context.getSystemService("phone")).getNetworkOperator());
            }
            return this.j;
        } catch (Exception e2) {
            return this.j;
        }
    }

    public String getPhoneOSBuildVersionSdk() {
        return m.a().m().b(Build.VERSION.SDK);
    }

    @SuppressLint({"DefaultLocale"})
    @TargetApi(3)
    public String getNetworkType(Context context) {
        Exception e2;
        String str;
        try {
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            if (activeNetworkInfo == null || !activeNetworkInfo.isConnectedOrConnecting()) {
                return IXAdSystemUtils.NT_NONE;
            }
            if (activeNetworkInfo.getType() == 1) {
                return IXAdSystemUtils.NT_WIFI;
            }
            str = "unknown";
            try {
                if (activeNetworkInfo.getSubtypeName() != null) {
                    return activeNetworkInfo.getSubtypeName().toLowerCase();
                }
                return str;
            } catch (Exception e3) {
                e2 = e3;
                m.a().f().i(e2);
                return str;
            }
        } catch (Exception e4) {
            Exception exc = e4;
            str = IXAdSystemUtils.NT_NONE;
            e2 = exc;
            m.a().f().i(e2);
            return str;
        }
    }

    public String getNetType(Context context) {
        try {
            String str = "_" + ((TelephonyManager) context.getSystemService("phone")).getNetworkType();
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
            NetworkInfo networkInfo = connectivityManager.getNetworkInfo(0);
            NetworkInfo networkInfo2 = connectivityManager.getNetworkInfo(1);
            if (networkInfo != null && networkInfo.isAvailable()) {
                return networkInfo.getExtraInfo() + str;
            }
            if (networkInfo2 == null || !networkInfo2.isAvailable()) {
                return str;
            }
            return IXAdSystemUtils.NT_WIFI + str;
        } catch (Exception e2) {
            Exception exc = e2;
            String str2 = "";
            j.a().e(exc);
            return str2;
        }
    }

    public int getNetworkCatagory(Context context) {
        try {
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
                if (activeNetworkInfo.getType() == 1) {
                    return 100;
                }
                if (activeNetworkInfo.getType() == 0) {
                    String subtypeName = activeNetworkInfo.getSubtypeName();
                    switch (activeNetworkInfo.getSubtype()) {
                        case 0:
                            return 1;
                        case 1:
                        case 2:
                        case 4:
                        case 7:
                        case 11:
                            return 2;
                        case 3:
                        case 5:
                        case 6:
                        case 8:
                        case 9:
                        case 10:
                            return 3;
                        default:
                            if (subtypeName == null || (!subtypeName.equalsIgnoreCase("TD-SCDMA") && !subtypeName.equalsIgnoreCase("WCDMA") && !subtypeName.equalsIgnoreCase("CDMA2000"))) {
                                return 1;
                            }
                            return 3;
                    }
                }
            }
            return 0;
        } catch (Exception e2) {
            return 0;
        }
    }

    public Boolean isWifiConnected(Context context) {
        return a(context, 1);
    }

    public Boolean is3GConnected(Context context) {
        return a(context, 0);
    }

    private Boolean a(Context context, int i2) {
        try {
            if (context.checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE") != 0) {
                m.a().f().e("Utils", "no permission android.permission.ACCESS_NETWORK_STATE");
                return false;
            }
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            return Boolean.valueOf(activeNetworkInfo != null && activeNetworkInfo.getType() == i2 && activeNetworkInfo.isConnected());
        } catch (Exception e2) {
            return false;
        }
    }

    public String getDeviceId(Context context) {
        return getIMEI(context);
    }

    public String getEncodedSN(Context context) {
        try {
            if (TextUtils.isEmpty(this.k)) {
                this.k = m.a().e().encode(getSn(context));
            }
            return this.k;
        } catch (Exception e2) {
            return this.k;
        }
    }

    public String getPhoneOSBrand() {
        return m.a().m().b(Build.BRAND);
    }

    public List<String[]> getCell(Context context) {
        try {
            Object a2 = m.a().m().a("cell");
            if (a2 != null) {
                return (List) a2;
            }
        } catch (Exception e2) {
            j.a().e(e2);
        }
        ArrayList arrayList = new ArrayList();
        try {
            CellLocation cellLocation = ((TelephonyManager) context.getSystemService("phone")).getCellLocation();
            if (cellLocation != null) {
                String[] strArr = new String[3];
                if (cellLocation instanceof GsmCellLocation) {
                    GsmCellLocation gsmCellLocation = (GsmCellLocation) cellLocation;
                    strArr[0] = gsmCellLocation.getCid() + "";
                    strArr[1] = gsmCellLocation.getLac() + "";
                    strArr[2] = "0";
                } else {
                    String[] split = cellLocation.toString().replace("[", "").replace("]", "").split(",");
                    strArr[0] = split[0];
                    strArr[1] = split[3];
                    strArr[2] = split[4];
                }
                arrayList.add(strArr);
            }
            m.a().m().a("cell", arrayList);
        } catch (Exception e3) {
        }
        return arrayList;
    }

    public List<String[]> getWIFI(Context context) {
        int i2 = 0;
        d m = m.a().m();
        try {
            Object a2 = m.a(IXAdSystemUtils.NT_WIFI);
            if (a2 != null) {
                return (List) a2;
            }
        } catch (Exception e2) {
            j.a().e(e2);
        }
        ArrayList arrayList = new ArrayList();
        try {
            if (m.hasPermission(context, "android.permission.ACCESS_WIFI_STATE")) {
                WifiManager wifiManager = (WifiManager) context.getSystemService(IXAdSystemUtils.NT_WIFI);
                if (wifiManager.isWifiEnabled()) {
                    List<ScanResult> scanResults = wifiManager.getScanResults();
                    Collections.sort(scanResults, new p(this));
                    while (i2 < scanResults.size() && i2 < 5) {
                        ScanResult scanResult = scanResults.get(i2);
                        arrayList.add(new String[]{scanResult.BSSID.replace(":", "").toLowerCase(Locale.getDefault()), Math.abs(scanResult.level) + ""});
                        i2++;
                    }
                }
            }
        } catch (Exception e3) {
            j.a().e(e3);
        }
        m.a(IXAdSystemUtils.NT_WIFI, arrayList);
        return arrayList;
    }

    public boolean canSupportSdcardStroage(Context context) {
        try {
            if (m.a().m().hasPermission(context, "android.permission.WRITE_EXTERNAL_STORAGE") || !isUseOldStoragePath()) {
                return true;
            }
            return false;
        } catch (Exception e2) {
            return false;
        }
    }

    public boolean isUseOldStoragePath() {
        return Build.VERSION.SDK_INT < 23;
    }

    public String getWifiConnected(Context context) {
        try {
            if (m.a().m().hasPermission(context, "android.permission.ACCESS_WIFI_STATE")) {
                WifiInfo connectionInfo = ((WifiManager) context.getSystemService(IXAdSystemUtils.NT_WIFI)).getConnectionInfo();
                String ssid = connectionInfo.getSSID();
                if (ssid == null) {
                    ssid = "";
                } else if (ssid.length() > 2 && ssid.startsWith("\"") && ssid.endsWith("\"")) {
                    ssid = ssid.substring(1, ssid.length() - 1);
                }
                return connectionInfo.getBSSID() + "|" + m.a().e().encode(ssid);
            }
        } catch (Exception e2) {
            m.a().f().d(e2);
        }
        return "";
    }

    public JSONArray getWifiScans(Context context) {
        try {
            Object a2 = m.a().m().a("wifiScans");
            if (a2 != null) {
                return (JSONArray) a2;
            }
        } catch (Exception e2) {
            j.a().e(e2);
        }
        JSONArray jSONArray = new JSONArray();
        try {
            if (m.a().m().hasPermission(context, "android.permission.ACCESS_WIFI_STATE")) {
                WifiManager wifiManager = (WifiManager) context.getSystemService(IXAdSystemUtils.NT_WIFI);
                if (wifiManager.isWifiEnabled()) {
                    List<ScanResult> scanResults = wifiManager.getScanResults();
                    Collections.sort(scanResults, new q(this));
                    int i2 = 0;
                    while (true) {
                        int i3 = i2;
                        if (i3 >= scanResults.size() || i3 >= 50) {
                            break;
                        }
                        ScanResult scanResult = scanResults.get(i3);
                        jSONArray.put(scanResult.BSSID + "|" + m.a().e().encode(scanResult.SSID));
                        i2 = i3 + 1;
                    }
                }
            }
        } catch (Exception e3) {
            m.a().f().d(e3);
        }
        m.a().m().a("wifiScans", jSONArray);
        return jSONArray;
    }

    public JSONArray getBackgroundBrowsers(Context context) {
        IXAdLogger f2 = m.a().f();
        String[] supportedBrowsers = m.a().p().getSupportedBrowsers();
        try {
            List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = ((ActivityManager) context.getSystemService("activity")).getRunningAppProcesses();
            PackageManager packageManager = context.getApplicationContext().getPackageManager();
            for (ActivityManager.RunningAppProcessInfo next : runningAppProcesses) {
                if (!(packageManager.getLaunchIntentForPackage(next.processName) == null || packageManager.getApplicationInfo(next.processName, 128) == null)) {
                    for (String equals : supportedBrowsers) {
                        if (next.processName.equals(equals)) {
                            this.f335a.put(next.processName);
                        }
                    }
                }
            }
        } catch (Exception e2) {
            f2.d(e2);
        }
        f2.d("bgBrowsers:" + this.f335a);
        return this.f335a;
    }

    public HttpURLConnection getHttpConnection(Context context, String str, int i2, int i3) {
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
            try {
                httpURLConnection.setConnectTimeout(i2);
                httpURLConnection.setReadTimeout(i3);
                return httpURLConnection;
            } catch (Exception e2) {
                return httpURLConnection;
            }
        } catch (Exception e3) {
            return null;
        }
    }

    public boolean isCurrentNetworkAvailable(Context context) {
        try {
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isAvailable();
        } catch (Exception e2) {
            m.a().f().d("isCurrentNetworkAvailable", e2);
            return false;
        }
    }

    public String getCurrentProcessName(Context context) {
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses;
        try {
            if (this.l == null) {
                int myPid = Process.myPid();
                ActivityManager activityManager = (ActivityManager) context.getSystemService("activity");
                if (!(activityManager == null || (runningAppProcesses = activityManager.getRunningAppProcesses()) == null)) {
                    for (ActivityManager.RunningAppProcessInfo next : runningAppProcesses) {
                        if (next.pid == myPid) {
                            this.l = next.processName;
                        }
                    }
                }
            }
            return this.l;
        } catch (Exception e2) {
            return this.l;
        }
    }

    public int getCurrentProcessId(Context context) {
        try {
            return Process.myPid();
        } catch (Exception e2) {
            return 0;
        }
    }
}
