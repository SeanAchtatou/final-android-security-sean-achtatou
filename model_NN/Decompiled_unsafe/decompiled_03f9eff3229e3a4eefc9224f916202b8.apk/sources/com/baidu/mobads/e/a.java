package com.baidu.mobads.e;

import android.text.TextUtils;
import com.baidu.mobads.interfaces.error.IXAdErrorCode;
import com.baidu.mobads.interfaces.error.XAdErrorCode;
import com.baidu.mobads.interfaces.utils.IXAdLogger;
import com.baidu.mobads.j.m;
import java.util.Map;

public class a implements IXAdErrorCode {

    /* renamed from: a  reason: collision with root package name */
    protected final IXAdLogger f281a;

    public a(IXAdLogger iXAdLogger) {
        this.f281a = iXAdLogger;
    }

    public void printErrorMessage(XAdErrorCode xAdErrorCode, String str) {
        this.f281a.e(genCompleteErrorMessage(xAdErrorCode, str));
    }

    public void printErrorMessage(String str, String str2, String str3) {
        this.f281a.e(a(str, str2, str3));
    }

    public String genCompleteErrorMessage(XAdErrorCode xAdErrorCode, String str) {
        if (xAdErrorCode == null) {
            return "";
        }
        return a(xAdErrorCode.getCode() + "", xAdErrorCode.getMessage(), str);
    }

    private String a(String str, String str2, String str3) {
        StringBuilder sb = new StringBuilder();
        sb.append("ErrorCode: [");
        sb.append(str);
        sb.append("]; ErrorDesc: [");
        sb.append(str2);
        sb.append("];");
        if (!TextUtils.isEmpty(str3)) {
            sb.append(" Extra: [");
            sb.append(str3);
            sb.append("];");
        }
        return sb.toString();
    }

    public String getMessage(Map<String, Object> map) {
        if (map == null) {
            return "";
        }
        return m.a().q().genCompleteErrorMessage((XAdErrorCode) map.get("msg"), "");
    }
}
