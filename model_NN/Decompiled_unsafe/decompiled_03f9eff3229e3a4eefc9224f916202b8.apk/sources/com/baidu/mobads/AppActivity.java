package com.baidu.mobads;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.support.v4.view.ViewCompat;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.baidu.mobads.command.XAdLandingPageExtraInfo;
import com.baidu.mobads.g.d;
import com.baidu.mobads.interfaces.IXAdRequestInfo;
import com.baidu.mobads.interfaces.utils.IXAdCommonUtils;
import com.baidu.mobads.interfaces.utils.IXAdLogger;
import com.baidu.mobads.j.j;
import com.baidu.mobads.j.m;
import com.baidu.mobads.openad.interfaces.download.IOAdDownloader;
import com.baidu.mobads.vo.a.c;
import java.io.File;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

public class AppActivity extends Activity {
    protected static final int ACTIONBAR_VIEW_ID = 1001;
    public static final String EXTRA_DATA = "EXTRA_DATA";
    /* access modifiers changed from: private */
    public static int G = 39;
    /* access modifiers changed from: private */
    public static ActionBarColorTheme J = ActionBarColorTheme.ACTION_BAR_WHITE_THEME;
    /* access modifiers changed from: private */
    public static final String o = AppActivity.class.getSimpleName();
    private static AtomicBoolean p = new AtomicBoolean(false);
    /* access modifiers changed from: private */
    public c A;
    private XAdLandingPageExtraInfo B;
    /* access modifiers changed from: private */
    public View C;
    /* access modifiers changed from: private */
    public final IXAdLogger D = m.a().f();
    /* access modifiers changed from: private */
    public IXAdCommonUtils E;
    private boolean F = true;
    /* access modifiers changed from: private */
    public PageFinishedListener H;
    private boolean I = true;

    /* renamed from: a  reason: collision with root package name */
    float f231a = 1.0f;
    public Handler actionBarHandler;
    float b = 1.0f;
    com.baidu.mobads.g.a c = null;
    public ac curWebview;
    RelativeLayout d;
    int e = 0;
    int f = 0;
    int g = -1;
    int h = 0;
    int i = 0;
    int j = 1;
    String k = "barC";
    String l = "";
    View m = null;
    protected d mBottomView;
    protected boolean mBottomViewIsShowing = false;
    HandlerThread n = new HandlerThread("handler_thread");
    /* access modifiers changed from: private */
    public Handler q = null;
    private int r = 0;
    /* access modifiers changed from: private */
    public long s = 0;
    private String t;
    private boolean u = false;
    /* access modifiers changed from: private */
    public int v = 1;
    private int w = 0;
    private int x = -1;
    /* access modifiers changed from: private */
    public boolean y = false;
    /* access modifiers changed from: private */
    public boolean z = false;

    public interface PageFinishedListener {
        void onPageFinished(WebView webView);
    }

    static /* synthetic */ int i(AppActivity appActivity) {
        int i2 = appActivity.r;
        appActivity.r = i2 + 1;
        return i2;
    }

    public static boolean isAppActivityOpening() {
        return p.get();
    }

    class a extends View {
        private Paint b = new Paint();
        private int c = 0;
        private int d = 0;

        public a(Context context) {
            super(context);
            this.b.setColor(AppActivity.J.getProgressColor());
            this.d = AppActivity.this.E.getScreenRect(AppActivity.this.getApplicationContext()).width();
        }

        public void onDraw(Canvas canvas) {
            super.onDraw(canvas);
            canvas.drawRect(0.0f, 0.0f, (float) ((this.d * this.c) / 100), (float) getLayoutParams().height, this.b);
        }

        public void a(int i) {
            if (i != this.c) {
                this.c = i;
                postInvalidate();
            }
        }
    }

    @TargetApi(3)
    private RelativeLayout a(String str) {
        a aVar = new a(this);
        this.curWebview = new ac(this, true, true);
        b bVar = new b();
        this.curWebview.f249a = str;
        this.curWebview.getSettings().setUseWideViewPort(true);
        this.curWebview.getSettings().setBuiltInZoomControls(true);
        Class<WebSettings> cls = WebSettings.class;
        try {
            cls.getMethod("setDisplayZoomControls", Boolean.TYPE).invoke(this.curWebview.getSettings(), false);
        } catch (Exception e2) {
            this.D.d(o, e2.getMessage());
        }
        this.curWebview.setWebChromeClient(new d(this, aVar));
        this.curWebview.setOnTouchListener(new i(this));
        this.curWebview.setWebViewClient(new j(this, bVar));
        getWindowManager().getDefaultDisplay().getMetrics(new DisplayMetrics());
        RelativeLayout relativeLayout = new RelativeLayout(this);
        this.m = d();
        relativeLayout.addView(this.m);
        relativeLayout.addView(this.curWebview, new RelativeLayout.LayoutParams(-1, -1));
        relativeLayout.addView(aVar, new RelativeLayout.LayoutParams(-1, this.E.getPixel(getApplicationContext(), 2)));
        return relativeLayout;
    }

    private View d() {
        ae aeVar = new ae(this);
        int pixel = m.a().m().getPixel(this, 38);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(pixel, pixel);
        layoutParams.addRule(13);
        aeVar.setLayoutParams(layoutParams);
        return aeVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.mobads.AppActivity.a(android.webkit.WebView, java.lang.String, boolean, java.lang.String):void
     arg types: [com.baidu.mobads.ac, java.lang.String, int, java.lang.String]
     candidates:
      com.baidu.mobads.AppActivity.a(android.webkit.WebView, java.lang.String, java.lang.Runnable, java.lang.Runnable):void
      com.baidu.mobads.AppActivity.a(android.webkit.WebView, java.lang.String, boolean, java.lang.String):void */
    public void onCreate(Bundle bundle) {
        com.baidu.mobads.openad.c.b a2;
        com.baidu.mobads.command.a a3;
        super.onCreate(bundle);
        p.set(true);
        this.u = k();
        this.E = m.a().m();
        Intent intent = getIntent();
        if (intent.getBooleanExtra("dealWithDownload", false)) {
            try {
                int intExtra = intent.getIntExtra("status", -1);
                String stringExtra = intent.getStringExtra(IXAdRequestInfo.PACKAGE);
                IOAdDownloader adsApkDownloader = com.baidu.mobads.openad.c.d.a(getApplicationContext()).getAdsApkDownloader(stringExtra);
                this.D.d(o, "dealWithDownload now: status=" + intExtra + ";pk=" + stringExtra + ";downloader=" + adsApkDownloader);
                if (intExtra == IOAdDownloader.DownloadStatus.COMPLETED.getCode()) {
                    String stringExtra2 = intent.getStringExtra("localApkPath");
                    if (m.a().l().isInstalled(this, stringExtra)) {
                        m.a().l().openApp(this, stringExtra);
                    } else {
                        File file = new File(stringExtra2);
                        if (!file.exists() || file.length() <= 0) {
                            this.D.i(o, "文件[" + stringExtra2 + "] 已经被删除");
                        } else {
                            startActivity(m.a().l().getInstallIntent(stringExtra2));
                        }
                    }
                } else if (intExtra == IOAdDownloader.DownloadStatus.ERROR.getCode() || intExtra == IOAdDownloader.DownloadStatus.PAUSED.getCode()) {
                    NetworkInfo activeNetworkInfo = ((ConnectivityManager) getSystemService("connectivity")).getActiveNetworkInfo();
                    if (!(activeNetworkInfo == null || !activeNetworkInfo.isConnected() || activeNetworkInfo.getType() != 0 || (a2 = com.baidu.mobads.openad.c.b.a(stringExtra)) == null || (a3 = a2.a()) == null)) {
                        a3.r = true;
                    }
                    if (adsApkDownloader != null) {
                        adsApkDownloader.resume();
                    }
                }
            } catch (Exception e2) {
                this.D.d(o, e2);
            }
            finish();
            return;
        }
        this.F = intent.getBooleanExtra("canOpenAppForAPO", this.F);
        com.baidu.mobads.j.d m2 = m.a().m();
        this.B = (XAdLandingPageExtraInfo) getIntent().getParcelableExtra(EXTRA_DATA);
        this.A = new c(this.B);
        Rect windowRect = m2.getWindowRect(this);
        this.b = (float) ((((double) windowRect.width()) * 1.0d) / 640.0d);
        this.f231a = (float) ((((double) windowRect.height()) * 1.0d) / 960.0d);
        requestWindowFeature(1);
        this.t = this.B.title;
        if (this.B.orientation == 1) {
            setRequestedOrientation(1);
        } else {
            setRequestedOrientation(0);
        }
        this.s = System.currentTimeMillis();
        try {
            if (this.B.isFullScreen) {
                getWindow().setFlags(1024, 1024);
            }
        } catch (Exception e3) {
            this.D.d(o, "exception when getIntent");
        }
        this.e = this.B.from;
        String str = this.B.url;
        if (ac.a(str)) {
            this.D.d(o, "AppActivity.browser external");
            if (ac.i(str)) {
                Intent intent2 = new Intent("android.intent.action.VIEW");
                intent2.setDataAndType(Uri.parse(str), "audio/*");
                startActivity(intent2);
            } else if (ac.h(str)) {
                Intent intent3 = new Intent("android.intent.action.VIEW");
                intent3.setDataAndType(Uri.parse(str), "video/*");
                startActivity(intent3);
            } else {
                m2.browserOutside(this, str);
            }
            finish();
            return;
        }
        this.q = startUrlHandler(this);
        initActionBar();
        b(str);
        a((WebView) this.curWebview, str, false, "http://mobads.baidu.com/" + m2.getAppPackage(this));
        this.d.setBackgroundColor(-1);
        setContentView(this.d);
    }

    private void b(String str) {
        this.d = new RelativeLayout(this);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, m.a().m().getPixel(this, 46));
        layoutParams.addRule(10);
        this.c.setId(1001);
        this.d.addView(this.c, layoutParams);
        RelativeLayout a2 = a(str);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, -1);
        layoutParams2.addRule(3, 1001);
        this.d.addView(a2, layoutParams2);
        if (this.u && canSupportAnimate()) {
            this.d.getViewTreeObserver().addOnPreDrawListener(new p(this));
        }
    }

    /* access modifiers changed from: protected */
    public boolean canSupportAnimate() {
        try {
            return Build.VERSION.SDK_INT >= 12;
        } catch (Exception e2) {
            j.a().e(e2);
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public void initActionBar() {
        this.c = new com.baidu.mobads.g.a(this, J);
        this.c.setId(1001);
        this.c.a(new q(this));
    }

    /* access modifiers changed from: private */
    public void e() {
        f();
        initBottomView();
        this.d.addView(this.C);
        this.d.addView(this.mBottomView);
        if (canSupportAnimate()) {
            this.mBottomView.getViewTreeObserver().addOnPreDrawListener(new r(this));
        }
    }

    @TargetApi(11)
    private void f() {
        this.C = new View(this);
        this.C.setOnClickListener(new s(this));
        if (canSupportAnimate()) {
            this.C.setBackgroundColor(ViewCompat.MEASURED_STATE_MASK);
            this.C.setAlpha(0.0f);
        }
        this.C.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
    }

    /* access modifiers changed from: protected */
    @TargetApi(11)
    public void initBottomView() {
        this.mBottomView = new d(this);
        this.mBottomView.a(new t(this));
        if (canSupportAnimate()) {
            this.mBottomView.setAlpha(0.0f);
        }
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams.addRule(12);
        this.mBottomView.setLayoutParams(layoutParams);
    }

    /* access modifiers changed from: protected */
    @TargetApi(11)
    public void copyCurrentPageUrl() {
        if (Build.VERSION.SDK_INT < 11) {
            Toast.makeText(this, "系统版本不支持", 0).show();
            return;
        }
        try {
            if (!TextUtils.isEmpty(this.curWebview.getUrl())) {
                ((ClipboardManager) getSystemService("clipboard")).setPrimaryClip(ClipData.newPlainText("网页链接", this.curWebview.getUrl()));
                Toast.makeText(this, "已复制到剪切板", 0).show();
            }
        } catch (Exception e2) {
            this.D.e(e2);
        }
    }

    /* access modifiers changed from: private */
    public void g() {
        if (this.curWebview != null) {
            this.curWebview.reload();
        }
    }

    /* access modifiers changed from: protected */
    @TargetApi(12)
    public void runBottomViewEnterAnimation(View view, View view2) {
        this.mBottomViewIsShowing = true;
        try {
            view.animate().setDuration(500).alpha(0.5f);
            view2.setTranslationY((float) view2.getHeight());
            view2.animate().setDuration(500).alpha(1.0f).translationY(0.0f);
        } catch (Exception e2) {
            this.D.e(e2);
        }
    }

    /* access modifiers changed from: protected */
    @TargetApi(16)
    public void runBottomViewExitAnimation(View view, View view2) {
        this.mBottomViewIsShowing = false;
        if (!canSupportAnimate()) {
            b(view2);
            b(view);
            return;
        }
        try {
            view.clearAnimation();
            view.animate().setDuration(250).alpha(0.0f);
            view2.setTranslationY(0.0f);
            ViewPropertyAnimator translationY = view2.animate().setDuration(250).alpha(0.0f).translationY((float) view2.getHeight());
            if (Build.VERSION.SDK_INT >= 16) {
                translationY.withEndAction(new u(this, view2, view));
            } else {
                new Handler(Looper.getMainLooper()).postDelayed(new e(this, view2, view), 250);
            }
        } catch (Exception e2) {
            this.D.e(e2);
        }
    }

    /* access modifiers changed from: private */
    @TargetApi(12)
    public void a(View view) {
        try {
            view.animate().setDuration(700);
            view.setTranslationX((float) view.getWidth());
            view.animate().setDuration(700).translationX(0.0f);
        } catch (Exception e2) {
            this.D.e(e2);
        }
    }

    /* access modifiers changed from: private */
    @TargetApi(16)
    public void h() {
        if (!canSupportAnimate()) {
            finish();
            return;
        }
        try {
            RelativeLayout relativeLayout = this.d;
            if (this.u) {
                relativeLayout.setTranslationX(0.0f);
                ViewPropertyAnimator translationX = relativeLayout.animate().setDuration(300).translationX((float) relativeLayout.getWidth());
                if (Build.VERSION.SDK_INT >= 16) {
                    translationX.withEndAction(new f(this));
                } else {
                    new Handler(Looper.getMainLooper()).postDelayed(new g(this), 300);
                }
            } else {
                finish();
            }
        } catch (Exception e2) {
            this.D.e(e2);
        }
    }

    /* access modifiers changed from: private */
    public void b(View view) {
        if (view != null) {
            try {
                if (view.getParent() != null) {
                    ((ViewGroup) view.getParent()).removeView(view);
                }
            } catch (Exception e2) {
                this.D.e(e2);
            }
        }
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 == 4) {
            try {
                if (this.mBottomViewIsShowing) {
                    runBottomViewExitAnimation(this.C, this.mBottomView);
                } else if (this.curWebview.canGoBack()) {
                    this.curWebview.goBack();
                } else {
                    this.k = "backC";
                    h();
                }
                this.A.G++;
                return true;
            } catch (Exception e2) {
                this.D.d(o, e2.getMessage());
            }
        } else {
            if (i2 == 46) {
                this.curWebview.reload();
                return true;
            }
            return super.onKeyDown(i2, keyEvent);
        }
    }

    public void onDestroy() {
        int i2 = 0;
        super.onDestroy();
        p.set(false);
        if (this.q != null) {
            this.A.n = this.k;
            c cVar = this.A;
            int i3 = this.r;
            this.r = i3 + 1;
            cVar.o = i3;
            this.A.p = this.curWebview != null ? this.curWebview.getContentHeight() : 0;
            c cVar2 = this.A;
            if (this.curWebview != null) {
                i2 = this.curWebview.getProgress();
            }
            cVar2.q = i2;
            this.A.s = this.f;
            this.A.t = this.g;
            this.A.u = System.currentTimeMillis() - this.s;
            this.A.v = this.v;
            this.A.w = this.w;
            this.A.x = this.e;
            this.A.y = this.j;
            Message obtain = Message.obtain();
            obtain.what = G;
            obtain.obj = this.A;
            this.q.sendMessage(obtain);
            try {
                this.curWebview.setVisibility(8);
                this.D.d(o, "onDestroy");
                this.curWebview.stopLoading();
                this.curWebview.destroy();
            } catch (Exception e2) {
                this.D.d(o, e2.getMessage());
            }
        }
        i();
    }

    /* access modifiers changed from: private */
    public void i() {
        if (this.m != null) {
            b(this.m);
            this.m = null;
        }
    }

    /* access modifiers changed from: private */
    public void a(WebView webView, String str, Runnable runnable, Runnable runnable2) {
        try {
            com.baidu.mobads.j.d m2 = m.a().m();
            if (!this.F ? ac.b(str) : ac.a(str)) {
                if (ac.i(str)) {
                    Intent intent = new Intent("android.intent.action.VIEW");
                    intent.setDataAndType(Uri.parse(str), "audio/*");
                    webView.getContext().startActivity(intent);
                } else if (ac.h(str)) {
                    Intent intent2 = new Intent("android.intent.action.VIEW");
                    intent2.setDataAndType(Uri.parse(str), "video/*");
                    webView.getContext().startActivity(intent2);
                } else {
                    m2.browserOutside(webView.getContext(), str);
                }
                if (runnable != null) {
                    runnable.run();
                }
            } else if (runnable2 != null) {
                runnable2.run();
            }
        } catch (Exception e2) {
            this.D.d(o, e2.getMessage());
        }
    }

    /* access modifiers changed from: private */
    public void a(WebView webView, String str, boolean z2, String str2) {
        if (webView != null) {
            com.baidu.mobads.j.d m2 = m.a().m();
            if (this.z) {
                this.f++;
                this.A.r.decrementAndGet();
                this.z = false;
            }
            if (!z2 || !str2.equals("ignore")) {
                try {
                    HashMap hashMap = new HashMap();
                    if (str2.equals("ignore")) {
                        hashMap.put("Referer", "http://mobads.baidu.com/" + m2.getAppPackage(this));
                    } else {
                        hashMap.put("Referer", str2);
                    }
                    this.curWebview.getClass().getMethod("loadUrl", String.class, Map.class).invoke(this.curWebview, str, hashMap);
                } catch (Exception e2) {
                    try {
                        this.curWebview.loadUrl(str);
                    } catch (Exception e3) {
                        this.D.d(o, e2.getMessage());
                    }
                }
            } else {
                try {
                    this.curWebview.loadUrl(str);
                } catch (Exception e4) {
                }
            }
        }
    }

    public Handler startUrlHandler(Context context) {
        this.n.start();
        return new h(this, this.n.getLooper());
    }

    class b {

        /* renamed from: a  reason: collision with root package name */
        boolean f234a = false;
        String b = "";
        boolean c = false;
        boolean d = true;

        b() {
        }
    }

    public void setPageFinishedListener(PageFinishedListener pageFinishedListener) {
        this.H = pageFinishedListener;
    }

    private int j() {
        try {
            Method method = Context.class.getMethod("getThemeResId", new Class[0]);
            method.setAccessible(true);
            return ((Integer) method.invoke(this, new Object[0])).intValue();
        } catch (Exception e2) {
            j.a().e(e2);
            return -1;
        }
    }

    private boolean k() {
        try {
            return 16973840 == j();
        } catch (Exception e2) {
            j.a().e(e2);
            return false;
        }
    }

    public void onPause() {
        super.onPause();
        if (this.I) {
            this.I = false;
            return;
        }
        this.A.H++;
    }

    public static class ActionBarColorTheme {
        public static final ActionBarColorTheme ACTION_BAR_BLACK_THEME = new ActionBarColorTheme(-1, -1, -12510, -13749450);
        public static final ActionBarColorTheme ACTION_BAR_BLUE_THEME = new ActionBarColorTheme(-1, -1, -12510, -13870424);
        public static final ActionBarColorTheme ACTION_BAR_COFFEE_THEME = new ActionBarColorTheme(-1, -1, -12510, -11255230);
        public static final ActionBarColorTheme ACTION_BAR_GREEN_THEME = new ActionBarColorTheme(-1, -1, -11113262, -14303071);
        public static final ActionBarColorTheme ACTION_BAR_NAVYBLUE_THEME = new ActionBarColorTheme(-1, -1, 16764706, -14210226);
        public static final ActionBarColorTheme ACTION_BAR_RED_THEME = new ActionBarColorTheme(-1, -1, -12510, -1294276);
        public static final ActionBarColorTheme ACTION_BAR_WHITE_THEME = new ActionBarColorTheme(-5987164, -6842473, -11113262, -328966);

        /* renamed from: a  reason: collision with root package name */
        private int f232a;
        private int b;
        private int c;
        private int d;

        public ActionBarColorTheme(int i, int i2, int i3, int i4) {
            this.f232a = i;
            this.b = i2;
            this.c = i3;
            this.d = i4;
        }

        public ActionBarColorTheme(ActionBarColorTheme actionBarColorTheme) {
            this.f232a = actionBarColorTheme.f232a;
            this.b = actionBarColorTheme.b;
            this.c = actionBarColorTheme.c;
            this.d = actionBarColorTheme.d;
        }

        public int getCloseColor() {
            return this.f232a;
        }

        public void setCloseColor(int i) {
            this.f232a = i;
        }

        public int getTitleColor() {
            return this.b;
        }

        public void setTitleColor(int i) {
            this.b = i;
        }

        public int getProgressColor() {
            return this.c;
        }

        public void setProgressColor(int i) {
            this.c = i;
        }

        public int getBackgroundColor() {
            return this.d;
        }

        public void setBackgroundColor(int i) {
            this.d = i;
        }

        public boolean equals(Object obj) {
            ActionBarColorTheme actionBarColorTheme = (ActionBarColorTheme) obj;
            return this.d == actionBarColorTheme.d && this.b == actionBarColorTheme.b && this.f232a == actionBarColorTheme.f232a && this.c == actionBarColorTheme.c;
        }
    }

    public static ActionBarColorTheme getActionBarColorTheme() {
        return J;
    }

    public static void setActionBarColorTheme(ActionBarColorTheme actionBarColorTheme) {
        if (actionBarColorTheme != null) {
            J = new ActionBarColorTheme(actionBarColorTheme);
        }
    }
}
