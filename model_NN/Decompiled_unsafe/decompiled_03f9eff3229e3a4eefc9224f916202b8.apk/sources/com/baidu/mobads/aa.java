package com.baidu.mobads;

import com.baidu.mobads.interfaces.event.IXAdEvent;
import com.baidu.mobads.j.m;
import com.baidu.mobads.openad.interfaces.event.IOAdEvent;

class aa implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ IOAdEvent f247a;
    final /* synthetic */ z b;

    aa(z zVar, IOAdEvent iOAdEvent) {
        this.b = zVar;
        this.f247a = iOAdEvent;
    }

    public void run() {
        if (IXAdEvent.AD_LOADED.equals(this.f247a.getType())) {
            this.b.f416a.e.onAdReady();
        } else if (IXAdEvent.AD_ERROR.equals(this.f247a.getType())) {
            this.b.f416a.e.onAdFailed(m.a().q().getMessage(this.f247a.getData()));
        } else if (IXAdEvent.AD_STOPPED.equals(this.f247a.getType())) {
            this.b.f416a.e.onAdDismissed();
        } else if (IXAdEvent.AD_STARTED.equals(this.f247a.getType())) {
            this.b.f416a.e.onAdPresent();
        } else if ("AdUserClick".equals(this.f247a.getType())) {
            this.b.f416a.e.onAdClick(this.b.f416a);
        }
    }
}
