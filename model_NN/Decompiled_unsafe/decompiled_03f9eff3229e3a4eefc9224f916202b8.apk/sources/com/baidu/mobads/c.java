package com.baidu.mobads;

import android.annotation.SuppressLint;
import android.view.KeyEvent;
import com.baidu.mobads.ao;

class c implements ao.a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AdView f273a;

    c(AdView adView) {
        this.f273a = adView;
    }

    public void a(int i) {
        this.f273a.c.a(i);
    }

    public void a(boolean z) {
        this.f273a.c.a(z);
    }

    public void a(int i, int i2) {
        this.f273a.a();
    }

    @SuppressLint({"MissingSuperCall"})
    public void a() {
        this.f273a.c.k();
    }

    public void b() {
        this.f273a.a();
        this.f273a.c.j();
    }

    public boolean a(int i, KeyEvent keyEvent) {
        return this.f273a.c.a(i, keyEvent);
    }
}
