package com.baidu.mobads;

import android.content.Context;
import android.os.Build;
import android.webkit.WebView;

public class af extends WebView {
    public af(Context context) {
        super(context);
        a();
    }

    private void a() {
        try {
            if (Integer.parseInt(Build.VERSION.SDK) >= 11) {
                Class.forName("android.webkit.WebView").getDeclaredMethod("removeJavascriptInterface", String.class).invoke(this, "searchBoxJavaBridge_");
            }
        } catch (Exception e) {
        }
    }
}
