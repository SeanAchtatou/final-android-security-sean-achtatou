package com.baidu.mobads.appoffers.a;

import android.content.Context;
import android.os.Build;
import cn.banshenggua.aichang.utils.StringUtil;
import com.baidu.mobads.interfaces.IXAdRequestInfo;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.jar.JarFile;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

final class e extends Thread {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ File f269a;
    final /* synthetic */ Context b;

    e(File file, Context context) {
        this.f269a = file;
        this.b = context;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.mobads.appoffers.a.b.a(boolean, android.content.Context, java.net.URL, java.lang.String):boolean
     arg types: [int, android.content.Context, java.net.URL, java.lang.String]
     candidates:
      com.baidu.mobads.appoffers.a.b.a(android.content.Context, java.lang.String, int, int):java.net.HttpURLConnection
      com.baidu.mobads.appoffers.a.b.a(boolean, android.content.Context, java.net.URL, java.lang.String):boolean */
    public void run() {
        try {
            c.a("start version check in ", 10, "s");
            sleep(10000);
            c.a(">>>start version check");
            double d = 0.0d;
            try {
                d = Double.parseDouble(new JarFile(this.f269a).getManifest().getMainAttributes().getValue("Implementation-Version"));
            } catch (Exception e) {
                c.c("baidu remote sdk is not ready");
            }
            ArrayList arrayList = new ArrayList();
            arrayList.add(new BasicNameValuePair(IXAdRequestInfo.V, "" + d));
            arrayList.add(new BasicNameValuePair(IXAdRequestInfo.PHONE_TYPE, Build.MODEL));
            arrayList.add(new BasicNameValuePair("os", "android"));
            arrayList.add(new BasicNameValuePair(IXAdRequestInfo.BDR, Build.VERSION.SDK));
            String str = "http://mobads.baidu.com/wall/pa/" + a.f + "?" + URLEncodedUtils.format(arrayList, StringUtil.Encoding);
            c.a("update req url is:", str);
            String a2 = b.a(this.b, str);
            c.a("update ret url is:", a2);
            JSONObject jSONObject = new JSONObject(a2);
            double d2 = jSONObject.getDouble("version");
            String string = jSONObject.getString("url");
            c.a("update serverVersion", Double.valueOf(d2), "localVersion", Double.valueOf(d), "downloadUrl", string);
            if (d2 > d && !"".equals(string)) {
                b.a(false, this.b, new URL(string), this.f269a.getName());
            }
        } catch (Exception e2) {
            boolean unused = d.b = false;
            c.c(e2);
        }
    }
}
