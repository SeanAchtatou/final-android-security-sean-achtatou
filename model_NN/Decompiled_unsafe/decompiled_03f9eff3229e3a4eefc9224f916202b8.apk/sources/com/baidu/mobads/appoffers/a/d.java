package com.baidu.mobads.appoffers.a;

import android.content.Context;
import dalvik.system.DexClassLoader;
import java.io.File;
import java.lang.reflect.Method;
import java.util.jar.JarFile;

public class d {

    /* renamed from: a  reason: collision with root package name */
    public static ClassLoader f268a = null;
    /* access modifiers changed from: private */
    public static boolean b = false;

    public static Class<?> a(Context context, String str) {
        return a(context).loadClass(str);
    }

    public static synchronized ClassLoader a(Context context) {
        double d;
        ClassLoader classLoader;
        double d2 = 0.0d;
        boolean z = true;
        synchronized (d.class) {
            StringBuilder append = new StringBuilder().append("AdUtil.getRemoteClassLoader  ");
            if (f268a != null) {
                z = false;
            }
            c.a(append.append(z).append(" thread: ").append(Thread.currentThread()).toString());
            if (f268a != null) {
                classLoader = f268a;
            } else {
                File file = new File(context.getFilesDir().getAbsolutePath() + "/" + a.d);
                File file2 = new File(context.getFilesDir().getAbsolutePath() + "/" + a.e);
                File file3 = new File(context.getFilesDir().getAbsolutePath() + "/" + a.c + ".tmp.jar");
                b(context);
                try {
                    a(context, "biduad_plugin/" + a.d, file3.getName());
                    d = Double.parseDouble(new JarFile(file3).getManifest().getMainAttributes().getValue("Implementation-Version"));
                } catch (Exception e) {
                    c.b("Baidu-SDK packed is not ok! this will affect the first few shows");
                    d = 0.0d;
                }
                try {
                    if (file.exists()) {
                        d2 = Double.parseDouble(new JarFile(file).getManifest().getMainAttributes().getValue("Implementation-Version"));
                    }
                    c.c(">>>local/cache core", Double.valueOf(d), Double.valueOf(d2));
                    if (d > d2) {
                        file.delete();
                        file3.renameTo(file);
                    }
                    file2.delete();
                    f268a = new DexClassLoader(file.getAbsolutePath(), context.getFilesDir().getAbsolutePath(), null, ClassLoader.getSystemClassLoader());
                    try {
                        Class<?> loadClass = f268a.loadClass("com.baidu.mobads.appoffers.remote.OffersManager");
                        Method declaredMethod = loadClass.getDeclaredMethod("setProxyLoader", ClassLoader.class);
                        declaredMethod.setAccessible(true);
                        declaredMethod.invoke(null, b.class.getClassLoader());
                        Method declaredMethod2 = loadClass.getDeclaredMethod("setPackageName", String.class);
                        declaredMethod2.setAccessible(true);
                        String name = d.class.getPackage().getName();
                        String substring = name.substring(0, name.lastIndexOf("."));
                        c.a("AdUtil.getRemoteClassLoader().getName()", name, substring);
                        declaredMethod2.invoke(null, substring);
                    } catch (Exception e2) {
                        c.c(e2);
                    }
                } catch (Exception e3) {
                    c.c(e3);
                }
                classLoader = f268a;
            }
        }
        return classLoader;
    }

    /* JADX WARN: Type inference failed for: r3v0 */
    /* JADX WARN: Type inference failed for: r3v1, types: [java.io.BufferedOutputStream] */
    /* JADX WARN: Type inference failed for: r3v3, types: [java.io.BufferedInputStream] */
    /* JADX WARN: Type inference failed for: r3v4 */
    /* JADX WARN: Type inference failed for: r3v6 */
    /* JADX WARN: Type inference failed for: r3v9 */
    /* JADX WARN: Type inference failed for: r3v11 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0039 A[SYNTHETIC, Splitter:B:19:0x0039] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x003e A[Catch:{ IOException -> 0x007f }] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0043 A[Catch:{ IOException -> 0x007f }] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0096 A[SYNTHETIC, Splitter:B:42:0x0096] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x009b A[Catch:{ IOException -> 0x00a4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x00a0 A[Catch:{ IOException -> 0x00a4 }] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean a(android.content.Context r11, java.lang.String r12, java.lang.String r13) {
        /*
            r3 = 0
            r9 = 3
            r8 = 2
            r0 = 1
            r1 = 0
            android.content.res.Resources r2 = r11.getResources()     // Catch:{ Exception -> 0x00c6, all -> 0x0091 }
            android.content.res.AssetManager r2 = r2.getAssets()     // Catch:{ Exception -> 0x00c6, all -> 0x0091 }
            java.io.InputStream r5 = r2.open(r12)     // Catch:{ Exception -> 0x00c6, all -> 0x0091 }
            java.io.BufferedInputStream r4 = new java.io.BufferedInputStream     // Catch:{ Exception -> 0x00cb, all -> 0x00b5 }
            r4.<init>(r5)     // Catch:{ Exception -> 0x00cb, all -> 0x00b5 }
            java.io.BufferedOutputStream r2 = new java.io.BufferedOutputStream     // Catch:{ Exception -> 0x00d0, all -> 0x00b8 }
            r6 = 0
            java.io.FileOutputStream r6 = r11.openFileOutput(r13, r6)     // Catch:{ Exception -> 0x00d0, all -> 0x00b8 }
            r2.<init>(r6)     // Catch:{ Exception -> 0x00d0, all -> 0x00b8 }
            r3 = 5120(0x1400, float:7.175E-42)
            byte[] r3 = new byte[r3]     // Catch:{ Exception -> 0x002f, all -> 0x00ba }
        L_0x0024:
            int r6 = r4.read(r3)     // Catch:{ Exception -> 0x002f, all -> 0x00ba }
            if (r6 <= 0) goto L_0x005e
            r7 = 0
            r2.write(r3, r7, r6)     // Catch:{ Exception -> 0x002f, all -> 0x00ba }
            goto L_0x0024
        L_0x002f:
            r3 = move-exception
            r3 = r4
            r4 = r5
        L_0x0032:
            java.lang.String r5 = "assets file writing error"
            com.baidu.mobads.appoffers.a.c.b(r5)     // Catch:{ all -> 0x00bf }
            if (r3 == 0) goto L_0x003c
            r3.close()     // Catch:{ IOException -> 0x007f }
        L_0x003c:
            if (r2 == 0) goto L_0x0041
            r2.close()     // Catch:{ IOException -> 0x007f }
        L_0x0041:
            if (r4 == 0) goto L_0x0046
            r4.close()     // Catch:{ IOException -> 0x007f }
        L_0x0046:
            r0 = r1
        L_0x0047:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "saveAssetsToFile"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r0)
            java.lang.String r1 = r1.toString()
            com.baidu.mobads.appoffers.a.c.a(r1)
            return r0
        L_0x005e:
            if (r4 == 0) goto L_0x0063
            r4.close()     // Catch:{ IOException -> 0x006e }
        L_0x0063:
            if (r2 == 0) goto L_0x0068
            r2.close()     // Catch:{ IOException -> 0x006e }
        L_0x0068:
            if (r5 == 0) goto L_0x0047
            r5.close()     // Catch:{ IOException -> 0x006e }
            goto L_0x0047
        L_0x006e:
            r2 = move-exception
            java.lang.Object[] r3 = new java.lang.Object[r9]
            java.lang.String r4 = "AdUtil.saveAssetsToFile"
            r3[r1] = r4
            java.lang.String r1 = ""
            r3[r0] = r1
            r3[r8] = r2
            com.baidu.mobads.appoffers.a.c.b(r3)
            goto L_0x0047
        L_0x007f:
            r2 = move-exception
            java.lang.Object[] r3 = new java.lang.Object[r9]
            java.lang.String r4 = "AdUtil.saveAssetsToFile"
            r3[r1] = r4
            java.lang.String r4 = ""
            r3[r0] = r4
            r3[r8] = r2
            com.baidu.mobads.appoffers.a.c.b(r3)
            r0 = r1
            goto L_0x0047
        L_0x0091:
            r2 = move-exception
            r4 = r3
            r5 = r3
        L_0x0094:
            if (r4 == 0) goto L_0x0099
            r4.close()     // Catch:{ IOException -> 0x00a4 }
        L_0x0099:
            if (r3 == 0) goto L_0x009e
            r3.close()     // Catch:{ IOException -> 0x00a4 }
        L_0x009e:
            if (r5 == 0) goto L_0x00a3
            r5.close()     // Catch:{ IOException -> 0x00a4 }
        L_0x00a3:
            throw r2
        L_0x00a4:
            r3 = move-exception
            java.lang.Object[] r4 = new java.lang.Object[r9]
            java.lang.String r5 = "AdUtil.saveAssetsToFile"
            r4[r1] = r5
            java.lang.String r1 = ""
            r4[r0] = r1
            r4[r8] = r3
            com.baidu.mobads.appoffers.a.c.b(r4)
            goto L_0x00a3
        L_0x00b5:
            r2 = move-exception
            r4 = r3
            goto L_0x0094
        L_0x00b8:
            r2 = move-exception
            goto L_0x0094
        L_0x00ba:
            r3 = move-exception
            r10 = r3
            r3 = r2
            r2 = r10
            goto L_0x0094
        L_0x00bf:
            r5 = move-exception
            r10 = r5
            r5 = r4
            r4 = r3
            r3 = r2
            r2 = r10
            goto L_0x0094
        L_0x00c6:
            r2 = move-exception
            r2 = r3
            r4 = r3
            goto L_0x0032
        L_0x00cb:
            r2 = move-exception
            r2 = r3
            r4 = r5
            goto L_0x0032
        L_0x00d0:
            r2 = move-exception
            r2 = r3
            r3 = r4
            r4 = r5
            goto L_0x0032
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mobads.appoffers.a.d.a(android.content.Context, java.lang.String, java.lang.String):boolean");
    }

    private static synchronized void b(Context context) {
        synchronized (d.class) {
            if (!b) {
                b = true;
                new e(new File(context.getFilesDir().getAbsolutePath() + "/" + a.d), context).start();
            }
        }
    }
}
