package com.baidu.mobads.appoffers;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import com.baidu.mobads.appoffers.a.c;
import com.baidu.mobads.appoffers.a.d;

public class OffersView extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    private static final String f264a = OffersView.class.getSimpleName();
    private static Class<?> c;
    private ViewGroup b;

    public OffersView(Context context, boolean z) {
        this(context, z, "");
    }

    public OffersView(Context context, boolean z, String str) {
        super(context);
        c.a("create AdView instance");
        try {
            if (c == null) {
                c = d.a(context, "com.baidu.mobads.appoffers.remote.OffersView");
            }
            this.b = (ViewGroup) c.getConstructor(Context.class, Boolean.TYPE, String.class).newInstance(context, Boolean.valueOf(z), str);
            addView(this.b, new ViewGroup.LayoutParams(-1, -1));
            c.a(c, this.b);
        } catch (Exception e) {
            c.a(e);
        }
    }
}
