package com.baidu.mobads.appoffers;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import com.baidu.mobads.appoffers.a.c;
import com.baidu.mobads.appoffers.a.d;
import java.lang.reflect.Method;

public class OffersActivity extends Activity {

    /* renamed from: a  reason: collision with root package name */
    private Object f262a;
    private Class<?> b;
    private Method[] c = null;

    private Method a(String str) {
        if (this.c == null) {
            return null;
        }
        for (Method method : this.c) {
            if (method.getName().equals(str)) {
                method.setAccessible(true);
                return method;
            }
        }
        return null;
    }

    private void a(String str, Object... objArr) {
        int i = 0;
        try {
            Object[] objArr2 = new Object[3];
            objArr2[0] = str;
            if (objArr != null) {
                i = objArr.length;
            }
            objArr2[1] = Integer.valueOf(i);
            objArr2[2] = objArr;
            c.a(objArr2);
            Method a2 = a(str);
            if (a2 == null) {
                return;
            }
            if (objArr == null || objArr.length == 0) {
                a2.invoke(this.f262a, new Object[0]);
            } else {
                a2.invoke(this.f262a, objArr);
            }
        } catch (Exception e) {
            c.a(e);
        }
    }

    private boolean b(String str, Object... objArr) {
        try {
            Object[] objArr2 = new Object[3];
            objArr2[0] = str;
            objArr2[1] = Integer.valueOf(objArr != null ? objArr.length : 0);
            objArr2[2] = objArr;
            c.a(objArr2);
            Method a2 = a(str);
            if (a2 != null) {
                return (objArr == null || objArr.length == 0) ? ((Boolean) a2.invoke(this.f262a, new Object[0])).booleanValue() : ((Boolean) a2.invoke(this.f262a, objArr)).booleanValue();
            }
        } catch (Exception e) {
            c.a(e);
        }
        return false;
    }

    private Object c(String str, Object... objArr) {
        int i = 0;
        try {
            Object[] objArr2 = new Object[3];
            objArr2[0] = str;
            if (objArr != null) {
                i = objArr.length;
            }
            objArr2[1] = Integer.valueOf(i);
            objArr2[2] = objArr;
            c.a(objArr2);
            Method a2 = a(str);
            if (a2 != null) {
                return (objArr == null || objArr.length == 0) ? a2.invoke(this.f262a, new Object[0]) : a2.invoke(this.f262a, objArr);
            }
        } catch (Exception e) {
            c.a(e);
        }
        return null;
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        if (b("dispatchKeyEvent", keyEvent)) {
            return true;
        }
        return super.dispatchKeyEvent(keyEvent);
    }

    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        if (b("dispatchTouchEvent", motionEvent)) {
            return true;
        }
        return super.dispatchTouchEvent(motionEvent);
    }

    public boolean dispatchTrackballEvent(MotionEvent motionEvent) {
        if (b("dispatchTrackballEvent", motionEvent)) {
            return true;
        }
        return super.dispatchTrackballEvent(motionEvent);
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        a("finalize", new Object[0]);
        super.finalize();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        a("onActivityResult", Integer.valueOf(i), Integer.valueOf(i2), intent);
        super.onActivityResult(i, i2, intent);
    }

    /* access modifiers changed from: protected */
    public void onApplyThemeResource(Resources.Theme theme, int i, boolean z) {
        a("onApplyThemeResource", theme, Integer.valueOf(i), Boolean.valueOf(z));
        super.onApplyThemeResource(theme, i, z);
    }

    /* access modifiers changed from: protected */
    public void onChildTitleChanged(Activity activity, CharSequence charSequence) {
        a("onChildTitleChanged", activity, charSequence);
        super.onChildTitleChanged(activity, charSequence);
    }

    public void onConfigurationChanged(Configuration configuration) {
        a("onConfigurationChanged", configuration);
        super.onConfigurationChanged(configuration);
    }

    public void onContentChanged() {
        a("onContentChanged", new Object[0]);
        super.onContentChanged();
    }

    public boolean onContextItemSelected(MenuItem menuItem) {
        if (b("onContextItemSelected", menuItem)) {
            return true;
        }
        return super.onContextItemSelected(menuItem);
    }

    public void onContextMenuClosed(Menu menu) {
        a("onContextMenuClosed", menu);
        super.onContextMenuClosed(menu);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        try {
            String string = getIntent().getExtras().getString("remote_activity");
            this.b = d.a(this, string);
            this.c = this.b.getDeclaredMethods();
            this.f262a = this.b.getConstructor(Activity.class).newInstance(this);
            c.a(string, this.b, this.f262a);
        } catch (Exception e) {
            c.a(e);
        }
        a("onCreate", bundle);
    }

    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        super.onCreateContextMenu(contextMenu, view, contextMenuInfo);
        a("onCreateContextMenu", contextMenu, view, contextMenuInfo);
    }

    public CharSequence onCreateDescription() {
        CharSequence charSequence = (CharSequence) c("onCreateDescription", new Object[0]);
        return charSequence != null ? charSequence : super.onCreateDescription();
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i) {
        Dialog dialog = (Dialog) c("onCreateDialog", Integer.valueOf(i));
        return dialog != null ? dialog : super.onCreateDialog(i);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        if (b("onCreateOptionsMenu", menu)) {
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onCreatePanelMenu(int i, Menu menu) {
        if (b("onCreatePanelMenu", Integer.valueOf(i), menu)) {
            return true;
        }
        return super.onCreatePanelMenu(i, menu);
    }

    public View onCreatePanelView(int i) {
        View view = (View) c("onCreatePanelView", Integer.valueOf(i));
        return view != null ? view : super.onCreatePanelView(i);
    }

    public boolean onCreateThumbnail(Bitmap bitmap, Canvas canvas) {
        if (b("onCreateThumbnail", bitmap, canvas)) {
            return true;
        }
        return super.onCreateThumbnail(bitmap, canvas);
    }

    public View onCreateView(String str, Context context, AttributeSet attributeSet) {
        View view = (View) c("onCreateView", str, context, attributeSet);
        return view != null ? view : super.onCreateView(str, context, attributeSet);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        a("onDestroy", new Object[0]);
        super.onDestroy();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (b("onKeyDown", Integer.valueOf(i), keyEvent)) {
            return true;
        }
        return super.onKeyDown(i, keyEvent);
    }

    public boolean onKeyMultiple(int i, int i2, KeyEvent keyEvent) {
        if (b("onKeyMultiple", Integer.valueOf(i), Integer.valueOf(i2), keyEvent)) {
            return true;
        }
        return super.onKeyMultiple(i, i2, keyEvent);
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        if (b("onKeyUp", Integer.valueOf(i), keyEvent)) {
            return true;
        }
        return super.onKeyUp(i, keyEvent);
    }

    public void onLowMemory() {
        a("onLowMemory", new Object[0]);
        super.onLowMemory();
    }

    public boolean onMenuItemSelected(int i, MenuItem menuItem) {
        if (b("onMenuItemSelected", Integer.valueOf(i), menuItem)) {
            return true;
        }
        return super.onMenuItemSelected(i, menuItem);
    }

    public boolean onMenuOpened(int i, Menu menu) {
        if (b("onMenuOpened", Integer.valueOf(i), menu)) {
            return true;
        }
        return super.onMenuOpened(i, menu);
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        a("onNewIntent", intent);
        super.onNewIntent(intent);
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (b("onOptionsItemSelected", menuItem)) {
            return true;
        }
        return super.onOptionsItemSelected(menuItem);
    }

    public void onOptionsMenuClosed(Menu menu) {
        a("onOptionsMenuClosed", menu);
        super.onOptionsMenuClosed(menu);
    }

    public void onPanelClosed(int i, Menu menu) {
        a("onPanelClosed", Integer.valueOf(i), menu);
        super.onPanelClosed(i, menu);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        a("onPause", new Object[0]);
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onPostCreate(Bundle bundle) {
        super.onPostCreate(bundle);
        a("onPostCreate", bundle);
    }

    /* access modifiers changed from: protected */
    public void onPostResume() {
        super.onPostResume();
        a("onPostResume", new Object[0]);
    }

    /* access modifiers changed from: protected */
    public void onPrepareDialog(int i, Dialog dialog) {
        super.onPrepareDialog(i, dialog);
        a("onPrepareDialog", Integer.valueOf(i), dialog);
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        if (b("onPrepareOptionsMenu", menu)) {
            return true;
        }
        return super.onPrepareOptionsMenu(menu);
    }

    public boolean onPreparePanel(int i, View view, Menu menu) {
        if (b("onPreparePanel", Integer.valueOf(i), view, menu)) {
            return true;
        }
        return super.onPreparePanel(i, view, menu);
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
        a("onRestart", new Object[0]);
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
        a("onRestoreInstanceState", bundle);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        a("onResume", new Object[0]);
    }

    public Object onRetainNonConfigurationInstance() {
        Object c2 = c("onRetainNonConfigurationInstance", new Object[0]);
        return c2 != null ? c2 : super.onRetainNonConfigurationInstance();
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        a("onSaveInstanceState", bundle);
    }

    public boolean onSearchRequested() {
        if (b("onSearchRequested", new Object[0])) {
            return true;
        }
        return super.onSearchRequested();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        a("onStart", new Object[0]);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        a("onStop", new Object[0]);
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onTitleChanged(CharSequence charSequence, int i) {
        super.onTitleChanged(charSequence, i);
        a("onTitleChanged", charSequence, Integer.valueOf(i));
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (b("onTouchEvent", motionEvent)) {
            return true;
        }
        return super.onTouchEvent(motionEvent);
    }

    public boolean onTrackballEvent(MotionEvent motionEvent) {
        if (b("onTrackballEvent", motionEvent)) {
            return true;
        }
        return super.onTrackballEvent(motionEvent);
    }

    public void onUserInteraction() {
        super.onUserInteraction();
        a("onUserInteraction", new Object[0]);
    }

    /* access modifiers changed from: protected */
    public void onUserLeaveHint() {
        super.onUserLeaveHint();
        a("onUserLeaveHint", new Object[0]);
    }

    public void onWindowAttributesChanged(WindowManager.LayoutParams layoutParams) {
        super.onWindowAttributesChanged(layoutParams);
        a("onWindowAttributesChanged", layoutParams);
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        a("onWindowFocusChanged", Boolean.valueOf(z));
    }
}
