package com.baidu.mobads.appoffers;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import com.baidu.mobads.appoffers.a.c;

class a implements Handler.Callback {

    /* renamed from: a  reason: collision with root package name */
    private final PointsUpdateListener f265a;

    protected a(PointsUpdateListener pointsUpdateListener) {
        this.f265a = pointsUpdateListener;
    }

    private String a(Bundle bundle) {
        try {
            return bundle.getString("method_param");
        } catch (Exception e) {
            c.a(e);
            return "";
        }
    }

    public boolean handleMessage(Message message) {
        try {
            Bundle data = message.getData();
            String string = data.getString("method");
            c.a("handleMessage", data);
            if ("onPointsUpdateSuccess".equals(string)) {
                try {
                    this.f265a.onPointsUpdateSuccess(Integer.valueOf(a(data)).intValue());
                } catch (Exception e) {
                    this.f265a.onPointsUpdateFailed("initernal error proxy-1");
                    c.b(e);
                }
                return false;
            }
            if ("onPointsUpdateFailed".equals(string)) {
                try {
                    this.f265a.onPointsUpdateFailed(a(data));
                } catch (Exception e2) {
                    this.f265a.onPointsUpdateFailed("initernal error proxy-2");
                    c.b(e2);
                }
            }
            return false;
        } catch (Exception e3) {
            c.a(e3);
        }
    }
}
