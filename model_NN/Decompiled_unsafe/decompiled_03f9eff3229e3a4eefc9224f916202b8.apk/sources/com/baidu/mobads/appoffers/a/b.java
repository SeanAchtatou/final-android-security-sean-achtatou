package com.baidu.mobads.appoffers.a;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.net.URLConnection;

public final class b {

    /* renamed from: a  reason: collision with root package name */
    private static final Proxy f267a = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("10.0.0.172", 80));
    private static final Proxy b = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("10.0.0.200", 80));

    public static String a(Context context, String str) {
        return b(context, str, 40000, 60000);
    }

    public static HttpURLConnection a(Context context, String str, int i, int i2) {
        HttpURLConnection httpURLConnection;
        URL url = new URL(str);
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        NetworkInfo networkInfo = connectivityManager.getNetworkInfo(0);
        NetworkInfo networkInfo2 = connectivityManager.getNetworkInfo(1);
        if (networkInfo2 != null && networkInfo2.isAvailable()) {
            c.a("", "WIFI is available");
            httpURLConnection = (HttpURLConnection) url.openConnection();
        } else if (networkInfo == null || !networkInfo.isAvailable()) {
            httpURLConnection = (HttpURLConnection) url.openConnection();
        } else {
            String extraInfo = networkInfo.getExtraInfo();
            String lowerCase = extraInfo != null ? extraInfo.toLowerCase() : "";
            c.a("current APN", lowerCase);
            httpURLConnection = (lowerCase.startsWith("cmwap") || lowerCase.startsWith("uniwap") || lowerCase.startsWith("3gwap")) ? (HttpURLConnection) url.openConnection(f267a) : lowerCase.startsWith("ctwap") ? (HttpURLConnection) url.openConnection(b) : (HttpURLConnection) url.openConnection();
        }
        httpURLConnection.setConnectTimeout(i);
        httpURLConnection.setReadTimeout(i2);
        return httpURLConnection;
    }

    private static HttpURLConnection a(Context context, URL url) {
        return b(context, url.toString());
    }

    public static boolean a(String str) {
        c.a("AdUtil.deleteExt", str);
        if (!"mounted".equals(Environment.getExternalStorageState())) {
            return false;
        }
        File file = new File(Environment.getExternalStorageDirectory() + File.separator + str);
        if (file.exists()) {
            return file.delete();
        }
        return false;
    }

    public static boolean a(boolean z, Context context, URL url, String str) {
        BufferedOutputStream bufferedOutputStream;
        File file;
        File file2;
        boolean z2;
        c.a("AdUtil.save", String.format("[%s] %s", str, url.toString()));
        String str2 = str + ".tm";
        HttpURLConnection a2 = a(context, url);
        a2.connect();
        BufferedInputStream bufferedInputStream = new BufferedInputStream(a2.getInputStream());
        if (!z) {
            File filesDir = context.getFilesDir();
            if (str2.contains("/")) {
                if (!filesDir.exists()) {
                    filesDir.mkdirs();
                }
                bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(filesDir + File.separator + str2));
            } else {
                bufferedOutputStream = new BufferedOutputStream(context.openFileOutput(str2, 1));
            }
            file = new File(filesDir + File.separator + str2);
            file2 = new File(filesDir + File.separator + str);
            c.a("AdUtil.save", "localfile", file2);
        } else if (!"mounted".equals(Environment.getExternalStorageState())) {
            return false;
        } else {
            File externalStorageDirectory = Environment.getExternalStorageDirectory();
            bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(externalStorageDirectory + File.separator + str2));
            file = new File(externalStorageDirectory + File.separator + str2);
            file2 = new File(externalStorageDirectory + File.separator + str);
        }
        byte[] bArr = new byte[5120];
        while (true) {
            int read = bufferedInputStream.read(bArr);
            if (read <= 0) {
                break;
            }
            bufferedOutputStream.write(bArr, 0, read);
        }
        if (bufferedInputStream != null) {
            bufferedInputStream.close();
        }
        if (bufferedOutputStream != null) {
            bufferedOutputStream.close();
        }
        if (file == null || !file.exists()) {
            c.a("AdUtil.save", "tmFile not exists");
            z2 = false;
        } else if (file.renameTo(file2)) {
            c.a("AdUtil.save", "rename success");
            z2 = true;
        } else {
            c.a("AdUtil.save", "rename failed");
            z2 = false;
        }
        return z2;
    }

    public static String b(Context context, String str, int i, int i2) {
        if (str.startsWith("file:///")) {
            return c(context, str);
        }
        StringBuilder sb = new StringBuilder();
        HttpURLConnection a2 = a(context, str, i, i2);
        a2.connect();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(a2.getInputStream()));
        while (true) {
            String readLine = bufferedReader.readLine();
            if (readLine == null) {
                break;
            }
            sb.append(readLine);
        }
        c.a("AdUtil.get", String.format("Header: %d/%d %d %s \t%s", Integer.valueOf(sb.length()), Integer.valueOf(a2.getContentLength()), Integer.valueOf(a2.getResponseCode()), a2.getResponseMessage(), sb));
        if (bufferedReader != null) {
            bufferedReader.close();
        }
        if (a2.getContentLength() < 0) {
            sb = new StringBuilder("{error}");
        }
        return sb.toString();
    }

    private static HttpURLConnection b(Context context, String str) {
        return a(context, str, 40000, 60000);
    }

    private static String c(Context context, String str) {
        StringBuilder sb = new StringBuilder();
        int indexOf = str.indexOf(63);
        if (indexOf >= 0) {
            str = str.substring(0, indexOf);
        }
        URLConnection openConnection = new URL(str).openConnection();
        openConnection.connect();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(openConnection.getInputStream()));
        while (true) {
            String readLine = bufferedReader.readLine();
            if (readLine == null) {
                break;
            }
            sb.append(readLine);
        }
        if (bufferedReader != null) {
            bufferedReader.close();
        }
        return sb.toString();
    }
}
