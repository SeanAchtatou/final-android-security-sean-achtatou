package com.baidu.mobads.appoffers;

public interface PointsUpdateListener {
    void onPointsUpdateFailed(String str);

    void onPointsUpdateSuccess(int i);
}
