package com.baidu.mobads.appoffers.a;

import android.util.Log;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

public final class c {
    static {
        a();
    }

    public static int a(String str) {
        if (!a(3)) {
            return -1;
        }
        a("Mobads SDK", str);
        return Log.d("Mobads SDK", str);
    }

    public static int a(String str, Throwable th) {
        if (!a(3)) {
            return -1;
        }
        a("Mobads SDK", str, th);
        return Log.d("Mobads SDK", str, th);
    }

    public static int a(Throwable th) {
        return a("", th);
    }

    public static int a(Object... objArr) {
        if (!a(3)) {
            return -1;
        }
        return a(d(objArr));
    }

    public static void a() {
        b.a("_b_sdk.log");
    }

    private static synchronized void a(String str, String str2) {
        synchronized (c.class) {
        }
    }

    private static void a(String str, String str2, Throwable th) {
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        th.printStackTrace(printWriter);
        a(str, str2 + "\n" + stringWriter.toString());
        printWriter.close();
        try {
            stringWriter.close();
        } catch (IOException e) {
            Log.w("Log.debug", "", e);
        }
    }

    public static boolean a(int i) {
        return a("Mobads SDK", i);
    }

    public static boolean a(String str, int i) {
        return i >= 4;
    }

    public static int b(String str) {
        if (!a(5)) {
            return -1;
        }
        a("Mobads SDK", str);
        return Log.w("Mobads SDK", str);
    }

    public static int b(String str, Throwable th) {
        if (!a(5)) {
            return -1;
        }
        a("Mobads SDK", str, th);
        return Log.w("Mobads SDK", str, th);
    }

    public static int b(Throwable th) {
        return b("", th);
    }

    public static int b(Object... objArr) {
        if (!a(6)) {
            return -1;
        }
        return c(d(objArr));
    }

    public static int c(String str) {
        if (!a(6)) {
            return -1;
        }
        a("Mobads SDK", str);
        return Log.e("Mobads SDK", str);
    }

    public static int c(String str, Throwable th) {
        if (!a(6)) {
            return -1;
        }
        a("Mobads SDK", str, th);
        return Log.e("Mobads SDK", str, th);
    }

    public static int c(Throwable th) {
        return c("", th);
    }

    public static int c(Object... objArr) {
        if (!a(4)) {
            return -1;
        }
        return d(d(objArr));
    }

    public static int d(String str) {
        if (!a(4)) {
            return -1;
        }
        a("Mobads SDK", str);
        return Log.i("Mobads SDK", str);
    }

    private static String d(Object[] objArr) {
        StringBuilder sb = new StringBuilder();
        for (Object append : objArr) {
            sb.append(append).append(' ');
        }
        return sb.toString();
    }
}
