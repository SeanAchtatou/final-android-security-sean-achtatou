package com.baidu.mobads.appoffers;

import android.content.Context;
import com.baidu.mobads.appoffers.a.c;
import com.baidu.mobads.appoffers.a.d;
import java.lang.reflect.Method;

public final class OffersManager {
    protected static final String P_VERSION = "3.0";

    /* renamed from: a  reason: collision with root package name */
    private static Method[] f263a = null;
    protected static String appChannel;

    private static Method a(Context context, String str) {
        try {
            if (f263a == null) {
                f263a = d.a(context, "com.baidu.mobads.appoffers.remote.OffersManager").getDeclaredMethods();
            }
            for (Method method : f263a) {
                if (method.getName().equals(str)) {
                    method.setAccessible(true);
                    return method;
                }
            }
        } catch (Exception e) {
            c.c(e);
        }
        c.c("proxy OffersManager getRemoteMethodExisted return null");
        return null;
    }

    private static void a(Context context, String str, Object... objArr) {
        int i = 0;
        try {
            Object[] objArr2 = new Object[3];
            objArr2[0] = str;
            if (objArr != null) {
                i = objArr.length;
            }
            objArr2[1] = Integer.valueOf(i);
            objArr2[2] = objArr;
            c.a(objArr2);
            Method a2 = a(context, str);
            if (a2 == null) {
                c.c("method is not exist: " + str);
            } else if (objArr == null || objArr.length == 0) {
                a2.invoke(null, new Object[0]);
            } else {
                a2.invoke(null, objArr);
            }
        } catch (Exception e) {
            c.a(e);
        }
    }

    public static void addPoints(Context context, int i) {
        a(context, "addPoints", context, Integer.valueOf(i));
    }

    private static Object b(Context context, String str, Object... objArr) {
        int i = 0;
        try {
            Object[] objArr2 = new Object[3];
            objArr2[0] = str;
            if (objArr != null) {
                i = objArr.length;
            }
            objArr2[1] = Integer.valueOf(i);
            objArr2[2] = objArr;
            c.a(objArr2);
            Method a2 = a(context, str);
            if (a2 != null) {
                return (objArr == null || objArr.length == 0) ? a2.invoke(null, new Object[0]) : a2.invoke(null, objArr);
            }
            return null;
        } catch (Exception e) {
            c.a(e);
            return null;
        }
    }

    private static String c(Context context, String str, Object... objArr) {
        try {
            return (String) b(context, str, objArr);
        } catch (Exception e) {
            c.a(e);
            return "";
        }
    }

    public static String getCurrencyName(Context context) {
        return c(context, "getCurrencyName", context);
    }

    public static void getPoints(Context context) {
        a(context, "getPoints", context);
    }

    public static void setAppSid(Context context, String str) {
        a(context, "setAppSid", str);
    }

    public static void setPointsUpdateListener(Context context, PointsUpdateListener pointsUpdateListener) {
        a(context, "setPointsUpdateListener", new a(pointsUpdateListener));
    }

    public static void setUserName(Context context, String str) {
        a(context, "setUserName", str);
    }

    public static void showOffers(Context context) {
        a(context, "showOffers", context);
    }

    public static void showOffersWithPlaceId(Context context, String str) {
        a(context, "showOffersWithPlaceId", context, str);
    }

    public static void subPoints(Context context, int i) {
        a(context, "subPoints", context, Integer.valueOf(i));
    }
}
