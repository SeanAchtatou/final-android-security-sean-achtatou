package com.baidu.mobads.command;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Process;
import com.baidu.mobads.interfaces.IXAdRequestInfo;
import com.baidu.mobads.interfaces.utils.IXAdCommonUtils;
import com.baidu.mobads.interfaces.utils.IXAdLogger;
import com.baidu.mobads.j.m;
import com.baidu.mobads.openad.interfaces.download.IOAdDownloader;
import com.tencent.stat.DeviceInfo;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;

public class a implements Serializable {
    private boolean A;

    /* renamed from: a  reason: collision with root package name */
    public String f277a;
    public String b;
    public String c;
    public long d = -1;
    public int e = 0;
    public int f;
    public IOAdDownloader.DownloadStatus g = IOAdDownloader.DownloadStatus.NONE;
    public Object h = null;
    public String i;
    public String j;
    public String k;
    public boolean l;
    public String m;
    public String n;
    public String o;
    public String p;
    public int q = 0;
    public boolean r = false;
    public long s;
    protected long t;
    protected long u;
    public boolean v = false;
    public String w = null;
    protected final IXAdLogger x = m.a().f();
    private long y;
    private long z;

    public a(String str, String str2) {
        this.i = str;
        this.f277a = str2;
    }

    public void a(String str, String str2, String str3, boolean z2) {
        this.m = str;
        this.n = str2;
        this.j = str3;
        this.l = z2;
    }

    public void a(String str, String str2) {
        this.b = str;
        this.c = str2;
    }

    public void b(String str, String str2) {
        this.o = str;
        this.p = str2;
    }

    public JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put(IXAdCommonUtils.PKGS_PREF_DOWNLOAD_STATUS, this.g.getCode());
            jSONObject.put("filename", this.b);
            jSONObject.put("folder", this.c);
            jSONObject.put("title", this.f277a);
            jSONObject.put("contentLength", this.d);
            jSONObject.put(IXAdRequestInfo.PACKAGE, this.i);
            jSONObject.put("qk", this.m);
            jSONObject.put("autoOpen", this.l);
            jSONObject.put("adid", this.n);
            jSONObject.put("placeId", this.o);
            jSONObject.put("prod", this.p);
            jSONObject.put("dlTunnel", 4);
            if (this.k == null || this.k.length() <= 0) {
                jSONObject.put("url", this.j);
            } else {
                jSONObject.put("turl", this.k);
            }
            jSONObject.put("mnCfm", this.r);
            jSONObject.put("dlCnt", this.q);
            jSONObject.put("cts", this.s);
            if (this.q == 1) {
                this.t = System.currentTimeMillis();
                this.u = (long) Process.myPid();
            }
            jSONObject.put(DeviceInfo.TAG_TIMESTAMPS, this.t);
            jSONObject.put("clickProcId", this.u);
        } catch (Exception e2) {
            this.x.d(e2);
        }
        return jSONObject;
    }

    public static String b() {
        return m.a().n().getCurrentProcessName(m.a().d());
    }

    public static a a(Context context, String str) {
        a aVar = null;
        if (str == null || "".equals(str)) {
            return null;
        }
        try {
            String string = context.getSharedPreferences(IXAdCommonUtils.PKGS_PREF_DOWNLOAD, 0).getString(str + "#$#" + b(), null);
            if (string == null) {
                return null;
            }
            JSONObject jSONObject = new JSONObject(string);
            String string2 = jSONObject.getString("title");
            String optString = jSONObject.optString("url", jSONObject.getString("turl"));
            a aVar2 = new a(str, string2);
            try {
                aVar2.a(jSONObject.optString("qk"), jSONObject.optString("adid"), optString, jSONObject.optBoolean("autoOpen"));
                aVar2.a(jSONObject.getString("filename"), jSONObject.getString("folder"));
                aVar2.b(jSONObject.optString("placeId"), jSONObject.optString("prod"));
                int i2 = jSONObject.getInt(IXAdCommonUtils.PKGS_PREF_DOWNLOAD_STATUS);
                IOAdDownloader.DownloadStatus[] values = IOAdDownloader.DownloadStatus.values();
                IOAdDownloader.DownloadStatus downloadStatus = IOAdDownloader.DownloadStatus.NONE;
                for (int i3 = 0; i3 < values.length; i3++) {
                    if (values[i3].getCode() == i2) {
                        downloadStatus = values[i3];
                    }
                }
                aVar2.g = downloadStatus;
                aVar2.r = jSONObject.optBoolean("mnCfm");
                aVar2.q = jSONObject.getInt("dlCnt");
                aVar2.s = jSONObject.optLong("cts");
                aVar2.t = jSONObject.optLong(DeviceInfo.TAG_TIMESTAMPS);
                aVar2.u = (long) jSONObject.optInt("clickProcId");
                return aVar2;
            } catch (Exception e2) {
                Exception exc = e2;
                aVar = aVar2;
                e = exc;
                m.a().f().d(e);
                com.baidu.mobads.c.a.a().a("get stored download info failed: " + e.toString());
                return aVar;
            }
        } catch (Exception e3) {
            e = e3;
            m.a().f().d(e);
            com.baidu.mobads.c.a.a().a("get stored download info failed: " + e.toString());
            return aVar;
        }
    }

    public static List<String> a(Context context, long j2) {
        int i2;
        ArrayList arrayList = new ArrayList();
        try {
            for (Map.Entry next : context.getSharedPreferences(IXAdCommonUtils.PKGS_PREF_DOWNLOAD, 0).getAll().entrySet()) {
                try {
                    String b2 = b();
                    String str = (String) next.getKey();
                    if (str.contains("#$#" + b2)) {
                        JSONObject jSONObject = new JSONObject((String) next.getValue());
                        if (jSONObject.getLong("cts") >= j2 && ((i2 = jSONObject.getInt(IXAdCommonUtils.PKGS_PREF_DOWNLOAD_STATUS)) == 0 || i2 == 1 || i2 == 4)) {
                            arrayList.add(str.substring(0, str.indexOf("#$#")));
                        }
                    }
                } catch (Exception e2) {
                    m.a().f().d("XAdDownloaderManager", e2.getMessage());
                }
            }
        } catch (Exception e3) {
            m.a().f().d(e3);
        }
        return arrayList;
    }

    public long c() {
        return this.y;
    }

    public void a(long j2) {
        this.y = j2;
    }

    public String d() {
        return this.i;
    }

    public long e() {
        return this.z;
    }

    public boolean f() {
        return this.A;
    }

    public void a(boolean z2) {
        this.A = z2;
    }

    public void b(long j2) {
        this.z = j2;
    }

    @TargetApi(9)
    public void a(Context context) {
        if (this.i != null && !"".equals(this.i)) {
            try {
                SharedPreferences.Editor edit = context.getSharedPreferences(IXAdCommonUtils.PKGS_PREF_DOWNLOAD, 0).edit();
                edit.putString(this.i + "#$#" + b(), a().toString());
                if (Build.VERSION.SDK_INT >= 9) {
                    edit.apply();
                } else {
                    edit.commit();
                }
            } catch (Exception e2) {
                m.a().f().d("XAdAPKDownloadExtraInfo", e2);
            }
        }
    }

    public String g() {
        return this.n;
    }

    public String h() {
        return this.m;
    }

    public String i() {
        return this.p;
    }
}
