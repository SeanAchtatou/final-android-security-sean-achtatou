package com.baidu.mobads.command.a;

import android.content.Context;
import android.widget.Toast;
import com.baidu.mobads.command.b;
import com.baidu.mobads.interfaces.IXAdContainerContext;
import com.baidu.mobads.interfaces.IXAdContainerFactory;
import com.baidu.mobads.interfaces.IXAdInstanceInfo;
import com.baidu.mobads.interfaces.IXAdResource;
import com.baidu.mobads.interfaces.IXNonLinearAdSlot;
import com.baidu.mobads.interfaces.download.activate.IXAppInfo;
import com.baidu.mobads.interfaces.utils.IXAdIOUtils;
import com.baidu.mobads.interfaces.utils.IXAdSystemUtils;
import com.baidu.mobads.interfaces.utils.IXAdURIUitls;
import com.baidu.mobads.j.d;
import com.baidu.mobads.j.m;
import com.baidu.mobads.openad.interfaces.download.IOAdDownloader;
import java.io.File;
import java.net.URL;

public class a extends b {
    public a(IXNonLinearAdSlot iXNonLinearAdSlot, IXAdInstanceInfo iXAdInstanceInfo, IXAdResource iXAdResource) {
        super(iXNonLinearAdSlot, iXAdInstanceInfo, iXAdResource);
    }

    public void a() {
        String str;
        com.baidu.mobads.command.a aVar;
        String str2;
        com.baidu.mobads.command.a aVar2;
        boolean z = false;
        d m = m.a().m();
        IXAdIOUtils k = m.a().k();
        IXAdURIUitls i = m.a().i();
        IXAdSystemUtils n = m.a().n();
        IXAdContainerContext adContainerContext = this.b.getCurrentXAdContainer().getAdContainerContext();
        try {
            String appPackageName = this.c.getAppPackageName();
            this.e.i("XAdDownloadAPKCommand", "download pkg = " + appPackageName);
            if (appPackageName == null || appPackageName.equals("")) {
                this.e.i("XAdDownloadAPKCommand", "start to download but package is empty");
                str = m.getMD5(this.c.getOriginClickUrl());
            } else {
                str = appPackageName;
            }
            IOAdDownloader adsApkDownloader = com.baidu.mobads.openad.c.d.a(this.f278a).getAdsApkDownloader(str);
            com.baidu.mobads.openad.c.b a2 = com.baidu.mobads.openad.c.b.a(str);
            if (a2 == null || adsApkDownloader == null) {
                if (adsApkDownloader != null) {
                    adsApkDownloader.cancel();
                    adsApkDownloader.removeObservers();
                }
                com.baidu.mobads.openad.c.b.b(str);
                com.baidu.mobads.openad.c.d.a(this.f278a).removeAdsApkDownloader(str);
                aVar = null;
            } else {
                com.baidu.mobads.command.a a3 = a2.a();
                IOAdDownloader.DownloadStatus state = adsApkDownloader.getState();
                this.e.d("XAdDownloadAPKCommand", "startDownload>> downloader exist: state=" + state);
                if (state == IOAdDownloader.DownloadStatus.CANCELLED || state == IOAdDownloader.DownloadStatus.ERROR || state == IOAdDownloader.DownloadStatus.PAUSED) {
                    adsApkDownloader.resume();
                    i.pintHttpInNewThread(this.c.getClickThroughUrl());
                    return;
                }
                if (state == IOAdDownloader.DownloadStatus.COMPLETED) {
                    if (a(this.f278a, a3)) {
                        i.pintHttpInNewThread(this.c.getClickThroughUrl());
                        b(a3);
                        return;
                    }
                    adsApkDownloader.cancel();
                    adsApkDownloader.removeObservers();
                    com.baidu.mobads.openad.c.b.b(str);
                    com.baidu.mobads.openad.c.d.a(this.f278a).removeAdsApkDownloader(str);
                } else if (state == IOAdDownloader.DownloadStatus.DOWNLOADING || state == IOAdDownloader.DownloadStatus.INITING) {
                    Toast.makeText(this.f278a, adsApkDownloader.getTitle() + adsApkDownloader.getState().getMessage(), 0).show();
                    return;
                }
                aVar = a3;
            }
            com.baidu.mobads.command.a a4 = com.baidu.mobads.command.a.a(this.f278a, str);
            if (a4 != null) {
                if (a4.g != IOAdDownloader.DownloadStatus.COMPLETED || !a(this.f278a, a4)) {
                    i.pintHttpInNewThread(this.c.getClickThroughUrl());
                    aVar2 = a4;
                } else {
                    b(a4);
                    return;
                }
            } else if (b()) {
                m.a().l().openApp(this.f278a, this.c.getAppPackageName());
                i.pintHttpInNewThread(this.c.getClickThroughUrl());
                b(aVar);
                return;
            } else {
                String appName = this.c.getAppName();
                if ((appName == null || appName.equals("")) && ((appName = this.c.getTitle()) == null || appName.equals(""))) {
                    str2 = "您点击的应用";
                } else {
                    str2 = appName;
                }
                com.baidu.mobads.command.a aVar3 = new com.baidu.mobads.command.a(str, str2);
                aVar3.a(this.c.getQueryKey(), this.c.getAdId(), this.c.getClickThroughUrl(), this.c.isAutoOpen());
                aVar3.a(m.getMD5(aVar3.j) + ".apk", k.getStoreagePath(this.f278a));
                aVar3.b(this.b.getAdRequestInfo().getApid(), this.b.getProdInfo().getProdType());
                aVar3.f = com.baidu.mobads.openad.c.b.c(str);
                if (!this.c.isActionOnlyWifi()) {
                    z = true;
                }
                aVar3.r = z;
                aVar3.a(System.currentTimeMillis());
                aVar3.b(this.c.getAppSize());
                aVar3.a(this.c.isTooLarge());
                aVar2 = aVar3;
            }
            aVar2.s = System.currentTimeMillis();
            IOAdDownloader createAdsApkDownloader = adContainerContext.getDownloaderManager(this.f278a).createAdsApkDownloader(new URL(aVar2.j), aVar2.c, aVar2.b, 3, aVar2.f277a, aVar2.i);
            if (this.c.getAPOOpen() && this.c.getPage() != null && !this.c.getPage().equals("")) {
                aVar2.v = true;
                aVar2.w = this.c.getPage();
            }
            createAdsApkDownloader.addObserver(new com.baidu.mobads.openad.c.b(this.f278a, aVar2));
            if (aVar2.r || !n.is3GConnected(this.f278a).booleanValue()) {
                createAdsApkDownloader.start();
                return;
            }
            createAdsApkDownloader.pause();
            Toast.makeText(this.f278a, createAdsApkDownloader.getTitle() + " 将在连入Wifi后开始下载", 0).show();
        } catch (Exception e) {
            this.e.e("XAdDownloadAPKCommand", e);
            com.baidu.mobads.c.a.a().a("ad app download failed: " + e.toString());
        }
    }

    private boolean b() {
        return m.a().l().isInstalled(this.f278a, this.c.getAppPackageName());
    }

    /* access modifiers changed from: protected */
    public boolean a(Context context, com.baidu.mobads.command.a aVar) {
        if (m.a().l().isInstalled(context, aVar.i)) {
            m.a().l().openApp(context, aVar.i);
            return true;
        }
        String str = aVar.c + aVar.b;
        File file = new File(str);
        if (!file.exists() || file.length() <= 0) {
            return false;
        }
        context.startActivity(m.a().l().getInstallIntent(str));
        return true;
    }

    private void b(com.baidu.mobads.command.a aVar) {
        if (com.baidu.mobads.production.a.b() != null) {
            IXAppInfo a2 = a(aVar);
            if (a2 != null) {
                com.baidu.mobads.production.a.b().getXMonitorActivation(this.f278a, this.e).addAppInfoForMonitor(a2);
            } else {
                this.e.e("addAppInfoForMonitor error, appInfo is null");
            }
        }
    }

    public static IXAppInfo a(com.baidu.mobads.command.a aVar) {
        IXAdContainerFactory b;
        if (aVar == null || (b = com.baidu.mobads.production.a.b()) == null) {
            return null;
        }
        IXAppInfo createAppInfo = b.createAppInfo();
        createAppInfo.setAdId(aVar.g());
        createAppInfo.setAppSize(aVar.e());
        createAppInfo.setClickTime(aVar.c());
        createAppInfo.setPackageName(aVar.d());
        createAppInfo.setQk(aVar.h());
        createAppInfo.setProd(aVar.i());
        createAppInfo.setTooLarge(aVar.f());
        return createAppInfo;
    }
}
