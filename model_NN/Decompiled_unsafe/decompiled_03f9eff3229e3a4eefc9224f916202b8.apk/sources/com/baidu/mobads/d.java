package com.baidu.mobads;

import android.webkit.WebChromeClient;
import android.webkit.WebView;
import com.baidu.mobads.AppActivity;

class d extends WebChromeClient {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppActivity.a f279a;
    final /* synthetic */ AppActivity b;

    d(AppActivity appActivity, AppActivity.a aVar) {
        this.b = appActivity;
        this.f279a = aVar;
    }

    public void onReceivedTitle(WebView webView, String str) {
        super.onReceivedTitle(webView, str);
        if (this.b.c != null) {
            this.b.c.a(str);
        }
    }

    public void onProgressChanged(WebView webView, int i) {
        if (this.f279a != null) {
            this.f279a.a(i);
            if (i > 50) {
                this.b.i();
                this.f279a.setVisibility(i >= 100 ? 4 : 0);
            }
        }
    }
}
