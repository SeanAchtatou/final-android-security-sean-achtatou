package com.baidu.mobads;

import android.app.Activity;
import com.baidu.mobads.j.m;
import com.baidu.mobads.production.e.b;

public class DubaoAd {

    /* renamed from: a  reason: collision with root package name */
    private b f235a = null;

    public DubaoAd(Activity activity, String str, Position position) {
        if (position == null) {
            m.a().f().e("parameter position can not be null");
        } else {
            this.f235a = new b(activity, str, position.getmLeftOrRight() == 0, position.getmTopMarginPercent());
        }
    }

    public void destroy() {
        if (this.f235a != null) {
            this.f235a.l();
            this.f235a = null;
        }
    }

    public static class Position {
        public static final int POSITION_LEFT = 0;
        public static final int POSITION_RIGHT = 1;

        /* renamed from: a  reason: collision with root package name */
        private int f236a;
        private double b;

        public Position(int i, double d) {
            this.f236a = i;
            this.b = d;
        }

        public int getmLeftOrRight() {
            return this.f236a;
        }

        public double getmTopMarginPercent() {
            return this.b;
        }
    }
}
