package com.baidu.mobads;

import android.content.Context;
import android.view.ViewGroup;
import com.baidu.mobads.j.m;

public class AdService {
    protected static String channelId = "";
    protected static int instanceCount = -1;

    /* renamed from: a  reason: collision with root package name */
    private AdView f224a;

    public static void setChannelId(String str) {
        channelId = str;
        m.a().m().setChannelId(str);
    }

    public AdService(Context context, ViewGroup viewGroup, ViewGroup.LayoutParams layoutParams, AdViewListener adViewListener) {
        this(context, viewGroup, layoutParams, adViewListener, AdSize.Banner, "");
    }

    public AdService(Context context, ViewGroup viewGroup, ViewGroup.LayoutParams layoutParams, AdViewListener adViewListener, AdSize adSize, String str) {
        if (context == null || viewGroup == null || layoutParams == null || adViewListener == null || adSize == null) {
            throw new IllegalArgumentException("One of arguments is null");
        }
        this.f224a = new AdView(context, false, adSize, str);
        this.f224a.setListener(adViewListener);
        a(viewGroup, layoutParams);
        instanceCount++;
    }

    private void a(ViewGroup viewGroup, ViewGroup.LayoutParams layoutParams) {
        try {
            if (this.f224a.getParent() != viewGroup) {
                if (this.f224a.getParent() != null) {
                    ((ViewGroup) this.f224a.getParent()).removeView(this.f224a);
                }
                viewGroup.addView(this.f224a, layoutParams);
            }
        } catch (Exception e) {
            m.a().f().d(e);
        }
    }

    public void destroy() {
        if (this.f224a != null) {
            this.f224a.destroy();
            this.f224a = null;
        }
    }
}
