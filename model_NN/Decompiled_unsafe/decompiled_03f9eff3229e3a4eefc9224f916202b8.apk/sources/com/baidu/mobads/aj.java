package com.baidu.mobads;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.KeyEvent;
import com.baidu.mobads.ao;
import com.baidu.mobads.interfaces.error.XAdErrorCode;
import com.baidu.mobads.interfaces.event.IXAdEvent;
import com.baidu.mobads.j.m;
import com.baidu.mobads.production.h.a;

class aj implements ao.a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Context f255a;
    final /* synthetic */ ao b;
    final /* synthetic */ String c;
    final /* synthetic */ boolean d;
    final /* synthetic */ SplashAd e;

    aj(SplashAd splashAd, Context context, ao aoVar, String str, boolean z) {
        this.e = splashAd;
        this.f255a = context;
        this.b = aoVar;
        this.c = str;
        this.d = z;
    }

    public void a(int i) {
        if (this.e.f238a != null) {
            this.e.f238a.a(i);
        }
    }

    public void a(boolean z) {
        if (this.e.f238a != null) {
            this.e.f238a.a(z);
        }
    }

    public void a(int i, int i2) {
        if (this.e.f238a == null) {
            float screenDensity = m.a().m().getScreenDensity(this.f255a);
            if (((float) i) < 200.0f * screenDensity || ((float) i2) < screenDensity * 150.0f) {
                m.a().f().e(m.a().q().genCompleteErrorMessage(XAdErrorCode.SHOW_STANDARD_UNFIT, "开屏显示区域太小,宽度至少200dp,高度至少150dp"));
                this.e.c.onAdDismissed();
                return;
            }
            a unused = this.e.f238a = new a(this.f255a, this.b, this.c, this.d, i, i2);
            this.e.f238a.addEventListener("AdUserClick", this.e.d);
            this.e.f238a.addEventListener(IXAdEvent.AD_LOADED, this.e.d);
            this.e.f238a.addEventListener(IXAdEvent.AD_STARTED, this.e.d);
            this.e.f238a.addEventListener(IXAdEvent.AD_STOPPED, this.e.d);
            this.e.f238a.addEventListener(IXAdEvent.AD_ERROR, this.e.d);
            this.e.f238a.request();
        }
    }

    @SuppressLint({"MissingSuperCall"})
    public void a() {
        if (this.e.f238a != null) {
            this.e.f238a.k();
        }
    }

    public void b() {
        if (this.e.f238a != null) {
            this.e.f238a.j();
        }
    }

    public boolean a(int i, KeyEvent keyEvent) {
        return false;
    }
}
