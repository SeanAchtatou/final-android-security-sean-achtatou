package com.baidu.mobads;

import android.view.ViewTreeObserver;

class r implements ViewTreeObserver.OnPreDrawListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppActivity f403a;

    r(AppActivity appActivity) {
        this.f403a = appActivity;
    }

    public boolean onPreDraw() {
        this.f403a.mBottomView.getViewTreeObserver().removeOnPreDrawListener(this);
        this.f403a.runBottomViewEnterAnimation(this.f403a.C, this.f403a.mBottomView);
        return true;
    }
}
