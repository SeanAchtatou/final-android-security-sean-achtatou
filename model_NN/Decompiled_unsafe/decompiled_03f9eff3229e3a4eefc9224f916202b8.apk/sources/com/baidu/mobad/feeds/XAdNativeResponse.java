package com.baidu.mobad.feeds;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.webkit.WebView;
import com.baidu.mobad.feeds.NativeResponse;
import com.baidu.mobads.interfaces.IXAdContainer;
import com.baidu.mobads.interfaces.IXAdInstanceInfo;
import com.baidu.mobads.interfaces.feeds.IXAdFeedsRequestParameters;
import com.baidu.mobads.j.m;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;

public class XAdNativeResponse implements NativeResponse {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public IXAdInstanceInfo f209a;
    /* access modifiers changed from: private */
    public BaiduNative b;
    private boolean c = false;
    /* access modifiers changed from: private */
    public IXAdFeedsRequestParameters d;
    private IXAdContainer e;

    public XAdNativeResponse(IXAdInstanceInfo iXAdInstanceInfo, BaiduNative baiduNative, IXAdFeedsRequestParameters iXAdFeedsRequestParameters, IXAdContainer iXAdContainer) {
        this.f209a = iXAdInstanceInfo;
        this.b = baiduNative;
        this.e = iXAdContainer;
        if (this.f209a.getActionType() == m.a().p().getActTypeDownload()) {
            this.c = true;
        }
        this.d = iXAdFeedsRequestParameters;
    }

    public String getAdLogoUrl() {
        return "https://cpro.baidustatic.com/cpro/ui/noexpire/img/mob_adicon.png";
    }

    public String getBaiduLogoUrl() {
        return "https://cpro.baidustatic.com/cpro/ui/noexpire/img/2.0.1/bd-logo4.png";
    }

    public String getTitle() {
        return this.f209a.getTitle();
    }

    public String getDesc() {
        return this.f209a.getDescription();
    }

    public String getIconUrl() {
        String iconUrl = this.f209a.getIconUrl();
        if (iconUrl == null || iconUrl.equals("")) {
            return this.f209a.getMainPictureUrl();
        }
        return iconUrl;
    }

    public String getImageUrl() {
        return this.f209a.getMainPictureUrl();
    }

    public boolean isDownloadApp() {
        return this.c;
    }

    public void setIsDownloadApp(boolean z) {
        this.c = z;
    }

    public boolean isAdAvailable(Context context) {
        return this.b.isAdAvailable(context, this.f209a, this.d);
    }

    public long getAppSize() {
        return this.f209a.getAppSize();
    }

    public String getAppPackage() {
        return this.f209a.getAppPackageName();
    }

    public List<String> getMultiPicUrls() {
        try {
            JSONArray optJSONArray = this.f209a.getOriginJsonObject().optJSONArray("morepics");
            if (optJSONArray == null || optJSONArray.length() <= 0) {
                return null;
            }
            ArrayList arrayList = new ArrayList();
            int i = 0;
            while (i < optJSONArray.length()) {
                try {
                    arrayList.add(optJSONArray.getString(i));
                    i++;
                } catch (Exception e2) {
                    return arrayList;
                }
            }
            return arrayList;
        } catch (Exception e3) {
            return null;
        }
    }

    public Map<String, String> getExtras() {
        return null;
    }

    public void recordImpression(View view) {
        this.b.recordImpression(view, this.f209a, this.d);
    }

    public void handleClick(View view) {
        handleClick(view, -1);
    }

    /* access modifiers changed from: private */
    public void a(Context context) {
        if (m.a().n().is3GConnected(context).booleanValue()) {
            this.f209a.setActionOnlyWifi(false);
        } else {
            this.f209a.setActionOnlyWifi(true);
        }
    }

    private void a(final View view, final int i) {
        final Context context = view.getContext();
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("确认下载\"" + getTitle() + "\"?");
        builder.setTitle("提示");
        builder.setPositiveButton("确认", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                XAdNativeResponse.this.a(context);
                XAdNativeResponse.this.b.handleClick(view, XAdNativeResponse.this.f209a, i, XAdNativeResponse.this.d);
            }
        });
        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.create().show();
    }

    public void handleClick(View view, int i) {
        if (isDownloadApp()) {
            Context context = view.getContext();
            if (this.d.getAPPConfirmPolicy() == 3) {
                this.f209a.setActionOnlyWifi(false);
                this.b.handleClick(view, this.f209a, i, this.d);
            } else if (this.d.getAPPConfirmPolicy() == 4) {
                a(context);
                this.b.handleClick(view, this.f209a, i, this.d);
            } else if (this.d.getAPPConfirmPolicy() == 2) {
                a(view, i);
            } else if (this.d.getAPPConfirmPolicy() != 1) {
            } else {
                if (m.a().n().is3GConnected(context).booleanValue()) {
                    a(view, i);
                    return;
                }
                a(context);
                this.b.handleClick(view, this.f209a, i, this.d);
            }
        } else {
            this.b.handleClick(view, this.f209a, i, this.d);
        }
    }

    public void onStart(Context context) {
        this.b.handleOnStart(context, this.f209a, this.d);
    }

    public void onError(Context context, int i, int i2) {
        this.b.handleOnError(context, i, i2, this.f209a);
    }

    public void onComplete(Context context) {
        this.b.handleOnComplete(context, this.f209a, this.d);
    }

    public void onClose(Context context, int i) {
        this.b.handleOnClose(context, i, this.f209a, this.d);
    }

    public void onFullScreen(Context context, int i) {
        this.b.handleOnFullScreen(context, i, this.f209a, this.d);
    }

    public String getVideoUrl() {
        return this.f209a.getVideoUrl();
    }

    public int getDuration() {
        return this.f209a.getVideoDuration();
    }

    public NativeResponse.MaterialType getMaterialType() {
        if (this.f209a.getCreativeType() == IXAdInstanceInfo.CreativeType.VIDEO) {
            return NativeResponse.MaterialType.VIDEO;
        }
        if (this.f209a.getCreativeType() == IXAdInstanceInfo.CreativeType.HTML) {
            return NativeResponse.MaterialType.HTML;
        }
        return NativeResponse.MaterialType.NORMAL;
    }

    public String getHtmlSnippet() {
        return this.f209a.getHtmlSnippet();
    }

    public WebView getWebView() {
        return (WebView) this.e.getAdView();
    }

    public void onClickAd(Context context) {
        this.b.handleOnClickAd(context, this.f209a, this.d);
    }

    public int getMainPicWidth() {
        return this.f209a.getMainMaterialWidth();
    }

    public int getMainPicHeight() {
        return this.f209a.getMainMaterialHeight();
    }

    public String getBrandName() {
        return this.f209a.getAppName();
    }
}
