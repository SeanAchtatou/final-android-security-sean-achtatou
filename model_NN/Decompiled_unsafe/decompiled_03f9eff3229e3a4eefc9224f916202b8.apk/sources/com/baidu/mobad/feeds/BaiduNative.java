package com.baidu.mobad.feeds;

import android.content.Context;
import android.view.View;
import com.baidu.mobad.feeds.RequestParameters;
import com.baidu.mobads.h.q;
import com.baidu.mobads.interfaces.IXAdInstanceInfo;
import com.baidu.mobads.interfaces.event.IXAdEvent;
import com.baidu.mobads.interfaces.feeds.IXAdFeedsRequestParameters;
import com.baidu.mobads.j.m;
import com.baidu.mobads.openad.interfaces.event.IOAdEventListener;
import com.baidu.mobads.production.d.a;
import java.util.List;

public class BaiduNative {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final Context f201a;
    private final String b;
    /* access modifiers changed from: private */
    public a c;
    /* access modifiers changed from: private */
    public BaiduNativeNetworkListener d;
    private BaiduNativeEventListener e;

    public interface BaiduNativeEventListener {
        void onClicked();

        void onImpressionSended();
    }

    public interface BaiduNativeNetworkListener {
        void onNativeFail(NativeErrorCode nativeErrorCode);

        void onNativeLoad(List<NativeResponse> list);
    }

    class CustomIOAdEventListener implements IOAdEventListener {
        private IXAdFeedsRequestParameters b;

        public CustomIOAdEventListener(IXAdFeedsRequestParameters iXAdFeedsRequestParameters) {
            this.b = iXAdFeedsRequestParameters;
        }

        /* JADX WARNING: Removed duplicated region for block: B:18:0x007c  */
        /* JADX WARNING: Removed duplicated region for block: B:35:0x0097 A[SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run(com.baidu.mobads.openad.interfaces.event.IOAdEvent r13) {
            /*
                r12 = this;
                r4 = 1
                r2 = 0
                java.lang.String r0 = "AdStarted"
                java.lang.String r1 = r13.getType()
                boolean r0 = r0.equals(r1)
                if (r0 == 0) goto L_0x00c5
                com.baidu.mobad.feeds.BaiduNative r0 = com.baidu.mobad.feeds.BaiduNative.this
                com.baidu.mobads.production.d.a r0 = r0.c
                r0.removeAllListeners()
                com.baidu.mobad.feeds.BaiduNative r0 = com.baidu.mobad.feeds.BaiduNative.this
                com.baidu.mobad.feeds.BaiduNative$BaiduNativeNetworkListener r0 = r0.d
                if (r0 == 0) goto L_0x00c5
                java.util.ArrayList r6 = new java.util.ArrayList
                r6.<init>()
                java.util.HashSet r7 = new java.util.HashSet
                r7.<init>()
                com.baidu.mobads.j.m r0 = com.baidu.mobads.j.m.a()
                com.baidu.mobads.interfaces.utils.IXAdConstants r8 = r0.p()
                r1 = r2
            L_0x0032:
                com.baidu.mobad.feeds.BaiduNative r0 = com.baidu.mobad.feeds.BaiduNative.this
                com.baidu.mobads.production.d.a r0 = r0.c
                java.util.ArrayList r0 = r0.m()
                int r0 = r0.size()
                if (r1 >= r0) goto L_0x00b5
                com.baidu.mobad.feeds.BaiduNative r0 = com.baidu.mobad.feeds.BaiduNative.this
                com.baidu.mobads.production.d.a r0 = r0.c
                java.util.ArrayList r0 = r0.m()
                java.lang.Object r0 = r0.get(r1)
                com.baidu.mobads.interfaces.IXAdInstanceInfo r0 = (com.baidu.mobads.interfaces.IXAdInstanceInfo) r0
                java.lang.String r3 = r0.getAppPackageName()
                int r5 = r0.getActionType()
                int r9 = r8.getActTypeDownload()
                if (r5 != r9) goto L_0x00ff
                if (r3 == 0) goto L_0x0078
                java.lang.String r5 = ""
                boolean r5 = r3.equals(r5)
                if (r5 != 0) goto L_0x0078
                java.lang.String r5 = "null"
                boolean r5 = r3.equals(r5)
                if (r5 != 0) goto L_0x0078
                boolean r5 = r7.contains(r3)
                if (r5 == 0) goto L_0x009b
            L_0x0078:
                r3 = r2
                r5 = r4
            L_0x007a:
                if (r5 != 0) goto L_0x0097
                com.baidu.mobad.feeds.XAdNativeResponse r5 = new com.baidu.mobad.feeds.XAdNativeResponse
                com.baidu.mobad.feeds.BaiduNative r9 = com.baidu.mobad.feeds.BaiduNative.this
                com.baidu.mobads.interfaces.feeds.IXAdFeedsRequestParameters r10 = r12.b
                com.baidu.mobad.feeds.BaiduNative r11 = com.baidu.mobad.feeds.BaiduNative.this
                com.baidu.mobads.production.d.a r11 = r11.c
                com.baidu.mobads.interfaces.IXAdContainer r11 = r11.getCurrentXAdContainer()
                r5.<init>(r0, r9, r10, r11)
                if (r3 != r4) goto L_0x0094
                r5.setIsDownloadApp(r2)
            L_0x0094:
                r6.add(r5)
            L_0x0097:
                int r0 = r1 + 1
                r1 = r0
                goto L_0x0032
            L_0x009b:
                r7.add(r3)
                com.baidu.mobads.j.m r5 = com.baidu.mobads.j.m.a()
                com.baidu.mobads.interfaces.utils.IXAdPackageUtils r5 = r5.l()
                com.baidu.mobad.feeds.BaiduNative r9 = com.baidu.mobad.feeds.BaiduNative.this
                android.content.Context r9 = r9.f201a
                boolean r3 = r5.isInstalled(r9, r3)
                if (r3 == 0) goto L_0x00ff
                r3 = r4
                r5 = r2
                goto L_0x007a
            L_0x00b5:
                com.baidu.mobads.j.m r0 = com.baidu.mobads.j.m.a()
                com.baidu.mobads.j.d r0 = r0.m()
                com.baidu.mobad.feeds.BaiduNative$CustomIOAdEventListener$1 r1 = new com.baidu.mobad.feeds.BaiduNative$CustomIOAdEventListener$1
                r1.<init>(r6)
                r0.a(r1)
            L_0x00c5:
                java.lang.String r0 = "AdError"
                java.lang.String r1 = r13.getType()
                boolean r0 = r0.equals(r1)
                if (r0 == 0) goto L_0x00fe
                com.baidu.mobad.feeds.BaiduNative r0 = com.baidu.mobad.feeds.BaiduNative.this
                com.baidu.mobads.production.d.a r0 = r0.c
                r0.removeAllListeners()
                java.util.Map r0 = r13.getData()
                java.lang.String r1 = "message"
                java.lang.Object r0 = r0.get(r1)
                java.lang.String r0 = (java.lang.String) r0
                com.baidu.mobad.feeds.BaiduNative r0 = com.baidu.mobad.feeds.BaiduNative.this
                com.baidu.mobad.feeds.BaiduNative$BaiduNativeNetworkListener r0 = r0.d
                if (r0 == 0) goto L_0x00fe
                com.baidu.mobads.j.m r0 = com.baidu.mobads.j.m.a()
                com.baidu.mobads.j.d r0 = r0.m()
                com.baidu.mobad.feeds.BaiduNative$CustomIOAdEventListener$2 r1 = new com.baidu.mobad.feeds.BaiduNative$CustomIOAdEventListener$2
                r1.<init>()
                r0.a(r1)
            L_0x00fe:
                return
            L_0x00ff:
                r3 = r2
                r5 = r2
                goto L_0x007a
            */
            throw new UnsupportedOperationException("Method not decompiled: com.baidu.mobad.feeds.BaiduNative.CustomIOAdEventListener.run(com.baidu.mobads.openad.interfaces.event.IOAdEvent):void");
        }
    }

    public BaiduNative(Context context, String str, BaiduNativeNetworkListener baiduNativeNetworkListener) {
        this(context, str, baiduNativeNetworkListener, new a(context, str));
    }

    public BaiduNative(Context context, String str, BaiduNativeNetworkListener baiduNativeNetworkListener, a aVar) {
        this.f201a = context;
        m.a().a(context.getApplicationContext());
        this.b = str;
        this.d = baiduNativeNetworkListener;
        q.a(context).a();
        this.c = aVar;
    }

    public void destroy() {
    }

    @Deprecated
    public void setNativeEventListener(BaiduNativeEventListener baiduNativeEventListener) {
        this.e = baiduNativeEventListener;
    }

    public void makeRequest() {
        makeRequest(null);
    }

    public void makeRequest(RequestParameters requestParameters) {
        if (requestParameters == null) {
            requestParameters = new RequestParameters.Builder().build();
        }
        requestParameters.mPlacementId = this.b;
        CustomIOAdEventListener customIOAdEventListener = new CustomIOAdEventListener(requestParameters);
        this.c.addEventListener(IXAdEvent.AD_STARTED, customIOAdEventListener);
        this.c.addEventListener(IXAdEvent.AD_ERROR, customIOAdEventListener);
        this.c.a(requestParameters);
        this.c.request();
    }

    /* access modifiers changed from: protected */
    public void recordImpression(View view, IXAdInstanceInfo iXAdInstanceInfo, IXAdFeedsRequestParameters iXAdFeedsRequestParameters) {
        this.c.a(view, iXAdInstanceInfo, iXAdFeedsRequestParameters);
    }

    /* access modifiers changed from: protected */
    public boolean isAdAvailable(Context context, IXAdInstanceInfo iXAdInstanceInfo, IXAdFeedsRequestParameters iXAdFeedsRequestParameters) {
        return this.c.a(context, iXAdInstanceInfo, iXAdFeedsRequestParameters);
    }

    /* access modifiers changed from: protected */
    public void handleClick(View view, IXAdInstanceInfo iXAdInstanceInfo, IXAdFeedsRequestParameters iXAdFeedsRequestParameters) {
        this.c.b(view, iXAdInstanceInfo, iXAdFeedsRequestParameters);
    }

    /* access modifiers changed from: protected */
    public void handleClick(View view, IXAdInstanceInfo iXAdInstanceInfo, int i, IXAdFeedsRequestParameters iXAdFeedsRequestParameters) {
        this.c.a(view, iXAdInstanceInfo, i, iXAdFeedsRequestParameters);
    }

    /* access modifiers changed from: protected */
    public void handleOnStart(Context context, IXAdInstanceInfo iXAdInstanceInfo, IXAdFeedsRequestParameters iXAdFeedsRequestParameters) {
        this.c.b(context, iXAdInstanceInfo, iXAdFeedsRequestParameters);
    }

    /* access modifiers changed from: protected */
    public void handleOnError(Context context, int i, int i2, IXAdInstanceInfo iXAdInstanceInfo) {
        this.c.a(context, i, i2, iXAdInstanceInfo);
    }

    /* access modifiers changed from: protected */
    public void handleOnComplete(Context context, IXAdInstanceInfo iXAdInstanceInfo, IXAdFeedsRequestParameters iXAdFeedsRequestParameters) {
        this.c.c(context, iXAdInstanceInfo, iXAdFeedsRequestParameters);
    }

    /* access modifiers changed from: protected */
    public void handleOnClose(Context context, int i, IXAdInstanceInfo iXAdInstanceInfo, IXAdFeedsRequestParameters iXAdFeedsRequestParameters) {
        this.c.a(context, i, iXAdInstanceInfo, iXAdFeedsRequestParameters);
    }

    /* access modifiers changed from: protected */
    public void handleOnClickAd(Context context, IXAdInstanceInfo iXAdInstanceInfo, IXAdFeedsRequestParameters iXAdFeedsRequestParameters) {
        this.c.d(context, iXAdInstanceInfo, iXAdFeedsRequestParameters);
    }

    /* access modifiers changed from: protected */
    public void handleOnFullScreen(Context context, int i, IXAdInstanceInfo iXAdInstanceInfo, IXAdFeedsRequestParameters iXAdFeedsRequestParameters) {
        this.c.b(context, i, iXAdInstanceInfo, iXAdFeedsRequestParameters);
    }

    public static void setAppSid(Context context, String str) {
        m.a().m().setAppId(str);
    }
}
