package com.baidu.mobad.nativevideo;

import android.content.Context;
import android.view.View;
import com.baidu.mobad.feeds.BaiduNative;
import com.baidu.mobad.feeds.NativeResponse;
import com.baidu.mobad.feeds.XAdNativeResponse;
import com.baidu.mobad.nativevideo.BaiduVideoResponse;
import com.baidu.mobads.interfaces.IXAdContainer;
import com.baidu.mobads.interfaces.IXAdInstanceInfo;
import com.baidu.mobads.interfaces.feeds.IXAdFeedsRequestParameters;

class XAdVideoResponse implements BaiduVideoResponse {

    /* renamed from: a  reason: collision with root package name */
    NativeResponse f214a;

    public XAdVideoResponse(IXAdInstanceInfo iXAdInstanceInfo, BaiduNative baiduNative, IXAdFeedsRequestParameters iXAdFeedsRequestParameters, IXAdContainer iXAdContainer) {
        this.f214a = new XAdNativeResponse(iXAdInstanceInfo, baiduNative, iXAdFeedsRequestParameters, iXAdContainer);
    }

    public XAdVideoResponse(NativeResponse nativeResponse) {
        this.f214a = nativeResponse;
    }

    public void recordImpression(View view) {
        this.f214a.recordImpression(view);
    }

    public void handleClick(View view) {
        this.f214a.handleClick(view);
    }

    public void handleClick(View view, int i) {
        this.f214a.handleClick(view, i);
    }

    public void onStart(Context context) {
        this.f214a.onStart(context);
    }

    public void onError(Context context, int i, int i2) {
        this.f214a.onError(context, i, i2);
    }

    public void onComplete(Context context) {
        this.f214a.onComplete(context);
    }

    public void onClose(Context context, int i) {
        this.f214a.onClose(context, i);
    }

    public void onClickAd(Context context) {
        this.f214a.onClickAd(context);
    }

    public void onFullScreen(Context context, int i) {
        this.f214a.onFullScreen(context, i);
    }

    public String getVideoUrl() {
        return this.f214a.getVideoUrl();
    }

    public int getDuration() {
        return this.f214a.getDuration();
    }

    public BaiduVideoResponse.PrerollMaterialType getMaterialType() {
        BaiduVideoResponse.PrerollMaterialType prerollMaterialType = BaiduVideoResponse.PrerollMaterialType.NORMAL;
        switch (this.f214a.getMaterialType()) {
            case VIDEO:
                return BaiduVideoResponse.PrerollMaterialType.VIDEO;
            case NORMAL:
                if (this.f214a.getImageUrl().endsWith(".gif")) {
                    return BaiduVideoResponse.PrerollMaterialType.GIF;
                }
                return prerollMaterialType;
            default:
                return prerollMaterialType;
        }
    }

    public String getImageUrl() {
        return this.f214a.getImageUrl();
    }

    public boolean isDownloadApp() {
        return this.f214a.isDownloadApp();
    }

    public String getAdLogoUrl() {
        return this.f214a.getAdLogoUrl();
    }

    public String getBaiduLogoUrl() {
        return this.f214a.getBaiduLogoUrl();
    }
}
