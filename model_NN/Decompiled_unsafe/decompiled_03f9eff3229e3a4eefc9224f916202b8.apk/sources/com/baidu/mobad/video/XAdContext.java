package com.baidu.mobad.video;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.os.Handler;
import android.widget.RelativeLayout;
import com.baidu.mobads.interfaces.IXAdConstants4PDK;
import com.baidu.mobads.interfaces.IXAdContext;
import com.baidu.mobads.interfaces.IXAdInstanceInfo;
import com.baidu.mobads.interfaces.IXAdInternalConstants;
import com.baidu.mobads.interfaces.IXAdManager;
import com.baidu.mobads.interfaces.IXAdProd;
import com.baidu.mobads.interfaces.IXLinearAdSlot;
import com.baidu.mobads.interfaces.event.IXAdEvent;
import com.baidu.mobads.interfaces.utils.IXAdLogger;
import com.baidu.mobads.interfaces.utils.IXAdURIUitls;
import com.baidu.mobads.j.m;
import com.baidu.mobads.openad.d.b;
import com.baidu.mobads.openad.d.c;
import com.baidu.mobads.openad.e.a;
import com.baidu.mobads.openad.e.d;
import com.baidu.mobads.openad.interfaces.event.IOAdEvent;
import com.baidu.mobads.openad.interfaces.event.IOAdEventDispatcher;
import com.baidu.mobads.openad.interfaces.event.IOAdEventListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class XAdContext implements IXAdContext {
    public static final String TAG = "XAdContext";

    /* renamed from: a  reason: collision with root package name */
    int f216a = 0;
    int b = 0;
    private HashMap<String, Object> c = new HashMap<>();
    private IXAdConstants4PDK.ScreenSizeMode d = IXAdConstants4PDK.ScreenSizeMode.FULL_SCREEN;
    private IXAdConstants4PDK.VideoState e = IXAdConstants4PDK.VideoState.IDLE;
    private IXAdConstants4PDK.ActivityState f = IXAdConstants4PDK.ActivityState.CREATE;
    private IXAdConstants4PDK.VisitorAction g;
    private double h;
    private int i;
    private int j;
    private Context k;
    private String l;
    private Location m;
    protected IXAdLogger mAdLogger;
    private Activity n;
    private RelativeLayout o;
    private final IOAdEventDispatcher p;
    /* access modifiers changed from: private */
    public final XAdSlotManager q;

    public XAdContext(Context context, String str, Location location) {
        this.k = context;
        this.l = str;
        this.m = location;
        this.p = new c();
        this.q = new XAdSlotManager();
        this.mAdLogger = m.a().f();
    }

    public void setVideoDisplayBase(RelativeLayout relativeLayout) {
        this.o = relativeLayout;
        setActivity((Activity) this.o.getContext());
        new Handler(getActivity().getMainLooper()).post(new Runnable() {
            public void run() {
                IXLinearAdSlot retrievePrerollAdSlot = XAdContext.this.q.retrievePrerollAdSlot();
                if (retrievePrerollAdSlot != null && retrievePrerollAdSlot.getSlotState() == IXAdConstants4PDK.SlotState.PLAYING) {
                    retrievePrerollAdSlot.resize();
                }
            }
        });
    }

    public void setActivity(Activity activity) {
        if (activity != null && this.n == null) {
            this.n = activity;
            if (this.k == null) {
                this.k = this.n.getApplicationContext();
            }
        }
    }

    public Activity getActivity() {
        return this.n;
    }

    public void setActivityState(IXAdConstants4PDK.ActivityState activityState) {
        this.f = activityState;
        this.mAdLogger.i(TAG, activityState.getValue());
        IXLinearAdSlot retrievePrerollAdSlot = this.q.retrievePrerollAdSlot();
        if (retrievePrerollAdSlot != null) {
            if (activityState == IXAdConstants4PDK.ActivityState.PAUSE) {
                retrievePrerollAdSlot.pause();
            }
            if (activityState == IXAdConstants4PDK.ActivityState.RESUME) {
                retrievePrerollAdSlot.resume();
            }
        }
    }

    public void setParameter(String str, Object obj) {
        this.c.put(str, obj);
    }

    public Object getParameter(String str) {
        return this.c.get(str);
    }

    public void setContentVideoScreenMode(IXAdConstants4PDK.ScreenSizeMode screenSizeMode) {
        IXAdInstanceInfo currentAdInstance;
        this.d = screenSizeMode;
        IXLinearAdSlot retrievePrerollAdSlot = this.q.retrievePrerollAdSlot();
        if (this.d == IXAdConstants4PDK.ScreenSizeMode.FULL_SCREEN && retrievePrerollAdSlot != null && retrievePrerollAdSlot.getSlotState() == IXAdConstants4PDK.SlotState.PLAYING && (currentAdInstance = retrievePrerollAdSlot.getCurrentAdInstance()) != null) {
            int playheadTime = (int) retrievePrerollAdSlot.getCurrentXAdContainer().getPlayheadTime();
            IXAdURIUitls i2 = m.a().i();
            ArrayList arrayList = new ArrayList();
            List<String> fullScreenTrackers = currentAdInstance.getFullScreenTrackers();
            int i3 = 0;
            while (true) {
                int i4 = i3;
                if (i4 < fullScreenTrackers.size()) {
                    arrayList.add(i2.addParameter(fullScreenTrackers.get(i4), "progress", "" + playheadTime));
                    i3 = i4 + 1;
                } else {
                    currentAdInstance.setFullScreenTrackers(arrayList);
                    HashSet hashSet = new HashSet();
                    hashSet.addAll(currentAdInstance.getFullScreenTrackers());
                    a(hashSet);
                    return;
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.mobads.openad.e.a.a(com.baidu.mobads.openad.e.d, java.lang.Boolean):void
     arg types: [com.baidu.mobads.openad.e.d, int]
     candidates:
      com.baidu.mobads.openad.e.a.a(com.baidu.mobads.openad.e.a, java.net.HttpURLConnection):java.net.HttpURLConnection
      com.baidu.mobads.openad.e.a.a(com.baidu.mobads.openad.e.d, double):void
      com.baidu.mobads.openad.e.a.a(com.baidu.mobads.openad.e.d, java.lang.Boolean):void */
    private void a(Set<String> set) {
        a aVar = new a();
        for (String dVar : set) {
            d dVar2 = new d(dVar, "");
            dVar2.e = 1;
            aVar.a(dVar2, (Boolean) true);
        }
    }

    public void setContentVideoState(IXAdConstants4PDK.VideoState videoState) {
        this.e = videoState;
    }

    public void setContentVideoPlayheadTime(double d2) {
        this.h = d2;
    }

    public void setAdServerRequestingTimeout(int i2) {
        this.i = i2;
    }

    public void setAdCreativeLoadingTimeout(int i2) {
        this.j = i2;
    }

    public void submitRequest() {
        IXLinearAdSlot retrievePrerollAdSlot = this.q.retrievePrerollAdSlot();
        if (this.i > 0 && this.j > 0) {
            HashMap<String, String> parameter = retrievePrerollAdSlot.getParameter();
            parameter.put(IXAdInternalConstants.PARAMETER_KEY_OF_AD_REQUESTING_TIMEOUT, "" + this.i);
            parameter.put(IXAdInternalConstants.PARAMETER_KEY_OF_AD_CREATIVE_LOADING_TIMEOUT, "" + this.j);
            parameter.put(IXAdInternalConstants.PARAMETER_KEY_OF_BASE_WIDTH, "" + this.f216a);
            parameter.put(IXAdInternalConstants.PARAMETER_KEY_OF_BASE_HEIGHT, "" + this.b);
            retrievePrerollAdSlot.setParameter(parameter);
        }
        retrievePrerollAdSlot.request();
    }

    public static class AdSlotEventListener implements IOAdEventListener {
        public static final String TAG = "AdSlotEventListener";

        /* renamed from: a  reason: collision with root package name */
        private final Context f218a;
        /* access modifiers changed from: private */
        public final IXAdProd b;
        /* access modifiers changed from: private */
        public final IOAdEventDispatcher c;

        public AdSlotEventListener(Context context, IXAdProd iXAdProd, IOAdEventDispatcher iOAdEventDispatcher) {
            this.f218a = context;
            this.b = iXAdProd;
            this.c = iOAdEventDispatcher;
        }

        public void run(final IOAdEvent iOAdEvent) {
            m.a().f().i(TAG, iOAdEvent.getType());
            m.a().m().a((Runnable) new Runnable() {
                public void run() {
                    if (iOAdEvent.getType().equals(b.COMPLETE)) {
                        AdSlotEventListener.this.c.dispatchEvent(new XAdEvent4PDK(IXAdConstants4PDK.EVENT_REQUEST_COMPLETE, AdSlotEventListener.this.b));
                    }
                    if (iOAdEvent.getType().equals(IXAdEvent.AD_STARTED)) {
                        if (AdSlotEventListener.this.b.getProdBase() != null) {
                            AdSlotEventListener.this.b.getProdBase().setVisibility(0);
                        }
                        AdSlotEventListener.this.c.dispatchEvent(new XAdEvent4PDK(IXAdConstants4PDK.EVENT_SLOT_STARTED, AdSlotEventListener.this.b));
                    }
                    if (iOAdEvent.getType().equals("AdUserClick")) {
                        AdSlotEventListener.this.c.dispatchEvent(new XAdEvent4PDK(IXAdConstants4PDK.EVENT_SLOT_CLICKED, AdSlotEventListener.this.b));
                    }
                    if (iOAdEvent.getType().equals(IXAdEvent.AD_STOPPED)) {
                        if (AdSlotEventListener.this.b.getProdBase() != null) {
                            AdSlotEventListener.this.b.getProdBase().setVisibility(4);
                        }
                        AdSlotEventListener.this.c.dispatchEvent(new XAdEvent4PDK(IXAdConstants4PDK.EVENT_SLOT_ENDED, AdSlotEventListener.this.b));
                    }
                    if (iOAdEvent.getType().equals(IXAdEvent.AD_ERROR)) {
                        if (AdSlotEventListener.this.b.getProdBase() != null) {
                            AdSlotEventListener.this.b.getProdBase().setVisibility(4);
                        }
                        AdSlotEventListener.this.c.dispatchEvent(new XAdEvent4PDK(IXAdConstants4PDK.EVENT_ERROR, AdSlotEventListener.this.b));
                    }
                }
            });
        }
    }

    public IXLinearAdSlot newPrerollAdSlot(String str, int i2, int i3) {
        if (!this.q.containsAdSlot(str).booleanValue()) {
            com.baidu.mobads.production.i.b bVar = new com.baidu.mobads.production.i.b(this.n, str);
            bVar.setActivity(this.n);
            bVar.setAdSlotBase(this.o);
            bVar.setId(str);
            AdSlotEventListener adSlotEventListener = new AdSlotEventListener(this.k, bVar, this.p);
            bVar.removeAllListeners();
            bVar.addEventListener(b.COMPLETE, adSlotEventListener);
            bVar.addEventListener(IXAdEvent.AD_STARTED, adSlotEventListener);
            bVar.addEventListener(IXAdEvent.AD_STOPPED, adSlotEventListener);
            bVar.addEventListener(IXAdEvent.AD_ERROR, adSlotEventListener);
            bVar.addEventListener("AdUserClick", adSlotEventListener);
            this.q.addAdSlot(bVar);
        }
        return this.q.retrievePrerollAdSlot();
    }

    public IXAdProd getSlotById(String str) {
        return this.q.retrieveAdSlotById(str);
    }

    public void addEventListener(String str, IOAdEventListener iOAdEventListener) {
        this.p.addEventListener(str, iOAdEventListener);
    }

    public void removeEventListener(String str, IOAdEventListener iOAdEventListener) {
        this.p.removeEventListener(str, iOAdEventListener);
    }

    public void dispatchEvent(IOAdEvent iOAdEvent) {
    }

    public IXAdManager getXAdManager() {
        return null;
    }

    public void notifyVisitorAction(IXAdConstants4PDK.VisitorAction visitorAction) {
        this.g = visitorAction;
    }

    public void dispose() {
    }

    public void setVideoDisplayBaseWidth(int i2) {
        this.f216a = i2;
    }

    public void setVideoDisplayBaseHeight(int i2) {
        this.b = i2;
    }
}
