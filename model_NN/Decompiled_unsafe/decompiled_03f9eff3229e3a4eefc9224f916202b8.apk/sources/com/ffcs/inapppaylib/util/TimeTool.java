package com.ffcs.inapppaylib.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeTool {
    public static String getTimestamp() {
        return Date2String(System.currentTimeMillis(), "yyyy-MM-dd HH:mm:ss");
    }

    public static String Date2String(long j, String str) {
        return new SimpleDateFormat(str).format(new Date(j));
    }

    public static long DateStr2Long(String str, String str2) {
        try {
            return new SimpleDateFormat(str2).parse(str).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
            return -1;
        }
    }
}
