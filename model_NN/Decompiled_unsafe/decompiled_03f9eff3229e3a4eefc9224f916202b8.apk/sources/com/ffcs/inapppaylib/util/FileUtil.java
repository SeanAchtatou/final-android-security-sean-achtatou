package com.ffcs.inapppaylib.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class FileUtil {
    public static String getFromAssets(Context context, String str) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(openAssetsFile(context, str)));
            StringBuilder sb = new StringBuilder();
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine == null) {
                    return sb.toString();
                }
                sb.append(readLine);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static InputStream openAssetsFile(Context context, String str) {
        try {
            return context.getResources().getAssets().open(str);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Bitmap getImageFromAssets(Context context, String str) {
        return BitmapFactory.decodeStream(openAssetsFile(context, str));
    }
}
