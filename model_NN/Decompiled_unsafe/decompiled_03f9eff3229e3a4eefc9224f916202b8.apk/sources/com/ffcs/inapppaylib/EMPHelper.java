package com.ffcs.inapppaylib;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.widget.Toast;
import com.c.a.a;
import com.c.a.a.b;
import com.c.a.b.b.c;
import com.c.a.b.d;
import com.c.a.b.e;
import com.ffcs.crypt.CryptSign;
import com.ffcs.inapppaylib.bean.Constants;
import com.ffcs.inapppaylib.bean.response.EmpResponse;
import com.ffcs.inapppaylib.bean.response.FindMdnResponse;
import com.ffcs.inapppaylib.bean.response.IValidatableResponse;
import com.ffcs.inapppaylib.bean.response.PackageListResponse;
import com.ffcs.inapppaylib.bean.response.PayResponse;
import com.ffcs.inapppaylib.bean.response.UserPackage;
import com.ffcs.inapppaylib.bean.response.UserPackageResponse;
import com.ffcs.inapppaylib.bean.response.VerifyResponse;
import com.ffcs.inapppaylib.impl.OnVCodeListener;
import com.ffcs.inapppaylib.util.DeviceUtil;
import com.ffcs.inapppaylib.util.TimeTool;
import com.google.a.j;
import com.sina.weibo.sdk.exception.WeiboAuthException;
import com.tencent.open.SocialConstants;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import org.json.JSONObject;

public class EMPHelper {
    private static final String HMAC_SHA1 = "HmacSHA1";
    private static final String SIGNATURE_METHOD = "HmacSHA1";
    private static final char[] encodeTable = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'};
    private static EMPHelper instance;
    private static final char last2byte = ((char) Integer.parseInt("00000011", 2));
    private static final char last4byte = ((char) Integer.parseInt("00001111", 2));
    private static final char last6byte = ((char) Integer.parseInt("00111111", 2));
    private static final char lead2byte = ((char) Integer.parseInt("11000000", 2));
    private static final char lead4byte = ((char) Integer.parseInt("11110000", 2));
    private static final char lead6byte = ((char) Integer.parseInt("11111100", 2));
    private String CHANNEL_ID = "";
    private String DEVICE_ID = "";
    private String DEVICE_SECRET = "";
    private String appId;
    private String appSecret;
    private int connTimeout = 30000;
    /* access modifiers changed from: private */
    public Context context;
    private CryptSign cryptSign;
    /* access modifiers changed from: private */
    public boolean isPaying;
    /* access modifiers changed from: private */
    public Handler msgHandle;
    private String phone;
    private String productId;
    /* access modifiers changed from: private */
    public int trycount = 0;
    /* access modifiers changed from: private */
    public String trypayCode;
    /* access modifiers changed from: private */
    public String trypayData;

    private EMPHelper(Context context2) {
        this.context = context2;
        this.cryptSign = new CryptSign();
        this.appId = "386563630000042539";
        this.appSecret = "57c03fafb903037500dbbcfe649e46a5";
    }

    public static synchronized EMPHelper getInstance(Context context2) {
        EMPHelper eMPHelper;
        synchronized (EMPHelper.class) {
            if (instance == null) {
                instance = new EMPHelper(context2);
            }
            eMPHelper = instance;
        }
        return eMPHelper;
    }

    public void init(String str, String str2, String str3, Handler handler, int i) {
        this.DEVICE_ID = str;
        this.CHANNEL_ID = str2;
        this.DEVICE_SECRET = str3;
        this.msgHandle = handler;
        this.connTimeout = i;
    }

    public void getTradeId(String str, String str2, String str3) {
        commit(str, str2, str3, null);
    }

    /* access modifiers changed from: protected */
    public void commit(String str, String str2, String str3, String str4) {
        this.isPaying = true;
        this.trycount = 0;
        this.trypayCode = str2;
        this.trypayData = str4;
        this.phone = str;
        this.productId = str3;
        checkValidata(str2, str4);
    }

    /* access modifiers changed from: protected */
    public void checkValidata(String str, String str2) {
        String authSign = this.cryptSign.authSign(this.context, "app_id=" + this.appId + "&pay_code=" + str + "&timestamp=" + TimeTool.getTimestamp() + "&state=" + str2 + "&phone=" + this.phone, this.appSecret);
        d dVar = new d();
        dVar.c("app_id", this.appId);
        dVar.c("sign", authSign);
        dVar.c("state", str2);
        a aVar = new a(this.connTimeout);
        this.trycount++;
        aVar.a(c.a.POST, "http://118.85.194.4:8083/iapSms/ws/v3.0.1/sp/validate", dVar, new com.c.a.b.a.c<String>() {
            public void onFailure(b bVar, String str) {
                IValidatableResponse iValidatableResponse = new IValidatableResponse();
                if (!DeviceUtil.hasNet(EMPHelper.this.context)) {
                    iValidatableResponse.setRes_code(-1);
                    iValidatableResponse.setRes_message("网络连接异常");
                } else {
                    iValidatableResponse.setRes_code(-3);
                    iValidatableResponse.setRes_message(str);
                }
                EMPHelper.this.invalid(iValidatableResponse);
            }

            public void onSuccess(e<String> eVar) {
                if ((eVar.d == 200 || eVar.d == 0) && eVar.f442a != null) {
                    IValidatableResponse iValidatableResponse = (IValidatableResponse) new j().a((String) eVar.f442a, IValidatableResponse.class);
                    if (iValidatableResponse.getRes_code() == 0 && iValidatableResponse.getOrder_no() != null && !iValidatableResponse.getOrder_no().equals("") && EMPHelper.this.isPaying) {
                        iValidatableResponse.setRes_message("计费成功");
                        Message message = new Message();
                        message.obj = iValidatableResponse;
                        message.what = Constants.RESULT_PAY_SUCCESS;
                        EMPHelper.this.msgHandle.sendMessage(message);
                    } else if (iValidatableResponse.getRes_code() == 0 && EMPHelper.this.isPaying) {
                        iValidatableResponse.setRes_message("获取交易id成功");
                        iValidatableResponse.setLowTariff(false);
                        Message message2 = new Message();
                        message2.obj = iValidatableResponse;
                        message2.what = 900;
                        EMPHelper.this.msgHandle.sendMessage(message2);
                    } else if (iValidatableResponse.getRes_code() == 420 && EMPHelper.this.isPaying) {
                        iValidatableResponse.setRes_message("获取交易id成功");
                        iValidatableResponse.setLowTariff(true);
                        Message message3 = new Message();
                        message3.obj = iValidatableResponse;
                        message3.what = 900;
                        EMPHelper.this.msgHandle.sendMessage(message3);
                    } else if (EMPHelper.this.trycount <= 5) {
                        EMPHelper.this.checkValidata(EMPHelper.this.trypayCode, EMPHelper.this.trypayData);
                    } else {
                        EMPHelper.this.invalid(iValidatableResponse);
                    }
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void invalid(IValidatableResponse iValidatableResponse) {
        Message message = new Message();
        message.obj = iValidatableResponse;
        message.what = Constants.RESULT_VALIDATE_FAILURE;
        this.msgHandle.sendMessage(message);
    }

    public void confirmPayment(String str, String str2, boolean z) {
        d dVar = new d();
        dVar.c("trade_id", str);
        dVar.c("sign", this.cryptSign.signAuth(this.context, "trade_id=" + str + "&verifyCode=" + str2 + "&timestamp=" + TimeTool.getTimestamp(), this.appSecret));
        if (z) {
            dVar.c("auth", "1");
        } else {
            dVar.c("auth", "0");
        }
        new a(this.connTimeout).a(c.a.POST, "http://118.85.194.4:8083/iapSms/ws/v3.0.1/sp/billing", dVar, new com.c.a.b.a.c<String>() {
            public void onFailure(b bVar, String str) {
                if (EMPHelper.this.msgHandle != null) {
                    PayResponse payResponse = new PayResponse();
                    if (!DeviceUtil.hasNet(EMPHelper.this.context)) {
                        payResponse.setRes_code(-1);
                        payResponse.setRes_message("网络连接异常");
                    } else {
                        payResponse.setRes_code(-3);
                        payResponse.setRes_message(str);
                    }
                    EMPHelper.this.onBillingFailure(payResponse);
                }
            }

            public void onSuccess(e<String> eVar) {
                if ((eVar.d == 200 || eVar.d == 0) && eVar.f442a != null) {
                    PayResponse payResponse = (PayResponse) new j().a((String) eVar.f442a, PayResponse.class);
                    if (payResponse.getRes_code() == 0) {
                        if (EMPHelper.this.msgHandle != null) {
                            if (EMPHelper.this.isPaying) {
                                Toast.makeText(EMPHelper.this.context, "支付成功", 0).show();
                            }
                            Message message = new Message();
                            message.obj = payResponse;
                            message.what = Constants.RESULT_PAY_SUCCESS;
                            EMPHelper.this.msgHandle.sendMessage(message);
                            EMPHelper.this.subscribeFeedback(payResponse);
                        }
                    } else if (EMPHelper.this.msgHandle != null) {
                        EMPHelper.this.onBillingFailure(payResponse);
                    }
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void onBillingFailure(PayResponse payResponse) {
        Message message = new Message();
        message.obj = payResponse;
        message.what = Constants.RESULT_PAY_FAILURE;
        this.msgHandle.sendMessage(message);
        subscribeFeedback(payResponse);
    }

    /* access modifiers changed from: private */
    public void subscribeFeedback(PayResponse payResponse) {
        try {
            d dVar = new d();
            String sb = new StringBuilder(String.valueOf(payResponse.getRes_code())).toString();
            String res_message = payResponse.getRes_message();
            dVar.c("product_id", this.productId);
            dVar.c("mdn", this.phone);
            dVar.c("state", sb);
            dVar.c(SocialConstants.PARAM_APP_DESC, res_message);
            String format = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
            dVar.a("auth-deviceid", this.DEVICE_ID);
            dVar.a("auth-channelid", this.CHANNEL_ID);
            dVar.a("auth-timestamp", format);
            dVar.a("auth-signature-method", "HmacSHA1");
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(this.DEVICE_ID).append("&");
            stringBuffer.append(this.CHANNEL_ID).append("&");
            stringBuffer.append(format).append("&");
            stringBuffer.append(this.productId).append("&");
            stringBuffer.append(this.phone).append("&");
            stringBuffer.append(sb).append("&");
            stringBuffer.append(res_message);
            dVar.a("auth-signature", generateMacSignature(this.DEVICE_SECRET, stringBuffer.toString()));
            new a(this.connTimeout).a(c.a.POST, "http://api.118100.cn/openapi/services/v2/package/packageservice/ismplog.json", dVar, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String generateMacSignature(String str, String str2) {
        byte[] bArr = null;
        try {
            Mac instance2 = Mac.getInstance("HmacSHA1");
            instance2.init(new SecretKeySpec(str.getBytes(), "HmacSHA1"));
            bArr = instance2.doFinal(str2.getBytes());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return encode(bArr);
    }

    public static String encode(byte[] bArr) {
        StringBuffer stringBuffer = new StringBuffer(((int) (((double) bArr.length) * 1.34d)) + 3);
        char c = 0;
        int i = 0;
        for (int i2 = 0; i2 < bArr.length; i2++) {
            i %= 8;
            while (i < 8) {
                switch (i) {
                    case 0:
                        c = (char) (((char) (bArr[i2] & lead6byte)) >>> 2);
                        break;
                    case 2:
                        c = (char) (bArr[i2] & last6byte);
                        break;
                    case 4:
                        c = (char) (((char) (bArr[i2] & last4byte)) << 2);
                        if (i2 + 1 >= bArr.length) {
                            break;
                        } else {
                            c = (char) (c | ((bArr[i2 + 1] & lead2byte) >>> 6));
                            break;
                        }
                    case 6:
                        c = (char) (((char) (bArr[i2] & last2byte)) << 4);
                        if (i2 + 1 >= bArr.length) {
                            break;
                        } else {
                            c = (char) (c | ((bArr[i2 + 1] & lead4byte) >>> 4));
                            break;
                        }
                }
                stringBuffer.append(encodeTable[c]);
                i += 6;
            }
        }
        if (stringBuffer.length() % 4 != 0) {
            for (int length = 4 - (stringBuffer.length() % 4); length > 0; length--) {
                stringBuffer.append("=");
            }
        }
        return stringBuffer.toString();
    }

    public void unsubscribe(String str, String str2) {
        try {
            d dVar = new d();
            dVar.c("mdn", str);
            dVar.c("package_id", str2);
            String format = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
            dVar.a("auth-deviceid", this.DEVICE_ID);
            dVar.a("auth-channelid", this.CHANNEL_ID);
            dVar.a("auth-timestamp", format);
            dVar.a("auth-signature-method", "HmacSHA1");
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(this.DEVICE_ID).append("&");
            stringBuffer.append(this.CHANNEL_ID).append("&");
            stringBuffer.append(format).append("&");
            stringBuffer.append(str).append("&");
            stringBuffer.append(str2);
            dVar.a("auth-signature", generateMacSignature(this.DEVICE_SECRET, stringBuffer.toString()));
            new a(this.connTimeout).a(c.a.POST, "http://api.118100.cn/openapi/services/v2/package/packageservice/unsubscribebyemp.json", dVar, new com.c.a.b.a.c<String>() {
                public void onFailure(b bVar, String str) {
                    try {
                        if (EMPHelper.this.msgHandle != null) {
                            EmpResponse empResponse = new EmpResponse();
                            if (!DeviceUtil.hasNet(EMPHelper.this.context)) {
                                empResponse.setRes_code(WeiboAuthException.DEFAULT_AUTH_ERROR_CODE);
                                empResponse.setRes_message("网络连接异常");
                            } else {
                                empResponse.setRes_code("-3");
                                empResponse.setRes_message(str);
                            }
                            Message message = new Message();
                            message.obj = empResponse;
                            message.what = 912;
                            EMPHelper.this.msgHandle.sendMessage(message);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                public void onSuccess(e<String> eVar) {
                    try {
                        if ((eVar.d == 200 || eVar.d == 0) && eVar.f442a != null && EMPHelper.this.msgHandle != null) {
                            if (((String) eVar.f442a).contains("BasicJTResponse")) {
                                eVar.f442a = new JSONObject((String) eVar.f442a).getString("BasicJTResponse");
                            } else {
                                Matcher matcher = Pattern.compile("^\\{\".+\"\\:\\{(.+?)\\}\\}$").matcher((CharSequence) eVar.f442a);
                                if (matcher.find()) {
                                    eVar.f442a = "{" + matcher.group(1) + "}";
                                }
                            }
                            EmpResponse empResponse = (EmpResponse) new j().a((String) eVar.f442a, EmpResponse.class);
                            Message message = new Message();
                            if ("0000".equals(empResponse.getRes_code()) || "0".equals(empResponse.getRes_code())) {
                                message.what = 910;
                            } else {
                                message.what = 912;
                            }
                            message.obj = empResponse;
                            EMPHelper.this.msgHandle.sendMessage(message);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void queryProductlist(String str, String str2, int i) {
        try {
            d dVar = new d();
            dVar.b("mdn", str);
            dVar.b("package_id", str2);
            dVar.b("is_count_down_num", new StringBuilder(String.valueOf(i)).toString());
            String format = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
            dVar.a("auth-deviceid", this.DEVICE_ID);
            dVar.a("auth-channelid", this.CHANNEL_ID);
            dVar.a("auth-timestamp", format);
            dVar.a("auth-signature-method", "HmacSHA1");
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(this.DEVICE_ID).append("&");
            stringBuffer.append(this.CHANNEL_ID).append("&");
            stringBuffer.append(format).append("&");
            stringBuffer.append(str).append("&");
            stringBuffer.append(str2).append("&");
            stringBuffer.append(new StringBuilder(String.valueOf(i)).toString());
            dVar.a("auth-signature", generateMacSignature(this.DEVICE_SECRET, stringBuffer.toString()));
            new a(this.connTimeout).a(c.a.GET, "http://api.118100.cn/openapi/services/v2/package/packageservice/querypackagelist.json", dVar, new com.c.a.b.a.c<String>() {
                public void onFailure(b bVar, String str) {
                    try {
                        if (EMPHelper.this.msgHandle != null) {
                            EmpResponse empResponse = new EmpResponse();
                            if (!DeviceUtil.hasNet(EMPHelper.this.context)) {
                                empResponse.setRes_code(WeiboAuthException.DEFAULT_AUTH_ERROR_CODE);
                                empResponse.setRes_message("网络连接异常");
                            } else {
                                empResponse.setRes_code("-3");
                                empResponse.setRes_message(str);
                            }
                            Message message = new Message();
                            message.obj = empResponse;
                            message.what = 922;
                            EMPHelper.this.msgHandle.sendMessage(message);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                public void onSuccess(e<String> eVar) {
                    try {
                        if ((eVar.d == 200 || eVar.d == 0) && eVar.f442a != null && EMPHelper.this.msgHandle != null) {
                            PackageListResponse packageListResponse = new PackageListResponse();
                            packageListResponse.setRes_code("-3");
                            packageListResponse.setRes_message("Json解析异常");
                            try {
                                if (((String) eVar.f442a).contains("UserPackageListResp")) {
                                    String string = new JSONObject((String) eVar.f442a).getString("UserPackageListResp");
                                    if (!TextUtils.isEmpty(string) && string.contains("res_code")) {
                                        packageListResponse.setRes_code(new JSONObject(string).getString("res_code"));
                                    }
                                    if (!TextUtils.isEmpty(string) && string.contains("res_message")) {
                                        packageListResponse.setRes_message(new JSONObject(string).getString("res_message"));
                                    }
                                    if (!TextUtils.isEmpty(string) && string.contains("user_package_list")) {
                                        String string2 = new JSONObject(string).getString("user_package_list");
                                        if (!TextUtils.isEmpty(string2) && string2.contains("user_package")) {
                                            j jVar = new j();
                                            if (!string2.contains("[") || !string2.contains("]")) {
                                                ArrayList arrayList = new ArrayList();
                                                arrayList.add((UserPackage) jVar.a(new JSONObject(string2).getString("user_package"), UserPackage.class));
                                                packageListResponse.setUser_package_list(arrayList);
                                            } else {
                                                packageListResponse.setUser_package_list(((UserPackageResponse) jVar.a(string2, UserPackageResponse.class)).getUser_package());
                                            }
                                        }
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            Message message = new Message();
                            if ("0000".equals(packageListResponse.getRes_code()) || "0".equals(packageListResponse.getRes_code())) {
                                message.what = 920;
                            } else {
                                message.what = 922;
                            }
                            message.obj = packageListResponse;
                            EMPHelper.this.msgHandle.sendMessage(message);
                        }
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void findMdnByImsi(String str) {
        try {
            d dVar = new d();
            dVar.b("imsi", str);
            String format = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
            dVar.a("auth-deviceid", this.DEVICE_ID);
            dVar.a("auth-channelid", this.CHANNEL_ID);
            dVar.a("auth-timestamp", format);
            dVar.a("auth-signature-method", "HmacSHA1");
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(this.DEVICE_ID).append("&");
            stringBuffer.append(this.CHANNEL_ID).append("&");
            stringBuffer.append(format).append("&");
            stringBuffer.append(str);
            dVar.a("auth-signature", generateMacSignature(this.DEVICE_SECRET, stringBuffer.toString()));
            new a(this.connTimeout).a(c.a.GET, "http://api.118100.cn/openapi/services/v2/qd/cooperateservice/findmdnbyimsi.json", dVar, new com.c.a.b.a.c<String>() {
                public void onFailure(b bVar, String str) {
                    try {
                        if (EMPHelper.this.msgHandle != null) {
                            EmpResponse empResponse = new EmpResponse();
                            if (!DeviceUtil.hasNet(EMPHelper.this.context)) {
                                empResponse.setRes_code(WeiboAuthException.DEFAULT_AUTH_ERROR_CODE);
                                empResponse.setRes_message("网络连接异常");
                            } else {
                                empResponse.setRes_code("-3");
                                empResponse.setRes_message(str);
                            }
                            Message message = new Message();
                            message.obj = empResponse;
                            message.what = 932;
                            EMPHelper.this.msgHandle.sendMessage(message);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                public void onSuccess(e<String> eVar) {
                    try {
                        if ((eVar.d == 200 || eVar.d == 0) && eVar.f442a != null && EMPHelper.this.msgHandle != null) {
                            if (((String) eVar.f442a).contains("DEPUserInfoResponse")) {
                                eVar.f442a = new JSONObject((String) eVar.f442a).getString("DEPUserInfoResponse");
                            } else {
                                Matcher matcher = Pattern.compile("^\\{\".+\"\\:\\{(.+?)\\}\\}$").matcher((CharSequence) eVar.f442a);
                                if (matcher.find()) {
                                    eVar.f442a = "{" + matcher.group(1) + "}";
                                }
                            }
                            FindMdnResponse findMdnResponse = (FindMdnResponse) new j().a((String) eVar.f442a, FindMdnResponse.class);
                            Message message = new Message();
                            if ("0000".equals(findMdnResponse.getRes_code()) || "0".equals(findMdnResponse.getRes_code())) {
                                message.what = 930;
                            } else {
                                message.what = 932;
                            }
                            message.obj = findMdnResponse;
                            EMPHelper.this.msgHandle.sendMessage(message);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void refreshVCode(String str, String str2, final OnVCodeListener onVCodeListener) {
        String signAuth = this.cryptSign.signAuth(this.context, "trade_id=" + str + "&timestamp=" + TimeTool.getTimestamp(), this.appSecret);
        d dVar = new d();
        dVar.c("trade_id", str);
        dVar.c("sign", signAuth);
        new a(this.connTimeout).a(c.a.POST, "http://118.85.194.4:8083/iapSms/ws/v3.0.1/sp/refreshVerifyCode", dVar, new com.c.a.b.a.c<String>() {
            public void onFailure(b bVar, String str) {
                if (EMPHelper.this.msgHandle != null) {
                    VerifyResponse verifyResponse = new VerifyResponse();
                    if (!DeviceUtil.hasNet(EMPHelper.this.context)) {
                        verifyResponse.setRes_code(-1);
                        verifyResponse.setRes_message("网络连接异常");
                    } else {
                        verifyResponse.setRes_code(-3);
                        verifyResponse.setRes_message(str);
                    }
                    onVCodeListener.onRefreshVcoBtnFailure(verifyResponse);
                }
            }

            public void onSuccess(e<String> eVar) {
                if ((eVar.d == 200 || eVar.d == 0) && eVar.f442a != null) {
                    VerifyResponse verifyResponse = (VerifyResponse) new j().a((String) eVar.f442a, VerifyResponse.class);
                    if (verifyResponse.getRes_code() == 0) {
                        if (onVCodeListener != null) {
                            onVCodeListener.onRefreshVcoBtnSuccess(verifyResponse);
                        }
                    } else if (onVCodeListener != null) {
                        onVCodeListener.onRefreshVcoBtnFailure(verifyResponse);
                    }
                }
            }
        });
    }

    public void quitPay() {
        this.isPaying = false;
    }
}
