package com.ffcs.inapppaylib.impl;

import com.ffcs.inapppaylib.bean.response.PayResponse;

public interface OnPayListener {
    void onBillingFailure(PayResponse payResponse);

    void onBillingSuccess(PayResponse payResponse);
}
