package com.ffcs.inapppaylib.bean.response;

import java.io.Serializable;

public class BaseResponse implements Serializable {
    int res_code;
    String res_message;
    String trade_id;

    public int getRes_code() {
        return this.res_code;
    }

    public void setRes_code(int i) {
        this.res_code = i;
    }

    public String getRes_message() {
        return this.res_message;
    }

    public void setRes_message(String str) {
        this.res_message = str;
    }

    public String getTrade_id() {
        return this.trade_id;
    }

    public void setTrade_id(String str) {
        this.trade_id = str;
    }
}
