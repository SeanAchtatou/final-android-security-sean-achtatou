package com.ffcs.inapppaylib.bean.response;

public class VerifyResponse extends BaseResponse {
    private String sms_num;
    private String verify_code;

    public String getSms_num() {
        return this.sms_num;
    }

    public String getVerify_code() {
        return this.verify_code;
    }

    public void setSms_num(String str) {
        this.sms_num = str;
    }

    public void setVerify_code(String str) {
        this.verify_code = str;
    }
}
