package com.ffcs.crypt;

import android.content.Context;

public class CryptSign {
    static {
        System.loadLibrary("crypt_sign");
    }

    public native String authSign(Context context, String str, String str2);

    public native String signAuth(Context context, String str, String str2);
}
