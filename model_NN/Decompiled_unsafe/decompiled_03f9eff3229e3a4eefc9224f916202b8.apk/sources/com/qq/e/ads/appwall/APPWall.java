package com.qq.e.ads.appwall;

import android.content.Context;
import com.qq.e.comm.a;
import com.qq.e.comm.managers.GDTADManager;
import com.qq.e.comm.managers.plugin.b;
import com.qq.e.comm.pi.AWI;
import com.qq.e.comm.util.GDTLogger;
import com.qq.e.comm.util.StringUtil;

public class APPWall {

    /* renamed from: a  reason: collision with root package name */
    private AWI f643a;

    public APPWall(Context context, String str, String str2) {
        if (StringUtil.isEmpty(str) || StringUtil.isEmpty(str2) || context == null) {
            GDTLogger.e(String.format("APPWall ADView Contructor paras error,appid=%s,posId=%s,context=%s", str, str2, context));
        } else if (!a.a(context)) {
            GDTLogger.e("Required Activity/Service/Permission Not Declared in AndroidManifest.xml");
        } else if (GDTADManager.getInstance().initWith(context, str)) {
            try {
                this.f643a = GDTADManager.getInstance().getPM().getPOFactory().getAppWallView(context, str, str2);
            } catch (b e) {
                GDTLogger.e("Exception while init APPWall plugin", e);
            }
        } else {
            GDTLogger.e("Fail to init ADManager");
        }
    }

    public void doShowAppWall() {
        if (this.f643a != null) {
            this.f643a.showAppWall();
        }
    }

    public void prepare() {
        if (this.f643a != null) {
            this.f643a.prepare();
        }
    }

    public void setScreenOrientation(int i) {
        if (this.f643a != null) {
            this.f643a.setScreenOrientation(i);
        }
    }
}
