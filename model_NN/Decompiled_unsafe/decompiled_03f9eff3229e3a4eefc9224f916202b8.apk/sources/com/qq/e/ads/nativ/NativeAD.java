package com.qq.e.ads.nativ;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import com.qq.e.ads.cfg.BrowserType;
import com.qq.e.ads.cfg.DownAPPConfirmPolicy;
import com.qq.e.comm.a;
import com.qq.e.comm.adevent.ADEvent;
import com.qq.e.comm.adevent.ADListener;
import com.qq.e.comm.managers.GDTADManager;
import com.qq.e.comm.pi.NADI;
import com.qq.e.comm.pi.POFactory;
import com.qq.e.comm.util.GDTLogger;
import com.qq.e.comm.util.StringUtil;
import java.util.ArrayList;
import java.util.List;

public class NativeAD {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public NADI f659a;
    /* access modifiers changed from: private */
    public NativeAdListener b;
    private boolean c;
    private boolean d;
    /* access modifiers changed from: private */
    public List<Integer> e = new ArrayList();
    /* access modifiers changed from: private */
    public boolean f = false;
    /* access modifiers changed from: private */
    public BrowserType g;
    /* access modifiers changed from: private */
    public DownAPPConfirmPolicy h;

    class ADListenerAdapter implements ADListener {
        private ADListenerAdapter() {
        }

        /* synthetic */ ADListenerAdapter(NativeAD nativeAD, byte b) {
            this();
        }

        public void onADEvent(ADEvent aDEvent) {
            if (NativeAD.this.b == null) {
                GDTLogger.i("No DevADListener Binded");
                return;
            }
            switch (aDEvent.getType()) {
                case 1:
                    if (aDEvent.getParas().length != 1 || !(aDEvent.getParas()[0] instanceof Integer)) {
                        GDTLogger.e("AdEvent.Paras error for NativeAD(" + aDEvent + ")");
                        return;
                    } else {
                        NativeAD.this.b.onNoAD(((Integer) aDEvent.getParas()[0]).intValue());
                        return;
                    }
                case 2:
                    if (aDEvent.getParas().length != 1 || !(aDEvent.getParas()[0] instanceof List)) {
                        GDTLogger.e("ADEvent.Paras error for NativeAD(" + aDEvent + ")");
                        return;
                    } else {
                        NativeAD.this.b.onADLoaded((List) aDEvent.getParas()[0]);
                        return;
                    }
                case 3:
                    if (aDEvent.getParas().length != 1 || !(aDEvent.getParas()[0] instanceof NativeADDataRef)) {
                        GDTLogger.e("ADEvent.Paras error for NativeAD(" + aDEvent + ")");
                        return;
                    } else {
                        NativeAD.this.b.onADStatusChanged((NativeADDataRef) aDEvent.getParas()[0]);
                        return;
                    }
                case 4:
                    if (aDEvent.getParas().length != 2 || !(aDEvent.getParas()[0] instanceof NativeADDataRef) || !(aDEvent.getParas()[1] instanceof Integer)) {
                        GDTLogger.e("ADEvent.Paras error for NativeAD(" + aDEvent + ")");
                        return;
                    } else {
                        NativeAD.this.b.onADError((NativeADDataRef) aDEvent.getParas()[0], ((Integer) aDEvent.getParas()[1]).intValue());
                        return;
                    }
                default:
                    return;
            }
        }
    }

    public interface NativeAdListener {
        void onADError(NativeADDataRef nativeADDataRef, int i);

        void onADLoaded(List<NativeADDataRef> list);

        void onADStatusChanged(NativeADDataRef nativeADDataRef);

        void onNoAD(int i);
    }

    public NativeAD(final Context context, final String str, final String str2, NativeAdListener nativeAdListener) {
        if (StringUtil.isEmpty(str) || StringUtil.isEmpty(str2) || context == null) {
            GDTLogger.e(String.format("GDTNativeAd Contructor paras error,appid=%s,posId=%s,context=%s", str, str2, context));
            return;
        }
        this.c = true;
        if (!a.a(context)) {
            GDTLogger.e("Required Activity/Service/Permission Not Declared in AndroidManifest.xml");
            return;
        }
        this.d = true;
        this.b = nativeAdListener;
        GDTADManager.INIT_EXECUTOR.execute(new Runnable() {
            public void run() {
                if (GDTADManager.getInstance().initWith(context, str)) {
                    try {
                        final POFactory pOFactory = GDTADManager.getInstance().getPM().getPOFactory();
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                             method: com.qq.e.ads.nativ.NativeAD.a(com.qq.e.ads.nativ.NativeAD, boolean):boolean
                             arg types: [com.qq.e.ads.nativ.NativeAD, int]
                             candidates:
                              com.qq.e.ads.nativ.NativeAD.a(com.qq.e.ads.nativ.NativeAD, com.qq.e.comm.pi.NADI):com.qq.e.comm.pi.NADI
                              com.qq.e.ads.nativ.NativeAD.a(com.qq.e.ads.nativ.NativeAD, boolean):boolean */
                            public void run() {
                                try {
                                    if (pOFactory != null) {
                                        NADI unused = NativeAD.this.f659a = pOFactory.getNativeADDelegate(context, str, str2, new ADListenerAdapter(NativeAD.this, (byte) 0));
                                        boolean unused2 = NativeAD.this.f = true;
                                        if (NativeAD.this.g != null) {
                                            NativeAD.this.setBrowserType(NativeAD.this.g);
                                        }
                                        if (NativeAD.this.h != null) {
                                            NativeAD.this.setDownAPPConfirmPolicy(NativeAD.this.h);
                                        }
                                        for (Integer intValue : NativeAD.this.e) {
                                            NativeAD.this.loadAD(intValue.intValue());
                                        }
                                    }
                                } catch (Throwable th) {
                                    GDTLogger.e("Exception while init Native Core", th);
                                } finally {
                                    boolean unused3 = NativeAD.this.f = true;
                                }
                            }
                        });
                    } catch (Throwable th) {
                        GDTLogger.e("Exception while init Native plugin", th);
                    }
                } else {
                    GDTLogger.e("Fail to init ADManager");
                }
            }
        });
    }

    public void loadAD(int i) {
        if (!this.c || !this.d) {
            GDTLogger.e("AD init Paras OR Context error,details in logs produced while init NativeAD");
        } else if (!this.f) {
            this.e.add(Integer.valueOf(i));
        } else if (this.f659a != null) {
            this.f659a.loadAd(i);
        } else {
            GDTLogger.e("NativeAD Init error,See More Logs");
        }
    }

    public void setBrowserType(BrowserType browserType) {
        this.g = browserType;
        if (this.f659a != null && browserType != null) {
            this.f659a.setBrowserType(browserType.value());
        }
    }

    public void setDownAPPConfirmPolicy(DownAPPConfirmPolicy downAPPConfirmPolicy) {
        this.h = downAPPConfirmPolicy;
        if (this.f659a != null && downAPPConfirmPolicy != null) {
            this.f659a.setDownAPPConfirmPolicy(downAPPConfirmPolicy);
        }
    }
}
