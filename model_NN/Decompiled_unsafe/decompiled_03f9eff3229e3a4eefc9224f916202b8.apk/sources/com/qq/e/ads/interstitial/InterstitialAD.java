package com.qq.e.ads.interstitial;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Handler;
import android.os.Looper;
import com.qq.e.comm.a;
import com.qq.e.comm.adevent.ADEvent;
import com.qq.e.comm.adevent.ADListener;
import com.qq.e.comm.managers.GDTADManager;
import com.qq.e.comm.pi.IADI;
import com.qq.e.comm.pi.POFactory;
import com.qq.e.comm.util.GDTLogger;
import com.qq.e.comm.util.StringUtil;

@SuppressLint({"ViewConstructor"})
public class InterstitialAD {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public IADI f655a;
    /* access modifiers changed from: private */
    public InterstitialADListener b;
    private boolean c = false;
    private boolean d = false;
    /* access modifiers changed from: private */
    public boolean e = false;
    private volatile int f = 0;

    class ADListenerAdapter implements ADListener {
        private ADListenerAdapter() {
        }

        /* synthetic */ ADListenerAdapter(InterstitialAD interstitialAD, byte b) {
            this();
        }

        public void onADEvent(ADEvent aDEvent) {
            if (InterstitialAD.this.b == null) {
                GDTLogger.i("No DevADListener Binded");
                return;
            }
            switch (aDEvent.getType()) {
                case 1:
                    if (aDEvent.getParas().length != 1 || !(aDEvent.getParas()[0] instanceof Integer)) {
                        GDTLogger.e("AdEvent.Paras error for Banner(" + aDEvent + ")");
                        return;
                    } else {
                        InterstitialAD.this.b.onNoAD(((Integer) aDEvent.getParas()[0]).intValue());
                        return;
                    }
                case 2:
                    InterstitialAD.this.b.onADReceive();
                    return;
                case 3:
                    InterstitialAD.this.b.onADExposure();
                    return;
                case 4:
                    InterstitialAD.this.b.onADOpened();
                    return;
                case 5:
                    InterstitialAD.this.b.onADClicked();
                    return;
                case 6:
                    InterstitialAD.this.b.onADLeftApplication();
                    return;
                case 7:
                    InterstitialAD.this.b.onADClosed();
                    return;
                default:
                    return;
            }
        }
    }

    public InterstitialAD(final Activity activity, final String str, final String str2) {
        if (StringUtil.isEmpty(str) || StringUtil.isEmpty(str2) || activity == null) {
            GDTLogger.e(String.format("Intersitial Contructor paras error,appid=%s,posId=%s,context=%s", str, str2, activity));
            return;
        }
        this.c = true;
        if (!a.a(activity)) {
            GDTLogger.e("Required Activity/Service/Permission Not Declared in AndroidManifest.xml");
            return;
        }
        this.d = true;
        GDTADManager.INIT_EXECUTOR.execute(new Runnable() {
            public void run() {
                if (GDTADManager.getInstance().initWith(activity, str)) {
                    try {
                        final POFactory pOFactory = GDTADManager.getInstance().getPM().getPOFactory();
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                             method: com.qq.e.ads.interstitial.InterstitialAD.a(com.qq.e.ads.interstitial.InterstitialAD, boolean):boolean
                             arg types: [com.qq.e.ads.interstitial.InterstitialAD, int]
                             candidates:
                              com.qq.e.ads.interstitial.InterstitialAD.a(com.qq.e.ads.interstitial.InterstitialAD, com.qq.e.comm.pi.IADI):com.qq.e.comm.pi.IADI
                              com.qq.e.ads.interstitial.InterstitialAD.a(com.qq.e.ads.interstitial.InterstitialAD, boolean):boolean */
                            public void run() {
                                try {
                                    if (pOFactory != null) {
                                        IADI unused = InterstitialAD.this.f655a = pOFactory.getIADView(activity, str, str2);
                                        InterstitialAD.this.f655a.setAdListener(new ADListenerAdapter(InterstitialAD.this, (byte) 0));
                                        boolean unused2 = InterstitialAD.this.e = true;
                                        while (InterstitialAD.b(InterstitialAD.this) > 0) {
                                            InterstitialAD.this.loadAD();
                                        }
                                    }
                                } catch (Throwable th) {
                                    GDTLogger.e("Exception while init IAD Core", th);
                                } finally {
                                    boolean unused3 = InterstitialAD.this.e = true;
                                }
                            }
                        });
                    } catch (Throwable th) {
                        GDTLogger.e("Exception while init IAD plugin", th);
                    }
                } else {
                    GDTLogger.e("Fail to init ADManager");
                }
            }
        });
    }

    static /* synthetic */ int b(InterstitialAD interstitialAD) {
        int i = interstitialAD.f;
        interstitialAD.f = i - 1;
        return i;
    }

    public void closePopupWindow() {
        if (this.f655a != null) {
            this.f655a.closePopupWindow();
        }
    }

    public void destory() {
        if (this.f655a != null) {
            this.f655a.destory();
        }
    }

    public void loadAD() {
        if (!this.c || !this.d) {
            GDTLogger.e("InterstitialAD init Paras OR Context error,See More logs while new InterstitialAD");
        } else if (!this.e) {
            this.f++;
        } else if (this.f655a != null) {
            this.f655a.loadAd();
        } else {
            GDTLogger.e("InterstitialAD Init error,See More Logs");
        }
    }

    public void setADListener(InterstitialADListener interstitialADListener) {
        this.b = interstitialADListener;
    }

    public synchronized void show() {
        if (this.f655a != null) {
            this.f655a.show();
        }
    }

    public synchronized void show(Activity activity) {
        if (this.f655a != null) {
            this.f655a.show(activity);
        }
    }

    public synchronized void showAsPopupWindow() {
        if (this.f655a != null) {
            this.f655a.showAsPopupWindown();
        }
    }

    public synchronized void showAsPopupWindow(Activity activity) {
        if (this.f655a != null) {
            this.f655a.showAsPopupWindown(activity);
        }
    }
}
