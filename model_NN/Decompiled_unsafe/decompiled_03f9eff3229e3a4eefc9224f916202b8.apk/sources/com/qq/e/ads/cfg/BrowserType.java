package com.qq.e.ads.cfg;

public enum BrowserType {
    Default(0),
    Inner(1),
    Sys(2);
    

    /* renamed from: a  reason: collision with root package name */
    private final int f651a;

    private BrowserType(int i) {
        this.f651a = i;
    }

    public final int value() {
        return this.f651a;
    }
}
