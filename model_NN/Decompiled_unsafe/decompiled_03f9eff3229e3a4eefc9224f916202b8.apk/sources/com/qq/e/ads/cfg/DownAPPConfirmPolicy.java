package com.qq.e.ads.cfg;

public enum DownAPPConfirmPolicy {
    Default(0),
    NOConfirm(2);
    

    /* renamed from: a  reason: collision with root package name */
    private final int f652a;

    private DownAPPConfirmPolicy(int i) {
        this.f652a = i;
    }

    public final int value() {
        return this.f652a;
    }
}
