package com.qq.e.ads.appwall;

import android.app.Activity;
import android.view.View;
import com.qq.e.comm.a;
import com.qq.e.comm.constants.ErrorCode;
import com.qq.e.comm.managers.GDTADManager;
import com.qq.e.comm.pi.GWI;
import com.qq.e.comm.util.GDTLogger;
import com.qq.e.comm.util.StringUtil;

public final class GridAPPWall {

    /* renamed from: a  reason: collision with root package name */
    private GWI f644a;

    public GridAPPWall(Activity activity, String str, String str2, GridAPPWallListener gridAPPWallListener) {
        if (StringUtil.isEmpty(str) || StringUtil.isEmpty(str2) || activity == null) {
            GDTLogger.e(String.format("GridAPPWall ADView Contructor paras error,appid=%s,posId=%s,context=%s", str, str2, activity));
        } else if (!a.a(activity)) {
            GDTLogger.e("Required Activity/Service/Permission Not Declared in AndroidManifest.xml");
            a(gridAPPWallListener, ErrorCode.OtherError.ANDROID_PERMMISON_ERROR);
        } else {
            try {
                if (!GDTADManager.getInstance().initWith(activity.getApplicationContext(), str)) {
                    GDTLogger.e("Fail to Init GDT AD SDK,report logcat info filter by gdt_ad_mob");
                    a(gridAPPWallListener, ErrorCode.InitError.INIT_ADMANGER_ERROR);
                    return;
                }
                this.f644a = GDTADManager.getInstance().getPM().getPOFactory().createGridAppWallView(activity, str, str2);
                if (this.f644a != null) {
                    this.f644a.setAdListener(gridAPPWallListener);
                    return;
                }
                GDTLogger.e("Fail to INIT GDT SDK");
                a(gridAPPWallListener, ErrorCode.InitError.GET_INTERFACE_ERROR);
            } catch (Exception e) {
                GDTLogger.e("Fail to init new appwall plugin", e);
                a(gridAPPWallListener, ErrorCode.InitError.INIT_PLUGIN_ERROR);
            } catch (Throwable th) {
                GDTLogger.e("Unknown Exception", th);
                a(gridAPPWallListener, ErrorCode.OtherError.UNKNOWN_ERROR);
            }
        }
    }

    private static void a(GridAPPWallListener gridAPPWallListener, int i) {
        if (gridAPPWallListener != null) {
            gridAPPWallListener.onNoAD(i);
        }
    }

    public final void show() {
        if (this.f644a != null) {
            this.f644a.show();
        }
    }

    public final void showRelativeTo(int i, int i2) {
        if (this.f644a != null) {
            this.f644a.showRelativeTo(i, i2);
        }
    }

    public final void showRelativeTo(View view) {
        if (this.f644a != null) {
            this.f644a.showRelativeTo(view);
        }
    }
}
