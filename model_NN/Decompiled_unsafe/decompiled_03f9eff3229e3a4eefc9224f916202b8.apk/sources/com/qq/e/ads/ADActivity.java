package com.qq.e.ads;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import com.qq.e.comm.managers.GDTADManager;
import com.qq.e.comm.pi.ACTD;
import com.qq.e.comm.util.GDTLogger;
import com.qq.e.comm.util.StringUtil;

public class ADActivity extends Activity {

    /* renamed from: a  reason: collision with root package name */
    private ACTD f642a;

    public void onBackPressed() {
        super.onBackPressed();
        if (this.f642a != null) {
            this.f642a.onBackPressed();
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        if (this.f642a != null) {
            this.f642a.onConfigurationChanged(configuration);
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        Intent intent = getIntent();
        String string = intent.getExtras().getString(ACTD.DELEGATE_NAME_KEY);
        String string2 = intent.getExtras().getString("appid");
        if (!StringUtil.isEmpty(string) && !StringUtil.isEmpty(string2)) {
            try {
                if (GDTADManager.getInstance().initWith(getApplicationContext(), string2)) {
                    this.f642a = GDTADManager.getInstance().getPM().getPOFactory().getActivityDelegate(string, this);
                    if (this.f642a == null) {
                        GDTLogger.e("Init ADActivity Delegate return null,delegateName" + string);
                    }
                } else {
                    GDTLogger.e("Init GDTADManager fail in AdActivity");
                }
            } catch (Throwable th) {
                GDTLogger.e("Init ADActivity Delegate Faile,DelegateName:" + string, th);
            }
        }
        if (this.f642a != null) {
            this.f642a.onBeforeCreate(bundle);
        } else {
            finish();
        }
        super.onCreate(bundle);
        if (this.f642a != null) {
            this.f642a.onAfterCreate(bundle);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.f642a != null) {
            this.f642a.onDestroy();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.f642a != null) {
            this.f642a.onResume();
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        if (this.f642a != null) {
            this.f642a.onStop();
        }
    }
}
