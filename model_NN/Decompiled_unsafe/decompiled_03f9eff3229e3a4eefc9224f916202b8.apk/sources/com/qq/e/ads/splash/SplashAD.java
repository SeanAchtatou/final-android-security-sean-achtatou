package com.qq.e.ads.splash;

import android.app.Activity;
import android.view.ViewGroup;
import com.qq.e.comm.a;
import com.qq.e.comm.adevent.ADEvent;
import com.qq.e.comm.adevent.ADListener;
import com.qq.e.comm.constants.ErrorCode;
import com.qq.e.comm.managers.GDTADManager;
import com.qq.e.comm.managers.plugin.b;
import com.qq.e.comm.pi.NSPVI;
import com.qq.e.comm.util.GDTLogger;
import com.qq.e.comm.util.StringUtil;

public final class SplashAD {

    /* renamed from: a  reason: collision with root package name */
    private NSPVI f663a;
    /* access modifiers changed from: private */
    public SplashADListener b;

    class ADListenerAdapter implements ADListener {
        private ADListenerAdapter() {
        }

        /* synthetic */ ADListenerAdapter(SplashAD splashAD, byte b) {
            this();
        }

        public void onADEvent(ADEvent aDEvent) {
            if (SplashAD.this.b == null) {
                GDTLogger.e("SplashADListener == null");
                return;
            }
            switch (aDEvent.getType()) {
                case 1:
                    SplashAD.this.b.onADDismissed();
                    return;
                case 2:
                    if (aDEvent.getParas().length != 1 || !(aDEvent.getParas()[0] instanceof Integer)) {
                        GDTLogger.e("Splash ADEvent error,");
                        return;
                    } else {
                        SplashAD.this.b.onNoAD(((Integer) aDEvent.getParas()[0]).intValue());
                        return;
                    }
                case 3:
                    SplashAD.this.b.onADPresent();
                    return;
                case 4:
                    SplashAD.this.b.onADClicked();
                    return;
                default:
                    return;
            }
        }
    }

    public SplashAD(Activity activity, ViewGroup viewGroup, String str, String str2, SplashADListener splashADListener) {
        this(activity, viewGroup, str, str2, splashADListener, 0);
    }

    public SplashAD(Activity activity, ViewGroup viewGroup, String str, String str2, SplashADListener splashADListener, int i) {
        this.b = splashADListener;
        if (StringUtil.isEmpty(str) || StringUtil.isEmpty(str2) || viewGroup == null || activity == null) {
            GDTLogger.e(String.format("SplashAd Constructor paras error,appid=%s,posId=%s,context=%s", str, str2, activity));
            a(splashADListener, ErrorCode.InitError.INIT_AD_ERROR);
        } else if (!a.a(activity)) {
            GDTLogger.e("Required Activity/Service/Permission Not Declared in AndroidManifest.xml");
            a(splashADListener, ErrorCode.InitError.INIT_AD_ERROR);
        } else {
            try {
                if (!GDTADManager.getInstance().initWith(activity, str)) {
                    GDTLogger.e("Fail to Init GDT AD SDK,report logcat info filter by gdt_ad_mob");
                    a(splashADListener, ErrorCode.InitError.INIT_ADMANGER_ERROR);
                    return;
                }
                this.f663a = GDTADManager.getInstance().getPM().getPOFactory().getNativeSplashAdView(activity, str, str2);
                if (this.f663a != null) {
                    this.f663a.setFetchDelay(i);
                    this.f663a.setAdListener(new ADListenerAdapter(this, (byte) 0));
                    this.f663a.fetchAndShowIn(viewGroup);
                    return;
                }
                GDTLogger.e("SplashAdView created by factory return null");
                a(splashADListener, ErrorCode.InitError.GET_INTERFACE_ERROR);
            } catch (b e) {
                GDTLogger.e("Fail to init splash plugin", e);
                a(splashADListener, ErrorCode.InitError.INIT_PLUGIN_ERROR);
            } catch (Throwable th) {
                GDTLogger.e("Unknown Exception", th);
                a(splashADListener, ErrorCode.OtherError.UNKNOWN_ERROR);
            }
        }
    }

    private static void a(SplashADListener splashADListener, int i) {
        if (splashADListener != null) {
            splashADListener.onNoAD(i);
        }
    }
}
