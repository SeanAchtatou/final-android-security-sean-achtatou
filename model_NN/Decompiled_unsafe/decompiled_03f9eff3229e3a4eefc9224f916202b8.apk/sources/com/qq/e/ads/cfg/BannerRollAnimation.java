package com.qq.e.ads.cfg;

public enum BannerRollAnimation {
    Default(0),
    NoAnimation(-1);
    

    /* renamed from: a  reason: collision with root package name */
    private final int f650a;

    private BannerRollAnimation(int i) {
        this.f650a = i;
    }

    public final int value() {
        return this.f650a;
    }
}
