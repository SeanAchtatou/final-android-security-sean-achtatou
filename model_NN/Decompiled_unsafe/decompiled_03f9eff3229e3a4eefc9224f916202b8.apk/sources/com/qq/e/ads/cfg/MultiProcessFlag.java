package com.qq.e.ads.cfg;

import com.qq.e.comm.util.GDTLogger;

public class MultiProcessFlag {

    /* renamed from: a  reason: collision with root package name */
    private static boolean f653a;
    private static boolean b;

    public static boolean isMultiProcess() {
        return f653a;
    }

    public static void setMultiProcess(boolean z) {
        if (!b) {
            b = true;
            f653a = z;
            return;
        }
        GDTLogger.w("MultiProcessFlag has already be setted,reset will not take any effect");
    }
}
