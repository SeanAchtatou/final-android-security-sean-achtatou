package com.qq.e.comm.net;

import com.qq.e.comm.net.NetworkClient;
import com.qq.e.comm.net.rr.Request;
import com.qq.e.comm.net.rr.Response;
import com.qq.e.comm.util.GDTLogger;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.conn.params.ConnPerRouteBean;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpProtocolParams;

public class NetworkClientImpl implements NetworkClient {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final HttpClient f684a;
    private static final NetworkClient b = new NetworkClientImpl();
    private final ExecutorService c = new ThreadPoolExecutor(5, 10, 180, TimeUnit.SECONDS, this.d);
    private PriorityBlockingQueue<Runnable> d = new PriorityBlockingQueue<>(15);

    class NetFutureTask<T> extends FutureTask<T> implements Comparable<NetFutureTask<T>> {

        /* renamed from: a  reason: collision with root package name */
        private final NetworkClient.Priority f686a;

        public NetFutureTask(NetworkClientImpl networkClientImpl, Callable<T> callable, NetworkClient.Priority priority) {
            super(callable);
            this.f686a = priority;
        }

        public int compareTo(NetFutureTask<T> netFutureTask) {
            if (netFutureTask == null) {
                return 1;
            }
            return this.f686a.value() - netFutureTask.f686a.value();
        }
    }

    static class TaskCallable implements Callable<Response> {

        /* renamed from: a  reason: collision with root package name */
        private Request f687a;
        private NetworkCallBack b;

        public TaskCallable(Request request) {
            this(request, null);
        }

        public TaskCallable(Request request, NetworkCallBack networkCallBack) {
            this.f687a = request;
            this.b = networkCallBack;
        }

        private void a(HttpRequestBase httpRequestBase) {
            for (Map.Entry next : this.f687a.getHeaders().entrySet()) {
                httpRequestBase.setHeader((String) next.getKey(), (String) next.getValue());
            }
            httpRequestBase.setHeader("User-Agent", "GDTADNetClient-[" + System.getProperty("http.agent") + "]");
            httpRequestBase.addHeader("Accept-Encoding", "gzip");
            BasicHttpParams params = httpRequestBase.getParams();
            if (params == null) {
                params = new BasicHttpParams();
            }
            if (this.f687a.getConnectionTimeOut() > 0) {
                HttpConnectionParams.setConnectionTimeout(params, this.f687a.getConnectionTimeOut());
            }
            if (this.f687a.getSocketTimeOut() > 0) {
                HttpConnectionParams.setSoTimeout(params, this.f687a.getSocketTimeOut());
            }
            httpRequestBase.setParams(params);
        }

        public Response call() throws Exception {
            HttpUriRequest httpGet;
            Response response = null;
            try {
                HttpClient a2 = NetworkClientImpl.f684a;
                switch (this.f687a.getMethod()) {
                    case POST:
                        httpGet = new HttpPost(this.f687a.getUrlWithParas());
                        a(httpGet);
                        byte[] postData = this.f687a.getPostData();
                        if (postData != null && postData.length > 0) {
                            httpGet.setEntity(new ByteArrayEntity(postData));
                        }
                        response = this.f687a.initResponse(httpGet, a2.execute(httpGet));
                        e = null;
                        break;
                    case GET:
                        httpGet = new HttpGet(this.f687a.getUrlWithParas());
                        a(httpGet);
                        response = this.f687a.initResponse(httpGet, a2.execute(httpGet));
                        e = null;
                        break;
                    default:
                        httpGet = null;
                        response = this.f687a.initResponse(httpGet, a2.execute(httpGet));
                        e = null;
                        break;
                }
            } catch (Exception e) {
                e = e;
            }
            if (e == null) {
                if (this.b != null) {
                    this.b.onResponse(this.f687a, response);
                }
                if (this.f687a.isAutoClose()) {
                    response.close();
                }
            } else if (this.b != null) {
                GDTLogger.w("NetworkClientException", e);
                this.b.onException(e);
                if (response != null) {
                    response.close();
                }
            } else {
                throw e;
            }
            return response;
        }
    }

    static {
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        ConnManagerParams.setTimeout(basicHttpParams, (long) StatisticConfig.MIN_UPLOAD_INTERVAL);
        HttpConnectionParams.setConnectionTimeout(basicHttpParams, 30000);
        HttpConnectionParams.setSoTimeout(basicHttpParams, 30000);
        ConnManagerParams.setMaxConnectionsPerRoute(basicHttpParams, new ConnPerRouteBean(3));
        ConnManagerParams.setMaxTotalConnections(basicHttpParams, 10);
        HttpProtocolParams.setVersion(basicHttpParams, HttpVersion.HTTP_1_1);
        HttpProtocolParams.setContentCharset(basicHttpParams, "UTF-8");
        HttpProtocolParams.setUserAgent(basicHttpParams, "GDTADNetClient-[" + System.getProperty("http.agent") + "]");
        SchemeRegistry schemeRegistry = new SchemeRegistry();
        schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
        f684a = new DefaultHttpClient(new ThreadSafeClientConnManager(basicHttpParams, schemeRegistry), basicHttpParams);
    }

    private NetworkClientImpl() {
    }

    public static NetworkClient getInstance() {
        return b;
    }

    public Future<Response> submit(Request request) {
        return submit(request, NetworkClient.Priority.Mid);
    }

    public Future<Response> submit(Request request, NetworkClient.Priority priority) {
        NetFutureTask netFutureTask = new NetFutureTask(this, new TaskCallable(request), priority);
        this.c.execute(netFutureTask);
        GDTLogger.d("QueueSize:" + this.d.size());
        return netFutureTask;
    }

    public void submit(Request request, NetworkCallBack networkCallBack) {
        submit(request, NetworkClient.Priority.Mid, networkCallBack);
    }

    public void submit(Request request, NetworkClient.Priority priority, NetworkCallBack networkCallBack) {
        this.c.execute(new NetFutureTask(this, new TaskCallable(request, networkCallBack), priority));
        GDTLogger.d("QueueSize:" + this.d.size());
    }
}
