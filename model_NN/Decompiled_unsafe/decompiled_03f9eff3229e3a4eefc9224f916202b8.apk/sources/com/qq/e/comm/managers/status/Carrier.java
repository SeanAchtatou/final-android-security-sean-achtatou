package com.qq.e.comm.managers.status;

public enum Carrier {
    UNKNOWN(0),
    CMCC(1),
    UNICOM(2),
    TELECOM(3);
    

    /* renamed from: a  reason: collision with root package name */
    private int f679a;

    private Carrier(int i) {
        this.f679a = i;
    }

    public final int getValue() {
        return this.f679a;
    }
}
