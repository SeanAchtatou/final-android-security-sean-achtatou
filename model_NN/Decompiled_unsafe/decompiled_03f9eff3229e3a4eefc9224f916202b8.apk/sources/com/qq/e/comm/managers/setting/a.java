package com.qq.e.comm.managers.setting;

import android.content.Context;
import android.util.Base64;
import android.util.Pair;
import com.qq.e.comm.constants.Constants;
import com.qq.e.comm.util.GDTLogger;
import com.qq.e.comm.util.StringUtil;
import java.io.File;
import java.io.IOException;
import org.json.JSONException;
import org.json.JSONObject;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private JSONObject f675a;

    public a() {
        this.f675a = new JSONObject();
    }

    public a(String str) {
        this();
        GDTLogger.d("Initialize GDTAPPSetting,Json=" + str);
        if (!StringUtil.isEmpty(str)) {
            try {
                this.f675a = new JSONObject(str);
            } catch (JSONException e) {
                GDTLogger.e("JsonException While build GDTAPPSetting Instance from JSON", e);
            }
        }
    }

    private static Pair<String, String> a(Context context, String str) {
        File dir = context.getDir(Constants.SETTING.SETTINGDIR, 0);
        if (!dir.exists()) {
            return null;
        }
        File file = new File(dir, str + ".sig");
        File file2 = new File(dir, str + ".cfg");
        if (!file.exists() || !file2.exists()) {
            return null;
        }
        try {
            return new Pair<>(StringUtil.readAll(file), StringUtil.readAll(file2));
        } catch (IOException e) {
            return null;
        }
    }

    public static com.qq.e.comm.a a(Context context) {
        Pair<String, String> a2 = a(context, Constants.SETTING.DEV_CLOUD_SETTING);
        if (a2 == null) {
            return null;
        }
        try {
            if (com.qq.e.comm.util.a.a().a((String) a2.first, (String) a2.second)) {
                return new com.qq.e.comm.a((String) a2.first, new a(new String(Base64.decode((String) a2.second, 0), "UTF-8")));
            }
            GDTLogger.e("verify local dev cloud setting fail");
            return null;
        } catch (Throwable th) {
            GDTLogger.e("exception while loading local dev cloud setting", th);
            return null;
        }
    }

    public static boolean a(Context context, String str, String str2) {
        return a(context, Constants.SETTING.SDK_CLOUD_SETTING, str, str2);
    }

    private static boolean a(Context context, String str, String str2, String str3) {
        if (StringUtil.isEmpty(str2) || StringUtil.isEmpty(str3)) {
            GDTLogger.e(String.format("Fail to update Cloud setting due to sig or setting is empty,name=%s\tsig=%s\tsetting=%s", str, str2, str3));
            return false;
        } else if (com.qq.e.comm.util.a.a().a(str2, str3)) {
            return b(context, str, str2, str3);
        } else {
            GDTLogger.e(String.format("Fail to update Cloud setting due to sig verify fail,name=%s\tsig=%s\tsetting=%s", str, str2, str3));
            return false;
        }
    }

    public static d b(Context context) {
        Pair<String, String> a2 = a(context, Constants.SETTING.SDK_CLOUD_SETTING);
        if (a2 == null) {
            return null;
        }
        try {
            if (com.qq.e.comm.util.a.a().a((String) a2.first, (String) a2.second)) {
                return new d((String) a2.first, new c(new String(Base64.decode((String) a2.second, 0), "UTF-8")), (byte) 0);
            }
            GDTLogger.e("verify local sdk cloud setting fail");
            return null;
        } catch (Throwable th) {
            GDTLogger.e("exception while loading local sdk cloud setting", th);
            return null;
        }
    }

    public static boolean b(Context context, String str, String str2) {
        return a(context, Constants.SETTING.DEV_CLOUD_SETTING, str, str2);
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0063 A[SYNTHETIC, Splitter:B:18:0x0063] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0068 A[Catch:{ Exception -> 0x006c }] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0072 A[SYNTHETIC, Splitter:B:26:0x0072] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0077 A[Catch:{ Exception -> 0x007b }] */
    /* JADX WARNING: Removed duplicated region for block: B:42:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean b(android.content.Context r7, java.lang.String r8, java.lang.String r9, java.lang.String r10) {
        /*
            r2 = 0
            r0 = 0
            java.lang.String r1 = "e_qq_com_setting"
            java.io.File r1 = r7.getDir(r1, r0)
            boolean r3 = r1.exists()
            if (r3 != 0) goto L_0x0011
            r1.mkdirs()
        L_0x0011:
            java.io.File r4 = new java.io.File
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.StringBuilder r3 = r3.append(r8)
            java.lang.String r5 = ".cfg"
            java.lang.StringBuilder r3 = r3.append(r5)
            java.lang.String r3 = r3.toString()
            r4.<init>(r1, r3)
            java.io.File r5 = new java.io.File
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.StringBuilder r3 = r3.append(r8)
            java.lang.String r6 = ".sig"
            java.lang.StringBuilder r3 = r3.append(r6)
            java.lang.String r3 = r3.toString()
            r5.<init>(r1, r3)
            java.io.FileWriter r3 = new java.io.FileWriter     // Catch:{ Exception -> 0x0059, all -> 0x006e }
            r3.<init>(r4)     // Catch:{ Exception -> 0x0059, all -> 0x006e }
            r3.write(r10)     // Catch:{ Exception -> 0x0086, all -> 0x007d }
            java.io.FileWriter r1 = new java.io.FileWriter     // Catch:{ Exception -> 0x0086, all -> 0x007d }
            r1.<init>(r5)     // Catch:{ Exception -> 0x0086, all -> 0x007d }
            r1.write(r9)     // Catch:{ Exception -> 0x008a, all -> 0x007f }
            r3.close()     // Catch:{ Exception -> 0x008d }
            r1.close()     // Catch:{ Exception -> 0x008d }
        L_0x0057:
            r0 = 1
        L_0x0058:
            return r0
        L_0x0059:
            r1 = move-exception
            r1 = r2
        L_0x005b:
            r4.delete()     // Catch:{ all -> 0x0082 }
            r5.delete()     // Catch:{ all -> 0x0082 }
            if (r2 == 0) goto L_0x0066
            r2.close()     // Catch:{ Exception -> 0x006c }
        L_0x0066:
            if (r1 == 0) goto L_0x0058
            r1.close()     // Catch:{ Exception -> 0x006c }
            goto L_0x0058
        L_0x006c:
            r1 = move-exception
            goto L_0x0058
        L_0x006e:
            r0 = move-exception
            r3 = r2
        L_0x0070:
            if (r3 == 0) goto L_0x0075
            r3.close()     // Catch:{ Exception -> 0x007b }
        L_0x0075:
            if (r2 == 0) goto L_0x007a
            r2.close()     // Catch:{ Exception -> 0x007b }
        L_0x007a:
            throw r0
        L_0x007b:
            r1 = move-exception
            goto L_0x007a
        L_0x007d:
            r0 = move-exception
            goto L_0x0070
        L_0x007f:
            r0 = move-exception
            r2 = r1
            goto L_0x0070
        L_0x0082:
            r0 = move-exception
            r3 = r2
            r2 = r1
            goto L_0x0070
        L_0x0086:
            r1 = move-exception
            r1 = r2
            r2 = r3
            goto L_0x005b
        L_0x008a:
            r2 = move-exception
            r2 = r3
            goto L_0x005b
        L_0x008d:
            r0 = move-exception
            goto L_0x0057
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qq.e.comm.managers.setting.a.b(android.content.Context, java.lang.String, java.lang.String, java.lang.String):boolean");
    }

    /* access modifiers changed from: package-private */
    public final Object a(String str) {
        return this.f675a.opt(str);
    }

    /* access modifiers changed from: package-private */
    public final Object a(String str, String str2) {
        JSONObject optJSONObject = this.f675a.optJSONObject(Constants.KEYS.PLACEMENTS);
        JSONObject optJSONObject2 = optJSONObject != null ? optJSONObject.optJSONObject(str2) : null;
        if (optJSONObject2 != null) {
            return optJSONObject2.opt(str);
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public final void a(String str, Object obj) {
        try {
            this.f675a.putOpt(str, obj);
        } catch (JSONException e) {
            GDTLogger.e("Exception while update setting", e);
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(String str, Object obj, String str2) {
        JSONObject jSONObject;
        try {
            JSONObject optJSONObject = this.f675a.optJSONObject(Constants.KEYS.PLACEMENTS);
            if (optJSONObject == null) {
                JSONObject jSONObject2 = new JSONObject();
                this.f675a.putOpt(Constants.KEYS.PLACEMENTS, jSONObject2);
                jSONObject = jSONObject2;
            } else {
                jSONObject = optJSONObject;
            }
            JSONObject optJSONObject2 = jSONObject != null ? jSONObject.optJSONObject(str2) : null;
            if (optJSONObject2 == null) {
                optJSONObject2 = new JSONObject();
                jSONObject.putOpt(str2, optJSONObject2);
            }
            if (obj == null) {
                optJSONObject2.remove(str);
            } else {
                optJSONObject2.putOpt(str, obj);
            }
        } catch (JSONException e) {
            GDTLogger.e("Exception while update setting", e);
        }
    }
}
