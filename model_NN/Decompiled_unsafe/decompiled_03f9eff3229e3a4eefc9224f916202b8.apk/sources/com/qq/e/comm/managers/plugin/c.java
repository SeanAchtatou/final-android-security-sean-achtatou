package com.qq.e.comm.managers.plugin;

import android.content.Context;
import com.qq.e.comm.util.FileUtil;
import com.qq.e.comm.util.GDTLogger;
import com.qq.e.comm.util.Md5Util;
import com.qq.e.comm.util.StringUtil;
import com.qq.e.comm.util.a;
import java.io.File;

class c {

    /* renamed from: a  reason: collision with root package name */
    private final File f673a;
    private final File b;
    private String c;
    private int d;

    public c(File file, File file2) {
        this.f673a = file;
        this.b = file2;
    }

    static File a(Context context) {
        return new File(context.getDir("e_qq_com_plugin", 0), "gdt_plugin.jar");
    }

    static File b(Context context) {
        return new File(context.getDir("e_qq_com_plugin", 0), "gdt_plugin.next");
    }

    static File c(Context context) {
        return new File(context.getDir("e_qq_com_plugin", 0), "gdt_plugin.jar.sig");
    }

    static File d(Context context) {
        return new File(context.getDir("e_qq_com_plugin", 0), "gdt_plugin.next.sig");
    }

    /* access modifiers changed from: package-private */
    public final boolean a() {
        try {
            if (!this.b.exists() || !this.f673a.exists()) {
                return false;
            }
            String[] split = StringUtil.readAll(this.b).split("#####");
            if (split.length != 2) {
                return false;
            }
            String str = split[1];
            int parseInteger = StringUtil.parseInteger(split[0], 0);
            a a2 = a.a();
            File file = this.f673a;
            if (!((file == null || !file.exists()) ? false : a2.b(str, Md5Util.encode(file)))) {
                return false;
            }
            this.c = str;
            this.d = parseInteger;
            return true;
        } catch (Throwable th) {
            GDTLogger.report("Exception while checking plugin", th);
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public final boolean a(File file, File file2) {
        if (file == null || file2 == null) {
            return false;
        }
        if (file.equals(this.f673a) || FileUtil.renameTo(this.f673a, file)) {
            return file2.equals(this.b) || FileUtil.renameTo(this.b, file2);
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public final int b() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public final String c() {
        return this.c;
    }
}
