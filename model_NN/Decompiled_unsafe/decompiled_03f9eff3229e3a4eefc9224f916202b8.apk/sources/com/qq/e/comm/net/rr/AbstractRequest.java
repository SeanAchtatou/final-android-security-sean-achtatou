package com.qq.e.comm.net.rr;

import android.net.Uri;
import com.qq.e.comm.net.rr.Request;
import com.qq.e.comm.util.StringUtil;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public abstract class AbstractRequest implements Request {

    /* renamed from: a  reason: collision with root package name */
    private boolean f688a = true;
    private int b;
    private int c;
    private int d;
    private String e;
    private Map<String, String> f = new HashMap();
    private Map<String, String> g = new HashMap();
    private Map<String, String> h = Collections.unmodifiableMap(this.f);
    private Map<String, String> i = Collections.unmodifiableMap(this.g);
    private Request.Method j;
    private byte[] k;

    public AbstractRequest(String str, Request.Method method, byte[] bArr) {
        this.e = str;
        this.j = method;
        if (bArr == null) {
            this.k = null;
        } else {
            this.k = (byte[]) bArr.clone();
        }
    }

    public void addHeader(String str, String str2) {
        if (!StringUtil.isEmpty(str) && !StringUtil.isEmpty(str2)) {
            this.f.put(str, str2);
        }
    }

    public void addQuery(String str, String str2) {
        this.g.put(str, str2);
    }

    public int getConnectionTimeOut() {
        return this.c;
    }

    public Map<String, String> getHeaders() {
        return this.h;
    }

    public Request.Method getMethod() {
        return this.j;
    }

    public byte[] getPostData() throws Exception {
        return this.k;
    }

    public int getPriority() {
        return this.b;
    }

    public Map<String, String> getQuerys() {
        return this.i;
    }

    public int getSocketTimeOut() {
        return this.d;
    }

    public String getUrl() {
        return this.e;
    }

    public String getUrlWithParas() {
        if (getQuerys().isEmpty()) {
            return getUrl();
        }
        Uri.Builder buildUpon = Uri.parse(getUrl()).buildUpon();
        for (Map.Entry next : getQuerys().entrySet()) {
            buildUpon.appendQueryParameter((String) next.getKey(), (String) next.getValue());
        }
        return buildUpon.build().toString();
    }

    public boolean isAutoClose() {
        return this.f688a;
    }

    public void setAutoClose(boolean z) {
        this.f688a = z;
    }

    public void setConnectionTimeOut(int i2) {
        this.c = i2;
    }

    public void setPriority(int i2) {
        this.b = i2;
    }

    public void setSocketTimeOut(int i2) {
        this.d = i2;
    }
}
