package com.qq.e.comm.managers.setting;

import com.qq.e.comm.util.GDTLogger;
import com.qq.e.comm.util.StringUtil;
import org.json.JSONException;
import org.json.JSONObject;

public class c {

    /* renamed from: a  reason: collision with root package name */
    private JSONObject f676a;

    public c() {
        this.f676a = new JSONObject();
    }

    public c(String str) {
        this();
        GDTLogger.d("Initialize GDTSDKSetting,Json=" + str);
        if (!StringUtil.isEmpty(str)) {
            try {
                this.f676a = new JSONObject(str);
            } catch (JSONException e) {
                GDTLogger.report("Exception while building GDTSDKSetting from json", e);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final Object a(String str) {
        return this.f676a.opt(str);
    }

    /* access modifiers changed from: package-private */
    public final void a(String str, Object obj) {
        try {
            this.f676a.putOpt(str, obj);
        } catch (JSONException e) {
            GDTLogger.e("Exception while update setting", e);
        }
    }

    public String toString() {
        return "GDTSDKSetting[" + this.f676a.toString() + "]";
    }
}
