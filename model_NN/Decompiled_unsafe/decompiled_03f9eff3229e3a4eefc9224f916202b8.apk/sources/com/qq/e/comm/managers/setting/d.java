package com.qq.e.comm.managers.setting;

public final class d {

    /* renamed from: a  reason: collision with root package name */
    private String f677a;
    private c b;

    private d(String str, c cVar) {
        this.f677a = str;
        this.b = cVar;
    }

    /* synthetic */ d(String str, c cVar, byte b2) {
        this(str, cVar);
    }

    public final String a() {
        return this.f677a;
    }

    public final c b() {
        return this.b;
    }
}
