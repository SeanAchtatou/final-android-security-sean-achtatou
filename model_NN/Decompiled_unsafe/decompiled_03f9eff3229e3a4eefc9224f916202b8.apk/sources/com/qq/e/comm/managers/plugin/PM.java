package com.qq.e.comm.managers.plugin;

import android.content.Context;
import com.qq.e.comm.pi.POFactory;
import com.qq.e.comm.util.GDTLogger;
import com.qq.e.comm.util.StringUtil;
import dalvik.system.DexClassLoader;
import java.io.File;
import java.io.RandomAccessFile;
import java.nio.channels.FileLock;
import java.util.HashMap;
import java.util.Map;

public class PM {
    private static final Map<Class<?>, String> i = new HashMap<Class<?>, String>() {
        {
            put(POFactory.class, "com.qq.e.comm.plugin.POFactoryImpl");
        }
    };

    /* renamed from: a  reason: collision with root package name */
    private final Context f670a;
    private String b;
    private File c;
    private int d;
    private DexClassLoader e;
    private RandomAccessFile f;
    private FileLock g;
    private boolean h = b();

    public PM(Context context) {
        this.f670a = context.getApplicationContext();
        if (a()) {
            GDTLogger.d("PluginFile:\t" + (this.c == null ? "null" : this.c.getAbsolutePath()));
            if (this.b != null) {
                try {
                    this.e = new DexClassLoader(this.c.getAbsolutePath(), this.f670a.getDir("e_qq_com_dex", 0).getAbsolutePath(), null, getClass().getClassLoader());
                } catch (Throwable th) {
                    GDTLogger.e("exception while init plugin class loader", th);
                }
            } else {
                this.e = null;
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0096, code lost:
        if (r2 != false) goto L_0x0098;
     */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0091 A[Catch:{ Throwable -> 0x00f3, all -> 0x0110 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean a() {
        /*
            r7 = this;
            r6 = 529(0x211, float:7.41E-43)
            r1 = 1
            r0 = 0
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00f3 }
            java.lang.String r3 = "TimeStap_BEFORE_PLUGIN_INIT:"
            r2.<init>(r3)     // Catch:{ Throwable -> 0x00f3 }
            long r4 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x00f3 }
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Throwable -> 0x00f3 }
            java.lang.String r2 = r2.toString()     // Catch:{ Throwable -> 0x00f3 }
            com.qq.e.comm.util.GDTLogger.d(r2)     // Catch:{ Throwable -> 0x00f3 }
            boolean r2 = r7.h     // Catch:{ Throwable -> 0x00f3 }
            if (r2 == 0) goto L_0x0057
            com.qq.e.comm.managers.plugin.c r2 = new com.qq.e.comm.managers.plugin.c     // Catch:{ Throwable -> 0x00f3 }
            android.content.Context r3 = r7.f670a     // Catch:{ Throwable -> 0x00f3 }
            java.io.File r3 = com.qq.e.comm.managers.plugin.c.b(r3)     // Catch:{ Throwable -> 0x00f3 }
            android.content.Context r4 = r7.f670a     // Catch:{ Throwable -> 0x00f3 }
            java.io.File r4 = com.qq.e.comm.managers.plugin.c.d(r4)     // Catch:{ Throwable -> 0x00f3 }
            r2.<init>(r3, r4)     // Catch:{ Throwable -> 0x00f3 }
            boolean r3 = r2.a()     // Catch:{ Throwable -> 0x00f3 }
            if (r3 == 0) goto L_0x0057
            android.content.Context r3 = r7.f670a     // Catch:{ Throwable -> 0x00f3 }
            java.io.File r3 = com.qq.e.comm.managers.plugin.c.a(r3)     // Catch:{ Throwable -> 0x00f3 }
            android.content.Context r4 = r7.f670a     // Catch:{ Throwable -> 0x00f3 }
            java.io.File r4 = com.qq.e.comm.managers.plugin.c.c(r4)     // Catch:{ Throwable -> 0x00f3 }
            boolean r2 = r2.a(r3, r4)     // Catch:{ Throwable -> 0x00f3 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00f3 }
            java.lang.String r4 = "NextExist,Updated="
            r3.<init>(r4)     // Catch:{ Throwable -> 0x00f3 }
            java.lang.StringBuilder r2 = r3.append(r2)     // Catch:{ Throwable -> 0x00f3 }
            java.lang.String r2 = r2.toString()     // Catch:{ Throwable -> 0x00f3 }
            com.qq.e.comm.util.GDTLogger.d(r2)     // Catch:{ Throwable -> 0x00f3 }
        L_0x0057:
            com.qq.e.comm.managers.plugin.c r2 = new com.qq.e.comm.managers.plugin.c     // Catch:{ Throwable -> 0x00f3 }
            android.content.Context r3 = r7.f670a     // Catch:{ Throwable -> 0x00f3 }
            java.io.File r3 = com.qq.e.comm.managers.plugin.c.a(r3)     // Catch:{ Throwable -> 0x00f3 }
            android.content.Context r4 = r7.f670a     // Catch:{ Throwable -> 0x00f3 }
            java.io.File r4 = com.qq.e.comm.managers.plugin.c.c(r4)     // Catch:{ Throwable -> 0x00f3 }
            r2.<init>(r3, r4)     // Catch:{ Throwable -> 0x00f3 }
            boolean r3 = r2.a()     // Catch:{ Throwable -> 0x00f3 }
            if (r3 == 0) goto L_0x008e
            int r3 = r2.b()     // Catch:{ Throwable -> 0x00f3 }
            if (r3 >= r6) goto L_0x00b0
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00f3 }
            java.lang.String r3 = "last updated plugin version ="
            r2.<init>(r3)     // Catch:{ Throwable -> 0x00f3 }
            int r3 = r7.d     // Catch:{ Throwable -> 0x00f3 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Throwable -> 0x00f3 }
            java.lang.String r3 = ";asset plugin version=529"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Throwable -> 0x00f3 }
            java.lang.String r2 = r2.toString()     // Catch:{ Throwable -> 0x00f3 }
            com.qq.e.comm.util.GDTLogger.d(r2)     // Catch:{ Throwable -> 0x00f3 }
        L_0x008e:
            r2 = r0
        L_0x008f:
            if (r2 != 0) goto L_0x0098
            boolean r2 = r7.h     // Catch:{ Throwable -> 0x00f3 }
            if (r2 != 0) goto L_0x00c6
            r2 = r0
        L_0x0096:
            if (r2 == 0) goto L_0x0099
        L_0x0098:
            r0 = r1
        L_0x0099:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "TimeStap_AFTER_PLUGIN_INIT:"
            r1.<init>(r2)
            long r2 = java.lang.System.currentTimeMillis()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            com.qq.e.comm.util.GDTLogger.d(r1)
        L_0x00af:
            return r0
        L_0x00b0:
            java.lang.String r3 = r2.c()     // Catch:{ Throwable -> 0x00f3 }
            r7.b = r3     // Catch:{ Throwable -> 0x00f3 }
            int r2 = r2.b()     // Catch:{ Throwable -> 0x00f3 }
            r7.d = r2     // Catch:{ Throwable -> 0x00f3 }
            android.content.Context r2 = r7.f670a     // Catch:{ Throwable -> 0x00f3 }
            java.io.File r2 = com.qq.e.comm.managers.plugin.c.a(r2)     // Catch:{ Throwable -> 0x00f3 }
            r7.c = r2     // Catch:{ Throwable -> 0x00f3 }
            r2 = r1
            goto L_0x008f
        L_0x00c6:
            android.content.Context r2 = r7.f670a     // Catch:{ Throwable -> 0x00f3 }
            android.content.Context r3 = r7.f670a     // Catch:{ Throwable -> 0x00f3 }
            java.io.File r3 = com.qq.e.comm.managers.plugin.c.a(r3)     // Catch:{ Throwable -> 0x00f3 }
            android.content.Context r4 = r7.f670a     // Catch:{ Throwable -> 0x00f3 }
            java.io.File r4 = com.qq.e.comm.managers.plugin.c.c(r4)     // Catch:{ Throwable -> 0x00f3 }
            boolean r2 = com.qq.e.comm.a.a(r2, r3, r4)     // Catch:{ Throwable -> 0x00f3 }
            if (r2 == 0) goto L_0x00ec
            java.lang.String r2 = "j5xPbkg9o6V1HCy4AS+zFW/ebEAFelrDFzrJs+MlmiTA40oLR1nSuq1fhq5tlzvoQtis1/GcwiuIRBVEGk9W4MCSENfzHcuPDBWnK/HDNtFWO3EhjrHMkeFU2Y0w8WIvfWb7sq27v9ctBhDPcDYt5cALp4z8cHGo5/OZRuAndAs="
            r7.b = r2     // Catch:{ Throwable -> 0x00f3 }
            android.content.Context r2 = r7.f670a     // Catch:{ Throwable -> 0x00f3 }
            java.io.File r2 = com.qq.e.comm.managers.plugin.c.a(r2)     // Catch:{ Throwable -> 0x00f3 }
            r7.c = r2     // Catch:{ Throwable -> 0x00f3 }
            r2 = 529(0x211, float:7.41E-43)
            r7.d = r2     // Catch:{ Throwable -> 0x00f3 }
            r2 = r1
            goto L_0x0096
        L_0x00ec:
            java.lang.String r2 = "Fail to prepair Defult plugin "
            com.qq.e.comm.util.GDTLogger.e(r2)     // Catch:{ Throwable -> 0x00f3 }
            r2 = r0
            goto L_0x0096
        L_0x00f3:
            r1 = move-exception
            java.lang.String r2 = "Exception while init plugin manager"
            com.qq.e.comm.util.GDTLogger.report(r2, r1)     // Catch:{ all -> 0x0110 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "TimeStap_AFTER_PLUGIN_INIT:"
            r1.<init>(r2)
            long r2 = java.lang.System.currentTimeMillis()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            com.qq.e.comm.util.GDTLogger.d(r1)
            goto L_0x00af
        L_0x0110:
            r0 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "TimeStap_AFTER_PLUGIN_INIT:"
            r1.<init>(r2)
            long r2 = java.lang.System.currentTimeMillis()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            com.qq.e.comm.util.GDTLogger.d(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qq.e.comm.managers.plugin.PM.a():boolean");
    }

    private boolean b() {
        try {
            File file = new File(this.f670a.getDir("e_qq_com_plugin", 0), "update_lc");
            if (!file.exists()) {
                file.createNewFile();
                StringUtil.writeTo("lock", file);
            }
            if (!file.exists()) {
                return false;
            }
            this.f = new RandomAccessFile(file, "rw");
            this.g = this.f.getChannel().tryLock();
            if (this.g == null) {
                return false;
            }
            this.f.writeByte(37);
            return true;
        } catch (Throwable th) {
            return false;
        }
    }

    public <T> T getFactory(Class<T> cls) throws b {
        GDTLogger.d("GetFactoryInstaceforInterface:" + cls);
        DexClassLoader dexClassLoader = this.e;
        if (dexClassLoader == null) {
            throw new b("Fail to init GDTADPLugin,PluginClassLoader == null;while loading factory impl for:" + cls);
        }
        try {
            String str = i.get(cls);
            if (StringUtil.isEmpty(str)) {
                throw new b("factory  implemention name is not specified for interface:" + cls.getName());
            }
            Class<?> loadClass = dexClassLoader.loadClass(str);
            return cls.cast(loadClass.getDeclaredMethod("getInstance", new Class[0]).invoke(loadClass, new Object[0]));
        } catch (Throwable th) {
            throw new b("Fail to getfactory implement instance for interface:" + cls.getName(), th);
        }
    }

    public String getLocalSig() {
        return this.b;
    }

    public POFactory getPOFactory() throws b {
        return (POFactory) getFactory(POFactory.class);
    }

    public int getPluginVersion() {
        return this.d;
    }

    public void update(String str, String str2) {
        if (this.h) {
            new a(this.f670a).a(str, str2);
        }
    }
}
