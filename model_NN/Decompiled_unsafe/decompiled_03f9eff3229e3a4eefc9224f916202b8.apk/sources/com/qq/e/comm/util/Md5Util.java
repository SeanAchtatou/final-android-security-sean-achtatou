package com.qq.e.comm.util;

import android.util.Base64;
import com.tencent.connect.common.Constants;
import java.security.MessageDigest;

public class Md5Util {

    /* renamed from: a  reason: collision with root package name */
    private static final String[] f698a = {"0", "1", "2", "3", "4", "5", Constants.VIA_SHARE_TYPE_INFO, "7", "8", "9", "a", "b", "c", "d", "e", "f"};

    /* JADX WARN: Failed to insert an additional move for type inference into block B:11:0x000f */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:10:0x000f */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v2, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v7, resolved type: int} */
    /* JADX WARNING: Incorrect type for immutable var: ssa=byte, code=int, for r1v1, types: [byte, int] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String byteArrayToHexString(byte[] r6) {
        /*
            java.lang.StringBuffer r2 = new java.lang.StringBuffer
            r2.<init>()
            r0 = 0
        L_0x0006:
            int r1 = r6.length
            if (r0 >= r1) goto L_0x0032
            byte r1 = r6[r0]
            if (r1 >= 0) goto L_0x000f
            int r1 = r1 + 256
        L_0x000f:
            int r3 = r1 / 16
            int r1 = r1 % 16
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String[] r5 = com.qq.e.comm.util.Md5Util.f698a
            r3 = r5[r3]
            java.lang.StringBuilder r3 = r4.append(r3)
            java.lang.String[] r4 = com.qq.e.comm.util.Md5Util.f698a
            r1 = r4[r1]
            java.lang.StringBuilder r1 = r3.append(r1)
            java.lang.String r1 = r1.toString()
            r2.append(r1)
            int r0 = r0 + 1
            goto L_0x0006
        L_0x0032:
            java.lang.String r0 = r2.toString()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qq.e.comm.util.Md5Util.byteArrayToHexString(byte[]):java.lang.String");
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x003e A[SYNTHETIC, Splitter:B:25:0x003e] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String encode(java.io.File r6) {
        /*
            if (r6 != 0) goto L_0x0005
            java.lang.String r0 = ""
        L_0x0004:
            return r0
        L_0x0005:
            r0 = 0
            java.lang.String r1 = "MD5"
            java.security.MessageDigest r2 = java.security.MessageDigest.getInstance(r1)     // Catch:{ Exception -> 0x0048, all -> 0x0038 }
            java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0048, all -> 0x0038 }
            r1.<init>(r6)     // Catch:{ Exception -> 0x0048, all -> 0x0038 }
            r0 = 1024(0x400, float:1.435E-42)
            byte[] r0 = new byte[r0]     // Catch:{ Exception -> 0x0020, all -> 0x0046 }
        L_0x0015:
            int r3 = r1.read(r0)     // Catch:{ Exception -> 0x0020, all -> 0x0046 }
            if (r3 <= 0) goto L_0x002a
            r4 = 0
            r2.update(r0, r4, r3)     // Catch:{ Exception -> 0x0020, all -> 0x0046 }
            goto L_0x0015
        L_0x0020:
            r0 = move-exception
            r0 = r1
        L_0x0022:
            if (r0 == 0) goto L_0x0027
            r0.close()     // Catch:{ Exception -> 0x0042 }
        L_0x0027:
            java.lang.String r0 = ""
            goto L_0x0004
        L_0x002a:
            byte[] r0 = r2.digest()     // Catch:{ Exception -> 0x0020, all -> 0x0046 }
            java.lang.String r0 = byteArrayToHexString(r0)     // Catch:{ Exception -> 0x0020, all -> 0x0046 }
            r1.close()     // Catch:{ Exception -> 0x0036 }
            goto L_0x0004
        L_0x0036:
            r1 = move-exception
            goto L_0x0004
        L_0x0038:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x003c:
            if (r1 == 0) goto L_0x0041
            r1.close()     // Catch:{ Exception -> 0x0044 }
        L_0x0041:
            throw r0
        L_0x0042:
            r0 = move-exception
            goto L_0x0027
        L_0x0044:
            r1 = move-exception
            goto L_0x0041
        L_0x0046:
            r0 = move-exception
            goto L_0x003c
        L_0x0048:
            r1 = move-exception
            goto L_0x0022
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qq.e.comm.util.Md5Util.encode(java.io.File):java.lang.String");
    }

    public static String encode(String str) {
        try {
            String str2 = new String(str);
            try {
                return byteArrayToHexString(MessageDigest.getInstance("MD5").digest(str2.getBytes()));
            } catch (Exception e) {
                return str2;
            }
        } catch (Exception e2) {
            return null;
        }
    }

    public static String encodeBase64String(String str) {
        try {
            return byteArrayToHexString(MessageDigest.getInstance("MD5").digest(Base64.decode(str, 0)));
        } catch (Exception e) {
            GDTLogger.e("Exception while md5 base64String", e);
            return null;
        }
    }

    public static byte[] hexStringtoByteArray(String str) {
        if (str.length() % 2 != 0) {
            return null;
        }
        byte[] bArr = new byte[(str.length() / 2)];
        for (int i = 0; i < str.length() - 1; i += 2) {
            char charAt = str.charAt(i);
            char charAt2 = str.charAt(i + 1);
            char lowerCase = Character.toLowerCase(charAt);
            char lowerCase2 = Character.toLowerCase(charAt2);
            int i2 = (lowerCase <= '9' ? lowerCase - '0' : (lowerCase - 'a') + 10) << 4;
            int i3 = lowerCase2 <= '9' ? i2 + (lowerCase2 - '0') : i2 + (lowerCase2 - 'a') + 10;
            if (i3 > 127) {
                i3 -= 256;
            }
            bArr[i / 2] = (byte) i3;
        }
        return bArr;
    }
}
