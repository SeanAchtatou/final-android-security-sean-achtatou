package com.qq.e.comm;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import com.alimama.mobile.pluginframework.core.PluginFramework;
import com.qq.e.comm.constants.Constants;
import com.qq.e.comm.constants.CustomPkgConstants;
import com.qq.e.comm.managers.plugin.PM;
import com.qq.e.comm.managers.setting.SM;
import com.qq.e.comm.managers.status.APPStatus;
import com.qq.e.comm.managers.status.DeviceStatus;
import com.qq.e.comm.managers.status.SDKStatus;
import com.qq.e.comm.util.FileUtil;
import com.qq.e.comm.util.GDTLogger;
import com.qq.e.comm.util.StringUtil;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private String f666a;
    private com.qq.e.comm.managers.setting.a b;

    public a(String str, com.qq.e.comm.managers.setting.a aVar) {
        this.f666a = str;
        this.b = aVar;
    }

    public static JSONObject a(PM pm) throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.putOpt(PluginFramework.KEY_UPDATE_SDKV, SDKStatus.getSDKVersion());
        jSONObject.putOpt("pv", Integer.valueOf(pm.getPluginVersion()));
        return jSONObject;
    }

    public static JSONObject a(SM sm) throws JSONException {
        JSONObject jSONObject = new JSONObject();
        if (sm != null) {
            jSONObject.putOpt("suid", sm.getSuid());
            jSONObject.putOpt("sid", sm.getSid());
        }
        return jSONObject;
    }

    public static JSONObject a(APPStatus aPPStatus) throws JSONException {
        JSONObject jSONObject = new JSONObject();
        if (aPPStatus != null) {
            jSONObject.putOpt("an", aPPStatus.getAPPName());
            jSONObject.putOpt(LogBuilder.KEY_APPKEY, aPPStatus.getAPPID());
            jSONObject.putOpt("appv", aPPStatus.getAPPVersion());
        }
        return jSONObject;
    }

    public static JSONObject a(DeviceStatus deviceStatus) throws JSONException {
        JSONObject jSONObject = new JSONObject();
        if (deviceStatus != null) {
            jSONObject.putOpt("so", deviceStatus.getScreenOrientation());
            jSONObject.putOpt("dn", deviceStatus.getDataNet());
            jSONObject.putOpt("lat", deviceStatus.getLat());
            jSONObject.putOpt("lng", deviceStatus.getLng());
            for (Map.Entry next : deviceStatus.getLacAndCeilId().entrySet()) {
                jSONObject.putOpt((String) next.getKey(), next.getValue());
            }
        }
        return jSONObject;
    }

    public static boolean a(Context context) {
        try {
            if (b(context)) {
                if (a(context, Class.forName(CustomPkgConstants.getADActivityName()))) {
                    if (b(context, Class.forName(CustomPkgConstants.getDownLoadServiceName()))) {
                        return true;
                    }
                }
            }
            return false;
        } catch (Throwable th) {
            GDTLogger.e("Exception While check SDK Env", th);
            return false;
        }
    }

    public static boolean a(Context context, File file, File file2) {
        int i;
        AssetManager assets = context.getAssets();
        try {
            if (Arrays.binarySearch(assets.list(CustomPkgConstants.getAssetPluginDir()), CustomPkgConstants.getAssetPluginName()) < 0) {
                return false;
            }
            String str = CustomPkgConstants.getAssetPluginDir() + File.separator + CustomPkgConstants.getAssetPluginName();
            StringUtil.writeTo("529#####" + Constants.PLUGIN.ASSET_PLUGIN_SIG, file2);
            if (StringUtil.isEmpty(CustomPkgConstants.getAssetPluginXorKey())) {
                return FileUtil.copyTo(assets.open(str), file);
            }
            InputStream open = assets.open(str);
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            byte[] bytes = CustomPkgConstants.getAssetPluginXorKey().getBytes();
            byte[] bArr = new byte[1024];
            int length = bytes.length;
            int i2 = 0;
            int i3 = 0;
            while (true) {
                int read = open.read(bArr);
                if (read > 0) {
                    int i4 = 0;
                    while (i4 < read) {
                        int i5 = i2 + 1;
                        if (i2 >= 64) {
                            i = i3 + 1;
                            bArr[i4] = (byte) (bytes[i3 % length] ^ bArr[i4]);
                        } else {
                            i = i3;
                        }
                        i4++;
                        i3 = i;
                        i2 = i5;
                    }
                    fileOutputStream.write(bArr, 0, read);
                } else {
                    open.close();
                    fileOutputStream.close();
                    return true;
                }
            }
        } catch (Throwable th) {
            GDTLogger.report("Exception while init default plugin manager", th);
            return false;
        }
    }

    private static boolean a(Context context, Class<?>... clsArr) {
        int i = 0;
        while (i <= 0) {
            try {
                Intent intent = new Intent();
                intent.setClass(context, clsArr[0]);
                if (context.getPackageManager().resolveActivity(intent, 65536) == null) {
                    GDTLogger.e(String.format("Activity[%s] is required in AndroidManifest.xml", clsArr[0].getName()));
                    return false;
                }
                i++;
            } catch (Throwable th) {
                GDTLogger.e("Exception while checking required activities", th);
                return false;
            }
        }
        return true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x0040 A[SYNTHETIC, Splitter:B:27:0x0040] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static byte[] a(byte[] r4) {
        /*
            r0 = 0
            if (r4 == 0) goto L_0x0006
            int r1 = r4.length
            if (r1 != 0) goto L_0x0008
        L_0x0006:
            r0 = r4
        L_0x0007:
            return r0
        L_0x0008:
            java.io.ByteArrayOutputStream r3 = new java.io.ByteArrayOutputStream
            r3.<init>()
            java.util.zip.GZIPOutputStream r2 = new java.util.zip.GZIPOutputStream     // Catch:{ Exception -> 0x0028, all -> 0x003b }
            r2.<init>(r3)     // Catch:{ Exception -> 0x0028, all -> 0x003b }
            r2.write(r4)     // Catch:{ Exception -> 0x004e }
            r2.finish()     // Catch:{ Exception -> 0x004e }
            byte[] r0 = r3.toByteArray()     // Catch:{ Exception -> 0x004e }
            r2.close()     // Catch:{ Exception -> 0x0023 }
            r3.close()     // Catch:{ Exception -> 0x0023 }
            goto L_0x0007
        L_0x0023:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0007
        L_0x0028:
            r1 = move-exception
            r2 = r0
        L_0x002a:
            r1.printStackTrace()     // Catch:{ all -> 0x004c }
            if (r2 == 0) goto L_0x0032
            r2.close()     // Catch:{ Exception -> 0x0036 }
        L_0x0032:
            r3.close()     // Catch:{ Exception -> 0x0036 }
            goto L_0x0007
        L_0x0036:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0007
        L_0x003b:
            r1 = move-exception
            r2 = r0
            r0 = r1
        L_0x003e:
            if (r2 == 0) goto L_0x0043
            r2.close()     // Catch:{ Exception -> 0x0047 }
        L_0x0043:
            r3.close()     // Catch:{ Exception -> 0x0047 }
        L_0x0046:
            throw r0
        L_0x0047:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0046
        L_0x004c:
            r0 = move-exception
            goto L_0x003e
        L_0x004e:
            r1 = move-exception
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qq.e.comm.a.a(byte[]):byte[]");
    }

    private static boolean b(Context context) {
        String[] strArr = {"android.permission.INTERNET", "android.permission.ACCESS_NETWORK_STATE", "android.permission.ACCESS_WIFI_STATE", "android.permission.READ_PHONE_STATE", "android.permission.WRITE_EXTERNAL_STORAGE"};
        int i = 0;
        while (i < 5) {
            try {
                String str = strArr[i];
                if (context.checkCallingOrSelfPermission(str) == -1) {
                    GDTLogger.e(String.format("Permission %s is required in AndroidManifest.xml", str));
                    return false;
                }
                i++;
            } catch (Throwable th) {
                GDTLogger.e("Check required Permissions error", th);
                return false;
            }
        }
        return true;
    }

    private static boolean b(Context context, Class<?>... clsArr) {
        int i = 0;
        while (i <= 0) {
            try {
                Class<?> cls = clsArr[0];
                Intent intent = new Intent();
                intent.setClass(context, cls);
                if (context.getPackageManager().resolveService(intent, 65536) == null) {
                    GDTLogger.e(String.format("Service[%s] is required in AndroidManifest.xml", cls.getName()));
                    return false;
                }
                i++;
            } catch (Throwable th) {
                GDTLogger.e("Exception while checking required services", th);
                return false;
            }
        }
        return true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x0057 A[SYNTHETIC, Splitter:B:30:0x0057] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static byte[] b(byte[] r7) {
        /*
            r0 = 0
            if (r7 == 0) goto L_0x0006
            int r1 = r7.length
            if (r1 != 0) goto L_0x0008
        L_0x0006:
            r0 = r7
        L_0x0007:
            return r0
        L_0x0008:
            java.io.ByteArrayInputStream r3 = new java.io.ByteArrayInputStream
            r3.<init>(r7)
            java.io.ByteArrayOutputStream r4 = new java.io.ByteArrayOutputStream
            r4.<init>()
            r1 = 1024(0x400, float:1.435E-42)
            byte[] r1 = new byte[r1]
            java.util.zip.GZIPInputStream r2 = new java.util.zip.GZIPInputStream     // Catch:{ Exception -> 0x0068, all -> 0x0052 }
            r2.<init>(r3)     // Catch:{ Exception -> 0x0068, all -> 0x0052 }
        L_0x001b:
            int r5 = r2.read(r1)     // Catch:{ Exception -> 0x0027 }
            r6 = -1
            if (r5 == r6) goto L_0x003c
            r6 = 0
            r4.write(r1, r6, r5)     // Catch:{ Exception -> 0x0027 }
            goto L_0x001b
        L_0x0027:
            r1 = move-exception
        L_0x0028:
            r1.printStackTrace()     // Catch:{ all -> 0x0066 }
            if (r2 == 0) goto L_0x0030
            r2.close()     // Catch:{ Exception -> 0x0037 }
        L_0x0030:
            r3.close()     // Catch:{ Exception -> 0x0037 }
            r4.close()     // Catch:{ Exception -> 0x0037 }
            goto L_0x0007
        L_0x0037:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0007
        L_0x003c:
            r4.flush()     // Catch:{ Exception -> 0x0027 }
            byte[] r0 = r4.toByteArray()     // Catch:{ Exception -> 0x0027 }
            r2.close()     // Catch:{ Exception -> 0x004d }
            r3.close()     // Catch:{ Exception -> 0x004d }
            r4.close()     // Catch:{ Exception -> 0x004d }
            goto L_0x0007
        L_0x004d:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0007
        L_0x0052:
            r1 = move-exception
            r2 = r0
            r0 = r1
        L_0x0055:
            if (r2 == 0) goto L_0x005a
            r2.close()     // Catch:{ Exception -> 0x0061 }
        L_0x005a:
            r3.close()     // Catch:{ Exception -> 0x0061 }
            r4.close()     // Catch:{ Exception -> 0x0061 }
        L_0x0060:
            throw r0
        L_0x0061:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0060
        L_0x0066:
            r0 = move-exception
            goto L_0x0055
        L_0x0068:
            r1 = move-exception
            r2 = r0
            goto L_0x0028
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qq.e.comm.a.b(byte[]):byte[]");
    }

    public String a() {
        return this.f666a;
    }

    public com.qq.e.comm.managers.setting.a b() {
        return this.b;
    }
}
