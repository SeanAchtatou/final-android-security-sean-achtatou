package com.qq.e.comm.managers.status;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
import android.util.DisplayMetrics;
import com.baidu.mobads.interfaces.IXAdRequestInfo;
import com.qq.e.comm.util.Md5Util;
import com.qq.e.comm.util.StringUtil;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class DeviceStatus {

    /* renamed from: a  reason: collision with root package name */
    private String f680a;
    private String b;
    private int c;
    private int d;
    private int e;
    private String f;
    private String g;
    private String h;
    private String i;
    private String j;
    /* access modifiers changed from: private */
    public volatile String k;
    /* access modifiers changed from: private */
    public volatile String l;
    private volatile float m;
    public final String model = Build.MODEL;
    private Context n;

    public DeviceStatus(Context context) {
        this.n = context;
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        this.e = getVersion() > 3 ? displayMetrics.densityDpi : 120;
        this.c = getVersion() > 3 ? a(displayMetrics.density, displayMetrics.widthPixels) : displayMetrics.widthPixels;
        this.d = getVersion() > 3 ? a(displayMetrics.density, displayMetrics.heightPixels) : displayMetrics.heightPixels;
        a();
    }

    private int a(float f2, int i2) {
        return (this.n.getApplicationInfo().flags & 8192) != 0 ? (int) (((float) i2) / f2) : i2;
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a() {
        /*
            r6 = this;
            android.content.Context r0 = r6.n     // Catch:{ Throwable -> 0x0070 }
            java.lang.String r1 = "location"
            java.lang.Object r0 = r0.getSystemService(r1)     // Catch:{ Throwable -> 0x0070 }
            android.location.LocationManager r0 = (android.location.LocationManager) r0     // Catch:{ Throwable -> 0x0070 }
            if (r0 != 0) goto L_0x000d
        L_0x000c:
            return
        L_0x000d:
            android.location.Criteria r1 = new android.location.Criteria     // Catch:{ Throwable -> 0x0070 }
            r1.<init>()     // Catch:{ Throwable -> 0x0070 }
            r2 = 2
            r1.setAccuracy(r2)     // Catch:{ Throwable -> 0x0070 }
            r2 = 0
            r1.setAltitudeRequired(r2)     // Catch:{ Throwable -> 0x0070 }
            r2 = 0
            r1.setBearingRequired(r2)     // Catch:{ Throwable -> 0x0070 }
            r2 = 1
            r1.setCostAllowed(r2)     // Catch:{ Throwable -> 0x0070 }
            r2 = 1
            r1.setPowerRequirement(r2)     // Catch:{ Throwable -> 0x0070 }
            r2 = 1
            java.lang.String r1 = r0.getBestProvider(r1, r2)     // Catch:{ Throwable -> 0x005e }
            android.location.Location r2 = r0.getLastKnownLocation(r1)     // Catch:{ Throwable -> 0x005e }
            if (r2 == 0) goto L_0x0060
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x005e }
            r0.<init>()     // Catch:{ Throwable -> 0x005e }
            double r4 = r2.getLatitude()     // Catch:{ Throwable -> 0x005e }
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ Throwable -> 0x005e }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x005e }
            r6.k = r0     // Catch:{ Throwable -> 0x005e }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x005e }
            r0.<init>()     // Catch:{ Throwable -> 0x005e }
            double r4 = r2.getLongitude()     // Catch:{ Throwable -> 0x005e }
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ Throwable -> 0x005e }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x005e }
            r6.l = r0     // Catch:{ Throwable -> 0x005e }
            float r0 = r2.getAccuracy()     // Catch:{ Throwable -> 0x005e }
            r6.m = r0     // Catch:{ Throwable -> 0x005e }
            goto L_0x000c
        L_0x005e:
            r0 = move-exception
            goto L_0x000c
        L_0x0060:
            com.qq.e.comm.managers.status.DeviceStatus$1 r5 = new com.qq.e.comm.managers.status.DeviceStatus$1     // Catch:{ Throwable -> 0x005e }
            r5.<init>(r0)     // Catch:{ Throwable -> 0x005e }
            r2 = 2000(0x7d0, double:9.88E-321)
            r4 = 1171963904(0x45dac000, float:7000.0)
            r0.requestLocationUpdates(r1, r2, r4, r5)     // Catch:{ Throwable -> 0x006e }
            goto L_0x000c
        L_0x006e:
            r0 = move-exception
            goto L_0x000c
        L_0x0070:
            r0 = move-exception
            goto L_0x000c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qq.e.comm.managers.status.DeviceStatus.a():void");
    }

    public Carrier getCarrier() {
        String operator = getOperator();
        if (operator != null) {
            if (operator.equals("46000") || operator.equals("46002") || operator.equals("46007") || operator.equals("46020")) {
                return Carrier.CMCC;
            }
            if (operator.equals("46001") || operator.equals("46006")) {
                return Carrier.UNICOM;
            }
            if (operator.equals("46003") || operator.equals("46005")) {
                return Carrier.TELECOM;
            }
        }
        return Carrier.UNKNOWN;
    }

    public String getDataNet() {
        NetworkInfo networkInfo;
        String str;
        try {
            networkInfo = ((ConnectivityManager) this.n.getSystemService("connectivity")).getActiveNetworkInfo();
        } catch (Exception e2) {
            networkInfo = null;
        }
        if (networkInfo == null) {
            return null;
        }
        switch (networkInfo.getType()) {
            case 0:
                str = "ed";
                break;
            case 1:
                str = IXAdRequestInfo.WIFI;
                break;
            default:
                str = "unknow";
                break;
        }
        this.i = str;
        return this.i;
    }

    public int getDeviceDensity() {
        return this.e;
    }

    public int getDeviceHeight() {
        return this.d;
    }

    public int getDeviceWidth() {
        return this.c;
    }

    public String getDid() {
        if (this.j == null) {
            try {
                TelephonyManager telephonyManager = (TelephonyManager) this.n.getSystemService("phone");
                if (StringUtil.isEmpty(telephonyManager.getDeviceId())) {
                    this.j = "";
                } else {
                    this.j = Md5Util.encode(telephonyManager.getDeviceId().toLowerCase(Locale.US)).toLowerCase(Locale.US);
                }
            } catch (Exception e2) {
            }
        }
        return this.j;
    }

    public Map<String, String> getLacAndCeilId() {
        int i2;
        int i3 = 0;
        String operator = getOperator();
        HashMap hashMap = new HashMap();
        if (!StringUtil.isEmpty(operator) && !"null".equalsIgnoreCase(operator)) {
            try {
                int parseInt = Integer.parseInt(operator.substring(0, 3));
                int parseInt2 = Integer.parseInt(operator.substring(3));
                if (parseInt == 460) {
                    TelephonyManager telephonyManager = (TelephonyManager) this.n.getSystemService("phone");
                    if (parseInt2 == 3 || parseInt2 == 5) {
                        CdmaCellLocation cdmaCellLocation = (CdmaCellLocation) telephonyManager.getCellLocation();
                        i3 = cdmaCellLocation.getNetworkId();
                        i2 = cdmaCellLocation.getBaseStationId();
                    } else {
                        GsmCellLocation gsmCellLocation = (GsmCellLocation) telephonyManager.getCellLocation();
                        if (gsmCellLocation != null) {
                            i3 = gsmCellLocation.getLac();
                            i2 = gsmCellLocation.getCid();
                        } else {
                            i2 = 0;
                        }
                    }
                    hashMap.put("lac", new StringBuilder().append(i3).toString());
                    hashMap.put("cellid", new StringBuilder().append(i2).toString());
                }
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
        return hashMap;
    }

    public String getLanguage() {
        if (this.b == null) {
            this.b = Locale.getDefault().getLanguage().toLowerCase(Locale.US);
            if (this.b.length() == 0) {
                this.b = "en";
            }
        }
        return this.b;
    }

    public String getLat() {
        return this.k;
    }

    public String getLng() {
        return this.l;
    }

    public float getLocationAccuracy() {
        return this.m;
    }

    public NetworkType getNetworkType() {
        int i2;
        String dataNet = getDataNet();
        try {
            i2 = Integer.parseInt(getPhoneNet());
        } catch (NumberFormatException e2) {
            i2 = 0;
        }
        if (dataNet != null && dataNet.equals(IXAdRequestInfo.WIFI)) {
            return NetworkType.WIFI;
        }
        switch (i2) {
            case 1:
            case 2:
                return NetworkType.NET_2G;
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 14:
                return NetworkType.NET_3G;
            case 13:
            case 15:
                return NetworkType.NET_4G;
            default:
                return NetworkType.UNKNOWN;
        }
    }

    public String getOperator() {
        try {
            this.g = ((TelephonyManager) this.n.getSystemService("phone")).getNetworkOperator();
        } catch (Exception e2) {
        }
        return this.g;
    }

    public String getPhoneNet() {
        try {
            this.h = new StringBuilder().append(((TelephonyManager) this.n.getSystemService("phone")).getNetworkType()).toString();
        } catch (Exception e2) {
        }
        return this.h;
    }

    public String getScreenOrientation() {
        if (this.n.getResources().getConfiguration().orientation == 2) {
            this.f = "l";
        } else if (this.n.getResources().getConfiguration().orientation == 1) {
            this.f = "p";
        }
        return this.f;
    }

    public String getUid() {
        if (this.f680a == null) {
            String string = Settings.Secure.getString(this.n.getContentResolver(), "android_id");
            this.f680a = string == null ? Md5Util.encode("emulator") : Md5Util.encode(string);
        }
        return this.f680a;
    }

    public int getVersion() {
        try {
            return Build.VERSION.SDK_INT;
        } catch (Exception e2) {
            return 3;
        }
    }
}
