package com.qq.e.comm;

import android.app.Service;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.IBinder;
import com.qq.e.comm.managers.GDTADManager;
import com.qq.e.comm.pi.SVSD;
import com.qq.e.comm.util.GDTLogger;
import com.qq.e.comm.util.StringUtil;

public class DownloadService extends Service {

    /* renamed from: a  reason: collision with root package name */
    private SVSD f665a;

    private boolean a(String str) {
        if (this.f665a == null) {
            try {
                if (GDTADManager.getInstance().initWith(getApplicationContext(), str)) {
                    this.f665a = GDTADManager.getInstance().getPM().getPOFactory().getAPKDownloadServiceDelegate(this);
                    this.f665a.onCreate();
                } else {
                    GDTLogger.report("Init GDTADManager fail in DownloadService.oncreate");
                }
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
        return this.f665a != null;
    }

    public IBinder onBind(Intent intent) {
        GDTLogger.d("DownloadService.onBind");
        if (this.f665a != null) {
            return this.f665a.onBind(intent);
        }
        String stringExtra = intent.getStringExtra("GDT_APPID");
        GDTLogger.d("DownloadService.onBind,appID=" + stringExtra);
        if (StringUtil.isEmpty(stringExtra) || !a(stringExtra)) {
            return null;
        }
        return this.f665a.onBind(intent);
    }

    public void onConfigurationChanged(Configuration configuration) {
        if (this.f665a != null) {
            this.f665a.onConfigurationChanged(configuration);
        }
    }

    public void onCreate() {
        super.onCreate();
    }

    public void onDestroy() {
        if (this.f665a != null) {
            this.f665a.onDestroy();
        }
    }

    public void onLowMemory() {
        if (this.f665a != null) {
            this.f665a.onLowMemory();
        }
    }

    public void onRebind(Intent intent) {
        if (this.f665a != null) {
            this.f665a.onRebind(intent);
        }
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        if (intent == null) {
            stopSelf(i2);
            return 2;
        }
        String stringExtra = intent.getStringExtra("GDT_APPID");
        if (!StringUtil.isEmpty(stringExtra) && a(stringExtra)) {
            return this.f665a.onStartCommand(intent, i, i2);
        }
        GDTLogger.w("Failto Start new download Service");
        return 2;
    }

    public void onTaskRemoved(Intent intent) {
        if (this.f665a != null) {
            this.f665a.onTaskRemoved(intent);
        }
    }

    public void onTrimMemory(int i) {
        if (this.f665a != null) {
            this.f665a.onTrimMemory(i);
        }
    }

    public boolean onUnbind(Intent intent) {
        return this.f665a != null ? this.f665a.onUnbind(intent) : super.onUnbind(intent);
    }
}
