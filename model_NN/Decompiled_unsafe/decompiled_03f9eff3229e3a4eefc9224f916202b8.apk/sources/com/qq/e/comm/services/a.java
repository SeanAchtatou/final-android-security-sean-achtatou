package com.qq.e.comm.services;

import android.app.ActivityManager;
import android.content.Context;
import android.os.Build;
import android.os.Process;
import com.baidu.mobads.interfaces.IXAdRequestInfo;
import com.qq.e.comm.constants.Constants;
import com.qq.e.comm.managers.GDTADManager;
import com.qq.e.comm.managers.plugin.PM;
import com.qq.e.comm.managers.setting.SM;
import com.qq.e.comm.managers.status.APPStatus;
import com.qq.e.comm.managers.status.DeviceStatus;
import com.qq.e.comm.net.NetworkCallBack;
import com.qq.e.comm.net.NetworkClient;
import com.qq.e.comm.net.NetworkClientImpl;
import com.qq.e.comm.net.rr.Request;
import com.qq.e.comm.net.rr.Response;
import com.qq.e.comm.net.rr.S2SSRequest;
import com.qq.e.comm.services.RetCodeService;
import com.qq.e.comm.util.GDTLogger;
import com.qq.e.comm.util.StringUtil;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import org.json.JSONException;
import org.json.JSONObject;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private static final a f696a = new a();
    private volatile Boolean b = false;

    public static a a() {
        return f696a;
    }

    private static String a(Context context) {
        int myPid = Process.myPid();
        for (ActivityManager.RunningAppProcessInfo next : ((ActivityManager) context.getSystemService("activity")).getRunningAppProcesses()) {
            if (next.pid == myPid) {
                return next.processName;
            }
        }
        return null;
    }

    private static String a(SM sm, PM pm, DeviceStatus deviceStatus, APPStatus aPPStatus, Context context, long j) {
        JSONException e;
        JSONObject jSONObject;
        JSONObject jSONObject2 = new JSONObject();
        try {
            jSONObject = com.qq.e.comm.a.a(sm);
            try {
                JSONObject jSONObject3 = new JSONObject();
                if (sm != null) {
                    jSONObject3.putOpt("app", sm.getDevCloudSettingSig());
                    jSONObject3.putOpt("sdk", sm.getSdkCloudSettingSig());
                }
                if (pm != null) {
                    jSONObject3.putOpt("jar", pm.getLocalSig());
                    jSONObject3.putOpt(Constants.KEYS.PLUGIN_VERSION, Integer.valueOf(pm.getPluginVersion()));
                }
                jSONObject.put("sig", jSONObject3);
                JSONObject jSONObject4 = new JSONObject();
                if (deviceStatus != null) {
                    jSONObject4.putOpt("did", deviceStatus.getDid());
                    jSONObject4.putOpt(IXAdRequestInfo.TEST_MODE, deviceStatus.model);
                    jSONObject4.putOpt("lg", deviceStatus.getLanguage());
                    jSONObject4.putOpt(IXAdRequestInfo.WIDTH, Integer.valueOf(deviceStatus.getDeviceWidth()));
                    jSONObject4.putOpt(IXAdRequestInfo.HEIGHT, Integer.valueOf(deviceStatus.getDeviceHeight()));
                    jSONObject4.putOpt("dd", Integer.valueOf(deviceStatus.getDeviceDensity()));
                    jSONObject4.putOpt("apil", Integer.valueOf(deviceStatus.getVersion()));
                    jSONObject4.putOpt("os", "android");
                    jSONObject4.putOpt("op", deviceStatus.getOperator());
                    jSONObject4.putOpt("mf", Build.MANUFACTURER);
                }
                jSONObject.put("dev", jSONObject4);
                jSONObject.put("app", com.qq.e.comm.a.a(aPPStatus));
                JSONObject a2 = com.qq.e.comm.a.a(deviceStatus);
                a2.putOpt("process", a(context));
                jSONObject.put("c", a2);
                jSONObject.put("sdk", com.qq.e.comm.a.a(pm));
                JSONObject jSONObject5 = new JSONObject();
                JSONObject jSONObject6 = new JSONObject();
                jSONObject6.put("sdk_init_time", (System.nanoTime() - j) / 1000000);
                jSONObject5.put("performance", jSONObject6);
                jSONObject.put(Constants.KEYS.BIZ, jSONObject5);
            } catch (JSONException e2) {
                e = e2;
                GDTLogger.e("JSONException while build init req", e);
                return jSONObject.toString();
            }
        } catch (JSONException e3) {
            JSONException jSONException = e3;
            jSONObject = jSONObject2;
            e = jSONException;
            GDTLogger.e("JSONException while build init req", e);
            return jSONObject.toString();
        }
        return jSONObject.toString();
    }

    public static void a(String str, Throwable th) {
        if (GDTADManager.getInstance() == null || !GDTADManager.getInstance().isInitialized()) {
            GDTLogger.w("Report Not Work while  ADManager  not Inited");
            return;
        }
        try {
            JSONObject a2 = com.qq.e.comm.a.a(GDTADManager.getInstance().getSM());
            a2.put("c", com.qq.e.comm.a.a(GDTADManager.getInstance().getDeviceStatus()));
            a2.put("app", com.qq.e.comm.a.a(GDTADManager.getInstance().getAppStatus()));
            HashMap hashMap = new HashMap();
            if (th != null) {
                hashMap.put("extype", th.getClass().getName());
                hashMap.put("ext", str + "\r" + th.getMessage() + "\r" + Arrays.toString(th.getStackTrace()));
            } else {
                hashMap.put("extype", "");
                hashMap.put("ex", str);
            }
            a2.put(Constants.KEYS.BIZ, new JSONObject(hashMap));
            NetworkClientImpl.getInstance().submit(new S2SSRequest("http://sdk.e.qq.com/err", a2.toString().getBytes()));
        } catch (Throwable th2) {
            GDTLogger.w("Exception While build s2ss error report req", th2);
        }
    }

    public final void a(Context context, SM sm, PM pm, DeviceStatus deviceStatus, APPStatus aPPStatus, long j) {
        if (!this.b.booleanValue()) {
            synchronized (this.b) {
                if (!this.b.booleanValue()) {
                    String a2 = a(sm, pm, deviceStatus, aPPStatus, context, j);
                    String str = !StringUtil.isEmpty(sm.getSuid()) ? "http://sdk.e.qq.com/launch" : "http://sdk.e.qq.com/activate";
                    final long currentTimeMillis = System.currentTimeMillis();
                    final SM sm2 = sm;
                    final PM pm2 = pm;
                    NetworkClientImpl.getInstance().submit(new S2SSRequest(str, a2.getBytes()), NetworkClient.Priority.High, new NetworkCallBack(this) {
                        public final void onException(Exception exc) {
                            GDTLogger.e("ActivateError", exc);
                            RetCodeService.getInstance().send(new RetCodeService.RetCodeInfo("sdk.e.qq.com", "launch", "", -1, (int) (System.currentTimeMillis() - currentTimeMillis), 0, 0, 1));
                        }

                        public final void onResponse(Request request, Response response) {
                            try {
                                if (response.getStatusCode() == 200) {
                                    String stringContent = response.getStringContent();
                                    GDTLogger.d("ACTIVERESPONSE:" + stringContent);
                                    if (StringUtil.isEmpty(stringContent)) {
                                        GDTLogger.report("SDK Server response empty string,maybe zip or tea format error");
                                        RetCodeService.getInstance().send(new RetCodeService.RetCodeInfo("sdk.e.qq.com", "launch", "", response.getStatusCode(), (int) (System.currentTimeMillis() - currentTimeMillis), 0, 0, 1));
                                        return;
                                    }
                                    JSONObject jSONObject = new JSONObject(stringContent);
                                    int i = -1;
                                    if (jSONObject.has(Constants.KEYS.RET)) {
                                        i = jSONObject.getInt(Constants.KEYS.RET);
                                    }
                                    if (i != 0) {
                                        GDTLogger.e("Response Error,retCode=" + i);
                                    } else {
                                        if (jSONObject.has("suid")) {
                                            String string = jSONObject.getString("suid");
                                            if (!StringUtil.isEmpty(string)) {
                                                sm2.updateSUID(string);
                                            }
                                        }
                                        if (jSONObject.has("sid")) {
                                            String string2 = jSONObject.getString("sid");
                                            if (!StringUtil.isEmpty(string2)) {
                                                sm2.updateSID(string2);
                                            }
                                        }
                                        if (jSONObject.has("sig")) {
                                            JSONObject jSONObject2 = jSONObject.getJSONObject("sig");
                                            if (jSONObject.has("setting")) {
                                                JSONObject jSONObject3 = jSONObject.getJSONObject("setting");
                                                if (jSONObject3.has("app") && jSONObject2.has("app")) {
                                                    String string3 = jSONObject3.getString("app");
                                                    sm2.updateDEVCloudSetting(jSONObject2.getString("app"), string3);
                                                }
                                                if (jSONObject3.has("sdk") && jSONObject2.has("sdk")) {
                                                    String string4 = jSONObject3.getString("sdk");
                                                    sm2.updateSDKCloudSetting(jSONObject2.getString("sdk"), string4);
                                                }
                                                if (jSONObject3.has("c")) {
                                                    sm2.updateContextSetting(jSONObject3.getString("c"));
                                                } else {
                                                    sm2.updateContextSetting(null);
                                                }
                                            }
                                            if (jSONObject2.has("jar") && jSONObject2.has("url")) {
                                                pm2.update(jSONObject2.getString("jar"), jSONObject2.getString("url"));
                                            }
                                        }
                                    }
                                } else {
                                    GDTLogger.e("SDK server response code error while launch or activate,code:" + response.getStatusCode());
                                }
                                RetCodeService.getInstance().send(new RetCodeService.RetCodeInfo("sdk.e.qq.com", "launch", "", response.getStatusCode(), (int) (System.currentTimeMillis() - currentTimeMillis), 0, 0, 1));
                            } catch (IOException e) {
                                GDTLogger.e("ActivateError", e);
                                RetCodeService.getInstance().send(new RetCodeService.RetCodeInfo("sdk.e.qq.com", "launch", "", response.getStatusCode(), (int) (System.currentTimeMillis() - currentTimeMillis), 0, 0, 1));
                            } catch (JSONException e2) {
                                GDTLogger.e("Parse Active or launch response exception", e2);
                                RetCodeService.getInstance().send(new RetCodeService.RetCodeInfo("sdk.e.qq.com", "launch", "", response.getStatusCode(), (int) (System.currentTimeMillis() - currentTimeMillis), 0, 0, 1));
                            } catch (Throwable th) {
                                Throwable th2 = th;
                                RetCodeService.getInstance().send(new RetCodeService.RetCodeInfo("sdk.e.qq.com", "launch", "", response.getStatusCode(), (int) (System.currentTimeMillis() - currentTimeMillis), 0, 0, 1));
                                throw th2;
                            }
                        }
                    });
                    this.b = true;
                }
            }
        }
    }
}
