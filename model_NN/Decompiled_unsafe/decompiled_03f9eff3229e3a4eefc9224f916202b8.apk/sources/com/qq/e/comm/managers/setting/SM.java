package com.qq.e.comm.managers.setting;

import android.content.Context;
import android.util.Base64;
import com.qq.e.comm.a;
import com.qq.e.comm.constants.Constants;
import com.qq.e.comm.util.GDTLogger;
import com.qq.e.comm.util.StringUtil;
import java.io.File;
import java.io.IOException;

public class SM {

    /* renamed from: a  reason: collision with root package name */
    private a f674a;
    private String b;
    private a c;
    private a d;
    private String e;
    private c f;
    private c g;
    private String h;
    private String i = "";
    private Context j;

    public SM(Context context) {
        this.j = context;
        this.f674a = new a();
        this.d = new a();
        this.g = new b();
        try {
            this.h = StringUtil.readAll(new File(this.j.getDir(Constants.SETTING.SETTINGDIR, 0), Constants.SETTING.SUID_FILE));
        } catch (Throwable th) {
            this.h = null;
            GDTLogger.e("IO Exception while loading suid");
        }
        a();
        b();
    }

    private void a() {
        d b2 = a.b(this.j);
        if (b2 != null) {
            this.e = b2.a();
            this.f = b2.b();
            return;
        }
        GDTLogger.d("Load Local SDK Cloud setting fail");
    }

    private void b() {
        a a2 = a.a(this.j);
        if (a2 != null) {
            this.c = a2.b();
            this.b = a2.a();
            return;
        }
        GDTLogger.d("Load Local DEV Cloud setting fail");
    }

    public Object get(String str) {
        Object a2;
        Object a3;
        Object a4;
        Object a5;
        if (StringUtil.isEmpty(str)) {
            return null;
        }
        try {
            if (this.f674a.a(str) != null && (a5 = this.f674a.a(str)) != null) {
                return a5;
            }
            if (this.c != null && (a4 = this.c.a(str)) != null) {
                return a4;
            }
            if (this.d != null && (a3 = this.d.a(str)) != null) {
                return a3;
            }
            if (this.f != null && (a2 = this.f.a(str)) != null) {
                return a2;
            }
            if (this.g != null) {
                return this.g.a(str);
            }
            return null;
        } catch (Throwable th) {
            GDTLogger.report("Exception in settingManager.get Setting for key:" + str, th);
            return null;
        }
    }

    public String getDevCloudSettingSig() {
        return this.b;
    }

    public Object getForPlacement(String str, String str2) {
        Object a2;
        Object a3;
        Object a4;
        if (StringUtil.isEmpty(str) || StringUtil.isEmpty(str2)) {
            return null;
        }
        try {
            return (this.f674a == null || (a4 = this.f674a.a(str, str2)) == null) ? (this.c == null || (a3 = this.c.a(str, str2)) == null) ? (this.d == null || (a2 = this.d.a(str, str2)) == null) ? get(str) : a2 : a3 : a4;
        } catch (Throwable th) {
            GDTLogger.report("Exception in settingManager.getForPlacement", th);
            return null;
        }
    }

    public int getInteger(String str, int i2) {
        Object obj = get(str);
        return (obj == null || !(obj instanceof Integer)) ? i2 : ((Integer) obj).intValue();
    }

    public int getIntegerForPlacement(String str, String str2, int i2) {
        Object forPlacement = getForPlacement(str, str2);
        return (forPlacement == null || !(forPlacement instanceof Integer)) ? i2 : ((Integer) forPlacement).intValue();
    }

    public String getSdkCloudSettingSig() {
        return this.e;
    }

    public String getSid() {
        return this.i;
    }

    public String getString(String str) {
        Object obj = get(str);
        if (obj == null) {
            return null;
        }
        return obj.toString();
    }

    public String getStringForPlacement(String str, String str2) {
        Object forPlacement = getForPlacement(str, str2);
        if (forPlacement == null) {
            return null;
        }
        return forPlacement.toString();
    }

    public String getSuid() {
        return this.h;
    }

    public void setDEVCodeSetting(String str, Object obj) {
        this.d.a(str, obj);
    }

    public void setDEVCodeSetting(String str, Object obj, String str2) {
        this.d.a(str, obj, str2);
    }

    public void updateContextSetting(String str) {
        try {
            a aVar = new a();
            if (!StringUtil.isEmpty(str)) {
                aVar = new a(new String(Base64.decode(str, 0), "UTF-8"));
            }
            this.f674a = aVar;
        } catch (Throwable th) {
            GDTLogger.report("Exception while update Context Setting", th);
        }
    }

    public void updateDEVCloudSetting(String str, String str2) {
        if (a.b(this.j, str, str2)) {
            b();
        }
    }

    public void updateSDKCloudSetting(String str, String str2) {
        if (a.a(this.j, str, str2)) {
            a();
        }
    }

    public void updateSID(String str) {
        this.i = str;
    }

    public void updateSUID(String str) {
        if (!StringUtil.isEmpty(str) && !str.equals(this.h)) {
            this.h = str;
            try {
                StringUtil.writeTo(str, new File(this.j.getDir(Constants.SETTING.SETTINGDIR, 0), Constants.SETTING.SUID_FILE));
            } catch (IOException e2) {
                GDTLogger.report("Exception while persit suid", e2);
            }
        }
    }
}
