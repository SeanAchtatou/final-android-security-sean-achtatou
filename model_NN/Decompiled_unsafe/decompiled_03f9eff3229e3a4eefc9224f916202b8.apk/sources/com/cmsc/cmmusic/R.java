package com.cmsc.cmmusic;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static int ic_launcher = com.shoujiduoduo.ringtone.R.drawable.ac_btn_ad_left_bkg;
    }

    public static final class string {
        public static int app_name = com.shoujiduoduo.ringtone.R.layout.about_info_layout;
        public static int hello_world = com.shoujiduoduo.ringtone.R.layout.ac_view_layout;
        public static int menu_settings = com.shoujiduoduo.ringtone.R.layout.activity_about;
    }

    public static final class style {
        public static int AppBaseTheme = com.shoujiduoduo.ringtone.R.anim.alpha_in;
        public static int AppTheme = com.shoujiduoduo.ringtone.R.anim.alpha_out;
    }
}
