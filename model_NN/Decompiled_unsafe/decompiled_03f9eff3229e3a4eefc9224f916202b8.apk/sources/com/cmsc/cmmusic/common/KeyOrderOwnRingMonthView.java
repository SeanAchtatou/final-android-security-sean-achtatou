package com.cmsc.cmmusic.common;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.cmsc.cmmusic.common.data.CrbtOpenCheckRsp;
import com.cmsc.cmmusic.common.data.OrderPolicy;
import com.cmsc.cmmusic.common.data.OrderResult;
import java.io.IOException;
import java.io.InputStream;
import org.xmlpull.v1.XmlPullParserException;

final class KeyOrderOwnRingMonthView extends RelativeLayout implements RadioGroup.OnCheckedChangeListener {
    public static final int FIVE_YUAN_MONTHLY = 21;
    public static final int FIVE_YUAN_MONTHLY_ORMMR = 23;
    public static final int KEY_ORDER_OWN_RING_MONTH_MODE = 1;
    public static final int OPEN_RINGTONE_MODE = 3;
    public static final int OWN_RING_MONTH_MODE = 2;
    public static final int THREE_YUAN_MONTHLY = 20;
    public static final int THREE_YUAN_MONTHLY_ORMMR = 22;
    public static CMMusicActivity mCurActivity;
    protected LinearLayout andvancedSetContentsLL = null;
    private TextView bottomTxtTip;
    protected Button btnCancel = null;
    protected Button btnSure = null;
    protected LinearLayout btnView = null;
    /* access modifiers changed from: private */
    public CrbtOpenCheckRsp crbtOpenCheckRsp;
    protected Bundle curExtraInfo = null;
    private String isOwnRingOrderMonthUser = "非包月用户";
    protected Handler mHandler = null;
    private int mode;
    /* access modifiers changed from: private */
    public String monthType = "3";
    private String phoneNum = "";
    OrderPolicy policy;
    private RadioGroup rdGroup;
    protected LinearLayout rootView = null;
    private TextView txtUserTip;

    public KeyOrderOwnRingMonthView(Context context, Bundle bundle, int i, OrderPolicy orderPolicy) {
        super(context);
        this.policy = orderPolicy;
        mCurActivity = (CMMusicActivity) context;
        this.mHandler = new Handler(mCurActivity.getMainLooper());
        this.curExtraInfo = bundle;
        setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(1);
        linearLayout.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        LinearLayout initLogoView = initLogoView();
        this.rootView = new LinearLayout(context);
        this.rootView.setOrientation(1);
        this.rootView.setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
        this.rootView.setPadding(20, 10, 10, 100);
        linearLayout.addView(initLogoView);
        linearLayout.addView(this.rootView);
        ScrollView scrollView = new ScrollView(context);
        scrollView.setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
        scrollView.addView(linearLayout);
        addView(scrollView);
        this.phoneNum = bundle.getString("phoneNum");
        initBtnView(context);
        setVisibility(0);
        initViews(i);
    }

    /* access modifiers changed from: private */
    public void initViews(int i) {
        this.mode = i;
        this.phoneNum = this.curExtraInfo.getString("phoneNum");
        this.isOwnRingOrderMonthUser = this.curExtraInfo.getString("isOwnRingOrderMonthUser");
        Log.i("KeyOrderOwnRingMonthView", "当前手机号码phoneNum = " + this.phoneNum + ", 是否开通个性化彩铃包月isOwnRingOrderMonthUser = " + this.isOwnRingOrderMonthUser);
        switch (i) {
            case 1:
                this.rootView.removeAllViews();
                this.txtUserTip = new TextView(mCurActivity);
                this.txtUserTip.setTextAppearance(mCurActivity, 16973892);
                this.txtUserTip.setText("尊敬的手机用户" + this.phoneNum + ",点击“确认”，将开通个性化彩铃包月功能。\n\n当前包月级别：" + this.isOwnRingOrderMonthUser);
                Log.i("KeyOrderOwnRingMonthView11111", "当前手机号码phoneNum = " + this.phoneNum + ", 是否开通个性化彩铃包月isOwnRingOrderMonthUser = " + this.isOwnRingOrderMonthUser);
                this.rootView.addView(this.txtUserTip);
                this.rdGroup = new RadioGroup(mCurActivity);
                RadioButton radioButton = new RadioButton(mCurActivity);
                radioButton.setText("3元包月");
                radioButton.setId(20);
                radioButton.setChecked(true);
                RadioButton radioButton2 = new RadioButton(mCurActivity);
                radioButton2.setText("5元包月");
                radioButton2.setId(21);
                this.rdGroup.addView(radioButton);
                this.rdGroup.addView(radioButton2);
                this.rootView.addView(this.rdGroup);
                this.rdGroup.setOnCheckedChangeListener(this);
                this.bottomTxtTip = new TextView(mCurActivity);
                this.bottomTxtTip.setTextAppearance(mCurActivity, 16973892);
                this.bottomTxtTip.setText("\n\n订购3元包月包每月可免费订购30首个性彩铃；5元包月包每月可免费不限次订购个性彩铃。");
                this.rootView.addView(this.bottomTxtTip);
                this.btnSure.setEnabled(true);
                this.btnSure.setText("确定");
                return;
            case 2:
                this.rootView.removeAllViews();
                this.txtUserTip = new TextView(mCurActivity);
                this.txtUserTip.setTextAppearance(mCurActivity, 16973892);
                this.txtUserTip.setText("尊敬的手机用户" + this.phoneNum + ",点击“确认”，将开通个性化彩铃包月功能。\n\n当前包月级别：" + this.isOwnRingOrderMonthUser);
                Log.i("KeyOrderOwnRingMonthView22222", "当前手机号码phoneNum = " + this.phoneNum + ", 是否开通个性化彩铃包月isOwnRingOrderMonthUser = " + this.isOwnRingOrderMonthUser);
                this.rootView.addView(this.txtUserTip);
                this.rdGroup = new RadioGroup(mCurActivity);
                RadioButton radioButton3 = new RadioButton(mCurActivity);
                radioButton3.setText("3元包月");
                radioButton3.setId(22);
                radioButton3.setChecked(true);
                RadioButton radioButton4 = new RadioButton(mCurActivity);
                radioButton4.setText("5元包月");
                radioButton4.setId(23);
                this.rdGroup.addView(radioButton3);
                this.rdGroup.addView(radioButton4);
                this.rootView.addView(this.rdGroup);
                this.rdGroup.setOnCheckedChangeListener(this);
                this.bottomTxtTip = new TextView(mCurActivity);
                this.bottomTxtTip.setTextAppearance(mCurActivity, 16973892);
                this.bottomTxtTip.setText("\n\n订购3元包月包每月可免费订购30首个性彩铃；5元包月包每月可免费不限次订购个性彩铃。");
                this.rootView.addView(this.bottomTxtTip);
                this.btnSure.setEnabled(true);
                this.btnSure.setText("确定");
                return;
            case 3:
                this.rootView.removeAllViews();
                this.txtUserTip = new TextView(mCurActivity);
                this.txtUserTip.setTextAppearance(mCurActivity, 16973892);
                this.txtUserTip.setText("尊敬的手机用户:\n\n\n\n您目前不是中国移动彩铃用户，请再次确认是否定制个性化彩铃包月功能。\n\n");
                this.rootView.addView(this.txtUserTip);
                this.bottomTxtTip = new TextView(mCurActivity);
                this.bottomTxtTip.setTextAppearance(mCurActivity, 16973892);
                this.bottomTxtTip.setText("\n\n非彩铃用户定制个性化彩铃包月将自动开通彩铃功能每月5元。");
                this.rootView.addView(this.bottomTxtTip);
                this.btnSure.setEnabled(true);
                this.btnSure.setText("确定");
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void sureClicked() {
        switch (this.mode) {
            case 1:
                EnablerInterface.KeyOrderOwnRingMonth(mCurActivity, this.monthType, new CMMusicCallback<OrderResult>() {
                    public void operationResult(OrderResult orderResult) {
                        KeyOrderOwnRingMonthView.mCurActivity.closeActivity(orderResult);
                        KeyOrderOwnRingMonthView.mCurActivity.hideProgressBar();
                    }
                });
                return;
            case 2:
                mCurActivity.showProgressBar("请稍候...");
                new Thread(new Runnable() {
                    public void run() {
                        try {
                            KeyOrderOwnRingMonthView.this.crbtOpenCheckRsp = EnablerInterface.crbtOpenCheck(KeyOrderOwnRingMonthView.mCurActivity, null);
                            if (KeyOrderOwnRingMonthView.this.crbtOpenCheckRsp != null && "000000".equals(KeyOrderOwnRingMonthView.this.crbtOpenCheckRsp.getResCode())) {
                                EnablerInterface.orderOwnRingMonth(KeyOrderOwnRingMonthView.mCurActivity, KeyOrderOwnRingMonthView.this.monthType, new CMMusicCallback<OrderResult>() {
                                    public void operationResult(OrderResult orderResult) {
                                        KeyOrderOwnRingMonthView.mCurActivity.closeActivity(orderResult);
                                        KeyOrderOwnRingMonthView.mCurActivity.hideProgressBar();
                                    }
                                });
                            } else if (KeyOrderOwnRingMonthView.this.crbtOpenCheckRsp == null || !"100100".equals(KeyOrderOwnRingMonthView.this.crbtOpenCheckRsp.getResCode())) {
                                KeyOrderOwnRingMonthView.mCurActivity.closeActivity(KeyOrderOwnRingMonthView.this.crbtOpenCheckRsp);
                            } else {
                                KeyOrderOwnRingMonthView.this.mHandler.post(new Runnable() {
                                    public void run() {
                                        KeyOrderOwnRingMonthView.this.initViews(3);
                                    }
                                });
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (XmlPullParserException e2) {
                            e2.printStackTrace();
                        } finally {
                            KeyOrderOwnRingMonthView.mCurActivity.hideProgressBar();
                        }
                    }
                }).start();
                return;
            case 3:
                EnablerInterface.KeyOrderOwnRingMonth(mCurActivity, this.monthType, new CMMusicCallback<OrderResult>() {
                    public void operationResult(OrderResult orderResult) {
                        KeyOrderOwnRingMonthView.mCurActivity.closeActivity(orderResult);
                    }
                });
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void cancelClicked() {
        mCurActivity.closeActivity(null);
    }

    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        switch (i) {
            case 20:
                this.monthType = "3";
                return;
            case 21:
                this.monthType = "5";
                return;
            case 22:
                this.monthType = "3";
                return;
            case 23:
                this.monthType = "5";
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public int getScreenHeightDip() {
        int i = mCurActivity.getResources().getDisplayMetrics().heightPixels;
        Log.d("getScreenHeightDip=", new StringBuilder().append(i).toString());
        return i;
    }

    private LinearLayout initLogoView() {
        LinearLayout linearLayout = new LinearLayout(mCurActivity);
        linearLayout.setId(299);
        linearLayout.setPadding(0, 5, 0, 0);
        linearLayout.setGravity(17);
        linearLayout.setOrientation(1);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams.addRule(10);
        linearLayout.setLayoutParams(layoutParams);
        try {
            InputStream open = mCurActivity.getAssets().open("logo.png");
            BitmapDrawable bitmapDrawable = new BitmapDrawable(BitmapFactory.decodeStream(open));
            open.close();
            ImageView imageView = new ImageView(mCurActivity);
            imageView.setImageDrawable(bitmapDrawable);
            imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
            imageView.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
            linearLayout.addView(imageView);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return linearLayout;
    }

    private void initBtnView(Context context) {
        this.btnView = new LinearLayout(context);
        this.btnView.setOrientation(0);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams.addRule(12);
        this.btnView.setLayoutParams(layoutParams);
        this.btnSure = new Button(context);
        this.btnSure.setText("确认");
        this.btnSure.setLayoutParams(new LinearLayout.LayoutParams(-2, -2, 0.5f));
        this.btnView.addView(this.btnSure);
        this.btnSure.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                KeyOrderOwnRingMonthView.this.sureClicked();
            }
        });
        this.btnCancel = new Button(context);
        this.btnCancel.setText("取消");
        this.btnCancel.setLayoutParams(new LinearLayout.LayoutParams(-2, -2, 0.5f));
        this.btnView.addView(this.btnCancel);
        this.btnCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Log.d(getClass().getSimpleName(), "cancel button clicked");
                KeyOrderOwnRingMonthView.this.cancelClicked();
            }
        });
        addView(this.btnView);
    }
}
