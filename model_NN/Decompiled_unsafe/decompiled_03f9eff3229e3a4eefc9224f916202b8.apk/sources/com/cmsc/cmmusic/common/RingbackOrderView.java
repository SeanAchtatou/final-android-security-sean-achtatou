package com.cmsc.cmmusic.common;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.cmsc.cmmusic.common.data.BizInfo;
import com.cmsc.cmmusic.common.data.GetUserInfoRsp;
import com.cmsc.cmmusic.common.data.MusicInfo;
import com.cmsc.cmmusic.common.data.OrderPolicy;
import com.cmsc.cmmusic.common.data.OrderResult;
import com.cmsc.cmmusic.common.data.UserInfo;
import com.tencent.connect.common.Constants;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

class RingbackOrderView extends OrderView {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$cmsc$cmmusic$common$data$OrderPolicy$OrderType = null;
    private static final String LOG_TAG = "RingbackOrderView";
    private BizInfo bizinfo;
    private TextView txtInvalidDate;
    private TextView txtPrice;

    static /* synthetic */ int[] $SWITCH_TABLE$com$cmsc$cmmusic$common$data$OrderPolicy$OrderType() {
        int[] iArr = $SWITCH_TABLE$com$cmsc$cmmusic$common$data$OrderPolicy$OrderType;
        if (iArr == null) {
            iArr = new int[OrderPolicy.OrderType.values().length];
            try {
                iArr[OrderPolicy.OrderType.net.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[OrderPolicy.OrderType.sms.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[OrderPolicy.OrderType.verifyCode.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$com$cmsc$cmmusic$common$data$OrderPolicy$OrderType = iArr;
        }
        return iArr;
    }

    public RingbackOrderView(Context context, Bundle bundle) {
        super(context, bundle);
    }

    /* access modifiers changed from: protected */
    public void updateNetView() {
        MusicInfo musicInfo = this.policyObj.getMusicInfo();
        this.bizinfo = getBizInfo();
        String str = "点击“确认”将该歌曲设置为您的彩铃";
        if (musicInfo == null || this.bizinfo == null) {
            str = "无法获取彩铃信息！";
        } else {
            this.txtInvalidDate.setText("彩铃有效期：" + getFormatTime(musicInfo));
            if (!Constants.VIA_ACT_TYPE_TWENTY_EIGHT.equals(this.bizinfo.getBizType())) {
                this.txtPrice.setText("彩铃价格: " + EnablerInterface.getPriceString(this.bizinfo.getSalePrice()));
            } else {
                str = "无法订购！请先订购该彩铃所属数字专辑";
            }
            if (!"3".equals(this.policyObj.getMonLevel())) {
                if (this.specMemInfoView == null) {
                    this.contentView.addView(getSpecMemInfoView());
                }
                updateSpecMemInfoView("推荐：开通咪咕特级会员专享彩铃订购7折优惠。");
            } else if (this.specMemInfoView != null) {
                this.specMemInfoView.setVisibility(8);
            }
        }
        setUserTip(str);
        Log.d(LOG_TAG, "get url by net. user : " + this.policyObj.getMobile() + " , price : " + ((Object) this.txtPrice.getText()));
        Log.d(LOG_TAG, "orderType : " + this.orderType + " , songName : " + this.curSongName + " , singerName : " + this.curSingerName);
    }

    /* access modifiers changed from: protected */
    public BizInfo getBizInfo() {
        ArrayList<BizInfo> bizInfos = this.policyObj.getBizInfos();
        if (bizInfos == null) {
            return null;
        }
        Iterator<BizInfo> it = bizInfos.iterator();
        while (it.hasNext()) {
            BizInfo next = it.next();
            if (next != null) {
                String bizType = next.getBizType();
                if ("00".equals(bizType) || "70".equals(bizType) || Constants.VIA_REPORT_TYPE_SHARE_TO_QZONE.equals(bizType)) {
                    return next;
                }
            }
        }
        return bizInfos.get(0);
    }

    private String getFormatTime(MusicInfo musicInfo) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
        try {
            Date parse = simpleDateFormat.parse(musicInfo.getCrbtValidity());
            simpleDateFormat.applyPattern("yyyy-MM-dd");
            return simpleDateFormat.format(parse);
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    public void sureClicked() {
        Log.d(LOG_TAG, "sure button clicked");
        try {
            switch ($SWITCH_TABLE$com$cmsc$cmmusic$common$data$OrderPolicy$OrderType()[this.orderType.ordinal()]) {
                case 1:
                    Log.d(LOG_TAG, "buy Ringback");
                    if (this.bizinfo != null) {
                        EnablerInterface.buyRingback(this.mCurActivity, this.curMusicID, this.bizinfo.getBizCode(), this.bizinfo.getBizType(), this.bizinfo.getSalePrice(), this.policyObj.getMonLevel(), this.bizinfo.getHold2(), new CMMusicCallback<OrderResult>() {
                            public void operationResult(OrderResult orderResult) {
                                if (GetUserInfoRsp.NON_MEM_ERROR_CODE.equals(orderResult.getResCode())) {
                                    RingbackOrderView.this.mCurActivity.showToast(orderResult.getResMsg());
                                    UserInfo userInfo = RingbackOrderView.this.policyObj.getUserInfo();
                                    String str = null;
                                    if (userInfo != null) {
                                        str = userInfo.getMemLevel();
                                    }
                                    if (!"3".equals(str)) {
                                        RingbackOrderView.this.mHandler.post(new Runnable() {
                                            public void run() {
                                                OpenMemberView openMemberView = new OpenMemberView(RingbackOrderView.this.mCurActivity, new Bundle());
                                                RingbackOrderView.this.mCurActivity.setContentView(openMemberView);
                                                openMemberView.updateView(RingbackOrderView.this.policyObj);
                                            }
                                        });
                                        return;
                                    }
                                    return;
                                }
                                RingbackOrderView.this.mCurActivity.closeActivity(orderResult);
                            }
                        });
                        return;
                    }
                    return;
                case 2:
                    Log.d(LOG_TAG, "Get download url by sms");
                    this.mCurActivity.closeActivity(null);
                    return;
                case 3:
                    Log.d(LOG_TAG, "Get download url by sign code");
                    return;
                default:
                    return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        e.printStackTrace();
    }

    /* access modifiers changed from: protected */
    public void initContentView(LinearLayout linearLayout) {
        linearLayout.addView(getMemInfoView());
        this.txtInvalidDate = new TextView(this.mCurActivity);
        this.txtInvalidDate.setTextAppearance(this.mCurActivity, 16973892);
        linearLayout.addView(this.txtInvalidDate);
        this.txtPrice = new TextView(this.mCurActivity);
        this.txtPrice.setTextAppearance(this.mCurActivity, 16973892);
        linearLayout.addView(this.txtPrice);
    }
}
