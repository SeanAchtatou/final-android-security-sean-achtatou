package com.cmsc.cmmusic.common.data;

import java.util.ArrayList;

public class PayPolicy extends Result {
    private ArrayList<BizInfoNet> bizInfos;
    private String monLevel;
    private String sequencesID;

    public ArrayList<BizInfoNet> getBizInfos() {
        return this.bizInfos;
    }

    public void setBizInfos(ArrayList<BizInfoNet> arrayList) {
        this.bizInfos = arrayList;
    }

    public String getSequencesID() {
        return this.sequencesID;
    }

    public void setSequencesID(String str) {
        this.sequencesID = str;
    }

    public String getMonLevel() {
        return this.monLevel;
    }

    public void setMonLevel(String str) {
        this.monLevel = str;
    }

    public String toString() {
        return "PayPolicy [bizInfos=" + this.bizInfos + ", sequencesID=" + this.sequencesID + ", monLevel=" + this.monLevel + ", getResCode()=" + getResCode() + ", getResMsg()=" + getResMsg() + "]";
    }
}
