package com.cmsc.cmmusic.common.data;

import java.util.ArrayList;

public class UserResult extends Result {
    private ArrayList<AccountInfo> accountInfoList;
    private String registerMode;
    private PaymentUserInfo userInfo;

    public String getRegisterMode() {
        return this.registerMode;
    }

    public void setRegisterMode(String str) {
        this.registerMode = str;
    }

    public PaymentUserInfo getUserInfo() {
        return this.userInfo;
    }

    public void setUserInfo(PaymentUserInfo paymentUserInfo) {
        this.userInfo = paymentUserInfo;
    }

    public ArrayList<AccountInfo> getAccountInfoList() {
        return this.accountInfoList;
    }

    public void setAccountInfoList(ArrayList<AccountInfo> arrayList) {
        this.accountInfoList = arrayList;
    }

    public String toString() {
        return "UserResult [userInfo=" + this.userInfo + ", accountInfoList=" + this.accountInfoList + ", registerMode=" + this.registerMode + ", getResCode()=" + getResCode() + ", getResMsg()=" + getResMsg() + "]";
    }
}
