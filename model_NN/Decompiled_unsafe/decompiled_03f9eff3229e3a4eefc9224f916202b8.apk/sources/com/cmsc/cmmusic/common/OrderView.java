package com.cmsc.cmmusic.common;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.cmsc.cmmusic.common.data.MemberOpenPolicy;
import com.cmsc.cmmusic.common.data.MusicInfo;
import com.cmsc.cmmusic.common.data.OrderPolicy;
import com.cmsc.cmmusic.common.data.UserInfo;

abstract class OrderView extends BaseView {
    private static final String LOG_TAG = "OrderView";
    protected LinearLayout baseView;
    protected LinearLayout contentView;
    protected String curMusicID;
    protected String curSingerName;
    protected String curSongName;
    protected LinearLayout memInfoView;
    private TextView memLevelTxt;
    private Button openMemButton;
    private Button openSpecMemButton;
    protected OrderPolicy.OrderType orderType;
    private TextView smsPromptTxt;
    protected LinearLayout smsView;
    private TextView specMemInfoTxt;
    protected LinearLayout specMemInfoView;
    protected TextView txtSingerName;
    protected TextView txtSongName;
    private TextView txtUserTip;

    /* access modifiers changed from: protected */
    public abstract void initContentView(LinearLayout linearLayout);

    /* access modifiers changed from: protected */
    public abstract void updateNetView();

    public OrderView(Context context, Bundle bundle) {
        super(context, bundle);
        this.curMusicID = "";
        this.curSongName = "";
        this.curSingerName = "";
        this.txtSongName = null;
        this.txtSingerName = null;
        this.curMusicID = this.curExtraInfo.getString("MusicId");
        this.curSongName = this.curExtraInfo.getString("SongName");
        this.curSingerName = this.curExtraInfo.getString("SingerName");
        initBaseView(context);
        initContentView(context);
        initSmsView(context);
    }

    private void initContentView(Context context) {
        this.contentView = new LinearLayout(context);
        this.contentView.setOrientation(1);
        this.contentView.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        this.contentView.setPadding(10, 10, 10, 10);
        this.txtUserTip = new TextView(context);
        this.txtUserTip.setTextAppearance(context, 16973892);
        this.contentView.addView(this.txtUserTip);
        this.rootView.addView(this.contentView);
        initContentView(this.contentView);
    }

    /* access modifiers changed from: protected */
    public void setUserTip(String str) {
        this.txtUserTip.setText("尊敬的手机用户\n" + this.policyObj.getMobile() + "，" + str);
    }

    /* access modifiers changed from: protected */
    public void setUserTipDirect(String str) {
        this.txtUserTip.setText(str);
    }

    private void initBaseView(Context context) {
        this.baseView = new LinearLayout(context);
        this.baseView.setOrientation(1);
        this.baseView.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        this.baseView.setPadding(10, 10, 10, 10);
        this.txtSongName = new TextView(context);
        this.txtSongName.setTextAppearance(context, 16973892);
        this.txtSingerName = new TextView(context);
        this.txtSingerName.setTextAppearance(context, 16973892);
        this.baseView.addView(this.txtSongName);
        this.baseView.addView(this.txtSingerName);
        this.rootView.addView(this.baseView);
    }

    private void initSmsView(Context context) {
        this.smsView = new LinearLayout(context);
        this.smsView.setOrientation(1);
        this.smsPromptTxt = new TextView(context);
        this.smsPromptTxt.setTextAppearance(context, 16973892);
        this.smsView.addView(this.smsPromptTxt);
        this.rootView.addView(this.smsView);
    }

    /* access modifiers changed from: package-private */
    public void updateView(OrderPolicy orderPolicy) {
        Log.d(LOG_TAG, "policy : " + orderPolicy);
        this.policyObj = orderPolicy;
        updateView(orderPolicy.getOrderType());
    }

    private void updateView(final OrderPolicy.OrderType orderType2) {
        this.orderType = orderType2;
        this.mHandler.post(new Runnable() {
            private static /* synthetic */ int[] $SWITCH_TABLE$com$cmsc$cmmusic$common$data$OrderPolicy$OrderType;

            static /* synthetic */ int[] $SWITCH_TABLE$com$cmsc$cmmusic$common$data$OrderPolicy$OrderType() {
                int[] iArr = $SWITCH_TABLE$com$cmsc$cmmusic$common$data$OrderPolicy$OrderType;
                if (iArr == null) {
                    iArr = new int[OrderPolicy.OrderType.values().length];
                    try {
                        iArr[OrderPolicy.OrderType.net.ordinal()] = 1;
                    } catch (NoSuchFieldError e) {
                    }
                    try {
                        iArr[OrderPolicy.OrderType.sms.ordinal()] = 2;
                    } catch (NoSuchFieldError e2) {
                    }
                    try {
                        iArr[OrderPolicy.OrderType.verifyCode.ordinal()] = 3;
                    } catch (NoSuchFieldError e3) {
                    }
                    $SWITCH_TABLE$com$cmsc$cmmusic$common$data$OrderPolicy$OrderType = iArr;
                }
                return iArr;
            }

            public void run() {
                switch ($SWITCH_TABLE$com$cmsc$cmmusic$common$data$OrderPolicy$OrderType()[orderType2.ordinal()]) {
                    case 1:
                        if (OrderView.this.policyObj != null) {
                            MusicInfo musicInfo = OrderView.this.policyObj.getMusicInfo();
                            if (musicInfo != null) {
                                String songName = musicInfo.getSongName();
                                if (songName != null && songName.length() > 0) {
                                    OrderView.this.curSongName = "歌   曲:   " + songName;
                                }
                                String singerName = musicInfo.getSingerName();
                                if (singerName != null && singerName.length() > 0) {
                                    OrderView.this.curSingerName = "歌   手:   " + singerName;
                                }
                            }
                            OrderView.this.updateMemInfoView();
                            OrderView.this.updateNetView();
                        }
                        OrderView.this.contentView.setVisibility(0);
                        OrderView.this.smsView.setVisibility(8);
                        break;
                    case 2:
                        OrderView.this.contentView.setVisibility(8);
                        OrderView.this.smsView.setVisibility(0);
                        break;
                }
                if (OrderView.this.curSongName == null || OrderView.this.curSongName.length() <= 0) {
                    OrderView.this.txtSongName.setText("");
                    OrderView.this.txtSongName.setVisibility(8);
                } else {
                    OrderView.this.txtSongName.setText(OrderView.this.curSongName);
                    OrderView.this.txtSongName.setVisibility(0);
                }
                if (OrderView.this.curSingerName == null || OrderView.this.curSingerName.length() <= 0) {
                    OrderView.this.txtSingerName.setText("");
                    OrderView.this.txtSingerName.setVisibility(8);
                } else {
                    OrderView.this.txtSingerName.setText(OrderView.this.curSingerName);
                    OrderView.this.txtSingerName.setVisibility(0);
                }
                OrderView.this.setVisibility(0);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void setSmsPrompt(String str) {
        this.smsPromptTxt.setText(str);
    }

    /* access modifiers changed from: protected */
    public LinearLayout getMemInfoView() {
        this.memInfoView = new LinearLayout(this.mCurActivity);
        this.memInfoView.setOrientation(0);
        this.memInfoView.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        this.memInfoView.setPadding(0, 10, 10, 0);
        this.memInfoView.setVisibility(8);
        this.openMemButton = new Button(this.mCurActivity);
        this.openMemButton.setHeight(30);
        this.openMemButton.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
        this.openMemButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                OrderView.this.mCurActivity.showProgressBar("请稍候...");
                new Thread() {
                    public void run() {
                        try {
                            final MemberOpenPolicy queryMemberOpenPolicyByNet = EnablerInterface.queryMemberOpenPolicyByNet(OrderView.this.mCurActivity);
                            if (queryMemberOpenPolicyByNet == null || !"000000".equals(queryMemberOpenPolicyByNet.getResCode())) {
                                OrderView.this.mCurActivity.showToast("请求失败");
                                return;
                            }
                            OrderView.this.mHandler.post(new Runnable() {
                                public void run() {
                                    OpenMemberView openMemberView = new OpenMemberView(OrderView.this.mCurActivity, new Bundle());
                                    OrderView.this.mCurActivity.setContentView(openMemberView);
                                    OrderView.this.policyObj.setClubUserInfos(queryMemberOpenPolicyByNet.getClubUserInfos());
                                    openMemberView.updateView(OrderView.this.policyObj);
                                }
                            });
                            OrderView.this.mCurActivity.hideProgressBar();
                        } catch (Exception e) {
                            e.printStackTrace();
                            OrderView.this.mCurActivity.showToast("请求失败");
                        } finally {
                            OrderView.this.mCurActivity.hideProgressBar();
                        }
                    }
                }.start();
            }
        });
        this.openMemButton.setVisibility(8);
        this.memLevelTxt = new TextView(this.mCurActivity);
        this.memLevelTxt.setTextAppearance(this.mCurActivity, 16973892);
        this.memInfoView.addView(this.memLevelTxt);
        this.memInfoView.addView(this.openMemButton);
        return this.memInfoView;
    }

    /* access modifiers changed from: protected */
    public LinearLayout getSpecMemInfoView() {
        this.specMemInfoView = new LinearLayout(this.mCurActivity);
        this.specMemInfoView.setOrientation(1);
        this.specMemInfoView.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        this.specMemInfoView.setPadding(0, 10, 10, 0);
        this.specMemInfoView.setVisibility(8);
        this.openSpecMemButton = new Button(this.mCurActivity);
        this.openSpecMemButton.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
        this.openSpecMemButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                OrderView.this.mCurActivity.showProgressBar("请稍候...");
                new Thread() {
                    public void run() {
                        try {
                            OrderView.this.mHandler.post(new Runnable() {
                                public void run() {
                                    OpenMemberView openMemberView = new OpenMemberView(OrderView.this.mCurActivity, new Bundle());
                                    OrderView.this.mCurActivity.setContentView(openMemberView);
                                    openMemberView.updateView(OrderView.this.policyObj);
                                }
                            });
                        } catch (Exception e) {
                            e.printStackTrace();
                            OrderView.this.mCurActivity.showToast("请求失败");
                        } finally {
                            OrderView.this.mCurActivity.hideProgressBar();
                        }
                    }
                }.start();
            }
        });
        this.openSpecMemButton.setText("了解咪咕特级会员");
        this.openSpecMemButton.setVisibility(8);
        this.specMemInfoTxt = new TextView(this.mCurActivity);
        this.specMemInfoTxt.setTextAppearance(this.mCurActivity, 16973892);
        this.specMemInfoView.addView(this.specMemInfoTxt);
        this.specMemInfoView.addView(this.openSpecMemButton);
        return this.specMemInfoView;
    }

    /* access modifiers changed from: protected */
    public void updateMemInfoView() {
        String str;
        UserInfo userInfo = this.policyObj.getUserInfo();
        if (userInfo != null && this.memInfoView != null) {
            String memLevel = userInfo.getMemLevel();
            if ("1".equals(memLevel)) {
                str = "普通会员";
                this.openMemButton.setText("开通特级会员");
            } else if ("2".equals(memLevel)) {
                str = "高级会员";
                this.openMemButton.setText("开通特级会员");
            } else if ("3".equals(memLevel)) {
                str = "特级会员";
                this.openMemButton.setVisibility(8);
            } else {
                str = "非会员";
                this.openMemButton.setText("开通特级会员");
            }
            this.memLevelTxt.setText("会员级别:  " + str + "   ");
            this.memInfoView.setVisibility(0);
        }
    }

    /* access modifiers changed from: package-private */
    public void updateSpecMemInfoView(String str) {
        if (this.specMemInfoView != null) {
            this.openSpecMemButton.setVisibility(0);
            this.specMemInfoTxt.setText(str);
            this.specMemInfoView.setVisibility(0);
        }
    }
}
