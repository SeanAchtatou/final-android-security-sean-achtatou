package com.cmsc.cmmusic.common.data;

public class StreamRsp extends Result {
    String streamUrl;

    public String getStreamUrl() {
        return this.streamUrl;
    }

    public void setStreamUrl(String str) {
        this.streamUrl = str;
    }

    public String toString() {
        return "StreamRsp [streamUrl=" + this.streamUrl + ", getResCode()=" + getResCode() + ", getResMsg()=" + getResMsg() + "]";
    }
}
