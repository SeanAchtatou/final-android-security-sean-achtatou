package com.cmsc.cmmusic.common.data;

public class ClubUserInfo {
    private String memDesc;
    private String memLevel;

    public String getMemLevel() {
        return this.memLevel;
    }

    public void setMemLevel(String str) {
        this.memLevel = str;
    }

    public String getMemDesc() {
        return this.memDesc;
    }

    public void setMemDesc(String str) {
        this.memDesc = str;
    }

    public String toString() {
        return "ClubUserInfo [memLevel=" + this.memLevel + ", memDesc=" + this.memDesc + "]";
    }
}
