package com.cmsc.cmmusic.common;

import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.text.method.DigitsKeyListener;
import android.util.Log;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.cmsc.cmmusic.common.data.BizInfo;
import com.cmsc.cmmusic.common.data.OrderPolicy;
import com.cmsc.cmmusic.common.data.OrderResult;
import com.cmsc.cmmusic.init.Utils;
import java.util.ArrayList;
import java.util.Iterator;

final class GiveRingbackView extends RingbackOrderView {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$cmsc$cmmusic$common$data$OrderPolicy$OrderType = null;
    private static final String LOG_TAG = "GiveRingbackView";
    private EditText edtPhoneNum;
    private LinearLayout phoneNumView;
    private TextView txtPhoneTip;

    static /* synthetic */ int[] $SWITCH_TABLE$com$cmsc$cmmusic$common$data$OrderPolicy$OrderType() {
        int[] iArr = $SWITCH_TABLE$com$cmsc$cmmusic$common$data$OrderPolicy$OrderType;
        if (iArr == null) {
            iArr = new int[OrderPolicy.OrderType.values().length];
            try {
                iArr[OrderPolicy.OrderType.net.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[OrderPolicy.OrderType.sms.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[OrderPolicy.OrderType.verifyCode.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$com$cmsc$cmmusic$common$data$OrderPolicy$OrderType = iArr;
        }
        return iArr;
    }

    public GiveRingbackView(Context context, Bundle bundle) {
        super(context, bundle);
    }

    /* access modifiers changed from: protected */
    public void updateNetView() {
        super.updateNetView();
        setUserTip("点击“确认”将把该歌曲赠送给您的好友");
        this.txtPhoneTip.setText("请输入好友的手机号码：");
        this.edtPhoneNum.setText(this.curExtraInfo.getString("PhoneNum"));
        this.memInfoView.setVisibility(8);
    }

    /* access modifiers changed from: protected */
    public BizInfo getBizInfo() {
        ArrayList<BizInfo> bizInfos = this.policyObj.getBizInfos();
        if (bizInfos == null) {
            return null;
        }
        Iterator<BizInfo> it = bizInfos.iterator();
        while (it.hasNext()) {
            BizInfo next = it.next();
            if (next != null && "30".equals(next.getBizType())) {
                return next;
            }
        }
        return bizInfos.get(0);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    public void sureClicked() {
        Log.d(LOG_TAG, "sure button clicked");
        String editable = this.edtPhoneNum.getText().toString();
        if (editable == null || !Utils.validatePhoneNumber(editable)) {
            this.edtPhoneNum.requestFocus();
            this.edtPhoneNum.setError(Html.fromHtml("<font color='red'>请正确输入手机号码</font>"));
            return;
        }
        try {
            switch ($SWITCH_TABLE$com$cmsc$cmmusic$common$data$OrderPolicy$OrderType()[this.orderType.ordinal()]) {
                case 1:
                    BizInfo bizInfo = getBizInfo();
                    if (bizInfo != null) {
                        EnablerInterface.giveRingback(this.mCurActivity, editable, this.curExtraInfo.getString("MusicId"), bizInfo.getBizCode(), bizInfo.getBizType(), bizInfo.getSalePrice(), this.policyObj.getMonLevel(), bizInfo.getHold2(), new CMMusicCallback<OrderResult>() {
                            public void operationResult(OrderResult orderResult) {
                                GiveRingbackView.this.mCurActivity.closeActivity(orderResult);
                            }
                        });
                        return;
                    }
                    return;
                case 2:
                    this.mCurActivity.closeActivity(null);
                    return;
                default:
                    return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        e.printStackTrace();
    }

    /* access modifiers changed from: protected */
    public void initContentView(LinearLayout linearLayout) {
        linearLayout.addView(getPhoneNumView());
        super.initContentView(linearLayout);
        updateSpecMemInfoView("推荐：开通咪咕特级会员专享彩铃赠送7折优惠。");
    }

    private LinearLayout getPhoneNumView() {
        this.phoneNumView = new LinearLayout(this.mCurActivity);
        this.phoneNumView.setOrientation(1);
        this.phoneNumView.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        this.phoneNumView.setVisibility(0);
        this.phoneNumView.setPadding(0, 10, 0, 0);
        this.phoneNumView.setGravity(16);
        this.txtPhoneTip = new TextView(this.mCurActivity);
        this.txtPhoneTip.setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
        this.txtPhoneTip.setTextAppearance(this.mCurActivity, 16973892);
        this.txtPhoneTip.setGravity(17);
        this.phoneNumView.addView(this.txtPhoneTip);
        this.edtPhoneNum = new EditText(this.mCurActivity);
        this.edtPhoneNum.setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
        this.edtPhoneNum.setInputType(3);
        this.edtPhoneNum.setKeyListener(new DigitsKeyListener(false, false));
        this.phoneNumView.addView(this.edtPhoneNum);
        return this.phoneNumView;
    }
}
