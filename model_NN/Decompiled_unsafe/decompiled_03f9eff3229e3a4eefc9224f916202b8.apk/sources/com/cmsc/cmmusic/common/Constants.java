package com.cmsc.cmmusic.common;

class Constants {
    static final String BUY_RINGBACK_URL = "http://218.200.227.123:95/sdkServer/1.0/crbt/order";
    static final String CANCEL_CP_MONTH_URL = "http://218.200.227.123:95/sdkServer/1.0/cp/cancel";
    static final String CANCLE_OWN_RING_MONTH_URL = "http://218.200.227.123:95/sdkServer/1.0/diycrbt/cancelMonth";
    static final String CHECK_OPEN_RINGBACK_URL = "http://218.200.227.123:95/sdkServer/1.0/crbt/open/check";
    static final String CHECK_SMSAUTH_LOGIN_URL = "http://218.200.227.123:95/sdkServer/1.0/crbt/smsAuthLoginValidate";
    static final String CHECK_VALIDATE_CODE_URL = "http://218.200.227.123:95/sdkServer/1.0/crbt/smsLoginAuth";
    static final String CMNET = "CMNET";
    static final String CMWAP = "CMWAP";
    static final boolean DEBUG = false;
    static final String DELETE_PERSON_RINGBACK_URL = "http://218.200.227.123:95/sdkServer/1.0/crbt/box/delete";
    static final String DIGITAL_ALBUM_AUTO_OPEN_MUMBER_URL = "http://218.200.227.123:95/sdkServer/1.0/album/memlv3/open";
    static final String DIGITAL_ALBUM_AUTO_OPEN_RINGBACK_URL = "http://218.200.227.123:95/sdkServer/1.0/album/crbt/open";
    static final String DIGITAL_ALBUM_GIVE_URL = "http://218.200.227.123:95/sdkServer/1.0/album/present";
    static final String DIGITAL_ALBUM_ORDER_URL = "http://218.200.227.123:95/sdkServer/1.0/album/order";
    static final String DOWNLOAD_FULLSONG_CP_MONTH_URL = "http://218.200.227.123:95/sdkServer/1.0/cp/sdownlink";
    static final String DOWNLOAD_FULLSONG_CP_URL = "http://218.200.227.123:95/sdkServer/1.0/cp/timesdownlink";
    static final String DOWNLOAD_FULL_SONG_URL = "http://218.200.227.123:95/sdkServer/1.0/song/downlink";
    static final String DOWNLOAD_VIBRATERING_CP_MONTH_URL = "http://218.200.227.123:95/sdkServer/1.0/cp/rdownlink";
    static final String DOWNLOAD_VIBRATERING_CP_URL = "http://218.200.227.123:95/sdkServer/1.0/cp/timerdownlink";
    static final String DOWNLOAD_VIBRATERING_RING_URL = "http://218.200.227.123:95/sdkServer/1.0/ring/downlink";
    static final String ENABLER_URL_DOMAIN = "http://218.200.227.123:95/sdkServer/1.0";
    static final String ENABLER_URL_DOMAIN_PRODUCTION = "http://218.200.227.123:95/sdkServer/1.0";
    static final String ENCODE = "UTF-8";
    static final String FULLSONG_DOWNLOAD_POLICY_MUSIC_URL = "http://218.200.227.123:95/sdkServer/1.0/song/policy";
    static final String GET_RINGBACK_ASKFOR_URL = "http://218.200.227.123:95/sdkServer/1.0/crbt/askfor";
    static final String GET_RINGBACK_POLICY_URL = "http://218.200.227.123:95/sdkServer/1.0/crbt/policy";
    static final String GET_RINGBACK_PRELISTEN_URL = "http://218.200.227.123:95/sdkServer/1.0/crbt/prelisten";
    static final String GET_USER_INFO_URL = "http://218.200.227.123:95/sdkServer/1.0/user/query";
    static final String GET_VALIDATE_CODE_URL = "http://218.200.227.123:95/sdkServer/1.0/crbt/getValidateCode";
    static final String GIVE_FULL_SONG_URL = "http://218.200.227.123:95/sdkServer/1.0/song/present";
    static final String GIVE_RINGBACK_URL = "http://218.200.227.123:95/sdkServer/1.0/crbt/present";
    static final String GIVE_VIBRATERING_URL = "http://218.200.227.123:95/sdkServer/1.0/ring/present";
    static final String IS_OWN_RING_ORDER_MONTH_USER_URL = "http://218.200.227.123:95/sdkServer/1.0/diycrbt/queryMonth";
    static final String KEY_ORDER_OWN_RING_MONTH_URL = "http://218.200.227.123:95/sdkServer/1.0/diycrbt/openOrderMonth";
    static final String MV_DOWN_POLICY_URL = "http://218.200.227.123:95/sdkServer/1.0/mv/policy";
    static final String MV_DOWN_URL = "http://218.200.227.123:95/sdkServer/1.0/mv/downlink";
    static final String MV_MONTH_CANCEL_URL = "http://218.200.227.123:95/sdkServer/1.0/mv/cancel";
    static final String MV_MONTH_POLICY_URL = "http://218.200.227.123:95/sdkServer/1.0/mv/query";
    static final String MV_OPEN_MONTH_URL = "http://218.200.227.123:95/sdkServer/1.0/mv/open";
    static final String NIISNULL = "NIISNUll";
    static final String NOWM = "NOWM";
    static final String ONLINE_LISTENER_URL = "http://218.200.227.123:95/sdkServer/1.0/stream/query";
    static final String OPEN_CP_MONTH_URL = "http://218.200.227.123:95/sdkServer/1.0/cp/open";
    static final String OPEN_EXCLUSIVE_ONCE_URL = "http://218.200.227.123:95/sdkServer/1.0/exclusive/open";
    static final String OPEN_FULLSONG_MONTH_URL = "http://218.200.227.123:95/sdkServer/1.0/song/open";
    static final String OPEN_MEMBER_URL = "http://218.200.227.123:95/sdkServer/1.0/user/member/open";
    static final String OPEN_RINGBACK_URL = "http://218.200.227.123:95/sdkServer/1.0/crbt/open";
    static final String ORDER_OWN_RING_URL = "http://218.200.227.123:95/sdkServer/1.0/diycrbt/orderDiycrbt";
    static final String OTHER = "OTHER";
    static final String OWNRING_BACKGROUND_MUSIC_URL = "http://218.200.227.123:95/sdkServer/1.0/diycrbt/bgMusic";
    static final String OWNRING_MP3_URL = "http://218.200.227.123:95/sdkServer/1.0/diycrbt/upload";
    static final String OWNRING_TEXT_URL = "http://218.200.227.123:95/sdkServer/1.0/diycrbt/createRing";
    static final String OWN_RING_MONTH_URL = "http://218.200.227.123:95/sdkServer/1.0/diycrbt/orderMonth";
    static final String PAYMENT_ACCOUNT_BANK_URL = "http://218.200.227.123:95/sdkServer/1.0/pay/account/bank";
    static final String PAYMENT_ACCOUNT_PAYMENT_URL = "http://218.200.227.123:95/sdkServer/1.0/pay/account/paymentInfo";
    static final String PAYMENT_ACCOUNT_QUERY_URL = "http://218.200.227.123:95/sdkServer/1.0/pay/account/query";
    static final String PAYMENT_ACCOUNT_RECHARGE_URL = "http://218.200.227.123:95/sdkServer/1.0/pay/account/recharge";
    static final String PAYMENT_ACCOUNT_TRANSFER_URL = "http://218.200.227.123:95/sdkServer/1.0/pay/account/transferInfo";
    static final String PAYMENT_MEMBER_CHECK_URL = "http://218.200.227.123:95/sdkServer/1.0/pay/member/check";
    static final String PAYMENT_MEMBER_LOGIN_URL = "http://218.200.227.123:95/sdkServer/1.0/pay/member/login";
    static final String PAYMENT_MEMBER_QUERY_URL = "http://218.200.227.123:95/sdkServer/1.0/pay/member/query";
    static final String PAYMENT_MEMBER_REGISTER_URL = "http://218.200.227.123:95/sdkServer/1.0/pay/member/register";
    static final String PAYMENT_MEMBER_TRANSFER_URL = "http://218.200.227.123:95/sdkServer/1.0/pay/account/transfer";
    static final String PAYMENT_MEMBER_UPDATE_URL = "http://218.200.227.123:95/sdkServer/1.0/pay/member/update";
    static final String PAYMENT_ORDER_FULLSONG_URL = "http://218.200.227.123:95/sdkServer/1.0/pay/order/sdownlink";
    static final String PAYMENT_ORDER_OPEN_URL = "http://218.200.227.123:95/sdkServer/1.0/pay/order/open";
    static final String PAYMENT_ORDER_POLICY_URL = "http://218.200.227.123:95/sdkServer/1.0/pay/order/policy";
    static final String PAYMENT_ORDER_VIBRATE_URL = "http://218.200.227.123:95/sdkServer/1.0/pay/order/rdownlink";
    static final String QUERY_CP_MONTH_POLICY_URL = "http://218.200.227.123:95/sdkServer/1.0/cp/query";
    static final String QUERY_DIGITAL_ALBUM_INFO_URL = "http://218.200.227.123:95/sdkServer/1.0/album/orderCount";
    static final String QUERY_EXCLUSIVE_URL = "http://218.200.227.123:95/sdkServer/1.0/cp/serviceEx";
    static final String QUERY_FULLSONG_MONTH_POLICY_URL = "http://218.200.227.123:95/sdkServer/1.0/song/month/query";
    static final String QUERY_OPEN_MEMBER_POLICY_URL = "http://218.200.227.123:95/sdkServer/1.0/user/member/query";
    static final String QUERY_RINGBACK_BOX_URL = "http://218.200.227.123:95/sdkServer/1.0/crbt/box/query";
    static final String QUERY_RINGBACK_BY_MSISDN_URL = "http://218.200.227.123:95/sdkServer/1.0/crbt/msisdn/query";
    static final String RINGBACK_AUTO_OPEN_MUMBER_URL = "http://218.200.227.123:95/sdkServer/1.0/crbt/simpMemberOrder";
    static final String RINGBACK_AUTO_OPEN_RINGBACK_URL = "http://218.200.227.123:95/sdkServer/1.0/crbt/simpOrder";
    static final String SDK_VERSION = "M2.1";
    static final String SEARCH_ALBUM_BY_MUSIC_URL = "http://218.200.227.123:95/sdkServer/1.0/search/album/listbymusic";
    static final String SEARCH_ALBUM_BY_SINGER_URL = "http://218.200.227.123:95/sdkServer/1.0/search/album/listbysinger";
    static final String SEARCH_CHART_LIST_URL = "http://218.200.227.123:95/sdkServer/1.0/search/chart/list";
    static final String SEARCH_MUSIC_BY_ALBUM_URL = "http://218.200.227.123:95/sdkServer/1.0/search/music/listbyalbum";
    static final String SEARCH_MUSIC_BY_CHART_URL = "http://218.200.227.123:95/sdkServer/1.0/search/music/listbychart";
    static final String SEARCH_MUSIC_BY_KEY_URL = "http://218.200.227.123:95/sdkServer/1.0/search/music/listbykey";
    static final String SEARCH_MUSIC_BY_MUSIC_URL = "http://218.200.227.123:95/sdkServer/1.0/search/music/querybymusic";
    static final String SEARCH_MUSIC_BY_SINGER_URL = "http://218.200.227.123:95/sdkServer/1.0/search/music/listbysinger";
    static final String SEARCH_MUSIC_BY_TAG_URL = "http://218.200.227.123:95/sdkServer/1.0/search/music/listbytag";
    static final String SEARCH_SINGER_INFO_URL = "http://218.200.227.123:95/sdkServer/1.0/search/singer/infobyid";
    static final String SEARCH_TAG_LIST_URL = "http://218.200.227.123:95/sdkServer/1.0/search/tag/list";
    static final String SET_DEFAULT_RINGBACK_URL = "http://218.200.227.123:95/sdkServer/1.0/crbt/box/default";
    static final String SR_ASSOCIATE_SINGER_SONG_RECOMMEND_URL = "http://218.200.227.123:95/sdkServer/1.0/recommond/singer";
    static final String SR_ASSOCIATE_SONG_RECOMMEND_URL = "http://218.200.227.123:95/sdkServer/1.0/recommond/music";
    static final String SR_DEAL_SONG_RECOMMEND_URL = "http://218.200.227.123:95/sdkServer/1.0/recommond/coop";
    static final String SR_LIKE_SONG_RECOMMEND_URL = "http://218.200.227.123:95/sdkServer/1.0/recommond/msisdn";
    static final String SUCCESS_CODE = "000000";
    static final String TAG = "SDK_MIGU_CMM";
    static final String VIBRATERING_DOWNLOAD_POLICY_MUSIC_URL = "http://218.200.227.123:95/sdkServer/1.0/ring/policy";
    static final String VIBRATERING_PRELISTEN_MUSIC_URL = "http://218.200.227.123:95/sdkServer/1.0/ring/query";
    static final String WIFI = "WIFI";
    static final String ZERO = "0";

    Constants() {
    }
}
