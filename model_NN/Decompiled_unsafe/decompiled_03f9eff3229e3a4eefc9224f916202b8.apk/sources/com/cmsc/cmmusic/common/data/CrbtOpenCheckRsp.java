package com.cmsc.cmmusic.common.data;

public class CrbtOpenCheckRsp extends Result {
    private String description;
    private String mobile;
    private String price;

    public String getPrice() {
        return this.price;
    }

    public void setPrice(String str) {
        this.price = str;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String str) {
        this.description = str;
    }

    public String getMobile() {
        return this.mobile;
    }

    public void setMobile(String str) {
        this.mobile = str;
    }

    public String toString() {
        return "CrbtOpenCheckRsp [price=" + this.price + ", description=" + this.description + ", mobile=" + this.mobile + ", getResCode()=" + getResCode() + ", getResMsg()=" + getResMsg() + "]";
    }
}
