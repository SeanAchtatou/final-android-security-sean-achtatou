package com.cmsc.cmmusic.common;

import android.content.Context;
import android.os.Bundle;
import com.cmsc.cmmusic.common.MiguSdkUtil;
import com.cmsc.cmmusic.common.data.OrderResult;
import java.io.IOException;

public class FullSongManagerInterface {
    public static void getFullSongDownloadUrl(Context context, String str, CMMusicCallback<OrderResult> cMMusicCallback) {
        Bundle bundle = new Bundle();
        bundle.putString("MusicId", str);
        CMMusicActivity.showActivityDefault(context, bundle, 1, cMMusicCallback);
    }

    public static void getFullSongDownloadUrl(Context context, String str, String str2, String str3, String str4, String str5, String str6, String str7, CMMusicCallback<OrderResult> cMMusicCallback) {
        MiguSdkUtil.TL.init();
        EnablerInterface.getFullSongDownloadUrl(context, str, str2, str3, str4, str5, str6, str7, cMMusicCallback);
    }

    public static void giveFullSong(Context context, String str, String str2, String str3, String str4, String str5, String str6, String str7, CMMusicCallback<OrderResult> cMMusicCallback) {
        EnablerInterface.giveSong(context, "http://218.200.227.123:95/sdkServer/1.0/song/present", str, EnablerInterface.getFullSong12Id(str2), str3, str4, str5, str6, str7, cMMusicCallback);
    }

    public static String getFullSongPolicy(Context context, String str) {
        try {
            return HttpPostCore.httpConnectionToString(context, "http://218.200.227.123:95/sdkServer/1.0/song/policy", EnablerInterface.buildGetSongPolicyRequsetXml(str));
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
