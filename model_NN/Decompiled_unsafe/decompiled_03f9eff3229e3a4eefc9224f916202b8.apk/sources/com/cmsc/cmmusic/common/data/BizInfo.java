package com.cmsc.cmmusic.common.data;

public class BizInfo {
    private String bizCode;
    private String bizType;
    private String description;
    private String hold2;
    private String originalPrice;
    private String payType;
    private String resource;
    private String salePrice;

    public String getHold2() {
        return this.hold2;
    }

    public void setHold2(String str) {
        this.hold2 = str;
    }

    public String getResource() {
        return this.resource;
    }

    public void setResource(String str) {
        this.resource = str;
    }

    public String getBizCode() {
        return this.bizCode;
    }

    public void setBizCode(String str) {
        this.bizCode = str;
    }

    public String getBizType() {
        return this.bizType;
    }

    public void setBizType(String str) {
        this.bizType = str;
    }

    public String getOriginalPrice() {
        return this.originalPrice;
    }

    public void setOriginalPrice(String str) {
        this.originalPrice = str;
    }

    public String getSalePrice() {
        return this.salePrice;
    }

    public void setSalePrice(String str) {
        this.salePrice = str;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String str) {
        this.description = str;
    }

    public String getPayType() {
        return this.payType;
    }

    public void setPayType(String str) {
        this.payType = str;
    }

    public String toString() {
        return "BizInfo [bizCode=" + this.bizCode + ", bizType=" + this.bizType + ", originalPrice=" + this.originalPrice + ", salePrice=" + this.salePrice + ", description=" + this.description + ", resource=" + this.resource + ", payType=" + this.payType + ", hold2=" + this.hold2 + "]";
    }
}
