package com.cmsc.cmmusic.common;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.internal.view.SupportMenu;
import android.text.Html;
import android.text.Spanned;
import android.text.method.DigitsKeyListener;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import cn.banshenggua.aichang.utils.Constants;
import com.cmsc.cmmusic.common.data.OrderPolicy;
import java.io.InputStream;

final class TransferView extends LinearLayout {
    private static final String KEY = "sdk2l@31";
    protected Button btnCancel = null;
    protected Button btnSure = null;
    protected LinearLayout btnView = null;
    protected Bundle curExtraInfo = null;
    private EditText edtConfirm;
    private EditText edtName;
    private EditText edtPwd;
    protected CMMusicActivity mCurActivity;
    protected Handler mHandler = null;
    protected OrderPolicy policyObj;
    protected LinearLayout rootView = null;
    private CheckBox tipCb;

    public TransferView(Context context, Bundle bundle) {
        super(context);
        this.mCurActivity = (CMMusicActivity) context;
        this.mHandler = new Handler();
        this.curExtraInfo = bundle;
        setOrientation(1);
        setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        this.rootView = new LinearLayout(context);
        this.rootView.setOrientation(1);
        this.rootView.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        this.rootView.setPadding(10, 5, 10, 20);
        ScrollView scrollView = new ScrollView(context);
        scrollView.setLayoutParams(new FrameLayout.LayoutParams(-1, -2));
        scrollView.addView(this.rootView);
        addView(scrollView);
        initLogoView();
        initContentView(context);
        initBtnView(context);
        initBottomView(context);
    }

    private void initLogoView() {
        LinearLayout linearLayout = new LinearLayout(this.mCurActivity);
        linearLayout.setOrientation(1);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        try {
            InputStream open = this.mCurActivity.getAssets().open("logo.png");
            BitmapDrawable bitmapDrawable = new BitmapDrawable(BitmapFactory.decodeStream(open));
            open.close();
            ImageView imageView = new ImageView(this.mCurActivity);
            imageView.setImageDrawable(bitmapDrawable);
            imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
            imageView.setLayoutParams(new LinearLayout.LayoutParams(-1, 100));
            linearLayout.addView(imageView);
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.rootView.addView(linearLayout);
    }

    private void initContentView(Context context) {
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(0);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        layoutParams.setMargins(dip2px(10.0f), dip2px(10.0f), dip2px(10.0f), 0);
        linearLayout.setLayoutParams(layoutParams);
        TextView textView = new TextView(context);
        textView.setLayoutParams(new LinearLayout.LayoutParams(-1, -2, 3.0f));
        textView.setTextAppearance(context, 16973892);
        textView.setText("收款人ID：");
        textView.setGravity(7);
        linearLayout.addView(textView);
        this.edtName = new EditText(this.mCurActivity);
        this.edtName.setLayoutParams(new LinearLayout.LayoutParams(-1, -2, 2.0f));
        linearLayout.addView(this.edtName);
        this.rootView.addView(linearLayout);
        LinearLayout linearLayout2 = new LinearLayout(context);
        linearLayout2.setOrientation(0);
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-1, -2);
        layoutParams2.setMargins(dip2px(10.0f), dip2px(10.0f), dip2px(10.0f), 0);
        linearLayout2.setLayoutParams(layoutParams2);
        TextView textView2 = new TextView(context);
        textView2.setLayoutParams(new LinearLayout.LayoutParams(-1, -2, 3.0f));
        textView2.setTextAppearance(context, 16973892);
        textView2.setText("转账金额：");
        textView2.setGravity(7);
        linearLayout2.addView(textView2);
        this.edtPwd = new EditText(this.mCurActivity);
        LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(-1, -2, 2.0f);
        layoutParams3.setMargins(15, 0, 0, 0);
        this.edtPwd.setLayoutParams(layoutParams3);
        this.edtPwd.setKeyListener(new DigitsKeyListener(false, true));
        linearLayout2.addView(this.edtPwd);
        TextView textView3 = new TextView(context);
        textView3.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
        textView3.setTextAppearance(context, 16973892);
        textView3.setText("元");
        textView3.setGravity(7);
        linearLayout2.addView(textView3);
        this.rootView.addView(linearLayout2);
        LinearLayout linearLayout3 = new LinearLayout(context);
        linearLayout3.setOrientation(0);
        LinearLayout.LayoutParams layoutParams4 = new LinearLayout.LayoutParams(-1, -2);
        layoutParams4.setMargins(dip2px(10.0f), dip2px(10.0f), dip2px(10.0f), 0);
        linearLayout3.setLayoutParams(layoutParams4);
        TextView textView4 = new TextView(context);
        textView4.setLayoutParams(new LinearLayout.LayoutParams(-1, -2, 3.0f));
        textView4.setTextAppearance(context, 16973892);
        textView4.setText("支付密码：");
        textView4.setGravity(7);
        linearLayout3.addView(textView4);
        this.edtConfirm = new EditText(this.mCurActivity);
        this.edtConfirm.setLayoutParams(new LinearLayout.LayoutParams(-1, -2, 2.0f));
        linearLayout3.addView(this.edtConfirm);
        this.rootView.addView(linearLayout3);
        LinearLayout linearLayout4 = new LinearLayout(context);
        linearLayout4.setOrientation(0);
        LinearLayout.LayoutParams layoutParams5 = new LinearLayout.LayoutParams(-1, -2);
        layoutParams5.setMargins(dip2px(10.0f), dip2px(10.0f), dip2px(10.0f), 0);
        linearLayout4.setLayoutParams(layoutParams5);
        TextView textView5 = new TextView(context);
        textView5.setLayoutParams(new LinearLayout.LayoutParams(-1, -2, 3.0f));
        textView5.setTextAppearance(context, 16973892);
        textView5.setText("");
        textView5.setGravity(7);
        linearLayout4.addView(textView5);
        TextView textView6 = new TextView(context);
        textView6.setLayoutParams(new LinearLayout.LayoutParams(-1, -2, 2.0f));
        textView6.setText("（支付密码可以为空）");
        textView6.setTextColor((int) SupportMenu.CATEGORY_MASK);
        textView6.setGravity(7);
        linearLayout4.addView(textView6);
        this.rootView.addView(linearLayout4);
    }

    private void initBtnView(Context context) {
        this.btnView = new LinearLayout(context);
        this.btnView.setOrientation(0);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        layoutParams.setMargins(dip2px(10.0f), dip2px(10.0f), dip2px(10.0f), 0);
        this.btnView.setLayoutParams(layoutParams);
        this.btnSure = new Button(context);
        this.btnSure.setText("转账");
        this.btnSure.setLayoutParams(new LinearLayout.LayoutParams(-2, -2, 0.5f));
        if (this.tipCb != null) {
            this.btnSure.setEnabled(this.tipCb.isChecked());
        }
        this.btnView.addView(this.btnSure);
        this.btnSure.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                TransferView.this.sureClicked();
            }
        });
        this.btnCancel = new Button(context);
        this.btnCancel.setText("取消");
        this.btnCancel.setLayoutParams(new LinearLayout.LayoutParams(-2, -2, 0.5f));
        this.btnView.addView(this.btnCancel);
        this.btnCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Log.d(getClass().getSimpleName(), "cancel button clicked");
                TransferView.this.cancelClicked();
            }
        });
        addView(this.btnView);
    }

    private void initBottomView(Context context) {
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(1);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -1);
        linearLayout.setGravity(16);
        layoutParams.setMargins(dip2px(20.0f), 0, dip2px(10.0f), 0);
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.setPadding(0, -dip2px(25.0f), 0, 0);
        TextView textView = new TextView(context);
        textView.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        textView.setText("【提示】\n1、收款人ID为收款人的UID；\n2、转账只能转咪咕币；可以输入小数点后两位，如0.01元；\n3、如果未设置支付密码的用户，可以无须输入密码，直接进行转账；");
        textView.setGravity(7);
        linearLayout.addView(textView);
        addView(linearLayout);
    }

    /* access modifiers changed from: protected */
    public void cancelClicked() {
        this.mCurActivity.closeActivity(null);
    }

    /* access modifiers changed from: protected */
    public void sureClicked() {
        final String editable = this.edtName.getText().toString();
        final String editable2 = this.edtPwd.getText().toString();
        final String editable3 = this.edtConfirm.getText().toString();
        if (editable == null) {
            setErrorText(this.edtName, "请输入正确的用户ID");
        } else if (editable2 == null || "".equalsIgnoreCase(editable2)) {
            setErrorText(this.edtPwd, "金额不能为空");
        } else {
            this.mCurActivity.showProgressBar("请稍候...");
            new Thread(new Runnable() {
                public void run() {
                    try {
                        TransferView.this.mCurActivity.closeActivity(EnablerInterface.getTransferResult(HttpPostCore.httpConnection(TransferView.this.mCurActivity, "http://218.200.227.123:95/sdkServer/1.0/pay/account/transfer", EnablerInterface.buildRequsetXml("<UID>" + TransferView.this.curExtraInfo.getString(Constants.UID) + "</UID><amount>" + editable2 + "</amount><RID>" + editable + "</RID><password>" + DES.encryptDES(editable3, TransferView.KEY) + "</password>"))));
                    } catch (Exception e) {
                        e.printStackTrace();
                        TransferView.this.mCurActivity.showToast("转账失败，请重试");
                    } finally {
                        TransferView.this.mCurActivity.hideProgressBar();
                    }
                }
            }).start();
        }
    }

    private void setErrorText(EditText editText, String str) {
        Spanned fromHtml = Html.fromHtml("<font color='blue'>" + str + "</font>");
        editText.requestFocus();
        editText.setError(fromHtml);
    }

    private int dip2px(float f) {
        return (int) ((this.mCurActivity.getResources().getDisplayMetrics().density * f) + 0.5f);
    }

    /* access modifiers changed from: protected */
    public int getScreenHeightPx() {
        int i = (int) ((((float) this.mCurActivity.getResources().getDisplayMetrics().heightPixels) * this.mCurActivity.getResources().getDisplayMetrics().density) + 0.5f);
        Log.d("getScreenHeightPx=", new StringBuilder().append(i).toString());
        return i;
    }

    /* access modifiers changed from: protected */
    public int getScreenHeightDip() {
        int i = this.mCurActivity.getResources().getDisplayMetrics().heightPixels;
        Log.d("getScreenHeightDip=", new StringBuilder().append(i).toString());
        return i;
    }
}
