package com.cmsc.cmmusic.common;

import android.content.Context;
import android.os.Bundle;
import com.cmsc.cmmusic.common.data.GetUserInfoRsp;
import com.cmsc.cmmusic.common.data.Result;
import com.cmsc.cmmusic.init.PreferenceUtil;
import com.cmsc.cmmusic.init.SmsLoginInfoRsp;
import java.io.IOException;
import java.io.InputStream;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

public class UserManagerInterface {
    public static GetUserInfoRsp getUserInfo(Context context) {
        try {
            return EnablerInterface.getUserInfo(context);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void openMember(Context context, CMMusicCallback<Result> cMMusicCallback) {
        CMMusicActivity.showActivityDefault(context, new Bundle(), 6, cMMusicCallback);
    }

    public static void smsAuthLogin(Context context, CMMusicCallback<Result> cMMusicCallback) {
        CMMusicActivity.showActivityDefault(context, new Bundle(), 8, cMMusicCallback);
    }

    public static SmsLoginInfoRsp smsAuthLoginValidate(Context context) {
        try {
            return getSmsLoginInfo(HttpPostCore.httpConnection(context, "http://218.200.227.123:95/sdkServer/1.0/crbt/smsAuthLoginValidate", EnablerInterface.buildRequsetXml("<token>" + PreferenceUtil.getToken(context) + "</token>")));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static SmsLoginInfoRsp getSmsLoginInfo(InputStream inputStream) throws XmlPullParserException, IOException {
        if (inputStream == null) {
            return null;
        }
        SmsLoginInfoRsp smsLoginInfoRsp = new SmsLoginInfoRsp();
        try {
            XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
            newPullParser.setInput(inputStream, "UTF-8");
            for (int eventType = newPullParser.getEventType(); 1 != eventType; eventType = newPullParser.next()) {
                String name = newPullParser.getName();
                switch (eventType) {
                    case 2:
                        if (!name.equalsIgnoreCase("resCode")) {
                            if (!name.equalsIgnoreCase("resMsg")) {
                                if (!name.equalsIgnoreCase("mobile")) {
                                    break;
                                } else {
                                    smsLoginInfoRsp.setMobile(newPullParser.nextText());
                                    break;
                                }
                            } else {
                                smsLoginInfoRsp.setResMsg(newPullParser.nextText());
                                break;
                            }
                        } else {
                            smsLoginInfoRsp.setResCode(newPullParser.nextText());
                            break;
                        }
                }
            }
            if (inputStream == null) {
                return smsLoginInfoRsp;
            }
            try {
                return smsLoginInfoRsp;
            } catch (IOException e) {
                return smsLoginInfoRsp;
            }
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e2) {
                }
            }
        }
    }
}
