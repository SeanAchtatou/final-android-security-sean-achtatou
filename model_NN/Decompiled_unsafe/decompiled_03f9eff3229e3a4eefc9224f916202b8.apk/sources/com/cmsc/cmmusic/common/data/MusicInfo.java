package com.cmsc.cmmusic.common.data;

public class MusicInfo {
    private String albumPicDir;
    private String count;
    private String crbtListenDir;
    private String crbtValidity;
    private String hasDolby;
    private String lrcDir;
    private String musicId;
    private String price;
    private String ringListenDir;
    private String ringValidity;
    private String singerId;
    private String singerName;
    private String singerPicDir;
    private String songListenDir;
    private String songName;
    private String songValidity;

    public String getHasDolby() {
        return this.hasDolby;
    }

    public void setHasDolby(String str) {
        this.hasDolby = str;
    }

    public String getRingValidity() {
        return this.ringValidity;
    }

    public void setRingValidity(String str) {
        this.ringValidity = str;
    }

    public String getSongValidity() {
        return this.songValidity;
    }

    public void setSongValidity(String str) {
        this.songValidity = str;
    }

    public String getAlbumPicDir() {
        return this.albumPicDir;
    }

    public void setAlbumPicDir(String str) {
        this.albumPicDir = str;
    }

    public String getSingerPicDir() {
        return this.singerPicDir;
    }

    public void setSingerPicDir(String str) {
        this.singerPicDir = str;
    }

    public String getCrbtListenDir() {
        return this.crbtListenDir;
    }

    public void setCrbtListenDir(String str) {
        this.crbtListenDir = str;
    }

    public String getRingListenDir() {
        return this.ringListenDir;
    }

    public void setRingListenDir(String str) {
        this.ringListenDir = str;
    }

    public String getSongListenDir() {
        return this.songListenDir;
    }

    public void setSongListenDir(String str) {
        this.songListenDir = str;
    }

    public String getLrcDir() {
        return this.lrcDir;
    }

    public void setLrcDir(String str) {
        this.lrcDir = str;
    }

    public String getSingerId() {
        return this.singerId;
    }

    public void setSingerId(String str) {
        this.singerId = str;
    }

    public String getMusicId() {
        return this.musicId;
    }

    public void setMusicId(String str) {
        this.musicId = str;
    }

    public String getCount() {
        return this.count;
    }

    public void setCount(String str) {
        this.count = str;
    }

    public String getCrbtValidity() {
        return this.crbtValidity;
    }

    public void setCrbtValidity(String str) {
        this.crbtValidity = str;
    }

    public String getPrice() {
        return this.price;
    }

    public void setPrice(String str) {
        this.price = str;
    }

    public String getSongName() {
        return this.songName;
    }

    public void setSongName(String str) {
        this.songName = str;
    }

    public String getSingerName() {
        return this.singerName;
    }

    public void setSingerName(String str) {
        this.singerName = str;
    }

    public String toString() {
        return "MusicInfo [musicId=" + this.musicId + ", count=" + this.count + ", crbtValidity=" + this.crbtValidity + ", price=" + this.price + ", songName=" + this.songName + ", singerId=" + this.singerId + ", singerName=" + this.singerName + ", ringValidity=" + this.ringValidity + ", songValidity=" + this.songValidity + ", albumPicDir=" + this.albumPicDir + ", singerPicDir=" + this.singerPicDir + ", crbtListenDir=" + this.crbtListenDir + ", ringListenDir=" + this.ringListenDir + ", songListenDir=" + this.songListenDir + ", lrcDir=" + this.lrcDir + ", hasDolby=" + this.hasDolby + "]";
    }
}
