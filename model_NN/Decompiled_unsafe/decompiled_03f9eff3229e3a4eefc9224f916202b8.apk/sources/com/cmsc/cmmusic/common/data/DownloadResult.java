package com.cmsc.cmmusic.common.data;

public class DownloadResult extends Result {
    private String downUrl;

    public DownloadResult() {
    }

    public DownloadResult(Result result) {
        if (result != null) {
            setResCode(result.getResCode());
            setResMsg(result.getResMsg());
        }
    }

    public String getDownUrl() {
        return this.downUrl;
    }

    public void setDownUrl(String str) {
        this.downUrl = str;
    }

    public String toString() {
        return "DownloadResult [downUrl=" + this.downUrl + ", getResCode()=" + getResCode() + ", getResMsg()=" + getResMsg() + "]";
    }
}
