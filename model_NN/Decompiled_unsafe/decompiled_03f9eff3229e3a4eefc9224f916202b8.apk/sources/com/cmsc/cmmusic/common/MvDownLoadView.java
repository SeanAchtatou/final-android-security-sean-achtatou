package com.cmsc.cmmusic.common;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.cmsc.cmmusic.common.data.BizInfo;
import com.cmsc.cmmusic.common.data.OrderPolicy;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

final class MvDownLoadView extends OrderView {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$cmsc$cmmusic$common$data$OrderPolicy$OrderType = null;
    private static final String LOG_TAG = "MvDownLoadView";
    private BizInfo binfo;
    Handler handler = new Handler(this.mCurActivity.getMainLooper()) {
        public void handleMessage(Message message) {
            super.handleMessage(message);
            MvDownLoadView.this.mCurActivity.hideProgressBar();
            MvDownLoadView.this.mCurActivity.setRequestedOrientation(0);
            PreferenceUtil.savePlayUri(MvDownLoadView.this.mCurActivity, (String) message.obj);
        }
    };
    private String hdPrice;
    private boolean isFree;
    private boolean isOpenMonth;
    private String mvId;
    private RadioGroup rdGroup;
    private String sdPrice;
    private TextView txtMonRemind;

    static /* synthetic */ int[] $SWITCH_TABLE$com$cmsc$cmmusic$common$data$OrderPolicy$OrderType() {
        int[] iArr = $SWITCH_TABLE$com$cmsc$cmmusic$common$data$OrderPolicy$OrderType;
        if (iArr == null) {
            iArr = new int[OrderPolicy.OrderType.values().length];
            try {
                iArr[OrderPolicy.OrderType.net.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[OrderPolicy.OrderType.sms.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[OrderPolicy.OrderType.verifyCode.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$com$cmsc$cmmusic$common$data$OrderPolicy$OrderType = iArr;
        }
        return iArr;
    }

    public MvDownLoadView(Context context, Bundle bundle) {
        super(context, bundle);
        this.mvId = bundle.getString("mvId");
        this.mCurActivity.getWindow().getDecorView().findViewById(16908290);
    }

    /* access modifiers changed from: protected */
    public void updateNetView() {
        this.curSongName = "歌   曲:   " + this.policyObj.getMvInfo().getMvName();
        this.curSingerName = "歌   手:   " + this.policyObj.getMvInfo().getSingerName();
        ArrayList<BizInfo> bizInfos = this.policyObj.getBizInfos();
        if (bizInfos != null) {
            HashMap hashMap = new HashMap();
            Iterator<BizInfo> it = bizInfos.iterator();
            while (it.hasNext()) {
                BizInfo next = it.next();
                if (next != null) {
                    String bizType = next.getBizType();
                    Logger.i("TAG", "bizType = " + bizType);
                    if ("20".equalsIgnoreCase(bizType)) {
                        if (!hashMap.containsKey("20")) {
                            hashMap.put("20", next);
                        }
                        String resource = next.getResource();
                        if ("050014".equals(resource)) {
                            this.sdPrice = String.valueOf(EnablerInterface.getPriceString(next.getSalePrice())) + "  ";
                        } else if ("050013".equals(resource)) {
                            this.hdPrice = String.valueOf(EnablerInterface.getPriceString(next.getSalePrice())) + "  ";
                        }
                        this.isFree = true;
                        this.isOpenMonth = true;
                    } else if (!Constants.VIA_REPORT_TYPE_QQFAVORITES.equalsIgnoreCase(bizType) && !Constants.VIA_REPORT_TYPE_DATALINE.equalsIgnoreCase(bizType)) {
                        if (Constants.VIA_REPORT_TYPE_SHARE_TO_QZONE.equalsIgnoreCase(bizType)) {
                            if (!hashMap.containsKey(Constants.VIA_REPORT_TYPE_SHARE_TO_QZONE)) {
                                hashMap.put(Constants.VIA_REPORT_TYPE_SHARE_TO_QZONE, next);
                            }
                            String resource2 = next.getResource();
                            if ("050014".equals(resource2)) {
                                this.sdPrice = String.valueOf(EnablerInterface.getPriceString(next.getSalePrice())) + "  ";
                            } else if ("050013".equals(resource2)) {
                                this.hdPrice = String.valueOf(EnablerInterface.getPriceString(next.getSalePrice())) + "  ";
                            }
                        } else if ("00".equalsIgnoreCase(bizType)) {
                            if (!hashMap.containsKey("00")) {
                                hashMap.put("00", next);
                            }
                            String resource3 = next.getResource();
                            if ("050014".equals(resource3)) {
                                this.sdPrice = String.valueOf(EnablerInterface.getPriceString(next.getSalePrice())) + "  ";
                            } else if ("050013".equals(resource3)) {
                                this.hdPrice = String.valueOf(EnablerInterface.getPriceString(next.getSalePrice())) + "  ";
                            }
                            this.isFree = true;
                        } else if ("30".equalsIgnoreCase(bizType)) {
                            if (!hashMap.containsKey("30")) {
                                hashMap.put("30", next);
                            }
                            String salePrice = next.getSalePrice();
                            if (salePrice != null && salePrice.trim().length() > 0) {
                                String resource4 = next.getResource();
                                if ("050014".equals(resource4)) {
                                    this.sdPrice = String.valueOf(EnablerInterface.getPriceString(next.getSalePrice())) + "  ";
                                } else if ("050013".equals(resource4)) {
                                    this.hdPrice = String.valueOf(EnablerInterface.getPriceString(next.getSalePrice())) + "  ";
                                }
                            }
                        }
                    }
                }
            }
            Iterator it2 = hashMap.entrySet().iterator();
            while (true) {
                if (it2.hasNext()) {
                    Map.Entry entry = (Map.Entry) it2.next();
                    if ("00".equals((String) entry.getKey())) {
                        this.binfo = (BizInfo) entry.getValue();
                        break;
                    }
                } else {
                    break;
                }
            }
            if (this.binfo == null) {
                Iterator it3 = hashMap.entrySet().iterator();
                while (true) {
                    if (it3.hasNext()) {
                        Map.Entry entry2 = (Map.Entry) it3.next();
                        if ("20".equals((String) entry2.getKey())) {
                            this.binfo = (BizInfo) entry2.getValue();
                            break;
                        }
                    } else {
                        break;
                    }
                }
            }
            if (this.binfo == null) {
                Iterator it4 = hashMap.entrySet().iterator();
                while (true) {
                    if (it4.hasNext()) {
                        Map.Entry entry3 = (Map.Entry) it4.next();
                        if (Constants.VIA_REPORT_TYPE_SHARE_TO_QZONE.equals((String) entry3.getKey())) {
                            this.binfo = (BizInfo) entry3.getValue();
                            break;
                        }
                    } else {
                        break;
                    }
                }
            }
            if (this.binfo == null) {
                Iterator it5 = hashMap.entrySet().iterator();
                while (true) {
                    if (it5.hasNext()) {
                        Map.Entry entry4 = (Map.Entry) it5.next();
                        if ("30".equals((String) entry4.getKey())) {
                            this.binfo = (BizInfo) entry4.getValue();
                            break;
                        }
                    } else {
                        break;
                    }
                }
            }
        }
        if (this.sdPrice != null && this.sdPrice.trim().length() > 0 && !this.sdPrice.equals("0") && !this.isFree) {
            ((RadioButton) this.rdGroup.findViewById(100)).setText("流畅版（300kbps/480*272）/" + this.sdPrice);
        } else if (this.sdPrice != null) {
            if (this.isFree) {
                this.sdPrice = "0";
            }
            ((RadioButton) this.rdGroup.findViewById(100)).setText("流畅版（300kbps/480*272）/" + EnablerInterface.getPriceString(this.sdPrice));
        } else {
            ((RadioButton) this.rdGroup.findViewById(100)).setVisibility(8);
        }
        if (this.hdPrice != null && this.hdPrice.trim().length() > 0 && !this.hdPrice.equals("0") && !this.isFree) {
            ((RadioButton) this.rdGroup.findViewById(cn.banshenggua.aichang.utils.Constants.RESULT_OK)).setText("高清版（700kbps/640*352）/" + this.hdPrice);
        } else if (this.hdPrice != null) {
            if (this.isFree) {
                this.hdPrice = "0";
            }
            ((RadioButton) this.rdGroup.findViewById(cn.banshenggua.aichang.utils.Constants.RESULT_OK)).setText("高清版（700kbps/640*352）/" + EnablerInterface.getPriceString(this.hdPrice));
        } else {
            ((RadioButton) this.rdGroup.findViewById(cn.banshenggua.aichang.utils.Constants.RESULT_OK)).setVisibility(8);
        }
        if (this.isOpenMonth) {
            setUserTip("您已成功开通MV点播包月，点击“确认”直接在线浏览歌曲MV");
            Logger.i("TAG", "restTimes = " + this.policyObj.getRestTimes());
            this.txtMonRemind.setText("温馨提示：包月内点播不收取费用；通过手机点播歌曲MV产生的流量费，按当地运营商资费标准收取");
            this.txtMonRemind.setVisibility(0);
            return;
        }
        setUserTip("点击“确认”直接在线浏览歌曲MV");
        Logger.i("TAG", "restTimes = " + this.policyObj.getRestTimes());
        this.txtMonRemind.setText("温馨提示:点播不成功不收取费用；通过手机点播歌曲MV产生的流量费，按当地运营商资费标准收取");
        this.txtMonRemind.setVisibility(0);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    public void sureClicked() {
        Log.d(LOG_TAG, "sure button clicked");
        try {
            switch ($SWITCH_TABLE$com$cmsc$cmmusic$common$data$OrderPolicy$OrderType()[this.orderType.ordinal()]) {
                case 1:
                    Log.d(LOG_TAG, "Get download url by net");
                    if (this.binfo != null) {
                        String bizCode = this.binfo.getBizCode();
                        String bizType = this.binfo.getBizType();
                        String str = "";
                        switch (this.rdGroup.getCheckedRadioButtonId()) {
                            case 100:
                                str = "050014";
                                break;
                            case cn.banshenggua.aichang.utils.Constants.RESULT_OK:
                                str = "050013";
                                break;
                        }
                        Log.d(LOG_TAG, "get url by net . bizCode : " + bizCode + " , bizType : " + bizType);
                        MiguSdkUtil.buildCpparam(this.mCurActivity, "http://218.200.227.123:95/sdkServer/1.0/mv/downlink", EnablerInterface.buildRequsetXml("<mvId>" + this.mvId + "</mvId><bizCode>" + bizCode + "</bizCode><biztype>" + bizType + "</biztype><codeRate>" + str + "</codeRate>"));
                        return;
                    }
                    return;
                case 2:
                    Log.d(LOG_TAG, "Get download url by sms");
                    this.mCurActivity.closeActivity(null);
                    return;
                default:
                    return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        e.printStackTrace();
    }

    /* access modifiers changed from: protected */
    public void initContentView(LinearLayout linearLayout) {
        this.rdGroup = new RadioGroup(this.mCurActivity);
        RadioButton radioButton = new RadioButton(this.mCurActivity);
        radioButton.setText("流畅版（300kbps/480*272）");
        radioButton.setId(100);
        radioButton.setChecked(true);
        RadioButton radioButton2 = new RadioButton(this.mCurActivity);
        radioButton2.setText("高清版（700kbps/640*352）");
        radioButton2.setId(cn.banshenggua.aichang.utils.Constants.RESULT_OK);
        this.rdGroup.addView(radioButton);
        this.rdGroup.addView(radioButton2);
        linearLayout.addView(this.rdGroup);
        linearLayout.addView(getMemInfoView());
        this.txtMonRemind = new TextView(this.mCurActivity);
        this.txtMonRemind.setTextAppearance(this.mCurActivity, 16973892);
        this.txtMonRemind.setPadding(0, 10, 10, 0);
        this.txtMonRemind.setVisibility(8);
        linearLayout.addView(this.txtMonRemind);
    }
}
