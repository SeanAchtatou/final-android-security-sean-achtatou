package com.cmsc.cmmusic.common.data;

public class TagInfo {
    private String description;
    private String hasChild;
    private String imgUrl;
    private String name;
    private String tagId;

    public String getTagId() {
        return this.tagId;
    }

    public void setTagId(String str) {
        this.tagId = str;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String str) {
        this.name = str;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String str) {
        this.description = str;
    }

    public String getImgUrl() {
        return this.imgUrl;
    }

    public void setImgUrl(String str) {
        this.imgUrl = str;
    }

    public String getHasChild() {
        return this.hasChild;
    }

    public void setHasChild(String str) {
        this.hasChild = str;
    }

    public String toString() {
        return "TagInfo [tagId=" + this.tagId + ", name=" + this.name + ", description=" + this.description + ", imgUrl=" + this.imgUrl + ", hasChild=" + this.hasChild + "]";
    }
}
