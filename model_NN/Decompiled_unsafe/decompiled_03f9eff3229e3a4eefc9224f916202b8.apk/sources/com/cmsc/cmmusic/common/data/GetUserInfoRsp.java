package com.cmsc.cmmusic.common.data;

public class GetUserInfoRsp extends Result {
    public static final String NON_MEM_ERROR_CODE = "301001";
    private UserInfo userInfo;

    public UserInfo getUserInfo() {
        return this.userInfo;
    }

    public void setUserInfo(UserInfo userInfo2) {
        this.userInfo = userInfo2;
    }

    public String toString() {
        return "UserInfoResult [userInfo=" + this.userInfo + ", getResCode()=" + getResCode() + ", getResMsg()=" + getResMsg() + "]";
    }
}
