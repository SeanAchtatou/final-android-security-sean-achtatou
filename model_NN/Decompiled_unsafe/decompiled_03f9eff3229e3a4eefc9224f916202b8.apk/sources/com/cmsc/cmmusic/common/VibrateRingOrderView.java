package com.cmsc.cmmusic.common;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import cn.banshenggua.aichang.utils.Constants;
import com.cmsc.cmmusic.common.data.BizInfo;
import com.cmsc.cmmusic.common.data.OrderResult;

final class VibrateRingOrderView extends DownloadSongView {
    private static final String LOG_TAG = "VibrateRingOrderView";

    public VibrateRingOrderView(Context context, Bundle bundle) {
        super(context, bundle);
    }

    /* access modifiers changed from: protected */
    public void showOrderPolicyView() {
        setUserTip("点击“确认”直接下载来电铃声至手机");
        setupMonTip("您已开通振铃包月下载功能，本月还可以免费下载");
        setupRadioButton(100, "000018", "标清版（40kbps）");
        setupRadioButton(Constants.RESULT_OK, "999992", "高清版（128kbps）");
    }

    /* access modifiers changed from: protected */
    public void downloadSong(BizInfo bizInfo, String str) {
        String bizCode = bizInfo.getBizCode();
        String bizType = bizInfo.getBizType();
        Log.d(LOG_TAG, "get url by net . bizCode : " + bizCode + " , bizType : " + bizType);
        EnablerInterface.getVibrateRingDownloadUrl(this.mCurActivity, this.curExtraInfo.getString("MusicId"), bizCode, bizType, str, bizInfo.getSalePrice(), this.policyObj.getMonLevel(), bizInfo.getHold2(), new CMMusicCallback<OrderResult>() {
            public void operationResult(OrderResult orderResult) {
                VibrateRingOrderView.this.mCurActivity.closeActivity(orderResult);
            }
        });
    }
}
