package com.cmsc.cmmusic.common;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import com.cmsc.cmmusic.common.MiguSdkUtil;
import com.cmsc.cmmusic.common.data.CrbtListRsp;
import com.cmsc.cmmusic.common.data.CrbtOpenCheckRsp;
import com.cmsc.cmmusic.common.data.CrbtPrelistenRsp;
import com.cmsc.cmmusic.common.data.OrderResult;
import com.cmsc.cmmusic.common.data.OwnRingRsp;
import com.cmsc.cmmusic.common.data.Result;
import com.cmsc.cmmusic.common.data.ToneInfo;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

public class RingbackManagerInterface {
    public static void buyOwnRingback(Context context, CMMusicCallback<Result> cMMusicCallback) {
        CMMusicActivity.showActivityDefault(context, new Bundle(), 16, cMMusicCallback);
    }

    public static void orderOwnRingback(Context context, String str, CMMusicCallback<OrderResult> cMMusicCallback) {
        MiguSdkUtil.TL.init();
        EnablerInterface.orderOwnRingback(context, str, cMMusicCallback);
    }

    public static OwnRingRsp createTextOwnRingback(Context context, String str, String str2, String str3, String str4) {
        try {
            return EnablerInterface.getTextOwnRingRsp(context, str, str2, str3, str4);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (XmlPullParserException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public static OwnRingRsp uploadOwnRingback(Context context, File file, String str, int i) {
        try {
            return EnablerInterface.getMp3OwnRingRsp(context, file, str, i);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (XmlPullParserException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public static Result isOwnRingMonthUser(Context context) {
        try {
            return EnablerInterface.getIsOwnRingOrderMonthUserRsp(context);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (XmlPullParserException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public static void keyOrderOwnRingMonth(Context context, CMMusicCallback<Result> cMMusicCallback) {
        CMMusicActivity.showActivityDefault(context, new Bundle(), 20, cMMusicCallback);
    }

    public static void keyOrderOwnRingMonth(Context context, String str, CMMusicCallback<OrderResult> cMMusicCallback) {
        MiguSdkUtil.TL.init();
        EnablerInterface.KeyOrderOwnRingMonth(context, str, cMMusicCallback);
    }

    public static void ownRingMonth(Context context, CMMusicCallback<Result> cMMusicCallback) {
        CMMusicActivity.showActivityDefault(context, new Bundle(), 21, cMMusicCallback);
    }

    public static void ownRingMonth(Context context, String str, CMMusicCallback<OrderResult> cMMusicCallback) {
        MiguSdkUtil.TL.init();
        EnablerInterface.orderOwnRingMonth(context, str, cMMusicCallback);
    }

    public static void buyRingBack(Context context, String str, CMMusicCallback<OrderResult> cMMusicCallback) {
        Bundle bundle = new Bundle();
        bundle.putString("MusicId", str);
        CMMusicActivity.showActivityDefault(context, bundle, 2, cMMusicCallback);
    }

    static void buyRingBackByOpenMember(Context context, String str, CMMusicCallback<OrderResult> cMMusicCallback) {
        Bundle bundle = new Bundle();
        bundle.putString("MusicId", str);
        CMMusicActivity.showActivityDefault(context, bundle, 22, cMMusicCallback);
    }

    static void buyRingbackByOpenMember(Context context, String str, String str2, String str3, CMMusicCallback<OrderResult> cMMusicCallback) {
        MiguSdkUtil.TL.init();
        EnablerInterface.buyRingBackByOpenMember(context, str, str2, str3, cMMusicCallback);
    }

    static void buyRingBackByOpenRingBack(Context context, String str, CMMusicCallback<OrderResult> cMMusicCallback) {
        Bundle bundle = new Bundle();
        bundle.putString("MusicId", str);
        CMMusicActivity.showActivityDefault(context, bundle, 23, cMMusicCallback);
    }

    static void buyRingbackByOpenRingBack(Context context, String str, String str2, String str3, CMMusicCallback<OrderResult> cMMusicCallback) {
        MiguSdkUtil.TL.init();
        EnablerInterface.buyRingBackByOpenRingBack(context, str, str2, str3, cMMusicCallback);
    }

    public static String getRingbackPolicy(Context context, String str) {
        try {
            return HttpPostCore.httpConnectionToString(context, "http://218.200.227.123:95/sdkServer/1.0/crbt/policy", EnablerInterface.buildRequsetXml("<musicId>" + str + "</musicId>"));
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void buyRingback(Context context, String str, String str2, String str3, String str4, String str5, String str6, CMMusicCallback<OrderResult> cMMusicCallback) {
        MiguSdkUtil.TL.init();
        EnablerInterface.buyRingback(context, str, str2, str3, str4, str5, str6, cMMusicCallback);
    }

    public static void openRingback(Context context, CMMusicCallback<Result> cMMusicCallback) {
        CMMusicActivity.showActivityDefault(context, new Bundle(), 4, cMMusicCallback);
    }

    public static void giveRingBack(Context context, String str, CMMusicCallback<OrderResult> cMMusicCallback) {
        Bundle bundle = new Bundle();
        bundle.putString("MusicId", str);
        CMMusicActivity.showActivityDefault(context, bundle, 5, cMMusicCallback);
    }

    public static void giveRingBack(Context context, String str, String str2, CMMusicCallback<OrderResult> cMMusicCallback) {
        Bundle bundle = new Bundle();
        bundle.putString("MusicId", str);
        bundle.putString("PhoneNum", str2);
        CMMusicActivity.showActivityDefault(context, bundle, 5, cMMusicCallback);
    }

    public static void giveRingbackByCustom(Context context, String str, String str2, String str3, String str4, String str5, String str6, String str7, CMMusicCallback<OrderResult> cMMusicCallback) {
        MiguSdkUtil.TL.init();
        EnablerInterface.giveRingback(context, str, str2, str3, str4, str5, str6, str7, cMMusicCallback);
    }

    public static Result deletePersonRing(Context context, String str) {
        Log.d("RingbackManagerInterface", "id : " + str);
        try {
            return EnablerInterface.deletePersonRingByNet(context, str);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (XmlPullParserException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public static CrbtOpenCheckRsp crbtOpenCheck(Context context, String str) {
        try {
            return EnablerInterface.crbtOpenCheck(context, str);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static CrbtPrelistenRsp getCrbtPrelisten(Context context, String str) {
        try {
            return getCrbtPrelisten(HttpPostCore.httpConnection(context, "http://218.200.227.123:95/sdkServer/1.0/crbt/prelisten", EnablerInterface.buildRequsetXml("<musicId>" + str + "</musicId>")));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static CrbtListRsp getCrbtBox(Context context) {
        try {
            return getCrbtBox(HttpPostCore.httpConnection(context, "http://218.200.227.123:95/sdkServer/1.0/crbt/box/query", ""));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Result setDefaultCrbt(Context context, String str) {
        try {
            return EnablerInterface.getResult(HttpPostCore.httpConnection(context, "http://218.200.227.123:95/sdkServer/1.0/crbt/box/default", EnablerInterface.buildRequsetXml("<crbtId>" + str + "</crbtId>")));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static CrbtListRsp getDefaultCrbt(Context context, String str) {
        try {
            return getCrbtBox(HttpPostCore.httpConnection(context, "http://218.200.227.123:95/sdkServer/1.0/crbt/msisdn/query", EnablerInterface.buildRequsetXml("<MSISDN>" + str + "</MSISDN>")));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Result getCrbtAskFor(Context context, String str, String str2, String str3) {
        try {
            return EnablerInterface.getResult(HttpPostCore.httpConnection(context, "http://218.200.227.123:95/sdkServer/1.0/crbt/askfor", EnablerInterface.buildRequsetXml("<receivemdn>" + str + "</receivemdn><musicId>" + str2 + "</musicId><validCode>" + str3 + "</validCode>")));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static CrbtListRsp getCrbtBox(InputStream inputStream) throws IOException, XmlPullParserException {
        ToneInfo toneInfo;
        ArrayList arrayList;
        ArrayList arrayList2 = null;
        if (inputStream == null) {
            return null;
        }
        CrbtListRsp crbtListRsp = new CrbtListRsp();
        try {
            XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
            newPullParser.setInput(inputStream, "UTF-8");
            ToneInfo toneInfo2 = null;
            for (int eventType = newPullParser.getEventType(); 1 != eventType; eventType = newPullParser.next()) {
                String name = newPullParser.getName();
                switch (eventType) {
                    case 0:
                        toneInfo = toneInfo2;
                        arrayList = arrayList2;
                        continue;
                        arrayList2 = arrayList;
                        toneInfo2 = toneInfo;
                    case 2:
                        if (name.equalsIgnoreCase("resCode")) {
                            crbtListRsp.setResCode(newPullParser.nextText());
                            toneInfo = toneInfo2;
                            arrayList = arrayList2;
                            continue;
                        } else if (name.equalsIgnoreCase("resMsg")) {
                            crbtListRsp.setResMsg(newPullParser.nextText());
                            toneInfo = toneInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("ToneInfo")) {
                            toneInfo = new ToneInfo();
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("toneID")) {
                            toneInfo2.setToneID(newPullParser.nextText());
                            toneInfo = toneInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("toneName")) {
                            toneInfo2.setToneName(newPullParser.nextText());
                            toneInfo = toneInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("toneNameLetter")) {
                            toneInfo2.setToneNameLetter(newPullParser.nextText());
                            toneInfo = toneInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("singerName")) {
                            toneInfo2.setSingerName(newPullParser.nextText());
                            toneInfo = toneInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("singerNameLetter")) {
                            toneInfo2.setSingerNameLetter(newPullParser.nextText());
                            toneInfo = toneInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("price")) {
                            toneInfo2.setPrice(newPullParser.nextText());
                            toneInfo = toneInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("toneValidDay")) {
                            toneInfo2.setToneValidDay(newPullParser.nextText());
                            toneInfo = toneInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("info")) {
                            toneInfo2.setInfo(newPullParser.nextText());
                            toneInfo = toneInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("tonePreListenAddress")) {
                            toneInfo2.setTonePreListenAddress(newPullParser.nextText());
                            toneInfo = toneInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("toneType")) {
                            toneInfo2.setToneType(newPullParser.nextText());
                            toneInfo = toneInfo2;
                            arrayList = arrayList2;
                        }
                        arrayList2 = arrayList;
                        toneInfo2 = toneInfo;
                        break;
                    case 3:
                        if (name.equalsIgnoreCase("ToneInfo")) {
                            if (arrayList2 == null) {
                                arrayList2 = new ArrayList();
                                crbtListRsp.setToneInfos(arrayList2);
                            }
                            arrayList2.add(toneInfo2);
                            break;
                        }
                        break;
                }
                toneInfo = toneInfo2;
                arrayList = arrayList2;
                arrayList2 = arrayList;
                toneInfo2 = toneInfo;
            }
            return crbtListRsp;
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                }
            }
        }
    }

    private static CrbtPrelistenRsp getCrbtPrelisten(InputStream inputStream) throws IOException, XmlPullParserException {
        if (inputStream == null) {
            return null;
        }
        CrbtPrelistenRsp crbtPrelistenRsp = new CrbtPrelistenRsp();
        try {
            XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
            newPullParser.setInput(inputStream, "UTF-8");
            for (int eventType = newPullParser.getEventType(); 1 != eventType; eventType = newPullParser.next()) {
                String name = newPullParser.getName();
                switch (eventType) {
                    case 2:
                        if (!name.equalsIgnoreCase("resCode")) {
                            if (!name.equalsIgnoreCase("resMsg")) {
                                if (!name.equalsIgnoreCase("streamUrl")) {
                                    if (!name.equalsIgnoreCase("price")) {
                                        if (!name.equalsIgnoreCase("invalidDate")) {
                                            if (!name.equalsIgnoreCase("mobile")) {
                                                break;
                                            } else {
                                                crbtPrelistenRsp.setMobile(newPullParser.nextText());
                                                break;
                                            }
                                        } else {
                                            crbtPrelistenRsp.setInvalidDate(newPullParser.nextText());
                                            break;
                                        }
                                    } else {
                                        crbtPrelistenRsp.setPrice(newPullParser.nextText());
                                        break;
                                    }
                                } else {
                                    crbtPrelistenRsp.setStreamUrl(newPullParser.nextText());
                                    break;
                                }
                            } else {
                                crbtPrelistenRsp.setResMsg(newPullParser.nextText());
                                break;
                            }
                        } else {
                            crbtPrelistenRsp.setResCode(newPullParser.nextText());
                            break;
                        }
                }
            }
            if (inputStream == null) {
                return crbtPrelistenRsp;
            }
            try {
                return crbtPrelistenRsp;
            } catch (IOException e) {
                return crbtPrelistenRsp;
            }
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e2) {
                }
            }
        }
    }
}
