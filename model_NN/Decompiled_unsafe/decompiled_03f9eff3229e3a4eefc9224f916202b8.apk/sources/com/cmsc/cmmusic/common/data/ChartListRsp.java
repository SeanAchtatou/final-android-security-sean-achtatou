package com.cmsc.cmmusic.common.data;

import java.util.List;

public class ChartListRsp extends Result {
    private List<ChartInfo> chartInfos;
    private String resCounter;

    public String getResCounter() {
        return this.resCounter;
    }

    public void setResCounter(String str) {
        this.resCounter = str;
    }

    public List<ChartInfo> getChartInfos() {
        return this.chartInfos;
    }

    public void setChartInfos(List<ChartInfo> list) {
        this.chartInfos = list;
    }

    public String toString() {
        return "ChartListRsp [resCounter=" + this.resCounter + ", chartInfos=" + this.chartInfos + ", getResCode()=" + getResCode() + ", getResMsg()=" + getResMsg() + "]";
    }
}
