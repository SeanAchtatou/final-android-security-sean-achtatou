package com.cmsc.cmmusic.common;

import android.content.Context;
import android.os.Bundle;
import com.cmsc.cmmusic.common.MiguSdkUtil;
import com.cmsc.cmmusic.common.data.OrderResult;
import java.io.IOException;

public class DigitalAlbumManagerInterface {
    public static String getDigitalAlbumInfo(Context context, String str) {
        try {
            return HttpPostCore.httpConnectionToString(context, "http://218.200.227.123:95/sdkServer/1.0/album/orderCount", EnablerInterface.buildGetDigitalAlbumInfoRequsetXml(str));
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void orderDigitalAlbum(Context context, String str, CMMusicCallback<OrderResult> cMMusicCallback) {
        Bundle bundle = new Bundle();
        bundle.putString("MusicId", str);
        CMMusicActivity.showActivityDefault(context, bundle, 24, cMMusicCallback);
    }

    public static void bugDigitalAlbum(Context context, String str, String str2, String str3, String str4, String str5, CMMusicCallback<OrderResult> cMMusicCallback) {
        MiguSdkUtil.TL.init();
        EnablerInterface.orderDigitalAlbum(context, str, str2, str3, str4, str5, cMMusicCallback);
    }

    public static void giveDigitalAlbum(Context context, String str, CMMusicCallback<OrderResult> cMMusicCallback) {
        Bundle bundle = new Bundle();
        bundle.putString("MusicId", str);
        CMMusicActivity.showActivityDefault(context, bundle, 25, cMMusicCallback);
    }

    public static void giveDigitalAlbum(Context context, String str, String str2, String str3, String str4, String str5, String str6, CMMusicCallback<OrderResult> cMMusicCallback) {
        MiguSdkUtil.TL.init();
        EnablerInterface.giveDigitalAlbum(context, str, str2, str3, str4, str5, str6, cMMusicCallback);
    }

    static void orderDigitalAlbumByOpenMember(Context context, String str, CMMusicCallback<OrderResult> cMMusicCallback) {
        Bundle bundle = new Bundle();
        bundle.putString("MusicId", str);
        CMMusicActivity.showActivityDefault(context, bundle, 27, cMMusicCallback);
    }

    static void bugDigitalAlbumByOpenMember(Context context, String str, CMMusicCallback<OrderResult> cMMusicCallback) {
        MiguSdkUtil.TL.init();
        EnablerInterface.orderDigitalAlbumByOpenMember(context, str, cMMusicCallback);
    }

    static void orderDigitalAlbumByOpenRingBack(Context context, String str, CMMusicCallback<OrderResult> cMMusicCallback) {
        Bundle bundle = new Bundle();
        bundle.putString("MusicId", str);
        CMMusicActivity.showActivityDefault(context, bundle, 26, cMMusicCallback);
    }

    static void orderDigitalAlbumOpenRingBack(Context context, String str, CMMusicCallback<OrderResult> cMMusicCallback) {
        MiguSdkUtil.TL.init();
        EnablerInterface.orderDigitalAlbumByOpenRingBack(context, str, cMMusicCallback);
    }
}
