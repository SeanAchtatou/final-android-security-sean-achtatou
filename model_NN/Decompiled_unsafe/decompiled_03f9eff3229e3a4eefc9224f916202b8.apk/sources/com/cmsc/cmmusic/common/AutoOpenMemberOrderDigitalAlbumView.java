package com.cmsc.cmmusic.common;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import com.cmsc.cmmusic.common.data.OrderResult;

public class AutoOpenMemberOrderDigitalAlbumView extends DigitalAlbumOrderView {
    public AutoOpenMemberOrderDigitalAlbumView(Context context, Bundle bundle) {
        super(context, bundle);
    }

    /* access modifiers changed from: protected */
    public void sureClicked() {
        Log.d(getClass().getName(), "sure button clicked");
        if (getPhonePayTypeBizInfo() != null) {
            EnablerInterface.orderDigitalAlbumByOpenMember(this.mCurActivity, this.curExtraInfo.getString("MusicId"), new CMMusicCallback<OrderResult>() {
                public void operationResult(OrderResult orderResult) {
                    AutoOpenMemberOrderDigitalAlbumView.this.mCurActivity.closeActivity(orderResult);
                }
            });
        }
    }
}
