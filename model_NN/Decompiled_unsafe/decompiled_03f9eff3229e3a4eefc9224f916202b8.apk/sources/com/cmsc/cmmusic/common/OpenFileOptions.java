package com.cmsc.cmmusic.common;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.NinePatchDrawable;
import android.graphics.drawable.StateListDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;
import com.baidu.mobads.CpuInfoManager;
import java.io.File;
import java.io.FileFilter;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class OpenFileOptions extends Dialog {
    private static final String CMMUSIC = "";
    protected static final String TAG = "OpenFileOptions";
    private static final int btn_close = 37;
    private static final int btn_height = 60;
    private static final int btn_width = 100;
    private static final int file_list = 36;
    private static final int file_name_url = 35;
    /* access modifiers changed from: private */
    public OpenFileAdapter mAdapter;
    /* access modifiers changed from: private */
    public boolean mAddTextViewFlag = false;
    private WindowManager.LayoutParams mBgLayoutParams;
    private LinearLayout mBgLinearLayout;
    /* access modifiers changed from: private */
    public ImageView mBitmapImgView;
    private HorizontalScrollView mButtonScrollView;
    /* access modifiers changed from: private */
    public LinearLayout mButtonsLayout;
    private File mDefaultPath;
    /* access modifiers changed from: private */
    public DissmissReturnListener mDissmissReturnListener;
    /* access modifiers changed from: private */
    public File mFile;
    /* access modifiers changed from: private */
    public List<File> mFileList = new ArrayList();
    private FrameLayout mFileListFrameLayout;
    private LinearLayout mFileListLayout;
    private FileOptionsLayout mFileOptionsLayout;
    private String mFileType;
    private FrameLayout mFileUrlFrameLayout;
    /* access modifiers changed from: private */
    public Handler mHandler = new Handler() {
        public void handleMessage(Message message) {
            if (!OpenFileOptions.this.mShowProgressDialogFlag) {
                OpenFileOptions.this.showProgressDialog();
            } else {
                OpenFileOptions.this.mShowProgressDialogFlag = false;
            }
        }
    };
    private ArrayList<File> mHasHandWritingPicList = null;
    private boolean mIsAddviewFlag = false;
    /* access modifiers changed from: private */
    public boolean mIsAnimtionFlag = false;
    private boolean mIsWindowOpenFlag = false;
    /* access modifiers changed from: private */
    public File mLastFile = null;
    private WindowManager.LayoutParams mLayoutParams;
    /* access modifiers changed from: private */
    public Bitmap mLocalBitmap;
    private File mOfficePath;
    /* access modifiers changed from: private */
    public ListView mOpenFileListView;
    private Animation mOptionAnimation;
    private boolean mPicToPdfFlag = false;
    /* access modifiers changed from: private */
    public ProgressDialog mProgressDialog;
    private OpenFileOptionsRefreshTask mRefreshTask = null;
    /* access modifiers changed from: private */
    public String mSdcardPath = "";
    /* access modifiers changed from: private */
    public boolean mShowProgressDialogFlag = false;
    private Toast mToast;
    private Button mToolBarButton;
    /* access modifiers changed from: private */
    public TranslateAnimation mTransAnimLeftIn;
    /* access modifiers changed from: private */
    public TranslateAnimation mTransAnimLeftOut;
    /* access modifiers changed from: private */
    public TranslateAnimation mTransAnimRightIn;
    /* access modifiers changed from: private */
    public TranslateAnimation mTransAnimRightOut;
    private ViewGroup viewParent;
    private int windowWidth = 0;

    public interface DissmissReturnListener {
        void backKey();

        void dissMigrate();

        void migrate();

        void openFile(String str);
    }

    public OpenFileOptions(Context context, String str, String str2) {
        super(context);
        this.mFileType = str;
        if (FilePath.isCanUseSdCard()) {
            this.mSdcardPath = Environment.getExternalStorageDirectory().getPath();
            Log.i("TAG", "mSdcardPath = " + this.mSdcardPath);
            this.mBgLinearLayout = new LinearLayout(context);
            this.mBgLinearLayout.setBackgroundColor(0);
            this.viewParent = getViewGroup();
            ((Button) this.viewParent.findViewById(37)).setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    OpenFileOptions.this.dissmissFileOptions();
                }
            });
            this.mOpenFileListView = new ListView(context);
            this.mOpenFileListView.setLayoutParams(new WindowManager.LayoutParams(-1, -1));
            this.mOpenFileListView.setCacheColorHint(0);
            this.mOpenFileListView.setDividerHeight(1);
            this.mOpenFileListView.setChoiceMode(0);
            this.mToolBarButton = new Button(context);
            this.mToolBarButton.setLayoutParams(new LinearLayout.LayoutParams(-2, -1));
            this.mToolBarButton.setPadding(5, 5, 5, 5);
            this.mToolBarButton.setGravity(17);
            this.mToolBarButton.setMinWidth(100);
            this.mButtonScrollView = new HorizontalScrollView(context);
            this.mButtonScrollView.setLayoutParams(new WindowManager.LayoutParams(-1, -1));
            this.mButtonScrollView.setPadding(5, 0, 5, 0);
            this.mButtonScrollView.setHorizontalScrollBarEnabled(false);
            this.mToolBarButton.setVerticalScrollBarEnabled(false);
            this.windowWidth = ((Activity) context).getWindowManager().getDefaultDisplay().getWidth();
            this.mButtonsLayout = new LinearLayout(context);
            this.mButtonsLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, 55));
            File file = new File(this.mSdcardPath);
            this.mShowProgressDialogFlag = true;
            this.mOfficePath = new File(new StringBuilder(String.valueOf(this.mSdcardPath)).toString());
            this.mDefaultPath = new File(this.mSdcardPath);
            if (!this.mDefaultPath.exists()) {
                this.mDefaultPath.mkdirs();
            }
            this.mFile = file;
            this.mToolBarButton.setText(file.getName());
            this.mToolBarButton.setTextColor((int) ViewCompat.MEASURED_STATE_MASK);
            initTransAnim();
            this.mOpenFileListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                    if (!OpenFileOptions.this.mIsAnimtionFlag) {
                        OpenFileOptions.this.mFile = (File) OpenFileOptions.this.mFileList.get(i);
                        if (OpenFileOptions.this.mFile.isDirectory()) {
                            OpenFileOptions.this.GetFiles(OpenFileOptions.this.mFile, false, true);
                            OpenFileOptions.this.setButtonOnClick(OpenFileOptions.this.mFile);
                        } else if (OpenFileOptions.this.mDissmissReturnListener != null) {
                            OpenFileOptions.this.mDissmissReturnListener.openFile(OpenFileOptions.this.mFile.getAbsolutePath());
                        }
                    }
                }
            });
            this.mToolBarButton.setBackgroundDrawable(addBtnStateDrawable(getContext(), "file_list_btn_default.9.png", "file_list_btn_pressed.9.png"));
            this.mToolBarButton.setOnClickListener(new View.OnClickListener() {
                final File thisFile;

                {
                    this.thisFile = new File(OpenFileOptions.this.mSdcardPath);
                }

                public void onClick(View view) {
                    OpenFileOptions.this.GetFiles(this.thisFile, true, true);
                    int childCount = OpenFileOptions.this.mButtonsLayout.getChildCount();
                    for (int i = 1; i < childCount; i++) {
                        OpenFileOptions.this.mButtonsLayout.removeView(OpenFileOptions.this.mButtonsLayout.getChildAt(OpenFileOptions.this.mButtonsLayout.getChildCount() - 1));
                    }
                }
            });
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        this.mFileOptionsLayout = new FileOptionsLayout(getContext());
        this.mFileOptionsLayout.setOrientation(1);
        this.mFileOptionsLayout.addView(this.viewParent, new ViewGroup.LayoutParams(-1, -1));
        this.mFileOptionsLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams(this.windowWidth, this.windowWidth, 0, 0, CpuInfoManager.CHANNEL_PICTURE, 397344, -2);
        getWindow().setBackgroundDrawableResource(17170445);
        setCancelable(false);
        setContentView(this.mFileOptionsLayout, layoutParams);
    }

    public String getFilePath() {
        return this.mFile.getPath();
    }

    public String getFileName() {
        return this.mFile.getName();
    }

    public void setHasHandWritingPicList(ArrayList<File> arrayList) {
        this.mPicToPdfFlag = true;
        if (this.mHasHandWritingPicList != null) {
            this.mHasHandWritingPicList = new ArrayList<>();
        }
        this.mHasHandWritingPicList = arrayList;
    }

    protected class FileOptionsLayout extends LinearLayout {
        public FileOptionsLayout(Context context) {
            super(context);
        }

        public boolean dispatchTouchEvent(MotionEvent motionEvent) {
            Log.i("TAG", "dispatchTouchEvent");
            if (OpenFileOptions.this.mIsAnimtionFlag) {
                return true;
            }
            return super.dispatchTouchEvent(motionEvent);
        }

        public boolean dispatchKeyEvent(KeyEvent keyEvent) {
            Log.i("TAG", "dispatchKeyEvent");
            if (keyEvent.getKeyCode() != 4) {
                return super.dispatchKeyEvent(keyEvent);
            }
            if (keyEvent.getAction() != 1) {
                return true;
            }
            if (OpenFileOptions.this.mDissmissReturnListener != null) {
                OpenFileOptions.this.mDissmissReturnListener.backKey();
            }
            OpenFileOptions.this.dissmissFileOptions();
            return true;
        }

        /* access modifiers changed from: protected */
        public void onDetachedFromWindow() {
            super.onDetachedFromWindow();
        }

        /* access modifiers changed from: protected */
        public void onAttachedToWindow() {
            super.onAttachedToWindow();
        }

        public boolean onTouchEvent(MotionEvent motionEvent) {
            return true;
        }
    }

    public void dissmisWindow() {
        dissmissFileOptions();
    }

    public void showFileOptions() {
        if (!FilePath.isCanUseSdCard()) {
            showToastCenter("SD不可用");
            return;
        }
        this.mShowProgressDialogFlag = false;
        showOptions();
    }

    /* access modifiers changed from: private */
    public void dissmissFileOptions() {
        dismiss();
    }

    private void showOptions() {
        this.mLastFile = null;
        GetFiles(this.mDefaultPath, false, false);
        if (this.mFileListFrameLayout == null) {
            this.mFileListFrameLayout = (FrameLayout) this.viewParent.findViewById(36);
            this.mFileListFrameLayout.addView(this.mOpenFileListView);
        }
        if (this.mBitmapImgView == null) {
            this.mBitmapImgView = new ImageView(getContext());
            this.mFileListFrameLayout.addView(this.mBitmapImgView, new WindowManager.LayoutParams(-1, -1));
        }
        if (this.mButtonsLayout == null) {
            this.mButtonsLayout = new LinearLayout(getContext());
            this.mButtonsLayout.setPadding(10, 0, 0, 2);
            this.mButtonsLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        } else {
            this.mButtonsLayout.removeAllViews();
        }
        this.mButtonsLayout.addView(this.mToolBarButton);
        if (this.mFileUrlFrameLayout == null) {
            this.mFileUrlFrameLayout = (FrameLayout) this.viewParent.findViewById(35);
        }
        this.mButtonScrollView.removeAllViews();
        this.mFileUrlFrameLayout.removeAllViews();
        this.mButtonScrollView.addView(this.mButtonsLayout);
        this.mFileUrlFrameLayout.addView(this.mButtonScrollView);
        show();
        if (this.mDissmissReturnListener != null) {
            this.mDissmissReturnListener.migrate();
        }
    }

    /* access modifiers changed from: private */
    public void GetFiles(File file, boolean z, boolean z2) {
        if (this.mLastFile != file) {
            if (!this.mFileList.isEmpty()) {
                this.mFileList = new ArrayList();
            }
            this.mRefreshTask = null;
            this.mRefreshTask = new OpenFileOptionsRefreshTask();
            this.mRefreshTask.setFlagOnTouch(file, z, z2);
            this.mRefreshTask.execute(new Void[0]);
        }
    }

    /* access modifiers changed from: private */
    public void refrashFile() {
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        for (int i = 0; i < this.mFileList.size(); i++) {
            if (this.mFileList.get(i).isDirectory()) {
                arrayList.add(this.mFileList.get(i));
            }
        }
        for (int i2 = 0; i2 < this.mFileList.size(); i2++) {
            if (!this.mFileList.get(i2).isDirectory()) {
                arrayList2.add(this.mFileList.get(i2));
            }
        }
        if (this.mPicToPdfFlag && this.mFile.getPath().equals(new StringBuilder(String.valueOf(this.mSdcardPath)).toString())) {
            for (int i3 = 0; i3 < this.mHasHandWritingPicList.size(); i3++) {
                arrayList2.add(this.mHasHandWritingPicList.get(i3));
            }
        }
        List<File> sortFile = sortFile(arrayList);
        sortFile.addAll(sortFile(arrayList2));
        this.mFileList = sortFile;
    }

    public void setDissmissReturnListener(DissmissReturnListener dissmissReturnListener) {
        this.mDissmissReturnListener = dissmissReturnListener;
    }

    private void initTransAnim() {
        this.mTransAnimLeftIn = new TranslateAnimation(650.0f, 0.0f, 0.0f, 0.0f);
        this.mTransAnimLeftOut = new TranslateAnimation(0.0f, -650.0f, 0.0f, 0.0f);
        this.mTransAnimRightIn = new TranslateAnimation(-650.0f, 0.0f, 0.0f, 0.0f);
        this.mTransAnimRightOut = new TranslateAnimation(0.0f, 650.0f, 0.0f, 0.0f);
        this.mTransAnimLeftIn.setFillAfter(true);
        this.mTransAnimLeftOut.setFillAfter(false);
        this.mTransAnimRightIn.setFillAfter(true);
        this.mTransAnimRightOut.setFillAfter(false);
        this.mTransAnimLeftIn.setDuration(300);
        this.mTransAnimLeftOut.setDuration(300);
        this.mTransAnimRightIn.setDuration(300);
        this.mTransAnimRightOut.setDuration(300);
        this.mTransAnimLeftOut.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationEnd(Animation animation) {
                OpenFileOptions.this.mOpenFileListView.setEnabled(true);
                OpenFileOptions.this.mIsAnimtionFlag = false;
                if (OpenFileOptions.this.mOpenFileListView != null && OpenFileOptions.this.mBitmapImgView != null) {
                    OpenFileOptions.this.mBitmapImgView.setVisibility(8);
                }
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
                OpenFileOptions.this.mOpenFileListView.setEnabled(false);
                OpenFileOptions.this.mIsAnimtionFlag = true;
                if (OpenFileOptions.this.mFileList.size() == 0) {
                    OpenFileOptions.this.mAddTextViewFlag = true;
                }
            }
        });
        this.mTransAnimRightOut.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationEnd(Animation animation) {
                OpenFileOptions.this.mOpenFileListView.setEnabled(true);
                OpenFileOptions.this.mIsAnimtionFlag = false;
                if (OpenFileOptions.this.mOpenFileListView != null && OpenFileOptions.this.mBitmapImgView != null) {
                    OpenFileOptions.this.mBitmapImgView.setVisibility(8);
                }
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
                OpenFileOptions.this.mIsAnimtionFlag = true;
                OpenFileOptions.this.mOpenFileListView.setEnabled(false);
            }
        });
    }

    /* access modifiers changed from: private */
    public void setButtonOnClick(File file) {
        this.mButtonsLayout.addView(preparePathButton(file));
    }

    private Button preparePathButton(final File file) {
        Button button = new Button(getContext());
        button.setMinWidth(100);
        button.setBackgroundDrawable(addBtnStateDrawable(getContext(), "file_list_btn_default.9.png", "file_list_btn_pressed.9.png"));
        button.setTextSize(16.0f);
        button.setLayoutParams(new LinearLayout.LayoutParams(-2, -1));
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                for (int i = 0; i < OpenFileOptions.this.mButtonsLayout.getChildCount(); i++) {
                    if (view == OpenFileOptions.this.mButtonsLayout.getChildAt(i)) {
                        int childCount = OpenFileOptions.this.mButtonsLayout.getChildCount() - i;
                        for (int i2 = 1; i2 < childCount; i2++) {
                            OpenFileOptions.this.mButtonsLayout.removeView(OpenFileOptions.this.mButtonsLayout.getChildAt(OpenFileOptions.this.mButtonsLayout.getChildCount() - 1));
                        }
                    }
                }
                OpenFileOptions.this.GetFiles(file, true, true);
            }
        });
        button.setText(file.getName());
        button.setTextColor((int) ViewCompat.MEASURED_STATE_MASK);
        return button;
    }

    private class OpenFileOptionsRefreshTask extends AsyncTask<Void, Integer, Integer> {
        boolean flagOnTouch;
        boolean mAnim;
        File mThisFile;

        public OpenFileOptionsRefreshTask() {
        }

        public void setFlagOnTouch(File file, boolean z, boolean z2) {
            this.mThisFile = file;
            this.flagOnTouch = z;
            this.mAnim = z2;
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
            OpenFileOptions.this.mHandler.sendEmptyMessageDelayed(1, 200);
            OpenFileOptions.this.mOpenFileListView.setEnabled(false);
            OpenFileOptions.this.mIsAnimtionFlag = true;
            OpenFileOptions.this.mFile = this.mThisFile;
        }

        /* access modifiers changed from: protected */
        public Integer doInBackground(Void... voidArr) {
            File[] listFiles = this.mThisFile.listFiles(OpenFileOptions.this.getFileFilter());
            if (listFiles != null) {
                for (File add : listFiles) {
                    OpenFileOptions.this.mFileList.add(add);
                }
            }
            OpenFileOptions.this.refrashFile();
            return null;
        }

        /* access modifiers changed from: protected */
        public void onProgressUpdate(Integer... numArr) {
            super.onProgressUpdate((Object[]) numArr);
        }

        /* access modifiers changed from: protected */
        public void onCancelled() {
            super.onCancelled();
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Integer num) {
            if (OpenFileOptions.this.mProgressDialog != null && OpenFileOptions.this.mProgressDialog.isShowing()) {
                OpenFileOptions.this.mProgressDialog.dismiss();
            }
            OpenFileOptions.this.mHandler.removeMessages(1);
            if (!this.mAnim) {
                OpenFileOptions.this.mIsAnimtionFlag = false;
            } else if (!(OpenFileOptions.this.mOpenFileListView == null || OpenFileOptions.this.mBitmapImgView == null)) {
                OpenFileOptions.this.mBitmapImgView.setVisibility(0);
                if (OpenFileOptions.this.mLocalBitmap != null && !OpenFileOptions.this.mLocalBitmap.isRecycled()) {
                    OpenFileOptions.this.mLocalBitmap.recycle();
                    OpenFileOptions.this.mLocalBitmap = null;
                }
                OpenFileOptions.this.mLocalBitmap = Bitmap.createBitmap(OpenFileOptions.this.mOpenFileListView.getWidth(), OpenFileOptions.this.mOpenFileListView.getHeight(), Bitmap.Config.ARGB_8888);
                OpenFileOptions.this.mOpenFileListView.draw(new Canvas(OpenFileOptions.this.mLocalBitmap));
                OpenFileOptions.this.mBitmapImgView.setImageBitmap(OpenFileOptions.this.mLocalBitmap);
                if (this.flagOnTouch) {
                    OpenFileOptions.this.mBitmapImgView.setAnimation(OpenFileOptions.this.mTransAnimRightOut);
                    OpenFileOptions.this.mOpenFileListView.setAnimation(OpenFileOptions.this.mTransAnimRightIn);
                    OpenFileOptions.this.mTransAnimRightOut.start();
                    OpenFileOptions.this.mTransAnimRightIn.start();
                } else {
                    OpenFileOptions.this.mBitmapImgView.setAnimation(OpenFileOptions.this.mTransAnimLeftOut);
                    OpenFileOptions.this.mOpenFileListView.setAnimation(OpenFileOptions.this.mTransAnimLeftIn);
                    OpenFileOptions.this.mTransAnimLeftOut.start();
                    OpenFileOptions.this.mTransAnimLeftIn.start();
                }
            }
            OpenFileOptions.this.mAdapter = new OpenFileAdapter(OpenFileOptions.this.getContext(), OpenFileOptions.this.mFileList);
            if (!this.mAnim) {
                if (OpenFileOptions.this.mFileList.size() == 0) {
                    OpenFileOptions.this.mAddTextViewFlag = true;
                }
            } else if (OpenFileOptions.this.mFileList.size() != 0 && OpenFileOptions.this.mAddTextViewFlag) {
                OpenFileOptions.this.mAddTextViewFlag = false;
            }
            OpenFileOptions.this.mOpenFileListView.setAdapter((ListAdapter) OpenFileOptions.this.mAdapter);
            OpenFileOptions.this.mOpenFileListView.setEnabled(true);
            OpenFileOptions.this.mLastFile = this.mThisFile;
            super.onPostExecute((Object) num);
        }
    }

    /* access modifiers changed from: private */
    public void showProgressDialog() {
        if (this.mProgressDialog == null) {
            this.mProgressDialog = new ProgressDialog(getContext());
            this.mProgressDialog.setMessage("");
            this.mProgressDialog.setIndeterminate(false);
            this.mProgressDialog.setCancelable(false);
            this.mProgressDialog.show();
        }
    }

    private List<File> sortFile(List<File> list) {
        File[] fileArr = (File[]) list.toArray(new File[0]);
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < fileArr.length - 1; i++) {
            for (int i2 = 0; i2 < (fileArr.length - 1) - i; i2++) {
                if (fileArr[i2].lastModified() < fileArr[i2 + 1].lastModified()) {
                    File file = fileArr[i2];
                    fileArr[i2] = fileArr[i2 + 1];
                    fileArr[i2 + 1] = file;
                }
            }
        }
        for (File add : fileArr) {
            arrayList.add(add);
        }
        return arrayList;
    }

    public void upDateLayout() {
        dissmissFileOptions();
        showFileOptions();
    }

    public boolean getShowFlag() {
        return this.mIsAddviewFlag;
    }

    public void showToastCenter(String str) {
        if (this.mToast == null) {
            this.mToast = Toast.makeText(getContext(), str, 0);
            this.mToast.setGravity(17, 0, 0);
        } else {
            this.mToast.setText(str);
        }
        this.mToast.show();
    }

    /* access modifiers changed from: private */
    public FileFilter getFileFilter() {
        return new FileFilter() {
            public boolean accept(File file) {
                String name = file.getName();
                if (file.isHidden()) {
                    return false;
                }
                if (name.toUpperCase().endsWith("") || file.isDirectory()) {
                    return true;
                }
                return false;
            }
        };
    }

    private LinearLayout getViewGroup() {
        LinearLayout linearLayout = new LinearLayout(getContext());
        linearLayout.setOrientation(1);
        linearLayout.setGravity(16);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(650, -1));
        LinearLayout linearLayout2 = new LinearLayout(getContext());
        linearLayout2.setGravity(16);
        linearLayout2.setLayoutParams(new LinearLayout.LayoutParams(-1, dip2px(40.0f)));
        linearLayout2.setBackgroundDrawable(get9Drawable("dialog_title_bg.9.png"));
        FrameLayout frameLayout = new FrameLayout(getContext());
        frameLayout.setId(35);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        layoutParams.setMargins(5, 5, 5, 5);
        frameLayout.setLayoutParams(layoutParams);
        linearLayout2.addView(frameLayout);
        LinearLayout linearLayout3 = new LinearLayout(getContext());
        linearLayout3.setOrientation(1);
        linearLayout3.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        linearLayout3.setPadding(10, 10, 10, 10);
        linearLayout3.setBackgroundDrawable(get9Drawable("dialog_bg.9.png"));
        FrameLayout frameLayout2 = new FrameLayout(getContext());
        frameLayout2.setId(36);
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-1, dip2px(220.0f));
        layoutParams2.setMargins(20, 15, 20, 5);
        frameLayout2.setLayoutParams(layoutParams2);
        frameLayout2.setBackgroundDrawable(get9Drawable("dialog_table_bg.9.png"));
        LinearLayout linearLayout4 = new LinearLayout(getContext());
        linearLayout4.setLayoutParams(new LinearLayout.LayoutParams(-1, 60));
        linearLayout4.setGravity(17);
        Button button = new Button(getContext());
        button.setId(37);
        button.setLayoutParams(new LinearLayout.LayoutParams(200, 50));
        button.setBackgroundDrawable(addStateDrawable(getContext(), "btn_bg_normal.9.png", "btn_bg_pressed.9.png"));
        button.setText("关闭");
        linearLayout4.addView(button);
        linearLayout3.addView(frameLayout2);
        linearLayout3.addView(linearLayout4);
        linearLayout.addView(linearLayout2);
        linearLayout.addView(linearLayout3);
        return linearLayout;
    }

    private StateListDrawable addStateDrawable(Context context, String str, String str2) {
        StateListDrawable stateListDrawable = new StateListDrawable();
        Drawable drawable = get9Drawable(str);
        Drawable drawable2 = get9Drawable(str2);
        stateListDrawable.addState(new int[]{16842919, 16842910}, drawable2);
        stateListDrawable.addState(new int[]{16842919}, drawable2);
        stateListDrawable.addState(new int[]{16842910}, drawable);
        stateListDrawable.addState(new int[0], drawable);
        return stateListDrawable;
    }

    private StateListDrawable addBtnStateDrawable(Context context, String str, String str2) {
        StateListDrawable stateListDrawable = new StateListDrawable();
        Drawable drawable = getDrawable(str);
        Drawable drawable2 = getDrawable(str2);
        stateListDrawable.addState(new int[]{16842919, 16842910}, drawable2);
        stateListDrawable.addState(new int[]{16842919}, drawable2);
        stateListDrawable.addState(new int[]{16842910}, drawable);
        stateListDrawable.addState(new int[0], drawable);
        return stateListDrawable;
    }

    private Drawable getDrawable(String str) {
        Exception e;
        BitmapDrawable bitmapDrawable;
        try {
            InputStream open = getContext().getAssets().open(str);
            bitmapDrawable = new BitmapDrawable(BitmapFactory.decodeStream(open));
            try {
                open.close();
            } catch (Exception e2) {
                e = e2;
                e.printStackTrace();
                return bitmapDrawable;
            }
        } catch (Exception e3) {
            Exception exc = e3;
            bitmapDrawable = null;
            e = exc;
            e.printStackTrace();
            return bitmapDrawable;
        }
        return bitmapDrawable;
    }

    private Drawable get9Drawable(String str) {
        Exception e;
        NinePatchDrawable ninePatchDrawable;
        try {
            InputStream open = getContext().getAssets().open(str);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inDensity = 320;
            options.inTargetDensity = getContext().getResources().getDisplayMetrics().densityDpi;
            options.inScaled = true;
            Bitmap decodeStream = BitmapFactory.decodeStream(open, new Rect(), options);
            ninePatchDrawable = new NinePatchDrawable(decodeStream, decodeStream.getNinePatchChunk(), new Rect(), null);
            try {
                open.close();
            } catch (Exception e2) {
                e = e2;
                e.printStackTrace();
                return ninePatchDrawable;
            }
        } catch (Exception e3) {
            Exception exc = e3;
            ninePatchDrawable = null;
            e = exc;
            e.printStackTrace();
            return ninePatchDrawable;
        }
        return ninePatchDrawable;
    }

    private int dip2px(float f) {
        return (int) ((getContext().getResources().getDisplayMetrics().density * f) + 0.5f);
    }
}
