package com.cmsc.cmmusic.common.data;

import java.util.ArrayList;

public class SongRecommendResult extends Result {
    private ArrayList<SongRecommendContentIds> songRecommendContentIds;

    public SongRecommendResult() {
    }

    public SongRecommendResult(Result result) {
        if (result != null) {
            setResCode(result.getResCode());
            setResMsg(result.getResMsg());
        }
    }

    public ArrayList<SongRecommendContentIds> getSongRecommendContentIds() {
        return this.songRecommendContentIds;
    }

    public void setSongRecommendContentIds(ArrayList<SongRecommendContentIds> arrayList) {
        this.songRecommendContentIds = arrayList;
    }

    public String toString() {
        return "RecommengSongResult [songRecommendContentIds=" + this.songRecommendContentIds + ", getResCode()=" + getResCode() + ", getResMsg()=" + getResMsg() + "]";
    }
}
