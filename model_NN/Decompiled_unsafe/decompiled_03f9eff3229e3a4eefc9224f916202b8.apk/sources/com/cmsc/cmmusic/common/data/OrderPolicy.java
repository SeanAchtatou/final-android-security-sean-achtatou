package com.cmsc.cmmusic.common.data;

import com.cmsc.cmmusic.common.Logger;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public final class OrderPolicy extends Result {
    public static final String DB = "2";
    public static final String HD = "1";
    public static final String SD = "0";
    private AlbumInfo albumInfo;
    private ArrayList<BizInfo> bizInfos;
    private List<ClubUserInfo> clubUserInfos;
    private String cpName;
    private String downUrl;
    private String downUrlL;
    private String invalidDate;
    private ArrayList<MVOrderInfo> mVOrderInfos;
    private String mobile;
    private String monLevel;
    private String monthType;
    private MusicInfo musicInfo;
    private MVInfo mvInfo;
    private String openrbDescription;
    private String openrbPrice;
    private OrderType orderType;
    private String restTimes;
    private List<SongMonthInfo> songMonthInfos;
    private String sum;
    private UserInfo userInfo;
    private String userMonType;

    public enum OrderPolicyType {
        fullSong,
        vibrateRing,
        ringback,
        own_ring_backround_music,
        openMember,
        ringbackOpen,
        openSongMonth,
        openCPMonth,
        cpFullSong,
        cpVibrateRing,
        mvDownLoad,
        exclusiveOnce,
        digitalAlbum
    }

    public enum OrderType {
        net,
        sms,
        verifyCode
    }

    public ArrayList<MVOrderInfo> getmVOrderInfos() {
        return this.mVOrderInfos;
    }

    public void setmVOrderInfos(ArrayList<MVOrderInfo> arrayList) {
        this.mVOrderInfos = arrayList;
    }

    public MVInfo getMvInfo() {
        return this.mvInfo;
    }

    public void setMvInfo(MVInfo mVInfo) {
        this.mvInfo = mVInfo;
    }

    public String getUserMonType() {
        return this.userMonType;
    }

    public void setUserMonType(String str) {
        this.userMonType = str;
    }

    public String getMobile() {
        return this.mobile;
    }

    public void setMobile(String str) {
        this.mobile = str;
    }

    public OrderType getOrderType() {
        return this.orderType;
    }

    public void setOrderType(OrderType orderType2) {
        this.orderType = orderType2;
    }

    public MusicInfo getMusicInfo() {
        return this.musicInfo;
    }

    public void setMusicInfo(MusicInfo musicInfo2) {
        this.musicInfo = musicInfo2;
    }

    public ArrayList<BizInfo> getBizInfos() {
        return this.bizInfos;
    }

    public void setBizInfos(ArrayList<BizInfo> arrayList) {
        this.bizInfos = arrayList;
    }

    public String getDownUrl() {
        return this.downUrl;
    }

    public void setDownUrl(String str) {
        this.downUrl = str;
    }

    public String getDownUrlL() {
        return this.downUrlL;
    }

    public void setDownUrlL(String str) {
        this.downUrlL = str;
    }

    public String getInvalidDate() {
        return this.invalidDate;
    }

    public void setInvalidDate(String str) {
        this.invalidDate = str;
    }

    public UserInfo getUserInfo() {
        return this.userInfo;
    }

    public void setUserInfo(UserInfo userInfo2) {
        this.userInfo = userInfo2;
    }

    public String getRestTimes() {
        return this.restTimes;
    }

    public void setRestTimes(String str) {
        this.restTimes = str;
    }

    public String getOpenrbPrice() {
        return this.openrbPrice;
    }

    public void setOpenrbPrice(String str) {
        this.openrbPrice = str;
    }

    public String getOpenrbDescription() {
        return this.openrbDescription;
    }

    public void setOpenrbDescription(String str) {
        this.openrbDescription = str;
    }

    public List<ClubUserInfo> getClubUserInfos() {
        return this.clubUserInfos;
    }

    public void setClubUserInfos(List<ClubUserInfo> list) {
        this.clubUserInfos = list;
    }

    public List<SongMonthInfo> getSongMonthInfos() {
        return this.songMonthInfos;
    }

    public void setSongMonthInfos(List<SongMonthInfo> list) {
        this.songMonthInfos = list;
    }

    public String getCpName() {
        return this.cpName;
    }

    public void setCpName(String str) {
        this.cpName = str;
    }

    public String getMonthType() {
        return this.monthType;
    }

    public void setMonthType(String str) {
        this.monthType = str;
    }

    public AlbumInfo getAlbumInfo() {
        return this.albumInfo;
    }

    public void setAlbumInfo(AlbumInfo albumInfo2) {
        this.albumInfo = albumInfo2;
    }

    public String getSum() {
        return this.sum;
    }

    public void setSum(String str) {
        this.sum = str;
    }

    private void optimizeDownloadPolicyForResource(HashMap<String, BizInfo> hashMap, BizInfo bizInfo) {
        if (bizInfo != null && -1 != getDownloadPolicyPriority(bizInfo.getBizType())) {
            if (!hashMap.containsKey(bizInfo.getResource())) {
                hashMap.put(bizInfo.getResource(), bizInfo);
            } else if (existBetterDownloadPolicy(hashMap, bizInfo)) {
                hashMap.put(bizInfo.getResource(), bizInfo);
            }
        }
    }

    private boolean existBetterDownloadPolicy(HashMap<String, BizInfo> hashMap, BizInfo bizInfo) {
        return getDownloadPolicyPriority(bizInfo.getBizType()) > getDownloadPolicyPriority(hashMap.get(bizInfo.getResource()).getBizType());
    }

    private int getDownloadPolicyPriority(String str) {
        return getPriority(new String[]{Constants.VIA_REPORT_TYPE_SHARE_TO_QZONE, "20", "29", "00"}, str);
    }

    private int getPriority(String[] strArr, String str) {
        return Arrays.asList(strArr).indexOf(str);
    }

    public HashMap<String, BizInfo> getDownloadResourceMap() {
        HashMap<String, BizInfo> hashMap = new HashMap<>();
        Iterator<BizInfo> it = this.bizInfos.iterator();
        while (it.hasNext()) {
            optimizeDownloadPolicyForResource(hashMap, it.next());
        }
        Logger.i("TAG", "resourceMap = " + hashMap);
        return hashMap;
    }

    public String getMonLevel() {
        return this.monLevel;
    }

    public void setMonLevel(String str) {
        this.monLevel = str;
    }

    public String toString() {
        return "OrderPolicy [mobile=" + this.mobile + ", bizInfos=" + this.bizInfos + ", orderType=" + this.orderType + ", musicInfo=" + this.musicInfo + ", albumInfo=" + this.albumInfo + ", sum=" + this.sum + ", mvInfo=" + this.mvInfo + ", userInfo=" + this.userInfo + ", invalidDate=" + this.invalidDate + ", restTimes=" + this.restTimes + ", openrbPrice=" + this.openrbPrice + ", openrbDescription=" + this.openrbDescription + ", monthType=" + this.monthType + ", cpName=" + this.cpName + ", userMonType=" + this.userMonType + ", monLevel=" + this.monLevel + ", downUrl=" + this.downUrl + ", downUrlL=" + this.downUrlL + ", clubUserInfos=" + this.clubUserInfos + ", songMonthInfos=" + this.songMonthInfos + ", mVOrderInfos=" + this.mVOrderInfos + "]";
    }
}
