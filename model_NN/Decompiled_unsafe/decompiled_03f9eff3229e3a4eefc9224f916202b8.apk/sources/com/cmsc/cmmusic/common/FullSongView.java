package com.cmsc.cmmusic.common;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import cn.banshenggua.aichang.utils.Constants;
import com.cmsc.cmmusic.common.data.BizInfo;
import com.cmsc.cmmusic.common.data.OrderResult;

final class FullSongView extends DownloadSongView {
    private static final String LOG_TAG = "FullSongView";

    public FullSongView(Context context, Bundle bundle) {
        super(context, bundle);
    }

    /* access modifiers changed from: protected */
    public void showOrderPolicyView() {
        setUserTip("点击“确认”直接下载歌曲至手机");
        setupMonTip("您已开通全曲包月下载功能，本月还可以免费下载");
        setupRadioButton(100, "000009", "标清版（40kbps）");
        setupRadioButton(Constants.RESULT_OK, "020007", "高清版（128kbps）");
        setupRadioButton(Constants.REGISTER_OK, "020022", "杜比高清版");
        Log.d(LOG_TAG, "orderType : " + this.orderType + " , songName : " + this.curSongName + " , singerName : " + this.curSingerName);
    }

    /* access modifiers changed from: protected */
    public void downloadSong(BizInfo bizInfo, String str) {
        String bizCode = bizInfo.getBizCode();
        String bizType = bizInfo.getBizType();
        Log.d(LOG_TAG, "get url by net . bizCode : " + bizCode + " , bizType : " + bizType);
        EnablerInterface.getFullSongDownloadUrl(this.mCurActivity, this.curExtraInfo.getString("MusicId"), bizCode, bizType, str, bizInfo.getSalePrice(), this.policyObj.getMonLevel(), bizInfo.getHold2(), new CMMusicCallback<OrderResult>() {
            public void operationResult(OrderResult orderResult) {
                FullSongView.this.mCurActivity.closeActivity(orderResult);
            }
        });
    }
}
