package com.cmsc.cmmusic.common;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import cn.banshenggua.aichang.utils.Constants;
import com.cmsc.cmmusic.common.MediaService;
import com.cmsc.cmmusic.common.OpenFileOptions;
import com.cmsc.cmmusic.common.data.OrderPolicy;
import com.cmsc.cmmusic.common.data.OrderResult;
import com.cmsc.cmmusic.common.data.OwnRingBackgroundMusicRsp;
import com.cmsc.cmmusic.common.data.OwnRingRsp;
import com.cmsc.cmmusic.common.data.Result;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import org.xmlpull.v1.XmlPullParserException;

final class OrderOwnRingView extends RelativeLayout implements RadioGroup.OnCheckedChangeListener {
    public static final int CHECK_MODE = 18;
    public static final int COMMON = 1;
    public static final int CURRENT_OWN_RING_MONTH_STATUS_MODE = 15;
    public static final int FIVE_YUAN_MONTHLY = 66;
    public static final int KEY_ORDER_OWN_RING_MONTH_MODE = 7;
    public static final int MP3 = 56;
    public static final int MP3_FAILED = 11;
    public static final int MP3_FAILED_MODE = 5;
    public static final int MP3_MODE = 1;
    public static final int MP3_SUCCESS = 10;
    public static final int MP3_SUCCESS_MODE = 3;
    public static final String ONPLAYCOMPLETED = "onplaycompleted";
    public static final int OWN_RING_MONTH_MODE = 12;
    public static final int PLAY_PAUSE = 1;
    public static final int PLAY_PLAY = 2;
    public static final int PLAY_STOP = 0;
    public static final int RETURN_ORDER_MONTH = 67;
    public static final int RETURN_OWN_RING_MONTH_MODE = 16;
    public static final int RINGLISTEN = 64;
    public static final int RING_SEX_FEMALE = 59;
    public static final int RING_SEX_MAN = 60;
    public static final int RING_SPEED = 61;
    public static final int RING_SPEED_CLASS_SEEK_BAR = 62;
    protected static final String TAG = "OrderOwnRingView";
    public static final int TAPE = 0;
    public static final int TEXT = 55;
    public static final int TEXT_ANDVANCEDSET = 58;
    public static final int TEXT_BACKMUSICSTOP = 63;
    public static final int TEXT_FAILED = 9;
    public static final int TEXT_FAILED_MODE = 4;
    public static final int TEXT_MODE = 0;
    public static final int TEXT_SUCCESS = 8;
    public static final int TEXT_SUCCESS_MODE = 2;
    public static final int THREE_YUAN_MONTHLY = 65;
    public static final int VERIFY_MODE = 6;
    public static CMMusicActivity mCurActivity;
    static final String[] musics = {"请点击此处选择下面的音乐", "Canto Pop1", "Canto Pop2", "Danny Boy Jazz", "Fascination", "Goin Home", "GUANTANRAMARA", "I DON‘T LIKE TO SLEEP ALONE", "Jig", "KISS ME GOOD BYE", "Lullaby", "Melancholy", "MOOR RIVER", "MORE", "MORNING BROKEN", "Music Box", "Oue Sera Sera", "Royal Feast", "TALL LAURA I LOVE YOU", "TENESEE WALTZ", "THE END OF THE WORLD", "Tiramisu", "Today", "一生情一生还", "东山飘雨西山晴", "伊人何处", "依然是你的双手", "南都夜曲", "可怜小姑娘", "夕阳西沉", "幸福在这里", "恰想也是你一人", "情锁", "想你", "我用思念包装我的心", "断魂岭钟声泪月", "月满西楼", "望郎早归", "梦寐以求", "水仙", "沙溶之钟", "相思", "相思雨", "看不见的温柔", "祈祷", "莫忘我的爱", "西湖春", "让思念伴着我", "读你", "送君谱", "雨夜红"};
    static final String[] musicsId = {"00000", "00001", "00002", "00003", "00004", "00005", "00006", "00007", "00008", "00009", "00010", "00011", "00012", "00013", "00014", "00015", "00016", "00017", "00018", "00019", "00020", "00021", "00022", "00023", "00024", "00025", "00026", "00027", "00028", "00029", "00030", "00031", "00032", "00033", "00034", "00035", "00036", "00037", "00038", "00039", "00040", "00041", "00042", "00043", "00044", "00045", "00046", "00047", "00048", "00049", "00050"};
    /* access modifiers changed from: private */
    public static SeekBar seekBar;
    public static Handler seekBarHandle = new Handler(CMMusicActivity.mCurActivity.getMainLooper()) {
        public void handleMessage(Message message) {
            switch (message.what) {
                case -1:
                    int i = message.getData().getInt("posiztion");
                    int i2 = message.getData().getInt("duration");
                    if (i2 != 0) {
                        Log.i(OrderOwnRingView.TAG, "duration = " + i2);
                        OrderOwnRingView.txtDuration.setText(OrderOwnRingView.durationToString(new StringBuilder(String.valueOf(i2)).toString()));
                    }
                    int i3 = message.getData().getInt("posiztion") / Constants.CLEARIMGED;
                    String str = "";
                    int i4 = i3 / 60;
                    int i5 = i3 % 60;
                    if (i4 > 9) {
                        if (i5 > 9) {
                            str = String.valueOf(i4) + ":" + i5;
                        }
                        if (i5 <= 9) {
                            str = String.valueOf(i4) + ":0" + i5;
                        }
                    } else {
                        if (i5 > 9) {
                            str = "0" + i4 + ":" + i5;
                        }
                        if (i5 <= 9) {
                            str = "0" + i4 + ":0" + i5;
                        }
                    }
                    OrderOwnRingView.txtLapse.setText(str);
                    OrderOwnRingView.seekBar.setProgress(i);
                    break;
            }
            super.handleMessage(message);
        }
    };
    /* access modifiers changed from: private */
    public static TextView txtDuration;
    /* access modifiers changed from: private */
    public static TextView txtLapse;
    protected LinearLayout andvancedSetContentsLL = null;
    /* access modifiers changed from: private */
    public TextView bottomTxtTip;
    protected Button btnCancel = null;
    protected Button btnSure = null;
    protected LinearLayout btnView = null;
    private LinearLayout contentLinearLayout;
    protected Bundle curExtraInfo = null;
    public int currentPlayStatus = 0;
    private EditText edRingName;
    /* access modifiers changed from: private */
    public EditText edSignCode;
    /* access modifiers changed from: private */
    public EditText edText;
    /* access modifiers changed from: private */
    public LinearLayout fillerLinearLayout;
    /* access modifiers changed from: private */
    public ImageView imageViewSet;
    /* access modifiers changed from: private */
    public Result isOwnRingOrderMonthUserResult;
    boolean isPlay = false;
    boolean isShow = false;
    protected Handler mHandler = null;
    /* access modifiers changed from: private */
    public OpenFileOptions mOpenFileOptions;
    /* access modifiers changed from: private */
    public String mPath;
    /* access modifiers changed from: private */
    public OpenFileOptions.DissmissReturnListener mReturnListener = new OpenFileOptions.DissmissReturnListener() {
        public void openFile(String str) {
            OrderOwnRingView.this.mOpenFileOptions.dismiss();
            OrderOwnRingView.this.mPath = str;
            if (OrderOwnRingView.this.mPath != null && OrderOwnRingView.this.edSignCode != null) {
                OrderOwnRingView.this.edSignCode.setText(OrderOwnRingView.this.mPath);
            }
        }

        public void migrate() {
        }

        public void dissMigrate() {
        }

        public void backKey() {
            OrderOwnRingView.this.mOpenFileOptions.dismiss();
        }
    };
    /* access modifiers changed from: private */
    public int mode;
    private String monthType = "3";
    /* access modifiers changed from: private */
    public String mp3ListenUrl;
    /* access modifiers changed from: private */
    public Spinner musicspinner;
    /* access modifiers changed from: private */
    public Result openOwnRingResult;
    /* access modifiers changed from: private */
    public OwnRingBackgroundMusicRsp ownRingBackgroundMusicRsp;
    /* access modifiers changed from: private */
    public MediaPlayer ownRingListenMediaPlayer;
    /* access modifiers changed from: private */
    public OwnRingRsp ownRingRsp;
    private String phoneNum = "";
    /* access modifiers changed from: private */
    public ImageButton playAndPauseButton;
    /* access modifiers changed from: private */
    public RadioButton radioButtonFemale;
    /* access modifiers changed from: private */
    public RadioButton radioButtonMale;
    private RadioGroup rdGroup;
    private BroadcastReceiver receiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if (OrderOwnRingView.this.playAndPauseButton != null) {
                try {
                    InputStream open = OrderOwnRingView.mCurActivity.getAssets().open("img_playback_bt_play.png");
                    BitmapDrawable bitmapDrawable = new BitmapDrawable(BitmapFactory.decodeStream(open));
                    open.close();
                    OrderOwnRingView.this.playAndPauseButton.setImageDrawable(bitmapDrawable);
                    OrderOwnRingView.this.currentPlayStatus = 0;
                    OrderOwnRingView.this.stop();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    };
    private Button returnOrderMOnthBtn;
    /* access modifiers changed from: private */
    public Result returnOwnRingMonthResult;
    private Button ringListen;
    private LinearLayout ringListenLL;
    /* access modifiers changed from: private */
    public EditText ringNameEdText;
    /* access modifiers changed from: private */
    public RadioGroup ringSexRadioGroup;
    /* access modifiers changed from: private */
    public RadioGroup ringTypeGroup;
    protected LinearLayout rootView = null;
    /* access modifiers changed from: private */
    public String sex = "female";
    /* access modifiers changed from: private */
    public String textListenUrl;
    private TextView txtUserTip;

    public OrderOwnRingView(Context context, Bundle bundle, int i, OrderPolicy orderPolicy) {
        super(context);
        mCurActivity = (CMMusicActivity) context;
        this.mHandler = new Handler(mCurActivity.getMainLooper());
        this.curExtraInfo = bundle;
        setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(1);
        linearLayout.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        LinearLayout initLogoView = initLogoView();
        this.rootView = new LinearLayout(context);
        this.rootView.setOrientation(1);
        this.rootView.setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
        this.rootView.setPadding(20, 10, 10, 100);
        linearLayout.addView(initLogoView);
        linearLayout.addView(this.rootView);
        ScrollView scrollView = new ScrollView(context);
        scrollView.setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
        scrollView.addView(linearLayout);
        addView(scrollView);
        this.phoneNum = bundle.getString("phoneNum");
        initBtnView(context);
        setVisibility(0);
        initViews(i);
        mCurActivity.startService(new Intent("com.android.sitech.ttpod.media.MEDIASERVICE"));
        mCurActivity.registerReceiver(this.receiver, new IntentFilter(ONPLAYCOMPLETED));
        MediaService.setErrorListener(new MediaService.ErrorListener() {
            public boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
                return false;
            }
        });
    }

    /* access modifiers changed from: private */
    public void initViews(int i) {
        this.mode = i;
        switch (i) {
            case 0:
                this.rootView.removeAllViews();
                setupHeader(55);
                this.edText = new EditText(mCurActivity);
                this.edText.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
                this.edText.setHint("输入需要生成彩铃的文字");
                this.edText.setMinLines(3);
                this.rootView.addView(this.edText);
                TextView textView = new TextView(mCurActivity);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
                layoutParams.setMargins(0, 0, 0, 0);
                textView.setLayoutParams(layoutParams);
                textView.setTextAppearance(mCurActivity, 16973892);
                textView.setText("选择文字彩铃背景音乐：");
                this.rootView.addView(textView);
                this.musicspinner = new Spinner(mCurActivity);
                LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-1, -2);
                layoutParams2.setMargins(0, 0, 0, 10);
                this.musicspinner.setLayoutParams(layoutParams2);
                this.rootView.addView(this.musicspinner);
                ArrayAdapter arrayAdapter = new ArrayAdapter(mCurActivity, 17367048, musics);
                arrayAdapter.setDropDownViewResource(17367049);
                this.musicspinner.setAdapter((SpinnerAdapter) arrayAdapter);
                this.musicspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(AdapterView<?> adapterView, View view, final int i, long j) {
                        if (i == 0) {
                            OrderOwnRingView.mCurActivity.hideProgressBar();
                        } else {
                            OrderOwnRingView.mCurActivity.showProgressBar("请稍候...");
                        }
                        Log.i(OrderOwnRingView.TAG, ((Object) ((CharSequence) OrderOwnRingView.this.musicspinner.getSelectedItem())) + "---" + i + "被点击了");
                        if (OrderOwnRingView.this.isPlay) {
                            if (OrderOwnRingView.this.ownRingListenMediaPlayer != null) {
                                OrderOwnRingView.this.ownRingListenMediaPlayer.stop();
                                OrderOwnRingView.this.ownRingListenMediaPlayer.release();
                            }
                            new Thread(new Runnable() {
                                public void run() {
                                    Log.i(OrderOwnRingView.TAG, ((Object) ((CharSequence) OrderOwnRingView.this.musicspinner.getSelectedItem())) + "---" + i + "被点击了");
                                    try {
                                        Log.i(OrderOwnRingView.TAG, "musicsId = " + OrderOwnRingView.musicsId[i]);
                                        OrderOwnRingView.this.ownRingBackgroundMusicRsp = EnablerInterface.getOwnRingBackgroundMusicRsp(OrderOwnRingView.mCurActivity, OrderOwnRingView.musicsId[i]);
                                        Log.i("OrderOwnRingView_RingBackgroundMusicRsp", "背景音乐试听地址url = " + OrderOwnRingView.this.ownRingBackgroundMusicRsp.getmusicUrl());
                                        if (OrderOwnRingView.this.ownRingBackgroundMusicRsp != null && "000000".equals(OrderOwnRingView.this.ownRingBackgroundMusicRsp.getResCode())) {
                                            OrderOwnRingView.this.ownRingListenMediaPlayer = new MediaPlayer();
                                            OrderOwnRingView.this.ownRingListenMediaPlayer.reset();
                                            OrderOwnRingView.this.ownRingListenMediaPlayer.setDataSource(OrderOwnRingView.this.ownRingBackgroundMusicRsp.getmusicUrl());
                                            OrderOwnRingView.this.ownRingListenMediaPlayer.prepareAsync();
                                            OrderOwnRingView.this.ownRingListenMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                                                public void onPrepared(MediaPlayer mediaPlayer) {
                                                    OrderOwnRingView.mCurActivity.hideProgressBar();
                                                    OrderOwnRingView.this.ownRingListenMediaPlayer.start();
                                                }
                                            });
                                        }
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    } catch (XmlPullParserException e2) {
                                        e2.printStackTrace();
                                    } catch (IllegalArgumentException e3) {
                                        e3.printStackTrace();
                                    } catch (SecurityException e4) {
                                        e4.printStackTrace();
                                    } catch (IllegalStateException e5) {
                                        e5.printStackTrace();
                                    }
                                }
                            }).start();
                        }
                        OrderOwnRingView.this.isPlay = true;
                    }

                    public void onNothingSelected(AdapterView<?> adapterView) {
                    }
                });
                LinearLayout linearLayout = new LinearLayout(mCurActivity);
                linearLayout.setLayoutParams(new RelativeLayout.LayoutParams(-1, 30));
                this.rootView.addView(linearLayout);
                LinearLayout linearLayout2 = new LinearLayout(mCurActivity);
                linearLayout2.setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
                linearLayout2.setOrientation(0);
                linearLayout2.setGravity(5);
                this.rootView.addView(linearLayout2);
                TextView textView2 = new TextView(mCurActivity);
                textView2.setLayoutParams(new RelativeLayout.LayoutParams(-2, -2));
                textView2.setText("高级配置");
                textView2.setTextSize(15.0f);
                textView2.setId(58);
                linearLayout2.addView(textView2);
                LinearLayout linearLayout3 = new LinearLayout(mCurActivity);
                linearLayout3.setLayoutParams(new RelativeLayout.LayoutParams(20, -1));
                linearLayout2.addView(linearLayout3);
                try {
                    this.imageViewSet = new ImageView(mCurActivity);
                    this.imageViewSet.setLayoutParams(new RelativeLayout.LayoutParams(-2, -2));
                    InputStream open = mCurActivity.getAssets().open("andvanced_img_right.png");
                    BitmapDrawable bitmapDrawable = new BitmapDrawable(BitmapFactory.decodeStream(open));
                    open.close();
                    this.imageViewSet.setBackgroundDrawable(bitmapDrawable);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                linearLayout2.addView(this.imageViewSet);
                LinearLayout linearLayout4 = new LinearLayout(mCurActivity);
                linearLayout4.setLayoutParams(new RelativeLayout.LayoutParams(20, -1));
                linearLayout2.addView(linearLayout4);
                LinearLayout linearLayout5 = new LinearLayout(mCurActivity);
                linearLayout5.setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
                linearLayout5.setOrientation(0);
                linearLayout5.setGravity(5);
                this.rootView.addView(linearLayout5);
                this.contentLinearLayout = new LinearLayout(mCurActivity);
                this.contentLinearLayout.setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
                this.contentLinearLayout.setOrientation(1);
                this.rootView.addView(this.contentLinearLayout);
                this.andvancedSetContentsLL = new LinearLayout(mCurActivity);
                this.andvancedSetContentsLL.setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
                this.andvancedSetContentsLL.setOrientation(1);
                this.andvancedSetContentsLL.setVisibility(8);
                this.contentLinearLayout.addView(this.andvancedSetContentsLL);
                setupFooter();
                this.fillerLinearLayout = new LinearLayout(mCurActivity);
                this.fillerLinearLayout.setLayoutParams(new RelativeLayout.LayoutParams(-1, 50));
                this.rootView.addView(this.fillerLinearLayout);
                textView2.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        try {
                            InputStream open = OrderOwnRingView.mCurActivity.getAssets().open("andvanced_img_right.png");
                            InputStream open2 = OrderOwnRingView.mCurActivity.getAssets().open("andvanced_img_below.png");
                            Bitmap decodeStream = BitmapFactory.decodeStream(open);
                            Bitmap decodeStream2 = BitmapFactory.decodeStream(open2);
                            BitmapDrawable bitmapDrawable = new BitmapDrawable(decodeStream);
                            BitmapDrawable bitmapDrawable2 = new BitmapDrawable(decodeStream2);
                            open.close();
                            open2.close();
                            if (!OrderOwnRingView.this.isShow) {
                                OrderOwnRingView.this.imageViewSet.setBackgroundDrawable(bitmapDrawable2);
                            } else {
                                OrderOwnRingView.this.imageViewSet.setBackgroundDrawable(bitmapDrawable);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        OrderOwnRingView.this.andvancedSetContentsLL.removeAllViews();
                        if (!OrderOwnRingView.this.isShow) {
                            OrderOwnRingView.this.andvancedSetContentsLL.setVisibility(0);
                            OrderOwnRingView.this.bottomTxtTip.setVisibility(0);
                            OrderOwnRingView.this.fillerLinearLayout.setVisibility(0);
                            OrderOwnRingView.this.isShow = true;
                        } else {
                            OrderOwnRingView.this.andvancedSetContentsLL.setVisibility(8);
                            OrderOwnRingView.this.bottomTxtTip.setVisibility(0);
                            OrderOwnRingView.this.fillerLinearLayout.setVisibility(0);
                            OrderOwnRingView.this.isShow = false;
                        }
                        TextView textView = new TextView(OrderOwnRingView.mCurActivity);
                        textView.setLayoutParams(new RelativeLayout.LayoutParams(-2, -2));
                        textView.setText("彩铃语音");
                        OrderOwnRingView.this.andvancedSetContentsLL.addView(textView);
                        OrderOwnRingView.this.ringSexRadioGroup = new RadioGroup(OrderOwnRingView.mCurActivity);
                        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
                        layoutParams.setMargins(80, 10, 0, 10);
                        OrderOwnRingView.this.ringSexRadioGroup.setLayoutParams(layoutParams);
                        OrderOwnRingView.this.ringSexRadioGroup.setOrientation(0);
                        OrderOwnRingView.this.radioButtonFemale = new RadioButton(OrderOwnRingView.mCurActivity);
                        OrderOwnRingView.this.radioButtonFemale.setText("女声");
                        OrderOwnRingView.this.radioButtonFemale.setId(59);
                        OrderOwnRingView.this.radioButtonFemale.setChecked(true);
                        OrderOwnRingView.this.ringSexRadioGroup.addView(OrderOwnRingView.this.radioButtonFemale);
                        LinearLayout linearLayout = new LinearLayout(OrderOwnRingView.mCurActivity);
                        linearLayout.setLayoutParams(new RelativeLayout.LayoutParams(80, -1));
                        OrderOwnRingView.this.ringSexRadioGroup.addView(linearLayout);
                        OrderOwnRingView.this.radioButtonMale = new RadioButton(OrderOwnRingView.mCurActivity);
                        OrderOwnRingView.this.radioButtonMale.setLayoutParams(new RelativeLayout.LayoutParams(-2, -2));
                        OrderOwnRingView.this.radioButtonMale.setText("男声");
                        OrderOwnRingView.this.radioButtonMale.setId(60);
                        OrderOwnRingView.this.ringSexRadioGroup.addView(OrderOwnRingView.this.radioButtonMale);
                        OrderOwnRingView.this.andvancedSetContentsLL.addView(OrderOwnRingView.this.ringSexRadioGroup);
                        OrderOwnRingView.this.ringSexRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                                switch (i) {
                                    case 59:
                                        OrderOwnRingView.this.sex = "female";
                                        return;
                                    case 60:
                                        OrderOwnRingView.this.sex = "male";
                                        return;
                                    default:
                                        return;
                                }
                            }
                        });
                    }
                });
                this.btnSure.setText("确定");
                return;
            case 1:
                if (this.ownRingListenMediaPlayer != null) {
                    this.ownRingListenMediaPlayer.stop();
                    this.ownRingListenMediaPlayer.release();
                    this.ownRingListenMediaPlayer = null;
                }
                this.rootView.removeAllViews();
                setupHeader(56);
                this.ringTypeGroup = new RadioGroup(mCurActivity);
                this.ringTypeGroup.setOrientation(0);
                RadioButton radioButton = new RadioButton(mCurActivity);
                radioButton.setText("录制铃音");
                radioButton.setId(0);
                radioButton.setChecked(true);
                RadioButton radioButton2 = new RadioButton(mCurActivity);
                radioButton2.setText("普通铃音");
                radioButton2.setId(1);
                this.ringTypeGroup.addView(radioButton);
                this.ringTypeGroup.addView(radioButton2);
                this.rootView.addView(this.ringTypeGroup);
                LinearLayout linearLayout6 = new LinearLayout(mCurActivity);
                linearLayout6.setOrientation(0);
                linearLayout6.setLayoutParams(new LinearLayout.LayoutParams(-1, -2, 1.0f));
                linearLayout6.setGravity(16);
                this.edSignCode = new EditText(mCurActivity);
                this.edSignCode.setLayoutParams(new LinearLayout.LayoutParams(-1, -2, 0.5f));
                this.edSignCode.setHint("要上传的铃音");
                this.edSignCode.setSingleLine();
                linearLayout6.addView(this.edSignCode);
                Button button = new Button(mCurActivity);
                button.setLayoutParams(new LinearLayout.LayoutParams(-1, -2, 0.5f));
                button.setText("选择铃音文件");
                button.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        if (OrderOwnRingView.this.mOpenFileOptions == null) {
                            OrderOwnRingView.this.mOpenFileOptions = new OpenFileOptions(OrderOwnRingView.mCurActivity, null, FilePath.SDCARD);
                            OrderOwnRingView.this.mOpenFileOptions.setDissmissReturnListener(OrderOwnRingView.this.mReturnListener);
                        }
                        OrderOwnRingView.this.mOpenFileOptions.showFileOptions();
                    }
                });
                linearLayout6.addView(button);
                this.rootView.addView(linearLayout6);
                TextView textView3 = new TextView(mCurActivity);
                textView3.setTextAppearance(mCurActivity, 16973894);
                textView3.setText("支持格式：MP3/WAV");
                this.rootView.addView(textView3);
                setupFooter();
                this.btnSure.setText("确定");
                return;
            case 2:
                this.rootView.removeAllViews();
                setupListenPage(this.textListenUrl);
                return;
            case 3:
                this.rootView.removeAllViews();
                setupListenPage(this.mp3ListenUrl);
                return;
            case 4:
                this.rootView.removeAllViews();
                this.txtUserTip = new TextView(mCurActivity);
                this.txtUserTip.setTextAppearance(mCurActivity, 16973892);
                this.txtUserTip.setText("\n尊敬的手机用户：\n\n\n\n您输入的文字不符合要求，请核实后重新输入。\n\n");
                this.rootView.addView(this.txtUserTip);
                this.bottomTxtTip = new TextView(mCurActivity);
                this.bottomTxtTip.setTextAppearance(mCurActivity, 16973892);
                this.bottomTxtTip.setText("\n\n\n\n为保证彩铃制作成功，文字不超过100个字符，且不能含有非法内容。");
                this.rootView.addView(this.bottomTxtTip);
                this.btnSure.setText("重置");
                return;
            case 5:
                this.rootView.removeAllViews();
                this.txtUserTip = new TextView(mCurActivity);
                this.txtUserTip.setTextAppearance(mCurActivity, 16973892);
                this.txtUserTip.setText("\n尊敬的手机用户：\n\n\n\n您制作的彩铃生成失败，您制作的彩铃生成失败，您上传音频文件不符合要求，请核实后重新上传。\n\n");
                this.rootView.addView(this.txtUserTip);
                this.bottomTxtTip = new TextView(mCurActivity);
                this.bottomTxtTip.setTextAppearance(mCurActivity, 16973892);
                this.bottomTxtTip.setText("\n\n\n\n为保证彩铃制作成功，Mp3大小不超过2MB，长度不超过48秒");
                this.rootView.addView(this.bottomTxtTip);
                this.btnSure.setText("重置");
                return;
            case 6:
                this.rootView.removeAllViews();
                this.txtUserTip = new TextView(mCurActivity);
                this.txtUserTip.setTextAppearance(mCurActivity, 16973892);
                this.txtUserTip.setText("\n尊敬的手机用户\n\n\n\n您订制的彩铃已经提交，请等待管理员进行审核。\n\n");
                this.rootView.addView(this.txtUserTip);
                this.bottomTxtTip = new TextView(mCurActivity);
                this.bottomTxtTip.setTextAppearance(mCurActivity, 16973892);
                this.bottomTxtTip.setText("\n\n\n\n\n订制审核过程比较长，稍后会以短信方式通知订制是否成功，给您带来的不便，请谅解！");
                this.rootView.addView(this.bottomTxtTip);
                return;
            case 7:
                this.rootView.removeAllViews();
                this.txtUserTip = new TextView(mCurActivity);
                this.txtUserTip.setTextAppearance(mCurActivity, 16973892);
                this.txtUserTip.setText("尊敬的手机用户" + this.phoneNum + ",点击“确定”，将开通个性化彩铃包月功能。\n\n当前包月级别：非包月用户。");
                this.rootView.addView(this.txtUserTip);
                this.rdGroup = new RadioGroup(mCurActivity);
                RadioButton radioButton3 = new RadioButton(mCurActivity);
                radioButton3.setText("3元包月");
                radioButton3.setId(65);
                radioButton3.setChecked(true);
                RadioButton radioButton4 = new RadioButton(mCurActivity);
                radioButton4.setText("5元包月");
                radioButton4.setId(66);
                this.rdGroup.addView(radioButton3);
                this.rdGroup.addView(radioButton4);
                this.rootView.addView(this.rdGroup);
                this.rdGroup.setOnCheckedChangeListener(this);
                this.bottomTxtTip = new TextView(mCurActivity);
                this.bottomTxtTip.setTextAppearance(mCurActivity, 16973892);
                this.bottomTxtTip.setText("\n\n订购3元包月包每月可免费订购10首个性彩铃；5元包月包每月可免费不限次订购个性彩铃。");
                this.rootView.addView(this.bottomTxtTip);
                this.btnSure.setText("确定");
                return;
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 17:
            default:
                return;
            case 15:
                this.rootView.removeAllViews();
                this.txtUserTip = new TextView(mCurActivity);
                this.txtUserTip.setTextAppearance(mCurActivity, 16973892);
                this.txtUserTip.setText("尊敬的手机用户:\n\n\n您当前个性化彩铃包月关系：" + this.isOwnRingOrderMonthUserResult.getResMsg() + "\n\n\n点击”确定“将继续使用当前包月；点击”退订“，退订当前包月。\n\n");
                this.rootView.addView(this.txtUserTip);
                this.returnOrderMOnthBtn = new Button(mCurActivity);
                this.returnOrderMOnthBtn.setLayoutParams(new RelativeLayout.LayoutParams(-2, -2));
                this.returnOrderMOnthBtn.setId(67);
                this.returnOrderMOnthBtn.setText("退订");
                LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(-1, -2);
                layoutParams3.setMargins(200, 0, 200, 0);
                this.ringListen.setLayoutParams(layoutParams3);
                this.rootView.addView(this.returnOrderMOnthBtn);
                this.returnOrderMOnthBtn.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        OrderOwnRingView.this.initViews(16);
                    }
                });
                this.bottomTxtTip = new TextView(mCurActivity);
                this.bottomTxtTip.setTextAppearance(mCurActivity, 16973892);
                this.bottomTxtTip.setText("\n\n若您想要更换包月，必须退订当前包月才能订购其他包月。");
                this.rootView.addView(this.bottomTxtTip);
                this.btnSure.setText("确定");
                return;
            case 16:
                this.rootView.removeAllViews();
                this.txtUserTip = new TextView(mCurActivity);
                this.txtUserTip.setTextAppearance(mCurActivity, 16973892);
                this.txtUserTip.setText("尊敬的手机用户:\n\n\n您当前个性化彩铃包月关系：" + this.isOwnRingOrderMonthUserResult.getResMsg() + "\n\n请再次确认是否退订当前包月。\n\n\n\n\n");
                this.rootView.addView(this.txtUserTip);
                this.bottomTxtTip = new TextView(mCurActivity);
                this.bottomTxtTip.setTextAppearance(mCurActivity, 16973892);
                this.bottomTxtTip.setText("若您想要更换包月，必须退订当前包月才能订购其他包月。");
                this.rootView.addView(this.bottomTxtTip);
                this.btnSure.setText("确定");
                return;
            case 18:
                this.rootView.removeAllViews();
                this.txtUserTip = new TextView(mCurActivity);
                this.txtUserTip.setTextAppearance(mCurActivity, 16973892);
                this.txtUserTip.setText("尊敬的手机用户:\n\n\n您订制的彩铃已成功提交，请等待管理员进行审核！");
                this.rootView.addView(this.txtUserTip);
                this.bottomTxtTip = new TextView(mCurActivity);
                this.bottomTxtTip.setTextAppearance(mCurActivity, 16973892);
                this.bottomTxtTip.setText("\n\n\n\n\n订制审核过程比较长，稍后会以短信方式通知订制是否成功，给您带来的不便，请谅解！");
                this.rootView.addView(this.bottomTxtTip);
                this.btnSure.setText("确定");
                return;
        }
    }

    private void setupListenPage(final String str) {
        this.txtUserTip = new TextView(mCurActivity);
        this.txtUserTip.setTextAppearance(mCurActivity, 16973892);
        this.txtUserTip.setText("\n尊敬的手机用户:\n\n您制作的彩铃已经生成，点击“确定”将订制此彩铃至手机\n");
        this.rootView.addView(this.txtUserTip);
        TextView textView = new TextView(mCurActivity);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
        layoutParams.setMargins(0, 40, 0, 0);
        textView.setLayoutParams(layoutParams);
        textView.setTextAppearance(mCurActivity, 16973892);
        textView.setText("铃音名称：");
        this.rootView.addView(textView);
        this.edRingName = new EditText(mCurActivity);
        this.edRingName.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        this.edRingName.setText(this.ringNameEdText.getText().toString());
        this.edRingName.setEnabled(false);
        this.rootView.addView(this.edRingName);
        this.ringListenLL = new LinearLayout(mCurActivity);
        this.ringListenLL.setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
        this.ringListenLL.setGravity(17);
        this.rootView.addView(this.ringListenLL);
        this.ringListen = new Button(mCurActivity);
        this.ringListen.setId(64);
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-1, -2);
        layoutParams2.setMargins(150, 0, 150, 0);
        this.ringListen.setLayoutParams(layoutParams2);
        this.ringListen.setText("铃音试听");
        this.ringListen.setGravity(17);
        this.ringListenLL.addView(this.ringListen);
        this.ringListen.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                OrderOwnRingView.this.ringListen(str);
            }
        });
        this.bottomTxtTip = new TextView(mCurActivity);
        this.bottomTxtTip.setTextAppearance(mCurActivity, 16973892);
        this.bottomTxtTip.setText("\n非彩铃用户定制个性化彩铃包月将自动开通彩铃功能每月5元。");
        this.rootView.addView(this.bottomTxtTip);
        this.btnSure.setText("确定");
    }

    private void setupFooter() {
        this.bottomTxtTip = new TextView(mCurActivity);
        this.bottomTxtTip.setTextAppearance(mCurActivity, 16973892);
        this.bottomTxtTip.setText("\n为保证彩铃制作成功，文字不超过100个字符，MP3大小不超过2MB，长度不超过48秒。");
        this.rootView.addView(this.bottomTxtTip);
    }

    private void setupHeader(int i) {
        this.txtUserTip = new TextView(mCurActivity);
        this.txtUserTip.setTextAppearance(mCurActivity, 16973892);
        this.txtUserTip.setText("尊敬的手机用户" + this.phoneNum + ",您现在可以使用个性化彩铃了！\n\n马上制作订购属于自己的专属彩铃：");
        this.rootView.addView(this.txtUserTip);
        this.rdGroup = new RadioGroup(mCurActivity);
        RadioButton radioButton = new RadioButton(mCurActivity);
        radioButton.setText("文字变彩铃");
        radioButton.setId(55);
        RadioButton radioButton2 = new RadioButton(mCurActivity);
        radioButton2.setText("MP3变彩铃");
        radioButton2.setId(56);
        switch (i) {
            case 55:
                radioButton.setChecked(true);
                break;
            case 56:
                radioButton2.setChecked(true);
                break;
        }
        this.rdGroup.addView(radioButton);
        this.rdGroup.addView(radioButton2);
        this.rootView.addView(this.rdGroup);
        this.rdGroup.setOnCheckedChangeListener(this);
        this.ringNameEdText = new EditText(mCurActivity);
        this.ringNameEdText.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        this.ringNameEdText.setHint("请输入铃音名称");
        this.rootView.addView(this.ringNameEdText);
    }

    /* access modifiers changed from: protected */
    public void sureClicked() {
        if (this.ownRingListenMediaPlayer != null) {
            this.ownRingListenMediaPlayer.stop();
            this.ownRingListenMediaPlayer.release();
            this.ownRingListenMediaPlayer = null;
        }
        mCurActivity.showProgressBar("请稍候...");
        switch (this.mode) {
            case 0:
                if (this.ringNameEdText.getText().toString().trim().length() == 0) {
                    mCurActivity.hideProgressBar();
                    this.ringNameEdText.requestFocus();
                    this.ringNameEdText.setError(Html.fromHtml("<font color='red'>请输入铃音名称</font>"));
                    return;
                } else if (this.edText.getText().toString().trim().length() == 0) {
                    mCurActivity.hideProgressBar();
                    this.edText.requestFocus();
                    this.edText.setError(Html.fromHtml("<font color='red'>请输入需要生成的彩铃文字</font>"));
                    return;
                } else {
                    new Thread(new Runnable() {
                        public void run() {
                            try {
                                String access$28 = OrderOwnRingView.this.getSpinnerSelectMusicId();
                                Log.i("OrderOwnRingView_bgMusicId", "北京音乐Id = " + access$28);
                                Log.i("OrderOwnRingView_sex", "音乐性别  sex= " + OrderOwnRingView.this.sex);
                                OrderOwnRingView.this.ownRingRsp = EnablerInterface.getTextOwnRingRsp(OrderOwnRingView.mCurActivity, OrderOwnRingView.this.edText.getText().toString(), access$28, OrderOwnRingView.this.sex, OrderOwnRingView.this.ringNameEdText.getText().toString());
                                if (OrderOwnRingView.this.ownRingRsp != null && "000000".equals(OrderOwnRingView.this.ownRingRsp.getResCode())) {
                                    OrderOwnRingView.this.textListenUrl = OrderOwnRingView.this.ownRingRsp.getListenUrl();
                                    OrderOwnRingView.this.mHandler.post(new Runnable() {
                                        public void run() {
                                            OrderOwnRingView.this.initViews(2);
                                        }
                                    });
                                } else if (OrderOwnRingView.this.ownRingRsp != null) {
                                    OrderOwnRingView.this.mHandler.post(new Runnable() {
                                        public void run() {
                                            OrderOwnRingView.this.initViews(4);
                                        }
                                    });
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            } finally {
                                OrderOwnRingView.mCurActivity.hideProgressBar();
                            }
                        }
                    }).start();
                    return;
                }
            case 1:
                if (this.ringNameEdText.getText().toString().trim().length() == 0) {
                    mCurActivity.hideProgressBar();
                    this.ringNameEdText.requestFocus();
                    this.ringNameEdText.setError(Html.fromHtml("<font color='red'>请输入铃音名称</font>"));
                    return;
                }
                String editable = this.edSignCode.getText().toString();
                if (editable == null || editable.length() <= 0) {
                    mCurActivity.showToast("请选择文件！");
                    mCurActivity.hideProgressBar();
                    return;
                }
                final File file = new File(editable);
                if (!file.exists()) {
                    mCurActivity.hideProgressBar();
                    mCurActivity.showToast("请选择文件！");
                    return;
                }
                new Thread(new Runnable() {
                    public void run() {
                        try {
                            Log.d(OrderOwnRingView.TAG, file + "上传文件");
                            OrderOwnRingView.this.ownRingRsp = EnablerInterface.getMp3OwnRingRsp(OrderOwnRingView.mCurActivity, file, OrderOwnRingView.this.ringNameEdText.getText().toString(), OrderOwnRingView.this.ringTypeGroup.getCheckedRadioButtonId());
                            if (OrderOwnRingView.this.ownRingRsp != null && "000000".equals(OrderOwnRingView.this.ownRingRsp.getResCode())) {
                                OrderOwnRingView.this.mp3ListenUrl = OrderOwnRingView.this.ownRingRsp.getListenUrl();
                                OrderOwnRingView.this.mHandler.post(new Runnable() {
                                    public void run() {
                                        OrderOwnRingView.this.initViews(3);
                                    }
                                });
                            } else if (OrderOwnRingView.this.ownRingRsp != null) {
                                OrderOwnRingView.this.mHandler.post(new Runnable() {
                                    public void run() {
                                        OrderOwnRingView.this.initViews(5);
                                    }
                                });
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {
                            OrderOwnRingView.mCurActivity.hideProgressBar();
                        }
                    }
                }).start();
                return;
            case 2:
            case 3:
                new Thread(new Runnable() {
                    public void run() {
                        try {
                            OrderOwnRingView.this.isOwnRingOrderMonthUserResult = EnablerInterface.getIsOwnRingOrderMonthUserRsp(OrderOwnRingView.mCurActivity);
                            if (OrderOwnRingView.this.isOwnRingOrderMonthUserResult == null) {
                                OrderOwnRingView.mCurActivity.showToast(OrderOwnRingView.this.isOwnRingOrderMonthUserResult.getResMsg());
                            } else if ("000000".equals(OrderOwnRingView.this.isOwnRingOrderMonthUserResult.getResCode())) {
                                OrderOwnRingView.this.mHandler.post(new Runnable() {
                                    public void run() {
                                        OrderOwnRingView.this.initViews(15);
                                    }
                                });
                            } else if ("300006".equals(OrderOwnRingView.this.isOwnRingOrderMonthUserResult.getResCode())) {
                                OrderOwnRingView.this.mHandler.post(new Runnable() {
                                    public void run() {
                                        OrderOwnRingView.this.initViews(7);
                                    }
                                });
                            } else {
                                OrderOwnRingView.mCurActivity.showToast(OrderOwnRingView.this.isOwnRingOrderMonthUserResult.getResMsg());
                            }
                            try {
                            } catch (Exception e) {
                                e.printStackTrace();
                                return;
                            } finally {
                                OrderOwnRingView.mCurActivity.hideProgressBar();
                            }
                        } catch (IOException e2) {
                            e2.printStackTrace();
                            OrderOwnRingView.mCurActivity.hideProgressBar();
                        } catch (XmlPullParserException e3) {
                            e3.printStackTrace();
                            OrderOwnRingView.mCurActivity.hideProgressBar();
                        } catch (Throwable th) {
                            OrderOwnRingView.mCurActivity.hideProgressBar();
                            throw th;
                        }
                        OrderOwnRingView.mCurActivity.hideProgressBar();
                    }
                }).start();
                stop();
                return;
            case 4:
                initViews(0);
                mCurActivity.hideProgressBar();
                return;
            case 5:
                initViews(1);
                mCurActivity.hideProgressBar();
                return;
            case 6:
                mCurActivity.closeActivity(null);
                mCurActivity.unregisterReceiver(this.receiver);
                mCurActivity.hideProgressBar();
                return;
            case 7:
                EnablerInterface.orderOwnRingMonth(mCurActivity, this.monthType, new CMMusicCallback<OrderResult>() {
                    public void operationResult(OrderResult orderResult) {
                        try {
                            if ("000000".equals(orderResult.getResCode())) {
                                OrderOwnRingView.this.mode = 2;
                                OrderOwnRingView.this.sureClicked();
                            } else if ("300009".equals(orderResult.getResCode())) {
                                OrderOwnRingView.mCurActivity.closeActivity(orderResult);
                            } else {
                                OrderOwnRingView.mCurActivity.closeActivity(orderResult);
                            }
                        } finally {
                            OrderOwnRingView.mCurActivity.hideProgressBar();
                        }
                    }
                });
                return;
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 17:
            default:
                return;
            case 15:
                Log.i("OrderRingParam", "铃音ID = " + this.ownRingRsp.getCrbtId());
                EnablerInterface.orderOwnRingback(mCurActivity, this.ownRingRsp.getCrbtId(), new CMMusicCallback<OrderResult>() {
                    public void operationResult(OrderResult orderResult) {
                        OrderOwnRingView.this.openOwnRingResult = orderResult;
                        if (OrderOwnRingView.this.openOwnRingResult.getResCode() == null) {
                            OrderOwnRingView.mCurActivity.showToast("请重试！");
                        } else if ("000000".equals(OrderOwnRingView.this.openOwnRingResult.getResCode())) {
                            OrderOwnRingView.this.initViews(18);
                        } else {
                            OrderOwnRingView.mCurActivity.showToast(OrderOwnRingView.this.openOwnRingResult.getResMsg());
                        }
                    }
                });
                return;
            case 16:
                mCurActivity.showProgressBar("请稍候...");
                new Thread(new Runnable() {
                    public void run() {
                        try {
                            OrderOwnRingView.this.returnOwnRingMonthResult = EnablerInterface.getReturnOwnRingMonthRsp(OrderOwnRingView.mCurActivity);
                            if (OrderOwnRingView.this.returnOwnRingMonthResult == null) {
                                OrderOwnRingView.mCurActivity.showToast(OrderOwnRingView.this.returnOwnRingMonthResult.getResMsg());
                            } else if ("000000".equals(OrderOwnRingView.this.returnOwnRingMonthResult.getResCode())) {
                                OrderOwnRingView.this.mode = 2;
                                OrderOwnRingView.this.sureClicked();
                            } else {
                                OrderOwnRingView.mCurActivity.showToast(OrderOwnRingView.this.returnOwnRingMonthResult.getResMsg());
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (XmlPullParserException e2) {
                            e2.printStackTrace();
                        } finally {
                            OrderOwnRingView.mCurActivity.hideProgressBar();
                        }
                    }
                }).start();
                return;
            case 18:
                mCurActivity.hideProgressBar();
                mCurActivity.closeActivity(this.openOwnRingResult);
                return;
        }
    }

    /* access modifiers changed from: private */
    public String getSpinnerSelectMusicId() {
        String str = new String(new StringBuilder(String.valueOf(this.musicspinner.getSelectedItemPosition() + 1)).toString());
        StringBuilder sb = new StringBuilder();
        if (str.length() == 1) {
            sb.append("0000");
            sb.append(str);
        } else if (str.length() == 2) {
            sb.append("000");
            sb.append(str);
        }
        return sb.toString();
    }

    /* access modifiers changed from: protected */
    public void cancelClicked() {
        if (this.ownRingListenMediaPlayer != null) {
            this.ownRingListenMediaPlayer.stop();
            this.ownRingListenMediaPlayer.release();
            this.ownRingListenMediaPlayer = null;
        }
        stop();
        mCurActivity.closeActivity(null);
        mCurActivity.unregisterReceiver(this.receiver);
    }

    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        switch (i) {
            case 55:
                initViews(0);
                return;
            case 56:
                initViews(1);
                return;
            case THREE_YUAN_MONTHLY /*65*/:
                this.monthType = "3";
                return;
            case FIVE_YUAN_MONTHLY /*66*/:
                this.monthType = "5";
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public void ringListen(final String str) {
        mCurActivity.showProgressBar("请稍候...");
        new Thread(new Runnable() {
            public void run() {
                try {
                    Log.i("OrderOwnRingView_ownRingListenRsp", "个性化铃音彩铃试听地址url = " + str);
                    if (str == null || str.length() <= 0) {
                        OrderOwnRingView.mCurActivity.showToast("铃音合成失败了，请联系管理员！");
                        return;
                    }
                    OrderOwnRingView.this.ownRingListenMediaPlayer = new MediaPlayer();
                    OrderOwnRingView.this.ownRingListenMediaPlayer.reset();
                    OrderOwnRingView.this.ownRingListenMediaPlayer.setDataSource(str);
                    OrderOwnRingView.this.ownRingListenMediaPlayer.prepareAsync();
                    OrderOwnRingView.this.ownRingListenMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        public void onPrepared(MediaPlayer mediaPlayer) {
                            OrderOwnRingView.mCurActivity.hideProgressBar();
                            OrderOwnRingView.this.ownRingListenMediaPlayer.start();
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (IllegalArgumentException e2) {
                    e2.printStackTrace();
                } catch (SecurityException e3) {
                    e3.printStackTrace();
                } catch (IllegalStateException e4) {
                    e4.printStackTrace();
                }
            }
        }).start();
    }

    /* access modifiers changed from: private */
    public void stop() {
        try {
            InputStream open = mCurActivity.getAssets().open("img_playback_bt_play.png");
            BitmapDrawable bitmapDrawable = new BitmapDrawable(BitmapFactory.decodeStream(open));
            open.close();
            if (this.playAndPauseButton != null) {
                this.playAndPauseButton.setImageDrawable(bitmapDrawable);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Intent intent = new Intent();
        intent.setAction(MediaService.ACTION);
        intent.putExtra("mean", MediaService.STOP);
        mCurActivity.sendBroadcast(intent);
        this.currentPlayStatus = 0;
    }

    public static String durationToString(String str) {
        String str2 = "";
        int intValue = Integer.valueOf(str).intValue();
        int i = intValue / Constants.CLEARIMGED;
        int i2 = i / 60;
        int i3 = i % 60;
        if (i2 > 9) {
            if (i3 > 9) {
                str2 = String.valueOf(i2) + ":" + i3;
            }
            if (i3 <= 9) {
                str2 = String.valueOf(i2) + ":0" + i3;
            }
        } else {
            if (i3 > 9) {
                str2 = "0" + i2 + ":" + i3;
            }
            if (i3 <= 9) {
                str2 = "0" + i2 + ":0" + i3;
            }
        }
        seekBar.setMax(intValue);
        return str2;
    }

    /* access modifiers changed from: protected */
    public int getScreenHeightDip() {
        int i = mCurActivity.getResources().getDisplayMetrics().heightPixels;
        Log.d("getScreenHeightDip=", new StringBuilder().append(i).toString());
        return i;
    }

    private LinearLayout initLogoView() {
        LinearLayout linearLayout = new LinearLayout(mCurActivity);
        linearLayout.setId(299);
        linearLayout.setPadding(0, 5, 0, 0);
        linearLayout.setGravity(17);
        linearLayout.setOrientation(1);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams.addRule(10);
        linearLayout.setLayoutParams(layoutParams);
        try {
            InputStream open = mCurActivity.getAssets().open("logo.png");
            BitmapDrawable bitmapDrawable = new BitmapDrawable(BitmapFactory.decodeStream(open));
            open.close();
            ImageView imageView = new ImageView(mCurActivity);
            imageView.setImageDrawable(bitmapDrawable);
            imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
            imageView.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
            linearLayout.addView(imageView);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return linearLayout;
    }

    private void initBtnView(Context context) {
        this.btnView = new LinearLayout(context);
        this.btnView.setOrientation(0);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams.addRule(12);
        this.btnView.setLayoutParams(layoutParams);
        this.btnSure = new Button(context);
        this.btnSure.setText("确定");
        this.btnSure.setLayoutParams(new LinearLayout.LayoutParams(-2, -2, 0.5f));
        this.btnView.addView(this.btnSure);
        this.btnSure.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                OrderOwnRingView.this.sureClicked();
            }
        });
        this.btnCancel = new Button(context);
        this.btnCancel.setText("取消");
        this.btnCancel.setLayoutParams(new LinearLayout.LayoutParams(-2, -2, 0.5f));
        this.btnView.addView(this.btnCancel);
        this.btnCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Log.d(getClass().getSimpleName(), "cancel button clicked");
                OrderOwnRingView.this.cancelClicked();
            }
        });
        addView(this.btnView);
    }
}
