package com.cmsc.cmmusic.common;

import android.content.Context;
import android.os.Bundle;
import com.cmsc.cmmusic.common.MiguSdkUtil;
import com.cmsc.cmmusic.common.data.DownloadResult;
import com.cmsc.cmmusic.common.data.OrderResult;
import java.io.IOException;
import java.io.InputStream;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

public class VibrateRingManagerInterface {
    public static void queryVibrateRingDownloadUrl(Context context, String str, CMMusicCallback<OrderResult> cMMusicCallback) {
        Bundle bundle = new Bundle();
        bundle.putString("MusicId", str);
        CMMusicActivity.showActivityDefault(context, bundle, 3, cMMusicCallback);
    }

    public static void getVibrateRingDownloadUrl(Context context, String str, String str2, String str3, String str4, String str5, String str6, String str7, CMMusicCallback<OrderResult> cMMusicCallback) {
        MiguSdkUtil.TL.init();
        EnablerInterface.getVibrateRingDownloadUrl(context, str, str2, str3, str4, str5, str6, str7, cMMusicCallback);
    }

    public static String getVibrateRingPolicy(Context context, String str) {
        try {
            return HttpPostCore.httpConnectionToString(context, "http://218.200.227.123:95/sdkServer/1.0/ring/policy", EnablerInterface.buildGetVibrateRingPolicyRequsetXml(str));
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void giveVibrateRing(Context context, String str, String str2, String str3, String str4, String str5, String str6, String str7, CMMusicCallback<OrderResult> cMMusicCallback) {
        EnablerInterface.giveSong(context, "http://218.200.227.123:95/sdkServer/1.0/ring/present", str, EnablerInterface.getVibrateRing12Id(str2), str3, str4, str5, str6, str7, cMMusicCallback);
    }

    public static DownloadResult getRingPrelisten(Context context, String str) {
        try {
            return getRingPrelisten(HttpPostCore.httpConnection(context, "http://218.200.227.123:95/sdkServer/1.0/ring/query", EnablerInterface.buildRequsetXml("<musicId>" + str + "</musicId>")));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static DownloadResult getRingPrelisten(InputStream inputStream) throws XmlPullParserException, IOException {
        if (inputStream == null) {
            return null;
        }
        DownloadResult downloadResult = new DownloadResult();
        try {
            XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
            newPullParser.setInput(inputStream, "UTF-8");
            for (int eventType = newPullParser.getEventType(); 1 != eventType; eventType = newPullParser.next()) {
                String name = newPullParser.getName();
                switch (eventType) {
                    case 2:
                        if (!name.equalsIgnoreCase("resCode")) {
                            if (!name.equalsIgnoreCase("resMsg")) {
                                if (!name.equalsIgnoreCase("streamUrl")) {
                                    break;
                                } else {
                                    downloadResult.setDownUrl(newPullParser.nextText());
                                    break;
                                }
                            } else {
                                downloadResult.setResMsg(newPullParser.nextText());
                                break;
                            }
                        } else {
                            downloadResult.setResCode(newPullParser.nextText());
                            break;
                        }
                }
            }
            if (inputStream == null) {
                return downloadResult;
            }
            try {
                return downloadResult;
            } catch (IOException e) {
                return downloadResult;
            }
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e2) {
                }
            }
        }
    }
}
