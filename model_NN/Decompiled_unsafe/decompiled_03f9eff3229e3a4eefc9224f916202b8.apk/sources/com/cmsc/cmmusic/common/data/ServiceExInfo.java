package com.cmsc.cmmusic.common.data;

import java.io.Serializable;

public class ServiceExInfo implements Serializable {
    String appName;
    String channelSubName;
    String contentId;
    private String copyrightName;
    private String copyright_id;
    private String cpId;
    private String cpName;
    private String monLevel;
    String noticeAfter;
    String noticeBefore;
    String phoneNum;
    String price;
    String prodType;
    String resCode;
    private String serviceId;
    private String serviceName;

    public String getCopyrightName() {
        return this.copyrightName;
    }

    public void setCopyrightName(String str) {
        this.copyrightName = str;
    }

    public String getPhoneNum() {
        return this.phoneNum;
    }

    public void setPhoneNum(String str) {
        this.phoneNum = str;
    }

    public String getServiceId() {
        return this.serviceId;
    }

    public void setServiceId(String str) {
        this.serviceId = str;
    }

    public String getCpId() {
        return this.cpId;
    }

    public void setCpId(String str) {
        this.cpId = str;
    }

    public String getCpName() {
        return this.cpName;
    }

    public void setCpName(String str) {
        this.cpName = str;
    }

    public String getServiceName() {
        return this.serviceName;
    }

    public void setServiceName(String str) {
        this.serviceName = str;
    }

    public String getCopyright_id() {
        return this.copyright_id;
    }

    public void setCopyright_id(String str) {
        this.copyright_id = str;
    }

    public String getProdType() {
        return this.prodType;
    }

    public void setProdType(String str) {
        this.prodType = str;
    }

    public String getContentId() {
        return this.contentId;
    }

    public void setContentId(String str) {
        this.contentId = str;
    }

    public String getChannelSubName() {
        return this.channelSubName;
    }

    public void setChannelSubName(String str) {
        this.channelSubName = str;
    }

    public String getAppName() {
        return this.appName;
    }

    public void setAppName(String str) {
        this.appName = str;
    }

    public String getPrice() {
        return this.price;
    }

    public void setPrice(String str) {
        this.price = str;
    }

    public String getNoticeBefore() {
        return this.noticeBefore;
    }

    public void setNoticeBefore(String str) {
        this.noticeBefore = str;
    }

    public String getNoticeAfter() {
        return this.noticeAfter;
    }

    public void setNoticeAfter(String str) {
        this.noticeAfter = str;
    }

    public String getResCode() {
        return this.resCode;
    }

    public void setResCode(String str) {
        this.resCode = str;
    }

    public String getMonLevel() {
        return this.monLevel;
    }

    public void setMonLevel(String str) {
        this.monLevel = str;
    }

    public String toString() {
        return "ServiceExInfo [serviceId=" + this.serviceId + ", cpId=" + this.cpId + ", cpName=" + this.cpName + ", serviceName=" + this.serviceName + ", copyright_id=" + this.copyright_id + ", copyrightName=" + this.copyrightName + ", monLevel=" + this.monLevel + ", prodType=" + this.prodType + ", contentId=" + this.contentId + ", channelSubName=" + this.channelSubName + ", appName=" + this.appName + ", price=" + this.price + ", noticeBefore=" + this.noticeBefore + ", noticeAfter=" + this.noticeAfter + ", phoneNum=" + this.phoneNum + ", resCode=" + this.resCode + "]";
    }
}
