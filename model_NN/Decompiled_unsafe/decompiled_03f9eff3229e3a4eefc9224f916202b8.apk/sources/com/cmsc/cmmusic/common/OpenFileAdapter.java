package com.cmsc.cmmusic.common;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.view.ViewCompat;
import android.text.format.Formatter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.io.File;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class OpenFileAdapter extends BaseAdapter {
    public static final String DATA_CSV_FILE_SUFFIX = "CSV";
    public static final String DATA_RECODER_FILE_SUFFIX = "DAT";
    public static final String DATA_TXT_FILE_SUFFIX = "TXT";
    public static final String DATA_WAVE_JPG = "JPG";
    public static final String DATA_WRITE_FILE_SUFFIX = "BIN";
    private ViewHolder holder;
    private Context mContext;
    private TextView mFileChangTimeTxtView;
    private List<File> mFileList = null;
    private TextView mFileNameTextView;
    private ImageView mFilePicImgView;
    private TextView mFileSizeTxtView;

    private static class ViewHolder {
        TextView mFileChangTimeTxtView;
        TextView mFileNameTextView;
        ImageView mFilePicImgView;
        TextView mFileSizeTxtView;

        private ViewHolder() {
        }

        /* synthetic */ ViewHolder(ViewHolder viewHolder) {
            this();
        }
    }

    public OpenFileAdapter(Context context, List<File> list) {
        this.mContext = context;
        this.mFileList = list;
    }

    public int getCount() {
        return this.mFileList.size();
    }

    public Object getItem(int i) {
        return Integer.valueOf(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    public View getView(int i, View view, ViewGroup viewGroup) {
        LinearLayout linearLayout;
        File file = this.mFileList.get(i);
        String upperCase = file.getName().toUpperCase();
        if (view == null) {
            LinearLayout linearLayout2 = new LinearLayout(this.mContext);
            linearLayout2.setOrientation(0);
            linearLayout2.setGravity(16);
            linearLayout2.setLayoutParams(new AbsListView.LayoutParams(-1, 70));
            linearLayout2.setPadding(5, 5, 5, 5);
            this.mFilePicImgView = new ImageView(this.mContext);
            this.mFilePicImgView.setLayoutParams(new LinearLayout.LayoutParams(40, 40));
            LinearLayout linearLayout3 = new LinearLayout(this.mContext);
            linearLayout3.setOrientation(1);
            linearLayout3.setLayoutParams(new LinearLayout.LayoutParams(490, 65));
            this.mFileNameTextView = new TextView(this.mContext);
            this.mFileNameTextView.setLayoutParams(new LinearLayout.LayoutParams(490, 30));
            this.mFileNameTextView.setTextColor((int) ViewCompat.MEASURED_STATE_MASK);
            linearLayout3.addView(this.mFileNameTextView);
            this.mFileChangTimeTxtView = new TextView(this.mContext);
            this.mFileChangTimeTxtView.setLayoutParams(new LinearLayout.LayoutParams(490, 25));
            linearLayout3.addView(this.mFileChangTimeTxtView);
            this.mFileSizeTxtView = new TextView(this.mContext);
            this.mFileSizeTxtView.setLayoutParams(new LinearLayout.LayoutParams(50, 40));
            linearLayout2.addView(this.mFilePicImgView);
            linearLayout2.addView(linearLayout3);
            linearLayout2.addView(this.mFileSizeTxtView);
            this.holder = new ViewHolder(null);
            this.holder.mFileNameTextView = this.mFileNameTextView;
            this.holder.mFileChangTimeTxtView = this.mFileChangTimeTxtView;
            this.holder.mFilePicImgView = this.mFilePicImgView;
            this.holder.mFileSizeTxtView = this.mFileSizeTxtView;
            linearLayout2.setTag(this.holder);
            linearLayout = linearLayout2;
        } else {
            this.holder = (ViewHolder) view.getTag();
            linearLayout = view;
        }
        this.holder.mFileNameTextView.setText(file.getName());
        this.holder.mFileChangTimeTxtView.setText(getStrTime(file.lastModified()));
        this.holder.mFileSizeTxtView.setText(Formatter.formatFileSize(this.mContext, file.length()));
        if (upperCase.endsWith(DATA_RECODER_FILE_SUFFIX)) {
            this.holder.mFilePicImgView.setBackgroundDrawable(getDrawable("openfile_file_icon_dat.png"));
        } else if (upperCase.endsWith(DATA_WAVE_JPG)) {
            this.holder.mFilePicImgView.setBackgroundDrawable(getDrawable("openfile_file_icon_pic.png"));
        } else if (upperCase.endsWith(DATA_WRITE_FILE_SUFFIX)) {
            this.holder.mFilePicImgView.setBackgroundDrawable(getDrawable("openfile_file_icon_bin.png"));
        } else if (upperCase.endsWith(DATA_CSV_FILE_SUFFIX)) {
            this.holder.mFilePicImgView.setBackgroundDrawable(getDrawable("openfile_file_icon_csv.png"));
        } else if (upperCase.endsWith(DATA_TXT_FILE_SUFFIX)) {
            this.holder.mFilePicImgView.setBackgroundDrawable(getDrawable("openfile_file_icon_csv.png"));
        } else if (file.isDirectory()) {
            this.holder.mFileSizeTxtView.setText("");
            this.holder.mFilePicImgView.setBackgroundDrawable(getDrawable("folder_pic.png"));
            this.holder.mFileSizeTxtView.setBackgroundDrawable(getDrawable("folder_arrow.png"));
        } else {
            this.holder.mFilePicImgView.setBackgroundDrawable(getDrawable("openfile_file_icon_dat.png"));
        }
        return linearLayout;
    }

    private String getStrTime(long j) {
        return new SimpleDateFormat("yyyy/MM/dd HH:mm").format(new Date(j));
    }

    private Drawable getDrawable(String str) {
        Exception e;
        BitmapDrawable bitmapDrawable;
        try {
            InputStream open = this.mContext.getAssets().open(str);
            bitmapDrawable = new BitmapDrawable(BitmapFactory.decodeStream(open));
            try {
                open.close();
            } catch (Exception e2) {
                e = e2;
                e.printStackTrace();
                return bitmapDrawable;
            }
        } catch (Exception e3) {
            Exception exc = e3;
            bitmapDrawable = null;
            e = exc;
            e.printStackTrace();
            return bitmapDrawable;
        }
        return bitmapDrawable;
    }
}
