package com.cmsc.cmmusic.common.data;

public class QueryResult extends Result {
    private String mobile;
    private String name;

    public QueryResult() {
    }

    public QueryResult(Result result) {
        if (result != null) {
            setResCode(result.getResCode());
            setResMsg(result.getResMsg());
        }
    }

    public String getMobile() {
        return this.mobile;
    }

    public void setMobile(String str) {
        this.mobile = str;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String str) {
        this.name = str;
    }

    public String toString() {
        return "QueryResult [mobile=" + this.mobile + ", name=" + this.name + ", getResCode()=" + getResCode() + ", getResMsg()=" + getResMsg() + "]";
    }
}
