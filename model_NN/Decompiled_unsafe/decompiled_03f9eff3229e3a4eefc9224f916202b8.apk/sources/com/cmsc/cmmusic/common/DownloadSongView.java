package com.cmsc.cmmusic.common;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import cn.banshenggua.aichang.utils.Constants;
import com.cmsc.cmmusic.common.data.BizInfo;
import com.cmsc.cmmusic.common.data.OrderPolicy;
import com.cmsc.cmmusic.common.data.SongOpenPolicy;
import java.util.HashMap;

abstract class DownloadSongView extends OrderView {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$cmsc$cmmusic$common$data$OrderPolicy$OrderType = null;
    private static final String LOG_TAG = "DownloadSongView";
    private Button openMonButton;
    private RadioGroup rdGroup;
    private HashMap<String, BizInfo> resourceMap;
    private TextView txtMonRemind;

    /* access modifiers changed from: protected */
    public abstract void downloadSong(BizInfo bizInfo, String str);

    /* access modifiers changed from: protected */
    public abstract void showOrderPolicyView();

    static /* synthetic */ int[] $SWITCH_TABLE$com$cmsc$cmmusic$common$data$OrderPolicy$OrderType() {
        int[] iArr = $SWITCH_TABLE$com$cmsc$cmmusic$common$data$OrderPolicy$OrderType;
        if (iArr == null) {
            iArr = new int[OrderPolicy.OrderType.values().length];
            try {
                iArr[OrderPolicy.OrderType.net.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[OrderPolicy.OrderType.sms.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[OrderPolicy.OrderType.verifyCode.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$com$cmsc$cmmusic$common$data$OrderPolicy$OrderType = iArr;
        }
        return iArr;
    }

    public DownloadSongView(Context context, Bundle bundle) {
        super(context, bundle);
    }

    /* access modifiers changed from: protected */
    public void initContentView(LinearLayout linearLayout) {
        this.rdGroup = new RadioGroup(this.mCurActivity);
        RadioButton radioButton = new RadioButton(this.mCurActivity);
        radioButton.setText("标清版（40kbps）");
        radioButton.setId(100);
        radioButton.setChecked(true);
        radioButton.setVisibility(8);
        RadioButton radioButton2 = new RadioButton(this.mCurActivity);
        radioButton2.setText("高清版（128kbps）");
        radioButton2.setId(Constants.RESULT_OK);
        radioButton2.setVisibility(8);
        RadioButton radioButton3 = new RadioButton(this.mCurActivity);
        radioButton3.setText("杜比高清版");
        radioButton3.setId(Constants.REGISTER_OK);
        radioButton3.setVisibility(8);
        this.rdGroup.addView(radioButton);
        this.rdGroup.addView(radioButton2);
        this.rdGroup.addView(radioButton3);
        linearLayout.addView(this.rdGroup);
        linearLayout.addView(getMemInfoView());
        linearLayout.addView(getPriceView());
        this.txtMonRemind = new TextView(this.mCurActivity);
        this.txtMonRemind.setTextAppearance(this.mCurActivity, 16973892);
        this.txtMonRemind.setVisibility(8);
        linearLayout.addView(this.txtMonRemind);
    }

    /* access modifiers changed from: protected */
    public void updateNetView() {
        if (this.policyObj.getBizInfos() != null) {
            this.resourceMap = this.policyObj.getDownloadResourceMap();
            showOrderPolicyView();
        }
        if ("3".equals(this.policyObj.getMonLevel())) {
            if (this.txtMonRemind != null) {
                this.txtMonRemind.setVisibility(8);
                this.contentView.removeView(this.txtMonRemind);
            }
            if (this.specMemInfoView != null) {
                this.specMemInfoView.setVisibility(8);
                this.contentView.removeView(this.specMemInfoView);
            }
        } else {
            this.contentView.addView(getSpecMemInfoView());
            updateSpecMemInfoView("推荐：开通咪咕特级会员专享0元无限量下载。");
        }
        Log.d(LOG_TAG, "orderType : " + this.orderType + " , songName : " + this.curSongName + " , singerName : " + this.curSingerName);
    }

    /* access modifiers changed from: protected */
    public void setupRadioButton(int i, String str, String str2) {
        String str3;
        RadioButton radioButton = (RadioButton) this.rdGroup.findViewById(i);
        radioButton.setTag(str);
        if (this.resourceMap.containsKey(str)) {
            BizInfo bizInfo = this.resourceMap.get(str);
            if (!com.tencent.connect.common.Constants.VIA_ACT_TYPE_TWENTY_EIGHT.equals(bizInfo.getBizType())) {
                str3 = String.valueOf(EnablerInterface.getPriceString(bizInfo.getSalePrice())) + "  ";
            } else {
                str3 = "无法下载！请先订购该歌曲所属数字专辑";
            }
            radioButton.setText(String.valueOf(str2) + "/" + str3);
            radioButton.setVisibility(0);
            return;
        }
        radioButton.setVisibility(8);
    }

    /* access modifiers changed from: protected */
    public void setupMonTip(String str) {
        String restTimes = this.policyObj.getRestTimes();
        Logger.i("TAG", "restTimes = " + restTimes);
        if (restTimes != null && !"".equals(restTimes)) {
            this.txtMonRemind.setText(String.valueOf(str) + restTimes + "首歌曲");
            this.txtMonRemind.setVisibility(0);
            this.openMonButton.setVisibility(8);
        }
    }

    /* access modifiers changed from: protected */
    public BizInfo getBizInfoByResource(String str) {
        return this.resourceMap.get(str);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    public void sureClicked() {
        String str;
        Log.d(LOG_TAG, "sure button clicked");
        try {
            switch ($SWITCH_TABLE$com$cmsc$cmmusic$common$data$OrderPolicy$OrderType()[this.orderType.ordinal()]) {
                case 1:
                    int checkedRadioButtonId = this.rdGroup.getCheckedRadioButtonId();
                    switch (checkedRadioButtonId) {
                        case 100:
                            str = "0";
                            break;
                        case Constants.RESULT_OK:
                            str = "1";
                            break;
                        case Constants.REGISTER_OK:
                            str = "2";
                            break;
                        default:
                            str = "";
                            break;
                    }
                    BizInfo bizInfoByResource = getBizInfoByResource((String) this.rdGroup.findViewById(checkedRadioButtonId).getTag());
                    if (bizInfoByResource != null) {
                        downloadSong(bizInfoByResource, str);
                        return;
                    }
                    return;
                case 2:
                    Log.d(LOG_TAG, "Get download url by sms");
                    this.mCurActivity.closeActivity(null);
                    return;
                case 3:
                    Log.d(LOG_TAG, "Get download url by sign code");
                    return;
                default:
                    return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        e.printStackTrace();
    }

    private LinearLayout getPriceView() {
        LinearLayout linearLayout = new LinearLayout(this.mCurActivity);
        linearLayout.setOrientation(0);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        linearLayout.setVisibility(0);
        this.openMonButton = new Button(this.mCurActivity);
        this.openMonButton.setHeight(30);
        this.openMonButton.setText("开通包月");
        this.openMonButton.setGravity(17);
        this.openMonButton.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
        this.openMonButton.setVisibility(8);
        this.openMonButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                DownloadSongView.this.mCurActivity.showProgressBar("请稍候...");
                new Thread() {
                    public void run() {
                        try {
                            final SongOpenPolicy querySongOpenPolicyByNet = EnablerInterface.querySongOpenPolicyByNet(DownloadSongView.this.mCurActivity);
                            if (querySongOpenPolicyByNet == null || !"000000".equals(querySongOpenPolicyByNet.getResCode())) {
                                DownloadSongView.this.mCurActivity.showToast("请求失败");
                                return;
                            }
                            DownloadSongView.this.mHandler.post(new Runnable() {
                                public void run() {
                                    OpenSongMonthView openSongMonthView = new OpenSongMonthView(DownloadSongView.this.mCurActivity, new Bundle());
                                    DownloadSongView.this.mCurActivity.setContentView(openSongMonthView);
                                    DownloadSongView.this.policyObj.setSongMonthInfos(querySongOpenPolicyByNet.getSongMonthInfos());
                                    openSongMonthView.updateView(DownloadSongView.this.policyObj);
                                }
                            });
                            DownloadSongView.this.mCurActivity.hideProgressBar();
                        } catch (Exception e) {
                            e.printStackTrace();
                            DownloadSongView.this.mCurActivity.showToast("请求失败");
                        } finally {
                            DownloadSongView.this.mCurActivity.hideProgressBar();
                        }
                    }
                }.start();
            }
        });
        linearLayout.addView(this.openMonButton);
        return linearLayout;
    }
}
