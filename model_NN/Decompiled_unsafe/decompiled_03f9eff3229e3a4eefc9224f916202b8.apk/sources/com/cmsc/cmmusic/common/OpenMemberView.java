package com.cmsc.cmmusic.common;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.cmsc.cmmusic.common.data.MVMonthPolicy;
import com.cmsc.cmmusic.common.data.OrderPolicy;
import com.cmsc.cmmusic.common.data.OrderResult;

final class OpenMemberView extends OrderView {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$cmsc$cmmusic$common$data$OrderPolicy$OrderType = null;
    private static final String LOG_TAG = "OpenMemberView";
    private RadioGroup memGroup = new RadioGroup(this.mCurActivity);
    private TextView memPriceTxt;

    static /* synthetic */ int[] $SWITCH_TABLE$com$cmsc$cmmusic$common$data$OrderPolicy$OrderType() {
        int[] iArr = $SWITCH_TABLE$com$cmsc$cmmusic$common$data$OrderPolicy$OrderType;
        if (iArr == null) {
            iArr = new int[OrderPolicy.OrderType.values().length];
            try {
                iArr[OrderPolicy.OrderType.net.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[OrderPolicy.OrderType.sms.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[OrderPolicy.OrderType.verifyCode.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$com$cmsc$cmmusic$common$data$OrderPolicy$OrderType = iArr;
        }
        return iArr;
    }

    public OpenMemberView(Context context, Bundle bundle) {
        super(context, bundle);
        RadioButton radioButton = new RadioButton(this.mCurActivity);
        radioButton.setText("特级会员（6元/月，免彩铃功能费，彩铃7折，免费无限量下载振铃、歌曲）");
        radioButton.setTag("2");
        this.memGroup.addView(radioButton);
        radioButton.setChecked(true);
        this.smsView.addView(this.memGroup);
    }

    /* access modifiers changed from: protected */
    public void initContentView(LinearLayout linearLayout) {
        this.memPriceTxt = new TextView(this.mCurActivity);
        this.memPriceTxt.setTextAppearance(this.mCurActivity, 16973892);
        this.memPriceTxt.setText("会员开通价格：6元/月");
        linearLayout.addView(this.memPriceTxt);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    public void sureClicked() {
        try {
            switch ($SWITCH_TABLE$com$cmsc$cmmusic$common$data$OrderPolicy$OrderType()[this.orderType.ordinal()]) {
                case 1:
                    MiguSdkUtil.order(this.mCurActivity, MiguSdkUtil.buildCpparam(this.mCurActivity, "http://218.200.227.123:95/sdkServer/1.0/user/member/open", EnablerInterface.buildRequsetXml("<type>3</type>")), new CMMusicCallback<OrderResult>() {
                        public void operationResult(OrderResult orderResult) {
                            if (OpenMemberView.this.mCurActivity.getIntent().getIntExtra("ReqType", -1) == 16) {
                                OpenMemberView.this.mCurActivity.popOwnRingView(orderResult);
                            } else if (OpenMemberView.this.mCurActivity.getIntent().getIntExtra("ReqType", -1) == 17) {
                                OpenMvMonthView openMvMonthView = (OpenMvMonthView) OpenMemberView.this.mCurActivity.popView(orderResult);
                                if (openMvMonthView != null && "000000".equals(orderResult.getResCode())) {
                                    if (OpenMemberView.this.policyObj.getUserInfo() != null) {
                                        OpenMemberView.this.policyObj.getUserInfo().setMemLevel("3");
                                    }
                                    MVMonthPolicy mVMonthPolicy = new MVMonthPolicy();
                                    mVMonthPolicy.setResCode(OpenMemberView.this.policyObj.getResCode());
                                    mVMonthPolicy.setResMsg(OpenMemberView.this.policyObj.getResMsg());
                                    mVMonthPolicy.setMobile(OpenMemberView.this.policyObj.getMobile());
                                    mVMonthPolicy.setmVOrderInfos(OpenMemberView.this.policyObj.getmVOrderInfos());
                                    mVMonthPolicy.setUserInfo(OpenMemberView.this.policyObj.getUserInfo());
                                    openMvMonthView.setPolicy(mVMonthPolicy);
                                    openMvMonthView.updateNetView();
                                }
                            } else {
                                OrderView orderView = (OrderView) OpenMemberView.this.mCurActivity.popView(orderResult);
                                if (orderView != null && "000000".equals(orderResult.getResCode())) {
                                    if (OpenMemberView.this.policyObj.getUserInfo() != null) {
                                        OpenMemberView.this.policyObj.getUserInfo().setMemLevel("3");
                                    }
                                    orderView.updateView(OpenMemberView.this.policyObj);
                                }
                            }
                        }
                    });
                    return;
                case 2:
                    Log.d(LOG_TAG, "open member by sms");
                    this.mCurActivity.popView(null);
                    return;
                default:
                    return;
            }
        } catch (Exception e) {
            e.printStackTrace();
            this.mCurActivity.showToast("开通失败");
        }
        e.printStackTrace();
        this.mCurActivity.showToast("开通失败");
    }

    /* access modifiers changed from: protected */
    public void cancelClicked() {
        this.mCurActivity.popView(null);
    }

    /* access modifiers changed from: protected */
    public void updateNetView() {
        this.baseView.setVisibility(8);
        setUserTip("点击”确认“将开通咪咕特级会员功能，开通即可获尊享权益：\n1、音乐免费在线听；\n2、彩铃功能免费享；\n3、来电铃声/标清歌曲免费无限量下载；\n4、订彩铃/赠送音乐7折优惠；\n5、专属铃音盒免费随心选；\n6、会员活动秒杀获取；\n7、咪咕音乐杂志免费看；\n\n");
    }
}
