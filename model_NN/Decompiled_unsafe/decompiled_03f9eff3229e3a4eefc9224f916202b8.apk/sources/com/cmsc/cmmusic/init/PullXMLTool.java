package com.cmsc.cmmusic.init;

import android.util.Log;
import com.cmsc.cmmusic.common.data.Result;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

public class PullXMLTool {
    public static String pull2Result(InputStream inputStream) {
        IOException e;
        String str;
        XmlPullParserException e2;
        String str2 = "";
        try {
            XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
            newPullParser.setInput(inputStream, "UTF-8");
            int eventType = newPullParser.getEventType();
            while (true) {
                int i = eventType;
                str = str2;
                int i2 = i;
                if (1 == i2) {
                    return str;
                }
                try {
                    String name = newPullParser.getName();
                    switch (i2) {
                        case 0:
                            str2 = str;
                            break;
                        case 2:
                            if (name.equalsIgnoreCase("return_code")) {
                                str2 = newPullParser.nextText();
                                break;
                            }
                        case 1:
                        default:
                            str2 = str;
                            break;
                        case 3:
                            str2 = str;
                            break;
                    }
                    eventType = newPullParser.next();
                } catch (XmlPullParserException e3) {
                    e2 = e3;
                } catch (IOException e4) {
                    e = e4;
                    Log.e("SDK_LW_CMM", e.getMessage());
                    return str;
                }
            }
        } catch (XmlPullParserException e5) {
            XmlPullParserException xmlPullParserException = e5;
            str = str2;
            e2 = xmlPullParserException;
        } catch (IOException e6) {
            IOException iOException = e6;
            str = str2;
            e = iOException;
            Log.e("SDK_LW_CMM", e.getMessage());
            return str;
        }
        Log.e("SDK_LW_CMM", e2.getMessage());
        return str;
    }

    public static String pull2ResultDesc(InputStream inputStream) {
        IOException e;
        String str;
        XmlPullParserException e2;
        String str2 = "";
        try {
            XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
            newPullParser.setInput(inputStream, "UTF-8");
            int eventType = newPullParser.getEventType();
            while (true) {
                int i = eventType;
                str = str2;
                int i2 = i;
                if (1 == i2) {
                    return str;
                }
                try {
                    String name = newPullParser.getName();
                    switch (i2) {
                        case 0:
                            str2 = str;
                            break;
                        case 2:
                            if (name.equalsIgnoreCase("return_desc")) {
                                str2 = newPullParser.nextText();
                                break;
                            }
                        case 1:
                        default:
                            str2 = str;
                            break;
                        case 3:
                            str2 = str;
                            break;
                    }
                    eventType = newPullParser.next();
                } catch (XmlPullParserException e3) {
                    e2 = e3;
                } catch (IOException e4) {
                    e = e4;
                    Log.e("SDK_LW_CMM", e.getMessage());
                    return str;
                }
            }
        } catch (XmlPullParserException e5) {
            XmlPullParserException xmlPullParserException = e5;
            str = str2;
            e2 = xmlPullParserException;
        } catch (IOException e6) {
            IOException iOException = e6;
            str = str2;
            e = iOException;
            Log.e("SDK_LW_CMM", e.getMessage());
            return str;
        }
        Log.e("SDK_LW_CMM", e2.getMessage());
        return str;
    }

    public static InputStream byte2InputStream(byte[] bArr) {
        try {
            Log.d("responsBody", new String(bArr, "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return new ByteArrayInputStream(bArr);
    }

    public static Result getResult(InputStream inputStream) throws IOException, XmlPullParserException {
        Result result = new Result();
        if (inputStream != null) {
            try {
                XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
                newPullParser.setInput(inputStream, "UTF-8");
                for (int eventType = newPullParser.getEventType(); 1 != eventType; eventType = newPullParser.next()) {
                    String name = newPullParser.getName();
                    switch (eventType) {
                        case 2:
                            if (!name.equalsIgnoreCase("resCode")) {
                                if (!name.equalsIgnoreCase("resMsg")) {
                                    break;
                                } else {
                                    result.setResMsg(newPullParser.nextText());
                                    break;
                                }
                            } else {
                                result.setResCode(newPullParser.nextText());
                                break;
                            }
                    }
                }
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e) {
                    }
                }
            } catch (Throwable th) {
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e2) {
                    }
                }
                throw th;
            }
        }
        return result;
    }

    static SmsLoginAuthResult getSmsLoginAuthResult(InputStream inputStream) throws IOException, XmlPullParserException {
        if (inputStream == null) {
            return null;
        }
        SmsLoginAuthResult smsLoginAuthResult = new SmsLoginAuthResult();
        try {
            XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
            newPullParser.setInput(inputStream, "UTF-8");
            for (int eventType = newPullParser.getEventType(); 1 != eventType; eventType = newPullParser.next()) {
                String name = newPullParser.getName();
                switch (eventType) {
                    case 2:
                        if (!name.equalsIgnoreCase("resCode")) {
                            if (!name.equalsIgnoreCase("resMsg")) {
                                if (!name.equalsIgnoreCase("token")) {
                                    break;
                                } else {
                                    smsLoginAuthResult.setToken(newPullParser.nextText());
                                    break;
                                }
                            } else {
                                smsLoginAuthResult.setResMsg(newPullParser.nextText());
                                break;
                            }
                        } else {
                            smsLoginAuthResult.setResCode(newPullParser.nextText());
                            break;
                        }
                }
            }
            if (inputStream == null) {
                return smsLoginAuthResult;
            }
            try {
                return smsLoginAuthResult;
            } catch (IOException e) {
                return smsLoginAuthResult;
            }
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e2) {
                }
            }
        }
    }

    static SmsLoginInfoRsp getSmsLoginInfo(InputStream inputStream) throws XmlPullParserException, IOException {
        if (inputStream == null) {
            return null;
        }
        SmsLoginInfoRsp smsLoginInfoRsp = new SmsLoginInfoRsp();
        try {
            XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
            newPullParser.setInput(inputStream, "UTF-8");
            for (int eventType = newPullParser.getEventType(); 1 != eventType; eventType = newPullParser.next()) {
                String name = newPullParser.getName();
                switch (eventType) {
                    case 2:
                        if (!name.equalsIgnoreCase("resCode")) {
                            if (!name.equalsIgnoreCase("resMsg")) {
                                if (!name.equalsIgnoreCase("mobile")) {
                                    break;
                                } else {
                                    smsLoginInfoRsp.setMobile(newPullParser.nextText());
                                    break;
                                }
                            } else {
                                smsLoginInfoRsp.setResMsg(newPullParser.nextText());
                                break;
                            }
                        } else {
                            smsLoginInfoRsp.setResCode(newPullParser.nextText());
                            break;
                        }
                }
            }
            if (inputStream == null) {
                return smsLoginInfoRsp;
            }
            try {
                return smsLoginInfoRsp;
            } catch (IOException e) {
                return smsLoginInfoRsp;
            }
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e2) {
                }
            }
        }
    }
}
