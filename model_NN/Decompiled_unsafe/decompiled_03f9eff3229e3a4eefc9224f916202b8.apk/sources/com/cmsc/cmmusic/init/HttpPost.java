package com.cmsc.cmmusic.init;

import android.content.Context;
import android.util.Log;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public final class HttpPost {
    public static String httpConnection(Context context, String str, String str2, ArrayList<String> arrayList) throws IOException {
        byte[] bytes = str2.getBytes("UTF-8");
        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
        httpURLConnection.setDoOutput(true);
        httpURLConnection.setDoInput(true);
        httpURLConnection.setConnectTimeout(30000);
        httpURLConnection.setReadTimeout(30000);
        httpURLConnection.setUseCaches(false);
        httpURLConnection.setRequestMethod("POST");
        if (GetAppInfo.isTokenExist(context)) {
            setTokenAuthorization(context, httpURLConnection);
        } else if (GetAppInfo.getIMSIbyFile(context).trim().length() != 0) {
            setIMSIAuthorization(context, httpURLConnection);
        } else {
            setAuthorization(context, httpURLConnection);
        }
        httpURLConnection.setRequestProperty("Content-length", new StringBuilder().append(bytes.length).toString());
        httpURLConnection.setRequestProperty("Accept", "*/*");
        httpURLConnection.setRequestProperty("Content-Type", "*/*");
        httpURLConnection.setRequestProperty("Connection", "Keep-Alive");
        httpURLConnection.setRequestProperty("Charset", "UTF-8");
        httpURLConnection.setRequestProperty("Token", PreferenceUtil.getToken(context));
        httpURLConnection.setRequestProperty("excode", GetAppInfo.getexCode(context));
        OutputStream outputStream = httpURLConnection.getOutputStream();
        outputStream.write(bytes);
        outputStream.flush();
        outputStream.close();
        int responseCode = httpURLConnection.getResponseCode();
        Log.i("httpConnection", "responseCode-----" + responseCode);
        if (200 == responseCode) {
            httpURLConnection.getInputStream();
        }
        return null;
    }

    static InputStream httpConnection1(Context context, String str, String str2) throws IOException {
        byte[] bytes = str2.getBytes("UTF-8");
        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
        httpURLConnection.setDoOutput(true);
        httpURLConnection.setDoInput(true);
        httpURLConnection.setConnectTimeout(30000);
        httpURLConnection.setReadTimeout(30000);
        httpURLConnection.setUseCaches(false);
        httpURLConnection.setRequestMethod("POST");
        if (GetAppInfo.isTokenExist(context)) {
            setTokenAuthorization(context, httpURLConnection);
        } else if (GetAppInfo.getIMSIbyFile(context).trim().length() != 0) {
            setIMSIAuthorization(context, httpURLConnection);
        } else {
            setAuthorization(context, httpURLConnection);
        }
        httpURLConnection.setRequestProperty("Content-length", new StringBuilder().append(bytes.length).toString());
        httpURLConnection.setRequestProperty("Accept", "*/*");
        httpURLConnection.setRequestProperty("Content-Type", "*/*");
        httpURLConnection.setRequestProperty("Connection", "Keep-Alive");
        httpURLConnection.setRequestProperty("Charset", "UTF-8");
        httpURLConnection.setRequestProperty("Token", PreferenceUtil.getToken(context));
        httpURLConnection.setRequestProperty("excode", GetAppInfo.getexCode(context));
        OutputStream outputStream = httpURLConnection.getOutputStream();
        outputStream.write(bytes);
        outputStream.flush();
        outputStream.close();
        int responseCode = httpURLConnection.getResponseCode();
        Log.i("httpConnection", "responseCode-----" + responseCode);
        if (200 == responseCode) {
            return httpURLConnection.getInputStream();
        }
        return null;
    }

    private static void setIMSIAuthorization(Context context, HttpURLConnection httpURLConnection) {
        httpURLConnection.addRequestProperty("Authorization", "OEPAUTH realm=\"OEP\",IMSI=\"" + GetAppInfo.getIMSIbyFile(context) + "\",chCode=\"" + GetAppInfoInterface.getChannelCode(context) + "\",appID=\"" + GetAppInfoInterface.getAppid(context) + "\",pubKey=\"" + GetAppInfoInterface.getSign(context) + "\",netMode=\"" + GetAppInfoInterface.getNetMode(context) + "\",packageName=\"" + GetAppInfoInterface.getPackageName(context) + "\",version=\"" + "I1.1" + "\",excode=\"" + GetAppInfo.getexCode(context) + "\"");
    }

    private static void setAuthorization(Context context, HttpURLConnection httpURLConnection) {
        httpURLConnection.addRequestProperty("Authorization", "OEPAUTH realm=\"OEP\",IMSI=\"\",chCode=\"" + GetAppInfoInterface.getChannelCode(context) + "\",appID=\"" + GetAppInfoInterface.getAppid(context) + "\",pubKey=\"" + GetAppInfoInterface.getSign(context) + "\",netMode=\"" + GetAppInfoInterface.getNetMode(context) + "\",packageName=\"" + GetAppInfoInterface.getPackageName(context) + "\",version=\"" + "I1.1" + "\",excode=\"" + GetAppInfo.getexCode(context) + "\"");
    }

    private static void setTokenAuthorization(Context context, HttpURLConnection httpURLConnection) {
        httpURLConnection.addRequestProperty("Authorization", "OEPAUTH realm=\"OEP\",Token=\"" + PreferenceUtil.getToken(context) + "\",chCode=\"" + GetAppInfoInterface.getChannelCode(context) + "\",appID=\"" + GetAppInfoInterface.getAppid(context) + "\",pubKey=\"" + GetAppInfoInterface.getSign(context) + "\",netMode=\"" + GetAppInfoInterface.getNetMode(context) + "\",packageName=\"" + GetAppInfoInterface.getPackageName(context) + "\",version=\"" + "I1.1" + "\",excode=\"" + GetAppInfo.getexCode(context) + "\"");
    }
}
