package com.cmsc.cmmusic.init;

import android.content.Context;
import android.util.Log;
import java.util.Random;
import java.util.regex.Pattern;

public class Utils {
    static String subString(String str) {
        return Pattern.compile("\\s*|\t|\r|\n").matcher(str).replaceAll("");
    }

    public static boolean validatePhoneNumber(String str) {
        return Pattern.compile("^0{0,1}(1)[0-9]{10}$").matcher(str).matches();
    }

    public static String getRandomString(int i) {
        Random random = new Random();
        StringBuilder sb = new StringBuilder();
        for (int i2 = 0; i2 < i; i2++) {
            sb.append("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".charAt(random.nextInt(62)));
        }
        return sb.toString();
    }

    public static String getRandomInt(int i) {
        Random random = new Random();
        StringBuilder sb = new StringBuilder();
        for (int i2 = 0; i2 < i; i2++) {
            sb.append(random.nextInt(10));
        }
        return sb.toString();
    }

    static void smsCount(Context context) {
        int intValue = Constants.countMap.get("initCount").intValue() + 1;
        Log.i("SDK_LW_CMM", "---------------------------------one cycle SMS send count=" + intValue);
        if (intValue == 1) {
            PreferenceUtil.saveCycleBeginTim(context, System.currentTimeMillis());
        }
        Constants.countMap.put("initCount", Integer.valueOf(intValue));
    }

    public static String buildRequsetXml(String str) {
        return "<?xml version='1.0' encoding='UTF-8'?><request>" + str + "</request>";
    }
}
