package com.cmsc.cmmusic.init;

import android.content.Context;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.Log;
import com.tencent.connect.common.Constants;
import com.tencent.open.SocialConstants;
import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.Hashtable;

class InitCmm3 {
    static int counter;
    static boolean flag;

    static native Hashtable<String, String> initCmm2(Context context);

    InitCmm3() {
    }

    static boolean initCheck(Context context) throws IllegalArgumentException, SecurityException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        boolean z;
        String str;
        String str2;
        boolean z2;
        boolean z3;
        boolean z4 = false;
        boolean simInserted = NetMode.simInserted(context, 0);
        boolean simInserted2 = NetMode.simInserted(context, 1);
        Log.i("SDK_LW_CMM", "isInserted sim0:" + simInserted + "_sim1:" + simInserted2);
        if (simInserted || simInserted2) {
            String str3 = "";
            String str4 = "";
            if (!simInserted || (str3 = GetAppInfo.getIMSI(context, 0)) == null || !isCmCard(str3)) {
                z = false;
                str = str3;
            } else {
                Log.i("SDK_LW_CMM", "initCheck sim0_imsi:" + str3);
                z = true;
                str = str3;
            }
            if (!simInserted2 || (str4 = GetAppInfo.getIMSI(context, 1)) == null || !isCmCard(str4)) {
                str2 = str4;
                z2 = false;
            } else {
                Log.i("SDK_LW_CMM", "initCheck sim1_imsi:" + str4);
                str2 = str4;
                z2 = true;
            }
            if (str.equals(str2)) {
                Log.i("SDK_LW_CMM", "sima equals simb conver to singlesim!");
                throw new NoSuchMethodException();
            } else if (z || z2) {
                if (z) {
                    Log.i("SDK_LW_CMM", "which_0;imsi:" + str);
                    z3 = initCheck1(context, 0, str);
                } else {
                    z3 = false;
                }
                if (z2) {
                    Log.i("SDK_LW_CMM", "which_1;imsi:" + str2);
                    z4 = initCheck1(context, 1, str2);
                }
                Log.i("SDK_LW_CMM", "initCheck result: sim0_" + z3 + "-----sim1_" + z4);
                if (z3 || z4) {
                    return true;
                }
                throw new NoSuchMethodException();
            } else {
                Log.i("SDK_LW_CMM", "no CM_SIM");
                throw new NoSuchMethodException();
            }
        } else {
            throw new NoSuchMethodException();
        }
    }

    static boolean initCheck1(Context context, int i, String str) throws NoSuchMethodException {
        if (str == null || "".equals(str)) {
            return false;
        }
        if (new File(XZip.getFilePath(context, i)).exists()) {
            if (GetAppInfo.equals(context, str, XZip.fromZIP(context, i))) {
                Log.i("SDK_LW_CMM", "not need initialize ...");
                return true;
            }
            Log.i("SDK_LW_CMM", "sim is changed");
            return false;
        } else if ("0".equals(InitCmm1.httpUrlConnection(context, NetMode.WIFIorMOBILE(context), "http://218.200.227.123:95/sdkServer/checksmsinitreturn", i).get("code"))) {
            XZip.out2(context, i);
            Log.i("SDK_LW_CMM", "server have pid");
            return true;
        } else {
            Log.i("SDK_LW_CMM", "server have no pid");
            return false;
        }
    }

    static Hashtable<String, String> initCmm(Context context) throws IllegalArgumentException, SecurityException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        String str = Build.MODEL;
        String str2 = Build.VERSION.RELEASE;
        String deviceId = ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
        boolean simInserted = NetMode.simInserted(context, 0);
        boolean simInserted2 = NetMode.simInserted(context, 1);
        String str3 = "";
        String str4 = "";
        if (simInserted && (str3 = GetAppInfo.getIMSI(context, 0)) == null) {
            Log.i("SDK_LW_CMM", "sim1 exist, but null");
            str3 = "";
        }
        if (simInserted2 && (str4 = GetAppInfo.getIMSI(context, 1)) == null) {
            Log.i("SDK_LW_CMM", "sim2 exist, but null");
            str4 = "";
        }
        Log.i("SDK_LW_CMM", "init2 calling");
        Log.i("SDK_LW_CMM", "chCode=" + GetAppInfo.getChannelCode(context) + ", devicemodel=" + str + ", deviceID=" + deviceId + ", release=" + str2 + ", subscriberID=" + str3 + "_" + str4);
        if ("sdk".equals(str)) {
            Log.i("SDK_LW_CMM", "google_sdk...模拟器运行...not apn setting");
        }
        if ("".equals(str3) && "".equals(str4)) {
            throw new NoSuchMethodException();
        } else if (!str3.equals(str4)) {
            return init1(context, str3, str4);
        } else {
            Log.i("SDK_LW_CMM", "sima equals simb conver to singlesim!");
            throw new NoSuchMethodException();
        }
    }

    static Hashtable<String, String> init1(Context context, String str, String str2) throws IllegalArgumentException, SecurityException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        if (!"".equals(str)) {
            if (!"".equals(str2)) {
                Log.i("SDK_LW_CMM", "sim sim");
                Hashtable<String, String> init15 = init15(context, str, 0);
                Hashtable<String, String> init152 = init15(context, str2, 1);
                Log.i("SDK_LW_CMM", "code-0:" + init15.get("code"));
                Log.i("SDK_LW_CMM", "code-1:" + init152.get("code"));
                if ("0".equals(init15.get("code"))) {
                    init15.put("detail", String.valueOf(init15.get("code")) + " " + init152.get("code"));
                    return init15;
                } else if ("0".equals(init152.get("code"))) {
                    init152.put("detail", String.valueOf(init15.get("code")) + " " + init152.get("code"));
                    return init152;
                } else {
                    Hashtable<String, String> hashtable = new Hashtable<>();
                    hashtable.put("code", Constants.VIA_REPORT_TYPE_WPA_STATE);
                    hashtable.put("detail", String.valueOf(init15.get("code")) + " " + init152.get("code"));
                    hashtable.put(SocialConstants.PARAM_APP_DESC, "双卡槽初始化都失败（可从detail字段查询每个卡槽失败的code）");
                    return hashtable;
                }
            } else {
                Log.i("SDK_LW_CMM", "sim null");
                return init15(context, str, 0);
            }
        } else if (!"".equals(str2)) {
            Log.i("SDK_LW_CMM", "null sim");
            return init15(context, str2, 1);
        } else {
            Log.i("SDK_LW_CMM", "null null");
            Hashtable<String, String> hashtable2 = new Hashtable<>();
            hashtable2.put("code", "4");
            hashtable2.put(SocialConstants.PARAM_APP_DESC, "无sim卡");
            return hashtable2;
        }
    }

    static Hashtable<String, String> init15(Context context, String str, int i) throws IllegalArgumentException, SecurityException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        if (!isCmCard(str)) {
            Hashtable<String, String> hashtable = new Hashtable<>();
            hashtable.put("code", "3");
            hashtable.put(SocialConstants.PARAM_APP_DESC, "非中国移动SIM卡");
            return hashtable;
        } else if (!new File(XZip.getFilePath(context, i)).exists()) {
            Hashtable<String, String> httpUrlConnection = InitCmm1.httpUrlConnection(context, NetMode.WIFIorMOBILE(context), "http://218.200.227.123:95/sdkServer/checksmsinitreturn", i);
            if ("0".equals(httpUrlConnection.get("code"))) {
                Log.i("SDK_LW_CMM", "server heve pid");
                XZip.out2(context, i);
                return httpUrlConnection;
            }
            Log.i("SDK_LW_CMM", "server heve no pid, initiating");
            Hashtable<String, String> init2 = init2(context, str, i);
            if (!"0".equals(init2.get("code"))) {
                return init2;
            }
            XZip.out2(context, i);
            Log.i("SDK_LW_CMM", "init success");
            return init2;
        } else if (GetAppInfo.equals(context, str, XZip.fromZIP(context, i))) {
            Log.i("SDK_LW_CMM", "the same file");
            Log.i("SDK_LW_CMM", "init success");
            Hashtable<String, String> hashtable2 = new Hashtable<>();
            hashtable2.put("code", "0");
            hashtable2.put(SocialConstants.PARAM_APP_DESC, "初始化成功");
            return hashtable2;
        } else {
            Log.i("SDK_LW_CMM", "difference file");
            Log.i("SDK_LW_CMM", "sim is changed, initiating");
            Hashtable<String, String> init22 = init2(context, str, i);
            if (!"0".equals(init22.get("code"))) {
                return init22;
            }
            XZip.out2(context, i);
            Log.i("SDK_LW_CMM", "init success");
            return init22;
        }
    }

    static Hashtable<String, String> init2(Context context, String str, int i) throws IllegalArgumentException, SecurityException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        String WIFIorMOBILE = NetMode.WIFIorMOBILE(context);
        if ("CMWAP".equals(WIFIorMOBILE) || "CMNET".equals(WIFIorMOBILE)) {
            if (NetMode.simWhichConnected(context) == i) {
                Log.i("SDK_LW_CMM", "netmode " + WIFIorMOBILE + " on_" + i);
                Hashtable<String, String> initCm = InitCmm1.initCm(context, i);
                if ((initCm == null || !"0".equals(initCm.get("code"))) && GetAppInfo.needSmsInit(context)) {
                    return initSMS(context, WIFIorMOBILE, str, i);
                }
                return initCm;
            } else if (GetAppInfo.needSmsInit(context)) {
                return initSMS(context, WIFIorMOBILE, str, i);
            } else {
                Hashtable<String, String> hashtable = new Hashtable<>();
                hashtable.put("code", Constants.VIA_REPORT_TYPE_MAKE_FRIEND);
                hashtable.put(SocialConstants.PARAM_APP_DESC, "初始化失败");
                return hashtable;
            }
        } else if ("WIFI".equals(WIFIorMOBILE) || "OTHER".equals(WIFIorMOBILE)) {
            Log.i("SDK_LW_CMM", "netmode wifi other " + i);
            if (GetAppInfo.needSmsInit(context)) {
                return initSMS(context, WIFIorMOBILE, str, i);
            }
            Hashtable<String, String> hashtable2 = new Hashtable<>();
            hashtable2.put("code", Constants.VIA_REPORT_TYPE_MAKE_FRIEND);
            hashtable2.put(SocialConstants.PARAM_APP_DESC, "初始化失败");
            return hashtable2;
        } else {
            Log.i("SDK_LW_CMM", "netmode--" + WIFIorMOBILE);
            Hashtable<String, String> hashtable3 = new Hashtable<>();
            hashtable3.put("code", "2");
            hashtable3.put(SocialConstants.PARAM_APP_DESC, "请检查网络连接");
            return hashtable3;
        }
    }

    static Hashtable<String, String> initSMS(Context context, String str, String str2, int i) throws NoSuchMethodException {
        if (Constants.countMap.get("initCount").intValue() >= 3) {
            Hashtable<String, String> hashtable = new Hashtable<>();
            hashtable.put("code", Constants.VIA_SHARE_TYPE_INFO);
            hashtable.put(SocialConstants.PARAM_APP_DESC, "在24小时内短信初始化调用次数不能超过3次。");
            return hashtable;
        }
        InitCmm1.send(context, i);
        Utils.smsCount(context);
        Log.i("SDK_LW_CMM", "sendSMS sleep");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            Log.e("SDK_LW_CMM", e.getMessage());
        }
        flag = false;
        counter = 0;
        Hashtable<String, String> hashtable2 = new Hashtable<>();
        while (!flag) {
            counter++;
            Log.i("SDK_LW_CMM", "initSMS " + counter);
            Hashtable<String, String> httpUrlConnection = InitCmm1.httpUrlConnection(context, str, "http://218.200.227.123:95/sdkServer/checksmsinitreturn", i);
            if ("0".equals(httpUrlConnection.get("code"))) {
                flag = true;
                return httpUrlConnection;
            } else if (counter >= 3) {
                flag = true;
                return httpUrlConnection;
            } else {
                try {
                    Thread.sleep(5000);
                    hashtable2 = httpUrlConnection;
                } catch (InterruptedException e2) {
                    Log.e("SDK_LW_CMM", e2.getMessage());
                    hashtable2 = httpUrlConnection;
                }
            }
        }
        return hashtable2;
    }

    static boolean isCmCard(String str) {
        String substring = str.substring(3, 5);
        if ("00".equals(substring) || "02".equals(substring) || "07".equals(substring)) {
            return true;
        }
        return false;
    }
}
