package com.cmsc.cmmusic.init;

import android.content.Context;
import java.io.BufferedOutputStream;
import java.io.IOException;

public class XZip {
    static native void out1(Context context);

    static native void out2(Context context, int i);

    private static native void write(Context context, BufferedOutputStream bufferedOutputStream, int i) throws IOException;

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0040 A[SYNTHETIC, Splitter:B:14:0x0040] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x005c A[SYNTHETIC, Splitter:B:24:0x005c] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0078 A[SYNTHETIC, Splitter:B:34:0x0078] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x008b A[SYNTHETIC, Splitter:B:41:0x008b] */
    /* JADX WARNING: Removed duplicated region for block: B:54:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:56:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:58:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static void toZIP(android.content.Context r5, int r6) {
        /*
            java.lang.String r0 = getFilePath(r5, r6)
            r2 = 0
            java.io.BufferedOutputStream r1 = new java.io.BufferedOutputStream     // Catch:{ UnsupportedEncodingException -> 0x0033, FileNotFoundException -> 0x004f, IOException -> 0x006b, all -> 0x0087 }
            java.util.zip.GZIPOutputStream r3 = new java.util.zip.GZIPOutputStream     // Catch:{ UnsupportedEncodingException -> 0x0033, FileNotFoundException -> 0x004f, IOException -> 0x006b, all -> 0x0087 }
            java.io.FileOutputStream r4 = new java.io.FileOutputStream     // Catch:{ UnsupportedEncodingException -> 0x0033, FileNotFoundException -> 0x004f, IOException -> 0x006b, all -> 0x0087 }
            r4.<init>(r0)     // Catch:{ UnsupportedEncodingException -> 0x0033, FileNotFoundException -> 0x004f, IOException -> 0x006b, all -> 0x0087 }
            r3.<init>(r4)     // Catch:{ UnsupportedEncodingException -> 0x0033, FileNotFoundException -> 0x004f, IOException -> 0x006b, all -> 0x0087 }
            r1.<init>(r3)     // Catch:{ UnsupportedEncodingException -> 0x0033, FileNotFoundException -> 0x004f, IOException -> 0x006b, all -> 0x0087 }
            java.lang.Runtime r2 = java.lang.Runtime.getRuntime()     // Catch:{ UnsupportedEncodingException -> 0x00ab, FileNotFoundException -> 0x00a9, IOException -> 0x00a7 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ UnsupportedEncodingException -> 0x00ab, FileNotFoundException -> 0x00a9, IOException -> 0x00a7 }
            java.lang.String r4 = "chmod 770 "
            r3.<init>(r4)     // Catch:{ UnsupportedEncodingException -> 0x00ab, FileNotFoundException -> 0x00a9, IOException -> 0x00a7 }
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ UnsupportedEncodingException -> 0x00ab, FileNotFoundException -> 0x00a9, IOException -> 0x00a7 }
            java.lang.String r0 = r0.toString()     // Catch:{ UnsupportedEncodingException -> 0x00ab, FileNotFoundException -> 0x00a9, IOException -> 0x00a7 }
            r2.exec(r0)     // Catch:{ UnsupportedEncodingException -> 0x00ab, FileNotFoundException -> 0x00a9, IOException -> 0x00a7 }
            write(r5, r1, r6)     // Catch:{ UnsupportedEncodingException -> 0x00ab, FileNotFoundException -> 0x00a9, IOException -> 0x00a7 }
            if (r1 == 0) goto L_0x0032
            r1.close()     // Catch:{ IOException -> 0x009a }
        L_0x0032:
            return
        L_0x0033:
            r0 = move-exception
            r1 = r2
        L_0x0035:
            java.lang.String r2 = "SDK_LW_CMM"
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x00a5 }
            android.util.Log.e(r2, r0)     // Catch:{ all -> 0x00a5 }
            if (r1 == 0) goto L_0x0032
            r1.close()     // Catch:{ IOException -> 0x0044 }
            goto L_0x0032
        L_0x0044:
            r0 = move-exception
            java.lang.String r1 = "SDK_LW_CMM"
            java.lang.String r0 = r0.getMessage()
            android.util.Log.e(r1, r0)
            goto L_0x0032
        L_0x004f:
            r0 = move-exception
            r1 = r2
        L_0x0051:
            java.lang.String r2 = "SDK_LW_CMM"
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x00a5 }
            android.util.Log.e(r2, r0)     // Catch:{ all -> 0x00a5 }
            if (r1 == 0) goto L_0x0032
            r1.close()     // Catch:{ IOException -> 0x0060 }
            goto L_0x0032
        L_0x0060:
            r0 = move-exception
            java.lang.String r1 = "SDK_LW_CMM"
            java.lang.String r0 = r0.getMessage()
            android.util.Log.e(r1, r0)
            goto L_0x0032
        L_0x006b:
            r0 = move-exception
            r1 = r2
        L_0x006d:
            java.lang.String r2 = "SDK_LW_CMM"
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x00a5 }
            android.util.Log.e(r2, r0)     // Catch:{ all -> 0x00a5 }
            if (r1 == 0) goto L_0x0032
            r1.close()     // Catch:{ IOException -> 0x007c }
            goto L_0x0032
        L_0x007c:
            r0 = move-exception
            java.lang.String r1 = "SDK_LW_CMM"
            java.lang.String r0 = r0.getMessage()
            android.util.Log.e(r1, r0)
            goto L_0x0032
        L_0x0087:
            r0 = move-exception
            r1 = r2
        L_0x0089:
            if (r1 == 0) goto L_0x008e
            r1.close()     // Catch:{ IOException -> 0x008f }
        L_0x008e:
            throw r0
        L_0x008f:
            r1 = move-exception
            java.lang.String r2 = "SDK_LW_CMM"
            java.lang.String r1 = r1.getMessage()
            android.util.Log.e(r2, r1)
            goto L_0x008e
        L_0x009a:
            r0 = move-exception
            java.lang.String r1 = "SDK_LW_CMM"
            java.lang.String r0 = r0.getMessage()
            android.util.Log.e(r1, r0)
            goto L_0x0032
        L_0x00a5:
            r0 = move-exception
            goto L_0x0089
        L_0x00a7:
            r0 = move-exception
            goto L_0x006d
        L_0x00a9:
            r0 = move-exception
            goto L_0x0051
        L_0x00ab:
            r0 = move-exception
            goto L_0x0035
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cmsc.cmmusic.init.XZip.toZIP(android.content.Context, int):void");
    }

    static String getFilePath(Context context, int i) {
        if (i == 0) {
            return "/data/data/" + context.getPackageName() + "/" + "scmsc0.si";
        }
        if (i == 1) {
            return "/data/data/" + context.getPackageName() + "/" + "scmsc1.si";
        }
        return "/data/data/" + context.getPackageName() + "/" + "scmsc.si";
    }

    /* JADX WARNING: Removed duplicated region for block: B:28:0x0064 A[SYNTHETIC, Splitter:B:28:0x0064] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0080 A[SYNTHETIC, Splitter:B:38:0x0080] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0094 A[SYNTHETIC, Splitter:B:45:0x0094] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static java.lang.String fromZIP(android.content.Context r6, int r7) {
        /*
            r0 = 0
            java.io.File r1 = new java.io.File
            java.lang.String r2 = getFilePath(r6, r7)
            r1.<init>(r2)
            boolean r2 = r1.exists()
            if (r2 == 0) goto L_0x002f
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ UnsupportedEncodingException -> 0x003b, FileNotFoundException -> 0x0057, IOException -> 0x0073, all -> 0x008f }
            java.io.InputStreamReader r3 = new java.io.InputStreamReader     // Catch:{ UnsupportedEncodingException -> 0x003b, FileNotFoundException -> 0x0057, IOException -> 0x0073, all -> 0x008f }
            java.util.zip.GZIPInputStream r4 = new java.util.zip.GZIPInputStream     // Catch:{ UnsupportedEncodingException -> 0x003b, FileNotFoundException -> 0x0057, IOException -> 0x0073, all -> 0x008f }
            java.io.FileInputStream r5 = new java.io.FileInputStream     // Catch:{ UnsupportedEncodingException -> 0x003b, FileNotFoundException -> 0x0057, IOException -> 0x0073, all -> 0x008f }
            r5.<init>(r1)     // Catch:{ UnsupportedEncodingException -> 0x003b, FileNotFoundException -> 0x0057, IOException -> 0x0073, all -> 0x008f }
            r4.<init>(r5)     // Catch:{ UnsupportedEncodingException -> 0x003b, FileNotFoundException -> 0x0057, IOException -> 0x0073, all -> 0x008f }
            java.lang.String r1 = "UTF-8"
            r3.<init>(r4, r1)     // Catch:{ UnsupportedEncodingException -> 0x003b, FileNotFoundException -> 0x0057, IOException -> 0x0073, all -> 0x008f }
            r2.<init>(r3)     // Catch:{ UnsupportedEncodingException -> 0x003b, FileNotFoundException -> 0x0057, IOException -> 0x0073, all -> 0x008f }
            java.lang.String r0 = r2.readLine()     // Catch:{ UnsupportedEncodingException -> 0x00a9, FileNotFoundException -> 0x00a7, IOException -> 0x00a5 }
            if (r2 == 0) goto L_0x002f
            r2.close()     // Catch:{ IOException -> 0x0030 }
        L_0x002f:
            return r0
        L_0x0030:
            r1 = move-exception
            java.lang.String r2 = "SDK_LW_CMM"
            java.lang.String r1 = r1.getMessage()
            android.util.Log.e(r2, r1)
            goto L_0x002f
        L_0x003b:
            r1 = move-exception
            r2 = r0
        L_0x003d:
            java.lang.String r3 = "SDK_LW_CMM"
            java.lang.String r1 = r1.getMessage()     // Catch:{ all -> 0x00a3 }
            android.util.Log.e(r3, r1)     // Catch:{ all -> 0x00a3 }
            if (r2 == 0) goto L_0x002f
            r2.close()     // Catch:{ IOException -> 0x004c }
            goto L_0x002f
        L_0x004c:
            r1 = move-exception
            java.lang.String r2 = "SDK_LW_CMM"
            java.lang.String r1 = r1.getMessage()
            android.util.Log.e(r2, r1)
            goto L_0x002f
        L_0x0057:
            r1 = move-exception
            r2 = r0
        L_0x0059:
            java.lang.String r3 = "SDK_LW_CMM"
            java.lang.String r1 = r1.getMessage()     // Catch:{ all -> 0x00a3 }
            android.util.Log.e(r3, r1)     // Catch:{ all -> 0x00a3 }
            if (r2 == 0) goto L_0x002f
            r2.close()     // Catch:{ IOException -> 0x0068 }
            goto L_0x002f
        L_0x0068:
            r1 = move-exception
            java.lang.String r2 = "SDK_LW_CMM"
            java.lang.String r1 = r1.getMessage()
            android.util.Log.e(r2, r1)
            goto L_0x002f
        L_0x0073:
            r1 = move-exception
            r2 = r0
        L_0x0075:
            java.lang.String r3 = "SDK_LW_CMM"
            java.lang.String r1 = r1.getMessage()     // Catch:{ all -> 0x00a3 }
            android.util.Log.e(r3, r1)     // Catch:{ all -> 0x00a3 }
            if (r2 == 0) goto L_0x002f
            r2.close()     // Catch:{ IOException -> 0x0084 }
            goto L_0x002f
        L_0x0084:
            r1 = move-exception
            java.lang.String r2 = "SDK_LW_CMM"
            java.lang.String r1 = r1.getMessage()
            android.util.Log.e(r2, r1)
            goto L_0x002f
        L_0x008f:
            r1 = move-exception
            r2 = r0
            r0 = r1
        L_0x0092:
            if (r2 == 0) goto L_0x0097
            r2.close()     // Catch:{ IOException -> 0x0098 }
        L_0x0097:
            throw r0
        L_0x0098:
            r1 = move-exception
            java.lang.String r2 = "SDK_LW_CMM"
            java.lang.String r1 = r1.getMessage()
            android.util.Log.e(r2, r1)
            goto L_0x0097
        L_0x00a3:
            r0 = move-exception
            goto L_0x0092
        L_0x00a5:
            r1 = move-exception
            goto L_0x0075
        L_0x00a7:
            r1 = move-exception
            goto L_0x0059
        L_0x00a9:
            r1 = move-exception
            goto L_0x003d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cmsc.cmmusic.init.XZip.fromZIP(android.content.Context, int):java.lang.String");
    }

    static void toZIP(Context context) {
        toZIP(context, -1);
    }

    static String fromZIP(Context context) {
        return fromZIP(context, -1);
    }
}
