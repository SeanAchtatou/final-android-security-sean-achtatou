package com.cmsc.cmmusic.init;

import android.content.Context;
import android.os.Build;
import android.util.Log;
import com.tencent.connect.common.Constants;
import com.tencent.open.SocialConstants;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.util.Hashtable;

class InitCmm1 {
    private static int counter;
    private static boolean flag;

    private static native OutputStream getOutputStream(Context context, HttpURLConnection httpURLConnection, byte[] bArr, String str, int i) throws IOException;

    static native Hashtable<String, String> initCm(Context context, int i);

    static native Hashtable<String, String> initCmm1(Context context);

    static native void send(Context context, int i);

    InitCmm1() {
    }

    static boolean initCheck(Context context) {
        File file = new File(XZip.getFilePath(context, -1));
        String imsi1 = GetAppInfo.getIMSI1(context);
        if (imsi1 == null || "".equals(imsi1)) {
            Log.i("SDK_LW_CMM", "no sim");
            return false;
        } else if (file.exists()) {
            if (GetAppInfo.equals(context, imsi1, XZip.fromZIP(context))) {
                Log.i("SDK_LW_CMM", "not need initialize ...");
                return true;
            }
            Log.i("SDK_LW_CMM", "sim is changed");
            return false;
        } else if ("0".equals(httpUrlConnection(context, NetMode.WIFIorMOBILE(context), "http://218.200.227.123:95/sdkServer/checksmsinitreturn", -1).get("code"))) {
            XZip.out1(context);
            Log.i("SDK_LW_CMM", "server have pid");
            return true;
        } else {
            Log.i("SDK_LW_CMM", "server have no pid");
            return false;
        }
    }

    private static Hashtable<String, String> initCmm(Context context) {
        String str = Build.MODEL;
        String str2 = Build.VERSION.RELEASE;
        String imei = GetAppInfo.getIMEI(context);
        String imsi1 = GetAppInfo.getIMSI1(context);
        Log.i("SDK_LW_CMM", "init1 calling");
        Log.i("SDK_LW_CMM", "ChannelCode=" + GetAppInfo.getChannelCode(context) + ", devicemodel=" + str + ", imei=" + imei + ", release=" + str2 + ", imsi=" + imsi1);
        if ("sdk".equals(str)) {
            Log.i("SDK_LW_CMM", "google_sdk...模拟器运行...not apn setting");
        }
        if (imsi1 == null || "".equals(imsi1)) {
            Hashtable<String, String> hashtable = new Hashtable<>();
            hashtable.put("code", "4");
            hashtable.put(SocialConstants.PARAM_APP_DESC, "无SIM卡，双卡手机请将中国移动sim插入主卡槽");
            return hashtable;
        } else if (!InitCmm3.isCmCard(imsi1)) {
            Hashtable<String, String> hashtable2 = new Hashtable<>();
            hashtable2.put("code", "3");
            hashtable2.put(SocialConstants.PARAM_APP_DESC, "请使用中国移动SIM卡，双卡手机请将中国移动sim卡插入主卡槽");
            return hashtable2;
        } else if (!new File(XZip.getFilePath(context, -1)).exists()) {
            Hashtable<String, String> httpUrlConnection = httpUrlConnection(context, NetMode.WIFIorMOBILE(context), "http://218.200.227.123:95/sdkServer/checksmsinitreturn", -1);
            if ("0".equals(httpUrlConnection.get("code"))) {
                Log.i("SDK_LW_CMM", "server heve pid");
                XZip.out1(context);
                return httpUrlConnection;
            }
            Log.i("SDK_LW_CMM", "server heve no pid, initiating");
            Hashtable<String, String> init = init(context);
            if (!"0".equals(init.get("code"))) {
                return init;
            }
            XZip.out1(context);
            Log.i("SDK_LW_CMM", "init success");
            return init;
        } else if (GetAppInfo.equals(context, imsi1, XZip.fromZIP(context))) {
            Log.i("SDK_LW_CMM", "the same file");
            Log.i("SDK_LW_CMM", "init success");
            Hashtable<String, String> hashtable3 = new Hashtable<>();
            hashtable3.put("code", "0");
            hashtable3.put(SocialConstants.PARAM_APP_DESC, "初始化成功");
            return hashtable3;
        } else {
            Log.i("SDK_LW_CMM", "difference file");
            Log.i("SDK_LW_CMM", "sim is changed, initiating");
            Hashtable<String, String> init2 = init(context);
            if (!"0".equals(init2.get("code"))) {
                return init2;
            }
            XZip.out1(context);
            Log.i("SDK_LW_CMM", "init success");
            return init2;
        }
    }

    private static Hashtable<String, String> init(Context context) {
        String WIFIorMOBILE = NetMode.WIFIorMOBILE(context);
        if ("CMWAP".equals(WIFIorMOBILE) || "CMNET".equals(WIFIorMOBILE)) {
            Log.i("SDK_LW_CMM", "netmode " + WIFIorMOBILE);
            Hashtable<String, String> initCm = initCm(context, -1);
            if ((initCm == null || !"0".equals(initCm.get("code"))) && GetAppInfo.needSmsInit(context)) {
                return initSMS(context, WIFIorMOBILE);
            }
            return initCm;
        } else if ("WIFI".equals(WIFIorMOBILE) || "OTHER".equals(WIFIorMOBILE)) {
            Log.i("SDK_LW_CMM", "netmode wifi other");
            if (GetAppInfo.needSmsInit(context)) {
                return initSMS(context, WIFIorMOBILE);
            }
            Hashtable<String, String> hashtable = new Hashtable<>();
            hashtable.put("code", Constants.VIA_REPORT_TYPE_MAKE_FRIEND);
            hashtable.put(SocialConstants.PARAM_APP_DESC, "初始化失败");
            return hashtable;
        } else {
            Log.i("SDK_LW_CMM", "netmode--" + WIFIorMOBILE);
            Hashtable<String, String> hashtable2 = new Hashtable<>();
            hashtable2.put("code", "2");
            hashtable2.put(SocialConstants.PARAM_APP_DESC, "请检查网络连接是否可以访问公网或是cmwap/cmnet/wifi方式联网");
            return hashtable2;
        }
    }

    private static Hashtable<String, String> initSMS(Context context, String str) {
        if (Constants.countMap.get("initCount").intValue() >= 3) {
            Hashtable<String, String> hashtable = new Hashtable<>();
            hashtable.put("code", Constants.VIA_SHARE_TYPE_INFO);
            hashtable.put(SocialConstants.PARAM_APP_DESC, "在24小时内短信初始化调用次数不能超过3次。");
            return hashtable;
        }
        send(context, -1);
        Utils.smsCount(context);
        Log.i("SDK_LW_CMM", "sendSMS sleep");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            Log.e("SDK_LW_CMM", e.getMessage());
        }
        flag = false;
        counter = 0;
        Hashtable<String, String> hashtable2 = new Hashtable<>();
        while (!flag) {
            counter++;
            Log.i("SDK_LW_CMM", "initSMS " + counter);
            Hashtable<String, String> httpUrlConnection = httpUrlConnection(context, str, "http://218.200.227.123:95/sdkServer/checksmsinitreturn", -1);
            if ("0".equals(httpUrlConnection.get("code"))) {
                flag = true;
                return httpUrlConnection;
            } else if (counter >= 3) {
                flag = true;
                return httpUrlConnection;
            } else {
                try {
                    Thread.sleep(5000);
                    hashtable2 = httpUrlConnection;
                } catch (InterruptedException e2) {
                    Log.e("SDK_LW_CMM", e2.getMessage());
                    hashtable2 = httpUrlConnection;
                }
            }
        }
        return hashtable2;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r3v0 */
    /* JADX WARN: Type inference failed for: r3v1, types: [java.io.InputStream] */
    /* JADX WARN: Type inference failed for: r3v2, types: [java.io.ByteArrayOutputStream] */
    /* JADX WARN: Type inference failed for: r3v3 */
    /* JADX WARN: Type inference failed for: r3v4, types: [java.io.InputStream] */
    /* JADX WARN: Type inference failed for: r3v6 */
    /* JADX WARN: Type inference failed for: r3v9 */
    /* JADX WARN: Type inference failed for: r3v10 */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00d5, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00d6, code lost:
        r8 = r2;
        r2 = r3;
        r3 = r4;
        r4 = r5;
        r5 = r0;
        r0 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x015f, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x0160, code lost:
        r8 = r1;
        r1 = r0;
        r0 = r8;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00f5  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00fa A[SYNTHETIC, Splitter:B:41:0x00fa] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00ff A[SYNTHETIC, Splitter:B:44:0x00ff] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0104 A[SYNTHETIC, Splitter:B:47:0x0104] */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x011e  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x0123 A[SYNTHETIC, Splitter:B:61:0x0123] */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x0128 A[SYNTHETIC, Splitter:B:64:0x0128] */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x012d A[SYNTHETIC, Splitter:B:67:0x012d] */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x015f A[ExcHandler: all (r1v11 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:10:0x0074] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static java.util.Hashtable<java.lang.String, java.lang.String> httpUrlConnection(android.content.Context r9, java.lang.String r10, java.lang.String r11, int r12) {
        /*
            r3 = 0
            java.util.Hashtable r1 = new java.util.Hashtable
            r1.<init>()
            java.net.URL r0 = new java.net.URL     // Catch:{ Exception -> 0x016a, all -> 0x0118 }
            r0.<init>(r11)     // Catch:{ Exception -> 0x016a, all -> 0x0118 }
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ Exception -> 0x016a, all -> 0x0118 }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x016a, all -> 0x0118 }
            r2 = 1
            r0.setDoOutput(r2)     // Catch:{ Exception -> 0x0170, all -> 0x0152 }
            r2 = 1
            r0.setDoInput(r2)     // Catch:{ Exception -> 0x0170, all -> 0x0152 }
            r2 = 30000(0x7530, float:4.2039E-41)
            r0.setConnectTimeout(r2)     // Catch:{ Exception -> 0x0170, all -> 0x0152 }
            r2 = 30000(0x7530, float:4.2039E-41)
            r0.setReadTimeout(r2)     // Catch:{ Exception -> 0x0170, all -> 0x0152 }
            r2 = 0
            r0.setUseCaches(r2)     // Catch:{ Exception -> 0x0170, all -> 0x0152 }
            java.lang.String r2 = "POST"
            r0.setRequestMethod(r2)     // Catch:{ Exception -> 0x0170, all -> 0x0152 }
            java.lang.String r2 = "<?xml version='1.0' encoding='UTF-8'?><request><request>"
            java.lang.String r4 = "UTF-8"
            byte[] r2 = r2.getBytes(r4)     // Catch:{ Exception -> 0x0170, all -> 0x0152 }
            java.lang.String r4 = "Content-length"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0170, all -> 0x0152 }
            r5.<init>()     // Catch:{ Exception -> 0x0170, all -> 0x0152 }
            int r6 = r2.length     // Catch:{ Exception -> 0x0170, all -> 0x0152 }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x0170, all -> 0x0152 }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x0170, all -> 0x0152 }
            r0.setRequestProperty(r4, r5)     // Catch:{ Exception -> 0x0170, all -> 0x0152 }
            java.lang.String r4 = "Accept"
            java.lang.String r5 = "*/*"
            r0.setRequestProperty(r4, r5)     // Catch:{ Exception -> 0x0170, all -> 0x0152 }
            java.lang.String r4 = "Content-Type"
            java.lang.String r5 = "*/*"
            r0.setRequestProperty(r4, r5)     // Catch:{ Exception -> 0x0170, all -> 0x0152 }
            java.lang.String r4 = "Connection"
            java.lang.String r5 = "Keep-Alive"
            r0.setRequestProperty(r4, r5)     // Catch:{ Exception -> 0x0170, all -> 0x0152 }
            java.lang.String r4 = "Accept-Charset"
            java.lang.String r5 = "UTF-8"
            r0.setRequestProperty(r4, r5)     // Catch:{ Exception -> 0x0170, all -> 0x0152 }
            java.io.OutputStream r5 = getOutputStream(r9, r0, r2, r11, r12)     // Catch:{ Exception -> 0x0170, all -> 0x0152 }
            int r2 = r0.getResponseCode()     // Catch:{ Exception -> 0x0177, all -> 0x0159 }
            r4 = 200(0xc8, float:2.8E-43)
            if (r4 != r2) goto L_0x0187
            java.io.ByteArrayOutputStream r4 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x0177, all -> 0x0159 }
            r4.<init>()     // Catch:{ Exception -> 0x0177, all -> 0x0159 }
            java.io.InputStream r3 = r0.getInputStream()     // Catch:{ Exception -> 0x017e, all -> 0x015f }
            r2 = 1024(0x400, float:1.435E-42)
            byte[] r2 = new byte[r2]     // Catch:{ Exception -> 0x00d5, all -> 0x015f }
        L_0x007c:
            r6 = 0
            r7 = 1024(0x400, float:1.435E-42)
            int r6 = r3.read(r2, r6, r7)     // Catch:{ Exception -> 0x00d5, all -> 0x015f }
            r7 = -1
            if (r6 != r7) goto L_0x00cd
            java.lang.String r2 = new java.lang.String     // Catch:{ Exception -> 0x00d5, all -> 0x015f }
            byte[] r6 = r4.toByteArray()     // Catch:{ Exception -> 0x00d5, all -> 0x015f }
            java.lang.String r7 = "UTF-8"
            r2.<init>(r6, r7)     // Catch:{ Exception -> 0x00d5, all -> 0x015f }
            java.lang.String r6 = "UTF-8"
            byte[] r6 = r2.getBytes(r6)     // Catch:{ Exception -> 0x00d5, all -> 0x015f }
            java.io.InputStream r6 = com.cmsc.cmmusic.init.PullXMLTool.byte2InputStream(r6)     // Catch:{ Exception -> 0x00d5, all -> 0x015f }
            java.lang.String r6 = com.cmsc.cmmusic.init.PullXMLTool.pull2Result(r6)     // Catch:{ Exception -> 0x00d5, all -> 0x015f }
            java.lang.String r7 = "UTF-8"
            byte[] r2 = r2.getBytes(r7)     // Catch:{ Exception -> 0x00d5, all -> 0x015f }
            java.io.InputStream r2 = com.cmsc.cmmusic.init.PullXMLTool.byte2InputStream(r2)     // Catch:{ Exception -> 0x00d5, all -> 0x015f }
            java.lang.String r2 = com.cmsc.cmmusic.init.PullXMLTool.pull2ResultDesc(r2)     // Catch:{ Exception -> 0x00d5, all -> 0x015f }
            java.lang.String r7 = "code"
            r1.put(r7, r6)     // Catch:{ Exception -> 0x00d5, all -> 0x015f }
            java.lang.String r6 = "desc"
            r1.put(r6, r2)     // Catch:{ Exception -> 0x00d5, all -> 0x015f }
        L_0x00b7:
            if (r0 == 0) goto L_0x00bc
            r0.disconnect()
        L_0x00bc:
            if (r5 == 0) goto L_0x00c1
            r5.close()     // Catch:{ IOException -> 0x0140 }
        L_0x00c1:
            if (r4 == 0) goto L_0x00c6
            r4.close()     // Catch:{ IOException -> 0x0146 }
        L_0x00c6:
            if (r3 == 0) goto L_0x00cb
            r3.close()     // Catch:{ IOException -> 0x014c }
        L_0x00cb:
            r0 = r1
        L_0x00cc:
            return r0
        L_0x00cd:
            r7 = 0
            r4.write(r2, r7, r6)     // Catch:{ Exception -> 0x00d5, all -> 0x015f }
            r4.flush()     // Catch:{ Exception -> 0x00d5, all -> 0x015f }
            goto L_0x007c
        L_0x00d5:
            r2 = move-exception
            r8 = r2
            r2 = r3
            r3 = r4
            r4 = r5
            r5 = r0
            r0 = r8
        L_0x00dc:
            java.lang.String r6 = "SDK_LW_CMM"
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x0164 }
            android.util.Log.e(r6, r0)     // Catch:{ all -> 0x0164 }
            java.lang.String r0 = "code"
            java.lang.String r6 = "-2"
            r1.put(r0, r6)     // Catch:{ all -> 0x0164 }
            java.lang.String r0 = "desc"
            java.lang.String r6 = "connection timeout and so on（网络不通，稍后再试）"
            r1.put(r0, r6)     // Catch:{ all -> 0x0164 }
            if (r5 == 0) goto L_0x00f8
            r5.disconnect()
        L_0x00f8:
            if (r4 == 0) goto L_0x00fd
            r4.close()     // Catch:{ IOException -> 0x0109 }
        L_0x00fd:
            if (r3 == 0) goto L_0x0102
            r3.close()     // Catch:{ IOException -> 0x010e }
        L_0x0102:
            if (r2 == 0) goto L_0x0107
            r2.close()     // Catch:{ IOException -> 0x0113 }
        L_0x0107:
            r0 = r1
            goto L_0x00cc
        L_0x0109:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00fd
        L_0x010e:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0102
        L_0x0113:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0107
        L_0x0118:
            r0 = move-exception
            r4 = r3
            r5 = r3
            r1 = r3
        L_0x011c:
            if (r1 == 0) goto L_0x0121
            r1.disconnect()
        L_0x0121:
            if (r5 == 0) goto L_0x0126
            r5.close()     // Catch:{ IOException -> 0x0131 }
        L_0x0126:
            if (r4 == 0) goto L_0x012b
            r4.close()     // Catch:{ IOException -> 0x0136 }
        L_0x012b:
            if (r3 == 0) goto L_0x0130
            r3.close()     // Catch:{ IOException -> 0x013b }
        L_0x0130:
            throw r0
        L_0x0131:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0126
        L_0x0136:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x012b
        L_0x013b:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0130
        L_0x0140:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00c1
        L_0x0146:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00c6
        L_0x014c:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00cb
        L_0x0152:
            r1 = move-exception
            r4 = r3
            r5 = r3
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x011c
        L_0x0159:
            r1 = move-exception
            r4 = r3
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x011c
        L_0x015f:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x011c
        L_0x0164:
            r0 = move-exception
            r1 = r5
            r5 = r4
            r4 = r3
            r3 = r2
            goto L_0x011c
        L_0x016a:
            r0 = move-exception
            r2 = r3
            r4 = r3
            r5 = r3
            goto L_0x00dc
        L_0x0170:
            r2 = move-exception
            r4 = r3
            r5 = r0
            r0 = r2
            r2 = r3
            goto L_0x00dc
        L_0x0177:
            r2 = move-exception
            r4 = r5
            r5 = r0
            r0 = r2
            r2 = r3
            goto L_0x00dc
        L_0x017e:
            r2 = move-exception
            r8 = r2
            r2 = r3
            r3 = r4
            r4 = r5
            r5 = r0
            r0 = r8
            goto L_0x00dc
        L_0x0187:
            r4 = r3
            goto L_0x00b7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cmsc.cmmusic.init.InitCmm1.httpUrlConnection(android.content.Context, java.lang.String, java.lang.String, int):java.util.Hashtable");
    }
}
