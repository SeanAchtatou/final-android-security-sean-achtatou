package com.cmsc.cmmusic.init;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import java.lang.reflect.InvocationTargetException;

public class NetMode {
    private static ConnectivityManager connManager;

    public static String WIFIorMOBILE(Context context) {
        if (checkWifiNetStatus(context)) {
            return "WIFI";
        }
        if (!checkMobileNetStatus(context)) {
            return "NOWM";
        }
        NetworkInfo networkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getNetworkInfo(0);
        if (networkInfo == null) {
            return "NIISNUll";
        }
        if ("cmwap".equals(networkInfo.getExtraInfo())) {
            return "CMWAP";
        }
        if ("cmnet".equals(networkInfo.getExtraInfo())) {
            return "CMNET";
        }
        return "OTHER";
    }

    static boolean checkWifiNetStatus(Context context) {
        NetworkInfo networkInfo;
        if (connManager == null) {
            connManager = (ConnectivityManager) context.getSystemService("connectivity");
        }
        if (connManager == null || (networkInfo = connManager.getNetworkInfo(1)) == null || NetworkInfo.State.CONNECTED != networkInfo.getState()) {
            return false;
        }
        return true;
    }

    static boolean checkMobileNetStatus(Context context) {
        NetworkInfo networkInfo;
        if (connManager == null) {
            connManager = (ConnectivityManager) context.getSystemService("connectivity");
        }
        if (connManager == null || (networkInfo = connManager.getNetworkInfo(0)) == null || NetworkInfo.State.CONNECTED != networkInfo.getState()) {
            return false;
        }
        return true;
    }

    static boolean simInserted(Context context, int i) throws IllegalArgumentException, SecurityException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        String imsi = GetAppInfo.getIMSI(context, i);
        if (imsi == null || imsi.trim().length() <= 0) {
            return false;
        }
        return true;
    }

    static int simWhichConnected(Context context) throws IllegalArgumentException, SecurityException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        return DualSimUtils.getDefaultSim(context);
    }

    public static boolean isConnected(Context context) {
        NetworkInfo activeNetworkInfo;
        NetworkInfo[] allNetworkInfo;
        if (connManager == null) {
            connManager = (ConnectivityManager) context.getSystemService("connectivity");
        }
        if (connManager == null || (activeNetworkInfo = connManager.getActiveNetworkInfo()) == null || !activeNetworkInfo.isAvailable() || activeNetworkInfo.getState() != NetworkInfo.State.CONNECTED || !"CONNECTED".equalsIgnoreCase(activeNetworkInfo.getState().name()) || (allNetworkInfo = connManager.getAllNetworkInfo()) == null) {
            return false;
        }
        for (int i = 0; i < allNetworkInfo.length; i++) {
            if (allNetworkInfo[i] != null && allNetworkInfo[i].isConnected()) {
                return true;
            }
        }
        return false;
    }
}
