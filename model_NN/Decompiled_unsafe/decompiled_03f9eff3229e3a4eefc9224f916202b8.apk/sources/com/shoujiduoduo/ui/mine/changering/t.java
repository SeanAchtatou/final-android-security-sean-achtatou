package com.shoujiduoduo.ui.mine.changering;

import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.ui.mine.changering.RingSettingFragment;
import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;
import java.util.Iterator;

/* compiled from: RingSettingFragment */
class t extends a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ s f1291a;

    t(s sVar) {
        this.f1291a = sVar;
    }

    public void a(c.b bVar) {
        boolean z;
        super.a(bVar);
        if (bVar instanceof c.x) {
            Iterator<c.ac> it = ((c.x) bVar).d().iterator();
            while (true) {
                if (!it.hasNext()) {
                    z = false;
                    break;
                }
                c.ac next = it.next();
                if (this.f1291a.f1290a.d.equals(next.b())) {
                    String unused = this.f1291a.f1290a.c = next.d();
                    String unused2 = this.f1291a.f1290a.b = next.c();
                    z = true;
                    break;
                }
            }
            if (z) {
                RingSettingFragment.b unused3 = this.f1291a.f1290a.l = RingSettingFragment.b.success;
                RingData ringData = new RingData();
                ringData.s = this.f1291a.f1290a.d;
                ringData.v = 0;
                ringData.e = this.f1291a.f1290a.b + "-" + this.f1291a.f1290a.c;
                ringData.j = 48;
                if (this.f1291a.f1290a.e.size() > 3) {
                    ((RingSettingFragment.e) this.f1291a.f1290a.e.get(3)).c = 48000;
                    ((RingSettingFragment.e) this.f1291a.f1290a.e.get(3)).b = this.f1291a.f1290a.b + "-" + this.f1291a.f1290a.c;
                    ((RingSettingFragment.e) this.f1291a.f1290a.e.get(3)).f1263a = ringData;
                }
                this.f1291a.f1290a.f.notifyDataSetChanged();
            }
        }
    }

    public void b(c.b bVar) {
        super.b(bVar);
        com.shoujiduoduo.base.a.a.c("RingSettingFragment", "查询默认彩铃失败，code:" + bVar.a() + " msg:" + bVar.b());
        this.f1291a.f1290a.e();
    }
}
