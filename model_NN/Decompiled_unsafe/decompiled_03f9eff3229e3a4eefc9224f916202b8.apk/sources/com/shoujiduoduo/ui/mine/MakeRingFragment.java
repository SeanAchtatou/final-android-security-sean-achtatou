package com.shoujiduoduo.ui.mine;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.a.b.b;
import com.shoujiduoduo.a.c.f;
import com.shoujiduoduo.a.c.o;
import com.shoujiduoduo.a.c.v;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.util.ak;

public class MakeRingFragment extends Fragment {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public ad f1209a;
    /* access modifiers changed from: private */
    public ListView b;
    /* access modifiers changed from: private */
    public boolean c;
    private View d;
    private Button e;
    /* access modifiers changed from: private */
    public Button f;
    /* access modifiers changed from: private */
    public int g;
    private TextView h;
    /* access modifiers changed from: private */
    public a i;
    /* access modifiers changed from: private */
    public boolean j;
    private View.OnKeyListener k = new v(this);
    private View.OnClickListener l = new w(this);
    private View.OnClickListener m = new x(this);
    private o n = new aa(this);
    private f o = new ab(this);
    private v p = new ac(this);

    static /* synthetic */ int g(MakeRingFragment makeRingFragment) {
        int i2 = makeRingFragment.g;
        makeRingFragment.g = i2 + 1;
        return i2;
    }

    static /* synthetic */ int h(MakeRingFragment makeRingFragment) {
        int i2 = makeRingFragment.g;
        makeRingFragment.g = i2 - 1;
        return i2;
    }

    public void onCreate(Bundle bundle) {
        this.f1209a = new ad(getActivity());
        this.f1209a.a();
        this.i = new a(getActivity(), b.b().a("make_ring_list"), "make_ring_list");
        com.shoujiduoduo.base.a.a.a("MakeRingFragment", "onCreate");
        super.onCreate(bundle);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate((int) R.layout.my_ringtone_make, viewGroup, false);
        this.b = (ListView) inflate.findViewById(R.id.my_ringtone_make_list);
        View inflate2 = layoutInflater.inflate((int) R.layout.ringtone_diy, (ViewGroup) null, false);
        this.h = (TextView) inflate.findViewById(R.id.make_hint);
        ((Button) inflate2.findViewById(R.id.btn_ringtone_diy)).setOnClickListener(new u(this));
        this.b.addFooterView(inflate2);
        if (b.b().c()) {
            this.b.setAdapter((ListAdapter) this.f1209a);
            this.j = true;
            b();
        }
        this.d = (LinearLayout) inflate.findViewById(R.id.del_confirm);
        this.e = (Button) this.d.findViewById(R.id.cancel);
        this.e.setOnClickListener(this.l);
        this.f = (Button) this.d.findViewById(R.id.delete);
        this.f.setOnClickListener(this.m);
        this.d.setVisibility(4);
        this.b.setChoiceMode(1);
        this.b.setOnItemClickListener(new a(this, null));
        x.a().a(com.shoujiduoduo.a.a.b.OBSERVER_USER_RING, this.p);
        x.a().a(com.shoujiduoduo.a.a.b.OBSERVER_LIST_DATA, this.o);
        x.a().a(com.shoujiduoduo.a.a.b.OBSERVER_RING_CHANGE, this.n);
        com.shoujiduoduo.base.a.a.a("MakeRingFragment", "onCreateview");
        return inflate;
    }

    public void onDestroy() {
        this.f1209a.b();
        super.onDestroy();
        com.shoujiduoduo.base.a.a.a("MakeRingFragment", "onDestroy");
    }

    public void onResume() {
        com.shoujiduoduo.base.a.a.a("MakeRingFragment", "onResume");
        super.onResume();
        getView().setFocusable(true);
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(this.k);
    }

    public void onDestroyView() {
        this.j = false;
        this.c = false;
        x.a().b(com.shoujiduoduo.a.a.b.OBSERVER_USER_RING, this.p);
        x.a().b(com.shoujiduoduo.a.a.b.OBSERVER_LIST_DATA, this.o);
        x.a().b(com.shoujiduoduo.a.a.b.OBSERVER_RING_CHANGE, this.n);
        super.onDestroyView();
        com.shoujiduoduo.base.a.a.a("MakeRingFragment", "onDestroyView");
    }

    public boolean a() {
        return this.c;
    }

    public void a(boolean z) {
        if (this.c != z && this.j) {
            this.c = z;
            this.d.setVisibility(z ? 0 : 8);
            if (z) {
                this.i.a(b.b().a("make_ring_list"));
                this.b.setAdapter((ListAdapter) this.i);
                this.g = 0;
                this.f.setText("删除");
                return;
            }
            this.b.setAdapter((ListAdapter) this.f1209a);
        }
    }

    /* access modifiers changed from: private */
    public void b() {
        if (b.b().a("make_ring_list").c() > 0) {
            this.h.setVisibility(8);
        } else {
            this.h.setVisibility(0);
        }
    }

    private class a implements AdapterView.OnItemClickListener {
        private a() {
        }

        /* synthetic */ a(MakeRingFragment makeRingFragment, u uVar) {
            this();
        }

        public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            if (MakeRingFragment.this.c) {
                CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkbox);
                checkBox.toggle();
                MakeRingFragment.this.i.a().set(i, Boolean.valueOf(checkBox.isChecked()));
                if (checkBox.isChecked()) {
                    MakeRingFragment.g(MakeRingFragment.this);
                } else {
                    MakeRingFragment.h(MakeRingFragment.this);
                }
                if (MakeRingFragment.this.g > 0) {
                    MakeRingFragment.this.f.setText("删除(" + MakeRingFragment.this.g + ")");
                } else {
                    MakeRingFragment.this.f.setText("删除");
                }
            } else {
                PlayerService b = ak.a().b();
                if (b != null) {
                    b.a(b.b().a("make_ring_list"), i);
                    MakeRingFragment.this.f1209a.notifyDataSetChanged();
                    return;
                }
                com.shoujiduoduo.base.a.a.c("MakeRingFragment", "PlayerService is unavailable!");
            }
        }
    }
}
