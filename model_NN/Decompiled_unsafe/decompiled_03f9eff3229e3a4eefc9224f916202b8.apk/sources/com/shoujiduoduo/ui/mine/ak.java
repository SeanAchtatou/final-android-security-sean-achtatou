package com.shoujiduoduo.ui.mine;

import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.base.bean.MakeRingData;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ui.user.UserLoginActivity;
import com.umeng.analytics.b;
import java.io.File;

/* compiled from: MakeRingListAdapter */
class ak implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ad f1223a;

    ak(ad adVar) {
        this.f1223a = adVar;
    }

    public void onClick(View view) {
        a.a("MyRingMakeAdapter", "RingtoneDuoduo: click upload button!");
        b.b(this.f1223a.b, "USER_CLICK_UPLOAD");
        if (!com.shoujiduoduo.a.b.b.g().g()) {
            this.f1223a.b.startActivity(new Intent(this.f1223a.b, UserLoginActivity.class));
            return;
        }
        RingData ringData = (RingData) com.shoujiduoduo.a.b.b.b().a("make_ring_list").a(this.f1223a.c);
        if (ringData != null) {
            String str = ((MakeRingData) ringData).m;
            if (!TextUtils.isEmpty(str)) {
                File file = new File(str);
                if (!file.exists() || file.length() == 0) {
                    Toast.makeText(this.f1223a.b, (int) R.string.upload_file_error, 1);
                    a.c("MyRingMakeAdapter", "当前录制铃声长度太小，不满足上传要求");
                    return;
                }
                new az(this.f1223a.b, R.style.DuoDuoDialog, ringData).show();
                return;
            }
            Toast.makeText(this.f1223a.b, (int) R.string.file_not_found, 1);
            a.c("MyRingMakeAdapter", "未找到当前录制铃声");
        }
    }
}
