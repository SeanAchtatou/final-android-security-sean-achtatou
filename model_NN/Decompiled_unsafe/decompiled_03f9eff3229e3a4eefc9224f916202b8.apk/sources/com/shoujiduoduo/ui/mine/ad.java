package com.shoujiduoduo.ui.mine;

import android.app.Activity;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.shoujiduoduo.a.a.b;
import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.a.c.n;
import com.shoujiduoduo.a.c.p;
import com.shoujiduoduo.base.bean.MakeRingData;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ui.utils.cc;
import com.shoujiduoduo.util.ak;
import com.shoujiduoduo.util.as;

/* compiled from: MakeRingListAdapter */
class ad extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    private LayoutInflater f1216a;
    /* access modifiers changed from: private */
    public Activity b;
    /* access modifiers changed from: private */
    public int c = -1;
    /* access modifiers changed from: private */
    public boolean d = false;
    private p e = new ae(this);
    private n f = new ah(this);
    /* access modifiers changed from: private */
    public DialogInterface.OnClickListener g = new ai(this);
    private View.OnClickListener h = new aj(this);
    private View.OnClickListener i = new ak(this);
    private View.OnClickListener j = new al(this);
    private View.OnClickListener k = new am(this);
    private View.OnClickListener l = new an(this);
    private View.OnClickListener m = new ao(this);
    private View.OnClickListener n = new af(this);
    private View.OnClickListener o = new ag(this);

    public ad(Activity activity) {
        this.b = activity;
        this.f1216a = LayoutInflater.from(this.b);
    }

    public void a() {
        x.a().a(b.OBSERVER_RING_UPLOAD, this.e);
        x.a().a(b.OBSERVER_PLAY_STATUS, this.f);
    }

    public void b() {
        x.a().b(b.OBSERVER_RING_UPLOAD, this.e);
        x.a().b(b.OBSERVER_PLAY_STATUS, this.f);
    }

    private void a(View view, int i2) {
        String string;
        MakeRingData makeRingData = (MakeRingData) com.shoujiduoduo.a.b.b.b().a("make_ring_list").a(i2);
        if (makeRingData != null) {
            TextView textView = (TextView) cc.a(view, R.id.item_song_name);
            TextView textView2 = (TextView) cc.a(view, R.id.item_artist);
            TextView textView3 = (TextView) cc.a(view, R.id.item_duration);
            ImageView imageView = (ImageView) cc.a(view, R.id.item_select_sign1);
            ImageView imageView2 = (ImageView) cc.a(view, R.id.item_select_sign2);
            ImageView imageView3 = (ImageView) cc.a(view, R.id.item_select_sign3);
            imageView.setVisibility(0);
            imageView2.setVisibility(0);
            imageView3.setVisibility(0);
            String a2 = as.a(this.b, "user_ring_phone_select", "ringtoneduoduo_not_set");
            String a3 = as.a(this.b, "user_ring_alarm_select", "ringtoneduoduo_not_set");
            String a4 = as.a(this.b, "user_ring_notification_select", "ringtoneduoduo_not_set");
            String a5 = as.a(this.b, "user_ring_phone_select", "ringtoneduoduo_not_set");
            String a6 = as.a(this.b, "user_ring_alarm_select", "ringtoneduoduo_not_set");
            String a7 = as.a(this.b, "user_ring_notification_select", "ringtoneduoduo_not_set");
            boolean equals = a2.equals("ringtoneduoduo_not_set") ? a5.equals(makeRingData.e) : a2.equalsIgnoreCase(makeRingData.g);
            boolean equals2 = a4.equals("ringtoneduoduo_not_set") ? a7.equals(makeRingData.e) : a4.equalsIgnoreCase(makeRingData.g);
            boolean equals3 = a3.equals("ringtoneduoduo_not_set") ? a6.equals(makeRingData.e) : a3.equalsIgnoreCase(makeRingData.g);
            imageView.setImageResource(equals ? R.drawable.ring_incoming_call_true : R.drawable.ring_incoming_call_false);
            imageView2.setImageResource(equals2 ? R.drawable.ring_notification_true : R.drawable.ring_notification_false);
            imageView3.setImageResource(equals3 ? R.drawable.ring_alarm_true : R.drawable.ring_alarm_false);
            textView.setText(makeRingData.e);
            textView2.setText(makeRingData.c);
            if (makeRingData.f814a != 0) {
                string = this.b.getResources().getString(R.string.upload_suc);
            } else if (makeRingData.b == -1) {
                string = this.b.getResources().getString(R.string.upload_error);
            } else if (makeRingData.b == 0) {
                string = "";
            } else {
                string = this.b.getResources().getString(R.string.uploading) + String.valueOf(makeRingData.b) + "%";
            }
            if (!com.shoujiduoduo.a.b.b.g().g()) {
                textView3.setText(String.format("%02d:%02d", Integer.valueOf(makeRingData.j / 60), Integer.valueOf(makeRingData.j % 60)));
            } else {
                textView3.setText(String.format("%02d:%02d %s", Integer.valueOf(makeRingData.j / 60), Integer.valueOf(makeRingData.j % 60), string));
            }
            if (makeRingData.j == 0) {
                textView3.setVisibility(4);
            } else {
                textView3.setVisibility(0);
            }
        }
    }

    public int getCount() {
        return com.shoujiduoduo.a.b.b.b().a("make_ring_list").c();
    }

    public Object getItem(int i2) {
        if (i2 < com.shoujiduoduo.a.b.b.b().a("make_ring_list").c()) {
            return com.shoujiduoduo.a.b.b.b().a("make_ring_list").a(i2);
        }
        return null;
    }

    public long getItemId(int i2) {
        return (long) i2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int i2, View view, ViewGroup viewGroup) {
        if (i2 < com.shoujiduoduo.a.b.b.b().a("make_ring_list").c()) {
            if (view == null) {
                view = this.f1216a.inflate((int) R.layout.listitem_ring, viewGroup, false);
            }
            a(view, i2);
            ProgressBar progressBar = (ProgressBar) cc.a(view, R.id.ringitem_download_progress);
            TextView textView = (TextView) cc.a(view, R.id.ringitem_serial_number);
            ImageButton imageButton = (ImageButton) cc.a(view, R.id.ringitem_play);
            ImageButton imageButton2 = (ImageButton) cc.a(view, R.id.ringitem_pause);
            ImageButton imageButton3 = (ImageButton) cc.a(view, R.id.ringitem_failed);
            imageButton.setOnClickListener(this.m);
            imageButton2.setOnClickListener(this.n);
            imageButton3.setOnClickListener(this.o);
            String str = "";
            PlayerService b2 = ak.a().b();
            if (b2 != null) {
                str = b2.b();
                this.c = b2.c();
            }
            if (i2 == this.c && str.equals(com.shoujiduoduo.a.b.b.b().a("make_ring_list").a())) {
                MakeRingData makeRingData = (MakeRingData) com.shoujiduoduo.a.b.b.b().a("make_ring_list").a(i2);
                Button button = (Button) cc.a(view, R.id.ring_item_button0);
                Button button2 = (Button) cc.a(view, R.id.ring_item_button1);
                Button button3 = (Button) cc.a(view, R.id.ring_item_button2);
                Button button4 = (Button) cc.a(view, R.id.ring_item_button4);
                button.setVisibility(0);
                if ((makeRingData.m.equals("") || makeRingData.f814a != 1 || makeRingData.g.equals("")) && (!makeRingData.m.equals("") || makeRingData.g.equals(""))) {
                    button.setOnClickListener(this.i);
                    button.setCompoundDrawablesWithIntrinsicBounds((int) R.drawable.icon_ringitem_upload, 0, 0, 0);
                    button.setText((int) R.string.upload);
                    button4.setVisibility(8);
                } else {
                    button.setOnClickListener(this.h);
                    button.setCompoundDrawablesWithIntrinsicBounds((int) R.drawable.icon_ringitem_share, 0, 0, 0);
                    button.setText((int) R.string.share);
                    if (!"false".equals(com.umeng.analytics.b.c(RingDDApp.c(), "quick_show_upload_switch"))) {
                        button4.setVisibility(0);
                    } else {
                        button4.setVisibility(8);
                    }
                }
                button4.setOnClickListener(this.l);
                button2.setVisibility(0);
                button2.setOnClickListener(this.j);
                button3.setVisibility(0);
                button3.setOnClickListener(this.k);
                textView.setVisibility(4);
                progressBar.setVisibility(4);
                imageButton.setVisibility(4);
                imageButton2.setVisibility(4);
                imageButton3.setVisibility(4);
                PlayerService b3 = ak.a().b();
                RingData ringData = (RingData) com.shoujiduoduo.a.b.b.b().a("make_ring_list").a(i2);
                if (this.d && b3 != null && b3.f() == ringData.k()) {
                    switch (b3.a()) {
                        case 1:
                            progressBar.setVisibility(0);
                            break;
                        case 2:
                            imageButton2.setVisibility(0);
                            break;
                        case 3:
                        case 4:
                        case 5:
                            imageButton.setVisibility(0);
                            break;
                        case 6:
                            imageButton3.setVisibility(0);
                            break;
                    }
                } else {
                    imageButton.setVisibility(0);
                }
            } else {
                ((Button) cc.a(view, R.id.ring_item_button0)).setVisibility(8);
                ((Button) cc.a(view, R.id.ring_item_button1)).setVisibility(8);
                ((Button) cc.a(view, R.id.ring_item_button2)).setVisibility(8);
                ((Button) cc.a(view, R.id.ring_item_button4)).setVisibility(8);
                textView.setText(Integer.toString(i2 + 1));
                textView.setVisibility(0);
                progressBar.setVisibility(4);
                imageButton.setVisibility(4);
                imageButton2.setVisibility(4);
                imageButton3.setVisibility(4);
            }
        }
        return view;
    }
}
