package com.shoujiduoduo.ui.mine.changering;

import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.ui.mine.changering.RingSettingFragment;
import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;

/* compiled from: RingSettingFragment */
class r extends a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RingSettingFragment f1289a;

    r(RingSettingFragment ringSettingFragment) {
        this.f1289a = ringSettingFragment;
    }

    public void a(c.b bVar) {
        super.a(bVar);
        if (bVar != null && (bVar instanceof c.x)) {
            c.x xVar = (c.x) bVar;
            if (xVar.a() == null || xVar.d() == null || xVar.d().size() <= 0) {
                com.shoujiduoduo.base.a.a.c("RingSettingFragment", "查询默认彩铃失败，code:" + bVar.a() + " msg:" + bVar.b());
                this.f1289a.e();
                return;
            }
            String unused = this.f1289a.d = xVar.d().get(0).b();
            String unused2 = this.f1289a.b = xVar.d().get(0).c();
            String unused3 = this.f1289a.c = xVar.d().get(0).d();
            RingSettingFragment.b unused4 = this.f1289a.l = RingSettingFragment.b.success;
            RingData ringData = new RingData();
            ringData.n = this.f1289a.d;
            ringData.q = 0;
            ringData.e = this.f1289a.b + "-" + this.f1289a.c;
            ringData.j = 48;
            if (this.f1289a.e.size() > 3) {
                ((RingSettingFragment.e) this.f1289a.e.get(3)).c = 48000;
                ((RingSettingFragment.e) this.f1289a.e.get(3)).b = this.f1289a.b + "-" + this.f1289a.c;
                ((RingSettingFragment.e) this.f1289a.e.get(3)).f1263a = ringData;
            }
            this.f1289a.f.notifyDataSetChanged();
        }
    }

    public void b(c.b bVar) {
        super.b(bVar);
        com.shoujiduoduo.base.a.a.c("RingSettingFragment", "查询默认彩铃失败，code:" + bVar.a() + " msg:" + bVar.b());
        this.f1289a.e();
    }
}
