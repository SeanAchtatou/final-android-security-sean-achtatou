package com.shoujiduoduo.ui.mine;

import android.content.Intent;
import android.view.View;
import com.shoujiduoduo.a.b.b;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ringtone.activity.RingToneDuoduoActivity;
import com.shoujiduoduo.ui.home.MusicAlbumActivity;
import com.tencent.open.SocialConstants;

/* compiled from: MakeRingListAdapter */
class an implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ad f1226a;

    an(ad adVar) {
        this.f1226a = adVar;
    }

    public void onClick(View view) {
        a.a("MyRingMakeAdapter", "RingtoneDuoduo: click weixiu button!");
        RingData ringData = (RingData) b.b().a("make_ring_list").a(this.f1226a.c);
        Intent intent = new Intent(RingDDApp.c(), MusicAlbumActivity.class);
        intent.putExtra("musicid", ringData.g);
        intent.putExtra("title", "快秀");
        intent.putExtra(SocialConstants.PARAM_APP_DESC, ringData.e);
        intent.putExtra("type", MusicAlbumActivity.a.create_ring_story);
        RingToneDuoduoActivity.a().startActivity(intent);
    }
}
