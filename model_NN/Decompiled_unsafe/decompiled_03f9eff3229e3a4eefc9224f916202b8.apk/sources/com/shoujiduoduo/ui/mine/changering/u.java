package com.shoujiduoduo.ui.mine.changering;

import android.text.TextUtils;
import android.view.View;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ui.mine.changering.RingSettingFragment;
import com.shoujiduoduo.util.ak;
import com.shoujiduoduo.util.widget.f;

/* compiled from: RingSettingFragment */
class u implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f1292a;
    final /* synthetic */ RingSettingFragment.c b;

    u(RingSettingFragment.c cVar, int i) {
        this.b = cVar;
        this.f1292a = i;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public void onClick(View view) {
        int unused = RingSettingFragment.this.h = this.f1292a;
        switch (((RingSettingFragment.e) RingSettingFragment.this.e.get(this.f1292a)).d) {
            case ringtone:
            case notification:
            case alarm:
                RingData ringData = ((RingSettingFragment.e) RingSettingFragment.this.e.get(this.f1292a)).f1263a;
                if (ringData == null) {
                    return;
                }
                if (TextUtils.isEmpty(ringData.m)) {
                    f.a((int) R.string.no_ring_hint);
                    return;
                }
                break;
            case cailing:
                switch (RingSettingFragment.this.l) {
                    case uninit:
                        f.a("请先设置一首彩铃吧");
                        return;
                    case fail:
                        f.a("未查询到您的当前彩铃");
                        return;
                    case querying:
                        f.a("正在查询彩铃状态，请稍候...");
                        return;
                    case success:
                    default:
                        if (((RingSettingFragment.e) RingSettingFragment.this.e.get(this.f1292a)).f1263a == null) {
                            a.c("RingSettingFragment", "cailing data error");
                            return;
                        }
                        break;
                }
        }
        PlayerService b2 = ak.a().b();
        if (b2 != null) {
            b2.a(((RingSettingFragment.e) RingSettingFragment.this.e.get(this.f1292a)).f1263a);
            RingSettingFragment.this.f.notifyDataSetChanged();
            return;
        }
        a.c("RingSettingFragment", "PlayerService is unavailable!");
    }
}
