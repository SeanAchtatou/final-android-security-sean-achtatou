package com.shoujiduoduo.ui.mine;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import com.shoujiduoduo.b.f.ai;
import com.shoujiduoduo.base.bean.MakeRingData;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.util.as;

/* compiled from: RingUploadDialog */
public class az extends Dialog {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Context f1239a;
    /* access modifiers changed from: private */
    public MakeRingData b;
    private EditText c;
    /* access modifiers changed from: private */
    public EditText d;
    private ImageButton e;
    private Button f;
    /* access modifiers changed from: private */
    public RadioGroup g;
    /* access modifiers changed from: private */
    public RadioGroup h;
    private RadioButton[] i = new RadioButton[10];
    /* access modifiers changed from: private */
    public ai j;
    /* access modifiers changed from: private */
    public int k;

    public az(Context context, int i2, RingData ringData) {
        super(context, i2);
        this.f1239a = context;
        this.b = (MakeRingData) ringData;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.dialog_ring_upload);
        this.j = new ai();
        String a2 = as.a(this.f1239a, "user_upload_name", "");
        this.c = (EditText) findViewById(R.id.et_upload_name);
        this.c.setText(this.b.e);
        this.d = (EditText) findViewById(R.id.et_user_name);
        this.d.setText(a2);
        this.g = (RadioGroup) findViewById(R.id.group1);
        this.h = (RadioGroup) findViewById(R.id.group2);
        this.i[0] = (RadioButton) findViewById(R.id.liuxingjinqu);
        this.i[1] = (RadioButton) findViewById(R.id.xinxiduanxi);
        this.i[2] = (RadioButton) findViewById(R.id.yingshiguanggao);
        this.i[3] = (RadioButton) findViewById(R.id.djwuqu);
        this.i[4] = (RadioButton) findViewById(R.id.dongmanyouxi);
        this.i[5] = (RadioButton) findViewById(R.id.gaoxiaolinglei);
        this.i[6] = (RadioButton) findViewById(R.id.rihanchaoliu);
        this.i[7] = (RadioButton) findViewById(R.id.chunyinyue);
        this.i[8] = (RadioButton) findViewById(R.id.oumeiliuxing);
        this.i[9] = (RadioButton) findViewById(R.id.yueyuliuxing);
        for (int i2 = 0; i2 <= 9; i2++) {
            this.i[i2].setOnCheckedChangeListener(new ba(this));
        }
        this.e = (ImageButton) findViewById(R.id.upload_close);
        this.e.setOnClickListener(new bb(this));
        this.f = (Button) findViewById(R.id.btn_upload);
        this.f.setOnClickListener(new bc(this));
    }
}
