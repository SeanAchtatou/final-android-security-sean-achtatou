package com.shoujiduoduo.ui.mine;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.a.b.b;
import com.shoujiduoduo.a.c.f;
import com.shoujiduoduo.a.c.v;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.ringtone.R;

public class UserCollectFragment extends Fragment {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public ListView f1211a;
    /* access modifiers changed from: private */
    public bd b;
    private View c;
    private Button d;
    /* access modifiers changed from: private */
    public Button e;
    /* access modifiers changed from: private */
    public int f;
    /* access modifiers changed from: private */
    public boolean g;
    /* access modifiers changed from: private */
    public boolean h;
    private TextView i;
    private View.OnKeyListener j = new be(this);
    private View.OnClickListener k = new bf(this);
    private View.OnClickListener l = new bg(this);
    private f m = new bj(this);
    private v n = new bk(this);
    private AdapterView.OnItemClickListener o = new bl(this);

    static /* synthetic */ int f(UserCollectFragment userCollectFragment) {
        int i2 = userCollectFragment.f;
        userCollectFragment.f = i2 + 1;
        return i2;
    }

    static /* synthetic */ int g(UserCollectFragment userCollectFragment) {
        int i2 = userCollectFragment.f;
        userCollectFragment.f = i2 - 1;
        return i2;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        a.a("UserCollectFragment", "onCreate");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        a.a("UserCollectFragment", "onCreateView");
        View inflate = layoutInflater.inflate((int) R.layout.my_ringtone_collect, viewGroup, false);
        this.f1211a = (ListView) inflate.findViewById(R.id.my_ringtone_collect);
        this.f1211a.setOnItemClickListener(this.o);
        this.b = new bd(getActivity(), b.b().d());
        this.i = (TextView) inflate.findViewById(R.id.hint);
        if (b.b().c()) {
            a.a("UserCollectFragment", "data is ready 1");
            this.f1211a.setAdapter((ListAdapter) this.b);
            b();
            this.h = true;
        } else {
            a.a("UserCollectFragment", "data is not ready");
        }
        this.c = (LinearLayout) inflate.findViewById(R.id.del_confirm);
        this.d = (Button) this.c.findViewById(R.id.cancel);
        this.d.setOnClickListener(this.k);
        this.e = (Button) this.c.findViewById(R.id.delete);
        this.e.setOnClickListener(this.l);
        this.c.setVisibility(4);
        x.a().a(com.shoujiduoduo.a.a.b.OBSERVER_USER_RING, this.n);
        x.a().a(com.shoujiduoduo.a.a.b.OBSERVER_LIST_DATA, this.m);
        return inflate;
    }

    public void onDestroyView() {
        this.h = false;
        this.g = false;
        x.a().b(com.shoujiduoduo.a.a.b.OBSERVER_USER_RING, this.n);
        x.a().b(com.shoujiduoduo.a.a.b.OBSERVER_LIST_DATA, this.m);
        super.onDestroyView();
        a.a("UserCollectFragment", "onDestroyView");
    }

    public void onDestroy() {
        super.onDestroy();
        a.a("UserCollectFragment", "onDestroy");
    }

    public void onResume() {
        a.a("UserCollectFragment", "onResume");
        super.onResume();
        getView().setFocusable(true);
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(this.j);
    }

    public boolean a() {
        return this.g;
    }

    public void a(boolean z) {
        if (this.g != z && this.h) {
            this.g = z;
            this.c.setVisibility(z ? 0 : 8);
            this.b.a(b.b().d());
            this.b.a(z);
            if (z) {
                this.f = 0;
                this.e.setText("删除");
            }
        }
    }

    /* access modifiers changed from: private */
    public void b() {
        if (b.b().d().c() > 0) {
            this.i.setVisibility(8);
        } else {
            this.i.setVisibility(0);
        }
    }
}
