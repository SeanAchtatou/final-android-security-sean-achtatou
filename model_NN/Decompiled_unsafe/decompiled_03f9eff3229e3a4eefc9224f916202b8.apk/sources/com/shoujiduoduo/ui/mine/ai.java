package com.shoujiduoduo.ui.mine;

import android.content.DialogInterface;
import com.shoujiduoduo.a.b.b;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.util.ak;

/* compiled from: MakeRingListAdapter */
class ai implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ad f1221a;

    ai(ad adVar) {
        this.f1221a = adVar;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        switch (i) {
            case -2:
            default:
                return;
            case -1:
                PlayerService b = ak.a().b();
                if (b != null && b.b().a("make_ring_list").a(this.f1221a.c) != null) {
                    if (b.f() == ((RingData) b.b().a("make_ring_list").a(this.f1221a.c)).k()) {
                        b.e();
                    }
                    int a2 = this.f1221a.c;
                    int unused = this.f1221a.c = -1;
                    b.b().a("make_ring_list", a2);
                    return;
                }
                return;
        }
    }
}
