package com.shoujiduoduo.ui.mine;

import android.view.View;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.util.widget.a;
import com.shoujiduoduo.util.widget.f;
import java.util.List;

/* compiled from: MakeRingFragment */
class x implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ MakeRingFragment f1318a;

    x(MakeRingFragment makeRingFragment) {
        this.f1318a = makeRingFragment;
    }

    public void onClick(View view) {
        List<Integer> b = this.f1318a.i.b();
        if (b == null || b.size() == 0) {
            f.a("请选择要删除的铃声", 0);
        } else {
            new a.C0034a(this.f1318a.getActivity()).b((int) R.string.hint).a("确定删除选中的铃声吗？").a((int) R.string.ok, new z(this, b)).b((int) R.string.cancel, new y(this)).a().show();
        }
    }
}
