package com.shoujiduoduo.ui.mine;

import android.view.View;
import com.shoujiduoduo.a.b.b;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.util.ak;

/* compiled from: MakeRingListAdapter */
class ag implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ad f1219a;

    ag(ad adVar) {
        this.f1219a = adVar;
    }

    public void onClick(View view) {
        PlayerService b;
        if (this.f1219a.c >= 0 && (b = ak.a().b()) != null) {
            b.a(b.b().a("make_ring_list"), this.f1219a.c);
        }
    }
}
