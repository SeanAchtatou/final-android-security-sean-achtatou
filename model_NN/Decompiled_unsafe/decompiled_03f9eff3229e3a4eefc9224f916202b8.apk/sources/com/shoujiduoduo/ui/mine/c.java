package com.shoujiduoduo.ui.mine;

import android.view.KeyEvent;
import android.view.View;
import com.shoujiduoduo.base.a.a;

/* compiled from: FavoriteRingFragment */
class c implements View.OnKeyListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ FavoriteRingFragment f1254a;

    c(FavoriteRingFragment favoriteRingFragment) {
        this.f1254a = favoriteRingFragment;
    }

    public boolean onKey(View view, int i, KeyEvent keyEvent) {
        if (i != 4 || keyEvent.getAction() != 0 || !this.f1254a.h) {
            return false;
        }
        a.a("FavoriteRingFrag", "edit mode, key back");
        this.f1254a.a(false);
        return true;
    }
}
