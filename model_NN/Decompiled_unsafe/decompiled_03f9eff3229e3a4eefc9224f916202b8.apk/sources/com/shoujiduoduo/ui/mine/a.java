package com.shoujiduoduo.ui.mine;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import com.shoujiduoduo.a.b.b;
import com.shoujiduoduo.base.bean.MakeRingData;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.base.bean.c;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ui.utils.cc;
import com.shoujiduoduo.util.as;
import java.util.ArrayList;
import java.util.List;

/* compiled from: EditModeAdapter */
public class a extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    private Context f1212a;
    private c b;
    private List<Boolean> c = new ArrayList();
    private String d;

    public a(Context context, c cVar, String str) {
        this.f1212a = context;
        this.b = cVar;
        this.d = str;
        for (int i = 0; i < cVar.c(); i++) {
            this.c.add(false);
        }
    }

    public void a(c cVar) {
        this.b = cVar;
        if (cVar.c() > 0) {
            this.c.clear();
            this.c = null;
            this.c = new ArrayList();
            for (int i = 0; i < cVar.c(); i++) {
                this.c.add(false);
            }
        }
    }

    public List<Boolean> a() {
        return this.c;
    }

    public List<Integer> b() {
        ArrayList arrayList;
        ArrayList arrayList2 = null;
        int i = 0;
        while (i < this.c.size()) {
            if (this.c.get(i).booleanValue()) {
                if (arrayList2 == null) {
                    arrayList = new ArrayList();
                } else {
                    arrayList = arrayList2;
                }
                arrayList.add(Integer.valueOf(i));
            } else {
                arrayList = arrayList2;
            }
            i++;
            arrayList2 = arrayList;
        }
        return arrayList2;
    }

    public int getCount() {
        if (this.b != null) {
            return this.b.c();
        }
        return 0;
    }

    public Object getItem(int i) {
        if (i < this.b.c()) {
            return this.b.a(i);
        }
        return null;
    }

    public long getItemId(int i) {
        return (long) i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (this.b != null && i < this.b.c()) {
            if (view == null) {
                view = LayoutInflater.from(this.f1212a).inflate((int) R.layout.ring_item_delete_mode, viewGroup, false);
            }
            ((CheckBox) cc.a(view, R.id.checkbox)).setChecked(this.c.get(i).booleanValue());
            if (this.d.equals("favorite_ring_list")) {
                a(view, i);
            } else if (this.d.equals("make_ring_list")) {
                b(view, i);
            }
        }
        return view;
    }

    private void a(View view, int i) {
        RingData ringData = (RingData) b.b().a("favorite_ring_list").a(i);
        TextView textView = (TextView) cc.a(view, R.id.item_duration);
        ImageView imageView = (ImageView) cc.a(view, R.id.item_select_sign1);
        ImageView imageView2 = (ImageView) cc.a(view, R.id.item_select_sign2);
        ImageView imageView3 = (ImageView) cc.a(view, R.id.item_select_sign3);
        imageView.setVisibility(0);
        imageView2.setVisibility(0);
        imageView3.setVisibility(0);
        ((TextView) cc.a(view, R.id.item_song_name)).setText(ringData.e);
        ((TextView) cc.a(view, R.id.item_artist)).setText(ringData.f);
        textView.setText(String.format("%02d:%02d", Integer.valueOf(ringData.j / 60), Integer.valueOf(ringData.j % 60)));
        if (ringData.j == 0) {
            textView.setVisibility(4);
        } else {
            textView.setVisibility(0);
        }
        imageView.setVisibility(0);
        imageView2.setVisibility(0);
        imageView3.setVisibility(0);
        String a2 = as.a(this.f1212a, "user_ring_phone_select", "ringtoneduoduo_not_set");
        String a3 = as.a(this.f1212a, "user_ring_alarm_select", "ringtoneduoduo_not_set");
        String a4 = as.a(this.f1212a, "user_ring_notification_select", "ringtoneduoduo_not_set");
        imageView.setImageResource(a2.equalsIgnoreCase(ringData.g) ? R.drawable.ring_incoming_call_true : R.drawable.ring_incoming_call_false);
        imageView2.setImageResource(a4.equalsIgnoreCase(ringData.g) ? R.drawable.ring_notification_true : R.drawable.ring_notification_false);
        imageView3.setImageResource(a3.equalsIgnoreCase(ringData.g) ? R.drawable.ring_alarm_true : R.drawable.ring_alarm_false);
    }

    private void b(View view, int i) {
        String string;
        MakeRingData makeRingData = (MakeRingData) b.b().a("make_ring_list").a(i);
        if (makeRingData != null) {
            TextView textView = (TextView) cc.a(view, R.id.item_duration);
            ((ImageView) cc.a(view, R.id.item_select_sign1)).setVisibility(0);
            ((ImageView) cc.a(view, R.id.item_select_sign2)).setVisibility(0);
            ((ImageView) cc.a(view, R.id.item_select_sign3)).setVisibility(0);
            ((TextView) cc.a(view, R.id.item_song_name)).setText(makeRingData.e);
            ((TextView) cc.a(view, R.id.item_artist)).setText(makeRingData.c);
            if (makeRingData.f814a != 0) {
                string = this.f1212a.getResources().getString(R.string.upload_suc);
            } else if (makeRingData.b == -1) {
                string = this.f1212a.getResources().getString(R.string.upload_error);
            } else if (makeRingData.b == 0) {
                string = "";
            } else {
                string = this.f1212a.getResources().getString(R.string.uploading) + String.valueOf(makeRingData.b) + "%";
            }
            if (!b.g().g()) {
                textView.setText(String.format("%02d:%02d", Integer.valueOf(makeRingData.j / 60), Integer.valueOf(makeRingData.j % 60)));
            } else {
                textView.setText(String.format("%02d:%02d %s", Integer.valueOf(makeRingData.j / 60), Integer.valueOf(makeRingData.j % 60), string));
            }
            if (makeRingData.j == 0) {
                textView.setVisibility(4);
            } else {
                textView.setVisibility(0);
            }
        }
    }
}
