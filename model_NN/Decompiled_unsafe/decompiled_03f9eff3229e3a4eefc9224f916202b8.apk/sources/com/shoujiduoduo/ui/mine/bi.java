package com.shoujiduoduo.ui.mine;

import android.content.DialogInterface;
import com.shoujiduoduo.a.b.b;
import com.shoujiduoduo.util.widget.f;
import java.util.List;

/* compiled from: UserCollectFragment */
class bi implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ List f1250a;
    final /* synthetic */ bg b;

    bi(bg bgVar, List list) {
        this.b = bgVar;
        this.f1250a = list;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        if (b.b().a(this.f1250a)) {
            f.a("已删除" + this.f1250a.size() + "个精选集", 0);
            this.b.f1248a.a(false);
        } else {
            f.a("删除精选集失败", 0);
        }
        dialogInterface.dismiss();
    }
}
