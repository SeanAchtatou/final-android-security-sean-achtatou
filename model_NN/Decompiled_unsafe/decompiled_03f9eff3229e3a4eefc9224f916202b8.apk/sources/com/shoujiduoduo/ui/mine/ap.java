package com.shoujiduoduo.ui.mine;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TextView;
import com.d.a.b.d;
import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.a.c.q;
import com.shoujiduoduo.a.c.u;
import com.shoujiduoduo.a.c.w;
import com.shoujiduoduo.b.c.n;
import com.shoujiduoduo.base.bean.ab;
import com.shoujiduoduo.base.bean.f;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ui.user.UserCenterActivity;
import com.shoujiduoduo.ui.user.UserLoginActivity;
import com.shoujiduoduo.ui.utils.DDListFragment;
import com.shoujiduoduo.ui.utils.pageindicator.TabPageIndicator;
import com.shoujiduoduo.ui.utils.v;
import com.shoujiduoduo.util.ak;
import com.shoujiduoduo.util.as;
import com.shoujiduoduo.util.av;
import com.shoujiduoduo.util.c.b;
import com.shoujiduoduo.util.f;
import com.umeng.analytics.b;
import java.util.ArrayList;

/* compiled from: MyRingtoneScene */
public class ap implements View.OnClickListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Activity f1228a;
    /* access modifiers changed from: private */
    public ViewPager b;
    /* access modifiers changed from: private */
    public ArrayList<Fragment> c;
    /* access modifiers changed from: private */
    public FavoriteRingFragment d;
    /* access modifiers changed from: private */
    public MakeRingFragment e;
    /* access modifiers changed from: private */
    public UserCollectFragment f;
    /* access modifiers changed from: private */
    public RelativeLayout g;
    private TextView h;
    private Button i;
    private ImageButton j;
    /* access modifiers changed from: private */
    public boolean k;
    /* access modifiers changed from: private */
    public RelativeLayout l;
    private RelativeLayout m;
    private RelativeLayout n;
    private RelativeLayout o;
    private RelativeLayout p;
    /* access modifiers changed from: private */
    public TabPageIndicator q;
    /* access modifiers changed from: private */
    public String[] r = {"收藏", "彩铃", "精选集", "作品"};
    /* access modifiers changed from: private */
    public String[] s = {"收藏", "精选集", "作品"};
    /* access modifiers changed from: private */
    public boolean t;
    /* access modifiers changed from: private */
    public boolean u;
    private u v = new aw(this);
    private w w = new ax(this);
    private q x = new ay(this);

    public ap(Activity activity) {
        this.f1228a = activity;
        this.d = new FavoriteRingFragment();
        this.e = new MakeRingFragment();
        this.f = new UserCollectFragment();
    }

    public void a() {
        int i2 = 0;
        com.shoujiduoduo.base.a.a.b("MyRingtoneScene", "MyRingtoneScene initScene in");
        if (this.f1228a != null) {
            this.l = (RelativeLayout) this.f1228a.findViewById(R.id.user_layout);
            j();
            this.l.setOnClickListener(this);
            this.c = new ArrayList<>();
            this.c.add(this.d);
            String a2 = as.a(RingDDApp.c(), "pref_phone_num", "");
            switch (f.u()) {
                case f1671a:
                    d();
                    break;
                case ct:
                    b(a2);
                    break;
                case cu:
                    a(a2);
                    break;
                default:
                    this.u = false;
                    break;
            }
            this.c.add(this.f);
            this.c.add(this.e);
            this.b = (ViewPager) this.f1228a.findViewById(R.id.vp_myring_pager);
            this.b.setAdapter(new a(((FragmentActivity) this.f1228a).getSupportFragmentManager()));
            this.b.setOffscreenPageLimit(3);
            this.q = (TabPageIndicator) this.f1228a.findViewById(R.id.my_ring_indicator);
            this.q.setViewPager(this.b);
            this.t = b.c(this.f1228a, "share_app_switch").equals("true");
            this.g = (RelativeLayout) this.f1228a.findViewById(R.id.share_app_layout);
            this.h = (TextView) this.g.findViewById(R.id.share_tips);
            String c2 = b.c(this.f1228a, "share_app_entrance");
            if (!TextUtils.isEmpty(c2)) {
                this.h.setText(c2);
            }
            this.i = (Button) this.g.findViewById(R.id.share_btn);
            this.i.setOnClickListener(this);
            this.j = (ImageButton) this.g.findViewById(R.id.close_btn);
            this.j.setOnClickListener(this);
            RelativeLayout relativeLayout = this.g;
            if (!this.t) {
                i2 = 8;
            }
            relativeLayout.setVisibility(i2);
            x.a().a(com.shoujiduoduo.a.a.b.OBSERVER_VIP, this.w);
            x.a().a(com.shoujiduoduo.a.a.b.OBSERVER_SCENE, this.x);
            x.a().a(com.shoujiduoduo.a.a.b.OBSERVER_USER_CENTER, this.v);
            com.shoujiduoduo.base.a.a.b("MyRingtoneScene", "MyRingtoneScene initScene out");
        }
    }

    private void a(String str) {
        if (!av.c(str)) {
            com.shoujiduoduo.base.a.a.a("MyRingtoneScene", "检查用户彩铃状态");
            if (com.shoujiduoduo.util.e.a.a().a(str)) {
                com.shoujiduoduo.util.e.a.a().f(new aq(this, str));
            }
        }
    }

    private void b(String str) {
        if (!av.c(str)) {
            com.shoujiduoduo.base.a.a.a("MyRingtoneScene", "检查用户彩铃状态");
            com.shoujiduoduo.util.d.b.a().a(str, new ar(this));
        }
    }

    private void d() {
        if (com.shoujiduoduo.util.c.b.a().c().equals(b.a.success)) {
            com.shoujiduoduo.base.a.a.a("MyRingtoneScene", "cmcc sdk is inited, show cailing");
            this.u = true;
            n nVar = new n(f.a.list_ring_cmcc, "", false, "");
            DDListFragment dDListFragment = new DDListFragment();
            Bundle bundle = new Bundle();
            bundle.putBoolean("support_lazy_load", true);
            bundle.putString("adapter_type", "cailing_list_adapter");
            dDListFragment.setArguments(bundle);
            dDListFragment.a(nVar);
            this.c.add(dDListFragment);
            return;
        }
        com.shoujiduoduo.base.a.a.a("MyRingtoneScene", "cmcc sdk is not inited");
    }

    public void b() {
        x.a().b(com.shoujiduoduo.a.a.b.OBSERVER_VIP, this.w);
        x.a().b(com.shoujiduoduo.a.a.b.OBSERVER_SCENE, this.x);
        x.a().b(com.shoujiduoduo.a.a.b.OBSERVER_USER_CENTER, this.v);
    }

    private View e() {
        if (this.p == null) {
            this.p = (RelativeLayout) LayoutInflater.from(this.f1228a).inflate((int) R.layout.layout_userinfo_not_support_vip, (ViewGroup) null);
        }
        ab c2 = com.shoujiduoduo.a.b.b.g().c();
        d.a().a(c2.c(), (ImageView) this.p.findViewById(R.id.user_head), v.a().d());
        ((TextView) this.p.findViewById(R.id.user_name)).setText(c2.b());
        ((Button) this.p.findViewById(R.id.logout)).setOnClickListener(new as(this));
        return this.p;
    }

    private View f() {
        if (this.m == null) {
            this.m = (RelativeLayout) LayoutInflater.from(this.f1228a).inflate((int) R.layout.layout_userinfo_no_login, (ViewGroup) null);
        }
        ((Button) this.m.findViewById(R.id.login)).setOnClickListener(new au(this));
        return this.m;
    }

    private View g() {
        if (this.n == null) {
            this.n = (RelativeLayout) LayoutInflater.from(this.f1228a).inflate((int) R.layout.layout_userinfo_login, (ViewGroup) null);
        }
        ab c2 = com.shoujiduoduo.a.b.b.g().c();
        d.a().a(c2.c(), (ImageView) this.n.findViewById(R.id.user_head), v.a().d());
        ((TextView) this.n.findViewById(R.id.user_name)).setText(c2.b());
        ((Button) this.n.findViewById(R.id.open_vip)).setOnClickListener(new av(this));
        return this.n;
    }

    private View h() {
        if (this.o == null) {
            this.o = (RelativeLayout) LayoutInflater.from(this.f1228a).inflate((int) R.layout.layout_userinfo_login_vip, (ViewGroup) null);
        }
        ImageView imageView = (ImageView) this.o.findViewById(R.id.user_head);
        ab c2 = com.shoujiduoduo.a.b.b.g().c();
        if (!TextUtils.isEmpty(c2.c())) {
            d.a().a(c2.c(), imageView, v.a().d());
        } else {
            imageView.setImageResource(R.drawable.vip_headpic_small);
        }
        ((TextView) this.o.findViewById(R.id.user_name)).setText(c2.b());
        return this.o;
    }

    public boolean a(int i2, KeyEvent keyEvent) {
        int i3 = 0;
        if (i2 != 4 || keyEvent.getAction() != 0) {
            return false;
        }
        switch (this.b.getCurrentItem()) {
            case 0:
                if (!this.d.a()) {
                    return false;
                }
                this.d.a(false);
                if (!this.k) {
                    RelativeLayout relativeLayout = this.g;
                    if (!this.t) {
                        i3 = 8;
                    }
                    relativeLayout.setVisibility(i3);
                }
                return true;
            case 1:
                if (!this.f.a()) {
                    return false;
                }
                this.f.a(false);
                if (!this.k) {
                    RelativeLayout relativeLayout2 = this.g;
                    if (!this.t) {
                        i3 = 8;
                    }
                    relativeLayout2.setVisibility(i3);
                }
                return true;
            case 2:
                if (!this.e.a()) {
                    return false;
                }
                this.e.a(false);
                if (!this.k) {
                    RelativeLayout relativeLayout3 = this.g;
                    if (!this.t) {
                        i3 = 8;
                    }
                    relativeLayout3.setVisibility(i3);
                }
                return true;
            default:
                return false;
        }
    }

    /* access modifiers changed from: private */
    public void i() {
        boolean h2;
        n nVar;
        f.b u2 = com.shoujiduoduo.util.f.u();
        if (u2.equals(f.b.f1671a)) {
            h2 = com.shoujiduoduo.util.c.b.a().c().equals(b.a.success);
        } else if (u2.equals(f.b.cu) || u2.equals(f.b.ct)) {
            h2 = com.shoujiduoduo.a.b.b.g().h();
        } else {
            com.shoujiduoduo.base.a.a.c("MyRingtoneScene", "unknown service type");
            h2 = false;
        }
        if (h2) {
            if (this.c.size() == 3) {
                if (u2.equals(f.b.cu)) {
                    nVar = new n(f.a.list_ring_cucc, "", false, "");
                } else if (u2.equals(f.b.ct)) {
                    nVar = new n(f.a.list_ring_ctcc, "", false, "");
                } else if (u2.equals(f.b.f1671a)) {
                    nVar = new n(f.a.list_ring_cmcc, "", false, "");
                } else {
                    com.shoujiduoduo.base.a.a.c("MyRingtoneScene", "not support vip type, 不添加彩铃fragment");
                    return;
                }
                DDListFragment dDListFragment = new DDListFragment();
                Bundle bundle = new Bundle();
                bundle.putString("adapter_type", "cailing_list_adapter");
                dDListFragment.setArguments(bundle);
                dDListFragment.a(nVar);
                this.c.add(1, dDListFragment);
                com.shoujiduoduo.base.a.a.a("MyRingtoneScene", "add cailing frag, now fragsize:" + this.c.size());
                this.u = true;
                this.q.a();
                this.b.getAdapter().notifyDataSetChanged();
            }
        } else if (this.c.size() == 4) {
            this.c.remove(1);
            com.shoujiduoduo.base.a.a.a("MyRingtoneScene", "remove cailing frag, now fragsize:" + this.c.size());
            this.u = false;
            this.q.a();
            this.b.getAdapter().notifyDataSetChanged();
        }
    }

    /* access modifiers changed from: private */
    public void j() {
        this.l.removeAllViews();
        if (!com.shoujiduoduo.a.b.b.g().g()) {
            this.l.addView(f(), new TableLayout.LayoutParams(-1, -1));
        } else if (com.shoujiduoduo.a.b.b.g().h()) {
            this.l.addView(h(), new TableLayout.LayoutParams(-1, -1));
        } else if (com.shoujiduoduo.util.f.u().equals(f.b.f1671a)) {
            this.l.addView(e(), new TableLayout.LayoutParams(-1, -1));
        } else {
            this.l.addView(g(), new TableLayout.LayoutParams(-1, -1));
        }
    }

    public void c() {
        PlayerService b2 = ak.a().b();
        if (b2 != null && b2.j()) {
            b2.k();
        }
        this.g.setVisibility(8);
        switch (this.b.getCurrentItem()) {
            case 0:
                if (com.shoujiduoduo.a.b.b.b().a("favorite_ring_list").c() > 0) {
                    com.shoujiduoduo.base.a.a.a("MyRingtoneScene", "set favorite to edit mode");
                    this.d.a(true);
                    return;
                }
                return;
            case 1:
                if (this.c.size() == 3 && com.shoujiduoduo.a.b.b.b().a("collect_ring_list").c() > 0) {
                    com.shoujiduoduo.base.a.a.a("MyRingtoneScene", "set collect to edit mode");
                    this.f.a(true);
                    return;
                }
                return;
            case 2:
                if (this.c.size() == 4) {
                    if (com.shoujiduoduo.a.b.b.b().a("collect_ring_list").c() > 0) {
                        this.f.a(true);
                        com.shoujiduoduo.base.a.a.a("MyRingtoneScene", "set collect to edit mode");
                        return;
                    }
                    return;
                } else if (com.shoujiduoduo.a.b.b.b().a("make_ring_list").c() > 0) {
                    com.shoujiduoduo.base.a.a.a("MyRingtoneScene", "set make to edit mode");
                    this.e.a(true);
                    return;
                } else {
                    return;
                }
            case 3:
                if (this.c.size() == 4 && com.shoujiduoduo.a.b.b.b().a("make_ring_list").c() > 0) {
                    com.shoujiduoduo.base.a.a.a("MyRingtoneScene", "set make to edit mode");
                    this.e.a(true);
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void a(int i2) {
        if (this.q != null && this.q.getChildCount() > i2) {
            this.q.setCurrentItem(i2);
        }
    }

    /* compiled from: MyRingtoneScene */
    private class a extends FragmentStatePagerAdapter {
        public a(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        public CharSequence getPageTitle(int i) {
            return ap.this.u ? ap.this.r[i] : ap.this.s[i];
        }

        public Fragment getItem(int i) {
            if (ap.this.c != null) {
                return (Fragment) ap.this.c.get(i);
            }
            return null;
        }

        public int getItemPosition(Object obj) {
            return -2;
        }

        public int getCount() {
            if (ap.this.c != null) {
                return ap.this.c.size();
            }
            return 0;
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.share_btn:
            default:
                return;
            case R.id.user_layout:
                if (com.shoujiduoduo.a.b.b.g().g() && com.shoujiduoduo.a.b.b.g().h()) {
                    this.f1228a.startActivity(new Intent(this.f1228a, com.shoujiduoduo.a.b.b.g().g() ? UserCenterActivity.class : UserLoginActivity.class));
                    return;
                }
                return;
            case R.id.close_btn:
                this.k = true;
                this.g.setVisibility(8);
                return;
        }
    }
}
