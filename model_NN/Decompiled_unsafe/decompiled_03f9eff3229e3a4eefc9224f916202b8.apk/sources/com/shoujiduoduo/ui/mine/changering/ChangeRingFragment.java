package com.shoujiduoduo.ui.mine.changering;

import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import com.shoujiduoduo.b.c.n;
import com.shoujiduoduo.base.bean.f;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ui.utils.DDListFragment;
import com.shoujiduoduo.util.ak;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.List;

public class ChangeRingFragment extends Fragment {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Button[] f1255a = new Button[2];
    private int b = 0;
    /* access modifiers changed from: private */
    public int c;
    /* access modifiers changed from: private */
    public int d;
    /* access modifiers changed from: private */
    public float e;
    /* access modifiers changed from: private */
    public ImageView f;
    /* access modifiers changed from: private */
    public ViewPager g;
    private n[] h = new n[2];
    /* access modifiers changed from: private */
    public List<DDListFragment> i = new ArrayList();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        com.shoujiduoduo.base.a.a.a("ChangeRingFragment", "onCreateView");
        View inflate = layoutInflater.inflate((int) R.layout.fragment_change_ring, viewGroup, false);
        a(inflate);
        PlayerService.a(true);
        this.f1255a[0] = (Button) inflate.findViewById(R.id.btn_list);
        this.f1255a[0].setOnClickListener(new a(this));
        this.f1255a[1] = (Button) inflate.findViewById(R.id.btn_system);
        this.f1255a[1].setOnClickListener(new b(this));
        this.g = (ViewPager) inflate.findViewById(R.id.vp_myring_setting_pager);
        int i2 = getArguments().getInt("type", 0);
        com.shoujiduoduo.base.a.a.a("ChangeRingFragment", "change type:" + i2);
        switch (i2) {
            case 0:
                this.f1255a[0].setText((int) R.string.hot_ringtone);
                this.h[0] = new n(f.a.list_ring_recommon, "1", false, "");
                this.h[1] = new n(f.a.sys_ringtone);
                break;
            case 1:
                this.f1255a[0].setText((int) R.string.hot_notification);
                this.h[0] = new n(f.a.list_ring_recommon, "5", false, "");
                this.h[1] = new n(f.a.sys_notify);
                break;
            case 2:
                this.f1255a[0].setText((int) R.string.hot_alarm);
                this.h[0] = new n(f.a.list_ring_search, "闹钟", "input");
                this.h[1] = new n(f.a.sys_alarm);
                break;
            case 3:
                this.f1255a[0].setText((int) R.string.hot_coloring);
                this.f1255a[1].setText((int) R.string.manage_coloring);
                if (!com.shoujiduoduo.util.f.v()) {
                    if (!com.shoujiduoduo.util.f.x()) {
                        if (com.shoujiduoduo.util.f.w()) {
                            this.h[0] = new n(f.a.list_ring_recommon, "26", false, "");
                            this.h[1] = new n(f.a.list_ring_cucc, "", false, "");
                            break;
                        }
                    } else {
                        this.h[0] = new n(f.a.list_ring_recommon, Constants.VIA_REPORT_TYPE_QQFAVORITES, false, "");
                        this.h[1] = new n(f.a.list_ring_ctcc, "", false, "");
                        break;
                    }
                } else {
                    this.h[0] = new n(f.a.list_ring_recommon, "20", false, "");
                    this.h[1] = new n(f.a.list_ring_cmcc, "", false, "");
                    break;
                }
                break;
        }
        DDListFragment dDListFragment = new DDListFragment();
        Bundle bundle2 = new Bundle();
        bundle2.putString("adapter_type", "ring_list_adapter");
        dDListFragment.setArguments(bundle2);
        dDListFragment.a(this.h[0]);
        this.i.add(dDListFragment);
        if (i2 == 3) {
            DDListFragment dDListFragment2 = new DDListFragment();
            Bundle bundle3 = new Bundle();
            bundle3.putString("adapter_type", "cailing_list_adapter");
            bundle3.putBoolean("support_lazy_load", true);
            dDListFragment2.setArguments(bundle3);
            dDListFragment2.a(this.h[1]);
            this.i.add(dDListFragment2);
        } else {
            DDListFragment dDListFragment3 = new DDListFragment();
            Bundle bundle4 = new Bundle();
            bundle4.putString("adapter_type", "system_ring_list_adapter");
            bundle4.putBoolean("support_lazy_load", true);
            dDListFragment3.setArguments(bundle4);
            dDListFragment3.a(this.h[1]);
            this.i.add(dDListFragment3);
        }
        this.g.setAdapter(new a(getChildFragmentManager()));
        this.g.setOnPageChangeListener(new c(this));
        return inflate;
    }

    public void onDestroyView() {
        super.onDestroyView();
        com.shoujiduoduo.base.a.a.a("ChangeRingFragment", "onDestroyView");
        PlayerService.a(false);
        PlayerService b2 = ak.a().b();
        if (b2 != null && b2.j()) {
            b2.k();
        }
    }

    public void onDestroy() {
        com.shoujiduoduo.base.a.a.a("ChangeRingFragment", "onDestroy");
        super.onDestroy();
    }

    public void onDetach() {
        com.shoujiduoduo.base.a.a.a("ChangeRingFragment", "onDetach");
        super.onDetach();
    }

    public void onPause() {
        com.shoujiduoduo.base.a.a.a("ChangeRingFragment", "onPause");
        super.onPause();
    }

    public void onStop() {
        com.shoujiduoduo.base.a.a.a("ChangeRingFragment", "onStop");
        super.onStop();
    }

    private void a(View view) {
        this.f = (ImageView) view.findViewById(R.id.iv_myring_seperator);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        this.b = displayMetrics.widthPixels;
        this.c = this.b / 2;
        this.d = this.c / 4;
        int width = BitmapFactory.decodeResource(getResources(), R.drawable.page_bar).getWidth();
        int i2 = this.c - this.d;
        com.shoujiduoduo.base.a.a.a("ChangeRingFragment", "bmpW = " + width + ", showW = " + i2);
        this.e = ((float) i2) / ((float) width);
        Matrix matrix = new Matrix();
        matrix.setScale(this.e, 1.0f);
        matrix.postTranslate((float) (this.d / 2), 0.0f);
        this.f.setImageMatrix(matrix);
    }

    private class a extends FragmentPagerAdapter {
        public a(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        public Fragment getItem(int i) {
            return (Fragment) ChangeRingFragment.this.i.get(i);
        }

        public int getCount() {
            return ChangeRingFragment.this.i.size();
        }
    }
}
