package com.shoujiduoduo.ui.mine;

import android.view.KeyEvent;
import android.view.View;
import com.shoujiduoduo.base.a.a;

/* compiled from: MakeRingFragment */
class v implements View.OnKeyListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ MakeRingFragment f1316a;

    v(MakeRingFragment makeRingFragment) {
        this.f1316a = makeRingFragment;
    }

    public boolean onKey(View view, int i, KeyEvent keyEvent) {
        if (i != 4 || keyEvent.getAction() != 0 || !this.f1316a.c) {
            return false;
        }
        a.a("MakeRingFragment", "edit mode, key back");
        this.f1316a.a(false);
        return true;
    }
}
