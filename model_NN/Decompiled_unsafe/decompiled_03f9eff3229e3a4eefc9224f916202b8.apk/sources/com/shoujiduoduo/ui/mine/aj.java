package com.shoujiduoduo.ui.mine;

import android.view.View;
import com.shoujiduoduo.a.b.b;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.ringtone.activity.RingToneDuoduoActivity;
import com.shoujiduoduo.util.ax;

/* compiled from: MakeRingListAdapter */
class aj implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ad f1222a;

    aj(ad adVar) {
        this.f1222a = adVar;
    }

    public void onClick(View view) {
        a.a("MyRingMakeAdapter", "RingtoneDuoduo: click share button!");
        RingData ringData = (RingData) b.b().a("make_ring_list").a(this.f1222a.c);
        if (ringData != null) {
            ax.a().a(RingToneDuoduoActivity.a(), ringData, "makering");
        }
    }
}
