package com.shoujiduoduo.ui.mine;

import android.view.View;
import com.shoujiduoduo.a.b.b;
import com.shoujiduoduo.base.bean.c;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.util.ak;

/* compiled from: FavoriteRingListAdapter */
class t implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ l f1314a;

    t(l lVar) {
        this.f1314a = lVar;
    }

    public void onClick(View view) {
        if (this.f1314a.b >= 0) {
            PlayerService b = ak.a().b();
            c a2 = b.b().a("favorite_ring_list");
            if (b != null) {
                b.a(a2, this.f1314a.b);
            }
        }
    }
}
