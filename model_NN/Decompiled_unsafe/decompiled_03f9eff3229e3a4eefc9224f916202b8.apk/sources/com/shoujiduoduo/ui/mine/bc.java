package com.shoujiduoduo.ui.mine;

import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.util.as;
import com.shoujiduoduo.util.widget.f;

/* compiled from: RingUploadDialog */
class bc implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ az f1243a;

    bc(az azVar) {
        this.f1243a = azVar;
    }

    public void onClick(View view) {
        if (this.f1243a.k == 0) {
            Toast.makeText(this.f1243a.f1239a, (int) R.string.pick_category, 1).show();
            return;
        }
        String obj = this.f1243a.d.getText().toString();
        if (!TextUtils.isEmpty(obj.trim())) {
            as.c(this.f1243a.f1239a, "user_upload_name", obj.trim());
            this.f1243a.j.a(this.f1243a.b, this.f1243a.k);
            f.a((int) R.string.ring_is_uploading);
            this.f1243a.dismiss();
            return;
        }
        Toast.makeText(this.f1243a.f1239a, (int) R.string.input_user_name_hint, 1).show();
    }
}
