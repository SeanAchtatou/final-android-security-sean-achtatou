package com.shoujiduoduo.ui.mine;

import android.os.Bundle;
import com.shoujiduoduo.b.c.n;
import com.shoujiduoduo.base.bean.f;
import com.shoujiduoduo.ui.utils.DDListFragment;
import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;

/* compiled from: MyRingtoneScene */
class aq extends a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f1231a;
    final /* synthetic */ ap b;

    aq(ap apVar, String str) {
        this.b = apVar;
        this.f1231a = str;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.ui.mine.ap.a(com.shoujiduoduo.ui.mine.ap, boolean):boolean
     arg types: [com.shoujiduoduo.ui.mine.ap, int]
     candidates:
      com.shoujiduoduo.ui.mine.ap.a(int, android.view.KeyEvent):boolean
      com.shoujiduoduo.ui.mine.ap.a(com.shoujiduoduo.ui.mine.ap, boolean):boolean */
    public void a(c.b bVar) {
        super.a(bVar);
        if (bVar != null && (bVar instanceof c.e)) {
            c.e eVar = (c.e) bVar;
            if (!eVar.d() || !eVar.e()) {
                com.shoujiduoduo.base.a.a.a("MyRingtoneScene", "vip:" + eVar.e() + ", cailing:" + eVar.d());
            } else {
                com.shoujiduoduo.base.a.a.a("MyRingtoneScene", "vip 和彩铃均开通，显示彩铃");
                boolean unused = this.b.u = true;
                n nVar = new n(f.a.list_ring_cucc, "", false, "");
                DDListFragment dDListFragment = new DDListFragment();
                Bundle bundle = new Bundle();
                bundle.putBoolean("support_lazy_load", true);
                bundle.putString("adapter_type", "cailing_list_adapter");
                dDListFragment.setArguments(bundle);
                dDListFragment.a(nVar);
                if (this.b.c.size() == 3) {
                    this.b.c.add(1, dDListFragment);
                    com.shoujiduoduo.base.a.a.a("MyRingtoneScene", "add cailing frag, now fragsize:" + this.b.c.size());
                    boolean unused2 = this.b.u = true;
                    this.b.q.a();
                    this.b.b.getAdapter().notifyDataSetChanged();
                }
            }
            if (eVar.f1598a.a().equals("40307") || eVar.f1598a.a().equals("40308")) {
                com.shoujiduoduo.util.e.a.a().a(this.f1231a, "");
            }
        }
    }

    public void b(c.b bVar) {
        super.b(bVar);
    }
}
