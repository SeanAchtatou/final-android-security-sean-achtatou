package com.shoujiduoduo.ui.mine.changering;

import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.d.b;

/* compiled from: RingSettingFragment */
class s extends a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RingSettingFragment f1290a;

    s(RingSettingFragment ringSettingFragment) {
        this.f1290a = ringSettingFragment;
    }

    public void a(c.b bVar) {
        super.a(bVar);
        if (bVar != null && (bVar instanceof c.x)) {
            c.x xVar = (c.x) bVar;
            if (xVar.a() == null || xVar.d() == null || xVar.d().size() <= 0) {
                com.shoujiduoduo.base.a.a.c("RingSettingFragment", "查询默认彩铃失败，code:" + bVar.a() + " msg:" + bVar.b());
                this.f1290a.e();
                return;
            }
            String unused = this.f1290a.d = xVar.d().get(0).b();
            b.a().e(com.shoujiduoduo.a.b.b.g().c().k(), new t(this));
        }
    }

    public void b(c.b bVar) {
        super.b(bVar);
        com.shoujiduoduo.base.a.a.c("RingSettingFragment", "查询默认彩铃失败，code:" + bVar.a() + " msg:" + bVar.b());
        this.f1290a.e();
    }
}
