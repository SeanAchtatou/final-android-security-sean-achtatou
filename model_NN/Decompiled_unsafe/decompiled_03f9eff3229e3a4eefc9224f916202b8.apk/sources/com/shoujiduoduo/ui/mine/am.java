package com.shoujiduoduo.ui.mine;

import android.view.View;
import com.shoujiduoduo.a.b.b;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.base.bean.f;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ui.settings.u;

/* compiled from: MakeRingListAdapter */
class am implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ad f1225a;

    am(ad adVar) {
        this.f1225a = adVar;
    }

    public void onClick(View view) {
        a.a("MyRingMakeAdapter", "MyRingtoneScene:clickApplyButton:SetRingTone:getInstance.");
        RingData ringData = (RingData) b.b().a("make_ring_list").a(this.f1225a.c);
        if (ringData != null) {
            new u(this.f1225a.b, R.style.DuoDuoDialog, ringData, "user_make_ring", f.a.list_user_make.toString()).show();
        }
    }
}
