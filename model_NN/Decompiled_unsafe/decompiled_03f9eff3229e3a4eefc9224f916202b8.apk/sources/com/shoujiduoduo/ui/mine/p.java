package com.shoujiduoduo.ui.mine;

import android.app.AlertDialog;
import android.view.View;
import com.shoujiduoduo.a.b.b;
import com.shoujiduoduo.base.bean.c;
import com.shoujiduoduo.ringtone.R;

/* compiled from: FavoriteRingListAdapter */
class p implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ l f1310a;

    p(l lVar) {
        this.f1310a = lVar;
    }

    public void onClick(View view) {
        c a2 = b.b().a("favorite_ring_list");
        if (this.f1310a.b >= 0 && this.f1310a.b < a2.c()) {
            new AlertDialog.Builder(this.f1310a.c).setTitle((int) R.string.hint).setMessage((int) R.string.delete_ring_confirm).setIcon(17301543).setPositiveButton((int) R.string.ok, this.f1310a.f).setNegativeButton((int) R.string.cancel, this.f1310a.f).show();
        }
    }
}
