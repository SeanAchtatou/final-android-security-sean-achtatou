package com.shoujiduoduo.ui.mine.changering;

import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.ui.mine.changering.RingSettingFragment;
import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;

/* compiled from: RingSettingFragment */
class g extends a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ f f1279a;

    g(f fVar) {
        this.f1279a = fVar;
    }

    public void a(c.b bVar) {
        super.a(bVar);
        if (bVar instanceof c.s) {
            c.z[] zVarArr = ((c.s) bVar).d;
            if (zVarArr == null || zVarArr.length == 0) {
                com.shoujiduoduo.base.a.a.a("RingSettingFragment", "userToneInfo == null");
                return;
            }
            for (int i = 0; i < zVarArr.length; i++) {
                if (this.f1279a.f1278a.d.equals(zVarArr[i].f1620a)) {
                    String unused = this.f1279a.f1278a.b = zVarArr[i].b;
                    String unused2 = this.f1279a.f1278a.c = zVarArr[i].d;
                    RingSettingFragment.b unused3 = this.f1279a.f1278a.l = RingSettingFragment.b.success;
                    RingData ringData = new RingData();
                    ringData.A = this.f1279a.f1278a.d;
                    ringData.e = this.f1279a.f1278a.b + "-" + this.f1279a.f1278a.c;
                    ringData.j = 48;
                    if (this.f1279a.f1278a.e.size() > 3) {
                        ((RingSettingFragment.e) this.f1279a.f1278a.e.get(3)).c = 48000;
                        ((RingSettingFragment.e) this.f1279a.f1278a.e.get(3)).b = this.f1279a.f1278a.b + "-" + this.f1279a.f1278a.c;
                        ((RingSettingFragment.e) this.f1279a.f1278a.e.get(3)).f1263a = ringData;
                    }
                    this.f1279a.f1278a.f.notifyDataSetChanged();
                    return;
                }
            }
        }
    }

    public void b(c.b bVar) {
        super.b(bVar);
        this.f1279a.f1278a.e();
    }
}
