package com.shoujiduoduo.ui.mine;

import android.view.View;
import com.shoujiduoduo.a.b.b;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.base.bean.f;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ui.settings.u;

/* compiled from: FavoriteRingListAdapter */
class q implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ l f1311a;

    q(l lVar) {
        this.f1311a = lVar;
    }

    public void onClick(View view) {
        a.a("CollectRingListAdapter", "MyRingtoneScene:clickApplyButton:SetRingTone:getInstance.");
        new u(this.f1311a.c, R.style.DuoDuoDialog, (RingData) b.b().a("favorite_ring_list").a(this.f1311a.b), "user_collect_ring", f.a.list_user_favorite.toString()).show();
    }
}
