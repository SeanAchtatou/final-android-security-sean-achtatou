package com.shoujiduoduo.ui.mine;

import android.view.View;
import com.shoujiduoduo.a.b.b;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.ringtone.activity.RingToneDuoduoActivity;
import com.shoujiduoduo.util.ax;

/* compiled from: FavoriteRingListAdapter */
class o implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ l f1309a;

    o(l lVar) {
        this.f1309a = lVar;
    }

    public void onClick(View view) {
        a.a("CollectRingListAdapter", "RingtoneDuoduo: click share button!");
        ax.a().a(RingToneDuoduoActivity.a(), (RingData) b.b().a("favorite_ring_list").a(this.f1309a.b), "myring");
    }
}
