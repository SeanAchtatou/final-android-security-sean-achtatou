package com.shoujiduoduo.ui.mine;

import android.view.KeyEvent;
import android.view.View;
import com.shoujiduoduo.base.a.a;

/* compiled from: UserCollectFragment */
class be implements View.OnKeyListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ UserCollectFragment f1246a;

    be(UserCollectFragment userCollectFragment) {
        this.f1246a = userCollectFragment;
    }

    public boolean onKey(View view, int i, KeyEvent keyEvent) {
        if (i != 4 || keyEvent.getAction() != 0 || !this.f1246a.g) {
            return false;
        }
        a.a("UserCollectFragment", "edit mode, key back");
        this.f1246a.a(false);
        return true;
    }
}
