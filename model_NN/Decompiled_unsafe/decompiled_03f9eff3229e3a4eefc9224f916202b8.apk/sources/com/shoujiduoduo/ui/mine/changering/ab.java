package com.shoujiduoduo.ui.mine.changering;

import com.shoujiduoduo.a.c.n;
import com.shoujiduoduo.base.a.a;

/* compiled from: SystemRingListAdapter */
class ab implements n {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ aa f1269a;

    ab(aa aaVar) {
        this.f1269a = aaVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.ui.mine.changering.aa.a(com.shoujiduoduo.ui.mine.changering.aa, boolean):boolean
     arg types: [com.shoujiduoduo.ui.mine.changering.aa, int]
     candidates:
      com.shoujiduoduo.ui.mine.changering.aa.a(com.shoujiduoduo.ui.mine.changering.aa, int):int
      com.shoujiduoduo.ui.mine.changering.aa.a(android.view.View, int):void
      com.shoujiduoduo.ui.mine.changering.aa.a(com.shoujiduoduo.ui.mine.changering.aa, boolean):boolean */
    public void a(String str, int i) {
        if (this.f1269a.f1267a != null && this.f1269a.f1267a.a().equals(str)) {
            a.a("SystemRingListAdapter", "onSetPlay, listid:" + str);
            if (str.equals(this.f1269a.f1267a.a())) {
                boolean unused = this.f1269a.c = true;
                int unused2 = this.f1269a.b = i;
            } else {
                boolean unused3 = this.f1269a.c = false;
            }
            this.f1269a.notifyDataSetChanged();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.ui.mine.changering.aa.a(com.shoujiduoduo.ui.mine.changering.aa, boolean):boolean
     arg types: [com.shoujiduoduo.ui.mine.changering.aa, int]
     candidates:
      com.shoujiduoduo.ui.mine.changering.aa.a(com.shoujiduoduo.ui.mine.changering.aa, int):int
      com.shoujiduoduo.ui.mine.changering.aa.a(android.view.View, int):void
      com.shoujiduoduo.ui.mine.changering.aa.a(com.shoujiduoduo.ui.mine.changering.aa, boolean):boolean */
    public void b(String str, int i) {
        if (this.f1269a.f1267a != null && this.f1269a.f1267a.a().equals(str)) {
            a.a("SystemRingListAdapter", "onCanclePlay, listId:" + str);
            boolean unused = this.f1269a.c = false;
            int unused2 = this.f1269a.b = i;
            this.f1269a.notifyDataSetChanged();
        }
    }

    public void a(String str, int i, int i2) {
        if (this.f1269a.f1267a != null && this.f1269a.f1267a.a().equals(str)) {
            a.a("SystemRingListAdapter", "onStatusChange, listid:" + str);
            this.f1269a.notifyDataSetChanged();
        }
    }
}
