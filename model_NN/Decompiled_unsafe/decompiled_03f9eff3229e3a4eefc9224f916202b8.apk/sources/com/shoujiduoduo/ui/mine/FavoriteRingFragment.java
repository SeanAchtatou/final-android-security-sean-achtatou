package com.shoujiduoduo.ui.mine;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.a.c.f;
import com.shoujiduoduo.a.c.o;
import com.shoujiduoduo.a.c.v;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.util.ak;
import com.shoujiduoduo.util.i;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FavoriteRingFragment extends Fragment {

    /* renamed from: a  reason: collision with root package name */
    private ListView f1205a;
    /* access modifiers changed from: private */
    public l b;
    /* access modifiers changed from: private */
    public a c;
    /* access modifiers changed from: private */
    public c d;
    /* access modifiers changed from: private */
    public String[] e;
    /* access modifiers changed from: private */
    public a f;
    private TextView g;
    /* access modifiers changed from: private */
    public boolean h;
    private View i;
    private Button j;
    /* access modifiers changed from: private */
    public Button k;
    /* access modifiers changed from: private */
    public int l;
    private boolean m;
    private View.OnKeyListener n = new c(this);
    private View.OnClickListener o = new d(this);
    private View.OnClickListener p = new e(this);
    private o q = new h(this);
    private f r = new i(this);
    private v s = new j(this);

    static /* synthetic */ int l(FavoriteRingFragment favoriteRingFragment) {
        int i2 = favoriteRingFragment.l;
        favoriteRingFragment.l = i2 + 1;
        return i2;
    }

    static /* synthetic */ int m(FavoriteRingFragment favoriteRingFragment) {
        int i2 = favoriteRingFragment.l;
        favoriteRingFragment.l = i2 - 1;
        return i2;
    }

    public void onCreate(Bundle bundle) {
        this.b = new l(getActivity());
        this.c = new a(getActivity(), com.shoujiduoduo.a.b.b.b().a("favorite_ring_list"), "favorite_ring_list");
        this.d = new c(this, null);
        this.e = new String[3];
        com.shoujiduoduo.base.a.a.a("FavoriteRingFrag", "onCreate");
        super.onCreate(bundle);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate((int) R.layout.my_ringtone_favorite, viewGroup, false);
        this.g = (TextView) inflate.findViewById(R.id.fav_hint);
        this.i = (LinearLayout) inflate.findViewById(R.id.del_confirm);
        this.j = (Button) this.i.findViewById(R.id.cancel);
        this.j.setOnClickListener(this.o);
        this.k = (Button) this.i.findViewById(R.id.delete);
        this.k.setOnClickListener(this.p);
        this.f1205a = (ListView) inflate.findViewById(R.id.my_ringtone_like_list);
        if (com.shoujiduoduo.a.b.b.b().c()) {
            com.shoujiduoduo.base.a.a.b("FavoriteRingFrag", "favorite ring data is loaded!");
            b();
            c();
        }
        this.f1205a.setChoiceMode(1);
        this.f1205a.setOnItemClickListener(new b(this, null));
        d();
        this.b.a();
        x.a().a(com.shoujiduoduo.a.a.b.OBSERVER_USER_RING, this.s);
        x.a().a(com.shoujiduoduo.a.a.b.OBSERVER_LIST_DATA, this.r);
        x.a().a(com.shoujiduoduo.a.a.b.OBSERVER_RING_CHANGE, this.q);
        com.shoujiduoduo.base.a.a.a("FavoriteRingFrag", "FavoriteRingFragment onCreateView");
        return inflate;
    }

    /* access modifiers changed from: private */
    public void b() {
        if (!this.m) {
            this.f = new a();
            this.f1205a.addHeaderView(this.f.a());
            this.f1205a.setAdapter((ListAdapter) this.b);
            c();
            this.m = true;
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        if (com.shoujiduoduo.a.b.b.b().a("favorite_ring_list").c() > 0) {
            this.g.setVisibility(8);
        } else {
            this.g.setVisibility(0);
        }
    }

    /* access modifiers changed from: private */
    public void d() {
        i.a(new b(this));
    }

    public void onResume() {
        com.shoujiduoduo.base.a.a.a("FavoriteRingFrag", "onResume");
        super.onResume();
        getView().setFocusable(true);
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(this.n);
    }

    public void onPause() {
        com.shoujiduoduo.base.a.a.a("FavoriteRingFrag", "onPause");
        super.onPause();
    }

    public void onStop() {
        com.shoujiduoduo.base.a.a.a("FavoriteRingFrag", "onStop");
        super.onStop();
    }

    public boolean a() {
        return this.h;
    }

    public void a(boolean z) {
        if (this.h == z || !this.m) {
            com.shoujiduoduo.base.a.a.a("FavoriteRingFrag", "return, bEdit:" + z + ", mEditMode:" + this.h + ", mListviewInited:" + this.m);
            return;
        }
        this.h = z;
        this.f.a(!z);
        this.i.setVisibility(z ? 0 : 8);
        if (z) {
            this.b.a(-1);
            this.c.a(com.shoujiduoduo.a.b.b.b().a("favorite_ring_list"));
            this.f1205a.setAdapter((ListAdapter) this.c);
            this.l = 0;
            this.k.setText("删除");
            return;
        }
        this.f1205a.setAdapter((ListAdapter) this.b);
    }

    public void a(ListView listView) {
        ListAdapter adapter = listView.getAdapter();
        if (adapter != null) {
            int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), 0);
            com.shoujiduoduo.base.a.a.a("FavoriteRingFrag", "listcount:" + adapter.getCount());
            View view = null;
            int i2 = 0;
            for (int i3 = 0; i3 < adapter.getCount(); i3++) {
                view = adapter.getView(i3, view, listView);
                if (i3 == 0) {
                    view.setLayoutParams(new ViewGroup.LayoutParams(makeMeasureSpec, -2));
                }
                view.measure(makeMeasureSpec, 0);
                i2 += view.getMeasuredHeight();
            }
            ViewGroup.LayoutParams layoutParams = listView.getLayoutParams();
            layoutParams.height = (listView.getDividerHeight() * (adapter.getCount() - 1)) + i2;
            listView.setLayoutParams(layoutParams);
            listView.requestLayout();
        }
    }

    public void onDestroy() {
        super.onDestroy();
        com.shoujiduoduo.base.a.a.a("FavoriteRingFrag", "onDestroy");
    }

    public void onDestroyView() {
        com.shoujiduoduo.base.a.a.a("FavoriteRingFrag", "FavoriteRingFragment ondestroyView");
        if (this.d != null) {
            this.d.removeCallbacksAndMessages(null);
        }
        this.b.b();
        x.a().b(com.shoujiduoduo.a.a.b.OBSERVER_USER_RING, this.s);
        x.a().b(com.shoujiduoduo.a.a.b.OBSERVER_LIST_DATA, this.r);
        x.a().b(com.shoujiduoduo.a.a.b.OBSERVER_RING_CHANGE, this.q);
        this.m = false;
        this.h = false;
        super.onDestroyView();
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    private class c extends Handler {
        private c() {
        }

        /* synthetic */ c(FavoriteRingFragment favoriteRingFragment, b bVar) {
            this();
        }

        public void handleMessage(Message message) {
            super.handleMessage(message);
            if (FavoriteRingFragment.this.f != null) {
                FavoriteRingFragment.this.f.b();
            }
        }
    }

    private class a {
        private Button b;
        private ListView c;
        private View d;
        private SimpleAdapter e;
        private RelativeLayout f;
        private View.OnClickListener g = new k(this);

        public a() {
            c();
        }

        public void a(boolean z) {
            this.f.setVisibility(z ? 0 : 8);
        }

        public View a() {
            return this.d;
        }

        public void b() {
            com.shoujiduoduo.base.a.a.a("FavoriteRingFrag", "refreshsetting");
            d();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, ?[OBJECT, ARRAY], int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public void c() {
            this.d = LayoutInflater.from(FavoriteRingFragment.this.getActivity()).inflate((int) R.layout.listitem_header_ringsetting, (ViewGroup) null, false);
            this.f = (RelativeLayout) this.d.findViewById(R.id.layout_cur_ring);
            this.b = (Button) this.d.findViewById(R.id.change_ring);
            this.b.setOnClickListener(this.g);
            this.c = (ListView) this.d.findViewById(R.id.ring_detail_list);
            d();
            FavoriteRingFragment.this.a(this.c);
        }

        private void d() {
            String[] strArr = {"icon", "ring_name"};
            int[] iArr = {R.id.icon, R.id.ring_name};
            if (FavoriteRingFragment.this.getActivity() != null) {
                this.e = new SimpleAdapter(FavoriteRingFragment.this.getActivity(), FavoriteRingFragment.this.e(), R.layout.listitem_current_ring, strArr, iArr);
                this.c.setAdapter((ListAdapter) this.e);
            }
        }
    }

    /* access modifiers changed from: private */
    public List<HashMap<String, Object>> e() {
        ArrayList arrayList = new ArrayList();
        HashMap hashMap = new HashMap();
        hashMap.put("icon", Integer.valueOf((int) R.drawable.icon_call_gray));
        hashMap.put("ring_name", this.e[0] == null ? "未设置" : this.e[0]);
        arrayList.add(hashMap);
        HashMap hashMap2 = new HashMap();
        hashMap2.put("icon", Integer.valueOf((int) R.drawable.icon_sms_gray));
        hashMap2.put("ring_name", this.e[1] == null ? "未设置" : this.e[1]);
        arrayList.add(hashMap2);
        HashMap hashMap3 = new HashMap();
        hashMap3.put("icon", Integer.valueOf((int) R.drawable.icon_alarm_gray));
        hashMap3.put("ring_name", this.e[2] == null ? "未设置" : this.e[2]);
        arrayList.add(hashMap3);
        return arrayList;
    }

    private class b implements AdapterView.OnItemClickListener {
        private b() {
        }

        /* synthetic */ b(FavoriteRingFragment favoriteRingFragment, b bVar) {
            this();
        }

        public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            com.shoujiduoduo.base.a.a.a("FavoriteRingFrag", "click MyRingtone Item.");
            if (j >= 0) {
                int i2 = (int) j;
                if (FavoriteRingFragment.this.h) {
                    CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkbox);
                    checkBox.toggle();
                    FavoriteRingFragment.this.c.a().set(i2, Boolean.valueOf(checkBox.isChecked()));
                    if (checkBox.isChecked()) {
                        FavoriteRingFragment.l(FavoriteRingFragment.this);
                    } else {
                        FavoriteRingFragment.m(FavoriteRingFragment.this);
                    }
                    if (FavoriteRingFragment.this.l > 0) {
                        FavoriteRingFragment.this.k.setText("删除(" + FavoriteRingFragment.this.l + ")");
                    } else {
                        FavoriteRingFragment.this.k.setText("删除");
                    }
                } else {
                    PlayerService b = ak.a().b();
                    if (b != null) {
                        b.a(com.shoujiduoduo.a.b.b.b().a("favorite_ring_list"), i2);
                    } else {
                        com.shoujiduoduo.base.a.a.c("FavoriteRingFrag", "PlayerService is unavailable!");
                    }
                }
            }
        }
    }
}
