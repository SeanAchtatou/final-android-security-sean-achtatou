package com.shoujiduoduo.ui.mine;

import android.view.View;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.util.widget.a;
import com.shoujiduoduo.util.widget.f;
import java.util.List;

/* compiled from: UserCollectFragment */
class bg implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ UserCollectFragment f1248a;

    bg(UserCollectFragment userCollectFragment) {
        this.f1248a = userCollectFragment;
    }

    public void onClick(View view) {
        List<Integer> b = this.f1248a.b.b();
        if (b == null || b.size() == 0) {
            f.a("请选择要删除的精选集", 0);
        } else {
            new a.C0034a(this.f1248a.getActivity()).b((int) R.string.hint).a("确定删除选中的精选集吗？").a((int) R.string.ok, new bi(this, b)).b((int) R.string.cancel, new bh(this)).a().show();
        }
    }
}
