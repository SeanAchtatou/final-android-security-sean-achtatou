package com.shoujiduoduo.ui.mine;

import android.content.DialogInterface;
import com.shoujiduoduo.a.b.b;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.base.bean.c;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.util.ak;

/* compiled from: FavoriteRingListAdapter */
class n implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ l f1308a;

    n(l lVar) {
        this.f1308a = lVar;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        switch (i) {
            case -2:
            default:
                return;
            case -1:
                c a2 = b.b().a("favorite_ring_list");
                PlayerService b = ak.a().b();
                if (b != null && a2.a(this.f1308a.b) != null) {
                    if (b.f() == ((RingData) a2.a(this.f1308a.b)).k()) {
                        b.e();
                    }
                    int a3 = this.f1308a.b;
                    int unused = this.f1308a.b = -1;
                    b.b().a("favorite_ring_list", a3);
                    this.f1308a.notifyDataSetChanged();
                    return;
                }
                return;
        }
    }
}
