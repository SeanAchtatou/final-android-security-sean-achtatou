package com.shoujiduoduo.ui.mine.changering;

import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ui.cailing.ck;
import com.shoujiduoduo.ui.mine.changering.RingSettingFragment;
import com.shoujiduoduo.ui.utils.w;
import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.d.b;
import com.shoujiduoduo.util.f;
import com.shoujiduoduo.util.widget.f;

/* compiled from: RingSettingFragment */
class n extends a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f1286a;
    final /* synthetic */ RingSettingFragment b;

    n(RingSettingFragment ringSettingFragment, String str) {
        this.b = ringSettingFragment;
        this.f1286a = str;
    }

    public void a(c.b bVar) {
        super.a(bVar);
        w.a();
        RingSettingFragment.b unused = this.b.l = RingSettingFragment.b.success;
        this.b.d();
    }

    public void b(c.b bVar) {
        super.b(bVar);
        w.a();
        RingSettingFragment.b unused = this.b.l = RingSettingFragment.b.fail;
        if (b.a().a(this.f1286a).equals(b.d.wait_open)) {
            f.a("正在为您开通业务，请耐心等待一会儿.");
        } else {
            new ck(this.b.getActivity(), R.style.DuoDuoDialog, f.b.ct, new o(this)).show();
        }
    }
}
