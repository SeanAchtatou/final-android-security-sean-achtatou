package com.shoujiduoduo.ui.mine;

import android.content.Intent;
import android.view.View;
import com.shoujiduoduo.ui.mine.FavoriteRingFragment;
import com.shoujiduoduo.ui.mine.changering.CurrentRingSettingActivity;

/* compiled from: FavoriteRingFragment */
class k implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ FavoriteRingFragment.a f1305a;

    k(FavoriteRingFragment.a aVar) {
        this.f1305a = aVar;
    }

    public void onClick(View view) {
        FavoriteRingFragment.this.getActivity().startActivity(new Intent(FavoriteRingFragment.this.getActivity(), CurrentRingSettingActivity.class));
    }
}
