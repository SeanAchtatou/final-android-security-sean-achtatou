package com.shoujiduoduo.ui.mine;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import cn.banshenggua.aichang.utils.Constants;
import com.d.a.b.d;
import com.shoujiduoduo.a.b.b;
import com.shoujiduoduo.base.bean.c;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ui.utils.v;
import com.shoujiduoduo.util.u;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/* compiled from: UserCollectAdapter */
public class bd extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    private Context f1244a;
    private c b;
    private List<Boolean> c = new ArrayList();
    private boolean d;

    public bd(Context context, c cVar) {
        this.f1244a = context;
        this.b = cVar;
        for (int i = 0; i < this.b.c(); i++) {
            this.c.add(false);
        }
    }

    public void a(c cVar) {
        this.b = cVar;
        if (cVar.c() > 0) {
            this.c.clear();
            this.c = null;
            this.c = new ArrayList();
            for (int i = 0; i < cVar.c(); i++) {
                this.c.add(false);
            }
        }
    }

    public List<Boolean> a() {
        return this.c;
    }

    public void a(boolean z) {
        this.d = z;
        notifyDataSetChanged();
    }

    public List<Integer> b() {
        ArrayList arrayList;
        ArrayList arrayList2 = null;
        int i = 0;
        while (i < this.c.size()) {
            if (this.c.get(i).booleanValue()) {
                if (arrayList2 == null) {
                    arrayList = new ArrayList();
                } else {
                    arrayList = arrayList2;
                }
                arrayList.add(Integer.valueOf(i));
            } else {
                arrayList = arrayList2;
            }
            i++;
            arrayList2 = arrayList;
        }
        return arrayList2;
    }

    public int getCount() {
        if (b.b().c()) {
            return b.b().d().c();
        }
        return 0;
    }

    public Object getItem(int i) {
        if (b.b().c()) {
            return b.b().d().a(i);
        }
        return null;
    }

    public long getItemId(int i) {
        return (long) i;
    }

    /* compiled from: UserCollectAdapter */
    private static class a {

        /* renamed from: a  reason: collision with root package name */
        ImageView f1245a;
        TextView b;
        TextView c;
        TextView d;
        TextView e;
        CheckBox f;

        private a() {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int i, View view, ViewGroup viewGroup) {
        a aVar;
        if (!b.b().c()) {
            com.shoujiduoduo.base.a.a.a("UserCollectAdapter", "collect data is not ready");
            return null;
        }
        if (view == null) {
            view = LayoutInflater.from(this.f1244a).inflate((int) R.layout.listitem_collect, viewGroup, false);
            aVar = new a();
            aVar.f1245a = (ImageView) view.findViewById(R.id.pic);
            aVar.b = (TextView) view.findViewById(R.id.title);
            aVar.c = (TextView) view.findViewById(R.id.content);
            aVar.d = (TextView) view.findViewById(R.id.releate_time);
            aVar.e = (TextView) view.findViewById(R.id.fav_num);
            aVar.f = (CheckBox) view.findViewById(R.id.checkbox);
            view.setTag(aVar);
        } else {
            aVar = (a) view.getTag();
        }
        if (this.d) {
            aVar.f.setVisibility(0);
            aVar.f.setChecked(this.c.get(i).booleanValue());
        } else {
            aVar.f.setVisibility(8);
        }
        com.shoujiduoduo.base.bean.b bVar = (com.shoujiduoduo.base.bean.b) b.b().d().a(i);
        d.a().a(bVar.f819a, aVar.f1245a, v.a().g());
        aVar.b.setText(bVar.b);
        aVar.c.setText(bVar.c);
        aVar.d.setText(bVar.d);
        int a2 = u.a(bVar.f, Constants.CLEARIMGED);
        StringBuilder sb = new StringBuilder();
        if (a2 > 10000) {
            sb.append(new DecimalFormat("#.00").format((double) (((float) a2) / 10000.0f)));
            sb.append("万");
        } else {
            sb.append(a2);
        }
        aVar.e.setText(sb.toString());
        return view;
    }
}
