package com.shoujiduoduo.ui.mine.changering;

import android.view.View;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.ui.mine.changering.RingSettingFragment;
import com.shoujiduoduo.util.ak;

/* compiled from: RingSettingFragment */
class w implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f1294a;
    final /* synthetic */ RingSettingFragment.c b;

    w(RingSettingFragment.c cVar, int i) {
        this.b = cVar;
        this.f1294a = i;
    }

    public void onClick(View view) {
        PlayerService b2 = ak.a().b();
        if (b2 != null) {
            b2.a(((RingSettingFragment.e) RingSettingFragment.this.e.get(this.f1294a)).f1263a);
            RingSettingFragment.this.f.notifyDataSetChanged();
            return;
        }
        a.c("RingSettingFragment", "PlayerService is unavailable!");
    }
}
