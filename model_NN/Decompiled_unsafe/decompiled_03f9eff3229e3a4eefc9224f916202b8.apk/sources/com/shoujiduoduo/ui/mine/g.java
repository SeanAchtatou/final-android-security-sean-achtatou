package com.shoujiduoduo.ui.mine;

import android.content.DialogInterface;
import com.shoujiduoduo.a.b.b;
import com.shoujiduoduo.util.widget.f;
import java.util.List;

/* compiled from: FavoriteRingFragment */
class g implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ List f1301a;
    final /* synthetic */ e b;

    g(e eVar, List list) {
        this.b = eVar;
        this.f1301a = list;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        if (b.b().a("favorite_ring_list", this.f1301a)) {
            f.a("已删除" + this.f1301a.size() + "首铃声", 0);
            this.b.f1299a.a(false);
        } else {
            f.a("删除铃声失败", 0);
        }
        dialogInterface.dismiss();
    }
}
