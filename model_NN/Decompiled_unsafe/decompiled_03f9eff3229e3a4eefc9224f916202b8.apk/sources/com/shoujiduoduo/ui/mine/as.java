package com.shoujiduoduo.ui.mine;

import android.view.View;
import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.base.bean.ab;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.util.ax;
import com.umeng.socialize.c.b;

/* compiled from: MyRingtoneScene */
class as implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ap f1233a;

    as(ap apVar) {
        this.f1233a = apVar;
    }

    public void onClick(View view) {
        b bVar = null;
        switch (com.shoujiduoduo.a.b.b.g().d()) {
            case 2:
                bVar = b.QQ;
                break;
            case 3:
                bVar = b.SINA;
                break;
            case 5:
                bVar = b.WEIXIN;
                break;
        }
        if (bVar != null) {
            ax.a().a(this.f1233a.f1228a, bVar);
        }
        com.umeng.analytics.b.b(RingDDApp.c(), "USER_LOGOUT");
        ab c = com.shoujiduoduo.a.b.b.g().c();
        c.c(0);
        com.shoujiduoduo.a.b.b.g().a(c);
        x.a().a(com.shoujiduoduo.a.a.b.OBSERVER_USER_CENTER, new at(this));
    }
}
