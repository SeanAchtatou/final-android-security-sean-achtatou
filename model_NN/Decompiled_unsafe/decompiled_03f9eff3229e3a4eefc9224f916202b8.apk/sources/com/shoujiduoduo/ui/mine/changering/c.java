package com.shoujiduoduo.ui.mine.changering;

import android.graphics.Matrix;
import android.support.v4.view.ViewPager;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.ringtone.R;

/* compiled from: ChangeRingFragment */
class c implements ViewPager.OnPageChangeListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ChangeRingFragment f1275a;

    c(ChangeRingFragment changeRingFragment) {
        this.f1275a = changeRingFragment;
    }

    public void onPageSelected(int i) {
        int i2 = 0;
        while (i2 < this.f1275a.f1255a.length) {
            a.a("ChangeRingFragment", "i:" + i2 + "  arg0:" + i);
            this.f1275a.f1255a[i2].setTextColor(this.f1275a.getResources().getColor(i2 == i ? R.drawable.home_list_button_text_selectcolor : R.drawable.home_list_button_text_color));
            i2++;
        }
    }

    public void onPageScrolled(int i, float f, int i2) {
        Matrix matrix = new Matrix();
        matrix.setScale(this.f1275a.e, 1.0f);
        matrix.postTranslate((float) ((this.f1275a.c * i) + (this.f1275a.d / 2) + ((int) (((float) this.f1275a.c) * f))), 0.0f);
        this.f1275a.f.setImageMatrix(matrix);
    }

    public void onPageScrollStateChanged(int i) {
    }
}
