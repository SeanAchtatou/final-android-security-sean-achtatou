package com.shoujiduoduo.ui.mine;

import android.app.AlertDialog;
import android.view.View;
import com.shoujiduoduo.ringtone.R;

/* compiled from: MakeRingListAdapter */
class al implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ad f1224a;

    al(ad adVar) {
        this.f1224a = adVar;
    }

    public void onClick(View view) {
        if (this.f1224a.c >= 0) {
            new AlertDialog.Builder(this.f1224a.b).setTitle((int) R.string.hint).setMessage((int) R.string.delete_ring_confirm).setIcon(17301543).setPositiveButton((int) R.string.ok, this.f1224a.g).setNegativeButton((int) R.string.cancel, this.f1224a.g).show();
        }
    }
}
