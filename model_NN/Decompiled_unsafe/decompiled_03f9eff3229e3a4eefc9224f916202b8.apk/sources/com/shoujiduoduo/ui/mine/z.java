package com.shoujiduoduo.ui.mine;

import android.content.DialogInterface;
import com.shoujiduoduo.a.b.b;
import com.shoujiduoduo.util.widget.f;
import java.util.List;

/* compiled from: MakeRingFragment */
class z implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ List f1320a;
    final /* synthetic */ x b;

    z(x xVar, List list) {
        this.b = xVar;
        this.f1320a = list;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        if (b.b().a("make_ring_list", this.f1320a)) {
            f.a("已删除" + this.f1320a.size() + "首铃声", 0);
            this.b.f1318a.a(false);
        } else {
            f.a("删除铃声失败", 0);
        }
        dialogInterface.dismiss();
    }
}
