package com.shoujiduoduo.ui.mine;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import com.shoujiduoduo.base.bean.b;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ui.home.CollectRingActivity;

/* compiled from: UserCollectFragment */
class bl implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ UserCollectFragment f1253a;

    bl(UserCollectFragment userCollectFragment) {
        this.f1253a = userCollectFragment;
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        if (this.f1253a.g) {
            CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkbox);
            checkBox.toggle();
            this.f1253a.b.a().set(i, Boolean.valueOf(checkBox.isChecked()));
            if (checkBox.isChecked()) {
                UserCollectFragment.f(this.f1253a);
            } else {
                UserCollectFragment.g(this.f1253a);
            }
            if (this.f1253a.f > 0) {
                this.f1253a.e.setText("删除(" + this.f1253a.f + ")");
            } else {
                this.f1253a.e.setText("删除");
            }
        } else {
            RingDDApp.b().a("collectdata", (b) com.shoujiduoduo.a.b.b.b().d().a(i));
            Intent intent = new Intent(this.f1253a.getActivity(), CollectRingActivity.class);
            intent.putExtra("parakey", "collectdata");
            this.f1253a.getActivity().startActivity(intent);
        }
    }
}
