package com.shoujiduoduo.ui.mine.changering;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.shoujiduoduo.a.a.b;
import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.b.c.n;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.base.bean.c;
import com.shoujiduoduo.base.bean.f;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ui.utils.cc;
import com.shoujiduoduo.ui.utils.g;
import com.shoujiduoduo.util.ak;

/* compiled from: SystemRingListAdapter */
public class aa extends g {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public n f1267a;
    /* access modifiers changed from: private */
    public int b = -1;
    /* access modifiers changed from: private */
    public boolean c;
    /* access modifiers changed from: private */
    public Context d;
    private com.shoujiduoduo.a.c.n e = new ab(this);
    private View.OnClickListener f = new ac(this);
    private View.OnClickListener g = new ad(this);
    private View.OnClickListener h = new ae(this);
    private View.OnClickListener i = new af(this);

    public aa(Context context) {
        this.d = context;
    }

    public void a() {
        x.a().a(b.OBSERVER_PLAY_STATUS, this.e);
    }

    public void b() {
        x.a().b(b.OBSERVER_PLAY_STATUS, this.e);
    }

    public int getCount() {
        if (this.f1267a == null) {
            return 0;
        }
        return this.f1267a.c();
    }

    public Object getItem(int i2) {
        if (this.f1267a != null && i2 >= 0 && i2 < this.f1267a.c()) {
            return this.f1267a.a(i2);
        }
        return null;
    }

    public long getItemId(int i2) {
        return (long) i2;
    }

    private void a(View view, int i2) {
        RingData b2 = this.f1267a.a(i2);
        TextView textView = (TextView) cc.a(view, R.id.item_duration);
        ((TextView) cc.a(view, R.id.item_song_name)).setText(b2.e);
        textView.setText(String.format("%02d:%02d", Integer.valueOf(b2.j / 60), Integer.valueOf(b2.j % 60)));
        if (b2.j == 0) {
            textView.setVisibility(4);
        } else {
            textView.setVisibility(0);
        }
    }

    /* renamed from: com.shoujiduoduo.ui.mine.changering.aa$1  reason: invalid class name */
    /* compiled from: SystemRingListAdapter */
    static /* synthetic */ class AnonymousClass1 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f1268a = new int[f.a.values().length];

        static {
            try {
                f1268a[f.a.sys_alarm.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                f1268a[f.a.sys_notify.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                f1268a[f.a.sys_ringtone.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int i2, View view, ViewGroup viewGroup) {
        if (this.f1267a == null) {
            return null;
        }
        if (i2 >= this.f1267a.c()) {
            return view;
        }
        if (view == null) {
            view = LayoutInflater.from(this.d).inflate((int) R.layout.system_ring_item, (ViewGroup) null, false);
        }
        a(view, i2);
        ProgressBar progressBar = (ProgressBar) cc.a(view, R.id.ringitem_download_progress);
        TextView textView = (TextView) cc.a(view, R.id.ringitem_serial_number);
        ImageButton imageButton = (ImageButton) cc.a(view, R.id.ringitem_play);
        imageButton.setOnClickListener(this.f);
        ImageButton imageButton2 = (ImageButton) cc.a(view, R.id.ringitem_pause);
        imageButton2.setOnClickListener(this.g);
        ImageButton imageButton3 = (ImageButton) cc.a(view, R.id.ringitem_failed);
        imageButton3.setOnClickListener(this.h);
        PlayerService b2 = ak.a().b();
        if (i2 != this.b || !this.c) {
            ((Button) cc.a(view, R.id.ring_item_set)).setVisibility(8);
            textView.setText(Integer.toString(i2 + 1));
            textView.setVisibility(0);
            progressBar.setVisibility(4);
            imageButton.setVisibility(4);
            imageButton2.setVisibility(4);
            imageButton3.setVisibility(4);
            return view;
        }
        Button button = (Button) cc.a(view, R.id.ring_item_set);
        button.setVisibility(0);
        button.setOnClickListener(this.i);
        textView.setVisibility(4);
        imageButton.setVisibility(4);
        imageButton2.setVisibility(4);
        imageButton3.setVisibility(4);
        int i3 = 5;
        if (b2 != null) {
            i3 = b2.a();
        }
        switch (i3) {
            case 1:
                progressBar.setVisibility(0);
                return view;
            case 2:
                imageButton2.setVisibility(0);
                return view;
            case 3:
            case 4:
            case 5:
                imageButton.setVisibility(0);
                return view;
            case 6:
                imageButton3.setVisibility(0);
                return view;
            default:
                return view;
        }
    }

    public void a(boolean z) {
    }

    public void a(c cVar) {
        if (this.f1267a != cVar) {
            this.f1267a = null;
            this.f1267a = (n) cVar;
            this.c = false;
            notifyDataSetChanged();
        }
    }
}
