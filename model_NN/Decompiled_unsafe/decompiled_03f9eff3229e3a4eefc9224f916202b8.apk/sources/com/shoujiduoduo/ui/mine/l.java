package com.shoujiduoduo.ui.mine;

import android.app.Activity;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.shoujiduoduo.a.a.b;
import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.a.c.n;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.base.bean.c;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ui.utils.cc;
import com.shoujiduoduo.util.ak;
import com.shoujiduoduo.util.as;

/* compiled from: FavoriteRingListAdapter */
class l extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    private LayoutInflater f1306a;
    /* access modifiers changed from: private */
    public int b = -1;
    /* access modifiers changed from: private */
    public Activity c;
    /* access modifiers changed from: private */
    public boolean d;
    private n e = new m(this);
    /* access modifiers changed from: private */
    public DialogInterface.OnClickListener f = new n(this);
    private View.OnClickListener g = new o(this);
    private View.OnClickListener h = new p(this);
    private View.OnClickListener i = new q(this);
    private View.OnClickListener j = new r(this);
    private View.OnClickListener k = new s(this);
    private View.OnClickListener l = new t(this);

    public l(Activity activity) {
        this.c = activity;
        this.f1306a = LayoutInflater.from(this.c);
    }

    public void a() {
        x.a().a(b.OBSERVER_PLAY_STATUS, this.e);
    }

    public void b() {
        x.a().b(b.OBSERVER_PLAY_STATUS, this.e);
    }

    public void a(int i2) {
        this.b = i2;
    }

    private void a(View view, int i2) {
        RingData ringData = (RingData) com.shoujiduoduo.a.b.b.b().a("favorite_ring_list").a(i2);
        TextView textView = (TextView) cc.a(view, R.id.item_duration);
        ImageView imageView = (ImageView) cc.a(view, R.id.item_select_sign1);
        ImageView imageView2 = (ImageView) cc.a(view, R.id.item_select_sign2);
        ImageView imageView3 = (ImageView) cc.a(view, R.id.item_select_sign3);
        imageView.setVisibility(0);
        imageView2.setVisibility(0);
        imageView3.setVisibility(0);
        ((TextView) cc.a(view, R.id.item_song_name)).setText(ringData.e);
        ((TextView) cc.a(view, R.id.item_artist)).setText(ringData.f);
        textView.setText(String.format("%02d:%02d", Integer.valueOf(ringData.j / 60), Integer.valueOf(ringData.j % 60)));
        if (ringData.j == 0) {
            textView.setVisibility(4);
        } else {
            textView.setVisibility(0);
        }
        imageView.setVisibility(0);
        imageView2.setVisibility(0);
        imageView3.setVisibility(0);
        String a2 = as.a(this.c, "user_ring_phone_select", "ringtoneduoduo_not_set");
        String a3 = as.a(this.c, "user_ring_alarm_select", "ringtoneduoduo_not_set");
        String a4 = as.a(this.c, "user_ring_notification_select", "ringtoneduoduo_not_set");
        imageView.setImageResource(a2.equalsIgnoreCase(ringData.g) ? R.drawable.ring_incoming_call_true : R.drawable.ring_incoming_call_false);
        imageView2.setImageResource(a4.equalsIgnoreCase(ringData.g) ? R.drawable.ring_notification_true : R.drawable.ring_notification_false);
        imageView3.setImageResource(a3.equalsIgnoreCase(ringData.g) ? R.drawable.ring_alarm_true : R.drawable.ring_alarm_false);
    }

    public int getCount() {
        c a2 = com.shoujiduoduo.a.b.b.b().a("favorite_ring_list");
        if (a2 != null) {
            return a2.c();
        }
        return 0;
    }

    public Object getItem(int i2) {
        c a2 = com.shoujiduoduo.a.b.b.b().a("favorite_ring_list");
        if (i2 < a2.c()) {
            return a2.a(i2);
        }
        return null;
    }

    public long getItemId(int i2) {
        return (long) i2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int i2, View view, ViewGroup viewGroup) {
        c a2 = com.shoujiduoduo.a.b.b.b().a("favorite_ring_list");
        if (a2 != null && i2 < a2.c()) {
            if (view == null) {
                view = this.f1306a.inflate((int) R.layout.listitem_ring, viewGroup, false);
            }
            a(view, i2);
            ProgressBar progressBar = (ProgressBar) cc.a(view, R.id.ringitem_download_progress);
            TextView textView = (TextView) cc.a(view, R.id.ringitem_serial_number);
            ImageButton imageButton = (ImageButton) cc.a(view, R.id.ringitem_play);
            ImageButton imageButton2 = (ImageButton) cc.a(view, R.id.ringitem_pause);
            ImageButton imageButton3 = (ImageButton) cc.a(view, R.id.ringitem_failed);
            imageButton.setOnClickListener(this.j);
            imageButton2.setOnClickListener(this.k);
            imageButton3.setOnClickListener(this.l);
            String str = "";
            PlayerService b2 = ak.a().b();
            if (b2 != null) {
                str = b2.b();
                this.b = b2.c();
            }
            if (i2 == this.b && str.equals(a2.a())) {
                RingData ringData = (RingData) a2.a(i2);
                Button button = (Button) cc.a(view, R.id.ring_item_button0);
                Button button2 = (Button) cc.a(view, R.id.ring_item_button1);
                Button button3 = (Button) cc.a(view, R.id.ring_item_button2);
                if (ringData.n.equals("") || (!ringData.n.equals("") && ringData.q != 0)) {
                    button.setVisibility(0);
                } else {
                    button.setVisibility(8);
                }
                button.setOnClickListener(this.g);
                button2.setVisibility(0);
                button2.setOnClickListener(this.h);
                button3.setVisibility(0);
                button3.setOnClickListener(this.i);
                textView.setVisibility(4);
                progressBar.setVisibility(4);
                imageButton.setVisibility(4);
                imageButton2.setVisibility(4);
                imageButton3.setVisibility(4);
                PlayerService b3 = ak.a().b();
                if (this.d && b3 != null && b3.f() == ((RingData) a2.a(i2)).k()) {
                    switch (b3.a()) {
                        case 1:
                            progressBar.setVisibility(0);
                            break;
                        case 2:
                            imageButton2.setVisibility(0);
                            break;
                        case 3:
                        case 4:
                        case 5:
                            imageButton.setVisibility(0);
                            break;
                        case 6:
                            imageButton3.setVisibility(0);
                            break;
                    }
                } else {
                    imageButton.setVisibility(0);
                }
            } else {
                ((Button) cc.a(view, R.id.ring_item_button0)).setVisibility(8);
                ((Button) cc.a(view, R.id.ring_item_button1)).setVisibility(8);
                ((Button) cc.a(view, R.id.ring_item_button2)).setVisibility(8);
                textView.setText(Integer.toString(i2 + 1));
                textView.setVisibility(0);
                progressBar.setVisibility(4);
                imageButton.setVisibility(4);
                imageButton2.setVisibility(4);
                imageButton3.setVisibility(4);
            }
        }
        return view;
    }
}
