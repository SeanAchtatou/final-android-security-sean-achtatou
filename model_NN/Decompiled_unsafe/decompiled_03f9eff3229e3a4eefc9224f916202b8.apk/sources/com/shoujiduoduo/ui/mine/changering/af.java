package com.shoujiduoduo.ui.mine.changering;

import android.net.Uri;
import android.view.View;
import com.shoujiduoduo.a.a.b;
import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ui.mine.changering.aa;
import com.shoujiduoduo.util.aw;
import com.shoujiduoduo.util.widget.f;
import java.io.File;

/* compiled from: SystemRingListAdapter */
class af implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ aa f1273a;

    af(aa aaVar) {
        this.f1273a = aaVar;
    }

    public void onClick(View view) {
        if (this.f1273a.f1267a.a(this.f1273a.b) != null) {
            int i = 0;
            switch (aa.AnonymousClass1.f1268a[this.f1273a.f1267a.h().ordinal()]) {
                case 1:
                    i = 4;
                    break;
                case 2:
                    i = 2;
                    break;
                case 3:
                    i = 1;
                    break;
            }
            aw awVar = new aw(RingDDApp.c());
            File file = new File(this.f1273a.f1267a.a(this.f1273a.b).m);
            if (file.exists()) {
                awVar.a(i, Uri.fromFile(file), this.f1273a.f1267a.a(this.f1273a.b).m);
                String str = "";
                String str2 = this.f1273a.f1267a.a(this.f1273a.b).e;
                String string = this.f1273a.d.getResources().getString(R.string.set_ring_hint2);
                switch (i) {
                    case 1:
                        str = str2 + string + this.f1273a.d.getResources().getString(R.string.set_ring_incoming_call);
                        break;
                    case 2:
                        str = str2 + string + this.f1273a.d.getResources().getString(R.string.set_ring_message);
                        break;
                    case 4:
                        str = str2 + string + this.f1273a.d.getResources().getString(R.string.set_ring_alarm);
                        break;
                }
                f.a(str);
                x.a().b(b.OBSERVER_RING_CHANGE, new ag(this, i));
            }
        }
    }
}
