package com.shoujiduoduo.ui.mine.changering;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import cn.banshenggua.aichang.utils.Constants;
import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.a.c.n;
import com.shoujiduoduo.a.c.o;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ui.cailing.cb;
import com.shoujiduoduo.ui.utils.w;
import com.shoujiduoduo.util.ak;
import com.shoujiduoduo.util.av;
import com.shoujiduoduo.util.aw;
import com.shoujiduoduo.util.c.b;
import com.shoujiduoduo.util.f;
import com.shoujiduoduo.util.i;
import java.util.ArrayList;
import java.util.Timer;

public class RingSettingFragment extends Fragment {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public ListView f1258a;
    /* access modifiers changed from: private */
    public String b;
    /* access modifiers changed from: private */
    public String c;
    /* access modifiers changed from: private */
    public String d;
    /* access modifiers changed from: private */
    public ArrayList<e> e;
    /* access modifiers changed from: private */
    public c f;
    /* access modifiers changed from: private */
    public g g;
    /* access modifiers changed from: private */
    public int h;
    /* access modifiers changed from: private */
    public boolean i;
    /* access modifiers changed from: private */
    public boolean j;
    private Timer k;
    /* access modifiers changed from: private */
    public b l;
    /* access modifiers changed from: private */
    public a m;
    private o n = new h(this);
    private n o = new i(this);
    private com.shoujiduoduo.util.b.a p = new r(this);
    private com.shoujiduoduo.util.b.a q = new s(this);
    private com.shoujiduoduo.util.b.a r = new f(this);

    public interface a {
        void a(int i);
    }

    private enum b {
        uninit,
        querying,
        success,
        fail
    }

    public enum f {
        ringtone,
        notification,
        alarm,
        cailing
    }

    public void onCreate(Bundle bundle) {
        this.g = new g(this, null);
        super.onCreate(bundle);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        com.shoujiduoduo.base.a.a.a("RingSettingFragment", "onCreateView");
        this.j = false;
        View inflate = layoutInflater.inflate((int) R.layout.my_ringtone_setting, viewGroup, false);
        this.f1258a = (ListView) inflate.findViewById(R.id.my_ringtone_setting_list);
        this.f = new c(this, null);
        i.a(new e(this));
        x.a().a(com.shoujiduoduo.a.a.b.OBSERVER_RING_CHANGE, this.n);
        x.a().a(com.shoujiduoduo.a.a.b.OBSERVER_PLAY_STATUS, this.o);
        return inflate;
    }

    public void onDestroy() {
        super.onDestroy();
        com.shoujiduoduo.base.a.a.a("RingSettingFragment", "onDestroy");
    }

    public void onDestroyView() {
        com.shoujiduoduo.base.a.a.a("RingSettingFragment", "onDestroyView");
        super.onDestroyView();
        if (this.g != null) {
            this.g.removeCallbacksAndMessages(null);
        }
        if (this.k != null) {
            this.k.cancel();
        }
        PlayerService b2 = ak.a().b();
        if (b2 != null && b2.j()) {
            b2.k();
        }
        x.a().b(com.shoujiduoduo.a.a.b.OBSERVER_RING_CHANGE, this.n);
        x.a().b(com.shoujiduoduo.a.a.b.OBSERVER_PLAY_STATUS, this.o);
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.m = (a) activity;
        } catch (ClassCastException e2) {
            throw new ClassCastException(activity.toString() + " must implents ChangRingListener");
        }
    }

    private class d implements View.OnClickListener {
        private int b;

        public d(int i) {
            this.b = i;
        }

        public void onClick(View view) {
            if (this.b == 0 || this.b == 1 || this.b == 2) {
                RingSettingFragment.this.m.a(this.b);
                return;
            }
            switch (com.shoujiduoduo.util.f.u()) {
                case f1671a:
                    if (com.shoujiduoduo.util.c.b.a().c().equals(b.a.success)) {
                        RingSettingFragment.this.m.a(3);
                        return;
                    }
                    switch (RingSettingFragment.this.l) {
                        case uninit:
                            RingSettingFragment.this.b();
                            return;
                        case fail:
                            RingSettingFragment.this.a();
                            return;
                        case querying:
                        default:
                            return;
                        case success:
                            RingSettingFragment.this.m.a(3);
                            return;
                    }
                case cu:
                    switch (RingSettingFragment.this.l) {
                        case uninit:
                        case fail:
                            String k = com.shoujiduoduo.a.b.b.g().c().k();
                            if (TextUtils.isEmpty(k) || !com.shoujiduoduo.util.e.a.a().a(k)) {
                                new cb(RingSettingFragment.this.getActivity(), R.style.DuoDuoDialog, "", f.b.cu, new x(this)).show();
                                return;
                            } else {
                                RingSettingFragment.this.a(k);
                                return;
                            }
                        case querying:
                        default:
                            return;
                        case success:
                            RingSettingFragment.this.m.a(3);
                            return;
                    }
                case ct:
                    switch (RingSettingFragment.this.l) {
                        case uninit:
                        case fail:
                            String k2 = com.shoujiduoduo.a.b.b.g().c().k();
                            if (!TextUtils.isEmpty(k2)) {
                                RingSettingFragment.this.b(k2);
                                return;
                            } else {
                                new cb(RingSettingFragment.this.getActivity(), R.style.DuoDuoDialog, "", f.b.ct, new y(this)).show();
                                return;
                            }
                        case querying:
                        default:
                            return;
                        case success:
                            RingSettingFragment.this.m.a(3);
                            return;
                    }
                default:
                    return;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.util.c.b.b(java.lang.String, com.shoujiduoduo.util.b.a, boolean):void
     arg types: [java.lang.String, com.shoujiduoduo.ui.mine.changering.j, int]
     candidates:
      com.shoujiduoduo.util.c.b.b(java.lang.String, java.lang.String, com.shoujiduoduo.util.b.a):void
      com.shoujiduoduo.util.c.b.b(java.lang.String, com.shoujiduoduo.util.b.a, boolean):void */
    /* access modifiers changed from: private */
    public void a() {
        w.a(getActivity());
        com.shoujiduoduo.util.c.b.a().b("", (com.shoujiduoduo.util.b.a) new j(this), false);
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        w.a(getActivity());
        com.shoujiduoduo.util.e.a.a().e(new l(this));
    }

    /* access modifiers changed from: private */
    public void b(String str) {
        w.a(getActivity());
        com.shoujiduoduo.util.d.b.a().g(str, new n(this, str));
    }

    /* access modifiers changed from: private */
    public void b() {
        com.shoujiduoduo.base.a.a.a("RingSettingFragment", "准备初始化移动sdk");
        w.a(getActivity(), "正在初始化，请稍候...");
        w.b(getActivity());
        this.g.sendEmptyMessageDelayed(1, 4000);
    }

    private class g extends Handler {
        private g() {
        }

        /* synthetic */ g(RingSettingFragment ringSettingFragment, e eVar) {
            this();
        }

        public void handleMessage(Message message) {
            super.handleMessage(message);
            switch (message.what) {
                case 1:
                    w.b();
                    com.shoujiduoduo.util.c.b.a().c(new z(this));
                    return;
                case 4:
                    RingSettingFragment.this.f.notifyDataSetChanged();
                    return;
                case 5:
                    RingSettingFragment.this.f1258a.setAdapter((ListAdapter) RingSettingFragment.this.f);
                    RingSettingFragment.this.d();
                    return;
                case 10:
                    int intValue = ((Integer) message.obj).intValue();
                    switch (intValue) {
                        case 1:
                        case 2:
                        case 4:
                            RingSettingFragment.this.a(intValue);
                            break;
                        case 16:
                            RingSettingFragment.this.d();
                            break;
                    }
                    RingSettingFragment.this.f.notifyDataSetChanged();
                    return;
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(String str, f.b bVar) {
        if (getActivity() != null && !getActivity().isFinishing()) {
            new cb(getActivity(), R.style.DuoDuoDialog, str, bVar, new p(this)).show();
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        this.e = new ArrayList<>();
        aw.b a2 = new aw(getActivity()).a();
        if (a2.f1578a != null) {
            this.e.add(new e(a2.f1578a.b, a2.f1578a.c, b(a2), f.ringtone));
        }
        if (a2.b != null) {
            this.e.add(new e(a2.b.b, a2.b.c, c(a2), f.notification));
        }
        if (a2.c != null) {
            this.e.add(new e(a2.c.b, a2.c.c, a(a2), f.alarm));
        }
        switch (com.shoujiduoduo.util.f.u()) {
            case f1671a:
                if (com.shoujiduoduo.util.c.b.a().c().equals(b.a.success)) {
                    this.l = b.querying;
                    this.e.add(new e(RingDDApp.c().getString(R.string.query_coloring_hint), 0, null, f.cailing));
                    return;
                }
                this.l = b.uninit;
                this.e.add(new e(RingDDApp.c().getString(R.string.set_coloring_hint), 0, null, f.cailing));
                return;
            case cu:
            case ct:
                if (!av.c(com.shoujiduoduo.a.b.b.g().c().k())) {
                    this.l = b.querying;
                    this.e.add(new e(RingDDApp.c().getString(R.string.query_coloring_hint), 0, null, f.cailing));
                    return;
                }
                this.l = b.uninit;
                this.e.add(new e(RingDDApp.c().getString(R.string.set_coloring_hint), 0, null, f.cailing));
                return;
            default:
                return;
        }
    }

    private RingData a(aw.b bVar) {
        RingData ringData = new RingData();
        ringData.m = bVar.c.f1577a;
        ringData.e = bVar.c.b;
        ringData.j = bVar.c.c / Constants.CLEARIMGED;
        return ringData;
    }

    private RingData b(aw.b bVar) {
        RingData ringData = new RingData();
        ringData.m = bVar.f1578a.f1577a;
        ringData.e = bVar.f1578a.b;
        ringData.j = bVar.f1578a.c / Constants.CLEARIMGED;
        return ringData;
    }

    private RingData c(aw.b bVar) {
        RingData ringData = new RingData();
        ringData.m = bVar.b.f1577a;
        ringData.e = bVar.b.b;
        ringData.j = bVar.b.c / Constants.CLEARIMGED;
        return ringData;
    }

    /* access modifiers changed from: private */
    public void a(int i2) {
        aw.b a2 = new aw(getActivity()).a();
        switch (i2) {
            case 1:
                this.e.get(0).c = a2.f1578a.c;
                this.e.get(0).b = a2.f1578a.b;
                this.e.get(0).f1263a = b(a2);
                break;
            case 2:
                this.e.get(1).c = a2.b.c;
                this.e.get(1).b = a2.b.b;
                this.e.get(1).f1263a = c(a2);
                break;
            case 4:
                this.e.get(2).c = a2.c.c;
                this.e.get(2).b = a2.c.b;
                this.e.get(2).f1263a = a(a2);
                break;
        }
        this.f.notifyDataSetChanged();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.util.c.b.a(java.lang.String, com.shoujiduoduo.util.b.a, boolean):void
     arg types: [java.lang.String, com.shoujiduoduo.util.b.a, int]
     candidates:
      com.shoujiduoduo.util.c.b.a(com.shoujiduoduo.util.b.c$b, java.lang.String, java.lang.String):void
      com.shoujiduoduo.util.c.b.a(java.lang.String, com.shoujiduoduo.util.b.c$b, java.lang.String):void
      com.shoujiduoduo.util.c.b.a(java.lang.String, java.lang.String, java.lang.String):void
      com.shoujiduoduo.util.c.b.a(java.lang.String, java.lang.String, com.shoujiduoduo.util.b.a):void
      com.shoujiduoduo.util.c.b.a(java.lang.String, com.shoujiduoduo.util.b.a, boolean):void */
    /* access modifiers changed from: private */
    public void d() {
        this.l = b.querying;
        this.f.notifyDataSetChanged();
        switch (com.shoujiduoduo.util.f.u()) {
            case f1671a:
                if (com.shoujiduoduo.util.c.b.a().c().equals(b.a.success)) {
                    com.shoujiduoduo.util.c.b.a().a("", this.p, false);
                    return;
                } else {
                    this.l = b.uninit;
                    return;
                }
            case cu:
                com.shoujiduoduo.util.e.a.a().h(this.r);
                return;
            case ct:
                com.shoujiduoduo.util.d.b.a().f(com.shoujiduoduo.a.b.b.g().c().k(), this.q);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public void e() {
        this.l = b.fail;
        if (this.e.size() > 3) {
            this.e.get(3).b = RingDDApp.c().getString(R.string.set_coloring_error);
        }
        this.f.notifyDataSetChanged();
        com.shoujiduoduo.util.widget.f.a("当前彩铃查询失败，请稍后再试");
    }

    private class e {

        /* renamed from: a  reason: collision with root package name */
        public RingData f1263a;
        public String b;
        public int c;
        public f d;

        public e(String str, int i, RingData ringData, f fVar) {
            this.b = str;
            this.c = i;
            this.f1263a = ringData;
            this.d = fVar;
        }
    }

    private class c extends BaseAdapter {
        private c() {
        }

        /* synthetic */ c(RingSettingFragment ringSettingFragment, e eVar) {
            this();
        }

        public int getCount() {
            return RingSettingFragment.this.e.size();
        }

        public Object getItem(int i) {
            return RingSettingFragment.this.e.get(i);
        }

        public long getItemId(int i) {
            return (long) i;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(int i, View view, ViewGroup viewGroup) {
            if (view == null) {
                view = ((LayoutInflater) RingSettingFragment.this.getActivity().getSystemService("layout_inflater")).inflate((int) R.layout.listitem_system_ring_setting, viewGroup, false);
            }
            TextView textView = (TextView) view.findViewById(R.id.ringtype);
            ((Button) view.findViewById(R.id.changeRing)).setOnClickListener(new d(i));
            switch (i) {
                case 0:
                    textView.setText("来电铃声");
                    break;
                case 1:
                    textView.setText("短信铃声");
                    break;
                case 2:
                    textView.setText("闹钟铃声");
                    break;
                case 3:
                    textView.setText("彩铃");
                    break;
            }
            ImageView imageView = (ImageView) view.findViewById(R.id.ringitem_play);
            imageView.setOnClickListener(new u(this, i));
            ImageView imageView2 = (ImageView) view.findViewById(R.id.ringitem_pause);
            imageView2.setOnClickListener(new v(this));
            ImageView imageView3 = (ImageView) view.findViewById(R.id.ringitem_failed);
            imageView3.setOnClickListener(new w(this, i));
            ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.ringitem_download_progress);
            progressBar.setVisibility(4);
            imageView.setVisibility(4);
            imageView2.setVisibility(4);
            imageView3.setVisibility(4);
            PlayerService b = ak.a().b();
            if (i != RingSettingFragment.this.h || b == null) {
                imageView.setVisibility(0);
            } else if (RingSettingFragment.this.i) {
                switch (b.a()) {
                    case 1:
                        progressBar.setVisibility(0);
                        break;
                    case 2:
                        imageView2.setVisibility(0);
                        break;
                    case 3:
                    case 4:
                    case 5:
                        imageView.setVisibility(0);
                        break;
                    case 6:
                        imageView3.setVisibility(0);
                        break;
                }
            } else {
                imageView.setVisibility(0);
            }
            ((TextView) view.findViewById(R.id.ring_name)).setText(((e) RingSettingFragment.this.e.get(i)).b);
            TextView textView2 = (TextView) view.findViewById(R.id.ring_duration);
            if (((e) RingSettingFragment.this.e.get(i)).c == 0) {
                textView2.setVisibility(8);
            } else {
                int i2 = ((e) RingSettingFragment.this.e.get(i)).c / Constants.CLEARIMGED;
                StringBuilder append = new StringBuilder().append("时长:");
                if (i2 == 0) {
                    i2 = 1;
                }
                textView2.setText(append.append(i2).append("秒").toString());
            }
            return view;
        }
    }
}
