package com.shoujiduoduo.ui.mine;

import android.content.Intent;
import android.view.View;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.ui.makering.MakeRingActivity;
import com.shoujiduoduo.util.ak;
import org.apache.mina.proxy.handlers.http.ntlm.NTLMConstants;

/* compiled from: MakeRingFragment */
class u implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ MakeRingFragment f1315a;

    u(MakeRingFragment makeRingFragment) {
        this.f1315a = makeRingFragment;
    }

    public void onClick(View view) {
        PlayerService b = ak.a().b();
        if (b != null && b.j()) {
            b.k();
        }
        Intent intent = new Intent();
        intent.setClass(this.f1315a.getActivity(), MakeRingActivity.class);
        intent.setFlags(NTLMConstants.FLAG_NEGOTIATE_128_BIT_ENCRYPTION);
        this.f1315a.getActivity().startActivity(intent);
    }
}
