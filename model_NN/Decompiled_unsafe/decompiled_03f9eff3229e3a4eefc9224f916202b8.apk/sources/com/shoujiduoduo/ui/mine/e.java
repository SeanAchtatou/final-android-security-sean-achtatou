package com.shoujiduoduo.ui.mine;

import android.view.View;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.util.widget.a;
import com.shoujiduoduo.util.widget.f;
import java.util.List;

/* compiled from: FavoriteRingFragment */
class e implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ FavoriteRingFragment f1299a;

    e(FavoriteRingFragment favoriteRingFragment) {
        this.f1299a = favoriteRingFragment;
    }

    public void onClick(View view) {
        List<Integer> b = this.f1299a.c.b();
        if (b == null || b.size() == 0) {
            f.a("请选择要删除的铃声", 0);
        } else {
            new a.C0034a(this.f1299a.getActivity()).b((int) R.string.hint).a("确定删除选中的铃声吗？").a((int) R.string.ok, new g(this, b)).b((int) R.string.cancel, new f(this)).a().show();
        }
    }
}
