package com.shoujiduoduo.ui.mine;

import android.os.Bundle;
import com.shoujiduoduo.b.c.n;
import com.shoujiduoduo.base.bean.f;
import com.shoujiduoduo.ui.utils.DDListFragment;
import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;

/* compiled from: MyRingtoneScene */
class ar extends a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ap f1232a;

    ar(ap apVar) {
        this.f1232a = apVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.ui.mine.ap.a(com.shoujiduoduo.ui.mine.ap, boolean):boolean
     arg types: [com.shoujiduoduo.ui.mine.ap, int]
     candidates:
      com.shoujiduoduo.ui.mine.ap.a(int, android.view.KeyEvent):boolean
      com.shoujiduoduo.ui.mine.ap.a(com.shoujiduoduo.ui.mine.ap, boolean):boolean */
    public void a(c.b bVar) {
        super.a(bVar);
        if (bVar != null && (bVar instanceof c.d)) {
            c.d dVar = (c.d) bVar;
            if (!dVar.d() || (!dVar.e() && !dVar.f())) {
                com.shoujiduoduo.base.a.a.a("MyRingtoneScene", "vip:" + dVar.e() + ", cailing:" + dVar.d());
                return;
            }
            com.shoujiduoduo.base.a.a.a("MyRingtoneScene", "vip 和彩铃均开通，显示彩铃");
            boolean unused = this.f1232a.u = true;
            n nVar = new n(f.a.list_ring_ctcc, "", false, "");
            DDListFragment dDListFragment = new DDListFragment();
            Bundle bundle = new Bundle();
            bundle.putBoolean("support_lazy_load", true);
            bundle.putString("adapter_type", "cailing_list_adapter");
            dDListFragment.setArguments(bundle);
            dDListFragment.a(nVar);
            if (this.f1232a.c.size() == 3) {
                this.f1232a.c.add(1, dDListFragment);
                com.shoujiduoduo.base.a.a.a("MyRingtoneScene", "add cailing frag, now fragsize:" + this.f1232a.c.size());
                boolean unused2 = this.f1232a.u = true;
                this.f1232a.q.a();
                this.f1232a.b.getAdapter().notifyDataSetChanged();
            }
        }
    }

    public void b(c.b bVar) {
        super.b(bVar);
    }
}
