package com.shoujiduoduo.ui.makering;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.View;
import com.shoujiduoduo.player.a;

/* compiled from: MakeRingTypeFragment */
class q implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ MakeRingTypeFragment f1203a;

    q(MakeRingTypeFragment makeRingTypeFragment) {
        this.f1203a = makeRingTypeFragment;
    }

    public void onClick(View view) {
        if (!a.a()) {
            new AlertDialog.Builder(this.f1203a.getActivity()).setTitle("录制").setMessage("对不起，录制模块加载失败！").setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
        } else {
            this.f1203a.c.a("record");
        }
    }
}
