package com.shoujiduoduo.ui.makering;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import cn.banshenggua.aichang.utils.Constants;
import com.qq.e.comm.constants.ErrorCode;
import com.shoujiduoduo.player.c;
import com.shoujiduoduo.player.m;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.util.be;
import com.shoujiduoduo.util.widget.Chronometer;
import com.shoujiduoduo.util.widget.WaveformView;
import com.shoujiduoduo.util.widget.k;
import java.io.File;
import java.io.FileInputStream;
import java.util.Timer;

public class MakeRingRecordFragment extends Fragment implements c.b, c.C0022c, c.f, m.b, m.c, WaveformView.b {
    /* access modifiers changed from: private */
    public int A;
    /* access modifiers changed from: private */
    public int B;
    /* access modifiers changed from: private */
    public int C;
    /* access modifiers changed from: private */
    public float D = 0.0f;
    /* access modifiers changed from: private */
    public float E = 1.0f;
    /* access modifiers changed from: private */
    public int F;
    /* access modifiers changed from: private */
    public int G;
    /* access modifiers changed from: private */
    public int H;
    /* access modifiers changed from: private */
    public int I;
    private int J;
    private long K;
    private float L;
    /* access modifiers changed from: private */
    public boolean M;
    /* access modifiers changed from: private */
    public boolean N;
    /* access modifiers changed from: private */
    public boolean O;
    private final int P = 5;
    private final int Q = 11025;
    /* access modifiers changed from: private */
    public com.shoujiduoduo.player.a R;
    /* access modifiers changed from: private */
    public MediaPlayer S;
    /* access modifiers changed from: private */
    public boolean T;
    /* access modifiers changed from: private */
    public d U = d.nothing;
    /* access modifiers changed from: private */
    public e V;
    /* access modifiers changed from: private */
    public b W;
    private int X = -1;

    /* renamed from: a  reason: collision with root package name */
    k<Integer> f1178a;
    /* access modifiers changed from: private */
    public Chronometer b;
    private Button c;
    private Button d;
    private View e;
    private View f;
    private View g;
    /* access modifiers changed from: private */
    public WaveformView h;
    /* access modifiers changed from: private */
    public ImageButton i;
    /* access modifiers changed from: private */
    public ImageButton j;
    /* access modifiers changed from: private */
    public ImageButton k;
    private ImageButton l;
    /* access modifiers changed from: private */
    public TextView m;
    /* access modifiers changed from: private */
    public TextView n;
    private TextView o;
    private Button p;
    private Button q;
    private Button r;
    private Button s;
    private Button t;
    private Button u;
    /* access modifiers changed from: private */
    public ImageButton v;
    private ViewGroup w;
    private TextView x;
    private TextView y;
    private Timer z;

    public interface b {
        void b(String str);

        void c();
    }

    public enum d {
        record,
        recording,
        rec_pause,
        edit,
        song_edit,
        nothing
    }

    static /* synthetic */ float c(MakeRingRecordFragment makeRingRecordFragment, float f2) {
        float f3 = makeRingRecordFragment.D - f2;
        makeRingRecordFragment.D = f3;
        return f3;
    }

    static /* synthetic */ float d(MakeRingRecordFragment makeRingRecordFragment, float f2) {
        float f3 = makeRingRecordFragment.E - f2;
        makeRingRecordFragment.E = f3;
        return f3;
    }

    static /* synthetic */ float e(MakeRingRecordFragment makeRingRecordFragment, float f2) {
        float f3 = makeRingRecordFragment.D + f2;
        makeRingRecordFragment.D = f3;
        return f3;
    }

    static /* synthetic */ float f(MakeRingRecordFragment makeRingRecordFragment, float f2) {
        float f3 = makeRingRecordFragment.E + f2;
        makeRingRecordFragment.E = f3;
        return f3;
    }

    public void onCreate(Bundle bundle) {
        com.shoujiduoduo.base.a.a.a("MakeRingRecordFragment", "onCreate");
        super.onCreate(bundle);
        if (getArguments() != null) {
            this.X = getArguments().getInt("timelimit", -1);
        }
        this.R = com.shoujiduoduo.player.a.b();
        this.R.a((m.c) this);
        this.R.a((m.b) this);
        if (this.U.equals(d.nothing)) {
            this.U = d.record;
        }
        this.V = new e();
        this.z = new Timer();
        this.M = false;
        this.O = false;
        this.F = 0;
        this.E = 1.0f;
        this.D = 0.0f;
        this.T = false;
        new h(this).start();
    }

    public void a(d dVar) {
        this.U = dVar;
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.W = (b) activity;
        } catch (ClassCastException e2) {
            e2.printStackTrace();
            throw new ClassCastException(activity.toString() + " must implents OnRingRecordListener");
        }
    }

    public void onDestroy() {
        super.onDestroy();
        com.shoujiduoduo.base.a.a.a("MakeRingRecordFragment", "onDestroy");
        if (this.R != null) {
            this.R.b((m.b) this);
            this.R.b((m.c) this);
            this.R.i();
        }
        this.E = 1.0f;
        this.D = 0.0f;
        this.F = 0;
        this.G = 0;
        this.z.cancel();
        be.a().i();
        if (this.V != null) {
            this.V.removeCallbacksAndMessages(null);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        com.shoujiduoduo.base.a.a.a("MakeRingRecordFragment", "onCreateView");
        View inflate = layoutInflater.inflate((int) R.layout.fragment_makering_record, viewGroup, false);
        this.x = (TextView) inflate.findViewById(R.id.txt_instructions);
        this.e = inflate.findViewById(R.id.rec_controls);
        this.f = inflate.findViewById(R.id.edit_controls);
        a aVar = new a();
        this.i = (ImageButton) this.e.findViewById(R.id.btn_rec_record);
        this.i.setOnClickListener(aVar);
        this.j = (ImageButton) this.e.findViewById(R.id.btn_rec_pause);
        this.j.setOnClickListener(aVar);
        this.k = (ImageButton) this.e.findViewById(R.id.btn_play);
        this.m = (TextView) this.e.findViewById(R.id.tv_play_inst);
        this.k.setOnClickListener(aVar);
        this.l = (ImageButton) this.e.findViewById(R.id.btn_edit);
        this.o = (TextView) this.e.findViewById(R.id.tv_edit_inst);
        this.l.setOnClickListener(aVar);
        this.v = (ImageButton) this.f.findViewById(R.id.btn_play_edit);
        this.n = (TextView) this.f.findViewById(R.id.tv_play_edit_inst);
        this.v.setOnClickListener(aVar);
        this.p = (Button) this.f.findViewById(R.id.ibtn_adjust_back_start);
        this.p.setOnClickListener(aVar);
        this.q = (Button) this.f.findViewById(R.id.ibtn_adjust_forward_start);
        this.q.setOnClickListener(aVar);
        this.r = (Button) this.f.findViewById(R.id.ibtn_adjust_back_end);
        this.r.setOnClickListener(aVar);
        this.s = (Button) this.f.findViewById(R.id.ibtn_adjust_forward_end);
        this.s.setOnClickListener(aVar);
        this.u = (Button) this.f.findViewById(R.id.ibtn_set_end);
        this.u.setOnClickListener(aVar);
        this.t = (Button) this.f.findViewById(R.id.ibtn_set_start);
        this.t.setOnClickListener(aVar);
        this.g = inflate.findViewById(R.id.bottom_control);
        this.c = (Button) this.g.findViewById(R.id.btn_left);
        this.c.setOnClickListener(aVar);
        this.d = (Button) this.g.findViewById(R.id.btn_right);
        this.d.setOnClickListener(aVar);
        this.b = (Chronometer) inflate.findViewById(R.id.chronometer);
        this.b.setVisibility(4);
        this.h = (WaveformView) inflate.findViewById(R.id.gauge_holder);
        this.h.setVisibility(4);
        this.y = (TextView) inflate.findViewById(R.id.tv_rec_bubble_tips);
        this.f1178a = new k<>(0, Integer.valueOf(viewGroup.getWidth()), getActivity());
        this.C = viewGroup.getWidth();
        if (this.G == 0) {
            this.G = this.C;
        }
        this.A = 0;
        this.B = viewGroup.getWidth();
        this.f1178a.setOnRangeSeekBarChangeListener(new c(this, null));
        this.f1178a.setNotifyWhileDragging(true);
        this.w = (ViewGroup) inflate.findViewById(R.id.range_seek_bar);
        this.w.addView(this.f1178a);
        this.z.schedule(new i(this), 0, 500);
        this.h.setListener(this);
        this.h.setWavDataProcess(be.a());
        b(this.U);
        return inflate;
    }

    public void onStart() {
        com.shoujiduoduo.base.a.a.a("MakeRingRecordFragment", "onStart");
        super.onStart();
    }

    public void onStop() {
        com.shoujiduoduo.base.a.a.a("MakeRingRecordFragment", "onStop");
        super.onStop();
    }

    public void onDestroyView() {
        com.shoujiduoduo.base.a.a.a("MakeRingRecordFragment", "onDestroyView");
        super.onDestroyView();
    }

    private class c implements k.b<Integer> {
        private c() {
        }

        /* synthetic */ c(MakeRingRecordFragment makeRingRecordFragment, h hVar) {
            this();
        }

        public /* bridge */ /* synthetic */ void a(k kVar, Object obj, Object obj2) {
            a((k<?>) kVar, (Integer) obj, (Integer) obj2);
        }

        public void a(k<?> kVar, Integer num, Integer num2) {
            MakeRingRecordFragment.this.h();
            if (num2.intValue() != MakeRingRecordFragment.this.B || num.intValue() != MakeRingRecordFragment.this.A) {
                com.shoujiduoduo.base.a.a.a("MakeRingRecordFragment", "min:" + num + " max:" + num2);
                if (MakeRingRecordFragment.this.M) {
                    MakeRingRecordFragment.this.j();
                }
                if (num2.intValue() != MakeRingRecordFragment.this.B) {
                    int unused = MakeRingRecordFragment.this.B = num2.intValue();
                    float unused2 = MakeRingRecordFragment.this.E = ((float) MakeRingRecordFragment.this.B) / ((float) MakeRingRecordFragment.this.C);
                    be.a().b(MakeRingRecordFragment.this.E);
                    int unused3 = MakeRingRecordFragment.this.G = num2.intValue();
                    MakeRingRecordFragment.this.h.setEndPos(MakeRingRecordFragment.this.B);
                    MakeRingRecordFragment.this.m();
                }
                if (num.intValue() != MakeRingRecordFragment.this.A) {
                    int unused4 = MakeRingRecordFragment.this.A = num.intValue();
                    float unused5 = MakeRingRecordFragment.this.D = ((float) MakeRingRecordFragment.this.A) / ((float) MakeRingRecordFragment.this.C);
                    be.a().a(MakeRingRecordFragment.this.D);
                    int unused6 = MakeRingRecordFragment.this.F = num.intValue();
                    MakeRingRecordFragment.this.h.setStartPos(MakeRingRecordFragment.this.A);
                    MakeRingRecordFragment.this.m();
                }
                MakeRingRecordFragment.this.b.setDuration((long) ((((float) be.a().c()) / 1000.0f) * (MakeRingRecordFragment.this.E - MakeRingRecordFragment.this.D)));
            }
        }
    }

    /* access modifiers changed from: private */
    public void h() {
        if (this.f1178a.getFocusThumb().equals(k.c.MIN)) {
            this.p.setEnabled(true);
            this.q.setEnabled(true);
            this.r.setEnabled(false);
            this.s.setEnabled(false);
            return;
        }
        this.p.setEnabled(false);
        this.q.setEnabled(false);
        this.r.setEnabled(true);
        this.s.setEnabled(true);
    }

    class e extends Handler {
        e() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.ui.makering.MakeRingRecordFragment.b(com.shoujiduoduo.ui.makering.MakeRingRecordFragment, boolean):boolean
         arg types: [com.shoujiduoduo.ui.makering.MakeRingRecordFragment, int]
         candidates:
          com.shoujiduoduo.ui.makering.MakeRingRecordFragment.b(com.shoujiduoduo.ui.makering.MakeRingRecordFragment, float):float
          com.shoujiduoduo.ui.makering.MakeRingRecordFragment.b(com.shoujiduoduo.ui.makering.MakeRingRecordFragment, int):int
          com.shoujiduoduo.ui.makering.MakeRingRecordFragment.b(com.shoujiduoduo.ui.makering.MakeRingRecordFragment, com.shoujiduoduo.ui.makering.MakeRingRecordFragment$d):com.shoujiduoduo.ui.makering.MakeRingRecordFragment$d
          com.shoujiduoduo.ui.makering.MakeRingRecordFragment.b(com.shoujiduoduo.ui.makering.MakeRingRecordFragment, boolean):boolean */
        public void handleMessage(Message message) {
            super.handleMessage(message);
            switch (message.what) {
                case 55:
                    com.shoujiduoduo.base.a.a.a("MakeRingRecordFragment", "wavplayer play error");
                    Toast.makeText(MakeRingRecordFragment.this.getActivity(), (int) R.string.play_error, 0).show();
                    MakeRingRecordFragment.this.v.setVisibility(0);
                    MakeRingRecordFragment.this.k.setVisibility(0);
                    MakeRingRecordFragment.this.m.setVisibility(0);
                    MakeRingRecordFragment.this.n.setVisibility(0);
                    return;
                case 56:
                default:
                    return;
                case D.TYPE_QHVIDEOADATTRIBUTES:
                    if (MakeRingRecordFragment.this.U.equals(d.recording)) {
                        MakeRingRecordFragment.this.b.setDuration((long) (be.a().c() / Constants.CLEARIMGED));
                        return;
                    }
                    int c = (int) (((float) (be.a().c() / Constants.CLEARIMGED)) * (MakeRingRecordFragment.this.E - MakeRingRecordFragment.this.D));
                    if (!MakeRingRecordFragment.this.M || MakeRingRecordFragment.this.S == null) {
                        MakeRingRecordFragment.this.b.setDuration((long) c);
                        return;
                    }
                    long currentPosition = (long) MakeRingRecordFragment.this.S.getCurrentPosition();
                    if (MakeRingRecordFragment.this.H == 0) {
                        currentPosition -= (long) MakeRingRecordFragment.this.I;
                    }
                    com.shoujiduoduo.base.a.a.a("MakeRingRecordFragment", "cur play pos:" + currentPosition);
                    MakeRingRecordFragment.this.b.a((long) ((int) (((float) currentPosition) / 1000.0f)), (long) c);
                    return;
                case 58:
                    if (message.obj != null) {
                        switch (((com.shoujiduoduo.player.a) message.obj).l()) {
                            case Start:
                                MakeRingRecordFragment.this.b.setVisibility(0);
                                MakeRingRecordFragment.this.j.setVisibility(0);
                                MakeRingRecordFragment.this.i.setVisibility(4);
                                return;
                            case Pause:
                                MakeRingRecordFragment.this.j.setVisibility(4);
                                MakeRingRecordFragment.this.i.setVisibility(0);
                                return;
                            case Stop:
                                MakeRingRecordFragment.this.j.setVisibility(4);
                                MakeRingRecordFragment.this.i.setVisibility(0);
                                return;
                            case Complete:
                            default:
                                return;
                            case Error:
                                MakeRingRecordFragment.this.j.setVisibility(4);
                                MakeRingRecordFragment.this.i.setVisibility(0);
                                if (MakeRingRecordFragment.this.R != null) {
                                    MakeRingRecordFragment.this.R.h();
                                }
                                Toast.makeText(MakeRingRecordFragment.this.getActivity(), (int) R.string.record_right_error, 1).show();
                                return;
                        }
                    } else {
                        return;
                    }
                case 59:
                    MakeRingRecordFragment.this.R.h();
                    MakeRingRecordFragment.this.b(d.rec_pause);
                    boolean unused = MakeRingRecordFragment.this.O = false;
                    MakeRingRecordFragment.this.f();
                    return;
            }
        }
    }

    class a implements View.OnClickListener {
        a() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.ui.makering.MakeRingRecordFragment.b(com.shoujiduoduo.ui.makering.MakeRingRecordFragment, boolean):boolean
         arg types: [com.shoujiduoduo.ui.makering.MakeRingRecordFragment, int]
         candidates:
          com.shoujiduoduo.ui.makering.MakeRingRecordFragment.b(com.shoujiduoduo.ui.makering.MakeRingRecordFragment, float):float
          com.shoujiduoduo.ui.makering.MakeRingRecordFragment.b(com.shoujiduoduo.ui.makering.MakeRingRecordFragment, int):int
          com.shoujiduoduo.ui.makering.MakeRingRecordFragment.b(com.shoujiduoduo.ui.makering.MakeRingRecordFragment, com.shoujiduoduo.ui.makering.MakeRingRecordFragment$d):com.shoujiduoduo.ui.makering.MakeRingRecordFragment$d
          com.shoujiduoduo.ui.makering.MakeRingRecordFragment.b(com.shoujiduoduo.ui.makering.MakeRingRecordFragment, boolean):boolean */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.ui.makering.MakeRingRecordFragment.b(com.shoujiduoduo.ui.makering.MakeRingRecordFragment, float):float
         arg types: [com.shoujiduoduo.ui.makering.MakeRingRecordFragment, int]
         candidates:
          com.shoujiduoduo.ui.makering.MakeRingRecordFragment.b(com.shoujiduoduo.ui.makering.MakeRingRecordFragment, int):int
          com.shoujiduoduo.ui.makering.MakeRingRecordFragment.b(com.shoujiduoduo.ui.makering.MakeRingRecordFragment, com.shoujiduoduo.ui.makering.MakeRingRecordFragment$d):com.shoujiduoduo.ui.makering.MakeRingRecordFragment$d
          com.shoujiduoduo.ui.makering.MakeRingRecordFragment.b(com.shoujiduoduo.ui.makering.MakeRingRecordFragment, boolean):boolean
          com.shoujiduoduo.ui.makering.MakeRingRecordFragment.b(com.shoujiduoduo.ui.makering.MakeRingRecordFragment, float):float */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.ui.makering.MakeRingRecordFragment.c(com.shoujiduoduo.ui.makering.MakeRingRecordFragment, boolean):void
         arg types: [com.shoujiduoduo.ui.makering.MakeRingRecordFragment, int]
         candidates:
          com.shoujiduoduo.ui.makering.MakeRingRecordFragment.c(com.shoujiduoduo.ui.makering.MakeRingRecordFragment, float):float
          com.shoujiduoduo.ui.makering.MakeRingRecordFragment.c(com.shoujiduoduo.ui.makering.MakeRingRecordFragment, int):int
          com.shoujiduoduo.ui.makering.MakeRingRecordFragment.c(com.shoujiduoduo.ui.makering.MakeRingRecordFragment, boolean):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.ui.makering.MakeRingRecordFragment.d(com.shoujiduoduo.ui.makering.MakeRingRecordFragment, boolean):void
         arg types: [com.shoujiduoduo.ui.makering.MakeRingRecordFragment, int]
         candidates:
          com.shoujiduoduo.ui.makering.MakeRingRecordFragment.d(com.shoujiduoduo.ui.makering.MakeRingRecordFragment, float):float
          com.shoujiduoduo.ui.makering.MakeRingRecordFragment.d(com.shoujiduoduo.ui.makering.MakeRingRecordFragment, int):int
          com.shoujiduoduo.ui.makering.MakeRingRecordFragment.d(com.shoujiduoduo.ui.makering.MakeRingRecordFragment, boolean):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.ui.makering.MakeRingRecordFragment.a(com.shoujiduoduo.ui.makering.MakeRingRecordFragment, float):float
         arg types: [com.shoujiduoduo.ui.makering.MakeRingRecordFragment, int]
         candidates:
          com.shoujiduoduo.ui.makering.MakeRingRecordFragment.a(com.shoujiduoduo.ui.makering.MakeRingRecordFragment, int):int
          com.shoujiduoduo.ui.makering.MakeRingRecordFragment.a(com.shoujiduoduo.ui.makering.MakeRingRecordFragment, android.media.MediaPlayer):android.media.MediaPlayer
          com.shoujiduoduo.ui.makering.MakeRingRecordFragment.a(com.shoujiduoduo.ui.makering.MakeRingRecordFragment, com.shoujiduoduo.ui.makering.MakeRingRecordFragment$d):void
          com.shoujiduoduo.ui.makering.MakeRingRecordFragment.a(com.shoujiduoduo.ui.makering.MakeRingRecordFragment, boolean):boolean
          com.shoujiduoduo.ui.makering.MakeRingRecordFragment.a(com.shoujiduoduo.player.m, long):void
          com.shoujiduoduo.ui.makering.MakeRingRecordFragment.a(com.shoujiduoduo.player.m, com.shoujiduoduo.player.m$a):void
          com.shoujiduoduo.player.m.b.a(com.shoujiduoduo.player.m, long):void
          com.shoujiduoduo.player.m.c.a(com.shoujiduoduo.player.m, com.shoujiduoduo.player.m$a):void
          com.shoujiduoduo.ui.makering.MakeRingRecordFragment.a(com.shoujiduoduo.ui.makering.MakeRingRecordFragment, float):float */
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_left:
                    if (MakeRingRecordFragment.this.U.equals(d.rec_pause)) {
                        new AlertDialog.Builder(MakeRingRecordFragment.this.getActivity()).setMessage((int) R.string.delete_record_confirm).setTitle((int) R.string.hint).setIcon(17301543).setNegativeButton((int) R.string.cancel, (DialogInterface.OnClickListener) null).setPositiveButton((int) R.string.ok, new l(this)).show();
                        return;
                    } else if (MakeRingRecordFragment.this.U.equals(d.edit)) {
                        new AlertDialog.Builder(MakeRingRecordFragment.this.getActivity()).setMessage(MakeRingRecordFragment.this.getResources().getString(R.string.delete_modify_confirm)).setTitle((int) R.string.hint).setIcon(17301543).setNegativeButton((int) R.string.cancel, (DialogInterface.OnClickListener) null).setPositiveButton((int) R.string.ok, new m(this)).show();
                        return;
                    } else if (MakeRingRecordFragment.this.U.equals(d.song_edit)) {
                        new AlertDialog.Builder(MakeRingRecordFragment.this.getActivity()).setMessage((int) R.string.edit_quit_confirm).setTitle((int) R.string.hint).setIcon(17301543).setNegativeButton((int) R.string.cancel, (DialogInterface.OnClickListener) null).setPositiveButton((int) R.string.ok, new n(this)).show();
                        return;
                    } else {
                        return;
                    }
                case R.id.btn_right:
                    if (MakeRingRecordFragment.this.M) {
                        MakeRingRecordFragment.this.j();
                    }
                    MakeRingRecordFragment.this.W.c();
                    return;
                case R.id.range_seek_bar:
                case R.id.layout_wav_form:
                case R.id.chronometer:
                case R.id.gauge_holder:
                case R.id.txt_instructions:
                case R.id.control_layout:
                case R.id.rec_controls:
                case R.id.tv_play_inst:
                case R.id.tv_edit_inst:
                case R.id.edit_controls:
                case R.id.layout_edit_control:
                case R.id.tv_play_edit_inst:
                case R.id.layout_adjust_start:
                case R.id.layout_adjust_end:
                default:
                    return;
                case R.id.btn_play:
                    MakeRingRecordFragment.this.a(MakeRingRecordFragment.this.F);
                    MakeRingRecordFragment.this.h.setDrawState(MakeRingRecordFragment.this.M ? WaveformView.a.listen_play : WaveformView.a.listen_pause);
                    return;
                case R.id.btn_rec_record:
                    if (!com.shoujiduoduo.player.a.b().l().equals(m.a.Pause)) {
                        com.shoujiduoduo.player.a.b().k();
                        be.a().h();
                        MakeRingRecordFragment.this.b();
                        MakeRingRecordFragment.this.R.f();
                        com.umeng.analytics.b.b(MakeRingRecordFragment.this.getActivity(), "RECORD_RING");
                    } else {
                        if (MakeRingRecordFragment.this.M) {
                            MakeRingRecordFragment.this.j();
                        }
                        MakeRingRecordFragment.this.g();
                        MakeRingRecordFragment.this.R.g();
                    }
                    MakeRingRecordFragment.this.b(d.recording);
                    boolean unused = MakeRingRecordFragment.this.O = true;
                    MakeRingRecordFragment.this.m();
                    return;
                case R.id.btn_rec_pause:
                    MakeRingRecordFragment.this.R.h();
                    MakeRingRecordFragment.this.b(d.rec_pause);
                    boolean unused2 = MakeRingRecordFragment.this.O = false;
                    MakeRingRecordFragment.this.f();
                    return;
                case R.id.btn_edit:
                    MakeRingRecordFragment.this.b(d.edit);
                    MakeRingRecordFragment.this.m();
                    return;
                case R.id.btn_play_edit:
                    MakeRingRecordFragment.this.a(MakeRingRecordFragment.this.F);
                    MakeRingRecordFragment.this.h.setDrawState(MakeRingRecordFragment.this.M ? WaveformView.a.edit_play : WaveformView.a.edit_pause);
                    return;
                case R.id.ibtn_adjust_forward_start:
                case R.id.ibtn_adjust_forward_end:
                    if (MakeRingRecordFragment.this.M) {
                        MakeRingRecordFragment.this.j();
                    }
                    int c = be.a().c();
                    if (c != 0) {
                        if (MakeRingRecordFragment.this.f1178a.getFocusThumb().equals(k.c.MIN)) {
                            MakeRingRecordFragment.c(MakeRingRecordFragment.this, ((float) MakeRingRecordFragment.this.i()) / ((float) c));
                            if (MakeRingRecordFragment.this.D < 0.0f) {
                                float unused3 = MakeRingRecordFragment.this.D = 0.0f;
                            }
                            MakeRingRecordFragment.this.b(true);
                            com.shoujiduoduo.base.a.a.a("MakeRingRecordFragment", "forward_adjust, new min value:" + MakeRingRecordFragment.this.D);
                        } else {
                            MakeRingRecordFragment.d(MakeRingRecordFragment.this, ((float) MakeRingRecordFragment.this.i()) / ((float) c));
                            if (MakeRingRecordFragment.this.E <= MakeRingRecordFragment.this.D) {
                                float unused4 = MakeRingRecordFragment.this.E = MakeRingRecordFragment.this.D;
                            }
                            MakeRingRecordFragment.this.a(true);
                            com.shoujiduoduo.base.a.a.a("MakeRingRecordFragment", "forward_adjust, new max value:" + MakeRingRecordFragment.this.E);
                        }
                        MakeRingRecordFragment.this.m();
                        return;
                    }
                    return;
                case R.id.ibtn_adjust_back_start:
                case R.id.ibtn_adjust_back_end:
                    if (MakeRingRecordFragment.this.M) {
                        MakeRingRecordFragment.this.j();
                    }
                    int c2 = be.a().c();
                    if (c2 != 0) {
                        if (MakeRingRecordFragment.this.f1178a.getFocusThumb().equals(k.c.MIN)) {
                            MakeRingRecordFragment.e(MakeRingRecordFragment.this, ((float) MakeRingRecordFragment.this.i()) / ((float) c2));
                            if (MakeRingRecordFragment.this.D >= MakeRingRecordFragment.this.E) {
                                float unused5 = MakeRingRecordFragment.this.D = MakeRingRecordFragment.this.E;
                            }
                            MakeRingRecordFragment.this.b(true);
                            com.shoujiduoduo.base.a.a.b("MakeRingRecordFragment", "back_adjust, new max value:" + MakeRingRecordFragment.this.E);
                        } else {
                            MakeRingRecordFragment.f(MakeRingRecordFragment.this, ((float) MakeRingRecordFragment.this.i()) / ((float) c2));
                            if (MakeRingRecordFragment.this.E > 1.0f) {
                                float unused6 = MakeRingRecordFragment.this.E = 1.0f;
                            }
                            MakeRingRecordFragment.this.a(true);
                            com.shoujiduoduo.base.a.a.b("MakeRingRecordFragment", "back_adjust, new max value:" + MakeRingRecordFragment.this.E);
                        }
                        MakeRingRecordFragment.this.m();
                        return;
                    }
                    return;
                case R.id.ibtn_set_end:
                    if (MakeRingRecordFragment.this.M && MakeRingRecordFragment.this.S != null) {
                        MakeRingRecordFragment.this.j();
                        int c3 = be.a().c();
                        int v = MakeRingRecordFragment.this.H + MakeRingRecordFragment.this.S.getCurrentPosition();
                        if (c3 != 0) {
                            float unused7 = MakeRingRecordFragment.this.E = ((float) v) / ((float) c3);
                            com.shoujiduoduo.base.a.a.a("MakeRingRecordFragment", "curpos:" + v + " duration:" + c3 + " set_end:" + MakeRingRecordFragment.this.E);
                            if (MakeRingRecordFragment.this.E > 1.0f) {
                                float unused8 = MakeRingRecordFragment.this.E = 1.0f;
                            }
                            MakeRingRecordFragment.this.a(false);
                        }
                    }
                    MakeRingRecordFragment.this.m();
                    return;
                case R.id.ibtn_set_start:
                    if ((MakeRingRecordFragment.this.M || MakeRingRecordFragment.this.N) && MakeRingRecordFragment.this.S != null) {
                        int c4 = be.a().c();
                        int v2 = MakeRingRecordFragment.this.H + MakeRingRecordFragment.this.S.getCurrentPosition();
                        if (c4 != 0) {
                            float unused9 = MakeRingRecordFragment.this.D = ((float) v2) / ((float) c4);
                            if (MakeRingRecordFragment.this.D >= MakeRingRecordFragment.this.E) {
                                float unused10 = MakeRingRecordFragment.this.D = MakeRingRecordFragment.this.E;
                            }
                            com.shoujiduoduo.base.a.a.a("MakeRingRecordFragment", "curpos:" + v2 + " duration:" + c4 + "  nor_start:" + MakeRingRecordFragment.this.D);
                            MakeRingRecordFragment.this.b(false);
                        }
                    }
                    MakeRingRecordFragment.this.m();
                    return;
            }
        }
    }

    /* access modifiers changed from: private */
    public int i() {
        int c2 = be.a().c();
        if (c2 > 180000) {
            return 500;
        }
        if (c2 > 120000) {
            return ErrorCode.NetWorkError.STUB_NETWORK_ERROR;
        }
        if (c2 > 60000) {
            return ErrorCode.InitError.INIT_AD_ERROR;
        }
        if (c2 > 30000) {
            return 200;
        }
        if (c2 > 10000) {
            return 100;
        }
        if (c2 > 5000) {
            return 50;
        }
        return 25;
    }

    /* access modifiers changed from: private */
    public synchronized void j() {
        if (this.S != null && this.S.isPlaying()) {
            this.S.pause();
        }
        this.h.setPlayback(-1);
        this.M = false;
        this.N = true;
        this.m.setText((int) R.string.pre_listen);
        this.n.setText((int) R.string.pre_listen);
        k();
    }

    /* access modifiers changed from: private */
    public synchronized void a(int i2) {
        if (this.M) {
            j();
        } else {
            this.N = false;
            if (this.S != null) {
                try {
                    int c2 = be.a().c();
                    this.I = (int) (((float) c2) * (((float) i2) / ((float) this.h.getWidth())));
                    if (i2 < this.F) {
                        this.J = (int) (((float) c2) * (((float) this.F) / ((float) this.h.getWidth())));
                    } else if (i2 > this.G) {
                        this.J = c2;
                    } else {
                        this.J = (int) (((float) c2) * (((float) this.G) / ((float) this.h.getWidth())));
                    }
                    this.H = 0;
                    int a2 = be.a().a(this.I);
                    int a3 = be.a().a(this.J);
                    File file = new File(be.a().e());
                    if (!this.T || a2 < 0 || a3 < 0) {
                        this.S.reset();
                        this.S.setDataSource(be.a().e());
                        this.S.setAudioStreamType(3);
                        this.S.prepare();
                    } else {
                        try {
                            this.S.reset();
                            this.S.setAudioStreamType(3);
                            this.S.setDataSource(new FileInputStream(file.getAbsolutePath()).getFD(), (long) a2, (long) (a3 - a2));
                            this.S.prepare();
                            this.H = this.I;
                        } catch (Exception e2) {
                            System.out.println("Exception trying to play file subset");
                            this.S.reset();
                            this.S.setAudioStreamType(3);
                            this.S.setDataSource(file.getAbsolutePath());
                            this.S.prepare();
                            this.H = 0;
                        }
                    }
                    this.S.setOnCompletionListener(new j(this));
                    this.M = true;
                    this.m.setText((int) R.string.pause);
                    this.n.setText((int) R.string.pause);
                    if (this.H == 0) {
                        this.S.seekTo(this.I);
                    }
                    this.S.start();
                    m();
                    k();
                } catch (Exception e3) {
                    new AlertDialog.Builder(getActivity()).setMessage((int) R.string.play_error).setTitle((int) R.string.hint).setIcon(17301543).setNegativeButton((int) R.string.cancel, (DialogInterface.OnClickListener) null).setPositiveButton((int) R.string.ok, new k(this)).show();
                }
            }
        }
        return;
    }

    private void k() {
        if (this.M) {
            this.k.setImageResource(R.drawable.btn_rec_play_pause_states);
            this.v.setImageResource(R.drawable.btn_rec_play_pause_states);
            return;
        }
        this.k.setImageResource(R.drawable.btn_rec_play_states);
        this.v.setImageResource(R.drawable.btn_rec_play_states);
    }

    /* access modifiers changed from: private */
    public void a(boolean z2) {
        if (this.M && z2) {
            j();
        }
        this.f1178a.setNormalizedMaxValue((double) this.E);
        be.a().b(this.E);
        this.G = this.f1178a.getSelectedMaxValue().intValue();
        this.h.setEndPos(this.G);
        h();
    }

    /* access modifiers changed from: private */
    public void b(boolean z2) {
        if (this.M && z2) {
            j();
        }
        this.f1178a.setNormalizedMinValue((double) this.D);
        be.a().a(this.D);
        this.F = this.f1178a.getSelectedMinValue().intValue();
        this.h.setStartPos(this.F);
        h();
    }

    public boolean a() {
        com.shoujiduoduo.base.a.a.b("makering", "doBackPressed in " + this.U.toString());
        switch (this.U) {
            case edit:
                l();
                this.E = 1.0f;
                this.D = 0.0f;
                this.F = 0;
                this.G = this.h.getWidth();
                a(true);
                b(true);
                return true;
            case song_edit:
                j();
                return false;
            default:
                return false;
        }
    }

    public void b() {
        this.E = 1.0f;
        this.D = 0.0f;
        this.F = 0;
        this.G = this.f1178a.getAbsoluteMaxValue().intValue();
        this.b.setDuration(0);
    }

    public boolean c() {
        return this.U != d.record;
    }

    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: private */
    public void l() {
        this.e.setVisibility(0);
        this.f.setVisibility(4);
        this.x.setVisibility(4);
        this.g.setVisibility(0);
        this.w.setVisibility(4);
        this.y.setVisibility(4);
        this.h.setVisibility(0);
        this.h.setDrawState(WaveformView.a.rec_pause);
        m();
        this.c.setText((int) R.string.delete);
        this.d.setText((int) R.string.ring_save);
        this.U = d.rec_pause;
        this.W.b(getResources().getString(R.string.pause));
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: private */
    public void b(d dVar) {
        this.U = dVar;
        switch (dVar) {
            case edit:
                this.f.setVisibility(0);
                this.e.setVisibility(4);
                this.g.setVisibility(0);
                this.w.setVisibility(0);
                this.x.setVisibility(4);
                this.y.setVisibility(4);
                this.h.setVisibility(0);
                this.h.setDrawState(WaveformView.a.edit_pause);
                this.c.setText((int) R.string.restore);
                this.d.setText((int) R.string.ring_save);
                this.W.b(getResources().getString(R.string.edit));
                break;
            case song_edit:
                break;
            case record:
                this.e.setVisibility(0);
                this.f.setVisibility(4);
                this.k.setVisibility(4);
                this.m.setVisibility(4);
                this.n.setVisibility(4);
                this.o.setVisibility(4);
                this.l.setVisibility(4);
                this.w.setVisibility(4);
                this.x.setVisibility(0);
                this.g.setVisibility(4);
                this.y.setVisibility(0);
                this.h.setVisibility(4);
                this.W.b(getResources().getString(R.string.record));
                return;
            case recording:
                this.e.setVisibility(0);
                this.f.setVisibility(4);
                this.k.setVisibility(4);
                this.m.setVisibility(4);
                this.n.setVisibility(4);
                this.o.setVisibility(4);
                this.l.setVisibility(4);
                this.w.setVisibility(4);
                this.x.setVisibility(4);
                this.g.setVisibility(4);
                this.y.setVisibility(4);
                this.h.setVisibility(0);
                this.h.setDrawState(WaveformView.a.recording);
                this.W.b(getResources().getString(R.string.recording));
                return;
            case rec_pause:
                this.e.setVisibility(0);
                this.f.setVisibility(4);
                this.x.setVisibility(4);
                this.g.setVisibility(0);
                this.k.setVisibility(0);
                this.m.setVisibility(0);
                this.n.setVisibility(0);
                this.l.setVisibility(0);
                this.o.setVisibility(0);
                this.w.setVisibility(4);
                this.y.setVisibility(4);
                this.h.setVisibility(0);
                this.h.setDrawState(WaveformView.a.rec_pause);
                this.c.setText((int) R.string.delete);
                this.d.setText((int) R.string.ring_save);
                this.W.b(getResources().getString(R.string.pause));
                return;
            default:
                return;
        }
        this.f.setVisibility(0);
        this.e.setVisibility(4);
        this.g.setVisibility(0);
        this.w.setVisibility(0);
        this.x.setVisibility(4);
        this.y.setVisibility(4);
        this.b.setVisibility(0);
        this.h.setVisibility(0);
        this.h.setDrawState(WaveformView.a.edit_pause);
        this.c.setText((int) R.string.back);
        this.d.setText((int) R.string.ring_save);
        this.W.b(getResources().getString(R.string.edit));
    }

    public void a(m mVar, m.a aVar) {
        com.shoujiduoduo.base.a.a.a("MakeRingRecordFragment", "record state changed:" + aVar);
        Message message = new Message();
        message.what = 58;
        message.obj = mVar;
        this.V.sendMessage(message);
    }

    public void a(m mVar, long j2) {
        com.shoujiduoduo.base.a.a.a("MakeRingRecordFragment", "timelimit:" + this.X + "record duration:" + j2);
        if (this.X != -1 && j2 > ((long) (this.X - 1))) {
            this.V.sendMessage(this.V.obtainMessage(59));
        }
    }

    public void a(com.shoujiduoduo.player.c cVar) {
    }

    public void b(com.shoujiduoduo.player.c cVar) {
        Message message = new Message();
        message.what = 56;
        message.obj = cVar;
        this.V.sendMessage(message);
    }

    public boolean a(com.shoujiduoduo.player.c cVar, int i2, int i3) {
        this.V.sendEmptyMessage(55);
        return false;
    }

    public void a(float f2) {
        this.L = f2;
        this.K = System.currentTimeMillis();
    }

    public void b(float f2) {
    }

    public void d() {
        if (System.currentTimeMillis() - this.K >= 300) {
            return;
        }
        if (this.M) {
            int width = (int) ((this.L / ((float) this.h.getWidth())) * ((float) be.a().c()));
            if (width < this.I || width >= this.J) {
                j();
            } else {
                this.S.seekTo(width - this.H);
            }
        } else {
            a((int) this.L);
        }
    }

    public void e() {
        if (this.O || this.M) {
            m();
        }
    }

    /* access modifiers changed from: private */
    public void m() {
        if (this.M) {
            int currentPosition = this.S.getCurrentPosition() + this.H;
            this.h.setPlayback((int) ((((float) currentPosition) / ((float) be.a().c())) * ((float) this.h.getWidth())));
            if (currentPosition >= this.J) {
                j();
            }
        }
        this.h.invalidate();
    }

    public void f() {
        com.shoujiduoduo.base.a.a.d("MakeRingRecordFragment", "pauseAnimationDraw in");
        this.h.setDrawState(WaveformView.a.animation);
        long j2 = be.a().j();
        if (j2 > 11025) {
            int k2 = (((int) j2) - 11025) / be.a().k();
            int i2 = k2;
            while (true) {
                if (i2 < 0) {
                    break;
                }
                this.h.a((float) this.h.getWidth(), i2);
                if (((float) i2) < ((float) k2) / 5.0f && i2 > 0) {
                    this.h.a((float) this.h.getWidth(), 0);
                    m();
                    break;
                }
                m();
                i2 = (int) (((float) i2) - (((float) k2) / 5.0f));
            }
        } else {
            float width = ((float) this.h.getWidth()) * (((float) j2) / 11025.0f);
            float width2 = (((float) this.h.getWidth()) - width) / 5.0f;
            for (int i3 = 0; i3 <= 5; i3++) {
                this.h.a((((float) i3) * width2) + width, 0);
                m();
            }
        }
        this.h.setDrawState(WaveformView.a.rec_pause);
        com.shoujiduoduo.base.a.a.d("MakeRingRecordFragment", "pauseAnimationDraw out");
    }

    public void g() {
        com.shoujiduoduo.base.a.a.d("MakeRingRecordFragment", "recordAnimationDraw in");
        this.h.setDrawState(WaveformView.a.animation);
        long j2 = be.a().j();
        if (j2 < 11025) {
            float width = (((float) this.h.getWidth()) - ((((float) j2) / 11025.0f) * ((float) this.h.getWidth()))) / 5.0f;
            for (int i2 = 0; i2 < 5; i2++) {
                this.h.a(((float) this.h.getWidth()) - (((float) i2) * width), 0);
                m();
            }
        } else {
            int k2 = (int) ((j2 - 11025) / ((long) be.a().k()));
            for (int i3 = 0; i3 <= k2; i3 = (int) (((float) i3) + (((float) k2) / 5.0f))) {
                this.h.a((float) this.h.getWidth(), i3);
                m();
            }
        }
        this.h.setDrawState(WaveformView.a.recording);
        com.shoujiduoduo.base.a.a.d("MakeRingRecordFragment", "recordAnimationDraw out");
    }
}
