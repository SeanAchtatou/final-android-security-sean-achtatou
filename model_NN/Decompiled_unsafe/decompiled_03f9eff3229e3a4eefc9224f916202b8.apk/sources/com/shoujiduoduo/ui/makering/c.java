package com.shoujiduoduo.ui.makering;

import android.content.DialogInterface;
import com.shoujiduoduo.player.a;
import com.shoujiduoduo.ui.makering.MakeRingActivity;
import com.shoujiduoduo.util.be;

/* compiled from: MakeRingActivity */
class c implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ MakeRingActivity f1190a;

    c(MakeRingActivity makeRingActivity) {
        this.f1190a = makeRingActivity;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        be.a().h();
        a.b().j();
        this.f1190a.f.b();
        if (this.f1190a.c.equals(MakeRingActivity.a.song_edit)) {
            MakeRingActivity.a unused = this.f1190a.c = MakeRingActivity.a.choose_song;
            this.f1190a.getSupportFragmentManager().popBackStack();
            return;
        }
        this.f1190a.a(MakeRingActivity.a.choose_type);
    }
}
