package com.shoujiduoduo.ui.adwall;

import android.content.Intent;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.util.widget.WebViewActivity;

/* compiled from: GameWallFragment */
class f extends WebViewClient {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GameWallFragment f896a;

    f(GameWallFragment gameWallFragment) {
        this.f896a = gameWallFragment;
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        a.a("GameWallFragment", "url:" + str);
        Intent intent = new Intent(this.f896a.getActivity(), WebViewActivity.class);
        intent.putExtra("url", str);
        a.a("GameWallFragment", "url:" + str);
        this.f896a.startActivity(intent);
        return true;
    }
}
