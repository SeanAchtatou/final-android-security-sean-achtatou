package com.shoujiduoduo.ui.adwall;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.util.as;
import com.shoujiduoduo.util.av;
import com.shoujiduoduo.util.f;
import com.umeng.analytics.b;
import java.util.ArrayList;

public class WallAdView {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final String f886a = WallAdView.class.getSimpleName();
    /* access modifiers changed from: private */
    public static final String f = ("pref_ad_app_pressed_" + f.p());
    /* access modifiers changed from: private */
    public static final String g = ("pref_ad_business_pressed_" + f.p());
    /* access modifiers changed from: private */
    public static final String h = ("pref_ad_game_pressed_" + f.p());
    /* access modifiers changed from: private */
    public Context b;
    /* access modifiers changed from: private */
    public ImageView c;
    /* access modifiers changed from: private */
    public ImageView d;
    /* access modifiers changed from: private */
    public RadioGroup e;
    /* access modifiers changed from: private */
    public ViewPager i;
    /* access modifiers changed from: private */
    public boolean j;
    private boolean k;
    private AppWallFragment l;
    private EbusinessWallFragment m;
    private GameWallFragment n;
    private RelativeLayout o;
    private TextView p;
    /* access modifiers changed from: private */
    public ArrayList<Fragment> q;
    /* access modifiers changed from: private */
    public a r = a.ebusiness;

    public enum a {
        game,
        ebusiness,
        app
    }

    public WallAdView(Context context) {
        this.b = context;
    }

    public void a() {
        com.shoujiduoduo.base.a.a.a(f886a, "WallAdView init in");
        long currentTimeMillis = System.currentTimeMillis();
        this.o = (RelativeLayout) ((Activity) this.b).findViewById(R.id.wall_top_banner);
        this.e = (RadioGroup) ((Activity) this.b).findViewById(R.id.radiogroup_ad_type);
        this.c = (ImageView) ((Activity) this.b).findViewById(R.id.iv_ad_left_point);
        this.d = (ImageView) ((Activity) this.b).findViewById(R.id.iv_ad_right_point);
        this.p = (TextView) ((Activity) this.b).findViewById(R.id.taobao_tips);
        ((RadioButton) this.e.getChildAt(0)).setText(g());
        ((RadioButton) this.e.getChildAt(2)).setText(h());
        if (k() || i()) {
            if (i()) {
                this.j = true;
            } else {
                this.e.removeViewAt(1);
                ((RadioButton) this.e.getChildAt(0)).setText((int) R.string.ebusiness_wall_title);
                this.j = false;
            }
            if (k()) {
                this.k = true;
            } else {
                this.e.removeViewAt(1);
                ((RadioButton) this.e.getChildAt(1)).setText((int) R.string.ebusiness_wall_title);
                this.k = false;
            }
        } else {
            this.p.setVisibility(0);
            this.e.setVisibility(8);
        }
        this.q = new ArrayList<>();
        if (this.j) {
            this.l = new AppWallFragment();
            String j2 = j();
            Bundle bundle = new Bundle();
            bundle.putString("url", j2);
            this.l.setArguments(bundle);
            this.q.add(this.l);
        }
        this.m = new EbusinessWallFragment();
        this.q.add(this.m);
        if (this.k) {
            this.n = new GameWallFragment();
            Bundle bundle2 = new Bundle();
            bundle2.putString("url", l());
            this.n.setArguments(bundle2);
            this.q.add(this.n);
        }
        this.i = (ViewPager) ((Activity) this.b).findViewById(R.id.vAdPager);
        MyViewPagerAdapter myViewPagerAdapter = new MyViewPagerAdapter(((FragmentActivity) this.b).getSupportFragmentManager());
        this.i.setOffscreenPageLimit(2);
        this.i.setAdapter(myViewPagerAdapter);
        this.i.setOnPageChangeListener(new MyOnPageChangeListener());
        String c2 = b.c(RingDDApp.c(), "wall_ad_type2");
        if (av.c(c2) || "null".equals(c2)) {
            c2 = "ebusiness";
        }
        com.shoujiduoduo.base.a.a.a(f886a, "default wall:" + c2);
        if (c2.equalsIgnoreCase("ebusiness")) {
            this.r = a.ebusiness;
            if (as.a(this.b, h, 0) == 0 && this.k) {
                this.d.setVisibility(0);
            }
            if (this.j) {
                this.i.setCurrentItem(1, true);
                if (as.a(this.b, f, 0) == 0) {
                    this.c.setVisibility(0);
                }
            } else {
                this.i.setCurrentItem(0, true);
                this.e.getChildAt(0).performClick();
            }
        } else if (c2.equalsIgnoreCase("game")) {
            if (this.k) {
                this.r = a.game;
                if (this.j) {
                    if (as.a(this.b, h, 0) == 0) {
                        this.d.setVisibility(0);
                    }
                    this.i.setCurrentItem(2, true);
                } else {
                    this.i.setCurrentItem(1, true);
                }
            } else {
                this.r = a.ebusiness;
                if (this.j) {
                    if (as.a(this.b, h, 0) == 0) {
                        this.d.setVisibility(0);
                    }
                    this.i.setCurrentItem(1, true);
                } else {
                    this.i.setCurrentItem(0, true);
                }
            }
        } else if (c2.equalsIgnoreCase("app")) {
            if (this.j) {
                this.r = a.app;
            } else {
                this.r = a.ebusiness;
            }
            this.i.setCurrentItem(0, true);
            this.e.getChildAt(0).performClick();
            if (as.a(this.b, f, 0) == 0) {
                this.c.setVisibility(0);
            }
        } else {
            com.shoujiduoduo.base.a.a.a(f886a, "not support type");
        }
        this.e.setOnCheckedChangeListener(new t(this));
        com.shoujiduoduo.base.a.a.a(f886a, "WallAdView init out, cost:" + (System.currentTimeMillis() - currentTimeMillis));
    }

    private String g() {
        String c2 = b.c(this.b, "app_wall_title2");
        return !av.c(c2) ? c2 : this.b.getResources().getString(R.string.app_wall_title);
    }

    private String h() {
        String c2 = b.c(this.b, "game_wall_title");
        return !TextUtils.isEmpty(c2) ? c2 : this.b.getResources().getString(R.string.game_wall_title);
    }

    private boolean i() {
        if (!"false".equals(b.c(RingDDApp.c(), "left_wall_switch_2")) && !av.b(b.c(this.b, "left_wall_url"))) {
            return true;
        }
        return false;
    }

    private String j() {
        return b.c(this.b, "left_wall_url");
    }

    private boolean k() {
        if (!"false".equals(b.c(RingDDApp.c(), "right_wall_switch")) && !av.b(b.c(this.b, "right_wall_url"))) {
            return true;
        }
        return false;
    }

    private String l() {
        return b.c(this.b, "right_wall_url");
    }

    public void b() {
    }

    public class MyViewPagerAdapter extends FragmentPagerAdapter {
        public MyViewPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        public Fragment getItem(int i) {
            return (Fragment) WallAdView.this.q.get(i);
        }

        public int getCount() {
            return WallAdView.this.q.size();
        }

        public CharSequence getPageTitle(int i) {
            return "";
        }
    }

    public boolean a(int i2, KeyEvent keyEvent) {
        switch (this.r) {
            case game:
                return this.n.a(i2, keyEvent);
            case app:
                return this.l.a(i2, keyEvent);
            case ebusiness:
                return this.m.a(i2, keyEvent);
            default:
                return false;
        }
    }

    public class MyOnPageChangeListener implements ViewPager.OnPageChangeListener {
        public MyOnPageChangeListener() {
        }

        public void onPageScrollStateChanged(int i) {
        }

        public void onPageScrolled(int i, float f, int i2) {
        }

        public void onPageSelected(int i) {
            com.shoujiduoduo.base.a.a.a(WallAdView.f886a, "page selected: " + i);
            WallAdView.this.e.getChildAt(i).performClick();
            if (WallAdView.this.e.isShown()) {
                switch (i) {
                    case 0:
                        if (WallAdView.this.j) {
                            a unused = WallAdView.this.r = a.app;
                            if (WallAdView.this.e.isShown()) {
                                com.shoujiduoduo.base.a.a.a("ad", "log game");
                                b.b(WallAdView.this.b, "USER_SEE_APPWALL");
                                return;
                            }
                            return;
                        }
                        a unused2 = WallAdView.this.r = a.ebusiness;
                        if (WallAdView.this.e.isShown()) {
                            com.shoujiduoduo.base.a.a.a("ad", "log ebusiness");
                            b.b(WallAdView.this.b, "USER_SEE_EBUSINESS_WALL");
                            return;
                        }
                        return;
                    case 1:
                        if (WallAdView.this.j) {
                            a unused3 = WallAdView.this.r = a.ebusiness;
                            if (WallAdView.this.e.isShown()) {
                                com.shoujiduoduo.base.a.a.a("ad", "log ebusiness");
                                b.b(WallAdView.this.b, "USER_SEE_EBUSINESS_WALL");
                                return;
                            }
                            return;
                        }
                        a unused4 = WallAdView.this.r = a.game;
                        if (WallAdView.this.e.isShown()) {
                            com.shoujiduoduo.base.a.a.a("ad", "log app");
                            b.b(WallAdView.this.b, "USER_SEE_GAME");
                            return;
                        }
                        return;
                    case 2:
                        a unused5 = WallAdView.this.r = a.game;
                        if (WallAdView.this.e.isShown()) {
                            com.shoujiduoduo.base.a.a.a("ad", "log app");
                            b.b(WallAdView.this.b, "USER_SEE_GAME");
                            return;
                        }
                        return;
                    default:
                        return;
                }
            }
        }
    }
}
