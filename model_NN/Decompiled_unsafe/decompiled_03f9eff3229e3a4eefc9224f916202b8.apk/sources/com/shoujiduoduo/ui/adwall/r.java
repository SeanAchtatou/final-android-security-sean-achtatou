package com.shoujiduoduo.ui.adwall;

/* compiled from: MoreOptionsScene */
class r extends Thread {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f913a;
    final /* synthetic */ g b;

    r(g gVar, int i) {
        this.b = gVar;
        this.f913a = i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.util.as.a(android.content.Context, java.lang.String, int):int
     arg types: [android.app.Activity, java.lang.String, int]
     candidates:
      com.shoujiduoduo.util.as.a(android.content.Context, java.lang.String, long):long
      com.shoujiduoduo.util.as.a(android.content.Context, java.lang.String, java.lang.String):java.lang.String
      com.shoujiduoduo.util.as.a(android.content.Context, java.lang.String, int):int */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x00fb  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0150  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r11 = this;
            r10 = 204(0xcc, float:2.86E-43)
            r7 = -1
            r1 = 0
            com.shoujiduoduo.ui.adwall.g r0 = r11.b
            com.shoujiduoduo.b.a.f r0 = r0.q
            int r2 = r11.f913a
            com.shoujiduoduo.base.bean.d r3 = r0.a(r2)
            java.lang.String r0 = r3.d
            java.lang.String r2 = r3.d
            java.lang.String r4 = "/"
            int r2 = r2.lastIndexOf(r4)
            int r2 = r2 + 1
            java.lang.String r0 = r0.substring(r2)
            java.lang.String r0 = r0.toLowerCase()
            java.lang.String r2 = ".apk"
            boolean r2 = r0.endsWith(r2)
            if (r2 != 0) goto L_0x003f
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r2 = ".apk"
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
        L_0x003f:
            java.lang.String r2 = com.shoujiduoduo.ui.adwall.g.b
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "download soft: cachePath = "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r4 = r4.append(r0)
            java.lang.String r4 = r4.toString()
            com.shoujiduoduo.base.a.a.a(r2, r4)
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r4 = com.shoujiduoduo.util.s.b()
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r4 = r0.toString()
            com.shoujiduoduo.ui.adwall.g r0 = r11.b
            android.app.Activity r0 = r0.e
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r5 = ":total"
            java.lang.StringBuilder r2 = r2.append(r5)
            java.lang.String r2 = r2.toString()
            int r5 = com.shoujiduoduo.util.as.a(r0, r2, r7)
            com.shoujiduoduo.ui.adwall.g r0 = r11.b
            android.app.Activity r0 = r0.e
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r6 = ":current"
            java.lang.StringBuilder r2 = r2.append(r6)
            java.lang.String r2 = r2.toString()
            int r0 = com.shoujiduoduo.util.as.a(r0, r2, r7)
            java.lang.String r2 = com.shoujiduoduo.ui.adwall.g.b
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "download soft: totalLength = "
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.StringBuilder r6 = r6.append(r5)
            java.lang.String r7 = "; currentLength = "
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.StringBuilder r6 = r6.append(r0)
            java.lang.String r6 = r6.toString()
            com.shoujiduoduo.base.a.a.a(r2, r6)
            r2 = 1
            if (r5 <= 0) goto L_0x017d
            if (r0 < 0) goto L_0x017d
            if (r0 > r5) goto L_0x017d
            java.io.File r6 = new java.io.File
            r6.<init>(r4)
            boolean r7 = r6.exists()
            if (r7 == 0) goto L_0x017d
            boolean r7 = r6.canWrite()
            if (r7 == 0) goto L_0x017d
            boolean r7 = r6.isFile()
            if (r7 == 0) goto L_0x017d
            long r6 = r6.length()
            long r8 = (long) r0
            int r6 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r6 != 0) goto L_0x017d
            r6 = 20000(0x4e20, float:2.8026E-41)
            if (r5 > r6) goto L_0x0132
            r0 = r1
            r1 = r2
        L_0x00f9:
            if (r1 == 0) goto L_0x0150
            com.shoujiduoduo.ui.adwall.g r1 = r11.b
            java.lang.String r2 = r3.d
            boolean r0 = r1.a(r2, r4, r0)
            if (r0 == 0) goto L_0x0138
            com.shoujiduoduo.ui.adwall.g r0 = r11.b
            android.os.Handler r0 = r0.v
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = r3.b
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = "\\"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r4)
            java.lang.String r1 = r1.toString()
            android.os.Message r0 = r0.obtainMessage(r10, r1)
            com.shoujiduoduo.ui.adwall.g r1 = r11.b
            android.os.Handler r1 = r1.v
            r1.sendMessage(r0)
        L_0x0131:
            return
        L_0x0132:
            if (r0 != r5) goto L_0x0136
            r0 = r1
            goto L_0x00f9
        L_0x0136:
            r1 = r2
            goto L_0x00f9
        L_0x0138:
            com.shoujiduoduo.ui.adwall.g r0 = r11.b
            android.os.Handler r0 = r0.v
            r1 = 205(0xcd, float:2.87E-43)
            java.lang.String r2 = r3.b
            android.os.Message r0 = r0.obtainMessage(r1, r2)
            com.shoujiduoduo.ui.adwall.g r1 = r11.b
            android.os.Handler r1 = r1.v
            r1.sendMessage(r0)
            goto L_0x0131
        L_0x0150:
            com.shoujiduoduo.ui.adwall.g r0 = r11.b
            android.os.Handler r0 = r0.v
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = r3.b
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = "\\"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r4)
            java.lang.String r1 = r1.toString()
            android.os.Message r0 = r0.obtainMessage(r10, r1)
            com.shoujiduoduo.ui.adwall.g r1 = r11.b
            android.os.Handler r1 = r1.v
            r1.sendMessage(r0)
            goto L_0x0131
        L_0x017d:
            r0 = r1
            r1 = r2
            goto L_0x00f9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.ui.adwall.r.run():void");
    }
}
