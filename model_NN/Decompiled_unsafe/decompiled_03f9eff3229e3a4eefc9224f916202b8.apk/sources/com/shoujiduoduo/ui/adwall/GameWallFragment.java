package com.shoujiduoduo.ui.adwall;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.media.TransportMediator;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.util.av;

public class GameWallFragment extends Fragment {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public WebView f885a;
    private View.OnKeyListener b = new d(this);

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        String str;
        a.a("GameWallFragment", "GameWallFragment onCreateView in");
        long currentTimeMillis = System.currentTimeMillis();
        View inflate = layoutInflater.inflate((int) R.layout.fragment_game_wall, viewGroup, false);
        Bundle arguments = getArguments();
        if (arguments != null) {
            String string = arguments.getString("url");
            if (av.c(string)) {
                str = "http://entry.baidu.com/rp/hot?type=hotpage&version=mobile_embed_top&mobile=true&di=u2550827&tus=u2550825%7Cu2550824&lu=u2550029&url=http%3A%2F%2Fwww.shoujiduoduo.com";
            } else {
                str = string;
            }
            this.f885a = (WebView) inflate.findViewById(R.id.game_ad_webview);
            WebSettings settings = this.f885a.getSettings();
            settings.setJavaScriptEnabled(true);
            settings.setUseWideViewPort(true);
            settings.setLoadWithOverviewMode(true);
            settings.setBuiltInZoomControls(false);
            settings.setDomStorageEnabled(true);
            settings.setSupportZoom(false);
            a(this.f885a);
            this.f885a.loadUrl(str);
        }
        a.a("GameWallFragment", "GameWallFragment onCreateView out, cost:" + (System.currentTimeMillis() - currentTimeMillis));
        return inflate;
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public void onPause() {
        super.onPause();
    }

    public void onResume() {
        a.a("GameWallFragment", "onResume");
        super.onResume();
        getView().setFocusable(true);
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(this.b);
    }

    public boolean a(int i, KeyEvent keyEvent) {
        if (this.f885a == null || !this.f885a.canGoBack()) {
            return false;
        }
        this.f885a.goBack();
        return true;
    }

    private void a(WebView webView) {
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setScrollBarStyle(0);
        webView.requestFocus(TransportMediator.KEYCODE_MEDIA_RECORD);
        webView.setOnTouchListener(new e(this));
        webView.setWebViewClient(new f(this));
    }
}
