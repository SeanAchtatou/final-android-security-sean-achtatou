package com.shoujiduoduo.ui.adwall;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.media.TransportMediator;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.RelativeLayout;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.util.av;
import com.umeng.analytics.b;

public class AppWallFragment extends Fragment {

    /* renamed from: a  reason: collision with root package name */
    private WebView f882a;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        try {
            RelativeLayout relativeLayout = (RelativeLayout) layoutInflater.inflate((int) R.layout.fragment_app_wall, viewGroup, false);
            Bundle arguments = getArguments();
            if (arguments == null) {
                return relativeLayout;
            }
            String string = arguments.getString("url");
            a.a("AppWallFragment", "url:" + string);
            if (av.c(string) || "null".equals(string)) {
                return relativeLayout;
            }
            this.f882a = (WebView) relativeLayout.findViewById(R.id.ad_webview);
            WebSettings settings = this.f882a.getSettings();
            settings.setJavaScriptEnabled(true);
            settings.setUseWideViewPort(true);
            settings.setLoadWithOverviewMode(true);
            settings.setBuiltInZoomControls(false);
            settings.setDomStorageEnabled(true);
            settings.setSupportZoom(false);
            a(this.f882a);
            this.f882a.loadUrl(string);
            return relativeLayout;
        } catch (Throwable th) {
            a.c("AppWallFragment", "baidu_app_wall_crash1");
            b.b(RingDDApp.c(), "baidu_app_wall_crash");
            th.printStackTrace();
            return super.onCreateView(layoutInflater, viewGroup, bundle);
        }
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public void onPause() {
        super.onPause();
    }

    public void onResume() {
        a.a("AppWallFragment", "onResume");
        super.onResume();
    }

    public boolean a(int i, KeyEvent keyEvent) {
        if (this.f882a == null || !this.f882a.canGoBack()) {
            return false;
        }
        this.f882a.goBack();
        return true;
    }

    private void a(WebView webView) {
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setScrollBarStyle(0);
        webView.requestFocus(TransportMediator.KEYCODE_MEDIA_RECORD);
        webView.setOnTouchListener(new a(this));
        webView.setWebViewClient(new b(this));
    }
}
