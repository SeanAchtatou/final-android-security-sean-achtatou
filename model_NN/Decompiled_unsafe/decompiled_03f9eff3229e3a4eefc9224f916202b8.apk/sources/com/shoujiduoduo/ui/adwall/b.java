package com.shoujiduoduo.ui.adwall;

import android.content.Intent;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.util.widget.WebViewActivity;

/* compiled from: AppWallFragment */
class b extends WebViewClient {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppWallFragment f892a;

    b(AppWallFragment appWallFragment) {
        this.f892a = appWallFragment;
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        a.a("AppWallFragment", "url:" + str);
        Intent intent = new Intent(this.f892a.getActivity(), WebViewActivity.class);
        intent.putExtra("url", str);
        a.a("AppWallFragment", "url:" + str);
        this.f892a.startActivity(intent);
        return true;
    }
}
