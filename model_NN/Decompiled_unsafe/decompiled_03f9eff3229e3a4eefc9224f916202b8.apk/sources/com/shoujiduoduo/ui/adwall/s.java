package com.shoujiduoduo.ui.adwall;

import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import com.shoujiduoduo.ui.adwall.g;
import com.shoujiduoduo.ui.utils.d;

/* compiled from: MoreOptionsScene */
class s implements d.a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ g.a f914a;

    s(g.a aVar) {
        this.f914a = aVar;
    }

    public void a(Drawable drawable, String str) {
        ImageView imageView = (ImageView) g.this.r.findViewWithTag(str);
        if (imageView != null && drawable != null) {
            imageView.setImageDrawable(drawable);
        }
    }
}
