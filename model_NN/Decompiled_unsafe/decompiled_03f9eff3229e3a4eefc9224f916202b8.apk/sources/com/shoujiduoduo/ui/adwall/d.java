package com.shoujiduoduo.ui.adwall;

import android.view.KeyEvent;
import android.view.View;

/* compiled from: GameWallFragment */
class d implements View.OnKeyListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GameWallFragment f894a;

    d(GameWallFragment gameWallFragment) {
        this.f894a = gameWallFragment;
    }

    public boolean onKey(View view, int i, KeyEvent keyEvent) {
        if (i == 4 && keyEvent.getAction() == 0 && this.f894a.f885a.canGoBack()) {
            return true;
        }
        return false;
    }
}
