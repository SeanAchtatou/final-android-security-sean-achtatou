package com.shoujiduoduo.ui.adwall;

import android.view.MotionEvent;
import android.view.View;

/* compiled from: GameWallFragment */
class e implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GameWallFragment f895a;

    e(GameWallFragment gameWallFragment) {
        this.f895a = gameWallFragment;
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
            case 1:
                if (view.hasFocus()) {
                    return false;
                }
                view.requestFocus();
                return false;
            default:
                return false;
        }
    }
}
