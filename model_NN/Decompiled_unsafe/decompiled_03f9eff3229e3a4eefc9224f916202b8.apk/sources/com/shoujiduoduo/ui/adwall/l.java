package com.shoujiduoduo.ui.adwall;

import android.app.AlertDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.widget.Toast;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.ringtone.R;
import java.io.File;

/* compiled from: MoreOptionsScene */
class l extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ g f907a;

    l(g gVar) {
        this.f907a = gVar;
    }

    public void handleMessage(Message message) {
        switch (message.what) {
            case 200:
                a.a(g.b, "MESSAGE_OK_CLEAR_CACHE Received.");
                this.f907a.g();
                return;
            case 201:
                this.f907a.p.cancel();
                Toast.makeText(this.f907a.e, (int) R.string.submit_suc, 0).show();
                return;
            case 202:
                this.f907a.p.cancel();
                new AlertDialog.Builder(this.f907a.e).setTitle((int) R.string.hint).setMessage((int) R.string.submit_error).setIcon(17301543).setPositiveButton((int) R.string.ok, new n(this)).setNegativeButton((int) R.string.cancel, new m(this)).show();
                return;
            case 203:
                this.f907a.f();
                return;
            case 204:
                String str = (String) message.obj;
                int lastIndexOf = str.lastIndexOf(92);
                if (lastIndexOf >= 0) {
                    str.substring(0, lastIndexOf);
                    String substring = str.substring(lastIndexOf + 1);
                    a.a(g.b, "install soft: path = " + substring);
                    Uri fromFile = Uri.fromFile(new File(substring));
                    Intent intent = new Intent("android.intent.action.VIEW");
                    intent.setDataAndType(fromFile, "application/vnd.android.package-archive");
                    this.f907a.e.startActivity(intent);
                    return;
                }
                return;
            case 205:
                String string = this.f907a.e.getResources().getString(R.string.download_error);
                Toast.makeText(this.f907a.e, ((String) message.obj) + string, 0).show();
                return;
            case 206:
                a.a(g.b, "MESSAGE_FINISH_CLEAR_CACHE");
                if (this.f907a.p != null) {
                    this.f907a.p.cancel();
                }
                Toast.makeText(this.f907a.e, (int) R.string.clean_cache_suc, 0).show();
                return;
            default:
                return;
        }
    }
}
