package com.shoujiduoduo.ui.adwall;

import android.view.View;
import com.shoujiduoduo.ringtone.activity.RingToneDuoduoActivity;
import com.shoujiduoduo.ui.adwall.g;

/* compiled from: MoreOptionsScene */
class h implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ g f903a;

    h(g gVar) {
        this.f903a = gVar;
    }

    public void onClick(View view) {
        if (this.f903a.q.b() > 0) {
            this.f903a.a(g.d.PANEL_DUODUO_FAMILY);
            View unused = this.f903a.m = this.f903a.j;
        } else if (this.f903a.q.b() == 0) {
            this.f903a.a(g.d.PANEL_LOADING_DUODUO_FAMILY);
            View unused2 = this.f903a.m = this.f903a.k;
            this.f903a.q.a();
        }
        ((RingToneDuoduoActivity) this.f903a.e).a(RingToneDuoduoActivity.a.HEADER_DUODUO_FAMILY);
    }
}
