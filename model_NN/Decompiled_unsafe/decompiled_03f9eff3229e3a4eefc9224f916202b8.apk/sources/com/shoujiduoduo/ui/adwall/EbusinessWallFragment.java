package com.shoujiduoduo.ui.adwall;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import com.alimama.mobile.sdk.MmuSDK;
import com.alimama.mobile.sdk.config.ContainerProperties;
import com.alimama.mobile.sdk.config.MmuSDKFactory;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.util.f;
import com.umeng.analytics.b;

public class EbusinessWallFragment extends Fragment {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public RelativeLayout f883a;
    private boolean b;
    private String c = ("http://w.m.taobao.com/api/wap?slot_id=48163&resource_type=itemlist&mc=" + f.c() + "&device_id=" + f.h());
    private a d = new a(this, null);

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        com.shoujiduoduo.base.a.a.a("EbusinessWallFragment", "EbusinessWallFragment onCreateView in");
        long currentTimeMillis = System.currentTimeMillis();
        View inflate = layoutInflater.inflate((int) R.layout.fragment_ebusiness_wall, viewGroup, false);
        this.f883a = (RelativeLayout) inflate.findViewById(R.id.taoBaoView);
        this.d.postDelayed(new c(this), 100);
        String c2 = b.c(getActivity(), "taobao_url");
        if (!TextUtils.isEmpty(c2)) {
            this.c = c2 + "&mc=" + f.c() + "&imei=" + f.h();
            com.shoujiduoduo.base.a.a.a("EbusinessWallFragment", "url:" + this.c);
        }
        com.shoujiduoduo.base.a.a.a("EbusinessWallFragment", "EbusinessWallFragment onCreateView out, cost:" + (System.currentTimeMillis() - currentTimeMillis));
        return inflate;
    }

    private class a extends Handler {
        private a() {
        }

        /* synthetic */ a(EbusinessWallFragment ebusinessWallFragment, c cVar) {
            this();
        }

        public void handleMessage(Message message) {
        }
    }

    private void a(ViewGroup viewGroup, String str) {
        MmuSDK mmuSDK = MmuSDKFactory.getMmuSDK();
        mmuSDK.init(RingDDApp.b());
        mmuSDK.accountServiceInit(RingDDApp.b());
        mmuSDK.attach(new ContainerProperties(getActivity(), str, viewGroup));
    }

    public void a() {
        if (!this.b) {
            this.b = true;
            com.shoujiduoduo.base.a.a.a("EbusinessWallFragment", "taobao sdk view begin load");
            a(this.f883a, "60850");
            com.shoujiduoduo.base.a.a.a("EbusinessWallFragment", "taobao sdk view finish load");
        }
    }

    public boolean a(int i, KeyEvent keyEvent) {
        return false;
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public void onPause() {
        super.onPause();
    }
}
