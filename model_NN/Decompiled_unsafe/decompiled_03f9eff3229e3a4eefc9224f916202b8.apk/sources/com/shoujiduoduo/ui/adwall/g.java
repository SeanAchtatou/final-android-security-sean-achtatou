package com.shoujiduoduo.ui.adwall;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.shoujiduoduo.a.c.f;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.activity.RingToneDuoduoActivity;
import com.shoujiduoduo.util.as;
import com.shoujiduoduo.util.q;
import com.umeng.fb.k;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import org.apache.mina.proxy.handlers.http.ntlm.NTLMConstants;

/* compiled from: MoreOptionsScene */
public class g implements AdapterView.OnItemClickListener, f {
    /* access modifiers changed from: private */
    public static final String b = g.class.getSimpleName();

    /* renamed from: a  reason: collision with root package name */
    c f897a;
    /* access modifiers changed from: private */
    public int c;
    private ListView d;
    /* access modifiers changed from: private */
    public Activity e;
    private BaseAdapter f;
    private LinearLayout g;
    private LinearLayout h;
    private RelativeLayout i;
    /* access modifiers changed from: private */
    public LinearLayout j;
    /* access modifiers changed from: private */
    public RelativeLayout k;
    private RelativeLayout l;
    /* access modifiers changed from: private */
    public View m;
    private ImageButton n;
    private Button o;
    /* access modifiers changed from: private */
    public ProgressDialog p;
    /* access modifiers changed from: private */
    public com.shoujiduoduo.b.a.f q;
    /* access modifiers changed from: private */
    public ListView r;
    private BaseAdapter s;
    private NotificationManager t;
    private View.OnClickListener u = new k(this);
    /* access modifiers changed from: private */
    public Handler v = new l(this);

    /* compiled from: MoreOptionsScene */
    public abstract class c {
        public abstract void a();

        public abstract void a(String str);
    }

    /* compiled from: MoreOptionsScene */
    private enum d {
        PANEL_ENTRANCE,
        PANEL_DUODUO_FAMILY,
        PANEL_USER_FEEDBACK,
        PANEL_ABOUT_INFO,
        PANEL_LOADING_DUODUO_FAMILY,
        PANEL_DUODUO_FAMILY_LOAD_FAILED
    }

    public g(Activity activity) {
        this.e = activity;
    }

    public void a() {
        String str;
        this.g = (LinearLayout) this.e.findViewById(R.id.more_options_entrance_panel);
        this.h = (LinearLayout) this.e.findViewById(R.id.user_feedback_panel);
        this.i = (RelativeLayout) this.e.findViewById(R.id.about_info_panel);
        if (com.shoujiduoduo.util.f.l().contains("anzhi")) {
            this.c = 4;
        } else {
            this.c = 3;
        }
        String str2 = "";
        try {
            str2 = this.e.getPackageManager().getPackageInfo(this.e.getPackageName(), 0).versionName;
            str = str2.substring(0, str2.lastIndexOf(46));
        } catch (PackageManager.NameNotFoundException e2) {
            PackageManager.NameNotFoundException nameNotFoundException = e2;
            nameNotFoundException.printStackTrace();
            str = str2;
        }
        TextView textView = (TextView) this.e.findViewById(R.id.about_app_name);
        if (textView != null) {
            textView.setText(((Object) this.e.getResources().getText(R.string.app_name)) + " " + str);
        }
        this.j = (LinearLayout) this.e.findViewById(R.id.duoduo_family_panel);
        this.k = (RelativeLayout) this.e.findViewById(R.id.loading_duoduo_family_panel);
        this.l = (RelativeLayout) this.e.findViewById(R.id.loading_duoduo_family_fail_panel);
        this.m = this.g;
        this.d = (ListView) this.e.findViewById(R.id.more_options_list);
        this.f = new b(this, this.e, null);
        this.d.setAdapter((ListAdapter) this.f);
        this.d.setOnItemClickListener(this);
        this.o = (Button) this.e.findViewById(R.id.btn_submit_advice);
        this.o.setOnClickListener(this.u);
        this.q = new com.shoujiduoduo.b.a.f();
        this.q.a(this);
        this.r = (ListView) this.e.findViewById(R.id.duoduo_family_list);
        this.s = new a(this.q, this.e);
        this.r.setAdapter((ListAdapter) this.s);
        this.r.setOnItemClickListener(this);
        this.l.setOnClickListener(new h(this));
        this.t = (NotificationManager) this.e.getSystemService("notification");
        ((TextView) this.e.findViewById(R.id.about_app_intro)).setText(this.e.getResources().getString(R.string.app_intro1) + "\n" + this.e.getResources().getString(R.string.app_intro2) + "\n" + this.e.getResources().getString(R.string.app_intro3) + "\n" + this.e.getResources().getString(R.string.app_intro4) + "\n" + this.e.getResources().getString(R.string.app_intro5));
    }

    public void b() {
    }

    public void c() {
        a(d.PANEL_ENTRANCE);
        if (this.e instanceof RingToneDuoduoActivity) {
            ((RingToneDuoduoActivity) this.e).a(RingToneDuoduoActivity.a.HEADER_MORE_OPTIONS);
        }
        this.m = this.g;
    }

    public void a(ImageButton imageButton) {
        this.n = imageButton;
        this.n.setVisibility(this.m == this.g ? 4 : 0);
        this.n.setOnClickListener(new i(this));
    }

    public RingToneDuoduoActivity.a d() {
        if (this.m == this.g) {
            return RingToneDuoduoActivity.a.HEADER_MORE_OPTIONS;
        }
        if (this.m == this.j || this.m == this.k || this.m == this.l) {
            return RingToneDuoduoActivity.a.HEADER_DUODUO_FAMILY;
        }
        if (this.m == this.h) {
            return RingToneDuoduoActivity.a.HEADER_USER_FEEDBACK;
        }
        if (this.m == this.i) {
            return RingToneDuoduoActivity.a.HEADER_ABOUT_INFO;
        }
        return null;
    }

    /* access modifiers changed from: private */
    public void f() {
        EditText editText = (EditText) this.e.findViewById(R.id.user_feedback_edit);
        String obj = editText.getText().toString();
        String obj2 = ((EditText) this.e.findViewById(R.id.contact_info_edit)).getText().toString();
        if (obj.length() == 0) {
            Toast.makeText(this.e, (int) R.string.user_feedback_hint, 0).show();
            editText.requestFocus();
            return;
        }
        this.p = new ProgressDialog(this.e);
        this.p.setProgressStyle(0);
        this.p.setIndeterminate(true);
        this.p.setTitle("");
        this.p.setMessage(this.e.getResources().getString(R.string.submitting));
        this.p.setCancelable(false);
        this.p.show();
        new j(this, obj, obj2).start();
    }

    /* access modifiers changed from: private */
    public void a(d dVar) {
        switch (dVar) {
            case PANEL_ENTRANCE:
                this.h.setVisibility(4);
                this.j.setVisibility(4);
                this.i.setVisibility(4);
                if (this.n != null) {
                    this.n.setVisibility(4);
                }
                this.k.setVisibility(4);
                this.l.setVisibility(4);
                this.g.setVisibility(0);
                if (this.f897a != null) {
                    this.f897a.a();
                    return;
                }
                return;
            case PANEL_DUODUO_FAMILY:
                this.g.setVisibility(4);
                this.h.setVisibility(4);
                this.i.setVisibility(4);
                this.k.setVisibility(4);
                this.l.setVisibility(4);
                if (this.n != null) {
                    this.n.setVisibility(0);
                }
                this.j.setVisibility(0);
                if (this.f897a != null) {
                    this.f897a.a(this.e.getResources().getString(R.string.duoduo_family_header));
                    return;
                }
                return;
            case PANEL_USER_FEEDBACK:
                this.g.setVisibility(4);
                this.j.setVisibility(4);
                this.i.setVisibility(4);
                this.k.setVisibility(4);
                this.l.setVisibility(4);
                this.h.setVisibility(0);
                if (this.n != null) {
                    this.n.setVisibility(0);
                }
                if (this.f897a != null) {
                    this.f897a.a(this.e.getResources().getString(R.string.user_feedback_header));
                    return;
                }
                return;
            case PANEL_ABOUT_INFO:
                this.g.setVisibility(4);
                this.h.setVisibility(4);
                this.j.setVisibility(4);
                this.k.setVisibility(4);
                this.l.setVisibility(4);
                this.i.setVisibility(0);
                if (this.n != null) {
                    this.n.setVisibility(0);
                }
                if (this.f897a != null) {
                    this.f897a.a(this.e.getResources().getString(R.string.about_info_header));
                    return;
                }
                return;
            case PANEL_LOADING_DUODUO_FAMILY:
                this.g.setVisibility(4);
                this.h.setVisibility(4);
                this.j.setVisibility(4);
                this.l.setVisibility(4);
                this.i.setVisibility(4);
                this.k.setVisibility(0);
                if (this.n != null) {
                    this.n.setVisibility(0);
                }
                if (this.f897a != null) {
                    this.f897a.a(this.e.getResources().getString(R.string.duoduo_family_header));
                    return;
                }
                return;
            case PANEL_DUODUO_FAMILY_LOAD_FAILED:
                this.g.setVisibility(4);
                this.h.setVisibility(4);
                this.j.setVisibility(4);
                this.i.setVisibility(4);
                this.k.setVisibility(4);
                this.l.setVisibility(0);
                if (this.n != null) {
                    this.n.setVisibility(0);
                }
                if (this.f897a != null) {
                    this.f897a.a(this.e.getResources().getString(R.string.duoduo_family_header));
                    return;
                }
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public void g() {
        this.p = new ProgressDialog(this.e);
        this.p.setProgressStyle(0);
        this.p.setIndeterminate(true);
        this.p.setTitle("");
        this.p.setMessage(this.e.getResources().getString(R.string.cleaning_cache));
        this.p.setCancelable(false);
        this.p.show();
        new o(this).start();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.util.as.b(android.content.Context, java.lang.String, int):boolean
     arg types: [android.app.Activity, java.lang.String, int]
     candidates:
      com.shoujiduoduo.util.as.b(android.content.Context, java.lang.String, long):boolean
      com.shoujiduoduo.util.as.b(android.content.Context, java.lang.String, java.lang.String):boolean
      com.shoujiduoduo.util.as.b(android.content.Context, java.lang.String, int):boolean */
    /* access modifiers changed from: private */
    public boolean a(String str, String str2, int i2) {
        com.shoujiduoduo.base.a.a.a(b, "download soft: url = " + str);
        com.shoujiduoduo.base.a.a.a(b, "download soft: path = " + str2);
        com.shoujiduoduo.base.a.a.a(b, "start_pos = " + i2);
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
            com.shoujiduoduo.base.a.a.a(b, "download soft: conn = " + httpURLConnection.toString());
            httpURLConnection.setRequestProperty("RANGE", "bytes=" + i2 + "-");
            httpURLConnection.connect();
            com.shoujiduoduo.base.a.a.a(b, "download soft: connect finished!");
            int responseCode = httpURLConnection.getResponseCode();
            if (responseCode == 200 || responseCode == 206) {
                int contentLength = httpURLConnection.getContentLength();
                if (contentLength <= 0) {
                    com.shoujiduoduo.base.a.a.a(b, "download soft: filesize Error! filesize= " + contentLength);
                    return false;
                }
                com.shoujiduoduo.base.a.a.a(b, "download soft: filesize = " + contentLength);
                as.b((Context) this.e, str2 + ":total", contentLength);
                InputStream inputStream = httpURLConnection.getInputStream();
                RandomAccessFile randomAccessFile = new RandomAccessFile(str2, "rw");
                randomAccessFile.seek((long) i2);
                byte[] bArr = new byte[1024];
                while (true) {
                    int read = inputStream.read(bArr, 0, 1024);
                    if (read > 0) {
                        randomAccessFile.write(bArr, 0, read);
                        i2 += read;
                        as.b((Context) this.e, str2 + ":current", i2);
                    } else {
                        randomAccessFile.close();
                        httpURLConnection.disconnect();
                        return true;
                    }
                }
            } else {
                com.shoujiduoduo.base.a.a.a(b, "download soft: filesize Error! response code = " + httpURLConnection.getResponseCode());
                return false;
            }
        } catch (MalformedURLException e2) {
            e2.printStackTrace();
            return false;
        } catch (IOException e3) {
            e3.printStackTrace();
            return false;
        }
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int i2, long j2) {
        if (this.m == this.g) {
            if (this.c == 3 || this.c == 4) {
                switch (i2) {
                    case 0:
                        com.shoujiduoduo.base.a.a.a(b, "enter User-Feedback panel.");
                        new k(this.e).e();
                        return;
                    case 1:
                        a(d.PANEL_ABOUT_INFO);
                        this.m = this.i;
                        if (this.e instanceof RingToneDuoduoActivity) {
                            ((RingToneDuoduoActivity) this.e).a(RingToneDuoduoActivity.a.HEADER_ABOUT_INFO);
                            return;
                        }
                        return;
                    case 2:
                        new AlertDialog.Builder(this.e).setTitle((int) R.string.hint).setMessage((int) R.string.clean_cache_confirm).setIcon(17301543).setPositiveButton((int) R.string.ok, new q(this)).setNegativeButton((int) R.string.cancel, new p(this)).show();
                        return;
                    case 3:
                        String c2 = com.umeng.analytics.b.c(this.e, "anzhi_down_url");
                        if (TextUtils.isEmpty(c2)) {
                            c2 = "http://m.anzhi.com/redirect.php?do=dlapk&puid=1140";
                        }
                        q.a(this.e).a(c2, "安智市场", q.a.immediatelly, false);
                        com.shoujiduoduo.util.widget.f.a("正在为您下载安智市场");
                        return;
                    default:
                        return;
                }
            }
        } else if (this.m == this.j) {
            Intent intent = new Intent(this.e, this.e.getClass());
            intent.addFlags(NTLMConstants.FLAG_NEGOTIATE_128_BIT_ENCRYPTION);
            PendingIntent activity = PendingIntent.getActivity(this.e, 0, intent, 0);
            Notification notification = new Notification(R.drawable.icon_download, "正在下载" + this.q.a(i2).b, System.currentTimeMillis());
            notification.flags |= 16;
            notification.setLatestEventInfo(this.e, this.e.getResources().getString(R.string.app_name), this.e.getResources().getString(R.string.downloading) + this.q.a(i2).b, activity);
            this.t.notify(1001, notification);
            new r(this, i2).start();
        }
    }

    public void a(com.shoujiduoduo.base.bean.c cVar, int i2) {
        if (this.m == this.k) {
            switch (i2) {
                case 0:
                    this.s.notifyDataSetChanged();
                    a(d.PANEL_DUODUO_FAMILY);
                    this.m = this.j;
                    ((RingToneDuoduoActivity) this.e).a(RingToneDuoduoActivity.a.HEADER_DUODUO_FAMILY);
                    return;
                case 1:
                    a(d.PANEL_DUODUO_FAMILY_LOAD_FAILED);
                    this.m = this.l;
                    ((RingToneDuoduoActivity) this.e).a(RingToneDuoduoActivity.a.HEADER_DUODUO_FAMILY);
                    return;
                default:
                    return;
            }
        }
    }

    /* compiled from: MoreOptionsScene */
    class b extends BaseAdapter {
        private Context b;
        private LayoutInflater c;

        /* synthetic */ b(g gVar, Context context, h hVar) {
            this(context);
        }

        private b(Context context) {
            this.b = context;
            this.c = LayoutInflater.from(context);
        }

        public int getCount() {
            return g.this.c;
        }

        public Object getItem(int i) {
            return Integer.valueOf(i);
        }

        public long getItemId(int i) {
            return (long) i;
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            if (view == null) {
                view = this.c.inflate((int) R.layout.more_options_list_item, (ViewGroup) null);
            }
            TextView textView = (TextView) view.findViewById(R.id.option_title);
            TextView textView2 = (TextView) view.findViewById(R.id.option_des);
            ImageView imageView = (ImageView) view.findViewById(R.id.icon);
            switch (i) {
                case 0:
                    textView.setText((int) R.string.more_options_feedback);
                    break;
                case 1:
                    textView.setText((int) R.string.more_options_help_about);
                    break;
                case 2:
                    textView.setText((int) R.string.more_options_clear_cache);
                    break;
                case 3:
                    textView2.setVisibility(0);
                    imageView.setVisibility(0);
                    textView.setText("安智市场");
                    textView2.setText("中国最大的安卓手机应用市场");
                    break;
            }
            return view;
        }
    }

    /* compiled from: MoreOptionsScene */
    class a extends BaseAdapter {

        /* renamed from: a  reason: collision with root package name */
        final int f899a = 10;
        private com.shoujiduoduo.b.a.f c;
        private Activity d;
        private LayoutInflater e;
        private Drawable[] f;

        public a(com.shoujiduoduo.b.a.f fVar, Activity activity) {
            this.c = fVar;
            this.d = activity;
            this.e = LayoutInflater.from(activity);
            if (this.c.b() <= 10) {
                this.f = new Drawable[10];
            } else {
                this.f = new Drawable[this.c.b()];
            }
        }

        public void notifyDataSetChanged() {
            com.shoujiduoduo.base.a.a.a(g.b, "notifyDataSetChanged Thread ID: " + Thread.currentThread().getName());
            com.shoujiduoduo.base.a.a.a(g.b, "Adapter:notifyDataSetChanged, software list size = " + this.c.b());
            if (this.c.b() > 10) {
                this.f = new Drawable[this.c.b()];
            }
            super.notifyDataSetChanged();
        }

        public int getCount() {
            return this.c.b();
        }

        /* renamed from: a */
        public com.shoujiduoduo.base.bean.d getItem(int i) {
            return this.c.a(i);
        }

        public long getItemId(int i) {
            return (long) i;
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            C0023a aVar;
            com.shoujiduoduo.base.a.a.a(g.b, "getView Thread ID: " + Thread.currentThread().getName());
            if (view == null) {
                view = this.e.inflate((int) R.layout.duoduo_soft, (ViewGroup) null);
                C0023a aVar2 = new C0023a();
                aVar2.c = (ImageView) view.findViewById(R.id.software_pic);
                aVar2.f900a = (TextView) view.findViewById(R.id.software_name);
                aVar2.b = (TextView) view.findViewById(R.id.software_intro);
                view.setTag(aVar2);
                aVar = aVar2;
            } else {
                aVar = (C0023a) view.getTag();
            }
            com.shoujiduoduo.base.bean.d a2 = this.c.a(i);
            aVar.f900a.setText(a2.b);
            aVar.b.setText(a2.c);
            aVar.c.setTag(a2.f820a);
            com.shoujiduoduo.base.a.a.a(g.b, "DuoduoSoftwareAdapter:getView:pos = " + i);
            if (this.f[i] == null) {
                Drawable a3 = com.shoujiduoduo.ui.utils.d.a(a2.f820a, new s(this));
                if (a3 != null) {
                    aVar.c.setImageDrawable(a3);
                    this.f[i] = a3;
                } else {
                    aVar.c.setImageResource(R.drawable.default_software_pic);
                }
            } else {
                aVar.c.setImageDrawable(this.f[i]);
            }
            return view;
        }

        /* renamed from: com.shoujiduoduo.ui.adwall.g$a$a  reason: collision with other inner class name */
        /* compiled from: MoreOptionsScene */
        class C0023a {

            /* renamed from: a  reason: collision with root package name */
            TextView f900a;
            TextView b;
            ImageView c;

            C0023a() {
            }
        }
    }
}
