package com.shoujiduoduo.ui.adwall;

import android.widget.RadioGroup;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.util.as;

/* compiled from: WallAdView */
class t implements RadioGroup.OnCheckedChangeListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ WallAdView f915a;

    t(WallAdView wallAdView) {
        this.f915a = wallAdView;
    }

    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        if (i == R.id.adbtn_3) {
            if (this.f915a.j) {
                this.f915a.i.setCurrentItem(2);
            } else {
                this.f915a.i.setCurrentItem(1);
            }
            as.b(this.f915a.b, WallAdView.h, 1);
            this.f915a.d.setVisibility(4);
        } else if (i == R.id.adbtn_1) {
            this.f915a.i.setCurrentItem(0);
            if (this.f915a.j) {
                as.b(this.f915a.b, WallAdView.f, 1);
            } else {
                as.b(this.f915a.b, WallAdView.g, 1);
            }
            this.f915a.c.setVisibility(4);
        } else if (i == R.id.adbtn_2) {
            this.f915a.i.setCurrentItem(1);
            as.b(this.f915a.b, WallAdView.g, 1);
        }
    }
}
