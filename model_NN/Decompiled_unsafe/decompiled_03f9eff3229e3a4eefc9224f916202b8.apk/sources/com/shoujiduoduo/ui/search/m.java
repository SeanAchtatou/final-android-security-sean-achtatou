package com.shoujiduoduo.ui.search;

import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;
import com.shoujiduoduo.ringtone.R;

/* compiled from: SearchActivity */
class m implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SearchActivity f1337a;

    m(SearchActivity searchActivity) {
        this.f1337a = searchActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.ui.search.SearchActivity.a(com.shoujiduoduo.ui.search.SearchActivity, boolean):boolean
     arg types: [com.shoujiduoduo.ui.search.SearchActivity, int]
     candidates:
      com.shoujiduoduo.ui.search.SearchActivity.a(com.shoujiduoduo.ui.search.SearchActivity, java.lang.String):java.lang.String
      com.shoujiduoduo.ui.search.SearchActivity.a(com.shoujiduoduo.ui.search.SearchActivity, java.lang.String[]):void
      com.shoujiduoduo.ui.search.SearchActivity.a(com.shoujiduoduo.ui.search.SearchActivity, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.ui.search.SearchActivity.b(com.shoujiduoduo.ui.search.SearchActivity, boolean):boolean
     arg types: [com.shoujiduoduo.ui.search.SearchActivity, int]
     candidates:
      com.shoujiduoduo.ui.search.SearchActivity.b(com.shoujiduoduo.ui.search.SearchActivity, java.lang.String):void
      com.shoujiduoduo.ui.search.SearchActivity.b(com.shoujiduoduo.ui.search.SearchActivity, boolean):boolean */
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        boolean unused = this.f1337a.k = true;
        if (((TextView) view.findViewById(R.id.auto_complete_item)).getText().equals("清空搜索历史")) {
            this.f1337a.h();
            this.f1337a.n.dismissDropDown();
            this.f1337a.n.setText("");
        } else if (this.f1337a.d != null) {
            boolean unused2 = this.f1337a.i = true;
            this.f1337a.d.performClick();
        }
    }
}
