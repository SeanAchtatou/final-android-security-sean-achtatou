package com.shoujiduoduo.ui.search;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import cn.banshenggua.aichang.entry.RoomsEntryFragment;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ui.utils.BaseFragmentActivity;

public class AcActivity extends BaseFragmentActivity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        a.a("search_ad", "Acactivity onCreate");
        setContentView((int) R.layout.activity_aichang);
        findViewById(R.id.backButton).setOnClickListener(new a(this));
        RoomsEntryFragment newInstance = RoomsEntryFragment.newInstance();
        FragmentTransaction beginTransaction = getSupportFragmentManager().beginTransaction();
        beginTransaction.replace(R.id.aichang_layout, newInstance);
        beginTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        beginTransaction.commitAllowingStateLoss();
        newInstance.getFragmentManager().executePendingTransactions();
    }
}
