package com.shoujiduoduo.ui.search;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.shoujiduoduo.b.a.t;
import com.shoujiduoduo.b.c.n;
import com.shoujiduoduo.base.bean.f;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ui.utils.DDListFragment;
import com.shoujiduoduo.ui.utils.pageindicator.TabPageIndicator;
import com.shoujiduoduo.util.av;
import com.umeng.analytics.b;

public class SearchResultFrag extends Fragment {

    /* renamed from: a  reason: collision with root package name */
    private TabPageIndicator f1325a;
    private ViewPager b;
    private com.shoujiduoduo.ui.utils.a c;
    /* access modifiers changed from: private */
    public DDListFragment d;
    private String e;
    private String f;
    /* access modifiers changed from: private */
    public boolean g;
    private boolean h;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate((int) R.layout.fragment_search_result, viewGroup, false);
        this.b = (ViewPager) inflate.findViewById(R.id.vp_search_result);
        this.b.setAdapter(new a(getActivity().getSupportFragmentManager()));
        this.g = false;
        this.f1325a = (TabPageIndicator) inflate.findViewById(R.id.title_indicator);
        this.f1325a.setViewPager(this.b);
        this.f1325a.setOnPageChangeListener(new p(this));
        if (!this.g) {
            this.f1325a.setVisibility(8);
        }
        this.h = com.shoujiduoduo.util.a.d();
        this.d = new DDListFragment();
        this.c = new com.shoujiduoduo.ui.utils.a(getActivity());
        Bundle bundle2 = new Bundle();
        bundle2.putString("adapter_type", "ring_list_adapter");
        String c2 = b.c(RingDDApp.c(), "feed_ad_list_id");
        if ((av.b(c2) || c2.contains("search")) && this.h) {
            bundle2.putBoolean("support_feed_ad", true);
        }
        this.d.setArguments(bundle2);
        this.d.a(this.c.a());
        com.shoujiduoduo.base.a.a.a("SearchResultFrag", "oncreateview");
        return inflate;
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public void a(String str, String str2) {
        com.shoujiduoduo.base.a.a.a("SearchResultFrag", "showResult");
        this.e = str;
        this.f = str2;
        if (com.shoujiduoduo.a.b.b.c().f()) {
            t.a a2 = com.shoujiduoduo.a.b.b.c().a(str);
            if (a2 != null) {
                this.c.a(a2);
                this.c.a(true);
                com.shoujiduoduo.base.a.a.a("SearchResultFrag", "显示搜索广告， " + a2.toString());
            } else {
                com.shoujiduoduo.base.a.a.a("SearchResultFrag", "没有匹配检索词的搜索广告");
                this.c.a(false);
            }
        } else {
            this.c.a(false);
            com.shoujiduoduo.base.a.a.a("SearchResultFrag", "检索广告数据尚未获取");
        }
        this.d.a(new n(f.a.list_ring_search, str, str2));
        com.shoujiduoduo.base.a.a.a("SearchResultFrag", "refreshList");
        if (this.g) {
        }
        this.b.setCurrentItem(0);
        this.b.getAdapter().notifyDataSetChanged();
    }

    private class a extends FragmentStatePagerAdapter {
        private String[] b = {"铃声", "相关壁纸"};

        public a(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        public CharSequence getPageTitle(int i) {
            return this.b[i];
        }

        public Fragment getItem(int i) {
            if (i == 0) {
                return SearchResultFrag.this.d;
            }
            if (i == 1) {
            }
            return null;
        }

        public int getCount() {
            if (SearchResultFrag.this.g) {
                return this.b.length;
            }
            return 1;
        }
    }
}
