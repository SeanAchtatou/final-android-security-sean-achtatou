package com.shoujiduoduo.ui.search;

import android.support.v4.view.ViewPager;
import cn.banshenggua.aichang.utils.Constants;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.umeng.analytics.b;
import java.util.HashMap;

/* compiled from: SearchResultFrag */
class p implements ViewPager.OnPageChangeListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SearchResultFrag f1340a;

    p(SearchResultFrag searchResultFrag) {
        this.f1340a = searchResultFrag;
    }

    public void onPageSelected(int i) {
        a.a("SearchResultFrag", "onPageSelected:" + i);
        HashMap hashMap = new HashMap();
        hashMap.put(Constants.ITEM, i == 0 ? "ring" : "wallpaper");
        b.a(RingDDApp.c(), "search_view_show", hashMap);
    }

    public void onPageScrolled(int i, float f, int i2) {
    }

    public void onPageScrollStateChanged(int i) {
    }
}
