package com.shoujiduoduo.ui.search;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import com.shoujiduoduo.b.d.h;
import com.shoujiduoduo.base.a.a;

/* compiled from: SearchActivity */
class n implements TextWatcher {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SearchActivity f1338a;

    n(SearchActivity searchActivity) {
        this.f1338a = searchActivity;
    }

    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        a.a("SearchActivity", "onTextChanged, :" + ((Object) charSequence));
        if (charSequence == null || charSequence.length() <= 0) {
            this.f1338a.e.setVisibility(4);
        } else {
            this.f1338a.e.setVisibility(0);
        }
        if (!this.f1338a.f && !this.f1338a.h && !this.f1338a.g) {
            if (charSequence != null) {
                String unused = this.f1338a.j = charSequence.toString();
            }
            if (TextUtils.isEmpty(this.f1338a.j)) {
                this.f1338a.a(this.f1338a.m);
            } else {
                new h().a(this.f1338a.j, new o(this));
            }
        }
    }

    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public void afterTextChanged(Editable editable) {
    }
}
