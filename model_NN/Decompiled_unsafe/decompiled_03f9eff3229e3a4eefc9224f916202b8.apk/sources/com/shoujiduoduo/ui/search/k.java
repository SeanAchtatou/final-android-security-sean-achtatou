package com.shoujiduoduo.ui.search;

import android.view.View;
import android.widget.AutoCompleteTextView;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.ringtone.RingDDApp;

/* compiled from: SearchActivity */
class k implements View.OnFocusChangeListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SearchActivity f1335a;

    k(SearchActivity searchActivity) {
        this.f1335a = searchActivity;
    }

    public void onFocusChange(View view, boolean z) {
        AutoCompleteTextView autoCompleteTextView = (AutoCompleteTextView) view;
        a.a("SearchActivity", "autocompletetextview, onFocusChange:" + z);
        if (z && autoCompleteTextView.isShown() && !RingDDApp.c().getSharedPreferences("search_history", 0).getString("history", "清空搜索历史").equals("清空搜索历史")) {
            autoCompleteTextView.showDropDown();
        }
    }
}
