package com.shoujiduoduo.ui.search;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageButton;
import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.a.c.w;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ui.home.DuoduoAdContainer;
import com.shoujiduoduo.ui.search.HotWordFrag;
import com.shoujiduoduo.ui.utils.BaseFragmentActivity;
import com.shoujiduoduo.util.av;
import com.umeng.analytics.b;

public class SearchActivity extends BaseFragmentActivity implements HotWordFrag.b {
    /* access modifiers changed from: private */
    public static int u = 1;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public HotWordFrag f1323a;
    /* access modifiers changed from: private */
    public SearchResultFrag b;
    private ImageButton c;
    /* access modifiers changed from: private */
    public Button d;
    /* access modifiers changed from: private */
    public ImageButton e;
    /* access modifiers changed from: private */
    public boolean f = false;
    /* access modifiers changed from: private */
    public boolean g = false;
    /* access modifiers changed from: private */
    public boolean h = false;
    /* access modifiers changed from: private */
    public boolean i = false;
    /* access modifiers changed from: private */
    public String j;
    /* access modifiers changed from: private */
    public boolean k;
    /* access modifiers changed from: private */
    public boolean l;
    /* access modifiers changed from: private */
    public String[] m;
    /* access modifiers changed from: private */
    public AutoCompleteTextView n;
    /* access modifiers changed from: private */
    public a o = new a(this, null);
    /* access modifiers changed from: private */
    public DuoduoAdContainer p;
    private boolean q;
    private boolean r;
    private w s = new h(this);
    private View.OnClickListener t = new i(this);
    private View.OnClickListener v = new e(this);

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_search);
        setTheme((int) R.style.StyledIndicators);
        this.n = (AutoCompleteTextView) findViewById(R.id.search_input);
        a(this.n);
        this.c = (ImageButton) findViewById(R.id.backButton);
        this.c.setOnClickListener(this.t);
        this.e = (ImageButton) findViewById(R.id.clear_edit_text);
        this.e.setVisibility(4);
        this.e.setOnClickListener(new d(this));
        this.d = (Button) findViewById(R.id.search_button);
        if (this.d != null) {
            this.d.setOnClickListener(this.v);
        }
        this.f1323a = new HotWordFrag();
        this.b = new SearchResultFrag();
        this.l = com.shoujiduoduo.util.a.d();
        if ("true".equals(b.c(getApplicationContext(), "search_banner_switch"))) {
            this.p = (DuoduoAdContainer) findViewById(R.id.ad_container);
            if (!com.shoujiduoduo.util.a.e() || this.l) {
                this.p.setVisibility(8);
            } else {
                this.p.setVisibility(0);
                this.p.c();
            }
        }
        x.a().a(com.shoujiduoduo.a.a.b.OBSERVER_VIP, this.s);
        d();
        x.a().b(new g(this));
    }

    private void d() {
        FragmentTransaction beginTransaction = getSupportFragmentManager().beginTransaction();
        beginTransaction.add((int) R.id.hotword_layout, this.f1323a);
        this.q = true;
        beginTransaction.add((int) R.id.ringlist_layout, this.b);
        this.r = true;
        beginTransaction.hide(this.b);
        beginTransaction.commitAllowingStateLoss();
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        e();
    }

    /* access modifiers changed from: private */
    public void e() {
        Intent intent = getIntent();
        if (intent != null) {
            String stringExtra = intent.getStringExtra("from");
            String stringExtra2 = intent.getStringExtra("key");
            if (!TextUtils.isEmpty(stringExtra)) {
                if (stringExtra.equals("push")) {
                    this.h = true;
                }
                this.n.setText(stringExtra2);
                this.d.performClick();
                return;
            }
            a();
        }
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4) {
            return super.onKeyDown(i2, keyEvent);
        }
        this.c.performClick();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.o != null) {
            this.o.removeCallbacksAndMessages(null);
        }
        x.a().b(com.shoujiduoduo.a.a.b.OBSERVER_VIP, this.s);
    }

    public void a() {
        com.shoujiduoduo.base.a.a.a("SearchActivity", "showHotwordlist");
        FragmentTransaction beginTransaction = getSupportFragmentManager().beginTransaction();
        if (!this.f1323a.isAdded() && !this.q) {
            beginTransaction.add((int) R.id.hotword_layout, this.f1323a);
        }
        beginTransaction.show(this.f1323a);
        if (this.b.isAdded() && this.b.isVisible()) {
            beginTransaction.hide(this.b);
        }
        beginTransaction.commitAllowingStateLoss();
    }

    public void b() {
        com.shoujiduoduo.base.a.a.a("SearchActivity", "showSearchRingList");
        FragmentTransaction beginTransaction = getSupportFragmentManager().beginTransaction();
        if (!this.b.isAdded() && !this.r) {
            beginTransaction.add((int) R.id.ringlist_layout, this.b);
        }
        beginTransaction.show(this.b);
        if (this.f1323a.isAdded() && this.f1323a.isVisible()) {
            beginTransaction.hide(this.f1323a);
        }
        beginTransaction.commitAllowingStateLoss();
    }

    public void a(String str) {
        if (this.d != null) {
            this.f = true;
            this.n.setText(str);
            this.d.performClick();
        }
    }

    private class a extends Handler {
        private a() {
        }

        /* synthetic */ a(SearchActivity searchActivity, d dVar) {
            this();
        }

        public void handleMessage(Message message) {
            super.handleMessage(message);
            if (!SearchActivity.this.isFinishing()) {
                SearchActivity.this.n.requestFocus();
                SearchActivity.this.n.showDropDown();
            }
        }
    }

    private void a(AutoCompleteTextView autoCompleteTextView) {
        String[] f2 = f();
        ArrayAdapter arrayAdapter = new ArrayAdapter(this, (int) R.layout.dropdown_item, f2);
        this.m = f2;
        autoCompleteTextView.setOnEditorActionListener(new j(this));
        autoCompleteTextView.setAdapter(arrayAdapter);
        autoCompleteTextView.setDropDownHeight(-2);
        autoCompleteTextView.setThreshold(1);
        autoCompleteTextView.setOnFocusChangeListener(new k(this));
        autoCompleteTextView.setOnClickListener(new l(this));
        autoCompleteTextView.setOnItemClickListener(new m(this));
        autoCompleteTextView.addTextChangedListener(new n(this));
    }

    private String[] f() {
        String[] split = RingDDApp.c().getSharedPreferences("search_history", 0).getString("history", "清空搜索历史").split(",");
        if (split.length <= 25) {
            return split;
        }
        String[] strArr = new String[25];
        System.arraycopy(split, 0, strArr, 0, 24);
        strArr[24] = "清空搜索历史";
        return strArr;
    }

    /* access modifiers changed from: private */
    public void a(String[] strArr) {
        this.n.setAdapter(new ArrayAdapter(this, (int) R.layout.dropdown_item, strArr));
    }

    /* access modifiers changed from: private */
    public void g() {
        ((InputMethodManager) RingDDApp.c().getSystemService("input_method")).hideSoftInputFromWindow(this.n.getWindowToken(), 2);
    }

    /* access modifiers changed from: private */
    public void b(String str) {
        SharedPreferences sharedPreferences = RingDDApp.c().getSharedPreferences("search_history", 0);
        String string = sharedPreferences.getString("history", "清空搜索历史");
        if (!string.contains(str + ",")) {
            StringBuilder sb = new StringBuilder(string);
            sb.insert(0, str + ",");
            String[] split = sb.toString().split(",");
            if (split.length > 25) {
                String[] strArr = new String[25];
                System.arraycopy(split, 0, strArr, 0, 24);
                strArr[24] = "清空搜索历史";
                sharedPreferences.edit().putString("history", av.a(strArr, ",")).commit();
                this.m = strArr;
                a(strArr);
                return;
            }
            sharedPreferences.edit().putString("history", sb.toString()).commit();
            this.m = split;
            a(split);
        }
    }

    /* access modifiers changed from: private */
    public void h() {
        RingDDApp.c().getSharedPreferences("search_history", 0).edit().putString("history", "清空搜索历史").commit();
        this.m = new String[]{"清空搜索历史"};
        a(this.m);
    }
}
