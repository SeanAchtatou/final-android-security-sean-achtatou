package com.shoujiduoduo.ui.search;

import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;
import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.base.a.b;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.ui.cailing.TestCmcc;
import com.shoujiduoduo.ui.cailing.TestCtcc;
import com.shoujiduoduo.ui.cailing.TestCucc;
import com.shoujiduoduo.util.ak;
import com.shoujiduoduo.util.widget.f;
import java.io.IOException;

/* compiled from: SearchActivity */
class e implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SearchActivity f1331a;

    e(SearchActivity searchActivity) {
        this.f1331a = searchActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.ui.search.SearchActivity.b(com.shoujiduoduo.ui.search.SearchActivity, boolean):boolean
     arg types: [com.shoujiduoduo.ui.search.SearchActivity, int]
     candidates:
      com.shoujiduoduo.ui.search.SearchActivity.b(com.shoujiduoduo.ui.search.SearchActivity, java.lang.String):void
      com.shoujiduoduo.ui.search.SearchActivity.b(com.shoujiduoduo.ui.search.SearchActivity, boolean):boolean */
    public void onClick(View view) {
        String str;
        a.a("SearchActivity", "Search Button Clicked!");
        String obj = this.f1331a.n.getText().toString();
        if (TextUtils.isEmpty(obj)) {
            f.a("请输入搜索关键词");
            return;
        }
        PlayerService b = ak.a().b();
        if (b != null && b.j()) {
            b.k();
        }
        this.f1331a.g();
        this.f1331a.b(obj);
        if (obj.equalsIgnoreCase("*#06#getinstallsrc")) {
            Toast.makeText(this.f1331a, com.shoujiduoduo.util.f.p(), 1).show();
        } else if (obj.equalsIgnoreCase("*#06#testctcc")) {
            this.f1331a.startActivity(new Intent(this.f1331a, TestCtcc.class));
        } else if (obj.equalsIgnoreCase("*#06#testcucc")) {
            this.f1331a.startActivity(new Intent(this.f1331a, TestCucc.class));
        } else if (obj.equalsIgnoreCase("*#06#testcmcc")) {
            this.f1331a.startActivity(new Intent(this.f1331a, TestCmcc.class));
        } else if (obj.equalsIgnoreCase("*#06#debug")) {
            a.f812a = true;
            b.f813a = false;
            f.a("已开启调试模式， 日志文件路径：/sdcard/shoujiduoduo/duoduo.log");
        } else if (obj.equalsIgnoreCase("*#06#logcat")) {
            try {
                Process exec = Runtime.getRuntime().exec(new String[]{"logcat", "-v", "time", "-d", "-f", "/sdcard/logcat.log"});
                exec.waitFor();
                exec.exitValue();
                Toast.makeText(this.f1331a, "日志已取出，请拷贝/sdcard/logcat.log", 1).show();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e2) {
                e2.printStackTrace();
            }
        } else {
            if (this.f1331a.g) {
                obj = "&rid=" + obj;
            }
            if (this.f1331a.h) {
                str = "push";
            } else if (this.f1331a.f) {
                str = "hot";
            } else if (this.f1331a.i) {
                str = "suggest";
            } else {
                str = "input";
            }
            this.f1331a.b();
            x.a().b(new f(this, obj, str));
            boolean unused = this.f1331a.f = false;
            boolean unused2 = this.f1331a.h = false;
            boolean unused3 = this.f1331a.g = false;
            boolean unused4 = this.f1331a.i = false;
        }
    }
}
