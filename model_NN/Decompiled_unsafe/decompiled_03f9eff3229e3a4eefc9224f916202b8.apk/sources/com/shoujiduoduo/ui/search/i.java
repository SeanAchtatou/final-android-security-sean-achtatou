package com.shoujiduoduo.ui.search;

import android.view.View;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.util.ak;

/* compiled from: SearchActivity */
class i implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SearchActivity f1333a;

    i(SearchActivity searchActivity) {
        this.f1333a = searchActivity;
    }

    public void onClick(View view) {
        PlayerService b = ak.a().b();
        if (b != null && b.j()) {
            b.k();
        }
        if (this.f1333a.f1323a.isVisible()) {
            this.f1333a.finish();
        } else {
            this.f1333a.a();
        }
    }
}
