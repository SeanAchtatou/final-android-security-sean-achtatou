package com.shoujiduoduo.ui.search;

import android.view.KeyEvent;
import android.widget.TextView;

/* compiled from: SearchActivity */
class j implements TextView.OnEditorActionListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SearchActivity f1334a;

    j(SearchActivity searchActivity) {
        this.f1334a = searchActivity;
    }

    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        if (i != 3 && i != 0) {
            return false;
        }
        this.f1334a.d.performClick();
        this.f1334a.n.dismissDropDown();
        return true;
    }
}
