package com.shoujiduoduo.ui.search;

import com.shoujiduoduo.b.d.h;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.util.av;

/* compiled from: SearchActivity */
class o implements h.b {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ n f1339a;

    o(n nVar) {
        this.f1339a = nVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.ui.search.SearchActivity.a(com.shoujiduoduo.ui.search.SearchActivity, boolean):boolean
     arg types: [com.shoujiduoduo.ui.search.SearchActivity, int]
     candidates:
      com.shoujiduoduo.ui.search.SearchActivity.a(com.shoujiduoduo.ui.search.SearchActivity, java.lang.String):java.lang.String
      com.shoujiduoduo.ui.search.SearchActivity.a(com.shoujiduoduo.ui.search.SearchActivity, java.lang.String[]):void
      com.shoujiduoduo.ui.search.SearchActivity.a(com.shoujiduoduo.ui.search.SearchActivity, boolean):boolean */
    public void a(String str, String[] strArr) {
        if (str != null && str.length() > 0 && !str.equals(this.f1339a.f1338a.j)) {
            a.a("SearchActivity", "不是当前检索词的联想结果， curword:" + this.f1339a.f1338a.j + ", return word:" + str);
        } else if (strArr == null || strArr.length <= 0) {
            a.a("SearchActivity", "onSuggestData, key:" + str + ", data:null");
        } else {
            a.a("SearchActivity", "onSuggestData, key:" + str + " ,data:" + av.a(strArr, ","));
            if (!this.f1339a.f1338a.k) {
                this.f1339a.f1338a.a(strArr);
                a.a("SearchActivity", "show drop down");
                this.f1339a.f1338a.o.sendEmptyMessage(SearchActivity.u);
            } else {
                a.a("SearchActivity", "from item click, not show");
            }
            boolean unused = this.f1339a.f1338a.k = false;
        }
    }
}
