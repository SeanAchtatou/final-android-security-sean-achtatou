package com.shoujiduoduo.ui.settings;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;
import com.shoujiduoduo.a.b.b;
import com.shoujiduoduo.base.bean.ab;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.activity.RingToneDuoduoActivity;
import com.shoujiduoduo.util.f;

/* compiled from: SettingMenu */
public class ae extends PopupWindow {

    /* renamed from: a  reason: collision with root package name */
    AdapterView.OnItemClickListener f1357a = new ag(this);
    private ListView b;
    /* access modifiers changed from: private */
    public View c;
    /* access modifiers changed from: private */
    public RingToneDuoduoActivity d;
    /* access modifiers changed from: private */
    public int[] e;
    /* access modifiers changed from: private */
    public int[] f;

    private int[] a() {
        boolean contains = f.l().contains("xiaomi");
        ab c2 = b.g().c();
        if (contains) {
            if (!c2.j() || c2.g() != 3) {
                return new int[]{R.drawable.icon_menu_feedback, R.drawable.icon_menu_clearcache, R.drawable.icon_menu_aboutinfo, R.drawable.icon_menu_continuous_play};
            }
            return new int[]{R.drawable.icon_menu_feedback, R.drawable.icon_menu_clearcache, R.drawable.icon_menu_aboutinfo, R.drawable.menu_cailing, R.drawable.icon_menu_continuous_play};
        } else if (!c2.j() || c2.g() != 3) {
            return new int[]{R.drawable.icon_menu_praise, R.drawable.icon_menu_feedback, R.drawable.icon_menu_clearcache, R.drawable.icon_menu_aboutinfo, R.drawable.icon_menu_continuous_play};
        } else {
            return new int[]{R.drawable.icon_menu_praise, R.drawable.icon_menu_feedback, R.drawable.icon_menu_clearcache, R.drawable.icon_menu_aboutinfo, R.drawable.menu_cailing, R.drawable.icon_menu_continuous_play};
        }
    }

    private int[] b() {
        int i;
        boolean contains = f.l().contains("xiaomi");
        ab c2 = b.g().c();
        if (contains) {
            if (!c2.j() || c2.g() != 3) {
                int[] iArr = new int[4];
                iArr[0] = R.string.menu_feedback;
                iArr[1] = R.string.menu_clean_cache;
                iArr[2] = R.string.menu_about;
                iArr[3] = f.y() ? R.string.menu_close_continus_play : R.string.menu_open_continus_play;
                return iArr;
            }
            int[] iArr2 = new int[5];
            iArr2[0] = R.string.menu_feedback;
            iArr2[1] = R.string.menu_clean_cache;
            iArr2[2] = R.string.menu_about;
            iArr2[3] = R.string.menu_cailing;
            if (f.y()) {
                i = R.string.menu_close_continus_play;
            } else {
                i = R.string.menu_open_continus_play;
            }
            iArr2[4] = i;
            return iArr2;
        } else if (!c2.j() || c2.g() != 3) {
            int[] iArr3 = new int[5];
            iArr3[0] = R.string.menu_praise;
            iArr3[1] = R.string.menu_feedback;
            iArr3[2] = R.string.menu_clean_cache;
            iArr3[3] = R.string.menu_about;
            iArr3[4] = f.y() ? R.string.menu_close_continus_play : R.string.menu_open_continus_play;
            return iArr3;
        } else {
            int[] iArr4 = new int[6];
            iArr4[0] = R.string.menu_praise;
            iArr4[1] = R.string.menu_feedback;
            iArr4[2] = R.string.menu_clean_cache;
            iArr4[3] = R.string.menu_about;
            iArr4[4] = R.string.menu_cailing;
            iArr4[5] = f.y() ? R.string.menu_close_continus_play : R.string.menu_open_continus_play;
            return iArr4;
        }
    }

    public ae(RingToneDuoduoActivity ringToneDuoduoActivity, boolean z) {
        super(ringToneDuoduoActivity);
        this.d = ringToneDuoduoActivity;
        this.c = ((LayoutInflater) ringToneDuoduoActivity.getSystemService("layout_inflater")).inflate((int) R.layout.menu, (ViewGroup) null);
        this.b = (ListView) this.c.findViewById(R.id.menu_list);
        this.e = a();
        this.f = b();
        this.b.setAdapter((ListAdapter) new a());
        this.b.setItemsCanFocus(false);
        this.b.setChoiceMode(2);
        this.b.setOnItemClickListener(this.f1357a);
        setContentView(this.c);
        setWidth(-2);
        setHeight(-2);
        setFocusable(true);
        if (z) {
            setAnimationStyle(R.style.menuPopupStyle);
        }
        setBackgroundDrawable(new ColorDrawable(-1342177280));
        this.c.setOnTouchListener(new af(this));
    }

    /* access modifiers changed from: private */
    public void c() {
        boolean y = f.y();
        f.a(!y);
        Toast.makeText(this.d, y ? R.string.continus_play_close : R.string.continus_play_open, 1).show();
        com.umeng.analytics.b.b(this.d, y ? "CONTINUOUS_PLAY_DISABLE" : "CONTINUOUS_PLAY_ENABLE");
    }

    /* access modifiers changed from: private */
    public void d() {
        this.d.startActivity(new Intent(this.d, AboutActivity.class));
    }

    /* compiled from: SettingMenu */
    class a extends BaseAdapter {
        a() {
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            View inflate = ae.this.d.getLayoutInflater().inflate((int) R.layout.menu_item, (ViewGroup) null);
            ((ImageView) inflate.findViewById(R.id.menu_logos)).setImageResource(ae.this.e[i]);
            ((TextView) inflate.findViewById(R.id.menu_item_text)).setText(ae.this.f[i]);
            return inflate;
        }

        public int getCount() {
            return ae.this.e.length;
        }

        public Object getItem(int i) {
            return Integer.valueOf(i);
        }

        public long getItemId(int i) {
            return (long) i;
        }
    }
}
