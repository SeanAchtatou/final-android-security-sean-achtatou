package com.shoujiduoduo.ui.settings;

import android.view.MotionEvent;
import android.view.View;
import com.shoujiduoduo.ringtone.R;

/* compiled from: SettingMenu */
class af implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ae f1359a;

    af(ae aeVar) {
        this.f1359a = aeVar;
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        int top = this.f1359a.c.findViewById(R.id.menu_layout).getTop();
        int y = (int) motionEvent.getY();
        if (motionEvent.getAction() == 1 && y < top) {
            this.f1359a.dismiss();
        }
        return true;
    }
}
