package com.shoujiduoduo.ui.settings;

import android.app.AlertDialog;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ui.cailing.CuccMemInfoActivity;
import com.umeng.fb.k;

/* compiled from: SettingMenu */
class ag implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ae f1360a;

    ag(ae aeVar) {
        this.f1360a = aeVar;
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        if (i < this.f1360a.e.length) {
            switch (this.f1360a.e[i]) {
                case R.drawable.icon_menu_aboutinfo:
                    this.f1360a.d();
                    break;
                case R.drawable.icon_menu_clearcache:
                    new AlertDialog.Builder(this.f1360a.d).setTitle(this.f1360a.d.getResources().getString(R.string.hint)).setMessage((int) R.string.clean_cache_confirm).setIcon(17301543).setPositiveButton((int) R.string.ok, new ai(this)).setNegativeButton((int) R.string.cancel, new ah(this)).show();
                    break;
                case R.drawable.icon_menu_continuous_play:
                    this.f1360a.c();
                    break;
                case R.drawable.icon_menu_feedback:
                    new k(this.f1360a.d).e();
                    break;
                case R.drawable.icon_menu_praise:
                    try {
                        this.f1360a.d.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=com.shoujiduoduo.ringtone")));
                        break;
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(this.f1360a.d, (int) R.string.not_found_store, 0).show();
                        break;
                    }
                case R.drawable.menu_cailing:
                    this.f1360a.d.startActivity(new Intent(this.f1360a.d, CuccMemInfoActivity.class));
                    break;
            }
            this.f1360a.dismiss();
        }
    }
}
