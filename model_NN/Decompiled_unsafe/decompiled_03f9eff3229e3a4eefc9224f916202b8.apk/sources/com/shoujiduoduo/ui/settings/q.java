package com.shoujiduoduo.ui.settings;

import com.baidu.mobad.feeds.BaiduNative;
import com.baidu.mobad.feeds.NativeErrorCode;
import com.baidu.mobad.feeds.NativeResponse;
import com.shoujiduoduo.base.a.a;
import java.util.List;

/* compiled from: QuitDialog */
class q implements BaiduNative.BaiduNativeNetworkListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ m f1377a;

    q(m mVar) {
        this.f1377a = mVar;
    }

    public void onNativeFail(NativeErrorCode nativeErrorCode) {
        a.e("QuitDialog", "baidu feed, onNativeFail reason:" + nativeErrorCode.name());
    }

    public void onNativeLoad(List<NativeResponse> list) {
        if (list.size() > 0) {
            a.a("QuitDialog", "baidu feed, onNativeLoad, ad size:" + list.size());
            long unused = this.f1377a.s = System.currentTimeMillis();
            List unused2 = this.f1377a.p = list;
            return;
        }
        a.a("QuitDialog", "baidu feed, onNativeLoad, ad size is 0");
    }
}
