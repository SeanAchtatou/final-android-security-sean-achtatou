package com.shoujiduoduo.ui.settings;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ui.utils.BaseActivity;

public class AboutActivity extends BaseActivity {
    public void onCreate(Bundle bundle) {
        String str;
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_about);
        String str2 = "";
        try {
            str2 = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
            str = str2.substring(0, str2.lastIndexOf(46));
        } catch (PackageManager.NameNotFoundException e) {
            PackageManager.NameNotFoundException nameNotFoundException = e;
            nameNotFoundException.printStackTrace();
            str = str2;
        }
        TextView textView = (TextView) findViewById(R.id.about_activity_app_name);
        if (textView != null) {
            textView.setText(((Object) getResources().getText(R.string.app_name)) + " " + str);
        }
        ((TextView) findViewById(R.id.about_activity_release_date)).setText("2016-08-31");
        ((TextView) findViewById(R.id.about_activity_app_intro)).setText(getResources().getString(R.string.app_intro1) + "\n" + getResources().getString(R.string.app_intro2) + "\n" + getResources().getString(R.string.app_intro3) + "\n" + getResources().getString(R.string.app_intro4) + "\n" + getResources().getString(R.string.app_intro5));
        ((Button) findViewById(R.id.about_activity_back)).setOnClickListener(new a(this));
    }
}
