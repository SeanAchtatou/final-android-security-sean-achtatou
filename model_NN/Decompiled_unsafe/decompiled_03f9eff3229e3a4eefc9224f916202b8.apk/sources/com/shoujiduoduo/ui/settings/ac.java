package com.shoujiduoduo.ui.settings;

import android.graphics.drawable.Drawable;
import android.text.Html;
import com.shoujiduoduo.ui.settings.u;

/* compiled from: SetRingDialog */
class ac implements Html.ImageGetter {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ u.a f1355a;

    ac(u.a aVar) {
        this.f1355a = aVar;
    }

    public Drawable getDrawable(String str) {
        int i;
        try {
            i = Integer.parseInt(str);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            i = 0;
        }
        Drawable drawable = u.this.f1381a.getResources().getDrawable(i);
        drawable.setBounds(0, 0, (int) (((double) drawable.getIntrinsicWidth()) * 0.7d), (int) (((double) drawable.getIntrinsicHeight()) * 0.7d));
        return drawable;
    }
}
