package com.shoujiduoduo.ui.settings;

import android.view.View;
import android.widget.AdapterView;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ui.settings.ChangeSkinActivity;
import com.shoujiduoduo.util.as;
import com.shoujiduoduo.util.i;
import com.shoujiduoduo.util.l;
import com.shoujiduoduo.util.t;
import com.shoujiduoduo.util.widget.f;

/* compiled from: ChangeSkinActivity */
class b implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ChangeSkinActivity f1363a;

    b(ChangeSkinActivity changeSkinActivity) {
        this.f1363a = changeSkinActivity;
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        if (i == 0) {
            as.c(RingDDApp.c(), this.f1363a.e, "default");
            as.c(RingDDApp.c(), "cur_splash_pic", "default");
        } else {
            as.c(RingDDApp.c(), this.f1363a.e, ((ChangeSkinActivity.c) this.f1363a.d.get(i)).f1344a);
            String str = ((ChangeSkinActivity.c) this.f1363a.d.get(i)).f1344a;
            String str2 = l.a(7) + t.d(str);
            if (t.f(str2)) {
                as.c(RingDDApp.c(), "cur_splash_pic", str2);
                f.a("个性启动图设置成功");
            } else {
                i.a(new c(this, str2, str));
            }
        }
        ((ChangeSkinActivity.a) this.f1363a.f1341a.getAdapter()).notifyDataSetChanged();
    }
}
