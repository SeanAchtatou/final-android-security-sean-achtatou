package com.shoujiduoduo.ui.settings;

import android.widget.CompoundButton;
import com.shoujiduoduo.ui.settings.ContactRingSettingActivity;

/* compiled from: ContactRingSettingActivity */
class h implements CompoundButton.OnCheckedChangeListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f1369a;
    final /* synthetic */ int b;
    final /* synthetic */ ContactRingSettingActivity.a c;

    h(ContactRingSettingActivity.a aVar, String str, int i) {
        this.c = aVar;
        this.f1369a = str;
        this.b = i;
    }

    public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
        if (z) {
            ContactRingSettingActivity.this.f1346a.put(this.f1369a, 1);
            ContactRingSettingActivity.this.l[this.b] = true;
            return;
        }
        ContactRingSettingActivity.this.f1346a.put(this.f1369a, 0);
        ContactRingSettingActivity.this.l[this.b] = false;
    }
}
