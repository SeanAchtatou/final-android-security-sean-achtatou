package com.shoujiduoduo.ui.settings;

import android.content.Intent;
import android.widget.Toast;
import com.shoujiduoduo.a.a.x;

/* compiled from: CropImageActivity */
class l extends x.b {
    final /* synthetic */ k d;

    l(k kVar) {
        this.d = kVar;
    }

    public void a() {
        if (this.d.f1372a.b != null) {
            Intent intent = new Intent();
            intent.setData(this.d.f1372a.b);
            this.d.f1372a.setResult(-1, intent);
        } else {
            Toast.makeText(this.d.f1372a, "背景图片保存时出错！", 0).show();
            this.d.f1372a.setResult(0);
        }
        this.d.f1372a.finish();
    }
}
