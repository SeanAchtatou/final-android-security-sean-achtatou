package com.shoujiduoduo.ui.settings;

import android.content.ContentValues;
import android.view.View;
import com.shoujiduoduo.ui.settings.ContactRingSettingActivity;

/* compiled from: ContactRingSettingActivity */
class j implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f1371a;
    final /* synthetic */ ContactRingSettingActivity.d b;
    final /* synthetic */ String c;
    final /* synthetic */ ContentValues d;
    final /* synthetic */ ContactRingSettingActivity.a e;

    j(ContactRingSettingActivity.a aVar, String str, ContactRingSettingActivity.d dVar, String str2, ContentValues contentValues) {
        this.e = aVar;
        this.f1371a = str;
        this.b = dVar;
        this.c = str2;
        this.d = contentValues;
    }

    public void onClick(View view) {
        ContactRingSettingActivity.this.f1346a.remove(this.f1371a);
        this.b.c.setText(this.c);
        this.d.put("user_set", "0");
        this.b.e.setVisibility(0);
        this.b.f.setVisibility(4);
    }
}
