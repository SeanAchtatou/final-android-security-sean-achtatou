package com.shoujiduoduo.ui.settings;

import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.ContactsContract;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SectionIndexer;
import android.widget.TextView;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ui.utils.BaseActivity;
import com.shoujiduoduo.util.aq;
import com.shoujiduoduo.util.widget.IndexListView;
import com.sina.weibo.sdk.register.mobile.SelectCountryActivity;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

public class ContactRingSettingActivity extends BaseActivity {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public HashMap<String, Integer> f1346a;
    private Button b;
    private Button c;
    private Button d;
    private String e;
    private String f;
    private RingData g;
    private BaseAdapter h;
    /* access modifiers changed from: private */
    public Uri i;
    private ListView j;
    private AsyncQueryHandler k;
    /* access modifiers changed from: private */
    public boolean[] l;
    /* access modifiers changed from: private */
    public c m;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.layout_contact_ring_setting);
        Intent intent = getIntent();
        if (intent != null) {
            this.e = intent.getStringExtra("listid");
            this.f = intent.getStringExtra("listtype");
            this.g = (RingData) intent.getExtras().getParcelable("ringdata");
        }
        this.f1346a = new HashMap<>();
        this.j = (IndexListView) findViewById(R.id.contact_list);
        this.b = (Button) findViewById(R.id.contact_back);
        this.d = (Button) findViewById(R.id.set_contact_ring_cancel);
        this.c = (Button) findViewById(R.id.set_contact_ring_save);
        this.j.setFastScrollEnabled(true);
        this.m = new c(this, null);
        this.k = new b(getContentResolver());
        this.i = RingtoneManager.getActualDefaultRingtoneUri(this, 1);
        this.b.setOnClickListener(new d(this));
        this.c.setOnClickListener(new e(this));
        this.d.setOnClickListener(new f(this));
        this.j.setOnItemClickListener(new g(this));
        PlayerService.a(true);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        PlayerService.a(false);
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4) {
            return super.onKeyDown(i2, keyEvent);
        }
        finish();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        com.shoujiduoduo.base.a.a.a("contact", "begin query");
        this.k.startQuery(0, null, ContactsContract.Contacts.CONTENT_URI, new String[]{"_id", "display_name", "custom_ringtone", "sort_key"}, null, null, "sort_key COLLATE LOCALIZED asc");
        com.shoujiduoduo.base.a.a.a("contact", "end query");
    }

    private class c extends Handler {
        private c() {
        }

        /* synthetic */ c(ContactRingSettingActivity contactRingSettingActivity, d dVar) {
            this();
        }

        public void handleMessage(Message message) {
            List list;
            super.handleMessage(message);
            if (message.what == 1 && (list = (List) message.obj) != null) {
                ContactRingSettingActivity.this.a(list);
            }
        }
    }

    private class b extends AsyncQueryHandler {
        public b(ContentResolver contentResolver) {
            super(contentResolver);
        }

        /* access modifiers changed from: protected */
        public void onQueryComplete(int i, Object obj, Cursor cursor) {
            com.shoujiduoduo.base.a.a.a("contact", "query complete in");
            ContactRingSettingActivity.this.f1346a.clear();
            if (cursor != null && cursor.getCount() > 0) {
                ArrayList arrayList = new ArrayList();
                ArrayList arrayList2 = new ArrayList();
                cursor.moveToFirst();
                for (int i2 = 0; i2 < cursor.getCount(); i2++) {
                    ContentValues contentValues = new ContentValues();
                    cursor.moveToPosition(i2);
                    String string = cursor.getString(0);
                    String string2 = cursor.getString(1);
                    String string3 = cursor.getString(2);
                    String string4 = cursor.getString(3);
                    contentValues.put("_id", string);
                    contentValues.put(SelectCountryActivity.EXTRA_COUNTRY_NAME, string2);
                    contentValues.put("custom_ringtone", string3);
                    contentValues.put("sort_key", string4);
                    contentValues.put("user_set", "0");
                    if (!(string3 == null || string3.equals("content://settings/system/ringtone") || ContactRingSettingActivity.this.i == null || string3.equals(ContactRingSettingActivity.this.i.toString()) || RingtoneManager.getRingtone(ContactRingSettingActivity.this, Uri.parse(string3)) == null)) {
                        ContentValues contentValues2 = new ContentValues();
                        contentValues2.put("_id", string);
                        contentValues2.put(SelectCountryActivity.EXTRA_COUNTRY_NAME, string2);
                        contentValues2.put("custom_ringtone", string3);
                        contentValues2.put("sort_key", "***duoduo");
                        contentValues2.put("user_set", "0");
                        arrayList2.add(contentValues2);
                    }
                    com.shoujiduoduo.base.a.a.a("contact", "2.11");
                    arrayList.add(contentValues);
                }
                if (arrayList.size() > 0) {
                    if (arrayList2.size() > 0) {
                        arrayList.addAll(0, arrayList2);
                    }
                    boolean[] unused = ContactRingSettingActivity.this.l = new boolean[arrayList.size()];
                    for (int i3 = 0; i3 < arrayList.size(); i3++) {
                        ContactRingSettingActivity.this.l[i3] = false;
                    }
                    com.shoujiduoduo.base.a.a.a("contact", "5");
                    Message message = new Message();
                    message.what = 1;
                    message.obj = arrayList;
                    ContactRingSettingActivity.this.m.sendMessage(message);
                    com.shoujiduoduo.base.a.a.a("contact", Constants.VIA_SHARE_TYPE_INFO);
                }
            }
            com.shoujiduoduo.base.a.a.a("contact", "query complete out");
        }
    }

    /* access modifiers changed from: private */
    public void a(List<ContentValues> list) {
        this.h = new a(this, list);
        this.j.setAdapter((ListAdapter) this.h);
    }

    private static class d {

        /* renamed from: a  reason: collision with root package name */
        TextView f1350a;
        TextView b;
        TextView c;
        CheckBox d;
        Button e;
        Button f;

        private d() {
        }

        /* synthetic */ d(d dVar) {
            this();
        }
    }

    private class a extends BaseAdapter implements SectionIndexer {
        private LayoutInflater b;
        private List<ContentValues> c;
        private HashMap<String, Integer> d = new HashMap<>();
        private String[] e;

        public a(Context context, List<ContentValues> list) {
            this.b = LayoutInflater.from(context);
            this.c = list;
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < list.size()) {
                    String a2 = ContactRingSettingActivity.this.a(list.get(i2).getAsString("sort_key"));
                    if (!this.d.containsKey(a2)) {
                        this.d.put(a2, Integer.valueOf(i2));
                    }
                    i = i2 + 1;
                } else {
                    ArrayList arrayList = new ArrayList(this.d.keySet());
                    Collections.sort(arrayList);
                    this.e = new String[arrayList.size()];
                    arrayList.toArray(this.e);
                    return;
                }
            }
        }

        public int getCount() {
            return this.c.size();
        }

        public Object getItem(int i) {
            return this.c.get(i);
        }

        public long getItemId(int i) {
            return (long) i;
        }

        /* JADX WARNING: Removed duplicated region for block: B:15:0x00ba  */
        /* JADX WARNING: Removed duplicated region for block: B:17:0x00c1  */
        /* JADX WARNING: Removed duplicated region for block: B:22:0x0117  */
        /* JADX WARNING: Removed duplicated region for block: B:25:0x0133  */
        /* JADX WARNING: Removed duplicated region for block: B:40:0x0196  */
        /* JADX WARNING: Removed duplicated region for block: B:42:0x01a9  */
        /* JADX WARNING: Removed duplicated region for block: B:43:0x01b5  */
        /* JADX WARNING: Removed duplicated region for block: B:44:0x01b9  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public android.view.View getView(int r12, android.view.View r13, android.view.ViewGroup r14) {
            /*
                r11 = this;
                r4 = 0
                r1 = 1
                r10 = 2131034174(0x7f05003e, float:1.7678858E38)
                r9 = 4
                r6 = 0
                if (r13 != 0) goto L_0x015e
                android.view.LayoutInflater r0 = r11.b
                r2 = 2130903146(0x7f03006a, float:1.7413102E38)
                android.view.View r13 = r0.inflate(r2, r4)
                com.shoujiduoduo.ui.settings.ContactRingSettingActivity$d r3 = new com.shoujiduoduo.ui.settings.ContactRingSettingActivity$d
                r3.<init>(r4)
                r0 = 2131427929(0x7f0b0259, float:1.8477488E38)
                android.view.View r0 = r13.findViewById(r0)
                android.widget.TextView r0 = (android.widget.TextView) r0
                r3.f1350a = r0
                r0 = 2131427933(0x7f0b025d, float:1.8477496E38)
                android.view.View r0 = r13.findViewById(r0)
                android.widget.TextView r0 = (android.widget.TextView) r0
                r3.b = r0
                r0 = 2131427934(0x7f0b025e, float:1.8477498E38)
                android.view.View r0 = r13.findViewById(r0)
                android.widget.TextView r0 = (android.widget.TextView) r0
                r3.c = r0
                r0 = 2131427930(0x7f0b025a, float:1.847749E38)
                android.view.View r0 = r13.findViewById(r0)
                android.widget.CheckBox r0 = (android.widget.CheckBox) r0
                r3.d = r0
                r0 = 2131427935(0x7f0b025f, float:1.84775E38)
                android.view.View r0 = r13.findViewById(r0)
                android.widget.Button r0 = (android.widget.Button) r0
                r3.e = r0
                r0 = 2131427931(0x7f0b025b, float:1.8477492E38)
                android.view.View r0 = r13.findViewById(r0)
                android.widget.Button r0 = (android.widget.Button) r0
                r3.f = r0
                r13.setTag(r3)
            L_0x005c:
                java.util.List<android.content.ContentValues> r0 = r11.c
                java.lang.Object r5 = r0.get(r12)
                android.content.ContentValues r5 = (android.content.ContentValues) r5
                java.lang.String r0 = "name"
                java.lang.String r0 = r5.getAsString(r0)
                java.lang.String r2 = "custom_ringtone"
                java.lang.String r4 = r5.getAsString(r2)
                java.lang.String r2 = "_id"
                java.lang.String r2 = r5.getAsString(r2)
                java.lang.String r7 = "user_set"
                java.lang.String r7 = r5.getAsString(r7)
                android.widget.TextView r8 = r3.b
                r8.setText(r0)
                if (r4 == 0) goto L_0x00a3
                java.lang.String r0 = "content://settings/system/ringtone"
                boolean r0 = r4.equals(r0)
                if (r0 != 0) goto L_0x00a3
                com.shoujiduoduo.ui.settings.ContactRingSettingActivity r0 = com.shoujiduoduo.ui.settings.ContactRingSettingActivity.this
                android.net.Uri r0 = r0.i
                if (r0 == 0) goto L_0x0167
                com.shoujiduoduo.ui.settings.ContactRingSettingActivity r0 = com.shoujiduoduo.ui.settings.ContactRingSettingActivity.this
                android.net.Uri r0 = r0.i
                java.lang.String r0 = r0.toString()
            L_0x009d:
                boolean r0 = r4.equals(r0)
                if (r0 == 0) goto L_0x016b
            L_0x00a3:
                com.shoujiduoduo.ui.settings.ContactRingSettingActivity r0 = com.shoujiduoduo.ui.settings.ContactRingSettingActivity.this
                android.content.Context r0 = r0.getApplicationContext()
                android.content.res.Resources r0 = r0.getResources()
                java.lang.String r4 = r0.getString(r10)
                r0 = r1
            L_0x00b2:
                java.lang.String r1 = "1"
                boolean r1 = r7.equals(r1)
                if (r1 == 0) goto L_0x0196
                android.widget.TextView r1 = r3.c
                r1.setText(r10)
            L_0x00bf:
                if (r0 != 0) goto L_0x01a9
                java.lang.String r0 = "1"
                boolean r0 = r7.equals(r0)
                if (r0 == 0) goto L_0x019d
                android.widget.Button r0 = r3.f
                r0.setVisibility(r6)
                android.widget.Button r0 = r3.e
                r0.setVisibility(r9)
            L_0x00d3:
                android.widget.CheckBox r0 = r3.d
                com.shoujiduoduo.ui.settings.h r1 = new com.shoujiduoduo.ui.settings.h
                r1.<init>(r11, r2, r12)
                r0.setOnCheckedChangeListener(r1)
                android.widget.CheckBox r0 = r3.d
                com.shoujiduoduo.ui.settings.ContactRingSettingActivity r1 = com.shoujiduoduo.ui.settings.ContactRingSettingActivity.this
                boolean[] r1 = r1.l
                boolean r1 = r1[r12]
                r0.setChecked(r1)
                android.widget.Button r0 = r3.e
                com.shoujiduoduo.ui.settings.i r1 = new com.shoujiduoduo.ui.settings.i
                r1.<init>(r11, r2, r5, r3)
                r0.setOnClickListener(r1)
                android.widget.Button r7 = r3.f
                com.shoujiduoduo.ui.settings.j r0 = new com.shoujiduoduo.ui.settings.j
                r1 = r11
                r0.<init>(r1, r2, r3, r4, r5)
                r7.setOnClickListener(r0)
                com.shoujiduoduo.ui.settings.ContactRingSettingActivity r1 = com.shoujiduoduo.ui.settings.ContactRingSettingActivity.this
                java.util.List<android.content.ContentValues> r0 = r11.c
                java.lang.Object r0 = r0.get(r12)
                android.content.ContentValues r0 = (android.content.ContentValues) r0
                java.lang.String r2 = "sort_key"
                java.lang.String r0 = r0.getAsString(r2)
                java.lang.String r1 = r1.a(r0)
                int r0 = r12 + -1
                if (r0 < 0) goto L_0x01b5
                com.shoujiduoduo.ui.settings.ContactRingSettingActivity r2 = com.shoujiduoduo.ui.settings.ContactRingSettingActivity.this
                java.util.List<android.content.ContentValues> r0 = r11.c
                int r4 = r12 + -1
                java.lang.Object r0 = r0.get(r4)
                android.content.ContentValues r0 = (android.content.ContentValues) r0
                java.lang.String r4 = "sort_key"
                java.lang.String r0 = r0.getAsString(r4)
                java.lang.String r0 = r2.a(r0)
            L_0x012d:
                boolean r0 = r0.equals(r1)
                if (r0 != 0) goto L_0x01b9
                android.widget.TextView r0 = r3.f1350a
                r0.setVisibility(r6)
                java.lang.String r0 = "!"
                boolean r0 = r1.equals(r0)
                if (r0 == 0) goto L_0x01c8
                com.shoujiduoduo.ui.settings.ContactRingSettingActivity r0 = com.shoujiduoduo.ui.settings.ContactRingSettingActivity.this
                android.content.Context r0 = r0.getApplicationContext()
                android.content.res.Resources r0 = r0.getResources()
                r1 = 2131034203(0x7f05005b, float:1.7678917E38)
                java.lang.String r0 = r0.getString(r1)
            L_0x0151:
                android.widget.TextView r1 = r3.f1350a
                r1.setText(r0)
                java.lang.String r0 = "contact"
                java.lang.String r1 = "show"
                android.util.Log.d(r0, r1)
            L_0x015d:
                return r13
            L_0x015e:
                java.lang.Object r0 = r13.getTag()
                com.shoujiduoduo.ui.settings.ContactRingSettingActivity$d r0 = (com.shoujiduoduo.ui.settings.ContactRingSettingActivity.d) r0
                r3 = r0
                goto L_0x005c
            L_0x0167:
                java.lang.String r0 = ""
                goto L_0x009d
            L_0x016b:
                android.net.Uri r0 = android.net.Uri.parse(r4)
                com.shoujiduoduo.ui.settings.ContactRingSettingActivity r4 = com.shoujiduoduo.ui.settings.ContactRingSettingActivity.this
                android.media.Ringtone r0 = android.media.RingtoneManager.getRingtone(r4, r0)
                if (r0 == 0) goto L_0x0185
                com.shoujiduoduo.ui.settings.ContactRingSettingActivity r1 = com.shoujiduoduo.ui.settings.ContactRingSettingActivity.this     // Catch:{ NotFoundException -> 0x0181 }
                java.lang.String r0 = r0.getTitle(r1)     // Catch:{ NotFoundException -> 0x0181 }
            L_0x017d:
                r4 = r0
                r0 = r6
                goto L_0x00b2
            L_0x0181:
                r0 = move-exception
                java.lang.String r0 = ""
                goto L_0x017d
            L_0x0185:
                com.shoujiduoduo.ui.settings.ContactRingSettingActivity r0 = com.shoujiduoduo.ui.settings.ContactRingSettingActivity.this
                android.content.Context r0 = r0.getApplicationContext()
                android.content.res.Resources r0 = r0.getResources()
                java.lang.String r4 = r0.getString(r10)
                r0 = r1
                goto L_0x00b2
            L_0x0196:
                android.widget.TextView r1 = r3.c
                r1.setText(r4)
                goto L_0x00bf
            L_0x019d:
                android.widget.Button r0 = r3.f
                r0.setVisibility(r9)
                android.widget.Button r0 = r3.e
                r0.setVisibility(r6)
                goto L_0x00d3
            L_0x01a9:
                android.widget.Button r0 = r3.f
                r0.setVisibility(r9)
                android.widget.Button r0 = r3.e
                r0.setVisibility(r9)
                goto L_0x00d3
            L_0x01b5:
                java.lang.String r0 = " "
                goto L_0x012d
            L_0x01b9:
                android.widget.TextView r0 = r3.f1350a
                r1 = 8
                r0.setVisibility(r1)
                java.lang.String r0 = "contact"
                java.lang.String r1 = "hide"
                android.util.Log.d(r0, r1)
                goto L_0x015d
            L_0x01c8:
                r0 = r1
                goto L_0x0151
            */
            throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.ui.settings.ContactRingSettingActivity.a.getView(int, android.view.View, android.view.ViewGroup):android.view.View");
        }

        public int getPositionForSection(int i) {
            return this.d.get(this.e[i]).intValue();
        }

        public int getSectionForPosition(int i) {
            String a2 = ContactRingSettingActivity.this.a(this.c.get(i).getAsString("sort_key"));
            for (int i2 = 0; i2 < this.e.length; i2++) {
                if (this.e[i2].equals(a2)) {
                    return i2;
                }
            }
            return 0;
        }

        public Object[] getSections() {
            return this.e;
        }
    }

    /* access modifiers changed from: private */
    public String a(String str) {
        if (str == null || str.trim().length() == 0) {
            return "#";
        }
        if (str.indexOf("***duoduo") != -1) {
            return "!";
        }
        char charAt = str.trim().substring(0, 1).charAt(0);
        if (Pattern.compile("^[A-Za-z]+$").matcher(charAt + "").matches()) {
            return (charAt + "").toUpperCase();
        }
        return "#";
    }

    public void a() {
        if (aq.a() != null) {
            aq.a().a(this.f1346a, this.g, this.e, this.f);
        }
    }
}
