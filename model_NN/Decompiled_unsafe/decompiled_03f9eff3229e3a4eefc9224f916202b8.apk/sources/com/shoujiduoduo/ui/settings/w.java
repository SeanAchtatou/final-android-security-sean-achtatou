package com.shoujiduoduo.ui.settings;

import android.view.View;
import com.shoujiduoduo.ui.settings.u;
import com.shoujiduoduo.util.i;
import com.shoujiduoduo.util.v;

/* compiled from: SetRingDialog */
class w implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ u f1385a;

    w(u uVar) {
        this.f1385a = uVar;
    }

    public void onClick(View view) {
        int i;
        int i2;
        int i3 = 4;
        int i4 = 0;
        if (!v.b() || (!((u.b) this.f1385a.e.get(0)).c && !((u.b) this.f1385a.e.get(1)).c)) {
            int i5 = (((u.b) this.f1385a.e.get(0)).c ? 1 : 0) | (((u.b) this.f1385a.e.get(1)).c ? 2 : 0);
            if (!((u.b) this.f1385a.e.get(2)).c) {
                i3 = 0;
            }
            i = i5 | i3;
        } else {
            int i6 = ((u.b) this.f1385a.e.get(0)).c ? this.f1385a.j == 0 ? 1 : 32 : 0;
            int i7 = ((u.b) this.f1385a.e.get(1)).c ? this.f1385a.j == 0 ? 64 : 128 : 0;
            if (((u.b) this.f1385a.e.get(1)).c) {
                i2 = 2;
            } else {
                i2 = 0;
            }
            if (((u.b) this.f1385a.e.get(2)).c) {
                i4 = 4;
            }
            i = i6 | i7 | i2 | i4;
        }
        i.a(new x(this, i));
        this.f1385a.dismiss();
    }
}
