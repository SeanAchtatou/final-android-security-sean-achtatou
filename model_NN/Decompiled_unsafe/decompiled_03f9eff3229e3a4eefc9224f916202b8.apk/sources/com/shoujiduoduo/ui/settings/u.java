package com.shoujiduoduo.ui.settings;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.util.v;
import com.shoujiduoduo.util.widget.a;
import java.util.ArrayList;

/* compiled from: SetRingDialog */
public class u extends Dialog {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Context f1381a;
    private ListView b;
    private Button c;
    private Button d;
    /* access modifiers changed from: private */
    public ArrayList<b> e;
    /* access modifiers changed from: private */
    public a f;
    /* access modifiers changed from: private */
    public RingData g;
    /* access modifiers changed from: private */
    public String h;
    /* access modifiers changed from: private */
    public String i;
    /* access modifiers changed from: private */
    public int j = 0;
    private AdapterView.OnItemClickListener k = new y(this);

    public u(Context context, int i2, RingData ringData, String str, String str2) {
        super(context, i2);
        this.f1381a = context;
        this.g = ringData;
        this.h = str;
        this.i = str2;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.dialog_set_ring);
        this.b = (ListView) findViewById(R.id.set_ring_action_list);
        this.b.setItemsCanFocus(false);
        this.b.setChoiceMode(2);
        this.b.setOnItemClickListener(this.k);
        this.c = (Button) findViewById(R.id.set_ring_cancel);
        this.f = new a(this, null);
        this.c.setOnClickListener(new v(this));
        this.d = (Button) findViewById(R.id.set_ring__ok);
        this.d.setOnClickListener(new w(this));
        setCanceledOnTouchOutside(true);
        this.e = new ArrayList<>();
        this.e.add(new b(this.f1381a.getResources().getString(R.string.set_ring_incoming_call), true, R.drawable.icon_ring_call));
        this.e.add(new b(this.f1381a.getResources().getString(R.string.set_ring_message), false, R.drawable.icon_ring_sms));
        this.e.add(new b(this.f1381a.getResources().getString(R.string.set_ring_alarm), false, R.drawable.icon_ring_alarm));
        this.e.add(new b(this.f1381a.getResources().getString(R.string.set_ring_contact), false, R.drawable.icon_ring_contact));
        this.b.setAdapter((ListAdapter) this.f);
    }

    /* compiled from: SetRingDialog */
    private class b {
        /* access modifiers changed from: private */
        public String b;
        /* access modifiers changed from: private */
        public boolean c;
        /* access modifiers changed from: private */
        public int d;

        public b(String str, boolean z, int i) {
            this.b = str;
            this.c = z;
            this.d = i;
        }
    }

    /* access modifiers changed from: private */
    public void a(int i2) {
        if ((i2 == 0 || i2 == 1) && v.b() && v.c()) {
            new AlertDialog.Builder(this.f1381a).setTitle("请选择sim卡").setSingleChoiceItems(new String[]{"sim卡1", "sim卡2"}, 0, new z(this)).show();
        }
    }

    /* access modifiers changed from: private */
    public void a() {
        new a.C0034a(this.f1381a).b((int) R.string.hint).a((int) R.string.buy_cailing_confirm).a((int) R.string.ok, new ab(this)).b((int) R.string.cancel, new aa(this)).a().show();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        PlayerService.a(true);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        PlayerService.a(false);
    }

    /* compiled from: SetRingDialog */
    private class a extends BaseAdapter {

        /* renamed from: a  reason: collision with root package name */
        Html.ImageGetter f1382a;

        private a() {
            this.f1382a = new ac(this);
        }

        /* synthetic */ a(u uVar, v vVar) {
            this();
        }

        public int getCount() {
            return u.this.e.size();
        }

        public Object getItem(int i) {
            return u.this.e.get(i);
        }

        public long getItemId(int i) {
            return (long) i;
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            View inflate = u.this.getLayoutInflater().inflate((int) R.layout.listitem_set_ring, (ViewGroup) null);
            TextView textView = (TextView) inflate.findViewById(R.id.ringtype_desc);
            CheckBox checkBox = (CheckBox) inflate.findViewById(R.id.checkbox);
            ImageView imageView = (ImageView) inflate.findViewById(R.id.ringtype_icon);
            checkBox.setOnCheckedChangeListener(new ad(this, i));
            if (i == u.this.e.size() - 1) {
                checkBox.setBackgroundResource(R.drawable.btn_right_arrow);
            }
            if (((b) u.this.e.get(i)).c) {
                checkBox.setChecked(true);
            } else {
                checkBox.setChecked(false);
            }
            if (i == u.this.e.size() - 2 && u.this.e.size() == 5) {
                textView.setText(Html.fromHtml(((b) u.this.e.get(i)).b + " " + "<img src=\"" + ((int) R.drawable.icon_cmcc_small) + "\" align='center'/>", this.f1382a, null));
            } else {
                textView.setText(((b) u.this.e.get(i)).b);
            }
            imageView.setImageResource(((b) u.this.e.get(i)).d);
            return inflate;
        }
    }
}
