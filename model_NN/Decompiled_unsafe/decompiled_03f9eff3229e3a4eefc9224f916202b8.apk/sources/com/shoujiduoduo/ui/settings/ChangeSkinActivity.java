package com.shoujiduoduo.ui.settings;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ui.utils.BaseActivity;
import com.shoujiduoduo.ui.utils.v;
import com.shoujiduoduo.util.as;
import com.shoujiduoduo.util.f;
import com.shoujiduoduo.util.i;
import com.shoujiduoduo.util.l;
import com.shoujiduoduo.util.w;
import com.sina.weibo.sdk.component.ShareRequestParam;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ChangeSkinActivity extends BaseActivity implements View.OnClickListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public GridView f1341a;
    /* access modifiers changed from: private */
    public RelativeLayout b;
    private d c;
    /* access modifiers changed from: private */
    public ArrayList<c> d;
    /* access modifiers changed from: private */
    public String e = "pref_cur_url";
    private String f = null;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_change_skin);
        findViewById(R.id.back).setOnClickListener(this);
        findViewById(R.id.btn_camera).setOnClickListener(this);
        findViewById(R.id.btn_album).setOnClickListener(this);
        this.f1341a = (GridView) findViewById(R.id.skin_gridview);
        this.b = (RelativeLayout) findViewById(R.id.loading);
        this.f1341a.setVisibility(4);
        this.b.setVisibility(0);
        this.c = new d(this, null);
        this.d = new ArrayList<>();
        this.f1341a.setOnItemClickListener(new b(this));
        i.a(new b(this.c));
    }

    private class d extends Handler {
        private d() {
        }

        /* synthetic */ d(ChangeSkinActivity changeSkinActivity, b bVar) {
            this();
        }

        public void handleMessage(Message message) {
            super.handleMessage(message);
            ArrayList unused = ChangeSkinActivity.this.d = (ArrayList) message.obj;
            ChangeSkinActivity.this.d.add(0, new c("default", "default"));
            ChangeSkinActivity.this.f1341a.setAdapter((ListAdapter) new a(ChangeSkinActivity.this, null));
            ChangeSkinActivity.this.f1341a.setVisibility(0);
            ChangeSkinActivity.this.b.setVisibility(4);
        }
    }

    private class b implements Runnable {
        private Handler b;

        public b(Handler handler) {
            this.b = handler;
        }

        public void run() {
            byte[] a2 = w.a("&type=vipskins", "");
            if (a2 != null) {
                com.shoujiduoduo.base.a.a.a("changeskin", new String(a2));
                this.b.sendMessage(this.b.obtainMessage(1, ChangeSkinActivity.this.a(new ByteArrayInputStream(a2))));
            }
        }
    }

    private class c {

        /* renamed from: a  reason: collision with root package name */
        public String f1344a;
        public String b;

        public c() {
        }

        public c(String str, String str2) {
            this.f1344a = str;
            this.b = str2;
        }
    }

    /* access modifiers changed from: private */
    public ArrayList<c> a(InputStream inputStream) {
        DocumentBuilder newDocumentBuilder;
        Document parse;
        Element documentElement;
        try {
            DocumentBuilderFactory newInstance = DocumentBuilderFactory.newInstance();
            if (newInstance == null || (newDocumentBuilder = newInstance.newDocumentBuilder()) == null || inputStream == null || (parse = newDocumentBuilder.parse(inputStream)) == null || (documentElement = parse.getDocumentElement()) == null) {
                return null;
            }
            documentElement.getAttribute("hasmore").equalsIgnoreCase("true");
            NodeList elementsByTagName = documentElement.getElementsByTagName("skin");
            if (elementsByTagName == null) {
                return null;
            }
            ArrayList<c> arrayList = new ArrayList<>();
            for (int i = 0; i < elementsByTagName.getLength(); i++) {
                NamedNodeMap attributes = elementsByTagName.item(i).getAttributes();
                c cVar = new c();
                cVar.f1344a = f.a(attributes, "img_b");
                cVar.b = f.a(attributes, "img_s");
                arrayList.add(cVar);
            }
            return arrayList;
        } catch (IOException e2) {
            e2.printStackTrace();
            return null;
        } catch (DOMException e3) {
            e3.printStackTrace();
            return null;
        } catch (SAXException e4) {
            e4.printStackTrace();
            return null;
        } catch (ParserConfigurationException e5) {
            e5.printStackTrace();
            return null;
        } catch (ArrayIndexOutOfBoundsException e6) {
            e6.printStackTrace();
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.c != null) {
            this.c.removeCallbacksAndMessages(null);
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back:
                finish();
                return;
            case R.id.title:
            case R.id.bottom_banner:
            default:
                return;
            case R.id.btn_camera:
                Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                this.f = String.valueOf(System.currentTimeMillis()) + ".jpg";
                intent.putExtra("output", Uri.fromFile(new File(l.a(6), this.f)));
                if (RingDDApp.b().getApplicationContext().getPackageManager().resolveActivity(intent, 65536) == null) {
                    com.shoujiduoduo.util.widget.f.a("请先安装相机");
                    return;
                } else {
                    startActivityForResult(intent, 2);
                    return;
                }
            case R.id.btn_album:
                Intent intent2 = new Intent("android.intent.action.GET_CONTENT");
                intent2.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
                if (RingDDApp.b().getApplicationContext().getPackageManager().resolveActivity(intent2, 65536) == null) {
                    com.shoujiduoduo.util.widget.f.a("请先安装相册");
                    return;
                } else {
                    startActivityForResult(intent2, 2);
                    return;
                }
        }
    }

    private class a extends BaseAdapter {
        private a() {
        }

        /* synthetic */ a(ChangeSkinActivity changeSkinActivity, b bVar) {
            this();
        }

        public int getCount() {
            if (ChangeSkinActivity.this.d == null) {
                return 0;
            }
            return ChangeSkinActivity.this.d.size();
        }

        public Object getItem(int i) {
            if (ChangeSkinActivity.this.d == null) {
                return 0;
            }
            return ChangeSkinActivity.this.d.get(i);
        }

        public long getItemId(int i) {
            return (long) i;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, ?[OBJECT, ARRAY], int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(int i, View view, ViewGroup viewGroup) {
            boolean z = false;
            if (view == null) {
                view = LayoutInflater.from(ChangeSkinActivity.this).inflate((int) R.layout.griditem_skin, (ViewGroup) null, false);
            }
            ImageView imageView = (ImageView) view.findViewById(R.id.skin);
            CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkbox);
            String a2 = as.a(RingDDApp.c(), ChangeSkinActivity.this.e, "default");
            if (i == 0) {
                imageView.setImageResource(R.drawable.skin_default);
            } else {
                com.d.a.b.d.a().a(((c) ChangeSkinActivity.this.d.get(i)).b, imageView, v.a().h());
            }
            checkBox.setVisibility(a2.equals(((c) ChangeSkinActivity.this.d.get(i)).f1344a) ? 0 : 4);
            if (a2.equals(((c) ChangeSkinActivity.this.d.get(i)).f1344a)) {
                z = true;
            }
            checkBox.setChecked(z);
            return view;
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        Bundle extras;
        Uri fromFile;
        super.onActivityResult(i, i2, intent);
        if (i2 == -1) {
            switch (i) {
                case 2:
                    if (intent != null) {
                        fromFile = intent.getData();
                    } else if (this.f != null) {
                        fromFile = Uri.fromFile(new File(l.a(6), this.f));
                    } else {
                        return;
                    }
                    DisplayMetrics displayMetrics = new DisplayMetrics();
                    getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                    if (fromFile == null) {
                        com.shoujiduoduo.util.widget.f.a("相机未提供图片,换个相机试试");
                        return;
                    } else {
                        a(fromFile, (displayMetrics.widthPixels * 3) / 4, (displayMetrics.heightPixels * 3) / 4, 3);
                        return;
                    }
                case 3:
                    Bitmap bitmap = null;
                    Uri data = intent.getData();
                    if (data != null) {
                        bitmap = BitmapFactory.decodeFile(data.getPath());
                    }
                    if (bitmap == null && (extras = intent.getExtras()) != null) {
                        bitmap = (Bitmap) extras.get(ShareRequestParam.RESP_UPLOAD_PIC_PARAM_DATA);
                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                        try {
                            byteArrayOutputStream.close();
                        } catch (IOException e2) {
                            e2.printStackTrace();
                        }
                    }
                    if (bitmap != null) {
                        a(bitmap);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    private void a(Bitmap bitmap) {
        String str = l.a(7) + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + ".jpg";
        a(bitmap, str);
        as.c(this, "cur_splash_pic", str);
    }

    /* access modifiers changed from: package-private */
    public boolean a(Bitmap bitmap, String str) {
        File file = new File(str);
        try {
            file.createNewFile();
            try {
                FileOutputStream fileOutputStream = new FileOutputStream(file);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
                try {
                    fileOutputStream.flush();
                    try {
                        fileOutputStream.close();
                        return true;
                    } catch (IOException e2) {
                        return false;
                    }
                } catch (IOException e3) {
                    return false;
                }
            } catch (FileNotFoundException e4) {
                return false;
            }
        } catch (IOException e5) {
            return false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void a(Uri uri, int i, int i2, int i3) {
        Intent intent = new Intent(this, CropImageActivity.class);
        intent.setDataAndType(uri, "image/*");
        intent.putExtra("crop", "true");
        intent.putExtra("aspectX", i);
        intent.putExtra("aspectY", i2);
        intent.putExtra("outputX", i);
        intent.putExtra("outputY", i2);
        intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
        intent.putExtra("noFaceDetection", true);
        intent.putExtra("return-data", false);
        startActivityForResult(intent, i3);
    }
}
