package com.shoujiduoduo.ui.settings;

import android.widget.CompoundButton;
import com.shoujiduoduo.ui.settings.u;

/* compiled from: SetRingDialog */
class ad implements CompoundButton.OnCheckedChangeListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f1356a;
    final /* synthetic */ u.a b;

    ad(u.a aVar, int i) {
        this.b = aVar;
        this.f1356a = i;
    }

    public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
        if (this.f1356a == u.this.e.size() - 2 && u.this.e.size() == 5 && z && !((u.b) u.this.e.get(this.f1356a)).c) {
            u.this.a();
        }
        if ((this.f1356a == 1 || this.f1356a == 0) && z && !((u.b) u.this.e.get(this.f1356a)).c) {
            u.this.a(this.f1356a);
        }
        boolean unused = ((u.b) u.this.e.get(this.f1356a)).c = z;
    }
}
