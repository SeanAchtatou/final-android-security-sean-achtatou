package com.shoujiduoduo.ui.settings;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.ringtone.R;
import com.umeng.analytics.b;

/* compiled from: SetRingDialog */
class y implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ u f1387a;

    y(u uVar) {
        this.f1387a = uVar;
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkbox);
        if (checkBox.isChecked()) {
            a.a("SetRingDialog", "onItemClick:" + i + "set to unchecked");
            checkBox.setChecked(false);
        } else {
            a.a("SetRingDialog", "onItemClick:" + i + "set to checked");
            checkBox.setChecked(true);
        }
        if (i == this.f1387a.e.size() - 1) {
            b.b(this.f1387a.f1381a, "SET_CONTACT_RING");
            Intent intent = new Intent(this.f1387a.f1381a, ContactRingSettingActivity.class);
            Bundle bundle = new Bundle();
            bundle.putParcelable("ringdata", this.f1387a.g);
            intent.putExtras(bundle);
            intent.putExtra("listid", this.f1387a.h);
            intent.putExtra("listtype", this.f1387a.i);
            this.f1387a.f1381a.startActivity(intent);
            this.f1387a.dismiss();
        }
    }
}
