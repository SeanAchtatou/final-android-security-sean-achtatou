package com.shoujiduoduo.ui.settings;

import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import com.shoujiduoduo.ringtone.R;

/* compiled from: ContactRingSettingActivity */
class g implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ContactRingSettingActivity f1368a;

    g(ContactRingSettingActivity contactRingSettingActivity) {
        this.f1368a = contactRingSettingActivity;
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        CheckBox checkBox = (CheckBox) view.findViewById(R.id.contact_item_check);
        if (checkBox.isChecked()) {
            checkBox.setChecked(false);
        } else {
            checkBox.setChecked(true);
        }
    }
}
