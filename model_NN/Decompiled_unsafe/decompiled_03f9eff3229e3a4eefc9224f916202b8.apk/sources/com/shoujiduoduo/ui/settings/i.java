package com.shoujiduoduo.ui.settings;

import android.content.ContentValues;
import android.view.View;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ui.settings.ContactRingSettingActivity;

/* compiled from: ContactRingSettingActivity */
class i implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f1370a;
    final /* synthetic */ ContentValues b;
    final /* synthetic */ ContactRingSettingActivity.d c;
    final /* synthetic */ ContactRingSettingActivity.a d;

    i(ContactRingSettingActivity.a aVar, String str, ContentValues contentValues, ContactRingSettingActivity.d dVar) {
        this.d = aVar;
        this.f1370a = str;
        this.b = contentValues;
        this.c = dVar;
    }

    public void onClick(View view) {
        ContactRingSettingActivity.this.f1346a.put(this.f1370a, 0);
        this.b.put("user_set", "1");
        this.c.c.setText((int) R.string.default_ring);
        this.c.e.setVisibility(4);
        this.c.f.setVisibility(0);
    }
}
