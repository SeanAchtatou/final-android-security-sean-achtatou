package com.shoujiduoduo.ui.settings;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ui.utils.CropImageView;
import com.shoujiduoduo.util.i;
import com.shoujiduoduo.util.l;
import java.io.File;

public class CropImageActivity extends Activity implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    Uri f1351a;
    Uri b;
    int c;
    int d;
    boolean e;
    int f;
    int g;
    CropImageView h;
    RelativeLayout i;
    View j;
    View k;
    LinearLayout l;
    ImageButton m;
    ImageButton n;
    ImageButton o;
    ImageButton p;

    private void a(Intent intent) {
        if (intent != null) {
            this.f1351a = intent.getData();
            this.c = intent.getIntExtra("outputX", (int) (((float) this.f) * 0.8f));
            this.d = intent.getIntExtra("outputY", (int) (((float) this.g) * 0.8f));
            this.e = intent.getBooleanExtra("rotateEnable", false);
            a.a("CropImageActivity", "outWidth=" + this.c + " , outHeight=" + this.d);
            a(this.f1351a);
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v3, resolved type: android.graphics.Bitmap} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v5, resolved type: java.lang.String} */
    /* JADX WARN: Type inference failed for: r3v0 */
    /* JADX WARN: Type inference failed for: r3v6 */
    /* JADX WARN: Type inference failed for: r3v9 */
    /* JADX WARN: Type inference failed for: r3v10 */
    /* JADX WARN: Type inference failed for: r3v11 */
    /* JADX WARN: Type inference failed for: r3v12 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(android.net.Uri r11) {
        /*
            r10 = this;
            r9 = 0
            r8 = 1149239296(0x44800000, float:1024.0)
            r4 = 4607632778762754458(0x3ff199999999999a, double:1.1)
            r3 = 0
            if (r11 == 0) goto L_0x00ad
            int r0 = r10.f
            double r0 = (double) r0
            double r0 = r0 * r4
            int r6 = (int) r0
            int r0 = r10.g
            double r0 = (double) r0
            double r0 = r0 * r4
            int r7 = (int) r0
            java.lang.String r0 = "content"
            java.lang.String r1 = r11.getScheme()
            boolean r0 = r0.equalsIgnoreCase(r1)
            if (r0 == 0) goto L_0x00bb
            android.content.ContentResolver r0 = r10.getContentResolver()
            if (r0 == 0) goto L_0x0055
            r1 = 1
            java.lang.String[] r2 = new java.lang.String[r1]
            java.lang.String r1 = "_data"
            r2[r9] = r1
            r1 = r11
            r4 = r3
            r5 = r3
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5)
            if (r1 == 0) goto L_0x0055
            java.lang.String r0 = "_data"
            int r0 = r1.getColumnIndexOrThrow(r0)     // Catch:{ Exception -> 0x00ae }
            r1.moveToFirst()     // Catch:{ Exception -> 0x00ae }
            java.lang.String r3 = r1.getString(r0)     // Catch:{ Exception -> 0x00ae }
            r1.close()
        L_0x0047:
            android.graphics.Bitmap r0 = r10.a(r3, r6, r7)
            if (r0 != 0) goto L_0x00e8
            int r0 = r10.c
            int r1 = r10.d
            android.graphics.Bitmap r3 = r10.a(r3, r0, r1)
        L_0x0055:
            if (r3 == 0) goto L_0x00d2
            java.lang.String r0 = "CropImageActivity"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            int r2 = r3.getWidth()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = " * "
            java.lang.StringBuilder r1 = r1.append(r2)
            int r2 = r3.getHeight()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = " size==="
            java.lang.StringBuilder r1 = r1.append(r2)
            int r2 = r3.getRowBytes()
            int r4 = r3.getHeight()
            int r2 = r2 * r4
            float r2 = (float) r2
            float r2 = r2 / r8
            float r2 = r2 / r8
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = "MB"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            com.shoujiduoduo.base.a.a.c(r0, r1)
            com.shoujiduoduo.ui.utils.CropImageView r0 = r10.h
            int r1 = r10.c
            int r2 = r10.d
            int r4 = r3.getWidth()
            int r5 = r3.getHeight()
            r0.a(r1, r2, r4, r5)
            com.shoujiduoduo.ui.utils.CropImageView r0 = r10.h
            r0.setImageBitmap(r3)
        L_0x00ad:
            return
        L_0x00ae:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x00b6 }
            r1.close()
            goto L_0x0047
        L_0x00b6:
            r0 = move-exception
            r1.close()
            throw r0
        L_0x00bb:
            java.lang.String r0 = r11.getPath()
            android.graphics.Bitmap r3 = r10.a(r0, r6, r7)
            if (r3 != 0) goto L_0x0055
            java.lang.String r0 = r11.getPath()
            int r1 = r10.c
            int r2 = r10.d
            android.graphics.Bitmap r3 = r10.a(r0, r1, r2)
            goto L_0x0055
        L_0x00d2:
            android.view.View r0 = r10.j
            r0.setEnabled(r9)
            com.shoujiduoduo.ui.utils.CropImageView r0 = r10.h
            android.content.res.Resources r1 = r10.getResources()
            r2 = 2131034588(0x7f0501dc, float:1.7679698E38)
            java.lang.String r1 = r1.getString(r2)
            r0.setErrorHint(r1)
            goto L_0x00ad
        L_0x00e8:
            r3 = r0
            goto L_0x0055
        */
        throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.ui.settings.CropImageActivity.a(android.net.Uri):void");
    }

    /* access modifiers changed from: private */
    public Uri a() {
        String str;
        File a2;
        byte[] cropImage = this.h.getCropImage();
        if (cropImage == null || (a2 = a(cropImage, (str = l.a(6) + "skin_" + String.valueOf(System.currentTimeMillis()) + ".png"))) == null || !a2.exists()) {
            return null;
        }
        a.a("CropImageActivity", "saveCropBitmap-->" + str);
        return Uri.fromFile(a2);
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0026 A[SYNTHETIC, Splitter:B:18:0x0026] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0032 A[SYNTHETIC, Splitter:B:24:0x0032] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.io.File a(byte[] r5, java.lang.String r6) {
        /*
            r4 = this;
            r2 = 0
            java.io.File r0 = new java.io.File     // Catch:{ Exception -> 0x001e }
            r0.<init>(r6)     // Catch:{ Exception -> 0x001e }
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x003e }
            r1.<init>(r0)     // Catch:{ Exception -> 0x003e }
            java.io.BufferedOutputStream r3 = new java.io.BufferedOutputStream     // Catch:{ Exception -> 0x003e }
            r3.<init>(r1)     // Catch:{ Exception -> 0x003e }
            r3.write(r5)     // Catch:{ Exception -> 0x0040, all -> 0x003b }
            if (r3 == 0) goto L_0x0018
            r3.close()     // Catch:{ IOException -> 0x0019 }
        L_0x0018:
            return r0
        L_0x0019:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0018
        L_0x001e:
            r0 = move-exception
            r1 = r0
            r0 = r2
        L_0x0021:
            r1.printStackTrace()     // Catch:{ all -> 0x002f }
            if (r2 == 0) goto L_0x0018
            r2.close()     // Catch:{ IOException -> 0x002a }
            goto L_0x0018
        L_0x002a:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0018
        L_0x002f:
            r0 = move-exception
        L_0x0030:
            if (r2 == 0) goto L_0x0035
            r2.close()     // Catch:{ IOException -> 0x0036 }
        L_0x0035:
            throw r0
        L_0x0036:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0035
        L_0x003b:
            r0 = move-exception
            r2 = r3
            goto L_0x0030
        L_0x003e:
            r1 = move-exception
            goto L_0x0021
        L_0x0040:
            r1 = move-exception
            r2 = r3
            goto L_0x0021
        */
        throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.ui.settings.CropImageActivity.a(byte[], java.lang.String):java.io.File");
    }

    public Bitmap a(String str, int i2, int i3) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(str, options);
            options.inSampleSize = a(options, i2, i3);
            if (options.outWidth > options.outHeight) {
                options.inSampleSize = a(options, i3, i2);
            }
            options.inJustDecodeBounds = false;
            return BitmapFactory.decodeFile(str, options);
        } catch (Exception e2) {
            System.gc();
            return null;
        }
    }

    public int a(BitmapFactory.Options options, int i2, int i3) {
        int i4 = options.outHeight;
        int i5 = options.outWidth;
        int i6 = 1;
        if (i4 > i3 || i5 > i2) {
            if (i5 > i4) {
                i6 = Math.round(((float) i4) / ((float) i3));
            } else {
                i6 = Math.round(((float) i5) / ((float) i2));
            }
        }
        a.a("CropImageActivity", i5 + "," + i4 + "--->" + i2 + "," + i3 + " calculateInSampleSize-->" + i6);
        return i6;
    }

    private void b() {
        this.h = (CropImageView) findViewById(R.id.cropimg_imageview);
        this.i = (RelativeLayout) findViewById(R.id.cropimg_progresslayout);
        this.l = (LinearLayout) findViewById(R.id.croping_rotatelayout);
        this.j = findViewById(R.id.cropimg_okbtn);
        this.k = findViewById(R.id.cropimg_cancelbtn);
        this.j.setOnClickListener(this);
        this.k.setOnClickListener(this);
        this.m = (ImageButton) findViewById(R.id.cropimg_zoomout);
        this.n = (ImageButton) findViewById(R.id.cropimg_zoomin);
        this.o = (ImageButton) findViewById(R.id.cropimg_rotateleft);
        this.p = (ImageButton) findViewById(R.id.cropimg_rotateright);
        this.m.setOnClickListener(this);
        this.n.setOnClickListener(this);
        this.o.setOnClickListener(this);
        this.p.setOnClickListener(this);
        try {
            this.j.setBackgroundResource(R.drawable.xml_btn_cropimg_save);
            this.k.setBackgroundResource(R.drawable.xml_btn_cropimg_cancel);
            this.m.setImageResource(R.drawable.btn_crop_zoomout);
            this.n.setImageResource(R.drawable.btn_crop_zoomin);
            this.o.setImageResource(R.drawable.btn_crop_rotate_left);
            this.p.setImageResource(R.drawable.btn_crop_rotate_right);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    @TargetApi(11)
    public void onCreate(Bundle bundle) {
        requestWindowFeature(1);
        if (Build.VERSION.SDK_INT < 11) {
            getWindow().setFlags(1024, 1024);
        }
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_cropimage);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        this.f = displayMetrics.widthPixels;
        this.g = displayMetrics.heightPixels;
        b();
        a(getIntent());
        if (Build.VERSION.SDK_INT >= 11) {
            this.h.setSystemUiVisibility(4);
        }
        if (this.e) {
            this.l.setVisibility(0);
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        this.j.setEnabled(true);
        this.i.setVisibility(8);
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        a(intent);
        super.onNewIntent(intent);
    }

    /* access modifiers changed from: protected */
    @TargetApi(11)
    public void onDestroy() {
        this.h.setImageBitmap(null);
        if (Build.VERSION.SDK_INT >= 11) {
            this.h.setSystemUiVisibility(0);
        }
        super.onDestroy();
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cropimg_okbtn:
                this.j.setEnabled(false);
                c();
                return;
            case R.id.cropimg_cancelbtn:
                setResult(0);
                finish();
                return;
            case R.id.croping_rotatelayout:
            default:
                return;
            case R.id.cropimg_zoomout:
                b(true);
                return;
            case R.id.cropimg_zoomin:
                b(false);
                return;
            case R.id.cropimg_rotateleft:
                a(true);
                return;
            case R.id.cropimg_rotateright:
                a(false);
                return;
        }
    }

    private void a(boolean z) {
        this.h.setImageRotate(z);
    }

    private void b(boolean z) {
        this.h.setImageZoom(z);
    }

    private void c() {
        this.i.setVisibility(0);
        i.a(new k(this));
    }
}
