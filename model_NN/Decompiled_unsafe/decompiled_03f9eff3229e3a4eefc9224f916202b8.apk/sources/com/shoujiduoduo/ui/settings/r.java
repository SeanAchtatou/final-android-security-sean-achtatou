package com.shoujiduoduo.ui.settings;

import com.duoduo.mobads.IBaiduNativeNetworkListener;
import com.duoduo.mobads.INativeErrorCode;
import com.duoduo.mobads.INativeResponse;
import com.shoujiduoduo.base.a.a;
import java.util.List;

/* compiled from: QuitDialog */
class r implements IBaiduNativeNetworkListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ m f1378a;

    r(m mVar) {
        this.f1378a = mVar;
    }

    public void onNativeFail(INativeErrorCode iNativeErrorCode) {
        a.e("QuitDialog", "baidu feed, onNativeFail reason:" + iNativeErrorCode.getErrorCode());
    }

    public void onNativeLoad(List<INativeResponse> list) {
        if (list.size() > 0) {
            a.a("QuitDialog", "baidu feed, onNativeLoad, ad size:" + list.size());
            long unused = this.f1378a.t = System.currentTimeMillis();
            List unused2 = this.f1378a.o = list;
            return;
        }
        a.a("QuitDialog", "baidu feed, onNativeLoad, ad size is 0");
    }
}
