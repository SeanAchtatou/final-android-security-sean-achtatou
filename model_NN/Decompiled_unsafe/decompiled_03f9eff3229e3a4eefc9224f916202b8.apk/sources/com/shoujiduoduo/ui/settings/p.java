package com.shoujiduoduo.ui.settings;

import com.alimama.mobile.sdk.config.FeedController;
import com.shoujiduoduo.base.a.a;

/* compiled from: QuitDialog */
class p implements FeedController.IncubatedListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ m f1376a;

    p(m mVar) {
        this.f1376a = mVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.ui.settings.m.a(com.shoujiduoduo.ui.settings.m, boolean):boolean
     arg types: [com.shoujiduoduo.ui.settings.m, int]
     candidates:
      com.shoujiduoduo.ui.settings.m.a(com.shoujiduoduo.ui.settings.m, long):long
      com.shoujiduoduo.ui.settings.m.a(com.shoujiduoduo.ui.settings.m, java.util.List):java.util.List
      com.shoujiduoduo.ui.settings.m.a(com.shoujiduoduo.ui.settings.m, boolean):boolean */
    public void onComplete(int i, String str) {
        if (i == 0) {
            try {
                boolean unused = this.f1376a.l = false;
                a.a("Feeds", String.format("推广位[%s]获取数据失败，将无法获取Feed.", str));
            } catch (Exception e) {
            }
        } else if (1 == i) {
            boolean unused2 = this.f1376a.l = true;
            a.a("Feeds", String.format("推广位[%s]的实时数据已经准备好", str));
        } else if (2 == i) {
            boolean unused3 = this.f1376a.l = false;
            a.a("Feeds", String.format("推广位[%s]的缓存数据已经准备好", str));
        }
    }

    public void onClick() {
    }
}
