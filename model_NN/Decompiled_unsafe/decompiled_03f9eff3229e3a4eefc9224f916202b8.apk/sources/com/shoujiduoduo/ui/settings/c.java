package com.shoujiduoduo.ui.settings;

import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.util.as;
import com.shoujiduoduo.util.t;
import com.shoujiduoduo.util.w;
import com.shoujiduoduo.util.widget.f;

/* compiled from: ChangeSkinActivity */
class c implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f1364a;
    final /* synthetic */ String b;
    final /* synthetic */ b c;

    c(b bVar, String str, String str2) {
        this.c = bVar;
        this.f1364a = str;
        this.b = str2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.util.w.a(java.lang.String, java.lang.String, boolean):boolean
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.shoujiduoduo.util.w.a(java.lang.String, java.lang.String, java.lang.String):void
      com.shoujiduoduo.util.w.a(int, int, int):byte[]
      com.shoujiduoduo.util.w.a(java.lang.String, boolean, java.lang.String):byte[]
      com.shoujiduoduo.util.w.a(java.lang.String, java.lang.String, boolean):boolean */
    public void run() {
        String str = this.f1364a + ".tmp";
        if (w.a(this.b, this.f1364a, true)) {
            f.a("个性启动图设置成功");
            t.a(str, this.f1364a);
            as.c(RingDDApp.c(), "cur_splash_pic", this.f1364a);
            return;
        }
        f.a("个性启动图下载失败，请稍候重试");
    }
}
