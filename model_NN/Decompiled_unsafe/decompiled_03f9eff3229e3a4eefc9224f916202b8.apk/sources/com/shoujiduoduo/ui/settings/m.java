package com.shoujiduoduo.ui.settings;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.alimama.mobile.sdk.MmuSDK;
import com.alimama.mobile.sdk.config.FeedController;
import com.alimama.mobile.sdk.config.FeedProperties;
import com.alimama.mobile.sdk.config.MmuSDKFactory;
import com.baidu.mobad.feeds.BaiduNative;
import com.baidu.mobad.feeds.NativeResponse;
import com.baidu.mobad.feeds.RequestParameters;
import com.d.a.b.d;
import com.duoduo.dynamicdex.DuoMobAdUtils;
import com.duoduo.mobads.IBaiduNative;
import com.duoduo.mobads.INativeResponse;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ui.utils.v;
import com.umeng.analytics.b;
import java.util.List;

/* compiled from: QuitDialog */
public class m extends Dialog {

    /* renamed from: a  reason: collision with root package name */
    private Button f1373a;
    private Button b;
    private Context c;
    private TextView d;
    private TextView e;
    private String f;
    private boolean g;
    private FeedController h;
    private FeedController.MMFeed i;
    private RelativeLayout j;
    private MmuSDK k;
    /* access modifiers changed from: private */
    public boolean l;
    private String m = "65412";
    private String n = "2010241";
    /* access modifiers changed from: private */
    public List<INativeResponse> o;
    /* access modifiers changed from: private */
    public List<NativeResponse> p;
    private ImageView q;
    private int r;
    /* access modifiers changed from: private */
    public long s;
    /* access modifiers changed from: private */
    public long t;
    private FeedController.IncubatedListener u = new p(this);

    public m(Context context, int i2) {
        super(context, i2);
        this.c = context;
        this.f = b.c(getContext(), "quit_ad_type2");
        if (TextUtils.isEmpty(this.f)) {
            this.f = "baidu";
        }
        a.a("QuitDialog", "quit ad type:" + this.f);
        this.g = !"false".equals(b.c(getContext(), "quit_ad_switch"));
        a.a("QuitDialog", "quit ad switch:" + this.g);
        if (!this.g || !com.shoujiduoduo.util.a.c()) {
            this.g = false;
        } else {
            a();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        a.a("QuitDialog", "onCreate");
        setContentView((int) R.layout.dialog_quit_app);
        this.f1373a = (Button) findViewById(R.id.ok);
        this.b = (Button) findViewById(R.id.cancel);
        this.d = (TextView) findViewById(R.id.tips);
        this.q = (ImageView) findViewById(R.id.ad_image);
        this.e = (TextView) findViewById(R.id.ad_hint);
        this.b.setOnClickListener(new n(this));
        this.f1373a.setOnClickListener(new o(this));
    }

    private void a() {
        if (this.f.equals("baidu")) {
            c();
        } else if (this.f.equals("taobao")) {
            b();
        } else if (this.f.equals("duomob")) {
            d();
        } else {
            a.e("QuitDialog", "not support");
        }
    }

    private void b() {
        a.a("QuitDialog", "init taobao feed");
        this.k = MmuSDKFactory.getMmuSDK();
        this.k.init(RingDDApp.b());
        this.h = FeedProperties.getMmuController();
        if (this.h != null) {
            this.h.setIncubatedListener(this.u);
            this.k.attach(new FeedProperties(this.m));
            return;
        }
        a.c("QuitDialog", "getMmuController return null");
    }

    private void c() {
        a.a("QuitDialog", "initBaiduFeed");
        new BaiduNative(this.c, this.n, new q(this)).makeRequest(new RequestParameters.Builder().confirmDownloading(true).build());
    }

    private void d() {
        a.a("QuitDialog", "initDomobFeed");
        IBaiduNative nativeAdIns = DuoMobAdUtils.Ins.BaiduIns.getNativeAdIns(this.c, "d7d3402a", "2652479", new r(this));
        if (nativeAdIns != null) {
            nativeAdIns.makeRequest();
        }
    }

    private void e() {
        if (this.g && com.shoujiduoduo.util.a.c()) {
            if (this.f.equals("taobao")) {
                a.a("QuitDialog", "show taobao ad");
                String c2 = b.c(this.c, "taobao_quit_ad_tips");
                if (!TextUtils.isEmpty(c2)) {
                    this.d.setText(c2);
                    this.d.setVisibility(0);
                } else {
                    this.d.setVisibility(8);
                }
                if (this.h != null && this.l) {
                    this.i = this.h.getProduct(this.m);
                    if (this.i == null) {
                        this.d.setVisibility(8);
                        a.a("QuitDialog", "获取信息流失败, id:" + this.m);
                        return;
                    }
                    a.a("QuitDialog", "获取信息流成功, id:" + this.m);
                    this.j = (RelativeLayout) findViewById(R.id.ad_view);
                    View feedView = this.h.getFeedView((Activity) this.c, this.i);
                    if (feedView != null) {
                        this.j.removeAllViews();
                        this.j.addView(feedView);
                        this.j.setTag("FeedView");
                        this.j.setVisibility(0);
                        return;
                    }
                    this.d.setVisibility(8);
                }
            } else if (this.f.equals("duomob")) {
                a.a("QuitDialog", "show duomob ad");
                if (System.currentTimeMillis() - this.t > 1800000) {
                    d();
                } else if (this.o != null && this.o.size() > 0) {
                    this.r++;
                    this.r %= this.o.size();
                    if (this.r < this.o.size()) {
                        INativeResponse iNativeResponse = this.o.get(this.r);
                        a.a("QuitDialog", "current baidu ad index:" + this.r);
                        a.a("QuitDialog", "ad title:" + iNativeResponse.getTitle());
                        a.a("QuitDialog", "ad image:" + iNativeResponse.getImageUrl());
                        this.q.setVisibility(0);
                        d.a().a(iNativeResponse.getImageUrl(), this.q, v.a().b());
                        this.d.setText(iNativeResponse.getTitle());
                        this.d.setVisibility(0);
                        this.e.setVisibility(0);
                        iNativeResponse.recordImpression(this.q);
                        this.q.setOnClickListener(new s(this, iNativeResponse));
                    }
                }
            } else if (this.f.equals("baidu")) {
                a.a("QuitDialog", "show baidu ad");
                if (System.currentTimeMillis() - this.s > 1800000) {
                    c();
                } else if (this.p != null && this.p.size() > 0) {
                    this.r++;
                    this.r %= this.p.size();
                    if (this.r < this.p.size()) {
                        NativeResponse nativeResponse = this.p.get(this.r);
                        a.a("QuitDialog", "current baidu ad index:" + this.r);
                        a.a("QuitDialog", "ad title:" + nativeResponse.getTitle());
                        a.a("QuitDialog", "ad image:" + nativeResponse.getImageUrl());
                        this.q.setVisibility(0);
                        d.a().a(nativeResponse.getImageUrl(), this.q, v.a().b());
                        this.d.setText(nativeResponse.getTitle());
                        this.d.setVisibility(0);
                        this.e.setVisibility(0);
                        nativeResponse.recordImpression(this.q);
                        this.q.setOnClickListener(new t(this, nativeResponse));
                    }
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        a.a("QuitDialog", "onStart");
        e();
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        a.a("QuitDialog", "onStop");
        if (this.g && com.shoujiduoduo.util.a.c()) {
            if (this.f.equals("taobao")) {
                this.l = false;
                this.k.attach(new FeedProperties(this.m));
            } else if (this.f.equals("baidu")) {
                c();
            } else if (this.f.equals("duomob")) {
                d();
            }
        }
        super.onStop();
    }
}
