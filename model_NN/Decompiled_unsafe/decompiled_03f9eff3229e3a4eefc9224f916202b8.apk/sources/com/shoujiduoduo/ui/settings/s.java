package com.shoujiduoduo.ui.settings;

import android.view.View;
import com.duoduo.mobads.INativeResponse;

/* compiled from: QuitDialog */
class s implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ INativeResponse f1379a;
    final /* synthetic */ m b;

    s(m mVar, INativeResponse iNativeResponse) {
        this.b = mVar;
        this.f1379a = iNativeResponse;
    }

    public void onClick(View view) {
        this.f1379a.handleClick(view);
    }
}
