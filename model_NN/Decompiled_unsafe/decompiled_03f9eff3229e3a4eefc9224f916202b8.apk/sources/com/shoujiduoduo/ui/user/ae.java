package com.shoujiduoduo.ui.user;

import com.shoujiduoduo.a.a.b;
import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.base.bean.ab;
import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;

/* compiled from: UserInfoEditActivity */
class ae extends a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ab f1402a;
    final /* synthetic */ UserInfoEditActivity b;

    ae(UserInfoEditActivity userInfoEditActivity, ab abVar) {
        this.b = userInfoEditActivity;
        this.f1402a = abVar;
    }

    public void a(c.b bVar) {
        boolean z = true;
        super.a(bVar);
        if (bVar instanceof c.e) {
            int g = this.f1402a.g();
            if (((c.e) bVar).e()) {
                this.f1402a.b(3);
                if (g == 3) {
                    z = false;
                }
            } else {
                this.f1402a.b(0);
                if (g == 0) {
                    z = false;
                }
            }
            if (z) {
                x.a().a(new af(this));
                x.a().a(b.OBSERVER_VIP, new ag(this));
            }
        }
    }

    public void b(c.b bVar) {
        super.b(bVar);
    }
}
