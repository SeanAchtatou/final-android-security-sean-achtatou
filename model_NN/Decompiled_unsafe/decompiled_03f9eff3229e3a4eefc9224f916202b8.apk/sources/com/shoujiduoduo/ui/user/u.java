package com.shoujiduoduo.ui.user;

import android.content.DialogInterface;
import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.widget.a;

/* compiled from: UserCenterActivity */
class u extends a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ s f1439a;

    u(s sVar) {
        this.f1439a = sVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.ui.user.UserCenterActivity.b(com.shoujiduoduo.ui.user.UserCenterActivity, boolean):void
     arg types: [com.shoujiduoduo.ui.user.UserCenterActivity, int]
     candidates:
      com.shoujiduoduo.ui.user.UserCenterActivity.b(com.shoujiduoduo.ui.user.UserCenterActivity, java.lang.String):void
      com.shoujiduoduo.ui.user.UserCenterActivity.b(com.shoujiduoduo.ui.user.UserCenterActivity, boolean):void */
    public void a(c.b bVar) {
        super.a(bVar);
        this.f1439a.c.c();
        com.shoujiduoduo.base.a.a.a("UserCenterActivity", "成功开通彩铃基础功能, 提示开通会员");
        this.f1439a.c.b(false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.ui.user.UserCenterActivity.b(com.shoujiduoduo.ui.user.UserCenterActivity, boolean):void
     arg types: [com.shoujiduoduo.ui.user.UserCenterActivity, int]
     candidates:
      com.shoujiduoduo.ui.user.UserCenterActivity.b(com.shoujiduoduo.ui.user.UserCenterActivity, java.lang.String):void
      com.shoujiduoduo.ui.user.UserCenterActivity.b(com.shoujiduoduo.ui.user.UserCenterActivity, boolean):void */
    public void b(c.b bVar) {
        super.b(bVar);
        this.f1439a.c.c();
        if (bVar.a().equals("000001") || bVar.a().equals("301000")) {
            this.f1439a.c.b(false);
        } else {
            new a.C0034a(this.f1439a.c).b("设置彩铃").a("为您免费开通彩铃功能失败， 原因：" + bVar.b()).a("确定", (DialogInterface.OnClickListener) null).a().show();
        }
    }
}
