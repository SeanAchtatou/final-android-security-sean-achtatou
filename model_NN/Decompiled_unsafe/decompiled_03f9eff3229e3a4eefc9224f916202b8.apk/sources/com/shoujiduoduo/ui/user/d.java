package com.shoujiduoduo.ui.user;

import android.content.DialogInterface;

/* compiled from: UserCenterActivity */
class d implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ UserCenterActivity f1423a;

    d(UserCenterActivity userCenterActivity) {
        this.f1423a = userCenterActivity;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        this.f1423a.e();
        dialogInterface.dismiss();
        this.f1423a.finish();
    }
}
