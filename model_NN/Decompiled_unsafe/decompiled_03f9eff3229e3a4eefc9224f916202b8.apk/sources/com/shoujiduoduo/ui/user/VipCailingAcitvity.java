package com.shoujiduoduo.ui.user;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import com.shoujiduoduo.b.c.n;
import com.shoujiduoduo.base.bean.f;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ui.utils.BaseFragmentActivity;
import com.shoujiduoduo.ui.utils.DDListFragment;
import com.shoujiduoduo.util.ak;
import com.shoujiduoduo.util.f;
import com.tencent.connect.common.Constants;

public class VipCailingAcitvity extends BaseFragmentActivity {

    /* renamed from: a  reason: collision with root package name */
    private DDListFragment f1397a;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_vip_cailing);
        this.f1397a = new DDListFragment();
        Bundle bundle2 = new Bundle();
        bundle2.putString("adapter_type", "ring_list_adapter");
        this.f1397a.setArguments(bundle2);
        String str = "";
        switch (f.u()) {
            case f1671a:
                str = "20";
                break;
            case cu:
                str = "26";
                break;
            case ct:
                str = Constants.VIA_REPORT_TYPE_QQFAVORITES;
                break;
        }
        this.f1397a.a(new n(f.a.list_ring_recommon, str, false, ""));
        findViewById(R.id.back).setOnClickListener(new bq(this));
        FragmentTransaction beginTransaction = getSupportFragmentManager().beginTransaction();
        beginTransaction.add((int) R.id.list_frag_layout, this.f1397a);
        beginTransaction.commit();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        PlayerService b = ak.a().b();
        if (b != null && b.j()) {
            b.k();
        }
    }
}
