package com.shoujiduoduo.ui.user;

import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.d.b;
import com.shoujiduoduo.util.widget.f;

/* compiled from: UserCenterActivity */
class l extends a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f1431a;
    final /* synthetic */ UserCenterActivity b;

    l(UserCenterActivity userCenterActivity, String str) {
        this.b = userCenterActivity;
        this.f1431a = str;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.ui.user.UserCenterActivity.a(com.shoujiduoduo.ui.user.UserCenterActivity, boolean):void
     arg types: [com.shoujiduoduo.ui.user.UserCenterActivity, int]
     candidates:
      com.shoujiduoduo.ui.user.UserCenterActivity.a(com.shoujiduoduo.ui.user.UserCenterActivity, android.app.ProgressDialog):android.app.ProgressDialog
      com.shoujiduoduo.ui.user.UserCenterActivity.a(com.shoujiduoduo.ui.user.UserCenterActivity, java.lang.String):void
      com.shoujiduoduo.ui.user.UserCenterActivity.a(java.lang.String, boolean):void
      com.shoujiduoduo.ui.user.UserCenterActivity.a(com.shoujiduoduo.ui.user.UserCenterActivity, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.ui.user.UserCenterActivity.b(com.shoujiduoduo.ui.user.UserCenterActivity, boolean):void
     arg types: [com.shoujiduoduo.ui.user.UserCenterActivity, int]
     candidates:
      com.shoujiduoduo.ui.user.UserCenterActivity.b(com.shoujiduoduo.ui.user.UserCenterActivity, java.lang.String):void
      com.shoujiduoduo.ui.user.UserCenterActivity.b(com.shoujiduoduo.ui.user.UserCenterActivity, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.ui.user.UserCenterActivity.c(com.shoujiduoduo.ui.user.UserCenterActivity, boolean):void
     arg types: [com.shoujiduoduo.ui.user.UserCenterActivity, int]
     candidates:
      com.shoujiduoduo.ui.user.UserCenterActivity.c(com.shoujiduoduo.ui.user.UserCenterActivity, java.lang.String):void
      com.shoujiduoduo.ui.user.UserCenterActivity.c(com.shoujiduoduo.ui.user.UserCenterActivity, boolean):void */
    public void a(c.b bVar) {
        super.a(bVar);
        this.b.c();
        if (bVar != null && (bVar instanceof c.d)) {
            c.d dVar = (c.d) bVar;
            if (dVar.d() && (dVar.e() || dVar.f())) {
                this.b.c(true);
                f.a("当前手机号已经是多多VIP会员啦");
            } else if (dVar.d() && !dVar.e() && !dVar.f()) {
                this.b.b(false);
            } else if (!dVar.d() && (dVar.e() || dVar.f())) {
                this.b.c(true);
                if (b.a().a(this.f1431a).equals(b.d.wait_open)) {
                    f.a("正在为您开通彩铃业务，请耐心等待一会儿...");
                } else {
                    this.b.a(true);
                }
            } else if (!dVar.d() && !dVar.e() && !dVar.f()) {
                if (b.a().a(this.f1431a).equals(b.d.wait_open)) {
                    f.a("正在为您开通彩铃业务，请耐心等待一会儿...");
                } else {
                    this.b.a(false);
                }
            }
        }
    }

    public void b(c.b bVar) {
        super.b(bVar);
        this.b.c();
    }
}
