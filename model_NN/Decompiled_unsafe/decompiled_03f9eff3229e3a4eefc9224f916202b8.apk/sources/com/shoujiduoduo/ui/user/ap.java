package com.shoujiduoduo.ui.user;

import android.app.ProgressDialog;

/* compiled from: UserInfoEditActivity */
class ap implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f1405a;
    final /* synthetic */ UserInfoEditActivity b;

    ap(UserInfoEditActivity userInfoEditActivity, String str) {
        this.b = userInfoEditActivity;
        this.f1405a = str;
    }

    public void run() {
        if (this.b.g == null) {
            ProgressDialog unused = this.b.g = new ProgressDialog(this.b);
            this.b.g.setMessage(this.f1405a);
            this.b.g.setIndeterminate(false);
            this.b.g.setCancelable(true);
            this.b.g.setCanceledOnTouchOutside(false);
            if (!this.b.isFinishing()) {
                this.b.g.show();
            }
        }
    }
}
