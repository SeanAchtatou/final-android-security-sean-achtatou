package com.shoujiduoduo.ui.user;

import cn.banshenggua.aichang.room.message.SocketMessage;
import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.base.a.a;
import com.umeng.socialize.UMAuthListener;
import com.umeng.socialize.c.b;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

/* compiled from: UserLoginActivity */
class be implements UMAuthListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ UserLoginActivity f1415a;

    be(UserLoginActivity userLoginActivity) {
        this.f1415a = userLoginActivity;
    }

    public void onComplete(b bVar, int i, Map<String, String> map) {
        String str;
        if (map != null) {
            a.a("UserLoginActivity", "[GetInfoListener]:onComplete" + map.toString());
            if (bVar.equals(b.WEIXIN)) {
                String unused = this.f1415a.q = map.get("nickname") != null ? map.get("nickname").toString() : "";
                str = map.get("headimgurl") != null ? map.get("headimgurl").toString() : "";
            } else if (bVar.equals(b.SINA)) {
                try {
                    JSONObject jSONObject = (JSONObject) new JSONTokener(map.get(SocketMessage.MSG_RESULE_KEY)).nextValue();
                    if (jSONObject != null) {
                        String unused2 = this.f1415a.q = jSONObject.optString("screen_name");
                        str = jSONObject.optString("profile_image_url");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                str = "";
            } else {
                String unused3 = this.f1415a.q = map.get("screen_name") != null ? map.get("screen_name").toString() : "";
                str = map.get("profile_image_url") != null ? map.get("profile_image_url").toString() : "";
            }
            int a2 = this.f1415a.a(bVar);
            x.a().a(new bf(this, str, a2));
            x.a().a(com.shoujiduoduo.a.a.b.OBSERVER_USER_CENTER, new bg(this, a2));
        }
        this.f1415a.finish();
    }

    public void onError(b bVar, int i, Throwable th) {
        a.a("UserLoginActivity", "[GetInfoListener]:onError:" + th.getMessage());
        x.a().a(com.shoujiduoduo.a.a.b.OBSERVER_USER_CENTER, new bh(this, this.f1415a.a(bVar), "" + i));
    }

    public void onCancel(b bVar, int i) {
        a.a("UserLoginActivity", "[GetInfoListener]:onCancel");
    }
}
