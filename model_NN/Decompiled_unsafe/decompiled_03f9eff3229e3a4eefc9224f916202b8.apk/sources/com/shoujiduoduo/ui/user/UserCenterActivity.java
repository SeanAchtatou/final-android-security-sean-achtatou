package com.shoujiduoduo.ui.user;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.d.a.b.d;
import com.shoujiduoduo.a.a.b;
import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.a.c.u;
import com.shoujiduoduo.a.c.w;
import com.shoujiduoduo.base.bean.ab;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ui.cailing.au;
import com.shoujiduoduo.ui.cailing.cb;
import com.shoujiduoduo.ui.cailing.ck;
import com.shoujiduoduo.ui.utils.BaseActivity;
import com.shoujiduoduo.ui.utils.v;
import com.shoujiduoduo.util.av;
import com.shoujiduoduo.util.ax;
import com.shoujiduoduo.util.f;
import com.shoujiduoduo.util.widget.a;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class UserCenterActivity extends BaseActivity implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private final String f1389a = "UserCenterActivity";
    private Button b;
    private ImageView c;
    private TextView d;
    private TextView e;
    /* access modifiers changed from: private */
    public RelativeLayout f;
    private LinearLayout g;
    private View h;
    private ListView i;
    /* access modifiers changed from: private */
    public ArrayList<Map<String, Object>> j;
    /* access modifiers changed from: private */
    public ImageView k;
    private TextView l;
    private f.b m;
    /* access modifiers changed from: private */
    public Handler n = new m(this);
    private ProgressDialog o = null;
    /* access modifiers changed from: private */
    public ProgressDialog p = null;
    private u q = new g(this);
    private w r = new h(this);

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_user_center);
        this.n = new Handler();
        this.c = (ImageView) findViewById(R.id.user_head);
        this.b = (Button) findViewById(R.id.user_center_back);
        this.b.setOnClickListener(this);
        this.h = findViewById(R.id.user_info);
        this.d = (TextView) findViewById(R.id.user_name);
        this.e = (TextView) findViewById(R.id.user_center_title);
        this.i = (ListView) findViewById(R.id.right_list);
        this.f = (RelativeLayout) findViewById(R.id.open_vip);
        this.f.setOnClickListener(this);
        this.l = (TextView) findViewById(R.id.cost_hint);
        this.j = g();
        this.i.setAdapter((ListAdapter) new a(this, null));
        this.k = (ImageView) findViewById(R.id.vip);
        this.g = (LinearLayout) findViewById(R.id.user_phone_layout);
        findViewById(R.id.edit_userinfo).setOnClickListener(this);
        findViewById(R.id.user_logout).setOnClickListener(this);
        switch (f.u()) {
            case f1671a:
                this.l.setText((int) R.string.six_yuan_per_month);
                break;
            case cu:
                this.l.setText((int) R.string.five_yuan_per_month);
                break;
            case ct:
                this.l.setText((int) R.string.six_yuan_per_month);
                break;
            default:
                com.shoujiduoduo.base.a.a.e("UserCenterActivity", "unknown service type ");
                break;
        }
        d();
        x.a().a(b.OBSERVER_VIP, this.r);
        x.a().a(b.OBSERVER_USER_CENTER, this.q);
    }

    /* access modifiers changed from: private */
    public void d() {
        if (com.shoujiduoduo.a.b.b.g().g()) {
            ab c2 = com.shoujiduoduo.a.b.b.g().c();
            this.d.setText(c2.b());
            if (c2.j()) {
                this.f.setVisibility(4);
                this.k.setVisibility(0);
                this.e.setText((int) R.string.my_vip);
            } else {
                this.e.setText((int) R.string.my_normal);
                this.k.setVisibility(8);
            }
            if (!TextUtils.isEmpty(c2.c())) {
                d.a().a(c2.c(), this.c, v.a().d());
            } else if (c2.j()) {
                this.c.setImageResource(R.drawable.vip_headpic_big);
            } else {
                this.c.setImageResource(R.drawable.auther_img);
            }
            if (!TextUtils.isEmpty(c2.k())) {
                this.g.setVisibility(0);
                ((TextView) this.g.findViewById(R.id.user_phone)).setText(c2.k());
                return;
            }
            this.g.setVisibility(4);
        }
    }

    /* access modifiers changed from: private */
    public void e() {
        com.umeng.socialize.c.b bVar = null;
        switch (com.shoujiduoduo.a.b.b.g().d()) {
            case 2:
                bVar = com.umeng.socialize.c.b.QQ;
                break;
            case 3:
                bVar = com.umeng.socialize.c.b.SINA;
                break;
            case 5:
                bVar = com.umeng.socialize.c.b.WEIXIN;
                break;
        }
        if (bVar != null) {
            ax.a().a(this, bVar);
        }
        this.h.setVisibility(4);
        com.umeng.analytics.b.b(RingDDApp.c(), "USER_LOGOUT");
        ab c2 = com.shoujiduoduo.a.b.b.g().c();
        c2.c(0);
        com.shoujiduoduo.a.b.b.g().a(c2);
        x.a().a(b.OBSERVER_USER_CENTER, new a(this));
    }

    /* access modifiers changed from: private */
    public void b(String str) {
        new cb(this, R.style.DuoDuoDialog, str, this.m, new j(this)).show();
    }

    /* access modifiers changed from: private */
    public void f() {
        ab c2 = com.shoujiduoduo.a.b.b.g().c();
        if (!av.c(c2.k())) {
            this.m = f.g(c2.k());
            c(c2.k());
            return;
        }
        switch (f.u()) {
            case f1671a:
                this.m = f.b.f1671a;
                c("");
                return;
            case cu:
                this.m = f.b.cu;
                b("");
                return;
            case ct:
                this.m = f.b.ct;
                String i2 = f.i();
                if (!TextUtils.isEmpty(i2)) {
                    a("请稍候...");
                    com.shoujiduoduo.util.d.b.a().b(i2, new k(this));
                    return;
                }
                b("");
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public void c(String str) {
        a("请稍候...");
        if (av.c(str)) {
            e(str);
            return;
        }
        switch (f.g(str)) {
            case f1671a:
                e(str);
                return;
            case cu:
                if (com.shoujiduoduo.util.e.a.a().a(str)) {
                    com.shoujiduoduo.base.a.a.a("UserCenterActivity", "当前手机号有token， phone:" + str);
                    g(str);
                    return;
                }
                b(str);
                return;
            case ct:
                d(str);
                return;
            default:
                c();
                com.shoujiduoduo.base.a.a.c("UserCenterActivity", "unknown phone type");
                return;
        }
    }

    private void d(String str) {
        com.shoujiduoduo.util.d.b.a().a(str, new l(this, str));
    }

    private void e(String str) {
        com.shoujiduoduo.util.c.b.a().a(new o(this, str));
    }

    /* access modifiers changed from: private */
    public void f(String str) {
        com.shoujiduoduo.util.c.b.a().e(new q(this));
    }

    private void g(String str) {
        a("请稍候...");
        com.shoujiduoduo.util.e.a.a().i(str, new r(this, str));
    }

    /* access modifiers changed from: private */
    public void a(String str, boolean z) {
        com.shoujiduoduo.util.e.a.a().f(new s(this, z, str));
    }

    /* access modifiers changed from: private */
    public void a(boolean z) {
        new ck(this, R.style.DuoDuoDialog, this.m, new b(this, z)).show();
    }

    /* access modifiers changed from: private */
    public void b(boolean z) {
        new au(this, this.m, null, "user_center", false, z, null).show();
    }

    /* access modifiers changed from: private */
    public void c(boolean z) {
        int i2;
        int i3 = 0;
        ab c2 = com.shoujiduoduo.a.b.b.g().c();
        if (this.m.equals(f.b.cu)) {
            i2 = 3;
        } else if (this.m.equals(f.b.ct)) {
            i2 = 2;
        } else {
            i2 = this.m.equals(f.b.f1671a) ? 1 : 0;
        }
        if (z) {
            i3 = i2;
        }
        c2.b(i3);
        com.shoujiduoduo.a.b.b.g().a(c2);
        x.a().a(b.OBSERVER_VIP, new c(this, i2));
    }

    private ArrayList<Map<String, Object>> g() {
        ArrayList<Map<String, Object>> arrayList = new ArrayList<>();
        HashMap hashMap = new HashMap();
        hashMap.put("icon", Integer.valueOf((int) R.drawable.vip_free_b));
        hashMap.put("title", "20万首彩铃免费换");
        hashMap.put("description", "换彩铃不用花钱啦");
        hashMap.put("arrow", Integer.valueOf((int) R.drawable.arraw_right));
        arrayList.add(hashMap);
        HashMap hashMap2 = new HashMap();
        hashMap2.put("icon", Integer.valueOf((int) R.drawable.vip_noad_b));
        hashMap2.put("title", "永久去除应用内广告");
        hashMap2.put("description", "无广告，真干净");
        hashMap2.put("arrow", Integer.valueOf((int) R.drawable.arraw_right));
        arrayList.add(hashMap2);
        HashMap hashMap3 = new HashMap();
        hashMap3.put("icon", Integer.valueOf((int) R.drawable.vip_personal_b));
        hashMap3.put("title", "私人定制炫酷启动画面");
        hashMap3.put("description", "小清新、文艺范、女汉子...");
        hashMap3.put("arrow", Integer.valueOf((int) R.drawable.arraw_right));
        arrayList.add(hashMap3);
        HashMap hashMap4 = new HashMap();
        hashMap4.put("icon", Integer.valueOf((int) R.drawable.vip_service));
        hashMap4.put("title", "客服MM一对一为您服务");
        hashMap4.put("description", "客服QQ：3219210390");
        hashMap4.put("arrow", Integer.valueOf((int) R.drawable.arraw_right));
        arrayList.add(hashMap4);
        return arrayList;
    }

    public void onClick(View view) {
        String str;
        switch (view.getId()) {
            case R.id.open_vip:
                f();
                return;
            case R.id.user_center_back:
                finish();
                return;
            case R.id.user_logout:
                if (com.shoujiduoduo.a.b.b.g().h()) {
                    str = "退出登录后将不再享受VIP特权，确定退出登录吗？";
                } else {
                    str = "确定退出当前账号吗？";
                }
                new a.C0034a(this).a(str).a("退出", new d(this)).b("取消", (DialogInterface.OnClickListener) null).a().show();
                return;
            case R.id.edit_userinfo:
                startActivity(new Intent(this, UserInfoEditActivity.class));
                return;
            default:
                return;
        }
    }

    private class a extends BaseAdapter {
        private a() {
        }

        /* synthetic */ a(UserCenterActivity userCenterActivity, a aVar) {
            this();
        }

        public int getCount() {
            return UserCenterActivity.this.j.size();
        }

        public Object getItem(int i) {
            return UserCenterActivity.this.j.get(i);
        }

        public long getItemId(int i) {
            return (long) i;
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            if (view == null) {
                view = LayoutInflater.from(UserCenterActivity.this).inflate((int) R.layout.listitem_vip_rights, (ViewGroup) null);
            }
            Map map = (Map) UserCenterActivity.this.j.get(i);
            TextView textView = (TextView) view.findViewById(R.id.title);
            Button button = (Button) view.findViewById(R.id.btn_action);
            ((ImageView) view.findViewById(R.id.icon)).setImageResource(((Integer) map.get("icon")).intValue());
            textView.setText((String) map.get("title"));
            ((TextView) view.findViewById(R.id.description)).setText((String) map.get("description"));
            if (i == 0) {
                textView.setTextColor(UserCenterActivity.this.getResources().getColor(R.color.text_orange));
                button.setText("换一首");
                button.setOnClickListener(new v(this));
            } else if (i == 1) {
                button.setVisibility(4);
            } else if (i == 2) {
                button.setText("来一张");
                button.setOnClickListener(new w(this));
            } else {
                button.setVisibility(4);
            }
            return view;
        }
    }

    /* access modifiers changed from: package-private */
    public void a() {
        if (this.o == null) {
            this.o = new ProgressDialog(this);
            this.o.setMessage("中国移动提示您：首次使用彩铃功能需要发送一条免费短信，该过程自动完成，如弹出警告，请选择允许。");
            this.o.setIndeterminate(false);
            this.o.setCancelable(true);
            this.o.setCanceledOnTouchOutside(false);
            this.o.setButton(-1, "确定", new e(this));
            this.o.show();
        }
    }

    /* access modifiers changed from: package-private */
    public void b() {
        if (this.o != null) {
            this.o.dismiss();
            this.o = null;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        this.n.post(new f(this, str));
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        x.a().b(b.OBSERVER_VIP, this.r);
        x.a().b(b.OBSERVER_USER_CENTER, this.q);
    }

    /* access modifiers changed from: package-private */
    public void c() {
        this.n.post(new i(this));
    }
}
