package com.shoujiduoduo.ui.user;

import com.shoujiduoduo.a.a.b;
import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.base.bean.ab;
import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;

/* compiled from: UserInfoEditActivity */
class ah extends a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ab f1403a;
    final /* synthetic */ UserInfoEditActivity b;

    ah(UserInfoEditActivity userInfoEditActivity, ab abVar) {
        this.b = userInfoEditActivity;
        this.f1403a = abVar;
    }

    public void a(c.b bVar) {
        boolean z = true;
        super.a(bVar);
        if (bVar != null && (bVar instanceof c.C0028c)) {
            int g = this.f1403a.g();
            if (((c.C0028c) bVar).e()) {
                this.f1403a.b(1);
                if (g == 1) {
                    z = false;
                }
            } else {
                this.f1403a.b(0);
                if (g == 0) {
                    z = false;
                }
            }
            if (z) {
                x.a().a(new ai(this));
                x.a().a(b.OBSERVER_VIP, new aj(this));
            }
        }
    }

    public void b(c.b bVar) {
        super.b(bVar);
    }
}
