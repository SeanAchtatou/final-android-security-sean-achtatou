package com.shoujiduoduo.ui.user;

import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.widget.f;

/* compiled from: UserLoginActivity */
class bl extends a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ UserLoginActivity f1419a;

    bl(UserLoginActivity userLoginActivity) {
        this.f1419a = userLoginActivity;
    }

    public void a(c.b bVar) {
        super.a(bVar);
        this.f1419a.a();
        if (bVar instanceof c.a) {
            c.a aVar = (c.a) bVar;
            com.shoujiduoduo.base.a.a.a("UserLoginActivity", "token:" + aVar.f1586a);
            com.shoujiduoduo.util.e.a.a().a(this.f1419a.l, aVar.f1586a);
            this.f1419a.b();
            this.f1419a.finish();
            return;
        }
        f.a("登录失败，验证码不对或失效，请重新获取验证码", 1);
    }

    public void b(c.b bVar) {
        super.b(bVar);
        this.f1419a.a();
        f.a("登录失败，验证码不对或失效，请重新获取验证码", 1);
    }
}
