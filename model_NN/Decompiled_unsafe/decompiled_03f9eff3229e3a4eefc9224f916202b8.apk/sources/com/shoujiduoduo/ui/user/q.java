package com.shoujiduoduo.ui.user;

import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.widget.f;

/* compiled from: UserCenterActivity */
class q extends a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ UserCenterActivity f1435a;

    q(UserCenterActivity userCenterActivity) {
        this.f1435a = userCenterActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.ui.user.UserCenterActivity.a(com.shoujiduoduo.ui.user.UserCenterActivity, boolean):void
     arg types: [com.shoujiduoduo.ui.user.UserCenterActivity, int]
     candidates:
      com.shoujiduoduo.ui.user.UserCenterActivity.a(com.shoujiduoduo.ui.user.UserCenterActivity, android.app.ProgressDialog):android.app.ProgressDialog
      com.shoujiduoduo.ui.user.UserCenterActivity.a(com.shoujiduoduo.ui.user.UserCenterActivity, java.lang.String):void
      com.shoujiduoduo.ui.user.UserCenterActivity.a(java.lang.String, boolean):void
      com.shoujiduoduo.ui.user.UserCenterActivity.a(com.shoujiduoduo.ui.user.UserCenterActivity, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.ui.user.UserCenterActivity.b(com.shoujiduoduo.ui.user.UserCenterActivity, boolean):void
     arg types: [com.shoujiduoduo.ui.user.UserCenterActivity, int]
     candidates:
      com.shoujiduoduo.ui.user.UserCenterActivity.b(com.shoujiduoduo.ui.user.UserCenterActivity, java.lang.String):void
      com.shoujiduoduo.ui.user.UserCenterActivity.b(com.shoujiduoduo.ui.user.UserCenterActivity, boolean):void */
    public void a(c.b bVar) {
        super.a(bVar);
        this.f1435a.c();
        if (bVar instanceof c.C0028c) {
            c.C0028c cVar = (c.C0028c) bVar;
            if (cVar.e() && cVar.d()) {
                this.f1435a.c(true);
                f.a("当前手机号已经是多多VIP会员啦");
            } else if (cVar.e() && !cVar.d()) {
                com.shoujiduoduo.base.a.a.a("UserCenterActivity", "vip开通，彩铃关闭");
                this.f1435a.c(true);
            } else if (cVar.e() || !cVar.d()) {
                com.shoujiduoduo.base.a.a.a("UserCenterActivity", "均关闭");
                this.f1435a.b(true);
            } else {
                com.shoujiduoduo.base.a.a.a("UserCenterActivity", "vip关闭，彩铃开通");
                this.f1435a.b(false);
            }
        }
    }

    public void b(c.b bVar) {
        super.b(bVar);
        this.f1435a.c();
        com.shoujiduoduo.base.a.a.a("UserCenterActivity", "查询状态失败");
        f.a("查询状态失败");
    }
}
