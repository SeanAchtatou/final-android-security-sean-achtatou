package com.shoujiduoduo.ui.user;

import com.shoujiduoduo.a.a.b;
import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.base.bean.ab;
import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;

/* compiled from: UserInfoEditActivity */
class ak extends a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ab f1404a;
    final /* synthetic */ UserInfoEditActivity b;

    ak(UserInfoEditActivity userInfoEditActivity, ab abVar) {
        this.b = userInfoEditActivity;
        this.f1404a = abVar;
    }

    public void a(c.b bVar) {
        boolean z = true;
        super.a(bVar);
        if (bVar != null && (bVar instanceof c.d)) {
            c.d dVar = (c.d) bVar;
            int g = this.f1404a.g();
            if (dVar.e() || dVar.f()) {
                this.f1404a.b(2);
                if (g == 2) {
                    z = false;
                }
            } else {
                this.f1404a.b(0);
                if (g == 0) {
                    z = false;
                }
            }
            if (z) {
                x.a().a(new al(this));
                x.a().a(b.OBSERVER_VIP, new am(this));
            }
        }
    }

    public void b(c.b bVar) {
        super.b(bVar);
    }
}
