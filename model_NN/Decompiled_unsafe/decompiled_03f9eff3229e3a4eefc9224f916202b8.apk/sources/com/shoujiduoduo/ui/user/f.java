package com.shoujiduoduo.ui.user;

import android.app.ProgressDialog;

/* compiled from: UserCenterActivity */
class f implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f1425a;
    final /* synthetic */ UserCenterActivity b;

    f(UserCenterActivity userCenterActivity, String str) {
        this.b = userCenterActivity;
        this.f1425a = str;
    }

    public void run() {
        if (this.b.p == null) {
            ProgressDialog unused = this.b.p = new ProgressDialog(this.b);
            this.b.p.setMessage(this.f1425a);
            this.b.p.setIndeterminate(false);
            this.b.p.setCancelable(false);
            if (!this.b.isFinishing()) {
                this.b.p.show();
            }
        }
    }
}
