package com.shoujiduoduo.ui.user;

import android.content.Intent;
import android.view.View;
import com.shoujiduoduo.a.b.b;
import com.shoujiduoduo.ui.settings.ChangeSkinActivity;
import com.shoujiduoduo.ui.user.UserCenterActivity;

/* compiled from: UserCenterActivity */
class w implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ UserCenterActivity.a f1441a;

    w(UserCenterActivity.a aVar) {
        this.f1441a = aVar;
    }

    public void onClick(View view) {
        if (b.g().h()) {
            UserCenterActivity.this.startActivity(new Intent(UserCenterActivity.this, ChangeSkinActivity.class));
            return;
        }
        UserCenterActivity.this.f();
    }
}
