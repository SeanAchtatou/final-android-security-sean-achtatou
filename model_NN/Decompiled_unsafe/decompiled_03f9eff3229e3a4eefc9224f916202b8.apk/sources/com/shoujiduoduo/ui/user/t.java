package com.shoujiduoduo.ui.user;

import android.content.DialogInterface;
import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.widget.a;
import com.shoujiduoduo.util.widget.f;

/* compiled from: UserCenterActivity */
class t extends a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ s f1438a;

    t(s sVar) {
        this.f1438a = sVar;
    }

    public void a(c.b bVar) {
        super.a(bVar);
        this.f1438a.c.c();
        com.shoujiduoduo.base.a.a.a("UserCenterActivity", "成功开通彩铃基础功能");
        f.a("当前手机号已经是多多VIP会员啦");
    }

    public void b(c.b bVar) {
        super.b(bVar);
        this.f1438a.c.c();
        if (bVar.a().equals("000001") || bVar.a().equals("301000")) {
            f.a("当前手机号已经是多多VIP会员啦");
        } else {
            new a.C0034a(this.f1438a.c).b("设置彩铃").a("为您免费开通彩铃功能失败， 原因：" + bVar.b()).a("确定", (DialogInterface.OnClickListener) null).a().show();
        }
    }
}
