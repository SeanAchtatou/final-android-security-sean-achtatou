package com.shoujiduoduo.ui.user;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.shoujiduoduo.a.a.b;
import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.a.c.u;
import com.shoujiduoduo.base.bean.ab;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ui.utils.BaseActivity;
import com.shoujiduoduo.util.at;
import com.shoujiduoduo.util.f;
import com.shoujiduoduo.util.widget.ClearEditText;
import com.umeng.socialize.UMAuthListener;
import com.umeng.socialize.UMShareAPI;
import java.util.HashMap;

public class UserLoginActivity extends BaseActivity implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private ClearEditText f1395a;
    private String b;
    private boolean c;
    private ContentObserver d;
    private RelativeLayout e;
    private RelativeLayout f;
    private RelativeLayout g;
    private EditText h;
    /* access modifiers changed from: private */
    public Button i;
    private boolean j;
    private a k;
    /* access modifiers changed from: private */
    public String l;
    private TextView m;
    private Handler n;
    private f.b o;
    /* access modifiers changed from: private */
    public String p;
    /* access modifiers changed from: private */
    public String q;
    /* access modifiers changed from: private */
    public UMShareAPI r = null;
    private UMAuthListener s = new as(this);
    /* access modifiers changed from: private */
    public UMAuthListener t = new be(this);
    private u u = new bi(this);
    /* access modifiers changed from: private */
    public ProgressDialog v = null;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_user_login);
        this.r = UMShareAPI.get(this);
        this.n = new Handler();
        this.f1395a = (ClearEditText) findViewById(R.id.et_phone_no);
        this.e = (RelativeLayout) findViewById(R.id.get_auth_code_layout);
        this.f = (RelativeLayout) findViewById(R.id.code_login_layout);
        this.e.setVisibility(0);
        this.f.setVisibility(4);
        this.i = (Button) findViewById(R.id.reget_sms_code);
        this.m = (TextView) findViewById(R.id.phone_num);
        this.k = new a(60000, 1000);
        this.h = (EditText) findViewById(R.id.et_auth_code);
        this.g = (RelativeLayout) findViewById(R.id.phone_auth_layout);
        findViewById(R.id.sina_weibo_login).setOnClickListener(this);
        findViewById(R.id.qq_login).setOnClickListener(this);
        findViewById(R.id.weixin_login).setOnClickListener(this);
        findViewById(R.id.user_center_back).setOnClickListener(this);
        findViewById(R.id.get_sms_code).setOnClickListener(this);
        findViewById(R.id.login).setOnClickListener(this);
        findViewById(R.id.reget_sms_code).setOnClickListener(this);
        x.a().a(b.OBSERVER_USER_CENTER, this.u);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.k.cancel();
        a();
        if (this.d != null && this.c) {
            getContentResolver().unregisterContentObserver(this.d);
        }
        x.a().b(b.OBSERVER_USER_CENTER, this.u);
    }

    private void b(String str) {
        if (this.d != null && this.c) {
            getContentResolver().unregisterContentObserver(this.d);
        }
        this.d = new at(this, new Handler(), this.h, str);
        getContentResolver().registerContentObserver(Uri.parse("content://sms/"), true, this.d);
        this.c = true;
    }

    /* access modifiers changed from: private */
    public int a(com.umeng.socialize.c.b bVar) {
        if (bVar.equals(com.umeng.socialize.c.b.QQ)) {
            return 2;
        }
        if (bVar.equals(com.umeng.socialize.c.b.SINA)) {
            return 3;
        }
        if (bVar.equals(com.umeng.socialize.c.b.WEIXIN)) {
            return 5;
        }
        if (bVar.equals(com.umeng.socialize.c.b.RENREN)) {
            return 4;
        }
        return 0;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        this.r.onActivityResult(i2, i3, intent);
    }

    private class a extends CountDownTimer {
        public a(long j, long j2) {
            super(j, j2);
        }

        public void onTick(long j) {
            UserLoginActivity.this.i.setClickable(false);
            UserLoginActivity.this.i.setText((j / 1000) + "秒");
        }

        public void onFinish() {
            UserLoginActivity.this.i.setClickable(true);
            UserLoginActivity.this.i.setText("重新获取");
        }
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        this.n.post(new bj(this, str));
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.n.post(new bk(this));
    }

    public void onClick(View view) {
        HashMap hashMap = new HashMap();
        switch (view.getId()) {
            case R.id.get_sms_code:
                this.l = this.f1395a.getText().toString();
                if (!f.f(this.l)) {
                    com.shoujiduoduo.util.widget.f.a("请输入正确的手机号", 0);
                    return;
                }
                this.o = f.g(this.l);
                if (this.o == f.b.d) {
                    com.shoujiduoduo.util.widget.f.a("未知的手机号类型，无法判断运营商，请确认手机号输入正确！");
                    com.shoujiduoduo.base.a.a.c("UserLoginActivity", "unknown phone type :" + this.l);
                    return;
                }
                d();
                this.e.setVisibility(4);
                this.f.setVisibility(0);
                this.k.start();
                this.m.setText(this.l);
                this.j = true;
                return;
            case R.id.user_center_back:
                if (this.j) {
                    this.e.setVisibility(0);
                    this.f.setVisibility(4);
                    this.j = false;
                    return;
                }
                finish();
                return;
            case R.id.reget_sms_code:
                this.k.start();
                d();
                return;
            case R.id.login:
                String obj = this.h.getText().toString();
                if (this.o.equals(f.b.ct)) {
                    if (TextUtils.isEmpty(obj) || obj.length() != 6 || !obj.equals(this.b)) {
                        com.shoujiduoduo.util.widget.f.a("请输入正确的验证码", 0);
                        return;
                    }
                    b();
                    finish();
                    return;
                } else if (this.o.equals(f.b.cu)) {
                    if (TextUtils.isEmpty(obj) || obj.length() != 6) {
                        com.shoujiduoduo.util.widget.f.a("请输入正确的验证码", 0);
                        return;
                    }
                    a("请稍候...");
                    com.shoujiduoduo.util.e.a.a().a(this.l, obj, new bl(this));
                    return;
                } else if (!this.o.equals(f.b.f1671a)) {
                    com.shoujiduoduo.util.widget.f.a("登录失败，未识别的运营商类型", 0);
                    return;
                } else if (TextUtils.isEmpty(obj) || obj.length() != 6) {
                    com.shoujiduoduo.util.widget.f.a("请输入正确的验证码", 0);
                    return;
                } else {
                    a("请稍候...");
                    com.shoujiduoduo.util.c.b.a().a(this.l, obj, new bm(this));
                    return;
                }
            case R.id.weixin_login:
                hashMap.put("platform", "weixin");
                this.r.doOauthVerify(this, com.umeng.socialize.c.b.WEIXIN, this.s);
                com.umeng.analytics.b.a(RingDDApp.c(), "USER_LOGIN", hashMap);
                a("正在登录...");
                return;
            case R.id.qq_login:
                hashMap.put("platform", "qq");
                this.r.doOauthVerify(this, com.umeng.socialize.c.b.QQ, this.s);
                com.umeng.analytics.b.a(RingDDApp.c(), "USER_LOGIN", hashMap);
                a("正在登录...");
                return;
            case R.id.sina_weibo_login:
                hashMap.put("platform", "sina");
                this.r.doOauthVerify(this, com.umeng.socialize.c.b.SINA, this.s);
                com.umeng.analytics.b.a(RingDDApp.c(), "USER_LOGIN", hashMap);
                a("正在登录...");
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public void b() {
        com.shoujiduoduo.util.widget.f.a("登录成功", 0);
        ab c2 = com.shoujiduoduo.a.b.b.g().c();
        c2.a("phone_" + this.l);
        c2.b(this.l);
        c2.d(this.l);
        c2.c(1);
        c2.a(1);
        com.shoujiduoduo.a.b.b.g().a(c2);
        c();
        x.a().a(b.OBSERVER_USER_CENTER, new bn(this));
    }

    private void c() {
        if (this.o.equals(f.b.ct)) {
            com.shoujiduoduo.util.d.b.a().a(this.l, new bo(this));
        } else if (this.o.equals(f.b.cu)) {
            c(this.l);
        } else if (this.o.equals(f.b.f1671a)) {
            com.shoujiduoduo.util.c.b.a().d(new aw(this));
        }
    }

    private void c(String str) {
        if (com.shoujiduoduo.util.e.a.a().a(str)) {
            com.shoujiduoduo.base.a.a.a("UserLoginActivity", "当前手机号有token， phone:" + str);
            com.shoujiduoduo.util.e.a.a().f(new az(this, str));
            return;
        }
        com.shoujiduoduo.base.a.a.a("UserLoginActivity", "当前手机号没有token， 不做vip查询");
    }

    private void d() {
        if (this.o.equals(f.b.ct)) {
            b("118100");
            this.b = f.a(6);
            com.shoujiduoduo.base.a.a.a("UserLoginActivity", "random key:" + this.b);
            com.shoujiduoduo.util.d.b.a().a(this.l, "铃声多多验证码：" + this.b + "【铃声多多，每天都有新铃声】", new bb(this));
        } else if (this.o.equals(f.b.cu)) {
            b("1065515888");
            com.shoujiduoduo.util.e.a.a().c(this.l, new bc(this));
        } else if (this.o.equals(f.b.f1671a)) {
            b("10658830");
            com.shoujiduoduo.util.c.b.a().a(this.l, new bd(this));
        } else {
            com.shoujiduoduo.base.a.a.c("UserLoginActivity", "unknown service type");
        }
    }
}
