package com.shoujiduoduo.ui.user;

import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.base.bean.ab;
import com.shoujiduoduo.util.f;
import com.shoujiduoduo.util.w;

/* compiled from: UserInfoEditActivity */
class ac implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f1401a;
    final /* synthetic */ String b;
    final /* synthetic */ ab c;
    final /* synthetic */ ab d;

    ac(ab abVar, String str, String str2, ab abVar2) {
        this.d = abVar;
        this.f1401a = str;
        this.b = str2;
        this.c = abVar2;
    }

    public void run() {
        StringBuilder sb = new StringBuilder();
        sb.append("&newimsi=").append(f.i()).append("&phone=").append(this.f1401a);
        byte[] a2 = w.a("&type=query3rd", sb.toString());
        if (a2 != null) {
            String str = new String(a2);
            a.a("UserInfoEditActivity", "curLoginType:" + this.b + ", bindedType:" + str);
            if (str.contains(this.b)) {
                a.a("UserInfoEditActivity", "已经与当前平台绑定");
                this.d.f1400a.a();
                this.d.f1400a.e.sendMessage(this.d.f1400a.e.obtainMessage(1, this.f1401a));
                return;
            }
            a.a("UserInfoEditActivity", "没有与当前平台绑定");
        }
        this.d.f1400a.a(this.f1401a, this.b, this.c);
    }
}
