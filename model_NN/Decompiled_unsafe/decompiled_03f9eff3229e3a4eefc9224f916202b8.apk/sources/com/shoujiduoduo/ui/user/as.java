package com.shoujiduoduo.ui.user;

import android.text.TextUtils;
import android.widget.Toast;
import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.umeng.socialize.UMAuthListener;
import com.umeng.socialize.c.b;
import java.util.Map;

/* compiled from: UserLoginActivity */
class as implements UMAuthListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ UserLoginActivity f1408a;

    as(UserLoginActivity userLoginActivity) {
        this.f1408a = userLoginActivity;
    }

    public void onComplete(b bVar, int i, Map<String, String> map) {
        if (map != null) {
            a.a("UserLoginActivity", "[AuthListener]:Auth onComplete:" + map.toString());
            if (bVar.equals(b.WEIXIN) || bVar.equals(b.QQ)) {
                String unused = this.f1408a.p = bVar.toString().toLowerCase() + "_" + map.get("openid");
            } else {
                String unused2 = this.f1408a.p = bVar.toString().toLowerCase() + "_" + map.get("uid");
            }
            a.a("UserLoginActivity", "uid:" + this.f1408a.p);
            if (!TextUtils.isEmpty(this.f1408a.p)) {
                com.shoujiduoduo.util.as.c(RingDDApp.c(), "user_uid", this.f1408a.p);
                this.f1408a.r.getPlatformInfo(this.f1408a, bVar, this.f1408a.t);
                return;
            }
            a.a("UserLoginActivity", "doAuth, has not Authenticated, did not get uid or openid");
            x.a().a(com.shoujiduoduo.a.a.b.OBSERVER_USER_CENTER, new at(this, this.f1408a.a(bVar)));
        }
    }

    public void onError(b bVar, int i, Throwable th) {
        Toast.makeText(this.f1408a.getApplicationContext(), "get fail", 0).show();
        String message = th != null ? th.getMessage() : "";
        a.a("UserLoginActivity", "[AuthListener]:doAuth error, msg:" + message);
        x.a().a(com.shoujiduoduo.a.a.b.OBSERVER_USER_CENTER, new au(this, this.f1408a.a(bVar), message));
    }

    public void onCancel(b bVar, int i) {
        Toast.makeText(this.f1408a.getApplicationContext(), "get cancel", 0).show();
        a.a("UserLoginActivity", "[AuthListener]:Auth, onCancel");
        x.a().a(com.shoujiduoduo.a.a.b.OBSERVER_USER_CENTER, new av(this, this.f1408a.a(bVar)));
    }
}
