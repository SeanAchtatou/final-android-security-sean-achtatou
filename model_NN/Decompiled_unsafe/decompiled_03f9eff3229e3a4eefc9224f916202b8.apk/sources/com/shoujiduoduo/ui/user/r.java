package com.shoujiduoduo.ui.user;

import android.content.DialogInterface;
import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.widget.a;

/* compiled from: UserCenterActivity */
class r extends a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f1436a;
    final /* synthetic */ UserCenterActivity b;

    r(UserCenterActivity userCenterActivity, String str) {
        this.b = userCenterActivity;
        this.f1436a = str;
    }

    public void a(c.b bVar) {
        super.a(bVar);
        if (bVar != null && (bVar instanceof c.af)) {
            c.af afVar = (c.af) bVar;
            com.shoujiduoduo.base.a.a.a("UserCenterActivity", "user location, provinceid:" + afVar.f1592a + ", province name:" + afVar.d);
            this.b.a(this.f1436a, com.shoujiduoduo.util.e.a.a().c(afVar.f1592a));
        }
    }

    public void b(c.b bVar) {
        super.b(bVar);
        this.b.c();
        new a.C0034a(this.b).b("设置彩铃").a("获取当前手机号信息失败，请稍候再试试。").a("确定", (DialogInterface.OnClickListener) null).a().show();
    }
}
