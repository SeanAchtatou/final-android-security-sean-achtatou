package com.shoujiduoduo.ui.user;

import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.a.b.b;
import com.shoujiduoduo.base.bean.ab;
import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;

/* compiled from: UserLoginActivity */
class az extends a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f1410a;
    final /* synthetic */ UserLoginActivity b;

    az(UserLoginActivity userLoginActivity, String str) {
        this.b = userLoginActivity;
        this.f1410a = str;
    }

    public void a(c.b bVar) {
        super.a(bVar);
        if (bVar instanceof c.e) {
            c.e eVar = (c.e) bVar;
            ab c = b.g().c();
            if (eVar.e()) {
                c.b(3);
            } else {
                c.b(0);
            }
            b.g().a(c);
            x.a().a(com.shoujiduoduo.a.a.b.OBSERVER_VIP, new ba(this));
            if (eVar.f1598a.a().equals("40307") || eVar.f1598a.a().equals("40308")) {
                com.shoujiduoduo.base.a.a.a("UserLoginActivity", "token 失效");
                com.shoujiduoduo.util.e.a.a().a(this.f1410a, "");
            }
        }
    }

    public void b(c.b bVar) {
        if (bVar.a().equals("40307") || bVar.a().equals("40308")) {
            com.shoujiduoduo.base.a.a.a("UserLoginActivity", "token 失效");
            com.shoujiduoduo.util.e.a.a().a(this.f1410a, "");
        }
        super.b(bVar);
    }
}
