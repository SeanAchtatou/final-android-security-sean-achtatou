package com.shoujiduoduo.ui.user;

import android.app.ProgressDialog;

/* compiled from: UserLoginActivity */
class bj implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f1417a;
    final /* synthetic */ UserLoginActivity b;

    bj(UserLoginActivity userLoginActivity, String str) {
        this.b = userLoginActivity;
        this.f1417a = str;
    }

    public void run() {
        if (this.b.v == null) {
            ProgressDialog unused = this.b.v = new ProgressDialog(this.b);
            this.b.v.setMessage(this.f1417a);
            this.b.v.setIndeterminate(false);
            this.b.v.setCancelable(true);
            this.b.v.setCanceledOnTouchOutside(false);
            if (!this.b.isFinishing()) {
                this.b.v.show();
            }
        }
    }
}
