package com.shoujiduoduo.ui.user;

import android.os.Message;
import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.a.b.b;
import com.shoujiduoduo.base.bean.ab;
import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;

/* compiled from: UserCenterActivity */
class o extends a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f1434a;
    final /* synthetic */ UserCenterActivity b;

    o(UserCenterActivity userCenterActivity, String str) {
        this.b = userCenterActivity;
        this.f1434a = str;
    }

    public void a(c.b bVar) {
        super.a(bVar);
        if (bVar instanceof c.l) {
            c.l lVar = (c.l) bVar;
            if (lVar.f1605a == c.l.a.sms_code) {
                ab c = b.g().c();
                if (!c.i()) {
                    c.b(lVar.d);
                    c.a("phone_" + lVar.d);
                }
                c.d(lVar.d);
                c.c(1);
                b.g().a(c);
                x.a().a(com.shoujiduoduo.a.a.b.OBSERVER_USER_CENTER, new p(this));
            }
        }
        this.b.f(this.f1434a);
    }

    public void b(c.b bVar) {
        super.b(bVar);
        com.shoujiduoduo.base.a.a.a("UserCenterActivity", "cmcc sdk is not inited");
        this.b.a();
        Message message = new Message();
        message.what = 1;
        message.obj = this.f1434a;
        this.b.n.sendMessageDelayed(message, 3000);
    }
}
