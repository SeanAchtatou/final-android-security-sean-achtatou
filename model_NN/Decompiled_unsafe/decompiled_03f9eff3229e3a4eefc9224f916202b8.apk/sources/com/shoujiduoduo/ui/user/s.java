package com.shoujiduoduo.ui.user;

import android.content.DialogInterface;
import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.widget.a;
import com.shoujiduoduo.util.widget.f;

/* compiled from: UserCenterActivity */
class s extends a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ boolean f1437a;
    final /* synthetic */ String b;
    final /* synthetic */ UserCenterActivity c;

    s(UserCenterActivity userCenterActivity, boolean z, String str) {
        this.c = userCenterActivity;
        this.f1437a = z;
        this.b = str;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.ui.user.UserCenterActivity.a(com.shoujiduoduo.ui.user.UserCenterActivity, boolean):void
     arg types: [com.shoujiduoduo.ui.user.UserCenterActivity, int]
     candidates:
      com.shoujiduoduo.ui.user.UserCenterActivity.a(com.shoujiduoduo.ui.user.UserCenterActivity, android.app.ProgressDialog):android.app.ProgressDialog
      com.shoujiduoduo.ui.user.UserCenterActivity.a(com.shoujiduoduo.ui.user.UserCenterActivity, java.lang.String):void
      com.shoujiduoduo.ui.user.UserCenterActivity.a(java.lang.String, boolean):void
      com.shoujiduoduo.ui.user.UserCenterActivity.a(com.shoujiduoduo.ui.user.UserCenterActivity, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.ui.user.UserCenterActivity.b(com.shoujiduoduo.ui.user.UserCenterActivity, boolean):void
     arg types: [com.shoujiduoduo.ui.user.UserCenterActivity, int]
     candidates:
      com.shoujiduoduo.ui.user.UserCenterActivity.b(com.shoujiduoduo.ui.user.UserCenterActivity, java.lang.String):void
      com.shoujiduoduo.ui.user.UserCenterActivity.b(com.shoujiduoduo.ui.user.UserCenterActivity, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.ui.user.UserCenterActivity.c(com.shoujiduoduo.ui.user.UserCenterActivity, boolean):void
     arg types: [com.shoujiduoduo.ui.user.UserCenterActivity, int]
     candidates:
      com.shoujiduoduo.ui.user.UserCenterActivity.c(com.shoujiduoduo.ui.user.UserCenterActivity, java.lang.String):void
      com.shoujiduoduo.ui.user.UserCenterActivity.c(com.shoujiduoduo.ui.user.UserCenterActivity, boolean):void */
    public void a(c.b bVar) {
        super.a(bVar);
        if (bVar instanceof c.e) {
            c.e eVar = (c.e) bVar;
            if (eVar.d() && eVar.e()) {
                com.shoujiduoduo.base.a.a.a("UserCenterActivity", "彩铃与会员均开");
                this.c.c();
                this.c.c(true);
                f.a("当前手机号已经是多多VIP会员啦");
            } else if (eVar.d() && !eVar.e()) {
                com.shoujiduoduo.base.a.a.a("UserCenterActivity", "彩铃开，会员关闭");
                this.c.c();
                if (this.f1437a) {
                    this.c.b(false);
                } else {
                    new a.C0034a(this.c).b("设置彩铃").a("抱歉，您所在的当前区域不支持铃声多多的彩铃功能！").a("确定", (DialogInterface.OnClickListener) null).a().show();
                }
            } else if (!eVar.d() && eVar.e()) {
                com.shoujiduoduo.base.a.a.a("UserCenterActivity", "彩铃关，会员开");
                this.c.c(true);
                if (eVar.i()) {
                    com.shoujiduoduo.base.a.a.a("UserCenterActivity", "属于免彩铃功能费范围，先 帮用户自动开通彩铃功能， net type:" + eVar.g() + ", location:" + eVar.f());
                    com.shoujiduoduo.util.e.a.a().e("&phone=" + this.b, new t(this));
                } else {
                    com.shoujiduoduo.base.a.a.a("UserCenterActivity", "不属于免彩铃功能费范围， 提示开通彩铃");
                    this.c.c();
                    this.c.a(false);
                }
            } else if (!eVar.d() && !eVar.e()) {
                com.shoujiduoduo.base.a.a.a("UserCenterActivity", "彩铃、会员均关闭");
                if (!this.f1437a) {
                    new a.C0034a(this.c).b("设置彩铃").a("抱歉，您所在的当前区域不支持铃声多多的彩铃功能！").a("确定", (DialogInterface.OnClickListener) null).a().show();
                    return;
                } else if (eVar.i()) {
                    com.shoujiduoduo.base.a.a.a("UserCenterActivity", "属于免彩铃功能费范围， 帮用户自动开通彩铃，net type:" + eVar.g() + ", location:" + eVar.f());
                    com.shoujiduoduo.util.e.a.a().e("&phone=" + this.b, new u(this));
                } else {
                    com.shoujiduoduo.base.a.a.a("UserCenterActivity", "不属于免彩铃功能费范围， 提示开通会员");
                    this.c.c();
                    this.c.a(true);
                }
            }
            if (eVar.f1598a.a().equals("40307") || eVar.f1598a.a().equals("40308")) {
                com.shoujiduoduo.util.e.a.a().a(this.b, "");
                this.c.c();
                this.c.b(this.b);
            }
        }
    }

    public void b(c.b bVar) {
        super.b(bVar);
        this.c.c();
    }
}
