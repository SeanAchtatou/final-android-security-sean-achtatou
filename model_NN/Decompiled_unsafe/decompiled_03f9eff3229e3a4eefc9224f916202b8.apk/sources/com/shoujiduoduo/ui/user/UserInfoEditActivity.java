package com.shoujiduoduo.ui.user;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.d.a.b.d;
import com.shoujiduoduo.a.a.b;
import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.a.c.u;
import com.shoujiduoduo.base.bean.ab;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ui.cailing.cb;
import com.shoujiduoduo.ui.utils.BaseActivity;
import com.shoujiduoduo.ui.utils.v;
import com.shoujiduoduo.util.f;
import com.shoujiduoduo.util.w;
import com.shoujiduoduo.util.widget.a;

public class UserInfoEditActivity extends BaseActivity implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private ImageView f1392a;
    /* access modifiers changed from: private */
    public Button b;
    /* access modifiers changed from: private */
    public TextView c;
    private TextView d;
    /* access modifiers changed from: private */
    public Handler e;
    private u f = new x(this);
    /* access modifiers changed from: private */
    public ProgressDialog g = null;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_edit_userinfo);
        this.d = (TextView) findViewById(R.id.user_nickname);
        this.c = (TextView) findViewById(R.id.phone);
        this.b = (Button) findViewById(R.id.btn_bind);
        this.f1392a = (ImageView) findViewById(R.id.user_head);
        findViewById(R.id.back).setOnClickListener(this);
        this.b.setOnClickListener(this);
        this.e = new a(this, null);
        b();
        x.a().a(b.OBSERVER_USER_CENTER, this.f);
    }

    /* access modifiers changed from: private */
    public void b() {
        ab c2 = com.shoujiduoduo.a.b.b.g().c();
        d.a().a(c2.c(), this.f1392a, v.a().d());
        this.d.setText(!TextUtils.isEmpty(c2.d()) ? c2.d() : c2.b());
        if (!TextUtils.isEmpty(c2.k())) {
            this.c.setText(c2.k());
            this.b.setText("解除绑定");
            return;
        }
        this.c.setText("无");
        this.b.setText("绑定手机");
    }

    private class a extends Handler {
        private a() {
        }

        /* synthetic */ a(UserInfoEditActivity userInfoEditActivity, x xVar) {
            this();
        }

        public void handleMessage(Message message) {
            String str;
            super.handleMessage(message);
            if (message.what == 1) {
                ab c = com.shoujiduoduo.a.b.b.g().c();
                String str2 = (String) message.obj;
                switch (c.e()) {
                    case 1:
                        str = "手机账号";
                        break;
                    case 2:
                        str = "QQ号码";
                        break;
                    case 3:
                        str = "微博账号";
                        break;
                    case 4:
                    default:
                        str = "账号";
                        break;
                    case 5:
                        str = "微信账号";
                        break;
                }
                new a.C0034a(UserInfoEditActivity.this).a("当前输入的手机号已经与另一个" + str + "绑定, 是否要取消之前的绑定状态并与当前账号绑定？").a("确定", new aq(this, str2, c)).b("取消", (DialogInterface.OnClickListener) null).a().show();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.e != null) {
            this.e.removeCallbacksAndMessages(null);
        }
        x.a().b(b.OBSERVER_USER_CENTER, this.f);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back:
                finish();
                return;
            case R.id.btn_bind:
                ab c2 = com.shoujiduoduo.a.b.b.g().c();
                if (!TextUtils.isEmpty(c2.k())) {
                    new a.C0034a(this).a("确定要解除与当前手机号的绑定吗？").a("确定", new z(this, c2)).b("取消", (DialogInterface.OnClickListener) null).a().show();
                    return;
                } else {
                    c();
                    return;
                }
            default:
                return;
        }
    }

    private void c() {
        new cb(this, R.style.DuoDuoDialog, "", f.u(), new ab(this)).show();
    }

    /* access modifiers changed from: private */
    public void a(String str, String str2, ab abVar) {
        StringBuilder sb = new StringBuilder();
        sb.append("&newimsi=").append(f.i()).append("&phone=").append(str).append("&3rd=").append(str2).append("&uid=").append(abVar.a());
        w.a("&type=connect3rd", sb.toString());
        x.a().a(new ad(this, str));
        "&from=user_info_edit&phone=" + abVar.k();
        switch (f.g(str)) {
            case cu:
                com.shoujiduoduo.util.e.a.a().f(new ae(this, abVar));
                break;
            case f1671a:
                com.shoujiduoduo.util.c.b.a().e(new ah(this, abVar));
                break;
            case ct:
                com.shoujiduoduo.util.d.b.a().a(abVar.k(), new ak(this, abVar));
                break;
        }
        a();
        com.shoujiduoduo.util.widget.f.a("手机号已经成功绑定");
    }

    /* access modifiers changed from: private */
    public void b(String str, String str2, ab abVar) {
        StringBuilder sb = new StringBuilder();
        sb.append("&phone=").append(str).append("&newimsi=").append(f.i()).append("&3rd=").append(str2).append("&uid=").append(abVar.a());
        w.a("&type=clear3rd", sb.toString());
        x.a().a(new an(this, abVar));
        com.shoujiduoduo.util.widget.f.a("已为您解除与当前手机号的绑定");
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        this.e.post(new ap(this, str));
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.e.post(new y(this));
    }
}
