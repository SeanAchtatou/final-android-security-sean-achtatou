package com.shoujiduoduo.ui.home;

import android.content.DialogInterface;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.umeng.analytics.b;
import java.util.HashMap;

/* compiled from: MusicAlbumActivity */
class ah implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ MusicAlbumActivity f1140a;

    ah(MusicAlbumActivity musicAlbumActivity) {
        this.f1140a = musicAlbumActivity;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        dialogInterface.dismiss();
        HashMap hashMap = new HashMap();
        hashMap.put("act", "cancel");
        b.a(RingDDApp.c(), "musicalbum_quit_dialog", hashMap);
        if (this.f1140a.f1127a.canGoBack()) {
            this.f1140a.f1127a.goBack();
        } else {
            this.f1140a.finish();
        }
    }
}
