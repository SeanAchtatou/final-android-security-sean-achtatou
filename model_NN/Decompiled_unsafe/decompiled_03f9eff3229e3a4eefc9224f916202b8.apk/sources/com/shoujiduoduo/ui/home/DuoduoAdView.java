package com.shoujiduoduo.ui.home;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import com.shoujiduoduo.a.c.f;
import com.shoujiduoduo.a.c.i;
import com.shoujiduoduo.base.bean.c;
import java.util.Timer;
import java.util.TimerTask;

public class DuoduoAdView extends ImageView implements f {
    /* access modifiers changed from: private */
    public static final String c = DuoduoAdView.class.getSimpleName();

    /* renamed from: a  reason: collision with root package name */
    Drawable[] f1125a = null;
    Handler b = new x(this);
    /* access modifiers changed from: private */
    public i d = null;
    private int e = 0;
    /* access modifiers changed from: private */
    public Context f = null;
    /* access modifiers changed from: private */
    public com.shoujiduoduo.b.a.f g = new com.shoujiduoduo.b.a.f("duoduo_ad.tmp");
    /* access modifiers changed from: private */
    public Timer h = new Timer();
    private final int i = 331;
    private final int j = 332;
    private final int k = 333;
    /* access modifiers changed from: private */
    public TimerTask l = new u(this);
    /* access modifiers changed from: private */
    public int m = -1;
    /* access modifiers changed from: private */
    public boolean n = false;

    public DuoduoAdView(Context context) {
        super(context);
        a(context);
    }

    public void setAdListener(i iVar) {
        this.d = iVar;
    }

    public DuoduoAdView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    /* access modifiers changed from: package-private */
    public boolean a() {
        return this.n;
    }

    public void b() {
        this.h.cancel();
    }

    /* access modifiers changed from: private */
    public void d() {
        int b2 = this.g.b();
        if (b2 != 0) {
            int i2 = this.m;
            while (true) {
                i2 = (i2 + 1) % b2;
                if (this.f1125a[i2] != null || i2 == this.m) {
                    this.m = i2;
                    setImageDrawable(this.f1125a[this.m]);
                }
            }
            this.m = i2;
            setImageDrawable(this.f1125a[this.m]);
            if (getVisibility() == 0) {
                getWidth();
                int height = getHeight();
                AnimationSet animationSet = new AnimationSet(true);
                TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 0.0f, (float) height, 0.0f);
                translateAnimation.setDuration(500);
                animationSet.addAnimation(translateAnimation);
                startAnimation(animationSet);
            }
        }
    }

    private void a(Context context) {
        this.f = context;
        this.e = ((Activity) context).getWindowManager().getDefaultDisplay().getWidth();
        this.g.a(this);
        this.g.a();
        setOnClickListener(new v(this));
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        setMeasuredDimension(this.e, (this.e * 50) / 320);
    }

    /* access modifiers changed from: private */
    public int a(String str) throws Exception {
        for (int i2 = 0; i2 < this.g.b(); i2++) {
            if (this.g.a(i2).f820a.equals(str)) {
                return i2;
            }
        }
        throw new Exception();
    }

    class a {

        /* renamed from: a  reason: collision with root package name */
        public Drawable f1126a;
        public String b;

        a(Drawable drawable, String str) {
            this.f1126a = drawable;
            this.b = str;
        }
    }

    public void a(c cVar, int i2) {
        com.shoujiduoduo.base.a.a.a(c, "DuoduoAdView: onDataUpdate called by DuoduoFamilyData!");
        String str = null;
        switch (i2) {
            case 0:
                str = "success";
                this.b.sendMessage(this.b.obtainMessage(332));
                break;
            case 1:
                str = "fail";
                break;
            case 2:
                str = "cache";
                this.b.sendMessage(this.b.obtainMessage(332));
                break;
        }
        com.shoujiduoduo.util.i.a(new aa(this, str));
    }
}
