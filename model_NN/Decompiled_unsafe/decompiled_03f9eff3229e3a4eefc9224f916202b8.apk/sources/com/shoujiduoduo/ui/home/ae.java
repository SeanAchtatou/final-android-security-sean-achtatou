package com.shoujiduoduo.ui.home;

import android.view.MotionEvent;
import android.view.View;

/* compiled from: MusicAlbumActivity */
class ae implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ MusicAlbumActivity f1137a;

    ae(MusicAlbumActivity musicAlbumActivity) {
        this.f1137a = musicAlbumActivity;
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
            case 1:
                if (view.hasFocus()) {
                    return false;
                }
                view.requestFocus();
                return false;
            default:
                return false;
        }
    }
}
