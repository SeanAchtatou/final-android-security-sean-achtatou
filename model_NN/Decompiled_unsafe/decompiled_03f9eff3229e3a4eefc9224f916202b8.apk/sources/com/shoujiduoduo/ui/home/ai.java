package com.shoujiduoduo.ui.home;

import android.content.DialogInterface;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.umeng.analytics.b;
import java.util.HashMap;

/* compiled from: MusicAlbumActivity */
class ai implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ MusicAlbumActivity f1141a;

    ai(MusicAlbumActivity musicAlbumActivity) {
        this.f1141a = musicAlbumActivity;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        dialogInterface.dismiss();
        if (this.f1141a.f1127a != null) {
            this.f1141a.f1127a.loadUrl("javascript:ddshare()");
        }
        HashMap hashMap = new HashMap();
        hashMap.put("act", "ok");
        b.a(RingDDApp.c(), "musicalbum_quit_dialog", hashMap);
    }
}
