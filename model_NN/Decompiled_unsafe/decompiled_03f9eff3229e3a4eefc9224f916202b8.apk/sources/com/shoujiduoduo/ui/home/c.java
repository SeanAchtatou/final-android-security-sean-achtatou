package com.shoujiduoduo.ui.home;

import android.view.View;
import android.widget.AdapterView;
import com.shoujiduoduo.a.a.b;
import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.base.a.a;

/* compiled from: ChangeAreaDialog */
class c implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ b f1145a;

    c(b bVar) {
        this.f1145a = bVar;
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        this.f1145a.f = i;
        this.f1145a.c.notifyDataSetChanged();
        if (this.f1145a.f > -1 && this.f1145a.f < this.f1145a.d.length) {
            a.a(b.h, "选择地域：" + this.f1145a.d[this.f1145a.f]);
            x.a().b(b.OBSERVER_LIST_AREA, new d(this, this.f1145a.d[this.f1145a.f], this.f1145a.g));
        }
        this.f1145a.f = -1;
        this.f1145a.dismiss();
    }
}
