package com.shoujiduoduo.ui.home;

import android.graphics.drawable.Drawable;
import android.os.Message;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.ui.home.DuoduoAdView;
import com.shoujiduoduo.ui.utils.d;
import com.shoujiduoduo.util.i;

/* compiled from: DuoduoAdView */
class y implements d.a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ x f1166a;

    y(x xVar) {
        this.f1166a = xVar;
    }

    public void a(Drawable drawable, String str) {
        String str2;
        if (drawable == null) {
            str2 = "fail";
        } else {
            str2 = "success";
            Message obtainMessage = this.f1166a.f1165a.b.obtainMessage(331, new DuoduoAdView.a(drawable, str));
            a.a(DuoduoAdView.c, "AsyncImageLoader in DuoduoAdView: finish load image: " + str);
            this.f1166a.f1165a.b.sendMessage(obtainMessage);
        }
        if (str2.equals("fail")) {
            i.a(new z(this, str2, str));
        }
    }
}
