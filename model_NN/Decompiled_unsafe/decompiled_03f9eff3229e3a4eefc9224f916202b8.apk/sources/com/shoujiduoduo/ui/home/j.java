package com.shoujiduoduo.ui.home;

import com.qq.e.ads.banner.AbstractBannerADListener;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.util.am;

/* compiled from: DuoduoAdContainer */
class j extends AbstractBannerADListener {

    /* renamed from: a  reason: collision with root package name */
    boolean f1151a = true;
    final /* synthetic */ DuoduoAdContainer b;

    j(DuoduoAdContainer duoduoAdContainer) {
        this.b = duoduoAdContainer;
    }

    public void onNoAD(int i) {
        a.a(DuoduoAdContainer.f1122a, "onNoAd tencent");
        this.b.y.sendMessage(this.b.y.obtainMessage(1106, am.a.TENCENT));
    }

    public void onADReceiv() {
        if (this.f1151a) {
            this.b.b.setVisibility(4);
            this.f1151a = false;
            this.b.y.sendMessage(this.b.y.obtainMessage(1105, am.a.TENCENT));
        }
    }
}
