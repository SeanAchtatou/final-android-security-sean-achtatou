package com.shoujiduoduo.ui.home;

import android.os.Handler;
import android.os.Message;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.util.am;
import java.util.Timer;

/* compiled from: DuoduoAdContainer */
class p extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DuoduoAdContainer f1157a;

    p(DuoduoAdContainer duoduoAdContainer) {
        this.f1157a = duoduoAdContainer;
    }

    public void handleMessage(Message message) {
        switch (message.what) {
            case 1104:
                am.a unused = this.f1157a.o = this.f1157a.getNextAdSource();
                a.a(DuoduoAdContainer.f1122a, "next ad source = " + this.f1157a.o);
                if (this.f1157a.o == null) {
                    if (this.f1157a.c != null) {
                        this.f1157a.c.setVisibility(8);
                    }
                    if (this.f1157a.e != null) {
                        this.f1157a.e.setVisibility(8);
                    }
                    if (this.f1157a.b != null) {
                        this.f1157a.b.setVisibility(8);
                    }
                    if (this.f1157a.k != null) {
                        this.f1157a.k.setVisibility(8);
                    }
                    if (this.f1157a.d != null) {
                        this.f1157a.d.setVisibility(8);
                    }
                    if (this.f1157a.p != null) {
                        this.f1157a.p.cancel();
                        Timer unused2 = this.f1157a.p = (Timer) null;
                    }
                    Timer unused3 = this.f1157a.p = new Timer();
                    this.f1157a.p.schedule(new q(this), 10000);
                    return;
                }
                this.f1157a.m();
                return;
            case 1105:
                am.a aVar = (am.a) message.obj;
                a.a(DuoduoAdContainer.f1122a, aVar + " ad ready.");
                if (aVar == am.a.BAIDU) {
                    boolean unused4 = this.f1157a.h = true;
                } else if (aVar == am.a.TENCENT) {
                    boolean unused5 = this.f1157a.i = true;
                } else if (aVar == am.a.TAOBAO) {
                    boolean unused6 = this.f1157a.j = true;
                }
                if (this.f1157a.o != null && aVar == this.f1157a.o) {
                    am.a unused7 = this.f1157a.o = aVar;
                    this.f1157a.m();
                    return;
                }
                return;
            case 1106:
                am.a aVar2 = (am.a) message.obj;
                if (!this.f1157a.m && aVar2 == this.f1157a.o && this.f1157a.y != null) {
                    this.f1157a.y.sendEmptyMessage(1104);
                    return;
                }
                return;
            default:
                return;
        }
    }
}
