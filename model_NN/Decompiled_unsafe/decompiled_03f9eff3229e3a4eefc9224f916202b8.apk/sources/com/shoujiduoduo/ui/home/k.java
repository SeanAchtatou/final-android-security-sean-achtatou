package com.shoujiduoduo.ui.home;

import com.baidu.mobads.AdView;
import com.baidu.mobads.AdViewListener;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.util.am;
import com.umeng.analytics.b;
import java.util.HashMap;
import org.json.JSONObject;

/* compiled from: DuoduoAdContainer */
class k implements AdViewListener {

    /* renamed from: a  reason: collision with root package name */
    boolean f1152a = true;
    final /* synthetic */ long b;
    final /* synthetic */ DuoduoAdContainer c;

    k(DuoduoAdContainer duoduoAdContainer, long j) {
        this.c = duoduoAdContainer;
        this.b = j;
    }

    public void onAdSwitch() {
        a.a(DuoduoAdContainer.f1122a, "onAdSwitch baidu");
    }

    public void onAdClose(JSONObject jSONObject) {
    }

    public void onAdShow(JSONObject jSONObject) {
        a.a(DuoduoAdContainer.f1122a, "onAdShow baidu," + jSONObject.toString());
    }

    public void onAdReady(AdView adView) {
        a.a(DuoduoAdContainer.f1122a, "onAdReady baidu");
        if (this.f1152a) {
            this.f1152a = false;
            this.c.y.sendMessage(this.c.y.obtainMessage(1105, am.a.BAIDU));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.ui.home.DuoduoAdContainer.a(com.shoujiduoduo.ui.home.DuoduoAdContainer, boolean):boolean
     arg types: [com.shoujiduoduo.ui.home.DuoduoAdContainer, int]
     candidates:
      com.shoujiduoduo.ui.home.DuoduoAdContainer.a(com.shoujiduoduo.ui.home.DuoduoAdContainer, int):int
      com.shoujiduoduo.ui.home.DuoduoAdContainer.a(com.shoujiduoduo.ui.home.DuoduoAdContainer, com.shoujiduoduo.util.am$a):com.shoujiduoduo.util.am$a
      com.shoujiduoduo.ui.home.DuoduoAdContainer.a(com.shoujiduoduo.ui.home.DuoduoAdContainer, java.util.ArrayList):java.util.ArrayList
      com.shoujiduoduo.ui.home.DuoduoAdContainer.a(com.shoujiduoduo.ui.home.DuoduoAdContainer, java.util.Timer):java.util.Timer
      com.shoujiduoduo.ui.home.DuoduoAdContainer.a(android.view.ViewGroup, java.lang.String):void
      com.shoujiduoduo.ui.home.DuoduoAdContainer.a(com.shoujiduoduo.ui.home.DuoduoAdContainer, boolean):boolean */
    public void onAdFailed(String str) {
        a.a(DuoduoAdContainer.f1122a, "onAdFailed baidu, resson:" + str);
        if (!this.c.n) {
            boolean unused = this.c.n = true;
            HashMap hashMap = new HashMap();
            hashMap.put("reason", str);
            b.a(RingDDApp.c(), "BAIDU_BANNER_FAILED", hashMap, (int) (System.currentTimeMillis() - this.b));
        }
        this.c.y.sendMessage(this.c.y.obtainMessage(1106, am.a.BAIDU));
    }

    public void onAdClick(JSONObject jSONObject) {
        a.e("", "onAdClick " + jSONObject.toString());
    }
}
