package com.shoujiduoduo.ui.home;

import android.view.View;
import com.shoujiduoduo.base.bean.d;
import com.shoujiduoduo.util.i;
import com.shoujiduoduo.util.q;

/* compiled from: DuoduoAdView */
class v implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DuoduoAdView f1163a;

    v(DuoduoAdView duoduoAdView) {
        this.f1163a = duoduoAdView;
    }

    public void onClick(View view) {
        d a2;
        if (this.f1163a.m >= 0 && (a2 = this.f1163a.g.a(this.f1163a.m)) != null) {
            i.a(new w(this, a2.b));
            q.a(this.f1163a.f).a(a2.d, a2.b);
        }
    }
}
