package com.shoujiduoduo.ui.home;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;
import android.widget.ListAdapter;
import android.widget.SectionIndexer;
import android.widget.TextView;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.util.widget.IndexListView;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

/* compiled from: ChangeAreaDialog */
public class b extends Dialog {
    /* access modifiers changed from: private */
    public static final String h = b.class.getSimpleName();

    /* renamed from: a  reason: collision with root package name */
    Context f1142a;
    IndexListView b;
    a c;
    String[] d;
    String[] e;
    int f = -1;
    String g;

    public b(Context context, int i, String str) {
        super(context, i);
        this.f1142a = context;
        this.g = str;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.dialog_change_area);
        this.d = this.f1142a.getResources().getStringArray(R.array.city_list);
        this.e = this.f1142a.getResources().getStringArray(R.array.city_list_alpha);
        this.c = new a(this.f1142a, this.d, this.e);
        this.b = (IndexListView) findViewById(R.id.area_list);
        this.b.setFastScrollEnabled(true);
        setCanceledOnTouchOutside(true);
        this.b.setAdapter((ListAdapter) this.c);
        this.b.setOnItemClickListener(new c(this));
    }

    /* renamed from: com.shoujiduoduo.ui.home.b$b  reason: collision with other inner class name */
    /* compiled from: ChangeAreaDialog */
    private static class C0026b {

        /* renamed from: a  reason: collision with root package name */
        TextView f1144a;
        CheckedTextView b;

        private C0026b() {
        }

        /* synthetic */ C0026b(c cVar) {
            this();
        }
    }

    /* compiled from: ChangeAreaDialog */
    private class a extends BaseAdapter implements SectionIndexer {
        private LayoutInflater b;
        private String[] c;
        private String[] d;
        private HashMap<String, Integer> e = new HashMap<>();
        private String[] f;

        public a(Context context, String[] strArr, String[] strArr2) {
            this.b = LayoutInflater.from(context);
            this.c = strArr;
            this.d = strArr2;
            for (int i = 0; i < strArr2.length; i++) {
                if (!this.e.containsKey(strArr2[i])) {
                    this.e.put(strArr2[i], Integer.valueOf(i));
                }
            }
            ArrayList arrayList = new ArrayList(this.e.keySet());
            Collections.sort(arrayList);
            this.f = new String[arrayList.size()];
            arrayList.toArray(this.f);
        }

        public int getCount() {
            return this.c.length;
        }

        public Object getItem(int i) {
            if (i < this.c.length) {
                return this.c[i];
            }
            return null;
        }

        public long getItemId(int i) {
            return (long) i;
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            C0026b bVar;
            if (view == null) {
                view = this.b.inflate((int) R.layout.listitem_area, (ViewGroup) null);
                C0026b bVar2 = new C0026b(null);
                bVar2.f1144a = (TextView) view.findViewById(R.id.area_list_alpha);
                bVar2.b = (CheckedTextView) view.findViewById(R.id.area_name);
                view.setTag(bVar2);
                bVar = bVar2;
            } else {
                bVar = (C0026b) view.getTag();
            }
            if (i == b.this.f) {
                bVar.b.setChecked(true);
            } else {
                bVar.b.setChecked(false);
            }
            bVar.b.setText(this.c[i]);
            String str = this.d[i];
            if (!(i + -1 >= 0 ? this.d[i - 1] : " ").equals(str)) {
                bVar.f1144a.setVisibility(0);
                bVar.f1144a.setText(str);
            } else {
                bVar.f1144a.setVisibility(8);
            }
            return view;
        }

        public int getPositionForSection(int i) {
            return this.e.get(this.f[i]).intValue();
        }

        public int getSectionForPosition(int i) {
            String str = this.d[i];
            for (int i2 = 0; i2 < this.f.length; i2++) {
                if (this.f[i2].equals(str)) {
                    return i2;
                }
            }
            return 0;
        }

        public Object[] getSections() {
            return this.f;
        }
    }
}
