package com.shoujiduoduo.ui.home;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.shoujiduoduo.a.a.b;
import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.a.c.k;
import com.shoujiduoduo.a.c.t;
import com.shoujiduoduo.b.c.g;
import com.shoujiduoduo.b.c.n;
import com.shoujiduoduo.base.bean.aa;
import com.shoujiduoduo.base.bean.f;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ui.utils.DDListFragment;
import com.shoujiduoduo.ui.utils.HtmlFragment;
import com.shoujiduoduo.ui.utils.pageindicator.TabPageIndicator;
import com.shoujiduoduo.util.as;
import com.shoujiduoduo.util.f;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: HomepageScene */
public class ab implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private Activity f1133a;
    private ViewPager b;
    /* access modifiers changed from: private */
    public List<Fragment> c = new ArrayList();
    private ArrayList<aa> d;
    /* access modifiers changed from: private */
    public TabPageIndicator e;
    private RelativeLayout f;
    private RelativeLayout g;
    private LinearLayout h;
    private boolean i;
    private t j = new ac(this);
    private k k = new ad(this);

    public ab(Activity activity) {
        this.f1133a = activity;
    }

    public void a() {
        c();
        x.a().a(b.OBSERVER_LIST_AREA, this.k);
        x.a().a(b.OBSERVER_TOP_LIST, this.j);
    }

    public void b() {
        x.a().b(b.OBSERVER_LIST_AREA, this.k);
        x.a().b(b.OBSERVER_TOP_LIST, this.j);
    }

    private void c() {
        com.shoujiduoduo.base.a.a.a("HomepageScene", "initView in");
        this.b = (ViewPager) this.f1133a.findViewById(R.id.vPager);
        this.b.setOffscreenPageLimit(6);
        this.b.setAdapter(new a(((FragmentActivity) this.f1133a).getSupportFragmentManager()));
        this.f = (RelativeLayout) this.f1133a.findViewById(R.id.failed_view);
        this.f.setOnClickListener(this);
        this.g = (RelativeLayout) this.f1133a.findViewById(R.id.loading_view);
        this.h = (LinearLayout) this.f1133a.findViewById(R.id.home_lists);
        this.e = (TabPageIndicator) this.f1133a.findViewById(R.id.indicator);
        this.e.setViewPager(this.b);
        this.i = com.shoujiduoduo.util.a.d();
        if (com.shoujiduoduo.a.b.b.f().c()) {
            f();
            d();
        } else {
            g();
            com.shoujiduoduo.base.a.a.a("HomepageScene", "top list dat is not ready,just wait");
        }
        com.shoujiduoduo.base.a.a.a("HomepageScene", "initView out");
    }

    /* access modifiers changed from: private */
    public void d() {
        n nVar;
        this.d = com.shoujiduoduo.a.b.b.f().d();
        Iterator<aa> it = this.d.iterator();
        while (it.hasNext()) {
            aa next = it.next();
            com.shoujiduoduo.base.a.a.a("HomepageScene", "listname:" + next.e + ", id:" + next.g + ", type:" + next.f);
            if (next.f.equals(aa.f817a)) {
                if (next.g == 20) {
                    if (f.v()) {
                        next.e = "彩铃榜";
                        nVar = new n(f.a.list_ring_recommon, "20", false, "");
                    } else {
                        next.e = "分享榜";
                        nVar = new n(f.a.list_ring_recommon, Constants.VIA_REPORT_TYPE_SHARE_TO_QZONE, false, "");
                    }
                } else if (next.g == 24) {
                    String a2 = as.a(this.f1133a, "user_area", "");
                    if (a2.equals("")) {
                        com.shoujiduoduo.base.a.a.a("HomepageScene", "全国榜");
                        next.e = "全国榜";
                        nVar = new n(f.a.list_ring_recommon, "24", false, "");
                    } else {
                        next.e = a2 + "榜";
                        com.shoujiduoduo.base.a.a.a("HomepageScene", "用户选择的地域榜:" + a2);
                        nVar = new n(f.a.list_ring_recommon, "25", false, a2);
                    }
                } else {
                    nVar = new n(f.a.list_ring_recommon, "" + next.g, false, "");
                }
                DDListFragment dDListFragment = new DDListFragment();
                Bundle bundle = new Bundle();
                if (next.g == 24 || next.g == 25) {
                    bundle.putBoolean("support_area", true);
                }
                if (this.i) {
                    bundle.putBoolean("support_feed_ad", true);
                }
                bundle.putBoolean("support_lazy_load", true);
                bundle.putString("adapter_type", "ring_list_adapter");
                dDListFragment.setArguments(bundle);
                dDListFragment.a(nVar);
                this.c.add(dDListFragment);
            } else if (next.f.equals(aa.b)) {
                DDListFragment dDListFragment2 = new DDListFragment();
                g gVar = new g("collect");
                Bundle bundle2 = new Bundle();
                bundle2.putString("adapter_type", "collect_list_adapter");
                bundle2.putBoolean("support_lazy_load", true);
                dDListFragment2.setArguments(bundle2);
                dDListFragment2.a(gVar);
                this.c.add(dDListFragment2);
            } else if (next.f.equals(aa.c)) {
                DDListFragment dDListFragment3 = new DDListFragment();
                Bundle bundle3 = new Bundle();
                bundle3.putString("adapter_type", "artist_list_adapter");
                bundle3.putBoolean("support_lazy_load", true);
                dDListFragment3.setArguments(bundle3);
                dDListFragment3.a(new com.shoujiduoduo.b.c.a("artist"));
                this.c.add(dDListFragment3);
            } else if (next.f.equals(aa.d)) {
                HtmlFragment htmlFragment = new HtmlFragment();
                Bundle bundle4 = new Bundle();
                bundle4.putString("url", next.h);
                htmlFragment.setArguments(bundle4);
                this.c.add(htmlFragment);
            } else {
                com.shoujiduoduo.base.a.a.e("HomepageScene", "不支持的列表类型，跳过吧。");
            }
        }
        this.b.getAdapter().notifyDataSetChanged();
        this.b.setCurrentItem(0);
    }

    /* access modifiers changed from: private */
    public void e() {
        this.f.setVisibility(0);
        this.g.setVisibility(4);
        this.h.setVisibility(4);
    }

    /* access modifiers changed from: private */
    public void f() {
        this.f.setVisibility(4);
        this.g.setVisibility(4);
        this.h.setVisibility(0);
    }

    private void g() {
        this.f.setVisibility(4);
        this.g.setVisibility(0);
        this.h.setVisibility(4);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.failed_view:
                com.shoujiduoduo.base.a.a.a("HomepageScene", "retry load top list data");
                g();
                com.shoujiduoduo.a.b.b.f().e();
                return;
            default:
                return;
        }
    }

    /* compiled from: HomepageScene */
    private class a extends FragmentPagerAdapter {
        public CharSequence getPageTitle(int i) {
            if (com.shoujiduoduo.a.b.b.f().c()) {
                return com.shoujiduoduo.a.b.b.f().d().get(i).e;
            }
            return "";
        }

        public a(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        public Fragment getItem(int i) {
            if (com.shoujiduoduo.a.b.b.f().c() && ab.this.c.size() > 0) {
                return (Fragment) ab.this.c.get(i % ab.this.c.size());
            }
            com.shoujiduoduo.base.a.a.c("HomepageScene", "return null fragment 2");
            return null;
        }

        public int getCount() {
            if (!com.shoujiduoduo.a.b.b.f().c() || ab.this.c.size() <= 0) {
                return 0;
            }
            return com.shoujiduoduo.a.b.b.f().d().size();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x004a A[EDGE_INSN: B:19:0x004a->B:8:0x004a ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:3:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(java.lang.String r7, java.lang.String r8) {
        /*
            r6 = this;
            r3 = 0
            com.shoujiduoduo.b.c.m r0 = com.shoujiduoduo.a.b.b.f()
            java.util.ArrayList r0 = r0.d()
            r6.d = r0
            android.app.Activity r0 = r6.f1133a
            java.lang.String r1 = "user_area"
            com.shoujiduoduo.util.as.c(r0, r1, r7)
            java.util.ArrayList<com.shoujiduoduo.base.bean.aa> r0 = r6.d
            java.util.Iterator r1 = r0.iterator()
        L_0x0018:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x004a
            java.lang.Object r0 = r1.next()
            com.shoujiduoduo.base.bean.aa r0 = (com.shoujiduoduo.base.bean.aa) r0
            int r2 = r0.g
            r4 = 24
            if (r2 == r4) goto L_0x0030
            int r2 = r0.g
            r4 = 25
            if (r2 != r4) goto L_0x0018
        L_0x0030:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.StringBuilder r1 = r1.append(r7)
            java.lang.String r2 = "榜"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.e = r1
            com.shoujiduoduo.ui.utils.pageindicator.TabPageIndicator r0 = r6.e
            r0.a()
        L_0x004a:
            r2 = r3
        L_0x004b:
            java.util.List<android.support.v4.app.Fragment> r0 = r6.c
            int r0 = r0.size()
            if (r2 >= r0) goto L_0x0083
            java.util.List<android.support.v4.app.Fragment> r0 = r6.c
            java.lang.Object r0 = r0.get(r2)
            android.support.v4.app.Fragment r0 = (android.support.v4.app.Fragment) r0
            boolean r1 = r0 instanceof com.shoujiduoduo.ui.utils.DDListFragment
            if (r1 == 0) goto L_0x007f
            r1 = r0
            com.shoujiduoduo.ui.utils.DDListFragment r1 = (com.shoujiduoduo.ui.utils.DDListFragment) r1
            java.lang.String r1 = r1.b()
            boolean r1 = r8.equals(r1)
            if (r1 == 0) goto L_0x007f
            com.shoujiduoduo.b.c.n r1 = new com.shoujiduoduo.b.c.n
            com.shoujiduoduo.base.bean.f$a r4 = com.shoujiduoduo.base.bean.f.a.list_ring_recommon
            java.lang.String r5 = "25"
            r1.<init>(r4, r5, r3, r7)
            com.shoujiduoduo.ui.utils.DDListFragment r0 = (com.shoujiduoduo.ui.utils.DDListFragment) r0
            r0.a(r1)
            com.shoujiduoduo.ui.utils.pageindicator.TabPageIndicator r0 = r6.e
            r0.a()
        L_0x007f:
            int r0 = r2 + 1
            r2 = r0
            goto L_0x004b
        L_0x0083:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.ui.home.ab.a(java.lang.String, java.lang.String):void");
    }
}
