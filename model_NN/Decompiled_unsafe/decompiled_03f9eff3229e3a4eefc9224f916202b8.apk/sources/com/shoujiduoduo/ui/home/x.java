package com.shoujiduoduo.ui.home;

import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.ui.home.DuoduoAdView;
import com.shoujiduoduo.ui.utils.d;
import com.shoujiduoduo.util.am;

/* compiled from: DuoduoAdView */
class x extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DuoduoAdView f1165a;

    x(DuoduoAdView duoduoAdView) {
        this.f1165a = duoduoAdView;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.ui.home.DuoduoAdView.a(com.shoujiduoduo.ui.home.DuoduoAdView, boolean):boolean
     arg types: [com.shoujiduoduo.ui.home.DuoduoAdView, int]
     candidates:
      com.shoujiduoduo.ui.home.DuoduoAdView.a(com.shoujiduoduo.ui.home.DuoduoAdView, java.lang.String):int
      com.shoujiduoduo.ui.home.DuoduoAdView.a(com.shoujiduoduo.base.bean.c, int):void
      com.shoujiduoduo.a.c.f.a(com.shoujiduoduo.base.bean.c, int):void
      com.shoujiduoduo.ui.home.DuoduoAdView.a(com.shoujiduoduo.ui.home.DuoduoAdView, boolean):boolean */
    public void handleMessage(Message message) {
        int i;
        switch (message.what) {
            case 331:
                a.d(DuoduoAdView.c, "MESSAGE_FINISH_LOAD_IMAGE got!");
                DuoduoAdView.a aVar = (DuoduoAdView.a) message.obj;
                try {
                    this.f1165a.f1125a[this.f1165a.a(aVar.b)] = aVar.f1126a;
                    if (this.f1165a.m < 0) {
                        this.f1165a.d();
                        boolean unused = this.f1165a.n = true;
                        this.f1165a.d.a();
                        String a2 = am.a().a("ad_switch_time");
                        if (a2 == null) {
                            a2 = "10000";
                        }
                        try {
                            int intValue = Integer.valueOf(a2).intValue();
                            if (intValue <= 0) {
                                intValue = 10000;
                            }
                            i = intValue;
                        } catch (NumberFormatException e) {
                            i = 10000;
                        }
                        try {
                            this.f1165a.h.schedule(this.f1165a.l, (long) i, (long) i);
                        } catch (IllegalStateException e2) {
                        }
                    }
                    a.d(DuoduoAdView.c, "MESSAGE_FINISH_LOAD_IMAGE handler finishes!");
                    return;
                } catch (Exception e3) {
                    return;
                }
            case 332:
                a.a(DuoduoAdView.c, "DuoduoAdView: get MESSAGE_FINISH_LOAD_AD");
                this.f1165a.f1125a = new Drawable[this.f1165a.g.b()];
                for (int i2 = 0; i2 < this.f1165a.g.b(); i2++) {
                    Drawable a3 = d.a(this.f1165a.g.a(i2).f820a, new y(this));
                    if (a3 != null) {
                        Message obtainMessage = this.f1165a.b.obtainMessage(331, new DuoduoAdView.a(a3, this.f1165a.g.a(i2).f820a));
                        a.a(DuoduoAdView.c, "AsyncImageLoader in DuoduoAdView: finish load image: " + this.f1165a.g.a(i2).f820a);
                        a.d(DuoduoAdView.c, "ready to sendMessage.");
                        this.f1165a.b.sendMessage(obtainMessage);
                        a.d(DuoduoAdView.c, "sendMessage returns!");
                    }
                }
                a.d(DuoduoAdView.c, "MESSAGE_FINISH_LOAD_AD handler finishes!");
                return;
            case 333:
                this.f1165a.d();
                return;
            default:
                return;
        }
    }
}
