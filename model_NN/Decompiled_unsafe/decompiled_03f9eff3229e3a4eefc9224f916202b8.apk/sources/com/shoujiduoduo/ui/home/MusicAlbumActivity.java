package com.shoujiduoduo.ui.home;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.media.TransportMediator;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.TextView;
import cn.banshenggua.aichang.utils.Constants;
import com.shoujiduoduo.base.bean.ab;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ui.utils.BaseActivity;
import com.shoujiduoduo.util.ak;
import com.shoujiduoduo.util.av;
import com.shoujiduoduo.util.ax;
import com.shoujiduoduo.util.widget.a;
import com.tencent.open.SocialConstants;
import com.umeng.analytics.b;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

public class MusicAlbumActivity extends BaseActivity implements View.OnClickListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public WebView f1127a;
    private TextView b;
    private String c;
    /* access modifiers changed from: private */
    public View d;
    /* access modifiers changed from: private */
    public ValueCallback<Uri> e;
    /* access modifiers changed from: private */
    public ValueCallback<Uri[]> f;
    private boolean g;
    /* access modifiers changed from: private */
    public boolean h;
    private a i;
    private String j;

    public enum a {
        my_album,
        create_album,
        ring_story,
        create_ring_story
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        String a2;
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_music_album);
        PlayerService b2 = ak.a().b();
        if (b2 != null && b2.j()) {
            b2.k();
        }
        findViewById(R.id.backButton).setOnClickListener(this);
        this.b = (TextView) findViewById(R.id.header_text);
        this.d = findViewById(R.id.loading_view);
        this.d.setVisibility(0);
        this.f1127a = (WebView) findViewById(R.id.webview_window);
        this.f1127a.setVisibility(4);
        String str = "";
        this.i = a.create_album;
        Intent intent = getIntent();
        if (intent != null) {
            String stringExtra = intent.getStringExtra("musicid");
            if (!av.c(stringExtra)) {
                str = stringExtra;
            }
            com.shoujiduoduo.base.a.a.a("MusicAlbumActivity", "musicId:" + str);
            a aVar = (a) intent.getSerializableExtra("type");
            if (aVar != null) {
                this.i = aVar;
                com.shoujiduoduo.base.a.a.a("MusicAlbumActivity", "web type:" + this.i);
                if (this.i == a.create_ring_story) {
                    this.c = intent.getStringExtra(SocialConstants.PARAM_APP_DESC);
                }
            }
            a2 = intent.getStringExtra("url");
            if (av.c(a2)) {
                a2 = a(this.i);
            }
            String stringExtra2 = intent.getStringExtra("title");
            if (!av.c(stringExtra2)) {
                this.b.setText(stringExtra2);
            }
        } else {
            a2 = a(a.my_album);
        }
        com.shoujiduoduo.base.a.a.a("MusicAlbumActivity", "base url:" + a2);
        this.j = a(a2, str);
        com.shoujiduoduo.base.a.a.a("MusicAlbumActivity", "url:" + this.j);
        WebSettings settings = this.f1127a.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setUseWideViewPort(true);
        settings.setLoadWithOverviewMode(true);
        settings.setBuiltInZoomControls(false);
        settings.setDomStorageEnabled(true);
        settings.setSupportZoom(false);
        this.f1127a.requestFocus(TransportMediator.KEYCODE_MEDIA_RECORD);
        this.f1127a.setOnTouchListener(new ae(this));
        this.f1127a.setWebViewClient(new af(this));
        this.f1127a.setWebChromeClient(new WebChromeClient() {
            public void openFileChooser(ValueCallback<Uri> valueCallback) {
                customOpenFileChooser(valueCallback);
            }

            /* access modifiers changed from: protected */
            public void openFileChooser(ValueCallback valueCallback, String str) {
                customOpenFileChooser(valueCallback);
            }

            /* access modifiers changed from: protected */
            public void openFileChooser(ValueCallback<Uri> valueCallback, String str, String str2) {
                customOpenFileChooser(valueCallback);
            }

            public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> valueCallback, WebChromeClient.FileChooserParams fileChooserParams) {
                com.shoujiduoduo.base.a.a.a("musicalbum", "onShowFileChooser");
                ValueCallback unused = MusicAlbumActivity.this.f = valueCallback;
                Intent intent = new Intent("android.intent.action.GET_CONTENT");
                intent.addCategory("android.intent.category.OPENABLE");
                intent.setType("image/*");
                MusicAlbumActivity.this.startActivityForResult(Intent.createChooser(intent, "选择文件"), Constants.REGISTER_OK);
                return true;
            }

            /* access modifiers changed from: protected */
            public void customOpenFileChooser(ValueCallback<Uri> valueCallback) {
                com.shoujiduoduo.base.a.a.a("musicalbum", "customOpenFileChooser");
                ValueCallback unused = MusicAlbumActivity.this.e = valueCallback;
                Intent intent = new Intent("android.intent.action.GET_CONTENT");
                intent.addCategory("android.intent.category.OPENABLE");
                intent.setType("image/*");
                MusicAlbumActivity.this.startActivityForResult(Intent.createChooser(intent, "选择文件"), Constants.REGISTER_OK);
            }
        });
        if (Build.VERSION.SDK_INT >= 17) {
            this.f1127a.getSettings().setMediaPlaybackRequiresUserGesture(false);
        }
        this.f1127a.loadUrl(this.j);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.backButton:
                if (this.f1127a == null) {
                    finish();
                    return;
                } else if (!this.g && this.h && !a.ring_story.equals(this.i)) {
                    new a.C0034a(this).a("是否将音乐相册分享给好友呢？").a("确定", new ai(this)).b("取消", new ah(this)).a().show();
                    return;
                } else if (this.f1127a.canGoBack()) {
                    this.f1127a.goBack();
                    return;
                } else {
                    finish();
                    return;
                }
            default:
                return;
        }
    }

    private String a(a aVar) {
        switch (aVar) {
            case create_album:
                String c2 = b.c(RingDDApp.c(), "create_music_album_url");
                if (TextUtils.isEmpty(c2)) {
                    return "http://musicalbum.shoujiduoduo.com/malbum/serv/createalbum.php?ddsrc=ring_ar";
                }
                return c2;
            case my_album:
                String c3 = b.c(RingDDApp.c(), "music_album_url");
                if (TextUtils.isEmpty(c3)) {
                    return "http://musicalbum.shoujiduoduo.com/malbum/serv/myalbums.php?ddsrc=ring_ar";
                }
                return c3;
            case ring_story:
                String c4 = b.c(RingDDApp.c(), "ring_story_url");
                if (TextUtils.isEmpty(c4)) {
                    return "http://musicstory1.csoot.com/mstory/serv/hotstory.php?ddsrc=ring_ar";
                }
                return c4;
            case create_ring_story:
                String c5 = b.c(RingDDApp.c(), "create_ring_story_url");
                if (TextUtils.isEmpty(c5)) {
                    return "http://musicalbum.shoujiduoduo.com/mstory/serv/edit.php?local=1&ddsrc=upload_ring_ar";
                }
                return c5;
            default:
                return "";
        }
    }

    private String a(String str, String str2) {
        String str3;
        String str4;
        int indexOf;
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        ab c2 = com.shoujiduoduo.a.b.b.g().c();
        String a2 = c2.a();
        com.shoujiduoduo.base.a.a.a("MusicAlbumActivity", "uid:" + a2);
        if (!TextUtils.isEmpty(a2) && (indexOf = a2.indexOf("_")) > 0) {
            a2 = a2.substring(indexOf + 1);
        }
        com.shoujiduoduo.base.a.a.a("MusicAlbumActivity", "dduid:" + a2);
        switch (c2.e()) {
            case 2:
                str3 = a2;
                str4 = "qq";
                break;
            case 3:
                str3 = a2;
                str4 = "wb";
                break;
            case 4:
            default:
                str3 = "";
                str4 = "dd";
                break;
            case 5:
                str3 = a2;
                str4 = "wx";
                break;
        }
        sb.append("&dduid=").append(str3);
        com.shoujiduoduo.base.a.a.a("MusicAlbumActivity", "dddid:" + RingDDApp.f847a);
        sb.append("&dddid=").append(RingDDApp.f847a);
        com.shoujiduoduo.base.a.a.a("MusicAlbumActivity", "ddut:" + str4);
        sb.append("&ddut=").append(str4);
        sb.append("&needstory=1");
        if (!TextUtils.isEmpty(str2)) {
            sb.append("&musicid=").append(str2);
        }
        try {
            sb.append("&nickname=").append(URLEncoder.encode(c2.b(), "UTF-8"));
            sb.append("&portrait=").append(URLEncoder.encode(c2.c(), "UTF-8"));
            if (!av.c(this.c)) {
                sb.append("&desc=").append(URLEncoder.encode(this.c, "UTF-8"));
            }
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
        }
        return sb.toString();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (i2 == 102) {
            a(i3, intent);
        }
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        if (str.startsWith("ddip://")) {
            int indexOf = str.indexOf("//");
            int indexOf2 = str.indexOf("/", indexOf + 2);
            int indexOf3 = str.indexOf("?", indexOf2 + 1);
            if (indexOf != -1 && indexOf2 != -1 && indexOf3 != -1) {
                String str2 = "";
                try {
                    str2 = str.substring(indexOf2 + 1, indexOf3);
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
                if (!TextUtils.isEmpty(str2)) {
                    String str3 = "";
                    if (str.indexOf("=") != -1) {
                        try {
                            str3 = URLDecoder.decode(str.substring(str.indexOf("=") + 1), "UTF-8");
                            com.shoujiduoduo.base.a.a.a("MusicAlbumActivity", "param:" + str3);
                        } catch (UnsupportedEncodingException e3) {
                            e3.printStackTrace();
                        }
                    }
                    if (str2.equals("w2c_share")) {
                        b(str3);
                    } else if (!str2.equals("w2c_setRing")) {
                        com.shoujiduoduo.base.a.a.e("MusicAlbumActivity", "not support method, " + str2);
                    }
                }
            }
        }
    }

    private void b(String str) {
        this.g = true;
        try {
            JSONObject jSONObject = (JSONObject) new JSONTokener(str).nextValue();
            ax.a().a(this, jSONObject.optString("title"), jSONObject.optString(SocialConstants.PARAM_APP_DESC), jSONObject.optString("imgUrl"), jSONObject.optString("link"));
        } catch (JSONException e2) {
            e2.printStackTrace();
        } catch (Exception e3) {
        }
    }

    private void a(int i2, Intent intent) {
        String str;
        if (Build.VERSION.SDK_INT < 21) {
            Uri data = (intent == null || i2 != -1) ? null : intent.getData();
            if ("smartisan".equals(Build.BRAND)) {
                try {
                    Cursor managedQuery = managedQuery(data, new String[]{"_data"}, null, null, null);
                    int columnIndexOrThrow = managedQuery.getColumnIndexOrThrow("_data");
                    managedQuery.moveToFirst();
                    str = managedQuery.getString(columnIndexOrThrow);
                } catch (Exception e2) {
                    str = null;
                }
                if (!TextUtils.isEmpty(str)) {
                    try {
                        data = Uri.fromFile(new File(str));
                    } catch (Exception e3) {
                    }
                }
            }
            if (this.e != null) {
                this.e.onReceiveValue(data);
            } else {
                com.shoujiduoduo.base.a.a.c("MusicAlbumActivity", "mUploadMsgOld is null");
            }
        } else if (this.f != null) {
            this.f.onReceiveValue(WebChromeClient.FileChooserParams.parseResult(i2, intent));
        } else {
            com.shoujiduoduo.base.a.a.c("MusicAlbumActivity", "mUploadMsg is null");
        }
        this.f = null;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.f1127a.loadUrl("about:blank");
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4) {
            return super.onKeyDown(i2, keyEvent);
        }
        findViewById(R.id.backButton).performClick();
        return true;
    }
}
