package com.shoujiduoduo.ui.home;

import android.text.TextUtils;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.shoujiduoduo.base.a.a;

/* compiled from: MusicAlbumActivity */
class af extends WebViewClient {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ MusicAlbumActivity f1138a;

    af(MusicAlbumActivity musicAlbumActivity) {
        this.f1138a = musicAlbumActivity;
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        a.a("MusicAlbumActivity", "override url:" + str);
        if (str.startsWith("ddip://")) {
            this.f1138a.a(str);
            return true;
        }
        webView.loadUrl(str);
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.ui.home.MusicAlbumActivity.a(com.shoujiduoduo.ui.home.MusicAlbumActivity, boolean):boolean
     arg types: [com.shoujiduoduo.ui.home.MusicAlbumActivity, int]
     candidates:
      com.shoujiduoduo.ui.home.MusicAlbumActivity.a(com.shoujiduoduo.ui.home.MusicAlbumActivity, android.webkit.ValueCallback):android.webkit.ValueCallback
      com.shoujiduoduo.ui.home.MusicAlbumActivity.a(java.lang.String, java.lang.String):java.lang.String
      com.shoujiduoduo.ui.home.MusicAlbumActivity.a(int, android.content.Intent):void
      com.shoujiduoduo.ui.home.MusicAlbumActivity.a(com.shoujiduoduo.ui.home.MusicAlbumActivity, java.lang.String):void
      com.shoujiduoduo.ui.home.MusicAlbumActivity.a(com.shoujiduoduo.ui.home.MusicAlbumActivity, boolean):boolean */
    public void onPageFinished(WebView webView, String str) {
        super.onPageFinished(webView, str);
        a.a("MusicAlbumActivity", "onPageFinished, url:" + str);
        if (TextUtils.isEmpty(str) || !str.contains("/album.php")) {
            a.a("MusicAlbumActivity", "not in create album view");
            boolean unused = this.f1138a.h = false;
        } else {
            a.a("MusicAlbumActivity", "in create album view");
            boolean unused2 = this.f1138a.h = true;
        }
        this.f1138a.d.setVisibility(4);
        this.f1138a.f1127a.setVisibility(0);
        this.f1138a.runOnUiThread(new ag(this));
    }
}
