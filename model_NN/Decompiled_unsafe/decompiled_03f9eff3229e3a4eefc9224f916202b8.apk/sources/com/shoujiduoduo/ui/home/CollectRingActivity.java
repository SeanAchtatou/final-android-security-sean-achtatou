package com.shoujiduoduo.ui.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import cn.banshenggua.aichang.utils.Constants;
import com.d.a.b.d;
import com.shoujiduoduo.b.c.n;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.base.bean.b;
import com.shoujiduoduo.base.bean.f;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ui.utils.BaseFragmentActivity;
import com.shoujiduoduo.ui.utils.DDListFragment;
import com.shoujiduoduo.ui.utils.v;
import com.shoujiduoduo.util.ak;
import com.shoujiduoduo.util.u;
import java.text.DecimalFormat;

public class CollectRingActivity extends BaseFragmentActivity {

    /* renamed from: a  reason: collision with root package name */
    private TextView f1121a;
    private DDListFragment b;
    private Button c;
    private Button d;
    /* access modifiers changed from: private */
    public b e;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_collect_ring);
        this.c = (Button) findViewById(R.id.fav_btn);
        this.d = (Button) findViewById(R.id.share_btn);
        this.f1121a = (TextView) findViewById(R.id.header_title);
        findViewById(R.id.backButton).setOnClickListener(new e(this));
        Intent intent = getIntent();
        if (intent != null) {
            this.e = (b) RingDDApp.b().a(intent.getStringExtra("parakey"));
            if (this.e != null) {
                this.f1121a.setText(this.e.b);
                a();
                this.b.a(new n(f.a.list_ring_collect, this.e.g, false, ""));
                b();
                return;
            }
            a.c("CollectListActivity", "wrong collect data prarm");
            return;
        }
        a.c("CollectListActivity", "wrong intent null");
    }

    private void a() {
        this.b = new DDListFragment();
        Bundle bundle = new Bundle();
        bundle.putString("adapter_type", "ring_list_adapter");
        this.b.setArguments(bundle);
        FragmentTransaction beginTransaction = getSupportFragmentManager().beginTransaction();
        beginTransaction.add((int) R.id.list_layout, this.b);
        beginTransaction.commitAllowingStateLoss();
    }

    private void b() {
        TextView textView = (TextView) findViewById(R.id.fav_num);
        d.a().a(this.e.f819a, (ImageView) findViewById(R.id.pic), v.a().g());
        ((TextView) findViewById(R.id.content)).setText(this.e.c);
        int a2 = u.a(this.e.f, Constants.CLEARIMGED);
        StringBuilder sb = new StringBuilder();
        if (a2 > 10000) {
            sb.append(new DecimalFormat("#.00").format((double) (((float) a2) / 10000.0f)));
            sb.append("万");
        } else {
            sb.append(a2);
        }
        textView.setText(sb.toString());
        this.c.setOnClickListener(new f(this));
        this.d.setOnClickListener(new g(this));
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        PlayerService b2 = ak.a().b();
        if (b2 != null && b2.j()) {
            b2.k();
        }
        super.onDestroy();
    }
}
