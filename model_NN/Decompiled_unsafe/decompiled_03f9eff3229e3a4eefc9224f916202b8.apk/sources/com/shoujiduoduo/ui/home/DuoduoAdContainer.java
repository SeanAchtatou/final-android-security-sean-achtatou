package com.shoujiduoduo.ui.home;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import cn.banshenggua.aichang.utils.Constants;
import com.alimama.mobile.sdk.MmuSDK;
import com.alimama.mobile.sdk.config.BannerProperties;
import com.alimama.mobile.sdk.config.MmuSDKFactory;
import com.baidu.mobads.AdSettings;
import com.baidu.mobads.AdView;
import com.baidu.mobads.BaiduManager;
import com.qq.e.ads.banner.ADSize;
import com.qq.e.ads.banner.BannerView;
import com.shoujiduoduo.a.a.b;
import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.a.c.e;
import com.shoujiduoduo.a.c.i;
import com.shoujiduoduo.a.c.q;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.util.am;
import com.shoujiduoduo.util.as;
import com.shoujiduoduo.util.f;
import com.shoujiduoduo.util.widget.a;
import java.util.ArrayList;
import java.util.Timer;

public class DuoduoAdContainer extends RelativeLayout implements i {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static String f1122a = DuoduoAdContainer.class.getSimpleName();
    /* access modifiers changed from: private */
    public View b = null;
    /* access modifiers changed from: private */
    public View c = null;
    /* access modifiers changed from: private */
    public RelativeLayout d = null;
    /* access modifiers changed from: private */
    public DuoduoAdView e = null;
    private BannerProperties f;
    private MmuSDK g;
    /* access modifiers changed from: private */
    public boolean h = false;
    /* access modifiers changed from: private */
    public boolean i = false;
    /* access modifiers changed from: private */
    public boolean j = false;
    /* access modifiers changed from: private */
    public ImageButton k = null;
    /* access modifiers changed from: private */
    public Context l;
    /* access modifiers changed from: private */
    public boolean m;
    /* access modifiers changed from: private */
    public boolean n;
    /* access modifiers changed from: private */
    public am.a o;
    /* access modifiers changed from: private */
    public Timer p = new Timer();
    private int q;
    private boolean r;
    /* access modifiers changed from: private */
    public ArrayList<am.b> s;
    private boolean t;
    private boolean u;
    /* access modifiers changed from: private */
    public int v;
    private e w = new l(this);
    private q x = new m(this);
    /* access modifiers changed from: private */
    public Handler y = new p(this);

    public DuoduoAdContainer(Context context) {
        super(context);
        this.l = context;
    }

    public DuoduoAdContainer(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.l = context;
    }

    public DuoduoAdContainer(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.l = context;
    }

    private void a(ViewGroup viewGroup, String str) {
        this.g = MmuSDKFactory.getMmuSDK();
        this.g.accountServiceInit(this.l);
        this.g.init(RingDDApp.b());
        this.g.accountServiceInit(RingDDApp.b());
        this.f = new BannerProperties(str, viewGroup);
        this.g.attach(this.f);
        this.f.setClickCallBackListener(new h(this));
        this.f.setOnStateChangeCallBackListener(new i(this));
    }

    private void f() {
        com.shoujiduoduo.base.a.a.a(f1122a, "initialize Taobao Ad");
        this.d = new RelativeLayout(this.l);
        this.d.setBackgroundResource(R.drawable.homepage_bkg_color);
        a(this.d, "61034");
        this.d.setGravity(1);
        this.d.setVisibility(8);
        addView(this.d, new RelativeLayout.LayoutParams(-1, -2));
        this.y.sendMessage(this.y.obtainMessage(1105, am.a.TAOBAO));
    }

    private void g() {
        com.shoujiduoduo.base.a.a.a(f1122a, "initialize duoduo Ad");
        this.e = new DuoduoAdView(this.l);
        this.e.setVisibility(8);
        this.e.setAdListener(this);
        addView(this.e);
    }

    private void h() {
        com.shoujiduoduo.base.a.a.a(f1122a, "initialize tencent Ad");
        this.b = new BannerView((Activity) this.l, ADSize.BANNER, "1101336966", "9079537217916261398");
        addView(this.b, new RelativeLayout.LayoutParams(-1, -2));
        this.b.setVisibility(8);
        ((BannerView) this.b).setRefresh(30);
        ((BannerView) this.b).setADListener(new j(this));
        ((BannerView) this.b).loadAD();
    }

    public void b() {
        if (com.shoujiduoduo.util.a.i() && this.c != null) {
            removeView(this.c);
            a(true);
        }
    }

    private void a(boolean z) {
        com.shoujiduoduo.base.a.a.a(f1122a, "initialize baidu Ad");
        BaiduManager.init(this.l);
        this.c = new AdView(this.l, "2296609");
        AdSettings.setKey(new String[]{"baidu", "中 国 "});
        ((AdView) this.c).setListener(new k(this, System.currentTimeMillis()));
        addView(this.c);
        if (!z) {
            this.c.setVisibility(8);
        }
    }

    public void c() {
        setVisibility(8);
        this.k = (ImageButton) findViewById(R.id.close_ad_button);
        this.k.setOnClickListener(new a(this, null));
        if (f.u().equals(f.b.ct)) {
            this.k.setImageResource(R.drawable.ad_close);
        }
        this.m = false;
        if (am.a().c()) {
            com.shoujiduoduo.base.a.a.a(f1122a, "ad config is updated");
        } else {
            com.shoujiduoduo.base.a.a.a(f1122a, "ad config is not updated");
        }
        this.s = am.a().d();
        if (this.s != null) {
            j();
            i();
        }
        x.a().a(b.OBSERVER_CONFIG, this.w);
        x.a().a(b.OBSERVER_SCENE, this.x);
        this.u = true;
    }

    private void i() {
        com.shoujiduoduo.base.a.a.a(f1122a, "original banner ad size:" + this.s.size());
        boolean z = false;
        for (int i2 = 0; i2 < this.s.size(); i2++) {
            switch (this.s.get(i2).c) {
                case DUODUO:
                    if (com.shoujiduoduo.util.a.j() && !z) {
                        this.q = i2;
                        this.o = am.a.DUODUO;
                        com.shoujiduoduo.base.a.a.a(f1122a, "first show AD : " + this.o + " order:" + this.q);
                        z = true;
                        break;
                    }
                case BAIDU:
                    if (com.shoujiduoduo.util.a.i() && !z) {
                        this.q = i2;
                        this.o = am.a.BAIDU;
                        com.shoujiduoduo.base.a.a.a(f1122a, "first show AD : " + this.o + " order:" + this.q);
                        z = true;
                        break;
                    }
                case TENCENT:
                    if (com.shoujiduoduo.util.a.g() && !z) {
                        this.q = i2;
                        this.o = am.a.TENCENT;
                        com.shoujiduoduo.base.a.a.a(f1122a, "first show AD : " + this.o + " order:" + this.q);
                        z = true;
                        break;
                    }
                case TAOBAO:
                    if (com.shoujiduoduo.util.a.h() && !z) {
                        this.q = i2;
                        this.o = am.a.TAOBAO;
                        com.shoujiduoduo.base.a.a.a(f1122a, "first show AD : " + this.o + " order:" + this.q);
                        z = true;
                        break;
                    }
            }
        }
        com.shoujiduoduo.base.a.a.a(f1122a, "finally banner ad size:" + this.s.size());
        if (!z) {
            com.shoujiduoduo.base.a.a.a(f1122a, "no ad when adview first initialized");
            this.o = null;
        }
    }

    private void j() {
        if (com.shoujiduoduo.util.a.j()) {
            g();
        }
        if (com.shoujiduoduo.util.a.i()) {
            a(false);
        }
        if (com.shoujiduoduo.util.a.g()) {
            h();
        }
        if (com.shoujiduoduo.util.a.h()) {
            f();
        }
        com.shoujiduoduo.base.a.a.a(f1122a, "finish initialize AD");
    }

    private class a implements View.OnClickListener {
        private a() {
        }

        /* synthetic */ a(DuoduoAdContainer duoduoAdContainer, h hVar) {
            this();
        }

        public void onClick(View view) {
            if (f.u().equals(f.b.ct)) {
                int a2 = as.a(DuoduoAdContainer.this.l, "close_banner_ad_times", 0);
                com.shoujiduoduo.base.a.a.a(DuoduoAdContainer.f1122a, "close ad times:" + a2);
                if (a2 < 3 && !com.shoujiduoduo.a.b.b.g().h()) {
                    new a.C0034a(DuoduoAdContainer.this.l).b("关闭广告").a("开通VIP会员可永久去除广告，同时尊享20万免费彩铃等多项特权.").a("了解会员", new s(this)).b("关闭一次", new r(this)).a().show();
                    as.b(DuoduoAdContainer.this.l, "close_banner_ad_times", a2 + 1);
                    return;
                }
            }
            DuoduoAdContainer.this.k();
        }
    }

    /* access modifiers changed from: private */
    public void k() {
        com.shoujiduoduo.base.a.a.a(f1122a, "ad close thread id: " + Thread.currentThread().getId());
        if (this.p != null) {
            this.p.cancel();
            this.p = null;
        }
        if (this.e != null) {
            this.e.b();
        }
        if (this.k != null) {
            this.k.setVisibility(8);
        }
        if (this.e != null) {
            this.e.setVisibility(8);
        }
        if (this.c != null) {
            this.c.setVisibility(8);
        }
        if (this.b != null) {
            this.b.setVisibility(8);
        }
        if (this.d != null) {
            this.d.setVisibility(8);
        }
        removeAllViews();
        this.k = null;
        this.e = null;
        this.c = null;
        this.b = null;
        this.d = null;
        this.r = true;
        com.shoujiduoduo.util.i.a(new n(this));
    }

    public void d() {
        if (this.p != null) {
            this.p.cancel();
            this.p = null;
        }
        if (this.e != null) {
            this.e.b();
        }
        x.a().b(b.OBSERVER_CONFIG, this.w);
        x.a().b(b.OBSERVER_SCENE, this.x);
    }

    private void l() {
        String str = null;
        try {
            if (this.q < this.s.size()) {
                String str2 = this.s.get(this.q).f1567a;
                com.shoujiduoduo.base.a.a.a(f1122a, "show ad:" + this.s.get(this.q).c + " duration:" + str2);
                str = str2;
            }
            if (str != null) {
                if (this.p != null) {
                    this.p.cancel();
                    this.p = null;
                }
                this.p = new Timer();
                this.p.schedule(new o(this), (long) (Integer.valueOf(str).intValue() * Constants.CLEARIMGED));
            }
        } catch (IllegalStateException e2) {
        }
    }

    /* access modifiers changed from: private */
    public am.a getNextAdSource() {
        if (this.s.size() <= 0) {
            return null;
        }
        for (int i2 = 0; i2 < this.s.size(); i2++) {
            this.q++;
            this.q %= this.s.size();
            com.shoujiduoduo.base.a.a.a(f1122a, "morder:" + this.q);
            am.a aVar = this.s.get(this.q).c;
            switch (aVar) {
                case DUODUO:
                    if (com.shoujiduoduo.util.a.j()) {
                        if (this.e != null) {
                            if (!this.e.a()) {
                                break;
                            } else {
                                return aVar;
                            }
                        } else {
                            com.shoujiduoduo.base.a.a.a(f1122a, "initializeDuoduoAd");
                            g();
                            break;
                        }
                    } else {
                        continue;
                    }
                case BAIDU:
                    if (com.shoujiduoduo.util.a.i()) {
                        if (this.c != null) {
                            if (!this.h) {
                                break;
                            } else {
                                return aVar;
                            }
                        } else {
                            com.shoujiduoduo.base.a.a.a(f1122a, "initializeBaiduAd");
                            a(false);
                            break;
                        }
                    } else {
                        continue;
                    }
                case TENCENT:
                    if (com.shoujiduoduo.util.a.g()) {
                        if (this.b != null) {
                            if (!this.i) {
                                break;
                            } else {
                                return aVar;
                            }
                        } else {
                            com.shoujiduoduo.base.a.a.a(f1122a, "initializeTencentAd");
                            h();
                            break;
                        }
                    } else {
                        continue;
                    }
                case TAOBAO:
                    if (com.shoujiduoduo.util.a.h()) {
                        if (this.d != null) {
                            if (!this.j) {
                                break;
                            } else {
                                return aVar;
                            }
                        } else {
                            com.shoujiduoduo.base.a.a.a(f1122a, "initializeTaobaoAd");
                            f();
                            break;
                        }
                    } else {
                        continue;
                    }
            }
        }
        return null;
    }

    private void a(am.a aVar) {
        int i2;
        int i3;
        int i4 = 4;
        if (!this.t || !am.a().a("adturbo_enable").equals("1")) {
            if (this.c != null) {
                this.c.setVisibility(aVar == am.a.BAIDU ? 0 : 4);
            }
            if (this.e != null) {
                DuoduoAdView duoduoAdView = this.e;
                if (aVar == am.a.DUODUO) {
                    i3 = 0;
                } else {
                    i3 = 4;
                }
                duoduoAdView.setVisibility(i3);
            }
            if (this.b != null) {
                View view = this.b;
                if (aVar == am.a.TENCENT) {
                    i2 = 0;
                } else {
                    i2 = 4;
                }
                view.setVisibility(i2);
            }
            if (this.d != null) {
                RelativeLayout relativeLayout = this.d;
                if (aVar == am.a.TAOBAO) {
                    i4 = 0;
                }
                relativeLayout.setVisibility(i4);
            }
        } else {
            if (this.c != null) {
                this.c.setVisibility(4);
            }
            if (this.e != null) {
                this.e.setVisibility(4);
            }
            if (this.b != null) {
                this.b.setVisibility(4);
            }
            if (this.d != null) {
                this.d.setVisibility(4);
            }
        }
        if (this.k != null) {
            this.k.setVisibility(0);
            this.k.bringToFront();
        }
    }

    /* access modifiers changed from: private */
    public void m() {
        com.shoujiduoduo.base.a.a.a(f1122a, "[banner]showAd");
        this.m = true;
        if (!(this.v == 2 || this.v == 3)) {
            setVisibility(0);
        }
        a(this.o);
        requestLayout();
        invalidate();
        l();
    }

    public void a() {
        if (this.y != null) {
            this.y.sendMessage(this.y.obtainMessage(1105, am.a.DUODUO));
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        super.onLayout(z, i2, i3, i4, i5);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        int measuredHeight;
        super.onMeasure(i2, i3);
        if (this.o != null && !this.r) {
            switch (this.o) {
                case DUODUO:
                    if (this.e != null) {
                        measuredHeight = this.e.getMeasuredHeight();
                        break;
                    } else {
                        return;
                    }
                case BAIDU:
                    if (this.c != null) {
                        measuredHeight = this.c.getMeasuredHeight();
                        break;
                    } else {
                        return;
                    }
                case TENCENT:
                    if (this.b != null) {
                        measuredHeight = this.b.getMeasuredHeight();
                        break;
                    } else {
                        return;
                    }
                case TAOBAO:
                    if (this.d != null) {
                        measuredHeight = this.d.getMeasuredHeight();
                        break;
                    } else {
                        return;
                    }
                default:
                    return;
            }
            setMeasuredDimension(View.MeasureSpec.getSize(i2), measuredHeight);
        }
    }
}
