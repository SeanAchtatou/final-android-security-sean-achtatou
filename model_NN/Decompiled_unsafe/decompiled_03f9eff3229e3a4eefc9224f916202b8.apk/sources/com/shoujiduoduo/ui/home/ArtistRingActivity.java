package com.shoujiduoduo.ui.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.widget.ImageView;
import android.widget.TextView;
import com.d.a.b.d;
import com.shoujiduoduo.b.c.n;
import com.shoujiduoduo.base.bean.a;
import com.shoujiduoduo.base.bean.f;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ui.utils.BaseFragmentActivity;
import com.shoujiduoduo.ui.utils.DDListFragment;
import com.shoujiduoduo.ui.utils.v;
import com.shoujiduoduo.util.ak;
import java.text.DecimalFormat;

public class ArtistRingActivity extends BaseFragmentActivity {

    /* renamed from: a  reason: collision with root package name */
    private TextView f1120a;
    private DDListFragment b;
    private a c;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_artist_ring);
        this.f1120a = (TextView) findViewById(R.id.header_title);
        findViewById(R.id.backButton).setOnClickListener(new a(this));
        Intent intent = getIntent();
        if (intent != null) {
            this.c = (a) RingDDApp.b().a(intent.getStringExtra("parakey"));
            if (this.c != null) {
                this.f1120a.setText(this.c.e);
                b();
                this.b.a(new n(f.a.list_ring_artist, this.c.f, false, ""));
                a();
                return;
            }
            com.shoujiduoduo.base.a.a.c("ArtistRingActivity", "wrong collect data prarm");
            return;
        }
        com.shoujiduoduo.base.a.a.c("ArtistRingActivity", "wrong intent == null");
    }

    private void a() {
        TextView textView = (TextView) findViewById(R.id.sale);
        d.a().a(this.c.f816a, (ImageView) findViewById(R.id.pic), v.a().f());
        ((TextView) findViewById(R.id.content)).setText(this.c.d);
        int i = this.c.c;
        StringBuilder sb = new StringBuilder();
        sb.append("彩铃销量:");
        if (i > 10000) {
            sb.append(new DecimalFormat("#.00").format((double) (((float) i) / 10000.0f)));
            sb.append("万");
        } else {
            sb.append(i);
        }
        textView.setText(sb.toString());
    }

    private void b() {
        this.b = new DDListFragment();
        Bundle bundle = new Bundle();
        bundle.putString("adapter_type", "ring_list_adapter");
        this.b.setArguments(bundle);
        FragmentTransaction beginTransaction = getSupportFragmentManager().beginTransaction();
        beginTransaction.add((int) R.id.list_layout, this.b);
        beginTransaction.commitAllowingStateLoss();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        PlayerService b2 = ak.a().b();
        if (b2 != null && b2.j()) {
            b2.k();
        }
        super.onDestroy();
    }
}
