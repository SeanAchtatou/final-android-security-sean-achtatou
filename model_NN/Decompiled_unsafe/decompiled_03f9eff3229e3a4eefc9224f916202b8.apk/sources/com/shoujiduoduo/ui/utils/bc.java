package com.shoujiduoduo.ui.utils;

import android.content.DialogInterface;
import com.shoujiduoduo.a.a.b;
import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.util.as;
import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.widget.a;
import com.shoujiduoduo.util.widget.f;

/* compiled from: RingListAdapter */
class bc extends a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RingData f1481a;
    final /* synthetic */ boolean b;
    final /* synthetic */ boolean c;
    final /* synthetic */ y d;

    bc(y yVar, RingData ringData, boolean z, boolean z2) {
        this.d = yVar;
        this.f1481a = ringData;
        this.b = z;
        this.c = z2;
    }

    public void a(c.b bVar) {
        super.a(bVar);
        this.d.f();
        f.a("已经设置为当前默认彩铃！");
        new a.C0034a(this.d.f).b("设置彩铃").a("已成功设置为您的当前彩铃.赶快试试吧！").a("确定", (DialogInterface.OnClickListener) null).a().show();
        as.c(this.d.f, "DEFAULT_CAILING_ID", this.f1481a.n);
        x.a().b(b.OBSERVER_RING_CHANGE, new bd(this));
        if (this.b) {
            as.b(this.d.f, "NeedUpdateCaiLingLib", 1);
            x.a().b(b.OBSERVER_CAILING, new be(this));
        }
    }

    public void b(c.b bVar) {
        super.b(bVar);
        this.d.f();
        bVar.b();
        if (this.c) {
            new a.C0034a(this.d.f).b("设置彩铃").a("设置未成功, 请打开“我的”->“彩铃”，删除几首不用的彩铃后再试试。彩铃有最大数量限制。").a("确定", (DialogInterface.OnClickListener) null).a().show();
        } else {
            new a.C0034a(this.d.f).b("设置彩铃").a("设置未成功, 原因:" + bVar.b()).a("确定", (DialogInterface.OnClickListener) null).a().show();
        }
    }
}
