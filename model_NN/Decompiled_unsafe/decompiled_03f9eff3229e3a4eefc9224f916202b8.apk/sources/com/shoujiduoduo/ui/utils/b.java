package com.shoujiduoduo.ui.utils;

import android.view.View;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.util.q;
import com.shoujiduoduo.util.widget.f;
import java.util.HashMap;

/* compiled from: AdHeaderViewHolder */
class b implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f1478a;

    b(a aVar) {
        this.f1478a = aVar;
    }

    public void onClick(View view) {
        f.a("开始下载\"" + this.f1478a.g.f748a + "\"");
        q.a(RingDDApp.c()).a(this.f1478a.g.f, this.f1478a.g.f748a, q.a.immediatelly, true);
        HashMap hashMap = new HashMap();
        hashMap.put("app", this.f1478a.g.f748a);
        com.umeng.analytics.b.a(RingDDApp.c(), "CLICK_SEARCH_AD_DOWN", hashMap);
    }
}
