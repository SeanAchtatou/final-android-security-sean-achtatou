package com.shoujiduoduo.ui.utils;

import android.os.Bundle;
import android.support.v4.media.TransportMediator;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.util.f;
import com.shoujiduoduo.util.g;
import com.umeng.analytics.b;

public class HtmlFragment extends LazyFragment {
    private boolean b;
    private WebView c;
    private String d;
    private boolean e;
    private String f = "http://musicalbum.shoujiduoduo.com/malbum/serv/billboard.php?create=1&ddsrc=ring_gzh&needstory=1";
    private WebViewClient g = new u(this);

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        a.a("HtmlFragment", "onCreateView");
        try {
            View inflate = layoutInflater.inflate((int) R.layout.html_frag_layout, viewGroup, false);
            this.c = (WebView) inflate.findViewById(R.id.webview_content);
            Bundle arguments = getArguments();
            if (arguments != null) {
                this.d = arguments.getString("url");
                this.d += "&isrc=" + f.p();
                a.a("HtmlFragment", "url:" + this.d);
                WebSettings settings = this.c.getSettings();
                settings.setJavaScriptEnabled(true);
                settings.setUseWideViewPort(true);
                settings.setLoadWithOverviewMode(true);
                settings.setBuiltInZoomControls(false);
                settings.setDomStorageEnabled(true);
                settings.setSupportZoom(false);
                this.c.requestFocus(TransportMediator.KEYCODE_MEDIA_RECORD);
                this.c.setWebViewClient(this.g);
            } else {
                a.e("HtmlFragment", "url is null");
            }
            this.b = true;
            a();
            return inflate;
        } catch (Exception e2) {
            e2.printStackTrace();
            b.b(RingDDApp.c(), "kuaixiu_wall_crash");
            return super.onCreateView(layoutInflater, viewGroup, bundle);
        }
    }

    public void onDestroyView() {
        super.onDestroyView();
        this.b = false;
        this.e = false;
    }

    /* access modifiers changed from: protected */
    public void a() {
        if (this.b && this.f1452a && !this.e && this.c != null) {
            this.c.loadUrl(g.b(this.d));
            this.e = true;
        }
    }
}
