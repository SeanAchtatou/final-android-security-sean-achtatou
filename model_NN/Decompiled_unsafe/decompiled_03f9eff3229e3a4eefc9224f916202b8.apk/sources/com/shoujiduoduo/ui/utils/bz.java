package com.shoujiduoduo.ui.utils;

import android.view.View;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.ui.cailing.ak;
import com.shoujiduoduo.util.f;

/* compiled from: RingListAdapter */
class bz implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ y f1498a;

    bz(y yVar) {
        this.f1498a = yVar;
    }

    public void onClick(View view) {
        a.a("RingListAdapter", "RingtoneDuoduo: click Cailing button!");
        RingData b = this.f1498a.b.a(this.f1498a.c);
        RingData unused = this.f1498a.l = b;
        switch (f.u()) {
            case f1671a:
                this.f1498a.b(b);
                return;
            case cu:
                this.f1498a.d(b);
                return;
            case ct:
                if (b.w == 1 || b.w == 2) {
                    this.f1498a.e(b);
                    return;
                } else {
                    ak.a(this.f1498a.f).a(view, b, this.f1498a.b.a(), this.f1498a.b.b());
                    return;
                }
            default:
                com.shoujiduoduo.util.widget.f.a("不支持的运营商类型!");
                return;
        }
    }
}
