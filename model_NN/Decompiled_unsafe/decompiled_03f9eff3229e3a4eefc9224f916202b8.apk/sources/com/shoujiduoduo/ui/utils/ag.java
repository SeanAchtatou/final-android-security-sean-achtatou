package com.shoujiduoduo.ui.utils;

import android.content.DialogInterface;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.widget.a;

/* compiled from: RingListAdapter */
class ag extends a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RingData f1461a;
    final /* synthetic */ String b;
    final /* synthetic */ y c;

    ag(y yVar, RingData ringData, String str) {
        this.c = yVar;
        this.f1461a = ringData;
        this.b = str;
    }

    public void a(c.b bVar) {
        super.a(bVar);
        if (bVar != null && (bVar instanceof c.af)) {
            c.af afVar = (c.af) bVar;
            com.shoujiduoduo.base.a.a.a("RingListAdapter", "user location, provinceid:" + afVar.f1592a + ", province name:" + afVar.d);
            this.c.a(this.f1461a, this.b, com.shoujiduoduo.util.e.a.a().c(afVar.f1592a));
        }
    }

    public void b(c.b bVar) {
        super.b(bVar);
        this.c.f();
        new a.C0034a(this.c.f).b("设置彩铃").a("获取当前手机号信息失败，请稍候再试试。").a("确定", (DialogInterface.OnClickListener) null).a().show();
    }
}
