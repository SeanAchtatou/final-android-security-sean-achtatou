package com.shoujiduoduo.ui.utils;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.text.Html;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;

/* compiled from: RingListAdapter */
class bl implements Html.ImageGetter {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ y f1486a;

    bl(y yVar) {
        this.f1486a = yVar;
    }

    public Drawable getDrawable(String str) {
        int i;
        try {
            i = Integer.parseInt(str);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            i = 0;
        }
        Resources resources = RingDDApp.c().getResources();
        Drawable drawable = resources.getDrawable(i);
        drawable.setBounds(0, 0, (int) resources.getDimension(R.dimen.cailing_icon_width), (int) resources.getDimension(R.dimen.cailing_icon_height));
        return drawable;
    }
}
