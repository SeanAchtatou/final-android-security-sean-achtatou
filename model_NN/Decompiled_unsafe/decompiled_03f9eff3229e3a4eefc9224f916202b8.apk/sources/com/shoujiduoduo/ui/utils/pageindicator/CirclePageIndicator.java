package com.shoujiduoduo.ui.utils.pageindicator;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.ViewConfigurationCompat;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import com.shoujiduoduo.ringtone.R;

public class CirclePageIndicator extends View implements PageIndicator {

    /* renamed from: a  reason: collision with root package name */
    private float f1514a;
    private final Paint b;
    private final Paint c;
    private final Paint d;
    private ViewPager e;
    private ViewPager.OnPageChangeListener f;
    private int g;
    private int h;
    private float i;
    private int j;
    private int k;
    private boolean l;
    private boolean m;
    private int n;
    private float o;
    private int p;
    private boolean q;

    public CirclePageIndicator(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, R.attr.vpiCirclePageIndicatorStyle);
    }

    public CirclePageIndicator(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.b = new Paint(1);
        this.c = new Paint(1);
        this.d = new Paint(1);
        this.o = -1.0f;
        this.p = -1;
        if (!isInEditMode()) {
            Resources resources = getResources();
            int color = resources.getColor(R.color.default_circle_indicator_page_color);
            int color2 = resources.getColor(R.color.default_circle_indicator_fill_color);
            int integer = resources.getInteger(R.integer.default_circle_indicator_orientation);
            int color3 = resources.getColor(R.color.default_circle_indicator_stroke_color);
            float dimension = resources.getDimension(R.dimen.default_circle_indicator_stroke_width);
            float dimension2 = resources.getDimension(R.dimen.default_circle_indicator_radius);
            boolean z = resources.getBoolean(R.bool.default_circle_indicator_centered);
            boolean z2 = resources.getBoolean(R.bool.default_circle_indicator_snap);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.CirclePageIndicator, i2, 0);
            this.l = obtainStyledAttributes.getBoolean(2, z);
            this.k = obtainStyledAttributes.getInt(0, integer);
            this.b.setStyle(Paint.Style.FILL);
            this.b.setColor(obtainStyledAttributes.getColor(5, color));
            this.c.setStyle(Paint.Style.STROKE);
            this.c.setColor(obtainStyledAttributes.getColor(8, color3));
            this.c.setStrokeWidth(obtainStyledAttributes.getDimension(3, dimension));
            this.d.setStyle(Paint.Style.FILL);
            this.d.setColor(obtainStyledAttributes.getColor(4, color2));
            this.f1514a = obtainStyledAttributes.getDimension(6, dimension2);
            this.m = obtainStyledAttributes.getBoolean(7, z2);
            Drawable drawable = obtainStyledAttributes.getDrawable(1);
            if (drawable != null) {
                setBackgroundDrawable(drawable);
            }
            obtainStyledAttributes.recycle();
            this.n = ViewConfigurationCompat.getScaledPagingTouchSlop(ViewConfiguration.get(context));
        }
    }

    public void setCentered(boolean z) {
        this.l = z;
        invalidate();
    }

    public void setPageColor(int i2) {
        this.b.setColor(i2);
        invalidate();
    }

    public int getPageColor() {
        return this.b.getColor();
    }

    public void setFillColor(int i2) {
        this.d.setColor(i2);
        invalidate();
    }

    public int getFillColor() {
        return this.d.getColor();
    }

    public void setOrientation(int i2) {
        switch (i2) {
            case 0:
            case 1:
                break;
            default:
                throw new IllegalArgumentException("Orientation must be either HORIZONTAL or VERTICAL.");
        }
        this.k = i2;
        requestLayout();
    }

    public int getOrientation() {
        return this.k;
    }

    public void setStrokeColor(int i2) {
        this.c.setColor(i2);
        invalidate();
    }

    public int getStrokeColor() {
        return this.c.getColor();
    }

    public void setStrokeWidth(float f2) {
        this.c.setStrokeWidth(f2);
        invalidate();
    }

    public float getStrokeWidth() {
        return this.c.getStrokeWidth();
    }

    public void setRadius(float f2) {
        this.f1514a = f2;
        invalidate();
    }

    public float getRadius() {
        return this.f1514a;
    }

    public void setSnap(boolean z) {
        this.m = z;
        invalidate();
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        int count;
        int height;
        int paddingTop;
        int paddingBottom;
        int paddingLeft;
        float f2;
        float f3;
        super.onDraw(canvas);
        if (this.e != null && (count = this.e.getAdapter().getCount()) != 0) {
            if (this.g >= count) {
                setCurrentItem(count - 1);
                return;
            }
            if (this.k == 0) {
                height = getWidth();
                paddingTop = getPaddingLeft();
                paddingBottom = getPaddingRight();
                paddingLeft = getPaddingTop();
            } else {
                height = getHeight();
                paddingTop = getPaddingTop();
                paddingBottom = getPaddingBottom();
                paddingLeft = getPaddingLeft();
            }
            float f4 = this.f1514a * 3.0f;
            float f5 = this.f1514a + ((float) paddingLeft);
            float f6 = ((float) paddingTop) + this.f1514a;
            if (this.l) {
                f6 += (((float) ((height - paddingTop) - paddingBottom)) / 2.0f) - ((((float) count) * f4) / 2.0f);
            }
            float f7 = this.f1514a;
            if (this.c.getStrokeWidth() > 0.0f) {
                f7 -= this.c.getStrokeWidth() / 2.0f;
            }
            for (int i2 = 0; i2 < count; i2++) {
                float f8 = (((float) i2) * f4) + f6;
                if (this.k == 0) {
                    f3 = f8;
                    f8 = f5;
                } else {
                    f3 = f5;
                }
                if (this.b.getAlpha() > 0) {
                    canvas.drawCircle(f3, f8, f7, this.b);
                }
                if (f7 != this.f1514a) {
                    canvas.drawCircle(f3, f8, this.f1514a, this.c);
                }
            }
            float f9 = ((float) (this.m ? this.h : this.g)) * f4;
            if (!this.m) {
                f9 += this.i * f4;
            }
            if (this.k == 0) {
                f2 = f6 + f9;
            } else {
                float f10 = f6 + f9;
                f2 = f5;
                f5 = f10;
            }
            canvas.drawCircle(f2, f5, this.f1514a, this.d);
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        int i2 = 0;
        if (super.onTouchEvent(motionEvent)) {
            return true;
        }
        if (this.e == null || this.e.getAdapter().getCount() == 0) {
            return false;
        }
        int action = motionEvent.getAction() & MotionEventCompat.ACTION_MASK;
        switch (action) {
            case 0:
                this.p = MotionEventCompat.getPointerId(motionEvent, 0);
                this.o = motionEvent.getX();
                return true;
            case 1:
            case 3:
                if (!this.q) {
                    int count = this.e.getAdapter().getCount();
                    int width = getWidth();
                    float f2 = ((float) width) / 2.0f;
                    float f3 = ((float) width) / 6.0f;
                    if (this.g <= 0 || motionEvent.getX() >= f2 - f3) {
                        if (this.g < count - 1 && motionEvent.getX() > f3 + f2) {
                            if (action == 3) {
                                return true;
                            }
                            this.e.setCurrentItem(this.g + 1);
                            return true;
                        }
                    } else if (action == 3) {
                        return true;
                    } else {
                        this.e.setCurrentItem(this.g - 1);
                        return true;
                    }
                }
                this.q = false;
                this.p = -1;
                if (!this.e.isFakeDragging()) {
                    return true;
                }
                this.e.endFakeDrag();
                return true;
            case 2:
                float x = MotionEventCompat.getX(motionEvent, MotionEventCompat.findPointerIndex(motionEvent, this.p));
                float f4 = x - this.o;
                if (!this.q && Math.abs(f4) > ((float) this.n)) {
                    this.q = true;
                }
                if (!this.q) {
                    return true;
                }
                this.o = x;
                if (!this.e.isFakeDragging() && !this.e.beginFakeDrag()) {
                    return true;
                }
                this.e.fakeDragBy(f4);
                return true;
            case 4:
            default:
                return true;
            case 5:
                int actionIndex = MotionEventCompat.getActionIndex(motionEvent);
                this.o = MotionEventCompat.getX(motionEvent, actionIndex);
                this.p = MotionEventCompat.getPointerId(motionEvent, actionIndex);
                return true;
            case 6:
                int actionIndex2 = MotionEventCompat.getActionIndex(motionEvent);
                if (MotionEventCompat.getPointerId(motionEvent, actionIndex2) == this.p) {
                    if (actionIndex2 == 0) {
                        i2 = 1;
                    }
                    this.p = MotionEventCompat.getPointerId(motionEvent, i2);
                }
                this.o = MotionEventCompat.getX(motionEvent, MotionEventCompat.findPointerIndex(motionEvent, this.p));
                return true;
        }
    }

    public void setViewPager(ViewPager viewPager) {
        if (this.e != viewPager) {
            if (this.e != null) {
                this.e.setOnPageChangeListener(null);
            }
            if (viewPager.getAdapter() == null) {
                throw new IllegalStateException("ViewPager does not have adapter instance.");
            }
            this.e = viewPager;
            this.e.setOnPageChangeListener(this);
            invalidate();
        }
    }

    public void setCurrentItem(int i2) {
        if (this.e == null) {
            throw new IllegalStateException("ViewPager has not been bound.");
        }
        this.e.setCurrentItem(i2);
        this.g = i2;
        invalidate();
    }

    public void onPageScrollStateChanged(int i2) {
        this.j = i2;
        if (this.f != null) {
            this.f.onPageScrollStateChanged(i2);
        }
    }

    public void onPageScrolled(int i2, float f2, int i3) {
        this.g = i2;
        this.i = f2;
        invalidate();
        if (this.f != null) {
            this.f.onPageScrolled(i2, f2, i3);
        }
    }

    public void onPageSelected(int i2) {
        if (this.m || this.j == 0) {
            this.g = i2;
            this.h = i2;
            invalidate();
        }
        if (this.f != null) {
            this.f.onPageSelected(i2);
        }
    }

    public void setOnPageChangeListener(ViewPager.OnPageChangeListener onPageChangeListener) {
        this.f = onPageChangeListener;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        if (this.k == 0) {
            setMeasuredDimension(a(i2), b(i3));
        } else {
            setMeasuredDimension(b(i2), a(i3));
        }
    }

    private int a(int i2) {
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        if (mode == 1073741824 || this.e == null) {
            return size;
        }
        int count = this.e.getAdapter().getCount();
        int paddingLeft = (int) ((((float) (count - 1)) * this.f1514a) + ((float) (getPaddingLeft() + getPaddingRight())) + (((float) (count * 2)) * this.f1514a) + 1.0f);
        if (mode == Integer.MIN_VALUE) {
            return Math.min(paddingLeft, size);
        }
        return paddingLeft;
    }

    private int b(int i2) {
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        if (mode == 1073741824) {
            return size;
        }
        int paddingTop = (int) ((2.0f * this.f1514a) + ((float) getPaddingTop()) + ((float) getPaddingBottom()) + 1.0f);
        return mode == Integer.MIN_VALUE ? Math.min(paddingTop, size) : paddingTop;
    }

    public void onRestoreInstanceState(Parcelable parcelable) {
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        this.g = savedState.f1515a;
        this.h = savedState.f1515a;
        requestLayout();
    }

    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.f1515a = this.g;
        return savedState;
    }

    static class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = new a();

        /* renamed from: a  reason: collision with root package name */
        int f1515a;

        public SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        private SavedState(Parcel parcel) {
            super(parcel);
            this.f1515a = parcel.readInt();
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.f1515a);
        }
    }
}
