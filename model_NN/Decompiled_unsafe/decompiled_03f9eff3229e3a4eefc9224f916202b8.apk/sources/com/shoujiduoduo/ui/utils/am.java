package com.shoujiduoduo.ui.utils;

import android.content.DialogInterface;
import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.f;
import com.shoujiduoduo.util.widget.a;

/* compiled from: RingListAdapter */
class am extends a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ah f1467a;

    am(ah ahVar) {
        this.f1467a = ahVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.f$b, boolean):void
     arg types: [com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.f$b, int]
     candidates:
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.f$b, java.lang.String, boolean):void
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, boolean, java.lang.String, java.lang.String, com.shoujiduoduo.base.bean.RingData):void
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.f$b, boolean):void */
    public void a(c.b bVar) {
        super.a(bVar);
        this.f1467a.d.f();
        com.shoujiduoduo.base.a.a.a("RingListAdapter", "成功开通彩铃基础功能, 提示开通会员");
        this.f1467a.d.a(this.f1467a.d.b.a(this.f1467a.d.c), this.f1467a.b, f.b.cu, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.f$b, boolean):void
     arg types: [com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.f$b, int]
     candidates:
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.f$b, java.lang.String, boolean):void
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, boolean, java.lang.String, java.lang.String, com.shoujiduoduo.base.bean.RingData):void
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.f$b, boolean):void */
    public void b(c.b bVar) {
        super.b(bVar);
        this.f1467a.d.f();
        if (bVar.a().equals("000001") || bVar.a().equals("301000")) {
            this.f1467a.d.a(this.f1467a.d.b.a(this.f1467a.d.c), this.f1467a.b, f.b.cu, false);
        } else {
            new a.C0034a(this.f1467a.d.f).b("设置彩铃").a("为您免费开通彩铃功能失败， 原因：" + bVar.b()).a("确定", (DialogInterface.OnClickListener) null).a().show();
        }
    }
}
