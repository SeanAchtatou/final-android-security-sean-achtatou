package com.shoujiduoduo.ui.utils;

import android.content.Intent;
import android.view.View;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ringtone.activity.RingToneDuoduoActivity;
import com.shoujiduoduo.ui.home.MusicAlbumActivity;

/* compiled from: RingListAdapter */
class br implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ y f1490a;

    br(y yVar) {
        this.f1490a = yVar;
    }

    public void onClick(View view) {
        a.a("RingListAdapter", "RingtoneDuoduo: click weixiu button!");
        RingData b = this.f1490a.b.a(this.f1490a.c);
        Intent intent = new Intent(RingDDApp.c(), MusicAlbumActivity.class);
        intent.putExtra("musicid", b.g);
        intent.putExtra("title", "快秀");
        intent.putExtra("type", MusicAlbumActivity.a.ring_story);
        RingToneDuoduoActivity.a().startActivity(intent);
    }
}
