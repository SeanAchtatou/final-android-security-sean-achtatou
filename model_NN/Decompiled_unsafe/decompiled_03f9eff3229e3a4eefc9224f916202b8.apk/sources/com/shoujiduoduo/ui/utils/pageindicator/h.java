package com.shoujiduoduo.ui.utils.pageindicator;

import android.os.Parcel;
import android.os.Parcelable;
import com.shoujiduoduo.ui.utils.pageindicator.TitlePageIndicator;

/* compiled from: TitlePageIndicator */
final class h implements Parcelable.Creator<TitlePageIndicator.SavedState> {
    h() {
    }

    /* renamed from: a */
    public TitlePageIndicator.SavedState createFromParcel(Parcel parcel) {
        return new TitlePageIndicator.SavedState(parcel);
    }

    /* renamed from: a */
    public TitlePageIndicator.SavedState[] newArray(int i) {
        return new TitlePageIndicator.SavedState[i];
    }
}
