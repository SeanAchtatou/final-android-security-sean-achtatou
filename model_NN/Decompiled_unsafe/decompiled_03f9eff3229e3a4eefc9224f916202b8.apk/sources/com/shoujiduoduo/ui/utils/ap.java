package com.shoujiduoduo.ui.utils;

import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.d.b;
import com.shoujiduoduo.util.f;
import com.shoujiduoduo.util.widget.f;

/* compiled from: RingListAdapter */
class ap extends a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f1470a;
    final /* synthetic */ RingData b;
    final /* synthetic */ y c;

    ap(y yVar, String str, RingData ringData) {
        this.c = yVar;
        this.f1470a = str;
        this.b = ringData;
    }

    public void a(c.b bVar) {
        super.a(bVar);
        this.c.f();
        com.shoujiduoduo.base.a.a.a("RingListAdapter", "基础业务开通状态");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.f$b, java.lang.String, boolean):void
     arg types: [com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.f$b, java.lang.String, int]
     candidates:
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.f$b, boolean):void
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, boolean, java.lang.String, java.lang.String, com.shoujiduoduo.base.bean.RingData):void
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.f$b, java.lang.String, boolean):void */
    public void b(c.b bVar) {
        super.b(bVar);
        com.shoujiduoduo.base.a.a.a("RingListAdapter", "彩铃基础业务尚未开通");
        this.c.f();
        if (b.a().a(this.f1470a).equals(b.d.wait_open)) {
            f.a("正在为您开通业务，请耐心等待一会儿.");
        } else {
            this.c.a(this.b, f.b.ct, this.f1470a, false);
        }
    }
}
