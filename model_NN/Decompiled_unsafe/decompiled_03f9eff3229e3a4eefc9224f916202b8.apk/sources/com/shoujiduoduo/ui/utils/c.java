package com.shoujiduoduo.ui.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.d.a.b.d;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.util.f;
import java.text.DecimalFormat;

/* compiled from: ArtistListAdapter */
public class c extends g {

    /* renamed from: a  reason: collision with root package name */
    private com.shoujiduoduo.b.c.a f1499a;
    private Context b;

    public c(Context context) {
        this.b = context;
    }

    public int getCount() {
        if (this.f1499a != null) {
            return this.f1499a.c();
        }
        com.shoujiduoduo.base.a.a.a("CollectListAdapter", "count:0");
        return 0;
    }

    public Object getItem(int i) {
        if (this.f1499a != null) {
            return this.f1499a.a(i);
        }
        return null;
    }

    public long getItemId(int i) {
        return (long) i;
    }

    /* compiled from: ArtistListAdapter */
    private static class a {

        /* renamed from: a  reason: collision with root package name */
        ImageView f1500a;
        TextView b;
        TextView c;
        TextView d;
        TextView e;

        private a() {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int i, View view, ViewGroup viewGroup) {
        a aVar;
        if (this.f1499a == null) {
            com.shoujiduoduo.base.a.a.a("CollectListAdapter", "return null");
            return null;
        }
        if (view == null) {
            view = LayoutInflater.from(this.b).inflate((int) R.layout.listitem_artist, viewGroup, false);
            a aVar2 = new a();
            aVar2.f1500a = (ImageView) view.findViewById(R.id.pic);
            aVar2.b = (TextView) view.findViewById(R.id.title);
            aVar2.c = (TextView) view.findViewById(R.id.content);
            aVar2.d = (TextView) view.findViewById(R.id.sale);
            aVar2.e = (TextView) view.findViewById(R.id.sn);
            if (f.n()) {
                aVar2.c.setLines(1);
                aVar2.e.setTextSize(2, 12.0f);
            }
            view.setTag(aVar2);
            aVar = aVar2;
        } else {
            aVar = (a) view.getTag();
        }
        com.shoujiduoduo.base.bean.a b2 = this.f1499a.a(i);
        d.a().a(b2.f816a, aVar.f1500a, v.a().f());
        aVar.b.setText(b2.e);
        aVar.c.setText(b2.d);
        aVar.e.setText("" + (i + 1));
        int i2 = b2.c;
        StringBuilder sb = new StringBuilder();
        sb.append("彩铃销售:");
        if (i2 > 10000) {
            sb.append(new DecimalFormat("#.00").format((double) (((float) i2) / 10000.0f)));
            sb.append("万");
        } else {
            sb.append(i2);
        }
        aVar.d.setText(sb.toString());
        return view;
    }

    public void a(boolean z) {
    }

    public void a(com.shoujiduoduo.base.bean.c cVar) {
        this.f1499a = (com.shoujiduoduo.b.c.a) cVar;
    }

    public void a() {
    }

    public void b() {
    }
}
