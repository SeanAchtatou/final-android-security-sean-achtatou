package com.shoujiduoduo.ui.utils;

import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.a.b.b;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.base.bean.ab;
import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.f;

/* compiled from: RingListAdapter */
class ae extends a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ab f1460a;
    final /* synthetic */ RingData b;
    final /* synthetic */ y c;

    ae(y yVar, ab abVar, RingData ringData) {
        this.c = yVar;
        this.f1460a = abVar;
        this.b = ringData;
    }

    public void a(c.b bVar) {
        super.a(bVar);
        if (bVar == null || !(bVar instanceof c.h)) {
            this.c.f();
            this.c.b(this.b, "", f.b.ct);
            return;
        }
        this.c.f();
        String d = ((c.h) bVar).d();
        if (!this.f1460a.i()) {
            this.f1460a.b(d);
            this.f1460a.a("phone_" + d);
        }
        this.f1460a.d(d);
        this.f1460a.c(1);
        b.g().a(this.f1460a);
        x.a().a(com.shoujiduoduo.a.a.b.OBSERVER_USER_CENTER, new af(this));
        this.c.a(this.b, this.f1460a.k(), f.b.ct);
    }

    public void b(c.b bVar) {
        super.b(bVar);
        this.c.f();
        this.c.b(this.b, "", f.b.ct);
    }
}
