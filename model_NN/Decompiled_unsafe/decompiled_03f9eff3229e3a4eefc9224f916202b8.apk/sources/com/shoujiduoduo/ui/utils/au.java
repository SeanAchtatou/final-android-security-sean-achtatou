package com.shoujiduoduo.ui.utils;

import com.shoujiduoduo.a.c.n;
import com.shoujiduoduo.base.a.a;

/* compiled from: RingListAdapter */
class au implements n {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ y f1472a;

    au(y yVar) {
        this.f1472a = yVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, boolean):boolean
     arg types: [com.shoujiduoduo.ui.utils.y, int]
     candidates:
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, int):int
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.f$b):android.view.View
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData):com.shoujiduoduo.base.bean.RingData
      com.shoujiduoduo.ui.utils.y.a(android.view.View, int):void
      com.shoujiduoduo.ui.utils.y.a(android.widget.TextView, java.lang.String):void
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.base.bean.RingData, java.lang.String):void
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.base.bean.RingData, boolean):void
      com.shoujiduoduo.ui.utils.y.a(java.lang.String, com.shoujiduoduo.base.bean.RingData):void
      com.shoujiduoduo.ui.utils.y.a(boolean, com.shoujiduoduo.util.f$b):void
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.ui.utils.y.b(com.shoujiduoduo.ui.utils.y, boolean):boolean
     arg types: [com.shoujiduoduo.ui.utils.y, int]
     candidates:
      com.shoujiduoduo.ui.utils.y.b(com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.f$b):java.util.ArrayList<java.util.Map<java.lang.String, java.lang.Object>>
      com.shoujiduoduo.ui.utils.y.b(com.shoujiduoduo.base.bean.RingData, java.lang.String):void
      com.shoujiduoduo.ui.utils.y.b(com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData):void
      com.shoujiduoduo.ui.utils.y.b(com.shoujiduoduo.ui.utils.y, boolean):boolean */
    public void a(String str, int i) {
        if (this.f1472a.b != null && this.f1472a.b.a().equals(str)) {
            a.a("RingListAdapter", "onSetPlay, listid:" + str);
            if (str.equals(this.f1472a.b.a())) {
                boolean unused = this.f1472a.e = true;
                int unused2 = this.f1472a.c = i;
            } else {
                boolean unused3 = this.f1472a.e = false;
            }
            boolean unused4 = this.f1472a.j = true;
            this.f1472a.notifyDataSetChanged();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, boolean):boolean
     arg types: [com.shoujiduoduo.ui.utils.y, int]
     candidates:
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, int):int
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.f$b):android.view.View
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData):com.shoujiduoduo.base.bean.RingData
      com.shoujiduoduo.ui.utils.y.a(android.view.View, int):void
      com.shoujiduoduo.ui.utils.y.a(android.widget.TextView, java.lang.String):void
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.base.bean.RingData, java.lang.String):void
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.base.bean.RingData, boolean):void
      com.shoujiduoduo.ui.utils.y.a(java.lang.String, com.shoujiduoduo.base.bean.RingData):void
      com.shoujiduoduo.ui.utils.y.a(boolean, com.shoujiduoduo.util.f$b):void
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.ui.utils.y.b(com.shoujiduoduo.ui.utils.y, boolean):boolean
     arg types: [com.shoujiduoduo.ui.utils.y, int]
     candidates:
      com.shoujiduoduo.ui.utils.y.b(com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.f$b):java.util.ArrayList<java.util.Map<java.lang.String, java.lang.Object>>
      com.shoujiduoduo.ui.utils.y.b(com.shoujiduoduo.base.bean.RingData, java.lang.String):void
      com.shoujiduoduo.ui.utils.y.b(com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData):void
      com.shoujiduoduo.ui.utils.y.b(com.shoujiduoduo.ui.utils.y, boolean):boolean */
    public void b(String str, int i) {
        if (this.f1472a.b != null && this.f1472a.b.a().equals(str)) {
            a.a("RingListAdapter", "onCanclePlay, listId:" + str);
            boolean unused = this.f1472a.e = false;
            int unused2 = this.f1472a.c = i;
            boolean unused3 = this.f1472a.j = true;
            this.f1472a.notifyDataSetChanged();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.ui.utils.y.b(com.shoujiduoduo.ui.utils.y, boolean):boolean
     arg types: [com.shoujiduoduo.ui.utils.y, int]
     candidates:
      com.shoujiduoduo.ui.utils.y.b(com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.f$b):java.util.ArrayList<java.util.Map<java.lang.String, java.lang.Object>>
      com.shoujiduoduo.ui.utils.y.b(com.shoujiduoduo.base.bean.RingData, java.lang.String):void
      com.shoujiduoduo.ui.utils.y.b(com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData):void
      com.shoujiduoduo.ui.utils.y.b(com.shoujiduoduo.ui.utils.y, boolean):boolean */
    public void a(String str, int i, int i2) {
        if (this.f1472a.b != null && this.f1472a.b.a().equals(str)) {
            boolean unused = this.f1472a.j = true;
            this.f1472a.notifyDataSetChanged();
        }
    }
}
