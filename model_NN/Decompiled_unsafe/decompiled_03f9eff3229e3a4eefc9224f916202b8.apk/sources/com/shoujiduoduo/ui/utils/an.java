package com.shoujiduoduo.ui.utils;

import android.content.DialogInterface;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.d.b;
import com.shoujiduoduo.util.f;
import com.shoujiduoduo.util.widget.a;

/* compiled from: RingListAdapter */
class an extends a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RingData f1468a;
    final /* synthetic */ String b;
    final /* synthetic */ y c;

    an(y yVar, RingData ringData, String str) {
        this.c = yVar;
        this.f1468a = ringData;
        this.b = str;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, boolean, com.shoujiduoduo.util.f$b):void
     arg types: [com.shoujiduoduo.ui.utils.y, int, com.shoujiduoduo.util.f$b]
     candidates:
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.f$b):android.view.View
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.f$b):void
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.base.bean.RingData, java.lang.String, boolean):void
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.base.bean.RingData, boolean, boolean):void
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, java.lang.String):void
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, boolean):void
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, java.lang.String, com.shoujiduoduo.base.bean.RingData):void
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, boolean, com.shoujiduoduo.util.f$b):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.f$b, boolean):void
     arg types: [com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.f$b, int]
     candidates:
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.f$b, java.lang.String, boolean):void
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, boolean, java.lang.String, java.lang.String, com.shoujiduoduo.base.bean.RingData):void
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.f$b, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.f$b, java.lang.String, boolean):void
     arg types: [com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.f$b, java.lang.String, int]
     candidates:
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.f$b, boolean):void
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, boolean, java.lang.String, java.lang.String, com.shoujiduoduo.base.bean.RingData):void
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.f$b, java.lang.String, boolean):void */
    public void a(c.b bVar) {
        super.a(bVar);
        if (bVar == null || !(bVar instanceof c.d)) {
            this.c.f();
            new a.C0034a(this.c.f).b("设置彩铃").a(bVar != null ? bVar.b() : "设置失败").a("确定", (DialogInterface.OnClickListener) null).a().show();
            com.shoujiduoduo.base.a.a.c("RingListAdapter", "checkCailingAndVip failed");
            return;
        }
        c.d dVar = (c.d) bVar;
        if (dVar.d() && (dVar.e() || dVar.f())) {
            this.c.f();
            this.c.a(true, f.b.ct);
            new a.C0034a(this.c.f).b("设置彩铃(免费)").a(this.c.a(this.f1468a, f.b.ct)).a("确定", new ao(this)).b("取消", (DialogInterface.OnClickListener) null).a().show();
        } else if (dVar.d() && !dVar.e() && !dVar.f()) {
            this.c.f();
            this.c.a(this.f1468a, this.b, f.b.ct, false);
        } else if (!dVar.d() && (dVar.e() || dVar.f())) {
            this.c.a(true, f.b.ct);
            this.c.f();
            if (b.a().a(this.b).equals(b.d.wait_open)) {
                com.shoujiduoduo.util.widget.f.a("正在为您开通彩铃业务，请耐心等待一会儿...");
            } else {
                this.c.a(this.f1468a, f.b.ct, this.b, true);
            }
        } else if (!dVar.d() && !dVar.e() && !dVar.f()) {
            this.c.f();
            if (b.a().a(this.b).equals(b.d.wait_open)) {
                com.shoujiduoduo.util.widget.f.a("正在为您开通彩铃业务，请耐心等待一会儿...");
            } else {
                this.c.a(this.f1468a, f.b.ct, this.b, false);
            }
        }
    }

    public void b(c.b bVar) {
        super.b(bVar);
        this.c.f();
        new a.C0034a(this.c.f).b("设置彩铃").a(bVar.b()).a("确定", (DialogInterface.OnClickListener) null).a().show();
    }
}
