package com.shoujiduoduo.ui.utils;

import android.content.DialogInterface;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.ui.cailing.ck;
import com.shoujiduoduo.util.am;
import com.shoujiduoduo.util.d.b;
import com.shoujiduoduo.util.f;
import com.shoujiduoduo.util.widget.a;

/* compiled from: RingListAdapter */
class ax implements ck.a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ boolean f1475a;
    final /* synthetic */ RingData b;
    final /* synthetic */ f.b c;
    final /* synthetic */ String d;
    final /* synthetic */ y e;

    ax(y yVar, boolean z, RingData ringData, f.b bVar, String str) {
        this.e = yVar;
        this.f1475a = z;
        this.b = ringData;
        this.c = bVar;
        this.d = str;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.f$b, boolean):void
     arg types: [com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.f$b, int]
     candidates:
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.f$b, java.lang.String, boolean):void
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, boolean, java.lang.String, java.lang.String, com.shoujiduoduo.base.bean.RingData):void
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.f$b, boolean):void */
    public void a(ck.a.C0025a aVar) {
        if (!aVar.equals(b.d.open)) {
            return;
        }
        if (this.f1475a) {
            new a.C0034a(this.e.f).a("多多会员业务已成功受理，正在为您开通。 是否设置 《" + this.b.e + "》 为您的当前彩铃？").a("确定", new ay(this)).b("取消", (DialogInterface.OnClickListener) null).a().show();
        } else if (!this.c.equals(f.b.f1671a)) {
            this.e.a(this.e.b.a(this.e.c), this.d, this.c, false);
        } else if (am.a().b("cm_sunshine_sdk_enable")) {
            this.e.g();
        }
    }
}
