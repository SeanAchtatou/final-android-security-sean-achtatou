package com.shoujiduoduo.ui.utils.pageindicator;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.ViewConfigurationCompat;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.FloatMath;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import com.shoujiduoduo.ringtone.R;

public class LinePageIndicator extends View implements PageIndicator {

    /* renamed from: a  reason: collision with root package name */
    private final Paint f1517a;
    private final Paint b;
    private ViewPager c;
    private ViewPager.OnPageChangeListener d;
    private int e;
    private boolean f;
    private float g;
    private float h;
    private int i;
    private float j;
    private int k;
    private boolean l;

    public LinePageIndicator(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, R.attr.vpiLinePageIndicatorStyle);
    }

    public LinePageIndicator(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.f1517a = new Paint(1);
        this.b = new Paint(1);
        this.j = -1.0f;
        this.k = -1;
        if (!isInEditMode()) {
            Resources resources = getResources();
            int color = resources.getColor(R.color.default_line_indicator_selected_color);
            int color2 = resources.getColor(R.color.default_line_indicator_unselected_color);
            float dimension = resources.getDimension(R.dimen.default_line_indicator_line_width);
            float dimension2 = resources.getDimension(R.dimen.default_line_indicator_gap_width);
            float dimension3 = resources.getDimension(R.dimen.default_line_indicator_stroke_width);
            boolean z = resources.getBoolean(R.bool.default_line_indicator_centered);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.LinePageIndicator, i2, 0);
            this.f = obtainStyledAttributes.getBoolean(1, z);
            this.g = obtainStyledAttributes.getDimension(5, dimension);
            this.h = obtainStyledAttributes.getDimension(6, dimension2);
            setStrokeWidth(obtainStyledAttributes.getDimension(3, dimension3));
            this.f1517a.setColor(obtainStyledAttributes.getColor(4, color2));
            this.b.setColor(obtainStyledAttributes.getColor(2, color));
            Drawable drawable = obtainStyledAttributes.getDrawable(0);
            if (drawable != null) {
                setBackgroundDrawable(drawable);
            }
            obtainStyledAttributes.recycle();
            this.i = ViewConfigurationCompat.getScaledPagingTouchSlop(ViewConfiguration.get(context));
        }
    }

    public void setCentered(boolean z) {
        this.f = z;
        invalidate();
    }

    public void setUnselectedColor(int i2) {
        this.f1517a.setColor(i2);
        invalidate();
    }

    public int getUnselectedColor() {
        return this.f1517a.getColor();
    }

    public void setSelectedColor(int i2) {
        this.b.setColor(i2);
        invalidate();
    }

    public int getSelectedColor() {
        return this.b.getColor();
    }

    public void setLineWidth(float f2) {
        this.g = f2;
        invalidate();
    }

    public float getLineWidth() {
        return this.g;
    }

    public void setStrokeWidth(float f2) {
        this.b.setStrokeWidth(f2);
        this.f1517a.setStrokeWidth(f2);
        invalidate();
    }

    public float getStrokeWidth() {
        return this.b.getStrokeWidth();
    }

    public void setGapWidth(float f2) {
        this.h = f2;
        invalidate();
    }

    public float getGapWidth() {
        return this.h;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        int count;
        float f2;
        super.onDraw(canvas);
        if (this.c != null && (count = this.c.getAdapter().getCount()) != 0) {
            if (this.e >= count) {
                setCurrentItem(count - 1);
                return;
            }
            float f3 = this.g + this.h;
            float f4 = (((float) count) * f3) - this.h;
            float paddingTop = (float) getPaddingTop();
            float paddingLeft = (float) getPaddingLeft();
            float paddingRight = (float) getPaddingRight();
            float height = paddingTop + (((((float) getHeight()) - paddingTop) - ((float) getPaddingBottom())) / 2.0f);
            if (this.f) {
                f2 = paddingLeft + ((((((float) getWidth()) - paddingLeft) - paddingRight) / 2.0f) - (f4 / 2.0f));
            } else {
                f2 = paddingLeft;
            }
            int i2 = 0;
            while (i2 < count) {
                float f5 = f2 + (((float) i2) * f3);
                canvas.drawLine(f5, height, f5 + this.g, height, i2 == this.e ? this.b : this.f1517a);
                i2++;
            }
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        int i2 = 0;
        if (super.onTouchEvent(motionEvent)) {
            return true;
        }
        if (this.c == null || this.c.getAdapter().getCount() == 0) {
            return false;
        }
        int action = motionEvent.getAction() & MotionEventCompat.ACTION_MASK;
        switch (action) {
            case 0:
                this.k = MotionEventCompat.getPointerId(motionEvent, 0);
                this.j = motionEvent.getX();
                return true;
            case 1:
            case 3:
                if (!this.l) {
                    int count = this.c.getAdapter().getCount();
                    int width = getWidth();
                    float f2 = ((float) width) / 2.0f;
                    float f3 = ((float) width) / 6.0f;
                    if (this.e <= 0 || motionEvent.getX() >= f2 - f3) {
                        if (this.e < count - 1 && motionEvent.getX() > f3 + f2) {
                            if (action == 3) {
                                return true;
                            }
                            this.c.setCurrentItem(this.e + 1);
                            return true;
                        }
                    } else if (action == 3) {
                        return true;
                    } else {
                        this.c.setCurrentItem(this.e - 1);
                        return true;
                    }
                }
                this.l = false;
                this.k = -1;
                if (!this.c.isFakeDragging()) {
                    return true;
                }
                this.c.endFakeDrag();
                return true;
            case 2:
                float x = MotionEventCompat.getX(motionEvent, MotionEventCompat.findPointerIndex(motionEvent, this.k));
                float f4 = x - this.j;
                if (!this.l && Math.abs(f4) > ((float) this.i)) {
                    this.l = true;
                }
                if (!this.l) {
                    return true;
                }
                this.j = x;
                if (!this.c.isFakeDragging() && !this.c.beginFakeDrag()) {
                    return true;
                }
                this.c.fakeDragBy(f4);
                return true;
            case 4:
            default:
                return true;
            case 5:
                int actionIndex = MotionEventCompat.getActionIndex(motionEvent);
                this.j = MotionEventCompat.getX(motionEvent, actionIndex);
                this.k = MotionEventCompat.getPointerId(motionEvent, actionIndex);
                return true;
            case 6:
                int actionIndex2 = MotionEventCompat.getActionIndex(motionEvent);
                if (MotionEventCompat.getPointerId(motionEvent, actionIndex2) == this.k) {
                    if (actionIndex2 == 0) {
                        i2 = 1;
                    }
                    this.k = MotionEventCompat.getPointerId(motionEvent, i2);
                }
                this.j = MotionEventCompat.getX(motionEvent, MotionEventCompat.findPointerIndex(motionEvent, this.k));
                return true;
        }
    }

    public void setViewPager(ViewPager viewPager) {
        if (this.c != viewPager) {
            if (this.c != null) {
                this.c.setOnPageChangeListener(null);
            }
            if (viewPager.getAdapter() == null) {
                throw new IllegalStateException("ViewPager does not have adapter instance.");
            }
            this.c = viewPager;
            this.c.setOnPageChangeListener(this);
            invalidate();
        }
    }

    public void setCurrentItem(int i2) {
        if (this.c == null) {
            throw new IllegalStateException("ViewPager has not been bound.");
        }
        this.c.setCurrentItem(i2);
        this.e = i2;
        invalidate();
    }

    public void onPageScrollStateChanged(int i2) {
        if (this.d != null) {
            this.d.onPageScrollStateChanged(i2);
        }
    }

    public void onPageScrolled(int i2, float f2, int i3) {
        if (this.d != null) {
            this.d.onPageScrolled(i2, f2, i3);
        }
    }

    public void onPageSelected(int i2) {
        this.e = i2;
        invalidate();
        if (this.d != null) {
            this.d.onPageSelected(i2);
        }
    }

    public void setOnPageChangeListener(ViewPager.OnPageChangeListener onPageChangeListener) {
        this.d = onPageChangeListener;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        setMeasuredDimension(a(i2), b(i3));
    }

    private int a(int i2) {
        float f2;
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        if (mode == 1073741824 || this.c == null) {
            f2 = (float) size;
        } else {
            int count = this.c.getAdapter().getCount();
            f2 = (((float) (count - 1)) * this.h) + ((float) (getPaddingLeft() + getPaddingRight())) + (((float) count) * this.g);
            if (mode == Integer.MIN_VALUE) {
                f2 = Math.min(f2, (float) size);
            }
        }
        return (int) FloatMath.ceil(f2);
    }

    private int b(int i2) {
        float strokeWidth;
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        if (mode == 1073741824) {
            strokeWidth = (float) size;
        } else {
            strokeWidth = this.b.getStrokeWidth() + ((float) getPaddingTop()) + ((float) getPaddingBottom());
            if (mode == Integer.MIN_VALUE) {
                strokeWidth = Math.min(strokeWidth, (float) size);
            }
        }
        return (int) FloatMath.ceil(strokeWidth);
    }

    public void onRestoreInstanceState(Parcelable parcelable) {
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        this.e = savedState.f1518a;
        requestLayout();
    }

    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.f1518a = this.e;
        return savedState;
    }

    static class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = new e();

        /* renamed from: a  reason: collision with root package name */
        int f1518a;

        public SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        private SavedState(Parcel parcel) {
            super(parcel);
            this.f1518a = parcel.readInt();
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.f1518a);
        }
    }
}
