package com.shoujiduoduo.ui.utils.pageindicator;

import android.view.View;
import com.shoujiduoduo.ui.utils.pageindicator.TabPageIndicator;

/* compiled from: TabPageIndicator */
class f implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TabPageIndicator f1530a;

    f(TabPageIndicator tabPageIndicator) {
        this.f1530a = tabPageIndicator;
    }

    public void onClick(View view) {
        int currentItem = this.f1530a.e.getCurrentItem();
        int a2 = ((TabPageIndicator.b) view).a();
        this.f1530a.e.setCurrentItem(a2);
        if (currentItem == a2 && this.f1530a.i != null) {
            this.f1530a.i.a(a2);
        }
    }
}
