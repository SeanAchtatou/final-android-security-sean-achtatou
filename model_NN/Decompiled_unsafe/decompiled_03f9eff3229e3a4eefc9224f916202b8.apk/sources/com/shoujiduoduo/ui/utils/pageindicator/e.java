package com.shoujiduoduo.ui.utils.pageindicator;

import android.os.Parcel;
import android.os.Parcelable;
import com.shoujiduoduo.ui.utils.pageindicator.LinePageIndicator;

/* compiled from: LinePageIndicator */
final class e implements Parcelable.Creator<LinePageIndicator.SavedState> {
    e() {
    }

    /* renamed from: a */
    public LinePageIndicator.SavedState createFromParcel(Parcel parcel) {
        return new LinePageIndicator.SavedState(parcel);
    }

    /* renamed from: a */
    public LinePageIndicator.SavedState[] newArray(int i) {
        return new LinePageIndicator.SavedState[i];
    }
}
