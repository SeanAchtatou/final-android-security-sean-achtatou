package com.shoujiduoduo.ui.utils;

import android.content.DialogInterface;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.widget.a;

/* compiled from: RingListAdapter */
class bk extends a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f1485a;
    final /* synthetic */ RingData b;
    final /* synthetic */ y c;

    bk(y yVar, String str, RingData ringData) {
        this.c = yVar;
        this.f1485a = str;
        this.b = ringData;
    }

    public void a(c.b bVar) {
        String str;
        boolean z = false;
        super.a(bVar);
        if (bVar != null && (bVar instanceof c.q)) {
            c.q qVar = (c.q) bVar;
            if (qVar.d != null) {
                int i = 0;
                while (true) {
                    if (i >= qVar.d.length) {
                        break;
                    } else if (qVar.d[i].c.equals("0")) {
                        z = true;
                        str = qVar.d[i].f1595a;
                        break;
                    } else {
                        i++;
                    }
                }
            }
            str = "";
            com.shoujiduoduo.base.a.a.a("RingListAdapter", "是否有timetype=0的默认铃声：" + z);
            this.c.a(z, str, this.f1485a, this.b);
        }
    }

    public void b(c.b bVar) {
        super.b(bVar);
        this.c.f();
        com.shoujiduoduo.base.a.a.c("RingListAdapter", "查询用户铃音设置失败");
        new a.C0034a(this.c.f).b("设置彩铃").a("设置未成功, 原因:" + bVar.b()).a("确定", (DialogInterface.OnClickListener) null).a().show();
    }
}
