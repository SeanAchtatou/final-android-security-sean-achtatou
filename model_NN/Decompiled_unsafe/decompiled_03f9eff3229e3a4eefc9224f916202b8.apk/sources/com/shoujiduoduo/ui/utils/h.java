package com.shoujiduoduo.ui.utils;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import cn.banshenggua.aichang.utils.Constants;
import com.d.a.b.d;
import com.shoujiduoduo.b.c.g;
import com.shoujiduoduo.base.bean.b;
import com.shoujiduoduo.base.bean.c;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.util.f;
import com.shoujiduoduo.util.u;
import java.text.DecimalFormat;

/* compiled from: CollectListAdapter */
public class h extends g {

    /* renamed from: a  reason: collision with root package name */
    private g f1505a;
    private Context b;

    public h(Context context) {
        this.b = context;
    }

    public int getCount() {
        if (this.f1505a != null) {
            return this.f1505a.c();
        }
        com.shoujiduoduo.base.a.a.a("CollectListAdapter", "count:0");
        return 0;
    }

    public Object getItem(int i) {
        if (this.f1505a != null) {
            return this.f1505a.a(i);
        }
        return null;
    }

    public long getItemId(int i) {
        return (long) i;
    }

    /* compiled from: CollectListAdapter */
    private static class a {

        /* renamed from: a  reason: collision with root package name */
        ImageView f1506a;
        TextView b;
        TextView c;
        TextView d;
        TextView e;
        TextView f;

        private a() {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int i, View view, ViewGroup viewGroup) {
        a aVar;
        if (this.f1505a == null) {
            com.shoujiduoduo.base.a.a.a("CollectListAdapter", "return null");
            return null;
        }
        if (view == null) {
            view = LayoutInflater.from(this.b).inflate((int) R.layout.listitem_collect, viewGroup, false);
            a aVar2 = new a();
            aVar2.f1506a = (ImageView) view.findViewById(R.id.pic);
            aVar2.b = (TextView) view.findViewById(R.id.title);
            aVar2.c = (TextView) view.findViewById(R.id.content);
            if (f.n()) {
                aVar2.c.setLines(1);
            }
            aVar2.d = (TextView) view.findViewById(R.id.releate_time);
            aVar2.e = (TextView) view.findViewById(R.id.fav_num);
            aVar2.f = (TextView) view.findViewById(R.id.artist);
            view.setTag(aVar2);
            aVar = aVar2;
        } else {
            aVar = (a) view.getTag();
        }
        b b2 = this.f1505a.a(i);
        com.shoujiduoduo.base.a.a.a("CollectListAdapter", "title:" + b2.b + ", content:" + b2.c);
        d.a().a(b2.f819a, aVar.f1506a, v.a().g());
        aVar.b.setText(b2.b);
        aVar.c.setText(b2.c);
        aVar.d.setText(b2.d);
        aVar.f.setText(TextUtils.isEmpty(b2.h) ? "多多网友" : b2.h);
        int a2 = u.a(b2.f, Constants.CLEARIMGED);
        StringBuilder sb = new StringBuilder();
        if (a2 > 10000) {
            sb.append(new DecimalFormat("#.00").format((double) (((float) a2) / 10000.0f)));
            sb.append("万");
        } else {
            sb.append(a2);
        }
        aVar.e.setText(sb.toString());
        return view;
    }

    public void a(boolean z) {
    }

    public void a(c cVar) {
        this.f1505a = (g) cVar;
    }

    public void a() {
    }

    public void b() {
    }
}
