package com.shoujiduoduo.ui.utils.pageindicator;

import android.view.View;

/* compiled from: TabPageIndicator */
class g implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ View f1531a;
    final /* synthetic */ TabPageIndicator b;

    g(TabPageIndicator tabPageIndicator, View view) {
        this.b = tabPageIndicator;
        this.f1531a = view;
    }

    public void run() {
        this.b.smoothScrollTo(this.f1531a.getLeft() - ((this.b.getWidth() - this.f1531a.getWidth()) / 2), 0);
        Runnable unused = this.b.b = null;
    }
}
