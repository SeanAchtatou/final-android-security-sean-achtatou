package com.shoujiduoduo.ui.utils;

import android.content.DialogInterface;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.util.av;
import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.d.b;
import com.shoujiduoduo.util.widget.a;

/* compiled from: RingListAdapter */
class az extends a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ StringBuilder f1477a;
    final /* synthetic */ RingData b;
    final /* synthetic */ String c;
    final /* synthetic */ y d;

    az(y yVar, StringBuilder sb, RingData ringData, String str) {
        this.d = yVar;
        this.f1477a = sb;
        this.b = ringData;
        this.c = str;
    }

    public void a(c.b bVar) {
        super.a(bVar);
        if (bVar != null && (bVar instanceof c.y)) {
            c.y yVar = (c.y) bVar;
            this.f1477a.append("&info=").append(av.a("audioId:" + yVar.f1619a + ", ringname:" + this.b.e));
            b.a().b(yVar.f1619a, this.c, this.f1477a.toString(), new ba(this));
        }
    }

    public void b(c.b bVar) {
        super.b(bVar);
        this.d.f();
        com.shoujiduoduo.base.a.a.a("RingListAdapter", "diy_clip_upload onFailure:" + bVar.toString());
        new a.C0034a(this.d.f).b("设置彩铃").a(bVar.b()).a("确定", (DialogInterface.OnClickListener) null).a().show();
    }
}
