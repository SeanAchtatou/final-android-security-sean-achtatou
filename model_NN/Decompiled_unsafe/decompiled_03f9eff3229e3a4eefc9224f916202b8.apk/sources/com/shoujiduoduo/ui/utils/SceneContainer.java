package com.shoujiduoduo.ui.utils;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.Scroller;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.util.a;

public class SceneContainer extends ViewGroup {

    /* renamed from: a  reason: collision with root package name */
    private static final String f1453a = SceneContainer.class.getSimpleName();
    private Scroller b;
    private int c = 0;
    private int d;
    private Animation e;
    private Animation f;
    private RelativeLayout g = null;
    private RelativeLayout h = null;
    private RelativeLayout i = null;

    public int getCurrentScene() {
        return this.c;
    }

    public SceneContainer(Context context) {
        super(context);
    }

    public SceneContainer(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
    }

    public SceneContainer(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        int childCount = getChildCount();
        for (int i4 = 0; i4 < childCount; i4++) {
            getChildAt(i4).measure(i2, i3);
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        int childCount = getChildCount();
        int i6 = 0;
        for (int i7 = 0; i7 < childCount; i7++) {
            View childAt = getChildAt(i7);
            if (childAt.getVisibility() != 8) {
                int measuredWidth = childAt.getMeasuredWidth();
                childAt.layout(i6, 0, i6 + measuredWidth, childAt.getMeasuredHeight());
                i6 += measuredWidth;
            }
        }
    }

    public void computeScroll() {
        if (this.b != null && this.b.computeScrollOffset()) {
            this.d = this.b.getCurrX();
            if (this.d != getScrollX()) {
                scrollTo(this.d, 0);
            }
            postInvalidate();
        }
    }

    public void a(Activity activity) {
        Context context = getContext();
        this.b = new Scroller(context, new AccelerateDecelerateInterpolator());
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService("layout_inflater");
        this.g = (RelativeLayout) layoutInflater.inflate((int) R.layout.homepage, (ViewGroup) null);
        this.h = (RelativeLayout) layoutInflater.inflate((int) R.layout.category, (ViewGroup) null);
        this.i = (RelativeLayout) layoutInflater.inflate((int) R.layout.my_ringtone, (ViewGroup) null);
        addView(this.g);
        addView(this.h);
        addView(this.i);
        if (a.b()) {
            addView(layoutInflater.inflate((int) R.layout.ac_view_layout, (ViewGroup) null));
        } else if (a.f()) {
            addView(layoutInflater.inflate((int) R.layout.wall_ad_view, (ViewGroup) null));
        } else {
            addView(layoutInflater.inflate((int) R.layout.more_options, (ViewGroup) null));
        }
    }

    public int a(int i2) {
        if (i2 == this.c) {
            return i2;
        }
        if (i2 >= 4 || i2 < 0) {
            return this.c;
        }
        View focusedChild = getFocusedChild();
        if (!(focusedChild == null || i2 == this.c || focusedChild != getChildAt(this.c))) {
            focusedChild.clearFocus();
        }
        int scrollX = getScrollX();
        int width = (getWidth() * i2) - scrollX;
        if (i2 - this.c == 3) {
            if (this.e == null) {
                this.e = AnimationUtils.loadAnimation(getContext(), R.anim.push_right_in);
            }
            this.b.startScroll(scrollX, 0, width, 0, 0);
            com.shoujiduoduo.base.a.a.b(f1453a, "1: switch from " + this.c + "to " + i2);
        } else if (i2 - this.c == -3) {
            if (this.f == null) {
                this.f = AnimationUtils.loadAnimation(getContext(), R.anim.push_left_in);
            }
            this.b.startScroll(scrollX, 0, width, 0, 0);
            com.shoujiduoduo.base.a.a.b(f1453a, "2: switch from " + this.c + "to " + i2);
        } else {
            this.b.startScroll(scrollX, 0, width, 0, 0);
            com.shoujiduoduo.base.a.a.b(f1453a, "3: switch from " + this.c + "to " + i2);
        }
        invalidate();
        int i3 = this.c;
        this.c = i2;
        com.shoujiduoduo.base.a.a.b(f1453a, "end switch.");
        return i3;
    }
}
