package com.shoujiduoduo.ui.utils;

import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.a.b.b;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.base.bean.ab;
import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;

/* compiled from: RingListAdapter */
class ca extends a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RingData f1501a;
    final /* synthetic */ y b;

    ca(y yVar, RingData ringData) {
        this.b = yVar;
        this.f1501a = ringData;
    }

    public void a(c.b bVar) {
        super.a(bVar);
        com.shoujiduoduo.base.a.a.a("RingListAdapter", "cmcc sdk is inited");
        if (bVar instanceof c.l) {
            c.l lVar = (c.l) bVar;
            if (lVar.f1605a == c.l.a.sms_code) {
                ab c = b.g().c();
                if (!c.i()) {
                    c.b(lVar.d);
                    c.a("phone_" + lVar.d);
                }
                c.d(lVar.d);
                c.c(1);
                b.g().a(c);
                x.a().a(com.shoujiduoduo.a.a.b.OBSERVER_USER_CENTER, new cb(this));
            }
        }
        this.b.c(this.f1501a);
    }

    public void b(c.b bVar) {
        super.b(bVar);
        com.shoujiduoduo.base.a.a.a("RingListAdapter", "cmcc sdk is not inited");
        this.b.c();
        this.b.s.sendEmptyMessageDelayed(1, 3000);
    }
}
