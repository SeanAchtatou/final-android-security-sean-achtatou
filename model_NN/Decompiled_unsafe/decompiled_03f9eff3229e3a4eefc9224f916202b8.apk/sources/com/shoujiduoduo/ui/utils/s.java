package com.shoujiduoduo.ui.utils;

import com.shoujiduoduo.a.c.f;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.base.bean.c;
import com.shoujiduoduo.ui.utils.DDListFragment;

/* compiled from: DDListFragment */
class s implements f {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DDListFragment f1536a;

    s(DDListFragment dDListFragment) {
        this.f1536a = dDListFragment;
    }

    public void a(c cVar, int i) {
        if (this.f1536a.c != null && cVar.a().equals(this.f1536a.c.a())) {
            a.a(DDListFragment.b, "onDataUpdate in, id:" + this.f1536a.c.a());
            if (!cVar.a().equals(this.f1536a.c.a())) {
                a.a(DDListFragment.b, "onDataUpdate: the list update is not current list.");
                return;
            }
            switch (i) {
                case 0:
                    if (this.f1536a.r == DDListFragment.e.LIST_LOADING) {
                        a.a(DDListFragment.b, "show content now! listid:" + cVar.a());
                        this.f1536a.a(DDListFragment.e.LIST_CONTENT);
                    }
                    this.f1536a.h.notifyDataSetChanged();
                    return;
                case 1:
                    a.a(DDListFragment.b, "show failed now. listid:" + cVar.a());
                    this.f1536a.a(DDListFragment.e.LIST_FAILED);
                    return;
                case 2:
                    a.a(DDListFragment.b, "more data ready. notify the adapter to update. listid:" + cVar.a());
                    a.a(DDListFragment.b, "FooterState: set failed onDataUpdate.");
                    this.f1536a.a(DDListFragment.c.RETRIEVE_FAILED);
                    this.f1536a.h.notifyDataSetChanged();
                    return;
                default:
                    return;
            }
        }
    }
}
