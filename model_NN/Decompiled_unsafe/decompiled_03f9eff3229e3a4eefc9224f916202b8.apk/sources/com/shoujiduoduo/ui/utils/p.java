package com.shoujiduoduo.ui.utils;

import com.shoujiduoduo.a.c.w;
import com.shoujiduoduo.b.c.n;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.base.bean.c;
import com.shoujiduoduo.base.bean.f;
import com.shoujiduoduo.ui.utils.DDListFragment;

/* compiled from: DDListFragment */
class p implements w {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DDListFragment f1513a;

    p(DDListFragment dDListFragment) {
        this.f1513a = dDListFragment;
    }

    public void a(int i) {
        if (this.f1513a.c != null && this.f1513a.i()) {
            a.a(DDListFragment.b, "vipType:" + i);
            if ((i == 1 && !this.f1513a.c.b().equals(f.a.list_ring_cmcc)) || ((i == 3 && !this.f1513a.c.b().equals(f.a.list_ring_cucc)) || (i == 2 && !this.f1513a.c.b().equals(f.a.list_ring_ctcc)))) {
                switch (i) {
                    case 1:
                        c unused = this.f1513a.c = new n(f.a.list_ring_cmcc, "", false, "");
                        break;
                    case 2:
                        c unused2 = this.f1513a.c = new n(f.a.list_ring_ctcc, "", false, "");
                        break;
                    case 3:
                        c unused3 = this.f1513a.c = new n(f.a.list_ring_cucc, "", false, "");
                        break;
                }
                a.a(DDListFragment.b, "vipType:" + i + ", cur list id:" + this.f1513a.c.a());
                this.f1513a.a(DDListFragment.e.LIST_LOADING);
                this.f1513a.c.f();
                this.f1513a.g();
            }
        }
    }
}
