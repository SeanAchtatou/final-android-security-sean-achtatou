package com.shoujiduoduo.ui.utils;

import android.widget.AbsListView;
import com.shoujiduoduo.ui.utils.DDListFragment;

/* compiled from: DDListFragment */
class j implements AbsListView.OnScrollListener {

    /* renamed from: a  reason: collision with root package name */
    boolean f1508a = false;
    final /* synthetic */ DDListFragment b;

    j(DDListFragment dDListFragment) {
        this.b = dDListFragment;
    }

    public void onScrollStateChanged(AbsListView absListView, int i) {
        if (this.f1508a && i == 0) {
            if (this.b.c != null) {
                if (this.b.c.g()) {
                    if (!this.b.c.d()) {
                        this.b.c.e();
                        this.b.a(DDListFragment.c.RETRIEVE);
                    }
                } else if (this.b.c.c() > 1) {
                    this.b.a(DDListFragment.c.TOTAL);
                } else {
                    this.b.a(DDListFragment.c.INVISIBLE);
                }
            }
            this.f1508a = false;
        }
    }

    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
        if (i + i2 == i3 && i3 > 0 && i2 < i3) {
            this.f1508a = true;
        }
    }
}
