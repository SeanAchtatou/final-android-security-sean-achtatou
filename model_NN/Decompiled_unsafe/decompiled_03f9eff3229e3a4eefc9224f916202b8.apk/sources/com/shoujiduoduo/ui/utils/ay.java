package com.shoujiduoduo.ui.utils;

import android.content.DialogInterface;
import com.shoujiduoduo.util.f;

/* compiled from: RingListAdapter */
class ay implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ax f1476a;

    ay(ax axVar) {
        this.f1476a = axVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.ui.utils.y.b(com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, java.lang.String, boolean):void
     arg types: [com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, java.lang.String, int]
     candidates:
      com.shoujiduoduo.ui.utils.y.b(com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.f$b):void
      com.shoujiduoduo.ui.utils.y.b(com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, boolean):void
     arg types: [com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, int]
     candidates:
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.f$b):android.view.View
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.f$b):void
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.base.bean.RingData, java.lang.String, boolean):void
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.base.bean.RingData, boolean, boolean):void
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, java.lang.String):void
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, java.lang.String, com.shoujiduoduo.base.bean.RingData):void
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, boolean, com.shoujiduoduo.util.f$b):void
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, boolean):void */
    public void onClick(DialogInterface dialogInterface, int i) {
        dialogInterface.dismiss();
        if (this.f1476a.c.equals(f.b.cu)) {
            this.f1476a.e.c(this.f1476a.b, this.f1476a.d, true);
        } else if (this.f1476a.c.equals(f.b.ct)) {
            this.f1476a.e.d(this.f1476a.b, this.f1476a.d, true);
        } else if (this.f1476a.c.equals(f.b.f1671a)) {
            this.f1476a.e.a(this.f1476a.b, true);
        }
    }
}
