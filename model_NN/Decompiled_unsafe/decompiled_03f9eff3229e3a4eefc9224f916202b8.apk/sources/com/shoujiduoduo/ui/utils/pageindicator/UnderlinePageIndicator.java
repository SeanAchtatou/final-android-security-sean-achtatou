package com.shoujiduoduo.ui.utils.pageindicator;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.ViewConfigurationCompat;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import com.shoujiduoduo.ringtone.R;

public class UnderlinePageIndicator extends View implements PageIndicator {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final Paint f1526a;
    /* access modifiers changed from: private */
    public boolean b;
    private int c;
    private int d;
    /* access modifiers changed from: private */
    public int e;
    private ViewPager f;
    private ViewPager.OnPageChangeListener g;
    private int h;
    private int i;
    private float j;
    private int k;
    private float l;
    private int m;
    private boolean n;
    /* access modifiers changed from: private */
    public final Runnable o;

    public UnderlinePageIndicator(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, R.attr.vpiUnderlinePageIndicatorStyle);
    }

    public UnderlinePageIndicator(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.f1526a = new Paint(1);
        this.l = -1.0f;
        this.m = -1;
        this.o = new i(this);
        if (!isInEditMode()) {
            Resources resources = getResources();
            boolean z = resources.getBoolean(R.bool.default_underline_indicator_fades);
            int integer = resources.getInteger(R.integer.default_underline_indicator_fade_delay);
            int integer2 = resources.getInteger(R.integer.default_underline_indicator_fade_length);
            int color = resources.getColor(R.color.default_underline_indicator_selected_color);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.UnderlinePageIndicator, i2, 0);
            setFades(obtainStyledAttributes.getBoolean(2, z));
            setSelectedColor(obtainStyledAttributes.getColor(1, color));
            setFadeDelay(obtainStyledAttributes.getInteger(3, integer));
            setFadeLength(obtainStyledAttributes.getInteger(4, integer2));
            Drawable drawable = obtainStyledAttributes.getDrawable(0);
            if (drawable != null) {
                setBackgroundDrawable(drawable);
            }
            obtainStyledAttributes.recycle();
            this.k = ViewConfigurationCompat.getScaledPagingTouchSlop(ViewConfiguration.get(context));
        }
    }

    public boolean getFades() {
        return this.b;
    }

    public void setFades(boolean z) {
        if (z != this.b) {
            this.b = z;
            if (z) {
                post(this.o);
                return;
            }
            removeCallbacks(this.o);
            this.f1526a.setAlpha(MotionEventCompat.ACTION_MASK);
            invalidate();
        }
    }

    public int getFadeDelay() {
        return this.c;
    }

    public void setFadeDelay(int i2) {
        this.c = i2;
    }

    public int getFadeLength() {
        return this.d;
    }

    public void setFadeLength(int i2) {
        this.d = i2;
        this.e = MotionEventCompat.ACTION_MASK / (this.d / 30);
    }

    public int getSelectedColor() {
        return this.f1526a.getColor();
    }

    public void setSelectedColor(int i2) {
        this.f1526a.setColor(i2);
        invalidate();
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        int count;
        super.onDraw(canvas);
        if (this.f != null && (count = this.f.getAdapter().getCount()) != 0) {
            if (this.i >= count) {
                setCurrentItem(count - 1);
                return;
            }
            int paddingLeft = getPaddingLeft();
            float width = ((float) ((getWidth() - paddingLeft) - getPaddingRight())) / (((float) count) * 1.0f);
            float f2 = ((float) paddingLeft) + ((((float) this.i) + this.j) * width);
            canvas.drawRect(f2, (float) getPaddingTop(), f2 + width, (float) (getHeight() - getPaddingBottom()), this.f1526a);
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        int i2 = 0;
        if (super.onTouchEvent(motionEvent)) {
            return true;
        }
        if (this.f == null || this.f.getAdapter().getCount() == 0) {
            return false;
        }
        int action = motionEvent.getAction() & MotionEventCompat.ACTION_MASK;
        switch (action) {
            case 0:
                this.m = MotionEventCompat.getPointerId(motionEvent, 0);
                this.l = motionEvent.getX();
                return true;
            case 1:
            case 3:
                if (!this.n) {
                    int count = this.f.getAdapter().getCount();
                    int width = getWidth();
                    float f2 = ((float) width) / 2.0f;
                    float f3 = ((float) width) / 6.0f;
                    if (this.i <= 0 || motionEvent.getX() >= f2 - f3) {
                        if (this.i < count - 1 && motionEvent.getX() > f3 + f2) {
                            if (action == 3) {
                                return true;
                            }
                            this.f.setCurrentItem(this.i + 1);
                            return true;
                        }
                    } else if (action == 3) {
                        return true;
                    } else {
                        this.f.setCurrentItem(this.i - 1);
                        return true;
                    }
                }
                this.n = false;
                this.m = -1;
                if (!this.f.isFakeDragging()) {
                    return true;
                }
                this.f.endFakeDrag();
                return true;
            case 2:
                float x = MotionEventCompat.getX(motionEvent, MotionEventCompat.findPointerIndex(motionEvent, this.m));
                float f4 = x - this.l;
                if (!this.n && Math.abs(f4) > ((float) this.k)) {
                    this.n = true;
                }
                if (!this.n) {
                    return true;
                }
                this.l = x;
                if (!this.f.isFakeDragging() && !this.f.beginFakeDrag()) {
                    return true;
                }
                this.f.fakeDragBy(f4);
                return true;
            case 4:
            default:
                return true;
            case 5:
                int actionIndex = MotionEventCompat.getActionIndex(motionEvent);
                this.l = MotionEventCompat.getX(motionEvent, actionIndex);
                this.m = MotionEventCompat.getPointerId(motionEvent, actionIndex);
                return true;
            case 6:
                int actionIndex2 = MotionEventCompat.getActionIndex(motionEvent);
                if (MotionEventCompat.getPointerId(motionEvent, actionIndex2) == this.m) {
                    if (actionIndex2 == 0) {
                        i2 = 1;
                    }
                    this.m = MotionEventCompat.getPointerId(motionEvent, i2);
                }
                this.l = MotionEventCompat.getX(motionEvent, MotionEventCompat.findPointerIndex(motionEvent, this.m));
                return true;
        }
    }

    public void setViewPager(ViewPager viewPager) {
        if (this.f != viewPager) {
            if (this.f != null) {
                this.f.setOnPageChangeListener(null);
            }
            if (viewPager.getAdapter() == null) {
                throw new IllegalStateException("ViewPager does not have adapter instance.");
            }
            this.f = viewPager;
            this.f.setOnPageChangeListener(this);
            invalidate();
            post(new j(this));
        }
    }

    public void setCurrentItem(int i2) {
        if (this.f == null) {
            throw new IllegalStateException("ViewPager has not been bound.");
        }
        this.f.setCurrentItem(i2);
        this.i = i2;
        invalidate();
    }

    public void onPageScrollStateChanged(int i2) {
        this.h = i2;
        if (this.g != null) {
            this.g.onPageScrollStateChanged(i2);
        }
    }

    public void onPageScrolled(int i2, float f2, int i3) {
        this.i = i2;
        this.j = f2;
        if (this.b) {
            if (i3 > 0) {
                removeCallbacks(this.o);
                this.f1526a.setAlpha(MotionEventCompat.ACTION_MASK);
            } else if (this.h != 1) {
                postDelayed(this.o, (long) this.c);
            }
        }
        invalidate();
        if (this.g != null) {
            this.g.onPageScrolled(i2, f2, i3);
        }
    }

    public void onPageSelected(int i2) {
        if (this.h == 0) {
            this.i = i2;
            this.j = 0.0f;
            invalidate();
            this.o.run();
        }
        if (this.g != null) {
            this.g.onPageSelected(i2);
        }
    }

    public void setOnPageChangeListener(ViewPager.OnPageChangeListener onPageChangeListener) {
        this.g = onPageChangeListener;
    }

    public void onRestoreInstanceState(Parcelable parcelable) {
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        this.i = savedState.f1527a;
        requestLayout();
    }

    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.f1527a = this.i;
        return savedState;
    }

    static class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = new k();

        /* renamed from: a  reason: collision with root package name */
        int f1527a;

        /* synthetic */ SavedState(Parcel parcel, i iVar) {
            this(parcel);
        }

        public SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        private SavedState(Parcel parcel) {
            super(parcel);
            this.f1527a = parcel.readInt();
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.f1527a);
        }
    }
}
