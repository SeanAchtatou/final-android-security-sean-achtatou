package com.shoujiduoduo.ui.utils;

import android.graphics.drawable.Drawable;
import com.shoujiduoduo.util.b;
import com.shoujiduoduo.util.e;
import com.shoujiduoduo.util.s;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;

/* compiled from: AsyncImageLoader */
public class d {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final String f1502a = d.class.getSimpleName();

    /* compiled from: AsyncImageLoader */
    public interface a {
        void a(Drawable drawable, String str);
    }

    public static Drawable a(String str, a aVar) {
        String lowerCase = str.substring(str.lastIndexOf(46)).toLowerCase();
        String b = s.b();
        com.shoujiduoduo.base.a.a.a(f1502a, "Cache Path = " + b);
        if (b.length() != 0) {
            b = b + b.a(str, "UTF-8", "mobile") + lowerCase;
            com.shoujiduoduo.base.a.a.a(f1502a, str);
            com.shoujiduoduo.base.a.a.a(f1502a, b);
            File file = new File(b);
            if (file != null && file.exists() && file.canRead()) {
                com.shoujiduoduo.base.a.a.a(f1502a, "Cache File Exist!");
                Drawable createFromPath = Drawable.createFromPath(b);
                if (createFromPath == null) {
                    a("Load AdImage from cache Failed! cachePath = " + b + "; url = " + str);
                }
                return createFromPath;
            }
        }
        new f(str, b, new e(aVar, str)).start();
        return null;
    }

    public static Drawable a(String str, String str2) {
        boolean z;
        Drawable drawable = null;
        com.shoujiduoduo.base.a.a.a(f1502a + ":loadImageFromUrl", "savePath = " + str2);
        try {
            InputStream inputStream = (InputStream) new URL(str).getContent();
            if (str2.length() > 0) {
                byte[] a2 = e.a(inputStream);
                com.shoujiduoduo.base.a.a.a(f1502a + ":loadImageFromUrl", str2);
                FileOutputStream fileOutputStream = new FileOutputStream(str2);
                fileOutputStream.write(a2);
                fileOutputStream.close();
            }
            drawable = Drawable.createFromPath(str2);
            if (inputStream != null) {
                inputStream.close();
            }
            z = false;
        } catch (Exception e) {
            a("Load AdImage from network failed, Exception caught! url = " + str + "; path = " + str2 + "\n" + com.shoujiduoduo.base.a.b.a(e));
            z = true;
        }
        if (drawable == null && !z) {
            a("Load AdImage from network failed, da = 0! url = " + str + "; path = " + str2);
        }
        return drawable;
    }

    private static void a(String str) {
    }
}
