package com.shoujiduoduo.ui.utils;

import android.content.DialogInterface;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.util.am;
import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.f;
import com.shoujiduoduo.util.widget.a;

/* compiled from: RingListAdapter */
class ac extends a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RingData f1458a;
    final /* synthetic */ y b;

    ac(y yVar, RingData ringData) {
        this.b = yVar;
        this.f1458a = ringData;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, boolean, com.shoujiduoduo.util.f$b):void
     arg types: [com.shoujiduoduo.ui.utils.y, int, com.shoujiduoduo.util.f$b]
     candidates:
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.f$b):android.view.View
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.f$b):void
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.base.bean.RingData, java.lang.String, boolean):void
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.base.bean.RingData, boolean, boolean):void
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, java.lang.String):void
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, boolean):void
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, java.lang.String, com.shoujiduoduo.base.bean.RingData):void
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, boolean, com.shoujiduoduo.util.f$b):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.f$b, java.lang.String, boolean):void
     arg types: [com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.f$b, java.lang.String, int]
     candidates:
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.f$b, boolean):void
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, boolean, java.lang.String, java.lang.String, com.shoujiduoduo.base.bean.RingData):void
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.f$b, java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.f$b, boolean):void
     arg types: [com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.f$b, int]
     candidates:
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.f$b, java.lang.String, boolean):void
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, boolean, java.lang.String, java.lang.String, com.shoujiduoduo.base.bean.RingData):void
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.f$b, boolean):void */
    public void a(c.b bVar) {
        super.a(bVar);
        if (bVar instanceof c.C0028c) {
            c.C0028c cVar = (c.C0028c) bVar;
            if (cVar.e() && cVar.d()) {
                this.b.f();
                com.shoujiduoduo.base.a.a.a("RingListAdapter", "均开通，直接订购");
                this.b.a(true, f.b.f1671a);
                new a.C0034a(this.b.f).b("设置彩铃(免费)").a(this.b.a(this.f1458a, f.b.f1671a)).a("确定", new ad(this)).b("取消", (DialogInterface.OnClickListener) null).a().show();
            } else if (cVar.e() && !cVar.d()) {
                this.b.f();
                com.shoujiduoduo.base.a.a.a("RingListAdapter", "vip开通，彩铃关闭");
                this.b.a(this.f1458a, f.b.f1671a, "", true);
            } else if (cVar.e() || !cVar.d()) {
                com.shoujiduoduo.base.a.a.a("RingListAdapter", "均关闭");
                this.b.f();
                if (am.a().b("cm_sunshine_sdk_enable")) {
                    this.b.a(this.f1458a, f.b.f1671a, "", false);
                } else {
                    this.b.a(this.f1458a, "", f.b.f1671a, true);
                }
            } else {
                com.shoujiduoduo.base.a.a.a("RingListAdapter", "vip关闭，彩铃开通");
                this.b.f();
                if (am.a().b("cm_sunshine_sdk_enable")) {
                    this.b.g();
                } else {
                    this.b.a(this.f1458a, "", f.b.f1671a, false);
                }
            }
        }
    }

    public void b(c.b bVar) {
        super.b(bVar);
        this.b.f();
        com.shoujiduoduo.base.a.a.a("RingListAdapter", "查询状态失败");
        com.shoujiduoduo.util.widget.f.a("查询状态失败");
    }
}
