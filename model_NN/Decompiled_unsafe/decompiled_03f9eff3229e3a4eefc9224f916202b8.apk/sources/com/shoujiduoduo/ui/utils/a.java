package com.shoujiduoduo.ui.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.d.a.b.d;
import com.shoujiduoduo.a.b.b;
import com.shoujiduoduo.b.a.t;
import com.shoujiduoduo.ringtone.R;

/* compiled from: AdHeaderViewHolder */
public class a {

    /* renamed from: a  reason: collision with root package name */
    private View f1455a;
    private RelativeLayout b;
    private TextView c;
    private TextView d;
    private ImageView e;
    private Button f;
    /* access modifiers changed from: private */
    public t.a g;
    private Context h;

    public a(Context context) {
        this.h = context;
        b();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void b() {
        this.f1455a = LayoutInflater.from(this.h).inflate((int) R.layout.listitem_header_search_ad, (ViewGroup) null, false);
        this.b = (RelativeLayout) this.f1455a.findViewById(R.id.header_layout);
        this.c = (TextView) this.f1455a.findViewById(R.id.app_name);
        this.d = (TextView) this.f1455a.findViewById(R.id.des);
        this.e = (ImageView) this.f1455a.findViewById(R.id.ad_icon);
        this.f = (Button) this.f1455a.findViewById(R.id.btn_install);
        this.f.setOnClickListener(new b(this));
    }

    public View a() {
        return this.f1455a;
    }

    public void a(boolean z) {
        int i = 8;
        if (b.g().h()) {
            this.b.setVisibility(8);
            return;
        }
        RelativeLayout relativeLayout = this.b;
        if (z) {
            i = 0;
        }
        relativeLayout.setVisibility(i);
    }

    public void a(t.a aVar) {
        this.g = aVar;
        this.c.setText(aVar.f748a);
        this.d.setText(aVar.c);
        if (aVar.b.equals("com.duoduo.child.story")) {
            this.e.setImageResource(R.drawable.child_story_logo);
        } else if (aVar.b.equals("com.shoujiduoduo.wallpaper")) {
            this.e.setImageResource(R.drawable.wallpaper_logo);
        } else {
            d.a().a(aVar.d, this.e, v.a().i());
        }
    }
}
