package com.shoujiduoduo.ui.utils;

import android.content.DialogInterface;
import com.shoujiduoduo.a.a.b;
import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.util.as;
import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.widget.a;

/* compiled from: RingListAdapter */
class bm extends a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f1487a;
    final /* synthetic */ RingData b;
    final /* synthetic */ y c;

    bm(y yVar, String str, RingData ringData) {
        this.c = yVar;
        this.f1487a = str;
        this.b = ringData;
    }

    public void a(c.b bVar) {
        super.a(bVar);
        this.c.f();
        new a.C0034a(this.c.f).b("设置彩铃").a("已成功设置为您的当前彩铃.赶快试试吧！").a("确定", (DialogInterface.OnClickListener) null).a().show();
        as.c(this.c.f, "DEFAULT_CAILING_ID", this.f1487a);
        x.a().b(b.OBSERVER_RING_CHANGE, new bn(this));
        as.b(this.c.f, "NeedUpdateCaiLingLib", 1);
        x.a().b(b.OBSERVER_CAILING, new bo(this));
    }

    public void b(c.b bVar) {
        super.b(bVar);
        this.c.f();
        com.shoujiduoduo.base.a.a.a("RingListAdapter", "setDefaultCtccCailing, onFailure, " + bVar.toString());
        new a.C0034a(this.c.f).b("设置彩铃").a("设置未成功, 原因:" + bVar.b()).a("确定", (DialogInterface.OnClickListener) null).a().show();
    }
}
