package com.shoujiduoduo.ui.utils;

import android.graphics.drawable.Drawable;
import android.os.Handler;
import com.shoujiduoduo.base.a.a;

/* compiled from: AsyncImageLoader */
final class f extends Thread {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f1504a;
    final /* synthetic */ String b;
    final /* synthetic */ Handler c;

    f(String str, String str2, Handler handler) {
        this.f1504a = str;
        this.b = str2;
        this.c = handler;
    }

    public void run() {
        Drawable a2 = d.a(this.f1504a, this.b);
        a.a(d.f1502a, "drawable = " + a2);
        this.c.sendMessage(this.c.obtainMessage(0, a2));
    }
}
