package com.shoujiduoduo.ui.utils.pageindicator;

import android.os.Parcel;
import android.os.Parcelable;
import com.shoujiduoduo.ui.utils.pageindicator.CirclePageIndicator;

/* compiled from: CirclePageIndicator */
final class a implements Parcelable.Creator<CirclePageIndicator.SavedState> {
    a() {
    }

    /* renamed from: a */
    public CirclePageIndicator.SavedState createFromParcel(Parcel parcel) {
        return new CirclePageIndicator.SavedState(parcel);
    }

    /* renamed from: a */
    public CirclePageIndicator.SavedState[] newArray(int i) {
        return new CirclePageIndicator.SavedState[i];
    }
}
