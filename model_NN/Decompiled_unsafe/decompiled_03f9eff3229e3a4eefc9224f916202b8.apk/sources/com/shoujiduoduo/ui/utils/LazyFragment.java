package com.shoujiduoduo.ui.utils;

import android.support.v4.app.Fragment;

public abstract class LazyFragment extends Fragment {

    /* renamed from: a  reason: collision with root package name */
    protected boolean f1452a;

    /* access modifiers changed from: protected */
    public abstract void a();

    public void setUserVisibleHint(boolean z) {
        super.setUserVisibleHint(z);
        if (getUserVisibleHint()) {
            this.f1452a = true;
            e();
            return;
        }
        this.f1452a = false;
        f();
    }

    /* access modifiers changed from: protected */
    public void e() {
        a();
    }

    /* access modifiers changed from: protected */
    public void f() {
    }
}
