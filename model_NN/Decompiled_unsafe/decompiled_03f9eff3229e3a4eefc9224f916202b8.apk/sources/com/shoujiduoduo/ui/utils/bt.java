package com.shoujiduoduo.ui.utils;

import android.view.View;
import com.shoujiduoduo.a.b.b;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.util.w;
import com.shoujiduoduo.util.widget.f;

/* compiled from: RingListAdapter */
class bt implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ y f1492a;

    bt(y yVar) {
        this.f1492a = yVar;
    }

    public void onClick(View view) {
        a.a("RingListAdapter", "RingtoneDuoduo: click collect button!");
        RingData b = this.f1492a.b.a(this.f1492a.c);
        if (b != null) {
            b.b().a(b, "favorite_ring_list");
            f.a((int) R.string.add_favorite_suc, 0);
            w.a(b.g, 0, this.f1492a.b.a(), this.f1492a.b.b().toString(), "&cucid=" + b.A);
        }
    }
}
