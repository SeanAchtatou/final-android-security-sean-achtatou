package com.shoujiduoduo.ui.utils;

import android.app.ProgressDialog;
import android.content.Context;

/* compiled from: ProgressWaitDlg */
public class w {

    /* renamed from: a  reason: collision with root package name */
    private static ProgressDialog f1540a = null;
    private static ProgressDialog b = null;

    public static void a(Context context) {
        a(context, "请稍候...");
    }

    public static void a(Context context, String str) {
        if (f1540a == null) {
            f1540a = new ProgressDialog(context);
            f1540a.setMessage(str);
            f1540a.setIndeterminate(false);
            f1540a.setCancelable(true);
            f1540a.setCanceledOnTouchOutside(false);
            f1540a.show();
        }
    }

    public static void a() {
        if (f1540a != null) {
            f1540a.dismiss();
            f1540a = null;
        }
    }

    public static void b(Context context) {
        if (b == null) {
            b = new ProgressDialog(context);
            b.setMessage("中国移动提示您：首次使用彩铃功能需要发送一条免费短信，该过程自动完成，如弹出警告，请选择允许。");
            b.setIndeterminate(false);
            b.setCancelable(true);
            b.setCanceledOnTouchOutside(false);
            b.setButton(-1, "确定", new x());
            b.show();
        }
    }

    public static void b() {
        if (b != null) {
            b.dismiss();
            b = null;
        }
    }
}
