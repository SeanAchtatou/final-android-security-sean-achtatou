package com.shoujiduoduo.ui.utils.pageindicator;

import android.os.Parcel;
import android.os.Parcelable;
import com.shoujiduoduo.ui.utils.pageindicator.UnderlinePageIndicator;

/* compiled from: UnderlinePageIndicator */
final class k implements Parcelable.Creator<UnderlinePageIndicator.SavedState> {
    k() {
    }

    /* renamed from: a */
    public UnderlinePageIndicator.SavedState createFromParcel(Parcel parcel) {
        return new UnderlinePageIndicator.SavedState(parcel, null);
    }

    /* renamed from: a */
    public UnderlinePageIndicator.SavedState[] newArray(int i) {
        return new UnderlinePageIndicator.SavedState[i];
    }
}
