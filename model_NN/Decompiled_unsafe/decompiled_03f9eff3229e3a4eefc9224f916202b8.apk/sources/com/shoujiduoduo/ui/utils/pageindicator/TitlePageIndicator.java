package com.shoujiduoduo.ui.utils.pageindicator;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.ViewConfigurationCompat;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import com.shoujiduoduo.ringtone.R;
import java.util.ArrayList;

public class TitlePageIndicator extends View implements PageIndicator {
    private c A;

    /* renamed from: a  reason: collision with root package name */
    private ViewPager f1521a;
    private ViewPager.OnPageChangeListener b;
    private int c;
    private float d;
    private int e;
    private final Paint f;
    private boolean g;
    private int h;
    private int i;
    private Path j;
    private final Rect k;
    private final Paint l;
    private a m;
    private b n;
    private final Paint o;
    private float p;
    private float q;
    private float r;
    private float s;
    private float t;
    private float u;
    private float v;
    private int w;
    private float x;
    private int y;
    private boolean z;

    public interface c {
        void a(int i);
    }

    public enum a {
        None(0),
        Triangle(1),
        Underline(2);
        
        public final int d;

        private a(int i) {
            this.d = i;
        }

        public static a a(int i) {
            for (a aVar : values()) {
                if (aVar.d == i) {
                    return aVar;
                }
            }
            return null;
        }
    }

    public enum b {
        Bottom(0),
        Top(1);
        
        public final int c;

        private b(int i) {
            this.c = i;
        }

        public static b a(int i) {
            for (b bVar : values()) {
                if (bVar.c == i) {
                    return bVar;
                }
            }
            return null;
        }
    }

    public TitlePageIndicator(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, R.attr.vpiTitlePageIndicatorStyle);
    }

    public TitlePageIndicator(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.c = -1;
        this.f = new Paint();
        this.j = new Path();
        this.k = new Rect();
        this.l = new Paint();
        this.o = new Paint();
        this.x = -1.0f;
        this.y = -1;
        if (!isInEditMode()) {
            Resources resources = getResources();
            int color = resources.getColor(R.color.default_title_indicator_footer_color);
            float dimension = resources.getDimension(R.dimen.default_title_indicator_footer_line_height);
            int integer = resources.getInteger(R.integer.default_title_indicator_footer_indicator_style);
            float dimension2 = resources.getDimension(R.dimen.default_title_indicator_footer_indicator_height);
            float dimension3 = resources.getDimension(R.dimen.default_title_indicator_footer_indicator_underline_padding);
            float dimension4 = resources.getDimension(R.dimen.default_title_indicator_footer_padding);
            int integer2 = resources.getInteger(R.integer.default_title_indicator_line_position);
            int color2 = resources.getColor(R.color.default_title_indicator_selected_color);
            boolean z2 = resources.getBoolean(R.bool.default_title_indicator_selected_bold);
            int color3 = resources.getColor(R.color.default_title_indicator_text_color);
            float dimension5 = resources.getDimension(R.dimen.default_title_indicator_text_size);
            float dimension6 = resources.getDimension(R.dimen.default_title_indicator_title_padding);
            float dimension7 = resources.getDimension(R.dimen.default_title_indicator_clip_padding);
            float dimension8 = resources.getDimension(R.dimen.default_title_indicator_top_padding);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.TitlePageIndicator, i2, 0);
            this.v = obtainStyledAttributes.getDimension(6, dimension);
            this.m = a.a(obtainStyledAttributes.getInteger(7, integer));
            this.p = obtainStyledAttributes.getDimension(8, dimension2);
            this.q = obtainStyledAttributes.getDimension(9, dimension3);
            this.r = obtainStyledAttributes.getDimension(10, dimension4);
            this.n = b.a(obtainStyledAttributes.getInteger(11, integer2));
            this.t = obtainStyledAttributes.getDimension(14, dimension8);
            this.s = obtainStyledAttributes.getDimension(13, dimension6);
            this.u = obtainStyledAttributes.getDimension(4, dimension7);
            this.i = obtainStyledAttributes.getColor(3, color2);
            this.h = obtainStyledAttributes.getColor(1, color3);
            this.g = obtainStyledAttributes.getBoolean(12, z2);
            float dimension9 = obtainStyledAttributes.getDimension(0, dimension5);
            int color4 = obtainStyledAttributes.getColor(5, color);
            this.f.setTextSize(dimension9);
            this.f.setAntiAlias(true);
            this.l.setStyle(Paint.Style.FILL_AND_STROKE);
            this.l.setStrokeWidth(this.v);
            this.l.setColor(color4);
            this.o.setStyle(Paint.Style.FILL_AND_STROKE);
            this.o.setColor(color4);
            Drawable drawable = obtainStyledAttributes.getDrawable(2);
            if (drawable != null) {
                setBackgroundDrawable(drawable);
            }
            obtainStyledAttributes.recycle();
            this.w = ViewConfigurationCompat.getScaledPagingTouchSlop(ViewConfiguration.get(context));
        }
    }

    public int getFooterColor() {
        return this.l.getColor();
    }

    public void setFooterColor(int i2) {
        this.l.setColor(i2);
        this.o.setColor(i2);
        invalidate();
    }

    public float getFooterLineHeight() {
        return this.v;
    }

    public void setFooterLineHeight(float f2) {
        this.v = f2;
        this.l.setStrokeWidth(this.v);
        invalidate();
    }

    public float getFooterIndicatorHeight() {
        return this.p;
    }

    public void setFooterIndicatorHeight(float f2) {
        this.p = f2;
        invalidate();
    }

    public float getFooterIndicatorPadding() {
        return this.r;
    }

    public void setFooterIndicatorPadding(float f2) {
        this.r = f2;
        invalidate();
    }

    public a getFooterIndicatorStyle() {
        return this.m;
    }

    public void setFooterIndicatorStyle(a aVar) {
        this.m = aVar;
        invalidate();
    }

    public b getLinePosition() {
        return this.n;
    }

    public void setLinePosition(b bVar) {
        this.n = bVar;
        invalidate();
    }

    public int getSelectedColor() {
        return this.i;
    }

    public void setSelectedColor(int i2) {
        this.i = i2;
        invalidate();
    }

    public void setSelectedBold(boolean z2) {
        this.g = z2;
        invalidate();
    }

    public int getTextColor() {
        return this.h;
    }

    public void setTextColor(int i2) {
        this.f.setColor(i2);
        this.h = i2;
        invalidate();
    }

    public float getTextSize() {
        return this.f.getTextSize();
    }

    public void setTextSize(float f2) {
        this.f.setTextSize(f2);
        invalidate();
    }

    public float getTitlePadding() {
        return this.s;
    }

    public void setTitlePadding(float f2) {
        this.s = f2;
        invalidate();
    }

    public float getTopPadding() {
        return this.t;
    }

    public void setTopPadding(float f2) {
        this.t = f2;
        invalidate();
    }

    public float getClipPadding() {
        return this.u;
    }

    public void setClipPadding(float f2) {
        this.u = f2;
        invalidate();
    }

    public void setTypeface(Typeface typeface) {
        this.f.setTypeface(typeface);
        invalidate();
    }

    public Typeface getTypeface() {
        return this.f.getTypeface();
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        int count;
        int i2;
        float f2;
        boolean z2;
        int i3;
        float f3;
        float f4;
        super.onDraw(canvas);
        if (this.f1521a != null && (count = this.f1521a.getAdapter().getCount()) != 0) {
            if (this.c == -1 && this.f1521a != null) {
                this.c = this.f1521a.getCurrentItem();
            }
            ArrayList<Rect> a2 = a(this.f);
            int size = a2.size();
            if (this.c >= size) {
                setCurrentItem(size - 1);
                return;
            }
            int i4 = count - 1;
            float width = ((float) getWidth()) / 2.0f;
            int left = getLeft();
            float f5 = ((float) left) + this.u;
            int width2 = getWidth();
            int height = getHeight();
            int i5 = left + width2;
            float f6 = ((float) i5) - this.u;
            int i6 = this.c;
            if (((double) this.d) <= 0.5d) {
                i2 = i6;
                f2 = this.d;
            } else {
                i2 = i6 + 1;
                f2 = 1.0f - this.d;
            }
            boolean z3 = f2 <= 0.25f;
            if (f2 <= 0.05f) {
                z2 = true;
            } else {
                z2 = false;
            }
            float f7 = (0.25f - f2) / 0.25f;
            Rect rect = a2.get(this.c);
            float f8 = (float) (rect.right - rect.left);
            if (((float) rect.left) < f5) {
                b(rect, f8, left);
            }
            if (((float) rect.right) > f6) {
                a(rect, f8, i5);
            }
            if (this.c > 0) {
                for (int i7 = this.c - 1; i7 >= 0; i7--) {
                    Rect rect2 = a2.get(i7);
                    if (((float) rect2.left) < f5) {
                        int i8 = rect2.right - rect2.left;
                        b(rect2, (float) i8, left);
                        Rect rect3 = a2.get(i7 + 1);
                        if (((float) rect2.right) + this.s > ((float) rect3.left)) {
                            rect2.left = (int) (((float) (rect3.left - i8)) - this.s);
                            rect2.right = rect2.left + i8;
                        }
                    }
                }
            }
            if (this.c < i4) {
                int i9 = this.c + 1;
                while (true) {
                    int i10 = i9;
                    if (i10 >= count) {
                        break;
                    }
                    Rect rect4 = a2.get(i10);
                    if (((float) rect4.right) > f6) {
                        int i11 = rect4.right - rect4.left;
                        a(rect4, (float) i11, i5);
                        Rect rect5 = a2.get(i10 - 1);
                        if (((float) rect4.left) - this.s < ((float) rect5.right)) {
                            rect4.left = (int) (((float) rect5.right) + this.s);
                            rect4.right = rect4.left + i11;
                        }
                    }
                    i9 = i10 + 1;
                }
            }
            int i12 = this.h >>> 24;
            int i13 = 0;
            while (true) {
                int i14 = i13;
                if (i14 >= count) {
                    break;
                }
                Rect rect6 = a2.get(i14);
                if ((rect6.left > left && rect6.left < i5) || (rect6.right > left && rect6.right < i5)) {
                    boolean z4 = i14 == i2;
                    CharSequence a3 = a(i14);
                    this.f.setFakeBoldText(z4 && z2 && this.g);
                    this.f.setColor(this.h);
                    if (z4 && z3) {
                        this.f.setAlpha(i12 - ((int) (((float) i12) * f7)));
                    }
                    if (i14 < size - 1) {
                        Rect rect7 = a2.get(i14 + 1);
                        if (((float) rect6.right) + this.s > ((float) rect7.left)) {
                            int i15 = rect6.right - rect6.left;
                            rect6.left = (int) (((float) (rect7.left - i15)) - this.s);
                            rect6.right = rect6.left + i15;
                        }
                    }
                    canvas.drawText(a3, 0, a3.length(), (float) rect6.left, this.t + ((float) rect6.bottom), this.f);
                    if (z4 && z3) {
                        this.f.setColor(this.i);
                        this.f.setAlpha((int) (((float) (this.i >>> 24)) * f7));
                        canvas.drawText(a3, 0, a3.length(), (float) rect6.left, this.t + ((float) rect6.bottom), this.f);
                    }
                }
                i13 = i14 + 1;
            }
            float f9 = this.v;
            float f10 = this.p;
            if (this.n == b.Top) {
                i3 = 0;
                float f11 = -f10;
                f4 = -f9;
                f3 = f11;
            } else {
                i3 = height;
                float f12 = f9;
                f3 = f10;
                f4 = f12;
            }
            this.j.reset();
            this.j.moveTo(0.0f, ((float) i3) - (f4 / 2.0f));
            this.j.lineTo((float) width2, ((float) i3) - (f4 / 2.0f));
            this.j.close();
            canvas.drawPath(this.j, this.l);
            float f13 = ((float) i3) - f4;
            switch (this.m) {
                case Triangle:
                    this.j.reset();
                    this.j.moveTo(width, f13 - f3);
                    this.j.lineTo(width + f3, f13);
                    this.j.lineTo(width - f3, f13);
                    this.j.close();
                    canvas.drawPath(this.j, this.o);
                    return;
                case Underline:
                    if (z3 && i2 < size) {
                        Rect rect8 = a2.get(i2);
                        float f14 = ((float) rect8.right) + this.q;
                        float f15 = ((float) rect8.left) - this.q;
                        float f16 = f13 - f3;
                        this.j.reset();
                        this.j.moveTo(f15, f13);
                        this.j.lineTo(f14, f13);
                        this.j.lineTo(f14, f16);
                        this.j.lineTo(f15, f16);
                        this.j.close();
                        this.o.setAlpha((int) (255.0f * f7));
                        canvas.drawPath(this.j, this.o);
                        this.o.setAlpha(MotionEventCompat.ACTION_MASK);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        int i2 = 0;
        if (super.onTouchEvent(motionEvent)) {
            return true;
        }
        if (this.f1521a == null || this.f1521a.getAdapter().getCount() == 0) {
            return false;
        }
        int action = motionEvent.getAction() & MotionEventCompat.ACTION_MASK;
        switch (action) {
            case 0:
                this.y = MotionEventCompat.getPointerId(motionEvent, 0);
                this.x = motionEvent.getX();
                return true;
            case 1:
            case 3:
                if (!this.z) {
                    int count = this.f1521a.getAdapter().getCount();
                    int width = getWidth();
                    float f2 = ((float) width) / 2.0f;
                    float f3 = ((float) width) / 6.0f;
                    float f4 = f2 - f3;
                    float f5 = f3 + f2;
                    float x2 = motionEvent.getX();
                    if (x2 < f4) {
                        if (this.c > 0) {
                            if (action == 3) {
                                return true;
                            }
                            this.f1521a.setCurrentItem(this.c - 1);
                            return true;
                        }
                    } else if (x2 > f5) {
                        if (this.c < count - 1) {
                            if (action == 3) {
                                return true;
                            }
                            this.f1521a.setCurrentItem(this.c + 1);
                            return true;
                        }
                    } else if (!(this.A == null || action == 3)) {
                        this.A.a(this.c);
                    }
                }
                this.z = false;
                this.y = -1;
                if (!this.f1521a.isFakeDragging()) {
                    return true;
                }
                this.f1521a.endFakeDrag();
                return true;
            case 2:
                float x3 = MotionEventCompat.getX(motionEvent, MotionEventCompat.findPointerIndex(motionEvent, this.y));
                float f6 = x3 - this.x;
                if (!this.z && Math.abs(f6) > ((float) this.w)) {
                    this.z = true;
                }
                if (!this.z) {
                    return true;
                }
                this.x = x3;
                if (!this.f1521a.isFakeDragging() && !this.f1521a.beginFakeDrag()) {
                    return true;
                }
                this.f1521a.fakeDragBy(f6);
                return true;
            case 4:
            default:
                return true;
            case 5:
                int actionIndex = MotionEventCompat.getActionIndex(motionEvent);
                this.x = MotionEventCompat.getX(motionEvent, actionIndex);
                this.y = MotionEventCompat.getPointerId(motionEvent, actionIndex);
                return true;
            case 6:
                int actionIndex2 = MotionEventCompat.getActionIndex(motionEvent);
                if (MotionEventCompat.getPointerId(motionEvent, actionIndex2) == this.y) {
                    if (actionIndex2 == 0) {
                        i2 = 1;
                    }
                    this.y = MotionEventCompat.getPointerId(motionEvent, i2);
                }
                this.x = MotionEventCompat.getX(motionEvent, MotionEventCompat.findPointerIndex(motionEvent, this.y));
                return true;
        }
    }

    private void a(Rect rect, float f2, int i2) {
        rect.right = (int) (((float) i2) - this.u);
        rect.left = (int) (((float) rect.right) - f2);
    }

    private void b(Rect rect, float f2, int i2) {
        rect.left = (int) (((float) i2) + this.u);
        rect.right = (int) (this.u + f2);
    }

    private ArrayList<Rect> a(Paint paint) {
        ArrayList<Rect> arrayList = new ArrayList<>();
        int count = this.f1521a.getAdapter().getCount();
        int width = getWidth();
        int i2 = width / 2;
        for (int i3 = 0; i3 < count; i3++) {
            Rect a2 = a(i3, paint);
            int i4 = a2.right - a2.left;
            int i5 = a2.bottom - a2.top;
            a2.left = (int) ((((float) i2) - (((float) i4) / 2.0f)) + ((((float) (i3 - this.c)) - this.d) * ((float) width)));
            a2.right = i4 + a2.left;
            a2.top = 0;
            a2.bottom = i5;
            arrayList.add(a2);
        }
        return arrayList;
    }

    private Rect a(int i2, Paint paint) {
        Rect rect = new Rect();
        CharSequence a2 = a(i2);
        rect.right = (int) paint.measureText(a2, 0, a2.length());
        rect.bottom = (int) (paint.descent() - paint.ascent());
        return rect;
    }

    public void setViewPager(ViewPager viewPager) {
        if (this.f1521a != viewPager) {
            if (this.f1521a != null) {
                this.f1521a.setOnPageChangeListener(null);
            }
            if (viewPager.getAdapter() == null) {
                throw new IllegalStateException("ViewPager does not have adapter instance.");
            }
            this.f1521a = viewPager;
            this.f1521a.setOnPageChangeListener(this);
            invalidate();
        }
    }

    public void setOnCenterItemClickListener(c cVar) {
        this.A = cVar;
    }

    public void setCurrentItem(int i2) {
        if (this.f1521a == null) {
            throw new IllegalStateException("ViewPager has not been bound.");
        }
        this.f1521a.setCurrentItem(i2);
        this.c = i2;
        invalidate();
    }

    public void onPageScrollStateChanged(int i2) {
        this.e = i2;
        if (this.b != null) {
            this.b.onPageScrollStateChanged(i2);
        }
    }

    public void onPageScrolled(int i2, float f2, int i3) {
        this.c = i2;
        this.d = f2;
        invalidate();
        if (this.b != null) {
            this.b.onPageScrolled(i2, f2, i3);
        }
    }

    public void onPageSelected(int i2) {
        if (this.e == 0) {
            this.c = i2;
            invalidate();
        }
        if (this.b != null) {
            this.b.onPageSelected(i2);
        }
    }

    public void setOnPageChangeListener(ViewPager.OnPageChangeListener onPageChangeListener) {
        this.b = onPageChangeListener;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        float f2;
        int size = View.MeasureSpec.getSize(i2);
        if (View.MeasureSpec.getMode(i3) == 1073741824) {
            f2 = (float) View.MeasureSpec.getSize(i3);
        } else {
            this.k.setEmpty();
            this.k.bottom = (int) (this.f.descent() - this.f.ascent());
            f2 = ((float) (this.k.bottom - this.k.top)) + this.v + this.r + this.t;
            if (this.m != a.None) {
                f2 += this.p;
            }
        }
        setMeasuredDimension(size, (int) f2);
    }

    public void onRestoreInstanceState(Parcelable parcelable) {
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        this.c = savedState.f1523a;
        requestLayout();
    }

    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.f1523a = this.c;
        return savedState;
    }

    static class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = new h();

        /* renamed from: a  reason: collision with root package name */
        int f1523a;

        public SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        private SavedState(Parcel parcel) {
            super(parcel);
            this.f1523a = parcel.readInt();
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.f1523a);
        }
    }

    private CharSequence a(int i2) {
        CharSequence pageTitle = this.f1521a.getAdapter().getPageTitle(i2);
        if (pageTitle == null) {
            return "";
        }
        return pageTitle;
    }
}
