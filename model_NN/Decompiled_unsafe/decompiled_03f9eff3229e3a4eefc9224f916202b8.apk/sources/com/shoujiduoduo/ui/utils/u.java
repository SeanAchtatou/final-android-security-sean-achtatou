package com.shoujiduoduo.ui.utils;

import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.util.ak;
import com.shoujiduoduo.util.g;

/* compiled from: HtmlFragment */
class u extends WebViewClient {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ HtmlFragment f1538a;

    u(HtmlFragment htmlFragment) {
        this.f1538a = htmlFragment;
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        PlayerService b;
        a.a("HtmlFragment", "override url:" + str);
        if (g.a(str)) {
            if (str.contains("w2c_open_webview") && (b = ak.a().b()) != null && b.j()) {
                b.k();
            }
            g.a(this.f1538a.getActivity(), str);
            return true;
        }
        webView.loadUrl(str);
        return true;
    }
}
