package com.shoujiduoduo.ui.utils;

import android.content.DialogInterface;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.util.av;
import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.w;
import com.shoujiduoduo.util.widget.a;

/* compiled from: RingListAdapter */
class bf extends a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RingData f1482a;
    final /* synthetic */ y b;

    bf(y yVar, RingData ringData) {
        this.b = yVar;
        this.f1482a = ringData;
    }

    public void a(c.b bVar) {
        super.a(bVar);
        com.shoujiduoduo.base.a.a.a("RingListAdapter", "0元会员订购彩铃成功");
        this.b.a(this.f1482a.A, this.f1482a);
        w.a(this.f1482a.g, 6, this.b.b.a(), this.b.b.b().toString(), "&cucid=" + this.f1482a.A);
        if (!av.c(this.f1482a.C)) {
            com.shoujiduoduo.util.e.a.a().f(this.f1482a.C, new a());
        }
    }

    public void b(c.b bVar) {
        super.b(bVar);
        com.shoujiduoduo.base.a.a.a("RingListAdapter", "0元会员订购彩铃失败， msg:" + bVar.toString());
        if (bVar.a().equals("400033")) {
            com.shoujiduoduo.base.a.a.a("RingListAdapter", "set default");
            this.b.a(this.f1482a.A, this.f1482a);
            w.a(this.f1482a.g, 6, this.b.b.a(), this.b.b.b().toString(), "&cucid=" + this.f1482a.A);
            return;
        }
        this.b.f();
        new a.C0034a(this.b.f).b("设置彩铃").a(bVar.b()).a("确定", (DialogInterface.OnClickListener) null).a().show();
    }
}
