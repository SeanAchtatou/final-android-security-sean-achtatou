package com.shoujiduoduo.ui.utils;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.a.c.f;
import com.shoujiduoduo.a.c.o;
import com.shoujiduoduo.a.c.w;
import com.shoujiduoduo.base.bean.f;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ui.cailing.q;
import com.shoujiduoduo.ui.home.ArtistRingActivity;
import com.shoujiduoduo.ui.home.CollectRingActivity;
import com.shoujiduoduo.ui.mine.changering.aa;
import com.shoujiduoduo.util.u;

public class DDListFragment extends LazyFragment {
    /* access modifiers changed from: private */
    public static final String b = DDListFragment.class.getSimpleName();
    private o A = new q(this);
    private com.shoujiduoduo.a.c.c B = new r(this);
    private f C = new s(this);
    private AbsListView.OnScrollListener D = new j(this);
    /* access modifiers changed from: private */
    public com.shoujiduoduo.base.bean.c c;
    /* access modifiers changed from: private */
    public RelativeLayout d;
    private RelativeLayout e;
    private RelativeLayout f;
    private RelativeLayout g;
    /* access modifiers changed from: private */
    public g h;
    private ListView i;
    private Button j;
    private Button k;
    private View l;
    private View m;
    private boolean n;
    private boolean o;
    private boolean p;
    private boolean q;
    /* access modifiers changed from: private */
    public e r = e.LIST_FAILED;
    private boolean s;
    private boolean t;
    private int u;
    private int v;
    /* access modifiers changed from: private */
    public boolean w;
    /* access modifiers changed from: private */
    public boolean x;
    private View.OnClickListener y = new k(this);
    private w z = new p(this);

    enum c {
        RETRIEVE,
        TOTAL,
        RETRIEVE_FAILED,
        INVISIBLE
    }

    public enum e {
        LIST_CONTENT,
        LIST_LOADING,
        LIST_FAILED
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        com.shoujiduoduo.base.a.a.a(b, "oncrateview");
        View inflate = layoutInflater.inflate((int) R.layout.ring_list_panel, viewGroup, false);
        this.d = (RelativeLayout) inflate.findViewById(R.id.content_view);
        this.e = (RelativeLayout) inflate.findViewById(R.id.failed_view);
        this.f = (RelativeLayout) inflate.findViewById(R.id.cailing_not_open_view);
        this.f.setVisibility(8);
        this.g = (RelativeLayout) inflate.findViewById(R.id.loading_view);
        this.g.setVisibility(0);
        ((RelativeLayout) this.f.findViewById(R.id.open)).setOnClickListener(this.y);
        this.i = (ListView) this.d.findViewById(R.id.list_view);
        this.l = layoutInflater.inflate((int) R.layout.get_more_rings, (ViewGroup) null, false);
        if (this.l != null) {
            this.i.addFooterView(this.l);
            this.l.setVisibility(4);
        }
        this.i.setOnScrollListener(this.D);
        this.e.setOnClickListener(new i(this));
        Bundle arguments = getArguments();
        if (arguments != null) {
            this.n = arguments.getBoolean("support_area", false);
            if (this.n) {
                h();
            }
            this.o = arguments.getBoolean("support_batch", false);
            if (this.o) {
                c();
            }
            this.p = arguments.getBoolean("support_feed_ad", false);
            this.q = arguments.getBoolean("support_lazy_load", false);
            if (!this.q) {
                setUserVisibleHint(true);
            }
            String string = arguments.getString("adapter_type");
            if ("ring_list_adapter".equals(string)) {
                this.h = new y(getActivity());
                this.i.setOnItemClickListener(new d());
            } else if ("cailing_list_adapter".equals(string)) {
                this.h = new q(getActivity());
                this.i.setOnItemClickListener(new d());
            } else if ("system_ring_list_adapter".equals(string)) {
                this.h = new aa(getActivity());
                this.i.setOnItemClickListener(new d());
            } else if ("collect_list_adapter".equals(string)) {
                this.h = new h(getActivity());
                this.i.setOnItemClickListener(new b());
            } else if ("artist_list_adapter".equals(string)) {
                this.h = new c(getActivity());
                this.i.setOnItemClickListener(new a());
            } else {
                com.shoujiduoduo.base.a.a.c(b, "not support adapter type");
            }
        }
        if (this.h != null) {
            if (this.p) {
                this.h.a(true);
            }
            this.h.a();
        }
        this.u = u.a(com.umeng.analytics.b.c(RingDDApp.c(), "feed_ad_gap"), 10);
        this.v = u.a(com.umeng.analytics.b.c(RingDDApp.c(), "feed_ad_start_pos"), 6);
        this.s = true;
        this.t = false;
        if (this.m != null) {
            a(this.m);
        }
        a();
        x.a().a(com.shoujiduoduo.a.a.b.OBSERVER_RING_CHANGE, this.A);
        x.a().a(com.shoujiduoduo.a.a.b.OBSERVER_CAILING, this.B);
        x.a().a(com.shoujiduoduo.a.a.b.OBSERVER_LIST_DATA, this.C);
        x.a().a(com.shoujiduoduo.a.a.b.OBSERVER_VIP, this.z);
        return inflate;
    }

    public void onDestroy() {
        com.shoujiduoduo.base.a.a.a(b, "onDestroy, list id:" + (this.c == null ? "no id" : this.c.a()));
        super.onDestroy();
    }

    public void onDestroyView() {
        super.onDestroyView();
        com.shoujiduoduo.base.a.a.a(b, "onDestroyView, id:" + (this.c == null ? "no id" : this.c.a()));
        if (this.h != null) {
            this.h.b();
        }
        this.s = false;
        x.a().b(com.shoujiduoduo.a.a.b.OBSERVER_RING_CHANGE, this.A);
        x.a().b(com.shoujiduoduo.a.a.b.OBSERVER_CAILING, this.B);
        x.a().b(com.shoujiduoduo.a.a.b.OBSERVER_LIST_DATA, this.C);
        x.a().b(com.shoujiduoduo.a.a.b.OBSERVER_VIP, this.z);
    }

    public void onDetach() {
        super.onDetach();
    }

    /* access modifiers changed from: protected */
    public void a() {
        if (this.s && this.f1452a && this.c != null && this.h != null && !this.t) {
            com.shoujiduoduo.base.a.a.a(b, "lazyLoad, loadListData");
            g();
            this.t = true;
        }
    }

    public void a(View view) {
        if (!this.s) {
            this.m = view;
        } else if (this.m == null || this.i.getHeaderViewsCount() == 0) {
            this.m = view;
            this.i.addHeaderView(this.m);
        }
    }

    public class a implements AdapterView.OnItemClickListener {
        public a() {
        }

        public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            if (DDListFragment.this.c != null && j >= 0) {
                RingDDApp.b().a("artistdata", DDListFragment.this.c.a(i));
                Intent intent = new Intent(DDListFragment.this.getActivity(), ArtistRingActivity.class);
                intent.putExtra("parakey", "artistdata");
                DDListFragment.this.getActivity().startActivity(intent);
            }
        }
    }

    public class b implements AdapterView.OnItemClickListener {
        public b() {
        }

        public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            if (DDListFragment.this.c != null && j >= 0) {
                RingDDApp.b().a("collectdata", DDListFragment.this.c.a(i));
                Intent intent = new Intent(DDListFragment.this.getActivity(), CollectRingActivity.class);
                intent.putExtra("parakey", "collectdata");
                DDListFragment.this.getActivity().startActivity(intent);
            }
        }
    }

    /* access modifiers changed from: private */
    public int a(int i2) {
        if (this.p && i2 + 1 >= this.v) {
            return (((i2 + 1) - this.v) / this.u) + 1;
        }
        return 0;
    }

    public class d implements AdapterView.OnItemClickListener {
        public d() {
        }

        /* JADX WARN: Type inference failed for: r1v0, types: [android.widget.Adapter] */
        /* JADX WARN: Type inference failed for: r1v2, types: [android.widget.Adapter] */
        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public void onItemClick(android.widget.AdapterView<?> r5, android.view.View r6, int r7, long r8) {
            /*
                r4 = this;
                com.shoujiduoduo.ui.utils.DDListFragment r0 = com.shoujiduoduo.ui.utils.DDListFragment.this
                com.shoujiduoduo.base.bean.c r0 = r0.c
                if (r0 == 0) goto L_0x000e
                r0 = 0
                int r0 = (r8 > r0 ? 1 : (r8 == r0 ? 0 : -1))
                if (r0 >= 0) goto L_0x000f
            L_0x000e:
                return
            L_0x000f:
                int r0 = (int) r8
                android.widget.Adapter r1 = r5.getAdapter()
                int r1 = r1.getItemViewType(r7)
                r2 = 1
                if (r1 == r2) goto L_0x000e
                android.widget.Adapter r1 = r5.getAdapter()
                int r1 = r1.getItemViewType(r7)
                if (r1 != 0) goto L_0x0040
                com.shoujiduoduo.util.ak r1 = com.shoujiduoduo.util.ak.a()
                com.shoujiduoduo.player.PlayerService r1 = r1.b()
                if (r1 == 0) goto L_0x000e
                com.shoujiduoduo.ui.utils.DDListFragment r2 = com.shoujiduoduo.ui.utils.DDListFragment.this
                com.shoujiduoduo.base.bean.c r2 = r2.c
                com.shoujiduoduo.ui.utils.DDListFragment r3 = com.shoujiduoduo.ui.utils.DDListFragment.this
                int r3 = r3.a(r0)
                int r0 = r0 - r3
                r1.a(r2, r0)
                goto L_0x000e
            L_0x0040:
                com.shoujiduoduo.util.ak r1 = com.shoujiduoduo.util.ak.a()
                com.shoujiduoduo.player.PlayerService r1 = r1.b()
                if (r1 == 0) goto L_0x000e
                com.shoujiduoduo.ui.utils.DDListFragment r2 = com.shoujiduoduo.ui.utils.DDListFragment.this
                com.shoujiduoduo.base.bean.c r2 = r2.c
                r1.a(r2, r0)
                goto L_0x000e
            */
            throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.ui.utils.DDListFragment.d.onItemClick(android.widget.AdapterView, android.view.View, int, long):void");
        }
    }

    public String b() {
        if (this.c != null) {
            return this.c.a();
        }
        return "";
    }

    public void a(com.shoujiduoduo.base.bean.c cVar) {
        if (cVar == this.c) {
            com.shoujiduoduo.base.a.a.a(b, "same list, just return, list id:" + cVar.a());
            return;
        }
        this.c = null;
        this.c = cVar;
        if (this.s) {
            this.t = false;
            a();
        }
    }

    /* access modifiers changed from: private */
    public void g() {
        this.i.setAdapter((ListAdapter) this.h);
        if (this.c != null) {
            this.h.a(this.c);
            if (this.c.c() == 0) {
                com.shoujiduoduo.base.a.a.a(b, "loadListData: show loading panel, id:" + this.c.a());
                a(e.LIST_LOADING);
                if (!this.c.d()) {
                    this.c.e();
                    return;
                }
                return;
            }
            com.shoujiduoduo.base.a.a.a(b, "setRingList: Show list content, id:" + this.c.a());
            a(e.LIST_CONTENT);
            return;
        }
        this.h.a((com.shoujiduoduo.base.bean.c) null);
        this.h.notifyDataSetChanged();
    }

    private void h() {
        this.k = (Button) this.d.findViewById(R.id.changeArea);
        this.k.setVisibility(0);
        this.k.setOnClickListener(new m(this));
    }

    public void c() {
        this.j = (Button) this.d.findViewById(R.id.changeBatch);
        this.j.setVisibility(0);
        this.j.setOnClickListener(new n(this));
    }

    /* access modifiers changed from: private */
    public boolean i() {
        if (this.c == null) {
            return false;
        }
        if (this.c.b().equals(f.a.list_ring_cmcc) || this.c.b().equals(f.a.list_ring_cucc) || this.c.b().equals(f.a.list_ring_ctcc)) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: private */
    public void a(c cVar) {
        String str;
        if (this.l != null) {
            ProgressBar progressBar = (ProgressBar) this.l.findViewById(R.id.circleProgressBar);
            TextView textView = (TextView) this.l.findViewById(R.id.get_more_text);
            switch (cVar) {
                case RETRIEVE:
                    progressBar.setVisibility(0);
                    textView.setText((int) R.string.ringlist_retrieving);
                    this.l.setVisibility(0);
                    return;
                case TOTAL:
                    progressBar.setVisibility(4);
                    if (this.i.getCount() > (this.i.getHeaderViewsCount() > 0 ? 2 : 1)) {
                        String string = RingDDApp.c().getResources().getString(R.string.total);
                        if (this.c.b().equals(f.a.list_artist)) {
                            str = "个歌手";
                        } else if (this.c.b().equals(f.a.list_collect)) {
                            str = "个精选集";
                        } else {
                            str = "首铃声";
                        }
                        int count = this.i.getCount();
                        if (this.i.getHeaderViewsCount() > 0) {
                            count -= this.i.getHeaderViewsCount();
                        }
                        textView.setText(string + (count - 1) + str);
                    }
                    this.l.setVisibility(0);
                    return;
                case RETRIEVE_FAILED:
                    progressBar.setVisibility(4);
                    textView.setText((int) R.string.ringlist_retrieve_error);
                    this.l.setVisibility(0);
                    return;
                case INVISIBLE:
                    this.l.setVisibility(4);
                    return;
                default:
                    return;
            }
        }
    }

    public void a(e eVar) {
        this.d.setVisibility(4);
        this.e.setVisibility(4);
        this.g.setVisibility(4);
        switch (eVar) {
            case LIST_CONTENT:
                this.i.post(new t(this));
                break;
            case LIST_LOADING:
                this.g.setVisibility(0);
                break;
            case LIST_FAILED:
                if (!this.w) {
                    this.e.setVisibility(0);
                    break;
                } else {
                    this.f.setVisibility(0);
                    TextView textView = (TextView) this.f.findViewById(R.id.hint);
                    TextView textView2 = (TextView) this.f.findViewById(R.id.open_tips);
                    TextView textView3 = (TextView) this.f.findViewById(R.id.cost_hint);
                    if (!this.x) {
                        textView.setText("尊敬的移动用户,\n您目前尚未开通彩铃功能,无法使用彩铃。");
                        textView2.setText("立即开通");
                        textView3.setVisibility(0);
                        break;
                    } else {
                        textView.setText("正在为您开通彩铃业务，\n请稍候点击“查询彩铃”获取当前彩铃");
                        textView2.setText("查询彩铃");
                        textView3.setVisibility(4);
                        break;
                    }
                }
        }
        this.r = eVar;
    }
}
