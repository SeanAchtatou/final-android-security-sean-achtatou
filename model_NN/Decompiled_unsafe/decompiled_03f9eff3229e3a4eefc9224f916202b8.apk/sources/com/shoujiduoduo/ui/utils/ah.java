package com.shoujiduoduo.ui.utils;

import android.content.DialogInterface;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.f;
import com.shoujiduoduo.util.widget.a;

/* compiled from: RingListAdapter */
class ah extends a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RingData f1462a;
    final /* synthetic */ String b;
    final /* synthetic */ boolean c;
    final /* synthetic */ y d;

    ah(y yVar, RingData ringData, String str, boolean z) {
        this.d = yVar;
        this.f1462a = ringData;
        this.b = str;
        this.c = z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, boolean, com.shoujiduoduo.util.f$b):void
     arg types: [com.shoujiduoduo.ui.utils.y, int, com.shoujiduoduo.util.f$b]
     candidates:
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.f$b):android.view.View
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.f$b):void
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.base.bean.RingData, java.lang.String, boolean):void
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.base.bean.RingData, boolean, boolean):void
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, java.lang.String):void
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, boolean):void
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, java.lang.String, com.shoujiduoduo.base.bean.RingData):void
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, boolean, com.shoujiduoduo.util.f$b):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.f$b, boolean):void
     arg types: [com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.f$b, int]
     candidates:
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.f$b, java.lang.String, boolean):void
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, boolean, java.lang.String, java.lang.String, com.shoujiduoduo.base.bean.RingData):void
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.f$b, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.f$b, java.lang.String, boolean):void
     arg types: [com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.f$b, java.lang.String, int]
     candidates:
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.f$b, boolean):void
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, boolean, java.lang.String, java.lang.String, com.shoujiduoduo.base.bean.RingData):void
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.f$b, java.lang.String, boolean):void */
    public void a(c.b bVar) {
        super.a(bVar);
        if (bVar == null || !(bVar instanceof c.e)) {
            this.d.f();
            new a.C0034a(this.d.f).b("设置彩铃").a(bVar != null ? bVar.b() : "设置失败").a("确定", (DialogInterface.OnClickListener) null).a().show();
            com.shoujiduoduo.base.a.a.c("RingListAdapter", "checkCailingAndVip failed");
            return;
        }
        c.e eVar = (c.e) bVar;
        if (eVar.d() && eVar.e()) {
            this.d.f();
            com.shoujiduoduo.base.a.a.a("RingListAdapter", "彩铃与会员均开通，直接订购");
            this.d.a(true, f.b.cu);
            new a.C0034a(this.d.f).b("设置彩铃(免费)").a(this.d.a(this.f1462a, f.b.cu)).a("确定", new ai(this)).b("取消", (DialogInterface.OnClickListener) null).a().show();
        } else if (eVar.d() && !eVar.e()) {
            com.shoujiduoduo.base.a.a.a("RingListAdapter", "彩铃开通，会员关闭，提示开通会员");
            this.d.f();
            if (this.c) {
                this.d.a(this.f1462a, this.b, f.b.cu, false);
            } else {
                new a.C0034a(this.d.f).b("设置彩铃").a("抱歉，您所在的当前区域不支持铃声多多的彩铃功能！").a("确定", (DialogInterface.OnClickListener) null).a().show();
            }
        } else if (!eVar.d() && eVar.e()) {
            com.shoujiduoduo.base.a.a.a("RingListAdapter", "彩铃关闭，会员开通");
            this.d.a(true, f.b.cu);
            if (eVar.i()) {
                com.shoujiduoduo.base.a.a.a("RingListAdapter", "属于免彩铃功能费范围，先 帮用户自动开通彩铃功能， net type:" + eVar.g() + ", location:" + eVar.f());
                com.shoujiduoduo.util.e.a.a().e("&phone=" + this.b, new aj(this));
            } else {
                com.shoujiduoduo.base.a.a.a("RingListAdapter", "不属于免彩铃功能费范围， 提示开通彩铃");
                this.d.f();
                this.d.a(this.f1462a, f.b.cu, this.b, true);
            }
        } else if (!eVar.d() && !eVar.e()) {
            com.shoujiduoduo.base.a.a.a("RingListAdapter", "彩铃会员均关闭");
            if (!this.c) {
                new a.C0034a(this.d.f).b("设置彩铃").a("抱歉，您所在的当前区域不支持铃声多多的彩铃功能！").a("确定", (DialogInterface.OnClickListener) null).a().show();
                return;
            } else if (eVar.i()) {
                com.shoujiduoduo.base.a.a.a("RingListAdapter", "属于免彩铃功能费范围， 帮用户自动开通彩铃，net type:" + eVar.g() + ", location:" + eVar.f());
                com.shoujiduoduo.util.e.a.a().e("&phone=" + this.b, new am(this));
            } else {
                com.shoujiduoduo.base.a.a.a("RingListAdapter", "不属于免彩铃功能费范围， 提示开通会员");
                this.d.f();
                this.d.a(this.f1462a, f.b.cu, this.b, false);
            }
        }
        if (eVar.f1598a.a().equals("40307") || eVar.f1598a.a().equals("40308")) {
            com.shoujiduoduo.util.e.a.a().a(this.b, "");
            this.d.f();
            this.d.b(this.f1462a, this.b, f.b.cu);
        }
    }

    public void b(c.b bVar) {
        super.b(bVar);
    }
}
