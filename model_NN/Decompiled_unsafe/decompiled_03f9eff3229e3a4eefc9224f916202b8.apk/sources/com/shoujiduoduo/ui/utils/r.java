package com.shoujiduoduo.ui.utils;

import com.shoujiduoduo.a.c.c;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.ui.utils.DDListFragment;
import com.shoujiduoduo.util.f;

/* compiled from: DDListFragment */
class r implements c {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DDListFragment f1535a;

    r(DDListFragment dDListFragment) {
        this.f1535a = dDListFragment;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.ui.utils.DDListFragment.a(com.shoujiduoduo.ui.utils.DDListFragment, boolean):boolean
     arg types: [com.shoujiduoduo.ui.utils.DDListFragment, int]
     candidates:
      com.shoujiduoduo.ui.utils.DDListFragment.a(com.shoujiduoduo.ui.utils.DDListFragment, int):int
      com.shoujiduoduo.ui.utils.DDListFragment.a(com.shoujiduoduo.ui.utils.DDListFragment, com.shoujiduoduo.base.bean.c):com.shoujiduoduo.base.bean.c
      com.shoujiduoduo.ui.utils.DDListFragment.a(com.shoujiduoduo.ui.utils.DDListFragment, com.shoujiduoduo.ui.utils.DDListFragment$c):void
      com.shoujiduoduo.ui.utils.DDListFragment.a(com.shoujiduoduo.ui.utils.DDListFragment, boolean):boolean */
    public void a(boolean z, f.b bVar) {
        if (this.f1535a.c != null && this.f1535a.c.a().equals("cmcc_cailing")) {
            a.a(DDListFragment.b, "on Cailing Status change:" + z);
            if (z) {
                a.a(DDListFragment.b, "cailing is wait to open");
                boolean unused = this.f1535a.w = false;
                boolean unused2 = this.f1535a.x = true;
                this.f1535a.a(DDListFragment.e.LIST_LOADING);
                this.f1535a.c.e();
                return;
            }
            boolean unused3 = this.f1535a.w = true;
            this.f1535a.a(DDListFragment.e.LIST_FAILED);
        }
    }

    public void a(f.b bVar) {
        if (this.f1535a.c == null) {
            return;
        }
        if (this.f1535a.c.a().equals("cmcc_cailing") || this.f1535a.c.a().equals("ctcc_cailing") || this.f1535a.c.a().equals("cucc_cailing")) {
            a.a(DDListFragment.b, "onOrderCailing");
            this.f1535a.a(DDListFragment.e.LIST_LOADING);
            this.f1535a.c.f();
        }
    }

    public void b(f.b bVar) {
        if (this.f1535a.c == null) {
            return;
        }
        if (this.f1535a.c.a().equals("cmcc_cailing") || this.f1535a.c.a().equals("ctcc_cailing") || this.f1535a.c.a().equals("cucc_cailing")) {
            a.a(DDListFragment.b, "onDeleteCailing");
            this.f1535a.a(DDListFragment.e.LIST_LOADING);
            this.f1535a.c.f();
        }
    }
}
