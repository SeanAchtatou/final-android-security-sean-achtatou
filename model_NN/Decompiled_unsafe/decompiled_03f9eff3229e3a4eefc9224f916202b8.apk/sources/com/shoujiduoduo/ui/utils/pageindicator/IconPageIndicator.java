package com.shoujiduoduo.ui.utils.pageindicator;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import com.shoujiduoduo.ringtone.R;

public class IconPageIndicator extends HorizontalScrollView implements PageIndicator {

    /* renamed from: a  reason: collision with root package name */
    private final d f1516a;
    private ViewPager b;
    private ViewPager.OnPageChangeListener c;
    /* access modifiers changed from: private */
    public Runnable d;
    private int e;

    public IconPageIndicator(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setHorizontalScrollBarEnabled(false);
        this.f1516a = new d(context, R.attr.vpiIconPageIndicatorStyle);
        addView(this.f1516a, new FrameLayout.LayoutParams(-2, -1, 17));
    }

    private void a(int i) {
        View childAt = this.f1516a.getChildAt(i);
        if (this.d != null) {
            removeCallbacks(this.d);
        }
        this.d = new b(this, childAt);
        post(this.d);
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.d != null) {
            post(this.d);
        }
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.d != null) {
            removeCallbacks(this.d);
        }
    }

    public void onPageScrollStateChanged(int i) {
        if (this.c != null) {
            this.c.onPageScrollStateChanged(i);
        }
    }

    public void onPageScrolled(int i, float f, int i2) {
        if (this.c != null) {
            this.c.onPageScrolled(i, f, i2);
        }
    }

    public void onPageSelected(int i) {
        setCurrentItem(i);
        if (this.c != null) {
            this.c.onPageSelected(i);
        }
    }

    public void setViewPager(ViewPager viewPager) {
        if (this.b != viewPager) {
            if (this.b != null) {
                this.b.setOnPageChangeListener(null);
            }
            if (viewPager.getAdapter() == null) {
                throw new IllegalStateException("ViewPager does not have adapter instance.");
            }
            this.b = viewPager;
            viewPager.setOnPageChangeListener(this);
            a();
        }
    }

    public void a() {
        this.f1516a.removeAllViews();
        c cVar = (c) this.b.getAdapter();
        int a2 = cVar.a();
        for (int i = 0; i < a2; i++) {
            ImageView imageView = new ImageView(getContext(), null, R.attr.vpiIconPageIndicatorStyle);
            imageView.setImageResource(cVar.a(i));
            this.f1516a.addView(imageView);
        }
        if (this.e > a2) {
            this.e = a2 - 1;
        }
        setCurrentItem(this.e);
        requestLayout();
    }

    public void setCurrentItem(int i) {
        boolean z;
        if (this.b == null) {
            throw new IllegalStateException("ViewPager has not been bound.");
        }
        this.e = i;
        this.b.setCurrentItem(i);
        int childCount = this.f1516a.getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = this.f1516a.getChildAt(i2);
            if (i2 == i) {
                z = true;
            } else {
                z = false;
            }
            childAt.setSelected(z);
            if (z) {
                a(i);
            }
        }
    }

    public void setOnPageChangeListener(ViewPager.OnPageChangeListener onPageChangeListener) {
        this.c = onPageChangeListener;
    }
}
