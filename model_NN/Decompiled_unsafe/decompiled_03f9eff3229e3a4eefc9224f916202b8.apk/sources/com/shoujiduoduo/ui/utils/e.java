package com.shoujiduoduo.ui.utils;

import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import com.shoujiduoduo.ui.utils.d;

/* compiled from: AsyncImageLoader */
final class e extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ d.a f1503a;
    final /* synthetic */ String b;

    e(d.a aVar, String str) {
        this.f1503a = aVar;
        this.b = str;
    }

    public void handleMessage(Message message) {
        this.f1503a.a((Drawable) message.obj, this.b);
    }
}
