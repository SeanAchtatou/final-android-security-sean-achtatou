package com.shoujiduoduo.ui.utils;

import android.app.AlertDialog;
import android.content.DialogInterface;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;
import com.umeng.analytics.b;

/* compiled from: RingListAdapter */
class bb extends a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RingData f1480a;
    final /* synthetic */ y b;

    bb(y yVar, RingData ringData) {
        this.b = yVar;
        this.f1480a = ringData;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, boolean, boolean):void
     arg types: [com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, int, int]
     candidates:
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.f$b, java.lang.String, boolean):void
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.f$b, boolean):void
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.f$b):void
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, java.lang.String, boolean):void
      com.shoujiduoduo.ui.utils.y.a(boolean, java.lang.String, java.lang.String, com.shoujiduoduo.base.bean.RingData):void
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, boolean, boolean):void */
    public void a(c.b bVar) {
        super.a(bVar);
        com.shoujiduoduo.base.a.a.a("RingListAdapter", "cmcc vip order success");
        this.b.a(this.f1480a, true, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, boolean, boolean):void
     arg types: [com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, int, int]
     candidates:
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.f$b, java.lang.String, boolean):void
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.f$b, boolean):void
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.f$b):void
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, java.lang.String, boolean):void
      com.shoujiduoduo.ui.utils.y.a(boolean, java.lang.String, java.lang.String, com.shoujiduoduo.base.bean.RingData):void
      com.shoujiduoduo.ui.utils.y.a(com.shoujiduoduo.ui.utils.y, com.shoujiduoduo.base.bean.RingData, boolean, boolean):void */
    public void b(c.b bVar) {
        super.b(bVar);
        this.b.f();
        if (bVar.a().equals("100002")) {
            this.b.a(this.f1480a, false, true);
        } else if (bVar.a().equals("302011")) {
            this.b.a(this.f1480a, false, false);
        } else {
            String b2 = bVar.b();
            if (bVar.a().equals("303023")) {
                b2 = "中国移动提醒您，您的彩铃库已达到上限，建议您删除部分不用的彩铃后，再重新设置";
            }
            try {
                new AlertDialog.Builder(this.b.f).setTitle("订购彩铃").setMessage(b2).setPositiveButton("确认", (DialogInterface.OnClickListener) null).show();
            } catch (Exception e) {
                b.a(this.b.f, "AlertDialog Failed!");
            }
        }
        com.shoujiduoduo.base.a.a.a("RingListAdapter", "cmcc vip order fail, " + bVar.toString());
    }
}
