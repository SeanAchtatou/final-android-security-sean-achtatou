package com.shoujiduoduo.ui.utils;

import android.view.View;
import com.shoujiduoduo.a.b.b;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ui.settings.u;

/* compiled from: RingListAdapter */
class bs implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ y f1491a;

    bs(y yVar) {
        this.f1491a = yVar;
    }

    public void onClick(View view) {
        a.a("RingListAdapter", "RingtoneDuoduo: CategoryScene: click apply button!");
        RingData b = this.f1491a.b.a(this.f1491a.c);
        if (b != null) {
            b.b().a(b, "favorite_ring_list");
            new u(this.f1491a.f, R.style.DuoDuoDialog, b, this.f1491a.b.a(), this.f1491a.b.b().toString()).show();
        }
    }
}
