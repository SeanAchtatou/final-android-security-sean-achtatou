package com.shoujiduoduo.ui.utils;

import android.content.DialogInterface;
import com.shoujiduoduo.a.a.b;
import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.util.as;
import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.widget.a;

/* compiled from: RingListAdapter */
class bi extends a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RingData f1484a;
    final /* synthetic */ y b;

    bi(y yVar, RingData ringData) {
        this.b = yVar;
        this.f1484a = ringData;
    }

    public void a(c.b bVar) {
        this.b.f();
        new a.C0034a(this.b.f).b("设置彩铃").a("已成功设置为您的当前彩铃.赶快试试吧！").a("确定", (DialogInterface.OnClickListener) null).a().show();
        as.c(this.b.f, "DEFAULT_CAILING_ID", this.f1484a.s);
        x.a().b(b.OBSERVER_RING_CHANGE, new bj(this));
        super.a(bVar);
    }

    public void b(c.b bVar) {
        this.b.f();
        com.shoujiduoduo.base.a.a.a("RingListAdapter", "setDefaultCtccCailing, onFailure, " + bVar.toString());
        new a.C0034a(this.b.f).b("设置彩铃").a("设置未成功, 原因:" + bVar.b()).a("确定", (DialogInterface.OnClickListener) null).a().show();
        super.b(bVar);
    }
}
