package com.shoujiduoduo.ui.utils;

import android.content.DialogInterface;
import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.f;
import com.shoujiduoduo.util.widget.a;

/* compiled from: RingListAdapter */
class aj extends a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ah f1464a;

    aj(ah ahVar) {
        this.f1464a = ahVar;
    }

    public void a(c.b bVar) {
        super.a(bVar);
        this.f1464a.d.f();
        com.shoujiduoduo.base.a.a.a("RingListAdapter", "成功开通彩铃基础功能");
        new a.C0034a(this.f1464a.d.f).b("设置彩铃(免费)").a(this.f1464a.d.a(this.f1464a.f1462a, f.b.cu)).a("确定", new ak(this)).b("取消", (DialogInterface.OnClickListener) null).a().show();
    }

    public void b(c.b bVar) {
        super.b(bVar);
        this.f1464a.d.f();
        if (bVar.a().equals("000001") || bVar.a().equals("301000")) {
            new a.C0034a(this.f1464a.d.f).b("设置彩铃(免费)").a(this.f1464a.d.a(this.f1464a.f1462a, f.b.cu)).a("确定", new al(this)).b("取消", (DialogInterface.OnClickListener) null).a().show();
        } else {
            new a.C0034a(this.f1464a.d.f).b("设置彩铃").a("为您免费开通彩铃功能失败， 原因：" + bVar.b()).a("确定", (DialogInterface.OnClickListener) null).a().show();
        }
    }
}
