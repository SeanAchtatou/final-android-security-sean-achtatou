package com.shoujiduoduo.ui.utils.pageindicator;

/* compiled from: UnderlinePageIndicator */
class i implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ UnderlinePageIndicator f1532a;

    i(UnderlinePageIndicator underlinePageIndicator) {
        this.f1532a = underlinePageIndicator;
    }

    public void run() {
        if (this.f1532a.b) {
            int max = Math.max(this.f1532a.f1526a.getAlpha() - this.f1532a.e, 0);
            this.f1532a.f1526a.setAlpha(max);
            this.f1532a.invalidate();
            if (max > 0) {
                this.f1532a.postDelayed(this, 30);
            }
        }
    }
}
