package com.shoujiduoduo.ui.utils;

import android.content.DialogInterface;
import com.shoujiduoduo.a.a.b;
import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.util.as;
import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.widget.a;
import com.shoujiduoduo.util.widget.f;

/* compiled from: RingListAdapter */
class bg extends a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RingData f1483a;
    final /* synthetic */ String b;
    final /* synthetic */ boolean c;
    final /* synthetic */ y d;

    bg(y yVar, RingData ringData, String str, boolean z) {
        this.d = yVar;
        this.f1483a = ringData;
        this.b = str;
        this.c = z;
    }

    public void a(c.b bVar) {
        super.a(bVar);
        this.d.d(this.f1483a, this.b);
        com.shoujiduoduo.base.a.a.a("RingListAdapter", "vipOrder onSuccess:" + bVar.toString());
        as.b(this.d.f, "NeedUpdateCaiLingLib", 1);
        x.a().b(b.OBSERVER_CAILING, new bh(this));
    }

    public void b(c.b bVar) {
        this.d.f();
        com.shoujiduoduo.base.a.a.a("RingListAdapter", "vipOrder onFailure:" + bVar.toString());
        if (bVar.a().equals("0536") || bVar.a().equals("0538") || bVar.a().equals("0531")) {
            this.d.d(this.f1483a, this.b);
        } else if (bVar.a().equals("0703")) {
            this.d.c(this.f1483a, this.b);
        } else if ((bVar.a().equals("0574") || bVar.a().equals("0015") || bVar.a().equals("9001")) && this.c) {
            f.a("正在为您开通会员业务，请稍等一会儿... ", 1);
        } else if (bVar.a().equals("0556")) {
            new a.C0034a(this.d.f).b("设置彩铃").a("铃音数量已经达到最大限制, 请到我的彩铃里删除部分彩铃后再购买").a("确定", (DialogInterface.OnClickListener) null).a().show();
        } else {
            new a.C0034a(this.d.f).b("设置彩铃").a(bVar.b()).a("确定", (DialogInterface.OnClickListener) null).a().show();
        }
        super.b(bVar);
    }
}
