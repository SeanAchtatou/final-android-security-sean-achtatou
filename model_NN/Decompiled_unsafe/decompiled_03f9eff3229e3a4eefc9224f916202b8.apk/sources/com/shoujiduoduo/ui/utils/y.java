package com.shoujiduoduo.ui.utils;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Handler;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import com.d.a.b.d;
import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.a.c.j;
import com.shoujiduoduo.b.a.i;
import com.shoujiduoduo.b.c.n;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.base.bean.ab;
import com.shoujiduoduo.base.bean.c;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ui.cailing.au;
import com.shoujiduoduo.ui.cailing.cb;
import com.shoujiduoduo.ui.cailing.ck;
import com.shoujiduoduo.util.ak;
import com.shoujiduoduo.util.av;
import com.shoujiduoduo.util.d.b;
import com.shoujiduoduo.util.e.a;
import com.shoujiduoduo.util.f;
import com.shoujiduoduo.util.u;
import com.umeng.analytics.b;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/* compiled from: RingListAdapter */
public class y extends g {

    /* renamed from: a  reason: collision with root package name */
    Html.ImageGetter f1541a = new bl(this);
    /* access modifiers changed from: private */
    public n b;
    /* access modifiers changed from: private */
    public int c = -1;
    private LayoutInflater d;
    /* access modifiers changed from: private */
    public boolean e;
    /* access modifiers changed from: private */
    public Context f;
    private boolean g;
    private int h;
    private int i;
    /* access modifiers changed from: private */
    public boolean j;
    private Map<Integer, i.a> k;
    /* access modifiers changed from: private */
    public RingData l;
    private j m = new z(this);
    private com.shoujiduoduo.a.c.n n = new au(this);
    private View.OnClickListener o = new bw(this);
    private View.OnClickListener p = new bx(this);
    private View.OnClickListener q = new by(this);
    private View.OnClickListener r = new bz(this);
    /* access modifiers changed from: private */
    public Handler s = new aa(this);
    private ProgressDialog t = null;
    private ProgressDialog u = null;
    private View.OnClickListener v = new bq(this);
    private View.OnClickListener w = new br(this);
    private View.OnClickListener x = new bs(this);
    private View.OnClickListener y = new bt(this);

    public y(Context context) {
        this.f = context;
        this.d = LayoutInflater.from(this.f);
        this.k = new HashMap();
    }

    public void a() {
        this.h = u.a(b.c(RingDDApp.c(), "feed_ad_gap"), 10);
        this.i = u.a(b.c(RingDDApp.c(), "feed_ad_start_pos"), 6);
        x.a().a(com.shoujiduoduo.a.a.b.OBSERVER_PLAY_STATUS, this.n);
        if (this.g) {
            x.a().a(com.shoujiduoduo.a.a.b.OBSERVER_FEED_AD, this.m);
        }
    }

    public void b() {
        x.a().b(com.shoujiduoduo.a.a.b.OBSERVER_PLAY_STATUS, this.n);
        if (this.g) {
            x.a().b(com.shoujiduoduo.a.a.b.OBSERVER_FEED_AD, this.m);
        }
    }

    public void a(boolean z) {
        this.g = z;
    }

    public void a(c cVar) {
        if (this.b != cVar) {
            this.b = null;
            this.b = (n) cVar;
            this.e = false;
            this.j = false;
            notifyDataSetChanged();
        }
    }

    public int getItemViewType(int i2) {
        if (!this.g || i2 + 1 < this.i || ((i2 + 1) - this.i) % this.h != 0) {
            return 0;
        }
        return 1;
    }

    public int getViewTypeCount() {
        if (this.g) {
            return 2;
        }
        return 1;
    }

    public int getCount() {
        if (this.b == null) {
            return 0;
        }
        if (!this.g) {
            return this.b.c();
        }
        if (this.b.c() > this.i) {
            return this.b.c() + ((this.b.c() - this.i) / (this.h - 1));
        }
        return this.b.c();
    }

    public Object getItem(int i2) {
        if (this.b == null || i2 < 0 || i2 >= this.b.c()) {
            return null;
        }
        return this.b.a(i2);
    }

    public long getItemId(int i2) {
        return (long) i2;
    }

    private boolean a(RingData ringData) {
        if (!ringData.n.equals("") && f.v()) {
            return true;
        }
        if (f.x()) {
            b.c b2 = com.shoujiduoduo.util.d.b.a().b(com.shoujiduoduo.a.b.b.g().c().k());
            boolean z = b2 == null || !b2.f1654a || b2.b;
            if (!ringData.s.equals("")) {
                return true;
            }
            if (ringData.w != 2 || !z) {
                return false;
            }
            return true;
        } else if (!f.w() || !a.a().b()) {
            return false;
        } else {
            if (ringData.y != 2) {
                return false;
            }
            return true;
        }
    }

    private void a(View view, int i2) {
        String str;
        int i3 = 4;
        RingData b2 = this.b.a(i2);
        TextView textView = (TextView) cc.a(view, R.id.item_song_name);
        TextView textView2 = (TextView) cc.a(view, R.id.item_artist);
        TextView textView3 = (TextView) cc.a(view, R.id.item_duration);
        TextView textView4 = (TextView) cc.a(view, R.id.item_playcnt);
        String str2 = b2.e;
        if (a(b2)) {
            str2 = str2 + " " + "<img src=\"" + ((int) R.drawable.icon_cailing_tips) + "\" align='center'/>";
        }
        if (b2.E != 0) {
            str2 = str2 + " " + "<img src=\"" + ((int) R.drawable.icon_weixu_tips) + "\" align='center'/>";
        }
        if (b2.l != 0) {
            str2 = str2 + " " + "<img src=\"" + ((int) R.drawable.icon_new) + "\" align='center'/>";
        }
        textView.setText(Html.fromHtml(str2, this.f1541a, null));
        textView2.setText(b2.f);
        textView3.setText(String.format("%02d:%02d", Integer.valueOf(b2.j / 60), Integer.valueOf(b2.j % 60)));
        textView3.setVisibility(b2.j == 0 ? 4 : 0);
        if (b2.k > 100000000) {
            str = "播放: " + String.format("%.1f", Double.valueOf(((double) b2.k) / 1.0E8d)) + "亿";
        } else if (b2.k > 10000) {
            str = ("播放: " + (b2.k / 10000)) + "万";
        } else {
            str = "播放: " + b2.k;
        }
        textView4.setText(str);
        if (b2.k != 0) {
            i3 = 0;
        }
        textView4.setVisibility(i3);
    }

    /* access modifiers changed from: private */
    public void b(RingData ringData) {
        e();
        com.shoujiduoduo.util.c.b.a().a(new ca(this, ringData));
    }

    /* access modifiers changed from: private */
    public void c(RingData ringData) {
        com.shoujiduoduo.util.c.b.a().e(new ac(this, ringData));
    }

    /* access modifiers changed from: private */
    public void d(RingData ringData) {
        ab c2 = com.shoujiduoduo.a.b.b.g().c();
        if (TextUtils.isEmpty(c2.k())) {
            b(ringData, "", f.b.cu);
            return;
        }
        String k2 = c2.k();
        if (a.a().a(k2)) {
            a(ringData, k2, f.b.cu);
        } else {
            b(ringData, k2, f.b.cu);
        }
    }

    /* access modifiers changed from: private */
    public void e(RingData ringData) {
        ab c2 = com.shoujiduoduo.a.b.b.g().c();
        if (TextUtils.isEmpty(c2.k()) || !c2.i()) {
            String i2 = f.i();
            if (!TextUtils.isEmpty(i2)) {
                a("请稍候...");
                com.shoujiduoduo.base.a.a.a("fuck", "RingListAdapter 调用 findMdnByImsi");
                com.shoujiduoduo.util.d.b.a().b(i2, new ae(this, c2, ringData));
                return;
            }
            b(ringData, "", f.b.ct);
            return;
        }
        a(ringData, c2.k(), f.b.ct);
    }

    /* access modifiers changed from: private */
    public void a(RingData ringData, String str, f.b bVar) {
        if (bVar.equals(f.b.cu)) {
            a(ringData, str);
        } else if (bVar.equals(f.b.ct)) {
            b(ringData, str);
        } else if (bVar.equals(f.b.f1671a)) {
            c(ringData);
        }
    }

    private void a(RingData ringData, String str) {
        a("请稍候...");
        a.a().i(str, new ag(this, ringData, str));
    }

    /* access modifiers changed from: private */
    public void a(RingData ringData, String str, boolean z) {
        a.a().f(new ah(this, ringData, str, z));
    }

    private void b(RingData ringData, String str) {
        a("请稍候...");
        com.shoujiduoduo.util.d.b.a().a(str, new an(this, ringData, str));
    }

    /* access modifiers changed from: private */
    public void c(RingData ringData, String str) {
        com.shoujiduoduo.util.d.b.a().g(str, new ap(this, str, ringData));
    }

    /* access modifiers changed from: private */
    public void a(boolean z, f.b bVar) {
        int i2;
        int i3 = 0;
        ab c2 = com.shoujiduoduo.a.b.b.g().c();
        if (bVar.equals(f.b.cu)) {
            i2 = 3;
        } else if (bVar.equals(f.b.ct)) {
            i2 = 2;
        } else if (bVar.equals(f.b.f1671a)) {
            i2 = 1;
        } else {
            i2 = 0;
        }
        if (z) {
            i3 = i2;
        }
        c2.b(i3);
        if (!c2.i()) {
            if (bVar != f.b.f1671a || !av.c(c2.k())) {
                c2.b(c2.k());
                c2.a("phone_" + c2.k());
            } else {
                c2.b("多多VIP");
                c2.a("phone_" + com.shoujiduoduo.util.c.b.a().b());
            }
            c2.c(1);
            com.shoujiduoduo.a.b.b.g().a(c2);
            x.a().a(com.shoujiduoduo.a.a.b.OBSERVER_USER_CENTER, new aq(this));
        } else {
            com.shoujiduoduo.a.b.b.g().a(c2);
        }
        x.a().a(com.shoujiduoduo.a.a.b.OBSERVER_VIP, new ar(this, i2));
    }

    /* access modifiers changed from: private */
    public void b(RingData ringData, String str, f.b bVar) {
        new cb(this.f, R.style.DuoDuoDialog, str, bVar, new as(this, ringData, bVar)).show();
    }

    /* access modifiers changed from: private */
    public void a(RingData ringData, String str, f.b bVar, boolean z) {
        new au(this.f, bVar, ringData, "ringlist", false, z, new av(this, ringData, bVar, str)).show();
    }

    /* access modifiers changed from: private */
    public void a(RingData ringData, f.b bVar, String str, boolean z) {
        new ck(this.f, R.style.DuoDuoDialog, bVar, new ax(this, z, ringData, bVar, str)).show();
    }

    private void b(RingData ringData, String str, boolean z) {
        StringBuilder sb = new StringBuilder();
        sb.append("&rid=").append(ringData.g).append("&from=").append(this.b.a()).append("&phone=").append(str);
        com.shoujiduoduo.util.d.b.a().a(ringData.e, str, ringData.x, sb.toString() + ("&info=" + av.a("ringname:" + ringData.e)), new az(this, sb, ringData, str));
    }

    /* access modifiers changed from: private */
    public void g() {
        com.shoujiduoduo.ui.cailing.a aVar = new com.shoujiduoduo.ui.cailing.a(this.f, R.style.DuoDuoDialog, f.b.f1671a);
        aVar.a(this.l, this.b.a(), this.b.b());
        aVar.show();
    }

    /* access modifiers changed from: private */
    public void a(RingData ringData, boolean z) {
        StringBuilder sb = new StringBuilder();
        sb.append("&rid=").append(ringData.g).append("&from=").append(this.b.a());
        com.shoujiduoduo.util.c.b.a().d(ringData.n, sb.toString(), new bb(this, ringData));
    }

    /* access modifiers changed from: private */
    public void a(RingData ringData, boolean z, boolean z2) {
        com.shoujiduoduo.util.c.b.a().f(ringData.n, new bc(this, ringData, z, z2));
    }

    /* access modifiers changed from: private */
    public void c(RingData ringData, String str, boolean z) {
        StringBuilder sb = new StringBuilder();
        sb.append("&rid=").append(ringData.g).append("&from=").append(this.b.a()).append("&cucid=").append(ringData.A).append("&phone=").append(str).append("&info=").append(av.a("ringname:" + ringData.e));
        a.a().b(ringData.A, sb.toString(), new bf(this, ringData));
    }

    /* access modifiers changed from: private */
    public void d(RingData ringData, String str, boolean z) {
        if (ringData.w == 2) {
            b(ringData, str, z);
            return;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("&ctcid=").append(ringData.s).append("&from=").append(this.b.a()).append("&phone=").append(str);
        com.shoujiduoduo.util.d.b.a().a(str, ringData.s, sb.toString(), new bg(this, ringData, str, z));
    }

    /* access modifiers changed from: private */
    public void d(RingData ringData, String str) {
        com.shoujiduoduo.base.a.a.a("RingListAdapter", "setDefaultCtccCailing, id:" + ringData.s);
        com.shoujiduoduo.util.d.b.a().c(str, ringData.s, new bi(this, ringData));
    }

    /* access modifiers changed from: private */
    public void a(String str, RingData ringData) {
        com.shoujiduoduo.base.a.a.a("RingListAdapter", "设置默认铃音");
        a.a().h(new bk(this, str, ringData));
    }

    /* access modifiers changed from: private */
    public void a(boolean z, String str, String str2, RingData ringData) {
        a.a().a(str2, z, str, new bm(this, str2, ringData));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: private */
    public View a(RingData ringData, f.b bVar) {
        View inflate = LayoutInflater.from(this.f).inflate((int) R.layout.layout_cailing_info, (ViewGroup) null, false);
        ((ListView) inflate.findViewById(R.id.cailing_info_list)).setAdapter((ListAdapter) new SimpleAdapter(this.f, b(ringData, bVar), R.layout.listitem_cailing_info, new String[]{"cailing_info_des", "divider", "cailing_info_content"}, new int[]{R.id.cailing_info_des, R.id.devider, R.id.cailing_info_content}));
        return inflate;
    }

    private ArrayList<Map<String, Object>> b(RingData ringData, f.b bVar) {
        ArrayList<Map<String, Object>> arrayList = new ArrayList<>();
        HashMap hashMap = new HashMap();
        hashMap.put("cailing_info_des", "歌名");
        hashMap.put("divider", ":");
        hashMap.put("cailing_info_content", ringData.e);
        arrayList.add(hashMap);
        HashMap hashMap2 = new HashMap();
        hashMap2.put("cailing_info_des", "歌手");
        hashMap2.put("divider", ":");
        hashMap2.put("cailing_info_content", ringData.f);
        arrayList.add(hashMap2);
        HashMap hashMap3 = new HashMap();
        hashMap3.put("cailing_info_des", "有效期");
        hashMap3.put("divider", ":");
        String str = "";
        switch (bVar) {
            case f1671a:
                str = ringData.o;
                break;
            case cu:
                if (ringData.y != 1) {
                    str = ringData.B;
                    break;
                } else {
                    str = h();
                    break;
                }
            case ct:
                if (ringData.w != 2) {
                    str = ringData.t;
                    break;
                } else {
                    str = h();
                    break;
                }
        }
        hashMap3.put("cailing_info_content", str);
        arrayList.add(hashMap3);
        return arrayList;
    }

    @SuppressLint({"SimpleDateFormat"})
    private String h() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
        Date date = new Date();
        return simpleDateFormat.format(new Date(date.getYear() + 1, date.getMonth(), date.getDate()));
    }

    /* access modifiers changed from: package-private */
    public void c() {
        if (this.t == null) {
            this.t = new ProgressDialog(this.f);
            this.t.setMessage("您好，初始化彩铃功能需要发送一条免费短信，该过程自动完成，如弹出警告，请选择允许。");
            this.t.setIndeterminate(false);
            this.t.setCancelable(true);
            this.t.setCanceledOnTouchOutside(false);
            this.t.setButton(-1, "确定", new bp(this));
            this.t.show();
        }
    }

    /* access modifiers changed from: package-private */
    public void d() {
        if (this.t != null) {
            this.t.dismiss();
            this.t = null;
        }
    }

    /* access modifiers changed from: package-private */
    public void e() {
        a("请稍候...");
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        if (this.u == null) {
            this.u = new ProgressDialog(this.f);
            this.u.setMessage(str);
            this.u.setIndeterminate(false);
            this.u.setCancelable(true);
            this.u.setCanceledOnTouchOutside(false);
            this.u.show();
        }
    }

    /* access modifiers changed from: package-private */
    public void f() {
        if (this.u != null) {
            this.u.dismiss();
            this.u = null;
        }
    }

    private int a(int i2) {
        if (!this.g || i2 + 1 < this.i) {
            return i2;
        }
        return i2 - ((((i2 + 1) - this.i) / this.h) + 1);
    }

    private void a(TextView textView, String str) {
        String c2 = com.umeng.analytics.b.c(RingDDApp.c(), "feed_ad_layout_type");
        if (c2.equals("1")) {
            textView.setText(str);
        } else if (c2.equals("2") || c2.equals("3")) {
            textView.setText(Html.fromHtml(str + " " + "<img src=\"" + ((int) R.drawable.icon_ad_new) + "\" align='center'/>", this.f1541a, null));
        } else {
            textView.setText(str);
        }
    }

    private int i() {
        String c2 = com.umeng.analytics.b.c(RingDDApp.c(), "feed_ad_layout_type");
        if (c2.equals("1")) {
            return R.layout.listitem_feed_ad;
        }
        if (c2.equals("2") || c2.equals("3")) {
        }
        return R.layout.listitem_feed_ad2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int i2, View view, ViewGroup viewGroup) {
        i.a h2;
        if (this.b == null) {
            return null;
        }
        int itemViewType = getItemViewType(i2);
        if (itemViewType == 0) {
            if (view == null) {
                view = this.d.inflate((int) R.layout.listitem_ring, viewGroup, false);
                view.setTag(R.id.list_item_tag_key, "ring_tag");
            } else {
                Object tag = view.getTag(R.id.list_item_tag_key);
                if (tag != null && !tag.toString().equals("ring_tag")) {
                    com.shoujiduoduo.base.a.a.a("RingListAdapter", "View type is ring , but tag is not ring tag");
                    view = this.d.inflate((int) R.layout.listitem_ring, viewGroup, false);
                    view.setTag(R.id.list_item_tag_key, "ring_tag");
                }
            }
            if (a(i2) >= this.b.c()) {
                com.shoujiduoduo.base.a.a.a("RingListAdapter", "fuck, 越界了");
                return view;
            }
            a(view, a(i2));
            ProgressBar progressBar = (ProgressBar) cc.a(view, R.id.ringitem_download_progress);
            TextView textView = (TextView) cc.a(view, R.id.ringitem_serial_number);
            ImageButton imageButton = (ImageButton) cc.a(view, R.id.ringitem_play);
            ImageButton imageButton2 = (ImageButton) cc.a(view, R.id.ringitem_pause);
            ImageButton imageButton3 = (ImageButton) cc.a(view, R.id.ringitem_failed);
            imageButton.setOnClickListener(this.o);
            imageButton2.setOnClickListener(this.p);
            imageButton3.setOnClickListener(this.q);
            String str = "";
            PlayerService b2 = ak.a().b();
            if (b2 != null) {
                str = b2.b();
                this.c = b2.c();
            }
            if (!str.equals(this.b.a()) || a(i2) != this.c || !this.e) {
                ((Button) cc.a(view, R.id.ring_item_button0)).setVisibility(8);
                ((Button) cc.a(view, R.id.ring_item_button1)).setVisibility(8);
                ((Button) cc.a(view, R.id.ring_item_button2)).setVisibility(8);
                ((Button) cc.a(view, R.id.ring_item_button3)).setVisibility(8);
                ((Button) cc.a(view, R.id.ring_item_button4)).setVisibility(8);
                textView.setText(Integer.toString(i2 + 1));
                textView.setVisibility(0);
                progressBar.setVisibility(4);
                imageButton.setVisibility(4);
                imageButton2.setVisibility(4);
                imageButton3.setVisibility(4);
                return view;
            }
            RingData b3 = this.b.a(a(i2));
            Button button = (Button) cc.a(view, R.id.ring_item_button0);
            Button button2 = (Button) cc.a(view, R.id.ring_item_button1);
            Button button3 = (Button) cc.a(view, R.id.ring_item_button2);
            Button button4 = (Button) cc.a(view, R.id.ring_item_button3);
            Button button5 = (Button) cc.a(view, R.id.ring_item_button4);
            button.setVisibility(b3.j() ? 0 : 8);
            button5.setVisibility(b3.E == 1 ? 0 : 8);
            button2.setVisibility(0);
            button3.setVisibility(0);
            button4.setVisibility(a(b3) ? 0 : 8);
            button.setOnClickListener(this.v);
            button2.setCompoundDrawablesWithIntrinsicBounds((int) R.drawable.icon_ringitem_fav, 0, 0, 0);
            button2.setText((int) R.string.like);
            button2.setOnClickListener(this.y);
            button3.setOnClickListener(this.x);
            button4.setOnClickListener(this.r);
            button5.setOnClickListener(this.w);
            textView.setVisibility(4);
            progressBar.setVisibility(4);
            imageButton.setVisibility(4);
            imageButton2.setVisibility(4);
            imageButton3.setVisibility(4);
            int i3 = 5;
            if (b2 != null) {
                i3 = b2.a();
            }
            switch (i3) {
                case 1:
                    progressBar.setVisibility(0);
                    return view;
                case 2:
                    imageButton2.setVisibility(0);
                    return view;
                case 3:
                case 4:
                case 5:
                    imageButton.setVisibility(0);
                    return view;
                case 6:
                    imageButton3.setVisibility(0);
                    return view;
                default:
                    return view;
            }
        } else if (itemViewType != 1) {
            return view;
        } else {
            com.shoujiduoduo.base.a.a.a("RingListAdapter", "getview, ad type");
            if (view == null) {
                view = this.d.inflate(i(), viewGroup, false);
                view.setTag(R.id.list_item_tag_key, "ad_tag");
            } else {
                Object tag2 = view.getTag(R.id.list_item_tag_key);
                if (tag2 != null && !tag2.toString().equals("ad_tag")) {
                    com.shoujiduoduo.base.a.a.a("RingListAdapter", "View type is ad , but tag is not ad tag");
                    view = this.d.inflate(i(), viewGroup, false);
                    view.setTag(R.id.list_item_tag_key, "ad_tag");
                }
            }
            TextView textView2 = (TextView) view.findViewById(R.id.sn);
            TextView textView3 = (TextView) view.findViewById(R.id.title);
            TextView textView4 = (TextView) view.findViewById(R.id.content);
            ImageView imageView = (ImageView) view.findViewById(R.id.pic);
            if (com.umeng.analytics.b.c(RingDDApp.c(), "feed_ad_layout_type").equals("3")) {
                imageView.setVisibility(8);
            }
            textView2.setText("" + (i2 + 1));
            if (!this.j) {
                if (this.k.containsKey(Integer.valueOf(i2))) {
                    h2 = this.k.get(Integer.valueOf(i2));
                    h2.b();
                    if (h2.c()) {
                        this.k.remove(Integer.valueOf(i2));
                    }
                } else {
                    h2 = com.shoujiduoduo.a.b.b.c().h();
                    if (h2 != null) {
                        h2.b();
                        if (!h2.c()) {
                            this.k.put(Integer.valueOf(i2), h2);
                        }
                    }
                }
                if (h2 != null) {
                    a(textView3, h2.d());
                    textView4.setText(h2.e());
                    d.a().a(h2.f(), imageView, v.a().j());
                    h2.a(view);
                    view.setOnClickListener(new bu(this, h2));
                } else {
                    com.shoujiduoduo.base.a.a.e("RingListAdapter", "can not get valid feed ad, pos:" + (i2 + 1));
                    a(textView3, "儿歌多多");
                    textView4.setText("多多团队出品，最好的儿歌故事类应用");
                    imageView.setImageResource(R.drawable.child_story_logo);
                    view.setOnClickListener(new bv(this));
                }
            }
            this.j = false;
            return view;
        }
    }
}
