package com.shoujiduoduo.ui.utils.pageindicator;

import android.view.View;

/* compiled from: IconPageIndicator */
class b implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ View f1528a;
    final /* synthetic */ IconPageIndicator b;

    b(IconPageIndicator iconPageIndicator, View view) {
        this.b = iconPageIndicator;
        this.f1528a = view;
    }

    public void run() {
        this.b.smoothScrollTo(this.f1528a.getLeft() - ((this.b.getWidth() - this.f1528a.getWidth()) / 2), 0);
        Runnable unused = this.b.d = null;
    }
}
