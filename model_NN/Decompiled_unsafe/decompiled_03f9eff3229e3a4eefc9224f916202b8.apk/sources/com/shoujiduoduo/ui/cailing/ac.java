package com.shoujiduoduo.ui.cailing;

import android.widget.Toast;
import com.shoujiduoduo.a.a.b;
import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.util.as;
import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.f;
import com.shoujiduoduo.util.widget.f;

/* compiled from: CailingListAdapter */
class ac extends a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ q f928a;

    ac(q qVar) {
        this.f928a = qVar;
    }

    public void a(c.b bVar) {
        this.f928a.c();
        String unused = this.f928a.e = this.f928a.d;
        f.a("已设置为默认彩铃");
        as.c(this.f928a.i, "DEFAULT_CAILING_ID", this.f928a.e);
        this.f928a.notifyDataSetChanged();
        x.a().b(b.OBSERVER_RING_CHANGE, new ad(this));
    }

    public void b(c.b bVar) {
        String str;
        this.f928a.c();
        if (bVar.a().equals("999018") || bVar.a().equals("999019")) {
            new cb(this.f928a.i, R.style.DuoDuoDialog, as.a(RingDDApp.c(), "user_phone_num", ""), f.b.f1671a, new ae(this)).show();
            return;
        }
        if (bVar != null) {
            str = bVar.b();
        } else {
            str = "对不起，彩铃设置失败。";
        }
        Toast.makeText(this.f928a.i, str, 1).show();
    }
}
