package com.shoujiduoduo.ui.cailing;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import cn.banshenggua.aichang.utils.Constants;
import com.ffcs.inapppaylib.EMPHelper;
import com.shoujiduoduo.b.a.i;
import com.shoujiduoduo.base.bean.ab;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ui.utils.BaseActivity;
import com.shoujiduoduo.util.at;
import com.shoujiduoduo.util.av;
import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.f;
import com.shoujiduoduo.util.widget.f;
import com.umeng.analytics.b;

public class TestCtcc extends BaseActivity implements View.OnClickListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public String f923a = "15313739485";
    private String b = "810027210086";
    private EditText c;
    /* access modifiers changed from: private */
    public EditText d;
    private ContentObserver e;
    /* access modifiers changed from: private */
    public CheckBox f;
    private i g = new i();
    private EMPHelper h;
    private String i = "";
    private boolean j = false;
    private Handler k = new du(this);

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        String str;
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_test_ctcc);
        this.c = (EditText) findViewById(R.id.music_id);
        this.c.setText(this.b);
        this.d = (EditText) findViewById(R.id.phone_num);
        this.d.setText(this.f923a);
        findViewById(R.id.query_vip_state).setOnClickListener(this);
        findViewById(R.id.open_vip).setOnClickListener(this);
        findViewById(R.id.emp_open_vip).setOnClickListener(this);
        findViewById(R.id.vip_order).setOnClickListener(this);
        findViewById(R.id.sms_random_key).setOnClickListener(this);
        findViewById(R.id.close_vip).setOnClickListener(this);
        findViewById(R.id.emp_close_vip).setOnClickListener(this);
        findViewById(R.id.emp_launch).setOnClickListener(this);
        findViewById(R.id.query3rd_phone).setOnClickListener(this);
        findViewById(R.id.query3rd_uid).setOnClickListener(this);
        findViewById(R.id.check_base_cailing_status).setOnClickListener(this);
        findViewById(R.id.find_mdn_by_imsi).setOnClickListener(this);
        findViewById(R.id.emp_one_key_open).setOnClickListener(this);
        findViewById(R.id.sms_random_key_common).setOnClickListener(this);
        findViewById(R.id.query_caililng_and_vip).setOnClickListener(this);
        findViewById(R.id.diy_clip_upload).setOnClickListener(this);
        findViewById(R.id.set_diy_ring).setOnClickListener(this);
        findViewById(R.id.get_diy_ring).setOnClickListener(this);
        findViewById(R.id.query_ring_info).setOnClickListener(this);
        findViewById(R.id.query_cailing_url).setOnClickListener(this);
        findViewById(R.id.query_zhenling_url).setOnClickListener(this);
        findViewById(R.id.query_diy_ring_status).setOnClickListener(this);
        findViewById(R.id.query_ring_box).setOnClickListener(this);
        findViewById(R.id.query_default_ring).setOnClickListener(this);
        this.f = (CheckBox) findViewById(R.id.checkbox);
        this.f.setChecked(true);
        if (!TextUtils.isEmpty(b.c(this, "ctcc_num_launch"))) {
            str = b.c(this, "ctcc_num_launch");
        } else {
            str = "1065987320001";
        }
        this.h = EMPHelper.getInstance(this);
        this.h.init("1000010404421", "5297", "x4lRWHcgRqfH", this.k, 15000);
        this.e = new at(this, new Handler(), (EditText) findViewById(R.id.random_key), str);
        getContentResolver().registerContentObserver(Uri.parse("content://sms/"), true, this.e);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        getContentResolver().unregisterContentObserver(this.e);
        super.onDestroy();
    }

    public void onClick(View view) {
        this.f923a = this.d.getText().toString();
        if (av.c(this.f923a)) {
            f.a("请输入手机号");
            a(this.f923a, f.b.ct);
            return;
        }
        switch (view.getId()) {
            case R.id.check_base_cailing_status:
                if (!TextUtils.isEmpty(this.d.getText().toString())) {
                    this.f923a = this.d.getText().toString();
                }
                com.shoujiduoduo.util.d.b.a().g(this.f923a, new eg(this));
                return;
            case R.id.query_vip_state:
                com.shoujiduoduo.util.d.b.a().a(this.f923a, this.f.isChecked(), "", new es(this));
                return;
            case R.id.query_caililng_and_vip:
                com.shoujiduoduo.util.d.b.a().a(this.f923a, new eu(this));
                return;
            case R.id.vip_order:
                if (!TextUtils.isEmpty(this.c.getText().toString())) {
                    this.b = this.c.getText().toString();
                }
                if (!TextUtils.isEmpty(this.d.getText().toString())) {
                    this.f923a = this.d.getText().toString();
                }
                com.shoujiduoduo.util.d.b.a().a(this.f923a, this.b, "", new dx(this));
                return;
            case R.id.open_vip:
                if (!TextUtils.isEmpty(this.d.getText().toString())) {
                    this.f923a = this.d.getText().toString();
                }
                new AlertDialog.Builder(this).setMessage("确定要开通包月？").setNegativeButton("取消", (DialogInterface.OnClickListener) null).setPositiveButton("确定", new ev(this)).show();
                return;
            case R.id.close_vip:
                if (!TextUtils.isEmpty(this.d.getText().toString())) {
                    this.f923a = this.d.getText().toString();
                }
                new AlertDialog.Builder(this).setMessage("确定关闭包月？").setNegativeButton("取消", (DialogInterface.OnClickListener) null).setPositiveButton("确定", new ea(this)).show();
                return;
            case R.id.get_sms_code:
            case R.id.checkbox:
            case R.id.emp_operate_layout:
            default:
                return;
            case R.id.query_cailing_url:
                com.shoujiduoduo.util.d.b.a().h("810027214935", new a());
                return;
            case R.id.query_ring_box:
                com.shoujiduoduo.util.d.b.a().e(this.f923a, new ei(this));
                return;
            case R.id.query_default_ring:
                com.shoujiduoduo.util.d.b.a().f(this.f923a, new eq(this));
                return;
            case R.id.find_mdn_by_imsi:
                String i2 = com.shoujiduoduo.util.f.i();
                if (!TextUtils.isEmpty(i2)) {
                    com.shoujiduoduo.util.d.b.a().b(i2, new eh(this));
                    return;
                }
                return;
            case R.id.diy_clip_upload:
                com.shoujiduoduo.util.d.b.a().a("剪辑铃声", this.f923a, "http://www.shoujiduoduo.com/data/test.wav ", "", new ej(this));
                return;
            case R.id.set_diy_ring:
                com.shoujiduoduo.util.d.b.a().b("588909", this.f923a, "", new ek(this));
                return;
            case R.id.get_diy_ring:
                com.shoujiduoduo.util.d.b.a().c(this.f923a, new em(this));
                return;
            case R.id.query_diy_ring_status:
                com.shoujiduoduo.util.d.b.a().b("810079618974", this.f923a, new el(this));
                return;
            case R.id.sms_random_key:
                if (!TextUtils.isEmpty(this.d.getText().toString())) {
                    this.f923a = this.d.getText().toString();
                }
                com.shoujiduoduo.util.d.b.a().a(this.f923a, "铃声多多自定义短信验证码：", new dy(this));
                return;
            case R.id.sms_random_key_common:
                if (!TextUtils.isEmpty(this.d.getText().toString())) {
                    this.f923a = this.d.getText().toString();
                }
                com.shoujiduoduo.util.d.b.a().d(this.f923a, new dz(this));
                return;
            case R.id.query_ring_info:
                com.shoujiduoduo.util.d.b.a().j("810027214935", new et(this));
                return;
            case R.id.query_zhenling_url:
                com.shoujiduoduo.util.d.b.a().i("810027214935", new a());
                return;
            case R.id.emp_launch:
                com.shoujiduoduo.util.d.b.a().a(this.f923a, this.f.isChecked(), new er(this));
                return;
            case R.id.emp_open_vip:
                if (!TextUtils.isEmpty(this.d.getText().toString())) {
                    this.f923a = this.d.getText().toString();
                }
                String obj = ((EditText) findViewById(R.id.random_key)).getText().toString();
                if (TextUtils.isEmpty(obj)) {
                    com.shoujiduoduo.util.widget.f.a("验证码不正确");
                    return;
                } else {
                    new AlertDialog.Builder(this).setMessage("确定要开通包月？").setPositiveButton("确定", new ex(this, obj)).setNegativeButton("取消", (DialogInterface.OnClickListener) null).show();
                    return;
                }
            case R.id.emp_close_vip:
                if (!TextUtils.isEmpty(this.d.getText().toString())) {
                    this.f923a = this.d.getText().toString();
                }
                new AlertDialog.Builder(this).setMessage("确定关闭包月？").setPositiveButton("确定", new ec(this)).setNegativeButton("取消", (DialogInterface.OnClickListener) null).show();
                return;
            case R.id.emp_one_key_open:
                if (!TextUtils.isEmpty(this.d.getText().toString())) {
                    this.f923a = this.d.getText().toString();
                }
                String obj2 = ((EditText) findViewById(R.id.random_key)).getText().toString();
                if (TextUtils.isEmpty(obj2)) {
                    com.shoujiduoduo.util.widget.f.a("验证码不正确");
                    return;
                } else {
                    new AlertDialog.Builder(this).setMessage("确定一键开通彩铃和包月？").setNegativeButton("取消", (DialogInterface.OnClickListener) null).setPositiveButton("确定", new dv(this, obj2)).show();
                    return;
                }
            case R.id.query3rd_phone:
                ab c2 = com.shoujiduoduo.a.b.b.g().c();
                String i3 = com.shoujiduoduo.util.f.i();
                StringBuilder sb = new StringBuilder();
                sb.append("&newimsi=").append(i3).append("&phone=").append(c2.k());
                com.shoujiduoduo.util.i.a(new ee(this, sb));
                return;
            case R.id.query3rd_uid:
                com.shoujiduoduo.util.i.a(new ef(this));
                return;
        }
    }

    /* access modifiers changed from: private */
    public void a() {
        ab c2 = com.shoujiduoduo.a.b.b.g().c();
        String i2 = com.shoujiduoduo.util.f.i();
        com.shoujiduoduo.base.a.a.a("testctcc", "getservice end");
        String bVar = com.shoujiduoduo.util.f.u().toString();
        String str = "";
        switch (c2.e()) {
            case 1:
                str = "phone";
                break;
            case 2:
                str = "qq";
                break;
            case 3:
                str = Constants.WEIBO;
                break;
            case 5:
                str = "weixin";
                break;
        }
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("&newimsi=").append(i2).append("&phone=").append(c2.k()).append("&st=").append(bVar).append("&uid=").append(c2.a()).append("&3rd=").append(str);
        com.shoujiduoduo.util.i.a(new en(this, stringBuffer));
    }

    /* access modifiers changed from: private */
    public void a(c.b bVar) {
        new AlertDialog.Builder(this).setMessage(bVar.toString()).setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
    }

    /* access modifiers changed from: private */
    public void a(c.b bVar, String str) {
        new AlertDialog.Builder(this).setMessage(bVar.toString() + " , " + str).setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
    }

    private void a(String str, f.b bVar) {
        new cb(this, R.style.DuoDuoDialog, str, bVar, new eo(this)).show();
    }
}
