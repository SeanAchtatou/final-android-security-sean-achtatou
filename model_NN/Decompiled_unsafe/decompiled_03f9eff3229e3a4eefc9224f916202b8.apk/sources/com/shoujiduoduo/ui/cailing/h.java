package com.shoujiduoduo.ui.cailing;

import android.app.ProgressDialog;

/* compiled from: BuyCailingDialog */
class h implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f1086a;
    final /* synthetic */ a b;

    h(a aVar, String str) {
        this.b = aVar;
        this.f1086a = str;
    }

    public void run() {
        if (this.b.p == null) {
            ProgressDialog unused = this.b.p = new ProgressDialog(this.b.b);
            this.b.p.setMessage(this.f1086a);
            this.b.p.setIndeterminate(false);
            this.b.p.setCancelable(false);
            this.b.p.show();
        }
    }
}
