package com.shoujiduoduo.ui.cailing;

import android.app.AlertDialog;
import android.content.DialogInterface;
import com.cmsc.cmmusic.common.data.GetUserInfoRsp;
import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.c.b;

/* compiled from: GiveCailingActivity */
class bu extends a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GiveCailingActivity f978a;

    bu(GiveCailingActivity giveCailingActivity) {
        this.f978a = giveCailingActivity;
    }

    public void a(c.b bVar) {
        this.f978a.a();
        new AlertDialog.Builder(this.f978a).setTitle("兑换彩铃").setMessage("兑换彩铃成功，谢谢使用").setPositiveButton("确认", (DialogInterface.OnClickListener) null).show();
        b.a().f(this.f978a.i.n, new bv(this));
        this.f978a.setResult(100);
    }

    public void b(c.b bVar) {
        this.f978a.a();
        if (bVar.a().equals(GetUserInfoRsp.NON_MEM_ERROR_CODE)) {
            try {
                new AlertDialog.Builder(this.f978a).setTitle("赠送彩铃").setMessage("对不起，您还未开通中移动彩铃功能，是否立即开通？").setPositiveButton("是", new bw(this)).setNegativeButton("否", (DialogInterface.OnClickListener) null).show();
            } catch (Exception e) {
                com.umeng.analytics.b.a(this.f978a, "AlertDialog Failed!");
            }
        } else {
            try {
                new AlertDialog.Builder(this.f978a).setTitle("免费兑换彩铃").setMessage(bVar.b()).setPositiveButton("确认", (DialogInterface.OnClickListener) null).show();
            } catch (Exception e2) {
                com.umeng.analytics.b.a(this.f978a, "AlertDialog Failed!");
            }
        }
    }
}
