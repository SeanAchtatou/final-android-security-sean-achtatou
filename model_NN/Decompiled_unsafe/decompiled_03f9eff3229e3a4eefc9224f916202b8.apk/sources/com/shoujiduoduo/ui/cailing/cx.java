package com.shoujiduoduo.ui.cailing;

import android.content.DialogInterface;
import com.shoujiduoduo.base.a.a;

/* compiled from: SmsAuthDialog */
class cx implements DialogInterface.OnDismissListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ cw f1007a;

    cx(cw cwVar) {
        this.f1007a = cwVar;
    }

    public void onDismiss(DialogInterface dialogInterface) {
        a.a("SmsAuthDialog", "dialog dismiss, unregister sms observer");
        if (this.f1007a.k != null) {
            this.f1007a.k.getContentResolver().unregisterContentObserver(this.f1007a.j);
        }
    }
}
