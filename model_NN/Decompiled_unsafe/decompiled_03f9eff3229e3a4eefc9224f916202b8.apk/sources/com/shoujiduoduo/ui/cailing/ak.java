package com.shoujiduoduo.ui.cailing;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.SimpleAdapter;
import android.widget.Toast;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.base.bean.f;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.util.as;
import com.shoujiduoduo.util.c.b;
import com.shoujiduoduo.util.f;
import com.shoujiduoduo.util.w;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;

/* compiled from: CailingMenu */
public class ak extends PopupWindow {
    private static final String c = ak.class.getSimpleName();
    private static ak p = null;

    /* renamed from: a  reason: collision with root package name */
    boolean f935a = false;
    AdapterView.OnItemClickListener b = new am(this);
    private ListView d = null;
    /* access modifiers changed from: private */
    public Context e = null;
    private View f = null;
    private RingData g = null;
    private String h;
    private f.a i;
    private final String j = "订购彩铃";
    private final String k = "赠送彩铃";
    private final String l = "管理彩铃";
    private final String m = "免费获取彩铃";
    /* access modifiers changed from: private */
    public a n = a.f937a;
    /* access modifiers changed from: private */
    public ArrayList<Map<String, Object>> o = new ArrayList<>();
    /* access modifiers changed from: private */
    public c q = new c(this, null);
    private ProgressDialog r = null;
    /* access modifiers changed from: private */
    public ProgressDialog s = null;

    /* compiled from: CailingMenu */
    private enum a {
        f937a,
        buy,
        give,
        manage,
        openMem,
        checkState,
        toggleMenu
    }

    private ArrayList<Map<String, Object>> d() {
        HashMap hashMap = new HashMap();
        hashMap.put("TEXT", "订购彩铃");
        this.o.add(hashMap);
        HashMap hashMap2 = new HashMap();
        hashMap2.put("TEXT", "赠送彩铃");
        this.o.add(hashMap2);
        HashMap hashMap3 = new HashMap();
        hashMap3.put("TEXT", "管理彩铃");
        this.o.add(hashMap3);
        return this.o;
    }

    private ak() {
    }

    private ak(Context context) {
        super(context);
        this.e = context;
        e();
    }

    public Context a() {
        return this.e;
    }

    public static ak a(Context context) {
        return new ak(context);
    }

    /* compiled from: CailingMenu */
    private class c extends Handler {
        private c() {
        }

        /* synthetic */ c(ak akVar, al alVar) {
            this();
        }

        public void handleMessage(Message message) {
            super.handleMessage(message);
            if (ak.this.e != null) {
                switch (message.what) {
                    case 1:
                        com.shoujiduoduo.util.c.b.a().c(new as(this));
                        return;
                    case 2:
                        as.b(ak.this.e, "NeedUpdateCaiLingLib", 1);
                        switch (ak.this.n) {
                            case buy:
                                ak.this.j();
                                return;
                            case give:
                                ak.this.k();
                                return;
                            case manage:
                                ak.this.i();
                                return;
                            case openMem:
                                ak.this.h();
                                return;
                            case f937a:
                            default:
                                return;
                        }
                    case 3:
                        ak.this.c();
                        Toast.makeText(RingDDApp.c(), "彩铃模块初始化失败", 0).show();
                        return;
                    default:
                        return;
                }
            }
        }
    }

    private void e() {
        this.f = ((LayoutInflater) this.e.getSystemService("layout_inflater")).inflate((int) R.layout.cailing_menu, (ViewGroup) null);
        this.d = (ListView) this.f.findViewById(R.id.cailing_menu_list);
        this.d.setAdapter((ListAdapter) new b(this.e, d(), R.layout.cailing_menu_item, new String[]{"TEXT"}, new int[]{R.id.cailing_menu_dest}));
        this.d.setItemsCanFocus(false);
        this.d.setChoiceMode(2);
        this.d.setOnItemClickListener(this.b);
        setContentView(this.f);
        setWindowLayoutMode(-2, -2);
        setBackgroundDrawable(this.e.getResources().getDrawable(R.drawable.transparent_bkg));
        setFocusable(true);
        setOutsideTouchable(false);
        setOnDismissListener(new al(this));
    }

    private void f() {
    }

    private void g() {
        com.shoujiduoduo.base.a.a.a(c, "准备初始化移动sdk");
        if (this.e != null) {
            b();
            new Timer().schedule(new an(this), 3000);
        }
    }

    /* access modifiers changed from: private */
    public void h() {
        if (com.shoujiduoduo.util.c.b.a().c() == b.a.success) {
            if (!((Activity) this.e).isFinishing()) {
                new ck(this.e, R.style.DuoDuoDialog, f.b.f1671a, null).show();
            } else {
                com.shoujiduoduo.base.a.a.c(c, "fuck, main activity is finishing");
            }
        } else if (com.shoujiduoduo.util.c.b.a().c() == b.a.initializing) {
            com.shoujiduoduo.base.a.a.b(c, "开通彩铃，移动sdk正在初始化，提示稍后再试");
            Toast.makeText(this.e, "中国移动彩铃业务正在初始化，请稍后再试", 0).show();
        } else {
            com.shoujiduoduo.base.a.a.b(c, "开通彩铃，准备初始化移动sdk");
            this.n = a.openMem;
            g();
        }
    }

    /* access modifiers changed from: private */
    public void i() {
        if (com.shoujiduoduo.util.f.v()) {
            com.shoujiduoduo.base.a.a.a(c, "彩铃管理， cmcc");
            if (com.shoujiduoduo.util.c.b.a().c() == b.a.success) {
                ((Activity) this.e).startActivity(new Intent(this.e, CailingManageActivity.class));
            } else if (com.shoujiduoduo.util.c.b.a().c() == b.a.initializing) {
                com.shoujiduoduo.base.a.a.b(c, "管理彩铃，移动sdk正在初始化，提示稍后再试");
                Toast.makeText(this.e, "中国移动彩铃业务正在初始化，请稍后再试", 0).show();
            } else {
                com.shoujiduoduo.base.a.a.b(c, "管理彩铃，准备初始化移动sdk");
                this.n = a.manage;
                g();
            }
        } else if (com.shoujiduoduo.util.f.x()) {
            com.shoujiduoduo.base.a.a.a(c, "彩铃管理， ctcc");
            String a2 = as.a(a(), "pref_phone_num", "");
            com.shoujiduoduo.base.a.a.a(c, "记录的手机号：" + a2);
            if (TextUtils.isEmpty(a2)) {
                new cb(this.e, R.style.DuoDuoDialog, "", f.b.ct, new ao(this)).show();
                return;
            }
            ((Activity) this.e).startActivity(new Intent(this.e, CailingManageActivity.class));
        }
    }

    /* access modifiers changed from: private */
    public void j() {
        com.shoujiduoduo.base.a.a.b(c, "订购彩铃");
        if (com.shoujiduoduo.util.f.v()) {
            if (com.shoujiduoduo.util.c.b.a().c() == b.a.success) {
                com.shoujiduoduo.base.a.a.a(c, "buy cailing3");
                if (!((Activity) this.e).isFinishing()) {
                    a aVar = new a(this.e, R.style.DuoDuoDialog, f.b.f1671a);
                    aVar.a(this.g, this.h, this.i);
                    aVar.show();
                    w.a(this.g.g, 6, this.h, this.i.toString(), "&cucid=" + this.g.A);
                    return;
                }
                com.shoujiduoduo.base.a.a.c(c, "fuck, main activity isfinishing, is " + this.e.toString());
            } else if (com.shoujiduoduo.util.c.b.a().c() == b.a.initializing) {
                com.shoujiduoduo.base.a.a.b(c, "订购彩铃，移动sdk正在初始化，提示稍后再试");
                Toast.makeText(this.e, "中国移动彩铃业务正在初始化，请稍后再试", 0).show();
            } else {
                this.n = a.buy;
                g();
            }
        } else if (com.shoujiduoduo.util.f.x()) {
            a aVar2 = new a(this.e, R.style.DuoDuoDialog, f.b.ct);
            aVar2.a(this.g, this.h, this.i);
            aVar2.show();
            w.a(this.g.g, 6, this.h, this.i.toString(), "&cucid=" + this.g.A);
        }
    }

    /* access modifiers changed from: private */
    public void k() {
        com.shoujiduoduo.base.a.a.b(c, "赠送彩铃");
        if (com.shoujiduoduo.util.f.v()) {
            if (com.shoujiduoduo.util.c.b.a().c() == b.a.success) {
                if (!((Activity) this.e).isFinishing()) {
                    Intent intent = new Intent(this.e, GiveCailingActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putParcelable("ringdata", this.g);
                    intent.putExtras(bundle);
                    intent.putExtra("listid", this.h);
                    intent.putExtra("listtype", this.i.toString());
                    intent.putExtra("operator_type", 0);
                    this.e.startActivity(intent);
                    w.a(this.g.g, 7, this.h, this.i.toString(), "&cucid=" + this.g.A);
                    return;
                }
                com.shoujiduoduo.base.a.a.c(c, "fuck, main activity isfinishing, is " + this.e.toString());
            } else if (com.shoujiduoduo.util.c.b.a().c() == b.a.initializing) {
                com.shoujiduoduo.base.a.a.b(c, "赠送彩铃，移动sdk正在初始化，提示稍后再试");
                Toast.makeText(this.e, "中国移动彩铃业务正在初始化，请稍后再试", 0).show();
            } else {
                this.n = a.give;
                g();
            }
        } else if (com.shoujiduoduo.util.f.x() && !((Activity) this.e).isFinishing()) {
            Intent intent2 = new Intent(this.e, GiveCailingActivity.class);
            Bundle bundle2 = new Bundle();
            bundle2.putParcelable("ringdata", this.g);
            intent2.putExtras(bundle2);
            intent2.putExtra("listid", this.h);
            intent2.putExtra("listtype", this.i.toString());
            intent2.putExtra("operator_type", 1);
            this.e.startActivity(intent2);
            w.a(this.g.g, 7, this.h, this.i.toString(), "&cucid=" + this.g.A);
        }
    }

    public void a(View view, RingData ringData, String str, f.a aVar) {
        if (view != null && ringData != null) {
            if (com.shoujiduoduo.util.c.b.a().c() == b.a.success) {
                f();
            }
            this.f935a = !this.f935a;
            this.h = str;
            this.i = aVar;
            if (this.f935a) {
                this.g = ringData;
                update();
                showAsDropDown(view);
                return;
            }
            dismiss();
        }
    }

    /* compiled from: CailingMenu */
    class b extends SimpleAdapter {
        public b(Context context, List<? extends Map<String, ?>> list, int i, String[] strArr, int[] iArr) {
            super(context, list, i, strArr, iArr);
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            View view2 = super.getView(i, view, viewGroup);
            if (i == 0) {
                view2.setBackgroundDrawable(ak.this.e.getResources().getDrawable(R.drawable.share_menu_top_bkg));
            } else if (i == ak.this.o.size() - 1) {
                view2.setBackgroundDrawable(ak.this.e.getResources().getDrawable(R.drawable.share_menu_bottom_bkg));
            } else {
                view2.setBackgroundDrawable(ak.this.e.getResources().getDrawable(R.drawable.share_menu_middle_bkg));
            }
            return view2;
        }
    }

    /* access modifiers changed from: package-private */
    public void b() {
        this.q.post(new ap(this));
    }

    /* access modifiers changed from: package-private */
    public void c() {
        this.q.post(new ar(this));
    }
}
