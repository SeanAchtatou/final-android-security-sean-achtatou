package com.shoujiduoduo.ui.cailing;

import android.view.View;
import android.widget.Toast;
import com.shoujiduoduo.util.f;

/* compiled from: GiveCailingActivity */
class bp implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GiveCailingActivity f973a;

    bp(GiveCailingActivity giveCailingActivity) {
        this.f973a = giveCailingActivity;
    }

    public void onClick(View view) {
        String obj = this.f973a.f.getText().toString();
        if (obj == null || !f.f(this.f973a.f.getText().toString())) {
            Toast.makeText(this.f973a, "请输入正确的手机号", 1).show();
            return;
        }
        this.f973a.b("请稍候...");
        if (this.f973a.r == f.b.ct) {
            this.f973a.c(obj);
        }
    }
}
