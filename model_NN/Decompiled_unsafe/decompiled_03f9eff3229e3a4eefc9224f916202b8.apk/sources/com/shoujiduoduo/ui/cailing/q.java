package com.shoujiduoduo.ui.cailing;

import android.app.ProgressDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.shoujiduoduo.a.a.b;
import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.b.c.n;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ui.utils.cc;
import com.shoujiduoduo.ui.utils.g;
import com.shoujiduoduo.util.ak;
import com.shoujiduoduo.util.as;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.f;

/* compiled from: CailingListAdapter */
public class q extends g {

    /* renamed from: a  reason: collision with root package name */
    boolean f1095a = false;
    public com.shoujiduoduo.util.b.a b = new ac(this);
    /* access modifiers changed from: private */
    public n c;
    /* access modifiers changed from: private */
    public String d = "";
    /* access modifiers changed from: private */
    public String e = "";
    /* access modifiers changed from: private */
    public int f = -1;
    private LayoutInflater g;
    /* access modifiers changed from: private */
    public f.b h;
    /* access modifiers changed from: private */
    public Context i;
    /* access modifiers changed from: private */
    public boolean j;
    /* access modifiers changed from: private */
    public String k;
    private com.shoujiduoduo.a.c.n l = new r(this);
    private View.OnClickListener m = new s(this);
    private View.OnClickListener n = new t(this);
    private View.OnClickListener o = new u(this);
    private View.OnClickListener p = new v(this);
    /* access modifiers changed from: private */
    public com.shoujiduoduo.util.b.a q = new x(this);
    private View.OnClickListener r = new ab(this);
    private View.OnClickListener s = new af(this);
    private ProgressDialog t = null;

    public q(Context context) {
        this.i = context;
        this.g = LayoutInflater.from(context);
    }

    public void a() {
        x.a().a(b.OBSERVER_PLAY_STATUS, this.l);
    }

    public void b() {
        x.a().b(b.OBSERVER_PLAY_STATUS, this.l);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.util.c.b.a(java.lang.String, com.shoujiduoduo.util.b.a, boolean):void
     arg types: [java.lang.String, com.shoujiduoduo.ui.cailing.q$a, int]
     candidates:
      com.shoujiduoduo.util.c.b.a(com.shoujiduoduo.util.b.c$b, java.lang.String, java.lang.String):void
      com.shoujiduoduo.util.c.b.a(java.lang.String, com.shoujiduoduo.util.b.c$b, java.lang.String):void
      com.shoujiduoduo.util.c.b.a(java.lang.String, java.lang.String, java.lang.String):void
      com.shoujiduoduo.util.c.b.a(java.lang.String, java.lang.String, com.shoujiduoduo.util.b.a):void
      com.shoujiduoduo.util.c.b.a(java.lang.String, com.shoujiduoduo.util.b.a, boolean):void */
    private void d() {
        com.shoujiduoduo.base.a.a.a("CailingListAdapter", "begin queryUserRingBox");
        if (this.h.equals(f.b.f1671a)) {
            com.shoujiduoduo.util.c.b.a().a("", (com.shoujiduoduo.util.b.a) new a(this, null), false);
        } else if (this.h.equals(f.b.ct)) {
            com.shoujiduoduo.util.d.b.a().f(as.a(RingDDApp.c(), "pref_phone_num"), new a(this, null));
        } else if (this.h.equals(f.b.cu)) {
            com.shoujiduoduo.util.e.a.a().h(new a(this, null));
        }
    }

    /* compiled from: CailingListAdapter */
    private class a extends com.shoujiduoduo.util.b.a {
        private a() {
        }

        /* synthetic */ a(q qVar, r rVar) {
            this();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.ui.cailing.q.a(com.shoujiduoduo.ui.cailing.q, boolean):boolean
         arg types: [com.shoujiduoduo.ui.cailing.q, int]
         candidates:
          com.shoujiduoduo.ui.cailing.q.a(com.shoujiduoduo.ui.cailing.q, int):int
          com.shoujiduoduo.ui.cailing.q.a(com.shoujiduoduo.ui.cailing.q, java.lang.String):java.lang.String
          com.shoujiduoduo.ui.cailing.q.a(android.view.View, int):void
          com.shoujiduoduo.ui.cailing.q.a(com.shoujiduoduo.ui.cailing.q, boolean):boolean */
        public void a(c.b bVar) {
            if (q.this.h.equals(f.b.cu)) {
                if (bVar != null && (bVar instanceof c.q)) {
                    c.q qVar = (c.q) bVar;
                    if (qVar.d != null) {
                        for (int i = 0; i < qVar.d.length; i++) {
                            if (qVar.d[i].c.equals("0")) {
                                boolean unused = q.this.j = true;
                                String unused2 = q.this.k = qVar.d[i].f1595a;
                                String unused3 = q.this.e = qVar.d[i].d;
                                as.c(q.this.i, "DEFAULT_CAILING_ID", q.this.e);
                                q.this.notifyDataSetChanged();
                                com.shoujiduoduo.base.a.a.a("CailingListAdapter", "default cucc cailing id:" + q.this.e);
                                return;
                            }
                        }
                    }
                }
            } else if (bVar != null && (bVar instanceof c.x)) {
                c.x xVar = (c.x) bVar;
                if (xVar.a() == null || xVar.d() == null || xVar.d().size() <= 0) {
                    com.shoujiduoduo.base.a.a.c("CailingListAdapter", "查询默认彩铃失败，code:" + bVar.a() + " msg:" + bVar.b());
                    return;
                }
                String unused4 = q.this.e = xVar.d().get(0).b();
                as.c(q.this.i, "DEFAULT_CAILING_ID", q.this.e);
                q.this.notifyDataSetChanged();
                com.shoujiduoduo.base.a.a.a("CailingListAdapter", "default cailing id:" + q.this.e);
            }
        }

        public void b(c.b bVar) {
            com.shoujiduoduo.base.a.a.c("CailingListAdapter", "查询默认彩铃失败，code:" + bVar.a() + " msg:" + bVar.b());
        }
    }

    public int getCount() {
        if (this.c == null) {
            return 0;
        }
        return this.c.c();
    }

    public Object getItem(int i2) {
        if (this.c != null && i2 >= 0 && i2 < this.c.c()) {
            return this.c.a(i2);
        }
        return null;
    }

    public long getItemId(int i2) {
        return (long) i2;
    }

    private void a(View view, int i2) {
        RingData b2 = this.c.a(i2);
        TextView textView = (TextView) cc.a(view, R.id.cailing_item_valid_date);
        TextView textView2 = (TextView) cc.a(view, R.id.cailing_item_default_tip);
        ((TextView) cc.a(view, R.id.cailing_item_song_name)).setText(b2.e);
        ((TextView) cc.a(view, R.id.cailing_item_artist)).setText(b2.f);
        String str = "";
        String str2 = "";
        if (this.h.equals(f.b.f1671a)) {
            str = b2.o;
            str2 = b2.n;
        } else if (this.h.equals(f.b.ct)) {
            str = b2.t;
            str2 = b2.s;
        } else if (this.h.equals(f.b.cu)) {
            str = b2.B;
            str2 = b2.A;
        }
        textView.setText(String.format("有效期:" + str, new Object[0]));
        if (str.equals("")) {
            textView.setVisibility(4);
        } else {
            textView.setVisibility(0);
        }
        textView2.setText("当前彩铃");
        this.e = as.a(RingDDApp.c(), "DEFAULT_CAILING_ID", "");
        if (str2.equals(this.e)) {
            textView2.setVisibility(0);
        } else {
            textView2.setVisibility(8);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int i2, View view, ViewGroup viewGroup) {
        if (this.c == null) {
            return null;
        }
        if (i2 >= this.c.c()) {
            return view;
        }
        if (view == null) {
            view = this.g.inflate((int) R.layout.listitem_cailing_manage, (ViewGroup) null, false);
        }
        a(view, i2);
        ProgressBar progressBar = (ProgressBar) cc.a(view, R.id.cailing_item_download_progress);
        TextView textView = (TextView) cc.a(view, R.id.cailing_item_serial_number);
        ImageButton imageButton = (ImageButton) cc.a(view, R.id.cailing_item_play);
        ImageButton imageButton2 = (ImageButton) cc.a(view, R.id.cailing_item_pause);
        ImageButton imageButton3 = (ImageButton) cc.a(view, R.id.cailing_item_failed);
        imageButton3.setOnClickListener(this.o);
        imageButton.setOnClickListener(this.m);
        imageButton2.setOnClickListener(this.n);
        if (i2 != this.f || !this.f1095a) {
            ((Button) cc.a(view, R.id.cailing_item_set_default)).setVisibility(8);
            ((Button) cc.a(view, R.id.cailing_item_give)).setVisibility(8);
            ((Button) cc.a(view, R.id.cailing_item_delete)).setVisibility(8);
            textView.setText(Integer.toString(i2 + 1));
            textView.setVisibility(0);
            progressBar.setVisibility(4);
            imageButton.setVisibility(4);
            imageButton2.setVisibility(4);
            imageButton3.setVisibility(4);
            return view;
        }
        Button button = (Button) cc.a(view, R.id.cailing_item_set_default);
        Button button2 = (Button) cc.a(view, R.id.cailing_item_give);
        Button button3 = (Button) cc.a(view, R.id.cailing_item_delete);
        button.setVisibility(0);
        button2.setVisibility(8);
        button3.setVisibility(0);
        button.setOnClickListener(this.r);
        button2.setOnClickListener(this.s);
        button3.setOnClickListener(this.p);
        textView.setVisibility(4);
        progressBar.setVisibility(4);
        imageButton.setVisibility(4);
        imageButton2.setVisibility(4);
        imageButton3.setVisibility(4);
        PlayerService b2 = ak.a().b();
        if (b2 == null) {
            return view;
        }
        switch (b2.a()) {
            case 1:
                progressBar.setVisibility(0);
                return view;
            case 2:
                imageButton2.setVisibility(0);
                return view;
            case 3:
            case 4:
            case 5:
                imageButton.setVisibility(0);
                return view;
            case 6:
                imageButton3.setVisibility(0);
                return view;
            default:
                return view;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        if (this.t == null) {
            this.t = new ProgressDialog(this.i);
            this.t.setMessage(str);
            this.t.setIndeterminate(false);
            this.t.setCancelable(false);
            this.t.show();
        }
    }

    /* access modifiers changed from: package-private */
    public void c() {
        if (this.t != null) {
            this.t.dismiss();
            this.t = null;
        }
    }

    public void a(boolean z) {
    }

    public void a(com.shoujiduoduo.base.bean.c cVar) {
        if (this.c != cVar) {
            this.c = (n) cVar;
            notifyDataSetChanged();
        }
        if (cVar.a().equals("cmcc_cailing")) {
            this.h = f.b.f1671a;
        } else if (cVar.a().equals("ctcc_cailing")) {
            this.h = f.b.ct;
        } else if (cVar.a().equals("cucc_cailing")) {
            this.h = f.b.cu;
        }
        d();
    }
}
