package com.shoujiduoduo.ui.cailing;

import android.content.DialogInterface;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.util.ak;
import com.shoujiduoduo.util.as;
import com.shoujiduoduo.util.c.b;
import com.shoujiduoduo.util.f;

/* compiled from: CailingListAdapter */
class w implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ v f1102a;

    w(v vVar) {
        this.f1102a = vVar;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        a.a("CailingListAdapter", "delete cailing");
        this.f1102a.f1101a.a("正在删除...");
        RingData b = this.f1102a.f1101a.c.a(this.f1102a.f1101a.f);
        PlayerService b2 = ak.a().b();
        if (b2 != null) {
            b2.e();
        }
        if (this.f1102a.f1101a.h == f.b.f1671a) {
            b.a().g(b.n, this.f1102a.f1101a.q);
        } else if (this.f1102a.f1101a.h == f.b.ct) {
            com.shoujiduoduo.util.d.b.a().e(as.a(RingDDApp.c(), "pref_phone_num", ""), b.s, this.f1102a.f1101a.q);
        } else if (this.f1102a.f1101a.h == f.b.cu) {
            com.shoujiduoduo.util.e.a.a().g(b.A, this.f1102a.f1101a.q);
        }
    }
}
