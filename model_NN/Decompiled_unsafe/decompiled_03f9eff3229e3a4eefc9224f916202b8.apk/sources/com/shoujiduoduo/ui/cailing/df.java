package com.shoujiduoduo.ui.cailing;

import android.app.ProgressDialog;

/* compiled from: SmsAuthDialog */
class df implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f1016a;
    final /* synthetic */ cw b;

    df(cw cwVar, String str) {
        this.b = cwVar;
        this.f1016a = str;
    }

    public void run() {
        if (this.b.f == null) {
            ProgressDialog unused = this.b.f = new ProgressDialog(this.b.k);
            this.b.f.setMessage(this.f1016a);
            this.b.f.setIndeterminate(false);
            this.b.f.setCancelable(false);
            this.b.f.show();
        }
    }
}
