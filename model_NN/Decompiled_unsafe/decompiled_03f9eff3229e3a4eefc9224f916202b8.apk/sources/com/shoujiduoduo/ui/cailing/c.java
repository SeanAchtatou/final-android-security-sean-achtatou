package com.shoujiduoduo.ui.cailing;

import android.app.AlertDialog;
import android.content.DialogInterface;
import com.cmsc.cmmusic.common.data.GetUserInfoRsp;
import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;
import com.umeng.analytics.b;

/* compiled from: BuyCailingDialog */
class c extends a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f984a;

    c(a aVar) {
        this.f984a = aVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.ui.cailing.a.a(com.shoujiduoduo.ui.cailing.a, boolean):void
     arg types: [com.shoujiduoduo.ui.cailing.a, int]
     candidates:
      com.shoujiduoduo.ui.cailing.a.a(com.shoujiduoduo.ui.cailing.a, android.app.ProgressDialog):android.app.ProgressDialog
      com.shoujiduoduo.ui.cailing.a.a(com.shoujiduoduo.ui.cailing.a, com.cmsc.cmmusic.common.data.Result):void
      com.shoujiduoduo.ui.cailing.a.a(com.shoujiduoduo.ui.cailing.a, java.lang.String):void
      com.shoujiduoduo.ui.cailing.a.a(com.shoujiduoduo.ui.cailing.a, boolean):void */
    public void a(c.b bVar) {
        this.f984a.a();
        this.f984a.a(true);
        this.f984a.dismiss();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.ui.cailing.a.a(com.shoujiduoduo.ui.cailing.a, boolean):void
     arg types: [com.shoujiduoduo.ui.cailing.a, int]
     candidates:
      com.shoujiduoduo.ui.cailing.a.a(com.shoujiduoduo.ui.cailing.a, android.app.ProgressDialog):android.app.ProgressDialog
      com.shoujiduoduo.ui.cailing.a.a(com.shoujiduoduo.ui.cailing.a, com.cmsc.cmmusic.common.data.Result):void
      com.shoujiduoduo.ui.cailing.a.a(com.shoujiduoduo.ui.cailing.a, java.lang.String):void
      com.shoujiduoduo.ui.cailing.a.a(com.shoujiduoduo.ui.cailing.a, boolean):void */
    public void b(c.b bVar) {
        this.f984a.a();
        if (bVar.a().equals(GetUserInfoRsp.NON_MEM_ERROR_CODE)) {
            try {
                new AlertDialog.Builder(this.f984a.b).setTitle("订购彩铃").setMessage("对不起，您还未开通中移动彩铃功能，是否立即开通(5元每月)并订购该彩铃？").setPositiveButton("是", new d(this)).setNegativeButton("否", (DialogInterface.OnClickListener) null).show();
            } catch (Exception e) {
                b.a(this.f984a.b, "AlertDialog Failed!");
            }
        } else if (bVar.a().equals("302011")) {
            this.f984a.a(false);
            this.f984a.dismiss();
        } else {
            String b = bVar.b();
            if (bVar.a().equals("303023")) {
                b = "中国移动提醒您，您的彩铃库已达到上限，建议您去\"彩铃管理\"清理部分不用的彩铃后，再重新购买。";
            }
            try {
                new AlertDialog.Builder(this.f984a.b).setTitle("订购彩铃").setMessage(b).setPositiveButton("确认", (DialogInterface.OnClickListener) null).show();
            } catch (Exception e2) {
                b.a(this.f984a.b, "AlertDialog Failed!");
            }
        }
    }
}
