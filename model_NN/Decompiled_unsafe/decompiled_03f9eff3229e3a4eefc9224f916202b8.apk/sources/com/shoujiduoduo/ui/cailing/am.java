package com.shoujiduoduo.ui.cailing;

import android.view.View;
import android.widget.AdapterView;
import java.util.Map;

/* compiled from: CailingMenu */
class am implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ak f941a;

    am(ak akVar) {
        this.f941a = akVar;
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        String str = (String) ((Map) adapterView.getItemAtPosition(i)).get("TEXT");
        if (str.equals("订购彩铃")) {
            this.f941a.j();
        } else if (str.equals("赠送彩铃")) {
            this.f941a.k();
        } else if (str.equals("管理彩铃")) {
            this.f941a.i();
        }
        this.f941a.dismiss();
    }
}
