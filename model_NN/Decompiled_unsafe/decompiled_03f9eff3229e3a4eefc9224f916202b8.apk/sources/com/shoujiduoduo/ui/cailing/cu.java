package com.shoujiduoduo.ui.cailing;

import android.content.DialogInterface;
import com.shoujiduoduo.ui.cailing.ck;
import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.widget.a;

/* compiled from: MemberOpenDialog */
class cu extends a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ck f1003a;

    cu(ck ckVar) {
        this.f1003a = ckVar;
    }

    public void a(c.b bVar) {
        super.a(bVar);
        this.f1003a.a();
        new a.C0034a(this.f1003a.b).b("开通彩铃").a("开通彩铃业务已成功受理，正在为您开通，稍候会短信通知结果").a("确认", (DialogInterface.OnClickListener) null).a().show();
        if (this.f1003a.k != null) {
            this.f1003a.k.a(ck.a.C0025a.open);
        }
        this.f1003a.dismiss();
    }

    public void b(c.b bVar) {
        super.b(bVar);
        this.f1003a.a();
        if (bVar.a().equals("0002") || bVar.a().equals("9028") || bVar.a().equals("0764") || bVar.a().equals("02000000")) {
            new a.C0034a(this.f1003a.b).b("开通彩铃").a("开通彩铃基础业务已成功受理，正在为您开通，稍候会短信通知结果").a("确认", (DialogInterface.OnClickListener) null).a().show();
            if (this.f1003a.k != null) {
                this.f1003a.k.a(ck.a.C0025a.waiting);
            }
        } else if (bVar.a().equals("0501")) {
            new a.C0034a(this.f1003a.b).b("开通彩铃").a("该手机号的彩铃业务已经是开通状态").a("确认", (DialogInterface.OnClickListener) null).a().show();
            if (this.f1003a.k != null) {
                this.f1003a.k.a(ck.a.C0025a.open);
            }
        } else {
            new a.C0034a(this.f1003a.b).b("开通彩铃").a(bVar.b()).a("确认", (DialogInterface.OnClickListener) null).a().show();
            if (this.f1003a.k != null) {
                this.f1003a.k.a(ck.a.C0025a.close);
            }
        }
        this.f1003a.dismiss();
    }
}
