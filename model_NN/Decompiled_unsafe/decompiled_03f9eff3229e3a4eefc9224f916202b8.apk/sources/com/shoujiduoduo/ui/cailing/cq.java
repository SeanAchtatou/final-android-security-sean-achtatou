package com.shoujiduoduo.ui.cailing;

import android.app.AlertDialog;
import android.content.DialogInterface;
import com.shoujiduoduo.a.a.b;
import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.ui.cailing.ck;
import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;

/* compiled from: MemberOpenDialog */
class cq extends a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ck f1001a;

    cq(ck ckVar) {
        this.f1001a = ckVar;
    }

    public void a(c.b bVar) {
        this.f1001a.a();
        this.f1001a.dismiss();
        new AlertDialog.Builder(this.f1001a.b).setTitle("开通彩铃").setMessage("彩铃业务已成功申请开通,稍候收到短信即可正常使用").setPositiveButton("确认", (DialogInterface.OnClickListener) null).show();
        if (this.f1001a.k != null) {
            this.f1001a.k.a(ck.a.C0025a.open);
        }
        x.a().b(b.OBSERVER_CAILING, new cr(this));
    }

    public void b(c.b bVar) {
        this.f1001a.a();
        this.f1001a.dismiss();
        if (bVar.a().equals("000001")) {
            new AlertDialog.Builder(this.f1001a.b).setTitle("开通彩铃").setMessage("彩铃业务已成功申请开通,稍候收到短信即可正常使用").setPositiveButton("确认", (DialogInterface.OnClickListener) null).show();
            if (this.f1001a.k != null) {
                this.f1001a.k.a(ck.a.C0025a.waiting);
            }
            x.a().b(b.OBSERVER_CAILING, new cs(this));
            return;
        }
        new AlertDialog.Builder(this.f1001a.b).setTitle("开通彩铃").setMessage(bVar.b()).setPositiveButton("确认", (DialogInterface.OnClickListener) null).show();
        if (this.f1001a.k != null) {
            this.f1001a.k.a(ck.a.C0025a.close);
        }
    }
}
