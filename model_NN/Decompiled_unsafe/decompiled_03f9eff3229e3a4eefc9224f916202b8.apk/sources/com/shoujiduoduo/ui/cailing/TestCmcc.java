package com.shoujiduoduo.ui.cailing;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.EditText;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ui.utils.BaseActivity;
import com.shoujiduoduo.util.at;
import com.shoujiduoduo.util.av;
import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.c.b;
import com.shoujiduoduo.util.f;
import com.shoujiduoduo.util.widget.f;

public class TestCmcc extends BaseActivity implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private String f922a = "810027210086";
    private EditText b;
    /* access modifiers changed from: private */
    public EditText c;
    private ContentObserver d;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_test_cmcc);
        this.b = (EditText) findViewById(R.id.music_id);
        this.b.setText(this.f922a);
        this.c = (EditText) findViewById(R.id.phone_num);
        findViewById(R.id.query_vip_state).setOnClickListener(this);
        findViewById(R.id.open_vip).setOnClickListener(this);
        findViewById(R.id.vip_order).setOnClickListener(this);
        findViewById(R.id.close_vip).setOnClickListener(this);
        findViewById(R.id.check_sdk_init_status).setOnClickListener(this);
        findViewById(R.id.get_sms_code).setOnClickListener(this);
        findViewById(R.id.check_base_cailing_status).setOnClickListener(this);
        findViewById(R.id.query_caililng_and_vip).setOnClickListener(this);
        findViewById(R.id.query_cailing_url).setOnClickListener(this);
        findViewById(R.id.query_ring_box).setOnClickListener(this);
        findViewById(R.id.query_default_ring).setOnClickListener(this);
        findViewById(R.id.phone_sms_init).setOnClickListener(this);
        this.d = new at(this, new Handler(), (EditText) findViewById(R.id.random_key), "10658830");
        getContentResolver().registerContentObserver(Uri.parse("content://sms/"), true, this.d);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.util.c.b.b(java.lang.String, com.shoujiduoduo.util.b.a, boolean):void
     arg types: [java.lang.String, com.shoujiduoduo.ui.cailing.dm, int]
     candidates:
      com.shoujiduoduo.util.c.b.b(java.lang.String, java.lang.String, com.shoujiduoduo.util.b.a):void
      com.shoujiduoduo.util.c.b.b(java.lang.String, com.shoujiduoduo.util.b.a, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.util.c.b.a(java.lang.String, com.shoujiduoduo.util.b.a, boolean):void
     arg types: [java.lang.String, com.shoujiduoduo.ui.cailing.dr, int]
     candidates:
      com.shoujiduoduo.util.c.b.a(com.shoujiduoduo.util.b.c$b, java.lang.String, java.lang.String):void
      com.shoujiduoduo.util.c.b.a(java.lang.String, com.shoujiduoduo.util.b.c$b, java.lang.String):void
      com.shoujiduoduo.util.c.b.a(java.lang.String, java.lang.String, java.lang.String):void
      com.shoujiduoduo.util.c.b.a(java.lang.String, java.lang.String, com.shoujiduoduo.util.b.a):void
      com.shoujiduoduo.util.c.b.a(java.lang.String, com.shoujiduoduo.util.b.a, boolean):void */
    public void onClick(View view) {
        String obj = this.c.getText().toString();
        if (av.c(obj)) {
            f.a("请输入查询的手机号");
            return;
        }
        switch (view.getId()) {
            case R.id.phone_sms_init:
                a(obj, f.b.f1671a);
                return;
            case R.id.check_sdk_init_status:
                b.a().a(new dh(this));
                return;
            case R.id.check_base_cailing_status:
                b.a().b(this.c.getText().toString(), (a) new dm(this), true);
                return;
            case R.id.query_vip_state:
                b.a().d(this.c.getText().toString(), new dl(this));
                return;
            case R.id.query_caililng_and_vip:
                b.a().e(this.c.getText().toString(), new dn(this));
                return;
            case R.id.vip_order:
            case R.id.get_sms_code:
            case R.id.query_cailing_url:
            default:
                return;
            case R.id.open_vip:
                new AlertDialog.Builder(this).setMessage("确定开通包月吗？").setPositiveButton("确定", new Cdo(this)).setNegativeButton("取消", (DialogInterface.OnClickListener) null).show();
                return;
            case R.id.close_vip:
                new AlertDialog.Builder(this).setMessage("确定关闭包月吗？").setPositiveButton("确定", new dj(this)).setNegativeButton("取消", (DialogInterface.OnClickListener) null).show();
                return;
            case R.id.query_ring_box:
                b.a().g(new dq(this));
                return;
            case R.id.query_default_ring:
                b.a().a(obj, (a) new dr(this), false);
                return;
        }
    }

    private void a(String str, f.b bVar) {
        new cb(this, R.style.DuoDuoDialog, str, bVar, new ds(this)).show();
    }

    /* access modifiers changed from: private */
    public void a(c.b bVar) {
        new AlertDialog.Builder(this).setMessage(bVar.toString()).setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
    }

    /* access modifiers changed from: private */
    public void a(c.b bVar, String str) {
        new AlertDialog.Builder(this).setMessage(bVar.toString() + " , " + str).setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
    }
}
