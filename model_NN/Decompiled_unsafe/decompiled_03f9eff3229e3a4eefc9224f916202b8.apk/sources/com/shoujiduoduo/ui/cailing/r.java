package com.shoujiduoduo.ui.cailing;

import com.shoujiduoduo.a.c.n;
import com.shoujiduoduo.base.a.a;

/* compiled from: CailingListAdapter */
class r implements n {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ q f1097a;

    r(q qVar) {
        this.f1097a = qVar;
    }

    public void a(String str, int i) {
        if (this.f1097a.c != null && this.f1097a.c.a().equals(str)) {
            a.a("CailingListAdapter", "onSetPlay, listid:" + str);
            if (str.equals(this.f1097a.c.a())) {
                this.f1097a.f1095a = true;
                int unused = this.f1097a.f = i;
            } else {
                this.f1097a.f1095a = false;
            }
            this.f1097a.notifyDataSetChanged();
        }
    }

    public void b(String str, int i) {
        if (this.f1097a.c != null && this.f1097a.c.a().equals(str)) {
            a.a("CailingListAdapter", "onCanclePlay, listId:" + str);
            this.f1097a.f1095a = false;
            int unused = this.f1097a.f = i;
            this.f1097a.notifyDataSetChanged();
        }
    }

    public void a(String str, int i, int i2) {
        if (this.f1097a.c != null && this.f1097a.c.a().equals(str)) {
            a.a("CailingListAdapter", "onStatusChange, listid:" + str);
            this.f1097a.notifyDataSetChanged();
        }
    }
}
