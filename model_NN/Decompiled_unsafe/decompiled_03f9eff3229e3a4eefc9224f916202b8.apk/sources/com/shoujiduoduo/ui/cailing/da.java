package com.shoujiduoduo.ui.cailing;

import android.view.View;
import android.widget.Toast;
import com.shoujiduoduo.util.as;
import com.shoujiduoduo.util.f;

/* compiled from: SmsAuthDialog */
class da implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ cw f1011a;

    da(cw cwVar) {
        this.f1011a = cwVar;
    }

    public void onClick(View view) {
        String obj = this.f1011a.f1005a.getText().toString();
        String obj2 = this.f1011a.b.getText().toString();
        if (obj == null || !f.f(obj)) {
            this.f1011a.f1005a.setError("请输入正确的手机号");
        } else if (obj2 == null || obj2.length() != 6) {
            this.f1011a.b.setError("请输入正确的验证码");
        } else {
            as.c(this.f1011a.getContext(), "pref_phone_num", obj);
            if (this.f1011a.n == f.b.f1671a) {
                this.f1011a.a("请稍候...");
                this.f1011a.a(obj, obj2);
            } else if (this.f1011a.n == f.b.ct) {
                Toast.makeText(this.f1011a.getContext(), "注意：验证码十分钟内有效,过期会重新获取！", 0).show();
            }
        }
    }
}
