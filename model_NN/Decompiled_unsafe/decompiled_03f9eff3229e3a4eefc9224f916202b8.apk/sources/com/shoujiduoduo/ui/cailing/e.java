package com.shoujiduoduo.ui.cailing;

import android.app.AlertDialog;
import android.content.DialogInterface;
import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;
import com.umeng.analytics.b;

/* compiled from: BuyCailingDialog */
class e extends a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ d f1036a;

    e(d dVar) {
        this.f1036a = dVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.ui.cailing.a.a(com.shoujiduoduo.ui.cailing.a, boolean):void
     arg types: [com.shoujiduoduo.ui.cailing.a, int]
     candidates:
      com.shoujiduoduo.ui.cailing.a.a(com.shoujiduoduo.ui.cailing.a, android.app.ProgressDialog):android.app.ProgressDialog
      com.shoujiduoduo.ui.cailing.a.a(com.shoujiduoduo.ui.cailing.a, com.cmsc.cmmusic.common.data.Result):void
      com.shoujiduoduo.ui.cailing.a.a(com.shoujiduoduo.ui.cailing.a, java.lang.String):void
      com.shoujiduoduo.ui.cailing.a.a(com.shoujiduoduo.ui.cailing.a, boolean):void */
    public void a(c.b bVar) {
        super.a(bVar);
        this.f1036a.f1010a.f984a.a(true);
        this.f1036a.f1010a.f984a.dismiss();
    }

    public void b(c.b bVar) {
        super.b(bVar);
        try {
            new AlertDialog.Builder(this.f1036a.f1010a.f984a.b).setTitle("订购彩铃").setMessage(bVar.b()).setPositiveButton("确认", (DialogInterface.OnClickListener) null).show();
        } catch (Exception e) {
            b.a(this.f1036a.f1010a.f984a.b, "AlertDialog Failed!");
        }
    }
}
