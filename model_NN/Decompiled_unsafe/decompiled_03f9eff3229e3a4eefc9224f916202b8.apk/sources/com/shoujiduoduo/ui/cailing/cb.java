package com.shoujiduoduo.ui.cailing;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.util.as;
import com.shoujiduoduo.util.at;
import com.shoujiduoduo.util.f;
import com.shoujiduoduo.util.w;

/* compiled from: InputPhoneNumDialog */
public class cb extends Dialog implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private EditText f986a;
    private EditText b;
    private String c;
    /* access modifiers changed from: private */
    public a d;
    /* access modifiers changed from: private */
    public Handler e;
    /* access modifiers changed from: private */
    public b f;
    /* access modifiers changed from: private */
    public Button g;
    private String h;
    /* access modifiers changed from: private */
    public String i;
    /* access modifiers changed from: private */
    public ContentObserver j;
    /* access modifiers changed from: private */
    public Context k;
    private f.b l;
    private ProgressDialog m = null;

    /* compiled from: InputPhoneNumDialog */
    public interface a {
        void a(String str);
    }

    public cb(Context context, int i2, String str, f.b bVar, a aVar) {
        super(context, i2);
        this.k = context;
        this.d = aVar;
        this.f = new b(60000, 1000);
        this.i = str;
        this.l = bVar;
        this.e = new cc(this);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.dialog_input_phone_num);
        setCanceledOnTouchOutside(false);
        this.f986a = (EditText) findViewById(R.id.et_phone_no);
        this.b = (EditText) findViewById(R.id.et_random_key);
        this.g = (Button) findViewById(R.id.reget_sms_code);
        this.g.setOnClickListener(this);
        findViewById(R.id.positiveButton).setOnClickListener(this);
        findViewById(R.id.negativeButton).setOnClickListener(this);
        this.f986a.setText(this.i);
        String str = "";
        if (this.l.equals(f.b.ct)) {
            str = "118100";
        } else if (this.l.equals(f.b.cu)) {
            str = "1065515888";
        } else if (this.l.equals(f.b.cu)) {
            str = "10658830";
        }
        this.j = new at(this.k, new Handler(), this.b, str);
        this.k.getContentResolver().registerContentObserver(Uri.parse("content://sms/"), true, this.j);
        setOnDismissListener(new cd(this));
        setOnCancelListener(new ce(this));
    }

    /* compiled from: InputPhoneNumDialog */
    private class b extends CountDownTimer {
        public b(long j, long j2) {
            super(j, j2);
        }

        public void onTick(long j) {
            cb.this.g.setClickable(false);
            cb.this.g.setText((j / 1000) + "秒");
        }

        public void onFinish() {
            cb.this.g.setClickable(true);
            cb.this.g.setText("重新获取");
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        PlayerService.a(true);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        PlayerService.a(false);
    }

    /* access modifiers changed from: private */
    public String a() {
        if (this.l.equals(f.b.f1671a)) {
            return "cm:cm_input_phone";
        }
        if (this.l.equals(f.b.cu)) {
            return "cu:cu_input_phone";
        }
        if (this.l.equals(f.b.ct)) {
            return "ct:ct_input_phone";
        }
        return "null";
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.reget_sms_code:
                this.i = this.f986a.getText().toString();
                if (!f.f(this.i)) {
                    com.shoujiduoduo.util.widget.f.a("请输入正确的手机号");
                    return;
                }
                this.f.start();
                b();
                return;
            case R.id.positiveButton:
                this.i = this.f986a.getText().toString();
                if (!f.f(this.i)) {
                    com.shoujiduoduo.util.widget.f.a("请输入正确的手机号");
                    return;
                }
                this.c = this.b.getText().toString();
                if (this.l.equals(f.b.ct)) {
                    if (TextUtils.isEmpty(this.c) || TextUtils.isEmpty(this.h) || !this.c.equals(this.h)) {
                        com.shoujiduoduo.util.widget.f.a("请输入正确的验证码");
                        return;
                    }
                    Message obtainMessage = this.e.obtainMessage();
                    obtainMessage.obj = this.i;
                    this.e.sendMessage(obtainMessage);
                    as.c(RingDDApp.c(), "pref_phone_num", this.i);
                    w.a(a(), "success", "&phone=" + this.i);
                    dismiss();
                    return;
                } else if (this.l.equals(f.b.cu)) {
                    if (TextUtils.isEmpty(this.c) || this.c.length() != 6) {
                        com.shoujiduoduo.util.widget.f.a("请输入正确的验证码");
                        return;
                    }
                    com.shoujiduoduo.base.a.a.a("InputPhoneNumDialog", "已获得手机号，发起获取用户token请求");
                    a("请稍候...");
                    com.shoujiduoduo.util.e.a.a().a(this.i, this.c, new cf(this));
                    return;
                } else if (this.l.equals(f.b.f1671a)) {
                    a("请稍候...");
                    com.shoujiduoduo.util.c.b.a().a(this.i, this.c, new cg(this));
                    return;
                } else {
                    com.shoujiduoduo.util.widget.f.a("不支持的运营商类型");
                    return;
                }
            case R.id.negativeButton:
                w.a(a(), "close", "");
                dismiss();
                return;
            default:
                return;
        }
    }

    private void b() {
        this.i = this.f986a.getText().toString();
        if (!f.f(this.i)) {
            com.shoujiduoduo.util.widget.f.a("请输入正确的手机号");
            return;
        }
        this.l = f.g(this.i);
        if (this.l == f.b.d) {
            com.shoujiduoduo.util.widget.f.a("未知的手机号类型，无法判断运营商，请确认手机号输入正确！");
            com.shoujiduoduo.base.a.a.c("InputPhoneNumDialog", "unknown phone type :" + this.i);
        } else if (this.l.equals(f.b.ct)) {
            this.h = f.a(6);
            com.shoujiduoduo.base.a.a.a("InputPhoneNumDialog", "random key:" + this.h);
            com.shoujiduoduo.util.d.b.a().a(this.i, "铃声多多验证码：" + this.h + "【铃声多多，每天都有新铃声】", new ch(this));
        } else if (this.l.equals(f.b.cu)) {
            com.shoujiduoduo.util.e.a.a().c(this.i, new ci(this));
        } else if (this.l.equals(f.b.f1671a)) {
            com.shoujiduoduo.util.c.b.a().a(this.i, new cj(this));
        } else {
            com.shoujiduoduo.base.a.a.c("InputPhoneNumDialog", "unsupport phone type");
        }
    }

    private void a(String str) {
        if (this.m == null) {
            this.m = new ProgressDialog(this.k);
            this.m.setMessage(str);
            this.m.setIndeterminate(false);
            this.m.setCancelable(true);
            this.m.setCanceledOnTouchOutside(false);
            this.m.show();
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        if (this.m != null) {
            this.m.dismiss();
            this.m = null;
        }
    }
}
