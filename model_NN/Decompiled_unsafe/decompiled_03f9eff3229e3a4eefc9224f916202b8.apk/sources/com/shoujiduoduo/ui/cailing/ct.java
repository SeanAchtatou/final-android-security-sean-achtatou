package com.shoujiduoduo.ui.cailing;

import android.content.DialogInterface;
import com.shoujiduoduo.ui.cailing.ck;
import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.widget.a;

/* compiled from: MemberOpenDialog */
class ct extends a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ck f1002a;

    ct(ck ckVar) {
        this.f1002a = ckVar;
    }

    public void a(c.b bVar) {
        super.a(bVar);
        this.f1002a.a();
        new a.C0034a(this.f1002a.b).b("开通彩铃").a("开通彩铃业务已成功受理，正在为您开通，稍候会短信通知结果").a("确认", (DialogInterface.OnClickListener) null).a().show();
        if (this.f1002a.k != null) {
            this.f1002a.k.a(ck.a.C0025a.open);
        }
        this.f1002a.dismiss();
    }

    public void b(c.b bVar) {
        super.b(bVar);
        this.f1002a.a();
        if (bVar.a().equals("000001") || bVar.a().equals("301000")) {
            new a.C0034a(this.f1002a.b).b("开通彩铃").a("开通彩铃业务已成功受理，正在为您开通，稍候会短信通知结果").a("确认", (DialogInterface.OnClickListener) null).a().show();
            if (this.f1002a.k != null) {
                this.f1002a.k.a(ck.a.C0025a.open);
            }
        } else {
            new a.C0034a(this.f1002a.b).b("开通彩铃").a(bVar.b()).a("确认", (DialogInterface.OnClickListener) null).a().show();
            if (this.f1002a.k != null) {
                this.f1002a.k.a(ck.a.C0025a.close);
            }
        }
        this.f1002a.dismiss();
    }
}
