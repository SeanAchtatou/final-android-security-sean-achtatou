package com.shoujiduoduo.ui.cailing;

import android.app.AlertDialog;
import android.content.DialogInterface;
import com.cmsc.cmmusic.common.RingbackManagerInterface;
import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;

/* compiled from: BuyCailingDialog */
class o extends a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ n f1093a;

    o(n nVar) {
        this.f1093a = nVar;
    }

    public void a(c.b bVar) {
        super.a(bVar);
        com.shoujiduoduo.base.a.a.a(a.f925a, "getringbadkPolicy success");
        c.j jVar = (c.j) bVar;
        com.shoujiduoduo.base.a.a.a(a.f925a, "bizCode:" + jVar.f1603a.getBizCode() + ",bizType:" + jVar.f1603a.getBizType() + ", salePrice:" + jVar.f1603a.getSalePrice() + ", monLevel:" + jVar.f + ", hode2:" + jVar.f1603a.getHold2());
        RingbackManagerInterface.buyRingback(this.f1093a.f1092a.b, this.f1093a.f1092a.f.n, jVar.f1603a.getBizCode(), jVar.f1603a.getBizType(), jVar.f1603a.getSalePrice(), jVar.f, jVar.f1603a.getHold2(), new p(this));
    }

    public void b(c.b bVar) {
        super.b(bVar);
        this.f1093a.f1092a.a();
        new AlertDialog.Builder(this.f1093a.f1092a.b).setTitle("订购彩铃").setMessage("订购失败，原因：" + bVar.toString()).setPositiveButton("确认", (DialogInterface.OnClickListener) null).show();
    }
}
