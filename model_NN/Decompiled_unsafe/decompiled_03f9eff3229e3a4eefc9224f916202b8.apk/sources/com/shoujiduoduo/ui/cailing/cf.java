package com.shoujiduoduo.ui.cailing;

import android.content.DialogInterface;
import android.os.Message;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.util.as;
import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.w;
import com.shoujiduoduo.util.widget.a;

/* compiled from: InputPhoneNumDialog */
class cf extends a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ cb f991a;

    cf(cb cbVar) {
        this.f991a = cbVar;
    }

    public void a(c.b bVar) {
        super.a(bVar);
        this.f991a.c();
        if (bVar == null || !(bVar instanceof c.a)) {
            new a.C0034a(this.f991a.k).a("输入验证码不对，请重试！").a("确定", (DialogInterface.OnClickListener) null).a().show();
            return;
        }
        c.a aVar = (c.a) bVar;
        com.shoujiduoduo.base.a.a.a("InputPhoneNumDialog", "token:" + aVar.f1586a);
        com.shoujiduoduo.util.e.a.a().a(this.f991a.i, aVar.f1586a);
        Message obtainMessage = this.f991a.e.obtainMessage();
        obtainMessage.obj = this.f991a.i;
        this.f991a.e.sendMessage(obtainMessage);
        as.c(RingDDApp.c(), "pref_phone_num", this.f991a.i);
        w.a(this.f991a.a(), "success", "&phone=" + this.f991a.i);
        this.f991a.dismiss();
    }

    public void b(c.b bVar) {
        super.b(bVar);
        this.f991a.c();
        new a.C0034a(this.f991a.k).a("输入验证码不对，请重试！").a("确定", (DialogInterface.OnClickListener) null).a().show();
        com.shoujiduoduo.base.a.a.c("InputPhoneNumDialog", "获取token失败");
    }
}
