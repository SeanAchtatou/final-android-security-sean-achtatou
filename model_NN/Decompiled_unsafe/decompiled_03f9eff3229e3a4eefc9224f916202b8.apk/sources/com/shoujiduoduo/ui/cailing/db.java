package com.shoujiduoduo.ui.cailing;

import android.view.View;
import android.widget.Toast;
import com.shoujiduoduo.util.f;

/* compiled from: SmsAuthDialog */
class db implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ cw f1012a;

    db(cw cwVar) {
        this.f1012a = cwVar;
    }

    public void onClick(View view) {
        String obj = this.f1012a.f1005a.getText().toString();
        if (obj == null || !f.f(this.f1012a.f1005a.getText().toString())) {
            Toast.makeText(this.f1012a.k, "请输入正确的手机号", 1).show();
            return;
        }
        this.f1012a.a("请稍候...");
        if (this.f1012a.n == f.b.f1671a) {
            this.f1012a.c(obj);
        } else if (this.f1012a.n == f.b.ct) {
            this.f1012a.d(obj);
        }
    }
}
