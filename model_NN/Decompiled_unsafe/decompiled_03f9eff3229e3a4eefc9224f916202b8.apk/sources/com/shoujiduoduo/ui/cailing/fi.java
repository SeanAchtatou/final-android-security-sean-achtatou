package com.shoujiduoduo.ui.cailing;

import android.app.AlertDialog;
import android.content.DialogInterface;
import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;

/* compiled from: TestCucc */
class fi extends a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TestCucc f1070a;

    fi(TestCucc testCucc) {
        this.f1070a = testCucc;
    }

    public void a(c.b bVar) {
        if (bVar instanceof c.a) {
            c.a aVar = (c.a) bVar;
            com.shoujiduoduo.base.a.a.a("testcucc", "getAuthToken onSuccess:" + bVar.toString() + ", token:" + aVar.f1586a);
            new AlertDialog.Builder(this.f1070a).setMessage(bVar.toString() + ", token:" + aVar.f1586a).setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
        } else {
            new AlertDialog.Builder(this.f1070a).setMessage("返回结果不对").setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
        }
        super.a(bVar);
    }

    public void b(c.b bVar) {
        com.shoujiduoduo.base.a.a.a("testcucc", "getAuthToken onFailure:" + bVar.toString());
        new AlertDialog.Builder(this.f1070a).setMessage(bVar.toString()).setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
        super.b(bVar);
    }
}
