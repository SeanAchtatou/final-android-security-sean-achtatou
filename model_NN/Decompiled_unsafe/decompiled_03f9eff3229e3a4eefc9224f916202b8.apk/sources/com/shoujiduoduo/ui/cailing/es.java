package com.shoujiduoduo.ui.cailing;

import android.app.AlertDialog;
import android.content.DialogInterface;
import com.shoujiduoduo.a.b.b;
import com.shoujiduoduo.base.bean.ab;
import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;
import com.sina.weibo.sdk.exception.WeiboAuthException;

/* compiled from: TestCtcc */
class es extends a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TestCtcc f1054a;

    es(TestCtcc testCtcc) {
        this.f1054a = testCtcc;
    }

    public void a(c.b bVar) {
        super.a(bVar);
        com.shoujiduoduo.base.a.a.a("testctcc", new StringBuilder().append("query vip onSuccess:").append(bVar).toString() != null ? bVar.toString() : "result is null");
        if (bVar != null && (bVar instanceof c.t)) {
            c.t tVar = (c.t) bVar;
            com.shoujiduoduo.base.a.a.a("testctcc", "code:" + tVar.a() + " msg:" + tVar.b());
            ab c = b.g().c();
            if (tVar.d) {
                c.b(2);
                new AlertDialog.Builder(this.f1054a).setMessage("开通状态").setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
                return;
            }
            c.b(0);
            new AlertDialog.Builder(this.f1054a).setMessage("未开通状态， status：" + WeiboAuthException.DEFAULT_AUTH_ERROR_CODE).setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
        }
    }

    public void b(c.b bVar) {
        com.shoujiduoduo.base.a.a.a("testctcc", "query vip onFailure:" + bVar.toString());
        new AlertDialog.Builder(this.f1054a).setMessage(bVar.toString()).setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
        super.b(bVar);
    }
}
