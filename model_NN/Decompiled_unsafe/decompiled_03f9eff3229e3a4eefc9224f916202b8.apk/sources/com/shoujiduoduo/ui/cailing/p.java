package com.shoujiduoduo.ui.cailing;

import android.os.Message;
import com.cmsc.cmmusic.common.CMMusicCallback;
import com.cmsc.cmmusic.common.data.OrderResult;
import com.shoujiduoduo.base.a.a;

/* compiled from: BuyCailingDialog */
class p implements CMMusicCallback<OrderResult> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ o f1094a;

    p(o oVar) {
        this.f1094a = oVar;
    }

    /* renamed from: a */
    public void operationResult(OrderResult orderResult) {
        a.a(a.f925a, "RingbackManagerInterface.buyRingback, operationResult, threadId:" + Thread.currentThread().getId());
        this.f1094a.f1093a.f1092a.a();
        Message message = new Message();
        message.what = 111;
        message.obj = orderResult;
        this.f1094a.f1093a.f1092a.o.sendMessage(message);
    }
}
