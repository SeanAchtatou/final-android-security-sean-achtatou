package com.shoujiduoduo.ui.cailing;

import android.app.AlertDialog;
import android.content.DialogInterface;
import com.cmsc.cmmusic.common.data.GetUserInfoRsp;
import com.shoujiduoduo.ui.cailing.GiveCailingActivity;
import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.w;
import com.umeng.analytics.b;

/* compiled from: GiveCailingActivity */
class bk extends a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GiveCailingActivity f968a;

    bk(GiveCailingActivity giveCailingActivity) {
        this.f968a = giveCailingActivity;
    }

    public void a(c.b bVar) {
        this.f968a.a();
        new AlertDialog.Builder(this.f968a).setTitle("赠送彩铃").setMessage("赠送彩铃成功，谢谢使用").setPositiveButton("确认", new GiveCailingActivity.a(this.f968a, null)).show();
    }

    public void b(c.b bVar) {
        this.f968a.a();
        bVar.b();
        if (bVar.a().equals(GetUserInfoRsp.NON_MEM_ERROR_CODE)) {
            try {
                new AlertDialog.Builder(this.f968a).setTitle("赠送彩铃").setMessage("对不起，您还未开通中移动彩铃功能，是否立即开通？").setPositiveButton("是", new bl(this)).setNegativeButton("否", (DialogInterface.OnClickListener) null).show();
            } catch (Exception e) {
                b.a(this.f968a, "AlertDialog Failed!");
            }
        } else {
            if (bVar.a().equals("999002")) {
                int i = 0;
                try {
                    i = Integer.valueOf(this.f968a.i.g).intValue();
                } catch (NumberFormatException e2) {
                    e2.printStackTrace();
                }
                w.a(i);
            }
            try {
                new AlertDialog.Builder(this.f968a).setTitle("赠送彩铃").setMessage(bVar.b()).setPositiveButton("确认", (DialogInterface.OnClickListener) null).show();
            } catch (Exception e3) {
                b.a(this.f968a, "AlertDialog Failed!");
            }
        }
    }
}
