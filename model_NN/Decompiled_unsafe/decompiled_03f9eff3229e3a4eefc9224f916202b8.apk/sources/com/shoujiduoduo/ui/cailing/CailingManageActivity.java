package com.shoujiduoduo.ui.cailing;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.widget.Button;
import android.widget.TextView;
import com.shoujiduoduo.a.a.b;
import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.a.c.c;
import com.shoujiduoduo.b.c.n;
import com.shoujiduoduo.base.bean.f;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ui.utils.BaseFragmentActivity;
import com.shoujiduoduo.ui.utils.DDListFragment;
import com.shoujiduoduo.util.ak;
import com.shoujiduoduo.util.f;

public class CailingManageActivity extends BaseFragmentActivity {

    /* renamed from: a  reason: collision with root package name */
    private static final String f916a = CailingManageActivity.class.getSimpleName();
    private Button b = null;
    private TextView c = null;
    private DDListFragment d;
    /* access modifiers changed from: private */
    public f.b e;
    private c f = new ah(this);
    /* access modifiers changed from: private */
    public DialogInterface.OnClickListener g = new ai(this);
    /* access modifiers changed from: private */
    public DialogInterface.OnClickListener h = new aj(this);

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.layout_cailing_manage);
        this.b = (Button) findViewById(R.id.cailing_manage_back);
        this.b.setOnClickListener(new ag(this));
        x.a().a(b.OBSERVER_CAILING, this.f);
        this.d = new DDListFragment();
        this.c = (TextView) findViewById(R.id.cailing_bottom_tips);
        n nVar = null;
        if (f.v()) {
            this.e = f.b.f1671a;
            nVar = new n(f.a.list_ring_cmcc, "", false, "");
            this.c.setText((int) R.string.cmcc_manage_hint);
        } else if (com.shoujiduoduo.util.f.x()) {
            this.e = f.b.ct;
            nVar = new n(f.a.list_ring_ctcc, "", false, "");
            this.c.setText((int) R.string.ctcc_manage_hint);
        } else if (com.shoujiduoduo.util.f.w()) {
            this.e = f.b.cu;
            nVar = new n(f.a.list_ring_cucc, "", false, "");
            this.c.setText((int) R.string.cucc_manage_hint);
        }
        Bundle bundle2 = new Bundle();
        bundle2.putString("adapter_type", "cailing_list_adapter");
        this.d.setArguments(bundle2);
        this.d.a(nVar);
        FragmentTransaction beginTransaction = getSupportFragmentManager().beginTransaction();
        beginTransaction.add((int) R.id.list_content, this.d);
        beginTransaction.commit();
        PlayerService b2 = ak.a().b();
        if (b2 != null && b2.j()) {
            b2.k();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        x.a().b(b.OBSERVER_CAILING, this.f);
    }

    /* access modifiers changed from: private */
    public void a() {
        setResult(0);
        PlayerService b2 = ak.a().b();
        if (b2 != null && b2.j()) {
            b2.k();
        }
        finish();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 4) {
            a();
        }
        return super.onKeyDown(i, keyEvent);
    }
}
