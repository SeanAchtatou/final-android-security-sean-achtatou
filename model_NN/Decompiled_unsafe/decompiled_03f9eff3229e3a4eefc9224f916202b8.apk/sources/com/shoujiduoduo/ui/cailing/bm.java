package com.shoujiduoduo.ui.cailing;

import android.app.ProgressDialog;

/* compiled from: GiveCailingActivity */
class bm implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f970a;
    final /* synthetic */ GiveCailingActivity b;

    bm(GiveCailingActivity giveCailingActivity, String str) {
        this.b = giveCailingActivity;
        this.f970a = str;
    }

    public void run() {
        if (this.b.t == null) {
            ProgressDialog unused = this.b.t = new ProgressDialog(this.b);
            this.b.t.setMessage(this.f970a);
            this.b.t.setIndeterminate(false);
            this.b.t.setCancelable(false);
            this.b.t.show();
        }
    }
}
