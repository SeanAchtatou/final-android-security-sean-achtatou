package com.shoujiduoduo.ui.cailing;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.util.as;
import com.shoujiduoduo.util.at;
import com.shoujiduoduo.util.c.b;
import com.shoujiduoduo.util.f;

/* compiled from: SmsAuthDialog */
public class cw extends Dialog {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public EditText f1005a;
    /* access modifiers changed from: private */
    public EditText b;
    private ImageButton c;
    private Button d;
    private ImageButton e;
    /* access modifiers changed from: private */
    public ProgressDialog f = null;
    private TextView g;
    private TextView h;
    private String i;
    /* access modifiers changed from: private */
    public ContentObserver j;
    /* access modifiers changed from: private */
    public Context k;
    /* access modifiers changed from: private */
    public Handler l;
    private ImageView m;
    /* access modifiers changed from: private */
    public f.b n;

    public cw(Context context, int i2, Handler handler, f.b bVar) {
        super(context, i2);
        this.k = context;
        this.l = handler;
        this.n = bVar;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        String str;
        String str2;
        super.onCreate(bundle);
        a.a("SmsAuthDialog", "onCreate");
        setContentView((int) R.layout.dialog_sms_auth);
        setOnDismissListener(new cx(this));
        if (this.n == f.b.f1671a) {
            this.i = this.k.getResources().getString(R.string.cmcc_sms_auth_dialog_title);
            str = this.k.getResources().getString(R.string.cmcc_auth_hint);
            str2 = "10658830";
        } else if (this.n == f.b.ct) {
            this.i = this.k.getResources().getString(R.string.ctcc_sms_auth_dialog_title);
            str = this.k.getResources().getString(R.string.cmcc_auth_hint);
            str2 = "118100";
        } else {
            if (this.n == f.b.cu) {
                this.i = this.k.getResources().getString(R.string.cucc_sms_auth_dialog_title);
            }
            str = "";
            str2 = "";
        }
        this.g = (TextView) findViewById(R.id.title);
        this.g.setText(this.i);
        this.h = (TextView) findViewById(R.id.verify_ins);
        this.h.setText(str);
        this.m = (ImageView) findViewById(R.id.type_icon);
        this.f1005a = (EditText) findViewById(R.id.et_phone_no);
        String b2 = f.b();
        String a2 = as.a(getContext(), "pref_phone_num");
        if (!TextUtils.isEmpty(b2)) {
            this.f1005a.setText(b2);
        } else if (!TextUtils.isEmpty(a2)) {
            this.f1005a.setText(a2);
        }
        switch (this.n) {
            case f1671a:
                this.f1005a.setHint((int) R.string.cmcc_num);
                this.m.setImageResource(R.drawable.icon_cmcc);
                break;
            case ct:
                this.f1005a.setHint((int) R.string.ctcc_num);
                this.m.setImageResource(R.drawable.icon_ctcc);
                break;
            case cu:
                this.f1005a.setHint((int) R.string.cucc_num);
                break;
        }
        this.b = (EditText) findViewById(R.id.et_phone_code);
        this.c = (ImageButton) findViewById(R.id.btn_phone_login_close);
        this.c.setOnClickListener(new cz(this));
        this.d = (Button) findViewById(R.id.btn_phone_no_login);
        this.d.setOnClickListener(new da(this));
        this.e = (ImageButton) findViewById(R.id.btn_get_code);
        this.e.setOnClickListener(new db(this));
        this.j = new at(this.k, this.l, this.b, str2);
        this.k.getContentResolver().registerContentObserver(Uri.parse("content://sms/"), true, this.j);
    }

    /* access modifiers changed from: private */
    public void a(String str, String str2) {
        b.a().a(str, str2, new dc(this));
    }

    /* access modifiers changed from: private */
    public void c(String str) {
        b.a().a(str, new dd(this));
    }

    /* access modifiers changed from: private */
    public void d(String str) {
        com.shoujiduoduo.util.d.b.a().d(str, new de(this));
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        this.l.post(new df(this, str));
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.l.post(new dg(this));
    }

    /* access modifiers changed from: package-private */
    public void b(String str) {
        this.l.post(new cy(this, str));
    }
}
