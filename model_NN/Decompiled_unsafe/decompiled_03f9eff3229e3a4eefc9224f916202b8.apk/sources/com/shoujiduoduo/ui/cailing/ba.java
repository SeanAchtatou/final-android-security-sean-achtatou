package com.shoujiduoduo.ui.cailing;

import com.ffcs.inapppaylib.bean.response.VerifyResponse;
import com.ffcs.inapppaylib.impl.OnVCodeListener;
import com.shoujiduoduo.base.a.a;

/* compiled from: DuoduoVipDialog */
class ba implements OnVCodeListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ au f958a;

    ba(au auVar) {
        this.f958a = auVar;
    }

    public void onRefreshVcoBtnSuccess(VerifyResponse verifyResponse) {
        a.a("DuoduoVipDialog", "refresh code success");
        this.f958a.a(verifyResponse, true);
    }

    public void onRefreshVcoBtnFailure(VerifyResponse verifyResponse) {
        a.a("DuoduoVipDialog", "refresh code fail");
        this.f958a.a(verifyResponse, false);
    }
}
