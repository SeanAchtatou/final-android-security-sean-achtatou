package com.shoujiduoduo.ui.cailing;

import android.app.AlertDialog;
import android.content.DialogInterface;
import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.d.b;

/* compiled from: GiveCailingActivity */
class bx extends a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f981a;
    final /* synthetic */ String b;
    final /* synthetic */ String c;
    final /* synthetic */ GiveCailingActivity d;

    bx(GiveCailingActivity giveCailingActivity, String str, String str2, String str3) {
        this.d = giveCailingActivity;
        this.f981a = str;
        this.b = str2;
        this.c = str3;
    }

    public void a(c.b bVar) {
        super.a(bVar);
        com.shoujiduoduo.base.a.a.a(GiveCailingActivity.f919a, "本机号码已开通彩铃功能");
        b.a().g(this.f981a, new by(this));
    }

    public void b(c.b bVar) {
        super.b(bVar);
        this.d.a();
        com.shoujiduoduo.base.a.a.a(GiveCailingActivity.f919a, "本机号码没有开通彩铃功能");
        new AlertDialog.Builder(this.d).setTitle("订购彩铃").setMessage("对不起，您还未开通彩铃功能，是否立即开通？").setPositiveButton("是", new bz(this)).setNegativeButton("否", (DialogInterface.OnClickListener) null).show();
    }
}
