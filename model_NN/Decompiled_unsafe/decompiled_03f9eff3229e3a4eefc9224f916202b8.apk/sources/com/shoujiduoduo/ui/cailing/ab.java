package com.shoujiduoduo.ui.cailing;

import android.view.View;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.util.as;
import com.shoujiduoduo.util.c.b;
import com.shoujiduoduo.util.f;

/* compiled from: CailingListAdapter */
class ab implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ q f927a;

    ab(q qVar) {
        this.f927a = qVar;
    }

    public void onClick(View view) {
        a.a("CailingListAdapter", "set default cailing");
        this.f927a.a("正在设置...");
        RingData b = this.f927a.c.a(this.f927a.f);
        if (b != null) {
            if (this.f927a.h.equals(f.b.f1671a)) {
                String unused = this.f927a.d = b.n;
                b.a().f(b.n, this.f927a.b);
            } else if (this.f927a.h.equals(f.b.ct)) {
                String unused2 = this.f927a.d = b.s;
                com.shoujiduoduo.util.d.b.a().c(as.a(RingDDApp.c(), "pref_phone_num"), b.s, this.f927a.b);
            } else {
                String unused3 = this.f927a.d = b.A;
                com.shoujiduoduo.util.e.a.a().a(b.A, this.f927a.j, this.f927a.k, this.f927a.b);
            }
        }
    }
}
