package com.shoujiduoduo.ui.cailing;

import android.content.DialogInterface;

/* compiled from: GiveCailingActivity */
class bo implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String[] f972a;
    final /* synthetic */ GiveCailingActivity b;

    bo(GiveCailingActivity giveCailingActivity, String[] strArr) {
        this.b = giveCailingActivity;
        this.f972a = strArr;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        dialogInterface.dismiss();
        String str = this.f972a[i];
        if (str != null) {
            str = str.trim().replace(" ", "").replace("-", "");
        }
        this.b.e.setText(str);
    }
}
