package com.shoujiduoduo.ui.cailing;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.base.bean.f;
import com.shoujiduoduo.util.w;

/* compiled from: CailingListAdapter */
class af implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ q f930a;

    af(q qVar) {
        this.f930a = qVar;
    }

    public void onClick(View view) {
        f.a aVar;
        a.a("CailingListAdapter", "give cailing");
        RingData b = this.f930a.c.a(this.f930a.f);
        if (b != null) {
            Bundle bundle = new Bundle();
            Intent intent = new Intent(this.f930a.i, GiveCailingActivity.class);
            bundle.putParcelable("ringdata", b);
            intent.putExtras(bundle);
            intent.putExtra("listid", "cailingmanage");
            if (com.shoujiduoduo.util.f.v()) {
                intent.putExtra("operator_type", 0);
                aVar = f.a.list_ring_cmcc;
            } else if (com.shoujiduoduo.util.f.x()) {
                intent.putExtra("operator_type", 1);
                aVar = f.a.list_ring_ctcc;
            } else {
                aVar = f.a.list_ring_cmcc;
            }
            intent.putExtra("listtype", aVar.toString());
            this.f930a.i.startActivity(intent);
            w.a(this.f930a.c.a(this.f930a.f).g, 7, "cailingmanage", aVar.toString(), "&cucid=" + this.f930a.c.a(this.f930a.f).A);
        }
    }
}
