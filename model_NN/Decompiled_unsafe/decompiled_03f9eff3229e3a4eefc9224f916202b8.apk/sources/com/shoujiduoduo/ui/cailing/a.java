package com.shoujiduoduo.ui.cailing;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.cmsc.cmmusic.common.data.Result;
import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.a.b.b;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.base.bean.f;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.activity.RingToneDuoduoActivity;
import com.shoujiduoduo.util.as;
import com.shoujiduoduo.util.av;
import com.shoujiduoduo.util.f;

/* compiled from: BuyCailingDialog */
public class a extends Dialog {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final String f925a = a.class.getSimpleName();
    /* access modifiers changed from: private */
    public Context b;
    private ImageButton c;
    private Button d;
    private ListView e;
    /* access modifiers changed from: private */
    public RingData f;
    /* access modifiers changed from: private */
    public String g;
    /* access modifiers changed from: private */
    public f.a h;
    /* access modifiers changed from: private */
    public f.b i;
    /* access modifiers changed from: private */
    public EditText j;
    private EditText k;
    private ImageButton l;
    private RelativeLayout m;
    private TextView n;
    /* access modifiers changed from: private */
    public Handler o = new b(this);
    /* access modifiers changed from: private */
    public ProgressDialog p = null;

    public a(Context context, int i2, f.b bVar) {
        super(context, i2);
        this.b = context;
        this.i = bVar;
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        PlayerService.a(true);
        com.shoujiduoduo.base.a.a.a(f925a, "onStart, threadId:" + Thread.currentThread().getId());
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        PlayerService.a(false);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.dialog_buy_cailing);
        this.c = (ImageButton) findViewById(R.id.buy_cailing_close);
        this.c.setOnClickListener(new j(this));
        setCanceledOnTouchOutside(true);
        this.e = (ListView) findViewById(R.id.cailing_info_list);
        this.e.setAdapter((ListAdapter) new C0024a(this, null));
        this.e.setEnabled(false);
        this.d = (Button) findViewById(R.id.buy_cailing);
        this.d.setOnClickListener(new k(this));
        this.k = (EditText) findViewById(R.id.et_phone_code);
        this.j = (EditText) findViewById(R.id.et_phone_no);
        this.n = (TextView) findViewById(R.id.buy_cailing_tips);
        this.m = (RelativeLayout) findViewById(R.id.random_key_auth_layout);
        if (this.i == f.b.f1671a) {
            this.m.setVisibility(8);
            this.n.setText((int) R.string.buy_cailing_hint_cmcc);
        } else if (this.i == f.b.ct) {
            this.m.setVisibility(0);
            this.n.setText((int) R.string.buy_cailing_hint_ctcc);
            this.j.setHint((int) R.string.ctcc_num);
        } else {
            this.m.setVisibility(0);
            this.n.setText((int) R.string.buy_cailing_hint_cmcc);
            this.n.setVisibility(8);
            this.j.setHint((int) R.string.cmcc_num);
        }
        this.l = (ImageButton) findViewById(R.id.btn_get_code);
        this.l.setOnClickListener(new l(this));
        String k2 = b.g().c().k();
        this.j.setText(k2);
        if (!av.c(k2) && !com.shoujiduoduo.util.c.b.a().a(k2).equals("")) {
            this.l.setVisibility(8);
            this.k.setVisibility(8);
        }
    }

    /* access modifiers changed from: private */
    public void b(String str) {
        com.shoujiduoduo.util.c.b.a().a(str, new m(this));
    }

    /* access modifiers changed from: private */
    public void c() {
        a("请稍候...");
        com.shoujiduoduo.util.c.b.a().a(RingToneDuoduoActivity.a(), new n(this));
    }

    /* access modifiers changed from: private */
    public void a(Result result) {
        if (result == null || result.getResCode() == null) {
            com.shoujiduoduo.util.widget.f.a("订购没有成功！");
        } else if (result.getResCode().equals("000000")) {
            com.shoujiduoduo.base.a.a.a(f925a, "订购成功");
            a(true);
            dismiss();
        } else if (result.getResCode().equals("302011")) {
            com.shoujiduoduo.base.a.a.a(f925a, "重复订购");
            a(false);
            dismiss();
        } else {
            com.shoujiduoduo.base.a.a.a(f925a, "订购失败");
            String resMsg = result.getResMsg();
            if (result.getResCode().equals("303023")) {
                resMsg = "中国移动提醒您，您的彩铃库已达到上限，建议您去\"彩铃管理\"清理部分不用的彩铃后，再重新购买。";
            }
            if (av.c(resMsg)) {
                resMsg = "订购未成功!";
            }
            try {
                new AlertDialog.Builder(this.b).setTitle("订购彩铃").setMessage(resMsg).setPositiveButton("确认", (DialogInterface.OnClickListener) null).show();
            } catch (Exception e2) {
                com.umeng.analytics.b.a(this.b, "AlertDialog Failed!");
            }
        }
    }

    /* access modifiers changed from: private */
    public void d() {
        com.shoujiduoduo.util.c.b.a().d(this.f.n, "", new c(this));
    }

    /* access modifiers changed from: private */
    public void a(boolean z) {
        try {
            new AlertDialog.Builder(this.b).setTitle("订购彩铃").setMessage("恭喜，订购成功，已帮您设置为默认彩铃").setPositiveButton("确认", (DialogInterface.OnClickListener) null).show();
        } catch (Exception e2) {
            com.umeng.analytics.b.a(this.b, "AlertDialog Failed!");
        }
        com.shoujiduoduo.util.c.b.a().f(this.f.n, new com.shoujiduoduo.util.b.a());
        as.c(this.b, "DEFAULT_CAILING_ID", this.f.n);
        x.a().b(com.shoujiduoduo.a.a.b.OBSERVER_RING_CHANGE, new f(this));
        if (z) {
            as.b(this.b, "NeedUpdateCaiLingLib", 1);
            x.a().b(com.shoujiduoduo.a.a.b.OBSERVER_CAILING, new g(this));
        }
    }

    public void a(RingData ringData, String str, f.a aVar) {
        this.f = ringData;
        this.g = str;
        this.h = aVar;
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        this.o.post(new h(this, str));
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.o.post(new i(this));
    }

    /* renamed from: com.shoujiduoduo.ui.cailing.a$a  reason: collision with other inner class name */
    /* compiled from: BuyCailingDialog */
    private class C0024a extends BaseAdapter {
        private C0024a() {
        }

        /* synthetic */ C0024a(a aVar, b bVar) {
            this();
        }

        public int getCount() {
            return 4;
        }

        public Object getItem(int i) {
            return null;
        }

        public long getItemId(int i) {
            return (long) i;
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            String str;
            View inflate = a.this.getLayoutInflater().inflate((int) R.layout.listitem_buy_cailing, (ViewGroup) null);
            TextView textView = (TextView) inflate.findViewById(R.id.cailing_info_des);
            TextView textView2 = (TextView) inflate.findViewById(R.id.cailing_info_content);
            switch (i) {
                case 0:
                    textView.setText("歌曲名");
                    textView2.setText(a.this.f.e);
                    break;
                case 1:
                    textView.setText("歌   手");
                    textView2.setText(a.this.f.f);
                    break;
                case 2:
                    textView.setText("有效期");
                    String str2 = "";
                    if (a.this.i == f.b.f1671a) {
                        str2 = a.this.f.o;
                    } else if (a.this.i == f.b.ct) {
                        str2 = a.this.f.t;
                    }
                    if (TextUtils.isEmpty(str2)) {
                        str2 = "2017-06-30";
                    }
                    textView2.setText(str2);
                    break;
                case 3:
                    textView.setText("彩铃价格");
                    int i2 = 200;
                    if (a.this.i == f.b.f1671a) {
                        i2 = a.this.f.p;
                    } else if (a.this.i == f.b.ct) {
                        i2 = a.this.f.u;
                    }
                    if (i2 == 0) {
                        str = "免费";
                    } else {
                        str = String.valueOf(((double) ((float) i2)) / 100.0d) + "元";
                    }
                    textView2.setText(str);
                    break;
            }
            return inflate;
        }
    }
}
