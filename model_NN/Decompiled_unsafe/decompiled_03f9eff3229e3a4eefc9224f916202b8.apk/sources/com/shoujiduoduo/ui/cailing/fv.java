package com.shoujiduoduo.ui.cailing;

import cn.banshenggua.aichang.room.message.SocketMessage;
import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;

/* compiled from: TestCucc */
class fv extends a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TestCucc f1082a;

    fv(TestCucc testCucc) {
        this.f1082a = testCucc;
    }

    public void a(c.b bVar) {
        super.a(bVar);
        com.shoujiduoduo.base.a.a.a("testcucc", "qryUserTone onSuccess:" + bVar.toString());
        if (bVar instanceof c.s) {
            c.z[] zVarArr = ((c.s) bVar).d;
            if (zVarArr == null || zVarArr.length == 0) {
                com.shoujiduoduo.base.a.a.a("testcucc", "userToneInfo == null");
                return;
            }
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < zVarArr.length; i++) {
                sb.append(zVarArr[i].d).append("-").append(zVarArr[i].b);
                sb.append("|");
            }
            this.f1082a.a(bVar, sb.toString());
            return;
        }
        this.f1082a.a(bVar, SocketMessage.MSG_ERROR_KEY);
    }

    public void b(c.b bVar) {
        super.b(bVar);
        com.shoujiduoduo.base.a.a.a("testcucc", "qryUserTone onSuccess:" + bVar.toString());
        this.f1082a.a(bVar);
    }
}
