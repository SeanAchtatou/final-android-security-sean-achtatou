package com.shoujiduoduo.ui.cailing;

import android.content.DialogInterface;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.widget.a;

/* compiled from: DuoduoVipDialog */
class bg extends a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ au f964a;

    bg(au auVar) {
        this.f964a = auVar;
    }

    public void a(c.b bVar) {
        super.a(bVar);
        this.f964a.h();
        this.f964a.c();
        this.f964a.a();
        this.f964a.m.sendMessage(this.f964a.m.obtainMessage(10));
    }

    public void b(c.b bVar) {
        String str;
        super.b(bVar);
        this.f964a.a();
        if (bVar.a().equals("2100")) {
            str = "当前区域尚未开通包月业务，请耐心等待，本次开通不扣费.";
        } else {
            str = "未成功开通会员. 原因:" + bVar.b();
        }
        new a.C0034a(this.f964a.j).a(str).b((int) R.string.hint).a((int) R.string.ok, (DialogInterface.OnClickListener) null).a().show();
        this.f964a.m.sendMessage(this.f964a.m.obtainMessage(11));
    }
}
