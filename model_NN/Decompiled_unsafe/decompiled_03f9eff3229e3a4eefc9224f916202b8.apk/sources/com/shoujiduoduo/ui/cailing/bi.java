package com.shoujiduoduo.ui.cailing;

import android.content.DialogInterface;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.util.av;
import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.widget.a;
import com.tencent.connect.common.Constants;
import com.umeng.analytics.b;

/* compiled from: DuoduoVipDialog */
class bi extends a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ au f966a;

    bi(au auVar) {
        this.f966a = auVar;
    }

    public void a(c.b bVar) {
        super.a(bVar);
        this.f966a.a();
        this.f966a.h();
        this.f966a.c();
        this.f966a.m.sendMessage(this.f966a.m.obtainMessage(10));
    }

    public void b(c.b bVar) {
        super.b(bVar);
        this.f966a.a();
        if (bVar.a().equals(Constants.DEFAULT_UIN)) {
            String c = b.c(RingDDApp.c(), "cmcc_open_delay_msg");
            if (av.b(c)) {
                c = "已为您提交会员开通申请，请按照短信提示操作，回复“是”确认订购。目前河北、山东用户开通后稍有延迟";
            }
            new a.C0034a(this.f966a.j).a(c).b((int) R.string.hint).a((int) R.string.ok, (DialogInterface.OnClickListener) null).a().show();
        } else {
            new a.C0034a(this.f966a.j).a(bVar.toString()).b((int) R.string.hint).a((int) R.string.ok, (DialogInterface.OnClickListener) null).a().show();
        }
        this.f966a.m.sendMessage(this.f966a.m.obtainMessage(11));
    }
}
