package com.shoujiduoduo.ui.cailing;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ffcs.inapppaylib.EMPHelper;
import com.ffcs.inapppaylib.bean.Constants;
import com.ffcs.inapppaylib.bean.response.BaseResponse;
import com.ffcs.inapppaylib.bean.response.IValidatableResponse;
import com.ffcs.inapppaylib.bean.response.VerifyResponse;
import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.base.bean.ab;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.util.at;
import com.shoujiduoduo.util.av;
import com.shoujiduoduo.util.f;
import com.shoujiduoduo.util.i;
import com.shoujiduoduo.util.w;
import com.shoujiduoduo.util.widget.a;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/* compiled from: DuoduoVipDialog */
public class au extends Dialog implements View.OnClickListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public EditText f949a;
    private String b;
    private String c;
    private EditText d;
    /* access modifiers changed from: private */
    public Button e;
    private TextView f;
    private TextView g;
    private TextView h;
    private RelativeLayout i;
    /* access modifiers changed from: private */
    public Context j;
    /* access modifiers changed from: private */
    public d k;
    private ListView l;
    /* access modifiers changed from: private */
    public a m;
    private f.b n;
    /* access modifiers changed from: private */
    public ArrayList<Map<String, Object>> o;
    /* access modifiers changed from: private */
    public c p;
    /* access modifiers changed from: private */
    public String q;
    /* access modifiers changed from: private */
    public RingData r;
    /* access modifiers changed from: private */
    public ContentObserver s;
    private boolean t;
    /* access modifiers changed from: private */
    public boolean u;
    private EMPHelper v;
    /* access modifiers changed from: private */
    public String w = "";
    /* access modifiers changed from: private */
    public boolean x = false;
    /* access modifiers changed from: private */
    public ProgressDialog y = null;

    /* compiled from: DuoduoVipDialog */
    public interface c {
        void a(boolean z);
    }

    public au(Context context, f.b bVar, RingData ringData, String str, boolean z, boolean z2, c cVar) {
        super(context, R.style.duoduo_dialog_theme);
        this.j = context;
        this.q = str;
        this.r = ringData;
        if (this.r == null) {
            this.r = new RingData();
        }
        this.t = z;
        this.k = new d(60000, 1000);
        this.u = z2;
        this.n = bVar;
        this.m = new a(this);
        if (this.n.equals(f.b.ct)) {
            com.shoujiduoduo.base.a.a.a("DuoduoVipDialog", "init ctcc emp helper");
            this.v = EMPHelper.getInstance(this.j);
            this.v.init("1000010404421", "5297", "x4lRWHcgRqfH", this.m, 15000);
        }
        this.o = d();
        this.p = cVar;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.dialog_duoduo_vip);
        this.l = (ListView) findViewById(R.id.vip_right_list);
        this.i = (RelativeLayout) findViewById(R.id.phone_auth_layout);
        this.i.setVisibility(0);
        this.f949a = (EditText) this.i.findViewById(R.id.et_phone_no);
        this.d = (EditText) this.i.findViewById(R.id.et_random_key);
        this.e = (Button) this.i.findViewById(R.id.reget_sms_code);
        this.e.setOnClickListener(this);
        this.f = (TextView) findViewById(R.id.open_tips);
        this.g = (TextView) findViewById(R.id.bottom_tips);
        this.h = (TextView) findViewById(R.id.cost_hint);
        if (this.u) {
            this.f.setText((int) R.string.open_cailing_and_update_vip);
            this.g.setText((int) R.string.open_cailing_hint);
        }
        if (this.n.equals(f.b.cu)) {
            this.g.setText(Html.fromHtml("<p>1.<font color=#ff0000>“铃声多多”</font>炫铃包月为包月计费，资费为<font color=#ff0000>5元/月</font>，用户订购即扣费。<br/>2.订购包月业务后，如月末不退订，次月1日将自动扣除包月费用。<br/>3.若您取消炫铃功能，将不能正常使用炫铃包月业务<br/>4.联通沃3G预付费20元卡不支持开通此业务<br/>5.使用炫铃过程中产生流量费按照您手机套餐内资费标准收取<br/>6.暂不支持山东用户进行订购，敬请期待"));
            this.f949a.setHint((int) R.string.cucc_num);
        } else if (this.n.equals(f.b.ct)) {
            this.f949a.setHint((int) R.string.ctcc_num);
        } else if (this.n.equals(f.b.f1671a)) {
            this.f949a.setHint("中国移动号码");
            this.i.setVisibility(8);
        }
        String k2 = com.shoujiduoduo.a.b.b.g().c().k();
        this.b = k2;
        this.f949a.setText(k2);
        if (this.n.equals(f.b.ct)) {
            this.h.setText((int) R.string.six_yuan_per_month);
            this.t = true;
            if (!av.c(k2)) {
                this.k.start();
                com.shoujiduoduo.base.a.a.a("DuoduoVipDialog", "start to get feed id");
                this.c = k2;
                this.v.getTradeId(k2, "90213829", "135000000000000214309");
            }
        } else if (!this.n.equals(f.b.cu) && this.n.equals(f.b.f1671a)) {
            this.h.setText((int) R.string.six_yuan_per_month);
        }
        a(this.t);
        this.l.setAdapter((ListAdapter) new b(this, null));
        findViewById(R.id.close).setOnClickListener(this);
        findViewById(R.id.open).setOnClickListener(this);
        setOnCancelListener(new av(this));
        setOnDismissListener(new bc(this));
        this.s = new at(this.j, new Handler(), this.d, "1065987320001", 4);
        this.j.getContentResolver().registerContentObserver(Uri.parse("content://sms/"), true, this.s);
    }

    private void a(boolean z) {
        if (z) {
            this.d.setVisibility(0);
            this.e.setVisibility(0);
            return;
        }
        this.d.setVisibility(8);
        this.e.setVisibility(8);
    }

    /* compiled from: DuoduoVipDialog */
    private static class a extends Handler {

        /* renamed from: a  reason: collision with root package name */
        private final WeakReference<au> f951a;

        public a(au auVar) {
            this.f951a = new WeakReference<>(auVar);
        }

        public void handleMessage(Message message) {
            String str;
            au auVar = this.f951a.get();
            if (auVar != null) {
                switch (message.what) {
                    case 10:
                        if (auVar.p != null) {
                            auVar.p.a(true);
                        }
                        auVar.dismiss();
                        return;
                    case 11:
                        if (auVar.p != null) {
                            auVar.p.a(false);
                        }
                        auVar.dismiss();
                        return;
                    case Constants.RESULT_PAY_SUCCESS:
                        com.shoujiduoduo.base.a.a.a("DuoduoVipDialog", "pay success");
                        w.a(auVar.i(), "success", auVar.b());
                        auVar.h();
                        auVar.c();
                        auVar.a();
                        if (TextUtils.isEmpty(auVar.r.g)) {
                            new a.C0034a(auVar.j).a((int) R.string.open_vip_success).b((int) R.string.hint).a((int) R.string.ok, (DialogInterface.OnClickListener) null).a().show();
                        } else if (auVar.p != null) {
                            auVar.p.a(true);
                        }
                        auVar.dismiss();
                        return;
                    case Constants.RESULT_PAY_FAILURE:
                        auVar.a();
                        BaseResponse baseResponse = (BaseResponse) message.obj;
                        if (baseResponse != null) {
                            str = baseResponse.getRes_code() + ":" + baseResponse.getRes_message();
                        } else {
                            str = "";
                        }
                        w.a(auVar.i(), "fail," + str, auVar.b());
                        com.shoujiduoduo.base.a.a.a("DuoduoVipDialog", "pay failure," + str);
                        new a.C0034a(auVar.j).a("未成功开通会员. 原因:" + str).b((int) R.string.hint).a((int) R.string.ok, (DialogInterface.OnClickListener) null).a().show();
                        if (auVar.p != null) {
                            auVar.p.a(false);
                        }
                        auVar.dismiss();
                        return;
                    case Constants.RESULT_VALIDATE_FAILURE:
                        BaseResponse baseResponse2 = (BaseResponse) message.obj;
                        if (baseResponse2 != null) {
                            String str2 = baseResponse2.getRes_code() + ":" + baseResponse2.getRes_message();
                            com.shoujiduoduo.base.a.a.a("DuoduoVipDialog", "RESULT_VALIDATE_FAILURE, " + baseResponse2.getRes_code() + ":" + baseResponse2.getRes_message());
                            com.shoujiduoduo.util.widget.f.a(str2);
                            w.a(auVar.i(), "validate fail," + str2, auVar.b());
                            return;
                        }
                        return;
                    case 900:
                        com.shoujiduoduo.base.a.a.a("DuoduoVipDialog", "get trade id success");
                        IValidatableResponse iValidatableResponse = (IValidatableResponse) message.obj;
                        if (iValidatableResponse != null) {
                            String unused = auVar.w = iValidatableResponse.getTrade_id();
                            boolean unused2 = auVar.x = iValidatableResponse.getLowTariff();
                            com.shoujiduoduo.base.a.a.a("DuoduoVipDialog", "tradeid:" + auVar.w + ", lowTariff:" + auVar.x);
                            com.shoujiduoduo.base.a.a.a("DuoduoVipDialog", iValidatableResponse.getRes_code() + ":" + iValidatableResponse.getRes_message());
                        } else {
                            com.shoujiduoduo.base.a.a.a("DuoduoVipDialog", "get trade id, response is null");
                        }
                        com.shoujiduoduo.util.widget.f.a("成功获取验证码");
                        return;
                    default:
                        return;
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public String b() {
        StringBuilder sb = new StringBuilder();
        sb.append("&ctcid=").append(this.r.s).append("&rid=").append(this.r.g).append("&cucid=").append(this.r.A).append("&from=").append(this.q).append("&phone=").append(this.b);
        return sb.toString();
    }

    /* compiled from: DuoduoVipDialog */
    private class d extends CountDownTimer {
        public d(long j, long j2) {
            super(j, j2);
        }

        public void onTick(long j) {
            au.this.e.setClickable(false);
            au.this.e.setText((j / 1000) + "秒");
        }

        public void onFinish() {
            au.this.e.setClickable(true);
            au.this.e.setText("重新获取");
        }
    }

    /* compiled from: DuoduoVipDialog */
    private class b extends BaseAdapter {
        private b() {
        }

        /* synthetic */ b(au auVar, av avVar) {
            this();
        }

        public int getCount() {
            if (au.this.o != null) {
                return au.this.o.size();
            }
            return 0;
        }

        public Object getItem(int i) {
            if (au.this.o != null) {
                return au.this.o.get(i);
            }
            return null;
        }

        public long getItemId(int i) {
            return (long) i;
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            if (view == null) {
                view = LayoutInflater.from(au.this.j).inflate((int) R.layout.listitem_vip_rights_small, (ViewGroup) null);
            }
            Map map = (Map) au.this.o.get(i);
            TextView textView = (TextView) view.findViewById(R.id.title);
            ((ImageView) view.findViewById(R.id.icon)).setImageResource(((Integer) map.get("icon")).intValue());
            textView.setText((String) map.get("title"));
            ((TextView) view.findViewById(R.id.description)).setText((String) map.get("description"));
            if (i == 0) {
                textView.setTextColor(au.this.j.getResources().getColor(R.color.text_orange));
            }
            return view;
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        String str;
        ab c2 = com.shoujiduoduo.a.b.b.g().c();
        String i2 = f.i();
        String bVar = f.u().toString();
        String str2 = "";
        switch (c2.e()) {
            case 1:
                str2 = "phone";
                break;
            case 2:
                str2 = "qq";
                break;
            case 3:
                str2 = cn.banshenggua.aichang.utils.Constants.WEIBO;
                break;
            case 5:
                str2 = "weixin";
                break;
        }
        switch (this.n) {
            case f1671a:
                str = "cm_open_vip";
                break;
            case ct:
                str = "ct_open_diy_sdk";
                break;
            case cu:
                str = "cu_open_vip";
                break;
            default:
                str = "unknown";
                break;
        }
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("&newimsi=").append(i2).append("&phone=").append(this.b).append("&st=").append(bVar).append("&uid=").append(c2.a()).append("&3rd=").append(str2).append("&rid=").append(this.r.g).append("&viptype=").append(str);
        if (this.n == f.b.f1671a) {
            stringBuffer.append("&cm_uid=").append(com.shoujiduoduo.util.c.b.a().b());
        }
        i.a(new bd(this, stringBuffer));
    }

    private ArrayList<Map<String, Object>> d() {
        ArrayList<Map<String, Object>> arrayList = new ArrayList<>();
        HashMap hashMap = new HashMap();
        hashMap.put("icon", Integer.valueOf((int) R.drawable.vip_free_s));
        hashMap.put("title", this.n == f.b.f1671a ? "百万彩铃免费用" : "所有彩铃免费用");
        hashMap.put("description", "换彩铃不用花钱啦");
        arrayList.add(hashMap);
        HashMap hashMap2 = new HashMap();
        hashMap2.put("icon", Integer.valueOf((int) R.drawable.vip_noad_s));
        hashMap2.put("title", "永久去除应用内广告");
        hashMap2.put("description", "无广告，真干净");
        arrayList.add(hashMap2);
        HashMap hashMap3 = new HashMap();
        hashMap3.put("icon", Integer.valueOf((int) R.drawable.vip_personal_s));
        hashMap3.put("title", "私人定制炫酷启动画面");
        hashMap3.put("description", "小清新、文艺范、女汉子...");
        arrayList.add(hashMap3);
        return arrayList;
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        this.m.post(new be(this, str));
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.m.post(new bf(this));
    }

    /* access modifiers changed from: private */
    public void e() {
        com.shoujiduoduo.util.e.a.a().d(b(), new bg(this));
    }

    private void b(String str) {
        try {
            com.shoujiduoduo.base.a.a.a("DuoduoVipDialog", "confirmPayment, tradeId:" + this.w + ", code:" + str + ", lowTariff:" + this.x);
            this.v.confirmPayment(this.w, str, this.x);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void f() {
        a("请稍候...");
        com.shoujiduoduo.util.c.b.a().b(b(), new bh(this));
    }

    /* access modifiers changed from: private */
    public void g() {
        a("请稍候...");
        com.shoujiduoduo.util.c.b.a().c(b(), new bi(this));
    }

    /* access modifiers changed from: private */
    public void h() {
        ab c2 = com.shoujiduoduo.a.b.b.g().c();
        int i2 = 0;
        switch (this.n) {
            case f1671a:
                i2 = 1;
                break;
            case ct:
                i2 = 2;
                break;
            case cu:
                i2 = 3;
                break;
        }
        c2.b(i2);
        c2.d(this.b);
        if (!c2.i()) {
            if (this.n != f.b.f1671a || !av.c(this.b)) {
                c2.b(this.b);
                c2.a("phone_" + this.b);
            } else {
                c2.b("多多VIP");
                c2.a("phone_" + com.shoujiduoduo.util.c.b.a().b());
            }
            c2.c(1);
            com.shoujiduoduo.a.b.b.g().a(c2);
            x.a().a(com.shoujiduoduo.a.a.b.OBSERVER_USER_CENTER, new aw(this));
        } else {
            com.shoujiduoduo.a.b.b.g().a(c2);
        }
        x.a().a(com.shoujiduoduo.a.a.b.OBSERVER_VIP, new ax(this, i2));
    }

    /* access modifiers changed from: private */
    public String i() {
        if (this.n.equals(f.b.ct)) {
            return "ct:ct_open_diy_sdk";
        }
        if (this.n.equals(f.b.cu)) {
            return "cu:cu_open_vip";
        }
        if (this.n.equals(f.b.f1671a)) {
            return "cm:cm_open_vip";
        }
        return "";
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.reget_sms_code:
                this.b = this.f949a.getText().toString();
                if (!f.f(this.b)) {
                    com.shoujiduoduo.util.widget.f.a("请输入正确的手机号", 0);
                    return;
                }
                this.n = f.g(this.b);
                if (this.n == f.b.d) {
                    com.shoujiduoduo.util.widget.f.a("未知的手机号类型，无法判断运营商，请确认手机号输入正确！");
                    com.shoujiduoduo.base.a.a.c("DuoduoVipDialog", "unknown phone type :" + this.b);
                    return;
                }
                this.e.setClickable(false);
                if (this.n.equals(f.b.ct)) {
                    if (this.b.equals(this.c)) {
                        com.shoujiduoduo.base.a.a.a("DuoduoVipDialog", "refresh vcode id from btn");
                        this.v.refreshVCode(this.w, "90213829", new ba(this));
                        return;
                    }
                    com.shoujiduoduo.base.a.a.a("DuoduoVipDialog", "get trade id from btn");
                    this.c = this.b;
                    this.v.getTradeId(this.b, "90213829", "135000000000000214309");
                    this.k.start();
                    return;
                } else if (this.n.equals(f.b.f1671a)) {
                    com.shoujiduoduo.util.c.b.a().a(this.b, new bb(this));
                    return;
                } else {
                    return;
                }
            case R.id.open:
                if (this.n.equals(f.b.cu)) {
                    TextView textView = new TextView(this.j);
                    textView.setLineSpacing(10.0f, 1.0f);
                    textView.setText(Html.fromHtml("即将为您开通铃声多多包月，标准资费<font color=#ff0000>5元/月</font>，资费由运营商收取"));
                    new a.C0034a(this.j).b("开通铃声多多包月").a(textView).b("取消", (DialogInterface.OnClickListener) null).a("确认开通", new ay(this)).a().show();
                    return;
                } else if (this.n.equals(f.b.ct)) {
                    String obj = this.d.getText().toString();
                    if (TextUtils.isEmpty(obj)) {
                        com.shoujiduoduo.util.widget.f.a("请输入正确的验证码");
                        return;
                    }
                    a("请稍候...");
                    b(obj);
                    return;
                } else if (!this.n.equals(f.b.f1671a)) {
                    return;
                } else {
                    if (!"false".equals(com.umeng.analytics.b.c(RingDDApp.c(), "cmcc_open_vip_twice_confirm"))) {
                        TextView textView2 = new TextView(this.j);
                        textView2.setLineSpacing(10.0f, 1.0f);
                        textView2.setText(Html.fromHtml("即将为您开通铃声多多包月，标准资费<font color=#ff0000>6元/月</font>，资费由运营商收取"));
                        new a.C0034a(this.j).b("开通铃声多多包月").a(textView2).b("取消", (DialogInterface.OnClickListener) null).a("确认开通", new az(this)).a().show();
                        return;
                    } else if (this.u) {
                        f();
                        return;
                    } else {
                        g();
                        return;
                    }
                }
            case R.id.close:
                String obj2 = this.f949a.getText().toString();
                StringBuilder sb = new StringBuilder();
                sb.append("&ctcid=").append(this.r.s).append("&rid=").append(this.r.g).append("&from=").append(this.q).append("&phone=").append(obj2);
                w.a(i(), "close", sb.toString());
                dismiss();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public void a(VerifyResponse verifyResponse, boolean z) {
        this.e.setClickable(true);
        if (z) {
            this.k = new d(60000, 1000);
            this.k.start();
        } else if (verifyResponse.getRes_code() == -1 || verifyResponse.getRes_code() == -3) {
            com.shoujiduoduo.util.widget.f.a("网络异常, 验证码刷新失败");
        }
    }
}
