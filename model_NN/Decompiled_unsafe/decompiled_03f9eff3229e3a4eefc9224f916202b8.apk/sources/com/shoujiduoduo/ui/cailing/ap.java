package com.shoujiduoduo.ui.cailing;

import android.app.ProgressDialog;

/* compiled from: CailingMenu */
class ap implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ak f944a;

    ap(ak akVar) {
        this.f944a = akVar;
    }

    public void run() {
        if (this.f944a.s == null) {
            ProgressDialog unused = this.f944a.s = new ProgressDialog(this.f944a.e);
            this.f944a.s.setMessage("中国移动提示您：首次使用彩铃功能需要发送一条免费短信，该过程自动完成，如有警告，请选择允许。");
            this.f944a.s.setIndeterminate(false);
            this.f944a.s.setCancelable(true);
            this.f944a.s.setCanceledOnTouchOutside(false);
            this.f944a.s.setButton("取消", new aq(this));
            this.f944a.s.show();
        }
    }
}
