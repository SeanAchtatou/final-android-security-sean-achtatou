package com.shoujiduoduo.ui.cailing;

import android.content.DialogInterface;

/* compiled from: InputPhoneNumDialog */
class cd implements DialogInterface.OnDismissListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ cb f989a;

    cd(cb cbVar) {
        this.f989a = cbVar;
    }

    public void onDismiss(DialogInterface dialogInterface) {
        this.f989a.k.getContentResolver().unregisterContentObserver(this.f989a.j);
        if (this.f989a.f != null) {
            this.f989a.f.cancel();
        }
    }
}
