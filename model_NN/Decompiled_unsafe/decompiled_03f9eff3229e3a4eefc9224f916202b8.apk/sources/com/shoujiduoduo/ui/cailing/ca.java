package com.shoujiduoduo.ui.cailing;

import android.app.AlertDialog;
import android.content.DialogInterface;
import com.shoujiduoduo.ui.cailing.GiveCailingActivity;
import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;

/* compiled from: GiveCailingActivity */
class ca extends a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GiveCailingActivity f985a;

    ca(GiveCailingActivity giveCailingActivity) {
        this.f985a = giveCailingActivity;
    }

    public void a(c.b bVar) {
        super.a(bVar);
        this.f985a.a();
        com.shoujiduoduo.base.a.a.a(GiveCailingActivity.f919a, "赠送成功");
        new AlertDialog.Builder(this.f985a).setTitle("赠送彩铃").setMessage("赠送彩铃成功，谢谢使用").setPositiveButton("确认", new GiveCailingActivity.a(this.f985a, null)).show();
    }

    public void b(c.b bVar) {
        super.b(bVar);
        this.f985a.a();
        com.shoujiduoduo.base.a.a.a(GiveCailingActivity.f919a, "赠送失败， code:" + bVar.a() + " , msg:" + bVar.b());
        new AlertDialog.Builder(this.f985a).setTitle("赠送彩铃").setMessage("赠送彩铃失败！ 原因:" + bVar.b() + ", code:" + bVar.a()).setPositiveButton("确认", (DialogInterface.OnClickListener) null).show();
    }
}
