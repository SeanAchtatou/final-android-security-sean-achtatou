package com.shoujiduoduo.ui.cailing;

import android.content.DialogInterface;
import android.os.Message;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.util.as;
import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.w;
import com.shoujiduoduo.util.widget.a;

/* compiled from: InputPhoneNumDialog */
class cg extends a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ cb f992a;

    cg(cb cbVar) {
        this.f992a = cbVar;
    }

    public void b(c.b bVar) {
        super.b(bVar);
        this.f992a.c();
        new a.C0034a(this.f992a.k).a("输入验证码不对，请重试！").a("确定", (DialogInterface.OnClickListener) null).a().show();
    }

    public void a(c.b bVar) {
        super.a(bVar);
        this.f992a.c();
        Message obtainMessage = this.f992a.e.obtainMessage();
        obtainMessage.obj = this.f992a.i;
        this.f992a.e.sendMessage(obtainMessage);
        as.c(RingDDApp.c(), "pref_phone_num", this.f992a.i);
        w.a(this.f992a.a(), "success", "&phone=" + this.f992a.i);
        this.f992a.dismiss();
    }
}
