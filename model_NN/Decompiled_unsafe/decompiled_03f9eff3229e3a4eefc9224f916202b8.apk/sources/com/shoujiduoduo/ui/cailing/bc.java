package com.shoujiduoduo.ui.cailing;

import android.content.DialogInterface;

/* compiled from: DuoduoVipDialog */
class bc implements DialogInterface.OnDismissListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ au f960a;

    bc(au auVar) {
        this.f960a = auVar;
    }

    public void onDismiss(DialogInterface dialogInterface) {
        this.f960a.j.getContentResolver().unregisterContentObserver(this.f960a.s);
        if (this.f960a.k != null) {
            this.f960a.k.cancel();
        }
    }
}
