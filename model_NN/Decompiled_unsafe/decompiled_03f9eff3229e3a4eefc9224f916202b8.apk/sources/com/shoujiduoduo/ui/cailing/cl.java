package com.shoujiduoduo.ui.cailing;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Handler;
import android.os.Message;
import com.cmsc.cmmusic.common.data.Result;
import com.shoujiduoduo.a.a.b;
import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.ui.cailing.ck;
import com.shoujiduoduo.util.av;

/* compiled from: MemberOpenDialog */
class cl extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ck f998a;

    cl(ck ckVar) {
        this.f998a = ckVar;
    }

    public void handleMessage(Message message) {
        super.handleMessage(message);
        if (message.what == 112) {
            Result result = (Result) message.obj;
            this.f998a.a();
            this.f998a.dismiss();
            if (result == null) {
                return;
            }
            if (result.getResCode().equals("000000")) {
                new AlertDialog.Builder(this.f998a.b).setTitle("开通彩铃").setMessage("彩铃业务已成功申请开通,稍候收到短信即可正常使用").setPositiveButton("确认", (DialogInterface.OnClickListener) null).show();
                if (this.f998a.k != null) {
                    this.f998a.k.a(ck.a.C0025a.open);
                }
                x.a().b(b.OBSERVER_CAILING, new cm(this));
            } else if (result.getResCode().equals("000001")) {
                new AlertDialog.Builder(this.f998a.b).setTitle("开通彩铃").setMessage("彩铃业务已成功申请开通,稍候收到短信即可正常使用").setPositiveButton("确认", (DialogInterface.OnClickListener) null).show();
                if (this.f998a.k != null) {
                    this.f998a.k.a(ck.a.C0025a.waiting);
                }
                x.a().b(b.OBSERVER_CAILING, new cn(this));
            } else {
                String resMsg = result.getResMsg();
                if (av.c(resMsg)) {
                    resMsg = "未成功开通彩铃业务！";
                }
                new AlertDialog.Builder(this.f998a.b).setTitle("开通彩铃").setMessage(resMsg).setPositiveButton("确认", (DialogInterface.OnClickListener) null).show();
                if (this.f998a.k != null) {
                    this.f998a.k.a(ck.a.C0025a.close);
                }
            }
        }
    }
}
