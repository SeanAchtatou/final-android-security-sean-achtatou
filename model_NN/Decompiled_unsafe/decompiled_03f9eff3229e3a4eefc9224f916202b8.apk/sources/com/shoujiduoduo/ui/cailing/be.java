package com.shoujiduoduo.ui.cailing;

import android.app.ProgressDialog;

/* compiled from: DuoduoVipDialog */
class be implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f962a;
    final /* synthetic */ au b;

    be(au auVar, String str) {
        this.b = auVar;
        this.f962a = str;
    }

    public void run() {
        if (this.b.y == null) {
            ProgressDialog unused = this.b.y = new ProgressDialog(this.b.j);
            this.b.y.setMessage(this.f962a);
            this.b.y.setIndeterminate(false);
            this.b.y.setCancelable(true);
            this.b.y.setCanceledOnTouchOutside(false);
            this.b.y.show();
        }
    }
}
