package com.shoujiduoduo.ui.cailing;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.cmsc.cmmusic.common.RingbackManagerInterface;
import com.shoujiduoduo.a.b.b;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.util.am;
import com.shoujiduoduo.util.at;
import com.shoujiduoduo.util.f;

/* compiled from: MemberOpenDialog */
public class ck extends Dialog implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private static String f996a = ck.class.getSimpleName();
    /* access modifiers changed from: private */
    public Context b;
    private ImageButton c;
    private ImageView d;
    private Button e;
    private f.b f;
    private RelativeLayout g;
    private EditText h;
    private EditText i;
    private ImageButton j;
    /* access modifiers changed from: private */
    public a k;
    private TextView l;
    /* access modifiers changed from: private */
    public ContentObserver m;
    private TextView n;
    /* access modifiers changed from: private */
    public Handler o = new cl(this);
    private ProgressDialog p = null;

    /* compiled from: MemberOpenDialog */
    public interface a {

        /* renamed from: com.shoujiduoduo.ui.cailing.ck$a$a  reason: collision with other inner class name */
        /* compiled from: MemberOpenDialog */
        public enum C0025a {
            open,
            waiting,
            close
        }

        void a(C0025a aVar);
    }

    public ck(Context context, int i2, f.b bVar, a aVar) {
        super(context, i2);
        this.b = context;
        this.f = bVar;
        this.k = aVar;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.dialog_open_member);
        this.l = (TextView) findViewById(R.id.cailing_des);
        this.c = (ImageButton) findViewById(R.id.open_migu_close);
        this.c.setOnClickListener(this);
        setOnDismissListener(new co(this));
        this.e = (Button) findViewById(R.id.open_migu_member);
        this.e.setOnClickListener(this);
        TextView textView = (TextView) findViewById(R.id.cailing_des);
        this.d = (ImageView) findViewById(R.id.member_icon);
        this.g = (RelativeLayout) findViewById(R.id.random_key_auth_layout);
        this.n = (TextView) findViewById(R.id.title);
        this.i = (EditText) findViewById(R.id.et_phone_code);
        this.h = (EditText) findViewById(R.id.et_phone_no);
        String b2 = f.b();
        if (!TextUtils.isEmpty(b2)) {
            this.h.setText(b2);
        } else if (!TextUtils.isEmpty(b.g().c().k())) {
            this.h.setText(b.g().c().k());
        }
        if (this.f == f.b.f1671a) {
            this.d.setImageResource(R.drawable.icon_cmcc);
            this.g.setVisibility(8);
            textView.setText((int) R.string.cmcc_member_open_des);
            this.h.setHint((int) R.string.cmcc_num);
        } else if (this.f == f.b.ct) {
            this.d.setImageResource(R.drawable.icon_ctcc);
            this.g.setVisibility(0);
            textView.setText((int) R.string.ctcc_member_open_des);
            this.h.setHint((int) R.string.ctcc_num);
        } else if (this.f == f.b.cu) {
            this.n.setText("开通炫铃业务");
            this.d.setImageResource(R.drawable.icon_cucc);
            this.l.setText((int) R.string.cucc_open_cailing_hint);
            this.g.setVisibility(8);
        }
        this.j = (ImageButton) findViewById(R.id.btn_get_code);
        this.j.setOnClickListener(this);
        this.m = new at(this.b, new Handler(), this.i, "118100", 6);
        this.b.getContentResolver().registerContentObserver(Uri.parse("content://sms/"), true, this.m);
        setCanceledOnTouchOutside(true);
    }

    private void b() {
        RingbackManagerInterface.openRingback(this.b, new cp(this));
    }

    private void c() {
        com.shoujiduoduo.util.c.b.a().b("", new cq(this));
    }

    private void d() {
        com.shoujiduoduo.util.e.a.a().e("&phone=" + b.g().c().k(), new ct(this));
    }

    private void e() {
        String obj = this.h.getText().toString();
        String obj2 = this.i.getText().toString();
        if (obj == null || !f.f(obj)) {
            this.h.setError("请输入正确的手机号");
        } else if (obj2 == null || obj2.length() != 6) {
            this.i.setError("请输入正确的验证码");
        } else {
            com.shoujiduoduo.util.d.b.a().d(obj, obj2, new cu(this));
        }
    }

    private void b(String str) {
        com.shoujiduoduo.util.d.b.a().d(str, new cv(this));
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        if (this.p == null) {
            this.p = new ProgressDialog(this.b);
            this.p.setMessage(str);
            this.p.setIndeterminate(false);
            this.p.setCancelable(false);
            this.p.show();
        }
    }

    /* access modifiers changed from: package-private */
    public void a() {
        if (this.p != null) {
            this.p.dismiss();
            this.p = null;
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_get_code:
                String obj = this.h.getText().toString();
                if (obj == null || !f.f(this.h.getText().toString())) {
                    Toast.makeText(this.b, "请输入正确的手机号", 1).show();
                    return;
                } else if (this.f == f.b.ct) {
                    a("请稍候...");
                    b(obj);
                    return;
                } else {
                    return;
                }
            case R.id.open_migu_close:
                dismiss();
                return;
            case R.id.open_migu_member:
                a("请稍候...");
                if (this.f == f.b.f1671a) {
                    if (am.a().b("cm_sunshine_sdk_enable")) {
                        b();
                        return;
                    } else {
                        c();
                        return;
                    }
                } else if (this.f == f.b.ct) {
                    e();
                    return;
                } else if (this.f == f.b.cu) {
                    d();
                    return;
                } else {
                    return;
                }
            default:
                return;
        }
    }
}
