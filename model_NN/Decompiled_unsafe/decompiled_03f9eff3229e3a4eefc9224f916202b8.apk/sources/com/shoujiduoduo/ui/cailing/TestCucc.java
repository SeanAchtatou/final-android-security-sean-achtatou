package com.shoujiduoduo.ui.cailing;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ui.utils.BaseActivity;
import com.shoujiduoduo.util.at;
import com.shoujiduoduo.util.av;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.e.a;
import com.shoujiduoduo.util.f;
import com.shoujiduoduo.util.widget.f;

public class TestCucc extends BaseActivity implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private String f924a = "18561951252";
    private String b = "";
    private String c = "810027210086";
    private EditText d;
    /* access modifiers changed from: private */
    public EditText e;
    private EditText f;
    private ContentObserver g;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_test_cucc);
        this.d = (EditText) findViewById(R.id.music_id);
        this.d.setText(this.c);
        this.e = (EditText) findViewById(R.id.phone_num);
        this.e.setText(this.f924a);
        this.f = (EditText) findViewById(R.id.random_key);
        findViewById(R.id.query_vip_state).setOnClickListener(this);
        findViewById(R.id.open_vip).setOnClickListener(this);
        findViewById(R.id.vip_order).setOnClickListener(this);
        findViewById(R.id.sms_random_key).setOnClickListener(this);
        findViewById(R.id.close_vip).setOnClickListener(this);
        findViewById(R.id.check_base_cailing_status).setOnClickListener(this);
        findViewById(R.id.sms_get_token).setOnClickListener(this);
        findViewById(R.id.open_cailing).setOnClickListener(this);
        findViewById(R.id.close_cailing).setOnClickListener(this);
        findViewById(R.id.query_subed_product).setOnClickListener(this);
        findViewById(R.id.query_tone_set).setOnClickListener(this);
        findViewById(R.id.query_user_tone).setOnClickListener(this);
        findViewById(R.id.query_ring_by_id).setOnClickListener(this);
        findViewById(R.id.query_cucc_and_area).setOnClickListener(this);
        findViewById(R.id.query_cucc_info).setOnClickListener(this);
        findViewById(R.id.check_token).setOnClickListener(this);
        findViewById(R.id.get_mobile_num).setOnClickListener(this);
        findViewById(R.id.buy_cucc_cailing).setOnClickListener(this);
        findViewById(R.id.del_cucc_cailing).setOnClickListener(this);
        findViewById(R.id.qry_user_box).setOnClickListener(this);
        findViewById(R.id.qry_box_mem).setOnClickListener(this);
        findViewById(R.id.phone_sms_login).setOnClickListener(this);
        this.g = new at(this, new Handler(), (EditText) findViewById(R.id.random_key), "1065515888");
        getContentResolver().registerContentObserver(Uri.parse("content://sms/"), true, this.g);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        getContentResolver().unregisterContentObserver(this.g);
        super.onDestroy();
    }

    /* access modifiers changed from: private */
    public void a(c.b bVar) {
        new AlertDialog.Builder(this).setMessage(bVar.toString()).setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
    }

    /* access modifiers changed from: private */
    public void a(c.b bVar, String str) {
        new AlertDialog.Builder(this).setMessage(bVar.toString() + " , " + str).setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
    }

    public void onClick(View view) {
        this.f924a = this.e.getText().toString();
        if (av.c(this.f924a)) {
            f.a("请输入手机号");
        } else if (!a.a().a(this.f924a)) {
            a(this.f924a, f.b.cu);
        } else {
            switch (view.getId()) {
                case R.id.check_base_cailing_status:
                    if (!TextUtils.isEmpty(this.e.getText().toString())) {
                        this.f924a = this.e.getText().toString();
                    }
                    a.a().e(new fl(this));
                    return;
                case R.id.query_vip_state:
                case R.id.query_caililng_and_vip:
                case R.id.get_sms_code:
                case R.id.query_cailing_url:
                case R.id.query_ring_box:
                case R.id.query_default_ring:
                case R.id.checkbox:
                case R.id.find_mdn_by_imsi:
                case R.id.diy_clip_upload:
                case R.id.set_diy_ring:
                case R.id.get_diy_ring:
                case R.id.query_diy_ring_status:
                case R.id.sms_random_key_common:
                case R.id.query_ring_info:
                case R.id.query_zhenling_url:
                case R.id.emp_operate_layout:
                case R.id.emp_launch:
                case R.id.emp_open_vip:
                case R.id.emp_close_vip:
                case R.id.emp_one_key_open:
                case R.id.query3rd_phone:
                case R.id.query3rd_uid:
                default:
                    return;
                case R.id.vip_order:
                    a.a().a(true, "一首很火的来电铃声", a.b(this.f924a), this.f924a, "diy/20150914/", "6725908.wav", "晓辰枫", "", new fg(this));
                    return;
                case R.id.open_vip:
                    new AlertDialog.Builder(this).setMessage("确定要开通包月吗？").setPositiveButton("确定", new fa(this)).setNegativeButton("取消", (DialogInterface.OnClickListener) null).show();
                    return;
                case R.id.close_vip:
                    new AlertDialog.Builder(this).setMessage("确定关闭包月吗？").setNegativeButton("取消", (DialogInterface.OnClickListener) null).setPositiveButton("确定", new fj(this)).show();
                    return;
                case R.id.sms_random_key:
                    if (!TextUtils.isEmpty(this.e.getText().toString())) {
                        this.f924a = this.e.getText().toString();
                    }
                    a.a().c(this.f924a, new fh(this));
                    return;
                case R.id.phone_sms_login:
                    a(this.f924a, f.b.cu);
                    return;
                case R.id.query_subed_product:
                    a.a().c(new fr(this));
                    return;
                case R.id.query_tone_set:
                    a.a().h(new fu(this));
                    return;
                case R.id.query_user_tone:
                    a.a().i(new fv(this));
                    return;
                case R.id.query_ring_by_id:
                    a.a().h(this.d.getText().toString(), new fy(this));
                    return;
                case R.id.query_cucc_and_area:
                    a.a().i(this.f924a, new fm(this));
                    return;
                case R.id.query_cucc_info:
                    this.f924a = this.e.getText().toString();
                    a.a().j(this.f924a, new fn(this));
                    return;
                case R.id.buy_cucc_cailing:
                    String obj = this.d.getText().toString();
                    if (TextUtils.isEmpty(obj)) {
                        obj = "9178900048603256445007";
                    }
                    a.a().b(obj, "", new fo(this));
                    return;
                case R.id.del_cucc_cailing:
                    if (TextUtils.isEmpty(this.d.getText().toString())) {
                    }
                    a.a().a("xxx", a.b(this.f924a), this.f924a, "", new ez(this));
                    return;
                case R.id.qry_user_box:
                    a.a().b(new fw(this));
                    return;
                case R.id.qry_box_mem:
                    a.a().a("907550290062", new fx(this));
                    return;
                case R.id.open_cailing:
                    if (!TextUtils.isEmpty(this.e.getText().toString())) {
                        this.f924a = this.e.getText().toString();
                    }
                    new AlertDialog.Builder(this).setMessage("确定要开通彩铃业务吗？").setPositiveButton("确定", new fc(this)).setNegativeButton("取消", (DialogInterface.OnClickListener) null).show();
                    return;
                case R.id.close_cailing:
                    if (!TextUtils.isEmpty(this.e.getText().toString())) {
                        this.f924a = this.e.getText().toString();
                    }
                    new AlertDialog.Builder(this).setMessage("确定要关闭彩铃业务吗？").setPositiveButton("确定", new fe(this)).setNegativeButton("取消", (DialogInterface.OnClickListener) null).show();
                    return;
                case R.id.sms_get_token:
                    if (!TextUtils.isEmpty(this.f.getText().toString())) {
                        this.b = this.f.getText().toString();
                    }
                    if (!TextUtils.isEmpty(this.e.getText().toString())) {
                        this.f924a = this.e.getText().toString();
                    }
                    a.a().a(this.f924a, this.b, new fi(this));
                    return;
                case R.id.check_token:
                    new AlertDialog.Builder(this).setMessage("当前号码是否有token:" + a.a().a(this.f924a)).setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
                    return;
                case R.id.get_mobile_num:
                    a.a().a(new fs(this));
                    return;
            }
        }
    }

    private void a(String str, f.b bVar) {
        new cb(this, R.style.DuoDuoDialog, str, bVar, new fp(this)).show();
    }
}
