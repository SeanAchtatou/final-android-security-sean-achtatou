package com.shoujiduoduo.ui.cailing;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.text.Html;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ui.utils.BaseActivity;
import com.shoujiduoduo.util.as;
import com.shoujiduoduo.util.f;
import java.util.ArrayList;

public class GiveCailingActivity extends BaseActivity {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final String f919a = GiveCailingActivity.class.getSimpleName();
    private ImageButton b;
    private Button c;
    private ListView d;
    /* access modifiers changed from: private */
    public EditText e;
    /* access modifiers changed from: private */
    public EditText f;
    private EditText g;
    private ImageButton h;
    /* access modifiers changed from: private */
    public RingData i;
    /* access modifiers changed from: private */
    public String j;
    /* access modifiers changed from: private */
    public String k;
    private TextView l;
    private ImageButton m;
    private ArrayList<String> n = new ArrayList<>();
    /* access modifiers changed from: private */
    public boolean o;
    /* access modifiers changed from: private */
    public int p;
    private Handler q = new Handler();
    /* access modifiers changed from: private */
    public f.b r;
    private RelativeLayout s;
    /* access modifiers changed from: private */
    public ProgressDialog t = null;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        PlayerService.a(true);
        super.onCreate(bundle);
        Intent intent = getIntent();
        com.shoujiduoduo.base.a.a.a(f919a, "intent = " + intent);
        if (intent != null) {
            this.j = intent.getStringExtra("listid");
            this.k = intent.getStringExtra("listtype");
            com.shoujiduoduo.base.a.a.a(f919a, "mListId = " + this.j);
            this.i = (RingData) intent.getExtras().getParcelable("ringdata");
            this.o = intent.getBooleanExtra("freering", false);
            this.p = intent.getIntExtra("apppoints", 500);
            int intExtra = intent.getIntExtra("operator_type", 0);
            if (intExtra == 0) {
                this.r = f.b.f1671a;
            } else if (intExtra == 1) {
                this.r = f.b.ct;
            } else {
                this.r = f.b.cu;
            }
            com.shoujiduoduo.base.a.a.a(f919a, "operator type:" + this.r);
            com.shoujiduoduo.base.a.a.a(f919a, "name = " + this.i.e);
            com.shoujiduoduo.base.a.a.a(f919a, "artist = " + this.i.f);
            com.shoujiduoduo.base.a.a.a(f919a, "rid = " + this.i.g);
            com.shoujiduoduo.base.a.a.a(f919a, "low aac bitrate = " + this.i.f());
            com.shoujiduoduo.base.a.a.a(f919a, "price = " + this.i.p);
            com.shoujiduoduo.base.a.a.a(f919a, "high aac url = " + this.i.c());
        }
        setContentView((int) R.layout.dialog_give_cailing);
        this.l = (TextView) findViewById(R.id.give_cailing_title);
        if (this.o) {
            this.l.setText("积分兑换彩铃");
        }
        this.b = (ImageButton) findViewById(R.id.buy_cailing_close);
        this.b.setOnClickListener(new bj(this));
        this.d = (ListView) findViewById(R.id.cailing_info_list);
        this.d.setAdapter((ListAdapter) new b(this, null));
        this.d.setEnabled(false);
        this.e = (EditText) findViewById(R.id.give_cailing_phone_num);
        this.s = (RelativeLayout) findViewById(R.id.random_key_auth_layout);
        if (this.r == f.b.ct) {
            this.s.setVisibility(0);
        } else {
            this.s.setVisibility(8);
        }
        this.g = (EditText) findViewById(R.id.et_phone_code);
        this.f = (EditText) findViewById(R.id.et_phone_no);
        String a2 = as.a(getApplicationContext(), "pref_phone_num", "");
        if (!TextUtils.isEmpty(a2)) {
            this.f.setText(a2);
        }
        this.h = (ImageButton) findViewById(R.id.btn_get_code);
        this.h.setOnClickListener(new bp(this));
        this.m = (ImageButton) findViewById(R.id.give_cailing_open_contact);
        this.m.setOnClickListener(new bq(this));
        this.c = (Button) findViewById(R.id.buy_cailing);
        if (this.o) {
            this.c.setText("免费兑换彩铃");
        }
        this.c.setOnClickListener(new br(this));
    }

    /* access modifiers changed from: private */
    public void c(String str) {
        com.shoujiduoduo.util.d.b.a().d(str, new bs(this));
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        this.q.post(new bt(this, str));
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4) {
            return super.onKeyDown(i2, keyEvent);
        }
        finish();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        PlayerService.a(false);
        super.onDestroy();
    }

    /* access modifiers changed from: private */
    public void c() {
        String obj = this.e.getText().toString();
        if (TextUtils.isEmpty(obj)) {
            obj = f.b();
        }
        if (f.f(obj)) {
            b("请稍候...");
            com.shoujiduoduo.util.c.b.a().e(obj, this.i.n, new bu(this));
            return;
        }
        Toast.makeText(this, "请输入正确的手机号", 1).show();
    }

    /* access modifiers changed from: private */
    public void d() {
        String obj = this.f.getText().toString();
        String obj2 = this.e.getText().toString();
        String obj3 = this.g.getText().toString();
        if (obj == null || !f.f(obj)) {
            this.f.setError("正确输入您的手机号");
        } else if (obj2 == null || !f.f(obj2)) {
            this.e.setError("正确输入被赠送者的手机号");
        } else if (obj3 == null || obj3.length() != 6) {
            this.g.setError("请输入正确的验证码");
        } else {
            as.c(this, "pref_phone_num", obj);
            b("请稍候...");
            com.shoujiduoduo.util.d.b.a().g(obj, new bx(this, obj2, obj, obj3));
        }
    }

    /* access modifiers changed from: private */
    public void a(String str, String str2, String str3) {
        String str4 = str;
        String str5 = str2;
        String str6 = str3;
        com.shoujiduoduo.util.d.b.a().a(str4, str5, this.i.s, str6, "&ctcid=" + this.i.s + "&phone=" + str, new ca(this));
    }

    private class a implements DialogInterface.OnClickListener {
        private a() {
        }

        /* synthetic */ a(GiveCailingActivity giveCailingActivity, bj bjVar) {
            this();
        }

        public void onClick(DialogInterface dialogInterface, int i) {
            GiveCailingActivity.this.finish();
        }
    }

    /* access modifiers changed from: private */
    public void e() {
        String obj = this.e.getText().toString();
        if (f.f(obj)) {
            b("请稍候...");
            com.shoujiduoduo.util.c.b.a().f(obj, this.i.n, new bk(this));
            return;
        }
        Toast.makeText(this, "请输入正确的手机号", 1).show();
    }

    /* access modifiers changed from: package-private */
    public void b(String str) {
        this.q.post(new bm(this, str));
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.q.post(new bn(this));
    }

    private class b extends BaseAdapter {
        private b() {
        }

        /* synthetic */ b(GiveCailingActivity giveCailingActivity, bj bjVar) {
            this();
        }

        public int getCount() {
            return 4;
        }

        public Object getItem(int i) {
            return null;
        }

        public long getItemId(int i) {
            return (long) i;
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            String str;
            View inflate = GiveCailingActivity.this.getLayoutInflater().inflate((int) R.layout.listitem_buy_cailing, (ViewGroup) null);
            TextView textView = (TextView) inflate.findViewById(R.id.cailing_info_des);
            TextView textView2 = (TextView) inflate.findViewById(R.id.cailing_info_content);
            switch (i) {
                case 0:
                    textView.setText("歌曲名");
                    textView2.setText(GiveCailingActivity.this.i.e);
                    break;
                case 1:
                    textView.setText("歌   手");
                    textView2.setText(GiveCailingActivity.this.i.f);
                    break;
                case 2:
                    textView.setText("彩铃价格");
                    int i2 = GiveCailingActivity.this.i.p;
                    if (i2 == 0) {
                        str = "免费";
                    } else {
                        str = String.valueOf(((double) ((float) i2)) / 100.0d) + "元";
                    }
                    if (!GiveCailingActivity.this.o) {
                        textView2.setText(str);
                        break;
                    } else {
                        textView2.setText(Html.fromHtml("2元 <font color=\"#68ad2e\">(" + GiveCailingActivity.this.p + "个积分兑换)</font>"));
                        break;
                    }
                case 3:
                    textView.setText("有效期");
                    textView2.setText(GiveCailingActivity.this.i.o.equals("") ? "2016-06-30" : GiveCailingActivity.this.i.o);
                    break;
            }
            return inflate;
        }
    }

    public void onActivityResult(int i2, int i3, Intent intent) {
        String str;
        Cursor query;
        super.onActivityResult(i2, i3, intent);
        this.n.clear();
        switch (i2) {
            case 780621:
                if (i3 == -1) {
                    Cursor managedQuery = managedQuery(intent.getData(), null, null, null, null);
                    if (managedQuery.moveToFirst()) {
                        String string = managedQuery.getString(managedQuery.getColumnIndexOrThrow("_id"));
                        if (managedQuery.getString(managedQuery.getColumnIndex("has_phone_number")).equalsIgnoreCase("1") && (query = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, "contact_id = " + string, null, null)) != null) {
                            if (query.moveToFirst()) {
                                String string2 = query.getString(query.getColumnIndex("data1"));
                                com.shoujiduoduo.base.a.a.a(f919a, "number is:" + string2);
                                if (string2 != null && string2.length() > 0) {
                                    this.n.add(string2);
                                }
                                while (query.moveToNext()) {
                                    String string3 = query.getString(query.getColumnIndex("data1"));
                                    com.shoujiduoduo.base.a.a.a(f919a, "number is:" + string3);
                                    if (string3 != null && string3.length() > 0) {
                                        this.n.add(string3);
                                    }
                                }
                            }
                            query.close();
                        }
                        String string4 = managedQuery.getString(managedQuery.getColumnIndex("display_name"));
                        com.shoujiduoduo.base.a.a.a(f919a, "name is:" + string4);
                        str = string4;
                    } else {
                        str = "";
                    }
                    if (this.n.size() == 1) {
                        String str2 = this.n.get(0);
                        if (str2 != null) {
                            str2 = str2.trim().replace(" ", "").replace("-", "");
                        }
                        this.e.setText(str2);
                        return;
                    } else if (this.n.size() > 1) {
                        String[] strArr = new String[this.n.size()];
                        for (int i4 = 0; i4 < this.n.size(); i4++) {
                            strArr[i4] = this.n.get(i4);
                        }
                        new AlertDialog.Builder(this).setTitle(str).setSingleChoiceItems(strArr, 0, new bo(this, strArr)).show();
                        return;
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            default:
                return;
        }
    }
}
