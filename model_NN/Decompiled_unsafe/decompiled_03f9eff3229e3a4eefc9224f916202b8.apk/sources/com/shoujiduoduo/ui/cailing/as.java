package com.shoujiduoduo.ui.cailing;

import android.app.Activity;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ui.cailing.ak;
import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.f;

/* compiled from: CailingMenu */
class as extends a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ak.c f947a;

    as(ak.c cVar) {
        this.f947a = cVar;
    }

    public void a(c.b bVar) {
        ak.this.c();
        com.shoujiduoduo.util.as.b(ak.this.e, "NeedUpdateCaiLingLib", 1);
        switch (ak.this.n) {
            case buy:
                ak.this.j();
                return;
            case give:
                ak.this.k();
                return;
            case manage:
                ak.this.i();
                return;
            case openMem:
                ak.this.h();
                return;
            case f937a:
            default:
                return;
        }
    }

    public void b(c.b bVar) {
        ak.this.c();
        if (ak.this.e != null && !((Activity) ak.this.e).isFinishing()) {
            new cw(ak.this.e, R.style.DuoDuoDialog, ak.this.q, f.b.f1671a).show();
        }
    }
}
