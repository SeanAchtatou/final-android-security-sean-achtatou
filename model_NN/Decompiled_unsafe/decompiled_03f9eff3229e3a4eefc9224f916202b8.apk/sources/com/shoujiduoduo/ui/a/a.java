package com.shoujiduoduo.ui.a;

import android.app.Activity;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import cn.banshenggua.aichang.entry.RoomsEntryFragment;
import com.shoujiduoduo.ringtone.R;

/* compiled from: AcView */
public class a {

    /* renamed from: a  reason: collision with root package name */
    private Activity f881a;

    public a(Activity activity) {
        this.f881a = activity;
    }

    public void a() {
        RoomsEntryFragment newInstance = RoomsEntryFragment.newInstance();
        FragmentTransaction beginTransaction = ((FragmentActivity) this.f881a).getSupportFragmentManager().beginTransaction();
        beginTransaction.replace(R.id.aichang_layout, newInstance);
        beginTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        beginTransaction.commitAllowingStateLoss();
        newInstance.getFragmentManager().executePendingTransactions();
    }
}
