package com.shoujiduoduo.ui.category;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.b.a.b;
import com.shoujiduoduo.b.b.a;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ui.utils.v;
import com.shoujiduoduo.util.ak;
import com.shoujiduoduo.util.av;
import com.shoujiduoduo.util.f;
import com.shoujiduoduo.util.u;
import com.shoujiduoduo.util.widget.MyGallery;
import com.shoujiduoduo.util.widget.MyGridView;
import com.shoujiduoduo.util.widget.WebViewActivity;
import com.shoujiduoduo.util.widget.a;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.HashMap;

public class CategoryListFrag extends Fragment {

    /* renamed from: a  reason: collision with root package name */
    private MyGridView f1104a;
    /* access modifiers changed from: private */
    public c b;
    /* access modifiers changed from: private */
    public MyGallery c;
    /* access modifiers changed from: private */
    public a d;
    private RelativeLayout e;
    /* access modifiers changed from: private */
    public LinearLayout f;
    /* access modifiers changed from: private */
    public ArrayList<a.C0019a> g;
    /* access modifiers changed from: private */
    public ArrayList<b.a> h;
    /* access modifiers changed from: private */
    public d i;
    private com.shoujiduoduo.a.c.b j = new a(this);
    private com.shoujiduoduo.a.c.d k = new b(this);
    private AdapterView.OnItemClickListener l = new c(this);

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.i = new d(this, null);
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate((int) R.layout.category_entrance, viewGroup, false);
        this.f1104a = (MyGridView) inflate.findViewById(R.id.categoryGridView);
        this.e = (RelativeLayout) inflate.findViewById(R.id.recommend_adv_panel);
        this.c = (MyGallery) inflate.findViewById(R.id.recommend_adv);
        this.f = (LinearLayout) inflate.findViewById(R.id.recommend_hint_panel);
        this.b = new c();
        this.f1104a.setAdapter((ListAdapter) this.b);
        this.e.setVisibility(8);
        this.d = new a();
        this.c.setAdapter((SpinnerAdapter) this.d);
        this.c.setOnItemClickListener(new b(this, null));
        this.f1104a.setOnItemClickListener(this.l);
        x.a().a(com.shoujiduoduo.a.a.b.OBSERVER_CATEGORY, this.k);
        x.a().a(com.shoujiduoduo.a.a.b.OBSERVER_BANNER_AD, this.j);
        c();
        a();
        return inflate;
    }

    public void onDestroyView() {
        super.onDestroyView();
        x.a().b(com.shoujiduoduo.a.a.b.OBSERVER_CATEGORY, this.k);
        x.a().b(com.shoujiduoduo.a.a.b.OBSERVER_BANNER_AD, this.j);
    }

    public void onDetach() {
        super.onDetach();
    }

    private class d extends Handler {
        private d() {
        }

        /* synthetic */ d(CategoryListFrag categoryListFrag, a aVar) {
            this();
        }

        public void handleMessage(Message message) {
            super.handleMessage(message);
            if (message.what == 1136) {
                CategoryListFrag.this.b.notifyDataSetChanged();
            } else if (message.what == 1) {
                if (CategoryListFrag.this.c.isShown()) {
                    CategoryListFrag.this.c.b();
                }
                CategoryListFrag.this.i.sendEmptyMessageDelayed(1, 3000);
            }
        }
    }

    private class c extends BaseAdapter {
        private LayoutInflater b = ((LayoutInflater) RingDDApp.c().getSystemService("layout_inflater"));

        public c() {
        }

        public int getCount() {
            if (CategoryListFrag.this.g == null) {
                return 0;
            }
            return CategoryListFrag.this.g.size();
        }

        public Object getItem(int i) {
            if (CategoryListFrag.this.g == null) {
                return null;
            }
            return CategoryListFrag.this.g.get(i);
        }

        public long getItemId(int i) {
            return (long) i;
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            if (view == null) {
                view = this.b.inflate((int) R.layout.listitem_category_item, (ViewGroup) null);
            }
            ImageView imageView = (ImageView) view.findViewById(R.id.category_item_pic);
            String str = ((a.C0019a) CategoryListFrag.this.g.get(i)).d;
            if (CategoryListFrag.this.a(str) != 0) {
                imageView.setImageResource(CategoryListFrag.this.a(str));
            } else {
                com.d.a.b.d.a().a(((a.C0019a) CategoryListFrag.this.g.get(i)).b, imageView, v.a().e());
            }
            ((TextView) view.findViewById(R.id.category_item_name)).setText(((a.C0019a) CategoryListFrag.this.g.get(i)).f751a);
            return view;
        }
    }

    /* access modifiers changed from: private */
    public int a(String str) {
        if (str.equals("2")) {
            return R.drawable.cate_2_normal;
        }
        if (str.equals("3")) {
            return R.drawable.cate_3_normal;
        }
        if (str.equals("4")) {
            return R.drawable.cate_4_normal;
        }
        if (str.equals("5")) {
            return R.drawable.cate_5_normal;
        }
        if (str.equals(Constants.VIA_SHARE_TYPE_INFO)) {
            return R.drawable.cate_6_normal;
        }
        if (str.equals("7")) {
            return R.drawable.cate_7_normal;
        }
        if (str.equals("8")) {
            return R.drawable.cate_8_normal;
        }
        if (str.equals("9")) {
            return R.drawable.cate_9_normal;
        }
        if (str.equals("101")) {
            return R.drawable.cate_101_normal;
        }
        if (str.equals("102")) {
            return R.drawable.cate_102_normal;
        }
        if (str.equals("103")) {
            return R.drawable.cate_103_normal;
        }
        if (str.equals("104")) {
            return R.drawable.cate_104_normal;
        }
        if (str.equals("aichang")) {
            return R.drawable.aichang_tuiguang;
        }
        return 0;
    }

    private class a extends BaseAdapter {
        private LayoutInflater b = ((LayoutInflater) RingDDApp.c().getSystemService("layout_inflater"));

        public a() {
        }

        public int getCount() {
            if (CategoryListFrag.this.h == null || CategoryListFrag.this.h.size() == 0) {
                return 0;
            }
            return Integer.MAX_VALUE;
        }

        public Object getItem(int i) {
            if (CategoryListFrag.this.h == null || CategoryListFrag.this.h.size() == 0) {
                return null;
            }
            return CategoryListFrag.this.h.get(i % CategoryListFrag.this.h.size());
        }

        public long getItemId(int i) {
            return (long) i;
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            Drawable a2;
            com.shoujiduoduo.base.a.a.a("CategoryListFrag", "Gallery adapter getview, pos:" + i);
            View inflate = this.b.inflate((int) R.layout.advertisement_item, (ViewGroup) null);
            ImageView imageView = (ImageView) inflate.findViewById(R.id.advertisement_image);
            if (!(CategoryListFrag.this.h == null || CategoryListFrag.this.h.size() <= 0 || CategoryListFrag.this.h.get(i % CategoryListFrag.this.h.size()) == null || (a2 = com.shoujiduoduo.ui.utils.d.a(((b.a) CategoryListFrag.this.h.get(i % CategoryListFrag.this.h.size())).e, new f(this, imageView))) == null)) {
                com.shoujiduoduo.base.a.a.a("CategoryListFrag", "set image drawable 2");
                imageView.setImageDrawable(a2);
            }
            return inflate;
        }
    }

    private class b implements AdapterView.OnItemClickListener {
        private b() {
        }

        /* synthetic */ b(CategoryListFrag categoryListFrag, a aVar) {
            this();
        }

        public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            String str = ((b.a) CategoryListFrag.this.h.get(i % CategoryListFrag.this.h.size())).f735a;
            String str2 = ((b.a) CategoryListFrag.this.h.get(i % CategoryListFrag.this.h.size())).b;
            HashMap hashMap = new HashMap();
            hashMap.put("type", str + " - " + str2);
            com.umeng.analytics.b.a(RingDDApp.c(), "CLICK_BANNER_AD", hashMap);
            if (str.equals("down")) {
                if (!TextUtils.isEmpty(((b.a) CategoryListFrag.this.h.get(i % CategoryListFrag.this.h.size())).f)) {
                    new a.C0034a(CategoryListFrag.this.getActivity()).b((int) R.string.hint).a(((b.a) CategoryListFrag.this.h.get(i % CategoryListFrag.this.h.size())).c).a((int) R.string.ok, new h(this, i)).b((int) R.string.cancel, new g(this)).a().show();
                }
            } else if (str.equals("web")) {
                Intent intent = new Intent(CategoryListFrag.this.getActivity(), WebViewActivity.class);
                intent.putExtra("url", ((b.a) CategoryListFrag.this.h.get(i % CategoryListFrag.this.h.size())).f);
                intent.putExtra("apkname", ((b.a) CategoryListFrag.this.h.get(i % CategoryListFrag.this.h.size())).b);
                CategoryListFrag.this.getActivity().startActivity(intent);
            } else if (str.equals("search")) {
                x.a().a(com.shoujiduoduo.a.a.b.OBSERVER_CATEGORY, new i(this, i));
            } else if (str.equals("list")) {
                x.a().a(com.shoujiduoduo.a.a.b.OBSERVER_CATEGORY, new j(this, i));
                PlayerService b = ak.a().b();
                if (b != null && b.j()) {
                    b.k();
                }
            } else {
                com.shoujiduoduo.base.a.a.c("CategoryListFrag", "not support ad type:" + str);
            }
        }
    }

    private void a() {
        this.g = b();
        this.b.notifyDataSetChanged();
    }

    /* access modifiers changed from: private */
    public ArrayList<a.C0019a> b() {
        ArrayList<a.C0019a> c2 = com.shoujiduoduo.a.b.b.d().c();
        if ("true".equals(com.umeng.analytics.b.c(RingDDApp.c(), "category_aichang_switch")) && !f.a("cn.banshenggua.aichang") && !com.shoujiduoduo.util.a.b() && c2 != null) {
            String c3 = com.umeng.analytics.b.c(RingDDApp.c(), "category_aichang_name");
            String c4 = com.umeng.analytics.b.c(RingDDApp.c(), "category_aichang_pos");
            a.C0019a aVar = new a.C0019a();
            aVar.d = "aichang";
            if (av.c(c3)) {
                c3 = "美女陪唱";
            }
            aVar.f751a = c3;
            int a2 = u.a(c4, 3);
            if (a2 < 0 || a2 > c2.size()) {
                c2.add(aVar);
            } else {
                c2.add(a2, aVar);
            }
        }
        return c2;
    }

    /* access modifiers changed from: private */
    public void c() {
        if (com.shoujiduoduo.a.b.b.c().c()) {
            com.shoujiduoduo.base.a.a.a("CategoryListFrag", "banner ad data is ready");
            this.h = com.shoujiduoduo.a.b.b.c().d();
            if (this.h.size() > 0) {
                this.e.setVisibility(0);
                this.d.notifyDataSetChanged();
                if (this.h.size() > 1) {
                    this.i.sendEmptyMessageDelayed(1, 3000);
                }
                this.e.setLayoutParams(new LinearLayout.LayoutParams(-1, f.a((float) com.shoujiduoduo.a.b.b.c().e())));
                int size = this.h.size();
                for (int i2 = 0; i2 < size; i2++) {
                    ImageView imageView = new ImageView(getActivity());
                    imageView.setBackgroundResource(R.drawable.adv_hint_normal);
                    this.f.addView(imageView, new LinearLayout.LayoutParams(-2, -2));
                }
                this.c.setSelection(0);
                this.c.setOnItemSelectedListener(new e(this, size));
                return;
            }
            this.e.setVisibility(8);
            return;
        }
        com.shoujiduoduo.base.a.a.a("CategoryListFrag", "banner ad data is not ready");
    }
}
