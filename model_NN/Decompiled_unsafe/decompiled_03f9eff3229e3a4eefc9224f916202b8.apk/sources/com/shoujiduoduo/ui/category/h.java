package com.shoujiduoduo.ui.category;

import android.content.DialogInterface;
import com.shoujiduoduo.b.a.b;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ui.category.CategoryListFrag;
import com.shoujiduoduo.util.q;
import java.util.HashMap;

/* compiled from: CategoryListFrag */
class h implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f1115a;
    final /* synthetic */ CategoryListFrag.b b;

    h(CategoryListFrag.b bVar, int i) {
        this.b = bVar;
        this.f1115a = i;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        q.a(CategoryListFrag.this.getActivity()).a(((b.a) CategoryListFrag.this.h.get(this.f1115a % CategoryListFrag.this.h.size())).f, ((b.a) CategoryListFrag.this.h.get(this.f1115a % CategoryListFrag.this.h.size())).b);
        dialogInterface.dismiss();
        HashMap hashMap = new HashMap();
        hashMap.put("type", "ok");
        com.umeng.analytics.b.a(RingDDApp.c(), "APK_DOWN_DIALOG_CLICK", hashMap);
    }
}
