package com.shoujiduoduo.ui.category;

import android.content.DialogInterface;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ui.category.CategoryListFrag;
import com.umeng.analytics.b;
import java.util.HashMap;

/* compiled from: CategoryListFrag */
class g implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CategoryListFrag.b f1114a;

    g(CategoryListFrag.b bVar) {
        this.f1114a = bVar;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        dialogInterface.dismiss();
        HashMap hashMap = new HashMap();
        hashMap.put("type", "cancel");
        b.a(RingDDApp.c(), "APK_DOWN_DIALOG_CLICK", hashMap);
    }
}
