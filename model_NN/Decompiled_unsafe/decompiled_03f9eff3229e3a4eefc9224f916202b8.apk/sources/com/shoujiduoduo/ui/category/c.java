package com.shoujiduoduo.ui.category;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import cn.banshenggua.aichang.utils.Constants;
import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.b.b.a;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ui.search.AcActivity;
import com.shoujiduoduo.util.ak;
import com.umeng.analytics.b;
import java.util.HashMap;

/* compiled from: CategoryListFrag */
class c implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CategoryListFrag f1111a;

    c(CategoryListFrag categoryListFrag) {
        this.f1111a = categoryListFrag;
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        a.C0019a aVar = (a.C0019a) this.f1111a.g.get(i);
        HashMap hashMap = new HashMap();
        hashMap.put(Constants.ITEM, aVar.d + "_" + aVar.f751a);
        b.a(RingDDApp.c(), "category_click", hashMap);
        if (aVar.d.equals("aichang")) {
            com.shoujiduoduo.base.a.a.a("search_ad", "click ad, aichang");
            this.f1111a.getActivity().startActivity(new Intent(this.f1111a.getActivity(), AcActivity.class));
        } else {
            x.a().b(com.shoujiduoduo.a.a.b.OBSERVER_CATEGORY, new d(this, aVar));
        }
        PlayerService b = ak.a().b();
        if (b != null && b.j()) {
            b.k();
        }
    }
}
