package com.shoujiduoduo.ui.category;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.a.c.d;
import com.shoujiduoduo.a.c.l;
import com.shoujiduoduo.b.a.t;
import com.shoujiduoduo.b.c.n;
import com.shoujiduoduo.base.bean.f;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ui.utils.DDListFragment;
import com.shoujiduoduo.ui.utils.y;
import com.shoujiduoduo.util.ak;
import com.shoujiduoduo.util.av;
import com.sina.weibo.sdk.component.WidgetRequestParam;
import com.tencent.connect.common.Constants;
import com.umeng.analytics.b;

/* compiled from: CategoryScene */
public class k {

    /* renamed from: a  reason: collision with root package name */
    private static final String f1116a = k.class.getSimpleName();
    /* access modifiers changed from: private */
    public Activity b;
    private ImageButton c;
    private TextView d;
    private RelativeLayout e;
    /* access modifiers changed from: private */
    public String f;
    private y g;
    /* access modifiers changed from: private */
    public n h = null;
    /* access modifiers changed from: private */
    public DDListFragment i;
    private CategoryListFrag j;
    private boolean k;
    /* access modifiers changed from: private */
    public a l;
    private com.shoujiduoduo.ui.utils.a m;
    private boolean n;
    private l o = new l(this);
    private d p = new m(this);

    /* compiled from: CategoryScene */
    public interface a {
        void a(String str);
    }

    public k(Activity activity) {
        this.b = activity;
        try {
            this.l = (a) activity;
        } catch (ClassCastException e2) {
            e2.printStackTrace();
            throw new ClassCastException(activity.toString() + "must implements ICategoryChangeListener");
        }
    }

    public void a() {
        this.g = new y(this.b);
        this.i = new DDListFragment();
        this.j = new CategoryListFrag();
        this.n = com.shoujiduoduo.util.a.d();
        Bundle bundle = new Bundle();
        bundle.putString("adapter_type", "ring_list_adapter");
        String c2 = b.c(RingDDApp.c(), "feed_ad_list_id");
        if ((av.b(c2) || c2.contains(WidgetRequestParam.REQ_PARAM_COMMENT_CATEGORY)) && this.n) {
            bundle.putBoolean("support_feed_ad", true);
        }
        this.i.setArguments(bundle);
        this.m = new com.shoujiduoduo.ui.utils.a(this.b);
        this.i.a(this.m.a());
        FragmentTransaction beginTransaction = ((FragmentActivity) this.b).getSupportFragmentManager().beginTransaction();
        beginTransaction.add((int) R.id.ringlist_frag, this.i);
        beginTransaction.add((int) R.id.category_frag, this.j);
        beginTransaction.hide(this.i);
        beginTransaction.commit();
        x.a().a(com.shoujiduoduo.a.a.b.OBSERVER_CATEGORY, this.p);
        x.a().a(com.shoujiduoduo.a.a.b.OBSERVER_CHANGE_BATCH, this.o);
    }

    public void b() {
        x.a().b(com.shoujiduoduo.a.a.b.OBSERVER_CATEGORY, this.p);
        x.a().b(com.shoujiduoduo.a.a.b.OBSERVER_CHANGE_BATCH, this.o);
    }

    public void c() {
        PlayerService b2 = ak.a().b();
        if (b2 != null && b2.j()) {
            b2.k();
        }
        e();
        this.h = null;
        this.l.a("");
    }

    public void a(ImageButton imageButton, TextView textView, RelativeLayout relativeLayout) {
        int i2;
        int i3;
        int i4 = 0;
        this.c = imageButton;
        this.d = textView;
        this.e = relativeLayout;
        this.c.setOnClickListener(new n(this));
        ImageButton imageButton2 = this.c;
        if (this.h == null) {
            i2 = 4;
        } else {
            i2 = 0;
        }
        imageButton2.setVisibility(i2);
        TextView textView2 = this.d;
        if (this.h == null) {
            i3 = 4;
        } else {
            i3 = 0;
        }
        textView2.setVisibility(i3);
        this.d.setText(this.f);
        RelativeLayout relativeLayout2 = this.e;
        if (this.h != null) {
            i4 = 4;
        }
        relativeLayout2.setVisibility(i4);
    }

    private void e() {
        FragmentTransaction beginTransaction = ((FragmentActivity) this.b).getSupportFragmentManager().beginTransaction();
        if (this.j.isHidden()) {
            beginTransaction.show(this.j);
        }
        if (this.i.isVisible()) {
            beginTransaction.hide(this.i);
        }
        beginTransaction.commitAllowingStateLoss();
        this.c.setVisibility(4);
        this.d.setVisibility(4);
        this.e.setVisibility(0);
        this.k = false;
    }

    public boolean d() {
        return this.k;
    }

    /* access modifiers changed from: private */
    public void a(String str, String str2) {
        if (this.c != null) {
            this.h = new n(f.a.list_ring_category, str2, false, "");
            this.i.a(this.h);
            this.c.setVisibility(0);
            this.e.setVisibility(4);
            this.d.setVisibility(0);
            this.d.setText(str);
            if (!str2.equals(Constants.VIA_SHARE_TYPE_INFO)) {
                this.m.a(false);
            } else if (com.shoujiduoduo.a.b.b.c().f()) {
                t.a a2 = com.shoujiduoduo.a.b.b.c().a("DJ");
                if (a2 != null) {
                    this.m.a(a2);
                    this.m.a(true);
                    com.shoujiduoduo.base.a.a.a(f1116a, "显示DJ广告， " + a2.toString());
                } else {
                    com.shoujiduoduo.base.a.a.a(f1116a, "没有匹配检索词的搜索广告");
                    this.m.a(false);
                }
            } else {
                this.m.a(false);
                com.shoujiduoduo.base.a.a.a(f1116a, "检索广告数据尚未获取");
            }
            FragmentTransaction beginTransaction = ((FragmentActivity) this.b).getSupportFragmentManager().beginTransaction();
            if (this.j.isVisible()) {
                beginTransaction.hide(this.j);
            }
            if (this.i.isHidden()) {
                beginTransaction.show(this.i);
            }
            beginTransaction.commitAllowingStateLoss();
            this.k = true;
        }
    }
}
