package com.shoujiduoduo.ui.category;

import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.ui.category.CategoryListFrag;
import com.shoujiduoduo.ui.utils.d;

/* compiled from: CategoryListFrag */
class f implements d.a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ImageView f1113a;
    final /* synthetic */ CategoryListFrag.a b;

    f(CategoryListFrag.a aVar, ImageView imageView) {
        this.b = aVar;
        this.f1113a = imageView;
    }

    public void a(Drawable drawable, String str) {
        if (!CategoryListFrag.this.getActivity().isFinishing() && drawable != null) {
            a.a("CategoryListFrag", "set image drawable 1");
            this.f1113a.setImageDrawable(drawable);
        }
    }
}
