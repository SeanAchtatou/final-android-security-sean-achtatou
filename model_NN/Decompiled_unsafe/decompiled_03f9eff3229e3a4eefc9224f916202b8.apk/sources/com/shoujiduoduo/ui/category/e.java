package com.shoujiduoduo.ui.category;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import com.shoujiduoduo.ringtone.R;

/* compiled from: CategoryListFrag */
class e implements AdapterView.OnItemSelectedListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f1112a;
    final /* synthetic */ CategoryListFrag b;

    e(CategoryListFrag categoryListFrag, int i) {
        this.b = categoryListFrag;
        this.f1112a = i;
    }

    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long j) {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < this.f1112a) {
                ImageView imageView = (ImageView) this.b.f.getChildAt(i3 % this.f1112a);
                if (i3 == i % this.f1112a) {
                    imageView.setBackgroundResource(R.drawable.adv_hint_selected);
                } else {
                    imageView.setBackgroundResource(R.drawable.adv_hint_normal);
                }
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }

    public void onNothingSelected(AdapterView<?> adapterView) {
    }
}
