package com.shoujiduoduo.base.a;

import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.util.f;

/* compiled from: DDLog */
public final class a {

    /* renamed from: a  reason: collision with root package name */
    public static boolean f812a = false;
    public static final f.b b = f.b.d;

    static {
        b.f813a = true;
    }

    public static void a(String str, String str2) {
        if (f812a) {
            b.b("DDLog: " + str, str2);
        }
    }

    public static void b(String str, String str2) {
        if (f812a) {
            b.d("DDlog: " + str, str2);
        }
    }

    public static void c(String str, String str2) {
        if (f812a) {
            b.e("DDlog: " + str, str2);
        }
    }

    public static void d(String str, String str2) {
        if (f812a) {
            b.a("DDlog: " + str, str2);
        }
    }

    public static void e(String str, String str2) {
        if (f812a) {
            b.c("DDlog: " + str, str2);
        }
    }

    public static void a(Exception exc) {
        if (f812a) {
            b.a("DuoduoException", exc);
        }
    }

    public static void a() {
        if (Thread.currentThread().getId() != RingDDApp.d()) {
            c("DDLog", "必须在主线程中调用");
        }
    }

    public static void a(Class<?> cls, Object obj) {
        if (!cls.isInstance(obj)) {
            c("DDLog", obj.toString() + "必须是" + cls.toString() + "的子类");
        }
    }
}
