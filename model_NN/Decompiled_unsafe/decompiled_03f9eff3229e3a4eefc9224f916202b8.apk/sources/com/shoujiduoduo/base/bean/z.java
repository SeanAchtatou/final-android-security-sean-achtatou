package com.shoujiduoduo.base.bean;

import android.os.Parcel;
import android.os.Parcelable;

/* compiled from: RingData */
final class z implements Parcelable.Creator<RingData> {
    z() {
    }

    /* renamed from: a */
    public RingData createFromParcel(Parcel parcel) {
        RingData ringData = new RingData();
        ringData.e = parcel.readString();
        ringData.f = parcel.readString();
        ringData.g = parcel.readString();
        ringData.h = parcel.readString();
        ringData.i = parcel.readInt();
        ringData.j = parcel.readInt();
        ringData.k = parcel.readInt();
        String unused = ringData.b = parcel.readString();
        int unused2 = ringData.c = parcel.readInt();
        String unused3 = ringData.d = parcel.readString();
        int unused4 = ringData.F = parcel.readInt();
        ringData.n = parcel.readString();
        ringData.o = parcel.readString();
        ringData.p = parcel.readInt();
        ringData.q = parcel.readInt();
        ringData.r = parcel.readString();
        ringData.l = parcel.readInt();
        ringData.s = parcel.readString();
        ringData.t = parcel.readString();
        ringData.u = parcel.readInt();
        ringData.v = parcel.readInt();
        ringData.w = parcel.readInt();
        ringData.x = parcel.readString();
        ringData.y = parcel.readInt();
        ringData.z = parcel.readString();
        ringData.A = parcel.readString();
        ringData.C = parcel.readString();
        ringData.B = parcel.readString();
        ringData.D = parcel.readString();
        ringData.E = parcel.readInt();
        return ringData;
    }

    /* renamed from: a */
    public RingData[] newArray(int i) {
        return new RingData[i];
    }
}
