package com.shoujiduoduo.base.bean;

public class MakeRingData extends RingData {

    /* renamed from: a  reason: collision with root package name */
    public int f814a;
    public int b;
    public String c = "";
    public int d;

    public MakeRingData a(RingData ringData) {
        this.f = ringData.f;
        this.h = ringData.h;
        this.n = ringData.n;
        this.g = ringData.g;
        this.j = ringData.j;
        this.q = ringData.q;
        a(ringData.a());
        a(ringData.d());
        b(ringData.c());
        this.l = ringData.l;
        b(ringData.f());
        c(ringData.e());
        this.r = ringData.r;
        this.e = ringData.e;
        this.k = ringData.k;
        this.p = ringData.p;
        this.i = ringData.i;
        this.o = ringData.o;
        this.m = ringData.m;
        if (ringData instanceof MakeRingData) {
            this.b = ((MakeRingData) ringData).b;
            this.f814a = ((MakeRingData) ringData).f814a;
            this.c = ((MakeRingData) ringData).c;
            this.d = ((MakeRingData) ringData).d;
        }
        return this;
    }
}
