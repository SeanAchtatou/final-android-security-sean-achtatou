package com.shoujiduoduo.base.bean;

import cn.banshenggua.aichang.utils.Constants;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.util.as;
import com.umeng.socialize.PlatformConfig;

/* compiled from: UserInfo */
public class ab {

    /* renamed from: a  reason: collision with root package name */
    private String f818a = "";
    private String b = "";
    private String c = "";
    private String d = "";
    private int e;
    private int f;
    private int g;
    private String h = "";

    public String a() {
        return this.f818a;
    }

    public void a(String str) {
        this.f818a = str;
    }

    public String b() {
        return this.b;
    }

    public void b(String str) {
        this.b = str;
    }

    public String c() {
        return this.c;
    }

    public void c(String str) {
        this.c = str;
    }

    public String d() {
        return this.d;
    }

    public int e() {
        return this.e;
    }

    public String f() {
        switch (this.e) {
            case 1:
                return "phone";
            case 2:
                return "qq";
            case 3:
                return Constants.WEIBO;
            case 4:
                return PlatformConfig.Renren.Name;
            case 5:
                return "weixin";
            default:
                return "";
        }
    }

    public void a(int i) {
        this.e = i;
    }

    public int g() {
        return this.g;
    }

    public void b(int i) {
        this.g = i;
    }

    public int h() {
        return this.f;
    }

    public void c(int i) {
        this.f = i;
    }

    public boolean i() {
        return this.f == 1;
    }

    public boolean j() {
        return this.g != 0 && i();
    }

    public void d(String str) {
        as.c(RingDDApp.c(), "pref_phone_num", str);
        this.h = str;
    }

    public String k() {
        return this.h;
    }
}
