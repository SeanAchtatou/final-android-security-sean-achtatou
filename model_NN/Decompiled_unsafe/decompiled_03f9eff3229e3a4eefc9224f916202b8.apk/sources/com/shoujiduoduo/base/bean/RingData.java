package com.shoujiduoduo.base.bean;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.shoujiduoduo.util.m;

public class RingData implements Parcelable {
    public static final Parcelable.Creator<RingData> CREATOR = new z();
    public String A = "";
    public String B = "";
    public String C = "";
    public String D = "";
    public int E = 0;
    /* access modifiers changed from: private */
    public int F;
    private String G = "";
    private int H;

    /* renamed from: a  reason: collision with root package name */
    private String f815a = "";
    /* access modifiers changed from: private */
    public String b = "";
    /* access modifiers changed from: private */
    public int c;
    /* access modifiers changed from: private */
    public String d = "";
    public String e = "";
    public String f = "";
    public String g = "";
    public String h = "";
    public int i;
    public int j;
    public int k;
    public int l;
    public String m = "";
    public String n = "";
    public String o = "";
    public int p;
    public int q = 1;
    public String r = "";
    public String s = "";
    public String t = "";
    public int u;
    public int v = 0;
    public int w;
    public String x = "";
    public int y;
    public String z = "";

    public String a() {
        return "http://" + m.a().c();
    }

    public void a(String str) {
        this.f815a = str;
    }

    public String b() {
        return a() + this.d;
    }

    public String c() {
        return this.d;
    }

    public void b(String str) {
        this.d = str;
    }

    public void a(int i2) {
        this.F = i2;
    }

    public int d() {
        return this.F;
    }

    public String e() {
        return this.b;
    }

    public void c(String str) {
        this.b = str;
    }

    public void b(int i2) {
        this.c = i2;
    }

    public int f() {
        return this.c;
    }

    public String g() {
        return this.G;
    }

    public void d(String str) {
        this.G = str;
    }

    public void c(int i2) {
        this.H = i2;
    }

    public int h() {
        return this.H;
    }

    public boolean i() {
        if (TextUtils.isEmpty(this.G) || this.H <= 0) {
            return false;
        }
        return true;
    }

    public boolean j() {
        if (!TextUtils.isEmpty(this.d) && this.F > 0) {
            return true;
        }
        if (TextUtils.isEmpty(this.b) || this.c <= 0) {
            return false;
        }
        return true;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("name: ").append(this.e).append(", artist: ").append(this.f).append(", rid: ").append(this.g).append(", duration: ").append(this.j).append(", score: ").append(this.i).append(", playcnt: ").append(this.k);
        return sb.toString();
    }

    public int k() {
        int i2 = 0;
        try {
            i2 = Integer.valueOf(this.g.equals("") ? "0" : this.g).intValue();
        } catch (NumberFormatException e2) {
            e2.printStackTrace();
        }
        if (this.n.equals("") || i2 > 900000000 || this.q == 1) {
            return i2;
        }
        return i2 + 1000000000;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        parcel.writeString(this.e);
        parcel.writeString(this.f);
        parcel.writeString(this.g);
        parcel.writeString(this.h);
        parcel.writeInt(this.i);
        parcel.writeInt(this.j);
        parcel.writeInt(this.k);
        parcel.writeString(this.b);
        parcel.writeInt(this.c);
        parcel.writeString(this.d);
        parcel.writeInt(this.F);
        parcel.writeString(this.n);
        parcel.writeString(this.o);
        parcel.writeInt(this.p);
        parcel.writeInt(this.q);
        parcel.writeString(this.r);
        parcel.writeInt(this.l);
        parcel.writeString(this.s);
        parcel.writeString(this.t);
        parcel.writeInt(this.u);
        parcel.writeInt(this.v);
        parcel.writeInt(this.w);
        parcel.writeString(this.x);
        parcel.writeInt(this.y);
        parcel.writeString(this.z);
        parcel.writeString(this.A);
        parcel.writeString(this.C);
        parcel.writeString(this.B);
        parcel.writeString(this.D);
        parcel.writeInt(this.E);
    }
}
