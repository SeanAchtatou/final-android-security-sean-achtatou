package com.shoujiduoduo.base.bean;

/* compiled from: ListType */
public final class f {

    /* compiled from: ListType */
    public enum a {
        list_artist,
        list_collect,
        list_user_collect,
        list_user_favorite,
        list_user_make,
        list_ring_recommon,
        list_ring_category,
        list_ring_artist,
        list_ring_collect,
        list_ring_search,
        list_ring_cmcc,
        list_ring_ctcc,
        list_ring_cucc,
        sys_ringtone,
        sys_notify,
        sys_alarm,
        list_simple;

        public abstract String toString();
    }
}
