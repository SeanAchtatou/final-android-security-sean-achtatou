package com.shoujiduoduo.ringtone;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Handler;
import cn.banshenggua.aichang.sdk.ACContext;
import cn.banshenggua.aichang.sdk.ACException;
import com.d.a.a.a.b.c;
import com.d.a.b.a.m;
import com.d.a.b.d;
import com.d.a.b.e;
import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.util.NetworkStateUtil;
import com.shoujiduoduo.util.am;
import com.shoujiduoduo.util.as;
import com.shoujiduoduo.util.f;
import com.shoujiduoduo.util.i;
import com.shoujiduoduo.util.t;
import com.umeng.analytics.b;
import com.umeng.socialize.PlatformConfig;
import java.util.HashMap;

public class RingDDApp extends Application {

    /* renamed from: a  reason: collision with root package name */
    public static String f847a;
    public static boolean b;
    /* access modifiers changed from: private */
    public static final String c = RingDDApp.class.getSimpleName();
    private static boolean d;
    /* access modifiers changed from: private */
    public static volatile boolean e;
    private static RingDDApp f = null;
    private static long g = Thread.currentThread().getId();
    private static Handler h = new Handler();
    private static boolean j;
    private static boolean l = false;
    private HashMap<String, Object> i;
    private BroadcastReceiver k = new f(this);

    public void onCreate() {
        super.onCreate();
        a.a(c, "\n\r\n\r");
        a.a(c, "App Class is created!, ThreadID = " + Thread.currentThread().getId());
        f = this;
        f.a(this);
        NetworkStateUtil.a(getApplicationContext());
        as.a(this);
        f.b bVar = f.b.d;
        try {
            f.u();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        t.a(this);
        am.a().b();
        this.i = new HashMap<>();
        i.a(new a(this));
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.shoujiduoduo.ringtone.exitapp");
        registerReceiver(this.k, intentFilter);
        h();
        k();
        try {
            ACContext.initContext(this);
        } catch (ACException e3) {
            e3.printStackTrace();
        } catch (Throwable th) {
        }
        j();
        a.a(c, "app version:" + f.q());
        a.a(c, "app install src:" + f.p());
        a.a(c, "device info:" + f.j());
        a.a(c, "os version:" + f.k());
        a.a(c, "Build.BRAND:" + Build.BRAND);
        a.a(c, "Build.MANUFACTURER:" + Build.MANUFACTURER);
        a.a(c, "Build.MODEL:" + Build.MODEL);
        a.a(c, "Build.DEVICE:" + Build.DEVICE);
        a.a(c, "Build.BOARD:" + Build.BOARD);
        a.a(c, "Build.DISPLAY:" + Build.DISPLAY);
        a.a(c, "Build.PRODUCT:" + Build.PRODUCT);
        a.a(c, "Build.BOOTLOADER:" + Build.BOOTLOADER);
    }

    private void j() {
        String a2 = as.a(this, "pref_load_cmcc_sunshine_sdk_start", "");
        String a3 = as.a(this, "pref_load_cmcc_sunshine_sdk_end", "");
        if (a2.equals("1") && !a3.equals("1")) {
            a(true);
            HashMap hashMap = new HashMap();
            hashMap.put("phone", Build.BRAND + "-" + Build.MODEL + "-" + Build.VERSION.SDK_INT);
            a.a(c, "sunshine sdk crash last time:" + Build.BRAND + "-" + Build.MODEL + "-" + Build.VERSION.RELEASE);
            b.a(this, "cmcc_sunshine_sdk_crash", hashMap);
        }
    }

    public boolean a() {
        return j;
    }

    public void a(boolean z) {
        j = z;
    }

    public static RingDDApp b() {
        return f;
    }

    public static Context c() {
        return f;
    }

    public static long d() {
        return g;
    }

    public static Handler e() {
        return h;
    }

    public static boolean f() {
        return e;
    }

    public static void g() {
        if (!d) {
            d = true;
            NetworkStateUtil.b(f.getApplicationContext());
            x.a().a(com.shoujiduoduo.a.a.b.OBSERVER_APP, new b());
            x.a().b(new c());
        }
    }

    public void onTerminate() {
        a.a(c, "App onTerminate.");
        unregisterReceiver(this.k);
        super.onTerminate();
    }

    public void a(String str, Object obj) {
        this.i.put(str, obj);
    }

    public Object a(String str) {
        if (this.i.containsKey(str)) {
            return this.i.get(str);
        }
        return null;
    }

    public static void h() {
        if (!l) {
            l = true;
            d.a().a(new e.a(c()).b(3).a().a(new c()).a(m.LIFO).c());
        }
    }

    private void k() {
        PlatformConfig.setWeixin("wxb4cd572ca73fd239", "48bccd48df1afa300a52bd75611a6710");
        PlatformConfig.setSinaWeibo("1981517408", "001b0b305f9cbb4f307866ea81cd473c");
        PlatformConfig.setQQZone("100382066", "2305e02edac0c67f822f453d92be2a66");
    }
}
