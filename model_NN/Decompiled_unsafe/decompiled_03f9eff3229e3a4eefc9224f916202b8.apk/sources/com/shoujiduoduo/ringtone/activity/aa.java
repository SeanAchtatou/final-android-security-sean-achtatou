package com.shoujiduoduo.ringtone.activity;

import android.content.Intent;
import android.view.View;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.util.av;
import com.shoujiduoduo.util.f;
import com.shoujiduoduo.util.q;
import com.shoujiduoduo.util.widget.WebViewActivity;
import com.umeng.analytics.b;
import java.util.HashMap;

/* compiled from: WelcomeActivity */
class aa implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ WelcomeActivity f856a;

    aa(WelcomeActivity welcomeActivity) {
        this.f856a = welcomeActivity;
    }

    public void onClick(View view) {
        a.a(WelcomeActivity.b, "click start ad");
        String c = this.f856a.i.c();
        if (!av.c(c)) {
            a.a(WelcomeActivity.b, "start ad is jump url, url:" + c);
            boolean unused = this.f856a.h = true;
            if (this.f856a.t != null) {
                this.f856a.t.removeCallbacksAndMessages(null);
            }
            HashMap hashMap = new HashMap();
            hashMap.put("ad_type", "url");
            b.a(this.f856a.getApplicationContext(), "DUODUO_SPLASH_AD_CLICK", hashMap);
            Intent intent = new Intent(this.f856a.n, WebViewActivity.class);
            intent.putExtra("url", c);
            this.f856a.n.startActivityForResult(intent, 888);
            return;
        }
        String d = this.f856a.i.d();
        String e = this.f856a.i.e();
        if (!av.c(d)) {
            HashMap hashMap2 = new HashMap();
            hashMap2.put("package_name", e);
            if (!f.a(e)) {
                a.a(WelcomeActivity.b, "start ad is apk_down");
                hashMap2.put("ad_type", "apk_down");
                b.b(this.f856a.getApplicationContext(), "CLICK_SPLASH_APK_DOWN");
                q.a(this.f856a.getApplicationContext()).a(d, this.f856a.i.f(), q.a.notifybar, true);
            } else if ("true".equals(b.c(RingDDApp.c(), "splash_support_launch_app"))) {
                f.a(RingDDApp.c(), e);
                hashMap2.put("ad_type", "apk_launch");
                a.a(WelcomeActivity.b, "start ad is apk_launch");
            } else {
                hashMap2.put("ad_type", "apk_show");
                a.a(WelcomeActivity.b, "start ad is apk_show");
            }
            b.a(this.f856a.getApplicationContext(), "DUODUO_SPLASH_AD_CLICK", hashMap2);
        }
    }
}
