package com.shoujiduoduo.ringtone.activity;

import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.a.b.b;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.util.f;
import com.shoujiduoduo.util.l;
import java.util.HashMap;

/* compiled from: WelcomeActivity */
class r extends x.b {
    final /* synthetic */ WelcomeActivity d;

    r(WelcomeActivity welcomeActivity) {
        this.d = welcomeActivity;
    }

    public void a() {
        if (!l.b()) {
            this.d.t.sendEmptyMessage(3);
        } else if (!l.a()) {
            this.d.t.sendEmptyMessage(4);
        } else {
            b.g();
            b.b();
            b.c();
            b.e();
            b.d();
            HashMap hashMap = new HashMap();
            if (f.a("com.kingroot.master")) {
                hashMap.put("res", "install");
            } else {
                hashMap.put("res", "uninstall");
            }
            com.umeng.analytics.b.a(RingDDApp.c(), "kingroot_install", hashMap);
        }
    }
}
