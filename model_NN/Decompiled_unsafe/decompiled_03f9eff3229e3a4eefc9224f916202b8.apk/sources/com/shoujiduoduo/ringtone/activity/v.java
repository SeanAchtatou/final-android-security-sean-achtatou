package com.shoujiduoduo.ringtone.activity;

import com.qq.e.ads.splash.SplashADListener;
import com.shoujiduoduo.base.a.a;
import com.umeng.analytics.b;
import java.util.HashMap;

/* compiled from: WelcomeActivity */
class v implements SplashADListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ WelcomeActivity f874a;

    v(WelcomeActivity welcomeActivity) {
        this.f874a = welcomeActivity;
    }

    public void onADPresent() {
        a.a(WelcomeActivity.b, "gdt ad onAdPresent");
        HashMap hashMap = new HashMap();
        hashMap.put("status", "onAdPresent");
        b.a(this.f874a.getApplicationContext(), "GDT_SPLASH_AD", hashMap);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.ringtone.activity.WelcomeActivity.a(com.shoujiduoduo.ringtone.activity.WelcomeActivity, boolean):boolean
     arg types: [com.shoujiduoduo.ringtone.activity.WelcomeActivity, int]
     candidates:
      com.shoujiduoduo.ringtone.activity.WelcomeActivity.a(com.shoujiduoduo.ringtone.activity.WelcomeActivity, com.shoujiduoduo.util.au):com.shoujiduoduo.util.au
      com.shoujiduoduo.ringtone.activity.WelcomeActivity.a(com.shoujiduoduo.ringtone.activity.WelcomeActivity, boolean):boolean */
    public void onADClicked() {
        boolean unused = this.f874a.o = true;
        a.a(WelcomeActivity.b, "gdt ad onADClicked");
    }

    public void onADDismissed() {
        a.a(WelcomeActivity.b, "gdt ad onAdDismissed");
        HashMap hashMap = new HashMap();
        hashMap.put("status", "onAdDismissed");
        b.a(this.f874a.getApplicationContext(), "GDT_SPLASH_AD", hashMap);
        if (!this.f874a.k && !this.f874a.l) {
            this.f874a.g();
        }
    }

    public void onNoAD(int i) {
        a.a(WelcomeActivity.b, "gdt no ad or failed, errCode:" + i);
        HashMap hashMap = new HashMap();
        hashMap.put("status", "onNoAD");
        hashMap.put("errcode", "" + i);
        b.a(this.f874a.getApplicationContext(), "GDT_SPLASH_AD", hashMap);
        if (!this.f874a.k && !this.f874a.l) {
            this.f874a.f();
        }
    }
}
