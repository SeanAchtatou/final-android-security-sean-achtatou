package com.shoujiduoduo.ringtone.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import cn.banshenggua.aichang.utils.Constants;
import com.baidu.mobads.SplashAd;
import com.d.a.b.d;
import com.duoduo.dynamicdex.DuoMobAdUtils;
import com.qq.e.ads.splash.SplashAD;
import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ui.utils.BaseActivity;
import com.shoujiduoduo.ui.utils.v;
import com.shoujiduoduo.util.as;
import com.shoujiduoduo.util.au;
import com.shoujiduoduo.util.av;
import com.shoujiduoduo.util.f;
import com.shoujiduoduo.util.t;
import com.shoujiduoduo.util.u;
import com.umeng.analytics.a.a;
import com.umeng.analytics.b;
import java.util.Timer;
import java.util.TimerTask;
import org.apache.mina.proxy.handlers.http.ntlm.NTLMConstants;

public class WelcomeActivity extends BaseActivity {
    /* access modifiers changed from: private */
    public static final String b = WelcomeActivity.class.getSimpleName();
    private static boolean m;
    private static String q = "1101336966";
    private static String r = "7050502419812471";

    /* renamed from: a  reason: collision with root package name */
    public boolean f854a = false;
    /* access modifiers changed from: private */
    public Timer c = new Timer();
    private ImageView d;
    private LinearLayout e;
    private Button f;
    /* access modifiers changed from: private */
    public boolean g;
    /* access modifiers changed from: private */
    public boolean h;
    /* access modifiers changed from: private */
    public au i;
    /* access modifiers changed from: private */
    public FrameLayout j;
    /* access modifiers changed from: private */
    public boolean k;
    /* access modifiers changed from: private */
    public boolean l;
    /* access modifiers changed from: private */
    public Activity n;
    /* access modifiers changed from: private */
    public boolean o;
    /* access modifiers changed from: private */
    public TimerTask p = new s(this);
    private a s = new w(this);
    /* access modifiers changed from: private */
    public Handler t = new x(this);

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.util.as.a(android.content.Context, java.lang.String, int):int
     arg types: [com.shoujiduoduo.ringtone.activity.WelcomeActivity, java.lang.String, int]
     candidates:
      com.shoujiduoduo.util.as.a(android.content.Context, java.lang.String, long):long
      com.shoujiduoduo.util.as.a(android.content.Context, java.lang.String, java.lang.String):java.lang.String
      com.shoujiduoduo.util.as.a(android.content.Context, java.lang.String, int):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.util.as.b(android.content.Context, java.lang.String, int):boolean
     arg types: [com.shoujiduoduo.ringtone.activity.WelcomeActivity, java.lang.String, int]
     candidates:
      com.shoujiduoduo.util.as.b(android.content.Context, java.lang.String, long):boolean
      com.shoujiduoduo.util.as.b(android.content.Context, java.lang.String, java.lang.String):boolean
      com.shoujiduoduo.util.as.b(android.content.Context, java.lang.String, int):boolean */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Intent intent = getIntent();
        String c2 = b.c(RingDDApp.c(), "start_ad_slient_gap");
        if (!(intent == null || (intent.getFlags() & NTLMConstants.FLAG_UNIDENTIFIED_6) == 0)) {
            int intExtra = intent.getIntExtra(f.f1667a, 0);
            if (m && intExtra != f.b) {
                if (RingDDApp.b().a("activity_on_stop_time") != null) {
                    long currentTimeMillis = System.currentTimeMillis() - ((Long) RingDDApp.b().a("activity_on_stop_time")).longValue();
                    com.shoujiduoduo.base.a.a.a(b, "background time:" + currentTimeMillis);
                    com.shoujiduoduo.base.a.a.a(b, "slient gap:" + u.a(c2, 30000));
                    if (c2.equals("0") || currentTimeMillis < ((long) u.a(c2, 30000)) || i()) {
                        com.shoujiduoduo.base.a.a.a(b, "restart time gap is too small, or is vip, so just put task to front");
                        finish();
                        return;
                    }
                } else {
                    finish();
                    return;
                }
            }
        }
        this.l = false;
        if (!m) {
            setContentView((int) R.layout.activity_welcome);
            b.c(getApplicationContext());
            b.a(this.s);
            com.shoujiduoduo.util.c.b.a().a((com.shoujiduoduo.util.b.a) null);
            this.h = false;
            this.n = this;
            this.g = false;
            b();
            if (as.a((Context) this, "preference_create_shortcut", 0) <= 0) {
                f.a(getApplicationContext(), getResources().getString(R.string.app_name), (int) R.drawable.duoduo_icon);
                as.b((Context) this, "preference_create_shortcut", 1);
            }
            a(false);
            x.a().b(new r(this));
        } else {
            setContentView((int) R.layout.activity_welcome);
            b();
            a(true);
        }
        com.shoujiduoduo.base.a.a.a(b, "onCreate");
    }

    private void a(boolean z) {
        if (i()) {
            com.shoujiduoduo.base.a.a.a(b, "current user is vip");
            if (h()) {
                l();
            } else {
                k();
            }
            if (z) {
                f();
            } else {
                this.t.sendEmptyMessageDelayed(2, 2000);
            }
        } else {
            com.shoujiduoduo.base.a.a.a(b, "user is not vip");
            String c2 = b.c(getApplicationContext(), "splashad_over_time_duration");
            if (z) {
                this.t.sendEmptyMessageDelayed(6, 3000);
            } else {
                this.t.sendEmptyMessageDelayed(6, (long) u.a(c2, 7000));
            }
            if (com.shoujiduoduo.util.a.a(false, false)) {
                d();
            } else if (com.shoujiduoduo.util.a.a(false)) {
                c();
            } else if (com.shoujiduoduo.util.a.b(false)) {
                e();
            } else {
                com.shoujiduoduo.base.a.a.a(b, "check umeng ad config");
                this.t.sendEmptyMessageDelayed(5, 1000);
            }
        }
    }

    private void b() {
        this.d = (ImageView) findViewById(R.id.image_top);
        this.e = (LinearLayout) findViewById(R.id.bottom_layout);
        this.f = (Button) findViewById(R.id.btn_down_apk);
        this.j = (FrameLayout) findViewById(R.id.ad_layout);
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 == 4) {
            return true;
        }
        return super.onKeyDown(i2, keyEvent);
    }

    /* access modifiers changed from: private */
    public void c() {
        this.o = false;
        this.j.setVisibility(0);
        com.shoujiduoduo.base.a.a.a(b, "showBaiduSplashAd");
        b.b(RingDDApp.c(), "baidu_splash_show");
        new SplashAd(this, this.j, new t(this, System.currentTimeMillis()), "2385862", true);
    }

    /* access modifiers changed from: private */
    public void d() {
        this.j.setVisibility(0);
        com.shoujiduoduo.base.a.a.a(b, "showDuomobSplashAd");
        b.b(RingDDApp.c(), "duomob_splash_show");
        this.o = false;
        DuoMobAdUtils.Ins.BaiduIns.getSplashAd(this, this.j, new u(this, System.currentTimeMillis()), "d7d3402a", "2652332", true);
    }

    /* access modifiers changed from: private */
    public void e() {
        com.shoujiduoduo.base.a.a.a(b, "showGDTSplashAd");
        this.o = false;
        this.j.setVisibility(0);
        b.b(getApplicationContext(), "gdt_splash_show");
        new SplashAD(this, this.j, q, r, new v(this));
    }

    /* access modifiers changed from: private */
    public void f() {
        com.shoujiduoduo.base.a.a.a(b, "startMainActivity");
        if (this.t != null) {
            this.t.removeCallbacksAndMessages(null);
        }
        m = true;
        Intent intent = new Intent(this, RingToneDuoduoActivity.class);
        a(intent);
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        finish();
    }

    /* access modifiers changed from: private */
    public void g() {
        if (this.f854a) {
            f();
        } else {
            this.f854a = true;
        }
    }

    private void a(Intent intent) {
        intent.setData(getIntent().getData());
        intent.setAction(getIntent().getAction());
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            intent.putExtras(extras);
        }
    }

    private boolean h() {
        String a2 = as.a(getApplicationContext(), "cur_splash_pic", "");
        return !TextUtils.isEmpty(a2) && t.f(a2);
    }

    private boolean i() {
        int a2 = as.a(RingDDApp.c(), "user_loginStatus", 0);
        int a3 = as.a(RingDDApp.c(), "user_vip_type", 0);
        if (a2 != 1 || a3 == 0) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.f854a) {
            g();
        }
        this.f854a = true;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.f854a = false;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.t != null) {
            this.t.removeCallbacksAndMessages(null);
        }
        this.l = true;
        com.shoujiduoduo.base.a.a.a(b, "onDestroy");
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        com.shoujiduoduo.base.a.a.a(b, "onActivityResult");
        if (i2 == 888) {
            this.t.sendEmptyMessage(2);
        }
    }

    /* access modifiers changed from: private */
    public boolean a(au auVar) {
        String d2 = auVar.d();
        String e2 = auVar.e();
        if (av.c(d2) || av.c(e2) || !f.a(e2) || !"true".equals(b.c(RingDDApp.c(), "show_ad_if_app_is_installed"))) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: private */
    public int j() {
        if (this.i.a()) {
            return this.i.g() * Constants.CLEARIMGED;
        }
        return 1500;
    }

    /* access modifiers changed from: private */
    public void k() {
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.1f, 1.0f);
        alphaAnimation.setDuration(1000);
        this.d.setImageResource(R.drawable.splash8);
        this.d.startAnimation(alphaAnimation);
        this.d.setVisibility(0);
    }

    private void l() {
        this.d.setScaleType(ImageView.ScaleType.CENTER_CROP);
        this.d.setImageDrawable(Drawable.createFromPath(as.a(getApplicationContext(), "cur_splash_pic", "")));
        this.d.setVisibility(0);
        this.e.setVisibility(8);
    }

    /* access modifiers changed from: private */
    public void m() {
        d.a().a(this.i.b(), this.d, v.a().c());
        this.d.setVisibility(0);
        b.b(getApplicationContext(), "DD_SPLASH_AD");
        this.d.setOnClickListener(new aa(this));
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        com.shoujiduoduo.base.a.a.a(b, "onStart");
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        com.shoujiduoduo.base.a.a.a(b, "onRestart");
        super.onRestart();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        com.shoujiduoduo.base.a.a.a(b, "onStop");
        super.onStop();
    }
}
