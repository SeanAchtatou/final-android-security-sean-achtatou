package com.shoujiduoduo.ringtone.activity;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.util.ak;

/* compiled from: RingToneDuoduoActivity */
class n implements ServiceConnection {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RingToneDuoduoActivity f868a;

    n(RingToneDuoduoActivity ringToneDuoduoActivity) {
        this.f868a = ringToneDuoduoActivity;
    }

    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        a.a("ServiceConnection", "ServiceConnection: onServiceConnected.");
        PlayerService unused = this.f868a.t = ((PlayerService.b) iBinder).a();
        ak.a().a(this.f868a.t);
        a.a(RingToneDuoduoActivity.f849a, "ServiceConnection: mPlayService = " + this.f868a.t);
    }

    public void onServiceDisconnected(ComponentName componentName) {
        a.a("ServiceConnection", "ServiceConnection: onServiceDisconnected.");
    }
}
