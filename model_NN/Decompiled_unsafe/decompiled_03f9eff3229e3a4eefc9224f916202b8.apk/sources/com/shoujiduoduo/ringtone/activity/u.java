package com.shoujiduoduo.ringtone.activity;

import com.duoduo.mobads.ISplashAdListener;
import com.shoujiduoduo.base.a.a;
import com.umeng.analytics.b;
import java.util.HashMap;

/* compiled from: WelcomeActivity */
class u implements ISplashAdListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ long f873a;
    final /* synthetic */ WelcomeActivity b;

    u(WelcomeActivity welcomeActivity, long j) {
        this.b = welcomeActivity;
        this.f873a = j;
    }

    public void onAdDismissed() {
        a.b(WelcomeActivity.b, "baidu ad onAdDismissed");
        HashMap hashMap = new HashMap();
        hashMap.put("status", "onAdDismissed");
        b.a(this.b.getApplicationContext(), "DUOMOB_SPLASH_AD", hashMap);
        b.a(this.b.getApplicationContext(), "duomob_splashad_dismiss", hashMap, (int) (System.currentTimeMillis() - this.f873a));
        if (!this.b.k && !this.b.l) {
            this.b.g();
        }
    }

    public void onAdFailed(String str) {
        a.c(WelcomeActivity.b, "baidu ad onAdFailed:" + str);
        HashMap hashMap = new HashMap();
        hashMap.put("status", "onAdFailed");
        hashMap.put("errcode", str);
        b.a(this.b.getApplicationContext(), "DUOMOB_SPLASH_AD", hashMap);
        b.a(this.b.getApplicationContext(), "duomob_splashad_fail", hashMap, (int) (System.currentTimeMillis() - this.f873a));
        a.a(WelcomeActivity.b, "failed time:" + (System.currentTimeMillis() - this.f873a));
        if (!this.b.l) {
            this.b.j.setVisibility(4);
            this.b.k();
            this.b.t.sendEmptyMessageDelayed(2, 2000);
        }
    }

    public void onAdPresent() {
        a.b(WelcomeActivity.b, "baidu ad onAdPresent");
        HashMap hashMap = new HashMap();
        hashMap.put("status", "onAdPresent");
        b.a(this.b.getApplicationContext(), "DUOMOB_SPLASH_AD", hashMap);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.ringtone.activity.WelcomeActivity.a(com.shoujiduoduo.ringtone.activity.WelcomeActivity, boolean):boolean
     arg types: [com.shoujiduoduo.ringtone.activity.WelcomeActivity, int]
     candidates:
      com.shoujiduoduo.ringtone.activity.WelcomeActivity.a(com.shoujiduoduo.ringtone.activity.WelcomeActivity, com.shoujiduoduo.util.au):com.shoujiduoduo.util.au
      com.shoujiduoduo.ringtone.activity.WelcomeActivity.a(com.shoujiduoduo.ringtone.activity.WelcomeActivity, boolean):boolean */
    public void onAdClick() {
        boolean unused = this.b.o = true;
        a.b(WelcomeActivity.b, "baidu ad onAdClick");
    }
}
