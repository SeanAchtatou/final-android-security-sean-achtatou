package com.shoujiduoduo.ringtone.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.widget.Toast;
import cn.banshenggua.aichang.utils.Constants;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.activity.RingToneDuoduoActivity;
import com.shoujiduoduo.ui.home.MusicAlbumActivity;
import com.shoujiduoduo.ui.search.SearchActivity;
import com.shoujiduoduo.util.u;
import com.shoujiduoduo.util.widget.WebViewActivity;
import java.io.File;

/* compiled from: RingToneDuoduoActivity */
class b extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RingToneDuoduoActivity f857a;

    b(RingToneDuoduoActivity ringToneDuoduoActivity) {
        this.f857a = ringToneDuoduoActivity;
    }

    public void handleMessage(Message message) {
        switch (message.what) {
            case 111:
                String c = com.umeng.analytics.b.c(this.f857a.getApplicationContext(), "bd_time");
                if (!TextUtils.isEmpty(c)) {
                    int a2 = u.a(c, 0);
                    if (a2 > 0) {
                        int a3 = this.f857a.a(a2, 30);
                        a.a("xxxad", "time:" + a3);
                        sendMessageDelayed(this.f857a.R.obtainMessage(111), (long) (a3 * Constants.CLEARIMGED));
                    }
                    if (!com.shoujiduoduo.util.a.i() || !this.f857a.B.isShown()) {
                        a.a("xxxad", "adcontainer not shown, skip");
                        return;
                    } else {
                        this.f857a.B.b();
                        return;
                    }
                } else {
                    return;
                }
            case 1100:
                this.f857a.b((String) message.obj);
                return;
            case 1101:
                Uri fromFile = Uri.fromFile(new File((String) message.obj));
                Intent intent = new Intent("android.intent.action.VIEW");
                intent.setDataAndType(fromFile, "application/vnd.android.package-archive");
                this.f857a.startActivity(intent);
                return;
            case 1102:
                Toast.makeText(this.f857a, (int) R.string.down_update_apk_error, 0).show();
                return;
            case 1130:
                if (this.f857a.A != null) {
                    this.f857a.A.cancel();
                }
                Toast.makeText(this.f857a, (int) R.string.clean_cache_suc, 0).show();
                return;
            case 1131:
                Intent intent2 = new Intent(this.f857a, SearchActivity.class);
                intent2.putExtra("from", "push");
                intent2.putExtra("key", (String) message.obj);
                this.f857a.startActivity(intent2);
                return;
            case 1133:
                String str = (String) message.obj;
                Intent intent3 = new Intent(this.f857a, WebViewActivity.class);
                intent3.putExtra("url", str);
                a.a(RingToneDuoduoActivity.f849a, "url:" + str);
                this.f857a.startActivity(intent3);
                return;
            case 1134:
                this.f857a.j();
                return;
            case 1135:
                RingToneDuoduoActivity.c cVar = (RingToneDuoduoActivity.c) message.obj;
                Intent intent4 = new Intent(this.f857a, MusicAlbumActivity.class);
                intent4.putExtra("url", cVar.f853a);
                intent4.putExtra("title", cVar.b);
                intent4.putExtra("type", MusicAlbumActivity.a.create_album);
                a.a(RingToneDuoduoActivity.f849a, "title:" + cVar.b + ", url:" + cVar.f853a);
                this.f857a.startActivity(intent4);
                return;
            default:
                return;
        }
    }
}
