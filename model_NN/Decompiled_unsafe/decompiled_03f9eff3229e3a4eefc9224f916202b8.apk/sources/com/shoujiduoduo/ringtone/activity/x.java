package com.shoujiduoduo.ringtone.activity;

import android.app.AlertDialog;
import android.os.Handler;
import android.os.Message;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.util.au;
import com.umeng.analytics.b;

/* compiled from: WelcomeActivity */
class x extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ WelcomeActivity f876a;

    x(WelcomeActivity welcomeActivity) {
        this.f876a = welcomeActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.ringtone.activity.WelcomeActivity.b(com.shoujiduoduo.ringtone.activity.WelcomeActivity, boolean):boolean
     arg types: [com.shoujiduoduo.ringtone.activity.WelcomeActivity, int]
     candidates:
      com.shoujiduoduo.ringtone.activity.WelcomeActivity.b(com.shoujiduoduo.ringtone.activity.WelcomeActivity, com.shoujiduoduo.util.au):boolean
      com.shoujiduoduo.ringtone.activity.WelcomeActivity.b(com.shoujiduoduo.ringtone.activity.WelcomeActivity, boolean):boolean */
    public void handleMessage(Message message) {
        switch (message.what) {
            case 2:
                a.a(WelcomeActivity.b, "MSG_TIMER_TRIGGER Received!");
                if (!this.f876a.k) {
                    if (this.f876a.g) {
                        b.b(this.f876a, "START_AD_SHOW");
                    }
                    this.f876a.f();
                    return;
                }
                return;
            case 3:
                boolean unused = this.f876a.k = true;
                new AlertDialog.Builder(this.f876a.n).setTitle("").setIcon(17301543).setMessage((int) R.string.sdcard_not_found).setPositiveButton((int) R.string.ok, new y(this)).show();
                return;
            case 4:
                boolean unused2 = this.f876a.k = true;
                new AlertDialog.Builder(this.f876a.n).setTitle("").setIcon(17301543).setMessage((int) R.string.sdcard_not_access).setPositiveButton((int) R.string.ok, new z(this)).show();
                return;
            case 5:
                if (com.shoujiduoduo.util.a.a(false, true)) {
                    this.f876a.d();
                    return;
                } else if (com.shoujiduoduo.util.a.a(false)) {
                    this.f876a.c();
                    return;
                } else if (com.shoujiduoduo.util.a.b(false)) {
                    this.f876a.e();
                    return;
                } else {
                    String c = b.c(this.f876a.getApplicationContext(), "start_ad_new");
                    a.a(WelcomeActivity.b, "umeng start ad config:" + c);
                    au unused3 = this.f876a.i = new au(c);
                    if (this.f876a.i.a()) {
                        a.a(WelcomeActivity.b, "canShowStartAd is true");
                        if (this.f876a.a(this.f876a.i)) {
                            a.a(WelcomeActivity.b, "hit start ad, but app is installed, show normal ad!");
                            if (com.shoujiduoduo.util.a.a(true, true)) {
                                this.f876a.d();
                                return;
                            } else if (com.shoujiduoduo.util.a.a(true)) {
                                this.f876a.c();
                                return;
                            } else if (com.shoujiduoduo.util.a.b(true)) {
                                this.f876a.e();
                                return;
                            } else {
                                a.a(WelcomeActivity.b, "show normal pic");
                                this.f876a.k();
                                this.f876a.c.schedule(this.f876a.p, (long) this.f876a.j());
                                return;
                            }
                        } else {
                            a.a(WelcomeActivity.b, "show duoduo splash ad");
                            this.f876a.m();
                            this.f876a.c.schedule(this.f876a.p, (long) this.f876a.j());
                            return;
                        }
                    } else {
                        a.a(WelcomeActivity.b, "can not show duoduo splash, show normal pic");
                        this.f876a.k();
                        this.f876a.c.schedule(this.f876a.p, (long) this.f876a.j());
                        return;
                    }
                }
            case 6:
                a.a(WelcomeActivity.b, "splash ad over time, enter main activity");
                if (!this.f876a.k && !this.f876a.o) {
                    b.b(this.f876a.getApplicationContext(), "splashad_over_time");
                    this.f876a.f();
                    return;
                }
                return;
            default:
                return;
        }
    }
}
