package com.shoujiduoduo.ringtone.activity;

import android.content.Intent;
import android.view.View;
import com.shoujiduoduo.ui.search.SearchActivity;

/* compiled from: RingToneDuoduoActivity */
class j implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RingToneDuoduoActivity f865a;

    j(RingToneDuoduoActivity ringToneDuoduoActivity) {
        this.f865a = ringToneDuoduoActivity;
    }

    public void onClick(View view) {
        this.f865a.startActivity(new Intent(this.f865a, SearchActivity.class));
    }
}
