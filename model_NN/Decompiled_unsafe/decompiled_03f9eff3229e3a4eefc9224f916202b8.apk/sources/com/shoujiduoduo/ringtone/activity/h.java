package com.shoujiduoduo.ringtone.activity;

import android.graphics.drawable.Drawable;
import com.shoujiduoduo.a.b.b;
import com.shoujiduoduo.a.c.w;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.util.a;

/* compiled from: RingToneDuoduoActivity */
class h implements w {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RingToneDuoduoActivity f863a;

    h(RingToneDuoduoActivity ringToneDuoduoActivity) {
        this.f863a = ringToneDuoduoActivity;
    }

    public void a(int i) {
        if (b.g().h()) {
            this.f863a.B.setVisibility(8);
            this.f863a.d.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, this.f863a.getResources().getDrawable(R.drawable.btn_navi_myring_vip), (Drawable) null, (Drawable) null);
            return;
        }
        if (a.e() && this.f863a.p.getCurrentScene() != 2 && !this.f863a.J) {
            this.f863a.B.setVisibility(0);
        }
        this.f863a.d.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, this.f863a.getResources().getDrawable(R.drawable.btn_navi_myring), (Drawable) null, (Drawable) null);
    }
}
