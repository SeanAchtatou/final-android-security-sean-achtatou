package com.shoujiduoduo.ringtone.activity;

import com.baidu.mobads.SplashAdListener;
import com.shoujiduoduo.base.a.a;
import com.umeng.analytics.b;
import java.util.HashMap;

/* compiled from: WelcomeActivity */
class t implements SplashAdListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ long f872a;
    final /* synthetic */ WelcomeActivity b;

    t(WelcomeActivity welcomeActivity, long j) {
        this.b = welcomeActivity;
        this.f872a = j;
    }

    public void onAdDismissed() {
        a.b(WelcomeActivity.b, "baidu ad onAdDismissed");
        HashMap hashMap = new HashMap();
        hashMap.put("status", "onAdDismissed");
        b.a(this.b.getApplicationContext(), "BAIDU_SPLASH_AD", hashMap);
        b.a(this.b.getApplicationContext(), "baidu_splashad_dismiss", hashMap, (int) (System.currentTimeMillis() - this.f872a));
        if (!this.b.k && !this.b.l) {
            this.b.g();
        }
    }

    public void onAdFailed(String str) {
        a.c(WelcomeActivity.b, "baidu ad onAdFailed:" + str);
        HashMap hashMap = new HashMap();
        hashMap.put("status", "onAdFailed");
        hashMap.put("errcode", str);
        b.a(this.b.getApplicationContext(), "BAIDU_SPLASH_AD", hashMap);
        b.a(this.b.getApplicationContext(), "baidu_splashad_fail", hashMap, (int) (System.currentTimeMillis() - this.f872a));
        a.a(WelcomeActivity.b, "failed time:" + (System.currentTimeMillis() - this.f872a));
        String c = b.c(this.b.getApplicationContext(), "gdt_splashad_retry_switch");
        if (this.b.l) {
            return;
        }
        if (((int) (System.currentTimeMillis() - this.f872a)) > 2000 || c.equals("false")) {
            this.b.j.setVisibility(4);
            this.b.k();
            this.b.t.sendEmptyMessageDelayed(2, 2000);
            return;
        }
        a.a(WelcomeActivity.b, "baidu splash ad fail, try gdt ad!");
        this.b.e();
    }

    public void onAdPresent() {
        a.b(WelcomeActivity.b, "baidu ad onAdPresent");
        HashMap hashMap = new HashMap();
        hashMap.put("status", "onAdPresent");
        b.a(this.b.getApplicationContext(), "BAIDU_SPLASH_AD", hashMap);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.ringtone.activity.WelcomeActivity.a(com.shoujiduoduo.ringtone.activity.WelcomeActivity, boolean):boolean
     arg types: [com.shoujiduoduo.ringtone.activity.WelcomeActivity, int]
     candidates:
      com.shoujiduoduo.ringtone.activity.WelcomeActivity.a(com.shoujiduoduo.ringtone.activity.WelcomeActivity, com.shoujiduoduo.util.au):com.shoujiduoduo.util.au
      com.shoujiduoduo.ringtone.activity.WelcomeActivity.a(com.shoujiduoduo.ringtone.activity.WelcomeActivity, boolean):boolean */
    public void onAdClick() {
        boolean unused = this.b.o = true;
        a.b(WelcomeActivity.b, "baidu ad onAdClick");
    }
}
