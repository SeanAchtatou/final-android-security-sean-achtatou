package com.shoujiduoduo.ringtone.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import cn.banshenggua.aichang.utils.Constants;
import com.alimama.mobile.sdk.config.MmuSDKFactory;
import com.cmsc.cmmusic.init.InitCmmInterface;
import com.d.a.b.c;
import com.d.a.b.d;
import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.a.c.e;
import com.shoujiduoduo.a.c.u;
import com.shoujiduoduo.a.c.w;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ui.adwall.WallAdView;
import com.shoujiduoduo.ui.adwall.g;
import com.shoujiduoduo.ui.category.k;
import com.shoujiduoduo.ui.home.DuoduoAdContainer;
import com.shoujiduoduo.ui.home.MusicAlbumActivity;
import com.shoujiduoduo.ui.home.ab;
import com.shoujiduoduo.ui.makering.MakeRingActivity;
import com.shoujiduoduo.ui.mine.ap;
import com.shoujiduoduo.ui.settings.ae;
import com.shoujiduoduo.ui.settings.m;
import com.shoujiduoduo.ui.utils.BaseFragmentActivity;
import com.shoujiduoduo.ui.utils.SceneContainer;
import com.shoujiduoduo.util.ak;
import com.shoujiduoduo.util.am;
import com.shoujiduoduo.util.aq;
import com.shoujiduoduo.util.as;
import com.shoujiduoduo.util.av;
import com.shoujiduoduo.util.f;
import com.shoujiduoduo.util.i;
import com.shoujiduoduo.util.p;
import com.shoujiduoduo.util.widget.a;
import com.umeng.socialize.UMShareAPI;
import java.util.Random;
import org.apache.mina.proxy.handlers.http.ntlm.NTLMConstants;

public class RingToneDuoduoActivity extends BaseFragmentActivity implements View.OnClickListener, k.a {
    private static final String C = ("user_click_ad:" + f.p());
    private static final String D = ("start_time:" + f.p());
    private static final String E = ("user_click_ad_time:" + f.p());
    private static RingToneDuoduoActivity H;
    private static boolean I;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final String f849a = RingToneDuoduoActivity.class.getSimpleName();
    /* access modifiers changed from: private */
    public ProgressDialog A;
    /* access modifiers changed from: private */
    public DuoduoAdContainer B;
    private b F;
    private boolean G;
    /* access modifiers changed from: private */
    public boolean J;
    private m K;
    private com.shoujiduoduo.a.c.m L = new a(this);
    private e M = new f(this);
    private u N = new g(this);
    private w O = new h(this);
    private com.shoujiduoduo.a.c.a P = new i(this);
    private ServiceConnection Q = new n(this);
    /* access modifiers changed from: private */
    @SuppressLint({"HandlerLeak"})
    public Handler R = new b(this);
    private RadioButton b;
    private RadioButton c;
    /* access modifiers changed from: private */
    public RadioButton d;
    /* access modifiers changed from: private */
    public int e = -1;
    private Drawable f;
    private boolean g;
    private TextView h;
    private ab i;
    private k j;
    /* access modifiers changed from: private */
    public ap k;
    private g l;
    private WallAdView m;
    private com.shoujiduoduo.ui.a.a n;
    private LinearLayout o;
    /* access modifiers changed from: private */
    public SceneContainer p;
    private ImageButton q;
    private RelativeLayout r;
    private RelativeLayout s;
    /* access modifiers changed from: private */
    public PlayerService t;
    private ImageButton u;
    private ImageButton v;
    private Button w;
    private Button x;
    private ImageView y;
    private RelativeLayout z;

    public enum a {
        HEADER_HOMEPAGE,
        HEADER_CATEGORY,
        HEADER_MY_RINGTONE,
        HEADER_MORE_OPTIONS,
        HEADER_DUODUO_FAMILY,
        HEADER_USER_FEEDBACK,
        HEADER_ABOUT_INFO,
        HEADER_SEARCH,
        HEADER_UMENG_AD
    }

    public static RingToneDuoduoActivity a() {
        return H;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.util.as.b(android.content.Context, java.lang.String, int):boolean
     arg types: [com.shoujiduoduo.ringtone.activity.RingToneDuoduoActivity, java.lang.String, int]
     candidates:
      com.shoujiduoduo.util.as.b(android.content.Context, java.lang.String, long):boolean
      com.shoujiduoduo.util.as.b(android.content.Context, java.lang.String, java.lang.String):boolean
      com.shoujiduoduo.util.as.b(android.content.Context, java.lang.String, int):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.util.as.a(android.content.Context, java.lang.String, int):int
     arg types: [com.shoujiduoduo.ringtone.activity.RingToneDuoduoActivity, java.lang.String, int]
     candidates:
      com.shoujiduoduo.util.as.a(android.content.Context, java.lang.String, long):long
      com.shoujiduoduo.util.as.a(android.content.Context, java.lang.String, java.lang.String):java.lang.String
      com.shoujiduoduo.util.as.a(android.content.Context, java.lang.String, int):int */
    public void onCreate(Bundle bundle) {
        int a2;
        com.shoujiduoduo.base.a.a.a(f849a, "RingToneDuoduoActivity:onCreate");
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_main);
        setTheme((int) R.style.StyledIndicators);
        H = this;
        com.shoujiduoduo.util.w.a();
        com.shoujiduoduo.util.c.m.a();
        com.shoujiduoduo.util.m.a().b();
        p.a(getApplicationContext());
        aq.a(getApplicationContext());
        com.shoujiduoduo.util.c.a.a(getApplicationContext());
        this.p = (SceneContainer) findViewById(R.id.sceneGroup);
        this.p.a(this);
        as.b((Context) this, D, as.a((Context) this, D, 0) + 1);
        e();
        this.J = com.shoujiduoduo.util.a.d();
        if (this.J) {
            com.shoujiduoduo.a.b.b.c().g();
        }
        this.B = (DuoduoAdContainer) findViewById(R.id.ad_container);
        if (!com.shoujiduoduo.util.a.e() || this.J) {
            this.B.setVisibility(8);
        } else {
            this.B.c();
        }
        com.shoujiduoduo.base.a.a.a(f849a, "RingToneDuoduoActivity:onCreate 1");
        this.i = new ab(this);
        this.i.a();
        this.j = new k(this);
        this.j.a();
        this.k = new ap(this);
        this.k.a();
        com.shoujiduoduo.base.a.a.a(f849a, "RingToneDuoduoActivity:onCreate 2");
        setVolumeControlStream(3);
        Intent intent = new Intent(this, PlayerService.class);
        startService(intent);
        com.shoujiduoduo.base.a.a.a(f849a, "Service: startService Finished!");
        bindService(intent, this.Q, 1);
        this.G = true;
        com.shoujiduoduo.base.a.a.a(f849a, "Service: bindService Finished!");
        b(true);
        this.c.setChecked(true);
        x.a().a(com.shoujiduoduo.a.a.b.OBSERVER_CONFIG, this.M);
        x.a().a(com.shoujiduoduo.a.a.b.OBSERVER_VIP, this.O);
        x.a().a(com.shoujiduoduo.a.a.b.OBSERVER_APP, this.P);
        x.a().a(com.shoujiduoduo.a.a.b.OBSERVER_MAKE_RING, this.L);
        x.a().a(com.shoujiduoduo.a.a.b.OBSERVER_USER_CENTER, this.N);
        f();
        this.R.sendEmptyMessageDelayed(1134, 2000);
        this.K = new m(this, R.style.DuoDuoDialog);
        if (d()) {
            com.shoujiduoduo.base.a.a.a(f849a, "create wallpaper shortcut");
            as.b((Context) this, "pred_other_shortcut_create", 1);
            f.a(getApplicationContext(), "最火壁纸", f.b, (int) R.drawable.shortcut_wallpaper_icon);
            com.umeng.analytics.b.b(getApplicationContext(), "CREATE_WALLPAPER_SHORTCUT");
        }
        com.shoujiduoduo.base.a.a.a(f849a, "RingToneDuoduoActivity:onCreate:end");
        String c2 = com.umeng.analytics.b.c(getApplicationContext(), "bd_time");
        if (!TextUtils.isEmpty(c2) && com.shoujiduoduo.util.a.e() && !this.J && (a2 = com.shoujiduoduo.util.u.a(c2, 0)) > 0) {
            int a3 = a(a2, 30);
            com.shoujiduoduo.base.a.a.a("xxxad", "time:" + a3);
            this.R.sendMessageDelayed(this.R.obtainMessage(111), (long) (a3 * Constants.CLEARIMGED));
        }
    }

    /* access modifiers changed from: private */
    public int a(int i2, int i3) {
        return (Math.abs(new Random().nextInt()) % (i3 - i2)) + i2;
    }

    private boolean d() {
        return false;
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_MusicAlbum:
            case R.id.btn_MusicAlbum:
                com.umeng.analytics.b.b(this, "CLICK_MUSIC_ALBUM");
                Intent intent = new Intent(this, MusicAlbumActivity.class);
                intent.putExtra("type", MusicAlbumActivity.a.my_album);
                intent.putExtra("title", "音乐相册");
                startActivity(intent);
                overridePendingTransition(17432578, 17432579);
                return;
            case R.id.btn_record:
                if (this.t != null && this.t.j()) {
                    this.t.k();
                }
                Intent intent2 = new Intent();
                intent2.setClass(this, MakeRingActivity.class);
                intent2.setFlags(NTLMConstants.FLAG_NEGOTIATE_128_BIT_ENCRYPTION);
                startActivity(intent2);
                return;
            case R.id.search_entrance:
            default:
                return;
            case R.id.startSettingButton:
                new ae(this, false).showAsDropDown(this.r, 5, 5);
                return;
            case R.id.deleteButton:
                this.k.c();
                return;
        }
    }

    private void e() {
        this.r = (RelativeLayout) findViewById(R.id.header);
        this.s = (RelativeLayout) findViewById(R.id.search_layout);
        this.c = (RadioButton) findViewById(R.id.buttonHomepage);
        this.d = (RadioButton) findViewById(R.id.buttonMyRingtone);
        if (com.shoujiduoduo.a.b.b.g().h() && com.shoujiduoduo.a.b.b.g().g()) {
            this.d.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, getResources().getDrawable(R.drawable.btn_navi_myring_vip), (Drawable) null, (Drawable) null);
        }
        i();
        this.b = (RadioButton) findViewById(R.id.buttonMoreOptions);
        this.b.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, this.f, (Drawable) null, (Drawable) null);
        if (!com.shoujiduoduo.util.a.f()) {
            this.b.setText((int) R.string.more_options_header);
        } else {
            this.b.setText(h());
        }
        this.h = (TextView) findViewById(R.id.header_text);
        this.q = (ImageButton) findViewById(R.id.backButton);
        this.u = (ImageButton) findViewById(R.id.startSettingButton);
        this.u.setOnClickListener(this);
        this.v = (ImageButton) findViewById(R.id.deleteButton);
        this.v.setOnClickListener(this);
        this.w = (Button) this.s.findViewById(R.id.btn_record);
        this.w.setOnClickListener(this);
        this.x = (Button) this.s.findViewById(R.id.btn_MusicAlbum);
        this.x.setOnClickListener(this);
        this.y = (ImageView) this.s.findViewById(R.id.iv_MusicAlbum);
        this.y.setOnClickListener(this);
        this.z = (RelativeLayout) this.s.findViewById(R.id.music_album_layout);
        if ("false".equalsIgnoreCase(com.umeng.analytics.b.c(RingDDApp.c(), "music_album_switch"))) {
            this.z.setVisibility(4);
        } else {
            String c2 = com.umeng.analytics.b.c(RingDDApp.c(), "music_album_title");
            if (!TextUtils.isEmpty(c2)) {
                this.x.setText(" " + c2);
            }
            String c3 = com.umeng.analytics.b.c(RingDDApp.c(), "music_album_icon_url_new");
            if (TextUtils.isEmpty(c3) || "null".equals(c3)) {
                this.x.setVisibility(0);
                this.y.setVisibility(4);
            } else {
                this.x.setVisibility(4);
                this.y.setVisibility(0);
                d.a().a(c3, this.y, new c.a().a(true).b(true).a(com.d.a.b.a.g.EXACTLY_STRETCHED).c());
            }
        }
        this.o = (LinearLayout) this.s.findViewById(R.id.search_entrance);
        this.o.setOnClickListener(new j(this));
    }

    private void f() {
        x.a().b(new k(this));
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        UMShareAPI.get(this).onActivityResult(i2, i3, intent);
        if (!MmuSDKFactory.getMmuSDK().accountServiceHandleResult(i2, i3, intent, this)) {
            super.onActivityResult(i2, i3, intent);
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        com.shoujiduoduo.base.a.a.a(f849a, "RingToneDuoduoActivity:onStart");
        super.onStart();
        this.F = new b(this, null);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("install_apk_from_start_ad");
        registerReceiver(this.F, intentFilter);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        com.shoujiduoduo.base.a.a.a(f849a, "RingToneDuoduoActivity:onStop");
        unregisterReceiver(this.F);
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        com.shoujiduoduo.base.a.a.a(f849a, "RingToneDuoduoActivity:onDestroy");
        x.a().b(com.shoujiduoduo.a.a.b.OBSERVER_VIP, this.O);
        x.a().b(com.shoujiduoduo.a.a.b.OBSERVER_USER_CENTER, this.N);
        x.a().b(com.shoujiduoduo.a.a.b.OBSERVER_APP, this.P);
        x.a().b(com.shoujiduoduo.a.a.b.OBSERVER_MAKE_RING, this.L);
        x.a().b(com.shoujiduoduo.a.a.b.OBSERVER_CONFIG, this.M);
        if (this.i != null) {
            this.i.b();
        }
        if (this.j != null) {
            this.j.b();
        }
        if (this.l != null) {
            this.l.b();
        }
        if (this.k != null) {
            this.k.b();
        }
        if (this.m != null) {
            this.m.b();
        }
        if (this.B != null) {
            this.B.d();
        }
        if (com.shoujiduoduo.util.c.m.a() != null) {
            com.shoujiduoduo.util.c.m.a().b();
        }
        if (ak.a() != null) {
            ak.a().c();
        }
        aq.a(this).b();
        if (this.R != null) {
            this.R.removeCallbacksAndMessages(null);
        }
        if (com.shoujiduoduo.util.c.b.a().d() && am.a().b("cm_sunshine_sdk_enable")) {
            try {
                InitCmmInterface.exitApp(this);
            } catch (Throwable th) {
            }
        }
        if (this == H) {
            H = null;
        }
        i.a();
    }

    /* access modifiers changed from: private */
    public void g() {
        if (this.G) {
            unbindService(this.Q);
            this.G = false;
        }
        this.Q = null;
        stopService(new Intent(this, PlayerService.class));
        ak.a().a(null);
        this.t = null;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        com.shoujiduoduo.base.a.a.a(f849a, "RingToneDuoduoActivity:onResume");
        super.onResume();
        Intent intent = getIntent();
        if (intent != null) {
            String stringExtra = intent.getStringExtra("update_fail");
            if (stringExtra == null || !stringExtra.equals("yes")) {
                String stringExtra2 = intent.getStringExtra("down_finish");
                if (stringExtra2 == null || stringExtra2.equals("yes")) {
                }
                return;
            }
            Toast.makeText(this, (int) R.string.update_error, 1).show();
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        com.shoujiduoduo.base.a.a.a(f849a, "RingToneDuoduoActivity:onPause");
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        com.shoujiduoduo.base.a.a.a(f849a, "RingToneDuoduoActivity:onRestart");
        super.onRestart();
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        com.shoujiduoduo.base.a.a.a(f849a, "RingToneDuoduoActivity:onNewIntent");
        super.onNewIntent(intent);
        setIntent(intent);
        f();
    }

    private class b extends BroadcastReceiver {
        private b() {
        }

        /* synthetic */ b(RingToneDuoduoActivity ringToneDuoduoActivity, a aVar) {
            this();
        }

        public void onReceive(Context context, Intent intent) {
            com.shoujiduoduo.base.a.a.a(RingToneDuoduoActivity.f849a, "receive broadcast");
            if (intent.getAction().equals("install_apk_from_start_ad")) {
                String stringExtra = intent.getStringExtra("PackagePath");
                String stringExtra2 = intent.getStringExtra("PackageName");
                if (!TextUtils.isEmpty(stringExtra2) && !TextUtils.isEmpty(stringExtra)) {
                    new a.C0034a(RingToneDuoduoActivity.this).b((int) R.string.hint).a(stringExtra2 + RingToneDuoduoActivity.this.getResources().getString(R.string.start_ad_down_apk_hint)).a((int) R.string.ok, new q(this, stringExtra)).b((int) R.string.cancel, new p(this)).a().show();
                }
            }
        }
    }

    private Drawable a(boolean z2) {
        String c2 = com.umeng.analytics.b.c(RingDDApp.c(), "wall_ad_type2");
        return c2.equals("game") ? !z2 ? getResources().getDrawable(R.drawable.btn_navi_job) : getResources().getDrawable(R.drawable.btn_navi_job_red) : c2.equals("app") ? !z2 ? getResources().getDrawable(R.drawable.btn_navi_car) : getResources().getDrawable(R.drawable.btn_navi_car_red) : !z2 ? getResources().getDrawable(R.drawable.btn_navi_taobao) : getResources().getDrawable(R.drawable.btn_navi_taobao_red);
    }

    private String h() {
        if (com.shoujiduoduo.util.a.b()) {
            String c2 = com.umeng.analytics.b.c(RingDDApp.c(), "ac_view_name");
            if (av.b(c2)) {
                return "直播";
            }
            return c2;
        }
        String c3 = com.umeng.analytics.b.c(RingDDApp.c(), "wall_ad_name");
        if (av.b(c3)) {
            return "打折";
        }
        return c3;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.util.as.a(android.content.Context, java.lang.String, int):int
     arg types: [com.shoujiduoduo.ringtone.activity.RingToneDuoduoActivity, java.lang.String, int]
     candidates:
      com.shoujiduoduo.util.as.a(android.content.Context, java.lang.String, long):long
      com.shoujiduoduo.util.as.a(android.content.Context, java.lang.String, java.lang.String):java.lang.String
      com.shoujiduoduo.util.as.a(android.content.Context, java.lang.String, int):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.util.as.a(android.content.Context, java.lang.String, long):long
     arg types: [com.shoujiduoduo.ringtone.activity.RingToneDuoduoActivity, java.lang.String, int]
     candidates:
      com.shoujiduoduo.util.as.a(android.content.Context, java.lang.String, int):int
      com.shoujiduoduo.util.as.a(android.content.Context, java.lang.String, java.lang.String):java.lang.String
      com.shoujiduoduo.util.as.a(android.content.Context, java.lang.String, long):long */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.util.as.b(android.content.Context, java.lang.String, int):boolean
     arg types: [com.shoujiduoduo.ringtone.activity.RingToneDuoduoActivity, java.lang.String, int]
     candidates:
      com.shoujiduoduo.util.as.b(android.content.Context, java.lang.String, long):boolean
      com.shoujiduoduo.util.as.b(android.content.Context, java.lang.String, java.lang.String):boolean
      com.shoujiduoduo.util.as.b(android.content.Context, java.lang.String, int):boolean */
    private void i() {
        if (com.shoujiduoduo.util.a.b()) {
            this.f = getResources().getDrawable(R.drawable.btn_navi_aichang);
        } else if (!com.shoujiduoduo.util.a.f()) {
            this.f = getResources().getDrawable(R.drawable.btn_navi_more);
        } else if (as.a((Context) this, C, 0) != 0) {
            if (System.currentTimeMillis() - as.a((Context) this, E, 0L) > 259200000) {
                this.f = a(true);
                this.g = true;
                return;
            }
            this.f = a(false);
            this.g = false;
        } else if (as.a((Context) this, D, 1) > 3) {
            this.f = a(false);
            this.g = false;
            as.b((Context) this, C, 1);
        } else {
            this.f = a(true);
            this.g = true;
        }
    }

    public void a(a aVar) {
        switch (aVar) {
            case HEADER_HOMEPAGE:
                this.h.setText((int) R.string.homepage_header);
                return;
            case HEADER_CATEGORY:
                this.h.setText((int) R.string.category_header);
                return;
            case HEADER_MY_RINGTONE:
                this.h.setText((int) R.string.my_ringtone_header);
                return;
            case HEADER_MORE_OPTIONS:
                this.h.setText((int) R.string.more_options_header);
                return;
            case HEADER_USER_FEEDBACK:
                this.h.setText((int) R.string.user_feedback_header);
                return;
            case HEADER_ABOUT_INFO:
                this.h.setText((int) R.string.about_info_header);
                return;
            case HEADER_SEARCH:
                this.h.setText((int) R.string.search_header);
                return;
            case HEADER_UMENG_AD:
                this.h.setText((int) R.string.umeng_ad_header);
                return;
            case HEADER_DUODUO_FAMILY:
                this.h.setText((int) R.string.duoduo_family_header);
                return;
            default:
                return;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.util.as.b(android.content.Context, java.lang.String, int):boolean
     arg types: [com.shoujiduoduo.ringtone.activity.RingToneDuoduoActivity, java.lang.String, int]
     candidates:
      com.shoujiduoduo.util.as.b(android.content.Context, java.lang.String, long):boolean
      com.shoujiduoduo.util.as.b(android.content.Context, java.lang.String, java.lang.String):boolean
      com.shoujiduoduo.util.as.b(android.content.Context, java.lang.String, int):boolean */
    public void onRadioButtonClicked(View view) {
        boolean isChecked = ((RadioButton) view).isChecked();
        switch (view.getId()) {
            case R.id.buttonHomepage:
                if (isChecked && this.e != 0) {
                    this.e = 0;
                    b(false);
                    break;
                }
            case R.id.buttonCategory:
                if (isChecked && this.e != 1) {
                    this.e = 1;
                    if (this.j == null) {
                        this.j = new k(this);
                        this.j.a();
                    }
                    this.p.a(1);
                    this.j.a(this.q, this.h, this.s);
                    this.u.setVisibility(4);
                    this.v.setVisibility(4);
                    if ("false".equalsIgnoreCase(com.umeng.analytics.b.c(RingDDApp.c(), "music_album_switch"))) {
                        this.x.setVisibility(4);
                    }
                    this.r.setVisibility(0);
                    if (this.j.d() && com.shoujiduoduo.util.a.e() && !this.J && "true".equals(com.umeng.analytics.b.c(getApplicationContext(), "category_banner_switch"))) {
                        this.B.setVisibility(0);
                        if ("true".equals(com.umeng.analytics.b.c(getApplicationContext(), "bd_radio_switch")) && com.shoujiduoduo.util.a.i()) {
                            com.shoujiduoduo.base.a.a.a("xxxad", "radio switch init");
                            this.B.b();
                            break;
                        }
                    } else {
                        this.B.setVisibility(8);
                        break;
                    }
                }
                break;
            case R.id.buttonMyRingtone:
                if (isChecked && this.e != 2) {
                    this.e = 2;
                    if (this.k == null) {
                        this.k = new ap(this);
                        this.k.a();
                    }
                    this.p.a(2);
                    a(a.HEADER_MY_RINGTONE);
                    this.q.setVisibility(4);
                    this.u.setVisibility(0);
                    this.v.setVisibility(0);
                    this.s.setVisibility(4);
                    this.h.setVisibility(0);
                    this.r.setVisibility(0);
                    this.B.setVisibility(8);
                    break;
                }
            case R.id.buttonMoreOptions:
                if (isChecked && this.e != 3) {
                    this.e = 3;
                    if (com.shoujiduoduo.util.a.b()) {
                        com.umeng.analytics.b.b(RingDDApp.c(), "click_ac_view");
                        if (this.n == null) {
                            this.n = new com.shoujiduoduo.ui.a.a(this);
                            this.n.a();
                        }
                        this.q.setVisibility(4);
                        this.r.setVisibility(8);
                    } else if (com.shoujiduoduo.util.a.f()) {
                        if (this.m == null) {
                            this.m = new WallAdView(this);
                            this.m.a();
                            if (!"true".equals(com.umeng.analytics.b.c(getApplicationContext(), "wallad_banner_switch")) || !com.shoujiduoduo.util.a.e() || this.J) {
                                this.B.setVisibility(8);
                            } else {
                                this.B.setVisibility(0);
                            }
                        }
                        as.b(this, E, System.currentTimeMillis());
                        this.q.setVisibility(4);
                        this.r.setVisibility(8);
                    } else {
                        if (this.l == null) {
                            this.l = new g(this);
                            this.l.a();
                            this.l.a(this.q);
                        }
                        a(this.l.d());
                        this.l.a(this.q);
                        this.r.setVisibility(0);
                    }
                    this.p.a(3);
                    this.h.setVisibility(4);
                    this.s.setVisibility(4);
                    this.u.setVisibility(4);
                    this.v.setVisibility(4);
                    if (this.g) {
                        this.f = a(false);
                        this.b.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, this.f, (Drawable) null, (Drawable) null);
                        this.g = false;
                        as.b((Context) this, C, 1);
                        break;
                    }
                }
                break;
        }
        x.a().b(com.shoujiduoduo.a.a.b.OBSERVER_SCENE, new o(this));
    }

    private void b(boolean z2) {
        this.e = 0;
        this.p.a(0);
        this.q.setVisibility(4);
        this.s.setVisibility(0);
        this.h.setVisibility(4);
        this.u.setVisibility(4);
        this.v.setVisibility(4);
        this.r.setVisibility(0);
        if (!com.shoujiduoduo.util.a.e() || this.J) {
            this.B.setVisibility(8);
            return;
        }
        this.B.setVisibility(0);
        if ("true".equals(com.umeng.analytics.b.c(getApplicationContext(), "bd_radio_switch")) && !z2 && com.shoujiduoduo.util.a.i()) {
            com.shoujiduoduo.base.a.a.a("xxxad", "radio switch init");
            this.B.b();
        }
    }

    public void onAttachFragment(Fragment fragment) {
        super.onAttachFragment(fragment);
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 == 4) {
            if (this.q.isShown()) {
                int currentScene = this.p.getCurrentScene();
                if (currentScene == 1) {
                    this.j.c();
                    return true;
                } else if (currentScene != 3) {
                    return true;
                } else {
                    this.l.c();
                    return true;
                }
            } else {
                if (this.p.getCurrentScene() == 2) {
                    if (this.k.a(i2, keyEvent)) {
                        return true;
                    }
                } else if (this.p.getCurrentScene() == 3 && com.shoujiduoduo.util.a.f() && this.m != null && this.m.a(i2, keyEvent)) {
                    return true;
                }
                this.K.show();
                return true;
            }
        } else if (i2 != 82) {
            return super.onKeyDown(i2, keyEvent);
        } else {
            new ae(this, true).showAtLocation(findViewById(R.id.framework), 81, 0, 0);
            return true;
        }
    }

    private static class c {

        /* renamed from: a  reason: collision with root package name */
        public String f853a;
        public String b;

        private c() {
        }

        /* synthetic */ c(a aVar) {
            this();
        }
    }

    /* access modifiers changed from: private */
    public void b(String str) {
        if (str != null && str.length() > 0) {
            new a.C0034a(this).b((int) R.string.update_hint).a((int) R.string.has_update_hint).a((int) R.string.ok, new d(this)).b((int) R.string.cancel, new c(this)).a().show();
        }
    }

    public void b() {
        this.A = new ProgressDialog(this);
        this.A.setProgressStyle(0);
        this.A.setIndeterminate(true);
        this.A.setTitle("");
        this.A.setMessage(getResources().getString(R.string.cleaning_cache));
        this.A.setCancelable(false);
        this.A.show();
        i.a(new e(this));
    }

    /* access modifiers changed from: private */
    public void j() {
        String a2;
        if (!I) {
            I = true;
            String a3 = am.a().a("update_version");
            com.shoujiduoduo.base.a.a.a(f849a, "onConfigListener: update version: " + a3);
            com.shoujiduoduo.base.a.a.a(f849a, "onConfigListener: cur version: " + f.s());
            if (a3.compareToIgnoreCase(f.s()) > 0 && (a2 = am.a().a("update_url")) != null && a2.length() > 0) {
                Message obtainMessage = this.R.obtainMessage(1100, a2);
                com.shoujiduoduo.base.a.a.a(f849a, "onConfigListener: update url: " + a2);
                this.R.sendMessage(obtainMessage);
            }
        }
    }

    public void a(String str) {
        if (TextUtils.isEmpty(str)) {
            this.B.setVisibility(8);
        } else if (com.shoujiduoduo.util.a.e() && !this.J && "true".equals(com.umeng.analytics.b.c(getApplicationContext(), "category_banner_switch"))) {
            this.B.setVisibility(0);
        }
    }
}
