package com.shoujiduoduo.ringtone.activity;

import android.content.Intent;
import cn.banshenggua.aichang.player.PlayerService;
import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.ringtone.activity.RingToneDuoduoActivity;
import com.shoujiduoduo.util.f;
import com.tencent.tauth.AuthActivity;

/* compiled from: RingToneDuoduoActivity */
class k extends x.b {
    final /* synthetic */ RingToneDuoduoActivity d;

    k(RingToneDuoduoActivity ringToneDuoduoActivity) {
        this.d = ringToneDuoduoActivity;
    }

    public void a() {
        Intent intent = this.d.getIntent();
        if (intent != null) {
            String stringExtra = intent.getStringExtra(AuthActivity.ACTION_KEY);
            a.a(RingToneDuoduoActivity.f849a, "action = " + stringExtra);
            if (stringExtra == null) {
                if (intent.getIntExtra(f.f1667a, -1) > 0) {
                }
            } else if (stringExtra.equalsIgnoreCase("search")) {
                String stringExtra2 = intent.getStringExtra("para");
                a.a(RingToneDuoduoActivity.f849a, "push task: search, keyword = " + stringExtra2);
                if (stringExtra2 != null) {
                    new l(this, stringExtra2).start();
                }
            } else if (stringExtra.equalsIgnoreCase(PlayerService.ACTION_PLAY)) {
                String stringExtra3 = intent.getStringExtra("para");
                a.a(RingToneDuoduoActivity.f849a, "push task: play, keyword = " + stringExtra3);
                if (stringExtra3 != null) {
                    new m(this, stringExtra3).start();
                }
            } else if (stringExtra.equalsIgnoreCase("webview")) {
                this.d.R.sendMessageDelayed(this.d.R.obtainMessage(1133, intent.getStringExtra("para")), 1000);
            } else if (stringExtra.equalsIgnoreCase("music_album")) {
                RingToneDuoduoActivity.c cVar = new RingToneDuoduoActivity.c(null);
                cVar.f853a = intent.getStringExtra("para");
                cVar.b = intent.getStringExtra("title");
                this.d.R.sendMessageDelayed(this.d.R.obtainMessage(1135, cVar), 1000);
            } else if (!stringExtra.equalsIgnoreCase("ad") && !stringExtra.equalsIgnoreCase("update")) {
                a.c(RingToneDuoduoActivity.f849a, "not support action");
            }
        }
    }
}
