package com.shoujiduoduo.wallpaper.utils;

import android.content.Context;
import android.view.animation.Interpolator;
import android.widget.Scroller;

public class n extends Scroller {

    /* renamed from: a  reason: collision with root package name */
    private double f1874a = 1.0d;

    public n(FixViewPager fixViewPager, Context context, Interpolator interpolator) {
        super(context, interpolator);
    }

    public void a(double d) {
        this.f1874a = d;
    }

    public void startScroll(int i, int i2, int i3, int i4, int i5) {
        super.startScroll(i, i2, i3, i4, (int) (((double) i5) * this.f1874a));
    }
}
