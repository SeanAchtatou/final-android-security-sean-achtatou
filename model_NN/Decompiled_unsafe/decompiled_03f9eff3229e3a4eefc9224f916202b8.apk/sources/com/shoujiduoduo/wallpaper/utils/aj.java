package com.shoujiduoduo.wallpaper.utils;

import android.content.Context;
import android.media.MediaScannerConnection;
import android.net.Uri;
import java.io.File;

public class aj implements MediaScannerConnection.MediaScannerConnectionClient {

    /* renamed from: a  reason: collision with root package name */
    private MediaScannerConnection f1840a;
    private File b;

    public aj(Context context, File file) {
        this.b = file;
        this.f1840a = new MediaScannerConnection(context, this);
        this.f1840a.connect();
    }

    public void onMediaScannerConnected() {
        this.f1840a.scanFile(this.b.getAbsolutePath(), null);
    }

    public void onScanCompleted(String str, Uri uri) {
        this.f1840a.disconnect();
    }
}
