package com.shoujiduoduo.wallpaper.utils;

import android.view.MotionEvent;
import android.view.View;

final class bg implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ j f1856a;

    bg(j jVar) {
        this.f1856a = jVar;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        int top = this.f1856a.c.findViewById(f.c("R.id.menu_layout")).getTop();
        int y = (int) motionEvent.getY();
        if (motionEvent.getAction() == 1 && y < top) {
            this.f1856a.dismiss();
        }
        return true;
    }
}
