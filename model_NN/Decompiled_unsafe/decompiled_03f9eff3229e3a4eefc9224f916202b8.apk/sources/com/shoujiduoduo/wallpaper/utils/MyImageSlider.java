package com.shoujiduoduo.wallpaper.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.AbsoluteLayout;
import android.widget.ImageView;
import android.widget.Scroller;
import com.d.a.b.c;
import com.d.a.b.d;
import com.shoujiduoduo.wallpaper.a.c;
import com.shoujiduoduo.wallpaper.kernel.App;
import com.shoujiduoduo.wallpaper.kernel.a;
import com.shoujiduoduo.wallpaper.kernel.b;
import java.io.File;

public class MyImageSlider extends ViewGroup implements s {
    /* access modifiers changed from: private */
    public static final String b = MyImageSlider.class.getSimpleName();

    /* renamed from: a  reason: collision with root package name */
    public boolean f1825a = true;
    /* access modifiers changed from: private */
    public ImageView[] c = new ImageView[3];
    /* access modifiers changed from: private */
    public int[] d = new int[3];
    private Context e;
    /* access modifiers changed from: private */
    public int f;
    /* access modifiers changed from: private */
    public int g;
    /* access modifiers changed from: private */
    public c h;
    /* access modifiers changed from: private */
    public int i = 0;
    /* access modifiers changed from: private */
    public int j;
    /* access modifiers changed from: private */
    public int k = 0;
    /* access modifiers changed from: private */
    public Scroller l;
    private boolean m = false;
    /* access modifiers changed from: private */
    public y n;
    /* access modifiers changed from: private */
    public Matrix[] o = new Matrix[3];
    private com.d.a.b.c p = new c.a().a(false).b(true).c(true).a(Bitmap.Config.RGB_565).c();
    private Drawable q = null;
    /* access modifiers changed from: private */
    public boolean r = true;
    private View.OnTouchListener s = new o(this);
    private int t = 0;

    public MyImageSlider(Context context) {
        super(context);
        this.e = context;
    }

    public MyImageSlider(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.e = context;
    }

    public MyImageSlider(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.e = context;
    }

    private static Bitmap a(Bitmap[] bitmapArr, Bitmap bitmap) {
        int length = bitmapArr.length;
        int i2 = 0;
        while (true) {
            if (i2 < length) {
                if (bitmap == bitmapArr[i2]) {
                    break;
                }
                i2++;
            } else {
                i2 = 0;
                break;
            }
        }
        int i3 = i2 + -1 < 0 ? length - 1 : i2 - 1;
        while (bitmapArr[i3] == null) {
            i3 = i3 + -1 < 0 ? length - 1 : i3 - 1;
        }
        return bitmapArr[i3];
    }

    private void a(int i2, ImageView imageView) {
        b.a(b, "pos = " + i2);
        if (i2 < 0 || i2 >= this.h.a()) {
            com.umeng.analytics.b.b(this.e, "EVENT_LIST_POS_ERROR");
            return;
        }
        this.q = null;
        File a2 = com.d.a.b.a.b.a(this.h.a(i2).b, d.a().d());
        if (a2 != null) {
            b.a(b, "cache thumb path: " + a2.getAbsolutePath());
        }
        if (a2 != null) {
            try {
                this.q = Drawable.createFromPath(a2.getAbsolutePath());
            } catch (OutOfMemoryError e2) {
            }
        }
        if (this.q == null) {
            this.q = App.e;
        }
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setImageDrawable(this.q);
        d.a().a(this.h.a(i2).c, imageView, this.p, new r(this, this.h.a(i2)), new p(this));
    }

    private static Bitmap b(Bitmap[] bitmapArr, Bitmap bitmap) {
        int length = bitmapArr.length;
        int i2 = 0;
        while (true) {
            if (i2 < length) {
                if (bitmap == bitmapArr[i2]) {
                    break;
                }
                i2++;
            } else {
                i2 = 0;
                break;
            }
        }
        while (true) {
            i2 = (i2 + 1) % length;
            if (bitmapArr[i2] != null) {
                return bitmapArr[i2];
            }
        }
    }

    private void b() {
        for (int i2 = 0; i2 < this.c.length; i2++) {
            if ((this.i + i2) - 1 >= 0 && (this.i + i2) - 1 < this.h.a()) {
                a((this.i + i2) - 1, this.c[i2]);
            }
        }
    }

    public Bitmap a(int i2, int i3) {
        this.f1825a = true;
        Bitmap bitmap = ((BitmapDrawable) this.c[1].getDrawable()).getBitmap();
        if (i3 != this.g) {
            i2 = (int) (((float) i2) * (((float) this.g) / ((float) i3)));
            i3 = this.g;
        }
        int width = (bitmap.getWidth() * i3) / bitmap.getHeight();
        Bitmap createScaledBitmap = Bitmap.createScaledBitmap(bitmap, width, i3, true);
        Bitmap[] bitmapArr = new Bitmap[3];
        if (width > this.f) {
            int i4 = ((width - this.f) / 2) + this.t;
            bitmapArr[1] = Bitmap.createBitmap(createScaledBitmap, i4, 0, this.f, this.g);
            if (i4 != 0) {
                bitmapArr[0] = Bitmap.createBitmap(createScaledBitmap, 0, 0, i4, this.g);
            }
            if (this.f + i4 < width) {
                bitmapArr[2] = Bitmap.createBitmap(createScaledBitmap, this.f + i4, 0, (width - i4) - this.f, this.g);
            }
        } else {
            bitmapArr[1] = createScaledBitmap;
        }
        Bitmap createBitmap = Bitmap.createBitmap(i2, i3, Bitmap.Config.RGB_565);
        Canvas canvas = new Canvas(createBitmap);
        int width2 = bitmapArr[1].getWidth();
        if (i2 > width2) {
            canvas.drawBitmap(bitmapArr[1], (float) ((i2 - width2) / 2), 0.0f, (Paint) null);
            int i5 = (i2 - width2) / 2;
            Bitmap bitmap2 = bitmapArr[1];
            int i6 = i5;
            while (i6 > 0) {
                bitmap2 = a(bitmapArr, bitmap2);
                if (i6 >= bitmap2.getWidth()) {
                    canvas.drawBitmap(bitmap2, (float) (i6 - bitmap2.getWidth()), 0.0f, (Paint) null);
                    i6 -= bitmap2.getWidth();
                } else {
                    canvas.drawBitmap(bitmap2, new Rect(bitmap2.getWidth() - i6, 0, bitmap2.getWidth(), bitmap2.getHeight()), new Rect(0, 0, i6, bitmap2.getHeight()), (Paint) null);
                    i6 = 0;
                }
            }
            int i7 = i5 + width2;
            Bitmap bitmap3 = bitmapArr[1];
            while (i7 < i2) {
                bitmap3 = b(bitmapArr, bitmap3);
                if (bitmap3.getWidth() + i7 <= i2) {
                    canvas.drawBitmap(bitmap3, (float) i7, 0.0f, (Paint) null);
                    i7 += bitmap3.getWidth();
                } else {
                    canvas.drawBitmap(bitmap3, (float) i7, 0.0f, (Paint) null);
                    i7 = i2;
                }
            }
            if (!(bitmapArr[1] == createScaledBitmap || bitmapArr[1] == null)) {
                bitmapArr[1].recycle();
                bitmapArr[1] = null;
            }
            if (bitmapArr[0] != null) {
                bitmapArr[0].recycle();
                bitmapArr[0] = null;
            }
            if (bitmapArr[2] != null) {
                bitmapArr[2].recycle();
                bitmapArr[2] = null;
            }
            if (createScaledBitmap == bitmap || createScaledBitmap == null) {
                return createBitmap;
            }
            createScaledBitmap.recycle();
            return createBitmap;
        }
        if (bitmapArr[0] != null) {
            bitmapArr[0].recycle();
            bitmapArr[0] = null;
        }
        if (bitmapArr[2] != null) {
            bitmapArr[2].recycle();
            bitmapArr[2] = null;
        }
        if (!(createScaledBitmap == bitmapArr[1] || createScaledBitmap == bitmap || createScaledBitmap == null)) {
            createScaledBitmap.recycle();
        }
        if (bitmapArr[1] == bitmap) {
            this.f1825a = false;
        }
        return bitmapArr[1];
    }

    public void a(int i2, int i3, int i4) {
        int i5 = (-(this.d[1] - this.f)) / 2;
        this.t = ((int) ((((float) (this.d[1] - this.f)) * ((float) i2)) / ((float) (i4 - i3)))) + i5;
        if (this.t < i5) {
            this.t = i5;
        } else if (this.t > (-i5)) {
            this.t = -i5;
        }
        Matrix matrix = new Matrix();
        matrix.set(this.o[1]);
        matrix.postTranslate((float) (-this.t), 0.0f);
        this.c[1].setImageMatrix(matrix);
    }

    public void a(com.shoujiduoduo.wallpaper.a.c cVar, int i2) {
        this.h = cVar;
        this.i = i2;
        this.f = this.e.getResources().getDisplayMetrics().widthPixels;
        this.g = this.e.getResources().getDisplayMetrics().heightPixels;
        this.j = this.f / 6;
        this.l = new Scroller(this.e, new AccelerateDecelerateInterpolator());
        setOnTouchListener(this.s);
        for (int i3 = 0; i3 < this.c.length; i3++) {
            this.o[i3] = new Matrix();
            this.c[i3] = new ImageView(this.e);
            this.c[i3].setLayoutParams(new AbsoluteLayout.LayoutParams(this.f, this.g, this.f * i3, 0));
            this.c[i3].setScaleType(ImageView.ScaleType.CENTER_CROP);
            addView(this.c[i3]);
        }
        b();
    }

    public void a(boolean z) {
        this.r = z;
    }

    public Bitmap b(boolean z) {
        this.f1825a = true;
        if ("samsung".equalsIgnoreCase(Build.BRAND)) {
            Bitmap bitmap = ((BitmapDrawable) this.c[1].getDrawable()).getBitmap();
            try {
                Bitmap createScaledBitmap = Bitmap.createScaledBitmap(bitmap, this.d[1], this.g, true);
                if (this.d[1] <= this.f || this.d[1] <= this.g) {
                    if (createScaledBitmap == bitmap) {
                        this.f1825a = false;
                    }
                    return createScaledBitmap;
                }
                int i2 = (this.t + (this.d[1] / 2)) - (this.g / 2);
                if (i2 < 0) {
                    i2 = 0;
                }
                if (this.g + i2 > this.d[1]) {
                    i2 = this.d[1] - this.g;
                }
                Bitmap createBitmap = Bitmap.createBitmap(createScaledBitmap, i2, 0, this.g, this.g);
                if (createScaledBitmap == bitmap) {
                    return createBitmap;
                }
                createScaledBitmap.recycle();
                return createBitmap;
            } catch (Exception e2) {
                return null;
            }
        } else if (!z || this.d[1] <= this.f) {
            try {
                Bitmap createBitmap2 = Bitmap.createBitmap(this.f, this.g, Bitmap.Config.RGB_565);
                this.c[1].draw(new Canvas(createBitmap2));
                return createBitmap2;
            } catch (Exception e3) {
                return null;
            }
        } else {
            Bitmap bitmap2 = ((BitmapDrawable) this.c[1].getDrawable()).getBitmap();
            if (bitmap2 == null) {
                return null;
            }
            try {
                Bitmap createScaledBitmap2 = Bitmap.createScaledBitmap(bitmap2, this.d[1], this.g, true);
                if (createScaledBitmap2 != bitmap2) {
                    return createScaledBitmap2;
                }
                this.f1825a = false;
                return createScaledBitmap2;
            } catch (Exception e4) {
                return null;
            }
        }
    }

    public void computeScroll() {
        if (this.l != null && this.l.computeScrollOffset()) {
            scrollTo(this.l.getCurrX(), 0);
            if (this.l.isFinished()) {
                b.a(b, "scroll animation finished! mScrollPos = " + this.k);
                if (this.k == this.f && this.i < this.h.a() - 1) {
                    this.t = 0;
                    ImageView imageView = this.c[0];
                    ImageView imageView2 = this.c[2];
                    ImageView imageView3 = this.c[1];
                    int i2 = this.d[0];
                    int i3 = this.d[1];
                    int i4 = this.d[2];
                    Matrix matrix = this.o[0];
                    Matrix matrix2 = this.o[1];
                    Matrix matrix3 = this.o[2];
                    this.c[0] = imageView3;
                    this.c[1] = imageView2;
                    this.c[2] = imageView;
                    this.d[0] = i3;
                    this.d[1] = i4;
                    this.d[2] = i2;
                    this.o[0] = matrix2;
                    this.o[1] = matrix3;
                    this.o[2] = matrix;
                    this.c[0].setImageMatrix(this.o[0]);
                    if (this.i < this.h.a() - 1) {
                        this.i++;
                        if (this.i + 1 < this.h.a()) {
                            a(this.i + 1, this.c[2]);
                        } else {
                            this.c[2].setImageResource(f.a(f.d().getPackageName(), "drawable", "wallpaperdd_background"));
                        }
                        if (this.n != null) {
                            this.n.b(this.i);
                        }
                    }
                    requestLayout();
                } else if (this.k == (-this.f) && this.i > 0) {
                    this.t = 0;
                    ImageView imageView4 = this.c[0];
                    ImageView imageView5 = this.c[2];
                    ImageView imageView6 = this.c[1];
                    int i5 = this.d[0];
                    int i6 = this.d[1];
                    int i7 = this.d[2];
                    Matrix matrix4 = this.o[0];
                    Matrix matrix5 = this.o[1];
                    Matrix matrix6 = this.o[2];
                    this.c[0] = imageView5;
                    this.c[1] = imageView4;
                    this.c[2] = imageView6;
                    this.d[0] = i7;
                    this.d[1] = i5;
                    this.d[2] = i6;
                    this.o[0] = matrix6;
                    this.o[1] = matrix4;
                    this.o[2] = matrix5;
                    this.c[2].setImageMatrix(this.o[2]);
                    if (this.i > 0) {
                        this.i--;
                        if (this.i - 1 < 0 || this.i - 1 >= this.h.a()) {
                            this.c[0].setImageResource(f.a(f.d().getPackageName(), "drawable", "wallpaperdd_background"));
                        } else {
                            a(this.i - 1, this.c[0]);
                        }
                        if (this.n != null) {
                            this.n.b(this.i);
                        }
                    }
                    requestLayout();
                } else if (this.m) {
                    this.m = false;
                    requestLayout();
                }
            }
            postInvalidate();
        }
    }

    public com.shoujiduoduo.wallpaper.kernel.d getOriginalBmpSize() {
        Bitmap bitmap = ((BitmapDrawable) this.c[1].getDrawable()).getBitmap();
        if (bitmap != null) {
            return new com.shoujiduoduo.wallpaper.kernel.d(bitmap.getWidth(), bitmap.getHeight());
        }
        return null;
    }

    public void m() {
        this.m = true;
    }

    public void n() {
        this.m = false;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        if (!this.m && this.l.isFinished()) {
            b.a(b, "scroll, onLayout");
            for (int i6 = 0; i6 < this.c.length; i6++) {
                this.c[i6].layout(this.f * (i6 - 1), 0, this.f * i6, this.g);
            }
            scrollTo(0, 0);
            String str = (String) this.c[1].getTag();
            if (this.i >= 0 && this.i < this.h.a()) {
                if (str == "loading") {
                    if (this.n != null) {
                        this.n.a(this.h.a(this.i).k, a.LOADING);
                    }
                } else if (str == "failed") {
                    if (this.n != null) {
                        this.n.a(this.h.a(this.i).k, a.LOAD_FAILED);
                    }
                } else if (str == "finished") {
                    if (this.n != null) {
                        this.n.a(this.h.a(this.i).k, a.LOAD_FINISHED);
                    }
                } else if (this.n != null) {
                    this.n.a(this.h.a(this.i).k, a.LOADING);
                }
            }
            this.k = 0;
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        int childCount = getChildCount();
        for (int i4 = 0; i4 < childCount; i4++) {
            getChildAt(i4).measure(i2, i3);
        }
    }

    public void setListener(y yVar) {
        this.n = yVar;
    }
}
