package com.shoujiduoduo.wallpaper.utils;

import android.support.v4.view.ViewPager;

final class w implements ViewPager.OnPageChangeListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ PagerSlidingTabStrip f1881a;

    private w(PagerSlidingTabStrip pagerSlidingTabStrip) {
        this.f1881a = pagerSlidingTabStrip;
    }

    /* synthetic */ w(PagerSlidingTabStrip pagerSlidingTabStrip, byte b) {
        this(pagerSlidingTabStrip);
    }

    public final void onPageScrollStateChanged(int i) {
        if (i == 0) {
            PagerSlidingTabStrip.a(this.f1881a, this.f1881a.f.getCurrentItem(), 0);
        }
        if (this.f1881a.f1826a != null) {
            this.f1881a.f1826a.onPageScrollStateChanged(i);
        }
    }

    public final void onPageScrolled(int i, float f, int i2) {
        this.f1881a.h = i;
        this.f1881a.i = f;
        PagerSlidingTabStrip.a(this.f1881a, i, (int) (((float) this.f1881a.e.getChildAt(i).getWidth()) * f));
        this.f1881a.b();
        this.f1881a.invalidate();
        if (this.f1881a.f1826a != null) {
            this.f1881a.f1826a.onPageScrolled(i, f, i2);
        }
    }

    public final void onPageSelected(int i) {
        if (this.f1881a.f1826a != null) {
            this.f1881a.f1826a.onPageSelected(i);
        }
    }
}
