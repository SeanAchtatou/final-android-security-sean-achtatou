package com.shoujiduoduo.wallpaper.utils;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.d.a.b.d;
import com.shoujiduoduo.wallpaper.a.j;
import com.shoujiduoduo.wallpaper.kernel.b;

public class ai extends BaseAdapter {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public /* synthetic */ ah f1839a;

    public ai(ah ahVar) {
        this.f1839a = ahVar;
    }

    public int getCount() {
        return this.f1839a.d.a();
    }

    public Object getItem(int i) {
        return null;
    }

    public long getItemId(int i) {
        return 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int i, View view, ViewGroup viewGroup) {
        View view2;
        if (i == getCount() - 1) {
            b.a(ah.i, "reach bottom of the gridview.");
            if (!this.f1839a.d.f() && this.f1839a.d.e()) {
                b.a(ah.i, "load more data.");
                this.f1839a.d.d();
            }
        }
        if (view == null || !"similar_pic_thumb".equals((String) view.getTag())) {
            View inflate = LayoutInflater.from(this.f1839a.f1838a).inflate(f.c("R.layout.wallpaperdd_similar_image_listitem_layout"), viewGroup, false);
            inflate.setTag("similar_pic_thumb");
            view2 = inflate;
        } else {
            view2 = view;
        }
        ImageView imageView = (ImageView) view2.findViewById(f.c("R.id.similar_pic_imageview"));
        if (!(view2 == view && this.f1839a.d.a(i).b == ((String) imageView.getTag()))) {
            j a2 = this.f1839a.d.a(i);
            d.a().a(a2 == null ? null : a2.b, imageView, this.f1839a.g, this.f1839a.h, new an(this));
            imageView.setOnClickListener(new ao(this, i));
            ((TextView) view2.findViewById(f.c("R.id.similar_pic_title"))).setText(a2.h);
        }
        return view2;
    }
}
