package com.shoujiduoduo.wallpaper.utils;

import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;
import com.qq.e.comm.constants.ErrorCode;
import com.shoujiduoduo.wallpaper.kernel.b;

final class o implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    private int f1875a;
    private int b;
    private /* synthetic */ MyImageSlider c;

    o(MyImageSlider myImageSlider) {
        this.c = myImageSlider;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        if (!this.c.r) {
            return false;
        }
        if (motionEvent.getAction() == 0) {
            if (this.c.l.isFinished()) {
                this.f1875a = (int) motionEvent.getRawX();
                this.b = 0;
                b.a(MyImageSlider.b, "width = " + this.c.getWidth() + ", height = " + this.c.getHeight());
            }
            return true;
        } else if (motionEvent.getAction() == 1) {
            if (!this.c.l.isFinished()) {
                return true;
            }
            b.a(MyImageSlider.b, "scrollx = " + this.b + ", threshold = " + this.c.j + ", mScrollPos = " + this.c.k);
            if (this.b > this.c.j && this.c.i < this.c.h.a() - 1) {
                b.a(MyImageSlider.b, "scroll left to next image");
                this.c.l.startScroll(this.c.k + this.b, 0, this.c.f - this.b, 0, ErrorCode.InitError.INIT_AD_ERROR);
                MyImageSlider myImageSlider = this.c;
                myImageSlider.k = myImageSlider.k + this.c.f;
            } else if (this.b >= (-this.c.j) || this.c.i <= 0) {
                b.a(MyImageSlider.b, "scroll back to current image");
                if (this.c.i == 0 && this.b < (-this.c.j)) {
                    Toast.makeText(f.d(), this.c.getResources().getString(f.a(f.d().getPackageName(), "string", "wallpaperdd_toast_first_pic")), 0).show();
                } else if (this.c.i == this.c.h.a() - 1 && this.b > this.c.j) {
                    Toast.makeText(f.d(), this.c.getResources().getString(f.a(f.d().getPackageName(), "string", "wallpaperdd_toast_last_pic")), 0).show();
                }
                if (this.b <= 10 && this.b >= -10 && this.c.n != null) {
                    this.c.n.m();
                }
                this.c.l.startScroll(this.c.k + this.b, 0, -this.b, 0, ErrorCode.InitError.INIT_AD_ERROR);
            } else {
                b.a(MyImageSlider.b, "scroll right to previous image");
                this.c.l.startScroll(this.c.k + this.b, 0, (-this.c.f) - this.b, 0, ErrorCode.InitError.INIT_AD_ERROR);
                MyImageSlider myImageSlider2 = this.c;
                myImageSlider2.k = myImageSlider2.k - this.c.f;
            }
            this.c.invalidate();
            return true;
        } else if (motionEvent.getAction() != 2) {
            return motionEvent.getAction() == 8;
        } else {
            if (!this.c.l.isFinished()) {
                return true;
            }
            int rawX = (int) motionEvent.getRawX();
            if (rawX != this.f1875a) {
                this.b -= (rawX - this.f1875a) * 2;
                if (this.b > this.c.f) {
                    this.b = this.c.f;
                } else if (this.b < this.c.f * -1) {
                    this.b = this.c.f * -1;
                }
                this.f1875a = rawX;
                this.c.scrollTo(this.c.k + this.b, 0);
            }
            return true;
        }
    }
}
