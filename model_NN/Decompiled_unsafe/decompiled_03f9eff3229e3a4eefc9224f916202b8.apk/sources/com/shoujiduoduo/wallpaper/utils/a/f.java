package com.shoujiduoduo.wallpaper.utils.a;

import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.widget.Toast;
import com.shoujiduoduo.wallpaper.kernel.b;
import com.shoujiduoduo.wallpaper.utils.m;
import java.io.File;

public class f extends b {
    private boolean b(String str) {
        try {
            Toast.makeText(this.b, this.b.getString(com.shoujiduoduo.wallpaper.utils.f.c("R.string.wallpaperdd_toast_on_setting_lockscreen")), 0).show();
            File file = new File(this.b.getFilesDir(), "lockwallpaper");
            b.a("LockScreenWallpaper", "smartisan, file target = " + file.getAbsolutePath());
            if (!m.a(new File(str), file)) {
                b.a("LockScreenWallpaper", "smartisan, copy failed.");
                return false;
            }
            if (!file.setReadable(true, false)) {
                b.a("LockScreenWallpaper", "smartisan, setReadable failed.");
            }
            boolean putString = Settings.System.putString(this.b.getContentResolver(), "lockscreen_background", Uri.fromFile(file).toString());
            b.a("LockScreenWallpaper", "smartisan, ret = " + putString);
            Toast.makeText(this.b, this.b.getString(com.shoujiduoduo.wallpaper.utils.f.c("R.string.wallpaperdd_toast_on_lockscreen_success")), 0).show();
            return putString;
        } catch (Exception e) {
            e.printStackTrace();
            b.a("LockScreenWallpaper", "smartisan exception");
            Toast.makeText(this.b, this.b.getString(com.shoujiduoduo.wallpaper.utils.f.c("R.string.wallpaperdd_toast_on_lockscreen_failed")), 0).show();
            return false;
        } catch (Error e2) {
            e2.printStackTrace();
            b.a("LockScreenWallpaper", "smartisan error");
            Toast.makeText(this.b, this.b.getString(com.shoujiduoduo.wallpaper.utils.f.c("R.string.wallpaperdd_toast_on_lockscreen_failed")), 0).show();
            return false;
        }
    }

    public void a(Context context, String str) {
        super.a(context, "smartisan");
    }

    public boolean a() {
        try {
            b.a("LockScreenWallpaper", "BRAND = " + Build.BRAND);
            return "smartisan".equalsIgnoreCase(Build.BRAND);
        } catch (Exception e) {
            return false;
        }
    }

    public boolean a(String str) {
        b.a("LockScreenWallpaper", "set smartisan lock wallpaper with path");
        return b(str);
    }
}
