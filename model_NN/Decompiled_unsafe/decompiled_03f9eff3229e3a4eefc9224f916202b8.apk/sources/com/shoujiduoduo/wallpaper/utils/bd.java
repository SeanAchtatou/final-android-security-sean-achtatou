package com.shoujiduoduo.wallpaper.utils;

import android.app.ProgressDialog;
import android.os.Handler;
import android.view.View;
import com.shoujiduoduo.wallpaper.a.g;
import com.shoujiduoduo.wallpaper.a.k;

final class bd implements View.OnClickListener, g {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public int f1853a;
    /* access modifiers changed from: private */
    public String b = null;
    /* access modifiers changed from: private */
    public String c = null;
    /* access modifiers changed from: private */
    public ProgressDialog d;
    private Handler e = new be(this);
    /* access modifiers changed from: private */
    public /* synthetic */ c f;

    public bd(c cVar, int i, String str, String str2) {
        this.f = cVar;
        this.f1853a = 800000000 + i;
        this.b = str;
        this.c = str2;
    }

    public final void a(k kVar, int i) {
        this.e.sendMessage(this.e.obtainMessage(14001, i, 0));
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x006a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void onClick(android.view.View r5) {
        /*
            r4 = this;
            r3 = 0
            com.shoujiduoduo.wallpaper.utils.c r0 = r4.f
            com.shoujiduoduo.wallpaper.a.k r0 = r0.b
            if (r0 == 0) goto L_0x0016
            com.shoujiduoduo.wallpaper.utils.c r0 = r4.f
            com.shoujiduoduo.wallpaper.a.k r0 = r0.b
            int r0 = r0.b()
            switch(r0) {
                case 5: goto L_0x0057;
                case 6: goto L_0x0074;
                default: goto L_0x0016;
            }
        L_0x0016:
            com.shoujiduoduo.wallpaper.a.m r0 = com.shoujiduoduo.wallpaper.a.m.b()
            int r1 = r4.f1853a
            com.shoujiduoduo.wallpaper.a.k r0 = r0.a(r1)
            r0.a(r4)
            int r1 = r0.a()
            if (r1 != 0) goto L_0x0080
            r0.d()
            android.app.ProgressDialog r0 = new android.app.ProgressDialog
            com.shoujiduoduo.wallpaper.utils.c r1 = r4.f
            android.content.Context r1 = r1.c
            r0.<init>(r1)
            r4.d = r0
            android.app.ProgressDialog r0 = r4.d
            r0.setCancelable(r3)
            android.app.ProgressDialog r0 = r4.d
            r0.setIndeterminate(r3)
            android.app.ProgressDialog r0 = r4.d
            java.lang.String r1 = ""
            r0.setTitle(r1)
            android.app.ProgressDialog r0 = r4.d
            java.lang.String r1 = "正在获取图片，请稍候..."
            r0.setMessage(r1)
            android.app.ProgressDialog r0 = r4.d
            r0.show()
        L_0x0056:
            return
        L_0x0057:
            java.lang.String r0 = com.shoujiduoduo.wallpaper.utils.c.f1865a
            java.lang.String r1 = "log album"
            com.shoujiduoduo.wallpaper.kernel.b.a(r0, r1)
            java.lang.String r0 = "CLICKPIC_IN_LIST_ALBUM"
        L_0x0062:
            com.shoujiduoduo.wallpaper.utils.c r1 = r4.f
            android.content.Context r1 = r1.c
            if (r1 == 0) goto L_0x0016
            com.shoujiduoduo.wallpaper.utils.c r1 = r4.f
            android.content.Context r1 = r1.c
            com.umeng.analytics.b.b(r1, r0)
            goto L_0x0016
        L_0x0074:
            java.lang.String r0 = com.shoujiduoduo.wallpaper.utils.c.f1865a
            java.lang.String r1 = "log sexy"
            com.shoujiduoduo.wallpaper.kernel.b.a(r0, r1)
            java.lang.String r0 = "CLICKPIC_IN_LIST_SEXY"
            goto L_0x0062
        L_0x0080:
            android.content.Intent r0 = new android.content.Intent
            com.shoujiduoduo.wallpaper.utils.c r1 = r4.f
            android.content.Context r1 = r1.c
            java.lang.Class<com.shoujiduoduo.wallpaper.activity.WallpaperActivity> r2 = com.shoujiduoduo.wallpaper.activity.WallpaperActivity.class
            r0.<init>(r1, r2)
            java.lang.String r1 = "listid"
            int r2 = r4.f1853a
            r0.putExtra(r1, r2)
            java.lang.String r1 = "serialno"
            r0.putExtra(r1, r3)
            java.lang.String r1 = "uploader"
            java.lang.String r2 = r4.b
            r0.putExtra(r1, r2)
            java.lang.String r1 = "intro"
            java.lang.String r2 = r4.c
            r0.putExtra(r1, r2)
            com.shoujiduoduo.wallpaper.utils.c r1 = r4.f
            android.content.Context r1 = r1.c
            r1.startActivity(r0)
            goto L_0x0056
        */
        throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.wallpaper.utils.bd.onClick(android.view.View):void");
    }
}
