package com.shoujiduoduo.wallpaper.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.widget.TextView;
import android.widget.Toast;
import com.shoujiduoduo.wallpaper.kernel.b;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class i extends AsyncTask {
    private static final String c = i.class.getSimpleName();

    /* renamed from: a  reason: collision with root package name */
    public AlertDialog f1870a;
    public Context b;
    private String d = (String.valueOf(f.a()) + "WallpaperDuoduo.apk");
    private TextView e;

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Boolean doInBackground(String... strArr) {
        int i = 0;
        if (strArr == null || strArr[0] == null) {
            return Boolean.FALSE;
        }
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(strArr[0]).openConnection();
            b.a(c, "download soft: conn = " + httpURLConnection.toString());
            httpURLConnection.connect();
            b.a(c, "download soft: connect finished!");
            int responseCode = httpURLConnection.getResponseCode();
            if (responseCode == 200 || responseCode == 206) {
                int contentLength = httpURLConnection.getContentLength();
                if (contentLength <= 0) {
                    b.a(c, "download soft: filesize Error! filesize= " + contentLength);
                    return Boolean.FALSE;
                }
                b.a(c, "download soft: filesize = " + contentLength);
                InputStream inputStream = httpURLConnection.getInputStream();
                RandomAccessFile randomAccessFile = new RandomAccessFile(this.d, "rw");
                randomAccessFile.setLength((long) contentLength);
                randomAccessFile.seek(0);
                byte[] bArr = new byte[10240];
                while (true) {
                    int read = inputStream.read(bArr, 0, 10240);
                    if (read <= 0) {
                        randomAccessFile.close();
                        httpURLConnection.disconnect();
                        return Boolean.TRUE;
                    }
                    randomAccessFile.write(bArr, 0, read);
                    i += read;
                    publishProgress(Integer.valueOf((i * 100) / contentLength));
                }
            } else {
                b.a(c, "download soft: filesize Error! response code = " + httpURLConnection.getResponseCode());
                return Boolean.FALSE;
            }
        } catch (MalformedURLException e2) {
            e2.printStackTrace();
            return Boolean.FALSE;
        } catch (IOException e3) {
            e3.printStackTrace();
            return Boolean.FALSE;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(Boolean bool) {
        if (!bool.booleanValue()) {
            if (this.f1870a.isShowing()) {
                this.f1870a.dismiss();
            }
            Toast.makeText(this.b, "抱歉，下载安装包失败，请稍后再试。", 1).show();
            return;
        }
        if (this.f1870a.isShowing()) {
            this.f1870a.dismiss();
        }
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setDataAndType(Uri.fromFile(new File(this.d)), "application/vnd.android.package-archive");
        this.b.startActivity(intent);
        this.f1870a = null;
        this.e = null;
        this.b = null;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onProgressUpdate(Integer... numArr) {
        this.e.setText("正在下载安装包...(" + numArr[0] + "%)");
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        this.e = (TextView) this.f1870a.findViewById(f.a(this.b.getPackageName(), "id", "install_wallpaper_app_text"));
    }
}
