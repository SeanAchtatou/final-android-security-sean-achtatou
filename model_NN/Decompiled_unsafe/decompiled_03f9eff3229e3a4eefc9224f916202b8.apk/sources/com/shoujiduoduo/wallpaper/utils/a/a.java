package com.shoujiduoduo.wallpaper.utils.a;

import android.content.Context;
import com.shoujiduoduo.wallpaper.kernel.b;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private static final String f1830a = a.class.getSimpleName();

    public static b a(Context context) {
        int i = 0;
        try {
            Class[] clsArr = {Class.forName("com.shoujiduoduo.wallpaper.utils.a.d"), Class.forName("com.shoujiduoduo.wallpaper.utils.a.e"), Class.forName("com.shoujiduoduo.wallpaper.utils.a.c"), Class.forName("com.shoujiduoduo.wallpaper.utils.a.f")};
            while (true) {
                int i2 = i;
                if (i2 >= clsArr.length) {
                    return null;
                }
                b.a(f1830a, "i = " + i2);
                try {
                    b bVar = (b) clsArr[i2].newInstance();
                    bVar.a(context, "");
                    if (bVar.a()) {
                        return bVar;
                    }
                    i = i2 + 1;
                } catch (SecurityException e) {
                    b.a(f1830a, "security exception");
                } catch (NoSuchMethodException e2) {
                    b.a(f1830a, "nosuchmethod exception.");
                }
            }
        } catch (ClassNotFoundException e3) {
            e3.printStackTrace();
            b.a(f1830a, "ClassNotFoundException");
            return null;
        } catch (InstantiationException e4) {
            e4.printStackTrace();
            b.a(f1830a, "InstantiationException");
            return null;
        } catch (IllegalAccessException e5) {
            e5.printStackTrace();
            b.a(f1830a, "IllegalAccessException");
            return null;
        }
    }
}
