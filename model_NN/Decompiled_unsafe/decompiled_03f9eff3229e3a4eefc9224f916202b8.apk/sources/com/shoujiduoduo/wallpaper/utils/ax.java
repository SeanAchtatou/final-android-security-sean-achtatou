package com.shoujiduoduo.wallpaper.utils;

import android.content.Intent;
import android.view.View;
import android.widget.Toast;
import com.shoujiduoduo.wallpaper.a.k;
import com.shoujiduoduo.wallpaper.a.m;
import com.shoujiduoduo.wallpaper.activity.WallpaperActivity;

final class ax implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ c f1849a;

    ax(c cVar) {
        this.f1849a = cVar;
    }

    public final void onClick(View view) {
        if (view != null) {
            String str = (String) view.getTag();
            if (str.startsWith("album_view_")) {
                int intValue = Integer.valueOf(str.substring(11)).intValue();
                k a2 = m.b().a(intValue + 800000000);
                if (a2.a() == 0) {
                    a2.d();
                }
                Intent intent = new Intent(this.f1849a.c, WallpaperActivity.class);
                Toast.makeText(this.f1849a.c, "id = " + intValue, 0).show();
                intent.putExtra("listid", intValue + 800000000);
                intent.putExtra("serialno", 0);
                this.f1849a.c.startActivity(intent);
            }
        }
    }
}
