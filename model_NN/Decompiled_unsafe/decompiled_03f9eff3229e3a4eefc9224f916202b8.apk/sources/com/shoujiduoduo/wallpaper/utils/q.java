package com.shoujiduoduo.wallpaper.utils;

import com.shoujiduoduo.wallpaper.a.l;
import com.shoujiduoduo.wallpaper.kernel.b;
import com.shoujiduoduo.wallpaper.kernel.e;
import java.net.URLEncoder;
import java.util.HashSet;
import org.json.JSONException;
import org.json.JSONObject;

public class q {

    /* renamed from: a  reason: collision with root package name */
    public static HashSet f1876a = null;
    private static final String b = q.class.getSimpleName();
    private static final String c = f.b();
    private static final String d = f.j();
    private static final String e = f.i();
    private static final String f = f.c().replaceAll(":", "");
    private static final String g = URLEncoder.encode(f.h());

    static {
        f.l();
    }

    public static void a(int i, int i2, int i3) {
        new bm("id=" + i + "&listid=" + i3 + "&cate=" + i2 + "&user=" + c + "&prod=" + d + "&isrc=" + e + "&type=logset&mac=" + f + "&dev=" + URLEncoder.encode(g) + "&vc=" + f.k()).start();
    }

    public static void a(String str) {
        new bo("keyword=" + URLEncoder.encode(str) + "&user=" + c + "&prod=" + d + "&isrc=" + e + "&type=logbdsearch&mac=" + f + "&dev=" + URLEncoder.encode(g) + "&vc=" + f.k()).start();
    }

    public static void a(String str, String str2, String str3) {
        String str4 = "keyword=" + URLEncoder.encode(str) + "&user=" + c + "&prod=" + d + "&isrc=" + e + "&type=logbdresult&mac=" + f + "&dev=" + URLEncoder.encode(g) + "&vc=" + f.k();
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("page_url", str2);
            jSONObject.put("pic_url", str3);
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
        new bp(str4, jSONObject).start();
    }

    public static byte[] a() {
        String str = "user=" + c + "&prod=" + d + "&isrc=" + e + "&type=getcate&mac=" + f + "&dev=" + URLEncoder.encode(g) + "&vc=" + f.k();
        b.a(b, "paraString in plainText: " + str);
        return e.b("http://360web.shoujiduoduo.com/wallpaper/wplist.php?" + str, 20000, 20000);
    }

    public static byte[] a(int i, l lVar, int i2, int i3) {
        String str = "user=" + c + "&prod=" + d + "&isrc=" + e + "&type=getlist&listid=" + i + "&st=" + lVar.toString() + "&pg=" + String.valueOf(i2) + "&pc=" + i3 + "&mac=" + f + "&dev=" + URLEncoder.encode(g) + "&vc=" + f.k();
        b.a(b, "httpGetRingList: paraString = " + str);
        byte[] b2 = e.b("http://360web.shoujiduoduo.com/wallpaper/wplist.php?" + str, 20000, 20000);
        b.a(b, "list url: http://360web.shoujiduoduo.com/wallpaper/wplist.php?" + str);
        return b2;
    }

    public static byte[] a(String str, int i, int i2, String str2) {
        String str3 = "user=" + c + "&prod=" + d + "&isrc=" + e + "&type=search&keyword=" + URLEncoder.encode(str) + "&src=" + str2 + "&pg=" + String.valueOf(i) + "&pc=" + i2 + "&mac=" + f + "&dev=" + URLEncoder.encode(g) + "&vc=" + f.k();
        if (f.m()) {
            str3 = String.valueOf(str3) + "&hidebeauty=1";
        }
        b.a(b, "httpSearchWallpaper: paraString = " + str3);
        byte[] b2 = e.b("http://360web.shoujiduoduo.com/wallpaper/wplist.php?" + str3, 20000, 20000);
        b.a(b, "list url: http://360web.shoujiduoduo.com/wallpaper/wplist.php?" + str3);
        return b2;
    }

    public static void b(int i, int i2, int i3) {
        new bn("id=" + i + "&listid=" + i3 + "&cate=" + i2 + "&user=" + c + "&prod=" + d + "&isrc=" + e + "&type=logshare&mac=" + f + "&dev=" + URLEncoder.encode(g) + "&vc=" + f.k()).start();
    }

    public static byte[] b() {
        String str = "user=" + c + "&prod=" + d + "&isrc=" + e + "&type=gethotkeyword&mac=" + f + "&dev=" + URLEncoder.encode(g) + "&vc=" + f.k();
        b.a(b, "paraString in plainText: " + str);
        return e.b("http://360web.shoujiduoduo.com/wallpaper/wplist.php?" + str, 20000, 20000);
    }

    public static void c(int i, int i2, int i3) {
        if (f1876a == null) {
            f1876a = new HashSet();
        }
        String str = "id=" + i + "&listid=" + i3 + "&cate=" + i2 + "&user=" + c + "&prod=" + d + "&isrc=" + e + "&type=logdownload&mac=" + f + "&dev=" + URLEncoder.encode(g) + "&vc=" + f.k();
        if (!f1876a.contains(Integer.valueOf(i))) {
            f1876a.add(Integer.valueOf(i));
            b.a(b, "log download->httpLogDownload. id = " + i);
            new bq(str).start();
        }
    }
}
