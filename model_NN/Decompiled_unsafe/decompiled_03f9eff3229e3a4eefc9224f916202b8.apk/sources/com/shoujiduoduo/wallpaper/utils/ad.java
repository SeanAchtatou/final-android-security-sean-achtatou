package com.shoujiduoduo.wallpaper.utils;

import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.widget.ImageView;
import com.shoujiduoduo.wallpaper.kernel.b;

public class ad extends Drawable {
    private static /* synthetic */ int[] p;

    /* renamed from: a  reason: collision with root package name */
    private final RectF f1835a = new RectF();
    private final RectF b = new RectF();
    private final RectF c = new RectF();
    private final BitmapShader d;
    private final Paint e;
    private final int f;
    private final int g;
    private final RectF h = new RectF();
    private final Paint i;
    private final Matrix j = new Matrix();
    private float k = 0.0f;
    private boolean l = false;
    private float m = 0.0f;
    private ColorStateList n = ColorStateList.valueOf(ViewCompat.MEASURED_STATE_MASK);
    private ImageView.ScaleType o = ImageView.ScaleType.FIT_CENTER;

    public ad(Bitmap bitmap) {
        this.f = bitmap.getWidth();
        this.g = bitmap.getHeight();
        this.c.set(0.0f, 0.0f, (float) this.f, (float) this.g);
        this.d = new BitmapShader(bitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
        this.d.setLocalMatrix(this.j);
        this.e = new Paint();
        this.e.setStyle(Paint.Style.FILL);
        this.e.setAntiAlias(true);
        this.e.setShader(this.d);
        this.i = new Paint();
        this.i.setStyle(Paint.Style.STROKE);
        this.i.setAntiAlias(true);
        this.i.setColor(this.n.getColorForState(getState(), ViewCompat.MEASURED_STATE_MASK));
        this.i.setStrokeWidth(this.m);
    }

    public static Drawable a(Drawable drawable) {
        if (drawable == null || (drawable instanceof ad)) {
            return drawable;
        }
        if (drawable instanceof LayerDrawable) {
            LayerDrawable layerDrawable = (LayerDrawable) drawable;
            int numberOfLayers = layerDrawable.getNumberOfLayers();
            for (int i2 = 0; i2 < numberOfLayers; i2++) {
                layerDrawable.setDrawableByLayerId(layerDrawable.getId(i2), a(layerDrawable.getDrawable(i2)));
            }
            return layerDrawable;
        }
        Bitmap b2 = b(drawable);
        if (b2 != null) {
            return new ad(b2);
        }
        Log.w("RoundedDrawable", "Failed to create bitmap from drawable!");
        return drawable;
    }

    public static ad a(Bitmap bitmap) {
        if (bitmap != null) {
            return new ad(bitmap);
        }
        return null;
    }

    private void a() {
        float width;
        float f2;
        float f3 = 0.0f;
        switch (b()[this.o.ordinal()]) {
            case 1:
                this.h.set(this.f1835a);
                this.h.inset(this.m / 2.0f, this.m / 2.0f);
                this.j.set(null);
                this.j.setTranslate((float) ((int) (((this.h.width() - ((float) this.f)) * 0.5f) + 0.5f)), (float) ((int) (((this.h.height() - ((float) this.g)) * 0.5f) + 0.5f)));
                break;
            case 2:
                this.h.set(this.f1835a);
                this.h.inset(this.m / 2.0f, this.m / 2.0f);
                this.j.set(null);
                if (((float) this.f) * this.h.height() > this.h.width() * ((float) this.g)) {
                    width = this.h.height() / ((float) this.g);
                    f2 = (this.h.width() - (((float) this.f) * width)) * 0.5f;
                } else {
                    width = this.h.width() / ((float) this.f);
                    f2 = 0.0f;
                    f3 = (this.h.height() - (((float) this.g) * width)) * 0.5f;
                }
                this.j.setScale(width, width);
                this.j.postTranslate(((float) ((int) (f2 + 0.5f))) + this.m, ((float) ((int) (f3 + 0.5f))) + this.m);
                break;
            case 3:
                this.j.set(null);
                float min = (((float) this.f) > this.f1835a.width() || ((float) this.g) > this.f1835a.height()) ? Math.min(this.f1835a.width() / ((float) this.f), this.f1835a.height() / ((float) this.g)) : 1.0f;
                this.j.setScale(min, min);
                this.j.postTranslate((float) ((int) (((this.f1835a.width() - (((float) this.f) * min)) * 0.5f) + 0.5f)), (float) ((int) (((this.f1835a.height() - (((float) this.g) * min)) * 0.5f) + 0.5f)));
                this.h.set(this.c);
                this.j.mapRect(this.h);
                this.h.inset(this.m / 2.0f, this.m / 2.0f);
                this.j.setRectToRect(this.c, this.h, Matrix.ScaleToFit.FILL);
                break;
            case 4:
            default:
                this.h.set(this.c);
                this.j.setRectToRect(this.c, this.f1835a, Matrix.ScaleToFit.CENTER);
                this.j.mapRect(this.h);
                this.h.inset(this.m / 2.0f, this.m / 2.0f);
                this.j.setRectToRect(this.c, this.h, Matrix.ScaleToFit.FILL);
                break;
            case 5:
                this.h.set(this.c);
                this.j.setRectToRect(this.c, this.f1835a, Matrix.ScaleToFit.END);
                this.j.mapRect(this.h);
                this.h.inset(this.m / 2.0f, this.m / 2.0f);
                this.j.setRectToRect(this.c, this.h, Matrix.ScaleToFit.FILL);
                break;
            case 6:
                this.h.set(this.c);
                this.j.setRectToRect(this.c, this.f1835a, Matrix.ScaleToFit.START);
                this.j.mapRect(this.h);
                this.h.inset(this.m / 2.0f, this.m / 2.0f);
                this.j.setRectToRect(this.c, this.h, Matrix.ScaleToFit.FILL);
                break;
            case 7:
                this.h.set(this.f1835a);
                this.h.inset(this.m / 2.0f, this.m / 2.0f);
                this.j.set(null);
                this.j.setRectToRect(this.c, this.h, Matrix.ScaleToFit.FILL);
                break;
        }
        this.b.set(this.h);
        this.d.setLocalMatrix(this.j);
    }

    public static Bitmap b(Drawable drawable) {
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        }
        try {
            Bitmap createBitmap = Bitmap.createBitmap(Math.max(drawable.getIntrinsicWidth(), 1), Math.max(drawable.getIntrinsicHeight(), 1), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(createBitmap);
            drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            drawable.draw(canvas);
            return createBitmap;
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    private static /* synthetic */ int[] b() {
        int[] iArr = p;
        if (iArr == null) {
            iArr = new int[ImageView.ScaleType.values().length];
            try {
                iArr[ImageView.ScaleType.CENTER.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[ImageView.ScaleType.CENTER_CROP.ordinal()] = 2;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[ImageView.ScaleType.CENTER_INSIDE.ordinal()] = 3;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[ImageView.ScaleType.FIT_CENTER.ordinal()] = 4;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[ImageView.ScaleType.FIT_END.ordinal()] = 5;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[ImageView.ScaleType.FIT_START.ordinal()] = 6;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[ImageView.ScaleType.FIT_XY.ordinal()] = 7;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[ImageView.ScaleType.MATRIX.ordinal()] = 8;
            } catch (NoSuchFieldError e9) {
            }
            p = iArr;
        }
        return iArr;
    }

    public ad a(float f2) {
        this.k = f2;
        return this;
    }

    public ad a(int i2) {
        this.m = (float) i2;
        this.i.setStrokeWidth(this.m);
        return this;
    }

    public ad a(ColorStateList colorStateList) {
        if (colorStateList == null) {
            colorStateList = ColorStateList.valueOf(0);
        }
        this.n = colorStateList;
        this.i.setColor(this.n.getColorForState(getState(), ViewCompat.MEASURED_STATE_MASK));
        return this;
    }

    public ad a(ImageView.ScaleType scaleType) {
        if (scaleType == null) {
            scaleType = ImageView.ScaleType.FIT_CENTER;
        }
        if (this.o != scaleType) {
            this.o = scaleType;
            a();
        }
        return this;
    }

    public ad a(boolean z) {
        this.l = z;
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    public void draw(Canvas canvas) {
        if (this.l) {
            if (this.m > 0.0f) {
                canvas.drawOval(this.b, this.e);
                canvas.drawOval(this.h, this.i);
                return;
            }
            canvas.drawOval(this.b, this.e);
        } else if (this.m > 0.0f) {
            canvas.drawRoundRect(this.b, Math.max(this.k, 0.0f), Math.max(this.k, 0.0f), this.e);
            canvas.drawRoundRect(this.h, this.k, this.k, this.i);
        } else {
            b.a("round", "drawable_rect.bottom = " + this.b.bottom + ",top = " + this.b.top + ", left = " + this.b.left + ", right = " + this.b.right);
            b.a("round", "corner radius = " + this.k);
            canvas.drawRoundRect(this.b, this.k, this.k, this.e);
        }
    }

    public int getIntrinsicHeight() {
        return this.g;
    }

    public int getIntrinsicWidth() {
        return this.f;
    }

    public int getOpacity() {
        return -3;
    }

    public boolean isStateful() {
        return this.n.isStateful();
    }

    /* access modifiers changed from: protected */
    public void onBoundsChange(Rect rect) {
        super.onBoundsChange(rect);
        this.f1835a.set(rect);
        a();
    }

    /* access modifiers changed from: protected */
    public boolean onStateChange(int[] iArr) {
        int colorForState = this.n.getColorForState(iArr, 0);
        if (this.i.getColor() == colorForState) {
            return super.onStateChange(iArr);
        }
        this.i.setColor(colorForState);
        return true;
    }

    public void setAlpha(int i2) {
        this.e.setAlpha(i2);
        invalidateSelf();
    }

    public void setColorFilter(ColorFilter colorFilter) {
        this.e.setColorFilter(colorFilter);
        invalidateSelf();
    }

    public void setDither(boolean z) {
        this.e.setDither(z);
        invalidateSelf();
    }

    public void setFilterBitmap(boolean z) {
        this.e.setFilterBitmap(z);
        invalidateSelf();
    }
}
