package com.shoujiduoduo.wallpaper.utils.a;

import android.app.WallpaperManager;
import android.content.Context;
import android.os.Build;
import android.widget.Toast;
import com.shoujiduoduo.wallpaper.utils.f;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class c extends b {
    private WallpaperManager c = null;
    private Method d = null;

    public void a(Context context, String str) {
        super.a(context, "meizu");
        this.c = WallpaperManager.getInstance(context);
        this.d = WallpaperManager.class.getMethod("setStreamToLockWallpaper", InputStream.class);
    }

    public boolean a() {
        try {
            return "meizu".equalsIgnoreCase(Build.BRAND);
        } catch (Exception e) {
            return false;
        }
    }

    public boolean a(String str) {
        try {
            FileInputStream fileInputStream = new FileInputStream(str);
            Object[] objArr = {fileInputStream};
            if (!(this.d == null || this.c == null)) {
                this.d.invoke(this.c, objArr);
            }
            fileInputStream.close();
            Toast.makeText(this.b, this.b.getString(f.c("R.string.wallpaperdd_toast_on_lockscreen_success")), 0).show();
            return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Toast.makeText(this.b, this.b.getString(f.c("R.string.wallpaperdd_toast_on_lockscreen_failed")), 0).show();
            return false;
        } catch (IllegalAccessException e2) {
            e2.printStackTrace();
            Toast.makeText(this.b, this.b.getString(f.c("R.string.wallpaperdd_toast_on_lockscreen_failed")), 0).show();
            return false;
        } catch (IllegalArgumentException e3) {
            e3.printStackTrace();
            Toast.makeText(this.b, this.b.getString(f.c("R.string.wallpaperdd_toast_on_lockscreen_failed")), 0).show();
            return false;
        } catch (InvocationTargetException e4) {
            e4.printStackTrace();
            Toast.makeText(this.b, this.b.getString(f.c("R.string.wallpaperdd_toast_on_lockscreen_failed")), 0).show();
            return false;
        } catch (IOException e5) {
            e5.printStackTrace();
            Toast.makeText(this.b, this.b.getString(f.c("R.string.wallpaperdd_toast_on_lockscreen_failed")), 0).show();
            return false;
        }
    }
}
