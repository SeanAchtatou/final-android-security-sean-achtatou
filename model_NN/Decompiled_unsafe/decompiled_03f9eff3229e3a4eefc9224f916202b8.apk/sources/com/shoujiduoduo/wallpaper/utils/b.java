package com.shoujiduoduo.wallpaper.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.a.a;
import com.baidu.mobad.feeds.NativeResponse;
import com.qq.e.ads.nativ.NativeADDataRef;

public class b {
    public static View a(Context context, View view) {
        NativeResponse c = at.a(context).c();
        if (c != null) {
            if (view == null || !"baidu_ad_view".equalsIgnoreCase((String) view.getTag())) {
                view = LayoutInflater.from(context).inflate(f.c("R.layout.gdt_nativelistitem"), (ViewGroup) null);
                view.setTag("baidu_ad_view");
            }
            view.setOnClickListener(new av(c));
            a aVar = new a(view);
            ((a) aVar.a(f.c("R.id.gdt_ad_image"))).a(c.getImageUrl(), false, true);
            ((a) aVar.a(f.c("R.id.gdt_ad_description"))).a(c.getDesc());
            c.recordImpression(view);
            if (context != null) {
                com.umeng.analytics.b.b(context, "EVENT_GET_ADVIEW");
            }
        } else {
            if (view == null || !"empty_ad_view".equalsIgnoreCase((String) view.getTag())) {
                view = LayoutInflater.from(context).inflate(f.c("R.layout.wallpaperdd_empty_ad_view"), (ViewGroup) null);
                view.setTag("empty_ad_view");
            }
            if (context != null) {
                com.umeng.analytics.b.b(context, "EVENT_GET_EMPTY_ADVIEW");
            }
        }
        return view;
    }

    public static View a(Context context, View view, int i) {
        int d = az.a(context).d();
        int i2 = d > 0 ? i % d : 0;
        NativeADDataRef a2 = az.a(context).a(i2);
        if (a2 != null) {
            if (view == null || !"gdt_ad_view".equalsIgnoreCase((String) view.getTag())) {
                view = LayoutInflater.from(context).inflate(f.c("R.layout.gdt_nativelistitem"), (ViewGroup) null);
                view.setTag("gdt_ad_view");
            }
            view.setOnClickListener(new ap(context, a2));
            a aVar = new a(view);
            ((a) aVar.a(f.c("R.id.gdt_ad_image"))).a(a2.getImgUrl(), false, true);
            ((a) aVar.a(f.c("R.id.gdt_ad_description"))).a(a2.getDesc());
            a2.onExposured(view);
            if (i2 < d - 1) {
                return view;
            }
            az.a(context).c();
            return view;
        } else if (view != null && "empty_ad_view".equalsIgnoreCase((String) view.getTag())) {
            return view;
        } else {
            View inflate = LayoutInflater.from(context).inflate(f.c("R.layout.wallpaperdd_empty_ad_view"), (ViewGroup) null);
            inflate.setTag("empty_ad_view");
            return inflate;
        }
    }
}
