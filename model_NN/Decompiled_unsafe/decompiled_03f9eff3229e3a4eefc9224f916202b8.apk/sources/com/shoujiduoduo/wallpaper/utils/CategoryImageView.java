package com.shoujiduoduo.wallpaper.utils;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

public class CategoryImageView extends ImageView {
    public CategoryImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public CategoryImageView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
    }
}
