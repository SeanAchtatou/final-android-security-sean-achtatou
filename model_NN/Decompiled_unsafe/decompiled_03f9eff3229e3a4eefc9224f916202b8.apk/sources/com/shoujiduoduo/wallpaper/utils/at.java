package com.shoujiduoduo.wallpaper.utils;

import android.content.Context;
import com.baidu.mobad.feeds.BaiduNative;
import com.baidu.mobad.feeds.NativeResponse;
import com.baidu.mobad.feeds.RequestParameters;
import com.umeng.analytics.b;
import java.util.List;

public class at {
    /* access modifiers changed from: private */
    public static final String b = at.class.getSimpleName();
    private static at d = null;

    /* renamed from: a  reason: collision with root package name */
    List f1847a = null;
    /* access modifiers changed from: private */
    public Context c;
    private List e = null;
    /* access modifiers changed from: private */
    public boolean f = false;
    private String g;
    private int h = 0;

    private at() {
    }

    private at(Context context) {
        this.c = context;
    }

    public static at a(Context context) {
        if (d == null) {
            d = new at(context);
        }
        return d;
    }

    private void e() {
        if (!this.f) {
            this.f = true;
            new BaiduNative(this.c, "2010924", new aq(this)).makeRequest(new RequestParameters.Builder().keywords(this.g).setAdsType(3).confirmDownloading(f.n()).build());
            if (this.c != null) {
                b.b(this.c, "EVENT_REQUEST_BAIDU_AD");
            }
        }
    }

    public void a() {
        this.c = null;
        d = null;
        this.e = null;
        this.f1847a = null;
    }

    public void b() {
        this.g = b.c(this.c, "baidu_ad_keywords");
        if (this.g == null || this.g.length() == 0) {
            this.g = "美女,动漫,游戏,影视,明星,情感,美图,性感";
        }
        e();
    }

    public NativeResponse c() {
        NativeResponse nativeResponse = null;
        if (this.e == null || this.h == this.e.size()) {
            this.e = this.f1847a;
            this.h = 0;
            this.f1847a = null;
        }
        if (this.e == null || this.e.size() <= this.h) {
            com.shoujiduoduo.wallpaper.kernel.b.a(b, "requestAD because of NULL current list");
            e();
        } else {
            nativeResponse = (NativeResponse) this.e.get(this.h);
            com.shoujiduoduo.wallpaper.kernel.b.a(b, "get ad NO. " + this.h + ", title = " + nativeResponse.getTitle());
            if (this.h + 3 >= this.e.size() && this.f1847a == null) {
                e();
            }
            this.h++;
        }
        return nativeResponse;
    }
}
