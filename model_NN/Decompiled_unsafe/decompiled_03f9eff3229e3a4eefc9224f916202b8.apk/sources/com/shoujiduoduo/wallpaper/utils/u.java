package com.shoujiduoduo.wallpaper.utils;

import android.os.Build;
import android.view.ViewTreeObserver;

final class u implements ViewTreeObserver.OnGlobalLayoutListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ PagerSlidingTabStrip f1879a;

    u(PagerSlidingTabStrip pagerSlidingTabStrip) {
        this.f1879a = pagerSlidingTabStrip;
    }

    public final void onGlobalLayout() {
        if (Build.VERSION.SDK_INT < 16) {
            this.f1879a.getViewTreeObserver().removeGlobalOnLayoutListener(this);
        } else {
            this.f1879a.getViewTreeObserver().removeOnGlobalLayoutListener(this);
        }
        this.f1879a.h = this.f1879a.f.getCurrentItem();
        PagerSlidingTabStrip.a(this.f1879a, this.f1879a.h, 0);
    }
}
