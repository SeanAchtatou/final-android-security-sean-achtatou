package com.shoujiduoduo.wallpaper.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import com.shoujiduoduo.wallpaper.kernel.b;

public class SwitchButton extends View implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private static final String f1829a = SwitchButton.class.getSimpleName();
    private Bitmap b;
    private Bitmap c;
    private Bitmap d;
    private Bitmap e;
    private float f;
    private boolean g;
    private int h;
    private float i;
    private Rect j;
    private Rect k;
    private int l;
    private Paint m;
    private ak n;

    public SwitchButton(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public SwitchButton(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.f = 0.0f;
        this.g = true;
        this.i = 0.0f;
        this.j = null;
        this.k = null;
        this.l = 0;
        this.m = null;
        this.n = null;
    }

    public void onClick(View view) {
        b.a(f1829a, "onClick");
        this.l = this.g ? this.h : -this.h;
        this.g = !this.g;
        if (this.n != null) {
            this.n.a(this, this.g);
        }
        invalidate();
        this.l = 0;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.l > 0 || (this.l == 0 && this.g)) {
            if (this.k != null) {
                this.k.set(this.h - this.l, 0, this.b.getWidth() - this.l, this.d.getHeight());
            }
        } else if ((this.l < 0 || (this.l == 0 && !this.g)) && this.k != null) {
            this.k.set(-this.l, 0, this.d.getWidth() - this.l, this.d.getHeight());
        }
        int saveLayer = canvas.saveLayer(new RectF(this.j), null, 31);
        canvas.drawBitmap(this.b, this.k, this.j, (Paint) null);
        canvas.drawBitmap(this.c, this.k, this.j, (Paint) null);
        canvas.drawBitmap(this.d, 0.0f, 0.0f, (Paint) null);
        canvas.drawBitmap(this.e, 0.0f, 0.0f, this.m);
        canvas.restoreToCount(saveLayer);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        setMeasuredDimension(this.d.getWidth(), this.d.getHeight());
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public boolean onTouchEvent(MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                this.i = motionEvent.getX();
                b.a(f1829a, "ACTION_DOWN, mLastX = " + this.i);
                break;
            case 1:
            case 3:
                b.a(f1829a, "ACTION_UP, mDeltX = " + this.l);
                if (Math.abs(this.l) >= 0 && Math.abs(this.l) < 6) {
                    b.a(f1829a, "move too little, click");
                    onClick(this);
                    return true;
                } else if (Math.abs(this.l) >= 6 && Math.abs(this.l) <= this.h / 2) {
                    b.a(f1829a, "move shorter than half, don't change.");
                    this.l = 0;
                    invalidate();
                    return true;
                } else if (Math.abs(this.l) <= this.h / 2 || Math.abs(this.l) > this.h) {
                    b.a(f1829a, "Strange things happened! don't move at all.");
                    return super.onTouchEvent(motionEvent);
                } else {
                    b.a(f1829a, "move longer than half, change.");
                    this.l = this.l > 0 ? this.h : -this.h;
                    this.g = !this.g;
                    if (this.n != null) {
                        this.n.a(this, this.g);
                    }
                    invalidate();
                    this.l = 0;
                    return true;
                }
            case 2:
                this.f = motionEvent.getX();
                this.l = (int) (this.f - this.i);
                b.a(f1829a, "ACTION_MOVE, mDeltX = " + this.l);
                if (Math.abs(this.l) > this.h) {
                    this.l = this.l > 0 ? this.h : -this.h;
                }
                invalidate();
                return true;
        }
        invalidate();
        return super.onTouchEvent(motionEvent);
    }

    public void setOnChangeListener(ak akVar) {
        this.n = akVar;
    }
}
