package com.shoujiduoduo.wallpaper.utils;

import android.view.MotionEvent;
import android.view.View;
import com.shoujiduoduo.wallpaper.kernel.b;

final class bl implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    private boolean f1859a = false;
    private int b;
    private /* synthetic */ HorizontalSlider c;

    bl(HorizontalSlider horizontalSlider) {
        this.c = horizontalSlider;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        int i = 0;
        if (motionEvent.getAction() == 0) {
            this.f1859a = true;
            this.b = (int) motionEvent.getRawX();
            this.c.d.setImageDrawable(this.c.c.getResources().getDrawable(f.a(this.c.c.getPackageName(), "drawable", "wallpaperdd_horizontal_slider_pressed")));
            if (this.c.f1823a != null) {
                this.c.f1823a.m();
            }
            return true;
        } else if (motionEvent.getAction() == 1) {
            b.a(HorizontalSlider.b, "bar up");
            this.f1859a = false;
            if (this.c.f1823a != null) {
                this.c.f1823a.n();
            }
            return true;
        } else if (motionEvent.getAction() == 2) {
            if (!this.f1859a) {
                return true;
            }
            int rawX = (int) motionEvent.getRawX();
            if (rawX != this.b) {
                int left = this.c.d.getLeft() + (rawX - this.b);
                if (left > this.c.g - this.c.e) {
                    left = this.c.g - this.c.e;
                }
                if (left >= 0) {
                    i = left;
                }
                b.a(HorizontalSlider.b, "bar moving. newX = " + i);
                this.c.i = i;
                this.c.d.layout(i, this.c.d.getTop(), this.c.d.getWidth() + i, this.c.d.getBottom());
                this.b = rawX;
                if (this.c.f1823a != null) {
                    this.c.f1823a.a(i, this.c.d.getWidth(), this.c.g);
                }
            }
            return true;
        } else if (motionEvent.getAction() != 8) {
            return false;
        } else {
            b.a(HorizontalSlider.b, "scroll.");
            return false;
        }
    }
}
