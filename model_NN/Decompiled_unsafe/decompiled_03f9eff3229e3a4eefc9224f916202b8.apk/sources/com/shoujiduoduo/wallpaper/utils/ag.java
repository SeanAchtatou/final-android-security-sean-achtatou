package com.shoujiduoduo.wallpaper.utils;

import android.content.Context;
import android.content.SharedPreferences;
import com.shoujiduoduo.wallpaper.kernel.b;

public class ag {

    /* renamed from: a  reason: collision with root package name */
    private static final String f1837a = ag.class.getSimpleName();
    private static Context b = null;

    public static int a(Context context, String str, int i) {
        if (context == null) {
            context = b;
        }
        return context.getSharedPreferences("wallpaper.shoujiduoduo.com", 4).getInt(str, i);
    }

    public static long a(Context context, String str, long j) {
        if (context == null) {
            context = b;
        }
        return context.getSharedPreferences("wallpaper.shoujiduoduo.com", 4).getLong(str, j);
    }

    public static void a(Context context) {
        b.a(f1837a, "setContext, ct = " + context.toString());
        b = context;
    }

    public static boolean b(Context context, String str, int i) {
        if (context == null) {
            context = b;
        }
        SharedPreferences.Editor edit = context.getSharedPreferences("wallpaper.shoujiduoduo.com", 4).edit();
        edit.putInt(str, i);
        return edit.commit();
    }

    public static boolean b(Context context, String str, long j) {
        if (context == null) {
            context = b;
        }
        SharedPreferences.Editor edit = context.getSharedPreferences("wallpaper.shoujiduoduo.com", 4).edit();
        edit.putLong(str, j);
        return edit.commit();
    }
}
