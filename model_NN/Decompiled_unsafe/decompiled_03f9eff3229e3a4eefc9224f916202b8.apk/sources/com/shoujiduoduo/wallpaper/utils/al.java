package com.shoujiduoduo.wallpaper.utils;

import android.os.Handler;
import android.os.Message;
import android.widget.RelativeLayout;
import com.shoujiduoduo.wallpaper.kernel.b;

final class al extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ah f1841a;

    al(ah ahVar) {
        this.f1841a = ahVar;
    }

    public final void handleMessage(Message message) {
        if (this.f1841a.d != null) {
            switch (message.what) {
                case 3577:
                    int i = message.arg1;
                    if (i == 31) {
                        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
                        layoutParams.addRule(14);
                        layoutParams.addRule(12);
                        this.f1841a.f.setLayoutParams(layoutParams);
                        this.f1841a.f.setPadding(0, 0, 0, 30);
                        this.f1841a.f.setVisibility(0);
                        return;
                    } else if (i == 0 || i == 32) {
                        b.a(ah.i, "list update success.");
                        if (this.f1841a.f.getVisibility() == 0) {
                            b.a(ah.i, "show grid view.");
                            this.f1841a.f.setVisibility(4);
                            this.f1841a.c.setVisibility(0);
                            this.f1841a.e.notifyDataSetChanged();
                            return;
                        }
                        return;
                    } else if (i == 1 && this.f1841a.f.getVisibility() == 0) {
                        this.f1841a.f.setVisibility(4);
                        this.f1841a.c.setVisibility(4);
                        return;
                    } else {
                        return;
                    }
                default:
                    return;
            }
        }
    }
}
