package com.shoujiduoduo.wallpaper.utils;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ImageView;

public class RoundedImageView extends ImageView {

    /* renamed from: a  reason: collision with root package name */
    private static final ImageView.ScaleType[] f1828a = {ImageView.ScaleType.MATRIX, ImageView.ScaleType.FIT_XY, ImageView.ScaleType.FIT_START, ImageView.ScaleType.FIT_CENTER, ImageView.ScaleType.FIT_END, ImageView.ScaleType.CENTER, ImageView.ScaleType.CENTER_CROP, ImageView.ScaleType.CENTER_INSIDE};
    private static /* synthetic */ int[] k;
    private int b;
    private int c;
    private ColorStateList d;
    private boolean e;
    private boolean f;
    private int g;
    private Drawable h;
    private Drawable i;
    private ImageView.ScaleType j;

    public RoundedImageView(Context context) {
        super(context);
        this.b = 0;
        this.c = 0;
        this.d = ColorStateList.valueOf(ViewCompat.MEASURED_STATE_MASK);
        this.e = false;
        this.f = false;
    }

    public RoundedImageView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public RoundedImageView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.b = 0;
        this.c = 0;
        this.d = ColorStateList.valueOf(ViewCompat.MEASURED_STATE_MASK);
        this.e = false;
        this.f = false;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, f.a(context, "wallpaperdd_RoundedImageView"), i2, 0);
        int i3 = obtainStyledAttributes.getInt(f.a(context.getPackageName(), "styleable", "wallpaperdd_RoundedImageView_android_scaleType"), -1);
        if (i3 >= 0) {
            setScaleType(f1828a[i3]);
        } else {
            setScaleType(ImageView.ScaleType.FIT_CENTER);
        }
        this.b = obtainStyledAttributes.getDimensionPixelSize(f.a(context.getPackageName(), "styleable", "wallpaperdd_RoundedImageView_wallpaperdd_corner_radius"), -1);
        this.c = obtainStyledAttributes.getDimensionPixelSize(f.a(context.getPackageName(), "styleable", "wallpaperdd_RoundedImageView_wallpaperdd_border_width"), -1);
        if (this.b < 0) {
            this.b = 0;
        }
        if (this.c < 0) {
            this.c = 0;
        }
        this.d = obtainStyledAttributes.getColorStateList(f.a(context.getPackageName(), "styleable", "wallpaperdd_RoundedImageView_wallpaperdd_border_color"));
        if (this.d == null) {
            this.d = ColorStateList.valueOf(ViewCompat.MEASURED_STATE_MASK);
        }
        this.f = obtainStyledAttributes.getBoolean(f.a(context.getPackageName(), "styleable", "wallpaperdd_RoundedImageView_wallpaperdd_round_background"), false);
        this.e = obtainStyledAttributes.getBoolean(f.a(context.getPackageName(), "styleable", "wallpaperdd_RoundedImageView_wallpaperdd_is_oval"), false);
        b();
        c();
        obtainStyledAttributes.recycle();
    }

    private Drawable a() {
        Drawable drawable = null;
        Resources resources = getResources();
        if (resources == null) {
            return null;
        }
        if (this.g != 0) {
            try {
                drawable = resources.getDrawable(this.g);
            } catch (Exception e2) {
                Log.w("RoundedImageView", "Unable to find resource: " + this.g, e2);
                this.g = 0;
            }
        }
        return ad.a(drawable);
    }

    private void a(Drawable drawable, boolean z) {
        int i2 = 0;
        if (drawable != null) {
            if (drawable instanceof ad) {
                ad a2 = ((ad) drawable).a(this.j).a((float) ((!z || this.f) ? this.b : 0));
                if (!z || this.f) {
                    i2 = this.c;
                }
                a2.a(i2).a(this.d).a(this.e);
            } else if (drawable instanceof LayerDrawable) {
                LayerDrawable layerDrawable = (LayerDrawable) drawable;
                int numberOfLayers = layerDrawable.getNumberOfLayers();
                while (i2 < numberOfLayers) {
                    a(layerDrawable.getDrawable(i2), z);
                    i2++;
                }
            }
        }
    }

    private void b() {
        a(this.h, false);
    }

    private void c() {
        a(this.i, true);
    }

    private static /* synthetic */ int[] d() {
        int[] iArr = k;
        if (iArr == null) {
            iArr = new int[ImageView.ScaleType.values().length];
            try {
                iArr[ImageView.ScaleType.CENTER.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[ImageView.ScaleType.CENTER_CROP.ordinal()] = 2;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[ImageView.ScaleType.CENTER_INSIDE.ordinal()] = 3;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[ImageView.ScaleType.FIT_CENTER.ordinal()] = 4;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[ImageView.ScaleType.FIT_END.ordinal()] = 5;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[ImageView.ScaleType.FIT_START.ordinal()] = 6;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[ImageView.ScaleType.FIT_XY.ordinal()] = 7;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[ImageView.ScaleType.MATRIX.ordinal()] = 8;
            } catch (NoSuchFieldError e9) {
            }
            k = iArr;
        }
        return iArr;
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        invalidate();
    }

    public int getBorderColor() {
        return this.d.getDefaultColor();
    }

    public ColorStateList getBorderColors() {
        return this.d;
    }

    public int getBorderWidth() {
        return this.c;
    }

    public int getCornerRadius() {
        return this.b;
    }

    public ImageView.ScaleType getScaleType() {
        return this.j;
    }

    public void setBackground(Drawable drawable) {
        setBackgroundDrawable(drawable);
    }

    public void setBackgroundDrawable(Drawable drawable) {
        this.i = ad.a(drawable);
        c();
        super.setBackgroundDrawable(drawable);
    }

    public void setBorderColor(int i2) {
        setBorderColors(ColorStateList.valueOf(i2));
    }

    public void setBorderColors(ColorStateList colorStateList) {
        if (!this.d.equals(colorStateList)) {
            if (colorStateList == null) {
                colorStateList = ColorStateList.valueOf(ViewCompat.MEASURED_STATE_MASK);
            }
            this.d = colorStateList;
            b();
            c();
            if (this.c > 0) {
                invalidate();
            }
        }
    }

    public void setBorderWidth(int i2) {
        if (this.c != i2) {
            this.c = i2;
            b();
            c();
            invalidate();
        }
    }

    public void setCornerRadius(int i2) {
        if (this.b != i2) {
            this.b = i2;
            b();
            c();
        }
    }

    public void setImageBitmap(Bitmap bitmap) {
        this.g = 0;
        this.h = ad.a(bitmap);
        b();
        super.setImageDrawable(this.h);
    }

    public void setImageDrawable(Drawable drawable) {
        this.g = 0;
        this.h = ad.a(drawable);
        b();
        super.setImageDrawable(this.h);
    }

    public void setImageResource(int i2) {
        if (this.g != i2) {
            this.g = i2;
            this.h = a();
            b();
            super.setImageDrawable(this.h);
        }
    }

    public void setImageURI(Uri uri) {
        super.setImageURI(uri);
        setImageDrawable(getDrawable());
    }

    public void setOval(boolean z) {
        this.e = z;
        b();
        c();
        invalidate();
    }

    public void setRoundBackground(boolean z) {
        if (this.f != z) {
            this.f = z;
            c();
            invalidate();
        }
    }

    public void setScaleType(ImageView.ScaleType scaleType) {
        if (scaleType == null) {
            throw new NullPointerException();
        } else if (this.j != scaleType) {
            this.j = scaleType;
            switch (d()[scaleType.ordinal()]) {
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                    super.setScaleType(ImageView.ScaleType.FIT_XY);
                    break;
                default:
                    super.setScaleType(scaleType);
                    break;
            }
            b();
            c();
            invalidate();
        }
    }
}
