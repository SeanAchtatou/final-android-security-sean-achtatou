package com.shoujiduoduo.wallpaper.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.Locale;

public class PagerSlidingTabStrip extends HorizontalScrollView {
    private static final int[] b = {16842901, 16842904};
    private int A;
    private int B;
    private Typeface C;
    private int D;
    private int E;
    private int F;
    private int G;
    private Locale H;

    /* renamed from: a  reason: collision with root package name */
    public ViewPager.OnPageChangeListener f1826a;
    private LinearLayout.LayoutParams c;
    private final w d;
    /* access modifiers changed from: private */
    public LinearLayout e;
    /* access modifiers changed from: private */
    public ViewPager f;
    private int g;
    /* access modifiers changed from: private */
    public int h;
    /* access modifiers changed from: private */
    public float i;
    private Paint j;
    private Paint k;
    private int l;
    private int m;
    private int n;
    private int o;
    private boolean p;
    private boolean q;
    private boolean r;
    private int s;
    private int t;
    private int u;
    private int v;
    private int w;
    private int x;
    private int y;
    private int z;

    class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator CREATOR = new x();

        /* renamed from: a  reason: collision with root package name */
        int f1827a;

        private SavedState(Parcel parcel) {
            super(parcel);
            this.f1827a = parcel.readInt();
        }

        /* synthetic */ SavedState(Parcel parcel, byte b) {
            this(parcel);
        }

        public SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.f1827a);
        }
    }

    public PagerSlidingTabStrip(Context context) {
        this(context, null);
    }

    public PagerSlidingTabStrip(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public PagerSlidingTabStrip(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.d = new w(this, (byte) 0);
        this.h = 0;
        this.i = 0.0f;
        this.l = -11029688;
        this.m = -1;
        this.n = -11029688;
        this.o = -4868683;
        this.p = false;
        this.q = false;
        this.r = true;
        this.s = 52;
        this.t = 2;
        this.u = 2;
        this.v = 16;
        this.w = 10;
        this.x = 1;
        this.y = 14;
        this.z = 14;
        this.A = -8224126;
        this.B = -11029688;
        this.C = null;
        this.D = 0;
        this.E = 0;
        this.F = 0;
        this.G = f.a(f.d().getPackageName(), "drawable", "wallpaperdd_background_tab");
        setFillViewport(true);
        setWillNotDraw(false);
        if (context.getResources().getDisplayMetrics().widthPixels >= 720) {
            this.w = 10;
        } else {
            this.w = 8;
        }
        this.e = new LinearLayout(context);
        this.e.setOrientation(0);
        this.e.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
        addView(this.e);
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        this.s = (int) TypedValue.applyDimension(1, (float) this.s, displayMetrics);
        this.t = (int) TypedValue.applyDimension(1, (float) this.t, displayMetrics);
        this.u = (int) TypedValue.applyDimension(1, (float) this.u, displayMetrics);
        this.v = (int) TypedValue.applyDimension(1, (float) this.v, displayMetrics);
        this.w = (int) TypedValue.applyDimension(1, (float) this.w, displayMetrics);
        this.x = (int) TypedValue.applyDimension(1, (float) this.x, displayMetrics);
        this.y = (int) TypedValue.applyDimension(2, (float) this.y, displayMetrics);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, b);
        this.y = obtainStyledAttributes.getDimensionPixelSize(0, this.y);
        this.z = (int) TypedValue.applyDimension(2, (float) this.z, displayMetrics);
        this.z = obtainStyledAttributes.getDimensionPixelSize(0, this.z);
        this.A = obtainStyledAttributes.getColor(1, this.A);
        obtainStyledAttributes.recycle();
        String packageName = f.d().getPackageName();
        TypedArray obtainStyledAttributes2 = context.obtainStyledAttributes(attributeSet, f.a(context, "wallpaperdd_PagerSlidingTabStrip"));
        this.l = obtainStyledAttributes2.getColor(f.a(packageName, "styleable", "wallpaperdd_PagerSlidingTabStrip_wallpaperdd_pstsIndicatorColor"), this.l);
        this.m = obtainStyledAttributes2.getColor(f.a(packageName, "styleable", "wallpaperdd_PagerSlidingTabStrip_wallpaperdd_pstsUnderlineColor"), this.m);
        this.o = obtainStyledAttributes2.getColor(f.a(packageName, "styleable", "wallpaperdd_PagerSlidingTabStrip_wallpaperdd_pstsDividerColor"), this.o);
        this.t = obtainStyledAttributes2.getDimensionPixelSize(f.a(packageName, "styleable", "wallpaperdd_PagerSlidingTabStrip_wallpaperdd_pstsIndicatorHeight"), this.t);
        this.u = obtainStyledAttributes2.getDimensionPixelSize(f.a(packageName, "styleable", "wallpaperdd_PagerSlidingTabStrip_wallpaperdd_pstsUnderlineHeight"), this.u);
        this.v = obtainStyledAttributes2.getDimensionPixelSize(f.a(packageName, "styleable", "wallpaperdd_PagerSlidingTabStrip_wallpaperdd_pstsDividerPadding"), this.v);
        this.w = obtainStyledAttributes2.getDimensionPixelSize(f.a(packageName, "styleable", "wallpaperdd_PagerSlidingTabStrip_wallpaperdd_pstsTabPaddingLeftRight"), this.w);
        this.G = obtainStyledAttributes2.getResourceId(f.a(packageName, "styleable", "wallpaperdd_PagerSlidingTabStrip_wallpaperdd_pstsTabBackground"), this.G);
        this.q = obtainStyledAttributes2.getBoolean(f.a(packageName, "styleable", "wallpaperdd_PagerSlidingTabStrip_wallpaperdd_pstsShouldExpand"), this.q);
        this.s = obtainStyledAttributes2.getDimensionPixelSize(f.a(packageName, "styleable", "wallpaperdd_PagerSlidingTabStrip_wallpaperdd_pstsScrollOffset"), this.s);
        this.r = obtainStyledAttributes2.getBoolean(f.a(packageName, "styleable", "wallpaperdd_PagerSlidingTabStrip_wallpaperdd_pstsTextAllCaps"), this.r);
        obtainStyledAttributes2.recycle();
        this.j = new Paint();
        this.j.setAntiAlias(true);
        this.j.setStyle(Paint.Style.FILL);
        this.k = new Paint();
        this.k.setAntiAlias(true);
        this.k.setStrokeWidth((float) this.x);
        this.c = new LinearLayout.LayoutParams(-2, -1);
        new LinearLayout.LayoutParams(0, -1, 1.0f);
        if (this.H == null) {
            this.H = getResources().getConfiguration().locale;
        }
    }

    private void a(int i2, View view) {
        view.setFocusable(true);
        view.setOnClickListener(new v(this, i2));
        view.setPadding(this.w, 0, this.w, 0);
        this.e.addView(view, i2, this.c);
    }

    static /* synthetic */ void a(PagerSlidingTabStrip pagerSlidingTabStrip, int i2, int i3) {
        if (pagerSlidingTabStrip.g != 0) {
            int left = pagerSlidingTabStrip.e.getChildAt(i2).getLeft() + i3;
            if (i2 > 0 || i3 > 0) {
                left -= pagerSlidingTabStrip.s;
            }
            if (left != pagerSlidingTabStrip.F) {
                pagerSlidingTabStrip.F = left;
                pagerSlidingTabStrip.scrollTo(left, 0);
            }
        }
    }

    /* access modifiers changed from: private */
    public void b() {
        for (int i2 = 0; i2 < this.g; i2++) {
            View childAt = this.e.getChildAt(i2);
            if (childAt instanceof TextView) {
                TextView textView = (TextView) childAt;
                if (i2 == this.h) {
                    textView.setTextSize(0, (float) this.z);
                    textView.setTypeface(this.C, 0);
                    textView.setTextColor(this.B);
                } else {
                    textView.setTextSize(0, (float) this.y);
                    textView.setTypeface(this.C, this.D);
                    textView.setTextColor(this.A);
                }
                if (this.r) {
                    if (Build.VERSION.SDK_INT >= 14) {
                        textView.setAllCaps(true);
                    } else {
                        textView.setText(textView.getText().toString().toUpperCase(this.H));
                    }
                }
            }
        }
    }

    public void a() {
        this.e.removeAllViews();
        this.g = this.f.getAdapter().getCount();
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= this.g) {
                b();
                getViewTreeObserver().addOnGlobalLayoutListener(new u(this));
                return;
            }
            if (this.f.getAdapter() instanceof aa) {
                int a2 = ((aa) this.f.getAdapter()).a(i3);
                ImageButton imageButton = new ImageButton(getContext());
                imageButton.setImageResource(a2);
                a(i3, imageButton);
            } else {
                String charSequence = this.f.getAdapter().getPageTitle(i3).toString();
                TextView textView = new TextView(getContext());
                textView.setText(charSequence);
                textView.setGravity(17);
                textView.setSingleLine();
                a(i3, textView);
            }
            i2 = i3 + 1;
        }
    }

    public int getDividerColor() {
        return this.o;
    }

    public int getDividerPadding() {
        return this.v;
    }

    public int getIndicatorColor() {
        return this.l;
    }

    public int getIndicatorHeight() {
        return this.t;
    }

    public int getScrollOffset() {
        return this.s;
    }

    public boolean getShouldExpand() {
        return this.q;
    }

    public int getTabBackground() {
        return this.G;
    }

    public int getTabPaddingLeftRight() {
        return this.w;
    }

    public int getTextColor() {
        return this.A;
    }

    public int getTextSize() {
        return this.y;
    }

    public int getUnderlineColor() {
        return this.m;
    }

    public int getUnderlineHeight() {
        return this.u;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!isInEditMode() && this.g != 0) {
            int height = getHeight();
            this.j.setColor(this.l);
            View childAt = this.e.getChildAt(this.h);
            float left = (float) childAt.getLeft();
            float right = (float) childAt.getRight();
            if (this.i > 0.0f && this.h < this.g - 1) {
                View childAt2 = this.e.getChildAt(this.h + 1);
                left = (left * (1.0f - this.i)) + (((float) childAt2.getLeft()) * this.i);
                right = (right * (1.0f - this.i)) + (((float) childAt2.getRight()) * this.i);
            }
            canvas.drawRect(left, (float) (height - this.t), right, (float) height, this.j);
        }
    }

    public void onRestoreInstanceState(Parcelable parcelable) {
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        this.h = savedState.f1827a;
        requestLayout();
    }

    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.f1827a = this.h;
        return savedState;
    }

    public void setAllCaps(boolean z2) {
        this.r = z2;
    }

    public void setDividerColor(int i2) {
        this.o = i2;
        invalidate();
    }

    public void setDividerColorResource(int i2) {
        this.o = getResources().getColor(i2);
        invalidate();
    }

    public void setDividerPadding(int i2) {
        this.v = i2;
        invalidate();
    }

    public void setIndicatorColor(int i2) {
        this.l = i2;
        invalidate();
    }

    public void setIndicatorColorResource(int i2) {
        this.l = getResources().getColor(i2);
        invalidate();
    }

    public void setIndicatorHeight(int i2) {
        this.t = i2;
        invalidate();
    }

    public void setOnPageChangeListener(ViewPager.OnPageChangeListener onPageChangeListener) {
        this.f1826a = onPageChangeListener;
    }

    public void setScrollOffset(int i2) {
        this.s = i2;
        invalidate();
    }

    public void setShouldExpand(boolean z2) {
        this.q = z2;
        requestLayout();
    }

    public void setTabBackground(int i2) {
        this.G = i2;
    }

    public void setTabPaddingLeftRight(int i2) {
        this.w = i2;
        b();
    }

    public void setTextColor(int i2) {
        this.A = i2;
        b();
    }

    public void setTextColorResource(int i2) {
        this.A = getResources().getColor(i2);
        b();
    }

    public void setTextSize(int i2) {
        this.y = i2;
        b();
    }

    public void setUnderlineColor(int i2) {
        this.m = i2;
        invalidate();
    }

    public void setUnderlineColorResource(int i2) {
        this.m = getResources().getColor(i2);
        invalidate();
    }

    public void setUnderlineHeight(int i2) {
        this.u = i2;
        invalidate();
    }

    public void setViewPager(ViewPager viewPager) {
        this.f = viewPager;
        if (viewPager.getAdapter() == null) {
            throw new IllegalStateException("ViewPager does not have adapter instance.");
        }
        viewPager.setOnPageChangeListener(this.d);
        a();
    }
}
