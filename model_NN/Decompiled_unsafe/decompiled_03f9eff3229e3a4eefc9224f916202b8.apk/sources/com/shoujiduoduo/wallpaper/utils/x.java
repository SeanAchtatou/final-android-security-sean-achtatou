package com.shoujiduoduo.wallpaper.utils;

import android.os.Parcel;
import android.os.Parcelable;
import com.shoujiduoduo.wallpaper.utils.PagerSlidingTabStrip;

final class x implements Parcelable.Creator {
    x() {
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        return new PagerSlidingTabStrip.SavedState(parcel, (byte) 0);
    }

    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new PagerSlidingTabStrip.SavedState[i];
    }
}
