package com.shoujiduoduo.wallpaper.utils.a;

import android.content.Context;
import android.os.Build;

public abstract class b {

    /* renamed from: a  reason: collision with root package name */
    protected String f1831a;
    protected Context b = null;

    static {
        b.class.getSimpleName();
    }

    public void a(Context context, String str) {
        this.b = context;
        this.f1831a = str;
    }

    public boolean a() {
        if (Build.VERSION.SDK_INT < 15) {
        }
        return false;
    }

    public abstract boolean a(String str);
}
