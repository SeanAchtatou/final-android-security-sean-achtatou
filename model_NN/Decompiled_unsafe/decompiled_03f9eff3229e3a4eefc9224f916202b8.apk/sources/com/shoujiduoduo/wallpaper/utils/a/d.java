package com.shoujiduoduo.wallpaper.utils.a;

import android.app.WallpaperManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Parcel;
import android.widget.Toast;
import com.shoujiduoduo.wallpaper.utils.f;
import java.io.FileInputStream;
import java.io.InputStream;
import java.lang.reflect.Method;

public class d extends b implements ServiceConnection {
    private Class c;
    private WallpaperManager d = null;
    private Method e = null;
    private String f = "miui.app.resourcebrowser.service.IThemeManagerService";
    private String g = "miui.app.resourcebrowser.service.IThemeManagerService";
    /* access modifiers changed from: private */
    public String h = null;
    private Handler i = new g(this);
    private IBinder j = null;

    /* access modifiers changed from: private */
    public boolean b(String str) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken(this.g);
            obtain.writeString(str);
            this.j.transact(1, obtain, obtain2, 0);
            if (obtain2.readInt() == 0) {
                Toast.makeText(this.b, this.b.getString(f.c("R.string.wallpaperdd_toast_on_lockscreen_success")), 0).show();
                return true;
            }
            if (!(this.d == null || this.e == null)) {
                FileInputStream fileInputStream = new FileInputStream(str);
                this.e.invoke(this.d, fileInputStream, new Boolean(true));
                fileInputStream.close();
                Toast.makeText(this.b, this.b.getString(f.c("R.string.wallpaperdd_toast_on_lockscreen_success")), 0).show();
                return true;
            }
            obtain.recycle();
            obtain2.recycle();
            Toast.makeText(this.b, this.b.getString(f.c("R.string.wallpaperdd_toast_on_lockscreen_failed")), 0).show();
            return false;
        } catch (Exception e2) {
        }
    }

    public void a(Context context, String str) {
        super.a(context, "xiaomi");
        if (Build.VERSION.SDK_INT < 14) {
            this.d = WallpaperManager.getInstance(context);
            this.e = WallpaperManager.class.getMethod("setStream", InputStream.class, Boolean.TYPE);
        }
        try {
            b();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public boolean a() {
        if (!this.f1831a.equalsIgnoreCase(Build.BRAND) || Build.VERSION.SDK_INT < 14) {
            return false;
        }
        try {
            if (b()) {
                return true;
            }
            if (!this.b.bindService(new Intent(this.f), null, 1)) {
                return false;
            }
            this.b.unbindService(null);
            return true;
        } catch (Exception e2) {
            e2.printStackTrace();
            return false;
        }
    }

    public boolean a(String str) {
        if (str == null || this.h != null) {
            return false;
        }
        this.h = str;
        if (Build.VERSION.SDK_INT >= 14) {
            if (!this.b.bindService(new Intent(this.f), this, 1)) {
                this.h = null;
                Toast.makeText(this.b, this.b.getString(f.c("R.string.wallpaperdd_toast_on_lockscreen_failed")), 0).show();
                return false;
            }
        }
        Toast.makeText(this.b, this.b.getString(f.c("R.string.wallpaperdd_toast_on_setting_lockscreen")), 0).show();
        return true;
    }

    public boolean b() {
        ClassLoader systemClassLoader = ClassLoader.getSystemClassLoader();
        if (systemClassLoader != null) {
            try {
                this.c = systemClassLoader.loadClass("miui.content.res.IThemeService");
                if (this.c != null) {
                    this.f = "com.miui.service.THEME";
                    this.g = "miui.content.res.IThemeService";
                }
                return true;
            } catch (ClassNotFoundException e2) {
                e2.printStackTrace();
            }
        }
        return false;
    }

    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        this.j = iBinder;
        this.i.sendEmptyMessage(7200);
    }

    public void onServiceDisconnected(ComponentName componentName) {
        this.j = null;
        this.h = null;
    }
}
