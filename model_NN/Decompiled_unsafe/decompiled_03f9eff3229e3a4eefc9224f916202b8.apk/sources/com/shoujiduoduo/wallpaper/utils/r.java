package com.shoujiduoduo.wallpaper.utils;

import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;
import com.d.a.b.a.c;
import com.d.a.b.a.n;
import com.d.a.b.d;
import com.shoujiduoduo.wallpaper.a.h;
import com.shoujiduoduo.wallpaper.a.j;
import com.shoujiduoduo.wallpaper.kernel.a;
import com.shoujiduoduo.wallpaper.kernel.b;
import java.io.File;

final class r extends n {

    /* renamed from: a  reason: collision with root package name */
    private j f1877a;
    private /* synthetic */ MyImageSlider b;

    public r(MyImageSlider myImageSlider, j jVar) {
        this.b = myImageSlider;
        this.f1877a = jVar;
    }

    public final void onLoadingComplete(String str, View view, Bitmap bitmap) {
        File a2;
        b.a(MyImageSlider.b, "onLoadingComplete thread id = " + Thread.currentThread().getId());
        b.a(MyImageSlider.b, "onLoadingComplete image width = " + bitmap.getWidth() + ", height = " + bitmap.getHeight());
        File file = null;
        if (this.f1877a.e != null && this.f1877a.e.length() > 0) {
            file = new File(this.f1877a.e);
        }
        if ((file == null || !file.exists()) && h.d().c(this.f1877a.k) && (a2 = com.d.a.b.a.b.a(this.f1877a.c, d.a().d())) != null && a2.exists()) {
            String str2 = String.valueOf(f.a()) + "favorate/" + this.f1877a.k + ".jpg";
            if (m.a(a2, new File(str2))) {
                h.d().a(this.f1877a.k, str2);
            }
        }
        view.setTag("finished");
        if (view == this.b.c[1] && this.b.n != null) {
            this.b.n.a(this.f1877a.k, a.LOAD_FINISHED);
        }
        if (view == this.b.c[0]) {
            b.a(MyImageSlider.b, "view 0. loadedImage width = " + bitmap.getWidth() + ", loadedImage height = " + bitmap.getHeight());
            this.b.d[0] = (bitmap.getWidth() * this.b.g) / bitmap.getHeight();
            this.b.c[0].setScaleType(ImageView.ScaleType.MATRIX);
            this.b.o[0].set(((ImageView) view).getImageMatrix());
            this.b.c[0].setImageMatrix(this.b.o[0]);
        } else if (view == this.b.c[1]) {
            b.a(MyImageSlider.b, "view 1. loadedImage width = " + bitmap.getWidth() + ", loadedImage height = " + bitmap.getHeight());
            this.b.d[1] = (bitmap.getWidth() * this.b.g) / bitmap.getHeight();
            this.b.c[1].setScaleType(ImageView.ScaleType.MATRIX);
            this.b.o[1].set(((ImageView) view).getImageMatrix());
            this.b.c[1].setImageMatrix(this.b.o[1]);
        } else if (view == this.b.c[2]) {
            b.a(MyImageSlider.b, "view 2. loadedImage width = " + bitmap.getWidth() + ", loadedImage height = " + bitmap.getHeight());
            this.b.d[2] = (bitmap.getWidth() * this.b.g) / bitmap.getHeight();
            this.b.c[2].setScaleType(ImageView.ScaleType.MATRIX);
            this.b.o[2].set(((ImageView) view).getImageMatrix());
            this.b.c[2].setImageMatrix(this.b.o[2]);
        }
    }

    public final void onLoadingFailed(String str, View view, c cVar) {
        view.setTag("failed");
        if (view == this.b.c[1] && this.b.n != null) {
            this.b.n.a(this.f1877a.k, a.LOAD_FAILED);
        }
        if (view == this.b.c[0]) {
            this.b.d[0] = this.b.f;
        } else if (view == this.b.c[1]) {
            this.b.d[1] = this.b.f;
        } else if (view == this.b.c[2]) {
            this.b.d[2] = this.b.f;
        }
    }

    public final void onLoadingStarted(String str, View view) {
        view.setTag("loading");
        if (view == this.b.c[1] && this.b.n != null) {
            this.b.n.a(this.f1877a.k, a.LOADING);
        }
    }
}
