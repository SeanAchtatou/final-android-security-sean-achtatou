package com.shoujiduoduo.wallpaper.utils;

import com.baidu.mobad.feeds.BaiduNative;
import com.baidu.mobad.feeds.NativeErrorCode;
import com.shoujiduoduo.wallpaper.kernel.b;
import java.util.List;

final class aq implements BaiduNative.BaiduNativeNetworkListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ at f1845a;

    aq(at atVar) {
        this.f1845a = atVar;
    }

    public final void onNativeFail(NativeErrorCode nativeErrorCode) {
        this.f1845a.f = false;
        if (nativeErrorCode != null) {
            b.a(at.b, "requestAD failed. onNativeFail reason: " + nativeErrorCode.name());
        }
        if (this.f1845a.c != null) {
            com.umeng.analytics.b.b(this.f1845a.c, "EVENT_REQUEST_BAIDU_AD_FAILED");
        }
    }

    public final void onNativeLoad(List list) {
        if (list != null && list.size() > 0) {
            this.f1845a.f1847a = list;
        }
        this.f1845a.f = false;
        if (this.f1845a.c != null) {
            com.umeng.analytics.b.b(this.f1845a.c, "EVENT_REQUEST_BAIDU_AD_SUCCESS");
        }
    }
}
