package com.shoujiduoduo.wallpaper.utils.b;

import android.app.Activity;
import android.content.Context;
import com.umeng.socialize.bean.SocializeConfig;
import com.umeng.socialize.c.b;
import com.umeng.socialize.controller.UMServiceFactory;
import com.umeng.socialize.controller.UMSocialService;
import com.umeng.socialize.sso.QZoneSsoHandler;
import com.umeng.socialize.sso.SinaSsoHandler;
import com.umeng.socialize.sso.UMQQSsoHandler;
import com.umeng.socialize.weixin.controller.UMWXHandler;

public class a {
    private static a d = null;
    private static /* synthetic */ int[] f;

    /* renamed from: a  reason: collision with root package name */
    private String f1851a = "wxb4cd572ca73fd239";
    private String b = "48bccd48df1afa300a52bd75611a6710";
    private Context c;
    private final UMSocialService e = UMServiceFactory.getUMSocialService("com.umeng.share", com.umeng.socialize.c.a.SOCIAL);

    private a() {
    }

    private a(Context context) {
        this.c = context;
        this.e.setAppWebSite(b.RENREN, "http://www.shoujiduoduo.com");
        this.e.setAppWebSite(b.QZONE, "http://www.umeng.com/social");
        this.e.getConfig().setSsoHandler(new SinaSsoHandler());
        new UMQQSsoHandler((Activity) this.c, "100382066", "2305e02edac0c67f822f453d92be2a66").addToSocialSDK();
        new QZoneSsoHandler((Activity) this.c, "100382066", "2305e02edac0c67f822f453d92be2a66").addToSocialSDK();
        new com.umeng.socialize.e.a().addToSocialSDK();
        new UMWXHandler((Activity) this.c, this.f1851a, this.b).addToSocialSDK();
        UMWXHandler uMWXHandler = new UMWXHandler((Activity) this.c, this.f1851a, this.b);
        uMWXHandler.setToCircle(true);
        uMWXHandler.addToSocialSDK();
        this.e.getConfig().setSsoHandler(new com.umeng.socialize.e.b());
        SocializeConfig config = this.e.getConfig();
        config.addFollow(b.SINA, "1694800747");
        config.setOauthDialogFollowListener(new b(this));
    }

    public static synchronized a a() {
        a aVar;
        synchronized (a.class) {
            aVar = d;
        }
        return aVar;
    }

    public static synchronized a a(Context context) {
        a aVar;
        synchronized (a.class) {
            if (d == null) {
                d = new a(context);
            }
            aVar = d;
        }
        return aVar;
    }

    private void a(b bVar) {
        com.shoujiduoduo.wallpaper.kernel.b.a("UmengSocialUtils", "share media:" + bVar);
        if (this.c != null && !((Activity) this.c).isFinishing()) {
            this.e.postShare(this.c, bVar, new c(this));
        }
    }

    private static /* synthetic */ int[] c() {
        int[] iArr = f;
        if (iArr == null) {
            iArr = new int[b.values().length];
            try {
                iArr[b.DOUBAN.ordinal()] = 12;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[b.EMAIL.ordinal()] = 4;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[b.EVERNOTE.ordinal()] = 21;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[b.FACEBOOK.ordinal()] = 13;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[b.FLICKR.ordinal()] = 28;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[b.FOURSQUARE.ordinal()] = 24;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[b.GENERIC.ordinal()] = 2;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[b.GOOGLEPLUS.ordinal()] = 1;
            } catch (NoSuchFieldError e9) {
            }
            try {
                iArr[b.INSTAGRAM.ordinal()] = 19;
            } catch (NoSuchFieldError e10) {
            }
            try {
                iArr[b.KAKAO.ordinal()] = 30;
            } catch (NoSuchFieldError e11) {
            }
            try {
                iArr[b.LAIWANG.ordinal()] = 15;
            } catch (NoSuchFieldError e12) {
            }
            try {
                iArr[b.LAIWANG_DYNAMIC.ordinal()] = 16;
            } catch (NoSuchFieldError e13) {
            }
            try {
                iArr[b.LINE.ordinal()] = 27;
            } catch (NoSuchFieldError e14) {
            }
            try {
                iArr[b.LINKEDIN.ordinal()] = 23;
            } catch (NoSuchFieldError e15) {
            }
            try {
                iArr[b.PINTEREST.ordinal()] = 20;
            } catch (NoSuchFieldError e16) {
            }
            try {
                iArr[b.POCKET.ordinal()] = 22;
            } catch (NoSuchFieldError e17) {
            }
            try {
                iArr[b.QQ.ordinal()] = 7;
            } catch (NoSuchFieldError e18) {
            }
            try {
                iArr[b.QZONE.ordinal()] = 6;
            } catch (NoSuchFieldError e19) {
            }
            try {
                iArr[b.RENREN.ordinal()] = 8;
            } catch (NoSuchFieldError e20) {
            }
            try {
                iArr[b.SINA.ordinal()] = 5;
            } catch (NoSuchFieldError e21) {
            }
            try {
                iArr[b.SMS.ordinal()] = 3;
            } catch (NoSuchFieldError e22) {
            }
            try {
                iArr[b.TENCENT.ordinal()] = 11;
            } catch (NoSuchFieldError e23) {
            }
            try {
                iArr[b.TUMBLR.ordinal()] = 29;
            } catch (NoSuchFieldError e24) {
            }
            try {
                iArr[b.TWITTER.ordinal()] = 14;
            } catch (NoSuchFieldError e25) {
            }
            try {
                iArr[b.WEIXIN.ordinal()] = 9;
            } catch (NoSuchFieldError e26) {
            }
            try {
                iArr[b.WEIXIN_CIRCLE.ordinal()] = 10;
            } catch (NoSuchFieldError e27) {
            }
            try {
                iArr[b.WHATSAPP.ordinal()] = 26;
            } catch (NoSuchFieldError e28) {
            }
            try {
                iArr[b.YIXIN.ordinal()] = 17;
            } catch (NoSuchFieldError e29) {
            }
            try {
                iArr[b.YIXIN_CIRCLE.ordinal()] = 18;
            } catch (NoSuchFieldError e30) {
            }
            try {
                iArr[b.YNOTE.ordinal()] = 25;
            } catch (NoSuchFieldError e31) {
            }
            f = iArr;
        }
        return iArr;
    }

    /* JADX WARN: Type inference failed for: r3v5, types: [com.umeng.socialize.media.c, com.umeng.socialize.media.UMediaObject] */
    /* JADX WARN: Type inference failed for: r3v6, types: [com.umeng.socialize.media.b, com.umeng.socialize.media.UMediaObject] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void a(java.lang.String r6, java.lang.String r7, java.lang.String r8, int r9, com.umeng.socialize.c.b r10) {
        /*
            r5 = this;
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = "我分享了一张美图“"
            r0.<init>(r1)
            java.lang.StringBuilder r0 = r0.append(r8)
            java.lang.String r1 = "”，快来看看吧！戳大图>> "
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = r0.toString()
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r2 = "http://main.shoujiduoduo.com/wpshare/index_"
            r0.<init>(r2)
            java.lang.StringBuilder r0 = r0.append(r9)
            java.lang.String r2 = ".html"
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r2 = r0.toString()
            com.umeng.socialize.media.g r0 = new com.umeng.socialize.media.g
            android.content.Context r3 = r5.c
            r0.<init>(r3, r6)
            int[] r3 = c()
            int r4 = r10.ordinal()
            r3 = r3[r4]
            switch(r3) {
                case 3: goto L_0x0120;
                case 4: goto L_0x003e;
                case 5: goto L_0x0101;
                case 6: goto L_0x00d4;
                case 7: goto L_0x00a7;
                case 8: goto L_0x003e;
                case 9: goto L_0x0042;
                case 10: goto L_0x006b;
                default: goto L_0x003e;
            }
        L_0x003e:
            r5.a(r10)
            return
        L_0x0042:
            com.umeng.socialize.weixin.media.WeiXinShareContent r3 = new com.umeng.socialize.weixin.media.WeiXinShareContent
            com.umeng.socialize.media.g r0 = (com.umeng.socialize.media.g) r0
            r3.<init>(r0)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = java.lang.String.valueOf(r1)
            r0.<init>(r1)
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
            r3.setShareContent(r0)
            java.lang.String r0 = "壁纸多多"
            r3.setTitle(r0)
            r3.setTargetUrl(r2)
            com.umeng.socialize.controller.UMSocialService r0 = r5.e
            r0.setShareMedia(r3)
            goto L_0x003e
        L_0x006b:
            com.umeng.socialize.weixin.media.CircleShareContent r3 = new com.umeng.socialize.weixin.media.CircleShareContent
            com.umeng.socialize.media.g r0 = (com.umeng.socialize.media.g) r0
            r3.<init>(r0)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = java.lang.String.valueOf(r1)
            r0.<init>(r1)
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
            r3.setShareContent(r0)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = "壁纸多多：我分享了一张美图“"
            r0.<init>(r1)
            java.lang.StringBuilder r0 = r0.append(r8)
            java.lang.String r1 = "”，大图请猛戳。"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            r3.setTitle(r0)
            r3.setTargetUrl(r2)
            com.umeng.socialize.controller.UMSocialService r0 = r5.e
            r0.setShareMedia(r3)
            goto L_0x003e
        L_0x00a7:
            com.umeng.socialize.media.b r3 = new com.umeng.socialize.media.b
            r3.<init>()
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r1 = java.lang.String.valueOf(r1)
            r4.<init>(r1)
            java.lang.StringBuilder r1 = r4.append(r2)
            java.lang.String r1 = r1.toString()
            r3.setShareContent(r1)
            java.lang.String r1 = "壁纸多多"
            r3.a(r1)
            com.umeng.socialize.media.g r0 = (com.umeng.socialize.media.g) r0
            r3.setShareImage(r0)
            r3.setTargetUrl(r2)
            com.umeng.socialize.controller.UMSocialService r0 = r5.e
            r0.setShareMedia(r3)
            goto L_0x003e
        L_0x00d4:
            com.umeng.socialize.media.c r3 = new com.umeng.socialize.media.c
            r3.<init>()
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r1 = java.lang.String.valueOf(r1)
            r4.<init>(r1)
            java.lang.StringBuilder r1 = r4.append(r2)
            java.lang.String r1 = r1.toString()
            r3.setShareContent(r1)
            r3.setTargetUrl(r2)
            java.lang.String r1 = "壁纸多多"
            r3.a(r1)
            com.umeng.socialize.media.g r0 = (com.umeng.socialize.media.g) r0
            r3.setShareImage(r0)
            com.umeng.socialize.controller.UMSocialService r0 = r5.e
            r0.setShareMedia(r3)
            goto L_0x003e
        L_0x0101:
            com.umeng.socialize.controller.UMSocialService r3 = r5.e
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r1 = java.lang.String.valueOf(r1)
            r4.<init>(r1)
            java.lang.StringBuilder r1 = r4.append(r2)
            java.lang.String r1 = r1.toString()
            r3.setShareContent(r1)
            com.umeng.socialize.controller.UMSocialService r1 = r5.e
            com.umeng.socialize.media.g r0 = (com.umeng.socialize.media.g) r0
            r1.setShareMedia(r0)
            goto L_0x003e
        L_0x0120:
            com.umeng.socialize.controller.UMSocialService r0 = r5.e
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r1 = java.lang.String.valueOf(r1)
            r3.<init>(r1)
            java.lang.StringBuilder r1 = r3.append(r2)
            java.lang.String r1 = r1.toString()
            r0.setShareContent(r1)
            goto L_0x003e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.wallpaper.utils.b.a.a(java.lang.String, java.lang.String, java.lang.String, int, com.umeng.socialize.c.b):void");
    }

    public void b() {
        this.c = null;
        d = null;
    }
}
