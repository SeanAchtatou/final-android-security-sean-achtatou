package com.shoujiduoduo.wallpaper.utils;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AbsoluteLayout;
import android.widget.ImageView;
import com.shoujiduoduo.wallpaper.kernel.b;

public class HorizontalSlider extends AbsoluteLayout {
    /* access modifiers changed from: private */
    public static final String b = HorizontalSlider.class.getSimpleName();

    /* renamed from: a  reason: collision with root package name */
    s f1823a;
    /* access modifiers changed from: private */
    public Context c;
    /* access modifiers changed from: private */
    public ImageView d;
    /* access modifiers changed from: private */
    public int e;
    private int f;
    /* access modifiers changed from: private */
    public int g;
    private int h;
    /* access modifiers changed from: private */
    public int i;
    private View.OnTouchListener j = new bl(this);

    public HorizontalSlider(Context context) {
        super(context);
        this.c = context;
        c();
    }

    public HorizontalSlider(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.c = context;
        c();
    }

    public HorizontalSlider(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.c = context;
        c();
    }

    private void c() {
        b.a(b, "init.");
        ImageView imageView = new ImageView(this.c);
        imageView.setLayoutParams(new AbsoluteLayout.LayoutParams(-2, -2, 0, 0));
        imageView.setBackgroundDrawable(this.c.getResources().getDrawable(17170445));
        imageView.setImageDrawable(this.c.getResources().getDrawable(f.a(this.c.getPackageName(), "drawable", "wallpaperdd_horizontal_slider_background")));
        addView(imageView);
        imageView.measure(View.MeasureSpec.makeMeasureSpec(0, 0), View.MeasureSpec.makeMeasureSpec(0, 0));
        this.h = imageView.getMeasuredHeight();
        this.g = imageView.getMeasuredWidth();
        b.a(b, "height = " + this.h + "width = " + this.g);
        Drawable drawable = this.c.getResources().getDrawable(f.a(this.c.getPackageName(), "drawable", "wallpaperdd_horizontal_slider_normal"));
        this.e = drawable.getIntrinsicWidth();
        this.f = drawable.getIntrinsicHeight();
        b.a(b, "front pic height = " + drawable.getIntrinsicHeight() + ", front pic width = " + drawable.getIntrinsicWidth());
        this.d = new ImageView(this.c);
        this.i = (this.g - this.e) / 2;
        this.d.setLayoutParams(new AbsoluteLayout.LayoutParams(this.e, this.f, this.i, (this.h - this.f) / 2));
        this.d.setBackgroundDrawable(this.c.getResources().getDrawable(17170445));
        this.d.setImageDrawable(this.c.getResources().getDrawable(f.a(this.c.getPackageName(), "drawable", "wallpaperdd_horizontal_slider_normal")));
        this.d.setOnTouchListener(this.j);
        addView(this.d);
    }

    public void a() {
        this.i = (this.g - this.e) / 2;
        requestLayout();
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        super.onLayout(z, i2, i3, i4, i5);
        this.d.layout(this.i, this.d.getTop(), this.i + this.d.getWidth(), this.d.getBottom());
    }

    public void setListener(s sVar) {
        this.f1823a = sVar;
    }
}
