package com.shoujiduoduo.wallpaper.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.d.a.b.a.n;
import com.d.a.b.c;
import com.d.a.b.d;
import com.shoujiduoduo.wallpaper.a.j;
import com.shoujiduoduo.wallpaper.a.k;
import com.shoujiduoduo.wallpaper.activity.NewMainActivity;
import com.shoujiduoduo.wallpaper.kernel.App;
import com.umeng.analytics.b;

public class t extends BaseAdapter {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final String f1878a = t.class.getSimpleName();
    /* access modifiers changed from: private */
    public k b;
    /* access modifiers changed from: private */
    public Context c;
    private c d;
    private int e = 12;
    private int f = 8;
    /* access modifiers changed from: private */
    public String g;
    private n h = new a(this);

    private t() {
    }

    public t(Context context, k kVar, boolean z) {
        this.b = kVar;
        this.c = context;
        this.d = new c.a().a(false).b(true).c(true).a(Bitmap.Config.RGB_565).c();
        if (!z) {
            this.e = 99999999;
        }
    }

    static /* synthetic */ void c(t tVar) {
        String str;
        switch (tVar.b.b()) {
            case 0:
                str = "CLICKPIC_IN_LIST_HOT";
                break;
            case 1:
                str = "CLICKPIC_IN_LIST_NEW";
                break;
            case 2:
                str = "CLICKPIC_IN_LIST_SHARE";
                break;
            case 3:
                str = "CLICKPIC_IN_LIST_HIGHRESOLUTION";
                break;
            case 4:
                str = "CLICKPIC_IN_LIST_BAIDU";
                break;
            case 11:
                str = "CLICKPIC_IN_LIST_MEINV";
                break;
            case 12:
                str = "CLICKPIC_IN_LIST_MINGXING";
                break;
            case 13:
                str = "CLICKPIC_IN_LIST_DONGMAN";
                break;
            case 14:
                str = "CLICKPIC_IN_LIST_DONGWU";
                break;
            case 15:
                str = "CLICKPIC_IN_LIST_QICHE";
                break;
            case 16:
                str = "CLICKPIC_IN_LIST_PINPAI";
                break;
            case 17:
                str = "CLICKPIC_IN_LIST_CHENGSHI";
                break;
            case 18:
                str = "CLICKPIC_IN_LIST_ZIRAN";
                break;
            case 19:
                str = "CLICKPIC_IN_LIST_KEJI";
                break;
            case 20:
                str = "CLICKPIC_IN_LIST_YUNDONG";
                break;
            case 21:
                str = "CLICKPIC_IN_LIST_SHEJI";
                break;
            case 22:
                str = "CLICKPIC_IN_LIST_WUYU";
                break;
            case 23:
                str = "CLICKPIC_IN_LIST_QITA";
                break;
            case D.QHINTERSTITIALAD_showAds:
            default:
                return;
            case D.QHINTERSTITIALAD_setAdEventListener:
                str = "CLICKPIC_IN_LIST_YINGSHI";
                break;
            case D.QHNATIVEAD_getContent:
                str = "CLICKPIC_IN_LIST_QINGGAN";
                break;
            case 27:
                str = "CLICKPIC_IN_LIST_WENZI";
                break;
            case 999999998:
                str = "CLICKPIC_IN_LIST_SEARCH";
                break;
        }
        if (tVar.c != null) {
            b.b(tVar.c, str);
        }
    }

    public void a() {
        this.c = null;
        this.b = null;
        this.d = null;
    }

    public void a(k kVar) {
        this.b = kVar;
        notifyDataSetChanged();
    }

    public void a(String str) {
        this.g = str;
    }

    public int getCount() {
        if (this.b == null) {
            return 0;
        }
        int a2 = this.b.a();
        int i = (a2 % 2) + (a2 / 2);
        if (i > this.f) {
            i = i + ((i - this.f) / this.e) + 1;
        }
        return (this.b.b() != 999999998 || this.b.e()) ? i : i + 1;
    }

    public Object getItem(int i) {
        return null;
    }

    public long getItemId(int i) {
        return 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int i, View view, ViewGroup viewGroup) {
        View view2;
        String format;
        String format2;
        if (i == getCount() - 1) {
            com.shoujiduoduo.wallpaper.kernel.b.a(f1878a, "reach bottom of the gridview.");
            if (!this.b.f() && this.b.e()) {
                com.shoujiduoduo.wallpaper.kernel.b.a(f1878a, "load more data.");
                this.b.d();
            }
        }
        if (this.b.b() != 999999998 || i != getCount() - 1 || this.b.f() || this.b.e()) {
            if (i < this.f || (i - this.f) % this.e != 0) {
                if (view == null || !"thumb_line_view".equals((String) view.getTag())) {
                    View inflate = LayoutInflater.from(this.c).inflate(f.c("R.layout.wallpaperdd_listview_img_thumb"), viewGroup, false);
                    inflate.setTag("thumb_line_view");
                    view2 = inflate;
                } else {
                    view2 = view;
                }
                if (i >= this.f) {
                    i = (i - ((i - this.f) / this.e)) - 1;
                }
                RoundedImageView roundedImageView = (RoundedImageView) view2.findViewById(f.a(this.c.getPackageName(), "id", "listview_image_thumb_left"));
                RoundedImageView roundedImageView2 = (RoundedImageView) view2.findViewById(f.a(this.c.getPackageName(), "id", "listview_image_thumb_right"));
                RelativeLayout relativeLayout = (RelativeLayout) view2.findViewById(f.c("R.id.listview_image_text_info_container_left"));
                RelativeLayout relativeLayout2 = (RelativeLayout) view2.findViewById(f.c("R.id.listview_image_text_info_container_right"));
                TextView textView = (TextView) view2.findViewById(f.c("R.id.listview_image_title_left"));
                TextView textView2 = (TextView) view2.findViewById(f.c("R.id.listview_image_down_count_left"));
                TextView textView3 = (TextView) view2.findViewById(f.c("R.id.listview_image_title_right"));
                TextView textView4 = (TextView) view2.findViewById(f.c("R.id.listview_image_down_count_right"));
                int i2 = this.c.getResources().getDisplayMetrics().widthPixels;
                if (i2 == 480 || i2 == 540) {
                    textView.setTextSize(12.0f);
                    textView2.setTextSize(12.0f);
                    textView3.setTextSize(12.0f);
                    textView4.setTextSize(12.0f);
                }
                if (view2 == view) {
                    String str = (String) roundedImageView.getTag();
                    String str2 = (String) roundedImageView2.getTag();
                    if (i < this.b.a() / 2) {
                        String str3 = this.b.a(i << 1).b;
                        String str4 = this.b.a((i << 1) + 1).b;
                        if (str == str3 && str2 == str4) {
                            return view2;
                        }
                    }
                }
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(App.f1817a, App.b);
                layoutParams.addRule(14);
                roundedImageView.setLayoutParams(layoutParams);
                roundedImageView2.setLayoutParams(layoutParams);
                RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(App.f1817a, App.d);
                layoutParams2.addRule(3, f.c("R.id.listview_image_thumb_left"));
                relativeLayout.setLayoutParams(layoutParams2);
                RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(App.f1817a, App.d);
                layoutParams3.addRule(3, f.c("R.id.listview_image_thumb_right"));
                relativeLayout2.setLayoutParams(layoutParams3);
                roundedImageView.setImageDrawable(App.e);
                roundedImageView.setCornerRadius(0);
                roundedImageView.setBorderWidth(0);
                roundedImageView.setBorderColor(-12303292);
                roundedImageView.setRoundBackground(false);
                roundedImageView.setBackgroundResource(f.a(this.c.getPackageName(), "drawable", "wallpaperdd_background"));
                j a2 = (i << 1) < this.b.a() ? this.b.a(i << 1) : null;
                d.a().a(a2 == null ? null : a2.b, roundedImageView, this.d, this.h, new e(this));
                textView.setText(a2 == null ? "" : a2.h);
                if (a2 != null) {
                    if (a2.k > 0) {
                        if (a2.f1724a < 10000) {
                            format2 = String.valueOf(a2.f1724a == 0 ? ((int) (Math.random() * 901.0d)) + 100 : a2.f1724a);
                        } else {
                            format2 = String.format("%.1f万", Float.valueOf(((float) a2.f1724a) / 10000.0f));
                        }
                        textView2.setText(format2);
                        textView2.setVisibility(0);
                    } else {
                        textView2.setVisibility(4);
                        com.shoujiduoduo.wallpaper.kernel.b.a(f1878a, "dataid = " + a2.k + ", set downtext invisible.");
                    }
                }
                roundedImageView.setOnClickListener(new g(this, i));
                roundedImageView2.setImageDrawable(App.e);
                roundedImageView2.setCornerRadius(0);
                roundedImageView2.setBorderWidth(0);
                roundedImageView2.setBorderColor(-12303292);
                roundedImageView2.setRoundBackground(false);
                roundedImageView2.setBackgroundResource(f.c("R.drawable.wallpaperdd_background"));
                j a3 = (i << 1) + 1 < this.b.a() ? this.b.a((i << 1) + 1) : null;
                d.a().a(a3 == null ? null : a3.b, roundedImageView2, this.d, this.h, new k(this));
                textView3.setText(a3 == null ? "" : a3.h);
                if (a3 != null) {
                    if (a3.k > 0) {
                        if (a3.f1724a < 10000) {
                            format = String.valueOf(a3.f1724a == 0 ? ((int) (Math.random() * 901.0d)) + 100 : a3.f1724a);
                        } else {
                            format = String.format("%.1f万", Float.valueOf(((float) a3.f1724a) / 10000.0f));
                        }
                        textView4.setText(format);
                        textView4.setVisibility(0);
                    } else {
                        com.shoujiduoduo.wallpaper.kernel.b.a(f1878a, "dataid = " + a3.k + ", set downtext invisible.");
                        textView4.setVisibility(4);
                    }
                    roundedImageView2.setOnClickListener(new l(this, i));
                    return view2;
                }
                textView4.setVisibility(4);
                roundedImageView2.setOnClickListener(null);
                return view2;
            }
            com.shoujiduoduo.wallpaper.kernel.b.a(f1878a, "show AD, position = " + i);
            if (!NewMainActivity.b) {
                return b.a(this.c, view);
            }
            return b.a(this.c, view, (i - this.f) / this.e);
        }
        if (view == null || !"bdimg_search_button".equalsIgnoreCase((String) view.getTag())) {
            view = LayoutInflater.from(this.c).inflate(f.c("R.layout.wallpaperdd_bdimg_search_button"), viewGroup, false);
            view.setTag("bdimg_search_button");
        }
        if (this.b.a() == 0) {
            view.findViewById(f.c("R.id.no_search_result_prompt")).setVisibility(0);
        } else {
            view.findViewById(f.c("R.id.no_search_result_prompt")).setVisibility(8);
        }
        ((Button) view.findViewById(f.c("R.id.bdimg_search_button"))).setOnClickListener(new d(this));
        return view;
    }
}
