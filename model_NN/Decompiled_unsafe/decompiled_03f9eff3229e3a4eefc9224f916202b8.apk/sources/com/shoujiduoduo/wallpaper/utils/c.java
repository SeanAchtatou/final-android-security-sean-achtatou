package com.shoujiduoduo.wallpaper.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.d.a.b.a.n;
import com.d.a.b.c;
import com.d.a.b.d;
import com.shoujiduoduo.wallpaper.a.a;
import com.shoujiduoduo.wallpaper.a.k;
import com.shoujiduoduo.wallpaper.activity.NewMainActivity;
import com.shoujiduoduo.wallpaper.kernel.App;
import com.umeng.analytics.b;

public class c extends BaseAdapter {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final String f1865a = c.class.getSimpleName();
    /* access modifiers changed from: private */
    public k b;
    /* access modifiers changed from: private */
    public Context c;
    private int d = 12;
    private int e = 8;
    private com.d.a.b.c f;
    private int g = 0;
    private int h = 0;
    private int i = 0;
    private boolean j = true;
    private n k = new aw(this);

    private c() {
        new ax(this);
    }

    public c(Context context, k kVar, boolean z) {
        new ax(this);
        this.c = context;
        this.b = kVar;
        if (!z) {
            this.d = 99999999;
        }
        String c2 = b.c(this.c, "show_date_info_in_albumlist");
        if (c2 != null && c2.length() > 0) {
            this.j = c2.equalsIgnoreCase("true");
        }
        this.f = new c.a().a(false).b(true).c(true).a(Bitmap.Config.RGB_565).c();
    }

    public void a() {
        this.g = 0;
        this.h = 0;
        this.i = 0;
        if (this.j) {
            int i2 = 0;
            for (int i3 = 1; i3 < this.b.a(); i3++) {
                a b2 = this.b.b(i3);
                a b3 = this.b.b(i3 - 1);
                if (!(b2 == null || b3 == null || b2.f == null || b3.f == null || b2.f.length() <= 0 || b3.f.length() <= 0 || b2.f.equalsIgnoreCase(b3.f))) {
                    i2++;
                    if (i2 == 1) {
                        this.g = i3;
                    } else if (i2 == 4) {
                        this.h = i3;
                    } else if (i2 == 7) {
                        this.i = i3;
                        return;
                    }
                }
            }
        }
    }

    public void b() {
        this.c = null;
        this.b = null;
        this.f = null;
    }

    public int getCount() {
        if (this.b == null) {
            return 0;
        }
        int a2 = this.b.a();
        return a2 > this.e ? a2 + ((a2 - this.e) / this.d) + 1 : a2;
    }

    public Object getItem(int i2) {
        return null;
    }

    public long getItemId(int i2) {
        return 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int i2, View view, ViewGroup viewGroup) {
        if (i2 == getCount() - 1) {
            com.shoujiduoduo.wallpaper.kernel.b.a(f1865a, "reach bottom of the gridview.");
            if (!this.b.f() && this.b.e()) {
                com.shoujiduoduo.wallpaper.kernel.b.a(f1865a, "load more data.");
                this.b.d();
            }
        }
        com.shoujiduoduo.wallpaper.kernel.b.a(f1865a, "position = " + i2 + ", AD_START_POS = " + this.e + ", AD_LINE_SPACING = " + this.d);
        if (i2 < this.e || (i2 - this.e) % this.d != 0) {
            if (view == null || !"album_view".equalsIgnoreCase((String) view.getTag())) {
                view = LayoutInflater.from(this.c).inflate(f.c("R.layout.wallpaperdd_album_list_item"), viewGroup, false);
                view.setTag("album_view");
            }
            if (i2 >= this.e) {
                i2 = (i2 - ((i2 - this.e) / this.d)) - 1;
            }
            a b2 = this.b.b(i2);
            TextView textView = (TextView) view.findViewById(f.c("R.id.album_date_info"));
            textView.setVisibility(8);
            if (this.j) {
                if (i2 == 0) {
                    if (b2.f != null && b2.f.length() > 0) {
                        textView.setText("今日最新");
                        textView.setVisibility(0);
                    }
                } else if (i2 == this.g) {
                    textView.setText("一天前");
                    textView.setVisibility(0);
                } else if (i2 == this.h) {
                    textView.setText("三天前");
                    textView.setVisibility(0);
                } else if (i2 == this.i) {
                    textView.setText("一周前");
                    textView.setVisibility(0);
                }
            }
            ImageView imageView = (ImageView) view.findViewById(f.c("R.id.album_big_thumb"));
            ImageView imageView2 = (ImageView) view.findViewById(f.c("R.id.album_small_thumb1"));
            ImageView imageView3 = (ImageView) view.findViewById(f.c("R.id.album_small_thumb2"));
            ImageView imageView4 = (ImageView) view.findViewById(f.c("R.id.album_small_thumb3"));
            imageView.setImageDrawable(App.e);
            imageView2.setImageDrawable(App.e);
            imageView3.setImageDrawable(App.e);
            imageView4.setImageDrawable(App.e);
            ((TextView) view.findViewById(f.c("R.id.album_pic_count_text"))).setText(b2 == null ? "" : String.valueOf(b2.g) + "P");
            d.a().a(b2 == null ? null : b2.f1720a, imageView, this.f, this.k, new ay(this));
            d.a().a(b2 == null ? null : b2.b, imageView2, this.f, this.k, new ba(this));
            d.a().a(b2 == null ? null : b2.c, imageView3, this.f, this.k, new bb(this));
            d.a().a(b2 == null ? null : b2.d, imageView4, this.f, this.k, new bc(this));
            ((TextView) view.findViewById(f.c("R.id.album_name"))).setText(b2.h);
            TextView textView2 = (TextView) view.findViewById(f.c("R.id.album_uploader"));
            if (b2.j == null || b2.j.length() == 0) {
                textView2.setText("上传者: 壁纸多多");
            } else {
                textView2.setText("来源: " + b2.j);
            }
            TextView textView3 = (TextView) view.findViewById(f.c("R.id.album_intro"));
            if (b2.e == null) {
                b2.e = "";
            }
            b2.e = b2.e.trim();
            if (b2.e == null || b2.e.length() == 0) {
                textView3.setText("简介:  暂无。");
            } else {
                textView3.setText("简介:  " + b2.e);
            }
            view.setOnClickListener(new bd(this, b2.k, b2.j, b2.e));
            return view;
        }
        com.shoujiduoduo.wallpaper.kernel.b.a(f1865a, "ad position");
        if (NewMainActivity.b) {
            return b.a(this.c, view, (i2 - this.e) / this.d);
        }
        com.shoujiduoduo.wallpaper.kernel.b.a(f1865a, "getADView");
        return b.a(this.c, view);
    }
}
