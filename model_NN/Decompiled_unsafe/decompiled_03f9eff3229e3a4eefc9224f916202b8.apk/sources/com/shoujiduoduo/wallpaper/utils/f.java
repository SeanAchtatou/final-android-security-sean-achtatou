package com.shoujiduoduo.wallpaper.utils;

import android.app.Activity;
import android.app.WallpaperManager;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import com.baidu.mobads.interfaces.utils.IXAdSystemUtils;
import com.umeng.analytics.b;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.lang.reflect.Field;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

public class f {

    /* renamed from: a  reason: collision with root package name */
    public static boolean f1867a = true;
    public static boolean b = true;
    private static final String c = f.class.getSimpleName();
    private static Context d = null;
    private static String e = null;
    private static String f = null;
    private static String g = null;
    private static String h = null;
    private static String i = null;
    private static String j = null;
    private static String k = null;
    private static String l = null;
    private static String m;
    private static int n = 0;
    private static String o = null;

    public static int a(String str, String str2, String str3) {
        try {
            Class<?>[] classes = Class.forName(String.valueOf(str) + ".R").getClasses();
            Class<?> cls = null;
            int i2 = 0;
            while (true) {
                if (i2 >= classes.length) {
                    break;
                } else if (classes[i2].getName().split("\\$")[1].equals(str2)) {
                    cls = classes[i2];
                    break;
                } else {
                    i2++;
                }
            }
            if (cls != null) {
                return cls.getField(str3).getInt(cls);
            }
            return 0;
        } catch (ClassNotFoundException e2) {
            e2.printStackTrace();
            return 0;
        } catch (IllegalArgumentException e3) {
            e3.printStackTrace();
            return 0;
        } catch (SecurityException e4) {
            e4.printStackTrace();
            return 0;
        } catch (IllegalAccessException e5) {
            e5.printStackTrace();
            return 0;
        } catch (NoSuchFieldException e6) {
            e6.printStackTrace();
            return 0;
        }
    }

    public static File a(String str, Activity activity) {
        Cursor managedQuery = activity.managedQuery(Uri.parse(str), new String[]{"_data"}, null, null, null);
        int columnIndexOrThrow = managedQuery.getColumnIndexOrThrow("_data");
        managedQuery.moveToFirst();
        String string = managedQuery.getString(columnIndexOrThrow);
        if (Integer.parseInt(Build.VERSION.SDK) < 14) {
            managedQuery.close();
        }
        File file = new File(string);
        if (!file.isFile() || !file.exists()) {
            return null;
        }
        return file;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x009c, code lost:
        if (r1 != false) goto L_0x009e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String a() {
        /*
            java.lang.String r0 = com.shoujiduoduo.wallpaper.utils.f.h
            if (r0 != 0) goto L_0x00a0
            java.lang.String r0 = android.os.Environment.getExternalStorageState()
            java.lang.String r1 = "mounted"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x00a0
            java.io.File r0 = android.os.Environment.getExternalStorageDirectory()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r0 = r0.toString()
            java.lang.String r0 = java.lang.String.valueOf(r0)
            r1.<init>(r0)
            java.lang.String r0 = "/shoujiduoduo/Ring/wallpaper/"
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String r0 = r0.toString()
            java.lang.String r1 = com.shoujiduoduo.wallpaper.utils.f.c
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = com.shoujiduoduo.wallpaper.utils.f.c
            java.lang.String r3 = java.lang.String.valueOf(r3)
            r2.<init>(r3)
            java.lang.String r3 = ": soft dir = "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r0)
            java.lang.String r2 = r2.toString()
            com.shoujiduoduo.wallpaper.kernel.b.a(r1, r2)
            java.io.File r1 = new java.io.File
            r1.<init>(r0)
            boolean r2 = r1.isDirectory()
            java.lang.String r3 = com.shoujiduoduo.wallpaper.utils.f.c
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r5 = com.shoujiduoduo.wallpaper.utils.f.c
            java.lang.String r5 = java.lang.String.valueOf(r5)
            r4.<init>(r5)
            java.lang.String r5 = ": b1 = "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r5 = java.lang.Boolean.toString(r2)
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r4 = r4.toString()
            com.shoujiduoduo.wallpaper.kernel.b.a(r3, r4)
            if (r2 != 0) goto L_0x009e
            boolean r1 = r1.mkdirs()
            java.lang.String r2 = com.shoujiduoduo.wallpaper.utils.f.c
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = com.shoujiduoduo.wallpaper.utils.f.c
            java.lang.String r4 = java.lang.String.valueOf(r4)
            r3.<init>(r4)
            java.lang.String r4 = ": b2 = "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r4 = java.lang.Boolean.toString(r1)
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            com.shoujiduoduo.wallpaper.kernel.b.a(r2, r3)
            if (r1 == 0) goto L_0x00a0
        L_0x009e:
            com.shoujiduoduo.wallpaper.utils.f.h = r0
        L_0x00a0:
            java.lang.String r0 = com.shoujiduoduo.wallpaper.utils.f.h
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.wallpaper.utils.f.a():java.lang.String");
    }

    public static String a(NamedNodeMap namedNodeMap, String str) {
        String nodeValue;
        Node namedItem = namedNodeMap.getNamedItem(str);
        return (namedItem == null || (nodeValue = namedItem.getNodeValue()) == null) ? "" : nodeValue;
    }

    public static void a(Context context) {
        d = context;
        if (context != null) {
            m = d.getPackageName();
        }
    }

    public static synchronized boolean a(Bitmap bitmap, boolean z) {
        boolean z2;
        synchronized (f.class) {
            if (d == null || bitmap == null) {
                z2 = false;
            } else {
                WallpaperManager instance = WallpaperManager.getInstance(d);
                z2 = true;
                if (z) {
                    try {
                        instance.suggestDesiredDimensions(bitmap.getWidth(), bitmap.getHeight());
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }
                try {
                    instance.setBitmap(bitmap);
                    if ("xiaomi".equalsIgnoreCase(Build.BRAND)) {
                        instance.setBitmap(bitmap);
                    }
                } catch (Exception e3) {
                    e3.printStackTrace();
                    z2 = false;
                }
            }
        }
        return z2;
    }

    public static boolean a(String str) {
        if (d == null || str == null || str.length() == 0) {
            return false;
        }
        b.a(d, str);
        return true;
    }

    public static final int[] a(Context context, String str) {
        try {
            for (Field field : Class.forName(String.valueOf(context.getPackageName()) + ".R$styleable").getFields()) {
                if (field.getName().equals(str)) {
                    return (int[]) field.get(null);
                }
            }
        } catch (Throwable th) {
        }
        return null;
    }

    public static String b() {
        if (e == null) {
            try {
                g();
            } catch (Exception e2) {
            }
            if (e == null) {
                e = "DEFAULT_USER";
            }
        }
        return e;
    }

    public static synchronized boolean b(String str) {
        PackageInfo packageInfo;
        boolean z = false;
        synchronized (f.class) {
            try {
                packageInfo = d.getPackageManager().getPackageInfo(str, 0);
            } catch (PackageManager.NameNotFoundException e2) {
                e2.printStackTrace();
                packageInfo = null;
            }
            if (packageInfo != null) {
                z = true;
            }
        }
        return z;
    }

    public static int c(String str) {
        if (d == null) {
            com.shoujiduoduo.wallpaper.kernel.b.a("getResouceId", "mAppContext = null.");
            return 0;
        }
        String[] split = str.split("\\.");
        if (split.length != 3) {
            com.shoujiduoduo.wallpaper.kernel.b.a("getResouceId", "res.length = " + split.length);
            return 0;
        } else if (!split[0].equals("R")) {
            com.shoujiduoduo.wallpaper.kernel.b.a("getResouceId", "res[0] = " + split[0]);
            return 0;
        } else if (split[1].equals("id") || split[1].equals("drawable") || split[1].equals("layout") || split[1].equals("style") || split[1].equals("anim") || split[1].equals("string") || split[1].equals("styleable") || split[1].equals("menu")) {
            com.shoujiduoduo.wallpaper.kernel.b.a("getResouceId", "res[1] = " + split[1]);
            return a(m == null ? d.getPackageName() : m, split[1], split[2]);
        } else {
            com.shoujiduoduo.wallpaper.kernel.b.a("getResouceId", "failed, res[1] = " + split[1]);
            return 0;
        }
    }

    public static String c() {
        WifiManager wifiManager;
        if (!((k != null && k.length() != 0) || d == null || (wifiManager = (WifiManager) d.getSystemService((String) IXAdSystemUtils.NT_WIFI)) == null)) {
            k = wifiManager.getConnectionInfo().getMacAddress();
        }
        if (k == null || k.length() == 0) {
            k = "MAC_ADDR_UNAVAILABLE";
        }
        return k;
    }

    public static Context d() {
        return d;
    }

    public static NetworkInfo e() {
        if (d == null) {
            return null;
        }
        return ((ConnectivityManager) d.getSystemService("connectivity")).getActiveNetworkInfo();
    }

    public static synchronized String f() {
        String str;
        synchronized (f.class) {
            if (d == null) {
                str = null;
            } else {
                if (l == null) {
                    TelephonyManager telephonyManager = (TelephonyManager) d.getSystemService("phone");
                    if (telephonyManager == null) {
                        str = null;
                    } else {
                        l = telephonyManager.getDeviceId();
                    }
                }
                str = l;
            }
        }
        return str;
    }

    public static void g() {
        if (d != null) {
            String f2 = f();
            if (f2 == null) {
                String o2 = o();
                if (!o2.equals("")) {
                    e = o2;
                    return;
                }
                return;
            }
            e = f2;
        }
    }

    public static String h() {
        if (f == null) {
            String str = String.valueOf(Build.BRAND) + ">" + Build.PRODUCT + ">" + Build.MODEL;
            f = str;
            if (str == null) {
                f = "Unknown_Model";
            }
        }
        return f;
    }

    public static String i() {
        Bundle bundle;
        if (j == null) {
            String str = "unknown";
            try {
                PackageInfo packageInfo = d.getPackageManager().getPackageInfo(d.getPackageName(), 0);
                ApplicationInfo applicationInfo = d.getPackageManager().getApplicationInfo(d.getPackageName(), 128);
                if (!(applicationInfo == null || (bundle = applicationInfo.metaData) == null)) {
                    str = bundle.getString("UMENG_CHANNEL");
                    com.shoujiduoduo.wallpaper.kernel.b.a(c, "channel = " + str);
                }
                j = String.valueOf(d.getResources().getString(a(d.getPackageName(), "string", "wallpaperdd_app_version_prefix"))) + packageInfo.versionName + "_" + str + d.getResources().getString(a(d.getPackageName(), "string", "wallpaperdd_install_source_suffix"));
            } catch (PackageManager.NameNotFoundException e2) {
                e2.printStackTrace();
                return String.valueOf(d.getResources().getString(a(d.getPackageName(), "string", "wallpaperdd_app_version_prefix"))) + "0.0.0.0" + d.getResources().getString(a(d.getPackageName(), "string", "wallpaperdd_install_source_suffix"));
            }
        }
        com.shoujiduoduo.wallpaper.kernel.b.a(c, "mInstallSrc = " + j);
        return j;
    }

    public static String j() {
        if (i == null) {
            try {
                i = String.valueOf(d.getResources().getString(a(d.getPackageName(), "string", "wallpaperdd_app_version_prefix"))) + d.getPackageManager().getPackageInfo(d.getPackageName(), 0).versionName;
            } catch (PackageManager.NameNotFoundException e2) {
                e2.printStackTrace();
                return String.valueOf(d.getResources().getString(a(d.getPackageName(), "string", "wallpaperdd_app_version_prefix"))) + "0.0.0.0";
            }
        }
        return i;
    }

    public static int k() {
        if (n == 0) {
            try {
                PackageInfo packageInfo = d.getPackageManager().getPackageInfo(d.getPackageName(), 0);
                if (packageInfo != null) {
                    n = packageInfo.versionCode;
                }
            } catch (PackageManager.NameNotFoundException e2) {
                e2.printStackTrace();
            }
        }
        return n;
    }

    public static String l() {
        return d.getResources().getString(a(d.getPackageName(), "string", "wallpaperdd_english_name"));
    }

    public static synchronized boolean m() {
        boolean z = false;
        synchronized (f.class) {
            String i2 = i();
            if (i2 != null && ((i2.contains("_anzhuo") || i2.contains("_baidu") || i2.contains("_91ch")) && d != null)) {
                long currentTimeMillis = System.currentTimeMillis();
                if (currentTimeMillis - ag.a(d, "pref_first_start_time", currentTimeMillis) <= 7200000) {
                    try {
                        String str = d.getPackageManager().getPackageInfo(d.getPackageName(), 0).versionName;
                        String[] split = b.c(d, "no_beauty").split(";");
                        int i3 = 0;
                        while (true) {
                            if (i3 >= split.length) {
                                break;
                            } else if (str.equals(split[i3])) {
                                z = true;
                                break;
                            } else {
                                i3++;
                            }
                        }
                    } catch (PackageManager.NameNotFoundException e2) {
                    }
                }
            }
        }
        return z;
    }

    public static synchronized boolean n() {
        boolean z = false;
        synchronized (f.class) {
            String i2 = i();
            if (i2 != null && ((i2.contains("_anzhuo") || i2.contains("_baidu") || i2.contains("_91ch")) && d != null)) {
                long currentTimeMillis = System.currentTimeMillis();
                if (currentTimeMillis - ag.a(d, "pref_first_start_time", currentTimeMillis) <= 7200000) {
                    try {
                        String str = d.getPackageManager().getPackageInfo(d.getPackageName(), 0).versionName;
                        String[] split = b.c(d, "show_alert_when_click_ad").split(";");
                        int i3 = 0;
                        while (true) {
                            if (i3 >= split.length) {
                                break;
                            } else if (str.equals(split[i3])) {
                                z = true;
                                break;
                            } else {
                                i3++;
                            }
                        }
                    } catch (PackageManager.NameNotFoundException e2) {
                    }
                }
            }
        }
        return z;
    }

    private static String o() {
        String readLine;
        try {
            LineNumberReader lineNumberReader = new LineNumberReader(new InputStreamReader(Runtime.getRuntime().exec("cat /proc/cpuinfo").getInputStream()));
            for (int i2 = 1; i2 < 100 && (readLine = lineNumberReader.readLine()) != null; i2++) {
                if (readLine.indexOf("Serial") >= 0) {
                    return readLine.substring(readLine.indexOf(":") + 1, readLine.length()).trim();
                }
            }
            return null;
        } catch (IOException e2) {
            e2.printStackTrace();
            return null;
        }
    }
}
