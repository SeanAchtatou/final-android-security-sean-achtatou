package com.shoujiduoduo.wallpaper.utils;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import com.shoujiduoduo.wallpaper.activity.WallpaperActivity;

final class ao implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ai f1843a;
    private final /* synthetic */ int b;

    ao(ai aiVar, int i) {
        this.f1843a = aiVar;
        this.b = i;
    }

    public final void onClick(View view) {
        Intent intent = new Intent((Activity) this.f1843a.f1839a.f1838a, WallpaperActivity.class);
        intent.putExtra("listid", this.f1843a.f1839a.d.b());
        intent.putExtra("serialno", this.b);
        intent.putExtra("sort", this.f1843a.f1839a.d.g().toString());
        this.f1843a.f1839a.f1838a.startActivity(intent);
    }
}
