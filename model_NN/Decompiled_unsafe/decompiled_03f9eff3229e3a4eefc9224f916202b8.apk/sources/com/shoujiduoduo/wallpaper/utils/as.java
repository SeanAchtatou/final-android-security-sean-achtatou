package com.shoujiduoduo.wallpaper.utils;

import android.content.DialogInterface;
import android.view.View;
import com.qq.e.ads.nativ.NativeADDataRef;

final class as implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ NativeADDataRef f1846a;
    private final /* synthetic */ View b;

    as(ap apVar, NativeADDataRef nativeADDataRef, View view) {
        this.f1846a = nativeADDataRef;
        this.b = view;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f1846a.onClicked(this.b);
    }
}
