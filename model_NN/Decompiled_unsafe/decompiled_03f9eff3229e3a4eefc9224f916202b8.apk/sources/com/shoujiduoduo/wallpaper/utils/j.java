package com.shoujiduoduo.wallpaper.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import java.util.ArrayList;
import java.util.HashMap;

public class j extends PopupWindow {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Context f1871a;
    private ListView b;
    /* access modifiers changed from: private */
    public View c;
    private ArrayList d = new ArrayList();
    private AdapterView.OnItemClickListener e = new bf(this);

    public j(Context context, boolean z) {
        super(context);
        this.f1871a = context;
        this.c = ((LayoutInflater) context.getSystemService("layout_inflater")).inflate(f.c("R.layout.wallpaperdd_menu"), (ViewGroup) null);
        this.b = (ListView) this.c.findViewById(f.c("R.id.menu_list"));
        HashMap hashMap = new HashMap();
        hashMap.put("PIC", Integer.valueOf(f.c("R.drawable.wallpaperdd_icon_auto_setting")));
        hashMap.put("TEXT", "自动设置壁纸");
        this.d.add(hashMap);
        HashMap hashMap2 = new HashMap();
        hashMap2.put("PIC", Integer.valueOf(f.c("R.drawable.wallpaperdd_icon_menu_clear_cache")));
        hashMap2.put("TEXT", "清除图片缓存");
        this.d.add(hashMap2);
        if (!f.i().contains("xiaomi")) {
            HashMap hashMap3 = new HashMap();
            hashMap3.put("PIC", Integer.valueOf(f.c("R.drawable.wallpaperdd_icon_praise")));
            hashMap3.put("TEXT", "五星支持我们");
            this.d.add(hashMap3);
        }
        HashMap hashMap4 = new HashMap();
        hashMap4.put("PIC", Integer.valueOf(f.c("R.drawable.wallpaperdd_icon_menu_feedback")));
        hashMap4.put("TEXT", "用户反馈");
        this.d.add(hashMap4);
        HashMap hashMap5 = new HashMap();
        hashMap5.put("PIC", Integer.valueOf(f.c("R.drawable.wallpaperdd_icon_menu_aboutinfo")));
        hashMap5.put("TEXT", "帮助关于");
        this.d.add(hashMap5);
        this.b.setAdapter((ListAdapter) new bk(this, context, this.d, f.c("R.layout.wallpaperdd_menu_item"), new String[]{"PIC", "TEXT"}, new int[]{f.c("R.id.menu_logos"), f.c("R.id.menu_item_text")}));
        this.b.setItemsCanFocus(false);
        this.b.setChoiceMode(2);
        this.b.setOnItemClickListener(this.e);
        setContentView(this.c);
        setWidth(-2);
        setHeight(-2);
        setFocusable(true);
        if (z) {
            setAnimationStyle(f.c("R.style.wallpaperdd_menuPopupStyle"));
        }
        setBackgroundDrawable(new ColorDrawable(-1342177280));
        this.c.setOnTouchListener(new bg(this));
    }

    static /* synthetic */ void c(j jVar) {
        boolean[] zArr = new boolean[5];
        zArr[0] = true;
        zArr[1] = true;
        zArr[2] = true;
        new AlertDialog.Builder(jVar.f1871a).setTitle("选择要清除的缓存").setMultiChoiceItems(h.f1869a, zArr, new bh(jVar, zArr)).setPositiveButton("确定", new bi(jVar, zArr)).setNegativeButton("取消", new bj(jVar)).show();
    }
}
