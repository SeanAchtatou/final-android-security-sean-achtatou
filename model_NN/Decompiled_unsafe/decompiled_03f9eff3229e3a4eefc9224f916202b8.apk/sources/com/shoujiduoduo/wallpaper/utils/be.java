package com.shoujiduoduo.wallpaper.utils;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.widget.Toast;
import com.shoujiduoduo.wallpaper.activity.WallpaperActivity;

final class be extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ bd f1854a;

    be(bd bdVar) {
        this.f1854a = bdVar;
    }

    public final void handleMessage(Message message) {
        if (this.f1854a.f.b != null) {
            switch (message.what) {
                case 14001:
                    int i = message.arg1;
                    if (i == 31) {
                        return;
                    }
                    if (i == 0 || i == 32) {
                        if (this.f1854a.d != null) {
                            this.f1854a.d.dismiss();
                            Intent intent = new Intent(this.f1854a.f.c, WallpaperActivity.class);
                            intent.putExtra("listid", this.f1854a.f1853a);
                            intent.putExtra("serialno", 0);
                            intent.putExtra("uploader", this.f1854a.b);
                            intent.putExtra("intro", this.f1854a.c);
                            this.f1854a.f.c.startActivity(intent);
                            return;
                        }
                        return;
                    } else if (i == 1) {
                        this.f1854a.d.dismiss();
                        Toast.makeText(this.f1854a.f.c, "加载套图列表失败，请检查您的网络连接。", 1).show();
                        return;
                    } else if (i == 2) {
                        this.f1854a.d.dismiss();
                        Toast.makeText(this.f1854a.f.c, "加载套图列表失败，请检查您的网络连接。", 1).show();
                        return;
                    } else {
                        return;
                    }
                default:
                    return;
            }
        }
    }
}
