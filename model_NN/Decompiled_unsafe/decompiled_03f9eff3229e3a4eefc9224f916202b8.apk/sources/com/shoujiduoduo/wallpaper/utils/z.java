package com.shoujiduoduo.wallpaper.utils;

import android.os.Handler;
import android.os.Message;
import android.widget.Toast;
import com.d.a.b.a.b;
import com.d.a.b.d;
import com.shoujiduoduo.wallpaper.a.h;
import java.io.File;

final class z extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ af f1882a;

    z(af afVar) {
        this.f1882a = afVar;
    }

    public final void handleMessage(Message message) {
        File a2;
        switch (message.what) {
            case 240:
                if (this.f1882a.b == ((String) message.obj) && this.f1882a.d != null) {
                    Toast.makeText(this.f1882a.d, "您取消了刚才的壁纸设置任务", 0).show();
                    return;
                }
                return;
            case 241:
                if (this.f1882a.b == ((String) message.obj) && this.f1882a.d != null) {
                    Toast.makeText(this.f1882a.d, "由于图片加载失败，未能成功设置壁纸。", 0).show();
                    return;
                }
                return;
            case 242:
                ab abVar = (ab) message.obj;
                if (abVar.f1833a == this.f1882a.b) {
                    if (abVar.c > 0) {
                        File file = new File(String.valueOf(f.a()) + "favorate/" + abVar.c + ".jpg");
                        if (!file.exists() && (a2 = b.a(abVar.f1833a, d.a().d())) != null) {
                            m.a(a2, file);
                            new aj(this.f1882a.d, file);
                        }
                    }
                    new ac(this.f1882a, abVar.b, abVar.f1833a, abVar.c).start();
                    return;
                }
                return;
            case 243:
                String str = (String) message.obj;
                h.d().b(message.arg1);
                if (this.f1882a.b == str && this.f1882a.d != null) {
                    Toast.makeText(this.f1882a.d, "设置壁纸成功。", 0).show();
                    return;
                }
                return;
            case 244:
                if (this.f1882a.b == ((String) message.obj) && this.f1882a.d != null) {
                    Toast.makeText(this.f1882a.d, "壁纸设置失败。", 0).show();
                    return;
                }
                return;
            default:
                return;
        }
    }
}
