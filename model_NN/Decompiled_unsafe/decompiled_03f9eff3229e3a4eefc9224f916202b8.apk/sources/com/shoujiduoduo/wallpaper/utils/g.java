package com.shoujiduoduo.wallpaper.utils;

import android.content.Intent;
import android.view.View;
import com.shoujiduoduo.wallpaper.activity.WallpaperActivity;

final class g implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ t f1868a;
    private final /* synthetic */ int b;

    g(t tVar, int i) {
        this.f1868a = tVar;
        this.b = i;
    }

    public final void onClick(View view) {
        t.c(this.f1868a);
        if (this.f1868a.c != null) {
            Intent intent = new Intent(this.f1868a.c, WallpaperActivity.class);
            intent.putExtra("listid", this.f1868a.b.b());
            intent.putExtra("serialno", this.b << 1);
            intent.putExtra("sort", this.f1868a.b.g().toString());
            this.f1868a.c.startActivity(intent);
        }
    }
}
