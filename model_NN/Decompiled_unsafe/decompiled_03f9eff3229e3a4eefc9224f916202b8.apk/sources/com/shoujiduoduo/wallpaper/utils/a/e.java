package com.shoujiduoduo.wallpaper.utils.a;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.webkit.MimeTypeMap;
import com.shoujiduoduo.wallpaper.utils.f;
import java.io.File;

public class e extends b {
    public static Intent a(Uri uri, String str) {
        int lastIndexOf;
        if (uri.getScheme().equals("file") && (lastIndexOf = uri.getPath().lastIndexOf(46)) != -1) {
            str = MimeTypeMap.getSingleton().getMimeTypeFromExtension(uri.getPath().substring(lastIndexOf + 1).toLowerCase());
        }
        Intent intent = new Intent("android.intent.action.ATTACH_DATA");
        intent.setDataAndType(uri, str);
        intent.putExtra("mimeType", str);
        return intent;
    }

    public void a(Context context, String str) {
        super.a(context, "samsung");
    }

    public boolean a() {
        try {
            return "samsung".equalsIgnoreCase(Build.BRAND);
        } catch (Exception e) {
        }
    }

    public boolean a(String str) {
        try {
            Intent a2 = a(Uri.fromFile(new File(str)), "image/jpeg");
            a2.addFlags(0);
            this.b.startActivity(Intent.createChooser(a2, this.b.getResources().getString(f.c("R.string.wallpaperdd_set_lockscreen"))));
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
