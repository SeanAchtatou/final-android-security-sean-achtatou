package com.shoujiduoduo.wallpaper.utils;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.AbsoluteLayout;
import android.widget.ImageView;

public class LocalPicView extends AbsoluteLayout {

    /* renamed from: a  reason: collision with root package name */
    private Context f1824a;
    private ImageView b;
    private int c = 0;
    private int d = 0;
    private int e = 0;

    static {
        LocalPicView.class.getSimpleName();
    }

    public LocalPicView(Context context) {
        super(context);
        this.f1824a = context;
        a();
    }

    public LocalPicView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f1824a = context;
        a();
    }

    public LocalPicView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f1824a = context;
        a();
    }

    private void a() {
        this.b = new ImageView(this.f1824a);
        this.b.setLayoutParams(new AbsoluteLayout.LayoutParams(-1, -1, 0, 0));
        this.b.setScaleType(ImageView.ScaleType.CENTER_CROP);
        this.b.setBackgroundColor(-1118482);
        addView(this.b);
    }

    public void a(int i, int i2, int i3) {
        this.c = i;
        this.d = i2;
        this.e = i3;
        this.b.layout(this.c, 0, this.c + this.d, this.e);
    }

    public ImageView getImageView() {
        return this.b;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        if (this.d > 0 && this.e > 0) {
            this.b.layout(this.c, 0, this.c + this.d, this.e);
        }
    }
}
