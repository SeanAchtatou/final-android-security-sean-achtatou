package com.shoujiduoduo.wallpaper.utils;

import android.content.Intent;
import android.view.View;
import com.shoujiduoduo.wallpaper.activity.BdImgActivity;
import com.umeng.analytics.b;
import java.net.URLEncoder;

final class d implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ t f1866a;

    d(t tVar) {
        this.f1866a = tVar;
    }

    public final void onClick(View view) {
        String c = b.c(this.f1866a.c, "wallpaper_bdimg_search_url");
        if (c != null && c.length() > 0) {
            String b = this.f1866a.g;
            if (!this.f1866a.g.contains("壁纸")) {
                b = String.valueOf(b) + " 壁纸";
            }
            String format = String.format(c, URLEncoder.encode(b));
            com.shoujiduoduo.wallpaper.kernel.b.a(t.f1878a, "searchURL = " + format);
            q.a(this.f1866a.g);
            Intent intent = new Intent(this.f1866a.c, BdImgActivity.class);
            intent.putExtra("url", format);
            intent.putExtra("keyword", this.f1866a.g);
            this.f1866a.c.startActivity(intent);
        }
    }
}
