package com.shoujiduoduo.wallpaper.utils;

import android.graphics.Bitmap;
import java.io.IOException;

final class ac extends Thread {

    /* renamed from: a  reason: collision with root package name */
    private Bitmap f1834a;
    private String b;
    private int c;
    private /* synthetic */ af d;

    public ac(af afVar, Bitmap bitmap, String str, int i) {
        this.d = afVar;
        this.f1834a = bitmap;
        this.b = str;
        this.c = i;
    }

    public final void run() {
        boolean z = true;
        try {
            this.d.f1836a.suggestDesiredDimensions(this.f1834a.getWidth(), this.f1834a.getHeight());
            this.d.f1836a.setBitmap(this.f1834a);
        } catch (IOException e) {
            e.printStackTrace();
            z = false;
        }
        if (z) {
            this.d.f.sendMessage(this.d.f.obtainMessage(243, this.c, 0, this.b));
        } else {
            this.d.f.sendMessage(this.d.f.obtainMessage(244, this.b));
        }
        if (this.f1834a != null) {
            this.f1834a = null;
        }
    }
}
