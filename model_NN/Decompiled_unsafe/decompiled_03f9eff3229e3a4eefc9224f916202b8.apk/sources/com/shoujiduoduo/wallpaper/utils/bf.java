package com.shoujiduoduo.wallpaper.utils;

import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;
import com.shoujiduoduo.wallpaper.activity.WallpaperAboutActivity;
import com.umeng.fb.k;
import java.util.Map;

final class bf implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ j f1855a;

    bf(j jVar) {
        this.f1855a = jVar;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        String str = (String) ((Map) adapterView.getItemAtPosition(i)).get("TEXT");
        if (str == "自动设置壁纸") {
            Toast.makeText(this.f1855a.f1871a, "请安装壁纸多多", 0).show();
        } else if (str == "用户反馈") {
            new k(this.f1855a.f1871a).e();
        } else if (str == "帮助关于") {
            this.f1855a.f1871a.startActivity(new Intent(this.f1855a.f1871a, WallpaperAboutActivity.class));
        } else if (str == "五星支持我们") {
            try {
                this.f1855a.f1871a.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=com.shoujiduoduo.wallpaper")));
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(this.f1855a.f1871a, "未找到合适的应用商店", 0).show();
            }
        } else if (str == "清除图片缓存") {
            j.c(this.f1855a);
        }
        this.f1855a.dismiss();
    }
}
