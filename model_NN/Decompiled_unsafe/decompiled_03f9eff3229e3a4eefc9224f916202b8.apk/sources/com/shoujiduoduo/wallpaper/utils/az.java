package com.shoujiduoduo.wallpaper.utils;

import android.app.Activity;
import android.content.Context;
import com.qq.e.ads.nativ.NativeAD;
import com.qq.e.ads.nativ.NativeADDataRef;
import java.util.List;

public class az {

    /* renamed from: a  reason: collision with root package name */
    private static Context f1850a = null;
    private static az b = null;
    private static NativeAD c;
    /* access modifiers changed from: private */
    public static List d;

    private az() {
    }

    private az(Context context) {
        f1850a = context;
    }

    public static az a(Context context) {
        if (b == null) {
            b = new az(context);
        }
        return b;
    }

    public NativeADDataRef a(int i) {
        if (d == null || d.size() <= i) {
            return null;
        }
        return (NativeADDataRef) d.get(i);
    }

    public void a() {
        f1850a = null;
        b = null;
        c = null;
        d = null;
    }

    public void b() {
        NativeAD nativeAD = new NativeAD((Activity) f1850a, "1101336966", "1060808460679917", new ar(this));
        c = nativeAD;
        nativeAD.loadAD(2);
    }

    public void c() {
        if (c != null) {
            c.loadAD(2);
        }
    }

    public int d() {
        if (d != null) {
            return d.size();
        }
        return 0;
    }
}
