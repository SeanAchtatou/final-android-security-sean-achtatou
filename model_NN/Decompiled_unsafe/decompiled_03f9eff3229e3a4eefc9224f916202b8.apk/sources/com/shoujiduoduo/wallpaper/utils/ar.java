package com.shoujiduoduo.wallpaper.utils;

import com.qq.e.ads.nativ.NativeAD;
import com.qq.e.ads.nativ.NativeADDataRef;
import com.shoujiduoduo.wallpaper.kernel.b;
import java.util.List;

final class ar implements NativeAD.NativeAdListener {
    ar(az azVar) {
    }

    public final void onADLoaded(List list) {
        int i = 0;
        if (az.d == null) {
            az.d = list;
            b.a("gdt", "get " + az.d.size() + " ad initially.");
            while (true) {
                int i2 = i;
                if (i2 < az.d.size()) {
                    b.a("gdt", "i = " + i2 + ", title = " + ((NativeADDataRef) az.d.get(i2)).getTitle());
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        } else {
            az.d.addAll(list);
            b.a("gdt", "append " + list.size() + "ad, now total " + az.d.size() + "ad.");
            while (true) {
                int i3 = i;
                if (i3 < az.d.size()) {
                    b.a("gdt", "i = " + i3 + ", title = " + ((NativeADDataRef) az.d.get(i3)).getTitle());
                    i = i3 + 1;
                } else {
                    return;
                }
            }
        }
    }

    public final void onADStatusChanged(NativeADDataRef nativeADDataRef) {
    }

    public final void onNoAD(int i) {
        b.a("gdt", "onGDTNativeAdFail");
    }
}
