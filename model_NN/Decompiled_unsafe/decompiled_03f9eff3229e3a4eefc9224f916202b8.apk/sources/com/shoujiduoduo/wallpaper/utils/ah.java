package com.shoujiduoduo.wallpaper.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import com.d.a.b.a.n;
import com.d.a.b.c;
import com.shoujiduoduo.wallpaper.a.g;
import com.shoujiduoduo.wallpaper.a.k;
import com.shoujiduoduo.wallpaper.a.m;
import com.shoujiduoduo.wallpaper.kernel.b;

public class ah extends PopupWindow implements g {
    /* access modifiers changed from: private */
    public static final String i = ah.class.getSimpleName();
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Context f1838a;
    private View b;
    /* access modifiers changed from: private */
    public ListView c;
    /* access modifiers changed from: private */
    public k d;
    /* access modifiers changed from: private */
    public ai e;
    /* access modifiers changed from: private */
    public ProgressBar f;
    /* access modifiers changed from: private */
    public c g = new c.a().a(false).b(true).c(true).a(Bitmap.Config.RGB_565).c();
    /* access modifiers changed from: private */
    public n h = new ae(this);
    private Handler j = new al(this);

    public ah(Context context, String str, int i2) {
        super(context);
        this.f1838a = context;
        this.b = ((LayoutInflater) context.getSystemService("layout_inflater")).inflate(f.c("R.layout.wallpaperdd_popup_similar_image_panel"), (ViewGroup) null);
        setContentView(this.b);
        setWidth(-2);
        setHeight(1920 - i2);
        setFocusable(true);
        setBackgroundDrawable(this.f1838a.getResources().getDrawable(f.c("R.drawable.wallpaperdd_duoduo_translucent")));
        this.b.setOnTouchListener(new am(this));
        this.c = (ListView) this.b.findViewById(f.c("R.id.similar_image_listview"));
        this.d = new k(999999998, str);
        this.d.a("similar_pic");
        this.d.a(this);
        m.b().a(this.d);
        this.f = (ProgressBar) this.b.findViewById(f.c("R.id.similar_image_list_loading_progress"));
        this.e = new ai(this);
        this.c.setAdapter((ListAdapter) this.e);
        if (this.d.a() == 0) {
            this.f.setVisibility(0);
            this.c.setVisibility(4);
            this.d.d();
            return;
        }
        this.f.setVisibility(4);
        this.c.setVisibility(0);
    }

    public k a() {
        return this.d;
    }

    public void a(k kVar, int i2) {
        Message obtainMessage = this.j.obtainMessage(3577, i2, 0);
        b.a(i, "CategoryListActivity:onListUpdate");
        this.j.sendMessage(obtainMessage);
    }

    public void a(String str) {
        this.d = new k(999999998, str);
        this.d.a(this);
        m.b().a(this.d);
        if (this.d.a() == 0) {
            this.f.setVisibility(0);
            this.c.setVisibility(4);
            this.d.d();
        } else {
            this.f.setVisibility(4);
            this.c.setVisibility(0);
        }
        this.e.notifyDataSetChanged();
    }
}
