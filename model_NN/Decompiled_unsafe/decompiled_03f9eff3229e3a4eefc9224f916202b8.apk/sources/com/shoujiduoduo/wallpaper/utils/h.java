package com.shoujiduoduo.wallpaper.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;
import com.d.a.b.d;
import java.io.File;

public class h extends AsyncTask {

    /* renamed from: a  reason: collision with root package name */
    public static final String[] f1869a = {"缩略图和临时图片", "自动更换壁纸缓存", "每日启动画面", "分享图片", "收藏图片"};
    private boolean[] b = null;
    private Context c = null;
    private ProgressDialog d = null;

    public h(Context context, boolean[] zArr) {
        this.b = zArr;
        this.c = context;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Boolean doInBackground(Void... voidArr) {
        if (this.b[0]) {
            publishProgress(0);
            d.a().e();
        }
        if (this.b[1]) {
            publishProgress(1);
        }
        if (this.b[2]) {
            publishProgress(2);
        }
        if (this.b[3]) {
            publishProgress(3);
            m.a(new File(String.valueOf(f.a()) + "share/"));
        }
        if (this.b[4]) {
            publishProgress(4);
            m.a(new File(String.valueOf(f.a()) + "favorate/"));
        }
        return Boolean.TRUE;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(Boolean bool) {
        this.d.dismiss();
        Toast.makeText(this.c, "缓存清理已完成。", 0).show();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onProgressUpdate(Integer... numArr) {
        switch (numArr[0].intValue()) {
            case 0:
                this.d.setMessage("正在清理缩略图和临时图片...");
                return;
            case 1:
                this.d.setMessage("正在清理自动更换的壁纸...");
                return;
            case 2:
                this.d.setMessage("正在清理每日启动画面...");
                return;
            case 3:
                this.d.setMessage("正在清理分享图片...");
                return;
            case 4:
                this.d.setMessage("正在清理收藏图片...");
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        this.d = new ProgressDialog(this.c);
        this.d.setCancelable(false);
        this.d.setIndeterminate(false);
        this.d.setTitle("清理缓存");
        this.d.setMessage("准备中...");
        this.d.show();
    }
}
