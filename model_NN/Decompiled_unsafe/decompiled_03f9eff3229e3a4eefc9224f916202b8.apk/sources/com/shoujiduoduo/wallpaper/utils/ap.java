package com.shoujiduoduo.wallpaper.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.view.View;
import com.qq.e.ads.nativ.NativeADDataRef;

final class ap implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ Context f1844a;
    private final /* synthetic */ NativeADDataRef b;

    ap(Context context, NativeADDataRef nativeADDataRef) {
        this.f1844a = context;
        this.b = nativeADDataRef;
    }

    public final void onClick(View view) {
        if (f.n()) {
            new AlertDialog.Builder(this.f1844a).setTitle(this.f1844a.getResources().getString(f.c("R.string.wallpaperdd_alert_dialog_header"))).setMessage("确认要打开“" + this.b.getTitle() + "”吗？").setIcon(17301543).setPositiveButton("确定", new as(this, this.b, view)).setNegativeButton("取消", new au(this)).show();
        } else {
            this.b.onClicked(view);
        }
    }
}
