package com.shoujiduoduo.wallpaper.utils;

import android.content.Intent;
import android.view.View;
import com.shoujiduoduo.wallpaper.activity.WallpaperActivity;

final class l implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ t f1872a;
    private final /* synthetic */ int b;

    l(t tVar, int i) {
        this.f1872a = tVar;
        this.b = i;
    }

    public final void onClick(View view) {
        t.c(this.f1872a);
        if (this.f1872a.c != null) {
            Intent intent = new Intent(this.f1872a.c, WallpaperActivity.class);
            intent.putExtra("listid", this.f1872a.b.b());
            intent.putExtra("serialno", (this.b << 1) + 1);
            intent.putExtra("sort", this.f1872a.b.g().toString());
            this.f1872a.c.startActivity(intent);
        }
    }
}
