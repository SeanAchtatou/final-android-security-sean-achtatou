package com.shoujiduoduo.wallpaper.utils;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.animation.Interpolator;
import java.lang.reflect.Field;

public class FixViewPager extends ViewPager {

    /* renamed from: a  reason: collision with root package name */
    private n f1822a = null;

    public FixViewPager(Context context) {
        super(context);
        a();
    }

    public FixViewPager(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a();
    }

    private void a() {
        try {
            Field declaredField = ViewPager.class.getDeclaredField("mScroller");
            declaredField.setAccessible(true);
            Field declaredField2 = ViewPager.class.getDeclaredField("sInterpolator");
            declaredField2.setAccessible(true);
            this.f1822a = new n(this, getContext(), (Interpolator) declaredField2.get(null));
            declaredField.set(this, this.f1822a);
        } catch (Exception e) {
        }
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        try {
            return super.onInterceptTouchEvent(motionEvent);
        } catch (IllegalArgumentException e) {
            return false;
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        try {
            return super.onTouchEvent(motionEvent);
        } catch (IllegalArgumentException e) {
            return false;
        }
    }

    public void setScrollDurationFactor(double d) {
        this.f1822a.a(d);
    }
}
