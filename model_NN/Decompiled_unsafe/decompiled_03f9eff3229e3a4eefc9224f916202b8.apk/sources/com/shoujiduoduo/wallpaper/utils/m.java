package com.shoujiduoduo.wallpaper.utils;

import android.content.Context;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class m {

    /* renamed from: a  reason: collision with root package name */
    private static Context f1873a = null;

    public static void a(Context context) {
        f1873a = context;
    }

    public static void a(File file) {
        if (!file.exists()) {
            return;
        }
        if (file.isDirectory()) {
            for (File a2 : file.listFiles()) {
                a(a2);
            }
            return;
        }
        file.delete();
    }

    /* JADX WARN: Type inference failed for: r2v0 */
    /* JADX WARN: Type inference failed for: r2v1, types: [java.io.FileOutputStream] */
    /* JADX WARN: Type inference failed for: r2v2, types: [java.io.FileInputStream] */
    /* JADX WARN: Type inference failed for: r2v4 */
    /* JADX WARN: Type inference failed for: r2v5 */
    /* JADX WARN: Type inference failed for: r2v7 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0031 A[SYNTHETIC, Splitter:B:22:0x0031] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0036 A[SYNTHETIC, Splitter:B:25:0x0036] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0040 A[SYNTHETIC, Splitter:B:31:0x0040] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0045 A[SYNTHETIC, Splitter:B:34:0x0045] */
    /* JADX WARNING: Removed duplicated region for block: B:50:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean a(java.io.File r6, java.io.File r7) {
        /*
            r2 = 0
            r0 = 0
            java.lang.System.currentTimeMillis()
            boolean r1 = r6.exists()
            if (r1 == 0) goto L_0x0027
            r1 = 2048(0x800, float:2.87E-42)
            byte[] r4 = new byte[r1]
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ IOException -> 0x0058, all -> 0x003c }
            r3.<init>(r6)     // Catch:{ IOException -> 0x0058, all -> 0x003c }
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x005b, all -> 0x0053 }
            r1.<init>(r7)     // Catch:{ IOException -> 0x005b, all -> 0x0053 }
        L_0x0019:
            int r2 = r3.read(r4)     // Catch:{ IOException -> 0x002d, all -> 0x0055 }
            r5 = -1
            if (r2 != r5) goto L_0x0028
            r3.close()     // Catch:{ IOException -> 0x0049 }
        L_0x0023:
            r1.close()     // Catch:{ IOException -> 0x004b }
        L_0x0026:
            r0 = 1
        L_0x0027:
            return r0
        L_0x0028:
            r5 = 0
            r1.write(r4, r5, r2)     // Catch:{ IOException -> 0x002d, all -> 0x0055 }
            goto L_0x0019
        L_0x002d:
            r2 = move-exception
            r2 = r3
        L_0x002f:
            if (r2 == 0) goto L_0x0034
            r2.close()     // Catch:{ IOException -> 0x004d }
        L_0x0034:
            if (r1 == 0) goto L_0x0027
            r1.close()     // Catch:{ IOException -> 0x003a }
            goto L_0x0027
        L_0x003a:
            r1 = move-exception
            goto L_0x0027
        L_0x003c:
            r0 = move-exception
            r3 = r2
        L_0x003e:
            if (r3 == 0) goto L_0x0043
            r3.close()     // Catch:{ IOException -> 0x004f }
        L_0x0043:
            if (r2 == 0) goto L_0x0048
            r2.close()     // Catch:{ IOException -> 0x0051 }
        L_0x0048:
            throw r0
        L_0x0049:
            r0 = move-exception
            goto L_0x0023
        L_0x004b:
            r0 = move-exception
            goto L_0x0026
        L_0x004d:
            r2 = move-exception
            goto L_0x0034
        L_0x004f:
            r1 = move-exception
            goto L_0x0043
        L_0x0051:
            r1 = move-exception
            goto L_0x0048
        L_0x0053:
            r0 = move-exception
            goto L_0x003e
        L_0x0055:
            r0 = move-exception
            r2 = r1
            goto L_0x003e
        L_0x0058:
            r1 = move-exception
            r1 = r2
            goto L_0x002f
        L_0x005b:
            r1 = move-exception
            r1 = r2
            r2 = r3
            goto L_0x002f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.wallpaper.utils.m.a(java.io.File, java.io.File):boolean");
    }

    public static boolean a(String str, String str2) {
        try {
            FileWriter fileWriter = new FileWriter(str);
            fileWriter.write(str2);
            fileWriter.close();
            return true;
        } catch (IOException e) {
            return false;
        }
    }
}
