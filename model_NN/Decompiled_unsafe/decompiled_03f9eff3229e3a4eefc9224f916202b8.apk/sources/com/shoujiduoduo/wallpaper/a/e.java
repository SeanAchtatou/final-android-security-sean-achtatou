package com.shoujiduoduo.wallpaper.a;

import android.os.Handler;
import com.shoujiduoduo.wallpaper.kernel.b;
import com.shoujiduoduo.wallpaper.utils.q;
import java.io.ByteArrayInputStream;
import java.util.ArrayList;

public class e {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final String f1722a = e.class.getSimpleName();
    /* access modifiers changed from: private */
    public ArrayList b;
    /* access modifiers changed from: private */
    public p c;
    /* access modifiers changed from: private */
    public f d = null;
    /* access modifiers changed from: private */
    public boolean e = false;
    /* access modifiers changed from: private */
    public boolean f = false;
    /* access modifiers changed from: private */
    public Handler g = new q(this);

    private e() {
    }

    public e(String str) {
        this.c = new p((str == null || str.length() == 0) ? "hotkey.tmp" : str);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:?, code lost:
        return null;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.ArrayList a(java.io.InputStream r6) {
        /*
            r0 = 0
            javax.xml.parsers.DocumentBuilderFactory r1 = javax.xml.parsers.DocumentBuilderFactory.newInstance()     // Catch:{ IOException -> 0x001e, SAXException -> 0x0030, ParserConfigurationException -> 0x0066, ArrayIndexOutOfBoundsException -> 0x0064, DOMException -> 0x0062, all -> 0x0060 }
            javax.xml.parsers.DocumentBuilder r1 = r1.newDocumentBuilder()     // Catch:{ IOException -> 0x001e, SAXException -> 0x0030, ParserConfigurationException -> 0x0066, ArrayIndexOutOfBoundsException -> 0x0064, DOMException -> 0x0062, all -> 0x0060 }
            org.w3c.dom.Document r1 = r1.parse(r6)     // Catch:{ IOException -> 0x001e, SAXException -> 0x0030, ParserConfigurationException -> 0x0066, ArrayIndexOutOfBoundsException -> 0x0064, DOMException -> 0x0062, all -> 0x0060 }
            if (r1 != 0) goto L_0x0010
        L_0x000f:
            return r0
        L_0x0010:
            org.w3c.dom.Element r1 = r1.getDocumentElement()     // Catch:{ IOException -> 0x001e, SAXException -> 0x0030, ParserConfigurationException -> 0x0066, ArrayIndexOutOfBoundsException -> 0x0064, DOMException -> 0x0062, all -> 0x0060 }
            if (r1 != 0) goto L_0x0020
            java.lang.String r1 = com.shoujiduoduo.wallpaper.a.e.f1722a     // Catch:{ IOException -> 0x001e, SAXException -> 0x0030, ParserConfigurationException -> 0x0066, ArrayIndexOutOfBoundsException -> 0x0064, DOMException -> 0x0062, all -> 0x0060 }
            java.lang.String r2 = "cannot find root node"
            com.shoujiduoduo.wallpaper.kernel.b.a(r1, r2)     // Catch:{ IOException -> 0x001e, SAXException -> 0x0030, ParserConfigurationException -> 0x0066, ArrayIndexOutOfBoundsException -> 0x0064, DOMException -> 0x0062, all -> 0x0060 }
            goto L_0x000f
        L_0x001e:
            r1 = move-exception
            goto L_0x000f
        L_0x0020:
            java.lang.String r2 = "key"
            org.w3c.dom.NodeList r3 = r1.getElementsByTagName(r2)     // Catch:{ IOException -> 0x001e, SAXException -> 0x0030, ParserConfigurationException -> 0x0066, ArrayIndexOutOfBoundsException -> 0x0064, DOMException -> 0x0062, all -> 0x0060 }
            if (r3 != 0) goto L_0x0032
            java.lang.String r1 = com.shoujiduoduo.wallpaper.a.e.f1722a     // Catch:{ IOException -> 0x001e, SAXException -> 0x0030, ParserConfigurationException -> 0x0066, ArrayIndexOutOfBoundsException -> 0x0064, DOMException -> 0x0062, all -> 0x0060 }
            java.lang.String r2 = "cannot find node named \"key\""
            com.shoujiduoduo.wallpaper.kernel.b.a(r1, r2)     // Catch:{ IOException -> 0x001e, SAXException -> 0x0030, ParserConfigurationException -> 0x0066, ArrayIndexOutOfBoundsException -> 0x0064, DOMException -> 0x0062, all -> 0x0060 }
            goto L_0x000f
        L_0x0030:
            r1 = move-exception
            goto L_0x000f
        L_0x0032:
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ IOException -> 0x001e, SAXException -> 0x0030, ParserConfigurationException -> 0x0066, ArrayIndexOutOfBoundsException -> 0x0064, DOMException -> 0x0062, all -> 0x0060 }
            r1.<init>()     // Catch:{ IOException -> 0x001e, SAXException -> 0x0030, ParserConfigurationException -> 0x0066, ArrayIndexOutOfBoundsException -> 0x0064, DOMException -> 0x0062, all -> 0x0060 }
            r2 = 0
        L_0x0038:
            int r4 = r3.getLength()     // Catch:{ IOException -> 0x001e, SAXException -> 0x0030, ParserConfigurationException -> 0x0066, ArrayIndexOutOfBoundsException -> 0x0064, DOMException -> 0x0062, all -> 0x0060 }
            if (r2 < r4) goto L_0x0040
            r0 = r1
            goto L_0x000f
        L_0x0040:
            org.w3c.dom.Node r4 = r3.item(r2)     // Catch:{ IOException -> 0x001e, SAXException -> 0x0030, ParserConfigurationException -> 0x0066, ArrayIndexOutOfBoundsException -> 0x0064, DOMException -> 0x0062, all -> 0x0060 }
            org.w3c.dom.NamedNodeMap r4 = r4.getAttributes()     // Catch:{ IOException -> 0x001e, SAXException -> 0x0030, ParserConfigurationException -> 0x0066, ArrayIndexOutOfBoundsException -> 0x0064, DOMException -> 0x0062, all -> 0x0060 }
            java.lang.String r5 = new java.lang.String     // Catch:{ IOException -> 0x001e, SAXException -> 0x0030, ParserConfigurationException -> 0x0066, ArrayIndexOutOfBoundsException -> 0x0064, DOMException -> 0x0062, all -> 0x0060 }
            r5.<init>()     // Catch:{ IOException -> 0x001e, SAXException -> 0x0030, ParserConfigurationException -> 0x0066, ArrayIndexOutOfBoundsException -> 0x0064, DOMException -> 0x0062, all -> 0x0060 }
            org.w3c.dom.Node r5 = r3.item(r2)     // Catch:{ IOException -> 0x001e, SAXException -> 0x0030, ParserConfigurationException -> 0x0066, ArrayIndexOutOfBoundsException -> 0x0064, DOMException -> 0x0062, all -> 0x0060 }
            r5.getFirstChild()     // Catch:{ IOException -> 0x001e, SAXException -> 0x0030, ParserConfigurationException -> 0x0066, ArrayIndexOutOfBoundsException -> 0x0064, DOMException -> 0x0062, all -> 0x0060 }
            java.lang.String r5 = "txt"
            java.lang.String r4 = com.shoujiduoduo.wallpaper.utils.f.a(r4, r5)     // Catch:{ IOException -> 0x001e, SAXException -> 0x0030, ParserConfigurationException -> 0x0066, ArrayIndexOutOfBoundsException -> 0x0064, DOMException -> 0x0062, all -> 0x0060 }
            r1.add(r4)     // Catch:{ IOException -> 0x001e, SAXException -> 0x0030, ParserConfigurationException -> 0x0066, ArrayIndexOutOfBoundsException -> 0x0064, DOMException -> 0x0062, all -> 0x0060 }
            int r2 = r2 + 1
            goto L_0x0038
        L_0x0060:
            r0 = move-exception
            throw r0
        L_0x0062:
            r1 = move-exception
            goto L_0x000f
        L_0x0064:
            r1 = move-exception
            goto L_0x000f
        L_0x0066:
            r1 = move-exception
            goto L_0x000f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.wallpaper.a.e.a(java.io.InputStream):java.util.ArrayList");
    }

    static /* synthetic */ boolean c(e eVar) {
        ArrayList a2;
        byte[] b2 = q.b();
        if (b2 == null || (a2 = a(new ByteArrayInputStream(b2))) == null) {
            return false;
        }
        b.a(f1722a, String.valueOf(a2.size()) + " keywords.");
        eVar.c.a(a2);
        eVar.g.sendMessage(eVar.g.obtainMessage(0, a2));
        return true;
    }

    static /* synthetic */ boolean d(e eVar) {
        ArrayList b2 = eVar.c.b();
        if (b2 == null) {
            return false;
        }
        b.a(f1722a, String.valueOf(b2.size()) + " keywords. read from cache.");
        eVar.g.sendMessage(eVar.g.obtainMessage(2, b2));
        return true;
    }

    public String a(int i) {
        if (this.b == null || i < 0 || i >= this.b.size()) {
            return null;
        }
        return (String) this.b.get(i);
    }

    public void a() {
        if (this.b == null) {
            this.e = true;
            this.f = false;
            new r(this).start();
        }
    }

    public void a(f fVar) {
        this.d = fVar;
    }

    public int b() {
        if (this.b == null) {
            return 0;
        }
        return this.b.size();
    }
}
