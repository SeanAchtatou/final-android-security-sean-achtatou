package com.shoujiduoduo.wallpaper.a;

import android.os.Handler;
import com.shoujiduoduo.wallpaper.kernel.b;
import com.shoujiduoduo.wallpaper.utils.f;
import com.shoujiduoduo.wallpaper.utils.q;
import com.sina.weibo.sdk.constant.WBPageConstants;
import com.sina.weibo.sdk.register.mobile.SelectCountryActivity;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class k implements c {
    /* access modifiers changed from: private */
    public static final String d = k.class.getName();

    /* renamed from: a  reason: collision with root package name */
    protected int f1725a;
    protected v b = null;
    String c = null;
    /* access modifiers changed from: private */
    public boolean e = true;
    /* access modifiers changed from: private */
    public boolean f = false;
    /* access modifiers changed from: private */
    public boolean g = false;
    private l h = l.SORT_NO_USE;
    private String i = "";
    /* access modifiers changed from: private */
    public g j = null;
    /* access modifiers changed from: private */
    public ArrayList k;
    /* access modifiers changed from: private */
    public Handler l = new s(this);
    private String m = "";
    /* access modifiers changed from: private */
    public boolean n = false;

    private k() {
    }

    public k(int i2) {
        this.f1725a = i2;
        this.b = new v(String.valueOf(i2) + ".list.tmp");
    }

    public k(int i2, l lVar) {
        this.f1725a = i2;
        this.h = lVar;
        this.b = new v(String.valueOf(i2) + ".list." + this.h.toString());
    }

    public k(int i2, String str) {
        this.f1725a = i2;
        if (this.f1725a == 999999998) {
            this.i = str;
            this.b = new v(String.valueOf(str.hashCode()) + ".list.tmp");
        }
    }

    public static w a(InputStream inputStream) {
        DocumentBuilder newDocumentBuilder;
        Document parse;
        Element documentElement;
        try {
            DocumentBuilderFactory newInstance = DocumentBuilderFactory.newInstance();
            if (newInstance == null || (newDocumentBuilder = newInstance.newDocumentBuilder()) == null || (parse = newDocumentBuilder.parse(inputStream)) == null || (documentElement = parse.getDocumentElement()) == null) {
                return null;
            }
            boolean equalsIgnoreCase = documentElement.getAttribute("hasmore").equalsIgnoreCase("true");
            String attribute = documentElement.getAttribute("baseurl");
            NodeList childNodes = documentElement.getChildNodes();
            if (childNodes == null) {
                return null;
            }
            ArrayList arrayList = new ArrayList();
            b.a(d, "parseContent: listwp size = " + arrayList.size());
            for (int i2 = 0; i2 < childNodes.getLength(); i2++) {
                String nodeName = childNodes.item(i2).getNodeName();
                NamedNodeMap attributes = childNodes.item(i2).getAttributes();
                String textContent = childNodes.item(i2).getTextContent();
                if ("img".equalsIgnoreCase(nodeName)) {
                    j jVar = new j();
                    jVar.h = f.a(attributes, SelectCountryActivity.EXTRA_COUNTRY_NAME);
                    jVar.i = f.a(attributes, "author");
                    try {
                        jVar.f1724a = Integer.parseInt(f.a(attributes, "downnum"));
                    } catch (NumberFormatException e2) {
                        jVar.f1724a = 0;
                    }
                    try {
                        jVar.k = Integer.parseInt(f.a(attributes, "id"));
                    } catch (NumberFormatException e3) {
                        b.a("favorate status", "parseContent numberformatexception, image id = " + f.a(attributes, "id"));
                        jVar.k = 0;
                    }
                    jVar.j = f.a(attributes, "uploader");
                    jVar.b = f.a(attributes, "thumblink");
                    if (!jVar.b.startsWith("http://")) {
                        jVar.b = String.valueOf(attribute) + jVar.b;
                    }
                    jVar.c = f.a(attributes, "link");
                    if (!jVar.c.startsWith("http://")) {
                        jVar.c = String.valueOf(attribute) + jVar.c;
                    }
                    jVar.d = Boolean.parseBoolean(f.a(attributes, "isnew"));
                    arrayList.add(jVar);
                } else if ("album".equalsIgnoreCase(nodeName)) {
                    a aVar = new a();
                    aVar.h = f.a(attributes, SelectCountryActivity.EXTRA_COUNTRY_NAME);
                    aVar.i = f.a(attributes, "author");
                    aVar.j = f.a(attributes, "uploader");
                    try {
                        aVar.k = Integer.parseInt(f.a(attributes, "id"));
                    } catch (NumberFormatException e4) {
                        b.a("favorate status", "parseContent numberformatexception, album id = " + f.a(attributes, "id"));
                        aVar.k = 0;
                    }
                    try {
                        aVar.g = Integer.parseInt(f.a(attributes, WBPageConstants.ParamKey.COUNT));
                    } catch (NumberFormatException e5) {
                        b.a("favorate status", "parseContent numberformatexception, album count = " + f.a(attributes, WBPageConstants.ParamKey.COUNT));
                        aVar.g = 0;
                    }
                    aVar.f = f.a(attributes, "date");
                    aVar.f1720a = f.a(attributes, "thumb1");
                    if (!aVar.f1720a.startsWith("http://")) {
                        aVar.f1720a = String.valueOf(attribute) + aVar.f1720a;
                    }
                    aVar.b = f.a(attributes, "thumb2");
                    if (!aVar.b.startsWith("http://")) {
                        aVar.b = String.valueOf(attribute) + aVar.b;
                    }
                    aVar.c = f.a(attributes, "thumb3");
                    if (!aVar.c.startsWith("http://")) {
                        aVar.c = String.valueOf(attribute) + aVar.c;
                    }
                    aVar.d = f.a(attributes, "thumb4");
                    if (!aVar.d.startsWith("http://")) {
                        aVar.d = String.valueOf(attribute) + aVar.d;
                    }
                    aVar.e = textContent;
                    arrayList.add(aVar);
                }
            }
            w wVar = new w();
            wVar.f1734a = arrayList;
            wVar.b = equalsIgnoreCase;
            wVar.c = attribute;
            return wVar;
        } catch (IOException e6) {
            return null;
        } catch (DOMException e7) {
            return null;
        } catch (SAXException e8) {
            return null;
        } catch (ParserConfigurationException e9) {
            return null;
        } catch (ArrayIndexOutOfBoundsException e10) {
            throw e10;
        }
    }

    public int a() {
        if (this.k == null) {
            return 0;
        }
        return this.k.size();
    }

    public j a(int i2) {
        if (this.k == null || i2 >= this.k.size() || i2 < 0) {
            return null;
        }
        b bVar = (b) this.k.get(i2);
        if (bVar instanceof j) {
            return (j) bVar;
        }
        return null;
    }

    public void a(g gVar) {
        this.j = gVar;
    }

    public void a(String str) {
        this.m = str;
    }

    public int b() {
        return this.f1725a;
    }

    public a b(int i2) {
        if (this.k == null || i2 >= this.k.size() || i2 < 0) {
            return null;
        }
        b bVar = (b) this.k.get(i2);
        if (bVar instanceof a) {
            return (a) bVar;
        }
        return null;
    }

    public void c() {
        this.k = null;
        this.b.a();
        d();
    }

    /* access modifiers changed from: protected */
    public byte[] c(int i2) {
        if (this.f1725a != 999999998) {
            return q.a(this.f1725a, this.h, i2, 20);
        }
        if (this.m == "") {
            this.m = "user_input";
        }
        return q.a(this.i, i2, 20, this.m);
    }

    public void d() {
        if (this.k == null) {
            this.f = true;
            this.g = false;
            if (this.j != null) {
                this.j.a(this, 31);
            }
            new t(this).start();
        } else if (this.e) {
            this.f = true;
            this.g = false;
            if (this.j != null) {
                this.j.a(this, 31);
            }
            new u(this).start();
        }
    }

    public boolean e() {
        b.a(d, this.e ? "has more data" : "have no more data.");
        return this.e;
    }

    public boolean f() {
        b.a(d, this.f ? "is retrieving" : "not retrieving");
        return this.f;
    }

    public l g() {
        return this.h;
    }
}
