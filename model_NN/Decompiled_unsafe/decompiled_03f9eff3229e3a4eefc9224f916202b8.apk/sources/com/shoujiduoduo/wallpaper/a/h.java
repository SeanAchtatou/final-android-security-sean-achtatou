package com.shoujiduoduo.wallpaper.a;

import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import com.shoujiduoduo.wallpaper.kernel.b;
import com.shoujiduoduo.wallpaper.utils.ag;
import com.shoujiduoduo.wallpaper.utils.f;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;

public class h extends SQLiteOpenHelper implements c {

    /* renamed from: a  reason: collision with root package name */
    private static final String f1723a = h.class.getSimpleName();
    private static h b = null;
    private int c;
    private i d;
    private ArrayList e;
    private SQLiteDatabase f;

    private h(Context context, String str, SQLiteDatabase.CursorFactory cursorFactory, int i) {
        super(context, str, (SQLiteDatabase.CursorFactory) null, 1);
        this.e = new ArrayList();
        this.f = null;
        this.f = getWritableDatabase();
        b.a(f1723a, "DownloadManager CreateTable begins.");
        synchronized (f1723a) {
            if (this.f != null) {
                try {
                    this.f.execSQL("CREATE TABLE IF NOT EXISTS wallpaper_duoduo_user_list (id INTEGER PRIMARY KEY AUTOINCREMENT, imageid INTEGER, name VARCHAR, author VARCHAR, url VARCHAR, thumb_url VARCHAR, path VARCHAR, uploader VARCHAR, down_count INTEGER, isnew INTEGER, tag_words VARCHAR, width INTEGER, height INTEGER, category INTEGER, size_in_bytes INTEGER, current_in_use INTEGER);");
                } catch (SQLException e2) {
                    e2.printStackTrace();
                }
            }
        }
        b.a(f1723a, "CreateTable ends.");
        g();
        this.c = ag.a(context, "pref_wallpaper_resource_id", -1);
        return;
    }

    public static int a(String str) {
        if (str == null || str.length() == 0) {
            return 0;
        }
        int hashCode = str.hashCode();
        return hashCode >= 0 ? -hashCode : hashCode;
    }

    public static synchronized h d() {
        h hVar;
        synchronized (h.class) {
            if (b == null) {
                b = new h(f.d(), "com.shoujiduoduo.wallpaper.database", null, 1);
            }
            hVar = b;
        }
        return hVar;
    }

    public int a() {
        return this.e.size();
    }

    public j a(int i) {
        if (i < 0 || i >= this.e.size()) {
            return null;
        }
        return (j) this.e.get(i);
    }

    public void a(int i, String str) {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= this.e.size()) {
                break;
            } else if (i == ((j) this.e.get(i3)).k) {
                b.a(f1723a, "updateLocalPath, path = " + str);
                ((j) this.e.get(i3)).e = str;
                break;
            } else {
                i2 = i3 + 1;
            }
        }
        String str2 = "update wallpaper_duoduo_user_list set path=" + DatabaseUtils.sqlEscapeString(str) + " where imageid=" + i;
        b.a(f1723a, "updateLocalPath, sql = " + str2);
        try {
            this.f.execSQL(str2);
        } catch (SQLiteException e2) {
        }
        if (this.d != null) {
            this.d.a();
        }
    }

    public void a(i iVar) {
        this.d = iVar;
    }

    public void a(j jVar) {
        if (!c(jVar.k)) {
            this.e.add(jVar);
            try {
                String str = "insert into wallpaper_duoduo_user_list (imageid, name, author, url, thumb_url, path, uploader, down_count, isnew, tag_words, width, height, category, size_in_bytes, current_in_use)VALUES (" + Integer.valueOf(jVar.k) + "," + DatabaseUtils.sqlEscapeString(jVar.h) + "," + DatabaseUtils.sqlEscapeString(jVar.i) + "," + DatabaseUtils.sqlEscapeString(jVar.c) + "," + DatabaseUtils.sqlEscapeString(jVar.b) + "," + DatabaseUtils.sqlEscapeString(jVar.e) + "," + DatabaseUtils.sqlEscapeString(jVar.j) + "," + Integer.valueOf(jVar.f1724a == 0 ? ((int) (Math.random() * 901.0d)) + 100 : jVar.f1724a) + "," + (jVar.d ? "1" : "0") + "," + DatabaseUtils.sqlEscapeString(jVar.m) + "," + Integer.valueOf(jVar.f) + "," + Integer.valueOf(jVar.g) + "," + Integer.valueOf(jVar.n) + "," + Integer.valueOf(jVar.l) + "," + (jVar.o ? "1" : "0") + ");";
                b.a(f1723a, "add2userlist, sql = " + str);
                this.f.execSQL(str);
            } catch (SQLiteException e2) {
            }
            if (this.d != null) {
                this.d.a();
            }
        }
    }

    public int b() {
        return -1;
    }

    public void b(int i) {
        if (i != this.c) {
            ag.b(f.d(), "pref_wallpaper_resource_id", i);
            this.c = i;
            if (this.d != null) {
                this.d.a();
            }
        }
    }

    public int c() {
        return this.c;
    }

    public boolean c(int i) {
        b.a("favorate status", "isWallpaperInUserList, input imageid = " + i);
        Iterator it = this.e.iterator();
        while (it.hasNext()) {
            if (((j) it.next()).k == i) {
                return true;
            }
        }
        return false;
    }

    public void d(int i) {
        b.a(f1723a, "removeWallpaperById");
        try {
            this.f.execSQL("delete from wallpaper_duoduo_user_list where imageid=" + i);
        } catch (SQLiteException e2) {
            e2.printStackTrace();
        }
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= this.e.size()) {
                break;
            } else if (((j) this.e.get(i3)).k == i) {
                String str = ((j) this.e.get(i3)).e;
                b.a(f1723a, "delete favorate file: " + str);
                if (!(str == null || str.length() == 0)) {
                    new File(str).delete();
                }
                b.a(f1723a, "remove the file from user list.");
                this.e.remove(i3);
            } else {
                i2 = i3 + 1;
            }
        }
        if (this.d != null) {
            this.d.a();
        }
    }

    public void e() {
        if (this.f != null) {
            this.f.close();
            this.f = null;
        }
        b = null;
    }

    public void f() {
        try {
            this.f.execSQL("delete from wallpaper_duoduo_user_list");
        } catch (SQLiteException e2) {
            e2.printStackTrace();
        }
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.e.size()) {
                break;
            }
            String str = ((j) this.e.get(i2)).e;
            b.a(f1723a, "delete favorate file: " + str);
            if (!(str == null || str.length() == 0)) {
                new File(str).delete();
            }
            b.a(f1723a, "remove the file from user list.");
            i = i2 + 1;
        }
        this.e.clear();
        if (this.d != null) {
            this.d.a();
        }
    }

    public void g() {
        this.e.clear();
        try {
            Cursor rawQuery = this.f.rawQuery("select * from wallpaper_duoduo_user_list order by id", null);
            if (rawQuery != null) {
                if (rawQuery.getCount() == 0) {
                    rawQuery.close();
                    return;
                }
                while (rawQuery.moveToNext()) {
                    j jVar = new j();
                    jVar.k = rawQuery.getInt(1);
                    jVar.h = rawQuery.getString(2);
                    jVar.i = rawQuery.getString(3);
                    jVar.c = rawQuery.getString(4);
                    jVar.b = rawQuery.getString(5);
                    jVar.e = rawQuery.getString(6);
                    jVar.j = rawQuery.getString(7);
                    jVar.f1724a = rawQuery.getInt(8);
                    jVar.d = rawQuery.getInt(9) == 1;
                    jVar.m = rawQuery.getString(10);
                    jVar.f = rawQuery.getInt(11);
                    jVar.g = rawQuery.getInt(12);
                    jVar.n = rawQuery.getInt(13);
                    jVar.l = rawQuery.getInt(14);
                    jVar.o = rawQuery.getInt(15) == 1;
                    this.e.add(jVar);
                }
                rawQuery.close();
            }
        } catch (SQLException e2) {
        }
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
    }
}
