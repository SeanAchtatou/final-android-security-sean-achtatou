package com.shoujiduoduo.wallpaper.a;

import com.shoujiduoduo.wallpaper.kernel.b;
import com.shoujiduoduo.wallpaper.utils.f;
import java.io.ByteArrayInputStream;

final class t extends Thread {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ k f1732a;

    t(k kVar) {
        this.f1732a = kVar;
    }

    public final void run() {
        w wVar;
        if (!this.f1732a.b.a(2)) {
            b.a(k.d, "WallpaperList: cache is available! Use Cache!");
            w b = this.f1732a.b.b();
            if (b != null) {
                b.a(k.d, "WallpaperList: Read WallpaperList Cache Success! send MESSAGE_SUCCESS_RETRIEVE_DATA");
                this.f1732a.l.sendMessage(this.f1732a.l.obtainMessage(0, b));
                return;
            }
        }
        b.a(k.d, "WallpaperList: cache is out of date or cache failed!");
        byte[] c = this.f1732a.c(0);
        if (c == null) {
            b.a(k.d, "WallpaperList: httpGetWallpaperList Failed!");
            this.f1732a.l.sendEmptyMessage(1);
            return;
        }
        b.a(k.d, "WallpaperList: content = \"" + c.toString() + "\"");
        try {
            wVar = k.a(new ByteArrayInputStream(c));
        } catch (ArrayIndexOutOfBoundsException e) {
            wVar = null;
            f.a("parse WallpaperList error! ArrayIndexOutOfBoundsException. " + new String(c));
        }
        if (wVar != null) {
            b.a("WallpaperList", "list data size = " + wVar.f1734a.size());
            b.a(k.d, "WallpaperList: Read from network Success! send MESSAGE_SUCCESS_RETRIEVE_DATA");
            this.f1732a.n = true;
            this.f1732a.l.sendMessage(this.f1732a.l.obtainMessage(0, wVar));
            return;
        }
        b.a(k.d, "WallpaperList: Read from network Success, but parse FAILED! send MESSAGE_FAIL_RETRIEVE_DATA");
        this.f1732a.l.sendEmptyMessage(1);
    }
}
