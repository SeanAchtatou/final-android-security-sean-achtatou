package com.shoujiduoduo.wallpaper.a;

import com.qhad.ads.sdk.adcore.HttpCacher;
import com.shoujiduoduo.wallpaper.kernel.b;
import com.shoujiduoduo.wallpaper.utils.f;
import java.io.File;

public abstract class d {
    public static String b = "cache/";
    protected static String c = (String.valueOf(f.a()) + b);

    /* renamed from: a  reason: collision with root package name */
    protected String f1721a = null;
    private boolean d = false;

    public d() {
    }

    public d(String str) {
        this.f1721a = str;
    }

    public void a() {
        this.d = true;
    }

    public boolean a(int i) {
        if (this.d) {
            this.d = false;
            return true;
        }
        long lastModified = new File(String.valueOf(c) + this.f1721a).lastModified();
        if (lastModified == 0) {
            return true;
        }
        long currentTimeMillis = System.currentTimeMillis() - lastModified;
        long j = currentTimeMillis / 1000;
        b.a("DuoduoCache Base Class", "time since cached: " + (j / 3600) + "小时" + ((j % 3600) / 60) + "分钟" + (j % 60) + "秒");
        if (currentTimeMillis > ((long) (i * HttpCacher.TIME_HOUR)) * 1000) {
            b.a("DuoduoCache Base Class", "cache out of date.");
            return true;
        }
        b.a("DuoduoCache Base Class", "cache is valid. use cache.");
        return false;
    }
}
