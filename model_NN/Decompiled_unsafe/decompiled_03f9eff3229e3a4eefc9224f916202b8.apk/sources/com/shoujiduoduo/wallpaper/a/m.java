package com.shoujiduoduo.wallpaper.a;

import com.shoujiduoduo.wallpaper.kernel.b;
import com.shoujiduoduo.wallpaper.utils.ag;
import com.shoujiduoduo.wallpaper.utils.f;
import java.util.ArrayList;
import java.util.HashMap;

public class m {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final String f1727a = m.class.getSimpleName();
    /* access modifiers changed from: private */
    public static String d = (String.valueOf(f.a()) + "category_info.tmp");
    private static m g = null;
    private static /* synthetic */ int[] h;
    private HashMap b = new HashMap();
    private HashMap c = new HashMap();
    /* access modifiers changed from: private */
    public n e = null;
    private k f = null;

    private m() {
    }

    public static m b() {
        if (g == null) {
            g = new m();
        }
        return g;
    }

    private void g() {
        new Thread(new x(this)).start();
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:42:?, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:?, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:?, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:?, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:?, code lost:
        return null;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.ArrayList h() {
        /*
            r11 = this;
            r8 = 0
            r6 = 0
            java.util.ArrayList r7 = new java.util.ArrayList
            r7.<init>()
            javax.xml.parsers.DocumentBuilderFactory r0 = javax.xml.parsers.DocumentBuilderFactory.newInstance()     // Catch:{ IOException -> 0x0071, SAXException -> 0x0074, ParserConfigurationException -> 0x0077, DOMException -> 0x007a, Exception -> 0x007d, all -> 0x0080 }
            javax.xml.parsers.DocumentBuilder r0 = r0.newDocumentBuilder()     // Catch:{ IOException -> 0x0071, SAXException -> 0x0074, ParserConfigurationException -> 0x0077, DOMException -> 0x007a, Exception -> 0x007d, all -> 0x0080 }
            java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ IOException -> 0x0071, SAXException -> 0x0074, ParserConfigurationException -> 0x0077, DOMException -> 0x007a, Exception -> 0x007d, all -> 0x0080 }
            java.lang.String r2 = com.shoujiduoduo.wallpaper.a.m.d     // Catch:{ IOException -> 0x0071, SAXException -> 0x0074, ParserConfigurationException -> 0x0077, DOMException -> 0x007a, Exception -> 0x007d, all -> 0x0080 }
            r1.<init>(r2)     // Catch:{ IOException -> 0x0071, SAXException -> 0x0074, ParserConfigurationException -> 0x0077, DOMException -> 0x007a, Exception -> 0x007d, all -> 0x0080 }
            org.w3c.dom.Document r0 = r0.parse(r1)     // Catch:{ IOException -> 0x0071, SAXException -> 0x0074, ParserConfigurationException -> 0x0077, DOMException -> 0x007a, Exception -> 0x007d, all -> 0x0080 }
            org.w3c.dom.Element r0 = r0.getDocumentElement()     // Catch:{ IOException -> 0x0071, SAXException -> 0x0074, ParserConfigurationException -> 0x0077, DOMException -> 0x007a, Exception -> 0x007d, all -> 0x0080 }
            if (r0 != 0) goto L_0x0022
            r0 = r6
        L_0x0021:
            return r0
        L_0x0022:
            java.lang.String r1 = "category"
            org.w3c.dom.NodeList r10 = r0.getElementsByTagName(r1)     // Catch:{ IOException -> 0x0071, SAXException -> 0x0074, ParserConfigurationException -> 0x0077, DOMException -> 0x007a, Exception -> 0x007d, all -> 0x0080 }
            r9 = r8
        L_0x0029:
            int r0 = r10.getLength()     // Catch:{ IOException -> 0x0071, SAXException -> 0x0074, ParserConfigurationException -> 0x0077, DOMException -> 0x007a, Exception -> 0x007d, all -> 0x0080 }
            if (r9 < r0) goto L_0x0031
            r0 = r7
            goto L_0x0021
        L_0x0031:
            org.w3c.dom.Node r0 = r10.item(r9)     // Catch:{ IOException -> 0x0071, SAXException -> 0x0074, ParserConfigurationException -> 0x0077, DOMException -> 0x007a, Exception -> 0x007d, all -> 0x0080 }
            org.w3c.dom.NamedNodeMap r0 = r0.getAttributes()     // Catch:{ IOException -> 0x0071, SAXException -> 0x0074, ParserConfigurationException -> 0x0077, DOMException -> 0x007a, Exception -> 0x007d, all -> 0x0080 }
            java.lang.String r1 = "name"
            java.lang.String r3 = com.shoujiduoduo.wallpaper.utils.f.a(r0, r1)     // Catch:{ IOException -> 0x0071, SAXException -> 0x0074, ParserConfigurationException -> 0x0077, DOMException -> 0x007a, Exception -> 0x007d, all -> 0x0080 }
            java.lang.String r1 = "id"
            java.lang.String r1 = com.shoujiduoduo.wallpaper.utils.f.a(r0, r1)     // Catch:{ NumberFormatException -> 0x0082 }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ NumberFormatException -> 0x0082 }
            int r2 = r1.intValue()     // Catch:{ NumberFormatException -> 0x0082 }
            java.lang.String r1 = "thumb"
            java.lang.String r4 = com.shoujiduoduo.wallpaper.utils.f.a(r0, r1)     // Catch:{ IOException -> 0x0071, SAXException -> 0x0074, ParserConfigurationException -> 0x0077, DOMException -> 0x007a, Exception -> 0x007d, all -> 0x0080 }
            java.lang.String r1 = "update_num"
            java.lang.String r0 = com.shoujiduoduo.wallpaper.utils.f.a(r0, r1)     // Catch:{ NumberFormatException -> 0x006e }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ NumberFormatException -> 0x006e }
            int r5 = r0.intValue()     // Catch:{ NumberFormatException -> 0x006e }
        L_0x0061:
            com.shoujiduoduo.wallpaper.a.o r0 = new com.shoujiduoduo.wallpaper.a.o     // Catch:{ IOException -> 0x0071, SAXException -> 0x0074, ParserConfigurationException -> 0x0077, DOMException -> 0x007a, Exception -> 0x007d, all -> 0x0080 }
            r1 = r11
            r0.<init>(r1, r2, r3, r4, r5)     // Catch:{ IOException -> 0x0071, SAXException -> 0x0074, ParserConfigurationException -> 0x0077, DOMException -> 0x007a, Exception -> 0x007d, all -> 0x0080 }
            r7.add(r0)     // Catch:{ IOException -> 0x0071, SAXException -> 0x0074, ParserConfigurationException -> 0x0077, DOMException -> 0x007a, Exception -> 0x007d, all -> 0x0080 }
        L_0x006a:
            int r0 = r9 + 1
            r9 = r0
            goto L_0x0029
        L_0x006e:
            r0 = move-exception
            r5 = r8
            goto L_0x0061
        L_0x0071:
            r0 = move-exception
            r0 = r6
            goto L_0x0021
        L_0x0074:
            r0 = move-exception
            r0 = r6
            goto L_0x0021
        L_0x0077:
            r0 = move-exception
            r0 = r6
            goto L_0x0021
        L_0x007a:
            r0 = move-exception
            r0 = r6
            goto L_0x0021
        L_0x007d:
            r0 = move-exception
            r0 = r6
            goto L_0x0021
        L_0x0080:
            r0 = move-exception
            throw r0
        L_0x0082:
            r0 = move-exception
            goto L_0x006a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.wallpaper.a.m.h():java.util.ArrayList");
    }

    private static /* synthetic */ int[] i() {
        int[] iArr = h;
        if (iArr == null) {
            iArr = new int[l.values().length];
            try {
                iArr[l.SORT_BY_HOT.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[l.SORT_BY_NEW.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[l.SORT_NO_USE.ordinal()] = 1;
            } catch (NoSuchFieldError e4) {
            }
            h = iArr;
        }
        return iArr;
    }

    public k a() {
        return this.f;
    }

    public k a(int i) {
        if (!this.b.containsKey(Integer.valueOf(i))) {
            this.b.put(Integer.valueOf(i), new k(i));
        }
        return (k) this.b.get(Integer.valueOf(i));
    }

    public k a(int i, l lVar) {
        switch (i()[lVar.ordinal()]) {
            case 1:
                return a(i);
            case 2:
                if (!this.b.containsKey(Integer.valueOf(i))) {
                    this.b.put(Integer.valueOf(i), new k(i, l.SORT_BY_HOT));
                }
                return (k) this.b.get(Integer.valueOf(i));
            case 3:
                if (!this.c.containsKey(Integer.valueOf(i))) {
                    this.c.put(Integer.valueOf(i), new k(i, l.SORT_BY_NEW));
                }
                return (k) this.c.get(Integer.valueOf(i));
            default:
                return null;
        }
    }

    public void a(k kVar) {
        this.f = kVar;
    }

    public void a(n nVar) {
        this.e = nVar;
    }

    public void c() {
        this.b = null;
        g = null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.wallpaper.utils.ag.a(android.content.Context, java.lang.String, long):long
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.shoujiduoduo.wallpaper.utils.ag.a(android.content.Context, java.lang.String, int):int
      com.shoujiduoduo.wallpaper.utils.ag.a(android.content.Context, java.lang.String, long):long */
    public void d() {
        ArrayList h2;
        b.a(f1727a, "begin loadServerConfig");
        long a2 = ag.a(f.d(), "pref_time_update_category_info", 0L);
        if (a2 != 0) {
            b.a(f1727a, "timeLastUpdate = " + a2);
            b.a(f1727a, "current time = " + System.currentTimeMillis());
            if (System.currentTimeMillis() - a2 <= LogBuilder.MAX_INTERVAL && (h2 = h()) != null) {
                if (this.e != null) {
                    this.e.a(h2);
                    return;
                }
                return;
            }
        }
        g();
    }
}
