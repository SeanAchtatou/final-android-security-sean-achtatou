package com.shoujiduoduo.wallpaper.a;

import com.shoujiduoduo.wallpaper.kernel.b;
import com.shoujiduoduo.wallpaper.utils.f;
import java.io.ByteArrayInputStream;

final class u extends Thread {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ k f1733a;

    u(k kVar) {
        this.f1733a = kVar;
    }

    public final void run() {
        w wVar;
        b.a(k.d, "retrieving more data, list size = " + this.f1733a.k.size());
        byte[] c = this.f1733a.c(this.f1733a.k.size() / 20);
        if (c == null) {
            this.f1733a.l.sendEmptyMessage(2);
            return;
        }
        try {
            wVar = k.a(new ByteArrayInputStream(c));
        } catch (ArrayIndexOutOfBoundsException e) {
            wVar = null;
            f.a("parse WallpaperList error! ArrayIndexOutOfBoundsException. " + new String(c));
        }
        if (wVar != null) {
            b.a("WallpaperList", "list data size = " + wVar.f1734a.size());
            this.f1733a.n = true;
            this.f1733a.l.sendMessage(this.f1733a.l.obtainMessage(0, wVar));
            return;
        }
        this.f1733a.l.sendEmptyMessage(2);
    }
}
