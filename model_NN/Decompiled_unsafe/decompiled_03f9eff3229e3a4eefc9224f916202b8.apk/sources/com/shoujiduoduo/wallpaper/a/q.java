package com.shoujiduoduo.wallpaper.a;

import android.os.Handler;
import android.os.Message;
import com.shoujiduoduo.wallpaper.kernel.b;
import java.util.ArrayList;

final class q extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ e f1729a;

    q(e eVar) {
        this.f1729a = eVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.wallpaper.a.e.a(com.shoujiduoduo.wallpaper.a.e, boolean):void
     arg types: [com.shoujiduoduo.wallpaper.a.e, int]
     candidates:
      com.shoujiduoduo.wallpaper.a.e.a(com.shoujiduoduo.wallpaper.a.e, java.util.ArrayList):void
      com.shoujiduoduo.wallpaper.a.e.a(com.shoujiduoduo.wallpaper.a.e, boolean):void */
    public final void handleMessage(Message message) {
        b.a(e.f1722a, "Thread ID: " + Thread.currentThread().getName());
        switch (message.what) {
            case 0:
            case 2:
                this.f1729a.b = (ArrayList) message.obj;
                this.f1729a.e = false;
                this.f1729a.f = false;
                break;
            case 1:
                this.f1729a.e = false;
                this.f1729a.f = true;
                break;
        }
        if (this.f1729a.d != null) {
            this.f1729a.d.a(null, message.what);
        }
    }
}
