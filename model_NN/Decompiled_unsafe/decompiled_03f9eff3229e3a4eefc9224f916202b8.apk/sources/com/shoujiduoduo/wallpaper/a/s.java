package com.shoujiduoduo.wallpaper.a;

import android.os.Handler;
import android.os.Message;
import com.shoujiduoduo.wallpaper.kernel.b;

final class s extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ k f1731a;

    s(k kVar) {
        this.f1731a = kVar;
    }

    public final void handleMessage(Message message) {
        boolean z = true;
        boolean z2 = false;
        switch (message.what) {
            case 0:
                w wVar = (w) message.obj;
                if (wVar != null) {
                    b.a("WallpaperList", "new obtained list data size = " + wVar.f1734a.size());
                    if (this.f1731a.k == null) {
                        this.f1731a.k = wVar.f1734a;
                        z = false;
                    } else {
                        this.f1731a.k.addAll(wVar.f1734a);
                    }
                    this.f1731a.e = wVar.b;
                    this.f1731a.c = wVar.c;
                    wVar.f1734a = this.f1731a.k;
                    if (this.f1731a.n) {
                        this.f1731a.b.a(wVar);
                        this.f1731a.n = false;
                    }
                } else {
                    z = false;
                }
                this.f1731a.f = false;
                this.f1731a.g = false;
                z2 = z;
                break;
            case 1:
            case 2:
                this.f1731a.f = false;
                this.f1731a.g = true;
                break;
        }
        if (this.f1731a.j != null) {
            this.f1731a.j.a(this.f1731a, z2 ? 32 : message.what);
        }
    }
}
