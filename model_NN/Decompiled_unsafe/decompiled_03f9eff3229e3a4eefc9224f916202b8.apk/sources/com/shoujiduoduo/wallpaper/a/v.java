package com.shoujiduoduo.wallpaper.a;

import android.util.Xml;
import com.shoujiduoduo.wallpaper.utils.m;
import com.sina.weibo.sdk.constant.WBPageConstants;
import com.sina.weibo.sdk.register.mobile.SelectCountryActivity;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.StringWriter;
import java.util.ArrayList;
import org.xmlpull.v1.XmlSerializer;

final class v extends d {
    v() {
    }

    v(String str) {
        super(str);
    }

    public final void a(w wVar) {
        ArrayList arrayList = wVar.f1734a;
        XmlSerializer newSerializer = Xml.newSerializer();
        StringWriter stringWriter = new StringWriter();
        try {
            newSerializer.setOutput(stringWriter);
            newSerializer.startDocument("UTF-8", true);
            newSerializer.startTag("", "list");
            newSerializer.attribute("", "num", String.valueOf(arrayList.size()));
            newSerializer.attribute("", "hasmore", String.valueOf(wVar.b));
            newSerializer.attribute("", "baseurl", wVar.c);
            for (int i = 0; i < arrayList.size(); i++) {
                b bVar = (b) arrayList.get(i);
                if (bVar instanceof j) {
                    j jVar = (j) bVar;
                    newSerializer.startTag("", "img");
                    newSerializer.attribute("", SelectCountryActivity.EXTRA_COUNTRY_NAME, jVar.h);
                    newSerializer.attribute("", "author", jVar.i);
                    newSerializer.attribute("", "id", String.valueOf(jVar.k));
                    newSerializer.attribute("", "thumblink", jVar.b);
                    newSerializer.attribute("", "downnum", String.valueOf(jVar.f1724a == 0 ? ((int) (Math.random() * 901.0d)) + 100 : jVar.f1724a));
                    newSerializer.attribute("", "link", jVar.c);
                    newSerializer.attribute("", "uploader", jVar.j);
                    newSerializer.attribute("", "isnew", String.valueOf(jVar.d));
                    newSerializer.endTag("", "img");
                } else if (bVar instanceof a) {
                    a aVar = (a) bVar;
                    newSerializer.startTag("", "album");
                    newSerializer.attribute("", SelectCountryActivity.EXTRA_COUNTRY_NAME, aVar.h);
                    newSerializer.attribute("", "author", aVar.i);
                    newSerializer.attribute("", "id", String.valueOf(aVar.k));
                    newSerializer.attribute("", "date", aVar.f);
                    newSerializer.attribute("", WBPageConstants.ParamKey.COUNT, String.valueOf(aVar.g));
                    newSerializer.attribute("", "thumb1", aVar.f1720a);
                    newSerializer.attribute("", "thumb2", aVar.b);
                    newSerializer.attribute("", "thumb3", aVar.c);
                    newSerializer.attribute("", "thumb4", aVar.d);
                    newSerializer.attribute("", "uploader", aVar.j);
                    newSerializer.cdsect(aVar.e);
                    newSerializer.endTag("", "album");
                }
            }
            newSerializer.endTag("", "list");
            newSerializer.endDocument();
            m.a(String.valueOf(c) + this.f1721a, stringWriter.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public final w b() {
        try {
            return k.a(new FileInputStream(String.valueOf(c) + this.f1721a));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (ArrayIndexOutOfBoundsException e2) {
            e2.printStackTrace();
            return null;
        }
    }
}
