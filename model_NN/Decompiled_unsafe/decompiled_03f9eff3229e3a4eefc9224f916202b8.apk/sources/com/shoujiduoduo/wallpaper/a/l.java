package com.shoujiduoduo.wallpaper.a;

public enum l {
    SORT_NO_USE,
    SORT_BY_HOT,
    SORT_BY_NEW;

    public final String toString() {
        return this == SORT_NO_USE ? "no" : this == SORT_BY_HOT ? "hot" : this == SORT_BY_NEW ? "new" : "";
    }
}
