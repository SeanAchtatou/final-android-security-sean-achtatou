package com.shoujiduoduo.wallpaper.a;

import android.util.Xml;
import com.shoujiduoduo.wallpaper.utils.m;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.StringWriter;
import java.util.ArrayList;
import org.xmlpull.v1.XmlSerializer;

final class p extends d {
    p() {
    }

    p(String str) {
        super(str);
    }

    public final void a(ArrayList arrayList) {
        if (arrayList != null) {
            XmlSerializer newSerializer = Xml.newSerializer();
            StringWriter stringWriter = new StringWriter();
            try {
                newSerializer.setOutput(stringWriter);
                newSerializer.startDocument("UTF-8", true);
                newSerializer.startTag("", "root");
                newSerializer.attribute("", "num", String.valueOf(arrayList.size()));
                int i = 0;
                while (true) {
                    int i2 = i;
                    if (i2 >= arrayList.size()) {
                        newSerializer.endTag("", "root");
                        newSerializer.endDocument();
                        m.a(String.valueOf(c) + this.f1721a, stringWriter.toString());
                        return;
                    }
                    newSerializer.startTag("", "key");
                    newSerializer.attribute("", "txt", (String) arrayList.get(i2));
                    newSerializer.endTag("", "key");
                    i = i2 + 1;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public final ArrayList b() {
        try {
            return e.a(new FileInputStream(String.valueOf(c) + this.f1721a));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }
}
