package com.shoujiduoduo.wallpaper.kernel;

import android.os.Environment;
import android.util.Log;
import java.io.File;

public class c {

    /* renamed from: a  reason: collision with root package name */
    protected static boolean f1819a = true;
    private static final Object b = new Object();
    private static final String c = Environment.getExternalStorageDirectory().getAbsolutePath();
    private static final String d = (String.valueOf(c) + File.separator + "duoduo.log");

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileWriter.<init>(java.lang.String, boolean):void throws java.io.IOException}
      ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException} */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0099 A[SYNTHETIC, Splitter:B:16:0x0099] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00a8 A[SYNTHETIC, Splitter:B:26:0x00a8] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00b1 A[SYNTHETIC, Splitter:B:31:0x00b1] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:11:0x0093=Splitter:B:11:0x0093, B:33:0x00b4=Splitter:B:33:0x00b4, B:18:0x009c=Splitter:B:18:0x009c} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static int a(int r8, java.lang.String r9, java.lang.String r10) {
        /*
            r3 = 1
            r0 = 0
            r1 = 8
            java.lang.String[] r1 = new java.lang.String[r1]
            java.lang.String r2 = ""
            r1[r0] = r2
            java.lang.String r2 = ""
            r1[r3] = r2
            r2 = 2
            java.lang.String r3 = "V"
            r1[r2] = r3
            r2 = 3
            java.lang.String r3 = "D"
            r1[r2] = r3
            r2 = 4
            java.lang.String r3 = "I"
            r1[r2] = r3
            r2 = 5
            java.lang.String r3 = "W"
            r1[r2] = r3
            r2 = 6
            java.lang.String r3 = "E"
            r1[r2] = r3
            r2 = 7
            java.lang.String r3 = "A"
            r1[r2] = r3
            java.text.SimpleDateFormat r2 = new java.text.SimpleDateFormat
            java.lang.String r3 = "[MM-dd hh:mm:ss.SSS]"
            r2.<init>(r3)
            java.util.Date r3 = new java.util.Date
            r3.<init>()
            java.lang.String r2 = r2.format(r3)
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r3.append(r2)
            java.lang.String r2 = "\t"
            r3.append(r2)
            r1 = r1[r8]
            r3.append(r1)
            java.lang.String r1 = "/"
            r3.append(r1)
            r3.append(r9)
            int r1 = android.os.Process.myPid()
            java.lang.String r2 = "("
            r3.append(r2)
            r3.append(r1)
            java.lang.String r1 = "):"
            r3.append(r1)
            r3.append(r10)
            java.lang.String r1 = "\n"
            r3.append(r1)
            r1 = 0
            java.lang.Object r4 = com.shoujiduoduo.wallpaper.kernel.c.b
            monitor-enter(r4)
            java.io.File r5 = new java.io.File     // Catch:{ FileNotFoundException -> 0x0095, IOException -> 0x009f, all -> 0x00ae }
            java.lang.String r2 = com.shoujiduoduo.wallpaper.kernel.c.d     // Catch:{ FileNotFoundException -> 0x0095, IOException -> 0x009f, all -> 0x00ae }
            r5.<init>(r2)     // Catch:{ FileNotFoundException -> 0x0095, IOException -> 0x009f, all -> 0x00ae }
            boolean r2 = r5.exists()     // Catch:{ FileNotFoundException -> 0x0095, IOException -> 0x009f, all -> 0x00ae }
            if (r2 != 0) goto L_0x0083
            r5.createNewFile()     // Catch:{ FileNotFoundException -> 0x0095, IOException -> 0x009f, all -> 0x00ae }
        L_0x0083:
            java.io.FileWriter r2 = new java.io.FileWriter     // Catch:{ FileNotFoundException -> 0x0095, IOException -> 0x009f, all -> 0x00ae }
            r6 = 1
            r2.<init>(r5, r6)     // Catch:{ FileNotFoundException -> 0x0095, IOException -> 0x009f, all -> 0x00ae }
            java.lang.String r1 = r3.toString()     // Catch:{ FileNotFoundException -> 0x00c3, IOException -> 0x00c1 }
            r2.write(r1)     // Catch:{ FileNotFoundException -> 0x00c3, IOException -> 0x00c1 }
            r2.close()     // Catch:{ IOException -> 0x00bc }
        L_0x0093:
            monitor-exit(r4)     // Catch:{ all -> 0x00b5 }
        L_0x0094:
            return r0
        L_0x0095:
            r0 = move-exception
            r0 = r1
        L_0x0097:
            if (r0 == 0) goto L_0x009c
            r0.close()     // Catch:{ IOException -> 0x00b8 }
        L_0x009c:
            monitor-exit(r4)     // Catch:{ all -> 0x00b5 }
            r0 = -1
            goto L_0x0094
        L_0x009f:
            r2 = move-exception
            r7 = r2
            r2 = r1
            r1 = r7
        L_0x00a3:
            r1.printStackTrace()     // Catch:{ all -> 0x00be }
            if (r2 == 0) goto L_0x0093
            r2.close()     // Catch:{ IOException -> 0x00ac }
            goto L_0x0093
        L_0x00ac:
            r1 = move-exception
            goto L_0x0093
        L_0x00ae:
            r0 = move-exception
        L_0x00af:
            if (r1 == 0) goto L_0x00b4
            r1.close()     // Catch:{ IOException -> 0x00ba }
        L_0x00b4:
            throw r0     // Catch:{ all -> 0x00b5 }
        L_0x00b5:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        L_0x00b8:
            r0 = move-exception
            goto L_0x009c
        L_0x00ba:
            r1 = move-exception
            goto L_0x00b4
        L_0x00bc:
            r1 = move-exception
            goto L_0x0093
        L_0x00be:
            r0 = move-exception
            r1 = r2
            goto L_0x00af
        L_0x00c1:
            r1 = move-exception
            goto L_0x00a3
        L_0x00c3:
            r0 = move-exception
            r0 = r2
            goto L_0x0097
        */
        throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.wallpaper.kernel.c.a(int, java.lang.String, java.lang.String):int");
    }

    public static int a(String str, String str2) {
        return f1819a ? Log.d(str, str2) : a(3, str, str2);
    }

    public static int b(String str, String str2) {
        return f1819a ? Log.e(str, str2) : a(6, str, str2);
    }
}
