package com.shoujiduoduo.wallpaper.kernel;

import android.app.Application;
import android.content.Context;
import android.graphics.drawable.Drawable;
import com.d.a.a.a.b.c;
import com.d.a.b.d;
import com.d.a.b.e;
import com.shoujiduoduo.wallpaper.utils.ag;
import com.shoujiduoduo.wallpaper.utils.f;
import com.shoujiduoduo.wallpaper.utils.m;
import com.umeng.analytics.b;

public class App extends Application {

    /* renamed from: a  reason: collision with root package name */
    public static int f1817a = 0;
    public static int b = 0;
    public static int c = 0;
    public static int d = 0;
    public static Drawable e;
    private static final String f = App.class.getSimpleName();

    public static void a(Context context) {
        ag.a(context);
        f.a(context);
        m.a(context);
        b(context);
        e = context.getResources().getDrawable(f.a(context.getPackageName(), "drawable", "wallpaperdd_ic_stub"));
        int i = context.getResources().getDisplayMetrics().widthPixels;
        float f2 = context.getResources().getDisplayMetrics().density;
        c = (int) ((5.0f * f2) + 0.5f);
        int i2 = (i - c) / 2;
        f1817a = i2;
        b = i2;
        d = (int) ((25.0f * f2) + 0.5f);
        b.a(false);
    }

    public static void b(Context context) {
        d.a().a(new e.a(context).b(3).a().a(new c()).a(com.d.a.b.a.m.LIFO).c());
    }

    public void onCreate() {
        super.onCreate();
        b.a(f, "App Class is created!, ThreadID = " + Thread.currentThread().getId());
        a(this);
    }

    public void onTerminate() {
        b.a(f, "App onTerminate.");
        e = null;
        super.onTerminate();
    }
}
