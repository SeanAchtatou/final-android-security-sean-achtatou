package com.shoujiduoduo.wallpaper.activity;

import android.app.AlertDialog;
import android.view.View;
import android.widget.Toast;
import com.shoujiduoduo.wallpaper.a.h;
import com.shoujiduoduo.wallpaper.a.j;
import com.shoujiduoduo.wallpaper.kernel.a;
import com.shoujiduoduo.wallpaper.kernel.b;
import com.shoujiduoduo.wallpaper.utils.f;

final class cg implements View.OnClickListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public /* synthetic */ FullScreenPicActivity f1796a;

    cg(FullScreenPicActivity fullScreenPicActivity) {
        this.f1796a = fullScreenPicActivity;
    }

    public final void onClick(View view) {
        boolean z = false;
        if (this.f1796a.b != null) {
            b.a("favorate status", "set favorate status: id = " + this.f1796a.b.k);
            Toast.makeText(this.f1796a, "id = " + this.f1796a.b.k, 1).show();
            if (this.f1796a.q) {
                new AlertDialog.Builder(this.f1796a).setTitle(this.f1796a.getResources().getString(f.c("R.string.wallpaperdd_alert_dialog_header"))).setMessage(this.f1796a.getResources().getString(f.c("R.string.wallpaperdd_on_remove_favorate_prompt"))).setIcon(17301543).setPositiveButton(this.f1796a.getResources().getString(f.c("R.string.wallpaperdd_text_ok_button")), new a(this)).setNegativeButton(this.f1796a.getResources().getString(f.c("R.string.wallpaperdd_text_cancel_button")), new b(this)).show();
                return;
            }
            if (this.f1796a instanceof WallpaperActivity) {
                com.umeng.analytics.b.b(this.f1796a, "CLICK_FAVORATE");
            }
            if (this.f1796a.f1739a == a.LOAD_FINISHED) {
                new c(this, new j(this.f1796a.b)).start();
                Toast.makeText(this.f1796a, String.format(this.f1796a.getResources().getString(f.c("R.string.wallpaperdd_favorate_success_prompt")), "shoujiduoduo/Wallpaper/favorate/"), 1).show();
            }
            h.d().a(this.f1796a.b);
            this.f1796a.o.setImageDrawable(this.f1796a.getResources().getDrawable(f.c("R.drawable.wallpaperdd_btn_remove_favorate")));
            FullScreenPicActivity fullScreenPicActivity = this.f1796a;
            if (!this.f1796a.q) {
                z = true;
            }
            fullScreenPicActivity.q = z;
        }
    }
}
