package com.shoujiduoduo.wallpaper.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import com.shoujiduoduo.wallpaper.kernel.b;

final class p extends FragmentPagerAdapter {

    /* renamed from: a  reason: collision with root package name */
    private String[] f1808a = {"最热", "最新", "分享榜", "高清", "热搜", "套图", "性感"};
    private int[] b;
    private /* synthetic */ HomepageFragment c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public p(HomepageFragment homepageFragment, FragmentManager fragmentManager) {
        super(fragmentManager);
        this.c = homepageFragment;
        int[] iArr = new int[7];
        iArr[1] = 1;
        iArr[2] = 2;
        iArr[3] = 3;
        iArr[4] = 4;
        iArr[5] = 5;
        iArr[6] = 6;
        this.b = iArr;
    }

    public final int getCount() {
        return this.f1808a.length;
    }

    public final Fragment getItem(int i) {
        b.a(HomepageFragment.e, "getItem " + i);
        Bundle bundle = new Bundle();
        bundle.putInt("list_id", this.b[i]);
        Fragment instantiate = Fragment.instantiate(this.c.getActivity(), WallpaperListFragment.class.getName(), bundle);
        b.a(HomepageFragment.e, "create fragment f = " + instantiate);
        return instantiate;
    }

    public final CharSequence getPageTitle(int i) {
        return this.f1808a[i];
    }
}
