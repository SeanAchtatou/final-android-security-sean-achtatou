package com.shoujiduoduo.wallpaper.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;
import com.shoujiduoduo.wallpaper.a.h;
import com.shoujiduoduo.wallpaper.a.j;
import com.shoujiduoduo.wallpaper.kernel.a;
import com.shoujiduoduo.wallpaper.kernel.d;
import com.shoujiduoduo.wallpaper.utils.a.b;
import com.shoujiduoduo.wallpaper.utils.ah;
import com.shoujiduoduo.wallpaper.utils.f;
import java.io.File;

public abstract class FullScreenPicActivity extends Activity {
    /* access modifiers changed from: private */
    public static final String n = FullScreenPicActivity.class.getSimpleName();

    /* renamed from: a  reason: collision with root package name */
    protected a f1739a = a.LOADING;
    protected j b = null;
    protected int c;
    protected PopupWindow d;
    protected b e = null;
    protected PopupWindow f;
    protected PopupWindow g;
    protected PopupWindow h;
    protected TextView i;
    protected TextView j;
    protected boolean k = false;
    protected boolean l = false;
    protected ah m;
    /* access modifiers changed from: private */
    public ImageButton o;
    private ImageButton p;
    /* access modifiers changed from: private */
    public boolean q = false;
    private View.OnClickListener r = new ca(this);
    private View.OnClickListener s = new ce(this);
    private View.OnClickListener t = new cf(this);
    private View.OnClickListener u = new cg(this);
    private View.OnClickListener v = new e(this);
    private View.OnTouchListener w;
    private View.OnClickListener x;
    private View.OnClickListener y;
    private View.OnClickListener z;

    public FullScreenPicActivity() {
        new g(this);
        this.w = new h(this);
        this.x = new j(this);
        this.y = new k(this);
        this.z = new cb(this);
    }

    public static Intent a(Uri uri, String str) {
        if (uri.getScheme().equals("file")) {
            uri.getPath().lastIndexOf(46);
        }
        Intent intent = new Intent("android.intent.action.ATTACH_DATA");
        intent.setDataAndType(uri, str);
        intent.putExtra("mimeType", str);
        return intent;
    }

    /* access modifiers changed from: protected */
    public abstract Bitmap a();

    /* access modifiers changed from: protected */
    public abstract Bitmap a(int i2, int i3);

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.widget.PopupWindow.<init>(android.view.View, int, int, boolean):void}
     arg types: [android.view.View, int, int, int]
     candidates:
      ClspMth{android.widget.PopupWindow.<init>(android.content.Context, android.util.AttributeSet, int, int):void}
      ClspMth{android.widget.PopupWindow.<init>(android.view.View, int, int, boolean):void} */
    public void a(int i2) {
        this.o = (ImageButton) findViewById(f.a(getPackageName(), "id", "btn_favorate"));
        j();
        this.o.setOnClickListener(this.u);
        ((TextView) findViewById(f.a(getPackageName(), "id", "textview_favorate_button"))).setOnClickListener(this.u);
        this.p = (ImageButton) findViewById(f.a(getPackageName(), "id", "btn_set_wallpaper_entrance"));
        LinearLayout linearLayout = (LinearLayout) findViewById(i2);
        linearLayout.getViewTreeObserver().addOnGlobalLayoutListener(new cc(this, linearLayout));
        View.OnClickListener onClickListener = ("huawei".equalsIgnoreCase(Build.BRAND) || "honor".equalsIgnoreCase(Build.BRAND) || "samsung".equalsIgnoreCase(Build.BRAND) || "yulong".equalsIgnoreCase(Build.BRAND)) ? this.s : this.v;
        ((ImageButton) findViewById(f.a(getPackageName(), "id", "btn_one_click_set"))).setOnClickListener(onClickListener);
        ((TextView) findViewById(f.a(getPackageName(), "id", "textview_oneclickset_button"))).setOnClickListener(onClickListener);
        View inflate = LayoutInflater.from(this).inflate(f.a(getPackageName(), "layout", "wallpaperdd_set_wallpaper_popup_window"), (ViewGroup) null);
        this.d = new PopupWindow(inflate, -2, -2, false);
        this.d.setBackgroundDrawable(getResources().getDrawable(f.a(getPackageName(), "drawable", "wallpaperdd_duoduo_translucent")));
        this.d.setFocusable(true);
        inflate.setOnTouchListener(this.w);
        if ("samsung".equalsIgnoreCase(Build.BRAND)) {
            inflate.findViewById(f.a(getPackageName(), "id", "btn_system_setting_layout")).setVisibility(8);
        } else {
            ((ImageButton) inflate.findViewById(f.a(getPackageName(), "id", "btn_open_system_setting"))).setOnClickListener(this.x);
            ((TextView) inflate.findViewById(f.a(getPackageName(), "id", "text_open_system_setting"))).setOnClickListener(this.x);
        }
        ((ImageButton) inflate.findViewById(f.a(getPackageName(), "id", "btn_set_one_screen_wallpaper"))).setOnClickListener(this.v);
        ((TextView) inflate.findViewById(f.a(getPackageName(), "id", "text_set_single_screen_wallpaper"))).setOnClickListener(this.v);
        if ("samsung".equalsIgnoreCase(Build.BRAND) || "meizu".equalsIgnoreCase(Build.BRAND) || "ZTE".equalsIgnoreCase(Build.BRAND) || "nubia".equalsIgnoreCase(Build.BRAND)) {
            inflate.findViewById(f.a(getPackageName(), "id", "btn_set_scroll_screen_wallpaper_layout")).setVisibility(8);
        } else {
            ((ImageButton) inflate.findViewById(f.a(getPackageName(), "id", "btn_set_scroll_screen_wallpaper"))).setOnClickListener(this.y);
            ((TextView) inflate.findViewById(f.a(getPackageName(), "id", "text_set_scroll_screen"))).setOnClickListener(this.y);
        }
        this.e = com.shoujiduoduo.wallpaper.utils.a.a.a(this);
        if (this.e != null) {
            com.shoujiduoduo.wallpaper.kernel.b.a(n, "LockScreenHandler class is: " + this.e.getClass().getSimpleName());
            ((ImageButton) inflate.findViewById(f.a(getPackageName(), "id", "btn_set_lockscreen"))).setOnClickListener(this.z);
            ((TextView) inflate.findViewById(f.a(getPackageName(), "id", "text_set_lockscreen"))).setOnClickListener(this.z);
        } else {
            inflate.findViewById(f.a(getPackageName(), "id", "btn_set_lockscreen_layout")).setVisibility(8);
        }
        cd cdVar = new cd(this);
        this.p.setOnClickListener(cdVar);
        ((TextView) findViewById(f.a(getPackageName(), "id", "text_set_as_button"))).setOnClickListener(cdVar);
        ((ImageButton) findViewById(f.a(getPackageName(), "id", "btn_set_wallpaper_preview"))).setOnClickListener(this.t);
        ((TextView) findViewById(f.a(getPackageName(), "id", "textview_preview_button"))).setOnClickListener(this.t);
        ((ImageButton) findViewById(f.a(getPackageName(), "id", "btn_open_pic_album"))).setOnClickListener(this.r);
        ((TextView) findViewById(f.c("R.id.text_similar_pics"))).setOnClickListener(this.r);
    }

    /* access modifiers changed from: protected */
    public abstract void a(Bitmap bitmap);

    /* access modifiers changed from: protected */
    public void a(ImageButton imageButton) {
        if (this.b != null) {
            if (this.b.k > 0) {
                if (this.f1739a == a.LOAD_FINISHED) {
                    a(this.b);
                }
                new i(this, this, false, this.b.c, this.b.e, this.b.h, this.b.k).showAsDropDown(imageButton);
                return;
            }
            a(this.b);
            if (this.b.e != null && this.b.e.length() > 0) {
                File file = new File(this.b.e);
                if (file.exists()) {
                    Intent intent = new Intent("android.intent.action.SEND");
                    intent.putExtra("android.intent.extra.STREAM", Uri.fromFile(file));
                    intent.setType("image/*");
                    intent.putExtra("android.intent.extra.TEXT", getResources().getString(f.c("R.string.wallpaperdd_text_share_message")));
                    startActivity(Intent.createChooser(intent, "分享到"));
                    return;
                }
            }
        }
        Toast.makeText(this, "图片分享失败，请稍后再试。", 0).show();
    }

    /* access modifiers changed from: protected */
    public abstract void a(j jVar);

    /* access modifiers changed from: protected */
    public abstract Bitmap b();

    /* access modifiers changed from: protected */
    public abstract void c();

    /* access modifiers changed from: protected */
    public abstract void d();

    /* access modifiers changed from: protected */
    public abstract void e();

    /* access modifiers changed from: protected */
    public abstract void f();

    /* access modifiers changed from: protected */
    public abstract void g();

    /* access modifiers changed from: protected */
    public abstract void h();

    /* access modifiers changed from: protected */
    public abstract d i();

    /* access modifiers changed from: protected */
    public void j() {
        if (this.b != null) {
            if (h.d().c(this.b.k)) {
                this.o.setImageDrawable(getResources().getDrawable(f.c("R.drawable.wallpaperdd_btn_remove_favorate")));
                this.q = true;
                return;
            }
            this.o.setImageDrawable(getResources().getDrawable(f.c("R.drawable.wallpaperdd_btn_add_favorate")));
            this.q = false;
        }
    }

    /* access modifiers changed from: protected */
    public void k() {
        if (this.f1739a == a.LOAD_FINISHED) {
            a(this.b);
            if (this.b.e != null && this.b.e.length() > 0) {
                File file = new File(this.b.e);
                if (file.exists()) {
                    Intent intent = new Intent("android.intent.action.SEND");
                    intent.putExtra("android.intent.extra.STREAM", Uri.fromFile(file));
                    intent.setType("image/*");
                    intent.putExtra("android.intent.extra.TEXT", getResources().getString(f.c("R.string.wallpaperdd_text_share_message")));
                    startActivity(Intent.createChooser(intent, "分享到"));
                    return;
                }
            }
            Toast.makeText(this, getResources().getString(f.c("R.string.wallpaperdd_text_share_when_pic_not_ready")), 0).show();
            return;
        }
        Toast.makeText(this, "图片还没加载完毕，请稍后再设置。", 0).show();
    }
}
