package com.shoujiduoduo.wallpaper.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Handler;
import com.shoujiduoduo.wallpaper.a.g;
import com.shoujiduoduo.wallpaper.a.k;
import com.shoujiduoduo.wallpaper.a.m;

final class ah implements g {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public int f1754a;
    /* access modifiers changed from: private */
    public ProgressDialog b;
    private Handler c = new ai(this);
    /* access modifiers changed from: private */
    public /* synthetic */ WallpaperListFragment d;

    public ah(WallpaperListFragment wallpaperListFragment, int i) {
        this.d = wallpaperListFragment;
        this.f1754a = 800000000 + i;
        k a2 = m.b().a(this.f1754a);
        a2.a(this);
        if (a2.a() == 0) {
            a2.d();
            this.b = new ProgressDialog(wallpaperListFragment.getActivity());
            this.b.setCancelable(false);
            this.b.setIndeterminate(false);
            this.b.setTitle("");
            this.b.setMessage("正在获取图片，请稍候...");
            this.b.show();
            return;
        }
        Intent intent = new Intent(wallpaperListFragment.getActivity(), WallpaperActivity.class);
        intent.putExtra("listid", this.f1754a);
        intent.putExtra("serialno", 0);
        wallpaperListFragment.getActivity().startActivity(intent);
    }

    public final void a(k kVar, int i) {
        this.c.sendMessage(this.c.obtainMessage(14001, i, 0));
    }
}
