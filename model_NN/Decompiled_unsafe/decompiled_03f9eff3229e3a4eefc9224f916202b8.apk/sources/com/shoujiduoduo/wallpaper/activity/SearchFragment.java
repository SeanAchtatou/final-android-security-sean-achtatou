package com.shoujiduoduo.wallpaper.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListAdapter;
import com.shoujiduoduo.wallpaper.a.e;
import com.shoujiduoduo.wallpaper.a.f;
import com.shoujiduoduo.wallpaper.kernel.b;
import java.util.ArrayList;

public class SearchFragment extends Fragment implements f {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final String f1743a = SearchFragment.class.getSimpleName();
    /* access modifiers changed from: private */
    public e b;
    private GridView c;
    private ao d;
    /* access modifiers changed from: private */
    public Button e = null;
    /* access modifiers changed from: private */
    public boolean f = false;
    /* access modifiers changed from: private */
    public View g = null;
    private View.OnClickListener h = new am(this);

    private void a(View view) {
        if (view.getBackground() != null) {
            view.getBackground().setCallback(null);
        }
        if ((view instanceof ViewGroup) && !(view instanceof AdapterView)) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= ((ViewGroup) view).getChildCount()) {
                    ((ViewGroup) view).removeAllViews();
                    return;
                } else {
                    a(((ViewGroup) view).getChildAt(i2));
                    i = i2 + 1;
                }
            }
        }
    }

    public void a(ArrayList arrayList, int i) {
        if (this.c.getVisibility() == 0) {
            this.d.notifyDataSetChanged();
        }
    }

    public void onCreate(Bundle bundle) {
        b.a(f1743a, "SearchFragment onCreate");
        super.onCreate(bundle);
        this.b = new e(null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        b.a(f1743a, "SearchFragment onCreateView");
        this.g = layoutInflater.inflate(com.shoujiduoduo.wallpaper.utils.f.c("R.layout.wallpaperdd_search_fragment_layout"), viewGroup, false);
        this.b.a(this);
        this.g.findViewById(com.shoujiduoduo.wallpaper.utils.f.c("R.id.search_hint_panel"));
        this.c = (GridView) this.g.findViewById(com.shoujiduoduo.wallpaper.utils.f.c("R.id.hotKeywordGrid"));
        this.e = (Button) this.g.findViewById(com.shoujiduoduo.wallpaper.utils.f.c("R.id.search_button"));
        if (this.e != null) {
            this.e.setOnClickListener(this.h);
        }
        this.d = new ao(this);
        this.c.setAdapter((ListAdapter) this.d);
        this.c.setOnItemClickListener(new an(this));
        b.a(f1743a, "hot keyword retrieve data.");
        this.b.a();
        return this.g;
    }

    public void onDestroyView() {
        super.onDestroyView();
        if (this.b != null) {
            this.b.a((f) null);
        }
        if (this.e != null) {
            this.e.setOnClickListener(null);
            this.e = null;
        }
        if (this.c != null) {
            this.c.setOnItemClickListener(null);
            this.c = null;
        }
        if (this.d != null) {
            this.d = null;
        }
        a(this.g);
        this.g = null;
        System.gc();
    }
}
