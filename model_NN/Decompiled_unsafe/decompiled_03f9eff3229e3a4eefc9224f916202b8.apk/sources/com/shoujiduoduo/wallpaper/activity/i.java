package com.shoujiduoduo.wallpaper.activity;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.view.ViewCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import com.shoujiduoduo.wallpaper.utils.f;
import java.util.ArrayList;
import java.util.HashMap;

public class i extends PopupWindow {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Context f1802a;
    private ListView b;
    /* access modifiers changed from: private */
    public View c;
    /* access modifiers changed from: private */
    public String d;
    /* access modifiers changed from: private */
    public String e;
    /* access modifiers changed from: private */
    public int f;
    /* access modifiers changed from: private */
    public String g;
    private ArrayList h = new ArrayList();
    private AdapterView.OnItemClickListener i = new m(this);
    /* access modifiers changed from: private */
    public /* synthetic */ FullScreenPicActivity j;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public i(FullScreenPicActivity fullScreenPicActivity, Context context, boolean z, String str, String str2, String str3, int i2) {
        super(context);
        this.j = fullScreenPicActivity;
        this.f1802a = context;
        this.d = str;
        this.f = i2;
        this.e = str2;
        this.g = str3;
        this.c = ((LayoutInflater) context.getSystemService("layout_inflater")).inflate(f.c("R.layout.wallpaperdd_share_menu"), (ViewGroup) null);
        this.c.setBackgroundColor(ViewCompat.MEASURED_SIZE_MASK);
        this.b = (ListView) this.c.findViewById(f.c("R.id.menu_list"));
        this.b.setBackgroundColor(ViewCompat.MEASURED_SIZE_MASK);
        this.b.setDivider(this.f1802a.getResources().getDrawable(f.c("R.drawable.wallpaperdd_share_menu_divider_color")));
        HashMap hashMap = new HashMap();
        hashMap.put("PIC", Integer.valueOf(f.c("R.drawable.wallpaperdd_weixin_normal")));
        hashMap.put("TEXT", "微信");
        this.h.add(hashMap);
        HashMap hashMap2 = new HashMap();
        hashMap2.put("PIC", Integer.valueOf(f.c("R.drawable.wallpaperdd_weixin_sns_normal")));
        hashMap2.put("TEXT", "微信朋友圈");
        this.h.add(hashMap2);
        HashMap hashMap3 = new HashMap();
        hashMap3.put("PIC", Integer.valueOf(f.c("R.drawable.wallpaperdd_qq_normal")));
        hashMap3.put("TEXT", "QQ好友");
        this.h.add(hashMap3);
        HashMap hashMap4 = new HashMap();
        hashMap4.put("PIC", Integer.valueOf(f.c("R.drawable.wallpaperdd_qzone_normal")));
        hashMap4.put("TEXT", "QQ空间");
        this.h.add(hashMap4);
        HashMap hashMap5 = new HashMap();
        hashMap5.put("PIC", Integer.valueOf(f.c("R.drawable.wallpaperdd_sms_normal")));
        hashMap5.put("TEXT", "手机短信");
        this.h.add(hashMap5);
        HashMap hashMap6 = new HashMap();
        hashMap6.put("TEXT", "分享原图");
        this.h.add(hashMap6);
        this.b.setAdapter((ListAdapter) new o(this, context, this.h, f.c("R.layout.wallpaperdd_share_menu_item"), new String[]{"PIC", "TEXT"}, new int[]{f.c("R.id.menu_logos"), f.c("R.id.menu_item_text")}));
        this.b.setItemsCanFocus(false);
        this.b.setChoiceMode(2);
        this.b.setOnItemClickListener(this.i);
        setContentView(this.c);
        setWidth(-2);
        setHeight(-2);
        setFocusable(true);
        if (z) {
            setAnimationStyle(f.c("R.style.wallpaperdd_menuPopupStyle"));
        }
        setBackgroundDrawable(new ColorDrawable(-1342177280));
        this.c.setOnTouchListener(new n(this));
    }
}
