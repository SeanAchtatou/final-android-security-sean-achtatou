package com.shoujiduoduo.wallpaper.activity;

import android.view.View;
import com.shoujiduoduo.wallpaper.utils.q;
import com.umeng.analytics.b;

final class bf implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ WallpaperActivity f1771a;

    bf(WallpaperActivity wallpaperActivity) {
        this.f1771a = wallpaperActivity;
    }

    public final void onClick(View view) {
        q.b(this.f1771a.b.k, this.f1771a.b.n, this.f1771a.u.b());
        b.b(this.f1771a, "CLICK_SHARE");
        this.f1771a.a(this.f1771a.z);
    }
}
