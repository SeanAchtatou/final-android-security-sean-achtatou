package com.shoujiduoduo.wallpaper.activity;

import android.view.View;
import android.widget.Toast;
import com.shoujiduoduo.wallpaper.utils.ah;
import com.shoujiduoduo.wallpaper.utils.f;
import com.umeng.analytics.b;

final class ca implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ FullScreenPicActivity f1790a;

    ca(FullScreenPicActivity fullScreenPicActivity) {
        this.f1790a = fullScreenPicActivity;
    }

    public final void onClick(View view) {
        if (this.f1790a.b != null && this.f1790a.b.h != null && this.f1790a.b.h.length() != 0) {
            if (this.f1790a instanceof WallpaperActivity) {
                b.b(this.f1790a, "CLICK_SIMILAR_PIC");
            }
            if (this.f1790a.m == null) {
                this.f1790a.m = new ah(this.f1790a, this.f1790a.b.h, this.f1790a.c);
                this.f1790a.m.setAnimationStyle(f.c("R.style.wallpaperdd_similar_pic_popup_anim_style"));
            } else {
                this.f1790a.m.a(this.f1790a.b.h);
            }
            this.f1790a.g();
        } else if (this.f1790a.b == null) {
            Toast.makeText(this.f1790a, "很抱歉，获取类似图片失败，请换张图片再试。", 0).show();
        } else {
            if (this.f1790a.b.h != null) {
                this.f1790a.b.h.length();
            }
            Toast.makeText(this.f1790a, "很抱歉，获取类似图片失败，请换张图片再试。", 0).show();
        }
    }
}
