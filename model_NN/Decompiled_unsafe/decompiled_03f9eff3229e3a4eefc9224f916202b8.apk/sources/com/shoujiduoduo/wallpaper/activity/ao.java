package com.shoujiduoduo.wallpaper.activity;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.shoujiduoduo.wallpaper.utils.f;

final class ao extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    private LayoutInflater f1760a;
    private /* synthetic */ SearchFragment b;

    public ao(SearchFragment searchFragment) {
        this.b = searchFragment;
        this.f1760a = LayoutInflater.from(searchFragment.getActivity());
    }

    public final int getCount() {
        return this.b.b.b();
    }

    public final Object getItem(int i) {
        return this.b.b.a(i);
    }

    public final long getItemId(int i) {
        return 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = this.f1760a.inflate(f.c("R.layout.wallpaperdd_hot_keyword_item"), (ViewGroup) null, false);
        }
        ((TextView) view.findViewById(f.c("R.id.hot_keyword_ranklist_item_sn"))).setText(String.valueOf(i + 1));
        ((TextView) view.findViewById(f.c("R.id.hot_keyword_ranklist_item"))).setText(this.b.b.a(i));
        return view;
    }
}
