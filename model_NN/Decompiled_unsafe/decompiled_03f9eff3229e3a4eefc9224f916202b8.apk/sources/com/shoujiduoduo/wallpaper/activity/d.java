package com.shoujiduoduo.wallpaper.activity;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.shoujiduoduo.wallpaper.a.o;
import com.shoujiduoduo.wallpaper.kernel.App;
import com.shoujiduoduo.wallpaper.utils.f;

public class d extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ CategoryFragment f1797a;

    public d(CategoryFragment categoryFragment) {
        this.f1797a = categoryFragment;
    }

    public int getCount() {
        if (this.f1797a.j == null) {
            return 0;
        }
        return this.f1797a.j.size();
    }

    public Object getItem(int i) {
        return null;
    }

    public long getItemId(int i) {
        return 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = LayoutInflater.from(this.f1797a.getActivity()).inflate(f.a(f.d().getPackageName(), "layout", "wallpaperdd_category_thumb"), viewGroup, false);
        }
        ImageView imageView = (ImageView) view.findViewById(f.a(f.d().getPackageName(), "id", "category_image_thumb"));
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(App.f1817a, App.b);
        layoutParams.addRule(13);
        imageView.setLayoutParams(layoutParams);
        ((ImageView) view.findViewById(f.a(f.d().getPackageName(), "id", "category_image_thumb_mask"))).setLayoutParams(layoutParams);
        com.d.a.b.d.a().a(((o) this.f1797a.j.get(i)).b, imageView, this.f1797a.g, new bs(this), new bt(this));
        ((TextView) view.findViewById(f.a(f.d().getPackageName(), "id", "category_text"))).setText(((o) this.f1797a.j.get(i)).c);
        ((TextView) view.findViewById(f.a(f.d().getPackageName(), "id", "category_update_number"))).setText("更新：" + String.valueOf(((o) this.f1797a.j.get(i)).d));
        return view;
    }
}
