package com.shoujiduoduo.wallpaper.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import com.shoujiduoduo.wallpaper.kernel.b;
import com.shoujiduoduo.wallpaper.utils.FixViewPager;
import com.shoujiduoduo.wallpaper.utils.PagerSlidingTabStrip;
import com.shoujiduoduo.wallpaper.utils.ag;
import com.shoujiduoduo.wallpaper.utils.f;

public class HomepageFragment extends Fragment implements ViewPager.OnPageChangeListener {
    /* access modifiers changed from: private */
    public static final String e = HomepageFragment.class.getSimpleName();

    /* renamed from: a  reason: collision with root package name */
    private p f1740a = null;
    private PagerSlidingTabStrip b = null;
    private FixViewPager c = null;
    private View d = null;

    private void a(View view) {
        if (view.getBackground() != null) {
            view.getBackground().setCallback(null);
        }
        if ((view instanceof ViewGroup) && !(view instanceof AdapterView)) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= ((ViewGroup) view).getChildCount()) {
                    ((ViewGroup) view).removeAllViews();
                    return;
                } else {
                    a(((ViewGroup) view).getChildAt(i2));
                    i = i2 + 1;
                }
            }
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        b.a(e, "onCreateView");
        this.f1740a = new p(this, getChildFragmentManager());
        this.d = layoutInflater.inflate(f.c("R.layout.wallpaperdd_homepage_layout"), viewGroup, false);
        this.b = (PagerSlidingTabStrip) this.d.findViewById(f.c("R.id.homepage_top_tab"));
        this.b.setShouldExpand(true);
        this.b.setOnPageChangeListener(this);
        this.c = (FixViewPager) this.d.findViewById(f.c("R.id.homepage_viewpager"));
        this.c.setAdapter(this.f1740a);
        this.b.setViewPager(this.c);
        this.c.setCurrentItem(0);
        return this.d;
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public void onDestroyView() {
        super.onDestroyView();
        if (this.f1740a != null) {
            this.f1740a = null;
        }
        if (this.c != null) {
            this.c = null;
        }
        if (this.b != null) {
            this.b.setOnPageChangeListener(null);
            this.b = null;
        }
        if (this.d != null) {
            a(this.d);
            this.d = null;
        }
        System.gc();
    }

    public void onPageScrollStateChanged(int i) {
    }

    public void onPageScrolled(int i, float f, int i2) {
    }

    public void onPageSelected(int i) {
    }

    public void onPause() {
        super.onPause();
        b.a(e, "onPause");
        com.umeng.analytics.b.b("HomepageFragment");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.wallpaper.utils.ag.a(android.content.Context, java.lang.String, int):int
     arg types: [android.support.v4.app.FragmentActivity, java.lang.String, int]
     candidates:
      com.shoujiduoduo.wallpaper.utils.ag.a(android.content.Context, java.lang.String, long):long
      com.shoujiduoduo.wallpaper.utils.ag.a(android.content.Context, java.lang.String, int):int */
    public void onResume() {
        super.onResume();
        b.a(e, "onResume");
        com.umeng.analytics.b.a("HomepageFragment");
        if (ag.a((Context) getActivity(), "PUSH_ALBUM_ID", 0) != 0) {
            this.c.setCurrentItem(5);
        }
    }
}
