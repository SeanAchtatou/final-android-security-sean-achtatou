package com.shoujiduoduo.wallpaper.activity;

import android.view.View;
import com.shoujiduoduo.wallpaper.a.l;
import com.shoujiduoduo.wallpaper.utils.f;

final class by implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ CategoryListActivity f1787a;

    by(CategoryListActivity categoryListActivity) {
        this.f1787a = categoryListActivity;
    }

    public final void onClick(View view) {
        if (this.f1787a.f != this.f1787a.g) {
            this.f1787a.l.setTextColor(this.f1787a.getResources().getColor(f.a(this.f1787a.getPackageName(), "drawable", "wallpaperdd_green_text_color")));
            this.f1787a.l.setBackgroundResource(f.a(this.f1787a.getPackageName(), "drawable", "wallpaperdd_category_sort_button_bkg_selected"));
            this.f1787a.k.setTextColor(this.f1787a.getResources().getColor(f.a(this.f1787a.getPackageName(), "drawable", "wallpaperdd_grey_text_color")));
            this.f1787a.k.setBackgroundResource(f.a(this.f1787a.getPackageName(), "drawable", "wallpaperdd_category_sort_button_bkg"));
            this.f1787a.a(l.SORT_BY_HOT);
        }
    }
}
