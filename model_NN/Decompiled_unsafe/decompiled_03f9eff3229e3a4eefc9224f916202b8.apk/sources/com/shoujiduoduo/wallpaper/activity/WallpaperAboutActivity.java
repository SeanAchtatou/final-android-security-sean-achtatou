package com.shoujiduoduo.wallpaper.activity;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.Button;
import android.widget.TextView;
import com.shoujiduoduo.wallpaper.utils.f;

public class WallpaperAboutActivity extends Activity {
    public void onCreate(Bundle bundle) {
        String str;
        super.onCreate(bundle);
        setContentView(f.c("R.layout.wallpaperdd_about_activity"));
        String str2 = "";
        try {
            str2 = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
            str = str2.substring(0, str2.lastIndexOf(46));
        } catch (PackageManager.NameNotFoundException e) {
            PackageManager.NameNotFoundException nameNotFoundException = e;
            nameNotFoundException.printStackTrace();
            str = str2;
        }
        TextView textView = (TextView) findViewById(f.c("R.id.about_activity_app_name"));
        if (textView != null) {
            textView.setText("壁纸多多 " + str);
        }
        ((TextView) findViewById(f.c("R.id.about_activity_app_intro"))).setText(String.valueOf(getResources().getString(f.c("R.string.wallpaperdd_app_intro1"))) + "\n" + getResources().getString(f.c("R.string.wallpaperdd_app_intro2")) + "\n" + getResources().getString(f.c("R.string.wallpaperdd_app_intro3")) + "\n" + getResources().getString(f.c("R.string.wallpaperdd_app_intro4")) + "\n" + getResources().getString(f.c("R.string.wallpaperdd_app_intro5")));
        ((Button) findViewById(f.c("R.id.about_activity_back"))).setOnClickListener(new bc(this));
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4) {
            return super.onKeyDown(i, keyEvent);
        }
        finish();
        return true;
    }
}
