package com.shoujiduoduo.wallpaper.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTabHost;
import android.view.KeyEvent;
import android.view.View;
import com.shoujiduoduo.wallpaper.a.h;
import com.shoujiduoduo.wallpaper.a.m;
import com.shoujiduoduo.wallpaper.utils.af;
import com.shoujiduoduo.wallpaper.utils.at;
import com.shoujiduoduo.wallpaper.utils.az;
import com.shoujiduoduo.wallpaper.utils.f;
import com.shoujiduoduo.wallpaper.utils.j;
import com.shoujiduoduo.wallpaper.utils.q;
import com.umeng.analytics.b;

public class NewMainActivity extends FragmentActivity {

    /* renamed from: a  reason: collision with root package name */
    public static boolean f1742a = false;
    public static boolean b = true;
    /* access modifiers changed from: private */
    public static final String c = NewMainActivity.class.getSimpleName();
    private FragmentTabHost d;
    private int[] e = {f.c("R.drawable.wallpaperdd_tab_btn_homepage"), f.c("R.drawable.wallpaperdd_tab_btn_category"), f.c("R.drawable.wallpaperdd_tab_btn_search"), f.c("R.drawable.wallpaperdd_tab_btn_mypictures")};
    private Class[] f = {HomepageFragment.class, CategoryFragment.class, SearchFragment.class, UserListFragment.class};
    /* access modifiers changed from: private */
    public String[] g = {"首页", "分类", "搜索", "我的"};
    /* access modifiers changed from: private */
    public boolean h = false;
    private View.OnClickListener i = new z(this);
    private View.OnClickListener j = new aa(this);

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0157, code lost:
        if (r0 != null) goto L_0x0159;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r11) {
        /*
            r10 = this;
            r2 = 0
            r4 = 1
            r3 = 0
            super.onCreate(r11)
            android.content.Intent r0 = r10.getIntent()
            if (r0 == 0) goto L_0x0019
            java.lang.String r1 = "start_from"
            java.lang.String r0 = r0.getStringExtra(r1)
            if (r0 == 0) goto L_0x0019
            java.lang.String r1 = "notif_click"
            r0.equalsIgnoreCase(r1)
        L_0x0019:
            java.lang.String r0 = "R.layout.wallpaperdd_new_main_activity"
            int r0 = com.shoujiduoduo.wallpaper.utils.f.c(r0)
            r10.setContentView(r0)
            int[] r0 = r10.e
            int r6 = r0.length
            r0 = 16908306(0x1020012, float:2.387728E-38)
            android.view.View r0 = r10.findViewById(r0)
            android.support.v4.app.FragmentTabHost r0 = (android.support.v4.app.FragmentTabHost) r0
            r10.d = r0
            android.support.v4.app.FragmentTabHost r0 = r10.d
            android.support.v4.app.FragmentManager r1 = r10.getSupportFragmentManager()
            java.lang.String r5 = "R.id.realtabcontent"
            int r5 = com.shoujiduoduo.wallpaper.utils.f.c(r5)
            r0.setup(r10, r1, r5)
            android.support.v4.app.FragmentTabHost r0 = r10.d
            com.shoujiduoduo.wallpaper.activity.ad r1 = new com.shoujiduoduo.wallpaper.activity.ad
            r1.<init>(r10)
            r0.setOnTabChangedListener(r1)
            java.lang.String r0 = "R.id.btn_app_settings"
            int r0 = com.shoujiduoduo.wallpaper.utils.f.c(r0)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            com.shoujiduoduo.wallpaper.activity.aj r1 = new com.shoujiduoduo.wallpaper.activity.aj
            r1.<init>(r10, r0)
            r0.setOnClickListener(r1)
            java.lang.String r0 = "R.id.wallpaperdd_back_to_ring"
            int r0 = com.shoujiduoduo.wallpaper.utils.f.c(r0)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            com.shoujiduoduo.wallpaper.activity.ak r1 = new com.shoujiduoduo.wallpaper.activity.ak
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            r5 = r3
        L_0x0072:
            if (r5 < r6) goto L_0x00e4
            android.support.v4.app.FragmentTabHost r0 = r10.d
            r0.setCurrentTab(r3)
            com.shoujiduoduo.wallpaper.activity.NewMainActivity.b = r4
            com.shoujiduoduo.wallpaper.activity.NewMainActivity.f1742a = r3
            boolean r0 = com.shoujiduoduo.wallpaper.activity.NewMainActivity.b
            if (r0 == 0) goto L_0x01a2
            java.lang.String r0 = com.shoujiduoduo.wallpaper.activity.NewMainActivity.c
            java.lang.String r1 = "init gdt_ad_utils"
            com.shoujiduoduo.wallpaper.kernel.b.a(r0, r1)
            com.shoujiduoduo.wallpaper.utils.az r0 = com.shoujiduoduo.wallpaper.utils.az.a(r10)
            r0.b()
            java.lang.String r0 = "no_gdt_ad_channel"
            java.lang.String r0 = com.umeng.analytics.b.c(r10, r0)
            java.lang.String r1 = "no_gdt_ad_version"
            java.lang.String r1 = com.umeng.analytics.b.c(r10, r1)
            if (r0 == 0) goto L_0x00a3
            int r5 = r0.length()
            if (r5 != 0) goto L_0x013c
        L_0x00a3:
            java.lang.String r0 = "R.id.wallpaperdd_wallpaperduoduo_ad_view"
            int r0 = com.shoujiduoduo.wallpaper.utils.f.c(r0)
            android.view.View r0 = r10.findViewById(r0)
            com.shoujiduoduo.wallpaper.activity.al r1 = new com.shoujiduoduo.wallpaper.activity.al
            r1.<init>(r10)
            r0.setOnClickListener(r1)
            java.lang.String r0 = r10.getPackageName()
            java.lang.String r1 = "id"
            java.lang.String r2 = "wallpaperduoduo_btn_install"
            int r0 = com.shoujiduoduo.wallpaper.utils.f.a(r0, r1, r2)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.Button r0 = (android.widget.Button) r0
            android.view.View$OnClickListener r1 = r10.j
            r0.setOnClickListener(r1)
            java.lang.String r0 = r10.getPackageName()
            java.lang.String r1 = "id"
            java.lang.String r2 = "wallpaperdd_exit_ad"
            int r0 = com.shoujiduoduo.wallpaper.utils.f.a(r0, r1, r2)
            android.view.View r0 = r10.findViewById(r0)
            android.widget.ImageButton r0 = (android.widget.ImageButton) r0
            android.view.View$OnClickListener r1 = r10.i
            r0.setOnClickListener(r1)
            return
        L_0x00e4:
            android.view.LayoutInflater r0 = android.view.LayoutInflater.from(r10)
            java.lang.String r1 = "R.layout.wallpaperdd_tab_item"
            int r1 = com.shoujiduoduo.wallpaper.utils.f.c(r1)
            android.view.View r7 = r0.inflate(r1, r2)
            java.lang.String r0 = "R.id.tab_button_imageview"
            int r0 = com.shoujiduoduo.wallpaper.utils.f.c(r0)
            android.view.View r0 = r7.findViewById(r0)
            android.widget.ImageView r0 = (android.widget.ImageView) r0
            java.lang.String r1 = "R.id.tab_button_textview"
            int r1 = com.shoujiduoduo.wallpaper.utils.f.c(r1)
            android.view.View r1 = r7.findViewById(r1)
            android.widget.TextView r1 = (android.widget.TextView) r1
            android.content.res.Resources r8 = r10.getResources()
            int[] r9 = r10.e
            r9 = r9[r5]
            android.graphics.drawable.Drawable r8 = r8.getDrawable(r9)
            r0.setImageDrawable(r8)
            java.lang.String[] r0 = r10.g
            r0 = r0[r5]
            r1.setText(r0)
            android.support.v4.app.FragmentTabHost r0 = r10.d
            java.lang.String[] r1 = r10.g
            r1 = r1[r5]
            android.widget.TabHost$TabSpec r0 = r0.newTabSpec(r1)
            android.widget.TabHost$TabSpec r0 = r0.setIndicator(r7)
            android.support.v4.app.FragmentTabHost r1 = r10.d
            java.lang.Class[] r7 = r10.f
            r7 = r7[r5]
            r1.addTab(r0, r7, r2)
            int r0 = r5 + 1
            r5 = r0
            goto L_0x0072
        L_0x013c:
            if (r1 == 0) goto L_0x00a3
            int r5 = r1.length()
            if (r5 == 0) goto L_0x00a3
            if (r0 == 0) goto L_0x01ad
            java.lang.String r5 = ";"
            java.lang.String[] r0 = r0.split(r5)
            if (r0 == 0) goto L_0x00a3
            r5 = r0
        L_0x014f:
            if (r1 == 0) goto L_0x01ab
            java.lang.String r0 = ";"
            java.lang.String[] r0 = r1.split(r0)
            if (r0 == 0) goto L_0x00a3
        L_0x0159:
            java.lang.String r6 = com.shoujiduoduo.wallpaper.utils.f.j()
            java.lang.String r2 = com.shoujiduoduo.wallpaper.utils.f.i()
            if (r6 == 0) goto L_0x00a3
            if (r2 == 0) goto L_0x00a3
            r1 = r3
        L_0x0166:
            int r7 = r5.length
            if (r1 < r7) goto L_0x0188
            r1 = r3
        L_0x016a:
            r2 = r3
        L_0x016b:
            int r5 = r0.length
            if (r2 < r5) goto L_0x0195
        L_0x016e:
            if (r3 == 0) goto L_0x00a3
            if (r1 == 0) goto L_0x00a3
            long r0 = java.lang.System.currentTimeMillis()
            java.lang.String r2 = "pref_first_start_time"
            long r2 = com.shoujiduoduo.wallpaper.utils.ag.a(r10, r2, r0)
            long r0 = r0 - r2
            r2 = 7200000(0x6ddd00, double:3.5572727E-317)
            int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r0 >= 0) goto L_0x00a3
            com.shoujiduoduo.wallpaper.activity.NewMainActivity.f1742a = r4
            goto L_0x00a3
        L_0x0188:
            r7 = r5[r1]
            boolean r7 = r2.contains(r7)
            if (r7 == 0) goto L_0x0192
            r1 = r4
            goto L_0x016a
        L_0x0192:
            int r1 = r1 + 1
            goto L_0x0166
        L_0x0195:
            r5 = r0[r2]
            boolean r5 = r6.contains(r5)
            if (r5 == 0) goto L_0x019f
            r3 = r4
            goto L_0x016e
        L_0x019f:
            int r2 = r2 + 1
            goto L_0x016b
        L_0x01a2:
            com.shoujiduoduo.wallpaper.utils.at r0 = com.shoujiduoduo.wallpaper.utils.at.a(r10)
            r0.b()
            goto L_0x00a3
        L_0x01ab:
            r0 = r2
            goto L_0x0159
        L_0x01ad:
            r5 = r2
            goto L_0x014f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.wallpaper.activity.NewMainActivity.onCreate(android.os.Bundle):void");
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        af.a().a((Context) null);
        h.d().e();
        m.b().c();
        if (b) {
            az.a(this).a();
        } else {
            at.a(this).a();
        }
        q.f1876a = null;
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 == 4) {
            finish();
            overridePendingTransition(17432578, f.c("R.anim.wallpaperdd_mainactivity_slide_out_of_left"));
            return true;
        } else if (i2 != 82) {
            return super.onKeyDown(i2, keyEvent);
        } else {
            new j(this, true).showAtLocation(findViewById(f.c("R.id.new_root_window")), 81, 0, 0);
            return true;
        }
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        String stringExtra;
        super.onNewIntent(intent);
        if (intent != null && (stringExtra = intent.getStringExtra("start_from")) != null) {
            stringExtra.equalsIgnoreCase("notif_click");
        }
    }

    public void onPause() {
        super.onPause();
        b.a(this);
    }

    /* access modifiers changed from: protected */
    public void onPostResume() {
        super.onPostResume();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        b.b(this);
        View findViewById = findViewById(f.c("R.id.wallpaperdd_wallpaperduoduo_ad_view"));
        if (f.b("com.shoujiduoduo.wallpaper") || this.h) {
            findViewById.setVisibility(8);
        } else {
            findViewById.setVisibility(0);
        }
    }
}
