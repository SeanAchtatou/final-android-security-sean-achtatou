package com.shoujiduoduo.wallpaper.activity;

import android.os.Handler;
import android.os.Message;
import com.shoujiduoduo.wallpaper.a.o;
import com.shoujiduoduo.wallpaper.kernel.b;
import com.shoujiduoduo.wallpaper.utils.f;
import java.util.ArrayList;

final class bq extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ CategoryFragment f1781a;

    bq(CategoryFragment categoryFragment) {
        this.f1781a = categoryFragment;
    }

    public final void handleMessage(Message message) {
        int i;
        switch (message.what) {
            case 2001:
                b.a(CategoryFragment.b, "MSG_UPDATE_CATEGORY_INFO received.");
                if (this.f1781a.c != null) {
                    this.f1781a.j = (ArrayList) message.obj;
                    if (this.f1781a.j != null && f.m()) {
                        int i2 = 0;
                        while (true) {
                            i = i2;
                            if (i >= this.f1781a.j.size()) {
                                i = -1;
                            } else if (((o) this.f1781a.j.get(i)).f1728a != 11) {
                                i2 = i + 1;
                            }
                        }
                        if (i >= 0) {
                            this.f1781a.j.remove(i);
                        }
                    }
                    this.f1781a.e.setVisibility(4);
                    if (this.f1781a.j == null || this.f1781a.j.size() == 0) {
                        b.a(CategoryFragment.b, "Show load error.");
                        this.f1781a.f1737a = bu.c;
                        this.f1781a.b();
                        return;
                    }
                    this.f1781a.f1737a = bu.b;
                    this.f1781a.b();
                    b.a(CategoryFragment.b, "show category content");
                    return;
                }
                return;
            default:
                return;
        }
    }
}
