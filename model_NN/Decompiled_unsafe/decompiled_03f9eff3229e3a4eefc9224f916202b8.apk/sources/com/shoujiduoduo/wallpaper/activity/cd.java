package com.shoujiduoduo.wallpaper.activity;

import android.view.View;
import com.umeng.analytics.b;

final class cd implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ FullScreenPicActivity f1793a;

    cd(FullScreenPicActivity fullScreenPicActivity) {
        this.f1793a = fullScreenPicActivity;
    }

    public final void onClick(View view) {
        if (this.f1793a instanceof WallpaperActivity) {
            b.b(this.f1793a, "CLICK_SET_AS");
        }
        this.f1793a.d();
    }
}
