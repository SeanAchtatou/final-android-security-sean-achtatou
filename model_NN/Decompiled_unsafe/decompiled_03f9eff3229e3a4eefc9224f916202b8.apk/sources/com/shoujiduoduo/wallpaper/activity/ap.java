package com.shoujiduoduo.wallpaper.activity;

import android.app.AlertDialog;
import android.view.View;
import com.shoujiduoduo.wallpaper.a.j;

final class ap implements View.OnClickListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public /* synthetic */ UserListFragment f1761a;

    ap(UserListFragment userListFragment) {
        this.f1761a = userListFragment;
    }

    public final void onClick(View view) {
        j jVar = (j) view.getTag();
        if (jVar != null) {
            new AlertDialog.Builder(this.f1761a.getActivity()).setTitle("提示").setMessage("确认要移除收藏图片“" + jVar.h + "”吗？").setIcon(17301543).setPositiveButton("确定", new aq(this, jVar)).setNegativeButton("取消", new ar(this)).show();
        }
    }
}
