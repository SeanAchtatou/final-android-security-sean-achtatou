package com.shoujiduoduo.wallpaper.activity;

import android.view.View;
import android.widget.Toast;
import com.shoujiduoduo.wallpaper.a.h;
import com.shoujiduoduo.wallpaper.a.j;
import com.shoujiduoduo.wallpaper.kernel.a;
import com.umeng.analytics.b;

final class g implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ FullScreenPicActivity f1800a;

    g(FullScreenPicActivity fullScreenPicActivity) {
        this.f1800a = fullScreenPicActivity;
    }

    public final void onClick(View view) {
        if (this.f1800a.d != null && this.f1800a.d.isShowing()) {
            this.f1800a.e();
        }
        if (this.f1800a.b != null) {
            if (this.f1800a instanceof WallpaperActivity) {
                b.b(this.f1800a, "CLICK_SET_SINGLE");
            }
            h.d().a(this.f1800a.b);
            if (this.f1800a.f1739a == a.LOAD_FINISHED) {
                l lVar = new l(this.f1800a);
                lVar.f1805a = "正在设置壁纸...";
                lVar.b = this.f1800a.b.k;
                lVar.c = new j(this.f1800a.b);
                lVar.d = false;
                lVar.execute(this.f1800a.a());
                this.f1800a.c();
                return;
            }
            Toast.makeText(this.f1800a, "图片还没加载完毕，请稍后再设置。", 0).show();
        }
    }
}
