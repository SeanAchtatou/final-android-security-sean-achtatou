package com.shoujiduoduo.wallpaper.activity;

import android.view.View;
import android.widget.Toast;
import com.shoujiduoduo.wallpaper.utils.f;

final class ax implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ y f1767a;

    ax(y yVar) {
        this.f1767a = yVar;
    }

    public final void onClick(View view) {
        if (f.b("com.shoujiduoduo.wallpaper")) {
            Toast.makeText(this.f1767a.f1815a.getActivity(), "请使用“壁纸多多”自动更换壁纸。", 0).show();
        } else {
            Toast.makeText(this.f1767a.f1815a.getActivity(), "请安装壁纸多多。", 0).show();
        }
    }
}
