package com.shoujiduoduo.wallpaper.activity;

import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.shoujiduoduo.wallpaper.kernel.b;

final class bn extends WebViewClient {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ BdImgActivity f1778a;

    bn(BdImgActivity bdImgActivity) {
        this.f1778a = bdImgActivity;
    }

    public final void onPageFinished(WebView webView, String str) {
        b.a(BdImgActivity.f1736a, "onPageFinished. goback = " + webView.canGoBack() + ", forward = " + webView.canGoForward());
        b.a(BdImgActivity.f1736a, "onPageFinished. url = " + str);
        this.f1778a.j = str;
        this.f1778a.d.setEnabled(webView.canGoBack());
        this.f1778a.e.setEnabled(webView.canGoForward());
    }

    public final void onReceivedError(WebView webView, int i, String str, String str2) {
    }

    public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
        b.a(BdImgActivity.f1736a, "jump url:" + str);
        if (!str.startsWith("wandoujia") && !str.endsWith("apk")) {
            webView.loadUrl(str);
        }
        return true;
    }
}
