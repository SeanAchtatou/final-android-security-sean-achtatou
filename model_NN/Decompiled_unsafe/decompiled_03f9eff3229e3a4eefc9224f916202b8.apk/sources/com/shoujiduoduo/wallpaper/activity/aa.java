package com.shoujiduoduo.wallpaper.activity;

import android.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import com.shoujiduoduo.wallpaper.utils.f;

final class aa implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ NewMainActivity f1747a;

    aa(NewMainActivity newMainActivity) {
        this.f1747a = newMainActivity;
    }

    public final void onClick(View view) {
        AlertDialog create = new AlertDialog.Builder(this.f1747a).create();
        create.show();
        create.getWindow().setContentView(f.a(this.f1747a.getPackageName(), "layout", "wallpaperdd_downapp_alert_dialog"));
        create.findViewById(f.a(this.f1747a.getPackageName(), "id", "btn_install_wallpaperapp")).setOnClickListener(new ab(this, create));
        create.findViewById(f.a(this.f1747a.getPackageName(), "id", "btn_cancel_install_wallpaperapp")).setOnClickListener(new ac(this, create));
        ((Button) create.findViewById(f.a(this.f1747a.getPackageName(), "id", "btn_install_wallpaperapp"))).performClick();
    }
}
