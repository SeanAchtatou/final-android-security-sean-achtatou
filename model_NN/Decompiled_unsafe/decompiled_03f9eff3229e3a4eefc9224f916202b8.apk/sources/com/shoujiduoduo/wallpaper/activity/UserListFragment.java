package com.shoujiduoduo.wallpaper.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;
import com.d.a.b.c;
import com.shoujiduoduo.wallpaper.a.h;
import com.shoujiduoduo.wallpaper.a.i;
import com.shoujiduoduo.wallpaper.kernel.App;
import com.shoujiduoduo.wallpaper.kernel.b;
import com.shoujiduoduo.wallpaper.utils.f;
import com.sina.weibo.sdk.register.mobile.SelectCountryActivity;

public class UserListFragment extends Fragment implements i {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final String f1744a = UserListFragment.class.getSimpleName();
    private View b;
    /* access modifiers changed from: private */
    public c c;
    private ListView d;
    /* access modifiers changed from: private */
    public BaseAdapter e;
    private View f = null;
    /* access modifiers changed from: private */
    public View.OnClickListener g = new ap(this);
    /* access modifiers changed from: private */
    public View.OnClickListener h = new as(this);

    private void a(View view) {
        if (view.getBackground() != null) {
            view.getBackground().setCallback(null);
        }
        if ((view instanceof ViewGroup) && !(view instanceof AdapterView)) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= ((ViewGroup) view).getChildCount()) {
                    ((ViewGroup) view).removeAllViews();
                    return;
                } else {
                    a(((ViewGroup) view).getChildAt(i2));
                    i = i2 + 1;
                }
            }
        }
    }

    public void a() {
        b.a(f1744a, "notify user list changed.");
        if (h.d().a() > 1) {
            this.f.setVisibility(0);
        } else {
            this.f.setVisibility(4);
        }
        this.e.notifyDataSetChanged();
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        if (i == 3033) {
            if (i2 == -1) {
                Uri data = intent.getData();
                if (data != null) {
                    b.a("uri", data.toString());
                    Intent intent2 = new Intent(getActivity(), LocalPaperActivity.class);
                    intent2.putExtra("uri", data.toString());
                    intent2.putExtra(SelectCountryActivity.EXTRA_COUNTRY_NAME, "用户本地图片");
                    intent2.putExtra("uploader", String.valueOf(Build.BRAND) + "_" + Build.PRODUCT + "_" + Build.MODEL);
                    startActivity(intent2);
                } else {
                    Toast.makeText(getActivity(), getResources().getString(f.c("R.string.wallpaperdd_toast_fail_load_local_pic")), 0).show();
                }
            } else if (i2 != 0) {
                Toast.makeText(getActivity(), getResources().getString(f.c("R.string.wallpaperdd_toast_fail_load_local_pic")), 0).show();
            }
        }
        super.onActivityResult(i, i2, intent);
    }

    public void onCreate(Bundle bundle) {
        b.a(f1744a, "WallpaperListFragment onCreate");
        super.onCreate(bundle);
        this.c = new c.a().a(false).b(true).c(true).a(Bitmap.Config.RGB_565).c();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        b.a(f1744a, "WallpaperListFragment onCreateView");
        h.d().a(this);
        this.b = layoutInflater.inflate(f.c("R.layout.wallpaperdd_wallpaper_grid_list"), viewGroup, false);
        this.d = (ListView) this.b.findViewById(f.c("R.id.wallpaper_userlistview"));
        this.e = new y(this);
        this.d.setDividerHeight(App.c);
        this.f = layoutInflater.inflate(f.c("R.layout.wallpaperdd_user_img_list_footerview"), (ViewGroup) null);
        this.d.addFooterView(this.f);
        Button button = (Button) this.f.findViewById(f.c("R.id.clear_user_list_button"));
        if (h.d().a() <= 1) {
            this.f.setVisibility(8);
        } else {
            this.f.setVisibility(0);
        }
        button.setOnClickListener(new at(this));
        this.d.setAdapter((ListAdapter) this.e);
        return this.b;
    }

    public void onDestroyView() {
        super.onDestroyView();
        h.d().a((i) null);
        if (this.f != null) {
            this.f = null;
        }
        if (this.d != null) {
            this.d = null;
        }
        if (this.e != null) {
            this.e = null;
        }
        a(this.b);
        this.b = null;
        System.gc();
    }

    public void onPause() {
        super.onPause();
        com.umeng.analytics.b.b("UserListPage");
    }

    public void onResume() {
        super.onResume();
        com.umeng.analytics.b.a("UserListPage");
    }
}
