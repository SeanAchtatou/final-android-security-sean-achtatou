package com.shoujiduoduo.wallpaper.activity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListAdapter;
import android.widget.ProgressBar;
import com.d.a.b.c;
import com.shoujiduoduo.wallpaper.a.m;
import com.shoujiduoduo.wallpaper.a.n;
import com.shoujiduoduo.wallpaper.kernel.App;
import com.shoujiduoduo.wallpaper.kernel.b;
import com.shoujiduoduo.wallpaper.utils.f;
import java.util.ArrayList;

public class CategoryFragment extends Fragment implements n {
    /* access modifiers changed from: private */
    public static final String b = CategoryFragment.class.getSimpleName();

    /* renamed from: a  reason: collision with root package name */
    bu f1737a = bu.f1783a;
    /* access modifiers changed from: private */
    public View c;
    private GridView d;
    /* access modifiers changed from: private */
    public ProgressBar e;
    /* access modifiers changed from: private */
    public ViewGroup f;
    /* access modifiers changed from: private */
    public c g;
    private d h;
    private View.OnClickListener i = new bp(this);
    /* access modifiers changed from: private */
    public ArrayList j;
    private Handler k = new bq(this);

    private void a(View view) {
        if (view.getBackground() != null) {
            view.getBackground().setCallback(null);
        }
        if ((view instanceof ViewGroup) && !(view instanceof AdapterView)) {
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 >= ((ViewGroup) view).getChildCount()) {
                    ((ViewGroup) view).removeAllViews();
                    return;
                } else {
                    a(((ViewGroup) view).getChildAt(i3));
                    i2 = i3 + 1;
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void b() {
        if (this.f1737a == bu.f1783a) {
            this.e.setVisibility(0);
            this.f.setVisibility(4);
            this.d.setVisibility(4);
        } else if (this.f1737a == bu.c) {
            this.e.setVisibility(4);
            this.f.setVisibility(0);
            this.d.setVisibility(4);
        } else if (this.f1737a == bu.b) {
            this.e.setVisibility(4);
            this.f.setVisibility(4);
            this.d.setVisibility(0);
            this.h.notifyDataSetChanged();
        }
    }

    public void a(ArrayList arrayList) {
        b.a(b, "onCategoryInfoUpdate, cate_info = " + arrayList);
        this.k.sendMessage(this.k.obtainMessage(2001, arrayList));
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.g = new c.a().a(false).b(true).c(true).a(Bitmap.Config.RGB_565).c();
        b.a(b, "onCreate");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        b.a(b, "onCreateView");
        this.c = layoutInflater.inflate(f.a(f.d().getPackageName(), "layout", "wallpaperdd_category_grid_list"), viewGroup, false);
        this.d = (GridView) this.c.findViewById(f.a(f.d().getPackageName(), "id", "category_gridview"));
        this.d.setColumnWidth(App.f1817a);
        this.d.setHorizontalSpacing(App.c);
        this.d.setVerticalSpacing(App.c);
        this.h = new d(this);
        this.d.setAdapter((ListAdapter) this.h);
        this.f = (ViewGroup) this.c.findViewById(f.a(f.d().getPackageName(), "id", "category_info_load_failed"));
        this.e = (ProgressBar) this.c.findViewById(f.a(f.d().getPackageName(), "id", "progress_loading_category_info"));
        this.f.setOnClickListener(this.i);
        this.d.setOnItemClickListener(new br(this));
        b();
        m.b().a(this);
        m.b().d();
        return this.c;
    }

    public void onDestroyView() {
        super.onDestroyView();
        m.b().a((n) null);
        if (this.d != null) {
            this.d.setOnItemClickListener(null);
            this.d = null;
        }
        if (this.h != null) {
            this.h = null;
        }
        a(this.c);
        this.c = null;
        System.gc();
    }

    public void onPause() {
        super.onPause();
        b.a(b, "onPause");
        com.umeng.analytics.b.b("AllCategoryPage");
    }

    public void onResume() {
        super.onResume();
        b.a(b, "onResume");
        com.umeng.analytics.b.a("AllCategoryPage");
    }
}
