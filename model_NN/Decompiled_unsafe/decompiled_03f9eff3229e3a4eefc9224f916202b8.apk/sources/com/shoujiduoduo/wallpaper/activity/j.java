package com.shoujiduoduo.wallpaper.activity;

import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.Toast;
import com.shoujiduoduo.wallpaper.kernel.a;
import com.umeng.analytics.b;
import java.io.File;

final class j implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ FullScreenPicActivity f1803a;

    j(FullScreenPicActivity fullScreenPicActivity) {
        this.f1803a = fullScreenPicActivity;
    }

    public final void onClick(View view) {
        if (this.f1803a.b == null) {
            Toast.makeText(this.f1803a, "很抱歉，打开系统设置失败。", 0).show();
        } else if (this.f1803a.b == null || this.f1803a.b.c == null || this.f1803a.b.c.length() == 0) {
            Toast.makeText(this.f1803a, "很抱歉，打开系统设置失败。", 0).show();
        } else {
            if (this.f1803a instanceof WallpaperActivity) {
                b.b(this.f1803a, "CLICK_SET_BY_SYS");
            }
            if (this.f1803a.f1739a == a.LOAD_FINISHED) {
                this.f1803a.a(this.f1803a.b);
                File file = new File(this.f1803a.b.e);
                if (!file.exists()) {
                    Toast.makeText(this.f1803a, "很抱歉，打开系统设置失败。", 0).show();
                    return;
                }
                Intent a2 = FullScreenPicActivity.a(Uri.fromFile(file), "image/jpeg");
                a2.addFlags(1);
                this.f1803a.startActivity(Intent.createChooser(a2, "系统设置"));
                return;
            }
            Toast.makeText(this.f1803a, "图片还没加载完毕，请稍后再设置。", 0).show();
        }
    }
}
