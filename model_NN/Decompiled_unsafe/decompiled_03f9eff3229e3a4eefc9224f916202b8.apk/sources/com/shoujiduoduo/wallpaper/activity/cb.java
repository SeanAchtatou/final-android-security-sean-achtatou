package com.shoujiduoduo.wallpaper.activity;

import android.view.View;
import android.widget.Toast;
import com.shoujiduoduo.wallpaper.a.h;
import com.shoujiduoduo.wallpaper.kernel.a;
import java.io.File;

final class cb implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ FullScreenPicActivity f1791a;

    cb(FullScreenPicActivity fullScreenPicActivity) {
        this.f1791a = fullScreenPicActivity;
    }

    public final void onClick(View view) {
        if (this.f1791a.d != null && this.f1791a.d.isShowing()) {
            this.f1791a.e();
        }
        if (this.f1791a.b != null) {
            h.d().a(this.f1791a.b);
            this.f1791a.c();
            if (this.f1791a.f1739a == a.LOAD_FINISHED) {
                try {
                    this.f1791a.a(this.f1791a.b);
                    if (this.f1791a.b.e != null && this.f1791a.b.e.length() > 0) {
                        File file = new File(this.f1791a.b.e);
                        if (this.f1791a.e != null && file.exists()) {
                            this.f1791a.e.a(this.f1791a.b.e);
                            return;
                        }
                    }
                    Toast.makeText(this.f1791a, "加载图片失败，请重试。", 0).show();
                } catch (SecurityException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(this.f1791a, "图片还没加载完毕，请稍后再设置。", 0).show();
            }
        }
    }
}
