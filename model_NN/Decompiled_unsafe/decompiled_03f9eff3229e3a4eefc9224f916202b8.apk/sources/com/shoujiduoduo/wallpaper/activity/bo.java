package com.shoujiduoduo.wallpaper.activity;

import android.content.Intent;
import android.webkit.DownloadListener;
import com.shoujiduoduo.wallpaper.kernel.b;
import com.shoujiduoduo.wallpaper.utils.q;
import com.sina.weibo.sdk.register.mobile.SelectCountryActivity;

final class bo implements DownloadListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ BdImgActivity f1779a;

    private bo(BdImgActivity bdImgActivity) {
        this.f1779a = bdImgActivity;
    }

    /* synthetic */ bo(BdImgActivity bdImgActivity, byte b) {
        this(bdImgActivity);
    }

    public final void onDownloadStart(String str, String str2, String str3, String str4, long j) {
        b.a(BdImgActivity.f1736a, "url = " + str);
        b.a(BdImgActivity.f1736a, "userAgent = " + str2);
        b.a(BdImgActivity.f1736a, "contentDisposition = " + str3);
        b.a(BdImgActivity.f1736a, "mimetype = " + str4);
        if (str4 != null && str4.contains("image/")) {
            q.a(this.f1779a.k, this.f1779a.j, str);
            Intent intent = new Intent(this.f1779a, LocalPaperActivity.class);
            intent.putExtra("uri", str);
            intent.putExtra(SelectCountryActivity.EXTRA_COUNTRY_NAME, String.valueOf(this.f1779a.k) + "_百度搜索");
            intent.putExtra("uploader", "百度搜索");
            this.f1779a.startActivity(intent);
        }
    }
}
