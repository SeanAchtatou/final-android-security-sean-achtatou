package com.shoujiduoduo.wallpaper.activity;

import android.view.MotionEvent;
import android.view.View;
import com.shoujiduoduo.wallpaper.utils.f;

final class n implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ i f1807a;

    n(i iVar) {
        this.f1807a = iVar;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        int top = this.f1807a.c.findViewById(f.c("R.id.menu_layout")).getTop();
        int y = (int) motionEvent.getY();
        if (motionEvent.getAction() == 1 && y < top) {
            this.f1807a.dismiss();
        }
        return true;
    }
}
