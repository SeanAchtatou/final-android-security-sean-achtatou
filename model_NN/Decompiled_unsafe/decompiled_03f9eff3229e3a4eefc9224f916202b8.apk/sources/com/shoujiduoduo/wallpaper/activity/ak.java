package com.shoujiduoduo.wallpaper.activity;

import android.view.View;
import com.shoujiduoduo.wallpaper.utils.f;

final class ak implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ NewMainActivity f1757a;

    ak(NewMainActivity newMainActivity) {
        this.f1757a = newMainActivity;
    }

    public final void onClick(View view) {
        this.f1757a.finish();
        this.f1757a.overridePendingTransition(17432578, f.c("R.anim.wallpaperdd_mainactivity_slide_out_of_left"));
    }
}
