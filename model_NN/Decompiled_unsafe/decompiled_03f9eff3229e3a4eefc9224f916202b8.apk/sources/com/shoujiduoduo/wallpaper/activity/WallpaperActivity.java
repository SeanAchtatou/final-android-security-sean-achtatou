package com.shoujiduoduo.wallpaper.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.method.ScrollingMovementMethod;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.baidu.mobads.CpuInfoManager;
import com.d.a.b.a.b;
import com.d.a.b.d;
import com.shoujiduoduo.wallpaper.a.c;
import com.shoujiduoduo.wallpaper.a.h;
import com.shoujiduoduo.wallpaper.a.j;
import com.shoujiduoduo.wallpaper.a.l;
import com.shoujiduoduo.wallpaper.kernel.a;
import com.shoujiduoduo.wallpaper.utils.HorizontalSlider;
import com.shoujiduoduo.wallpaper.utils.MyImageSlider;
import com.shoujiduoduo.wallpaper.utils.aj;
import com.shoujiduoduo.wallpaper.utils.f;
import com.shoujiduoduo.wallpaper.utils.m;
import com.shoujiduoduo.wallpaper.utils.q;
import com.shoujiduoduo.wallpaper.utils.y;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

public class WallpaperActivity extends FullScreenPicActivity implements y {
    /* access modifiers changed from: private */
    public static final String o = WallpaperActivity.class.getSimpleName();
    /* access modifiers changed from: private */
    public TextView A;
    private float B;
    private float C;
    private float D;
    private float E;
    private Handler F = new bd(this);
    protected boolean n = false;
    private TextView p;
    private MyImageSlider q;
    /* access modifiers changed from: private */
    public ProgressBar r;
    private ImageButton s;
    /* access modifiers changed from: private */
    public HorizontalSlider t;
    /* access modifiers changed from: private */
    public c u;
    private int v;
    /* access modifiers changed from: private */
    public int w;
    private String x;
    private String y;
    /* access modifiers changed from: private */
    public ImageButton z;

    private void a(View view) {
        if (view != null) {
            if (view.getBackground() != null) {
                view.getBackground().setCallback(null);
            }
            if ((view instanceof ViewGroup) && !(view instanceof AdapterView)) {
                int i = 0;
                while (true) {
                    int i2 = i;
                    if (i2 >= ((ViewGroup) view).getChildCount()) {
                        ((ViewGroup) view).removeAllViews();
                        return;
                    } else {
                        a(((ViewGroup) view).getChildAt(i2));
                        i = i2 + 1;
                    }
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public Bitmap a() {
        return this.q.b(true);
    }

    /* access modifiers changed from: protected */
    public Bitmap a(int i, int i2) {
        return this.q.a(i, i2);
    }

    public void a(int i, a aVar) {
        if (aVar == a.LOADING) {
            this.F.sendMessage(this.F.obtainMessage(CpuInfoManager.CHANNEL_PICTURE, i, 0));
        } else if (aVar == a.LOAD_FAILED) {
            this.F.sendMessage(this.F.obtainMessage(1002, i, 0));
        } else if (aVar == a.LOAD_FINISHED) {
            this.F.sendMessage(this.F.obtainMessage(1001, i, 0));
        }
    }

    /* access modifiers changed from: protected */
    public void a(Bitmap bitmap) {
        if (this.q != null && this.q.f1825a && !"ZTE".equalsIgnoreCase(Build.BRAND) && !"nubia".equalsIgnoreCase(Build.BRAND)) {
            bitmap.recycle();
        }
    }

    /* access modifiers changed from: protected */
    public synchronized void a(j jVar) {
        File file = new File(jVar.e);
        if (!file.isFile() || !file.exists()) {
            File a2 = b.a(jVar.c, d.a().d());
            if (a2 != null && a2.exists()) {
                com.shoujiduoduo.wallpaper.kernel.b.a(o, "image cache file path:" + a2.getAbsolutePath());
                if (!file.exists() && m.a(a2, file)) {
                    new aj(this, file);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public Bitmap b() {
        return this.q.b(false);
    }

    public void b(int i) {
        com.shoujiduoduo.wallpaper.kernel.b.a(o, "scroll: onImageChange");
        this.w = i;
        this.b = this.u.a(this.w);
        if (this.b.e == null || this.b.e.length() == 0) {
            this.b.e = String.valueOf(f.a()) + "favorate/" + this.b.k + ".jpg";
        }
        if (this.w >= 0 && this.w < this.u.a()) {
            String str = this.u.a(this.w).h;
            if (this.v > 800000000 && this.v <= 899999999) {
                str = String.valueOf(str) + "(" + (this.w + 1) + "/" + this.u.a() + ")";
            }
            this.p.setText(str);
            j();
            this.t.a();
        }
    }

    /* access modifiers changed from: protected */
    public void c() {
        j a2;
        if (this.u != null && (a2 = this.u.a(this.w)) != null) {
            q.a(a2.k, a2.n, this.u.b());
        }
    }

    /* access modifiers changed from: protected */
    public void d() {
        com.shoujiduoduo.wallpaper.kernel.b.a(o, "mActionPanelHeight = " + this.c);
        LinearLayout linearLayout = (LinearLayout) findViewById(f.c("R.id.btn_set_wallpaper_layout"));
        int paddingBottom = linearLayout.getPaddingBottom();
        int paddingTop = linearLayout.getPaddingTop();
        int paddingRight = linearLayout.getPaddingRight();
        int paddingLeft = linearLayout.getPaddingLeft();
        linearLayout.setBackgroundResource(f.c("R.drawable.wallpaperdd_triangle_button_bkg"));
        linearLayout.setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom);
        this.d.showAtLocation(findViewById(f.c("R.id.wallpaper_activity_layout")), 85, 0, this.c);
        this.q.a(false);
    }

    /* access modifiers changed from: protected */
    public void e() {
        this.d.dismiss();
        LinearLayout linearLayout = (LinearLayout) findViewById(f.c("R.id.btn_set_wallpaper_layout"));
        int paddingBottom = linearLayout.getPaddingBottom();
        int paddingTop = linearLayout.getPaddingTop();
        int paddingRight = linearLayout.getPaddingRight();
        int paddingLeft = linearLayout.getPaddingLeft();
        linearLayout.setBackgroundResource(f.c("R.drawable.wallpaperdd_action_button_bkg"));
        linearLayout.setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom);
        this.q.a(true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.widget.PopupWindow.<init>(android.view.View, int, int, boolean):void}
     arg types: [android.view.View, int, int, int]
     candidates:
      ClspMth{android.widget.PopupWindow.<init>(android.content.Context, android.util.AttributeSet, int, int):void}
      ClspMth{android.widget.PopupWindow.<init>(android.view.View, int, int, boolean):void} */
    /* access modifiers changed from: protected */
    public void f() {
        if (this.v > 800000000 && this.v <= 899999999 && this.A.getVisibility() == 0) {
            this.A.setAnimation(AnimationUtils.loadAnimation(this, f.c("R.anim.wallpaperdd_preview_icon_slide_out")));
            this.A.setVisibility(4);
            this.n = true;
        }
        findViewById(f.c("R.id.wallpaper_title_panel")).setVisibility(4);
        findViewById(f.c("R.id.wallpaper_action_panel")).setVisibility(4);
        findViewById(f.c("R.id.wallpaper_slider")).setVisibility(4);
        if (this.f == null) {
            this.f = new PopupWindow(LayoutInflater.from(this).inflate(f.c("R.layout.wallpaperdd_popup_preview_bottom"), (ViewGroup) null), -2, -2, false);
            this.f.setAnimationStyle(f.c("R.style.wallpaperdd_menuPopupStyle"));
        }
        if (this.g == null) {
            View inflate = LayoutInflater.from(this).inflate(f.c("R.layout.wallpaperdd_popup_preview_top"), (ViewGroup) null);
            this.i = (TextView) inflate.findViewById(f.c("R.id.textview_date"));
            this.j = (TextView) inflate.findViewById(f.c("R.id.textview_time"));
            this.g = new PopupWindow(inflate, -2, -2, false);
            this.g.setAnimationStyle(f.c("R.style.wallpaperdd_previewtop_anim_style"));
        }
        if (this.h == null) {
            this.h = new PopupWindow(LayoutInflater.from(this).inflate(f.c("R.layout.wallpaperdd_popup_preview_duoduo_family"), (ViewGroup) null), -2, -2, false);
            this.h.setAnimationStyle(f.c("R.style.wallpaperdd_previewicon_anim_style"));
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("M月d日 E");
        SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("HH:mm");
        Date date = new Date(System.currentTimeMillis());
        String format = simpleDateFormat.format(date);
        String format2 = simpleDateFormat2.format(date);
        this.i.setText(format);
        this.j.setText(format2);
        this.g.showAtLocation(findViewById(f.c("R.id.wallpaper_activity_layout")), 49, 0, getResources().getDisplayMetrics().heightPixels / 10);
        this.f.showAtLocation(findViewById(f.c("R.id.wallpaper_activity_layout")), 81, 0, 0);
        this.h.showAtLocation(findViewById(f.c("R.id.wallpaper_activity_layout")), 17, 0, 0);
        this.k = true;
        this.q.a(false);
    }

    /* access modifiers changed from: protected */
    public void g() {
        this.m.showAtLocation(findViewById(f.c("R.id.wallpaper_activity_layout")), 51, 0, 0);
    }

    /* access modifiers changed from: protected */
    public void h() {
        if (this.g.isShowing()) {
            this.g.dismiss();
        }
        if (this.f.isShowing()) {
            this.f.dismiss();
        }
        if (this.h.isShowing()) {
            this.h.dismiss();
        }
        findViewById(f.c("R.id.wallpaper_title_panel")).setVisibility(0);
        findViewById(f.c("R.id.wallpaper_action_panel")).setVisibility(0);
        findViewById(f.c("R.id.wallpaper_slider")).setVisibility(0);
        if (this.n && this.v > 800000000 && this.v <= 899999999) {
            this.A.setAnimation(AnimationUtils.loadAnimation(this, f.c("R.anim.wallpaperdd_preview_icon_slide_in")));
            this.A.setVisibility(0);
            this.n = false;
        }
        this.k = false;
        this.q.a(true);
    }

    /* access modifiers changed from: protected */
    public com.shoujiduoduo.wallpaper.kernel.d i() {
        return this.q.getOriginalBmpSize();
    }

    public void m() {
        if (!this.l) {
            if (this.v > 800000000 && this.v <= 899999999 && this.A.getVisibility() == 0) {
                this.A.setAnimation(AnimationUtils.loadAnimation(this, f.c("R.anim.wallpaperdd_preview_icon_slide_out")));
                this.A.setVisibility(4);
            }
            View findViewById = findViewById(f.c("R.id.wallpaper_title_panel"));
            findViewById.setAnimation(AnimationUtils.loadAnimation(this, f.c("R.anim.wallpaperdd_preview_icon_slide_out")));
            findViewById.setVisibility(4);
            View findViewById2 = findViewById(f.c("R.id.wallpaper_action_panel"));
            findViewById2.setAnimation(AnimationUtils.loadAnimation(this, f.c("R.anim.wallpaperdd_preview_icon_slide_out")));
            findViewById2.setVisibility(4);
            View findViewById3 = findViewById(f.c("R.id.wallpaper_slider"));
            findViewById3.setAnimation(AnimationUtils.loadAnimation(this, f.c("R.anim.wallpaperdd_preview_icon_slide_out")));
            findViewById3.setVisibility(4);
            this.l = true;
            return;
        }
        if (this.v > 800000000 && this.v <= 899999999) {
            this.A.setAnimation(AnimationUtils.loadAnimation(this, f.c("R.anim.wallpaperdd_preview_icon_slide_in")));
            this.A.setVisibility(0);
        }
        View findViewById4 = findViewById(f.c("R.id.wallpaper_title_panel"));
        findViewById4.setAnimation(AnimationUtils.loadAnimation(this, f.c("R.anim.wallpaperdd_preview_icon_slide_in")));
        findViewById4.setVisibility(0);
        View findViewById5 = findViewById(f.c("R.id.wallpaper_action_panel"));
        findViewById5.setAnimation(AnimationUtils.loadAnimation(this, f.c("R.anim.wallpaperdd_preview_icon_slide_in")));
        findViewById5.setVisibility(0);
        if (this.f1739a == a.LOAD_FINISHED) {
            View findViewById6 = findViewById(f.c("R.id.wallpaper_slider"));
            findViewById6.setAnimation(AnimationUtils.loadAnimation(this, f.c("R.anim.wallpaperdd_preview_icon_slide_in")));
            findViewById6.setVisibility(0);
        }
        this.l = false;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(f.c("R.layout.wallpaperdd_wallpaper_activity"));
        com.shoujiduoduo.wallpaper.kernel.b.a(o, "ui thread id = " + Thread.currentThread().getId());
        Intent intent = getIntent();
        if (intent != null) {
            this.v = intent.getIntExtra("listid", 0);
            this.w = intent.getIntExtra("serialno", 0);
            this.x = intent.getStringExtra("uploader");
            this.y = intent.getStringExtra("intro");
        }
        com.shoujiduoduo.wallpaper.kernel.b.a(o, "list id = " + this.v + ", mPicNo = " + this.w);
        if (this.v == 999999999) {
            com.shoujiduoduo.wallpaper.kernel.b.a(o, "User List!");
            this.u = h.d();
        } else if (this.v == 999999998) {
            this.u = com.shoujiduoduo.wallpaper.a.m.b().a();
        } else {
            String stringExtra = intent.getStringExtra("sort");
            if (stringExtra != null && stringExtra.equalsIgnoreCase(l.SORT_BY_HOT.toString())) {
                this.u = com.shoujiduoduo.wallpaper.a.m.b().a(this.v, l.SORT_BY_HOT);
            } else if (stringExtra == null || !stringExtra.equalsIgnoreCase(l.SORT_BY_NEW.toString())) {
                this.u = com.shoujiduoduo.wallpaper.a.m.b().a(this.v);
            } else {
                this.u = com.shoujiduoduo.wallpaper.a.m.b().a(this.v, l.SORT_BY_NEW);
            }
        }
        this.b = this.u.a(this.w);
        if (this.b == null) {
            Toast.makeText(this, "很抱歉，打开大图失败", 0).show();
            finish();
            return;
        }
        this.b.e = String.valueOf(f.a()) + "favorate/" + this.b.k + ".jpg";
        this.p = (TextView) findViewById(f.c("R.id.wallpaper_title"));
        if (this.u.a() > 0 && this.w < this.u.a()) {
            String str = this.u.a(this.w).h;
            if (this.v > 800000000 && this.v <= 899999999) {
                str = String.valueOf(str) + "(" + (this.w + 1) + "/" + this.u.a() + ")";
            }
            this.p.setText(str);
        }
        this.s = (ImageButton) findViewById(f.c("R.id.btn_back_to_main"));
        this.s.setOnClickListener(new be(this));
        this.z = (ImageButton) findViewById(f.c("R.id.btn_share"));
        this.z.setOnClickListener(new bf(this));
        this.A = (TextView) findViewById(f.c("R.id.album_intro_text"));
        this.A.setMovementMethod(ScrollingMovementMethod.getInstance());
        if (this.v <= 800000000 || this.v > 899999999) {
            this.A.setVisibility(4);
        } else {
            String str2 = "";
            if (this.x != null && this.x.length() > 0) {
                str2 = "来源: " + this.x + "\n";
            }
            if (this.y != null && this.y.length() > 0) {
                str2 = String.valueOf(str2) + "简介: " + this.y;
            }
            this.A.setText(str2);
            this.A.setAnimation(AnimationUtils.loadAnimation(this, f.c("R.anim.wallpaperdd_preview_icon_slide_in")));
            this.A.setVisibility(0);
            this.A.setOnClickListener(new bg(this));
        }
        a(f.c("R.id.wallpaper_action_panel"));
        this.r = (ProgressBar) findViewById(f.c("R.id.wallpaper_loading_progress"));
        this.t = (HorizontalSlider) findViewById(f.c("R.id.wallpaper_slider"));
        this.q = (MyImageSlider) findViewById(f.c("R.id.wallpaper_full_view"));
        this.q.setListener(this);
        this.t.setListener(this.q);
        this.q.a(this.u, this.w);
    }

    public void onDestroy() {
        com.shoujiduoduo.wallpaper.kernel.b.a(o, "onDestroy");
        super.onDestroy();
        this.u = null;
        if (this.t != null) {
            this.t.setListener(null);
            this.t = null;
        }
        if (this.q != null) {
            this.q.setListener(null);
            this.q = null;
        }
        this.e = null;
        this.d = null;
        if (this.F != null) {
            this.F.removeMessages(1001);
            this.F.removeMessages(CpuInfoManager.CHANNEL_PICTURE);
            this.F.removeMessages(1002);
            this.F.removeMessages(1004);
        }
        a(findViewById(f.c("R.id.wallpaper_activity_layout")));
        System.gc();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 4) {
            finish();
            return true;
        } else if (i != 82) {
            return super.onKeyDown(i, keyEvent);
        } else {
            new com.shoujiduoduo.wallpaper.utils.j(this, true).showAtLocation(findViewById(f.c("R.id.wallpaper_activity_layout")), 81, 0, 0);
            return true;
        }
    }

    public void onPause() {
        super.onPause();
        com.umeng.analytics.b.b("WallpaperActivity");
        com.umeng.analytics.b.a(this);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.m != null && this.m.isShowing()) {
            com.shoujiduoduo.wallpaper.a.m.b().a(this.m.a());
        }
        com.umeng.analytics.b.a("WallpaperActivity");
        com.umeng.analytics.b.b(this);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (!this.k) {
            return super.onTouchEvent(motionEvent);
        }
        if (motionEvent.getAction() == 0) {
            this.B = motionEvent.getX();
            this.C = motionEvent.getY();
            return true;
        } else if (motionEvent.getAction() == 2 || motionEvent.getAction() != 1) {
            return super.onTouchEvent(motionEvent);
        } else {
            this.D = motionEvent.getX();
            this.E = motionEvent.getY();
            if (Math.abs(this.D - this.B) >= 35.0f || Math.abs(this.E - this.C) >= 35.0f) {
                return true;
            }
            h();
            return true;
        }
    }
}
