package com.shoujiduoduo.wallpaper.activity;

import android.app.AlertDialog;
import android.view.View;

final class at implements View.OnClickListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public /* synthetic */ UserListFragment f1764a;

    at(UserListFragment userListFragment) {
        this.f1764a = userListFragment;
    }

    public final void onClick(View view) {
        new AlertDialog.Builder(this.f1764a.getActivity()).setTitle("提示").setMessage("确认要删除所有收藏图片吗？").setIcon(17301543).setPositiveButton("确定", new au(this)).setNegativeButton("取消", new av(this)).show();
    }
}
