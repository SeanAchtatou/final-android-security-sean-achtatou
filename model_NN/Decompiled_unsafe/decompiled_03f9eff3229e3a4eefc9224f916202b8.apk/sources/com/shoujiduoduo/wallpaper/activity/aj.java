package com.shoujiduoduo.wallpaper.activity;

import android.view.View;
import android.widget.ImageButton;
import com.shoujiduoduo.wallpaper.utils.j;

final class aj implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ NewMainActivity f1756a;
    private final /* synthetic */ ImageButton b;

    aj(NewMainActivity newMainActivity, ImageButton imageButton) {
        this.f1756a = newMainActivity;
        this.b = imageButton;
    }

    public final void onClick(View view) {
        new j(this.f1756a, false).showAsDropDown(this.b);
    }
}
