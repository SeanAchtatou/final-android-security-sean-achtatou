package com.shoujiduoduo.wallpaper.activity;

import android.view.View;
import android.widget.Toast;
import com.shoujiduoduo.wallpaper.a.h;
import com.shoujiduoduo.wallpaper.a.j;
import com.shoujiduoduo.wallpaper.kernel.a;
import com.umeng.analytics.b;

final class e implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ FullScreenPicActivity f1798a;

    e(FullScreenPicActivity fullScreenPicActivity) {
        this.f1798a = fullScreenPicActivity;
    }

    public final void onClick(View view) {
        if (this.f1798a.d != null && this.f1798a.d.isShowing()) {
            this.f1798a.e();
        }
        if (this.f1798a.b != null) {
            if (this.f1798a instanceof WallpaperActivity) {
                b.b(this.f1798a, "CLICK_SET_SINGLE");
            }
            h.d().a(this.f1798a.b);
            if (this.f1798a.f1739a == a.LOAD_FINISHED) {
                l lVar = new l(this.f1798a);
                lVar.f1805a = "正在设置壁纸...";
                lVar.b = this.f1798a.b.k;
                lVar.c = new j(this.f1798a.b);
                lVar.execute(this.f1798a.b());
                this.f1798a.c();
                return;
            }
            Toast.makeText(this.f1798a, "图片还没加载完毕，请稍后再设置。", 0).show();
        }
    }
}
