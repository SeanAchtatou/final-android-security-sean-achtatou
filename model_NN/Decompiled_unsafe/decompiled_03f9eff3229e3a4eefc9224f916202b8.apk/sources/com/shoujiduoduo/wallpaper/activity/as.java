package com.shoujiduoduo.wallpaper.activity;

import android.content.Intent;
import android.view.View;
import com.umeng.analytics.b;

final class as implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ UserListFragment f1763a;

    as(UserListFragment userListFragment) {
        this.f1763a = userListFragment;
    }

    public final void onClick(View view) {
        b.b(this.f1763a.getActivity(), "CLICKPIC_IN_LIST_MINE");
        Intent intent = new Intent(this.f1763a.getActivity(), WallpaperActivity.class);
        intent.putExtra("listid", 999999999);
        intent.putExtra("serialno", (Integer) view.getTag());
        this.f1763a.startActivity(intent);
    }
}
