package com.shoujiduoduo.wallpaper.activity;

import android.app.WallpaperManager;
import android.view.View;
import android.widget.Toast;
import com.shoujiduoduo.wallpaper.a.h;
import com.shoujiduoduo.wallpaper.a.j;
import com.shoujiduoduo.wallpaper.kernel.a;
import com.shoujiduoduo.wallpaper.kernel.d;
import com.shoujiduoduo.wallpaper.utils.f;
import com.umeng.analytics.b;

final class ce implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ FullScreenPicActivity f1794a;

    ce(FullScreenPicActivity fullScreenPicActivity) {
        this.f1794a = fullScreenPicActivity;
    }

    public final void onClick(View view) {
        if (this.f1794a.b != null) {
            if (this.f1794a instanceof WallpaperActivity) {
                b.b(this.f1794a, "CLICK_SET_SINGLE");
            }
            if (this.f1794a.f1739a != a.LOAD_FINISHED) {
                Toast.makeText(this.f1794a, "图片还没加载完毕，请稍后再设置。", 0).show();
                return;
            }
            int desiredMinimumWidth = WallpaperManager.getInstance(f.d()).getDesiredMinimumWidth();
            int desiredMinimumHeight = WallpaperManager.getInstance(f.d()).getDesiredMinimumHeight();
            d i = this.f1794a.i();
            if (i == null || i.f1820a <= 0 || i.b <= 0) {
                Toast.makeText(this.f1794a, "图片加载失败，请稍后再设置。", 0).show();
                return;
            }
            float f = ((float) i.f1820a) / ((float) i.b);
            float f2 = 0.0f;
            if (desiredMinimumWidth > 0 && desiredMinimumHeight > 0) {
                f2 = ((float) desiredMinimumWidth) / ((float) desiredMinimumHeight);
            }
            if (f >= f2 || ((double) f) >= 0.9d) {
                int i2 = this.f1794a.getResources().getDisplayMetrics().heightPixels;
                int i3 = (i.f1820a * i2) / i.b;
                h.d().a(this.f1794a.b);
                if (this.f1794a.f1739a == a.LOAD_FINISHED) {
                    l lVar = new l(this.f1794a);
                    lVar.f1805a = "正在设置壁纸...";
                    lVar.b = this.f1794a.b.k;
                    lVar.c = new j(this.f1794a.b);
                    lVar.d = false;
                    lVar.execute(this.f1794a.a(i3, i2));
                    this.f1794a.c();
                    return;
                }
                Toast.makeText(this.f1794a, "图片还没加载完毕，请稍后再设置。", 0).show();
                return;
            }
            h.d().a(this.f1794a.b);
            if (this.f1794a.f1739a == a.LOAD_FINISHED) {
                l lVar2 = new l(this.f1794a);
                lVar2.f1805a = "正在设置壁纸...";
                lVar2.b = this.f1794a.b.k;
                lVar2.c = new j(this.f1794a.b);
                lVar2.d = false;
                lVar2.execute(this.f1794a.a(desiredMinimumWidth, desiredMinimumHeight));
                this.f1794a.c();
                return;
            }
            Toast.makeText(this.f1794a, "图片还没加载完毕，请稍后再设置。", 0).show();
        }
    }
}
