package com.shoujiduoduo.wallpaper.activity;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.Gallery;

public class DuoduoGallery extends Gallery {
    public DuoduoGallery(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        onKeyDown((motionEvent2.getX() > motionEvent.getX() ? 1 : (motionEvent2.getX() == motionEvent.getX() ? 0 : -1)) > 0 ? 21 : 22, null);
        return true;
    }
}
