package com.shoujiduoduo.wallpaper.activity;

import android.widget.TabHost;
import android.widget.TextView;
import com.shoujiduoduo.wallpaper.utils.f;

final class ad implements TabHost.OnTabChangeListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ NewMainActivity f1750a;

    ad(NewMainActivity newMainActivity) {
        this.f1750a = newMainActivity;
    }

    public final void onTabChanged(String str) {
        for (String equalsIgnoreCase : this.f1750a.g) {
            if (equalsIgnoreCase.equalsIgnoreCase(str)) {
                ((TextView) this.f1750a.findViewById(f.c("R.id.main_title_text"))).setText(str);
                return;
            }
        }
    }
}
