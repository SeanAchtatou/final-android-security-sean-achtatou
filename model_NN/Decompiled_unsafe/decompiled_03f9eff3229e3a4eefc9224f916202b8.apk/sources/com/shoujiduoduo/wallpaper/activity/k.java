package com.shoujiduoduo.wallpaper.activity;

import android.view.View;
import android.widget.Toast;
import com.shoujiduoduo.wallpaper.a.h;
import com.shoujiduoduo.wallpaper.a.j;
import com.shoujiduoduo.wallpaper.kernel.a;
import com.umeng.analytics.b;

final class k implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ FullScreenPicActivity f1804a;

    k(FullScreenPicActivity fullScreenPicActivity) {
        this.f1804a = fullScreenPicActivity;
    }

    public final void onClick(View view) {
        if (this.f1804a.d != null && this.f1804a.d.isShowing()) {
            this.f1804a.e();
        }
        if (this.f1804a.b != null) {
            if (this.f1804a instanceof WallpaperActivity) {
                b.b(this.f1804a, "CLICK_SET_MULTI");
            }
            h.d().a(this.f1804a.b);
            if (this.f1804a.f1739a == a.LOAD_FINISHED) {
                l lVar = new l(this.f1804a);
                lVar.f1805a = "正在设置滚屏壁纸...";
                lVar.b = this.f1804a.b.k;
                lVar.c = new j(this.f1804a.b);
                lVar.execute(this.f1804a.a());
                this.f1804a.c();
                return;
            }
            Toast.makeText(this.f1804a, "图片还没加载完毕，请稍后再设置。", 0).show();
        }
    }
}
