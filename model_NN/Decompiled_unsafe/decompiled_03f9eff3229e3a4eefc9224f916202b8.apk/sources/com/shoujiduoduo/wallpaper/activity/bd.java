package com.shoujiduoduo.wallpaper.activity;

import android.os.Handler;
import android.os.Message;
import com.baidu.mobads.CpuInfoManager;
import com.shoujiduoduo.wallpaper.kernel.a;
import com.shoujiduoduo.wallpaper.kernel.b;
import com.shoujiduoduo.wallpaper.utils.q;

final class bd extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ WallpaperActivity f1769a;

    bd(WallpaperActivity wallpaperActivity) {
        this.f1769a = wallpaperActivity;
    }

    public final void handleMessage(Message message) {
        if (this.f1769a.u != null) {
            switch (message.what) {
                case 1001:
                    int i = message.arg1;
                    if (this.f1769a.w >= 0 && this.f1769a.w < this.f1769a.u.a() && i == this.f1769a.u.a(this.f1769a.w).k && this.f1769a.f1739a != a.LOAD_FINISHED) {
                        this.f1769a.f1739a = a.LOAD_FINISHED;
                        this.f1769a.r.setVisibility(4);
                        if (!this.f1769a.l) {
                            this.f1769a.t.setVisibility(0);
                        }
                        b.a(WallpaperActivity.o, "now log download. id = " + i);
                        q.c(this.f1769a.u.a(this.f1769a.w).k, this.f1769a.u.a(this.f1769a.w).n, this.f1769a.u.b());
                        return;
                    }
                    return;
                case 1002:
                    int i2 = message.arg1;
                    if (this.f1769a.w >= 0 && this.f1769a.w < this.f1769a.u.a() && i2 == this.f1769a.u.a(this.f1769a.w).k) {
                        this.f1769a.f1739a = a.LOAD_FAILED;
                        this.f1769a.r.setVisibility(4);
                        this.f1769a.t.setVisibility(4);
                        return;
                    }
                    return;
                case CpuInfoManager.CHANNEL_PICTURE:
                    int i3 = message.arg1;
                    if (this.f1769a.w >= 0 && this.f1769a.w < this.f1769a.u.a() && i3 == this.f1769a.u.a(this.f1769a.w).k) {
                        this.f1769a.f1739a = a.LOADING;
                        this.f1769a.r.setVisibility(0);
                        this.f1769a.t.setVisibility(4);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }
}
