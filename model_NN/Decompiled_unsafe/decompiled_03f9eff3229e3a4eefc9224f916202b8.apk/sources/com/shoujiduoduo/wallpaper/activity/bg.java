package com.shoujiduoduo.wallpaper.activity;

import android.view.View;
import android.view.animation.AnimationUtils;
import com.shoujiduoduo.wallpaper.utils.f;

final class bg implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ WallpaperActivity f1772a;

    bg(WallpaperActivity wallpaperActivity) {
        this.f1772a = wallpaperActivity;
    }

    public final void onClick(View view) {
        this.f1772a.A.setAnimation(AnimationUtils.loadAnimation(this.f1772a, f.c("R.anim.wallpaperdd_preview_icon_slide_out")));
        this.f1772a.A.setVisibility(4);
    }
}
