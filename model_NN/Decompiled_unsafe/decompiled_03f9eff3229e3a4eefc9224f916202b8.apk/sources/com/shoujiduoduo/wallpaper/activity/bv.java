package com.shoujiduoduo.wallpaper.activity;

import android.os.Handler;
import android.os.Message;
import android.widget.RelativeLayout;
import com.shoujiduoduo.wallpaper.kernel.b;

final class bv extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ CategoryListActivity f1784a;

    bv(CategoryListActivity categoryListActivity) {
        this.f1784a = categoryListActivity;
    }

    public final void handleMessage(Message message) {
        if (this.f1784a.f != null) {
            switch (message.what) {
                case 5001:
                    int i = message.arg1;
                    if (i == 31) {
                        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
                        layoutParams.addRule(14);
                        layoutParams.addRule(12);
                        this.f1784a.i.setLayoutParams(layoutParams);
                        this.f1784a.i.setPadding(0, 0, 0, 30);
                        this.f1784a.i.setVisibility(0);
                        this.f1784a.j.setVisibility(4);
                        return;
                    } else if (i == 0 || i == 32) {
                        b.a(CategoryListActivity.f1738a, "list update success.");
                        if (this.f1784a.i.getVisibility() == 0) {
                            b.a(CategoryListActivity.f1738a, "show grid view.");
                            this.f1784a.i.setVisibility(4);
                            this.f1784a.j.setVisibility(4);
                            this.f1784a.d.setVisibility(0);
                            this.f1784a.e.notifyDataSetChanged();
                            return;
                        }
                        return;
                    } else if (i == 1 && this.f1784a.i.getVisibility() == 0) {
                        this.f1784a.i.setVisibility(4);
                        this.f1784a.j.setVisibility(0);
                        this.f1784a.d.setVisibility(4);
                        return;
                    } else {
                        return;
                    }
                default:
                    return;
            }
        }
    }
}
