package com.shoujiduoduo.wallpaper.activity;

import android.webkit.WebChromeClient;
import android.webkit.WebView;
import com.shoujiduoduo.wallpaper.kernel.b;

final class f extends WebChromeClient {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ BdImgActivity f1799a;

    f(BdImgActivity bdImgActivity) {
        this.f1799a = bdImgActivity;
    }

    public final void onProgressChanged(WebView webView, int i) {
        if (i < 100) {
            this.f1799a.g.setVisibility(0);
            this.f1799a.g.setProgress(i);
        }
        if (i == 100) {
            this.f1799a.g.setProgress(0);
        }
    }

    public final void onReceivedTitle(WebView webView, String str) {
        b.a(BdImgActivity.f1736a, "title = " + str);
        this.f1799a.i.setText(str);
    }
}
