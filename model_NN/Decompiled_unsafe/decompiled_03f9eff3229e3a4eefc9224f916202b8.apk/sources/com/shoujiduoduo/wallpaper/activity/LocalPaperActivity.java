package com.shoujiduoduo.wallpaper.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;
import com.d.a.b.c;
import com.d.a.b.d;
import com.shoujiduoduo.wallpaper.a.h;
import com.shoujiduoduo.wallpaper.a.j;
import com.shoujiduoduo.wallpaper.kernel.a;
import com.shoujiduoduo.wallpaper.kernel.b;
import com.shoujiduoduo.wallpaper.utils.HorizontalSlider;
import com.shoujiduoduo.wallpaper.utils.LocalPicView;
import com.shoujiduoduo.wallpaper.utils.aj;
import com.shoujiduoduo.wallpaper.utils.f;
import com.shoujiduoduo.wallpaper.utils.m;
import com.shoujiduoduo.wallpaper.utils.s;
import com.sina.weibo.sdk.register.mobile.SelectCountryActivity;
import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class LocalPaperActivity extends FullScreenPicActivity implements s {
    /* access modifiers changed from: private */
    public static final String v = LocalPaperActivity.class.getSimpleName();
    private float A;
    /* access modifiers changed from: private */
    public int B = 0;
    /* access modifiers changed from: private */
    public HorizontalSlider n;
    /* access modifiers changed from: private */
    public ImageView o;
    /* access modifiers changed from: private */
    public int p;
    /* access modifiers changed from: private */
    public int q;
    /* access modifiers changed from: private */
    public int r;
    /* access modifiers changed from: private */
    public int s;
    /* access modifiers changed from: private */
    public int t;
    /* access modifiers changed from: private */
    public int u;
    /* access modifiers changed from: private */
    public LocalPicView w;
    private float x;
    private float y;
    private float z;

    private static Bitmap a(Bitmap[] bitmapArr, Bitmap bitmap) {
        int length = bitmapArr.length;
        int i = 0;
        while (true) {
            if (i < length) {
                if (bitmap == bitmapArr[i]) {
                    break;
                }
                i++;
            } else {
                i = 0;
                break;
            }
        }
        int i2 = i + -1 < 0 ? length - 1 : i - 1;
        while (bitmapArr[i2] == null) {
            i2 = i2 + -1 < 0 ? length - 1 : i2 - 1;
        }
        return bitmapArr[i2];
    }

    private static Bitmap b(Bitmap[] bitmapArr, Bitmap bitmap) {
        int length = bitmapArr.length;
        int i = 0;
        while (true) {
            if (i < length) {
                if (bitmap == bitmapArr[i]) {
                    break;
                }
                i++;
            } else {
                i = 0;
                break;
            }
        }
        while (true) {
            i = (i + 1) % length;
            if (bitmapArr[i] != null) {
                return bitmapArr[i];
            }
        }
    }

    /* access modifiers changed from: protected */
    public Bitmap a() {
        Bitmap createBitmap = Bitmap.createBitmap(this.o.getWidth(), this.o.getHeight(), Bitmap.Config.RGB_565);
        this.o.draw(new Canvas(createBitmap));
        return createBitmap;
    }

    /* access modifiers changed from: protected */
    public Bitmap a(int i, int i2) {
        int width = this.o.getWidth();
        try {
            Bitmap createBitmap = Bitmap.createBitmap(width, this.o.getHeight(), Bitmap.Config.RGB_565);
            this.o.draw(new Canvas(createBitmap));
            if (i2 != this.s) {
                i = (int) (((float) i) * (((float) this.s) / ((float) i2)));
                i2 = this.s;
            }
            Bitmap[] bitmapArr = new Bitmap[3];
            if (width > this.r) {
                int i3 = -this.B;
                bitmapArr[1] = Bitmap.createBitmap(createBitmap, i3, 0, this.r, this.s);
                if (i3 != 0) {
                    bitmapArr[0] = Bitmap.createBitmap(createBitmap, 0, 0, i3, this.s);
                }
                if (this.r + i3 < width) {
                    bitmapArr[2] = Bitmap.createBitmap(createBitmap, this.r + i3, 0, (width - i3) - this.r, this.s);
                }
            } else {
                bitmapArr[1] = createBitmap;
            }
            Bitmap createBitmap2 = Bitmap.createBitmap(i, i2, Bitmap.Config.RGB_565);
            Canvas canvas = new Canvas(createBitmap2);
            int width2 = bitmapArr[1].getWidth();
            if (i > width2) {
                canvas.drawBitmap(bitmapArr[1], (float) ((i - width2) / 2), 0.0f, (Paint) null);
                int i4 = (i - width2) / 2;
                Bitmap bitmap = bitmapArr[1];
                int i5 = i4;
                while (i5 > 0) {
                    bitmap = a(bitmapArr, bitmap);
                    if (i5 >= bitmap.getWidth()) {
                        canvas.drawBitmap(bitmap, (float) (i5 - bitmap.getWidth()), 0.0f, (Paint) null);
                        i5 -= bitmap.getWidth();
                    } else {
                        canvas.drawBitmap(bitmap, new Rect(bitmap.getWidth() - i5, 0, bitmap.getWidth(), bitmap.getHeight()), new Rect(0, 0, i5, bitmap.getHeight()), (Paint) null);
                        i5 = 0;
                    }
                }
                int i6 = i4 + width2;
                Bitmap bitmap2 = bitmapArr[1];
                while (i6 < i) {
                    bitmap2 = b(bitmapArr, bitmap2);
                    if (bitmap2.getWidth() + i6 <= i) {
                        canvas.drawBitmap(bitmap2, (float) i6, 0.0f, (Paint) null);
                        i6 += bitmap2.getWidth();
                    } else {
                        canvas.drawBitmap(bitmap2, (float) i6, 0.0f, (Paint) null);
                        i6 = i;
                    }
                }
                if (!(bitmapArr[1] == createBitmap || bitmapArr[1] == null)) {
                    bitmapArr[1].recycle();
                    bitmapArr[1] = null;
                }
                if (bitmapArr[0] != null) {
                    bitmapArr[0].recycle();
                    bitmapArr[0] = null;
                }
                if (bitmapArr[2] != null) {
                    bitmapArr[2].recycle();
                    bitmapArr[2] = null;
                }
                if (createBitmap == null) {
                    return createBitmap2;
                }
                createBitmap.recycle();
                return createBitmap2;
            }
            if (bitmapArr[0] != null) {
                bitmapArr[0].recycle();
                bitmapArr[0] = null;
            }
            if (bitmapArr[2] != null) {
                bitmapArr[2].recycle();
                bitmapArr[2] = null;
            }
            if (createBitmap != bitmapArr[1]) {
                createBitmap.recycle();
            }
            return bitmapArr[1];
        } catch (Exception e) {
            return null;
        }
    }

    public void a(int i, int i2, int i3) {
        b.a("LocalPaperActivity", "x = " + i + ", barwidth = " + i2 + "sliderwidth = " + i3);
        this.B = (int) ((((float) (-(this.t - this.r))) * ((float) i)) / ((float) (i3 - i2)));
        this.w.a(this.B, this.t, this.s);
    }

    /* access modifiers changed from: protected */
    public void a(Bitmap bitmap) {
        if (!"ZTE".equalsIgnoreCase(Build.BRAND) && !"nubia".equalsIgnoreCase(Build.BRAND)) {
            bitmap.recycle();
        }
    }

    /* access modifiers changed from: protected */
    public synchronized void a(j jVar) {
        File a2;
        File file = new File(jVar.e);
        if (!file.isFile() || !file.exists()) {
            Uri parse = Uri.parse(jVar.c);
            if (parse.getScheme().equalsIgnoreCase("file")) {
                try {
                    a2 = new File(new URI(jVar.c));
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
            } else {
                a2 = parse.getScheme().equalsIgnoreCase("content") ? f.a(jVar.c, this) : com.d.a.b.a.b.a(jVar.c, d.a().d());
            }
            if (a2 != null) {
                if (a2.exists() && !file.exists() && m.a(a2, file)) {
                    new aj(this, file);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public Bitmap b() {
        Bitmap createBitmap = Bitmap.createBitmap(this.o.getWidth(), this.o.getHeight(), Bitmap.Config.RGB_565);
        this.o.draw(new Canvas(createBitmap));
        if ("samsung".equalsIgnoreCase(Build.BRAND)) {
            b.a(v, "show width = " + this.t + ", screen width = " + this.r + ", screen height = " + this.s);
            if (this.t <= this.r || this.t <= this.s) {
                return createBitmap;
            }
            int i = ((this.B * -1) + (this.r / 2)) - (this.s / 2);
            if (i < 0) {
                i = 0;
            }
            if (this.s + i > this.t) {
                i = this.t - this.s;
            }
            Bitmap createBitmap2 = Bitmap.createBitmap(createBitmap, i, 0, this.s, this.s);
            createBitmap.recycle();
            return createBitmap2;
        } else if (this.t <= this.r) {
            return createBitmap;
        } else {
            Bitmap createBitmap3 = Bitmap.createBitmap(createBitmap, this.B * -1, 0, this.r, this.s);
            createBitmap.recycle();
            return createBitmap3;
        }
    }

    /* access modifiers changed from: protected */
    public void c() {
    }

    /* access modifiers changed from: protected */
    public void d() {
        b.a(v, "mActionPanelHeight = " + this.c);
        LinearLayout linearLayout = (LinearLayout) findViewById(f.c("R.id.btn_set_wallpaper_layout"));
        int paddingBottom = linearLayout.getPaddingBottom();
        int paddingTop = linearLayout.getPaddingTop();
        int paddingRight = linearLayout.getPaddingRight();
        int paddingLeft = linearLayout.getPaddingLeft();
        linearLayout.setBackgroundResource(f.c("R.drawable.wallpaperdd_triangle_button_bkg"));
        linearLayout.setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom);
        this.d.showAtLocation(findViewById(f.c("R.id.local_wallpaper_action_panel")), 85, 0, this.c);
    }

    /* access modifiers changed from: protected */
    public void e() {
        this.d.dismiss();
        LinearLayout linearLayout = (LinearLayout) findViewById(f.c("R.id.btn_set_wallpaper_layout"));
        int paddingBottom = linearLayout.getPaddingBottom();
        int paddingTop = linearLayout.getPaddingTop();
        int paddingRight = linearLayout.getPaddingRight();
        int paddingLeft = linearLayout.getPaddingLeft();
        linearLayout.setBackgroundResource(f.c("R.drawable.wallpaperdd_action_button_bkg"));
        linearLayout.setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.widget.PopupWindow.<init>(android.view.View, int, int, boolean):void}
     arg types: [android.view.View, int, int, int]
     candidates:
      ClspMth{android.widget.PopupWindow.<init>(android.content.Context, android.util.AttributeSet, int, int):void}
      ClspMth{android.widget.PopupWindow.<init>(android.view.View, int, int, boolean):void} */
    /* access modifiers changed from: protected */
    public void f() {
        findViewById(f.c("R.id.localpic_title_panel")).setVisibility(4);
        findViewById(f.c("R.id.local_wallpaper_action_panel")).setVisibility(4);
        findViewById(f.c("R.id.localpic_slider")).setVisibility(4);
        if (this.f == null) {
            this.f = new PopupWindow(LayoutInflater.from(this).inflate(f.c("R.layout.wallpaperdd_popup_preview_bottom"), (ViewGroup) null), -2, -2, false);
            this.f.setAnimationStyle(f.c("R.style.wallpaperdd_menuPopupStyle"));
        }
        if (this.g == null) {
            View inflate = LayoutInflater.from(this).inflate(f.c("R.layout.wallpaperdd_popup_preview_top"), (ViewGroup) null);
            this.i = (TextView) inflate.findViewById(f.c("R.id.textview_date"));
            this.j = (TextView) inflate.findViewById(f.c("R.id.textview_time"));
            this.g = new PopupWindow(inflate, -2, -2, false);
            this.g.setAnimationStyle(f.c("R.style.wallpaperdd_previewtop_anim_style"));
        }
        if (this.h == null) {
            this.h = new PopupWindow(LayoutInflater.from(this).inflate(f.c("R.layout.wallpaperdd_popup_preview_duoduo_family"), (ViewGroup) null), -2, -2, false);
            this.h.setAnimationStyle(f.c("R.style.wallpaperdd_previewicon_anim_style"));
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("M月d日 E");
        SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("HH:mm");
        Date date = new Date(System.currentTimeMillis());
        String format = simpleDateFormat.format(date);
        String format2 = simpleDateFormat2.format(date);
        this.i.setText(format);
        this.j.setText(format2);
        this.g.showAtLocation(findViewById(f.c("R.id.local_activity_layout")), 49, 0, getResources().getDisplayMetrics().heightPixels / 10);
        this.f.showAtLocation(findViewById(f.c("R.id.local_activity_layout")), 81, 0, 0);
        this.h.showAtLocation(findViewById(f.c("R.id.local_activity_layout")), 17, 0, 0);
        this.k = true;
    }

    /* access modifiers changed from: protected */
    public void g() {
    }

    /* access modifiers changed from: protected */
    public void h() {
        if (this.g.isShowing()) {
            this.g.dismiss();
        }
        if (this.f.isShowing()) {
            this.f.dismiss();
        }
        if (this.h.isShowing()) {
            this.h.dismiss();
        }
        findViewById(f.c("R.id.localpic_title_panel")).setVisibility(0);
        findViewById(f.c("R.id.local_wallpaper_action_panel")).setVisibility(0);
        findViewById(f.c("R.id.localpic_slider")).setVisibility(0);
        this.k = false;
    }

    /* access modifiers changed from: protected */
    public com.shoujiduoduo.wallpaper.kernel.d i() {
        return new com.shoujiduoduo.wallpaper.kernel.d(this.o.getWidth(), this.o.getHeight());
    }

    public void m() {
    }

    public void n() {
        b.a(v, "onSliderPushUp, mScrollPos = " + this.B);
    }

    public void onCreate(Bundle bundle) {
        int lastIndexOf;
        super.onCreate(bundle);
        setContentView(f.c("R.layout.wallpaperdd_localpaper_activity"));
        findViewById(f.c("R.id.btn_pic_album_layout")).setVisibility(8);
        Intent intent = getIntent();
        String stringExtra = intent.getStringExtra("uri");
        String stringExtra2 = intent.getStringExtra(SelectCountryActivity.EXTRA_COUNTRY_NAME);
        String stringExtra3 = intent.getStringExtra("uploader");
        this.r = getResources().getDisplayMetrics().widthPixels;
        this.s = getResources().getDisplayMetrics().heightPixels;
        ((ImageButton) findViewById(f.c("R.id.btn_localpic_back_to_main"))).setOnClickListener(new q(this));
        this.b = new j();
        this.b.c = stringExtra;
        if (stringExtra2 != null) {
            this.b.h = stringExtra2;
        }
        if (stringExtra3 != null) {
            this.b.j = stringExtra3;
        }
        if (Uri.parse(this.b.c).getScheme().equalsIgnoreCase("content")) {
            File a2 = f.a(this.b.c, this);
            if (a2 == null) {
                Toast.makeText(this, "没有找到您要打开的文件，请确认您打开的图片是否还在。", 0).show();
                finish();
                return;
            }
            this.b.k = h.a(a2.getAbsolutePath());
        } else {
            this.b.k = h.a(this.b.c);
        }
        j jVar = this.b;
        String str = String.valueOf(f.a()) + "favorate/";
        String str2 = "";
        if (Uri.parse(this.b.c).getScheme().equalsIgnoreCase("content")) {
            File a3 = f.a(this.b.c, this);
            String name = a3.getName();
            if (a3 != null && a3.exists() && (lastIndexOf = name.lastIndexOf(46)) >= 0) {
                str2 = name.substring(lastIndexOf);
            }
        } else {
            int lastIndexOf2 = this.b.c.lastIndexOf(46);
            if (lastIndexOf2 >= 0) {
                str2 = this.b.c.substring(lastIndexOf2);
            }
        }
        jVar.e = String.valueOf(str) + this.b.k + str2;
        ImageButton imageButton = (ImageButton) findViewById(f.c("R.id.btn_localpic_share"));
        imageButton.setOnClickListener(new r(this, imageButton));
        a(f.c("R.id.local_wallpaper_action_panel"));
        this.n = (HorizontalSlider) findViewById(f.c("R.id.localpic_slider"));
        this.n.setListener(this);
        this.w = (LocalPicView) findViewById(f.c("R.id.local_pic_container"));
        this.o = this.w.getImageView();
        d.a().a(stringExtra, this.o, new c.a().a(false).b(this.b.j.contains("百度搜索")).c(true).a(Bitmap.Config.RGB_565).c(), new s(this), new t(this));
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 4) {
            finish();
            return true;
        } else if (i != 82) {
            return super.onKeyDown(i, keyEvent);
        } else {
            new com.shoujiduoduo.wallpaper.utils.j(this, true).showAtLocation(findViewById(f.c("R.id.local_activity_layout")), 81, 0, 0);
            return true;
        }
    }

    public void onPause() {
        super.onPause();
        com.umeng.analytics.b.b("LocalPaperActivity");
        com.umeng.analytics.b.a(this);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        b.a(v, "onResume, mScrollPos = " + this.B);
        com.umeng.analytics.b.a("LocalPaperActivity");
        com.umeng.analytics.b.b(this);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            this.x = motionEvent.getX();
            this.y = motionEvent.getY();
            return true;
        } else if (motionEvent.getAction() == 2 || motionEvent.getAction() != 1) {
            return super.onTouchEvent(motionEvent);
        } else {
            this.z = motionEvent.getX();
            this.A = motionEvent.getY();
            if (Math.abs(this.z - this.x) >= 35.0f || Math.abs(this.A - this.y) >= 35.0f) {
                return true;
            }
            if (this.k) {
                h();
                return true;
            }
            this.l = !this.l;
            b.a(v, "showPicOnly");
            if (this.l) {
                View findViewById = findViewById(f.c("R.id.localpic_title_panel"));
                findViewById.setAnimation(AnimationUtils.loadAnimation(this, f.c("R.anim.wallpaperdd_preview_icon_slide_out")));
                findViewById.setVisibility(4);
                View findViewById2 = findViewById(f.c("R.id.local_wallpaper_action_panel"));
                findViewById2.setAnimation(AnimationUtils.loadAnimation(this, f.c("R.anim.wallpaperdd_preview_icon_slide_out")));
                findViewById2.setVisibility(4);
                View findViewById3 = findViewById(f.c("R.id.localpic_slider"));
                findViewById3.setAnimation(AnimationUtils.loadAnimation(this, f.c("R.anim.wallpaperdd_preview_icon_slide_out")));
                findViewById3.setVisibility(4);
                return true;
            }
            View findViewById4 = findViewById(f.c("R.id.localpic_title_panel"));
            findViewById4.setAnimation(AnimationUtils.loadAnimation(this, f.c("R.anim.wallpaperdd_preview_icon_slide_in")));
            findViewById4.setVisibility(0);
            View findViewById5 = findViewById(f.c("R.id.local_wallpaper_action_panel"));
            findViewById5.setAnimation(AnimationUtils.loadAnimation(this, f.c("R.anim.wallpaperdd_preview_icon_slide_in")));
            findViewById5.setVisibility(0);
            if (this.f1739a == a.LOAD_FINISHED) {
                View findViewById6 = findViewById(f.c("R.id.localpic_slider"));
                findViewById6.setAnimation(AnimationUtils.loadAnimation(this, f.c("R.anim.wallpaperdd_preview_icon_slide_in")));
                findViewById6.setVisibility(0);
            }
            this.l = false;
            return true;
        }
    }
}
