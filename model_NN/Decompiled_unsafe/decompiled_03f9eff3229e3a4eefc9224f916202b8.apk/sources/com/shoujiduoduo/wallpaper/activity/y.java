package com.shoujiduoduo.wallpaper.activity;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.d.a.b.d;
import com.shoujiduoduo.wallpaper.a.h;
import com.shoujiduoduo.wallpaper.a.j;
import com.shoujiduoduo.wallpaper.kernel.App;
import com.shoujiduoduo.wallpaper.kernel.b;
import com.shoujiduoduo.wallpaper.utils.RoundedImageView;
import com.shoujiduoduo.wallpaper.utils.f;
import java.io.File;

public class y extends BaseAdapter {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public /* synthetic */ UserListFragment f1815a;

    public y(UserListFragment userListFragment) {
        this.f1815a = userListFragment;
    }

    public int getCount() {
        int a2 = h.d().a();
        return (a2 % 2) + (a2 / 2) + 1;
    }

    public Object getItem(int i) {
        return Integer.valueOf(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int i, View view, ViewGroup viewGroup) {
        RelativeLayout relativeLayout;
        View view2;
        View view3;
        if (i != 0) {
            if (view == null || !"userwallpaper".equalsIgnoreCase((String) view.getTag())) {
                relativeLayout = new RelativeLayout(this.f1815a.getActivity());
                View inflate = LayoutInflater.from(this.f1815a.getActivity()).inflate(f.c("R.layout.wallpaperdd_user_img_thumb"), (ViewGroup) null, false);
                View inflate2 = LayoutInflater.from(this.f1815a.getActivity()).inflate(f.c("R.layout.wallpaperdd_user_img_thumb"), (ViewGroup) null, false);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
                layoutParams.addRule(9);
                relativeLayout.addView(inflate, 0, layoutParams);
                RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
                layoutParams2.addRule(11);
                relativeLayout.addView(inflate2, 1, layoutParams2);
                relativeLayout.setTag("userwallpaper");
                view3 = inflate2;
                view2 = inflate;
            } else {
                relativeLayout = (RelativeLayout) view;
                View childAt = relativeLayout.getChildAt(0);
                view3 = relativeLayout.getChildAt(1);
                view2 = childAt;
            }
            j a2 = h.d().a((i - 1) << 1);
            j a3 = h.d().a(((i - 1) << 1) + 1);
            String valueOf = a2 == null ? "null" : String.valueOf(a2.k);
            String valueOf2 = a3 == null ? "null" : String.valueOf(a3.k);
            if (!(view2.getTag() == null || view3.getTag() == null)) {
                String str = (String) view3.getTag();
                if (valueOf.equalsIgnoreCase((String) view2.getTag()) && valueOf2.equalsIgnoreCase(str)) {
                    return relativeLayout;
                }
            }
            view2.setTag(valueOf);
            view3.setTag(valueOf2);
            RoundedImageView roundedImageView = (RoundedImageView) view2.findViewById(f.c("R.id.user_image_thumb"));
            ImageButton imageButton = (ImageButton) view2.findViewById(f.c("R.id.user_image_delete_button"));
            ((RelativeLayout) view2.findViewById(f.c("R.id.user_image_container"))).setLayoutParams(new RelativeLayout.LayoutParams(App.f1817a, App.b));
            roundedImageView.setImageDrawable(App.e);
            roundedImageView.setCornerRadius(0);
            roundedImageView.setBorderWidth(0);
            roundedImageView.setBorderColor(-12303292);
            roundedImageView.setRoundBackground(false);
            roundedImageView.setBackgroundResource(f.c("R.drawable.wallpaperdd_background"));
            RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(App.f1817a, App.d);
            layoutParams3.addRule(3, f.c("R.id.user_image_container"));
            ((RelativeLayout) view2.findViewById(f.c("R.id.user_image_text_info_container"))).setLayoutParams(layoutParams3);
            ((TextView) view2.findViewById(f.c("R.id.user_image_title"))).setText(a2.h);
            roundedImageView.setTag(Integer.valueOf((i - 1) * 2));
            roundedImageView.setOnClickListener(this.f1815a.h);
            imageButton.setTag(a2);
            imageButton.setOnClickListener(this.f1815a.g);
            b.a(UserListFragment.f1744a, "position = " + i);
            b.a(UserListFragment.f1744a, "url = " + a2.c);
            b.a(UserListFragment.f1744a, "localPath = " + a2.e);
            int i2 = a2.k;
            h.d().c();
            ((ImageView) view2.findViewById(f.c("R.id.user_wallpaper_tick"))).setVisibility(4);
            d.a().a((a2.e == null || a2.e.length() == 0) ? a2.c : new File(a2.e).exists() ? "file://" + a2.e : a2.c, roundedImageView, this.f1815a.c, new ay(this), new az(this));
            RoundedImageView roundedImageView2 = (RoundedImageView) view3.findViewById(f.c("R.id.user_image_thumb"));
            TextView textView = (TextView) view3.findViewById(f.c("R.id.user_image_title"));
            ImageButton imageButton2 = (ImageButton) view3.findViewById(f.c("R.id.user_image_delete_button"));
            ((RelativeLayout) view3.findViewById(f.c("R.id.user_image_container"))).setLayoutParams(new RelativeLayout.LayoutParams(App.f1817a, App.b));
            roundedImageView2.setImageDrawable(App.e);
            roundedImageView2.setVisibility(0);
            roundedImageView2.setCornerRadius(0);
            roundedImageView2.setBorderWidth(0);
            roundedImageView2.setBorderColor(-12303292);
            roundedImageView2.setRoundBackground(false);
            roundedImageView2.setBackgroundResource(f.c("R.drawable.wallpaperdd_background"));
            RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(App.f1817a, App.d);
            layoutParams4.addRule(3, f.c("R.id.user_image_container"));
            ((RelativeLayout) view3.findViewById(f.c("R.id.user_image_text_info_container"))).setLayoutParams(layoutParams4);
            ImageView imageView = (ImageView) view2.findViewById(f.c("R.id.user_wallpaper_tick"));
            if (a3 != null) {
                textView.setText(a3.h);
                roundedImageView2.setTag(Integer.valueOf(((i - 1) << 1) + 1));
                roundedImageView2.setOnClickListener(this.f1815a.h);
                imageButton2.setVisibility(0);
                imageButton2.setTag(a3);
                imageButton2.setOnClickListener(this.f1815a.g);
                b.a(UserListFragment.f1744a, "position = " + i);
                b.a(UserListFragment.f1744a, "url = " + a3.c);
                b.a(UserListFragment.f1744a, "localPath = " + a3.e);
                int i3 = a3.k;
                h.d().c();
                imageView.setVisibility(4);
                d.a().a((a3.e == null || a3.e.length() == 0) ? a3.c : new File(a3.e).exists() ? "file://" + a3.e : a3.c, roundedImageView2, this.f1815a.c, new ba(this), new bb(this));
                return relativeLayout;
            }
            textView.setText("");
            roundedImageView2.setImageDrawable(this.f1815a.getActivity().getResources().getDrawable(f.c("R.drawable.wallpaperdd_background")));
            roundedImageView2.setVisibility(4);
            imageButton2.setVisibility(8);
            return relativeLayout;
        } else if (view != null && "localwallpaper".equalsIgnoreCase((String) view.getTag())) {
            return view;
        } else {
            RelativeLayout relativeLayout2 = new RelativeLayout(this.f1815a.getActivity());
            View inflate3 = LayoutInflater.from(this.f1815a.getActivity()).inflate(f.c("R.layout.wallpaperdd_local_wallpaper_icon"), (ViewGroup) null, false);
            View inflate4 = LayoutInflater.from(this.f1815a.getActivity()).inflate(f.c("R.layout.wallpaperdd_local_wallpaper_icon"), (ViewGroup) null, false);
            relativeLayout2.setTag("localwallpaper");
            RelativeLayout.LayoutParams layoutParams5 = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams5.addRule(9);
            relativeLayout2.addView(inflate3, layoutParams5);
            ((RelativeLayout) inflate3.findViewById(f.c("R.id.user_wallpaper_icon_container"))).setLayoutParams(new RelativeLayout.LayoutParams(App.f1817a, App.b));
            ((ImageView) inflate3.findViewById(f.c("R.id.user_wallpaper_icon_image"))).setImageDrawable(this.f1815a.getActivity().getResources().getDrawable(f.c("R.drawable.wallpaperdd_local_wallpaper_button")));
            ((TextView) inflate3.findViewById(f.c("R.id.user_wallpaper_icon_text"))).setText("本地图片");
            RelativeLayout.LayoutParams layoutParams6 = new RelativeLayout.LayoutParams(App.f1817a, App.d);
            layoutParams6.addRule(3, f.c("R.id.user_wallpaper_icon_container"));
            ((LinearLayout) inflate3.findViewById(f.c("R.id.user_wallpaper_text_info_container"))).setLayoutParams(layoutParams6);
            ((TextView) inflate3.findViewById(f.c("R.id.user_wallpaper_title_text"))).setText("选择本地图片");
            inflate3.setOnClickListener(new aw(this));
            RelativeLayout.LayoutParams layoutParams7 = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams7.addRule(11);
            relativeLayout2.addView(inflate4, layoutParams7);
            ((RelativeLayout) inflate4.findViewById(f.c("R.id.user_wallpaper_icon_container"))).setLayoutParams(new RelativeLayout.LayoutParams(App.f1817a, App.b));
            ((ImageView) inflate4.findViewById(f.c("R.id.user_wallpaper_icon_image"))).setImageDrawable(this.f1815a.getActivity().getResources().getDrawable(f.c("R.drawable.wallpaperdd_user_img_icon_autochange")));
            ((TextView) inflate4.findViewById(f.c("R.id.user_wallpaper_icon_text"))).setText("自动换壁纸");
            RelativeLayout.LayoutParams layoutParams8 = new RelativeLayout.LayoutParams(App.f1817a, App.d);
            layoutParams8.addRule(3, f.c("R.id.user_wallpaper_icon_container"));
            ((LinearLayout) inflate4.findViewById(f.c("R.id.user_wallpaper_text_info_container"))).setLayoutParams(layoutParams8);
            ((TextView) inflate4.findViewById(f.c("R.id.user_wallpaper_title_text"))).setText("自动更换壁纸");
            inflate4.setOnClickListener(new ax(this));
            return relativeLayout2;
        }
    }
}
