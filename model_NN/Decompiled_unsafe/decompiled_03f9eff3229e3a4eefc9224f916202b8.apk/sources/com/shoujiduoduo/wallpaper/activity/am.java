package com.shoujiduoduo.wallpaper.activity;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import com.shoujiduoduo.wallpaper.kernel.b;
import com.shoujiduoduo.wallpaper.utils.f;
import java.io.IOException;

final class am implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SearchFragment f1758a;

    am(SearchFragment searchFragment) {
        this.f1758a = searchFragment;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.wallpaper.activity.SearchFragment.a(com.shoujiduoduo.wallpaper.activity.SearchFragment, boolean):void
     arg types: [com.shoujiduoduo.wallpaper.activity.SearchFragment, int]
     candidates:
      com.shoujiduoduo.wallpaper.activity.SearchFragment.a(java.util.ArrayList, int):void
      com.shoujiduoduo.wallpaper.a.f.a(java.util.ArrayList, int):void
      com.shoujiduoduo.wallpaper.activity.SearchFragment.a(com.shoujiduoduo.wallpaper.activity.SearchFragment, boolean):void */
    public final void onClick(View view) {
        b.a(SearchFragment.f1743a, "Search Button Clicked!");
        String editable = ((EditText) this.f1758a.g.findViewById(f.c("R.id.search_input"))).getText().toString();
        if (editable.length() == 0) {
            b.a(SearchFragment.f1743a, "the keyword is empty! Search Returns Immediately!");
        } else if (editable.equalsIgnoreCase("*#06#getinstallsrc")) {
            Toast.makeText(this.f1758a.getActivity(), f.i(), 1).show();
        } else if (editable.equalsIgnoreCase("*#06#logcat")) {
            try {
                Process exec = Runtime.getRuntime().exec(new String[]{"logcat", "-v", "time", "-d", "-f", "/sdcard/logcat.log"});
                exec.waitFor();
                Log.d("test", "exitcode = " + exec.exitValue());
                Toast.makeText(this.f1758a.getActivity(), "日志已取出，请拷贝/sdcard/logcat.log", 1).show();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e2) {
                e2.printStackTrace();
            }
        } else {
            String trim = editable.trim();
            if (trim.length() > 0) {
                Intent intent = new Intent(this.f1758a.getActivity(), CategoryListActivity.class);
                intent.putExtra("listid", 999999998);
                intent.putExtra("listname", trim);
                if (this.f1758a.f) {
                    intent.putExtra("search_type", "hot_keyword");
                    this.f1758a.f = false;
                } else {
                    intent.putExtra("search_type", "user_input");
                }
                this.f1758a.startActivity(intent);
            }
        }
    }
}
