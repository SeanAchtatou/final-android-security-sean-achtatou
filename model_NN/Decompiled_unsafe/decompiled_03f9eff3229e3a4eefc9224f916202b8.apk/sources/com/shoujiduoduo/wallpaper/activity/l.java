package com.shoujiduoduo.wallpaper.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.widget.Toast;
import com.shoujiduoduo.wallpaper.a.h;
import com.shoujiduoduo.wallpaper.a.j;
import com.shoujiduoduo.wallpaper.utils.f;

final class l extends AsyncTask {

    /* renamed from: a  reason: collision with root package name */
    public String f1805a;
    public int b = -1;
    public j c;
    public boolean d = true;
    private AlertDialog e;
    private /* synthetic */ FullScreenPicActivity f;

    l(FullScreenPicActivity fullScreenPicActivity) {
        this.f = fullScreenPicActivity;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object doInBackground(Object... objArr) {
        Bitmap[] bitmapArr = (Bitmap[]) objArr;
        this.f.a(this.c);
        Boolean bool = Boolean.FALSE;
        if (!(bitmapArr == null || bitmapArr[0] == null)) {
            Bitmap bitmap = bitmapArr[0];
            if (f.a(bitmap, this.d)) {
                bool = Boolean.TRUE;
            }
            this.f.a(bitmap);
        }
        return bool;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void onPostExecute(Object obj) {
        Boolean bool = (Boolean) obj;
        this.e.dismiss();
        if (bool == Boolean.TRUE) {
            h.d().b(this.b);
            Toast.makeText(this.f, "设置壁纸成功。", 0).show();
            return;
        }
        Toast.makeText(this.f, "很抱歉，设置壁纸失败。", 0).show();
    }

    /* access modifiers changed from: protected */
    public final void onPreExecute() {
        this.e = new ProgressDialog(this.f);
        this.e.setCancelable(false);
        this.e.setMessage(this.f1805a);
        this.e.show();
    }
}
