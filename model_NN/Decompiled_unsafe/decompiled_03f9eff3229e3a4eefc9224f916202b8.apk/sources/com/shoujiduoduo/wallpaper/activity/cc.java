package com.shoujiduoduo.wallpaper.activity;

import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import com.shoujiduoduo.wallpaper.kernel.b;

final class cc implements ViewTreeObserver.OnGlobalLayoutListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ FullScreenPicActivity f1792a;
    private final /* synthetic */ LinearLayout b;

    cc(FullScreenPicActivity fullScreenPicActivity, LinearLayout linearLayout) {
        this.f1792a = fullScreenPicActivity;
        this.b = linearLayout;
    }

    public final void onGlobalLayout() {
        this.f1792a.c = this.b.getHeight();
        b.a(FullScreenPicActivity.n, "mActionPanelHeight = " + this.f1792a.c);
    }
}
