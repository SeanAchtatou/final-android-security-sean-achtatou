package com.shoujiduoduo.wallpaper.activity;

import android.graphics.Bitmap;
import android.view.View;
import android.widget.AbsoluteLayout;
import android.widget.ImageView;
import android.widget.Toast;
import com.d.a.b.a.c;
import com.d.a.b.a.n;
import com.shoujiduoduo.wallpaper.kernel.a;
import com.shoujiduoduo.wallpaper.kernel.b;
import com.shoujiduoduo.wallpaper.utils.f;
import java.io.File;

final class s extends n {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ LocalPaperActivity f1811a;

    s(LocalPaperActivity localPaperActivity) {
        this.f1811a = localPaperActivity;
    }

    public final void onLoadingComplete(String str, View view, Bitmap bitmap) {
        b.a(LocalPaperActivity.v, "onLoadingComplete.");
        this.f1811a.f1739a = a.LOAD_FINISHED;
        new File(this.f1811a.b.e);
        this.f1811a.b.f = bitmap.getWidth();
        this.f1811a.b.g = bitmap.getHeight();
        this.f1811a.findViewById(f.c("R.id.wallpaper_bdimg_loading_progress")).setVisibility(4);
        this.f1811a.n.setVisibility(0);
        this.f1811a.p = bitmap.getWidth();
        this.f1811a.q = bitmap.getHeight();
        this.f1811a.t = (this.f1811a.p * this.f1811a.s) / this.f1811a.q;
        this.f1811a.u = this.f1811a.s;
        b.a(LocalPaperActivity.v, "show width = " + this.f1811a.t + ", show height = " + this.f1811a.s);
        this.f1811a.o.setLayoutParams(new AbsoluteLayout.LayoutParams(this.f1811a.t, this.f1811a.u, (-(this.f1811a.t - this.f1811a.r)) / 2, 0));
        this.f1811a.o.setScaleType(ImageView.ScaleType.CENTER_CROP);
        this.f1811a.B = (-(this.f1811a.t - this.f1811a.r)) / 2;
        this.f1811a.w.a(this.f1811a.B, this.f1811a.t, this.f1811a.s);
    }

    public final void onLoadingFailed(String str, View view, c cVar) {
        this.f1811a.f1739a = a.LOAD_FAILED;
        this.f1811a.findViewById(f.c("R.id.wallpaper_bdimg_loading_progress")).setVisibility(4);
        Toast.makeText(this.f1811a, this.f1811a.getResources().getString(f.c("R.string.wallpaperdd_toast_fail_load_local_pic")), 0).show();
    }

    public final void onLoadingStarted(String str, View view) {
        this.f1811a.f1739a = a.LOADING;
    }
}
