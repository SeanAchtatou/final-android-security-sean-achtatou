package com.shoujiduoduo.wallpaper.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import com.shoujiduoduo.wallpaper.kernel.b;
import com.shoujiduoduo.wallpaper.utils.f;

final class x extends FragmentPagerAdapter {

    /* renamed from: a  reason: collision with root package name */
    private String[] f1814a;
    private /* synthetic */ MainActivity b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public x(MainActivity mainActivity, FragmentManager fragmentManager) {
        super(fragmentManager);
        this.b = mainActivity;
        this.f1814a = new String[]{mainActivity.getString(f.c("R.string.wallpaperdd_tab1_title")), mainActivity.getString(f.c("R.string.wallpaperdd_tab2_title")), mainActivity.getString(f.c("R.string.wallpaperdd_tab3_title")), mainActivity.getString(f.c("R.string.wallpaperdd_tab4_title")), mainActivity.getString(f.c("R.string.wallpaperdd_tab5_title"))};
    }

    public final int getCount() {
        return this.f1814a.length;
    }

    public final Fragment getItem(int i) {
        b.a("TabAdapter", "getItem " + i);
        Bundle bundle = new Bundle();
        if (i == 0) {
            bundle.putInt("list_id", 1);
        } else if (i == 1) {
            bundle.putInt("list_id", 0);
        }
        if (i == 0 || i == 1) {
            return Fragment.instantiate(this.b, WallpaperListFragment.class.getName(), bundle);
        }
        if (i == 2) {
            return Fragment.instantiate(this.b, CategoryFragment.class.getName());
        }
        if (i == 4) {
            return Fragment.instantiate(this.b, UserListFragment.class.getName());
        }
        if (i != 3) {
            return null;
        }
        b.a(MainActivity.d, "SearchFragment instantiate.");
        return Fragment.instantiate(this.b, SearchFragment.class.getName());
    }

    public final CharSequence getPageTitle(int i) {
        return this.f1814a[i];
    }
}
