package com.shoujiduoduo.wallpaper.activity;

import android.view.View;
import com.shoujiduoduo.wallpaper.a.l;
import com.shoujiduoduo.wallpaper.utils.f;

final class bz implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ CategoryListActivity f1788a;

    bz(CategoryListActivity categoryListActivity) {
        this.f1788a = categoryListActivity;
    }

    public final void onClick(View view) {
        if (this.f1788a.f == this.f1788a.g) {
            this.f1788a.k.setTextColor(this.f1788a.getResources().getColor(f.a(this.f1788a.getPackageName(), "drawable", "wallpaperdd_green_text_color")));
            this.f1788a.k.setBackgroundResource(f.a(this.f1788a.getPackageName(), "drawable", "wallpaperdd_category_sort_button_bkg_selected"));
            this.f1788a.l.setTextColor(this.f1788a.getResources().getColor(f.a(this.f1788a.getPackageName(), "drawable", "wallpaperdd_grey_text_color")));
            this.f1788a.l.setBackgroundResource(f.a(this.f1788a.getPackageName(), "drawable", "wallpaperdd_category_sort_button_bkg"));
            this.f1788a.a(l.SORT_BY_NEW);
        }
    }
}
