package com.shoujiduoduo.wallpaper.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.shoujiduoduo.wallpaper.a.g;
import com.shoujiduoduo.wallpaper.a.k;
import com.shoujiduoduo.wallpaper.a.l;
import com.shoujiduoduo.wallpaper.a.m;
import com.shoujiduoduo.wallpaper.kernel.App;
import com.shoujiduoduo.wallpaper.kernel.b;
import com.shoujiduoduo.wallpaper.utils.f;
import com.shoujiduoduo.wallpaper.utils.j;
import com.shoujiduoduo.wallpaper.utils.t;

public class CategoryListActivity extends Activity implements g {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final String f1738a = CategoryListActivity.class.getSimpleName();
    private int b;
    private String c;
    /* access modifiers changed from: private */
    public ListView d;
    /* access modifiers changed from: private */
    public t e;
    /* access modifiers changed from: private */
    public k f;
    /* access modifiers changed from: private */
    public k g;
    private k h;
    /* access modifiers changed from: private */
    public ProgressBar i;
    /* access modifiers changed from: private */
    public ViewGroup j;
    /* access modifiers changed from: private */
    public Button k;
    /* access modifiers changed from: private */
    public Button l;
    private TextView m;
    private Handler n = new bv(this);

    private void a(View view) {
        if (view.getBackground() != null) {
            view.getBackground().setCallback(null);
        }
        if ((view instanceof ViewGroup) && !(view instanceof AdapterView)) {
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 >= ((ViewGroup) view).getChildCount()) {
                    ((ViewGroup) view).removeAllViews();
                    return;
                } else {
                    a(((ViewGroup) view).getChildAt(i3));
                    i2 = i3 + 1;
                }
            }
        }
    }

    public void a(k kVar, int i2) {
        Message obtainMessage = this.n.obtainMessage(5001, i2, 0);
        b.a(f1738a, "CategoryListActivity:onListUpdate");
        this.n.sendMessage(obtainMessage);
    }

    public void a(l lVar) {
        if (lVar == l.SORT_BY_HOT && this.f != this.g) {
            this.f = this.g;
        } else if (lVar == l.SORT_BY_NEW && this.f != this.h) {
            this.f = this.h;
        } else {
            return;
        }
        this.e.a(this.f);
        if (this.f.a() == 0) {
            this.i.setVisibility(0);
            this.j.setVisibility(4);
            this.d.setVisibility(4);
            this.f.d();
            return;
        }
        this.i.setVisibility(4);
        this.j.setVisibility(4);
        this.d.setVisibility(0);
    }

    public void onCreate(Bundle bundle) {
        b.a(f1738a, "onCreate");
        super.onCreate(bundle);
        Intent intent = getIntent();
        if (intent != null) {
            this.b = intent.getIntExtra("listid", 0);
            this.c = intent.getStringExtra("listname");
        }
        setContentView(f.a(getPackageName(), "layout", "wallpaperdd_category_list"));
        this.i = (ProgressBar) findViewById(f.a(getPackageName(), "id", "list_loading_progress"));
        this.j = (ViewGroup) findViewById(f.a(getPackageName(), "id", "list_load_failed"));
        ((ImageButton) findViewById(f.a(getPackageName(), "id", "back_to_category_main"))).setOnClickListener(new bw(this));
        this.d = (ListView) findViewById(f.a(getPackageName(), "id", "category_listview"));
        this.d.setDividerHeight(App.c);
        this.l = (Button) findViewById(f.a(getPackageName(), "id", "btn_sort_by_hot"));
        this.k = (Button) findViewById(f.a(getPackageName(), "id", "btn_sort_by_new"));
        if (this.b >= 800000000 && this.b <= 899999999) {
            this.l.setVisibility(8);
            this.k.setVisibility(8);
        }
        if (this.b != 999999998) {
            this.g = m.b().a(this.b, l.SORT_BY_HOT);
            this.h = m.b().a(this.b, l.SORT_BY_NEW);
            this.g.a(this);
            this.h.a(this);
            this.f = this.g;
        } else {
            this.f = new k(this.b, this.c);
            this.f.a(intent.getStringExtra("search_type"));
            m.b().a(this.f);
            this.f.a(this);
            this.l.setVisibility(4);
            this.k.setVisibility(4);
        }
        this.e = new t(this, this.f, true);
        if (this.b == 999999998) {
            this.e.a(this.c);
        }
        this.d.setAdapter((ListAdapter) this.e);
        this.j.setOnClickListener(new bx(this));
        this.m = (TextView) findViewById(f.a(getPackageName(), "id", "category_list_title_text"));
        this.m.setText(this.c);
        this.l.setOnClickListener(new by(this));
        this.k.setOnClickListener(new bz(this));
        if (this.f.a() == 0) {
            this.i.setVisibility(0);
            this.j.setVisibility(4);
            this.d.setVisibility(4);
            this.f.d();
            return;
        }
        this.i.setVisibility(4);
        this.j.setVisibility(4);
        this.d.setVisibility(0);
    }

    public void onDestroy() {
        b.a(f1738a, "onDestroy");
        super.onDestroy();
        if (this.f != null) {
            this.f.a((g) null);
            this.f = null;
        }
        if (this.g != null) {
            this.g.a((g) null);
            this.g = null;
        }
        if (this.h != null) {
            this.h.a((g) null);
            this.h = null;
        }
        if (this.e != null) {
            this.e.a();
            this.e = null;
        }
        this.n.removeMessages(5001);
        a(findViewById(f.a(getPackageName(), "id", "category_list_activity_window")));
        System.gc();
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 == 4) {
            b.a(f1738a, "finish()");
            finish();
            return true;
        } else if (i2 != 82) {
            return super.onKeyDown(i2, keyEvent);
        } else {
            new j(this, true).showAtLocation(findViewById(f.a(getPackageName(), "id", "category_list_activity_window")), 81, 0, 0);
            return true;
        }
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        b.a(f1738a, "onNewIntent");
    }

    public void onPause() {
        b.a(f1738a, "onPause");
        super.onPause();
        com.umeng.analytics.b.b("CategoryListActivity");
        com.umeng.analytics.b.a(this);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        b.a(f1738a, "onResume");
        if (this.f != null && this.f.b() == 999999998) {
            m.b().a(this.f);
        }
        super.onResume();
        com.umeng.analytics.b.a("CategoryListActivity");
        com.umeng.analytics.b.b(this);
    }
}
