package com.shoujiduoduo.wallpaper.activity;

import android.content.Intent;
import android.view.View;
import com.umeng.analytics.b;

final class aw implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ y f1766a;

    aw(y yVar) {
        this.f1766a = yVar;
    }

    public final void onClick(View view) {
        b.b(this.f1766a.f1815a.getActivity(), "EVENT_CLICK_LOCAL_PIC");
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction("android.intent.action.PICK");
        this.f1766a.f1815a.startActivityForResult(intent, 3033);
    }
}
