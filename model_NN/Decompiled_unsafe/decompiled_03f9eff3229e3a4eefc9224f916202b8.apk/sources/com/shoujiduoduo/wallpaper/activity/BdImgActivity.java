package com.shoujiduoduo.wallpaper.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.media.TransportMediator;
import android.view.KeyEvent;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.shoujiduoduo.wallpaper.kernel.b;
import com.shoujiduoduo.wallpaper.utils.f;

public class BdImgActivity extends Activity {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final String f1736a = BdImgActivity.class.getSimpleName();
    /* access modifiers changed from: private */
    public final Activity b = this;
    private ImageButton c;
    /* access modifiers changed from: private */
    public ImageButton d;
    /* access modifiers changed from: private */
    public ImageButton e;
    private ImageButton f;
    /* access modifiers changed from: private */
    public ProgressBar g;
    /* access modifiers changed from: private */
    public WebView h;
    /* access modifiers changed from: private */
    public TextView i;
    /* access modifiers changed from: private */
    public String j = "";
    /* access modifiers changed from: private */
    public String k = "";

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(7);
        setContentView(f.a(getPackageName(), "layout", "wallpaperdd_bdimg_activity"));
        getWindow().setFeatureInt(7, f.a(getPackageName(), "layout", "wallpaperdd_bdimg_activity_title"));
        this.c = (ImageButton) findViewById(f.a(getPackageName(), "id", "bdimg_btn_exit_web_activity"));
        this.c.setOnClickListener(new ae(this));
        this.d = (ImageButton) findViewById(f.a(getPackageName(), "id", "bdimg_web_back"));
        this.d.setEnabled(false);
        this.d.setOnClickListener(new bj(this));
        this.e = (ImageButton) findViewById(f.a(getPackageName(), "id", "bdimg_web_forward"));
        this.e.setEnabled(false);
        this.e.setOnClickListener(new bk(this));
        this.f = (ImageButton) findViewById(f.a(getPackageName(), "id", "bdimg_web_refresh"));
        this.f.setOnClickListener(new bl(this));
        this.g = (ProgressBar) findViewById(f.a(getPackageName(), "id", "bdimg_progressWebLoading"));
        this.i = (TextView) findViewById(f.a(getPackageName(), "id", "bdimg_textWebActivityTitle"));
        this.g.setVisibility(8);
        Intent intent = getIntent();
        String stringExtra = intent.getStringExtra("url");
        this.k = intent.getStringExtra("keyword");
        this.h = (WebView) findViewById(f.a(getPackageName(), "id", "bdimg_webview_window"));
        this.h.getSettings().setJavaScriptEnabled(true);
        this.h.getSettings().setSupportZoom(true);
        this.h.requestFocus(TransportMediator.KEYCODE_MEDIA_RECORD);
        this.h.setOnTouchListener(new bm(this));
        this.h.setDownloadListener(new bo(this, (byte) 0));
        this.h.setWebChromeClient(new f(this));
        this.h.setWebViewClient(new bn(this));
        this.h.loadUrl(stringExtra);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4) {
            return super.onKeyDown(i2, keyEvent);
        }
        WebView webView = (WebView) findViewById(f.a(getPackageName(), "id", "bdimg_webview_window"));
        if (webView.canGoBack()) {
            webView.goBack();
            return true;
        }
        b.a(f1736a, "finish()");
        finish();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        com.umeng.analytics.b.a(this);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        com.umeng.analytics.b.b(this);
    }
}
