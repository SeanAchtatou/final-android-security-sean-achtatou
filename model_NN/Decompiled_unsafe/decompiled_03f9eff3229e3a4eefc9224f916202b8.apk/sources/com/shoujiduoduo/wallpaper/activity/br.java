package com.shoujiduoduo.wallpaper.activity;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import com.shoujiduoduo.wallpaper.a.o;

final class br implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ CategoryFragment f1782a;

    br(CategoryFragment categoryFragment) {
        this.f1782a = categoryFragment;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        Intent intent = new Intent(this.f1782a.getActivity(), CategoryListActivity.class);
        intent.putExtra("listid", ((o) this.f1782a.j.get(i)).f1728a);
        intent.putExtra("listname", ((o) this.f1782a.j.get(i)).c);
        this.f1782a.startActivity(intent);
    }
}
