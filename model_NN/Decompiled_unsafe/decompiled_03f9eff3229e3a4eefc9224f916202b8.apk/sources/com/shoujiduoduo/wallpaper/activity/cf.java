package com.shoujiduoduo.wallpaper.activity;

import android.view.View;
import android.widget.Toast;
import com.shoujiduoduo.wallpaper.kernel.a;
import com.umeng.analytics.b;

final class cf implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ FullScreenPicActivity f1795a;

    cf(FullScreenPicActivity fullScreenPicActivity) {
        this.f1795a = fullScreenPicActivity;
    }

    public final void onClick(View view) {
        if (this.f1795a instanceof WallpaperActivity) {
            b.b(this.f1795a, "CLICK_PREVIEW");
        }
        if (this.f1795a.f1739a != a.LOAD_FINISHED) {
            Toast.makeText(this.f1795a, "图片还未加载完毕，请稍候...", 0).show();
        } else {
            this.f1795a.f();
        }
    }
}
