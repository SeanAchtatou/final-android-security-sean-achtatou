package com.shoujiduoduo.wallpaper.activity;

import android.view.View;
import android.widget.AdapterView;
import com.shoujiduoduo.wallpaper.utils.b.a;
import com.umeng.socialize.c.b;
import java.util.Map;

final class m implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ i f1806a;

    m(i iVar) {
        this.f1806a = iVar;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        String str = (String) ((Map) adapterView.getItemAtPosition(i)).get("TEXT");
        b bVar = null;
        if (str == "微信") {
            bVar = b.WEIXIN;
        } else if (str == "微信朋友圈") {
            bVar = b.WEIXIN_CIRCLE;
        } else if (str == "QQ好友") {
            bVar = b.QQ;
        } else if (str == "QQ空间") {
            bVar = b.QZONE;
        } else if (str == "新浪微博") {
            bVar = b.SINA;
        } else if (str == "手机短信") {
            bVar = b.SMS;
        } else if (str == "分享原图") {
            this.f1806a.j.k();
            return;
        }
        if (bVar != null) {
            a.a(this.f1806a.f1802a).a(this.f1806a.d, this.f1806a.e, this.f1806a.g, this.f1806a.f, bVar);
            if (a.a() != null) {
                a.a().b();
            }
        }
        this.f1806a.dismiss();
    }
}
