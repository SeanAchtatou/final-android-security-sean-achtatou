package com.shoujiduoduo.wallpaper.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Process;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.KeyEvent;
import android.widget.ImageButton;
import com.shoujiduoduo.wallpaper.a.h;
import com.shoujiduoduo.wallpaper.a.m;
import com.shoujiduoduo.wallpaper.utils.FixViewPager;
import com.shoujiduoduo.wallpaper.utils.PagerSlidingTabStrip;
import com.shoujiduoduo.wallpaper.utils.af;
import com.shoujiduoduo.wallpaper.utils.f;
import com.shoujiduoduo.wallpaper.utils.j;
import com.shoujiduoduo.wallpaper.utils.q;
import com.umeng.analytics.b;

public class MainActivity extends FragmentActivity implements ViewPager.OnPageChangeListener {
    /* access modifiers changed from: private */
    public static final String d = MainActivity.class.getSimpleName();

    /* renamed from: a  reason: collision with root package name */
    private x f1741a = null;
    private PagerSlidingTabStrip b = null;
    private FixViewPager c = null;
    /* access modifiers changed from: private */
    public ImageButton e = null;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f1741a = new x(this, getSupportFragmentManager());
        setContentView(f.c("R.layout.wallpaperdd_main_activity"));
        this.b = (PagerSlidingTabStrip) findViewById(f.c("R.id.main_tab"));
        this.b.setShouldExpand(true);
        this.b.setOnPageChangeListener(this);
        this.c = (FixViewPager) findViewById(f.c("R.id.main_viewpager"));
        this.c.setAdapter(this.f1741a);
        this.b.setViewPager(this.c);
        this.c.setCurrentItem(0);
        af.a().a(this);
        this.e = (ImageButton) findViewById(f.c("R.id.menu_button"));
        this.e.setOnClickListener(new u(this));
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        af.a().a((Context) null);
        h.d().e();
        m.b().c();
        q.f1876a = null;
        Process.killProcess(Process.myPid());
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 4) {
            new AlertDialog.Builder(this).setTitle(getResources().getString(f.c("R.string.wallpaperdd_alert_dialog_header"))).setMessage(getResources().getString(f.c("R.string.wallpaperdd_on_exit_prompt"))).setIcon(17301543).setPositiveButton(getResources().getString(f.c("R.string.wallpaperdd_text_ok_button")), new v(this)).setNegativeButton(getResources().getString(f.c("R.string.wallpaperdd_text_cancel_button")), new w(this)).show();
            return true;
        } else if (i != 82) {
            return super.onKeyDown(i, keyEvent);
        } else {
            new j(this, true).showAtLocation(findViewById(f.c("R.id.topWindow")), 81, 0, 0);
            return true;
        }
    }

    public void onPageScrollStateChanged(int i) {
    }

    public void onPageScrolled(int i, float f, int i2) {
    }

    public void onPageSelected(int i) {
    }

    public void onPause() {
        super.onPause();
        b.a(this);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        b.b(this);
    }
}
