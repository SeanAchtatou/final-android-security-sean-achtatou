package com.shoujiduoduo.wallpaper.activity;

import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import com.shoujiduoduo.wallpaper.utils.f;

final class an implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ SearchFragment f1759a;

    an(SearchFragment searchFragment) {
        this.f1759a = searchFragment;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.wallpaper.activity.SearchFragment.a(com.shoujiduoduo.wallpaper.activity.SearchFragment, boolean):void
     arg types: [com.shoujiduoduo.wallpaper.activity.SearchFragment, int]
     candidates:
      com.shoujiduoduo.wallpaper.activity.SearchFragment.a(java.util.ArrayList, int):void
      com.shoujiduoduo.wallpaper.a.f.a(java.util.ArrayList, int):void
      com.shoujiduoduo.wallpaper.activity.SearchFragment.a(com.shoujiduoduo.wallpaper.activity.SearchFragment, boolean):void */
    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        EditText editText;
        String a2 = this.f1759a.b.a(i);
        if (a2 != null && (editText = (EditText) this.f1759a.getActivity().findViewById(f.c("R.id.search_input"))) != null) {
            editText.setText(a2);
            if (this.f1759a.e != null) {
                this.f1759a.f = true;
                this.f1759a.e.performClick();
            }
        }
    }
}
