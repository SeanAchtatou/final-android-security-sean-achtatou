package com.shoujiduoduo.wallpaper.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import com.shoujiduoduo.wallpaper.a.g;
import com.shoujiduoduo.wallpaper.a.k;
import com.shoujiduoduo.wallpaper.a.m;
import com.shoujiduoduo.wallpaper.kernel.App;
import com.shoujiduoduo.wallpaper.utils.ag;
import com.shoujiduoduo.wallpaper.utils.c;
import com.shoujiduoduo.wallpaper.utils.f;
import com.shoujiduoduo.wallpaper.utils.t;
import com.umeng.analytics.b;

public class WallpaperListFragment extends Fragment implements AdapterView.OnItemClickListener, g {

    /* renamed from: a  reason: collision with root package name */
    private static final String f1745a = WallpaperListFragment.class.getSimpleName();
    private View b;
    private ListView c;
    /* access modifiers changed from: private */
    public k d;
    private BaseAdapter e;
    private int f;
    private ProgressBar g;
    private ViewGroup h;
    /* access modifiers changed from: private */
    public int i = 0;
    private String j;
    private Handler k = new bh(this);
    private int l = 0;

    private void a(View view) {
        if (view.getBackground() != null) {
            view.getBackground().setCallback(null);
        }
        if ((view instanceof ViewGroup) && !(view instanceof AdapterView)) {
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 >= ((ViewGroup) view).getChildCount()) {
                    ((ViewGroup) view).removeAllViews();
                    return;
                } else {
                    a(((ViewGroup) view).getChildAt(i3));
                    i2 = i3 + 1;
                }
            }
        }
    }

    static /* synthetic */ void a(WallpaperListFragment wallpaperListFragment) {
        b.b(wallpaperListFragment.getActivity(), "EVENT_SHOW_WALLPAPER_APP_DLG");
        AlertDialog create = new AlertDialog.Builder(wallpaperListFragment.getActivity()).create();
        create.show();
        create.getWindow().setContentView(f.a(wallpaperListFragment.getActivity().getPackageName(), "layout", "wallpaperdd_downapp_alert_dialog"));
        create.findViewById(f.a(wallpaperListFragment.getActivity().getPackageName(), "id", "btn_install_wallpaperapp")).setOnClickListener(new af(wallpaperListFragment, create));
        create.findViewById(f.a(wallpaperListFragment.getActivity().getPackageName(), "id", "btn_cancel_install_wallpaperapp")).setOnClickListener(new ag(wallpaperListFragment, create));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.wallpaper.utils.ag.a(android.content.Context, java.lang.String, long):long
     arg types: [android.support.v4.app.FragmentActivity, java.lang.String, int]
     candidates:
      com.shoujiduoduo.wallpaper.utils.ag.a(android.content.Context, java.lang.String, int):int
      com.shoujiduoduo.wallpaper.utils.ag.a(android.content.Context, java.lang.String, long):long */
    public void a(k kVar, int i2) {
        if (i2 == 31) {
            this.h.setVisibility(4);
            if (this.g != null) {
                this.g.setVisibility(0);
            }
        } else if (i2 == 1) {
            this.g.setVisibility(4);
            this.c.setVisibility(0);
            this.h.setVisibility(0);
        } else {
            if (this.g != null) {
                this.g.setVisibility(8);
            }
            this.h.setVisibility(4);
            this.c.setVisibility(0);
            com.shoujiduoduo.wallpaper.kernel.b.a(f1745a, "onListUpdate, list size = " + kVar.a());
            this.l++;
            if (this.l == 3 && !f.b("com.shoujiduoduo.wallpaper")) {
                long a2 = ag.a((Context) getActivity(), "SHOW_WALLPAPER_APP_DIALOG_TIME", 0L);
                long currentTimeMillis = System.currentTimeMillis();
                if (currentTimeMillis - a2 > 259200000) {
                    ag.b(getActivity(), "SHOW_WALLPAPER_APP_DIALOG_TIME", currentTimeMillis);
                    this.k.sendEmptyMessage(5038);
                }
            }
            if (this.e instanceof c) {
                ((c) this.e).a();
            }
            this.e.notifyDataSetChanged();
        }
    }

    public void onCreate(Bundle bundle) {
        com.shoujiduoduo.wallpaper.kernel.b.a(f1745a, "WallpaperListFragment onCreate");
        super.onCreate(bundle);
        Bundle arguments = getArguments();
        if (arguments != null) {
            this.f = arguments.getInt("list_id");
            com.shoujiduoduo.wallpaper.kernel.b.a(f1745a, "onCreate, title = " + ((String) null));
            if (this.f != 999999998) {
                this.d = m.b().a(this.f);
                return;
            }
            String string = arguments.getString("list_name");
            if (!(string == null || string.length() == 0)) {
                this.j = string;
            }
            if (this.j == null) {
                this.j = "";
            }
            this.d = new k(this.f, this.j);
            this.d.a(arguments.getString("from"));
            m.b().a(this.d);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        boolean z = false;
        com.shoujiduoduo.wallpaper.kernel.b.a(f1745a, "WallpaperListFragment onCreateView");
        this.d.a(this);
        this.b = layoutInflater.inflate(f.a(getActivity().getPackageName(), "layout", "wallpaperdd_wallpaper_list_view"), viewGroup, false);
        this.c = (ListView) this.b.findViewById(f.a(getActivity().getPackageName(), "id", "wallpaper_listview"));
        this.c.setDividerHeight(App.c);
        this.g = (ProgressBar) this.b.findViewById(f.a(getActivity().getPackageName(), "id", "list_loading_progress"));
        this.h = (ViewGroup) this.b.findViewById(f.a(getActivity().getPackageName(), "id", "load_failed_view"));
        this.h.setOnClickListener(new bi(this));
        if (this.f == 5 || this.f == 6) {
            if (NewMainActivity.b) {
                this.e = new c(getActivity(), this.d, !NewMainActivity.f1742a);
            } else {
                this.e = new c(getActivity(), this.d, true);
            }
        } else if (NewMainActivity.b) {
            FragmentActivity activity = getActivity();
            k kVar = this.d;
            if (!NewMainActivity.f1742a) {
                z = true;
            }
            this.e = new t(activity, kVar, z);
            if (this.f == 999999998) {
                ((t) this.e).a(this.j);
            }
        } else {
            this.e = new t(getActivity(), this.d, true);
            if (this.f == 999999998) {
                ((t) this.e).a(this.j);
            }
        }
        this.c.setAdapter((ListAdapter) this.e);
        return this.b;
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.d != null) {
            this.d = null;
        }
        com.shoujiduoduo.wallpaper.kernel.b.a(f1745a, "WallpaperListFragment:onDestroy");
    }

    public void onDestroyView() {
        super.onDestroyView();
        this.d.a((g) null);
        if (this.e != null && (this.e instanceof t)) {
            ((t) this.e).a();
            this.e = null;
        }
        if (this.e != null && (this.e instanceof c)) {
            ((c) this.e).b();
            this.e = null;
        }
        a(this.b);
        this.b = null;
        this.i = 0;
        System.gc();
        com.shoujiduoduo.wallpaper.kernel.b.a(f1745a, "WallpaperListFragment:onDestroyView");
    }

    public void onItemClick(AdapterView adapterView, View view, int i2, long j2) {
        if (i2 < this.d.a() && i2 >= 0) {
            Intent intent = new Intent(getActivity(), WallpaperActivity.class);
            intent.putExtra("listid", this.f);
            intent.putExtra("serialno", i2);
            startActivity(intent);
        }
    }

    public void onPause() {
        super.onPause();
        b.b("SelectedPage");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.wallpaper.utils.ag.a(android.content.Context, java.lang.String, int):int
     arg types: [android.support.v4.app.FragmentActivity, java.lang.String, int]
     candidates:
      com.shoujiduoduo.wallpaper.utils.ag.a(android.content.Context, java.lang.String, long):long
      com.shoujiduoduo.wallpaper.utils.ag.a(android.content.Context, java.lang.String, int):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.wallpaper.utils.ag.b(android.content.Context, java.lang.String, int):boolean
     arg types: [android.support.v4.app.FragmentActivity, java.lang.String, int]
     candidates:
      com.shoujiduoduo.wallpaper.utils.ag.b(android.content.Context, java.lang.String, long):boolean
      com.shoujiduoduo.wallpaper.utils.ag.b(android.content.Context, java.lang.String, int):boolean */
    public void onResume() {
        super.onResume();
        b.a("SelectedPage");
        if (this.f == 5) {
            this.i = ag.a((Context) getActivity(), "PUSH_ALBUM_ID", 0);
            if (this.i != 0) {
                ag.b((Context) getActivity(), "PUSH_ALBUM_ID", 0);
                this.d.c();
                new ah(this, this.i);
                return;
            }
            this.d.d();
            return;
        }
        this.d.d();
    }
}
