package com.shoujiduoduo.wallpaper.activity;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import com.shoujiduoduo.wallpaper.utils.f;
import java.util.List;

final class o extends SimpleAdapter {
    public o(i iVar, Context context, List list, int i, String[] strArr, int[] iArr) {
        super(context, list, i, strArr, iArr);
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        View view2 = super.getView(i, view, viewGroup);
        ((TextView) view2.findViewById(f.c("R.id.menu_item_text"))).setTextColor(-1);
        return view2;
    }
}
