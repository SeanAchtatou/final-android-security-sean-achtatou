package com.shoujiduoduo.a.b;

import android.support.v4.BuildConfig;
import com.shoujiduoduo.b.a.s;
import com.shoujiduoduo.b.b.d;
import com.shoujiduoduo.b.b.e;
import com.shoujiduoduo.b.c.m;
import com.shoujiduoduo.b.c.x;
import com.shoujiduoduo.b.d.f;
import com.shoujiduoduo.b.d.g;
import com.shoujiduoduo.b.e.a;
import com.shoujiduoduo.b.f.ap;
import com.shoujiduoduo.b.f.u;
import com.shoujiduoduo.ringtone.RingDDApp;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

/* compiled from: ModMgr */
public final class b {

    /* renamed from: a  reason: collision with root package name */
    private static LinkedList<a> f732a = new LinkedList<>();
    private static boolean b;
    private static u c = null;
    private static s d = null;
    private static e e = null;
    private static f f = null;
    private static m g = null;
    private static a h = null;

    public static void a() {
        b = true;
        if (Thread.currentThread().getId() != RingDDApp.d()) {
            com.shoujiduoduo.base.a.a.c("ModMgr", "releaseAll");
            HashMap hashMap = new HashMap();
            hashMap.put(BuildConfig.BUILD_TYPE, "true");
            hashMap.put("method", "releaseAll");
            com.umeng.analytics.b.a(RingDDApp.c(), "MODMGR_ERROR", hashMap);
        }
        com.shoujiduoduo.base.a.a.a("ModMgr", "release module num:" + f732a.size());
        Iterator<a> it = f732a.iterator();
        while (it.hasNext()) {
            try {
                it.next().b();
            } catch (Throwable th) {
            }
        }
        f732a.clear();
    }

    private static void a(a aVar) {
        aVar.a();
        f732a.add(aVar);
    }

    public static synchronized u b() {
        u uVar;
        synchronized (b.class) {
            if (Thread.currentThread().getId() != RingDDApp.d()) {
                com.shoujiduoduo.base.a.a.c("ModMgr", "getUserListMgr");
                HashMap hashMap = new HashMap();
                hashMap.put(BuildConfig.BUILD_TYPE, b ? "true" : "false");
                hashMap.put("method", "getUserListMgr");
                com.umeng.analytics.b.a(RingDDApp.c(), "MODMGR_ERROR", hashMap);
            }
            if (c == null) {
                c = new ap();
                a(c);
            }
            uVar = c;
        }
        return uVar;
    }

    public static synchronized s c() {
        s sVar;
        synchronized (b.class) {
            if (Thread.currentThread().getId() != RingDDApp.d()) {
                com.shoujiduoduo.base.a.a.c("ModMgr", "getAdMgr");
                HashMap hashMap = new HashMap();
                hashMap.put(BuildConfig.BUILD_TYPE, b ? "true" : "false");
                hashMap.put("method", "getAdMgr");
                com.umeng.analytics.b.a(RingDDApp.c(), "MODMGR_ERROR", hashMap);
            }
            if (d == null) {
                d = new com.shoujiduoduo.b.a.a();
                a(d);
            }
            sVar = d;
        }
        return sVar;
    }

    public static synchronized e d() {
        e eVar;
        synchronized (b.class) {
            if (Thread.currentThread().getId() != RingDDApp.d()) {
                com.shoujiduoduo.base.a.a.c("ModMgr", "getCategoryMgr");
                HashMap hashMap = new HashMap();
                hashMap.put(BuildConfig.BUILD_TYPE, b ? "true" : "false");
                hashMap.put("method", "getCategoryMgr");
                com.umeng.analytics.b.a(RingDDApp.c(), "MODMGR_ERROR", hashMap);
            }
            if (e == null) {
                e = new d();
                a(e);
            }
            eVar = e;
        }
        return eVar;
    }

    public static synchronized f e() {
        f fVar;
        synchronized (b.class) {
            if (Thread.currentThread().getId() != RingDDApp.d()) {
                com.shoujiduoduo.base.a.a.c("ModMgr", "getSearchMgr");
                HashMap hashMap = new HashMap();
                hashMap.put(BuildConfig.BUILD_TYPE, b ? "true" : "false");
                hashMap.put("method", "getSearchMgr");
                com.umeng.analytics.b.a(RingDDApp.c(), "MODMGR_ERROR", hashMap);
            }
            if (f == null) {
                f = new g();
                a(f);
            }
            fVar = f;
        }
        return fVar;
    }

    public static synchronized m f() {
        m mVar;
        synchronized (b.class) {
            if (Thread.currentThread().getId() != RingDDApp.d()) {
                com.shoujiduoduo.base.a.a.c("ModMgr", "getTopListMgr");
                HashMap hashMap = new HashMap();
                hashMap.put(BuildConfig.BUILD_TYPE, b ? "true" : "false");
                hashMap.put("method", "getTopListMgr");
                com.umeng.analytics.b.a(RingDDApp.c(), "MODMGR_ERROR", hashMap);
            }
            if (g == null) {
                g = new x();
                a(g);
            }
            mVar = g;
        }
        return mVar;
    }

    public static synchronized a g() {
        a aVar;
        synchronized (b.class) {
            if (Thread.currentThread().getId() != RingDDApp.d()) {
                com.shoujiduoduo.base.a.a.c("ModMgr", "getUserInfoMgr");
                HashMap hashMap = new HashMap();
                hashMap.put(BuildConfig.BUILD_TYPE, b ? "true" : "false");
                hashMap.put("method", "getUserInfoMgr");
                com.umeng.analytics.b.a(RingDDApp.c(), "MODMGR_ERROR", hashMap);
            }
            if (h == null) {
                h = new com.shoujiduoduo.b.e.b();
                a(h);
            }
            aVar = h;
        }
        return aVar;
    }
}
