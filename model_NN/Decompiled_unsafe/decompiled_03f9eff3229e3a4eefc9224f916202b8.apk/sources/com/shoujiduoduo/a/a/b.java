package com.shoujiduoduo.a.a;

/* compiled from: MessageID */
public enum b {
    OBSERVER_ID_RESERVE,
    OBSERVER_APP,
    OBSERVER_PLAY_STATUS,
    OBSERVER_RING_CHANGE,
    OBSERVER_RING_UPLOAD,
    OBSERVER_LIST_DATA,
    OBSERVER_LIST_AREA,
    OBSERVER_CAILING,
    OBSERVER_USER_RING,
    OBSERVER_USER_CENTER,
    OBSERVER_CATEGORY,
    OBSERVER_SEARCH_AD,
    OBSERVER_BANNER_AD,
    OBSERVER_SEARCH_HOT_WORD,
    OBSERVER_MAKE_RING,
    OBSERVER_TOP_LIST,
    OBSERVER_SCENE,
    OBSERVER_CHANGE_BATCH,
    OBSERVER_VIP,
    OBSERVER_FEED_AD,
    OBSERVER_CONFIG;

    /* access modifiers changed from: package-private */
    public abstract Class<? extends a> a();
}
