package com.shoujiduoduo.player;

import android.media.MediaPlayer;
import com.shoujiduoduo.base.a.a;

/* compiled from: SystemPlayer */
public class s extends c implements MediaPlayer.OnBufferingUpdateListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener, MediaPlayer.OnPreparedListener {
    private static final String h = s.class.getSimpleName();
    private MediaPlayer i = new MediaPlayer();
    private boolean j;

    public s() {
        this.i.setOnCompletionListener(this);
        this.i.setOnErrorListener(this);
    }

    public synchronized int a(String str) {
        int i2 = 0;
        synchronized (this) {
            a.d(h, "play " + str);
            d();
            this.j = false;
            try {
                this.i.setDataSource(str);
                this.i.prepare();
                a(2);
                if (this.b != null) {
                    this.b.a(this);
                }
                if (this.f833a) {
                    this.i.start();
                    a(4);
                    if (this.f != null) {
                        this.f.a(this, 0, 1);
                    }
                } else {
                    a(3);
                }
            } catch (Exception e) {
                a.a(e);
                i2 = 1;
            }
        }
        return i2;
    }

    public synchronized void b() {
        a.d(h, "pause");
        if (i()) {
            try {
                this.i.pause();
                a(3);
            } catch (IllegalStateException e) {
                a.a(e);
            }
        }
        return;
    }

    public synchronized void c() {
        a.d(h, "resume");
        if (j()) {
            try {
                this.i.start();
                a(4);
            } catch (IllegalStateException e) {
                a.a(e);
            }
        }
        return;
    }

    public void d() {
        this.i.reset();
        a(0);
        a(true);
    }

    public void l() {
        this.i.release();
        this.i = null;
        a(0);
    }

    public int f() {
        if (i() || j()) {
            return this.i.getDuration();
        }
        return 0;
    }

    public boolean i() {
        return this.i.isPlaying();
    }

    public void onCompletion(MediaPlayer mediaPlayer) {
        a(5);
        if (this.c != null) {
            this.c.a(this);
        }
    }

    public boolean onError(MediaPlayer mediaPlayer, int i2, int i3) {
        a.d(h, "onError");
        a.d(h, "what:" + i2 + " extra:" + i3);
        if (!(i2 == -38 || i2 == 100 || i2 == Integer.MIN_VALUE || ((i2 == 1 && (i3 == -1004 || i3 == Integer.MIN_VALUE)) || (i2 == -1 && i3 == 0)))) {
            a.d(h, "tell another");
            if (this.d != null) {
                this.d.a(this, i2, i3);
            }
        }
        return true;
    }

    public void onBufferingUpdate(MediaPlayer mediaPlayer, int i2) {
        a.d(h, "func:onBufferingUpdate  percent: " + i2 + "  isPlaying:" + i());
        if (this.g != null) {
            this.g.a(this, i2);
        }
    }

    public void onPrepared(MediaPlayer mediaPlayer) {
        a.d(h, "onPrepared");
        if (this.j) {
            a(2);
            if (this.b != null) {
                this.b.a(this);
            }
            try {
                if (this.f833a) {
                    this.i.start();
                    a(4);
                    if (this.f != null) {
                        this.f.a(this, 0, 1);
                    }
                } else {
                    a(3);
                }
                if (this.e != null) {
                    this.e.b(this);
                }
            } catch (Exception e) {
                a.b(h, "play failed!");
                a.a(e);
                if (this.d != null) {
                    this.d.a(this, 1, 0);
                }
            }
        }
    }

    public int e() {
        return 0;
    }

    public int h() {
        return 0;
    }
}
