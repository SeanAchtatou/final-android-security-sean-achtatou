package com.shoujiduoduo.player;

import android.os.Handler;
import android.os.Message;
import cn.banshenggua.aichang.room.SimpleLiveRoomActivity;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.base.bean.y;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.util.f;
import java.util.Timer;

/* compiled from: PlayerService */
class l extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PlayerService f840a;

    l(PlayerService playerService) {
        this.f840a = playerService;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.player.PlayerService.a(com.shoujiduoduo.player.PlayerService, boolean):boolean
     arg types: [com.shoujiduoduo.player.PlayerService, int]
     candidates:
      com.shoujiduoduo.player.PlayerService.a(com.shoujiduoduo.player.PlayerService, int):int
      com.shoujiduoduo.player.PlayerService.a(com.shoujiduoduo.base.bean.RingData, boolean):com.shoujiduoduo.base.bean.y
      com.shoujiduoduo.player.PlayerService.a(com.shoujiduoduo.player.PlayerService, java.util.Timer):java.util.Timer
      com.shoujiduoduo.player.PlayerService.a(com.shoujiduoduo.player.PlayerService, com.shoujiduoduo.base.bean.y):boolean
      com.shoujiduoduo.player.PlayerService.a(com.shoujiduoduo.base.bean.c, int):void
      com.shoujiduoduo.player.PlayerService.a(com.shoujiduoduo.base.bean.y, int):void
      com.shoujiduoduo.a.c.h.a(com.shoujiduoduo.base.bean.y, int):void
      com.shoujiduoduo.player.PlayerService.a(com.shoujiduoduo.player.PlayerService, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.player.PlayerService.b(com.shoujiduoduo.player.PlayerService, boolean):boolean
     arg types: [com.shoujiduoduo.player.PlayerService, int]
     candidates:
      com.shoujiduoduo.player.PlayerService.b(com.shoujiduoduo.base.bean.c, int):void
      com.shoujiduoduo.player.PlayerService.b(com.shoujiduoduo.player.PlayerService, boolean):boolean */
    public void handleMessage(Message message) {
        y yVar;
        int e;
        switch (message.what) {
            case SimpleLiveRoomActivity.DOWNLOAD_PROCESS:
                y yVar2 = (y) message.obj;
                if (PlayerService.d != null && yVar2 != null && yVar2.c == PlayerService.d.c) {
                    PlayerService.d.e = yVar2.e;
                    PlayerService.d.g = yVar2.g;
                    PlayerService.d.h = yVar2.h;
                    PlayerService.d.f = yVar2.f;
                    a.a(PlayerService.b, "download_start message: bitrate = " + PlayerService.d.f + ", filesize:" + yVar2.e);
                    return;
                }
                return;
            case SimpleLiveRoomActivity.DOWNLOAD_COMP:
                y yVar3 = (y) message.obj;
                if (PlayerService.d != null && yVar3 != null && yVar3.c == PlayerService.d.c && this.f840a.r.equals(yVar3.g)) {
                    PlayerService.d.d = yVar3.d;
                    if (this.f840a.h && this.f840a.f(PlayerService.d)) {
                        a.a(PlayerService.b, "download_progress message: start play, bitrate = " + PlayerService.d.f);
                        a.a(PlayerService.b, "Download Enough, Start Play!");
                        if (this.f840a.g() == 1) {
                            f.c("play from PlayerService download progress message handler, currentSong is null!");
                        }
                        boolean unused = this.f840a.h = false;
                    }
                    if (this.f840a.i) {
                        if (PlayerService.d.e > 0) {
                            e = PlayerService.d.d - ((int) ((((float) this.f840a.f.e()) * ((float) PlayerService.d.e)) / ((float) this.f840a.j)));
                        } else {
                            e = PlayerService.d.d - ((int) ((((float) PlayerService.d.f) * ((float) this.f840a.f.e())) / 8000.0f));
                        }
                        if (e > (PlayerService.d.f * 10) / 8) {
                            this.f840a.l();
                            boolean unused2 = this.f840a.i = false;
                            return;
                        }
                        return;
                    }
                    return;
                }
                return;
            case 3003:
                a.a(PlayerService.b, "PlayerService: MESSAGE_DONLOAD_FINISH received!");
                y yVar4 = (y) message.obj;
                if (yVar4 != null && PlayerService.d != null && PlayerService.d.c == yVar4.c) {
                    if (this.f840a.h) {
                        if (this.f840a.g() == 1) {
                            f.c("play from PlayerService download finish message handler, currentSong is null!");
                        }
                        boolean unused3 = this.f840a.h = false;
                    }
                    if (this.f840a.i) {
                        this.f840a.l();
                        boolean unused4 = this.f840a.i = false;
                        return;
                    }
                    return;
                }
                return;
            case 3004:
                y yVar5 = (y) message.obj;
                if (yVar5 != null) {
                    this.f840a.c.a(yVar5);
                    return;
                }
                return;
            case 3005:
                PlayerService.a aVar = (PlayerService.a) message.obj;
                if (aVar != null && (yVar = aVar.f828a) != null) {
                    if (yVar.d > 0) {
                        this.f840a.c.a(yVar.d, yVar.c);
                    }
                    if (PlayerService.d != null) {
                        if (yVar.c == PlayerService.d.c && this.f840a.g) {
                            this.f840a.k();
                        }
                        this.f840a.a(6);
                        return;
                    }
                    return;
                }
                return;
            case 3100:
                if (PlayerService.d != null) {
                    int e2 = this.f840a.f.e();
                    int i = 1234567890;
                    if (PlayerService.d.e > 0 && PlayerService.d.d < PlayerService.d.e) {
                        i = PlayerService.d.d - ((int) ((((float) e2) * ((float) PlayerService.d.e)) / ((float) this.f840a.j)));
                    } else if (PlayerService.d.e < 0) {
                        i = PlayerService.d.d - ((int) ((((float) e2) * ((float) PlayerService.d.f)) / 8000.0f));
                    }
                    if (i < (PlayerService.d.f * 5) / 8) {
                        this.f840a.i();
                        boolean unused5 = this.f840a.i = true;
                        return;
                    }
                    return;
                }
                return;
            case 3101:
                if (this.f840a.l != null) {
                    this.f840a.l.cancel();
                    Timer unused6 = this.f840a.l = (Timer) null;
                }
                this.f840a.k();
                this.f840a.a(4);
                if (f.y() && !PlayerService.w && PlayerService.e.c() > 1) {
                    int unused7 = this.f840a.y = (this.f840a.y + 1) % PlayerService.e.c();
                    this.f840a.a(PlayerService.e, this.f840a.y);
                    return;
                }
                return;
            case 3200:
                this.f840a.b(PlayerService.e, this.f840a.y);
                return;
            case 3201:
                boolean unused8 = this.f840a.s = false;
                if (this.f840a.t) {
                    boolean unused9 = this.f840a.t = false;
                    if (this.f840a.m == 3) {
                        this.f840a.l();
                        return;
                    } else if (this.f840a.g() == 1) {
                        f.c("play from PlayerService call idle message handler, currentSong is null!");
                        return;
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            case 3202:
                boolean unused10 = this.f840a.s = true;
                if (this.f840a.m == 2) {
                    this.f840a.h();
                    boolean unused11 = this.f840a.t = true;
                    return;
                }
                return;
            default:
                return;
        }
    }
}
