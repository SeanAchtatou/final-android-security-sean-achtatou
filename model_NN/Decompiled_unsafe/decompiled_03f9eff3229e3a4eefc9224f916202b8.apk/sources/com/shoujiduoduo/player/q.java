package com.shoujiduoduo.player;

import android.content.SharedPreferences;
import android.media.MediaPlayer;
import com.shoujiduoduo.base.a.a;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.RandomAccessFile;
import java.util.Date;
import java.util.Random;
import org.apache.mina.proxy.handlers.socks.SocksProxyConstants;

/* compiled from: SeekTest */
public class q {

    /* renamed from: a  reason: collision with root package name */
    static long f846a;
    static long b;
    private static byte[] c = {-1, -5, 16, -60, 0, 3, -127, -12, 1, 38, 96, 0, 64, 32, 89, Byte.MIN_VALUE, 35, 72, 0, 9, 116, 0, 1, 18, 3, -1, -1, -1, -1, -2, -97, 99, -65, -47, 122, 63, SocksProxyConstants.V4_REPLY_REQUEST_FAILED_ID_NOT_CONFIRMED, 1, -1, -1, -1, -1, -2, -115, -83, 108, 49, 66, -61, 2, -57, 12, 9, -122, -125, -88, 122, 58, 104, 76, 65, 77, 69, 51, 46, 57, 56, 46, 50, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

    public static boolean a(SharedPreferences sharedPreferences) {
        boolean z;
        a.b("SeekTest", "Running CanSeekAccurately");
        boolean z2 = sharedPreferences.getBoolean("seek_test_result", false);
        long j = sharedPreferences.getLong("seek_test_date", 0);
        long time = new Date().getTime();
        if (time - j < 604800000) {
            a.b("SeekTest", "Fast MP3 seek result cached: " + z2);
            return z2;
        }
        String str = "/sdcard/silence" + new Random().nextLong() + ".mp3";
        File file = new File(str);
        boolean z3 = false;
        try {
            new RandomAccessFile(file, "r").close();
        } catch (Exception e) {
            z3 = true;
        }
        if (!z3) {
            a.b("SeekTest", "Couldn't find temporary filename");
            return false;
        }
        a.b("SeekTest", "Writing " + str);
        try {
            file.createNewFile();
            try {
                FileOutputStream fileOutputStream = new FileOutputStream(file);
                for (int i = 0; i < 80; i++) {
                    fileOutputStream.write(c, 0, c.length);
                }
                try {
                    a.b("SeekTest", "File written, starting to play");
                    MediaPlayer mediaPlayer = new MediaPlayer();
                    mediaPlayer.setAudioStreamType(3);
                    mediaPlayer.setDataSource(new FileInputStream(str).getFD(), (long) (c.length * 70), (long) (c.length * 10));
                    a.b("SeekTest", "Preparing");
                    mediaPlayer.prepare();
                    f846a = 0;
                    b = 0;
                    mediaPlayer.setOnCompletionListener(new r());
                    a.b("SeekTest", "Starting");
                    mediaPlayer.start();
                    for (int i2 = 0; i2 < 200 && f846a == 0; i2++) {
                        if (mediaPlayer.getCurrentPosition() > 0) {
                            a.b("SeekTest", "Started playing after " + (i2 * 10) + " ms");
                            f846a = System.currentTimeMillis();
                        }
                        Thread.sleep(10);
                    }
                    if (f846a == 0) {
                        a.b("SeekTest", "Never started playing.");
                        a.b("SeekTest", "Fast MP3 seek disabled by default");
                        try {
                            file.delete();
                        } catch (Exception e2) {
                        }
                        SharedPreferences.Editor edit = sharedPreferences.edit();
                        edit.putLong("seek_test_date", time);
                        edit.putBoolean("seek_test_result", z2);
                        edit.commit();
                        return false;
                    }
                    a.b("SeekTest", "Sleeping");
                    for (int i3 = 0; i3 < 300 && b == 0; i3++) {
                        a.b("SeekTest", "Pos: " + mediaPlayer.getCurrentPosition());
                        Thread.sleep(10);
                    }
                    a.b("SeekTest", "Result: " + f846a + ", " + b);
                    if (b <= f846a || b >= f846a + 2000) {
                        a.b("SeekTest", "Fast MP3 seek disabled");
                        z = z2;
                    } else {
                        a.b("SeekTest", "Fast MP3 seek enabled: " + (b > f846a ? b - f846a : -1));
                        z = true;
                    }
                    SharedPreferences.Editor edit2 = sharedPreferences.edit();
                    edit2.putLong("seek_test_date", time);
                    edit2.putBoolean("seek_test_result", z);
                    edit2.commit();
                    try {
                        file.delete();
                    } catch (Exception e3) {
                    }
                    return z;
                } catch (Exception e4) {
                    e4.printStackTrace();
                    a.b("SeekTest", "Couldn't play: " + e4.toString());
                    a.b("SeekTest", "Fast MP3 seek disabled by default");
                    try {
                        file.delete();
                    } catch (Exception e5) {
                    }
                    SharedPreferences.Editor edit3 = sharedPreferences.edit();
                    edit3.putLong("seek_test_date", time);
                    edit3.putBoolean("seek_test_result", z2);
                    edit3.commit();
                    return false;
                }
            } catch (Exception e6) {
                a.b("SeekTest", "Couldn't write temp silence file");
                try {
                    file.delete();
                } catch (Exception e7) {
                }
                return false;
            }
        } catch (Exception e8) {
            a.b("SeekTest", "Couldn't output for writing");
            return false;
        }
    }
}
