package com.shoujiduoduo.player;

/* compiled from: BasePlayer */
public abstract class c {

    /* renamed from: a  reason: collision with root package name */
    protected boolean f833a = true;
    protected e b = null;
    protected b c = null;
    protected C0022c d = null;
    protected f e = null;
    protected d f = null;
    protected a g = null;
    private final Object h = new Object();
    private int i = 0;

    /* compiled from: BasePlayer */
    public interface a {
        void a(c cVar, int i);
    }

    /* compiled from: BasePlayer */
    public interface b {
        void a(c cVar);
    }

    /* renamed from: com.shoujiduoduo.player.c$c  reason: collision with other inner class name */
    /* compiled from: BasePlayer */
    public interface C0022c {
        boolean a(c cVar, int i, int i2);
    }

    /* compiled from: BasePlayer */
    public interface d {
        void a(c cVar, int i, int i2);
    }

    /* compiled from: BasePlayer */
    public interface e {
        void a(c cVar);
    }

    /* compiled from: BasePlayer */
    public interface f {
        void b(c cVar);
    }

    public abstract int a(String str);

    public abstract void b();

    public abstract void c();

    public abstract void d();

    public abstract int e();

    public abstract int f();

    public abstract int h();

    public void a(boolean z) {
        this.f833a = z;
    }

    public void a() {
        d();
        a(0);
    }

    public int g() {
        int i2;
        synchronized (this.h) {
            i2 = this.i;
        }
        return i2;
    }

    public void a(int i2) {
        int i3;
        synchronized (this.h) {
            i3 = this.i;
            this.i = i2;
        }
        if (i3 != i2 && this.e != null) {
            this.e.b(this);
        }
    }

    public boolean i() {
        return g() == 4;
    }

    public boolean j() {
        return g() == 3;
    }

    public boolean k() {
        return g() == 0 || g() == 5;
    }

    public void a(b bVar) {
        this.c = bVar;
    }
}
