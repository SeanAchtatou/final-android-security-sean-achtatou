package com.shoujiduoduo.player;

import cn.banshenggua.aichang.utils.Constants;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.util.ah;

public class NativeAACDecoder implements d {

    /* renamed from: a  reason: collision with root package name */
    static String[] f825a = {"aac", "m4a", "m4b", "mp4"};
    private static final String b = NativeAACDecoder.class.getSimpleName();
    private static boolean d = ah.a("opencore_aac");
    private int c = -1;

    private native void closeFile(int i);

    private native int getBitrate(int i);

    private native int getChannelNum(int i);

    private native int getCurrentPosition(int i);

    private native int getDuration(int i);

    private native int getSamplePerFrame(int i);

    private native int getSamplerate(int i);

    private native int isReadFinished(int i);

    private native int openFile(String str);

    public native int readSamples(int i, short[] sArr, int i2);

    static {
        a.a(b, "load aac codec, res:" + d);
    }

    public static boolean j() {
        return d;
    }

    public int a(String str) {
        this.c = openFile(str);
        return this.c;
    }

    public int b() {
        if (this.c != -1) {
            return getChannelNum(this.c);
        }
        return 0;
    }

    public int c() {
        return getBitrate(this.c);
    }

    public int d() {
        return getSamplerate(this.c);
    }

    public int e() {
        if (this.c != -1) {
            return getDuration(this.c);
        }
        return 0;
    }

    public int f() {
        if (this.c != -1) {
            return getCurrentPosition(this.c);
        }
        return 0;
    }

    public int g() {
        return getSamplePerFrame(this.c);
    }

    public void a() {
        if (this.c != -1) {
            closeFile(this.c);
            this.c = -1;
        }
    }

    public boolean k() {
        return this.c == -1;
    }

    public int a(short[] sArr) {
        if (this.c != -1) {
            return readSamples(this.c, sArr, sArr.length);
        }
        return 0;
    }

    public boolean h() {
        if (k() || isReadFinished(this.c) == 1 || f() / Constants.CLEARIMGED == e()) {
            return true;
        }
        return false;
    }

    public String[] i() {
        return f825a;
    }
}
