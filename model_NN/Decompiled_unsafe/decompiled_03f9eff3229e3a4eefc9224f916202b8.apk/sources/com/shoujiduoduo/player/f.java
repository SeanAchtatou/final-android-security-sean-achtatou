package com.shoujiduoduo.player;

/* compiled from: PlayerManager */
public class f {

    /* renamed from: a  reason: collision with root package name */
    private static final String f836a = f.class.getSimpleName();
    private e b;
    private s c;

    /* compiled from: PlayerManager */
    private static class a {

        /* renamed from: a  reason: collision with root package name */
        public static f f837a = new f();
    }

    private f() {
        this.b = null;
        this.c = null;
        this.c = new s();
        this.b = new e();
    }

    public static f a() {
        return a.f837a;
    }

    public void b() {
        if (this.c != null) {
            this.c.a();
            this.c.l();
        }
        if (this.b != null) {
            this.b.a();
            this.b.m();
        }
    }

    public c c() {
        if (this.b == null) {
            this.b = new e();
        }
        return this.b;
    }

    public c d() {
        if (this.c == null) {
            this.c = new s();
        }
        return this.c;
    }
}
