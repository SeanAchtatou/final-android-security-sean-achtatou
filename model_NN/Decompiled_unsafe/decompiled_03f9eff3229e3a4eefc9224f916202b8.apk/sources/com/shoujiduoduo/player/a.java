package com.shoujiduoduo.player;

import android.media.AudioRecord;
import android.support.v4.BuildConfig;
import android.text.format.Time;
import com.shoujiduoduo.player.m;
import com.shoujiduoduo.util.NativeMP3Lame;
import com.shoujiduoduo.util.ah;
import com.shoujiduoduo.util.bi;
import com.shoujiduoduo.util.l;
import com.shoujiduoduo.util.t;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/* compiled from: AudioRecorder */
public class a extends m {
    private static boolean n = ah.a("mp3lame");
    private static a o;
    /* access modifiers changed from: private */
    public AudioRecord g;
    private Thread h;
    /* access modifiers changed from: private */
    public int i;
    /* access modifiers changed from: private */
    public FileOutputStream j;
    /* access modifiers changed from: private */
    public long k;
    private String l;
    private boolean m = false;

    static /* synthetic */ long a(a aVar, long j2) {
        long j3 = aVar.k + j2;
        aVar.k = j3;
        return j3;
    }

    static {
        com.shoujiduoduo.base.a.a.a("AudioRecorder", "load lame lib, res:" + n);
    }

    public static boolean a() {
        return n;
    }

    private a() {
    }

    public static a b() {
        if (o == null) {
            o = new a();
        }
        return o;
    }

    private int m() {
        this.f841a = 0;
        this.k = 0;
        if (this.b == m.a.Start) {
            return -100;
        }
        this.i = AudioRecord.getMinBufferSize(44100, 16, 2);
        if (this.i == -2) {
            com.shoujiduoduo.base.a.a.a("AudioRecorder", "getMinBufferSize error, AudioRecord.ERROR_BAD_VALUE");
            return -2;
        }
        com.shoujiduoduo.base.a.a.b("AudioRecorder", "minBufferSize is " + this.i);
        this.g = new AudioRecord(1, 44100, 16, 2, this.i);
        if (this.g.getState() != 1) {
            com.shoujiduoduo.base.a.a.a("AudioRecorder", "create audio record instance error, AudioRecord.STATE_UNINITIALIZED");
            return 0;
        }
        this.g.setPositionNotificationPeriod(this.g.getSampleRate());
        this.g.setRecordPositionUpdateListener(new b(this));
        this.l = "";
        n();
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(a("wav"));
            bi.a(fileOutputStream, 0, 44100, 1);
            fileOutputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        if (NativeMP3Lame.a().initEncoder(1, 44100, 128, 3, 5) == -1) {
            com.shoujiduoduo.base.a.a.c("AudioRecorder", "lame encoder init error");
            return -1;
        } else if (NativeMP3Lame.a().setTargetFile(a("mp3")) != -1) {
            return this.i;
        } else {
            com.shoujiduoduo.base.a.a.c("AudioRecorder", "lame encoder setTargetFile error");
            return -1;
        }
    }

    public int c() {
        return (int) ((((float) this.k) * 1000.0f) / 88200.0f);
    }

    public String d() {
        return a("wav");
    }

    public int e() {
        return this.i / 80;
    }

    public int f() {
        super.f();
        if (!this.m) {
            int m2 = m();
            if (m2 <= 0) {
                return m2;
            }
            this.m = true;
        }
        if (this.b == m.a.Start) {
            return -100;
        }
        if (this.g == null) {
            com.shoujiduoduo.base.a.a.c("AudioRecorder", "audioRecord is null when start");
            return -1;
        } else if (this.g.getState() == 0) {
            com.shoujiduoduo.base.a.a.a("AudioRecorder", "audioRecord UNINITIALIZED");
            return -1;
        } else {
            com.shoujiduoduo.base.a.a.b("AudioRecorder", "start:");
            a(this, m.a.Start);
            this.h = new Thread(new C0021a());
            this.h.setName("ThreadAudioRecord");
            this.h.start();
            return this.i;
        }
    }

    public void g() {
        if (this.b == m.a.Start) {
            com.shoujiduoduo.base.a.a.c("AudioRecorder", "resume: audioRecord is recording");
        } else if (this.g == null) {
            com.shoujiduoduo.base.a.a.c("AudioRecorder", "resume: audioRecord is null when start");
        } else if (this.g.getState() == 0) {
            com.shoujiduoduo.base.a.a.a("AudioRecorder", "resume: audioRecord UNINITIALIZED");
        } else {
            com.shoujiduoduo.base.a.a.b("AudioRecorder", "resume:");
            a(this, m.a.Start);
            this.h = new Thread(new C0021a());
            this.h.setName("ThreadAudioRecord");
            this.h.start();
        }
    }

    public void h() {
        if (this.b == m.a.Start) {
            if (this.g == null) {
                com.shoujiduoduo.base.a.a.c("AudioRecorder", "audioRecord is null when pause");
                return;
            }
            com.shoujiduoduo.base.a.a.b("AudioRecorder", "pauseRecord");
            a(this, m.a.Pause);
            this.g.stop();
            this.h = null;
        }
    }

    public void i() {
        com.shoujiduoduo.base.a.a.b("AudioRecorder", "stopRecord");
        this.f841a = 0;
        if (this.b != null && this.b != m.a.Stop) {
            if (this.g == null) {
                com.shoujiduoduo.base.a.a.c("AudioRecorder", "audioRecord is null when stop");
                return;
            }
            a(this, m.a.Stop);
            j();
            this.m = false;
            this.k = 0;
            NativeMP3Lame.a().destroyEncoder();
        }
    }

    /* renamed from: com.shoujiduoduo.player.a$a  reason: collision with other inner class name */
    /* compiled from: AudioRecorder */
    class C0021a implements Runnable {
        C0021a() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
         arg types: [java.lang.String, int]
         candidates:
          ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
          ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException} */
        /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
            com.shoujiduoduo.player.a.b(r12.f831a).flush();
            com.shoujiduoduo.player.a.b(r12.f831a).close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
            r0 = new java.io.RandomAccessFile(r12.f831a.a("wav"), "rw");
            r0.seek(4);
            r0.write(new byte[]{(byte) ((int) ((com.shoujiduoduo.player.a.d(r12.f831a) + 36) & 255)), (byte) ((int) (((com.shoujiduoduo.player.a.d(r12.f831a) + 36) >> 8) & 255)), (byte) ((int) (((com.shoujiduoduo.player.a.d(r12.f831a) + 36) >> 16) & 255)), (byte) ((int) (((com.shoujiduoduo.player.a.d(r12.f831a) + 36) >> 24) & 255))});
            r0.seek(40);
            r0.write(new byte[]{(byte) ((int) (com.shoujiduoduo.player.a.d(r12.f831a) & 255)), (byte) ((int) ((com.shoujiduoduo.player.a.d(r12.f831a) >> 8) & 255)), (byte) ((int) ((com.shoujiduoduo.player.a.d(r12.f831a) >> 16) & 255)), (byte) ((int) ((com.shoujiduoduo.player.a.d(r12.f831a) >> 24) & 255))});
            r0.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:19:0x0113, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:20:0x0114, code lost:
            com.shoujiduoduo.base.a.a.c("AudioRecorder", "record ioexception");
            com.shoujiduoduo.base.a.a.a(r0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:52:0x01ae, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:54:?, code lost:
            r0.printStackTrace();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:65:?, code lost:
            return;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r12 = this;
                r10 = 36
                r8 = 255(0xff, double:1.26E-321)
                r1 = 0
                com.shoujiduoduo.player.a r0 = com.shoujiduoduo.player.a.this
                int r0 = r0.i
                int r0 = r0 / 2
                short[] r4 = new short[r0]
                com.shoujiduoduo.player.a r0 = com.shoujiduoduo.player.a.this
                java.lang.String r2 = "wav"
                java.lang.String r0 = r0.a(r2)
                com.shoujiduoduo.player.a r2 = com.shoujiduoduo.player.a.this     // Catch:{ FileNotFoundException -> 0x002b }
                java.io.FileOutputStream r3 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x002b }
                r5 = 1
                r3.<init>(r0, r5)     // Catch:{ FileNotFoundException -> 0x002b }
                java.io.FileOutputStream unused = r2.j = r3     // Catch:{ FileNotFoundException -> 0x002b }
            L_0x0022:
                com.shoujiduoduo.player.a r0 = com.shoujiduoduo.player.a.this
                java.io.FileOutputStream r0 = r0.j
                if (r0 != 0) goto L_0x0030
            L_0x002a:
                return
            L_0x002b:
                r0 = move-exception
                com.shoujiduoduo.base.a.a.a(r0)
                goto L_0x0022
            L_0x0030:
                com.shoujiduoduo.player.a r0 = com.shoujiduoduo.player.a.this
                android.media.AudioRecord r0 = r0.g
                r0.startRecording()
                java.lang.String r0 = "AudioRecorder"
                java.lang.String r2 = "start recroding"
                com.shoujiduoduo.base.a.a.a(r0, r2)
                com.shoujiduoduo.player.a r0 = com.shoujiduoduo.player.a.this
                com.shoujiduoduo.player.a r2 = com.shoujiduoduo.player.a.this
                com.shoujiduoduo.player.m$a r3 = com.shoujiduoduo.player.m.a.Start
                r0.a(r2, r3)
                r0 = r1
            L_0x004a:
                com.shoujiduoduo.player.a r2 = com.shoujiduoduo.player.a.this
                com.shoujiduoduo.player.m$a r2 = r2.b
                com.shoujiduoduo.player.m$a r3 = com.shoujiduoduo.player.m.a.Start
                if (r2 != r3) goto L_0x005a
                com.shoujiduoduo.player.a r2 = com.shoujiduoduo.player.a.this
                android.media.AudioRecord r2 = r2.g
                if (r2 != 0) goto L_0x0120
            L_0x005a:
                com.shoujiduoduo.player.a r0 = com.shoujiduoduo.player.a.this     // Catch:{ IOException -> 0x0113 }
                java.io.FileOutputStream r0 = r0.j     // Catch:{ IOException -> 0x0113 }
                r0.flush()     // Catch:{ IOException -> 0x0113 }
                com.shoujiduoduo.player.a r0 = com.shoujiduoduo.player.a.this     // Catch:{ IOException -> 0x0113 }
                java.io.FileOutputStream r0 = r0.j     // Catch:{ IOException -> 0x0113 }
                r0.close()     // Catch:{ IOException -> 0x0113 }
                java.io.RandomAccessFile r0 = new java.io.RandomAccessFile     // Catch:{ Exception -> 0x01ae }
                com.shoujiduoduo.player.a r1 = com.shoujiduoduo.player.a.this     // Catch:{ Exception -> 0x01ae }
                java.lang.String r2 = "wav"
                java.lang.String r1 = r1.a(r2)     // Catch:{ Exception -> 0x01ae }
                java.lang.String r2 = "rw"
                r0.<init>(r1, r2)     // Catch:{ Exception -> 0x01ae }
                r2 = 4
                r0.seek(r2)     // Catch:{ Exception -> 0x01ae }
                r1 = 4
                byte[] r1 = new byte[r1]     // Catch:{ Exception -> 0x01ae }
                r2 = 0
                com.shoujiduoduo.player.a r3 = com.shoujiduoduo.player.a.this     // Catch:{ Exception -> 0x01ae }
                long r4 = r3.k     // Catch:{ Exception -> 0x01ae }
                long r4 = r4 + r10
                long r4 = r4 & r8
                int r3 = (int) r4     // Catch:{ Exception -> 0x01ae }
                byte r3 = (byte) r3     // Catch:{ Exception -> 0x01ae }
                r1[r2] = r3     // Catch:{ Exception -> 0x01ae }
                r2 = 1
                com.shoujiduoduo.player.a r3 = com.shoujiduoduo.player.a.this     // Catch:{ Exception -> 0x01ae }
                long r4 = r3.k     // Catch:{ Exception -> 0x01ae }
                long r4 = r4 + r10
                r3 = 8
                long r4 = r4 >> r3
                long r4 = r4 & r8
                int r3 = (int) r4     // Catch:{ Exception -> 0x01ae }
                byte r3 = (byte) r3     // Catch:{ Exception -> 0x01ae }
                r1[r2] = r3     // Catch:{ Exception -> 0x01ae }
                r2 = 2
                com.shoujiduoduo.player.a r3 = com.shoujiduoduo.player.a.this     // Catch:{ Exception -> 0x01ae }
                long r4 = r3.k     // Catch:{ Exception -> 0x01ae }
                long r4 = r4 + r10
                r3 = 16
                long r4 = r4 >> r3
                long r4 = r4 & r8
                int r3 = (int) r4     // Catch:{ Exception -> 0x01ae }
                byte r3 = (byte) r3     // Catch:{ Exception -> 0x01ae }
                r1[r2] = r3     // Catch:{ Exception -> 0x01ae }
                r2 = 3
                com.shoujiduoduo.player.a r3 = com.shoujiduoduo.player.a.this     // Catch:{ Exception -> 0x01ae }
                long r4 = r3.k     // Catch:{ Exception -> 0x01ae }
                long r4 = r4 + r10
                r3 = 24
                long r4 = r4 >> r3
                long r4 = r4 & r8
                int r3 = (int) r4     // Catch:{ Exception -> 0x01ae }
                byte r3 = (byte) r3     // Catch:{ Exception -> 0x01ae }
                r1[r2] = r3     // Catch:{ Exception -> 0x01ae }
                r0.write(r1)     // Catch:{ Exception -> 0x01ae }
                r2 = 40
                r0.seek(r2)     // Catch:{ Exception -> 0x01ae }
                r1 = 4
                byte[] r1 = new byte[r1]     // Catch:{ Exception -> 0x01ae }
                r2 = 0
                com.shoujiduoduo.player.a r3 = com.shoujiduoduo.player.a.this     // Catch:{ Exception -> 0x01ae }
                long r4 = r3.k     // Catch:{ Exception -> 0x01ae }
                long r4 = r4 & r8
                int r3 = (int) r4     // Catch:{ Exception -> 0x01ae }
                byte r3 = (byte) r3     // Catch:{ Exception -> 0x01ae }
                r1[r2] = r3     // Catch:{ Exception -> 0x01ae }
                r2 = 1
                com.shoujiduoduo.player.a r3 = com.shoujiduoduo.player.a.this     // Catch:{ Exception -> 0x01ae }
                long r4 = r3.k     // Catch:{ Exception -> 0x01ae }
                r3 = 8
                long r4 = r4 >> r3
                long r4 = r4 & r8
                int r3 = (int) r4     // Catch:{ Exception -> 0x01ae }
                byte r3 = (byte) r3     // Catch:{ Exception -> 0x01ae }
                r1[r2] = r3     // Catch:{ Exception -> 0x01ae }
                r2 = 2
                com.shoujiduoduo.player.a r3 = com.shoujiduoduo.player.a.this     // Catch:{ Exception -> 0x01ae }
                long r4 = r3.k     // Catch:{ Exception -> 0x01ae }
                r3 = 16
                long r4 = r4 >> r3
                long r4 = r4 & r8
                int r3 = (int) r4     // Catch:{ Exception -> 0x01ae }
                byte r3 = (byte) r3     // Catch:{ Exception -> 0x01ae }
                r1[r2] = r3     // Catch:{ Exception -> 0x01ae }
                r2 = 3
                com.shoujiduoduo.player.a r3 = com.shoujiduoduo.player.a.this     // Catch:{ Exception -> 0x01ae }
                long r4 = r3.k     // Catch:{ Exception -> 0x01ae }
                r3 = 24
                long r4 = r4 >> r3
                long r4 = r4 & r8
                int r3 = (int) r4     // Catch:{ Exception -> 0x01ae }
                byte r3 = (byte) r3     // Catch:{ Exception -> 0x01ae }
                r1[r2] = r3     // Catch:{ Exception -> 0x01ae }
                r0.write(r1)     // Catch:{ Exception -> 0x01ae }
                r0.close()     // Catch:{ Exception -> 0x01ae }
            L_0x010a:
                java.lang.String r0 = "AudioRecorder"
                java.lang.String r1 = "writeAudioDataToFile end"
                com.shoujiduoduo.base.a.a.b(r0, r1)     // Catch:{ IOException -> 0x0113 }
                goto L_0x002a
            L_0x0113:
                r0 = move-exception
                java.lang.String r1 = "AudioRecorder"
                java.lang.String r2 = "record ioexception"
                com.shoujiduoduo.base.a.a.c(r1, r2)
                com.shoujiduoduo.base.a.a.a(r0)
                goto L_0x002a
            L_0x0120:
                com.shoujiduoduo.player.a r2 = com.shoujiduoduo.player.a.this
                android.media.AudioRecord r2 = r2.g
                monitor-enter(r2)
                com.shoujiduoduo.player.a r3 = com.shoujiduoduo.player.a.this     // Catch:{ all -> 0x0179 }
                android.media.AudioRecord r3 = r3.g     // Catch:{ all -> 0x0179 }
                r5 = 0
                com.shoujiduoduo.player.a r6 = com.shoujiduoduo.player.a.this     // Catch:{ all -> 0x0179 }
                int r6 = r6.i     // Catch:{ all -> 0x0179 }
                int r6 = r6 / 2
                int r3 = r3.read(r4, r5, r6)     // Catch:{ all -> 0x0179 }
                monitor-exit(r2)     // Catch:{ all -> 0x0179 }
                r2 = -3
                if (r3 == r2) goto L_0x0197
                r2 = -2
                if (r3 == r2) goto L_0x0197
                if (r3 <= 0) goto L_0x0197
                com.shoujiduoduo.player.a r2 = com.shoujiduoduo.player.a.this     // Catch:{ IOException -> 0x0191 }
                int r5 = r3 * 2
                long r6 = (long) r5     // Catch:{ IOException -> 0x0191 }
                com.shoujiduoduo.player.a.a(r2, r6)     // Catch:{ IOException -> 0x0191 }
                com.shoujiduoduo.util.NativeMP3Lame r2 = com.shoujiduoduo.util.NativeMP3Lame.a()     // Catch:{ IOException -> 0x0191 }
                r2.encodeSamples(r4, r3)     // Catch:{ IOException -> 0x0191 }
                com.shoujiduoduo.player.a r2 = com.shoujiduoduo.player.a.this     // Catch:{ IOException -> 0x0191 }
                java.io.FileOutputStream r2 = r2.j     // Catch:{ IOException -> 0x0191 }
                r5 = 0
                int r6 = r4.length     // Catch:{ IOException -> 0x0191 }
                byte[] r5 = com.shoujiduoduo.util.ad.a(r4, r5, r6)     // Catch:{ IOException -> 0x0191 }
                r6 = 0
                int r7 = r3 * 2
                r2.write(r5, r6, r7)     // Catch:{ IOException -> 0x0191 }
                int r2 = r3 / 40
                short[] r5 = new short[r2]     // Catch:{ IOException -> 0x0191 }
                r2 = r1
                r3 = r1
            L_0x016a:
                int r6 = r5.length     // Catch:{ IOException -> 0x0191 }
                if (r3 >= r6) goto L_0x017c
                int r6 = r4.length     // Catch:{ IOException -> 0x0191 }
                if (r2 >= r6) goto L_0x017c
                short r2 = r4[r2]     // Catch:{ IOException -> 0x0191 }
                r5[r3] = r2     // Catch:{ IOException -> 0x0191 }
                int r3 = r3 + 1
                int r2 = r3 * 40
                goto L_0x016a
            L_0x0179:
                r0 = move-exception
                monitor-exit(r2)     // Catch:{ all -> 0x0179 }
                throw r0
            L_0x017c:
                com.shoujiduoduo.player.a r2 = com.shoujiduoduo.player.a.this     // Catch:{ IOException -> 0x0191 }
                com.shoujiduoduo.player.m$a r2 = r2.b     // Catch:{ IOException -> 0x0191 }
                com.shoujiduoduo.player.m$a r3 = com.shoujiduoduo.player.m.a.Pause     // Catch:{ IOException -> 0x0191 }
                boolean r2 = r2.equals(r3)     // Catch:{ IOException -> 0x0191 }
                if (r2 != 0) goto L_0x004a
                com.shoujiduoduo.player.a r2 = com.shoujiduoduo.player.a.this     // Catch:{ IOException -> 0x0191 }
                com.shoujiduoduo.player.a r3 = com.shoujiduoduo.player.a.this     // Catch:{ IOException -> 0x0191 }
                r2.a(r3, r5)     // Catch:{ IOException -> 0x0191 }
                goto L_0x004a
            L_0x0191:
                r2 = move-exception
                com.shoujiduoduo.base.a.a.a(r2)
                goto L_0x004a
            L_0x0197:
                int r0 = r0 + 1
                r2 = 5
                if (r0 <= r2) goto L_0x004a
                java.lang.String r0 = "AudioRecorder"
                java.lang.String r1 = "录制无法读取到数据，认为出错了"
                com.shoujiduoduo.base.a.a.c(r0, r1)
                com.shoujiduoduo.player.a r0 = com.shoujiduoduo.player.a.this
                com.shoujiduoduo.player.a r1 = com.shoujiduoduo.player.a.this
                com.shoujiduoduo.player.m$a r2 = com.shoujiduoduo.player.m.a.Error
                r0.a(r1, r2)
                goto L_0x005a
            L_0x01ae:
                r0 = move-exception
                r0.printStackTrace()     // Catch:{ IOException -> 0x0113 }
                goto L_0x010a
            */
            throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.player.a.C0021a.run():void");
        }
    }

    public void j() {
        com.shoujiduoduo.base.a.a.a("AudioRecorder", BuildConfig.BUILD_TYPE);
        if (this.g != null) {
            if (this.g.getState() == 1) {
                try {
                    this.g.stop();
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
            }
            this.g.release();
            this.g = null;
        }
        this.b = m.a.f842a;
        this.m = false;
        this.h = null;
        k();
    }

    public void k() {
        com.shoujiduoduo.base.a.a.a("AudioRecorder", "clear");
        try {
            new File(a("wav")).delete();
            new File(a("mp3")).delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.l = "";
    }

    private void n() {
    }

    public String a(String str) {
        if (this.l == null || this.l.equals("")) {
            Time time = new Time();
            time.setToNow();
            this.l = l.a(3) + time.format("%Y%m%d%H%M%S") + ".wav";
        }
        return t.a(this.l) + "." + str;
    }
}
