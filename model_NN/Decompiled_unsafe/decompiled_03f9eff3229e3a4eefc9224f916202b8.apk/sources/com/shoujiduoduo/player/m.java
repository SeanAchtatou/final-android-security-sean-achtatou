package com.shoujiduoduo.player;

import android.os.Handler;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: Recorder */
public abstract class m {

    /* renamed from: a  reason: collision with root package name */
    protected int f841a = 0;
    public a b = a.f842a;
    protected ArrayList<d> c = new ArrayList<>();
    protected ArrayList<b> d = new ArrayList<>();
    protected ArrayList<c> e = new ArrayList<>();
    protected Handler f = new Handler();

    /* compiled from: Recorder */
    public enum a {
        f842a,
        Start,
        Pause,
        Stop,
        Complete,
        Error
    }

    /* compiled from: Recorder */
    public interface b {
        void a(m mVar, long j);
    }

    /* compiled from: Recorder */
    public interface c {
        void a(m mVar, a aVar);
    }

    /* compiled from: Recorder */
    public interface d {
        void a(m mVar, short[] sArr);
    }

    public int f() {
        return 0;
    }

    public a l() {
        return this.b;
    }

    public void a(d dVar) {
        this.c.add(dVar);
    }

    public void b(d dVar) {
        if (this.c.contains(dVar)) {
            this.c.remove(dVar);
        }
    }

    public void a(b bVar) {
        this.d.add(bVar);
    }

    public void b(b bVar) {
        if (this.d.contains(bVar)) {
            this.d.remove(bVar);
        }
    }

    public void a(c cVar) {
        this.e.add(cVar);
    }

    public void b(c cVar) {
        if (this.e.contains(cVar)) {
            this.e.remove(cVar);
        }
    }

    /* access modifiers changed from: protected */
    public void a(m mVar, short[] sArr) {
        Iterator<d> it = this.c.iterator();
        while (it.hasNext()) {
            d next = it.next();
            if (next != null) {
                this.f.post(new n(this, next, mVar, sArr));
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(m mVar) {
        Iterator<b> it = this.d.iterator();
        while (it.hasNext()) {
            b next = it.next();
            if (next != null) {
                this.f.post(new o(this, next, mVar));
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(m mVar, a aVar) {
        this.b = aVar;
        Iterator<c> it = this.e.iterator();
        while (it.hasNext()) {
            c next = it.next();
            if (next != null) {
                this.f.post(new p(this, next, mVar, aVar));
            }
        }
    }
}
