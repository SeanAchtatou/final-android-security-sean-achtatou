package com.shoujiduoduo.player;

import android.media.AudioRecord;
import com.shoujiduoduo.base.a.a;

/* compiled from: AudioRecorder */
class b implements AudioRecord.OnRecordPositionUpdateListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f832a;

    b(a aVar) {
        this.f832a = aVar;
    }

    public void onPeriodicNotification(AudioRecord audioRecord) {
        this.f832a.f841a++;
        this.f832a.a((m) this.f832a);
    }

    public void onMarkerReached(AudioRecord audioRecord) {
        a.b("AudioRecorder", "onMarkerReached");
    }
}
