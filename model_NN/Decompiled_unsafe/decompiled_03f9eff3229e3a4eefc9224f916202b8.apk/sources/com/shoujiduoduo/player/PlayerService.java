package com.shoujiduoduo.player;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.v4.app.NotificationCompat;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.widget.RemoteViews;
import cn.banshenggua.aichang.room.SimpleLiveRoomActivity;
import cn.banshenggua.aichang.utils.Constants;
import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.a.c.h;
import com.shoujiduoduo.b.c.w;
import com.shoujiduoduo.base.bean.MakeRingData;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.base.bean.y;
import com.shoujiduoduo.player.c;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.activity.WelcomeActivity;
import com.shoujiduoduo.util.p;
import com.shoujiduoduo.util.t;
import com.shoujiduoduo.util.u;
import com.shoujiduoduo.util.widget.f;
import java.io.File;
import java.util.HashMap;
import java.util.Timer;
import org.apache.mina.proxy.handlers.http.ntlm.NTLMConstants;

public class PlayerService extends Service implements h, c.b {
    /* access modifiers changed from: private */
    public static final String b = PlayerService.class.getSimpleName();
    /* access modifiers changed from: private */
    public static y d;
    /* access modifiers changed from: private */
    public static com.shoujiduoduo.base.bean.c e;
    /* access modifiers changed from: private */
    public static boolean w = false;

    /* renamed from: a  reason: collision with root package name */
    public int f827a = 0;
    /* access modifiers changed from: private */
    public p c;
    /* access modifiers changed from: private */
    public c f;
    /* access modifiers changed from: private */
    public boolean g = false;
    /* access modifiers changed from: private */
    public boolean h;
    /* access modifiers changed from: private */
    public boolean i = false;
    /* access modifiers changed from: private */
    public int j;
    private int k;
    /* access modifiers changed from: private */
    public Timer l;
    /* access modifiers changed from: private */
    public int m;
    /* access modifiers changed from: private */
    public final Object n = new Object();
    private IBinder o = new b();
    private boolean p;
    private boolean q;
    /* access modifiers changed from: private */
    public String r = "";
    /* access modifiers changed from: private */
    public boolean s = false;
    /* access modifiers changed from: private */
    public boolean t = false;
    private c u = null;
    private NotificationManager v = null;
    private TelephonyManager x;
    /* access modifiers changed from: private */
    public int y = -1;
    /* access modifiers changed from: private */
    public Handler z = new l(this);

    public static void a(boolean z2) {
        w = z2;
    }

    public class b extends Binder {
        public b() {
        }

        public PlayerService a() {
            com.shoujiduoduo.base.a.a.a(PlayerService.b, "Service: getService finished!");
            return PlayerService.this;
        }
    }

    public IBinder onBind(Intent intent) {
        com.shoujiduoduo.base.a.a.a(b, "Service: PlayerService onBind Finished!");
        return this.o;
    }

    public PlayerService() {
        com.shoujiduoduo.base.a.a.a(b, "PlayerService constructor.");
    }

    public int a() {
        return this.m;
    }

    public String b() {
        if (e == null || d == null) {
            return "";
        }
        return e.a();
    }

    public int c() {
        if (d == null) {
            return -1;
        }
        return this.y;
    }

    public void a(int i2) {
        if (!(e == null || this.m == i2)) {
            String a2 = e.a();
            int i3 = this.y;
            com.shoujiduoduo.base.a.a.a("RingListAdapter", "playerService, listid:" + a2 + ", status:" + i2);
            x.a().b(com.shoujiduoduo.a.a.b.OBSERVER_PLAY_STATUS, new g(this, a2, i3, i2));
        }
        this.m = i2;
        q();
        if (this.f827a == -12) {
            f.a((int) R.string.sdcard_full);
        }
    }

    private void q() {
        this.v.notify(2001, r());
    }

    private Notification r() {
        Intent intent = new Intent("android.intent.action.MAIN");
        intent.addCategory("android.intent.category.LAUNCHER");
        intent.setClass(this, WelcomeActivity.class);
        PendingIntent activity = PendingIntent.getActivity(this, 2001, intent, NTLMConstants.FLAG_UNIDENTIFIED_10);
        Intent intent2 = new Intent("com.shoujiduoduo.ringtone.PlayerService.togglePlayPause");
        ComponentName componentName = new ComponentName(this, PlayerService.class);
        intent2.setComponent(componentName);
        PendingIntent service = PendingIntent.getService(this, 0, intent2, NTLMConstants.FLAG_UNIDENTIFIED_10);
        PendingIntent broadcast = PendingIntent.getBroadcast(getApplicationContext(), 0, new Intent("com.shoujiduoduo.ringtone.exitapp"), NTLMConstants.FLAG_UNIDENTIFIED_10);
        Intent intent3 = new Intent("com.shoujiduoduo.ringtone.PlayerService.toggleNext");
        intent3.setComponent(componentName);
        PendingIntent service2 = PendingIntent.getService(this, 0, intent3, NTLMConstants.FLAG_UNIDENTIFIED_10);
        String string = getResources().getString(R.string.start_notification);
        String string2 = getResources().getString(R.string.app_name);
        if (d != null) {
            string = d.f824a + " - " + d.b;
        }
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder.setSmallIcon(R.drawable.duoduo_icon);
        builder.setContentTitle(string2);
        builder.setContentText(string);
        builder.setTicker(string2);
        builder.setContentIntent(activity);
        builder.setOngoing(true);
        if (Build.VERSION.SDK_INT >= 11) {
            RemoteViews remoteViews = new RemoteViews(getPackageName(), (int) R.layout.notif_bar);
            remoteViews.setImageViewResource(R.id.notif_icon, R.drawable.duoduo_icon);
            remoteViews.setTextViewText(R.id.notif_ring_name, d == null ? string2 : d.f824a);
            remoteViews.setTextViewText(R.id.notif_ring_artist, d == null ? getResources().getString(R.string.start_notification) : d.b);
            if (this.m == 2) {
                remoteViews.setImageViewResource(R.id.notif_toggle_playpause, R.drawable.btn_notif_pause);
            } else {
                remoteViews.setImageViewResource(R.id.notif_toggle_playpause, R.drawable.btn_notif_play);
            }
            remoteViews.setOnClickPendingIntent(R.id.notif_toggle_playpause, service);
            remoteViews.setOnClickPendingIntent(R.id.notif_toggle_next, service2);
            remoteViews.setOnClickPendingIntent(R.id.notif_toggle_exit, broadcast);
            builder.setContent(remoteViews);
        }
        return builder.build();
    }

    public void onCreate() {
        super.onCreate();
        com.shoujiduoduo.base.a.a.a(b, "PlayerService onCreate.");
        this.c = p.a(getApplicationContext());
        this.c.a((h) this);
        this.c.a(this);
        this.u = new c(this, null);
        this.x = (TelephonyManager) getSystemService("phone");
        this.x.listen(this.u, 32);
        f.a().d().a(this);
        f.a().c().a(this);
        this.m = 5;
        d = null;
        this.v = (NotificationManager) getSystemService("notification");
        q();
        if (!NativeMP3Decoder.j()) {
            HashMap hashMap = new HashMap();
            hashMap.put("lib", "mad");
            com.umeng.analytics.b.a(getApplicationContext(), "LOAD_SO_ERROR", hashMap);
        }
        if (!NativeAACDecoder.j()) {
            HashMap hashMap2 = new HashMap();
            hashMap2.put("lib", "aac");
            com.umeng.analytics.b.a(getApplicationContext(), "LOAD_SO_ERROR", hashMap2);
        }
        com.shoujiduoduo.base.a.a.a(b, "Service: PlayerService onCreate Finished!");
    }

    public void onDestroy() {
        com.shoujiduoduo.base.a.a.a(b, "PlayerService onDestroy.");
        if (this.g) {
            k();
        }
        this.x.listen(this.u, 0);
        this.u = null;
        f.a().b();
        this.v.cancel(2001);
        this.v = null;
        stopSelf();
        d = null;
        synchronized (this.n) {
            this.z.removeCallbacksAndMessages(null);
            this.z = null;
            com.shoujiduoduo.base.a.a.a(b, "ServiceOnDestroy: mHandler = null!");
        }
        p.a(getApplicationContext()).b();
        stopForeground(true);
        com.shoujiduoduo.base.a.a.a(b, "Service: onDestroy Finished!");
        super.onDestroy();
    }

    public int onStartCommand(Intent intent, int i2, int i3) {
        com.shoujiduoduo.base.a.a.a(b, "PlayerService onStartCommand.");
        startForeground(2001, r());
        if (intent != null) {
            String action = intent.getAction();
            if (action == null || !action.equals("com.shoujiduoduo.ringtone.PlayerService.togglePlayPause")) {
                if (!(action == null || !action.equals("com.shoujiduoduo.ringtone.PlayerService.toggleNext") || d == null)) {
                    if (this.g) {
                        k();
                    }
                    if (com.shoujiduoduo.util.f.y() && !w && e.c() > 1) {
                        this.y = (this.y + 1) % e.c();
                        a(e, this.y);
                    }
                }
            } else if (d != null) {
                if (this.m == 2) {
                    h();
                    com.umeng.analytics.b.b(this, "CONTINUOUS_PLAY_PAUSE_IN_NOTIF");
                } else if (this.m == 3) {
                    l();
                    com.umeng.analytics.b.b(this, "CONTINUOUS_PLAY_RESUME_IN_NOTIF");
                } else {
                    if (g() == 1) {
                        com.shoujiduoduo.base.a.a.a(b, "fuck, current song is full from notification bar");
                        com.shoujiduoduo.util.f.c("play from notification bar. currentSong is null!");
                    }
                    com.umeng.analytics.b.b(this, "CONTINUOUS_PLAY_PLAY_IN_NOTIF");
                }
            }
        }
        com.shoujiduoduo.base.a.a.a(b, "Service: onStartCommand Finished!");
        return super.onStartCommand(intent, i2, i3);
    }

    public void a(RingData ringData) {
        w wVar = new w();
        wVar.a(ringData);
        a(wVar, 0);
    }

    public void a(com.shoujiduoduo.base.bean.c cVar, int i2) {
        RingData ringData;
        y yVar;
        if (cVar != null && (ringData = (RingData) cVar.a(i2)) != null) {
            com.shoujiduoduo.base.a.a.a(b, "setSong in, listId:" + cVar.a() + ", index:" + i2);
            synchronized (this.n) {
                com.shoujiduoduo.base.a.a.a(b, "SetSong: enter, get mLock.");
                if (this.g) {
                    k();
                }
                if (e != cVar) {
                    if (e != null) {
                        x.a().b(com.shoujiduoduo.a.a.b.OBSERVER_PLAY_STATUS, new h(this, e.a(), this.y));
                    }
                    e = cVar;
                }
                this.y = i2;
                x.a().b(com.shoujiduoduo.a.a.b.OBSERVER_PLAY_STATUS, new i(this, e.a(), this.y));
                this.i = false;
                this.z.removeMessages(3101);
                try {
                    yVar = a(ringData, this.q);
                    this.q = false;
                } catch (Exception e2) {
                    e2.printStackTrace();
                    this.q = false;
                    yVar = null;
                } catch (Throwable th) {
                    this.q = false;
                    throw th;
                }
                if (yVar == null) {
                    a(6);
                    return;
                }
                d = yVar;
                if (cVar.a().equals("cmcc_cailing")) {
                    if (ringData.n.equals("") || ringData.n.length() != 32) {
                        f.a("该彩铃为移动赠送彩铃，暂时无法试听，可以正常设置为默认彩铃");
                        return;
                    }
                } else if (cVar.a().equals("cucc_cailing") && ringData.A.startsWith("91789000")) {
                    f.a("该彩铃暂时无法试听, 可以正常设置为默认彩铃");
                    return;
                }
                if (e(d) || !s()) {
                    com.shoujiduoduo.base.a.a.a(b, "play complete resource.");
                    this.f = f.a().d();
                } else {
                    com.shoujiduoduo.base.a.a.a(b, "play incomplete resource.");
                    this.f = f.a().c();
                }
                com.shoujiduoduo.base.a.a.a("TAG", "setSong: currentSong rid = " + d.c);
                if (f(d)) {
                    this.h = false;
                    g();
                } else {
                    this.h = true;
                    a(1);
                }
                com.shoujiduoduo.base.a.a.a(b, "SetSong: leave, release mLock.");
            }
        }
    }

    /* access modifiers changed from: private */
    public void b(com.shoujiduoduo.base.bean.c cVar, int i2) {
        this.q = true;
        a(cVar, i2);
    }

    private boolean e(y yVar) {
        return d.d == d.e && d.e > 0;
    }

    /* access modifiers changed from: private */
    public boolean f(y yVar) {
        boolean z2;
        boolean z3 = yVar.d == yVar.e;
        if (yVar.f <= 0 || yVar.d <= (yVar.f * 10) / 8) {
            z2 = false;
        } else {
            z2 = true;
        }
        if (yVar.g.equals("wav")) {
            return z3;
        }
        if ((!z2 || !s()) && !z3) {
            return false;
        }
        return true;
    }

    private boolean s() {
        return NativeAACDecoder.j() && NativeMP3Decoder.j();
    }

    public String d() {
        if (e != null) {
            return e.a();
        }
        return "";
    }

    private y a(RingData ringData, boolean z2) {
        boolean z3;
        boolean z4;
        boolean z5 = true;
        boolean z6 = false;
        this.p = false;
        if (ringData instanceof MakeRingData) {
            com.shoujiduoduo.base.a.a.a(b, "制作的铃声");
            MakeRingData makeRingData = (MakeRingData) ringData;
            File file = new File(makeRingData.m);
            if (file.exists()) {
                com.shoujiduoduo.base.a.a.a(b, "本地存在, path:" + makeRingData.m);
                String b2 = t.b(makeRingData.m);
                int a2 = u.a(makeRingData.g, 0);
                this.r = b2;
                this.p = true;
                y yVar = new y(makeRingData.e, makeRingData.f, a2, (int) file.length(), (int) file.length(), 128000, b2, "");
                yVar.e(makeRingData.m);
                return yVar;
            } else if (!makeRingData.g.equals("")) {
                this.r = "mp3";
                com.shoujiduoduo.base.a.a.a(b, "mCurFormat = mp3");
                return this.c.a(ringData, this.r);
            } else {
                com.shoujiduoduo.base.a.a.c(b, "制作的铃声，但本地没有，线上也没有，无法播放");
                return null;
            }
        } else if (!ringData.m.equals("")) {
            File file2 = new File(ringData.m);
            if (!file2.exists()) {
                return null;
            }
            com.shoujiduoduo.base.a.a.a(b, "本地存在, path:" + ringData.m);
            String b3 = t.b(ringData.m);
            this.r = b3;
            this.p = true;
            y yVar2 = new y(ringData.e, ringData.f, 0, (int) file2.length(), (int) file2.length(), 128000, b3, "");
            yVar2.e(ringData.m);
            return yVar2;
        } else {
            com.shoujiduoduo.base.a.a.a(b, "在线铃声");
            if (!ringData.n.equals("") && ringData.q == 0) {
                com.shoujiduoduo.base.a.a.a(b, "SetSong: current ring is cmcc cailing");
                z3 = false;
                z4 = true;
                z5 = false;
            } else if (!ringData.s.equals("") && ringData.v == 0) {
                com.shoujiduoduo.base.a.a.a(b, "SetSong: current ring is ctcc cailing");
                if (ringData.s.startsWith("81007")) {
                    com.shoujiduoduo.base.a.a.a(b, "ctcc diy cailing");
                    z3 = true;
                    z4 = false;
                } else {
                    z3 = true;
                    z4 = false;
                    z5 = false;
                }
            } else if (ringData.A.equals("") || !ringData.D.equals("") || ringData.j()) {
                z5 = false;
                z3 = false;
                z4 = false;
            } else {
                com.shoujiduoduo.base.a.a.a(b, "SetSong: current ring is cucc cailing");
                z3 = false;
                z4 = false;
                z5 = false;
                z6 = true;
            }
            if (z5) {
                this.r = "wav";
            } else if (z4 || z3 || z6) {
                this.r = "mp3";
            } else if (z2) {
                this.r = "mp3";
            } else if (ringData.j()) {
                this.r = "aac";
            } else if (ringData.i() || !ringData.D.equals("")) {
                this.r = "mp3";
            } else {
                this.r = "aac";
            }
            com.shoujiduoduo.base.a.a.a(b, "mCurFormat = " + this.r);
            return this.c.a(ringData, this.r);
        }
    }

    public void e() {
        synchronized (this.n) {
            com.shoujiduoduo.base.a.a.a(b, "reset: enter: get mLock.");
            if (this.g) {
                k();
            }
            d = null;
            this.y = -1;
            this.i = false;
            com.shoujiduoduo.base.a.a.a(b, "reset: leave, release mLock.");
        }
    }

    public int f() {
        int i2;
        synchronized (this.n) {
            if (d != null) {
                i2 = d.c;
            } else {
                i2 = -1;
            }
        }
        return i2;
    }

    public int g() {
        com.shoujiduoduo.base.a.a.a(b, "PlayerService: play!");
        if (this.s) {
            this.t = true;
            com.shoujiduoduo.base.a.a.e(b, "play failed, mCallIn == true");
            return 0;
        } else if (d == null) {
            com.shoujiduoduo.util.f.c("currentSong is null when Play!");
            com.shoujiduoduo.base.a.a.e(b, "play failed, currentSong == null");
            return 1;
        } else if (d.l() == null) {
            com.shoujiduoduo.util.f.c("currentSong.getSongPath is null when Play!");
            com.shoujiduoduo.base.a.a.e(b, "play failed, currentSong.getSongPath() == null");
            return 2;
        } else {
            if (e(d) || !s()) {
                com.shoujiduoduo.base.a.a.b(b, "download finished data or local data");
                this.f = f.a().d();
                if (this.f.a(d.l()) != 0) {
                    com.shoujiduoduo.base.a.a.e(b, "system player play failed!");
                    if (this.p || this.r.equals("mp3")) {
                        com.shoujiduoduo.base.a.a.c(b, "duoduo player play failed! final fail!!");
                        a(6);
                    } else {
                        com.shoujiduoduo.base.a.a.b(b, "retry mp3 format");
                        this.z.sendEmptyMessage(3200);
                    }
                    return 4;
                }
            } else {
                com.shoujiduoduo.base.a.a.b(b, "download unfinished data");
                this.f = f.a().c();
                if (this.f.a(d.l()) != 0) {
                    com.shoujiduoduo.base.a.a.e(b, "duoduo player play failed");
                    if (this.p || this.r.equals("mp3")) {
                        com.shoujiduoduo.base.a.a.c(b, "duoduo player play failed! final fail!!");
                        a(6);
                    } else {
                        com.shoujiduoduo.base.a.a.b(b, "retry mp3 format");
                        this.z.sendEmptyMessage(3200);
                    }
                    return 4;
                }
            }
            StringBuilder sb = new StringBuilder();
            sb.append("&rid=").append(d.c).append("&listid=").append(e.a()).append("&listtype=").append(e.b().toString());
            com.shoujiduoduo.util.w.a(Constants.PLAYMUSIC, cn.banshenggua.aichang.player.PlayerService.ACTION_PLAY, sb.toString());
            this.g = true;
            this.j = this.f.f();
            this.k = this.f.h();
            com.shoujiduoduo.base.a.a.a(b, "PlayerService: play success: duration = " + this.j + ", bitrate = " + this.k);
            this.l = new Timer();
            this.l.schedule(new j(this), 1000, 1000);
            a(2);
            return 0;
        }
    }

    public boolean h() {
        this.f.b();
        if (this.l != null) {
            this.l.cancel();
            this.l = null;
        }
        a(3);
        return true;
    }

    public boolean i() {
        this.f.b();
        if (this.l != null) {
            this.l.cancel();
            this.l = null;
        }
        a(1);
        return true;
    }

    public boolean j() {
        return this.g;
    }

    public boolean k() {
        if (this.f != null) {
            this.f.a();
        }
        if (this.l != null) {
            this.l.cancel();
            this.l = null;
        }
        this.g = false;
        this.t = false;
        a(5);
        return true;
    }

    public boolean l() {
        if (this.s) {
            this.t = true;
        } else {
            this.f.c();
            this.l = new Timer();
            this.l.schedule(new k(this), 1000, 1000);
            a(2);
        }
        return true;
    }

    public void a(y yVar) {
        if (d != null && yVar != null && yVar.c == d.c) {
            synchronized (this.n) {
                if (this.z != null) {
                    this.z.sendMessage(this.z.obtainMessage(SimpleLiveRoomActivity.DOWNLOAD_COMP, yVar));
                }
            }
        }
    }

    public void a(y yVar, int i2) {
        a aVar = new a(this, null);
        aVar.f828a = yVar;
        aVar.b = i2;
        this.f827a = i2;
        synchronized (this.n) {
            if (this.z != null) {
                this.z.sendMessage(this.z.obtainMessage(3005, aVar));
            }
        }
    }

    public void b(y yVar) {
        com.shoujiduoduo.base.a.a.a(b, "PlayerService: onDownloadFinish!");
        synchronized (this.n) {
            if (this.z != null) {
                this.z.sendMessage(this.z.obtainMessage(3003, yVar));
            }
        }
    }

    public void c(y yVar) {
        if (d != null && yVar != null && yVar.c == d.c) {
            synchronized (this.n) {
                if (this.z != null) {
                    this.z.sendMessage(this.z.obtainMessage(SimpleLiveRoomActivity.DOWNLOAD_PROCESS, yVar));
                }
            }
        }
    }

    public void d(y yVar) {
        synchronized (this.n) {
            if (this.z != null) {
                this.z.sendMessage(this.z.obtainMessage(3004, yVar));
            }
        }
    }

    private class a {

        /* renamed from: a  reason: collision with root package name */
        public y f828a;
        public int b;

        private a() {
        }

        /* synthetic */ a(PlayerService playerService, g gVar) {
            this();
        }
    }

    public void a(c cVar) {
        synchronized (this.n) {
            if (this.z != null) {
                Message obtainMessage = this.z.obtainMessage(3101, null);
                com.shoujiduoduo.base.a.a.a(b, "PlayerService: send MESSAGE_PLAY_COMPLETE");
                this.z.sendMessage(obtainMessage);
            }
        }
    }

    private class c extends PhoneStateListener {
        private c() {
        }

        /* synthetic */ c(PlayerService playerService, g gVar) {
            this();
        }

        public void onCallStateChanged(int i, String str) {
            super.onCallStateChanged(i, str);
            switch (i) {
                case 0:
                    synchronized (PlayerService.this.n) {
                        if (PlayerService.this.z != null) {
                            PlayerService.this.z.sendEmptyMessage(3201);
                        }
                    }
                    return;
                case 1:
                case 2:
                    synchronized (PlayerService.this.n) {
                        if (PlayerService.this.z != null) {
                            PlayerService.this.z.sendEmptyMessage(3202);
                        }
                    }
                    return;
                default:
                    return;
            }
        }
    }
}
