package com.shoujiduoduo.util;

import com.shoujiduoduo.base.a.a;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/* compiled from: HttpRequest */
final class y implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f1718a;
    final /* synthetic */ String b;
    final /* synthetic */ String c;

    y(String str, String str2, String str3) {
        this.f1718a = str;
        this.b = str2;
        this.c = str3;
    }

    public void run() {
        StringBuilder sb = new StringBuilder();
        String str = "";
        try {
            str = URLEncoder.encode(this.f1718a, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        sb.append(w.i).append("&type=keyvalue").append("&key=").append("ringDD_ar:" + this.b).append("&param=").append(str).append(this.c);
        String str2 = "http://log.shoujiduoduo.com/log.php?" + sb.toString();
        a.a(w.b, str2);
        bj.a(str2);
    }
}
