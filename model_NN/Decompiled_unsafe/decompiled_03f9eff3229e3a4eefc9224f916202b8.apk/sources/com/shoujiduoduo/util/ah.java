package com.shoujiduoduo.util;

import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.ringtone.RingDDApp;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.HashSet;
import java.util.Set;

/* compiled from: NativeLibLoadHelper */
public final class ah {

    /* renamed from: a  reason: collision with root package name */
    private static String f1560a = "NativeLibLoadHelper";
    private static Set<String> b = new HashSet();

    public static synchronized boolean a(String str) {
        boolean a2;
        synchronized (ah.class) {
            a2 = a(str, false, new StringBuilder());
            if (!a2) {
            }
        }
        return a2;
    }

    public static boolean a(String str, boolean z, StringBuilder sb) {
        boolean z2 = true;
        if (!b.contains(str)) {
            if (!a(str, sb) && !b(str, sb) && !c(str, sb) && !d(str, sb)) {
                z2 = false;
            }
            if (z2) {
                b.add(str);
            }
            a.a(f1560a, "load lib " + str + " res:" + z2);
        }
        return z2;
    }

    private static boolean a(String str, StringBuilder sb) {
        try {
            System.loadLibrary(str);
            return true;
        } catch (Throwable th) {
            return false;
        }
    }

    private static boolean b(String str, StringBuilder sb) {
        String str2 = l.a(5) + "lib" + str + ".so";
        if (!t.f(str2)) {
            sb.append("\nloadFullPath failed,fullpath not exist:").append(str2);
            return false;
        }
        try {
            System.load(str2);
            return true;
        } catch (Throwable th) {
            return false;
        }
    }

    private static boolean c(String str, StringBuilder sb) {
        BufferedInputStream bufferedInputStream;
        FileOutputStream fileOutputStream;
        try {
            String str2 = RingDDApp.b().getFilesDir().getAbsolutePath() + File.separator + ("lib" + str + ".so");
            byte[] bArr = new byte[4096];
            bufferedInputStream = new BufferedInputStream(RingDDApp.b().getAssets().open("libs/lib" + str));
            fileOutputStream = new FileOutputStream(str2);
            while (true) {
                int read = bufferedInputStream.read(bArr);
                if (read != -1) {
                    fileOutputStream.write(bArr, 0, read);
                } else {
                    bufferedInputStream.close();
                    fileOutputStream.close();
                    sb.append("\nassert read success");
                    System.load(str2);
                    return true;
                }
            }
        } catch (Throwable th) {
            sb.append("\nload from assert failed:\n").append(th);
            return false;
        }
    }

    private static boolean d(String str, StringBuilder sb) {
        BufferedInputStream bufferedInputStream;
        FileOutputStream fileOutputStream;
        try {
            String str2 = l.a(2) + ("lib" + str + ".so");
            byte[] bArr = new byte[4096];
            bufferedInputStream = new BufferedInputStream(RingDDApp.b().getAssets().open("libs/lib" + str));
            fileOutputStream = new FileOutputStream(str2);
            while (true) {
                int read = bufferedInputStream.read(bArr);
                if (read != -1) {
                    fileOutputStream.write(bArr, 0, read);
                } else {
                    bufferedInputStream.close();
                    fileOutputStream.close();
                    sb.append("\nsdcard read success");
                    System.load(str2);
                    return true;
                }
            }
        } catch (Throwable th) {
            sb.append("\nload from sdcard failed:\n").append(th);
            return false;
        }
    }
}
