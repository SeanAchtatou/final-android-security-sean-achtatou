package com.shoujiduoduo.util;

import java.util.ArrayList;
import java.util.HashMap;
import org.w3c.dom.NamedNodeMap;

/* compiled from: ServerConfig */
public class am {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final String f1564a = am.class.getSimpleName();
    /* access modifiers changed from: private */
    public static String c = l.b(0);
    private static boolean d = true;
    private static HashMap<String, String> f = new HashMap<>();
    private ArrayList<b> b;
    /* access modifiers changed from: private */
    public boolean e;

    /* compiled from: ServerConfig */
    public enum a {
        DOMOB,
        GOOGLE,
        DUODUO,
        BAIDU,
        TENCENT,
        TAOBAO
    }

    /* compiled from: ServerConfig */
    private static class c {

        /* renamed from: a  reason: collision with root package name */
        public static am f1568a = new am(null);
    }

    /* synthetic */ am(an anVar) {
        this();
    }

    private am() {
        this.e = false;
        this.b = new ArrayList<>();
        if (!i()) {
            com.shoujiduoduo.base.a.a.a(f1564a, "load server cache failed, set default");
            g();
            return;
        }
        com.shoujiduoduo.base.a.a.a(f1564a, "load server cache success");
    }

    public static am a() {
        return c.f1568a;
    }

    private void g() {
        synchronized (f) {
            f.put("update_version", "");
            f.put("update_url", "");
            f.put("update_type", "");
            f.put("ad_cancel_download", "false");
            f.put("ad_switch_time", "10000");
            f.put("ad_install_immediately", "true");
            f.put("ad_wall_default", "ebusiness");
            f.put("duoduo_ad_duration", "0");
            f.put("domob_ad_duration", "0");
            f.put("baidu_ad_duration", "3000");
            f.put("tencent_ad_duration", "30");
            f.put("taobao_ad_duration", "0");
            f.put("adturbo_enable", "1");
            f.put("taobao_wall_type", "sdk");
            f.put("share_url", "http://main.shoujiduoduo.com/share/index.php");
            f.put("feed_ad_type", "baidu");
            f.put("feed_ad_enable", "true");
            f.put("feed_ad_channel", "all");
            f.put("bcs_domain_name", "bj.bcebos.com");
            f.put("mi_push_enable", "true");
            f.put("ctcc_enable", "true");
            f.put("dns_retry", "3");
            f.put("dns_timeout", "8000");
            f.put("dns_value1", "cdnringhlt.shoujiduoduo.com");
            f.put("dns_value2", "cdnringfw.shoujiduoduo.com");
            f.put("dns_check1", "http://cdnringhlt.shoujiduoduo.com/ringres/verify.dat");
            f.put("dns_check2", "http://cdnringfw.shoujiduoduo.com/ringres/verify.dat");
            f.put("dns_switchcondition", "network");
            f.put("dns_duoduo_check", "http://www.shoujiduoduo.com/ringv1/verify.dat");
            f.put("aichang_enable", "false");
            f.put("ad_delay_time", "0");
            f.put("cm_sunshine_sdk_enable", "true");
            h();
        }
    }

    private void h() {
        com.shoujiduoduo.base.a.a.a(f1564a, "initDefaultAdOrder");
        ArrayList<b> arrayList = new ArrayList<>();
        arrayList.add(new b(0, "3000", a.BAIDU));
        arrayList.add(new b(1, "60", a.TENCENT));
        this.b = arrayList;
    }

    public void b() {
        com.shoujiduoduo.base.a.a.a(f1564a, "begin loadServerConfig");
        this.e = false;
        j();
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:219:?, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:221:?, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:222:?, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:223:?, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:224:?, code lost:
        return false;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean i() {
        /*
            r38 = this;
            java.lang.String r3 = com.shoujiduoduo.util.am.f1564a
            java.lang.String r4 = "begin loadCache"
            com.shoujiduoduo.base.a.a.a(r3, r4)
            java.lang.String r31 = ""
            java.lang.String r30 = ""
            java.lang.String r29 = ""
            java.lang.String r28 = ""
            java.lang.String r27 = ""
            java.lang.String r26 = ""
            java.lang.String r25 = ""
            java.lang.String r24 = ""
            java.lang.String r23 = ""
            java.lang.String r22 = ""
            java.lang.String r21 = ""
            java.lang.String r20 = ""
            java.lang.String r19 = ""
            java.lang.String r18 = ""
            java.lang.String r17 = ""
            java.lang.String r16 = ""
            java.lang.String r15 = ""
            java.lang.String r14 = ""
            java.lang.String r13 = ""
            java.lang.String r12 = ""
            java.lang.String r11 = ""
            java.lang.String r10 = ""
            java.lang.String r9 = ""
            java.lang.String r8 = ""
            java.lang.String r7 = ""
            java.lang.String r6 = ""
            java.lang.String r5 = ""
            java.lang.String r4 = ""
            java.util.HashMap<java.lang.String, java.lang.String> r32 = com.shoujiduoduo.util.am.f
            monitor-enter(r32)
            java.util.ArrayList r3 = new java.util.ArrayList     // Catch:{ all -> 0x0127 }
            r3.<init>()     // Catch:{ all -> 0x0127 }
            r0 = r38
            r0.b = r3     // Catch:{ all -> 0x0127 }
            javax.xml.parsers.DocumentBuilderFactory r3 = javax.xml.parsers.DocumentBuilderFactory.newInstance()     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            javax.xml.parsers.DocumentBuilder r3 = r3.newDocumentBuilder()     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            java.io.FileInputStream r33 = new java.io.FileInputStream     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            java.lang.String r34 = com.shoujiduoduo.util.am.c     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            r33.<init>(r34)     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            r0 = r33
            org.w3c.dom.Document r3 = r3.parse(r0)     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            org.w3c.dom.Element r3 = r3.getDocumentElement()     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            if (r3 != 0) goto L_0x0069
            r3 = 0
            monitor-exit(r32)     // Catch:{ all -> 0x0127 }
        L_0x0068:
            return r3
        L_0x0069:
            java.lang.String r33 = "item"
            r0 = r33
            org.w3c.dom.NodeList r33 = r3.getElementsByTagName(r0)     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            r3 = 0
            r37 = r3
            r3 = r4
            r4 = r5
            r5 = r6
            r6 = r7
            r7 = r8
            r8 = r9
            r9 = r10
            r10 = r11
            r11 = r12
            r12 = r13
            r13 = r14
            r14 = r15
            r15 = r16
            r16 = r17
            r17 = r18
            r18 = r19
            r19 = r20
            r20 = r21
            r21 = r22
            r22 = r23
            r23 = r24
            r24 = r25
            r25 = r26
            r26 = r27
            r27 = r28
            r28 = r29
            r29 = r30
            r30 = r31
            r31 = r37
        L_0x00a2:
            int r34 = r33.getLength()     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            r0 = r31
            r1 = r34
            if (r0 >= r1) goto L_0x02d2
            r0 = r33
            r1 = r31
            org.w3c.dom.Node r34 = r0.item(r1)     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            org.w3c.dom.NamedNodeMap r34 = r34.getAttributes()     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            java.lang.String r35 = "name"
            java.lang.String r35 = com.shoujiduoduo.util.f.a(r34, r35)     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            java.lang.String r36 = "update"
            boolean r36 = r35.equalsIgnoreCase(r36)     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            if (r36 == 0) goto L_0x00e7
            java.lang.String r28 = "ver"
            r0 = r34
            r1 = r28
            java.lang.String r30 = com.shoujiduoduo.util.f.a(r0, r1)     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            java.lang.String r28 = "url"
            r0 = r34
            r1 = r28
            java.lang.String r29 = com.shoujiduoduo.util.f.a(r0, r1)     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            java.lang.String r28 = "type"
            r0 = r34
            r1 = r28
            java.lang.String r28 = com.shoujiduoduo.util.f.a(r0, r1)     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
        L_0x00e4:
            int r31 = r31 + 1
            goto L_0x00a2
        L_0x00e7:
            java.lang.String r36 = "ad"
            boolean r36 = r35.equalsIgnoreCase(r36)     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            if (r36 == 0) goto L_0x010e
            java.lang.String r25 = "cancel_enable"
            r0 = r34
            r1 = r25
            java.lang.String r27 = com.shoujiduoduo.util.f.a(r0, r1)     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            java.lang.String r25 = "switch_time"
            r0 = r34
            r1 = r25
            java.lang.String r26 = com.shoujiduoduo.util.f.a(r0, r1)     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            java.lang.String r25 = "install_immediately"
            r0 = r34
            r1 = r25
            java.lang.String r25 = com.shoujiduoduo.util.f.a(r0, r1)     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            goto L_0x00e4
        L_0x010e:
            java.lang.String r36 = "duoduo_ad"
            boolean r36 = r35.equalsIgnoreCase(r36)     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            if (r36 == 0) goto L_0x012a
            com.shoujiduoduo.util.am$a r35 = com.shoujiduoduo.util.am.a.DUODUO     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            r0 = r38
            r1 = r34
            r2 = r35
            r0.a(r1, r2)     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            goto L_0x00e4
        L_0x0122:
            r3 = move-exception
            r3 = 0
            monitor-exit(r32)     // Catch:{ all -> 0x0127 }
            goto L_0x0068
        L_0x0127:
            r3 = move-exception
            monitor-exit(r32)     // Catch:{ all -> 0x0127 }
            throw r3
        L_0x012a:
            java.lang.String r36 = "domob_ad"
            boolean r36 = r35.equalsIgnoreCase(r36)     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            if (r36 == 0) goto L_0x0143
            com.shoujiduoduo.util.am$a r35 = com.shoujiduoduo.util.am.a.DOMOB     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            r0 = r38
            r1 = r34
            r2 = r35
            r0.a(r1, r2)     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            goto L_0x00e4
        L_0x013e:
            r3 = move-exception
            r3 = 0
            monitor-exit(r32)     // Catch:{ all -> 0x0127 }
            goto L_0x0068
        L_0x0143:
            java.lang.String r36 = "baidu_ad"
            boolean r36 = r35.equalsIgnoreCase(r36)     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            if (r36 == 0) goto L_0x015c
            com.shoujiduoduo.util.am$a r35 = com.shoujiduoduo.util.am.a.BAIDU     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            r0 = r38
            r1 = r34
            r2 = r35
            r0.a(r1, r2)     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            goto L_0x00e4
        L_0x0157:
            r3 = move-exception
            r3 = 0
            monitor-exit(r32)     // Catch:{ all -> 0x0127 }
            goto L_0x0068
        L_0x015c:
            java.lang.String r36 = "tencent_ad"
            boolean r36 = r35.equalsIgnoreCase(r36)     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            if (r36 == 0) goto L_0x0176
            com.shoujiduoduo.util.am$a r35 = com.shoujiduoduo.util.am.a.TENCENT     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            r0 = r38
            r1 = r34
            r2 = r35
            r0.a(r1, r2)     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            goto L_0x00e4
        L_0x0171:
            r3 = move-exception
            r3 = 0
            monitor-exit(r32)     // Catch:{ all -> 0x0127 }
            goto L_0x0068
        L_0x0176:
            java.lang.String r36 = "taobao_ad"
            boolean r36 = r35.equalsIgnoreCase(r36)     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            if (r36 == 0) goto L_0x0190
            com.shoujiduoduo.util.am$a r35 = com.shoujiduoduo.util.am.a.TAOBAO     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            r0 = r38
            r1 = r34
            r2 = r35
            r0.a(r1, r2)     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            goto L_0x00e4
        L_0x018b:
            r3 = move-exception
            r3 = 0
            monitor-exit(r32)     // Catch:{ all -> 0x0127 }
            goto L_0x0068
        L_0x0190:
            java.lang.String r36 = "ad_wall"
            boolean r36 = r35.equalsIgnoreCase(r36)     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            if (r36 == 0) goto L_0x01a4
            java.lang.String r24 = "default"
            r0 = r34
            r1 = r24
            java.lang.String r24 = com.shoujiduoduo.util.f.a(r0, r1)     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            goto L_0x00e4
        L_0x01a4:
            java.lang.String r36 = "game_url1"
            boolean r36 = r35.equalsIgnoreCase(r36)     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            if (r36 == 0) goto L_0x01b8
            java.lang.String r23 = "url"
            r0 = r34
            r1 = r23
            java.lang.String r23 = com.shoujiduoduo.util.f.a(r0, r1)     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            goto L_0x00e4
        L_0x01b8:
            java.lang.String r36 = "taobao_wall_v2"
            boolean r36 = r35.equalsIgnoreCase(r36)     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            if (r36 == 0) goto L_0x01cc
            java.lang.String r22 = "type"
            r0 = r34
            r1 = r22
            java.lang.String r22 = com.shoujiduoduo.util.f.a(r0, r1)     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            goto L_0x00e4
        L_0x01cc:
            java.lang.String r36 = "bcs_domain_name"
            boolean r36 = r35.equalsIgnoreCase(r36)     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            if (r36 == 0) goto L_0x01e0
            java.lang.String r21 = "value"
            r0 = r34
            r1 = r21
            java.lang.String r21 = com.shoujiduoduo.util.f.a(r0, r1)     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            goto L_0x00e4
        L_0x01e0:
            java.lang.String r36 = "mi_push"
            boolean r36 = r35.equalsIgnoreCase(r36)     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            if (r36 == 0) goto L_0x01f4
            java.lang.String r20 = "enable"
            r0 = r34
            r1 = r20
            java.lang.String r20 = com.shoujiduoduo.util.f.a(r0, r1)     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            goto L_0x00e4
        L_0x01f4:
            java.lang.String r36 = "adturbo"
            boolean r36 = r35.equalsIgnoreCase(r36)     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            if (r36 == 0) goto L_0x0208
            java.lang.String r19 = "enable"
            r0 = r34
            r1 = r19
            java.lang.String r19 = com.shoujiduoduo.util.f.a(r0, r1)     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            goto L_0x00e4
        L_0x0208:
            java.lang.String r36 = "ctcc"
            boolean r36 = r35.equalsIgnoreCase(r36)     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            if (r36 == 0) goto L_0x021c
            java.lang.String r18 = "enable"
            r0 = r34
            r1 = r18
            java.lang.String r18 = com.shoujiduoduo.util.f.a(r0, r1)     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            goto L_0x00e4
        L_0x021c:
            java.lang.String r36 = "share"
            boolean r36 = r35.equalsIgnoreCase(r36)     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            if (r36 == 0) goto L_0x0230
            java.lang.String r17 = "url"
            r0 = r34
            r1 = r17
            java.lang.String r17 = com.shoujiduoduo.util.f.a(r0, r1)     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            goto L_0x00e4
        L_0x0230:
            java.lang.String r36 = "feed_ad"
            boolean r36 = r35.equalsIgnoreCase(r36)     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            if (r36 == 0) goto L_0x0252
            java.lang.String r14 = "type"
            r0 = r34
            java.lang.String r16 = com.shoujiduoduo.util.f.a(r0, r14)     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            java.lang.String r14 = "enable"
            r0 = r34
            java.lang.String r15 = com.shoujiduoduo.util.f.a(r0, r14)     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            java.lang.String r14 = "channel"
            r0 = r34
            java.lang.String r14 = com.shoujiduoduo.util.f.a(r0, r14)     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            goto L_0x00e4
        L_0x0252:
            java.lang.String r36 = "dns"
            boolean r36 = r35.equalsIgnoreCase(r36)     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            if (r36 == 0) goto L_0x029c
            java.lang.String r6 = "retry"
            r0 = r34
            java.lang.String r12 = com.shoujiduoduo.util.f.a(r0, r6)     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            java.lang.String r6 = "timeout"
            r0 = r34
            java.lang.String r13 = com.shoujiduoduo.util.f.a(r0, r6)     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            java.lang.String r6 = "value1"
            r0 = r34
            java.lang.String r11 = com.shoujiduoduo.util.f.a(r0, r6)     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            java.lang.String r6 = "value2"
            r0 = r34
            java.lang.String r10 = com.shoujiduoduo.util.f.a(r0, r6)     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            java.lang.String r6 = "check1"
            r0 = r34
            java.lang.String r9 = com.shoujiduoduo.util.f.a(r0, r6)     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            java.lang.String r6 = "check2"
            r0 = r34
            java.lang.String r8 = com.shoujiduoduo.util.f.a(r0, r6)     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            java.lang.String r6 = "duoduocheck"
            r0 = r34
            java.lang.String r6 = com.shoujiduoduo.util.f.a(r0, r6)     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            java.lang.String r7 = "switchcondition"
            r0 = r34
            java.lang.String r7 = com.shoujiduoduo.util.f.a(r0, r7)     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            goto L_0x00e4
        L_0x029c:
            java.lang.String r36 = "aichang"
            boolean r36 = r35.equalsIgnoreCase(r36)     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            if (r36 == 0) goto L_0x02ae
            java.lang.String r5 = "enable"
            r0 = r34
            java.lang.String r5 = com.shoujiduoduo.util.f.a(r0, r5)     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            goto L_0x00e4
        L_0x02ae:
            java.lang.String r36 = "ad_delay"
            boolean r36 = r35.equalsIgnoreCase(r36)     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            if (r36 == 0) goto L_0x02c0
            java.lang.String r4 = "time"
            r0 = r34
            java.lang.String r4 = com.shoujiduoduo.util.f.a(r0, r4)     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            goto L_0x00e4
        L_0x02c0:
            java.lang.String r36 = "cm_sunshine_sdk"
            boolean r35 = r35.equalsIgnoreCase(r36)     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            if (r35 == 0) goto L_0x00e4
            java.lang.String r3 = "enable"
            r0 = r34
            java.lang.String r3 = com.shoujiduoduo.util.f.a(r0, r3)     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            goto L_0x00e4
        L_0x02d2:
            java.lang.String r31 = com.shoujiduoduo.util.am.f1564a     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            java.lang.StringBuilder r33 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            r33.<init>()     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            java.lang.String r34 = "update ver = "
            java.lang.StringBuilder r33 = r33.append(r34)     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            r0 = r33
            r1 = r30
            java.lang.StringBuilder r33 = r0.append(r1)     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            java.lang.String r34 = "url = "
            java.lang.StringBuilder r33 = r33.append(r34)     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            r0 = r33
            r1 = r29
            java.lang.StringBuilder r33 = r0.append(r1)     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            java.lang.String r34 = "type = "
            java.lang.StringBuilder r33 = r33.append(r34)     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            r0 = r33
            r1 = r28
            java.lang.StringBuilder r33 = r0.append(r1)     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            java.lang.String r33 = r33.toString()     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            r0 = r31
            r1 = r33
            com.shoujiduoduo.base.a.a.a(r0, r1)     // Catch:{ IOException -> 0x0122, SAXException -> 0x013e, ParserConfigurationException -> 0x0157, DOMException -> 0x0171, Exception -> 0x018b, all -> 0x05b8 }
            java.util.HashMap<java.lang.String, java.lang.String> r31 = com.shoujiduoduo.util.am.f     // Catch:{ all -> 0x0127 }
            java.lang.String r33 = "update_version"
            r0 = r31
            r1 = r33
            r2 = r30
            r0.put(r1, r2)     // Catch:{ all -> 0x0127 }
            java.util.HashMap<java.lang.String, java.lang.String> r30 = com.shoujiduoduo.util.am.f     // Catch:{ all -> 0x0127 }
            java.lang.String r31 = "update_url"
            r0 = r30
            r1 = r31
            r2 = r29
            r0.put(r1, r2)     // Catch:{ all -> 0x0127 }
            java.util.HashMap<java.lang.String, java.lang.String> r29 = com.shoujiduoduo.util.am.f     // Catch:{ all -> 0x0127 }
            java.lang.String r30 = "update_type"
            r0 = r29
            r1 = r30
            r2 = r28
            r0.put(r1, r2)     // Catch:{ all -> 0x0127 }
            java.util.HashMap<java.lang.String, java.lang.String> r28 = com.shoujiduoduo.util.am.f     // Catch:{ all -> 0x0127 }
            java.lang.String r29 = "ad_cancel_download"
            java.lang.String r30 = ""
            r0 = r27
            r1 = r30
            boolean r30 = r0.equals(r1)     // Catch:{ all -> 0x0127 }
            if (r30 == 0) goto L_0x0347
            java.lang.String r27 = "false"
        L_0x0347:
            r0 = r28
            r1 = r29
            r2 = r27
            r0.put(r1, r2)     // Catch:{ all -> 0x0127 }
            java.util.HashMap<java.lang.String, java.lang.String> r27 = com.shoujiduoduo.util.am.f     // Catch:{ all -> 0x0127 }
            java.lang.String r28 = "ad_switch_time"
            java.lang.String r29 = ""
            r0 = r26
            r1 = r29
            boolean r29 = r0.equals(r1)     // Catch:{ all -> 0x0127 }
            if (r29 == 0) goto L_0x0362
            java.lang.String r26 = "10000"
        L_0x0362:
            r0 = r27
            r1 = r28
            r2 = r26
            r0.put(r1, r2)     // Catch:{ all -> 0x0127 }
            java.util.HashMap<java.lang.String, java.lang.String> r26 = com.shoujiduoduo.util.am.f     // Catch:{ all -> 0x0127 }
            java.lang.String r27 = "ad_install_immediately"
            java.lang.String r28 = ""
            r0 = r25
            r1 = r28
            boolean r28 = r0.equals(r1)     // Catch:{ all -> 0x0127 }
            if (r28 == 0) goto L_0x037d
            java.lang.String r25 = "true"
        L_0x037d:
            r0 = r26
            r1 = r27
            r2 = r25
            r0.put(r1, r2)     // Catch:{ all -> 0x0127 }
            java.util.HashMap<java.lang.String, java.lang.String> r25 = com.shoujiduoduo.util.am.f     // Catch:{ all -> 0x0127 }
            java.lang.String r26 = "ad_wall_default"
            java.lang.String r27 = ""
            r0 = r24
            r1 = r27
            boolean r27 = r0.equals(r1)     // Catch:{ all -> 0x0127 }
            if (r27 == 0) goto L_0x0398
            java.lang.String r24 = "ebusiness"
        L_0x0398:
            r0 = r25
            r1 = r26
            r2 = r24
            r0.put(r1, r2)     // Catch:{ all -> 0x0127 }
            java.util.HashMap<java.lang.String, java.lang.String> r24 = com.shoujiduoduo.util.am.f     // Catch:{ all -> 0x0127 }
            java.lang.String r25 = "game_url1"
            java.lang.String r26 = ""
            r0 = r23
            r1 = r26
            boolean r26 = r0.equals(r1)     // Catch:{ all -> 0x0127 }
            if (r26 == 0) goto L_0x03b3
            java.lang.String r23 = ""
        L_0x03b3:
            r0 = r24
            r1 = r25
            r2 = r23
            r0.put(r1, r2)     // Catch:{ all -> 0x0127 }
            java.util.HashMap<java.lang.String, java.lang.String> r23 = com.shoujiduoduo.util.am.f     // Catch:{ all -> 0x0127 }
            java.lang.String r24 = "bcs_domain_name"
            java.lang.String r25 = ""
            r0 = r21
            r1 = r25
            boolean r25 = r0.equals(r1)     // Catch:{ all -> 0x0127 }
            if (r25 == 0) goto L_0x03ce
            java.lang.String r21 = "bj.bcebos.com"
        L_0x03ce:
            r0 = r23
            r1 = r24
            r2 = r21
            r0.put(r1, r2)     // Catch:{ all -> 0x0127 }
            java.util.HashMap<java.lang.String, java.lang.String> r21 = com.shoujiduoduo.util.am.f     // Catch:{ all -> 0x0127 }
            java.lang.String r23 = "taobao_wall_type"
            java.lang.String r24 = ""
            r0 = r22
            r1 = r24
            boolean r24 = r0.equals(r1)     // Catch:{ all -> 0x0127 }
            if (r24 == 0) goto L_0x03e9
            java.lang.String r22 = "sdk"
        L_0x03e9:
            r0 = r21
            r1 = r23
            r2 = r22
            r0.put(r1, r2)     // Catch:{ all -> 0x0127 }
            java.util.HashMap<java.lang.String, java.lang.String> r21 = com.shoujiduoduo.util.am.f     // Catch:{ all -> 0x0127 }
            java.lang.String r22 = "mi_push_enable"
            java.lang.String r23 = ""
            r0 = r20
            r1 = r23
            boolean r23 = r0.equals(r1)     // Catch:{ all -> 0x0127 }
            if (r23 == 0) goto L_0x0404
            java.lang.String r20 = "true"
        L_0x0404:
            r0 = r21
            r1 = r22
            r2 = r20
            r0.put(r1, r2)     // Catch:{ all -> 0x0127 }
            java.util.HashMap<java.lang.String, java.lang.String> r20 = com.shoujiduoduo.util.am.f     // Catch:{ all -> 0x0127 }
            java.lang.String r21 = "adturbo_enable"
            java.lang.String r22 = ""
            r0 = r19
            r1 = r22
            boolean r22 = r0.equals(r1)     // Catch:{ all -> 0x0127 }
            if (r22 == 0) goto L_0x041f
            java.lang.String r19 = "1"
        L_0x041f:
            r0 = r20
            r1 = r21
            r2 = r19
            r0.put(r1, r2)     // Catch:{ all -> 0x0127 }
            java.util.HashMap<java.lang.String, java.lang.String> r19 = com.shoujiduoduo.util.am.f     // Catch:{ all -> 0x0127 }
            java.lang.String r20 = "ctcc_enable"
            java.lang.String r21 = ""
            r0 = r18
            r1 = r21
            boolean r21 = r0.equals(r1)     // Catch:{ all -> 0x0127 }
            if (r21 == 0) goto L_0x043a
            java.lang.String r18 = "true"
        L_0x043a:
            r0 = r19
            r1 = r20
            r2 = r18
            r0.put(r1, r2)     // Catch:{ all -> 0x0127 }
            java.util.HashMap<java.lang.String, java.lang.String> r18 = com.shoujiduoduo.util.am.f     // Catch:{ all -> 0x0127 }
            java.lang.String r19 = "share_url"
            java.lang.String r20 = ""
            r0 = r17
            r1 = r20
            boolean r20 = r0.equals(r1)     // Catch:{ all -> 0x0127 }
            if (r20 == 0) goto L_0x0455
            java.lang.String r17 = "http://main.shoujiduoduo.com/share/index.php"
        L_0x0455:
            r0 = r18
            r1 = r19
            r2 = r17
            r0.put(r1, r2)     // Catch:{ all -> 0x0127 }
            java.util.HashMap<java.lang.String, java.lang.String> r17 = com.shoujiduoduo.util.am.f     // Catch:{ all -> 0x0127 }
            java.lang.String r18 = "feed_ad_type"
            java.lang.String r19 = ""
            r0 = r16
            r1 = r19
            boolean r19 = r0.equals(r1)     // Catch:{ all -> 0x0127 }
            if (r19 == 0) goto L_0x0470
            java.lang.String r16 = "baidu"
        L_0x0470:
            r0 = r17
            r1 = r18
            r2 = r16
            r0.put(r1, r2)     // Catch:{ all -> 0x0127 }
            java.util.HashMap<java.lang.String, java.lang.String> r16 = com.shoujiduoduo.util.am.f     // Catch:{ all -> 0x0127 }
            java.lang.String r17 = "feed_ad_enable"
            java.lang.String r18 = ""
            r0 = r18
            boolean r18 = r15.equals(r0)     // Catch:{ all -> 0x0127 }
            if (r18 == 0) goto L_0x0489
            java.lang.String r15 = "true"
        L_0x0489:
            r0 = r16
            r1 = r17
            r0.put(r1, r15)     // Catch:{ all -> 0x0127 }
            java.util.HashMap<java.lang.String, java.lang.String> r15 = com.shoujiduoduo.util.am.f     // Catch:{ all -> 0x0127 }
            java.lang.String r16 = "feed_ad_channel"
            java.lang.String r17 = ""
            r0 = r17
            boolean r17 = r14.equals(r0)     // Catch:{ all -> 0x0127 }
            if (r17 == 0) goto L_0x04a0
            java.lang.String r14 = "all"
        L_0x04a0:
            r0 = r16
            r15.put(r0, r14)     // Catch:{ all -> 0x0127 }
            java.util.HashMap<java.lang.String, java.lang.String> r14 = com.shoujiduoduo.util.am.f     // Catch:{ all -> 0x0127 }
            java.lang.String r15 = "dns_retry"
            java.lang.String r16 = ""
            r0 = r16
            boolean r16 = r12.equals(r0)     // Catch:{ all -> 0x0127 }
            if (r16 == 0) goto L_0x04b5
            java.lang.String r12 = "3"
        L_0x04b5:
            r14.put(r15, r12)     // Catch:{ all -> 0x0127 }
            java.util.HashMap<java.lang.String, java.lang.String> r12 = com.shoujiduoduo.util.am.f     // Catch:{ all -> 0x0127 }
            java.lang.String r14 = "dns_timeout"
            java.lang.String r15 = ""
            boolean r15 = r13.equals(r15)     // Catch:{ all -> 0x0127 }
            if (r15 == 0) goto L_0x04c6
            java.lang.String r13 = "8000"
        L_0x04c6:
            r12.put(r14, r13)     // Catch:{ all -> 0x0127 }
            java.util.HashMap<java.lang.String, java.lang.String> r12 = com.shoujiduoduo.util.am.f     // Catch:{ all -> 0x0127 }
            java.lang.String r13 = "dns_value1"
            java.lang.String r14 = ""
            boolean r14 = r11.equals(r14)     // Catch:{ all -> 0x0127 }
            if (r14 == 0) goto L_0x04d7
            java.lang.String r11 = "cdnringhlt.shoujiduoduo.com"
        L_0x04d7:
            r12.put(r13, r11)     // Catch:{ all -> 0x0127 }
            java.util.HashMap<java.lang.String, java.lang.String> r11 = com.shoujiduoduo.util.am.f     // Catch:{ all -> 0x0127 }
            java.lang.String r12 = "dns_value2"
            java.lang.String r13 = ""
            boolean r13 = r10.equals(r13)     // Catch:{ all -> 0x0127 }
            if (r13 == 0) goto L_0x04e8
            java.lang.String r10 = "cdnringfw.shoujiduoduo.com"
        L_0x04e8:
            r11.put(r12, r10)     // Catch:{ all -> 0x0127 }
            java.util.HashMap<java.lang.String, java.lang.String> r10 = com.shoujiduoduo.util.am.f     // Catch:{ all -> 0x0127 }
            java.lang.String r11 = "dns_check1"
            java.lang.String r12 = ""
            boolean r12 = r9.equals(r12)     // Catch:{ all -> 0x0127 }
            if (r12 == 0) goto L_0x04f9
            java.lang.String r9 = "http://cdnringhlt.shoujiduoduo.com/ringres/verify.dat"
        L_0x04f9:
            r10.put(r11, r9)     // Catch:{ all -> 0x0127 }
            java.util.HashMap<java.lang.String, java.lang.String> r9 = com.shoujiduoduo.util.am.f     // Catch:{ all -> 0x0127 }
            java.lang.String r10 = "dns_check2"
            java.lang.String r11 = ""
            boolean r11 = r8.equals(r11)     // Catch:{ all -> 0x0127 }
            if (r11 == 0) goto L_0x050a
            java.lang.String r8 = "http://cdnringfw.shoujiduoduo.com/ringres/verify.dat"
        L_0x050a:
            r9.put(r10, r8)     // Catch:{ all -> 0x0127 }
            java.util.HashMap<java.lang.String, java.lang.String> r8 = com.shoujiduoduo.util.am.f     // Catch:{ all -> 0x0127 }
            java.lang.String r9 = "dns_switchcondition"
            java.lang.String r10 = ""
            boolean r10 = r7.equals(r10)     // Catch:{ all -> 0x0127 }
            if (r10 == 0) goto L_0x051b
            java.lang.String r7 = "network"
        L_0x051b:
            r8.put(r9, r7)     // Catch:{ all -> 0x0127 }
            java.util.HashMap<java.lang.String, java.lang.String> r7 = com.shoujiduoduo.util.am.f     // Catch:{ all -> 0x0127 }
            java.lang.String r8 = "dns_duoduo_check"
            java.lang.String r9 = ""
            boolean r9 = r6.equals(r9)     // Catch:{ all -> 0x0127 }
            if (r9 == 0) goto L_0x052c
            java.lang.String r6 = "http://www.shoujiduoduo.com/ringv1/verify.dat"
        L_0x052c:
            r7.put(r8, r6)     // Catch:{ all -> 0x0127 }
            java.util.HashMap<java.lang.String, java.lang.String> r6 = com.shoujiduoduo.util.am.f     // Catch:{ all -> 0x0127 }
            java.lang.String r7 = "aichang_enable"
            java.lang.String r8 = ""
            boolean r8 = r5.equals(r8)     // Catch:{ all -> 0x0127 }
            if (r8 == 0) goto L_0x053d
            java.lang.String r5 = "false"
        L_0x053d:
            r6.put(r7, r5)     // Catch:{ all -> 0x0127 }
            java.util.HashMap<java.lang.String, java.lang.String> r5 = com.shoujiduoduo.util.am.f     // Catch:{ all -> 0x0127 }
            java.lang.String r6 = "ad_delay_time"
            java.lang.String r7 = ""
            boolean r7 = r4.equals(r7)     // Catch:{ all -> 0x0127 }
            if (r7 == 0) goto L_0x054e
            java.lang.String r4 = "0"
        L_0x054e:
            r5.put(r6, r4)     // Catch:{ all -> 0x0127 }
            java.util.HashMap<java.lang.String, java.lang.String> r4 = com.shoujiduoduo.util.am.f     // Catch:{ all -> 0x0127 }
            java.lang.String r5 = "cm_sunshine_sdk_enable"
            java.lang.String r6 = ""
            boolean r6 = r3.equals(r6)     // Catch:{ all -> 0x0127 }
            if (r6 == 0) goto L_0x055f
            java.lang.String r3 = "true"
        L_0x055f:
            r4.put(r5, r3)     // Catch:{ all -> 0x0127 }
            r0 = r38
            java.util.ArrayList<com.shoujiduoduo.util.am$b> r3 = r0.b     // Catch:{ all -> 0x0127 }
            com.shoujiduoduo.util.an r4 = new com.shoujiduoduo.util.an     // Catch:{ all -> 0x0127 }
            r0 = r38
            r4.<init>(r0)     // Catch:{ all -> 0x0127 }
            java.util.Collections.sort(r3, r4)     // Catch:{ all -> 0x0127 }
            java.lang.String r3 = com.shoujiduoduo.util.am.f1564a     // Catch:{ all -> 0x0127 }
            java.lang.String r4 = "banner ad 顺序"
            com.shoujiduoduo.base.a.a.a(r3, r4)     // Catch:{ all -> 0x0127 }
            r0 = r38
            java.util.ArrayList<com.shoujiduoduo.util.am$b> r3 = r0.b     // Catch:{ all -> 0x0127 }
            java.util.Iterator r4 = r3.iterator()     // Catch:{ all -> 0x0127 }
        L_0x057f:
            boolean r3 = r4.hasNext()     // Catch:{ all -> 0x0127 }
            if (r3 == 0) goto L_0x05ba
            java.lang.Object r3 = r4.next()     // Catch:{ all -> 0x0127 }
            com.shoujiduoduo.util.am$b r3 = (com.shoujiduoduo.util.am.b) r3     // Catch:{ all -> 0x0127 }
            java.lang.String r5 = com.shoujiduoduo.util.am.f1564a     // Catch:{ all -> 0x0127 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x0127 }
            r6.<init>()     // Catch:{ all -> 0x0127 }
            com.shoujiduoduo.util.am$a r7 = r3.c     // Catch:{ all -> 0x0127 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ all -> 0x0127 }
            java.lang.String r7 = " order:"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ all -> 0x0127 }
            int r7 = r3.b     // Catch:{ all -> 0x0127 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ all -> 0x0127 }
            java.lang.String r7 = " duration:"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ all -> 0x0127 }
            java.lang.String r3 = r3.f1567a     // Catch:{ all -> 0x0127 }
            java.lang.StringBuilder r3 = r6.append(r3)     // Catch:{ all -> 0x0127 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0127 }
            com.shoujiduoduo.base.a.a.a(r5, r3)     // Catch:{ all -> 0x0127 }
            goto L_0x057f
        L_0x05b8:
            r3 = move-exception
            throw r3     // Catch:{ all -> 0x0127 }
        L_0x05ba:
            monitor-exit(r32)     // Catch:{ all -> 0x0127 }
            java.lang.String r3 = com.shoujiduoduo.util.am.f1564a
            java.lang.String r4 = "end load cache! return TRUE!"
            com.shoujiduoduo.base.a.a.a(r3, r4)
            r3 = 1
            goto L_0x0068
        */
        throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.util.am.i():boolean");
    }

    /* compiled from: ServerConfig */
    public static class b {

        /* renamed from: a  reason: collision with root package name */
        public String f1567a;
        public int b;
        public a c;

        public b() {
            this.f1567a = "0";
            this.b = -1;
            this.c = a.BAIDU;
        }

        public b(int i, String str, a aVar) {
            this.f1567a = str;
            this.b = i;
            this.c = aVar;
        }
    }

    private b a(NamedNodeMap namedNodeMap, a aVar) {
        b bVar = new b();
        bVar.f1567a = f.a(namedNodeMap, "duration");
        try {
            bVar.b = Integer.valueOf(f.a(namedNodeMap, "neworder")).intValue();
        } catch (NumberFormatException e2) {
            bVar.b = -1;
        }
        bVar.c = aVar;
        if (bVar.b >= 0 && !bVar.f1567a.equalsIgnoreCase("0") && !bVar.f1567a.equalsIgnoreCase("")) {
            this.b.add(bVar);
        }
        switch (aVar) {
            case DUODUO:
                f.put("duoduo_ad_duration", bVar.f1567a.equals("") ? "0" : bVar.f1567a);
                break;
            case DOMOB:
                f.put("domob_ad_duration", bVar.f1567a.equals("") ? "0" : bVar.f1567a);
                break;
            case BAIDU:
                f.put("baidu_ad_duration", bVar.f1567a.equals("") ? "3000" : bVar.f1567a);
                break;
            case TENCENT:
                f.put("tencent_ad_duration", bVar.f1567a.equals("") ? "30" : bVar.f1567a);
                break;
            case TAOBAO:
                f.put("taobao_ad_duration", bVar.f1567a.equals("") ? "0" : bVar.f1567a);
                break;
        }
        return bVar;
    }

    public boolean c() {
        return this.e;
    }

    public ArrayList<b> d() {
        return this.b;
    }

    public String a(String str) {
        String str2;
        synchronized (f) {
            if (f.containsKey(str)) {
                str2 = f.get(str);
            } else {
                str2 = "";
            }
        }
        return str2;
    }

    public boolean b(String str) {
        boolean z;
        synchronized (f) {
            if (f.containsKey(str)) {
                z = "true".equalsIgnoreCase(f.get(str));
            } else {
                z = false;
            }
        }
        return z;
    }

    private void j() {
        i.a(new ao(this));
    }
}
