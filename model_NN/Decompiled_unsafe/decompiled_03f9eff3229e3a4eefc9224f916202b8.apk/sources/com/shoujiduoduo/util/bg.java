package com.shoujiduoduo.util;

import android.os.Handler;
import android.os.Message;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.util.a.g;
import java.io.File;

/* compiled from: WavDataProcess */
class bg extends Thread {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f1629a;
    final /* synthetic */ Handler b;
    final /* synthetic */ be c;

    bg(be beVar, String str, Handler handler) {
        this.c = beVar;
        this.f1629a = str;
        this.b = handler;
    }

    public void run() {
        String string;
        this.c.i.sendEmptyMessage(1);
        a.a("WavDataProcess", "begin to save, edit mode, local file:" + this.c.h);
        if (this.c.l == 0.0f && this.c.m == 1.0f) {
            File file = new File(this.c.g);
            File file2 = new File(this.f1629a);
            if (file.getParent().equals(file2.getParent())) {
                if (t.a(this.c.g, this.f1629a)) {
                    this.b.sendEmptyMessage(3);
                } else {
                    this.b.sendEmptyMessage(4);
                }
            } else if (t.a(file, file2)) {
                this.b.sendEmptyMessage(3);
            } else {
                this.b.sendEmptyMessage(4);
            }
        } else {
            double c2 = ((double) (((float) this.c.c()) * this.c.l)) * 0.001d;
            double c3 = ((double) (((float) this.c.c()) * this.c.m)) * 0.001d;
            int a2 = this.c.a(c2);
            int a3 = this.c.a(c3);
            a.a("WavDataProcess", "save ring, duration:" + ((int) ((c3 - c2) + 0.5d)));
            try {
                this.c.k.a(new File(this.c.h), a2, a3 - a2);
                g.a(this.c.h, new bh(this));
                a.a("WavDataProcess", "begin to save, edit mode, save complete");
                this.c.i.sendEmptyMessage(3);
            } catch (Exception e) {
                if (e.getMessage().equals("No space left on device")) {
                    string = RingDDApp.c().getResources().getString(R.string.record_ring_sd_full);
                } else {
                    string = RingDDApp.c().getResources().getString(R.string.record_save_error);
                }
                Message message = new Message();
                message.what = 4;
                message.obj = string;
                this.c.i.sendMessage(message);
                a.a("WavDataProcess", "begin to save, edit mode, save error, msg:" + string);
            }
        }
    }
}
