package com.shoujiduoduo.util;

import com.baidu.mobads.interfaces.IXAdRequestInfo;
import com.shoujiduoduo.base.bean.MakeRingData;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.base.bean.a;
import com.shoujiduoduo.base.bean.b;
import com.shoujiduoduo.base.bean.e;
import com.sina.weibo.sdk.constant.WBConstants;
import com.sina.weibo.sdk.register.mobile.SelectCountryActivity;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/* compiled from: DataParse */
public class j {
    public static e<b> a(String str) {
        try {
            return b(new FileInputStream(str));
        } catch (ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e2) {
            e2.printStackTrace();
        }
        return null;
    }

    public static e<a> a(InputStream inputStream) {
        DocumentBuilder newDocumentBuilder;
        Document parse;
        Element documentElement;
        try {
            DocumentBuilderFactory newInstance = DocumentBuilderFactory.newInstance();
            if (newInstance == null || (newDocumentBuilder = newInstance.newDocumentBuilder()) == null || inputStream == null || (parse = newDocumentBuilder.parse(inputStream)) == null || (documentElement = parse.getDocumentElement()) == null) {
                return null;
            }
            boolean equalsIgnoreCase = documentElement.getAttribute("hasmore").equalsIgnoreCase("true");
            String attribute = documentElement.getAttribute("area");
            String attribute2 = documentElement.getAttribute("baseurl");
            NodeList elementsByTagName = documentElement.getElementsByTagName("artist");
            if (elementsByTagName == null) {
                return null;
            }
            ArrayList<T> arrayList = new ArrayList<>();
            for (int i = 0; i < elementsByTagName.getLength(); i++) {
                NamedNodeMap attributes = elementsByTagName.item(i).getAttributes();
                a aVar = new a();
                a(attributes, aVar);
                arrayList.add(aVar);
            }
            e<a> eVar = new e<>();
            eVar.f821a = arrayList;
            eVar.b = equalsIgnoreCase;
            eVar.c = attribute;
            eVar.d = attribute2;
            return eVar;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (DOMException e2) {
            e2.printStackTrace();
            return null;
        } catch (SAXException e3) {
            e3.printStackTrace();
            return null;
        } catch (ParserConfigurationException e4) {
            e4.printStackTrace();
            return null;
        } catch (ArrayIndexOutOfBoundsException e5) {
            e5.printStackTrace();
            return null;
        }
    }

    public static e<b> b(InputStream inputStream) {
        DocumentBuilder newDocumentBuilder;
        Document parse;
        Element documentElement;
        try {
            DocumentBuilderFactory newInstance = DocumentBuilderFactory.newInstance();
            if (newInstance == null || (newDocumentBuilder = newInstance.newDocumentBuilder()) == null || inputStream == null || (parse = newDocumentBuilder.parse(inputStream)) == null || (documentElement = parse.getDocumentElement()) == null) {
                return null;
            }
            boolean equalsIgnoreCase = documentElement.getAttribute("hasmore").equalsIgnoreCase("true");
            String attribute = documentElement.getAttribute("area");
            String attribute2 = documentElement.getAttribute("baseurl");
            NodeList elementsByTagName = documentElement.getElementsByTagName("collect");
            if (elementsByTagName == null) {
                return null;
            }
            ArrayList<T> arrayList = new ArrayList<>();
            for (int i = 0; i < elementsByTagName.getLength(); i++) {
                NamedNodeMap attributes = elementsByTagName.item(i).getAttributes();
                b bVar = new b();
                a(attributes, bVar);
                arrayList.add(bVar);
            }
            e<b> eVar = new e<>();
            eVar.f821a = arrayList;
            eVar.b = equalsIgnoreCase;
            eVar.c = attribute;
            eVar.d = attribute2;
            return eVar;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (DOMException e2) {
            e2.printStackTrace();
            return null;
        } catch (SAXException e3) {
            e3.printStackTrace();
            return null;
        } catch (ParserConfigurationException e4) {
            e4.printStackTrace();
            return null;
        } catch (ArrayIndexOutOfBoundsException e5) {
            e5.printStackTrace();
            return null;
        }
    }

    public static e<RingData> c(InputStream inputStream) {
        DocumentBuilder newDocumentBuilder;
        Document parse;
        Element documentElement;
        try {
            DocumentBuilderFactory newInstance = DocumentBuilderFactory.newInstance();
            if (newInstance == null || (newDocumentBuilder = newInstance.newDocumentBuilder()) == null || inputStream == null || (parse = newDocumentBuilder.parse(inputStream)) == null || (documentElement = parse.getDocumentElement()) == null) {
                return null;
            }
            boolean equalsIgnoreCase = documentElement.getAttribute("hasmore").equalsIgnoreCase("true");
            String attribute = documentElement.getAttribute("area");
            String attribute2 = documentElement.getAttribute("baseurl");
            NodeList elementsByTagName = documentElement.getElementsByTagName("ring");
            if (elementsByTagName == null) {
                return null;
            }
            ArrayList<T> arrayList = new ArrayList<>();
            for (int i = 0; i < elementsByTagName.getLength(); i++) {
                NamedNodeMap attributes = elementsByTagName.item(i).getAttributes();
                RingData ringData = new RingData();
                ringData.a(attribute2);
                a(attributes, ringData);
                arrayList.add(ringData);
            }
            e<RingData> eVar = new e<>();
            eVar.f821a = arrayList;
            eVar.b = equalsIgnoreCase;
            eVar.c = attribute;
            eVar.d = attribute2;
            return eVar;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (DOMException e2) {
            e2.printStackTrace();
            return null;
        } catch (SAXException e3) {
            e3.printStackTrace();
            return null;
        } catch (ParserConfigurationException e4) {
            e4.printStackTrace();
            return null;
        } catch (ArrayIndexOutOfBoundsException e5) {
            e5.printStackTrace();
            return null;
        }
    }

    public static e<MakeRingData> d(InputStream inputStream) {
        DocumentBuilder newDocumentBuilder;
        Document parse;
        Element documentElement;
        try {
            DocumentBuilderFactory newInstance = DocumentBuilderFactory.newInstance();
            if (newInstance == null || (newDocumentBuilder = newInstance.newDocumentBuilder()) == null || inputStream == null || (parse = newDocumentBuilder.parse(inputStream)) == null || (documentElement = parse.getDocumentElement()) == null) {
                return null;
            }
            boolean equalsIgnoreCase = documentElement.getAttribute("hasmore").equalsIgnoreCase("true");
            String attribute = documentElement.getAttribute("area");
            String attribute2 = documentElement.getAttribute("baseurl");
            NodeList elementsByTagName = documentElement.getElementsByTagName("ring");
            if (elementsByTagName == null) {
                return null;
            }
            ArrayList<T> arrayList = new ArrayList<>();
            com.shoujiduoduo.base.a.a.a("DataParse", "parseContent: listRing size = " + arrayList.size());
            for (int i = 0; i < elementsByTagName.getLength(); i++) {
                NamedNodeMap attributes = elementsByTagName.item(i).getAttributes();
                MakeRingData makeRingData = new MakeRingData();
                makeRingData.a(attribute2);
                a(attributes, makeRingData);
                makeRingData.c = f.a(attributes, "date");
                makeRingData.f814a = u.a(f.a(attributes, "upload"), 0);
                makeRingData.d = u.a(f.a(attributes, "makeType"), 0);
                makeRingData.m = f.a(attributes, "localPath");
                arrayList.add(makeRingData);
            }
            e<MakeRingData> eVar = new e<>();
            eVar.f821a = arrayList;
            eVar.b = equalsIgnoreCase;
            eVar.c = attribute;
            eVar.d = attribute2;
            return eVar;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (DOMException e2) {
            e2.printStackTrace();
            return null;
        } catch (SAXException e3) {
            e3.printStackTrace();
            return null;
        } catch (ParserConfigurationException e4) {
            e4.printStackTrace();
            return null;
        } catch (ArrayIndexOutOfBoundsException e5) {
            e5.printStackTrace();
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.util.f.a(org.w3c.dom.NamedNodeMap, java.lang.String, boolean):boolean
     arg types: [org.w3c.dom.NamedNodeMap, java.lang.String, int]
     candidates:
      com.shoujiduoduo.util.f.a(org.w3c.dom.NamedNodeMap, java.lang.String, int):int
      com.shoujiduoduo.util.f.a(org.w3c.dom.NamedNodeMap, java.lang.String, java.lang.String):java.lang.String
      com.shoujiduoduo.util.f.a(android.content.Context, java.lang.String, int):void
      com.shoujiduoduo.util.f.a(org.w3c.dom.NamedNodeMap, java.lang.String, boolean):boolean */
    private static void a(NamedNodeMap namedNodeMap, a aVar) {
        aVar.f816a = f.a(namedNodeMap, "pic");
        aVar.b = f.a(namedNodeMap, "isNew", false);
        aVar.c = u.a(f.a(namedNodeMap, "sale"), 100);
        aVar.d = f.a(namedNodeMap, "work");
        aVar.e = f.a(namedNodeMap, SelectCountryActivity.EXTRA_COUNTRY_NAME);
        aVar.f = f.a(namedNodeMap, "id");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.util.f.a(org.w3c.dom.NamedNodeMap, java.lang.String, boolean):boolean
     arg types: [org.w3c.dom.NamedNodeMap, java.lang.String, int]
     candidates:
      com.shoujiduoduo.util.f.a(org.w3c.dom.NamedNodeMap, java.lang.String, int):int
      com.shoujiduoduo.util.f.a(org.w3c.dom.NamedNodeMap, java.lang.String, java.lang.String):java.lang.String
      com.shoujiduoduo.util.f.a(android.content.Context, java.lang.String, int):void
      com.shoujiduoduo.util.f.a(org.w3c.dom.NamedNodeMap, java.lang.String, boolean):boolean */
    private static void a(NamedNodeMap namedNodeMap, b bVar) {
        bVar.f819a = f.a(namedNodeMap, "pic");
        bVar.h = f.a(namedNodeMap, "artist");
        bVar.b = f.a(namedNodeMap, "title");
        bVar.c = f.a(namedNodeMap, "content");
        bVar.d = f.a(namedNodeMap, "time");
        bVar.e = f.a(namedNodeMap, "isnew", false);
        bVar.f = f.a(namedNodeMap, "fav");
        bVar.g = f.a(namedNodeMap, "id");
    }

    private static void a(NamedNodeMap namedNodeMap, RingData ringData) {
        ringData.e = f.a(namedNodeMap, SelectCountryActivity.EXTRA_COUNTRY_NAME);
        ringData.f = f.a(namedNodeMap, "artist");
        ringData.j = u.a(f.a(namedNodeMap, "duration"), 0);
        ringData.i = u.a(f.a(namedNodeMap, WBConstants.GAME_PARAMS_SCORE), 0);
        ringData.k = u.a(f.a(namedNodeMap, "playcnt"), 0);
        ringData.g = f.a(namedNodeMap, "rid");
        ringData.h = f.a(namedNodeMap, "bdurl");
        ringData.a(u.a(f.a(namedNodeMap, "hbr"), 0));
        ringData.b(f.a(namedNodeMap, "hurl"));
        ringData.b(u.a(f.a(namedNodeMap, "lbr"), 0));
        ringData.c(f.a(namedNodeMap, "lurl"));
        ringData.c(u.a(f.a(namedNodeMap, "mp3br"), 0));
        ringData.d(f.a(namedNodeMap, "mp3url"));
        ringData.l = u.a(f.a(namedNodeMap, "isnew"), 0);
        ringData.n = f.a(namedNodeMap, IXAdRequestInfo.CELL_ID);
        ringData.o = f.a(namedNodeMap, "valid");
        ringData.r = f.a(namedNodeMap, "singerId");
        ringData.p = u.a(f.a(namedNodeMap, "price"), 200);
        ringData.q = u.a(f.a(namedNodeMap, "hasmedia"), 0);
        ringData.s = f.a(namedNodeMap, "ctcid");
        ringData.t = f.a(namedNodeMap, "ctvalid");
        ringData.u = u.a(f.a(namedNodeMap, "ctprice"), 200);
        ringData.v = u.a(f.a(namedNodeMap, "cthasmedia"), 0);
        ringData.w = u.a(f.a(namedNodeMap, "ctvip"), 0);
        ringData.x = f.a(namedNodeMap, "wavurl");
        ringData.y = u.a(f.a(namedNodeMap, "cuvip"), 0);
        ringData.z = f.a(namedNodeMap, "cuftp");
        ringData.A = f.a(namedNodeMap, "cucid");
        ringData.C = f.a(namedNodeMap, "cusid");
        ringData.D = f.a(namedNodeMap, "cuurl");
        ringData.B = f.a(namedNodeMap, "cuvalid");
        ringData.E = u.a(f.a(namedNodeMap, "hasshow"), 0);
    }
}
