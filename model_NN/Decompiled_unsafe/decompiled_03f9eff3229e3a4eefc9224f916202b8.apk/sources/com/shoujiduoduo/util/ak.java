package com.shoujiduoduo.util;

import com.shoujiduoduo.player.PlayerService;

/* compiled from: PlayerServiceUtil */
public class ak {
    private static ak b = null;

    /* renamed from: a  reason: collision with root package name */
    private PlayerService f1562a;

    private ak() {
    }

    public static ak a() {
        if (b == null) {
            b = new ak();
        }
        return b;
    }

    public void a(PlayerService playerService) {
        this.f1562a = playerService;
    }

    public PlayerService b() {
        return this.f1562a;
    }

    public void c() {
        this.f1562a = null;
        b = null;
    }
}
