package com.shoujiduoduo.util;

import com.shoujiduoduo.base.a.a;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.net.HttpURLConnection;
import java.net.URL;
import org.apache.mina.proxy.handlers.http.HttpProxyConstants;

/* compiled from: BcsUtils */
public class c {

    /* renamed from: a  reason: collision with root package name */
    private static String f1633a = "http://bj.bcebos.com";

    public static boolean a(String str, String str2, int i) {
        String str3;
        a.a("BcsUtils", "putObject in");
        try {
            f1633a = "http://" + am.a().a("bcs_domain_name");
            a.a("BcsUtils", "url:" + f1633a + "/duoduo-ring-user-upload" + (i == 0 ? "/record-ring/" : "/edit-ring/"));
            URL url = new URL(f1633a + "/duoduo-ring-user-upload" + (i == 0 ? "/record-ring/" : "/edit-ring/") + str2);
            a.a("BcsUtils", "url2:" + f1633a + "/duoduo-ring-user-upload" + (i == 0 ? "/record-ring/" : "/edit-ring/") + str2);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            httpURLConnection.setConnectTimeout(30000);
            httpURLConnection.setReadTimeout(30000);
            httpURLConnection.setUseCaches(false);
            httpURLConnection.setRequestMethod(HttpProxyConstants.PUT);
            File file = new File(str);
            if (!file.exists()) {
                return false;
            }
            httpURLConnection.setRequestProperty("Content-length", "" + file.length());
            httpURLConnection.setRequestProperty("Accept", "*/*");
            httpURLConnection.setRequestProperty("Content-Type", "binary/octet-stream");
            httpURLConnection.setRequestProperty("Connection", "Keep-Alive");
            httpURLConnection.setRequestProperty("Charset", "UTF-8");
            a.a("BcsUtils", "output before");
            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(httpURLConnection.getOutputStream());
            bufferedOutputStream.write(ad.a(file));
            bufferedOutputStream.flush();
            bufferedOutputStream.close();
            a.a("BcsUtils", "output flush");
            int responseCode = httpURLConnection.getResponseCode();
            a.a("BcsUtils", "responseCode-----" + responseCode);
            if (200 == responseCode) {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                byte[] bArr = new byte[1024];
                while (true) {
                    int read = httpURLConnection.getInputStream().read(bArr, 0, 1024);
                    if (read != -1) {
                        byteArrayOutputStream.write(bArr, 0, read);
                    } else {
                        a.a("BcsUtils", "responseBody------------\r\n" + new String(byteArrayOutputStream.toByteArray(), "UTF-8"));
                        return true;
                    }
                }
            } else {
                str3 = "";
                a.a("cm httppostcore return:", str3);
                return false;
            }
        } catch (Exception e) {
            Exception exc = e;
            str3 = "";
            exc.printStackTrace();
            a.c("BcsUtils", "bcs upload exception");
        }
    }
}
