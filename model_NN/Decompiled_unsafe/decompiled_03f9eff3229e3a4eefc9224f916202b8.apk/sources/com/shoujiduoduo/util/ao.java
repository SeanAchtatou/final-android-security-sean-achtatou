package com.shoujiduoduo.util;

import com.shoujiduoduo.a.a.b;
import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.ringtone.RingDDApp;

/* compiled from: ServerConfig */
class ao implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ am f1570a;

    ao(am amVar) {
        this.f1570a = amVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.util.am.a(com.shoujiduoduo.util.am, boolean):boolean
     arg types: [com.shoujiduoduo.util.am, int]
     candidates:
      com.shoujiduoduo.util.am.a(org.w3c.dom.NamedNodeMap, com.shoujiduoduo.util.am$a):com.shoujiduoduo.util.am$b
      com.shoujiduoduo.util.am.a(com.shoujiduoduo.util.am, boolean):boolean */
    public void run() {
        a.a(am.f1564a, "loadFromNetwork Thread, ThreadID = " + Thread.currentThread().getId());
        byte[] b = w.b();
        if (b != null) {
            String str = new String(b);
            t.b(am.c, str);
            as.b(RingDDApp.c(), "update_config_time", System.currentTimeMillis());
            as.c(RingDDApp.c(), "pref_config_src", f.p());
            if (this.f1570a.i()) {
                a.a("WelcomeActivity", "loadFromNetWork success");
                a.a("WelcomeActivity", "content:" + str);
                boolean unused = this.f1570a.e = true;
                x.a().b(b.OBSERVER_CONFIG, new ap(this));
                return;
            }
            return;
        }
        a.c(am.f1564a, "get server config error!!");
    }
}
