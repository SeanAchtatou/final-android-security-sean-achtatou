package com.shoujiduoduo.util;

import cn.banshenggua.aichang.room.message.SocketMessage;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.util.m;
import com.umeng.analytics.b;
import java.util.HashMap;

/* compiled from: DnsDetector */
class n implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ m f1680a;

    n(m mVar) {
        this.f1680a = mVar;
    }

    /* JADX INFO: finally extract failed */
    public void run() {
        boolean z = false;
        int i = 0;
        while (true) {
            if (i >= this.f1680a.k) {
                break;
            }
            m.a aVar = new m.a(this.f1680a.f1678a);
            aVar.start();
            try {
                aVar.join((long) this.f1680a.j);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (aVar.a() == null || this.f1680a.a(this.f1680a.b, true)) {
                a.e("dnsdetector", "cdn dns not work:" + this.f1680a.f1678a + ", retry times:" + (i + 1));
                i++;
            } else {
                a.a("dnsdetector", "cdn dns available:" + this.f1680a.f1678a + ", ip:" + aVar.a());
                this.f1680a.n.lock();
                try {
                    String unused = m.e = this.f1680a.f1678a;
                    this.f1680a.n.unlock();
                    z = true;
                    break;
                } catch (Throwable th) {
                    this.f1680a.n.unlock();
                    throw th;
                }
            }
        }
        HashMap hashMap = new HashMap();
        if (!z) {
            HashMap hashMap2 = new HashMap();
            m.a aVar2 = new m.a(this.f1680a.c);
            aVar2.start();
            try {
                aVar2.join((long) this.f1680a.j);
            } catch (InterruptedException e2) {
                e2.printStackTrace();
            }
            if (aVar2.a() != null) {
                hashMap2.put("cdn2 available", "yes");
                hashMap.put(SocketMessage.MSG_RESULE_KEY, "cdn2 success");
            } else {
                hashMap2.put("cdn2 available", "no");
                hashMap.put(SocketMessage.MSG_RESULE_KEY, "fail");
            }
            hashMap2.put("network type", NetworkStateUtil.b());
            b.a(RingDDApp.c(), "cdn_dns_error_new", hashMap2);
            this.f1680a.n.lock();
            try {
                String unused2 = m.e = this.f1680a.c;
                this.f1680a.n.unlock();
                a.a("dnsdetector", "availableCdn use cdn2:" + m.e);
            } catch (Throwable th2) {
                this.f1680a.n.unlock();
                throw th2;
            }
        } else {
            hashMap.put(SocketMessage.MSG_RESULE_KEY, "cdn1 success");
        }
        b.a(RingDDApp.c(), "cdn_dns_check", hashMap);
        a.a("dnsdetector", "detect dns server end");
    }
}
