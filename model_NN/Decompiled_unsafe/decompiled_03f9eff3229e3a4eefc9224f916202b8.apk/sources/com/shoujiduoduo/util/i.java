package com.shoujiduoduo.util;

import com.shoujiduoduo.base.a.a;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/* compiled from: DDThreadPool */
public class i {

    /* renamed from: a  reason: collision with root package name */
    private static ExecutorService f1676a = Executors.newCachedThreadPool();

    public static void a(Runnable runnable) {
        a.a("threadpool", "count:" + Thread.activeCount());
        f1676a.execute(runnable);
    }

    public static void a() {
        if (f1676a != null) {
            f1676a.shutdown();
        }
    }
}
