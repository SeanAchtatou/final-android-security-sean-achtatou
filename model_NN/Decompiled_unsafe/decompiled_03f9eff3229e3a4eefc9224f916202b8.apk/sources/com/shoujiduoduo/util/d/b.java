package com.shoujiduoduo.util.d;

import android.text.TextUtils;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.f;
import com.shoujiduoduo.util.i;
import com.shoujiduoduo.util.w;
import com.sina.weibo.sdk.constant.WBPageConstants;
import com.sina.weibo.sdk.exception.WeiboAuthException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

/* compiled from: ChinaTelecomUtils */
public class b {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final c.b f1651a = new c.b(WeiboAuthException.DEFAULT_AUTH_ERROR_CODE, "对不起，中国电信的彩铃服务正在进行系统维护，请谅解");
    /* access modifiers changed from: private */
    public static final c.b b = new c.b("-2", "对不起，网络好像出了点问题，请稍后再试试");
    /* access modifiers changed from: private */
    public HashMap<String, d> c = new HashMap<>();
    /* access modifiers changed from: private */
    public HashMap<String, c> d = new HashMap<>();
    private String e;
    private String f;

    /* compiled from: ChinaTelecomUtils */
    private enum a {
        GET,
        POST
    }

    /* renamed from: com.shoujiduoduo.util.d.b$b  reason: collision with other inner class name */
    /* compiled from: ChinaTelecomUtils */
    private static class C0030b {

        /* renamed from: a  reason: collision with root package name */
        public static b f1653a = new b();
    }

    /* compiled from: ChinaTelecomUtils */
    public enum d {
        wait_open,
        open,
        close,
        unknown
    }

    /* compiled from: ChinaTelecomUtils */
    public static class c {

        /* renamed from: a  reason: collision with root package name */
        public boolean f1654a;
        public boolean b;

        public c() {
            this.f1654a = false;
            this.b = false;
        }

        public c(boolean z, boolean z2) {
            this.f1654a = z;
            this.b = z2;
        }
    }

    public static b a() {
        return C0030b.f1653a;
    }

    public d a(String str) {
        if (this.c.get(str) != null) {
            return this.c.get(str);
        }
        return d.unknown;
    }

    public c b(String str) {
        if (this.d.get(str) != null) {
            return this.d.get(str);
        }
        return null;
    }

    /* access modifiers changed from: private */
    public c.b d(String str) {
        JSONObject optJSONObject;
        com.shoujiduoduo.base.a.a.a("ChinaTelecomUtils", "content:" + str);
        if (str == null || str.equals("")) {
            return null;
        }
        try {
            JSONObject jSONObject = (JSONObject) new JSONTokener(str).nextValue();
            JSONObject optJSONObject2 = jSONObject.optJSONObject("BasicJTResponse");
            if (optJSONObject2 != null) {
                c.b bVar = new c.b();
                bVar.b = optJSONObject2.optString("res_code");
                bVar.c = optJSONObject2.optString("res_message");
                return bVar;
            }
            JSONObject optJSONObject3 = jSONObject.optJSONObject("playModeResponse");
            if (optJSONObject3 != null) {
                c.u uVar = new c.u();
                uVar.b = optJSONObject3.optString("res_code");
                uVar.c = optJSONObject3.optString("res_message");
                uVar.f1615a = optJSONObject3.optString("play_mode");
                return uVar;
            }
            JSONObject optJSONObject4 = jSONObject.optJSONObject("queryRingResponse");
            if (optJSONObject4 != null) {
                c.x xVar = new c.x();
                xVar.b = optJSONObject4.optString("res_code");
                xVar.c = optJSONObject4.optString("res_message");
                xVar.f1618a = new ArrayList();
                JSONArray optJSONArray = optJSONObject4.optJSONArray("ring_item");
                if (optJSONArray != null) {
                    for (int i = 0; i < optJSONArray.length(); i++) {
                        c.ac acVar = new c.ac();
                        JSONObject optJSONObject5 = optJSONArray.optJSONObject(i);
                        if (optJSONObject5 != null) {
                            acVar.f1589a = optJSONObject5.optString("ringId");
                            acVar.f = optJSONObject5.optString("price");
                            acVar.b = optJSONObject5.optString("ringName");
                            acVar.d = optJSONObject5.optString("author");
                            String optString = optJSONObject5.optString("validDate");
                            if (!TextUtils.isEmpty(optString) && optString.length() >= 10) {
                                optString = optString.substring(0, 10);
                            }
                            acVar.g = optString;
                            xVar.f1618a.add(acVar);
                        }
                    }
                } else {
                    JSONObject optJSONObject6 = optJSONObject4.optJSONObject("ring_item");
                    if (optJSONObject6 != null) {
                        c.ac acVar2 = new c.ac();
                        acVar2.f1589a = optJSONObject6.optString("ringId");
                        acVar2.f = optJSONObject6.optString("price");
                        acVar2.b = optJSONObject6.optString("ringName");
                        acVar2.d = optJSONObject6.optString("author");
                        String optString2 = optJSONObject6.optString("validDate");
                        if (!TextUtils.isEmpty(optString2) && optString2.length() >= 10) {
                            optString2 = optString2.substring(0, 10);
                        }
                        acVar2.g = optString2;
                        xVar.f1618a.add(acVar2);
                    }
                }
                return xVar;
            }
            JSONObject optJSONObject7 = jSONObject.optJSONObject("defaultRingResponse");
            if (optJSONObject7 != null) {
                c.x xVar2 = new c.x();
                xVar2.b = optJSONObject7.optString("res_code");
                xVar2.c = optJSONObject7.optString("res_message");
                JSONObject optJSONObject8 = optJSONObject7.optJSONObject("crbt_id_list");
                if (optJSONObject8 == null) {
                    return xVar2;
                }
                xVar2.f1618a = new ArrayList();
                c.ac acVar3 = new c.ac();
                acVar3.f1589a = optJSONObject8.optString("crbt_id");
                xVar2.f1618a.add(acVar3);
                return xVar2;
            }
            JSONObject optJSONObject9 = jSONObject.optJSONObject("audioFileResponse");
            if (optJSONObject9 != null) {
                c.k kVar = new c.k();
                kVar.b = optJSONObject9.optString("res_code");
                kVar.c = optJSONObject9.optString("res_message");
                JSONObject optJSONObject10 = optJSONObject9.optJSONObject("audioFileItemList");
                if (optJSONObject10 != null) {
                    JSONArray optJSONArray2 = optJSONObject10.optJSONArray("audioFileItem");
                    if (optJSONArray2 != null) {
                        int i2 = 0;
                        for (int i3 = 0; i3 < optJSONArray2.length(); i3++) {
                            JSONObject optJSONObject11 = optJSONArray2.optJSONObject(i3);
                            if (optJSONObject11 != null && optJSONObject11.optInt("bit_rate", 0) >= 0) {
                                i2 = i3;
                            }
                        }
                        JSONObject optJSONObject12 = optJSONArray2.optJSONObject(i2);
                        if (optJSONObject12 != null) {
                            kVar.f1604a = optJSONObject12.optString("file_address");
                            kVar.d = optJSONObject12.optInt("bit_rate", 128);
                            kVar.e = optJSONObject12.optString("format", "mp3");
                        }
                    } else {
                        JSONObject optJSONObject13 = optJSONObject10.optJSONObject("audioFileItem");
                        if (optJSONObject13 != null) {
                            kVar.f1604a = optJSONObject13.optString("file_address");
                            kVar.d = optJSONObject13.optInt("bit_rate", 128);
                            kVar.e = optJSONObject13.optString("format", "mp3");
                        }
                    }
                }
                return kVar;
            }
            JSONObject optJSONObject14 = jSONObject.optJSONObject("music_product");
            if (optJSONObject14 != null) {
                c.v vVar = new c.v();
                vVar.b = optJSONObject14.optString("res_code");
                vVar.c = optJSONObject14.optString("res_message");
                vVar.d = optJSONObject14.optString("resource_id");
                vVar.f1616a = new c.ac();
                vVar.f1616a.f1589a = optJSONObject14.optString("product_id");
                vVar.f1616a.f = optJSONObject14.optString("price");
                vVar.f1616a.g = optJSONObject14.optString("invalid_time");
                vVar.f1616a.b = optJSONObject14.optString("song_name");
                vVar.f1616a.d = optJSONObject14.optString("singer_name");
                return vVar;
            }
            JSONObject optJSONObject15 = jSONObject.optJSONObject("UserPackageListResp");
            if (optJSONObject15 != null) {
                c.t tVar = new c.t();
                tVar.b = optJSONObject15.optString("res_code");
                tVar.c = optJSONObject15.optString("res_message");
                JSONObject optJSONObject16 = optJSONObject15.optJSONObject("user_package_list");
                if (optJSONObject16 == null || (optJSONObject = optJSONObject16.optJSONObject("user_package")) == null) {
                    return tVar;
                }
                c.ah ahVar = new c.ah();
                ahVar.f1594a = optJSONObject.optString("package_id");
                ahVar.e = optJSONObject.optString("count_down_num");
                ahVar.b = optJSONObject.optString("order_time");
                ahVar.d = optJSONObject.optString("unsubscribe_time");
                ahVar.c = optJSONObject.optString("status");
                tVar.f1614a = optJSONObject.optString("status");
                if (!ahVar.c.equals("0") && !ahVar.c.equals("2")) {
                    return tVar;
                }
                tVar.d = true;
                return tVar;
            }
            JSONObject optJSONObject17 = jSONObject.optJSONObject("EmpPackageResp");
            if (optJSONObject17 != null) {
                c.m mVar = new c.m();
                mVar.b = optJSONObject17.optString("res_code");
                mVar.c = optJSONObject17.optString("res_message");
                mVar.f1607a = optJSONObject17.optString("fee_type");
                return mVar;
            }
            JSONObject optJSONObject18 = jSONObject.optJSONObject("DEPUserInfoResponse");
            if (optJSONObject18 != null) {
                c.h hVar = new c.h();
                hVar.b = optJSONObject18.optString("res_code");
                hVar.c = optJSONObject18.optString("res_message");
                hVar.f1601a = optJSONObject18.optString("mdn");
                return hVar;
            }
            JSONObject optJSONObject19 = jSONObject.optJSONObject("ringClipJTResponse");
            if (optJSONObject19 != null) {
                c.y yVar = new c.y();
                yVar.b = optJSONObject19.optString("res_code");
                yVar.c = optJSONObject19.optString("res_message");
                yVar.f1619a = optJSONObject19.optString("audio_id");
                yVar.d = optJSONObject19.optString("audio_url");
                return yVar;
            }
            JSONObject optJSONObject20 = jSONObject.optJSONObject("queryRingStatueJTResponse");
            if (optJSONObject20 == null) {
                return null;
            }
            c.g gVar = new c.g();
            gVar.b = optJSONObject20.optString("res_code");
            gVar.c = optJSONObject20.optString("res_message");
            gVar.f1600a = optJSONObject20.optString("ring_id");
            gVar.d = optJSONObject20.optString("ringStatus");
            gVar.e = optJSONObject20.optString("audio_url");
            return gVar;
        } catch (JSONException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public void a(String str, boolean z, com.shoujiduoduo.util.b.a aVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("mdn", str));
        arrayList.add(new BasicNameValuePair("package_id", z ? "135000000000000214309" : "135000000000000214311"));
        a(arrayList, "/package/packageservice/emplanunched", aVar, "&phone=" + str, a.POST);
    }

    public void a(String str, boolean z, String str2, boolean z2, String str3, com.shoujiduoduo.util.b.a aVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("mdn", str));
        if (z) {
            arrayList.add(new BasicNameValuePair("random_key", str2));
        }
        this.e = str;
        if (z) {
            this.f = z2 ? "ct_open_diy_emp" : "ct_open_vip_emp";
        } else {
            this.f = z2 ? "ct_open_diy" : "ct_open_vip";
        }
        arrayList.add(new BasicNameValuePair("package_id", z2 ? "135000000000000214309" : "135000000000000214311"));
        a(arrayList, z ? "/package/packageservice/subscribebyemp" : "/package/packageservice/subscribe", aVar, str3, a.POST);
    }

    public void a(String str, String str2, boolean z, String str3, com.shoujiduoduo.util.b.a aVar) {
        this.e = str;
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("mdn", str));
        arrayList.add(new BasicNameValuePair("random_key", str2));
        arrayList.add(new BasicNameValuePair("order_num", z ? "2" : "1"));
        this.f = z ? "ct_open_cailing_diy_emp" : "ct_open_cailing_vip_emp";
        a(arrayList, "/package/packageservice/ringduoduosubscribebyemp", aVar, str3, a.POST);
    }

    public void a(String str, com.shoujiduoduo.util.b.a aVar) {
        this.e = str;
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("mdn", str));
        ArrayList arrayList2 = new ArrayList();
        arrayList2.add(new BasicNameValuePair("mdn", str));
        arrayList2.add(new BasicNameValuePair("package_id", "135000000000000214311"));
        arrayList2.add(new BasicNameValuePair("is_count_down_num", "1"));
        ArrayList arrayList3 = new ArrayList();
        arrayList3.add(new BasicNameValuePair("mdn", str));
        arrayList3.add(new BasicNameValuePair("package_id", "135000000000000214309"));
        arrayList3.add(new BasicNameValuePair("is_count_down_num", "1"));
        a(str, arrayList, arrayList2, arrayList3, aVar);
    }

    public void a(String str, boolean z, boolean z2, com.shoujiduoduo.util.b.a aVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("mdn", str));
        arrayList.add(new BasicNameValuePair("package_id", z2 ? "135000000000000214309" : "135000000000000214311"));
        a(arrayList, z ? "/package/packageservice/unsubscribebyemp" : "/package/packageservice/unsubscribe", aVar, a.POST);
    }

    public void a(String str, boolean z, String str2, com.shoujiduoduo.util.b.a aVar) {
        if (!this.d.containsKey(str) || (!z ? !this.d.get(str).f1654a : !this.d.get(str).b)) {
            com.shoujiduoduo.base.a.a.a("ChinaTelecomUtils", "queryVipState, 去电信查询");
            this.e = str;
            ArrayList arrayList = new ArrayList();
            arrayList.add(new BasicNameValuePair("mdn", str));
            arrayList.add(new BasicNameValuePair("package_id", z ? "135000000000000214309" : "135000000000000214311"));
            arrayList.add(new BasicNameValuePair("is_count_down_num", "1"));
            a(arrayList, "/package/packageservice/querypackagelist", aVar, a.GET);
            return;
        }
        com.shoujiduoduo.base.a.a.a("ChinaTelecomUtils", "queryVipState, 返回缓存VIP开通状态");
        c.t tVar = new c.t();
        tVar.b = "0000";
        tVar.c = "开通状态";
        tVar.f1614a = "0";
        tVar.d = true;
        aVar.g(tVar);
    }

    public void a(String str, String str2, com.shoujiduoduo.util.b.a aVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("mdn", str));
        arrayList.add(new BasicNameValuePair("info", str2));
        a(arrayList, "/ivr/ivrservice/sendsms", aVar, "&phone=" + str, a.POST);
    }

    public void a(String str, String str2, String str3, com.shoujiduoduo.util.b.a aVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("mdn", str));
        arrayList.add(new BasicNameValuePair("crbt_id", str2));
        arrayList.add(new BasicNameValuePair("type", "1"));
        a(arrayList, "/music/ringduoduocrbtservice/order", aVar, str3, a.POST);
    }

    public void b(String str, com.shoujiduoduo.util.b.a aVar) {
        com.shoujiduoduo.base.a.a.a("ChinaTelecomUtils", "findMdnByImsi");
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("imsi", str));
        a(arrayList, "/depservice/dep/findmdnbyimsi", aVar, a.GET);
    }

    public void a(String str, String str2, String str3, String str4, com.shoujiduoduo.util.b.a aVar) {
        ArrayList arrayList = new ArrayList();
        com.shoujiduoduo.base.a.a.a("ChinaTelecomUtils", "diyClipUpload, ringname:" + str);
        String d2 = f.d(str);
        com.shoujiduoduo.base.a.a.a("ChinaTelecomUtils", "diyClipUpload, filtered ringname:" + d2);
        if (d2.length() > 30) {
            d2 = d2.substring(0, 30);
        }
        com.shoujiduoduo.base.a.a.a("ChinaTelecomUtils", "diyClipUpload, ringname:" + d2 + ", length:" + d2.length());
        arrayList.add(new BasicNameValuePair("ringName", d2));
        arrayList.add(new BasicNameValuePair("userPhone", str2));
        arrayList.add(new BasicNameValuePair("url", str3));
        a(arrayList, "/ringdiy/ringdiyservice/ringClip", aVar, str4, a.POST);
    }

    public void b(String str, String str2, String str3, com.shoujiduoduo.util.b.a aVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("audio_id", str));
        arrayList.add(new BasicNameValuePair("userPhone", str2));
        a(arrayList, "/ringdiy/ringdiyservice/setDiyRing", aVar, str3, a.POST);
    }

    public void c(String str, com.shoujiduoduo.util.b.a aVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair(WBPageConstants.ParamKey.PAGE, "0"));
        arrayList.add(new BasicNameValuePair("per_page", "100"));
        arrayList.add(new BasicNameValuePair("order", "1"));
        arrayList.add(new BasicNameValuePair("userPhone", str));
        a(arrayList, "/ringdiy/ringdiyservice/getDiyRing", aVar, a.POST);
    }

    public void b(String str, String str2, com.shoujiduoduo.util.b.a aVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("audio_id", str));
        arrayList.add(new BasicNameValuePair("userPhone", str2));
        a(arrayList, "/ringdiy/ringdiyservice/queryRingStatus", aVar, a.GET);
    }

    public c.g a(String str, String str2) {
        c.b d2;
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("audio_id", str));
        arrayList.add(new BasicNameValuePair("userPhone", str2));
        try {
            String a2 = e.a(arrayList, "/ringdiy/ringdiyservice/queryRingStatus", ".json");
            if (!(a2 == null || (d2 = d(a2)) == null || !(d2 instanceof c.g))) {
                return (c.g) d2;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return null;
    }

    public void d(String str, com.shoujiduoduo.util.b.a aVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("mdn", str));
        a(arrayList, "/music/crbtservice/sendrandom", aVar, "&phone=" + str, a.POST);
    }

    public c.b c(String str) {
        try {
            ArrayList arrayList = new ArrayList();
            arrayList.add(new BasicNameValuePair("mdn", str));
            arrayList.add(new BasicNameValuePair(WBPageConstants.ParamKey.COUNT, "100"));
            arrayList.add(new BasicNameValuePair(WBPageConstants.ParamKey.PAGE, "0"));
            String a2 = e.a(arrayList, "/music/crbtservice/queryring", ".json");
            if (a2 == null) {
                return null;
            }
            c.b d2 = d(a2);
            a("/music/crbtservice/queryring", d2, "&phone=" + str);
            return d2;
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public void e(String str, com.shoujiduoduo.util.b.a aVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("mdn", str));
        arrayList.add(new BasicNameValuePair(WBPageConstants.ParamKey.COUNT, "100"));
        arrayList.add(new BasicNameValuePair(WBPageConstants.ParamKey.PAGE, "0"));
        a(arrayList, "/music/crbtservice/queryring", aVar, a.GET);
    }

    public void f(String str, com.shoujiduoduo.util.b.a aVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("mdn", str));
        a(arrayList, "/music/crbtservice/querydefaultring", aVar, a.GET);
    }

    public void c(String str, String str2, com.shoujiduoduo.util.b.a aVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("mdn", str));
        arrayList.add(new BasicNameValuePair("crbt_id", str2));
        a(arrayList, "/music/crbtservice/setring", aVar, a.POST);
    }

    public void d(String str, String str2, com.shoujiduoduo.util.b.a aVar) {
        this.e = str;
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("mdn", str));
        arrayList.add(new BasicNameValuePair("random_key", str2));
        a(arrayList, "/music/crbtservice/open", aVar, "&phone=" + str, a.POST);
    }

    public void a(String str, String str2, String str3, String str4, String str5, com.shoujiduoduo.util.b.a aVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("mdn", str));
        arrayList.add(new BasicNameValuePair("to_mdn", str2));
        arrayList.add(new BasicNameValuePair("crbt_id", str3));
        arrayList.add(new BasicNameValuePair("random_key", str4));
        a(arrayList, "/music/crbtservice/present", aVar, str5, a.POST);
    }

    public void e(String str, String str2, com.shoujiduoduo.util.b.a aVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("mdn", str));
        arrayList.add(new BasicNameValuePair("crbt_id", str2));
        a(arrayList, "/ivr/ivrservice/deletecrbtring", aVar, a.POST);
    }

    public void g(String str, com.shoujiduoduo.util.b.a aVar) {
        if (!this.c.containsKey(str) || !this.c.get(str).equals(d.open)) {
            com.shoujiduoduo.base.a.a.a("ChinaTelecomUtils", "openCheck, 去电信查询状态");
            this.e = str;
            ArrayList arrayList = new ArrayList();
            arrayList.add(new BasicNameValuePair("mdn", str));
            a(arrayList, "/music/crbtservice/iscrbtuser", aVar, "&phone=" + str, a.GET);
            return;
        }
        com.shoujiduoduo.base.a.a.a("ChinaTelecomUtils", "openCheck, 返回缓存状态：开通");
        aVar.g(new c.b("0000", "成功"));
    }

    public void h(String str, com.shoujiduoduo.util.b.a aVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("resource_id", str));
        arrayList.add(new BasicNameValuePair("format", "mp3"));
        a(arrayList, "/audio/iaudiomanager/queryringtone", aVar, a.GET);
    }

    public void i(String str, com.shoujiduoduo.util.b.a aVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("resource_id", str));
        arrayList.add(new BasicNameValuePair("format", "mp3"));
        a(arrayList, "/audio/iaudiomanager/querycrbt", aVar, a.GET);
    }

    public void j(String str, com.shoujiduoduo.util.b.a aVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("resource_id", str));
        a(arrayList, "/product/productquery/querycrbtinfo", aVar, a.GET);
    }

    private void a(List<NameValuePair> list, String str, com.shoujiduoduo.util.b.a aVar, a aVar2) {
        a(list, str, aVar, "", aVar2);
    }

    private void a(List<NameValuePair> list, String str, com.shoujiduoduo.util.b.a aVar, String str2, a aVar2) {
        i.a(new c(this, aVar2, list, str, str, str2, aVar));
    }

    private void a(String str, List<NameValuePair> list, List<NameValuePair> list2, List<NameValuePair> list3, com.shoujiduoduo.util.b.a aVar) {
        com.shoujiduoduo.base.a.a.a("ChinaTelecomUtils", "doRequestCailingAndVipCheck");
        i.a(new d(this, str, list, list2, list3, aVar));
    }

    /* access modifiers changed from: private */
    public void a(String str, c.b bVar, String str2) {
        String str3;
        String str4;
        if (str.equals("/depservice/dep/findmdnbyimsi")) {
            HashMap hashMap = new HashMap();
            if (bVar.c()) {
                hashMap.put("res", "success");
                a("ct_findmdnbyimsi", "success", str2);
            } else {
                hashMap.put("res", bVar.toString());
                a("ct_findmdnbyimsi", "fail, " + bVar.toString(), str2);
            }
            com.umeng.analytics.b.a(RingDDApp.c(), "ctcc_find_mdn_by_imsi", hashMap);
        } else if (str.equals("/music/crbtservice/queryring")) {
            HashMap hashMap2 = new HashMap();
            if (bVar.c()) {
                hashMap2.put("res", "success");
                a("ct_box_query", "success", str2);
            } else {
                hashMap2.put("res", bVar.toString());
                a("ct_box_query", "fail, " + bVar.toString(), str2);
            }
            com.umeng.analytics.b.a(RingDDApp.c(), "ctcc_query_ringbox", hashMap2);
        } else if (str.equals("/music/ringduoduocrbtservice/order")) {
            if (bVar.c()) {
                a("ct_vip_order", "success", str2);
            } else {
                a("ct_vip_order", "fail, " + bVar.toString(), str2);
            }
        } else if (str.equals("/ringdiy/ringdiyservice/ringClip")) {
            if (bVar.c()) {
                a("ct_diy_clip_upload", "success", str2);
            } else {
                a("ct_diy_clip_upload", "fail, " + bVar.toString(), str2);
            }
        } else if (str.equals("/ringdiy/ringdiyservice/setDiyRing")) {
            if (bVar.c()) {
                a("ct_set_diy_ring", "success", str2);
            } else {
                a("ct_set_diy_ring", "fail, " + bVar.toString(), str2);
            }
        } else if (str.equals("/package/packageservice/emplanunched")) {
            if (bVar.c()) {
                a("ct_launch_emp", "success", str2);
            } else {
                a("ct_launch_emp", "fail, " + bVar.toString(), str2);
            }
        } else if (str.equals("/package/packageservice/subscribe") || str.equals("/package/packageservice/subscribebyemp")) {
            if (bVar.c()) {
                a(this.f, "success", str2);
            } else {
                a(this.f, "fail, " + bVar.toString(), str2);
            }
        } else if (str.equals("/music/crbtservice/open")) {
            if (bVar.c() || bVar.a().equals("0002") || bVar.a().equals("9028") || bVar.a().equals("0764") || bVar.a().equals("0501") || bVar.a().equals("02000000")) {
                this.c.put(this.e, d.wait_open);
            }
            if (bVar.c()) {
                a("ct_open_cailing", "success", str2);
            } else {
                a("ct_open_cailing", "fail," + bVar.toString(), str2);
            }
        } else if (str.equals("/music/crbtservice/iscrbtuser")) {
            HashMap hashMap3 = new HashMap();
            if (bVar.c()) {
                hashMap3.put("res", "success");
                this.c.put(this.e, d.open);
                a("ct_check_cailing", "success", str2);
            } else {
                hashMap3.put("res", bVar.toString());
                a("ct_check_cailing", "fail, " + bVar.toString(), str2);
            }
            com.umeng.analytics.b.a(RingDDApp.c(), "ctcc_open_check", hashMap3);
        } else if (str.equals("/package/packageservice/querypackagelist")) {
            if (!bVar.c()) {
                a("ct_vip_query", "fail, " + bVar.toString(), str2);
            } else if (bVar instanceof c.t) {
                if (((c.t) bVar).d) {
                    String str5 = "&status=" + "open";
                    if (this.d.get(this.e) != null) {
                        this.d.get(this.e).f1654a = true;
                        str4 = str5;
                    } else {
                        this.d.put(this.e, new c(true, false));
                        str4 = str5;
                    }
                } else {
                    String str6 = "&status=" + "close";
                    if (this.d.get(this.e) != null) {
                        this.d.get(this.e).f1654a = false;
                        str4 = str6;
                    } else {
                        this.d.put(this.e, new c(false, false));
                        str4 = str6;
                    }
                }
                a("ct_vip_query", "success", str4 + str2);
            }
        } else if (str.equals("query_diy_state")) {
            if (!bVar.c()) {
                a("ct_diy_query", "fail, " + bVar.toString(), str2);
            } else if (bVar instanceof c.t) {
                if (((c.t) bVar).d) {
                    String str7 = "&status=" + "open";
                    if (this.d.get(this.e) != null) {
                        this.d.get(this.e).b = true;
                        str3 = str7;
                    } else {
                        this.d.put(this.e, new c(false, true));
                        str3 = str7;
                    }
                } else {
                    String str8 = "&status=" + "close";
                    if (this.d.get(this.e) != null) {
                        this.d.get(this.e).b = false;
                        str3 = str8;
                    } else {
                        this.d.put(this.e, new c(false, false));
                        str3 = str8;
                    }
                }
                a("ct_diy_query", "success", str3 + str2);
            }
        } else if (str.equals("/package/packageservice/ringduoduosubscribebyemp")) {
            if (bVar.c() || bVar.a().equals("c0002") || bVar.a().equals("c9028") || bVar.a().equals("c0764") || bVar.a().equals("c02000000")) {
                this.c.put(this.e, d.wait_open);
            }
            if (bVar.c()) {
                a(this.f, "success", str2);
            } else {
                a(this.f, "fail, " + bVar.toString(), str2);
            }
        } else if (str.equals("/music/crbtservice/sendrandom")) {
            if (bVar.c()) {
                a("ct_sendrandom", "success", str2);
            } else {
                a("ct_sendrandom", "fail," + bVar.toString(), str2);
            }
        } else if (str.equals("/music/crbtservice/order")) {
            if (bVar.c()) {
                a("ct_buy", "success", str2);
            } else {
                a("ct_buy", "fail," + bVar.toString(), str2);
            }
        } else if (str.equals("/ivr/ivrservice/sendsms")) {
            if (bVar.c()) {
                a("ct_send_sms", "success", str2);
            } else {
                a("ct_send_sms", "fail," + bVar.toString(), str2);
            }
        } else if (!str.equals("/music/crbtservice/present")) {
        } else {
            if (bVar.c()) {
                a("ct_give", "success", str2);
            } else {
                a("ct_give", "fail," + bVar.toString(), str2);
            }
        }
    }

    private void a(String str, String str2, String str3) {
        w.a("ct:" + str, str2, str3);
    }
}
