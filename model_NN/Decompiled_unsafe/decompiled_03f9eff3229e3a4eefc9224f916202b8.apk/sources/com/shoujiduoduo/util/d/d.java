package com.shoujiduoduo.util.d;

import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.d.b;
import java.util.List;

/* compiled from: ChinaTelecomUtils */
class d implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f1657a;
    final /* synthetic */ List b;
    final /* synthetic */ List c;
    final /* synthetic */ List d;
    final /* synthetic */ a e;
    final /* synthetic */ b f;

    d(b bVar, String str, List list, List list2, List list3, a aVar) {
        this.f = bVar;
        this.f1657a = str;
        this.b = list;
        this.c = list2;
        this.d = list3;
        this.e = aVar;
    }

    public void run() {
        try {
            c.d dVar = new c.d();
            dVar.b = "0000";
            dVar.c = "查询成功";
            if (!this.f.c.containsKey(this.f1657a) || !((b.d) this.f.c.get(this.f1657a)).equals(b.d.open)) {
                com.shoujiduoduo.base.a.a.a("ChinaTelecomUtils", "doRequestCailingAndVipCheck, 去电信查询状态");
                String a2 = e.a(this.b, "/music/crbtservice/iscrbtuser", ".json");
                if (a2 != null) {
                    c.b a3 = this.f.d(a2);
                    if (a3 != null) {
                        dVar.f1597a = a3;
                    } else {
                        dVar.f1597a = b.f1651a;
                    }
                } else {
                    dVar.f1597a = b.f1651a;
                }
                this.f.a("/music/crbtservice/iscrbtuser", dVar.f1597a, "&phone=" + this.f1657a);
            } else {
                com.shoujiduoduo.base.a.a.a("ChinaTelecomUtils", "openCheck, 返回缓存状态：开通");
                dVar.f1597a = new c.b("0000", "成功");
            }
            if (!this.f.d.containsKey(this.f1657a) || !((b.c) this.f.d.get(this.f1657a)).f1654a) {
                com.shoujiduoduo.base.a.a.a("ChinaTelecomUtils", "queryVipState, 去电信查询");
                String a4 = e.a(this.c, "/package/packageservice/querypackagelist", ".json");
                if (a4 != null) {
                    c.b a5 = this.f.d(a4);
                    if (a5 != null) {
                        dVar.d = a5;
                    } else {
                        dVar.d = b.f1651a;
                    }
                } else {
                    dVar.d = b.f1651a;
                }
                this.f.a("/package/packageservice/querypackagelist", dVar.d, "&phone=" + this.f1657a);
            } else {
                com.shoujiduoduo.base.a.a.a("ChinaTelecomUtils", "queryVipState, 返回缓存VIP开通状态");
                c.t tVar = new c.t();
                tVar.b = "0000";
                tVar.c = "开通状态";
                tVar.d = true;
                tVar.f1614a = "0";
                dVar.d = tVar;
            }
            if (!this.f.d.containsKey(this.f1657a) || !((b.c) this.f.d.get(this.f1657a)).b) {
                com.shoujiduoduo.base.a.a.a("ChinaTelecomUtils", "queryDiyVipState, 去电信查询");
                String a6 = e.a(this.d, "/package/packageservice/querypackagelist", ".json");
                if (a6 != null) {
                    c.b a7 = this.f.d(a6);
                    if (a7 != null) {
                        dVar.e = a7;
                    } else {
                        dVar.e = b.f1651a;
                    }
                } else {
                    dVar.e = b.f1651a;
                }
                this.f.a("query_diy_state", dVar.e, "&phone=" + this.f1657a);
            } else {
                com.shoujiduoduo.base.a.a.a("ChinaTelecomUtils", "queryDiyVipState, 返回缓存VIP开通状态");
                c.t tVar2 = new c.t();
                tVar2.b = "0000";
                tVar2.c = "开通状态";
                tVar2.d = true;
                tVar2.f1614a = "0";
                dVar.e = tVar2;
            }
            this.e.g(dVar);
        } catch (Exception e2) {
            e2.printStackTrace();
            this.e.g(b.b);
        }
    }
}
