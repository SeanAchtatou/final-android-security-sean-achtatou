package com.shoujiduoduo.util.d;

import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.d.b;
import java.util.List;

/* compiled from: ChinaTelecomUtils */
class c implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ b.a f1656a;
    final /* synthetic */ List b;
    final /* synthetic */ String c;
    final /* synthetic */ String d;
    final /* synthetic */ String e;
    final /* synthetic */ a f;
    final /* synthetic */ b g;

    c(b bVar, b.a aVar, List list, String str, String str2, String str3, a aVar2) {
        this.g = bVar;
        this.f1656a = aVar;
        this.b = list;
        this.c = str;
        this.d = str2;
        this.e = str3;
        this.f = aVar2;
    }

    public void run() {
        c.b c2;
        String str = null;
        try {
            if (this.f1656a.equals(b.a.POST)) {
                str = e.b(this.b, this.c, ".json");
            } else if (this.f1656a.equals(b.a.GET)) {
                str = e.a(this.b, this.c, ".json");
            }
            if (str != null) {
                c2 = this.g.d(str);
                if (c2 == null) {
                    c2 = b.f1651a;
                }
            } else {
                c2 = b.b;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            c2 = b.b;
        }
        this.g.a(this.d, c2, this.e);
        this.f.g(c2);
    }
}
