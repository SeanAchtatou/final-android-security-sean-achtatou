package com.shoujiduoduo.util;

import android.content.ContentValues;
import android.content.Context;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.widget.Toast;
import com.shoujiduoduo.a.a.b;
import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.a.c.h;
import com.shoujiduoduo.base.bean.MakeRingData;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.base.bean.y;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.HashMap;

/* compiled from: SetRingTone */
public class aq implements h {

    /* renamed from: a  reason: collision with root package name */
    private static final String f1571a = aq.class.getSimpleName();
    private static aq j = null;
    /* access modifiers changed from: private */
    public RingData b;
    private String c;
    private String d;
    /* access modifiers changed from: private */
    public Context e;
    private HashMap<String, Integer> f = new HashMap<>();
    /* access modifiers changed from: private */
    public boolean g = false;
    /* access modifiers changed from: private */
    public int h;
    private a i = new a(this);
    private String k;

    private aq(Context context) {
        this.e = context;
    }

    public static synchronized aq a(Context context) {
        aq aqVar;
        synchronized (aq.class) {
            if (j == null) {
                j = new aq(context);
                p.a(context).a(j);
                com.shoujiduoduo.base.a.a.a(f1571a, "getInstance: SetRingTone created!");
            }
            aqVar = j;
        }
        return aqVar;
    }

    public static aq a() {
        if (j != null) {
            return j;
        }
        return null;
    }

    /* compiled from: SetRingTone */
    private static class a extends Handler {

        /* renamed from: a  reason: collision with root package name */
        private WeakReference<aq> f1572a;

        public a(aq aqVar) {
            this.f1572a = new WeakReference<>(aqVar);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.util.aq.a(com.shoujiduoduo.util.aq, boolean):boolean
         arg types: [com.shoujiduoduo.util.aq, int]
         candidates:
          com.shoujiduoduo.util.aq.a(com.shoujiduoduo.base.bean.RingData, int):void
          com.shoujiduoduo.util.aq.a(java.lang.String, android.net.Uri):boolean
          com.shoujiduoduo.util.aq.a(com.shoujiduoduo.base.bean.y, int):void
          com.shoujiduoduo.a.c.h.a(com.shoujiduoduo.base.bean.y, int):void
          com.shoujiduoduo.util.aq.a(com.shoujiduoduo.util.aq, boolean):boolean */
        public void handleMessage(Message message) {
            super.handleMessage(message);
            aq aqVar = this.f1572a.get();
            if (aqVar != null) {
                switch (message.what) {
                    case 1001:
                        Toast.makeText(aqVar.e, aqVar.e.getResources().getText(R.string.set_ring_error_message), 1).show();
                        return;
                    case 1002:
                        Toast.makeText(aqVar.e, (String) message.obj, 1).show();
                        return;
                    case 2000:
                        y yVar = (y) message.obj;
                        if (aqVar.b != null && aqVar.g && yVar.c == aqVar.b.k()) {
                            aqVar.a(aqVar.h);
                            boolean unused = aqVar.g = false;
                            return;
                        }
                        return;
                    default:
                        return;
                }
            }
        }
    }

    public void a(HashMap<String, Integer> hashMap, RingData ringData, String str, String str2) {
        this.f.clear();
        this.f.putAll(hashMap);
        this.g = false;
        this.b = ringData;
        this.c = str;
        this.d = str2;
        y c2 = c();
        if (c2 == null) {
            com.shoujiduoduo.base.a.a.c(f1571a, "不应该为空，找漏洞吧！");
            this.i.sendEmptyMessage(1001);
        } else if (c2.d < c2.e || c2.e < 0) {
            this.g = true;
            this.h = 0;
            Message message = new Message();
            message.what = 1002;
            message.obj = this.e.getResources().getString(R.string.set_ring_not_down_finish_hint) + this.e.getResources().getString(R.string.set_ring_contact);
            this.i.sendMessage(message);
        } else {
            a(8);
        }
    }

    public void a(int i2, RingData ringData, String str, String str2) {
        this.g = false;
        this.b = ringData;
        this.c = str;
        this.d = str2;
        y c2 = c();
        if (c2 == null) {
            com.shoujiduoduo.base.a.a.c(f1571a, "不应该为空，找漏洞吧！");
            this.i.sendEmptyMessage(1001);
            return;
        }
        com.shoujiduoduo.base.a.a.a(f1571a, "type = " + i2);
        if (c2.d < c2.e || c2.e < 0) {
            com.shoujiduoduo.base.a.a.a(f1571a, "铃声尚未下载完成，下载完之后再设置铃声");
            this.g = true;
            this.h = i2;
            Message message = new Message();
            message.what = 1002;
            String string = this.e.getResources().getString(R.string.set_ring_not_down_finish_hint);
            String str3 = "";
            if (!((i2 & 1) == 0 && (i2 & 32) == 0)) {
                str3 = str3 + this.e.getResources().getString(R.string.set_ring_incoming_call);
            }
            if (!((i2 & 2) == 0 && (i2 & 64) == 0 && (i2 & 128) == 0)) {
                str3 = str3 + this.e.getResources().getString(R.string.set_ring_message);
            }
            if ((i2 & 4) != 0) {
                str3 = str3 + this.e.getResources().getString(R.string.set_ring_alarm);
            }
            message.obj = string + str3;
            this.i.sendMessage(message);
            return;
        }
        a(i2);
    }

    private boolean a(Uri uri) {
        for (String next : this.f.keySet()) {
            if (this.f.get(next).intValue() == 0) {
                a(next);
            } else if (this.f.get(next).intValue() == 1) {
                a(next, uri);
            }
        }
        return true;
    }

    private boolean a(String str, Uri uri) {
        try {
            Uri withAppendedPath = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_URI, str);
            ContentValues contentValues = new ContentValues();
            contentValues.put("custom_ringtone", uri.toString());
            this.e.getContentResolver().update(withAppendedPath, contentValues, null, null);
            return true;
        } catch (Exception e2) {
            e2.printStackTrace();
            return false;
        }
    }

    private boolean a(String str) {
        try {
            Uri withAppendedPath = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_URI, str);
            ContentValues contentValues = new ContentValues();
            contentValues.put("custom_ringtone", RingtoneManager.getActualDefaultRingtoneUri(this.e, 1).toString());
            this.e.getContentResolver().update(withAppendedPath, contentValues, null, null);
            return true;
        } catch (Exception e2) {
            e2.printStackTrace();
            return false;
        }
    }

    private y c() {
        if (!(this.b instanceof MakeRingData)) {
            return p.a(this.e).b(this.b.k());
        }
        MakeRingData makeRingData = (MakeRingData) this.b;
        File file = new File(makeRingData.m);
        if (file.exists()) {
            y yVar = new y(this.b.e, this.b.f, 0, (int) file.length(), (int) file.length(), 128000, t.b(makeRingData.m), "");
            yVar.e(makeRingData.m);
            return yVar;
        } else if (!makeRingData.g.equals("")) {
            return p.a(this.e).b(this.b.k());
        } else {
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void} */
    public static boolean a(int i2, String str, String str2, String str3, String str4) {
        File file = new File(str);
        try {
            FileInputStream fileInputStream = new FileInputStream(str);
            int available = fileInputStream.available();
            fileInputStream.close();
            com.shoujiduoduo.base.a.a.a(f1571a, "ringtone path: " + file.getAbsolutePath());
            String b2 = t.b(str);
            ContentValues contentValues = new ContentValues();
            if (b2.equals("mp3")) {
                contentValues.put("mime_type", "audio/mp3");
            } else if (b2.equals("aac")) {
                contentValues.put("mime_type", "audio/aac");
            } else {
                com.shoujiduoduo.base.a.a.c(f1571a, "format data not support");
                return false;
            }
            contentValues.put("_data", file.getAbsolutePath());
            contentValues.put("title", str2);
            contentValues.put("_size", Integer.valueOf(available));
            contentValues.put("artist", str3);
            contentValues.put("duration", str4);
            contentValues.put("is_ringtone", (Boolean) true);
            contentValues.put("is_notification", (Boolean) true);
            contentValues.put("is_alarm", (Boolean) true);
            contentValues.put("is_music", (Boolean) false);
            Uri contentUriForPath = MediaStore.Audio.Media.getContentUriForPath(file.getAbsolutePath());
            if (contentUriForPath == null) {
                com.shoujiduoduo.base.a.a.a(f1571a, "setringtone:uri is NULL!");
                return false;
            }
            com.shoujiduoduo.base.a.a.a(f1571a, "uri encoded = " + contentUriForPath.toString());
            com.shoujiduoduo.base.a.a.a(f1571a, "uri decoded = " + contentUriForPath.getPath());
            try {
                RingDDApp.c().getContentResolver().delete(contentUriForPath, "_data=\"" + file.getAbsolutePath() + "\"", null);
            } catch (Exception e2) {
            }
            try {
                Uri insert = RingDDApp.c().getContentResolver().insert(contentUriForPath, contentValues);
                if (insert == null) {
                    com.shoujiduoduo.base.a.a.a(f1571a, "setringtone: newURI is NULL!");
                    return false;
                }
                RingtoneManager.setActualDefaultRingtoneUri(RingDDApp.c(), i2, insert);
                return true;
            } catch (Exception e3) {
                return false;
            }
        } catch (IOException e4) {
            e4.printStackTrace();
            com.shoujiduoduo.base.a.a.c(f1571a, "ringtone file missing!");
            return false;
        }
    }

    public boolean a(int i2) {
        com.shoujiduoduo.base.a.a.a(f1571a, "in setRing");
        Uri d2 = d();
        if (d2 == null) {
            return false;
        }
        if ((i2 & 1) != 0) {
            a(this.b, 1);
            as.c(this.e, "user_ring_phone_select", this.b.g);
            as.c(this.e, "user_ring_phone_select_name", this.b.e);
            if (v.b()) {
                v.a(this.e, d2, i2);
            } else {
                aw.a(this.e, d2, this.k);
                RingtoneManager.setActualDefaultRingtoneUri(this.e, 1, d2);
            }
        }
        if ((i2 & 2) != 0) {
            a(this.b, 2);
            as.c(this.e, "user_ring_notification_select", this.b.g);
            as.c(this.e, "user_ring_notification_select_name", this.b.e);
            if (v.b()) {
                v.a(this.e, d2, i2);
            } else {
                aw.b(this.e, d2, this.k);
                if (!Build.BRAND.equalsIgnoreCase("OPPO")) {
                    RingtoneManager.setActualDefaultRingtoneUri(this.e, 2, d2);
                }
            }
        }
        if ((i2 & 4) != 0) {
            a(this.b, 3);
            as.c(this.e, "user_ring_alarm_select", this.b.g);
            as.c(this.e, "user_ring_alarm_select_name", this.b.e);
            if (v.b()) {
                v.a(this.e, d2, i2);
            } else {
                aw.c(this.e, d2, this.k);
                RingtoneManager.setActualDefaultRingtoneUri(this.e, 4, d2);
            }
        }
        if ((i2 & 8) != 0) {
            a(this.b, 4);
            a(d2);
        }
        x.a().b(b.OBSERVER_RING_CHANGE, new ar(this, i2));
        Message message = new Message();
        message.what = 1002;
        String string = this.e.getResources().getString(R.string.set_ring_hint);
        if ((i2 & 1) != 0) {
            string = (string + this.e.getResources().getString(R.string.set_ring_incoming_call)) + " ";
        }
        if ((i2 & 2) != 0) {
            string = (string + this.e.getResources().getString(R.string.set_ring_message)) + " ";
        }
        if ((i2 & 4) != 0) {
            string = (string + this.e.getResources().getString(R.string.set_ring_alarm)) + " ";
        }
        if ((i2 & 8) != 0) {
            string = string + this.e.getResources().getString(R.string.set_ring_contact);
        }
        message.obj = string;
        this.i.sendMessage(message);
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void} */
    private Uri d() {
        y c2 = c();
        if (c2 == null) {
            com.shoujiduoduo.base.a.a.c(f1571a, "data is null");
            this.i.sendEmptyMessage(1001);
            return null;
        }
        String l = c2.l();
        this.k = l;
        File file = new File(l);
        try {
            FileInputStream fileInputStream = new FileInputStream(l);
            int available = fileInputStream.available();
            fileInputStream.close();
            com.shoujiduoduo.base.a.a.a(f1571a, "ringtone path: " + file.getAbsolutePath());
            ContentValues contentValues = new ContentValues();
            if (c2.g == null || c2.g.length() == 0) {
                contentValues.put("mime_type", "audio/mp3");
            } else if (c2.g.equalsIgnoreCase("mp3")) {
                contentValues.put("mime_type", "audio/mp3");
            } else if (c2.g.equalsIgnoreCase("aac")) {
                contentValues.put("mime_type", "audio/aac");
            } else {
                com.shoujiduoduo.base.a.a.c(f1571a, "format data not support");
                this.i.sendEmptyMessage(1001);
                return null;
            }
            contentValues.put("_data", file.getAbsolutePath());
            contentValues.put("title", this.b.e);
            contentValues.put("_size", Integer.valueOf(available));
            contentValues.put("artist", this.b.f);
            contentValues.put("duration", Double.valueOf(((double) this.b.j) * 1000.0d));
            contentValues.put("is_ringtone", (Boolean) true);
            contentValues.put("is_notification", (Boolean) true);
            contentValues.put("is_alarm", (Boolean) true);
            contentValues.put("is_music", (Boolean) false);
            Uri contentUriForPath = MediaStore.Audio.Media.getContentUriForPath(file.getAbsolutePath());
            if (contentUriForPath == null) {
                com.shoujiduoduo.base.a.a.a(f1571a, "setringtone:uri is NULL!");
                this.i.sendEmptyMessage(1001);
                return null;
            }
            com.shoujiduoduo.base.a.a.a(f1571a, "uri encoded = " + contentUriForPath.toString());
            com.shoujiduoduo.base.a.a.a(f1571a, "uri decoded = " + contentUriForPath.getPath());
            try {
                this.e.getContentResolver().delete(contentUriForPath, "_data=\"" + file.getAbsolutePath() + "\"", null);
            } catch (Exception e2) {
                f.c("delete media uri error! uri = " + contentUriForPath.toString() + "\n" + "path = " + file.getAbsolutePath() + "\n" + com.shoujiduoduo.base.a.b.a(e2));
            }
            try {
                Uri insert = this.e.getContentResolver().insert(contentUriForPath, contentValues);
                if (insert != null) {
                    return insert;
                }
                com.shoujiduoduo.base.a.a.a(f1571a, "setringtone: newURI is NULL!");
                this.i.sendEmptyMessage(1001);
                return null;
            } catch (Exception e3) {
                f.c("insert media uri error! uri = " + contentUriForPath.toString() + "\n" + "path = " + file.getAbsolutePath() + "\n" + com.shoujiduoduo.base.a.b.a(e3));
                this.i.sendEmptyMessage(1001);
                return null;
            }
        } catch (IOException e4) {
            e4.printStackTrace();
            com.shoujiduoduo.base.a.a.c(f1571a, "ringtone file missing!");
            this.i.sendEmptyMessage(1001);
            return null;
        }
    }

    private void a(RingData ringData, int i2) {
        w.a(ringData.g, i2, this.c, this.d, "&cucid=" + ringData.A);
    }

    public void a(y yVar) {
    }

    public void a(y yVar, int i2) {
    }

    public void b(y yVar) {
        this.i.sendMessage(this.i.obtainMessage(2000, yVar));
    }

    public void c(y yVar) {
    }

    public void d(y yVar) {
    }

    public void b() {
        if (this.i != null) {
            this.i.removeCallbacksAndMessages(null);
        }
        j = null;
    }
}
