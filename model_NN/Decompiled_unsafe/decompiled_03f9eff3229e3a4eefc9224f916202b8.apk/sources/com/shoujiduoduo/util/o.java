package com.shoujiduoduo.util;

import cn.banshenggua.aichang.room.message.SocketMessage;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.util.m;
import com.umeng.analytics.b;
import java.util.HashMap;

/* compiled from: DnsDetector */
class o implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f1681a;
    final /* synthetic */ m b;

    o(m mVar, String str) {
        this.b = mVar;
        this.f1681a = str;
    }

    /* JADX INFO: finally extract failed */
    public void run() {
        m.a aVar = new m.a(this.b.f);
        aVar.start();
        try {
            aVar.join((long) this.b.j);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        HashMap hashMap = new HashMap();
        if (aVar.a() == null || this.b.a(this.b.h, false)) {
            HashMap hashMap2 = new HashMap();
            hashMap2.put("network type", NetworkStateUtil.b());
            b.a(RingDDApp.c(), "duoduo_server_dns_error_new", hashMap2);
            a.e("dnsdetector", "duoduo server dns not work:" + this.b.f + ", use ip:" + this.f1681a);
            this.b.n.lock();
            try {
                String unused = m.i = this.f1681a;
                this.b.n.unlock();
                hashMap.put(SocketMessage.MSG_RESULE_KEY, "fail");
            } catch (Throwable th) {
                this.b.n.unlock();
                throw th;
            }
        } else {
            hashMap.put(SocketMessage.MSG_RESULE_KEY, "success");
            a.a("dnsdetector", "duoduo server dns available:" + this.b.f + ", ip:" + aVar.a());
        }
        b.a(RingDDApp.c(), "duoduo_server_dns_check", hashMap);
        a.a("dnsdetector", "detect duoduo server end");
    }
}
