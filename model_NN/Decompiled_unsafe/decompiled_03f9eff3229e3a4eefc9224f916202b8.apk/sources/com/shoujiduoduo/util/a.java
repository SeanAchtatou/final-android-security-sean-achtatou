package com.shoujiduoduo.util;

import android.os.Build;
import android.text.TextUtils;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.umeng.analytics.b;

/* compiled from: AdUtils */
public class a {

    /* renamed from: a  reason: collision with root package name */
    private static Boolean f1547a;

    public static boolean a() {
        return true;
    }

    public static boolean b() {
        if (f1547a != null) {
            return f1547a.booleanValue();
        }
        if (am.a().b("aichang_enable")) {
            f1547a = true;
            return true;
        }
        f1547a = false;
        return false;
    }

    public static boolean a(boolean z, boolean z2) {
        if (!k()) {
            b.c(RingDDApp.c(), z ? "duomob_splash_ad_switch_duoduo_splash" : "duomob_splash_ad_switch");
        }
        return false;
    }

    public static boolean a(boolean z) {
        boolean z2;
        if (k()) {
            return false;
        }
        String c = b.c(RingDDApp.c(), "start_ad_baidu_disable_src");
        if ("false".equals(b.c(RingDDApp.c(), z ? "start_ad_baidu_duoduo_splash" : "start_ad_baidu")) || c.contains(f.l()) || !i()) {
            z2 = false;
        } else {
            z2 = true;
        }
        return z2;
    }

    public static boolean b(boolean z) {
        boolean z2;
        if (k()) {
            return false;
        }
        String c = b.c(RingDDApp.c(), "start_ad_gdt_disable_src");
        if ("false".equals(b.c(RingDDApp.c(), z ? "start_ad_gdt_duoduo_splash" : "start_ad_gdt")) || c.contains(f.l())) {
            z2 = false;
        } else {
            z2 = true;
        }
        return z2;
    }

    public static boolean c() {
        if (k() || RingDDApp.b) {
            return false;
        }
        b.c(RingDDApp.c(), "duomob_quit_ad_switch");
        if ("qq".equals(f.l())) {
        }
        return true;
    }

    public static boolean d() {
        if (k()) {
            return false;
        }
        b.c(RingDDApp.c(), "duomob_feed_ad_switch");
        if ("duoduo".contains("360")) {
            return true;
        }
        String a2 = am.a().a("feed_ad_channel");
        if (!am.a().a("feed_ad_enable").equals("true")) {
            return false;
        }
        if (a2.contains(f.l()) || a2.equals("all")) {
            return true;
        }
        return false;
    }

    private static boolean k() {
        int a2 = as.a(RingDDApp.c(), "user_vip_type", 0);
        if (as.a(RingDDApp.c(), "user_loginStatus", 0) != 1 || a2 == 0) {
            return false;
        }
        return true;
    }

    public static synchronized boolean e() {
        boolean z = false;
        synchronized (a.class) {
            if (!k()) {
                if (!RingDDApp.b) {
                    if ("qq".equals(f.l())) {
                    }
                    z = true;
                }
            }
        }
        return z;
    }

    public static synchronized boolean f() {
        boolean z;
        synchronized (a.class) {
            if (RingDDApp.b) {
                z = false;
            } else {
                z = true;
            }
        }
        return z;
    }

    public static synchronized boolean g() {
        boolean z;
        synchronized (a.class) {
            String a2 = am.a().a("tencent_ad_duration");
            if (TextUtils.isEmpty(a2) || "0".equalsIgnoreCase(a2)) {
                z = false;
            } else {
                z = true;
            }
        }
        return z;
    }

    public static synchronized boolean h() {
        boolean z;
        synchronized (a.class) {
            String a2 = am.a().a("taobao_ad_duration");
            if (TextUtils.isEmpty(a2) || "0".equalsIgnoreCase(a2)) {
                z = false;
            } else {
                z = true;
            }
        }
        return z;
    }

    public static synchronized boolean i() {
        boolean z = false;
        synchronized (a.class) {
            if (Build.VERSION.SDK_INT > 10) {
                String a2 = am.a().a("baidu_ad_duration");
                if (!TextUtils.isEmpty(a2) && !"0".equalsIgnoreCase(a2)) {
                    z = true;
                }
            }
        }
        return z;
    }

    public static synchronized boolean j() {
        boolean z;
        synchronized (a.class) {
            String a2 = am.a().a("duoduo_ad_duration");
            if (TextUtils.isEmpty(a2) || "0".equalsIgnoreCase(a2)) {
                z = false;
            } else {
                z = true;
            }
        }
        return z;
    }
}
