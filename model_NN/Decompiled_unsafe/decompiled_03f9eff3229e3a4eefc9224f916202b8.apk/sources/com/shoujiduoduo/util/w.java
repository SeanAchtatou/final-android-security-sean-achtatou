package com.shoujiduoduo.util;

import android.net.NetworkInfo;
import android.text.TextUtils;
import cn.banshenggua.aichang.utils.Constants;
import com.duoduo.cmmusic.init.GetAppInfo;
import com.duoduo.cmmusic.init.GetAppInfoInterface;
import com.shoujiduoduo.a.c.g;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.base.bean.MakeRingData;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.base.bean.x;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.umeng.analytics.b;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Locale;

/* compiled from: HttpRequest */
public class w {

    /* renamed from: a  reason: collision with root package name */
    public static final NativeDES f1693a = new NativeDES();
    /* access modifiers changed from: private */
    public static final String b = w.class.getSimpleName();
    private static final String c = f.a();
    private static final String d = f.q();
    private static final String e = f.p();
    private static final String f = f.t();
    private static final String g = f.c().replaceAll(":", "");
    private static final String h = URLEncoder.encode(f.j());
    /* access modifiers changed from: private */
    public static final String i;

    static {
        String str;
        StringBuilder append = new StringBuilder().append("user=").append(c).append("&prod=").append(d).append("&isrc=").append(e).append("&mac=");
        if (TextUtils.isEmpty(g.trim())) {
            str = "unknown_mac";
        } else {
            str = g;
        }
        i = append.append(str).append("&dev=").append(h).append("&vc=").append(f.r()).append("&loc=").append(Locale.getDefault().getCountry()).append("&sp=").append(f.u().toString()).toString();
    }

    public static int a() {
        NetworkInfo d2 = f.d();
        if (d2 == null || !d2.isAvailable() || !d2.isConnected()) {
            return 0;
        }
        int type = d2.getType();
        if (type == 1) {
            return 1;
        }
        if (type != 0) {
            return 2;
        }
        switch (f.g()) {
            case 0:
                a.a(b, "mobile: UNKNOWN");
                return 2;
            case 1:
                a.a(b, "mobile: GPRS");
                return 2;
            case 2:
                a.a(b, "mobile: EDGE");
                return 2;
            case 3:
                a.a(b, "mobile: UMTS");
                return 2;
            case 4:
                a.a(b, "mobile: CDMA");
                return 3;
            case 5:
                a.a(b, "mobile: EVDO_0");
                return 3;
            case 6:
                a.a(b, "mobile: EVDO_A");
                return 3;
            case 7:
                a.a(b, "mobile: 1xRTT");
                return 2;
            case 8:
                a.a(b, "mobile: HSDPA");
                return 3;
            case 9:
                a.a(b, "mobile: HSUPA");
                return 3;
            case 10:
                a.a(b, "mobile: HSPA");
                return 3;
            case 11:
                a.a(b, "mobile: IDEN");
                return 2;
            case 12:
                a.a(b, "mobile: EVDO_B");
                return 3;
            case 13:
                a.a(b, "mobile: LTE");
                return 3;
            case 14:
                a.a(b, "mobile: EHRPD");
                return 3;
            case 15:
                a.a(b, "mobile: HSPAP");
                return 3;
            default:
                return 2;
        }
    }

    public static byte[] b() {
        String str = i + "&type=getconfig";
        a.a(b, "httpGetConfig: " + str);
        String e2 = e(str);
        StringBuilder sb = new StringBuilder();
        sb.append("http://117.121.41.242/ring_enc.php?q=").append(e2);
        a.a(b, "realRequest: url = " + sb.toString());
        byte[] b2 = bj.b(sb.toString());
        if (b2 != null) {
            return b2;
        }
        a.a(b, "retry request");
        return j(e2);
    }

    public static String a(String str) {
        String str2 = "http://www.shoujiduoduo.com/ringv1/suggest.php?";
        try {
            str2 = str2 + "word=" + URLEncoder.encode(str, "UTF-8");
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
        }
        byte[] b2 = bj.b(str2 + "&count=10");
        if (b2 == null) {
            return null;
        }
        return new String(b2);
    }

    /* access modifiers changed from: private */
    public static byte[] j(String str) {
        String c2 = b.c(RingDDApp.c(), "duoduo_dns");
        if (c2 == null || c2.equals("")) {
            return null;
        }
        String str2 = l(c2) + str;
        a.a(b, "retryRequest, url:" + str2);
        return bj.b(str2);
    }

    public static byte[] a(String str, boolean z, String str2) {
        String imsi = GetAppInfoInterface.getIMSI(RingDDApp.c());
        if (av.c(imsi)) {
            imsi = GetAppInfo.getIMSIbyFile(RingDDApp.c());
        }
        StringBuilder sb = new StringBuilder();
        sb.append(i).append("&type=cailingurl&mode=").append(str2).append("&cid=").append(str).append("&nocache=").append(z ? "1" : "0").append(TextUtils.isEmpty(imsi) ? "" : "&imsi=" + imsi);
        a.a(b, "httpGetCMCailingUrl: " + sb.toString());
        return k(sb.toString());
    }

    public static x a(String str, boolean z) {
        StringBuilder sb = new StringBuilder();
        sb.append(i).append("&type=cucailingurl").append("&cucid=").append(str).append("&nocache=").append(z ? "1" : "0");
        a.a(b, "httpGetCUCailingUrl, paraString:" + sb.toString());
        byte[] k = k(sb.toString());
        if (k == null) {
            return null;
        }
        try {
            String[] split = new String(k, "UTF-8").split("\t", 0);
            if (split.length != 3) {
                return null;
            }
            for (int i2 = 0; i2 < split.length; i2++) {
                a.a(b, "result " + i2 + ": " + split[i2]);
            }
            a.a(b, "httpGetCUCailingUrl: url =" + split[2]);
            a.a(b, "httpGetCUCailingUrl: bitrate =" + split[0]);
            a.a(b, "httpGetCUCailingUrl: format =" + split[1]);
            try {
                return new x(split[2], split[1].toLowerCase(), Integer.valueOf(split[0]).intValue());
            } catch (NumberFormatException e2) {
                return null;
            }
        } catch (UnsupportedEncodingException e3) {
            return null;
        }
    }

    public static x b(String str, boolean z) {
        StringBuilder sb = new StringBuilder();
        sb.append(i).append("&type=ctcailingurl").append("&ctcid=").append(str).append("&nocache=").append(z ? "1" : "0");
        a.a(b, "httpGetCTCailingUrl, paraString:" + sb.toString());
        byte[] k = k(sb.toString());
        if (k == null) {
            return null;
        }
        try {
            String[] split = new String(k, "UTF-8").split("\t", 0);
            if (split.length != 3) {
                return null;
            }
            for (int i2 = 0; i2 < split.length; i2++) {
                a.a(b, "result " + i2 + ": " + split[i2]);
            }
            a.a(b, "httpGetCTCailingUrl: url =" + split[2]);
            a.a(b, "httpGetCTCailingUrl: bitrate =" + split[0]);
            a.a(b, "httpGetCTCailingUrl: format =" + split[1]);
            try {
                return new x(split[2], split[1].toLowerCase(), Integer.valueOf(split[0]).intValue());
            } catch (NumberFormatException e2) {
                return null;
            }
        } catch (UnsupportedEncodingException e3) {
            return null;
        }
    }

    public static byte[] a(String str, String str2, int i2, int i3) {
        StringBuilder sb = new StringBuilder();
        try {
            StringBuilder append = sb.append(i).append("&type=search&keyword=");
            if (!NativeDES.a()) {
                str = URLEncoder.encode(str, "GBK");
            }
            append.append(str).append("&src=").append(str2).append("&page=").append(i2).append("&pagesize=").append(i3).append("&include=all&ctdb=1&cudb=1");
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
        }
        a.a(b, "paraString before encod:" + sb.toString());
        return k(sb.toString());
    }

    public static byte[] c() {
        StringBuilder sb = new StringBuilder();
        a.a(b, "httpGetCategoryList, st:" + f.u());
        sb.append(i).append("&type=getcategory");
        a.a(b, "httpGetCategoryList: paraString = " + sb.toString());
        return k(sb.toString());
    }

    public static byte[] d() {
        String str = i + "&type=getbanner";
        a.a(b, "httpGetBannerAdList: paraString = " + str);
        return k(str);
    }

    public static byte[] e() {
        return bj.b("http://www.shoujiduoduo.com/ringv1/keywordad.xml");
    }

    private static byte[] k(String str) {
        String e2 = e(str);
        StringBuilder sb = new StringBuilder();
        sb.append(o()).append(e2);
        a.a(b, "realRequest: url = " + sb.toString());
        byte[] b2 = bj.b(sb.toString());
        if (b2 != null) {
            return b2;
        }
        a.a(b, "retry request");
        return j(e2);
    }

    /* access modifiers changed from: private */
    public static String o() {
        if (NativeDES.a()) {
            return "http://" + m.a().d() + "/ring_enc.php?q=";
        }
        return "http://www.shoujiduoduo.com/ring.php?";
    }

    private static String l(String str) {
        return "http://" + str + "/ring_enc.php?q=";
    }

    public static byte[] a(int i2, int i3, int i4) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(i).append("&type=getlist&listid=").append(i2).append("&page=").append(i3).append("&pagesize=").append(i4);
        a.a(b, "httpGetRingList: paraString = " + stringBuffer.toString());
        return k(stringBuffer.toString());
    }

    public static byte[] a(int i2, int i3, int i4, int i5) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(i).append("&type=getlist&listid=").append(i2).append("&page=").append(i3).append("&pagesize=").append(i4).append("&ranid=").append(i5);
        a.a(b, "httpGetRingList: paraString = " + stringBuffer.toString());
        return k(stringBuffer.toString());
    }

    public static byte[] a(int i2, int i3, int i4, String str) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(i).append("&type=getlist&listid=").append(i2).append("&page=").append(i3).append("&pagesize=").append(i4).append("&area=").append(str);
        a.a(b, "httpGetRingList: paraString = " + stringBuffer.toString());
        return k(stringBuffer.toString());
    }

    public static byte[] a(int i2, int i3, int i4, String str, int i5) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(i).append("&type=getlist&listid=").append(i2).append("&page=").append(i3).append("&pagesize=").append(i4).append("&area=").append(str).append("&ranid=").append(i5);
        a.a(b, "httpGetRingList: paraString = " + stringBuffer.toString());
        return k(stringBuffer.toString());
    }

    public static final byte[] a(String str, String str2) {
        a.b(b, "method: " + str);
        a.a(b, "param: " + str2);
        StringBuilder sb = new StringBuilder();
        sb.append(i).append(str);
        if (!TextUtils.isEmpty(str2)) {
            sb.append(str2);
        }
        a.a(b, "request param:" + sb.toString());
        String e2 = e(sb.toString());
        StringBuilder sb2 = new StringBuilder();
        sb2.append(o()).append(e2);
        a.a(b, "request url encoded:" + sb2.toString());
        byte[] b2 = bj.b(sb2.toString());
        if (b2 == null) {
            return j(e2);
        }
        return b2;
    }

    public static String f() {
        String str = i + "&type=genrid&uid=" + as.a(RingDDApp.c(), "user_uid", "");
        String str2 = o() + e(str);
        a.a(b, "httpGenRid: url = " + o() + str);
        a.a(b, "httpGenRid: url = " + str2);
        byte[] a2 = bj.a(str2);
        if (a2 == null) {
            return "";
        }
        return new String(a2);
    }

    public static byte[] g() {
        String str = i + "&type=userringlist&uid=" + as.a(RingDDApp.c(), "user_uid", "");
        a.a(b, "httpGetUserMakeRingList: url = " + o() + str);
        String str2 = o() + e(str);
        a.a(b, "httpGetUserMakeRingList: url = " + str2);
        byte[] b2 = bj.b(str2);
        if (b2 != null) {
        }
        return b2;
    }

    public static byte[] h() {
        String a2 = as.a(RingDDApp.c(), "user_uid", "");
        if (TextUtils.isEmpty(a2)) {
            a.c(b, "not login, no online favorite ring list");
            return null;
        }
        String str = i + "&type=getuserfavorite&uid=" + a2;
        a.a(b, "httpGetUserFavorite: url = " + o() + str);
        String str2 = o() + e(str);
        a.a(b, "httpGetUserFavorite: url = " + str2);
        byte[] b2 = bj.b(str2);
        if (b2 != null) {
        }
        return b2;
    }

    public static byte[] b(String str) {
        String str2 = i + "&type=adduserfavorite&uid=" + as.a(RingDDApp.c(), "user_uid", "") + "&rid=" + str;
        a.a(b, "httpAddUserFavorite: url = " + o() + str2);
        String str3 = o() + e(str2);
        a.a(b, "httpAddUserFavorite: url = " + str3);
        byte[] b2 = bj.b(str3);
        if (b2 != null) {
            a.a(b, "httpAddUserFavorite: data = " + new String(b2));
        }
        return b2;
    }

    public static byte[] c(String str) {
        String str2 = i + "&type=deluserfavorite&uid=" + as.a(RingDDApp.c(), "user_uid", "") + "&rid=" + str;
        a.a(b, "httpDelUserFavorite: url = " + o() + str2);
        String str3 = o() + e(str2);
        a.a(b, "httpDelUserFavorite: url = " + str3);
        byte[] b2 = bj.b(str3);
        if (b2 != null) {
            a.a(b, "httpDelUserFavorite: data = " + new String(b2));
        }
        return b2;
    }

    public static boolean a(RingData ringData, String str) {
        String str2;
        String a2 = as.a(RingDDApp.c(), "user_uid", "");
        int i2 = ((MakeRingData) ringData).d == 0 ? 1 : 0;
        String a3 = as.a(RingDDApp.c(), "user_upload_name", "");
        String str3 = ((MakeRingData) ringData).m;
        File file = new File(str3);
        String b2 = t.b(str3);
        String str4 = "http://" + am.a().a("bcs_domain_name") + "/duoduo-ring-user-upload";
        if (i2 == 1) {
            str2 = str4 + "/record-ring/" + ringData.g + "." + b2;
        } else {
            str2 = str4 + "/edit-ring/" + ringData.g + "." + b2;
        }
        String str5 = i + String.format("&type=uploadsucc&rid=%s&uid=%s&url=%s&isrec=%d&audiocate=%s&username=%s&artist=%s&duration=%d&length=%d&fmt=%s&name=%s&date=%s", ringData.g, a2, m(str2), Integer.valueOf(i2), str, m(a3), m(ringData.f), Integer.valueOf(ringData.j * Constants.CLEARIMGED), Long.valueOf(file.length()), b2, m(ringData.e), m(((MakeRingData) ringData).c));
        a.a(b, "httpUploadSuccess: url = " + o() + str5);
        String str6 = o() + e(str5);
        a.a(b, "httpUploadSuccess: url = " + str6);
        byte[] b3 = bj.b(str6);
        if (b3 == null || !new String(b3).equals("0")) {
            return false;
        }
        return true;
    }

    public static void d(String str) {
        i.a(new x(as.a(RingDDApp.c(), "user_uid", ""), str));
    }

    private static String m(String str) {
        try {
            return URLEncoder.encode(str, "UTF-8");
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public static void b(String str, String str2) {
        a(str, str2, "");
    }

    public static void a(String str, String str2, String str3) {
        i.a(new y(str2, str, str3));
    }

    public static byte[] i() {
        String str = o() + e(i + "&type=getduoduofamily");
        a.a(b, "Duoduofamily URL: " + str);
        return bj.a(str);
    }

    public static String e(String str) {
        if (!TextUtils.isEmpty(str) && NativeDES.a()) {
            return URLEncoder.encode(f1693a.Encrypt(str));
        }
        return str;
    }

    public static boolean c(String str, String str2) {
        String str3 = "http://www.shoujiduoduo.com/vlogc.php?" + (i + "&s=" + f + "&act=feedback");
        String str4 = "ACT:FEEDBACK|V:" + f.q() + "|INSTALL_SRC:" + f.p() + "|USER:" + f.a() + "|DEV:" + f.j() + "|SYS:" + f.k() + "|MSG:" + str + "|CONTACT:" + str2;
        a.a(b, "httpSubmitAdvice, url = " + str3);
        a.a(b, "httpSubmitAdvice, content = " + str4);
        return bj.a(str3, b.a(str4, "UTF-8", (String) null));
    }

    public static void a(String str, int i2, String str2, String str3, String str4) {
        i.a(new z(str, i2, str2, str3, str4));
    }

    public static void a(int i2) {
        i.a(new aa(i2));
    }

    public static void f(String str) {
        i.a(new ab("http://www.shoujiduoduo.com/ring.php?type=kv&group=cmsdk&key=" + URLEncoder.encode(str) + h));
    }

    public static void g(String str) {
        i.a(new ac("http://www.shoujiduoduo.com/ring.php?type=kv&group=downError404&key=" + URLEncoder.encode(str)));
    }

    public static boolean h(String str) {
        String e2 = e(i + "&type=ad" + "&act=" + str);
        String str2 = o() + e2;
        a.a(b, "logAD: url = " + str2);
        byte[] a2 = bj.a(str2);
        if (a2 == null) {
            a2 = j(e2);
        }
        return a2 != null;
    }

    public static boolean b(String str, String str2, String str3) {
        String str4;
        StringBuilder append = new StringBuilder().append(i).append("&type=logduoduoad").append("&act=").append(str).append("&result=").append(str2);
        if (str3 == null) {
            str4 = "";
        } else {
            str4 = "&url=" + str3;
        }
        String sb = append.append(str4).toString();
        a.a(b, "logAD: paraString = " + sb);
        String e2 = e(sb);
        String str5 = o() + e2;
        a.a(b, "logAD: url = " + str5);
        byte[] a2 = bj.a(str5);
        if (a2 == null) {
            a2 = j(e2);
        }
        if (a2 != null) {
            return true;
        }
        return false;
    }

    public static byte[] j() {
        String str = i + "&type=getad";
        a.a(b, "paraString in plainText: " + str);
        String e2 = e(str);
        String str2 = o() + e2;
        a.a(b, "httpGetAD: url = " + str2);
        byte[] b2 = bj.b(str2);
        if (b2 == null) {
            return j(e2);
        }
        return b2;
    }

    public static byte[] k() {
        StringBuilder sb = new StringBuilder();
        sb.append(i).append("&type=gethotkeyword");
        a.a(b, "paraString in plainText: " + sb.toString());
        String e2 = e(sb.toString());
        String str = o() + e2;
        a.a(b, "httpGetHotKeyword: url = " + str);
        byte[] b2 = bj.b(str);
        if (b2 == null) {
            return j(e2);
        }
        return b2;
    }

    public static boolean a(String str, String str2, long j, g gVar) {
        a.a(b, "download soft: url = " + str);
        a.a(b, "download soft: path = " + str2);
        a.a(b, "start_pos = " + j);
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
            a.a(b, "download soft: conn = " + httpURLConnection.toString());
            httpURLConnection.setRequestProperty("RANGE", "bytes=" + j + "-");
            httpURLConnection.connect();
            a.a(b, "download soft: connect finished!");
            int contentLength = httpURLConnection.getContentLength();
            if (contentLength <= 0) {
                a.a(b, "download soft: filesize Error! filesize= " + contentLength);
                if (gVar != null) {
                    gVar.a(0);
                }
                return false;
            }
            if (gVar != null) {
                gVar.b((long) contentLength);
            }
            a.a(b, "download soft: filesize = " + contentLength);
            InputStream inputStream = httpURLConnection.getInputStream();
            RandomAccessFile randomAccessFile = new RandomAccessFile(str2, "rw");
            randomAccessFile.seek(j);
            byte[] bArr = new byte[10240];
            while (true) {
                int read = inputStream.read(bArr, 0, 10240);
                if (read <= 0) {
                    break;
                }
                randomAccessFile.write(bArr, 0, read);
                j += (long) read;
                if (gVar != null) {
                    gVar.a(j);
                }
            }
            randomAccessFile.close();
            httpURLConnection.disconnect();
            if (gVar != null) {
                gVar.a();
            }
            return true;
        } catch (MalformedURLException e2) {
            e2.printStackTrace();
            return false;
        } catch (IOException e3) {
            e3.printStackTrace();
            return false;
        }
    }

    public static InputStream c(String str, boolean z) {
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
            if (z) {
                httpURLConnection.setRequestProperty("Accept-Encoding", "gzip,deflate,sdch");
            }
            return httpURLConnection.getInputStream();
        } catch (Exception e2) {
            System.out.println(e2.getMessage());
            return null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0051 A[SYNTHETIC, Splitter:B:17:0x0051] */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0056 A[Catch:{ Exception -> 0x009d }] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x008d A[SYNTHETIC, Splitter:B:40:0x008d] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0092 A[Catch:{ Exception -> 0x0096 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean a(java.lang.String r8, java.lang.String r9, boolean r10) {
        /*
            r1 = 0
            r0 = 0
            r4 = 4096(0x1000, float:5.74E-42)
            java.io.InputStream r3 = c(r8, r10)     // Catch:{ Exception -> 0x009f, all -> 0x0088 }
            java.io.File r5 = new java.io.File     // Catch:{ Exception -> 0x006d, all -> 0x0098 }
            r5.<init>(r9)     // Catch:{ Exception -> 0x006d, all -> 0x0098 }
            boolean r2 = r5.exists()     // Catch:{ Exception -> 0x006d, all -> 0x0098 }
            if (r2 == 0) goto L_0x005a
            r5.delete()     // Catch:{ Exception -> 0x006d, all -> 0x0098 }
        L_0x0016:
            r5.createNewFile()     // Catch:{ Exception -> 0x006d, all -> 0x0098 }
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x006d, all -> 0x0098 }
            r2.<init>(r5)     // Catch:{ Exception -> 0x006d, all -> 0x0098 }
            byte[] r1 = new byte[r4]     // Catch:{ Exception -> 0x004c, all -> 0x009b }
            java.lang.String r4 = com.shoujiduoduo.util.w.b     // Catch:{ Exception -> 0x004c, all -> 0x009b }
            java.lang.String r5 = "image write begin"
            com.shoujiduoduo.base.a.a.a(r4, r5)     // Catch:{ Exception -> 0x004c, all -> 0x009b }
        L_0x0027:
            if (r3 == 0) goto L_0x0070
            int r4 = r3.read(r1)     // Catch:{ Exception -> 0x004c, all -> 0x009b }
            if (r4 <= 0) goto L_0x0070
            java.lang.String r5 = com.shoujiduoduo.util.w.b     // Catch:{ Exception -> 0x004c, all -> 0x009b }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x004c, all -> 0x009b }
            r6.<init>()     // Catch:{ Exception -> 0x004c, all -> 0x009b }
            java.lang.String r7 = "image write size:"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x004c, all -> 0x009b }
            java.lang.StringBuilder r6 = r6.append(r4)     // Catch:{ Exception -> 0x004c, all -> 0x009b }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x004c, all -> 0x009b }
            com.shoujiduoduo.base.a.a.a(r5, r6)     // Catch:{ Exception -> 0x004c, all -> 0x009b }
            r5 = 0
            r2.write(r1, r5, r4)     // Catch:{ Exception -> 0x004c, all -> 0x009b }
            goto L_0x0027
        L_0x004c:
            r1 = move-exception
            r1 = r2
            r2 = r3
        L_0x004f:
            if (r1 == 0) goto L_0x0054
            r1.close()     // Catch:{ Exception -> 0x009d }
        L_0x0054:
            if (r2 == 0) goto L_0x0059
            r2.close()     // Catch:{ Exception -> 0x009d }
        L_0x0059:
            return r0
        L_0x005a:
            java.lang.String r2 = r5.getParent()     // Catch:{ Exception -> 0x006d, all -> 0x0098 }
            java.io.File r6 = new java.io.File     // Catch:{ Exception -> 0x006d, all -> 0x0098 }
            r6.<init>(r2)     // Catch:{ Exception -> 0x006d, all -> 0x0098 }
            boolean r2 = r6.exists()     // Catch:{ Exception -> 0x006d, all -> 0x0098 }
            if (r2 != 0) goto L_0x0016
            r6.mkdirs()     // Catch:{ Exception -> 0x006d, all -> 0x0098 }
            goto L_0x0016
        L_0x006d:
            r2 = move-exception
            r2 = r3
            goto L_0x004f
        L_0x0070:
            r2.flush()     // Catch:{ Exception -> 0x004c, all -> 0x009b }
            java.lang.String r1 = com.shoujiduoduo.util.w.b     // Catch:{ Exception -> 0x004c, all -> 0x009b }
            java.lang.String r4 = "image write end"
            com.shoujiduoduo.base.a.a.a(r1, r4)     // Catch:{ Exception -> 0x004c, all -> 0x009b }
            r0 = 1
            if (r2 == 0) goto L_0x0080
            r2.close()     // Catch:{ Exception -> 0x0086 }
        L_0x0080:
            if (r3 == 0) goto L_0x0059
            r3.close()     // Catch:{ Exception -> 0x0086 }
            goto L_0x0059
        L_0x0086:
            r1 = move-exception
            goto L_0x0059
        L_0x0088:
            r0 = move-exception
            r2 = r1
            r3 = r1
        L_0x008b:
            if (r2 == 0) goto L_0x0090
            r2.close()     // Catch:{ Exception -> 0x0096 }
        L_0x0090:
            if (r3 == 0) goto L_0x0095
            r3.close()     // Catch:{ Exception -> 0x0096 }
        L_0x0095:
            throw r0
        L_0x0096:
            r1 = move-exception
            goto L_0x0095
        L_0x0098:
            r0 = move-exception
            r2 = r1
            goto L_0x008b
        L_0x009b:
            r0 = move-exception
            goto L_0x008b
        L_0x009d:
            r1 = move-exception
            goto L_0x0059
        L_0x009f:
            r2 = move-exception
            r2 = r1
            goto L_0x004f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.util.w.a(java.lang.String, java.lang.String, boolean):boolean");
    }
}
