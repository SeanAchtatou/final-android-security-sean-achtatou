package com.shoujiduoduo.util.c;

import com.duoduo.cmmusic.init.InitCmmInterface;
import com.duoduo.cmmusic.init.Result;
import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParserException;

/* compiled from: ChinaMobileUtils */
class k implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f1646a;
    final /* synthetic */ a b;
    final /* synthetic */ b c;

    k(b bVar, String str, a aVar) {
        this.c = bVar;
        this.f1646a = str;
        this.b = aVar;
    }

    public void run() {
        if (this.c.g == null) {
            com.shoujiduoduo.base.a.a.c("ChinaMobileUtils", "context is null, main activity is destroyed");
            return;
        }
        try {
            c.b bVar = new c.b();
            Result validateCode = InitCmmInterface.getValidateCode(this.c.g, this.f1646a);
            if (validateCode != null && validateCode.getResCode().equals("000000")) {
                bVar.b = validateCode.getResCode();
                bVar.c = validateCode.getResMsg();
                this.b.g(bVar);
                com.shoujiduoduo.base.a.a.a("ChinaMobileUtils", "getValidateCode, success");
            } else if (validateCode != null) {
                com.shoujiduoduo.base.a.a.a("ChinaMobileUtils", "getValidateCode, failed, " + validateCode.toString());
                this.b.g(b.k);
                com.shoujiduoduo.base.a.a.a("ChinaMobileUtils", "getValidateCode, failed");
            } else {
                com.shoujiduoduo.base.a.a.a("ChinaMobileUtils", "getValidateCode, failed, res is null");
                this.b.g(b.k);
                com.shoujiduoduo.base.a.a.a("ChinaMobileUtils", "getValidateCode, failed");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XmlPullParserException e2) {
            e2.printStackTrace();
        }
    }
}
