package com.shoujiduoduo.util.c;

import android.content.Context;
import com.duoduo.cmmusic.init.GetAppInfo;
import com.duoduo.cmmusic.init.GetAppInfoInterface;
import com.shoujiduoduo.base.a.a;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import org.apache.mina.proxy.handlers.http.HttpProxyConstants;

/* compiled from: HttpPostCore */
public class n {

    /* renamed from: a  reason: collision with root package name */
    private static final String f1649a = n.class.getSimpleName();

    public static String a(Context context, String str, byte[] bArr, boolean z) {
        a.b(f1649a, "address----" + str);
        try {
            a.a(f1649a, "requestString : " + new String(bArr, "UTF-8"));
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            httpURLConnection.setConnectTimeout(30000);
            httpURLConnection.setReadTimeout(30000);
            httpURLConnection.setUseCaches(false);
            httpURLConnection.setRequestMethod("POST");
            if (a(context)) {
                c(context, httpURLConnection, z);
            } else if (GetAppInfo.getIMSIbyFile(context).trim().length() != 0) {
                a(context, httpURLConnection, z);
            } else {
                b(context, httpURLConnection, z);
            }
            httpURLConnection.setRequestProperty("Content-length", "" + bArr.length);
            httpURLConnection.setRequestProperty("Accept", "*/*");
            httpURLConnection.setRequestProperty("Content-Type", "*/*");
            httpURLConnection.setRequestProperty("Connection", "Keep-Alive");
            httpURLConnection.setRequestProperty("Charset", "UTF-8");
            httpURLConnection.setRequestProperty("Token", GetAppInfoInterface.getToken(context));
            httpURLConnection.setRequestProperty("excode", GetAppInfo.getexCode(context));
            a.b(f1649a, "output before");
            OutputStream outputStream = httpURLConnection.getOutputStream();
            outputStream.write(bArr);
            outputStream.flush();
            a.b(f1649a, "output flush");
            outputStream.close();
            int responseCode = httpURLConnection.getResponseCode();
            a.b(f1649a, "responseCode-----" + responseCode);
            if (200 == responseCode) {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                byte[] bArr2 = new byte[1024];
                while (true) {
                    int read = httpURLConnection.getInputStream().read(bArr2, 0, 1024);
                    if (read != -1) {
                        byteArrayOutputStream.write(bArr2, 0, read);
                    } else {
                        String str2 = new String(byteArrayOutputStream.toByteArray(), "UTF-8");
                        a.b(f1649a, "responseBody------------\r\n" + str2);
                        return str2;
                    }
                }
            }
        } catch (Exception e) {
            a.a(f1649a, "cm httppostcore return: null");
            e.printStackTrace();
        }
        return null;
    }

    private static void a(Context context, HttpURLConnection httpURLConnection, boolean z) {
        httpURLConnection.addRequestProperty("Authorization", "OEPAUTH realm=\"OEP\",IMSI=\"" + GetAppInfo.getIMSIbyFile(context) + "\",appID=\"" + (z ? "004750246952188534" : GetAppInfoInterface.getAppid(context)) + "\",pubKey=\"" + GetAppInfoInterface.getSign(context) + "\",netMode=\"" + GetAppInfoInterface.getNetMode(context) + "\",packageName=\"" + GetAppInfoInterface.getPackageName(context) + "\",version=\"" + GetAppInfoInterface.getSDKVersion() + "\",excode=\"" + GetAppInfo.getexCode(context) + "\"");
        System.out.println("===============================================================");
        System.out.println("imsi\r\n" + GetAppInfoInterface.getIMSI(context) + HttpProxyConstants.CRLF);
        System.out.println("sign\r\n" + GetAppInfoInterface.getSign(context) + HttpProxyConstants.CRLF);
        a.a(f1649a, "imsi\r\n" + GetAppInfoInterface.getIMSI(context) + HttpProxyConstants.CRLF);
        a.a(f1649a, "sign\r\n" + GetAppInfoInterface.getSign(context) + HttpProxyConstants.CRLF);
        System.out.println("appid\r\n" + GetAppInfoInterface.getAppid(context) + HttpProxyConstants.CRLF);
        System.out.println("netmode\r\n" + GetAppInfoInterface.getNetMode(context) + HttpProxyConstants.CRLF);
        System.out.println("packagename\r\n" + GetAppInfoInterface.getPackageName(context) + HttpProxyConstants.CRLF);
        System.out.println("sdkversion\r\n" + GetAppInfoInterface.getSDKVersion() + HttpProxyConstants.CRLF);
        System.out.println("excode\r\n" + GetAppInfo.getexCode(context) + HttpProxyConstants.CRLF);
        System.out.println("===============================================================");
    }

    private static void b(Context context, HttpURLConnection httpURLConnection, boolean z) {
        httpURLConnection.addRequestProperty("Authorization", "OEPAUTH realm=\"OEP\",IMSI=\"\",appID=\"" + (z ? "004750246952188534" : GetAppInfoInterface.getAppid(context)) + "\",pubKey=\"" + GetAppInfoInterface.getSign(context) + "\",netMode=\"" + GetAppInfoInterface.getNetMode(context) + "\",packageName=\"" + GetAppInfoInterface.getPackageName(context) + "\",version=\"" + GetAppInfoInterface.getSDKVersion() + "\",excode=\"" + GetAppInfo.getexCode(context) + "\"");
        System.out.println("===============================================================");
        System.out.println("sign\r\n" + GetAppInfoInterface.getSign(context) + HttpProxyConstants.CRLF);
        a.a(f1649a, "sign\r\n" + GetAppInfoInterface.getSign(context) + HttpProxyConstants.CRLF);
        System.out.println("appid\r\n" + GetAppInfoInterface.getAppid(context) + HttpProxyConstants.CRLF);
        System.out.println("netmode\r\n" + GetAppInfoInterface.getNetMode(context) + HttpProxyConstants.CRLF);
        System.out.println("packagename\r\n" + GetAppInfoInterface.getPackageName(context) + HttpProxyConstants.CRLF);
        System.out.println("sdkversion\r\n" + GetAppInfoInterface.getSDKVersion() + HttpProxyConstants.CRLF);
        System.out.println("excode\r\n" + GetAppInfo.getexCode(context) + HttpProxyConstants.CRLF);
        System.out.println("===============================================================");
    }

    private static void c(Context context, HttpURLConnection httpURLConnection, boolean z) {
        httpURLConnection.addRequestProperty("Authorization", "OEPAUTH realm=\"OEP\",Token=\"" + GetAppInfoInterface.getToken(context) + "\",appID=\"" + (z ? "004750246952188534" : GetAppInfoInterface.getAppid(context)) + "\",pubKey=\"" + GetAppInfoInterface.getSign(context) + "\",netMode=\"" + GetAppInfoInterface.getNetMode(context) + "\",packageName=\"" + GetAppInfoInterface.getPackageName(context) + "\",version=\"" + GetAppInfoInterface.getSDKVersion() + "\",excode=\"" + GetAppInfo.getexCode(context) + "\"");
        System.out.println("===============================================================");
        System.out.println("token\r\n" + GetAppInfoInterface.getToken(context) + HttpProxyConstants.CRLF);
        System.out.println("sign\r\n" + GetAppInfoInterface.getSign(context) + HttpProxyConstants.CRLF);
        a.a(f1649a, "sign\r\n" + GetAppInfoInterface.getSign(context) + HttpProxyConstants.CRLF);
        System.out.println("appid\r\n" + GetAppInfoInterface.getAppid(context) + HttpProxyConstants.CRLF);
        System.out.println("netmode\r\n" + GetAppInfoInterface.getNetMode(context) + HttpProxyConstants.CRLF);
        System.out.println("packagename\r\n" + GetAppInfoInterface.getPackageName(context) + HttpProxyConstants.CRLF);
        System.out.println("sdkversion\r\n" + GetAppInfoInterface.getSDKVersion() + HttpProxyConstants.CRLF);
        System.out.println("excode\r\n" + GetAppInfo.getexCode(context) + HttpProxyConstants.CRLF);
        System.out.println("===============================================================");
    }

    static boolean a(Context context) {
        return GetAppInfoInterface.getToken(context).length() > 0;
    }
}
