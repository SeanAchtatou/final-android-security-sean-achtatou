package com.shoujiduoduo.util.c;

import com.shoujiduoduo.util.av;
import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/* compiled from: ChinaMobileUtils */
class e implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f1641a;
    final /* synthetic */ boolean b;
    final /* synthetic */ String c;
    final /* synthetic */ a d;
    final /* synthetic */ b e;

    e(b bVar, String str, boolean z, String str2, a aVar) {
        this.e = bVar;
        this.f1641a = str;
        this.b = z;
        this.c = str2;
        this.d = aVar;
    }

    public void run() {
        c.C0028c cVar = new c.C0028c();
        cVar.b = "000000";
        cVar.c = "成功";
        if (av.c(this.e.d) || !this.e.b.containsKey(this.e.d) || !((Boolean) this.e.b.get(this.e.d)).booleanValue()) {
            try {
                String a2 = n.a(this.e.g, (this.b ? "http://mm.shoujiduoduo.com/mm/mmweb.php?from=ringdd_ar&cmd=" : "http://mm.shoujiduoduo.com/mm/mm.php?from=ringdd_ar&cmd=") + URLEncoder.encode("/crbt/querymonth", "UTF-8"), this.f1641a.getBytes("UTF-8"), this.b);
                if (a2 != null) {
                    c.b b2 = this.e.d(a2);
                    if (b2 != null) {
                        cVar.d = b2;
                    } else {
                        cVar.d = b.k;
                    }
                } else {
                    cVar.d = b.k;
                }
                this.e.a("/crbt/querymonth", cVar.d, "");
            } catch (UnsupportedEncodingException e2) {
                e2.printStackTrace();
                cVar.d = b.k;
            }
        } else {
            cVar.d = new c.b();
            cVar.d.b = "000000";
            cVar.d.c = "开通";
        }
        if (av.c(this.e.d) || !this.e.c.containsKey(this.e.d) || !((Boolean) this.e.c.get(this.e.d)).booleanValue()) {
            try {
                String a3 = n.a(this.e.g, (this.b ? "http://mm.shoujiduoduo.com/mm/mmweb.php?from=ringdd_ar&cmd=" : "http://mm.shoujiduoduo.com/mm/mm.php?from=ringdd_ar&cmd=") + URLEncoder.encode("/crbt/open/check", "UTF-8"), this.c.getBytes("UTF-8"), this.b);
                if (a3 != null) {
                    c.b b3 = this.e.d(a3);
                    if (b3 != null) {
                        cVar.f1596a = b3;
                    } else {
                        cVar.f1596a = b.k;
                    }
                } else {
                    cVar.f1596a = b.k;
                }
                this.e.a("/crbt/open/check", cVar.f1596a, "");
            } catch (UnsupportedEncodingException e3) {
                e3.printStackTrace();
                cVar.f1596a = b.k;
            }
        } else {
            cVar.f1596a = new c.b();
            cVar.f1596a.b = "000000";
            cVar.f1596a.c = "开通";
        }
        this.d.g(cVar);
    }
}
