package com.shoujiduoduo.util.c;

import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/* compiled from: ChinaMobileUtils */
class d implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f1640a;
    final /* synthetic */ boolean b;
    final /* synthetic */ String c;
    final /* synthetic */ String d;
    final /* synthetic */ a e;
    final /* synthetic */ b f;

    d(b bVar, String str, boolean z, String str2, String str3, a aVar) {
        this.f = bVar;
        this.f1640a = str;
        this.b = z;
        this.c = str2;
        this.d = str3;
        this.e = aVar;
    }

    public void run() {
        c.b f2;
        try {
            byte[] bytes = this.f1640a.getBytes("UTF-8");
            if (this.f.g == null) {
                com.shoujiduoduo.base.a.a.c("ChinaMobileUtils", "context is null, main activity is destroyed");
                return;
            }
            String a2 = n.a(this.f.g, (this.b ? "http://mm.shoujiduoduo.com/mm/mmweb.php?from=ringdd_ar&cmd=" : "http://mm.shoujiduoduo.com/mm/mm.php?from=ringdd_ar&cmd=") + URLEncoder.encode(this.c, "UTF-8"), bytes, this.b);
            if (a2 != null) {
                f2 = this.f.d(a2);
                if (f2 == null) {
                    f2 = b.k;
                }
            } else {
                f2 = b.k;
            }
            this.f.a(this.c, f2, this.d);
            this.e.g(f2);
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
            f2 = b.k;
        }
    }
}
