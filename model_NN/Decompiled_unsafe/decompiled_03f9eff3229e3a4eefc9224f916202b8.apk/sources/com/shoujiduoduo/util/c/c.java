package com.shoujiduoduo.util.c;

import com.cmsc.cmmusic.common.RingbackManagerInterface;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.util.av;
import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;

/* compiled from: ChinaMobileUtils */
class c implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f1639a;
    final /* synthetic */ String b;
    final /* synthetic */ a c;
    final /* synthetic */ b d;

    c(b bVar, String str, String str2, a aVar) {
        this.d = bVar;
        this.f1639a = str;
        this.b = str2;
        this.c = aVar;
    }

    public void run() {
        c.j a2;
        String ringbackPolicy = RingbackManagerInterface.getRingbackPolicy(RingDDApp.c(), this.f1639a);
        com.shoujiduoduo.base.a.a.a("ChinaMobileUtils", "getRingbackPolicy, result:" + ringbackPolicy);
        if (av.c(ringbackPolicy) || (a2 = this.d.c(ringbackPolicy)) == null) {
            this.d.a("cm_get_ringback_policy", "fail, return content wrong!", this.b);
            this.c.g(b.k);
            return;
        }
        this.d.a(a2, "cm_get_ringback_policy", this.b);
        this.c.g(a2);
    }
}
