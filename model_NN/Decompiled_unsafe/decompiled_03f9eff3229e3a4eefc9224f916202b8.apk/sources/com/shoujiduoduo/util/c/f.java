package com.shoujiduoduo.util.c;

import com.cmsc.cmmusic.init.InitCmmInterface;
import com.shoujiduoduo.util.am;
import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.c.b;
import com.sina.weibo.sdk.exception.WeiboAuthException;

/* compiled from: ChinaMobileUtils */
class f implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f1642a;
    final /* synthetic */ b b;

    f(b bVar, a aVar) {
        this.b = bVar;
        this.f1642a = aVar;
    }

    public void run() {
        if (this.b.g != null) {
            c.l lVar = new c.l();
            b.c a2 = this.b.b(this.b.g);
            if (a2 == null) {
                com.shoujiduoduo.base.a.a.a("ChinaMobileUtils", "短信验证码尚未初始化成功");
                b.a unused = this.b.i = b.a.fail;
            } else if (a2.getResCode().equals("000000")) {
                com.shoujiduoduo.base.a.a.a("ChinaMobileUtils", "短信验证码已经初始化成功, mobile is " + a2.a());
                com.shoujiduoduo.base.a.a.a("MyRingtoneScene", "短信验证码已经初始化成功, mobile is " + a2.a());
                b.a unused2 = this.b.i = b.a.success;
                lVar.b = a2.getResCode();
                lVar.c = a2.getResMsg();
                lVar.d = a2.a();
                lVar.f1605a = c.l.a.sms_code;
            } else {
                b.a unused3 = this.b.i = b.a.fail;
                com.shoujiduoduo.base.a.a.a("ChinaMobileUtils", "短信验证码初始化检查失败，res:" + a2.toString());
            }
            if (am.a().b("cm_sunshine_sdk_enable")) {
                if (InitCmmInterface.initCheck(this.b.g)) {
                    com.shoujiduoduo.base.a.a.a("ChinaMobileUtils", "sunshine sdk is inited");
                } else {
                    com.shoujiduoduo.base.a.a.a("ChinaMobileUtils", "sunshine sdk is not inited");
                }
            }
            if (this.b.i != b.a.success) {
                if (com.duoduo.cmmusic.init.InitCmmInterface.initCheck(this.b.g)) {
                    com.shoujiduoduo.base.a.a.a("ChinaMobileUtils", "sms wap 已经初始化成功");
                    b.a unused4 = this.b.i = b.a.success;
                    lVar.b = "000000";
                    lVar.c = "smswap 初始化检查成功";
                    lVar.d = "";
                    lVar.f1605a = c.l.a.wap;
                } else {
                    com.shoujiduoduo.base.a.a.a("ChinaMobileUtils", "初始化检查失败");
                    lVar.b = WeiboAuthException.DEFAULT_AUTH_ERROR_CODE;
                    lVar.c = "初始化检查失败";
                }
            }
            if (this.f1642a != null) {
                this.f1642a.g(lVar);
            }
        }
    }
}
