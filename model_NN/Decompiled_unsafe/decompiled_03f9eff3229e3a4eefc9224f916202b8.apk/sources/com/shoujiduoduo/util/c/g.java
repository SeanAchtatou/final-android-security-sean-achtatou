package com.shoujiduoduo.util.c;

import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.c.b;
import com.sina.weibo.sdk.exception.WeiboAuthException;

/* compiled from: ChinaMobileUtils */
class g implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f1643a;
    final /* synthetic */ b b;

    g(b bVar, a aVar) {
        this.b = bVar;
        this.f1643a = aVar;
    }

    public void run() {
        if (this.b.g == null) {
            com.shoujiduoduo.base.a.a.c("ChinaMobileUtils", "context is null, main activity is destroyed");
            return;
        }
        c.aa aaVar = new c.aa();
        b.c a2 = this.b.b(this.b.g);
        if (a2 != null) {
            if (a2.getResCode().equals("000000")) {
                com.shoujiduoduo.base.a.a.a("ChinaMobileUtils", "短信验证码已经初始化成功");
                b.a unused = this.b.i = b.a.success;
            } else {
                b.a unused2 = this.b.i = b.a.fail;
                com.shoujiduoduo.base.a.a.a("ChinaMobileUtils", "短信验证码初始化检查失败，res:" + a2.toString());
            }
            aaVar.b = a2.getResCode();
            aaVar.c = a2.getResMsg();
            aaVar.f1587a = a2.a();
        } else {
            com.shoujiduoduo.base.a.a.a("ChinaMobileUtils", "短信验证码尚未初始化成功");
            b.a unused3 = this.b.i = b.a.fail;
            aaVar.b = WeiboAuthException.DEFAULT_AUTH_ERROR_CODE;
            aaVar.c = "初始化检查失败";
        }
        this.f1643a.g(aaVar);
    }
}
