package com.shoujiduoduo.util.c;

import com.duoduo.cmmusic.init.InitCmmInterface;
import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.c.b;
import com.shoujiduoduo.util.w;
import com.sina.weibo.sdk.exception.WeiboAuthException;
import com.tencent.open.SocialConstants;
import java.util.HashMap;
import java.util.Hashtable;

/* compiled from: ChinaMobileUtils */
class j implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f1645a;
    final /* synthetic */ b b;

    j(b bVar, a aVar) {
        this.b = bVar;
        this.f1645a = aVar;
    }

    public void run() {
        if (this.b.g == null) {
            com.shoujiduoduo.base.a.a.c("ChinaMobileUtils", "context is null, main activity is destroyed");
            return;
        }
        Hashtable<String, String> initCmmEnv = InitCmmInterface.initCmmEnv(this.b.g);
        c.b bVar = new c.b();
        if (initCmmEnv == null || initCmmEnv.get("code") == null) {
            bVar.b = WeiboAuthException.DEFAULT_AUTH_ERROR_CODE;
            bVar.c = "对不起，中国移动的彩铃服务正在进行系统维护，请谅解";
            com.shoujiduoduo.base.a.a.b("ChinaMobileUtils", "初始化彩铃sdk失败");
        } else {
            bVar.b = initCmmEnv.get("code");
            bVar.c = initCmmEnv.get(SocialConstants.PARAM_APP_DESC);
            com.shoujiduoduo.base.a.a.b("ChinaMobileUtils", "初始化彩铃sdk code:" + bVar.a() + " desc:" + bVar.b());
        }
        this.f1645a.g(bVar);
        HashMap hashMap = new HashMap();
        if (initCmmEnv == null || initCmmEnv.get("code") == null || !initCmmEnv.get("code").equals("0")) {
            b.a unused = this.b.i = b.a.fail;
            com.umeng.analytics.b.b(this.b.g, "CM_SDK_INIT_FAIL");
            if (initCmmEnv != null) {
                hashMap.put("res", "failed,code:" + initCmmEnv.get("code") + " desc:" + initCmmEnv.get(SocialConstants.PARAM_APP_DESC));
                w.f("code:" + initCmmEnv.get("code") + " desc:" + initCmmEnv.get(SocialConstants.PARAM_APP_DESC));
            } else {
                w.f("sdk return null");
            }
        } else {
            b.a unused2 = this.b.i = b.a.success;
            hashMap.put("res", "success");
            com.umeng.analytics.b.b(this.b.g, "CM_SDK_INIT_SUC");
        }
        hashMap.put("netType", String.valueOf(w.a()));
        com.umeng.analytics.b.a(this.b.g, "CM_SDK_INIT", hashMap);
    }
}
