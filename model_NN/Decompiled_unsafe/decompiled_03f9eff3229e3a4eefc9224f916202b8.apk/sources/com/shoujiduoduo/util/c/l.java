package com.shoujiduoduo.util.c;

import com.duoduo.cmmusic.init.InitCmmInterface;
import com.duoduo.cmmusic.init.Result;
import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.c.b;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParserException;

/* compiled from: ChinaMobileUtils */
class l implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f1647a;
    final /* synthetic */ String b;
    final /* synthetic */ a c;
    final /* synthetic */ b d;

    l(b bVar, String str, String str2, a aVar) {
        this.d = bVar;
        this.f1647a = str;
        this.b = str2;
        this.c = aVar;
    }

    public void run() {
        if (this.d.g == null) {
            com.shoujiduoduo.base.a.a.c("ChinaMobileUtils", "context is null, main activity is destroyed");
            return;
        }
        try {
            c.b bVar = new c.b();
            Result smsLoginAuth = InitCmmInterface.smsLoginAuth(this.d.g, this.f1647a, this.b);
            if (smsLoginAuth != null && smsLoginAuth.getResCode().equals("000000")) {
                bVar.b = smsLoginAuth.getResCode();
                bVar.c = smsLoginAuth.getResMsg();
                this.c.g(bVar);
                b.a unused = this.d.i = b.a.success;
                com.umeng.analytics.b.b(this.d.g, "CM_SDK_INIT_PHONE_NUM_SUC");
                com.shoujiduoduo.base.a.a.a("ChinaMobileUtils", "smsLoginAuth, success");
                return;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XmlPullParserException e2) {
            e2.printStackTrace();
        }
        b.a unused2 = this.d.i = b.a.fail;
        com.umeng.analytics.b.b(this.d.g, "CM_SDK_INIT_PHONE_NUM_FAIL");
        this.c.g(b.k);
        com.shoujiduoduo.base.a.a.a("ChinaMobileUtils", "smsLoginAuth, failed");
    }
}
