package com.shoujiduoduo.util.c;

import com.cmsc.cmmusic.init.InitCmmInterface;
import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.util.as;
import com.shoujiduoduo.util.b.c;
import com.sina.weibo.sdk.exception.WeiboAuthException;

/* compiled from: ChinaMobileUtils */
class i extends x.b {
    final /* synthetic */ h d;

    i(h hVar) {
        this.d = hVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.util.c.b.a(com.shoujiduoduo.util.c.b, boolean):boolean
     arg types: [com.shoujiduoduo.util.c.b, int]
     candidates:
      com.shoujiduoduo.util.c.b.a(com.shoujiduoduo.util.c.b, java.lang.String):com.shoujiduoduo.util.b.c$j
      com.shoujiduoduo.util.c.b.a(com.shoujiduoduo.util.c.b, com.shoujiduoduo.util.c.b$a):com.shoujiduoduo.util.c.b$a
      com.shoujiduoduo.util.c.b.a(com.shoujiduoduo.util.c.b, android.content.Context):com.shoujiduoduo.util.c.b$c
      com.shoujiduoduo.util.c.b.a(java.lang.String, boolean):com.shoujiduoduo.util.b.c$b
      com.shoujiduoduo.util.c.b.a(android.app.Activity, com.shoujiduoduo.util.b.a):void
      com.shoujiduoduo.util.c.b.a(java.lang.String, com.shoujiduoduo.util.b.a):void
      com.shoujiduoduo.util.c.b.a(com.shoujiduoduo.util.c.b, boolean):boolean */
    public void a() {
        c.b bVar = new c.b();
        try {
            if (!this.d.c.f) {
                as.b(this.d.f1644a, "pref_load_cmcc_sunshine_sdk_start", "1");
                as.b(this.d.f1644a, "pref_load_cmcc_sunshine_sdk_end", "0");
                System.loadLibrary("mg20pbase");
                RingDDApp.b().a(false);
                as.b(this.d.f1644a, "pref_load_cmcc_sunshine_sdk_end", "1");
                InitCmmInterface.initSDK(this.d.f1644a);
                boolean unused = this.d.c.f = true;
            }
            bVar.b = "000000";
            bVar.c = "初始化成功";
        } catch (Throwable th) {
            RingDDApp.b().a(true);
            bVar.b = WeiboAuthException.DEFAULT_AUTH_ERROR_CODE;
            bVar.c = "初始化检查失败";
        }
        if (this.d.b != null) {
            this.d.b.g(bVar);
        }
    }
}
