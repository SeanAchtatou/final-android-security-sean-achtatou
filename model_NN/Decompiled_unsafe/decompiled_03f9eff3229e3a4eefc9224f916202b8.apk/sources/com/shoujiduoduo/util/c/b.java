package com.shoujiduoduo.util.c;

import android.app.Activity;
import android.content.Context;
import cn.banshenggua.aichang.utils.Constants;
import cn.banshenggua.aichang.utils.StringUtil;
import com.cmsc.cmmusic.common.data.BizInfo;
import com.cmsc.cmmusic.common.data.MusicInfo;
import com.duoduo.cmmusic.init.GetAppInfo;
import com.duoduo.cmmusic.init.GetAppInfoInterface;
import com.duoduo.cmmusic.init.Result;
import com.qq.e.comm.constants.ErrorCode;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.util.as;
import com.shoujiduoduo.util.av;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.f;
import com.shoujiduoduo.util.i;
import com.shoujiduoduo.util.w;
import com.sina.weibo.sdk.exception.WeiboAuthException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/* compiled from: ChinaMobileUtils */
public class b {
    private static final b h = new b();
    /* access modifiers changed from: private */
    public static final c.b k = new c.b(WeiboAuthException.DEFAULT_AUTH_ERROR_CODE, "对不起，中国移动的彩铃服务正在进行系统维护，请谅解");

    /* renamed from: a  reason: collision with root package name */
    private HashMap<String, C0029b> f1635a = new HashMap<>();
    /* access modifiers changed from: private */
    public HashMap<String, Boolean> b = new HashMap<>();
    /* access modifiers changed from: private */
    public HashMap<String, Boolean> c = new HashMap<>();
    /* access modifiers changed from: private */
    public String d;
    private String e;
    /* access modifiers changed from: private */
    public boolean f;
    /* access modifiers changed from: private */
    public Context g = RingDDApp.c();
    /* access modifiers changed from: private */
    public a i = a.f1636a;
    private String j = "";
    private boolean l;
    /* access modifiers changed from: private */
    public int m = ErrorCode.OtherError.CONTAINER_INVISIBLE_ERROR;

    /* compiled from: ChinaMobileUtils */
    public enum a {
        f1636a,
        checking,
        initializing,
        success,
        fail
    }

    /* renamed from: com.shoujiduoduo.util.c.b$b  reason: collision with other inner class name */
    /* compiled from: ChinaMobileUtils */
    private class C0029b {

        /* renamed from: a  reason: collision with root package name */
        public String f1637a;
        public long b;

        public C0029b(String str, long j) {
            this.f1637a = str;
            this.b = j;
        }

        public boolean a() {
            return System.currentTimeMillis() - this.b < ((long) (b.this.m * Constants.CLEARIMGED));
        }
    }

    private b() {
    }

    public static b a() {
        return h;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.util.as.a(android.content.Context, java.lang.String, long):long
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.shoujiduoduo.util.as.a(android.content.Context, java.lang.String, int):int
      com.shoujiduoduo.util.as.a(android.content.Context, java.lang.String, java.lang.String):java.lang.String
      com.shoujiduoduo.util.as.a(android.content.Context, java.lang.String, long):long */
    public String a(String str) {
        String a2 = as.a(RingDDApp.c(), str + "_smscode", "");
        long a3 = as.a(RingDDApp.c(), str + "_smscode_gettime", 0L);
        if (this.f1635a.containsKey(str) && this.f1635a.get(str).a()) {
            return this.f1635a.get(str).f1637a;
        }
        if (av.c(a2) || System.currentTimeMillis() - a3 >= ((long) (this.m * Constants.CLEARIMGED))) {
            return "";
        }
        return a2;
    }

    private c b(String str) {
        DocumentBuilder newDocumentBuilder;
        Document parse;
        Element documentElement;
        if (str == null || str.equals("")) {
            return null;
        }
        try {
            DocumentBuilderFactory newInstance = DocumentBuilderFactory.newInstance();
            if (newInstance == null || (newDocumentBuilder = newInstance.newDocumentBuilder()) == null || (parse = newDocumentBuilder.parse(new InputSource(new ByteArrayInputStream(str.getBytes((String) StringUtil.Encoding))))) == null || (documentElement = parse.getDocumentElement()) == null) {
                return null;
            }
            c cVar = new c();
            NodeList childNodes = documentElement.getChildNodes();
            if (childNodes == null) {
                return null;
            }
            for (int i2 = 0; i2 < childNodes.getLength(); i2++) {
                Node item = childNodes.item(i2);
                if (item.getNodeName().equalsIgnoreCase("resMsg")) {
                    cVar.setResMsg(item.getFirstChild().getNodeValue());
                } else if (item.getNodeName().equalsIgnoreCase("resCode")) {
                    cVar.setResCode(item.getFirstChild().getNodeValue());
                } else if (item.getNodeName().equalsIgnoreCase("mobile")) {
                    cVar.a(item.getFirstChild().getNodeValue());
                }
            }
            return cVar;
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
            return null;
        } catch (ParserConfigurationException e3) {
            e3.printStackTrace();
            return null;
        } catch (SAXException e4) {
            e4.printStackTrace();
            return null;
        } catch (IOException e5) {
            e5.printStackTrace();
            return null;
        } catch (DOMException e6) {
            e6.printStackTrace();
            return null;
        }
    }

    private String a(Node node) {
        if (node == null) {
            return "";
        }
        try {
            if (node.getFirstChild() == null) {
                return "";
            }
            return node.getFirstChild().getNodeValue();
        } catch (DOMException e2) {
            e2.printStackTrace();
            return "";
        }
    }

    /* access modifiers changed from: private */
    public c.j c(String str) {
        DocumentBuilder newDocumentBuilder;
        Document parse;
        Element documentElement;
        if (str == null || str.equals("")) {
            return null;
        }
        try {
            DocumentBuilderFactory newInstance = DocumentBuilderFactory.newInstance();
            if (newInstance == null || (newDocumentBuilder = newInstance.newDocumentBuilder()) == null || (parse = newDocumentBuilder.parse(new InputSource(new ByteArrayInputStream(str.getBytes((String) StringUtil.Encoding))))) == null || (documentElement = parse.getDocumentElement()) == null) {
                return null;
            }
            NodeList childNodes = documentElement.getChildNodes();
            c.j jVar = new c.j();
            jVar.f1603a = new BizInfo();
            jVar.d = new MusicInfo();
            if (childNodes == null) {
                return null;
            }
            for (int i2 = 0; i2 < childNodes.getLength(); i2++) {
                Node item = childNodes.item(i2);
                if (item.getNodeName().equalsIgnoreCase("resMsg")) {
                    jVar.c = a(item);
                } else if (item.getNodeName().equalsIgnoreCase("resCode")) {
                    jVar.b = a(item);
                } else if (item.getNodeName().equalsIgnoreCase("BizInfo")) {
                    BizInfo bizInfo = new BizInfo();
                    for (Node firstChild = item.getFirstChild(); firstChild != null; firstChild = firstChild.getNextSibling()) {
                        if (firstChild.getNodeName() == null || firstChild.getFirstChild() == null) {
                            com.shoujiduoduo.base.a.a.c("ChinaMobileUtils", "null pointer happens");
                        } else if (firstChild.getNodeName().equalsIgnoreCase("bizCode")) {
                            bizInfo.setBizCode(a(firstChild));
                        } else if (firstChild.getNodeName().equalsIgnoreCase("bizType")) {
                            bizInfo.setBizType(a(firstChild));
                        } else if (firstChild.getNodeName().equalsIgnoreCase("originalPrice")) {
                            bizInfo.setOriginalPrice(a(firstChild));
                        } else if (firstChild.getNodeName().equalsIgnoreCase("salePrice")) {
                            bizInfo.setSalePrice(a(firstChild));
                        } else if (firstChild.getNodeName().equalsIgnoreCase("description")) {
                            bizInfo.setDescription(a(firstChild));
                        } else if (firstChild.getNodeName().equalsIgnoreCase("resource")) {
                            bizInfo.setResource(a(firstChild));
                        } else if (firstChild.getNodeName().equalsIgnoreCase("hold2")) {
                            bizInfo.setHold2(a(firstChild));
                        }
                    }
                    if (com.tencent.connect.common.Constants.VIA_REPORT_TYPE_SHARE_TO_QZONE.equals(bizInfo.getBizType()) || "00".equals(bizInfo.getBizType())) {
                        jVar.f1603a = bizInfo;
                    }
                } else if (item.getNodeName().equalsIgnoreCase("monLevel")) {
                    jVar.f = a(item);
                } else if (item.getNodeName().equalsIgnoreCase("mobile")) {
                    jVar.e = a(item);
                }
            }
            return jVar;
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
            return null;
        } catch (ParserConfigurationException e3) {
            e3.printStackTrace();
            return null;
        } catch (SAXException e4) {
            e4.printStackTrace();
            return null;
        } catch (IOException e5) {
            e5.printStackTrace();
            return null;
        } catch (DOMException e6) {
            e6.printStackTrace();
            return null;
        }
    }

    /* access modifiers changed from: private */
    public c.b d(String str) {
        if (str == null || str.equals("")) {
            return null;
        }
        try {
            DocumentBuilderFactory newInstance = DocumentBuilderFactory.newInstance();
            if (newInstance == null) {
                return null;
            }
            DocumentBuilder newDocumentBuilder = newInstance.newDocumentBuilder();
            if (newDocumentBuilder == null) {
                return null;
            }
            Document parse = newDocumentBuilder.parse(new InputSource(new ByteArrayInputStream(str.getBytes(StringUtil.Encoding))));
            if (parse == null) {
                return null;
            }
            Element documentElement = parse.getDocumentElement();
            if (documentElement == null) {
                return null;
            }
            c.b bVar = new c.b();
            NodeList childNodes = documentElement.getChildNodes();
            if (childNodes != null) {
                int i2 = 0;
                while (true) {
                    if (i2 >= childNodes.getLength()) {
                        break;
                    }
                    Node item = childNodes.item(i2);
                    if (item.getNodeName().equalsIgnoreCase("ToneInfo")) {
                        bVar = new c.x();
                        ((c.x) bVar).f1618a = new ArrayList();
                        break;
                    } else if (item.getNodeName().equalsIgnoreCase("UserInfo")) {
                        bVar = new c.ae();
                        break;
                    } else if (item.getNodeName().equalsIgnoreCase("crbtId")) {
                        bVar = new c.f();
                        break;
                    } else if (item.getNodeName().equalsIgnoreCase("streamUrl")) {
                        bVar = new c.k();
                        break;
                    } else {
                        i2++;
                    }
                }
            }
            if (childNodes == null) {
                return null;
            }
            for (int i3 = 0; i3 < childNodes.getLength(); i3++) {
                Node item2 = childNodes.item(i3);
                if (item2.getNodeName().equalsIgnoreCase("resMsg")) {
                    bVar.c = item2.getFirstChild().getNodeValue();
                } else if (item2.getNodeName().equalsIgnoreCase("resCode")) {
                    bVar.b = item2.getFirstChild().getNodeValue();
                } else if (item2.getNodeName().equalsIgnoreCase("ToneInfo")) {
                    c.ac acVar = new c.ac();
                    for (Node firstChild = item2.getFirstChild(); firstChild != null; firstChild = firstChild.getNextSibling()) {
                        if (firstChild.getNodeName() == null || firstChild.getFirstChild() == null) {
                            com.shoujiduoduo.base.a.a.c("ChinaMobileUtils", "null pointer happens");
                        } else if (firstChild.getNodeName().equalsIgnoreCase("toneID")) {
                            acVar.f1589a = firstChild.getFirstChild().getNodeValue();
                        } else if (firstChild.getNodeName().equalsIgnoreCase("toneName")) {
                            acVar.b = firstChild.getFirstChild().getNodeValue();
                        } else if (firstChild.getNodeName().equalsIgnoreCase("singerName")) {
                            acVar.d = firstChild.getFirstChild().getNodeValue();
                        } else if (firstChild.getNodeName().equalsIgnoreCase("price")) {
                            acVar.f = firstChild.getFirstChild().getNodeValue();
                        } else if (firstChild.getNodeName().equalsIgnoreCase("toneValidDay")) {
                            acVar.g = firstChild.getFirstChild().getNodeValue();
                        } else if (firstChild.getNodeName().equalsIgnoreCase("tonePreListenAddress")) {
                            acVar.i = firstChild.getFirstChild().getNodeValue();
                        } else if (firstChild.getNodeName().equalsIgnoreCase("toneType")) {
                            acVar.j = firstChild.getFirstChild().getNodeValue();
                        }
                    }
                    ((c.x) bVar).f1618a.add(acVar);
                } else if (item2.getNodeName().equalsIgnoreCase("UserInfo")) {
                    for (Node firstChild2 = item2.getFirstChild(); firstChild2 != null; firstChild2 = firstChild2.getNextSibling()) {
                        if (firstChild2.getNodeName().equalsIgnoreCase("memLevel")) {
                            ((c.ae) bVar).f1591a = firstChild2.getFirstChild().getNodeValue();
                        }
                    }
                } else if (item2.getNodeName().equalsIgnoreCase("crbtId")) {
                    ((c.f) bVar).f1599a = item2.getFirstChild().getNodeValue();
                } else if (item2.getNodeName().equalsIgnoreCase("streamUrl")) {
                    ((c.k) bVar).f1604a = item2.getFirstChild().getNodeValue();
                    ((c.k) bVar).d = 128;
                }
            }
            return bVar;
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
            return null;
        } catch (ParserConfigurationException e3) {
            e3.printStackTrace();
            return null;
        } catch (SAXException e4) {
            e4.printStackTrace();
            return null;
        } catch (IOException e5) {
            e5.printStackTrace();
            return null;
        } catch (DOMException e6) {
            e6.printStackTrace();
            return null;
        }
    }

    public String b() {
        String token = GetAppInfoInterface.getToken(RingDDApp.c());
        String imsi = GetAppInfoInterface.getIMSI(RingDDApp.c());
        if (av.c(imsi)) {
            imsi = GetAppInfo.getIMSIbyFile(RingDDApp.c());
        }
        if (!av.c(token)) {
            return token;
        }
        return av.c(imsi) ? f.a() : imsi;
    }

    public void a(com.shoujiduoduo.util.b.a aVar) {
        com.shoujiduoduo.base.a.a.a("ChinaMobileUtils", "init check");
        this.i = a.checking;
        i.a(new f(this, aVar));
    }

    public void b(com.shoujiduoduo.util.b.a aVar) {
        com.shoujiduoduo.base.a.a.a("ChinaMobileUtils", "sms initCheck");
        this.i = a.checking;
        i.a(new g(this, aVar));
    }

    /* compiled from: ChinaMobileUtils */
    public static class c extends Result {

        /* renamed from: a  reason: collision with root package name */
        private String f1638a;

        public String a() {
            return this.f1638a;
        }

        public void a(String str) {
            this.f1638a = str;
        }

        public String toString() {
            return "SmsLoginInfoRsp [mobile=" + this.f1638a + ", getResCode()=" + getResCode() + ", getResMsg()=" + getResMsg() + "]";
        }
    }

    private boolean a(Context context) {
        return GetAppInfoInterface.getToken(context).length() > 0;
    }

    /* access modifiers changed from: private */
    public c b(Context context) {
        if (!a(context)) {
            com.shoujiduoduo.base.a.a.a("ChinaMobileUtils", "smsAuthLoginValidate, token is not exist");
            c cVar = new c();
            cVar.setResCode("999005");
            cVar.setResMsg("【OPEN】业务参数为空");
            return cVar;
        }
        try {
            String a2 = n.a(context, "http://218.200.227.123:90/wapServer/1.0/crbt/smsAuthLoginValidate", e("<token>" + GetAppInfo.getToken(context) + "</token>").getBytes("UTF-8"), false);
            com.shoujiduoduo.base.a.a.a("ChinaMobileUtils", "smsvalidate return:" + a2);
            return b(a2);
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public a c() {
        return this.i;
    }

    public boolean d() {
        return this.f;
    }

    public void a(Activity activity, com.shoujiduoduo.util.b.a aVar) {
        i.a(new h(this, activity, aVar));
    }

    public void c(com.shoujiduoduo.util.b.a aVar) {
        com.shoujiduoduo.base.a.a.a("ChinaMobileUtils", "移动sdk准备初始化");
        this.i = a.initializing;
        i.a(new j(this, aVar));
    }

    public void a(String str, com.shoujiduoduo.util.b.a aVar) {
        com.shoujiduoduo.base.a.a.a("ChinaMobileUtils", "getValidateCode");
        i.a(new k(this, str, aVar));
    }

    public void a(String str, String str2, com.shoujiduoduo.util.b.a aVar) {
        com.shoujiduoduo.base.a.a.a("ChinaMobileUtils", "smsLoginAuth");
        i.a(new l(this, str, str2, aVar));
    }

    public void b(String str, String str2, com.shoujiduoduo.util.b.a aVar) {
        a(e("<musicId>" + str + "</musicId>"), "/crbt/simpOrder", aVar, str2, false);
    }

    public void c(String str, String str2, com.shoujiduoduo.util.b.a aVar) {
        i.a(new c(this, str, str2, aVar));
    }

    public void d(String str, String str2, com.shoujiduoduo.util.b.a aVar) {
        a(e("<musicId>" + str + "</musicId>"), "/crbt/order", aVar, str2, false);
    }

    public void e(String str, String str2, com.shoujiduoduo.util.b.a aVar) {
        g(e("<receivemdn>" + str + "</receivemdn><musicId>" + str2 + "</musicId>"), "/crbt/present&user=" + f.a(), aVar);
    }

    public void f(String str, String str2, com.shoujiduoduo.util.b.a aVar) {
        g(e("<receivemdn>" + str + "</receivemdn><musicId>" + str2 + "</musicId>"), "/crbt/present", aVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.util.c.b.a(java.lang.String, java.lang.String, com.shoujiduoduo.util.b.a, boolean):void
     arg types: [java.lang.String, java.lang.String, com.shoujiduoduo.util.b.a, int]
     candidates:
      com.shoujiduoduo.util.c.b.a(com.shoujiduoduo.util.c.b, com.shoujiduoduo.util.b.c$b, java.lang.String, java.lang.String):void
      com.shoujiduoduo.util.c.b.a(com.shoujiduoduo.util.c.b, java.lang.String, com.shoujiduoduo.util.b.c$b, java.lang.String):void
      com.shoujiduoduo.util.c.b.a(com.shoujiduoduo.util.c.b, java.lang.String, java.lang.String, java.lang.String):void
      com.shoujiduoduo.util.c.b.a(java.lang.String, java.lang.String, com.shoujiduoduo.util.b.a, boolean):void */
    private void g(String str, String str2, com.shoujiduoduo.util.b.a aVar) {
        a(str, str2, aVar, false);
    }

    private void a(String str, String str2, com.shoujiduoduo.util.b.a aVar, boolean z) {
        a(str, str2, aVar, "", z);
    }

    private void a(String str, String str2, com.shoujiduoduo.util.b.a aVar, String str3, boolean z) {
        i.a(new d(this, str, z, str2, str3, aVar));
    }

    private void b(String str, String str2, com.shoujiduoduo.util.b.a aVar, boolean z) {
        i.a(new e(this, str, z, str2, aVar));
    }

    /* access modifiers changed from: private */
    public void a(String str, c.b bVar, String str2) {
        boolean z = true;
        if (str.equals("/crbt/open/check")) {
            a(bVar, "cm_cailing_check", str2);
            HashMap<String, Boolean> hashMap = this.c;
            String str3 = this.d;
            if (!bVar.c()) {
                z = false;
            }
            hashMap.put(str3, Boolean.valueOf(z));
        } else if (str.equals("/crbt/openmonth")) {
            a(bVar, "cm_open_vip", str2);
            HashMap<String, Boolean> hashMap2 = this.b;
            String str4 = this.d;
            if (!bVar.c()) {
                z = false;
            }
            hashMap2.put(str4, Boolean.valueOf(z));
            if (bVar.c()) {
                this.f1635a.put(this.d, new C0029b(this.e, System.currentTimeMillis()));
            }
        } else if (str.equals("/crbt/querymonth")) {
            a(bVar, "cm_query_month", str2);
            HashMap<String, Boolean> hashMap3 = this.b;
            String str5 = this.d;
            if (!bVar.c()) {
                z = false;
            }
            hashMap3.put(str5, Boolean.valueOf(z));
            if (bVar.c()) {
                this.f1635a.put(this.d, new C0029b(this.e, System.currentTimeMillis()));
            }
        } else if (str.equals("/crbt/order")) {
            a(bVar, "cm_buy", str2);
            if (bVar.c()) {
                this.f1635a.put(this.d, new C0029b(this.e, System.currentTimeMillis()));
            }
        } else if (str.equals("/crbt/simpOrder")) {
            a(bVar, "cm_buy_with_open", str2);
            if (bVar.c()) {
                this.f1635a.put(this.d, new C0029b(this.e, System.currentTimeMillis()));
            }
        } else if (str.equals("/crbt/present")) {
            a(bVar, "cm_give", str2);
        } else if (str.equals("/crbt/box/default")) {
            a(bVar, "cm_set_default", str2);
        } else if (str.equals("/crbt/box/query")) {
            a(bVar, "cm_box_query", str2);
        } else if (str.equals("/crbt/open")) {
            a(bVar, "cm_open_cailing", str2);
            if (bVar.c()) {
                this.f1635a.put(this.d, new C0029b(this.e, System.currentTimeMillis()));
            }
        } else if (str.equals("/crbt/smsLoginAuth")) {
            a(bVar, "cm_sms_login_web", str2);
            if (bVar.c()) {
                this.f1635a.put(this.d, new C0029b(this.e, System.currentTimeMillis()));
            } else {
                this.f1635a.remove(this.d);
            }
        } else if (str.equals("/crbt/getValidateCode")) {
            a(bVar, "cm_get_validate_code", str2);
        }
    }

    /* access modifiers changed from: private */
    public void a(c.b bVar, String str, String str2) {
        if (bVar.c()) {
            a(str, "success", str2);
        } else {
            a(str, "fail, " + bVar.toString(), str2);
        }
    }

    public void b(String str, com.shoujiduoduo.util.b.a aVar) {
        a("", "/crbt/open", aVar, str, false);
    }

    public void c(String str, com.shoujiduoduo.util.b.a aVar) {
        a(e("<serviceId>600927020000006624</serviceId>"), "/crbt/openmonth", aVar, str, false);
    }

    public void d(com.shoujiduoduo.util.b.a aVar) {
        if (this.l) {
            com.shoujiduoduo.base.a.a.a("ChinaMobileUtils", "彩铃处于开通状态，直接返回缓存状态");
            c.b bVar = new c.b();
            bVar.b = "000000";
            bVar.c = "包月处于开通状态";
            aVar.g(bVar);
            return;
        }
        g(e("<serviceId>600927020000006624</serviceId>"), "/crbt/querymonth", aVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.util.c.b.a(java.lang.String, java.lang.String, com.shoujiduoduo.util.b.a, boolean):void
     arg types: [java.lang.String, java.lang.String, com.shoujiduoduo.util.b.a, int]
     candidates:
      com.shoujiduoduo.util.c.b.a(com.shoujiduoduo.util.c.b, com.shoujiduoduo.util.b.c$b, java.lang.String, java.lang.String):void
      com.shoujiduoduo.util.c.b.a(com.shoujiduoduo.util.c.b, java.lang.String, com.shoujiduoduo.util.b.c$b, java.lang.String):void
      com.shoujiduoduo.util.c.b.a(com.shoujiduoduo.util.c.b, java.lang.String, java.lang.String, java.lang.String):void
      com.shoujiduoduo.util.c.b.a(java.lang.String, java.lang.String, com.shoujiduoduo.util.b.a, boolean):void */
    public void d(String str, com.shoujiduoduo.util.b.a aVar) {
        if (!this.b.containsKey(str) || !this.b.get(str).booleanValue()) {
            a(e("<serviceId>600927020000006624</serviceId><MSISDN>" + str + "</MSISDN>"), "/crbt/querymonth", aVar, true);
            return;
        }
        com.shoujiduoduo.base.a.a.a("ChinaMobileUtils", "彩铃处于开通状态，直接返回缓存状态");
        c.b bVar = new c.b();
        bVar.b = "000000";
        bVar.c = "包月处于开通状态";
        aVar.g(bVar);
    }

    public void e(String str, com.shoujiduoduo.util.b.a aVar) {
        this.d = str;
        b(e("<serviceId>600927020000006624</serviceId><MSISDN>" + str + "</MSISDN>"), e("<MSISDN>" + str + "</MSISDN>"), aVar, true);
    }

    public void e(com.shoujiduoduo.util.b.a aVar) {
        b(e("<serviceId>600927020000006624</serviceId>"), e("<MSISDN></MSISDN>"), aVar, false);
    }

    public void f(com.shoujiduoduo.util.b.a aVar) {
        g(e("<serviceId>600927020000006624</serviceId>"), "/crbt/orderbackmonth", aVar);
    }

    public c.b e() {
        try {
            byte[] bytes = "".getBytes("UTF-8");
            if (this.g == null) {
                com.shoujiduoduo.base.a.a.c("ChinaMobileUtils", "context is null, main activity is destroyed");
                return null;
            }
            String a2 = n.a(this.g, "http://mm.shoujiduoduo.com/mm/mm.php?from=ringdd_ar&cmd=" + URLEncoder.encode("/crbt/box/query", "UTF-8"), bytes, false);
            if (a2 != null) {
                return d(a2);
            }
            com.shoujiduoduo.base.a.a.c("ChinaMobileUtils", "queryRingBox, return null");
            return null;
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
            com.shoujiduoduo.base.a.a.c("ChinaMobileUtils", "queryRingBox, exception");
            return null;
        }
    }

    public void g(com.shoujiduoduo.util.b.a aVar) {
        g("", "/crbt/box/query", aVar);
    }

    public void f(String str, com.shoujiduoduo.util.b.a aVar) {
        g(e("<crbtId>" + str + "</crbtId>"), "/crbt/box/default", aVar);
    }

    public void g(String str, com.shoujiduoduo.util.b.a aVar) {
        g(e("<crbtId>" + str + "</crbtId>"), "/crbt/box/delete", aVar);
    }

    public void a(String str, com.shoujiduoduo.util.b.a aVar, boolean z) {
        a(e("<MSISDN>" + str + "</MSISDN>"), "/crbt/msisdn/query", aVar, z);
    }

    public void b(String str, com.shoujiduoduo.util.b.a aVar, boolean z) {
        a(e("<MSISDN>" + str + "</MSISDN>"), "/crbt/open/check", aVar, z);
    }

    public c.b a(String str, boolean z) {
        try {
            byte[] bytes = e("<MSISDN>" + str + "</MSISDN>").getBytes("UTF-8");
            if (this.g == null) {
                com.shoujiduoduo.base.a.a.c("ChinaMobileUtils", "context is null, main activity is destroyed");
                return null;
            }
            String a2 = n.a(this.g, (z ? "http://mm.shoujiduoduo.com/mm/mmweb.php?from=ringdd_ar&cmd=" : "http://mm.shoujiduoduo.com/mm/mm.php?from=ringdd_ar&cmd=") + URLEncoder.encode("/crbt/open/check", "UTF-8"), bytes, z);
            if (a2 != null) {
                return d(a2);
            }
            com.shoujiduoduo.base.a.a.c("ChinaMobileUtils", "openCheckSync, return null");
            return null;
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
            com.shoujiduoduo.base.a.a.c("ChinaMobileUtils", "openCheckSync, exception");
            return null;
        }
    }

    private String e(String str) {
        return "<?xml version='1.0' encoding='UTF-8'?><request>" + str + "</request>";
    }

    /* access modifiers changed from: private */
    public void a(String str, String str2, String str3) {
        w.a("cm:" + str, str2, str3);
    }
}
