package com.shoujiduoduo.util;

import android.os.Environment;
import android.text.TextUtils;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.ringtone.RingDDApp;
import java.io.File;

/* compiled from: DirManager */
public class l {

    /* renamed from: a  reason: collision with root package name */
    public static String f1677a;
    public static String b;
    private static String[] c = new String[50];
    private static String[] d = new String[50];

    public static String a(int i) {
        String str = c[i];
        if (str == null) {
            str = "";
            switch (i) {
                case 0:
                    str = c();
                    break;
                case 1:
                    str = d();
                    break;
                case 2:
                    str = d() + "cache/";
                    break;
                case 3:
                    str = d() + "recordings/";
                    break;
                case 4:
                    str = d() + "log/";
                    break;
                case 5:
                    File filesDir = RingDDApp.b().getFilesDir();
                    if (filesDir == null) {
                        File dir = RingDDApp.b().getDir("lib", 0);
                        if (dir != null) {
                            str = t.e(dir.getAbsolutePath()) + File.separator + "lib";
                            break;
                        }
                    } else {
                        str = t.e(filesDir.getAbsolutePath()) + File.separator + "lib";
                        break;
                    }
                    break;
                case 6:
                    str = d() + "temp/";
                    break;
                case 7:
                    str = d() + "splash_pic/";
                    break;
            }
            if (!TextUtils.isEmpty(str) && !str.endsWith(File.separator)) {
                str = str + File.separator;
            }
            if (i < 50) {
                c[i] = str;
            }
            File file = new File(str);
            if (!file.exists()) {
                try {
                    file.mkdirs();
                } catch (Exception e) {
                }
            }
        }
        return str;
    }

    public static String b(int i) {
        String str = d[i];
        if (str == null) {
            str = "";
            switch (i) {
                case 0:
                    str = d() + "config.tmp";
                    break;
                case 1:
                    str = d() + "adconfig.tmp";
                    break;
                case 2:
                    str = d() + "make_ring.xml";
                    break;
                case 3:
                    str = d() + "user_ring.xml";
                    break;
                case 4:
                    str = d() + "collect_ring.xml";
                    break;
            }
            if (i < 50) {
                d[i] = str;
            }
        }
        return str;
    }

    public static boolean a() {
        boolean z;
        boolean z2;
        if (f1677a == null) {
            f1677a = d();
        }
        File file = new File(a(2));
        boolean z3 = file.isDirectory() || file.mkdirs();
        File file2 = new File(a(4));
        if (file2.isDirectory() || file2.mkdirs()) {
            z = true;
        } else {
            z = false;
        }
        File file3 = new File(a(3));
        if (file3.isDirectory() || file3.mkdirs()) {
            z2 = true;
        } else {
            z2 = false;
        }
        if (!z3 || !z || !z2) {
            return false;
        }
        return true;
    }

    private static String c() {
        File externalStorageDirectory;
        if (b == null && Environment.getExternalStorageState().equals("mounted") && (externalStorageDirectory = Environment.getExternalStorageDirectory()) != null) {
            b = externalStorageDirectory.toString();
        }
        return b;
    }

    private static String d() {
        if (f1677a == null && Environment.getExternalStorageState().equals("mounted")) {
            String str = Environment.getExternalStorageDirectory().toString() + "/shoujiduoduo/Ring/";
            a.a("DirManager", "DirManager: soft dir = " + str);
            File file = new File(str);
            if (file.isDirectory()) {
                f1677a = str;
            } else if (file.mkdirs()) {
                f1677a = str;
            }
        }
        return f1677a;
    }

    public static boolean b() {
        try {
            return "mounted".equals(Environment.getExternalStorageState());
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
