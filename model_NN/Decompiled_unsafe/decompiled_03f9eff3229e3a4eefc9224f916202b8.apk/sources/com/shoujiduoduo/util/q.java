package com.shoujiduoduo.util;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.widget.RemoteViews;
import com.shoujiduoduo.a.c.g;
import com.shoujiduoduo.ringtone.R;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import org.apache.mina.proxy.handlers.http.ntlm.NTLMConstants;

/* compiled from: DownloadSoftManager */
public class q {
    private static q b = null;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Context f1687a;
    private int c = 0;
    /* access modifiers changed from: private */
    public boolean d;
    /* access modifiers changed from: private */
    public HashMap<String, b> e = new HashMap<>();

    /* compiled from: DownloadSoftManager */
    public enum a {
        f1688a,
        immediatelly,
        notifybar
    }

    private q(Context context) {
        this.f1687a = context;
    }

    public static synchronized q a(Context context) {
        q qVar;
        synchronized (q.class) {
            if (b == null) {
                b = new q(context);
            }
            qVar = b;
        }
        return qVar;
    }

    public void a(String str, String str2) {
        a(str, str2, a.f1688a, false);
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(java.lang.String r8, java.lang.String r9, com.shoujiduoduo.util.q.a r10, boolean r11) {
        /*
            r7 = this;
            java.util.HashMap<java.lang.String, com.shoujiduoduo.util.q$b> r6 = r7.e
            monitor-enter(r6)
            java.net.URL r0 = new java.net.URL     // Catch:{ MalformedURLException -> 0x0030 }
            r0.<init>(r8)     // Catch:{ MalformedURLException -> 0x0030 }
            r7.d = r11     // Catch:{ all -> 0x0036 }
            java.util.HashMap<java.lang.String, com.shoujiduoduo.util.q$b> r0 = r7.e     // Catch:{ all -> 0x0036 }
            boolean r0 = r0.containsKey(r8)     // Catch:{ all -> 0x0036 }
            if (r0 != 0) goto L_0x002e
            com.shoujiduoduo.util.q$b r0 = new com.shoujiduoduo.util.q$b     // Catch:{ all -> 0x0036 }
            int r4 = r7.c     // Catch:{ all -> 0x0036 }
            r1 = r7
            r2 = r8
            r3 = r9
            r5 = r10
            r0.<init>(r2, r3, r4, r5)     // Catch:{ all -> 0x0036 }
            java.util.HashMap<java.lang.String, com.shoujiduoduo.util.q$b> r1 = r7.e     // Catch:{ all -> 0x0036 }
            r1.put(r8, r0)     // Catch:{ all -> 0x0036 }
            int r1 = r7.c     // Catch:{ all -> 0x0036 }
            int r1 = r1 + 1
            r7.c = r1     // Catch:{ all -> 0x0036 }
            r1 = 0
            java.lang.Void[] r1 = new java.lang.Void[r1]     // Catch:{ all -> 0x0036 }
            r0.execute(r1)     // Catch:{ all -> 0x0036 }
        L_0x002e:
            monitor-exit(r6)     // Catch:{ all -> 0x0036 }
        L_0x002f:
            return
        L_0x0030:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x0036 }
            monitor-exit(r6)     // Catch:{ all -> 0x0036 }
            goto L_0x002f
        L_0x0036:
            r0 = move-exception
            monitor-exit(r6)     // Catch:{ all -> 0x0036 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.util.q.a(java.lang.String, java.lang.String, com.shoujiduoduo.util.q$a, boolean):void");
    }

    /* compiled from: DownloadSoftManager */
    class b extends AsyncTask<Void, Integer, Boolean> implements g {

        /* renamed from: a  reason: collision with root package name */
        public long f1689a = -1;
        public long b = 0;
        public int c = 0;
        private final String e = b.class.getSimpleName();
        private String f;
        private String g;
        private String h;
        private String i;
        private int j;
        private boolean k = false;
        private a l = a.f1688a;
        private final int m = 1922;
        private final int n = 2922;
        private final int o = 3922;
        private NotificationManager p = null;
        private Notification q = null;
        private Notification r = null;
        private Notification s = null;

        public b(String str, String str2, int i2, a aVar) {
            com.shoujiduoduo.base.a.a.a(this.e, "UpdateTask start!");
            this.f = str;
            this.g = str2;
            this.c = i2;
            this.l = aVar;
            this.p = (NotificationManager) q.this.f1687a.getSystemService("notification");
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.util.as.b(android.content.Context, java.lang.String, long):boolean
         arg types: [android.content.Context, java.lang.String, int]
         candidates:
          com.shoujiduoduo.util.as.b(android.content.Context, java.lang.String, int):boolean
          com.shoujiduoduo.util.as.b(android.content.Context, java.lang.String, java.lang.String):boolean
          com.shoujiduoduo.util.as.b(android.content.Context, java.lang.String, long):boolean */
        private void c() {
            this.f1689a = -1;
            as.b(q.this.f1687a, this.h + ":total", -1L);
            this.b = 0;
            as.b(q.this.f1687a, this.h + ":current", 0L);
            k.a(this.i);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.util.as.a(android.content.Context, java.lang.String, long):long
         arg types: [android.content.Context, java.lang.String, int]
         candidates:
          com.shoujiduoduo.util.as.a(android.content.Context, java.lang.String, int):int
          com.shoujiduoduo.util.as.a(android.content.Context, java.lang.String, java.lang.String):java.lang.String
          com.shoujiduoduo.util.as.a(android.content.Context, java.lang.String, long):long */
        private boolean d() {
            boolean z = false;
            this.h = this.f.substring(this.f.lastIndexOf("/") + 1);
            this.h = this.h.toLowerCase();
            if (!this.h.endsWith(".apk")) {
                com.shoujiduoduo.base.a.a.a(this.e, "url not end with apk, user url hash to cache");
                this.h = this.f.hashCode() + ".apk";
            }
            com.shoujiduoduo.base.a.a.a(this.e, "mCacheName = " + this.h);
            this.i = s.b() + this.h;
            com.shoujiduoduo.base.a.a.a(this.e, "download soft: cachePath = " + this.i);
            this.f1689a = as.a(q.this.f1687a, this.h + ":total", -1L);
            this.b = as.a(q.this.f1687a, this.h + ":current", 0L);
            com.shoujiduoduo.base.a.a.a(this.e, "download soft: totalLength = " + this.f1689a + "; currentLength = " + this.b);
            this.k = true;
            if (this.f1689a > 0 && this.b >= 0) {
                if (this.b <= this.f1689a) {
                    File file = new File(this.i);
                    if (!file.exists() || !file.canWrite() || !file.isFile()) {
                        this.b = 0;
                        this.f1689a = -1;
                        z = true;
                    } else if (this.b == this.f1689a) {
                        if (file.length() == this.b) {
                            this.k = false;
                        } else {
                            this.b = 0;
                            this.f1689a = -1;
                            z = true;
                        }
                    }
                } else {
                    this.b = 0;
                    this.f1689a = -1;
                    z = true;
                }
                if (z) {
                    c();
                }
            }
            return true;
        }

        private void b(int i2) {
            String str = this.g;
            Intent intent = new Intent(q.this.f1687a, q.this.f1687a.getClass());
            intent.addFlags(NTLMConstants.FLAG_NEGOTIATE_128_BIT_ENCRYPTION);
            this.s = new Notification(R.drawable.icon_download, str, System.currentTimeMillis());
            com.shoujiduoduo.base.a.a.a(this.e, "package name: " + q.this.f1687a.getPackageName());
            this.s.contentIntent = PendingIntent.getActivity(q.this.f1687a, 0, intent, 0);
            this.s.contentView = new RemoteViews(q.this.f1687a.getPackageName(), (int) R.layout.download_notif);
            this.s.contentView.setProgressBar(R.id.down_progress_bar, 100, i2, false);
            this.s.contentView.setTextViewText(R.id.down_tv, "正在下载" + this.g);
            this.p.notify(this.c + 1922, this.s);
        }

        public boolean a(String str, String str2, long j2, g gVar) {
            com.shoujiduoduo.base.a.a.a(this.e, "download soft: url = " + str);
            com.shoujiduoduo.base.a.a.a(this.e, "download soft: path = " + str2);
            com.shoujiduoduo.base.a.a.a(this.e, "start_pos = " + j2);
            try {
                URL url = new URL(str);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                com.shoujiduoduo.base.a.a.a(this.e, "download soft: conn = " + httpURLConnection.toString());
                httpURLConnection.setRequestProperty("RANGE", "bytes=" + j2 + "-");
                httpURLConnection.connect();
                com.shoujiduoduo.base.a.a.a(this.e, "download soft: connect finished!");
                int contentLength = httpURLConnection.getContentLength();
                if (contentLength <= 0) {
                    com.shoujiduoduo.base.a.a.a(this.e, "download soft: filesize Error! filesize= " + contentLength);
                    if (gVar != null) {
                        gVar.a(0);
                    }
                    return false;
                }
                if (!(j2 == 0 || this.f1689a <= 0 || ((long) contentLength) + j2 == this.f1689a)) {
                    com.shoujiduoduo.base.a.a.a(this.e, "Error! the URL filesize changed! filesize = " + contentLength + ", start = " + j2 + ", totalsize = " + this.f1689a);
                    c();
                    j2 = 0;
                    httpURLConnection.disconnect();
                    httpURLConnection = (HttpURLConnection) url.openConnection();
                    com.shoujiduoduo.base.a.a.a(this.e, "download soft: conn = " + httpURLConnection.toString());
                    httpURLConnection.setRequestProperty("RANGE", "bytes=" + 0L + "-");
                    httpURLConnection.connect();
                    com.shoujiduoduo.base.a.a.a(this.e, "download soft: connect finished!");
                    contentLength = httpURLConnection.getContentLength();
                    if (contentLength <= 0) {
                        com.shoujiduoduo.base.a.a.a(this.e, "download soft: filesize Error! filesize= " + contentLength);
                        if (gVar != null) {
                            gVar.a(0);
                        }
                        return false;
                    }
                }
                int i2 = contentLength;
                HttpURLConnection httpURLConnection2 = httpURLConnection;
                long j3 = j2;
                if (gVar != null) {
                    gVar.b(((long) i2) + j3);
                }
                if (isCancelled()) {
                    if (gVar != null) {
                        gVar.b();
                    }
                    return true;
                }
                com.shoujiduoduo.base.a.a.a(this.e, "download soft: filesize = " + i2);
                InputStream inputStream = httpURLConnection2.getInputStream();
                RandomAccessFile randomAccessFile = new RandomAccessFile(str2, "rw");
                randomAccessFile.seek(j3);
                byte[] bArr = new byte[10240];
                while (true) {
                    int read = inputStream.read(bArr, 0, 10240);
                    if (read <= 0) {
                        break;
                    }
                    randomAccessFile.write(bArr, 0, read);
                    j3 += (long) read;
                    if (gVar != null) {
                        gVar.a(j3);
                    }
                    if (isCancelled()) {
                        if (gVar != null) {
                            gVar.b();
                        }
                    }
                }
                randomAccessFile.close();
                httpURLConnection2.disconnect();
                if (gVar != null && !isCancelled()) {
                    gVar.a();
                }
                return true;
            } catch (MalformedURLException e2) {
                e2.printStackTrace();
                if (gVar != null) {
                    gVar.a(0);
                }
                return false;
            } catch (IOException e3) {
                e3.printStackTrace();
                if (gVar != null) {
                    gVar.a(0);
                }
                return false;
            }
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            com.shoujiduoduo.base.a.a.a(this.e, "UPdateTask: onPreExecute");
            b(0);
        }

        /* access modifiers changed from: protected */
        public void onCancelled() {
            synchronized (q.this.e) {
                q.this.e.remove(this.f);
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public Boolean doInBackground(Void... voidArr) {
            com.shoujiduoduo.base.a.a.a(this.e, "UPdateTask: doInBackground");
            if (!d()) {
                com.shoujiduoduo.base.a.a.a(this.e, "getCacheInfo failed");
                return Boolean.FALSE;
            } else if (!this.k) {
                publishProgress(100, 100);
                new Thread(new r(this, this.f)).start();
                com.shoujiduoduo.base.a.a.a(this.e, "已经下载完毕，不需要下载了");
                return Boolean.TRUE;
            } else {
                com.shoujiduoduo.base.a.a.a(this.e, "准备开始下载");
                return Boolean.valueOf(a(this.f, this.i, this.b, this));
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onPostExecute(Boolean bool) {
            synchronized (q.this.e) {
                this.p.cancel(this.c + 1922);
                if (!bool.booleanValue()) {
                    Intent intent = new Intent(q.this.f1687a, q.this.f1687a.getClass());
                    intent.addFlags(NTLMConstants.FLAG_NEGOTIATE_128_BIT_ENCRYPTION);
                    intent.putExtra("update_fail", "yes");
                    PendingIntent activity = PendingIntent.getActivity(q.this.f1687a, 0, intent, 0);
                    this.q = new Notification(R.drawable.duoduo_icon, "下载失败", System.currentTimeMillis());
                    this.q.flags |= 16;
                    this.q.setLatestEventInfo(q.this.f1687a, this.g, this.g + "下载失败，请稍后再试", activity);
                    this.p.notify(this.c + 2922, this.q);
                } else if (this.l == a.f1688a) {
                    String a2 = am.a().a("ad_install_immediately");
                    if (a2 == null) {
                        a2 = "true";
                    }
                    if (a2.equalsIgnoreCase("false")) {
                        e();
                    } else {
                        f.b(this.i);
                    }
                } else if (this.l == a.immediatelly) {
                    f.b(this.i);
                } else if (this.l == a.notifybar) {
                    if (q.this.d) {
                        Intent intent2 = new Intent();
                        intent2.setAction("install_apk_from_start_ad");
                        intent2.putExtra("PackagePath", this.i);
                        intent2.putExtra("PackageName", this.g);
                        if (q.this.f1687a != null) {
                            q.this.f1687a.sendBroadcast(intent2);
                            com.shoujiduoduo.base.a.a.a(this.e, "send broadcast");
                        }
                    }
                    e();
                }
                q.this.e.remove(this.f);
            }
        }

        private void e() {
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setFlags(NTLMConstants.FLAG_UNIDENTIFIED_11);
            intent.setDataAndType(Uri.fromFile(new File(this.i)), "application/vnd.android.package-archive");
            intent.putExtra("down_finish", "yes");
            PendingIntent activity = PendingIntent.getActivity(q.this.f1687a, 0, intent, 0);
            this.r = new Notification(R.drawable.duoduo_icon, "下载完毕", System.currentTimeMillis());
            this.r.flags |= 16;
            this.r.setLatestEventInfo(q.this.f1687a, this.g, this.g + "下载完成，点击安装", activity);
            this.p.notify(this.c + 3922, this.r);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onProgressUpdate(Integer... numArr) {
            if (!isCancelled()) {
                com.shoujiduoduo.base.a.a.a(this.e, "onProgressUpdate: progress = " + numArr[1]);
                if (this.s != null) {
                    this.s.contentView.setProgressBar(R.id.down_progress_bar, 100, numArr[1].intValue(), false);
                    this.p.notify(this.c + 1922, this.s);
                }
            }
        }

        public void a(long j2) {
            this.b = j2;
            int i2 = (int) ((((float) this.b) * 100.0f) / ((float) this.f1689a));
            if (this.j != i2) {
                publishProgress(100, Integer.valueOf(i2));
                if (i2 > 0 && i2 % 5 == 0) {
                    as.b(q.this.f1687a, this.h + ":current", j2);
                }
                this.j = i2;
            }
        }

        public void a(int i2) {
        }

        public void a() {
            as.b(q.this.f1687a, this.h + ":current", this.f1689a);
            publishProgress(100, 100);
        }

        public void b(long j2) {
            as.b(q.this.f1687a, this.h + ":total", j2);
            if (this.f1689a != j2) {
                this.f1689a = j2;
                this.b = 0;
            }
        }

        public void b() {
        }
    }
}
