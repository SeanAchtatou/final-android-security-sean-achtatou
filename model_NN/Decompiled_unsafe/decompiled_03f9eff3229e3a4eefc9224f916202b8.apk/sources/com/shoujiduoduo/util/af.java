package com.shoujiduoduo.util;

import android.app.ActivityManager;
import android.os.Process;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.xiaomi.mipush.sdk.b;
import com.xiaomi.mipush.sdk.c;
import java.util.List;

/* compiled from: MiPushUtil */
public class af {
    public static void a() {
        String a2 = am.a().a("mi_push_enable");
        if (f.m() || a2.equals("true")) {
            if (b()) {
                c.a(RingDDApp.c(), "2882303761517124001", "5621712441001");
                c.a(RingDDApp.c(), 9, 0, 23, 0, null);
            }
            if (a.f812a) {
                a.a("MiPushUtil", "RegId: " + c.g(RingDDApp.c()));
            }
            b.a(RingDDApp.c(), new ag());
        }
    }

    private static boolean b() {
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses;
        ActivityManager activityManager = (ActivityManager) RingDDApp.c().getSystemService("activity");
        if (!(activityManager == null || (runningAppProcesses = activityManager.getRunningAppProcesses()) == null)) {
            String packageName = RingDDApp.c().getPackageName();
            int myPid = Process.myPid();
            for (ActivityManager.RunningAppProcessInfo next : runningAppProcesses) {
                if (next.pid == myPid && packageName.equals(next.processName)) {
                    return true;
                }
            }
        }
        return false;
    }
}
