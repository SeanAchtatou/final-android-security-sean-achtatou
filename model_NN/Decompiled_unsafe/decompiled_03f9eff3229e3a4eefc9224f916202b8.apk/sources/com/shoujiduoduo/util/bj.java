package com.shoujiduoduo.util;

import com.shoujiduoduo.base.a.a;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.zip.GZIPInputStream;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

/* compiled from: http */
public class bj {

    /* renamed from: a  reason: collision with root package name */
    private static final String f1632a = bj.class.getSimpleName();

    public static byte[] a(String str, int i, int i2) {
        try {
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
            HttpParams params = defaultHttpClient.getParams();
            HttpConnectionParams.setConnectionTimeout(params, i);
            HttpConnectionParams.setSoTimeout(params, i2);
            HttpResponse execute = defaultHttpClient.execute(new HttpGet(str));
            int statusCode = execute.getStatusLine().getStatusCode();
            a.a(f1632a, "httpGet: http client status = " + statusCode);
            if (statusCode < 200 || statusCode >= 300) {
                a.a(f1632a, "httpGet: Request failed, status code:" + statusCode);
                return null;
            }
            HttpEntity entity = execute.getEntity();
            if (entity == null) {
                a.a(f1632a, "httpGet: Fail to getEntity");
                return null;
            }
            a.a(f1632a, "httpGet: http response entity = " + entity.toString());
            InputStream content = entity.getContent();
            if (content == null) {
                return null;
            }
            byte[] bArr = new byte[2048];
            BufferedInputStream bufferedInputStream = new BufferedInputStream(content);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            while (true) {
                int read = bufferedInputStream.read(bArr);
                if (read <= 0) {
                    break;
                }
                byteArrayOutputStream.write(bArr, 0, read);
            }
            bufferedInputStream.close();
            content.close();
            a.a(f1632a, "httpGet: content = " + byteArrayOutputStream.toString());
            if (byteArrayOutputStream.toByteArray() != null) {
                return byteArrayOutputStream.toByteArray();
            }
            return "ok".getBytes();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
            a.c("http", "httpGet: ClientProtocolException catched!");
            return null;
        } catch (IOException e2) {
            e2.printStackTrace();
            a.c("http", "httpGet: IOException catched!");
            return null;
        }
    }

    public static byte[] b(String str, int i, int i2) {
        boolean z = false;
        try {
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
            HttpParams params = defaultHttpClient.getParams();
            HttpConnectionParams.setConnectionTimeout(params, i);
            HttpConnectionParams.setSoTimeout(params, i2);
            HttpGet httpGet = new HttpGet(str);
            httpGet.addHeader("Accept-Encoding", "gzip");
            HttpResponse execute = defaultHttpClient.execute(httpGet);
            int statusCode = execute.getStatusLine().getStatusCode();
            a.a(f1632a, "httpGetGZIP: http client status = " + statusCode);
            if (statusCode < 200 || statusCode >= 300) {
                a.a(f1632a, "httpGetGZIP: Request failed, status code:" + statusCode);
                return null;
            }
            Header firstHeader = execute.getFirstHeader("Content-Encoding");
            if (firstHeader != null) {
                if (!firstHeader.getValue().equalsIgnoreCase("gzip")) {
                    a.a(f1632a, "httpGetGZIP: Request failed, response encoding is " + firstHeader.getValue());
                } else {
                    z = true;
                }
            }
            HttpEntity entity = execute.getEntity();
            if (entity == null) {
                a.a(f1632a, "httpGetGZIP: Fail to getEntity");
                return null;
            }
            InputStream content = entity.getContent();
            if (content == null) {
                return null;
            }
            byte[] bArr = new byte[2048];
            InputStream gZIPInputStream = z ? new GZIPInputStream(content) : new BufferedInputStream(content);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            while (true) {
                int read = gZIPInputStream.read(bArr);
                if (read <= 0) {
                    break;
                }
                byteArrayOutputStream.write(bArr, 0, read);
            }
            gZIPInputStream.close();
            content.close();
            if (byteArrayOutputStream.toByteArray() != null) {
                return byteArrayOutputStream.toByteArray();
            }
            return "ok".getBytes();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
            a.c("http", "httpGetGZIP: ClientProtocolException catched!");
            return null;
        } catch (IOException e2) {
            e2.printStackTrace();
            a.c("http", "httpGetGZIP: IOException catched!");
            return null;
        }
    }

    public static boolean a(String str, String str2, int i, int i2) {
        try {
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
            HttpParams params = defaultHttpClient.getParams();
            HttpConnectionParams.setConnectionTimeout(params, i);
            HttpConnectionParams.setSoTimeout(params, i2);
            HttpPost httpPost = new HttpPost(str);
            httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded");
            a.a(f1632a, "post data after Base64: " + str2);
            ArrayList arrayList = new ArrayList();
            arrayList.add(new BasicNameValuePair("content", str2));
            httpPost.setEntity(new UrlEncodedFormEntity(arrayList));
            int statusCode = defaultHttpClient.execute(httpPost).getStatusLine().getStatusCode();
            if (statusCode == 200) {
                return true;
            }
            a.c("http", "httpPost: Request failed, status code:" + statusCode);
            return false;
        } catch (ClientProtocolException e) {
            e.printStackTrace();
            a.c("http", "httpPost: ClientProtocolException catched!");
            return false;
        } catch (IOException e2) {
            e2.printStackTrace();
            a.c("http", "httpPost: IOException catched!");
            return false;
        }
    }

    public static byte[] a(String str) {
        a.a(f1632a, str);
        return a(str, 20000, 20000);
    }

    public static byte[] b(String str) {
        return b(str, 20000, 20000);
    }

    public static boolean a(String str, String str2) {
        return a(str, str2, 20000, 20000);
    }
}
