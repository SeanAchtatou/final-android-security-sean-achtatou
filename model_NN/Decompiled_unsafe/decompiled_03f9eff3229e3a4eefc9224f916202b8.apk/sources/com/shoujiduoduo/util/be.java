package com.shoujiduoduo.util;

import android.os.Handler;
import android.support.v4.BuildConfig;
import android.support.v4.view.MotionEventCompat;
import com.shoujiduoduo.player.d;
import com.shoujiduoduo.player.m;
import com.shoujiduoduo.util.a.g;
import com.shoujiduoduo.util.f.e;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

/* compiled from: WavDataProcess */
public class be implements m.d {

    /* renamed from: a  reason: collision with root package name */
    private Object f1625a;
    private long b;
    private Thread c;
    private ArrayList<short[]> d;
    private double[] e;
    private boolean f;
    /* access modifiers changed from: private */
    public String g;
    /* access modifiers changed from: private */
    public String h;
    /* access modifiers changed from: private */
    public Handler i;
    private int j;
    /* access modifiers changed from: private */
    public g k;
    /* access modifiers changed from: private */
    public float l;
    /* access modifiers changed from: private */
    public float m;
    private d n;
    /* access modifiers changed from: private */
    public int o;
    /* access modifiers changed from: private */
    public int p;
    /* access modifiers changed from: private */
    public int q;
    private int r;
    private int s;
    private Thread t;

    /* compiled from: WavDataProcess */
    public static class b {

        /* renamed from: a  reason: collision with root package name */
        public static be f1627a = new be(null);
    }

    /* synthetic */ be(bf bfVar) {
        this();
    }

    static {
        try {
            ah.a("mp3lame");
        } catch (UnsatisfiedLinkError e2) {
            com.shoujiduoduo.base.a.a.c("WavDataProcess", "加载libmp3lame失败");
            e2.printStackTrace();
        }
    }

    public static be a() {
        return b.f1627a;
    }

    private be() {
        this.j = 0;
        this.j = 0;
        this.l = 0.0f;
        this.m = 1.0f;
        this.f1625a = new Object();
        this.d = new ArrayList<>();
        com.shoujiduoduo.player.a.b().a(this);
    }

    public void a(float f2) {
        this.l = f2;
    }

    public void b(float f2) {
        this.m = f2;
    }

    public void a(m mVar, short[] sArr) {
        if (mVar != null) {
            this.j = 0;
        }
        this.b += (long) sArr.length;
        synchronized (this.f1625a) {
            if (this.d != null && sArr.length > 0) {
                this.d.add(sArr.clone());
            }
        }
    }

    public int b() {
        return this.j;
    }

    public void a(String str, Handler handler) {
        if (this.c != null && this.c.isAlive()) {
            this.c.interrupt();
            try {
                this.c.join();
            } catch (InterruptedException e2) {
                e2.printStackTrace();
            }
        }
        synchronized (this.f1625a) {
            this.d.clear();
        }
        if (!new File(str).exists()) {
            com.shoujiduoduo.base.a.a.c("WavDataProcess", "file not exist, :" + str);
        }
        this.g = str;
        com.shoujiduoduo.base.a.a.b("WavDataProcess", "local file path:" + this.g);
        this.i = handler;
        this.f = false;
        this.c = new Thread(new a(this, null));
        this.c.setName("decode local audio thread");
        this.c.start();
        this.j = 1;
    }

    /* compiled from: WavDataProcess */
    private class a implements Runnable {
        private a() {
        }

        /* synthetic */ a(be beVar, bf bfVar) {
            this();
        }

        public void run() {
            long currentTimeMillis = System.currentTimeMillis();
            com.shoujiduoduo.base.a.a.a("WavDataProcess", "begin parse local audio file");
            be.this.i.sendEmptyMessage(13);
            try {
                g unused = be.this.k = g.a(be.this.g, null);
                int unused2 = be.this.p = be.this.k.e();
                int unused3 = be.this.o = be.this.k.f();
                int unused4 = be.this.q = be.this.k.c();
                be.this.l();
                be.this.i.sendEmptyMessage(12);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                com.shoujiduoduo.base.a.a.a("WavDataProcess", "file not found exp");
                be.this.i.sendEmptyMessage(11);
            } catch (IOException e2) {
                be.this.i.sendEmptyMessage(11);
                e2.printStackTrace();
                com.shoujiduoduo.base.a.a.a("WavDataProcess", "IO exp");
            }
            com.shoujiduoduo.base.a.a.a("WavDataProcess", "end parse local audio file, cost time:" + (System.currentTimeMillis() - currentTimeMillis));
        }
    }

    public void b(String str, Handler handler) {
        this.h = str;
        this.i = handler;
        if (this.j == 0) {
            new Thread(new bf(this, str, handler)).start();
        } else {
            new bg(this, str, handler).start();
        }
    }

    public int c() {
        double d2 = 0.0d;
        if (this.j == 0) {
            return com.shoujiduoduo.player.a.b().c();
        }
        if (this.k == null) {
            return 0;
        }
        double e2 = (double) this.k.e();
        if (e2 > 0.0d) {
            d2 = (((double) this.k.c()) * 1.0d) / e2;
        }
        return (int) (d2 * ((double) this.k.b()) * 1000.0d);
    }

    public int d() {
        return (int) (((float) c()) * (this.m - this.l));
    }

    public String e() {
        if (this.j == 0) {
            return com.shoujiduoduo.player.a.b().d();
        }
        return this.g;
    }

    public int a(double d2) {
        return (int) ((((1.0d * d2) * ((double) this.p)) / ((double) this.q)) + 0.5d);
    }

    public int a(int i2) {
        if (this.j == 0) {
            File file = new File(com.shoujiduoduo.player.a.b().d());
            return (int) (((float) (file.length() - 44)) * (((float) i2) / ((float) this.r)));
        }
        int a2 = a(((double) i2) * 0.001d);
        if (this.k != null) {
            return this.k.a_(a2);
        }
        return 0;
    }

    /* access modifiers changed from: private */
    public void a(String str, String str2, Handler handler) {
        try {
            handler.sendEmptyMessage(1);
            if (str.equals(str2)) {
                handler.sendEmptyMessage(3);
            } else if (this.l == 0.0f && this.m == 1.0f) {
                File file = new File(str);
                File file2 = new File(str2);
                if (file.getParent().equals(file2.getParent())) {
                    if (t.a(str, str2)) {
                        handler.sendEmptyMessage(3);
                    } else {
                        handler.sendEmptyMessage(4);
                    }
                } else if (t.a(file, file2)) {
                    handler.sendEmptyMessage(3);
                } else {
                    handler.sendEmptyMessage(4);
                }
            } else {
                int c2 = c();
                new e(str).a((long) (((float) c2) * this.l), (long) (((float) c2) * this.m), str2);
                handler.sendEmptyMessage(3);
            }
        } catch (com.shoujiduoduo.util.f.d e2) {
            com.shoujiduoduo.base.a.a.c("WavDataProcess", "MP3Exception");
            handler.sendEmptyMessage(4);
            e2.printStackTrace();
        } catch (IOException e3) {
            com.shoujiduoduo.base.a.a.c("WavDataProcess", "IOException");
            handler.sendEmptyMessage(4);
            e3.printStackTrace();
        }
    }

    public String f() {
        if (this.g == null || this.g.equals("")) {
            return "";
        }
        return t.c(this.g);
    }

    public void g() {
        if (this.t != null && this.t.isAlive()) {
            this.f = true;
            try {
                this.t.join();
            } catch (InterruptedException e2) {
                e2.printStackTrace();
            }
        }
    }

    public void h() {
        if (this.c != null && this.c.isAlive()) {
            this.f = true;
            try {
                this.c.join();
            } catch (InterruptedException e2) {
                e2.printStackTrace();
            }
        }
        this.j = 0;
        this.l = 0.0f;
        this.m = 1.0f;
        synchronized (this.f1625a) {
            this.d.clear();
            this.b = 0;
        }
        if (this.n != null) {
            this.n.a();
            this.n = null;
            com.shoujiduoduo.base.a.a.a("WavDataProcess", "decoder release");
        }
    }

    public void i() {
        com.shoujiduoduo.base.a.a.a("WavDataProcess", BuildConfig.BUILD_TYPE);
        h();
        com.shoujiduoduo.player.a.b().b(this);
    }

    public long j() {
        if (b() == 0) {
            return this.b;
        }
        if (this.e != null) {
            return (long) this.e.length;
        }
        return 0;
    }

    public int k() {
        if (this.j == 0) {
            return com.shoujiduoduo.player.a.b().e();
        }
        return this.s;
    }

    public synchronized short[] a(int i2, int i3, int i4) {
        short[] sArr;
        int i5 = 0;
        synchronized (this) {
            if (b() == 0) {
                if (k() == 0) {
                    com.shoujiduoduo.base.a.a.c("WavDataProcess", "getBlockBufferSize == 0");
                    sArr = null;
                } else {
                    synchronized (this.f1625a) {
                        if (i3 == 0) {
                            try {
                                i3 = this.d.size();
                            } catch (IndexOutOfBoundsException e2) {
                                e2.printStackTrace();
                                com.shoujiduoduo.base.a.a.c("WavDataProcess", "getDrawData  return null");
                                sArr = null;
                                return sArr;
                            } catch (IllegalArgumentException e3) {
                                e3.printStackTrace();
                                com.shoujiduoduo.base.a.a.c("WavDataProcess", "getDrawData  return null");
                                sArr = null;
                                return sArr;
                            }
                        }
                        if (i3 <= i2) {
                            com.shoujiduoduo.base.a.a.c("WavDataProcess", "getDrawData  end <= begin");
                            sArr = null;
                        } else {
                            int k2 = (i3 - i2) * k();
                            int k3 = k();
                            if (k2 <= i4) {
                                short[] sArr2 = new short[k2];
                                while (i5 < k2) {
                                    sArr2[i5] = this.d.get((i5 / k3) + i2)[i5 % k3];
                                    i5++;
                                }
                                sArr = sArr2;
                            } else {
                                short[] sArr3 = new short[i4];
                                float f2 = ((float) k2) / ((float) i4);
                                int i6 = 0;
                                while (i6 < i4 && (i5 / k3) + i2 < this.d.size()) {
                                    sArr3[i6] = this.d.get((i5 / k3) + i2)[i5 % k3];
                                    int i7 = i6 + 1;
                                    i5 = (int) (((float) i7) * f2);
                                    i6 = i7;
                                }
                                sArr = sArr3;
                            }
                        }
                    }
                }
            } else if (this.e == null) {
                sArr = null;
            } else if (this.e.length == 0) {
                sArr = null;
            } else if (this.e.length < i4) {
                double length = ((double) i4) / ((double) this.e.length);
                sArr = new short[i4];
                int i8 = 0;
                while (i5 < this.e.length && ((int) (((double) i5) * length)) < i4) {
                    sArr[(int) (((double) i5) * length)] = (short) ((int) (this.e[i5] * 32767.0d));
                    if (i5 != 0) {
                        for (int i9 = i8 + 1; i9 < ((int) (((double) i5) * length)); i9++) {
                            sArr[i9] = (short) ((int) (((double) sArr[i9 - 1]) + (((double) (sArr[(int) (((double) i5) * length)] - sArr[i8])) / length)));
                        }
                    }
                    i5++;
                    i8 = (int) (((double) i5) * length);
                }
                com.shoujiduoduo.base.a.a.a("WavDataProcess", "mTotalGainData.length < resamplesize, length:" + this.e.length);
            } else {
                sArr = new short[i4];
                float length2 = ((float) this.e.length) / ((float) i4);
                int i10 = 0;
                while (i5 < i4 && i10 < this.e.length) {
                    if (i5 == 0) {
                        sArr[i5] = (short) ((int) (this.e[i10] * 32767.0d));
                    } else {
                        sArr[i5] = (short) ((int) ((this.e[i10] + this.e[i10 - 1]) * 0.5d * 32767.0d));
                    }
                    i5++;
                    i10 = (int) (((float) i5) * length2);
                }
                com.shoujiduoduo.base.a.a.a("WavDataProcess", "mTotalGainData.length > resamplesize, length:" + this.e.length + " resample size:" + i4);
            }
        }
        return sArr;
    }

    /* access modifiers changed from: private */
    public void l() {
        double d2;
        int b2 = this.k.b();
        int[] d3 = this.k.d();
        double[] dArr = new double[b2];
        if (b2 == 1) {
            dArr[0] = (double) d3[0];
        } else if (b2 == 2) {
            dArr[0] = (double) d3[0];
            dArr[1] = (double) d3[1];
        } else if (b2 > 2) {
            dArr[0] = (((double) d3[0]) / 2.0d) + (((double) d3[1]) / 2.0d);
            for (int i2 = 1; i2 < b2 - 1; i2++) {
                dArr[i2] = (((double) d3[i2 - 1]) / 3.0d) + (((double) d3[i2]) / 3.0d) + (((double) d3[i2 + 1]) / 3.0d);
            }
            dArr[b2 - 1] = (((double) d3[b2 - 2]) / 2.0d) + (((double) d3[b2 - 1]) / 2.0d);
        }
        double d4 = 1.0d;
        for (int i3 = 0; i3 < b2; i3++) {
            if (dArr[i3] > d4) {
                d4 = dArr[i3];
            }
        }
        if (d4 > 255.0d) {
            d2 = 255.0d / d4;
        } else {
            d2 = 1.0d;
        }
        int[] iArr = new int[256];
        double d5 = 0.0d;
        for (int i4 = 0; i4 < b2; i4++) {
            int i5 = (int) (dArr[i4] * d2);
            if (i5 < 0) {
                i5 = 0;
            }
            if (i5 > 255) {
                i5 = MotionEventCompat.ACTION_MASK;
            }
            if (((double) i5) > d5) {
                d5 = (double) i5;
            }
            iArr[i5] = iArr[i5] + 1;
        }
        double d6 = 0.0d;
        int i6 = 0;
        while (d6 < 255.0d && i6 < b2 / 20) {
            i6 += iArr[(int) d6];
            d6 += 1.0d;
        }
        double d7 = d5;
        int i7 = 0;
        while (d7 > 2.0d && i7 < b2 / 100) {
            i7 += iArr[(int) d7];
            d7 -= 1.0d;
        }
        double[] dArr2 = new double[b2];
        double d8 = d7 - d6;
        for (int i8 = 0; i8 < b2; i8++) {
            double d9 = ((dArr[i8] * d2) - d6) / d8;
            if (d9 < 0.0d) {
                d9 = 0.0d;
            }
            if (d9 > 1.0d) {
                d9 = 1.0d;
            }
            dArr2[i8] = d9 * d9;
        }
        this.e = new double[b2];
        for (int i9 = 0; i9 < b2; i9++) {
            this.e[i9] = dArr2[i9];
        }
    }
}
