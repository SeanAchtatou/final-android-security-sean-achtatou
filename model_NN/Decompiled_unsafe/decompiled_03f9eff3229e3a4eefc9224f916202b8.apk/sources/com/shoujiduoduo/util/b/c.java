package com.shoujiduoduo.util.b;

import android.text.TextUtils;
import com.cmsc.cmmusic.common.data.BizInfo;
import com.cmsc.cmmusic.common.data.MusicInfo;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.sina.weibo.sdk.exception.WeiboAuthException;
import java.util.List;

/* compiled from: RequstResult */
public class c {

    /* compiled from: RequstResult */
    public static class a extends b {

        /* renamed from: a  reason: collision with root package name */
        public String f1586a;
    }

    /* compiled from: RequstResult */
    public static class aa extends b {

        /* renamed from: a  reason: collision with root package name */
        public String f1587a;
    }

    /* compiled from: RequstResult */
    public static class ab extends b {

        /* renamed from: a  reason: collision with root package name */
        public n f1588a;
    }

    /* compiled from: RequstResult */
    public static class ae extends b {

        /* renamed from: a  reason: collision with root package name */
        public String f1591a = "";
    }

    /* compiled from: RequstResult */
    public static class af extends b {

        /* renamed from: a  reason: collision with root package name */
        public String f1592a;
        public String d;
    }

    /* compiled from: RequstResult */
    public static class ag extends b {

        /* renamed from: a  reason: collision with root package name */
        public String f1593a;
    }

    /* compiled from: RequstResult */
    public static class ah {

        /* renamed from: a  reason: collision with root package name */
        public String f1594a;
        public String b;
        public String c;
        public String d;
        public String e;
    }

    /* compiled from: RequstResult */
    public static class ai {

        /* renamed from: a  reason: collision with root package name */
        public String f1595a;
        public String b;
        public String c;
        public String d;
        public String e;
    }

    /* compiled from: RequstResult */
    public static class f extends b {

        /* renamed from: a  reason: collision with root package name */
        public String f1599a = "";
    }

    /* compiled from: RequstResult */
    public static class g extends b {

        /* renamed from: a  reason: collision with root package name */
        public String f1600a;
        public String d;
        public String e;
    }

    /* compiled from: RequstResult */
    public static class i extends b {

        /* renamed from: a  reason: collision with root package name */
        public String f1602a;
    }

    /* compiled from: RequstResult */
    public static class j extends b {

        /* renamed from: a  reason: collision with root package name */
        public BizInfo f1603a;
        public MusicInfo d;
        public String e;
        public String f;
    }

    /* compiled from: RequstResult */
    public static class k extends b {

        /* renamed from: a  reason: collision with root package name */
        public String f1604a = "";
        public int d;
        public String e = "";
    }

    /* compiled from: RequstResult */
    public static class l extends b {

        /* renamed from: a  reason: collision with root package name */
        public a f1605a;
        public String d;

        /* compiled from: RequstResult */
        public enum a {
            wap,
            sms_code
        }
    }

    /* compiled from: RequstResult */
    public static class n {

        /* renamed from: a  reason: collision with root package name */
        public String f1608a;
        public String b;
    }

    /* compiled from: RequstResult */
    public static class o extends b {

        /* renamed from: a  reason: collision with root package name */
        public w f1609a;
    }

    /* compiled from: RequstResult */
    public static class p extends b {

        /* renamed from: a  reason: collision with root package name */
        public boolean f1610a;
    }

    /* compiled from: RequstResult */
    public static class q extends b {

        /* renamed from: a  reason: collision with root package name */
        public int f1611a;
        public ai[] d;
    }

    /* compiled from: RequstResult */
    public static class r extends b {

        /* renamed from: a  reason: collision with root package name */
        public String f1612a;
        public String d;
        public String e;
        public String f;
    }

    /* compiled from: RequstResult */
    public static class s extends b {

        /* renamed from: a  reason: collision with root package name */
        public int f1613a;
        public z[] d;
    }

    /* compiled from: RequstResult */
    public static class t extends b {

        /* renamed from: a  reason: collision with root package name */
        public String f1614a;
        public boolean d;
    }

    /* compiled from: RequstResult */
    public static class u extends b {

        /* renamed from: a  reason: collision with root package name */
        public String f1615a = "";
    }

    /* compiled from: RequstResult */
    public static class v extends b {

        /* renamed from: a  reason: collision with root package name */
        public ac f1616a;
        public String d = "";
    }

    /* compiled from: RequstResult */
    public static class w {

        /* renamed from: a  reason: collision with root package name */
        public String f1617a;
        public String b;
        public String c;
        public String d;
        public String e;
        public String f;
        public String g;
        public String h;
    }

    /* compiled from: RequstResult */
    public static class y extends b {

        /* renamed from: a  reason: collision with root package name */
        public String f1619a;
        public String d;
    }

    /* compiled from: RequstResult */
    public static class z {

        /* renamed from: a  reason: collision with root package name */
        public String f1620a;
        public String b;
        public String c;
        public String d;
        public String e;
        public String f;
        public String g;
    }

    /* compiled from: RequstResult */
    public static class b {
        public String b;
        public String c;

        public b() {
            this.b = "";
            this.c = "";
        }

        public b(String str, String str2) {
            this.b = str;
            this.c = str2;
        }

        public String a() {
            return this.b;
        }

        public String b() {
            return this.c;
        }

        public String toString() {
            return "code:" + this.b + ", msg:" + this.c;
        }

        public boolean c() {
            return this.b.equals("0000") || this.b.equals("000000") || this.b.equals("0");
        }
    }

    /* compiled from: RequstResult */
    public static class ac {

        /* renamed from: a  reason: collision with root package name */
        public String f1589a = "";
        public String b = "";
        public String c = "";
        public String d = "";
        public String e = "";
        public String f = "";
        public String g = "";
        public String h = "";
        public String i = "";
        public String j = "";

        public String a() {
            return this.f;
        }

        public String b() {
            return this.f1589a;
        }

        public String c() {
            return this.b;
        }

        public String d() {
            return this.d;
        }

        public String e() {
            return this.g;
        }
    }

    /* compiled from: RequstResult */
    public static class x extends b {

        /* renamed from: a  reason: collision with root package name */
        public List<ac> f1618a;

        public List<ac> d() {
            return this.f1618a;
        }
    }

    /* renamed from: com.shoujiduoduo.util.b.c$c  reason: collision with other inner class name */
    /* compiled from: RequstResult */
    public static class C0028c extends b {

        /* renamed from: a  reason: collision with root package name */
        public b f1596a;
        public b d;

        public boolean d() {
            return this.f1596a != null && this.f1596a.c();
        }

        public boolean e() {
            return this.d != null && this.d.c();
        }
    }

    /* compiled from: RequstResult */
    public static class m extends b {

        /* renamed from: a  reason: collision with root package name */
        public String f1607a;

        public String d() {
            return this.f1607a;
        }
    }

    /* compiled from: RequstResult */
    public static class h extends b {

        /* renamed from: a  reason: collision with root package name */
        public String f1601a;

        public String d() {
            return this.f1601a;
        }
    }

    /* compiled from: RequstResult */
    public static class d extends b {

        /* renamed from: a  reason: collision with root package name */
        public b f1597a;
        public b d;
        public b e;

        public boolean d() {
            if (this.f1597a != null) {
                return this.f1597a.a().equals("0000");
            }
            return false;
        }

        public boolean e() {
            if (this.d instanceof t) {
                return ((t) this.d).d;
            }
            return false;
        }

        public boolean f() {
            if (this.e instanceof t) {
                return ((t) this.e).d;
            }
            return false;
        }
    }

    /* compiled from: RequstResult */
    public static class ad extends b {

        /* renamed from: a  reason: collision with root package name */
        public String f1590a = WeiboAuthException.DEFAULT_AUTH_ERROR_CODE;

        public boolean d() {
            return this.f1590a.equals("0") || this.f1590a.equals("2") || this.f1590a.equals("4");
        }
    }

    /* compiled from: RequstResult */
    public static class e extends b {

        /* renamed from: a  reason: collision with root package name */
        public b f1598a;
        public b d;
        public b e;
        public b f;

        public boolean d() {
            if (this.f1598a == null || !this.f1598a.c() || !(this.f1598a instanceof ad)) {
                return false;
            }
            return ((ad) this.f1598a).d();
        }

        public boolean e() {
            if (this.d == null || !this.d.c() || !(this.d instanceof p)) {
                return false;
            }
            return ((p) this.d).f1610a;
        }

        public String f() {
            if (this.f == null || !this.f.c() || !(this.f instanceof af)) {
                return "";
            }
            return ((af) this.f).f1592a;
        }

        public String g() {
            if (this.e == null || !this.e.c() || !(this.e instanceof ag)) {
                return "";
            }
            return ((ag) this.e).f1593a;
        }

        public boolean h() {
            if (this.e == null || !this.e.c() || !(this.e instanceof ag)) {
                com.shoujiduoduo.base.a.a.a("RequestResult", "is4G, return:false");
                return false;
            }
            ag agVar = (ag) this.e;
            com.shoujiduoduo.base.a.a.a("RequestResult", "is4G, return:" + agVar.f1593a.equals("4"));
            return agVar.f1593a.equals("4");
        }

        public boolean i() {
            String c = com.umeng.analytics.b.c(RingDDApp.c(), "cu_4g_cailing_free_switch");
            if (h() && !c.equals("false")) {
                com.shoujiduoduo.base.a.a.a("RequestResult", "isFreeCailingQualified, return true");
                return true;
            } else if (this.f == null || !this.f.c() || !(this.f instanceof af)) {
                com.shoujiduoduo.base.a.a.a("RequestResult", "isFreeCailingQualified, return false 2");
                return false;
            } else {
                af afVar = (af) this.f;
                String c2 = com.umeng.analytics.b.c(RingDDApp.c(), "cu_3g_cailing_free");
                if (TextUtils.isEmpty(c2) || !c2.contains(afVar.f1592a)) {
                    com.shoujiduoduo.base.a.a.a("RequestResult", "isFreeCailingQualified, return false");
                    return false;
                }
                com.shoujiduoduo.base.a.a.a("RequestResult", "isFreeCailingQualified, return true 2, provinceId:" + afVar.f1592a + ", ids:" + c2);
                return true;
            }
        }
    }
}
