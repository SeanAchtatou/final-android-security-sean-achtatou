package com.shoujiduoduo.util;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.media.RingtoneManager;
import android.net.Uri;
import android.provider.ContactsContract;
import com.shoujiduoduo.a.c.h;
import com.shoujiduoduo.base.bean.y;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

/* compiled from: DownloadManager */
public class p extends SQLiteOpenHelper {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final String f1682a = p.class.getSimpleName();
    private static int b = 0;
    private static p g;
    /* access modifiers changed from: private */
    public static int h = -1;
    /* access modifiers changed from: private */
    public static int i = -2;
    /* access modifiers changed from: private */
    public static int j = -3;
    /* access modifiers changed from: private */
    public static int k = -4;
    /* access modifiers changed from: private */
    public static int l = -5;
    private static int m = -6;
    private static int n = -8;
    /* access modifiers changed from: private */
    public static int o = -9;
    private static int p = -10;
    /* access modifiers changed from: private */
    public static int q = -11;
    /* access modifiers changed from: private */
    public static int r = -12;
    /* access modifiers changed from: private */
    public static int s = -13;
    private static int t = -14;
    /* access modifiers changed from: private */
    public static int u = 0;
    /* access modifiers changed from: private */
    public static int v = 1;
    /* access modifiers changed from: private */
    public static int w = 2;
    /* access modifiers changed from: private */
    public static int x = 3;
    /* access modifiers changed from: private */
    public PlayerService c = null;
    private b d = null;
    /* access modifiers changed from: private */
    public ArrayList<h> e = new ArrayList<>();
    /* access modifiers changed from: private */
    public Context f;
    private int y = 10;
    /* access modifiers changed from: private */
    public HashMap<Integer, a> z = new HashMap<>(this.y);

    public void a(h hVar) {
        if (hVar != null) {
            this.e.add(hVar);
        }
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    private p(Context context, String str, SQLiteDatabase.CursorFactory cursorFactory, int i2) {
        super(context, str, cursorFactory, i2);
        String str2 = null;
        this.f = context;
        com.shoujiduoduo.base.a.a.a(f1682a, "DownloadManager constructor begins.");
        q();
        SQLiteDatabase readableDatabase = getReadableDatabase();
        if (readableDatabase != null) {
            Cursor rawQuery = readableDatabase.rawQuery("select DISTINCT tbl_name from sqlite_master where tbl_name = 'ringtoneduoduo_downloadtable'", null);
            if (rawQuery == null || rawQuery.getCount() <= 0) {
                rawQuery = readableDatabase.rawQuery("select DISTINCT tbl_name from sqlite_master where tbl_name = 'ringtoneduoduo_downloadtable_2'", null);
                if (rawQuery != null && rawQuery.getCount() > 0) {
                    str2 = "ringtoneduoduo_downloadtable_2";
                }
            } else {
                str2 = "ringtoneduoduo_downloadtable";
            }
            if (rawQuery != null) {
                rawQuery.close();
            }
            readableDatabase.close();
        }
        if (str2 != null) {
            com.shoujiduoduo.base.a.a.a(f1682a, "DownloadManager: rename thread start!");
            this.d = new b(str2);
            this.d.start();
        }
        com.shoujiduoduo.base.a.a.a(f1682a, "DownloadManager constructor ends.");
    }

    public void a(PlayerService playerService) {
        this.c = playerService;
    }

    public static p a(Context context) {
        p pVar;
        com.shoujiduoduo.base.a.a.a(f1682a, "enter DownloadManger.getInstance.");
        synchronized (f1682a) {
            if (g == null) {
                g = new p(context, "duoduo.ringtone.database", null, 3);
            } else {
                com.shoujiduoduo.base.a.a.a(f1682a, "mThis = " + g.toString());
            }
            pVar = g;
        }
        return pVar;
    }

    private void q() {
        com.shoujiduoduo.base.a.a.a(f1682a, "DownloadManager CreateTable begins.");
        synchronized (f1682a) {
            SQLiteDatabase writableDatabase = getWritableDatabase();
            if (writableDatabase != null) {
                com.shoujiduoduo.base.a.a.a(f1682a, "DownloadManager CreateTable 1");
                try {
                    writableDatabase.execSQL("CREATE TABLE IF NOT EXISTS ringtoneduoduo_resourcetable_3 (id INTEGER PRIMARY KEY AUTOINCREMENT, rid INTEGER, name VARCHAR, artist VARCHAR, album VARCHAR, down_size INTEGER, total_size INTEGER, bitrate INTEGER, format VARCHAR, url VARCHAR, path VARCHAR);");
                    com.shoujiduoduo.base.a.a.a(f1682a, "Create ringtoneduoduo_resourcetable_3");
                } catch (SQLException e2) {
                    e2.printStackTrace();
                    com.shoujiduoduo.base.a.a.c(f1682a, "Create ringtoneduoduo_resourcetable_3 failed!");
                }
            }
        }
        com.shoujiduoduo.base.a.a.a(f1682a, "DownloadManager CreateTable ends.");
    }

    public y a(int i2, String str, String str2, String str3, int i3, int i4, int i5, String str4, String str5) {
        y yVar;
        synchronized (f1682a) {
            SQLiteDatabase writableDatabase = getWritableDatabase();
            if (writableDatabase == null) {
                f.c("addDownloadInfo returns null because mDB is null!");
                yVar = null;
            } else {
                String a2 = a(writableDatabase, str, str2, i2, str4);
                String str6 = "insert into ringtoneduoduo_resourcetable_3 (rid, name, artist, album, down_size, total_size, bitrate, format, url, path)VALUES (" + i2 + "," + DatabaseUtils.sqlEscapeString(str) + "," + DatabaseUtils.sqlEscapeString(str2) + "," + DatabaseUtils.sqlEscapeString(str3) + "," + Integer.toString(i3) + "," + Integer.toString(i4) + "," + i5 + ",'" + str4 + "'," + DatabaseUtils.sqlEscapeString(str5) + "," + DatabaseUtils.sqlEscapeString(a2) + ");";
                com.shoujiduoduo.base.a.a.a(f1682a, "SQLITE: " + str6);
                try {
                    writableDatabase.execSQL(str6);
                    com.shoujiduoduo.base.a.a.a(f1682a, "Success: Add new ring to the table.");
                    yVar = new y(str, str2, i2, i3, i4, i5, str4, str5);
                    yVar.e(a2);
                } catch (SQLException e2) {
                    e2.printStackTrace();
                    f.c("addDownloadInfo returns null because fail to insert into resource table!\n" + com.shoujiduoduo.base.a.b.a(e2));
                    com.shoujiduoduo.base.a.a.c(f1682a, "Database: insert into table FAILED!");
                    yVar = null;
                }
            }
        }
        return yVar;
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(int r6) {
        /*
            r5 = this;
            java.lang.String r1 = com.shoujiduoduo.util.p.f1682a
            monitor-enter(r1)
            android.database.sqlite.SQLiteDatabase r0 = r5.getWritableDatabase()     // Catch:{ all -> 0x0030 }
            if (r0 != 0) goto L_0x000b
            monitor-exit(r1)     // Catch:{ all -> 0x0030 }
        L_0x000a:
            return
        L_0x000b:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0030 }
            r2.<init>()     // Catch:{ all -> 0x0030 }
            java.lang.String r3 = "delete from ringtoneduoduo_resourcetable_3 where rid='"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0030 }
            java.lang.StringBuilder r2 = r2.append(r6)     // Catch:{ all -> 0x0030 }
            java.lang.String r3 = "'"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0030 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0030 }
            r0.execSQL(r2)     // Catch:{ SQLException -> 0x0033 }
            java.lang.String r0 = com.shoujiduoduo.util.p.f1682a     // Catch:{ SQLException -> 0x0033 }
            java.lang.String r3 = "Success: delete ring record from the table."
            com.shoujiduoduo.base.a.a.a(r0, r3)     // Catch:{ SQLException -> 0x0033 }
        L_0x002e:
            monitor-exit(r1)     // Catch:{ all -> 0x0030 }
            goto L_0x000a
        L_0x0030:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0030 }
            throw r0
        L_0x0033:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x0030 }
            java.lang.String r0 = com.shoujiduoduo.util.p.f1682a     // Catch:{ all -> 0x0030 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0030 }
            r3.<init>()     // Catch:{ all -> 0x0030 }
            java.lang.String r4 = "Database: exec \""
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0030 }
            java.lang.StringBuilder r2 = r3.append(r2)     // Catch:{ all -> 0x0030 }
            java.lang.String r3 = "\" FAILED!"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0030 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0030 }
            com.shoujiduoduo.base.a.a.c(r0, r2)     // Catch:{ all -> 0x0030 }
            goto L_0x002e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.util.p.a(int):void");
    }

    public y b(int i2) {
        if (i2 == 0) {
            return null;
        }
        synchronized (f1682a) {
            SQLiteDatabase writableDatabase = getWritableDatabase();
            if (writableDatabase == null) {
                return null;
            }
            try {
                String str = "select * from ringtoneduoduo_resourcetable_3 where rid=" + i2 + " order by rid;";
                com.shoujiduoduo.base.a.a.a(f1682a, str);
                Cursor rawQuery = writableDatabase.rawQuery(str, null);
                if (rawQuery == null || rawQuery.getCount() == 0) {
                    if (rawQuery != null) {
                        rawQuery.close();
                    }
                    com.shoujiduoduo.base.a.a.c(f1682a, "searchSongById: c == null || c.getCount() == 0");
                    return null;
                } else if (rawQuery.getCount() != 1) {
                    rawQuery.close();
                    a(i2);
                    com.shoujiduoduo.base.a.a.c(f1682a, "searchSongById: c.getCount() != 1");
                    return null;
                } else if (rawQuery.moveToNext()) {
                    int i3 = rawQuery.getInt(1);
                    int i4 = rawQuery.getInt(5);
                    int i5 = rawQuery.getInt(6);
                    int i6 = rawQuery.getInt(7);
                    String string = rawQuery.getString(8);
                    String string2 = rawQuery.getString(2);
                    String string3 = rawQuery.getString(3);
                    String string4 = rawQuery.getString(9);
                    String string5 = rawQuery.getString(10);
                    rawQuery.close();
                    com.shoujiduoduo.base.a.a.a(f1682a, "searchSongById: rid=" + i3 + ",down_size=" + i4 + ",total_size=" + i5 + "bitrate=" + i6 + "format=" + string + "url=" + string4 + ", path = " + string5);
                    y yVar = new y(string2, string3, i3, i4, i5, i6, string, string4);
                    yVar.e(string5);
                    return yVar;
                } else {
                    rawQuery.close();
                    com.shoujiduoduo.base.a.a.c(f1682a, "searchSongById: return null");
                    return null;
                }
            } catch (SQLiteException e2) {
                e2.printStackTrace();
                com.shoujiduoduo.base.a.a.c(f1682a, "searchSongById: database query failed!");
            }
        }
    }

    public void a() {
        synchronized (f1682a) {
            SQLiteDatabase writableDatabase = getWritableDatabase();
            if (writableDatabase != null) {
                try {
                    com.shoujiduoduo.base.a.a.a(f1682a, "select * from ringtoneduoduo_resourcetable_3;");
                    Cursor rawQuery = writableDatabase.rawQuery("select * from ringtoneduoduo_resourcetable_3;", null);
                    if (rawQuery == null || rawQuery.getCount() == 0) {
                        if (rawQuery != null) {
                            rawQuery.close();
                        }
                        com.shoujiduoduo.base.a.a.c(f1682a, "searchSongById: c == null || c.getCount() == 0");
                        return;
                    }
                    ArrayList arrayList = new ArrayList();
                    while (rawQuery.moveToNext()) {
                        int i2 = rawQuery.getInt(1);
                        int i3 = rawQuery.getInt(5);
                        int i4 = rawQuery.getInt(6);
                        String string = rawQuery.getString(8);
                        String string2 = rawQuery.getString(2);
                        String string3 = rawQuery.getString(3);
                        String string4 = rawQuery.getString(10);
                        com.shoujiduoduo.base.a.a.a(f1682a, "searchSongById: i1=" + i2 + ",i2=" + i3 + ",i3=" + i4);
                        if ((this.c == null || this.c.f() != i2) && i2 != b && !com.shoujiduoduo.a.b.b.b().a("" + i2, "favorite_ring_list")) {
                            y yVar = new y(string2, string3, i2, i3, i4, 0, string, "");
                            yVar.e(string4);
                            arrayList.add(yVar);
                        }
                    }
                    rawQuery.close();
                    for (int i5 = 0; i5 < arrayList.size(); i5++) {
                        y yVar2 = (y) arrayList.get(i5);
                        a(yVar2.c);
                        k.a(yVar2.l());
                    }
                    com.shoujiduoduo.base.a.a.c(f1682a, "searchSongById: return null");
                } catch (SQLiteException e2) {
                    e2.printStackTrace();
                    com.shoujiduoduo.base.a.a.c(f1682a, "searchSongById: database query failed!");
                }
            }
        }
    }

    public boolean a(int i2, int i3) {
        boolean z2 = false;
        synchronized (f1682a) {
            SQLiteDatabase writableDatabase = getWritableDatabase();
            if (writableDatabase != null) {
                try {
                    String str = "UPDATE ringtoneduoduo_resourcetable_3 SET down_size=" + i2 + " WHERE rid=" + i3;
                    com.shoujiduoduo.base.a.a.a(f1682a, str);
                    writableDatabase.execSQL(str);
                    com.shoujiduoduo.base.a.a.a(f1682a, "Success:updateDownloadProgress.");
                    z2 = true;
                } catch (SQLiteException e2) {
                    e2.printStackTrace();
                    com.shoujiduoduo.base.a.a.c(f1682a, "FAIL:updateDownloadProgress.");
                }
            }
        }
        return z2;
    }

    public boolean a(y yVar) {
        boolean z2 = false;
        if (yVar.c != 0) {
            synchronized (f1682a) {
                SQLiteDatabase writableDatabase = getWritableDatabase();
                if (writableDatabase != null) {
                    try {
                        String str = "UPDATE ringtoneduoduo_resourcetable_3 SET down_size=" + yVar.d + ",total_size=" + yVar.e + ",bitrate=" + yVar.f + ",format='" + yVar.g + "',url=" + DatabaseUtils.sqlEscapeString(yVar.h) + " WHERE rid=" + yVar.c;
                        com.shoujiduoduo.base.a.a.a(f1682a, str);
                        writableDatabase.execSQL(str);
                        com.shoujiduoduo.base.a.a.a(f1682a, "Success:updateSongInfo.");
                        z2 = true;
                    } catch (SQLiteException e2) {
                        e2.printStackTrace();
                        com.shoujiduoduo.base.a.a.c(f1682a, "FAIL:updateSongInfo.");
                    }
                }
            }
        }
        return z2;
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
    }

    private boolean a(SQLiteDatabase sQLiteDatabase, String str) {
        boolean z2 = true;
        if (!(sQLiteDatabase == null || str == null || str.length() == 0)) {
            synchronized (f1682a) {
                try {
                    String str2 = "select * from ringtoneduoduo_resourcetable_3 where path=" + DatabaseUtils.sqlEscapeString(str) + " order by rid;";
                    com.shoujiduoduo.base.a.a.a(f1682a, str2);
                    Cursor rawQuery = sQLiteDatabase.rawQuery(str2, null);
                    if (rawQuery == null || rawQuery.getCount() == 0) {
                        if (rawQuery != null) {
                            rawQuery.close();
                        }
                        com.shoujiduoduo.base.a.a.c(f1682a, "searchSongByPath: c == null || c.getCount() == 0");
                    } else {
                        rawQuery.close();
                        com.shoujiduoduo.base.a.a.c(f1682a, "searchSongByPath: return null");
                        z2 = false;
                    }
                } catch (SQLiteException e2) {
                    e2.printStackTrace();
                    com.shoujiduoduo.base.a.a.c(f1682a, "searchSongByPath: database query failed!");
                }
            }
        }
        return z2;
    }

    /* access modifiers changed from: private */
    public String a(SQLiteDatabase sQLiteDatabase, String str, String str2, int i2, String str3) {
        String str4;
        String str5;
        String str6 = null;
        if ((str == null || str.length() == 0) && (str2 == null || str2.length() == 0)) {
            StringBuilder append = new StringBuilder().append(s.b()).append(i2).append(".");
            if (str3 == null || str3.length() == 0) {
                str3 = "mp3";
            }
            return append.append(str3).toString();
        }
        if (str != null) {
            str4 = f.e(str.replaceAll("[\\$\\|\\&\\*\\[\\]\\\"\\'\\\\\\/:;<>@#{}]", "").replaceAll("\\s+", " "));
        } else {
            str4 = null;
        }
        if (str2 != null) {
            str6 = f.e(str2.replaceAll("[\\$\\|\\&\\*\\[\\]\\\"\\'\\\\\\/:;<>@#{}]", "").replaceAll("\\s+", " "));
        }
        String str7 = str4 + "_" + str6;
        if (str7.length() > 100) {
            str7 = str7.substring(0, 100);
        }
        String string = this.f.getResources().getString(R.string.cache_file_prefix);
        StringBuilder append2 = new StringBuilder().append(s.b()).append(string).append("_").append(str7).append(".");
        if (str3 == null || str3.length() == 0) {
            str5 = "mp3";
        } else {
            str5 = str3;
        }
        String sb = append2.append(str5).toString();
        if (a(sQLiteDatabase, sb)) {
            return sb;
        }
        StringBuilder append3 = new StringBuilder().append(s.b()).append(string).append("_").append(str7 + "_" + i2).append(".");
        if (str3 == null || str3.length() == 0) {
            str3 = "mp3";
        }
        return append3.append(str3).toString();
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i2, int i3) {
        com.shoujiduoduo.base.a.a.a(f1682a, "database upgrade begins!");
        try {
            sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS ringtoneduoduo_resourcetable_3 (id INTEGER PRIMARY KEY AUTOINCREMENT, rid INTEGER, name VARCHAR, artist VARCHAR, album VARCHAR, down_size INTEGER, total_size INTEGER, bitrate INTEGER, format VARCHAR, url VARCHAR, path VARCHAR);");
            com.shoujiduoduo.base.a.a.a(f1682a, "onUpgrade: Create ringtoneduoduo_resourcetable_3");
            if (i2 == 1) {
                try {
                    com.shoujiduoduo.base.a.a.a(f1682a, "select * from ringtoneduoduo_downloadtable;");
                    Cursor rawQuery = sQLiteDatabase.rawQuery("select * from ringtoneduoduo_downloadtable;", null);
                    if (rawQuery == null) {
                        com.shoujiduoduo.base.a.a.c(f1682a, "onUpgrade: c == null");
                        return;
                    }
                    while (rawQuery.moveToNext()) {
                        int i4 = rawQuery.getInt(1);
                        String string = rawQuery.getString(2);
                        String string2 = rawQuery.getString(3);
                        String string3 = rawQuery.getString(4);
                        int i5 = rawQuery.getInt(5);
                        int i6 = rawQuery.getInt(6);
                        String a2 = y.a(i4, null);
                        if (i5 == i6) {
                            String str = "insert into ringtoneduoduo_resourcetable_3 (rid, name, artist, album, down_size, total_size, bitrate, format, url, path)VALUES (" + i4 + ",'" + DatabaseUtils.sqlEscapeString(string) + "','" + DatabaseUtils.sqlEscapeString(string2) + "','" + DatabaseUtils.sqlEscapeString(string3) + "'," + Integer.toString(i5) + "," + Integer.toString(i6) + "," + "128000" + "," + "'mp3'" + ",'" + "" + "'" + ",'" + DatabaseUtils.sqlEscapeString(a2) + "'" + ");";
                            com.shoujiduoduo.base.a.a.a(f1682a, "SQLITE: " + str);
                            try {
                                sQLiteDatabase.execSQL(str);
                                com.shoujiduoduo.base.a.a.a(f1682a, "onUpgrade: Success: Add new ring to the table.");
                            } catch (SQLException e2) {
                                e2.printStackTrace();
                                com.shoujiduoduo.base.a.a.c(f1682a, "onUpgrade: Database: insert into table FAILED!");
                            }
                        }
                    }
                    rawQuery.close();
                } catch (SQLiteException e3) {
                    e3.printStackTrace();
                    com.shoujiduoduo.base.a.a.c(f1682a, "onUpgrade: database operation failed!");
                }
            } else if (i2 == 2) {
                try {
                    com.shoujiduoduo.base.a.a.a(f1682a, "select * from ringtoneduoduo_downloadtable_2;");
                    Cursor rawQuery2 = sQLiteDatabase.rawQuery("select * from ringtoneduoduo_downloadtable_2;", null);
                    if (rawQuery2 == null) {
                        com.shoujiduoduo.base.a.a.c(f1682a, "onUpgrade: c == null");
                        return;
                    }
                    while (rawQuery2.moveToNext()) {
                        int i7 = rawQuery2.getInt(1);
                        String string4 = rawQuery2.getString(2);
                        String string5 = rawQuery2.getString(3);
                        String string6 = rawQuery2.getString(4);
                        int i8 = rawQuery2.getInt(5);
                        int i9 = rawQuery2.getInt(6);
                        int i10 = rawQuery2.getInt(7);
                        String string7 = rawQuery2.getString(8);
                        String str2 = "insert into ringtoneduoduo_resourcetable_3 (rid, name, artist, album, down_size, total_size, bitrate, format, url, path)VALUES (" + i7 + "," + DatabaseUtils.sqlEscapeString(string4) + "," + DatabaseUtils.sqlEscapeString(string5) + "," + DatabaseUtils.sqlEscapeString(string6) + "," + i8 + "," + i9 + "," + i10 + "," + DatabaseUtils.sqlEscapeString(string7) + "," + DatabaseUtils.sqlEscapeString(rawQuery2.getString(9)) + "," + DatabaseUtils.sqlEscapeString(y.a(i7, string7)) + ");";
                        com.shoujiduoduo.base.a.a.a(f1682a, "SQLITE: " + str2);
                        try {
                            sQLiteDatabase.execSQL(str2);
                            com.shoujiduoduo.base.a.a.a(f1682a, "onUpgrade: Success: Add new ring to the table.");
                        } catch (SQLException e4) {
                            e4.printStackTrace();
                            com.shoujiduoduo.base.a.a.c(f1682a, "onUpgrade: Database: insert into table FAILED!");
                        }
                    }
                    rawQuery2.close();
                } catch (SQLiteException e5) {
                    e5.printStackTrace();
                    com.shoujiduoduo.base.a.a.c(f1682a, "onUpgrade: database operation failed!");
                }
            }
        } catch (SQLException e6) {
            e6.printStackTrace();
            com.shoujiduoduo.base.a.a.c(f1682a, "Create database failed!");
        }
    }

    /* compiled from: DownloadManager */
    class a extends Thread {
        private final String b = a.class.getSimpleName();
        private y c = null;
        /* access modifiers changed from: private */
        public boolean d = false;
        private boolean e = true;

        public a(y yVar) {
            this.c = yVar;
        }

        public void a(boolean z) {
            this.d = z;
        }

        public boolean a() {
            return this.e;
        }

        public void start() {
            super.start();
        }

        private void a(ArrayList<h> arrayList, y yVar, int i) {
            if (arrayList == null) {
                return;
            }
            if (i < 0) {
                if (!this.d) {
                    Iterator<h> it = arrayList.iterator();
                    while (it.hasNext()) {
                        it.next().a(yVar, i);
                    }
                }
            } else if (i == p.u) {
                if (!this.d) {
                    Iterator<h> it2 = arrayList.iterator();
                    while (it2.hasNext()) {
                        it2.next().d(yVar);
                    }
                } else if (yVar.e > 0 || yVar.d > 0) {
                    p.this.a(yVar);
                }
            } else if (i == p.w) {
                if (!this.d) {
                    Iterator<h> it3 = arrayList.iterator();
                    while (it3.hasNext()) {
                        it3.next().a(yVar);
                    }
                }
            } else if (i == p.x) {
                p.this.a(yVar);
                if (!this.d) {
                    Iterator<h> it4 = arrayList.iterator();
                    while (it4.hasNext()) {
                        it4.next().b(yVar);
                    }
                }
            } else if (i == p.v) {
                p.this.a(yVar);
                if (!this.d) {
                    Iterator<h> it5 = arrayList.iterator();
                    while (it5.hasNext()) {
                        it5.next().c(yVar);
                    }
                }
            }
        }

        private void b() {
            synchronized (p.this.z) {
                if (p.this.z.get(Integer.valueOf(this.c.c)) == this) {
                    p.this.z.remove(Integer.valueOf(this.c.c));
                }
            }
        }

        /* compiled from: DownloadManager */
        private class b {

            /* renamed from: a  reason: collision with root package name */
            String f1685a;
            String b;
            int c;
            boolean d;

            public b(String str, String str2, int i, boolean z) {
                this.f1685a = str;
                this.b = str2;
                this.c = i;
                this.d = z;
            }
        }

        /* renamed from: com.shoujiduoduo.util.p$a$a  reason: collision with other inner class name */
        /* compiled from: DownloadManager */
        private class C0033a extends Exception {

            /* renamed from: a  reason: collision with root package name */
            int f1684a;

            public C0033a(int i) {
                this.f1684a = i;
            }
        }

        private b a(int i, String str) throws C0033a {
            Object valueOf;
            String str2;
            int i2 = this.c.c;
            int i3 = this.c.f;
            StringBuilder sb = new StringBuilder();
            StringBuilder append = sb.append("user=").append(f.a()).append("&prod=").append(f.q()).append("&isrc=").append(f.p()).append("&type=geturlv1").append("&rid=").append(i2).append("&network=").append(i).append("&fmt=").append(this.c.g).append("&br=");
            if (i3 == 0) {
                valueOf = "";
            } else {
                valueOf = Integer.valueOf(i3);
            }
            StringBuilder append2 = append.append(valueOf).append("&from=");
            if (p.this.c != null) {
                str2 = p.this.c.d();
            } else {
                str2 = "";
            }
            append2.append(str2).append("&reason=").append(str).append("&cdn=").append(m.a().c());
            com.shoujiduoduo.base.a.a.a(this.b, "downloadSong: getURL: paraString = " + sb.toString());
            String str3 = "http://www.shoujiduoduo.com/ring_enc.php?q=" + w.e(sb.toString());
            com.shoujiduoduo.base.a.a.a(this.b, "url encoded: " + str3);
            byte[] a2 = bj.a(str3);
            if (a2 == null) {
                com.shoujiduoduo.base.a.a.c(this.b, "downloadSong(" + i2 + "): fail to get ring URL 1.  requese url:" + str3);
                throw new C0033a(p.i);
            } else if (this.d) {
                com.shoujiduoduo.base.a.a.a(this.b, "downloadSong(" + i2 + "): Cancel 1.");
                throw new C0033a(p.u);
            } else {
                try {
                    com.shoujiduoduo.base.a.a.a(this.b, "downloadSong(" + i2 + "): getURL return = " + new String(a2, "UTF-8"));
                    String[] split = new String(a2, "UTF-8").split("\t", 0);
                    if (split.length != 3) {
                        com.shoujiduoduo.base.a.a.c(this.b, "downloadSong(" + i2 + "): fail to get ring URL 3. ");
                        f.c("get AntiStealingLink failure!\nparaString = " + sb.toString() + "\nurl = " + str3 + "\nreturn content = " + new String(a2));
                        throw new C0033a(p.k);
                    }
                    for (int i4 = 0; i4 < split.length; i4++) {
                        com.shoujiduoduo.base.a.a.a(this.b, "result " + i4 + ": " + split[i4]);
                    }
                    com.shoujiduoduo.base.a.a.a(this.b, "downloadSong(" + i2 + "): url =" + split[2]);
                    com.shoujiduoduo.base.a.a.a(this.b, "downloadSong(" + i2 + "): bitrate =" + split[0]);
                    com.shoujiduoduo.base.a.a.a(this.b, "downloadSong(" + i2 + "): format =" + split[1]);
                    try {
                        int intValue = Integer.valueOf(split[0]).intValue();
                        String lowerCase = split[1].toLowerCase();
                        if (this.c.g.compareToIgnoreCase(lowerCase) == 0) {
                            return new b(split[2], lowerCase, intValue, false);
                        }
                        com.shoujiduoduo.base.a.a.c(this.b, "downloadSong(" + i2 + "), mData.format:" + this.c.g);
                        f.c("format error! \n paraString = " + sb.toString() + "\nurl = " + str3 + "\nreturn content = " + new String(a2));
                        throw new C0033a(p.q);
                    } catch (NumberFormatException e2) {
                        com.shoujiduoduo.base.a.a.c(this.b, "downloadSong(" + i2 + "): fail to get ring URL 4. ");
                        f.c("parse Error! NumberformatException." + new String(a2));
                        throw new C0033a(p.q);
                    }
                } catch (UnsupportedEncodingException e3) {
                    com.shoujiduoduo.base.a.a.c(this.b, "downloadSong(" + i2 + "): fail to get ring URL 2. ");
                    throw new C0033a(p.j);
                }
            }
        }

        private b a(int i, boolean z) throws C0033a {
            if (z) {
                return a(i, "failconnect");
            }
            if (this.c.g != null && !this.c.g.equalsIgnoreCase("mp3") && this.c.g.length() != 0) {
                if (this.c.g.equalsIgnoreCase("aac")) {
                    if (i == 2) {
                        if (this.c.k()) {
                            return new b(this.c.d(), "aac", this.c.e(), true);
                        }
                    } else if (this.c.j()) {
                        return new b(this.c.b(), "aac", this.c.c(), true);
                    }
                }
                return a(i, "nolink");
            } else if (this.c.h()) {
                return new b(this.c.f(), "mp3", this.c.g(), true);
            } else {
                if (!this.c.n.equals("")) {
                    return new b(this.c.n, "mp3", 128000, true);
                }
                return a(i, "wantmp3");
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:17:0x0137, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:18:0x0138, code lost:
            com.shoujiduoduo.base.a.a.a(r10.b, "downloadSong(" + r10.c.c + "): UnknownHostException!");
            a(r11, "", "", -1, com.shoujiduoduo.base.a.b.a(r0));
         */
        /* JADX WARNING: Code restructure failed: missing block: B:19:0x016c, code lost:
            throw new com.shoujiduoduo.util.p.a.C0033a(r10, com.shoujiduoduo.util.p.k());
         */
        /* JADX WARNING: Code restructure failed: missing block: B:20:0x016d, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x016e, code lost:
            com.shoujiduoduo.base.a.a.a(r10.b, "downloadSong(" + r10.c.c + "): IndexOutOfBoundsException!");
            a(r11, "", "", -1, com.shoujiduoduo.base.a.b.a(r0));
         */
        /* JADX WARNING: Code restructure failed: missing block: B:22:0x01a2, code lost:
            throw new com.shoujiduoduo.util.p.a.C0033a(r10, com.shoujiduoduo.util.p.l());
         */
        /* JADX WARNING: Code restructure failed: missing block: B:23:0x01a3, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:24:0x01a4, code lost:
            com.shoujiduoduo.base.a.a.a(r10.b, "downloadSong(" + r10.c.c + "): MalformedURLException!");
            a(r11, "", "", -1, com.shoujiduoduo.base.a.b.a(r0));
         */
        /* JADX WARNING: Code restructure failed: missing block: B:25:0x01d8, code lost:
            throw new com.shoujiduoduo.util.p.a.C0033a(r10, com.shoujiduoduo.util.p.k());
         */
        /* JADX WARNING: Code restructure failed: missing block: B:26:0x01d9, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:27:0x01da, code lost:
            com.shoujiduoduo.base.a.a.a(r10.b, "downloadSong(" + r10.c.c + "): IOException!" + com.shoujiduoduo.base.a.b.a(r0));
            a(r11, "", "", -1, com.shoujiduoduo.base.a.b.a(r0));
            r0 = null;
         */
        /* JADX WARNING: Removed duplicated region for block: B:17:0x0137 A[ExcHandler: UnknownHostException (r0v14 'e' java.net.UnknownHostException A[CUSTOM_DECLARE]), Splitter:B:1:0x0022] */
        /* JADX WARNING: Removed duplicated region for block: B:20:0x016d A[ExcHandler: IndexOutOfBoundsException (r0v11 'e' java.lang.IndexOutOfBoundsException A[CUSTOM_DECLARE]), Splitter:B:1:0x0022] */
        /* JADX WARNING: Removed duplicated region for block: B:23:0x01a3 A[ExcHandler: MalformedURLException (r0v8 'e' java.net.MalformedURLException A[CUSTOM_DECLARE]), Splitter:B:1:0x0022] */
        /* JADX WARNING: Removed duplicated region for block: B:26:0x01d9 A[ExcHandler: IOException (r0v5 'e' java.io.IOException A[CUSTOM_DECLARE]), Splitter:B:1:0x0022] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private java.net.HttpURLConnection a(java.lang.String r11) throws com.shoujiduoduo.util.p.a.C0033a {
            /*
                r10 = this;
                r6 = 0
                r4 = -1
                java.lang.String r2 = ""
                java.lang.String r3 = ""
                com.shoujiduoduo.base.bean.y r0 = r10.c
                int r1 = r0.d
                java.lang.String r0 = r10.b
                java.lang.StringBuilder r5 = new java.lang.StringBuilder
                r5.<init>()
                java.lang.String r7 = "buildConnection, url = "
                java.lang.StringBuilder r5 = r5.append(r7)
                java.lang.StringBuilder r5 = r5.append(r11)
                java.lang.String r5 = r5.toString()
                com.shoujiduoduo.base.a.a.a(r0, r5)
                java.net.URL r0 = new java.net.URL     // Catch:{ UnknownHostException -> 0x0137, IndexOutOfBoundsException -> 0x016d, MalformedURLException -> 0x01a3, IOException -> 0x01d9, NullPointerException -> 0x0210 }
                r0.<init>(r11)     // Catch:{ UnknownHostException -> 0x0137, IndexOutOfBoundsException -> 0x016d, MalformedURLException -> 0x01a3, IOException -> 0x01d9, NullPointerException -> 0x0210 }
                java.net.URLConnection r0 = r0.openConnection()     // Catch:{ UnknownHostException -> 0x0137, IndexOutOfBoundsException -> 0x016d, MalformedURLException -> 0x01a3, IOException -> 0x01d9, NullPointerException -> 0x0210 }
                java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ UnknownHostException -> 0x0137, IndexOutOfBoundsException -> 0x016d, MalformedURLException -> 0x01a3, IOException -> 0x01d9, NullPointerException -> 0x0210 }
                com.shoujiduoduo.util.p r5 = com.shoujiduoduo.util.p.this     // Catch:{ UnknownHostException -> 0x0137, IndexOutOfBoundsException -> 0x016d, MalformedURLException -> 0x01a3, IOException -> 0x01d9, NullPointerException -> 0x0210 }
                android.content.Context r5 = r5.f     // Catch:{ UnknownHostException -> 0x0137, IndexOutOfBoundsException -> 0x016d, MalformedURLException -> 0x01a3, IOException -> 0x01d9, NullPointerException -> 0x0210 }
                java.lang.String r7 = "connect_time_out"
                java.lang.String r5 = com.umeng.analytics.b.c(r5, r7)     // Catch:{ UnknownHostException -> 0x0137, IndexOutOfBoundsException -> 0x016d, MalformedURLException -> 0x01a3, IOException -> 0x01d9, NullPointerException -> 0x0210 }
                r7 = 8
                int r7 = com.shoujiduoduo.util.u.a(r5, r7)     // Catch:{ UnknownHostException -> 0x0137, IndexOutOfBoundsException -> 0x016d, MalformedURLException -> 0x01a3, IOException -> 0x01d9, NullPointerException -> 0x0210 }
                int r7 = r7 * 1000
                r0.setConnectTimeout(r7)     // Catch:{ UnknownHostException -> 0x0137, IndexOutOfBoundsException -> 0x016d, MalformedURLException -> 0x01a3, IOException -> 0x01d9, NullPointerException -> 0x0210 }
                java.lang.String r7 = r10.b     // Catch:{ UnknownHostException -> 0x0137, IndexOutOfBoundsException -> 0x016d, MalformedURLException -> 0x01a3, IOException -> 0x01d9, NullPointerException -> 0x0210 }
                java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ UnknownHostException -> 0x0137, IndexOutOfBoundsException -> 0x016d, MalformedURLException -> 0x01a3, IOException -> 0x01d9, NullPointerException -> 0x0210 }
                r8.<init>()     // Catch:{ UnknownHostException -> 0x0137, IndexOutOfBoundsException -> 0x016d, MalformedURLException -> 0x01a3, IOException -> 0x01d9, NullPointerException -> 0x0210 }
                java.lang.String r9 = "connect time out:"
                java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ UnknownHostException -> 0x0137, IndexOutOfBoundsException -> 0x016d, MalformedURLException -> 0x01a3, IOException -> 0x01d9, NullPointerException -> 0x0210 }
                r9 = 8
                int r5 = com.shoujiduoduo.util.u.a(r5, r9)     // Catch:{ UnknownHostException -> 0x0137, IndexOutOfBoundsException -> 0x016d, MalformedURLException -> 0x01a3, IOException -> 0x01d9, NullPointerException -> 0x0210 }
                int r5 = r5 * 1000
                java.lang.StringBuilder r5 = r8.append(r5)     // Catch:{ UnknownHostException -> 0x0137, IndexOutOfBoundsException -> 0x016d, MalformedURLException -> 0x01a3, IOException -> 0x01d9, NullPointerException -> 0x0210 }
                java.lang.String r5 = r5.toString()     // Catch:{ UnknownHostException -> 0x0137, IndexOutOfBoundsException -> 0x016d, MalformedURLException -> 0x01a3, IOException -> 0x01d9, NullPointerException -> 0x0210 }
                com.shoujiduoduo.base.a.a.a(r7, r5)     // Catch:{ UnknownHostException -> 0x0137, IndexOutOfBoundsException -> 0x016d, MalformedURLException -> 0x01a3, IOException -> 0x01d9, NullPointerException -> 0x0210 }
                java.lang.String r5 = r10.b     // Catch:{ UnknownHostException -> 0x0137, IndexOutOfBoundsException -> 0x016d, MalformedURLException -> 0x01a3, IOException -> 0x01d9, NullPointerException -> 0x0210 }
                java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ UnknownHostException -> 0x0137, IndexOutOfBoundsException -> 0x016d, MalformedURLException -> 0x01a3, IOException -> 0x01d9, NullPointerException -> 0x0210 }
                r7.<init>()     // Catch:{ UnknownHostException -> 0x0137, IndexOutOfBoundsException -> 0x016d, MalformedURLException -> 0x01a3, IOException -> 0x01d9, NullPointerException -> 0x0210 }
                java.lang.String r8 = "downloadSong("
                java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ UnknownHostException -> 0x0137, IndexOutOfBoundsException -> 0x016d, MalformedURLException -> 0x01a3, IOException -> 0x01d9, NullPointerException -> 0x0210 }
                com.shoujiduoduo.base.bean.y r8 = r10.c     // Catch:{ UnknownHostException -> 0x0137, IndexOutOfBoundsException -> 0x016d, MalformedURLException -> 0x01a3, IOException -> 0x01d9, NullPointerException -> 0x0210 }
                int r8 = r8.c     // Catch:{ UnknownHostException -> 0x0137, IndexOutOfBoundsException -> 0x016d, MalformedURLException -> 0x01a3, IOException -> 0x01d9, NullPointerException -> 0x0210 }
                java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ UnknownHostException -> 0x0137, IndexOutOfBoundsException -> 0x016d, MalformedURLException -> 0x01a3, IOException -> 0x01d9, NullPointerException -> 0x0210 }
                java.lang.String r8 = "): conn = "
                java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ UnknownHostException -> 0x0137, IndexOutOfBoundsException -> 0x016d, MalformedURLException -> 0x01a3, IOException -> 0x01d9, NullPointerException -> 0x0210 }
                java.lang.String r8 = r0.toString()     // Catch:{ UnknownHostException -> 0x0137, IndexOutOfBoundsException -> 0x016d, MalformedURLException -> 0x01a3, IOException -> 0x01d9, NullPointerException -> 0x0210 }
                java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ UnknownHostException -> 0x0137, IndexOutOfBoundsException -> 0x016d, MalformedURLException -> 0x01a3, IOException -> 0x01d9, NullPointerException -> 0x0210 }
                java.lang.String r7 = r7.toString()     // Catch:{ UnknownHostException -> 0x0137, IndexOutOfBoundsException -> 0x016d, MalformedURLException -> 0x01a3, IOException -> 0x01d9, NullPointerException -> 0x0210 }
                com.shoujiduoduo.base.a.a.a(r5, r7)     // Catch:{ UnknownHostException -> 0x0137, IndexOutOfBoundsException -> 0x016d, MalformedURLException -> 0x01a3, IOException -> 0x01d9, NullPointerException -> 0x0210 }
                java.lang.String r5 = "RANGE"
                java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ UnknownHostException -> 0x0137, IndexOutOfBoundsException -> 0x016d, MalformedURLException -> 0x01a3, IOException -> 0x01d9, NullPointerException -> 0x0210 }
                r7.<init>()     // Catch:{ UnknownHostException -> 0x0137, IndexOutOfBoundsException -> 0x016d, MalformedURLException -> 0x01a3, IOException -> 0x01d9, NullPointerException -> 0x0210 }
                java.lang.String r8 = "bytes="
                java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ UnknownHostException -> 0x0137, IndexOutOfBoundsException -> 0x016d, MalformedURLException -> 0x01a3, IOException -> 0x01d9, NullPointerException -> 0x0210 }
                java.lang.StringBuilder r1 = r7.append(r1)     // Catch:{ UnknownHostException -> 0x0137, IndexOutOfBoundsException -> 0x016d, MalformedURLException -> 0x01a3, IOException -> 0x01d9, NullPointerException -> 0x0210 }
                java.lang.String r7 = "-"
                java.lang.StringBuilder r1 = r1.append(r7)     // Catch:{ UnknownHostException -> 0x0137, IndexOutOfBoundsException -> 0x016d, MalformedURLException -> 0x01a3, IOException -> 0x01d9, NullPointerException -> 0x0210 }
                java.lang.String r1 = r1.toString()     // Catch:{ UnknownHostException -> 0x0137, IndexOutOfBoundsException -> 0x016d, MalformedURLException -> 0x01a3, IOException -> 0x01d9, NullPointerException -> 0x0210 }
                r0.setRequestProperty(r5, r1)     // Catch:{ UnknownHostException -> 0x0137, IndexOutOfBoundsException -> 0x016d, MalformedURLException -> 0x01a3, IOException -> 0x01d9, NullPointerException -> 0x0210 }
                r0.connect()     // Catch:{ NullPointerException -> 0x0118, UnknownHostException -> 0x0137, IndexOutOfBoundsException -> 0x016d, MalformedURLException -> 0x01a3, IOException -> 0x01d9 }
                int r4 = r0.getResponseCode()     // Catch:{ UnknownHostException -> 0x0137, IndexOutOfBoundsException -> 0x016d, MalformedURLException -> 0x01a3, IOException -> 0x01d9, NullPointerException -> 0x0210 }
            L_0x00b3:
                java.lang.String r1 = r10.b
                java.lang.StringBuilder r5 = new java.lang.StringBuilder
                r5.<init>()
                java.lang.String r7 = "downloadSong("
                java.lang.StringBuilder r5 = r5.append(r7)
                com.shoujiduoduo.base.bean.y r7 = r10.c
                int r7 = r7.c
                java.lang.StringBuilder r5 = r5.append(r7)
                java.lang.String r7 = "): http status code = "
                java.lang.StringBuilder r5 = r5.append(r7)
                java.lang.StringBuilder r5 = r5.append(r4)
                java.lang.String r7 = " ,url = "
                java.lang.StringBuilder r5 = r5.append(r7)
                java.lang.StringBuilder r5 = r5.append(r11)
                java.lang.String r5 = r5.toString()
                com.shoujiduoduo.base.a.a.a(r1, r5)
                r1 = 200(0xc8, float:2.8E-43)
                if (r4 < r1) goto L_0x00eb
                r1 = 300(0x12c, float:4.2E-43)
                if (r4 < r1) goto L_0x0117
            L_0x00eb:
                if (r4 < 0) goto L_0x0117
                java.lang.String r0 = r10.b
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                r1.<init>()
                java.lang.String r5 = "downloadSong("
                java.lang.StringBuilder r1 = r1.append(r5)
                com.shoujiduoduo.base.bean.y r5 = r10.c
                int r5 = r5.c
                java.lang.StringBuilder r1 = r1.append(r5)
                java.lang.String r5 = "): http status code error!"
                java.lang.StringBuilder r1 = r1.append(r5)
                java.lang.String r1 = r1.toString()
                com.shoujiduoduo.base.a.a.a(r0, r1)
                java.lang.String r5 = "status code error!"
                r0 = r10
                r1 = r11
                r0.a(r1, r2, r3, r4, r5)
                r0 = r6
            L_0x0117:
                return r0
            L_0x0118:
                r0 = move-exception
                r0.printStackTrace()     // Catch:{ UnknownHostException -> 0x0137, IndexOutOfBoundsException -> 0x016d, MalformedURLException -> 0x01a3, IOException -> 0x01d9, NullPointerException -> 0x0210 }
                android.content.Context r1 = com.shoujiduoduo.ringtone.RingDDApp.c()     // Catch:{ UnknownHostException -> 0x0137, IndexOutOfBoundsException -> 0x016d, MalformedURLException -> 0x01a3, IOException -> 0x01d9, NullPointerException -> 0x0210 }
                java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ UnknownHostException -> 0x0137, IndexOutOfBoundsException -> 0x016d, MalformedURLException -> 0x01a3, IOException -> 0x01d9, NullPointerException -> 0x0210 }
                r5.<init>()     // Catch:{ UnknownHostException -> 0x0137, IndexOutOfBoundsException -> 0x016d, MalformedURLException -> 0x01a3, IOException -> 0x01d9, NullPointerException -> 0x0210 }
                java.lang.String r7 = "connect NullPointerException, url : "
                java.lang.StringBuilder r5 = r5.append(r7)     // Catch:{ UnknownHostException -> 0x0137, IndexOutOfBoundsException -> 0x016d, MalformedURLException -> 0x01a3, IOException -> 0x01d9, NullPointerException -> 0x0210 }
                java.lang.StringBuilder r5 = r5.append(r11)     // Catch:{ UnknownHostException -> 0x0137, IndexOutOfBoundsException -> 0x016d, MalformedURLException -> 0x01a3, IOException -> 0x01d9, NullPointerException -> 0x0210 }
                java.lang.String r5 = r5.toString()     // Catch:{ UnknownHostException -> 0x0137, IndexOutOfBoundsException -> 0x016d, MalformedURLException -> 0x01a3, IOException -> 0x01d9, NullPointerException -> 0x0210 }
                com.umeng.analytics.b.a(r1, r5)     // Catch:{ UnknownHostException -> 0x0137, IndexOutOfBoundsException -> 0x016d, MalformedURLException -> 0x01a3, IOException -> 0x01d9, NullPointerException -> 0x0210 }
                throw r0     // Catch:{ UnknownHostException -> 0x0137, IndexOutOfBoundsException -> 0x016d, MalformedURLException -> 0x01a3, IOException -> 0x01d9, NullPointerException -> 0x0210 }
            L_0x0137:
                r0 = move-exception
                java.lang.String r1 = r10.b
                java.lang.StringBuilder r5 = new java.lang.StringBuilder
                r5.<init>()
                java.lang.String r6 = "downloadSong("
                java.lang.StringBuilder r5 = r5.append(r6)
                com.shoujiduoduo.base.bean.y r6 = r10.c
                int r6 = r6.c
                java.lang.StringBuilder r5 = r5.append(r6)
                java.lang.String r6 = "): UnknownHostException!"
                java.lang.StringBuilder r5 = r5.append(r6)
                java.lang.String r5 = r5.toString()
                com.shoujiduoduo.base.a.a.a(r1, r5)
                java.lang.String r5 = com.shoujiduoduo.base.a.b.a(r0)
                r0 = r10
                r1 = r11
                r0.a(r1, r2, r3, r4, r5)
                com.shoujiduoduo.util.p$a$a r0 = new com.shoujiduoduo.util.p$a$a
                int r1 = com.shoujiduoduo.util.p.l
                r0.<init>(r1)
                throw r0
            L_0x016d:
                r0 = move-exception
                java.lang.String r1 = r10.b
                java.lang.StringBuilder r5 = new java.lang.StringBuilder
                r5.<init>()
                java.lang.String r6 = "downloadSong("
                java.lang.StringBuilder r5 = r5.append(r6)
                com.shoujiduoduo.base.bean.y r6 = r10.c
                int r6 = r6.c
                java.lang.StringBuilder r5 = r5.append(r6)
                java.lang.String r6 = "): IndexOutOfBoundsException!"
                java.lang.StringBuilder r5 = r5.append(r6)
                java.lang.String r5 = r5.toString()
                com.shoujiduoduo.base.a.a.a(r1, r5)
                java.lang.String r5 = com.shoujiduoduo.base.a.b.a(r0)
                r0 = r10
                r1 = r11
                r0.a(r1, r2, r3, r4, r5)
                com.shoujiduoduo.util.p$a$a r0 = new com.shoujiduoduo.util.p$a$a
                int r1 = com.shoujiduoduo.util.p.s
                r0.<init>(r1)
                throw r0
            L_0x01a3:
                r0 = move-exception
                java.lang.String r1 = r10.b
                java.lang.StringBuilder r5 = new java.lang.StringBuilder
                r5.<init>()
                java.lang.String r6 = "downloadSong("
                java.lang.StringBuilder r5 = r5.append(r6)
                com.shoujiduoduo.base.bean.y r6 = r10.c
                int r6 = r6.c
                java.lang.StringBuilder r5 = r5.append(r6)
                java.lang.String r6 = "): MalformedURLException!"
                java.lang.StringBuilder r5 = r5.append(r6)
                java.lang.String r5 = r5.toString()
                com.shoujiduoduo.base.a.a.a(r1, r5)
                java.lang.String r5 = com.shoujiduoduo.base.a.b.a(r0)
                r0 = r10
                r1 = r11
                r0.a(r1, r2, r3, r4, r5)
                com.shoujiduoduo.util.p$a$a r0 = new com.shoujiduoduo.util.p$a$a
                int r1 = com.shoujiduoduo.util.p.l
                r0.<init>(r1)
                throw r0
            L_0x01d9:
                r0 = move-exception
                java.lang.String r1 = r10.b
                java.lang.StringBuilder r5 = new java.lang.StringBuilder
                r5.<init>()
                java.lang.String r7 = "downloadSong("
                java.lang.StringBuilder r5 = r5.append(r7)
                com.shoujiduoduo.base.bean.y r7 = r10.c
                int r7 = r7.c
                java.lang.StringBuilder r5 = r5.append(r7)
                java.lang.String r7 = "): IOException!"
                java.lang.StringBuilder r5 = r5.append(r7)
                java.lang.String r7 = com.shoujiduoduo.base.a.b.a(r0)
                java.lang.StringBuilder r5 = r5.append(r7)
                java.lang.String r5 = r5.toString()
                com.shoujiduoduo.base.a.a.a(r1, r5)
                java.lang.String r5 = com.shoujiduoduo.base.a.b.a(r0)
                r0 = r10
                r1 = r11
                r0.a(r1, r2, r3, r4, r5)
                r0 = r6
                goto L_0x00b3
            L_0x0210:
                r0 = move-exception
                java.lang.String r0 = r10.b
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                r1.<init>()
                java.lang.String r5 = "downloadSong("
                java.lang.StringBuilder r1 = r1.append(r5)
                com.shoujiduoduo.base.bean.y r5 = r10.c
                int r5 = r5.c
                java.lang.StringBuilder r1 = r1.append(r5)
                java.lang.String r5 = "): Null Pointer Exception!"
                java.lang.StringBuilder r1 = r1.append(r5)
                java.lang.String r1 = r1.toString()
                com.shoujiduoduo.base.a.a.a(r0, r1)
                r0 = r6
                goto L_0x00b3
            */
            throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.util.p.a.a(java.lang.String):java.net.HttpURLConnection");
        }

        private void a(String str, String str2, String str3, int i, String str4) {
            com.umeng.analytics.b.b(RingDDApp.c(), "CONNECTION_ERROR");
            if (this.c != null && i == 404) {
                w.g("rid:" + this.c.c + " url:" + str);
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.util.w.a(java.lang.String, boolean, java.lang.String):byte[]
         arg types: [java.lang.String, int, java.lang.String]
         candidates:
          com.shoujiduoduo.util.w.a(java.lang.String, java.lang.String, java.lang.String):void
          com.shoujiduoduo.util.w.a(java.lang.String, java.lang.String, boolean):boolean
          com.shoujiduoduo.util.w.a(int, int, int):byte[]
          com.shoujiduoduo.util.w.a(java.lang.String, boolean, java.lang.String):byte[] */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.util.w.b(java.lang.String, boolean):com.shoujiduoduo.base.bean.x
         arg types: [java.lang.String, int]
         candidates:
          com.shoujiduoduo.util.w.b(java.lang.String, java.lang.String):void
          com.shoujiduoduo.util.w.b(java.lang.String, boolean):com.shoujiduoduo.base.bean.x */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.util.w.a(java.lang.String, boolean):com.shoujiduoduo.base.bean.x
         arg types: [java.lang.String, int]
         candidates:
          com.shoujiduoduo.util.w.a(com.shoujiduoduo.base.bean.RingData, java.lang.String):boolean
          com.shoujiduoduo.util.w.a(java.lang.String, java.lang.String):byte[]
          com.shoujiduoduo.util.w.a(java.lang.String, boolean):com.shoujiduoduo.base.bean.x */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.util.p.a.a(int, boolean):com.shoujiduoduo.util.p$a$b
         arg types: [int, int]
         candidates:
          com.shoujiduoduo.util.p.a.a(int, java.lang.String):com.shoujiduoduo.util.p$a$b
          com.shoujiduoduo.util.p.a.a(int, boolean):com.shoujiduoduo.util.p$a$b */
        /* JADX WARNING: Code restructure failed: missing block: B:174:0x0779, code lost:
            r2 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:175:0x077a, code lost:
            r11 = r2;
            r7 = 0;
         */
        /* JADX WARNING: Removed duplicated region for block: B:157:0x0721  */
        /* JADX WARNING: Removed duplicated region for block: B:174:0x0779 A[ExcHandler: Exception (r2v40 'e' java.lang.Exception A[CUSTOM_DECLARE]), Splitter:B:126:0x05e1] */
        /* JADX WARNING: Removed duplicated region for block: B:178:0x07b3  */
        /* JADX WARNING: Removed duplicated region for block: B:181:0x07be  */
        /* JADX WARNING: Removed duplicated region for block: B:201:0x088a  */
        /* JADX WARNING: Removed duplicated region for block: B:203:0x0893  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r27 = this;
                r0 = r27
                java.lang.String r2 = r0.b
                java.lang.String r3 = "begin running."
                com.shoujiduoduo.base.a.a.a(r2, r3)
                r0 = r27
                com.shoujiduoduo.base.bean.y r2 = r0.c
                int r5 = r2.c
                r2 = 0
                r0 = r27
                com.shoujiduoduo.util.p r3 = com.shoujiduoduo.util.p.this
                java.util.HashMap r3 = r3.z
                monitor-enter(r3)
                r0 = r27
                com.shoujiduoduo.util.p r4 = com.shoujiduoduo.util.p.this     // Catch:{ all -> 0x006b }
                java.util.HashMap r4 = r4.z     // Catch:{ all -> 0x006b }
                java.lang.Integer r6 = java.lang.Integer.valueOf(r5)     // Catch:{ all -> 0x006b }
                boolean r4 = r4.containsKey(r6)     // Catch:{ all -> 0x006b }
                if (r4 == 0) goto L_0x003d
                r0 = r27
                com.shoujiduoduo.util.p r2 = com.shoujiduoduo.util.p.this     // Catch:{ all -> 0x006b }
                java.util.HashMap r2 = r2.z     // Catch:{ all -> 0x006b }
                java.lang.Integer r4 = java.lang.Integer.valueOf(r5)     // Catch:{ all -> 0x006b }
                java.lang.Object r2 = r2.get(r4)     // Catch:{ all -> 0x006b }
                com.shoujiduoduo.util.p$a r2 = (com.shoujiduoduo.util.p.a) r2     // Catch:{ all -> 0x006b }
            L_0x003d:
                r0 = r27
                com.shoujiduoduo.util.p r4 = com.shoujiduoduo.util.p.this     // Catch:{ all -> 0x006b }
                java.util.HashMap r4 = r4.z     // Catch:{ all -> 0x006b }
                java.lang.Integer r6 = java.lang.Integer.valueOf(r5)     // Catch:{ all -> 0x006b }
                r0 = r27
                r4.put(r6, r0)     // Catch:{ all -> 0x006b }
                monitor-exit(r3)     // Catch:{ all -> 0x006b }
                r0 = r27
                java.lang.String r3 = r0.b
                java.lang.String r4 = "run: 1"
                com.shoujiduoduo.base.a.a.a(r3, r4)
                if (r2 == 0) goto L_0x006e
            L_0x005a:
                boolean r3 = r2.isAlive()
                if (r3 == 0) goto L_0x006e
                r6 = 200(0xc8, double:9.9E-322)
                sleep(r6)     // Catch:{ InterruptedException -> 0x0066 }
                goto L_0x005a
            L_0x0066:
                r3 = move-exception
                r3.printStackTrace()
                goto L_0x005a
            L_0x006b:
                r2 = move-exception
                monitor-exit(r3)     // Catch:{ all -> 0x006b }
                throw r2
            L_0x006e:
                r0 = r27
                java.lang.String r2 = r0.b
                java.lang.String r3 = "run: 2"
                com.shoujiduoduo.base.a.a.a(r2, r3)
                r0 = r27
                com.shoujiduoduo.base.bean.y r2 = r0.c
                int r6 = r2.d
                r2 = 0
                r7 = 0
                r0 = r27
                com.shoujiduoduo.base.bean.y r3 = r0.c
                int r8 = r3.f
                r0 = r27
                com.shoujiduoduo.base.bean.y r3 = r0.c
                java.lang.String r3 = r3.f824a
                r0 = r27
                com.shoujiduoduo.base.bean.y r4 = r0.c
                java.lang.String r4 = r4.b
                r11 = 0
                r0 = r27
                boolean r9 = r0.d
                if (r9 == 0) goto L_0x00d7
                r0 = r27
                java.lang.String r2 = r0.b
                java.lang.StringBuilder r7 = new java.lang.StringBuilder
                r7.<init>()
                java.lang.String r9 = "downloadSong("
                java.lang.StringBuilder r7 = r7.append(r9)
                java.lang.StringBuilder r7 = r7.append(r5)
                java.lang.String r9 = "): Cancel 1."
                java.lang.StringBuilder r7 = r7.append(r9)
                java.lang.String r7 = r7.toString()
                com.shoujiduoduo.base.a.a.a(r2, r7)
                r0 = r27
                com.shoujiduoduo.util.p r2 = com.shoujiduoduo.util.p.this
                java.util.ArrayList r11 = r2.e
                com.shoujiduoduo.base.bean.y r2 = new com.shoujiduoduo.base.bean.y
                r7 = -1
                java.lang.String r9 = ""
                java.lang.String r10 = ""
                r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10)
                int r3 = com.shoujiduoduo.util.p.u
                r0 = r27
                r0.a(r11, r2, r3)
                r27.b()
            L_0x00d6:
                return
            L_0x00d7:
                int r15 = com.shoujiduoduo.util.w.a()
                if (r15 != 0) goto L_0x011c
                r0 = r27
                java.lang.String r2 = r0.b
                java.lang.StringBuilder r7 = new java.lang.StringBuilder
                r7.<init>()
                java.lang.String r9 = "downloadSong("
                java.lang.StringBuilder r7 = r7.append(r9)
                java.lang.StringBuilder r7 = r7.append(r5)
                java.lang.String r9 = "): network is unavailable."
                java.lang.StringBuilder r7 = r7.append(r9)
                java.lang.String r7 = r7.toString()
                com.shoujiduoduo.base.a.a.a(r2, r7)
                r0 = r27
                com.shoujiduoduo.util.p r2 = com.shoujiduoduo.util.p.this
                java.util.ArrayList r11 = r2.e
                com.shoujiduoduo.base.bean.y r2 = new com.shoujiduoduo.base.bean.y
                r7 = -1
                java.lang.String r9 = ""
                java.lang.String r10 = ""
                r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10)
                int r3 = com.shoujiduoduo.util.p.h
                r0 = r27
                r0.a(r11, r2, r3)
                r27.b()
                goto L_0x00d6
            L_0x011c:
                r10 = 0
                r9 = 0
                r0 = r27
                com.shoujiduoduo.base.bean.y r12 = r0.c
                int r12 = r12.j
                if (r12 != 0) goto L_0x01e3
                r0 = r27
                com.shoujiduoduo.base.bean.y r12 = r0.c
                java.lang.String r12 = r12.i
                java.lang.String r13 = ""
                boolean r12 = r12.equals(r13)
                if (r12 != 0) goto L_0x01e3
                r0 = r27
                com.shoujiduoduo.base.bean.y r7 = r0.c
                java.lang.String r7 = r7.i
                r8 = 0
                java.lang.String r10 = "ring"
                byte[] r7 = com.shoujiduoduo.util.w.a(r7, r8, r10)
                if (r7 == 0) goto L_0x0148
                java.lang.String r2 = new java.lang.String
                r2.<init>(r7)
            L_0x0148:
                r8 = 128000(0x1f400, float:1.79366E-40)
                java.lang.String r10 = "mp3"
                r7 = 1
                r0 = r27
                java.lang.String r12 = r0.b
                java.lang.StringBuilder r13 = new java.lang.StringBuilder
                r13.<init>()
                java.lang.String r14 = "缓冲移动彩铃资源， url:"
                java.lang.StringBuilder r13 = r13.append(r14)
                java.lang.StringBuilder r13 = r13.append(r2)
                java.lang.String r13 = r13.toString()
                com.shoujiduoduo.base.a.a.a(r12, r13)
                r25 = r9
                r9 = r11
                r11 = r2
                r2 = r25
            L_0x016e:
                r0 = r27
                java.lang.String r12 = r0.b
                java.lang.StringBuilder r13 = new java.lang.StringBuilder
                r13.<init>()
                java.lang.String r14 = "downloadSong("
                java.lang.StringBuilder r13 = r13.append(r14)
                java.lang.StringBuilder r13 = r13.append(r5)
                java.lang.String r14 = "): start_pos = "
                java.lang.StringBuilder r13 = r13.append(r14)
                java.lang.StringBuilder r13 = r13.append(r6)
                java.lang.String r13 = r13.toString()
                com.shoujiduoduo.base.a.a.a(r12, r13)
                r14 = 0
                r13 = -1
                r12 = 0
                r0 = r27
                java.net.HttpURLConnection r12 = r0.a(r11)     // Catch:{ a -> 0x039d }
            L_0x019b:
                r0 = r27
                boolean r0 = r0.d
                r16 = r0
                if (r16 == 0) goto L_0x03e0
                r0 = r27
                java.lang.String r2 = r0.b
                java.lang.StringBuilder r7 = new java.lang.StringBuilder
                r7.<init>()
                java.lang.String r9 = "downloadSong("
                java.lang.StringBuilder r7 = r7.append(r9)
                java.lang.StringBuilder r7 = r7.append(r5)
                java.lang.String r9 = "): Cancel 2."
                java.lang.StringBuilder r7 = r7.append(r9)
                java.lang.String r7 = r7.toString()
                com.shoujiduoduo.base.a.a.a(r2, r7)
                r0 = r27
                com.shoujiduoduo.util.p r2 = com.shoujiduoduo.util.p.this
                java.util.ArrayList r11 = r2.e
                com.shoujiduoduo.base.bean.y r2 = new com.shoujiduoduo.base.bean.y
                r7 = -1
                java.lang.String r9 = ""
                java.lang.String r10 = ""
                r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10)
                int r3 = com.shoujiduoduo.util.p.u
                r0 = r27
                r0.a(r11, r2, r3)
                r27.b()
                goto L_0x00d6
            L_0x01e3:
                r0 = r27
                com.shoujiduoduo.base.bean.y r12 = r0.c
                int r12 = r12.l
                if (r12 != 0) goto L_0x029f
                r0 = r27
                com.shoujiduoduo.base.bean.y r12 = r0.c
                java.lang.String r12 = r12.k
                java.lang.String r13 = ""
                boolean r12 = r12.equals(r13)
                if (r12 != 0) goto L_0x029f
                r0 = r27
                java.lang.String r12 = r0.b
                java.lang.String r13 = "缓冲电信彩铃资源"
                com.shoujiduoduo.base.a.a.a(r12, r13)
                r0 = r27
                com.shoujiduoduo.base.bean.y r12 = r0.c
                java.lang.String r12 = r12.k
                java.lang.String r13 = "81007"
                boolean r12 = r12.startsWith(r13)
                if (r12 == 0) goto L_0x0254
                com.shoujiduoduo.b.e.a r12 = com.shoujiduoduo.a.b.b.g()
                com.shoujiduoduo.base.bean.ab r12 = r12.c()
                com.shoujiduoduo.util.d.b r13 = com.shoujiduoduo.util.d.b.a()
                r0 = r27
                com.shoujiduoduo.base.bean.y r14 = r0.c
                java.lang.String r14 = r14.k
                java.lang.String r12 = r12.k()
                com.shoujiduoduo.util.b.c$g r12 = r13.a(r14, r12)
                if (r12 == 0) goto L_0x08b4
                java.lang.String r7 = r12.e
                java.lang.String r2 = "wav"
            L_0x0230:
                r0 = r27
                java.lang.String r12 = r0.b
                java.lang.StringBuilder r13 = new java.lang.StringBuilder
                r13.<init>()
                java.lang.String r14 = "电信diy彩铃资源, format:wav, url:"
                java.lang.StringBuilder r13 = r13.append(r14)
                java.lang.StringBuilder r13 = r13.append(r7)
                java.lang.String r13 = r13.toString()
                com.shoujiduoduo.base.a.a.a(r12, r13)
                r25 = r9
                r9 = r11
                r11 = r7
                r7 = r10
                r10 = r2
                r2 = r25
                goto L_0x016e
            L_0x0254:
                r0 = r27
                com.shoujiduoduo.base.bean.y r9 = r0.c
                java.lang.String r9 = r9.k
                r12 = 0
                com.shoujiduoduo.base.bean.x r9 = com.shoujiduoduo.util.w.b(r9, r12)
                if (r9 == 0) goto L_0x026d
                java.lang.String r2 = r9.a()
                int r8 = r9.c()
                java.lang.String r7 = r9.b()
            L_0x026d:
                r9 = 1
                r0 = r27
                java.lang.String r12 = r0.b
                java.lang.StringBuilder r13 = new java.lang.StringBuilder
                r13.<init>()
                java.lang.String r14 = "电信自有彩铃资源, format:"
                java.lang.StringBuilder r13 = r13.append(r14)
                java.lang.StringBuilder r13 = r13.append(r7)
                java.lang.String r14 = ",url:"
                java.lang.StringBuilder r13 = r13.append(r14)
                java.lang.StringBuilder r13 = r13.append(r2)
                java.lang.String r13 = r13.toString()
                com.shoujiduoduo.base.a.a.a(r12, r13)
                r25 = r9
                r9 = r11
                r11 = r2
                r2 = r25
                r26 = r7
                r7 = r10
                r10 = r26
                goto L_0x016e
            L_0x029f:
                r0 = r27
                com.shoujiduoduo.base.bean.y r12 = r0.c
                java.lang.String r12 = r12.m
                java.lang.String r13 = ""
                boolean r12 = r12.equals(r13)
                if (r12 != 0) goto L_0x0349
                r0 = r27
                com.shoujiduoduo.base.bean.y r12 = r0.c
                boolean r12 = r12.i()
                if (r12 != 0) goto L_0x0349
                r0 = r27
                com.shoujiduoduo.base.bean.y r12 = r0.c
                java.lang.String r12 = r12.n
                java.lang.String r13 = ""
                boolean r12 = r12.equals(r13)
                if (r12 == 0) goto L_0x0349
                r0 = r27
                java.lang.String r12 = r0.b
                java.lang.String r13 = "缓冲联通彩铃资源"
                com.shoujiduoduo.base.a.a.a(r12, r13)
                r0 = r27
                com.shoujiduoduo.base.bean.y r12 = r0.c
                java.lang.String r12 = r12.m
                r13 = 0
                com.shoujiduoduo.base.bean.x r12 = com.shoujiduoduo.util.w.a(r12, r13)
                if (r12 == 0) goto L_0x0318
                java.lang.String r2 = r12.a()
                int r8 = r12.c()
                java.lang.String r7 = r12.b()
                r0 = r27
                java.lang.String r12 = r0.b
                java.lang.StringBuilder r13 = new java.lang.StringBuilder
                r13.<init>()
                java.lang.String r14 = "联通彩铃， format:"
                java.lang.StringBuilder r13 = r13.append(r14)
                java.lang.StringBuilder r13 = r13.append(r7)
                java.lang.String r14 = ", url:"
                java.lang.StringBuilder r13 = r13.append(r14)
                java.lang.StringBuilder r13 = r13.append(r2)
                java.lang.String r13 = r13.toString()
                com.shoujiduoduo.base.a.a.a(r12, r13)
            L_0x030b:
                r25 = r9
                r9 = r11
                r11 = r2
                r2 = r25
                r26 = r7
                r7 = r10
                r10 = r26
                goto L_0x016e
            L_0x0318:
                com.shoujiduoduo.util.e.a r12 = com.shoujiduoduo.util.e.a.a()
                r0 = r27
                com.shoujiduoduo.base.bean.y r13 = r0.c
                java.lang.String r13 = r13.m
                com.shoujiduoduo.util.b.c$o r12 = r12.d(r13)
                if (r12 == 0) goto L_0x032e
                com.shoujiduoduo.util.b.c$w r2 = r12.f1609a
                java.lang.String r2 = r2.h
                java.lang.String r7 = "wav"
            L_0x032e:
                r0 = r27
                java.lang.String r12 = r0.b
                java.lang.StringBuilder r13 = new java.lang.StringBuilder
                r13.<init>()
                java.lang.String r14 = "联通彩铃， format:wav, url:"
                java.lang.StringBuilder r13 = r13.append(r14)
                java.lang.StringBuilder r13 = r13.append(r2)
                java.lang.String r13 = r13.toString()
                com.shoujiduoduo.base.a.a.a(r12, r13)
                goto L_0x030b
            L_0x0349:
                r0 = r27
                java.lang.String r2 = r0.b
                java.lang.String r7 = "缓冲多多资源"
                com.shoujiduoduo.base.a.a.a(r2, r7)
                r2 = 0
                r0 = r27
                com.shoujiduoduo.util.p$a$b r2 = r0.a(r15, r2)     // Catch:{ a -> 0x036b }
                java.lang.String r11 = r2.f1685a     // Catch:{ a -> 0x036b }
                int r8 = r2.c     // Catch:{ a -> 0x036b }
                java.lang.String r7 = r2.b     // Catch:{ a -> 0x036b }
                r25 = r9
                r9 = r2
                r2 = r25
                r26 = r10
                r10 = r7
                r7 = r26
                goto L_0x016e
            L_0x036b:
                r2 = move-exception
                r11 = r2
                r0 = r27
                com.shoujiduoduo.util.p r2 = com.shoujiduoduo.util.p.this
                java.util.ArrayList r12 = r2.e
                com.shoujiduoduo.base.bean.y r2 = new com.shoujiduoduo.base.bean.y
                r0 = r27
                com.shoujiduoduo.base.bean.y r3 = r0.c
                java.lang.String r3 = r3.f824a
                r0 = r27
                com.shoujiduoduo.base.bean.y r4 = r0.c
                java.lang.String r4 = r4.b
                r0 = r27
                com.shoujiduoduo.base.bean.y r5 = r0.c
                int r5 = r5.c
                r7 = -1
                java.lang.String r9 = ""
                java.lang.String r10 = ""
                r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10)
                int r3 = r11.f1684a
                r0 = r27
                r0.a(r12, r2, r3)
                r27.b()
                goto L_0x00d6
            L_0x039d:
                r16 = move-exception
                r0 = r16
                int r0 = r0.f1684a
                r17 = r0
                int r18 = com.shoujiduoduo.util.p.l
                r0 = r17
                r1 = r18
                if (r0 == r1) goto L_0x019b
                r0 = r27
                com.shoujiduoduo.util.p r2 = com.shoujiduoduo.util.p.this
                java.util.ArrayList r11 = r2.e
                com.shoujiduoduo.base.bean.y r2 = new com.shoujiduoduo.base.bean.y
                r0 = r27
                com.shoujiduoduo.base.bean.y r3 = r0.c
                java.lang.String r3 = r3.f824a
                r0 = r27
                com.shoujiduoduo.base.bean.y r4 = r0.c
                java.lang.String r4 = r4.b
                r0 = r27
                com.shoujiduoduo.base.bean.y r5 = r0.c
                int r5 = r5.c
                r7 = -1
                java.lang.String r9 = ""
                java.lang.String r10 = ""
                r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10)
                r0 = r16
                int r3 = r0.f1684a
                r0 = r27
                r0.a(r11, r2, r3)
                r27.b()
                goto L_0x00d6
            L_0x03e0:
                if (r12 != 0) goto L_0x08ae
                if (r9 == 0) goto L_0x0475
                boolean r12 = r9.d
                if (r12 == 0) goto L_0x0475
                r2 = 1
                r0 = r27
                com.shoujiduoduo.util.p$a$b r2 = r0.a(r15, r2)     // Catch:{ a -> 0x0443 }
                java.lang.String r11 = r2.f1685a     // Catch:{ a -> 0x0443 }
                int r8 = r2.c     // Catch:{ a -> 0x0443 }
                java.lang.String r10 = r2.b     // Catch:{ a -> 0x0443 }
                r9 = r10
                r10 = r11
            L_0x03f7:
                r0 = r27
                java.net.HttpURLConnection r2 = r0.a(r10)     // Catch:{ a -> 0x04b0 }
                r0 = r27
                boolean r7 = r0.d
                if (r7 == 0) goto L_0x04e1
                r0 = r27
                java.lang.String r2 = r0.b
                java.lang.StringBuilder r7 = new java.lang.StringBuilder
                r7.<init>()
                java.lang.String r9 = "downloadSong("
                java.lang.StringBuilder r7 = r7.append(r9)
                java.lang.StringBuilder r7 = r7.append(r5)
                java.lang.String r9 = "): Cancel 2."
                java.lang.StringBuilder r7 = r7.append(r9)
                java.lang.String r7 = r7.toString()
                com.shoujiduoduo.base.a.a.a(r2, r7)
                r0 = r27
                com.shoujiduoduo.util.p r2 = com.shoujiduoduo.util.p.this
                java.util.ArrayList r11 = r2.e
                com.shoujiduoduo.base.bean.y r2 = new com.shoujiduoduo.base.bean.y
                r7 = -1
                java.lang.String r9 = ""
                java.lang.String r10 = ""
                r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10)
                int r3 = com.shoujiduoduo.util.p.u
                r0 = r27
                r0.a(r11, r2, r3)
                r27.b()
                goto L_0x00d6
            L_0x0443:
                r2 = move-exception
                r11 = r2
                r0 = r27
                com.shoujiduoduo.util.p r2 = com.shoujiduoduo.util.p.this
                java.util.ArrayList r12 = r2.e
                com.shoujiduoduo.base.bean.y r2 = new com.shoujiduoduo.base.bean.y
                r0 = r27
                com.shoujiduoduo.base.bean.y r3 = r0.c
                java.lang.String r3 = r3.f824a
                r0 = r27
                com.shoujiduoduo.base.bean.y r4 = r0.c
                java.lang.String r4 = r4.b
                r0 = r27
                com.shoujiduoduo.base.bean.y r5 = r0.c
                int r5 = r5.c
                r7 = -1
                java.lang.String r9 = ""
                java.lang.String r10 = ""
                r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10)
                int r3 = r11.f1684a
                r0 = r27
                r0.a(r12, r2, r3)
                r27.b()
                goto L_0x00d6
            L_0x0475:
                if (r9 != 0) goto L_0x08aa
                if (r7 == 0) goto L_0x0491
                r0 = r27
                com.shoujiduoduo.base.bean.y r2 = r0.c
                java.lang.String r2 = r2.i
                r7 = 1
                java.lang.String r9 = "ring"
                byte[] r2 = com.shoujiduoduo.util.w.a(r2, r7, r9)
                if (r2 == 0) goto L_0x048d
                java.lang.String r11 = new java.lang.String
                r11.<init>(r2)
            L_0x048d:
                r9 = r10
                r10 = r11
                goto L_0x03f7
            L_0x0491:
                if (r2 == 0) goto L_0x08aa
                r0 = r27
                com.shoujiduoduo.base.bean.y r2 = r0.c
                java.lang.String r2 = r2.k
                r7 = 1
                com.shoujiduoduo.base.bean.x r2 = com.shoujiduoduo.util.w.b(r2, r7)
                if (r2 == 0) goto L_0x08aa
                java.lang.String r11 = r2.a()
                int r8 = r2.c()
                java.lang.String r10 = r2.b()
                r9 = r10
                r10 = r11
                goto L_0x03f7
            L_0x04b0:
                r11 = move-exception
                r0 = r27
                com.shoujiduoduo.util.p r2 = com.shoujiduoduo.util.p.this
                java.util.ArrayList r12 = r2.e
                com.shoujiduoduo.base.bean.y r2 = new com.shoujiduoduo.base.bean.y
                r0 = r27
                com.shoujiduoduo.base.bean.y r3 = r0.c
                java.lang.String r3 = r3.f824a
                r0 = r27
                com.shoujiduoduo.base.bean.y r4 = r0.c
                java.lang.String r4 = r4.b
                r0 = r27
                com.shoujiduoduo.base.bean.y r5 = r0.c
                int r5 = r5.c
                r7 = -1
                java.lang.String r9 = ""
                java.lang.String r10 = ""
                r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10)
                int r3 = r11.f1684a
                r0 = r27
                r0.a(r12, r2, r3)
                r27.b()
                goto L_0x00d6
            L_0x04e1:
                if (r2 != 0) goto L_0x056e
                r0 = r27
                java.net.HttpURLConnection r2 = r0.a(r10)     // Catch:{ a -> 0x053d }
                if (r2 != 0) goto L_0x056e
                r0 = r27
                java.lang.String r2 = r0.b
                java.lang.StringBuilder r3 = new java.lang.StringBuilder
                r3.<init>()
                java.lang.String r4 = "downloadSong("
                java.lang.StringBuilder r3 = r3.append(r4)
                java.lang.StringBuilder r3 = r3.append(r5)
                java.lang.String r4 = "): buildConnection return null."
                java.lang.StringBuilder r3 = r3.append(r4)
                java.lang.String r3 = r3.toString()
                com.shoujiduoduo.base.a.a.a(r2, r3)
                r0 = r27
                com.shoujiduoduo.util.p r2 = com.shoujiduoduo.util.p.this
                java.util.ArrayList r11 = r2.e
                com.shoujiduoduo.base.bean.y r2 = new com.shoujiduoduo.base.bean.y
                r0 = r27
                com.shoujiduoduo.base.bean.y r3 = r0.c
                java.lang.String r3 = r3.f824a
                r0 = r27
                com.shoujiduoduo.base.bean.y r4 = r0.c
                java.lang.String r4 = r4.b
                r0 = r27
                com.shoujiduoduo.base.bean.y r5 = r0.c
                int r5 = r5.c
                r7 = -1
                java.lang.String r9 = ""
                java.lang.String r10 = ""
                r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10)
                int r3 = com.shoujiduoduo.util.p.l
                r0 = r27
                r0.a(r11, r2, r3)
                r27.b()
                goto L_0x00d6
            L_0x053d:
                r11 = move-exception
                r0 = r27
                com.shoujiduoduo.util.p r2 = com.shoujiduoduo.util.p.this
                java.util.ArrayList r12 = r2.e
                com.shoujiduoduo.base.bean.y r2 = new com.shoujiduoduo.base.bean.y
                r0 = r27
                com.shoujiduoduo.base.bean.y r3 = r0.c
                java.lang.String r3 = r3.f824a
                r0 = r27
                com.shoujiduoduo.base.bean.y r4 = r0.c
                java.lang.String r4 = r4.b
                r0 = r27
                com.shoujiduoduo.base.bean.y r5 = r0.c
                int r5 = r5.c
                r7 = -1
                java.lang.String r9 = ""
                java.lang.String r10 = ""
                r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10)
                int r3 = r11.f1684a
                r0 = r27
                r0.a(r12, r2, r3)
                r27.b()
                goto L_0x00d6
            L_0x056e:
                r0 = r27
                boolean r7 = r0.d
                if (r7 == 0) goto L_0x05b4
                r0 = r27
                java.lang.String r2 = r0.b
                java.lang.StringBuilder r7 = new java.lang.StringBuilder
                r7.<init>()
                java.lang.String r9 = "downloadSong("
                java.lang.StringBuilder r7 = r7.append(r9)
                java.lang.StringBuilder r7 = r7.append(r5)
                java.lang.String r9 = "): Cancel 2."
                java.lang.StringBuilder r7 = r7.append(r9)
                java.lang.String r7 = r7.toString()
                com.shoujiduoduo.base.a.a.a(r2, r7)
                r0 = r27
                com.shoujiduoduo.util.p r2 = com.shoujiduoduo.util.p.this
                java.util.ArrayList r11 = r2.e
                com.shoujiduoduo.base.bean.y r2 = new com.shoujiduoduo.base.bean.y
                r7 = -1
                java.lang.String r9 = ""
                java.lang.String r10 = ""
                r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10)
                int r3 = com.shoujiduoduo.util.p.u
                r0 = r27
                r0.a(r11, r2, r3)
                r27.b()
                goto L_0x00d6
            L_0x05b4:
                r20 = r2
            L_0x05b6:
                r0 = r27
                java.lang.String r2 = r0.b
                java.lang.StringBuilder r7 = new java.lang.StringBuilder
                r7.<init>()
                java.lang.String r11 = "downloadSong("
                java.lang.StringBuilder r7 = r7.append(r11)
                java.lang.StringBuilder r7 = r7.append(r5)
                java.lang.String r11 = "): connect finished!"
                java.lang.StringBuilder r7 = r7.append(r11)
                java.lang.String r7 = r7.toString()
                com.shoujiduoduo.base.a.a.a(r2, r7)
                int r21 = r20.getContentLength()     // Catch:{ IOException -> 0x08a4, Exception -> 0x089d }
                if (r21 > 0) goto L_0x05df
                r21 = 1000000000(0x3b9aca00, float:0.0047237873)
            L_0x05df:
                r0 = r27
                com.shoujiduoduo.util.p r2 = com.shoujiduoduo.util.p.this     // Catch:{ IOException -> 0x0771, Exception -> 0x0779 }
                java.util.ArrayList r11 = r2.e     // Catch:{ IOException -> 0x0771, Exception -> 0x0779 }
                com.shoujiduoduo.base.bean.y r2 = new com.shoujiduoduo.base.bean.y     // Catch:{ IOException -> 0x0771, Exception -> 0x0779 }
                r7 = 1000000000(0x3b9aca00, float:0.0047237873)
                r0 = r21
                if (r0 != r7) goto L_0x0745
                r7 = -1
            L_0x05f1:
                r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10)     // Catch:{ IOException -> 0x0771, Exception -> 0x0779 }
                int r7 = com.shoujiduoduo.util.p.v     // Catch:{ IOException -> 0x0771, Exception -> 0x0779 }
                r0 = r27
                r0.a(r11, r2, r7)     // Catch:{ IOException -> 0x0771, Exception -> 0x0779 }
                r0 = r27
                java.lang.String r7 = r0.b     // Catch:{ IOException -> 0x0771, Exception -> 0x0779 }
                java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0771, Exception -> 0x0779 }
                r2.<init>()     // Catch:{ IOException -> 0x0771, Exception -> 0x0779 }
                java.lang.String r11 = "downloadSong("
                java.lang.StringBuilder r2 = r2.append(r11)     // Catch:{ IOException -> 0x0771, Exception -> 0x0779 }
                java.lang.StringBuilder r2 = r2.append(r5)     // Catch:{ IOException -> 0x0771, Exception -> 0x0779 }
                java.lang.String r11 = "): filesize = "
                java.lang.StringBuilder r11 = r2.append(r11)     // Catch:{ IOException -> 0x0771, Exception -> 0x0779 }
                r2 = 1000000000(0x3b9aca00, float:0.0047237873)
                r0 = r21
                if (r0 != r2) goto L_0x0749
                r2 = -1
            L_0x061e:
                java.lang.StringBuilder r2 = r11.append(r2)     // Catch:{ IOException -> 0x0771, Exception -> 0x0779 }
                java.lang.String r2 = r2.toString()     // Catch:{ IOException -> 0x0771, Exception -> 0x0779 }
                com.shoujiduoduo.base.a.a.a(r7, r2)     // Catch:{ IOException -> 0x0771, Exception -> 0x0779 }
                java.io.InputStream r22 = r20.getInputStream()     // Catch:{ IOException -> 0x0771, Exception -> 0x0779 }
                r0 = r27
                java.lang.String r2 = r0.b     // Catch:{ IOException -> 0x0771, Exception -> 0x0779 }
                java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0771, Exception -> 0x0779 }
                r7.<init>()     // Catch:{ IOException -> 0x0771, Exception -> 0x0779 }
                java.lang.String r11 = "save file path:"
                java.lang.StringBuilder r7 = r7.append(r11)     // Catch:{ IOException -> 0x0771, Exception -> 0x0779 }
                r0 = r27
                com.shoujiduoduo.base.bean.y r11 = r0.c     // Catch:{ IOException -> 0x0771, Exception -> 0x0779 }
                java.lang.String r11 = r11.l()     // Catch:{ IOException -> 0x0771, Exception -> 0x0779 }
                java.lang.StringBuilder r7 = r7.append(r11)     // Catch:{ IOException -> 0x0771, Exception -> 0x0779 }
                java.lang.String r7 = r7.toString()     // Catch:{ IOException -> 0x0771, Exception -> 0x0779 }
                com.shoujiduoduo.base.a.a.a(r2, r7)     // Catch:{ IOException -> 0x0771, Exception -> 0x0779 }
                java.io.RandomAccessFile r2 = new java.io.RandomAccessFile     // Catch:{ IOException -> 0x074d, Exception -> 0x0779 }
                r0 = r27
                com.shoujiduoduo.base.bean.y r7 = r0.c     // Catch:{ IOException -> 0x074d, Exception -> 0x0779 }
                java.lang.String r7 = r7.l()     // Catch:{ IOException -> 0x074d, Exception -> 0x0779 }
                java.lang.String r11 = "rw"
                r2.<init>(r7, r11)     // Catch:{ IOException -> 0x074d, Exception -> 0x0779 }
            L_0x065e:
                r7 = 1000000000(0x3b9aca00, float:0.0047237873)
                r0 = r21
                if (r0 == r7) goto L_0x066b
                int r7 = r21 + r6
                long r12 = (long) r7
                r2.setLength(r12)     // Catch:{ IOException -> 0x0771, Exception -> 0x0779 }
            L_0x066b:
                long r12 = (long) r6     // Catch:{ IOException -> 0x0771, Exception -> 0x0779 }
                r2.seek(r12)     // Catch:{ IOException -> 0x0771, Exception -> 0x0779 }
                r7 = 1024(0x400, float:1.435E-42)
                byte[] r0 = new byte[r7]     // Catch:{ IOException -> 0x0771, Exception -> 0x0779 }
                r23 = r0
                r7 = r14
            L_0x0676:
                r11 = 0
                r12 = 1024(0x400, float:1.435E-42)
                r0 = r22
                r1 = r23
                int r11 = r0.read(r1, r11, r12)     // Catch:{ IOException -> 0x06e7, Exception -> 0x0809 }
                if (r11 <= 0) goto L_0x0810
                r12 = 0
                r0 = r23
                r2.write(r0, r12, r11)     // Catch:{ IOException -> 0x06e7, Exception -> 0x0809 }
                int r7 = r7 + r11
                r0 = r27
                boolean r11 = r0.d     // Catch:{ IOException -> 0x06e7, Exception -> 0x0809 }
                if (r11 == 0) goto L_0x07db
                r0 = r27
                java.lang.String r11 = r0.b     // Catch:{ IOException -> 0x06e7, Exception -> 0x0809 }
                java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x06e7, Exception -> 0x0809 }
                r12.<init>()     // Catch:{ IOException -> 0x06e7, Exception -> 0x0809 }
                java.lang.String r13 = "downloadSong("
                java.lang.StringBuilder r12 = r12.append(r13)     // Catch:{ IOException -> 0x06e7, Exception -> 0x0809 }
                java.lang.StringBuilder r12 = r12.append(r5)     // Catch:{ IOException -> 0x06e7, Exception -> 0x0809 }
                java.lang.String r13 = "): Cancel 2."
                java.lang.StringBuilder r12 = r12.append(r13)     // Catch:{ IOException -> 0x06e7, Exception -> 0x0809 }
                java.lang.String r12 = r12.toString()     // Catch:{ IOException -> 0x06e7, Exception -> 0x0809 }
                com.shoujiduoduo.base.a.a.a(r11, r12)     // Catch:{ IOException -> 0x06e7, Exception -> 0x0809 }
                r0 = r27
                com.shoujiduoduo.util.p r11 = com.shoujiduoduo.util.p.this     // Catch:{ IOException -> 0x06e7, Exception -> 0x0809 }
                java.util.ArrayList r22 = r11.e     // Catch:{ IOException -> 0x06e7, Exception -> 0x0809 }
                com.shoujiduoduo.base.bean.y r11 = new com.shoujiduoduo.base.bean.y     // Catch:{ IOException -> 0x06e7, Exception -> 0x0809 }
                int r15 = r7 + r6
                r12 = 1000000000(0x3b9aca00, float:0.0047237873)
                r0 = r21
                if (r0 != r12) goto L_0x07d7
                r16 = -1
            L_0x06c5:
                r12 = r3
                r13 = r4
                r14 = r5
                r17 = r8
                r18 = r9
                r19 = r10
                r11.<init>(r12, r13, r14, r15, r16, r17, r18, r19)     // Catch:{ IOException -> 0x06e7, Exception -> 0x0809 }
                int r9 = com.shoujiduoduo.util.p.u     // Catch:{ IOException -> 0x06e7, Exception -> 0x0809 }
                r0 = r27
                r1 = r22
                r0.a(r1, r11, r9)     // Catch:{ IOException -> 0x06e7, Exception -> 0x0809 }
                r2.close()     // Catch:{ IOException -> 0x06e7, Exception -> 0x0809 }
                r20.disconnect()     // Catch:{ IOException -> 0x06e7, Exception -> 0x0809 }
                r27.b()     // Catch:{ IOException -> 0x06e7, Exception -> 0x0809 }
                goto L_0x00d6
            L_0x06e7:
                r2 = move-exception
                r11 = r2
                r9 = r7
                r7 = r21
            L_0x06ec:
                r0 = r27
                java.lang.String r2 = r0.b
                java.lang.StringBuilder r10 = new java.lang.StringBuilder
                r10.<init>()
                java.lang.String r12 = "downloadSong("
                java.lang.StringBuilder r10 = r10.append(r12)
                java.lang.StringBuilder r10 = r10.append(r5)
                java.lang.String r12 = "): notify listener that disk failed!"
                java.lang.StringBuilder r10 = r10.append(r12)
                java.lang.String r10 = r10.toString()
                com.shoujiduoduo.base.a.a.a(r2, r10)
                java.lang.String r12 = r11.getLocalizedMessage()
                r0 = r27
                com.shoujiduoduo.util.p r2 = com.shoujiduoduo.util.p.this
                java.util.ArrayList r13 = r2.e
                com.shoujiduoduo.base.bean.y r2 = new com.shoujiduoduo.base.bean.y
                int r14 = r9 + r6
                r9 = 1000000000(0x3b9aca00, float:0.0047237873)
                if (r7 != r9) goto L_0x088a
                r7 = -1
            L_0x0722:
                java.lang.String r9 = ""
                java.lang.String r10 = ""
                r6 = r14
                r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10)
                if (r12 == 0) goto L_0x088d
                java.lang.String r3 = "No space left on device"
                boolean r3 = r12.contains(r3)
                if (r3 == 0) goto L_0x088d
                int r3 = com.shoujiduoduo.util.p.r
            L_0x0738:
                r0 = r27
                r0.a(r13, r2, r3)
                r11.printStackTrace()
                r27.b()
                goto L_0x00d6
            L_0x0745:
                int r7 = r21 + r6
                goto L_0x05f1
            L_0x0749:
                int r2 = r21 + r6
                goto L_0x061e
            L_0x074d:
                r2 = move-exception
                r2.printStackTrace()     // Catch:{ IOException -> 0x0771, Exception -> 0x0779 }
                r0 = r27
                java.lang.String r7 = r0.b     // Catch:{ IOException -> 0x0771, Exception -> 0x0779 }
                java.lang.String r11 = "IOException, check duoduo DIR and retry"
                com.shoujiduoduo.base.a.a.c(r7, r11)     // Catch:{ IOException -> 0x0771, Exception -> 0x0779 }
                boolean r7 = com.shoujiduoduo.util.l.a()     // Catch:{ IOException -> 0x0771, Exception -> 0x0779 }
                if (r7 == 0) goto L_0x0778
                java.io.RandomAccessFile r2 = new java.io.RandomAccessFile     // Catch:{ IOException -> 0x0771, Exception -> 0x0779 }
                r0 = r27
                com.shoujiduoduo.base.bean.y r7 = r0.c     // Catch:{ IOException -> 0x0771, Exception -> 0x0779 }
                java.lang.String r7 = r7.l()     // Catch:{ IOException -> 0x0771, Exception -> 0x0779 }
                java.lang.String r11 = "rw"
                r2.<init>(r7, r11)     // Catch:{ IOException -> 0x0771, Exception -> 0x0779 }
                goto L_0x065e
            L_0x0771:
                r2 = move-exception
                r11 = r2
                r7 = r21
                r9 = r14
                goto L_0x06ec
            L_0x0778:
                throw r2     // Catch:{ IOException -> 0x0771, Exception -> 0x0779 }
            L_0x0779:
                r2 = move-exception
                r11 = r2
                r7 = r14
            L_0x077c:
                r0 = r27
                java.lang.String r2 = r0.b
                java.lang.StringBuilder r9 = new java.lang.StringBuilder
                r9.<init>()
                java.lang.String r10 = "downloadSong("
                java.lang.StringBuilder r9 = r9.append(r10)
                java.lang.StringBuilder r9 = r9.append(r5)
                java.lang.String r10 = "): notify failed, exception"
                java.lang.StringBuilder r9 = r9.append(r10)
                java.lang.String r9 = r9.toString()
                com.shoujiduoduo.base.a.a.a(r2, r9)
                java.lang.String r12 = r11.getLocalizedMessage()
                r0 = r27
                com.shoujiduoduo.util.p r2 = com.shoujiduoduo.util.p.this
                java.util.ArrayList r13 = r2.e
                com.shoujiduoduo.base.bean.y r2 = new com.shoujiduoduo.base.bean.y
                int r14 = r7 + r6
                r7 = 1000000000(0x3b9aca00, float:0.0047237873)
                r0 = r21
                if (r0 != r7) goto L_0x0893
                r7 = -1
            L_0x07b4:
                java.lang.String r9 = ""
                java.lang.String r10 = ""
                r6 = r14
                r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10)
                if (r12 == 0) goto L_0x0897
                java.lang.String r3 = "No space left on device"
                boolean r3 = r12.contains(r3)
                if (r3 == 0) goto L_0x0897
                int r3 = com.shoujiduoduo.util.p.r
            L_0x07ca:
                r0 = r27
                r0.a(r13, r2, r3)
                r11.printStackTrace()
                r27.b()
                goto L_0x00d6
            L_0x07d7:
                int r16 = r6 + r21
                goto L_0x06c5
            L_0x07db:
                r0 = r27
                com.shoujiduoduo.util.p r11 = com.shoujiduoduo.util.p.this     // Catch:{ IOException -> 0x06e7, Exception -> 0x0809 }
                java.util.ArrayList r24 = r11.e     // Catch:{ IOException -> 0x06e7, Exception -> 0x0809 }
                com.shoujiduoduo.base.bean.y r11 = new com.shoujiduoduo.base.bean.y     // Catch:{ IOException -> 0x06e7, Exception -> 0x0809 }
                int r15 = r7 + r6
                r12 = 1000000000(0x3b9aca00, float:0.0047237873)
                r0 = r21
                if (r0 != r12) goto L_0x080d
                r16 = -1
            L_0x07f0:
                r12 = r3
                r13 = r4
                r14 = r5
                r17 = r8
                r18 = r9
                r19 = r10
                r11.<init>(r12, r13, r14, r15, r16, r17, r18, r19)     // Catch:{ IOException -> 0x06e7, Exception -> 0x0809 }
                int r12 = com.shoujiduoduo.util.p.w     // Catch:{ IOException -> 0x06e7, Exception -> 0x0809 }
                r0 = r27
                r1 = r24
                r0.a(r1, r11, r12)     // Catch:{ IOException -> 0x06e7, Exception -> 0x0809 }
                goto L_0x0676
            L_0x0809:
                r2 = move-exception
                r11 = r2
                goto L_0x077c
            L_0x080d:
                int r16 = r6 + r21
                goto L_0x07f0
            L_0x0810:
                r0 = r27
                java.lang.String r11 = r0.b     // Catch:{ IOException -> 0x06e7, Exception -> 0x0809 }
                java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x06e7, Exception -> 0x0809 }
                r12.<init>()     // Catch:{ IOException -> 0x06e7, Exception -> 0x0809 }
                java.lang.String r13 = "downloadSong("
                java.lang.StringBuilder r12 = r12.append(r13)     // Catch:{ IOException -> 0x06e7, Exception -> 0x0809 }
                java.lang.StringBuilder r12 = r12.append(r5)     // Catch:{ IOException -> 0x06e7, Exception -> 0x0809 }
                java.lang.String r13 = "): out of read loop."
                java.lang.StringBuilder r12 = r12.append(r13)     // Catch:{ IOException -> 0x06e7, Exception -> 0x0809 }
                java.lang.String r12 = r12.toString()     // Catch:{ IOException -> 0x06e7, Exception -> 0x0809 }
                com.shoujiduoduo.base.a.a.a(r11, r12)     // Catch:{ IOException -> 0x06e7, Exception -> 0x0809 }
                r0 = r27
                com.shoujiduoduo.util.p r11 = com.shoujiduoduo.util.p.this     // Catch:{ IOException -> 0x06e7, Exception -> 0x0809 }
                java.util.ArrayList r22 = r11.e     // Catch:{ IOException -> 0x06e7, Exception -> 0x0809 }
                com.shoujiduoduo.base.bean.y r11 = new com.shoujiduoduo.base.bean.y     // Catch:{ IOException -> 0x06e7, Exception -> 0x0809 }
                int r15 = r7 + r6
                r12 = 1000000000(0x3b9aca00, float:0.0047237873)
                r0 = r21
                if (r0 != r12) goto L_0x0887
                int r16 = r6 + r7
            L_0x0845:
                r12 = r3
                r13 = r4
                r14 = r5
                r17 = r8
                r18 = r9
                r19 = r10
                r11.<init>(r12, r13, r14, r15, r16, r17, r18, r19)     // Catch:{ IOException -> 0x06e7, Exception -> 0x0809 }
                int r9 = com.shoujiduoduo.util.p.x     // Catch:{ IOException -> 0x06e7, Exception -> 0x0809 }
                r0 = r27
                r1 = r22
                r0.a(r1, r11, r9)     // Catch:{ IOException -> 0x06e7, Exception -> 0x0809 }
                r2.close()     // Catch:{ IOException -> 0x06e7, Exception -> 0x0809 }
                r20.disconnect()     // Catch:{ IOException -> 0x06e7, Exception -> 0x0809 }
                r0 = r27
                java.lang.String r2 = r0.b     // Catch:{ IOException -> 0x06e7, Exception -> 0x0809 }
                java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x06e7, Exception -> 0x0809 }
                r9.<init>()     // Catch:{ IOException -> 0x06e7, Exception -> 0x0809 }
                java.lang.String r10 = "downloadSong("
                java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ IOException -> 0x06e7, Exception -> 0x0809 }
                java.lang.StringBuilder r9 = r9.append(r5)     // Catch:{ IOException -> 0x06e7, Exception -> 0x0809 }
                java.lang.String r10 = "): download thread end!"
                java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ IOException -> 0x06e7, Exception -> 0x0809 }
                java.lang.String r9 = r9.toString()     // Catch:{ IOException -> 0x06e7, Exception -> 0x0809 }
                com.shoujiduoduo.base.a.a.a(r2, r9)     // Catch:{ IOException -> 0x06e7, Exception -> 0x0809 }
                r27.b()
                goto L_0x00d6
            L_0x0887:
                int r16 = r6 + r21
                goto L_0x0845
            L_0x088a:
                int r7 = r7 + r6
                goto L_0x0722
            L_0x088d:
                int r3 = com.shoujiduoduo.util.p.o
                goto L_0x0738
            L_0x0893:
                int r7 = r6 + r21
                goto L_0x07b4
            L_0x0897:
                int r3 = com.shoujiduoduo.util.p.o
                goto L_0x07ca
            L_0x089d:
                r2 = move-exception
                r11 = r2
                r21 = r13
                r7 = r14
                goto L_0x077c
            L_0x08a4:
                r2 = move-exception
                r11 = r2
                r7 = r13
                r9 = r14
                goto L_0x06ec
            L_0x08aa:
                r9 = r10
                r10 = r11
                goto L_0x03f7
            L_0x08ae:
                r20 = r12
                r9 = r10
                r10 = r11
                goto L_0x05b6
            L_0x08b4:
                r25 = r7
                r7 = r2
                r2 = r25
                goto L_0x0230
            */
            throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.util.p.a.run():void");
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x008e, code lost:
        if (r3 == null) goto L_0x019d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0092, code lost:
        if (r3.g == null) goto L_0x019d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x009a, code lost:
        if (r3.g.equalsIgnoreCase(r13) != false) goto L_0x019d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x009c, code lost:
        a(r1);
        r0 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00a0, code lost:
        if (r0 == null) goto L_0x00ae;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00a8, code lost:
        if (android.text.TextUtils.isEmpty(r0.g) != false) goto L_0x00ae;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00ac, code lost:
        if (r0.f != 0) goto L_0x0122;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00ae, code lost:
        if (r0 != null) goto L_0x00c3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00b0, code lost:
        r0 = a(r1, r12.e, r12.f, "", 0, -1, 0, r13, "");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00bf, code lost:
        if (r0 != null) goto L_0x00cf;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00c3, code lost:
        r0.g = r13;
        r0.e = -1;
        r0.d = 0;
        r0.f = 0;
        r0.h = "";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00cf, code lost:
        r0.i = r12.n;
        r0.j = r12.q;
        r0.k = r12.s;
        r0.l = r12.v;
        r0.m = r12.A;
        r0.n = r12.D;
        r0.a(r12.a());
        r0.b(r12.f());
        r0.c(r12.e());
        r0.a(r12.d());
        r0.b(r12.c());
        r0.d(r12.g());
        r0.c(r12.h());
        new com.shoujiduoduo.util.p.a(r11, r0).start();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0122, code lost:
        r0.i = r12.n;
        r0.j = r12.q;
        r0.k = r12.s;
        r0.l = r12.v;
        r0.m = r12.A;
        r0.n = r12.D;
        r0.a(r12.a());
        r0.b(r12.f());
        r0.c(r12.e());
        r0.a(r12.d());
        r0.b(r12.c());
        r0.d(r12.g());
        r0.c(r12.h());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0178, code lost:
        if (new java.io.File(r0.l()).exists() != false) goto L_0x0189;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x017a, code lost:
        r0.d = 0;
        r0.e = -1;
        r0.g = r13;
        r0.f = 0;
        r0.h = "";
        a(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x018d, code lost:
        if (r0.d < r0.e) goto L_0x0193;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0191, code lost:
        if (r0.e >= 0) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0193, code lost:
        new com.shoujiduoduo.util.p.a(r11, r0).start();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x019d, code lost:
        r0 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:?, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:?, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:?, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:?, code lost:
        return r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.shoujiduoduo.base.bean.y a(com.shoujiduoduo.base.bean.RingData r12, java.lang.String r13) {
        /*
            r11 = this;
            r10 = 0
            r6 = -1
            r5 = 0
            int r1 = r12.k()
            com.shoujiduoduo.base.bean.y r3 = r11.b(r1)
            com.shoujiduoduo.util.p.b = r1
            java.util.HashMap<java.lang.Integer, com.shoujiduoduo.util.p$a> r4 = r11.z
            monitor-enter(r4)
            java.util.HashMap<java.lang.Integer, com.shoujiduoduo.util.p$a> r0 = r11.z     // Catch:{ all -> 0x008a }
            java.util.Set r0 = r0.entrySet()     // Catch:{ all -> 0x008a }
            java.util.Iterator r7 = r0.iterator()     // Catch:{ all -> 0x008a }
        L_0x001a:
            boolean r0 = r7.hasNext()     // Catch:{ all -> 0x008a }
            if (r0 == 0) goto L_0x008d
            java.lang.Object r0 = r7.next()     // Catch:{ all -> 0x008a }
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0     // Catch:{ all -> 0x008a }
            java.lang.Object r2 = r0.getKey()     // Catch:{ all -> 0x008a }
            java.lang.Integer r2 = (java.lang.Integer) r2     // Catch:{ all -> 0x008a }
            int r2 = r2.intValue()     // Catch:{ all -> 0x008a }
            if (r2 != r1) goto L_0x0079
            java.lang.Object r0 = r0.getValue()     // Catch:{ all -> 0x008a }
            com.shoujiduoduo.util.p$a r0 = (com.shoujiduoduo.util.p.a) r0     // Catch:{ all -> 0x008a }
            boolean r0 = r0.d     // Catch:{ all -> 0x008a }
            if (r0 != 0) goto L_0x001a
            java.lang.String r0 = com.shoujiduoduo.util.p.f1682a     // Catch:{ all -> 0x008a }
            java.lang.String r1 = "song in downloading and NOT CANCEL!"
            com.shoujiduoduo.base.a.a.a(r0, r1)     // Catch:{ all -> 0x008a }
            java.lang.String r0 = r12.a()     // Catch:{ all -> 0x008a }
            r3.a(r0)     // Catch:{ all -> 0x008a }
            int r0 = r12.f()     // Catch:{ all -> 0x008a }
            r3.b(r0)     // Catch:{ all -> 0x008a }
            java.lang.String r0 = r12.e()     // Catch:{ all -> 0x008a }
            r3.c(r0)     // Catch:{ all -> 0x008a }
            int r0 = r12.d()     // Catch:{ all -> 0x008a }
            r3.a(r0)     // Catch:{ all -> 0x008a }
            java.lang.String r0 = r12.c()     // Catch:{ all -> 0x008a }
            r3.b(r0)     // Catch:{ all -> 0x008a }
            java.lang.String r0 = r12.g()     // Catch:{ all -> 0x008a }
            r3.d(r0)     // Catch:{ all -> 0x008a }
            int r0 = r12.h()     // Catch:{ all -> 0x008a }
            r3.c(r0)     // Catch:{ all -> 0x008a }
            monitor-exit(r4)     // Catch:{ all -> 0x008a }
            r0 = r3
        L_0x0078:
            return r0
        L_0x0079:
            java.lang.Object r0 = r0.getValue()     // Catch:{ all -> 0x008a }
            com.shoujiduoduo.util.p$a r0 = (com.shoujiduoduo.util.p.a) r0     // Catch:{ all -> 0x008a }
            boolean r2 = r0.a()     // Catch:{ all -> 0x008a }
            if (r2 == 0) goto L_0x001a
            r2 = 1
            r0.a(r2)     // Catch:{ all -> 0x008a }
            goto L_0x001a
        L_0x008a:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x008a }
            throw r0
        L_0x008d:
            monitor-exit(r4)     // Catch:{ all -> 0x008a }
            if (r3 == 0) goto L_0x019d
            java.lang.String r0 = r3.g
            if (r0 == 0) goto L_0x019d
            java.lang.String r0 = r3.g
            boolean r0 = r0.equalsIgnoreCase(r13)
            if (r0 != 0) goto L_0x019d
            r11.a(r1)
            r0 = r10
        L_0x00a0:
            if (r0 == 0) goto L_0x00ae
            java.lang.String r2 = r0.g
            boolean r2 = android.text.TextUtils.isEmpty(r2)
            if (r2 != 0) goto L_0x00ae
            int r2 = r0.f
            if (r2 != 0) goto L_0x0122
        L_0x00ae:
            if (r0 != 0) goto L_0x00c3
            java.lang.String r2 = r12.e
            java.lang.String r3 = r12.f
            java.lang.String r4 = ""
            java.lang.String r9 = ""
            r0 = r11
            r7 = r5
            r8 = r13
            com.shoujiduoduo.base.bean.y r0 = r0.a(r1, r2, r3, r4, r5, r6, r7, r8, r9)
            if (r0 != 0) goto L_0x00cf
            r0 = r10
            goto L_0x0078
        L_0x00c3:
            r0.g = r13
            r0.e = r6
            r0.d = r5
            r0.f = r5
            java.lang.String r1 = ""
            r0.h = r1
        L_0x00cf:
            java.lang.String r1 = r12.n
            r0.i = r1
            int r1 = r12.q
            r0.j = r1
            java.lang.String r1 = r12.s
            r0.k = r1
            int r1 = r12.v
            r0.l = r1
            java.lang.String r1 = r12.A
            r0.m = r1
            java.lang.String r1 = r12.D
            r0.n = r1
            java.lang.String r1 = r12.a()
            r0.a(r1)
            int r1 = r12.f()
            r0.b(r1)
            java.lang.String r1 = r12.e()
            r0.c(r1)
            int r1 = r12.d()
            r0.a(r1)
            java.lang.String r1 = r12.c()
            r0.b(r1)
            java.lang.String r1 = r12.g()
            r0.d(r1)
            int r1 = r12.h()
            r0.c(r1)
            com.shoujiduoduo.util.p$a r1 = new com.shoujiduoduo.util.p$a
            r1.<init>(r0)
            r1.start()
            goto L_0x0078
        L_0x0122:
            java.lang.String r1 = r12.n
            r0.i = r1
            int r1 = r12.q
            r0.j = r1
            java.lang.String r1 = r12.s
            r0.k = r1
            int r1 = r12.v
            r0.l = r1
            java.lang.String r1 = r12.A
            r0.m = r1
            java.lang.String r1 = r12.D
            r0.n = r1
            java.lang.String r1 = r12.a()
            r0.a(r1)
            int r1 = r12.f()
            r0.b(r1)
            java.lang.String r1 = r12.e()
            r0.c(r1)
            int r1 = r12.d()
            r0.a(r1)
            java.lang.String r1 = r12.c()
            r0.b(r1)
            java.lang.String r1 = r12.g()
            r0.d(r1)
            int r1 = r12.h()
            r0.c(r1)
            java.io.File r1 = new java.io.File
            java.lang.String r2 = r0.l()
            r1.<init>(r2)
            boolean r1 = r1.exists()
            if (r1 != 0) goto L_0x0189
            r0.d = r5
            r0.e = r6
            r0.g = r13
            r0.f = r5
            java.lang.String r1 = ""
            r0.h = r1
            r11.a(r0)
        L_0x0189:
            int r1 = r0.d
            int r2 = r0.e
            if (r1 < r2) goto L_0x0193
            int r1 = r0.e
            if (r1 >= 0) goto L_0x0078
        L_0x0193:
            com.shoujiduoduo.util.p$a r1 = new com.shoujiduoduo.util.p$a
            r1.<init>(r0)
            r1.start()
            goto L_0x0078
        L_0x019d:
            r0 = r3
            goto L_0x00a0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.util.p.a(com.shoujiduoduo.base.bean.RingData, java.lang.String):com.shoujiduoduo.base.bean.y");
    }

    public void b() {
        this.c = null;
        if (this.e != null) {
            this.e.clear();
        }
    }

    /* compiled from: DownloadManager */
    class b extends Thread {

        /* renamed from: a  reason: collision with root package name */
        public boolean f1686a = true;
        public boolean b = false;
        private String d;
        private SQLiteDatabase e;
        private HashSet<String> f;

        public b(String str) {
            this.d = str;
        }

        private void a() {
            String str;
            com.shoujiduoduo.base.a.a.a(p.f1682a, "getAllContactRingID");
            this.f = new HashSet<>();
            Uri actualDefaultRingtoneUri = RingtoneManager.getActualDefaultRingtoneUri(p.this.f, 1);
            String[] strArr = {"contact_id", "display_name", "custom_ringtone", "sort_key"};
            ContentResolver contentResolver = p.this.f.getContentResolver();
            if (contentResolver != null) {
                try {
                    Cursor query = contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, strArr, null, null, "sort_key COLLATE LOCALIZED asc");
                    if (query != null && query.moveToFirst()) {
                        while (query.moveToNext()) {
                            String string = query.getString(2);
                            if (!(string == null || RingtoneManager.getRingtone(p.this.f, Uri.parse(string)) == null || string.equals("content://settings/system/ringtone") || actualDefaultRingtoneUri == null || string.equals(actualDefaultRingtoneUri.toString()))) {
                                Cursor query2 = contentResolver.query(Uri.parse(string), new String[]{"_data"}, null, null, null);
                                if (query2 != null) {
                                    int columnIndexOrThrow = query2.getColumnIndexOrThrow("_data");
                                    query2.moveToFirst();
                                    str = query2.getString(columnIndexOrThrow);
                                    query2.close();
                                } else {
                                    str = null;
                                }
                                if (str != null) {
                                    this.f.add(str);
                                }
                            }
                        }
                        query.close();
                    }
                } catch (Exception e2) {
                }
            }
        }

        /* JADX WARNING: CFG modification limit reached, blocks count: 201 */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r12 = this;
                r7 = 0
                r11 = 1
                com.shoujiduoduo.util.p r0 = com.shoujiduoduo.util.p.this
                android.database.sqlite.SQLiteDatabase r0 = r0.getWritableDatabase()
                r12.e = r0
                android.database.sqlite.SQLiteDatabase r0 = r12.e
                if (r0 == 0) goto L_0x0012
                java.lang.String r0 = r12.d
                if (r0 != 0) goto L_0x0013
            L_0x0012:
                return
            L_0x0013:
                r0 = 0
                r12.f1686a = r0
                r12.a()
                java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x003b }
                r0.<init>()     // Catch:{ SQLException -> 0x003b }
                java.lang.String r1 = "select * from "
                java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SQLException -> 0x003b }
                java.lang.String r1 = r12.d     // Catch:{ SQLException -> 0x003b }
                java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SQLException -> 0x003b }
                java.lang.String r0 = r0.toString()     // Catch:{ SQLException -> 0x003b }
                android.database.sqlite.SQLiteDatabase r1 = r12.e     // Catch:{ SQLException -> 0x003b }
                r2 = 0
                android.database.Cursor r0 = r1.rawQuery(r0, r2)     // Catch:{ SQLException -> 0x003b }
                if (r0 != 0) goto L_0x003f
                r0 = 1
                r12.f1686a = r0     // Catch:{ SQLException -> 0x003b }
                goto L_0x0012
            L_0x003b:
                r0 = move-exception
                r12.f1686a = r11
                goto L_0x0012
            L_0x003f:
                int r1 = r0.getCount()     // Catch:{ SQLException -> 0x003b }
                if (r1 != 0) goto L_0x004c
                r0.close()     // Catch:{ SQLException -> 0x003b }
                r0 = 1
                r12.f1686a = r0     // Catch:{ SQLException -> 0x003b }
                goto L_0x0012
            L_0x004c:
                java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ SQLException -> 0x003b }
                r1.<init>()     // Catch:{ SQLException -> 0x003b }
            L_0x0051:
                boolean r2 = r0.moveToNext()     // Catch:{ SQLException -> 0x003b }
                if (r2 == 0) goto L_0x006c
                boolean r2 = r12.b     // Catch:{ SQLException -> 0x003b }
                if (r2 == 0) goto L_0x005f
                r0 = 1
                r12.f1686a = r0     // Catch:{ SQLException -> 0x003b }
                goto L_0x0012
            L_0x005f:
                r2 = 1
                int r2 = r0.getInt(r2)     // Catch:{ SQLException -> 0x003b }
                java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ SQLException -> 0x003b }
                r1.add(r2)     // Catch:{ SQLException -> 0x003b }
                goto L_0x0051
            L_0x006c:
                r0.close()     // Catch:{ SQLException -> 0x003b }
                java.util.Iterator r8 = r1.iterator()
            L_0x0073:
                boolean r0 = r8.hasNext()
                if (r0 == 0) goto L_0x0232
                boolean r0 = r12.b
                if (r0 == 0) goto L_0x0080
                r12.f1686a = r11
                goto L_0x0012
            L_0x0080:
                java.lang.Object r0 = r8.next()
                java.lang.Integer r0 = (java.lang.Integer) r0
                int r4 = r0.intValue()
                com.shoujiduoduo.util.p r0 = com.shoujiduoduo.util.p.this
                android.content.Context r0 = r0.f
                java.lang.String r1 = "user_ring_phone_select"
                java.lang.String r2 = ""
                java.lang.String r0 = com.shoujiduoduo.util.as.a(r0, r1, r2)
                com.shoujiduoduo.util.p r1 = com.shoujiduoduo.util.p.this
                android.content.Context r1 = r1.f
                java.lang.String r2 = "user_ring_alarm_select"
                java.lang.String r3 = ""
                java.lang.String r1 = com.shoujiduoduo.util.as.a(r1, r2, r3)
                com.shoujiduoduo.util.p r2 = com.shoujiduoduo.util.p.this
                android.content.Context r2 = r2.f
                java.lang.String r3 = "user_ring_notification_select"
                java.lang.String r5 = ""
                java.lang.String r2 = com.shoujiduoduo.util.as.a(r2, r3, r5)
                java.lang.StringBuilder r3 = new java.lang.StringBuilder
                r3.<init>()
                java.lang.String r5 = ""
                java.lang.StringBuilder r3 = r3.append(r5)
                java.lang.StringBuilder r3 = r3.append(r4)
                java.lang.String r3 = r3.toString()
                boolean r1 = r1.equals(r3)
                if (r1 != 0) goto L_0x00ff
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                r1.<init>()
                java.lang.String r3 = ""
                java.lang.StringBuilder r1 = r1.append(r3)
                java.lang.StringBuilder r1 = r1.append(r4)
                java.lang.String r1 = r1.toString()
                boolean r0 = r0.equals(r1)
                if (r0 != 0) goto L_0x00ff
                java.lang.StringBuilder r0 = new java.lang.StringBuilder
                r0.<init>()
                java.lang.String r1 = ""
                java.lang.StringBuilder r0 = r0.append(r1)
                java.lang.StringBuilder r0 = r0.append(r4)
                java.lang.String r0 = r0.toString()
                boolean r0 = r2.equals(r0)
                if (r0 == 0) goto L_0x0131
            L_0x00ff:
                java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x012b }
                r0.<init>()     // Catch:{ SQLException -> 0x012b }
                java.lang.String r1 = "delete from "
                java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SQLException -> 0x012b }
                java.lang.String r1 = r12.d     // Catch:{ SQLException -> 0x012b }
                java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SQLException -> 0x012b }
                java.lang.String r1 = " where rid='"
                java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SQLException -> 0x012b }
                java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ SQLException -> 0x012b }
                java.lang.String r1 = "'"
                java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SQLException -> 0x012b }
                java.lang.String r0 = r0.toString()     // Catch:{ SQLException -> 0x012b }
                android.database.sqlite.SQLiteDatabase r1 = r12.e     // Catch:{ SQLException -> 0x012b }
                r1.execSQL(r0)     // Catch:{ SQLException -> 0x012b }
                goto L_0x0073
            L_0x012b:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x0073
            L_0x0131:
                java.lang.String r9 = com.shoujiduoduo.util.p.f1682a
                monitor-enter(r9)
                java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x0194 }
                r0.<init>()     // Catch:{ SQLiteException -> 0x0194 }
                java.lang.String r1 = "select * from ringtoneduoduo_resourcetable_3 where rid="
                java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SQLiteException -> 0x0194 }
                java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ SQLiteException -> 0x0194 }
                java.lang.String r1 = " order by rid;"
                java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SQLiteException -> 0x0194 }
                java.lang.String r0 = r0.toString()     // Catch:{ SQLiteException -> 0x0194 }
                android.database.sqlite.SQLiteDatabase r1 = r12.e     // Catch:{ SQLiteException -> 0x0194 }
                r2 = 0
                android.database.Cursor r0 = r1.rawQuery(r0, r2)     // Catch:{ SQLiteException -> 0x0194 }
                r6 = r0
            L_0x0157:
                if (r6 == 0) goto L_0x015f
                int r0 = r6.getCount()     // Catch:{ all -> 0x0191 }
                if (r0 != 0) goto L_0x01d3
            L_0x015f:
                if (r6 == 0) goto L_0x0164
                r6.close()     // Catch:{ all -> 0x0191 }
            L_0x0164:
                java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x019a }
                r0.<init>()     // Catch:{ SQLException -> 0x019a }
                java.lang.String r1 = "delete from "
                java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SQLException -> 0x019a }
                java.lang.String r1 = r12.d     // Catch:{ SQLException -> 0x019a }
                java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SQLException -> 0x019a }
                java.lang.String r1 = " where rid='"
                java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SQLException -> 0x019a }
                java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ SQLException -> 0x019a }
                java.lang.String r1 = "'"
                java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SQLException -> 0x019a }
                java.lang.String r0 = r0.toString()     // Catch:{ SQLException -> 0x019a }
                android.database.sqlite.SQLiteDatabase r1 = r12.e     // Catch:{ SQLException -> 0x019a }
                r1.execSQL(r0)     // Catch:{ SQLException -> 0x019a }
            L_0x018e:
                monitor-exit(r9)     // Catch:{ all -> 0x0191 }
                goto L_0x0073
            L_0x0191:
                r0 = move-exception
                monitor-exit(r9)     // Catch:{ all -> 0x0191 }
                throw r0
            L_0x0194:
                r0 = move-exception
                r0.printStackTrace()     // Catch:{ all -> 0x0191 }
                r6 = r7
                goto L_0x0157
            L_0x019a:
                r0 = move-exception
                r0.printStackTrace()     // Catch:{ all -> 0x0191 }
                goto L_0x018e
            L_0x019f:
                com.shoujiduoduo.util.p r0 = com.shoujiduoduo.util.p.this     // Catch:{ all -> 0x0191 }
                android.database.sqlite.SQLiteDatabase r1 = r12.e     // Catch:{ all -> 0x0191 }
                java.lang.String r0 = r0.a(r1, r2, r3, r4, r5)     // Catch:{ all -> 0x0191 }
                boolean r1 = com.shoujiduoduo.util.t.a(r10, r0)     // Catch:{ all -> 0x0191 }
                if (r1 == 0) goto L_0x01d3
                java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x022d }
                r1.<init>()     // Catch:{ SQLiteException -> 0x022d }
                java.lang.String r2 = "UPDATE ringtoneduoduo_resourcetable_3 SET path="
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ SQLiteException -> 0x022d }
                java.lang.String r0 = android.database.DatabaseUtils.sqlEscapeString(r0)     // Catch:{ SQLiteException -> 0x022d }
                java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ SQLiteException -> 0x022d }
                java.lang.String r1 = " WHERE rid="
                java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SQLiteException -> 0x022d }
                java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ SQLiteException -> 0x022d }
                java.lang.String r0 = r0.toString()     // Catch:{ SQLiteException -> 0x022d }
                android.database.sqlite.SQLiteDatabase r1 = r12.e     // Catch:{ SQLiteException -> 0x022d }
                r1.execSQL(r0)     // Catch:{ SQLiteException -> 0x022d }
            L_0x01d3:
                boolean r0 = r6.moveToNext()     // Catch:{ all -> 0x0191 }
                if (r0 == 0) goto L_0x01f7
                r0 = 8
                java.lang.String r5 = r6.getString(r0)     // Catch:{ all -> 0x0191 }
                r0 = 2
                java.lang.String r2 = r6.getString(r0)     // Catch:{ all -> 0x0191 }
                r0 = 3
                java.lang.String r3 = r6.getString(r0)     // Catch:{ all -> 0x0191 }
                r0 = 10
                java.lang.String r10 = r6.getString(r0)     // Catch:{ all -> 0x0191 }
                java.util.HashSet<java.lang.String> r0 = r12.f     // Catch:{ all -> 0x0191 }
                boolean r0 = r0.contains(r10)     // Catch:{ all -> 0x0191 }
                if (r0 == 0) goto L_0x019f
            L_0x01f7:
                r6.close()     // Catch:{ all -> 0x0191 }
                monitor-exit(r9)     // Catch:{ all -> 0x0191 }
                java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0227 }
                r0.<init>()     // Catch:{ SQLException -> 0x0227 }
                java.lang.String r1 = "delete from "
                java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SQLException -> 0x0227 }
                java.lang.String r1 = r12.d     // Catch:{ SQLException -> 0x0227 }
                java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SQLException -> 0x0227 }
                java.lang.String r1 = " where rid='"
                java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SQLException -> 0x0227 }
                java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ SQLException -> 0x0227 }
                java.lang.String r1 = "'"
                java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SQLException -> 0x0227 }
                java.lang.String r0 = r0.toString()     // Catch:{ SQLException -> 0x0227 }
                android.database.sqlite.SQLiteDatabase r1 = r12.e     // Catch:{ SQLException -> 0x0227 }
                r1.execSQL(r0)     // Catch:{ SQLException -> 0x0227 }
                goto L_0x0073
            L_0x0227:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x0073
            L_0x022d:
                r0 = move-exception
                r0.printStackTrace()     // Catch:{ all -> 0x0191 }
                goto L_0x01d3
            L_0x0232:
                android.database.sqlite.SQLiteDatabase r0 = r12.e     // Catch:{ SQLException -> 0x0264, NumberFormatException -> 0x0269, NullPointerException -> 0x026e }
                java.lang.String r1 = r12.d     // Catch:{ SQLException -> 0x0264, NumberFormatException -> 0x0269, NullPointerException -> 0x026e }
                long r0 = android.database.DatabaseUtils.queryNumEntries(r0, r1)     // Catch:{ SQLException -> 0x0264, NumberFormatException -> 0x0269, NullPointerException -> 0x026e }
                r2 = 0
                int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
                if (r0 != 0) goto L_0x0260
                android.database.sqlite.SQLiteDatabase r0 = r12.e     // Catch:{ SQLException -> 0x0264, NumberFormatException -> 0x0269, NullPointerException -> 0x026e }
                java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0264, NumberFormatException -> 0x0269, NullPointerException -> 0x026e }
                r1.<init>()     // Catch:{ SQLException -> 0x0264, NumberFormatException -> 0x0269, NullPointerException -> 0x026e }
                java.lang.String r2 = "DROP TABLE "
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ SQLException -> 0x0264, NumberFormatException -> 0x0269, NullPointerException -> 0x026e }
                java.lang.String r2 = r12.d     // Catch:{ SQLException -> 0x0264, NumberFormatException -> 0x0269, NullPointerException -> 0x026e }
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ SQLException -> 0x0264, NumberFormatException -> 0x0269, NullPointerException -> 0x026e }
                java.lang.String r2 = ";"
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ SQLException -> 0x0264, NumberFormatException -> 0x0269, NullPointerException -> 0x026e }
                java.lang.String r1 = r1.toString()     // Catch:{ SQLException -> 0x0264, NumberFormatException -> 0x0269, NullPointerException -> 0x026e }
                r0.execSQL(r1)     // Catch:{ SQLException -> 0x0264, NumberFormatException -> 0x0269, NullPointerException -> 0x026e }
            L_0x0260:
                r12.f1686a = r11
                goto L_0x0012
            L_0x0264:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x0260
            L_0x0269:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x0260
            L_0x026e:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x0260
            */
            throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.util.p.b.run():void");
        }
    }
}
