package com.shoujiduoduo.util.e;

import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.e.a;
import java.util.List;

/* compiled from: ChinaUnicomUtils */
class b implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a.C0031a f1663a;
    final /* synthetic */ List b;
    final /* synthetic */ String c;
    final /* synthetic */ String d;
    final /* synthetic */ String e;
    final /* synthetic */ com.shoujiduoduo.util.b.a f;
    final /* synthetic */ a g;

    b(a aVar, a.C0031a aVar2, List list, String str, String str2, String str3, com.shoujiduoduo.util.b.a aVar3) {
        this.g = aVar;
        this.f1663a = aVar2;
        this.b = list;
        this.c = str;
        this.d = str2;
        this.e = str3;
        this.f = aVar3;
    }

    public void run() {
        c.b e2;
        String str = null;
        try {
            if (this.f1663a.equals(a.C0031a.POST)) {
                str = f.a(this.b, this.c, this.d);
            } else if (this.f1663a.equals(a.C0031a.GET)) {
                str = f.a(this.b, this.d);
            }
            if (str != null) {
                e2 = this.g.b(str, this.d);
                if (e2 == null) {
                    e2 = a.h;
                }
            } else {
                e2 = a.h;
            }
        } catch (Exception e3) {
            e3.printStackTrace();
            e2 = a.i;
        }
        this.g.a(this.d, e2, this.e);
        this.f.g(e2);
    }
}
