package com.shoujiduoduo.util.e;

import java.util.Comparator;
import org.apache.http.NameValuePair;

/* compiled from: HttpUtils */
final class g implements Comparator<NameValuePair> {
    g() {
    }

    /* renamed from: a */
    public int compare(NameValuePair nameValuePair, NameValuePair nameValuePair2) {
        return nameValuePair.toString().compareTo(nameValuePair2.toString());
    }
}
