package com.shoujiduoduo.util.e;

import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.e.a;
import java.util.List;

/* compiled from: ChinaUnicomUtils */
class d implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a.C0031a f1665a;
    final /* synthetic */ List b;
    final /* synthetic */ String c;
    final /* synthetic */ String d;
    final /* synthetic */ com.shoujiduoduo.util.b.a e;
    final /* synthetic */ a f;

    d(a aVar, a.C0031a aVar2, List list, String str, String str2, com.shoujiduoduo.util.b.a aVar3) {
        this.f = aVar;
        this.f1665a = aVar2;
        this.b = list;
        this.c = str;
        this.d = str2;
        this.e = aVar3;
    }

    public void run() {
        c.b e2;
        String str = null;
        try {
            if (this.f1665a.equals(a.C0031a.POST)) {
                str = f.b(this.b, this.c);
            } else if (this.f1665a.equals(a.C0031a.GET)) {
                str = f.a(this.b, this.c);
            }
            if (str != null) {
                e2 = this.f.b(str, this.c);
                if (e2 == null) {
                    e2 = a.h;
                }
            } else {
                e2 = a.h;
            }
        } catch (Exception e3) {
            e3.printStackTrace();
            e2 = a.i;
        }
        this.f.a(this.c, e2, this.d);
        this.e.g(e2);
    }
}
