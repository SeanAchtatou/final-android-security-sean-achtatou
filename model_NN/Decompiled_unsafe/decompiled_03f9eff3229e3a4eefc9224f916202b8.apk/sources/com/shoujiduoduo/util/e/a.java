package com.shoujiduoduo.util.e;

import android.text.TextUtils;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.util.as;
import com.shoujiduoduo.util.av;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.i;
import com.shoujiduoduo.util.w;
import com.sina.weibo.sdk.exception.WeiboAuthException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

/* compiled from: ChinaUnicomUtils */
public class a {
    /* access modifiers changed from: private */
    public static final c.b h = new c.b(WeiboAuthException.DEFAULT_AUTH_ERROR_CODE, "对不起，中国联通的彩铃服务正在进行系统维护，请谅解");
    /* access modifiers changed from: private */
    public static final c.b i = new c.b("-2", "对不起，网络好像出了点问题，请稍后再试试");

    /* renamed from: a  reason: collision with root package name */
    private String f1658a = "3000004860";
    private String b = "860FF03C47581ED6";
    private String c = "www.shoujiduoduo.com";
    private String d = "openplatform";
    private String e;
    private String f;
    private boolean g;
    /* access modifiers changed from: private */
    public HashMap<String, c> j = new HashMap<>();

    /* renamed from: com.shoujiduoduo.util.e.a$a  reason: collision with other inner class name */
    /* compiled from: ChinaUnicomUtils */
    private enum C0031a {
        GET,
        POST
    }

    /* compiled from: ChinaUnicomUtils */
    private static class b {

        /* renamed from: a  reason: collision with root package name */
        public static a f1660a = new a();
    }

    /* compiled from: ChinaUnicomUtils */
    private enum d {
        unknown,
        open,
        close
    }

    public void a(String str, String str2) {
        this.e = str2;
        this.f = str;
        as.c(RingDDApp.c(), str + "_token", str2);
    }

    public boolean a(String str) {
        String a2 = as.a(RingDDApp.c(), str + "_token", "");
        if (TextUtils.isEmpty(a2)) {
            return false;
        }
        this.e = a2;
        this.f = str;
        return true;
    }

    public static String b(String str) {
        if (str.length() == 11) {
            return "917890004860" + str.substring(1);
        }
        return "";
    }

    public static a a() {
        return b.f1660a;
    }

    public void a(com.shoujiduoduo.util.b.a aVar) {
        a(new ArrayList(), "/v1/uerNetInfo/genUnikey", aVar, C0031a.GET);
    }

    public void b(com.shoujiduoduo.util.b.a aVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("access_token", this.e));
        arrayList.add(new BasicNameValuePair("showNum", "50"));
        arrayList.add(new BasicNameValuePair("pageNo", "0"));
        a(arrayList, "/v1/ringGroup/qryUserBox", aVar, C0031a.GET);
    }

    public void a(String str, com.shoujiduoduo.util.b.a aVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("access_token", this.e));
        arrayList.add(new BasicNameValuePair("boxID", str));
        a(arrayList, "/v1/ringGroup/qryBoxMem", aVar, C0031a.GET);
    }

    public void b(String str, com.shoujiduoduo.util.b.a aVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("unikey", str));
        a(arrayList, "/v1/uerNetInfo/qryUserMobile", aVar, C0031a.GET);
    }

    public void a(boolean z, String str, String str2) {
        this.g = z;
        if (this.g) {
            HashMap hashMap = new HashMap();
            hashMap.put("provinceId", str);
            hashMap.put("provinceName", str2);
            com.umeng.analytics.b.a(RingDDApp.c(), "area_support_cucc", hashMap);
        }
    }

    public boolean b() {
        return true;
    }

    public boolean c(String str) {
        String c2 = com.umeng.analytics.b.c(RingDDApp.c(), "cu_cailing_province_new");
        if (av.c(c2)) {
            c2 = "11/13/17/18/19/30/31/34/36/38/50/51/70/75/76/79/81//85/87/88/89/97";
        }
        if (c2.contains("all") || c2.contains(str)) {
            return true;
        }
        return false;
    }

    public void c(String str, com.shoujiduoduo.util.b.a aVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("callNumber", str));
        a(arrayList, "/v1/verifyCode/sendLoginCode", aVar, "&phone=" + str, C0031a.GET);
    }

    public void a(String str, String str2, com.shoujiduoduo.util.b.a aVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("callNumber", str));
        arrayList.add(new BasicNameValuePair("verifyCode", str2));
        this.f = str;
        a(arrayList, "/v1/token/codeAuthToken", aVar, "&phone=" + this.f, C0031a.GET);
    }

    public void d(String str, com.shoujiduoduo.util.b.a aVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("access_token", this.e));
        arrayList.add(new BasicNameValuePair("productId", "0000001599"));
        a(arrayList, "/v1/product/subProduct", aVar, str, C0031a.GET);
    }

    public void c(com.shoujiduoduo.util.b.a aVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("access_token", this.e));
        a(arrayList, "/v1/product/qrySubedProducts", aVar, "&phone=" + this.f, C0031a.GET);
    }

    public void d(com.shoujiduoduo.util.b.a aVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("access_token", this.e));
        arrayList.add(new BasicNameValuePair("productId", "0000001599"));
        a(arrayList, "/v1/product/unSubProduct", aVar, C0031a.GET);
    }

    public void e(com.shoujiduoduo.util.b.a aVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("access_token", this.e));
        a(arrayList, "/v1/ring/qryUserBasInfo", aVar, "&phone=" + this.f, C0031a.GET);
    }

    public void f(com.shoujiduoduo.util.b.a aVar) {
        com.shoujiduoduo.base.a.a.a("ChinaUnicomUtils", "checkCailingAndVip");
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("access_token", this.e));
        ArrayList arrayList2 = new ArrayList();
        arrayList2.add(new BasicNameValuePair("access_token", this.e));
        a(arrayList, arrayList2, "&phone=" + this.f, aVar);
    }

    public void e(String str, com.shoujiduoduo.util.b.a aVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("access_token", this.e));
        a(arrayList, "/v1/ring/openAccount", aVar, str, C0031a.GET);
    }

    public void g(com.shoujiduoduo.util.b.a aVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("access_token", this.e));
        arrayList.add(new BasicNameValuePair("serviceType", "0"));
        a(arrayList, "/v1/ring/closeAccount", aVar, C0031a.GET);
    }

    public void b(String str, String str2, com.shoujiduoduo.util.b.a aVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("access_token", this.e));
        arrayList.add(new BasicNameValuePair("toneType", "1"));
        arrayList.add(new BasicNameValuePair("toneID", str));
        arrayList.add(new BasicNameValuePair("sP_ProductID", "7000100301"));
        arrayList.add(new BasicNameValuePair("oprType", "00"));
        a(arrayList, "/v1/ring/buyTone", aVar, str2, C0031a.GET);
    }

    public void f(String str, com.shoujiduoduo.util.b.a aVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("access_token", this.e));
        arrayList.add(new BasicNameValuePair("productId", "0000001599"));
        arrayList.add(new BasicNameValuePair("contentId", str));
        a(arrayList, "/v1/product/subscribeCurrentFee", aVar, "", C0031a.GET);
    }

    public void g(String str, com.shoujiduoduo.util.b.a aVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("access_token", this.e));
        arrayList.add(new BasicNameValuePair("toneType", "1"));
        arrayList.add(new BasicNameValuePair("toneID", str));
        arrayList.add(new BasicNameValuePair("oprType", "00"));
        a(arrayList, "/v1/ring/delTone", aVar, "&phone=" + this.f, C0031a.GET);
    }

    public void a(String str, boolean z, String str2, com.shoujiduoduo.util.b.a aVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("access_token", this.e));
        arrayList.add(new BasicNameValuePair("operType", z ? "2" : "1"));
        StringBuilder sb = new StringBuilder();
        StringBuilder append = sb.append("setting=").append("{").append("\"settingObjType\":\"1\",  ").append("\"timeType\":\"0\",  ").append("\"startTime\":\"0\",  ").append("\"settingID\":\"");
        if (!z) {
            str2 = "0000000000";
        }
        append.append(str2).append("\",  ").append("\"endTime\":\"0\",  ").append("\"toneType\":\"0\",  ").append("\"toneID\":\"").append(str).append("\"").append("}");
        com.shoujiduoduo.base.a.a.a("ChinaUnicomUtils", "setTone, json:" + sb.toString());
        a(arrayList, "/v1/ringSetting/setTone", sb.toString(), aVar, "&phone=" + this.f, C0031a.POST);
    }

    public void h(com.shoujiduoduo.util.b.a aVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("access_token", this.e));
        arrayList.add(new BasicNameValuePair("showNum", "100"));
        arrayList.add(new BasicNameValuePair("pageNo", "0"));
        arrayList.add(new BasicNameValuePair("oprType", "00"));
        a(arrayList, "/v1/ringSetting/qryToneSet", aVar, "&phone=" + this.f, C0031a.GET);
    }

    public void i(com.shoujiduoduo.util.b.a aVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("access_token", this.e));
        arrayList.add(new BasicNameValuePair("showNum", "30"));
        arrayList.add(new BasicNameValuePair("pageNo", "0"));
        arrayList.add(new BasicNameValuePair("oprType", "00"));
        a(arrayList, "/v1/ring/qryUserTone", aVar, "&phone=" + this.f, C0031a.GET);
    }

    public c.b c() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("access_token", this.e));
        arrayList.add(new BasicNameValuePair("showNum", "30"));
        arrayList.add(new BasicNameValuePair("pageNo", "0"));
        arrayList.add(new BasicNameValuePair("oprType", "00"));
        try {
            String a2 = f.a(arrayList, "/v1/ring/qryUserTone");
            if (a2 == null) {
                return null;
            }
            c.b b2 = b(a2, "/v1/ring/qryUserTone");
            if (b2 != null) {
                return b2;
            }
            return null;
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public void h(String str, com.shoujiduoduo.util.b.a aVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("ringId", str));
        a(arrayList, "/v1/ring/qryRingById", aVar, C0031a.GET);
    }

    public c.o d(String str) {
        c.b b2;
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("ringId", str));
        try {
            String a2 = f.a(arrayList, "/v1/ring/qryRingById");
            if (!(a2 == null || (b2 = b(a2, "/v1/ring/qryRingById")) == null || !(b2 instanceof c.o))) {
                return (c.o) b2;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return null;
    }

    public void a(boolean z, String str, String str2, String str3, String str4, String str5, String str6, String str7, com.shoujiduoduo.util.b.a aVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("toneType", "2"));
        arrayList.add(new BasicNameValuePair("operType", z ? "4" : "1"));
        arrayList.add(new BasicNameValuePair("ringID", str2));
        arrayList.add(new BasicNameValuePair("accountID", str3));
        arrayList.add(new BasicNameValuePair("fileName", str5));
        arrayList.add(new BasicNameValuePair("ftpPath", str4));
        String c2 = com.umeng.analytics.b.c(RingDDApp.c(), "cu_ftp_ip");
        if (TextUtils.isEmpty(c2)) {
            c2 = "10.123.254.218";
        }
        arrayList.add(new BasicNameValuePair("ftpIP", c2));
        arrayList.add(new BasicNameValuePair("ftpUser", "ringdiy"));
        arrayList.add(new BasicNameValuePair("ftpPwd", "cudiy1qaz"));
        arrayList.add(new BasicNameValuePair("valiDate", "2018.10.23"));
        arrayList.add(new BasicNameValuePair("tonePrice", "0"));
        String c3 = com.umeng.analytics.b.c(RingDDApp.c(), "cu_SP_ProductID");
        if (TextUtils.isEmpty(c3)) {
            c3 = "7000102100";
        }
        arrayList.add(new BasicNameValuePair("SP_ProductID", c3));
        String c4 = com.umeng.analytics.b.c(RingDDApp.c(), "cu_sp_id");
        if (TextUtils.isEmpty(c4)) {
            c4 = "90115000";
        }
        arrayList.add(new BasicNameValuePair("spID", c4));
        String c5 = com.umeng.analytics.b.c(RingDDApp.c(), "cu_cp_id");
        if (TextUtils.isEmpty(c5)) {
            c5 = "96580000";
        }
        arrayList.add(new BasicNameValuePair("cpID", c5));
        arrayList.add(new BasicNameValuePair("toneName", str));
        arrayList.add(new BasicNameValuePair("singerName", str6));
        a(arrayList, "/v1/ring/uploadRing", aVar, str7, C0031a.GET);
    }

    public void a(String str, String str2, String str3, String str4, com.shoujiduoduo.util.b.a aVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("toneType", "2"));
        arrayList.add(new BasicNameValuePair("operType", "2"));
        arrayList.add(new BasicNameValuePair("ringID", str2));
        arrayList.add(new BasicNameValuePair("accountID", str3));
        arrayList.add(new BasicNameValuePair("fileName", ""));
        arrayList.add(new BasicNameValuePair("ftpPath", ""));
        arrayList.add(new BasicNameValuePair("ftpIP", ""));
        arrayList.add(new BasicNameValuePair("ftpUser", ""));
        arrayList.add(new BasicNameValuePair("ftpPwd", ""));
        arrayList.add(new BasicNameValuePair("valiDate", "2018.10.23"));
        arrayList.add(new BasicNameValuePair("tonePrice", "0"));
        String c2 = com.umeng.analytics.b.c(RingDDApp.c(), "cu_SP_ProductID");
        if (TextUtils.isEmpty(c2)) {
            c2 = "7000102100";
        }
        arrayList.add(new BasicNameValuePair("SP_ProductID", c2));
        String c3 = com.umeng.analytics.b.c(RingDDApp.c(), "cu_sp_id");
        if (TextUtils.isEmpty(c3)) {
            c3 = "90115000";
        }
        arrayList.add(new BasicNameValuePair("spID", c3));
        String c4 = com.umeng.analytics.b.c(RingDDApp.c(), "cu_cp_id");
        if (TextUtils.isEmpty(c4)) {
            c4 = "96580000";
        }
        arrayList.add(new BasicNameValuePair("cpID", c4));
        arrayList.add(new BasicNameValuePair("toneName", str));
        arrayList.add(new BasicNameValuePair("singerName", ""));
        a(arrayList, "/v1/ring/uploadRing", aVar, str4, C0031a.GET);
    }

    public void i(String str, com.shoujiduoduo.util.b.a aVar) {
        com.shoujiduoduo.base.a.a.a("ChinaUnicomUtils", "qryUserLocation");
        String a2 = as.a(RingDDApp.c(), str + "_provinceName", "");
        String a3 = as.a(RingDDApp.c(), str + "_provinceId", "");
        if (!TextUtils.isEmpty(a3)) {
            c.af afVar = new c.af();
            afVar.b = "000000";
            afVar.c = "成功";
            afVar.f1592a = a3;
            afVar.d = a2;
            com.shoujiduoduo.base.a.a.a("ChinaUnicomUtils", "从缓存获取归属地， provinceId:" + afVar.f1592a);
            aVar.g(afVar);
            return;
        }
        com.shoujiduoduo.base.a.a.a("ChinaUnicomUtils", "从联通查询归属地");
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("callNumber", str));
        a(arrayList, "/v1/unicomAccount/qryUserLocation", aVar, "&phone=" + this.f, C0031a.GET);
    }

    public void j(String str, com.shoujiduoduo.util.b.a aVar) {
        String a2 = as.a(RingDDApp.c(), str + "_netType", "");
        if (!TextUtils.isEmpty(a2)) {
            c.ag agVar = new c.ag();
            agVar.b = "000000";
            agVar.c = "成功";
            agVar.f1593a = a2;
            com.shoujiduoduo.base.a.a.a("ChinaUnicomUtils", "从缓存获取网络类型， netType:" + a2);
            aVar.g(agVar);
            return;
        }
        com.shoujiduoduo.base.a.a.a("ChinaUnicomUtils", "去联通查询网络类型");
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("callNumber", str));
        a(arrayList, "/v1/unicomAccount/queryUserInfo", aVar, "&phone=" + this.f, C0031a.GET);
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:113:0x01c2 */
    /* JADX WARN: Type inference failed for: r1v0 */
    /* JADX WARN: Type inference failed for: r1v15, types: [int] */
    /* JADX WARN: Type inference failed for: r1v17, types: [int] */
    /* JADX WARN: Type inference failed for: r1v20, types: [boolean] */
    /* JADX WARN: Type inference failed for: r1v21 */
    /* JADX WARN: Type inference failed for: r1v22 */
    /* JADX WARN: Type inference failed for: r1v23 */
    /* JADX WARN: Type inference failed for: r1v29 */
    /* JADX WARN: Type inference failed for: r1v30 */
    /* JADX WARN: Type inference failed for: r1v31 */
    /* access modifiers changed from: private */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.shoujiduoduo.util.b.c.b b(java.lang.String r7, java.lang.String r8) {
        /*
            r6 = this;
            r2 = 0
            r1 = 0
            java.lang.String r0 = "ChinaUnicomUtils"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "content:"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r3 = r3.append(r7)
            java.lang.String r3 = r3.toString()
            com.shoujiduoduo.base.a.a.a(r0, r3)
            if (r7 == 0) goto L_0x0024
            java.lang.String r0 = ""
            boolean r0 = r7.equals(r0)
            if (r0 == 0) goto L_0x0026
        L_0x0024:
            r0 = r2
        L_0x0025:
            return r0
        L_0x0026:
            org.json.JSONTokener r0 = new org.json.JSONTokener
            r0.<init>(r7)
            java.lang.Object r0 = r0.nextValue()     // Catch:{ JSONException -> 0x0058 }
            org.json.JSONObject r0 = (org.json.JSONObject) r0     // Catch:{ JSONException -> 0x0058 }
            java.lang.String r3 = "/v1/uerNetInfo/genUnikey"
            boolean r3 = r8.equals(r3)
            if (r3 == 0) goto L_0x005e
            com.shoujiduoduo.util.b.c$i r1 = new com.shoujiduoduo.util.b.c$i
            r1.<init>()
            java.lang.String r2 = "returnCode"
            java.lang.String r2 = r0.optString(r2)
            r1.b = r2
            java.lang.String r2 = "description"
            java.lang.String r2 = r0.optString(r2)
            r1.c = r2
            java.lang.String r2 = "unikey"
            java.lang.String r0 = r0.optString(r2)
            r1.f1602a = r0
            r0 = r1
            goto L_0x0025
        L_0x0058:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r2
            goto L_0x0025
        L_0x005e:
            java.lang.String r3 = "/v1/uerNetInfo/qryUserMobile"
            boolean r3 = r8.equals(r3)
            if (r3 == 0) goto L_0x009d
            com.shoujiduoduo.util.b.c$r r1 = new com.shoujiduoduo.util.b.c$r
            r1.<init>()
            java.lang.String r2 = "returnCode"
            java.lang.String r2 = r0.optString(r2)
            r1.b = r2
            java.lang.String r2 = "description"
            java.lang.String r2 = r0.optString(r2)
            r1.c = r2
            java.lang.String r2 = "imsi"
            java.lang.String r2 = r0.optString(r2)
            r1.d = r2
            java.lang.String r2 = "mobile"
            java.lang.String r2 = r0.optString(r2)
            r1.f1612a = r2
            java.lang.String r2 = "rateType"
            java.lang.String r2 = r0.optString(r2)
            r1.f = r2
            java.lang.String r2 = "APN"
            java.lang.String r0 = r0.optString(r2)
            r1.e = r0
            r0 = r1
            goto L_0x0025
        L_0x009d:
            java.lang.String r3 = "/v1/verifyCode/sendLoginCode"
            boolean r3 = r8.equals(r3)
            if (r3 == 0) goto L_0x00bd
            com.shoujiduoduo.util.b.c$b r1 = new com.shoujiduoduo.util.b.c$b
            r1.<init>()
            java.lang.String r2 = "returnCode"
            java.lang.String r2 = r0.optString(r2)
            r1.b = r2
            java.lang.String r2 = "description"
            java.lang.String r0 = r0.optString(r2)
            r1.c = r0
            r0 = r1
            goto L_0x0025
        L_0x00bd:
            java.lang.String r3 = "/v1/token/codeAuthToken"
            boolean r3 = r8.equals(r3)
            if (r3 == 0) goto L_0x00fc
            com.shoujiduoduo.util.b.c$a r1 = new com.shoujiduoduo.util.b.c$a
            r1.<init>()
            java.lang.String r2 = "returnCode"
            java.lang.String r2 = r0.optString(r2)
            r1.b = r2
            java.lang.String r2 = "description"
            java.lang.String r2 = r0.optString(r2)
            r1.c = r2
            java.lang.String r2 = "token"
            org.json.JSONObject r0 = r0.optJSONObject(r2)
            if (r0 == 0) goto L_0x00f9
            java.lang.String r2 = "access_token"
            java.lang.String r0 = r0.optString(r2)
            r1.f1586a = r0
            java.lang.String r0 = r1.f1586a
            r6.e = r0
            android.content.Context r0 = com.shoujiduoduo.ringtone.RingDDApp.c()
            java.lang.String r2 = "accesstoken"
            java.lang.String r3 = r6.e
            com.shoujiduoduo.util.as.c(r0, r2, r3)
        L_0x00f9:
            r0 = r1
            goto L_0x0025
        L_0x00fc:
            java.lang.String r3 = "/v1/ring/qryUserBasInfo"
            boolean r3 = r8.equals(r3)
            if (r3 == 0) goto L_0x012c
            com.shoujiduoduo.util.b.c$ad r1 = new com.shoujiduoduo.util.b.c$ad
            r1.<init>()
            java.lang.String r2 = "returnCode"
            java.lang.String r2 = r0.optString(r2)
            r1.b = r2
            java.lang.String r2 = "description"
            java.lang.String r2 = r0.optString(r2)
            r1.c = r2
            java.lang.String r2 = "userInfo"
            org.json.JSONObject r0 = r0.optJSONObject(r2)
            if (r0 == 0) goto L_0x0129
            java.lang.String r2 = "userStatus"
            java.lang.String r0 = r0.optString(r2)
            r1.f1590a = r0
        L_0x0129:
            r0 = r1
            goto L_0x0025
        L_0x012c:
            java.lang.String r3 = "/v1/ring/openAccount"
            boolean r3 = r8.equals(r3)
            if (r3 != 0) goto L_0x0164
            java.lang.String r3 = "/v1/ring/closeAccount"
            boolean r3 = r8.equals(r3)
            if (r3 != 0) goto L_0x0164
            java.lang.String r3 = "/v1/ring/buyTone"
            boolean r3 = r8.equals(r3)
            if (r3 != 0) goto L_0x0164
            java.lang.String r3 = "/v1/ring/delTone"
            boolean r3 = r8.equals(r3)
            if (r3 != 0) goto L_0x0164
            java.lang.String r3 = "/v1/product/unSubProduct"
            boolean r3 = r8.equals(r3)
            if (r3 != 0) goto L_0x0164
            java.lang.String r3 = "/v1/ring/uploadRing"
            boolean r3 = r8.equals(r3)
            if (r3 != 0) goto L_0x0164
            java.lang.String r3 = "/v1/ringSetting/setTone"
            boolean r3 = r8.equals(r3)
            if (r3 == 0) goto L_0x017c
        L_0x0164:
            com.shoujiduoduo.util.b.c$b r1 = new com.shoujiduoduo.util.b.c$b
            r1.<init>()
            java.lang.String r2 = "returnCode"
            java.lang.String r2 = r0.optString(r2)
            r1.b = r2
            java.lang.String r2 = "description"
            java.lang.String r0 = r0.optString(r2)
            r1.c = r0
            r0 = r1
            goto L_0x0025
        L_0x017c:
            java.lang.String r3 = "/v1/product/qrySubedProducts"
            boolean r3 = r8.equals(r3)
            if (r3 == 0) goto L_0x01c7
            com.shoujiduoduo.util.b.c$p r2 = new com.shoujiduoduo.util.b.c$p
            r2.<init>()
            java.lang.String r3 = "returnCode"
            java.lang.String r3 = r0.optString(r3)
            r2.b = r3
            java.lang.String r3 = "description"
            java.lang.String r3 = r0.optString(r3)
            r2.c = r3
            java.lang.String r3 = "subedProducts"
            org.json.JSONArray r3 = r0.optJSONArray(r3)
            if (r3 == 0) goto L_0x01c2
            r0 = r1
        L_0x01a2:
            int r4 = r3.length()
            if (r0 >= r4) goto L_0x01c2
            org.json.JSONObject r4 = r3.optJSONObject(r0)
            if (r4 == 0) goto L_0x01bf
            java.lang.String r5 = "productId"
            java.lang.String r4 = r4.optString(r5)
            if (r4 == 0) goto L_0x01bf
            java.lang.String r5 = "0000001599"
            boolean r4 = r5.equals(r4)
            if (r4 == 0) goto L_0x01bf
            r1 = 1
        L_0x01bf:
            int r0 = r0 + 1
            goto L_0x01a2
        L_0x01c2:
            r2.f1610a = r1
            r0 = r2
            goto L_0x0025
        L_0x01c7:
            java.lang.String r3 = "/v1/product/subProduct"
            boolean r3 = r8.equals(r3)
            if (r3 == 0) goto L_0x020a
            com.shoujiduoduo.util.b.c$ab r1 = new com.shoujiduoduo.util.b.c$ab
            r1.<init>()
            java.lang.String r2 = "returnCode"
            java.lang.String r2 = r0.optString(r2)
            r1.b = r2
            java.lang.String r2 = "description"
            java.lang.String r2 = r0.optString(r2)
            r1.c = r2
            java.lang.String r2 = "product"
            org.json.JSONObject r0 = r0.optJSONObject(r2)
            if (r0 == 0) goto L_0x0207
            com.shoujiduoduo.util.b.c$n r2 = new com.shoujiduoduo.util.b.c$n
            r2.<init>()
            r1.f1588a = r2
            com.shoujiduoduo.util.b.c$n r2 = r1.f1588a
            java.lang.String r3 = "productId"
            java.lang.String r3 = r0.optString(r3)
            r2.b = r3
            com.shoujiduoduo.util.b.c$n r2 = r1.f1588a
            java.lang.String r3 = "productName"
            java.lang.String r0 = r0.optString(r3)
            r2.f1608a = r0
        L_0x0207:
            r0 = r1
            goto L_0x0025
        L_0x020a:
            java.lang.String r3 = "/v1/ringSetting/qryToneSet"
            boolean r3 = r8.equals(r3)
            if (r3 == 0) goto L_0x0288
            com.shoujiduoduo.util.b.c$q r2 = new com.shoujiduoduo.util.b.c$q
            r2.<init>()
            java.lang.String r3 = "returnCode"
            java.lang.String r3 = r0.optString(r3)
            r2.b = r3
            java.lang.String r3 = "description"
            java.lang.String r3 = r0.optString(r3)
            r2.c = r3
            java.lang.String r3 = "totalCount"
            int r3 = r0.optInt(r3)
            r2.f1611a = r3
            java.lang.String r3 = "userToneSettingInfos"
            org.json.JSONArray r0 = r0.optJSONArray(r3)
            if (r0 == 0) goto L_0x0285
            int r3 = r2.f1611a
            if (r3 <= 0) goto L_0x0285
            int r3 = r2.f1611a
            com.shoujiduoduo.util.b.c$ai[] r3 = new com.shoujiduoduo.util.b.c.ai[r3]
            r2.d = r3
        L_0x0241:
            int r3 = r0.length()
            if (r1 >= r3) goto L_0x0285
            int r3 = r2.f1611a
            if (r1 >= r3) goto L_0x0285
            org.json.JSONObject r3 = r0.optJSONObject(r1)
            if (r3 == 0) goto L_0x0282
            com.shoujiduoduo.util.b.c$ai r4 = new com.shoujiduoduo.util.b.c$ai
            r4.<init>()
            java.lang.String r5 = "settingID"
            java.lang.String r5 = r3.optString(r5)
            r4.f1595a = r5
            java.lang.String r5 = "settingObjType"
            java.lang.String r5 = r3.optString(r5)
            r4.b = r5
            java.lang.String r5 = "toneID"
            java.lang.String r5 = r3.optString(r5)
            r4.d = r5
            java.lang.String r5 = "toneType"
            java.lang.String r5 = r3.optString(r5)
            r4.c = r5
            java.lang.String r5 = "timeType"
            java.lang.String r3 = r3.optString(r5)
            r4.e = r3
            com.shoujiduoduo.util.b.c$ai[] r3 = r2.d
            r3[r1] = r4
        L_0x0282:
            int r1 = r1 + 1
            goto L_0x0241
        L_0x0285:
            r0 = r2
            goto L_0x0025
        L_0x0288:
            java.lang.String r3 = "/v1/ring/qryUserTone"
            boolean r3 = r8.equals(r3)
            if (r3 == 0) goto L_0x0316
            com.shoujiduoduo.util.b.c$s r2 = new com.shoujiduoduo.util.b.c$s
            r2.<init>()
            java.lang.String r3 = "returnCode"
            java.lang.String r3 = r0.optString(r3)
            r2.b = r3
            java.lang.String r3 = "description"
            java.lang.String r3 = r0.optString(r3)
            r2.c = r3
            java.lang.String r3 = "totalCount"
            int r3 = r0.optInt(r3)
            r2.f1613a = r3
            java.lang.String r3 = "ringConciseInfos"
            org.json.JSONArray r0 = r0.optJSONArray(r3)
            if (r0 == 0) goto L_0x0313
            int r3 = r2.f1613a
            if (r3 <= 0) goto L_0x0313
            int r3 = r2.f1613a
            com.shoujiduoduo.util.b.c$z[] r3 = new com.shoujiduoduo.util.b.c.z[r3]
            r2.d = r3
        L_0x02bf:
            int r3 = r0.length()
            if (r1 >= r3) goto L_0x0313
            int r3 = r2.f1613a
            if (r1 >= r3) goto L_0x0313
            org.json.JSONObject r3 = r0.optJSONObject(r1)
            if (r3 == 0) goto L_0x0310
            com.shoujiduoduo.util.b.c$z r4 = new com.shoujiduoduo.util.b.c$z
            r4.<init>()
            java.lang.String r5 = "ringID"
            java.lang.String r5 = r3.optString(r5)
            r4.f1620a = r5
            java.lang.String r5 = "ringName"
            java.lang.String r5 = r3.optString(r5)
            r4.b = r5
            java.lang.String r5 = "ringPrice"
            java.lang.String r5 = r3.optString(r5)
            r4.c = r5
            java.lang.String r5 = "ringProvider"
            java.lang.String r5 = r3.optString(r5)
            r4.e = r5
            java.lang.String r5 = "ringSinger"
            java.lang.String r5 = r3.optString(r5)
            r4.d = r5
            java.lang.String r5 = "subcribeDate"
            java.lang.String r5 = r3.optString(r5)
            r4.f = r5
            java.lang.String r5 = "validDate"
            java.lang.String r3 = r3.optString(r5)
            r4.g = r3
            com.shoujiduoduo.util.b.c$z[] r3 = r2.d
            r3[r1] = r4
        L_0x0310:
            int r1 = r1 + 1
            goto L_0x02bf
        L_0x0313:
            r0 = r2
            goto L_0x0025
        L_0x0316:
            java.lang.String r1 = "/v1/ring/qryRingById"
            boolean r1 = r8.equals(r1)
            if (r1 == 0) goto L_0x0395
            com.shoujiduoduo.util.b.c$o r1 = new com.shoujiduoduo.util.b.c$o
            r1.<init>()
            java.lang.String r2 = "returnCode"
            java.lang.String r2 = r0.optString(r2)
            r1.b = r2
            java.lang.String r2 = "description"
            java.lang.String r2 = r0.optString(r2)
            r1.c = r2
            com.shoujiduoduo.util.b.c$w r2 = new com.shoujiduoduo.util.b.c$w
            r2.<init>()
            r1.f1609a = r2
            java.lang.String r2 = "ring"
            org.json.JSONObject r0 = r0.optJSONObject(r2)
            if (r0 == 0) goto L_0x0392
            com.shoujiduoduo.util.b.c$w r2 = r1.f1609a
            java.lang.String r3 = "ringId"
            java.lang.String r3 = r0.optString(r3)
            r2.f1617a = r3
            com.shoujiduoduo.util.b.c$w r2 = r1.f1609a
            java.lang.String r3 = "ringName"
            java.lang.String r3 = r0.optString(r3)
            r2.b = r3
            com.shoujiduoduo.util.b.c$w r2 = r1.f1609a
            java.lang.String r3 = "ringPrice"
            java.lang.String r3 = r0.optString(r3)
            r2.c = r3
            com.shoujiduoduo.util.b.c$w r2 = r1.f1609a
            java.lang.String r3 = "ringValidDate"
            java.lang.String r3 = r0.optString(r3)
            r2.d = r3
            com.shoujiduoduo.util.b.c$w r2 = r1.f1609a
            java.lang.String r3 = "ringValidDays"
            java.lang.String r3 = r0.optString(r3)
            r2.e = r3
            com.shoujiduoduo.util.b.c$w r2 = r1.f1609a
            java.lang.String r3 = "singerName"
            java.lang.String r3 = r0.optString(r3)
            r2.f = r3
            com.shoujiduoduo.util.b.c$w r2 = r1.f1609a
            java.lang.String r3 = "songId"
            java.lang.String r3 = r0.optString(r3)
            r2.g = r3
            com.shoujiduoduo.util.b.c$w r2 = r1.f1609a
            java.lang.String r3 = "url"
            java.lang.String r0 = r0.optString(r3)
            r2.h = r0
        L_0x0392:
            r0 = r1
            goto L_0x0025
        L_0x0395:
            java.lang.String r1 = "/v1/unicomAccount/queryUserInfo"
            boolean r1 = r8.equals(r1)
            if (r1 == 0) goto L_0x03bd
            com.shoujiduoduo.util.b.c$ag r1 = new com.shoujiduoduo.util.b.c$ag
            r1.<init>()
            java.lang.String r2 = "returnCode"
            java.lang.String r2 = r0.optString(r2)
            r1.b = r2
            java.lang.String r2 = "description"
            java.lang.String r2 = r0.optString(r2)
            r1.c = r2
            java.lang.String r2 = "netType"
            java.lang.String r0 = r0.optString(r2)
            r1.f1593a = r0
            r0 = r1
            goto L_0x0025
        L_0x03bd:
            java.lang.String r1 = "/v1/unicomAccount/qryUserLocation"
            boolean r1 = r8.equals(r1)
            if (r1 == 0) goto L_0x03ed
            com.shoujiduoduo.util.b.c$af r1 = new com.shoujiduoduo.util.b.c$af
            r1.<init>()
            java.lang.String r2 = "returnCode"
            java.lang.String r2 = r0.optString(r2)
            r1.b = r2
            java.lang.String r2 = "description"
            java.lang.String r2 = r0.optString(r2)
            r1.c = r2
            java.lang.String r2 = "provinceId"
            java.lang.String r2 = r0.optString(r2)
            r1.f1592a = r2
            java.lang.String r2 = "provinceName"
            java.lang.String r0 = r0.optString(r2)
            r1.d = r0
            r0 = r1
            goto L_0x0025
        L_0x03ed:
            java.lang.String r0 = "ChinaUnicomUtils"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r3 = "not support method : "
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.StringBuilder r1 = r1.append(r8)
            java.lang.String r1 = r1.toString()
            com.shoujiduoduo.base.a.a.a(r0, r1)
            r0 = r2
            goto L_0x0025
        */
        throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.util.e.a.b(java.lang.String, java.lang.String):com.shoujiduoduo.util.b.c$b");
    }

    private void a(List<NameValuePair> list, String str, String str2, com.shoujiduoduo.util.b.a aVar, String str3, C0031a aVar2) {
        i.a(new b(this, aVar2, list, str2, str, str3, aVar));
    }

    /* compiled from: ChinaUnicomUtils */
    private class c {

        /* renamed from: a  reason: collision with root package name */
        public d f1661a;
        public String b;

        private c() {
            this.f1661a = d.unknown;
            this.b = WeiboAuthException.DEFAULT_AUTH_ERROR_CODE;
        }

        /* synthetic */ c(a aVar, b bVar) {
            this();
        }

        public boolean a() {
            return this.b.equals("0") || this.b.equals("2") || this.b.equals("4");
        }
    }

    private void a(List<NameValuePair> list, List<NameValuePair> list2, String str, com.shoujiduoduo.util.b.a aVar) {
        i.a(new c(this, com.shoujiduoduo.a.b.b.g().c().k(), list, str, list2, aVar));
    }

    private void a(List<NameValuePair> list, String str, com.shoujiduoduo.util.b.a aVar, C0031a aVar2) {
        a(list, str, aVar, "", aVar2);
    }

    private void a(List<NameValuePair> list, String str, com.shoujiduoduo.util.b.a aVar, String str2, C0031a aVar2) {
        i.a(new d(this, aVar2, list, str, str2, aVar));
    }

    /* access modifiers changed from: private */
    public void a(String str, c.b bVar, String str2) {
        if (bVar != null && bVar.a().equals("40303")) {
            HashMap hashMap = new HashMap();
            hashMap.put("method", str);
            com.umeng.analytics.b.a(RingDDApp.c(), "cucc_app_reach_limit", hashMap);
        }
        if (bVar != null && bVar.a().equals("40304")) {
            HashMap hashMap2 = new HashMap();
            hashMap2.put("method", str);
            com.umeng.analytics.b.a(RingDDApp.c(), "cucc_api_reach_limit", hashMap2);
        }
        String k = com.shoujiduoduo.a.b.b.g().c().k();
        if (str.equals("/v1/ring/qryUserBasInfo")) {
            if (bVar.c()) {
                if (bVar instanceof c.ad) {
                    if (this.j.get(k) != null) {
                        this.j.get(k).b = ((c.ad) bVar).f1590a;
                    } else {
                        c cVar = new c(this, null);
                        cVar.b = ((c.ad) bVar).f1590a;
                        this.j.put(k, cVar);
                    }
                }
                a("cu_qry_user_basinfo", "success", str2);
                return;
            }
            a("cu_qry_user_basinfo", "fail, " + bVar.toString(), str2);
        } else if (str.equals("/v1/ring/buyTone")) {
            if (bVar.c()) {
                a("cu_buy_tone", "success", str2);
            } else {
                a("cu_buy_tone", "fail, " + bVar.toString(), str2);
            }
        } else if (str.equals("/v1/product/subProduct")) {
            HashMap hashMap3 = new HashMap();
            if (bVar.c()) {
                hashMap3.put("res", "success");
                a("cu_open_vip", "success", str2);
                if (this.j.get(k) != null) {
                    this.j.get(k).f1661a = d.open;
                } else {
                    c cVar2 = new c(this, null);
                    cVar2.f1661a = d.open;
                    this.j.put(k, cVar2);
                }
            } else {
                hashMap3.put("res", bVar.toString());
                a("cu_open_vip", "fail, " + bVar.toString(), str2);
            }
            com.umeng.analytics.b.a(RingDDApp.c(), "cucc_open_vip", hashMap3);
        } else if (str.equals("/v1/ring/openAccount")) {
            HashMap hashMap4 = new HashMap();
            if (bVar.c()) {
                hashMap4.put("res", "success");
                a("cu_open_cailing", "success", str2);
                if (this.j.get(k) != null) {
                    this.j.get(k).b = "0";
                } else {
                    c cVar3 = new c(this, null);
                    cVar3.b = "0";
                    this.j.put(k, cVar3);
                }
            } else {
                hashMap4.put("res", bVar.toString());
                a("cu_open_cailing", "fail, " + bVar.toString(), str2);
            }
            com.umeng.analytics.b.a(RingDDApp.c(), "cucc_open_cailing", hashMap4);
        } else if (str.equals("/v1/ringSetting/setTone")) {
            HashMap hashMap5 = new HashMap();
            if (bVar.c()) {
                hashMap5.put("res", "success");
                a("cu_settone", "success", str2);
            } else {
                hashMap5.put("res", bVar.toString());
                a("cu_settone", "fail, " + bVar.toString(), str2);
            }
            com.umeng.analytics.b.a(RingDDApp.c(), "cucc_set_ring", hashMap5);
        } else if (str.equals("/v1/ring/uploadRing")) {
            HashMap hashMap6 = new HashMap();
            if (bVar.c()) {
                hashMap6.put("res", "success");
                a("cu_upload_ring", "success", str2);
            } else {
                hashMap6.put("res", bVar.toString());
                a("cu_upload_ring", "fail, " + bVar.toString(), str2);
            }
            com.umeng.analytics.b.a(RingDDApp.c(), "cucc_upload_ring", hashMap6);
        } else if (str.equals("/v1/ringSetting/qryToneSet")) {
            if (bVar.c()) {
                a("cu_qry_toneset", "success", str2);
            } else {
                a("cu_qry_toneset", "fail, " + bVar.toString(), str2);
            }
        } else if (str.equals("/v1/verifyCode/sendLoginCode")) {
            if (bVar.c()) {
                a("cu_send_login_code", "success", str2);
            } else {
                a("cu_send_login_code", "fail, " + bVar.toString(), str2);
            }
        } else if (str.equals("/v1/token/codeAuthToken")) {
            if (bVar.c()) {
                a("cu_get_token", "success", str2);
            } else {
                a("cu_get_token", "fail, " + bVar.toString(), str2);
            }
        } else if (str.equals("/v1/product/qrySubedProducts")) {
            if (bVar.c()) {
                if (bVar instanceof c.p) {
                    if (this.j.get(k) != null) {
                        this.j.get(k).f1661a = ((c.p) bVar).f1610a ? d.open : d.close;
                    } else {
                        c cVar4 = new c(this, null);
                        cVar4.f1661a = ((c.p) bVar).f1610a ? d.open : d.close;
                        this.j.put(k, cVar4);
                    }
                }
                a("cu_qry_subed_products", "success", str2);
                return;
            }
            a("cu_qry_subed_products", "fail, " + bVar.toString(), str2);
        } else if (str.equals("/v1/ring/qryUserTone")) {
            if (bVar.c()) {
                a("cu_qry_user_tone", "success", str2);
            } else {
                a("cu_qry_user_tone", "fail, " + bVar.toString(), str2);
            }
        } else if (str.equals("/v1/unicomAccount/queryUserInfo")) {
            if (bVar.c()) {
                a("cu_qry_user_info", "success", str2);
                if (bVar instanceof c.ag) {
                    as.c(RingDDApp.c(), k + "_netType", ((c.ag) bVar).f1593a);
                    return;
                }
                return;
            }
            a("cu_qry_user_info", "fail, " + bVar.toString(), str2);
        } else if (str.equals("/v1/unicomAccount/qryUserLocation")) {
            com.shoujiduoduo.base.a.a.e("ChinaUnicomUtils", "log qryUserLocation");
            if (bVar.c()) {
                a("cu_qry_user_location", "success", str2);
                if (bVar instanceof c.af) {
                    as.c(RingDDApp.c(), k + "_provinceId", ((c.af) bVar).f1592a);
                    as.c(RingDDApp.c(), k + "_provinceName", ((c.af) bVar).d);
                    return;
                }
                return;
            }
            a("cu_qry_user_location", "fail, " + bVar.toString(), str2);
        } else {
            com.shoujiduoduo.base.a.a.b("ChinaUnicomUtils", "do not care method:" + str);
        }
    }

    private void a(String str, String str2, String str3) {
        w.a("cu:" + str, str2, str3);
    }
}
