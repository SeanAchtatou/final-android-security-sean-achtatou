package com.shoujiduoduo.util.e;

import com.shoujiduoduo.util.b.a;
import java.util.List;

/* compiled from: ChinaUnicomUtils */
class c implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f1664a;
    final /* synthetic */ List b;
    final /* synthetic */ String c;
    final /* synthetic */ List d;
    final /* synthetic */ a e;
    final /* synthetic */ a f;

    c(a aVar, String str, List list, String str2, List list2, a aVar2) {
        this.f = aVar;
        this.f1664a = str;
        this.b = list;
        this.c = str2;
        this.d = list2;
        this.e = aVar2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x00b7 A[Catch:{ Exception -> 0x0198 }] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0124 A[Catch:{ Exception -> 0x0198 }] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x01c1 A[Catch:{ Exception -> 0x0198 }] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x01ff A[Catch:{ Exception -> 0x0198 }] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0206 A[Catch:{ Exception -> 0x0198 }] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0264 A[Catch:{ Exception -> 0x0198 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r6 = this;
            com.shoujiduoduo.util.b.c$e r1 = new com.shoujiduoduo.util.b.c$e
            r1.<init>()
            java.lang.String r0 = "000000"
            r1.b = r0
            java.lang.String r0 = "成功"
            r1.c = r0
            com.shoujiduoduo.util.e.a r0 = r6.f
            java.util.HashMap r0 = r0.j
            java.lang.String r2 = r6.f1664a
            java.lang.Object r0 = r0.get(r2)
            com.shoujiduoduo.util.e.a$c r0 = (com.shoujiduoduo.util.e.a.c) r0
            if (r0 == 0) goto L_0x0157
            boolean r2 = r0.a()     // Catch:{ Exception -> 0x0198 }
            if (r2 == 0) goto L_0x0157
            com.shoujiduoduo.util.b.c$ad r2 = new com.shoujiduoduo.util.b.c$ad     // Catch:{ Exception -> 0x0198 }
            r2.<init>()     // Catch:{ Exception -> 0x0198 }
            java.lang.String r3 = "000000"
            r2.b = r3     // Catch:{ Exception -> 0x0198 }
            java.lang.String r3 = "成功"
            r2.c = r3     // Catch:{ Exception -> 0x0198 }
            java.lang.String r3 = r0.b     // Catch:{ Exception -> 0x0198 }
            r2.f1590a = r3     // Catch:{ Exception -> 0x0198 }
            r1.f1598a = r2     // Catch:{ Exception -> 0x0198 }
            java.lang.String r2 = "ChinaUnicomUtils"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0198 }
            r3.<init>()     // Catch:{ Exception -> 0x0198 }
            java.lang.String r4 = "从缓存获取彩铃基础业务状态， userStatus:"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0198 }
            java.lang.String r4 = r0.b     // Catch:{ Exception -> 0x0198 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0198 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0198 }
            com.shoujiduoduo.base.a.a.a(r2, r3)     // Catch:{ Exception -> 0x0198 }
        L_0x0050:
            if (r0 == 0) goto L_0x01b7
            com.shoujiduoduo.util.e.a$d r2 = r0.f1661a     // Catch:{ Exception -> 0x0198 }
            com.shoujiduoduo.util.e.a$d r3 = com.shoujiduoduo.util.e.a.d.unknown     // Catch:{ Exception -> 0x0198 }
            boolean r2 = r2.equals(r3)     // Catch:{ Exception -> 0x0198 }
            if (r2 != 0) goto L_0x01b7
            com.shoujiduoduo.util.b.c$p r2 = new com.shoujiduoduo.util.b.c$p     // Catch:{ Exception -> 0x0198 }
            r2.<init>()     // Catch:{ Exception -> 0x0198 }
            java.lang.String r3 = "000000"
            r2.b = r3     // Catch:{ Exception -> 0x0198 }
            java.lang.String r3 = "成功"
            r2.c = r3     // Catch:{ Exception -> 0x0198 }
            com.shoujiduoduo.util.e.a$d r0 = r0.f1661a     // Catch:{ Exception -> 0x0198 }
            com.shoujiduoduo.util.e.a$d r3 = com.shoujiduoduo.util.e.a.d.open     // Catch:{ Exception -> 0x0198 }
            boolean r0 = r0.equals(r3)     // Catch:{ Exception -> 0x0198 }
            if (r0 == 0) goto L_0x01b4
            r0 = 1
        L_0x0074:
            r2.f1610a = r0     // Catch:{ Exception -> 0x0198 }
            r1.d = r2     // Catch:{ Exception -> 0x0198 }
            java.lang.String r0 = "ChinaUnicomUtils"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0198 }
            r3.<init>()     // Catch:{ Exception -> 0x0198 }
            java.lang.String r4 = "从缓存获取已订购产品状态， duoduovipOpen:"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0198 }
            boolean r2 = r2.f1610a     // Catch:{ Exception -> 0x0198 }
            java.lang.StringBuilder r2 = r3.append(r2)     // Catch:{ Exception -> 0x0198 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0198 }
            com.shoujiduoduo.base.a.a.a(r0, r2)     // Catch:{ Exception -> 0x0198 }
        L_0x0092:
            android.content.Context r0 = com.shoujiduoduo.ringtone.RingDDApp.c()     // Catch:{ Exception -> 0x0198 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0198 }
            r2.<init>()     // Catch:{ Exception -> 0x0198 }
            java.lang.String r3 = r6.f1664a     // Catch:{ Exception -> 0x0198 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0198 }
            java.lang.String r3 = "_netType"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0198 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0198 }
            java.lang.String r3 = ""
            java.lang.String r0 = com.shoujiduoduo.util.as.a(r0, r2, r3)     // Catch:{ Exception -> 0x0198 }
            boolean r2 = android.text.TextUtils.isEmpty(r0)     // Catch:{ Exception -> 0x0198 }
            if (r2 != 0) goto L_0x0206
            com.shoujiduoduo.util.b.c$ag r2 = new com.shoujiduoduo.util.b.c$ag     // Catch:{ Exception -> 0x0198 }
            r2.<init>()     // Catch:{ Exception -> 0x0198 }
            java.lang.String r3 = "000000"
            r2.b = r3     // Catch:{ Exception -> 0x0198 }
            java.lang.String r3 = "成功"
            r2.c = r3     // Catch:{ Exception -> 0x0198 }
            r2.f1593a = r0     // Catch:{ Exception -> 0x0198 }
            r1.e = r2     // Catch:{ Exception -> 0x0198 }
            java.lang.String r2 = "ChinaUnicomUtils"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0198 }
            r3.<init>()     // Catch:{ Exception -> 0x0198 }
            java.lang.String r4 = "从缓存获取网络类型， netType:"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0198 }
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ Exception -> 0x0198 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0198 }
            com.shoujiduoduo.base.a.a.a(r2, r0)     // Catch:{ Exception -> 0x0198 }
        L_0x00e0:
            android.content.Context r0 = com.shoujiduoduo.ringtone.RingDDApp.c()     // Catch:{ Exception -> 0x0198 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0198 }
            r2.<init>()     // Catch:{ Exception -> 0x0198 }
            java.lang.String r3 = r6.f1664a     // Catch:{ Exception -> 0x0198 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0198 }
            java.lang.String r3 = "_provinceName"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0198 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0198 }
            java.lang.String r3 = ""
            java.lang.String r0 = com.shoujiduoduo.util.as.a(r0, r2, r3)     // Catch:{ Exception -> 0x0198 }
            android.content.Context r2 = com.shoujiduoduo.ringtone.RingDDApp.c()     // Catch:{ Exception -> 0x0198 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0198 }
            r3.<init>()     // Catch:{ Exception -> 0x0198 }
            java.lang.String r4 = r6.f1664a     // Catch:{ Exception -> 0x0198 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0198 }
            java.lang.String r4 = "_provinceId"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0198 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0198 }
            java.lang.String r4 = ""
            java.lang.String r2 = com.shoujiduoduo.util.as.a(r2, r3, r4)     // Catch:{ Exception -> 0x0198 }
            boolean r3 = android.text.TextUtils.isEmpty(r2)     // Catch:{ Exception -> 0x0198 }
            if (r3 != 0) goto L_0x0264
            com.shoujiduoduo.util.b.c$af r3 = new com.shoujiduoduo.util.b.c$af     // Catch:{ Exception -> 0x0198 }
            r3.<init>()     // Catch:{ Exception -> 0x0198 }
            java.lang.String r4 = "000000"
            r3.b = r4     // Catch:{ Exception -> 0x0198 }
            java.lang.String r4 = "成功"
            r3.c = r4     // Catch:{ Exception -> 0x0198 }
            r3.f1592a = r2     // Catch:{ Exception -> 0x0198 }
            r3.d = r0     // Catch:{ Exception -> 0x0198 }
            r1.f = r3     // Catch:{ Exception -> 0x0198 }
            java.lang.String r0 = "ChinaUnicomUtils"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0198 }
            r2.<init>()     // Catch:{ Exception -> 0x0198 }
            java.lang.String r4 = "从缓存获取归属地， provinceId:"
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x0198 }
            java.lang.String r3 = r3.f1592a     // Catch:{ Exception -> 0x0198 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0198 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0198 }
            com.shoujiduoduo.base.a.a.a(r0, r2)     // Catch:{ Exception -> 0x0198 }
        L_0x0151:
            com.shoujiduoduo.util.b.a r0 = r6.e     // Catch:{ Exception -> 0x0198 }
            r0.g(r1)     // Catch:{ Exception -> 0x0198 }
        L_0x0156:
            return
        L_0x0157:
            java.util.List r2 = r6.b     // Catch:{ Exception -> 0x0198 }
            java.lang.String r3 = "/v1/ring/qryUserBasInfo"
            java.lang.String r2 = com.shoujiduoduo.util.e.f.a(r2, r3)     // Catch:{ Exception -> 0x0198 }
            if (r2 == 0) goto L_0x01ad
            com.shoujiduoduo.util.e.a r3 = r6.f     // Catch:{ Exception -> 0x0198 }
            java.lang.String r4 = "/v1/ring/qryUserBasInfo"
            com.shoujiduoduo.util.b.c$b r2 = r3.b(r2, r4)     // Catch:{ Exception -> 0x0198 }
            if (r2 == 0) goto L_0x01a6
            r1.f1598a = r2     // Catch:{ Exception -> 0x0198 }
        L_0x016d:
            java.lang.String r2 = "ChinaUnicomUtils"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0198 }
            r3.<init>()     // Catch:{ Exception -> 0x0198 }
            java.lang.String r4 = "从联通获取彩铃基础业务状态， res:"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0198 }
            com.shoujiduoduo.util.b.c$b r4 = r1.f1598a     // Catch:{ Exception -> 0x0198 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x0198 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0198 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0198 }
            com.shoujiduoduo.base.a.a.a(r2, r3)     // Catch:{ Exception -> 0x0198 }
            com.shoujiduoduo.util.e.a r2 = r6.f     // Catch:{ Exception -> 0x0198 }
            java.lang.String r3 = "/v1/ring/qryUserBasInfo"
            com.shoujiduoduo.util.b.c$b r4 = r1.f1598a     // Catch:{ Exception -> 0x0198 }
            java.lang.String r5 = r6.c     // Catch:{ Exception -> 0x0198 }
            r2.a(r3, r4, r5)     // Catch:{ Exception -> 0x0198 }
            goto L_0x0050
        L_0x0198:
            r0 = move-exception
            r0.printStackTrace()
            com.shoujiduoduo.util.b.a r0 = r6.e
            com.shoujiduoduo.util.b.c$b r1 = com.shoujiduoduo.util.e.a.h
            r0.g(r1)
            goto L_0x0156
        L_0x01a6:
            com.shoujiduoduo.util.b.c$b r2 = com.shoujiduoduo.util.e.a.h     // Catch:{ Exception -> 0x0198 }
            r1.f1598a = r2     // Catch:{ Exception -> 0x0198 }
            goto L_0x016d
        L_0x01ad:
            com.shoujiduoduo.util.b.c$b r2 = com.shoujiduoduo.util.e.a.h     // Catch:{ Exception -> 0x0198 }
            r1.f1598a = r2     // Catch:{ Exception -> 0x0198 }
            goto L_0x016d
        L_0x01b4:
            r0 = 0
            goto L_0x0074
        L_0x01b7:
            java.util.List r0 = r6.d     // Catch:{ Exception -> 0x0198 }
            java.lang.String r2 = "/v1/product/qrySubedProducts"
            java.lang.String r0 = com.shoujiduoduo.util.e.f.a(r0, r2)     // Catch:{ Exception -> 0x0198 }
            if (r0 == 0) goto L_0x01ff
            com.shoujiduoduo.util.e.a r2 = r6.f     // Catch:{ Exception -> 0x0198 }
            java.lang.String r3 = "/v1/product/qrySubedProducts"
            com.shoujiduoduo.util.b.c$b r0 = r2.b(r0, r3)     // Catch:{ Exception -> 0x0198 }
            if (r0 == 0) goto L_0x01f8
            r1.d = r0     // Catch:{ Exception -> 0x0198 }
        L_0x01cd:
            java.lang.String r0 = "ChinaUnicomUtils"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0198 }
            r2.<init>()     // Catch:{ Exception -> 0x0198 }
            java.lang.String r3 = "从联通获取已订购产品状态， res:"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0198 }
            com.shoujiduoduo.util.b.c$b r3 = r1.d     // Catch:{ Exception -> 0x0198 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0198 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0198 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0198 }
            com.shoujiduoduo.base.a.a.a(r0, r2)     // Catch:{ Exception -> 0x0198 }
            com.shoujiduoduo.util.e.a r0 = r6.f     // Catch:{ Exception -> 0x0198 }
            java.lang.String r2 = "/v1/product/qrySubedProducts"
            com.shoujiduoduo.util.b.c$b r3 = r1.d     // Catch:{ Exception -> 0x0198 }
            java.lang.String r4 = r6.c     // Catch:{ Exception -> 0x0198 }
            r0.a(r2, r3, r4)     // Catch:{ Exception -> 0x0198 }
            goto L_0x0092
        L_0x01f8:
            com.shoujiduoduo.util.b.c$b r0 = com.shoujiduoduo.util.e.a.h     // Catch:{ Exception -> 0x0198 }
            r1.d = r0     // Catch:{ Exception -> 0x0198 }
            goto L_0x01cd
        L_0x01ff:
            com.shoujiduoduo.util.b.c$b r0 = com.shoujiduoduo.util.e.a.h     // Catch:{ Exception -> 0x0198 }
            r1.d = r0     // Catch:{ Exception -> 0x0198 }
            goto L_0x01cd
        L_0x0206:
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ Exception -> 0x0198 }
            r0.<init>()     // Catch:{ Exception -> 0x0198 }
            org.apache.http.message.BasicNameValuePair r2 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0198 }
            java.lang.String r3 = "callNumber"
            java.lang.String r4 = r6.f1664a     // Catch:{ Exception -> 0x0198 }
            r2.<init>(r3, r4)     // Catch:{ Exception -> 0x0198 }
            r0.add(r2)     // Catch:{ Exception -> 0x0198 }
            java.lang.String r2 = "/v1/unicomAccount/queryUserInfo"
            java.lang.String r0 = com.shoujiduoduo.util.e.f.a(r0, r2)     // Catch:{ Exception -> 0x0198 }
            if (r0 == 0) goto L_0x025d
            com.shoujiduoduo.util.e.a r2 = r6.f     // Catch:{ Exception -> 0x0198 }
            java.lang.String r3 = "/v1/unicomAccount/queryUserInfo"
            com.shoujiduoduo.util.b.c$b r0 = r2.b(r0, r3)     // Catch:{ Exception -> 0x0198 }
            if (r0 == 0) goto L_0x0256
            r1.e = r0     // Catch:{ Exception -> 0x0198 }
        L_0x022b:
            java.lang.String r0 = "ChinaUnicomUtils"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0198 }
            r2.<init>()     // Catch:{ Exception -> 0x0198 }
            java.lang.String r3 = "从联通获取网络类型， res:"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0198 }
            com.shoujiduoduo.util.b.c$b r3 = r1.e     // Catch:{ Exception -> 0x0198 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0198 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0198 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0198 }
            com.shoujiduoduo.base.a.a.a(r0, r2)     // Catch:{ Exception -> 0x0198 }
            com.shoujiduoduo.util.e.a r0 = r6.f     // Catch:{ Exception -> 0x0198 }
            java.lang.String r2 = "/v1/unicomAccount/queryUserInfo"
            com.shoujiduoduo.util.b.c$b r3 = r1.e     // Catch:{ Exception -> 0x0198 }
            java.lang.String r4 = r6.c     // Catch:{ Exception -> 0x0198 }
            r0.a(r2, r3, r4)     // Catch:{ Exception -> 0x0198 }
            goto L_0x00e0
        L_0x0256:
            com.shoujiduoduo.util.b.c$b r0 = com.shoujiduoduo.util.e.a.h     // Catch:{ Exception -> 0x0198 }
            r1.e = r0     // Catch:{ Exception -> 0x0198 }
            goto L_0x022b
        L_0x025d:
            com.shoujiduoduo.util.b.c$b r0 = com.shoujiduoduo.util.e.a.h     // Catch:{ Exception -> 0x0198 }
            r1.e = r0     // Catch:{ Exception -> 0x0198 }
            goto L_0x022b
        L_0x0264:
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ Exception -> 0x0198 }
            r0.<init>()     // Catch:{ Exception -> 0x0198 }
            org.apache.http.message.BasicNameValuePair r2 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0198 }
            java.lang.String r3 = "callNumber"
            java.lang.String r4 = r6.f1664a     // Catch:{ Exception -> 0x0198 }
            r2.<init>(r3, r4)     // Catch:{ Exception -> 0x0198 }
            r0.add(r2)     // Catch:{ Exception -> 0x0198 }
            java.lang.String r2 = "/v1/unicomAccount/qryUserLocation"
            java.lang.String r0 = com.shoujiduoduo.util.e.f.a(r0, r2)     // Catch:{ Exception -> 0x0198 }
            if (r0 == 0) goto L_0x02bb
            com.shoujiduoduo.util.e.a r2 = r6.f     // Catch:{ Exception -> 0x0198 }
            java.lang.String r3 = "/v1/unicomAccount/qryUserLocation"
            com.shoujiduoduo.util.b.c$b r0 = r2.b(r0, r3)     // Catch:{ Exception -> 0x0198 }
            if (r0 == 0) goto L_0x02b4
            r1.f = r0     // Catch:{ Exception -> 0x0198 }
        L_0x0289:
            java.lang.String r0 = "ChinaUnicomUtils"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0198 }
            r2.<init>()     // Catch:{ Exception -> 0x0198 }
            java.lang.String r3 = "从联通获取归属地， res:"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0198 }
            com.shoujiduoduo.util.b.c$b r3 = r1.f     // Catch:{ Exception -> 0x0198 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0198 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0198 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0198 }
            com.shoujiduoduo.base.a.a.a(r0, r2)     // Catch:{ Exception -> 0x0198 }
            com.shoujiduoduo.util.e.a r0 = r6.f     // Catch:{ Exception -> 0x0198 }
            java.lang.String r2 = "/v1/unicomAccount/qryUserLocation"
            com.shoujiduoduo.util.b.c$b r3 = r1.f     // Catch:{ Exception -> 0x0198 }
            java.lang.String r4 = r6.c     // Catch:{ Exception -> 0x0198 }
            r0.a(r2, r3, r4)     // Catch:{ Exception -> 0x0198 }
            goto L_0x0151
        L_0x02b4:
            com.shoujiduoduo.util.b.c$b r0 = com.shoujiduoduo.util.e.a.h     // Catch:{ Exception -> 0x0198 }
            r1.f = r0     // Catch:{ Exception -> 0x0198 }
            goto L_0x0289
        L_0x02bb:
            com.shoujiduoduo.util.b.c$b r0 = com.shoujiduoduo.util.e.a.h     // Catch:{ Exception -> 0x0198 }
            r1.f = r0     // Catch:{ Exception -> 0x0198 }
            goto L_0x0289
        */
        throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.util.e.c.run():void");
    }
}
