package com.shoujiduoduo.util;

import android.os.Environment;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/* compiled from: BuildProperties */
public class d {

    /* renamed from: a  reason: collision with root package name */
    private final Properties f1650a = new Properties();

    private d() throws IOException {
        this.f1650a.load(new FileInputStream(new File(Environment.getRootDirectory(), "build.prop")));
    }

    public String a(String str, String str2) {
        return this.f1650a.getProperty(str, str2);
    }

    public static d a() throws IOException {
        return new d();
    }
}
