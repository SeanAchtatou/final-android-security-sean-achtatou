package com.shoujiduoduo.util;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.RemoteViews;
import com.shoujiduoduo.a.c.g;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.activity.RingToneDuoduoActivity;
import java.io.File;
import org.apache.mina.proxy.handlers.http.ntlm.NTLMConstants;

/* compiled from: UpdateTask */
public class bd extends AsyncTask<Void, Integer, Boolean> implements g {
    public static boolean c = false;
    private static final String d = bd.class.getSimpleName();

    /* renamed from: a  reason: collision with root package name */
    public long f1624a = -1;
    public long b = 0;
    private Context e;
    private String f;
    private String g;
    private String h;
    private boolean i = false;
    private int j;
    private final int k = 1922;
    private final int l = 1923;
    private final int m = 1924;
    private NotificationManager n = null;
    private Notification o = null;
    private Notification p = null;
    private Notification q = null;

    public bd(Context context, String str) {
        a.a(d, "UpdateTask start!");
        this.e = context;
        c = false;
        this.f = str;
        this.n = (NotificationManager) this.e.getSystemService("notification");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.util.as.a(android.content.Context, java.lang.String, long):long
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.shoujiduoduo.util.as.a(android.content.Context, java.lang.String, int):int
      com.shoujiduoduo.util.as.a(android.content.Context, java.lang.String, java.lang.String):java.lang.String
      com.shoujiduoduo.util.as.a(android.content.Context, java.lang.String, long):long */
    private boolean c() {
        this.g = this.f.substring(this.f.lastIndexOf("/") + 1);
        this.g = this.g.toLowerCase();
        a.a(d, "mCacheName = " + this.g);
        if (!this.g.endsWith(".apk")) {
            return false;
        }
        this.h = s.b() + this.g;
        a.a(d, "download soft: cachePath = " + this.h);
        this.f1624a = as.a(this.e, this.g + ":total", -1L);
        this.b = as.a(this.e, this.g + ":current", 0L);
        a.a(d, "download soft: totalLength = " + this.f1624a + "; currentLength = " + this.b);
        this.i = true;
        if (this.f1624a > 0 && this.b >= 0) {
            if (this.b <= this.f1624a) {
                File file = new File(this.h);
                if (!file.exists() || !file.canWrite() || !file.isFile() || file.length() != this.b) {
                    this.b = 0;
                } else if (this.b == this.f1624a) {
                    this.i = false;
                }
            } else {
                this.b = 0;
                this.f1624a = -1;
            }
        }
        return true;
    }

    private void b(int i2) {
        if (!c) {
            Intent intent = new Intent(this.e, RingToneDuoduoActivity.class);
            intent.addFlags(NTLMConstants.FLAG_NEGOTIATE_128_BIT_ENCRYPTION);
            this.q = new Notification(R.drawable.icon_download, "铃声多多", System.currentTimeMillis());
            a.a(d, "package name: " + this.e.getPackageName());
            this.q.contentIntent = PendingIntent.getActivity(this.e, 0, intent, 0);
            this.q.contentView = new RemoteViews(this.e.getPackageName(), (int) R.layout.download_notif);
            this.q.contentView.setProgressBar(R.id.down_progress_bar, 100, i2, false);
            this.q.contentView.setTextViewText(R.id.down_tv, "正在下载铃声多多...");
            this.n.notify(1922, this.q);
        }
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        b(0);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Boolean doInBackground(Void... voidArr) {
        if (!c()) {
            return Boolean.FALSE;
        }
        if (this.i) {
            return Boolean.valueOf(w.a(this.f, this.h, this.b, this));
        }
        publishProgress(100, 100);
        return Boolean.TRUE;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(Boolean bool) {
        if (!c) {
            this.n.cancel(1922);
            if (!bool.booleanValue()) {
                Intent intent = new Intent(this.e, RingToneDuoduoActivity.class);
                intent.addFlags(NTLMConstants.FLAG_NEGOTIATE_128_BIT_ENCRYPTION);
                intent.putExtra("update_fail", "yes");
                PendingIntent activity = PendingIntent.getActivity(this.e, 0, intent, 0);
                this.o = new Notification(R.drawable.duoduo_icon, "下载失败", System.currentTimeMillis());
                this.o.flags |= 16;
                this.o.setLatestEventInfo(this.e, "铃声多多", "铃声多多升级失败，请稍后再试", activity);
                this.n.notify(1923, this.o);
                return;
            }
            f.b(this.h);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onProgressUpdate(Integer... numArr) {
        a.a(d, "onProgressUpdate: progress = " + numArr[1]);
        if (this.q != null) {
            this.q.contentView.setProgressBar(R.id.down_progress_bar, 100, numArr[1].intValue(), false);
            this.n.notify(1922, this.q);
        }
    }

    public void a(long j2) {
        this.b = j2;
        int i2 = (int) ((((float) this.b) * 100.0f) / ((float) this.f1624a));
        if (i2 != this.j) {
            this.j = i2;
            a.a(d, "onDownloadProgtess, downSize = " + j2 + ", totalSize = " + this.f1624a + "," + " progress = " + this.j);
            as.b(this.e, this.g + ":current", j2);
            publishProgress(100, Integer.valueOf(this.j));
        }
    }

    public void a(int i2) {
    }

    public void a() {
        a.a(d, "onDownloadFinish");
        as.b(this.e, this.g + ":current", this.f1624a);
        publishProgress(100, 100);
    }

    public void b(long j2) {
        a.a(d, "onDownloadStart, totalsize:" + j2);
        as.b(this.e, this.g + ":total", j2);
        if (this.f1624a != j2) {
            this.f1624a = j2;
            this.b = 0;
        }
    }

    public void b() {
    }
}
