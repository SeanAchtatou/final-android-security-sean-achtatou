package com.shoujiduoduo.util;

import android.app.Activity;
import android.text.TextUtils;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.c.b;
import com.umeng.socialize.media.g;
import com.umeng.socialize.media.p;

/* compiled from: UmengSocialUtils */
public class ax {
    private static ax c = null;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public String f1579a;
    /* access modifiers changed from: private */
    public RingData b;
    private UMShareListener d = new ba(this);
    private UMShareListener e = new bb(this);

    private ax() {
    }

    public static synchronized ax a() {
        ax axVar;
        synchronized (ax.class) {
            if (c == null) {
                c = new ax();
            }
            axVar = c;
        }
        return axVar;
    }

    public void a(Activity activity, String str, String str2, String str3, String str4) {
        new ShareAction(activity).setDisplayList(b.WEIXIN, b.WEIXIN_CIRCLE, b.QQ, b.QZONE).setShareboardclickCallback(new ay(this, activity, str, str2, str3, str4)).open();
    }

    public void a(Activity activity, RingData ringData, String str) {
        this.f1579a = str;
        this.b = ringData;
        new ShareAction(activity).setDisplayList(b.WEIXIN, b.WEIXIN_CIRCLE, b.QQ, b.QZONE, b.SINA).addButton("music_album", "music_album", "music_alblum_share_on", "music_alblum_share_off").setShareboardclickCallback(new az(this, ringData, activity)).open();
    }

    /* access modifiers changed from: private */
    public void a(Activity activity, String str, String str2, String str3, String str4, b bVar) {
        g gVar = new g(activity, str3);
        switch (bVar) {
            case WEIXIN:
            case WEIXIN_CIRCLE:
            case QQ:
            case QZONE:
                new ShareAction(activity).withTitle(str).withText(str2).withTargetUrl(str4).withMedia(gVar).setPlatform(bVar).setCallback(this.e).share();
                return;
            case SINA:
                new ShareAction(activity).withText(str2 + " 地址>>" + str4).withMedia(gVar).setPlatform(bVar).setCallback(this.e).share();
                return;
            default:
                a.c("UmengSocialUtils", "not support media");
                return;
        }
    }

    /* access modifiers changed from: private */
    public void a(Activity activity, RingData ringData, b bVar) {
        String str = "分享手机铃声   “" + ringData.e + "”  来自  @铃声多多 ，快来听听吧!";
        String c2 = com.umeng.analytics.b.c(RingDDApp.c(), "share_icon");
        if (TextUtils.isEmpty(c2)) {
            c2 = "http://cdnringhlt.shoujiduoduo.com/ringres/software/ring/ar/icon114.png";
        }
        String str2 = av.d(am.a().a("share_url")) + "ddsid=" + ringData.g + "&ddsrc=" + bVar.toString();
        a.a("UmengSocialUtils", "[shareRing] targeturl:" + str2);
        g gVar = new g(activity, c2);
        p pVar = new p(ringData.b());
        pVar.a(ringData.e);
        pVar.a(new g(activity, c2));
        pVar.b(str2);
        switch (bVar) {
            case WEIXIN:
            case WEIXIN_CIRCLE:
            case QQ:
            case QZONE:
                new ShareAction(activity).withTitle("铃声多多").withText(str).withTargetUrl(str2).withMedia(pVar).setPlatform(bVar).setCallback(this.d).share();
                return;
            case SINA:
                new ShareAction(activity).withText(str + " 试听地址>>" + str2).withMedia(gVar).setPlatform(bVar).setCallback(this.d).share();
                return;
            default:
                a.c("UmengSocialUtils", "not support media");
                return;
        }
    }

    public void a(Activity activity, b bVar) {
        UMShareAPI.get(activity).deleteOauth(activity, bVar, new bc(this));
    }
}
