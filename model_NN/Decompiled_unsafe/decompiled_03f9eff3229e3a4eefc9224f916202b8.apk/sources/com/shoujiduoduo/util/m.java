package com.shoujiduoduo.util;

import com.shoujiduoduo.ringtone.RingDDApp;
import com.umeng.analytics.b;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/* compiled from: DnsDetector */
public class m {
    /* access modifiers changed from: private */
    public static volatile String e = "cdnringhlt.shoujiduoduo.com";
    /* access modifiers changed from: private */
    public static volatile String i = "www.shoujiduoduo.com";
    private static final m o = new m();
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public String f1678a = "cdnringhlt.shoujiduoduo.com";
    /* access modifiers changed from: private */
    public String b = "";
    /* access modifiers changed from: private */
    public String c = "cdnringfw.shoujiduoduo.com";
    private String d = "";
    /* access modifiers changed from: private */
    public String f = "www.shoujiduoduo.com";
    private String g = "117.121.41.242";
    /* access modifiers changed from: private */
    public String h = "";
    /* access modifiers changed from: private */
    public int j = 8000;
    /* access modifiers changed from: private */
    public int k = 3;
    private final ReentrantReadWriteLock l = new ReentrantReadWriteLock();
    private final Lock m = this.l.readLock();
    /* access modifiers changed from: private */
    public final Lock n = this.l.writeLock();

    private m() {
    }

    public static m a() {
        return o;
    }

    public void b() {
        this.f1678a = am.a().a("dns_value1");
        this.b = am.a().a("dns_check1");
        this.c = am.a().a("dns_value2");
        this.d = am.a().a("dns_check2");
        this.h = am.a().a("dns_duoduo_check");
        this.j = u.a(am.a().a("dns_timeout"), 8000);
        this.k = u.a(am.a().a("dns_retry"), 3);
        com.shoujiduoduo.base.a.a.a("dnsdetector", "detect start");
        com.shoujiduoduo.base.a.a.a("dnsdetector", "online cdn1:" + this.f1678a);
        com.shoujiduoduo.base.a.a.a("dnsdetector", "online cdn2:" + this.c);
        com.shoujiduoduo.base.a.a.a("dnsdetector", "online timeout:" + this.j);
        com.shoujiduoduo.base.a.a.a("dnsdetector", "online retry:" + this.k);
        if (!NetworkStateUtil.a()) {
            com.shoujiduoduo.base.a.a.e("dnsdetector", "network is not available");
            return;
        }
        new Thread(new n(this)).start();
        String c2 = b.c(RingDDApp.c(), "duoduo_dns");
        if (av.c(c2)) {
            c2 = this.g;
        }
        com.shoujiduoduo.base.a.a.a("dnsdetector", "duoduo server ip:" + c2);
        new Thread(new o(this, c2)).start();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.util.w.a(java.lang.String, java.lang.String, boolean):boolean
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.shoujiduoduo.util.w.a(java.lang.String, java.lang.String, java.lang.String):void
      com.shoujiduoduo.util.w.a(int, int, int):byte[]
      com.shoujiduoduo.util.w.a(java.lang.String, boolean, java.lang.String):byte[]
      com.shoujiduoduo.util.w.a(java.lang.String, java.lang.String, boolean):boolean */
    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:134:0x024c  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00dc  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00e3  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00f5  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x0165 A[SYNTHETIC, Splitter:B:66:0x0165] */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x0177 A[SYNTHETIC, Splitter:B:75:0x0177] */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x0188 A[SYNTHETIC, Splitter:B:83:0x0188] */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x0191  */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x01a8  */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:63:0x0160=Splitter:B:63:0x0160, B:72:0x0172=Splitter:B:72:0x0172} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(java.lang.String r12, boolean r13) {
        /*
            r11 = this;
            r10 = 512(0x200, float:7.175E-43)
            r8 = 2
            r2 = 1
            r1 = 0
            com.shoujiduoduo.util.am r0 = com.shoujiduoduo.util.am.a()
            java.lang.String r3 = "dns_switchcondition"
            java.lang.String r6 = r0.a(r3)
            java.lang.String r0 = "none"
            boolean r0 = r0.equals(r6)
            if (r0 == 0) goto L_0x0018
        L_0x0017:
            return r1
        L_0x0018:
            java.lang.String r5 = ""
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r3 = com.shoujiduoduo.util.l.a(r8)
            java.lang.StringBuilder r0 = r0.append(r3)
            java.lang.String r3 = "verify.dat"
            java.lang.StringBuilder r0 = r0.append(r3)
            java.lang.String r0 = r0.toString()
            boolean r7 = com.shoujiduoduo.util.w.a(r12, r0, r2)
            java.lang.String r3 = "dnsdetector"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            if (r13 == 0) goto L_0x014f
            java.lang.String r0 = "CDN_check"
        L_0x0040:
            java.lang.StringBuilder r0 = r4.append(r0)
            java.lang.String r4 = " [isDownloadFileVerifyError] download file success:"
            java.lang.StringBuilder r0 = r0.append(r4)
            java.lang.StringBuilder r0 = r0.append(r7)
            java.lang.String r0 = r0.toString()
            com.shoujiduoduo.base.a.a.a(r3, r0)
            if (r7 == 0) goto L_0x024f
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r3 = com.shoujiduoduo.util.l.a(r8)
            java.lang.StringBuilder r0 = r0.append(r3)
            java.lang.String r3 = "verify.dat"
            java.lang.StringBuilder r0 = r0.append(r3)
            java.lang.String r0 = r0.toString()
            boolean r0 = com.shoujiduoduo.util.t.f(r0)
            if (r0 == 0) goto L_0x024f
            r4 = 0
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x015d, IOException -> 0x0170, all -> 0x0184 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x015d, IOException -> 0x0170, all -> 0x0184 }
            r0.<init>()     // Catch:{ FileNotFoundException -> 0x015d, IOException -> 0x0170, all -> 0x0184 }
            r8 = 2
            java.lang.String r8 = com.shoujiduoduo.util.l.a(r8)     // Catch:{ FileNotFoundException -> 0x015d, IOException -> 0x0170, all -> 0x0184 }
            java.lang.StringBuilder r0 = r0.append(r8)     // Catch:{ FileNotFoundException -> 0x015d, IOException -> 0x0170, all -> 0x0184 }
            java.lang.String r8 = "verify.dat"
            java.lang.StringBuilder r0 = r0.append(r8)     // Catch:{ FileNotFoundException -> 0x015d, IOException -> 0x0170, all -> 0x0184 }
            java.lang.String r0 = r0.toString()     // Catch:{ FileNotFoundException -> 0x015d, IOException -> 0x0170, all -> 0x0184 }
            r3.<init>(r0)     // Catch:{ FileNotFoundException -> 0x015d, IOException -> 0x0170, all -> 0x0184 }
            r0 = 256(0x100, float:3.59E-43)
            byte[] r0 = new byte[r0]     // Catch:{ FileNotFoundException -> 0x0245, IOException -> 0x023e }
            int r4 = r3.read(r0)     // Catch:{ FileNotFoundException -> 0x0245, IOException -> 0x023e }
            r8 = -1
            if (r4 == r8) goto L_0x0252
            java.lang.String r8 = new java.lang.String     // Catch:{ FileNotFoundException -> 0x0245, IOException -> 0x023e }
            r9 = 0
            r8.<init>(r0, r9, r4)     // Catch:{ FileNotFoundException -> 0x0245, IOException -> 0x023e }
            java.lang.String r4 = r8.trim()     // Catch:{ FileNotFoundException -> 0x0245, IOException -> 0x023e }
        L_0x00a7:
            java.lang.String r5 = "dnsdetector"
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x0249, IOException -> 0x0241 }
            r8.<init>()     // Catch:{ FileNotFoundException -> 0x0249, IOException -> 0x0241 }
            if (r13 == 0) goto L_0x0153
            java.lang.String r0 = "CDN_check"
        L_0x00b2:
            java.lang.StringBuilder r0 = r8.append(r0)     // Catch:{ FileNotFoundException -> 0x0249, IOException -> 0x0241 }
            java.lang.String r8 = " verify file content:"
            java.lang.StringBuilder r0 = r0.append(r8)     // Catch:{ FileNotFoundException -> 0x0249, IOException -> 0x0241 }
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ FileNotFoundException -> 0x0249, IOException -> 0x0241 }
            java.lang.String r0 = r0.toString()     // Catch:{ FileNotFoundException -> 0x0249, IOException -> 0x0241 }
            com.shoujiduoduo.base.a.a.a(r5, r0)     // Catch:{ FileNotFoundException -> 0x0249, IOException -> 0x0241 }
            if (r3 == 0) goto L_0x00cc
            r3.close()     // Catch:{ IOException -> 0x0157 }
        L_0x00cc:
            if (r7 == 0) goto L_0x00e8
            java.lang.String r0 = "ok"
            boolean r0 = r0.equalsIgnoreCase(r4)
            if (r0 != 0) goto L_0x00e8
            int r0 = r4.length()
            if (r0 <= r10) goto L_0x024c
            java.lang.String r0 = r4.substring(r1, r10)
            r3 = r0
        L_0x00e1:
            if (r13 == 0) goto L_0x0191
            java.lang.String r0 = "cdn_file_check"
        L_0x00e5:
            com.shoujiduoduo.util.w.b(r0, r3)
        L_0x00e8:
            java.util.HashMap r3 = new java.util.HashMap
            r3.<init>()
            java.lang.String r0 = "network"
            boolean r0 = r6.equals(r0)
            if (r0 == 0) goto L_0x01a8
            if (r7 == 0) goto L_0x00ff
            java.lang.String r0 = "ok"
            boolean r0 = r0.equalsIgnoreCase(r4)
            if (r0 != 0) goto L_0x0195
        L_0x00ff:
            r0 = r2
        L_0x0100:
            java.lang.String r2 = "dnsdetector"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            if (r13 == 0) goto L_0x0198
            java.lang.String r1 = "CDN_check"
        L_0x010b:
            java.lang.StringBuilder r1 = r5.append(r1)
            java.lang.String r5 = " [isDownloadFileVerifyError] 下载失败或者内容错误都认为verify 失败, bres:"
            java.lang.StringBuilder r1 = r1.append(r5)
            java.lang.StringBuilder r1 = r1.append(r0)
            java.lang.String r1 = r1.toString()
            com.shoujiduoduo.base.a.a.a(r2, r1)
            java.lang.String r2 = "downSuc"
            if (r7 == 0) goto L_0x019c
            java.lang.String r1 = "true"
        L_0x0126:
            r3.put(r2, r1)
            java.lang.String r2 = "contentSuc"
            java.lang.String r1 = "ok"
            boolean r1 = r1.equalsIgnoreCase(r4)
            if (r1 == 0) goto L_0x019f
            java.lang.String r1 = "true"
        L_0x0135:
            r3.put(r2, r1)
            java.lang.String r2 = "res"
            if (r0 == 0) goto L_0x01a2
            java.lang.String r1 = "true"
        L_0x013e:
            r3.put(r2, r1)
            android.content.Context r2 = com.shoujiduoduo.ringtone.RingDDApp.c()
            if (r13 == 0) goto L_0x01a5
            java.lang.String r1 = "cdn_file_check_by_network"
        L_0x0149:
            com.umeng.analytics.b.a(r2, r1, r3)
            r1 = r0
            goto L_0x0017
        L_0x014f:
            java.lang.String r0 = "duoduo_check"
            goto L_0x0040
        L_0x0153:
            java.lang.String r0 = "duoduo_check"
            goto L_0x00b2
        L_0x0157:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00cc
        L_0x015d:
            r0 = move-exception
            r3 = r4
            r4 = r5
        L_0x0160:
            r0.printStackTrace()     // Catch:{ all -> 0x023b }
            if (r3 == 0) goto L_0x00cc
            r3.close()     // Catch:{ IOException -> 0x016a }
            goto L_0x00cc
        L_0x016a:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00cc
        L_0x0170:
            r0 = move-exception
            r3 = r4
        L_0x0172:
            r0.printStackTrace()     // Catch:{ all -> 0x023b }
            if (r3 == 0) goto L_0x024f
            r3.close()     // Catch:{ IOException -> 0x017d }
            r4 = r5
            goto L_0x00cc
        L_0x017d:
            r0 = move-exception
            r0.printStackTrace()
            r4 = r5
            goto L_0x00cc
        L_0x0184:
            r0 = move-exception
            r3 = r4
        L_0x0186:
            if (r3 == 0) goto L_0x018b
            r3.close()     // Catch:{ IOException -> 0x018c }
        L_0x018b:
            throw r0
        L_0x018c:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x018b
        L_0x0191:
            java.lang.String r0 = "duoduo_file_check"
            goto L_0x00e5
        L_0x0195:
            r0 = r1
            goto L_0x0100
        L_0x0198:
            java.lang.String r1 = "duoduo_check"
            goto L_0x010b
        L_0x019c:
            java.lang.String r1 = "false"
            goto L_0x0126
        L_0x019f:
            java.lang.String r1 = "false"
            goto L_0x0135
        L_0x01a2:
            java.lang.String r1 = "false"
            goto L_0x013e
        L_0x01a5:
            java.lang.String r1 = "duoduo_file_check_by_network"
            goto L_0x0149
        L_0x01a8:
            java.lang.String r0 = "content"
            boolean r0 = r6.equals(r0)
            if (r0 == 0) goto L_0x021a
            if (r7 == 0) goto L_0x0209
            java.lang.String r0 = "ok"
            boolean r0 = r0.equalsIgnoreCase(r4)
            if (r0 != 0) goto L_0x0209
        L_0x01ba:
            java.lang.String r1 = "dnsdetector"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            if (r13 == 0) goto L_0x020b
            java.lang.String r0 = "CDN_check"
        L_0x01c5:
            java.lang.StringBuilder r0 = r5.append(r0)
            java.lang.String r5 = " [isDownloadFileVerifyError] 下载成功，内容错误才认为verify 失败, bres:"
            java.lang.StringBuilder r0 = r0.append(r5)
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
            com.shoujiduoduo.base.a.a.a(r1, r0)
            java.lang.String r1 = "downSuc"
            if (r7 == 0) goto L_0x020e
            java.lang.String r0 = "true"
        L_0x01e0:
            r3.put(r1, r0)
            java.lang.String r1 = "contentSuc"
            java.lang.String r0 = "ok"
            boolean r0 = r0.equalsIgnoreCase(r4)
            if (r0 == 0) goto L_0x0211
            java.lang.String r0 = "true"
        L_0x01ef:
            r3.put(r1, r0)
            java.lang.String r1 = "res"
            if (r2 == 0) goto L_0x0214
            java.lang.String r0 = "true"
        L_0x01f8:
            r3.put(r1, r0)
            android.content.Context r1 = com.shoujiduoduo.ringtone.RingDDApp.c()
            if (r13 == 0) goto L_0x0217
            java.lang.String r0 = "cdn_file_check_by_content"
        L_0x0203:
            com.umeng.analytics.b.a(r1, r0, r3)
            r1 = r2
            goto L_0x0017
        L_0x0209:
            r2 = r1
            goto L_0x01ba
        L_0x020b:
            java.lang.String r0 = "duoduo_check"
            goto L_0x01c5
        L_0x020e:
            java.lang.String r0 = "false"
            goto L_0x01e0
        L_0x0211:
            java.lang.String r0 = "false"
            goto L_0x01ef
        L_0x0214:
            java.lang.String r0 = "false"
            goto L_0x01f8
        L_0x0217:
            java.lang.String r0 = "duoduo_file_check_by_content"
            goto L_0x0203
        L_0x021a:
            java.lang.String r2 = "dnsdetector"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            if (r13 == 0) goto L_0x0238
            java.lang.String r0 = "CDN_check"
        L_0x0225:
            java.lang.StringBuilder r0 = r3.append(r0)
            java.lang.String r3 = " [isDownloadFileVerifyError] , 默认返回false"
            java.lang.StringBuilder r0 = r0.append(r3)
            java.lang.String r0 = r0.toString()
            com.shoujiduoduo.base.a.a.a(r2, r0)
            goto L_0x0017
        L_0x0238:
            java.lang.String r0 = "duoduo_check"
            goto L_0x0225
        L_0x023b:
            r0 = move-exception
            goto L_0x0186
        L_0x023e:
            r0 = move-exception
            goto L_0x0172
        L_0x0241:
            r0 = move-exception
            r5 = r4
            goto L_0x0172
        L_0x0245:
            r0 = move-exception
            r4 = r5
            goto L_0x0160
        L_0x0249:
            r0 = move-exception
            goto L_0x0160
        L_0x024c:
            r3 = r4
            goto L_0x00e1
        L_0x024f:
            r4 = r5
            goto L_0x00cc
        L_0x0252:
            r4 = r5
            goto L_0x00a7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.util.m.a(java.lang.String, boolean):boolean");
    }

    public String c() {
        this.m.lock();
        try {
            return e;
        } finally {
            this.m.unlock();
        }
    }

    public String d() {
        this.m.lock();
        try {
            return i;
        } finally {
            this.m.unlock();
        }
    }

    /* compiled from: DnsDetector */
    public class a extends Thread {
        private InetAddress b;
        private String c;

        public a(String str) {
            this.c = str;
        }

        public void run() {
            try {
                a(InetAddress.getByName(this.c));
            } catch (UnknownHostException e) {
            }
        }

        private synchronized void a(InetAddress inetAddress) {
            this.b = inetAddress;
        }

        public synchronized String a() {
            String str;
            if (this.b != null) {
                str = this.b.getHostAddress();
            } else {
                str = null;
            }
            return str;
        }
    }
}
