package com.shoujiduoduo.util;

/* compiled from: KwThreadPool */
public final class ae {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static volatile int f1557a = 0;
    /* access modifiers changed from: private */
    public static b[] b = new b[5];

    /* compiled from: KwThreadPool */
    public enum a {
        NORMAL,
        NET,
        IMMEDIATELY
    }

    static /* synthetic */ int c() {
        int i = f1557a + 1;
        f1557a = i;
        return i;
    }

    public static void a(a aVar, Runnable runnable) {
        if (aVar == a.NET) {
        }
        d().a(runnable, 0);
    }

    private static b d() {
        if (f1557a == 0) {
            return new b();
        }
        synchronized (b) {
            if (f1557a == 0) {
                b bVar = new b();
                return bVar;
            }
            f1557a--;
            b bVar2 = b[f1557a];
            b[f1557a] = null;
            return bVar2;
        }
    }

    /* compiled from: KwThreadPool */
    private static final class b extends Thread {

        /* renamed from: a  reason: collision with root package name */
        private volatile Runnable f1559a;
        private volatile int b;
        private volatile boolean c;

        private b() {
        }

        public void a(Runnable runnable, int i) {
            this.f1559a = runnable;
            this.b = i;
            if (!this.c) {
                start();
                this.c = true;
                return;
            }
            synchronized (this) {
                notify();
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
            wait();
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r4 = this;
                r3 = 5
            L_0x0001:
                int r0 = r4.b
                android.os.Process.setThreadPriority(r0)
                java.lang.Runnable r0 = r4.f1559a
                r0.run()
                int r0 = com.shoujiduoduo.util.ae.f1557a
                if (r0 < r3) goto L_0x0015
            L_0x0011:
                r0 = 0
                r4.c = r0
                return
            L_0x0015:
                monitor-enter(r4)
                com.shoujiduoduo.util.ae$b[] r1 = com.shoujiduoduo.util.ae.b     // Catch:{ all -> 0x0024 }
                monitor-enter(r1)     // Catch:{ all -> 0x0024 }
                int r0 = com.shoujiduoduo.util.ae.f1557a     // Catch:{ all -> 0x003a }
                if (r0 < r3) goto L_0x0027
                monitor-exit(r1)     // Catch:{ all -> 0x003a }
                monitor-exit(r4)     // Catch:{ all -> 0x0024 }
                goto L_0x0011
            L_0x0024:
                r0 = move-exception
                monitor-exit(r4)     // Catch:{ all -> 0x0024 }
                throw r0
            L_0x0027:
                com.shoujiduoduo.util.ae$b[] r0 = com.shoujiduoduo.util.ae.b     // Catch:{ all -> 0x003a }
                int r2 = com.shoujiduoduo.util.ae.f1557a     // Catch:{ all -> 0x003a }
                r0[r2] = r4     // Catch:{ all -> 0x003a }
                com.shoujiduoduo.util.ae.c()     // Catch:{ all -> 0x003a }
                monitor-exit(r1)     // Catch:{ all -> 0x003a }
                r4.wait()     // Catch:{ InterruptedException -> 0x003d }
                monitor-exit(r4)     // Catch:{ all -> 0x0024 }
                goto L_0x0001
            L_0x003a:
                r0 = move-exception
                monitor-exit(r1)     // Catch:{ all -> 0x003a }
                throw r0     // Catch:{ all -> 0x0024 }
            L_0x003d:
                r0 = move-exception
                monitor-exit(r4)     // Catch:{ all -> 0x0024 }
                goto L_0x0011
            */
            throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.util.ae.b.run():void");
        }
    }
}
