package com.shoujiduoduo.util.widget;

import android.view.View;
import com.shoujiduoduo.util.widget.a;

/* compiled from: DuoduoAlertDialog */
class b implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f1705a;
    final /* synthetic */ a.C0034a b;

    b(a.C0034a aVar, a aVar2) {
        this.b = aVar;
        this.f1705a = aVar2;
    }

    public void onClick(View view) {
        if (this.b.i != null) {
            this.b.i.onClick(this.f1705a, -1);
        } else {
            this.f1705a.dismiss();
        }
    }
}
