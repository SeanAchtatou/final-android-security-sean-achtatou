package com.shoujiduoduo.util.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.ringtone.R;

public class IndexListView extends ListView {

    /* renamed from: a  reason: collision with root package name */
    private boolean f1697a = true;
    private e b = null;
    private int c;
    private int d;
    private int e;
    private int f;
    private int g;

    public IndexListView(Context context) {
        super(context);
    }

    public IndexListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context, attributeSet);
    }

    public IndexListView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        a(context, attributeSet);
    }

    private void a(Context context, AttributeSet attributeSet) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.IndexListView);
        this.c = obtainStyledAttributes.getColor(0, -7829368);
        this.d = obtainStyledAttributes.getColor(1, -1);
        this.e = obtainStyledAttributes.getColor(2, -16759672);
        this.f = obtainStyledAttributes.getColor(3, 0);
        this.g = obtainStyledAttributes.getColor(4, ViewCompat.MEASURED_STATE_MASK);
        obtainStyledAttributes.recycle();
    }

    public boolean isFastScrollEnabled() {
        return this.f1697a;
    }

    public void setFastScrollEnabled(boolean z) {
        this.f1697a = z;
        if (this.f1697a) {
            if (this.b == null) {
                this.b = new e(getContext(), this);
                this.b.a(this.c, this.d, this.e, this.f, this.g);
                this.b.a();
            }
        } else if (this.b != null) {
            this.b.b();
            this.b = null;
        }
    }

    public void draw(Canvas canvas) {
        try {
            super.draw(canvas);
            if (this.b != null) {
                this.b.a(canvas);
            }
        } catch (Exception e2) {
            a.a(e2);
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.b == null || !this.b.a(motionEvent)) {
            return super.onTouchEvent(motionEvent);
        }
        return true;
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        return super.onInterceptTouchEvent(motionEvent);
    }

    public void setAdapter(ListAdapter listAdapter) {
        super.setAdapter(listAdapter);
        if (this.b != null) {
            this.b.a(listAdapter);
        }
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        if (this.b != null) {
            this.b.a(i, i2, i3, i4);
        }
    }
}
