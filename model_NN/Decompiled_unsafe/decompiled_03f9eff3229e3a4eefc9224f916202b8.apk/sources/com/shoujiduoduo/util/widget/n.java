package com.shoujiduoduo.util.widget;

import android.graphics.Bitmap;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.util.g;
import com.shoujiduoduo.util.q;

/* compiled from: WebViewActivity */
class n extends WebViewClient {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ WebViewActivity f1716a;

    n(WebViewActivity webViewActivity) {
        this.f1716a = webViewActivity;
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        a.a(WebViewActivity.f1701a, "jump url:" + str);
        if (str.endsWith("apk")) {
            q.a(this.f1716a).a(str, this.f1716a.l == null ? "" : this.f1716a.l);
            return true;
        } else if (g.a(str)) {
            g.a(this.f1716a, str);
            return true;
        } else {
            webView.loadUrl(str);
            return true;
        }
    }

    public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        super.onPageStarted(webView, str, bitmap);
    }

    public void onPageFinished(WebView webView, String str) {
        super.onPageFinished(webView, str);
        a.a(WebViewActivity.f1701a, "onPageFinished. goback = " + webView.canGoBack() + ", forward = " + webView.canGoForward());
        this.f1716a.d.setEnabled(webView.canGoBack());
        this.f1716a.e.setEnabled(webView.canGoForward());
    }

    public void onReceivedError(WebView webView, int i, String str, String str2) {
        super.onReceivedError(webView, i, str, str2);
    }
}
