package com.shoujiduoduo.util.widget;

import android.widget.Toast;
import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.ringtone.RingDDApp;

/* compiled from: KwToast */
public final class f {

    /* renamed from: a  reason: collision with root package name */
    private static Toast f1709a = null;

    public static void a(String str, int i) {
        x.a().a(new g(str, i));
    }

    public static void a(String str) {
        x.a().a(new h(str));
    }

    /* access modifiers changed from: private */
    public static void c(String str, int i) {
        if (f1709a != null) {
            f1709a.setText(str);
            f1709a.setDuration(i);
            f1709a.show();
            return;
        }
        f1709a = Toast.makeText(RingDDApp.b().getApplicationContext(), str, i);
        f1709a.show();
    }

    public static void a(int i, int i2) {
        x.a().a(new i(i, i2));
    }

    public static void a(int i) {
        x.a().a(new j(i));
    }

    /* access modifiers changed from: private */
    public static void c(int i, int i2) {
        if (f1709a != null) {
            f1709a.setText(i);
            f1709a.setDuration(i2);
            f1709a.show();
            return;
        }
        f1709a = Toast.makeText(RingDDApp.b().getApplicationContext(), i, i2);
        f1709a.show();
    }
}
