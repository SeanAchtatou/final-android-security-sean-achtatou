package com.shoujiduoduo.util.widget;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.shoujiduoduo.ringtone.R;

/* compiled from: DuoduoAlertDialog */
public class a extends Dialog {
    public a(Context context, int i) {
        super(context, i);
    }

    /* renamed from: com.shoujiduoduo.util.widget.a$a  reason: collision with other inner class name */
    /* compiled from: DuoduoAlertDialog */
    public static class C0034a {

        /* renamed from: a  reason: collision with root package name */
        private Context f1704a;
        private String b;
        private String c;
        private String d;
        private String e;
        private View f;
        private View g;
        private boolean h;
        /* access modifiers changed from: private */
        public DialogInterface.OnClickListener i;
        /* access modifiers changed from: private */
        public DialogInterface.OnClickListener j;

        public C0034a(Context context) {
            this.f1704a = context;
        }

        public C0034a a(String str) {
            this.c = str;
            return this;
        }

        public C0034a a(int i2) {
            this.c = (String) this.f1704a.getText(i2);
            return this;
        }

        public C0034a b(int i2) {
            this.b = (String) this.f1704a.getText(i2);
            return this;
        }

        public C0034a b(String str) {
            this.b = str;
            return this;
        }

        public C0034a a(View view) {
            this.f = view;
            return this;
        }

        public C0034a a(int i2, DialogInterface.OnClickListener onClickListener) {
            this.d = (String) this.f1704a.getText(i2);
            this.i = onClickListener;
            return this;
        }

        public C0034a a(String str, DialogInterface.OnClickListener onClickListener) {
            this.d = str;
            this.i = onClickListener;
            return this;
        }

        public C0034a b(int i2, DialogInterface.OnClickListener onClickListener) {
            this.e = (String) this.f1704a.getText(i2);
            this.j = onClickListener;
            return this;
        }

        public C0034a b(String str, DialogInterface.OnClickListener onClickListener) {
            this.e = str;
            this.j = onClickListener;
            return this;
        }

        public a a() {
            LayoutInflater layoutInflater = (LayoutInflater) this.f1704a.getSystemService("layout_inflater");
            a aVar = new a(this.f1704a, R.style.Dialog);
            if (this.g != null) {
                aVar.setContentView(this.g);
            } else {
                a(layoutInflater, aVar);
            }
            return aVar;
        }

        private void a(LayoutInflater layoutInflater, a aVar) {
            View inflate = layoutInflater.inflate((int) R.layout.custom_dialog, (ViewGroup) null);
            aVar.addContentView(inflate, new ViewGroup.LayoutParams(-1, -2));
            if (!TextUtils.isEmpty(this.b)) {
                ((TextView) inflate.findViewById(R.id.title)).setText(this.b);
            } else {
                inflate.findViewById(R.id.top_title_layout).setVisibility(8);
            }
            if (this.d != null) {
                ((Button) inflate.findViewById(R.id.positiveButton)).setText(this.d);
                ((Button) inflate.findViewById(R.id.positiveButton)).setOnClickListener(new b(this, aVar));
            } else {
                inflate.findViewById(R.id.positiveButton).setVisibility(8);
            }
            if (this.e != null) {
                ((Button) inflate.findViewById(R.id.negativeButton)).setText(this.e);
                ((Button) inflate.findViewById(R.id.negativeButton)).setOnClickListener(new c(this, aVar));
            } else {
                inflate.findViewById(R.id.negativeButton).setVisibility(8);
            }
            if (this.e == null && this.d == null) {
                inflate.findViewById(R.id.confirm_cancel_layout).setVisibility(8);
            }
            if (this.h) {
                Button button = (Button) inflate.findViewById(R.id.close);
                button.setVisibility(0);
                button.setOnClickListener(new d(this, aVar));
            } else {
                inflate.findViewById(R.id.close).setVisibility(8);
            }
            if (this.c != null) {
                ((TextView) inflate.findViewById(R.id.message)).setText(this.c);
            } else if (this.f != null) {
                ((LinearLayout) inflate.findViewById(R.id.content)).removeAllViews();
                ((LinearLayout) inflate.findViewById(R.id.content)).addView(this.f, new ViewGroup.LayoutParams(-1, -2));
            }
            aVar.setContentView(inflate);
        }
    }
}
