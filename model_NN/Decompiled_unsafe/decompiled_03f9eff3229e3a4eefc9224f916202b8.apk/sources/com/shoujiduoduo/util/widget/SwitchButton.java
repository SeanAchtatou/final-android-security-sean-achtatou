package com.shoujiduoduo.util.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.support.v4.view.MotionEventCompat;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import com.shoujiduoduo.ringtone.R;

public class SwitchButton extends View implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private static final String f1698a = SwitchButton.class.getSimpleName();
    private Bitmap b;
    private Bitmap c;
    private Bitmap d;
    private Bitmap e;
    private float f;
    private boolean g;
    private int h;
    private float i;
    private Rect j;
    private Rect k;
    private int l;
    private Paint m;
    private a n;
    private boolean o;

    public interface a {
        void a(SwitchButton switchButton, boolean z);
    }

    public SwitchButton(Context context) {
        this(context, null);
    }

    public SwitchButton(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public SwitchButton(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.f = 0.0f;
        this.g = true;
        this.i = 0.0f;
        this.j = null;
        this.k = null;
        this.l = 0;
        this.m = null;
        this.n = null;
        this.o = false;
        a(false);
    }

    public void a(boolean z) {
        this.g = z;
        this.b = BitmapFactory.decodeResource(getResources(), R.drawable.switch_bottom);
        this.c = BitmapFactory.decodeResource(getResources(), R.drawable.switch_btn_pressed);
        this.d = BitmapFactory.decodeResource(getResources(), R.drawable.switch_frame);
        this.e = BitmapFactory.decodeResource(getResources(), R.drawable.switch_mask);
        setOnClickListener(this);
        setOnTouchListener(new l(this));
        this.h = this.b.getWidth() - this.d.getWidth();
        this.j = new Rect(0, 0, this.d.getWidth() / 2, this.d.getHeight() / 2);
        com.shoujiduoduo.base.a.a.a(f1698a, "width:" + getWidth() + " height:" + getHeight());
        this.k = new Rect();
        this.m = new Paint();
        this.m.setAntiAlias(true);
        this.m.setAlpha(MotionEventCompat.ACTION_MASK);
        this.m.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.l > 0 || (this.l == 0 && this.g)) {
            if (this.k != null) {
                this.k.set(this.h - this.l, 0, this.b.getWidth() - this.l, this.d.getHeight());
            }
        } else if ((this.l < 0 || (this.l == 0 && !this.g)) && this.k != null) {
            this.k.set(-this.l, 0, this.d.getWidth() - this.l, this.d.getHeight());
        }
        this.j.set(0, 0, getWidth(), getHeight());
        com.shoujiduoduo.base.a.a.a(f1698a, "ondraw: width:" + getWidth() + " height:" + getHeight());
        com.shoujiduoduo.base.a.a.a(f1698a, "ondraw: width:" + getMeasuredWidth() + " height:" + getMeasuredHeight());
        int saveLayer = canvas.saveLayer(new RectF(this.j), null, 31);
        canvas.drawBitmap(this.b, this.k, this.j, (Paint) null);
        canvas.drawBitmap(this.c, this.k, this.j, (Paint) null);
        canvas.drawBitmap(this.d, (Rect) null, this.j, (Paint) null);
        canvas.drawBitmap(this.e, (Rect) null, this.j, this.m);
        canvas.restoreToCount(saveLayer);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public boolean onTouchEvent(MotionEvent motionEvent) {
        boolean z;
        switch (motionEvent.getAction()) {
            case 0:
                this.i = motionEvent.getX();
                com.shoujiduoduo.base.a.a.a(f1698a, "ACTION_DOWN, mLastX = " + this.i);
                break;
            case 1:
            case 3:
                com.shoujiduoduo.base.a.a.a(f1698a, "ACTION_UP, mDeltX = " + this.l);
                if (Math.abs(this.l) >= 0 && Math.abs(this.l) < 6) {
                    com.shoujiduoduo.base.a.a.a(f1698a, "move too little, click");
                    onClick(this);
                    return true;
                } else if (Math.abs(this.l) >= 6 && Math.abs(this.l) <= this.h / 4) {
                    com.shoujiduoduo.base.a.a.a(f1698a, "move shorter than half, don't change.");
                    this.l = 0;
                    invalidate();
                    return true;
                } else if (Math.abs(this.l) <= this.h / 4 || Math.abs(this.l) > this.h) {
                    com.shoujiduoduo.base.a.a.a(f1698a, "Strange things happened! don't move at all.");
                    return super.onTouchEvent(motionEvent);
                } else {
                    com.shoujiduoduo.base.a.a.a(f1698a, "move longer than half, change.");
                    this.l = this.l > 0 ? this.h : -this.h;
                    if (!this.g) {
                        z = true;
                    } else {
                        z = false;
                    }
                    this.g = z;
                    if (this.n != null) {
                        this.n.a(this, this.g);
                    }
                    invalidate();
                    this.l = 0;
                    return true;
                }
            case 2:
                this.f = motionEvent.getX();
                this.l = (int) (this.f - this.i);
                com.shoujiduoduo.base.a.a.a(f1698a, "ACTION_MOVE, mDeltX = " + this.l);
                if (Math.abs(this.l) > this.h) {
                    this.l = this.l > 0 ? this.h : -this.h;
                }
                invalidate();
                return true;
        }
        invalidate();
        return super.onTouchEvent(motionEvent);
    }

    public void setOnChangeListener(a aVar) {
        this.n = aVar;
    }

    public void setSwitchStatus(boolean z) {
        this.g = z;
        invalidate();
        this.l = 0;
    }

    public void onClick(View view) {
        boolean z;
        com.shoujiduoduo.base.a.a.a(f1698a, "onClick");
        this.l = this.g ? this.h : -this.h;
        if (!this.g) {
            z = true;
        } else {
            z = false;
        }
        this.g = z;
        if (this.n != null) {
            this.n.a(this, this.g);
        }
        invalidate();
        this.l = 0;
    }
}
