package com.shoujiduoduo.util.widget;

import android.view.MotionEvent;
import android.view.View;

/* compiled from: WebViewActivity */
class m implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ WebViewActivity f1715a;

    m(WebViewActivity webViewActivity) {
        this.f1715a = webViewActivity;
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
            case 1:
                if (view.hasFocus()) {
                    return false;
                }
                view.requestFocus();
                return false;
            default:
                return false;
        }
    }
}
