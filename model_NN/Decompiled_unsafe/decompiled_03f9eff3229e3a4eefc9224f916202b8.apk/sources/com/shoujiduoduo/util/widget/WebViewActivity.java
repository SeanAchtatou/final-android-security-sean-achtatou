package com.shoujiduoduo.util.widget;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.media.TransportMediator;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import cn.banshenggua.aichang.utils.Constants;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ui.utils.BaseActivity;
import com.shoujiduoduo.util.av;
import java.io.File;

public class WebViewActivity extends BaseActivity implements View.OnClickListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final String f1701a = WebViewActivity.class.getSimpleName();
    private final Activity b = this;
    private ImageButton c;
    /* access modifiers changed from: private */
    public ImageButton d;
    /* access modifiers changed from: private */
    public ImageButton e;
    private ImageButton f;
    /* access modifiers changed from: private */
    public ProgressBar g;
    /* access modifiers changed from: private */
    public ValueCallback<Uri> h;
    /* access modifiers changed from: private */
    public ValueCallback<Uri[]> i;
    private WebView j;
    private TextView k;
    /* access modifiers changed from: private */
    public String l;
    private String m;
    private WebViewClient n = new n(this);

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.webview_activity);
        this.c = (ImageButton) findViewById(R.id.btn_exit_web_activity);
        this.c.setOnClickListener(this);
        this.d = (ImageButton) findViewById(R.id.web_back);
        this.d.setEnabled(false);
        this.d.setOnClickListener(this);
        this.e = (ImageButton) findViewById(R.id.web_forward);
        this.e.setEnabled(false);
        this.e.setOnClickListener(this);
        this.f = (ImageButton) findViewById(R.id.web_refresh);
        this.f.setOnClickListener(this);
        this.g = (ProgressBar) findViewById(R.id.progressWebLoading);
        this.k = (TextView) findViewById(R.id.textWebActivityTitle);
        this.g.setVisibility(8);
        this.j = (WebView) findViewById(R.id.webview_window);
        Intent intent = getIntent();
        if (intent != null) {
            String stringExtra = intent.getStringExtra("url");
            this.m = intent.getStringExtra("title");
            a.a(f1701a, "url:" + stringExtra);
            this.l = intent.getStringExtra("apkname");
            if (!av.c(this.m)) {
                this.k.setText(this.m);
            }
            WebSettings settings = this.j.getSettings();
            settings.setJavaScriptEnabled(true);
            settings.setUseWideViewPort(true);
            settings.setLoadWithOverviewMode(true);
            settings.setBuiltInZoomControls(false);
            settings.setDomStorageEnabled(true);
            settings.setSupportZoom(false);
            this.j.requestFocus(TransportMediator.KEYCODE_MEDIA_RECORD);
            this.j.setOnTouchListener(new m(this));
            this.j.setWebChromeClient(new WebChromeClient() {
                public void onProgressChanged(WebView webView, int i) {
                    if (i < 100) {
                        WebViewActivity.this.g.setVisibility(0);
                        WebViewActivity.this.g.setProgress(i);
                    }
                    if (i == 100) {
                        WebViewActivity.this.g.setProgress(0);
                        WebViewActivity.this.g.setVisibility(8);
                    }
                }

                public void onReceivedTitle(WebView webView, String str) {
                }
            });
            this.j.setWebViewClient(this.n);
            this.j.setWebChromeClient(new WebChromeClient() {
                public void openFileChooser(ValueCallback<Uri> valueCallback) {
                    customOpenFileChooser(valueCallback);
                }

                /* access modifiers changed from: protected */
                public void openFileChooser(ValueCallback valueCallback, String str) {
                    customOpenFileChooser(valueCallback);
                }

                /* access modifiers changed from: protected */
                public void openFileChooser(ValueCallback<Uri> valueCallback, String str, String str2) {
                    customOpenFileChooser(valueCallback);
                }

                public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> valueCallback, WebChromeClient.FileChooserParams fileChooserParams) {
                    a.a("musicalbum", "onShowFileChooser");
                    ValueCallback unused = WebViewActivity.this.i = valueCallback;
                    Intent intent = new Intent("android.intent.action.GET_CONTENT");
                    intent.addCategory("android.intent.category.OPENABLE");
                    intent.setType("image/*");
                    WebViewActivity.this.startActivityForResult(Intent.createChooser(intent, "选择文件"), Constants.REGISTER_OK);
                    return true;
                }

                /* access modifiers changed from: protected */
                public void customOpenFileChooser(ValueCallback<Uri> valueCallback) {
                    a.a("musicalbum", "customOpenFileChooser");
                    ValueCallback unused = WebViewActivity.this.h = valueCallback;
                    Intent intent = new Intent("android.intent.action.GET_CONTENT");
                    intent.addCategory("android.intent.category.OPENABLE");
                    intent.setType("image/*");
                    WebViewActivity.this.startActivityForResult(Intent.createChooser(intent, "选择文件"), Constants.REGISTER_OK);
                }
            });
            if (Build.VERSION.SDK_INT >= 17) {
                this.j.getSettings().setMediaPlaybackRequiresUserGesture(false);
            }
            this.j.loadUrl(stringExtra);
            return;
        }
        a.e(f1701a, "intent is null");
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (i2 == 102) {
            a(i3, intent);
        }
    }

    private void a(int i2, Intent intent) {
        String str;
        if (Build.VERSION.SDK_INT < 21) {
            Uri data = (intent == null || i2 != -1) ? null : intent.getData();
            if ("smartisan".equals(Build.BRAND)) {
                try {
                    Cursor managedQuery = managedQuery(data, new String[]{"_data"}, null, null, null);
                    int columnIndexOrThrow = managedQuery.getColumnIndexOrThrow("_data");
                    managedQuery.moveToFirst();
                    str = managedQuery.getString(columnIndexOrThrow);
                } catch (Exception e2) {
                    str = null;
                }
                if (!TextUtils.isEmpty(str)) {
                    try {
                        data = Uri.fromFile(new File(str));
                    } catch (Exception e3) {
                    }
                }
            }
            if (this.h != null) {
                this.h.onReceiveValue(data);
            } else {
                a.c(f1701a, "mUploadMsgOld is null");
            }
        } else if (this.i != null) {
            this.i.onReceiveValue(WebChromeClient.FileChooserParams.parseResult(i2, intent));
        } else {
            a.c(f1701a, "mUploadMsg is null");
        }
        this.i = null;
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4) {
            return super.onKeyDown(i2, keyEvent);
        }
        a.a(f1701a, "keycode back");
        if (this.j == null || !this.j.canGoBack()) {
            finish();
        } else {
            this.j.goBack();
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.j != null) {
            this.j.loadUrl("about:blank");
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_exit_web_activity:
                this.b.finish();
                return;
            case R.id.textWebActivityTitle:
            case R.id.progressWebLoading:
            case R.id.webview_activity_layout:
            case R.id.web_view_buttons:
            default:
                return;
            case R.id.web_forward:
                if (this.j.canGoForward()) {
                    this.j.goForward();
                    return;
                }
                return;
            case R.id.web_back:
                if (this.j.canGoBack()) {
                    this.j.goBack();
                    return;
                }
                return;
            case R.id.web_refresh:
                this.j.reload();
                return;
        }
    }
}
