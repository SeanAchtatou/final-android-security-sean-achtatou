package com.shoujiduoduo.util;

import com.qhad.ads.sdk.adcore.HttpCacher;
import com.shoujiduoduo.base.a.a;
import java.io.File;
import java.util.Calendar;

/* compiled from: DuoduoCache */
public abstract class s<T> {
    protected static String c = l.a(2);
    protected String b = null;

    public s(String str) {
        this.b = str;
    }

    public s() {
    }

    public static String b() {
        return c;
    }

    public boolean a(int i) {
        long lastModified = new File(c + this.b).lastModified();
        if (lastModified == 0) {
            return true;
        }
        long currentTimeMillis = System.currentTimeMillis() - lastModified;
        long j = currentTimeMillis / 1000;
        a.a("DuoduoCache Base Class", "time since cached: " + (j / 3600) + "小时" + ((j % 3600) / 60) + "分钟" + (j % 60) + "秒");
        if (currentTimeMillis > ((long) (i * HttpCacher.TIME_HOUR)) * 1000) {
            a.a("DuoduoCache Base Class", "cache out of date.");
            return true;
        }
        a.a("DuoduoCache Base Class", "cache is valid. use cache.");
        return false;
    }

    public boolean c() {
        long lastModified = new File(c + this.b).lastModified();
        if (lastModified == 0) {
            return true;
        }
        Calendar instance = Calendar.getInstance();
        instance.setTimeInMillis(lastModified);
        a.a("DuoduoCache", "isCacheOutOfDate, last modified: " + instance.toString());
        long currentTimeMillis = System.currentTimeMillis();
        Calendar instance2 = Calendar.getInstance();
        instance2.setTimeInMillis(currentTimeMillis);
        a.a("DuoduoCache", "isCacheOutOfDate, current: " + instance2.toString());
        if (instance2.get(11) < 4) {
            instance2.add(5, -1);
        }
        instance2.set(11, 4);
        instance2.set(12, 0);
        long timeInMillis = instance2.getTimeInMillis();
        a.a("DuoduoCache", "isCacheOutOfDate, 4:00 of current day:" + instance2.toString());
        if (lastModified >= timeInMillis) {
            return false;
        }
        return true;
    }
}
