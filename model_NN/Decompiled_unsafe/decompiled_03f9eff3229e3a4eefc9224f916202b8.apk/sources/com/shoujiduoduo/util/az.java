package com.shoujiduoduo.util;

import android.app.Activity;
import android.content.Intent;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ringtone.activity.RingToneDuoduoActivity;
import com.shoujiduoduo.ui.home.MusicAlbumActivity;
import com.umeng.socialize.c.b;
import com.umeng.socialize.shareboard.a;
import com.umeng.socialize.utils.ShareBoardlistener;

/* compiled from: UmengSocialUtils */
class az implements ShareBoardlistener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RingData f1582a;
    final /* synthetic */ Activity b;
    final /* synthetic */ ax c;

    az(ax axVar, RingData ringData, Activity activity) {
        this.c = axVar;
        this.f1582a = ringData;
        this.b = activity;
    }

    public void a(a aVar, b bVar) {
        if (bVar != null) {
            this.c.a(this.b, this.f1582a, bVar);
        } else if (aVar.b.equals("music_album")) {
            Intent intent = new Intent(RingDDApp.c(), MusicAlbumActivity.class);
            intent.putExtra("musicid", this.f1582a.g);
            intent.putExtra("title", "音乐相册");
            intent.putExtra("type", MusicAlbumActivity.a.create_album);
            RingToneDuoduoActivity.a().startActivity(intent);
        }
    }
}
