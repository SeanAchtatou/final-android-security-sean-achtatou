package com.shoujiduoduo.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import com.baidu.mobads.interfaces.IXAdRequestInfo;
import com.baidu.mobads.interfaces.utils.IXAdSystemUtils;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ringtone.activity.WelcomeActivity;
import com.shoujiduoduo.util.c.b;
import com.shoujiduoduo.util.c.m;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.util.HashMap;
import java.util.Random;
import java.util.regex.Pattern;
import org.apache.mina.proxy.handlers.http.ntlm.NTLMConstants;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

/* compiled from: CommonUtils */
public final class f {

    /* renamed from: a  reason: collision with root package name */
    public static String f1667a = "com.shoujiduoduo.ringtone.shortcut_action";
    public static int b = 1;
    private static final String c = f.class.getSimpleName();
    private static Context d;
    private static String e;
    private static String f;
    private static String g;
    private static String h;
    private static String i;
    private static String j;
    private static String k;
    private static String l;
    private static String m;
    private static int n;
    private static String o;
    private static a p;

    /* compiled from: CommonUtils */
    public static class a {

        /* renamed from: a  reason: collision with root package name */
        public b f1668a = b.d;
        public String b = "";
    }

    /* compiled from: CommonUtils */
    public enum b {
        f1671a,
        ct,
        cu,
        d
    }

    public static synchronized String a() {
        String str;
        synchronized (f.class) {
            if (e == null) {
                try {
                    z();
                } catch (Exception e2) {
                }
                if (TextUtils.isEmpty(e)) {
                    e = "DEFAULT_USER";
                }
            }
            str = e;
        }
        return str;
    }

    public static String b() {
        TelephonyManager telephonyManager = (TelephonyManager) RingDDApp.c().getSystemService("phone");
        if (telephonyManager == null) {
            return "";
        }
        String line1Number = telephonyManager.getLine1Number();
        com.shoujiduoduo.base.a.a.a(c, "phone num:" + line1Number);
        if (!TextUtils.isEmpty(line1Number)) {
            return line1Number.trim().replace("+86", "").replace(" ", "").replace("\t", "");
        }
        return line1Number;
    }

    public static synchronized String c() {
        String str;
        WifiManager wifiManager;
        synchronized (f.class) {
            if (k == null) {
                if (!(d == null || (wifiManager = (WifiManager) d.getSystemService((String) IXAdSystemUtils.NT_WIFI)) == null)) {
                    k = wifiManager.getConnectionInfo().getMacAddress();
                }
                String str2 = null;
                if (k != null) {
                    str2 = k.trim();
                }
                if (TextUtils.isEmpty(str2)) {
                    k = "MAC_ADDR_UNAVAILABLE";
                }
            }
            str = k;
        }
        return str;
    }

    public static void a(Context context) {
        d = context;
    }

    public static NetworkInfo d() {
        if (d == null) {
            return null;
        }
        return ((ConnectivityManager) d.getSystemService("connectivity")).getActiveNetworkInfo();
    }

    public static boolean e() {
        NetworkInfo d2 = d();
        if (d2 == null || !d2.isConnected()) {
            return false;
        }
        return true;
    }

    public static boolean f() {
        NetworkInfo d2 = d();
        if (d2 == null || !d2.isConnected() || d2.getType() != 1) {
            return false;
        }
        return true;
    }

    public static int g() {
        if (d == null) {
            return -1;
        }
        TelephonyManager telephonyManager = (TelephonyManager) d.getSystemService("phone");
        if (telephonyManager == null) {
            return -1;
        }
        return telephonyManager.getNetworkType();
    }

    public static synchronized String h() {
        String str;
        TelephonyManager telephonyManager;
        synchronized (f.class) {
            if (l == null) {
                if (!(d == null || (telephonyManager = (TelephonyManager) d.getSystemService("phone")) == null)) {
                    l = telephonyManager.getDeviceId();
                }
                if (TextUtils.isEmpty(l)) {
                    l = "default_imei";
                }
            }
            str = l;
        }
        return str;
    }

    public static synchronized String i() {
        String str;
        TelephonyManager telephonyManager;
        synchronized (f.class) {
            if (m == null) {
                if (!(d == null || (telephonyManager = (TelephonyManager) d.getSystemService("phone")) == null)) {
                    m = telephonyManager.getSubscriberId();
                }
                if (TextUtils.isEmpty(m)) {
                    m = "default_imsi";
                }
            }
            str = m;
        }
        return str;
    }

    private static void z() {
        if (d != null) {
            String h2 = h();
            if (h2 == null) {
                String A = A();
                if (!A.equals("")) {
                    e = A;
                    return;
                }
                return;
            }
            e = h2;
        }
    }

    private static String A() {
        String readLine;
        try {
            LineNumberReader lineNumberReader = new LineNumberReader(new InputStreamReader(Runtime.getRuntime().exec("cat /proc/cpuinfo").getInputStream()));
            for (int i2 = 1; i2 < 100 && (readLine = lineNumberReader.readLine()) != null; i2++) {
                if (readLine.indexOf("Serial") > -1) {
                    return readLine.substring(readLine.indexOf(":") + 1, readLine.length()).trim();
                }
            }
            return null;
        } catch (IOException e2) {
            e2.printStackTrace();
            return "0000000000000000";
        }
    }

    public static synchronized String j() {
        String str;
        synchronized (f.class) {
            if (f == null) {
                f = Build.BRAND + ">" + Build.PRODUCT + ">" + Build.MODEL;
            }
            str = f;
        }
        return str;
    }

    public static synchronized String k() {
        String str;
        synchronized (f.class) {
            if (g == null) {
                g = Build.VERSION.SDK_INT + ">" + Build.VERSION.RELEASE;
            }
            str = g;
        }
        return str;
    }

    public static synchronized String l() {
        String str;
        Bundle bundle;
        synchronized (f.class) {
            if (o == null) {
                String str2 = "unknown";
                try {
                    ApplicationInfo applicationInfo = d.getPackageManager().getApplicationInfo(d.getPackageName(), 128);
                    if (!(applicationInfo == null || (bundle = applicationInfo.metaData) == null)) {
                        str2 = bundle.getString("UMENG_CHANNEL");
                        com.shoujiduoduo.base.a.a.a(c, "channel = " + str2);
                    }
                    o = str2;
                    if (TextUtils.isEmpty(o)) {
                        o = "unknown";
                    }
                } catch (PackageManager.NameNotFoundException e2) {
                    PackageManager.NameNotFoundException nameNotFoundException = e2;
                    str = str2;
                    nameNotFoundException.printStackTrace();
                }
            }
            str = o;
        }
        return str;
    }

    public static boolean m() {
        try {
            d a2 = d.a();
            if (a2.a("ro.miui.ui.version.code", null) == null && a2.a("ro.miui.ui.version.name", null) == null && a2.a("ro.miui.internal.storage", null) == null) {
                return false;
            }
            return true;
        } catch (IOException e2) {
            return false;
        }
    }

    @SuppressLint({"NewApi"})
    public static boolean n() {
        int i2 = Resources.getSystem().getConfiguration().uiMode & 15;
        if (i2 == 12 || i2 == 13 || (i2 != 14 && i2 != 15 && i2 != 11)) {
            return false;
        }
        return true;
    }

    public static boolean o() {
        try {
            if (Build.class.getMethod("hasSmartBar", new Class[0]) != null) {
                return true;
            }
            return false;
        } catch (Exception e2) {
            return false;
        }
    }

    public static synchronized String p() {
        String str;
        Bundle bundle;
        synchronized (f.class) {
            if (j == null) {
                String str2 = "unknown";
                try {
                    PackageInfo packageInfo = d.getPackageManager().getPackageInfo(d.getPackageName(), 0);
                    ApplicationInfo applicationInfo = d.getPackageManager().getApplicationInfo(d.getPackageName(), 128);
                    if (!(applicationInfo == null || (bundle = applicationInfo.metaData) == null)) {
                        str2 = bundle.getString("UMENG_CHANNEL");
                        com.shoujiduoduo.base.a.a.a(c, "channel = " + str2);
                    }
                    j = d.getResources().getString(R.string.app_version_prefix) + packageInfo.versionName + "_" + str2 + d.getResources().getString(R.string.install_source_suffix);
                    if (TextUtils.isEmpty(j)) {
                        j = "unknown";
                    }
                } catch (PackageManager.NameNotFoundException e2) {
                    e2.printStackTrace();
                    str = d.getResources().getString(R.string.app_version_prefix) + "0.0.0.0" + d.getResources().getString(R.string.install_source_suffix);
                }
            }
            str = j;
        }
        return str;
    }

    public static synchronized String q() {
        String str;
        synchronized (f.class) {
            if (h == null) {
                try {
                    h = d.getResources().getString(R.string.app_version_prefix) + d.getPackageManager().getPackageInfo(d.getPackageName(), 0).versionName;
                    if (TextUtils.isEmpty(h)) {
                        h = "unknown";
                    }
                } catch (PackageManager.NameNotFoundException e2) {
                    e2.printStackTrace();
                    str = d.getResources().getString(R.string.app_version_prefix) + "0.0.0.0";
                }
            }
            str = h;
        }
        return str;
    }

    public static synchronized int r() {
        int i2;
        synchronized (f.class) {
            if (n == 0) {
                try {
                    PackageInfo packageInfo = d.getPackageManager().getPackageInfo(d.getPackageName(), 0);
                    if (packageInfo != null) {
                        n = packageInfo.versionCode;
                    }
                } catch (PackageManager.NameNotFoundException e2) {
                    e2.printStackTrace();
                }
            }
            i2 = n;
        }
        return i2;
    }

    public static synchronized String s() {
        String str;
        synchronized (f.class) {
            if (i == null) {
                try {
                    PackageInfo packageInfo = d.getPackageManager().getPackageInfo(d.getPackageName(), 0);
                    if (packageInfo != null) {
                        i = packageInfo.versionName;
                    }
                } catch (PackageManager.NameNotFoundException e2) {
                    e2.printStackTrace();
                }
                if (TextUtils.isEmpty(i)) {
                    i = "unknown";
                }
            }
            str = i;
        }
        return str;
    }

    public static String t() {
        return d.getResources().getString(R.string.english_name);
    }

    public static boolean a(String str) {
        PackageInfo packageInfo;
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        try {
            packageInfo = d.getPackageManager().getPackageInfo(str, 0);
        } catch (PackageManager.NameNotFoundException e2) {
            packageInfo = null;
        }
        if (packageInfo != null) {
            return true;
        }
        return false;
    }

    public static boolean a(Context context, String str) {
        if (av.c(str)) {
            return false;
        }
        try {
            PackageManager packageManager = context.getPackageManager();
            new Intent();
            context.startActivity(packageManager.getLaunchIntentForPackage(str));
            return true;
        } catch (Exception e2) {
            return false;
        }
    }

    public static void b(String str) {
        Uri fromFile = Uri.fromFile(new File(str));
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setFlags(NTLMConstants.FLAG_UNIDENTIFIED_11);
        intent.setDataAndType(fromFile, "application/vnd.android.package-archive");
        d.startActivity(intent);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public static void a(Context context, String str, int i2) {
        Intent intent = new Intent("com.android.launcher.action.INSTALL_SHORTCUT");
        Intent intent2 = new Intent("android.intent.action.MAIN");
        intent2.addCategory("android.intent.category.LAUNCHER");
        intent2.setClass(context, WelcomeActivity.class);
        intent.putExtra("android.intent.extra.shortcut.INTENT", intent2);
        intent.putExtra("android.intent.extra.shortcut.NAME", str);
        intent.putExtra("duplicate", false);
        intent.putExtra("android.intent.extra.shortcut.ICON_RESOURCE", Intent.ShortcutIconResource.fromContext(context, i2));
        context.sendBroadcast(intent);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public static void a(Context context, String str, int i2, int i3) {
        Intent intent = new Intent("com.android.launcher.action.INSTALL_SHORTCUT");
        Intent intent2 = new Intent("android.intent.action.MAIN");
        intent2.addCategory("android.intent.category.LAUNCHER");
        intent2.setClass(context, WelcomeActivity.class);
        intent2.putExtra(f1667a, i2);
        intent.putExtra("android.intent.extra.shortcut.INTENT", intent2);
        intent.putExtra("android.intent.extra.shortcut.NAME", str);
        intent.putExtra("duplicate", false);
        intent.putExtra("android.intent.extra.shortcut.ICON_RESOURCE", Intent.ShortcutIconResource.fromContext(context, i3));
        context.sendBroadcast(intent);
    }

    public static String a(NamedNodeMap namedNodeMap, String str) {
        String nodeValue;
        Node namedItem = namedNodeMap.getNamedItem(str);
        if (namedItem == null || (nodeValue = namedItem.getNodeValue()) == null) {
            return "";
        }
        return nodeValue;
    }

    public static String a(NamedNodeMap namedNodeMap, String str, String str2) {
        String nodeValue;
        Node namedItem = namedNodeMap.getNamedItem(str);
        if (namedItem == null || (nodeValue = namedItem.getNodeValue()) == null) {
            return str2;
        }
        return nodeValue;
    }

    public static int a(NamedNodeMap namedNodeMap, String str, int i2) {
        Node namedItem = namedNodeMap.getNamedItem(str);
        if (namedItem == null) {
            return i2;
        }
        String nodeValue = namedItem.getNodeValue();
        if (TextUtils.isEmpty(nodeValue)) {
            return i2;
        }
        try {
            return Integer.parseInt(nodeValue);
        } catch (NumberFormatException e2) {
            e2.printStackTrace();
            return i2;
        }
    }

    public static boolean a(NamedNodeMap namedNodeMap, String str, boolean z) {
        String nodeValue;
        Node namedItem = namedNodeMap.getNamedItem(str);
        if (namedItem == null || (nodeValue = namedItem.getNodeValue()) == null) {
            return z;
        }
        try {
            return Boolean.parseBoolean(nodeValue);
        } catch (Throwable th) {
            th.printStackTrace();
            return z;
        }
    }

    public static boolean c(String str) {
        if (d == null || str == null || str.length() == 0) {
            return false;
        }
        com.umeng.analytics.b.a(d, str);
        return true;
    }

    public static void a(Context context, String str, HashMap<String, String> hashMap, long j2) {
        hashMap.put("__ct__", String.valueOf(j2));
        com.umeng.analytics.b.a(context, str, hashMap);
    }

    public static int a(float f2) {
        if (d == null) {
            return -1;
        }
        return (int) ((d.getResources().getDisplayMetrics().density * f2) + 0.5f);
    }

    public static String d(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        StringBuilder sb = new StringBuilder();
        for (int i2 = 0; i2 < str.length(); i2++) {
            if (a(str.charAt(i2))) {
                sb.append(str.charAt(i2));
            }
        }
        return sb.toString();
    }

    public static boolean a(char c2) {
        return (c2 >= 19968 && c2 <= 40869) || (c2 >= 'A' && c2 <= 'Z') || ((c2 >= 'a' && c2 <= 'z') || ((c2 >= 0 && c2 <= 9) || c2 == '-' || c2 == '_' || c2 == '(' || c2 == ')' || c2 == ' '));
    }

    public static String e(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        StringBuilder sb = new StringBuilder();
        for (int i2 = 0; i2 < str.length(); i2++) {
            if (!b(str.charAt(i2))) {
                sb.append(str.charAt(i2));
            }
        }
        return sb.toString();
    }

    public static boolean b(char c2) {
        return (c2 >= 0 && c2 <= 8) || (c2 >= 11 && c2 <= 12) || ((c2 >= 14 && c2 <= 31) || ((c2 >= 55296 && c2 <= 57343) || c2 == 65534 || c2 == 65535));
    }

    public static synchronized b u() {
        boolean z;
        b bVar;
        synchronized (f.class) {
            if (com.shoujiduoduo.base.a.a.b != b.d) {
                bVar = com.shoujiduoduo.base.a.a.b;
            } else {
                String a2 = as.a(RingDDApp.c(), "user_phone_num", "");
                int a3 = as.a(RingDDApp.c(), "user_loginStatus", 0);
                if (TextUtils.isEmpty(a2) || a3 == 0 || (bVar = g(a2)) == b.d) {
                    if (p != null) {
                        bVar = p.f1668a;
                    } else {
                        p = new a();
                        if (d == null) {
                            p.f1668a = b.d;
                            bVar = b.d;
                        } else {
                            if (!m.a().c() || !m.a().d()) {
                                z = false;
                            } else {
                                com.shoujiduoduo.base.a.a.a(c, "多多判断：双卡平台，并且安装了中国移动sim卡");
                                z = true;
                            }
                            if (z) {
                                com.umeng.analytics.b.b(d, "DUAL_SIM_CARD_CMCC");
                                p.f1668a = b.f1671a;
                                bVar = b.f1671a;
                            } else if (com.shoujiduoduo.util.c.b.a().c() == b.a.success) {
                                p.f1668a = b.f1671a;
                                bVar = b.f1671a;
                            } else {
                                TelephonyManager telephonyManager = (TelephonyManager) d.getSystemService("phone");
                                if (telephonyManager == null) {
                                    p.f1668a = b.d;
                                    bVar = b.d;
                                } else {
                                    String subscriberId = telephonyManager.getSubscriberId();
                                    if (subscriberId != null) {
                                        com.shoujiduoduo.base.a.a.a(c, "imsi:" + subscriberId);
                                        if (subscriberId.startsWith("46000") || subscriberId.startsWith("46002") || subscriberId.startsWith("46007")) {
                                            com.shoujiduoduo.base.a.a.a(c, IXAdRequestInfo.MAX_CONTENT_LENGTH);
                                            p.f1668a = b.f1671a;
                                            bVar = b.f1671a;
                                        } else if (subscriberId.startsWith("46001") || subscriberId.startsWith("46006")) {
                                            com.shoujiduoduo.base.a.a.a(c, "cu");
                                            p.f1668a = b.cu;
                                            bVar = b.cu;
                                        } else if (subscriberId.startsWith("46003") || subscriberId.startsWith("46005")) {
                                            com.shoujiduoduo.base.a.a.a(c, "ct");
                                            p.f1668a = b.ct;
                                            bVar = b.ct;
                                        } else if (subscriberId.startsWith("46020")) {
                                        }
                                    } else {
                                        com.shoujiduoduo.base.a.a.a(c, "imsi is null");
                                    }
                                    p.f1668a = b.f1671a;
                                    bVar = p.f1668a;
                                }
                            }
                        }
                    }
                }
            }
        }
        return bVar;
    }

    public static boolean f(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        return Pattern.compile("^((13[0-9])|14[5,7]|(15[^4,\\D])|(18[0-9])|(17[0-9]))\\d{8}$").matcher(str.trim().replace("+86", "")).matches();
    }

    public static b g(String str) {
        String h2 = h(str);
        if (h2.equals("1")) {
            return b.cu;
        }
        if (h2.equals("2")) {
            return b.f1671a;
        }
        if (h2.equals("3")) {
            return b.ct;
        }
        return b.d;
    }

    private static String h(String str) {
        if (TextUtils.isEmpty(str)) {
            return "0";
        }
        String trim = str.trim();
        if (trim.startsWith("0") || trim.startsWith("+860")) {
            trim = trim.substring(trim.indexOf("0") + 1);
        } else if (trim.startsWith("+86")) {
            trim = trim.substring(3);
        }
        if (trim.length() < 11) {
            return "0";
        }
        String c2 = com.umeng.analytics.b.c(RingDDApp.c(), "cmcc_num_list");
        if (TextUtils.isEmpty(c2)) {
            c2 = "134/135/136/137/138/139/147/150/151/152/157/158/159/182/183/184/187/188/178";
        }
        String c3 = com.umeng.analytics.b.c(RingDDApp.c(), "cucc_num_list");
        if (TextUtils.isEmpty(c3)) {
            c3 = "130/131/132/145/155/156/185/186/176";
        }
        String c4 = com.umeng.analytics.b.c(RingDDApp.c(), "ctcc_num_list");
        if (TextUtils.isEmpty(c4)) {
            c4 = "133/153/180/181/189/177/170/173";
        }
        if (c2.contains(trim.substring(0, 3))) {
            return "2";
        }
        if (c3.contains(trim.substring(0, 3))) {
            return "1";
        }
        if (c4.contains(trim.substring(0, 3))) {
            return "3";
        }
        HashMap hashMap = new HashMap();
        hashMap.put("num", trim.substring(0, 3));
        hashMap.put("detail", trim);
        com.umeng.analytics.b.a(RingDDApp.c(), "unknown_phone_type", hashMap);
        return "0";
    }

    public static synchronized boolean v() {
        boolean z = false;
        synchronized (f.class) {
            if (!"false".equals(com.umeng.analytics.b.c(d, "cmcc_cailing_switch"))) {
                if (!RingDDApp.b().a() && u().equals(b.f1671a)) {
                    z = true;
                }
            }
        }
        return z;
    }

    public static synchronized boolean w() {
        boolean z = false;
        synchronized (f.class) {
            if (!"false".equals(com.umeng.analytics.b.c(d, "cucc_cailing_switch"))) {
                if (u().equals(b.cu)) {
                    z = true;
                }
            }
        }
        return z;
    }

    public static synchronized boolean x() {
        boolean z = false;
        synchronized (f.class) {
            if (!"false".equals(com.umeng.analytics.b.c(d, "ctcc_cailing_switch"))) {
                if (u().equals(b.ct)) {
                    z = true;
                }
            }
        }
        return z;
    }

    public static boolean y() {
        int a2 = as.a(d, "preference_continuous_play", -1);
        if (a2 < 0) {
            as.b(d, "preference_continuous_play", 1);
            return true;
        } else if (a2 != 1) {
            return false;
        } else {
            return true;
        }
    }

    public static void a(boolean z) {
        if (d != null) {
            as.b(d, "preference_continuous_play", z ? 1 : 0);
        }
    }

    public static String a(int i2) {
        Random random = new Random();
        StringBuilder sb = new StringBuilder();
        do {
            int nextInt = random.nextInt();
            if (nextInt != Integer.MIN_VALUE) {
                sb.append(Math.abs(nextInt) % 10);
            }
        } while (sb.length() < i2);
        return sb.toString();
    }
}
