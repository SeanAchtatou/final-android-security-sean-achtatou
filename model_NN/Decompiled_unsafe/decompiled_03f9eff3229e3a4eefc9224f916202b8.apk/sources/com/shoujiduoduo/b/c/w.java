package com.shoujiduoduo.b.c;

import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.base.bean.c;
import com.shoujiduoduo.base.bean.f;
import java.util.ArrayList;

/* compiled from: SimpleList */
public class w implements c {

    /* renamed from: a  reason: collision with root package name */
    private ArrayList<RingData> f769a = new ArrayList<>();

    public void a(RingData ringData) {
        this.f769a.add(ringData);
    }

    public Object a(int i) {
        if (i < 0 || i >= this.f769a.size()) {
            return null;
        }
        return this.f769a.get(i);
    }

    public String a() {
        return "";
    }

    public f.a b() {
        return f.a.list_simple;
    }

    public int c() {
        if (this.f769a != null) {
            return this.f769a.size();
        }
        return 0;
    }

    public boolean d() {
        return false;
    }

    public void e() {
    }

    public void f() {
    }

    public boolean g() {
        return false;
    }
}
