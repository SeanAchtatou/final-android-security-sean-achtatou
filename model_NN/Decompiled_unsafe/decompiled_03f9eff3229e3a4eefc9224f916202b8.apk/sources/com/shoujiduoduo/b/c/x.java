package com.shoujiduoduo.b.c;

import android.util.Xml;
import cn.banshenggua.aichang.utils.Constants;
import com.shoujiduoduo.a.a.b;
import com.shoujiduoduo.base.bean.aa;
import com.shoujiduoduo.util.f;
import com.shoujiduoduo.util.i;
import com.shoujiduoduo.util.s;
import com.shoujiduoduo.util.t;
import com.shoujiduoduo.util.w;
import com.sina.weibo.sdk.register.mobile.SelectCountryActivity;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashSet;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xmlpull.v1.XmlSerializer;

/* compiled from: TopListMgrImpl */
public class x implements m {

    /* renamed from: a  reason: collision with root package name */
    private static final String f770a = x.class.getSimpleName();
    private ArrayList<aa> b;
    /* access modifiers changed from: private */
    public a c;
    private boolean d;
    private HashSet<String> e;

    public void a() {
        this.c = new a("toplist.tmp");
        this.e = new HashSet<>();
        this.e.add("list");
        this.e.add("collect");
        this.e.add("artist");
        this.e.add("html");
        e();
    }

    public void b() {
    }

    public boolean c() {
        return this.d;
    }

    public ArrayList<aa> d() {
        if (this.d) {
            return this.b;
        }
        return null;
    }

    /* access modifiers changed from: private */
    public ArrayList<aa> a(InputStream inputStream) {
        try {
            Document parse = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(inputStream);
            if (parse == null) {
                return null;
            }
            Element documentElement = parse.getDocumentElement();
            if (documentElement == null) {
                com.shoujiduoduo.base.a.a.c(f770a, "cannot find root node");
                return null;
            }
            NodeList elementsByTagName = documentElement.getElementsByTagName(Constants.ITEM);
            if (elementsByTagName == null) {
                com.shoujiduoduo.base.a.a.c(f770a, "cannot find node named \"item\"");
                return null;
            }
            ArrayList<aa> arrayList = new ArrayList<>();
            for (int i = 0; i < elementsByTagName.getLength(); i++) {
                NamedNodeMap attributes = elementsByTagName.item(i).getAttributes();
                aa aaVar = new aa();
                aaVar.e = f.a(attributes, SelectCountryActivity.EXTRA_COUNTRY_NAME);
                aaVar.f = f.a(attributes, "type", "");
                aaVar.g = f.a(attributes, "id", 0);
                aaVar.h = f.a(attributes, "url");
                if (!this.e.contains(aaVar.f)) {
                    com.shoujiduoduo.base.a.a.e(f770a, "not support top list type:" + aaVar.f);
                } else if (!"html".equals(aaVar.f)) {
                    arrayList.add(aaVar);
                } else if (com.shoujiduoduo.util.a.a()) {
                    arrayList.add(aaVar);
                }
            }
            return arrayList;
        } catch (IOException e2) {
            com.shoujiduoduo.base.a.a.a(e2);
            return null;
        } catch (SAXException e3) {
            com.shoujiduoduo.base.a.a.a(e3);
            return null;
        } catch (ParserConfigurationException e4) {
            com.shoujiduoduo.base.a.a.a(e4);
            return null;
        } catch (ArrayIndexOutOfBoundsException e5) {
            com.shoujiduoduo.base.a.a.a(e5);
            return null;
        } catch (DOMException e6) {
            com.shoujiduoduo.base.a.a.a(e6);
            return null;
        }
    }

    /* access modifiers changed from: private */
    public boolean f() {
        String str = "";
        if (f.u().toString().contains("cu")) {
            str = "&cucc=1";
        }
        byte[] a2 = w.a("&type=gettabs", str);
        if (a2 != null) {
            this.b = a(new ByteArrayInputStream(a2));
            if (this.b == null || this.b.size() <= 0) {
                com.shoujiduoduo.base.a.a.c(f770a, "parse net data error");
            } else {
                com.shoujiduoduo.base.a.a.a(f770a, this.b.size() + " keywords.");
                this.c.a(this.b);
                this.d = true;
                com.shoujiduoduo.a.a.x.a().a(b.OBSERVER_TOP_LIST, new y(this));
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    public boolean g() {
        this.b = this.c.a();
        if (this.b == null || this.b.size() <= 0) {
            com.shoujiduoduo.base.a.a.a(f770a, "cache is not valid");
            return false;
        }
        com.shoujiduoduo.base.a.a.a(f770a, this.b.size() + " list. read from cache.");
        this.d = true;
        com.shoujiduoduo.a.a.x.a().a(b.OBSERVER_TOP_LIST, new z(this));
        return true;
    }

    public void e() {
        if (this.b == null) {
            i.a(new aa(this));
        }
    }

    /* compiled from: TopListMgrImpl */
    class a extends s<ArrayList<aa>> {
        a(String str) {
            super(str);
        }

        public ArrayList<aa> a() {
            try {
                return x.this.a(new FileInputStream(c + this.b));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                return null;
            }
        }

        public void a(ArrayList<aa> arrayList) {
            if (arrayList != null && arrayList.size() != 0) {
                XmlSerializer newSerializer = Xml.newSerializer();
                StringWriter stringWriter = new StringWriter();
                try {
                    newSerializer.setOutput(stringWriter);
                    newSerializer.startDocument("UTF-8", true);
                    newSerializer.startTag("", "root");
                    newSerializer.attribute("", "num", String.valueOf(arrayList.size()));
                    for (int i = 0; i < arrayList.size(); i++) {
                        aa aaVar = arrayList.get(i);
                        newSerializer.startTag("", Constants.ITEM);
                        newSerializer.attribute("", SelectCountryActivity.EXTRA_COUNTRY_NAME, aaVar.e);
                        newSerializer.attribute("", "type", "" + aaVar.f);
                        newSerializer.attribute("", "id", "" + aaVar.g);
                        newSerializer.attribute("", "url", "" + aaVar.h);
                        newSerializer.endTag("", Constants.ITEM);
                    }
                    newSerializer.endTag("", "root");
                    newSerializer.endDocument();
                    t.b(c + this.b, stringWriter.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
