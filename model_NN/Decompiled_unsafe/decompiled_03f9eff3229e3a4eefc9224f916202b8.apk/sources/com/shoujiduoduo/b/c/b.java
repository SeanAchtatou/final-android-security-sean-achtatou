package com.shoujiduoduo.b.c;

import android.os.Handler;
import android.os.Message;
import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.base.bean.e;
import java.util.ArrayList;

/* compiled from: ArtistList */
class b extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f756a;

    b(a aVar) {
        this.f756a = aVar;
    }

    public void handleMessage(Message message) {
        switch (message.what) {
            case 0:
                e eVar = (e) message.obj;
                if (eVar != null) {
                    a.a("RingList", "new obtained list data size = " + eVar.f821a.size());
                    if (this.f756a.g == null) {
                        ArrayList unused = this.f756a.g = eVar.f821a;
                    } else {
                        this.f756a.g.addAll(eVar.f821a);
                    }
                    boolean unused2 = this.f756a.c = eVar.b;
                    eVar.f821a = this.f756a.g;
                    if (this.f756a.f && this.f756a.g.size() > 0) {
                        this.f756a.f754a.a(eVar);
                        boolean unused3 = this.f756a.f = false;
                    }
                }
                boolean unused4 = this.f756a.d = false;
                boolean unused5 = this.f756a.e = false;
                break;
            case 1:
            case 2:
                boolean unused6 = this.f756a.d = false;
                boolean unused7 = this.f756a.e = true;
                break;
        }
        x.a().a(com.shoujiduoduo.a.a.b.OBSERVER_LIST_DATA, new c(this, message.what));
    }
}
