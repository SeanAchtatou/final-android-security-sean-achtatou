package com.shoujiduoduo.b.c;

import android.os.Handler;
import android.os.Message;
import com.shoujiduoduo.a.a.b;
import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.base.bean.e;
import java.util.ArrayList;

/* compiled from: CollectList */
class h extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ g f760a;

    h(g gVar) {
        this.f760a = gVar;
    }

    public void handleMessage(Message message) {
        switch (message.what) {
            case 0:
                e eVar = (e) message.obj;
                if (eVar != null) {
                    a.a("RingList", "new obtained list data size = " + eVar.f821a.size());
                    if (this.f760a.g == null) {
                        ArrayList unused = this.f760a.g = eVar.f821a;
                    } else {
                        this.f760a.g.addAll(eVar.f821a);
                    }
                    boolean unused2 = this.f760a.c = eVar.b;
                    eVar.f821a = this.f760a.g;
                    if (this.f760a.f && this.f760a.g.size() > 0) {
                        this.f760a.f759a.a(eVar);
                        boolean unused3 = this.f760a.f = false;
                    }
                }
                boolean unused4 = this.f760a.d = false;
                boolean unused5 = this.f760a.e = false;
                break;
            case 1:
            case 2:
                boolean unused6 = this.f760a.d = false;
                boolean unused7 = this.f760a.e = true;
                break;
        }
        x.a().a(b.OBSERVER_LIST_DATA, new i(this, message.what));
    }
}
