package com.shoujiduoduo.b.c;

import android.os.Handler;
import android.os.Message;
import com.shoujiduoduo.a.a.b;
import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.base.bean.e;
import java.util.ArrayList;

/* compiled from: RingList */
class o extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ n f765a;

    o(n nVar) {
        this.f765a = nVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.b.c.n.b(com.shoujiduoduo.b.c.n, boolean):boolean
     arg types: [com.shoujiduoduo.b.c.n, int]
     candidates:
      com.shoujiduoduo.b.c.n.b(com.shoujiduoduo.b.c.n, java.lang.String):java.lang.String
      com.shoujiduoduo.b.c.n.b(com.shoujiduoduo.b.c.n, boolean):boolean */
    public void handleMessage(Message message) {
        switch (message.what) {
            case 0:
                e eVar = (e) message.obj;
                if (eVar != null) {
                    a.a("RingList", "new obtained list data size = " + eVar.f821a.size());
                    if (this.f765a.l) {
                        this.f765a.r.clear();
                        this.f765a.r.addAll(eVar.f821a);
                    } else if (this.f765a.r == null) {
                        ArrayList unused = this.f765a.r = eVar.f821a;
                    } else {
                        this.f765a.r.addAll(eVar.f821a);
                    }
                    String unused2 = this.f765a.o = eVar.c;
                    boolean unused3 = this.f765a.h = eVar.b;
                    String unused4 = this.f765a.q = eVar.d;
                    eVar.f821a = this.f765a.r;
                    if (this.f765a.k && this.f765a.r.size() > 0) {
                        if (this.f765a.n != null) {
                            this.f765a.n.a(eVar);
                        }
                        boolean unused5 = this.f765a.k = false;
                    }
                } else {
                    this.f765a.r.clear();
                }
                boolean unused6 = this.f765a.i = false;
                boolean unused7 = this.f765a.j = false;
                break;
            case 1:
            case 2:
                boolean unused8 = this.f765a.i = false;
                boolean unused9 = this.f765a.j = true;
                break;
        }
        x.a().a(b.OBSERVER_LIST_DATA, new p(this, message.what));
    }
}
