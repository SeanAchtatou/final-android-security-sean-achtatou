package com.shoujiduoduo.b.c;

import cn.banshenggua.aichang.utils.StringUtil;
import com.baidu.mobads.interfaces.IXAdRequestInfo;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.base.bean.e;
import com.shoujiduoduo.util.j;
import com.shoujiduoduo.util.s;
import com.sina.weibo.sdk.constant.WBConstants;
import com.sina.weibo.sdk.register.mobile.SelectCountryActivity;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/* compiled from: RingListCache */
public class v extends s<e<RingData>> {
    v() {
    }

    v(String str) {
        super(str);
    }

    public e<RingData> a() {
        try {
            return j.c(new FileInputStream(c + this.b));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (ArrayIndexOutOfBoundsException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public void a(e<RingData> eVar) {
        ArrayList<T> arrayList = eVar.f821a;
        try {
            Document newDocument = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
            Element createElement = newDocument.createElement("list");
            createElement.setAttribute("num", String.valueOf(arrayList.size()));
            createElement.setAttribute("hasmore", String.valueOf(eVar.b));
            createElement.setAttribute("area", String.valueOf(eVar.c));
            createElement.setAttribute("baseurl", eVar.d);
            newDocument.appendChild(createElement);
            for (int i = 0; i < arrayList.size(); i++) {
                RingData ringData = (RingData) arrayList.get(i);
                Element createElement2 = newDocument.createElement("ring");
                createElement2.setAttribute(SelectCountryActivity.EXTRA_COUNTRY_NAME, ringData.e);
                createElement2.setAttribute("artist", ringData.f);
                createElement2.setAttribute("duration", String.valueOf(ringData.j));
                createElement2.setAttribute(WBConstants.GAME_PARAMS_SCORE, String.valueOf(ringData.i));
                createElement2.setAttribute("playcnt", String.valueOf(ringData.k));
                createElement2.setAttribute("rid", ringData.g);
                createElement2.setAttribute("bdurl", ringData.h);
                createElement2.setAttribute("hbr", String.valueOf(ringData.d()));
                createElement2.setAttribute("hurl", ringData.c());
                createElement2.setAttribute("lbr", String.valueOf(ringData.f()));
                createElement2.setAttribute("lurl", ringData.e());
                createElement2.setAttribute("mp3br", String.valueOf(ringData.h()));
                createElement2.setAttribute("mp3url", ringData.g());
                if (ringData.l != 0) {
                    createElement2.setAttribute("isnew", String.valueOf(ringData.l));
                }
                createElement2.setAttribute(IXAdRequestInfo.CELL_ID, ringData.n);
                createElement2.setAttribute("valid", ringData.o);
                createElement2.setAttribute("hasmedia", String.valueOf(ringData.q));
                createElement2.setAttribute("singerId", ringData.r);
                createElement2.setAttribute("price", String.valueOf(ringData.p));
                createElement2.setAttribute("ctcid", ringData.s);
                createElement2.setAttribute("ctvalid", ringData.t);
                createElement2.setAttribute("cthasmedia", String.valueOf(ringData.v));
                createElement2.setAttribute("ctprice", String.valueOf(ringData.u));
                createElement2.setAttribute("ctvip", String.valueOf(ringData.w));
                createElement2.setAttribute("wavurl", ringData.x);
                createElement2.setAttribute("cuvip", String.valueOf(ringData.y));
                createElement2.setAttribute("cuftp", ringData.z);
                createElement2.setAttribute("cucid", ringData.A);
                createElement2.setAttribute("cusid", ringData.C);
                createElement2.setAttribute("cuurl", ringData.D);
                createElement2.setAttribute("cuvalid", ringData.B);
                createElement2.setAttribute("hasshow", String.valueOf(ringData.E));
                createElement.appendChild(createElement2);
            }
            Transformer newTransformer = TransformerFactory.newInstance().newTransformer();
            newTransformer.setOutputProperty("encoding", StringUtil.Encoding);
            newTransformer.setOutputProperty("indent", "yes");
            newTransformer.setOutputProperty("standalone", "yes");
            newTransformer.setOutputProperty("method", "xml");
            newTransformer.transform(new DOMSource(newDocument.getDocumentElement()), new StreamResult(new FileOutputStream(new File(c + this.b))));
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
            a.a(e);
        } catch (TransformerException e2) {
            e2.printStackTrace();
            a.a(e2);
        } catch (FileNotFoundException e3) {
            e3.printStackTrace();
            a.a(e3);
        } catch (Exception e4) {
            e4.printStackTrace();
            a.a(e4);
        }
    }
}
