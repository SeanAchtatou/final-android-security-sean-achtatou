package com.shoujiduoduo.b.c;

import android.content.Context;
import android.os.Handler;
import android.text.TextUtils;
import cn.banshenggua.aichang.utils.Constants;
import com.cmsc.cmmusic.common.data.GetUserInfoRsp;
import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.base.bean.c;
import com.shoujiduoduo.base.bean.e;
import com.shoujiduoduo.base.bean.f;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.util.as;
import com.shoujiduoduo.util.aw;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.c.b;
import com.shoujiduoduo.util.i;
import com.shoujiduoduo.util.j;
import com.shoujiduoduo.util.u;
import com.shoujiduoduo.util.w;
import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/* compiled from: RingList */
public class n implements c {

    /* renamed from: a  reason: collision with root package name */
    private static final String f763a = n.class.getSimpleName();
    private int b = 0;
    private boolean c = false;
    private String d = "";
    private int e;
    private aw f;
    private String g = "";
    /* access modifiers changed from: private */
    public boolean h = true;
    /* access modifiers changed from: private */
    public boolean i = false;
    /* access modifiers changed from: private */
    public boolean j = false;
    /* access modifiers changed from: private */
    public boolean k = false;
    /* access modifiers changed from: private */
    public boolean l = false;
    /* access modifiers changed from: private */
    public f.a m;
    /* access modifiers changed from: private */
    public v n = null;
    /* access modifiers changed from: private */
    public String o;
    private String p;
    /* access modifiers changed from: private */
    public String q = null;
    /* access modifiers changed from: private */
    public ArrayList<RingData> r = new ArrayList<>();
    private Handler s = new o(this);

    public n(f.a aVar, String str, String str2) {
        this.p = str;
        this.g = str2;
        this.m = aVar;
        this.n = new v(this.p.hashCode() + ".tmp");
    }

    public n(f.a aVar) {
        this.m = aVar;
    }

    public n(f.a aVar, String str, boolean z, String str2) {
        this.m = aVar;
        this.e = u.a(str, 0);
        this.c = z;
        this.d = str2;
        if (aVar == f.a.list_ring_recommon || aVar == f.a.list_ring_category) {
            if (!TextUtils.isEmpty(str2)) {
                if (z) {
                    this.b = new Random().nextInt(100) + 1;
                    this.n = new v("list_" + str + "_" + str2 + "_" + this.b + ".tmp");
                    return;
                }
                this.n = new v("list_" + str + "_" + str2 + ".tmp");
            } else if (z) {
                this.b = new Random().nextInt(100) + 1;
                this.n = new v("list_" + str + "_" + this.b + ".tmp");
            } else {
                this.n = new v("list_" + str + ".tmp");
            }
        } else if (aVar == f.a.list_ring_cmcc) {
            this.n = new v("cmcc_cailing.tmp");
        } else if (aVar == f.a.list_ring_ctcc) {
            this.n = new v("ctcc_cailing.tmp");
        } else if (aVar == f.a.list_ring_cucc) {
            this.n = new v("cucc_cailing.tmp");
        } else if (aVar == f.a.list_ring_collect) {
            this.n = new v("collect_" + str + ".tmp");
        } else if (aVar == f.a.list_ring_artist) {
            this.n = new v("artist_" + str + ".tmp");
        } else {
            this.n = new v("unknown.tmp");
        }
    }

    public f.a h() {
        return this.m;
    }

    public String a() {
        switch (this.m) {
            case list_ring_recommon:
            case list_ring_category:
                return "" + this.e;
            case list_ring_search:
                return "search_" + this.g;
            case list_ring_cmcc:
                return "cmcc_cailing";
            case list_ring_ctcc:
                return "ctcc_cailing";
            case list_ring_cucc:
                return "cucc_cailing";
            case sys_alarm:
                return "sys_ring_alram";
            case sys_notify:
                return "sys_ring_notify";
            case sys_ringtone:
                return "sys_ring_ringtone";
            default:
                return "unknown";
        }
    }

    public void f() {
        this.i = true;
        this.j = false;
        this.l = true;
        i.a(new q(this));
    }

    public void e() {
        if (this.r == null || this.r.size() == 0) {
            this.i = true;
            this.j = false;
            this.l = false;
            i.a(new r(this));
        } else if (this.h) {
            this.i = true;
            this.j = false;
            this.l = false;
            i.a(new s(this));
        }
    }

    /* access modifiers changed from: private */
    public void i() {
        if (!this.n.a(4)) {
            a.a(f763a, "RingList: cache is available! Use Cache!");
            e<RingData> a2 = this.n.a();
            if (!(a2 == null || a2.f821a == null || a2.f821a.size() <= 0)) {
                a.a(f763a, "RingList: Read RingList Cache Success!");
                this.s.sendMessage(this.s.obtainMessage(0, a2));
                return;
            }
        }
        a.a(f763a, "RingList: cache is out of date or read cache failed!");
        byte[] c2 = c(0);
        if (c2 == null) {
            a.a(f763a, "RingList: httpGetRingList Failed!");
            this.s.sendEmptyMessage(1);
            return;
        }
        e<RingData> c3 = j.c(new ByteArrayInputStream(c2));
        if (c3 != null) {
            a.a("RingList", "list data size = " + c3.f821a.size());
            a.a(f763a, "RingList: Read from network Success! send MESSAGE_SUCCESS_RETRIEVE_DATA");
            this.k = true;
            this.s.sendMessage(this.s.obtainMessage(0, c3));
            return;
        }
        a.a(f763a, "RingList: Read from network Success, but parse FAILED! send MESSAGE_FAIL_RETRIEVE_DATA");
        this.s.sendEmptyMessage(1);
    }

    /* access modifiers changed from: private */
    public void j() {
        e<RingData> eVar;
        a.a(f763a, "retrieving more data, list size = " + this.r.size());
        byte[] c2 = c(this.r.size() / 25);
        if (c2 == null) {
            this.s.sendEmptyMessage(2);
            return;
        }
        try {
            eVar = j.c(new ByteArrayInputStream(c2));
        } catch (ArrayIndexOutOfBoundsException e2) {
            eVar = null;
            com.shoujiduoduo.util.f.c("parse ringlist error! ArrayIndexOutOfBoundsException. " + new String(c2));
        }
        if (eVar != null) {
            a.a("RingList", "list data size = " + eVar.f821a.size());
            this.k = true;
            this.s.sendMessage(this.s.obtainMessage(0, eVar));
            return;
        }
        this.s.sendEmptyMessage(2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.util.c.b.a(java.lang.String, boolean):com.shoujiduoduo.util.b.c$b
     arg types: [java.lang.String, int]
     candidates:
      com.shoujiduoduo.util.c.b.a(com.shoujiduoduo.util.c.b, java.lang.String):com.shoujiduoduo.util.b.c$j
      com.shoujiduoduo.util.c.b.a(com.shoujiduoduo.util.c.b, com.shoujiduoduo.util.c.b$a):com.shoujiduoduo.util.c.b$a
      com.shoujiduoduo.util.c.b.a(com.shoujiduoduo.util.c.b, android.content.Context):com.shoujiduoduo.util.c.b$c
      com.shoujiduoduo.util.c.b.a(com.shoujiduoduo.util.c.b, boolean):boolean
      com.shoujiduoduo.util.c.b.a(android.app.Activity, com.shoujiduoduo.util.b.a):void
      com.shoujiduoduo.util.c.b.a(java.lang.String, com.shoujiduoduo.util.b.a):void
      com.shoujiduoduo.util.c.b.a(java.lang.String, boolean):com.shoujiduoduo.util.b.c$b */
    /* access modifiers changed from: private */
    public void k() {
        c.b bVar;
        a.a(f763a, "彩铃，查询个人铃音库, type:" + this.m.toString());
        if (b.a() == null || com.shoujiduoduo.util.c.a.a((Context) null) == null) {
            a.c(f763a, "main activity is destroyed");
            return;
        }
        if (!this.n.c() && as.a((Context) null, "NeedUpdateCaiLingLib", 1) == 0) {
            a.a(f763a, "RingList: cache is available! Use Cache!");
            e<RingData> a2 = this.n.a();
            if (a2 != null) {
                a.a(f763a, "RingList: Read RingList Cache Success!");
                this.s.sendMessage(this.s.obtainMessage(0, a2));
                return;
            }
        }
        a.a(f763a, "RingList: cache is out of date or cache failed!");
        com.shoujiduoduo.a.b.b.g().c();
        c.b a3 = b.a().a("", false);
        if (a3 == null) {
            a.e(f763a, "查询彩铃功能请求失败");
            this.s.sendEmptyMessage(1);
        } else if (a3.a().equals("000000")) {
            a.a(f763a, "彩铃功能已开通");
            if (this.m == f.a.list_ring_cmcc) {
                bVar = b.a().e();
            } else {
                bVar = null;
            }
            if (b.a() == null || com.shoujiduoduo.util.c.a.a((Context) null) == null) {
                a.c(f763a, "main activity is destroyed 2");
            } else if (bVar == null) {
                a.c(f763a, "查询失败，cRsp == null");
                this.s.sendEmptyMessage(1);
            } else if (bVar.a() != null && bVar.a().equals(GetUserInfoRsp.NON_MEM_ERROR_CODE)) {
                a.a(f763a, "查询失败，用户非彩铃用户，提示开通咪咕特级会员");
                x.a().b(com.shoujiduoduo.a.a.b.OBSERVER_CAILING, new u(this));
                this.s.sendEmptyMessage(1);
            } else if (bVar.a() == null || ((!bVar.a().equals("000000") && !bVar.a().equals("0000")) || !(bVar instanceof c.x))) {
                this.s.sendEmptyMessage(1);
                a.c(f763a, "查询失败，res：" + bVar.b());
            } else {
                a.a(f763a, "查询成功");
                List<c.ac> d2 = ((c.x) bVar).d();
                if (d2 == null || d2.size() == 0) {
                    a.a(f763a, "userToneInfo == null");
                    this.s.sendMessage(this.s.obtainMessage(0, null));
                    return;
                }
                e eVar = new e();
                ArrayList<T> arrayList = new ArrayList<>();
                for (int i2 = 0; i2 < d2.size(); i2++) {
                    RingData ringData = new RingData();
                    ringData.e = d2.get(i2).c();
                    ringData.f = d2.get(i2).d();
                    if (this.m == f.a.list_ring_cmcc) {
                        ringData.p = u.a(d2.get(i2).a(), 200);
                        ringData.n = d2.get(i2).b();
                        a.a(f763a, "用户铃音库：name:" + ringData.e + " 彩铃id：" + ringData.n);
                        String e2 = d2.get(i2).e();
                        try {
                            if (e2.indexOf(" ") > 0) {
                                ringData.o = e2.substring(0, e2.indexOf(" "));
                            } else {
                                ringData.o = e2;
                            }
                        } catch (Exception e3) {
                            ringData.o = "";
                            e3.printStackTrace();
                        }
                        ringData.q = 0;
                        if (com.shoujiduoduo.util.c.a.a((Context) null) == null) {
                            a.c(f763a, "main activity is destroyed 3");
                            return;
                        }
                        ringData.g = com.shoujiduoduo.util.c.a.a((Context) null).a(ringData.n);
                    }
                    arrayList.add(ringData);
                }
                eVar.f821a = arrayList;
                eVar.b = false;
                this.k = true;
                as.b((Context) null, "NeedUpdateCaiLingLib", 0);
                this.s.sendMessage(this.s.obtainMessage(0, eVar));
            }
        } else {
            x.a().b(com.shoujiduoduo.a.a.b.OBSERVER_CAILING, new t(this));
            this.s.sendEmptyMessage(1);
            a.e(f763a, "彩铃功能尚未开通");
        }
    }

    /* access modifiers changed from: private */
    public void l() {
        a.a(f763a, "彩铃，查询个人铃音库, type:" + this.m.toString());
        if (com.shoujiduoduo.util.e.a.a() == null || com.shoujiduoduo.util.c.a.a((Context) null) == null) {
            a.c(f763a, "main activity is destroyed");
            return;
        }
        if (!this.n.c() && as.a((Context) null, "NeedUpdateCaiLingLib", 1) == 0) {
            a.a(f763a, "RingList: cache is available! Use Cache!");
            e<RingData> a2 = this.n.a();
            if (a2 != null) {
                a.a(f763a, "RingList: Read RingList Cache Success!");
                this.s.sendMessage(this.s.obtainMessage(0, a2));
                return;
            }
        }
        a.a(f763a, "RingList: cache is out of date or cache failed!");
        c.b c2 = com.shoujiduoduo.util.e.a.a().c();
        if (c2 == null) {
            a.c(f763a, "查询失败，cRsp == null");
            this.s.sendEmptyMessage(1);
        } else if ((c2 instanceof c.s) && c2.a().equals("000000")) {
            a.a(f763a, "查询成功");
            c.z[] zVarArr = ((c.s) c2).d;
            if (zVarArr == null || zVarArr.length == 0) {
                a.a(f763a, "userToneInfo == null");
                this.s.sendMessage(this.s.obtainMessage(0, null));
                return;
            }
            e eVar = new e();
            ArrayList<T> arrayList = new ArrayList<>();
            for (int i2 = 0; i2 < zVarArr.length; i2++) {
                RingData ringData = new RingData();
                ringData.e = zVarArr[i2].b;
                ringData.f = zVarArr[i2].d;
                if (this.m == f.a.list_ring_cucc) {
                    ringData.A = zVarArr[i2].f1620a;
                    a.a(f763a, "用户铃音库：name:" + ringData.e + " 彩铃id：" + ringData.A);
                    String str = zVarArr[i2].g;
                    try {
                        if (str.indexOf(" ") > 0) {
                            ringData.B = str.substring(0, str.indexOf(" "));
                        } else {
                            ringData.B = str;
                        }
                    } catch (Exception e2) {
                        ringData.B = "";
                        e2.printStackTrace();
                    }
                    if (com.shoujiduoduo.util.c.a.a((Context) null) == null) {
                        a.c(f763a, "main activity is destroyed 3");
                        return;
                    }
                    ringData.g = com.shoujiduoduo.util.c.a.a((Context) null).a(ringData.A);
                }
                arrayList.add(ringData);
            }
            eVar.f821a = arrayList;
            eVar.b = false;
            this.k = true;
            as.b((Context) null, "NeedUpdateCaiLingLib", 0);
            this.s.sendMessage(this.s.obtainMessage(0, eVar));
        }
    }

    /* access modifiers changed from: private */
    public void m() {
        c.b bVar;
        a.a(f763a, "彩铃，查询个人铃音库, type:" + this.m.toString());
        if (com.shoujiduoduo.util.d.b.a() == null || com.shoujiduoduo.util.c.a.a((Context) null) == null) {
            a.c(f763a, "main activity is destroyed");
            return;
        }
        if (!this.n.c() && as.a((Context) null, "NeedUpdateCaiLingLib", 1) == 0) {
            a.a(f763a, "RingList: cache is available! Use Cache!");
            e<RingData> a2 = this.n.a();
            if (a2 != null) {
                a.a(f763a, "RingList: Read RingList Cache Success!");
                this.s.sendMessage(this.s.obtainMessage(0, a2));
                return;
            }
        }
        a.a(f763a, "RingList: cache is out of date or cache failed!");
        String a3 = as.a(RingDDApp.c(), "pref_phone_num", "");
        if (this.m == f.a.list_ring_ctcc) {
            bVar = com.shoujiduoduo.util.d.b.a().c(a3);
        } else {
            bVar = null;
        }
        if (com.shoujiduoduo.util.d.b.a() == null || com.shoujiduoduo.util.c.a.a((Context) null) == null) {
            a.c(f763a, "main activity is destroyed 2");
        } else if (bVar == null) {
            a.c(f763a, "查询失败，cRsp == null");
            this.s.sendEmptyMessage(1);
        } else if (bVar.a() == null || ((!bVar.a().equals("000000") && !bVar.a().equals("0000")) || !(bVar instanceof c.x))) {
            this.s.sendEmptyMessage(1);
            a.c(f763a, "查询失败，res：" + bVar.b());
        } else {
            a.a(f763a, "查询成功");
            List<c.ac> d2 = ((c.x) bVar).d();
            if (d2 == null || d2.size() == 0) {
                a.a(f763a, "userToneInfo == null");
                this.s.sendMessage(this.s.obtainMessage(0, null));
                return;
            }
            e eVar = new e();
            ArrayList<T> arrayList = new ArrayList<>();
            for (int i2 = 0; i2 < d2.size(); i2++) {
                RingData ringData = new RingData();
                ringData.e = d2.get(i2).c();
                ringData.f = d2.get(i2).d();
                if (this.m == f.a.list_ring_ctcc) {
                    ringData.s = d2.get(i2).b();
                    a.a(f763a, "用户铃音库：name:" + ringData.e + " 彩铃id：" + ringData.s);
                    String e2 = d2.get(i2).e();
                    try {
                        if (e2.indexOf(" ") > 0) {
                            ringData.t = e2.substring(0, e2.indexOf(" "));
                        } else {
                            ringData.t = e2;
                        }
                    } catch (Exception e3) {
                        ringData.t = "";
                        e3.printStackTrace();
                    }
                    ringData.v = 0;
                    if (com.shoujiduoduo.util.c.a.a((Context) null) == null) {
                        a.c(f763a, "main activity is destroyed 3");
                        return;
                    }
                    ringData.g = com.shoujiduoduo.util.c.a.a((Context) null).a(ringData.s);
                }
                arrayList.add(ringData);
            }
            eVar.f821a = arrayList;
            eVar.b = false;
            this.k = true;
            as.b((Context) null, "NeedUpdateCaiLingLib", 0);
            this.s.sendMessage(this.s.obtainMessage(0, eVar));
        }
    }

    /* access modifiers changed from: private */
    public void n() {
        int i2;
        switch (this.m) {
            case sys_alarm:
                i2 = 4;
                break;
            case sys_notify:
                i2 = 2;
                break;
            case sys_ringtone:
                i2 = 1;
                break;
            default:
                i2 = 0;
                break;
        }
        if (this.f == null) {
            this.f = new aw(RingDDApp.c());
        }
        List<aw.a> a2 = this.f.a(i2, true);
        if (a2 == null) {
            this.s.sendEmptyMessage(1);
            a.c(f763a, "get system ring error");
            return;
        }
        ArrayList<T> arrayList = new ArrayList<>();
        for (aw.a next : a2) {
            RingData ringData = new RingData();
            ringData.m = next.f1577a;
            ringData.e = next.b;
            ringData.j = next.c / Constants.CLEARIMGED == 0 ? 1 : next.c / Constants.CLEARIMGED;
            arrayList.add(ringData);
        }
        if (arrayList.size() > 0) {
            e eVar = new e();
            eVar.f821a = arrayList;
            this.s.sendMessage(this.s.obtainMessage(0, eVar));
            return;
        }
        this.s.sendEmptyMessage(1);
        a.c(f763a, "get system ring error");
    }

    private byte[] c(int i2) {
        switch (this.m) {
            case list_ring_recommon:
            case list_ring_category:
                if (!TextUtils.isEmpty(this.d)) {
                    if (this.c) {
                        return w.a(this.e, i2, 25, this.d, this.b);
                    }
                    return w.a(this.e, i2, 25, this.d);
                } else if (this.c) {
                    return w.a(this.e, i2, 25, this.b);
                } else {
                    return w.a(this.e, i2, 25);
                }
            case list_ring_search:
                return w.a(this.p, this.g, i2, 25);
            case list_ring_cmcc:
            case list_ring_ctcc:
            case list_ring_cucc:
            case sys_alarm:
            case sys_notify:
            case sys_ringtone:
            default:
                if (this.c) {
                    return w.a(this.e, i2, 25, this.b);
                }
                return w.a(this.e, i2, 25);
            case list_ring_collect:
                return w.a("&type=getlist&iscol=1", "&page=" + i2 + "&pagesize=" + 25 + "&listid=" + this.e);
            case list_ring_artist:
                return w.a("&type=getlist", "&page=" + i2 + "&pagesize=" + 25 + "&artistid=" + this.e);
        }
    }

    public boolean g() {
        return this.h;
    }

    public boolean d() {
        return this.i;
    }

    /* renamed from: b */
    public RingData a(int i2) {
        if (i2 < 0 || i2 >= this.r.size()) {
            return null;
        }
        return this.r.get(i2);
    }

    public int c() {
        return this.r.size();
    }

    public f.a b() {
        return this.m;
    }
}
