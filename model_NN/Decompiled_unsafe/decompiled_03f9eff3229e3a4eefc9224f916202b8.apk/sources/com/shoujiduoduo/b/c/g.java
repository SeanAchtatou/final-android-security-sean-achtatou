package com.shoujiduoduo.b.c;

import android.os.Handler;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.base.bean.b;
import com.shoujiduoduo.base.bean.c;
import com.shoujiduoduo.base.bean.e;
import com.shoujiduoduo.base.bean.f;
import com.shoujiduoduo.util.f;
import com.shoujiduoduo.util.i;
import com.shoujiduoduo.util.j;
import com.shoujiduoduo.util.w;
import java.io.ByteArrayInputStream;
import java.util.ArrayList;

/* compiled from: CollectList */
public class g implements c {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public l f759a = null;
    private String b;
    /* access modifiers changed from: private */
    public boolean c = true;
    /* access modifiers changed from: private */
    public boolean d = false;
    /* access modifiers changed from: private */
    public boolean e = false;
    /* access modifiers changed from: private */
    public boolean f = false;
    /* access modifiers changed from: private */
    public ArrayList<b> g = new ArrayList<>();
    private Handler h = new h(this);

    public g(String str) {
        this.f759a = new l("collect_" + str + ".tmp");
        this.b = str;
    }

    public String a() {
        return "collect";
    }

    public void e() {
        if (this.g == null || this.g.size() == 0) {
            this.d = true;
            this.e = false;
            i.a(new j(this));
        } else if (this.c) {
            this.d = true;
            this.e = false;
            i.a(new k(this));
        }
    }

    /* access modifiers changed from: private */
    public void h() {
        if (!this.f759a.a(4)) {
            a.a("CollectList", "CollectList: cache is available! Use Cache!");
            e<b> a2 = this.f759a.a();
            if (!(a2 == null || a2.f821a == null || a2.f821a.size() <= 0)) {
                a.a("CollectList", "RingList: Read RingList Cache Success!");
                this.h.sendMessage(this.h.obtainMessage(0, a2));
                return;
            }
        }
        a.a("CollectList", "RingList: cache is out of date or read cache failed!");
        byte[] c2 = c(0);
        if (c2 == null) {
            a.a("CollectList", "RingList: httpGetRingList Failed!");
            this.h.sendEmptyMessage(1);
            return;
        }
        e<b> b2 = j.b(new ByteArrayInputStream(c2));
        if (b2 != null) {
            a.a("CollectList", "list data size = " + b2.f821a.size());
            a.a("CollectList", "RingList: Read from network Success! send MESSAGE_SUCCESS_RETRIEVE_DATA");
            this.f = true;
            this.h.sendMessage(this.h.obtainMessage(0, b2));
            return;
        }
        a.a("CollectList", "RingList: Read from network Success, but parse FAILED! send MESSAGE_FAIL_RETRIEVE_DATA");
        this.h.sendEmptyMessage(1);
    }

    /* access modifiers changed from: private */
    public void i() {
        e<b> eVar;
        a.a("CollectList", "retrieving more data, list size = " + this.g.size());
        byte[] c2 = c(this.g.size() / 25);
        if (c2 == null) {
            this.h.sendEmptyMessage(2);
            return;
        }
        try {
            eVar = j.b(new ByteArrayInputStream(c2));
        } catch (ArrayIndexOutOfBoundsException e2) {
            eVar = null;
            f.c("parse ringlist error! ArrayIndexOutOfBoundsException. " + new String(c2));
        }
        if (eVar != null) {
            a.a("RingList", "list data size = " + eVar.f821a.size());
            this.f = true;
            this.h.sendMessage(this.h.obtainMessage(0, eVar));
            return;
        }
        this.h.sendEmptyMessage(2);
    }

    private byte[] c(int i) {
        StringBuilder sb = new StringBuilder();
        sb.append("&page=").append(i).append("&pagesize=").append(25).append("&listid=").append(this.b);
        return w.a("&type=getcollects", sb.toString());
    }

    public boolean g() {
        return this.c;
    }

    public boolean d() {
        return this.d;
    }

    /* renamed from: b */
    public b a(int i) {
        if (i < 0 || i >= this.g.size()) {
            return null;
        }
        return this.g.get(i);
    }

    public int c() {
        return this.g.size();
    }

    public f.a b() {
        return f.a.list_collect;
    }

    public void f() {
    }
}
