package com.shoujiduoduo.b.c;

import android.os.Handler;
import com.shoujiduoduo.base.bean.c;
import com.shoujiduoduo.base.bean.e;
import com.shoujiduoduo.base.bean.f;
import com.shoujiduoduo.util.f;
import com.shoujiduoduo.util.i;
import com.shoujiduoduo.util.j;
import com.shoujiduoduo.util.w;
import java.io.ByteArrayInputStream;
import java.util.ArrayList;

/* compiled from: ArtistList */
public class a implements c {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public f f754a = null;
    private String b;
    /* access modifiers changed from: private */
    public boolean c = true;
    /* access modifiers changed from: private */
    public boolean d = false;
    /* access modifiers changed from: private */
    public boolean e = false;
    /* access modifiers changed from: private */
    public boolean f = false;
    /* access modifiers changed from: private */
    public ArrayList<com.shoujiduoduo.base.bean.a> g = new ArrayList<>();
    private Handler h = new b(this);

    public a(String str) {
        this.f754a = new f("artist_" + str + ".tmp");
        this.b = str;
    }

    public String a() {
        return "artist";
    }

    public void e() {
        if (this.g == null || this.g.size() == 0) {
            this.d = true;
            this.e = false;
            i.a(new d(this));
        } else if (this.c) {
            this.d = true;
            this.e = false;
            i.a(new e(this));
        }
    }

    /* access modifiers changed from: private */
    public void h() {
        if (!this.f754a.a(4)) {
            com.shoujiduoduo.base.a.a.a("ArtistList", "CollectList: cache is available! Use Cache!");
            e<com.shoujiduoduo.base.bean.a> a2 = this.f754a.a();
            if (!(a2 == null || a2.f821a == null || a2.f821a.size() <= 0)) {
                com.shoujiduoduo.base.a.a.a("ArtistList", "RingList: Read RingList Cache Success!");
                this.h.sendMessage(this.h.obtainMessage(0, a2));
                return;
            }
        }
        com.shoujiduoduo.base.a.a.a("ArtistList", "RingList: cache is out of date or read cache failed!");
        byte[] c2 = c(0);
        if (c2 == null) {
            com.shoujiduoduo.base.a.a.a("ArtistList", "RingList: httpGetRingList Failed!");
            this.h.sendEmptyMessage(1);
            return;
        }
        e<com.shoujiduoduo.base.bean.a> a3 = j.a(new ByteArrayInputStream(c2));
        if (a3 != null) {
            com.shoujiduoduo.base.a.a.a("ArtistList", "list data size = " + a3.f821a.size());
            com.shoujiduoduo.base.a.a.a("ArtistList", "RingList: Read from network Success! send MESSAGE_SUCCESS_RETRIEVE_DATA");
            this.f = true;
            this.h.sendMessage(this.h.obtainMessage(0, a3));
            return;
        }
        com.shoujiduoduo.base.a.a.a("ArtistList", "RingList: Read from network Success, but parse FAILED! send MESSAGE_FAIL_RETRIEVE_DATA");
        this.h.sendEmptyMessage(1);
    }

    /* access modifiers changed from: private */
    public void i() {
        e<com.shoujiduoduo.base.bean.a> eVar;
        com.shoujiduoduo.base.a.a.a("ArtistList", "retrieving more data, list size = " + this.g.size());
        byte[] c2 = c(this.g.size() / 25);
        if (c2 == null) {
            this.h.sendEmptyMessage(2);
            return;
        }
        try {
            eVar = j.a(new ByteArrayInputStream(c2));
        } catch (ArrayIndexOutOfBoundsException e2) {
            eVar = null;
            f.c("parse ringlist error! ArrayIndexOutOfBoundsException. " + new String(c2));
        }
        if (eVar != null) {
            com.shoujiduoduo.base.a.a.a("RingList", "list data size = " + eVar.f821a.size());
            this.f = true;
            this.h.sendMessage(this.h.obtainMessage(0, eVar));
            return;
        }
        this.h.sendEmptyMessage(2);
    }

    private byte[] c(int i) {
        StringBuilder sb = new StringBuilder();
        sb.append("&page=").append(i).append("&pagesize=").append(25).append("&listid=").append(this.b);
        return w.a("&type=hotartist", sb.toString());
    }

    public boolean d() {
        return this.d;
    }

    /* renamed from: b */
    public com.shoujiduoduo.base.bean.a a(int i) {
        if (i < 0 || i >= this.g.size()) {
            return null;
        }
        return this.g.get(i);
    }

    public int c() {
        return this.g.size();
    }

    public boolean g() {
        return this.c;
    }

    public f.a b() {
        return f.a.list_artist;
    }

    public void f() {
    }
}
