package com.shoujiduoduo.b.e;

import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;

/* compiled from: UserInfoMgrImpl */
class f extends a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ e f784a;

    f(e eVar) {
        this.f784a = eVar;
    }

    public void a(c.b bVar) {
        super.a(bVar);
        if (bVar instanceof c.e) {
            c.e eVar = (c.e) bVar;
            if (eVar.e()) {
                com.shoujiduoduo.base.a.a.a("UserInfoMgrImpl", "联通vip 开通状态");
                if (this.f784a.b) {
                    this.f784a.c.a(3);
                }
            } else {
                com.shoujiduoduo.base.a.a.a("UserInfoMgrImpl", "联通vip 未开通");
                if (this.f784a.b) {
                    this.f784a.c.a(0);
                }
            }
            if (eVar.f1598a.a().equals("40307") || eVar.f1598a.a().equals("40308")) {
                com.shoujiduoduo.base.a.a.a("UserInfoMgrImpl", "token 失效");
                com.shoujiduoduo.util.e.a.a().a(this.f784a.f783a, "");
            }
        }
    }

    public void b(c.b bVar) {
        if (bVar.a().equals("40307") || bVar.a().equals("40308")) {
            com.shoujiduoduo.base.a.a.a("UserInfoMgrImpl", "token 失效");
            com.shoujiduoduo.util.e.a.a().a(this.f784a.f783a, "");
        }
        super.b(bVar);
    }
}
