package com.shoujiduoduo.b.e;

import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;

/* compiled from: UserInfoMgrImpl */
class e extends a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f783a;
    final /* synthetic */ boolean b;
    final /* synthetic */ b c;

    e(b bVar, String str, boolean z) {
        this.c = bVar;
        this.f783a = str;
        this.b = z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.util.e.a.a(boolean, java.lang.String, java.lang.String):void
     arg types: [int, java.lang.String, java.lang.String]
     candidates:
      com.shoujiduoduo.util.e.a.a(com.shoujiduoduo.util.e.a, java.lang.String, java.lang.String):com.shoujiduoduo.util.b.c$b
      com.shoujiduoduo.util.e.a.a(java.lang.String, com.shoujiduoduo.util.b.c$b, java.lang.String):void
      com.shoujiduoduo.util.e.a.a(java.lang.String, java.lang.String, java.lang.String):void
      com.shoujiduoduo.util.e.a.a(java.lang.String, java.lang.String, com.shoujiduoduo.util.b.a):void
      com.shoujiduoduo.util.e.a.a(boolean, java.lang.String, java.lang.String):void */
    public void a(c.b bVar) {
        super.a(bVar);
        if (bVar != null && (bVar instanceof c.af)) {
            c.af afVar = (c.af) bVar;
            com.shoujiduoduo.base.a.a.a("UserInfoMgrImpl", "user location, provinceid:" + afVar.f1592a + ", province name:" + afVar.d);
            if (com.shoujiduoduo.util.e.a.a().c(afVar.f1592a)) {
                com.shoujiduoduo.base.a.a.a("UserInfoMgrImpl", "in qualified area, support cucc");
                if (com.shoujiduoduo.util.e.a.a().a(this.f783a)) {
                    com.shoujiduoduo.base.a.a.a("UserInfoMgrImpl", "当前手机号有token， phone:" + this.f783a);
                    com.shoujiduoduo.util.e.a.a().f(new f(this));
                    return;
                }
                com.shoujiduoduo.base.a.a.a("UserInfoMgrImpl", "当前手机号没有token， 不做vip查询");
                return;
            }
            com.shoujiduoduo.util.e.a.a().a(false, "", "");
            com.shoujiduoduo.base.a.a.a("UserInfoMgrImpl", "not in qualified area, not support cucc");
        }
    }

    public void b(c.b bVar) {
        super.b(bVar);
        com.shoujiduoduo.base.a.a.a("UserInfoMgrImpl", "get UserLocation failed");
    }
}
