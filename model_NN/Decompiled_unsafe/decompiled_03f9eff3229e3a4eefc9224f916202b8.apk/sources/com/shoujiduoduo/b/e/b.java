package com.shoujiduoduo.b.e;

import android.text.TextUtils;
import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.a.c.u;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.base.bean.ab;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.util.as;
import com.shoujiduoduo.util.av;
import com.shoujiduoduo.util.f;
import com.shoujiduoduo.util.i;
import java.util.HashMap;

/* compiled from: UserInfoMgrImpl */
public class b implements a {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public ab f779a = new ab();
    private u b = new g(this);

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.b.e.b.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.shoujiduoduo.b.e.b.a(com.shoujiduoduo.b.e.b, int):void
      com.shoujiduoduo.b.e.b.a(java.lang.String, boolean):void */
    public void a() {
        int a2 = as.a(RingDDApp.c(), "user_loginStatus", 0);
        if (a2 != 0) {
            a.a("UserInfoMgrImpl", "user in login status， 加载登录信息");
            String a3 = as.a(RingDDApp.c(), "user_name", "");
            a.b("UserInfoMgrImpl", "user_name:" + a3);
            String a4 = as.a(RingDDApp.c(), "user_headpic", "");
            a.b("UserInfoMgrImpl", "user_headpic:" + a4);
            String a5 = as.a(RingDDApp.c(), "user_uid", "");
            a.b("UserInfoMgrImpl", "user_uid:" + a5);
            int a6 = as.a(RingDDApp.c(), "user_loginType", 0);
            a.b("UserInfoMgrImpl", "user_loginType:" + a6);
            int a7 = as.a(RingDDApp.c(), "user_vip_type", 0);
            a.b("UserInfoMgrImpl", "user_vip_type:" + a7);
            String a8 = as.a(RingDDApp.c(), "user_phone_num", "");
            a.b("UserInfoMgrImpl", "user_phone_num:" + a8);
            this.f779a.a(a5);
            this.f779a.c(a4);
            this.f779a.b(a3);
            this.f779a.a(a6);
            this.f779a.c(a2);
            this.f779a.b(a7);
            this.f779a.d(a8);
            if (!TextUtils.isEmpty(a8)) {
                c(a8, true);
            } else {
                i();
            }
            if (!TextUtils.isEmpty(a5)) {
                String str = "unknown";
                if (a5.indexOf("_") > 0) {
                    str = a5.substring(0, a5.indexOf("_"));
                }
                HashMap hashMap = new HashMap();
                hashMap.put("platform", str);
                com.umeng.analytics.b.a(RingDDApp.c(), "USER_ACCOUNT_INFO", hashMap);
            }
        } else {
            a.a("UserInfoMgrImpl", "user is not in  login status");
            String a9 = as.a(RingDDApp.c(), "user_phone_num", "");
            a.b("UserInfoMgrImpl", "user_phone_num:" + a9);
            this.f779a.d(a9);
            a(a9, false);
        }
        x.a().a(com.shoujiduoduo.a.a.b.OBSERVER_USER_CENTER, this.b);
    }

    public void b() {
        x.a().b(com.shoujiduoduo.a.a.b.OBSERVER_USER_CENTER, this.b);
    }

    private void a(String str, boolean z) {
        if (!TextUtils.isEmpty(str)) {
            c(str, z);
        } else if (f.w()) {
            a.a("UserInfoMgrImpl", "can show cu cailing");
            if (f.e() && !f.f()) {
                com.shoujiduoduo.util.e.a.a().a(new c(this, z));
            }
        } else {
            a.a("UserInfoMgrImpl", "unknown cailing type");
        }
    }

    /* access modifiers changed from: private */
    public void b(String str, boolean z) {
        com.shoujiduoduo.util.e.a.a().i(str, new e(this, str, z));
    }

    /* access modifiers changed from: private */
    public void i() {
        String f = com.shoujiduoduo.a.b.b.g().f();
        if (!av.c(f)) {
            i.a(new h(this, f));
        }
    }

    /* access modifiers changed from: private */
    public void c(String str, boolean z) {
        switch (f.g(str)) {
            case f1671a:
                return;
            case cu:
                b(str, z);
                return;
            case ct:
                com.shoujiduoduo.util.d.b.a().a(str, new j(this, z));
                return;
            default:
                a.e("UserInfoMgrImpl", "unknown phone type");
                return;
        }
    }

    /* access modifiers changed from: private */
    public void a(int i) {
        this.f779a.b(i);
        as.b(RingDDApp.c(), "user_vip_type", i);
        x.a().a(com.shoujiduoduo.a.a.b.OBSERVER_VIP, new k(this, i));
    }

    public ab c() {
        return this.f779a;
    }

    public void a(ab abVar) {
        this.f779a = abVar;
        as.c(RingDDApp.c(), "user_name", this.f779a.b());
        as.c(RingDDApp.c(), "user_headpic", this.f779a.c());
        as.c(RingDDApp.c(), "user_uid", this.f779a.a());
        as.b(RingDDApp.c(), "user_loginType", this.f779a.e());
        as.b(RingDDApp.c(), "user_loginStatus", this.f779a.h());
        as.b(RingDDApp.c(), "user_vip_type", this.f779a.g());
        as.c(RingDDApp.c(), "user_phone_num", this.f779a.k());
        x.a().a(com.shoujiduoduo.a.a.b.OBSERVER_USER_CENTER, new l(this, abVar));
    }

    public String f() {
        return this.f779a.a();
    }

    public boolean g() {
        return this.f779a.i();
    }

    public boolean h() {
        return this.f779a.j();
    }

    public int d() {
        return this.f779a.e();
    }

    public int e() {
        return this.f779a.g();
    }
}
