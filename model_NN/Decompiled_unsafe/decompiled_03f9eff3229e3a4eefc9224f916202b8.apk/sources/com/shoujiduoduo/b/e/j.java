package com.shoujiduoduo.b.e;

import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;

/* compiled from: UserInfoMgrImpl */
class j extends a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ boolean f787a;
    final /* synthetic */ b b;

    j(b bVar, boolean z) {
        this.b = bVar;
        this.f787a = z;
    }

    public void a(c.b bVar) {
        super.a(bVar);
        com.shoujiduoduo.base.a.a.a("UserInfoMgrImpl", "查询会员状态成功");
        if (bVar != null && (bVar instanceof c.d)) {
            c.d dVar = (c.d) bVar;
            com.shoujiduoduo.base.a.a.a("UserInfoMgrImpl", "code:" + dVar.a() + " msg:" + dVar.b());
            int i = (dVar.e() || dVar.f()) ? 2 : 0;
            if (this.f787a) {
                this.b.a(i);
            }
        }
    }

    public void b(c.b bVar) {
        super.b(bVar);
        com.shoujiduoduo.base.a.a.c("UserInfoMgrImpl", "查询会员状态失败, code:" + bVar.a() + ", msg:" + bVar.b());
        if (this.f787a) {
            this.b.a(0);
        }
    }
}
