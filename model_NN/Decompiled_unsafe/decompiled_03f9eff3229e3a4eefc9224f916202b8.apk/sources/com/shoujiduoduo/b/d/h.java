package com.shoujiduoduo.b.d;

import android.text.TextUtils;
import com.shoujiduoduo.util.i;
import com.shoujiduoduo.util.s;
import com.shoujiduoduo.util.t;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONTokener;

/* compiled from: SearchSuggestData */
public class h {

    /* compiled from: SearchSuggestData */
    public interface b {
        void a(String str, String[] strArr);
    }

    /* compiled from: SearchSuggestData */
    private static class a extends s<String> {
        public a(String str) {
            super(str);
        }

        public void a(String str) {
            t.b(c + this.b, str);
        }

        /* JADX WARNING: Removed duplicated region for block: B:24:0x0049 A[SYNTHETIC, Splitter:B:24:0x0049] */
        /* JADX WARNING: Removed duplicated region for block: B:34:0x005a A[SYNTHETIC, Splitter:B:34:0x005a] */
        /* JADX WARNING: Removed duplicated region for block: B:41:0x0067 A[SYNTHETIC, Splitter:B:41:0x0067] */
        /* JADX WARNING: Unknown top exception splitter block from list: {B:21:0x0044=Splitter:B:21:0x0044, B:31:0x0055=Splitter:B:31:0x0055} */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.lang.String a() {
            /*
                r6 = this;
                r1 = 0
                java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x0042, IOException -> 0x0053, all -> 0x0064 }
                java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x0042, IOException -> 0x0053, all -> 0x0064 }
                r0.<init>()     // Catch:{ FileNotFoundException -> 0x0042, IOException -> 0x0053, all -> 0x0064 }
                java.lang.String r3 = com.shoujiduoduo.b.d.h.a.c     // Catch:{ FileNotFoundException -> 0x0042, IOException -> 0x0053, all -> 0x0064 }
                java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ FileNotFoundException -> 0x0042, IOException -> 0x0053, all -> 0x0064 }
                java.lang.String r3 = r6.b     // Catch:{ FileNotFoundException -> 0x0042, IOException -> 0x0053, all -> 0x0064 }
                java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ FileNotFoundException -> 0x0042, IOException -> 0x0053, all -> 0x0064 }
                java.lang.String r0 = r0.toString()     // Catch:{ FileNotFoundException -> 0x0042, IOException -> 0x0053, all -> 0x0064 }
                r2.<init>(r0)     // Catch:{ FileNotFoundException -> 0x0042, IOException -> 0x0053, all -> 0x0064 }
                r0 = 1024(0x400, float:1.435E-42)
                byte[] r3 = new byte[r0]     // Catch:{ FileNotFoundException -> 0x0075, IOException -> 0x0073 }
                int r4 = r2.read(r3)     // Catch:{ FileNotFoundException -> 0x0075, IOException -> 0x0073 }
                if (r4 <= 0) goto L_0x0036
                java.lang.String r0 = new java.lang.String     // Catch:{ FileNotFoundException -> 0x0075, IOException -> 0x0073 }
                r5 = 0
                r0.<init>(r3, r5, r4)     // Catch:{ FileNotFoundException -> 0x0075, IOException -> 0x0073 }
                if (r2 == 0) goto L_0x0030
                r2.close()     // Catch:{ IOException -> 0x0031 }
            L_0x0030:
                return r0
            L_0x0031:
                r1 = move-exception
                r1.printStackTrace()
                goto L_0x0030
            L_0x0036:
                if (r2 == 0) goto L_0x003b
                r2.close()     // Catch:{ IOException -> 0x003d }
            L_0x003b:
                r0 = r1
                goto L_0x0030
            L_0x003d:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x003b
            L_0x0042:
                r0 = move-exception
                r2 = r1
            L_0x0044:
                r0.printStackTrace()     // Catch:{ all -> 0x0070 }
                if (r2 == 0) goto L_0x004c
                r2.close()     // Catch:{ IOException -> 0x004e }
            L_0x004c:
                r0 = r1
                goto L_0x0030
            L_0x004e:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x004c
            L_0x0053:
                r0 = move-exception
                r2 = r1
            L_0x0055:
                r0.printStackTrace()     // Catch:{ all -> 0x0070 }
                if (r2 == 0) goto L_0x005d
                r2.close()     // Catch:{ IOException -> 0x005f }
            L_0x005d:
                r0 = r1
                goto L_0x0030
            L_0x005f:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x005d
            L_0x0064:
                r0 = move-exception
            L_0x0065:
                if (r1 == 0) goto L_0x006a
                r1.close()     // Catch:{ IOException -> 0x006b }
            L_0x006a:
                throw r0
            L_0x006b:
                r1 = move-exception
                r1.printStackTrace()
                goto L_0x006a
            L_0x0070:
                r0 = move-exception
                r1 = r2
                goto L_0x0065
            L_0x0073:
                r0 = move-exception
                goto L_0x0055
            L_0x0075:
                r0 = move-exception
                goto L_0x0044
            */
            throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.b.d.h.a.a():java.lang.String");
        }
    }

    public void a(String str, b bVar) {
        i iVar = new i(this, bVar, str);
        a aVar = new a("suggest_" + str.hashCode() + ".tmp");
        if (!aVar.a(24)) {
            com.shoujiduoduo.base.a.a.a("SearchSuggestData", "缓存未过期，使用缓存数据, key:" + str);
            iVar.sendMessage(iVar.obtainMessage(11, a(aVar.a())));
            return;
        }
        com.shoujiduoduo.base.a.a.a("SearchSuggestData", "缓存过期或者尚未缓存，请求数据, key:" + str);
        i.a(new j(this, str, aVar, iVar));
    }

    /* access modifiers changed from: private */
    public String[] a(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            JSONArray jSONArray = (JSONArray) new JSONTokener(str).nextValue();
            if (jSONArray != null && jSONArray.length() > 0) {
                String[] strArr = new String[jSONArray.length()];
                for (int i = 0; i < jSONArray.length(); i++) {
                    strArr[i] = jSONArray.optString(i);
                }
                return strArr;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return null;
    }
}
