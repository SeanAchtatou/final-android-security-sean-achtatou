package com.shoujiduoduo.b.d;

import android.os.Handler;
import android.text.TextUtils;
import com.shoujiduoduo.b.d.h;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.util.w;

/* compiled from: SearchSuggestData */
class j implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f778a;
    final /* synthetic */ h.a b;
    final /* synthetic */ Handler c;
    final /* synthetic */ h d;

    j(h hVar, String str, h.a aVar, Handler handler) {
        this.d = hVar;
        this.f778a = str;
        this.b = aVar;
        this.c = handler;
    }

    public void run() {
        String a2 = w.a(this.f778a);
        if (!TextUtils.isEmpty(a2)) {
            this.b.a(a2);
            this.c.sendMessage(this.c.obtainMessage(11, this.d.a(a2)));
            return;
        }
        a.e("SearchSuggestData", "no suggest data, key:" + this.f778a);
    }
}
