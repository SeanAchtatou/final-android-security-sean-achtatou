package com.shoujiduoduo.b.d;

import android.util.Xml;
import com.shoujiduoduo.a.a.b;
import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.util.f;
import com.shoujiduoduo.util.i;
import com.shoujiduoduo.util.s;
import com.shoujiduoduo.util.t;
import com.shoujiduoduo.util.w;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xmlpull.v1.XmlSerializer;

/* compiled from: HotKeyword */
public class a {

    /* renamed from: a  reason: collision with root package name */
    private static final String f772a = a.class.getSimpleName();
    private ArrayList<e> b;
    /* access modifiers changed from: private */
    public C0020a c = new C0020a("hotkey.tmp");
    private boolean d;

    /* access modifiers changed from: private */
    public ArrayList<e> a(InputStream inputStream) {
        try {
            Document parse = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(inputStream);
            if (parse == null) {
                com.shoujiduoduo.base.a.a.a(f772a, "parse error");
                return null;
            }
            Element documentElement = parse.getDocumentElement();
            if (documentElement == null) {
                com.shoujiduoduo.base.a.a.a(f772a, "cannot find root node");
                return null;
            }
            NodeList elementsByTagName = documentElement.getElementsByTagName("key");
            if (elementsByTagName == null) {
                com.shoujiduoduo.base.a.a.a(f772a, "cannot find node named \"key\"");
                return null;
            }
            ArrayList<e> arrayList = new ArrayList<>();
            for (int i = 0; i < elementsByTagName.getLength(); i++) {
                NamedNodeMap attributes = elementsByTagName.item(i).getAttributes();
                e eVar = new e();
                eVar.f775a = f.a(attributes, "txt");
                eVar.b = f.a(attributes, "new", 0);
                eVar.c = f.a(attributes, "trend", 0);
                arrayList.add(eVar);
            }
            return arrayList;
        } catch (IOException e) {
            com.shoujiduoduo.base.a.a.a(e);
            return null;
        } catch (SAXException e2) {
            com.shoujiduoduo.base.a.a.a(e2);
            return null;
        } catch (ParserConfigurationException e3) {
            com.shoujiduoduo.base.a.a.a(e3);
            return null;
        } catch (ArrayIndexOutOfBoundsException e4) {
            com.shoujiduoduo.base.a.a.a(e4);
            return null;
        } catch (DOMException e5) {
            com.shoujiduoduo.base.a.a.a(e5);
            return null;
        }
    }

    /* access modifiers changed from: private */
    public boolean d() {
        byte[] k = w.k();
        if (k != null) {
            this.b = a(new ByteArrayInputStream(k));
            if (this.b != null && this.b.size() > 0) {
                com.shoujiduoduo.base.a.a.a(f772a, this.b.size() + " keywords.");
                this.c.a(this.b);
                this.d = true;
                x.a().a(b.OBSERVER_SEARCH_HOT_WORD, new b(this));
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    public boolean e() {
        this.b = this.c.a();
        if (this.b == null || this.b.size() <= 0) {
            com.shoujiduoduo.base.a.a.e(f772a, "cache is not valid");
            return false;
        }
        com.shoujiduoduo.base.a.a.a(f772a, this.b.size() + " keywords. read from cache.");
        this.d = true;
        x.a().a(b.OBSERVER_SEARCH_HOT_WORD, new c(this));
        return true;
    }

    public void a() {
        if (this.b == null) {
            i.a(new d(this));
        }
    }

    public boolean b() {
        return this.d;
    }

    public ArrayList<e> c() {
        return this.b;
    }

    /* renamed from: com.shoujiduoduo.b.d.a$a  reason: collision with other inner class name */
    /* compiled from: HotKeyword */
    class C0020a extends s<ArrayList<e>> {
        C0020a(String str) {
            super(str);
        }

        public ArrayList<e> a() {
            try {
                return a.this.a(new FileInputStream(c + this.b));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                return null;
            }
        }

        public void a(ArrayList<e> arrayList) {
            if (arrayList != null) {
                XmlSerializer newSerializer = Xml.newSerializer();
                StringWriter stringWriter = new StringWriter();
                try {
                    newSerializer.setOutput(stringWriter);
                    newSerializer.startDocument("UTF-8", true);
                    newSerializer.startTag("", "root");
                    newSerializer.attribute("", "num", String.valueOf(arrayList.size()));
                    for (int i = 0; i < arrayList.size(); i++) {
                        e eVar = arrayList.get(i);
                        newSerializer.startTag("", "key");
                        newSerializer.attribute("", "txt", eVar.f775a);
                        newSerializer.attribute("", "new", "" + eVar.b);
                        newSerializer.attribute("", "trend", "" + eVar.c);
                        newSerializer.endTag("", "key");
                    }
                    newSerializer.endTag("", "root");
                    newSerializer.endDocument();
                    t.b(c + this.b, stringWriter.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
