package com.shoujiduoduo.b.d;

import android.os.Handler;
import android.os.Message;
import com.shoujiduoduo.b.d.h;

/* compiled from: SearchSuggestData */
class i extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ h.b f777a;
    final /* synthetic */ String b;
    final /* synthetic */ h c;

    i(h hVar, h.b bVar, String str) {
        this.c = hVar;
        this.f777a = bVar;
        this.b = str;
    }

    public void handleMessage(Message message) {
        this.f777a.a(this.b, (String[]) message.obj);
    }
}
