package com.shoujiduoduo.b.f;

import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.base.bean.e;
import com.shoujiduoduo.util.j;
import com.shoujiduoduo.util.l;
import com.shoujiduoduo.util.t;
import com.shoujiduoduo.util.w;
import java.io.ByteArrayInputStream;

/* compiled from: FavoriteRingList */
class m implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f804a;
    final /* synthetic */ g b;

    m(g gVar, String str) {
        this.b = gVar;
        this.f804a = str;
    }

    public void run() {
        a.a(g.f800a, "get user online ring  begin");
        byte[] h = w.h();
        if (h != null) {
            a.a(g.f800a, "save online ring to cache");
            t.a(l.a(2) + this.f804a + ".xml", h);
            try {
                e<RingData> c = j.c(new ByteArrayInputStream(h));
                if (c != null) {
                    synchronized (g.f800a) {
                        a.a(g.f800a, "merge user local ring with online data");
                        if (c.f821a.size() > 0) {
                            a.a(g.f800a, "online ring num: " + c.f821a.size());
                            x.a().a(new n(this, this.b.a(c.f821a, this.b.g, false)));
                        } else {
                            a.a(g.f800a, "online ring num: 0");
                        }
                    }
                    return;
                }
                a.c(g.f800a, "Get user favorite rings failed!!");
            } catch (ArrayIndexOutOfBoundsException e) {
                a.c(g.f800a, "Get user favorite rings failed!!, crash");
                e.printStackTrace();
            }
        } else {
            a.a(g.f800a, "get user online ring error, return null");
        }
    }
}
