package com.shoujiduoduo.b.f;

import com.shoujiduoduo.a.a.b;
import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.base.bean.MakeRingData;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.base.bean.c;
import java.util.Collection;

/* compiled from: UserRingListMgrImpl */
public class ap implements u {

    /* renamed from: a  reason: collision with root package name */
    private boolean f796a;
    private boolean b;
    private g c;
    private a d;
    private v e;

    public void a() {
        this.c = new g();
        this.e = new v();
        this.d = new a();
        this.c.j();
        this.e.j();
        this.f796a = true;
        this.b = false;
        x.a().a(b.OBSERVER_USER_RING, new aq(this));
        e();
    }

    public void b() {
        this.c.k();
        this.e.k();
    }

    private void e() {
        this.c.i();
        this.e.i();
        this.d.i();
        this.f796a = false;
        this.b = true;
    }

    public boolean c() {
        return this.c.h() && this.e.h() && this.d.h();
    }

    public boolean a(RingData ringData, String str) {
        if (str.equals("favorite_ring_list")) {
            return this.c.a(ringData);
        }
        if (str.equals("make_ring_list")) {
            if (ringData instanceof MakeRingData) {
                return this.e.a((MakeRingData) ringData);
            }
            a.c("UserRingListMgrImpl", "add make ring, but data is not makeringdata type");
        }
        return false;
    }

    public boolean a(String str, String str2) {
        if (str2.equals("favorite_ring_list")) {
            return this.c.a(str);
        }
        if (str2.equals("make_ring_list")) {
            return this.e.a(str);
        }
        return false;
    }

    public boolean a(String str, int i) {
        if (str.equals("favorite_ring_list")) {
            return this.c.b(i);
        }
        if (str.equals("make_ring_list")) {
            return this.e.b(i);
        }
        return false;
    }

    public c a(String str) {
        if (str.equals("favorite_ring_list")) {
            return this.c;
        }
        if (str.equals("make_ring_list")) {
            return this.e;
        }
        if (str.equals("collect_ring_list")) {
            return this.d;
        }
        return null;
    }

    public boolean a(com.shoujiduoduo.base.bean.b bVar) {
        return this.d.a(bVar);
    }

    public c d() {
        return this.d;
    }

    public boolean a(String str, Collection<Integer> collection) {
        if (str.equals("make_ring_list")) {
            return this.e.a(collection);
        }
        if (str.equals("favorite_ring_list")) {
            return this.c.a(collection);
        }
        return false;
    }

    public boolean a(Collection<Integer> collection) {
        return this.d.a(collection);
    }
}
