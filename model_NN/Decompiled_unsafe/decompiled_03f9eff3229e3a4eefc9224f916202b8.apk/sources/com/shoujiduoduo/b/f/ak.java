package com.shoujiduoduo.b.f;

import android.text.TextUtils;
import com.qq.e.comm.constants.Constants;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.base.bean.MakeRingData;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.util.c;
import com.shoujiduoduo.util.t;
import com.shoujiduoduo.util.w;
import com.umeng.analytics.b;
import java.util.HashMap;
import java.util.Timer;

/* compiled from: RingUploader */
class ak implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ MakeRingData f794a;
    final /* synthetic */ int b;
    final /* synthetic */ ai c;

    ak(ai aiVar, MakeRingData makeRingData, int i) {
        this.c = aiVar;
        this.f794a = makeRingData;
        this.b = i;
    }

    public void run() {
        int unused = this.c.b = 0;
        Timer unused2 = this.c.f793a = new Timer();
        this.c.f793a.schedule(new al(this), 0, 600);
        HashMap hashMap = new HashMap();
        if (this.f794a.g.equals("")) {
            String f = w.f();
            if (TextUtils.isEmpty(f) || f.equals("0")) {
                a.c("RingUploader", "genrid error");
                this.c.b(this.f794a);
                hashMap.put(Constants.KEYS.RET, "genrid error");
                b.a(RingDDApp.c(), "USER_RING_UPLOAD", hashMap);
                return;
            }
            a.a("RingUploader", "genrid,success. rid:" + f);
            this.f794a.g = f;
        }
        String str = this.f794a.m;
        if (!c.a(str, this.f794a.g + "." + t.b(str), this.f794a.d)) {
            a.c("RingUploader", "bcs 上传失败");
            this.c.b(this.f794a);
            hashMap.put(Constants.KEYS.RET, "bcs upload error");
            b.a(RingDDApp.c(), "USER_RING_UPLOAD", hashMap);
            return;
        }
        a.a("RingUploader", "bcs 上传成功");
        if (w.a(this.f794a, String.valueOf(this.b))) {
            a.a("RingUploader", "上传后的铃声数据通知服务器。 成功");
            this.c.a(this.f794a);
            hashMap.put(Constants.KEYS.RET, "success");
            b.a(RingDDApp.c(), "USER_RING_UPLOAD", hashMap);
            return;
        }
        a.a("RingUploader", "上传后的铃声数据通知服务器。 失败");
        this.c.b(this.f794a);
        hashMap.put(Constants.KEYS.RET, "upload inform error");
        b.a(RingDDApp.c(), "USER_RING_UPLOAD", hashMap);
    }
}
