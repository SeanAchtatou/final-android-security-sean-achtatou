package com.shoujiduoduo.b.f;

import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.a.c.u;
import com.shoujiduoduo.base.bean.b;
import com.shoujiduoduo.base.bean.c;
import com.shoujiduoduo.base.bean.f;
import com.shoujiduoduo.util.i;
import com.shoujiduoduo.util.l;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/* compiled from: CollectRingList */
public class a implements c {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public String f788a = l.b(4);
    /* access modifiers changed from: private */
    public ArrayList<b> b = new ArrayList<>();
    /* access modifiers changed from: private */
    public boolean c;
    private final byte[] d = new byte[0];
    private u e = new d(this);

    public boolean h() {
        return this.c;
    }

    public String a() {
        return "user_collect";
    }

    public void i() {
        i.a(new b(this));
    }

    public boolean a(b bVar) {
        synchronized (this.d) {
            Iterator<b> it = this.b.iterator();
            while (true) {
                if (it.hasNext()) {
                    if (it.next().g.equals(bVar.g)) {
                        break;
                    }
                } else {
                    this.b.add(bVar);
                    j();
                    k();
                    break;
                }
            }
        }
        return true;
    }

    public boolean a(Collection<Integer> collection) {
        if (collection == null || collection.size() == 0) {
            return false;
        }
        synchronized (this.d) {
            ArrayList arrayList = new ArrayList();
            for (Integer intValue : collection) {
                arrayList.add(this.b.get(intValue.intValue()));
            }
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                this.b.remove((b) it.next());
            }
            j();
            k();
        }
        return true;
    }

    private void j() {
        x.a().a(com.shoujiduoduo.a.a.b.OBSERVER_LIST_DATA, new e(this));
    }

    private void k() {
        i.a(new f(this));
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean l() {
        /*
            r9 = this;
            r1 = 0
            byte[] r4 = r9.d
            monitor-enter(r4)
            javax.xml.parsers.DocumentBuilderFactory r0 = javax.xml.parsers.DocumentBuilderFactory.newInstance()     // Catch:{ ParserConfigurationException -> 0x00c9, TransformerConfigurationException -> 0x00d0, FileNotFoundException -> 0x00d8, TransformerException -> 0x00dd, Exception -> 0x00e2 }
            javax.xml.parsers.DocumentBuilder r0 = r0.newDocumentBuilder()     // Catch:{ ParserConfigurationException -> 0x00c9, TransformerConfigurationException -> 0x00d0, FileNotFoundException -> 0x00d8, TransformerException -> 0x00dd, Exception -> 0x00e2 }
            org.w3c.dom.Document r5 = r0.newDocument()     // Catch:{ ParserConfigurationException -> 0x00c9, TransformerConfigurationException -> 0x00d0, FileNotFoundException -> 0x00d8, TransformerException -> 0x00dd, Exception -> 0x00e2 }
            java.lang.String r0 = "list"
            org.w3c.dom.Element r6 = r5.createElement(r0)     // Catch:{ ParserConfigurationException -> 0x00c9, TransformerConfigurationException -> 0x00d0, FileNotFoundException -> 0x00d8, TransformerException -> 0x00dd, Exception -> 0x00e2 }
            java.lang.String r0 = "num"
            java.util.ArrayList<com.shoujiduoduo.base.bean.b> r2 = r9.b     // Catch:{ ParserConfigurationException -> 0x00c9, TransformerConfigurationException -> 0x00d0, FileNotFoundException -> 0x00d8, TransformerException -> 0x00dd, Exception -> 0x00e2 }
            int r2 = r2.size()     // Catch:{ ParserConfigurationException -> 0x00c9, TransformerConfigurationException -> 0x00d0, FileNotFoundException -> 0x00d8, TransformerException -> 0x00dd, Exception -> 0x00e2 }
            java.lang.String r2 = java.lang.String.valueOf(r2)     // Catch:{ ParserConfigurationException -> 0x00c9, TransformerConfigurationException -> 0x00d0, FileNotFoundException -> 0x00d8, TransformerException -> 0x00dd, Exception -> 0x00e2 }
            r6.setAttribute(r0, r2)     // Catch:{ ParserConfigurationException -> 0x00c9, TransformerConfigurationException -> 0x00d0, FileNotFoundException -> 0x00d8, TransformerException -> 0x00dd, Exception -> 0x00e2 }
            r5.appendChild(r6)     // Catch:{ ParserConfigurationException -> 0x00c9, TransformerConfigurationException -> 0x00d0, FileNotFoundException -> 0x00d8, TransformerException -> 0x00dd, Exception -> 0x00e2 }
            r3 = r1
        L_0x0029:
            java.util.ArrayList<com.shoujiduoduo.base.bean.b> r0 = r9.b     // Catch:{ ParserConfigurationException -> 0x00c9, TransformerConfigurationException -> 0x00d0, FileNotFoundException -> 0x00d8, TransformerException -> 0x00dd, Exception -> 0x00e2 }
            int r0 = r0.size()     // Catch:{ ParserConfigurationException -> 0x00c9, TransformerConfigurationException -> 0x00d0, FileNotFoundException -> 0x00d8, TransformerException -> 0x00dd, Exception -> 0x00e2 }
            if (r3 >= r0) goto L_0x0085
            java.util.ArrayList<com.shoujiduoduo.base.bean.b> r0 = r9.b     // Catch:{ ParserConfigurationException -> 0x00c9, TransformerConfigurationException -> 0x00d0, FileNotFoundException -> 0x00d8, TransformerException -> 0x00dd, Exception -> 0x00e2 }
            java.lang.Object r0 = r0.get(r3)     // Catch:{ ParserConfigurationException -> 0x00c9, TransformerConfigurationException -> 0x00d0, FileNotFoundException -> 0x00d8, TransformerException -> 0x00dd, Exception -> 0x00e2 }
            com.shoujiduoduo.base.bean.b r0 = (com.shoujiduoduo.base.bean.b) r0     // Catch:{ ParserConfigurationException -> 0x00c9, TransformerConfigurationException -> 0x00d0, FileNotFoundException -> 0x00d8, TransformerException -> 0x00dd, Exception -> 0x00e2 }
            java.lang.String r2 = "collect"
            org.w3c.dom.Element r7 = r5.createElement(r2)     // Catch:{ ParserConfigurationException -> 0x00c9, TransformerConfigurationException -> 0x00d0, FileNotFoundException -> 0x00d8, TransformerException -> 0x00dd, Exception -> 0x00e2 }
            java.lang.String r2 = "title"
            java.lang.String r8 = r0.b     // Catch:{ ParserConfigurationException -> 0x00c9, TransformerConfigurationException -> 0x00d0, FileNotFoundException -> 0x00d8, TransformerException -> 0x00dd, Exception -> 0x00e2 }
            r7.setAttribute(r2, r8)     // Catch:{ ParserConfigurationException -> 0x00c9, TransformerConfigurationException -> 0x00d0, FileNotFoundException -> 0x00d8, TransformerException -> 0x00dd, Exception -> 0x00e2 }
            java.lang.String r2 = "content"
            java.lang.String r8 = r0.c     // Catch:{ ParserConfigurationException -> 0x00c9, TransformerConfigurationException -> 0x00d0, FileNotFoundException -> 0x00d8, TransformerException -> 0x00dd, Exception -> 0x00e2 }
            r7.setAttribute(r2, r8)     // Catch:{ ParserConfigurationException -> 0x00c9, TransformerConfigurationException -> 0x00d0, FileNotFoundException -> 0x00d8, TransformerException -> 0x00dd, Exception -> 0x00e2 }
            java.lang.String r2 = "artist"
            java.lang.String r8 = r0.h     // Catch:{ ParserConfigurationException -> 0x00c9, TransformerConfigurationException -> 0x00d0, FileNotFoundException -> 0x00d8, TransformerException -> 0x00dd, Exception -> 0x00e2 }
            r7.setAttribute(r2, r8)     // Catch:{ ParserConfigurationException -> 0x00c9, TransformerConfigurationException -> 0x00d0, FileNotFoundException -> 0x00d8, TransformerException -> 0x00dd, Exception -> 0x00e2 }
            java.lang.String r2 = "time"
            java.lang.String r8 = r0.d     // Catch:{ ParserConfigurationException -> 0x00c9, TransformerConfigurationException -> 0x00d0, FileNotFoundException -> 0x00d8, TransformerException -> 0x00dd, Exception -> 0x00e2 }
            r7.setAttribute(r2, r8)     // Catch:{ ParserConfigurationException -> 0x00c9, TransformerConfigurationException -> 0x00d0, FileNotFoundException -> 0x00d8, TransformerException -> 0x00dd, Exception -> 0x00e2 }
            java.lang.String r2 = "pic"
            java.lang.String r8 = r0.f819a     // Catch:{ ParserConfigurationException -> 0x00c9, TransformerConfigurationException -> 0x00d0, FileNotFoundException -> 0x00d8, TransformerException -> 0x00dd, Exception -> 0x00e2 }
            r7.setAttribute(r2, r8)     // Catch:{ ParserConfigurationException -> 0x00c9, TransformerConfigurationException -> 0x00d0, FileNotFoundException -> 0x00d8, TransformerException -> 0x00dd, Exception -> 0x00e2 }
            java.lang.String r2 = "id"
            java.lang.String r8 = r0.g     // Catch:{ ParserConfigurationException -> 0x00c9, TransformerConfigurationException -> 0x00d0, FileNotFoundException -> 0x00d8, TransformerException -> 0x00dd, Exception -> 0x00e2 }
            r7.setAttribute(r2, r8)     // Catch:{ ParserConfigurationException -> 0x00c9, TransformerConfigurationException -> 0x00d0, FileNotFoundException -> 0x00d8, TransformerException -> 0x00dd, Exception -> 0x00e2 }
            java.lang.String r8 = "isnew"
            boolean r2 = r0.e     // Catch:{ ParserConfigurationException -> 0x00c9, TransformerConfigurationException -> 0x00d0, FileNotFoundException -> 0x00d8, TransformerException -> 0x00dd, Exception -> 0x00e2 }
            if (r2 == 0) goto L_0x0082
            java.lang.String r2 = "1"
        L_0x0071:
            r7.setAttribute(r8, r2)     // Catch:{ ParserConfigurationException -> 0x00c9, TransformerConfigurationException -> 0x00d0, FileNotFoundException -> 0x00d8, TransformerException -> 0x00dd, Exception -> 0x00e2 }
            java.lang.String r2 = "fav"
            java.lang.String r0 = r0.f     // Catch:{ ParserConfigurationException -> 0x00c9, TransformerConfigurationException -> 0x00d0, FileNotFoundException -> 0x00d8, TransformerException -> 0x00dd, Exception -> 0x00e2 }
            r7.setAttribute(r2, r0)     // Catch:{ ParserConfigurationException -> 0x00c9, TransformerConfigurationException -> 0x00d0, FileNotFoundException -> 0x00d8, TransformerException -> 0x00dd, Exception -> 0x00e2 }
            r6.appendChild(r7)     // Catch:{ ParserConfigurationException -> 0x00c9, TransformerConfigurationException -> 0x00d0, FileNotFoundException -> 0x00d8, TransformerException -> 0x00dd, Exception -> 0x00e2 }
            int r0 = r3 + 1
            r3 = r0
            goto L_0x0029
        L_0x0082:
            java.lang.String r2 = "0"
            goto L_0x0071
        L_0x0085:
            javax.xml.transform.TransformerFactory r0 = javax.xml.transform.TransformerFactory.newInstance()     // Catch:{ ParserConfigurationException -> 0x00c9, TransformerConfigurationException -> 0x00d0, FileNotFoundException -> 0x00d8, TransformerException -> 0x00dd, Exception -> 0x00e2 }
            javax.xml.transform.Transformer r0 = r0.newTransformer()     // Catch:{ ParserConfigurationException -> 0x00c9, TransformerConfigurationException -> 0x00d0, FileNotFoundException -> 0x00d8, TransformerException -> 0x00dd, Exception -> 0x00e2 }
            java.lang.String r2 = "encoding"
            java.lang.String r3 = "utf-8"
            r0.setOutputProperty(r2, r3)     // Catch:{ ParserConfigurationException -> 0x00c9, TransformerConfigurationException -> 0x00d0, FileNotFoundException -> 0x00d8, TransformerException -> 0x00dd, Exception -> 0x00e2 }
            java.lang.String r2 = "indent"
            java.lang.String r3 = "yes"
            r0.setOutputProperty(r2, r3)     // Catch:{ ParserConfigurationException -> 0x00c9, TransformerConfigurationException -> 0x00d0, FileNotFoundException -> 0x00d8, TransformerException -> 0x00dd, Exception -> 0x00e2 }
            java.lang.String r2 = "standalone"
            java.lang.String r3 = "yes"
            r0.setOutputProperty(r2, r3)     // Catch:{ ParserConfigurationException -> 0x00c9, TransformerConfigurationException -> 0x00d0, FileNotFoundException -> 0x00d8, TransformerException -> 0x00dd, Exception -> 0x00e2 }
            java.lang.String r2 = "method"
            java.lang.String r3 = "xml"
            r0.setOutputProperty(r2, r3)     // Catch:{ ParserConfigurationException -> 0x00c9, TransformerConfigurationException -> 0x00d0, FileNotFoundException -> 0x00d8, TransformerException -> 0x00dd, Exception -> 0x00e2 }
            javax.xml.transform.dom.DOMSource r2 = new javax.xml.transform.dom.DOMSource     // Catch:{ ParserConfigurationException -> 0x00c9, TransformerConfigurationException -> 0x00d0, FileNotFoundException -> 0x00d8, TransformerException -> 0x00dd, Exception -> 0x00e2 }
            org.w3c.dom.Element r3 = r5.getDocumentElement()     // Catch:{ ParserConfigurationException -> 0x00c9, TransformerConfigurationException -> 0x00d0, FileNotFoundException -> 0x00d8, TransformerException -> 0x00dd, Exception -> 0x00e2 }
            r2.<init>(r3)     // Catch:{ ParserConfigurationException -> 0x00c9, TransformerConfigurationException -> 0x00d0, FileNotFoundException -> 0x00d8, TransformerException -> 0x00dd, Exception -> 0x00e2 }
            java.io.FileOutputStream r3 = new java.io.FileOutputStream     // Catch:{ ParserConfigurationException -> 0x00c9, TransformerConfigurationException -> 0x00d0, FileNotFoundException -> 0x00d8, TransformerException -> 0x00dd, Exception -> 0x00e2 }
            java.io.File r5 = new java.io.File     // Catch:{ ParserConfigurationException -> 0x00c9, TransformerConfigurationException -> 0x00d0, FileNotFoundException -> 0x00d8, TransformerException -> 0x00dd, Exception -> 0x00e2 }
            java.lang.String r6 = r9.f788a     // Catch:{ ParserConfigurationException -> 0x00c9, TransformerConfigurationException -> 0x00d0, FileNotFoundException -> 0x00d8, TransformerException -> 0x00dd, Exception -> 0x00e2 }
            r5.<init>(r6)     // Catch:{ ParserConfigurationException -> 0x00c9, TransformerConfigurationException -> 0x00d0, FileNotFoundException -> 0x00d8, TransformerException -> 0x00dd, Exception -> 0x00e2 }
            r3.<init>(r5)     // Catch:{ ParserConfigurationException -> 0x00c9, TransformerConfigurationException -> 0x00d0, FileNotFoundException -> 0x00d8, TransformerException -> 0x00dd, Exception -> 0x00e2 }
            javax.xml.transform.stream.StreamResult r5 = new javax.xml.transform.stream.StreamResult     // Catch:{ ParserConfigurationException -> 0x00c9, TransformerConfigurationException -> 0x00d0, FileNotFoundException -> 0x00d8, TransformerException -> 0x00dd, Exception -> 0x00e2 }
            r5.<init>(r3)     // Catch:{ ParserConfigurationException -> 0x00c9, TransformerConfigurationException -> 0x00d0, FileNotFoundException -> 0x00d8, TransformerException -> 0x00dd, Exception -> 0x00e2 }
            r0.transform(r2, r5)     // Catch:{ ParserConfigurationException -> 0x00c9, TransformerConfigurationException -> 0x00d0, FileNotFoundException -> 0x00d8, TransformerException -> 0x00dd, Exception -> 0x00e2 }
            r0 = 1
            monitor-exit(r4)     // Catch:{ all -> 0x00d5 }
        L_0x00c8:
            return r0
        L_0x00c9:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x00d5 }
        L_0x00cd:
            monitor-exit(r4)     // Catch:{ all -> 0x00d5 }
            r0 = r1
            goto L_0x00c8
        L_0x00d0:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x00d5 }
            goto L_0x00cd
        L_0x00d5:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x00d5 }
            throw r0
        L_0x00d8:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x00d5 }
            goto L_0x00cd
        L_0x00dd:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x00d5 }
            goto L_0x00cd
        L_0x00e2:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x00d5 }
            goto L_0x00cd
        */
        throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.b.f.a.l():boolean");
    }

    /* renamed from: b */
    public b a(int i) {
        if (i < 0 || i >= this.b.size()) {
            return null;
        }
        return this.b.get(i);
    }

    public int c() {
        return this.b.size();
    }

    public boolean d() {
        return false;
    }

    public void e() {
    }

    public boolean g() {
        return false;
    }

    public f.a b() {
        return f.a.list_user_collect;
    }

    public void f() {
    }
}
