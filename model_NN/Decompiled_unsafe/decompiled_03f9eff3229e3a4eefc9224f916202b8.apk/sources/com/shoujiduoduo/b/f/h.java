package com.shoujiduoduo.b.f;

import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.base.a.a;
import java.util.ArrayList;

/* compiled from: FavoriteRingList */
class h implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ boolean f802a;
    final /* synthetic */ String b;
    final /* synthetic */ g c;

    h(g gVar, boolean z, String str) {
        this.c = gVar;
        this.f802a = z;
        this.b = str;
    }

    public void run() {
        ArrayList a2 = this.c.m();
        ArrayList arrayList = null;
        if (this.f802a) {
            a.a(g.f800a, "user is in login state, get online ring from cache");
            arrayList = this.c.b(this.b);
        }
        x.a().a(new i(this, a2, this.c.a(arrayList, a2, true)));
        if (this.f802a) {
            this.c.c(this.b);
        }
    }
}
