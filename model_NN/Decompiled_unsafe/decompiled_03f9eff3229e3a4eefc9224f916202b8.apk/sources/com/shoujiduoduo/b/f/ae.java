package com.shoujiduoduo.b.f;

import com.shoujiduoduo.base.bean.MakeRingData;
import com.shoujiduoduo.base.bean.RingData;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;

/* compiled from: MakeRingList */
class ae implements Comparator<RingData> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SimpleDateFormat f791a;
    final /* synthetic */ v b;

    ae(v vVar, SimpleDateFormat simpleDateFormat) {
        this.b = vVar;
        this.f791a = simpleDateFormat;
    }

    /* renamed from: a */
    public int compare(RingData ringData, RingData ringData2) {
        try {
            int compareTo = this.f791a.parse(((MakeRingData) ringData).c).compareTo(this.f791a.parse(((MakeRingData) ringData2).c));
            if (compareTo > 0) {
                return -1;
            }
            if (compareTo != 0) {
                return 1;
            }
            return 0;
        } catch (ParseException e) {
            e.printStackTrace();
            return 0;
        }
    }
}
