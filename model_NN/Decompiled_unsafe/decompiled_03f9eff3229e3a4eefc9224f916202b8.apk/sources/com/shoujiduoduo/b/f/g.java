package com.shoujiduoduo.b.f;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.a.b.b;
import com.shoujiduoduo.a.c.u;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.base.bean.c;
import com.shoujiduoduo.base.bean.e;
import com.shoujiduoduo.base.bean.f;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.util.as;
import com.shoujiduoduo.util.i;
import com.shoujiduoduo.util.j;
import com.shoujiduoduo.util.l;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/* compiled from: FavoriteRingList */
public class g implements c {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final String f800a = g.class.getSimpleName();
    private static final String b = l.b(3);
    private a c = new a(RingDDApp.c(), "duoduo.ringtone.database", null, 3);
    private String d;
    private String e;
    private String f;
    /* access modifiers changed from: private */
    public ArrayList<RingData> g = new ArrayList<>();
    /* access modifiers changed from: private */
    public ArrayList<RingData> h = new ArrayList<>();
    /* access modifiers changed from: private */
    public boolean i;
    private u j = new k(this);

    public boolean h() {
        return this.i;
    }

    public void i() {
        com.shoujiduoduo.base.a.a.b(f800a, "begin init favorite ring data");
        this.d = as.a(RingDDApp.c(), "user_ring_phone_select", "ringtoneduoduo_not_set");
        this.e = as.a(RingDDApp.c(), "user_ring_alarm_select", "ringtoneduoduo_not_set");
        this.f = as.a(RingDDApp.c(), "user_ring_notification_select", "ringtoneduoduo_not_set");
        i.a(new h(this, b.g().g(), b.g().f()));
        com.shoujiduoduo.base.a.a.b(f800a, "end init favorite ring data");
    }

    public void j() {
        x.a().a(com.shoujiduoduo.a.a.b.OBSERVER_USER_CENTER, this.j);
    }

    public void k() {
        x.a().b(com.shoujiduoduo.a.a.b.OBSERVER_USER_CENTER, this.j);
    }

    public String a() {
        return "user_favorite";
    }

    /* access modifiers changed from: private */
    public ArrayList<RingData> b(String str) {
        String str2 = l.a(2) + str + ".xml";
        File file = new File(str2);
        if (file.exists()) {
            com.shoujiduoduo.base.a.a.a(f800a, "user online ring cache exists, cache file:" + str2);
            try {
                e<RingData> c2 = j.c(new FileInputStream(file));
                if (c2 != null) {
                    com.shoujiduoduo.base.a.a.a(f800a, "user online ring cache size:" + c2.f821a.size());
                    return c2.f821a;
                }
                com.shoujiduoduo.base.a.a.c(f800a, "parse user online ring cache failed!!");
            } catch (ArrayIndexOutOfBoundsException e2) {
                e2.printStackTrace();
            } catch (FileNotFoundException e3) {
                e3.printStackTrace();
            }
        } else {
            com.shoujiduoduo.base.a.a.a(f800a, "user online ring cache not exists");
        }
        return null;
    }

    /* access modifiers changed from: private */
    public void c(String str) {
        i.a(new m(this, str));
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0116, code lost:
        r4.j = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x011b, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x011c, code lost:
        r1.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:?, code lost:
        r4.i = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0156, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0157, code lost:
        r1.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:?, code lost:
        r4.k = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0161, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0162, code lost:
        r1.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0166, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0167, code lost:
        r1.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0170, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x0171, code lost:
        r1.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0175, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0176, code lost:
        r1.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x017a, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x017b, code lost:
        r1.printStackTrace();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x011b A[ExcHandler: FileNotFoundException (r1v10 'e' java.io.FileNotFoundException A[CUSTOM_DECLARE]), Splitter:B:8:0x0024] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0156 A[ExcHandler: ParserConfigurationException (r1v7 'e' javax.xml.parsers.ParserConfigurationException A[CUSTOM_DECLARE]), Splitter:B:8:0x0024] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0161 A[ExcHandler: SAXException (r1v6 'e' org.xml.sax.SAXException A[CUSTOM_DECLARE]), Splitter:B:8:0x0024] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0166 A[ExcHandler: IOException (r1v5 'e' java.io.IOException A[CUSTOM_DECLARE]), Splitter:B:8:0x0024] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0170 A[ExcHandler: NullPointerException (r1v3 'e' java.lang.NullPointerException A[CUSTOM_DECLARE]), Splitter:B:8:0x0024] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0175 A[ExcHandler: DOMException (r1v2 'e' org.w3c.dom.DOMException A[CUSTOM_DECLARE]), Splitter:B:8:0x0024] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x017a A[ExcHandler: ArrayIndexOutOfBoundsException (r1v1 'e' java.lang.ArrayIndexOutOfBoundsException A[CUSTOM_DECLARE]), Splitter:B:8:0x0024] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.ArrayList<com.shoujiduoduo.base.bean.RingData> m() {
        /*
            r7 = this;
            r1 = 0
            java.io.File r0 = new java.io.File
            java.lang.String r2 = com.shoujiduoduo.b.f.g.b
            r0.<init>(r2)
            boolean r2 = r0.exists()
            if (r2 != 0) goto L_0x0018
            r0.createNewFile()     // Catch:{ IOException -> 0x0013 }
        L_0x0011:
            r0 = 0
        L_0x0012:
            return r0
        L_0x0013:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0011
        L_0x0018:
            java.lang.String r0 = com.shoujiduoduo.b.f.g.f800a
            java.lang.String r2 = "read Local ringlist begin"
            com.shoujiduoduo.base.a.a.a(r0, r2)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x011b, ParserConfigurationException -> 0x0156, SAXException -> 0x0161, IOException -> 0x0166, NumberFormatException -> 0x016b, NullPointerException -> 0x0170, DOMException -> 0x0175, ArrayIndexOutOfBoundsException -> 0x017a }
            java.lang.String r3 = com.shoujiduoduo.b.f.g.b     // Catch:{ FileNotFoundException -> 0x011b, ParserConfigurationException -> 0x0156, SAXException -> 0x0161, IOException -> 0x0166, NumberFormatException -> 0x016b, NullPointerException -> 0x0170, DOMException -> 0x0175, ArrayIndexOutOfBoundsException -> 0x017a }
            r2.<init>(r3)     // Catch:{ FileNotFoundException -> 0x011b, ParserConfigurationException -> 0x0156, SAXException -> 0x0161, IOException -> 0x0166, NumberFormatException -> 0x016b, NullPointerException -> 0x0170, DOMException -> 0x0175, ArrayIndexOutOfBoundsException -> 0x017a }
            javax.xml.parsers.DocumentBuilderFactory r3 = javax.xml.parsers.DocumentBuilderFactory.newInstance()     // Catch:{ FileNotFoundException -> 0x011b, ParserConfigurationException -> 0x0156, SAXException -> 0x0161, IOException -> 0x0166, NumberFormatException -> 0x016b, NullPointerException -> 0x0170, DOMException -> 0x0175, ArrayIndexOutOfBoundsException -> 0x017a }
            javax.xml.parsers.DocumentBuilder r3 = r3.newDocumentBuilder()     // Catch:{ FileNotFoundException -> 0x011b, ParserConfigurationException -> 0x0156, SAXException -> 0x0161, IOException -> 0x0166, NumberFormatException -> 0x016b, NullPointerException -> 0x0170, DOMException -> 0x0175, ArrayIndexOutOfBoundsException -> 0x017a }
            org.w3c.dom.Document r2 = r3.parse(r2)     // Catch:{ FileNotFoundException -> 0x011b, ParserConfigurationException -> 0x0156, SAXException -> 0x0161, IOException -> 0x0166, NumberFormatException -> 0x016b, NullPointerException -> 0x0170, DOMException -> 0x0175, ArrayIndexOutOfBoundsException -> 0x017a }
            org.w3c.dom.Element r2 = r2.getDocumentElement()     // Catch:{ FileNotFoundException -> 0x011b, ParserConfigurationException -> 0x0156, SAXException -> 0x0161, IOException -> 0x0166, NumberFormatException -> 0x016b, NullPointerException -> 0x0170, DOMException -> 0x0175, ArrayIndexOutOfBoundsException -> 0x017a }
            java.lang.String r3 = "phone_ring_id"
            java.lang.String r3 = r2.getAttribute(r3)     // Catch:{ FileNotFoundException -> 0x011b, ParserConfigurationException -> 0x0156, SAXException -> 0x0161, IOException -> 0x0166, NumberFormatException -> 0x016b, NullPointerException -> 0x0170, DOMException -> 0x0175, ArrayIndexOutOfBoundsException -> 0x017a }
            r7.d = r3     // Catch:{ FileNotFoundException -> 0x011b, ParserConfigurationException -> 0x0156, SAXException -> 0x0161, IOException -> 0x0166, NumberFormatException -> 0x016b, NullPointerException -> 0x0170, DOMException -> 0x0175, ArrayIndexOutOfBoundsException -> 0x017a }
            java.lang.String r3 = "alarm_ring_id"
            java.lang.String r3 = r2.getAttribute(r3)     // Catch:{ FileNotFoundException -> 0x011b, ParserConfigurationException -> 0x0156, SAXException -> 0x0161, IOException -> 0x0166, NumberFormatException -> 0x016b, NullPointerException -> 0x0170, DOMException -> 0x0175, ArrayIndexOutOfBoundsException -> 0x017a }
            r7.e = r3     // Catch:{ FileNotFoundException -> 0x011b, ParserConfigurationException -> 0x0156, SAXException -> 0x0161, IOException -> 0x0166, NumberFormatException -> 0x016b, NullPointerException -> 0x0170, DOMException -> 0x0175, ArrayIndexOutOfBoundsException -> 0x017a }
            java.lang.String r3 = "notif_ring_id"
            java.lang.String r3 = r2.getAttribute(r3)     // Catch:{ FileNotFoundException -> 0x011b, ParserConfigurationException -> 0x0156, SAXException -> 0x0161, IOException -> 0x0166, NumberFormatException -> 0x016b, NullPointerException -> 0x0170, DOMException -> 0x0175, ArrayIndexOutOfBoundsException -> 0x017a }
            r7.f = r3     // Catch:{ FileNotFoundException -> 0x011b, ParserConfigurationException -> 0x0156, SAXException -> 0x0161, IOException -> 0x0166, NumberFormatException -> 0x016b, NullPointerException -> 0x0170, DOMException -> 0x0175, ArrayIndexOutOfBoundsException -> 0x017a }
            java.lang.String r3 = "ring"
            org.w3c.dom.NodeList r2 = r2.getElementsByTagName(r3)     // Catch:{ FileNotFoundException -> 0x011b, ParserConfigurationException -> 0x0156, SAXException -> 0x0161, IOException -> 0x0166, NumberFormatException -> 0x016b, NullPointerException -> 0x0170, DOMException -> 0x0175, ArrayIndexOutOfBoundsException -> 0x017a }
        L_0x0059:
            int r3 = r2.getLength()     // Catch:{ FileNotFoundException -> 0x011b, ParserConfigurationException -> 0x0156, SAXException -> 0x0161, IOException -> 0x0166, NumberFormatException -> 0x016b, NullPointerException -> 0x0170, DOMException -> 0x0175, ArrayIndexOutOfBoundsException -> 0x017a }
            if (r1 >= r3) goto L_0x011f
            org.w3c.dom.Node r3 = r2.item(r1)     // Catch:{ FileNotFoundException -> 0x011b, ParserConfigurationException -> 0x0156, SAXException -> 0x0161, IOException -> 0x0166, NumberFormatException -> 0x016b, NullPointerException -> 0x0170, DOMException -> 0x0175, ArrayIndexOutOfBoundsException -> 0x017a }
            org.w3c.dom.NamedNodeMap r3 = r3.getAttributes()     // Catch:{ FileNotFoundException -> 0x011b, ParserConfigurationException -> 0x0156, SAXException -> 0x0161, IOException -> 0x0166, NumberFormatException -> 0x016b, NullPointerException -> 0x0170, DOMException -> 0x0175, ArrayIndexOutOfBoundsException -> 0x017a }
            com.shoujiduoduo.base.bean.RingData r4 = new com.shoujiduoduo.base.bean.RingData     // Catch:{ FileNotFoundException -> 0x011b, ParserConfigurationException -> 0x0156, SAXException -> 0x0161, IOException -> 0x0166, NumberFormatException -> 0x016b, NullPointerException -> 0x0170, DOMException -> 0x0175, ArrayIndexOutOfBoundsException -> 0x017a }
            r4.<init>()     // Catch:{ FileNotFoundException -> 0x011b, ParserConfigurationException -> 0x0156, SAXException -> 0x0161, IOException -> 0x0166, NumberFormatException -> 0x016b, NullPointerException -> 0x0170, DOMException -> 0x0175, ArrayIndexOutOfBoundsException -> 0x017a }
            java.lang.String r5 = "name"
            java.lang.String r5 = com.shoujiduoduo.util.f.a(r3, r5)     // Catch:{ FileNotFoundException -> 0x011b, ParserConfigurationException -> 0x0156, SAXException -> 0x0161, IOException -> 0x0166, NumberFormatException -> 0x016b, NullPointerException -> 0x0170, DOMException -> 0x0175, ArrayIndexOutOfBoundsException -> 0x017a }
            r4.e = r5     // Catch:{ FileNotFoundException -> 0x011b, ParserConfigurationException -> 0x0156, SAXException -> 0x0161, IOException -> 0x0166, NumberFormatException -> 0x016b, NullPointerException -> 0x0170, DOMException -> 0x0175, ArrayIndexOutOfBoundsException -> 0x017a }
            java.lang.String r5 = "artist"
            java.lang.String r5 = com.shoujiduoduo.util.f.a(r3, r5)     // Catch:{ FileNotFoundException -> 0x011b, ParserConfigurationException -> 0x0156, SAXException -> 0x0161, IOException -> 0x0166, NumberFormatException -> 0x016b, NullPointerException -> 0x0170, DOMException -> 0x0175, ArrayIndexOutOfBoundsException -> 0x017a }
            r4.f = r5     // Catch:{ FileNotFoundException -> 0x011b, ParserConfigurationException -> 0x0156, SAXException -> 0x0161, IOException -> 0x0166, NumberFormatException -> 0x016b, NullPointerException -> 0x0170, DOMException -> 0x0175, ArrayIndexOutOfBoundsException -> 0x017a }
            java.lang.String r5 = "duration"
            java.lang.String r5 = com.shoujiduoduo.util.f.a(r3, r5)     // Catch:{ NumberFormatException -> 0x0115, FileNotFoundException -> 0x011b, ParserConfigurationException -> 0x0156, SAXException -> 0x0161, IOException -> 0x0166, NullPointerException -> 0x0170, DOMException -> 0x0175, ArrayIndexOutOfBoundsException -> 0x017a }
            int r5 = java.lang.Integer.parseInt(r5)     // Catch:{ NumberFormatException -> 0x0115, FileNotFoundException -> 0x011b, ParserConfigurationException -> 0x0156, SAXException -> 0x0161, IOException -> 0x0166, NullPointerException -> 0x0170, DOMException -> 0x0175, ArrayIndexOutOfBoundsException -> 0x017a }
            r4.j = r5     // Catch:{ NumberFormatException -> 0x0115, FileNotFoundException -> 0x011b, ParserConfigurationException -> 0x0156, SAXException -> 0x0161, IOException -> 0x0166, NullPointerException -> 0x0170, DOMException -> 0x0175, ArrayIndexOutOfBoundsException -> 0x017a }
        L_0x0088:
            java.lang.String r5 = "score"
            java.lang.String r5 = com.shoujiduoduo.util.f.a(r3, r5)     // Catch:{ NumberFormatException -> 0x0150, FileNotFoundException -> 0x011b, ParserConfigurationException -> 0x0156, SAXException -> 0x0161, IOException -> 0x0166, NullPointerException -> 0x0170, DOMException -> 0x0175, ArrayIndexOutOfBoundsException -> 0x017a }
            int r5 = java.lang.Integer.parseInt(r5)     // Catch:{ NumberFormatException -> 0x0150, FileNotFoundException -> 0x011b, ParserConfigurationException -> 0x0156, SAXException -> 0x0161, IOException -> 0x0166, NullPointerException -> 0x0170, DOMException -> 0x0175, ArrayIndexOutOfBoundsException -> 0x017a }
            r4.i = r5     // Catch:{ NumberFormatException -> 0x0150, FileNotFoundException -> 0x011b, ParserConfigurationException -> 0x0156, SAXException -> 0x0161, IOException -> 0x0166, NullPointerException -> 0x0170, DOMException -> 0x0175, ArrayIndexOutOfBoundsException -> 0x017a }
        L_0x0094:
            java.lang.String r5 = "playcnt"
            java.lang.String r5 = com.shoujiduoduo.util.f.a(r3, r5)     // Catch:{ NumberFormatException -> 0x015b, FileNotFoundException -> 0x011b, ParserConfigurationException -> 0x0156, SAXException -> 0x0161, IOException -> 0x0166, NullPointerException -> 0x0170, DOMException -> 0x0175, ArrayIndexOutOfBoundsException -> 0x017a }
            int r5 = java.lang.Integer.parseInt(r5)     // Catch:{ NumberFormatException -> 0x015b, FileNotFoundException -> 0x011b, ParserConfigurationException -> 0x0156, SAXException -> 0x0161, IOException -> 0x0166, NullPointerException -> 0x0170, DOMException -> 0x0175, ArrayIndexOutOfBoundsException -> 0x017a }
            r4.k = r5     // Catch:{ NumberFormatException -> 0x015b, FileNotFoundException -> 0x011b, ParserConfigurationException -> 0x0156, SAXException -> 0x0161, IOException -> 0x0166, NullPointerException -> 0x0170, DOMException -> 0x0175, ArrayIndexOutOfBoundsException -> 0x017a }
        L_0x00a0:
            java.lang.String r5 = "cid"
            java.lang.String r5 = com.shoujiduoduo.util.f.a(r3, r5)     // Catch:{ FileNotFoundException -> 0x011b, ParserConfigurationException -> 0x0156, SAXException -> 0x0161, IOException -> 0x0166, NumberFormatException -> 0x016b, NullPointerException -> 0x0170, DOMException -> 0x0175, ArrayIndexOutOfBoundsException -> 0x017a }
            r4.n = r5     // Catch:{ FileNotFoundException -> 0x011b, ParserConfigurationException -> 0x0156, SAXException -> 0x0161, IOException -> 0x0166, NumberFormatException -> 0x016b, NullPointerException -> 0x0170, DOMException -> 0x0175, ArrayIndexOutOfBoundsException -> 0x017a }
            java.lang.String r5 = "valid"
            java.lang.String r5 = com.shoujiduoduo.util.f.a(r3, r5)     // Catch:{ FileNotFoundException -> 0x011b, ParserConfigurationException -> 0x0156, SAXException -> 0x0161, IOException -> 0x0166, NumberFormatException -> 0x016b, NullPointerException -> 0x0170, DOMException -> 0x0175, ArrayIndexOutOfBoundsException -> 0x017a }
            r4.o = r5     // Catch:{ FileNotFoundException -> 0x011b, ParserConfigurationException -> 0x0156, SAXException -> 0x0161, IOException -> 0x0166, NumberFormatException -> 0x016b, NullPointerException -> 0x0170, DOMException -> 0x0175, ArrayIndexOutOfBoundsException -> 0x017a }
            java.lang.String r5 = "singerId"
            java.lang.String r5 = com.shoujiduoduo.util.f.a(r3, r5)     // Catch:{ FileNotFoundException -> 0x011b, ParserConfigurationException -> 0x0156, SAXException -> 0x0161, IOException -> 0x0166, NumberFormatException -> 0x016b, NullPointerException -> 0x0170, DOMException -> 0x0175, ArrayIndexOutOfBoundsException -> 0x017a }
            r4.r = r5     // Catch:{ FileNotFoundException -> 0x011b, ParserConfigurationException -> 0x0156, SAXException -> 0x0161, IOException -> 0x0166, NumberFormatException -> 0x016b, NullPointerException -> 0x0170, DOMException -> 0x0175, ArrayIndexOutOfBoundsException -> 0x017a }
            java.lang.String r5 = "price"
            java.lang.String r5 = com.shoujiduoduo.util.f.a(r3, r5)     // Catch:{ FileNotFoundException -> 0x011b, ParserConfigurationException -> 0x0156, SAXException -> 0x0161, IOException -> 0x0166, NumberFormatException -> 0x016b, NullPointerException -> 0x0170, DOMException -> 0x0175, ArrayIndexOutOfBoundsException -> 0x017a }
            r6 = 200(0xc8, float:2.8E-43)
            int r5 = com.shoujiduoduo.util.u.a(r5, r6)     // Catch:{ FileNotFoundException -> 0x011b, ParserConfigurationException -> 0x0156, SAXException -> 0x0161, IOException -> 0x0166, NumberFormatException -> 0x016b, NullPointerException -> 0x0170, DOMException -> 0x0175, ArrayIndexOutOfBoundsException -> 0x017a }
            r4.p = r5     // Catch:{ FileNotFoundException -> 0x011b, ParserConfigurationException -> 0x0156, SAXException -> 0x0161, IOException -> 0x0166, NumberFormatException -> 0x016b, NullPointerException -> 0x0170, DOMException -> 0x0175, ArrayIndexOutOfBoundsException -> 0x017a }
            java.lang.String r5 = "hasmedia"
            java.lang.String r5 = com.shoujiduoduo.util.f.a(r3, r5)     // Catch:{ FileNotFoundException -> 0x011b, ParserConfigurationException -> 0x0156, SAXException -> 0x0161, IOException -> 0x0166, NumberFormatException -> 0x016b, NullPointerException -> 0x0170, DOMException -> 0x0175, ArrayIndexOutOfBoundsException -> 0x017a }
            r6 = 0
            int r5 = com.shoujiduoduo.util.u.a(r5, r6)     // Catch:{ FileNotFoundException -> 0x011b, ParserConfigurationException -> 0x0156, SAXException -> 0x0161, IOException -> 0x0166, NumberFormatException -> 0x016b, NullPointerException -> 0x0170, DOMException -> 0x0175, ArrayIndexOutOfBoundsException -> 0x017a }
            r4.q = r5     // Catch:{ FileNotFoundException -> 0x011b, ParserConfigurationException -> 0x0156, SAXException -> 0x0161, IOException -> 0x0166, NumberFormatException -> 0x016b, NullPointerException -> 0x0170, DOMException -> 0x0175, ArrayIndexOutOfBoundsException -> 0x017a }
            java.lang.String r5 = "ctcid"
            java.lang.String r5 = com.shoujiduoduo.util.f.a(r3, r5)     // Catch:{ FileNotFoundException -> 0x011b, ParserConfigurationException -> 0x0156, SAXException -> 0x0161, IOException -> 0x0166, NumberFormatException -> 0x016b, NullPointerException -> 0x0170, DOMException -> 0x0175, ArrayIndexOutOfBoundsException -> 0x017a }
            r4.s = r5     // Catch:{ FileNotFoundException -> 0x011b, ParserConfigurationException -> 0x0156, SAXException -> 0x0161, IOException -> 0x0166, NumberFormatException -> 0x016b, NullPointerException -> 0x0170, DOMException -> 0x0175, ArrayIndexOutOfBoundsException -> 0x017a }
            java.lang.String r5 = "ctvalid"
            java.lang.String r5 = com.shoujiduoduo.util.f.a(r3, r5)     // Catch:{ FileNotFoundException -> 0x011b, ParserConfigurationException -> 0x0156, SAXException -> 0x0161, IOException -> 0x0166, NumberFormatException -> 0x016b, NullPointerException -> 0x0170, DOMException -> 0x0175, ArrayIndexOutOfBoundsException -> 0x017a }
            r4.t = r5     // Catch:{ FileNotFoundException -> 0x011b, ParserConfigurationException -> 0x0156, SAXException -> 0x0161, IOException -> 0x0166, NumberFormatException -> 0x016b, NullPointerException -> 0x0170, DOMException -> 0x0175, ArrayIndexOutOfBoundsException -> 0x017a }
            java.lang.String r5 = "ctprice"
            java.lang.String r5 = com.shoujiduoduo.util.f.a(r3, r5)     // Catch:{ FileNotFoundException -> 0x011b, ParserConfigurationException -> 0x0156, SAXException -> 0x0161, IOException -> 0x0166, NumberFormatException -> 0x016b, NullPointerException -> 0x0170, DOMException -> 0x0175, ArrayIndexOutOfBoundsException -> 0x017a }
            r6 = 200(0xc8, float:2.8E-43)
            int r5 = com.shoujiduoduo.util.u.a(r5, r6)     // Catch:{ FileNotFoundException -> 0x011b, ParserConfigurationException -> 0x0156, SAXException -> 0x0161, IOException -> 0x0166, NumberFormatException -> 0x016b, NullPointerException -> 0x0170, DOMException -> 0x0175, ArrayIndexOutOfBoundsException -> 0x017a }
            r4.u = r5     // Catch:{ FileNotFoundException -> 0x011b, ParserConfigurationException -> 0x0156, SAXException -> 0x0161, IOException -> 0x0166, NumberFormatException -> 0x016b, NullPointerException -> 0x0170, DOMException -> 0x0175, ArrayIndexOutOfBoundsException -> 0x017a }
            java.lang.String r5 = "cthasmedia"
            java.lang.String r5 = com.shoujiduoduo.util.f.a(r3, r5)     // Catch:{ FileNotFoundException -> 0x011b, ParserConfigurationException -> 0x0156, SAXException -> 0x0161, IOException -> 0x0166, NumberFormatException -> 0x016b, NullPointerException -> 0x0170, DOMException -> 0x0175, ArrayIndexOutOfBoundsException -> 0x017a }
            r6 = 0
            int r5 = com.shoujiduoduo.util.u.a(r5, r6)     // Catch:{ FileNotFoundException -> 0x011b, ParserConfigurationException -> 0x0156, SAXException -> 0x0161, IOException -> 0x0166, NumberFormatException -> 0x016b, NullPointerException -> 0x0170, DOMException -> 0x0175, ArrayIndexOutOfBoundsException -> 0x017a }
            r4.v = r5     // Catch:{ FileNotFoundException -> 0x011b, ParserConfigurationException -> 0x0156, SAXException -> 0x0161, IOException -> 0x0166, NumberFormatException -> 0x016b, NullPointerException -> 0x0170, DOMException -> 0x0175, ArrayIndexOutOfBoundsException -> 0x017a }
            java.lang.String r5 = "rid"
            java.lang.String r5 = com.shoujiduoduo.util.f.a(r3, r5)     // Catch:{ FileNotFoundException -> 0x011b, ParserConfigurationException -> 0x0156, SAXException -> 0x0161, IOException -> 0x0166, NumberFormatException -> 0x016b, NullPointerException -> 0x0170, DOMException -> 0x0175, ArrayIndexOutOfBoundsException -> 0x017a }
            r4.g = r5     // Catch:{ FileNotFoundException -> 0x011b, ParserConfigurationException -> 0x0156, SAXException -> 0x0161, IOException -> 0x0166, NumberFormatException -> 0x016b, NullPointerException -> 0x0170, DOMException -> 0x0175, ArrayIndexOutOfBoundsException -> 0x017a }
            java.lang.String r5 = "bdurl"
            java.lang.String r3 = com.shoujiduoduo.util.f.a(r3, r5)     // Catch:{ FileNotFoundException -> 0x011b, ParserConfigurationException -> 0x0156, SAXException -> 0x0161, IOException -> 0x0166, NumberFormatException -> 0x016b, NullPointerException -> 0x0170, DOMException -> 0x0175, ArrayIndexOutOfBoundsException -> 0x017a }
            r4.h = r3     // Catch:{ FileNotFoundException -> 0x011b, ParserConfigurationException -> 0x0156, SAXException -> 0x0161, IOException -> 0x0166, NumberFormatException -> 0x016b, NullPointerException -> 0x0170, DOMException -> 0x0175, ArrayIndexOutOfBoundsException -> 0x017a }
            r0.add(r4)     // Catch:{ FileNotFoundException -> 0x011b, ParserConfigurationException -> 0x0156, SAXException -> 0x0161, IOException -> 0x0166, NumberFormatException -> 0x016b, NullPointerException -> 0x0170, DOMException -> 0x0175, ArrayIndexOutOfBoundsException -> 0x017a }
            int r1 = r1 + 1
            goto L_0x0059
        L_0x0115:
            r5 = move-exception
            r5 = 0
            r4.j = r5     // Catch:{ FileNotFoundException -> 0x011b, ParserConfigurationException -> 0x0156, SAXException -> 0x0161, IOException -> 0x0166, NumberFormatException -> 0x016b, NullPointerException -> 0x0170, DOMException -> 0x0175, ArrayIndexOutOfBoundsException -> 0x017a }
            goto L_0x0088
        L_0x011b:
            r1 = move-exception
            r1.printStackTrace()
        L_0x011f:
            java.util.HashMap r1 = new java.util.HashMap
            r1.<init>()
            android.content.Context r2 = com.shoujiduoduo.ringtone.RingDDApp.c()
            java.lang.String r3 = "COLLECT_RING_NUM_NEW"
            int r4 = r0.size()
            long r4 = (long) r4
            com.shoujiduoduo.util.f.a(r2, r3, r1, r4)
            java.lang.String r1 = com.shoujiduoduo.b.f.g.f800a
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "local ring size:"
            java.lang.StringBuilder r2 = r2.append(r3)
            int r3 = r0.size()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            com.shoujiduoduo.base.a.a.a(r1, r2)
            goto L_0x0012
        L_0x0150:
            r5 = move-exception
            r5 = 0
            r4.i = r5     // Catch:{ FileNotFoundException -> 0x011b, ParserConfigurationException -> 0x0156, SAXException -> 0x0161, IOException -> 0x0166, NumberFormatException -> 0x016b, NullPointerException -> 0x0170, DOMException -> 0x0175, ArrayIndexOutOfBoundsException -> 0x017a }
            goto L_0x0094
        L_0x0156:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x011f
        L_0x015b:
            r5 = move-exception
            r5 = 0
            r4.k = r5     // Catch:{ FileNotFoundException -> 0x011b, ParserConfigurationException -> 0x0156, SAXException -> 0x0161, IOException -> 0x0166, NumberFormatException -> 0x016b, NullPointerException -> 0x0170, DOMException -> 0x0175, ArrayIndexOutOfBoundsException -> 0x017a }
            goto L_0x00a0
        L_0x0161:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x011f
        L_0x0166:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x011f
        L_0x016b:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x011f
        L_0x0170:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x011f
        L_0x0175:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x011f
        L_0x017a:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x011f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.b.f.g.m():java.util.ArrayList");
    }

    /* access modifiers changed from: private */
    public ArrayList<RingData> a(ArrayList<RingData> arrayList, ArrayList<RingData> arrayList2, boolean z) {
        com.shoujiduoduo.base.a.a.a(f800a, "\n\n");
        com.shoujiduoduo.base.a.a.a(f800a, "mergeRingdata begin, bFromCache:" + z);
        if (arrayList == null || arrayList.size() == 0) {
            com.shoujiduoduo.base.a.a.a(f800a, "online list is null or size is 0, return localRinglist");
            return arrayList2;
        } else if (arrayList2 == null || arrayList2.size() == 0) {
            com.shoujiduoduo.base.a.a.a(f800a, "localRinglist is null or size is 0, return online list");
            return arrayList;
        } else {
            HashMap hashMap = new HashMap();
            ArrayList<RingData> arrayList3 = new ArrayList<>();
            ArrayList arrayList4 = new ArrayList();
            com.shoujiduoduo.base.a.a.a(f800a, "local ring size:" + arrayList2.size());
            com.shoujiduoduo.base.a.a.a(f800a, "online ring size:" + arrayList.size());
            Iterator<RingData> it = arrayList.iterator();
            while (it.hasNext()) {
                RingData next = it.next();
                hashMap.put("" + next.k(), next);
            }
            arrayList3.addAll(arrayList);
            Iterator<RingData> it2 = arrayList2.iterator();
            while (it2.hasNext()) {
                RingData next2 = it2.next();
                if (!hashMap.containsKey("" + next2.k())) {
                    arrayList3.add(next2);
                    arrayList4.add(next2);
                }
            }
            if (arrayList4.size() > 0 && !z) {
                StringBuilder sb = new StringBuilder();
                Iterator it3 = arrayList4.iterator();
                while (it3.hasNext()) {
                    sb.append(((RingData) it3.next()).g + "|");
                }
                String substring = sb.substring(0, sb.length() - 1);
                com.shoujiduoduo.base.a.a.a(f800a, "本地有新增铃声，需要同步至服务器");
                com.shoujiduoduo.base.a.a.a(f800a, "add user favorite, size:" + arrayList4.size() + ", rid:" + substring);
                i.a(new p(this, substring));
            }
            com.shoujiduoduo.base.a.a.a(f800a, "merge ring size:" + arrayList3.size());
            return arrayList3;
        }
    }

    public boolean a(String str) {
        synchronized (f800a) {
            for (int i2 = 0; i2 < this.h.size(); i2++) {
                if (str.equalsIgnoreCase(this.h.get(i2).g)) {
                    return true;
                }
            }
            return false;
        }
    }

    private void n() {
        i.a(new q(this));
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean o() {
        /*
            r9 = this;
            r1 = 0
            java.lang.String r3 = com.shoujiduoduo.b.f.g.f800a
            monitor-enter(r3)
            javax.xml.parsers.DocumentBuilderFactory r0 = javax.xml.parsers.DocumentBuilderFactory.newInstance()     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            javax.xml.parsers.DocumentBuilder r0 = r0.newDocumentBuilder()     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            org.w3c.dom.Document r4 = r0.newDocument()     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            java.lang.String r0 = "list"
            org.w3c.dom.Element r5 = r4.createElement(r0)     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            java.lang.String r0 = "num"
            java.util.ArrayList<com.shoujiduoduo.base.bean.RingData> r2 = r9.g     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            int r2 = r2.size()     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            java.lang.String r2 = java.lang.String.valueOf(r2)     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            r5.setAttribute(r0, r2)     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            java.lang.String r0 = "phone_ring_id"
            java.lang.String r2 = r9.d     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            r5.setAttribute(r0, r2)     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            java.lang.String r0 = "alarm_ring_id"
            java.lang.String r2 = r9.e     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            r5.setAttribute(r0, r2)     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            java.lang.String r0 = "notif_ring_id"
            java.lang.String r2 = r9.f     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            r5.setAttribute(r0, r2)     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            r4.appendChild(r5)     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            r2 = r1
        L_0x003e:
            java.util.ArrayList<com.shoujiduoduo.base.bean.RingData> r0 = r9.g     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            int r0 = r0.size()     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            if (r2 >= r0) goto L_0x00e8
            java.util.ArrayList<com.shoujiduoduo.base.bean.RingData> r0 = r9.g     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            java.lang.Object r0 = r0.get(r2)     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            com.shoujiduoduo.base.bean.RingData r0 = (com.shoujiduoduo.base.bean.RingData) r0     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            java.lang.String r6 = "ring"
            org.w3c.dom.Element r6 = r4.createElement(r6)     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            java.lang.String r7 = "name"
            java.lang.String r8 = r0.e     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            r6.setAttribute(r7, r8)     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            java.lang.String r7 = "artist"
            java.lang.String r8 = r0.f     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            r6.setAttribute(r7, r8)     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            java.lang.String r7 = "duration"
            int r8 = r0.j     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            java.lang.String r8 = java.lang.String.valueOf(r8)     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            r6.setAttribute(r7, r8)     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            java.lang.String r7 = "score"
            int r8 = r0.i     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            java.lang.String r8 = java.lang.String.valueOf(r8)     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            r6.setAttribute(r7, r8)     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            java.lang.String r7 = "playcnt"
            int r8 = r0.k     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            java.lang.String r8 = java.lang.String.valueOf(r8)     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            r6.setAttribute(r7, r8)     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            java.lang.String r7 = "rid"
            java.lang.String r8 = r0.g     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            r6.setAttribute(r7, r8)     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            java.lang.String r7 = "bdurl"
            java.lang.String r8 = r0.h     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            r6.setAttribute(r7, r8)     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            java.lang.String r7 = "cid"
            java.lang.String r8 = r0.n     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            r6.setAttribute(r7, r8)     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            java.lang.String r7 = "valid"
            java.lang.String r8 = r0.o     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            r6.setAttribute(r7, r8)     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            java.lang.String r7 = "hasmedia"
            int r8 = r0.q     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            java.lang.String r8 = java.lang.String.valueOf(r8)     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            r6.setAttribute(r7, r8)     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            java.lang.String r7 = "singerId"
            java.lang.String r8 = r0.r     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            r6.setAttribute(r7, r8)     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            java.lang.String r7 = "price"
            int r8 = r0.p     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            java.lang.String r8 = java.lang.String.valueOf(r8)     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            r6.setAttribute(r7, r8)     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            java.lang.String r7 = "ctcid"
            java.lang.String r8 = r0.s     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            r6.setAttribute(r7, r8)     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            java.lang.String r7 = "ctvalid"
            java.lang.String r8 = r0.t     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            r6.setAttribute(r7, r8)     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            java.lang.String r7 = "cthasmedia"
            int r8 = r0.v     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            java.lang.String r8 = java.lang.String.valueOf(r8)     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            r6.setAttribute(r7, r8)     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            java.lang.String r7 = "ctprice"
            int r0 = r0.u     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            java.lang.String r0 = java.lang.String.valueOf(r0)     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            r6.setAttribute(r7, r0)     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            r5.appendChild(r6)     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            int r0 = r2 + 1
            r2 = r0
            goto L_0x003e
        L_0x00e8:
            javax.xml.transform.TransformerFactory r0 = javax.xml.transform.TransformerFactory.newInstance()     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            javax.xml.transform.Transformer r0 = r0.newTransformer()     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            java.lang.String r2 = "encoding"
            java.lang.String r5 = "utf-8"
            r0.setOutputProperty(r2, r5)     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            java.lang.String r2 = "indent"
            java.lang.String r5 = "yes"
            r0.setOutputProperty(r2, r5)     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            java.lang.String r2 = "standalone"
            java.lang.String r5 = "yes"
            r0.setOutputProperty(r2, r5)     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            java.lang.String r2 = "method"
            java.lang.String r5 = "xml"
            r0.setOutputProperty(r2, r5)     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            javax.xml.transform.dom.DOMSource r2 = new javax.xml.transform.dom.DOMSource     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            org.w3c.dom.Element r4 = r4.getDocumentElement()     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            r2.<init>(r4)     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            java.io.FileOutputStream r4 = new java.io.FileOutputStream     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            java.io.File r5 = new java.io.File     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            java.lang.String r6 = com.shoujiduoduo.b.f.g.b     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            r5.<init>(r6)     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            r4.<init>(r5)     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            javax.xml.transform.stream.StreamResult r5 = new javax.xml.transform.stream.StreamResult     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            r5.<init>(r4)     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            r0.transform(r2, r5)     // Catch:{ ParserConfigurationException -> 0x012c, TransformerConfigurationException -> 0x0133, FileNotFoundException -> 0x013b, TransformerException -> 0x0140, Exception -> 0x0145 }
            r0 = 1
            monitor-exit(r3)     // Catch:{ all -> 0x0138 }
        L_0x012b:
            return r0
        L_0x012c:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x0138 }
        L_0x0130:
            monitor-exit(r3)     // Catch:{ all -> 0x0138 }
            r0 = r1
            goto L_0x012b
        L_0x0133:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x0138 }
            goto L_0x0130
        L_0x0138:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0138 }
            throw r0
        L_0x013b:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x0138 }
            goto L_0x0130
        L_0x0140:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x0138 }
            goto L_0x0130
        L_0x0145:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x0138 }
            goto L_0x0130
        */
        throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.b.f.g.o():boolean");
    }

    public boolean a(List<RingData> list) {
        boolean z;
        synchronized (f800a) {
            if (list != null) {
                if (list.size() != 0) {
                    StringBuilder sb = new StringBuilder();
                    for (RingData ringData : list) {
                        sb.append(ringData.g);
                        sb.append("|");
                    }
                    if (this.h == this.g) {
                        this.h.removeAll(list);
                    } else {
                        this.h.removeAll(list);
                        this.g.removeAll(list);
                    }
                    if (sb.toString().endsWith("|")) {
                        sb.deleteCharAt(sb.length() - 1);
                    }
                    e(sb.toString());
                    n();
                    p();
                    z = true;
                }
            }
            z = false;
        }
        return z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
        r0 = a((java.util.List<com.shoujiduoduo.base.bean.RingData>) r2);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(java.util.Collection<java.lang.Integer> r7) {
        /*
            r6 = this;
            java.lang.String r1 = com.shoujiduoduo.b.f.g.f800a
            monitor-enter(r1)
            java.util.ArrayList r2 = new java.util.ArrayList     // Catch:{ all -> 0x003b }
            r2.<init>()     // Catch:{ all -> 0x003b }
            java.util.Iterator r3 = r7.iterator()     // Catch:{ all -> 0x003b }
        L_0x000c:
            boolean r0 = r3.hasNext()     // Catch:{ all -> 0x003b }
            if (r0 == 0) goto L_0x003e
            java.lang.Object r0 = r3.next()     // Catch:{ all -> 0x003b }
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ all -> 0x003b }
            int r4 = r0.intValue()     // Catch:{ all -> 0x003b }
            if (r4 < 0) goto L_0x002a
            int r4 = r0.intValue()     // Catch:{ all -> 0x003b }
            java.util.ArrayList<com.shoujiduoduo.base.bean.RingData> r5 = r6.h     // Catch:{ all -> 0x003b }
            int r5 = r5.size()     // Catch:{ all -> 0x003b }
            if (r4 <= r5) goto L_0x002d
        L_0x002a:
            r0 = 0
            monitor-exit(r1)     // Catch:{ all -> 0x003b }
        L_0x002c:
            return r0
        L_0x002d:
            java.util.ArrayList<com.shoujiduoduo.base.bean.RingData> r4 = r6.h     // Catch:{ all -> 0x003b }
            int r0 = r0.intValue()     // Catch:{ all -> 0x003b }
            java.lang.Object r0 = r4.get(r0)     // Catch:{ all -> 0x003b }
            r2.add(r0)     // Catch:{ all -> 0x003b }
            goto L_0x000c
        L_0x003b:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x003b }
            throw r0
        L_0x003e:
            boolean r0 = r6.a(r2)     // Catch:{ all -> 0x003b }
            monitor-exit(r1)     // Catch:{ all -> 0x003b }
            goto L_0x002c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.b.f.g.a(java.util.Collection):boolean");
    }

    public boolean b(int i2) {
        boolean z;
        synchronized (f800a) {
            if (i2 >= this.h.size() || i2 < 0) {
                z = false;
            } else {
                String str = this.h.get(i2).g;
                if (this.h != this.g) {
                    this.h.remove(i2);
                }
                d(str);
                e(str);
                p();
                z = true;
            }
        }
        return z;
    }

    private void d(String str) {
        com.shoujiduoduo.base.a.a.a(f800a, "delete from local data, rid:" + str);
        Iterator<RingData> it = this.g.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            RingData next = it.next();
            if (next.g.equals(str)) {
                com.shoujiduoduo.base.a.a.a(f800a, "found in local, delete success");
                this.g.remove(next);
                n();
                break;
            }
        }
        this.c.a(str);
    }

    private void e(String str) {
        if (b.g().g()) {
            i.a(new r(this, str));
        }
    }

    public boolean a(RingData ringData) {
        boolean add;
        synchronized (f800a) {
            Iterator<RingData> it = this.h.iterator();
            while (true) {
                if (it.hasNext()) {
                    if (it.next().g.equals(ringData.g)) {
                        add = true;
                        break;
                    }
                } else {
                    add = this.h.add(ringData);
                    b(ringData);
                    c(ringData);
                    p();
                    break;
                }
            }
        }
        return add;
    }

    private void b(RingData ringData) {
        com.shoujiduoduo.base.a.a.a(f800a, "append to local data, name:" + ringData.e);
        if (this.h != this.g) {
            this.g.add(ringData);
        }
        this.c.a(ringData);
        n();
    }

    private void c(RingData ringData) {
        if (b.g().g()) {
            i.a(new s(this, ringData));
        }
    }

    private void p() {
        x.a().a(com.shoujiduoduo.a.a.b.OBSERVER_LIST_DATA, new t(this));
    }

    /* compiled from: FavoriteRingList */
    private class a extends SQLiteOpenHelper {
        private final String b = a.class.getSimpleName();

        public a(Context context, String str, SQLiteDatabase.CursorFactory cursorFactory, int i) {
            super(context, str, cursorFactory, i);
        }

        public void a(String str) {
            SQLiteDatabase sQLiteDatabase = null;
            try {
                sQLiteDatabase = getWritableDatabase();
                sQLiteDatabase.execSQL("delete from user_ring_table where rid='" + str + "'");
                if (sQLiteDatabase != null) {
                    sQLiteDatabase.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
                if (sQLiteDatabase != null) {
                    sQLiteDatabase.close();
                }
            } catch (Throwable th) {
                if (sQLiteDatabase != null) {
                    sQLiteDatabase.close();
                }
                throw th;
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:10:0x00d4, code lost:
            r1 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:11:0x00d5, code lost:
            r3 = r1;
            r1 = r0;
            r0 = r3;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:13:0x00da, code lost:
            r1.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:8:0x00ce, code lost:
            if (r0 != null) goto L_0x00d0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:9:0x00d0, code lost:
            r0.close();
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Removed duplicated region for block: B:13:0x00da  */
        /* JADX WARNING: Removed duplicated region for block: B:7:0x00cd A[ExcHandler: Exception (e java.lang.Exception), Splitter:B:1:0x0001] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void a(com.shoujiduoduo.base.bean.RingData r5) {
            /*
                r4 = this;
                r0 = 0
                android.database.sqlite.SQLiteDatabase r0 = r4.getWritableDatabase()     // Catch:{ Exception -> 0x00cd, all -> 0x00d4 }
                java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                r1.<init>()     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = "insert into user_ring_table (rid, name, artist, duration, score, play_count, cailing_id, cailing_valid_date, has_media, singer_id, price)VALUES ("
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = r5.g     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = android.database.DatabaseUtils.sqlEscapeString(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = ","
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = r5.e     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = android.database.DatabaseUtils.sqlEscapeString(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = ","
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = r5.f     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = android.database.DatabaseUtils.sqlEscapeString(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = ","
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                int r2 = r5.j     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = java.lang.Integer.toString(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = ","
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                int r2 = r5.i     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = java.lang.Integer.toString(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = ","
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                int r2 = r5.k     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = java.lang.Integer.toString(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = ","
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = r5.n     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = android.database.DatabaseUtils.sqlEscapeString(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = ","
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = r5.o     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = android.database.DatabaseUtils.sqlEscapeString(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = ","
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                int r2 = r5.q     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = java.lang.Integer.toString(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = ","
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = r5.r     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = android.database.DatabaseUtils.sqlEscapeString(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = ","
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                int r2 = r5.p     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = java.lang.Integer.toString(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = ");"
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                r0.execSQL(r1)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                if (r0 == 0) goto L_0x00cc
                r0.close()
            L_0x00cc:
                return
            L_0x00cd:
                r1 = move-exception
                if (r0 == 0) goto L_0x00cc
                r0.close()
                goto L_0x00cc
            L_0x00d4:
                r1 = move-exception
                r3 = r1
                r1 = r0
                r0 = r3
            L_0x00d8:
                if (r1 == 0) goto L_0x00dd
                r1.close()
            L_0x00dd:
                throw r0
            L_0x00de:
                r1 = move-exception
                r3 = r1
                r1 = r0
                r0 = r3
                goto L_0x00d8
            */
            throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.b.f.g.a.a(com.shoujiduoduo.base.bean.RingData):void");
        }

        public void onCreate(SQLiteDatabase sQLiteDatabase) {
        }

        public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        }
    }

    /* renamed from: c */
    public RingData a(int i2) {
        if (i2 < 0 || i2 >= this.h.size()) {
            return null;
        }
        return this.h.get(i2);
    }

    public int c() {
        return this.h.size();
    }

    public boolean d() {
        return false;
    }

    public void e() {
    }

    public boolean g() {
        return false;
    }

    public f.a b() {
        return f.a.list_user_favorite;
    }

    public void f() {
    }
}
