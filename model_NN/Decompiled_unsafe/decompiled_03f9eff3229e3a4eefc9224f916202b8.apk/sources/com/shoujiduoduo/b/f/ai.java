package com.shoujiduoduo.b.f;

import com.shoujiduoduo.a.a.b;
import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.base.bean.MakeRingData;
import com.shoujiduoduo.util.i;
import java.util.Timer;

/* compiled from: RingUploader */
public class ai {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Timer f793a;
    /* access modifiers changed from: private */
    public int b;

    static /* synthetic */ int a(ai aiVar) {
        int i = aiVar.b;
        aiVar.b = i + 1;
        return i;
    }

    public void a(MakeRingData makeRingData, int i) {
        x.a().b(b.OBSERVER_RING_UPLOAD, new aj(this, makeRingData));
        i.a(new ak(this, makeRingData, i));
    }

    /* access modifiers changed from: private */
    public void a(MakeRingData makeRingData) {
        this.f793a.cancel();
        x.a().b(b.OBSERVER_RING_UPLOAD, new am(this, makeRingData));
    }

    /* access modifiers changed from: private */
    public void b(MakeRingData makeRingData) {
        this.f793a.cancel();
        x.a().b(b.OBSERVER_RING_UPLOAD, new an(this, makeRingData));
    }

    /* access modifiers changed from: private */
    public void b(MakeRingData makeRingData, int i) {
        x.a().b(b.OBSERVER_RING_UPLOAD, new ao(this, makeRingData, i));
    }
}
