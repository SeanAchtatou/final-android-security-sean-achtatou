package com.shoujiduoduo.b.f;

import cn.banshenggua.aichang.utils.StringUtil;
import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.a.b.b;
import com.shoujiduoduo.a.c.p;
import com.shoujiduoduo.a.c.u;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.base.bean.MakeRingData;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.base.bean.c;
import com.shoujiduoduo.base.bean.f;
import com.shoujiduoduo.util.i;
import com.shoujiduoduo.util.l;
import com.shoujiduoduo.util.w;
import com.sina.weibo.sdk.constant.WBConstants;
import com.sina.weibo.sdk.register.mobile.SelectCountryActivity;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/* compiled from: MakeRingList */
public class v implements c {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final String f809a = v.class.getSimpleName();
    private static final String b = l.b(2);
    private int c = -1;
    private int d = -1;
    private int e = -1;
    /* access modifiers changed from: private */
    public ArrayList<RingData> f;
    /* access modifiers changed from: private */
    public boolean g;
    private u h = new z(this);
    private p i = new ab(this);

    public v() {
        a.a(f809a, "UserRingMake initializer.");
        this.f = new ArrayList<>();
    }

    public boolean h() {
        return this.g;
    }

    public void i() {
        a.b(f809a, "begin init makering data");
        i.a(new w(this, b.g().g()));
        a.b(f809a, "end init makering data");
    }

    public void j() {
        x.a().a(com.shoujiduoduo.a.a.b.OBSERVER_USER_CENTER, this.h);
        x.a().a(com.shoujiduoduo.a.a.b.OBSERVER_RING_UPLOAD, this.i);
    }

    public void k() {
        x.a().b(com.shoujiduoduo.a.a.b.OBSERVER_USER_CENTER, this.h);
        x.a().b(com.shoujiduoduo.a.a.b.OBSERVER_RING_UPLOAD, this.i);
    }

    public String a() {
        return "user_make";
    }

    public int l() {
        return this.f.size();
    }

    public boolean a(String str) {
        synchronized (f809a) {
            for (int i2 = 0; i2 < this.f.size(); i2++) {
                if (str.equalsIgnoreCase(this.f.get(i2).g)) {
                    return true;
                }
            }
            return false;
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0166, code lost:
        r8.j = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x016b, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:?, code lost:
        r1.printStackTrace();
        r1 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:?, code lost:
        r8.i = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x018c, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:?, code lost:
        r1.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x0190, code lost:
        r1 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x0198, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:?, code lost:
        r1.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x019c, code lost:
        r1 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x019e, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:?, code lost:
        r8.d = 0;
        r8.f814a = 0;
        r1.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x01a9, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:?, code lost:
        r1.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x01ad, code lost:
        r1 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x01e6, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:?, code lost:
        r1.printStackTrace();
        r1 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x01ec, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x01ed, code lost:
        r1.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x01f0, code lost:
        r1 = true;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x016b A[ExcHandler: FileNotFoundException (r1v18 'e' java.io.FileNotFoundException A[CUSTOM_DECLARE]), Splitter:B:18:0x002a] */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x018c A[ExcHandler: ParserConfigurationException (r1v11 'e' javax.xml.parsers.ParserConfigurationException A[CUSTOM_DECLARE]), Splitter:B:18:0x002a] */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x0198 A[ExcHandler: SAXException (r1v9 'e' org.xml.sax.SAXException A[CUSTOM_DECLARE]), Splitter:B:18:0x002a] */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x01a9 A[ExcHandler: IOException (r1v7 'e' java.io.IOException A[CUSTOM_DECLARE]), Splitter:B:18:0x002a] */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x01ec A[Catch:{ IOException -> 0x0016 }, ExcHandler: NullPointerException (r1v3 'e' java.lang.NullPointerException A[CUSTOM_DECLARE, Catch:{  }]), Splitter:B:18:0x002a] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized java.util.ArrayList<com.shoujiduoduo.base.bean.RingData> n() {
        /*
            r10 = this;
            r3 = 1
            r2 = 0
            monitor-enter(r10)
            java.io.File r0 = new java.io.File     // Catch:{ all -> 0x001b }
            java.lang.String r1 = com.shoujiduoduo.b.f.v.b     // Catch:{ all -> 0x001b }
            r0.<init>(r1)     // Catch:{ all -> 0x001b }
            boolean r1 = r0.exists()     // Catch:{ all -> 0x001b }
            if (r1 != 0) goto L_0x001e
            r0.createNewFile()     // Catch:{ IOException -> 0x0016 }
        L_0x0013:
            r0 = 0
        L_0x0014:
            monitor-exit(r10)
            return r0
        L_0x0016:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x001b }
            goto L_0x0013
        L_0x001b:
            r0 = move-exception
            monitor-exit(r10)
            throw r0
        L_0x001e:
            java.lang.String r0 = com.shoujiduoduo.b.f.v.f809a     // Catch:{ all -> 0x001b }
            java.lang.String r1 = "UserRingMake: read begin"
            com.shoujiduoduo.base.a.a.a(r0, r1)     // Catch:{ all -> 0x001b }
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ all -> 0x001b }
            r0.<init>()     // Catch:{ all -> 0x001b }
            java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            java.lang.String r4 = com.shoujiduoduo.b.f.v.b     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            r1.<init>(r4)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            javax.xml.parsers.DocumentBuilderFactory r4 = javax.xml.parsers.DocumentBuilderFactory.newInstance()     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            javax.xml.parsers.DocumentBuilder r4 = r4.newDocumentBuilder()     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            org.w3c.dom.Document r1 = r4.parse(r1)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            org.w3c.dom.Element r1 = r1.getDocumentElement()     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            java.lang.String r4 = "phone_sel"
            java.lang.String r4 = r1.getAttribute(r4)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            int r4 = java.lang.Integer.parseInt(r4)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            r10.c = r4     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            java.lang.String r4 = "alarm_sel"
            java.lang.String r4 = r1.getAttribute(r4)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            int r4 = java.lang.Integer.parseInt(r4)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            r10.d = r4     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            java.lang.String r4 = "notification_sel"
            java.lang.String r4 = r1.getAttribute(r4)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            int r4 = java.lang.Integer.parseInt(r4)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            r10.e = r4     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            java.lang.String r4 = "ring"
            org.w3c.dom.NodeList r6 = r1.getElementsByTagName(r4)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            r5 = r2
        L_0x006c:
            int r1 = r6.getLength()     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            if (r5 >= r1) goto L_0x01af
            org.w3c.dom.Node r1 = r6.item(r5)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            org.w3c.dom.NamedNodeMap r7 = r1.getAttributes()     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            com.shoujiduoduo.base.bean.MakeRingData r8 = new com.shoujiduoduo.base.bean.MakeRingData     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            r8.<init>()     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            java.lang.String r1 = "name"
            java.lang.String r1 = com.shoujiduoduo.util.f.a(r7, r1)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            r8.e = r1     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            java.lang.String r1 = "artist"
            java.lang.String r1 = com.shoujiduoduo.util.f.a(r7, r1)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            r8.f = r1     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            java.lang.String r1 = "duration"
            java.lang.String r1 = com.shoujiduoduo.util.f.a(r7, r1)     // Catch:{ NumberFormatException -> 0x0165, FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NullPointerException -> 0x01ec }
            int r1 = java.lang.Integer.parseInt(r1)     // Catch:{ NumberFormatException -> 0x0165, FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NullPointerException -> 0x01ec }
            r8.j = r1     // Catch:{ NumberFormatException -> 0x0165, FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NullPointerException -> 0x01ec }
        L_0x009b:
            java.lang.String r1 = "score"
            java.lang.String r1 = com.shoujiduoduo.util.f.a(r7, r1)     // Catch:{ NumberFormatException -> 0x0186, FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NullPointerException -> 0x01ec }
            int r1 = java.lang.Integer.parseInt(r1)     // Catch:{ NumberFormatException -> 0x0186, FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NullPointerException -> 0x01ec }
            r8.i = r1     // Catch:{ NumberFormatException -> 0x0186, FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NullPointerException -> 0x01ec }
        L_0x00a7:
            java.lang.String r1 = "playcnt"
            java.lang.String r1 = com.shoujiduoduo.util.f.a(r7, r1)     // Catch:{ NumberFormatException -> 0x0192, FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NullPointerException -> 0x01ec }
            int r1 = java.lang.Integer.parseInt(r1)     // Catch:{ NumberFormatException -> 0x0192, FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NullPointerException -> 0x01ec }
            r8.k = r1     // Catch:{ NumberFormatException -> 0x0192, FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NullPointerException -> 0x01ec }
        L_0x00b3:
            java.lang.String r1 = "cid"
            java.lang.String r1 = com.shoujiduoduo.util.f.a(r7, r1)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            r8.n = r1     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            java.lang.String r1 = "valid"
            java.lang.String r1 = com.shoujiduoduo.util.f.a(r7, r1)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            r8.o = r1     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            java.lang.String r1 = "singerId"
            java.lang.String r1 = com.shoujiduoduo.util.f.a(r7, r1)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            r8.r = r1     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            java.lang.String r1 = "price"
            java.lang.String r1 = com.shoujiduoduo.util.f.a(r7, r1)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            r4 = 200(0xc8, float:2.8E-43)
            int r1 = com.shoujiduoduo.util.u.a(r1, r4)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            r8.p = r1     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            java.lang.String r1 = "hasmedia"
            java.lang.String r1 = com.shoujiduoduo.util.f.a(r7, r1)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            r4 = 0
            int r1 = com.shoujiduoduo.util.u.a(r1, r4)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            r8.q = r1     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            java.lang.String r1 = "ctcid"
            java.lang.String r1 = com.shoujiduoduo.util.f.a(r7, r1)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            r8.s = r1     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            java.lang.String r1 = "ctvalid"
            java.lang.String r1 = com.shoujiduoduo.util.f.a(r7, r1)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            r8.t = r1     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            java.lang.String r1 = "ctprice"
            java.lang.String r1 = com.shoujiduoduo.util.f.a(r7, r1)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            r4 = 200(0xc8, float:2.8E-43)
            int r1 = com.shoujiduoduo.util.u.a(r1, r4)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            r8.u = r1     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            java.lang.String r1 = "cthasmedia"
            java.lang.String r1 = com.shoujiduoduo.util.f.a(r7, r1)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            r4 = 0
            int r1 = com.shoujiduoduo.util.u.a(r1, r4)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            r8.v = r1     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            java.lang.String r1 = "localPath"
            java.lang.String r1 = com.shoujiduoduo.util.f.a(r7, r1)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            r8.m = r1     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            java.lang.String r1 = "makeDate"
            java.lang.String r1 = com.shoujiduoduo.util.f.a(r7, r1)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            r8.c = r1     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            java.lang.String r1 = "makeType"
            java.lang.String r4 = com.shoujiduoduo.util.f.a(r7, r1)     // Catch:{ NumberFormatException -> 0x019e, FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NullPointerException -> 0x01ec }
            java.lang.String r1 = "upload"
            java.lang.String r1 = com.shoujiduoduo.util.f.a(r7, r1)     // Catch:{ NumberFormatException -> 0x019e, FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NullPointerException -> 0x01ec }
            java.lang.String r9 = ""
            boolean r9 = r4.equals(r9)     // Catch:{ NumberFormatException -> 0x019e, FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NullPointerException -> 0x01ec }
            if (r9 == 0) goto L_0x0137
            java.lang.String r4 = "0"
        L_0x0137:
            int r4 = java.lang.Integer.parseInt(r4)     // Catch:{ NumberFormatException -> 0x019e, FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NullPointerException -> 0x01ec }
            r8.d = r4     // Catch:{ NumberFormatException -> 0x019e, FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NullPointerException -> 0x01ec }
            java.lang.String r4 = ""
            boolean r4 = r1.equals(r4)     // Catch:{ NumberFormatException -> 0x019e, FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NullPointerException -> 0x01ec }
            if (r4 == 0) goto L_0x0147
            java.lang.String r1 = "0"
        L_0x0147:
            int r1 = java.lang.Integer.parseInt(r1)     // Catch:{ NumberFormatException -> 0x019e, FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NullPointerException -> 0x01ec }
            r8.f814a = r1     // Catch:{ NumberFormatException -> 0x019e, FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NullPointerException -> 0x01ec }
        L_0x014d:
            java.lang.String r1 = "rid"
            java.lang.String r1 = com.shoujiduoduo.util.f.a(r7, r1)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            r8.g = r1     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            java.lang.String r1 = "bdurl"
            java.lang.String r1 = com.shoujiduoduo.util.f.a(r7, r1)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            r8.h = r1     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            r0.add(r8)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            int r1 = r5 + 1
            r5 = r1
            goto L_0x006c
        L_0x0165:
            r1 = move-exception
            r1 = 0
            r8.j = r1     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            goto L_0x009b
        L_0x016b:
            r1 = move-exception
            r1.printStackTrace()     // Catch:{ all -> 0x001b }
            r1 = r3
        L_0x0170:
            if (r1 == 0) goto L_0x017b
            r1 = -1
            r10.c = r1     // Catch:{ all -> 0x001b }
            r1 = -1
            r10.d = r1     // Catch:{ all -> 0x001b }
            r1 = -1
            r10.e = r1     // Catch:{ all -> 0x001b }
        L_0x017b:
            int r1 = r0.size()     // Catch:{ all -> 0x001b }
            if (r1 <= r3) goto L_0x0014
            r10.b(r0)     // Catch:{ all -> 0x001b }
            goto L_0x0014
        L_0x0186:
            r1 = move-exception
            r1 = 0
            r8.i = r1     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            goto L_0x00a7
        L_0x018c:
            r1 = move-exception
            r1.printStackTrace()     // Catch:{ all -> 0x001b }
            r1 = r3
            goto L_0x0170
        L_0x0192:
            r1 = move-exception
            r1 = 0
            r8.k = r1     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            goto L_0x00b3
        L_0x0198:
            r1 = move-exception
            r1.printStackTrace()     // Catch:{ all -> 0x001b }
            r1 = r3
            goto L_0x0170
        L_0x019e:
            r1 = move-exception
            r4 = 0
            r8.d = r4     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            r4 = 0
            r8.f814a = r4     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            r1.printStackTrace()     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            goto L_0x014d
        L_0x01a9:
            r1 = move-exception
            r1.printStackTrace()     // Catch:{ all -> 0x001b }
            r1 = r3
            goto L_0x0170
        L_0x01af:
            java.util.HashMap r1 = new java.util.HashMap     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            r1.<init>()     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            android.content.Context r4 = com.shoujiduoduo.ringtone.RingDDApp.c()     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            java.lang.String r5 = "MAKE_RING_NUM"
            int r6 = r0.size()     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            long r6 = (long) r6     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            com.shoujiduoduo.util.f.a(r4, r5, r1, r6)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            java.lang.String r1 = com.shoujiduoduo.b.f.v.f809a     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            r4.<init>()     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            java.lang.String r5 = "read "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            int r5 = r10.l()     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            java.lang.String r5 = " rings."
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            java.lang.String r4 = r4.toString()     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            com.shoujiduoduo.base.a.a.a(r1, r4)     // Catch:{ FileNotFoundException -> 0x016b, ParserConfigurationException -> 0x018c, SAXException -> 0x0198, IOException -> 0x01a9, NumberFormatException -> 0x01e6, NullPointerException -> 0x01ec }
            r1 = r2
            goto L_0x0170
        L_0x01e6:
            r1 = move-exception
            r1.printStackTrace()     // Catch:{ all -> 0x001b }
            r1 = r3
            goto L_0x0170
        L_0x01ec:
            r1 = move-exception
            r1.printStackTrace()     // Catch:{ all -> 0x001b }
            r1 = r3
            goto L_0x0170
        */
        throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.b.f.v.n():java.util.ArrayList");
    }

    /* access modifiers changed from: private */
    public void o() {
        i.a(new ac(this));
    }

    /* access modifiers changed from: private */
    public synchronized boolean p() {
        boolean z;
        try {
            Document newDocument = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
            Element createElement = newDocument.createElement("list");
            createElement.setAttribute("num", String.valueOf(this.f.size()));
            createElement.setAttribute("phone_sel", "" + this.c);
            createElement.setAttribute("alarm_sel", "" + this.d);
            createElement.setAttribute("notification_sel", "" + this.e);
            newDocument.appendChild(createElement);
            for (int i2 = 0; i2 < this.f.size(); i2++) {
                MakeRingData makeRingData = (MakeRingData) this.f.get(i2);
                Element createElement2 = newDocument.createElement("ring");
                createElement2.setAttribute(SelectCountryActivity.EXTRA_COUNTRY_NAME, makeRingData.e);
                createElement2.setAttribute("artist", makeRingData.f);
                createElement2.setAttribute("duration", String.valueOf(makeRingData.j));
                createElement2.setAttribute(WBConstants.GAME_PARAMS_SCORE, String.valueOf(makeRingData.i));
                createElement2.setAttribute("playcnt", String.valueOf(makeRingData.k));
                createElement2.setAttribute("rid", makeRingData.g);
                createElement2.setAttribute("bdurl", makeRingData.h);
                createElement2.setAttribute("localPath", makeRingData.m);
                createElement2.setAttribute("makeType", String.valueOf(makeRingData.d));
                createElement2.setAttribute("makeDate", makeRingData.c);
                createElement2.setAttribute("upload", String.valueOf(makeRingData.f814a));
                createElement.appendChild(createElement2);
            }
            Transformer newTransformer = TransformerFactory.newInstance().newTransformer();
            newTransformer.setOutputProperty("encoding", StringUtil.Encoding);
            newTransformer.setOutputProperty("indent", "yes");
            newTransformer.setOutputProperty("standalone", "yes");
            newTransformer.setOutputProperty("method", "xml");
            newTransformer.transform(new DOMSource(newDocument.getDocumentElement()), new StreamResult(new FileOutputStream(new File(b))));
            z = true;
        } catch (ParserConfigurationException e2) {
            e2.printStackTrace();
            z = false;
            return z;
        } catch (TransformerConfigurationException e3) {
            e3.printStackTrace();
            z = false;
            return z;
        } catch (FileNotFoundException e4) {
            e4.printStackTrace();
            z = false;
            return z;
        } catch (TransformerException e5) {
            e5.printStackTrace();
            z = false;
            return z;
        } catch (Exception e6) {
            e6.printStackTrace();
            z = false;
            return z;
        }
        return z;
    }

    public synchronized boolean a(Collection<Integer> collection) {
        ArrayList arrayList = new ArrayList();
        StringBuilder sb = new StringBuilder();
        for (Integer next : collection) {
            arrayList.add((MakeRingData) this.f.get(next.intValue()));
            if (!((MakeRingData) this.f.get(next.intValue())).g.equals("")) {
                sb.append(this.f.get(next.intValue()).g);
                sb.append("|");
            }
            try {
                File file = new File(((MakeRingData) this.f.get(next.intValue())).m);
                if (file.exists()) {
                    file.delete();
                }
            } catch (Exception e2) {
            }
        }
        if (sb.toString().endsWith("|")) {
            sb.deleteCharAt(sb.length() - 1);
        }
        w.d(sb.toString());
        this.f.removeAll(arrayList);
        q();
        o();
        return true;
    }

    public synchronized boolean b(int i2) {
        boolean z;
        if (i2 >= this.f.size() || i2 < 0) {
            z = false;
        } else {
            try {
                File file = new File(((MakeRingData) this.f.get(i2)).m);
                if (file.exists()) {
                    file.delete();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            w.d(this.f.get(i2).g);
            this.f.remove(i2);
            q();
            o();
            z = true;
        }
        return z;
    }

    /* access modifiers changed from: private */
    public synchronized void q() {
        x.a().a(com.shoujiduoduo.a.a.b.OBSERVER_LIST_DATA, new ad(this));
    }

    public synchronized boolean a(MakeRingData makeRingData) {
        boolean addAll;
        ArrayList arrayList = new ArrayList();
        arrayList.add(makeRingData);
        addAll = this.f.addAll(0, arrayList);
        q();
        o();
        return addAll;
    }

    /* access modifiers changed from: private */
    public synchronized void r() {
        Iterator<RingData> it = this.f.iterator();
        while (it.hasNext()) {
            if (((MakeRingData) it.next()).m.equals("")) {
                it.remove();
            }
        }
    }

    /* access modifiers changed from: private */
    public synchronized ArrayList<RingData> a(ArrayList<MakeRingData> arrayList) {
        ArrayList<RingData> arrayList2;
        boolean z;
        arrayList2 = new ArrayList<>();
        Iterator<RingData> it = this.f.iterator();
        while (it.hasNext()) {
            RingData next = it.next();
            if (!next.m.equals("")) {
                MakeRingData makeRingData = new MakeRingData();
                makeRingData.a(next);
                arrayList2.add(makeRingData);
            }
        }
        Iterator<MakeRingData> it2 = arrayList.iterator();
        while (it2.hasNext()) {
            RingData next2 = it2.next();
            Iterator<RingData> it3 = this.f.iterator();
            while (true) {
                if (!it3.hasNext()) {
                    z = false;
                    break;
                }
                RingData next3 = it3.next();
                if (next2.g.equals(next3.g)) {
                    if (((MakeRingData) next3).m.equals("")) {
                        MakeRingData makeRingData2 = new MakeRingData();
                        makeRingData2.a(next3);
                        arrayList2.add(makeRingData2);
                        z = true;
                    } else {
                        z = true;
                    }
                }
            }
            if (!z) {
                MakeRingData makeRingData3 = new MakeRingData();
                makeRingData3.a(next2);
                arrayList2.add(makeRingData3);
            }
        }
        b(arrayList2);
        return arrayList2;
    }

    private void b(ArrayList<RingData> arrayList) {
        Collections.sort(arrayList, new ae(this, new SimpleDateFormat("yyyy-MM-dd HH:mm")));
    }

    /* access modifiers changed from: private */
    public void s() {
        i.a(new af(this));
    }

    /* renamed from: c */
    public RingData a(int i2) {
        if (i2 < 0 || i2 >= this.f.size()) {
            return null;
        }
        return this.f.get(i2);
    }

    public int c() {
        return this.f.size();
    }

    public boolean d() {
        return false;
    }

    public void e() {
    }

    public boolean g() {
        return false;
    }

    public f.a b() {
        return f.a.list_user_make;
    }

    public void f() {
    }
}
