package com.shoujiduoduo.b.f;

import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.base.bean.MakeRingData;
import com.shoujiduoduo.base.bean.e;
import com.shoujiduoduo.util.j;
import com.shoujiduoduo.util.w;
import java.io.ByteArrayInputStream;
import java.util.ArrayList;

/* compiled from: MakeRingList */
class af implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ v f792a;

    af(v vVar) {
        this.f792a = vVar;
    }

    public void run() {
        e<MakeRingData> eVar;
        byte[] g = w.g();
        if (g == null) {
            a.c(v.f809a, "get user makering failed");
            return;
        }
        a.a(v.f809a, "获取线上数据成功");
        try {
            eVar = j.d(new ByteArrayInputStream(g));
        } catch (ArrayIndexOutOfBoundsException e) {
            a.c(v.f809a, "parse user makering exception");
            e.printStackTrace();
            eVar = null;
        }
        if (eVar != null) {
            a.a(v.f809a, "解析线上数据成功，线上数据个数：" + eVar.f821a.size());
            a.a(v.f809a, "准备merge数据");
            x.a().a(new ag(this, this.f792a.a((ArrayList<MakeRingData>) eVar.f821a)));
            a.a(v.f809a, "merge 完毕， 总共个数：" + this.f792a.f.size());
            return;
        }
        a.a(v.f809a, "解析线上数据失败");
    }
}
