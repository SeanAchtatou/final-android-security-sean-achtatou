package com.shoujiduoduo.b.a;

import android.util.Xml;
import com.shoujiduoduo.base.bean.d;
import com.shoujiduoduo.util.s;
import com.shoujiduoduo.util.t;
import com.sina.weibo.sdk.register.mobile.SelectCountryActivity;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.StringWriter;
import java.util.ArrayList;
import org.xmlpull.v1.XmlSerializer;

/* compiled from: DuoduoFamilyData */
class e extends s<ArrayList<d>> {
    e() {
    }

    e(String str) {
        super(str);
    }

    public ArrayList<d> a() {
        try {
            return f.a(new FileInputStream(c + this.b));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void a(ArrayList<d> arrayList) {
        if (arrayList != null) {
            XmlSerializer newSerializer = Xml.newSerializer();
            StringWriter stringWriter = new StringWriter();
            try {
                newSerializer.setOutput(stringWriter);
                newSerializer.startDocument("UTF-8", true);
                newSerializer.startTag("", "root");
                for (int i = 0; i < arrayList.size(); i++) {
                    d dVar = arrayList.get(i);
                    newSerializer.startTag("", "software");
                    newSerializer.attribute("", SelectCountryActivity.EXTRA_COUNTRY_NAME, dVar.b);
                    newSerializer.attribute("", "pic", dVar.f820a);
                    newSerializer.attribute("", "url", dVar.d);
                    newSerializer.text(dVar.c);
                    newSerializer.endTag("", "software");
                }
                newSerializer.endTag("", "root");
                newSerializer.endDocument();
                t.b(c + this.b, stringWriter.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
