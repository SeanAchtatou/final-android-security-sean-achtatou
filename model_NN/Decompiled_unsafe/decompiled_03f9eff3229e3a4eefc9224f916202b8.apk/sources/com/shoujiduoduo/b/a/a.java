package com.shoujiduoduo.b.a;

import com.shoujiduoduo.b.a.b;
import com.shoujiduoduo.b.a.i;
import com.shoujiduoduo.b.a.t;
import java.util.ArrayList;

/* compiled from: AdMgrImpl */
public class a implements s {

    /* renamed from: a  reason: collision with root package name */
    private b f733a = new b();
    private t b = new t();
    private i c = new i();

    public void a() {
        this.f733a.a();
        this.b.a();
        this.c.a();
    }

    public void b() {
        this.f733a.c();
        this.b.b();
        this.c.c();
    }

    public boolean c() {
        return this.f733a.b();
    }

    public ArrayList<b.a> d() {
        if (this.f733a.b()) {
            return this.f733a.d();
        }
        return null;
    }

    public int e() {
        if (this.f733a.b()) {
            return this.f733a.e();
        }
        return 0;
    }

    public boolean f() {
        return this.b.c();
    }

    public t.a a(String str) {
        if (this.b.c()) {
            return this.b.a(str);
        }
        return null;
    }

    public void g() {
        this.c.b();
    }

    public i.a h() {
        return this.c.d();
    }
}
