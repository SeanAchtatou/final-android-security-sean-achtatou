package com.shoujiduoduo.b.a;

import com.shoujiduoduo.b.a.i;
import com.shoujiduoduo.base.a.a;
import java.util.TimerTask;

/* compiled from: FeedAdData */
class j extends TimerTask {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ i f742a;

    j(i iVar) {
        this.f742a = iVar;
    }

    public void run() {
        a.a("feedAdData", "time to check ad is valid");
        synchronized (this.f742a.f) {
            i.a aVar = (i.a) this.f742a.e.peek();
            if (aVar == null || !aVar.a()) {
                a.a("feedAdData", "the first ad is not valid, so clear ad list and refetch ad");
                this.f742a.e.clear();
                this.f742a.i();
            } else {
                a.a("feedAdData", "ad is ok, do nothing");
            }
        }
    }
}
