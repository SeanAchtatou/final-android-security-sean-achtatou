package com.shoujiduoduo.b.a;

import com.baidu.mobad.feeds.BaiduNative;
import com.baidu.mobad.feeds.NativeErrorCode;
import com.baidu.mobad.feeds.NativeResponse;
import com.baidu.mobad.feeds.RequestParameters;
import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.b.a.i;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.umeng.analytics.b;
import java.util.HashMap;
import java.util.List;

/* compiled from: FeedAdData */
class k implements BaiduNative.BaiduNativeNetworkListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ i f743a;

    k(i iVar) {
        this.f743a = iVar;
    }

    public void onNativeLoad(List<NativeResponse> list) {
        a.a("feedAdData", "baidu feed ad load success");
        if (list == null || list.size() <= 0) {
            boolean unused = this.f743a.p = false;
            a.a("feedAdData", "onNativeAdLoadSucceeded, size is 0");
            return;
        }
        a.a("feedAdData", "onNativeAdLoadSucceeded, size:" + list.size());
        HashMap hashMap = new HashMap();
        hashMap.put("status", "onNativeLoad");
        b.a(RingDDApp.c(), "baidu_feed_ad", hashMap);
        long currentTimeMillis = System.currentTimeMillis();
        synchronized (this.f743a.f) {
            for (NativeResponse aVar : list) {
                i.a aVar2 = new i.a(aVar, currentTimeMillis, (long) this.f743a.d, 1);
                aVar2.a(this.f743a.j());
                this.f743a.e.add(aVar2);
            }
        }
        if (!this.f743a.m) {
            x.a().b(com.shoujiduoduo.a.a.b.OBSERVER_FEED_AD, new l(this));
            boolean unused2 = this.f743a.m = true;
        }
        a.a("feedAdData", "ad list size:" + this.f743a.e.size());
        if (this.f743a.e.size() < this.f743a.f740a) {
            a.a("feedAdData", "ad list size is < " + this.f743a.f740a + ", so fetch more data");
            boolean unused3 = this.f743a.p = true;
            ((BaiduNative) this.f743a.i).makeRequest((RequestParameters) this.f743a.j);
            return;
        }
        boolean unused4 = this.f743a.p = false;
    }

    public void onNativeFail(NativeErrorCode nativeErrorCode) {
        if (nativeErrorCode != null) {
            HashMap hashMap = new HashMap();
            hashMap.put("status", "onNativeFail");
            hashMap.put("errCode", nativeErrorCode.toString());
            b.a(RingDDApp.c(), "baidu_feed_ad", hashMap);
            a.a("feedAdData", "baidu feed ad load failed, errcode:" + nativeErrorCode.toString());
        }
        a.a("feedAdData", "baidu feed ad load failed, try tad");
    }
}
