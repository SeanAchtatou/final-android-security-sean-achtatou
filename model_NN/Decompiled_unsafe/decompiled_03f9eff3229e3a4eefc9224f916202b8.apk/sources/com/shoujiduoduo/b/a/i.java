package com.shoujiduoduo.b.a;

import android.view.View;
import cn.banshenggua.aichang.utils.Constants;
import com.baidu.mobad.feeds.BaiduNative;
import com.baidu.mobad.feeds.NativeResponse;
import com.baidu.mobad.feeds.RequestParameters;
import com.duoduo.dynamicdex.DuoMobAdUtils;
import com.duoduo.mobads.IBaiduNative;
import com.duoduo.mobads.INativeResponse;
import com.qhad.ads.sdk.adcore.Qhad;
import com.qhad.ads.sdk.interfaces.IQhNativeAd;
import com.qhad.ads.sdk.interfaces.IQhNativeAdLoader;
import com.qq.e.ads.nativ.NativeAD;
import com.qq.e.ads.nativ.NativeADDataRef;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ringtone.activity.RingToneDuoduoActivity;
import com.shoujiduoduo.util.am;
import com.shoujiduoduo.util.av;
import com.shoujiduoduo.util.u;
import com.tencent.open.SocialConstants;
import com.umeng.analytics.b;
import java.util.LinkedList;
import java.util.Timer;

/* compiled from: FeedAdData */
public class i {
    private static int b = 1800000;
    private static int c = 1800;
    private static int n = 3;
    private static int o = 1;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public int f740a;
    /* access modifiers changed from: private */
    public int d;
    /* access modifiers changed from: private */
    public LinkedList<a> e = new LinkedList<>();
    /* access modifiers changed from: private */
    public final byte[] f = new byte[0];
    /* access modifiers changed from: private */
    public Object g;
    /* access modifiers changed from: private */
    public Object h;
    /* access modifiers changed from: private */
    public Object i;
    /* access modifiers changed from: private */
    public Object j;
    private Timer k;
    private int l;
    /* access modifiers changed from: private */
    public volatile boolean m;
    /* access modifiers changed from: private */
    public boolean p;

    /* compiled from: FeedAdData */
    public static class a {

        /* renamed from: a  reason: collision with root package name */
        private Object f741a;
        private long b;
        private long c;
        private int d;
        private int e;
        private int f;

        public a() {
        }

        public a(Object obj, long j, long j2, int i) {
            this.f741a = obj;
            this.b = j;
            this.c = j2;
            this.d = i;
        }

        public boolean a() {
            boolean z;
            switch (this.d) {
                case 1:
                    try {
                        return ((NativeResponse) this.f741a).isAdAvailable(RingDDApp.c());
                    } catch (Exception e2) {
                        b.a(RingDDApp.c(), e2);
                        e2.printStackTrace();
                        return false;
                    }
                case 2:
                case 4:
                    if (System.currentTimeMillis() - this.b < this.c) {
                        z = true;
                    } else {
                        z = false;
                    }
                    return z;
                case 3:
                    try {
                        return ((INativeResponse) this.f741a).isAdAvailable(RingDDApp.c());
                    } catch (Exception e3) {
                        b.a(RingDDApp.c(), e3);
                        e3.printStackTrace();
                        return false;
                    }
                default:
                    return false;
            }
        }

        public void b() {
            this.e++;
        }

        public void a(int i) {
            this.f = i;
        }

        public boolean c() {
            return this.e >= this.f;
        }

        public String d() {
            switch (this.d) {
                case 1:
                    return ((NativeResponse) this.f741a).getTitle();
                case 2:
                    return ((IQhNativeAd) this.f741a).getContent().optString("title", "");
                case 3:
                    return ((INativeResponse) this.f741a).getTitle();
                case 4:
                    return ((NativeADDataRef) this.f741a).getTitle();
                default:
                    return "";
            }
        }

        public String e() {
            switch (this.d) {
                case 1:
                    return ((NativeResponse) this.f741a).getDesc();
                case 2:
                    return ((IQhNativeAd) this.f741a).getContent().optString(SocialConstants.PARAM_APP_DESC, "");
                case 3:
                    return ((INativeResponse) this.f741a).getDesc();
                case 4:
                    return ((NativeADDataRef) this.f741a).getDesc();
                default:
                    return "";
            }
        }

        public String f() {
            switch (this.d) {
                case 1:
                    return ((NativeResponse) this.f741a).getIconUrl();
                case 2:
                    return ((IQhNativeAd) this.f741a).getContent().optString("contentimg", "");
                case 3:
                    return ((INativeResponse) this.f741a).getIconUrl();
                case 4:
                    return ((NativeADDataRef) this.f741a).getIconUrl();
                default:
                    return "";
            }
        }

        public void a(View view) {
            switch (this.d) {
                case 1:
                    b.b(RingDDApp.c(), "baidu_feed_ad_show");
                    ((NativeResponse) this.f741a).recordImpression(view);
                    return;
                case 2:
                    b.b(RingDDApp.c(), "qihu_feed_ad_show");
                    ((IQhNativeAd) this.f741a).onAdShowed();
                    return;
                case 3:
                    b.b(RingDDApp.c(), "duomob_feed_ad_show");
                    ((INativeResponse) this.f741a).recordImpression(view);
                    return;
                case 4:
                    b.b(RingDDApp.c(), "gdt_feed_ad_show");
                    ((NativeADDataRef) this.f741a).onExposured(view);
                    return;
                default:
                    return;
            }
        }

        public void b(View view) {
            switch (this.d) {
                case 1:
                    b.b(RingDDApp.c(), "baidu_feed_ad_click");
                    ((NativeResponse) this.f741a).handleClick(view);
                    return;
                case 2:
                    b.b(RingDDApp.c(), "qihu_feed_ad_click");
                    ((IQhNativeAd) this.f741a).onAdClicked();
                    return;
                case 3:
                    b.b(RingDDApp.c(), "duomob_feed_ad_click");
                    ((INativeResponse) this.f741a).handleClick(view);
                    return;
                case 4:
                    b.b(RingDDApp.c(), "gdt_feed_ad_click");
                    ((NativeADDataRef) this.f741a).onClicked(view);
                    return;
                default:
                    return;
            }
        }
    }

    public void a() {
        String a2 = am.a().a("feed_ad_type");
        if ("duoduo".contains("360")) {
            a2 = "360";
        }
        if (a2.equalsIgnoreCase("baidu")) {
            this.l = 1;
        } else if (a2.equalsIgnoreCase("360")) {
            this.l = 2;
        } else if (a2.equals("gdt")) {
            this.l = 4;
        } else {
            com.shoujiduoduo.base.a.a.c("feedAdData", "not support ad type, use default value baidu");
            this.l = 1;
        }
        String str = "";
        switch (this.l) {
            case 1:
                str = "baidu";
                break;
            case 2:
                str = "qihu";
                break;
            case 3:
                str = "duomob";
                break;
            case 4:
                str = "gdt";
                break;
        }
        com.shoujiduoduo.base.a.a.a("feedAdData", "ad type is :" + str);
        n = u.a(b.c(RingDDApp.c(), "baidu_feed_ad_reuse_times"), 3);
        o = u.a(b.c(RingDDApp.c(), "qihu_feed_ad_reuse_times"), 3);
    }

    public void b() {
        if (com.shoujiduoduo.util.a.d()) {
            if (this.l == 2) {
                h();
            } else if (this.l == 1) {
                e();
            } else if (this.l == 3) {
                g();
            } else if (this.l == 4) {
                f();
            } else {
                return;
            }
            this.k = new Timer();
            com.shoujiduoduo.base.a.a.a("feedAdData", "ad valid duration is :" + this.d);
            this.k.schedule(new j(this), 5000, (long) (this.d / 2));
            return;
        }
        com.shoujiduoduo.base.a.a.a("feedAdData", "not support feed ad");
    }

    private void e() {
        String c2 = b.c(RingDDApp.c(), "baidu_feed_ad_valid_time");
        if (av.c(c2)) {
            this.d = b;
        } else {
            this.d = u.a(c2, c) * Constants.CLEARIMGED;
        }
        String c3 = b.c(RingDDApp.c(), "baidu_feed_ad_list_capacity");
        if (av.c(c3)) {
            this.f740a = 20;
        } else {
            this.f740a = u.a(c3, 20);
        }
        this.i = new BaiduNative(RingToneDuoduoActivity.a(), "2417956", new k(this));
        boolean equals = "true".equals(b.c(RingDDApp.c(), "feed_ad_confirm_down"));
        com.shoujiduoduo.base.a.a.a("feedAdData", "baidu feed ad confirmdown:" + equals);
        this.j = new RequestParameters.Builder().confirmDownloading(equals).build();
        this.p = true;
        ((BaiduNative) this.i).makeRequest((RequestParameters) this.j);
    }

    private void f() {
        String c2 = b.c(RingDDApp.c(), "gdt_feed_ad_valid_time");
        if (av.c(c2)) {
            this.d = b;
        } else {
            this.d = u.a(c2, c) * Constants.CLEARIMGED;
        }
        String c3 = b.c(RingDDApp.c(), "gdt_feed_ad_list_capacity");
        if (av.c(c3)) {
            this.f740a = 20;
        } else {
            this.f740a = u.a(c3, 20);
        }
        this.g = new NativeAD(RingToneDuoduoActivity.a(), "1101336966", "1060808460679917", new m(this));
        this.p = true;
        ((NativeAD) this.g).loadAD(10);
    }

    private void g() {
        com.shoujiduoduo.base.a.a.a("feedAdData", "init Duomob Feed ad");
        String c2 = b.c(RingDDApp.c(), "baidu_feed_ad_valid_time");
        if (av.c(c2)) {
            this.d = b;
        } else {
            this.d = u.a(c2, c) * Constants.CLEARIMGED;
        }
        String c3 = b.c(RingDDApp.c(), "baidu_feed_ad_list_capacity");
        if (av.c(c3)) {
            this.f740a = 20;
        } else {
            this.f740a = u.a(c3, 20);
        }
        this.i = DuoMobAdUtils.Ins.BaiduIns.getNativeAdIns(RingToneDuoduoActivity.a(), "d7d3402a", "2652329", new o(this));
        com.shoujiduoduo.base.a.a.a("feedAdData", "Duomob feed ad confirmdown:" + "true".equals(b.c(RingDDApp.c(), "feed_ad_confirm_down")));
        this.p = true;
        if (this.i != null) {
            ((IBaiduNative) this.i).makeRequest();
        } else {
            com.shoujiduoduo.base.a.a.a("feedAdData", "mBaiduAdLoader is null");
        }
    }

    private void h() {
        String c2 = b.c(RingDDApp.c(), "360_feed_ad_valid_time");
        if (av.c(c2)) {
            this.d = b;
        } else {
            this.d = u.a(c2, c) * Constants.CLEARIMGED;
        }
        String c3 = b.c(RingDDApp.c(), "360_feed_ad_list_capacity");
        if (av.c(c3)) {
            this.f740a = 20;
        } else {
            this.f740a = u.a(c3, 20);
        }
        this.h = Qhad.initNativeAdLoader(RingToneDuoduoActivity.a(), "uaaGFTRq7O", new q(this), false);
        if (this.h != null) {
            this.p = true;
            ((IQhNativeAdLoader) this.h).loadAds();
        }
    }

    public void c() {
        if (this.k != null) {
            this.k.cancel();
        }
    }

    /* access modifiers changed from: private */
    public void i() {
        switch (this.l) {
            case 1:
                if (this.i != null && !this.p) {
                    ((BaiduNative) this.i).makeRequest((RequestParameters) this.j);
                    return;
                }
                return;
            case 2:
                if (this.h != null && !this.p) {
                    ((IQhNativeAdLoader) this.h).loadAds();
                    return;
                }
                return;
            case 3:
                if (this.i != null && !this.p) {
                    ((IBaiduNative) this.i).makeRequest();
                    return;
                }
                return;
            case 4:
                if (this.g != null && !this.p) {
                    ((NativeAD) this.g).loadAD(10);
                    return;
                }
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public int j() {
        if (this.l == 1 || this.l == 3 || this.l == 4) {
            return n;
        }
        if (this.l == 2) {
            return o;
        }
        return n;
    }

    public a d() {
        a aVar;
        com.shoujiduoduo.base.a.a.a("feedAdData", "getNextAdItem");
        synchronized (this.f) {
            while (true) {
                if (this.e.size() <= 0) {
                    aVar = null;
                    break;
                }
                aVar = this.e.poll();
                if (aVar.a()) {
                    break;
                }
            }
        }
        com.shoujiduoduo.base.a.a.a("feedAdData", "ad list size is :" + this.e.size());
        if (this.e.size() < this.f740a / 2) {
            com.shoujiduoduo.base.a.a.a("feedAdData", "current list size < " + (this.f740a / 2) + ", so fetch more data");
            i();
        }
        if (aVar == null) {
            com.shoujiduoduo.base.a.a.a("feedAdData", "no valid ad, return null");
        }
        return aVar;
    }
}
