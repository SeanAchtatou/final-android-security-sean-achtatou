package com.shoujiduoduo.b.a;

import android.os.Handler;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.base.bean.d;
import com.shoujiduoduo.util.w;
import java.io.ByteArrayInputStream;
import java.util.ArrayList;

/* compiled from: DuoduoFamilyData */
public class f {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final String f737a = f.class.getSimpleName();
    /* access modifiers changed from: private */
    public ArrayList<d> b;
    /* access modifiers changed from: private */
    public e c;
    /* access modifiers changed from: private */
    public com.shoujiduoduo.a.c.f d = null;
    /* access modifiers changed from: private */
    public boolean e = false;
    /* access modifiers changed from: private */
    public boolean f = false;
    private String g;
    /* access modifiers changed from: private */
    public Handler h = new g(this);

    public f() {
        new f("duoduo_family.tmp");
    }

    public f(String str) {
        this.c = new e(str);
        this.g = str;
    }

    public void a(com.shoujiduoduo.a.c.f fVar) {
        this.d = fVar;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
        return null;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.ArrayList<com.shoujiduoduo.base.bean.d> a(java.io.InputStream r7) {
        /*
            r0 = 0
            javax.xml.parsers.DocumentBuilderFactory r1 = javax.xml.parsers.DocumentBuilderFactory.newInstance()     // Catch:{ IOException -> 0x0082, SAXException -> 0x008e, ParserConfigurationException -> 0x008c, DOMException -> 0x008a, Exception -> 0x0088, all -> 0x0086 }
            javax.xml.parsers.DocumentBuilder r1 = r1.newDocumentBuilder()     // Catch:{ IOException -> 0x0082, SAXException -> 0x008e, ParserConfigurationException -> 0x008c, DOMException -> 0x008a, Exception -> 0x0088, all -> 0x0086 }
            org.w3c.dom.Document r1 = r1.parse(r7)     // Catch:{ IOException -> 0x0082, SAXException -> 0x008e, ParserConfigurationException -> 0x008c, DOMException -> 0x008a, Exception -> 0x0088, all -> 0x0086 }
            if (r1 != 0) goto L_0x0010
        L_0x000f:
            return r0
        L_0x0010:
            org.w3c.dom.Element r1 = r1.getDocumentElement()     // Catch:{ IOException -> 0x0082, SAXException -> 0x008e, ParserConfigurationException -> 0x008c, DOMException -> 0x008a, Exception -> 0x0088, all -> 0x0086 }
            if (r1 == 0) goto L_0x000f
            java.lang.String r2 = "software"
            org.w3c.dom.NodeList r3 = r1.getElementsByTagName(r2)     // Catch:{ IOException -> 0x0082, SAXException -> 0x008e, ParserConfigurationException -> 0x008c, DOMException -> 0x008a, Exception -> 0x0088, all -> 0x0086 }
            if (r3 == 0) goto L_0x000f
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ IOException -> 0x0082, SAXException -> 0x008e, ParserConfigurationException -> 0x008c, DOMException -> 0x008a, Exception -> 0x0088, all -> 0x0086 }
            r1.<init>()     // Catch:{ IOException -> 0x0082, SAXException -> 0x008e, ParserConfigurationException -> 0x008c, DOMException -> 0x008a, Exception -> 0x0088, all -> 0x0086 }
            r2 = 0
        L_0x0024:
            int r4 = r3.getLength()     // Catch:{ IOException -> 0x0082, SAXException -> 0x008e, ParserConfigurationException -> 0x008c, DOMException -> 0x008a, Exception -> 0x0088, all -> 0x0086 }
            if (r2 >= r4) goto L_0x0084
            java.lang.String r4 = com.shoujiduoduo.b.a.f.f737a     // Catch:{ IOException -> 0x0082, SAXException -> 0x008e, ParserConfigurationException -> 0x008c, DOMException -> 0x008a, Exception -> 0x0088, all -> 0x0086 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0082, SAXException -> 0x008e, ParserConfigurationException -> 0x008c, DOMException -> 0x008a, Exception -> 0x0088, all -> 0x0086 }
            r5.<init>()     // Catch:{ IOException -> 0x0082, SAXException -> 0x008e, ParserConfigurationException -> 0x008c, DOMException -> 0x008a, Exception -> 0x0088, all -> 0x0086 }
            java.lang.String r6 = "soft "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ IOException -> 0x0082, SAXException -> 0x008e, ParserConfigurationException -> 0x008c, DOMException -> 0x008a, Exception -> 0x0088, all -> 0x0086 }
            java.lang.StringBuilder r5 = r5.append(r2)     // Catch:{ IOException -> 0x0082, SAXException -> 0x008e, ParserConfigurationException -> 0x008c, DOMException -> 0x008a, Exception -> 0x0088, all -> 0x0086 }
            java.lang.String r5 = r5.toString()     // Catch:{ IOException -> 0x0082, SAXException -> 0x008e, ParserConfigurationException -> 0x008c, DOMException -> 0x008a, Exception -> 0x0088, all -> 0x0086 }
            com.shoujiduoduo.base.a.a.a(r4, r5)     // Catch:{ IOException -> 0x0082, SAXException -> 0x008e, ParserConfigurationException -> 0x008c, DOMException -> 0x008a, Exception -> 0x0088, all -> 0x0086 }
            org.w3c.dom.Node r4 = r3.item(r2)     // Catch:{ IOException -> 0x0082, SAXException -> 0x008e, ParserConfigurationException -> 0x008c, DOMException -> 0x008a, Exception -> 0x0088, all -> 0x0086 }
            org.w3c.dom.NamedNodeMap r4 = r4.getAttributes()     // Catch:{ IOException -> 0x0082, SAXException -> 0x008e, ParserConfigurationException -> 0x008c, DOMException -> 0x008a, Exception -> 0x0088, all -> 0x0086 }
            com.shoujiduoduo.base.bean.d r5 = new com.shoujiduoduo.base.bean.d     // Catch:{ IOException -> 0x0082, SAXException -> 0x008e, ParserConfigurationException -> 0x008c, DOMException -> 0x008a, Exception -> 0x0088, all -> 0x0086 }
            r5.<init>()     // Catch:{ IOException -> 0x0082, SAXException -> 0x008e, ParserConfigurationException -> 0x008c, DOMException -> 0x008a, Exception -> 0x0088, all -> 0x0086 }
            org.w3c.dom.Node r6 = r3.item(r2)     // Catch:{ IOException -> 0x0082, SAXException -> 0x008e, ParserConfigurationException -> 0x008c, DOMException -> 0x008a, Exception -> 0x0088, all -> 0x0086 }
            org.w3c.dom.Node r6 = r6.getFirstChild()     // Catch:{ IOException -> 0x0082, SAXException -> 0x008e, ParserConfigurationException -> 0x008c, DOMException -> 0x008a, Exception -> 0x0088, all -> 0x0086 }
            if (r6 == 0) goto L_0x007d
            java.lang.String r6 = r6.getNodeValue()     // Catch:{ IOException -> 0x0082, SAXException -> 0x008e, ParserConfigurationException -> 0x008c, DOMException -> 0x008a, Exception -> 0x0088, all -> 0x0086 }
            r5.c = r6     // Catch:{ IOException -> 0x0082, SAXException -> 0x008e, ParserConfigurationException -> 0x008c, DOMException -> 0x008a, Exception -> 0x0088, all -> 0x0086 }
        L_0x005f:
            java.lang.String r6 = "name"
            java.lang.String r6 = com.shoujiduoduo.util.f.a(r4, r6)     // Catch:{ IOException -> 0x0082, SAXException -> 0x008e, ParserConfigurationException -> 0x008c, DOMException -> 0x008a, Exception -> 0x0088, all -> 0x0086 }
            r5.b = r6     // Catch:{ IOException -> 0x0082, SAXException -> 0x008e, ParserConfigurationException -> 0x008c, DOMException -> 0x008a, Exception -> 0x0088, all -> 0x0086 }
            java.lang.String r6 = "pic"
            java.lang.String r6 = com.shoujiduoduo.util.f.a(r4, r6)     // Catch:{ IOException -> 0x0082, SAXException -> 0x008e, ParserConfigurationException -> 0x008c, DOMException -> 0x008a, Exception -> 0x0088, all -> 0x0086 }
            r5.f820a = r6     // Catch:{ IOException -> 0x0082, SAXException -> 0x008e, ParserConfigurationException -> 0x008c, DOMException -> 0x008a, Exception -> 0x0088, all -> 0x0086 }
            java.lang.String r6 = "url"
            java.lang.String r4 = com.shoujiduoduo.util.f.a(r4, r6)     // Catch:{ IOException -> 0x0082, SAXException -> 0x008e, ParserConfigurationException -> 0x008c, DOMException -> 0x008a, Exception -> 0x0088, all -> 0x0086 }
            r5.d = r4     // Catch:{ IOException -> 0x0082, SAXException -> 0x008e, ParserConfigurationException -> 0x008c, DOMException -> 0x008a, Exception -> 0x0088, all -> 0x0086 }
            r1.add(r5)     // Catch:{ IOException -> 0x0082, SAXException -> 0x008e, ParserConfigurationException -> 0x008c, DOMException -> 0x008a, Exception -> 0x0088, all -> 0x0086 }
            int r2 = r2 + 1
            goto L_0x0024
        L_0x007d:
            java.lang.String r6 = ""
            r5.c = r6     // Catch:{ IOException -> 0x0082, SAXException -> 0x008e, ParserConfigurationException -> 0x008c, DOMException -> 0x008a, Exception -> 0x0088, all -> 0x0086 }
            goto L_0x005f
        L_0x0082:
            r1 = move-exception
            goto L_0x000f
        L_0x0084:
            r0 = r1
            goto L_0x000f
        L_0x0086:
            r0 = move-exception
            throw r0
        L_0x0088:
            r1 = move-exception
            goto L_0x000f
        L_0x008a:
            r1 = move-exception
            goto L_0x000f
        L_0x008c:
            r1 = move-exception
            goto L_0x000f
        L_0x008e:
            r1 = move-exception
            goto L_0x000f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.b.a.f.a(java.io.InputStream):java.util.ArrayList");
    }

    /* access modifiers changed from: private */
    public boolean d() {
        ArrayList<d> a2;
        byte[] i = this.g == "duoduo_family.tmp" ? w.i() : w.j();
        if (i == null || (a2 = a(new ByteArrayInputStream(i))) == null) {
            return false;
        }
        a.a(f737a, a2.size() + " softwares in family.");
        this.c.a(a2);
        this.h.sendMessage(this.h.obtainMessage(0, a2));
        return true;
    }

    /* access modifiers changed from: private */
    public boolean e() {
        ArrayList<d> a2 = this.c.a();
        if (a2 == null) {
            return false;
        }
        a.a(f737a, a2.size() + " softwares in family. read from cache.");
        this.h.sendMessage(this.h.obtainMessage(2, a2));
        return true;
    }

    public void a() {
        if (this.b == null) {
            this.e = true;
            this.f = false;
            new h(this).start();
        }
    }

    public int b() {
        if (this.b == null) {
            return 0;
        }
        return this.b.size();
    }

    public d a(int i) {
        if (this.b == null || i < 0 || i >= this.b.size()) {
            return null;
        }
        return this.b.get(i);
    }
}
