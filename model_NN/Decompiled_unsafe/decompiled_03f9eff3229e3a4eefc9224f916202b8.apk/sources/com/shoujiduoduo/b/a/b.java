package com.shoujiduoduo.b.a;

import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.util.as;
import com.shoujiduoduo.util.i;
import com.shoujiduoduo.util.l;
import java.util.ArrayList;

/* compiled from: BannerAdData */
public class b {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static String f734a = (l.a(2) + "banner_ad.tmp");
    private ArrayList<a> b = new ArrayList<>();
    /* access modifiers changed from: private */
    public boolean c;
    private int d;

    public void a() {
        g();
    }

    public boolean b() {
        com.shoujiduoduo.base.a.a.a("BannerAdData", "isDataReady:" + this.c);
        return this.c;
    }

    public void c() {
        if (this.b != null) {
            this.b.clear();
        }
    }

    /* compiled from: BannerAdData */
    public class a {

        /* renamed from: a  reason: collision with root package name */
        public String f735a = "";
        public String b = "";
        public String c = "";
        public String d = "";
        public String e = "";
        public String f = "";
        public String g = "";

        public a() {
        }
    }

    public ArrayList<a> d() {
        return this.b;
    }

    public int e() {
        return this.d;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.util.as.a(android.content.Context, java.lang.String, long):long
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.shoujiduoduo.util.as.a(android.content.Context, java.lang.String, int):int
      com.shoujiduoduo.util.as.a(android.content.Context, java.lang.String, java.lang.String):java.lang.String
      com.shoujiduoduo.util.as.a(android.content.Context, java.lang.String, long):long */
    private void g() {
        long a2 = as.a(RingDDApp.c(), "update_banner_ad_time", 0L);
        if (a2 != 0) {
            com.shoujiduoduo.base.a.a.a("BannerAdData", "timeLastUpdate = " + a2);
            com.shoujiduoduo.base.a.a.a("BannerAdData", "current time = " + System.currentTimeMillis());
            i();
            return;
        }
        com.shoujiduoduo.base.a.a.a("BannerAdData", "no cache, read from net");
        i();
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00dd, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00de, code lost:
        r2.printStackTrace();
        r8.d = 100;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x00e7, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:?, code lost:
        com.shoujiduoduo.base.a.a.a("BannerAdData", "load cache exception");
        com.shoujiduoduo.base.a.a.a(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0119, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:?, code lost:
        com.shoujiduoduo.base.a.a.a("BannerAdData", "load cache exception");
        com.shoujiduoduo.base.a.a.a(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0126, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0127, code lost:
        com.shoujiduoduo.base.a.a.a("BannerAdData", "load cache exception");
        com.shoujiduoduo.base.a.a.a(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0133, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0134, code lost:
        com.shoujiduoduo.base.a.a.a("BannerAdData", "load cache exception");
        com.shoujiduoduo.base.a.a.a(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:?, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:?, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:?, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:?, code lost:
        return false;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00e7 A[ExcHandler: IOException (r1v5 'e' java.io.IOException A[CUSTOM_DECLARE]), Splitter:B:3:0x0017] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0119 A[ExcHandler: SAXException (r1v4 'e' org.xml.sax.SAXException A[CUSTOM_DECLARE]), Splitter:B:3:0x0017] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0126 A[Catch:{ IOException -> 0x00e7, SAXException -> 0x0119, ParserConfigurationException -> 0x0126, DOMException -> 0x0133, Exception -> 0x0140 }, ExcHandler: ParserConfigurationException (r1v3 'e' javax.xml.parsers.ParserConfigurationException A[CUSTOM_DECLARE, Catch:{  }]), Splitter:B:3:0x0017] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0133 A[Catch:{ IOException -> 0x00e7, SAXException -> 0x0119, ParserConfigurationException -> 0x0126, DOMException -> 0x0133, Exception -> 0x0140 }, ExcHandler: DOMException (r1v2 'e' org.w3c.dom.DOMException A[CUSTOM_DECLARE, Catch:{  }]), Splitter:B:3:0x0017] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean h() {
        /*
            r8 = this;
            r1 = 1
            r0 = 0
            java.io.File r2 = new java.io.File
            java.lang.String r3 = com.shoujiduoduo.b.a.b.f734a
            r2.<init>(r3)
            boolean r2 = r2.exists()
            if (r2 != 0) goto L_0x0017
            java.lang.String r1 = "BannerAdData"
            java.lang.String r2 = "banner_ad.tmp not exist, 不显示广告"
            com.shoujiduoduo.base.a.a.a(r1, r2)
        L_0x0016:
            return r0
        L_0x0017:
            javax.xml.parsers.DocumentBuilderFactory r2 = javax.xml.parsers.DocumentBuilderFactory.newInstance()     // Catch:{ IOException -> 0x00e7, SAXException -> 0x0119, ParserConfigurationException -> 0x0126, DOMException -> 0x0133, Exception -> 0x0140 }
            javax.xml.parsers.DocumentBuilder r2 = r2.newDocumentBuilder()     // Catch:{ IOException -> 0x00e7, SAXException -> 0x0119, ParserConfigurationException -> 0x0126, DOMException -> 0x0133, Exception -> 0x0140 }
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ IOException -> 0x00e7, SAXException -> 0x0119, ParserConfigurationException -> 0x0126, DOMException -> 0x0133, Exception -> 0x0140 }
            java.lang.String r4 = com.shoujiduoduo.b.a.b.f734a     // Catch:{ IOException -> 0x00e7, SAXException -> 0x0119, ParserConfigurationException -> 0x0126, DOMException -> 0x0133, Exception -> 0x0140 }
            r3.<init>(r4)     // Catch:{ IOException -> 0x00e7, SAXException -> 0x0119, ParserConfigurationException -> 0x0126, DOMException -> 0x0133, Exception -> 0x0140 }
            org.w3c.dom.Document r2 = r2.parse(r3)     // Catch:{ IOException -> 0x00e7, SAXException -> 0x0119, ParserConfigurationException -> 0x0126, DOMException -> 0x0133, Exception -> 0x0140 }
            org.w3c.dom.Element r3 = r2.getDocumentElement()     // Catch:{ IOException -> 0x00e7, SAXException -> 0x0119, ParserConfigurationException -> 0x0126, DOMException -> 0x0133, Exception -> 0x0140 }
            if (r3 == 0) goto L_0x0016
            java.lang.String r2 = "height"
            java.lang.String r4 = r3.getAttribute(r2)     // Catch:{ IOException -> 0x00e7, SAXException -> 0x0119, ParserConfigurationException -> 0x0126, DOMException -> 0x0133, Exception -> 0x0140 }
            int r2 = java.lang.Integer.parseInt(r4)     // Catch:{ Exception -> 0x00dd, IOException -> 0x00e7, SAXException -> 0x0119, ParserConfigurationException -> 0x0126, DOMException -> 0x0133 }
            r8.d = r2     // Catch:{ Exception -> 0x00dd, IOException -> 0x00e7, SAXException -> 0x0119, ParserConfigurationException -> 0x0126, DOMException -> 0x0133 }
        L_0x003c:
            java.lang.String r2 = "BannerAdData"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00e7, SAXException -> 0x0119, ParserConfigurationException -> 0x0126, DOMException -> 0x0133, Exception -> 0x0140 }
            r5.<init>()     // Catch:{ IOException -> 0x00e7, SAXException -> 0x0119, ParserConfigurationException -> 0x0126, DOMException -> 0x0133, Exception -> 0x0140 }
            java.lang.String r6 = "ad height:"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ IOException -> 0x00e7, SAXException -> 0x0119, ParserConfigurationException -> 0x0126, DOMException -> 0x0133, Exception -> 0x0140 }
            java.lang.StringBuilder r4 = r5.append(r4)     // Catch:{ IOException -> 0x00e7, SAXException -> 0x0119, ParserConfigurationException -> 0x0126, DOMException -> 0x0133, Exception -> 0x0140 }
            java.lang.String r4 = r4.toString()     // Catch:{ IOException -> 0x00e7, SAXException -> 0x0119, ParserConfigurationException -> 0x0126, DOMException -> 0x0133, Exception -> 0x0140 }
            com.shoujiduoduo.base.a.a.a(r2, r4)     // Catch:{ IOException -> 0x00e7, SAXException -> 0x0119, ParserConfigurationException -> 0x0126, DOMException -> 0x0133, Exception -> 0x0140 }
            java.lang.String r2 = "banner"
            org.w3c.dom.NodeList r3 = r3.getElementsByTagName(r2)     // Catch:{ IOException -> 0x00e7, SAXException -> 0x0119, ParserConfigurationException -> 0x0126, DOMException -> 0x0133, Exception -> 0x0140 }
            if (r3 == 0) goto L_0x0016
            java.util.ArrayList<com.shoujiduoduo.b.a.b$a> r2 = r8.b     // Catch:{ IOException -> 0x00e7, SAXException -> 0x0119, ParserConfigurationException -> 0x0126, DOMException -> 0x0133, Exception -> 0x0140 }
            r2.clear()     // Catch:{ IOException -> 0x00e7, SAXException -> 0x0119, ParserConfigurationException -> 0x0126, DOMException -> 0x0133, Exception -> 0x0140 }
            r2 = r0
        L_0x0062:
            int r4 = r3.getLength()     // Catch:{ IOException -> 0x00e7, SAXException -> 0x0119, ParserConfigurationException -> 0x0126, DOMException -> 0x0133, Exception -> 0x0140 }
            if (r2 >= r4) goto L_0x00f6
            com.shoujiduoduo.b.a.b$a r4 = new com.shoujiduoduo.b.a.b$a     // Catch:{ IOException -> 0x00e7, SAXException -> 0x0119, ParserConfigurationException -> 0x0126, DOMException -> 0x0133, Exception -> 0x0140 }
            r4.<init>()     // Catch:{ IOException -> 0x00e7, SAXException -> 0x0119, ParserConfigurationException -> 0x0126, DOMException -> 0x0133, Exception -> 0x0140 }
            org.w3c.dom.Node r5 = r3.item(r2)     // Catch:{ IOException -> 0x00e7, SAXException -> 0x0119, ParserConfigurationException -> 0x0126, DOMException -> 0x0133, Exception -> 0x0140 }
            org.w3c.dom.NamedNodeMap r5 = r5.getAttributes()     // Catch:{ IOException -> 0x00e7, SAXException -> 0x0119, ParserConfigurationException -> 0x0126, DOMException -> 0x0133, Exception -> 0x0140 }
            java.lang.String r6 = "method"
            java.lang.String r6 = com.shoujiduoduo.util.f.a(r5, r6)     // Catch:{ IOException -> 0x00e7, SAXException -> 0x0119, ParserConfigurationException -> 0x0126, DOMException -> 0x0133, Exception -> 0x0140 }
            r4.f735a = r6     // Catch:{ IOException -> 0x00e7, SAXException -> 0x0119, ParserConfigurationException -> 0x0126, DOMException -> 0x0133, Exception -> 0x0140 }
            java.lang.String r6 = r4.f735a     // Catch:{ IOException -> 0x00e7, SAXException -> 0x0119, ParserConfigurationException -> 0x0126, DOMException -> 0x0133, Exception -> 0x0140 }
            java.lang.String r7 = "down"
            boolean r6 = r6.equals(r7)     // Catch:{ IOException -> 0x00e7, SAXException -> 0x0119, ParserConfigurationException -> 0x0126, DOMException -> 0x0133, Exception -> 0x0140 }
            if (r6 != 0) goto L_0x00a5
            java.lang.String r6 = r4.f735a     // Catch:{ IOException -> 0x00e7, SAXException -> 0x0119, ParserConfigurationException -> 0x0126, DOMException -> 0x0133, Exception -> 0x0140 }
            java.lang.String r7 = "web"
            boolean r6 = r6.equals(r7)     // Catch:{ IOException -> 0x00e7, SAXException -> 0x0119, ParserConfigurationException -> 0x0126, DOMException -> 0x0133, Exception -> 0x0140 }
            if (r6 != 0) goto L_0x00a5
            java.lang.String r6 = r4.f735a     // Catch:{ IOException -> 0x00e7, SAXException -> 0x0119, ParserConfigurationException -> 0x0126, DOMException -> 0x0133, Exception -> 0x0140 }
            java.lang.String r7 = "search"
            boolean r6 = r6.equals(r7)     // Catch:{ IOException -> 0x00e7, SAXException -> 0x0119, ParserConfigurationException -> 0x0126, DOMException -> 0x0133, Exception -> 0x0140 }
            if (r6 != 0) goto L_0x00a5
            java.lang.String r6 = r4.f735a     // Catch:{ IOException -> 0x00e7, SAXException -> 0x0119, ParserConfigurationException -> 0x0126, DOMException -> 0x0133, Exception -> 0x0140 }
            java.lang.String r7 = "list"
            boolean r6 = r6.equals(r7)     // Catch:{ IOException -> 0x00e7, SAXException -> 0x0119, ParserConfigurationException -> 0x0126, DOMException -> 0x0133, Exception -> 0x0140 }
            if (r6 == 0) goto L_0x00da
        L_0x00a5:
            java.lang.String r6 = "title"
            java.lang.String r6 = com.shoujiduoduo.util.f.a(r5, r6)     // Catch:{ IOException -> 0x00e7, SAXException -> 0x0119, ParserConfigurationException -> 0x0126, DOMException -> 0x0133, Exception -> 0x0140 }
            r4.b = r6     // Catch:{ IOException -> 0x00e7, SAXException -> 0x0119, ParserConfigurationException -> 0x0126, DOMException -> 0x0133, Exception -> 0x0140 }
            java.lang.String r6 = "keyword"
            java.lang.String r6 = com.shoujiduoduo.util.f.a(r5, r6)     // Catch:{ IOException -> 0x00e7, SAXException -> 0x0119, ParserConfigurationException -> 0x0126, DOMException -> 0x0133, Exception -> 0x0140 }
            r4.d = r6     // Catch:{ IOException -> 0x00e7, SAXException -> 0x0119, ParserConfigurationException -> 0x0126, DOMException -> 0x0133, Exception -> 0x0140 }
            java.lang.String r6 = "pic"
            java.lang.String r6 = com.shoujiduoduo.util.f.a(r5, r6)     // Catch:{ IOException -> 0x00e7, SAXException -> 0x0119, ParserConfigurationException -> 0x0126, DOMException -> 0x0133, Exception -> 0x0140 }
            r4.e = r6     // Catch:{ IOException -> 0x00e7, SAXException -> 0x0119, ParserConfigurationException -> 0x0126, DOMException -> 0x0133, Exception -> 0x0140 }
            java.lang.String r6 = "url"
            java.lang.String r6 = com.shoujiduoduo.util.f.a(r5, r6)     // Catch:{ IOException -> 0x00e7, SAXException -> 0x0119, ParserConfigurationException -> 0x0126, DOMException -> 0x0133, Exception -> 0x0140 }
            r4.f = r6     // Catch:{ IOException -> 0x00e7, SAXException -> 0x0119, ParserConfigurationException -> 0x0126, DOMException -> 0x0133, Exception -> 0x0140 }
            java.lang.String r6 = "tips"
            java.lang.String r6 = com.shoujiduoduo.util.f.a(r5, r6)     // Catch:{ IOException -> 0x00e7, SAXException -> 0x0119, ParserConfigurationException -> 0x0126, DOMException -> 0x0133, Exception -> 0x0140 }
            r4.c = r6     // Catch:{ IOException -> 0x00e7, SAXException -> 0x0119, ParserConfigurationException -> 0x0126, DOMException -> 0x0133, Exception -> 0x0140 }
            java.lang.String r6 = "listid"
            java.lang.String r5 = com.shoujiduoduo.util.f.a(r5, r6)     // Catch:{ IOException -> 0x00e7, SAXException -> 0x0119, ParserConfigurationException -> 0x0126, DOMException -> 0x0133, Exception -> 0x0140 }
            r4.g = r5     // Catch:{ IOException -> 0x00e7, SAXException -> 0x0119, ParserConfigurationException -> 0x0126, DOMException -> 0x0133, Exception -> 0x0140 }
            java.util.ArrayList<com.shoujiduoduo.b.a.b$a> r5 = r8.b     // Catch:{ IOException -> 0x00e7, SAXException -> 0x0119, ParserConfigurationException -> 0x0126, DOMException -> 0x0133, Exception -> 0x0140 }
            r5.add(r4)     // Catch:{ IOException -> 0x00e7, SAXException -> 0x0119, ParserConfigurationException -> 0x0126, DOMException -> 0x0133, Exception -> 0x0140 }
        L_0x00da:
            int r2 = r2 + 1
            goto L_0x0062
        L_0x00dd:
            r2 = move-exception
            r2.printStackTrace()     // Catch:{ IOException -> 0x00e7, SAXException -> 0x0119, ParserConfigurationException -> 0x0126, DOMException -> 0x0133, Exception -> 0x0140 }
            r2 = 100
            r8.d = r2     // Catch:{ IOException -> 0x00e7, SAXException -> 0x0119, ParserConfigurationException -> 0x0126, DOMException -> 0x0133, Exception -> 0x0140 }
            goto L_0x003c
        L_0x00e7:
            r1 = move-exception
            java.lang.String r2 = "BannerAdData"
            java.lang.String r3 = "load cache exception"
            com.shoujiduoduo.base.a.a.a(r2, r3)     // Catch:{ all -> 0x00f4 }
            com.shoujiduoduo.base.a.a.a(r1)     // Catch:{ all -> 0x00f4 }
            goto L_0x0016
        L_0x00f4:
            r0 = move-exception
            throw r0
        L_0x00f6:
            java.lang.String r2 = "BannerAdData"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00e7, SAXException -> 0x0119, ParserConfigurationException -> 0x0126, DOMException -> 0x0133, Exception -> 0x0140 }
            r3.<init>()     // Catch:{ IOException -> 0x00e7, SAXException -> 0x0119, ParserConfigurationException -> 0x0126, DOMException -> 0x0133, Exception -> 0x0140 }
            java.lang.String r4 = "read success, list size:"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ IOException -> 0x00e7, SAXException -> 0x0119, ParserConfigurationException -> 0x0126, DOMException -> 0x0133, Exception -> 0x0140 }
            java.util.ArrayList<com.shoujiduoduo.b.a.b$a> r4 = r8.b     // Catch:{ IOException -> 0x00e7, SAXException -> 0x0119, ParserConfigurationException -> 0x0126, DOMException -> 0x0133, Exception -> 0x0140 }
            int r4 = r4.size()     // Catch:{ IOException -> 0x00e7, SAXException -> 0x0119, ParserConfigurationException -> 0x0126, DOMException -> 0x0133, Exception -> 0x0140 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ IOException -> 0x00e7, SAXException -> 0x0119, ParserConfigurationException -> 0x0126, DOMException -> 0x0133, Exception -> 0x0140 }
            java.lang.String r3 = r3.toString()     // Catch:{ IOException -> 0x00e7, SAXException -> 0x0119, ParserConfigurationException -> 0x0126, DOMException -> 0x0133, Exception -> 0x0140 }
            com.shoujiduoduo.base.a.a.a(r2, r3)     // Catch:{ IOException -> 0x00e7, SAXException -> 0x0119, ParserConfigurationException -> 0x0126, DOMException -> 0x0133, Exception -> 0x0140 }
            r8.c = r1
            r0 = r1
            goto L_0x0016
        L_0x0119:
            r1 = move-exception
            java.lang.String r2 = "BannerAdData"
            java.lang.String r3 = "load cache exception"
            com.shoujiduoduo.base.a.a.a(r2, r3)     // Catch:{ all -> 0x00f4 }
            com.shoujiduoduo.base.a.a.a(r1)     // Catch:{ all -> 0x00f4 }
            goto L_0x0016
        L_0x0126:
            r1 = move-exception
            java.lang.String r2 = "BannerAdData"
            java.lang.String r3 = "load cache exception"
            com.shoujiduoduo.base.a.a.a(r2, r3)     // Catch:{ all -> 0x00f4 }
            com.shoujiduoduo.base.a.a.a(r1)     // Catch:{ all -> 0x00f4 }
            goto L_0x0016
        L_0x0133:
            r1 = move-exception
            java.lang.String r2 = "BannerAdData"
            java.lang.String r3 = "load cache exception"
            com.shoujiduoduo.base.a.a.a(r2, r3)     // Catch:{ all -> 0x00f4 }
            com.shoujiduoduo.base.a.a.a(r1)     // Catch:{ all -> 0x00f4 }
            goto L_0x0016
        L_0x0140:
            r1 = move-exception
            java.lang.String r2 = "BannerAdData"
            java.lang.String r3 = "load cache exception"
            com.shoujiduoduo.base.a.a.a(r2, r3)     // Catch:{ all -> 0x00f4 }
            com.shoujiduoduo.base.a.a.a(r1)     // Catch:{ all -> 0x00f4 }
            goto L_0x0016
        */
        throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.b.a.b.h():boolean");
    }

    private void i() {
        i.a(new c(this));
    }
}
