package com.shoujiduoduo.b.a;

import android.text.TextUtils;
import com.shoujiduoduo.a.a.b;
import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.util.as;
import com.shoujiduoduo.util.t;
import com.shoujiduoduo.util.w;

/* compiled from: SearchAdData */
class u implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ t f749a;

    u(t tVar) {
        this.f749a = tVar;
    }

    public void run() {
        byte[] e = w.e();
        if (e != null) {
            String str = new String(e);
            if (!TextUtils.isEmpty(str.trim())) {
                t.b(t.f747a, str.trim());
                a.a("SearchAdData", "loadFromNetwork, write to local cache file");
                a.a("SearchAdData", "content:" + new String(e));
                as.b(RingDDApp.c(), "update_search_ad_time", System.currentTimeMillis());
            }
        }
        if (this.f749a.f()) {
            x.a().a(b.OBSERVER_SEARCH_AD, new v(this));
            boolean unused = this.f749a.c = true;
            return;
        }
        boolean unused2 = this.f749a.c = false;
    }
}
