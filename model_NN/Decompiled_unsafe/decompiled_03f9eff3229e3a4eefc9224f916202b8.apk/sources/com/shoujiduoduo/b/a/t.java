package com.shoujiduoduo.b.a;

import cn.banshenggua.aichang.utils.Constants;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.util.as;
import com.shoujiduoduo.util.f;
import com.shoujiduoduo.util.i;
import com.shoujiduoduo.util.l;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.DOMException;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/* compiled from: SearchAdData */
public class t {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static String f747a = (l.a(2) + "search_ad.tmp");
    private ArrayList<a> b = new ArrayList<>();
    /* access modifiers changed from: private */
    public boolean c;

    public void a() {
        e();
    }

    public void b() {
        if (this.b != null) {
            this.b.clear();
        }
    }

    public boolean c() {
        com.shoujiduoduo.base.a.a.a("SearchAdData", "isDataReady:" + this.c);
        return this.c;
    }

    public a a(String str) {
        if (!this.c) {
            return null;
        }
        try {
            Iterator<a> it = this.b.iterator();
            while (it.hasNext()) {
                a next = it.next();
                if (com.shoujiduoduo.util.a.b() && next.b.equals("cn.banshenggua.aichang")) {
                    return null;
                }
                String[] split = next.e.split(";");
                int length = split.length;
                int i = 0;
                while (true) {
                    if (i < length) {
                        String str2 = split[i];
                        if (str2.equalsIgnoreCase("all")) {
                            if (f.a(next.b)) {
                                return null;
                            }
                            return next;
                        } else if (str.equalsIgnoreCase(str2) && !f.a(next.b)) {
                            return next;
                        } else {
                            i++;
                        }
                    }
                }
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
        return null;
    }

    /* compiled from: SearchAdData */
    public class a {

        /* renamed from: a  reason: collision with root package name */
        public String f748a = "";
        public String b = "";
        public String c = "";
        public String d = "";
        public String e = "";
        public String f = "";

        public a() {
        }

        public String toString() {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("app:" + this.f748a);
            stringBuffer.append(", packagename:" + this.b);
            stringBuffer.append(", des:" + this.c);
            stringBuffer.append(", icon:" + this.d);
            stringBuffer.append(", key:" + this.e);
            stringBuffer.append(", download:" + this.f);
            return stringBuffer.toString();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.util.as.a(android.content.Context, java.lang.String, long):long
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.shoujiduoduo.util.as.a(android.content.Context, java.lang.String, int):int
      com.shoujiduoduo.util.as.a(android.content.Context, java.lang.String, java.lang.String):java.lang.String
      com.shoujiduoduo.util.as.a(android.content.Context, java.lang.String, long):long */
    private void e() {
        long a2 = as.a(RingDDApp.c(), "update_search_ad_time", 0L);
        if (a2 != 0) {
            com.shoujiduoduo.base.a.a.a("SearchAdData", "timeLastUpdate = " + a2);
            com.shoujiduoduo.base.a.a.a("SearchAdData", "current time = " + System.currentTimeMillis());
            if (System.currentTimeMillis() - a2 > LogBuilder.MAX_INTERVAL) {
                com.shoujiduoduo.base.a.a.a("SearchAdData", "cache out of data, download new data");
                g();
            } else if (!f()) {
                com.shoujiduoduo.base.a.a.a("SearchAdData", "cache is available, but read failed, download new data");
                g();
            }
        } else {
            com.shoujiduoduo.base.a.a.a("SearchAdData", "no cache, read from net");
            g();
        }
    }

    /* access modifiers changed from: private */
    public boolean f() {
        NodeList elementsByTagName;
        if (!new File(f747a).exists()) {
            com.shoujiduoduo.base.a.a.a("SearchAdData", "banner_ad.tmp not exist, 不显示广告");
            return false;
        }
        try {
            Element documentElement = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new FileInputStream(f747a)).getDocumentElement();
            if (documentElement == null || (elementsByTagName = documentElement.getElementsByTagName(Constants.ITEM)) == null) {
                return false;
            }
            this.b.clear();
            for (int i = 0; i < elementsByTagName.getLength(); i++) {
                a aVar = new a();
                NamedNodeMap attributes = elementsByTagName.item(i).getAttributes();
                aVar.f748a = f.a(attributes, "app");
                aVar.b = f.a(attributes, "packagename");
                aVar.c = f.a(attributes, "des");
                aVar.d = f.a(attributes, "icon");
                aVar.e = f.a(attributes, "key");
                aVar.f = f.a(attributes, "download");
                this.b.add(aVar);
            }
            com.shoujiduoduo.base.a.a.a("SearchAdData", "read success, list size:" + this.b.size());
            this.c = true;
            return true;
        } catch (IOException e) {
            com.shoujiduoduo.base.a.a.a("SearchAdData", "load cache exception");
            com.shoujiduoduo.base.a.a.a(e);
            return false;
        } catch (SAXException e2) {
            com.shoujiduoduo.base.a.a.a("SearchAdData", "load cache exception");
            com.shoujiduoduo.base.a.a.a(e2);
            return false;
        } catch (ParserConfigurationException e3) {
            com.shoujiduoduo.base.a.a.a("SearchAdData", "load cache exception");
            com.shoujiduoduo.base.a.a.a(e3);
            return false;
        } catch (DOMException e4) {
            com.shoujiduoduo.base.a.a.a("SearchAdData", "load cache exception");
            com.shoujiduoduo.base.a.a.a(e4);
            return false;
        } catch (Exception e5) {
            com.shoujiduoduo.base.a.a.a("SearchAdData", "load cache exception");
            com.shoujiduoduo.base.a.a.a(e5);
            return false;
        }
    }

    private void g() {
        i.a(new u(this));
    }
}
