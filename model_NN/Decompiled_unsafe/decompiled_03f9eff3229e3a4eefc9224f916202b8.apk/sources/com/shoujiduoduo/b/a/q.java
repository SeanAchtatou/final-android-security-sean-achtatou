package com.shoujiduoduo.b.a;

import com.qhad.ads.sdk.interfaces.IQhNativeAd;
import com.qhad.ads.sdk.interfaces.IQhNativeAdListener;
import com.qhad.ads.sdk.interfaces.IQhNativeAdLoader;
import com.shoujiduoduo.a.a.b;
import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.b.a.i;
import com.shoujiduoduo.base.a.a;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: FeedAdData */
class q implements IQhNativeAdListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ i f746a;

    q(i iVar) {
        this.f746a = iVar;
    }

    public void onNativeAdLoadSucceeded(ArrayList<IQhNativeAd> arrayList) {
        if (arrayList == null || arrayList.size() <= 0) {
            boolean unused = this.f746a.p = false;
            a.a("feedAdData", "onNativeAdLoadSucceeded, size is 0");
            return;
        }
        a.a("feedAdData", "onNativeAdLoadSucceeded, size:" + arrayList.size());
        long currentTimeMillis = System.currentTimeMillis();
        synchronized (this.f746a.f) {
            Iterator<IQhNativeAd> it = arrayList.iterator();
            while (it.hasNext()) {
                i.a aVar = new i.a(it.next(), currentTimeMillis, (long) this.f746a.d, 2);
                aVar.a(this.f746a.j());
                this.f746a.e.add(aVar);
            }
        }
        if (!this.f746a.m) {
            x.a().b(b.OBSERVER_FEED_AD, new r(this));
            boolean unused2 = this.f746a.m = true;
        }
        a.a("feedAdData", "ad list size:" + this.f746a.e.size());
        if (this.f746a.e.size() < this.f746a.f740a) {
            a.a("feedAdData", "ad list size is < " + this.f746a.f740a + ", so fetch more data");
            boolean unused3 = this.f746a.p = true;
            ((IQhNativeAdLoader) this.f746a.h).loadAds();
            return;
        }
        boolean unused4 = this.f746a.p = false;
    }

    public void onNativeAdLoadFailed() {
        a.a("feedAdData", "onNativeAdLoadFailed");
    }
}
