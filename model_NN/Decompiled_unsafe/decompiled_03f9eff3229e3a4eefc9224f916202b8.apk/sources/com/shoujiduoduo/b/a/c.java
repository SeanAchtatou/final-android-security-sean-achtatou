package com.shoujiduoduo.b.a;

import android.text.TextUtils;
import com.shoujiduoduo.a.a.b;
import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.util.as;
import com.shoujiduoduo.util.t;
import com.shoujiduoduo.util.w;

/* compiled from: BannerAdData */
class c implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ b f736a;

    c(b bVar) {
        this.f736a = bVar;
    }

    public void run() {
        byte[] d = w.d();
        if (d != null) {
            String str = new String(d);
            if (!TextUtils.isEmpty(str.trim())) {
                t.b(b.f734a, str.trim());
                a.a("BannerAdData", "loadFromNetwork, write to local cache file");
                a.a("BannerAdData", "content:" + new String(d));
                as.b(RingDDApp.c(), "update_banner_ad_time", System.currentTimeMillis());
            }
        }
        if (this.f736a.h()) {
            x.a().a(b.OBSERVER_BANNER_AD, new d(this));
            boolean unused = this.f736a.c = true;
            return;
        }
        boolean unused2 = this.f736a.c = false;
    }
}
