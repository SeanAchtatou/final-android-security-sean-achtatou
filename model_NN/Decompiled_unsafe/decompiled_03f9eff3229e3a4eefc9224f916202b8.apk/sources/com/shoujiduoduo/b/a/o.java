package com.shoujiduoduo.b.a;

import com.duoduo.mobads.IBaiduNative;
import com.duoduo.mobads.IBaiduNativeNetworkListener;
import com.duoduo.mobads.INativeErrorCode;
import com.duoduo.mobads.INativeResponse;
import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.b.a.i;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.umeng.analytics.b;
import java.util.HashMap;
import java.util.List;

/* compiled from: FeedAdData */
class o implements IBaiduNativeNetworkListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ i f745a;

    o(i iVar) {
        this.f745a = iVar;
    }

    public void onNativeLoad(List<INativeResponse> list) {
        a.a("feedAdData", "Duomob feed ad load success");
        if (list == null || list.size() <= 0) {
            boolean unused = this.f745a.p = false;
            a.a("feedAdData", "onNativeAdLoadSucceeded, size is 0");
            return;
        }
        a.a("feedAdData", "onNativeAdLoadSucceeded, size:" + list.size());
        HashMap hashMap = new HashMap();
        hashMap.put("status", "onNativeLoad");
        b.a(RingDDApp.c(), "duomob_list_feed_ad", hashMap);
        long currentTimeMillis = System.currentTimeMillis();
        synchronized (this.f745a.f) {
            for (INativeResponse aVar : list) {
                i.a aVar2 = new i.a(aVar, currentTimeMillis, (long) this.f745a.d, 3);
                aVar2.a(this.f745a.j());
                this.f745a.e.add(aVar2);
            }
        }
        if (!this.f745a.m) {
            x.a().b(com.shoujiduoduo.a.a.b.OBSERVER_FEED_AD, new p(this));
            boolean unused2 = this.f745a.m = true;
        }
        a.a("feedAdData", "ad list size:" + this.f745a.e.size());
        if (this.f745a.e.size() < this.f745a.f740a) {
            a.a("feedAdData", "ad list size is < " + this.f745a.f740a + ", so fetch more data");
            boolean unused3 = this.f745a.p = true;
            ((IBaiduNative) this.f745a.i).makeRequest();
            return;
        }
        boolean unused4 = this.f745a.p = false;
    }

    public void onNativeFail(INativeErrorCode iNativeErrorCode) {
        if (iNativeErrorCode != null) {
            HashMap hashMap = new HashMap();
            hashMap.put("status", "onNativeFail");
            hashMap.put("errCode", "" + iNativeErrorCode.getErrorCode());
            b.a(RingDDApp.c(), "duomob_list_feed_ad", hashMap);
            a.a("feedAdData", "Duomob feed ad load failed, errcode:" + iNativeErrorCode.getErrorCode());
        }
    }
}
