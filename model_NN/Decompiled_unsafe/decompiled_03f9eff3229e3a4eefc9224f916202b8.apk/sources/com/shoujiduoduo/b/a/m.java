package com.shoujiduoduo.b.a;

import com.qq.e.ads.nativ.NativeAD;
import com.qq.e.ads.nativ.NativeADDataRef;
import com.shoujiduoduo.a.a.x;
import com.shoujiduoduo.b.a.i;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.umeng.analytics.b;
import java.util.HashMap;
import java.util.List;

/* compiled from: FeedAdData */
class m implements NativeAD.NativeAdListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ i f744a;

    m(i iVar) {
        this.f744a = iVar;
    }

    public void onADLoaded(List<NativeADDataRef> list) {
        a.a("feedAdData", "gdt feed ad load success");
        if (list == null || list.size() <= 0) {
            boolean unused = this.f744a.p = false;
            a.a("feedAdData", "onADLoaded, size is 0");
            return;
        }
        a.a("feedAdData", "onADLoaded, size:" + list.size());
        HashMap hashMap = new HashMap();
        hashMap.put("status", "onNativeLoad");
        b.a(RingDDApp.c(), "gdt_feed_ad", hashMap);
        long currentTimeMillis = System.currentTimeMillis();
        synchronized (this.f744a.f) {
            for (NativeADDataRef aVar : list) {
                i.a aVar2 = new i.a(aVar, currentTimeMillis, (long) this.f744a.d, 4);
                aVar2.a(this.f744a.j());
                this.f744a.e.add(aVar2);
            }
        }
        if (!this.f744a.m) {
            x.a().b(com.shoujiduoduo.a.a.b.OBSERVER_FEED_AD, new n(this));
            boolean unused2 = this.f744a.m = true;
        }
        a.a("feedAdData", "ad list size:" + this.f744a.e.size());
        if (this.f744a.e.size() < this.f744a.f740a) {
            a.a("feedAdData", "ad list size is < " + this.f744a.f740a + ", so fetch more data");
            boolean unused3 = this.f744a.p = true;
            ((NativeAD) this.f744a.g).loadAD(10);
            return;
        }
        boolean unused4 = this.f744a.p = false;
    }

    public void onNoAD(int i) {
        a.a("feedAdData", "onNoAd");
        HashMap hashMap = new HashMap();
        hashMap.put("status", "onNoAD");
        b.a(RingDDApp.c(), "gdt_feed_ad", hashMap);
    }

    public void onADStatusChanged(NativeADDataRef nativeADDataRef) {
        a.a("feedAdData", "onADStatusChanged");
    }

    public void onADError(NativeADDataRef nativeADDataRef, int i) {
        a.a("feedAdData", "onADError:" + i);
        HashMap hashMap = new HashMap();
        hashMap.put("status", "onADError");
        hashMap.put("errCode", "" + i);
        b.a(RingDDApp.c(), "gdt_feed_ad", hashMap);
    }
}
