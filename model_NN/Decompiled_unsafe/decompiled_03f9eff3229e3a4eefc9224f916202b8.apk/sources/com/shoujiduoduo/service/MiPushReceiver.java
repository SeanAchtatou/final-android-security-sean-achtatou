package com.shoujiduoduo.service;

import android.content.Context;
import android.text.TextUtils;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.util.al;
import com.xiaomi.mipush.sdk.PushMessageReceiver;
import com.xiaomi.mipush.sdk.c;
import com.xiaomi.mipush.sdk.d;
import com.xiaomi.mipush.sdk.e;
import java.util.List;

public class MiPushReceiver extends PushMessageReceiver {

    /* renamed from: a  reason: collision with root package name */
    private String f880a;
    private long b = -1;
    private String c;
    private String d;
    private String e;
    private String f;
    private String g;

    public void a(Context context, e eVar) {
        this.c = eVar.c();
        a.a("MiPushReceiver", "content:" + this.c);
        a.a("MiPushReceiver", "title:" + eVar.g());
        if (!TextUtils.isEmpty(eVar.e())) {
            this.d = eVar.e();
        } else if (!TextUtils.isEmpty(eVar.d())) {
            this.e = eVar.d();
        }
        if (!TextUtils.isEmpty(this.c)) {
            try {
                new al(context).a(this.c);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    public void b(Context context, e eVar) {
        super.b(context, eVar);
    }

    public void a(Context context, d dVar) {
        String str;
        String a2 = dVar.a();
        List<String> b2 = dVar.b();
        String str2 = (b2 == null || b2.size() <= 0) ? null : b2.get(0);
        if (b2 == null || b2.size() <= 1) {
            str = null;
        } else {
            str = b2.get(1);
        }
        if ("register".equals(a2)) {
            if (dVar.c() == 0) {
                a.a("MiPushReceiver", "onCommandResult:register, success, regid:" + str2);
                c.b(context, "music_album_new", null);
                this.f880a = str2;
            }
        } else if ("set-alias".equals(a2)) {
            if (dVar.c() == 0) {
                this.e = str2;
            }
        } else if ("unset-alias".equals(a2)) {
            if (dVar.c() == 0) {
                this.e = str2;
            }
        } else if ("subscribe-topic".equals(a2)) {
            if (dVar.c() == 0) {
                a.a("MiPushReceiver", "onCommandResult:subscribe topic, success, topic:" + str2);
                this.d = str2;
            }
        } else if ("unsubscibe-topic".equals(a2)) {
            if (dVar.c() == 0) {
                this.d = str2;
            }
        } else if ("accept-time".equals(a2) && dVar.c() == 0) {
            this.f = str2;
            this.g = str;
        }
    }
}
