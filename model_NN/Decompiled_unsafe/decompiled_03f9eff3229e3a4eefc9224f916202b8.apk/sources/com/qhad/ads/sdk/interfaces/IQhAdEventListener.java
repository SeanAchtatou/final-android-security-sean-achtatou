package com.qhad.ads.sdk.interfaces;

public interface IQhAdEventListener {
    void onAdviewClicked();

    void onAdviewClosed();

    void onAdviewDestroyed();

    void onAdviewDismissedLandpage();

    void onAdviewGotAdFail();

    void onAdviewGotAdSucceed();

    void onAdviewIntoLandpage();

    void onAdviewRendered();
}
