package com.qhad.ads.sdk.adcore;

import com.qhad.ads.sdk.interfaces.DynamicObject;
import com.qhad.ads.sdk.interfaces.IQhAdEventListener;
import com.qhad.ads.sdk.log.QHADLog;

class QhAdEventListenerProxy implements DynamicObject {
    private final IQhAdEventListener adEventListener;

    public QhAdEventListenerProxy(IQhAdEventListener iQhAdEventListener) {
        this.adEventListener = iQhAdEventListener;
    }

    public Object invoke(int i, Object obj) {
        switch (i) {
            case 4:
                QHADLog.d("ADSUPDATE", "QHADEVENTLISTENER_onAdviewGotAdSucceed");
                this.adEventListener.onAdviewGotAdSucceed();
                return null;
            case 5:
                QHADLog.d("ADSUPDATE", "QHADEVENTLISTENER_onAdviewGotAdFail");
                this.adEventListener.onAdviewGotAdFail();
                return null;
            case 6:
                QHADLog.d("ADSUPDATE", "QHADEVENTLISTENER_onAdviewIntoLandpage");
                this.adEventListener.onAdviewIntoLandpage();
                return null;
            case 7:
                QHADLog.d("ADSUPDATE", "QHADEVENTLISTENER_onAdviewDismissedLandpage");
                this.adEventListener.onAdviewDismissedLandpage();
                return null;
            case 8:
                QHADLog.d("ADSUPDATE", "QHADEVENTLISTENER_onAdviewClicked");
                this.adEventListener.onAdviewClicked();
                return null;
            case 9:
                QHADLog.d("ADSUPDATE", "QHADEVENTLISTENER_onAdviewClosed");
                this.adEventListener.onAdviewClosed();
                return null;
            case 10:
                QHADLog.d("ADSUPDATE", "QHADEVENTLISTENER_onAdviewDestroyed");
                this.adEventListener.onAdviewDestroyed();
                return null;
            case 64:
                QHADLog.d("ADSUPDATE", "QHADEVENTLISTENER_onAdviewRendered");
                this.adEventListener.onAdviewRendered();
                return null;
            default:
                return null;
        }
    }
}
