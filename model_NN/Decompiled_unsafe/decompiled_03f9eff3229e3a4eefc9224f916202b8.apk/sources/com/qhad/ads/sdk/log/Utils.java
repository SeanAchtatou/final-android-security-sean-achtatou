package com.qhad.ads.sdk.log;

import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Base64;
import android.util.DisplayMetrics;
import com.baidu.mobads.interfaces.utils.IXAdSystemUtils;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.security.MessageDigest;

public class Utils {
    private static Context mContext;

    public static Context getContext() {
        return mContext;
    }

    public static void init(Context context) {
        mContext = context;
    }

    public static int getIdByName(Context context, String str, String str2) {
        return context.getResources().getIdentifier(str2, str, context.getPackageName());
    }

    public static String getAndroidid() {
        try {
            return Settings.Secure.getString(getContext().getContentResolver(), "android_id");
        } catch (Exception e) {
            QHADLog.e("获取AndroidId失败");
            return "";
        }
    }

    public static String getDeviceSerial() {
        try {
            Class<?> cls = Class.forName("android.os.SystemProperties");
            return (String) cls.getMethod("get", String.class).invoke(cls, "ro.serialno");
        } catch (Exception e) {
            return "";
        }
    }

    public static String getm2id() {
        return MD5(getIMEI() + getAndroidid() + getDeviceSerial());
    }

    public static String getAndroididWithMD5() {
        try {
            String string = Settings.Secure.getString(getContext().getContentResolver(), "android_id");
            if (string != null) {
                return MD5(string);
            }
            return string;
        } catch (Exception e) {
            QHADLog.e("获取AndroidId失败");
            return "";
        }
    }

    public static String getAppPackageName() {
        try {
            return mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), 0).packageName;
        } catch (Exception e) {
            QHADLog.e("工具-应用包名 Error=" + e.getMessage());
            return "";
        }
    }

    public static String getScreenOrientation() {
        return mContext.getResources().getConfiguration().orientation + "";
    }

    public static String getAppname() {
        try {
            PackageManager packageManager = mContext.getPackageManager();
            return packageManager.getPackageInfo(mContext.getPackageName(), 0).applicationInfo.loadLabel(packageManager).toString();
        } catch (Exception e) {
            QHADLog.e("工具-AppName Error=" + e.getMessage());
            return "";
        }
    }

    public static String getAppVersion() {
        try {
            return mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), 0).versionName;
        } catch (Exception e) {
            QHADLog.e("工具-AppVer Error=" + e.getMessage());
            return "";
        }
    }

    public static String getAppVersionCode() {
        try {
            return String.valueOf(mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), 0).versionCode);
        } catch (Exception e) {
            QHADLog.e("工具-AppVer Error=" + e.getMessage());
            return "";
        }
    }

    public static String getMac() {
        try {
            String macAddress = ((WifiManager) mContext.getSystemService(IXAdSystemUtils.NT_WIFI)).getConnectionInfo().getMacAddress();
            if (macAddress != null) {
                return macAddress;
            }
        } catch (Exception e) {
            QHADLog.e("工具-Mac Error=" + e.getMessage());
        }
        return "";
    }

    public static String getRouteMac() {
        try {
            WifiInfo connectionInfo = ((WifiManager) mContext.getSystemService(IXAdSystemUtils.NT_WIFI)).getConnectionInfo();
            if (connectionInfo.getBSSID() == null) {
                return "";
            }
            return connectionInfo.getBSSID() + "";
        } catch (Exception e) {
            QHADLog.e("工具-BSSID Error=" + e.getMessage());
            return "";
        }
    }

    public static String getRouteSSID() {
        try {
            WifiInfo connectionInfo = ((WifiManager) mContext.getSystemService(IXAdSystemUtils.NT_WIFI)).getConnectionInfo();
            if (connectionInfo.getSSID().contains("<")) {
                return "";
            }
            return connectionInfo.getSSID().replace("\"", "") + "";
        } catch (Exception e) {
            QHADLog.e("工具-SSID Error=" + e.getMessage());
            return "";
        }
    }

    public static String getMacWhitMD5() {
        try {
            String macAddress = ((WifiManager) mContext.getSystemService(IXAdSystemUtils.NT_WIFI)).getConnectionInfo().getMacAddress();
            if (macAddress != null && isNotEmpty(macAddress)) {
                return MD5(macAddress);
            }
        } catch (Exception e) {
            QHADLog.e("工具-Mac Error=" + e.getMessage());
        }
        return "";
    }

    public static String getSysteminfo() {
        try {
            return "Android%20" + Build.VERSION.RELEASE;
        } catch (Exception e) {
            QHADLog.e("工具-SysVer Error=" + e.getMessage());
            return "";
        }
    }

    public static boolean isNotEmpty(String str) {
        return str != null && !"".equals(str);
    }

    public static boolean isNetEnable() {
        try {
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) mContext.getSystemService("connectivity")).getActiveNetworkInfo();
            if (activeNetworkInfo == null || !activeNetworkInfo.isConnected()) {
                return false;
            }
            return true;
        } catch (Exception e) {
            QHADLog.e("工具-NetIsOn Error=" + e.getMessage());
            return false;
        }
    }

    public static String getIMEI() {
        Exception e;
        String str;
        try {
            TelephonyManager telephonyManager = (TelephonyManager) mContext.getSystemService("phone");
            if (telephonyManager == null) {
                return "";
            }
            str = telephonyManager.getDeviceId();
            try {
                if (TextUtils.isEmpty(str)) {
                    return Settings.Secure.getString(mContext.getContentResolver(), "android_id");
                }
                return str;
            } catch (Exception e2) {
                e = e2;
                QHADLog.e("工具-IMEI Error=" + e.getMessage());
                return str;
            }
        } catch (Exception e3) {
            Exception exc = e3;
            str = "";
            e = exc;
            QHADLog.e("工具-IMEI Error=" + e.getMessage());
            return str;
        }
    }

    public static String getIMEIWhitMD5() {
        Exception e;
        String str;
        String str2 = "";
        try {
            TelephonyManager telephonyManager = (TelephonyManager) mContext.getSystemService("phone");
            if (telephonyManager == null) {
                return str2;
            }
            str2 = telephonyManager.getDeviceId();
            if (!TextUtils.isEmpty(str2)) {
                return MD5(str2);
            }
            String string = Settings.Secure.getString(mContext.getContentResolver(), "android_id");
            if (string == null) {
                return string;
            }
            try {
                return MD5(string);
            } catch (Exception e2) {
                e = e2;
            }
        } catch (Exception e3) {
            Exception exc = e3;
            str = str2;
            e = exc;
        }
        QHADLog.e("工具-IMEI Error=" + e.getMessage());
        return str;
    }

    public static String getIMSI() {
        Exception e;
        String str;
        try {
            TelephonyManager telephonyManager = (TelephonyManager) mContext.getSystemService("phone");
            if (telephonyManager != null) {
                str = telephonyManager.getSubscriberId();
            } else {
                str = "";
            }
            try {
                if (TextUtils.isEmpty(str)) {
                    return "UNKNOWN";
                }
                return str;
            } catch (Exception e2) {
                e = e2;
                QHADLog.e("工具-IMSI Error=" + e.getMessage());
                return str;
            }
        } catch (Exception e3) {
            Exception exc = e3;
            str = "";
            e = exc;
            QHADLog.e("工具-IMSI Error=" + e.getMessage());
            return str;
        }
    }

    public static String getIMSIWhitMD5() {
        Exception e;
        String str;
        String str2 = "";
        try {
            TelephonyManager telephonyManager = (TelephonyManager) mContext.getSystemService("phone");
            if (telephonyManager == null || (str2 = telephonyManager.getSubscriberId()) == null) {
                str = str2;
            } else {
                str = MD5(str2);
            }
            try {
                if (TextUtils.isEmpty(str)) {
                    return "UNKNOWN";
                }
                return str;
            } catch (Exception e2) {
                e = e2;
                QHADLog.e("工具-IMSI Error=" + e.getMessage());
                return str;
            }
        } catch (Exception e3) {
            Exception exc = e3;
            str = str2;
            e = exc;
            QHADLog.e("工具-IMSI Error=" + e.getMessage());
            return str;
        }
    }

    public static String MD5(String str) {
        char[] cArr = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        try {
            byte[] bytes = str.getBytes();
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(bytes);
            char[] cArr2 = new char[(r4 * 2)];
            int i = 0;
            for (byte b : instance.digest()) {
                int i2 = i + 1;
                cArr2[i] = cArr[(b >>> 4) & 15];
                i = i2 + 1;
                cArr2[i2] = cArr[b & 15];
            }
            return new String(cArr2);
        } catch (Exception e) {
            QHADLog.e("工具-fileName-MD5 Error=" + e.getMessage());
            return null;
        }
    }

    public static String getBrand() {
        String str = Build.BRAND;
        if (str == null) {
            return "";
        }
        return str;
    }

    public static String getProductModel() {
        try {
            return Build.MODEL;
        } catch (Exception e) {
            QHADLog.e("工具-PhoneModel Error=" + e.getMessage());
            return "";
        }
    }

    public static boolean isSDCardEnable() {
        if (!"mounted".equals(Environment.getExternalStorageState()) || !Environment.getExternalStorageDirectory().canWrite()) {
            return false;
        }
        return true;
    }

    public static String getNetworkOperator() {
        try {
            TelephonyManager telephonyManager = (TelephonyManager) mContext.getSystemService("phone");
            if (telephonyManager.getNetworkOperator() != null) {
                return telephonyManager.getNetworkOperator();
            }
        } catch (Exception e) {
            QHADLog.e("工具-getCarrierName=" + e.getMessage());
        }
        return "";
    }

    public static String getDeviceScreenSizeWithString(Boolean bool) {
        try {
            DisplayMetrics displayMetrics = mContext.getResources().getDisplayMetrics();
            int i = displayMetrics.widthPixels;
            int i2 = displayMetrics.heightPixels;
            if (bool.booleanValue()) {
                return i + "";
            }
            return i2 + "";
        } catch (Exception e) {
            QHADLog.e("工具-ScrrenSize Error=" + e.getMessage());
            return "";
        }
    }

    public static double getDeviceDensity() {
        try {
            return (double) mContext.getResources().getDisplayMetrics().density;
        } catch (Exception e) {
            QHADLog.e("工具-Density Error=" + e.getMessage());
            return -1.0d;
        }
    }

    public static String getCurrentNetWorkInfo() {
        try {
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) mContext.getSystemService("connectivity")).getActiveNetworkInfo();
            if (activeNetworkInfo.getType() == 1) {
                return "0";
            }
            return activeNetworkInfo.getExtraInfo();
        } catch (Exception e) {
            QHADLog.e("工具-CurrentNetInfo Error=" + e.getMessage());
            return "";
        }
    }

    private static String getString(byte[] bArr) {
        StringBuffer stringBuffer = new StringBuffer();
        for (byte b : bArr) {
            stringBuffer.append((int) b);
        }
        return stringBuffer.toString();
    }

    public static int getDeviceType() {
        return (mContext.getResources().getConfiguration().screenLayout & 15) >= 3 ? 2 : 1;
    }

    public static String getCacheDir() {
        boolean z = false;
        boolean equals = "mounted".equals(Environment.getExternalStorageState());
        if (Build.VERSION.SDK_INT > 8 && !equals && !Environment.isExternalStorageRemovable()) {
            z = true;
        }
        if (!equals && !z) {
            return mContext.getCacheDir().getPath();
        }
        try {
            return mContext.getExternalCacheDir().getPath();
        } catch (Exception e) {
            QHADLog.e(e.getMessage());
            return mContext.getCacheDir().getPath();
        }
    }

    public static String stackTraceToString(Throwable th) {
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        th.printStackTrace(printWriter);
        printWriter.close();
        return stringWriter.toString();
    }

    public static String base64Encode(String str) {
        if (str == null) {
            return "";
        }
        return new String(Base64.encode(str.getBytes(), 2));
    }
}
