package com.qhad.ads.sdk.adcore;

import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.view.ViewGroup;
import com.qhad.ads.sdk.interfaces.DynamicObject;
import com.qhad.ads.sdk.interfaces.IBridge;
import com.qhad.ads.sdk.interfaces.IQhAdEventListener;
import com.qhad.ads.sdk.interfaces.IQhLandingPageView;
import com.qhad.ads.sdk.interfaces.IQhNativeAdListener;
import com.qhad.ads.sdk.interfaces.IQhVideoAdListener;
import com.qhad.ads.sdk.interfaces.ServiceBridge;
import com.qhad.ads.sdk.log.QHADLog;

class BridgeProxy implements IBridge {
    private final DynamicObject dynamicObject;

    public BridgeProxy(DynamicObject dynamicObject2) {
        this.dynamicObject = dynamicObject2;
    }

    public Object getInterstitial(Activity activity, String str, Boolean bool) {
        QHADLog.d("ADSUPDATE", "BRIDGE_getInterstitial");
        return new QhInterstitialAdProxy((DynamicObject) this.dynamicObject.invoke(12, new Object[]{activity, str, bool}));
    }

    public Object getBanner(ViewGroup viewGroup, Activity activity, String str, Boolean bool) {
        QHADLog.d("ADSUPDATE", "BRIDGE_getBanner");
        return new QhBannerAdProxy((DynamicObject) this.dynamicObject.invoke(11, new Object[]{viewGroup, activity, str, bool}));
    }

    public Object getNativeBanner(ViewGroup viewGroup, Activity activity, String str, Boolean bool) {
        QHADLog.d("ADSUPDATE", "BRIDGE_getNativeBanner");
        return new QhNativeBannerAdProxy((DynamicObject) this.dynamicObject.invoke(60, new Object[]{viewGroup, activity, str, bool}));
    }

    public Object getFloatingBanner(Activity activity, String str, Boolean bool, Integer num, Integer num2) {
        QHADLog.d("ADSUPDATE", "BRIDGE_getFloatingBanner");
        return new QhFloatbannerAdProxy((DynamicObject) this.dynamicObject.invoke(13, new Object[]{activity, str, bool, num, num2}));
    }

    public void getSplashAd(ViewGroup viewGroup, Activity activity, String str, IQhAdEventListener iQhAdEventListener, Boolean bool, Boolean bool2) {
        QHADLog.d("ADSUPDATE", "BRIDGE_getSplashAd");
        this.dynamicObject.invoke(14, new Object[]{viewGroup, activity, str, new QhAdEventListenerProxy(iQhAdEventListener), bool, bool2});
    }

    public Object getNativeAdLoader(Activity activity, String str, IQhNativeAdListener iQhNativeAdListener, Boolean bool) {
        QHADLog.d("ADSUPDATE", "BRIDGE_getNativeAdLoader");
        return new QhNativeAdLoaderProxy((DynamicObject) this.dynamicObject.invoke(15, new Object[]{activity, str, new QhNativeAdListenerProxy(iQhNativeAdListener), bool}));
    }

    public Object getVideoAdLoader(Context context, String str, IQhVideoAdListener iQhVideoAdListener, Boolean bool) {
        QHADLog.d("ADSUPDATE", "BRIDGE_getVideoAdLoader");
        return new QhVideoAdLoaderProxy((DynamicObject) this.dynamicObject.invoke(16, new Object[]{context, str, new QhVideoAdListenerProxy(iQhVideoAdListener), bool}));
    }

    public void activityDestroy(Activity activity) {
        QHADLog.d("ADSUPDATE", "BRIDGE_activityDestroy");
        this.dynamicObject.invoke(17, activity);
    }

    public ServiceBridge getServiceBridge(Service service) {
        QHADLog.d("ADSUPDATE", "BRIDGE_getServiceBridge");
        return (ServiceBridge) this.dynamicObject.invoke(18, service);
    }

    public void setLogSwitch(boolean z) {
        QHADLog.d("ADSUPDATE", "BRIDGE_setLogSwitch");
        this.dynamicObject.invoke(19, Boolean.valueOf(z));
    }

    public void setLandingPageView(IQhLandingPageView iQhLandingPageView) {
        QHADLog.d("ADSUPDATE", "BRIDGE_setLandingPageView");
        this.dynamicObject.invoke(20, new QhLandingPageViewProxy(iQhLandingPageView));
    }
}
