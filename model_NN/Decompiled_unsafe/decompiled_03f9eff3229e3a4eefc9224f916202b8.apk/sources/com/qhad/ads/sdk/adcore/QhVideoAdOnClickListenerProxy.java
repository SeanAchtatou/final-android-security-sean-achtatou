package com.qhad.ads.sdk.adcore;

import com.qhad.ads.sdk.interfaces.DynamicObject;
import com.qhad.ads.sdk.interfaces.IQhVideoAdOnClickListener;
import com.qhad.ads.sdk.log.QHADLog;

class QhVideoAdOnClickListenerProxy implements DynamicObject {
    private final IQhVideoAdOnClickListener listener;

    public QhVideoAdOnClickListenerProxy(IQhVideoAdOnClickListener iQhVideoAdOnClickListener) {
        this.listener = iQhVideoAdOnClickListener;
    }

    public Object invoke(int i, Object obj) {
        switch (i) {
            case 43:
                QHADLog.d("ADSUPDATE", "QHVIDEOADONCLICKLISTENER_onDownloadConfirmed");
                this.listener.onDownloadConfirmed();
                return null;
            case D.QHVIDEOADONCLICKLISTENER_onDownloadCancelled:
                QHADLog.d("ADSUPDATE", "QHVIDEOADONCLICKLISTENER_onDownloadCancelled");
                this.listener.onDownloadCancelled();
                return null;
            case 45:
                QHADLog.d("ADSUPDATE", "QHVIDEOADONCLICKLISTENER_onLandingpageOpened");
                this.listener.onLandingpageOpened();
                return null;
            case D.QHVIDEOADONCLICKLISTENER_onLandingpageClosed:
                QHADLog.d("ADSUPDATE", "QHVIDEOADONCLICKLISTENER_onLandingpageClosed");
                this.listener.onLandingpageClosed();
                return null;
            default:
                return null;
        }
    }
}
