package com.qhad.ads.sdk.adcore;

import com.qhad.ads.sdk.interfaces.DynamicObject;
import com.qhad.ads.sdk.interfaces.IQhAdAttributes;
import com.qhad.ads.sdk.interfaces.IQhProductAdAttributes;
import com.qhad.ads.sdk.interfaces.IQhVideoAdAttributes;
import com.qhad.ads.sdk.interfaces.ObjectDescriptor;
import com.qhad.ads.sdk.log.QHADLog;

class QhAdAttributesProxy implements DynamicObject, ObjectDescriptor {
    private final IQhAdAttributes adAttributes;
    private final Object descriptor;

    public QhAdAttributesProxy(IQhAdAttributes iQhAdAttributes) {
        this.adAttributes = iQhAdAttributes;
        if (iQhAdAttributes instanceof IQhProductAdAttributes) {
            QHADLog.d("ADSUPDATE", "TYPE_QHPRODUCTADATTRIBUTES");
            this.descriptor = 58;
        } else if (iQhAdAttributes instanceof IQhVideoAdAttributes) {
            QHADLog.d("ADSUPDATE", "TYPE_QHVIDEOADATTRIBUTES");
            this.descriptor = 57;
        } else {
            this.descriptor = null;
        }
    }

    public Object invoke(int i, Object obj) {
        switch (i) {
            case 56:
                QHADLog.d("ADSUPDATE", "QHADATTRIBUTES_getAttributes");
                return this.adAttributes.getAttributes();
            default:
                return null;
        }
    }

    public Object getDescriptor() {
        return this.descriptor;
    }
}
