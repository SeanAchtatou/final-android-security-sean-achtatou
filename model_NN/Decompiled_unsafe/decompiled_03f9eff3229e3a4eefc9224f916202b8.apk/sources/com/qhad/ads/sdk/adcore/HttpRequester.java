package com.qhad.ads.sdk.adcore;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class HttpRequester {
    /* access modifiers changed from: private */
    public static Handler handler = null;
    public static Boolean isOpenLog = true;

    public interface Listener {
        void onGetDataFailed(String str);

        void onGetDataSucceed(byte[] bArr);
    }

    public static void getAsynData(Context context, String str, Boolean bool, Listener listener) {
        if (handler == null) {
            handler = new Handler() {
                public void handleMessage(Message message) {
                    super.handleMessage(message);
                    HttpRequester.handler.post(new ResultRunable(message));
                }
            };
        }
        new Thread(new HttpRunable(str, handler, listener, context, bool)).start();
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0045  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0077 A[Catch:{ Exception -> 0x00b1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x009d  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00ab A[SYNTHETIC, Splitter:B:33:0x00ab] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static byte[] getSyncData(android.content.Context r7, java.lang.String r8, java.lang.Boolean r9) {
        /*
            r2 = 0
            com.qhad.ads.sdk.adcore.HttpCacher r1 = com.qhad.ads.sdk.adcore.HttpCacher.get(r7)     // Catch:{ Exception -> 0x0020 }
            byte[] r0 = r1.getAsBinary(r8)     // Catch:{ Exception -> 0x00c2 }
            r3 = r1
        L_0x000a:
            if (r0 == 0) goto L_0x003f
            boolean r1 = r9.booleanValue()
            if (r1 == 0) goto L_0x003f
            java.lang.Boolean r1 = com.qhad.ads.sdk.adcore.HttpRequester.isOpenLog
            boolean r1 = r1.booleanValue()
            if (r1 == 0) goto L_0x001f
            java.lang.String r1 = "同步:缓存命中"
            com.qhad.ads.sdk.log.QHADLog.d(r1)
        L_0x001f:
            return r0
        L_0x0020:
            r0 = move-exception
            r1 = r2
        L_0x0022:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Cache error"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r0 = r0.getMessage()
            java.lang.StringBuilder r0 = r3.append(r0)
            java.lang.String r0 = r0.toString()
            com.qhad.ads.sdk.log.QHADLog.e(r0)
            r0 = r2
            r3 = r1
            goto L_0x000a
        L_0x003f:
            boolean r0 = r9.booleanValue()
            if (r0 == 0) goto L_0x009d
            java.lang.Boolean r0 = com.qhad.ads.sdk.adcore.HttpRequester.isOpenLog
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x0052
            java.lang.String r0 = "同步:缓存未命中"
            com.qhad.ads.sdk.log.QHADLog.d(r0)
        L_0x0052:
            java.net.URL r0 = new java.net.URL     // Catch:{ Exception -> 0x00b1 }
            r0.<init>(r8)     // Catch:{ Exception -> 0x00b1 }
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ Exception -> 0x00b1 }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x00b1 }
            r1 = 1000(0x3e8, float:1.401E-42)
            r0.setConnectTimeout(r1)     // Catch:{ Exception -> 0x00b1 }
            r1 = 0
            r0.setUseCaches(r1)     // Catch:{ Exception -> 0x00b1 }
            java.lang.String r1 = "GET"
            r0.setRequestMethod(r1)     // Catch:{ Exception -> 0x00b1 }
            r1 = 1
            r0.setDoInput(r1)     // Catch:{ Exception -> 0x00b1 }
            int r1 = r0.getResponseCode()     // Catch:{ Exception -> 0x00b1 }
            r4 = 200(0xc8, float:2.8E-43)
            if (r1 != r4) goto L_0x00ab
            java.io.InputStream r4 = r0.getInputStream()     // Catch:{ Exception -> 0x00b1 }
            java.io.BufferedInputStream r5 = new java.io.BufferedInputStream     // Catch:{ Exception -> 0x00b1 }
            r5.<init>(r4)     // Catch:{ Exception -> 0x00b1 }
            byte[] r1 = getBytes(r4)     // Catch:{ Exception -> 0x00b1 }
            r5.close()     // Catch:{ Exception -> 0x00b1 }
            r4.close()     // Catch:{ Exception -> 0x00b1 }
            r0.disconnect()     // Catch:{ Exception -> 0x00b1 }
            boolean r0 = r9.booleanValue()     // Catch:{ Exception -> 0x00bd }
            if (r0 == 0) goto L_0x009b
            if (r3 == 0) goto L_0x009b
            r0 = 86400(0x15180, float:1.21072E-40)
            r3.put(r8, r1, r0)     // Catch:{ Exception -> 0x00bd }
        L_0x009b:
            r0 = r1
            goto L_0x001f
        L_0x009d:
            java.lang.Boolean r0 = com.qhad.ads.sdk.adcore.HttpRequester.isOpenLog
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x0052
            java.lang.String r0 = "同步:不使用缓存"
            com.qhad.ads.sdk.log.QHADLog.d(r0)
            goto L_0x0052
        L_0x00ab:
            r0.disconnect()     // Catch:{ Exception -> 0x00b1 }
            r0 = r2
            goto L_0x001f
        L_0x00b1:
            r0 = move-exception
            r1 = r0
            r0 = r2
        L_0x00b4:
            java.lang.String r1 = r1.getMessage()
            com.qhad.ads.sdk.log.QHADLog.e(r1)
            goto L_0x001f
        L_0x00bd:
            r0 = move-exception
            r6 = r0
            r0 = r1
            r1 = r6
            goto L_0x00b4
        L_0x00c2:
            r0 = move-exception
            goto L_0x0022
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qhad.ads.sdk.adcore.HttpRequester.getSyncData(android.content.Context, java.lang.String, java.lang.Boolean):byte[]");
    }

    private static byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] bArr = new byte[1024];
        while (true) {
            int read = inputStream.read(bArr, 0, 1024);
            if (read == -1) {
                return byteArrayOutputStream.toByteArray();
            }
            byteArrayOutputStream.write(bArr, 0, read);
            byteArrayOutputStream.flush();
        }
    }
}
