package com.qhad.ads.sdk.adcore;

import com.qhad.ads.sdk.interfaces.DynamicObject;
import com.qhad.ads.sdk.interfaces.IQhAdEventListener;
import com.qhad.ads.sdk.interfaces.IQhFloatbannerAd;
import com.qhad.ads.sdk.log.QHADLog;

class QhFloatbannerAdProxy implements IQhFloatbannerAd {
    private final DynamicObject dynamicObject;

    public QhFloatbannerAdProxy(DynamicObject dynamicObject2) {
        this.dynamicObject = dynamicObject2;
    }

    public void closeAds() {
        QHADLog.d("ADSUPDATE", "QHFLOATBANNERAD_closeAds");
        this.dynamicObject.invoke(21, null);
    }

    public void setAdEventListener(Object obj) {
        QHADLog.d("ADSUPDATE", "QHFLOATBANNERAD_setAdEventListener");
        this.dynamicObject.invoke(22, new QhAdEventListenerProxy((IQhAdEventListener) obj));
    }
}
