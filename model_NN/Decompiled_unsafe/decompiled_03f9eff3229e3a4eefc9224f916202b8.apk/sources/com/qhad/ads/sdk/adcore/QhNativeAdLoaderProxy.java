package com.qhad.ads.sdk.adcore;

import com.qhad.ads.sdk.interfaces.DynamicObject;
import com.qhad.ads.sdk.interfaces.IQhAdAttributes;
import com.qhad.ads.sdk.interfaces.IQhNativeAdLoader;
import com.qhad.ads.sdk.log.QHADLog;
import java.util.HashSet;

class QhNativeAdLoaderProxy implements IQhNativeAdLoader {
    private final DynamicObject dynamicObject;

    public QhNativeAdLoaderProxy(DynamicObject dynamicObject2) {
        this.dynamicObject = dynamicObject2;
    }

    public void loadAds() {
        QHADLog.d("ADSUPDATE", "QHNATIVEADLOADER_loadAds");
        this.dynamicObject.invoke(31, null);
    }

    public void loadAds(int i) {
        QHADLog.d("ADSUPDATE", "QHNATIVEADLOADER_loadAds_2");
        this.dynamicObject.invoke(32, Integer.valueOf(i));
    }

    public void setKeywords(HashSet<String> hashSet) {
        QHADLog.d("ADSUPDATE", "QHNATIVEADLOADER_setKeywords");
        this.dynamicObject.invoke(34, hashSet);
    }

    public void clearKeywords() {
        QHADLog.d("ADSUPDATE", "QHNATIVEADLOADER_clearKeywords");
        this.dynamicObject.invoke(35, null);
    }

    public void setAdAttributes(IQhAdAttributes iQhAdAttributes) {
        QHADLog.d("ADSUPDATE", "QHNATIVEADLOADER_setAdAttributes");
        this.dynamicObject.invoke(36, new QhAdAttributesProxy(iQhAdAttributes));
    }

    public void clearAdAttributes() {
        QHADLog.d("ADSUPDATE", "QHNATIVEADLOADER_clearAdAttributes");
        this.dynamicObject.invoke(37, null);
    }
}
