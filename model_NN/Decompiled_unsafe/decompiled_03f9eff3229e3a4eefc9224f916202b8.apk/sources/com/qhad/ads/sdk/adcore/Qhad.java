package com.qhad.ads.sdk.adcore;

import android.app.Activity;
import android.content.Context;
import android.view.ViewGroup;
import com.qhad.ads.sdk.interfaces.IBridge;
import com.qhad.ads.sdk.interfaces.IQhAdEventListener;
import com.qhad.ads.sdk.interfaces.IQhBannerAd;
import com.qhad.ads.sdk.interfaces.IQhFloatbannerAd;
import com.qhad.ads.sdk.interfaces.IQhInterstitialAd;
import com.qhad.ads.sdk.interfaces.IQhLandingPageView;
import com.qhad.ads.sdk.interfaces.IQhNativeAdListener;
import com.qhad.ads.sdk.interfaces.IQhNativeAdLoader;
import com.qhad.ads.sdk.interfaces.IQhNativeBannerAd;
import com.qhad.ads.sdk.interfaces.IQhVideoAdListener;
import com.qhad.ads.sdk.interfaces.IQhVideoAdLoader;
import com.qhad.ads.sdk.log.QHADLog;

public class Qhad {
    private static IQhFloatbannerAd floatbannerAd = null;
    private static IQhInterstitialAd interstitialAd = null;
    private static IBridge qhad = null;

    public enum FLOAT_BANNER_SIZE {
        SIZE_DEFAULT,
        SIZE_MATCH_PARENT,
        SIZE_640X100,
        SIZE_936x120,
        SIZE_728x90
    }

    public enum FLOAT_LOCATION {
        TOP,
        BOTTOM
    }

    public static IQhBannerAd showBanner(ViewGroup viewGroup, Activity activity, String str, Boolean bool) {
        QHADLog.i("QHAD", "Show Banner");
        if (qhad == null) {
            qhad = UpdateBridge.getBridge(activity);
            if (qhad == null) {
                return null;
            }
        }
        try {
            return (IQhBannerAd) qhad.getBanner(viewGroup, activity, str, bool);
        } catch (Throwable th) {
            QHADLog.e("初始化横幅失败:" + th.getMessage());
            return null;
        }
    }

    public static IQhNativeBannerAd showNativeBanner(ViewGroup viewGroup, Activity activity, String str, Boolean bool) {
        QHADLog.i("QHAD", "Show NativeBanner");
        if (qhad == null) {
            qhad = UpdateBridge.getBridge(activity);
            if (qhad == null) {
                return null;
            }
        }
        try {
            return (IQhNativeBannerAd) qhad.getNativeBanner(viewGroup, activity, str, bool);
        } catch (Throwable th) {
            QHADLog.e("初始化单品失败:" + th.getMessage());
            return null;
        }
    }

    public static IQhFloatbannerAd showFloatbannerAd(Activity activity, String str, Boolean bool, FLOAT_BANNER_SIZE float_banner_size, FLOAT_LOCATION float_location) {
        QHADLog.i("QHAD", "Show Float Banner");
        if (qhad == null) {
            qhad = UpdateBridge.getBridge(activity);
            if (qhad == null) {
                return null;
            }
        }
        try {
            if (floatbannerAd == null) {
                floatbannerAd = (IQhFloatbannerAd) qhad.getFloatingBanner(activity, str, bool, Integer.valueOf(float_banner_size.ordinal()), Integer.valueOf(float_location.ordinal()));
            } else {
                qhad.getFloatingBanner(activity, str, bool, Integer.valueOf(float_banner_size.ordinal()), Integer.valueOf(float_location.ordinal()));
            }
            return floatbannerAd;
        } catch (Throwable th) {
            QHADLog.e("初始化浮动横幅失败:" + th.getMessage());
            return null;
        }
    }

    public static IQhFloatbannerAd closeFloatbannerAd(Activity activity) {
        QHADLog.i("QHAD", "Close Float Banner");
        if (floatbannerAd == null) {
            return null;
        }
        floatbannerAd.closeAds();
        return null;
    }

    public static void showSplashAd(ViewGroup viewGroup, Activity activity, String str, IQhAdEventListener iQhAdEventListener, Boolean bool, Boolean bool2) {
        QHADLog.i("QHAD", "Show SplashAd");
        if (qhad == null) {
            qhad = UpdateBridge.getBridge(activity);
            if (qhad == null) {
                return;
            }
        }
        try {
            qhad.getSplashAd(viewGroup, activity, str, iQhAdEventListener, bool, bool2);
        } catch (Throwable th) {
            QHADLog.e("初始化开屏失败:" + th.getMessage());
        }
    }

    public static IQhInterstitialAd showInterstitial(Activity activity, String str, Boolean bool) {
        QHADLog.i("QHAD", "Show Interstitial");
        if (qhad == null) {
            qhad = UpdateBridge.getBridge(activity);
            if (qhad == null) {
                return null;
            }
        }
        try {
            if (interstitialAd == null) {
                interstitialAd = (IQhInterstitialAd) qhad.getInterstitial(activity, str, bool);
            } else {
                interstitialAd.showAds(activity);
            }
            return interstitialAd;
        } catch (Throwable th) {
            QHADLog.e("初始化插屏失败:" + th.getMessage());
            return null;
        }
    }

    public static IQhInterstitialAd closeInterstitial(Activity activity) {
        QHADLog.i("QHAD", "Close Interstitial");
        if (interstitialAd == null) {
            return null;
        }
        interstitialAd.closeAds();
        return null;
    }

    public static IQhNativeAdLoader initNativeAdLoader(Activity activity, String str, IQhNativeAdListener iQhNativeAdListener, Boolean bool) {
        QHADLog.i("QHAD", "initNativeAdLoader " + str);
        if (qhad == null) {
            qhad = UpdateBridge.getBridge(activity.getApplicationContext());
            if (qhad == null) {
                return null;
            }
        }
        try {
            return (IQhNativeAdLoader) qhad.getNativeAdLoader(activity, str, iQhNativeAdListener, bool);
        } catch (Throwable th) {
            QHADLog.e("初始化原生广告失败:" + th.getMessage());
            return null;
        }
    }

    public static IQhVideoAdLoader initVideoAdLoader(Context context, String str, IQhVideoAdListener iQhVideoAdListener, Boolean bool) {
        QHADLog.i("QHAD", "initVideoAdLoader " + str);
        if (qhad == null) {
            qhad = UpdateBridge.getBridge(context);
            if (qhad == null) {
                return null;
            }
        }
        try {
            return (IQhVideoAdLoader) qhad.getVideoAdLoader(context, str, iQhVideoAdListener, bool);
        } catch (Throwable th) {
            QHADLog.d("初始化视频广告失败：" + th.getMessage());
            return null;
        }
    }

    public static void activityDestroy(Activity activity) {
        if (qhad != null) {
            qhad.activityDestroy(activity);
            floatbannerAd = null;
            interstitialAd = null;
        }
    }

    public static void setLogSwitch(Context context, boolean z) {
        QHADLog.i("QHAD", "Set log switch: " + z);
        if (qhad == null) {
            qhad = UpdateBridge.getBridge(context);
            if (qhad == null) {
                return;
            }
        }
        QHADLog.logcatSwitch = z;
        qhad.setLogSwitch(z);
    }

    public static void setLandingPageView(Context context, IQhLandingPageView iQhLandingPageView) {
        QHADLog.i("QHAD", "Set custom landing page view");
        if (qhad == null) {
            qhad = UpdateBridge.getBridge(context);
            if (qhad == null) {
                return;
            }
        }
        qhad.setLandingPageView(iQhLandingPageView);
    }
}
