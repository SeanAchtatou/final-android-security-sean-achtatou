package com.qhad.ads.sdk.log;

import android.content.Context;
import android.content.SharedPreferences;
import com.qhad.ads.sdk.adcore.Config;
import com.sina.weibo.sdk.constant.WBPageConstants;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.apache.http.HttpResponse;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

public class LogUploader {
    private static final int DAILY_MAX_LIMIT = 100;
    private static final String ERROR_LOG_KEY = "qhadsdkerrordaycheck";
    private static int logid = 0;

    public static synchronized void postLog(HashMap<String, String> hashMap, Context context, boolean z) {
        synchronized (LogUploader.class) {
            if (z) {
                logid++;
                hashMap.put("elogid", logid + "");
            }
            incLogCount(context);
            if (!checkLimit(context)) {
                QHADLog.d("上传LOG数已超过上限，取消上传");
            } else {
                QHADLog.d("上传LOG");
                if (Utils.isNetEnable() && !postData(Config.ERROR_LOG_URL, hashMap) && z) {
                    LogFileManager.saveLog(hashMap);
                }
            }
        }
    }

    private static void incLogCount(Context context) {
        String str = WBPageConstants.ParamKey.COUNT + new SimpleDateFormat("yyyyMMdd").format(Long.valueOf(System.currentTimeMillis()));
        context.getSharedPreferences(ERROR_LOG_KEY, 0).edit().putInt(str, context.getSharedPreferences(ERROR_LOG_KEY, 0).getInt(str, 0) + 1).commit();
    }

    private static boolean checkLimit(Context context) {
        String str = WBPageConstants.ParamKey.COUNT + new SimpleDateFormat("yyyyMMdd").format(Long.valueOf(System.currentTimeMillis()));
        int i = context.getSharedPreferences(ERROR_LOG_KEY, 0).getInt(str, -1);
        if (i < 0) {
            SharedPreferences.Editor edit = context.getSharedPreferences(ERROR_LOG_KEY, 0).edit();
            edit.clear();
            edit.putInt(str, 0);
            edit.commit();
            return true;
        } else if (i >= 100) {
            return false;
        } else {
            return true;
        }
    }

    public static boolean postData(String str, HashMap<String, String> hashMap) {
        HttpPost httpPost = new HttpPost(str);
        ArrayList arrayList = new ArrayList();
        if (hashMap != null) {
            for (Map.Entry next : hashMap.entrySet()) {
                arrayList.add(new BasicNameValuePair((String) next.getKey(), (String) next.getValue()));
            }
        }
        try {
            httpPost.setEntity(new UrlEncodedFormEntity(arrayList, "UTF-8"));
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
            defaultHttpClient.getParams().setParameter("http.connection.timeout", 20000);
            defaultHttpClient.getParams().setParameter("http.socket.timeout", 20000);
            HttpResponse execute = defaultHttpClient.execute(httpPost);
            if (execute.getStatusLine().getStatusCode() == 200) {
                return true;
            }
            QHADLog.d("POST异常:Code=" + execute.getStatusLine().getStatusCode());
            return false;
        } catch (Exception e) {
            QHADLog.d("POST异常:" + e.getMessage());
        }
    }
}
