package com.qhad.ads.sdk.log;

import android.content.Context;
import android.util.Log;
import com.alimama.mobile.pluginframework.core.PluginFramework;
import com.qhad.ads.sdk.adcore.Config;
import java.util.HashMap;

public class QHADLog {
    private static final String TAG = "QHAD";
    /* access modifiers changed from: private */
    public static Context context;
    public static boolean logcatSwitch = true;

    private QHADLog() {
    }

    public static void init(Context context2) {
        context = context2;
        Utils.init(context);
        new Thread(new Runnable() {
            public void run() {
                LogFileManager.uploadAllLogs(QHADLog.context);
            }
        }).start();
    }

    public static void i(String str) {
        if (str != null && logcatSwitch) {
            Log.i(TAG, str);
        }
    }

    public static void d(String str) {
        if (str != null && logcatSwitch) {
            Log.d(TAG, str);
        }
    }

    public static void w(Exception exc) {
        if (exc != null && logcatSwitch) {
            exc.printStackTrace();
        }
    }

    public static void e(String str) {
        if (str != null) {
            Log.e(TAG, str);
        }
    }

    public static void e(int i, String str) {
        e(i, str, null);
    }

    public static void e(int i, String str, Throwable th) {
        if (str != null) {
            if (logcatSwitch) {
                Log.e(TAG, str);
            }
            if (context != null) {
                final HashMap hashMap = new HashMap();
                hashMap.put("etype", (i / 100) + "");
                hashMap.put("ecode", i + "");
                hashMap.put("emsg", Utils.base64Encode(str));
                hashMap.put("etime", System.currentTimeMillis() + "");
                if (th != null) {
                    hashMap.put("exception", Utils.base64Encode(th.getMessage()));
                    hashMap.put("trace", Utils.base64Encode(Utils.stackTraceToString(th)));
                }
                hashMap.putAll(getBasicParameters());
                new Thread(new Runnable() {
                    public void run() {
                        LogUploader.postLog(hashMap, QHADLog.context, true);
                    }
                }).start();
            }
        }
    }

    public static void i(String str, String str2) {
        if (str2 != null && logcatSwitch) {
            Log.i(str, str2);
        }
    }

    public static void d(String str, String str2) {
        if (str2 != null && logcatSwitch) {
            Log.d(str, str2);
        }
    }

    public static void w(String str, String str2) {
        if (str2 != null && logcatSwitch) {
            Log.w(str, str2);
        }
    }

    public static void e(String str, String str2) {
        if (str2 != null && logcatSwitch) {
            Log.e(str, str2);
        }
    }

    private static HashMap<String, String> getBasicParameters() {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("apppackagename", Utils.getAppPackageName());
        hashMap.put("appname", Utils.getAppname());
        hashMap.put("appv", Utils.getAppVersion());
        hashMap.put(PluginFramework.KEY_UPDATE_SDKV, Config.DEFAULT_SDK_VER);
        hashMap.put(LogBuilder.KEY_CHANNEL, "4");
        hashMap.put("os", Utils.getSysteminfo());
        hashMap.put("imei", Utils.getIMEI());
        hashMap.put("imei_md5", Utils.getIMEIWhitMD5());
        hashMap.put("imsi", Utils.getIMSI());
        hashMap.put("imsi_md5", Utils.getIMSIWhitMD5());
        hashMap.put("mac", Utils.getMac());
        hashMap.put("mac_md5", Utils.getMacWhitMD5());
        hashMap.put("model", Utils.getProductModel());
        hashMap.put("screenwidth", Utils.getDeviceScreenSizeWithString(true));
        hashMap.put("screenheight", Utils.getDeviceScreenSizeWithString(false));
        hashMap.put("so", Utils.getScreenOrientation());
        hashMap.put("density", Utils.getDeviceDensity() + "");
        hashMap.put("appname", Utils.getAppname());
        hashMap.put("apppkg", Utils.getAppPackageName());
        hashMap.put("net", Utils.getCurrentNetWorkInfo());
        hashMap.put("androidid", Utils.getAndroidid());
        hashMap.put("androidid_md5", Utils.getAndroididWithMD5());
        hashMap.put("brand", Utils.getBrand());
        hashMap.put("carrier", Utils.getNetworkOperator());
        hashMap.put("m2id", Utils.getm2id());
        hashMap.put("serialid", Utils.getDeviceSerial());
        hashMap.put("devicetype", Utils.getDeviceType() + "");
        hashMap.put("rmac", Utils.getRouteMac());
        hashMap.put("rssid", Utils.getRouteSSID());
        return hashMap;
    }
}
