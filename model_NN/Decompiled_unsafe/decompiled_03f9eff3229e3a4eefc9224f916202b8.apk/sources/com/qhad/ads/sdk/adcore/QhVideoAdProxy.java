package com.qhad.ads.sdk.adcore;

import android.app.Activity;
import com.qhad.ads.sdk.interfaces.DynamicObject;
import com.qhad.ads.sdk.interfaces.IQhVideoAd;
import com.qhad.ads.sdk.interfaces.IQhVideoAdOnClickListener;
import com.qhad.ads.sdk.log.QHADLog;
import org.json.JSONObject;

class QhVideoAdProxy implements IQhVideoAd {
    private final DynamicObject dynamicObject;

    public QhVideoAdProxy(DynamicObject dynamicObject2) {
        this.dynamicObject = dynamicObject2;
    }

    public JSONObject getContent() {
        return (JSONObject) this.dynamicObject.invoke(38, null);
    }

    public void onAdPlayStarted() {
        QHADLog.d("ADSUPDATE", "QHVIDEOAD_onAdPlayStarted");
        this.dynamicObject.invoke(39, null);
    }

    public void onAdPlayExit(int i) {
        QHADLog.d("ADSUPDATE", "QHVIDEOAD_onAdPlayExit");
        this.dynamicObject.invoke(40, Integer.valueOf(i));
    }

    public void onAdPlayFinshed(int i) {
        QHADLog.d("ADSUPDATE", "QHVIDEOAD_onAdPlayFinshed");
        this.dynamicObject.invoke(41, Integer.valueOf(i));
    }

    public void onAdClicked(Activity activity, int i, IQhVideoAdOnClickListener iQhVideoAdOnClickListener) {
        QHADLog.d("ADSUPDATE", "QHVIDEOAD_onAdClicked");
        this.dynamicObject.invoke(42, new Object[]{activity, Integer.valueOf(i), new QhVideoAdOnClickListenerProxy(iQhVideoAdOnClickListener)});
    }
}
