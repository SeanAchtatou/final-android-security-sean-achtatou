package com.qhad.ads.sdk.interfaces;

import android.app.Activity;
import org.json.JSONObject;

public interface IQhNativeAd {
    JSONObject getContent();

    void onAdClicked();

    void onAdClicked(Activity activity);

    void onAdShowed();
}
