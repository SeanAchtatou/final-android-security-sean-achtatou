package com.qhad.ads.sdk.adcore;

import android.app.Activity;
import com.qhad.ads.sdk.interfaces.DynamicObject;
import com.qhad.ads.sdk.interfaces.IQhNativeAd;
import com.qhad.ads.sdk.log.QHADLog;
import org.json.JSONObject;

class QhNativeAdProxy implements IQhNativeAd {
    private final DynamicObject dynamicObject;

    public QhNativeAdProxy(DynamicObject dynamicObject2) {
        this.dynamicObject = dynamicObject2;
    }

    public JSONObject getContent() {
        QHADLog.d("ADSUPDATE", "QHNATIVEAD_getContent");
        return (JSONObject) this.dynamicObject.invoke(26, null);
    }

    public void onAdShowed() {
        QHADLog.d("ADSUPDATE", "QHNATIVEAD_onAdShowed");
        this.dynamicObject.invoke(27, null);
    }

    public void onAdClicked() {
        QHADLog.d("ADSUPDATE", "QHNATIVEAD_onAdClicked");
        this.dynamicObject.invoke(28, null);
    }

    public void onAdClicked(Activity activity) {
        QHADLog.d("ADSUPDATE", "QHNATIVEAD_onAdClicked");
        this.dynamicObject.invoke(28, activity);
    }
}
