package com.qhad.ads.sdk.adcore;

import com.qhad.ads.sdk.interfaces.DynamicObject;
import com.qhad.ads.sdk.interfaces.IQhAdAttributes;
import com.qhad.ads.sdk.interfaces.IQhVideoAdLoader;
import com.qhad.ads.sdk.log.QHADLog;

class QhVideoAdLoaderProxy implements IQhVideoAdLoader {
    private final DynamicObject dynamicObject;

    public QhVideoAdLoaderProxy(DynamicObject dynamicObject2) {
        this.dynamicObject = dynamicObject2;
    }

    public void loadAds() {
        QHADLog.d("ADSUPDATE", "QHVIDEOADLOADER_loadAds");
        this.dynamicObject.invoke(51, null);
    }

    public void setAdAttributes(IQhAdAttributes iQhAdAttributes) {
        QHADLog.d("ADSUPDATE", "QHVIDEOADLOADER_setAdAttributes");
        this.dynamicObject.invoke(52, new QhAdAttributesProxy(iQhAdAttributes));
    }

    public void clearAdAttributes() {
        QHADLog.d("ADSUPDATE", "QHVIDEOADLOADER_clearAdAttributes");
        this.dynamicObject.invoke(53, null);
    }
}
