package com.qhad.ads.sdk.adcore;

import com.qhad.ads.sdk.interfaces.DynamicObject;
import com.qhad.ads.sdk.interfaces.IQhNativeAdListener;
import com.qhad.ads.sdk.log.QHADLog;
import java.util.ArrayList;
import java.util.List;

class QhNativeAdListenerProxy implements DynamicObject {
    private final IQhNativeAdListener listener;

    public QhNativeAdListenerProxy(IQhNativeAdListener iQhNativeAdListener) {
        this.listener = iQhNativeAdListener;
    }

    public Object invoke(int i, Object obj) {
        switch (i) {
            case D.QHNATIVEADLISTENER_onNativeAdLoadSucceeded:
                QHADLog.d("ADSUPDATE", "QHNATIVEADLISTENER_onNativeAdLoadSucceeded");
                ArrayList arrayList = new ArrayList();
                for (DynamicObject qhNativeAdProxy : (List) obj) {
                    arrayList.add(new QhNativeAdProxy(qhNativeAdProxy));
                }
                this.listener.onNativeAdLoadSucceeded(arrayList);
                return null;
            case D.QHNATIVEADLISTENER_onNativeAdLoadFailed:
                QHADLog.d("ADSUPDATE", "QHNATIVEADLISTENER_onNativeAdLoadFailed");
                this.listener.onNativeAdLoadFailed();
                return null;
            default:
                return null;
        }
    }
}
