package com.qhad.ads.sdk.interfaces;

import java.util.HashMap;

public interface IQhAdAttributes {
    HashMap<String, String> getAttributes();
}
