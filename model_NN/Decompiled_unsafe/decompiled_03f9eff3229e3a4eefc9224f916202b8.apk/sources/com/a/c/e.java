package com.a.c;

import android.app.Activity;
import android.app.ProgressDialog;
import android.view.View;
import android.widget.ProgressBar;
import com.a.a;

/* compiled from: Progress */
public class e implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private ProgressBar f125a;
    private ProgressDialog b;
    private Activity c;
    private View d;
    private boolean e;
    private int f;
    private int g;
    private String h;

    public e(Object obj) {
        if (obj instanceof ProgressBar) {
            this.f125a = (ProgressBar) obj;
        } else if (obj instanceof ProgressDialog) {
            this.b = (ProgressDialog) obj;
        } else if (obj instanceof Activity) {
            this.c = (Activity) obj;
        } else if (obj instanceof View) {
            this.d = (View) obj;
        }
    }

    public void a() {
        if (this.f125a != null) {
            this.f125a.setProgress(0);
            this.f125a.setMax(10000);
        }
        if (this.b != null) {
            this.b.setProgress(0);
            this.b.setMax(10000);
        }
        if (this.c != null) {
            this.c.setProgress(0);
        }
        this.e = false;
        this.g = 0;
        this.f = 10000;
    }

    public void a(int i) {
        if (i <= 0) {
            this.e = true;
            i = 10000;
        }
        this.f = i;
        if (this.f125a != null) {
            this.f125a.setProgress(0);
            this.f125a.setMax(i);
        }
        if (this.b != null) {
            this.b.setProgress(0);
            this.b.setMax(i);
        }
    }

    public void b(int i) {
        int i2;
        int i3 = 1;
        if (this.f125a != null) {
            this.f125a.incrementProgressBy(this.e ? 1 : i);
        }
        if (this.b != null) {
            ProgressDialog progressDialog = this.b;
            if (!this.e) {
                i3 = i;
            }
            progressDialog.incrementProgressBy(i3);
        }
        if (this.c != null) {
            if (this.e) {
                i2 = this.g;
                this.g = i2 + 1;
            } else {
                this.g += i;
                i2 = (this.g * 10000) / this.f;
            }
            if (i2 > 9999) {
                i2 = 9999;
            }
            this.c.setProgress(i2);
        }
    }

    public void b() {
        if (this.f125a != null) {
            this.f125a.setProgress(this.f125a.getMax());
        }
        if (this.b != null) {
            this.b.setProgress(this.b.getMax());
        }
        if (this.c != null) {
            this.c.setProgress(9999);
        }
    }

    public void run() {
        a(this.h);
    }

    private void a(String str) {
        if (this.b != null) {
            new a(this.b.getContext()).b(this.b);
        }
        if (this.c != null) {
            this.c.setProgressBarIndeterminateVisibility(false);
            this.c.setProgressBarVisibility(false);
        }
        if (this.f125a != null) {
            this.f125a.setTag(1090453505, str);
            this.f125a.setVisibility(0);
        }
        View view = this.f125a;
        if (view == null) {
            view = this.d;
        }
        if (view != null) {
            Object tag = view.getTag(1090453505);
            if (tag == null || tag.equals(str)) {
                view.setTag(1090453505, null);
                if (this.f125a != null && this.f125a.isIndeterminate()) {
                    view.setVisibility(8);
                }
            }
        }
    }
}
