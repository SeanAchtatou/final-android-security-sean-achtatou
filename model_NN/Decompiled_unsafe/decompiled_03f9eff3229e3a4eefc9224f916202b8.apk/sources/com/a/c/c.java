package com.a.c;

import android.app.Activity;
import android.app.Dialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ListAdapter;
import android.widget.ProgressBar;
import com.a.a;
import java.io.File;
import java.util.Comparator;

/* compiled from: Common */
public class c implements TextWatcher, View.OnClickListener, View.OnLongClickListener, AbsListView.OnScrollListener, AdapterView.OnItemClickListener, AdapterView.OnItemSelectedListener, Runnable, Comparator<File> {

    /* renamed from: a  reason: collision with root package name */
    private Object f124a;
    private String b;
    private Object[] c;
    private boolean d;
    private Class<?>[] e;
    private int f;
    private int g = 0;
    private AbsListView.OnScrollListener h;
    private int i;
    private AdapterView.OnItemSelectedListener j;
    private boolean k = false;

    public c a(int i2, Object... objArr) {
        this.f = i2;
        this.c = objArr;
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.c.a.a(java.lang.Object, java.lang.String, boolean, boolean, java.lang.Class<?>[], java.lang.Object[]):java.lang.Object
     arg types: [java.lang.Object, java.lang.String, boolean, int, java.lang.Class<?>[], java.lang.Object[]]
     candidates:
      com.a.c.a.a(java.lang.Object, java.lang.String, boolean, java.lang.Class<?>[], java.lang.Class<?>[], java.lang.Object[]):java.lang.Object
      com.a.c.a.a(java.lang.Object, java.lang.String, boolean, boolean, java.lang.Class<?>[], java.lang.Object[]):java.lang.Object */
    private Object a(Object... objArr) {
        Object[] objArr2;
        if (this.b != null) {
            if (this.c != null) {
                objArr2 = this.c;
            } else {
                objArr2 = objArr;
            }
            Object obj = this.f124a;
            if (obj == null) {
                obj = this;
            }
            return a.a(obj, this.b, this.d, true, this.e, objArr2);
        }
        if (this.f != 0) {
            switch (this.f) {
                case 1:
                    a.b((File) this.c[0], (byte[]) this.c[1]);
                    break;
                case 2:
                    a.a((File) this.c[0], ((Long) this.c[1]).longValue(), ((Long) this.c[2]).longValue());
                    break;
            }
        }
        return null;
    }

    /* renamed from: a */
    public int compare(File file, File file2) {
        long lastModified = file.lastModified();
        long lastModified2 = file2.lastModified();
        if (lastModified2 > lastModified) {
            return 1;
        }
        if (lastModified2 == lastModified) {
            return 0;
        }
        return -1;
    }

    public void run() {
        a(new Object[0]);
    }

    public void onClick(View view) {
        a(view);
    }

    public boolean onLongClick(View view) {
        Object a2 = a(view);
        if (a2 instanceof Boolean) {
            return ((Boolean) a2).booleanValue();
        }
        return false;
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int i2, long j2) {
        a(adapterView, view, Integer.valueOf(i2), Long.valueOf(j2));
    }

    public void onScroll(AbsListView absListView, int i2, int i3, int i4) {
        a(absListView, this.g);
        if (this.h != null) {
            this.h.onScroll(absListView, i2, i3, i4);
        }
    }

    private void a(AbsListView absListView, int i2) {
        int count = absListView.getCount();
        int lastVisiblePosition = absListView.getLastVisiblePosition();
        if (i2 != 0 || count != lastVisiblePosition + 1) {
            this.i = -1;
        } else if (lastVisiblePosition != this.i) {
            this.i = lastVisiblePosition;
            a(absListView, Integer.valueOf(i2));
        }
    }

    public void onScrollStateChanged(AbsListView absListView, int i2) {
        this.g = i2;
        a(absListView, i2);
        if (absListView instanceof ExpandableListView) {
            a((ExpandableListView) absListView, i2);
        } else {
            b(absListView, i2);
        }
        if (this.h != null) {
            this.h.onScrollStateChanged(absListView, i2);
        }
    }

    private void a(ExpandableListView expandableListView, int i2) {
        expandableListView.setTag(1090453508, Integer.valueOf(i2));
        if (i2 == 0) {
            int firstVisiblePosition = expandableListView.getFirstVisiblePosition();
            int lastVisiblePosition = expandableListView.getLastVisiblePosition() - firstVisiblePosition;
            ExpandableListAdapter expandableListAdapter = expandableListView.getExpandableListAdapter();
            for (int i3 = 0; i3 <= lastVisiblePosition; i3++) {
                long expandableListPosition = expandableListView.getExpandableListPosition(i3 + firstVisiblePosition);
                int packedPositionGroup = ExpandableListView.getPackedPositionGroup(expandableListPosition);
                int packedPositionChild = ExpandableListView.getPackedPositionChild(expandableListPosition);
                if (packedPositionGroup >= 0) {
                    View childAt = expandableListView.getChildAt(i3);
                    Long l = (Long) childAt.getTag(1090453508);
                    if (l != null && l.longValue() == expandableListPosition) {
                        if (packedPositionChild == -1) {
                            expandableListAdapter.getGroupView(packedPositionGroup, expandableListView.isGroupExpanded(packedPositionGroup), childAt, expandableListView);
                        } else {
                            expandableListAdapter.getChildView(packedPositionGroup, packedPositionChild, packedPositionChild == expandableListAdapter.getChildrenCount(packedPositionGroup) + -1, childAt, expandableListView);
                        }
                        childAt.setTag(1090453508, null);
                    }
                }
            }
        }
    }

    private void b(AbsListView absListView, int i2) {
        absListView.setTag(1090453508, Integer.valueOf(i2));
        if (i2 == 0) {
            int firstVisiblePosition = absListView.getFirstVisiblePosition();
            int lastVisiblePosition = absListView.getLastVisiblePosition() - firstVisiblePosition;
            ListAdapter listAdapter = (ListAdapter) absListView.getAdapter();
            for (int i3 = 0; i3 <= lastVisiblePosition; i3++) {
                long j2 = (long) (i3 + firstVisiblePosition);
                View childAt = absListView.getChildAt(i3);
                if (((Number) childAt.getTag(1090453508)) != null) {
                    listAdapter.getView((int) j2, childAt, absListView);
                    childAt.setTag(1090453508, null);
                }
            }
        }
    }

    public void afterTextChanged(Editable editable) {
    }

    public void beforeTextChanged(CharSequence charSequence, int i2, int i3, int i4) {
    }

    public void onTextChanged(CharSequence charSequence, int i2, int i3, int i4) {
        a(charSequence, Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf(i4));
    }

    /* JADX WARN: Type inference failed for: r2v2, types: [android.widget.Adapter] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void onItemSelected(android.widget.AdapterView<?> r9, android.view.View r10, int r11, long r12) {
        /*
            r8 = this;
            r6 = 0
            r7 = 1090453508(0x40ff0004, float:7.968752)
            r0 = 4
            java.lang.Object[] r0 = new java.lang.Object[r0]
            r0[r6] = r9
            r1 = 1
            r0[r1] = r10
            r1 = 2
            java.lang.Integer r2 = java.lang.Integer.valueOf(r11)
            r0[r1] = r2
            r1 = 3
            java.lang.Long r2 = java.lang.Long.valueOf(r12)
            r0[r1] = r2
            r8.a(r0)
            android.widget.AdapterView$OnItemSelectedListener r0 = r8.j
            if (r0 == 0) goto L_0x002a
            android.widget.AdapterView$OnItemSelectedListener r0 = r8.j
            r1 = r9
            r2 = r10
            r3 = r11
            r4 = r12
            r0.onItemSelected(r1, r2, r3, r4)
        L_0x002a:
            boolean r0 = r8.k
            if (r0 == 0) goto L_0x0050
            java.lang.Object r0 = r9.getTag(r7)
            java.lang.Integer r0 = (java.lang.Integer) r0
            int r0 = r0.intValue()
            if (r0 == r11) goto L_0x0050
            android.widget.Adapter r2 = r9.getAdapter()
            java.lang.Integer r0 = java.lang.Integer.valueOf(r11)
            r9.setTag(r7, r0)
            int r3 = r9.getChildCount()
            int r4 = r9.getFirstVisiblePosition()
            r1 = r6
        L_0x004e:
            if (r1 < r3) goto L_0x0051
        L_0x0050:
            return
        L_0x0051:
            android.view.View r5 = r9.getChildAt(r1)
            int r6 = r4 + r1
            java.lang.Object r0 = r5.getTag(r7)
            java.lang.Integer r0 = (java.lang.Integer) r0
            if (r0 == 0) goto L_0x0065
            int r0 = r0.intValue()
            if (r0 == r6) goto L_0x0068
        L_0x0065:
            r2.getView(r6, r5, r9)
        L_0x0068:
            int r0 = r1 + 1
            r1 = r0
            goto L_0x004e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.a.c.c.onItemSelected(android.widget.AdapterView, android.view.View, int, long):void");
    }

    public void onNothingSelected(AdapterView<?> adapterView) {
        if (this.j != null) {
            this.j.onNothingSelected(adapterView);
        }
    }

    public static void a(Object obj, String str, boolean z) {
        ProgressBar progressBar;
        if (obj == null) {
            return;
        }
        if (obj instanceof View) {
            View view = (View) obj;
            if (obj instanceof ProgressBar) {
                progressBar = (ProgressBar) obj;
            } else {
                progressBar = null;
            }
            if (z) {
                view.setTag(1090453505, str);
                view.setVisibility(0);
                if (progressBar != null) {
                    progressBar.setProgress(0);
                    progressBar.setMax(100);
                    return;
                }
                return;
            }
            Object tag = view.getTag(1090453505);
            if (tag == null || tag.equals(str)) {
                view.setTag(1090453505, null);
                if (progressBar == null || progressBar.isIndeterminate()) {
                    view.setVisibility(8);
                }
            }
        } else if (obj instanceof Dialog) {
            Dialog dialog = (Dialog) obj;
            a aVar = new a(dialog.getContext());
            if (z) {
                aVar.a(dialog);
            } else {
                aVar.b(dialog);
            }
        } else if (obj instanceof Activity) {
            Activity activity = (Activity) obj;
            activity.setProgressBarIndeterminateVisibility(z);
            activity.setProgressBarVisibility(z);
            if (z) {
                activity.setProgress(0);
            }
        }
    }
}
