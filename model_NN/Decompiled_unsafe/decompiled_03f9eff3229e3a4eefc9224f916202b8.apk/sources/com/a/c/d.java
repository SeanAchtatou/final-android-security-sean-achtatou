package com.a.c;

import java.io.ByteArrayOutputStream;

/* compiled from: PredefinedBAOS */
public class d extends ByteArrayOutputStream {
    public d(int i) {
        super(i);
    }

    public byte[] toByteArray() {
        if (this.count == this.buf.length) {
            return this.buf;
        }
        return super.toByteArray();
    }
}
