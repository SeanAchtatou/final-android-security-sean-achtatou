package com.a.c;

import android.graphics.Bitmap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

/* compiled from: BitmapCache */
public class b extends LinkedHashMap<String, Bitmap> {

    /* renamed from: a  reason: collision with root package name */
    private int f123a;
    private int b;
    private int c;
    private int d;

    public b(int i, int i2, int i3) {
        super(8, 0.75f, true);
        this.f123a = i;
        this.b = i2;
        this.c = i3;
    }

    /* renamed from: a */
    public Bitmap put(String str, Bitmap bitmap) {
        Bitmap bitmap2 = null;
        int a2 = a(bitmap);
        if (a2 <= this.b) {
            this.d += a2;
            bitmap2 = (Bitmap) super.put(str, bitmap);
            if (bitmap2 != null) {
                this.d -= a(bitmap2);
            }
        }
        return bitmap2;
    }

    /* renamed from: a */
    public Bitmap remove(Object obj) {
        Bitmap bitmap = (Bitmap) super.remove(obj);
        if (bitmap != null) {
            this.d -= a(bitmap);
        }
        return bitmap;
    }

    public void clear() {
        super.clear();
        this.d = 0;
    }

    private int a(Bitmap bitmap) {
        if (bitmap == null) {
            return 0;
        }
        return bitmap.getWidth() * bitmap.getHeight();
    }

    private void a() {
        if (this.d > this.c) {
            Iterator it = keySet().iterator();
            while (it.hasNext()) {
                it.next();
                it.remove();
                if (this.d <= this.c) {
                    return;
                }
            }
        }
    }

    public boolean removeEldestEntry(Map.Entry<String, Bitmap> entry) {
        if (this.d > this.c || size() > this.f123a) {
            remove(entry.getKey());
        }
        a();
        return false;
    }
}
