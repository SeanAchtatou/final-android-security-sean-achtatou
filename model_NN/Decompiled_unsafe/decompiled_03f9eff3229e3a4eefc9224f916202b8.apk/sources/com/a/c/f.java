package com.a.c;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.view.ViewGroup;
import android.widget.ImageView;
import java.lang.ref.WeakReference;

/* compiled from: RatioDrawable */
public class f extends BitmapDrawable {

    /* renamed from: a  reason: collision with root package name */
    private float f126a;
    private WeakReference<ImageView> b;
    private boolean c;
    private Matrix d;
    private int e;
    private float f;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.c.f.a(android.widget.ImageView, android.graphics.Bitmap, boolean):void
     arg types: [android.widget.ImageView, android.graphics.Bitmap, int]
     candidates:
      com.a.c.f.a(int, int, int):int
      com.a.c.f.a(android.graphics.Canvas, android.widget.ImageView, android.graphics.Bitmap):void
      com.a.c.f.a(android.widget.ImageView, android.graphics.Bitmap, boolean):void */
    public f(Resources resources, Bitmap bitmap, ImageView imageView, float f2, float f3) {
        super(resources, bitmap);
        this.b = new WeakReference<>(imageView);
        this.f126a = f2;
        this.f = f3;
        imageView.setScaleType(ImageView.ScaleType.MATRIX);
        imageView.setImageMatrix(new Matrix());
        a(imageView, bitmap, false);
    }

    private int a(ImageView imageView) {
        int i = 0;
        ViewGroup.LayoutParams layoutParams = imageView.getLayoutParams();
        if (layoutParams != null) {
            i = layoutParams.width;
        }
        if (i <= 0) {
            i = imageView.getWidth();
        }
        if (i > 0) {
            return (i - imageView.getPaddingLeft()) - imageView.getPaddingRight();
        }
        return i;
    }

    public void draw(Canvas canvas) {
        ImageView imageView = null;
        if (this.b != null) {
            imageView = this.b.get();
        }
        if (this.f126a == 0.0f || imageView == null) {
            super.draw(canvas);
        } else {
            a(canvas, imageView, getBitmap());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.c.f.a(android.widget.ImageView, android.graphics.Bitmap, boolean):void
     arg types: [android.widget.ImageView, android.graphics.Bitmap, int]
     candidates:
      com.a.c.f.a(int, int, int):int
      com.a.c.f.a(android.graphics.Canvas, android.widget.ImageView, android.graphics.Bitmap):void
      com.a.c.f.a(android.widget.ImageView, android.graphics.Bitmap, boolean):void */
    private void a(Canvas canvas, ImageView imageView, Bitmap bitmap) {
        Matrix a2 = a(imageView, bitmap);
        if (a2 != null) {
            int paddingTop = imageView.getPaddingTop() + imageView.getPaddingBottom();
            int paddingLeft = imageView.getPaddingLeft() + imageView.getPaddingRight();
            if (paddingTop > 0 || paddingLeft > 0) {
                canvas.clipRect(0, 0, imageView.getWidth() - paddingLeft, imageView.getHeight() - paddingTop);
            }
            canvas.drawBitmap(bitmap, a2, getPaint());
        }
        if (!this.c) {
            a(imageView, bitmap, true);
        }
    }

    private void a(ImageView imageView, Bitmap bitmap, boolean z) {
        int a2 = a(imageView);
        if (a2 > 0) {
            int a3 = a(bitmap.getWidth(), bitmap.getHeight(), a2) + imageView.getPaddingTop() + imageView.getPaddingBottom();
            ViewGroup.LayoutParams layoutParams = imageView.getLayoutParams();
            if (layoutParams != null) {
                if (a3 != layoutParams.height) {
                    layoutParams.height = a3;
                    imageView.setLayoutParams(layoutParams);
                }
                if (z) {
                    this.c = true;
                }
            }
        }
    }

    private int a(int i, int i2, int i3) {
        float f2 = this.f126a;
        if (this.f126a == Float.MAX_VALUE) {
            f2 = ((float) i2) / ((float) i);
        }
        return (int) (f2 * ((float) i3));
    }

    private Matrix a(ImageView imageView, Bitmap bitmap) {
        float f2;
        float f3;
        float f4 = 0.0f;
        int width = bitmap.getWidth();
        if (this.d != null && width == this.e) {
            return this.d;
        }
        int height = bitmap.getHeight();
        int a2 = a(imageView);
        int a3 = a(width, height, a2);
        if (width <= 0 || height <= 0 || a2 <= 0 || a3 <= 0) {
            return null;
        }
        if (this.d == null || width != this.e) {
            this.d = new Matrix();
            if (width * a3 >= a2 * height) {
                f2 = ((float) a3) / ((float) height);
                f3 = (((float) a2) - (((float) width) * f2)) * 0.5f;
            } else {
                f2 = ((float) a2) / ((float) width);
                float a4 = (((float) a3) - (((float) height) * f2)) * a(width, height);
                f3 = 0.0f;
                f4 = a4;
            }
            this.d.setScale(f2, f2);
            this.d.postTranslate(f3, f4);
            this.e = width;
        }
        return this.d;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    private float a(int i, int i2) {
        if (this.f != Float.MAX_VALUE) {
            return (1.0f - this.f) / 2.0f;
        }
        return ((1.5f - Math.max(1.0f, Math.min(1.5f, ((float) i2) / ((float) i)))) / 2.0f) + 0.25f;
    }
}
