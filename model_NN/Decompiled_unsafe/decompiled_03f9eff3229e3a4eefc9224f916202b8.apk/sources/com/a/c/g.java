package com.a.c;

import android.util.Xml;
import cn.banshenggua.aichang.utils.StringUtil;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.Arrays;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xmlpull.v1.XmlSerializer;

/* compiled from: XmlDom */
public class g {

    /* renamed from: a  reason: collision with root package name */
    private Element f127a;

    public g(InputStream inputStream) throws SAXException {
        try {
            this.f127a = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(inputStream).getDocumentElement();
        } catch (ParserConfigurationException e) {
        } catch (IOException e2) {
            throw new SAXException(e2);
        }
    }

    public String toString() {
        return a(0);
    }

    public String a(int i) {
        return a(this.f127a, i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Arrays.fill(char[], char):void}
     arg types: [char[], int]
     candidates:
      ClspMth{java.util.Arrays.fill(double[], double):void}
      ClspMth{java.util.Arrays.fill(byte[], byte):void}
      ClspMth{java.util.Arrays.fill(long[], long):void}
      ClspMth{java.util.Arrays.fill(boolean[], boolean):void}
      ClspMth{java.util.Arrays.fill(short[], short):void}
      ClspMth{java.util.Arrays.fill(java.lang.Object[], java.lang.Object):void}
      ClspMth{java.util.Arrays.fill(int[], int):void}
      ClspMth{java.util.Arrays.fill(float[], float):void}
      ClspMth{java.util.Arrays.fill(char[], char):void} */
    private String a(Element element, int i) {
        String str;
        try {
            XmlSerializer newSerializer = Xml.newSerializer();
            StringWriter stringWriter = new StringWriter();
            newSerializer.setOutput(stringWriter);
            newSerializer.startDocument(StringUtil.Encoding, null);
            if (i > 0) {
                char[] cArr = new char[i];
                Arrays.fill(cArr, ' ');
                str = new String(cArr);
            } else {
                str = null;
            }
            a(this.f127a, newSerializer, 0, str);
            newSerializer.endDocument();
            return stringWriter.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private void a(XmlSerializer xmlSerializer, int i, String str) throws Exception {
        if (str != null) {
            xmlSerializer.text("\n");
            for (int i2 = 0; i2 < i; i2++) {
                xmlSerializer.text(str);
            }
        }
    }

    private String a(Node node) {
        String str = null;
        switch (node.getNodeType()) {
            case 3:
                str = node.getNodeValue();
                if (str != null) {
                    str = str.trim();
                    break;
                }
                break;
            case 4:
                str = node.getNodeValue();
                break;
        }
        if (str == null) {
            return "";
        }
        return str;
    }

    private void a(Element element, XmlSerializer xmlSerializer, int i, String str) throws Exception {
        int i2 = 0;
        String tagName = element.getTagName();
        a(xmlSerializer, i, str);
        xmlSerializer.startTag("", tagName);
        if (element.hasAttributes()) {
            NamedNodeMap attributes = element.getAttributes();
            for (int i3 = 0; i3 < attributes.getLength(); i3++) {
                Attr attr = (Attr) attributes.item(i3);
                xmlSerializer.attribute("", attr.getName(), attr.getValue());
            }
        }
        if (element.hasChildNodes()) {
            NodeList childNodes = element.getChildNodes();
            int i4 = 0;
            while (i2 < childNodes.getLength()) {
                Node item = childNodes.item(i2);
                switch (item.getNodeType()) {
                    case 1:
                        a((Element) item, xmlSerializer, i + 1, str);
                        i4++;
                        break;
                    case 3:
                        xmlSerializer.text(a(item));
                        break;
                    case 4:
                        xmlSerializer.cdsect(a(item));
                        break;
                }
                i2++;
                i4 = i4;
            }
            if (i4 > 0) {
                a(xmlSerializer, i, str);
            }
        }
        xmlSerializer.endTag("", tagName);
    }
}
