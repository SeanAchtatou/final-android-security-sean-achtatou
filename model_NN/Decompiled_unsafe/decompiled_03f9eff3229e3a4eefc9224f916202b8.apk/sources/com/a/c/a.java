package com.a.c;

import android.content.Context;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Thread;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/* compiled from: AQUtility */
public class a {

    /* renamed from: a  reason: collision with root package name */
    public static boolean f122a = false;
    private static boolean b = false;
    private static Object c;
    private static Thread.UncaughtExceptionHandler d;
    private static Map<String, Long> e = new HashMap();
    private static Handler f;
    private static ScheduledExecutorService g;
    private static File h;
    private static File i;
    private static final char[] j = new char[64];
    private static final byte[] k = new byte[128];

    static {
        char c2 = 'A';
        int i2 = 0;
        while (c2 <= 'Z') {
            j[i2] = c2;
            c2 = (char) (c2 + 1);
            i2++;
        }
        char c3 = 'a';
        while (c3 <= 'z') {
            j[i2] = c3;
            c3 = (char) (c3 + 1);
            i2++;
        }
        char c4 = '0';
        while (c4 <= '9') {
            j[i2] = c4;
            c4 = (char) (c4 + 1);
            i2++;
        }
        int i3 = i2 + 1;
        j[i2] = '+';
        int i4 = i3 + 1;
        j[i3] = '/';
        for (int i5 = 0; i5 < k.length; i5++) {
            k[i5] = -1;
        }
        for (int i6 = 0; i6 < 64; i6++) {
            k[j[i6]] = (byte) i6;
        }
    }

    public static void a() {
        if (b && c != null) {
            synchronized (c) {
                c.notifyAll();
            }
        }
    }

    public static void a(Object obj) {
        if (b) {
            Log.w("AQuery", new StringBuilder().append(obj).toString());
        }
    }

    public static void a(Object obj, Object obj2) {
        Log.w("AQuery", obj + ":" + obj2);
    }

    public static void b(Object obj, Object obj2) {
        if (b) {
            Log.w("AQuery", obj + ":" + obj2);
        }
    }

    public static void a(Throwable th) {
        if (b) {
            Log.w("AQuery", Log.getStackTraceString(th));
        }
    }

    public static void b(Throwable th) {
        if (th != null) {
            try {
                a("reporting", Log.getStackTraceString(th));
                if (d != null) {
                    d.uncaughtException(Thread.currentThread(), th);
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    public static Object a(Object obj, String str, boolean z, boolean z2, Class<?>[] clsArr, Object... objArr) {
        return a(obj, str, z, z2, clsArr, null, objArr);
    }

    public static Object a(Object obj, String str, boolean z, boolean z2, Class<?>[] clsArr, Class<?>[] clsArr2, Object... objArr) {
        try {
            return a(obj, str, z, clsArr, clsArr2, objArr);
        } catch (Exception e2) {
            if (z2) {
                b(e2);
            } else {
                a((Throwable) e2);
            }
            return null;
        }
    }

    private static Object a(Object obj, String str, boolean z, Class<?>[] clsArr, Class<?>[] clsArr2, Object... objArr) throws Exception {
        if (obj == null || str == null) {
            return null;
        }
        if (clsArr == null) {
            try {
                clsArr = new Class[0];
            } catch (NoSuchMethodException e2) {
                if (!z) {
                    return null;
                }
                if (clsArr2 != null) {
                    return obj.getClass().getMethod(str, clsArr2).invoke(obj, objArr);
                }
                try {
                    return obj.getClass().getMethod(str, new Class[0]).invoke(obj, new Object[0]);
                } catch (NoSuchMethodException e3) {
                    return null;
                }
            }
        }
        return obj.getClass().getMethod(str, clsArr).invoke(obj, objArr);
    }

    public static boolean b() {
        return Looper.getMainLooper().getThread().getId() == Thread.currentThread().getId();
    }

    public static Handler c() {
        if (f == null) {
            f = new Handler(Looper.getMainLooper());
        }
        return f;
    }

    public static void a(Runnable runnable) {
        c().post(runnable);
    }

    private static String a(String str) {
        return new BigInteger(a(str.getBytes())).abs().toString(36);
    }

    private static byte[] a(byte[] bArr) {
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(bArr);
            return instance.digest();
        } catch (NoSuchAlgorithmException e2) {
            b(e2);
            return null;
        }
    }

    public static void a(InputStream inputStream, OutputStream outputStream) throws IOException {
        a(inputStream, outputStream, 0, null);
    }

    public static void a(InputStream inputStream, OutputStream outputStream, int i2, e eVar) throws IOException {
        if (eVar != null) {
            eVar.a();
            eVar.a(i2);
        }
        byte[] bArr = new byte[4096];
        int i3 = 0;
        while (true) {
            int read = inputStream.read(bArr);
            if (read != -1) {
                outputStream.write(bArr, 0, read);
                i3++;
                if (f122a && i3 > 2) {
                    a((Object) "simulating internet error");
                    throw new IOException();
                } else if (eVar != null) {
                    eVar.b(read);
                }
            } else if (eVar != null) {
                eVar.b();
                return;
            } else {
                return;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.c.a.a(java.io.InputStream, java.io.OutputStream):void
     arg types: [java.io.InputStream, java.io.ByteArrayOutputStream]
     candidates:
      com.a.c.a.a(android.content.Context, int):java.io.File
      com.a.c.a.a(java.io.File, java.lang.String):java.io.File
      com.a.c.a.a(java.io.File, byte[]):void
      com.a.c.a.a(java.lang.Object, java.lang.Object):void
      com.a.c.a.a(java.io.File[], long):boolean
      com.a.c.a.a(java.io.InputStream, java.io.OutputStream):void */
    public static byte[] a(InputStream inputStream) {
        byte[] bArr = null;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            a(inputStream, (OutputStream) byteArrayOutputStream);
            bArr = byteArrayOutputStream.toByteArray();
        } catch (IOException e2) {
            b(e2);
        }
        a((Closeable) inputStream);
        return bArr;
    }

    public static void a(File file, byte[] bArr) {
        try {
            if (!file.exists()) {
                try {
                    file.createNewFile();
                } catch (Exception e2) {
                    b("file create fail", file);
                    b(e2);
                }
            }
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(bArr);
            fileOutputStream.close();
        } catch (Exception e3) {
            b(e3);
        }
    }

    public static void a(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (Exception e2) {
            }
        }
    }

    private static ScheduledExecutorService e() {
        if (g == null) {
            g = Executors.newSingleThreadScheduledExecutor();
        }
        return g;
    }

    public static void a(File file, byte[] bArr, long j2) {
        e().schedule(new c().a(1, file, bArr), j2, TimeUnit.MILLISECONDS);
    }

    public static File a(Context context, int i2) {
        if (i2 != 1) {
            return a(context);
        }
        if (i != null) {
            return i;
        }
        i = new File(a(context), "persistent");
        i.mkdirs();
        return i;
    }

    public static File a(Context context) {
        if (h == null) {
            h = new File(context.getCacheDir(), "aquery");
            h.mkdirs();
        }
        return h;
    }

    private static File c(File file, String str) {
        return new File(file, str);
    }

    private static String b(String str) {
        return a(str);
    }

    public static File a(File file, String str) {
        if (str == null) {
            return null;
        }
        if (str.startsWith(File.separator)) {
            return new File(str);
        }
        return c(file, b(str));
    }

    public static File b(File file, String str) {
        File a2 = a(file, str);
        if (a2 == null || !a2.exists() || a2.length() == 0) {
            return null;
        }
        return a2;
    }

    public static void b(File file, byte[] bArr) {
        if (file != null) {
            try {
                a(file, bArr);
            } catch (Exception e2) {
                b(e2);
            }
        }
    }

    public static void a(File file, long j2, long j3) {
        try {
            File[] listFiles = file.listFiles();
            if (listFiles != null) {
                Arrays.sort(listFiles, new c());
                if (a(listFiles, j2)) {
                    b(listFiles, j3);
                }
                File d2 = d();
                if (d2 != null && d2.exists()) {
                    b(d2.listFiles(), 0);
                }
            }
        } catch (Exception e2) {
            b(e2);
        }
    }

    public static File d() {
        File file = new File(Environment.getExternalStorageDirectory(), "aquery/temp");
        file.mkdirs();
        if (!file.exists() || !file.canWrite()) {
            return null;
        }
        return file;
    }

    private static boolean a(File[] fileArr, long j2) {
        long j3 = 0;
        for (File length : fileArr) {
            j3 += length.length();
            if (j3 > j2) {
                return true;
            }
        }
        return false;
    }

    private static void b(File[] fileArr, long j2) {
        long j3 = 0;
        int i2 = 0;
        for (File file : fileArr) {
            if (file.isFile()) {
                j3 += file.length();
                if (j3 >= j2) {
                    file.delete();
                    i2++;
                }
            }
        }
        b("deleted", Integer.valueOf(i2));
    }
}
