package com.a;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;
import com.a.a.a;
import com.a.b;
import com.a.b.d;
import com.a.b.f;
import java.util.WeakHashMap;
import org.apache.http.HttpHost;

/* compiled from: AbstractAQuery */
public abstract class b<T extends b<T>> {
    private static final Class<?>[] j = {View.class};
    private static Class<?>[] k = {AdapterView.class, View.class, Integer.TYPE, Long.TYPE};
    private static Class<?>[] l = {AbsListView.class, Integer.TYPE};
    private static final Class<?>[] m = {CharSequence.class, Integer.TYPE, Integer.TYPE, Integer.TYPE};
    private static Class<?>[] n = {Integer.TYPE, Integer.TYPE};
    private static final Class<?>[] o = {Integer.TYPE};
    private static Class<?>[] p = {Integer.TYPE, Paint.class};
    private static WeakHashMap<Dialog, Void> q = new WeakHashMap<>();

    /* renamed from: a  reason: collision with root package name */
    protected View f118a;
    protected Object b;
    protected a c;
    private View d;
    private Activity e;
    private Context f;
    private f g;
    private int h = 0;
    private HttpHost i;

    public b(View view) {
        this.d = view;
        this.f118a = view;
    }

    public b(Context context) {
        this.f = context;
    }

    private View b(int i2) {
        if (this.d != null) {
            return this.d.findViewById(i2);
        }
        if (this.e != null) {
            return this.e.findViewById(i2);
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public T a() {
        return this;
    }

    public T a(int i2) {
        return a(b(i2));
    }

    public T a(View view) {
        this.f118a = view;
        c();
        return a();
    }

    public T a(CharSequence charSequence) {
        if (this.f118a instanceof TextView) {
            ((TextView) this.f118a).setText(charSequence);
        }
        return a();
    }

    public T a(String str, boolean z, boolean z2) {
        return a(str, z, z2, 0, 0);
    }

    public T a(String str, boolean z, boolean z2, int i2, int i3) {
        return a(str, z, z2, i2, i3, null, 0);
    }

    public T a(String str, boolean z, boolean z2, int i2, int i3, Bitmap bitmap, int i4) {
        return a(str, z, z2, i2, i3, bitmap, i4, 0.0f);
    }

    public T a(String str, boolean z, boolean z2, int i2, int i3, Bitmap bitmap, int i4, float f2) {
        return a(str, z, z2, i2, i3, bitmap, i4, f2, 0, null);
    }

    /* access modifiers changed from: protected */
    public T a(String str, boolean z, boolean z2, int i2, int i3, Bitmap bitmap, int i4, float f2, int i5, String str2) {
        if (this.f118a instanceof ImageView) {
            d.a(this.e, b(), (ImageView) this.f118a, str, z, z2, i2, i3, bitmap, i4, f2, Float.MAX_VALUE, this.b, this.c, this.h, i5, this.i, str2);
            c();
        }
        return a();
    }

    public Context b() {
        if (this.e != null) {
            return this.e;
        }
        if (this.d != null) {
            return this.d.getContext();
        }
        return this.f;
    }

    /* access modifiers changed from: protected */
    public void c() {
        this.c = null;
        this.b = null;
        this.g = null;
        this.h = 0;
        this.i = null;
    }

    public T a(Dialog dialog) {
        if (dialog != null) {
            try {
                dialog.show();
                q.put(dialog, null);
            } catch (Exception e2) {
            }
        }
        return a();
    }

    public T b(Dialog dialog) {
        if (dialog != null) {
            try {
                q.remove(dialog);
                dialog.dismiss();
            } catch (Exception e2) {
            }
        }
        return a();
    }
}
