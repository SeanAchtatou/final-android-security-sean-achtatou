package com.a.a;

import com.a.b.c;
import java.net.HttpURLConnection;
import java.util.LinkedHashSet;
import org.apache.http.HttpRequest;

/* compiled from: AccountHandle */
public abstract class a {

    /* renamed from: a  reason: collision with root package name */
    private LinkedHashSet<com.a.b.a<?, ?>> f117a;

    public abstract boolean a();

    public abstract boolean a(com.a.b.a<?, ?> aVar, c cVar);

    /* access modifiers changed from: protected */
    public abstract void b();

    public abstract boolean b(com.a.b.a<?, ?> aVar);

    public synchronized void a(com.a.b.a<?, ?> aVar) {
        if (this.f117a == null) {
            this.f117a = new LinkedHashSet<>();
            this.f117a.add(aVar);
            b();
        } else {
            this.f117a.add(aVar);
        }
    }

    public void a(com.a.b.a<?, ?> aVar, HttpRequest httpRequest) {
    }

    public void a(com.a.b.a<?, ?> aVar, HttpURLConnection httpURLConnection) {
    }

    public String a(String str) {
        return str;
    }

    public String b(String str) {
        return str;
    }
}
