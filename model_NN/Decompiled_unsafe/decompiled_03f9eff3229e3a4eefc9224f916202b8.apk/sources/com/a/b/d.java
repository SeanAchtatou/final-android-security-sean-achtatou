package com.a.b;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.media.ExifInterface;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import com.a.c.a;
import com.a.c.b;
import com.a.c.c;
import com.a.c.f;
import com.qq.e.comm.constants.ErrorCode;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.WeakHashMap;
import org.apache.http.HttpHost;

/* compiled from: BitmapAjaxCallback */
public class d extends a<Bitmap, d> {
    private static Bitmap F = Bitmap.createBitmap(1, 1, Bitmap.Config.ALPHA_8);
    private static Bitmap G = Bitmap.createBitmap(1, 1, Bitmap.Config.ALPHA_8);
    private static int i = 20;
    private static int j = 20;
    private static int k = 2500;
    private static int l = 160000;
    private static int m = 1000000;
    private static boolean n = false;
    private static Map<String, Bitmap> o;
    private static Map<String, Bitmap> p;
    private static Map<String, Bitmap> q;
    private static HashMap<String, WeakHashMap<ImageView, d>> r = new HashMap<>();
    private int A;
    private boolean B = true;
    private float C = Float.MAX_VALUE;
    private boolean D;
    private boolean E;
    private WeakReference<ImageView> s;
    private int t;
    private int u;
    private File v;
    private Bitmap w;
    private int x;
    private Bitmap y;
    private float z;

    public d() {
        ((d) ((d) ((d) a(Bitmap.class)).b(true)).a(true)).a("");
    }

    public d a(ImageView imageView) {
        this.s = new WeakReference<>(imageView);
        return this;
    }

    public d b(int i2) {
        this.t = i2;
        return this;
    }

    public d a(Bitmap bitmap) {
        this.y = bitmap;
        return this;
    }

    public d c(int i2) {
        this.u = i2;
        return this;
    }

    public d d(int i2) {
        this.x = i2;
        return this;
    }

    public d a(float f) {
        this.z = f;
        return this;
    }

    public d b(float f) {
        this.C = f;
        return this;
    }

    public d e(int i2) {
        this.A = i2;
        return this;
    }

    private static Bitmap a(String str, byte[] bArr, BitmapFactory.Options options, boolean z2) {
        Bitmap bitmap = null;
        if (str != null) {
            bitmap = a(str, options, z2);
        } else if (bArr != null) {
            bitmap = BitmapFactory.decodeByteArray(bArr, 0, bArr.length, options);
        }
        if (bitmap == null && options != null && !options.inJustDecodeBounds) {
            a.b("decode image failed", str);
        }
        return bitmap;
    }

    private static Bitmap a(String str, BitmapFactory.Options options, boolean z2) {
        FileInputStream fileInputStream;
        IOException e;
        Bitmap bitmap;
        Bitmap bitmap2;
        if (options == null) {
            options = new BitmapFactory.Options();
        }
        options.inInputShareable = true;
        options.inPurgeable = true;
        try {
            fileInputStream = new FileInputStream(str);
            try {
                bitmap = BitmapFactory.decodeFileDescriptor(fileInputStream.getFD(), null, options);
                if (bitmap != null && z2) {
                    try {
                        bitmap = b(str, bitmap);
                    } catch (IOException e2) {
                        e = e2;
                    }
                }
                a.a((Closeable) fileInputStream);
            } catch (IOException e3) {
                IOException iOException = e3;
                bitmap2 = null;
                e = iOException;
            }
        } catch (IOException e4) {
            fileInputStream = null;
            IOException iOException2 = e4;
            bitmap2 = null;
            e = iOException2;
            try {
                a.b(e);
                a.a((Closeable) fileInputStream);
                return bitmap;
            } catch (Throwable th) {
                th = th;
                a.a((Closeable) fileInputStream);
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            fileInputStream = null;
            a.a((Closeable) fileInputStream);
            throw th;
        }
        return bitmap;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    private static Bitmap b(String str, Bitmap bitmap) {
        int i2;
        if (bitmap == null) {
            return null;
        }
        try {
            i2 = new ExifInterface(str).getAttributeInt("Orientation", 1);
        } catch (Exception e) {
            a.a((Throwable) e);
            i2 = 1;
        }
        if (i2 <= 0) {
            return bitmap;
        }
        Bitmap createBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), f(i2), true);
        a.b("before", String.valueOf(bitmap.getWidth()) + ":" + bitmap.getHeight());
        a.b("after", String.valueOf(createBitmap.getWidth()) + ":" + createBitmap.getHeight());
        if (bitmap != createBitmap) {
            bitmap.recycle();
        }
        return createBitmap;
    }

    private static Matrix f(int i2) {
        Matrix matrix = new Matrix();
        switch (i2) {
            case 2:
                matrix.setScale(-1.0f, 1.0f);
                break;
            case 3:
                matrix.setRotate(180.0f);
                break;
            case 4:
                matrix.setRotate(180.0f);
                matrix.postScale(-1.0f, 1.0f);
                break;
            case 5:
                matrix.setRotate(90.0f);
                matrix.postScale(-1.0f, 1.0f);
                break;
            case 6:
                matrix.setRotate(90.0f);
                break;
            case 7:
                matrix.setRotate(-90.0f);
                matrix.postScale(-1.0f, 1.0f);
                break;
            case 8:
                matrix.setRotate(-90.0f);
                break;
        }
        return matrix;
    }

    public static Bitmap a(String str, byte[] bArr, int i2, boolean z2, int i3, boolean z3) {
        BitmapFactory.Options options;
        Bitmap bitmap;
        if (str == null && bArr == null) {
            return null;
        }
        if (i2 > 0) {
            BitmapFactory.Options options2 = new BitmapFactory.Options();
            options2.inJustDecodeBounds = true;
            a(str, bArr, options2, z3);
            int i4 = options2.outWidth;
            if (!z2) {
                i4 = Math.max(i4, options2.outHeight);
            }
            int a2 = a(i4, i2);
            options = new BitmapFactory.Options();
            options.inSampleSize = a2;
        } else {
            options = null;
        }
        try {
            bitmap = a(str, bArr, options, z3);
        } catch (OutOfMemoryError e) {
            g();
            a.b(e);
            bitmap = null;
        }
        if (i3 > 0) {
            bitmap = a(bitmap, i3);
        }
        return bitmap;
    }

    private static int a(int i2, int i3) {
        int i4 = 1;
        for (int i5 = 0; i5 < 10 && i2 >= i3 * 2; i5++) {
            i2 /= 2;
            i4 *= 2;
        }
        return i4;
    }

    private Bitmap a(String str, byte[] bArr) {
        return a(str, bArr, this.t, this.B, this.A, this.E);
    }

    /* access modifiers changed from: protected */
    public File a(File file, String str) {
        if (this.v == null || !this.v.exists()) {
            return super.a(file, str);
        }
        return this.v;
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public Bitmap a(String str, File file, c cVar) {
        return a(file.getAbsolutePath(), (byte[]) null);
    }

    /* renamed from: b */
    public Bitmap a(String str, byte[] bArr, c cVar) {
        String str2 = null;
        File j2 = cVar.j();
        if (j2 != null) {
            str2 = j2.getAbsolutePath();
        }
        Bitmap a2 = a(str2, bArr);
        if (a2 == null) {
            if (this.u > 0) {
                a2 = h();
            } else if (this.u == -2 || this.u == -1) {
                a2 = G;
            } else if (this.u == -3) {
                a2 = this.y;
            }
            if (cVar.g() != 200) {
                this.D = true;
            }
            if (cVar.k() == 1 && j2 != null) {
                a.a((Object) "invalid bm from net");
                j2.delete();
            }
        }
        return a2;
    }

    private Bitmap h() {
        View view = this.s.get();
        if (view == null) {
            return null;
        }
        String num = Integer.toString(this.u);
        Bitmap e = d(num);
        if (e != null) {
            return e;
        }
        Bitmap decodeResource = BitmapFactory.decodeResource(view.getResources(), this.u);
        if (decodeResource == null) {
            return decodeResource;
        }
        a(num, decodeResource);
        return decodeResource;
    }

    public final void a(String str, Bitmap bitmap, c cVar) {
        ImageView imageView = this.s.get();
        WeakHashMap remove = r.remove(str);
        if (remove == null || !remove.containsKey(imageView)) {
            a(this, str, imageView, bitmap, cVar);
        }
        if (remove != null) {
            for (ImageView imageView2 : remove.keySet()) {
                d dVar = (d) remove.get(imageView2);
                dVar.f = cVar;
                a(dVar, str, imageView2, bitmap, cVar);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void b(String str, Bitmap bitmap, c cVar) {
        r.remove(str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.b.d.a(java.lang.String, android.widget.ImageView, android.graphics.Bitmap, boolean):void
     arg types: [java.lang.String, android.widget.ImageView, android.graphics.Bitmap, int]
     candidates:
      com.a.b.d.a(java.lang.String, byte[], android.graphics.BitmapFactory$Options, boolean):android.graphics.Bitmap
      com.a.b.d.a(android.widget.ImageView, android.graphics.Bitmap, float, float):android.graphics.drawable.Drawable
      com.a.b.d.a(java.lang.String, android.widget.ImageView, android.graphics.Bitmap, com.a.b.c):void
      com.a.b.a.a(java.io.DataOutputStream, java.lang.String, java.lang.String, java.io.InputStream):void
      com.a.b.a.a(java.lang.String, org.apache.http.client.methods.HttpEntityEnclosingRequestBase, java.util.Map<java.lang.String, java.lang.Object>, com.a.b.c):void
      com.a.b.a.a(java.lang.String, java.lang.Object, java.io.File, byte[]):void
      com.a.b.d.a(java.lang.String, android.widget.ImageView, android.graphics.Bitmap, boolean):void */
    private void a(d dVar, String str, ImageView imageView, Bitmap bitmap, c cVar) {
        if (imageView != null && dVar != null) {
            if (str.equals(imageView.getTag(1090453505))) {
                if (imageView instanceof ImageView) {
                    dVar.a(str, imageView, bitmap, cVar);
                } else {
                    dVar.a(str, imageView, bitmap, false);
                }
            }
            dVar.c(false);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.b.d.a(java.lang.String, android.widget.ImageView, android.graphics.Bitmap, boolean):void
     arg types: [java.lang.String, android.widget.ImageView, android.graphics.Bitmap, int]
     candidates:
      com.a.b.d.a(java.lang.String, byte[], android.graphics.BitmapFactory$Options, boolean):android.graphics.Bitmap
      com.a.b.d.a(android.widget.ImageView, android.graphics.Bitmap, float, float):android.graphics.drawable.Drawable
      com.a.b.d.a(java.lang.String, android.widget.ImageView, android.graphics.Bitmap, com.a.b.c):void
      com.a.b.a.a(java.io.DataOutputStream, java.lang.String, java.lang.String, java.io.InputStream):void
      com.a.b.a.a(java.lang.String, org.apache.http.client.methods.HttpEntityEnclosingRequestBase, java.util.Map<java.lang.String, java.lang.Object>, com.a.b.c):void
      com.a.b.a.a(java.lang.String, java.lang.Object, java.io.File, byte[]):void
      com.a.b.d.a(java.lang.String, android.widget.ImageView, android.graphics.Bitmap, boolean):void */
    /* access modifiers changed from: protected */
    public void a(String str, ImageView imageView, Bitmap bitmap, c cVar) {
        a(str, imageView, bitmap, false);
    }

    public static void g() {
        p = null;
        o = null;
        q = null;
    }

    private static Map<String, Bitmap> i() {
        if (p == null) {
            p = Collections.synchronizedMap(new b(j, l, m));
        }
        return p;
    }

    private static Map<String, Bitmap> j() {
        if (o == null) {
            o = Collections.synchronizedMap(new b(i, k, 250000));
        }
        return o;
    }

    private static Map<String, Bitmap> k() {
        if (q == null) {
            q = Collections.synchronizedMap(new b(100, l, 250000));
        }
        return q;
    }

    /* access modifiers changed from: protected */
    /* renamed from: e */
    public Bitmap d(String str) {
        if (this.w != null) {
            return this.w;
        }
        if (!this.h) {
            return null;
        }
        return a(str, this.t, this.A);
    }

    private static Bitmap a(String str, int i2, int i3) {
        String b = b(str, i2, i3);
        Bitmap bitmap = i().get(b);
        if (bitmap == null) {
            bitmap = j().get(b);
        }
        if (bitmap != null) {
            return bitmap;
        }
        Bitmap bitmap2 = k().get(b);
        if (bitmap2 == null || f() != 200) {
            return bitmap2;
        }
        q = null;
        return null;
    }

    private static String b(String str, int i2, int i3) {
        String str2;
        if (i2 > 0) {
            str2 = String.valueOf(str) + "#" + i2;
        } else {
            str2 = str;
        }
        if (i3 > 0) {
            return String.valueOf(str2) + "#" + i3;
        }
        return str2;
    }

    private static void a(String str, int i2, int i3, Bitmap bitmap, boolean z2) {
        Map<String, Bitmap> i4;
        if (bitmap != null) {
            int width = bitmap.getWidth() * bitmap.getHeight();
            if (z2) {
                i4 = k();
            } else if (width <= k) {
                i4 = j();
            } else {
                i4 = i();
            }
            if (i2 > 0 || i3 > 0) {
                i4.put(b(str, i2, i3), bitmap);
                if (!i4.containsKey(str)) {
                    i4.put(str, null);
                    return;
                }
                return;
            }
            i4.put(str, bitmap);
        }
    }

    /* access modifiers changed from: protected */
    public void a(String str, Bitmap bitmap) {
        a(str, this.t, this.A, bitmap, this.D);
    }

    private static Bitmap a(View view, Bitmap bitmap, int i2) {
        if (bitmap != null && bitmap.getWidth() == 1 && bitmap.getHeight() == 1 && bitmap != F) {
            bitmap = null;
        }
        if (bitmap != null) {
            view.setVisibility(0);
        } else if (i2 == -2) {
            view.setVisibility(8);
        } else if (i2 == -1) {
            view.setVisibility(4);
        }
        return bitmap;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.b.d.a(java.lang.String, android.widget.ImageView, android.graphics.Bitmap, boolean):void
     arg types: [java.lang.String, android.widget.ImageView, android.graphics.Bitmap, int]
     candidates:
      com.a.b.d.a(java.lang.String, byte[], android.graphics.BitmapFactory$Options, boolean):android.graphics.Bitmap
      com.a.b.d.a(android.widget.ImageView, android.graphics.Bitmap, float, float):android.graphics.drawable.Drawable
      com.a.b.d.a(java.lang.String, android.widget.ImageView, android.graphics.Bitmap, com.a.b.c):void
      com.a.b.a.a(java.io.DataOutputStream, java.lang.String, java.lang.String, java.io.InputStream):void
      com.a.b.a.a(java.lang.String, org.apache.http.client.methods.HttpEntityEnclosingRequestBase, java.util.Map<java.lang.String, java.lang.Object>, com.a.b.c):void
      com.a.b.a.a(java.lang.String, java.lang.Object, java.io.File, byte[]):void
      com.a.b.d.a(java.lang.String, android.widget.ImageView, android.graphics.Bitmap, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.b.d.a(java.lang.String, android.widget.ImageView, android.graphics.Bitmap, boolean):void
     arg types: [java.lang.String, android.widget.ImageView, ?[OBJECT, ARRAY], int]
     candidates:
      com.a.b.d.a(java.lang.String, byte[], android.graphics.BitmapFactory$Options, boolean):android.graphics.Bitmap
      com.a.b.d.a(android.widget.ImageView, android.graphics.Bitmap, float, float):android.graphics.drawable.Drawable
      com.a.b.d.a(java.lang.String, android.widget.ImageView, android.graphics.Bitmap, com.a.b.c):void
      com.a.b.a.a(java.io.DataOutputStream, java.lang.String, java.lang.String, java.io.InputStream):void
      com.a.b.a.a(java.lang.String, org.apache.http.client.methods.HttpEntityEnclosingRequestBase, java.util.Map<java.lang.String, java.lang.Object>, com.a.b.c):void
      com.a.b.a.a(java.lang.String, java.lang.Object, java.io.File, byte[]):void
      com.a.b.d.a(java.lang.String, android.widget.ImageView, android.graphics.Bitmap, boolean):void */
    private void a(String str, ImageView imageView) {
        if (!str.equals(imageView.getTag(1090453505)) || this.y != null) {
            imageView.setTag(1090453505, str);
            if (this.y == null || b(imageView.getContext())) {
                a(str, imageView, (Bitmap) null, true);
            } else {
                a(str, imageView, this.y, true);
            }
        }
    }

    private void a(String str, ImageView imageView, Bitmap bitmap, boolean z2) {
        if (bitmap == null) {
            imageView.setImageDrawable(null);
        } else if (z2) {
            imageView.setImageDrawable(a(imageView, bitmap, this.z, this.C));
        } else if (this.f != null) {
            a(imageView, bitmap, this.y, this.u, this.x, this.z, this.C, this.f.k());
        }
    }

    private static Drawable a(ImageView imageView, Bitmap bitmap, float f, float f2) {
        if (f > 0.0f) {
            return new f(imageView.getResources(), bitmap, imageView, f, f2);
        }
        return new BitmapDrawable(imageView.getResources(), bitmap);
    }

    private static void a(ImageView imageView, Bitmap bitmap, Bitmap bitmap2, int i2, int i3, float f, float f2, int i4) {
        Animation animation;
        Bitmap a2 = a(imageView, bitmap, i2);
        if (a2 == null) {
            imageView.setImageBitmap(null);
            return;
        }
        Drawable a3 = a(imageView, a2, f, f2);
        if (b(i3, i4)) {
            if (bitmap2 == null) {
                animation = new AlphaAnimation(0.0f, 1.0f);
                animation.setInterpolator(new DecelerateInterpolator());
                animation.setDuration(300);
            } else {
                TransitionDrawable transitionDrawable = new TransitionDrawable(new Drawable[]{a(imageView, bitmap2, f, f2), a3});
                transitionDrawable.setCrossFadeEnabled(true);
                transitionDrawable.startTransition(ErrorCode.InitError.INIT_AD_ERROR);
                a3 = transitionDrawable;
                animation = null;
            }
        } else if (i3 > 0) {
            animation = AnimationUtils.loadAnimation(imageView.getContext(), i3);
        } else {
            animation = null;
        }
        imageView.setImageDrawable(a3);
        if (animation != null) {
            animation.setStartTime(AnimationUtils.currentAnimationTimeMillis());
            imageView.startAnimation(animation);
            return;
        }
        imageView.setAnimation(null);
    }

    /* JADX WARNING: Removed duplicated region for block: B:2:0x0004 A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:8:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean b(int r2, int r3) {
        /*
            r0 = 1
            switch(r2) {
                case -3: goto L_0x0006;
                case -2: goto L_0x0009;
                case -1: goto L_0x0005;
                default: goto L_0x0004;
            }
        L_0x0004:
            r0 = 0
        L_0x0005:
            return r0
        L_0x0006:
            r1 = 3
            if (r3 == r1) goto L_0x0005
        L_0x0009:
            if (r3 != r0) goto L_0x0004
            goto L_0x0005
        */
        throw new UnsupportedOperationException("Method not decompiled: com.a.b.d.b(int, int):boolean");
    }

    public static void a(Activity activity, Context context, ImageView imageView, String str, boolean z2, boolean z3, int i2, int i3, Bitmap bitmap, int i4, float f, float f2, Object obj, com.a.a.a aVar, int i5, int i6, HttpHost httpHost, String str2) {
        Bitmap bitmap2 = null;
        if (z2) {
            bitmap2 = a(str, i2, i6);
        }
        if (bitmap2 != null) {
            imageView.setTag(1090453505, str);
            c.a(obj, str, false);
            a(imageView, bitmap2, bitmap, i3, i4, f, f2, 4);
            return;
        }
        d dVar = new d();
        ((d) ((d) ((d) ((d) ((d) ((d) dVar.a(str)).a(imageView).b(z2)).a(z3)).b(i2).c(i3).a(bitmap).d(i4).a(f).b(f2).a(obj)).a(aVar)).a(i5)).e(i6).b(str2);
        if (httpHost != null) {
            dVar.a(httpHost.getHostName(), httpHost.getPort());
        }
        if (activity != null) {
            dVar.a(activity);
        } else {
            dVar.a(context);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.b.d.a(java.lang.String, android.widget.ImageView, android.graphics.Bitmap, boolean):void
     arg types: [java.lang.String, android.widget.ImageView, ?[OBJECT, ARRAY], int]
     candidates:
      com.a.b.d.a(java.lang.String, byte[], android.graphics.BitmapFactory$Options, boolean):android.graphics.Bitmap
      com.a.b.d.a(android.widget.ImageView, android.graphics.Bitmap, float, float):android.graphics.drawable.Drawable
      com.a.b.d.a(java.lang.String, android.widget.ImageView, android.graphics.Bitmap, com.a.b.c):void
      com.a.b.a.a(java.io.DataOutputStream, java.lang.String, java.lang.String, java.io.InputStream):void
      com.a.b.a.a(java.lang.String, org.apache.http.client.methods.HttpEntityEnclosingRequestBase, java.util.Map<java.lang.String, java.lang.Object>, com.a.b.c):void
      com.a.b.a.a(java.lang.String, java.lang.Object, java.io.File, byte[]):void
      com.a.b.d.a(java.lang.String, android.widget.ImageView, android.graphics.Bitmap, boolean):void */
    public void a(Context context) {
        String d = d();
        ImageView imageView = this.s.get();
        if (d == null) {
            c(false);
            a(d, imageView, (Bitmap) null, false);
            return;
        }
        Bitmap e = d(d);
        if (e != null) {
            imageView.setTag(1090453505, d);
            this.f = new c().a(4).a();
            a(d, e, this.f);
            return;
        }
        a(d, imageView);
        if (!r.containsKey(d)) {
            b(d, imageView);
            super.a(imageView.getContext());
            return;
        }
        c(true);
        b(d, imageView);
    }

    /* access modifiers changed from: protected */
    public boolean c() {
        return !n;
    }

    private void b(String str, ImageView imageView) {
        WeakHashMap weakHashMap = r.get(str);
        if (weakHashMap != null) {
            weakHashMap.put(imageView, this);
        } else if (r.containsKey(str)) {
            WeakHashMap weakHashMap2 = new WeakHashMap();
            weakHashMap2.put(imageView, this);
            r.put(str, weakHashMap2);
        } else {
            r.put(str, null);
        }
    }

    private static Bitmap a(Bitmap bitmap, int i2) {
        Bitmap createBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        Paint paint = new Paint();
        Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        RectF rectF = new RectF(rect);
        float f = (float) i2;
        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(-12434878);
        canvas.drawRoundRect(rectF, f, f, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        return createBitmap;
    }
}
