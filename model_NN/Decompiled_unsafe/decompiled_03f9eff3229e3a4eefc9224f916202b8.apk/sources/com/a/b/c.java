package com.a.b;

import com.a.c.a;
import java.io.Closeable;
import java.io.File;
import java.util.Date;
import org.apache.http.Header;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HttpContext;

/* compiled from: AjaxStatus */
public class c {

    /* renamed from: a  reason: collision with root package name */
    private int f121a = 200;
    private String b = "OK";
    private String c;
    private byte[] d;
    private File e;
    private Date f = new Date();
    private boolean g;
    private DefaultHttpClient h;
    private long i;
    private int j = 1;
    private long k = System.currentTimeMillis();
    private boolean l;
    private boolean m;
    private boolean n;
    private String o;
    private HttpContext p;
    private Header[] q;
    private Closeable r;

    /* access modifiers changed from: protected */
    public c a(int i2) {
        this.j = i2;
        return this;
    }

    public c b(int i2) {
        this.f121a = i2;
        return this;
    }

    /* access modifiers changed from: protected */
    public c a(String str) {
        this.o = str;
        return this;
    }

    public c b(String str) {
        this.b = str;
        return this;
    }

    /* access modifiers changed from: protected */
    public c c(String str) {
        this.c = str;
        return this;
    }

    /* access modifiers changed from: protected */
    public c a(HttpContext httpContext) {
        this.p = httpContext;
        return this;
    }

    /* access modifiers changed from: protected */
    public c a(Date date) {
        this.f = date;
        return this;
    }

    /* access modifiers changed from: protected */
    public c a(boolean z) {
        this.g = z;
        return this;
    }

    /* access modifiers changed from: protected */
    public c b(boolean z) {
        this.n = z;
        return this;
    }

    /* access modifiers changed from: protected */
    public c a(DefaultHttpClient defaultHttpClient) {
        this.h = defaultHttpClient;
        return this;
    }

    /* access modifiers changed from: protected */
    public c a(Header[] headerArr) {
        this.q = headerArr;
        return this;
    }

    public c a() {
        this.i = System.currentTimeMillis() - this.k;
        this.l = true;
        this.n = false;
        return this;
    }

    /* access modifiers changed from: protected */
    public c b() {
        this.i = System.currentTimeMillis() - this.k;
        this.l = false;
        c();
        return this;
    }

    /* access modifiers changed from: protected */
    public void a(Closeable closeable) {
        this.r = closeable;
    }

    public void c() {
        a.a(this.r);
        this.r = null;
    }

    /* access modifiers changed from: protected */
    public c a(byte[] bArr) {
        this.d = bArr;
        return this;
    }

    /* access modifiers changed from: protected */
    public c a(File file) {
        this.e = file;
        return this;
    }

    /* access modifiers changed from: protected */
    public boolean d() {
        return this.l;
    }

    /* access modifiers changed from: protected */
    public boolean e() {
        return this.n;
    }

    /* access modifiers changed from: protected */
    public boolean f() {
        return this.m;
    }

    public int g() {
        return this.f121a;
    }

    public String h() {
        return this.b;
    }

    /* access modifiers changed from: protected */
    public byte[] i() {
        return this.d;
    }

    /* access modifiers changed from: protected */
    public File j() {
        return this.e;
    }

    public int k() {
        return this.j;
    }

    public String d(String str) {
        if (this.q == null) {
            return null;
        }
        for (int i2 = 0; i2 < this.q.length; i2++) {
            if (str.equalsIgnoreCase(this.q[i2].getName())) {
                return this.q[i2].getValue();
            }
        }
        return null;
    }
}
