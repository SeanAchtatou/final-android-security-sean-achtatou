package com.a.b;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import cn.banshenggua.aichang.room.message.SocketMessage;
import cn.banshenggua.aichang.utils.StringUtil;
import com.a.c.c;
import com.a.c.e;
import com.a.c.g;
import com.qq.e.comm.constants.ErrorCode;
import java.io.ByteArrayInputStream;
import java.io.Closeable;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.conn.params.ConnPerRouteBean;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.scheme.SocketFactory;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HttpContext;
import org.apache.mina.proxy.handlers.http.HttpProxyConstants;
import org.xmlpull.v1.XmlPullParser;

/* compiled from: AbstractAjaxCallback */
public abstract class a<T, K> implements Runnable {
    private static f J;
    private static final Class<?>[] L = {String.class, Object.class, c.class};
    private static ExecutorService P;
    private static SocketFactory Q;
    private static DefaultHttpClient R;
    private static e S;
    private static int T = 200;
    private static int i = 30000;
    private static String j = null;
    private static int k = 4;
    private static boolean l = true;
    private static boolean m = true;
    private static boolean n = false;
    private int A = 0;
    private boolean B = true;
    private long C;
    private String D = "UTF-8";
    private WeakReference<Activity> E;
    private int F = 4;
    private HttpUriRequest G;
    private boolean H = true;
    private int I = 0;
    private HttpHost K;
    private boolean M;
    private boolean N;
    private boolean O;
    private boolean U;

    /* renamed from: a  reason: collision with root package name */
    protected Map<String, Object> f119a;
    protected Map<String, String> b;
    protected Map<String, String> c;
    protected T d;
    protected com.a.a.a e;
    protected c f;
    protected boolean g;
    protected boolean h;
    private Class<T> o;
    private Reference<Object> p;
    private Object q;
    private String r;
    private WeakReference<Object> s;
    /* access modifiers changed from: private */
    public String t;
    private String u;
    private f v;
    private int w = 0;
    private File x;
    private File y;
    private boolean z;

    private K g() {
        return this;
    }

    private void h() {
        this.p = null;
        this.q = null;
        this.s = null;
        this.G = null;
        this.v = null;
        this.e = null;
        this.E = null;
    }

    public K a(String str) {
        this.t = str;
        return g();
    }

    public K b(String str) {
        this.u = str;
        return g();
    }

    public K a(Class<T> cls) {
        this.o = cls;
        return g();
    }

    public K a(boolean z2) {
        this.g = z2;
        return g();
    }

    public K b(boolean z2) {
        this.h = z2;
        return g();
    }

    public K a(int i2) {
        this.w = i2;
        return g();
    }

    public K a(String str, int i2) {
        this.K = new HttpHost(str, i2);
        return g();
    }

    public K a(Object obj) {
        if (obj != null) {
            this.s = new WeakReference<>(obj);
        }
        return g();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.b.a.a(java.lang.String, java.lang.Object, com.a.b.c):void
     arg types: [java.lang.String, T, com.a.b.c]
     candidates:
      com.a.b.a.a(byte[], java.lang.String, com.a.b.c):java.lang.String
      com.a.b.a.a(org.apache.http.client.methods.HttpUriRequest, org.apache.http.impl.client.DefaultHttpClient, org.apache.http.protocol.HttpContext):org.apache.http.HttpResponse
      com.a.b.a.a(java.io.DataOutputStream, java.lang.String, java.lang.Object):void
      com.a.b.a.a(java.io.DataOutputStream, java.lang.String, java.lang.String):void
      com.a.b.a.a(java.io.InputStream, java.io.OutputStream, int):void
      com.a.b.a.a(java.lang.String, java.util.Map<java.lang.String, java.lang.Object>, com.a.b.c):void
      com.a.b.a.a(org.apache.http.client.methods.HttpUriRequest, java.lang.String, com.a.b.c):void
      com.a.b.a.a(java.lang.String, java.io.File, com.a.b.c):T
      com.a.b.a.a(java.lang.String, byte[], com.a.b.c):T
      com.a.b.a.a(java.lang.String, java.lang.Object, com.a.b.c):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.b.a.b(java.lang.String, java.lang.Object, com.a.b.c):void
     arg types: [java.lang.String, T, com.a.b.c]
     candidates:
      com.a.b.a.b(java.lang.String, java.util.Map<java.lang.String, java.lang.Object>, com.a.b.c):void
      com.a.b.a.b(java.lang.String, java.lang.Object, com.a.b.c):void */
    /* access modifiers changed from: package-private */
    public void a() {
        c(false);
        this.M = true;
        if (!j()) {
            b(this.t, (Object) this.d, this.f);
        } else if (this.r != null) {
            Object e2 = e();
            Class[] clsArr = {String.class, this.o, c.class};
            com.a.c.a.a(e2, this.r, true, true, clsArr, L, this.t, this.d, this.f);
        } else {
            try {
                a(this.t, (Object) this.d, this.f);
            } catch (Exception e3) {
                com.a.c.a.b(e3);
            }
        }
        q();
        if (!this.N) {
            this.f.c();
        }
        i();
        com.a.c.a.a();
    }

    private void i() {
        if (this.N) {
            synchronized (this) {
                try {
                    notifyAll();
                } catch (Exception e2) {
                }
            }
        }
    }

    public void a(String str, T t2, c cVar) {
    }

    /* access modifiers changed from: protected */
    public void b(String str, T t2, c cVar) {
    }

    /* access modifiers changed from: protected */
    public T a(String str, File file, c cVar) {
        byte[] a2;
        try {
            if (c()) {
                cVar.a(file);
                a2 = null;
            } else {
                a2 = com.a.c.a.a((InputStream) new FileInputStream(file));
            }
            return a(str, a2, cVar);
        } catch (Exception e2) {
            com.a.c.a.a((Throwable) e2);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public T c(String str) {
        return null;
    }

    /* access modifiers changed from: protected */
    public void c(boolean z2) {
        Object obj = this.s == null ? null : this.s.get();
        if (obj == null) {
            return;
        }
        if (com.a.c.a.b()) {
            c.a(obj, this.t, z2);
        } else {
            com.a.c.a.a((Runnable) new b(this, obj, z2));
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected T a(java.lang.String r7, byte[] r8, com.a.b.c r9) {
        /*
            r6 = this;
            r1 = 0
            java.lang.Class<T> r0 = r6.o
            if (r0 != 0) goto L_0x0006
        L_0x0005:
            return r1
        L_0x0006:
            java.io.File r0 = r9.j()
            if (r8 == 0) goto L_0x00ce
            java.lang.Class<T> r0 = r6.o
            java.lang.Class<android.graphics.Bitmap> r2 = android.graphics.Bitmap.class
            boolean r0 = r0.equals(r2)
            if (r0 == 0) goto L_0x001d
            r0 = 0
            int r1 = r8.length
            android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeByteArray(r8, r0, r1)
            goto L_0x0005
        L_0x001d:
            java.lang.Class<T> r0 = r6.o
            java.lang.Class<org.json.JSONObject> r2 = org.json.JSONObject.class
            boolean r0 = r0.equals(r2)
            if (r0 == 0) goto L_0x0045
            java.lang.String r2 = new java.lang.String     // Catch:{ Exception -> 0x003b }
            java.lang.String r0 = r6.D     // Catch:{ Exception -> 0x003b }
            r2.<init>(r8, r0)     // Catch:{ Exception -> 0x003b }
            org.json.JSONTokener r0 = new org.json.JSONTokener     // Catch:{ Exception -> 0x013c }
            r0.<init>(r2)     // Catch:{ Exception -> 0x013c }
            java.lang.Object r0 = r0.nextValue()     // Catch:{ Exception -> 0x013c }
            org.json.JSONObject r0 = (org.json.JSONObject) r0     // Catch:{ Exception -> 0x013c }
        L_0x0039:
            r1 = r0
            goto L_0x0005
        L_0x003b:
            r0 = move-exception
            r2 = r1
        L_0x003d:
            com.a.c.a.a(r0)
            com.a.c.a.a(r2)
            r0 = r1
            goto L_0x0039
        L_0x0045:
            java.lang.Class<T> r0 = r6.o
            java.lang.Class<org.json.JSONArray> r2 = org.json.JSONArray.class
            boolean r0 = r0.equals(r2)
            if (r0 == 0) goto L_0x0069
            java.lang.String r0 = new java.lang.String     // Catch:{ Exception -> 0x0063 }
            java.lang.String r2 = r6.D     // Catch:{ Exception -> 0x0063 }
            r0.<init>(r8, r2)     // Catch:{ Exception -> 0x0063 }
            org.json.JSONTokener r2 = new org.json.JSONTokener     // Catch:{ Exception -> 0x0063 }
            r2.<init>(r0)     // Catch:{ Exception -> 0x0063 }
            java.lang.Object r0 = r2.nextValue()     // Catch:{ Exception -> 0x0063 }
            org.json.JSONArray r0 = (org.json.JSONArray) r0     // Catch:{ Exception -> 0x0063 }
        L_0x0061:
            r1 = r0
            goto L_0x0005
        L_0x0063:
            r0 = move-exception
            com.a.c.a.a(r0)
            r0 = r1
            goto L_0x0061
        L_0x0069:
            java.lang.Class<T> r0 = r6.o
            java.lang.Class<java.lang.String> r2 = java.lang.String.class
            boolean r0 = r0.equals(r2)
            if (r0 == 0) goto L_0x009b
            int r0 = r9.k()
            r2 = 1
            if (r0 != r2) goto L_0x0086
            java.lang.String r0 = "network"
            com.a.c.a.a(r0)
            java.lang.String r0 = r6.D
            java.lang.String r1 = r6.a(r8, r0, r9)
            goto L_0x0005
        L_0x0086:
            java.lang.String r0 = "file"
            com.a.c.a.a(r0)
            java.lang.String r0 = new java.lang.String     // Catch:{ Exception -> 0x0095 }
            java.lang.String r2 = r6.D     // Catch:{ Exception -> 0x0095 }
            r0.<init>(r8, r2)     // Catch:{ Exception -> 0x0095 }
            r1 = r0
            goto L_0x0005
        L_0x0095:
            r0 = move-exception
            com.a.c.a.a(r0)
            goto L_0x0005
        L_0x009b:
            java.lang.Class<T> r0 = r6.o
            java.lang.Class<byte[]> r2 = byte[].class
            boolean r0 = r0.equals(r2)
            if (r0 == 0) goto L_0x00a8
            r1 = r8
            goto L_0x0005
        L_0x00a8:
            com.a.b.f r0 = r6.v
            if (r0 == 0) goto L_0x00bb
            com.a.b.f r0 = r6.v
            java.lang.Class<T> r2 = r6.o
            java.lang.String r3 = r6.D
            r1 = r7
            r4 = r8
            r5 = r9
            java.lang.Object r1 = r0.a(r1, r2, r3, r4, r5)
            goto L_0x0005
        L_0x00bb:
            com.a.b.f r0 = com.a.b.a.J
            if (r0 == 0) goto L_0x0005
            com.a.b.f r0 = com.a.b.a.J
            java.lang.Class<T> r2 = r6.o
            java.lang.String r3 = r6.D
            r1 = r7
            r4 = r8
            r5 = r9
            java.lang.Object r1 = r0.a(r1, r2, r3, r4, r5)
            goto L_0x0005
        L_0x00ce:
            if (r0 == 0) goto L_0x0005
            java.lang.Class<T> r2 = r6.o
            java.lang.Class<java.io.File> r3 = java.io.File.class
            boolean r2 = r2.equals(r3)
            if (r2 == 0) goto L_0x00dd
            r1 = r0
            goto L_0x0005
        L_0x00dd:
            java.lang.Class<T> r2 = r6.o
            java.lang.Class<com.a.c.g> r3 = com.a.c.g.class
            boolean r2 = r2.equals(r3)
            if (r2 == 0) goto L_0x00fd
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ Exception -> 0x00f7 }
            r2.<init>(r0)     // Catch:{ Exception -> 0x00f7 }
            com.a.c.g r0 = new com.a.c.g     // Catch:{ Exception -> 0x00f7 }
            r0.<init>(r2)     // Catch:{ Exception -> 0x00f7 }
            r9.a(r2)     // Catch:{ Exception -> 0x00f7 }
            r1 = r0
            goto L_0x0005
        L_0x00f7:
            r0 = move-exception
            com.a.c.a.b(r0)
            goto L_0x0005
        L_0x00fd:
            java.lang.Class<T> r2 = r6.o
            java.lang.Class<org.xmlpull.v1.XmlPullParser> r3 = org.xmlpull.v1.XmlPullParser.class
            boolean r2 = r2.equals(r3)
            if (r2 == 0) goto L_0x0121
            org.xmlpull.v1.XmlPullParser r8 = android.util.Xml.newPullParser()
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ Exception -> 0x011b }
            r2.<init>(r0)     // Catch:{ Exception -> 0x011b }
            java.lang.String r0 = r6.D     // Catch:{ Exception -> 0x011b }
            r8.setInput(r2, r0)     // Catch:{ Exception -> 0x011b }
            r9.a(r2)     // Catch:{ Exception -> 0x011b }
            r1 = r8
            goto L_0x0005
        L_0x011b:
            r0 = move-exception
            com.a.c.a.b(r0)
            goto L_0x0005
        L_0x0121:
            java.lang.Class<T> r2 = r6.o
            java.lang.Class<java.io.InputStream> r3 = java.io.InputStream.class
            boolean r2 = r2.equals(r3)
            if (r2 == 0) goto L_0x0005
            java.io.FileInputStream r8 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0136 }
            r8.<init>(r0)     // Catch:{ Exception -> 0x0136 }
            r9.a(r8)     // Catch:{ Exception -> 0x0136 }
            r1 = r8
            goto L_0x0005
        L_0x0136:
            r0 = move-exception
            com.a.c.a.b(r0)
            goto L_0x0005
        L_0x013c:
            r0 = move-exception
            goto L_0x003d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.a.b.a.a(java.lang.String, byte[], com.a.b.c):java.lang.Object");
    }

    private String e(String str) {
        Matcher matcher = Pattern.compile("<meta [^>]*http-equiv[^>]*\"Content-Type\"[^>]*>", 2).matcher(str);
        if (!matcher.find()) {
            return null;
        }
        return f(matcher.group());
    }

    private String f(String str) {
        int indexOf;
        if (str == null || (indexOf = str.indexOf("charset")) == -1) {
            return null;
        }
        int indexOf2 = str.indexOf(";", indexOf);
        if (indexOf2 == -1) {
            indexOf2 = str.length();
        }
        return str.substring(indexOf + 7, indexOf2).replaceAll("[^\\w-]", "");
    }

    private String a(byte[] bArr, String str, c cVar) {
        Exception e2;
        String str2;
        try {
            if (!StringUtil.Encoding.equalsIgnoreCase(str)) {
                return new String(bArr, str);
            }
            String f2 = f(cVar.d("Content-Type"));
            com.a.c.a.b("parsing header", f2);
            if (f2 != null) {
                return new String(bArr, f2);
            }
            String str3 = new String(bArr, StringUtil.Encoding);
            try {
                String e3 = e(str3);
                com.a.c.a.b("parsing needed", e3);
                if (e3 == null || StringUtil.Encoding.equalsIgnoreCase(e3)) {
                    return str3;
                }
                com.a.c.a.b("correction needed", e3);
                str2 = new String(bArr, e3);
                try {
                    cVar.a(str2.getBytes(StringUtil.Encoding));
                    return str2;
                } catch (Exception e4) {
                    e2 = e4;
                }
            } catch (Exception e5) {
                Exception exc = e5;
                str2 = str3;
                e2 = exc;
                com.a.c.a.b(e2);
                return str2;
            }
        } catch (Exception e6) {
            e2 = e6;
            str2 = null;
            com.a.c.a.b(e2);
            return str2;
        }
    }

    /* access modifiers changed from: protected */
    public T d(String str) {
        return null;
    }

    /* access modifiers changed from: protected */
    public void a(String str, T t2) {
    }

    /* access modifiers changed from: protected */
    public void a(String str, T t2, File file, byte[] bArr) {
        if (file != null && bArr != null) {
            com.a.c.a.a(file, bArr, 0);
        }
    }

    /* access modifiers changed from: protected */
    public File a(File file, String str) {
        if (this.C < 0) {
            return null;
        }
        File b2 = com.a.c.a.b(file, str);
        if (b2 == null || this.C == 0 || System.currentTimeMillis() - b2.lastModified() <= this.C) {
            return b2;
        }
        return null;
    }

    public void a(Activity activity) {
        if (activity.isFinishing()) {
            com.a.c.a.a("Warning", "Possible memory leak. Calling ajax with a terminated activity.");
        }
        if (this.o == null) {
            com.a.c.a.a("Warning", "type() is not called with response type.");
            return;
        }
        this.E = new WeakReference<>(activity);
        a((Context) activity);
    }

    public void a(Context context) {
        if (this.f == null) {
            this.f = new c();
            this.f.c(this.t).a(this.z);
        } else if (this.f.d()) {
            this.f.b();
            this.d = null;
        }
        c(true);
        if (this.e == null || this.e.a()) {
            c(context);
            return;
        }
        com.a.c.a.b("auth needed", this.t);
        this.e.a(this);
    }

    private boolean j() {
        if (this.E == null) {
            return true;
        }
        Activity activity = this.E.get();
        return activity != null && !activity.isFinishing();
    }

    private void c(Context context) {
        T d2 = d(this.t);
        if (d2 != null) {
            this.d = d2;
            this.f.a(4).a();
            a();
            return;
        }
        this.x = com.a.c.a.a(context, this.w);
        a((Runnable) this);
    }

    /* access modifiers changed from: protected */
    public boolean b(Context context) {
        return this.g && com.a.c.a.b(com.a.c.a.a(context, this.w), this.t) != null;
    }

    public void run() {
        if (!this.f.d()) {
            try {
                k();
            } catch (Throwable th) {
                com.a.c.a.a(th);
                this.f.b(-101).a();
            }
            if (this.f.e()) {
                return;
            }
            if (this.H) {
                com.a.c.a.a((Runnable) this);
            } else {
                s();
            }
        } else {
            s();
        }
    }

    private void k() {
        if (!this.z && this.g) {
            m();
        }
        if (this.d == null) {
            n();
        }
        if (this.d == null) {
            o();
        }
    }

    private String l() {
        if (this.e != null) {
            return this.e.b(this.t);
        }
        return this.t;
    }

    private String g(String str) {
        if (this.u != null) {
            str = this.u;
        }
        if (this.e != null) {
            return this.e.a(str);
        }
        return str;
    }

    private void m() {
        File a2 = a(this.x, l());
        if (a2 != null) {
            this.f.a(3);
            this.d = a(this.t, a2, this.f);
            if (this.d != null) {
                this.f.a(new Date(a2.lastModified())).a();
            }
        }
    }

    private void n() {
        this.d = c(this.t);
        if (this.d != null) {
            this.f.a(2).a();
        }
    }

    private void o() {
        if (this.t == null) {
            this.f.b(-101).a();
            return;
        }
        byte[] bArr = null;
        try {
            b(this.I + 1);
            if (this.e != null && this.e.a(this, this.f) && !this.O) {
                com.a.c.a.b("reauth needed", this.f.h());
                this.O = true;
                if (this.e.b(this)) {
                    r();
                } else {
                    this.f.b(true);
                    return;
                }
            }
            bArr = this.f.i();
        } catch (IOException e2) {
            com.a.c.a.a((Object) "IOException");
            String message = e2.getMessage();
            if (message == null || !message.contains("No authentication challenges found")) {
                this.f.b(-101).b("network error");
            } else {
                this.f.b((int) ErrorCode.NetWorkError.QUEUE_FULL_ERROR).b(message);
            }
        } catch (Exception e3) {
            com.a.c.a.a((Throwable) e3);
            this.f.b(-101).b("network error");
        }
        try {
            this.d = a(this.t, bArr, this.f);
        } catch (Exception e4) {
            com.a.c.a.a((Throwable) e4);
        }
        if (this.d == null && bArr != null) {
            this.f.b(-103).b("transform error");
        }
        T = this.f.g();
        this.f.a();
    }

    /* access modifiers changed from: protected */
    public File b() {
        return com.a.c.a.a(this.x, l());
    }

    /* access modifiers changed from: protected */
    public boolean c() {
        return File.class.equals(this.o) || XmlPullParser.class.equals(this.o) || InputStream.class.equals(this.o) || g.class.equals(this.o);
    }

    private File p() {
        File file;
        if (!c()) {
            file = null;
        } else if (this.y != null) {
            file = this.y;
        } else if (this.g) {
            file = b();
        } else {
            File d2 = com.a.c.a.d();
            if (d2 == null) {
                d2 = this.x;
            }
            file = com.a.c.a.a(d2, this.t);
        }
        if (file == null || file.exists()) {
            return file;
        }
        try {
            file.getParentFile().mkdirs();
            file.createNewFile();
            return file;
        } catch (Exception e2) {
            com.a.c.a.b(e2);
            return null;
        }
    }

    private void q() {
        if (this.d != null && this.g) {
            byte[] i2 = this.f.i();
            if (i2 != null) {
                try {
                    if (this.f.k() == 1) {
                        File b2 = b();
                        if (!this.f.f()) {
                            a(this.t, this.d, b2, i2);
                        } else if (b2.exists()) {
                            b2.delete();
                        }
                    }
                } catch (Exception e2) {
                    com.a.c.a.a((Throwable) e2);
                }
            }
            this.f.a((byte[]) null);
        } else if (this.f.g() == -103) {
            File b3 = b();
            if (b3.exists()) {
                b3.delete();
                com.a.c.a.a((Object) "invalidated cache due to transform error");
            }
        }
    }

    private static String a(Uri uri) {
        String str = String.valueOf(uri.getScheme()) + "://" + uri.getAuthority() + uri.getPath();
        String fragment = uri.getFragment();
        if (fragment != null) {
            return String.valueOf(str) + "#" + fragment;
        }
        return str;
    }

    private static Map<String, Object> b(Uri uri) {
        HashMap hashMap = new HashMap();
        for (String split : uri.getQuery().split("&")) {
            String[] split2 = split.split("=");
            if (split2.length >= 2) {
                hashMap.put(split2[0], split2[1]);
            } else if (split2.length == 1) {
                hashMap.put(split2[0], "");
            }
        }
        return hashMap;
    }

    private void b(int i2) throws IOException {
        if (i2 <= 1) {
            r();
            return;
        }
        int i3 = 0;
        while (i3 < i2) {
            try {
                r();
                return;
            } catch (IOException e2) {
                if (i3 == i2 - 1) {
                    throw e2;
                }
                i3++;
            }
        }
    }

    private void r() throws IOException {
        String str = this.t;
        Map<String, Object> map = this.f119a;
        if (map == null && str.length() > 2000) {
            Uri parse = Uri.parse(str);
            str = a(parse);
            map = b(parse);
        }
        String g2 = g(str);
        if (2 == this.F) {
            b(g2, this.f);
        } else if (3 == this.F) {
            b(g2, map, this.f);
        } else {
            if (1 == this.F && map == null) {
                map = new HashMap<>();
            }
            if (map == null) {
                a(g2, this.f);
            } else if (a(map)) {
                c(g2, map, this.f);
            } else {
                a(g2, map, this.f);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.b.a.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, T]
     candidates:
      com.a.b.a.a(java.lang.String, com.a.b.c):void
      com.a.b.a.a(java.lang.String, java.io.InputStream):byte[]
      com.a.b.a.a(java.io.File, java.lang.String):java.io.File
      com.a.b.a.a(java.lang.String, int):K
      com.a.b.a.a(java.lang.String, java.lang.Object):void */
    private void s() {
        if (this.t != null && this.h) {
            a(this.t, (Object) this.d);
        }
        a();
        h();
    }

    public static void a(Runnable runnable) {
        if (P == null) {
            P = Executors.newFixedThreadPool(k);
        }
        P.execute(runnable);
    }

    private static String h(String str) {
        return str.replaceAll(" ", "%20").replaceAll("\\|", "%7C");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.b.a.a(org.apache.http.client.methods.HttpUriRequest, java.lang.String, com.a.b.c):void
     arg types: [org.apache.http.client.methods.HttpGet, java.lang.String, com.a.b.c]
     candidates:
      com.a.b.a.a(byte[], java.lang.String, com.a.b.c):java.lang.String
      com.a.b.a.a(org.apache.http.client.methods.HttpUriRequest, org.apache.http.impl.client.DefaultHttpClient, org.apache.http.protocol.HttpContext):org.apache.http.HttpResponse
      com.a.b.a.a(java.io.DataOutputStream, java.lang.String, java.lang.Object):void
      com.a.b.a.a(java.io.DataOutputStream, java.lang.String, java.lang.String):void
      com.a.b.a.a(java.io.InputStream, java.io.OutputStream, int):void
      com.a.b.a.a(java.lang.String, java.util.Map<java.lang.String, java.lang.Object>, com.a.b.c):void
      com.a.b.a.a(java.lang.String, java.io.File, com.a.b.c):T
      com.a.b.a.a(java.lang.String, byte[], com.a.b.c):T
      com.a.b.a.a(java.lang.String, java.lang.Object, com.a.b.c):void
      com.a.b.a.a(org.apache.http.client.methods.HttpUriRequest, java.lang.String, com.a.b.c):void */
    private void a(String str, c cVar) throws IOException {
        com.a.c.a.b("get", str);
        String h2 = h(str);
        a((HttpUriRequest) new HttpGet(h2), h2, cVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.b.a.a(org.apache.http.client.methods.HttpUriRequest, java.lang.String, com.a.b.c):void
     arg types: [org.apache.http.client.methods.HttpDelete, java.lang.String, com.a.b.c]
     candidates:
      com.a.b.a.a(byte[], java.lang.String, com.a.b.c):java.lang.String
      com.a.b.a.a(org.apache.http.client.methods.HttpUriRequest, org.apache.http.impl.client.DefaultHttpClient, org.apache.http.protocol.HttpContext):org.apache.http.HttpResponse
      com.a.b.a.a(java.io.DataOutputStream, java.lang.String, java.lang.Object):void
      com.a.b.a.a(java.io.DataOutputStream, java.lang.String, java.lang.String):void
      com.a.b.a.a(java.io.InputStream, java.io.OutputStream, int):void
      com.a.b.a.a(java.lang.String, java.util.Map<java.lang.String, java.lang.Object>, com.a.b.c):void
      com.a.b.a.a(java.lang.String, java.io.File, com.a.b.c):T
      com.a.b.a.a(java.lang.String, byte[], com.a.b.c):T
      com.a.b.a.a(java.lang.String, java.lang.Object, com.a.b.c):void
      com.a.b.a.a(org.apache.http.client.methods.HttpUriRequest, java.lang.String, com.a.b.c):void */
    private void b(String str, c cVar) throws IOException {
        com.a.c.a.b("get", str);
        String h2 = h(str);
        a((HttpUriRequest) new HttpDelete(h2), h2, cVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.b.a.a(java.lang.String, org.apache.http.client.methods.HttpEntityEnclosingRequestBase, java.util.Map<java.lang.String, java.lang.Object>, com.a.b.c):void
     arg types: [java.lang.String, org.apache.http.client.methods.HttpPost, java.util.Map<java.lang.String, java.lang.Object>, com.a.b.c]
     candidates:
      com.a.b.a.a(java.io.DataOutputStream, java.lang.String, java.lang.String, java.io.InputStream):void
      com.a.b.a.a(java.lang.String, java.lang.Object, java.io.File, byte[]):void
      com.a.b.a.a(java.lang.String, org.apache.http.client.methods.HttpEntityEnclosingRequestBase, java.util.Map<java.lang.String, java.lang.Object>, com.a.b.c):void */
    private void a(String str, Map<String, Object> map, c cVar) throws ClientProtocolException, IOException {
        com.a.c.a.b("post", str);
        a(str, (HttpEntityEnclosingRequestBase) new HttpPost(str), map, cVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.b.a.a(java.lang.String, org.apache.http.client.methods.HttpEntityEnclosingRequestBase, java.util.Map<java.lang.String, java.lang.Object>, com.a.b.c):void
     arg types: [java.lang.String, org.apache.http.client.methods.HttpPut, java.util.Map<java.lang.String, java.lang.Object>, com.a.b.c]
     candidates:
      com.a.b.a.a(java.io.DataOutputStream, java.lang.String, java.lang.String, java.io.InputStream):void
      com.a.b.a.a(java.lang.String, java.lang.Object, java.io.File, byte[]):void
      com.a.b.a.a(java.lang.String, org.apache.http.client.methods.HttpEntityEnclosingRequestBase, java.util.Map<java.lang.String, java.lang.Object>, com.a.b.c):void */
    private void b(String str, Map<String, Object> map, c cVar) throws ClientProtocolException, IOException {
        com.a.c.a.b("put", str);
        a(str, (HttpEntityEnclosingRequestBase) new HttpPut(str), map, cVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.b.a.a(org.apache.http.client.methods.HttpUriRequest, java.lang.String, com.a.b.c):void
     arg types: [org.apache.http.client.methods.HttpEntityEnclosingRequestBase, java.lang.String, com.a.b.c]
     candidates:
      com.a.b.a.a(byte[], java.lang.String, com.a.b.c):java.lang.String
      com.a.b.a.a(org.apache.http.client.methods.HttpUriRequest, org.apache.http.impl.client.DefaultHttpClient, org.apache.http.protocol.HttpContext):org.apache.http.HttpResponse
      com.a.b.a.a(java.io.DataOutputStream, java.lang.String, java.lang.Object):void
      com.a.b.a.a(java.io.DataOutputStream, java.lang.String, java.lang.String):void
      com.a.b.a.a(java.io.InputStream, java.io.OutputStream, int):void
      com.a.b.a.a(java.lang.String, java.util.Map<java.lang.String, java.lang.Object>, com.a.b.c):void
      com.a.b.a.a(java.lang.String, java.io.File, com.a.b.c):T
      com.a.b.a.a(java.lang.String, byte[], com.a.b.c):T
      com.a.b.a.a(java.lang.String, java.lang.Object, com.a.b.c):void
      com.a.b.a.a(org.apache.http.client.methods.HttpUriRequest, java.lang.String, com.a.b.c):void */
    private void a(String str, HttpEntityEnclosingRequestBase httpEntityEnclosingRequestBase, Map<String, Object> map, c cVar) throws ClientProtocolException, IOException {
        HttpEntity urlEncodedFormEntity;
        httpEntityEnclosingRequestBase.getParams().setBooleanParameter("http.protocol.expect-continue", false);
        Object obj = map.get("%entity");
        if (obj instanceof HttpEntity) {
            urlEncodedFormEntity = (HttpEntity) obj;
        } else {
            ArrayList arrayList = new ArrayList();
            for (Map.Entry next : map.entrySet()) {
                Object value = next.getValue();
                if (value != null) {
                    arrayList.add(new BasicNameValuePair((String) next.getKey(), value.toString()));
                }
            }
            urlEncodedFormEntity = new UrlEncodedFormEntity(arrayList, "UTF-8");
        }
        if (this.b != null && !this.b.containsKey("Content-Type")) {
            this.b.put("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
        }
        httpEntityEnclosingRequestBase.setEntity(urlEncodedFormEntity);
        a((HttpUriRequest) httpEntityEnclosingRequestBase, str, cVar);
    }

    private static DefaultHttpClient t() {
        if (R == null || !m) {
            com.a.c.a.a((Object) "creating http client");
            BasicHttpParams basicHttpParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(basicHttpParams, i);
            HttpConnectionParams.setSoTimeout(basicHttpParams, i);
            ConnManagerParams.setMaxConnectionsPerRoute(basicHttpParams, new ConnPerRouteBean(25));
            HttpConnectionParams.setSocketBufferSize(basicHttpParams, 8192);
            SchemeRegistry schemeRegistry = new SchemeRegistry();
            schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            schemeRegistry.register(new Scheme("https", Q == null ? SSLSocketFactory.getSocketFactory() : Q, 443));
            R = new DefaultHttpClient(new ThreadSafeClientConnManager(basicHttpParams, schemeRegistry), basicHttpParams);
        }
        return R;
    }

    private HttpResponse a(HttpUriRequest httpUriRequest, DefaultHttpClient defaultHttpClient, HttpContext httpContext) throws ClientProtocolException, IOException {
        HttpHost httpHost;
        if (!httpUriRequest.getURI().getAuthority().contains("_")) {
            return defaultHttpClient.execute(httpUriRequest, httpContext);
        }
        URL url = httpUriRequest.getURI().toURL();
        if (url.getPort() == -1) {
            httpHost = new HttpHost(url.getHost(), 80, url.getProtocol());
        } else {
            httpHost = new HttpHost(url.getHost(), url.getPort(), url.getProtocol());
        }
        return defaultHttpClient.execute(httpHost, httpUriRequest, httpContext);
    }

    /* JADX INFO: finally extract failed */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.a.a.a(com.a.b.a<?, ?>, org.apache.http.HttpRequest):void
     arg types: [com.a.b.a, org.apache.http.client.methods.HttpUriRequest]
     candidates:
      com.a.a.a.a(com.a.b.a<?, ?>, java.net.HttpURLConnection):void
      com.a.a.a.a(com.a.b.a<?, ?>, com.a.b.c):boolean
      com.a.a.a.a(com.a.b.a<?, ?>, org.apache.http.HttpRequest):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.b.a.a(org.apache.http.client.methods.HttpUriRequest, org.apache.http.impl.client.DefaultHttpClient, org.apache.http.protocol.HttpContext):org.apache.http.HttpResponse
     arg types: [org.apache.http.client.methods.HttpUriRequest, org.apache.http.impl.client.DefaultHttpClient, org.apache.http.protocol.BasicHttpContext]
     candidates:
      com.a.b.a.a(byte[], java.lang.String, com.a.b.c):java.lang.String
      com.a.b.a.a(java.io.DataOutputStream, java.lang.String, java.lang.Object):void
      com.a.b.a.a(java.io.DataOutputStream, java.lang.String, java.lang.String):void
      com.a.b.a.a(java.io.InputStream, java.io.OutputStream, int):void
      com.a.b.a.a(java.lang.String, java.util.Map<java.lang.String, java.lang.Object>, com.a.b.c):void
      com.a.b.a.a(org.apache.http.client.methods.HttpUriRequest, java.lang.String, com.a.b.c):void
      com.a.b.a.a(java.lang.String, java.io.File, com.a.b.c):T
      com.a.b.a.a(java.lang.String, byte[], com.a.b.c):T
      com.a.b.a.a(java.lang.String, java.lang.Object, com.a.b.c):void
      com.a.b.a.a(org.apache.http.client.methods.HttpUriRequest, org.apache.http.impl.client.DefaultHttpClient, org.apache.http.protocol.HttpContext):org.apache.http.HttpResponse */
    /* JADX WARNING: Code restructure failed: missing block: B:109:0x028f, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:110:0x0290, code lost:
        r20 = r3;
        r3 = r2;
        r2 = r20;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:112:0x029a, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:113:0x029b, code lost:
        r20 = r3;
        r3 = r2;
        r2 = r20;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:109:0x028f A[ExcHandler: all (r3v28 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:60:0x013d] */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x0164  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(org.apache.http.client.methods.HttpUriRequest r22, java.lang.String r23, com.a.b.c r24) throws org.apache.http.client.ClientProtocolException, java.io.IOException {
        /*
            r21 = this;
            org.apache.http.impl.client.DefaultHttpClient r12 = t()
            com.a.b.e r2 = com.a.b.a.S
            if (r2 == 0) goto L_0x0011
            com.a.b.e r2 = com.a.b.a.S
            r0 = r21
            r1 = r22
            r2.a(r0, r1, r12)
        L_0x0011:
            java.lang.String r2 = com.a.b.a.j
            if (r2 == 0) goto L_0x00d5
            java.lang.String r2 = "User-Agent"
            java.lang.String r3 = com.a.b.a.j
            r0 = r22
            r0.addHeader(r2, r3)
        L_0x001e:
            r0 = r21
            java.util.Map<java.lang.String, java.lang.String> r2 = r0.b
            if (r2 == 0) goto L_0x0036
            r0 = r21
            java.util.Map<java.lang.String, java.lang.String> r2 = r0.b
            java.util.Set r2 = r2.keySet()
            java.util.Iterator r4 = r2.iterator()
        L_0x0030:
            boolean r2 = r4.hasNext()
            if (r2 != 0) goto L_0x00e8
        L_0x0036:
            boolean r2 = com.a.b.a.l
            if (r2 == 0) goto L_0x0055
            r0 = r21
            java.util.Map<java.lang.String, java.lang.String> r2 = r0.b
            if (r2 == 0) goto L_0x004c
            r0 = r21
            java.util.Map<java.lang.String, java.lang.String> r2 = r0.b
            java.lang.String r3 = "Accept-Encoding"
            boolean r2 = r2.containsKey(r3)
            if (r2 != 0) goto L_0x0055
        L_0x004c:
            java.lang.String r2 = "Accept-Encoding"
            java.lang.String r3 = "gzip"
            r0 = r22
            r0.addHeader(r2, r3)
        L_0x0055:
            r0 = r21
            com.a.a.a r2 = r0.e
            if (r2 == 0) goto L_0x0066
            r0 = r21
            com.a.a.a r2 = r0.e
            r0 = r21
            r1 = r22
            r2.a(r0, r1)
        L_0x0066:
            java.lang.String r2 = r21.u()
            if (r2 == 0) goto L_0x0073
            java.lang.String r3 = "Cookie"
            r0 = r22
            r0.addHeader(r3, r2)
        L_0x0073:
            org.apache.http.params.HttpParams r2 = r22.getParams()
            r0 = r21
            org.apache.http.HttpHost r3 = r0.K
            if (r3 == 0) goto L_0x0086
            java.lang.String r3 = "http.route.default-proxy"
            r0 = r21
            org.apache.http.HttpHost r4 = r0.K
            r2.setParameter(r3, r4)
        L_0x0086:
            r0 = r21
            int r3 = r0.A
            if (r3 <= 0) goto L_0x00a6
            java.lang.String r3 = "http.connection.timeout"
            r0 = r21
            int r4 = r0.A
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)
            r2.setParameter(r3, r4)
            java.lang.String r3 = "http.socket.timeout"
            r0 = r21
            int r4 = r0.A
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)
            r2.setParameter(r3, r4)
        L_0x00a6:
            r0 = r21
            boolean r3 = r0.B
            if (r3 != 0) goto L_0x00b2
            java.lang.String r3 = "http.protocol.handle-redirects"
            r4 = 0
            r2.setBooleanParameter(r3, r4)
        L_0x00b2:
            org.apache.http.protocol.BasicHttpContext r13 = new org.apache.http.protocol.BasicHttpContext
            r13.<init>()
            org.apache.http.impl.client.BasicCookieStore r3 = new org.apache.http.impl.client.BasicCookieStore
            r3.<init>()
            java.lang.String r4 = "http.cookie-store"
            r13.setAttribute(r4, r3)
            r0 = r22
            r1 = r21
            r1.G = r0
            r0 = r21
            boolean r3 = r0.U
            if (r3 == 0) goto L_0x00ff
            java.io.IOException r2 = new java.io.IOException
            java.lang.String r3 = "Aborted"
            r2.<init>(r3)
            throw r2
        L_0x00d5:
            java.lang.String r2 = com.a.b.a.j
            if (r2 != 0) goto L_0x001e
            boolean r2 = com.a.b.a.l
            if (r2 == 0) goto L_0x001e
            java.lang.String r2 = "User-Agent"
            java.lang.String r3 = "gzip"
            r0 = r22
            r0.addHeader(r2, r3)
            goto L_0x001e
        L_0x00e8:
            java.lang.Object r2 = r4.next()
            java.lang.String r2 = (java.lang.String) r2
            r0 = r21
            java.util.Map<java.lang.String, java.lang.String> r3 = r0.b
            java.lang.Object r3 = r3.get(r2)
            java.lang.String r3 = (java.lang.String) r3
            r0 = r22
            r0.addHeader(r2, r3)
            goto L_0x0030
        L_0x00ff:
            boolean r3 = com.a.b.a.n
            if (r3 == 0) goto L_0x010b
            java.io.IOException r2 = new java.io.IOException
            java.lang.String r3 = "Simulated Error"
            r2.<init>(r3)
            throw r2
        L_0x010b:
            r0 = r21
            r1 = r22
            org.apache.http.HttpResponse r2 = r0.a(r1, r12, r13)     // Catch:{ HttpHostConnectException -> 0x01a1 }
            r8 = r2
        L_0x0114:
            r10 = 0
            org.apache.http.StatusLine r2 = r8.getStatusLine()
            int r14 = r2.getStatusCode()
            org.apache.http.StatusLine r2 = r8.getStatusLine()
            java.lang.String r15 = r2.getReasonPhrase()
            r9 = 0
            org.apache.http.HttpEntity r5 = r8.getEntity()
            r7 = 0
            r6 = 0
            r2 = 200(0xc8, float:2.8E-43)
            if (r14 < r2) goto L_0x0134
            r2 = 300(0x12c, float:4.2E-43)
            if (r14 < r2) goto L_0x01d8
        L_0x0134:
            r2 = 0
            if (r5 == 0) goto L_0x02aa
            java.io.InputStream r2 = r5.getContent()     // Catch:{ Exception -> 0x01bf, all -> 0x01ce }
            r0 = r21
            java.lang.String r3 = r0.a(r5)     // Catch:{ Exception -> 0x029a, all -> 0x028f }
            r0 = r21
            byte[] r4 = r0.a(r3, r2)     // Catch:{ Exception -> 0x029a, all -> 0x028f }
            java.lang.String r3 = new java.lang.String     // Catch:{ Exception -> 0x029a, all -> 0x028f }
            java.lang.String r5 = "UTF-8"
            r3.<init>(r4, r5)     // Catch:{ Exception -> 0x029a, all -> 0x028f }
            java.lang.String r4 = "error"
            com.a.c.a.b(r4, r3)     // Catch:{ Exception -> 0x02a2, all -> 0x028f }
        L_0x0153:
            com.a.c.a.a(r2)
            r9 = r3
            r2 = r23
        L_0x0159:
            java.lang.String r3 = "response"
            java.lang.Integer r4 = java.lang.Integer.valueOf(r14)
            com.a.c.a.b(r3, r4)
            if (r10 == 0) goto L_0x016e
            int r3 = r10.length
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            r0 = r23
            com.a.c.a.b(r3, r0)
        L_0x016e:
            r0 = r24
            com.a.b.c r3 = r0.b(r14)
            com.a.b.c r3 = r3.b(r15)
            com.a.b.c r3 = r3.a(r9)
            com.a.b.c r2 = r3.c(r2)
            java.util.Date r3 = new java.util.Date
            r3.<init>()
            com.a.b.c r2 = r2.a(r3)
            com.a.b.c r2 = r2.a(r10)
            com.a.b.c r2 = r2.a(r7)
            com.a.b.c r2 = r2.a(r12)
            com.a.b.c r2 = r2.a(r13)
            org.apache.http.Header[] r3 = r8.getAllHeaders()
            r2.a(r3)
            return
        L_0x01a1:
            r3 = move-exception
            r0 = r21
            org.apache.http.HttpHost r4 = r0.K
            if (r4 == 0) goto L_0x01be
            java.lang.String r3 = "proxy failed, retrying without proxy"
            com.a.c.a.a(r3)
            java.lang.String r3 = "http.route.default-proxy"
            r4 = 0
            r2.setParameter(r3, r4)
            r0 = r21
            r1 = r22
            org.apache.http.HttpResponse r2 = r0.a(r1, r12, r13)
            r8 = r2
            goto L_0x0114
        L_0x01be:
            throw r3
        L_0x01bf:
            r3 = move-exception
            r20 = r3
            r3 = r2
            r2 = r20
        L_0x01c5:
            com.a.c.a.a(r2)     // Catch:{ all -> 0x0297 }
            com.a.c.a.a(r3)
            r2 = r23
            goto L_0x0159
        L_0x01ce:
            r3 = move-exception
            r20 = r3
            r3 = r2
            r2 = r20
        L_0x01d4:
            com.a.c.a.a(r3)
            throw r2
        L_0x01d8:
            java.lang.String r2 = "http.target_host"
            java.lang.Object r2 = r13.getAttribute(r2)
            org.apache.http.HttpHost r2 = (org.apache.http.HttpHost) r2
            java.lang.String r3 = "http.request"
            java.lang.Object r3 = r13.getAttribute(r3)
            org.apache.http.client.methods.HttpUriRequest r3 = (org.apache.http.client.methods.HttpUriRequest) r3
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r2 = r2.toURI()
            java.lang.String r2 = java.lang.String.valueOf(r2)
            r4.<init>(r2)
            java.net.URI r2 = r3.getURI()
            java.lang.StringBuilder r2 = r4.append(r2)
            java.lang.String r11 = r2.toString()
            r2 = 32
            r3 = 65536(0x10000, float:9.18355E-41)
            long r16 = r5.getContentLength()
            r0 = r16
            int r4 = (int) r0
            int r3 = java.lang.Math.min(r3, r4)
            int r16 = java.lang.Math.max(r2, r3)
            r4 = 0
            r3 = 0
            java.io.File r7 = r21.p()     // Catch:{ all -> 0x0287 }
            if (r7 != 0) goto L_0x025e
            com.a.c.d r2 = new com.a.c.d     // Catch:{ all -> 0x0287 }
            r0 = r16
            r2.<init>(r0)     // Catch:{ all -> 0x0287 }
            r4 = r2
        L_0x0224:
            java.io.InputStream r3 = r5.getContent()     // Catch:{ all -> 0x0287 }
            java.lang.String r2 = "gzip"
            r0 = r21
            java.lang.String r16 = r0.a(r5)     // Catch:{ all -> 0x0287 }
            r0 = r16
            boolean r2 = r2.equalsIgnoreCase(r0)     // Catch:{ all -> 0x0287 }
            if (r2 == 0) goto L_0x023e
            java.util.zip.GZIPInputStream r2 = new java.util.zip.GZIPInputStream     // Catch:{ all -> 0x0287 }
            r2.<init>(r3)     // Catch:{ all -> 0x0287 }
            r3 = r2
        L_0x023e:
            long r16 = r5.getContentLength()     // Catch:{ all -> 0x0287 }
            r0 = r16
            int r5 = (int) r0     // Catch:{ all -> 0x0287 }
            r2 = r21
            r2.a(r3, r4, r5, r6, r7)     // Catch:{ all -> 0x0287 }
            if (r7 != 0) goto L_0x0274
            r0 = r4
            com.a.c.d r0 = (com.a.c.d) r0     // Catch:{ all -> 0x0287 }
            r2 = r0
            byte[] r2 = r2.toByteArray()     // Catch:{ all -> 0x0287 }
        L_0x0254:
            com.a.c.a.a(r3)
            com.a.c.a.a(r4)
            r10 = r2
            r2 = r11
            goto L_0x0159
        L_0x025e:
            r0 = r21
            java.io.File r6 = r0.a(r7)     // Catch:{ all -> 0x0287 }
            java.io.BufferedOutputStream r2 = new java.io.BufferedOutputStream     // Catch:{ all -> 0x0287 }
            java.io.FileOutputStream r16 = new java.io.FileOutputStream     // Catch:{ all -> 0x0287 }
            r0 = r16
            r0.<init>(r6)     // Catch:{ all -> 0x0287 }
            r0 = r16
            r2.<init>(r0)     // Catch:{ all -> 0x0287 }
            r4 = r2
            goto L_0x0224
        L_0x0274:
            boolean r2 = r7.exists()     // Catch:{ all -> 0x0287 }
            if (r2 == 0) goto L_0x0284
            long r16 = r7.length()     // Catch:{ all -> 0x0287 }
            r18 = 0
            int r2 = (r16 > r18 ? 1 : (r16 == r18 ? 0 : -1))
            if (r2 != 0) goto L_0x02a8
        L_0x0284:
            r7 = 0
            r2 = r10
            goto L_0x0254
        L_0x0287:
            r2 = move-exception
            com.a.c.a.a(r3)
            com.a.c.a.a(r4)
            throw r2
        L_0x028f:
            r3 = move-exception
            r20 = r3
            r3 = r2
            r2 = r20
            goto L_0x01d4
        L_0x0297:
            r2 = move-exception
            goto L_0x01d4
        L_0x029a:
            r3 = move-exception
            r20 = r3
            r3 = r2
            r2 = r20
            goto L_0x01c5
        L_0x02a2:
            r4 = move-exception
            r9 = r3
            r3 = r2
            r2 = r4
            goto L_0x01c5
        L_0x02a8:
            r2 = r10
            goto L_0x0254
        L_0x02aa:
            r3 = r9
            goto L_0x0153
        */
        throw new UnsupportedOperationException("Method not decompiled: com.a.b.a.a(org.apache.http.client.methods.HttpUriRequest, java.lang.String, com.a.b.c):void");
    }

    private String a(HttpEntity httpEntity) {
        Header contentEncoding;
        if (httpEntity == null || (contentEncoding = httpEntity.getContentEncoding()) == null) {
            return null;
        }
        return contentEncoding.getValue();
    }

    private void a(InputStream inputStream, OutputStream outputStream, int i2, File file, File file2) throws IOException {
        if (file2 == null) {
            a(inputStream, outputStream, i2);
            return;
        }
        try {
            a(inputStream, outputStream, i2);
            inputStream.close();
            outputStream.close();
            file.renameTo(file2);
        } catch (IOException e2) {
            com.a.c.a.a((Object) "copy failed, deleting files");
            file.delete();
            file2.delete();
            com.a.c.a.a((Closeable) inputStream);
            com.a.c.a.a((Closeable) outputStream);
            throw e2;
        }
    }

    private File a(File file) throws IOException {
        File file2 = new File(String.valueOf(file.getAbsolutePath()) + ".tmp");
        file2.createNewFile();
        return file2;
    }

    private void a(InputStream inputStream, OutputStream outputStream, int i2) throws IOException {
        Object obj;
        e eVar = null;
        if (this.s != null) {
            obj = this.s.get();
        } else {
            obj = null;
        }
        if (obj != null) {
            eVar = new e(obj);
        }
        com.a.c.a.a(inputStream, outputStream, i2, eVar);
    }

    public K a(com.a.a.a aVar) {
        this.e = aVar;
        return g();
    }

    public String d() {
        return this.t;
    }

    public Object e() {
        if (this.q != null) {
            return this.q;
        }
        if (this.p == null) {
            return null;
        }
        return this.p.get();
    }

    protected static int f() {
        return T;
    }

    /* JADX WARNING: Removed duplicated region for block: B:4:0x0010  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean a(java.util.Map<java.lang.String, java.lang.Object> r3) {
        /*
            java.util.Set r0 = r3.entrySet()
            java.util.Iterator r1 = r0.iterator()
        L_0x0008:
            boolean r0 = r1.hasNext()
            if (r0 != 0) goto L_0x0010
            r0 = 0
        L_0x000f:
            return r0
        L_0x0010:
            java.lang.Object r0 = r1.next()
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0
            java.lang.Object r2 = r0.getValue()
            java.lang.Object r0 = r0.getKey()
            com.a.c.a.b(r0, r2)
            boolean r0 = r2 instanceof java.io.File
            if (r0 != 0) goto L_0x002d
            boolean r0 = r2 instanceof byte[]
            if (r0 != 0) goto L_0x002d
            boolean r0 = r2 instanceof java.io.InputStream
            if (r0 == 0) goto L_0x0008
        L_0x002d:
            r0 = 1
            goto L_0x000f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.a.b.a.a(java.util.Map):boolean");
    }

    private void c(String str, Map<String, Object> map, c cVar) throws IOException {
        Proxy proxy;
        HttpURLConnection httpURLConnection;
        byte[] bArr;
        String str2;
        com.a.c.a.b("multipart", str);
        URL url = new URL(str);
        if (this.K != null) {
            com.a.c.a.b("proxy", this.K);
            proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(this.K.getHostName(), this.K.getPort()));
        } else if (S != null) {
            proxy = S.a(this);
        } else {
            proxy = null;
        }
        if (proxy == null) {
            httpURLConnection = (HttpURLConnection) url.openConnection();
        } else {
            httpURLConnection = (HttpURLConnection) url.openConnection(proxy);
        }
        httpURLConnection.setInstanceFollowRedirects(false);
        httpURLConnection.setConnectTimeout(i * 4);
        httpURLConnection.setDoInput(true);
        httpURLConnection.setDoOutput(true);
        httpURLConnection.setUseCaches(false);
        httpURLConnection.setRequestMethod("POST");
        httpURLConnection.setRequestProperty("Connection", "Keep-Alive");
        httpURLConnection.setRequestProperty("Content-Type", "multipart/form-data;charset=utf-8;boundary=*****");
        if (this.b != null) {
            for (String next : this.b.keySet()) {
                httpURLConnection.setRequestProperty(next, this.b.get(next));
            }
        }
        String u2 = u();
        if (u2 != null) {
            httpURLConnection.setRequestProperty("Cookie", u2);
        }
        if (this.e != null) {
            this.e.a(this, httpURLConnection);
        }
        DataOutputStream dataOutputStream = new DataOutputStream(httpURLConnection.getOutputStream());
        for (Map.Entry next2 : map.entrySet()) {
            a(dataOutputStream, (String) next2.getKey(), next2.getValue());
        }
        dataOutputStream.writeBytes("--*****--\r\n");
        dataOutputStream.flush();
        dataOutputStream.close();
        httpURLConnection.connect();
        int responseCode = httpURLConnection.getResponseCode();
        String responseMessage = httpURLConnection.getResponseMessage();
        String contentEncoding = httpURLConnection.getContentEncoding();
        if (responseCode < 200 || responseCode >= 300) {
            str2 = new String(a(contentEncoding, httpURLConnection.getErrorStream()), "UTF-8");
            com.a.c.a.b(SocketMessage.MSG_ERROR_KEY, str2);
            bArr = null;
        } else {
            bArr = a(contentEncoding, httpURLConnection.getInputStream());
            str2 = null;
        }
        com.a.c.a.b("response", Integer.valueOf(responseCode));
        if (bArr != null) {
            com.a.c.a.b(Integer.valueOf(bArr.length), str);
        }
        cVar.b(responseCode).b(responseMessage).c(str).a(new Date()).a(bArr).a(str2).a((DefaultHttpClient) null);
    }

    private byte[] a(String str, InputStream inputStream) throws IOException {
        if ("gzip".equalsIgnoreCase(str)) {
            inputStream = new GZIPInputStream(inputStream);
        }
        return com.a.c.a.a(inputStream);
    }

    private static void a(DataOutputStream dataOutputStream, String str, Object obj) throws IOException {
        if (obj != null) {
            if (obj instanceof File) {
                File file = (File) obj;
                a(dataOutputStream, str, file.getName(), new FileInputStream(file));
            } else if (obj instanceof byte[]) {
                a(dataOutputStream, str, str, new ByteArrayInputStream((byte[]) obj));
            } else if (obj instanceof InputStream) {
                a(dataOutputStream, str, str, (InputStream) obj);
            } else {
                a(dataOutputStream, str, obj.toString());
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.c.a.a(java.io.InputStream, java.io.OutputStream):void
     arg types: [java.io.InputStream, java.io.DataOutputStream]
     candidates:
      com.a.c.a.a(android.content.Context, int):java.io.File
      com.a.c.a.a(java.io.File, java.lang.String):java.io.File
      com.a.c.a.a(java.io.File, byte[]):void
      com.a.c.a.a(java.lang.Object, java.lang.Object):void
      com.a.c.a.a(java.io.File[], long):boolean
      com.a.c.a.a(java.io.InputStream, java.io.OutputStream):void */
    private static void a(DataOutputStream dataOutputStream, String str, String str2, InputStream inputStream) throws IOException {
        dataOutputStream.writeBytes("--*****\r\n");
        dataOutputStream.writeBytes("Content-Disposition: form-data; name=\"" + str + "\";" + " filename=\"" + str2 + "\"" + HttpProxyConstants.CRLF);
        dataOutputStream.writeBytes("Content-Type: application/octet-stream");
        dataOutputStream.writeBytes(HttpProxyConstants.CRLF);
        dataOutputStream.writeBytes("Content-Transfer-Encoding: binary");
        dataOutputStream.writeBytes(HttpProxyConstants.CRLF);
        dataOutputStream.writeBytes(HttpProxyConstants.CRLF);
        com.a.c.a.a(inputStream, (OutputStream) dataOutputStream);
        dataOutputStream.writeBytes(HttpProxyConstants.CRLF);
    }

    private static void a(DataOutputStream dataOutputStream, String str, String str2) throws IOException {
        dataOutputStream.writeBytes("--*****\r\n");
        dataOutputStream.writeBytes("Content-Disposition: form-data; name=\"" + str + "\"");
        dataOutputStream.writeBytes(HttpProxyConstants.CRLF);
        dataOutputStream.writeBytes(HttpProxyConstants.CRLF);
        dataOutputStream.write(str2.getBytes("UTF-8"));
        dataOutputStream.writeBytes(HttpProxyConstants.CRLF);
    }

    private String u() {
        if (this.c == null || this.c.size() == 0) {
            return null;
        }
        Iterator<String> it = this.c.keySet().iterator();
        StringBuilder sb = new StringBuilder();
        while (it.hasNext()) {
            String next = it.next();
            sb.append(next);
            sb.append("=");
            sb.append(this.c.get(next));
            if (it.hasNext()) {
                sb.append("; ");
            }
        }
        return sb.toString();
    }
}
