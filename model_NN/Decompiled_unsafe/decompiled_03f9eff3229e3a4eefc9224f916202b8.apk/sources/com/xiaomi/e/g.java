package com.xiaomi.e;

import com.xiaomi.a.a.c.c;
import com.xiaomi.e.c;
import com.xiaomi.f.a.u;
import com.xiaomi.push.c.b;
import com.xiaomi.push.service.XMPushService;
import com.xiaomi.push.service.ao;
import java.util.Hashtable;

public class g {

    /* renamed from: a  reason: collision with root package name */
    private static final int f2408a = com.xiaomi.push.c.a.PING_RTT.a();

    static class a {

        /* renamed from: a  reason: collision with root package name */
        static Hashtable<Integer, Long> f2409a = new Hashtable<>();
    }

    public static void a() {
        a(0, f2408a);
    }

    public static void a(int i) {
        b f = e.a().f();
        f.a(com.xiaomi.push.c.a.CHANNEL_STATS_COUNTER.a());
        f.c(i);
        e.a().a(f);
    }

    public static synchronized void a(int i, int i2) {
        synchronized (g.class) {
            if (i2 < 16777215) {
                a.f2409a.put(Integer.valueOf((i << 24) | i2), Long.valueOf(System.currentTimeMillis()));
            } else {
                c.d("stats key should less than 16777215");
            }
        }
    }

    public static void a(int i, int i2, int i3, String str, int i4) {
        b f = e.a().f();
        f.a((byte) i);
        f.a(i2);
        f.b(i3);
        f.b(str);
        f.c(i4);
        e.a().a(f);
    }

    public static synchronized void a(int i, int i2, String str, int i3) {
        synchronized (g.class) {
            long currentTimeMillis = System.currentTimeMillis();
            int i4 = (i << 24) | i2;
            if (a.f2409a.containsKey(Integer.valueOf(i4))) {
                b f = e.a().f();
                f.a(i2);
                f.b((int) (currentTimeMillis - a.f2409a.get(Integer.valueOf(i4)).longValue()));
                f.b(str);
                if (i3 > -1) {
                    f.c(i3);
                }
                e.a().a(f);
                a.f2409a.remove(Integer.valueOf(i2));
            } else {
                c.d("stats key not found");
            }
        }
    }

    public static void a(XMPushService xMPushService, ao.b bVar) {
        new a(xMPushService, bVar).a();
    }

    public static void a(String str, int i, Exception exc) {
        b f = e.a().f();
        if (i > 0) {
            f.a(com.xiaomi.push.c.a.GSLB_REQUEST_SUCCESS.a());
            f.b(str);
            f.b(i);
            e.a().a(f);
            return;
        }
        try {
            c.a a2 = c.a(exc);
            f.a(a2.f2403a.a());
            f.c(a2.b);
            f.b(str);
            e.a().a(f);
        } catch (NullPointerException e) {
        }
    }

    public static void a(String str, Exception exc) {
        try {
            c.a b = c.b(exc);
            b f = e.a().f();
            f.a(b.f2403a.a());
            f.c(b.b);
            f.b(str);
            e.a().a(f);
        } catch (NullPointerException e) {
        }
    }

    public static void b() {
        a(0, f2408a, null, -1);
    }

    public static void b(String str, Exception exc) {
        try {
            c.a d = c.d(exc);
            b f = e.a().f();
            f.a(d.f2403a.a());
            f.c(d.b);
            f.b(str);
            e.a().a(f);
        } catch (NullPointerException e) {
        }
    }

    public static String c() {
        byte[] a2;
        com.xiaomi.push.c.c e = e.a().e();
        if (e == null || (a2 = u.a(e)) == null) {
            return null;
        }
        String str = new String(com.xiaomi.a.a.g.a.a(a2));
        com.xiaomi.a.a.c.c.a("stat encoded size = " + str.length());
        com.xiaomi.a.a.c.c.c(str);
        return str;
    }
}
