package com.xiaomi.e;

import cn.banshenggua.aichang.utils.Constants;
import com.xiaomi.a.a.e.d;
import com.xiaomi.a.a.f.a;
import com.xiaomi.push.c.b;
import com.xiaomi.push.c.c;
import com.xiaomi.push.service.XMPushService;
import com.xiaomi.push.service.h;
import com.xiaomi.push.service.x;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import org.apache.a.b.f;
import org.apache.a.b.l;

public class e {

    /* renamed from: a  reason: collision with root package name */
    private String f2405a;
    private boolean b = false;
    private int c;
    private long d;
    private d e;
    private com.xiaomi.a.a.f.a f = com.xiaomi.a.a.f.a.a();

    static class a {

        /* renamed from: a  reason: collision with root package name */
        static final e f2406a = new e();
    }

    public static e a() {
        return a.f2406a;
    }

    private b a(a.C0055a aVar) {
        if (aVar.f2339a != 0) {
            b f2 = f();
            f2.a(com.xiaomi.push.c.a.CHANNEL_STATS_COUNTER.a());
            f2.c(aVar.f2339a);
            f2.c(aVar.b);
            return f2;
        } else if (aVar.c instanceof b) {
            return (b) aVar.c;
        } else {
            return null;
        }
    }

    static d b() {
        return a.f2406a.e;
    }

    private c b(int i) {
        ArrayList arrayList = new ArrayList();
        c cVar = new c(this.f2405a, arrayList);
        if (!d.e(this.e.f2404a)) {
            cVar.a(x.d(this.e.f2404a));
        }
        org.apache.a.c.b bVar = new org.apache.a.c.b(i);
        f a2 = new l.a().a(bVar);
        try {
            cVar.b(a2);
        } catch (org.apache.a.f e2) {
        }
        LinkedList<a.C0055a> c2 = this.f.c();
        while (c2.size() > 0) {
            try {
                b a3 = a(c2.getLast());
                if (a3 != null) {
                    a3.b(a2);
                }
                if (bVar.b_() > i) {
                    break;
                }
                if (a3 != null) {
                    arrayList.add(a3);
                }
                c2.removeLast();
            } catch (NoSuchElementException | org.apache.a.f e3) {
            }
        }
        com.xiaomi.a.a.c.c.a("stat approximate size = " + bVar.b_());
        return cVar;
    }

    private void g() {
        if (this.d == 0) {
            this.d = System.currentTimeMillis();
        }
    }

    private void h() {
        if (this.b && System.currentTimeMillis() - this.d > ((long) this.c)) {
            this.b = false;
            this.d = 0;
        }
    }

    public void a(int i) {
        int i2 = 604800000;
        if (i > 0) {
            this.b = true;
            int i3 = i * Constants.CLEARIMGED;
            if (i3 <= 604800000) {
                i2 = i3;
            }
            if (this.c != i2) {
                this.c = i2;
                g();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void a(b bVar) {
        this.f.a(bVar);
    }

    public synchronized void a(XMPushService xMPushService, com.xiaomi.d.l lVar) {
        this.e = new d(xMPushService);
        this.f2405a = "";
        if (lVar != null) {
            lVar.a(this.e);
        }
        h.a().a(new f(this));
    }

    public boolean c() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public boolean d() {
        h();
        return this.b && this.f.b() > 0;
    }

    /* access modifiers changed from: package-private */
    public synchronized c e() {
        c cVar;
        cVar = null;
        if (d()) {
            int i = 750;
            if (!d.e(com.xiaomi.d.e.h.a())) {
                i = 375;
            }
            cVar = b(i);
        }
        return cVar;
    }

    /* access modifiers changed from: package-private */
    public b f() {
        b bVar = new b();
        bVar.a(d.f(this.e.f2404a));
        bVar.f2491a = 0;
        bVar.c = 1;
        bVar.d((int) (System.currentTimeMillis() / 1000));
        if (this.e.b != null) {
            bVar.e(this.e.b.e());
        }
        return bVar;
    }
}
