package com.xiaomi.e;

import com.xiaomi.e.c;
import com.xiaomi.push.c.b;
import com.xiaomi.push.service.XMPushService;
import com.xiaomi.push.service.ao;

class a implements ao.b.a {

    /* renamed from: a  reason: collision with root package name */
    private XMPushService f2400a;
    private ao.b b;
    private com.xiaomi.d.a c;
    private ao.c d;
    private int e;
    private boolean f = false;

    a(XMPushService xMPushService, ao.b bVar) {
        this.f2400a = xMPushService;
        this.d = ao.c.binding;
        this.b = bVar;
    }

    private void b() {
        this.b.b(this);
    }

    /* access modifiers changed from: private */
    public void c() {
        b();
        if (this.f && this.e != 11) {
            b f2 = e.a().f();
            switch (this.d) {
                case unbind:
                    if (this.e != 17) {
                        if (this.e != 21) {
                            try {
                                c.a c2 = c.c(e.b().a());
                                f2.b = c2.f2403a.a();
                                f2.c(c2.b);
                                break;
                            } catch (NullPointerException e2) {
                                f2 = null;
                                break;
                            }
                        } else {
                            f2.b = com.xiaomi.push.c.a.BIND_TIMEOUT.a();
                            break;
                        }
                    } else {
                        f2.b = com.xiaomi.push.c.a.BIND_TCP_READ_TIMEOUT.a();
                        break;
                    }
                case binded:
                    f2.b = com.xiaomi.push.c.a.BIND_SUCCESS.a();
                    break;
            }
            if (f2 != null) {
                f2.b(this.c.c());
                f2.d(this.b.b);
                f2.c = 1;
                try {
                    f2.a((byte) Integer.parseInt(this.b.h));
                } catch (NumberFormatException e3) {
                }
                e.a().a(f2);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.b.a(this);
        this.c = this.f2400a.g();
    }

    public void a(ao.c cVar, ao.c cVar2, int i) {
        if (!this.f && cVar == ao.c.binding) {
            this.d = cVar2;
            this.e = i;
            this.f = true;
        }
        this.f2400a.a(new b(this, 4));
    }
}
