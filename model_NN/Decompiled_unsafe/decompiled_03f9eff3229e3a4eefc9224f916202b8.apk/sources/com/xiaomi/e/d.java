package com.xiaomi.e;

import com.xiaomi.d.a;
import com.xiaomi.push.service.XMPushService;

public class d implements com.xiaomi.d.d {

    /* renamed from: a  reason: collision with root package name */
    XMPushService f2404a;
    a b;
    private int c;
    private Exception d;

    d(XMPushService xMPushService) {
        this.f2404a = xMPushService;
    }

    /* access modifiers changed from: package-private */
    public Exception a() {
        return this.d;
    }

    public void a(a aVar) {
        g.a(0, com.xiaomi.push.c.a.CONN_SUCCESS.a(), aVar.c(), aVar.i());
    }

    public void a(a aVar, int i, Exception exc) {
        if (this.c == 0 && this.d == null) {
            this.c = i;
            this.d = exc;
            g.b(aVar.c(), exc);
        }
    }

    public void a(a aVar, Exception exc) {
        g.a(0, com.xiaomi.push.c.a.CHANNEL_CON_FAIL.a(), 1, aVar.c(), com.xiaomi.a.a.e.d.d(this.f2404a) ? 1 : 0);
    }

    public void b(a aVar) {
        this.c = 0;
        this.d = null;
        this.b = aVar;
        g.a(0, com.xiaomi.push.c.a.CONN_SUCCESS.a());
    }
}
