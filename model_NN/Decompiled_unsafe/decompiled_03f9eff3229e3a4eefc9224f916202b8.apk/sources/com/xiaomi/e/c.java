package com.xiaomi.e;

import com.xiaomi.d.p;
import java.net.UnknownHostException;

final class c {

    static class a {

        /* renamed from: a  reason: collision with root package name */
        com.xiaomi.push.c.a f2403a;
        String b;

        a() {
        }
    }

    /* JADX INFO: additional move instructions added (2) to help type inference */
    /* JADX INFO: additional move instructions added (1) to help type inference */
    static a a(Exception exc) {
        e(exc);
        boolean z = exc instanceof p;
        Throwable th = exc;
        if (z) {
            Throwable a2 = ((p) exc).a();
            th = exc;
            if (a2 != null) {
                th = ((p) exc).a();
            }
        }
        a aVar = new a();
        String message = th.getMessage();
        if (th.getCause() != null) {
            message = th.getCause().getMessage();
        }
        String str = th.getClass().getSimpleName() + ":" + message;
        int a3 = com.xiaomi.d.c.a(th);
        if (a3 != 0) {
            aVar.f2403a = com.xiaomi.push.c.a.a(a3 + com.xiaomi.push.c.a.GSLB_REQUEST_SUCCESS.a());
        }
        if (aVar.f2403a == null) {
            aVar.f2403a = com.xiaomi.push.c.a.GSLB_TCP_ERR_OTHER;
        }
        if (aVar.f2403a == com.xiaomi.push.c.a.GSLB_TCP_ERR_OTHER) {
            aVar.b = str;
        }
        return aVar;
    }

    /* JADX INFO: additional move instructions added (2) to help type inference */
    /* JADX INFO: additional move instructions added (1) to help type inference */
    static a b(Exception exc) {
        Throwable cause;
        e(exc);
        boolean z = exc instanceof p;
        Throwable th = exc;
        if (z) {
            Throwable a2 = ((p) exc).a();
            th = exc;
            if (a2 != null) {
                th = ((p) exc).a();
            }
        }
        a aVar = new a();
        String message = th.getMessage();
        if (th.getCause() != null) {
            message = th.getCause().getMessage();
        }
        int a3 = com.xiaomi.d.c.a(th);
        String str = th.getClass().getSimpleName() + ":" + message;
        if (a3 != 0) {
            aVar.f2403a = com.xiaomi.push.c.a.a(a3 + com.xiaomi.push.c.a.CONN_SUCCESS.a());
            if (aVar.f2403a == com.xiaomi.push.c.a.CONN_BOSH_ERR && (cause = th.getCause()) != null && (cause instanceof UnknownHostException)) {
                aVar.f2403a = com.xiaomi.push.c.a.CONN_BOSH_UNKNOWNHOST;
            }
        } else {
            aVar.f2403a = com.xiaomi.push.c.a.CONN_XMPP_ERR;
        }
        if (aVar.f2403a == com.xiaomi.push.c.a.CONN_TCP_ERR_OTHER || aVar.f2403a == com.xiaomi.push.c.a.CONN_XMPP_ERR || aVar.f2403a == com.xiaomi.push.c.a.CONN_BOSH_ERR) {
            aVar.b = str;
        }
        return aVar;
    }

    /* JADX INFO: additional move instructions added (2) to help type inference */
    /* JADX INFO: additional move instructions added (1) to help type inference */
    static a c(Exception exc) {
        e(exc);
        boolean z = exc instanceof p;
        Throwable th = exc;
        if (z) {
            Throwable a2 = ((p) exc).a();
            th = exc;
            if (a2 != null) {
                th = ((p) exc).a();
            }
        }
        a aVar = new a();
        String message = th.getMessage();
        if (th.getCause() != null) {
            message = th.getCause().getMessage();
        }
        String str = th.getClass().getSimpleName() + ":" + message;
        switch (com.xiaomi.d.c.a(th)) {
            case 105:
                aVar.f2403a = com.xiaomi.push.c.a.BIND_TCP_READ_TIMEOUT;
                break;
            case 109:
                aVar.f2403a = com.xiaomi.push.c.a.BIND_TCP_CONNRESET;
                break;
            case 110:
                aVar.f2403a = com.xiaomi.push.c.a.BIND_TCP_BROKEN_PIPE;
                break;
            case 199:
                aVar.f2403a = com.xiaomi.push.c.a.BIND_TCP_ERR;
                break;
            case 499:
                aVar.f2403a = com.xiaomi.push.c.a.BIND_BOSH_ERR;
                if (message.startsWith("Terminal binding condition encountered: item-not-found")) {
                    aVar.f2403a = com.xiaomi.push.c.a.BIND_BOSH_ITEM_NOT_FOUND;
                    break;
                }
                break;
            default:
                aVar.f2403a = com.xiaomi.push.c.a.BIND_XMPP_ERR;
                break;
        }
        if (aVar.f2403a == com.xiaomi.push.c.a.BIND_TCP_ERR || aVar.f2403a == com.xiaomi.push.c.a.BIND_XMPP_ERR || aVar.f2403a == com.xiaomi.push.c.a.BIND_BOSH_ERR) {
            aVar.b = str;
        }
        return aVar;
    }

    /* JADX INFO: additional move instructions added (2) to help type inference */
    /* JADX INFO: additional move instructions added (1) to help type inference */
    static a d(Exception exc) {
        e(exc);
        boolean z = exc instanceof p;
        Throwable th = exc;
        if (z) {
            Throwable a2 = ((p) exc).a();
            th = exc;
            if (a2 != null) {
                th = ((p) exc).a();
            }
        }
        a aVar = new a();
        String message = th.getMessage();
        String str = th.getClass().getSimpleName() + ":" + message;
        switch (com.xiaomi.d.c.a(th)) {
            case 105:
                aVar.f2403a = com.xiaomi.push.c.a.CHANNEL_TCP_READTIMEOUT;
                break;
            case 109:
                aVar.f2403a = com.xiaomi.push.c.a.CHANNEL_TCP_CONNRESET;
                break;
            case 110:
                aVar.f2403a = com.xiaomi.push.c.a.CHANNEL_TCP_BROKEN_PIPE;
                break;
            case 199:
                aVar.f2403a = com.xiaomi.push.c.a.CHANNEL_TCP_ERR;
                break;
            case 499:
                aVar.f2403a = com.xiaomi.push.c.a.CHANNEL_BOSH_EXCEPTION;
                if (message.startsWith("Terminal binding condition encountered: item-not-found")) {
                    aVar.f2403a = com.xiaomi.push.c.a.CHANNEL_BOSH_ITEMNOTFIND;
                    break;
                }
                break;
            default:
                aVar.f2403a = com.xiaomi.push.c.a.CHANNEL_XMPPEXCEPTION;
                break;
        }
        if (aVar.f2403a == com.xiaomi.push.c.a.CHANNEL_TCP_ERR || aVar.f2403a == com.xiaomi.push.c.a.CHANNEL_XMPPEXCEPTION || aVar.f2403a == com.xiaomi.push.c.a.CHANNEL_BOSH_EXCEPTION) {
            aVar.b = str;
        }
        return aVar;
    }

    private static void e(Exception exc) {
        if (exc == null) {
            throw new NullPointerException();
        }
    }
}
