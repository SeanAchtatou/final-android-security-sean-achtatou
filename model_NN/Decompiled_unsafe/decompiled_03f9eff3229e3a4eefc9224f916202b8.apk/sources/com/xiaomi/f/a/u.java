package com.xiaomi.f.a;

import com.xiaomi.a.a.c.c;
import org.apache.a.b;
import org.apache.a.b.a;
import org.apache.a.b.l;
import org.apache.a.e;
import org.apache.a.f;
import org.apache.a.g;

public class u {
    public static <T extends b<T, ?>> void a(T t, byte[] bArr) {
        if (bArr == null) {
            throw new f("the message byte is empty.");
        }
        new e(new l.a(true, true, bArr.length)).a(t, bArr);
    }

    public static <T extends b<T, ?>> byte[] a(T t) {
        if (t == null) {
            return null;
        }
        try {
            return new g(new a.C0061a()).a(t);
        } catch (f e) {
            c.a("convertThriftObjectToBytes catch TException.", e);
            return null;
        }
    }
}
