package com.xiaomi.f.a;

import com.sina.weibo.sdk.component.WidgetRequestParam;
import java.io.Serializable;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.apache.a.a.g;
import org.apache.a.b;
import org.apache.a.b.c;
import org.apache.a.b.f;
import org.apache.a.b.i;
import org.apache.a.b.k;

public class s implements Serializable, Cloneable, b<s, a> {
    public static final Map<a, org.apache.a.a.b> h;
    private static final k i = new k("XmPushActionUnSubscription");
    private static final c j = new c("debug", (byte) 11, 1);
    private static final c k = new c("target", (byte) 12, 2);
    private static final c l = new c("id", (byte) 11, 3);
    private static final c m = new c("appId", (byte) 11, 4);
    private static final c n = new c("topic", (byte) 11, 5);
    private static final c o = new c("packageName", (byte) 11, 6);
    private static final c p = new c(WidgetRequestParam.REQ_PARAM_COMMENT_CATEGORY, (byte) 11, 7);

    /* renamed from: a  reason: collision with root package name */
    public String f2445a;
    public d b;
    public String c;
    public String d;
    public String e;
    public String f;
    public String g;

    public enum a {
        DEBUG(1, "debug"),
        TARGET(2, "target"),
        ID(3, "id"),
        APP_ID(4, "appId"),
        TOPIC(5, "topic"),
        PACKAGE_NAME(6, "packageName"),
        CATEGORY(7, WidgetRequestParam.REQ_PARAM_COMMENT_CATEGORY);
        
        private static final Map<String, a> h = new HashMap();
        private final short i;
        private final String j;

        static {
            Iterator it = EnumSet.allOf(a.class).iterator();
            while (it.hasNext()) {
                a aVar = (a) it.next();
                h.put(aVar.a(), aVar);
            }
        }

        private a(short s, String str) {
            this.i = s;
            this.j = str;
        }

        public String a() {
            return this.j;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [com.xiaomi.f.a.s$a, org.apache.a.a.b]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        EnumMap enumMap = new EnumMap(a.class);
        enumMap.put((Object) a.DEBUG, (Object) new org.apache.a.a.b("debug", (byte) 2, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.TARGET, (Object) new org.apache.a.a.b("target", (byte) 2, new g((byte) 12, d.class)));
        enumMap.put((Object) a.ID, (Object) new org.apache.a.a.b("id", (byte) 1, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.APP_ID, (Object) new org.apache.a.a.b("appId", (byte) 1, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.TOPIC, (Object) new org.apache.a.a.b("topic", (byte) 1, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.PACKAGE_NAME, (Object) new org.apache.a.a.b("packageName", (byte) 2, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.CATEGORY, (Object) new org.apache.a.a.b(WidgetRequestParam.REQ_PARAM_COMMENT_CATEGORY, (byte) 2, new org.apache.a.a.c((byte) 11)));
        h = Collections.unmodifiableMap(enumMap);
        org.apache.a.a.b.a(s.class, h);
    }

    public void a(f fVar) {
        fVar.g();
        while (true) {
            c i2 = fVar.i();
            if (i2.b == 0) {
                fVar.h();
                h();
                return;
            }
            switch (i2.c) {
                case 1:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.f2445a = fVar.w();
                        break;
                    }
                case 2:
                    if (i2.b != 12) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.b = new d();
                        this.b.a(fVar);
                        break;
                    }
                case 3:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.c = fVar.w();
                        break;
                    }
                case 4:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.d = fVar.w();
                        break;
                    }
                case 5:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.e = fVar.w();
                        break;
                    }
                case 6:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.f = fVar.w();
                        break;
                    }
                case 7:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.g = fVar.w();
                        break;
                    }
                default:
                    i.a(fVar, i2.b);
                    break;
            }
            fVar.j();
        }
    }

    public boolean a() {
        return this.f2445a != null;
    }

    public boolean a(s sVar) {
        if (sVar == null) {
            return false;
        }
        boolean a2 = a();
        boolean a3 = sVar.a();
        if ((a2 || a3) && (!a2 || !a3 || !this.f2445a.equals(sVar.f2445a))) {
            return false;
        }
        boolean b2 = b();
        boolean b3 = sVar.b();
        if ((b2 || b3) && (!b2 || !b3 || !this.b.a(sVar.b))) {
            return false;
        }
        boolean c2 = c();
        boolean c3 = sVar.c();
        if ((c2 || c3) && (!c2 || !c3 || !this.c.equals(sVar.c))) {
            return false;
        }
        boolean d2 = d();
        boolean d3 = sVar.d();
        if ((d2 || d3) && (!d2 || !d3 || !this.d.equals(sVar.d))) {
            return false;
        }
        boolean e2 = e();
        boolean e3 = sVar.e();
        if ((e2 || e3) && (!e2 || !e3 || !this.e.equals(sVar.e))) {
            return false;
        }
        boolean f2 = f();
        boolean f3 = sVar.f();
        if ((f2 || f3) && (!f2 || !f3 || !this.f.equals(sVar.f))) {
            return false;
        }
        boolean g2 = g();
        boolean g3 = sVar.g();
        return (!g2 && !g3) || (g2 && g3 && this.g.equals(sVar.g));
    }

    /* renamed from: b */
    public int compareTo(s sVar) {
        int a2;
        int a3;
        int a4;
        int a5;
        int a6;
        int a7;
        int a8;
        if (!getClass().equals(sVar.getClass())) {
            return getClass().getName().compareTo(sVar.getClass().getName());
        }
        int compareTo = Boolean.valueOf(a()).compareTo(Boolean.valueOf(sVar.a()));
        if (compareTo != 0) {
            return compareTo;
        }
        if (a() && (a8 = org.apache.a.c.a(this.f2445a, sVar.f2445a)) != 0) {
            return a8;
        }
        int compareTo2 = Boolean.valueOf(b()).compareTo(Boolean.valueOf(sVar.b()));
        if (compareTo2 != 0) {
            return compareTo2;
        }
        if (b() && (a7 = org.apache.a.c.a(this.b, sVar.b)) != 0) {
            return a7;
        }
        int compareTo3 = Boolean.valueOf(c()).compareTo(Boolean.valueOf(sVar.c()));
        if (compareTo3 != 0) {
            return compareTo3;
        }
        if (c() && (a6 = org.apache.a.c.a(this.c, sVar.c)) != 0) {
            return a6;
        }
        int compareTo4 = Boolean.valueOf(d()).compareTo(Boolean.valueOf(sVar.d()));
        if (compareTo4 != 0) {
            return compareTo4;
        }
        if (d() && (a5 = org.apache.a.c.a(this.d, sVar.d)) != 0) {
            return a5;
        }
        int compareTo5 = Boolean.valueOf(e()).compareTo(Boolean.valueOf(sVar.e()));
        if (compareTo5 != 0) {
            return compareTo5;
        }
        if (e() && (a4 = org.apache.a.c.a(this.e, sVar.e)) != 0) {
            return a4;
        }
        int compareTo6 = Boolean.valueOf(f()).compareTo(Boolean.valueOf(sVar.f()));
        if (compareTo6 != 0) {
            return compareTo6;
        }
        if (f() && (a3 = org.apache.a.c.a(this.f, sVar.f)) != 0) {
            return a3;
        }
        int compareTo7 = Boolean.valueOf(g()).compareTo(Boolean.valueOf(sVar.g()));
        if (compareTo7 != 0) {
            return compareTo7;
        }
        if (!g() || (a2 = org.apache.a.c.a(this.g, sVar.g)) == 0) {
            return 0;
        }
        return a2;
    }

    public void b(f fVar) {
        h();
        fVar.a(i);
        if (this.f2445a != null && a()) {
            fVar.a(j);
            fVar.a(this.f2445a);
            fVar.b();
        }
        if (this.b != null && b()) {
            fVar.a(k);
            this.b.b(fVar);
            fVar.b();
        }
        if (this.c != null) {
            fVar.a(l);
            fVar.a(this.c);
            fVar.b();
        }
        if (this.d != null) {
            fVar.a(m);
            fVar.a(this.d);
            fVar.b();
        }
        if (this.e != null) {
            fVar.a(n);
            fVar.a(this.e);
            fVar.b();
        }
        if (this.f != null && f()) {
            fVar.a(o);
            fVar.a(this.f);
            fVar.b();
        }
        if (this.g != null && g()) {
            fVar.a(p);
            fVar.a(this.g);
            fVar.b();
        }
        fVar.c();
        fVar.a();
    }

    public boolean b() {
        return this.b != null;
    }

    public boolean c() {
        return this.c != null;
    }

    public boolean d() {
        return this.d != null;
    }

    public boolean e() {
        return this.e != null;
    }

    public boolean equals(Object obj) {
        if (obj != null && (obj instanceof s)) {
            return a((s) obj);
        }
        return false;
    }

    public boolean f() {
        return this.f != null;
    }

    public boolean g() {
        return this.g != null;
    }

    public void h() {
        if (this.c == null) {
            throw new org.apache.a.b.g("Required field 'id' was not present! Struct: " + toString());
        } else if (this.d == null) {
            throw new org.apache.a.b.g("Required field 'appId' was not present! Struct: " + toString());
        } else if (this.e == null) {
            throw new org.apache.a.b.g("Required field 'topic' was not present! Struct: " + toString());
        }
    }

    public int hashCode() {
        return 0;
    }

    public String toString() {
        boolean z = false;
        StringBuilder sb = new StringBuilder("XmPushActionUnSubscription(");
        boolean z2 = true;
        if (a()) {
            sb.append("debug:");
            if (this.f2445a == null) {
                sb.append("null");
            } else {
                sb.append(this.f2445a);
            }
            z2 = false;
        }
        if (b()) {
            if (!z2) {
                sb.append(", ");
            }
            sb.append("target:");
            if (this.b == null) {
                sb.append("null");
            } else {
                sb.append(this.b);
            }
        } else {
            z = z2;
        }
        if (!z) {
            sb.append(", ");
        }
        sb.append("id:");
        if (this.c == null) {
            sb.append("null");
        } else {
            sb.append(this.c);
        }
        sb.append(", ");
        sb.append("appId:");
        if (this.d == null) {
            sb.append("null");
        } else {
            sb.append(this.d);
        }
        sb.append(", ");
        sb.append("topic:");
        if (this.e == null) {
            sb.append("null");
        } else {
            sb.append(this.e);
        }
        if (f()) {
            sb.append(", ");
            sb.append("packageName:");
            if (this.f == null) {
                sb.append("null");
            } else {
                sb.append(this.f);
            }
        }
        if (g()) {
            sb.append(", ");
            sb.append("category:");
            if (this.g == null) {
                sb.append("null");
            } else {
                sb.append(this.g);
            }
        }
        sb.append(")");
        return sb.toString();
    }
}
