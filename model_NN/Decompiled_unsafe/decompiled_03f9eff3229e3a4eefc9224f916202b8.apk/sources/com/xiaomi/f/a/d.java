package com.xiaomi.f.a;

import java.io.Serializable;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.apache.a.b;
import org.apache.a.b.c;
import org.apache.a.b.f;
import org.apache.a.b.g;
import org.apache.a.b.i;
import org.apache.a.b.k;

public class d implements Serializable, Cloneable, b<d, a> {
    public static final Map<a, org.apache.a.a.b> f;
    private static final k g = new k("Target");
    private static final c h = new c("channelId", (byte) 10, 1);
    private static final c i = new c("userId", (byte) 11, 2);
    private static final c j = new c("server", (byte) 11, 3);
    private static final c k = new c("resource", (byte) 11, 4);
    private static final c l = new c("isPreview", (byte) 2, 5);

    /* renamed from: a  reason: collision with root package name */
    public long f2415a = 5;
    public String b;
    public String c = "xiaomi.com";
    public String d = "";
    public boolean e = false;
    private BitSet m = new BitSet(2);

    public enum a {
        CHANNEL_ID(1, "channelId"),
        USER_ID(2, "userId"),
        SERVER(3, "server"),
        RESOURCE(4, "resource"),
        IS_PREVIEW(5, "isPreview");
        
        private static final Map<String, a> f = new HashMap();
        private final short g;
        private final String h;

        static {
            Iterator it = EnumSet.allOf(a.class).iterator();
            while (it.hasNext()) {
                a aVar = (a) it.next();
                f.put(aVar.a(), aVar);
            }
        }

        private a(short s, String str) {
            this.g = s;
            this.h = str;
        }

        public String a() {
            return this.h;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [com.xiaomi.f.a.d$a, org.apache.a.a.b]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        EnumMap enumMap = new EnumMap(a.class);
        enumMap.put((Object) a.CHANNEL_ID, (Object) new org.apache.a.a.b("channelId", (byte) 1, new org.apache.a.a.c((byte) 10)));
        enumMap.put((Object) a.USER_ID, (Object) new org.apache.a.a.b("userId", (byte) 1, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.SERVER, (Object) new org.apache.a.a.b("server", (byte) 2, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.RESOURCE, (Object) new org.apache.a.a.b("resource", (byte) 2, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.IS_PREVIEW, (Object) new org.apache.a.a.b("isPreview", (byte) 2, new org.apache.a.a.c((byte) 2)));
        f = Collections.unmodifiableMap(enumMap);
        org.apache.a.a.b.a(d.class, f);
    }

    public void a(f fVar) {
        fVar.g();
        while (true) {
            c i2 = fVar.i();
            if (i2.b == 0) {
                fVar.h();
                if (!a()) {
                    throw new g("Required field 'channelId' was not found in serialized data! Struct: " + toString());
                }
                f();
                return;
            }
            switch (i2.c) {
                case 1:
                    if (i2.b != 10) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.f2415a = fVar.u();
                        a(true);
                        break;
                    }
                case 2:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.b = fVar.w();
                        break;
                    }
                case 3:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.c = fVar.w();
                        break;
                    }
                case 4:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.d = fVar.w();
                        break;
                    }
                case 5:
                    if (i2.b != 2) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.e = fVar.q();
                        b(true);
                        break;
                    }
                default:
                    i.a(fVar, i2.b);
                    break;
            }
            fVar.j();
        }
    }

    public void a(boolean z) {
        this.m.set(0, z);
    }

    public boolean a() {
        return this.m.get(0);
    }

    public boolean a(d dVar) {
        if (dVar == null || this.f2415a != dVar.f2415a) {
            return false;
        }
        boolean b2 = b();
        boolean b3 = dVar.b();
        if ((b2 || b3) && (!b2 || !b3 || !this.b.equals(dVar.b))) {
            return false;
        }
        boolean c2 = c();
        boolean c3 = dVar.c();
        if ((c2 || c3) && (!c2 || !c3 || !this.c.equals(dVar.c))) {
            return false;
        }
        boolean d2 = d();
        boolean d3 = dVar.d();
        if ((d2 || d3) && (!d2 || !d3 || !this.d.equals(dVar.d))) {
            return false;
        }
        boolean e2 = e();
        boolean e3 = dVar.e();
        return (!e2 && !e3) || (e2 && e3 && this.e == dVar.e);
    }

    /* renamed from: b */
    public int compareTo(d dVar) {
        int a2;
        int a3;
        int a4;
        int a5;
        int a6;
        if (!getClass().equals(dVar.getClass())) {
            return getClass().getName().compareTo(dVar.getClass().getName());
        }
        int compareTo = Boolean.valueOf(a()).compareTo(Boolean.valueOf(dVar.a()));
        if (compareTo != 0) {
            return compareTo;
        }
        if (a() && (a6 = org.apache.a.c.a(this.f2415a, dVar.f2415a)) != 0) {
            return a6;
        }
        int compareTo2 = Boolean.valueOf(b()).compareTo(Boolean.valueOf(dVar.b()));
        if (compareTo2 != 0) {
            return compareTo2;
        }
        if (b() && (a5 = org.apache.a.c.a(this.b, dVar.b)) != 0) {
            return a5;
        }
        int compareTo3 = Boolean.valueOf(c()).compareTo(Boolean.valueOf(dVar.c()));
        if (compareTo3 != 0) {
            return compareTo3;
        }
        if (c() && (a4 = org.apache.a.c.a(this.c, dVar.c)) != 0) {
            return a4;
        }
        int compareTo4 = Boolean.valueOf(d()).compareTo(Boolean.valueOf(dVar.d()));
        if (compareTo4 != 0) {
            return compareTo4;
        }
        if (d() && (a3 = org.apache.a.c.a(this.d, dVar.d)) != 0) {
            return a3;
        }
        int compareTo5 = Boolean.valueOf(e()).compareTo(Boolean.valueOf(dVar.e()));
        if (compareTo5 != 0) {
            return compareTo5;
        }
        if (!e() || (a2 = org.apache.a.c.a(this.e, dVar.e)) == 0) {
            return 0;
        }
        return a2;
    }

    public void b(f fVar) {
        f();
        fVar.a(g);
        fVar.a(h);
        fVar.a(this.f2415a);
        fVar.b();
        if (this.b != null) {
            fVar.a(i);
            fVar.a(this.b);
            fVar.b();
        }
        if (this.c != null && c()) {
            fVar.a(j);
            fVar.a(this.c);
            fVar.b();
        }
        if (this.d != null && d()) {
            fVar.a(k);
            fVar.a(this.d);
            fVar.b();
        }
        if (e()) {
            fVar.a(l);
            fVar.a(this.e);
            fVar.b();
        }
        fVar.c();
        fVar.a();
    }

    public void b(boolean z) {
        this.m.set(1, z);
    }

    public boolean b() {
        return this.b != null;
    }

    public boolean c() {
        return this.c != null;
    }

    public boolean d() {
        return this.d != null;
    }

    public boolean e() {
        return this.m.get(1);
    }

    public boolean equals(Object obj) {
        if (obj != null && (obj instanceof d)) {
            return a((d) obj);
        }
        return false;
    }

    public void f() {
        if (this.b == null) {
            throw new g("Required field 'userId' was not present! Struct: " + toString());
        }
    }

    public int hashCode() {
        return 0;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("Target(");
        sb.append("channelId:");
        sb.append(this.f2415a);
        sb.append(", ");
        sb.append("userId:");
        if (this.b == null) {
            sb.append("null");
        } else {
            sb.append(this.b);
        }
        if (c()) {
            sb.append(", ");
            sb.append("server:");
            if (this.c == null) {
                sb.append("null");
            } else {
                sb.append(this.c);
            }
        }
        if (d()) {
            sb.append(", ");
            sb.append("resource:");
            if (this.d == null) {
                sb.append("null");
            } else {
                sb.append(this.d);
            }
        }
        if (e()) {
            sb.append(", ");
            sb.append("isPreview:");
            sb.append(this.e);
        }
        sb.append(")");
        return sb.toString();
    }
}
