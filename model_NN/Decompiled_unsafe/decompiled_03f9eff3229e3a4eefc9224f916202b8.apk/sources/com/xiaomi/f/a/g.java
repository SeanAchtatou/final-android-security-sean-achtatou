package com.xiaomi.f.a;

import com.sina.weibo.sdk.component.WidgetRequestParam;
import com.tencent.open.SocialConstants;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.a.a.d;
import org.apache.a.b;
import org.apache.a.b.c;
import org.apache.a.b.f;
import org.apache.a.b.i;
import org.apache.a.b.k;

public class g implements Serializable, Cloneable, b<g, a> {
    public static final Map<a, org.apache.a.a.b> l;
    private static final k m = new k("XmPushActionCommandResult");
    private static final c n = new c("debug", (byte) 11, 1);
    private static final c o = new c("target", (byte) 12, 2);
    private static final c p = new c("id", (byte) 11, 3);
    private static final c q = new c("appId", (byte) 11, 4);
    private static final c r = new c("cmdName", (byte) 11, 5);
    private static final c s = new c(SocialConstants.TYPE_REQUEST, (byte) 12, 6);
    private static final c t = new c("errorCode", (byte) 10, 7);
    private static final c u = new c("reason", (byte) 11, 8);
    private static final c v = new c("packageName", (byte) 11, 9);
    private static final c w = new c("cmdArgs", (byte) 15, 10);
    private static final c x = new c(WidgetRequestParam.REQ_PARAM_COMMENT_CATEGORY, (byte) 11, 12);

    /* renamed from: a  reason: collision with root package name */
    public String f2421a;
    public d b;
    public String c;
    public String d;
    public String e;
    public f f;
    public long g;
    public String h;
    public String i;
    public List<String> j;
    public String k;
    private BitSet y = new BitSet(1);

    public enum a {
        DEBUG(1, "debug"),
        TARGET(2, "target"),
        ID(3, "id"),
        APP_ID(4, "appId"),
        CMD_NAME(5, "cmdName"),
        REQUEST(6, SocialConstants.TYPE_REQUEST),
        ERROR_CODE(7, "errorCode"),
        REASON(8, "reason"),
        PACKAGE_NAME(9, "packageName"),
        CMD_ARGS(10, "cmdArgs"),
        CATEGORY(12, WidgetRequestParam.REQ_PARAM_COMMENT_CATEGORY);
        
        private static final Map<String, a> l = new HashMap();
        private final short m;
        private final String n;

        static {
            Iterator it = EnumSet.allOf(a.class).iterator();
            while (it.hasNext()) {
                a aVar = (a) it.next();
                l.put(aVar.a(), aVar);
            }
        }

        private a(short s, String str) {
            this.m = s;
            this.n = str;
        }

        public String a() {
            return this.n;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [com.xiaomi.f.a.g$a, org.apache.a.a.b]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        EnumMap enumMap = new EnumMap(a.class);
        enumMap.put((Object) a.DEBUG, (Object) new org.apache.a.a.b("debug", (byte) 2, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.TARGET, (Object) new org.apache.a.a.b("target", (byte) 2, new org.apache.a.a.g((byte) 12, d.class)));
        enumMap.put((Object) a.ID, (Object) new org.apache.a.a.b("id", (byte) 1, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.APP_ID, (Object) new org.apache.a.a.b("appId", (byte) 1, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.CMD_NAME, (Object) new org.apache.a.a.b("cmdName", (byte) 1, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.REQUEST, (Object) new org.apache.a.a.b(SocialConstants.TYPE_REQUEST, (byte) 2, new org.apache.a.a.g((byte) 12, f.class)));
        enumMap.put((Object) a.ERROR_CODE, (Object) new org.apache.a.a.b("errorCode", (byte) 1, new org.apache.a.a.c((byte) 10)));
        enumMap.put((Object) a.REASON, (Object) new org.apache.a.a.b("reason", (byte) 2, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.PACKAGE_NAME, (Object) new org.apache.a.a.b("packageName", (byte) 2, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.CMD_ARGS, (Object) new org.apache.a.a.b("cmdArgs", (byte) 2, new d((byte) 15, new org.apache.a.a.c((byte) 11))));
        enumMap.put((Object) a.CATEGORY, (Object) new org.apache.a.a.b(WidgetRequestParam.REQ_PARAM_COMMENT_CATEGORY, (byte) 2, new org.apache.a.a.c((byte) 11)));
        l = Collections.unmodifiableMap(enumMap);
        org.apache.a.a.b.a(g.class, l);
    }

    public void a(f fVar) {
        fVar.g();
        while (true) {
            c i2 = fVar.i();
            if (i2.b == 0) {
                fVar.h();
                if (!h()) {
                    throw new org.apache.a.b.g("Required field 'errorCode' was not found in serialized data! Struct: " + toString());
                }
                o();
                return;
            }
            switch (i2.c) {
                case 1:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.f2421a = fVar.w();
                        break;
                    }
                case 2:
                    if (i2.b != 12) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.b = new d();
                        this.b.a(fVar);
                        break;
                    }
                case 3:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.c = fVar.w();
                        break;
                    }
                case 4:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.d = fVar.w();
                        break;
                    }
                case 5:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.e = fVar.w();
                        break;
                    }
                case 6:
                    if (i2.b != 12) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.f = new f();
                        this.f.a(fVar);
                        break;
                    }
                case 7:
                    if (i2.b != 10) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.g = fVar.u();
                        a(true);
                        break;
                    }
                case 8:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.h = fVar.w();
                        break;
                    }
                case 9:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.i = fVar.w();
                        break;
                    }
                case 10:
                    if (i2.b != 15) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        org.apache.a.b.d m2 = fVar.m();
                        this.j = new ArrayList(m2.b);
                        for (int i3 = 0; i3 < m2.b; i3++) {
                            this.j.add(fVar.w());
                        }
                        fVar.n();
                        break;
                    }
                case 11:
                default:
                    i.a(fVar, i2.b);
                    break;
                case 12:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.k = fVar.w();
                        break;
                    }
            }
            fVar.j();
        }
    }

    public void a(boolean z) {
        this.y.set(0, z);
    }

    public boolean a() {
        return this.f2421a != null;
    }

    public boolean a(g gVar) {
        if (gVar == null) {
            return false;
        }
        boolean a2 = a();
        boolean a3 = gVar.a();
        if ((a2 || a3) && (!a2 || !a3 || !this.f2421a.equals(gVar.f2421a))) {
            return false;
        }
        boolean b2 = b();
        boolean b3 = gVar.b();
        if ((b2 || b3) && (!b2 || !b3 || !this.b.a(gVar.b))) {
            return false;
        }
        boolean c2 = c();
        boolean c3 = gVar.c();
        if ((c2 || c3) && (!c2 || !c3 || !this.c.equals(gVar.c))) {
            return false;
        }
        boolean d2 = d();
        boolean d3 = gVar.d();
        if ((d2 || d3) && (!d2 || !d3 || !this.d.equals(gVar.d))) {
            return false;
        }
        boolean f2 = f();
        boolean f3 = gVar.f();
        if ((f2 || f3) && (!f2 || !f3 || !this.e.equals(gVar.e))) {
            return false;
        }
        boolean g2 = g();
        boolean g3 = gVar.g();
        if (((g2 || g3) && (!g2 || !g3 || !this.f.a(gVar.f))) || this.g != gVar.g) {
            return false;
        }
        boolean i2 = i();
        boolean i3 = gVar.i();
        if ((i2 || i3) && (!i2 || !i3 || !this.h.equals(gVar.h))) {
            return false;
        }
        boolean j2 = j();
        boolean j3 = gVar.j();
        if ((j2 || j3) && (!j2 || !j3 || !this.i.equals(gVar.i))) {
            return false;
        }
        boolean l2 = l();
        boolean l3 = gVar.l();
        if ((l2 || l3) && (!l2 || !l3 || !this.j.equals(gVar.j))) {
            return false;
        }
        boolean n2 = n();
        boolean n3 = gVar.n();
        return (!n2 && !n3) || (n2 && n3 && this.k.equals(gVar.k));
    }

    /* renamed from: b */
    public int compareTo(g gVar) {
        int a2;
        int a3;
        int a4;
        int a5;
        int a6;
        int a7;
        int a8;
        int a9;
        int a10;
        int a11;
        int a12;
        if (!getClass().equals(gVar.getClass())) {
            return getClass().getName().compareTo(gVar.getClass().getName());
        }
        int compareTo = Boolean.valueOf(a()).compareTo(Boolean.valueOf(gVar.a()));
        if (compareTo != 0) {
            return compareTo;
        }
        if (a() && (a12 = org.apache.a.c.a(this.f2421a, gVar.f2421a)) != 0) {
            return a12;
        }
        int compareTo2 = Boolean.valueOf(b()).compareTo(Boolean.valueOf(gVar.b()));
        if (compareTo2 != 0) {
            return compareTo2;
        }
        if (b() && (a11 = org.apache.a.c.a(this.b, gVar.b)) != 0) {
            return a11;
        }
        int compareTo3 = Boolean.valueOf(c()).compareTo(Boolean.valueOf(gVar.c()));
        if (compareTo3 != 0) {
            return compareTo3;
        }
        if (c() && (a10 = org.apache.a.c.a(this.c, gVar.c)) != 0) {
            return a10;
        }
        int compareTo4 = Boolean.valueOf(d()).compareTo(Boolean.valueOf(gVar.d()));
        if (compareTo4 != 0) {
            return compareTo4;
        }
        if (d() && (a9 = org.apache.a.c.a(this.d, gVar.d)) != 0) {
            return a9;
        }
        int compareTo5 = Boolean.valueOf(f()).compareTo(Boolean.valueOf(gVar.f()));
        if (compareTo5 != 0) {
            return compareTo5;
        }
        if (f() && (a8 = org.apache.a.c.a(this.e, gVar.e)) != 0) {
            return a8;
        }
        int compareTo6 = Boolean.valueOf(g()).compareTo(Boolean.valueOf(gVar.g()));
        if (compareTo6 != 0) {
            return compareTo6;
        }
        if (g() && (a7 = org.apache.a.c.a(this.f, gVar.f)) != 0) {
            return a7;
        }
        int compareTo7 = Boolean.valueOf(h()).compareTo(Boolean.valueOf(gVar.h()));
        if (compareTo7 != 0) {
            return compareTo7;
        }
        if (h() && (a6 = org.apache.a.c.a(this.g, gVar.g)) != 0) {
            return a6;
        }
        int compareTo8 = Boolean.valueOf(i()).compareTo(Boolean.valueOf(gVar.i()));
        if (compareTo8 != 0) {
            return compareTo8;
        }
        if (i() && (a5 = org.apache.a.c.a(this.h, gVar.h)) != 0) {
            return a5;
        }
        int compareTo9 = Boolean.valueOf(j()).compareTo(Boolean.valueOf(gVar.j()));
        if (compareTo9 != 0) {
            return compareTo9;
        }
        if (j() && (a4 = org.apache.a.c.a(this.i, gVar.i)) != 0) {
            return a4;
        }
        int compareTo10 = Boolean.valueOf(l()).compareTo(Boolean.valueOf(gVar.l()));
        if (compareTo10 != 0) {
            return compareTo10;
        }
        if (l() && (a3 = org.apache.a.c.a(this.j, gVar.j)) != 0) {
            return a3;
        }
        int compareTo11 = Boolean.valueOf(n()).compareTo(Boolean.valueOf(gVar.n()));
        if (compareTo11 != 0) {
            return compareTo11;
        }
        if (!n() || (a2 = org.apache.a.c.a(this.k, gVar.k)) == 0) {
            return 0;
        }
        return a2;
    }

    public void b(f fVar) {
        o();
        fVar.a(m);
        if (this.f2421a != null && a()) {
            fVar.a(n);
            fVar.a(this.f2421a);
            fVar.b();
        }
        if (this.b != null && b()) {
            fVar.a(o);
            this.b.b(fVar);
            fVar.b();
        }
        if (this.c != null) {
            fVar.a(p);
            fVar.a(this.c);
            fVar.b();
        }
        if (this.d != null) {
            fVar.a(q);
            fVar.a(this.d);
            fVar.b();
        }
        if (this.e != null) {
            fVar.a(r);
            fVar.a(this.e);
            fVar.b();
        }
        if (this.f != null && g()) {
            fVar.a(s);
            this.f.b(fVar);
            fVar.b();
        }
        fVar.a(t);
        fVar.a(this.g);
        fVar.b();
        if (this.h != null && i()) {
            fVar.a(u);
            fVar.a(this.h);
            fVar.b();
        }
        if (this.i != null && j()) {
            fVar.a(v);
            fVar.a(this.i);
            fVar.b();
        }
        if (this.j != null && l()) {
            fVar.a(w);
            fVar.a(new org.apache.a.b.d((byte) 11, this.j.size()));
            for (String a2 : this.j) {
                fVar.a(a2);
            }
            fVar.e();
            fVar.b();
        }
        if (this.k != null && n()) {
            fVar.a(x);
            fVar.a(this.k);
            fVar.b();
        }
        fVar.c();
        fVar.a();
    }

    public boolean b() {
        return this.b != null;
    }

    public boolean c() {
        return this.c != null;
    }

    public boolean d() {
        return this.d != null;
    }

    public String e() {
        return this.e;
    }

    public boolean equals(Object obj) {
        if (obj != null && (obj instanceof g)) {
            return a((g) obj);
        }
        return false;
    }

    public boolean f() {
        return this.e != null;
    }

    public boolean g() {
        return this.f != null;
    }

    public boolean h() {
        return this.y.get(0);
    }

    public int hashCode() {
        return 0;
    }

    public boolean i() {
        return this.h != null;
    }

    public boolean j() {
        return this.i != null;
    }

    public List<String> k() {
        return this.j;
    }

    public boolean l() {
        return this.j != null;
    }

    public String m() {
        return this.k;
    }

    public boolean n() {
        return this.k != null;
    }

    public void o() {
        if (this.c == null) {
            throw new org.apache.a.b.g("Required field 'id' was not present! Struct: " + toString());
        } else if (this.d == null) {
            throw new org.apache.a.b.g("Required field 'appId' was not present! Struct: " + toString());
        } else if (this.e == null) {
            throw new org.apache.a.b.g("Required field 'cmdName' was not present! Struct: " + toString());
        }
    }

    public String toString() {
        boolean z = false;
        StringBuilder sb = new StringBuilder("XmPushActionCommandResult(");
        boolean z2 = true;
        if (a()) {
            sb.append("debug:");
            if (this.f2421a == null) {
                sb.append("null");
            } else {
                sb.append(this.f2421a);
            }
            z2 = false;
        }
        if (b()) {
            if (!z2) {
                sb.append(", ");
            }
            sb.append("target:");
            if (this.b == null) {
                sb.append("null");
            } else {
                sb.append(this.b);
            }
        } else {
            z = z2;
        }
        if (!z) {
            sb.append(", ");
        }
        sb.append("id:");
        if (this.c == null) {
            sb.append("null");
        } else {
            sb.append(this.c);
        }
        sb.append(", ");
        sb.append("appId:");
        if (this.d == null) {
            sb.append("null");
        } else {
            sb.append(this.d);
        }
        sb.append(", ");
        sb.append("cmdName:");
        if (this.e == null) {
            sb.append("null");
        } else {
            sb.append(this.e);
        }
        if (g()) {
            sb.append(", ");
            sb.append("request:");
            if (this.f == null) {
                sb.append("null");
            } else {
                sb.append(this.f);
            }
        }
        sb.append(", ");
        sb.append("errorCode:");
        sb.append(this.g);
        if (i()) {
            sb.append(", ");
            sb.append("reason:");
            if (this.h == null) {
                sb.append("null");
            } else {
                sb.append(this.h);
            }
        }
        if (j()) {
            sb.append(", ");
            sb.append("packageName:");
            if (this.i == null) {
                sb.append("null");
            } else {
                sb.append(this.i);
            }
        }
        if (l()) {
            sb.append(", ");
            sb.append("cmdArgs:");
            if (this.j == null) {
                sb.append("null");
            } else {
                sb.append(this.j);
            }
        }
        if (n()) {
            sb.append(", ");
            sb.append("category:");
            if (this.k == null) {
                sb.append("null");
            } else {
                sb.append(this.k);
            }
        }
        sb.append(")");
        return sb.toString();
    }
}
