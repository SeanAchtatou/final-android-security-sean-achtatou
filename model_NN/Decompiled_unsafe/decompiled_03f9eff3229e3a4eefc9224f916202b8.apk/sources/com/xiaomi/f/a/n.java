package com.xiaomi.f.a;

import com.sina.weibo.sdk.component.WidgetRequestParam;
import java.io.Serializable;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.apache.a.a.e;
import org.apache.a.a.g;
import org.apache.a.b;
import org.apache.a.b.c;
import org.apache.a.b.f;
import org.apache.a.b.i;
import org.apache.a.b.k;

public class n implements Serializable, Cloneable, b<n, a> {
    public static final Map<a, org.apache.a.a.b> m;
    private static final k n = new k("XmPushActionSendMessage");
    private static final c o = new c("debug", (byte) 11, 1);
    private static final c p = new c("target", (byte) 12, 2);
    private static final c q = new c("id", (byte) 11, 3);
    private static final c r = new c("appId", (byte) 11, 4);
    private static final c s = new c("packageName", (byte) 11, 5);
    private static final c t = new c("topic", (byte) 11, 6);
    private static final c u = new c("aliasName", (byte) 11, 7);
    private static final c v = new c(com.baidu.mobads.openad.d.b.EVENT_MESSAGE, (byte) 12, 8);
    private static final c w = new c("needAck", (byte) 2, 9);
    private static final c x = new c("params", (byte) 13, 10);
    private static final c y = new c(WidgetRequestParam.REQ_PARAM_COMMENT_CATEGORY, (byte) 11, 11);
    private static final c z = new c("userAccount", (byte) 11, 12);
    private BitSet A = new BitSet(1);

    /* renamed from: a  reason: collision with root package name */
    public String f2435a;
    public d b;
    public String c;
    public String d;
    public String e;
    public String f;
    public String g;
    public b h;
    public boolean i = true;
    public Map<String, String> j;
    public String k;
    public String l;

    public enum a {
        DEBUG(1, "debug"),
        TARGET(2, "target"),
        ID(3, "id"),
        APP_ID(4, "appId"),
        PACKAGE_NAME(5, "packageName"),
        TOPIC(6, "topic"),
        ALIAS_NAME(7, "aliasName"),
        MESSAGE(8, com.baidu.mobads.openad.d.b.EVENT_MESSAGE),
        NEED_ACK(9, "needAck"),
        PARAMS(10, "params"),
        CATEGORY(11, WidgetRequestParam.REQ_PARAM_COMMENT_CATEGORY),
        USER_ACCOUNT(12, "userAccount");
        
        private static final Map<String, a> m = new HashMap();
        private final short n;
        private final String o;

        static {
            Iterator it = EnumSet.allOf(a.class).iterator();
            while (it.hasNext()) {
                a aVar = (a) it.next();
                m.put(aVar.a(), aVar);
            }
        }

        private a(short s, String str) {
            this.n = s;
            this.o = str;
        }

        public String a() {
            return this.o;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [com.xiaomi.f.a.n$a, org.apache.a.a.b]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        EnumMap enumMap = new EnumMap(a.class);
        enumMap.put((Object) a.DEBUG, (Object) new org.apache.a.a.b("debug", (byte) 2, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.TARGET, (Object) new org.apache.a.a.b("target", (byte) 2, new g((byte) 12, d.class)));
        enumMap.put((Object) a.ID, (Object) new org.apache.a.a.b("id", (byte) 1, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.APP_ID, (Object) new org.apache.a.a.b("appId", (byte) 1, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.PACKAGE_NAME, (Object) new org.apache.a.a.b("packageName", (byte) 2, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.TOPIC, (Object) new org.apache.a.a.b("topic", (byte) 2, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.ALIAS_NAME, (Object) new org.apache.a.a.b("aliasName", (byte) 2, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.MESSAGE, (Object) new org.apache.a.a.b(com.baidu.mobads.openad.d.b.EVENT_MESSAGE, (byte) 2, new g((byte) 12, b.class)));
        enumMap.put((Object) a.NEED_ACK, (Object) new org.apache.a.a.b("needAck", (byte) 2, new org.apache.a.a.c((byte) 2)));
        enumMap.put((Object) a.PARAMS, (Object) new org.apache.a.a.b("params", (byte) 2, new e((byte) 13, new org.apache.a.a.c((byte) 11), new org.apache.a.a.c((byte) 11))));
        enumMap.put((Object) a.CATEGORY, (Object) new org.apache.a.a.b(WidgetRequestParam.REQ_PARAM_COMMENT_CATEGORY, (byte) 2, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.USER_ACCOUNT, (Object) new org.apache.a.a.b("userAccount", (byte) 2, new org.apache.a.a.c((byte) 11)));
        m = Collections.unmodifiableMap(enumMap);
        org.apache.a.a.b.a(n.class, m);
    }

    public void a(f fVar) {
        fVar.g();
        while (true) {
            c i2 = fVar.i();
            if (i2.b == 0) {
                fVar.h();
                t();
                return;
            }
            switch (i2.c) {
                case 1:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.f2435a = fVar.w();
                        break;
                    }
                case 2:
                    if (i2.b != 12) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.b = new d();
                        this.b.a(fVar);
                        break;
                    }
                case 3:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.c = fVar.w();
                        break;
                    }
                case 4:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.d = fVar.w();
                        break;
                    }
                case 5:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.e = fVar.w();
                        break;
                    }
                case 6:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.f = fVar.w();
                        break;
                    }
                case 7:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.g = fVar.w();
                        break;
                    }
                case 8:
                    if (i2.b != 12) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.h = new b();
                        this.h.a(fVar);
                        break;
                    }
                case 9:
                    if (i2.b != 2) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.i = fVar.q();
                        a(true);
                        break;
                    }
                case 10:
                    if (i2.b != 13) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        org.apache.a.b.e k2 = fVar.k();
                        this.j = new HashMap(k2.c * 2);
                        for (int i3 = 0; i3 < k2.c; i3++) {
                            this.j.put(fVar.w(), fVar.w());
                        }
                        fVar.l();
                        break;
                    }
                case 11:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.k = fVar.w();
                        break;
                    }
                case 12:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.l = fVar.w();
                        break;
                    }
                default:
                    i.a(fVar, i2.b);
                    break;
            }
            fVar.j();
        }
    }

    public void a(boolean z2) {
        this.A.set(0, z2);
    }

    public boolean a() {
        return this.f2435a != null;
    }

    public boolean a(n nVar) {
        if (nVar == null) {
            return false;
        }
        boolean a2 = a();
        boolean a3 = nVar.a();
        if ((a2 || a3) && (!a2 || !a3 || !this.f2435a.equals(nVar.f2435a))) {
            return false;
        }
        boolean b2 = b();
        boolean b3 = nVar.b();
        if ((b2 || b3) && (!b2 || !b3 || !this.b.a(nVar.b))) {
            return false;
        }
        boolean d2 = d();
        boolean d3 = nVar.d();
        if ((d2 || d3) && (!d2 || !d3 || !this.c.equals(nVar.c))) {
            return false;
        }
        boolean f2 = f();
        boolean f3 = nVar.f();
        if ((f2 || f3) && (!f2 || !f3 || !this.d.equals(nVar.d))) {
            return false;
        }
        boolean g2 = g();
        boolean g3 = nVar.g();
        if ((g2 || g3) && (!g2 || !g3 || !this.e.equals(nVar.e))) {
            return false;
        }
        boolean i2 = i();
        boolean i3 = nVar.i();
        if ((i2 || i3) && (!i2 || !i3 || !this.f.equals(nVar.f))) {
            return false;
        }
        boolean k2 = k();
        boolean k3 = nVar.k();
        if ((k2 || k3) && (!k2 || !k3 || !this.g.equals(nVar.g))) {
            return false;
        }
        boolean m2 = m();
        boolean m3 = nVar.m();
        if ((m2 || m3) && (!m2 || !m3 || !this.h.a(nVar.h))) {
            return false;
        }
        boolean n2 = n();
        boolean n3 = nVar.n();
        if ((n2 || n3) && (!n2 || !n3 || this.i != nVar.i)) {
            return false;
        }
        boolean o2 = o();
        boolean o3 = nVar.o();
        if ((o2 || o3) && (!o2 || !o3 || !this.j.equals(nVar.j))) {
            return false;
        }
        boolean q2 = q();
        boolean q3 = nVar.q();
        if ((q2 || q3) && (!q2 || !q3 || !this.k.equals(nVar.k))) {
            return false;
        }
        boolean s2 = s();
        boolean s3 = nVar.s();
        return (!s2 && !s3) || (s2 && s3 && this.l.equals(nVar.l));
    }

    /* renamed from: b */
    public int compareTo(n nVar) {
        int a2;
        int a3;
        int a4;
        int a5;
        int a6;
        int a7;
        int a8;
        int a9;
        int a10;
        int a11;
        int a12;
        int a13;
        if (!getClass().equals(nVar.getClass())) {
            return getClass().getName().compareTo(nVar.getClass().getName());
        }
        int compareTo = Boolean.valueOf(a()).compareTo(Boolean.valueOf(nVar.a()));
        if (compareTo != 0) {
            return compareTo;
        }
        if (a() && (a13 = org.apache.a.c.a(this.f2435a, nVar.f2435a)) != 0) {
            return a13;
        }
        int compareTo2 = Boolean.valueOf(b()).compareTo(Boolean.valueOf(nVar.b()));
        if (compareTo2 != 0) {
            return compareTo2;
        }
        if (b() && (a12 = org.apache.a.c.a(this.b, nVar.b)) != 0) {
            return a12;
        }
        int compareTo3 = Boolean.valueOf(d()).compareTo(Boolean.valueOf(nVar.d()));
        if (compareTo3 != 0) {
            return compareTo3;
        }
        if (d() && (a11 = org.apache.a.c.a(this.c, nVar.c)) != 0) {
            return a11;
        }
        int compareTo4 = Boolean.valueOf(f()).compareTo(Boolean.valueOf(nVar.f()));
        if (compareTo4 != 0) {
            return compareTo4;
        }
        if (f() && (a10 = org.apache.a.c.a(this.d, nVar.d)) != 0) {
            return a10;
        }
        int compareTo5 = Boolean.valueOf(g()).compareTo(Boolean.valueOf(nVar.g()));
        if (compareTo5 != 0) {
            return compareTo5;
        }
        if (g() && (a9 = org.apache.a.c.a(this.e, nVar.e)) != 0) {
            return a9;
        }
        int compareTo6 = Boolean.valueOf(i()).compareTo(Boolean.valueOf(nVar.i()));
        if (compareTo6 != 0) {
            return compareTo6;
        }
        if (i() && (a8 = org.apache.a.c.a(this.f, nVar.f)) != 0) {
            return a8;
        }
        int compareTo7 = Boolean.valueOf(k()).compareTo(Boolean.valueOf(nVar.k()));
        if (compareTo7 != 0) {
            return compareTo7;
        }
        if (k() && (a7 = org.apache.a.c.a(this.g, nVar.g)) != 0) {
            return a7;
        }
        int compareTo8 = Boolean.valueOf(m()).compareTo(Boolean.valueOf(nVar.m()));
        if (compareTo8 != 0) {
            return compareTo8;
        }
        if (m() && (a6 = org.apache.a.c.a(this.h, nVar.h)) != 0) {
            return a6;
        }
        int compareTo9 = Boolean.valueOf(n()).compareTo(Boolean.valueOf(nVar.n()));
        if (compareTo9 != 0) {
            return compareTo9;
        }
        if (n() && (a5 = org.apache.a.c.a(this.i, nVar.i)) != 0) {
            return a5;
        }
        int compareTo10 = Boolean.valueOf(o()).compareTo(Boolean.valueOf(nVar.o()));
        if (compareTo10 != 0) {
            return compareTo10;
        }
        if (o() && (a4 = org.apache.a.c.a(this.j, nVar.j)) != 0) {
            return a4;
        }
        int compareTo11 = Boolean.valueOf(q()).compareTo(Boolean.valueOf(nVar.q()));
        if (compareTo11 != 0) {
            return compareTo11;
        }
        if (q() && (a3 = org.apache.a.c.a(this.k, nVar.k)) != 0) {
            return a3;
        }
        int compareTo12 = Boolean.valueOf(s()).compareTo(Boolean.valueOf(nVar.s()));
        if (compareTo12 != 0) {
            return compareTo12;
        }
        if (!s() || (a2 = org.apache.a.c.a(this.l, nVar.l)) == 0) {
            return 0;
        }
        return a2;
    }

    public void b(f fVar) {
        t();
        fVar.a(n);
        if (this.f2435a != null && a()) {
            fVar.a(o);
            fVar.a(this.f2435a);
            fVar.b();
        }
        if (this.b != null && b()) {
            fVar.a(p);
            this.b.b(fVar);
            fVar.b();
        }
        if (this.c != null) {
            fVar.a(q);
            fVar.a(this.c);
            fVar.b();
        }
        if (this.d != null) {
            fVar.a(r);
            fVar.a(this.d);
            fVar.b();
        }
        if (this.e != null && g()) {
            fVar.a(s);
            fVar.a(this.e);
            fVar.b();
        }
        if (this.f != null && i()) {
            fVar.a(t);
            fVar.a(this.f);
            fVar.b();
        }
        if (this.g != null && k()) {
            fVar.a(u);
            fVar.a(this.g);
            fVar.b();
        }
        if (this.h != null && m()) {
            fVar.a(v);
            this.h.b(fVar);
            fVar.b();
        }
        if (n()) {
            fVar.a(w);
            fVar.a(this.i);
            fVar.b();
        }
        if (this.j != null && o()) {
            fVar.a(x);
            fVar.a(new org.apache.a.b.e((byte) 11, (byte) 11, this.j.size()));
            for (Map.Entry next : this.j.entrySet()) {
                fVar.a((String) next.getKey());
                fVar.a((String) next.getValue());
            }
            fVar.d();
            fVar.b();
        }
        if (this.k != null && q()) {
            fVar.a(y);
            fVar.a(this.k);
            fVar.b();
        }
        if (this.l != null && s()) {
            fVar.a(z);
            fVar.a(this.l);
            fVar.b();
        }
        fVar.c();
        fVar.a();
    }

    public boolean b() {
        return this.b != null;
    }

    public String c() {
        return this.c;
    }

    public boolean d() {
        return this.c != null;
    }

    public String e() {
        return this.d;
    }

    public boolean equals(Object obj) {
        if (obj != null && (obj instanceof n)) {
            return a((n) obj);
        }
        return false;
    }

    public boolean f() {
        return this.d != null;
    }

    public boolean g() {
        return this.e != null;
    }

    public String h() {
        return this.f;
    }

    public int hashCode() {
        return 0;
    }

    public boolean i() {
        return this.f != null;
    }

    public String j() {
        return this.g;
    }

    public boolean k() {
        return this.g != null;
    }

    public b l() {
        return this.h;
    }

    public boolean m() {
        return this.h != null;
    }

    public boolean n() {
        return this.A.get(0);
    }

    public boolean o() {
        return this.j != null;
    }

    public String p() {
        return this.k;
    }

    public boolean q() {
        return this.k != null;
    }

    public String r() {
        return this.l;
    }

    public boolean s() {
        return this.l != null;
    }

    public void t() {
        if (this.c == null) {
            throw new org.apache.a.b.g("Required field 'id' was not present! Struct: " + toString());
        } else if (this.d == null) {
            throw new org.apache.a.b.g("Required field 'appId' was not present! Struct: " + toString());
        }
    }

    public String toString() {
        boolean z2 = false;
        StringBuilder sb = new StringBuilder("XmPushActionSendMessage(");
        boolean z3 = true;
        if (a()) {
            sb.append("debug:");
            if (this.f2435a == null) {
                sb.append("null");
            } else {
                sb.append(this.f2435a);
            }
            z3 = false;
        }
        if (b()) {
            if (!z3) {
                sb.append(", ");
            }
            sb.append("target:");
            if (this.b == null) {
                sb.append("null");
            } else {
                sb.append(this.b);
            }
        } else {
            z2 = z3;
        }
        if (!z2) {
            sb.append(", ");
        }
        sb.append("id:");
        if (this.c == null) {
            sb.append("null");
        } else {
            sb.append(this.c);
        }
        sb.append(", ");
        sb.append("appId:");
        if (this.d == null) {
            sb.append("null");
        } else {
            sb.append(this.d);
        }
        if (g()) {
            sb.append(", ");
            sb.append("packageName:");
            if (this.e == null) {
                sb.append("null");
            } else {
                sb.append(this.e);
            }
        }
        if (i()) {
            sb.append(", ");
            sb.append("topic:");
            if (this.f == null) {
                sb.append("null");
            } else {
                sb.append(this.f);
            }
        }
        if (k()) {
            sb.append(", ");
            sb.append("aliasName:");
            if (this.g == null) {
                sb.append("null");
            } else {
                sb.append(this.g);
            }
        }
        if (m()) {
            sb.append(", ");
            sb.append("message:");
            if (this.h == null) {
                sb.append("null");
            } else {
                sb.append(this.h);
            }
        }
        if (n()) {
            sb.append(", ");
            sb.append("needAck:");
            sb.append(this.i);
        }
        if (o()) {
            sb.append(", ");
            sb.append("params:");
            if (this.j == null) {
                sb.append("null");
            } else {
                sb.append(this.j);
            }
        }
        if (q()) {
            sb.append(", ");
            sb.append("category:");
            if (this.k == null) {
                sb.append("null");
            } else {
                sb.append(this.k);
            }
        }
        if (s()) {
            sb.append(", ");
            sb.append("userAccount:");
            if (this.l == null) {
                sb.append("null");
            } else {
                sb.append(this.l);
            }
        }
        sb.append(")");
        return sb.toString();
    }
}
