package com.xiaomi.f.a;

import com.sina.weibo.sdk.component.WidgetRequestParam;
import java.io.Serializable;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.apache.a.a.e;
import org.apache.a.a.g;
import org.apache.a.b;
import org.apache.a.b.c;
import org.apache.a.b.f;
import org.apache.a.b.k;

public class i implements Serializable, Cloneable, b<i, a> {
    public static final Map<a, org.apache.a.a.b> k;
    private static final k l = new k("XmPushActionNotification");
    private static final c m = new c("debug", (byte) 11, 1);
    private static final c n = new c("target", (byte) 12, 2);
    private static final c o = new c("id", (byte) 11, 3);
    private static final c p = new c("appId", (byte) 11, 4);
    private static final c q = new c("type", (byte) 11, 5);
    private static final c r = new c("requireAck", (byte) 2, 6);
    private static final c s = new c("payload", (byte) 11, 7);
    private static final c t = new c("extra", (byte) 13, 8);
    private static final c u = new c("packageName", (byte) 11, 9);
    private static final c v = new c(WidgetRequestParam.REQ_PARAM_COMMENT_CATEGORY, (byte) 11, 10);

    /* renamed from: a  reason: collision with root package name */
    public String f2425a;
    public d b;
    public String c;
    public String d;
    public String e;
    public boolean f = true;
    public String g;
    public Map<String, String> h;
    public String i;
    public String j;
    private BitSet w = new BitSet(1);

    public enum a {
        DEBUG(1, "debug"),
        TARGET(2, "target"),
        ID(3, "id"),
        APP_ID(4, "appId"),
        TYPE(5, "type"),
        REQUIRE_ACK(6, "requireAck"),
        PAYLOAD(7, "payload"),
        EXTRA(8, "extra"),
        PACKAGE_NAME(9, "packageName"),
        CATEGORY(10, WidgetRequestParam.REQ_PARAM_COMMENT_CATEGORY);
        
        private static final Map<String, a> k = new HashMap();
        private final short l;
        private final String m;

        static {
            Iterator it = EnumSet.allOf(a.class).iterator();
            while (it.hasNext()) {
                a aVar = (a) it.next();
                k.put(aVar.a(), aVar);
            }
        }

        private a(short s, String str) {
            this.l = s;
            this.m = str;
        }

        public String a() {
            return this.m;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [com.xiaomi.f.a.i$a, org.apache.a.a.b]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        EnumMap enumMap = new EnumMap(a.class);
        enumMap.put((Object) a.DEBUG, (Object) new org.apache.a.a.b("debug", (byte) 2, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.TARGET, (Object) new org.apache.a.a.b("target", (byte) 2, new g((byte) 12, d.class)));
        enumMap.put((Object) a.ID, (Object) new org.apache.a.a.b("id", (byte) 1, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.APP_ID, (Object) new org.apache.a.a.b("appId", (byte) 2, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.TYPE, (Object) new org.apache.a.a.b("type", (byte) 2, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.REQUIRE_ACK, (Object) new org.apache.a.a.b("requireAck", (byte) 1, new org.apache.a.a.c((byte) 2)));
        enumMap.put((Object) a.PAYLOAD, (Object) new org.apache.a.a.b("payload", (byte) 2, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.EXTRA, (Object) new org.apache.a.a.b("extra", (byte) 2, new e((byte) 13, new org.apache.a.a.c((byte) 11), new org.apache.a.a.c((byte) 11))));
        enumMap.put((Object) a.PACKAGE_NAME, (Object) new org.apache.a.a.b("packageName", (byte) 2, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.CATEGORY, (Object) new org.apache.a.a.b(WidgetRequestParam.REQ_PARAM_COMMENT_CATEGORY, (byte) 2, new org.apache.a.a.c((byte) 11)));
        k = Collections.unmodifiableMap(enumMap);
        org.apache.a.a.b.a(i.class, k);
    }

    public i a(String str) {
        this.c = str;
        return this;
    }

    public i a(Map<String, String> map) {
        this.h = map;
        return this;
    }

    public i a(boolean z) {
        this.f = z;
        b(true);
        return this;
    }

    public void a(f fVar) {
        fVar.g();
        while (true) {
            c i2 = fVar.i();
            if (i2.b == 0) {
                fVar.h();
                if (!f()) {
                    throw new org.apache.a.b.g("Required field 'requireAck' was not found in serialized data! Struct: " + toString());
                }
                l();
                return;
            }
            switch (i2.c) {
                case 1:
                    if (i2.b != 11) {
                        org.apache.a.b.i.a(fVar, i2.b);
                        break;
                    } else {
                        this.f2425a = fVar.w();
                        break;
                    }
                case 2:
                    if (i2.b != 12) {
                        org.apache.a.b.i.a(fVar, i2.b);
                        break;
                    } else {
                        this.b = new d();
                        this.b.a(fVar);
                        break;
                    }
                case 3:
                    if (i2.b != 11) {
                        org.apache.a.b.i.a(fVar, i2.b);
                        break;
                    } else {
                        this.c = fVar.w();
                        break;
                    }
                case 4:
                    if (i2.b != 11) {
                        org.apache.a.b.i.a(fVar, i2.b);
                        break;
                    } else {
                        this.d = fVar.w();
                        break;
                    }
                case 5:
                    if (i2.b != 11) {
                        org.apache.a.b.i.a(fVar, i2.b);
                        break;
                    } else {
                        this.e = fVar.w();
                        break;
                    }
                case 6:
                    if (i2.b != 2) {
                        org.apache.a.b.i.a(fVar, i2.b);
                        break;
                    } else {
                        this.f = fVar.q();
                        b(true);
                        break;
                    }
                case 7:
                    if (i2.b != 11) {
                        org.apache.a.b.i.a(fVar, i2.b);
                        break;
                    } else {
                        this.g = fVar.w();
                        break;
                    }
                case 8:
                    if (i2.b != 13) {
                        org.apache.a.b.i.a(fVar, i2.b);
                        break;
                    } else {
                        org.apache.a.b.e k2 = fVar.k();
                        this.h = new HashMap(k2.c * 2);
                        for (int i3 = 0; i3 < k2.c; i3++) {
                            this.h.put(fVar.w(), fVar.w());
                        }
                        fVar.l();
                        break;
                    }
                case 9:
                    if (i2.b != 11) {
                        org.apache.a.b.i.a(fVar, i2.b);
                        break;
                    } else {
                        this.i = fVar.w();
                        break;
                    }
                case 10:
                    if (i2.b != 11) {
                        org.apache.a.b.i.a(fVar, i2.b);
                        break;
                    } else {
                        this.j = fVar.w();
                        break;
                    }
                default:
                    org.apache.a.b.i.a(fVar, i2.b);
                    break;
            }
            fVar.j();
        }
    }

    public boolean a() {
        return this.f2425a != null;
    }

    public boolean a(i iVar) {
        if (iVar == null) {
            return false;
        }
        boolean a2 = a();
        boolean a3 = iVar.a();
        if ((a2 || a3) && (!a2 || !a3 || !this.f2425a.equals(iVar.f2425a))) {
            return false;
        }
        boolean b2 = b();
        boolean b3 = iVar.b();
        if ((b2 || b3) && (!b2 || !b3 || !this.b.a(iVar.b))) {
            return false;
        }
        boolean c2 = c();
        boolean c3 = iVar.c();
        if ((c2 || c3) && (!c2 || !c3 || !this.c.equals(iVar.c))) {
            return false;
        }
        boolean d2 = d();
        boolean d3 = iVar.d();
        if ((d2 || d3) && (!d2 || !d3 || !this.d.equals(iVar.d))) {
            return false;
        }
        boolean e2 = e();
        boolean e3 = iVar.e();
        if (((e2 || e3) && (!e2 || !e3 || !this.e.equals(iVar.e))) || this.f != iVar.f) {
            return false;
        }
        boolean g2 = g();
        boolean g3 = iVar.g();
        if ((g2 || g3) && (!g2 || !g3 || !this.g.equals(iVar.g))) {
            return false;
        }
        boolean i2 = i();
        boolean i3 = iVar.i();
        if ((i2 || i3) && (!i2 || !i3 || !this.h.equals(iVar.h))) {
            return false;
        }
        boolean j2 = j();
        boolean j3 = iVar.j();
        if ((j2 || j3) && (!j2 || !j3 || !this.i.equals(iVar.i))) {
            return false;
        }
        boolean k2 = k();
        boolean k3 = iVar.k();
        return (!k2 && !k3) || (k2 && k3 && this.j.equals(iVar.j));
    }

    /* renamed from: b */
    public int compareTo(i iVar) {
        int a2;
        int a3;
        int a4;
        int a5;
        int a6;
        int a7;
        int a8;
        int a9;
        int a10;
        int a11;
        if (!getClass().equals(iVar.getClass())) {
            return getClass().getName().compareTo(iVar.getClass().getName());
        }
        int compareTo = Boolean.valueOf(a()).compareTo(Boolean.valueOf(iVar.a()));
        if (compareTo != 0) {
            return compareTo;
        }
        if (a() && (a11 = org.apache.a.c.a(this.f2425a, iVar.f2425a)) != 0) {
            return a11;
        }
        int compareTo2 = Boolean.valueOf(b()).compareTo(Boolean.valueOf(iVar.b()));
        if (compareTo2 != 0) {
            return compareTo2;
        }
        if (b() && (a10 = org.apache.a.c.a(this.b, iVar.b)) != 0) {
            return a10;
        }
        int compareTo3 = Boolean.valueOf(c()).compareTo(Boolean.valueOf(iVar.c()));
        if (compareTo3 != 0) {
            return compareTo3;
        }
        if (c() && (a9 = org.apache.a.c.a(this.c, iVar.c)) != 0) {
            return a9;
        }
        int compareTo4 = Boolean.valueOf(d()).compareTo(Boolean.valueOf(iVar.d()));
        if (compareTo4 != 0) {
            return compareTo4;
        }
        if (d() && (a8 = org.apache.a.c.a(this.d, iVar.d)) != 0) {
            return a8;
        }
        int compareTo5 = Boolean.valueOf(e()).compareTo(Boolean.valueOf(iVar.e()));
        if (compareTo5 != 0) {
            return compareTo5;
        }
        if (e() && (a7 = org.apache.a.c.a(this.e, iVar.e)) != 0) {
            return a7;
        }
        int compareTo6 = Boolean.valueOf(f()).compareTo(Boolean.valueOf(iVar.f()));
        if (compareTo6 != 0) {
            return compareTo6;
        }
        if (f() && (a6 = org.apache.a.c.a(this.f, iVar.f)) != 0) {
            return a6;
        }
        int compareTo7 = Boolean.valueOf(g()).compareTo(Boolean.valueOf(iVar.g()));
        if (compareTo7 != 0) {
            return compareTo7;
        }
        if (g() && (a5 = org.apache.a.c.a(this.g, iVar.g)) != 0) {
            return a5;
        }
        int compareTo8 = Boolean.valueOf(i()).compareTo(Boolean.valueOf(iVar.i()));
        if (compareTo8 != 0) {
            return compareTo8;
        }
        if (i() && (a4 = org.apache.a.c.a(this.h, iVar.h)) != 0) {
            return a4;
        }
        int compareTo9 = Boolean.valueOf(j()).compareTo(Boolean.valueOf(iVar.j()));
        if (compareTo9 != 0) {
            return compareTo9;
        }
        if (j() && (a3 = org.apache.a.c.a(this.i, iVar.i)) != 0) {
            return a3;
        }
        int compareTo10 = Boolean.valueOf(k()).compareTo(Boolean.valueOf(iVar.k()));
        if (compareTo10 != 0) {
            return compareTo10;
        }
        if (!k() || (a2 = org.apache.a.c.a(this.j, iVar.j)) == 0) {
            return 0;
        }
        return a2;
    }

    public i b(String str) {
        this.d = str;
        return this;
    }

    public void b(f fVar) {
        l();
        fVar.a(l);
        if (this.f2425a != null && a()) {
            fVar.a(m);
            fVar.a(this.f2425a);
            fVar.b();
        }
        if (this.b != null && b()) {
            fVar.a(n);
            this.b.b(fVar);
            fVar.b();
        }
        if (this.c != null) {
            fVar.a(o);
            fVar.a(this.c);
            fVar.b();
        }
        if (this.d != null && d()) {
            fVar.a(p);
            fVar.a(this.d);
            fVar.b();
        }
        if (this.e != null && e()) {
            fVar.a(q);
            fVar.a(this.e);
            fVar.b();
        }
        fVar.a(r);
        fVar.a(this.f);
        fVar.b();
        if (this.g != null && g()) {
            fVar.a(s);
            fVar.a(this.g);
            fVar.b();
        }
        if (this.h != null && i()) {
            fVar.a(t);
            fVar.a(new org.apache.a.b.e((byte) 11, (byte) 11, this.h.size()));
            for (Map.Entry next : this.h.entrySet()) {
                fVar.a((String) next.getKey());
                fVar.a((String) next.getValue());
            }
            fVar.d();
            fVar.b();
        }
        if (this.i != null && j()) {
            fVar.a(u);
            fVar.a(this.i);
            fVar.b();
        }
        if (this.j != null && k()) {
            fVar.a(v);
            fVar.a(this.j);
            fVar.b();
        }
        fVar.c();
        fVar.a();
    }

    public void b(boolean z) {
        this.w.set(0, z);
    }

    public boolean b() {
        return this.b != null;
    }

    public i c(String str) {
        this.e = str;
        return this;
    }

    public boolean c() {
        return this.c != null;
    }

    public boolean d() {
        return this.d != null;
    }

    public boolean e() {
        return this.e != null;
    }

    public boolean equals(Object obj) {
        if (obj != null && (obj instanceof i)) {
            return a((i) obj);
        }
        return false;
    }

    public boolean f() {
        return this.w.get(0);
    }

    public boolean g() {
        return this.g != null;
    }

    public Map<String, String> h() {
        return this.h;
    }

    public int hashCode() {
        return 0;
    }

    public boolean i() {
        return this.h != null;
    }

    public boolean j() {
        return this.i != null;
    }

    public boolean k() {
        return this.j != null;
    }

    public void l() {
        if (this.c == null) {
            throw new org.apache.a.b.g("Required field 'id' was not present! Struct: " + toString());
        }
    }

    public String toString() {
        boolean z = false;
        StringBuilder sb = new StringBuilder("XmPushActionNotification(");
        boolean z2 = true;
        if (a()) {
            sb.append("debug:");
            if (this.f2425a == null) {
                sb.append("null");
            } else {
                sb.append(this.f2425a);
            }
            z2 = false;
        }
        if (b()) {
            if (!z2) {
                sb.append(", ");
            }
            sb.append("target:");
            if (this.b == null) {
                sb.append("null");
            } else {
                sb.append(this.b);
            }
        } else {
            z = z2;
        }
        if (!z) {
            sb.append(", ");
        }
        sb.append("id:");
        if (this.c == null) {
            sb.append("null");
        } else {
            sb.append(this.c);
        }
        if (d()) {
            sb.append(", ");
            sb.append("appId:");
            if (this.d == null) {
                sb.append("null");
            } else {
                sb.append(this.d);
            }
        }
        if (e()) {
            sb.append(", ");
            sb.append("type:");
            if (this.e == null) {
                sb.append("null");
            } else {
                sb.append(this.e);
            }
        }
        sb.append(", ");
        sb.append("requireAck:");
        sb.append(this.f);
        if (g()) {
            sb.append(", ");
            sb.append("payload:");
            if (this.g == null) {
                sb.append("null");
            } else {
                sb.append(this.g);
            }
        }
        if (i()) {
            sb.append(", ");
            sb.append("extra:");
            if (this.h == null) {
                sb.append("null");
            } else {
                sb.append(this.h);
            }
        }
        if (j()) {
            sb.append(", ");
            sb.append("packageName:");
            if (this.i == null) {
                sb.append("null");
            } else {
                sb.append(this.i);
            }
        }
        if (k()) {
            sb.append(", ");
            sb.append("category:");
            if (this.j == null) {
                sb.append("null");
            } else {
                sb.append(this.j);
            }
        }
        sb.append(")");
        return sb.toString();
    }
}
