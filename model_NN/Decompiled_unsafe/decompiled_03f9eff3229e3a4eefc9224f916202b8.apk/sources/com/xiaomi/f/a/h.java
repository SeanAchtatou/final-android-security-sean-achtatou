package com.xiaomi.f.a;

import com.tencent.tauth.AuthActivity;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.apache.a.a.g;
import org.apache.a.b;
import org.apache.a.b.c;
import org.apache.a.b.f;
import org.apache.a.b.i;
import org.apache.a.b.k;

public class h implements Serializable, Cloneable, b<h, a> {
    public static final Map<a, org.apache.a.a.b> i;
    private static final k j = new k("XmPushActionContainer");
    private static final c k = new c(AuthActivity.ACTION_KEY, (byte) 8, 1);
    private static final c l = new c("encryptAction", (byte) 2, 2);
    private static final c m = new c("isRequest", (byte) 2, 3);
    private static final c n = new c("pushAction", (byte) 11, 4);
    private static final c o = new c("appid", (byte) 11, 5);
    private static final c p = new c("packageName", (byte) 11, 6);
    private static final c q = new c("target", (byte) 12, 7);
    private static final c r = new c("metaInfo", (byte) 12, 8);

    /* renamed from: a  reason: collision with root package name */
    public a f2423a;
    public boolean b = true;
    public boolean c = true;
    public ByteBuffer d;
    public String e;
    public String f;
    public d g;
    public c h;
    private BitSet s = new BitSet(2);

    public enum a {
        ACTION(1, AuthActivity.ACTION_KEY),
        ENCRYPT_ACTION(2, "encryptAction"),
        IS_REQUEST(3, "isRequest"),
        PUSH_ACTION(4, "pushAction"),
        APPID(5, "appid"),
        PACKAGE_NAME(6, "packageName"),
        TARGET(7, "target"),
        META_INFO(8, "metaInfo");
        
        private static final Map<String, a> i = new HashMap();
        private final short j;
        private final String k;

        static {
            Iterator it = EnumSet.allOf(a.class).iterator();
            while (it.hasNext()) {
                a aVar = (a) it.next();
                i.put(aVar.a(), aVar);
            }
        }

        private a(short s, String str) {
            this.j = s;
            this.k = str;
        }

        public String a() {
            return this.k;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [com.xiaomi.f.a.h$a, org.apache.a.a.b]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        EnumMap enumMap = new EnumMap(a.class);
        enumMap.put((Object) a.ACTION, (Object) new org.apache.a.a.b(AuthActivity.ACTION_KEY, (byte) 1, new org.apache.a.a.a((byte) 16, a.class)));
        enumMap.put((Object) a.ENCRYPT_ACTION, (Object) new org.apache.a.a.b("encryptAction", (byte) 1, new org.apache.a.a.c((byte) 2)));
        enumMap.put((Object) a.IS_REQUEST, (Object) new org.apache.a.a.b("isRequest", (byte) 1, new org.apache.a.a.c((byte) 2)));
        enumMap.put((Object) a.PUSH_ACTION, (Object) new org.apache.a.a.b("pushAction", (byte) 1, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.APPID, (Object) new org.apache.a.a.b("appid", (byte) 2, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.PACKAGE_NAME, (Object) new org.apache.a.a.b("packageName", (byte) 2, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.TARGET, (Object) new org.apache.a.a.b("target", (byte) 1, new g((byte) 12, d.class)));
        enumMap.put((Object) a.META_INFO, (Object) new org.apache.a.a.b("metaInfo", (byte) 2, new g((byte) 12, c.class)));
        i = Collections.unmodifiableMap(enumMap);
        org.apache.a.a.b.a(h.class, i);
    }

    public a a() {
        return this.f2423a;
    }

    public h a(a aVar) {
        this.f2423a = aVar;
        return this;
    }

    public h a(c cVar) {
        this.h = cVar;
        return this;
    }

    public h a(d dVar) {
        this.g = dVar;
        return this;
    }

    public h a(String str) {
        this.e = str;
        return this;
    }

    public h a(ByteBuffer byteBuffer) {
        this.d = byteBuffer;
        return this;
    }

    public h a(boolean z) {
        this.b = z;
        b(true);
        return this;
    }

    public void a(f fVar) {
        fVar.g();
        while (true) {
            c i2 = fVar.i();
            if (i2.b == 0) {
                fVar.h();
                if (!d()) {
                    throw new org.apache.a.b.g("Required field 'encryptAction' was not found in serialized data! Struct: " + toString());
                } else if (!e()) {
                    throw new org.apache.a.b.g("Required field 'isRequest' was not found in serialized data! Struct: " + toString());
                } else {
                    o();
                    return;
                }
            } else {
                switch (i2.c) {
                    case 1:
                        if (i2.b != 8) {
                            i.a(fVar, i2.b);
                            break;
                        } else {
                            this.f2423a = a.a(fVar.t());
                            break;
                        }
                    case 2:
                        if (i2.b != 2) {
                            i.a(fVar, i2.b);
                            break;
                        } else {
                            this.b = fVar.q();
                            b(true);
                            break;
                        }
                    case 3:
                        if (i2.b != 2) {
                            i.a(fVar, i2.b);
                            break;
                        } else {
                            this.c = fVar.q();
                            d(true);
                            break;
                        }
                    case 4:
                        if (i2.b != 11) {
                            i.a(fVar, i2.b);
                            break;
                        } else {
                            this.d = fVar.x();
                            break;
                        }
                    case 5:
                        if (i2.b != 11) {
                            i.a(fVar, i2.b);
                            break;
                        } else {
                            this.e = fVar.w();
                            break;
                        }
                    case 6:
                        if (i2.b != 11) {
                            i.a(fVar, i2.b);
                            break;
                        } else {
                            this.f = fVar.w();
                            break;
                        }
                    case 7:
                        if (i2.b != 12) {
                            i.a(fVar, i2.b);
                            break;
                        } else {
                            this.g = new d();
                            this.g.a(fVar);
                            break;
                        }
                    case 8:
                        if (i2.b != 12) {
                            i.a(fVar, i2.b);
                            break;
                        } else {
                            this.h = new c();
                            this.h.a(fVar);
                            break;
                        }
                    default:
                        i.a(fVar, i2.b);
                        break;
                }
                fVar.j();
            }
        }
    }

    public boolean a(h hVar) {
        if (hVar == null) {
            return false;
        }
        boolean b2 = b();
        boolean b3 = hVar.b();
        if (((b2 || b3) && (!b2 || !b3 || !this.f2423a.equals(hVar.f2423a))) || this.b != hVar.b || this.c != hVar.c) {
            return false;
        }
        boolean g2 = g();
        boolean g3 = hVar.g();
        if ((g2 || g3) && (!g2 || !g3 || !this.d.equals(hVar.d))) {
            return false;
        }
        boolean i2 = i();
        boolean i3 = hVar.i();
        if ((i2 || i3) && (!i2 || !i3 || !this.e.equals(hVar.e))) {
            return false;
        }
        boolean k2 = k();
        boolean k3 = hVar.k();
        if ((k2 || k3) && (!k2 || !k3 || !this.f.equals(hVar.f))) {
            return false;
        }
        boolean l2 = l();
        boolean l3 = hVar.l();
        if ((l2 || l3) && (!l2 || !l3 || !this.g.a(hVar.g))) {
            return false;
        }
        boolean n2 = n();
        boolean n3 = hVar.n();
        return (!n2 && !n3) || (n2 && n3 && this.h.a(hVar.h));
    }

    /* renamed from: b */
    public int compareTo(h hVar) {
        int a2;
        int a3;
        int a4;
        int a5;
        int a6;
        int a7;
        int a8;
        int a9;
        if (!getClass().equals(hVar.getClass())) {
            return getClass().getName().compareTo(hVar.getClass().getName());
        }
        int compareTo = Boolean.valueOf(b()).compareTo(Boolean.valueOf(hVar.b()));
        if (compareTo != 0) {
            return compareTo;
        }
        if (b() && (a9 = org.apache.a.c.a(this.f2423a, hVar.f2423a)) != 0) {
            return a9;
        }
        int compareTo2 = Boolean.valueOf(d()).compareTo(Boolean.valueOf(hVar.d()));
        if (compareTo2 != 0) {
            return compareTo2;
        }
        if (d() && (a8 = org.apache.a.c.a(this.b, hVar.b)) != 0) {
            return a8;
        }
        int compareTo3 = Boolean.valueOf(e()).compareTo(Boolean.valueOf(hVar.e()));
        if (compareTo3 != 0) {
            return compareTo3;
        }
        if (e() && (a7 = org.apache.a.c.a(this.c, hVar.c)) != 0) {
            return a7;
        }
        int compareTo4 = Boolean.valueOf(g()).compareTo(Boolean.valueOf(hVar.g()));
        if (compareTo4 != 0) {
            return compareTo4;
        }
        if (g() && (a6 = org.apache.a.c.a(this.d, hVar.d)) != 0) {
            return a6;
        }
        int compareTo5 = Boolean.valueOf(i()).compareTo(Boolean.valueOf(hVar.i()));
        if (compareTo5 != 0) {
            return compareTo5;
        }
        if (i() && (a5 = org.apache.a.c.a(this.e, hVar.e)) != 0) {
            return a5;
        }
        int compareTo6 = Boolean.valueOf(k()).compareTo(Boolean.valueOf(hVar.k()));
        if (compareTo6 != 0) {
            return compareTo6;
        }
        if (k() && (a4 = org.apache.a.c.a(this.f, hVar.f)) != 0) {
            return a4;
        }
        int compareTo7 = Boolean.valueOf(l()).compareTo(Boolean.valueOf(hVar.l()));
        if (compareTo7 != 0) {
            return compareTo7;
        }
        if (l() && (a3 = org.apache.a.c.a(this.g, hVar.g)) != 0) {
            return a3;
        }
        int compareTo8 = Boolean.valueOf(n()).compareTo(Boolean.valueOf(hVar.n()));
        if (compareTo8 != 0) {
            return compareTo8;
        }
        if (!n() || (a2 = org.apache.a.c.a(this.h, hVar.h)) == 0) {
            return 0;
        }
        return a2;
    }

    public h b(String str) {
        this.f = str;
        return this;
    }

    public void b(f fVar) {
        o();
        fVar.a(j);
        if (this.f2423a != null) {
            fVar.a(k);
            fVar.a(this.f2423a.a());
            fVar.b();
        }
        fVar.a(l);
        fVar.a(this.b);
        fVar.b();
        fVar.a(m);
        fVar.a(this.c);
        fVar.b();
        if (this.d != null) {
            fVar.a(n);
            fVar.a(this.d);
            fVar.b();
        }
        if (this.e != null && i()) {
            fVar.a(o);
            fVar.a(this.e);
            fVar.b();
        }
        if (this.f != null && k()) {
            fVar.a(p);
            fVar.a(this.f);
            fVar.b();
        }
        if (this.g != null) {
            fVar.a(q);
            this.g.b(fVar);
            fVar.b();
        }
        if (this.h != null && n()) {
            fVar.a(r);
            this.h.b(fVar);
            fVar.b();
        }
        fVar.c();
        fVar.a();
    }

    public void b(boolean z) {
        this.s.set(0, z);
    }

    public boolean b() {
        return this.f2423a != null;
    }

    public h c(boolean z) {
        this.c = z;
        d(true);
        return this;
    }

    public boolean c() {
        return this.b;
    }

    public void d(boolean z) {
        this.s.set(1, z);
    }

    public boolean d() {
        return this.s.get(0);
    }

    public boolean e() {
        return this.s.get(1);
    }

    public boolean equals(Object obj) {
        if (obj != null && (obj instanceof h)) {
            return a((h) obj);
        }
        return false;
    }

    public byte[] f() {
        a(org.apache.a.c.c(this.d));
        return this.d.array();
    }

    public boolean g() {
        return this.d != null;
    }

    public String h() {
        return this.e;
    }

    public int hashCode() {
        return 0;
    }

    public boolean i() {
        return this.e != null;
    }

    public String j() {
        return this.f;
    }

    public boolean k() {
        return this.f != null;
    }

    public boolean l() {
        return this.g != null;
    }

    public c m() {
        return this.h;
    }

    public boolean n() {
        return this.h != null;
    }

    public void o() {
        if (this.f2423a == null) {
            throw new org.apache.a.b.g("Required field 'action' was not present! Struct: " + toString());
        } else if (this.d == null) {
            throw new org.apache.a.b.g("Required field 'pushAction' was not present! Struct: " + toString());
        } else if (this.g == null) {
            throw new org.apache.a.b.g("Required field 'target' was not present! Struct: " + toString());
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("XmPushActionContainer(");
        sb.append("action:");
        if (this.f2423a == null) {
            sb.append("null");
        } else {
            sb.append(this.f2423a);
        }
        sb.append(", ");
        sb.append("encryptAction:");
        sb.append(this.b);
        sb.append(", ");
        sb.append("isRequest:");
        sb.append(this.c);
        sb.append(", ");
        sb.append("pushAction:");
        if (this.d == null) {
            sb.append("null");
        } else {
            org.apache.a.c.a(this.d, sb);
        }
        if (i()) {
            sb.append(", ");
            sb.append("appid:");
            if (this.e == null) {
                sb.append("null");
            } else {
                sb.append(this.e);
            }
        }
        if (k()) {
            sb.append(", ");
            sb.append("packageName:");
            if (this.f == null) {
                sb.append("null");
            } else {
                sb.append(this.f);
            }
        }
        sb.append(", ");
        sb.append("target:");
        if (this.g == null) {
            sb.append("null");
        } else {
            sb.append(this.g);
        }
        if (n()) {
            sb.append(", ");
            sb.append("metaInfo:");
            if (this.h == null) {
                sb.append("null");
            } else {
                sb.append(this.h);
            }
        }
        sb.append(")");
        return sb.toString();
    }
}
