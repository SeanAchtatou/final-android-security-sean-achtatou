package com.xiaomi.f.a;

import java.io.Serializable;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.apache.a.a.g;
import org.apache.a.b;
import org.apache.a.b.c;
import org.apache.a.b.f;
import org.apache.a.b.i;
import org.apache.a.b.k;

public class j implements Serializable, Cloneable, b<j, a> {
    public static final Map<a, org.apache.a.a.b> k;
    private static final k l = new k("XmPushActionRegistration");
    private static final c m = new c("debug", (byte) 11, 1);
    private static final c n = new c("target", (byte) 12, 2);
    private static final c o = new c("id", (byte) 11, 3);
    private static final c p = new c("appId", (byte) 11, 4);
    private static final c q = new c("appVersion", (byte) 11, 5);
    private static final c r = new c("packageName", (byte) 11, 6);
    private static final c s = new c("token", (byte) 11, 7);
    private static final c t = new c("deviceId", (byte) 11, 8);
    private static final c u = new c("aliasName", (byte) 11, 9);
    private static final c v = new c("sdkVersion", (byte) 11, 10);

    /* renamed from: a  reason: collision with root package name */
    public String f2427a;
    public d b;
    public String c;
    public String d;
    public String e;
    public String f;
    public String g;
    public String h;
    public String i;
    public String j;

    public enum a {
        DEBUG(1, "debug"),
        TARGET(2, "target"),
        ID(3, "id"),
        APP_ID(4, "appId"),
        APP_VERSION(5, "appVersion"),
        PACKAGE_NAME(6, "packageName"),
        TOKEN(7, "token"),
        DEVICE_ID(8, "deviceId"),
        ALIAS_NAME(9, "aliasName"),
        SDK_VERSION(10, "sdkVersion");
        
        private static final Map<String, a> k = new HashMap();
        private final short l;
        private final String m;

        static {
            Iterator it = EnumSet.allOf(a.class).iterator();
            while (it.hasNext()) {
                a aVar = (a) it.next();
                k.put(aVar.a(), aVar);
            }
        }

        private a(short s, String str) {
            this.l = s;
            this.m = str;
        }

        public String a() {
            return this.m;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [com.xiaomi.f.a.j$a, org.apache.a.a.b]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        EnumMap enumMap = new EnumMap(a.class);
        enumMap.put((Object) a.DEBUG, (Object) new org.apache.a.a.b("debug", (byte) 2, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.TARGET, (Object) new org.apache.a.a.b("target", (byte) 2, new g((byte) 12, d.class)));
        enumMap.put((Object) a.ID, (Object) new org.apache.a.a.b("id", (byte) 1, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.APP_ID, (Object) new org.apache.a.a.b("appId", (byte) 1, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.APP_VERSION, (Object) new org.apache.a.a.b("appVersion", (byte) 2, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.PACKAGE_NAME, (Object) new org.apache.a.a.b("packageName", (byte) 2, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.TOKEN, (Object) new org.apache.a.a.b("token", (byte) 1, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.DEVICE_ID, (Object) new org.apache.a.a.b("deviceId", (byte) 2, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.ALIAS_NAME, (Object) new org.apache.a.a.b("aliasName", (byte) 2, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.SDK_VERSION, (Object) new org.apache.a.a.b("sdkVersion", (byte) 2, new org.apache.a.a.c((byte) 11)));
        k = Collections.unmodifiableMap(enumMap);
        org.apache.a.a.b.a(j.class, k);
    }

    public j a(String str) {
        this.c = str;
        return this;
    }

    public void a(f fVar) {
        fVar.g();
        while (true) {
            c i2 = fVar.i();
            if (i2.b == 0) {
                fVar.h();
                m();
                return;
            }
            switch (i2.c) {
                case 1:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.f2427a = fVar.w();
                        break;
                    }
                case 2:
                    if (i2.b != 12) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.b = new d();
                        this.b.a(fVar);
                        break;
                    }
                case 3:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.c = fVar.w();
                        break;
                    }
                case 4:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.d = fVar.w();
                        break;
                    }
                case 5:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.e = fVar.w();
                        break;
                    }
                case 6:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.f = fVar.w();
                        break;
                    }
                case 7:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.g = fVar.w();
                        break;
                    }
                case 8:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.h = fVar.w();
                        break;
                    }
                case 9:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.i = fVar.w();
                        break;
                    }
                case 10:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.j = fVar.w();
                        break;
                    }
                default:
                    i.a(fVar, i2.b);
                    break;
            }
            fVar.j();
        }
    }

    public boolean a() {
        return this.f2427a != null;
    }

    public boolean a(j jVar) {
        if (jVar == null) {
            return false;
        }
        boolean a2 = a();
        boolean a3 = jVar.a();
        if ((a2 || a3) && (!a2 || !a3 || !this.f2427a.equals(jVar.f2427a))) {
            return false;
        }
        boolean b2 = b();
        boolean b3 = jVar.b();
        if ((b2 || b3) && (!b2 || !b3 || !this.b.a(jVar.b))) {
            return false;
        }
        boolean c2 = c();
        boolean c3 = jVar.c();
        if ((c2 || c3) && (!c2 || !c3 || !this.c.equals(jVar.c))) {
            return false;
        }
        boolean e2 = e();
        boolean e3 = jVar.e();
        if ((e2 || e3) && (!e2 || !e3 || !this.d.equals(jVar.d))) {
            return false;
        }
        boolean f2 = f();
        boolean f3 = jVar.f();
        if ((f2 || f3) && (!f2 || !f3 || !this.e.equals(jVar.e))) {
            return false;
        }
        boolean g2 = g();
        boolean g3 = jVar.g();
        if ((g2 || g3) && (!g2 || !g3 || !this.f.equals(jVar.f))) {
            return false;
        }
        boolean i2 = i();
        boolean i3 = jVar.i();
        if ((i2 || i3) && (!i2 || !i3 || !this.g.equals(jVar.g))) {
            return false;
        }
        boolean j2 = j();
        boolean j3 = jVar.j();
        if ((j2 || j3) && (!j2 || !j3 || !this.h.equals(jVar.h))) {
            return false;
        }
        boolean k2 = k();
        boolean k3 = jVar.k();
        if ((k2 || k3) && (!k2 || !k3 || !this.i.equals(jVar.i))) {
            return false;
        }
        boolean l2 = l();
        boolean l3 = jVar.l();
        return (!l2 && !l3) || (l2 && l3 && this.j.equals(jVar.j));
    }

    /* renamed from: b */
    public int compareTo(j jVar) {
        int a2;
        int a3;
        int a4;
        int a5;
        int a6;
        int a7;
        int a8;
        int a9;
        int a10;
        int a11;
        if (!getClass().equals(jVar.getClass())) {
            return getClass().getName().compareTo(jVar.getClass().getName());
        }
        int compareTo = Boolean.valueOf(a()).compareTo(Boolean.valueOf(jVar.a()));
        if (compareTo != 0) {
            return compareTo;
        }
        if (a() && (a11 = org.apache.a.c.a(this.f2427a, jVar.f2427a)) != 0) {
            return a11;
        }
        int compareTo2 = Boolean.valueOf(b()).compareTo(Boolean.valueOf(jVar.b()));
        if (compareTo2 != 0) {
            return compareTo2;
        }
        if (b() && (a10 = org.apache.a.c.a(this.b, jVar.b)) != 0) {
            return a10;
        }
        int compareTo3 = Boolean.valueOf(c()).compareTo(Boolean.valueOf(jVar.c()));
        if (compareTo3 != 0) {
            return compareTo3;
        }
        if (c() && (a9 = org.apache.a.c.a(this.c, jVar.c)) != 0) {
            return a9;
        }
        int compareTo4 = Boolean.valueOf(e()).compareTo(Boolean.valueOf(jVar.e()));
        if (compareTo4 != 0) {
            return compareTo4;
        }
        if (e() && (a8 = org.apache.a.c.a(this.d, jVar.d)) != 0) {
            return a8;
        }
        int compareTo5 = Boolean.valueOf(f()).compareTo(Boolean.valueOf(jVar.f()));
        if (compareTo5 != 0) {
            return compareTo5;
        }
        if (f() && (a7 = org.apache.a.c.a(this.e, jVar.e)) != 0) {
            return a7;
        }
        int compareTo6 = Boolean.valueOf(g()).compareTo(Boolean.valueOf(jVar.g()));
        if (compareTo6 != 0) {
            return compareTo6;
        }
        if (g() && (a6 = org.apache.a.c.a(this.f, jVar.f)) != 0) {
            return a6;
        }
        int compareTo7 = Boolean.valueOf(i()).compareTo(Boolean.valueOf(jVar.i()));
        if (compareTo7 != 0) {
            return compareTo7;
        }
        if (i() && (a5 = org.apache.a.c.a(this.g, jVar.g)) != 0) {
            return a5;
        }
        int compareTo8 = Boolean.valueOf(j()).compareTo(Boolean.valueOf(jVar.j()));
        if (compareTo8 != 0) {
            return compareTo8;
        }
        if (j() && (a4 = org.apache.a.c.a(this.h, jVar.h)) != 0) {
            return a4;
        }
        int compareTo9 = Boolean.valueOf(k()).compareTo(Boolean.valueOf(jVar.k()));
        if (compareTo9 != 0) {
            return compareTo9;
        }
        if (k() && (a3 = org.apache.a.c.a(this.i, jVar.i)) != 0) {
            return a3;
        }
        int compareTo10 = Boolean.valueOf(l()).compareTo(Boolean.valueOf(jVar.l()));
        if (compareTo10 != 0) {
            return compareTo10;
        }
        if (!l() || (a2 = org.apache.a.c.a(this.j, jVar.j)) == 0) {
            return 0;
        }
        return a2;
    }

    public j b(String str) {
        this.d = str;
        return this;
    }

    public void b(f fVar) {
        m();
        fVar.a(l);
        if (this.f2427a != null && a()) {
            fVar.a(m);
            fVar.a(this.f2427a);
            fVar.b();
        }
        if (this.b != null && b()) {
            fVar.a(n);
            this.b.b(fVar);
            fVar.b();
        }
        if (this.c != null) {
            fVar.a(o);
            fVar.a(this.c);
            fVar.b();
        }
        if (this.d != null) {
            fVar.a(p);
            fVar.a(this.d);
            fVar.b();
        }
        if (this.e != null && f()) {
            fVar.a(q);
            fVar.a(this.e);
            fVar.b();
        }
        if (this.f != null && g()) {
            fVar.a(r);
            fVar.a(this.f);
            fVar.b();
        }
        if (this.g != null) {
            fVar.a(s);
            fVar.a(this.g);
            fVar.b();
        }
        if (this.h != null && j()) {
            fVar.a(t);
            fVar.a(this.h);
            fVar.b();
        }
        if (this.i != null && k()) {
            fVar.a(u);
            fVar.a(this.i);
            fVar.b();
        }
        if (this.j != null && l()) {
            fVar.a(v);
            fVar.a(this.j);
            fVar.b();
        }
        fVar.c();
        fVar.a();
    }

    public boolean b() {
        return this.b != null;
    }

    public j c(String str) {
        this.e = str;
        return this;
    }

    public boolean c() {
        return this.c != null;
    }

    public j d(String str) {
        this.f = str;
        return this;
    }

    public String d() {
        return this.d;
    }

    public j e(String str) {
        this.g = str;
        return this;
    }

    public boolean e() {
        return this.d != null;
    }

    public boolean equals(Object obj) {
        if (obj != null && (obj instanceof j)) {
            return a((j) obj);
        }
        return false;
    }

    public j f(String str) {
        this.h = str;
        return this;
    }

    public boolean f() {
        return this.e != null;
    }

    public boolean g() {
        return this.f != null;
    }

    public String h() {
        return this.g;
    }

    public int hashCode() {
        return 0;
    }

    public boolean i() {
        return this.g != null;
    }

    public boolean j() {
        return this.h != null;
    }

    public boolean k() {
        return this.i != null;
    }

    public boolean l() {
        return this.j != null;
    }

    public void m() {
        if (this.c == null) {
            throw new org.apache.a.b.g("Required field 'id' was not present! Struct: " + toString());
        } else if (this.d == null) {
            throw new org.apache.a.b.g("Required field 'appId' was not present! Struct: " + toString());
        } else if (this.g == null) {
            throw new org.apache.a.b.g("Required field 'token' was not present! Struct: " + toString());
        }
    }

    public String toString() {
        boolean z = false;
        StringBuilder sb = new StringBuilder("XmPushActionRegistration(");
        boolean z2 = true;
        if (a()) {
            sb.append("debug:");
            if (this.f2427a == null) {
                sb.append("null");
            } else {
                sb.append(this.f2427a);
            }
            z2 = false;
        }
        if (b()) {
            if (!z2) {
                sb.append(", ");
            }
            sb.append("target:");
            if (this.b == null) {
                sb.append("null");
            } else {
                sb.append(this.b);
            }
        } else {
            z = z2;
        }
        if (!z) {
            sb.append(", ");
        }
        sb.append("id:");
        if (this.c == null) {
            sb.append("null");
        } else {
            sb.append(this.c);
        }
        sb.append(", ");
        sb.append("appId:");
        if (this.d == null) {
            sb.append("null");
        } else {
            sb.append(this.d);
        }
        if (f()) {
            sb.append(", ");
            sb.append("appVersion:");
            if (this.e == null) {
                sb.append("null");
            } else {
                sb.append(this.e);
            }
        }
        if (g()) {
            sb.append(", ");
            sb.append("packageName:");
            if (this.f == null) {
                sb.append("null");
            } else {
                sb.append(this.f);
            }
        }
        sb.append(", ");
        sb.append("token:");
        if (this.g == null) {
            sb.append("null");
        } else {
            sb.append(this.g);
        }
        if (j()) {
            sb.append(", ");
            sb.append("deviceId:");
            if (this.h == null) {
                sb.append("null");
            } else {
                sb.append(this.h);
            }
        }
        if (k()) {
            sb.append(", ");
            sb.append("aliasName:");
            if (this.i == null) {
                sb.append("null");
            } else {
                sb.append(this.i);
            }
        }
        if (l()) {
            sb.append(", ");
            sb.append("sdkVersion:");
            if (this.j == null) {
                sb.append("null");
            } else {
                sb.append(this.j);
            }
        }
        sb.append(")");
        return sb.toString();
    }
}
