package com.xiaomi.f.a;

import com.sina.weibo.sdk.component.WidgetRequestParam;
import com.tencent.open.SocialConstants;
import java.io.Serializable;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.apache.a.a.g;
import org.apache.a.b;
import org.apache.a.b.c;
import org.apache.a.b.f;
import org.apache.a.b.i;
import org.apache.a.b.k;

public class e implements Serializable, Cloneable, b<e, a> {
    public static final Map<a, org.apache.a.a.b> k;
    private static final k l = new k("XmPushActionAckMessage");
    private static final c m = new c("debug", (byte) 11, 1);
    private static final c n = new c("target", (byte) 12, 2);
    private static final c o = new c("id", (byte) 11, 3);
    private static final c p = new c("appId", (byte) 11, 4);
    private static final c q = new c("messageTs", (byte) 10, 5);
    private static final c r = new c("topic", (byte) 11, 6);
    private static final c s = new c("aliasName", (byte) 11, 7);
    private static final c t = new c(SocialConstants.TYPE_REQUEST, (byte) 12, 8);
    private static final c u = new c("packageName", (byte) 11, 9);
    private static final c v = new c(WidgetRequestParam.REQ_PARAM_COMMENT_CATEGORY, (byte) 11, 10);

    /* renamed from: a  reason: collision with root package name */
    public String f2417a;
    public d b;
    public String c;
    public String d;
    public long e;
    public String f;
    public String g;
    public n h;
    public String i;
    public String j;
    private BitSet w = new BitSet(1);

    public enum a {
        DEBUG(1, "debug"),
        TARGET(2, "target"),
        ID(3, "id"),
        APP_ID(4, "appId"),
        MESSAGE_TS(5, "messageTs"),
        TOPIC(6, "topic"),
        ALIAS_NAME(7, "aliasName"),
        REQUEST(8, SocialConstants.TYPE_REQUEST),
        PACKAGE_NAME(9, "packageName"),
        CATEGORY(10, WidgetRequestParam.REQ_PARAM_COMMENT_CATEGORY);
        
        private static final Map<String, a> k = new HashMap();
        private final short l;
        private final String m;

        static {
            Iterator it = EnumSet.allOf(a.class).iterator();
            while (it.hasNext()) {
                a aVar = (a) it.next();
                k.put(aVar.a(), aVar);
            }
        }

        private a(short s, String str) {
            this.l = s;
            this.m = str;
        }

        public String a() {
            return this.m;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [com.xiaomi.f.a.e$a, org.apache.a.a.b]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        EnumMap enumMap = new EnumMap(a.class);
        enumMap.put((Object) a.DEBUG, (Object) new org.apache.a.a.b("debug", (byte) 2, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.TARGET, (Object) new org.apache.a.a.b("target", (byte) 2, new g((byte) 12, d.class)));
        enumMap.put((Object) a.ID, (Object) new org.apache.a.a.b("id", (byte) 1, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.APP_ID, (Object) new org.apache.a.a.b("appId", (byte) 1, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.MESSAGE_TS, (Object) new org.apache.a.a.b("messageTs", (byte) 1, new org.apache.a.a.c((byte) 10)));
        enumMap.put((Object) a.TOPIC, (Object) new org.apache.a.a.b("topic", (byte) 2, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.ALIAS_NAME, (Object) new org.apache.a.a.b("aliasName", (byte) 2, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.REQUEST, (Object) new org.apache.a.a.b(SocialConstants.TYPE_REQUEST, (byte) 2, new g((byte) 12, n.class)));
        enumMap.put((Object) a.PACKAGE_NAME, (Object) new org.apache.a.a.b("packageName", (byte) 2, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.CATEGORY, (Object) new org.apache.a.a.b(WidgetRequestParam.REQ_PARAM_COMMENT_CATEGORY, (byte) 2, new org.apache.a.a.c((byte) 11)));
        k = Collections.unmodifiableMap(enumMap);
        org.apache.a.a.b.a(e.class, k);
    }

    public e a(long j2) {
        this.e = j2;
        a(true);
        return this;
    }

    public e a(String str) {
        this.c = str;
        return this;
    }

    public void a(f fVar) {
        fVar.g();
        while (true) {
            c i2 = fVar.i();
            if (i2.b == 0) {
                fVar.h();
                if (!e()) {
                    throw new org.apache.a.b.g("Required field 'messageTs' was not found in serialized data! Struct: " + toString());
                }
                k();
                return;
            }
            switch (i2.c) {
                case 1:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.f2417a = fVar.w();
                        break;
                    }
                case 2:
                    if (i2.b != 12) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.b = new d();
                        this.b.a(fVar);
                        break;
                    }
                case 3:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.c = fVar.w();
                        break;
                    }
                case 4:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.d = fVar.w();
                        break;
                    }
                case 5:
                    if (i2.b != 10) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.e = fVar.u();
                        a(true);
                        break;
                    }
                case 6:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.f = fVar.w();
                        break;
                    }
                case 7:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.g = fVar.w();
                        break;
                    }
                case 8:
                    if (i2.b != 12) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.h = new n();
                        this.h.a(fVar);
                        break;
                    }
                case 9:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.i = fVar.w();
                        break;
                    }
                case 10:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.j = fVar.w();
                        break;
                    }
                default:
                    i.a(fVar, i2.b);
                    break;
            }
            fVar.j();
        }
    }

    public void a(boolean z) {
        this.w.set(0, z);
    }

    public boolean a() {
        return this.f2417a != null;
    }

    public boolean a(e eVar) {
        if (eVar == null) {
            return false;
        }
        boolean a2 = a();
        boolean a3 = eVar.a();
        if ((a2 || a3) && (!a2 || !a3 || !this.f2417a.equals(eVar.f2417a))) {
            return false;
        }
        boolean b2 = b();
        boolean b3 = eVar.b();
        if ((b2 || b3) && (!b2 || !b3 || !this.b.a(eVar.b))) {
            return false;
        }
        boolean c2 = c();
        boolean c3 = eVar.c();
        if ((c2 || c3) && (!c2 || !c3 || !this.c.equals(eVar.c))) {
            return false;
        }
        boolean d2 = d();
        boolean d3 = eVar.d();
        if (((d2 || d3) && (!d2 || !d3 || !this.d.equals(eVar.d))) || this.e != eVar.e) {
            return false;
        }
        boolean f2 = f();
        boolean f3 = eVar.f();
        if ((f2 || f3) && (!f2 || !f3 || !this.f.equals(eVar.f))) {
            return false;
        }
        boolean g2 = g();
        boolean g3 = eVar.g();
        if ((g2 || g3) && (!g2 || !g3 || !this.g.equals(eVar.g))) {
            return false;
        }
        boolean h2 = h();
        boolean h3 = eVar.h();
        if ((h2 || h3) && (!h2 || !h3 || !this.h.a(eVar.h))) {
            return false;
        }
        boolean i2 = i();
        boolean i3 = eVar.i();
        if ((i2 || i3) && (!i2 || !i3 || !this.i.equals(eVar.i))) {
            return false;
        }
        boolean j2 = j();
        boolean j3 = eVar.j();
        return (!j2 && !j3) || (j2 && j3 && this.j.equals(eVar.j));
    }

    /* renamed from: b */
    public int compareTo(e eVar) {
        int a2;
        int a3;
        int a4;
        int a5;
        int a6;
        int a7;
        int a8;
        int a9;
        int a10;
        int a11;
        if (!getClass().equals(eVar.getClass())) {
            return getClass().getName().compareTo(eVar.getClass().getName());
        }
        int compareTo = Boolean.valueOf(a()).compareTo(Boolean.valueOf(eVar.a()));
        if (compareTo != 0) {
            return compareTo;
        }
        if (a() && (a11 = org.apache.a.c.a(this.f2417a, eVar.f2417a)) != 0) {
            return a11;
        }
        int compareTo2 = Boolean.valueOf(b()).compareTo(Boolean.valueOf(eVar.b()));
        if (compareTo2 != 0) {
            return compareTo2;
        }
        if (b() && (a10 = org.apache.a.c.a(this.b, eVar.b)) != 0) {
            return a10;
        }
        int compareTo3 = Boolean.valueOf(c()).compareTo(Boolean.valueOf(eVar.c()));
        if (compareTo3 != 0) {
            return compareTo3;
        }
        if (c() && (a9 = org.apache.a.c.a(this.c, eVar.c)) != 0) {
            return a9;
        }
        int compareTo4 = Boolean.valueOf(d()).compareTo(Boolean.valueOf(eVar.d()));
        if (compareTo4 != 0) {
            return compareTo4;
        }
        if (d() && (a8 = org.apache.a.c.a(this.d, eVar.d)) != 0) {
            return a8;
        }
        int compareTo5 = Boolean.valueOf(e()).compareTo(Boolean.valueOf(eVar.e()));
        if (compareTo5 != 0) {
            return compareTo5;
        }
        if (e() && (a7 = org.apache.a.c.a(this.e, eVar.e)) != 0) {
            return a7;
        }
        int compareTo6 = Boolean.valueOf(f()).compareTo(Boolean.valueOf(eVar.f()));
        if (compareTo6 != 0) {
            return compareTo6;
        }
        if (f() && (a6 = org.apache.a.c.a(this.f, eVar.f)) != 0) {
            return a6;
        }
        int compareTo7 = Boolean.valueOf(g()).compareTo(Boolean.valueOf(eVar.g()));
        if (compareTo7 != 0) {
            return compareTo7;
        }
        if (g() && (a5 = org.apache.a.c.a(this.g, eVar.g)) != 0) {
            return a5;
        }
        int compareTo8 = Boolean.valueOf(h()).compareTo(Boolean.valueOf(eVar.h()));
        if (compareTo8 != 0) {
            return compareTo8;
        }
        if (h() && (a4 = org.apache.a.c.a(this.h, eVar.h)) != 0) {
            return a4;
        }
        int compareTo9 = Boolean.valueOf(i()).compareTo(Boolean.valueOf(eVar.i()));
        if (compareTo9 != 0) {
            return compareTo9;
        }
        if (i() && (a3 = org.apache.a.c.a(this.i, eVar.i)) != 0) {
            return a3;
        }
        int compareTo10 = Boolean.valueOf(j()).compareTo(Boolean.valueOf(eVar.j()));
        if (compareTo10 != 0) {
            return compareTo10;
        }
        if (!j() || (a2 = org.apache.a.c.a(this.j, eVar.j)) == 0) {
            return 0;
        }
        return a2;
    }

    public e b(String str) {
        this.d = str;
        return this;
    }

    public void b(f fVar) {
        k();
        fVar.a(l);
        if (this.f2417a != null && a()) {
            fVar.a(m);
            fVar.a(this.f2417a);
            fVar.b();
        }
        if (this.b != null && b()) {
            fVar.a(n);
            this.b.b(fVar);
            fVar.b();
        }
        if (this.c != null) {
            fVar.a(o);
            fVar.a(this.c);
            fVar.b();
        }
        if (this.d != null) {
            fVar.a(p);
            fVar.a(this.d);
            fVar.b();
        }
        fVar.a(q);
        fVar.a(this.e);
        fVar.b();
        if (this.f != null && f()) {
            fVar.a(r);
            fVar.a(this.f);
            fVar.b();
        }
        if (this.g != null && g()) {
            fVar.a(s);
            fVar.a(this.g);
            fVar.b();
        }
        if (this.h != null && h()) {
            fVar.a(t);
            this.h.b(fVar);
            fVar.b();
        }
        if (this.i != null && i()) {
            fVar.a(u);
            fVar.a(this.i);
            fVar.b();
        }
        if (this.j != null && j()) {
            fVar.a(v);
            fVar.a(this.j);
            fVar.b();
        }
        fVar.c();
        fVar.a();
    }

    public boolean b() {
        return this.b != null;
    }

    public e c(String str) {
        this.f = str;
        return this;
    }

    public boolean c() {
        return this.c != null;
    }

    public e d(String str) {
        this.g = str;
        return this;
    }

    public boolean d() {
        return this.d != null;
    }

    public boolean e() {
        return this.w.get(0);
    }

    public boolean equals(Object obj) {
        if (obj != null && (obj instanceof e)) {
            return a((e) obj);
        }
        return false;
    }

    public boolean f() {
        return this.f != null;
    }

    public boolean g() {
        return this.g != null;
    }

    public boolean h() {
        return this.h != null;
    }

    public int hashCode() {
        return 0;
    }

    public boolean i() {
        return this.i != null;
    }

    public boolean j() {
        return this.j != null;
    }

    public void k() {
        if (this.c == null) {
            throw new org.apache.a.b.g("Required field 'id' was not present! Struct: " + toString());
        } else if (this.d == null) {
            throw new org.apache.a.b.g("Required field 'appId' was not present! Struct: " + toString());
        }
    }

    public String toString() {
        boolean z = false;
        StringBuilder sb = new StringBuilder("XmPushActionAckMessage(");
        boolean z2 = true;
        if (a()) {
            sb.append("debug:");
            if (this.f2417a == null) {
                sb.append("null");
            } else {
                sb.append(this.f2417a);
            }
            z2 = false;
        }
        if (b()) {
            if (!z2) {
                sb.append(", ");
            }
            sb.append("target:");
            if (this.b == null) {
                sb.append("null");
            } else {
                sb.append(this.b);
            }
        } else {
            z = z2;
        }
        if (!z) {
            sb.append(", ");
        }
        sb.append("id:");
        if (this.c == null) {
            sb.append("null");
        } else {
            sb.append(this.c);
        }
        sb.append(", ");
        sb.append("appId:");
        if (this.d == null) {
            sb.append("null");
        } else {
            sb.append(this.d);
        }
        sb.append(", ");
        sb.append("messageTs:");
        sb.append(this.e);
        if (f()) {
            sb.append(", ");
            sb.append("topic:");
            if (this.f == null) {
                sb.append("null");
            } else {
                sb.append(this.f);
            }
        }
        if (g()) {
            sb.append(", ");
            sb.append("aliasName:");
            if (this.g == null) {
                sb.append("null");
            } else {
                sb.append(this.g);
            }
        }
        if (h()) {
            sb.append(", ");
            sb.append("request:");
            if (this.h == null) {
                sb.append("null");
            } else {
                sb.append(this.h);
            }
        }
        if (i()) {
            sb.append(", ");
            sb.append("packageName:");
            if (this.i == null) {
                sb.append("null");
            } else {
                sb.append(this.i);
            }
        }
        if (j()) {
            sb.append(", ");
            sb.append("category:");
            if (this.j == null) {
                sb.append("null");
            } else {
                sb.append(this.j);
            }
        }
        sb.append(")");
        return sb.toString();
    }
}
