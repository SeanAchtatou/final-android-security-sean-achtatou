package com.xiaomi.f.a;

import java.io.Serializable;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.apache.a.a.g;
import org.apache.a.b.c;
import org.apache.a.b.f;
import org.apache.a.b.i;
import org.apache.a.b.k;

public class b implements Serializable, Cloneable, org.apache.a.b<b, a> {
    public static final Map<a, org.apache.a.a.b> i;
    private static final k j = new k("PushMessage");
    private static final c k = new c("to", (byte) 12, 1);
    private static final c l = new c("id", (byte) 11, 2);
    private static final c m = new c("appId", (byte) 11, 3);
    private static final c n = new c("payload", (byte) 11, 4);
    private static final c o = new c("createAt", (byte) 10, 5);
    private static final c p = new c("ttl", (byte) 10, 6);
    private static final c q = new c("collapseKey", (byte) 11, 7);
    private static final c r = new c("packageName", (byte) 11, 8);

    /* renamed from: a  reason: collision with root package name */
    public d f2411a;
    public String b;
    public String c;
    public String d;
    public long e;
    public long f;
    public String g;
    public String h;
    private BitSet s = new BitSet(2);

    public enum a {
        TO(1, "to"),
        ID(2, "id"),
        APP_ID(3, "appId"),
        PAYLOAD(4, "payload"),
        CREATE_AT(5, "createAt"),
        TTL(6, "ttl"),
        COLLAPSE_KEY(7, "collapseKey"),
        PACKAGE_NAME(8, "packageName");
        
        private static final Map<String, a> i = new HashMap();
        private final short j;
        private final String k;

        static {
            Iterator it = EnumSet.allOf(a.class).iterator();
            while (it.hasNext()) {
                a aVar = (a) it.next();
                i.put(aVar.a(), aVar);
            }
        }

        private a(short s, String str) {
            this.j = s;
            this.k = str;
        }

        public String a() {
            return this.k;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [com.xiaomi.f.a.b$a, org.apache.a.a.b]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        EnumMap enumMap = new EnumMap(a.class);
        enumMap.put((Object) a.TO, (Object) new org.apache.a.a.b("to", (byte) 2, new g((byte) 12, d.class)));
        enumMap.put((Object) a.ID, (Object) new org.apache.a.a.b("id", (byte) 1, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.APP_ID, (Object) new org.apache.a.a.b("appId", (byte) 1, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.PAYLOAD, (Object) new org.apache.a.a.b("payload", (byte) 1, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.CREATE_AT, (Object) new org.apache.a.a.b("createAt", (byte) 2, new org.apache.a.a.c((byte) 10)));
        enumMap.put((Object) a.TTL, (Object) new org.apache.a.a.b("ttl", (byte) 2, new org.apache.a.a.c((byte) 10)));
        enumMap.put((Object) a.COLLAPSE_KEY, (Object) new org.apache.a.a.b("collapseKey", (byte) 2, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.PACKAGE_NAME, (Object) new org.apache.a.a.b("packageName", (byte) 2, new org.apache.a.a.c((byte) 11)));
        i = Collections.unmodifiableMap(enumMap);
        org.apache.a.a.b.a(b.class, i);
    }

    public void a(f fVar) {
        fVar.g();
        while (true) {
            c i2 = fVar.i();
            if (i2.b == 0) {
                fVar.h();
                m();
                return;
            }
            switch (i2.c) {
                case 1:
                    if (i2.b != 12) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.f2411a = new d();
                        this.f2411a.a(fVar);
                        break;
                    }
                case 2:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.b = fVar.w();
                        break;
                    }
                case 3:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.c = fVar.w();
                        break;
                    }
                case 4:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.d = fVar.w();
                        break;
                    }
                case 5:
                    if (i2.b != 10) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.e = fVar.u();
                        a(true);
                        break;
                    }
                case 6:
                    if (i2.b != 10) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.f = fVar.u();
                        b(true);
                        break;
                    }
                case 7:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.g = fVar.w();
                        break;
                    }
                case 8:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.h = fVar.w();
                        break;
                    }
                default:
                    i.a(fVar, i2.b);
                    break;
            }
            fVar.j();
        }
    }

    public void a(boolean z) {
        this.s.set(0, z);
    }

    public boolean a() {
        return this.f2411a != null;
    }

    public boolean a(b bVar) {
        if (bVar == null) {
            return false;
        }
        boolean a2 = a();
        boolean a3 = bVar.a();
        if ((a2 || a3) && (!a2 || !a3 || !this.f2411a.a(bVar.f2411a))) {
            return false;
        }
        boolean c2 = c();
        boolean c3 = bVar.c();
        if ((c2 || c3) && (!c2 || !c3 || !this.b.equals(bVar.b))) {
            return false;
        }
        boolean e2 = e();
        boolean e3 = bVar.e();
        if ((e2 || e3) && (!e2 || !e3 || !this.c.equals(bVar.c))) {
            return false;
        }
        boolean g2 = g();
        boolean g3 = bVar.g();
        if ((g2 || g3) && (!g2 || !g3 || !this.d.equals(bVar.d))) {
            return false;
        }
        boolean i2 = i();
        boolean i3 = bVar.i();
        if ((i2 || i3) && (!i2 || !i3 || this.e != bVar.e)) {
            return false;
        }
        boolean j2 = j();
        boolean j3 = bVar.j();
        if ((j2 || j3) && (!j2 || !j3 || this.f != bVar.f)) {
            return false;
        }
        boolean k2 = k();
        boolean k3 = bVar.k();
        if ((k2 || k3) && (!k2 || !k3 || !this.g.equals(bVar.g))) {
            return false;
        }
        boolean l2 = l();
        boolean l3 = bVar.l();
        return (!l2 && !l3) || (l2 && l3 && this.h.equals(bVar.h));
    }

    /* renamed from: b */
    public int compareTo(b bVar) {
        int a2;
        int a3;
        int a4;
        int a5;
        int a6;
        int a7;
        int a8;
        int a9;
        if (!getClass().equals(bVar.getClass())) {
            return getClass().getName().compareTo(bVar.getClass().getName());
        }
        int compareTo = Boolean.valueOf(a()).compareTo(Boolean.valueOf(bVar.a()));
        if (compareTo != 0) {
            return compareTo;
        }
        if (a() && (a9 = org.apache.a.c.a(this.f2411a, bVar.f2411a)) != 0) {
            return a9;
        }
        int compareTo2 = Boolean.valueOf(c()).compareTo(Boolean.valueOf(bVar.c()));
        if (compareTo2 != 0) {
            return compareTo2;
        }
        if (c() && (a8 = org.apache.a.c.a(this.b, bVar.b)) != 0) {
            return a8;
        }
        int compareTo3 = Boolean.valueOf(e()).compareTo(Boolean.valueOf(bVar.e()));
        if (compareTo3 != 0) {
            return compareTo3;
        }
        if (e() && (a7 = org.apache.a.c.a(this.c, bVar.c)) != 0) {
            return a7;
        }
        int compareTo4 = Boolean.valueOf(g()).compareTo(Boolean.valueOf(bVar.g()));
        if (compareTo4 != 0) {
            return compareTo4;
        }
        if (g() && (a6 = org.apache.a.c.a(this.d, bVar.d)) != 0) {
            return a6;
        }
        int compareTo5 = Boolean.valueOf(i()).compareTo(Boolean.valueOf(bVar.i()));
        if (compareTo5 != 0) {
            return compareTo5;
        }
        if (i() && (a5 = org.apache.a.c.a(this.e, bVar.e)) != 0) {
            return a5;
        }
        int compareTo6 = Boolean.valueOf(j()).compareTo(Boolean.valueOf(bVar.j()));
        if (compareTo6 != 0) {
            return compareTo6;
        }
        if (j() && (a4 = org.apache.a.c.a(this.f, bVar.f)) != 0) {
            return a4;
        }
        int compareTo7 = Boolean.valueOf(k()).compareTo(Boolean.valueOf(bVar.k()));
        if (compareTo7 != 0) {
            return compareTo7;
        }
        if (k() && (a3 = org.apache.a.c.a(this.g, bVar.g)) != 0) {
            return a3;
        }
        int compareTo8 = Boolean.valueOf(l()).compareTo(Boolean.valueOf(bVar.l()));
        if (compareTo8 != 0) {
            return compareTo8;
        }
        if (!l() || (a2 = org.apache.a.c.a(this.h, bVar.h)) == 0) {
            return 0;
        }
        return a2;
    }

    public String b() {
        return this.b;
    }

    public void b(f fVar) {
        m();
        fVar.a(j);
        if (this.f2411a != null && a()) {
            fVar.a(k);
            this.f2411a.b(fVar);
            fVar.b();
        }
        if (this.b != null) {
            fVar.a(l);
            fVar.a(this.b);
            fVar.b();
        }
        if (this.c != null) {
            fVar.a(m);
            fVar.a(this.c);
            fVar.b();
        }
        if (this.d != null) {
            fVar.a(n);
            fVar.a(this.d);
            fVar.b();
        }
        if (i()) {
            fVar.a(o);
            fVar.a(this.e);
            fVar.b();
        }
        if (j()) {
            fVar.a(p);
            fVar.a(this.f);
            fVar.b();
        }
        if (this.g != null && k()) {
            fVar.a(q);
            fVar.a(this.g);
            fVar.b();
        }
        if (this.h != null && l()) {
            fVar.a(r);
            fVar.a(this.h);
            fVar.b();
        }
        fVar.c();
        fVar.a();
    }

    public void b(boolean z) {
        this.s.set(1, z);
    }

    public boolean c() {
        return this.b != null;
    }

    public String d() {
        return this.c;
    }

    public boolean e() {
        return this.c != null;
    }

    public boolean equals(Object obj) {
        if (obj != null && (obj instanceof b)) {
            return a((b) obj);
        }
        return false;
    }

    public String f() {
        return this.d;
    }

    public boolean g() {
        return this.d != null;
    }

    public long h() {
        return this.e;
    }

    public int hashCode() {
        return 0;
    }

    public boolean i() {
        return this.s.get(0);
    }

    public boolean j() {
        return this.s.get(1);
    }

    public boolean k() {
        return this.g != null;
    }

    public boolean l() {
        return this.h != null;
    }

    public void m() {
        if (this.b == null) {
            throw new org.apache.a.b.g("Required field 'id' was not present! Struct: " + toString());
        } else if (this.c == null) {
            throw new org.apache.a.b.g("Required field 'appId' was not present! Struct: " + toString());
        } else if (this.d == null) {
            throw new org.apache.a.b.g("Required field 'payload' was not present! Struct: " + toString());
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("PushMessage(");
        boolean z = true;
        if (a()) {
            sb.append("to:");
            if (this.f2411a == null) {
                sb.append("null");
            } else {
                sb.append(this.f2411a);
            }
            z = false;
        }
        if (!z) {
            sb.append(", ");
        }
        sb.append("id:");
        if (this.b == null) {
            sb.append("null");
        } else {
            sb.append(this.b);
        }
        sb.append(", ");
        sb.append("appId:");
        if (this.c == null) {
            sb.append("null");
        } else {
            sb.append(this.c);
        }
        sb.append(", ");
        sb.append("payload:");
        if (this.d == null) {
            sb.append("null");
        } else {
            sb.append(this.d);
        }
        if (i()) {
            sb.append(", ");
            sb.append("createAt:");
            sb.append(this.e);
        }
        if (j()) {
            sb.append(", ");
            sb.append("ttl:");
            sb.append(this.f);
        }
        if (k()) {
            sb.append(", ");
            sb.append("collapseKey:");
            if (this.g == null) {
                sb.append("null");
            } else {
                sb.append(this.g);
            }
        }
        if (l()) {
            sb.append(", ");
            sb.append("packageName:");
            if (this.h == null) {
                sb.append("null");
            } else {
                sb.append(this.h);
            }
        }
        sb.append(")");
        return sb.toString();
    }
}
