package com.xiaomi.f.a;

import java.io.Serializable;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.apache.a.a.e;
import org.apache.a.b;
import org.apache.a.b.f;
import org.apache.a.b.g;
import org.apache.a.b.i;
import org.apache.a.b.k;

public class c implements Serializable, Cloneable, b<c, a> {
    public static final Map<a, org.apache.a.a.b> m;
    private static final k n = new k("PushMetaInfo");
    private static final org.apache.a.b.c o = new org.apache.a.b.c("id", (byte) 11, 1);
    private static final org.apache.a.b.c p = new org.apache.a.b.c("messageTs", (byte) 10, 2);
    private static final org.apache.a.b.c q = new org.apache.a.b.c("topic", (byte) 11, 3);
    private static final org.apache.a.b.c r = new org.apache.a.b.c("title", (byte) 11, 4);
    private static final org.apache.a.b.c s = new org.apache.a.b.c("description", (byte) 11, 5);
    private static final org.apache.a.b.c t = new org.apache.a.b.c("notifyType", (byte) 8, 6);
    private static final org.apache.a.b.c u = new org.apache.a.b.c("url", (byte) 11, 7);
    private static final org.apache.a.b.c v = new org.apache.a.b.c("passThrough", (byte) 8, 8);
    private static final org.apache.a.b.c w = new org.apache.a.b.c("notifyId", (byte) 8, 9);
    private static final org.apache.a.b.c x = new org.apache.a.b.c("extra", (byte) 13, 10);
    private static final org.apache.a.b.c y = new org.apache.a.b.c("internal", (byte) 13, 11);
    private static final org.apache.a.b.c z = new org.apache.a.b.c("ignoreRegInfo", (byte) 2, 12);
    private BitSet A;

    /* renamed from: a  reason: collision with root package name */
    public String f2413a;
    public long b;
    public String c;
    public String d;
    public String e;
    public int f;
    public String g;
    public int h;
    public int i;
    public Map<String, String> j;
    public Map<String, String> k;
    public boolean l;

    public enum a {
        ID(1, "id"),
        MESSAGE_TS(2, "messageTs"),
        TOPIC(3, "topic"),
        TITLE(4, "title"),
        DESCRIPTION(5, "description"),
        NOTIFY_TYPE(6, "notifyType"),
        URL(7, "url"),
        PASS_THROUGH(8, "passThrough"),
        NOTIFY_ID(9, "notifyId"),
        EXTRA(10, "extra"),
        INTERNAL(11, "internal"),
        IGNORE_REG_INFO(12, "ignoreRegInfo");
        
        private static final Map<String, a> m = new HashMap();
        private final short n;
        private final String o;

        static {
            Iterator it = EnumSet.allOf(a.class).iterator();
            while (it.hasNext()) {
                a aVar = (a) it.next();
                m.put(aVar.a(), aVar);
            }
        }

        private a(short s, String str) {
            this.n = s;
            this.o = str;
        }

        public String a() {
            return this.o;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [com.xiaomi.f.a.c$a, org.apache.a.a.b]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        EnumMap enumMap = new EnumMap(a.class);
        enumMap.put((Object) a.ID, (Object) new org.apache.a.a.b("id", (byte) 1, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.MESSAGE_TS, (Object) new org.apache.a.a.b("messageTs", (byte) 1, new org.apache.a.a.c((byte) 10)));
        enumMap.put((Object) a.TOPIC, (Object) new org.apache.a.a.b("topic", (byte) 2, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.TITLE, (Object) new org.apache.a.a.b("title", (byte) 2, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.DESCRIPTION, (Object) new org.apache.a.a.b("description", (byte) 2, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.NOTIFY_TYPE, (Object) new org.apache.a.a.b("notifyType", (byte) 2, new org.apache.a.a.c((byte) 8)));
        enumMap.put((Object) a.URL, (Object) new org.apache.a.a.b("url", (byte) 2, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.PASS_THROUGH, (Object) new org.apache.a.a.b("passThrough", (byte) 2, new org.apache.a.a.c((byte) 8)));
        enumMap.put((Object) a.NOTIFY_ID, (Object) new org.apache.a.a.b("notifyId", (byte) 2, new org.apache.a.a.c((byte) 8)));
        enumMap.put((Object) a.EXTRA, (Object) new org.apache.a.a.b("extra", (byte) 2, new e((byte) 13, new org.apache.a.a.c((byte) 11), new org.apache.a.a.c((byte) 11))));
        enumMap.put((Object) a.INTERNAL, (Object) new org.apache.a.a.b("internal", (byte) 2, new e((byte) 13, new org.apache.a.a.c((byte) 11), new org.apache.a.a.c((byte) 11))));
        enumMap.put((Object) a.IGNORE_REG_INFO, (Object) new org.apache.a.a.b("ignoreRegInfo", (byte) 2, new org.apache.a.a.c((byte) 2)));
        m = Collections.unmodifiableMap(enumMap);
        org.apache.a.a.b.a(c.class, m);
    }

    public c() {
        this.A = new BitSet(5);
        this.l = false;
    }

    public c(c cVar) {
        this.A = new BitSet(5);
        this.A.clear();
        this.A.or(cVar.A);
        if (cVar.c()) {
            this.f2413a = cVar.f2413a;
        }
        this.b = cVar.b;
        if (cVar.g()) {
            this.c = cVar.c;
        }
        if (cVar.i()) {
            this.d = cVar.d;
        }
        if (cVar.k()) {
            this.e = cVar.e;
        }
        this.f = cVar.f;
        if (cVar.n()) {
            this.g = cVar.g;
        }
        this.h = cVar.h;
        this.i = cVar.i;
        if (cVar.t()) {
            HashMap hashMap = new HashMap();
            for (Map.Entry next : cVar.j.entrySet()) {
                hashMap.put((String) next.getKey(), (String) next.getValue());
            }
            this.j = hashMap;
        }
        if (cVar.u()) {
            HashMap hashMap2 = new HashMap();
            for (Map.Entry next2 : cVar.k.entrySet()) {
                hashMap2.put((String) next2.getKey(), (String) next2.getValue());
            }
            this.k = hashMap2;
        }
        this.l = cVar.l;
    }

    public c a() {
        return new c(this);
    }

    public void a(String str, String str2) {
        if (this.j == null) {
            this.j = new HashMap();
        }
        this.j.put(str, str2);
    }

    public void a(f fVar) {
        fVar.g();
        while (true) {
            org.apache.a.b.c i2 = fVar.i();
            if (i2.b == 0) {
                fVar.h();
                if (!e()) {
                    throw new g("Required field 'messageTs' was not found in serialized data! Struct: " + toString());
                }
                x();
                return;
            }
            switch (i2.c) {
                case 1:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.f2413a = fVar.w();
                        break;
                    }
                case 2:
                    if (i2.b != 10) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.b = fVar.u();
                        a(true);
                        break;
                    }
                case 3:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.c = fVar.w();
                        break;
                    }
                case 4:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.d = fVar.w();
                        break;
                    }
                case 5:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.e = fVar.w();
                        break;
                    }
                case 6:
                    if (i2.b != 8) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.f = fVar.t();
                        b(true);
                        break;
                    }
                case 7:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.g = fVar.w();
                        break;
                    }
                case 8:
                    if (i2.b != 8) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.h = fVar.t();
                        c(true);
                        break;
                    }
                case 9:
                    if (i2.b != 8) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.i = fVar.t();
                        d(true);
                        break;
                    }
                case 10:
                    if (i2.b != 13) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        org.apache.a.b.e k2 = fVar.k();
                        this.j = new HashMap(k2.c * 2);
                        for (int i3 = 0; i3 < k2.c; i3++) {
                            this.j.put(fVar.w(), fVar.w());
                        }
                        fVar.l();
                        break;
                    }
                case 11:
                    if (i2.b != 13) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        org.apache.a.b.e k3 = fVar.k();
                        this.k = new HashMap(k3.c * 2);
                        for (int i4 = 0; i4 < k3.c; i4++) {
                            this.k.put(fVar.w(), fVar.w());
                        }
                        fVar.l();
                        break;
                    }
                case 12:
                    if (i2.b != 2) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.l = fVar.q();
                        e(true);
                        break;
                    }
                default:
                    i.a(fVar, i2.b);
                    break;
            }
            fVar.j();
        }
    }

    public void a(boolean z2) {
        this.A.set(0, z2);
    }

    public boolean a(c cVar) {
        if (cVar == null) {
            return false;
        }
        boolean c2 = c();
        boolean c3 = cVar.c();
        if (((c2 || c3) && (!c2 || !c3 || !this.f2413a.equals(cVar.f2413a))) || this.b != cVar.b) {
            return false;
        }
        boolean g2 = g();
        boolean g3 = cVar.g();
        if ((g2 || g3) && (!g2 || !g3 || !this.c.equals(cVar.c))) {
            return false;
        }
        boolean i2 = i();
        boolean i3 = cVar.i();
        if ((i2 || i3) && (!i2 || !i3 || !this.d.equals(cVar.d))) {
            return false;
        }
        boolean k2 = k();
        boolean k3 = cVar.k();
        if ((k2 || k3) && (!k2 || !k3 || !this.e.equals(cVar.e))) {
            return false;
        }
        boolean m2 = m();
        boolean m3 = cVar.m();
        if ((m2 || m3) && (!m2 || !m3 || this.f != cVar.f)) {
            return false;
        }
        boolean n2 = n();
        boolean n3 = cVar.n();
        if ((n2 || n3) && (!n2 || !n3 || !this.g.equals(cVar.g))) {
            return false;
        }
        boolean p2 = p();
        boolean p3 = cVar.p();
        if ((p2 || p3) && (!p2 || !p3 || this.h != cVar.h)) {
            return false;
        }
        boolean r2 = r();
        boolean r3 = cVar.r();
        if ((r2 || r3) && (!r2 || !r3 || this.i != cVar.i)) {
            return false;
        }
        boolean t2 = t();
        boolean t3 = cVar.t();
        if ((t2 || t3) && (!t2 || !t3 || !this.j.equals(cVar.j))) {
            return false;
        }
        boolean u2 = u();
        boolean u3 = cVar.u();
        if ((u2 || u3) && (!u2 || !u3 || !this.k.equals(cVar.k))) {
            return false;
        }
        boolean w2 = w();
        boolean w3 = cVar.w();
        return (!w2 && !w3) || (w2 && w3 && this.l == cVar.l);
    }

    /* renamed from: b */
    public int compareTo(c cVar) {
        int a2;
        int a3;
        int a4;
        int a5;
        int a6;
        int a7;
        int a8;
        int a9;
        int a10;
        int a11;
        int a12;
        int a13;
        if (!getClass().equals(cVar.getClass())) {
            return getClass().getName().compareTo(cVar.getClass().getName());
        }
        int compareTo = Boolean.valueOf(c()).compareTo(Boolean.valueOf(cVar.c()));
        if (compareTo != 0) {
            return compareTo;
        }
        if (c() && (a13 = org.apache.a.c.a(this.f2413a, cVar.f2413a)) != 0) {
            return a13;
        }
        int compareTo2 = Boolean.valueOf(e()).compareTo(Boolean.valueOf(cVar.e()));
        if (compareTo2 != 0) {
            return compareTo2;
        }
        if (e() && (a12 = org.apache.a.c.a(this.b, cVar.b)) != 0) {
            return a12;
        }
        int compareTo3 = Boolean.valueOf(g()).compareTo(Boolean.valueOf(cVar.g()));
        if (compareTo3 != 0) {
            return compareTo3;
        }
        if (g() && (a11 = org.apache.a.c.a(this.c, cVar.c)) != 0) {
            return a11;
        }
        int compareTo4 = Boolean.valueOf(i()).compareTo(Boolean.valueOf(cVar.i()));
        if (compareTo4 != 0) {
            return compareTo4;
        }
        if (i() && (a10 = org.apache.a.c.a(this.d, cVar.d)) != 0) {
            return a10;
        }
        int compareTo5 = Boolean.valueOf(k()).compareTo(Boolean.valueOf(cVar.k()));
        if (compareTo5 != 0) {
            return compareTo5;
        }
        if (k() && (a9 = org.apache.a.c.a(this.e, cVar.e)) != 0) {
            return a9;
        }
        int compareTo6 = Boolean.valueOf(m()).compareTo(Boolean.valueOf(cVar.m()));
        if (compareTo6 != 0) {
            return compareTo6;
        }
        if (m() && (a8 = org.apache.a.c.a(this.f, cVar.f)) != 0) {
            return a8;
        }
        int compareTo7 = Boolean.valueOf(n()).compareTo(Boolean.valueOf(cVar.n()));
        if (compareTo7 != 0) {
            return compareTo7;
        }
        if (n() && (a7 = org.apache.a.c.a(this.g, cVar.g)) != 0) {
            return a7;
        }
        int compareTo8 = Boolean.valueOf(p()).compareTo(Boolean.valueOf(cVar.p()));
        if (compareTo8 != 0) {
            return compareTo8;
        }
        if (p() && (a6 = org.apache.a.c.a(this.h, cVar.h)) != 0) {
            return a6;
        }
        int compareTo9 = Boolean.valueOf(r()).compareTo(Boolean.valueOf(cVar.r()));
        if (compareTo9 != 0) {
            return compareTo9;
        }
        if (r() && (a5 = org.apache.a.c.a(this.i, cVar.i)) != 0) {
            return a5;
        }
        int compareTo10 = Boolean.valueOf(t()).compareTo(Boolean.valueOf(cVar.t()));
        if (compareTo10 != 0) {
            return compareTo10;
        }
        if (t() && (a4 = org.apache.a.c.a(this.j, cVar.j)) != 0) {
            return a4;
        }
        int compareTo11 = Boolean.valueOf(u()).compareTo(Boolean.valueOf(cVar.u()));
        if (compareTo11 != 0) {
            return compareTo11;
        }
        if (u() && (a3 = org.apache.a.c.a(this.k, cVar.k)) != 0) {
            return a3;
        }
        int compareTo12 = Boolean.valueOf(w()).compareTo(Boolean.valueOf(cVar.w()));
        if (compareTo12 != 0) {
            return compareTo12;
        }
        if (!w() || (a2 = org.apache.a.c.a(this.l, cVar.l)) == 0) {
            return 0;
        }
        return a2;
    }

    public String b() {
        return this.f2413a;
    }

    public void b(f fVar) {
        x();
        fVar.a(n);
        if (this.f2413a != null) {
            fVar.a(o);
            fVar.a(this.f2413a);
            fVar.b();
        }
        fVar.a(p);
        fVar.a(this.b);
        fVar.b();
        if (this.c != null && g()) {
            fVar.a(q);
            fVar.a(this.c);
            fVar.b();
        }
        if (this.d != null && i()) {
            fVar.a(r);
            fVar.a(this.d);
            fVar.b();
        }
        if (this.e != null && k()) {
            fVar.a(s);
            fVar.a(this.e);
            fVar.b();
        }
        if (m()) {
            fVar.a(t);
            fVar.a(this.f);
            fVar.b();
        }
        if (this.g != null && n()) {
            fVar.a(u);
            fVar.a(this.g);
            fVar.b();
        }
        if (p()) {
            fVar.a(v);
            fVar.a(this.h);
            fVar.b();
        }
        if (r()) {
            fVar.a(w);
            fVar.a(this.i);
            fVar.b();
        }
        if (this.j != null && t()) {
            fVar.a(x);
            fVar.a(new org.apache.a.b.e((byte) 11, (byte) 11, this.j.size()));
            for (Map.Entry next : this.j.entrySet()) {
                fVar.a((String) next.getKey());
                fVar.a((String) next.getValue());
            }
            fVar.d();
            fVar.b();
        }
        if (this.k != null && u()) {
            fVar.a(y);
            fVar.a(new org.apache.a.b.e((byte) 11, (byte) 11, this.k.size()));
            for (Map.Entry next2 : this.k.entrySet()) {
                fVar.a((String) next2.getKey());
                fVar.a((String) next2.getValue());
            }
            fVar.d();
            fVar.b();
        }
        if (w()) {
            fVar.a(z);
            fVar.a(this.l);
            fVar.b();
        }
        fVar.c();
        fVar.a();
    }

    public void b(boolean z2) {
        this.A.set(1, z2);
    }

    public void c(boolean z2) {
        this.A.set(2, z2);
    }

    public boolean c() {
        return this.f2413a != null;
    }

    public long d() {
        return this.b;
    }

    public void d(boolean z2) {
        this.A.set(3, z2);
    }

    public void e(boolean z2) {
        this.A.set(4, z2);
    }

    public boolean e() {
        return this.A.get(0);
    }

    public boolean equals(Object obj) {
        if (obj != null && (obj instanceof c)) {
            return a((c) obj);
        }
        return false;
    }

    public String f() {
        return this.c;
    }

    public boolean g() {
        return this.c != null;
    }

    public String h() {
        return this.d;
    }

    public int hashCode() {
        return 0;
    }

    public boolean i() {
        return this.d != null;
    }

    public String j() {
        return this.e;
    }

    public boolean k() {
        return this.e != null;
    }

    public int l() {
        return this.f;
    }

    public boolean m() {
        return this.A.get(1);
    }

    public boolean n() {
        return this.g != null;
    }

    public int o() {
        return this.h;
    }

    public boolean p() {
        return this.A.get(2);
    }

    public int q() {
        return this.i;
    }

    public boolean r() {
        return this.A.get(3);
    }

    public Map<String, String> s() {
        return this.j;
    }

    public boolean t() {
        return this.j != null;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("PushMetaInfo(");
        sb.append("id:");
        if (this.f2413a == null) {
            sb.append("null");
        } else {
            sb.append(this.f2413a);
        }
        sb.append(", ");
        sb.append("messageTs:");
        sb.append(this.b);
        if (g()) {
            sb.append(", ");
            sb.append("topic:");
            if (this.c == null) {
                sb.append("null");
            } else {
                sb.append(this.c);
            }
        }
        if (i()) {
            sb.append(", ");
            sb.append("title:");
            if (this.d == null) {
                sb.append("null");
            } else {
                sb.append(this.d);
            }
        }
        if (k()) {
            sb.append(", ");
            sb.append("description:");
            if (this.e == null) {
                sb.append("null");
            } else {
                sb.append(this.e);
            }
        }
        if (m()) {
            sb.append(", ");
            sb.append("notifyType:");
            sb.append(this.f);
        }
        if (n()) {
            sb.append(", ");
            sb.append("url:");
            if (this.g == null) {
                sb.append("null");
            } else {
                sb.append(this.g);
            }
        }
        if (p()) {
            sb.append(", ");
            sb.append("passThrough:");
            sb.append(this.h);
        }
        if (r()) {
            sb.append(", ");
            sb.append("notifyId:");
            sb.append(this.i);
        }
        if (t()) {
            sb.append(", ");
            sb.append("extra:");
            if (this.j == null) {
                sb.append("null");
            } else {
                sb.append(this.j);
            }
        }
        if (u()) {
            sb.append(", ");
            sb.append("internal:");
            if (this.k == null) {
                sb.append("null");
            } else {
                sb.append(this.k);
            }
        }
        if (w()) {
            sb.append(", ");
            sb.append("ignoreRegInfo:");
            sb.append(this.l);
        }
        sb.append(")");
        return sb.toString();
    }

    public boolean u() {
        return this.k != null;
    }

    public boolean v() {
        return this.l;
    }

    public boolean w() {
        return this.A.get(4);
    }

    public void x() {
        if (this.f2413a == null) {
            throw new g("Required field 'id' was not present! Struct: " + toString());
        }
    }
}
