package com.xiaomi.c.a;

import com.xiaomi.d.d;
import com.xiaomi.d.e.b;
import com.xiaomi.d.e.m;
import com.xiaomi.d.f;
import java.io.Reader;
import java.io.Writer;
import java.text.SimpleDateFormat;

public class a implements com.xiaomi.d.a.a {

    /* renamed from: a  reason: collision with root package name */
    public static boolean f2359a = false;
    /* access modifiers changed from: private */
    public SimpleDateFormat b = new SimpleDateFormat("hh:mm:ss aaa");
    /* access modifiers changed from: private */
    public com.xiaomi.d.a c = null;
    private f d = null;
    private d e = null;
    private Writer f;
    private Reader g;
    private com.xiaomi.d.e.f h;
    private m i;

    public a(com.xiaomi.d.a aVar, Writer writer, Reader reader) {
        this.c = aVar;
        this.f = writer;
        this.g = reader;
        e();
    }

    private void e() {
        com.xiaomi.d.e.a aVar = new com.xiaomi.d.e.a(this.g);
        this.h = new b(this);
        aVar.a(this.h);
        b bVar = new b(this.f);
        this.i = new c(this);
        bVar.a(this.i);
        this.g = aVar;
        this.f = bVar;
        this.d = new d(this);
        this.e = new e(this);
    }

    public Reader a() {
        return this.g;
    }

    public Reader a(Reader reader) {
        ((com.xiaomi.d.e.a) this.g).b(this.h);
        com.xiaomi.d.e.a aVar = new com.xiaomi.d.e.a(reader);
        aVar.a(this.h);
        this.g = aVar;
        return this.g;
    }

    public Writer a(Writer writer) {
        ((b) this.f).b(this.i);
        b bVar = new b(writer);
        bVar.a(this.i);
        this.f = bVar;
        return this.f;
    }

    public Writer b() {
        return this.f;
    }

    public f c() {
        return this.d;
    }

    public f d() {
        return null;
    }
}
