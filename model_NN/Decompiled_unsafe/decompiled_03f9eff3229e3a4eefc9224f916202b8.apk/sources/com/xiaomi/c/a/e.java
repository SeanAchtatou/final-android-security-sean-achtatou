package com.xiaomi.c.a;

import com.xiaomi.a.a.c.c;
import com.xiaomi.d.a;
import com.xiaomi.d.d;
import java.util.Date;

class e implements d {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f2363a;

    e(a aVar) {
        this.f2363a = aVar;
    }

    public void a(a aVar) {
        c.c("SMACK " + this.f2363a.b.format(new Date()) + " Connection reconnected (" + this.f2363a.c.hashCode() + ")");
    }

    public void a(a aVar, int i, Exception exc) {
        c.c("SMACK " + this.f2363a.b.format(new Date()) + " Connection closed (" + this.f2363a.c.hashCode() + ")");
    }

    public void a(a aVar, Exception exc) {
        c.c("SMACK " + this.f2363a.b.format(new Date()) + " Reconnection failed due to an exception (" + this.f2363a.c.hashCode() + ")");
        exc.printStackTrace();
    }

    public void b(a aVar) {
        c.c("SMACK " + this.f2363a.b.format(new Date()) + " Connection started (" + this.f2363a.c.hashCode() + ")");
    }
}
