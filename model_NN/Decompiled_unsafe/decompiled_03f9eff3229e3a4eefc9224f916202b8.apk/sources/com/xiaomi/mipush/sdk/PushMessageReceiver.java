package com.xiaomi.mipush.sdk;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.xiaomi.mipush.sdk.MessageHandleService;

public abstract class PushMessageReceiver extends BroadcastReceiver {
    public void a(Context context, d dVar) {
    }

    public void a(Context context, e eVar) {
    }

    public void b(Context context, d dVar) {
    }

    public void b(Context context, e eVar) {
    }

    public void c(Context context, e eVar) {
    }

    @Deprecated
    public void d(Context context, e eVar) {
    }

    public final void onReceive(Context context, Intent intent) {
        MessageHandleService.a(new MessageHandleService.a(intent, this));
        try {
            context.startService(new Intent(context, MessageHandleService.class));
        } catch (Exception e) {
        }
    }
}
