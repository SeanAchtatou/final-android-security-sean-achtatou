package com.xiaomi.mipush.sdk;

import android.app.IntentService;
import android.content.Context;
import android.text.TextUtils;
import com.xiaomi.mipush.sdk.c;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PushMessageHandler extends IntentService {

    /* renamed from: a  reason: collision with root package name */
    private static List<c.a> f2451a = new ArrayList();

    interface a extends Serializable {
    }

    public PushMessageHandler() {
        super("mipush message handler");
    }

    protected static void a() {
        synchronized (f2451a) {
            f2451a.clear();
        }
    }

    public static void a(long j, String str, String str2) {
        synchronized (f2451a) {
            for (c.a a2 : f2451a) {
                a2.a(j, str, str2);
            }
        }
    }

    public static void a(Context context, a aVar) {
        String str = null;
        if (aVar instanceof e) {
            a(context, (e) aVar);
        } else if (aVar instanceof d) {
            d dVar = (d) aVar;
            String a2 = dVar.a();
            if ("register".equals(a2)) {
                List<String> b = dVar.b();
                if (b != null && !b.isEmpty()) {
                    str = b.get(0);
                }
                a(dVar.c(), dVar.d(), str);
            } else if ("set-alias".equals(a2) || "unset-alias".equals(a2) || "accept-time".equals(a2)) {
                a(context, dVar.e(), a2, dVar.c(), dVar.d(), dVar.b());
            } else if ("subscribe-topic".equals(a2)) {
                List<String> b2 = dVar.b();
                a(context, dVar.e(), dVar.c(), dVar.d(), (b2 == null || b2.isEmpty()) ? null : b2.get(0));
            } else if ("unsubscibe-topic".equals(a2)) {
                List<String> b3 = dVar.b();
                b(context, dVar.e(), dVar.c(), dVar.d(), (b3 == null || b3.isEmpty()) ? null : b3.get(0));
            }
        }
    }

    public static void a(Context context, e eVar) {
        synchronized (f2451a) {
            for (c.a next : f2451a) {
                if (a(eVar.h(), next.a())) {
                    next.a(eVar.c(), eVar.d(), eVar.e(), eVar.f());
                    next.a(eVar);
                }
            }
        }
    }

    protected static void a(Context context, String str, long j, String str2, String str3) {
        synchronized (f2451a) {
            for (c.a next : f2451a) {
                if (a(str, next.a())) {
                    next.b(j, str2, str3);
                }
            }
        }
    }

    protected static void a(Context context, String str, String str2, long j, String str3, List<String> list) {
        synchronized (f2451a) {
            for (c.a next : f2451a) {
                if (a(str, next.a())) {
                    next.a(str2, j, str3, list);
                }
            }
        }
    }

    protected static void a(c.a aVar) {
        synchronized (f2451a) {
            if (!f2451a.contains(aVar)) {
                f2451a.add(aVar);
            }
        }
    }

    protected static boolean a(String str, String str2) {
        return (TextUtils.isEmpty(str) && TextUtils.isEmpty(str2)) || TextUtils.equals(str, str2);
    }

    protected static void b(Context context, String str, long j, String str2, String str3) {
        synchronized (f2451a) {
            for (c.a next : f2451a) {
                if (a(str, next.a())) {
                    next.c(j, str2, str3);
                }
            }
        }
    }

    public static boolean b() {
        return f2451a.isEmpty();
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onHandleIntent(android.content.Intent r7) {
        /*
            r6 = this;
            java.lang.String r0 = "com.xiaomi.mipush.sdk.WAKEUP"
            java.lang.String r1 = r7.getAction()     // Catch:{ Throwable -> 0x0031 }
            boolean r0 = r0.equals(r1)     // Catch:{ Throwable -> 0x0031 }
            if (r0 == 0) goto L_0x001e
            com.xiaomi.mipush.sdk.g r0 = com.xiaomi.mipush.sdk.g.a(r6)     // Catch:{ Throwable -> 0x0031 }
            boolean r0 = r0.i()     // Catch:{ Throwable -> 0x0031 }
            if (r0 == 0) goto L_0x001d
            com.xiaomi.mipush.sdk.m r0 = com.xiaomi.mipush.sdk.m.a(r6)     // Catch:{ Throwable -> 0x0031 }
            r0.a()     // Catch:{ Throwable -> 0x0031 }
        L_0x001d:
            return
        L_0x001e:
            r0 = 1
            int r1 = com.xiaomi.mipush.sdk.f.a(r6)     // Catch:{ Throwable -> 0x0031 }
            if (r0 != r1) goto L_0x0044
            boolean r0 = b()     // Catch:{ Throwable -> 0x0031 }
            if (r0 == 0) goto L_0x0036
            java.lang.String r0 = "receive a message before application calling initialize"
            com.xiaomi.a.a.c.c.d(r0)     // Catch:{ Throwable -> 0x0031 }
            goto L_0x001d
        L_0x0031:
            r0 = move-exception
            com.xiaomi.a.a.c.c.a(r0)
            goto L_0x001d
        L_0x0036:
            com.xiaomi.mipush.sdk.l r0 = com.xiaomi.mipush.sdk.l.a(r6)     // Catch:{ Throwable -> 0x0031 }
            com.xiaomi.mipush.sdk.PushMessageHandler$a r0 = r0.a(r7)     // Catch:{ Throwable -> 0x0031 }
            if (r0 == 0) goto L_0x001d
            a(r6, r0)     // Catch:{ Throwable -> 0x0031 }
            goto L_0x001d
        L_0x0044:
            android.content.Intent r2 = new android.content.Intent     // Catch:{ Throwable -> 0x0031 }
            java.lang.String r0 = "com.xiaomi.mipush.RECEIVE_MESSAGE"
            r2.<init>(r0)     // Catch:{ Throwable -> 0x0031 }
            java.lang.String r0 = r6.getPackageName()     // Catch:{ Throwable -> 0x0031 }
            r2.setPackage(r0)     // Catch:{ Throwable -> 0x0031 }
            r2.putExtras(r7)     // Catch:{ Throwable -> 0x0031 }
            android.content.pm.PackageManager r0 = r6.getPackageManager()     // Catch:{ Throwable -> 0x0031 }
            r1 = 32
            java.util.List r0 = r0.queryBroadcastReceivers(r2, r1)     // Catch:{ Exception -> 0x009c }
            r1 = 0
            if (r0 == 0) goto L_0x00a9
            java.util.Iterator r3 = r0.iterator()     // Catch:{ Exception -> 0x009c }
        L_0x0066:
            boolean r0 = r3.hasNext()     // Catch:{ Exception -> 0x009c }
            if (r0 == 0) goto L_0x00a9
            java.lang.Object r0 = r3.next()     // Catch:{ Exception -> 0x009c }
            android.content.pm.ResolveInfo r0 = (android.content.pm.ResolveInfo) r0     // Catch:{ Exception -> 0x009c }
            android.content.pm.ActivityInfo r4 = r0.activityInfo     // Catch:{ Exception -> 0x009c }
            if (r4 == 0) goto L_0x0066
            android.content.pm.ActivityInfo r4 = r0.activityInfo     // Catch:{ Exception -> 0x009c }
            java.lang.String r4 = r4.packageName     // Catch:{ Exception -> 0x009c }
            java.lang.String r5 = r6.getPackageName()     // Catch:{ Exception -> 0x009c }
            boolean r4 = r4.equals(r5)     // Catch:{ Exception -> 0x009c }
            if (r4 == 0) goto L_0x0066
        L_0x0084:
            if (r0 == 0) goto L_0x00a2
            android.content.pm.ActivityInfo r0 = r0.activityInfo     // Catch:{ Exception -> 0x009c }
            java.lang.String r0 = r0.name     // Catch:{ Exception -> 0x009c }
            java.lang.Class r0 = java.lang.Class.forName(r0)     // Catch:{ Exception -> 0x009c }
            java.lang.Object r0 = r0.newInstance()     // Catch:{ Exception -> 0x009c }
            com.xiaomi.mipush.sdk.PushMessageReceiver r0 = (com.xiaomi.mipush.sdk.PushMessageReceiver) r0     // Catch:{ Exception -> 0x009c }
            android.content.Context r1 = r6.getApplicationContext()     // Catch:{ Exception -> 0x009c }
            r0.onReceive(r1, r2)     // Catch:{ Exception -> 0x009c }
            goto L_0x001d
        L_0x009c:
            r0 = move-exception
            com.xiaomi.a.a.c.c.a(r0)     // Catch:{ Throwable -> 0x0031 }
            goto L_0x001d
        L_0x00a2:
            java.lang.String r0 = "cannot find the receiver to handler this message, check your manifest"
            com.xiaomi.a.a.c.c.d(r0)     // Catch:{ Exception -> 0x009c }
            goto L_0x001d
        L_0x00a9:
            r0 = r1
            goto L_0x0084
        */
        throw new UnsupportedOperationException("Method not decompiled: com.xiaomi.mipush.sdk.PushMessageHandler.onHandleIntent(android.content.Intent):void");
    }
}
