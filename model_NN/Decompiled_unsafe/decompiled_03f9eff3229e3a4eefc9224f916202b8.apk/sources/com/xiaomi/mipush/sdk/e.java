package com.xiaomi.mipush.sdk;

import com.xiaomi.mipush.sdk.PushMessageHandler;
import java.util.HashMap;
import java.util.Map;

public class e implements PushMessageHandler.a {

    /* renamed from: a  reason: collision with root package name */
    private String f2456a;
    private int b;
    private String c;
    private String d;
    private String e;
    private String f;
    private int g;
    private int h;
    private int i;
    private boolean j;
    private String k;
    private String l;
    private String m;
    private boolean n = false;
    private HashMap<String, String> o = new HashMap<>();

    public String a() {
        return this.f2456a;
    }

    public void a(int i2) {
        this.b = i2;
    }

    public void a(String str) {
        this.f2456a = str;
    }

    public void a(Map<String, String> map) {
        this.o.clear();
        if (map != null) {
            this.o.putAll(map);
        }
    }

    public void a(boolean z) {
        this.n = z;
    }

    public void b(int i2) {
        this.h = i2;
    }

    public void b(String str) {
        this.c = str;
    }

    public void b(boolean z) {
        this.j = z;
    }

    public boolean b() {
        return this.n;
    }

    public String c() {
        return this.c;
    }

    public void c(int i2) {
        this.i = i2;
    }

    public void c(String str) {
        this.d = str;
    }

    public String d() {
        return this.d;
    }

    public void d(int i2) {
        this.g = i2;
    }

    public void d(String str) {
        this.f = str;
    }

    public String e() {
        return this.e;
    }

    public void e(String str) {
        this.e = str;
    }

    public void f(String str) {
        this.k = str;
    }

    public boolean f() {
        return this.j;
    }

    public String g() {
        return this.l;
    }

    public void g(String str) {
        this.l = str;
    }

    public String h() {
        return this.m;
    }

    public void h(String str) {
        this.m = str;
    }

    public int i() {
        return this.g;
    }

    public Map<String, String> j() {
        return this.o;
    }

    public String toString() {
        return "messageId={" + this.f2456a + "},passThrough={" + this.g + "},alias={" + this.d + "},topic={" + this.e + "},userAccount={" + this.f + "},content={" + this.c + "},description={" + this.k + "},title={" + this.l + "},isNotified={" + this.j + "},notifyId={" + this.i + "},notifyType={" + this.h + "}, category={" + this.m + "}, extra={" + this.o + "}";
    }
}
