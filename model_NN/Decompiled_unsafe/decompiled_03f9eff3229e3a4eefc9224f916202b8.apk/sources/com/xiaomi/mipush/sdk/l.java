package com.xiaomi.mipush.sdk;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.text.TextUtils;
import cn.banshenggua.aichang.utils.Constants;
import com.xiaomi.a.a.c.c;
import com.xiaomi.a.a.g.d;
import com.xiaomi.f.a.a;
import com.xiaomi.f.a.e;
import com.xiaomi.f.a.g;
import com.xiaomi.f.a.h;
import com.xiaomi.f.a.i;
import com.xiaomi.f.a.k;
import com.xiaomi.f.a.n;
import com.xiaomi.f.a.p;
import com.xiaomi.f.a.r;
import com.xiaomi.f.a.t;
import com.xiaomi.f.a.u;
import com.xiaomi.mipush.sdk.PushMessageHandler;
import com.xiaomi.push.service.ak;
import com.xiaomi.push.service.aq;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.TimeZone;
import org.apache.a.b;
import org.apache.a.f;

public class l {

    /* renamed from: a  reason: collision with root package name */
    private static l f2464a = null;
    private static Queue<String> c;
    private static Object d = new Object();
    private Context b;

    private l(Context context) {
        this.b = context.getApplicationContext();
        if (this.b == null) {
            this.b = context;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:71:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0026  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.content.Intent a(android.content.Context r6, java.lang.String r7, java.util.Map<java.lang.String, java.lang.String> r8) {
        /*
            r1 = 0
            if (r8 == 0) goto L_0x000b
            java.lang.String r0 = "notify_effect"
            boolean r0 = r8.containsKey(r0)
            if (r0 != 0) goto L_0x000c
        L_0x000b:
            return r1
        L_0x000c:
            java.lang.String r0 = "notify_effect"
            java.lang.Object r0 = r8.get(r0)
            java.lang.String r0 = (java.lang.String) r0
            java.lang.String r2 = com.xiaomi.push.service.aq.f2528a
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x0056
            android.content.pm.PackageManager r0 = r6.getPackageManager()     // Catch:{ Exception -> 0x0039 }
            android.content.Intent r0 = r0.getLaunchIntentForPackage(r7)     // Catch:{ Exception -> 0x0039 }
        L_0x0024:
            if (r0 == 0) goto L_0x000b
            r2 = 268435456(0x10000000, float:2.5243549E-29)
            r0.addFlags(r2)
            android.content.pm.PackageManager r2 = r6.getPackageManager()     // Catch:{ Exception -> 0x016c }
            r3 = 65536(0x10000, float:9.18355E-41)
            android.content.pm.ResolveInfo r2 = r2.resolveActivity(r0, r3)     // Catch:{ Exception -> 0x016c }
            if (r2 == 0) goto L_0x000b
            r1 = r0
            goto L_0x000b
        L_0x0039:
            r0 = move-exception
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Cause: "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r0 = r0.getMessage()
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            com.xiaomi.a.a.c.c.d(r0)
            r0 = r1
            goto L_0x0024
        L_0x0056:
            java.lang.String r2 = com.xiaomi.push.service.aq.b
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x00e8
            java.lang.String r0 = "intent_uri"
            boolean r0 = r8.containsKey(r0)
            if (r0 == 0) goto L_0x0095
            java.lang.String r0 = "intent_uri"
            java.lang.Object r0 = r8.get(r0)
            java.lang.String r0 = (java.lang.String) r0
            if (r0 == 0) goto L_0x0199
            r2 = 1
            android.content.Intent r0 = android.content.Intent.parseUri(r0, r2)     // Catch:{ URISyntaxException -> 0x018c }
            r0.setPackage(r7)     // Catch:{ URISyntaxException -> 0x0079 }
            goto L_0x0024
        L_0x0079:
            r2 = move-exception
        L_0x007a:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Cause: "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r2 = r2.getMessage()
            java.lang.StringBuilder r2 = r3.append(r2)
            java.lang.String r2 = r2.toString()
            com.xiaomi.a.a.c.c.d(r2)
            goto L_0x0024
        L_0x0095:
            java.lang.String r0 = "class_name"
            boolean r0 = r8.containsKey(r0)
            if (r0 == 0) goto L_0x0196
            java.lang.String r0 = "class_name"
            java.lang.Object r0 = r8.get(r0)
            java.lang.String r0 = (java.lang.String) r0
            android.content.Intent r2 = new android.content.Intent
            r2.<init>()
            android.content.ComponentName r3 = new android.content.ComponentName
            r3.<init>(r7, r0)
            r2.setComponent(r3)
            java.lang.String r0 = "intent_flag"
            boolean r0 = r8.containsKey(r0)     // Catch:{ NumberFormatException -> 0x00cc }
            if (r0 == 0) goto L_0x00c9
            java.lang.String r0 = "intent_flag"
            java.lang.Object r0 = r8.get(r0)     // Catch:{ NumberFormatException -> 0x00cc }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ NumberFormatException -> 0x00cc }
            int r0 = java.lang.Integer.parseInt(r0)     // Catch:{ NumberFormatException -> 0x00cc }
            r2.setFlags(r0)     // Catch:{ NumberFormatException -> 0x00cc }
        L_0x00c9:
            r0 = r2
            goto L_0x0024
        L_0x00cc:
            r0 = move-exception
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Cause by intent_flag: "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r0 = r0.getMessage()
            java.lang.StringBuilder r0 = r3.append(r0)
            java.lang.String r0 = r0.toString()
            com.xiaomi.a.a.c.c.d(r0)
            goto L_0x00c9
        L_0x00e8:
            java.lang.String r2 = com.xiaomi.push.service.aq.c
            boolean r0 = r2.equals(r0)
            if (r0 == 0) goto L_0x0196
            java.lang.String r0 = "web_uri"
            java.lang.Object r0 = r8.get(r0)
            java.lang.String r0 = (java.lang.String) r0
            if (r0 == 0) goto L_0x0196
            java.lang.String r0 = r0.trim()
            java.lang.String r2 = "http://"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x0194
            java.lang.String r2 = "https://"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x0194
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "http://"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            r2 = r0
        L_0x0122:
            java.net.URL r0 = new java.net.URL     // Catch:{ MalformedURLException -> 0x0189 }
            r0.<init>(r2)     // Catch:{ MalformedURLException -> 0x0189 }
            java.lang.String r0 = r0.getProtocol()     // Catch:{ MalformedURLException -> 0x0189 }
            java.lang.String r3 = "http"
            boolean r3 = r3.equals(r0)     // Catch:{ MalformedURLException -> 0x0189 }
            if (r3 != 0) goto L_0x013b
            java.lang.String r3 = "https"
            boolean r0 = r3.equals(r0)     // Catch:{ MalformedURLException -> 0x0189 }
            if (r0 == 0) goto L_0x0191
        L_0x013b:
            android.content.Intent r0 = new android.content.Intent     // Catch:{ MalformedURLException -> 0x0189 }
            java.lang.String r3 = "android.intent.action.VIEW"
            r0.<init>(r3)     // Catch:{ MalformedURLException -> 0x0189 }
            android.net.Uri r2 = android.net.Uri.parse(r2)     // Catch:{ MalformedURLException -> 0x014b }
            r0.setData(r2)     // Catch:{ MalformedURLException -> 0x014b }
            goto L_0x0024
        L_0x014b:
            r2 = move-exception
            r5 = r2
            r2 = r0
            r0 = r5
        L_0x014f:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Cause: "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r0 = r0.getMessage()
            java.lang.StringBuilder r0 = r3.append(r0)
            java.lang.String r0 = r0.toString()
            com.xiaomi.a.a.c.c.d(r0)
            r0 = r2
            goto L_0x0024
        L_0x016c:
            r0 = move-exception
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Cause: "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r0 = r0.getMessage()
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            com.xiaomi.a.a.c.c.d(r0)
            goto L_0x000b
        L_0x0189:
            r0 = move-exception
            r2 = r1
            goto L_0x014f
        L_0x018c:
            r0 = move-exception
            r2 = r0
            r0 = r1
            goto L_0x007a
        L_0x0191:
            r0 = r1
            goto L_0x0024
        L_0x0194:
            r2 = r0
            goto L_0x0122
        L_0x0196:
            r0 = r1
            goto L_0x0024
        L_0x0199:
            r0 = r1
            goto L_0x0024
        */
        throw new UnsupportedOperationException("Method not decompiled: com.xiaomi.mipush.sdk.l.a(android.content.Context, java.lang.String, java.util.Map):android.content.Intent");
    }

    private PushMessageHandler.a a(h hVar, boolean z, byte[] bArr) {
        List<String> list;
        ArrayList arrayList;
        ArrayList arrayList2;
        ArrayList arrayList3;
        e eVar = null;
        try {
            b a2 = k.a(this.b, hVar);
            if (a2 == null) {
                c.d("receiving an un-recognized message. " + hVar.f2423a);
                return null;
            }
            c.c("receive a message." + a2);
            a a3 = hVar.a();
            c.a("processing a message, action=" + a3);
            switch (a3) {
                case SendMessage:
                    if (!g.a(this.b).l() || z) {
                        n nVar = (n) a2;
                        com.xiaomi.f.a.b l = nVar.l();
                        if (l == null) {
                            c.d("receive an empty message without push content, drop it");
                            return null;
                        }
                        if (z) {
                            if (ak.b(hVar)) {
                                c.a(this.b, l.b(), hVar.m(), hVar.f, l.d());
                            } else {
                                c.a(this.b, l.b(), hVar.m(), l.d());
                            }
                        }
                        if (!z) {
                            if (!TextUtils.isEmpty(nVar.j()) && c.h(this.b, nVar.j()) < 0) {
                                c.a(this.b, nVar.j());
                            } else if (!TextUtils.isEmpty(nVar.h()) && c.g(this.b, nVar.h()) < 0) {
                                c.e(this.b, nVar.h());
                            }
                        }
                        String str = (hVar.h == null || hVar.h.s() == null) ? null : hVar.h.j.get("jobkey");
                        String b2 = TextUtils.isEmpty(str) ? l.b() : str;
                        if (z || !a(this.b, b2)) {
                            e a4 = f.a(nVar, hVar.m(), z);
                            if (a4.i() != 0 || z || !ak.a(a4.j())) {
                                c.a("receive a message, msgid=" + l.b() + ", jobkey=" + b2);
                                if (!z || a4.j() == null || !a4.j().containsKey("notify_effect")) {
                                    eVar = a4;
                                } else {
                                    Map<String, String> j = a4.j();
                                    String str2 = j.get("notify_effect");
                                    if (ak.b(hVar)) {
                                        Intent a5 = a(this.b, hVar.f, j);
                                        if (a5 == null) {
                                            c.a("Getting Intent fail from ignore reg message. ");
                                            return null;
                                        }
                                        String f = l.f();
                                        if (!TextUtils.isEmpty(f)) {
                                            a5.putExtra("payload", f);
                                        }
                                        this.b.startActivity(a5);
                                        return null;
                                    }
                                    Intent a6 = a(this.b, this.b.getPackageName(), j);
                                    if (a6 == null) {
                                        return null;
                                    }
                                    if (!str2.equals(aq.c)) {
                                        a6.putExtra("key_message", a4);
                                    }
                                    this.b.startActivity(a6);
                                    return null;
                                }
                            } else {
                                ak.a(this.b, hVar, bArr);
                                return null;
                            }
                        } else {
                            c.a("drop a duplicate message, key=" + b2);
                        }
                        if (hVar.m() != null || z) {
                            return eVar;
                        }
                        a(nVar, hVar.m());
                        return eVar;
                    }
                    c.a("receive a message in pause state. drop it");
                    return null;
                case Registration:
                    k kVar = (k) a2;
                    if (kVar.f == 0) {
                        g.a(this.b).b(kVar.h, kVar.i);
                    }
                    if (!TextUtils.isEmpty(kVar.h)) {
                        arrayList3 = new ArrayList();
                        arrayList3.add(kVar.h);
                    } else {
                        arrayList3 = null;
                    }
                    d a7 = f.a("register", arrayList3, kVar.f, kVar.g, null);
                    m.a(this.b).d();
                    return a7;
                case UnRegistration:
                    if (((r) a2).f == 0) {
                        g.a(this.b).h();
                        c.b(this.b);
                    }
                    PushMessageHandler.a();
                    return null;
                case Subscription:
                    p pVar = (p) a2;
                    if (pVar.f == 0) {
                        c.e(this.b, pVar.h());
                    }
                    if (!TextUtils.isEmpty(pVar.h())) {
                        arrayList2 = new ArrayList();
                        arrayList2.add(pVar.h());
                    } else {
                        arrayList2 = null;
                    }
                    return f.a("subscribe-topic", arrayList2, pVar.f, pVar.g, pVar.k());
                case UnSubscription:
                    t tVar = (t) a2;
                    if (tVar.f == 0) {
                        c.f(this.b, tVar.h());
                    }
                    if (!TextUtils.isEmpty(tVar.h())) {
                        arrayList = new ArrayList();
                        arrayList.add(tVar.h());
                    } else {
                        arrayList = null;
                    }
                    return f.a("unsubscibe-topic", arrayList, tVar.f, tVar.g, tVar.k());
                case Command:
                    g gVar = (g) a2;
                    String e = gVar.e();
                    List<String> k = gVar.k();
                    if (gVar.g == 0) {
                        if (TextUtils.equals(e, "accept-time") && k != null && k.size() > 1) {
                            c.c(this.b, k.get(0), k.get(1));
                            if (!"00:00".equals(k.get(0)) || !"00:00".equals(k.get(1))) {
                                g.a(this.b).a(false);
                            } else {
                                g.a(this.b).a(true);
                            }
                            list = a(TimeZone.getTimeZone("GMT+08"), TimeZone.getDefault(), k);
                            return f.a(e, list, gVar.g, gVar.h, gVar.m());
                        } else if (TextUtils.equals(e, "set-alias") && k != null && k.size() > 0) {
                            c.a(this.b, k.get(0));
                            list = k;
                            return f.a(e, list, gVar.g, gVar.h, gVar.m());
                        } else if (TextUtils.equals(e, "unset-alias") && k != null && k.size() > 0) {
                            c.b(this.b, k.get(0));
                            list = k;
                            return f.a(e, list, gVar.g, gVar.h, gVar.m());
                        } else if (TextUtils.equals(e, "set-account") && k != null && k.size() > 0) {
                            c.c(this.b, k.get(0));
                            list = k;
                            return f.a(e, list, gVar.g, gVar.h, gVar.m());
                        } else if (TextUtils.equals(e, "unset-account") && k != null && k.size() > 0) {
                            c.d(this.b, k.get(0));
                        }
                    }
                    list = k;
                    return f.a(e, list, gVar.g, gVar.h, gVar.m());
                case Notification:
                    i iVar = (i) a2;
                    if ("registration id expired".equalsIgnoreCase(iVar.e)) {
                        c.c(this.b);
                        return null;
                    } else if (!"client_info_update_ok".equalsIgnoreCase(iVar.e) || iVar.h() == null || !iVar.h().containsKey("app_version")) {
                        return null;
                    } else {
                        g.a(this.b).a(iVar.h().get("app_version"));
                        return null;
                    }
                default:
                    return null;
            }
        } catch (f e2) {
            c.a(e2);
            c.d("receive a message which action string is not valid. is the reg expired?");
            return null;
        }
    }

    private PushMessageHandler.a a(h hVar, byte[] bArr) {
        String str = null;
        try {
            b a2 = k.a(this.b, hVar);
            if (a2 == null) {
                c.d("message arrived: receiving an un-recognized message. " + hVar.f2423a);
                return null;
            }
            c.c("message arrived: receive a message." + a2);
            a a3 = hVar.a();
            c.a("message arrived: processing an arrived message, action=" + a3);
            switch (a3) {
                case SendMessage:
                    n nVar = (n) a2;
                    com.xiaomi.f.a.b l = nVar.l();
                    if (l == null) {
                        c.d("message arrived: receive an empty message without push content, drop it");
                        return null;
                    }
                    if (!(hVar.h == null || hVar.h.s() == null)) {
                        str = hVar.h.j.get("jobkey");
                    }
                    e a4 = f.a(nVar, hVar.m(), false);
                    a4.a(true);
                    c.a("message arrived: receive a message, msgid=" + l.b() + ", jobkey=" + str);
                    return a4;
                default:
                    return null;
            }
        } catch (f e) {
            c.a(e);
            c.d("message arrived: receive a message which action string is not valid. is the reg expired?");
            return null;
        }
    }

    public static l a(Context context) {
        if (f2464a == null) {
            f2464a = new l(context);
        }
        return f2464a;
    }

    private void a(h hVar) {
        com.xiaomi.f.a.c m = hVar.m();
        e eVar = new e();
        eVar.b(hVar.h());
        eVar.a(m.b());
        eVar.a(m.d());
        if (!TextUtils.isEmpty(m.f())) {
            eVar.c(m.f());
        }
        m.a(this.b).a(eVar, a.AckMessage, false, hVar.m());
    }

    private void a(n nVar, com.xiaomi.f.a.c cVar) {
        e eVar = new e();
        eVar.b(nVar.e());
        eVar.a(nVar.c());
        eVar.a(nVar.l().h());
        if (!TextUtils.isEmpty(nVar.h())) {
            eVar.c(nVar.h());
        }
        if (!TextUtils.isEmpty(nVar.j())) {
            eVar.d(nVar.j());
        }
        m.a(this.b).a(eVar, a.AckMessage, cVar);
    }

    private static boolean a(Context context, String str) {
        boolean z = false;
        synchronized (d) {
            SharedPreferences j = g.a(context).j();
            if (c == null) {
                String[] split = j.getString("pref_msg_ids", "").split(",");
                c = new LinkedList();
                for (String add : split) {
                    c.add(add);
                }
            }
            if (c.contains(str)) {
                z = true;
            } else {
                c.add(str);
                if (c.size() > 10) {
                    c.poll();
                }
                String a2 = d.a(c, ",");
                SharedPreferences.Editor edit = j.edit();
                edit.putString("pref_msg_ids", a2);
                edit.commit();
            }
        }
        return z;
    }

    public PushMessageHandler.a a(Intent intent) {
        String action = intent.getAction();
        c.a("receive an intent from server, action=" + action);
        String stringExtra = intent.getStringExtra("mrt");
        if (stringExtra == null) {
            stringExtra = Long.toString(System.currentTimeMillis());
        }
        if ("com.xiaomi.mipush.RECEIVE_MESSAGE".equals(action)) {
            byte[] byteArrayExtra = intent.getByteArrayExtra("mipush_payload");
            boolean booleanExtra = intent.getBooleanExtra("mipush_notified", false);
            if (byteArrayExtra == null) {
                c.d("receiving an empty message, drop");
                return null;
            }
            h hVar = new h();
            try {
                u.a(hVar, byteArrayExtra);
                g a2 = g.a(this.b);
                com.xiaomi.f.a.c m = hVar.m();
                if (hVar.a() == a.SendMessage && m != null && !a2.l() && !booleanExtra) {
                    if (m != null) {
                        hVar.m().a("mrt", stringExtra);
                        hVar.m().a("mat", Long.toString(System.currentTimeMillis()));
                    }
                    a(hVar);
                }
                if (hVar.a() == a.SendMessage && !hVar.c()) {
                    if (!ak.b(hVar)) {
                        Object[] objArr = new Object[2];
                        objArr[0] = hVar.j();
                        objArr[1] = m != null ? m.b() : "";
                        c.a(String.format("drop an un-encrypted messages. %1$s, %2$s", objArr));
                        return null;
                    } else if (!booleanExtra || m.s() == null || !m.s().containsKey("notify_effect")) {
                        c.a(String.format("drop an un-encrypted messages. %1$s, %2$s", hVar.j(), m.b()));
                        return null;
                    }
                }
                if (a2.i() || hVar.f2423a == a.Registration) {
                    if (!a2.i() || !a2.n()) {
                        return a(hVar, booleanExtra, byteArrayExtra);
                    }
                    if (hVar.f2423a == a.UnRegistration) {
                        a2.h();
                        c.b(this.b);
                        PushMessageHandler.a();
                    } else {
                        c.e(this.b);
                    }
                } else if (ak.b(hVar)) {
                    return a(hVar, booleanExtra, byteArrayExtra);
                } else {
                    c.d("receive message without registration. need unregister or re-register!");
                }
            } catch (f e) {
                c.a(e);
            } catch (Exception e2) {
                c.a(e2);
            }
        } else if ("com.xiaomi.mipush.ERROR".equals(action)) {
            d dVar = new d();
            h hVar2 = new h();
            try {
                byte[] byteArrayExtra2 = intent.getByteArrayExtra("mipush_payload");
                if (byteArrayExtra2 != null) {
                    u.a(hVar2, byteArrayExtra2);
                }
            } catch (f e3) {
            }
            dVar.a(String.valueOf(hVar2.a()));
            dVar.a((long) intent.getIntExtra("mipush_error_code", 0));
            dVar.b(intent.getStringExtra("mipush_error_msg"));
            c.d("receive a error message. code = " + intent.getIntExtra("mipush_error_code", 0) + ", msg= " + intent.getStringExtra("mipush_error_msg"));
            return dVar;
        } else if ("com.xiaomi.mipush.MESSAGE_ARRIVED".equals(action)) {
            byte[] byteArrayExtra3 = intent.getByteArrayExtra("mipush_payload");
            if (byteArrayExtra3 == null) {
                c.d("message arrived: receiving an empty message, drop");
                return null;
            }
            h hVar3 = new h();
            try {
                u.a(hVar3, byteArrayExtra3);
                g a3 = g.a(this.b);
                if (ak.b(hVar3)) {
                    c.d("message arrived: receive ignore reg message, ignore!");
                } else if (!a3.i()) {
                    c.d("message arrived: receive message without registration. need unregister or re-register!");
                } else if (!a3.i() || !a3.n()) {
                    return a(hVar3, byteArrayExtra3);
                } else {
                    c.d("message arrived: app info is invalidated");
                }
            } catch (f e4) {
                c.a(e4);
            } catch (Exception e5) {
                c.a(e5);
            }
        }
        return null;
    }

    public List<String> a(TimeZone timeZone, TimeZone timeZone2, List<String> list) {
        if (timeZone.equals(timeZone2)) {
            return list;
        }
        long rawOffset = (long) (((timeZone.getRawOffset() - timeZone2.getRawOffset()) / Constants.CLEARIMGED) / 60);
        long parseLong = Long.parseLong(list.get(0).split(":")[0]);
        long parseLong2 = Long.parseLong(list.get(0).split(":")[1]);
        long j = ((((parseLong * 60) + parseLong2) - rawOffset) + 1440) % 1440;
        long parseLong3 = (((Long.parseLong(list.get(1).split(":")[1]) + (60 * Long.parseLong(list.get(1).split(":")[0]))) - rawOffset) + 1440) % 1440;
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = arrayList;
        arrayList2.add(String.format("%1$02d:%2$02d", Long.valueOf(j / 60), Long.valueOf(j % 60)));
        ArrayList arrayList3 = arrayList;
        arrayList3.add(String.format("%1$02d:%2$02d", Long.valueOf(parseLong3 / 60), Long.valueOf(parseLong3 % 60)));
        return arrayList;
    }
}
