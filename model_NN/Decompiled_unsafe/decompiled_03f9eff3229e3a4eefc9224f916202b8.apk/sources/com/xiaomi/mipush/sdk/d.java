package com.xiaomi.mipush.sdk;

import com.xiaomi.mipush.sdk.PushMessageHandler;
import java.util.List;

public class d implements PushMessageHandler.a {

    /* renamed from: a  reason: collision with root package name */
    private String f2455a;
    private long b;
    private String c;
    private List<String> d;
    private String e;

    public String a() {
        return this.f2455a;
    }

    public void a(long j) {
        this.b = j;
    }

    public void a(String str) {
        this.f2455a = str;
    }

    public void a(List<String> list) {
        this.d = list;
    }

    public List<String> b() {
        return this.d;
    }

    public void b(String str) {
        this.c = str;
    }

    public long c() {
        return this.b;
    }

    public void c(String str) {
        this.e = str;
    }

    public String d() {
        return this.c;
    }

    public String e() {
        return this.e;
    }

    public String toString() {
        return "command={" + this.f2455a + "}, resultCode={" + this.b + "}, reason={" + this.c + "}, category={" + this.e + "}, commandArguments={" + this.d + "}";
    }
}
