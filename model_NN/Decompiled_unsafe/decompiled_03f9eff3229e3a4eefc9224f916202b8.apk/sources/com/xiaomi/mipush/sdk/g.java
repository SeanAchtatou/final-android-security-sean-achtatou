package com.xiaomi.mipush.sdk;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.text.TextUtils;
import com.xiaomi.a.a.c.c;
import com.xiaomi.push.service.x;

public class g {

    /* renamed from: a  reason: collision with root package name */
    private static g f2458a;
    /* access modifiers changed from: private */
    public Context b;
    private a c;

    private class a {

        /* renamed from: a  reason: collision with root package name */
        public String f2459a;
        public String b;
        public String c;
        public String d;
        public String e;
        public String f;
        public String g;
        public boolean h;
        public boolean i;
        public int j;

        private a() {
            this.h = true;
            this.i = false;
            this.j = 1;
        }

        private String d() {
            return g.a(g.this.b, g.this.b.getPackageName());
        }

        public void a(int i2) {
            this.j = i2;
        }

        public void a(String str, String str2) {
            this.c = str;
            this.d = str2;
            this.f = x.c(g.this.b);
            this.e = d();
            this.h = true;
            SharedPreferences.Editor edit = g.this.j().edit();
            edit.putString("regId", str);
            edit.putString("regSec", str2);
            edit.putString("devId", this.f);
            edit.putString("vName", d());
            edit.putBoolean("valid", true);
            edit.commit();
        }

        public void a(String str, String str2, String str3) {
            this.f2459a = str;
            this.b = str2;
            this.g = str3;
            SharedPreferences.Editor edit = g.this.j().edit();
            edit.putString("appId", this.f2459a);
            edit.putString("appToken", str2);
            edit.putString("regResource", str3);
            edit.commit();
        }

        public void a(boolean z) {
            this.i = z;
        }

        public boolean a() {
            return b(this.f2459a, this.b);
        }

        public void b() {
            g.this.j().edit().clear().commit();
            this.f2459a = null;
            this.b = null;
            this.c = null;
            this.d = null;
            this.f = null;
            this.e = null;
            this.h = false;
            this.i = false;
            this.j = 1;
        }

        public boolean b(String str, String str2) {
            return TextUtils.equals(this.f2459a, str) && TextUtils.equals(this.b, str2) && !TextUtils.isEmpty(this.c) && !TextUtils.isEmpty(this.d) && TextUtils.equals(this.f, x.c(g.this.b));
        }

        public void c() {
            this.h = false;
            g.this.j().edit().putBoolean("valid", this.h).commit();
        }
    }

    private g(Context context) {
        this.b = context;
        o();
    }

    public static g a(Context context) {
        if (f2458a == null) {
            f2458a = new g(context);
        }
        return f2458a;
    }

    public static String a(Context context, String str) {
        PackageInfo packageInfo;
        try {
            packageInfo = context.getPackageManager().getPackageInfo(str, 16384);
        } catch (Exception e) {
            c.a(e);
            packageInfo = null;
        }
        return packageInfo != null ? packageInfo.versionName : "1.0";
    }

    private void o() {
        this.c = new a();
        SharedPreferences j = j();
        this.c.f2459a = j.getString("appId", null);
        this.c.b = j.getString("appToken", null);
        this.c.c = j.getString("regId", null);
        this.c.d = j.getString("regSec", null);
        this.c.f = j.getString("devId", null);
        if (!TextUtils.isEmpty(this.c.f) && this.c.f.startsWith("a-")) {
            this.c.f = x.c(this.b);
            j.edit().putString("devId", this.c.f).commit();
        }
        this.c.e = j.getString("vName", null);
        this.c.h = j.getBoolean("valid", true);
        this.c.i = j.getBoolean("paused", false);
        this.c.j = j.getInt("envType", 1);
        this.c.g = j.getString("regResource", null);
    }

    public void a(int i) {
        this.c.a(i);
        j().edit().putInt("envType", i).commit();
    }

    public void a(String str) {
        SharedPreferences.Editor edit = j().edit();
        edit.putString("vName", str);
        edit.commit();
        this.c.e = str;
    }

    public void a(String str, String str2, String str3) {
        this.c.a(str, str2, str3);
    }

    public void a(boolean z) {
        this.c.a(z);
        j().edit().putBoolean("paused", z).commit();
    }

    public boolean a() {
        return !TextUtils.equals(a(this.b, this.b.getPackageName()), this.c.e);
    }

    public boolean a(String str, String str2) {
        return this.c.b(str, str2);
    }

    public void b(String str, String str2) {
        this.c.a(str, str2);
    }

    public boolean b() {
        if (this.c.a()) {
            return true;
        }
        c.a("Don't send message before initialization succeeded!");
        return false;
    }

    public String c() {
        return this.c.f2459a;
    }

    public String d() {
        return this.c.b;
    }

    public String e() {
        return this.c.c;
    }

    public String f() {
        return this.c.d;
    }

    public String g() {
        return this.c.g;
    }

    public void h() {
        this.c.b();
    }

    public boolean i() {
        return this.c.a();
    }

    public SharedPreferences j() {
        return this.b.getSharedPreferences("mipush", 0);
    }

    public void k() {
        this.c.c();
    }

    public boolean l() {
        return this.c.i;
    }

    public int m() {
        return this.c.j;
    }

    public boolean n() {
        return !this.c.h;
    }
}
