package com.xiaomi.mipush.sdk;

import android.content.Context;
import com.xiaomi.a.a.c.a;
import com.xiaomi.a.a.c.c;
import com.xiaomi.push.a.e;
import com.xiaomi.push.a.f;

public class b {

    /* renamed from: a  reason: collision with root package name */
    private static boolean f2452a = false;
    private static a b = null;

    private static void a(Context context) {
        boolean z = b != null;
        f fVar = new f(context);
        if (!f2452a && b(context) && z) {
            c.a(new e(b, fVar));
        } else if (!f2452a && b(context)) {
            c.a(fVar);
        } else if (z) {
            c.a(b);
        } else {
            c.a(new e(null, null));
        }
    }

    public static void a(Context context, a aVar) {
        b = aVar;
        a(context);
    }

    private static boolean b(Context context) {
        try {
            String[] strArr = context.getPackageManager().getPackageInfo(context.getPackageName(), 4096).requestedPermissions;
            if (strArr == null) {
                return false;
            }
            for (String equals : strArr) {
                if ("android.permission.WRITE_EXTERNAL_STORAGE".equals(equals)) {
                    return true;
                }
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }
}
