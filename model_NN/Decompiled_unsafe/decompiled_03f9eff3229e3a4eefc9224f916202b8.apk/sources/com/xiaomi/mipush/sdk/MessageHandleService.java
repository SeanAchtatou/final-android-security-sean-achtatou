package com.xiaomi.mipush.sdk;

import android.app.IntentService;
import android.content.Intent;
import android.text.TextUtils;
import com.xiaomi.a.a.c.c;
import com.xiaomi.mipush.sdk.PushMessageHandler;
import java.util.concurrent.ConcurrentLinkedQueue;

public class MessageHandleService extends IntentService {

    /* renamed from: a  reason: collision with root package name */
    private static ConcurrentLinkedQueue<a> f2449a = new ConcurrentLinkedQueue<>();

    public static class a {

        /* renamed from: a  reason: collision with root package name */
        private PushMessageReceiver f2450a;
        private Intent b;

        public a(Intent intent, PushMessageReceiver pushMessageReceiver) {
            this.f2450a = pushMessageReceiver;
            this.b = intent;
        }

        public PushMessageReceiver a() {
            return this.f2450a;
        }

        public Intent b() {
            return this.b;
        }
    }

    public MessageHandleService() {
        super("MessageHandleThread");
    }

    public static void a(a aVar) {
        if (aVar != null) {
            f2449a.add(aVar);
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    public void onHandleIntent(Intent intent) {
        a poll;
        if (intent != null && (poll = f2449a.poll()) != null) {
            try {
                PushMessageReceiver a2 = poll.a();
                Intent b = poll.b();
                switch (b.getIntExtra("message_type", 1)) {
                    case 1:
                        PushMessageHandler.a a3 = l.a(this).a(b);
                        if (a3 == null) {
                            return;
                        }
                        if (a3 instanceof e) {
                            e eVar = (e) a3;
                            if (!eVar.b()) {
                                a2.d(this, eVar);
                            }
                            if (eVar.i() == 1) {
                                a2.c(this, eVar);
                                return;
                            } else if (eVar.f()) {
                                a2.a(this, eVar);
                                return;
                            } else {
                                a2.b(this, eVar);
                                return;
                            }
                        } else if (a3 instanceof d) {
                            d dVar = (d) a3;
                            a2.a(this, dVar);
                            if (TextUtils.equals(dVar.a(), "register")) {
                                a2.b(this, dVar);
                                return;
                            }
                            return;
                        } else {
                            return;
                        }
                    case 2:
                    default:
                        return;
                    case 3:
                        d dVar2 = (d) b.getSerializableExtra("key_command");
                        a2.a(this, dVar2);
                        if (TextUtils.equals(dVar2.a(), "register")) {
                            a2.b(this, dVar2);
                            return;
                        }
                        return;
                    case 4:
                        return;
                }
            } catch (RuntimeException e) {
                c.a(e);
            }
            c.a(e);
        }
    }
}
