package com.xiaomi.mipush.sdk;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import cn.banshenggua.aichang.utils.Constants;
import com.alimama.mobile.pluginframework.core.PluginFramework;
import com.xiaomi.a.a.g.d;
import com.xiaomi.f.a.f;
import com.xiaomi.f.a.i;
import com.xiaomi.f.a.j;
import com.xiaomi.f.a.o;
import com.xiaomi.f.a.q;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;

public abstract class c {

    /* renamed from: a  reason: collision with root package name */
    private static boolean f2453a = true;
    /* access modifiers changed from: private */
    public static Context b;
    private static long c = System.currentTimeMillis();

    @Deprecated
    public static abstract class a {

        /* renamed from: a  reason: collision with root package name */
        private String f2454a;

        /* access modifiers changed from: protected */
        public String a() {
            return this.f2454a;
        }

        public void a(long j, String str, String str2) {
        }

        public void a(e eVar) {
        }

        public void a(String str, long j, String str2, List<String> list) {
        }

        public void a(String str, String str2, String str3, boolean z) {
        }

        public void b(long j, String str, String str2) {
        }

        public void c(long j, String str, String str2) {
        }
    }

    protected static synchronized String a() {
        String str;
        synchronized (c.class) {
            str = d.a(4) + c;
            c++;
        }
        return str;
    }

    public static void a(Context context, int i, int i2, int i3, int i4, String str) {
        if (i < 0 || i >= 24 || i3 < 0 || i3 >= 24 || i2 < 0 || i2 >= 60 || i4 < 0 || i4 >= 60) {
            throw new IllegalArgumentException("the input parameter is not valid.");
        }
        long rawOffset = (long) (((TimeZone.getTimeZone("GMT+08").getRawOffset() - TimeZone.getDefault().getRawOffset()) / Constants.CLEARIMGED) / 60);
        long j = ((((long) ((i * 60) + i2)) + rawOffset) + 1440) % 1440;
        long j2 = ((rawOffset + ((long) ((i3 * 60) + i4))) + 1440) % 1440;
        ArrayList arrayList = new ArrayList();
        arrayList.add(String.format("%1$02d:%2$02d", Long.valueOf(j / 60), Long.valueOf(j % 60)));
        arrayList.add(String.format("%1$02d:%2$02d", Long.valueOf(j2 / 60), Long.valueOf(j2 % 60)));
        ArrayList arrayList2 = new ArrayList();
        arrayList2.add(String.format("%1$02d:%2$02d", Integer.valueOf(i), Integer.valueOf(i2)));
        arrayList2.add(String.format("%1$02d:%2$02d", Integer.valueOf(i3), Integer.valueOf(i4)));
        if (!d(context, (String) arrayList.get(0), (String) arrayList.get(1))) {
            a(context, "accept-time", arrayList, str);
        } else if (1 == f.a(context)) {
            PushMessageHandler.a(context, str, "accept-time", 0, null, arrayList2);
        } else {
            f.a(context, f.a("accept-time", arrayList2, 0, null, null));
        }
    }

    static synchronized void a(Context context, String str) {
        synchronized (c.class) {
            context.getSharedPreferences("mipush_extra", 0).edit().putLong("alias_" + str, System.currentTimeMillis()).commit();
        }
    }

    static void a(Context context, String str, com.xiaomi.f.a.c cVar, String str2) {
        i iVar = new i();
        if (!TextUtils.isEmpty(str2)) {
            iVar.b(str2);
        } else if (g.a(context).b()) {
            iVar.b(g.a(context).c());
        } else {
            com.xiaomi.a.a.c.c.d("do not report clicked message");
            return;
        }
        iVar.c("bar:click");
        iVar.a(str);
        iVar.a(false);
        m.a(context).a(iVar, com.xiaomi.f.a.a.Notification, false, cVar);
    }

    static void a(Context context, String str, com.xiaomi.f.a.c cVar, String str2, String str3) {
        i iVar = new i();
        if (TextUtils.isEmpty(str3)) {
            com.xiaomi.a.a.c.c.d("do not report clicked message");
            return;
        }
        iVar.b(str3);
        iVar.c("bar:click");
        iVar.a(str);
        iVar.a(false);
        m.a(context).a(iVar, com.xiaomi.f.a.a.Notification, false, true, cVar, true, str2, str3);
    }

    public static void a(Context context, String str, String str2) {
        new Thread(new h(context, str, str2)).start();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.xiaomi.mipush.sdk.c.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.Context, java.lang.String]
     candidates:
      com.xiaomi.mipush.sdk.c.a(android.content.Context, java.lang.String):void
      com.xiaomi.mipush.sdk.c.a(java.lang.Object, java.lang.String):void */
    @Deprecated
    public static void a(Context context, String str, String str2, a aVar) {
        boolean z = false;
        a((Object) context, "context");
        a(str, "appID");
        a(str2, "appToken");
        try {
            b = context.getApplicationContext();
            if (b == null) {
                b = context;
            }
            if (aVar != null) {
                PushMessageHandler.a(aVar);
            }
            if (g.a(b).m() != a.a()) {
                z = true;
            }
            if (z || l(b)) {
                if (z || !g.a(b).a(str, str2) || g.a(b).n()) {
                    String a2 = d.a(6);
                    g.a(b).h();
                    g.a(b).a(a.a());
                    g.a(b).a(str, str2, a2);
                    b(b);
                    j jVar = new j();
                    jVar.a(a());
                    jVar.b(str);
                    jVar.e(str2);
                    jVar.d(context.getPackageName());
                    jVar.f(a2);
                    jVar.c(g.a(context, context.getPackageName()));
                    m.a(b).a(jVar, z);
                } else {
                    if (1 == f.a(context)) {
                        a(aVar, "callback");
                        aVar.a(0, null, g.a(context).e());
                    } else {
                        ArrayList arrayList = new ArrayList();
                        arrayList.add(g.a(context).e());
                        f.a(b, f.a("register", arrayList, 0, null, null));
                    }
                    m.a(context).a();
                    if (g.a(b).a()) {
                        i iVar = new i();
                        iVar.b(g.a(context).c());
                        iVar.c("client_info_update");
                        iVar.a(a());
                        iVar.h = new HashMap();
                        iVar.h.put("app_version", g.a(b, b.getPackageName()));
                        String g = g.a(b).g();
                        if (!TextUtils.isEmpty(g)) {
                            iVar.h.put(PluginFramework.KEY_UPDATE_DEVICEID, g);
                        }
                        m.a(context).a(iVar, com.xiaomi.f.a.a.Notification, false, null);
                    }
                    if (!com.xiaomi.a.a.a.a.a(b, "update_devId", false)) {
                        c();
                        com.xiaomi.a.a.a.a.b(b, "update_devId", true);
                    }
                    if (a(b) && j(b)) {
                        i iVar2 = new i();
                        iVar2.b(g.a(b).c());
                        iVar2.c("pull");
                        iVar2.a(a());
                        iVar2.a(false);
                        m.a(b).a(iVar2, com.xiaomi.f.a.a.Notification, false, null, false);
                        i(b);
                    }
                }
                if (f2453a) {
                    h(b);
                }
                k(b);
                return;
            }
            m.a(context).a();
            com.xiaomi.a.a.c.c.a("Could not send  register message within 5s repeatly .");
        } catch (Throwable th) {
            com.xiaomi.a.a.c.c.a(th);
        }
    }

    protected static void a(Context context, String str, ArrayList<String> arrayList, String str2) {
        if (!TextUtils.isEmpty(g.a(context).c())) {
            f fVar = new f();
            fVar.a(a());
            fVar.b(g.a(context).c());
            fVar.c(str);
            Iterator<String> it = arrayList.iterator();
            while (it.hasNext()) {
                fVar.d(it.next());
            }
            fVar.f(str2);
            fVar.e(context.getPackageName());
            m.a(context).a(fVar, com.xiaomi.f.a.a.Command, (com.xiaomi.f.a.c) null);
        }
    }

    private static void a(Object obj, String str) {
        if (obj == null) {
            throw new IllegalArgumentException("param " + str + " is not nullable");
        }
    }

    public static boolean a(Context context) {
        return m.a(context).b();
    }

    protected static void b(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("mipush_extra", 0);
        long j = sharedPreferences.getLong("wake_up", 0);
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.clear();
        if (j > 0) {
            edit.putLong("wake_up", j);
        }
        edit.commit();
    }

    static synchronized void b(Context context, String str) {
        synchronized (c.class) {
            context.getSharedPreferences("mipush_extra", 0).edit().remove("alias_" + str).commit();
        }
    }

    public static void b(Context context, String str, String str2) {
        if (!TextUtils.isEmpty(g.a(context).c()) && !TextUtils.isEmpty(str)) {
            if (System.currentTimeMillis() - g(context, str) > LogBuilder.MAX_INTERVAL) {
                o oVar = new o();
                oVar.a(a());
                oVar.b(g.a(context).c());
                oVar.c(str);
                oVar.d(context.getPackageName());
                oVar.e(str2);
                m.a(context).a(oVar, com.xiaomi.f.a.a.Subscription, (com.xiaomi.f.a.c) null);
            } else if (1 == f.a(context)) {
                PushMessageHandler.a(context, str2, 0, null, str);
            } else {
                ArrayList arrayList = new ArrayList();
                arrayList.add(str);
                f.a(context, f.a("subscribe-topic", arrayList, 0, null, null));
            }
        }
    }

    private static void c() {
        new Thread(new i()).start();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.xiaomi.mipush.sdk.m.a(com.xiaomi.f.a.j, boolean):void
     arg types: [com.xiaomi.f.a.j, int]
     candidates:
      com.xiaomi.mipush.sdk.m.a(com.xiaomi.mipush.sdk.m, java.lang.Integer):java.lang.Integer
      com.xiaomi.mipush.sdk.m.a(com.xiaomi.f.a.j, boolean):void */
    static void c(Context context) {
        if (g.a(context).i()) {
            String a2 = d.a(6);
            String c2 = g.a(context).c();
            String d = g.a(context).d();
            g.a(context).h();
            g.a(context).a(c2, d, a2);
            j jVar = new j();
            jVar.a(a());
            jVar.b(c2);
            jVar.e(d);
            jVar.f(a2);
            jVar.d(context.getPackageName());
            jVar.c(g.a(context, context.getPackageName()));
            m.a(context).a(jVar, false);
        }
    }

    static synchronized void c(Context context, String str) {
        synchronized (c.class) {
            context.getSharedPreferences("mipush_extra", 0).edit().putLong("account_" + str, System.currentTimeMillis()).commit();
        }
    }

    static synchronized void c(Context context, String str, String str2) {
        synchronized (c.class) {
            context.getSharedPreferences("mipush_extra", 0).edit().putString("accept_time", str + "," + str2).commit();
        }
    }

    public static void d(Context context) {
        m.a(context).e();
    }

    static synchronized void d(Context context, String str) {
        synchronized (c.class) {
            context.getSharedPreferences("mipush_extra", 0).edit().remove("account_" + str).commit();
        }
    }

    private static boolean d(Context context, String str, String str2) {
        return TextUtils.equals(context.getSharedPreferences("mipush_extra", 0).getString("accept_time", ""), str + "," + str2);
    }

    public static void e(Context context) {
        if (g.a(context).b()) {
            q qVar = new q();
            qVar.a(a());
            qVar.b(g.a(context).c());
            qVar.c(g.a(context).e());
            qVar.e(g.a(context).d());
            qVar.d(context.getPackageName());
            m.a(context).a(qVar);
            PushMessageHandler.a();
            g.a(context).k();
            b(context);
            d(context);
            f(context);
        }
    }

    static synchronized void e(Context context, String str) {
        synchronized (c.class) {
            context.getSharedPreferences("mipush_extra", 0).edit().putLong("topic_" + str, System.currentTimeMillis()).commit();
        }
    }

    public static void f(Context context) {
        m.a(context).a(-1);
    }

    static synchronized void f(Context context, String str) {
        synchronized (c.class) {
            context.getSharedPreferences("mipush_extra", 0).edit().remove("topic_" + str).commit();
        }
    }

    public static long g(Context context, String str) {
        return context.getSharedPreferences("mipush_extra", 0).getLong("topic_" + str, -1);
    }

    public static String g(Context context) {
        if (g.a(context).i()) {
            return g.a(context).e();
        }
        return null;
    }

    public static long h(Context context, String str) {
        return context.getSharedPreferences("mipush_extra", 0).getLong("alias_" + str, -1);
    }

    private static void h(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("mipush_extra", 0);
        if (System.currentTimeMillis() - 600000 >= sharedPreferences.getLong("wake_up", 0)) {
            sharedPreferences.edit().putLong("wake_up", System.currentTimeMillis()).commit();
            new Thread(new j(context)).start();
        }
    }

    private static void i(Context context) {
        context.getSharedPreferences("mipush_extra", 0).edit().putLong("last_pull_notification", System.currentTimeMillis()).commit();
    }

    private static boolean j(Context context) {
        return System.currentTimeMillis() - context.getSharedPreferences("mipush_extra", 0).getLong("last_pull_notification", -1) > 300000;
    }

    private static void k(Context context) {
        context.getSharedPreferences("mipush_extra", 0).edit().putLong("last_reg_request", System.currentTimeMillis()).commit();
    }

    private static boolean l(Context context) {
        return System.currentTimeMillis() - context.getSharedPreferences("mipush_extra", 0).getLong("last_reg_request", -1) > 5000;
    }
}
