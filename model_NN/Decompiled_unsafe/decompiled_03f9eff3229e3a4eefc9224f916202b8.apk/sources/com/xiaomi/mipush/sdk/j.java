package com.xiaomi.mipush.sdk;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.ServiceInfo;
import java.util.List;

final class j implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Context f2461a;

    j(Context context) {
        this.f2461a = context;
    }

    public void run() {
        if (!c.a(this.f2461a) && 1 == g.a(this.f2461a).m()) {
            try {
                List<PackageInfo> installedPackages = this.f2461a.getPackageManager().getInstalledPackages(4);
                if (installedPackages != null) {
                    for (PackageInfo packageInfo : installedPackages) {
                        ServiceInfo[] serviceInfoArr = packageInfo.services;
                        if (serviceInfoArr != null) {
                            for (ServiceInfo serviceInfo : serviceInfoArr) {
                                if (serviceInfo.exported && serviceInfo.enabled && "com.xiaomi.mipush.sdk.PushMessageHandler".equals(serviceInfo.name) && !this.f2461a.getPackageName().equals(serviceInfo.packageName)) {
                                    try {
                                        Thread.sleep(((long) ((Math.random() * 2.0d) + 1.0d)) * 1000);
                                    } catch (InterruptedException e) {
                                    }
                                    Intent intent = new Intent();
                                    intent.setClassName(serviceInfo.packageName, serviceInfo.name);
                                    intent.setAction("com.xiaomi.mipush.sdk.WAKEUP");
                                    this.f2461a.startService(intent);
                                }
                            }
                        }
                    }
                }
            } catch (Throwable th) {
            }
        }
    }
}
