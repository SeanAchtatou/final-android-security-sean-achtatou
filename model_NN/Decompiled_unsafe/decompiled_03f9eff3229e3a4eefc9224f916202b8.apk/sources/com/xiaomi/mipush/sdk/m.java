package com.xiaomi.mipush.sdk;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.os.Handler;
import android.os.Looper;
import com.xiaomi.a.a.c.c;
import com.xiaomi.a.a.e.d;
import com.xiaomi.f.a.h;
import com.xiaomi.f.a.j;
import com.xiaomi.f.a.q;
import com.xiaomi.f.a.u;
import com.xiaomi.push.service.aq;
import java.util.ArrayList;
import java.util.Iterator;
import org.apache.a.b;

public class m {
    private static m b;
    private static final ArrayList<a> e = new ArrayList<>();

    /* renamed from: a  reason: collision with root package name */
    private boolean f2466a = false;
    /* access modifiers changed from: private */
    public Context c;
    private String d;
    private Intent f = null;
    /* access modifiers changed from: private */
    public Integer g = null;

    static class a<T extends b<T, ?>> {

        /* renamed from: a  reason: collision with root package name */
        T f2467a;
        com.xiaomi.f.a.a b;
        boolean c;

        a() {
        }
    }

    private m(Context context) {
        this.c = context.getApplicationContext();
        this.d = null;
        this.f2466a = g();
    }

    public static m a(Context context) {
        if (b == null) {
            b = new m(context);
        }
        return b;
    }

    private boolean g() {
        try {
            PackageInfo packageInfo = this.c.getPackageManager().getPackageInfo("com.xiaomi.xmsf", 4);
            return packageInfo != null && packageInfo.versionCode >= 105;
        } catch (Exception e2) {
            return false;
        }
    }

    private Intent h() {
        Intent intent = new Intent();
        String packageName = this.c.getPackageName();
        if (!b() || "com.xiaomi.xmsf".equals(packageName)) {
            k();
            intent.setComponent(new ComponentName(this.c, "com.xiaomi.push.service.XMPushService"));
            intent.putExtra("mipush_app_package", packageName);
        } else {
            intent.setPackage("com.xiaomi.xmsf");
            intent.setClassName("com.xiaomi.xmsf", i());
            intent.putExtra("mipush_app_package", packageName);
            j();
        }
        return intent;
    }

    private String i() {
        try {
            return this.c.getPackageManager().getPackageInfo("com.xiaomi.xmsf", 4).versionCode >= 106 ? "com.xiaomi.push.service.XMPushService" : "com.xiaomi.xmsf.push.service.XMPushService";
        } catch (Exception e2) {
        }
    }

    private void j() {
        try {
            this.c.getPackageManager().setComponentEnabledSetting(new ComponentName(this.c, "com.xiaomi.push.service.XMPushService"), 2, 1);
        } catch (Throwable th) {
        }
    }

    private void k() {
        try {
            this.c.getPackageManager().setComponentEnabledSetting(new ComponentName(this.c, "com.xiaomi.push.service.XMPushService"), 1, 1);
        } catch (Throwable th) {
        }
    }

    private boolean l() {
        String packageName = this.c.getPackageName();
        return packageName.contains("miui") || packageName.contains("xiaomi") || (this.c.getApplicationInfo().flags & 1) != 0;
    }

    public void a() {
        this.c.startService(h());
    }

    public void a(int i) {
        Intent h = h();
        h.setAction("com.xiaomi.mipush.CLEAR_NOTIFICATION");
        h.putExtra(aq.y, this.c.getPackageName());
        h.putExtra(aq.z, i);
        this.c.startService(h);
    }

    public final void a(j jVar, boolean z) {
        this.f = null;
        Intent h = h();
        byte[] a2 = u.a(k.a(this.c, jVar, com.xiaomi.f.a.a.Registration));
        if (a2 == null) {
            c.a("register fail, because msgBytes is null.");
            return;
        }
        h.setAction("com.xiaomi.mipush.REGISTER_APP");
        h.putExtra("mipush_app_id", g.a(this.c).c());
        h.putExtra("mipush_payload", a2);
        h.putExtra("mipush_session", this.d);
        h.putExtra("mipush_env_chanage", z);
        h.putExtra("mipush_env_type", g.a(this.c).m());
        if (!d.d(this.c) || !f()) {
            this.f = h;
        } else {
            this.c.startService(h);
        }
    }

    public final void a(q qVar) {
        Intent h = h();
        byte[] a2 = u.a(k.a(this.c, qVar, com.xiaomi.f.a.a.UnRegistration));
        if (a2 == null) {
            c.a("unregister fail, because msgBytes is null.");
            return;
        }
        h.setAction("com.xiaomi.mipush.UNREGISTER_APP");
        h.putExtra("mipush_app_id", g.a(this.c).c());
        h.putExtra("mipush_payload", a2);
        this.c.startService(h);
    }

    public final <T extends b<T, ?>> void a(b bVar, com.xiaomi.f.a.a aVar, com.xiaomi.f.a.c cVar) {
        a(bVar, aVar, !aVar.equals(com.xiaomi.f.a.a.Registration), cVar);
    }

    public <T extends b<T, ?>> void a(b bVar, com.xiaomi.f.a.a aVar, boolean z) {
        a aVar2 = new a();
        aVar2.f2467a = bVar;
        aVar2.b = aVar;
        aVar2.c = z;
        synchronized (e) {
            e.add(aVar2);
            if (e.size() > 10) {
                e.remove(0);
            }
        }
    }

    public final <T extends b<T, ?>> void a(T t, com.xiaomi.f.a.a aVar, boolean z, com.xiaomi.f.a.c cVar) {
        a(t, aVar, z, true, cVar, true);
    }

    public final <T extends b<T, ?>> void a(T t, com.xiaomi.f.a.a aVar, boolean z, com.xiaomi.f.a.c cVar, boolean z2) {
        a(t, aVar, z, true, cVar, z2);
    }

    public final <T extends b<T, ?>> void a(T t, com.xiaomi.f.a.a aVar, boolean z, boolean z2, com.xiaomi.f.a.c cVar, boolean z3) {
        a(t, aVar, z, z2, cVar, z3, this.c.getPackageName(), g.a(this.c).c());
    }

    public final <T extends b<T, ?>> void a(T t, com.xiaomi.f.a.a aVar, boolean z, boolean z2, com.xiaomi.f.a.c cVar, boolean z3, String str, String str2) {
        if (g.a(this.c).i()) {
            Intent h = h();
            h a2 = k.a(this.c, t, aVar, z, str, str2);
            if (cVar != null) {
                a2.a(cVar);
            }
            byte[] a3 = u.a(a2);
            if (a3 == null) {
                c.a("send message fail, because msgBytes is null.");
                return;
            }
            h.setAction("com.xiaomi.mipush.SEND_MESSAGE");
            h.putExtra("mipush_payload", a3);
            h.putExtra("com.xiaomi.mipush.MESSAGE_CACHE", z3);
            this.c.startService(h);
        } else if (z2) {
            a(t, aVar, z);
        } else {
            c.a("drop the message before initialization.");
        }
    }

    public boolean b() {
        return this.f2466a && 1 == g.a(this.c).m();
    }

    public void c() {
        if (this.f != null) {
            this.c.startService(this.f);
            this.f = null;
        }
    }

    public void d() {
        synchronized (e) {
            Iterator<a> it = e.iterator();
            while (it.hasNext()) {
                a next = it.next();
                a(next.f2467a, next.b, next.c, false, null, true);
            }
            e.clear();
        }
    }

    public void e() {
        Intent h = h();
        h.setAction("com.xiaomi.mipush.SET_NOTIFICATION_TYPE");
        h.putExtra(aq.y, this.c.getPackageName());
        h.putExtra(aq.C, com.xiaomi.a.a.g.c.b(this.c.getPackageName()));
        this.c.startService(h);
    }

    public boolean f() {
        if (!b() || !l()) {
            return true;
        }
        if (this.g == null) {
            this.g = Integer.valueOf(com.xiaomi.push.service.c.a(this.c).b());
            if (this.g.intValue() == 0) {
                this.c.getContentResolver().registerContentObserver(com.xiaomi.push.service.c.a(this.c).c(), false, new n(this, new Handler(Looper.getMainLooper())));
            }
        }
        return this.g.intValue() != 0;
    }
}
