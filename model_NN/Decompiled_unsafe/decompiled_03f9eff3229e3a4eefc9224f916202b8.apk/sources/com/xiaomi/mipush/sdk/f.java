package com.xiaomi.mipush.sdk;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.text.TextUtils;
import com.xiaomi.f.a.c;
import com.xiaomi.f.a.n;
import java.util.List;

public class f {

    /* renamed from: a  reason: collision with root package name */
    private static int f2457a = 0;

    public static int a(Context context) {
        if (f2457a == 0) {
            if (b(context)) {
                a(1);
            } else {
                a(2);
            }
        }
        return f2457a;
    }

    public static d a(String str, List<String> list, long j, String str2, String str3) {
        d dVar = new d();
        dVar.a(str);
        dVar.a(list);
        dVar.a(j);
        dVar.b(str2);
        dVar.c(str3);
        return dVar;
    }

    public static e a(n nVar, c cVar, boolean z) {
        e eVar = new e();
        eVar.a(nVar.c());
        if (!TextUtils.isEmpty(nVar.j())) {
            eVar.a(1);
            eVar.c(nVar.j());
        } else if (!TextUtils.isEmpty(nVar.h())) {
            eVar.a(2);
            eVar.e(nVar.h());
        } else if (!TextUtils.isEmpty(nVar.r())) {
            eVar.a(3);
            eVar.d(nVar.r());
        } else {
            eVar.a(0);
        }
        eVar.h(nVar.p());
        if (nVar.l() != null) {
            eVar.b(nVar.l().f());
        }
        if (cVar != null) {
            if (TextUtils.isEmpty(eVar.a())) {
                eVar.a(cVar.b());
            }
            if (TextUtils.isEmpty(eVar.e())) {
                eVar.e(cVar.f());
            }
            eVar.f(cVar.j());
            eVar.g(cVar.h());
            eVar.b(cVar.l());
            eVar.c(cVar.q());
            eVar.d(cVar.o());
            eVar.a(cVar.s());
        }
        eVar.b(z);
        return eVar;
    }

    private static void a(int i) {
        f2457a = i;
    }

    public static void a(Context context, d dVar) {
        Intent intent = new Intent("com.xiaomi.mipush.RECEIVE_MESSAGE");
        intent.setPackage(context.getPackageName());
        intent.putExtra("message_type", 3);
        intent.putExtra("key_command", dVar);
        new PushServiceReceiver().onReceive(context, intent);
    }

    private static boolean a(Context context, Intent intent) {
        try {
            List<ResolveInfo> queryBroadcastReceivers = context.getPackageManager().queryBroadcastReceivers(intent, 32);
            return queryBroadcastReceivers != null && !queryBroadcastReceivers.isEmpty();
        } catch (Exception e) {
            return true;
        }
    }

    public static boolean b(Context context) {
        Intent intent = new Intent("com.xiaomi.mipush.RECEIVE_MESSAGE");
        intent.setClassName(context.getPackageName(), "com.xiaomi.mipush.sdk.PushServiceReceiver");
        return a(context, intent);
    }
}
