package com.xiaomi.mipush.sdk;

import android.content.Context;
import android.text.TextUtils;
import com.xiaomi.a.a.c.c;
import com.xiaomi.f.a.a;
import com.xiaomi.f.a.d;
import com.xiaomi.f.a.e;
import com.xiaomi.f.a.g;
import com.xiaomi.f.a.h;
import com.xiaomi.f.a.i;
import com.xiaomi.f.a.m;
import com.xiaomi.f.a.n;
import com.xiaomi.f.a.p;
import com.xiaomi.f.a.r;
import com.xiaomi.f.a.t;
import com.xiaomi.f.a.u;
import java.nio.ByteBuffer;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.apache.a.b;
import org.apache.a.f;

public class k {

    /* renamed from: a  reason: collision with root package name */
    private static final byte[] f2462a = {100, 23, 84, 114, 72, 0, 4, 97, 73, 97, 2, 52, 84, 102, 18, 32};

    protected static <T extends b<T, ?>> h a(Context context, T t, a aVar) {
        return a(context, t, aVar, !aVar.equals(a.Registration), context.getPackageName(), g.a(context).c());
    }

    protected static <T extends b<T, ?>> h a(Context context, T t, a aVar, boolean z, String str, String str2) {
        byte[] a2 = u.a(t);
        if (a2 == null) {
            c.a("invoke convertThriftObjectToBytes method, return null.");
            return null;
        }
        h hVar = new h();
        if (z) {
            String f = g.a(context).f();
            if (TextUtils.isEmpty(f)) {
                c.a("regSecret is empty, return null");
                return null;
            }
            try {
                a2 = b(com.xiaomi.a.a.g.a.a(f), a2);
            } catch (Exception e) {
                c.d("encryption error. ");
            }
        }
        d dVar = new d();
        dVar.f2415a = 5;
        dVar.b = "fakeid";
        hVar.a(dVar);
        hVar.a(ByteBuffer.wrap(a2));
        hVar.a(aVar);
        hVar.c(true);
        hVar.b(str);
        hVar.a(z);
        hVar.a(str2);
        return hVar;
    }

    private static Cipher a(byte[] bArr, int i) {
        SecretKeySpec secretKeySpec = new SecretKeySpec(bArr, "AES");
        IvParameterSpec ivParameterSpec = new IvParameterSpec(f2462a);
        Cipher instance = Cipher.getInstance("AES/CBC/PKCS5Padding");
        instance.init(i, secretKeySpec, ivParameterSpec);
        return instance;
    }

    protected static b a(Context context, h hVar) {
        byte[] f;
        if (hVar.c()) {
            try {
                f = a(com.xiaomi.a.a.g.a.a(g.a(context).f()), hVar.f());
            } catch (Exception e) {
                throw new f("the aes decrypt failed.", e);
            }
        } else {
            f = hVar.f();
        }
        b a2 = a(hVar.a());
        if (a2 != null) {
            u.a(a2, f);
        }
        return a2;
    }

    private static b a(a aVar) {
        switch (aVar) {
            case Registration:
                return new com.xiaomi.f.a.k();
            case UnRegistration:
                return new r();
            case Subscription:
                return new p();
            case UnSubscription:
                return new t();
            case SendMessage:
                return new n();
            case AckMessage:
                return new e();
            case SetConfig:
                return new g();
            case ReportFeedback:
                return new m();
            case Notification:
                return new i();
            case Command:
                return new g();
            default:
                return null;
        }
    }

    public static byte[] a(byte[] bArr, byte[] bArr2) {
        return a(bArr, 2).doFinal(bArr2);
    }

    public static byte[] b(byte[] bArr, byte[] bArr2) {
        return a(bArr, 1).doFinal(bArr2);
    }
}
