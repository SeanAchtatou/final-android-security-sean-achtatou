package com.xiaomi.mipush.sdk;

import android.database.ContentObserver;
import android.os.Handler;
import com.xiaomi.a.a.e.d;
import com.xiaomi.push.service.c;

class n extends ContentObserver {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ m f2468a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    n(m mVar, Handler handler) {
        super(handler);
        this.f2468a = mVar;
    }

    public void onChange(boolean z) {
        Integer unused = this.f2468a.g = Integer.valueOf(c.a(this.f2468a.c).b());
        if (this.f2468a.g.intValue() != 0) {
            this.f2468a.c.getContentResolver().unregisterContentObserver(this);
            if (d.d(this.f2468a.c)) {
                this.f2468a.c();
            }
        }
    }
}
