package com.xiaomi.b.a.a.a;

import com.sina.weibo.sdk.component.WidgetRequestParam;
import java.io.Serializable;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.apache.a.a.g;
import org.apache.a.b;
import org.apache.a.b.f;
import org.apache.a.b.i;
import org.apache.a.b.k;

public class c implements Serializable, Cloneable, b<c, a> {

    /* renamed from: a  reason: collision with root package name */
    public static final Map<a, org.apache.a.a.b> f2347a;
    private static final k b = new k("HttpLog");
    private static final org.apache.a.b.c c = new org.apache.a.b.c("common", (byte) 12, 1);
    private static final org.apache.a.b.c d = new org.apache.a.b.c(WidgetRequestParam.REQ_PARAM_COMMENT_CATEGORY, (byte) 11, 2);
    private static final org.apache.a.b.c e = new org.apache.a.b.c("httpApi", (byte) 12, 3);
    private static final org.apache.a.b.c f = new org.apache.a.b.c("passport", (byte) 12, 4);
    private com.xiaomi.b.a.a.a g;
    private String h = "";
    private b i;
    private f j;

    public enum a {
        COMMON(1, "common"),
        CATEGORY(2, WidgetRequestParam.REQ_PARAM_COMMENT_CATEGORY),
        HTTP_API(3, "httpApi"),
        PASSPORT(4, "passport");
        
        private static final Map<String, a> e = new HashMap();
        private final short f;
        private final String g;

        static {
            Iterator it = EnumSet.allOf(a.class).iterator();
            while (it.hasNext()) {
                a aVar = (a) it.next();
                e.put(aVar.a(), aVar);
            }
        }

        private a(short s, String str) {
            this.f = s;
            this.g = str;
        }

        public String a() {
            return this.g;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [com.xiaomi.b.a.a.a.c$a, org.apache.a.a.b]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        EnumMap enumMap = new EnumMap(a.class);
        enumMap.put((Object) a.COMMON, (Object) new org.apache.a.a.b("common", (byte) 1, new g((byte) 12, com.xiaomi.b.a.a.a.class)));
        enumMap.put((Object) a.CATEGORY, (Object) new org.apache.a.a.b(WidgetRequestParam.REQ_PARAM_COMMENT_CATEGORY, (byte) 1, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.HTTP_API, (Object) new org.apache.a.a.b("httpApi", (byte) 2, new g((byte) 12, b.class)));
        enumMap.put((Object) a.PASSPORT, (Object) new org.apache.a.a.b("passport", (byte) 2, new g((byte) 12, f.class)));
        f2347a = Collections.unmodifiableMap(enumMap);
        org.apache.a.a.b.a(c.class, f2347a);
    }

    public c a(b bVar) {
        this.i = bVar;
        return this;
    }

    public c a(com.xiaomi.b.a.a.a aVar) {
        this.g = aVar;
        return this;
    }

    public c a(String str) {
        this.h = str;
        return this;
    }

    public void a(f fVar) {
        fVar.g();
        while (true) {
            org.apache.a.b.c i2 = fVar.i();
            if (i2.b == 0) {
                fVar.h();
                e();
                return;
            }
            switch (i2.c) {
                case 1:
                    if (i2.b != 12) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.g = new com.xiaomi.b.a.a.a();
                        this.g.a(fVar);
                        break;
                    }
                case 2:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.h = fVar.w();
                        break;
                    }
                case 3:
                    if (i2.b != 12) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.i = new b();
                        this.i.a(fVar);
                        break;
                    }
                case 4:
                    if (i2.b != 12) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.j = new f();
                        this.j.a(fVar);
                        break;
                    }
                default:
                    i.a(fVar, i2.b);
                    break;
            }
            fVar.j();
        }
    }

    public boolean a() {
        return this.g != null;
    }

    public boolean a(c cVar) {
        if (cVar == null) {
            return false;
        }
        boolean a2 = a();
        boolean a3 = cVar.a();
        if ((a2 || a3) && (!a2 || !a3 || !this.g.a(cVar.g))) {
            return false;
        }
        boolean b2 = b();
        boolean b3 = cVar.b();
        if ((b2 || b3) && (!b2 || !b3 || !this.h.equals(cVar.h))) {
            return false;
        }
        boolean c2 = c();
        boolean c3 = cVar.c();
        if ((c2 || c3) && (!c2 || !c3 || !this.i.a(cVar.i))) {
            return false;
        }
        boolean d2 = d();
        boolean d3 = cVar.d();
        return (!d2 && !d3) || (d2 && d3 && this.j.a(cVar.j));
    }

    /* renamed from: b */
    public int compareTo(c cVar) {
        int a2;
        int a3;
        int a4;
        int a5;
        if (!getClass().equals(cVar.getClass())) {
            return getClass().getName().compareTo(cVar.getClass().getName());
        }
        int compareTo = Boolean.valueOf(a()).compareTo(Boolean.valueOf(cVar.a()));
        if (compareTo != 0) {
            return compareTo;
        }
        if (a() && (a5 = org.apache.a.c.a(this.g, cVar.g)) != 0) {
            return a5;
        }
        int compareTo2 = Boolean.valueOf(b()).compareTo(Boolean.valueOf(cVar.b()));
        if (compareTo2 != 0) {
            return compareTo2;
        }
        if (b() && (a4 = org.apache.a.c.a(this.h, cVar.h)) != 0) {
            return a4;
        }
        int compareTo3 = Boolean.valueOf(c()).compareTo(Boolean.valueOf(cVar.c()));
        if (compareTo3 != 0) {
            return compareTo3;
        }
        if (c() && (a3 = org.apache.a.c.a(this.i, cVar.i)) != 0) {
            return a3;
        }
        int compareTo4 = Boolean.valueOf(d()).compareTo(Boolean.valueOf(cVar.d()));
        if (compareTo4 != 0) {
            return compareTo4;
        }
        if (!d() || (a2 = org.apache.a.c.a(this.j, cVar.j)) == 0) {
            return 0;
        }
        return a2;
    }

    public void b(f fVar) {
        e();
        fVar.a(b);
        if (this.g != null) {
            fVar.a(c);
            this.g.b(fVar);
            fVar.b();
        }
        if (this.h != null) {
            fVar.a(d);
            fVar.a(this.h);
            fVar.b();
        }
        if (this.i != null && c()) {
            fVar.a(e);
            this.i.b(fVar);
            fVar.b();
        }
        if (this.j != null && d()) {
            fVar.a(f);
            this.j.b(fVar);
            fVar.b();
        }
        fVar.c();
        fVar.a();
    }

    public boolean b() {
        return this.h != null;
    }

    public boolean c() {
        return this.i != null;
    }

    public boolean d() {
        return this.j != null;
    }

    public void e() {
        if (this.g == null) {
            throw new org.apache.a.b.g("Required field 'common' was not present! Struct: " + toString());
        } else if (this.h == null) {
            throw new org.apache.a.b.g("Required field 'category' was not present! Struct: " + toString());
        }
    }

    public boolean equals(Object obj) {
        if (obj != null && (obj instanceof c)) {
            return a((c) obj);
        }
        return false;
    }

    public int hashCode() {
        return 0;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("HttpLog(");
        sb.append("common:");
        if (this.g == null) {
            sb.append("null");
        } else {
            sb.append(this.g);
        }
        sb.append(", ");
        sb.append("category:");
        if (this.h == null) {
            sb.append("null");
        } else {
            sb.append(this.h);
        }
        if (c()) {
            sb.append(", ");
            sb.append("httpApi:");
            if (this.i == null) {
                sb.append("null");
            } else {
                sb.append(this.i);
            }
        }
        if (d()) {
            sb.append(", ");
            sb.append("passport:");
            if (this.j == null) {
                sb.append("null");
            } else {
                sb.append(this.j);
            }
        }
        sb.append(")");
        return sb.toString();
    }
}
