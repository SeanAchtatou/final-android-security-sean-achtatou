package com.xiaomi.b.a.a.a;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.a.a.d;
import org.apache.a.b;
import org.apache.a.b.c;
import org.apache.a.b.f;
import org.apache.a.b.i;
import org.apache.a.b.k;

public class g implements Serializable, Cloneable, b<g, a> {

    /* renamed from: a  reason: collision with root package name */
    public static final Map<a, org.apache.a.a.b> f2355a;
    private static final k b = new k("PassportHostInfo");
    private static final c c = new c("host", (byte) 11, 1);
    private static final c d = new c("land_node_info", (byte) 15, 2);
    private String e;
    private List<h> f;

    public enum a {
        HOST(1, "host"),
        LAND_NODE_INFO(2, "land_node_info");
        
        private static final Map<String, a> c = new HashMap();
        private final short d;
        private final String e;

        static {
            Iterator it = EnumSet.allOf(a.class).iterator();
            while (it.hasNext()) {
                a aVar = (a) it.next();
                c.put(aVar.a(), aVar);
            }
        }

        private a(short s, String str) {
            this.d = s;
            this.e = str;
        }

        public String a() {
            return this.e;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [com.xiaomi.b.a.a.a.g$a, org.apache.a.a.b]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        EnumMap enumMap = new EnumMap(a.class);
        enumMap.put((Object) a.HOST, (Object) new org.apache.a.a.b("host", (byte) 1, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.LAND_NODE_INFO, (Object) new org.apache.a.a.b("land_node_info", (byte) 1, new d((byte) 15, new org.apache.a.a.g((byte) 12, h.class))));
        f2355a = Collections.unmodifiableMap(enumMap);
        org.apache.a.a.b.a(g.class, f2355a);
    }

    public void a(f fVar) {
        fVar.g();
        while (true) {
            c i = fVar.i();
            if (i.b == 0) {
                fVar.h();
                c();
                return;
            }
            switch (i.c) {
                case 1:
                    if (i.b != 11) {
                        i.a(fVar, i.b);
                        break;
                    } else {
                        this.e = fVar.w();
                        break;
                    }
                case 2:
                    if (i.b != 15) {
                        i.a(fVar, i.b);
                        break;
                    } else {
                        org.apache.a.b.d m = fVar.m();
                        this.f = new ArrayList(m.b);
                        for (int i2 = 0; i2 < m.b; i2++) {
                            h hVar = new h();
                            hVar.a(fVar);
                            this.f.add(hVar);
                        }
                        fVar.n();
                        break;
                    }
                default:
                    i.a(fVar, i.b);
                    break;
            }
            fVar.j();
        }
    }

    public boolean a() {
        return this.e != null;
    }

    public boolean a(g gVar) {
        if (gVar == null) {
            return false;
        }
        boolean a2 = a();
        boolean a3 = gVar.a();
        if ((a2 || a3) && (!a2 || !a3 || !this.e.equals(gVar.e))) {
            return false;
        }
        boolean b2 = b();
        boolean b3 = gVar.b();
        return (!b2 && !b3) || (b2 && b3 && this.f.equals(gVar.f));
    }

    /* renamed from: b */
    public int compareTo(g gVar) {
        int a2;
        int a3;
        if (!getClass().equals(gVar.getClass())) {
            return getClass().getName().compareTo(gVar.getClass().getName());
        }
        int compareTo = Boolean.valueOf(a()).compareTo(Boolean.valueOf(gVar.a()));
        if (compareTo != 0) {
            return compareTo;
        }
        if (a() && (a3 = org.apache.a.c.a(this.e, gVar.e)) != 0) {
            return a3;
        }
        int compareTo2 = Boolean.valueOf(b()).compareTo(Boolean.valueOf(gVar.b()));
        if (compareTo2 != 0) {
            return compareTo2;
        }
        if (!b() || (a2 = org.apache.a.c.a(this.f, gVar.f)) == 0) {
            return 0;
        }
        return a2;
    }

    public void b(f fVar) {
        c();
        fVar.a(b);
        if (this.e != null) {
            fVar.a(c);
            fVar.a(this.e);
            fVar.b();
        }
        if (this.f != null) {
            fVar.a(d);
            fVar.a(new org.apache.a.b.d((byte) 12, this.f.size()));
            for (h b2 : this.f) {
                b2.b(fVar);
            }
            fVar.e();
            fVar.b();
        }
        fVar.c();
        fVar.a();
    }

    public boolean b() {
        return this.f != null;
    }

    public void c() {
        if (this.e == null) {
            throw new org.apache.a.b.g("Required field 'host' was not present! Struct: " + toString());
        } else if (this.f == null) {
            throw new org.apache.a.b.g("Required field 'land_node_info' was not present! Struct: " + toString());
        }
    }

    public boolean equals(Object obj) {
        if (obj != null && (obj instanceof g)) {
            return a((g) obj);
        }
        return false;
    }

    public int hashCode() {
        return 0;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("PassportHostInfo(");
        sb.append("host:");
        if (this.e == null) {
            sb.append("null");
        } else {
            sb.append(this.e);
        }
        sb.append(", ");
        sb.append("land_node_info:");
        if (this.f == null) {
            sb.append("null");
        } else {
            sb.append(this.f);
        }
        sb.append(")");
        return sb.toString();
    }
}
