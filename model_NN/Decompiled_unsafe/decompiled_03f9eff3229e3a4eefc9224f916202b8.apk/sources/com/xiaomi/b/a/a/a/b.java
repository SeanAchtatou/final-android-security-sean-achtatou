package com.xiaomi.b.a.a.a;

import com.sina.weibo.sdk.component.WidgetRequestParam;
import com.tencent.open.GameAppOperation;
import java.io.Serializable;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.apache.a.a.f;
import org.apache.a.a.g;
import org.apache.a.b.c;
import org.apache.a.b.i;
import org.apache.a.b.j;
import org.apache.a.b.k;

public class b implements Serializable, Cloneable, org.apache.a.b<b, a> {

    /* renamed from: a  reason: collision with root package name */
    public static final Map<a, org.apache.a.a.b> f2345a;
    private static final k b = new k("HttpApi");
    private static final c c = new c(WidgetRequestParam.REQ_PARAM_COMMENT_CATEGORY, (byte) 11, 1);
    private static final c d = new c("uuid", (byte) 11, 2);
    private static final c e = new c("version", (byte) 11, 3);
    private static final c f = new c("network", (byte) 11, 4);
    private static final c g = new c("client_ip", (byte) 11, 5);
    private static final c h = new c("location", (byte) 12, 6);
    private static final c i = new c("host_info", (byte) 14, 7);
    private static final c j = new c("version_type", (byte) 11, 8);
    private static final c k = new c(GameAppOperation.QQFAV_DATALINE_APPNAME, (byte) 11, 9);
    private static final c l = new c("app_version", (byte) 11, 10);
    private String m = "";
    private String n;
    private String o;
    private String p;
    private String q;
    private e r;
    private Set<a> s;
    private String t = "";
    private String u = "";
    private String v = "";

    public enum a {
        CATEGORY(1, WidgetRequestParam.REQ_PARAM_COMMENT_CATEGORY),
        UUID(2, "uuid"),
        VERSION(3, "version"),
        NETWORK(4, "network"),
        CLIENT_IP(5, "client_ip"),
        LOCATION(6, "location"),
        HOST_INFO(7, "host_info"),
        VERSION_TYPE(8, "version_type"),
        APP_NAME(9, GameAppOperation.QQFAV_DATALINE_APPNAME),
        APP_VERSION(10, "app_version");
        
        private static final Map<String, a> k = new HashMap();
        private final short l;
        private final String m;

        static {
            Iterator it = EnumSet.allOf(a.class).iterator();
            while (it.hasNext()) {
                a aVar = (a) it.next();
                k.put(aVar.a(), aVar);
            }
        }

        private a(short s, String str) {
            this.l = s;
            this.m = str;
        }

        public String a() {
            return this.m;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [com.xiaomi.b.a.a.a.b$a, org.apache.a.a.b]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        EnumMap enumMap = new EnumMap(a.class);
        enumMap.put((Object) a.CATEGORY, (Object) new org.apache.a.a.b(WidgetRequestParam.REQ_PARAM_COMMENT_CATEGORY, (byte) 1, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.UUID, (Object) new org.apache.a.a.b("uuid", (byte) 1, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.VERSION, (Object) new org.apache.a.a.b("version", (byte) 1, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.NETWORK, (Object) new org.apache.a.a.b("network", (byte) 1, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.CLIENT_IP, (Object) new org.apache.a.a.b("client_ip", (byte) 2, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.LOCATION, (Object) new org.apache.a.a.b("location", (byte) 2, new g((byte) 12, e.class)));
        enumMap.put((Object) a.HOST_INFO, (Object) new org.apache.a.a.b("host_info", (byte) 2, new f((byte) 14, new g((byte) 12, a.class))));
        enumMap.put((Object) a.VERSION_TYPE, (Object) new org.apache.a.a.b("version_type", (byte) 2, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.APP_NAME, (Object) new org.apache.a.a.b(GameAppOperation.QQFAV_DATALINE_APPNAME, (byte) 2, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.APP_VERSION, (Object) new org.apache.a.a.b("app_version", (byte) 2, new org.apache.a.a.c((byte) 11)));
        f2345a = Collections.unmodifiableMap(enumMap);
        org.apache.a.a.b.a(b.class, f2345a);
    }

    public b a(e eVar) {
        this.r = eVar;
        return this;
    }

    public b a(String str) {
        this.m = str;
        return this;
    }

    public void a(a aVar) {
        if (this.s == null) {
            this.s = new HashSet();
        }
        this.s.add(aVar);
    }

    public void a(org.apache.a.b.f fVar) {
        fVar.g();
        while (true) {
            c i2 = fVar.i();
            if (i2.b == 0) {
                fVar.h();
                l();
                return;
            }
            switch (i2.c) {
                case 1:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.m = fVar.w();
                        break;
                    }
                case 2:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.n = fVar.w();
                        break;
                    }
                case 3:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.o = fVar.w();
                        break;
                    }
                case 4:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.p = fVar.w();
                        break;
                    }
                case 5:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.q = fVar.w();
                        break;
                    }
                case 6:
                    if (i2.b != 12) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.r = new e();
                        this.r.a(fVar);
                        break;
                    }
                case 7:
                    if (i2.b != 14) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        j o2 = fVar.o();
                        this.s = new HashSet(o2.b * 2);
                        for (int i3 = 0; i3 < o2.b; i3++) {
                            a aVar = new a();
                            aVar.a(fVar);
                            this.s.add(aVar);
                        }
                        fVar.p();
                        break;
                    }
                case 8:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.t = fVar.w();
                        break;
                    }
                case 9:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.u = fVar.w();
                        break;
                    }
                case 10:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.v = fVar.w();
                        break;
                    }
                default:
                    i.a(fVar, i2.b);
                    break;
            }
            fVar.j();
        }
    }

    public boolean a() {
        return this.m != null;
    }

    public boolean a(b bVar) {
        if (bVar == null) {
            return false;
        }
        boolean a2 = a();
        boolean a3 = bVar.a();
        if ((a2 || a3) && (!a2 || !a3 || !this.m.equals(bVar.m))) {
            return false;
        }
        boolean b2 = b();
        boolean b3 = bVar.b();
        if ((b2 || b3) && (!b2 || !b3 || !this.n.equals(bVar.n))) {
            return false;
        }
        boolean c2 = c();
        boolean c3 = bVar.c();
        if ((c2 || c3) && (!c2 || !c3 || !this.o.equals(bVar.o))) {
            return false;
        }
        boolean d2 = d();
        boolean d3 = bVar.d();
        if ((d2 || d3) && (!d2 || !d3 || !this.p.equals(bVar.p))) {
            return false;
        }
        boolean e2 = e();
        boolean e3 = bVar.e();
        if ((e2 || e3) && (!e2 || !e3 || !this.q.equals(bVar.q))) {
            return false;
        }
        boolean f2 = f();
        boolean f3 = bVar.f();
        if ((f2 || f3) && (!f2 || !f3 || !this.r.a(bVar.r))) {
            return false;
        }
        boolean h2 = h();
        boolean h3 = bVar.h();
        if ((h2 || h3) && (!h2 || !h3 || !this.s.equals(bVar.s))) {
            return false;
        }
        boolean i2 = i();
        boolean i3 = bVar.i();
        if ((i2 || i3) && (!i2 || !i3 || !this.t.equals(bVar.t))) {
            return false;
        }
        boolean j2 = j();
        boolean j3 = bVar.j();
        if ((j2 || j3) && (!j2 || !j3 || !this.u.equals(bVar.u))) {
            return false;
        }
        boolean k2 = k();
        boolean k3 = bVar.k();
        return (!k2 && !k3) || (k2 && k3 && this.v.equals(bVar.v));
    }

    /* renamed from: b */
    public int compareTo(b bVar) {
        int a2;
        int a3;
        int a4;
        int a5;
        int a6;
        int a7;
        int a8;
        int a9;
        int a10;
        int a11;
        if (!getClass().equals(bVar.getClass())) {
            return getClass().getName().compareTo(bVar.getClass().getName());
        }
        int compareTo = Boolean.valueOf(a()).compareTo(Boolean.valueOf(bVar.a()));
        if (compareTo != 0) {
            return compareTo;
        }
        if (a() && (a11 = org.apache.a.c.a(this.m, bVar.m)) != 0) {
            return a11;
        }
        int compareTo2 = Boolean.valueOf(b()).compareTo(Boolean.valueOf(bVar.b()));
        if (compareTo2 != 0) {
            return compareTo2;
        }
        if (b() && (a10 = org.apache.a.c.a(this.n, bVar.n)) != 0) {
            return a10;
        }
        int compareTo3 = Boolean.valueOf(c()).compareTo(Boolean.valueOf(bVar.c()));
        if (compareTo3 != 0) {
            return compareTo3;
        }
        if (c() && (a9 = org.apache.a.c.a(this.o, bVar.o)) != 0) {
            return a9;
        }
        int compareTo4 = Boolean.valueOf(d()).compareTo(Boolean.valueOf(bVar.d()));
        if (compareTo4 != 0) {
            return compareTo4;
        }
        if (d() && (a8 = org.apache.a.c.a(this.p, bVar.p)) != 0) {
            return a8;
        }
        int compareTo5 = Boolean.valueOf(e()).compareTo(Boolean.valueOf(bVar.e()));
        if (compareTo5 != 0) {
            return compareTo5;
        }
        if (e() && (a7 = org.apache.a.c.a(this.q, bVar.q)) != 0) {
            return a7;
        }
        int compareTo6 = Boolean.valueOf(f()).compareTo(Boolean.valueOf(bVar.f()));
        if (compareTo6 != 0) {
            return compareTo6;
        }
        if (f() && (a6 = org.apache.a.c.a(this.r, bVar.r)) != 0) {
            return a6;
        }
        int compareTo7 = Boolean.valueOf(h()).compareTo(Boolean.valueOf(bVar.h()));
        if (compareTo7 != 0) {
            return compareTo7;
        }
        if (h() && (a5 = org.apache.a.c.a(this.s, bVar.s)) != 0) {
            return a5;
        }
        int compareTo8 = Boolean.valueOf(i()).compareTo(Boolean.valueOf(bVar.i()));
        if (compareTo8 != 0) {
            return compareTo8;
        }
        if (i() && (a4 = org.apache.a.c.a(this.t, bVar.t)) != 0) {
            return a4;
        }
        int compareTo9 = Boolean.valueOf(j()).compareTo(Boolean.valueOf(bVar.j()));
        if (compareTo9 != 0) {
            return compareTo9;
        }
        if (j() && (a3 = org.apache.a.c.a(this.u, bVar.u)) != 0) {
            return a3;
        }
        int compareTo10 = Boolean.valueOf(k()).compareTo(Boolean.valueOf(bVar.k()));
        if (compareTo10 != 0) {
            return compareTo10;
        }
        if (!k() || (a2 = org.apache.a.c.a(this.v, bVar.v)) == 0) {
            return 0;
        }
        return a2;
    }

    public b b(String str) {
        this.n = str;
        return this;
    }

    public void b(org.apache.a.b.f fVar) {
        l();
        fVar.a(b);
        if (this.m != null) {
            fVar.a(c);
            fVar.a(this.m);
            fVar.b();
        }
        if (this.n != null) {
            fVar.a(d);
            fVar.a(this.n);
            fVar.b();
        }
        if (this.o != null) {
            fVar.a(e);
            fVar.a(this.o);
            fVar.b();
        }
        if (this.p != null) {
            fVar.a(f);
            fVar.a(this.p);
            fVar.b();
        }
        if (this.q != null && e()) {
            fVar.a(g);
            fVar.a(this.q);
            fVar.b();
        }
        if (this.r != null && f()) {
            fVar.a(h);
            this.r.b(fVar);
            fVar.b();
        }
        if (this.s != null && h()) {
            fVar.a(i);
            fVar.a(new j((byte) 12, this.s.size()));
            for (a b2 : this.s) {
                b2.b(fVar);
            }
            fVar.f();
            fVar.b();
        }
        if (this.t != null && i()) {
            fVar.a(j);
            fVar.a(this.t);
            fVar.b();
        }
        if (this.u != null && j()) {
            fVar.a(k);
            fVar.a(this.u);
            fVar.b();
        }
        if (this.v != null && k()) {
            fVar.a(l);
            fVar.a(this.v);
            fVar.b();
        }
        fVar.c();
        fVar.a();
    }

    public boolean b() {
        return this.n != null;
    }

    public b c(String str) {
        this.o = str;
        return this;
    }

    public boolean c() {
        return this.o != null;
    }

    public b d(String str) {
        this.p = str;
        return this;
    }

    public boolean d() {
        return this.p != null;
    }

    public b e(String str) {
        this.q = str;
        return this;
    }

    public boolean e() {
        return this.q != null;
    }

    public boolean equals(Object obj) {
        if (obj != null && (obj instanceof b)) {
            return a((b) obj);
        }
        return false;
    }

    public b f(String str) {
        this.t = str;
        return this;
    }

    public boolean f() {
        return this.r != null;
    }

    public int g() {
        if (this.s == null) {
            return 0;
        }
        return this.s.size();
    }

    public b g(String str) {
        this.u = str;
        return this;
    }

    public b h(String str) {
        this.v = str;
        return this;
    }

    public boolean h() {
        return this.s != null;
    }

    public int hashCode() {
        return 0;
    }

    public boolean i() {
        return this.t != null;
    }

    public boolean j() {
        return this.u != null;
    }

    public boolean k() {
        return this.v != null;
    }

    public void l() {
        if (this.m == null) {
            throw new org.apache.a.b.g("Required field 'category' was not present! Struct: " + toString());
        } else if (this.n == null) {
            throw new org.apache.a.b.g("Required field 'uuid' was not present! Struct: " + toString());
        } else if (this.o == null) {
            throw new org.apache.a.b.g("Required field 'version' was not present! Struct: " + toString());
        } else if (this.p == null) {
            throw new org.apache.a.b.g("Required field 'network' was not present! Struct: " + toString());
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("HttpApi(");
        sb.append("category:");
        if (this.m == null) {
            sb.append("null");
        } else {
            sb.append(this.m);
        }
        sb.append(", ");
        sb.append("uuid:");
        if (this.n == null) {
            sb.append("null");
        } else {
            sb.append(this.n);
        }
        sb.append(", ");
        sb.append("version:");
        if (this.o == null) {
            sb.append("null");
        } else {
            sb.append(this.o);
        }
        sb.append(", ");
        sb.append("network:");
        if (this.p == null) {
            sb.append("null");
        } else {
            sb.append(this.p);
        }
        if (e()) {
            sb.append(", ");
            sb.append("client_ip:");
            if (this.q == null) {
                sb.append("null");
            } else {
                sb.append(this.q);
            }
        }
        if (f()) {
            sb.append(", ");
            sb.append("location:");
            if (this.r == null) {
                sb.append("null");
            } else {
                sb.append(this.r);
            }
        }
        if (h()) {
            sb.append(", ");
            sb.append("host_info:");
            if (this.s == null) {
                sb.append("null");
            } else {
                sb.append(this.s);
            }
        }
        if (i()) {
            sb.append(", ");
            sb.append("version_type:");
            if (this.t == null) {
                sb.append("null");
            } else {
                sb.append(this.t);
            }
        }
        if (j()) {
            sb.append(", ");
            sb.append("app_name:");
            if (this.u == null) {
                sb.append("null");
            } else {
                sb.append(this.u);
            }
        }
        if (k()) {
            sb.append(", ");
            sb.append("app_version:");
            if (this.v == null) {
                sb.append("null");
            } else {
                sb.append(this.v);
            }
        }
        sb.append(")");
        return sb.toString();
    }
}
