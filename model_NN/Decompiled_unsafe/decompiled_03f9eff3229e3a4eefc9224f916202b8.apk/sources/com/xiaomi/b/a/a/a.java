package com.xiaomi.b.a.a;

import java.io.Serializable;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.apache.a.b;
import org.apache.a.b.c;
import org.apache.a.b.f;
import org.apache.a.b.i;
import org.apache.a.b.k;

public class a implements Serializable, Cloneable, b<a, C0056a> {
    public static final Map<C0056a, org.apache.a.a.b> f;
    private static final k g = new k("Common");
    private static final c h = new c("uuid", (byte) 10, 1);
    private static final c i = new c("time", (byte) 11, 2);
    private static final c j = new c("clientIp", (byte) 11, 3);
    private static final c k = new c("serverIp", (byte) 11, 4);
    private static final c l = new c("serverHost", (byte) 11, 5);

    /* renamed from: a  reason: collision with root package name */
    public long f2341a = 0;
    public String b = "";
    public String c = "";
    public String d = "";
    public String e = "";
    private BitSet m = new BitSet(1);

    /* renamed from: com.xiaomi.b.a.a.a$a  reason: collision with other inner class name */
    public enum C0056a {
        UUID(1, "uuid"),
        TIME(2, "time"),
        CLIENT_IP(3, "clientIp"),
        SERVER_IP(4, "serverIp"),
        SERVER_HOST(5, "serverHost");
        
        private static final Map<String, C0056a> f = new HashMap();
        private final short g;
        private final String h;

        static {
            Iterator it = EnumSet.allOf(C0056a.class).iterator();
            while (it.hasNext()) {
                C0056a aVar = (C0056a) it.next();
                f.put(aVar.a(), aVar);
            }
        }

        private C0056a(short s, String str) {
            this.g = s;
            this.h = str;
        }

        public String a() {
            return this.h;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [com.xiaomi.b.a.a.a$a, org.apache.a.a.b]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        EnumMap enumMap = new EnumMap(C0056a.class);
        enumMap.put((Object) C0056a.UUID, (Object) new org.apache.a.a.b("uuid", (byte) 2, new org.apache.a.a.c((byte) 10)));
        enumMap.put((Object) C0056a.TIME, (Object) new org.apache.a.a.b("time", (byte) 2, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) C0056a.CLIENT_IP, (Object) new org.apache.a.a.b("clientIp", (byte) 2, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) C0056a.SERVER_IP, (Object) new org.apache.a.a.b("serverIp", (byte) 2, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) C0056a.SERVER_HOST, (Object) new org.apache.a.a.b("serverHost", (byte) 2, new org.apache.a.a.c((byte) 11)));
        f = Collections.unmodifiableMap(enumMap);
        org.apache.a.a.b.a(a.class, f);
    }

    public void a(f fVar) {
        fVar.g();
        while (true) {
            c i2 = fVar.i();
            if (i2.b == 0) {
                fVar.h();
                f();
                return;
            }
            switch (i2.c) {
                case 1:
                    if (i2.b != 10) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.f2341a = fVar.u();
                        a(true);
                        break;
                    }
                case 2:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.b = fVar.w();
                        break;
                    }
                case 3:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.c = fVar.w();
                        break;
                    }
                case 4:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.d = fVar.w();
                        break;
                    }
                case 5:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.e = fVar.w();
                        break;
                    }
                default:
                    i.a(fVar, i2.b);
                    break;
            }
            fVar.j();
        }
    }

    public void a(boolean z) {
        this.m.set(0, z);
    }

    public boolean a() {
        return this.m.get(0);
    }

    public boolean a(a aVar) {
        if (aVar == null) {
            return false;
        }
        boolean a2 = a();
        boolean a3 = aVar.a();
        if ((a2 || a3) && (!a2 || !a3 || this.f2341a != aVar.f2341a)) {
            return false;
        }
        boolean b2 = b();
        boolean b3 = aVar.b();
        if ((b2 || b3) && (!b2 || !b3 || !this.b.equals(aVar.b))) {
            return false;
        }
        boolean c2 = c();
        boolean c3 = aVar.c();
        if ((c2 || c3) && (!c2 || !c3 || !this.c.equals(aVar.c))) {
            return false;
        }
        boolean d2 = d();
        boolean d3 = aVar.d();
        if ((d2 || d3) && (!d2 || !d3 || !this.d.equals(aVar.d))) {
            return false;
        }
        boolean e2 = e();
        boolean e3 = aVar.e();
        return (!e2 && !e3) || (e2 && e3 && this.e.equals(aVar.e));
    }

    /* renamed from: b */
    public int compareTo(a aVar) {
        int a2;
        int a3;
        int a4;
        int a5;
        int a6;
        if (!getClass().equals(aVar.getClass())) {
            return getClass().getName().compareTo(aVar.getClass().getName());
        }
        int compareTo = Boolean.valueOf(a()).compareTo(Boolean.valueOf(aVar.a()));
        if (compareTo != 0) {
            return compareTo;
        }
        if (a() && (a6 = org.apache.a.c.a(this.f2341a, aVar.f2341a)) != 0) {
            return a6;
        }
        int compareTo2 = Boolean.valueOf(b()).compareTo(Boolean.valueOf(aVar.b()));
        if (compareTo2 != 0) {
            return compareTo2;
        }
        if (b() && (a5 = org.apache.a.c.a(this.b, aVar.b)) != 0) {
            return a5;
        }
        int compareTo3 = Boolean.valueOf(c()).compareTo(Boolean.valueOf(aVar.c()));
        if (compareTo3 != 0) {
            return compareTo3;
        }
        if (c() && (a4 = org.apache.a.c.a(this.c, aVar.c)) != 0) {
            return a4;
        }
        int compareTo4 = Boolean.valueOf(d()).compareTo(Boolean.valueOf(aVar.d()));
        if (compareTo4 != 0) {
            return compareTo4;
        }
        if (d() && (a3 = org.apache.a.c.a(this.d, aVar.d)) != 0) {
            return a3;
        }
        int compareTo5 = Boolean.valueOf(e()).compareTo(Boolean.valueOf(aVar.e()));
        if (compareTo5 != 0) {
            return compareTo5;
        }
        if (!e() || (a2 = org.apache.a.c.a(this.e, aVar.e)) == 0) {
            return 0;
        }
        return a2;
    }

    public void b(f fVar) {
        f();
        fVar.a(g);
        if (a()) {
            fVar.a(h);
            fVar.a(this.f2341a);
            fVar.b();
        }
        if (this.b != null && b()) {
            fVar.a(i);
            fVar.a(this.b);
            fVar.b();
        }
        if (this.c != null && c()) {
            fVar.a(j);
            fVar.a(this.c);
            fVar.b();
        }
        if (this.d != null && d()) {
            fVar.a(k);
            fVar.a(this.d);
            fVar.b();
        }
        if (this.e != null && e()) {
            fVar.a(l);
            fVar.a(this.e);
            fVar.b();
        }
        fVar.c();
        fVar.a();
    }

    public boolean b() {
        return this.b != null;
    }

    public boolean c() {
        return this.c != null;
    }

    public boolean d() {
        return this.d != null;
    }

    public boolean e() {
        return this.e != null;
    }

    public boolean equals(Object obj) {
        if (obj != null && (obj instanceof a)) {
            return a((a) obj);
        }
        return false;
    }

    public void f() {
    }

    public int hashCode() {
        return 0;
    }

    public String toString() {
        boolean z = false;
        StringBuilder sb = new StringBuilder("Common(");
        boolean z2 = true;
        if (a()) {
            sb.append("uuid:");
            sb.append(this.f2341a);
            z2 = false;
        }
        if (b()) {
            if (!z2) {
                sb.append(", ");
            }
            sb.append("time:");
            if (this.b == null) {
                sb.append("null");
            } else {
                sb.append(this.b);
            }
            z2 = false;
        }
        if (c()) {
            if (!z2) {
                sb.append(", ");
            }
            sb.append("clientIp:");
            if (this.c == null) {
                sb.append("null");
            } else {
                sb.append(this.c);
            }
            z2 = false;
        }
        if (d()) {
            if (!z2) {
                sb.append(", ");
            }
            sb.append("serverIp:");
            if (this.d == null) {
                sb.append("null");
            } else {
                sb.append(this.d);
            }
        } else {
            z = z2;
        }
        if (e()) {
            if (!z) {
                sb.append(", ");
            }
            sb.append("serverHost:");
            if (this.e == null) {
                sb.append("null");
            } else {
                sb.append(this.e);
            }
        }
        sb.append(")");
        return sb.toString();
    }
}
