package com.xiaomi.b.a.a.a;

import java.io.Serializable;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.apache.a.b;
import org.apache.a.b.c;
import org.apache.a.b.f;
import org.apache.a.b.g;
import org.apache.a.b.i;
import org.apache.a.b.k;

public class h implements Serializable, Cloneable, b<h, a> {

    /* renamed from: a  reason: collision with root package name */
    public static final Map<a, org.apache.a.a.b> f2357a;
    private static final k b = new k("PassportLandNodeInfo");
    private static final c c = new c("ip", (byte) 8, 1);
    private static final c d = new c("eid", (byte) 8, 2);
    private static final c e = new c("rt", (byte) 8, 3);
    private int f;
    private int g;
    private int h;
    private BitSet i = new BitSet(3);

    public enum a {
        IP(1, "ip"),
        EID(2, "eid"),
        RT(3, "rt");
        
        private static final Map<String, a> d = new HashMap();
        private final short e;
        private final String f;

        static {
            Iterator it = EnumSet.allOf(a.class).iterator();
            while (it.hasNext()) {
                a aVar = (a) it.next();
                d.put(aVar.a(), aVar);
            }
        }

        private a(short s, String str) {
            this.e = s;
            this.f = str;
        }

        public String a() {
            return this.f;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [com.xiaomi.b.a.a.a.h$a, org.apache.a.a.b]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        EnumMap enumMap = new EnumMap(a.class);
        enumMap.put((Object) a.IP, (Object) new org.apache.a.a.b("ip", (byte) 1, new org.apache.a.a.c((byte) 8)));
        enumMap.put((Object) a.EID, (Object) new org.apache.a.a.b("eid", (byte) 1, new org.apache.a.a.c((byte) 8)));
        enumMap.put((Object) a.RT, (Object) new org.apache.a.a.b("rt", (byte) 1, new org.apache.a.a.c((byte) 8)));
        f2357a = Collections.unmodifiableMap(enumMap);
        org.apache.a.a.b.a(h.class, f2357a);
    }

    public void a(f fVar) {
        fVar.g();
        while (true) {
            c i2 = fVar.i();
            if (i2.b == 0) {
                fVar.h();
                if (!a()) {
                    throw new g("Required field 'ip' was not found in serialized data! Struct: " + toString());
                } else if (!b()) {
                    throw new g("Required field 'eid' was not found in serialized data! Struct: " + toString());
                } else if (!c()) {
                    throw new g("Required field 'rt' was not found in serialized data! Struct: " + toString());
                } else {
                    d();
                    return;
                }
            } else {
                switch (i2.c) {
                    case 1:
                        if (i2.b != 8) {
                            i.a(fVar, i2.b);
                            break;
                        } else {
                            this.f = fVar.t();
                            a(true);
                            break;
                        }
                    case 2:
                        if (i2.b != 8) {
                            i.a(fVar, i2.b);
                            break;
                        } else {
                            this.g = fVar.t();
                            b(true);
                            break;
                        }
                    case 3:
                        if (i2.b != 8) {
                            i.a(fVar, i2.b);
                            break;
                        } else {
                            this.h = fVar.t();
                            c(true);
                            break;
                        }
                    default:
                        i.a(fVar, i2.b);
                        break;
                }
                fVar.j();
            }
        }
    }

    public void a(boolean z) {
        this.i.set(0, z);
    }

    public boolean a() {
        return this.i.get(0);
    }

    public boolean a(h hVar) {
        return hVar != null && this.f == hVar.f && this.g == hVar.g && this.h == hVar.h;
    }

    /* renamed from: b */
    public int compareTo(h hVar) {
        int a2;
        int a3;
        int a4;
        if (!getClass().equals(hVar.getClass())) {
            return getClass().getName().compareTo(hVar.getClass().getName());
        }
        int compareTo = Boolean.valueOf(a()).compareTo(Boolean.valueOf(hVar.a()));
        if (compareTo != 0) {
            return compareTo;
        }
        if (a() && (a4 = org.apache.a.c.a(this.f, hVar.f)) != 0) {
            return a4;
        }
        int compareTo2 = Boolean.valueOf(b()).compareTo(Boolean.valueOf(hVar.b()));
        if (compareTo2 != 0) {
            return compareTo2;
        }
        if (b() && (a3 = org.apache.a.c.a(this.g, hVar.g)) != 0) {
            return a3;
        }
        int compareTo3 = Boolean.valueOf(c()).compareTo(Boolean.valueOf(hVar.c()));
        if (compareTo3 != 0) {
            return compareTo3;
        }
        if (!c() || (a2 = org.apache.a.c.a(this.h, hVar.h)) == 0) {
            return 0;
        }
        return a2;
    }

    public void b(f fVar) {
        d();
        fVar.a(b);
        fVar.a(c);
        fVar.a(this.f);
        fVar.b();
        fVar.a(d);
        fVar.a(this.g);
        fVar.b();
        fVar.a(e);
        fVar.a(this.h);
        fVar.b();
        fVar.c();
        fVar.a();
    }

    public void b(boolean z) {
        this.i.set(1, z);
    }

    public boolean b() {
        return this.i.get(1);
    }

    public void c(boolean z) {
        this.i.set(2, z);
    }

    public boolean c() {
        return this.i.get(2);
    }

    public void d() {
    }

    public boolean equals(Object obj) {
        if (obj != null && (obj instanceof h)) {
            return a((h) obj);
        }
        return false;
    }

    public int hashCode() {
        return 0;
    }

    public String toString() {
        return "PassportLandNodeInfo(" + "ip:" + this.f + ", " + "eid:" + this.g + ", " + "rt:" + this.h + ")";
    }
}
