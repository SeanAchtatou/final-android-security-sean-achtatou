package com.xiaomi.b.a.a.a;

import com.sina.weibo.sdk.component.WidgetRequestParam;
import java.io.Serializable;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.apache.a.a.g;
import org.apache.a.b;
import org.apache.a.b.c;
import org.apache.a.b.i;
import org.apache.a.b.j;
import org.apache.a.b.k;

public class f implements Serializable, Cloneable, b<f, a> {

    /* renamed from: a  reason: collision with root package name */
    public static final Map<a, org.apache.a.a.b> f2353a;
    private static final k b = new k("Passport");
    private static final c c = new c(WidgetRequestParam.REQ_PARAM_COMMENT_CATEGORY, (byte) 11, 1);
    private static final c d = new c("uuid", (byte) 11, 2);
    private static final c e = new c("version", (byte) 11, 3);
    private static final c f = new c("network", (byte) 11, 4);
    private static final c g = new c("rid", (byte) 11, 5);
    private static final c h = new c("location", (byte) 12, 6);
    private static final c i = new c("host_info", (byte) 14, 7);
    private String j = "";
    private String k;
    private String l;
    private String m;
    private String n;
    private e o;
    private Set<g> p;

    public enum a {
        CATEGORY(1, WidgetRequestParam.REQ_PARAM_COMMENT_CATEGORY),
        UUID(2, "uuid"),
        VERSION(3, "version"),
        NETWORK(4, "network"),
        RID(5, "rid"),
        LOCATION(6, "location"),
        HOST_INFO(7, "host_info");
        
        private static final Map<String, a> h = new HashMap();
        private final short i;
        private final String j;

        static {
            Iterator it = EnumSet.allOf(a.class).iterator();
            while (it.hasNext()) {
                a aVar = (a) it.next();
                h.put(aVar.a(), aVar);
            }
        }

        private a(short s, String str) {
            this.i = s;
            this.j = str;
        }

        public String a() {
            return this.j;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [com.xiaomi.b.a.a.a.f$a, org.apache.a.a.b]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        EnumMap enumMap = new EnumMap(a.class);
        enumMap.put((Object) a.CATEGORY, (Object) new org.apache.a.a.b(WidgetRequestParam.REQ_PARAM_COMMENT_CATEGORY, (byte) 1, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.UUID, (Object) new org.apache.a.a.b("uuid", (byte) 1, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.VERSION, (Object) new org.apache.a.a.b("version", (byte) 1, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.NETWORK, (Object) new org.apache.a.a.b("network", (byte) 1, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.RID, (Object) new org.apache.a.a.b("rid", (byte) 1, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.LOCATION, (Object) new org.apache.a.a.b("location", (byte) 2, new g((byte) 12, e.class)));
        enumMap.put((Object) a.HOST_INFO, (Object) new org.apache.a.a.b("host_info", (byte) 2, new org.apache.a.a.f((byte) 14, new g((byte) 12, g.class))));
        f2353a = Collections.unmodifiableMap(enumMap);
        org.apache.a.a.b.a(f.class, f2353a);
    }

    public void a(org.apache.a.b.f fVar) {
        fVar.g();
        while (true) {
            c i2 = fVar.i();
            if (i2.b == 0) {
                fVar.h();
                h();
                return;
            }
            switch (i2.c) {
                case 1:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.j = fVar.w();
                        break;
                    }
                case 2:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.k = fVar.w();
                        break;
                    }
                case 3:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.l = fVar.w();
                        break;
                    }
                case 4:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.m = fVar.w();
                        break;
                    }
                case 5:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.n = fVar.w();
                        break;
                    }
                case 6:
                    if (i2.b != 12) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.o = new e();
                        this.o.a(fVar);
                        break;
                    }
                case 7:
                    if (i2.b != 14) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        j o2 = fVar.o();
                        this.p = new HashSet(o2.b * 2);
                        for (int i3 = 0; i3 < o2.b; i3++) {
                            g gVar = new g();
                            gVar.a(fVar);
                            this.p.add(gVar);
                        }
                        fVar.p();
                        break;
                    }
                default:
                    i.a(fVar, i2.b);
                    break;
            }
            fVar.j();
        }
    }

    public boolean a() {
        return this.j != null;
    }

    public boolean a(f fVar) {
        if (fVar == null) {
            return false;
        }
        boolean a2 = a();
        boolean a3 = fVar.a();
        if ((a2 || a3) && (!a2 || !a3 || !this.j.equals(fVar.j))) {
            return false;
        }
        boolean b2 = b();
        boolean b3 = fVar.b();
        if ((b2 || b3) && (!b2 || !b3 || !this.k.equals(fVar.k))) {
            return false;
        }
        boolean c2 = c();
        boolean c3 = fVar.c();
        if ((c2 || c3) && (!c2 || !c3 || !this.l.equals(fVar.l))) {
            return false;
        }
        boolean d2 = d();
        boolean d3 = fVar.d();
        if ((d2 || d3) && (!d2 || !d3 || !this.m.equals(fVar.m))) {
            return false;
        }
        boolean e2 = e();
        boolean e3 = fVar.e();
        if ((e2 || e3) && (!e2 || !e3 || !this.n.equals(fVar.n))) {
            return false;
        }
        boolean f2 = f();
        boolean f3 = fVar.f();
        if ((f2 || f3) && (!f2 || !f3 || !this.o.a(fVar.o))) {
            return false;
        }
        boolean g2 = g();
        boolean g3 = fVar.g();
        return (!g2 && !g3) || (g2 && g3 && this.p.equals(fVar.p));
    }

    /* renamed from: b */
    public int compareTo(f fVar) {
        int a2;
        int a3;
        int a4;
        int a5;
        int a6;
        int a7;
        int a8;
        if (!getClass().equals(fVar.getClass())) {
            return getClass().getName().compareTo(fVar.getClass().getName());
        }
        int compareTo = Boolean.valueOf(a()).compareTo(Boolean.valueOf(fVar.a()));
        if (compareTo != 0) {
            return compareTo;
        }
        if (a() && (a8 = org.apache.a.c.a(this.j, fVar.j)) != 0) {
            return a8;
        }
        int compareTo2 = Boolean.valueOf(b()).compareTo(Boolean.valueOf(fVar.b()));
        if (compareTo2 != 0) {
            return compareTo2;
        }
        if (b() && (a7 = org.apache.a.c.a(this.k, fVar.k)) != 0) {
            return a7;
        }
        int compareTo3 = Boolean.valueOf(c()).compareTo(Boolean.valueOf(fVar.c()));
        if (compareTo3 != 0) {
            return compareTo3;
        }
        if (c() && (a6 = org.apache.a.c.a(this.l, fVar.l)) != 0) {
            return a6;
        }
        int compareTo4 = Boolean.valueOf(d()).compareTo(Boolean.valueOf(fVar.d()));
        if (compareTo4 != 0) {
            return compareTo4;
        }
        if (d() && (a5 = org.apache.a.c.a(this.m, fVar.m)) != 0) {
            return a5;
        }
        int compareTo5 = Boolean.valueOf(e()).compareTo(Boolean.valueOf(fVar.e()));
        if (compareTo5 != 0) {
            return compareTo5;
        }
        if (e() && (a4 = org.apache.a.c.a(this.n, fVar.n)) != 0) {
            return a4;
        }
        int compareTo6 = Boolean.valueOf(f()).compareTo(Boolean.valueOf(fVar.f()));
        if (compareTo6 != 0) {
            return compareTo6;
        }
        if (f() && (a3 = org.apache.a.c.a(this.o, fVar.o)) != 0) {
            return a3;
        }
        int compareTo7 = Boolean.valueOf(g()).compareTo(Boolean.valueOf(fVar.g()));
        if (compareTo7 != 0) {
            return compareTo7;
        }
        if (!g() || (a2 = org.apache.a.c.a(this.p, fVar.p)) == 0) {
            return 0;
        }
        return a2;
    }

    public void b(org.apache.a.b.f fVar) {
        h();
        fVar.a(b);
        if (this.j != null) {
            fVar.a(c);
            fVar.a(this.j);
            fVar.b();
        }
        if (this.k != null) {
            fVar.a(d);
            fVar.a(this.k);
            fVar.b();
        }
        if (this.l != null) {
            fVar.a(e);
            fVar.a(this.l);
            fVar.b();
        }
        if (this.m != null) {
            fVar.a(f);
            fVar.a(this.m);
            fVar.b();
        }
        if (this.n != null) {
            fVar.a(g);
            fVar.a(this.n);
            fVar.b();
        }
        if (this.o != null && f()) {
            fVar.a(h);
            this.o.b(fVar);
            fVar.b();
        }
        if (this.p != null && g()) {
            fVar.a(i);
            fVar.a(new j((byte) 12, this.p.size()));
            for (g b2 : this.p) {
                b2.b(fVar);
            }
            fVar.f();
            fVar.b();
        }
        fVar.c();
        fVar.a();
    }

    public boolean b() {
        return this.k != null;
    }

    public boolean c() {
        return this.l != null;
    }

    public boolean d() {
        return this.m != null;
    }

    public boolean e() {
        return this.n != null;
    }

    public boolean equals(Object obj) {
        if (obj != null && (obj instanceof f)) {
            return a((f) obj);
        }
        return false;
    }

    public boolean f() {
        return this.o != null;
    }

    public boolean g() {
        return this.p != null;
    }

    public void h() {
        if (this.j == null) {
            throw new org.apache.a.b.g("Required field 'category' was not present! Struct: " + toString());
        } else if (this.k == null) {
            throw new org.apache.a.b.g("Required field 'uuid' was not present! Struct: " + toString());
        } else if (this.l == null) {
            throw new org.apache.a.b.g("Required field 'version' was not present! Struct: " + toString());
        } else if (this.m == null) {
            throw new org.apache.a.b.g("Required field 'network' was not present! Struct: " + toString());
        } else if (this.n == null) {
            throw new org.apache.a.b.g("Required field 'rid' was not present! Struct: " + toString());
        }
    }

    public int hashCode() {
        return 0;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("Passport(");
        sb.append("category:");
        if (this.j == null) {
            sb.append("null");
        } else {
            sb.append(this.j);
        }
        sb.append(", ");
        sb.append("uuid:");
        if (this.k == null) {
            sb.append("null");
        } else {
            sb.append(this.k);
        }
        sb.append(", ");
        sb.append("version:");
        if (this.l == null) {
            sb.append("null");
        } else {
            sb.append(this.l);
        }
        sb.append(", ");
        sb.append("network:");
        if (this.m == null) {
            sb.append("null");
        } else {
            sb.append(this.m);
        }
        sb.append(", ");
        sb.append("rid:");
        if (this.n == null) {
            sb.append("null");
        } else {
            sb.append(this.n);
        }
        if (f()) {
            sb.append(", ");
            sb.append("location:");
            if (this.o == null) {
                sb.append("null");
            } else {
                sb.append(this.o);
            }
        }
        if (g()) {
            sb.append(", ");
            sb.append("host_info:");
            if (this.p == null) {
                sb.append("null");
            } else {
                sb.append(this.p);
            }
        }
        sb.append(")");
        return sb.toString();
    }
}
