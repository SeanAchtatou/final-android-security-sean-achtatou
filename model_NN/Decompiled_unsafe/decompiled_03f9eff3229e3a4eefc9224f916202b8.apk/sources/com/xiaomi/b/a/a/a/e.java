package com.xiaomi.b.a.a.a;

import java.io.Serializable;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.apache.a.b;
import org.apache.a.b.c;
import org.apache.a.b.f;
import org.apache.a.b.i;
import org.apache.a.b.k;

public class e implements Serializable, Cloneable, b<e, a> {

    /* renamed from: a  reason: collision with root package name */
    public static final Map<a, org.apache.a.a.b> f2351a;
    private static final k b = new k("Location");
    private static final c c = new c("contry", (byte) 11, 1);
    private static final c d = new c("province", (byte) 11, 2);
    private static final c e = new c("city", (byte) 11, 3);
    private static final c f = new c("isp", (byte) 11, 4);
    private String g;
    private String h;
    private String i;
    private String j;

    public enum a {
        CONTRY(1, "contry"),
        PROVINCE(2, "province"),
        CITY(3, "city"),
        ISP(4, "isp");
        
        private static final Map<String, a> e = new HashMap();
        private final short f;
        private final String g;

        static {
            Iterator it = EnumSet.allOf(a.class).iterator();
            while (it.hasNext()) {
                a aVar = (a) it.next();
                e.put(aVar.a(), aVar);
            }
        }

        private a(short s, String str) {
            this.f = s;
            this.g = str;
        }

        public String a() {
            return this.g;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [com.xiaomi.b.a.a.a.e$a, org.apache.a.a.b]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        EnumMap enumMap = new EnumMap(a.class);
        enumMap.put((Object) a.CONTRY, (Object) new org.apache.a.a.b("contry", (byte) 2, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.PROVINCE, (Object) new org.apache.a.a.b("province", (byte) 2, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.CITY, (Object) new org.apache.a.a.b("city", (byte) 2, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.ISP, (Object) new org.apache.a.a.b("isp", (byte) 2, new org.apache.a.a.c((byte) 11)));
        f2351a = Collections.unmodifiableMap(enumMap);
        org.apache.a.a.b.a(e.class, f2351a);
    }

    public e a(String str) {
        this.g = str;
        return this;
    }

    public void a(f fVar) {
        fVar.g();
        while (true) {
            c i2 = fVar.i();
            if (i2.b == 0) {
                fVar.h();
                e();
                return;
            }
            switch (i2.c) {
                case 1:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.g = fVar.w();
                        break;
                    }
                case 2:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.h = fVar.w();
                        break;
                    }
                case 3:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.i = fVar.w();
                        break;
                    }
                case 4:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.j = fVar.w();
                        break;
                    }
                default:
                    i.a(fVar, i2.b);
                    break;
            }
            fVar.j();
        }
    }

    public boolean a() {
        return this.g != null;
    }

    public boolean a(e eVar) {
        if (eVar == null) {
            return false;
        }
        boolean a2 = a();
        boolean a3 = eVar.a();
        if ((a2 || a3) && (!a2 || !a3 || !this.g.equals(eVar.g))) {
            return false;
        }
        boolean b2 = b();
        boolean b3 = eVar.b();
        if ((b2 || b3) && (!b2 || !b3 || !this.h.equals(eVar.h))) {
            return false;
        }
        boolean c2 = c();
        boolean c3 = eVar.c();
        if ((c2 || c3) && (!c2 || !c3 || !this.i.equals(eVar.i))) {
            return false;
        }
        boolean d2 = d();
        boolean d3 = eVar.d();
        return (!d2 && !d3) || (d2 && d3 && this.j.equals(eVar.j));
    }

    /* renamed from: b */
    public int compareTo(e eVar) {
        int a2;
        int a3;
        int a4;
        int a5;
        if (!getClass().equals(eVar.getClass())) {
            return getClass().getName().compareTo(eVar.getClass().getName());
        }
        int compareTo = Boolean.valueOf(a()).compareTo(Boolean.valueOf(eVar.a()));
        if (compareTo != 0) {
            return compareTo;
        }
        if (a() && (a5 = org.apache.a.c.a(this.g, eVar.g)) != 0) {
            return a5;
        }
        int compareTo2 = Boolean.valueOf(b()).compareTo(Boolean.valueOf(eVar.b()));
        if (compareTo2 != 0) {
            return compareTo2;
        }
        if (b() && (a4 = org.apache.a.c.a(this.h, eVar.h)) != 0) {
            return a4;
        }
        int compareTo3 = Boolean.valueOf(c()).compareTo(Boolean.valueOf(eVar.c()));
        if (compareTo3 != 0) {
            return compareTo3;
        }
        if (c() && (a3 = org.apache.a.c.a(this.i, eVar.i)) != 0) {
            return a3;
        }
        int compareTo4 = Boolean.valueOf(d()).compareTo(Boolean.valueOf(eVar.d()));
        if (compareTo4 != 0) {
            return compareTo4;
        }
        if (!d() || (a2 = org.apache.a.c.a(this.j, eVar.j)) == 0) {
            return 0;
        }
        return a2;
    }

    public e b(String str) {
        this.h = str;
        return this;
    }

    public void b(f fVar) {
        e();
        fVar.a(b);
        if (this.g != null && a()) {
            fVar.a(c);
            fVar.a(this.g);
            fVar.b();
        }
        if (this.h != null && b()) {
            fVar.a(d);
            fVar.a(this.h);
            fVar.b();
        }
        if (this.i != null && c()) {
            fVar.a(e);
            fVar.a(this.i);
            fVar.b();
        }
        if (this.j != null && d()) {
            fVar.a(f);
            fVar.a(this.j);
            fVar.b();
        }
        fVar.c();
        fVar.a();
    }

    public boolean b() {
        return this.h != null;
    }

    public e c(String str) {
        this.i = str;
        return this;
    }

    public boolean c() {
        return this.i != null;
    }

    public e d(String str) {
        this.j = str;
        return this;
    }

    public boolean d() {
        return this.j != null;
    }

    public void e() {
    }

    public boolean equals(Object obj) {
        if (obj != null && (obj instanceof e)) {
            return a((e) obj);
        }
        return false;
    }

    public int hashCode() {
        return 0;
    }

    public String toString() {
        boolean z = false;
        StringBuilder sb = new StringBuilder("Location(");
        boolean z2 = true;
        if (a()) {
            sb.append("contry:");
            if (this.g == null) {
                sb.append("null");
            } else {
                sb.append(this.g);
            }
            z2 = false;
        }
        if (b()) {
            if (!z2) {
                sb.append(", ");
            }
            sb.append("province:");
            if (this.h == null) {
                sb.append("null");
            } else {
                sb.append(this.h);
            }
            z2 = false;
        }
        if (c()) {
            if (!z2) {
                sb.append(", ");
            }
            sb.append("city:");
            if (this.i == null) {
                sb.append("null");
            } else {
                sb.append(this.i);
            }
        } else {
            z = z2;
        }
        if (d()) {
            if (!z) {
                sb.append(", ");
            }
            sb.append("isp:");
            if (this.j == null) {
                sb.append("null");
            } else {
                sb.append(this.j);
            }
        }
        sb.append(")");
        return sb.toString();
    }
}
