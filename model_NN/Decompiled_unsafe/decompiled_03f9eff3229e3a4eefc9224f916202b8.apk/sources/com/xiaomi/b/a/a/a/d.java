package com.xiaomi.b.a.a.a;

import java.io.Serializable;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.apache.a.a.e;
import org.apache.a.b;
import org.apache.a.b.c;
import org.apache.a.b.f;
import org.apache.a.b.g;
import org.apache.a.b.i;
import org.apache.a.b.k;

public class d implements Serializable, Cloneable, b<d, a> {

    /* renamed from: a  reason: collision with root package name */
    public static final Map<a, org.apache.a.a.b> f2349a;
    private static final k b = new k("LandNodeInfo");
    private static final c c = new c("ip", (byte) 11, 1);
    private static final c d = new c("failed_count", (byte) 8, 2);
    private static final c e = new c("success_count", (byte) 8, 3);
    private static final c f = new c("duration", (byte) 10, 4);
    private static final c g = new c("size", (byte) 8, 5);
    private static final c h = new c("exp_info", (byte) 13, 6);
    private static final c i = new c("http_info", (byte) 13, 7);
    private String j;
    private int k;
    private int l;
    private long m;
    private int n;
    private Map<String, Integer> o;
    private Map<Integer, Integer> p;
    private BitSet q = new BitSet(4);

    public enum a {
        IP(1, "ip"),
        FAILED_COUNT(2, "failed_count"),
        SUCCESS_COUNT(3, "success_count"),
        DURATION(4, "duration"),
        SIZE(5, "size"),
        EXP_INFO(6, "exp_info"),
        HTTP_INFO(7, "http_info");
        
        private static final Map<String, a> h = new HashMap();
        private final short i;
        private final String j;

        static {
            Iterator it = EnumSet.allOf(a.class).iterator();
            while (it.hasNext()) {
                a aVar = (a) it.next();
                h.put(aVar.a(), aVar);
            }
        }

        private a(short s, String str) {
            this.i = s;
            this.j = str;
        }

        public String a() {
            return this.j;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [com.xiaomi.b.a.a.a.d$a, org.apache.a.a.b]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        EnumMap enumMap = new EnumMap(a.class);
        enumMap.put((Object) a.IP, (Object) new org.apache.a.a.b("ip", (byte) 1, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.FAILED_COUNT, (Object) new org.apache.a.a.b("failed_count", (byte) 1, new org.apache.a.a.c((byte) 8)));
        enumMap.put((Object) a.SUCCESS_COUNT, (Object) new org.apache.a.a.b("success_count", (byte) 1, new org.apache.a.a.c((byte) 8)));
        enumMap.put((Object) a.DURATION, (Object) new org.apache.a.a.b("duration", (byte) 1, new org.apache.a.a.c((byte) 10)));
        enumMap.put((Object) a.SIZE, (Object) new org.apache.a.a.b("size", (byte) 1, new org.apache.a.a.c((byte) 8)));
        enumMap.put((Object) a.EXP_INFO, (Object) new org.apache.a.a.b("exp_info", (byte) 2, new e((byte) 13, new org.apache.a.a.c((byte) 11), new org.apache.a.a.c((byte) 8))));
        enumMap.put((Object) a.HTTP_INFO, (Object) new org.apache.a.a.b("http_info", (byte) 2, new e((byte) 13, new org.apache.a.a.c((byte) 8), new org.apache.a.a.c((byte) 8))));
        f2349a = Collections.unmodifiableMap(enumMap);
        org.apache.a.a.b.a(d.class, f2349a);
    }

    public d a(int i2) {
        this.k = i2;
        a(true);
        return this;
    }

    public d a(long j2) {
        this.m = j2;
        c(true);
        return this;
    }

    public d a(String str) {
        this.j = str;
        return this;
    }

    public d a(Map<String, Integer> map) {
        this.o = map;
        return this;
    }

    public void a(f fVar) {
        fVar.g();
        while (true) {
            c i2 = fVar.i();
            if (i2.b == 0) {
                fVar.h();
                if (!b()) {
                    throw new g("Required field 'failed_count' was not found in serialized data! Struct: " + toString());
                } else if (!c()) {
                    throw new g("Required field 'success_count' was not found in serialized data! Struct: " + toString());
                } else if (!d()) {
                    throw new g("Required field 'duration' was not found in serialized data! Struct: " + toString());
                } else if (!e()) {
                    throw new g("Required field 'size' was not found in serialized data! Struct: " + toString());
                } else {
                    h();
                    return;
                }
            } else {
                switch (i2.c) {
                    case 1:
                        if (i2.b != 11) {
                            i.a(fVar, i2.b);
                            break;
                        } else {
                            this.j = fVar.w();
                            break;
                        }
                    case 2:
                        if (i2.b != 8) {
                            i.a(fVar, i2.b);
                            break;
                        } else {
                            this.k = fVar.t();
                            a(true);
                            break;
                        }
                    case 3:
                        if (i2.b != 8) {
                            i.a(fVar, i2.b);
                            break;
                        } else {
                            this.l = fVar.t();
                            b(true);
                            break;
                        }
                    case 4:
                        if (i2.b != 10) {
                            i.a(fVar, i2.b);
                            break;
                        } else {
                            this.m = fVar.u();
                            c(true);
                            break;
                        }
                    case 5:
                        if (i2.b != 8) {
                            i.a(fVar, i2.b);
                            break;
                        } else {
                            this.n = fVar.t();
                            d(true);
                            break;
                        }
                    case 6:
                        if (i2.b != 13) {
                            i.a(fVar, i2.b);
                            break;
                        } else {
                            org.apache.a.b.e k2 = fVar.k();
                            this.o = new HashMap(k2.c * 2);
                            for (int i3 = 0; i3 < k2.c; i3++) {
                                this.o.put(fVar.w(), Integer.valueOf(fVar.t()));
                            }
                            fVar.l();
                            break;
                        }
                    case 7:
                        if (i2.b != 13) {
                            i.a(fVar, i2.b);
                            break;
                        } else {
                            org.apache.a.b.e k3 = fVar.k();
                            this.p = new HashMap(k3.c * 2);
                            for (int i4 = 0; i4 < k3.c; i4++) {
                                this.p.put(Integer.valueOf(fVar.t()), Integer.valueOf(fVar.t()));
                            }
                            fVar.l();
                            break;
                        }
                    default:
                        i.a(fVar, i2.b);
                        break;
                }
                fVar.j();
            }
        }
    }

    public void a(boolean z) {
        this.q.set(0, z);
    }

    public boolean a() {
        return this.j != null;
    }

    public boolean a(d dVar) {
        if (dVar == null) {
            return false;
        }
        boolean a2 = a();
        boolean a3 = dVar.a();
        if (((a2 || a3) && (!a2 || !a3 || !this.j.equals(dVar.j))) || this.k != dVar.k || this.l != dVar.l || this.m != dVar.m || this.n != dVar.n) {
            return false;
        }
        boolean f2 = f();
        boolean f3 = dVar.f();
        if ((f2 || f3) && (!f2 || !f3 || !this.o.equals(dVar.o))) {
            return false;
        }
        boolean g2 = g();
        boolean g3 = dVar.g();
        return (!g2 && !g3) || (g2 && g3 && this.p.equals(dVar.p));
    }

    /* renamed from: b */
    public int compareTo(d dVar) {
        int a2;
        int a3;
        int a4;
        int a5;
        int a6;
        int a7;
        int a8;
        if (!getClass().equals(dVar.getClass())) {
            return getClass().getName().compareTo(dVar.getClass().getName());
        }
        int compareTo = Boolean.valueOf(a()).compareTo(Boolean.valueOf(dVar.a()));
        if (compareTo != 0) {
            return compareTo;
        }
        if (a() && (a8 = org.apache.a.c.a(this.j, dVar.j)) != 0) {
            return a8;
        }
        int compareTo2 = Boolean.valueOf(b()).compareTo(Boolean.valueOf(dVar.b()));
        if (compareTo2 != 0) {
            return compareTo2;
        }
        if (b() && (a7 = org.apache.a.c.a(this.k, dVar.k)) != 0) {
            return a7;
        }
        int compareTo3 = Boolean.valueOf(c()).compareTo(Boolean.valueOf(dVar.c()));
        if (compareTo3 != 0) {
            return compareTo3;
        }
        if (c() && (a6 = org.apache.a.c.a(this.l, dVar.l)) != 0) {
            return a6;
        }
        int compareTo4 = Boolean.valueOf(d()).compareTo(Boolean.valueOf(dVar.d()));
        if (compareTo4 != 0) {
            return compareTo4;
        }
        if (d() && (a5 = org.apache.a.c.a(this.m, dVar.m)) != 0) {
            return a5;
        }
        int compareTo5 = Boolean.valueOf(e()).compareTo(Boolean.valueOf(dVar.e()));
        if (compareTo5 != 0) {
            return compareTo5;
        }
        if (e() && (a4 = org.apache.a.c.a(this.n, dVar.n)) != 0) {
            return a4;
        }
        int compareTo6 = Boolean.valueOf(f()).compareTo(Boolean.valueOf(dVar.f()));
        if (compareTo6 != 0) {
            return compareTo6;
        }
        if (f() && (a3 = org.apache.a.c.a(this.o, dVar.o)) != 0) {
            return a3;
        }
        int compareTo7 = Boolean.valueOf(g()).compareTo(Boolean.valueOf(dVar.g()));
        if (compareTo7 != 0) {
            return compareTo7;
        }
        if (!g() || (a2 = org.apache.a.c.a(this.p, dVar.p)) == 0) {
            return 0;
        }
        return a2;
    }

    public d b(int i2) {
        this.l = i2;
        b(true);
        return this;
    }

    public void b(f fVar) {
        h();
        fVar.a(b);
        if (this.j != null) {
            fVar.a(c);
            fVar.a(this.j);
            fVar.b();
        }
        fVar.a(d);
        fVar.a(this.k);
        fVar.b();
        fVar.a(e);
        fVar.a(this.l);
        fVar.b();
        fVar.a(f);
        fVar.a(this.m);
        fVar.b();
        fVar.a(g);
        fVar.a(this.n);
        fVar.b();
        if (this.o != null && f()) {
            fVar.a(h);
            fVar.a(new org.apache.a.b.e((byte) 11, (byte) 8, this.o.size()));
            for (Map.Entry next : this.o.entrySet()) {
                fVar.a((String) next.getKey());
                fVar.a(((Integer) next.getValue()).intValue());
            }
            fVar.d();
            fVar.b();
        }
        if (this.p != null && g()) {
            fVar.a(i);
            fVar.a(new org.apache.a.b.e((byte) 8, (byte) 8, this.p.size()));
            for (Map.Entry next2 : this.p.entrySet()) {
                fVar.a(((Integer) next2.getKey()).intValue());
                fVar.a(((Integer) next2.getValue()).intValue());
            }
            fVar.d();
            fVar.b();
        }
        fVar.c();
        fVar.a();
    }

    public void b(boolean z) {
        this.q.set(1, z);
    }

    public boolean b() {
        return this.q.get(0);
    }

    public d c(int i2) {
        this.n = i2;
        d(true);
        return this;
    }

    public void c(boolean z) {
        this.q.set(2, z);
    }

    public boolean c() {
        return this.q.get(1);
    }

    public void d(boolean z) {
        this.q.set(3, z);
    }

    public boolean d() {
        return this.q.get(2);
    }

    public boolean e() {
        return this.q.get(3);
    }

    public boolean equals(Object obj) {
        if (obj != null && (obj instanceof d)) {
            return a((d) obj);
        }
        return false;
    }

    public boolean f() {
        return this.o != null;
    }

    public boolean g() {
        return this.p != null;
    }

    public void h() {
        if (this.j == null) {
            throw new g("Required field 'ip' was not present! Struct: " + toString());
        }
    }

    public int hashCode() {
        return 0;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("LandNodeInfo(");
        sb.append("ip:");
        if (this.j == null) {
            sb.append("null");
        } else {
            sb.append(this.j);
        }
        sb.append(", ");
        sb.append("failed_count:");
        sb.append(this.k);
        sb.append(", ");
        sb.append("success_count:");
        sb.append(this.l);
        sb.append(", ");
        sb.append("duration:");
        sb.append(this.m);
        sb.append(", ");
        sb.append("size:");
        sb.append(this.n);
        if (f()) {
            sb.append(", ");
            sb.append("exp_info:");
            if (this.o == null) {
                sb.append("null");
            } else {
                sb.append(this.o);
            }
        }
        if (g()) {
            sb.append(", ");
            sb.append("http_info:");
            if (this.p == null) {
                sb.append("null");
            } else {
                sb.append(this.p);
            }
        }
        sb.append(")");
        return sb.toString();
    }
}
