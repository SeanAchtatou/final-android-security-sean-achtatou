package com.xiaomi.network;

import com.xiaomi.b.a.a.a.b;
import com.xiaomi.network.j;
import java.util.List;
import org.json.JSONException;

class k implements j.a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ f f2476a;

    k(f fVar) {
        this.f2476a = fVar;
    }

    public List<b> a() {
        try {
            return this.f2476a.j();
        } catch (JSONException e) {
            return null;
        }
    }

    public double b() {
        b b = this.f2476a.b("f3.mi-stat.gslb.mi-idc.com");
        if (b != null) {
            return b.g();
        }
        return 0.1d;
    }
}
