package com.xiaomi.network;

import java.net.InetSocketAddress;

public final class d {

    /* renamed from: a  reason: collision with root package name */
    private String f2472a;
    private int b;

    public d(String str, int i) {
        this.f2472a = str;
        this.b = i;
    }

    public static d a(String str, int i) {
        int lastIndexOf = str.lastIndexOf(":");
        if (lastIndexOf != -1) {
            String substring = str.substring(0, lastIndexOf);
            try {
                int parseInt = Integer.parseInt(str.substring(lastIndexOf + 1));
                if (parseInt > 0) {
                    i = parseInt;
                }
                str = substring;
            } catch (NumberFormatException e) {
                str = substring;
            }
        }
        return new d(str, i);
    }

    public static InetSocketAddress b(String str, int i) {
        d a2 = a(str, i);
        return new InetSocketAddress(a2.b(), a2.a());
    }

    public int a() {
        return this.b;
    }

    public String b() {
        return this.f2472a;
    }

    public String toString() {
        return this.b > 0 ? this.f2472a + ":" + this.b : this.f2472a;
    }
}
