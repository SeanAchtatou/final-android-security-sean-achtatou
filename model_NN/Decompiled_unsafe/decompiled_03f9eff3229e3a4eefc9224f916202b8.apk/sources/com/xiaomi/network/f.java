package com.xiaomi.network;

import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Process;
import android.text.TextUtils;
import com.baidu.mobads.interfaces.utils.IXAdSystemUtils;
import com.xiaomi.a.a.c.c;
import com.xiaomi.a.a.e.d;
import com.xiaomi.b.a.a.a.e;
import com.xiaomi.network.j;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class f {
    protected static Map<String, ArrayList<String>> b = new HashMap();
    protected static boolean e = false;
    private static f m;
    private static a n;
    private static String o;
    private static String p;

    /* renamed from: a  reason: collision with root package name */
    protected Map<String, c> f2473a = new HashMap();
    protected Context c;
    protected b d;
    private e f;
    private String g = "0";
    private long h = 0;
    private final long i = 15;
    private long j = 0;
    private String k = "isp_prov_city_country_ip";
    private j.a l = new k(this);

    public interface a {
        f a(Context context, e eVar, b bVar, String str);
    }

    public interface b {
        String a(String str);
    }

    protected f(Context context, e eVar, b bVar, String str, String str2, String str3) {
        this.c = context.getApplicationContext();
        if (this.c == null) {
            this.c = context;
        }
        this.d = bVar;
        if (eVar == null) {
            this.f = new l(this);
        } else {
            this.f = eVar;
        }
        this.g = str;
        o = str2 == null ? context.getPackageName() : str2;
        p = str3 == null ? m() : str3;
    }

    public static synchronized f a() {
        f fVar;
        synchronized (f.class) {
            if (m == null) {
                throw new IllegalStateException("the host manager is not initialized yet.");
            }
            fVar = m;
        }
        return fVar;
    }

    public static <T> String a(Collection collection, String str) {
        if (collection == null || collection.isEmpty()) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            sb.append(it.next());
            if (it.hasNext()) {
                sb.append(str);
            }
        }
        return sb.toString();
    }

    public static String a(String[] strArr, String str) {
        if (strArr == null || strArr.length == 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        sb.append(strArr[0]);
        for (int i2 = 1; i2 < strArr.length; i2++) {
            sb.append(str);
            sb.append(strArr[i2]);
        }
        return sb.toString();
    }

    private ArrayList<b> a(ArrayList<String> arrayList) {
        k();
        synchronized (this.f2473a) {
            g();
            for (String next : this.f2473a.keySet()) {
                if (!arrayList.contains(next)) {
                    arrayList.add(next);
                }
            }
        }
        synchronized (b) {
            for (String next2 : b.keySet()) {
                if (!arrayList.contains(next2)) {
                    arrayList.add(next2);
                }
            }
        }
        if (!arrayList.contains(c())) {
            arrayList.add(c());
        }
        ArrayList<b> arrayList2 = new ArrayList<>(arrayList.size());
        for (int i2 = 0; i2 < arrayList.size(); i2++) {
            arrayList2.add(null);
        }
        try {
            String str = d.e(this.c) ? IXAdSystemUtils.NT_WIFI : "wap";
            String a2 = a(arrayList, str, this.g);
            if (!TextUtils.isEmpty(a2)) {
                JSONObject jSONObject = new JSONObject(a2);
                if ("OK".equalsIgnoreCase(jSONObject.getString("S"))) {
                    JSONObject jSONObject2 = jSONObject.getJSONObject("R");
                    String string = jSONObject2.getString("province");
                    String string2 = jSONObject2.getString("city");
                    String string3 = jSONObject2.getString("isp");
                    String string4 = jSONObject2.getString("ip");
                    String string5 = jSONObject2.getString("country");
                    JSONObject jSONObject3 = jSONObject2.getJSONObject(str);
                    if (str.equals("wap")) {
                        str = b();
                    }
                    c.a("get bucket: " + string5 + " " + string + " " + " isp:" + string3 + " " + str + " hosts:" + jSONObject3.toString());
                    for (int i3 = 0; i3 < arrayList.size(); i3++) {
                        String str2 = arrayList.get(i3);
                        JSONArray optJSONArray = jSONObject3.optJSONArray(str2);
                        if (optJSONArray == null) {
                            c.a("no bucket found for " + str2);
                        } else {
                            b bVar = new b(str2);
                            for (int i4 = 0; i4 < optJSONArray.length(); i4++) {
                                String string6 = optJSONArray.getString(i4);
                                if (!TextUtils.isEmpty(string6)) {
                                    bVar.a(new o(string6, optJSONArray.length() - i4));
                                }
                            }
                            arrayList2.set(i3, bVar);
                            bVar.g = string5;
                            bVar.c = string;
                            bVar.e = string3;
                            bVar.f = string4;
                            bVar.d = string2;
                            if (jSONObject2.has("stat-percent")) {
                                bVar.a(jSONObject2.getDouble("stat-percent"));
                            }
                            if (jSONObject2.has("stat-domain")) {
                                bVar.b(jSONObject2.getString("stat-domain"));
                            }
                            if (jSONObject2.has("ttl")) {
                                bVar.a(((long) jSONObject2.getInt("ttl")) * 1000);
                            }
                            e(bVar.e());
                        }
                    }
                }
            }
        } catch (JSONException e2) {
            c.a("failed to get bucket" + e2.getMessage());
        } catch (IOException e3) {
            c.a("failed to get bucket" + e3.getMessage());
        } catch (Exception e4) {
            c.a("failed to get bucket" + e4.getMessage());
        }
        int i5 = 0;
        while (true) {
            int i6 = i5;
            if (i6 < arrayList.size()) {
                b bVar2 = arrayList2.get(i6);
                if (bVar2 != null) {
                    a(arrayList.get(i6), bVar2);
                }
                i5 = i6 + 1;
            } else {
                h();
                return arrayList2;
            }
        }
    }

    public static synchronized void a(Context context, e eVar, b bVar, String str, String str2, String str3) {
        synchronized (f.class) {
            if (m == null) {
                if (n == null) {
                    m = new f(context, eVar, bVar, str, str2, str3);
                } else {
                    m = n.a(context, eVar, bVar, str);
                }
                if (m != null) {
                    if (j.a() == null) {
                        j.a(context);
                    }
                    j.a().a(m.l);
                }
            }
        }
    }

    public static synchronized void a(a aVar) {
        synchronized (f.class) {
            n = aVar;
            if (!(j.a() == null || m == null)) {
                j.a().b(m.l);
            }
            m = null;
        }
    }

    public static void a(String str, String str2) {
        ArrayList arrayList = b.get(str);
        synchronized (b) {
            if (arrayList == null) {
                ArrayList arrayList2 = new ArrayList();
                arrayList2.add(str2);
                b.put(str, arrayList2);
            } else if (!arrayList.contains(str2)) {
                arrayList.add(str2);
            }
        }
    }

    private String g(String str) {
        return TextUtils.isEmpty(str) ? "unknown" : str.startsWith("WIFI") ? "WIFI" : str;
    }

    private String m() {
        try {
            PackageInfo packageInfo = this.c.getPackageManager().getPackageInfo(this.c.getPackageName(), 16384);
            if (packageInfo != null) {
                return packageInfo.versionName;
            }
        } catch (Exception e2) {
        }
        return "0";
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.xiaomi.network.f.a(java.lang.String, boolean):com.xiaomi.network.b
     arg types: [java.lang.String, int]
     candidates:
      com.xiaomi.network.f.a(java.util.Collection, java.lang.String):java.lang.String
      com.xiaomi.network.f.a(java.lang.String[], java.lang.String):java.lang.String
      com.xiaomi.network.f.a(java.lang.String, java.lang.String):void
      com.xiaomi.network.f.a(java.lang.String, com.xiaomi.network.b):void
      com.xiaomi.network.f.a(java.lang.String, boolean):com.xiaomi.network.b */
    public b a(String str) {
        if (!TextUtils.isEmpty(str)) {
            return a(new URL(str).getHost(), true);
        }
        throw new IllegalArgumentException("the url is empty");
    }

    public b a(String str, boolean z) {
        b d2;
        if (TextUtils.isEmpty(str)) {
            throw new IllegalArgumentException("the host is empty");
        } else if (!this.f.a(str)) {
            return null;
        } else {
            b c2 = c(str);
            return (c2 == null || !c2.b()) ? (!z || !d.d(this.c) || (d2 = d(str)) == null) ? new m(this, str, c2) : d2 : c2;
        }
    }

    /* access modifiers changed from: protected */
    public String a(ArrayList<String> arrayList, String str, String str2) {
        ArrayList<String> arrayList2 = new ArrayList<>();
        ArrayList<com.xiaomi.a.a.e.c> arrayList3 = new ArrayList<>();
        arrayList3.add(new com.xiaomi.a.a.e.a("type", str));
        arrayList3.add(new com.xiaomi.a.a.e.a("uuid", str2));
        arrayList3.add(new com.xiaomi.a.a.e.a("list", a(arrayList, ",")));
        b c2 = c("resolver.gslb.mi-idc.com");
        String format = String.format("http://%1$s/gslb/gslb/getbucket.asp?ver=3.0", "resolver.gslb.mi-idc.com");
        if (c2 == null) {
            arrayList2.add(format);
        } else {
            arrayList2 = c2.a(format);
        }
        Iterator<String> it = arrayList2.iterator();
        IOException e2 = null;
        while (it.hasNext()) {
            Uri.Builder buildUpon = Uri.parse(it.next()).buildUpon();
            for (com.xiaomi.a.a.e.c cVar : arrayList3) {
                buildUpon.appendQueryParameter(cVar.a(), cVar.b());
            }
            try {
                return this.d == null ? d.a(this.c, new URL(buildUpon.toString())) : this.d.a(buildUpon.toString());
            } catch (IOException e3) {
                e2 = e3;
            }
        }
        if (e2 == null) {
            return null;
        }
        throw e2;
    }

    public void a(String str, b bVar) {
        if (TextUtils.isEmpty(str) || bVar == null) {
            throw new IllegalArgumentException("the argument is invalid " + str + ", " + bVar);
        } else if (this.f.a(str)) {
            synchronized (this.f2473a) {
                g();
                if (this.f2473a.containsKey(str)) {
                    this.f2473a.get(str).a(bVar);
                } else {
                    c cVar = new c(str);
                    cVar.a(bVar);
                    this.f2473a.put(str, cVar);
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.xiaomi.network.f.a(java.lang.String, boolean):com.xiaomi.network.b
     arg types: [java.lang.String, int]
     candidates:
      com.xiaomi.network.f.a(java.util.Collection, java.lang.String):java.lang.String
      com.xiaomi.network.f.a(java.lang.String[], java.lang.String):java.lang.String
      com.xiaomi.network.f.a(java.lang.String, java.lang.String):void
      com.xiaomi.network.f.a(java.lang.String, com.xiaomi.network.b):void
      com.xiaomi.network.f.a(java.lang.String, boolean):com.xiaomi.network.b */
    public b b(String str) {
        return a(str, true);
    }

    public String b() {
        if (this.c == null) {
            return "unknown";
        }
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) this.c.getSystemService("connectivity");
            if (connectivityManager == null) {
                return "unknown";
            }
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            if (activeNetworkInfo == null) {
                return "unknown";
            }
            if (activeNetworkInfo.getType() != 1) {
                return activeNetworkInfo.getTypeName() + "-" + activeNetworkInfo.getSubtypeName();
            }
            WifiManager wifiManager = (WifiManager) this.c.getSystemService(IXAdSystemUtils.NT_WIFI);
            if (!(wifiManager == null || wifiManager.getConnectionInfo() == null)) {
                return "WIFI-" + wifiManager.getConnectionInfo().getSSID();
            }
            return "unknown";
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: protected */
    public b c(String str) {
        c cVar;
        b a2;
        synchronized (this.f2473a) {
            g();
            cVar = this.f2473a.get(str);
        }
        if (cVar == null || (a2 = cVar.a()) == null) {
            return null;
        }
        return a2;
    }

    /* access modifiers changed from: protected */
    public String c() {
        return "resolver.gslb.mi-idc.com";
    }

    /* access modifiers changed from: protected */
    public b d(String str) {
        if (System.currentTimeMillis() - this.j > this.h * 60 * 1000) {
            this.j = System.currentTimeMillis();
            ArrayList arrayList = new ArrayList();
            arrayList.add(str);
            b bVar = a(arrayList).get(0);
            if (bVar != null) {
                this.h = 0;
                return bVar;
            } else if (this.h < 15) {
                this.h++;
            }
        }
        return null;
    }

    public void d() {
        synchronized (this.f2473a) {
            this.f2473a.clear();
        }
    }

    public void e() {
        ArrayList arrayList;
        synchronized (this.f2473a) {
            g();
            arrayList = new ArrayList(this.f2473a.keySet());
            for (int size = arrayList.size() - 1; size >= 0; size--) {
                c cVar = this.f2473a.get(arrayList.get(size));
                if (!(cVar == null || cVar.a() == null)) {
                    arrayList.remove(size);
                }
            }
        }
        ArrayList<b> a2 = a(arrayList);
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < arrayList.size()) {
                if (a2.get(i3) != null) {
                    a((String) arrayList.get(i3), a2.get(i3));
                }
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }

    public void e(String str) {
        this.k = str;
    }

    /* access modifiers changed from: protected */
    public String f() {
        BufferedReader bufferedReader;
        Throwable th;
        String str = null;
        try {
            File file = new File(this.c.getFilesDir(), i());
            if (file.isFile()) {
                bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
                try {
                    StringBuilder sb = new StringBuilder();
                    while (true) {
                        String readLine = bufferedReader.readLine();
                        if (readLine == null) {
                            break;
                        }
                        sb.append(readLine);
                    }
                    str = sb.toString();
                    com.xiaomi.a.a.b.a.a(bufferedReader);
                } catch (Throwable th2) {
                    th = th2;
                    try {
                        c.a("load host exception " + th.getMessage());
                        com.xiaomi.a.a.b.a.a(bufferedReader);
                        return str;
                    } catch (Throwable th3) {
                        th = th3;
                        com.xiaomi.a.a.b.a.a(bufferedReader);
                        throw th;
                    }
                }
            } else {
                com.xiaomi.a.a.b.a.a((Reader) null);
            }
        } catch (Throwable th4) {
            bufferedReader = null;
            th = th4;
            com.xiaomi.a.a.b.a.a(bufferedReader);
            throw th;
        }
        return str;
    }

    /* access modifiers changed from: protected */
    public void f(String str) {
        synchronized (this.f2473a) {
            this.f2473a.clear();
            JSONArray jSONArray = new JSONArray(str);
            for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                c a2 = new c().a(jSONArray.getJSONObject(i2));
                this.f2473a.put(a2.c(), a2);
            }
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean g() {
        /*
            r4 = this;
            r0 = 1
            java.util.Map<java.lang.String, com.xiaomi.network.c> r1 = r4.f2473a
            monitor-enter(r1)
            boolean r2 = com.xiaomi.network.f.e     // Catch:{ all -> 0x0044 }
            if (r2 != 0) goto L_0x0042
            r2 = 1
            com.xiaomi.network.f.e = r2     // Catch:{ all -> 0x0044 }
            java.util.Map<java.lang.String, com.xiaomi.network.c> r2 = r4.f2473a     // Catch:{ all -> 0x0044 }
            r2.clear()     // Catch:{ all -> 0x0044 }
            java.lang.String r2 = r4.f()     // Catch:{ Throwable -> 0x0024 }
            boolean r3 = android.text.TextUtils.isEmpty(r2)     // Catch:{ Throwable -> 0x0024 }
            if (r3 != 0) goto L_0x003f
            r4.f(r2)     // Catch:{ Throwable -> 0x0024 }
            java.lang.String r2 = "loading the new hosts succeed"
            com.xiaomi.a.a.c.c.a(r2)     // Catch:{ Throwable -> 0x0024 }
            monitor-exit(r1)     // Catch:{ all -> 0x0044 }
        L_0x0023:
            return r0
        L_0x0024:
            r0 = move-exception
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0044 }
            r2.<init>()     // Catch:{ all -> 0x0044 }
            java.lang.String r3 = "load host exception "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0044 }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x0044 }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ all -> 0x0044 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0044 }
            com.xiaomi.a.a.c.c.a(r0)     // Catch:{ all -> 0x0044 }
        L_0x003f:
            monitor-exit(r1)     // Catch:{ all -> 0x0044 }
            r0 = 0
            goto L_0x0023
        L_0x0042:
            monitor-exit(r1)     // Catch:{ all -> 0x0044 }
            goto L_0x0023
        L_0x0044:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0044 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.xiaomi.network.f.g():boolean");
    }

    public void h() {
        k();
        synchronized (this.f2473a) {
            try {
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(this.c.openFileOutput(i(), 0)));
                String jSONArray = l().toString();
                if (!TextUtils.isEmpty(jSONArray)) {
                    bufferedWriter.write(jSONArray);
                }
                bufferedWriter.close();
            } catch (IOException e2) {
                e2.printStackTrace();
            } catch (JSONException e3) {
                e3.printStackTrace();
            } catch (Exception e4) {
                e4.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: protected */
    public String i() {
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = ((ActivityManager) this.c.getSystemService("activity")).getRunningAppProcesses();
        if (runningAppProcesses != null) {
            for (ActivityManager.RunningAppProcessInfo next : runningAppProcesses) {
                if (next.pid == Process.myPid()) {
                    return next.processName;
                }
            }
        }
        return "com.xiaomi";
    }

    public ArrayList<com.xiaomi.b.a.a.a.b> j() {
        ArrayList<com.xiaomi.b.a.a.a.b> arrayList;
        com.xiaomi.b.a.a.a.b bVar;
        int i2;
        int i3;
        int i4;
        synchronized (this.f2473a) {
            HashMap hashMap = new HashMap();
            for (String str : this.f2473a.keySet()) {
                c cVar = this.f2473a.get(str);
                if (cVar != null) {
                    Iterator<b> it = cVar.b().iterator();
                    while (it.hasNext()) {
                        b next = it.next();
                        com.xiaomi.b.a.a.a.b bVar2 = (com.xiaomi.b.a.a.a.b) hashMap.get(next.e());
                        if (bVar2 == null) {
                            com.xiaomi.b.a.a.a.b bVar3 = new com.xiaomi.b.a.a.a.b();
                            bVar3.a("httpapi");
                            bVar3.e(next.f);
                            bVar3.d(g(next.f2470a));
                            bVar3.b(this.g);
                            bVar3.c(p);
                            bVar3.f(o);
                            bVar3.g(this.c.getPackageName());
                            bVar3.h(m());
                            e eVar = new e();
                            eVar.c(next.d);
                            eVar.a(next.g);
                            eVar.b(next.c);
                            eVar.d(next.e);
                            bVar3.a(eVar);
                            hashMap.put(next.e(), bVar3);
                            bVar = bVar3;
                        } else {
                            bVar = bVar2;
                        }
                        com.xiaomi.b.a.a.a.a aVar = new com.xiaomi.b.a.a.a.a();
                        aVar.a(next.b);
                        ArrayList arrayList2 = new ArrayList();
                        Iterator<o> it2 = next.f().iterator();
                        while (it2.hasNext()) {
                            o next2 = it2.next();
                            ArrayList<a> a2 = next2.a();
                            if (!a2.isEmpty()) {
                                com.xiaomi.b.a.a.a.d dVar = new com.xiaomi.b.a.a.a.d();
                                dVar.a(next2.f2479a);
                                int i5 = 0;
                                int i6 = 0;
                                long j2 = 0;
                                int i7 = 0;
                                HashMap hashMap2 = new HashMap();
                                Iterator<a> it3 = a2.iterator();
                                while (it3.hasNext()) {
                                    a next3 = it3.next();
                                    if (next3.a() >= 0) {
                                        j2 += next3.b();
                                        i3 = (int) (next3.d() + ((long) i7));
                                        i4 = i6;
                                        i2 = i5 + 1;
                                    } else {
                                        String e2 = next3.e();
                                        if (!TextUtils.isEmpty(e2)) {
                                            hashMap2.put(e2, Integer.valueOf(hashMap2.containsKey(e2) ? ((Integer) hashMap2.get(e2)).intValue() + 1 : 1));
                                        }
                                        int i8 = i6 + 1;
                                        i2 = i5;
                                        int i9 = i8;
                                        i3 = i7;
                                        i4 = i9;
                                    }
                                    i5 = i2;
                                    i6 = i4;
                                    i7 = i3;
                                }
                                dVar.a(hashMap2);
                                dVar.b(i5);
                                dVar.a(i6);
                                dVar.a(j2);
                                dVar.c(i7);
                                arrayList2.add(dVar);
                            }
                        }
                        if (!arrayList2.isEmpty()) {
                            aVar.a(arrayList2);
                            bVar.a(aVar);
                        }
                    }
                    continue;
                }
            }
            arrayList = new ArrayList<>();
            for (com.xiaomi.b.a.a.a.b bVar4 : hashMap.values()) {
                if (bVar4.g() > 0) {
                    arrayList.add(bVar4);
                }
            }
        }
        return arrayList;
    }

    public void k() {
        synchronized (this.f2473a) {
            for (c a2 : this.f2473a.values()) {
                a2.a(false);
            }
            boolean z = false;
            while (!z) {
                Iterator<String> it = this.f2473a.keySet().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        z = true;
                        break;
                    }
                    String next = it.next();
                    if (this.f2473a.get(next).b().isEmpty()) {
                        this.f2473a.remove(next);
                        z = false;
                        break;
                    }
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public JSONArray l() {
        JSONArray jSONArray;
        synchronized (this.f2473a) {
            jSONArray = new JSONArray();
            for (c d2 : this.f2473a.values()) {
                jSONArray.put(d2.d());
            }
        }
        return jSONArray;
    }
}
