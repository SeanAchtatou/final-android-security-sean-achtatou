package com.xiaomi.network;

import com.tencent.stat.DeviceInfo;
import org.json.JSONObject;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private int f2469a;
    private long b;
    private long c;
    private String d;
    private long e;

    public a() {
        this(0, 0, 0, null);
    }

    public a(int i, long j, long j2, Exception exc) {
        this.f2469a = i;
        this.b = j;
        this.e = j2;
        this.c = System.currentTimeMillis();
        if (exc != null) {
            this.d = exc.getClass().getSimpleName();
        }
    }

    public int a() {
        return this.f2469a;
    }

    public a a(JSONObject jSONObject) {
        this.b = jSONObject.getLong("cost");
        this.e = jSONObject.getLong("size");
        this.c = jSONObject.getLong(DeviceInfo.TAG_TIMESTAMPS);
        this.f2469a = jSONObject.getInt("wt");
        this.d = jSONObject.optString("expt");
        return this;
    }

    public long b() {
        return this.b;
    }

    public long c() {
        return this.c;
    }

    public long d() {
        return this.e;
    }

    public String e() {
        return this.d;
    }

    public JSONObject f() {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("cost", this.b);
        jSONObject.put("size", this.e);
        jSONObject.put(DeviceInfo.TAG_TIMESTAMPS, this.c);
        jSONObject.put("wt", this.f2469a);
        jSONObject.put("expt", this.d);
        return jSONObject;
    }
}
