package com.xiaomi.network;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import com.xiaomi.a.a.e.c;
import com.xiaomi.a.a.e.d;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public abstract class i {

    public static class a extends h {
        public a() {
            super(1);
        }

        public String b(Context context, String str, List<c> list) {
            if (list == null) {
                return d.a(context, new URL(str));
            }
            Uri.Builder buildUpon = Uri.parse(str).buildUpon();
            for (c next : list) {
                buildUpon.appendQueryParameter(next.a(), next.b());
            }
            return d.a(context, new URL(buildUpon.toString()));
        }
    }

    static int a(int i, int i2) {
        return (((i2 + 243) / 1448) * 132) + 1080 + i + i2;
    }

    static int a(int i, int i2, int i3) {
        return (((i2 + 200) / 1448) * 132) + 1011 + i2 + i + i3;
    }

    private static int a(h hVar, String str, List<c> list, String str2) {
        if (hVar.a() == 1) {
            return a(str.length(), a(str2));
        }
        if (hVar.a() != 2) {
            return -1;
        }
        return a(str.length(), a(list), a(str2));
    }

    static int a(String str) {
        if (TextUtils.isEmpty(str)) {
            return 0;
        }
        try {
            return str.getBytes("UTF-8").length;
        } catch (UnsupportedEncodingException e) {
            return 0;
        }
    }

    static int a(List<c> list) {
        int i = 0;
        Iterator<c> it = list.iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                return i2 * 2;
            }
            c next = it.next();
            if (!TextUtils.isEmpty(next.a())) {
                i2 += next.a().length();
            }
            i = !TextUtils.isEmpty(next.b()) ? next.b().length() + i2 : i2;
        }
    }

    public static String a(Context context, String str, List<c> list) {
        return a(context, str, list, new a(), true);
    }

    public static String a(Context context, String str, List<c> list, h hVar, boolean z) {
        if (d.d(context)) {
            try {
                ArrayList<String> arrayList = new ArrayList<>();
                b bVar = null;
                if (z && (bVar = f.a().a(str)) != null) {
                    arrayList = bVar.a(str);
                }
                if (!arrayList.contains(str)) {
                    arrayList.add(str);
                }
                String str2 = null;
                Iterator<String> it = arrayList.iterator();
                while (it.hasNext()) {
                    String next = it.next();
                    ArrayList arrayList2 = list != null ? new ArrayList(list) : null;
                    long currentTimeMillis = System.currentTimeMillis();
                    try {
                        if (!hVar.a(context, next, arrayList2)) {
                            return str2;
                        }
                        str2 = hVar.b(context, next, arrayList2);
                        if (TextUtils.isEmpty(str2)) {
                            if (bVar != null) {
                                bVar.a(next, System.currentTimeMillis() - currentTimeMillis, (long) a(hVar, next, arrayList2, str2), null);
                            }
                            str2 = str2;
                        } else if (bVar == null) {
                            return str2;
                        } else {
                            bVar.a(next, System.currentTimeMillis() - currentTimeMillis, (long) a(hVar, next, arrayList2, str2));
                            return str2;
                        }
                    } catch (IOException e) {
                        if (bVar != null) {
                            bVar.a(next, System.currentTimeMillis() - currentTimeMillis, (long) a(hVar, next, arrayList2, str2), e);
                        }
                        e.printStackTrace();
                    }
                }
                return str2;
            } catch (MalformedURLException e2) {
                e2.printStackTrace();
            }
        }
        return null;
    }
}
