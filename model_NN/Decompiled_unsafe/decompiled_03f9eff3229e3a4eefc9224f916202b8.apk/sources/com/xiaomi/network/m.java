package com.xiaomi.network;

import java.util.ArrayList;
import java.util.Iterator;

class m extends b {
    b i = this.j;
    final /* synthetic */ b j;
    final /* synthetic */ f k;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    m(f fVar, String str, b bVar) {
        super(str);
        this.k = fVar;
        this.j = bVar;
        this.b = this.b;
        if (this.j != null) {
            this.f = this.j.f;
        }
    }

    public synchronized ArrayList<String> a(boolean z) {
        ArrayList<String> arrayList;
        arrayList = new ArrayList<>();
        if (this.i != null) {
            arrayList.addAll(this.i.a(true));
        }
        synchronized (f.b) {
            ArrayList arrayList2 = f.b.get(this.b);
            if (arrayList2 != null) {
                ArrayList<String> a2 = this.i != null ? this.i.a(false) : arrayList;
                Iterator it = arrayList2.iterator();
                while (it.hasNext()) {
                    String str = (String) it.next();
                    if (a2.indexOf(str) == -1) {
                        arrayList.add(str);
                    }
                }
                arrayList.remove(this.b);
                arrayList.add(this.b);
            }
        }
        return arrayList;
    }

    public synchronized void a(String str, a aVar) {
        if (this.i != null) {
            this.i.a(str, aVar);
        }
    }

    public boolean b() {
        return false;
    }
}
