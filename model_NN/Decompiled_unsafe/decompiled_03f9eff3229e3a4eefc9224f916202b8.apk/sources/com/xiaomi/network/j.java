package com.xiaomi.network;

import android.content.Context;
import com.baidu.mobads.interfaces.IXAdRequestInfo;
import com.xiaomi.a.a.e.d;
import com.xiaomi.b.a.a.a.b;
import com.xiaomi.b.a.a.a.c;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TreeMap;
import org.apache.a.b.b;
import org.apache.a.g;

public class j {
    private static j e;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public List<a> f2475a = new ArrayList();
    private final Random b = new Random();
    private Timer c = new Timer("Upload Http Record Timer");
    /* access modifiers changed from: private */
    public boolean d = false;
    private Context f = null;

    public interface a {
        List<b> a();

        double b();
    }

    private j(Context context) {
        this.f = context.getApplicationContext();
    }

    public static synchronized j a() {
        j jVar;
        synchronized (j.class) {
            jVar = e;
        }
        return jVar;
    }

    private String a(String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(b(str));
            return String.format("%1$032X", new BigInteger(1, instance.digest()));
        } catch (NoSuchAlgorithmException e2) {
            throw new RuntimeException(e2);
        }
    }

    public static synchronized void a(Context context) {
        synchronized (j.class) {
            if (e == null) {
                e = new j(context);
            }
        }
    }

    private void a(String str, String str2) {
        String valueOf = String.valueOf(System.nanoTime());
        String valueOf2 = String.valueOf(System.currentTimeMillis());
        TreeMap treeMap = new TreeMap();
        treeMap.put(IXAdRequestInfo.AD_COUNT, valueOf);
        treeMap.put("d", str2);
        treeMap.put("t", valueOf2);
        treeMap.put("s", a(valueOf + str2 + valueOf2 + "56C6A520%$C99119A0&^229(!@2746C7"));
        d.a(this.f, String.format("http://%1$s/diagnoses/v1/report", str), treeMap);
    }

    /* access modifiers changed from: private */
    public void a(List<b> list, double d2) {
        for (b a2 : list) {
            c cVar = new c();
            cVar.a("httpapi");
            cVar.a(a2);
            cVar.a(new com.xiaomi.b.a.a.a());
            String str = new String(com.xiaomi.a.a.g.a.a(new g(new b.a()).a(cVar)));
            if (((double) this.b.nextInt(10000)) < 10000.0d * d2) {
                try {
                    a("f3.mi-stat.gslb.mi-idc.com", str);
                } catch (IOException | Exception e2) {
                }
            }
        }
    }

    private byte[] b(String str) {
        try {
            return str.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e2) {
            return str.getBytes();
        }
    }

    public synchronized void a(a aVar) {
        this.f2475a.add(aVar);
    }

    public void b() {
        if (!this.d) {
            this.d = true;
            this.c.schedule(new n(this), 60000);
        }
    }

    public synchronized void b(a aVar) {
        this.f2475a.remove(aVar);
    }
}
