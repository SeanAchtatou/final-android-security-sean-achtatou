package com.xiaomi.network;

import com.xiaomi.a.a.c.c;
import com.xiaomi.b.a.a.a.b;
import com.xiaomi.network.j;
import java.util.ArrayList;
import java.util.List;
import java.util.TimerTask;
import org.apache.a.f;

class n extends TimerTask {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ j f2478a;

    n(j jVar) {
        this.f2478a = jVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.xiaomi.network.j.a(com.xiaomi.network.j, boolean):boolean
     arg types: [com.xiaomi.network.j, int]
     candidates:
      com.xiaomi.network.j.a(java.lang.String, java.lang.String):void
      com.xiaomi.network.j.a(java.util.List<com.xiaomi.b.a.a.a.b>, double):void
      com.xiaomi.network.j.a(com.xiaomi.network.j, boolean):boolean */
    public void run() {
        ArrayList<j.a> arrayList = new ArrayList<>();
        synchronized (this.f2478a) {
            arrayList.addAll(this.f2478a.f2475a);
        }
        for (j.a aVar : arrayList) {
            List<b> a2 = aVar.a();
            double b = aVar.b();
            if (a2 != null) {
                try {
                    if (a2.size() > 0) {
                        this.f2478a.a(a2, b);
                    }
                } catch (f e) {
                    c.a("uploadHostStat exception" + e.toString());
                }
            }
        }
        boolean unused = this.f2478a.d = false;
    }
}
