package com.xiaomi.network;

import android.content.Context;
import com.xiaomi.a.a.e.c;
import java.util.List;

public abstract class h {

    /* renamed from: a  reason: collision with root package name */
    private int f2474a;

    public h(int i) {
        this.f2474a = i;
    }

    public int a() {
        return this.f2474a;
    }

    public boolean a(Context context, String str, List<c> list) {
        return true;
    }

    public abstract String b(Context context, String str, List<c> list);
}
