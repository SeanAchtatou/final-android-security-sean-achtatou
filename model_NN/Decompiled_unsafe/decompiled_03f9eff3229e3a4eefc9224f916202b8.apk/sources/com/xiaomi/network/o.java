package com.xiaomi.network;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import org.json.JSONArray;
import org.json.JSONObject;

class o implements Comparable<o> {

    /* renamed from: a  reason: collision with root package name */
    String f2479a;
    protected int b;
    private final LinkedList<a> c;
    private long d;

    public o() {
        this(null, 0);
    }

    public o(String str, int i) {
        this.c = new LinkedList<>();
        this.d = 0;
        this.f2479a = str;
        this.b = i;
    }

    /* renamed from: a */
    public int compareTo(o oVar) {
        if (oVar == null) {
            return 1;
        }
        return oVar.b - this.b;
    }

    public synchronized o a(JSONObject jSONObject) {
        this.d = jSONObject.getLong("tt");
        this.b = jSONObject.getInt("wt");
        this.f2479a = jSONObject.getString("host");
        JSONArray jSONArray = jSONObject.getJSONArray("ah");
        for (int i = 0; i < jSONArray.length(); i++) {
            this.c.add(new a().a(jSONArray.getJSONObject(i)));
        }
        return this;
    }

    public synchronized ArrayList<a> a() {
        ArrayList<a> arrayList;
        arrayList = new ArrayList<>();
        Iterator<a> it = this.c.iterator();
        while (it.hasNext()) {
            a next = it.next();
            if (next.c() > this.d) {
                arrayList.add(next);
            }
        }
        this.d = System.currentTimeMillis();
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public synchronized void a(a aVar) {
        if (aVar != null) {
            j.a().b();
            this.c.add(aVar);
            int a2 = aVar.a();
            if (a2 > 0) {
                this.b += aVar.a();
            } else {
                int i = 0;
                int size = this.c.size() - 1;
                while (size >= 0 && this.c.get(size).a() < 0) {
                    i++;
                    size--;
                }
                this.b += a2 * i;
            }
            if (this.c.size() > 30) {
                this.b -= this.c.remove().a();
            }
        }
    }

    public synchronized JSONObject b() {
        JSONObject jSONObject;
        jSONObject = new JSONObject();
        jSONObject.put("tt", this.d);
        jSONObject.put("wt", this.b);
        jSONObject.put("host", this.f2479a);
        JSONArray jSONArray = new JSONArray();
        Iterator<a> it = this.c.iterator();
        while (it.hasNext()) {
            jSONArray.put(it.next().f());
        }
        jSONObject.put("ah", jSONArray);
        return jSONObject;
    }

    public String toString() {
        return this.f2479a + ":" + this.b;
    }
}
