package com.xiaomi.network;

import android.text.TextUtils;
import java.util.ArrayList;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONObject;

class c {

    /* renamed from: a  reason: collision with root package name */
    private String f2471a;
    private final ArrayList<b> b = new ArrayList<>();

    public c() {
    }

    public c(String str) {
        if (TextUtils.isEmpty(str)) {
            throw new IllegalArgumentException("the host is empty");
        }
        this.f2471a = str;
    }

    public synchronized b a() {
        b bVar;
        int size = this.b.size() - 1;
        while (true) {
            if (size < 0) {
                bVar = null;
                break;
            }
            bVar = this.b.get(size);
            if (bVar.a()) {
                f.a().e(bVar.e());
                break;
            }
            size--;
        }
        return bVar;
    }

    public synchronized c a(JSONObject jSONObject) {
        this.f2471a = jSONObject.getString("host");
        JSONArray jSONArray = jSONObject.getJSONArray("fbs");
        for (int i = 0; i < jSONArray.length(); i++) {
            this.b.add(new b(this.f2471a).a(jSONArray.getJSONObject(i)));
        }
        return this;
    }

    public synchronized void a(b bVar) {
        int i;
        int i2 = 0;
        while (true) {
            i = i2;
            if (i >= this.b.size()) {
                break;
            } else if (this.b.get(i).a(bVar)) {
                this.b.set(i, bVar);
                break;
            } else {
                i2 = i + 1;
            }
        }
        if (i >= this.b.size()) {
            this.b.add(bVar);
        }
    }

    public synchronized void a(boolean z) {
        for (int size = this.b.size() - 1; size >= 0; size--) {
            b bVar = this.b.get(size);
            if (z) {
                if (bVar.c()) {
                    this.b.remove(size);
                }
            } else if (!bVar.b()) {
                this.b.remove(size);
            }
        }
    }

    public ArrayList<b> b() {
        return this.b;
    }

    public String c() {
        return this.f2471a;
    }

    public synchronized JSONObject d() {
        JSONObject jSONObject;
        jSONObject = new JSONObject();
        jSONObject.put("host", this.f2471a);
        JSONArray jSONArray = new JSONArray();
        Iterator<b> it = this.b.iterator();
        while (it.hasNext()) {
            jSONArray.put(it.next().h());
        }
        jSONObject.put("fbs", jSONArray);
        return jSONObject;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.f2471a);
        sb.append("\n");
        Iterator<b> it = this.b.iterator();
        while (it.hasNext()) {
            sb.append(it.next());
        }
        return sb.toString();
    }
}
