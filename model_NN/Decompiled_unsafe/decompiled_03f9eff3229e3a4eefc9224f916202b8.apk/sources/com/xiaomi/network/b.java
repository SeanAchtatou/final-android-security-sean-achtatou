package com.xiaomi.network;

import android.text.TextUtils;
import com.tencent.stat.DeviceInfo;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONObject;

public class b {

    /* renamed from: a  reason: collision with root package name */
    public String f2470a;
    public String b;
    public String c;
    public String d;
    public String e;
    public String f;
    public String g;
    protected String h;
    private long i;
    private ArrayList<o> j = new ArrayList<>();
    private String k;
    private double l = 0.1d;
    private String m = "s.mi1.cc";
    private long n = LogBuilder.MAX_INTERVAL;

    public b(String str) {
        if (TextUtils.isEmpty(str)) {
            throw new IllegalArgumentException("the host is empty");
        }
        this.i = System.currentTimeMillis();
        this.j.add(new o(str, -1));
        this.f2470a = f.a().b();
        this.b = str;
    }

    private synchronized void c(String str) {
        Iterator<o> it = this.j.iterator();
        while (it.hasNext()) {
            if (TextUtils.equals(it.next().f2479a, str)) {
                it.remove();
            }
        }
    }

    public synchronized b a(JSONObject jSONObject) {
        this.f2470a = jSONObject.optString("net");
        this.n = jSONObject.getLong("ttl");
        this.l = jSONObject.getDouble("pct");
        this.i = jSONObject.getLong(DeviceInfo.TAG_TIMESTAMPS);
        this.d = jSONObject.optString("city");
        this.c = jSONObject.optString("prv");
        this.g = jSONObject.optString("cty");
        this.e = jSONObject.optString("isp");
        this.f = jSONObject.optString("ip");
        this.b = jSONObject.optString("host");
        this.h = jSONObject.optString("xf");
        JSONArray jSONArray = jSONObject.getJSONArray("fbs");
        for (int i2 = 0; i2 < jSONArray.length(); i2++) {
            a(new o().a(jSONArray.getJSONObject(i2)));
        }
        return this;
    }

    public ArrayList<String> a(String str) {
        if (TextUtils.isEmpty(str)) {
            throw new IllegalArgumentException("the url is empty.");
        }
        URL url = new URL(str);
        if (TextUtils.equals(url.getHost(), this.b)) {
            ArrayList<String> arrayList = new ArrayList<>();
            Iterator<String> it = a(true).iterator();
            while (it.hasNext()) {
                d a2 = d.a(it.next(), url.getPort());
                arrayList.add(new URL(url.getProtocol(), a2.b(), a2.a(), url.getFile()).toString());
            }
            return arrayList;
        }
        throw new IllegalArgumentException("the url is not supported by the fallback");
    }

    public synchronized ArrayList<String> a(boolean z) {
        ArrayList<String> arrayList;
        synchronized (this) {
            o[] oVarArr = new o[this.j.size()];
            this.j.toArray(oVarArr);
            Arrays.sort(oVarArr);
            arrayList = new ArrayList<>();
            for (o oVar : oVarArr) {
                if (z) {
                    arrayList.add(oVar.f2479a);
                } else {
                    int indexOf = oVar.f2479a.indexOf(":");
                    if (indexOf != -1) {
                        arrayList.add(oVar.f2479a.substring(0, indexOf));
                    } else {
                        arrayList.add(oVar.f2479a);
                    }
                }
            }
        }
        return arrayList;
    }

    public void a(double d2) {
        this.l = d2;
    }

    public void a(long j2) {
        if (j2 <= 0) {
            throw new IllegalArgumentException("the duration is invalid " + j2);
        }
        this.n = j2;
    }

    /* access modifiers changed from: package-private */
    public synchronized void a(o oVar) {
        c(oVar.f2479a);
        this.j.add(oVar);
    }

    public void a(String str, int i2, long j2, long j3, Exception exc) {
        a(str, new a(i2, j2, j3, exc));
    }

    public void a(String str, long j2, long j3) {
        try {
            b(new URL(str).getHost(), j2, j3);
        } catch (MalformedURLException e2) {
        }
    }

    public void a(String str, long j2, long j3, Exception exc) {
        try {
            b(new URL(str).getHost(), j2, j3, exc);
        } catch (MalformedURLException e2) {
        }
    }

    public synchronized void a(String str, a aVar) {
        Iterator<o> it = this.j.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            o next = it.next();
            if (TextUtils.equals(str, next.f2479a)) {
                next.a(aVar);
                break;
            }
        }
    }

    public synchronized void a(String[] strArr) {
        for (int size = this.j.size() - 1; size >= 0; size--) {
            int length = strArr.length;
            int i2 = 0;
            while (true) {
                if (i2 >= length) {
                    break;
                }
                if (TextUtils.equals(this.j.get(size).f2479a, strArr[i2])) {
                    this.j.remove(size);
                    break;
                }
                i2++;
            }
        }
        Iterator<o> it = this.j.iterator();
        int i3 = 0;
        while (it.hasNext()) {
            o next = it.next();
            i3 = next.b > i3 ? next.b : i3;
        }
        for (int i4 = 0; i4 < strArr.length; i4++) {
            a(new o(strArr[i4], (strArr.length + i3) - i4));
        }
    }

    public boolean a() {
        return TextUtils.equals(this.f2470a, f.a().b());
    }

    public boolean a(b bVar) {
        return TextUtils.equals(this.f2470a, bVar.f2470a);
    }

    public void b(String str) {
        this.m = str;
    }

    public void b(String str, long j2, long j3) {
        a(str, 0, j2, j3, null);
    }

    public void b(String str, long j2, long j3, Exception exc) {
        a(str, -1, j2, j3, exc);
    }

    public boolean b() {
        return System.currentTimeMillis() - this.i < this.n;
    }

    /* access modifiers changed from: package-private */
    public boolean c() {
        long j2 = 864000000;
        if (864000000 < this.n) {
            j2 = this.n;
        }
        long currentTimeMillis = System.currentTimeMillis();
        return currentTimeMillis - this.i > j2 || (currentTimeMillis - this.i > this.n && this.f2470a.startsWith("WIFI-"));
    }

    public synchronized ArrayList<String> d() {
        return a(false);
    }

    public synchronized String e() {
        String str;
        if (!TextUtils.isEmpty(this.k)) {
            str = this.k;
        } else if (TextUtils.isEmpty(this.e)) {
            str = "hardcode_isp";
        } else {
            this.k = f.a(new String[]{this.e, this.c, this.d, this.g, this.f}, "_");
            str = this.k;
        }
        return str;
    }

    /* access modifiers changed from: package-private */
    public ArrayList<o> f() {
        return this.j;
    }

    public double g() {
        if (this.l < 1.0E-5d) {
            return 0.1d;
        }
        return this.l;
    }

    public synchronized JSONObject h() {
        JSONObject jSONObject;
        jSONObject = new JSONObject();
        jSONObject.put("net", this.f2470a);
        jSONObject.put("ttl", this.n);
        jSONObject.put("pct", this.l);
        jSONObject.put(DeviceInfo.TAG_TIMESTAMPS, this.i);
        jSONObject.put("city", this.d);
        jSONObject.put("prv", this.c);
        jSONObject.put("cty", this.g);
        jSONObject.put("isp", this.e);
        jSONObject.put("ip", this.f);
        jSONObject.put("host", this.b);
        jSONObject.put("xf", this.h);
        JSONArray jSONArray = new JSONArray();
        Iterator<o> it = this.j.iterator();
        while (it.hasNext()) {
            jSONArray.put(it.next().b());
        }
        jSONObject.put("fbs", jSONArray);
        return jSONObject;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.f2470a);
        sb.append("\n");
        sb.append(e());
        Iterator<o> it = this.j.iterator();
        while (it.hasNext()) {
            sb.append("\n");
            sb.append(it.next().toString());
        }
        sb.append("\n");
        return sb.toString();
    }
}
