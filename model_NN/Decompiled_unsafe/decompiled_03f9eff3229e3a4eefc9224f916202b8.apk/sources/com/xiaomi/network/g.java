package com.xiaomi.network;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import com.sina.weibo.sdk.component.ShareRequestParam;
import com.tencent.stat.DeviceInfo;
import com.xiaomi.a.a.e.a;
import com.xiaomi.a.a.e.c;
import com.xiaomi.a.a.e.d;
import com.xiaomi.network.f;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class g extends f {
    private final int f;
    private final int g;
    private int h;

    protected g(Context context, e eVar, f.b bVar, String str) {
        this(context, eVar, bVar, str, null, null);
    }

    protected g(Context context, e eVar, f.b bVar, String str, String str2, String str3) {
        super(context, eVar, bVar, str, str2, str3);
        this.f = 80;
        this.g = 5222;
        this.h = 80;
        a("resolver.msg.xiaomi.net", "resolver.msg.xiaomi.net:5222");
    }

    /* access modifiers changed from: protected */
    public String a(ArrayList<String> arrayList, String str, String str2) {
        ArrayList<String> a2;
        ArrayList<String> arrayList2 = new ArrayList<>();
        ArrayList<c> arrayList3 = new ArrayList<>();
        arrayList3.add(new a("type", str));
        if (str.equals("wap")) {
            arrayList3.add(new a("connpt", d.f(this.c)));
        }
        arrayList3.add(new a("uuid", str2));
        arrayList3.add(new a("list", a(arrayList, ",")));
        b c = c("resolver.msg.xiaomi.net");
        String format = String.format(Locale.US, "http://%1$s/gslb/?ver=3.0", "resolver.msg.xiaomi.net:" + this.h);
        if (c == null) {
            arrayList2.add(format);
            synchronized (b) {
                Iterator it = ((ArrayList) b.get("resolver.msg.xiaomi.net")).iterator();
                while (it.hasNext()) {
                    arrayList2.add(String.format(Locale.US, "http://%1$s/gslb/?ver=3.0", (String) it.next()));
                }
            }
            a2 = arrayList2;
        } else {
            a2 = c.a(format);
        }
        Iterator it2 = a2.iterator();
        IOException e = null;
        while (it2.hasNext()) {
            Uri.Builder buildUpon = Uri.parse((String) it2.next()).buildUpon();
            for (c cVar : arrayList3) {
                buildUpon.appendQueryParameter(cVar.a(), cVar.b());
            }
            try {
                return this.d == null ? d.a(this.c, new URL(buildUpon.toString())) : this.d.a(buildUpon.toString());
            } catch (IOException e2) {
                e = e2;
            }
        }
        if (e != null) {
            return super.a(arrayList, str, str2);
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public String c() {
        return "resolver.msg.xiaomi.net";
    }

    /* access modifiers changed from: protected */
    public void g(String str) {
        synchronized (this.f2473a) {
            this.f2473a.clear();
            JSONObject jSONObject = new JSONObject(str);
            if (jSONObject.optInt(DeviceInfo.TAG_VERSION) != 2) {
                throw new JSONException("Bad version");
            }
            JSONArray optJSONArray = jSONObject.optJSONArray(ShareRequestParam.RESP_UPLOAD_PIC_PARAM_DATA);
            for (int i = 0; i < optJSONArray.length(); i++) {
                c a2 = new c().a(optJSONArray.getJSONObject(i));
                this.f2473a.put(a2.c(), a2);
            }
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean g() {
        /*
            r4 = this;
            r0 = 1
            java.util.Map r1 = r4.f2473a
            monitor-enter(r1)
            boolean r2 = com.xiaomi.network.g.e     // Catch:{ all -> 0x0044 }
            if (r2 != 0) goto L_0x0042
            r2 = 1
            com.xiaomi.network.g.e = r2     // Catch:{ all -> 0x0044 }
            java.util.Map r2 = r4.f2473a     // Catch:{ all -> 0x0044 }
            r2.clear()     // Catch:{ all -> 0x0044 }
            java.lang.String r2 = r4.f()     // Catch:{ Throwable -> 0x0024 }
            boolean r3 = android.text.TextUtils.isEmpty(r2)     // Catch:{ Throwable -> 0x0024 }
            if (r3 != 0) goto L_0x003f
            r4.g(r2)     // Catch:{ Throwable -> 0x0024 }
            java.lang.String r2 = "loading the new hosts succeed"
            com.xiaomi.a.a.c.c.b(r2)     // Catch:{ Throwable -> 0x0024 }
            monitor-exit(r1)     // Catch:{ all -> 0x0044 }
        L_0x0023:
            return r0
        L_0x0024:
            r0 = move-exception
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0044 }
            r2.<init>()     // Catch:{ all -> 0x0044 }
            java.lang.String r3 = "load bucket failure: "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0044 }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x0044 }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ all -> 0x0044 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0044 }
            com.xiaomi.a.a.c.c.a(r0)     // Catch:{ all -> 0x0044 }
        L_0x003f:
            monitor-exit(r1)     // Catch:{ all -> 0x0044 }
            r0 = 0
            goto L_0x0023
        L_0x0042:
            monitor-exit(r1)     // Catch:{ all -> 0x0044 }
            goto L_0x0023
        L_0x0044:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0044 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.xiaomi.network.g.g():boolean");
    }

    public void h() {
        synchronized (this.f2473a) {
            try {
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(this.c.openFileOutput(i(), 0)));
                String jSONObject = m().toString();
                if (!TextUtils.isEmpty(jSONObject)) {
                    bufferedWriter.write(jSONObject);
                }
                bufferedWriter.close();
            } catch (Exception e) {
                com.xiaomi.a.a.c.c.a("persist bucket failure: " + e.getMessage());
            }
        }
    }

    public void k() {
        synchronized (this.f2473a) {
            for (c a2 : this.f2473a.values()) {
                a2.a(true);
            }
            boolean z = false;
            while (!z) {
                Iterator it = this.f2473a.keySet().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        z = true;
                        break;
                    }
                    String str = (String) it.next();
                    if (((c) this.f2473a.get(str)).b().isEmpty()) {
                        this.f2473a.remove(str);
                        z = false;
                        break;
                    }
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public JSONObject m() {
        JSONObject jSONObject;
        synchronized (this.f2473a) {
            jSONObject = new JSONObject();
            jSONObject.put(DeviceInfo.TAG_VERSION, 2);
            jSONObject.put(ShareRequestParam.RESP_UPLOAD_PIC_PARAM_DATA, l());
        }
        return jSONObject;
    }
}
