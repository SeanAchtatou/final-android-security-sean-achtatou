package com.xiaomi.a.a.e;

public class a implements c {

    /* renamed from: a  reason: collision with root package name */
    private final String f2333a;
    private final String b;

    public a(String str, String str2) {
        if (str == null) {
            throw new IllegalArgumentException("Name may not be null");
        }
        this.f2333a = str;
        this.b = str2;
    }

    public String a() {
        return this.f2333a;
    }

    public String b() {
        return this.b;
    }
}
