package com.xiaomi.a.a.f;

import java.util.LinkedList;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private LinkedList<C0055a> f2338a = new LinkedList<>();

    /* renamed from: com.xiaomi.a.a.f.a$a  reason: collision with other inner class name */
    public static class C0055a {
        /* access modifiers changed from: private */
        public static final a d = new a();

        /* renamed from: a  reason: collision with root package name */
        public int f2339a;
        public String b;
        public Object c;

        C0055a(int i, Object obj) {
            this.f2339a = i;
            this.c = obj;
        }
    }

    public static a a() {
        return C0055a.d;
    }

    private void d() {
        if (this.f2338a.size() > 100) {
            this.f2338a.removeFirst();
        }
    }

    public synchronized void a(Object obj) {
        this.f2338a.add(new C0055a(0, obj));
        d();
    }

    public synchronized int b() {
        return this.f2338a.size();
    }

    public synchronized LinkedList<C0055a> c() {
        LinkedList<C0055a> linkedList;
        linkedList = this.f2338a;
        this.f2338a = new LinkedList<>();
        return linkedList;
    }
}
