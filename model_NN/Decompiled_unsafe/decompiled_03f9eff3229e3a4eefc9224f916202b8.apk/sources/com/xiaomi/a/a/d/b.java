package com.xiaomi.a.a.d;

import android.os.Handler;
import android.os.Looper;
import com.xiaomi.a.a.c.c;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

public class b {

    /* renamed from: a  reason: collision with root package name */
    private a f2329a;
    /* access modifiers changed from: private */
    public Handler b;
    /* access modifiers changed from: private */
    public volatile boolean c;
    private final boolean d;
    /* access modifiers changed from: private */
    public int e;
    /* access modifiers changed from: private */
    public volatile C0054b f;

    private class a extends Thread {
        private final LinkedBlockingQueue<C0054b> b = new LinkedBlockingQueue<>();

        public a() {
            super("PackageProcessor");
        }

        public void a(C0054b bVar) {
            this.b.add(bVar);
        }

        public void run() {
            int a2 = b.this.e > 0 ? b.this.e : 1;
            while (!b.this.c) {
                try {
                    C0054b unused = b.this.f = this.b.poll((long) a2, TimeUnit.SECONDS);
                    if (b.this.f != null) {
                        b.this.b.sendMessage(b.this.b.obtainMessage(0, b.this.f));
                        b.this.f.b();
                        b.this.b.sendMessage(b.this.b.obtainMessage(1, b.this.f));
                    } else if (b.this.e > 0) {
                        b.this.a();
                    }
                } catch (InterruptedException e) {
                    c.a(e);
                }
            }
        }
    }

    /* renamed from: com.xiaomi.a.a.d.b$b  reason: collision with other inner class name */
    public static abstract class C0054b {
        public void a() {
        }

        public abstract void b();

        public void c() {
        }
    }

    public b() {
        this(false);
    }

    public b(boolean z) {
        this(z, 0);
    }

    public b(boolean z, int i) {
        this.b = null;
        this.c = false;
        this.e = 0;
        this.b = new c(this, Looper.getMainLooper());
        this.d = z;
        this.e = i;
    }

    /* access modifiers changed from: private */
    public synchronized void a() {
        this.f2329a = null;
        this.c = true;
    }

    public synchronized void a(C0054b bVar) {
        if (this.f2329a == null) {
            this.f2329a = new a();
            this.f2329a.setDaemon(this.d);
            this.c = false;
            this.f2329a.start();
        }
        this.f2329a.a(bVar);
    }

    public void a(C0054b bVar, long j) {
        this.b.postDelayed(new d(this, bVar), j);
    }
}
