package com.xiaomi.a.a.d;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.xiaomi.a.a.d.b;

class c extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ b f2331a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    c(b bVar, Looper looper) {
        super(looper);
        this.f2331a = bVar;
    }

    public void handleMessage(Message message) {
        b.C0054b bVar = (b.C0054b) message.obj;
        if (message.what == 0) {
            bVar.a();
        } else if (message.what == 1) {
            bVar.c();
        }
        super.handleMessage(message);
    }
}
