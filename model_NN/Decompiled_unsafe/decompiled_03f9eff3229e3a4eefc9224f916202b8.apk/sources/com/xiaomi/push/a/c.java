package com.xiaomi.push.a;

import com.xiaomi.push.a.b;
import java.io.File;
import java.util.Date;

class c extends b.C0059b {

    /* renamed from: a  reason: collision with root package name */
    File f2484a;
    final /* synthetic */ int d;
    final /* synthetic */ Date e;
    final /* synthetic */ Date f;
    final /* synthetic */ String g;
    final /* synthetic */ String h;
    final /* synthetic */ boolean i;
    final /* synthetic */ b j;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    c(b bVar, int i2, Date date, Date date2, String str, String str2, boolean z) {
        super();
        this.j = bVar;
        this.d = i2;
        this.e = date;
        this.f = date2;
        this.g = str;
        this.h = str2;
        this.i = z;
    }

    public void b() {
        if (com.xiaomi.a.a.b.c.d()) {
            try {
                File file = new File(this.j.b.getExternalFilesDir(null) + "/.logcache");
                file.mkdirs();
                if (file.isDirectory()) {
                    a aVar = new a();
                    aVar.a(this.d);
                    this.f2484a = aVar.a(this.j.b, this.e, this.f, file);
                }
            } catch (NullPointerException e2) {
            }
        }
    }

    public void c() {
        if (this.f2484a != null && this.f2484a.exists()) {
            this.j.f2481a.add(new b.c(this.g, this.h, this.f2484a, this.i));
        }
        this.j.a(0);
    }
}
