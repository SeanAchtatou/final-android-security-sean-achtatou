package com.xiaomi.push.a;

import android.annotation.SuppressLint;
import android.content.Context;
import com.xiaomi.a.a.c.c;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class a {
    private static String b = "/MiPushLog";
    @SuppressLint({"SimpleDateFormat"})

    /* renamed from: a  reason: collision with root package name */
    private final SimpleDateFormat f2480a = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private String c;
    private String d;
    private boolean e;
    private int f;
    private int g = 2097152;
    private ArrayList<File> h = new ArrayList<>();

    a() {
    }

    private void a(BufferedReader bufferedReader, BufferedWriter bufferedWriter, Pattern pattern) {
        int i;
        boolean z;
        char[] cArr = new char[4096];
        int read = bufferedReader.read(cArr);
        boolean z2 = false;
        while (read != -1 && !z2) {
            String str = new String(cArr, 0, read);
            Matcher matcher = pattern.matcher(str);
            int i2 = 0;
            int i3 = 0;
            while (true) {
                if (i2 >= read || !matcher.find(i2)) {
                    i = read;
                    z = z2;
                } else {
                    i = matcher.start();
                    String substring = str.substring(i, this.c.length() + i);
                    if (this.e) {
                        if (substring.compareTo(this.d) > 0) {
                            z = true;
                            break;
                        }
                    } else if (substring.compareTo(this.c) >= 0) {
                        this.e = true;
                        i3 = i;
                    }
                    int indexOf = str.indexOf(10, i);
                    i2 = indexOf != -1 ? i + indexOf : i + this.c.length();
                }
            }
            i = read;
            z = z2;
            if (this.e) {
                int i4 = i - i3;
                this.f += i4;
                if (z) {
                    bufferedWriter.write(cArr, i3, i4);
                    return;
                }
                bufferedWriter.write(cArr, i3, i4);
                if (this.f > this.g) {
                    return;
                }
            }
            z2 = z;
            read = bufferedReader.read(cArr);
        }
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:20:0x00c1=Splitter:B:20:0x00c1, B:15:0x009e=Splitter:B:15:0x009e} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void b(java.io.File r9) {
        /*
            r8 = this;
            r2 = 0
            java.lang.String r0 = "\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}"
            java.lang.String r0 = "\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}"
            java.util.regex.Pattern r4 = java.util.regex.Pattern.compile(r0)
            java.io.BufferedWriter r1 = new java.io.BufferedWriter     // Catch:{ FileNotFoundException -> 0x009c, IOException -> 0x00bf, all -> 0x00e2 }
            java.io.OutputStreamWriter r0 = new java.io.OutputStreamWriter     // Catch:{ FileNotFoundException -> 0x009c, IOException -> 0x00bf, all -> 0x00e2 }
            java.io.FileOutputStream r3 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x009c, IOException -> 0x00bf, all -> 0x00e2 }
            r3.<init>(r9)     // Catch:{ FileNotFoundException -> 0x009c, IOException -> 0x00bf, all -> 0x00e2 }
            r0.<init>(r3)     // Catch:{ FileNotFoundException -> 0x009c, IOException -> 0x00bf, all -> 0x00e2 }
            r1.<init>(r0)     // Catch:{ FileNotFoundException -> 0x009c, IOException -> 0x00bf, all -> 0x00e2 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x00f5, IOException -> 0x00f0 }
            r0.<init>()     // Catch:{ FileNotFoundException -> 0x00f5, IOException -> 0x00f0 }
            java.lang.String r3 = "model :"
            java.lang.StringBuilder r3 = r0.append(r3)     // Catch:{ FileNotFoundException -> 0x00f5, IOException -> 0x00f0 }
            java.lang.String r5 = android.os.Build.MODEL     // Catch:{ FileNotFoundException -> 0x00f5, IOException -> 0x00f0 }
            r3.append(r5)     // Catch:{ FileNotFoundException -> 0x00f5, IOException -> 0x00f0 }
            java.lang.String r3 = "; os :"
            java.lang.StringBuilder r3 = r0.append(r3)     // Catch:{ FileNotFoundException -> 0x00f5, IOException -> 0x00f0 }
            java.lang.String r5 = android.os.Build.VERSION.INCREMENTAL     // Catch:{ FileNotFoundException -> 0x00f5, IOException -> 0x00f0 }
            r3.append(r5)     // Catch:{ FileNotFoundException -> 0x00f5, IOException -> 0x00f0 }
            java.lang.String r3 = "; uid :"
            java.lang.StringBuilder r3 = r0.append(r3)     // Catch:{ FileNotFoundException -> 0x00f5, IOException -> 0x00f0 }
            java.lang.String r5 = com.xiaomi.d.e.h.b()     // Catch:{ FileNotFoundException -> 0x00f5, IOException -> 0x00f0 }
            r3.append(r5)     // Catch:{ FileNotFoundException -> 0x00f5, IOException -> 0x00f0 }
            java.lang.String r3 = "; lng :"
            java.lang.StringBuilder r3 = r0.append(r3)     // Catch:{ FileNotFoundException -> 0x00f5, IOException -> 0x00f0 }
            java.util.Locale r5 = java.util.Locale.getDefault()     // Catch:{ FileNotFoundException -> 0x00f5, IOException -> 0x00f0 }
            java.lang.String r5 = r5.toString()     // Catch:{ FileNotFoundException -> 0x00f5, IOException -> 0x00f0 }
            r3.append(r5)     // Catch:{ FileNotFoundException -> 0x00f5, IOException -> 0x00f0 }
            java.lang.String r3 = "; sdk :"
            java.lang.StringBuilder r3 = r0.append(r3)     // Catch:{ FileNotFoundException -> 0x00f5, IOException -> 0x00f0 }
            r5 = 8
            r3.append(r5)     // Catch:{ FileNotFoundException -> 0x00f5, IOException -> 0x00f0 }
            java.lang.String r3 = "\n"
            r0.append(r3)     // Catch:{ FileNotFoundException -> 0x00f5, IOException -> 0x00f0 }
            java.lang.String r0 = r0.toString()     // Catch:{ FileNotFoundException -> 0x00f5, IOException -> 0x00f0 }
            r1.write(r0)     // Catch:{ FileNotFoundException -> 0x00f5, IOException -> 0x00f0 }
            r0 = 0
            r8.f = r0     // Catch:{ FileNotFoundException -> 0x00f5, IOException -> 0x00f0 }
            java.util.ArrayList<java.io.File> r0 = r8.h     // Catch:{ FileNotFoundException -> 0x00f5, IOException -> 0x00f0 }
            java.util.Iterator r5 = r0.iterator()     // Catch:{ FileNotFoundException -> 0x00f5, IOException -> 0x00f0 }
            r3 = r2
        L_0x0072:
            boolean r0 = r5.hasNext()     // Catch:{ FileNotFoundException -> 0x00f7, IOException -> 0x00f2, all -> 0x00ed }
            if (r0 == 0) goto L_0x0095
            java.lang.Object r0 = r5.next()     // Catch:{ FileNotFoundException -> 0x00f7, IOException -> 0x00f2, all -> 0x00ed }
            java.io.File r0 = (java.io.File) r0     // Catch:{ FileNotFoundException -> 0x00f7, IOException -> 0x00f2, all -> 0x00ed }
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ FileNotFoundException -> 0x00f7, IOException -> 0x00f2, all -> 0x00ed }
            java.io.InputStreamReader r6 = new java.io.InputStreamReader     // Catch:{ FileNotFoundException -> 0x00f7, IOException -> 0x00f2, all -> 0x00ed }
            java.io.FileInputStream r7 = new java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x00f7, IOException -> 0x00f2, all -> 0x00ed }
            r7.<init>(r0)     // Catch:{ FileNotFoundException -> 0x00f7, IOException -> 0x00f2, all -> 0x00ed }
            r6.<init>(r7)     // Catch:{ FileNotFoundException -> 0x00f7, IOException -> 0x00f2, all -> 0x00ed }
            r2.<init>(r6)     // Catch:{ FileNotFoundException -> 0x00f7, IOException -> 0x00f2, all -> 0x00ed }
            r8.a(r2, r1, r4)     // Catch:{ FileNotFoundException -> 0x00f5, IOException -> 0x00f0 }
            r2.close()     // Catch:{ FileNotFoundException -> 0x00f5, IOException -> 0x00f0 }
            r3 = r2
            goto L_0x0072
        L_0x0095:
            com.xiaomi.a.a.b.a.a(r1)
            com.xiaomi.a.a.b.a.a(r3)
        L_0x009b:
            return
        L_0x009c:
            r0 = move-exception
            r1 = r2
        L_0x009e:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x00eb }
            r3.<init>()     // Catch:{ all -> 0x00eb }
            java.lang.String r4 = "LOG: filter error = "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x00eb }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x00eb }
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ all -> 0x00eb }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00eb }
            com.xiaomi.a.a.c.c.c(r0)     // Catch:{ all -> 0x00eb }
            com.xiaomi.a.a.b.a.a(r1)
            com.xiaomi.a.a.b.a.a(r2)
            goto L_0x009b
        L_0x00bf:
            r0 = move-exception
            r1 = r2
        L_0x00c1:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x00eb }
            r3.<init>()     // Catch:{ all -> 0x00eb }
            java.lang.String r4 = "LOG: filter error = "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x00eb }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x00eb }
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ all -> 0x00eb }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00eb }
            com.xiaomi.a.a.c.c.c(r0)     // Catch:{ all -> 0x00eb }
            com.xiaomi.a.a.b.a.a(r1)
            com.xiaomi.a.a.b.a.a(r2)
            goto L_0x009b
        L_0x00e2:
            r0 = move-exception
            r1 = r2
        L_0x00e4:
            com.xiaomi.a.a.b.a.a(r1)
            com.xiaomi.a.a.b.a.a(r2)
            throw r0
        L_0x00eb:
            r0 = move-exception
            goto L_0x00e4
        L_0x00ed:
            r0 = move-exception
            r2 = r3
            goto L_0x00e4
        L_0x00f0:
            r0 = move-exception
            goto L_0x00c1
        L_0x00f2:
            r0 = move-exception
            r2 = r3
            goto L_0x00c1
        L_0x00f5:
            r0 = move-exception
            goto L_0x009e
        L_0x00f7:
            r0 = move-exception
            r2 = r3
            goto L_0x009e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.xiaomi.push.a.a.b(java.io.File):void");
    }

    /* access modifiers changed from: package-private */
    public a a(File file) {
        if (file.exists()) {
            this.h.add(file);
        }
        return this;
    }

    /* access modifiers changed from: package-private */
    public a a(Date date, Date date2) {
        if (date.after(date2)) {
            this.c = this.f2480a.format(date2);
            this.d = this.f2480a.format(date);
        } else {
            this.c = this.f2480a.format(date);
            this.d = this.f2480a.format(date2);
        }
        return this;
    }

    /* access modifiers changed from: package-private */
    public File a(Context context, Date date, Date date2, File file) {
        File file2;
        if ("com.xiaomi.xmsf".equalsIgnoreCase(context.getPackageName())) {
            file2 = context.getFilesDir();
            a(new File(file2, "xmsf.log.1"));
            a(new File(file2, "xmsf.log"));
        } else {
            file2 = new File(context.getExternalFilesDir(null) + b);
            a(new File(file2, "log0.txt"));
            a(new File(file2, "log1.txt"));
        }
        if (!file2.isDirectory()) {
            return null;
        }
        File file3 = new File(file, date.getTime() + "-" + date2.getTime() + ".zip");
        if (file3.exists()) {
            return null;
        }
        a(date, date2);
        long currentTimeMillis = System.currentTimeMillis();
        File file4 = new File(file, "log.txt");
        b(file4);
        c.c("LOG: filter cost = " + (System.currentTimeMillis() - currentTimeMillis));
        if (file4.exists()) {
            long currentTimeMillis2 = System.currentTimeMillis();
            com.xiaomi.a.a.b.a.a(file3, file4);
            c.c("LOG: zip cost = " + (System.currentTimeMillis() - currentTimeMillis2));
            file4.delete();
            if (file3.exists()) {
                return file3;
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public void a(int i) {
        if (i != 0) {
            this.g = i;
        }
    }
}
