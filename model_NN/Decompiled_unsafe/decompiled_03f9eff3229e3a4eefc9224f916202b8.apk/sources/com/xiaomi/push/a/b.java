package com.xiaomi.push.a;

import android.content.Context;
import android.content.SharedPreferences;
import cn.banshenggua.aichang.utils.Constants;
import com.xiaomi.a.a.d.b;
import com.xiaomi.a.a.e.d;
import com.xiaomi.d.e.h;
import com.xiaomi.d.e.i;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import org.json.JSONException;
import org.json.JSONObject;

public class b {
    private static b c = null;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final ConcurrentLinkedQueue<C0059b> f2481a = new ConcurrentLinkedQueue<>();
    /* access modifiers changed from: private */
    public Context b;

    class a extends C0059b {
        a() {
            super();
        }

        public void b() {
            b.this.b();
        }
    }

    /* renamed from: com.xiaomi.push.a.b$b  reason: collision with other inner class name */
    class C0059b extends b.C0054b {
        long b = System.currentTimeMillis();

        C0059b() {
        }

        public void b() {
        }

        public boolean d() {
            return true;
        }

        /* access modifiers changed from: package-private */
        public final boolean e() {
            return System.currentTimeMillis() - this.b > 172800000;
        }
    }

    class c extends C0059b {

        /* renamed from: a  reason: collision with root package name */
        String f2483a;
        String d;
        File e;
        int f;
        boolean g;
        boolean h;

        c(String str, String str2, File file, boolean z) {
            super();
            this.f2483a = str;
            this.d = str2;
            this.e = file;
            this.h = z;
        }

        private boolean f() {
            int i2;
            SharedPreferences sharedPreferences = b.this.b.getSharedPreferences("log.timestamp", 0);
            String string = sharedPreferences.getString("log.requst", "");
            long currentTimeMillis = System.currentTimeMillis();
            try {
                JSONObject jSONObject = new JSONObject(string);
                currentTimeMillis = jSONObject.getLong("time");
                i2 = jSONObject.getInt("times");
            } catch (JSONException e2) {
                i2 = 0;
            }
            if (System.currentTimeMillis() - currentTimeMillis >= LogBuilder.MAX_INTERVAL) {
                currentTimeMillis = System.currentTimeMillis();
                i2 = 0;
            } else if (i2 > 10) {
                return false;
            }
            JSONObject jSONObject2 = new JSONObject();
            try {
                jSONObject2.put("time", currentTimeMillis);
                jSONObject2.put("times", i2 + 1);
                sharedPreferences.edit().putString("log.requst", jSONObject2.toString()).commit();
            } catch (JSONException e3) {
                com.xiaomi.a.a.c.c.c("JSONException on put " + e3.getMessage());
            }
            return true;
        }

        public void b() {
            try {
                if (f()) {
                    HashMap hashMap = new HashMap();
                    hashMap.put("uid", h.b());
                    hashMap.put("token", this.d);
                    hashMap.put("net", d.f(b.this.b));
                    d.a(this.f2483a, hashMap, this.e, "file");
                }
                this.g = true;
            } catch (IOException e2) {
            }
        }

        public void c() {
            if (!this.g) {
                this.f++;
                if (this.f < 3) {
                    b.this.f2481a.add(this);
                }
            }
            if (this.g || this.f >= 3) {
                this.e.delete();
            }
            b.this.a((long) ((1 << this.f) * Constants.CLEARIMGED));
        }

        public boolean d() {
            return d.e(b.this.b) || (this.h && d.d(b.this.b));
        }
    }

    private b(Context context) {
        this.b = context;
        this.f2481a.add(new a());
        b(0);
    }

    public static b a(Context context) {
        if (c == null) {
            synchronized (b.class) {
                if (c == null) {
                    c = new b(context);
                }
            }
        }
        c.b = context;
        return c;
    }

    /* access modifiers changed from: private */
    public void a(long j) {
        C0059b peek = this.f2481a.peek();
        if (peek != null && peek.d()) {
            b(j);
        }
    }

    /* access modifiers changed from: private */
    public void b() {
        if (!com.xiaomi.a.a.b.c.b() && !com.xiaomi.a.a.b.c.a()) {
            try {
                File file = new File(this.b.getExternalFilesDir(null) + "/.logcache");
                if (file.exists() && file.isDirectory()) {
                    for (File delete : file.listFiles()) {
                        delete.delete();
                    }
                }
            } catch (NullPointerException e) {
            }
        }
    }

    private void b(long j) {
        if (!this.f2481a.isEmpty()) {
            i.a(new d(this), j);
        }
    }

    private void c() {
        while (!this.f2481a.isEmpty()) {
            if (this.f2481a.peek().e() || this.f2481a.size() > 6) {
                com.xiaomi.a.a.c.c.c("remove Expired task");
                this.f2481a.remove();
            } else {
                return;
            }
        }
    }

    public void a() {
        c();
        a(0);
    }

    public void a(String str, String str2, Date date, Date date2, int i, boolean z) {
        this.f2481a.add(new c(this, i, date, date2, str, str2, z));
        b(0);
    }
}
