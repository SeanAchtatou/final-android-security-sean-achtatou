package com.xiaomi.push.a;

import com.xiaomi.a.a.c.a;

public class e implements a {

    /* renamed from: a  reason: collision with root package name */
    private a f2486a = null;
    private a b = null;

    public e(a aVar, a aVar2) {
        this.f2486a = aVar;
        this.b = aVar2;
    }

    public void a(String str) {
        if (this.f2486a != null) {
            this.f2486a.a(str);
        }
        if (this.b != null) {
            this.b.a(str);
        }
    }

    public void a(String str, Throwable th) {
        if (this.f2486a != null) {
            this.f2486a.a(str, th);
        }
        if (this.b != null) {
            this.b.a(str, th);
        }
    }
}
