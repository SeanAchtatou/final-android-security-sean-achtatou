package com.xiaomi.push.service;

import cn.banshenggua.aichang.room.message.SocketMessage;
import com.xiaomi.a.a.c.c;
import com.xiaomi.d.p;
import com.xiaomi.f.a.h;
import com.xiaomi.push.service.XMPushService;

final class aj extends XMPushService.e {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ XMPushService f2519a;
    final /* synthetic */ h b;
    final /* synthetic */ String c;
    final /* synthetic */ String e;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    aj(int i, XMPushService xMPushService, h hVar, String str, String str2) {
        super(i);
        this.f2519a = xMPushService;
        this.b = hVar;
        this.c = str;
        this.e = str2;
    }

    public void a() {
        try {
            h a2 = ad.e(this.f2519a, this.b);
            a2.h.a(SocketMessage.MSG_ERROR_KEY, this.c);
            a2.h.a("reason", this.e);
            this.f2519a.b(a2);
        } catch (p e2) {
            c.a(e2);
            this.f2519a.a(10, e2);
        }
    }

    public String b() {
        return "send wrong message ack for message.";
    }
}
