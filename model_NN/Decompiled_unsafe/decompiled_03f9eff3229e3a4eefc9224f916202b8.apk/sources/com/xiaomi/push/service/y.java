package com.xiaomi.push.service;

import android.content.Context;
import com.xiaomi.push.service.ao;
import java.util.Locale;

public class y {

    /* renamed from: a  reason: collision with root package name */
    public final String f2554a;
    protected final String b;
    protected final String c;
    protected final String d;
    protected final String e;
    protected final String f;
    protected final int g;

    public y(String str, String str2, String str3, String str4, String str5, String str6, int i) {
        this.f2554a = str;
        this.b = str2;
        this.c = str3;
        this.d = str4;
        this.e = str5;
        this.f = str6;
        this.g = i;
    }

    private static boolean a(Context context) {
        return context.getPackageName().equals("com.xiaomi.xmsf");
    }

    public ao.b a(XMPushService xMPushService) {
        ao.b bVar = new ao.b(xMPushService);
        bVar.f2525a = xMPushService.getPackageName();
        bVar.b = this.f2554a;
        bVar.i = this.c;
        bVar.c = this.b;
        bVar.h = "5";
        bVar.d = "XMPUSH-PASS";
        bVar.e = false;
        bVar.f = "sdk_ver:8";
        bVar.g = String.format("%1$s:%2$s,%3$s:%4$s", "appid", a(xMPushService) ? "1000271" : this.d, "locale", Locale.getDefault().toString());
        bVar.k = xMPushService.d();
        return bVar;
    }
}
