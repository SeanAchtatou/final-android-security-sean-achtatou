package com.xiaomi.push.service;

import com.xiaomi.a.a.c.c;
import com.xiaomi.d.p;
import com.xiaomi.f.a.h;
import com.xiaomi.push.service.XMPushService;

final class ai extends XMPushService.e {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ XMPushService f2518a;
    final /* synthetic */ h b;
    final /* synthetic */ String c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    ai(int i, XMPushService xMPushService, h hVar, String str) {
        super(i);
        this.f2518a = xMPushService;
        this.b = hVar;
        this.c = str;
    }

    public void a() {
        try {
            h a2 = ad.e(this.f2518a, this.b);
            a2.m().a("absent_target_package", this.c);
            this.f2518a.b(a2);
        } catch (p e) {
            c.a(e);
            this.f2518a.a(10, e);
        }
    }

    public String b() {
        return "send app absent ack message for message.";
    }
}
