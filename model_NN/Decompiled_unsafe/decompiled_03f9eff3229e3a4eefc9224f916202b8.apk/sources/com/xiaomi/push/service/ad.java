package com.xiaomi.push.service;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.text.TextUtils;
import com.xiaomi.d.c.d;
import com.xiaomi.d.e.k;
import com.xiaomi.f.a.a;
import com.xiaomi.f.a.c;
import com.xiaomi.f.a.e;
import com.xiaomi.f.a.h;
import com.xiaomi.f.a.u;
import com.xiaomi.push.service.ao;
import java.io.IOException;
import java.util.List;
import org.apache.a.f;
import org.json.JSONException;

public class ad {
    private static void a(XMPushService xMPushService, h hVar, String str) {
        xMPushService.a(new ai(4, xMPushService, hVar, str));
    }

    private static void a(XMPushService xMPushService, h hVar, String str, String str2) {
        xMPushService.a(new aj(4, xMPushService, hVar, str, str2));
    }

    private static void a(XMPushService xMPushService, byte[] bArr, long j) {
        boolean z;
        Long valueOf = Long.valueOf(System.currentTimeMillis());
        h hVar = new h();
        try {
            u.a(hVar, bArr);
            if (!TextUtils.isEmpty(hVar.f)) {
                Intent intent = new Intent("com.xiaomi.mipush.RECEIVE_MESSAGE");
                intent.putExtra("mipush_payload", bArr);
                intent.putExtra("mrt", Long.toString(valueOf.longValue()));
                intent.setPackage(hVar.f);
                String a2 = ak.a(hVar);
                k.a(xMPushService, a2, j, true, System.currentTimeMillis());
                c m = hVar.m();
                if (m != null) {
                    m.a("mrt", Long.toString(valueOf.longValue()));
                }
                if (a.SendMessage == hVar.a() && aa.a(xMPushService).a(hVar.f) && !ak.b(hVar)) {
                    String str = "";
                    if (m != null) {
                        str = m.b();
                    }
                    com.xiaomi.a.a.c.c.a("Drop a message for unregistered, msgid=" + str);
                    a(xMPushService, hVar, hVar.f);
                } else if (a.SendMessage != hVar.a() || TextUtils.equals(xMPushService.getPackageName(), "com.xiaomi.xmsf") || TextUtils.equals(xMPushService.getPackageName(), hVar.f)) {
                    if (m != null) {
                        if (m.b() != null) {
                            com.xiaomi.a.a.c.c.a(String.format("receive a message, appid=%1$s, msgid= %2$s", hVar.h(), m.b()));
                        }
                    }
                    if (c(hVar) && a(xMPushService, a2)) {
                        c(xMPushService, hVar);
                    } else if (a(hVar) && !a(xMPushService, a2) && !b(hVar)) {
                        d(xMPushService, hVar);
                    } else if ((!ak.b(hVar) || !b(xMPushService, hVar.f)) && !a(xMPushService, intent)) {
                        xMPushService.a(new ae(4, xMPushService, hVar));
                    } else {
                        if (a.Registration == hVar.a()) {
                            String j2 = hVar.j();
                            SharedPreferences.Editor edit = xMPushService.getSharedPreferences("pref_registered_pkg_names", 0).edit();
                            edit.putString(j2, hVar.e);
                            edit.commit();
                        }
                        if (m == null || TextUtils.isEmpty(m.h()) || TextUtils.isEmpty(m.j()) || m.h == 1 || (!ak.a(m.s()) && ak.a(xMPushService, hVar.f))) {
                            xMPushService.sendBroadcast(intent, t.a(hVar.f));
                        } else {
                            String str2 = null;
                            if (m != null) {
                                if (m.j != null) {
                                    str2 = m.j.get("jobkey");
                                }
                                if (TextUtils.isEmpty(str2)) {
                                    str2 = m.b();
                                }
                                z = al.a(xMPushService, hVar.f, str2);
                            } else {
                                z = false;
                            }
                            if (z) {
                                com.xiaomi.a.a.c.c.a("drop a duplicate message, key=" + str2);
                            } else {
                                ak.a(xMPushService, hVar, bArr);
                                if (!ak.b(hVar)) {
                                    Intent intent2 = new Intent("com.xiaomi.mipush.MESSAGE_ARRIVED");
                                    intent2.putExtra("mipush_payload", bArr);
                                    intent2.setPackage(hVar.f);
                                    try {
                                        List<ResolveInfo> queryBroadcastReceivers = xMPushService.getPackageManager().queryBroadcastReceivers(intent2, 0);
                                        if (queryBroadcastReceivers != null && !queryBroadcastReceivers.isEmpty()) {
                                            xMPushService.sendBroadcast(intent2, t.a(hVar.f));
                                        }
                                    } catch (Exception e) {
                                        xMPushService.sendBroadcast(intent2, t.a(hVar.f));
                                    }
                                }
                            }
                            b(xMPushService, hVar);
                        }
                        if (hVar.a() == a.UnRegistration && !"com.xiaomi.xmsf".equals(xMPushService.getPackageName())) {
                            xMPushService.stopSelf();
                        }
                    }
                } else {
                    com.xiaomi.a.a.c.c.a("Receive a message with wrong package name, expect " + xMPushService.getPackageName() + ", received " + hVar.f);
                    a(xMPushService, hVar, "unmatched_package", "package should be " + xMPushService.getPackageName() + ", but got " + hVar.f);
                }
            } else {
                com.xiaomi.a.a.c.c.a("receive a mipush message without package name");
            }
        } catch (f e2) {
            com.xiaomi.a.a.c.c.a(e2);
        }
    }

    private static boolean a(Context context, Intent intent) {
        try {
            List<ResolveInfo> queryBroadcastReceivers = context.getPackageManager().queryBroadcastReceivers(intent, 32);
            return queryBroadcastReceivers != null && !queryBroadcastReceivers.isEmpty();
        } catch (Exception e) {
            return true;
        }
    }

    private static boolean a(Context context, String str) {
        Intent intent = new Intent("com.xiaomi.mipush.miui.CLICK_MESSAGE");
        intent.setPackage(str);
        Intent intent2 = new Intent("com.xiaomi.mipush.miui.RECEIVE_MESSAGE");
        intent2.setPackage(str);
        PackageManager packageManager = context.getPackageManager();
        try {
            return !packageManager.queryBroadcastReceivers(intent2, 32).isEmpty() || !packageManager.queryIntentServices(intent, 32).isEmpty();
        } catch (Exception e) {
            com.xiaomi.a.a.c.c.a(e);
            return false;
        }
    }

    private static boolean a(h hVar) {
        return "com.xiaomi.xmsf".equals(hVar.f) && hVar.m() != null && hVar.m().s() != null && hVar.m().s().containsKey("miui_package_name");
    }

    private static void b(XMPushService xMPushService, h hVar) {
        xMPushService.a(new af(4, xMPushService, hVar));
    }

    private static boolean b(Context context, String str) {
        PackageInfo packageInfo;
        try {
            packageInfo = context.getPackageManager().getPackageInfo(str, 0);
        } catch (PackageManager.NameNotFoundException e) {
            packageInfo = null;
        }
        return packageInfo != null;
    }

    private static boolean b(h hVar) {
        return hVar.m().s().containsKey("notify_effect");
    }

    private static void c(XMPushService xMPushService, h hVar) {
        xMPushService.a(new ag(4, xMPushService, hVar));
    }

    private static boolean c(h hVar) {
        if (hVar.m() == null || hVar.m().s() == null) {
            return false;
        }
        return "1".equals(hVar.m().s().get("obslete_ads_message"));
    }

    private static void d(XMPushService xMPushService, h hVar) {
        xMPushService.a(new ah(4, xMPushService, hVar));
    }

    /* access modifiers changed from: private */
    public static h e(XMPushService xMPushService, h hVar) {
        e eVar = new e();
        eVar.b(hVar.h());
        c m = hVar.m();
        if (m != null) {
            eVar.a(m.b());
            eVar.a(m.d());
            if (!TextUtils.isEmpty(m.f())) {
                eVar.c(m.f());
            }
        }
        h a2 = xMPushService.a(hVar.j(), hVar.h(), eVar, a.AckMessage);
        c a3 = hVar.m().a();
        a3.a("mat", Long.toString(System.currentTimeMillis()));
        a2.a(a3);
        return a2;
    }

    public void a(Context context, ao.b bVar, boolean z, int i, String str) {
        y a2;
        if (!z && (a2 = z.a(context)) != null && "token-expired".equals(str)) {
            try {
                z.a(context, a2.d, a2.e, a2.f);
            } catch (IOException e) {
                com.xiaomi.a.a.c.c.a(e);
            } catch (JSONException e2) {
                com.xiaomi.a.a.c.c.a(e2);
            }
        }
    }

    public void a(XMPushService xMPushService, d dVar, ao.b bVar) {
        if (dVar instanceof com.xiaomi.d.c.c) {
            com.xiaomi.d.c.c cVar = (com.xiaomi.d.c.c) dVar;
            com.xiaomi.d.c.a p = cVar.p("s");
            if (p != null) {
                try {
                    a(xMPushService, e.b(e.a(bVar.i, cVar.k()), p.c()), (long) k.a(dVar.a()));
                } catch (IllegalArgumentException e) {
                    com.xiaomi.a.a.c.c.a(e);
                }
            }
        } else {
            com.xiaomi.a.a.c.c.a("not a mipush message");
        }
    }
}
