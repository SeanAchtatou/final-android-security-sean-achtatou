package com.xiaomi.push.service;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcelable;
import android.text.TextUtils;
import com.xiaomi.d.c.f;
import com.xiaomi.d.p;
import com.xiaomi.f.a.u;
import com.xiaomi.push.service.ao;
import java.nio.ByteBuffer;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class XMPushService extends Service implements com.xiaomi.d.d {

    /* renamed from: a  reason: collision with root package name */
    public static int f2497a = 1;
    /* access modifiers changed from: private */
    public com.xiaomi.d.b b;
    private f c;
    private long d = 0;
    /* access modifiers changed from: private */
    public com.xiaomi.d.l e;
    /* access modifiers changed from: private */
    public com.xiaomi.d.a f;
    private t g;
    /* access modifiers changed from: private */
    public a h = null;
    private com.xiaomi.push.service.a.a i = null;
    /* access modifiers changed from: private */
    public v j = null;
    private com.xiaomi.d.f k = new j(this);

    class a extends e {

        /* renamed from: a  reason: collision with root package name */
        ao.b f2498a = null;

        public a(ao.b bVar) {
            super(9);
            this.f2498a = bVar;
        }

        public void a() {
            try {
                if (!XMPushService.this.e()) {
                    com.xiaomi.a.a.c.c.d("trying bind while the connection is not created, quit!");
                    return;
                }
                ao.b b2 = ao.a().b(this.f2498a.h, this.f2498a.b);
                if (b2 == null) {
                    com.xiaomi.a.a.c.c.a("ignore bind because the channel " + this.f2498a.h + " is removed ");
                } else if (b2.m == ao.c.unbind) {
                    b2.a(ao.c.binding, 0, 0, null, null);
                    XMPushService.this.f.a(b2);
                    com.xiaomi.e.g.a(XMPushService.this, b2);
                } else {
                    com.xiaomi.a.a.c.c.a("trying duplicate bind, ingore! " + b2.m);
                }
            } catch (p e) {
                com.xiaomi.a.a.c.c.a(e);
                XMPushService.this.a(10, e);
            }
        }

        public String b() {
            return "bind the client. " + this.f2498a.h + ", " + this.f2498a.b;
        }
    }

    static class b extends e {

        /* renamed from: a  reason: collision with root package name */
        private final ao.b f2499a;

        public b(ao.b bVar) {
            super(12);
            this.f2499a = bVar;
        }

        public void a() {
            this.f2499a.a(ao.c.unbind, 1, 21, null, null);
        }

        public String b() {
            return "bind time out. chid=" + this.f2499a.h;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof b)) {
                return false;
            }
            return TextUtils.equals(((b) obj).f2499a.h, this.f2499a.h);
        }

        public int hashCode() {
            return this.f2499a.h.hashCode();
        }
    }

    public class c extends e {
        c() {
            super(1);
        }

        public void a() {
            if (XMPushService.this.a()) {
                XMPushService.this.k();
            } else {
                com.xiaomi.a.a.c.c.a("should not connect. quit the job.");
            }
        }

        public String b() {
            return "do reconnect..";
        }
    }

    public class d extends e {

        /* renamed from: a  reason: collision with root package name */
        public int f2501a;
        public Exception b;

        d(int i, Exception exc) {
            super(2);
            this.f2501a = i;
            this.b = exc;
        }

        public void a() {
            XMPushService.this.a(this.f2501a, this.b);
        }

        public String b() {
            return "disconnect the connection.";
        }
    }

    public static abstract class e {
        protected int d;

        public e(int i) {
            this.d = i;
        }

        public abstract void a();

        public abstract String b();

        public void c() {
            if (!(this.d == 4 || this.d == 8)) {
                com.xiaomi.a.a.c.c.a("JOB: " + b());
            }
            a();
        }
    }

    class f extends e {
        public f() {
            super(5);
        }

        public void a() {
            XMPushService.this.j.quit();
        }

        public String b() {
            return "ask the job queue to quit";
        }
    }

    public class g extends Binder {
        public g() {
        }
    }

    class h extends e {
        private com.xiaomi.d.c.d b = null;

        public h(com.xiaomi.d.c.d dVar) {
            super(8);
            this.b = dVar;
        }

        public void a() {
            XMPushService.this.h.a(this.b);
        }

        public String b() {
            return "receive a message.";
        }
    }

    class i extends e {
        public i() {
            super(4);
        }

        public void a() {
            if (XMPushService.this.e()) {
                try {
                    com.xiaomi.e.g.a();
                    XMPushService.this.f.m();
                } catch (p e) {
                    com.xiaomi.a.a.c.c.a(e);
                    XMPushService.this.a(10, e);
                }
            }
        }

        public String b() {
            return "send ping..";
        }
    }

    class j extends e {

        /* renamed from: a  reason: collision with root package name */
        ao.b f2506a = null;

        public j(ao.b bVar) {
            super(4);
            this.f2506a = bVar;
        }

        public void a() {
            try {
                this.f2506a.a(ao.c.unbind, 1, 16, null, null);
                XMPushService.this.f.a(this.f2506a.h, this.f2506a.b);
                this.f2506a.a(ao.c.binding, 1, 16, null, null);
                XMPushService.this.f.a(this.f2506a);
            } catch (p e) {
                com.xiaomi.a.a.c.c.a(e);
                XMPushService.this.a(10, e);
            }
        }

        public String b() {
            return "bind the client. " + this.f2506a.h + ", " + this.f2506a.b;
        }
    }

    class k extends e {
        k() {
            super(3);
        }

        public void a() {
            XMPushService.this.a(11, (Exception) null);
            if (XMPushService.this.a()) {
                XMPushService.this.k();
            }
        }

        public String b() {
            return "reset the connection.";
        }
    }

    class l extends e {

        /* renamed from: a  reason: collision with root package name */
        ao.b f2508a = null;
        int b;
        String c;
        String e;

        public l(ao.b bVar, int i, String str, String str2) {
            super(9);
            this.f2508a = bVar;
            this.b = i;
            this.c = str;
            this.e = str2;
        }

        public void a() {
            if (!(this.f2508a.m == ao.c.unbind || XMPushService.this.f == null)) {
                try {
                    XMPushService.this.f.a(this.f2508a.h, this.f2508a.b);
                } catch (p e2) {
                    com.xiaomi.a.a.c.c.a(e2);
                    XMPushService.this.a(10, e2);
                }
            }
            this.f2508a.a(ao.c.unbind, this.b, 0, this.e, this.c);
        }

        public String b() {
            return "unbind the channel. " + this.f2508a.h + ", " + this.f2508a.b;
        }
    }

    static {
        com.xiaomi.network.f.a("app.chat.xiaomi.net", "42.62.94.2");
        com.xiaomi.network.f.a("app.chat.xiaomi.net", "114.54.23.2");
        com.xiaomi.network.f.a("app.chat.xiaomi.net", "111.13.142.2");
        com.xiaomi.network.f.a("app.chat.xiaomi.net", "111.206.200.2");
        com.xiaomi.network.f.a("app.chat.xiaomi.net", "app.chat.xiaomi.net");
        com.xiaomi.d.l.f2364a = true;
    }

    private com.xiaomi.d.c.c a(com.xiaomi.d.c.c cVar, String str) {
        byte[] a2 = e.a(str, cVar.k());
        com.xiaomi.d.c.c cVar2 = new com.xiaomi.d.c.c();
        cVar2.n(cVar.n());
        cVar2.m(cVar.m());
        cVar2.k(cVar.k());
        cVar2.l(cVar.l());
        cVar2.b(true);
        String a3 = e.a(a2, com.xiaomi.d.e.g.c(cVar.a()));
        com.xiaomi.d.c.a aVar = new com.xiaomi.d.c.a("s", null, null, null);
        aVar.b(a3);
        cVar2.a(aVar);
        return cVar2;
    }

    private com.xiaomi.d.c.d a(com.xiaomi.d.c.d dVar, String str, String str2, boolean z) {
        ao a2 = ao.a();
        List<String> b2 = a2.b(str);
        if (b2.isEmpty()) {
            com.xiaomi.a.a.c.c.a("open channel should be called first before sending a packet, pkg=" + str);
        } else {
            dVar.o(str);
            String l2 = dVar.l();
            if (TextUtils.isEmpty(l2)) {
                l2 = b2.get(0);
                dVar.l(l2);
            }
            ao.b b3 = a2.b(l2, dVar.n());
            if (!e()) {
                com.xiaomi.a.a.c.c.a("drop a packet as the channel is not connected, chid=" + l2);
            } else if (b3 == null || b3.m != ao.c.binded) {
                com.xiaomi.a.a.c.c.a("drop a packet as the channel is not opened, chid=" + l2);
            } else if (TextUtils.equals(str2, b3.j)) {
                return (!(dVar instanceof com.xiaomi.d.c.c) || !z) ? dVar : a((com.xiaomi.d.c.c) dVar, b3.i);
            } else {
                com.xiaomi.a.a.c.c.a("invalid session. " + str2);
            }
        }
        return null;
    }

    private String a(String str) {
        return "<iq to='" + str + "' id='0' chid='0' type='get'><ping xmlns='urn:xmpp:ping'>%1$s%2$s</ping></iq>";
    }

    private void a(String str, int i2) {
        Collection<ao.b> c2 = ao.a().c(str);
        if (c2 != null) {
            for (ao.b next : c2) {
                if (next != null) {
                    a(new l(next, i2, null, null));
                }
            }
        }
        ao.a().a(str);
    }

    private boolean a(String str, Intent intent) {
        ao.b b2 = ao.a().b(str, intent.getStringExtra(aq.p));
        boolean z = false;
        if (b2 == null || str == null) {
            return false;
        }
        String stringExtra = intent.getStringExtra(aq.B);
        String stringExtra2 = intent.getStringExtra(aq.u);
        if (!TextUtils.isEmpty(b2.j) && !TextUtils.equals(stringExtra, b2.j)) {
            com.xiaomi.a.a.c.c.a("session changed. old session=" + b2.j + ", new session=" + stringExtra);
            z = true;
        }
        if (stringExtra2.equals(b2.i)) {
            return z;
        }
        com.xiaomi.a.a.c.c.a("security changed. ");
        return true;
    }

    private ao.b b(String str, Intent intent) {
        ao.b b2 = ao.a().b(str, intent.getStringExtra(aq.p));
        if (b2 == null) {
            b2 = new ao.b(this);
        }
        b2.h = intent.getStringExtra(aq.q);
        b2.b = intent.getStringExtra(aq.p);
        b2.c = intent.getStringExtra(aq.s);
        b2.f2525a = intent.getStringExtra(aq.y);
        b2.f = intent.getStringExtra(aq.w);
        b2.g = intent.getStringExtra(aq.x);
        b2.e = intent.getBooleanExtra(aq.v, false);
        b2.i = intent.getStringExtra(aq.u);
        b2.j = intent.getStringExtra(aq.B);
        b2.d = intent.getStringExtra(aq.t);
        b2.k = this.g;
        b2.l = getApplicationContext();
        ao.a().a(b2);
        return b2;
    }

    /* access modifiers changed from: private */
    public void i() {
        if (z.a(getApplicationContext()) != null) {
            ao.b a2 = z.a(getApplicationContext()).a(this);
            a(a2);
            ao.a().a(a2);
            if (com.xiaomi.a.a.e.d.d(getApplicationContext())) {
                a(true);
            }
        }
    }

    /* access modifiers changed from: private */
    public void j() {
        if (!a()) {
            this.i.a();
        } else if (!this.i.b()) {
            this.i.a(true);
        }
    }

    /* access modifiers changed from: private */
    public void k() {
        if (this.f != null && this.f.g()) {
            com.xiaomi.a.a.c.c.d("try to connect while connecting.");
        } else if (this.f == null || !this.f.h()) {
            this.b.b(com.xiaomi.a.a.e.d.f(this));
            l();
            if (this.f == null) {
                ao.a().a(this);
                sendBroadcast(new Intent("miui.intent.action.NETWORK_BLOCKED"));
                return;
            }
            sendBroadcast(new Intent("miui.intent.action.NETWORK_CONNECTED"));
        } else {
            com.xiaomi.a.a.c.c.d("try to connect while is connected.");
        }
    }

    private void l() {
        try {
            this.e.a(this.k, new s(this));
            this.e.s();
            this.f = this.e;
        } catch (p e2) {
            com.xiaomi.a.a.c.c.a("fail to create xmpp connection", e2);
            this.e.a(new com.xiaomi.d.c.f(f.b.unavailable), 3, e2);
        }
    }

    public com.xiaomi.d.c.c a(com.xiaomi.f.a.h hVar) {
        try {
            com.xiaomi.d.c.c cVar = new com.xiaomi.d.c.c();
            cVar.l("5");
            cVar.m("xiaomi.com");
            cVar.n(z.a(this).f2554a);
            cVar.b(true);
            cVar.f("push");
            cVar.o(hVar.f);
            String str = z.a(this).f2554a;
            hVar.g.b = str.substring(0, str.indexOf("@"));
            hVar.g.d = str.substring(str.indexOf("/") + 1);
            String valueOf = String.valueOf(com.xiaomi.a.a.g.a.a(e.a(e.a(z.a(this).c, cVar.k()), u.a(hVar))));
            com.xiaomi.d.c.a aVar = new com.xiaomi.d.c.a("s", null, null, null);
            aVar.b(valueOf);
            cVar.a(aVar);
            com.xiaomi.a.a.c.c.a("try send mi push message. packagename:" + hVar.f + " action:" + hVar.f2423a);
            return cVar;
        } catch (NullPointerException e2) {
            com.xiaomi.a.a.c.c.a(e2);
            return null;
        }
    }

    public com.xiaomi.d.c.c a(byte[] bArr) {
        com.xiaomi.f.a.h hVar = new com.xiaomi.f.a.h();
        try {
            u.a(hVar, bArr);
            return a(hVar);
        } catch (org.apache.a.f e2) {
            com.xiaomi.a.a.c.c.a(e2);
            return null;
        }
    }

    public com.xiaomi.d.l a(com.xiaomi.d.b bVar) {
        return new com.xiaomi.d.l(this, bVar);
    }

    public com.xiaomi.f.a.h a(String str, String str2) {
        com.xiaomi.f.a.i iVar = new com.xiaomi.f.a.i();
        iVar.b(str2);
        iVar.c("package uninstalled");
        iVar.a(com.xiaomi.d.c.d.j());
        iVar.a(false);
        return a(str, str2, iVar, com.xiaomi.f.a.a.Notification);
    }

    public <T extends org.apache.a.b<T, ?>> com.xiaomi.f.a.h a(String str, String str2, T t, com.xiaomi.f.a.a aVar) {
        byte[] a2 = u.a(t);
        com.xiaomi.f.a.h hVar = new com.xiaomi.f.a.h();
        com.xiaomi.f.a.d dVar = new com.xiaomi.f.a.d();
        dVar.f2415a = 5;
        dVar.b = "fakeid";
        hVar.a(dVar);
        hVar.a(ByteBuffer.wrap(a2));
        hVar.a(aVar);
        hVar.c(true);
        hVar.b(str);
        hVar.a(false);
        hVar.a(str2);
        return hVar;
    }

    public void a(int i2) {
        this.j.a(i2);
    }

    public void a(int i2, Exception exc) {
        com.xiaomi.a.a.c.c.a("disconnect " + hashCode() + ", " + (this.f == null ? null : Integer.valueOf(this.f.hashCode())));
        if (this.f != null) {
            this.f.a(new com.xiaomi.d.c.f(f.b.unavailable), i2, exc);
            this.f = null;
        }
        a(7);
        a(4);
        ao.a().a(this, i2);
    }

    public void a(com.xiaomi.d.a aVar) {
        this.c.a();
        Iterator<ao.b> it = ao.a().b().iterator();
        while (it.hasNext()) {
            a(new a(it.next()));
        }
    }

    public void a(com.xiaomi.d.a aVar, int i2, Exception exc) {
        a(false);
    }

    public void a(com.xiaomi.d.a aVar, Exception exc) {
        a(false);
    }

    public void a(com.xiaomi.d.c.d dVar) {
        if (this.f != null) {
            this.f.a(dVar);
            return;
        }
        throw new p("try send msg while connection is null.");
    }

    public void a(e eVar) {
        a(eVar, 0);
    }

    public void a(e eVar, long j2) {
        this.j.a(eVar, j2);
    }

    public void a(ao.b bVar) {
        bVar.a(new q(this));
    }

    public void a(String str, String str2, int i2, String str3, String str4) {
        ao.b b2 = ao.a().b(str, str2);
        if (b2 != null) {
            a(new l(b2, i2, str4, str3));
        }
        ao.a().a(str, str2);
    }

    public void a(String str, byte[] bArr) {
        if (this.f != null) {
            com.xiaomi.d.c.c a2 = a(bArr);
            if (a2 != null) {
                this.f.a(a2);
            } else {
                ac.a(this, str, bArr, 70000003, "not a valid message");
            }
        } else {
            throw new p("try send msg while connection is null.");
        }
    }

    public void a(boolean z) {
        this.c.a(z);
    }

    public void a(byte[] bArr, String str) {
        if (bArr == null) {
            ac.a(this, str, bArr, 70000003, "null payload");
            com.xiaomi.a.a.c.c.a("register request without payload");
            return;
        }
        com.xiaomi.f.a.h hVar = new com.xiaomi.f.a.h();
        try {
            u.a(hVar, bArr);
            if (hVar.f2423a == com.xiaomi.f.a.a.Registration) {
                com.xiaomi.f.a.j jVar = new com.xiaomi.f.a.j();
                try {
                    u.a(jVar, hVar.f());
                    ac.a(hVar.j(), bArr);
                    a(new ab(this, hVar.j(), jVar.d(), jVar.h(), bArr));
                } catch (org.apache.a.f e2) {
                    com.xiaomi.a.a.c.c.a(e2);
                    ac.a(this, str, bArr, 70000003, " data action error.");
                }
            } else {
                ac.a(this, str, bArr, 70000003, " registration action required.");
                com.xiaomi.a.a.c.c.a("register request with invalid payload");
            }
        } catch (org.apache.a.f e3) {
            com.xiaomi.a.a.c.c.a(e3);
            ac.a(this, str, bArr, 70000003, " data container error.");
        }
    }

    public void a(com.xiaomi.d.c.d[] dVarArr) {
        if (this.f != null) {
            this.f.a(dVarArr);
            return;
        }
        throw new p("try send msg while connection is null.");
    }

    public boolean a() {
        return com.xiaomi.a.a.e.d.d(this) && ao.a().c() > 0 && !b();
    }

    public void b(com.xiaomi.d.a aVar) {
        com.xiaomi.a.a.c.c.c("begin to connect...");
    }

    public void b(com.xiaomi.f.a.h hVar) {
        if (this.f != null) {
            com.xiaomi.d.c.c a2 = a(hVar);
            if (a2 != null) {
                this.f.a(a2);
                return;
            }
            return;
        }
        throw new p("try send msg while connection is null.");
    }

    public void b(e eVar) {
        this.j.a(eVar.d, eVar);
    }

    public void b(ao.b bVar) {
        if (bVar != null) {
            long a2 = bVar.a();
            com.xiaomi.a.a.c.c.a("schedule rebind job in " + (a2 / 1000));
            a(new a(bVar), a2);
        }
    }

    public boolean b() {
        try {
            Class<?> cls = Class.forName("miui.os.Build");
            return cls.getField("IS_CM_CUSTOMIZATION_TEST").getBoolean(null) || cls.getField("IS_CU_CUSTOMIZATION_TEST").getBoolean(null);
        } catch (Throwable th) {
            return false;
        }
    }

    public boolean b(int i2) {
        return this.j.b(i2);
    }

    public t c() {
        return new t();
    }

    public t d() {
        return this.g;
    }

    public boolean e() {
        return this.f != null && this.f.h();
    }

    public boolean f() {
        return this.f != null && this.f.g();
    }

    public com.xiaomi.d.a g() {
        return this.f;
    }

    public void h() {
        a(new k(this, 10), 120000);
    }

    public IBinder onBind(Intent intent) {
        return new g();
    }

    public void onCreate() {
        super.onCreate();
        com.xiaomi.d.e.h.a(this);
        y a2 = z.a(this);
        if (a2 != null) {
            com.xiaomi.a.a.d.a.a(a2.g);
        }
        ar.a(this);
        this.b = new l(this, null, 5222, "xiaomi.com", null);
        this.b.a(true);
        this.e = a(this.b);
        this.e.b(a("xiaomi.com"));
        new com.xiaomi.network.b("mibind.chat.gslb.mi-idc.com");
        this.g = c();
        try {
            if (TextUtils.equals((String) Class.forName("android.os.SystemProperties").getMethod("get", String.class).invoke(null, "sys.boot_completed"), "1")) {
                this.g.a(this);
            }
        } catch (Exception e2) {
            com.xiaomi.a.a.c.c.a(e2);
        }
        this.i = new com.xiaomi.push.service.a.a(this);
        this.e.a(this);
        this.h = new a(this);
        this.c = new f(this);
        new u().a();
        this.j = new v("Connection Controller Thread");
        this.j.start();
        a(new m(this, 11));
        ao a3 = ao.a();
        a3.e();
        a3.a(new n(this));
    }

    public void onDestroy() {
        this.j.a();
        a(new r(this, 2));
        a(new f());
        ao.a().e();
        ao.a().a(this, 15);
        ao.a().d();
        this.e.b(this);
        h.a().b();
        this.i.a();
        super.onDestroy();
        com.xiaomi.a.a.c.c.a("Service destroyed");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.xiaomi.push.service.XMPushService.a(com.xiaomi.d.c.d, java.lang.String, java.lang.String, boolean):com.xiaomi.d.c.d
     arg types: [com.xiaomi.d.c.b, java.lang.String, java.lang.String, int]
     candidates:
      com.xiaomi.push.service.XMPushService.a(java.lang.String, java.lang.String, org.apache.a.b, com.xiaomi.f.a.a):com.xiaomi.f.a.h
      com.xiaomi.push.service.XMPushService.a(com.xiaomi.d.c.d, java.lang.String, java.lang.String, boolean):com.xiaomi.d.c.d */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.xiaomi.push.service.XMPushService.a(com.xiaomi.d.c.d, java.lang.String, java.lang.String, boolean):com.xiaomi.d.c.d
     arg types: [com.xiaomi.d.c.f, java.lang.String, java.lang.String, int]
     candidates:
      com.xiaomi.push.service.XMPushService.a(java.lang.String, java.lang.String, org.apache.a.b, com.xiaomi.f.a.a):com.xiaomi.f.a.h
      com.xiaomi.push.service.XMPushService.a(com.xiaomi.d.c.d, java.lang.String, java.lang.String, boolean):com.xiaomi.d.c.d */
    public void onStart(Intent intent, int i2) {
        String b2;
        int i3;
        NetworkInfo networkInfo;
        ao.b bVar = null;
        boolean z = true;
        int i4 = 0;
        if (intent == null) {
            com.xiaomi.a.a.c.c.d("onStart() with intent NULL");
        } else {
            com.xiaomi.a.a.c.c.c(String.format("onStart() with intent.Action = %s, chid = %s", intent.getAction(), intent.getStringExtra(aq.q)));
        }
        ao a2 = ao.a();
        if (intent != null && intent.getAction() != null) {
            if (aq.d.equalsIgnoreCase(intent.getAction()) || aq.j.equalsIgnoreCase(intent.getAction())) {
                String stringExtra = intent.getStringExtra(aq.q);
                if (TextUtils.isEmpty(intent.getStringExtra(aq.u))) {
                    com.xiaomi.a.a.c.c.a("security is empty. ignore.");
                } else if (stringExtra != null) {
                    boolean a3 = a(stringExtra, intent);
                    ao.b b3 = b(stringExtra, intent);
                    if (!com.xiaomi.a.a.e.d.d(this)) {
                        this.g.a(this, b3, false, 2, null);
                    } else if (!e()) {
                        a(true);
                    } else if (b3.m == ao.c.unbind) {
                        a(new a(b3));
                    } else if (a3) {
                        a(new j(b3));
                    } else if (b3.m == ao.c.binding) {
                        com.xiaomi.a.a.c.c.a(String.format("the client is binding. %1$s %2$s.", b3.h, b3.b));
                    } else if (b3.m == ao.c.binded) {
                        this.g.a(this, b3, true, 0, null);
                    }
                } else {
                    com.xiaomi.a.a.c.c.d("channel id is empty, do nothing!");
                }
            } else if (aq.i.equalsIgnoreCase(intent.getAction())) {
                String stringExtra2 = intent.getStringExtra(aq.y);
                String stringExtra3 = intent.getStringExtra(aq.q);
                String stringExtra4 = intent.getStringExtra(aq.p);
                if (TextUtils.isEmpty(stringExtra3)) {
                    for (String a4 : a2.b(stringExtra2)) {
                        a(a4, 2);
                    }
                } else if (TextUtils.isEmpty(stringExtra4)) {
                    a(stringExtra3, 2);
                } else {
                    a(stringExtra3, stringExtra4, 2, null, null);
                }
            } else if (aq.e.equalsIgnoreCase(intent.getAction())) {
                com.xiaomi.d.c.d a5 = a(new com.xiaomi.d.c.c(intent.getBundleExtra("ext_packet")), intent.getStringExtra(aq.y), intent.getStringExtra(aq.B), intent.getBooleanExtra("ext_encrypt", true));
                if (a5 != null) {
                    a(new g(this, a5));
                }
            } else if (aq.g.equalsIgnoreCase(intent.getAction())) {
                String stringExtra5 = intent.getStringExtra(aq.y);
                String stringExtra6 = intent.getStringExtra(aq.B);
                Parcelable[] parcelableArrayExtra = intent.getParcelableArrayExtra("ext_packets");
                com.xiaomi.d.c.c[] cVarArr = new com.xiaomi.d.c.c[parcelableArrayExtra.length];
                boolean booleanExtra = intent.getBooleanExtra("ext_encrypt", true);
                while (i4 < parcelableArrayExtra.length) {
                    cVarArr[i4] = new com.xiaomi.d.c.c((Bundle) parcelableArrayExtra[i4]);
                    cVarArr[i4] = (com.xiaomi.d.c.c) a(cVarArr[i4], stringExtra5, stringExtra6, booleanExtra);
                    if (cVarArr[i4] != null) {
                        i4++;
                    } else {
                        return;
                    }
                }
                a(new b(this, cVarArr));
            } else if (aq.f.equalsIgnoreCase(intent.getAction())) {
                String stringExtra7 = intent.getStringExtra(aq.y);
                String stringExtra8 = intent.getStringExtra(aq.B);
                com.xiaomi.d.c.b bVar2 = new com.xiaomi.d.c.b(intent.getBundleExtra("ext_packet"));
                if (a((com.xiaomi.d.c.d) bVar2, stringExtra7, stringExtra8, false) != null) {
                    a(new g(this, bVar2));
                }
            } else if (aq.h.equalsIgnoreCase(intent.getAction())) {
                String stringExtra9 = intent.getStringExtra(aq.y);
                String stringExtra10 = intent.getStringExtra(aq.B);
                com.xiaomi.d.c.f fVar = new com.xiaomi.d.c.f(intent.getBundleExtra("ext_packet"));
                if (a((com.xiaomi.d.c.d) fVar, stringExtra9, stringExtra10, false) != null) {
                    a(new g(this, fVar));
                }
            } else if ("com.xiaomi.push.timer".equalsIgnoreCase(intent.getAction()) || "com.xiaomi.push.check_alive".equalsIgnoreCase(intent.getAction())) {
                if ("com.xiaomi.push.timer".equalsIgnoreCase(intent.getAction())) {
                    com.xiaomi.a.a.c.c.a("Service called on timer");
                } else if (System.currentTimeMillis() - this.d >= StatisticConfig.MIN_UPLOAD_INTERVAL) {
                    this.d = System.currentTimeMillis();
                    com.xiaomi.a.a.c.c.a("Service called on check alive.");
                } else {
                    return;
                }
                if (this.j.b()) {
                    com.xiaomi.a.a.c.c.d("ERROR, the job controller is blocked.");
                    ao.a().a(this, 14);
                    stopSelf();
                } else if (!e()) {
                    if ("com.xiaomi.push.timer".equalsIgnoreCase(intent.getAction())) {
                        a(false);
                    } else {
                        a(true);
                    }
                } else if (this.f.p()) {
                    a(new i());
                } else {
                    a(new d(17, null));
                }
            } else if ("com.xiaomi.push.network_status_changed".equalsIgnoreCase(intent.getAction())) {
                try {
                    networkInfo = ((ConnectivityManager) getSystemService("connectivity")).getActiveNetworkInfo();
                } catch (Exception e2) {
                    com.xiaomi.a.a.c.c.a(e2);
                    networkInfo = null;
                }
                if (networkInfo != null) {
                    com.xiaomi.a.a.c.c.a("network changed, " + networkInfo.toString());
                } else {
                    com.xiaomi.a.a.c.c.a("network changed, no active network");
                }
                this.e.q();
                if (com.xiaomi.a.a.e.d.d(this)) {
                    if (!e() && !f()) {
                        this.j.a(1);
                        a(new c());
                    }
                    com.xiaomi.push.a.b.a(this).a();
                } else {
                    a(new d(2, null));
                }
                j();
            } else if (aq.k.equals(intent.getAction())) {
                String stringExtra11 = intent.getStringExtra(aq.q);
                if (stringExtra11 != null) {
                    b(stringExtra11, intent);
                }
                a(new k());
            } else if (aq.l.equals(intent.getAction())) {
                String stringExtra12 = intent.getStringExtra(aq.y);
                List<String> b4 = a2.b(stringExtra12);
                if (b4.isEmpty()) {
                    com.xiaomi.a.a.c.c.a("open channel should be called first before update info, pkg=" + stringExtra12);
                    return;
                }
                String stringExtra13 = intent.getStringExtra(aq.q);
                String stringExtra14 = intent.getStringExtra(aq.p);
                if (TextUtils.isEmpty(stringExtra13)) {
                    stringExtra13 = b4.get(0);
                }
                if (TextUtils.isEmpty(stringExtra14)) {
                    Collection<ao.b> c2 = a2.c(stringExtra13);
                    if (c2 != null && !c2.isEmpty()) {
                        bVar = c2.iterator().next();
                    }
                } else {
                    bVar = a2.b(stringExtra13, stringExtra14);
                }
                if (bVar != null) {
                    if (intent.hasExtra(aq.w)) {
                        bVar.f = intent.getStringExtra(aq.w);
                    }
                    if (intent.hasExtra(aq.x)) {
                        bVar.g = intent.getStringExtra(aq.x);
                    }
                }
            } else if ("com.xiaomi.mipush.REGISTER_APP".equals(intent.getAction())) {
                if (!c.a(getApplicationContext()).a() || c.a(getApplicationContext()).b() != 0) {
                    byte[] byteArrayExtra = intent.getByteArrayExtra("mipush_payload");
                    String stringExtra15 = intent.getStringExtra("mipush_app_package");
                    boolean booleanExtra2 = intent.getBooleanExtra("mipush_env_chanage", false);
                    int intExtra = intent.getIntExtra("mipush_env_type", 1);
                    aa.a(this).c(stringExtra15);
                    if (!booleanExtra2 || "com.xiaomi.xmsf".equals(getPackageName())) {
                        a(byteArrayExtra, stringExtra15);
                    } else {
                        a(new o(this, 14, intExtra, byteArrayExtra, stringExtra15));
                    }
                } else {
                    com.xiaomi.a.a.c.c.a("register without being provisioned. " + intent.getStringExtra("mipush_app_package"));
                }
            } else if ("com.xiaomi.mipush.SEND_MESSAGE".equals(intent.getAction()) || "com.xiaomi.mipush.UNREGISTER_APP".equals(intent.getAction())) {
                String stringExtra16 = intent.getStringExtra("mipush_app_package");
                byte[] byteArrayExtra2 = intent.getByteArrayExtra("mipush_payload");
                boolean booleanExtra3 = intent.getBooleanExtra("com.xiaomi.mipush.MESSAGE_CACHE", true);
                Collection<ao.b> c3 = ao.a().c("5");
                if ("com.xiaomi.mipush.UNREGISTER_APP".equals(intent.getAction())) {
                    aa.a(this).b(stringExtra16);
                }
                if (c3.isEmpty()) {
                    if (booleanExtra3) {
                        ac.b(stringExtra16, byteArrayExtra2);
                    }
                } else if (c3.iterator().next().m == ao.c.binded) {
                    a(new p(this, 4, stringExtra16, byteArrayExtra2));
                } else if (booleanExtra3) {
                    ac.b(stringExtra16, byteArrayExtra2);
                }
            } else if (d.f2532a.equals(intent.getAction())) {
                String stringExtra17 = intent.getStringExtra("uninstall_pkg_name");
                if (stringExtra17 != null && !TextUtils.isEmpty(stringExtra17.trim())) {
                    try {
                        getPackageManager().getPackageInfo(stringExtra17, 256);
                        z = false;
                    } catch (PackageManager.NameNotFoundException e3) {
                    }
                    if (!"com.xiaomi.channel".equals(stringExtra17) || ao.a().c("1").isEmpty() || !z) {
                        SharedPreferences sharedPreferences = getSharedPreferences("pref_registered_pkg_names", 0);
                        String string = sharedPreferences.getString(stringExtra17, null);
                        if (!TextUtils.isEmpty(string) && z) {
                            SharedPreferences.Editor edit = sharedPreferences.edit();
                            edit.remove(stringExtra17);
                            edit.commit();
                            if (ak.e(this, stringExtra17)) {
                                ak.d(this, stringExtra17);
                            }
                            ak.b(this, stringExtra17);
                            if (e() && string != null) {
                                try {
                                    b(a(stringExtra17, string));
                                    com.xiaomi.a.a.c.c.a("uninstall " + stringExtra17 + " msg sent");
                                } catch (p e4) {
                                    com.xiaomi.a.a.c.c.d("Fail to send Message: " + e4.getMessage());
                                    a(10, e4);
                                }
                            }
                        }
                    } else {
                        a("1", 0);
                        com.xiaomi.a.a.c.c.a("close the miliao channel as the app is uninstalled.");
                    }
                }
            } else if ("com.xiaomi.mipush.CLEAR_NOTIFICATION".equals(intent.getAction())) {
                String stringExtra18 = intent.getStringExtra(aq.y);
                int intExtra2 = intent.getIntExtra(aq.z, 0);
                if (TextUtils.isEmpty(stringExtra18)) {
                    return;
                }
                if (intExtra2 >= 0) {
                    ak.a(this, stringExtra18, intExtra2);
                } else if (intExtra2 == -1) {
                    ak.b(this, stringExtra18);
                }
            } else if ("com.xiaomi.mipush.SET_NOTIFICATION_TYPE".equals(intent.getAction())) {
                String stringExtra19 = intent.getStringExtra(aq.y);
                String stringExtra20 = intent.getStringExtra(aq.C);
                if (intent.hasExtra(aq.A)) {
                    i3 = intent.getIntExtra(aq.A, 0);
                    b2 = com.xiaomi.a.a.g.c.b(stringExtra19 + i3);
                } else {
                    b2 = com.xiaomi.a.a.g.c.b(stringExtra19);
                    i3 = 0;
                    i4 = 1;
                }
                if (TextUtils.isEmpty(stringExtra19) || !TextUtils.equals(stringExtra20, b2)) {
                    com.xiaomi.a.a.c.c.d("invalid notification for " + stringExtra19);
                } else if (i4 != 0) {
                    ak.d(this, stringExtra19);
                } else {
                    ak.b(this, stringExtra19, i3);
                }
            }
        }
    }

    public int onStartCommand(Intent intent, int i2, int i3) {
        onStart(intent, i3);
        return f2497a;
    }
}
