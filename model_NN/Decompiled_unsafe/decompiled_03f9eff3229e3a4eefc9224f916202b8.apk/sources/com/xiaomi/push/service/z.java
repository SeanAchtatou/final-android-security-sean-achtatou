package com.xiaomi.push.service;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.os.Build;
import android.text.TextUtils;
import com.sina.weibo.sdk.component.ShareRequestParam;
import com.xiaomi.a.a.c.c;
import com.xiaomi.a.a.d.a;
import com.xiaomi.a.a.e.b;
import com.xiaomi.a.a.g.d;
import java.util.TreeMap;
import org.json.JSONObject;

public class z {

    /* renamed from: a  reason: collision with root package name */
    private static y f2555a;

    public static synchronized y a(Context context) {
        y yVar = null;
        synchronized (z.class) {
            if (f2555a != null) {
                yVar = f2555a;
            } else {
                SharedPreferences sharedPreferences = context.getSharedPreferences("mipush_account", 0);
                String string = sharedPreferences.getString("uuid", null);
                String string2 = sharedPreferences.getString("token", null);
                String string3 = sharedPreferences.getString("security", null);
                String string4 = sharedPreferences.getString("app_id", null);
                String string5 = sharedPreferences.getString("app_token", null);
                String string6 = sharedPreferences.getString("package_name", null);
                String string7 = sharedPreferences.getString("device_id", null);
                int i = sharedPreferences.getInt("env_type", 1);
                if (!TextUtils.isEmpty(string7) && string7.startsWith("a-")) {
                    string7 = x.c(context);
                    sharedPreferences.edit().putString("device_id", string7).commit();
                }
                if (!TextUtils.isEmpty(string) && !TextUtils.isEmpty(string2) && !TextUtils.isEmpty(string3)) {
                    String c = x.c(context);
                    if ("com.xiaomi.xmsf".equals(context.getPackageName()) || TextUtils.isEmpty(c) || TextUtils.isEmpty(string7) || string7.equals(c)) {
                        f2555a = new y(string, string2, string3, string4, string5, string6, i);
                        yVar = f2555a;
                    } else {
                        c.d("erase the old account.");
                        b(context);
                    }
                }
            }
        }
        return yVar;
    }

    public static synchronized y a(Context context, String str, String str2, String str3) {
        PackageInfo packageInfo;
        y yVar = null;
        synchronized (z.class) {
            TreeMap treeMap = new TreeMap();
            treeMap.put("devid", x.a(context));
            String str4 = c(context) ? "1000271" : str2;
            String str5 = c(context) ? "420100086271" : str3;
            String str6 = c(context) ? "com.xiaomi.xmsf" : str;
            treeMap.put("appid", str4);
            treeMap.put("apptoken", str5);
            try {
                packageInfo = context.getPackageManager().getPackageInfo(str6, 16384);
            } catch (Exception e) {
                c.a(e);
                packageInfo = null;
            }
            treeMap.put("appversion", packageInfo != null ? String.valueOf(packageInfo.versionCode) : "0");
            treeMap.put("'sdkversion", Integer.toString(8));
            treeMap.put("packagename", str6);
            treeMap.put("model", Build.MODEL);
            treeMap.put("imei_md5", d.a(x.b(context)));
            treeMap.put("os", Build.VERSION.RELEASE + "-" + Build.VERSION.INCREMENTAL);
            b a2 = com.xiaomi.a.a.e.d.a(context, a(), treeMap);
            String str7 = "";
            if (a2 != null) {
                str7 = a2.a();
            }
            if (!TextUtils.isEmpty(str7)) {
                JSONObject jSONObject = new JSONObject(str7);
                if (jSONObject.getInt("code") == 0) {
                    JSONObject jSONObject2 = jSONObject.getJSONObject(ShareRequestParam.RESP_UPLOAD_PIC_PARAM_DATA);
                    yVar = new y(jSONObject2.getString("userId") + "@xiaomi.com/an" + d.a(6), jSONObject2.getString("token"), jSONObject2.getString("ssecurity"), str4, str5, str6, a.c());
                    a(context, yVar);
                    f2555a = yVar;
                } else {
                    ac.a(context, jSONObject.getInt("code"), jSONObject.optString("description"));
                    c.a(str7);
                }
            }
        }
        return yVar;
    }

    public static String a() {
        if (a.b()) {
            return "http://10.237.12.17:9085/pass/register";
        }
        return "https://" + (a.a() ? "sandbox.xmpush.xiaomi.com" : "register.xmpush.xiaomi.com") + "/pass/register";
    }

    private static void a(Context context, y yVar) {
        SharedPreferences.Editor edit = context.getSharedPreferences("mipush_account", 0).edit();
        edit.putString("uuid", yVar.f2554a);
        edit.putString("security", yVar.c);
        edit.putString("token", yVar.b);
        edit.putString("app_id", yVar.d);
        edit.putString("package_name", yVar.f);
        edit.putString("app_token", yVar.e);
        edit.putString("device_id", x.c(context));
        edit.putInt("env_type", yVar.g);
        edit.commit();
    }

    public static void b(Context context) {
        context.getSharedPreferences("mipush_account", 0).edit().clear().commit();
        f2555a = null;
    }

    private static boolean c(Context context) {
        return context.getPackageName().equals("com.xiaomi.xmsf");
    }
}
