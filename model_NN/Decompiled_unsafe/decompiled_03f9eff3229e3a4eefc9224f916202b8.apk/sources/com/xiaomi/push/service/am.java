package com.xiaomi.push.service;

import com.xiaomi.a.a.c.c;
import com.xiaomi.e.e;
import com.xiaomi.network.d;
import com.xiaomi.push.protobuf.a;
import java.net.Socket;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

public class am {

    /* renamed from: a  reason: collision with root package name */
    private static final Pattern f2522a = Pattern.compile("([0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3})");
    private static long b = 0;
    private static ThreadPoolExecutor c = new ThreadPoolExecutor(1, 1, 20, TimeUnit.SECONDS, new LinkedBlockingQueue());

    public static void a() {
        a.C0060a d;
        long currentTimeMillis = System.currentTimeMillis();
        if ((c.getActiveCount() <= 0 || currentTimeMillis - b >= 1800000) && e.a().c() && (d = h.a().d()) != null && d.l() > 0) {
            b = currentTimeMillis;
            a(d.k(), true);
        }
    }

    public static void a(List<String> list, boolean z) {
        c.execute(new an(list, z));
    }

    /* access modifiers changed from: private */
    public static boolean b(String str) {
        long currentTimeMillis = System.currentTimeMillis();
        try {
            c.a("ConnectivityTest: begin to connect to " + str);
            Socket socket = new Socket();
            socket.connect(d.b(str, 5222), 5000);
            socket.setTcpNoDelay(true);
            c.a("ConnectivityTest: connect to " + str + " in " + (System.currentTimeMillis() - currentTimeMillis));
            socket.close();
            return true;
        } catch (Throwable th) {
            c.d("ConnectivityTest: could not connect to:" + str + " exception: " + th.getClass().getSimpleName() + " description: " + th.getMessage());
            return false;
        }
    }
}
