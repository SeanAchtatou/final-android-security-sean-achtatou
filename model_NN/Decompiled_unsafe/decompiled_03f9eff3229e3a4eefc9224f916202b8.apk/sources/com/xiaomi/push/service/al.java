package com.xiaomi.push.service;

import android.content.SharedPreferences;
import com.xiaomi.a.a.g.d;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

public class al {

    /* renamed from: a  reason: collision with root package name */
    private static Object f2521a = new Object();
    private static Map<String, Queue<String>> b = new HashMap();

    public static boolean a(XMPushService xMPushService, String str, String str2) {
        synchronized (f2521a) {
            SharedPreferences sharedPreferences = xMPushService.getSharedPreferences("push_message_ids", 0);
            Queue queue = b.get(str);
            if (queue == null) {
                String[] split = sharedPreferences.getString(str, "").split(",");
                queue = new LinkedList();
                for (String add : split) {
                    queue.add(add);
                }
                b.put(str, queue);
            }
            if (queue.contains(str2)) {
                return true;
            }
            queue.add(str2);
            if (queue.size() > 10) {
                queue.poll();
            }
            String a2 = d.a(queue, ",");
            SharedPreferences.Editor edit = sharedPreferences.edit();
            edit.putString(str, a2);
            edit.commit();
            return false;
        }
    }
}
