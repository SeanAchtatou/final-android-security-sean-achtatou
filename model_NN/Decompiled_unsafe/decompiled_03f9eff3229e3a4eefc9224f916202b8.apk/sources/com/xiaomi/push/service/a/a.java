package com.xiaomi.push.service.a;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.SystemClock;
import com.xiaomi.a.a.c.c;
import com.xiaomi.d.j;
import com.xiaomi.push.service.aq;

public class a {
    private static volatile long c = 0;

    /* renamed from: a  reason: collision with root package name */
    private PendingIntent f2510a = null;
    private Context b = null;

    public a(Context context) {
        this.b = context;
    }

    private void a(AlarmManager alarmManager, long j, PendingIntent pendingIntent) {
        Class<AlarmManager> cls = AlarmManager.class;
        try {
            cls.getMethod("setExact", Integer.TYPE, Long.TYPE, PendingIntent.class).invoke(alarmManager, 0, Long.valueOf(j), pendingIntent);
        } catch (Exception e) {
            c.a(e);
        }
    }

    public synchronized void a() {
        if (this.f2510a != null) {
            ((AlarmManager) this.b.getSystemService("alarm")).cancel(this.f2510a);
            this.f2510a = null;
            c.c("unregister timer");
            c = 0;
        }
    }

    public synchronized void a(Intent intent, long j) {
        if (this.f2510a == null) {
            AlarmManager alarmManager = (AlarmManager) this.b.getSystemService("alarm");
            this.f2510a = PendingIntent.getBroadcast(this.b, 0, intent, 0);
            if (Build.VERSION.SDK_INT >= 19) {
                a(alarmManager, j, this.f2510a);
            } else {
                alarmManager.set(0, j, this.f2510a);
            }
            c.c("register timer " + c);
        }
    }

    public synchronized void a(boolean z) {
        Intent intent = new Intent(aq.o);
        intent.setPackage(this.b.getPackageName());
        long c2 = (long) j.c();
        if (z || c == 0) {
            c = (c2 - (SystemClock.elapsedRealtime() % c2)) + System.currentTimeMillis();
        } else {
            c += c2;
            if (c < System.currentTimeMillis()) {
                c = c2 + System.currentTimeMillis();
            }
        }
        a(intent, c);
    }

    public synchronized boolean b() {
        return this.f2510a != null;
    }
}
