package com.xiaomi.push.service;

import com.xiaomi.a.a.c.c;
import com.xiaomi.d.p;
import com.xiaomi.f.a.h;
import com.xiaomi.push.service.XMPushService;

final class ag extends XMPushService.e {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ XMPushService f2516a;
    final /* synthetic */ h b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    ag(int i, XMPushService xMPushService, h hVar) {
        super(i);
        this.f2516a = xMPushService;
        this.b = hVar;
    }

    public void a() {
        try {
            h a2 = ad.e(this.f2516a, this.b);
            a2.m().a("message_obsleted", "1");
            this.f2516a.b(a2);
        } catch (p e) {
            c.a(e);
            this.f2516a.a(10, e);
        }
    }

    public String b() {
        return "send ack message for obsleted message.";
    }
}
