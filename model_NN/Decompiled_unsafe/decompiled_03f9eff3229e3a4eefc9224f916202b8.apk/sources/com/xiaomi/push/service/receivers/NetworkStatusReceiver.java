package com.xiaomi.push.service.receivers;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import com.xiaomi.a.a.c.c;
import com.xiaomi.a.a.e.d;
import com.xiaomi.mipush.sdk.g;
import com.xiaomi.mipush.sdk.m;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class NetworkStatusReceiver extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    private static int f2547a = 1;
    private static int b = 1;
    private static int c = 2;
    private static BlockingQueue<Runnable> d = new LinkedBlockingQueue();
    private static ThreadPoolExecutor e = new ThreadPoolExecutor(f2547a, b, (long) c, TimeUnit.SECONDS, d);

    /* access modifiers changed from: private */
    public void a(Context context) {
        if (!m.a(context).b() && g.a(context).i() && !g.a(context).n()) {
            try {
                Intent intent = new Intent();
                intent.setComponent(new ComponentName(context, "com.xiaomi.push.service.XMPushService"));
                intent.setAction("com.xiaomi.push.network_status_changed");
                context.startService(intent);
            } catch (Exception e2) {
                c.a(e2);
            }
        }
        if (d.d(context) && m.a(context).f()) {
            m.a(context).c();
        }
    }

    public void onReceive(Context context, Intent intent) {
        e.execute(new a(this, context));
    }
}
