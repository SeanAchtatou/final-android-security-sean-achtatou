package com.xiaomi.push.service;

import android.content.Context;
import android.text.TextUtils;
import com.xiaomi.a.a.g.d;
import java.util.ArrayList;
import java.util.List;

public class aa {

    /* renamed from: a  reason: collision with root package name */
    private static aa f2511a = null;
    private Context b;
    private List<String> c = new ArrayList();

    private aa(Context context) {
        this.b = context.getApplicationContext();
        if (this.b == null) {
            this.b = context;
        }
        for (String str : this.b.getSharedPreferences("mipush_app_info", 0).getString("unregistered_pkg_names", "").split(",")) {
            if (TextUtils.isEmpty(str)) {
                this.c.add(str);
            }
        }
    }

    public static aa a(Context context) {
        if (f2511a == null) {
            f2511a = new aa(context);
        }
        return f2511a;
    }

    public boolean a(String str) {
        boolean contains;
        synchronized (this.c) {
            contains = this.c.contains(str);
        }
        return contains;
    }

    public void b(String str) {
        synchronized (this.c) {
            if (!this.c.contains(str)) {
                this.c.add(str);
                this.b.getSharedPreferences("mipush_app_info", 0).edit().putString("unregistered_pkg_names", d.a(this.c, ",")).commit();
            }
        }
    }

    public void c(String str) {
        synchronized (this.c) {
            if (this.c.contains(str)) {
                this.c.remove(str);
                this.b.getSharedPreferences("mipush_app_info", 0).edit().putString("unregistered_pkg_names", d.a(this.c, ",")).commit();
            }
        }
    }
}
