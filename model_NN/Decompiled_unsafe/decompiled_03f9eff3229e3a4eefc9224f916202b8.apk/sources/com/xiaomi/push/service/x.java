package com.xiaomi.push.service;

import android.content.Context;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import com.xiaomi.a.a.c.c;
import com.xiaomi.a.a.g.d;

public class x {

    /* renamed from: a  reason: collision with root package name */
    private static String f2553a = null;
    private static String b = null;
    private static String c = null;

    public static String a(Context context) {
        String str;
        String str2 = null;
        if (b == null) {
            String b2 = b(context);
            try {
                str = Settings.Secure.getString(context.getContentResolver(), "android_id");
            } catch (Throwable th) {
                c.a(th);
                str = null;
            }
            if (Build.VERSION.SDK_INT > 8) {
                str2 = Build.SERIAL;
            }
            b = "a-" + d.b(b2 + str + str2);
        }
        return b;
    }

    public static String b(Context context) {
        if (f2553a != null) {
            return f2553a;
        }
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
            String deviceId = telephonyManager.getDeviceId();
            int i = 10;
            while (deviceId == null) {
                int i2 = i - 1;
                if (i <= 0) {
                    break;
                }
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                }
                deviceId = telephonyManager.getDeviceId();
                i = i2;
            }
            if (deviceId != null) {
                f2553a = deviceId;
            }
            return deviceId;
        } catch (Throwable th) {
            c.a(th);
            return null;
        }
    }

    public static synchronized String c(Context context) {
        String str;
        String str2;
        synchronized (x.class) {
            if (c != null) {
                str2 = c;
            } else {
                try {
                    str = Settings.Secure.getString(context.getContentResolver(), "android_id");
                } catch (Throwable th) {
                    c.a(th);
                    str = null;
                }
                c = d.b(str + (Build.VERSION.SDK_INT > 8 ? Build.SERIAL : null));
                str2 = c;
            }
        }
        return str2;
    }

    public static String d(Context context) {
        return ((TelephonyManager) context.getSystemService("phone")).getSimOperatorName();
    }
}
