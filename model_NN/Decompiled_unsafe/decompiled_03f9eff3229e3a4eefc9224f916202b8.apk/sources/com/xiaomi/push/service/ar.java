package com.xiaomi.push.service;

import android.content.Context;
import com.xiaomi.a.a.c.c;
import com.xiaomi.a.a.e.d;
import com.xiaomi.e.g;
import com.xiaomi.network.e;
import com.xiaomi.network.f;
import com.xiaomi.push.protobuf.a;
import com.xiaomi.push.protobuf.b;
import com.xiaomi.push.service.h;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;

public class ar extends h.a implements f.a {

    /* renamed from: a  reason: collision with root package name */
    private XMPushService f2529a;
    private long b;

    static class a implements f.b {
        a() {
        }

        public String a(String str) {
            URL url = new URL(str);
            int port = url.getPort() == -1 ? 80 : url.getPort();
            try {
                long currentTimeMillis = System.currentTimeMillis();
                String a2 = d.a(com.xiaomi.d.e.h.a(), url);
                g.a(url.getHost() + ":" + port, (int) (System.currentTimeMillis() - currentTimeMillis), null);
                return a2;
            } catch (IOException e) {
                g.a(url.getHost() + ":" + port, -1, e);
                throw e;
            }
        }
    }

    static class b extends com.xiaomi.network.g {
        protected b(Context context, e eVar, f.b bVar, String str) {
            super(context, eVar, bVar, str);
        }

        /* access modifiers changed from: protected */
        public String a(ArrayList<String> arrayList, String str, String str2) {
            try {
                if (com.xiaomi.e.e.a().c()) {
                    str2 = com.xiaomi.d.e.h.b();
                }
                return super.a(arrayList, str, str2);
            } catch (IOException e) {
                IOException iOException = e;
                g.a(0, com.xiaomi.push.c.a.GSLB_ERR.a(), 1, null, d.d(this.c) ? 1 : 0);
                throw iOException;
            }
        }
    }

    ar(XMPushService xMPushService) {
        this.f2529a = xMPushService;
    }

    public static void a(XMPushService xMPushService) {
        ar arVar = new ar(xMPushService);
        h.a().a(arVar);
        a.C0060a d = h.a().d();
        boolean z = true;
        if (d != null && d.f()) {
            z = d.f();
        }
        if (z) {
            f.a(arVar);
        }
        f.a(xMPushService, null, new a(), "0", "push", "2.2");
    }

    public f a(Context context, e eVar, f.b bVar, String str) {
        return new b(context, eVar, bVar, str);
    }

    public void a(a.C0060a aVar) {
        if (aVar.f()) {
            c.a("Switch to BucketV2 :" + aVar.e());
            f a2 = f.a();
            synchronized (f.class) {
                if (aVar.e()) {
                    if (!(a2 instanceof com.xiaomi.network.g)) {
                        f.a(this);
                        f.a(this.f2529a, null, new a(), "0", "push", "2.2");
                    }
                } else if (f.a() instanceof com.xiaomi.network.g) {
                    f.a((f.a) null);
                    f.a(this.f2529a, null, new a(), "0", "push", "2.2");
                }
            }
        }
    }

    public void a(b.a aVar) {
        com.xiaomi.network.b b2;
        boolean z;
        if (aVar.d() && System.currentTimeMillis() - this.b > 3600000) {
            c.a("fetch bucket :" + aVar.c());
            this.b = System.currentTimeMillis();
            f a2 = f.a();
            a2.d();
            a2.e();
            com.xiaomi.d.a g = this.f2529a.g();
            if (g != null && (b2 = a2.b(g.a().f())) != null) {
                ArrayList<String> d = b2.d();
                Iterator<String> it = d.iterator();
                while (true) {
                    if (it.hasNext()) {
                        if (it.next().equals(g.c())) {
                            z = false;
                            break;
                        }
                    } else {
                        z = true;
                        break;
                    }
                }
                if (z && !d.isEmpty()) {
                    c.a("bucket changed, force reconnect");
                    this.f2529a.a(0, (Exception) null);
                    this.f2529a.a(false);
                }
            }
        }
    }
}
