package com.xiaomi.push.service;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.text.TextUtils;
import android.util.Pair;
import android.widget.RemoteViews;
import com.sina.weibo.sdk.constant.WBConstants;
import com.xiaomi.a.a.c.c;
import com.xiaomi.f.a.h;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.apache.mina.proxy.handlers.http.ntlm.NTLMConstants;
import org.json.JSONException;
import org.json.JSONObject;

public class ak {

    /* renamed from: a  reason: collision with root package name */
    public static long f2520a = 0;
    private static LinkedList<Pair<Integer, String>> b = new LinkedList<>();

    private static int a(Context context, String str, String str2) {
        if (str.equals(context.getPackageName())) {
            return context.getResources().getIdentifier(str2, "drawable", str);
        }
        return 0;
    }

    private static Notification a(Notification notification, String str) {
        try {
            Field declaredField = Notification.class.getDeclaredField("extraNotification");
            declaredField.setAccessible(true);
            Object obj = declaredField.get(notification);
            Method declaredMethod = obj.getClass().getDeclaredMethod("setTargetPkg", CharSequence.class);
            declaredMethod.setAccessible(true);
            declaredMethod.invoke(obj, str);
        } catch (Exception e) {
            c.a(e);
        }
        return notification;
    }

    @SuppressLint({"NewApi"})
    private static Notification a(Context context, h hVar, byte[] bArr, RemoteViews remoteViews, PendingIntent pendingIntent) {
        com.xiaomi.f.a.c m = hVar.m();
        Notification.Builder builder = new Notification.Builder(context);
        String[] a2 = a(context, m);
        builder.setContentTitle(a2[0]);
        builder.setContentText(a2[1]);
        if (remoteViews != null) {
            builder.setContent(remoteViews);
        } else if (Build.VERSION.SDK_INT >= 16) {
            builder.setStyle(new Notification.BigTextStyle().bigText(a2[1]));
        }
        builder.setWhen(System.currentTimeMillis());
        builder.setContentIntent(pendingIntent);
        int a3 = a(context, a(hVar), "mipush_notification");
        int a4 = a(context, a(hVar), "mipush_small_notification");
        if (a3 <= 0 || a4 <= 0) {
            builder.setSmallIcon(f(context, a(hVar)));
        } else {
            builder.setLargeIcon(a(context, a3));
            builder.setSmallIcon(a4);
        }
        builder.setAutoCancel(true);
        long currentTimeMillis = System.currentTimeMillis();
        Map<String, String> s = m.s();
        if (s != null && s.containsKey("ticker")) {
            builder.setTicker(s.get("ticker"));
        }
        if (currentTimeMillis - f2520a > 10000) {
            f2520a = currentTimeMillis;
            int c = e(context, a(hVar)) ? c(context, a(hVar)) : m.f;
            builder.setDefaults(c);
            if (!(s == null || (c & 1) == 0)) {
                String str = s.get("sound_uri");
                if (!TextUtils.isEmpty(str) && str.startsWith("android.resource://" + a(hVar))) {
                    builder.setDefaults(c ^ 1);
                    builder.setSound(Uri.parse(str));
                }
            }
        }
        return builder.getNotification();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    private static PendingIntent a(Context context, h hVar, com.xiaomi.f.a.c cVar, byte[] bArr) {
        if (cVar != null && !TextUtils.isEmpty(cVar.g)) {
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setData(Uri.parse(cVar.g));
            intent.addFlags(NTLMConstants.FLAG_UNIDENTIFIED_11);
            return PendingIntent.getActivity(context, 0, intent, NTLMConstants.FLAG_UNIDENTIFIED_10);
        } else if (b(hVar)) {
            Intent intent2 = new Intent();
            intent2.setComponent(new ComponentName("com.xiaomi.xmsf", "com.xiaomi.mipush.sdk.PushMessageHandler"));
            intent2.putExtra("mipush_payload", bArr);
            intent2.putExtra("mipush_notified", true);
            intent2.addCategory(String.valueOf(cVar.q()));
            return PendingIntent.getService(context, 0, intent2, NTLMConstants.FLAG_UNIDENTIFIED_10);
        } else {
            Intent intent3 = new Intent("com.xiaomi.mipush.RECEIVE_MESSAGE");
            intent3.setComponent(new ComponentName(hVar.f, "com.xiaomi.mipush.sdk.PushMessageHandler"));
            intent3.putExtra("mipush_payload", bArr);
            intent3.putExtra("mipush_notified", true);
            intent3.addCategory(String.valueOf(cVar.q()));
            return PendingIntent.getService(context, 0, intent3, NTLMConstants.FLAG_UNIDENTIFIED_10);
        }
    }

    private static Bitmap a(Context context, int i) {
        return a(context.getResources().getDrawable(i));
    }

    public static Bitmap a(Drawable drawable) {
        int i = 1;
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        }
        int intrinsicWidth = drawable.getIntrinsicWidth();
        if (intrinsicWidth <= 0) {
            intrinsicWidth = 1;
        }
        int intrinsicHeight = drawable.getIntrinsicHeight();
        if (intrinsicHeight > 0) {
            i = intrinsicHeight;
        }
        Bitmap createBitmap = Bitmap.createBitmap(intrinsicWidth, i, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return createBitmap;
    }

    static String a(h hVar) {
        com.xiaomi.f.a.c m;
        if (!(!"com.xiaomi.xmsf".equals(hVar.f) || (m = hVar.m()) == null || m.s() == null)) {
            String str = m.s().get("miui_package_name");
            if (!TextUtils.isEmpty(str)) {
                return str;
            }
        }
        return hVar.f;
    }

    public static void a(Context context, h hVar, byte[] bArr) {
        Notification notification;
        int i;
        NotificationManager notificationManager = (NotificationManager) context.getSystemService("notification");
        com.xiaomi.f.a.c m = hVar.m();
        RemoteViews b2 = b(context, hVar, bArr);
        PendingIntent a2 = a(context, hVar, m, bArr);
        if (a2 == null) {
            c.a("The click PendingIntent is null. ");
            return;
        }
        if (Build.VERSION.SDK_INT >= 11) {
            notification = a(context, hVar, bArr, b2, a2);
        } else {
            Notification notification2 = new Notification(f(context, a(hVar)), null, System.currentTimeMillis());
            String[] a3 = a(context, m);
            try {
                notification2.getClass().getMethod("setLatestEventInfo", Context.class, CharSequence.class, CharSequence.class, PendingIntent.class).invoke(notification2, context, a3[0], a3[1], a2);
            } catch (NoSuchMethodException e) {
                c.a(e);
            } catch (IllegalAccessException e2) {
                c.a(e2);
            } catch (IllegalArgumentException e3) {
                c.a(e3);
            } catch (InvocationTargetException e4) {
                c.a(e4);
            }
            Map<String, String> s = m.s();
            if (s != null && s.containsKey("ticker")) {
                notification2.tickerText = s.get("ticker");
            }
            long currentTimeMillis = System.currentTimeMillis();
            if (currentTimeMillis - f2520a > 10000) {
                f2520a = currentTimeMillis;
                int i2 = m.f;
                if (e(context, a(hVar))) {
                    i = c(context, a(hVar));
                } else {
                    i = i2;
                }
                notification2.defaults = i;
                if (!(s == null || (i & 1) == 0)) {
                    String str = s.get("sound_uri");
                    if (!TextUtils.isEmpty(str) && str.startsWith("android.resource://" + a(hVar))) {
                        notification2.defaults = i ^ 1;
                        notification2.sound = Uri.parse(str);
                    }
                }
            }
            notification2.flags |= 16;
            if (b2 != null) {
                notification2.contentView = b2;
            }
            notification = notification2;
        }
        if ("com.xiaomi.xmsf".equals(context.getPackageName())) {
            a(notification, a(hVar));
        }
        int q = m.q() + ((a(hVar).hashCode() / 10) * 10);
        notificationManager.notify(q, notification);
        Pair pair = new Pair(Integer.valueOf(q), a(hVar));
        synchronized (b) {
            b.add(pair);
            if (b.size() > 100) {
                b.remove();
            }
        }
    }

    public static void a(Context context, String str, int i) {
        int hashCode = ((str.hashCode() / 10) * 10) + i;
        ((NotificationManager) context.getSystemService("notification")).cancel(hashCode);
        synchronized (b) {
            Iterator<Pair<Integer, String>> it = b.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                Pair next = it.next();
                if (hashCode == ((Integer) next.first).intValue() && TextUtils.equals(str, (CharSequence) next.second)) {
                    b.remove(next);
                    break;
                }
            }
        }
    }

    public static boolean a(Context context, String str) {
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = ((ActivityManager) context.getSystemService("activity")).getRunningAppProcesses();
        if (runningAppProcesses != null) {
            for (ActivityManager.RunningAppProcessInfo next : runningAppProcesses) {
                if (next.importance == 100 && Arrays.asList(next.pkgList).contains(str)) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean a(Map<String, String> map) {
        if (map == null || !map.containsKey("notify_foreground")) {
            return true;
        }
        return "1".equals(map.get("notify_foreground"));
    }

    private static String[] a(Context context, com.xiaomi.f.a.c cVar) {
        String h = cVar.h();
        String j = cVar.j();
        Map<String, String> s = cVar.s();
        if (s != null) {
            int intValue = Float.valueOf((((float) context.getResources().getDisplayMetrics().widthPixels) / context.getResources().getDisplayMetrics().density) + 0.5f).intValue();
            if (intValue <= 320) {
                String str = s.get("title_short");
                if (!TextUtils.isEmpty(str)) {
                    h = str;
                }
                String str2 = s.get("description_short");
                if (TextUtils.isEmpty(str2)) {
                    str2 = j;
                }
                j = str2;
            } else if (intValue > 360) {
                String str3 = s.get("title_long");
                if (!TextUtils.isEmpty(str3)) {
                    h = str3;
                }
                String str4 = s.get("description_long");
                if (!TextUtils.isEmpty(str4)) {
                    j = str4;
                }
            }
        }
        return new String[]{h, j};
    }

    private static RemoteViews b(Context context, h hVar, byte[] bArr) {
        com.xiaomi.f.a.c m = hVar.m();
        String a2 = a(hVar);
        Map<String, String> s = m.s();
        if (s == null) {
            return null;
        }
        String str = s.get("layout_name");
        String str2 = s.get("layout_value");
        if (TextUtils.isEmpty(str) || TextUtils.isEmpty(str2)) {
            return null;
        }
        try {
            Resources resourcesForApplication = context.getPackageManager().getResourcesForApplication(a2);
            int identifier = resourcesForApplication.getIdentifier(str, "layout", a2);
            if (identifier == 0) {
                return null;
            }
            RemoteViews remoteViews = new RemoteViews(a2, identifier);
            try {
                JSONObject jSONObject = new JSONObject(str2);
                if (jSONObject.has("text")) {
                    JSONObject jSONObject2 = jSONObject.getJSONObject("text");
                    Iterator<String> keys = jSONObject2.keys();
                    while (keys.hasNext()) {
                        String next = keys.next();
                        String string = jSONObject2.getString(next);
                        int identifier2 = resourcesForApplication.getIdentifier(next, "id", a2);
                        if (identifier2 > 0) {
                            remoteViews.setTextViewText(identifier2, string);
                        }
                    }
                }
                if (jSONObject.has(WBConstants.GAME_PARAMS_GAME_IMAGE_URL)) {
                    JSONObject jSONObject3 = jSONObject.getJSONObject(WBConstants.GAME_PARAMS_GAME_IMAGE_URL);
                    Iterator<String> keys2 = jSONObject3.keys();
                    while (keys2.hasNext()) {
                        String next2 = keys2.next();
                        String string2 = jSONObject3.getString(next2);
                        int identifier3 = resourcesForApplication.getIdentifier(next2, "id", a2);
                        int identifier4 = resourcesForApplication.getIdentifier(string2, "drawable", a2);
                        if (identifier3 > 0) {
                            remoteViews.setImageViewResource(identifier3, identifier4);
                        }
                    }
                }
                if (jSONObject.has("time")) {
                    JSONObject jSONObject4 = jSONObject.getJSONObject("time");
                    Iterator<String> keys3 = jSONObject4.keys();
                    while (keys3.hasNext()) {
                        String next3 = keys3.next();
                        String string3 = jSONObject4.getString(next3);
                        if (string3.length() == 0) {
                            string3 = "yy-MM-dd hh:mm";
                        }
                        int identifier5 = resourcesForApplication.getIdentifier(next3, "id", a2);
                        if (identifier5 > 0) {
                            remoteViews.setTextViewText(identifier5, new SimpleDateFormat(string3).format(new Date(System.currentTimeMillis())));
                        }
                    }
                }
                return remoteViews;
            } catch (JSONException e) {
                c.a(e);
                return null;
            }
        } catch (PackageManager.NameNotFoundException e2) {
            c.a(e2);
            return null;
        }
    }

    public static void b(Context context, String str) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService("notification");
        synchronized (b) {
            Iterator it = ((LinkedList) b.clone()).iterator();
            while (it.hasNext()) {
                Pair pair = (Pair) it.next();
                if (TextUtils.equals((CharSequence) pair.second, str)) {
                    notificationManager.cancel(((Integer) pair.first).intValue());
                    b.remove(pair);
                }
            }
        }
    }

    static void b(Context context, String str, int i) {
        context.getSharedPreferences("pref_notify_type", 0).edit().putInt(str, i).commit();
    }

    public static boolean b(h hVar) {
        com.xiaomi.f.a.c m = hVar.m();
        return m != null && m.v();
    }

    static int c(Context context, String str) {
        return context.getSharedPreferences("pref_notify_type", 0).getInt(str, Integer.MAX_VALUE);
    }

    static void d(Context context, String str) {
        context.getSharedPreferences("pref_notify_type", 0).edit().remove(str).commit();
    }

    static boolean e(Context context, String str) {
        return context.getSharedPreferences("pref_notify_type", 0).contains(str);
    }

    private static int f(Context context, String str) {
        int a2 = a(context, str, "mipush_notification");
        int a3 = a(context, str, "mipush_small_notification");
        if (a2 <= 0) {
            a2 = a3 > 0 ? a3 : context.getApplicationInfo().icon;
        }
        return (a2 != 0 || Build.VERSION.SDK_INT < 9) ? a2 : context.getApplicationInfo().logo;
    }
}
