package com.xiaomi.push.service;

import android.text.TextUtils;
import android.util.Base64;
import cn.banshenggua.aichang.room.message.SocketMessage;
import com.qq.e.comm.constants.Constants;
import com.xiaomi.a.a.c.c;
import com.xiaomi.d.c.d;
import com.xiaomi.d.c.h;
import com.xiaomi.d.e.k;
import com.xiaomi.d.k;
import com.xiaomi.d.l;
import com.xiaomi.e.g;
import com.xiaomi.network.b;
import com.xiaomi.network.f;
import com.xiaomi.push.protobuf.b;
import com.xiaomi.push.service.ao;
import java.util.Date;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private XMPushService f2509a;

    a(XMPushService xMPushService) {
        this.f2509a = xMPushService;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.xiaomi.network.f.a(java.lang.String, boolean):com.xiaomi.network.b
     arg types: [java.lang.String, int]
     candidates:
      com.xiaomi.network.f.a(java.util.Collection, java.lang.String):java.lang.String
      com.xiaomi.network.f.a(java.lang.String[], java.lang.String):java.lang.String
      com.xiaomi.network.f.a(java.lang.String, java.lang.String):void
      com.xiaomi.network.f.a(java.lang.String, com.xiaomi.network.b):void
      com.xiaomi.network.f.a(java.lang.String, boolean):com.xiaomi.network.b */
    private void a(com.xiaomi.d.c.a aVar) {
        String c = aVar.c();
        if (!TextUtils.isEmpty(c)) {
            String[] split = c.split(";");
            b a2 = f.a().a(com.xiaomi.d.b.b(), false);
            if (a2 != null && split.length > 0) {
                a2.a(split);
                this.f2509a.a(20, (Exception) null);
                this.f2509a.a(true);
            }
        }
    }

    private void b(d dVar) {
        ao.b b;
        String m = dVar.m();
        String l = dVar.l();
        if (!TextUtils.isEmpty(m) && !TextUtils.isEmpty(l) && (b = ao.a().b(l, m)) != null) {
            k.a(this.f2509a, b.f2525a, (long) k.a(dVar.a()), true, System.currentTimeMillis());
        }
    }

    public void a(d dVar) {
        com.xiaomi.d.c.a p;
        ao.b b;
        if (!"5".equals(dVar.l())) {
            b(dVar);
        }
        if (dVar instanceof k.b) {
            k.b bVar = (k.b) dVar;
            k.b.a b2 = bVar.b();
            String l = bVar.l();
            String m = bVar.m();
            if (TextUtils.isEmpty(l) || (b = ao.a().b(l, m)) == null) {
                return;
            }
            if (b2 == k.b.a.f2395a) {
                b.a(ao.c.binded, 1, 0, null, null);
                c.a("SMACK: channel bind succeeded, chid=" + l);
                return;
            }
            h p2 = bVar.p();
            c.a("SMACK: channel bind failed, error=" + p2.d());
            if (p2 != null) {
                if ("auth".equals(p2.b())) {
                    if ("invalid-sig".equals(p2.a())) {
                        c.a("SMACK: bind error invalid-sig token = " + b.c + " sec = " + b.i);
                        g.a(0, com.xiaomi.push.c.a.BIND_INVALID_SIG.a(), 1, null, 0);
                    }
                    b.a(ao.c.unbind, 1, 5, p2.a(), p2.b());
                    ao.a().a(l, m);
                } else if ("cancel".equals(p2.b())) {
                    b.a(ao.c.unbind, 1, 7, p2.a(), p2.b());
                    ao.a().a(l, m);
                } else if ("wait".equals(p2.b())) {
                    this.f2509a.b(b);
                    b.a(ao.c.unbind, 1, 7, p2.a(), p2.b());
                }
                c.a("SMACK: channel bind failed, chid=" + l + " reason=" + p2.a());
                return;
            }
            return;
        }
        String l2 = dVar.l();
        if (TextUtils.isEmpty(l2)) {
            l2 = "1";
            dVar.l(l2);
        }
        if (!l2.equals("0")) {
            if (dVar instanceof com.xiaomi.d.c.b) {
                com.xiaomi.d.c.a p3 = dVar.p("kick");
                if (p3 != null) {
                    String m2 = dVar.m();
                    String a2 = p3.a("type");
                    String a3 = p3.a("reason");
                    c.a("kicked by server, chid=" + l2 + " userid=" + m2 + " type=" + a2 + " reason=" + a3);
                    if ("wait".equals(a2)) {
                        ao.b b3 = ao.a().b(l2, m2);
                        if (b3 != null) {
                            this.f2509a.b(b3);
                            b3.a(ao.c.unbind, 3, 0, a3, a2);
                            return;
                        }
                        return;
                    }
                    this.f2509a.a(l2, m2, 3, a3, a2);
                    ao.a().a(l2, m2);
                    return;
                }
            } else if (dVar instanceof com.xiaomi.d.c.c) {
                com.xiaomi.d.c.c cVar = (com.xiaomi.d.c.c) dVar;
                if ("redir".equals(cVar.b())) {
                    com.xiaomi.d.c.a p4 = cVar.p("hosts");
                    if (p4 != null) {
                        a(p4);
                        return;
                    }
                    return;
                }
            }
            this.f2509a.d().a(this.f2509a, l2, dVar);
        } else if (dVar instanceof com.xiaomi.d.c.b) {
            com.xiaomi.d.c.b bVar2 = (com.xiaomi.d.c.b) dVar;
            if ("0".equals(dVar.k()) && SocketMessage.MSG_RESULE_KEY.equals(bVar2.b().toString())) {
                com.xiaomi.d.a g = this.f2509a.g();
                if (g instanceof l) {
                    ((l) g).w();
                }
                g.b();
            } else if ("command".equals(bVar2.b().toString()) && (p = dVar.p("u")) != null) {
                String a4 = p.a("url");
                String a5 = p.a("startts");
                String a6 = p.a("endts");
                try {
                    Date date = new Date(Long.parseLong(a5));
                    Date date2 = new Date(Long.parseLong(a6));
                    String a7 = p.a("token");
                    boolean equals = "true".equals(p.a("force"));
                    String a8 = p.a("maxlen");
                    com.xiaomi.push.a.b.a(this.f2509a).a(a4, a7, date2, date, !TextUtils.isEmpty(a8) ? Integer.parseInt(a8) * 1024 : 0, equals);
                } catch (NumberFormatException e) {
                    c.a("parseLong fail " + e.getMessage());
                }
            }
            if (bVar2.a(Constants.KEYS.PLACEMENTS) != null) {
                try {
                    h.a().a(b.a.b(Base64.decode(bVar2.a(Constants.KEYS.PLACEMENTS), 8)));
                } catch (IllegalArgumentException e2) {
                    c.a("invalid Base64 exception + " + e2.getMessage());
                } catch (com.google.protobuf.micro.c e3) {
                    c.a("invalid pb exception + " + e3.getMessage());
                }
            }
        }
    }
}
