package com.xiaomi.push.service;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;

public class c {

    /* renamed from: a  reason: collision with root package name */
    private static c f2531a;
    private Context b;
    private int c = 0;

    private c(Context context) {
        this.b = context.getApplicationContext();
    }

    public static c a(Context context) {
        if (f2531a == null) {
            f2531a = new c(context);
        }
        return f2531a;
    }

    public boolean a() {
        return "@SHIP.TO.2A2FE0D7@".contains("xmsf") || "@SHIP.TO.2A2FE0D7@".contains("xiaomi") || "@SHIP.TO.2A2FE0D7@".contains("miui");
    }

    @SuppressLint({"NewApi"})
    public int b() {
        if (this.c != 0) {
            return this.c;
        }
        if (Build.VERSION.SDK_INT >= 17) {
            this.c = Settings.Global.getInt(this.b.getContentResolver(), "device_provisioned", 0);
            return this.c;
        }
        this.c = Settings.Secure.getInt(this.b.getContentResolver(), "device_provisioned", 0);
        return this.c;
    }

    @SuppressLint({"NewApi"})
    public Uri c() {
        return Build.VERSION.SDK_INT >= 17 ? Settings.Global.getUriFor("device_provisioned") : Settings.Secure.getUriFor("device_provisioned");
    }
}
