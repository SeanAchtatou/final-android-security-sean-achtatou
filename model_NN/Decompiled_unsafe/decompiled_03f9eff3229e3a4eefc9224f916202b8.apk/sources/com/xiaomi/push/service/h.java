package com.xiaomi.push.service;

import com.xiaomi.a.a.c.c;
import com.xiaomi.a.a.d.b;
import com.xiaomi.d.e.i;
import com.xiaomi.push.protobuf.a;
import com.xiaomi.push.protobuf.b;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.util.ArrayList;
import java.util.List;

public class h {
    private static h d = new h();
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public List<a> f2536a = new ArrayList();
    /* access modifiers changed from: private */
    public a.C0060a b;
    /* access modifiers changed from: private */
    public b.C0054b c;

    public static abstract class a {
        public void a(a.C0060a aVar) {
        }

        public void a(b.a aVar) {
        }
    }

    static {
        d.f();
    }

    private h() {
    }

    public static h a() {
        return d;
    }

    private void e() {
        if (this.c == null) {
            this.c = new i(this);
            i.a(this.c);
        }
    }

    private void f() {
        BufferedInputStream bufferedInputStream;
        BufferedInputStream bufferedInputStream2 = null;
        try {
            if (this.b != null) {
                bufferedInputStream = new BufferedInputStream(com.xiaomi.d.e.h.a().openFileInput("XMCloudCfg"));
                try {
                    this.b = a.C0060a.c(com.google.protobuf.micro.a.a(bufferedInputStream));
                    bufferedInputStream.close();
                } catch (Exception e) {
                    Exception exc = e;
                    bufferedInputStream2 = bufferedInputStream;
                    e = exc;
                    try {
                        c.a("save config failure: " + e.getMessage());
                        com.xiaomi.a.a.b.a.a(bufferedInputStream2);
                    } catch (Throwable th) {
                        th = th;
                        com.xiaomi.a.a.b.a.a(bufferedInputStream2);
                        throw th;
                    }
                } catch (Throwable th2) {
                    Throwable th3 = th2;
                    bufferedInputStream2 = bufferedInputStream;
                    th = th3;
                    com.xiaomi.a.a.b.a.a(bufferedInputStream2);
                    throw th;
                }
            } else {
                bufferedInputStream = null;
            }
            com.xiaomi.a.a.b.a.a(bufferedInputStream);
        } catch (Exception e2) {
            e = e2;
            c.a("save config failure: " + e.getMessage());
            com.xiaomi.a.a.b.a.a(bufferedInputStream2);
        }
    }

    /* access modifiers changed from: private */
    public void g() {
        try {
            if (this.b != null) {
                BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(com.xiaomi.d.e.h.a().openFileOutput("XMCloudCfg", 0));
                com.google.protobuf.micro.b a2 = com.google.protobuf.micro.b.a(bufferedOutputStream);
                this.b.a(a2);
                a2.a();
                bufferedOutputStream.close();
            }
        } catch (Exception e) {
            c.a("save config failure: " + e.getMessage());
        }
    }

    /* access modifiers changed from: package-private */
    public void a(b.a aVar) {
        a[] aVarArr;
        if (aVar.h() && aVar.g() > c()) {
            e();
        }
        synchronized (this) {
            aVarArr = (a[]) this.f2536a.toArray(new a[this.f2536a.size()]);
        }
        for (a a2 : aVarArr) {
            a2.a(aVar);
        }
    }

    public synchronized void a(a aVar) {
        this.f2536a.add(aVar);
    }

    /* access modifiers changed from: package-private */
    public synchronized void b() {
        this.f2536a.clear();
    }

    /* access modifiers changed from: package-private */
    public int c() {
        if (this.b != null) {
            return this.b.c();
        }
        return 0;
    }

    public a.C0060a d() {
        return this.b;
    }
}
