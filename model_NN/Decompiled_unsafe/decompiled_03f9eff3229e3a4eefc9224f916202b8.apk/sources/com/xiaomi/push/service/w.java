package com.xiaomi.push.service;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.xiaomi.push.service.XMPushService;

class w extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ v f2552a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    w(v vVar, Looper looper) {
        super(looper);
        this.f2552a = vVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.xiaomi.push.service.v.a(com.xiaomi.push.service.v, boolean):boolean
     arg types: [com.xiaomi.push.service.v, int]
     candidates:
      com.xiaomi.push.service.v.a(com.xiaomi.push.service.v, long):long
      com.xiaomi.push.service.v.a(int, java.lang.Object):void
      com.xiaomi.push.service.v.a(com.xiaomi.push.service.XMPushService$e, long):void
      com.xiaomi.push.service.v.a(com.xiaomi.push.service.v, boolean):boolean */
    public void handleMessage(Message message) {
        boolean unused = this.f2552a.b = true;
        long unused2 = this.f2552a.f2551a = System.currentTimeMillis();
        if (message.obj instanceof XMPushService.e) {
            ((XMPushService.e) message.obj).c();
        }
        boolean unused3 = this.f2552a.b = false;
    }
}
