package com.xiaomi.push.service;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.util.Pair;
import com.xiaomi.a.a.c.c;
import com.xiaomi.push.service.XMPushService;
import java.util.ArrayList;
import java.util.List;

public class v extends HandlerThread {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public volatile long f2551a = 0;
    /* access modifiers changed from: private */
    public volatile boolean b = false;
    private volatile Handler c;
    private List<Pair<XMPushService.e, Long>> d = new ArrayList();

    public v(String str) {
        super(str);
    }

    public void a() {
        for (int i = 1; i < 15; i++) {
            a(i);
        }
    }

    public void a(int i) {
        if (this.c != null) {
            this.c.removeMessages(i);
        }
    }

    public void a(int i, Object obj) {
        if (this.c != null) {
            this.c.removeMessages(i, obj);
        }
    }

    public void a(XMPushService.e eVar, long j) {
        synchronized (this.d) {
            if (this.c != null) {
                Message obtain = Message.obtain();
                obtain.what = eVar.d;
                obtain.obj = eVar;
                this.c.sendMessageDelayed(obtain, j);
            } else {
                c.a("the job is pended, the controller is not ready.");
                this.d.add(new Pair(eVar, Long.valueOf(j)));
            }
        }
    }

    public boolean b() {
        return this.b && System.currentTimeMillis() - this.f2551a > 600000;
    }

    public boolean b(int i) {
        if (this.c != null) {
            return this.c.hasMessages(i);
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void onLooperPrepared() {
        this.c = new w(this, getLooper());
        synchronized (this.d) {
            for (Pair next : this.d) {
                c.a("executing the pending job.");
                a((XMPushService.e) next.first, ((Long) next.second).longValue());
            }
            this.d.clear();
        }
    }
}
