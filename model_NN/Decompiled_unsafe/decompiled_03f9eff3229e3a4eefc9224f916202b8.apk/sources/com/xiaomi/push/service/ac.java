package com.xiaomi.push.service;

import android.content.Context;
import android.content.Intent;
import android.util.Pair;
import com.xiaomi.a.a.c.c;
import com.xiaomi.d.p;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class ac {

    /* renamed from: a  reason: collision with root package name */
    private static final Map<String, byte[]> f2513a = new HashMap();
    private static ArrayList<Pair<String, byte[]>> b = new ArrayList<>();

    public static void a(Context context, int i, String str) {
        synchronized (f2513a) {
            for (String next : f2513a.keySet()) {
                a(context, next, f2513a.get(next), i, str);
            }
            f2513a.clear();
        }
    }

    public static void a(Context context, String str, byte[] bArr, int i, String str2) {
        Intent intent = new Intent("com.xiaomi.mipush.ERROR");
        intent.setPackage(str);
        intent.putExtra("mipush_payload", bArr);
        intent.putExtra("mipush_error_code", i);
        intent.putExtra("mipush_error_msg", str2);
        context.sendBroadcast(intent, t.a(str));
    }

    public static void a(XMPushService xMPushService) {
        try {
            synchronized (f2513a) {
                for (String next : f2513a.keySet()) {
                    xMPushService.a(next, f2513a.get(next));
                }
                f2513a.clear();
            }
        } catch (p e) {
            c.a(e);
            xMPushService.a(10, e);
        }
    }

    public static void a(String str, byte[] bArr) {
        synchronized (f2513a) {
            f2513a.put(str, bArr);
        }
    }

    public static void b(XMPushService xMPushService) {
        ArrayList<Pair<String, byte[]>> arrayList;
        try {
            synchronized (b) {
                arrayList = b;
                b = new ArrayList<>();
            }
            Iterator<Pair<String, byte[]>> it = arrayList.iterator();
            while (it.hasNext()) {
                Pair next = it.next();
                xMPushService.a((String) next.first, (byte[]) next.second);
            }
        } catch (p e) {
            c.a(e);
            xMPushService.a(10, e);
        }
    }

    public static void b(String str, byte[] bArr) {
        synchronized (b) {
            b.add(new Pair(str, bArr));
            if (b.size() > 50) {
                b.remove(0);
            }
        }
    }
}
