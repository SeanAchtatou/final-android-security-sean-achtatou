package com.xiaomi.push.service;

import cn.banshenggua.aichang.utils.Constants;
import com.qq.e.comm.constants.ErrorCode;
import com.xiaomi.a.a.c.c;
import com.xiaomi.push.service.XMPushService;

class f {
    private static int e = ErrorCode.InitError.INIT_AD_ERROR;

    /* renamed from: a  reason: collision with root package name */
    private XMPushService f2534a;
    private int b;
    private long c;
    private int d = 0;

    public f(XMPushService xMPushService) {
        this.f2534a = xMPushService;
        this.b = 10;
        this.c = 0;
    }

    private int b() {
        int i = 40;
        if (this.d > 8) {
            return ErrorCode.InitError.INIT_AD_ERROR;
        }
        if (this.d > 4) {
            return 60;
        }
        if (this.d >= 1) {
            return 10;
        }
        if (this.c == 0) {
            return 0;
        }
        long currentTimeMillis = System.currentTimeMillis() - this.c;
        if (currentTimeMillis < 300000) {
            if (this.b >= e) {
                return this.b;
            }
            int i2 = this.b;
            this.b = (int) (((double) this.b) * 1.5d);
            return i2;
        } else if (currentTimeMillis < 900000) {
            if (this.b < 40) {
                i = this.b;
            }
            this.b = i;
            return this.b;
        } else if (currentTimeMillis < 1800000) {
            this.b = this.b < 20 ? this.b : 20;
            return this.b;
        } else {
            this.b = 10;
            return this.b;
        }
    }

    public void a() {
        this.c = System.currentTimeMillis();
        this.f2534a.a(1);
        this.d = 0;
    }

    public void a(boolean z) {
        if (!this.f2534a.a()) {
            c.c("should not reconnect as no client or network.");
        } else if (z) {
            this.f2534a.a(1);
            XMPushService xMPushService = this.f2534a;
            XMPushService xMPushService2 = this.f2534a;
            xMPushService2.getClass();
            xMPushService.a(new XMPushService.c());
            this.d++;
        } else if (!this.f2534a.b(1)) {
            int b2 = b();
            c.a("schedule reconnect in " + b2 + "s");
            XMPushService xMPushService3 = this.f2534a;
            XMPushService xMPushService4 = this.f2534a;
            xMPushService4.getClass();
            xMPushService3.a(new XMPushService.c(), (long) (b2 * Constants.CLEARIMGED));
            this.d++;
            if (this.d == 3) {
                am.a();
            }
        }
    }
}
