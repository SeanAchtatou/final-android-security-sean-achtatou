package com.xiaomi.push.service;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.xiaomi.a.a.c.c;
import com.xiaomi.d.c.b;
import com.xiaomi.d.c.d;
import com.xiaomi.d.c.f;
import com.xiaomi.push.service.ao;

public class t {

    /* renamed from: a  reason: collision with root package name */
    private ad f2550a = new ad();

    public static String a(String str) {
        return str + ".permission.MIPUSH_RECEIVE";
    }

    private static void a(Context context, Intent intent, String str) {
        if ("com.xiaomi.xmsf".equals(context.getPackageName())) {
            context.sendBroadcast(intent);
        } else {
            context.sendBroadcast(intent, a(str));
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0035  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.xiaomi.push.service.ao.b a(com.xiaomi.d.c.d r7) {
        /*
            r6 = this;
            r1 = 0
            com.xiaomi.push.service.ao r0 = com.xiaomi.push.service.ao.a()
            java.lang.String r2 = r7.l()
            java.util.Collection r0 = r0.c(r2)
            boolean r2 = r0.isEmpty()
            if (r2 == 0) goto L_0x0015
            r0 = r1
        L_0x0014:
            return r0
        L_0x0015:
            java.util.Iterator r2 = r0.iterator()
            int r0 = r0.size()
            r3 = 1
            if (r0 != r3) goto L_0x0027
            java.lang.Object r0 = r2.next()
            com.xiaomi.push.service.ao$b r0 = (com.xiaomi.push.service.ao.b) r0
            goto L_0x0014
        L_0x0027:
            java.lang.String r3 = r7.n()
            java.lang.String r4 = r7.m()
        L_0x002f:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x004c
            java.lang.Object r0 = r2.next()
            com.xiaomi.push.service.ao$b r0 = (com.xiaomi.push.service.ao.b) r0
            java.lang.String r5 = r0.b
            boolean r5 = android.text.TextUtils.equals(r3, r5)
            if (r5 != 0) goto L_0x0014
            java.lang.String r5 = r0.b
            boolean r5 = android.text.TextUtils.equals(r4, r5)
            if (r5 == 0) goto L_0x002f
            goto L_0x0014
        L_0x004c:
            r0 = r1
            goto L_0x0014
        */
        throw new UnsupportedOperationException("Method not decompiled: com.xiaomi.push.service.t.a(com.xiaomi.d.c.d):com.xiaomi.push.service.ao$b");
    }

    public void a(Context context) {
        Intent intent = new Intent();
        intent.setAction("com.xiaomi.push.service_started");
        context.sendBroadcast(intent);
    }

    public void a(Context context, ao.b bVar, int i) {
        if (!"5".equalsIgnoreCase(bVar.h)) {
            Intent intent = new Intent();
            intent.setAction("com.xiaomi.push.channel_closed");
            intent.setPackage(bVar.f2525a);
            intent.putExtra(aq.q, bVar.h);
            intent.putExtra("ext_reason", i);
            intent.putExtra(aq.p, bVar.b);
            intent.putExtra(aq.B, bVar.j);
            a(context, intent, bVar.f2525a);
        }
    }

    public void a(Context context, ao.b bVar, String str, String str2) {
        if ("5".equalsIgnoreCase(bVar.h)) {
            c.d("mipush kicked by server");
            return;
        }
        Intent intent = new Intent();
        intent.setAction("com.xiaomi.push.kicked");
        intent.setPackage(bVar.f2525a);
        intent.putExtra("ext_kick_type", str);
        intent.putExtra("ext_kick_reason", str2);
        intent.putExtra("ext_chid", bVar.h);
        intent.putExtra(aq.p, bVar.b);
        intent.putExtra(aq.B, bVar.j);
        a(context, intent, bVar.f2525a);
    }

    public void a(Context context, ao.b bVar, boolean z, int i, String str) {
        if ("5".equalsIgnoreCase(bVar.h)) {
            this.f2550a.a(context, bVar, z, i, str);
            return;
        }
        Intent intent = new Intent();
        intent.setAction("com.xiaomi.push.channel_opened");
        intent.setPackage(bVar.f2525a);
        intent.putExtra("ext_succeeded", z);
        if (!z) {
            intent.putExtra("ext_reason", i);
        }
        if (!TextUtils.isEmpty(str)) {
            intent.putExtra("ext_reason_msg", str);
        }
        intent.putExtra("ext_chid", bVar.h);
        intent.putExtra(aq.p, bVar.b);
        intent.putExtra(aq.B, bVar.j);
        a(context, intent, bVar.f2525a);
    }

    public void a(XMPushService xMPushService, String str, d dVar) {
        String str2;
        ao.b a2 = a(dVar);
        if (a2 == null) {
            c.d("error while notify channel closed! channel " + str + " not registered");
        } else if ("5".equalsIgnoreCase(str)) {
            this.f2550a.a(xMPushService, dVar, a2);
        } else {
            String str3 = a2.f2525a;
            if (dVar instanceof com.xiaomi.d.c.c) {
                str2 = "com.xiaomi.push.new_msg";
            } else if (dVar instanceof b) {
                str2 = "com.xiaomi.push.new_iq";
            } else if (dVar instanceof f) {
                str2 = "com.xiaomi.push.new_pres";
            } else {
                c.d("unknown packet type, drop it");
                return;
            }
            Intent intent = new Intent();
            intent.setAction(str2);
            intent.setPackage(str3);
            intent.putExtra("ext_chid", str);
            intent.putExtra("ext_packet", dVar.c());
            intent.putExtra(aq.B, a2.j);
            intent.putExtra(aq.u, a2.i);
            a(xMPushService, intent, str3);
        }
    }
}
