package com.xiaomi.push.service;

import android.util.Base64;
import com.xiaomi.a.a.d.b;
import com.xiaomi.a.a.e.c;
import com.xiaomi.d.e.h;
import com.xiaomi.push.protobuf.a;
import com.xiaomi.push.service.h;
import java.util.List;

class i extends b.C0054b {

    /* renamed from: a  reason: collision with root package name */
    boolean f2537a = false;
    final /* synthetic */ h b;

    i(h hVar) {
        this.b = hVar;
    }

    public void b() {
        try {
            a.C0060a b2 = a.C0060a.b(Base64.decode(com.xiaomi.network.i.a(h.a(), "http://resolver.msg.xiaomi.net/psc/?t=a", (List<c>) null), 10));
            if (b2 != null) {
                a.C0060a unused = this.b.b = b2;
                this.f2537a = true;
                this.b.g();
            }
        } catch (Exception e) {
            com.xiaomi.a.a.c.c.a("fetch config failure: " + e.getMessage());
        }
    }

    public void c() {
        h.a[] aVarArr;
        b.C0054b unused = this.b.c = (b.C0054b) null;
        if (this.f2537a) {
            synchronized (this.b) {
                aVarArr = (h.a[]) this.b.f2536a.toArray(new h.a[this.b.f2536a.size()]);
            }
            for (h.a a2 : aVarArr) {
                a2.a(this.b.b);
            }
        }
    }
}
