package com.xiaomi.push.c;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.a.a.d;
import org.apache.a.a.g;
import org.apache.a.b;
import org.apache.a.b.f;
import org.apache.a.b.i;
import org.apache.a.b.k;

public class c implements Serializable, Cloneable, b<c, a> {
    public static final Map<a, org.apache.a.a.b> d;
    private static final k e = new k("StatsEvents");
    private static final org.apache.a.b.c f = new org.apache.a.b.c("uuid", (byte) 11, 1);
    private static final org.apache.a.b.c g = new org.apache.a.b.c("operator", (byte) 11, 2);
    private static final org.apache.a.b.c h = new org.apache.a.b.c("events", (byte) 15, 3);

    /* renamed from: a  reason: collision with root package name */
    public String f2493a;
    public String b;
    public List<b> c;

    public enum a {
        UUID(1, "uuid"),
        OPERATOR(2, "operator"),
        EVENTS(3, "events");
        
        private static final Map<String, a> d = new HashMap();
        private final short e;
        private final String f;

        static {
            Iterator it = EnumSet.allOf(a.class).iterator();
            while (it.hasNext()) {
                a aVar = (a) it.next();
                d.put(aVar.a(), aVar);
            }
        }

        private a(short s, String str) {
            this.e = s;
            this.f = str;
        }

        public String a() {
            return this.f;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [com.xiaomi.push.c.c$a, org.apache.a.a.b]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        EnumMap enumMap = new EnumMap(a.class);
        enumMap.put((Object) a.UUID, (Object) new org.apache.a.a.b("uuid", (byte) 1, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.OPERATOR, (Object) new org.apache.a.a.b("operator", (byte) 2, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.EVENTS, (Object) new org.apache.a.a.b("events", (byte) 1, new d((byte) 15, new g((byte) 12, b.class))));
        d = Collections.unmodifiableMap(enumMap);
        org.apache.a.a.b.a(c.class, d);
    }

    public c() {
    }

    public c(String str, List<b> list) {
        this();
        this.f2493a = str;
        this.c = list;
    }

    public c a(String str) {
        this.b = str;
        return this;
    }

    public void a(f fVar) {
        fVar.g();
        while (true) {
            org.apache.a.b.c i = fVar.i();
            if (i.b == 0) {
                fVar.h();
                d();
                return;
            }
            switch (i.c) {
                case 1:
                    if (i.b != 11) {
                        i.a(fVar, i.b);
                        break;
                    } else {
                        this.f2493a = fVar.w();
                        break;
                    }
                case 2:
                    if (i.b != 11) {
                        i.a(fVar, i.b);
                        break;
                    } else {
                        this.b = fVar.w();
                        break;
                    }
                case 3:
                    if (i.b != 15) {
                        i.a(fVar, i.b);
                        break;
                    } else {
                        org.apache.a.b.d m = fVar.m();
                        this.c = new ArrayList(m.b);
                        for (int i2 = 0; i2 < m.b; i2++) {
                            b bVar = new b();
                            bVar.a(fVar);
                            this.c.add(bVar);
                        }
                        fVar.n();
                        break;
                    }
                default:
                    i.a(fVar, i.b);
                    break;
            }
            fVar.j();
        }
    }

    public boolean a() {
        return this.f2493a != null;
    }

    public boolean a(c cVar) {
        if (cVar == null) {
            return false;
        }
        boolean a2 = a();
        boolean a3 = cVar.a();
        if ((a2 || a3) && (!a2 || !a3 || !this.f2493a.equals(cVar.f2493a))) {
            return false;
        }
        boolean b2 = b();
        boolean b3 = cVar.b();
        if ((b2 || b3) && (!b2 || !b3 || !this.b.equals(cVar.b))) {
            return false;
        }
        boolean c2 = c();
        boolean c3 = cVar.c();
        return (!c2 && !c3) || (c2 && c3 && this.c.equals(cVar.c));
    }

    /* renamed from: b */
    public int compareTo(c cVar) {
        int a2;
        int a3;
        int a4;
        if (!getClass().equals(cVar.getClass())) {
            return getClass().getName().compareTo(cVar.getClass().getName());
        }
        int compareTo = Boolean.valueOf(a()).compareTo(Boolean.valueOf(cVar.a()));
        if (compareTo != 0) {
            return compareTo;
        }
        if (a() && (a4 = org.apache.a.c.a(this.f2493a, cVar.f2493a)) != 0) {
            return a4;
        }
        int compareTo2 = Boolean.valueOf(b()).compareTo(Boolean.valueOf(cVar.b()));
        if (compareTo2 != 0) {
            return compareTo2;
        }
        if (b() && (a3 = org.apache.a.c.a(this.b, cVar.b)) != 0) {
            return a3;
        }
        int compareTo3 = Boolean.valueOf(c()).compareTo(Boolean.valueOf(cVar.c()));
        if (compareTo3 != 0) {
            return compareTo3;
        }
        if (!c() || (a2 = org.apache.a.c.a(this.c, cVar.c)) == 0) {
            return 0;
        }
        return a2;
    }

    public void b(f fVar) {
        d();
        fVar.a(e);
        if (this.f2493a != null) {
            fVar.a(f);
            fVar.a(this.f2493a);
            fVar.b();
        }
        if (this.b != null && b()) {
            fVar.a(g);
            fVar.a(this.b);
            fVar.b();
        }
        if (this.c != null) {
            fVar.a(h);
            fVar.a(new org.apache.a.b.d((byte) 12, this.c.size()));
            for (b b2 : this.c) {
                b2.b(fVar);
            }
            fVar.e();
            fVar.b();
        }
        fVar.c();
        fVar.a();
    }

    public boolean b() {
        return this.b != null;
    }

    public boolean c() {
        return this.c != null;
    }

    public void d() {
        if (this.f2493a == null) {
            throw new org.apache.a.b.g("Required field 'uuid' was not present! Struct: " + toString());
        } else if (this.c == null) {
            throw new org.apache.a.b.g("Required field 'events' was not present! Struct: " + toString());
        }
    }

    public boolean equals(Object obj) {
        if (obj != null && (obj instanceof c)) {
            return a((c) obj);
        }
        return false;
    }

    public int hashCode() {
        return 0;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("StatsEvents(");
        sb.append("uuid:");
        if (this.f2493a == null) {
            sb.append("null");
        } else {
            sb.append(this.f2493a);
        }
        if (b()) {
            sb.append(", ");
            sb.append("operator:");
            if (this.b == null) {
                sb.append("null");
            } else {
                sb.append(this.b);
            }
        }
        sb.append(", ");
        sb.append("events:");
        if (this.c == null) {
            sb.append("null");
        } else {
            sb.append(this.c);
        }
        sb.append(")");
        return sb.toString();
    }
}
