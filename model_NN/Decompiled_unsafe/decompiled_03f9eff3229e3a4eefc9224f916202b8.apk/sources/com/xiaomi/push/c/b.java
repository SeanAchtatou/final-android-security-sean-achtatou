package com.xiaomi.push.c;

import java.io.Serializable;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.apache.a.b.c;
import org.apache.a.b.f;
import org.apache.a.b.g;
import org.apache.a.b.i;
import org.apache.a.b.k;

public class b implements Serializable, Cloneable, org.apache.a.b<b, a> {
    public static final Map<a, org.apache.a.a.b> k;
    private static final k l = new k("StatsEvent");
    private static final c m = new c("chid", (byte) 3, 1);
    private static final c n = new c("type", (byte) 8, 2);
    private static final c o = new c("value", (byte) 8, 3);
    private static final c p = new c("connpt", (byte) 11, 4);
    private static final c q = new c("host", (byte) 11, 5);
    private static final c r = new c("subvalue", (byte) 8, 6);
    private static final c s = new c("annotation", (byte) 11, 7);
    private static final c t = new c("user", (byte) 11, 8);
    private static final c u = new c("time", (byte) 8, 9);
    private static final c v = new c("clientIp", (byte) 8, 10);

    /* renamed from: a  reason: collision with root package name */
    public byte f2491a;
    public int b;
    public int c;
    public String d;
    public String e;
    public int f;
    public String g;
    public String h;
    public int i;
    public int j;
    private BitSet w = new BitSet(6);

    public enum a {
        CHID(1, "chid"),
        TYPE(2, "type"),
        VALUE(3, "value"),
        CONNPT(4, "connpt"),
        HOST(5, "host"),
        SUBVALUE(6, "subvalue"),
        ANNOTATION(7, "annotation"),
        USER(8, "user"),
        TIME(9, "time"),
        CLIENT_IP(10, "clientIp");
        
        private static final Map<String, a> k = new HashMap();
        private final short l;
        private final String m;

        static {
            Iterator it = EnumSet.allOf(a.class).iterator();
            while (it.hasNext()) {
                a aVar = (a) it.next();
                k.put(aVar.a(), aVar);
            }
        }

        private a(short s, String str) {
            this.l = s;
            this.m = str;
        }

        public String a() {
            return this.m;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [com.xiaomi.push.c.b$a, org.apache.a.a.b]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        EnumMap enumMap = new EnumMap(a.class);
        enumMap.put((Object) a.CHID, (Object) new org.apache.a.a.b("chid", (byte) 1, new org.apache.a.a.c((byte) 3)));
        enumMap.put((Object) a.TYPE, (Object) new org.apache.a.a.b("type", (byte) 1, new org.apache.a.a.c((byte) 8)));
        enumMap.put((Object) a.VALUE, (Object) new org.apache.a.a.b("value", (byte) 1, new org.apache.a.a.c((byte) 8)));
        enumMap.put((Object) a.CONNPT, (Object) new org.apache.a.a.b("connpt", (byte) 1, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.HOST, (Object) new org.apache.a.a.b("host", (byte) 2, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.SUBVALUE, (Object) new org.apache.a.a.b("subvalue", (byte) 2, new org.apache.a.a.c((byte) 8)));
        enumMap.put((Object) a.ANNOTATION, (Object) new org.apache.a.a.b("annotation", (byte) 2, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.USER, (Object) new org.apache.a.a.b("user", (byte) 2, new org.apache.a.a.c((byte) 11)));
        enumMap.put((Object) a.TIME, (Object) new org.apache.a.a.b("time", (byte) 2, new org.apache.a.a.c((byte) 8)));
        enumMap.put((Object) a.CLIENT_IP, (Object) new org.apache.a.a.b("clientIp", (byte) 2, new org.apache.a.a.c((byte) 8)));
        k = Collections.unmodifiableMap(enumMap);
        org.apache.a.a.b.a(b.class, k);
    }

    public b a(byte b2) {
        this.f2491a = b2;
        a(true);
        return this;
    }

    public b a(int i2) {
        this.b = i2;
        b(true);
        return this;
    }

    public b a(String str) {
        this.d = str;
        return this;
    }

    public void a(f fVar) {
        fVar.g();
        while (true) {
            c i2 = fVar.i();
            if (i2.b == 0) {
                fVar.h();
                if (!a()) {
                    throw new g("Required field 'chid' was not found in serialized data! Struct: " + toString());
                } else if (!b()) {
                    throw new g("Required field 'type' was not found in serialized data! Struct: " + toString());
                } else if (!c()) {
                    throw new g("Required field 'value' was not found in serialized data! Struct: " + toString());
                } else {
                    k();
                    return;
                }
            } else {
                switch (i2.c) {
                    case 1:
                        if (i2.b != 3) {
                            i.a(fVar, i2.b);
                            break;
                        } else {
                            this.f2491a = fVar.r();
                            a(true);
                            break;
                        }
                    case 2:
                        if (i2.b != 8) {
                            i.a(fVar, i2.b);
                            break;
                        } else {
                            this.b = fVar.t();
                            b(true);
                            break;
                        }
                    case 3:
                        if (i2.b != 8) {
                            i.a(fVar, i2.b);
                            break;
                        } else {
                            this.c = fVar.t();
                            c(true);
                            break;
                        }
                    case 4:
                        if (i2.b != 11) {
                            i.a(fVar, i2.b);
                            break;
                        } else {
                            this.d = fVar.w();
                            break;
                        }
                    case 5:
                        if (i2.b != 11) {
                            i.a(fVar, i2.b);
                            break;
                        } else {
                            this.e = fVar.w();
                            break;
                        }
                    case 6:
                        if (i2.b != 8) {
                            i.a(fVar, i2.b);
                            break;
                        } else {
                            this.f = fVar.t();
                            d(true);
                            break;
                        }
                    case 7:
                        if (i2.b != 11) {
                            i.a(fVar, i2.b);
                            break;
                        } else {
                            this.g = fVar.w();
                            break;
                        }
                    case 8:
                        if (i2.b != 11) {
                            i.a(fVar, i2.b);
                            break;
                        } else {
                            this.h = fVar.w();
                            break;
                        }
                    case 9:
                        if (i2.b != 8) {
                            i.a(fVar, i2.b);
                            break;
                        } else {
                            this.i = fVar.t();
                            e(true);
                            break;
                        }
                    case 10:
                        if (i2.b != 8) {
                            i.a(fVar, i2.b);
                            break;
                        } else {
                            this.j = fVar.t();
                            f(true);
                            break;
                        }
                    default:
                        i.a(fVar, i2.b);
                        break;
                }
                fVar.j();
            }
        }
    }

    public void a(boolean z) {
        this.w.set(0, z);
    }

    public boolean a() {
        return this.w.get(0);
    }

    public boolean a(b bVar) {
        if (bVar == null || this.f2491a != bVar.f2491a || this.b != bVar.b || this.c != bVar.c) {
            return false;
        }
        boolean d2 = d();
        boolean d3 = bVar.d();
        if ((d2 || d3) && (!d2 || !d3 || !this.d.equals(bVar.d))) {
            return false;
        }
        boolean e2 = e();
        boolean e3 = bVar.e();
        if ((e2 || e3) && (!e2 || !e3 || !this.e.equals(bVar.e))) {
            return false;
        }
        boolean f2 = f();
        boolean f3 = bVar.f();
        if ((f2 || f3) && (!f2 || !f3 || this.f != bVar.f)) {
            return false;
        }
        boolean g2 = g();
        boolean g3 = bVar.g();
        if ((g2 || g3) && (!g2 || !g3 || !this.g.equals(bVar.g))) {
            return false;
        }
        boolean h2 = h();
        boolean h3 = bVar.h();
        if ((h2 || h3) && (!h2 || !h3 || !this.h.equals(bVar.h))) {
            return false;
        }
        boolean i2 = i();
        boolean i3 = bVar.i();
        if ((i2 || i3) && (!i2 || !i3 || this.i != bVar.i)) {
            return false;
        }
        boolean j2 = j();
        boolean j3 = bVar.j();
        return (!j2 && !j3) || (j2 && j3 && this.j == bVar.j);
    }

    /* renamed from: b */
    public int compareTo(b bVar) {
        int a2;
        int a3;
        int a4;
        int a5;
        int a6;
        int a7;
        int a8;
        int a9;
        int a10;
        int a11;
        if (!getClass().equals(bVar.getClass())) {
            return getClass().getName().compareTo(bVar.getClass().getName());
        }
        int compareTo = Boolean.valueOf(a()).compareTo(Boolean.valueOf(bVar.a()));
        if (compareTo != 0) {
            return compareTo;
        }
        if (a() && (a11 = org.apache.a.c.a(this.f2491a, bVar.f2491a)) != 0) {
            return a11;
        }
        int compareTo2 = Boolean.valueOf(b()).compareTo(Boolean.valueOf(bVar.b()));
        if (compareTo2 != 0) {
            return compareTo2;
        }
        if (b() && (a10 = org.apache.a.c.a(this.b, bVar.b)) != 0) {
            return a10;
        }
        int compareTo3 = Boolean.valueOf(c()).compareTo(Boolean.valueOf(bVar.c()));
        if (compareTo3 != 0) {
            return compareTo3;
        }
        if (c() && (a9 = org.apache.a.c.a(this.c, bVar.c)) != 0) {
            return a9;
        }
        int compareTo4 = Boolean.valueOf(d()).compareTo(Boolean.valueOf(bVar.d()));
        if (compareTo4 != 0) {
            return compareTo4;
        }
        if (d() && (a8 = org.apache.a.c.a(this.d, bVar.d)) != 0) {
            return a8;
        }
        int compareTo5 = Boolean.valueOf(e()).compareTo(Boolean.valueOf(bVar.e()));
        if (compareTo5 != 0) {
            return compareTo5;
        }
        if (e() && (a7 = org.apache.a.c.a(this.e, bVar.e)) != 0) {
            return a7;
        }
        int compareTo6 = Boolean.valueOf(f()).compareTo(Boolean.valueOf(bVar.f()));
        if (compareTo6 != 0) {
            return compareTo6;
        }
        if (f() && (a6 = org.apache.a.c.a(this.f, bVar.f)) != 0) {
            return a6;
        }
        int compareTo7 = Boolean.valueOf(g()).compareTo(Boolean.valueOf(bVar.g()));
        if (compareTo7 != 0) {
            return compareTo7;
        }
        if (g() && (a5 = org.apache.a.c.a(this.g, bVar.g)) != 0) {
            return a5;
        }
        int compareTo8 = Boolean.valueOf(h()).compareTo(Boolean.valueOf(bVar.h()));
        if (compareTo8 != 0) {
            return compareTo8;
        }
        if (h() && (a4 = org.apache.a.c.a(this.h, bVar.h)) != 0) {
            return a4;
        }
        int compareTo9 = Boolean.valueOf(i()).compareTo(Boolean.valueOf(bVar.i()));
        if (compareTo9 != 0) {
            return compareTo9;
        }
        if (i() && (a3 = org.apache.a.c.a(this.i, bVar.i)) != 0) {
            return a3;
        }
        int compareTo10 = Boolean.valueOf(j()).compareTo(Boolean.valueOf(bVar.j()));
        if (compareTo10 != 0) {
            return compareTo10;
        }
        if (!j() || (a2 = org.apache.a.c.a(this.j, bVar.j)) == 0) {
            return 0;
        }
        return a2;
    }

    public b b(int i2) {
        this.c = i2;
        c(true);
        return this;
    }

    public b b(String str) {
        this.e = str;
        return this;
    }

    public void b(f fVar) {
        k();
        fVar.a(l);
        fVar.a(m);
        fVar.a(this.f2491a);
        fVar.b();
        fVar.a(n);
        fVar.a(this.b);
        fVar.b();
        fVar.a(o);
        fVar.a(this.c);
        fVar.b();
        if (this.d != null) {
            fVar.a(p);
            fVar.a(this.d);
            fVar.b();
        }
        if (this.e != null && e()) {
            fVar.a(q);
            fVar.a(this.e);
            fVar.b();
        }
        if (f()) {
            fVar.a(r);
            fVar.a(this.f);
            fVar.b();
        }
        if (this.g != null && g()) {
            fVar.a(s);
            fVar.a(this.g);
            fVar.b();
        }
        if (this.h != null && h()) {
            fVar.a(t);
            fVar.a(this.h);
            fVar.b();
        }
        if (i()) {
            fVar.a(u);
            fVar.a(this.i);
            fVar.b();
        }
        if (j()) {
            fVar.a(v);
            fVar.a(this.j);
            fVar.b();
        }
        fVar.c();
        fVar.a();
    }

    public void b(boolean z) {
        this.w.set(1, z);
    }

    public boolean b() {
        return this.w.get(1);
    }

    public b c(int i2) {
        this.f = i2;
        d(true);
        return this;
    }

    public b c(String str) {
        this.g = str;
        return this;
    }

    public void c(boolean z) {
        this.w.set(2, z);
    }

    public boolean c() {
        return this.w.get(2);
    }

    public b d(int i2) {
        this.i = i2;
        e(true);
        return this;
    }

    public b d(String str) {
        this.h = str;
        return this;
    }

    public void d(boolean z) {
        this.w.set(3, z);
    }

    public boolean d() {
        return this.d != null;
    }

    public b e(int i2) {
        this.j = i2;
        f(true);
        return this;
    }

    public void e(boolean z) {
        this.w.set(4, z);
    }

    public boolean e() {
        return this.e != null;
    }

    public boolean equals(Object obj) {
        if (obj != null && (obj instanceof b)) {
            return a((b) obj);
        }
        return false;
    }

    public void f(boolean z) {
        this.w.set(5, z);
    }

    public boolean f() {
        return this.w.get(3);
    }

    public boolean g() {
        return this.g != null;
    }

    public boolean h() {
        return this.h != null;
    }

    public int hashCode() {
        return 0;
    }

    public boolean i() {
        return this.w.get(4);
    }

    public boolean j() {
        return this.w.get(5);
    }

    public void k() {
        if (this.d == null) {
            throw new g("Required field 'connpt' was not present! Struct: " + toString());
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("StatsEvent(");
        sb.append("chid:");
        sb.append((int) this.f2491a);
        sb.append(", ");
        sb.append("type:");
        sb.append(this.b);
        sb.append(", ");
        sb.append("value:");
        sb.append(this.c);
        sb.append(", ");
        sb.append("connpt:");
        if (this.d == null) {
            sb.append("null");
        } else {
            sb.append(this.d);
        }
        if (e()) {
            sb.append(", ");
            sb.append("host:");
            if (this.e == null) {
                sb.append("null");
            } else {
                sb.append(this.e);
            }
        }
        if (f()) {
            sb.append(", ");
            sb.append("subvalue:");
            sb.append(this.f);
        }
        if (g()) {
            sb.append(", ");
            sb.append("annotation:");
            if (this.g == null) {
                sb.append("null");
            } else {
                sb.append(this.g);
            }
        }
        if (h()) {
            sb.append(", ");
            sb.append("user:");
            if (this.h == null) {
                sb.append("null");
            } else {
                sb.append(this.h);
            }
        }
        if (i()) {
            sb.append(", ");
            sb.append("time:");
            sb.append(this.i);
        }
        if (j()) {
            sb.append(", ");
            sb.append("clientIp:");
            sb.append(this.j);
        }
        sb.append(")");
        return sb.toString();
    }
}
