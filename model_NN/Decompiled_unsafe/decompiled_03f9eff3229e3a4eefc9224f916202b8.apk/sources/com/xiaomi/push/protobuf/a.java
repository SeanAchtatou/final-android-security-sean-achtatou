package com.xiaomi.push.protobuf;

import com.google.protobuf.micro.b;
import com.google.protobuf.micro.d;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class a {

    /* renamed from: com.xiaomi.push.protobuf.a$a  reason: collision with other inner class name */
    public static final class C0060a extends d {

        /* renamed from: a  reason: collision with root package name */
        private boolean f2495a;
        private int b = 0;
        private boolean c;
        private boolean d = false;
        private boolean e;
        private int f = 0;
        private boolean g;
        private boolean h = false;
        private List<String> i = Collections.emptyList();
        private int j = -1;

        public static C0060a b(byte[] bArr) {
            return (C0060a) new C0060a().a(bArr);
        }

        public static C0060a c(com.google.protobuf.micro.a aVar) {
            return new C0060a().a(aVar);
        }

        public int a() {
            int i2 = 0;
            int d2 = d() ? b.d(1, c()) + 0 : 0;
            if (f()) {
                d2 += b.b(2, e());
            }
            if (h()) {
                d2 += b.c(3, g());
            }
            int b2 = j() ? d2 + b.b(4, i()) : d2;
            for (String b3 : k()) {
                i2 += b.b(b3);
            }
            int size = b2 + i2 + (k().size() * 1);
            this.j = size;
            return size;
        }

        public C0060a a(int i2) {
            this.f2495a = true;
            this.b = i2;
            return this;
        }

        public C0060a a(String str) {
            if (str == null) {
                throw new NullPointerException();
            }
            if (this.i.isEmpty()) {
                this.i = new ArrayList();
            }
            this.i.add(str);
            return this;
        }

        public C0060a a(boolean z) {
            this.c = true;
            this.d = z;
            return this;
        }

        public void a(b bVar) {
            if (d()) {
                bVar.b(1, c());
            }
            if (f()) {
                bVar.a(2, e());
            }
            if (h()) {
                bVar.a(3, g());
            }
            if (j()) {
                bVar.a(4, i());
            }
            for (String a2 : k()) {
                bVar.a(5, a2);
            }
        }

        public C0060a b(int i2) {
            this.e = true;
            this.f = i2;
            return this;
        }

        /* renamed from: b */
        public C0060a a(com.google.protobuf.micro.a aVar) {
            while (true) {
                int a2 = aVar.a();
                switch (a2) {
                    case 0:
                        break;
                    case 8:
                        a(aVar.f());
                        break;
                    case 16:
                        a(aVar.d());
                        break;
                    case D.QHINTERSTITIALAD_showAds:
                        b(aVar.c());
                        break;
                    case 32:
                        b(aVar.d());
                        break;
                    case D.QHVIDEOAD_onAdClicked:
                        a(aVar.e());
                        break;
                    default:
                        if (a(aVar, a2)) {
                            break;
                        } else {
                            break;
                        }
                }
            }
            return this;
        }

        public C0060a b(boolean z) {
            this.g = true;
            this.h = z;
            return this;
        }

        public int c() {
            return this.b;
        }

        public boolean d() {
            return this.f2495a;
        }

        public boolean e() {
            return this.d;
        }

        public boolean f() {
            return this.c;
        }

        public int g() {
            return this.f;
        }

        public boolean h() {
            return this.e;
        }

        public boolean i() {
            return this.h;
        }

        public boolean j() {
            return this.g;
        }

        public List<String> k() {
            return this.i;
        }

        public int l() {
            return this.i.size();
        }
    }
}
