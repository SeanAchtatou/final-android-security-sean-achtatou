package com.xiaomi.d;

import android.text.TextUtils;
import cn.banshenggua.aichang.room.message.SocketMessage;
import com.tencent.connect.common.Constants;
import com.xiaomi.a.a.c.c;
import com.xiaomi.d.c.d;
import com.xiaomi.d.c.h;
import com.xiaomi.d.e.g;
import com.xiaomi.push.service.ao;
import java.util.HashMap;

public class k {

    public class a extends d {
        public a(ao.b bVar, String str, a aVar) {
            String str2;
            String str3;
            HashMap hashMap = new HashMap();
            int i = aVar.i();
            hashMap.put("challenge", str);
            hashMap.put("token", bVar.c);
            hashMap.put("chid", bVar.h);
            hashMap.put("from", bVar.b);
            hashMap.put("id", k());
            hashMap.put("to", "xiaomi.com");
            if (bVar.e) {
                hashMap.put("kick", "1");
            } else {
                hashMap.put("kick", "0");
            }
            if (aVar.k() > 0) {
                String format = String.format("conn:%1$d,t:%2$d", Integer.valueOf(i), Long.valueOf(aVar.k()));
                hashMap.put(Constants.PARAM_PLATFORM_ID, format);
                aVar.j();
                aVar.l();
                str2 = format;
            } else {
                str2 = null;
            }
            if (!TextUtils.isEmpty(bVar.f)) {
                hashMap.put("client_attrs", bVar.f);
            } else {
                hashMap.put("client_attrs", "");
            }
            if (!TextUtils.isEmpty(bVar.g)) {
                hashMap.put("cloud_attrs", bVar.g);
            } else {
                hashMap.put("cloud_attrs", "");
            }
            if (bVar.d.equals("XIAOMI-PASS") || bVar.d.equals("XMPUSH-PASS")) {
                str3 = com.xiaomi.a.a.g.b.a(bVar.d, null, hashMap, bVar.i);
            } else {
                if (bVar.d.equals("XIAOMI-SASL")) {
                }
                str3 = null;
            }
            l(bVar.h);
            n(bVar.b);
            m("xiaomi.com");
            o(bVar.f2525a);
            com.xiaomi.d.c.a aVar2 = new com.xiaomi.d.c.a("token", null, null, null);
            aVar2.b(bVar.c);
            a(aVar2);
            com.xiaomi.d.c.a aVar3 = new com.xiaomi.d.c.a("kick", null, null, null);
            aVar3.b(bVar.e ? "1" : "0");
            a(aVar3);
            com.xiaomi.d.c.a aVar4 = new com.xiaomi.d.c.a("sig", null, null, null);
            aVar4.b(str3);
            a(aVar4);
            com.xiaomi.d.c.a aVar5 = new com.xiaomi.d.c.a("method", null, null, null);
            if (!TextUtils.isEmpty(bVar.d)) {
                aVar5.b(bVar.d);
            } else {
                aVar5.b("XIAOMI-SASL");
            }
            a(aVar5);
            com.xiaomi.d.c.a aVar6 = new com.xiaomi.d.c.a("client_attrs", null, null, null);
            aVar6.b(bVar.f == null ? "" : g.a(bVar.f));
            a(aVar6);
            com.xiaomi.d.c.a aVar7 = new com.xiaomi.d.c.a("cloud_attrs", null, null, null);
            aVar7.b(bVar.g == null ? "" : g.a(bVar.g));
            a(aVar7);
            if (!TextUtils.isEmpty(str2)) {
                com.xiaomi.d.c.a aVar8 = new com.xiaomi.d.c.a(Constants.PARAM_PLATFORM_ID, null, null, null);
                aVar8.b(str2);
                a(aVar8);
            }
        }

        public String a() {
            StringBuilder sb = new StringBuilder();
            sb.append("<bind ");
            if (k() != null) {
                sb.append("id=\"" + k() + "\" ");
            }
            if (m() != null) {
                sb.append("to=\"").append(g.a(m())).append("\" ");
            }
            if (n() != null) {
                sb.append("from=\"").append(g.a(n())).append("\" ");
            }
            if (l() != null) {
                sb.append("chid=\"").append(g.a(l())).append("\">");
            }
            if (q() != null) {
                for (com.xiaomi.d.c.a d : q()) {
                    sb.append(d.d());
                }
            }
            sb.append("</bind>");
            return sb.toString();
        }
    }

    public static class b extends d {

        /* renamed from: a  reason: collision with root package name */
        private a f2394a;

        public static class a {

            /* renamed from: a  reason: collision with root package name */
            public static final a f2395a = new a(SocketMessage.MSG_RESULE_KEY);
            public static final a b = new a(SocketMessage.MSG_ERROR_KEY);
            private String c;

            private a(String str) {
                this.c = str;
            }

            public static a a(String str) {
                if (str == null) {
                    return null;
                }
                String lowerCase = str.toLowerCase();
                if (b.toString().equals(lowerCase)) {
                    return b;
                }
                if (f2395a.toString().equals(lowerCase)) {
                    return f2395a;
                }
                return null;
            }

            public String toString() {
                return this.c;
            }
        }

        public String a() {
            StringBuilder sb = new StringBuilder();
            sb.append("<bind ");
            if (k() != null) {
                sb.append("id=\"" + k() + "\" ");
            }
            if (m() != null) {
                sb.append("to=\"").append(g.a(m())).append("\" ");
            }
            if (n() != null) {
                sb.append("from=\"").append(g.a(n())).append("\" ");
            }
            if (l() != null) {
                sb.append(" chid=\"").append(g.a(l())).append("\" ");
            }
            if (this.f2394a == null) {
                sb.append("type=\"result\">");
            } else {
                sb.append("type=\"").append(b()).append("\">");
            }
            if (q() != null) {
                for (com.xiaomi.d.c.a d : q()) {
                    sb.append(d.d());
                }
            }
            h p = p();
            if (p != null) {
                sb.append(p.d());
            }
            sb.append("</bind>");
            return sb.toString();
        }

        public void a(a aVar) {
            if (aVar == null) {
                this.f2394a = a.f2395a;
            } else {
                this.f2394a = aVar;
            }
        }

        public a b() {
            return this.f2394a;
        }
    }

    public void a(ao.b bVar, String str, a aVar) {
        a aVar2 = new a(bVar, str, aVar);
        aVar.a(aVar2);
        c.a("SMACK: bind id=" + aVar2.k());
    }
}
