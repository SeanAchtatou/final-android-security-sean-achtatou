package com.xiaomi.d.e;

import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

public class b extends Writer {

    /* renamed from: a  reason: collision with root package name */
    Writer f2380a = null;
    List b = new ArrayList();

    public b(Writer writer) {
        this.f2380a = writer;
    }

    private void a(String str) {
        m[] mVarArr;
        synchronized (this.b) {
            mVarArr = new m[this.b.size()];
            this.b.toArray(mVarArr);
        }
        for (m a2 : mVarArr) {
            a2.a(str);
        }
    }

    public void a(m mVar) {
        if (mVar != null) {
            synchronized (this.b) {
                if (!this.b.contains(mVar)) {
                    this.b.add(mVar);
                }
            }
        }
    }

    public void b(m mVar) {
        synchronized (this.b) {
            this.b.remove(mVar);
        }
    }

    public void close() {
        this.f2380a.close();
    }

    public void flush() {
        this.f2380a.flush();
    }

    public void write(int i) {
        this.f2380a.write(i);
    }

    public void write(String str) {
        this.f2380a.write(str);
        a(str);
    }

    public void write(String str, int i, int i2) {
        this.f2380a.write(str, i, i2);
        a(str.substring(i, i + i2));
    }

    public void write(char[] cArr) {
        this.f2380a.write(cArr);
        a(new String(cArr));
    }

    public void write(char[] cArr, int i, int i2) {
        this.f2380a.write(cArr, i, i2);
        a(new String(cArr, i, i2));
    }
}
