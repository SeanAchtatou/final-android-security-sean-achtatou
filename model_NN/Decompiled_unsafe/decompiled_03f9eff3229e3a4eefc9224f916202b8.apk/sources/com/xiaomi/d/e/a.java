package com.xiaomi.d.e;

import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

public class a extends Reader {

    /* renamed from: a  reason: collision with root package name */
    Reader f2379a = null;
    List b = new ArrayList();

    public a(Reader reader) {
        this.f2379a = reader;
    }

    public void a(f fVar) {
        if (fVar != null) {
            synchronized (this.b) {
                if (!this.b.contains(fVar)) {
                    this.b.add(fVar);
                }
            }
        }
    }

    public void b(f fVar) {
        synchronized (this.b) {
            this.b.remove(fVar);
        }
    }

    public void close() {
        this.f2379a.close();
    }

    public void mark(int i) {
        this.f2379a.mark(i);
    }

    public boolean markSupported() {
        return this.f2379a.markSupported();
    }

    public int read() {
        return this.f2379a.read();
    }

    public int read(char[] cArr) {
        return this.f2379a.read(cArr);
    }

    public int read(char[] cArr, int i, int i2) {
        f[] fVarArr;
        int read = this.f2379a.read(cArr, i, i2);
        if (read > 0) {
            String str = new String(cArr, i, read);
            synchronized (this.b) {
                fVarArr = new f[this.b.size()];
                this.b.toArray(fVarArr);
            }
            for (f a2 : fVarArr) {
                a2.a(str);
            }
        }
        return read;
    }

    public boolean ready() {
        return this.f2379a.ready();
    }

    public void reset() {
        this.f2379a.reset();
    }

    public long skip(long j) {
        return this.f2379a.skip(j);
    }
}
