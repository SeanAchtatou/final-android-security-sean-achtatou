package com.xiaomi.d.e;

import android.content.Context;
import android.util.Base64;
import com.xiaomi.a.a.c.c;
import com.xiaomi.push.service.x;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class h {

    /* renamed from: a  reason: collision with root package name */
    private static Context f2383a;
    private static String b;

    public static Context a() {
        return f2383a;
    }

    public static void a(Context context) {
        f2383a = context;
    }

    public static String b() {
        String b2;
        if (b == null && (b2 = x.b(f2383a)) != null) {
            try {
                b = Base64.encodeToString(MessageDigest.getInstance("SHA1").digest(b2.getBytes()), 8).substring(0, 16);
            } catch (NoSuchAlgorithmException e) {
                c.a(e);
            }
        }
        return b;
    }
}
