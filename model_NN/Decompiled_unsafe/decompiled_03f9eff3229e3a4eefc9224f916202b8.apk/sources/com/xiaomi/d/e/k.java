package com.xiaomi.d.e;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import com.xiaomi.a.a.c.c;
import com.xiaomi.a.a.d.b;
import com.xiaomi.push.service.XMPushService;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class k {

    /* renamed from: a  reason: collision with root package name */
    private static b f2386a = new b(true);
    private static int b = -1;
    /* access modifiers changed from: private */
    public static final Object c = new Object();
    /* access modifiers changed from: private */
    public static List<a> d = Collections.synchronizedList(new ArrayList());
    private static String e = "";
    private static com.xiaomi.push.b.a f = null;

    static class a {

        /* renamed from: a  reason: collision with root package name */
        public String f2387a = "";
        public long b = 0;
        public int c = -1;
        public int d = -1;
        public String e = "";
        public long f = 0;

        public a(String str, long j, int i, int i2, String str2, long j2) {
            this.f2387a = str;
            this.b = j;
            this.c = i;
            this.d = i2;
            this.e = str2;
            this.f = j2;
        }

        public boolean a(a aVar) {
            return TextUtils.equals(aVar.f2387a, this.f2387a) && TextUtils.equals(aVar.e, this.e) && aVar.c == this.c && aVar.d == this.d && Math.abs(aVar.b - this.b) <= 5000;
        }
    }

    private static int a(Context context) {
        if (b == -1) {
            b = b(context);
        }
        return b;
    }

    public static int a(String str) {
        try {
            return str.getBytes("UTF-8").length;
        } catch (UnsupportedEncodingException e2) {
            return str.getBytes().length;
        }
    }

    private static long a(int i, long j) {
        return (((long) (i == 0 ? 13 : 11)) * j) / 10;
    }

    private static void a(a aVar) {
        for (a next : d) {
            if (next.a(aVar)) {
                next.f += aVar.f;
                return;
            }
        }
        d.add(aVar);
    }

    public static void a(XMPushService xMPushService, String str, long j, boolean z, long j2) {
        int a2;
        boolean isEmpty;
        if (xMPushService != null && !TextUtils.isEmpty(str) && "com.xiaomi.xmsf".equals(xMPushService.getPackageName()) && !"com.xiaomi.xmsf".equals(str) && -1 != (a2 = a(xMPushService))) {
            synchronized (c) {
                isEmpty = d.isEmpty();
                a(new a(str, j2, a2, z ? 1 : 0, a2 == 0 ? c(xMPushService) : "", a(a2, j)));
            }
            if (isEmpty) {
                f2386a.a(new l(xMPushService), 5000);
            }
        }
    }

    private static int b(Context context) {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
            if (connectivityManager == null) {
                return -1;
            }
            try {
                NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                if (activeNetworkInfo == null) {
                    return -1;
                }
                return activeNetworkInfo.getType();
            } catch (Exception e2) {
                return -1;
            }
        } catch (Exception e3) {
            return -1;
        }
    }

    /* access modifiers changed from: private */
    public static void b(Context context, List<a> list) {
        try {
            synchronized (com.xiaomi.push.b.a.f2489a) {
                SQLiteDatabase writableDatabase = d(context).getWritableDatabase();
                writableDatabase.beginTransaction();
                try {
                    for (a next : list) {
                        ContentValues contentValues = new ContentValues();
                        contentValues.put("package_name", next.f2387a);
                        contentValues.put("message_ts", Long.valueOf(next.b));
                        contentValues.put("network_type", Integer.valueOf(next.c));
                        contentValues.put("bytes", Long.valueOf(next.f));
                        contentValues.put("rcv", Integer.valueOf(next.d));
                        contentValues.put("imsi", next.e);
                        writableDatabase.insert("traffic", null, contentValues);
                    }
                    writableDatabase.setTransactionSuccessful();
                    writableDatabase.endTransaction();
                } catch (Throwable th) {
                    writableDatabase.endTransaction();
                    throw th;
                }
            }
        } catch (SQLiteException e2) {
            c.a(e2);
        }
    }

    private static synchronized String c(Context context) {
        String str;
        synchronized (k.class) {
            if (!TextUtils.isEmpty(e)) {
                str = e;
            } else {
                try {
                    TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
                    if (telephonyManager != null) {
                        e = telephonyManager.getSubscriberId();
                    }
                } catch (Exception e2) {
                }
                str = e;
            }
        }
        return str;
    }

    private static com.xiaomi.push.b.a d(Context context) {
        if (f != null) {
            return f;
        }
        f = new com.xiaomi.push.b.a(context);
        return f;
    }
}
