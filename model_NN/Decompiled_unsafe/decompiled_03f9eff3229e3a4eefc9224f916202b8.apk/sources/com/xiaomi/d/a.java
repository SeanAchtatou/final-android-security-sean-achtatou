package com.xiaomi.d;

import android.util.Pair;
import com.xiaomi.a.a.c.c;
import com.xiaomi.d.c.d;
import com.xiaomi.d.c.f;
import com.xiaomi.push.service.XMPushService;
import com.xiaomi.push.service.ao;
import com.xiaomi.push.service.aq;
import java.io.Reader;
import java.io.Writer;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class a {

    /* renamed from: a  reason: collision with root package name */
    public static boolean f2364a;
    private static final AtomicInteger n = new AtomicInteger(0);
    protected int b = 0;
    protected long c = -1;
    protected int d;
    protected final Map<f, C0058a> e = new ConcurrentHashMap();
    protected final Map<f, C0058a> f = new ConcurrentHashMap();
    protected com.xiaomi.d.a.a g = null;
    protected Reader h;
    protected Writer i;
    protected String j = "";
    protected final int k = n.getAndIncrement();
    protected b l;
    protected XMPushService m;
    private LinkedList<Pair<Integer, Long>> o = new LinkedList<>();
    private final Collection<d> p = new CopyOnWriteArrayList();
    private int q = 2;
    private long r = 0;

    /* renamed from: com.xiaomi.d.a$a  reason: collision with other inner class name */
    protected static class C0058a {

        /* renamed from: a  reason: collision with root package name */
        private f f2365a;
        private com.xiaomi.d.b.a b;

        public C0058a(f fVar, com.xiaomi.d.b.a aVar) {
            this.f2365a = fVar;
            this.b = aVar;
        }

        public void a(d dVar) {
            if (this.b == null || this.b.a(dVar)) {
                this.f2365a.a(dVar);
            }
        }
    }

    static {
        f2364a = false;
        try {
            f2364a = Boolean.getBoolean("smack.debugEnabled");
        } catch (Exception e2) {
        }
        j.a();
    }

    protected a(XMPushService xMPushService, b bVar) {
        this.l = bVar;
        this.m = xMPushService;
    }

    private String a(int i2) {
        return i2 == 1 ? "connected" : i2 == 0 ? "connecting" : i2 == 2 ? "disconnected" : "unknown";
    }

    private void b(int i2) {
        synchronized (this.o) {
            if (i2 == 1) {
                this.o.clear();
            } else {
                this.o.add(new Pair(Integer.valueOf(i2), Long.valueOf(System.currentTimeMillis())));
                if (this.o.size() > 6) {
                    this.o.remove(0);
                }
            }
        }
    }

    public b a() {
        return this.l;
    }

    public void a(int i2, int i3, Exception exc) {
        if (i2 != this.q) {
            c.a(String.format("update the connection status. %1$s -> %2$s : %3$s ", a(this.q), a(i2), aq.a(i3)));
        }
        if (com.xiaomi.a.a.e.d.d(this.m)) {
            b(i2);
        }
        if (i2 == 1) {
            this.m.a(10);
            if (this.q != 0) {
                c.a("try set connected while not connecting.");
            }
            this.q = i2;
            for (d a2 : this.p) {
                a2.a(this);
            }
        } else if (i2 == 0) {
            this.m.h();
            if (this.q != 2) {
                c.a("try set connecting while not disconnected.");
            }
            this.q = i2;
            for (d b2 : this.p) {
                b2.b(this);
            }
        } else if (i2 == 2) {
            this.m.a(10);
            if (this.q == 0) {
                for (d a3 : this.p) {
                    a3.a(this, exc == null ? new CancellationException("disconnect while connecting") : exc);
                }
            } else if (this.q == 1) {
                for (d a4 : this.p) {
                    a4.a(this, i3, exc);
                }
            }
            this.q = i2;
        }
    }

    public abstract void a(d dVar);

    public abstract void a(f fVar, int i2, Exception exc);

    public void a(d dVar) {
        if (dVar != null && !this.p.contains(dVar)) {
            this.p.add(dVar);
        }
    }

    public void a(f fVar, com.xiaomi.d.b.a aVar) {
        if (fVar == null) {
            throw new NullPointerException("Packet listener is null.");
        }
        this.e.put(fVar, new C0058a(fVar, aVar));
    }

    public abstract void a(ao.b bVar);

    public void a(String str) {
        c.a("setChallenge hash = " + com.xiaomi.a.a.g.c.a(str).substring(0, 8));
        this.j = str;
        a(1, 0, (Exception) null);
    }

    public abstract void a(String str, String str2);

    public abstract void a(d[] dVarArr);

    public boolean a(long j2) {
        return this.r >= j2;
    }

    public String b() {
        return this.l.c();
    }

    /* access modifiers changed from: protected */
    public void b(d dVar) {
        for (C0058a a2 : this.f.values()) {
            a2.a(dVar);
        }
    }

    public void b(d dVar) {
        this.p.remove(dVar);
    }

    public void b(f fVar, com.xiaomi.d.b.a aVar) {
        if (fVar == null) {
            throw new NullPointerException("Packet listener is null.");
        }
        this.f.put(fVar, new C0058a(fVar, aVar));
    }

    public String c() {
        return this.l.f();
    }

    public String d() {
        return this.l.d();
    }

    public int e() {
        return this.d;
    }

    /* access modifiers changed from: protected */
    public void f() {
        String str;
        Class<?> cls = null;
        if (this.h != null && this.i != null && this.l.g()) {
            if (this.g == null) {
                try {
                    str = System.getProperty("smack.debuggerClass");
                } catch (Throwable th) {
                    str = null;
                }
                if (str != null) {
                    try {
                        cls = Class.forName(str);
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }
                if (cls == null) {
                    this.g = new com.xiaomi.c.a.a(this, this.i, this.h);
                    this.h = this.g.a();
                    this.i = this.g.b();
                    return;
                }
                try {
                    this.g = (com.xiaomi.d.a.a) cls.getConstructor(a.class, Writer.class, Reader.class).newInstance(this, this.i, this.h);
                    this.h = this.g.a();
                    this.i = this.g.b();
                } catch (Exception e3) {
                    throw new IllegalArgumentException("Can't initialize the configured debugger!", e3);
                }
            } else {
                this.h = this.g.a(this.h);
                this.i = this.g.a(this.i);
            }
        }
    }

    public boolean g() {
        return this.q == 0;
    }

    public boolean h() {
        return this.q == 1;
    }

    public int i() {
        return this.b;
    }

    public void j() {
        this.b = 0;
    }

    public long k() {
        return this.c;
    }

    public void l() {
        this.c = -1;
    }

    public abstract void m();

    public int n() {
        return this.q;
    }

    public void o() {
        this.r = System.currentTimeMillis();
    }

    public boolean p() {
        return System.currentTimeMillis() - this.r < ((long) j.b());
    }

    public void q() {
        synchronized (this.o) {
            this.o.clear();
        }
    }
}
