package com.xiaomi.d;

import cn.banshenggua.aichang.utils.Constants;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

public class c {
    public static int a(Throwable th) {
        Throwable a2 = (!(th instanceof p) || ((p) th).a() == null) ? th : ((p) th).a();
        String message = a2.getMessage();
        if (a2.getCause() != null) {
            message = a2.getCause().getMessage();
        }
        if (a2 instanceof SocketTimeoutException) {
            return 105;
        }
        if (a2 instanceof SocketException) {
            if (message.indexOf("Network is unreachable") != -1) {
                return Constants.REGISTER_OK;
            }
            if (message.indexOf("Connection refused") != -1) {
                return 103;
            }
            if (message.indexOf("Connection timed out") != -1) {
                return 105;
            }
            if (message.endsWith("EACCES (Permission denied)")) {
                return Constants.RESULT_OK;
            }
            if (message.indexOf("Connection reset by peer") != -1) {
                return 109;
            }
            if (message.indexOf("Broken pipe") != -1) {
                return 110;
            }
            if (message.indexOf("No route to host") != -1) {
                return 104;
            }
            return message.endsWith("EINVAL (Invalid argument)") ? 106 : 199;
        } else if (a2 instanceof UnknownHostException) {
            return 107;
        } else {
            return th instanceof p ? 399 : 0;
        }
    }
}
