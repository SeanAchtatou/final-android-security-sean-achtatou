package com.xiaomi.d.c;

import android.os.Bundle;
import com.xiaomi.d.e.g;

public class f extends d {

    /* renamed from: a  reason: collision with root package name */
    private b f2372a = b.available;
    private String d = null;
    private int e = Integer.MIN_VALUE;
    private a f = null;

    public enum a {
        chat,
        available,
        away,
        xa,
        dnd
    }

    public enum b {
        available,
        unavailable,
        subscribe,
        subscribed,
        unsubscribe,
        unsubscribed,
        g,
        probe
    }

    public f(Bundle bundle) {
        super(bundle);
        if (bundle.containsKey("ext_pres_type")) {
            this.f2372a = b.valueOf(bundle.getString("ext_pres_type"));
        }
        if (bundle.containsKey("ext_pres_status")) {
            this.d = bundle.getString("ext_pres_status");
        }
        if (bundle.containsKey("ext_pres_prio")) {
            this.e = bundle.getInt("ext_pres_prio");
        }
        if (bundle.containsKey("ext_pres_mode")) {
            this.f = a.valueOf(bundle.getString("ext_pres_mode"));
        }
    }

    public f(b bVar) {
        a(bVar);
    }

    public String a() {
        StringBuilder sb = new StringBuilder();
        sb.append("<presence");
        if (t() != null) {
            sb.append(" xmlns=\"").append(t()).append("\"");
        }
        if (k() != null) {
            sb.append(" id=\"").append(k()).append("\"");
        }
        if (m() != null) {
            sb.append(" to=\"").append(g.a(m())).append("\"");
        }
        if (n() != null) {
            sb.append(" from=\"").append(g.a(n())).append("\"");
        }
        if (l() != null) {
            sb.append(" chid=\"").append(g.a(l())).append("\"");
        }
        if (this.f2372a != null) {
            sb.append(" type=\"").append(this.f2372a).append("\"");
        }
        sb.append(">");
        if (this.d != null) {
            sb.append("<status>").append(g.a(this.d)).append("</status>");
        }
        if (this.e != Integer.MIN_VALUE) {
            sb.append("<priority>").append(this.e).append("</priority>");
        }
        if (!(this.f == null || this.f == a.available)) {
            sb.append("<show>").append(this.f).append("</show>");
        }
        sb.append(s());
        h p = p();
        if (p != null) {
            sb.append(p.d());
        }
        sb.append("</presence>");
        return sb.toString();
    }

    public void a(int i) {
        if (i < -128 || i > 128) {
            throw new IllegalArgumentException("Priority value " + i + " is not valid. Valid range is -128 through 128.");
        }
        this.e = i;
    }

    public void a(a aVar) {
        this.f = aVar;
    }

    public void a(b bVar) {
        if (bVar == null) {
            throw new NullPointerException("Type cannot be null");
        }
        this.f2372a = bVar;
    }

    public void a(String str) {
        this.d = str;
    }

    public Bundle c() {
        Bundle c = super.c();
        if (this.f2372a != null) {
            c.putString("ext_pres_type", this.f2372a.toString());
        }
        if (this.d != null) {
            c.putString("ext_pres_status", this.d);
        }
        if (this.e != Integer.MIN_VALUE) {
            c.putInt("ext_pres_prio", this.e);
        }
        if (!(this.f == null || this.f == a.available)) {
            c.putString("ext_pres_mode", this.f.toString());
        }
        return c;
    }
}
