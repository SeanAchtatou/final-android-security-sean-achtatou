package com.xiaomi.d.c;

import android.os.Bundle;
import cn.banshenggua.aichang.room.message.SocketMessage;
import com.xiaomi.d.e.g;
import java.util.HashMap;
import java.util.Map;

public class b extends d {

    /* renamed from: a  reason: collision with root package name */
    private a f2368a = a.f2369a;
    private final Map<String, String> d = new HashMap();

    public static class a {

        /* renamed from: a  reason: collision with root package name */
        public static final a f2369a = new a("get");
        public static final a b = new a("set");
        public static final a c = new a(SocketMessage.MSG_RESULE_KEY);
        public static final a d = new a(SocketMessage.MSG_ERROR_KEY);
        public static final a e = new a("command");
        private String f;

        private a(String str) {
            this.f = str;
        }

        public static a a(String str) {
            if (str == null) {
                return null;
            }
            String lowerCase = str.toLowerCase();
            if (f2369a.toString().equals(lowerCase)) {
                return f2369a;
            }
            if (b.toString().equals(lowerCase)) {
                return b;
            }
            if (d.toString().equals(lowerCase)) {
                return d;
            }
            if (c.toString().equals(lowerCase)) {
                return c;
            }
            if (e.toString().equals(lowerCase)) {
                return e;
            }
            return null;
        }

        public String toString() {
            return this.f;
        }
    }

    public b() {
    }

    public b(Bundle bundle) {
        super(bundle);
        if (bundle.containsKey("ext_iq_type")) {
            this.f2368a = a.a(bundle.getString("ext_iq_type"));
        }
    }

    public String a() {
        StringBuilder sb = new StringBuilder();
        sb.append("<iq ");
        if (k() != null) {
            sb.append("id=\"" + k() + "\" ");
        }
        if (m() != null) {
            sb.append("to=\"").append(g.a(m())).append("\" ");
        }
        if (n() != null) {
            sb.append("from=\"").append(g.a(n())).append("\" ");
        }
        if (l() != null) {
            sb.append("chid=\"").append(g.a(l())).append("\" ");
        }
        for (Map.Entry next : this.d.entrySet()) {
            sb.append(g.a((String) next.getKey())).append("=\"");
            sb.append(g.a((String) next.getValue())).append("\" ");
        }
        if (this.f2368a == null) {
            sb.append("type=\"get\">");
        } else {
            sb.append("type=\"").append(b()).append("\">");
        }
        String d2 = d();
        if (d2 != null) {
            sb.append(d2);
        }
        sb.append(s());
        h p = p();
        if (p != null) {
            sb.append(p.d());
        }
        sb.append("</iq>");
        return sb.toString();
    }

    public synchronized String a(String str) {
        return this.d.get(str);
    }

    public void a(a aVar) {
        if (aVar == null) {
            this.f2368a = a.f2369a;
        } else {
            this.f2368a = aVar;
        }
    }

    public synchronized void a(String str, String str2) {
        this.d.put(str, str2);
    }

    public synchronized void a(Map<String, String> map) {
        this.d.putAll(map);
    }

    public a b() {
        return this.f2368a;
    }

    public Bundle c() {
        Bundle c = super.c();
        if (this.f2368a != null) {
            c.putString("ext_iq_type", this.f2368a.toString());
        }
        return c;
    }

    public String d() {
        return null;
    }
}
