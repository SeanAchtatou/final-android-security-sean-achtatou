package com.xiaomi.d;

import com.xiaomi.d.c.g;
import com.xiaomi.d.c.h;
import java.io.PrintStream;
import java.io.PrintWriter;

public class p extends Exception {

    /* renamed from: a  reason: collision with root package name */
    private g f2399a = null;
    private h b = null;
    private Throwable c = null;

    public p() {
    }

    public p(g gVar) {
        this.f2399a = gVar;
    }

    public p(String str) {
        super(str);
    }

    public p(String str, Throwable th) {
        super(str);
        this.c = th;
    }

    public p(Throwable th) {
        this.c = th;
    }

    public Throwable a() {
        return this.c;
    }

    public String getMessage() {
        String message = super.getMessage();
        return (message != null || this.b == null) ? (message != null || this.f2399a == null) ? message : this.f2399a.toString() : this.b.toString();
    }

    public void printStackTrace() {
        printStackTrace(System.err);
    }

    public void printStackTrace(PrintStream printStream) {
        super.printStackTrace(printStream);
        if (this.c != null) {
            printStream.println("Nested Exception: ");
            this.c.printStackTrace(printStream);
        }
    }

    public void printStackTrace(PrintWriter printWriter) {
        super.printStackTrace(printWriter);
        if (this.c != null) {
            printWriter.println("Nested Exception: ");
            this.c.printStackTrace(printWriter);
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        String message = super.getMessage();
        if (message != null) {
            sb.append(message).append(": ");
        }
        if (this.b != null) {
            sb.append(this.b);
        }
        if (this.f2399a != null) {
            sb.append(this.f2399a);
        }
        if (this.c != null) {
            sb.append("\n  -- caused by: ").append(this.c);
        }
        return sb.toString();
    }
}
