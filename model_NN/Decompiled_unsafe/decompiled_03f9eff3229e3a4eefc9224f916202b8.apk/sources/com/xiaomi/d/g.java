package com.xiaomi.d;

import cn.banshenggua.aichang.room.message.SocketMessage;
import com.baidu.mobads.openad.d.b;
import com.qq.e.comm.constants.Constants;
import com.xiaomi.d.a;
import com.xiaomi.d.c.b;
import com.xiaomi.d.c.d;
import com.xiaomi.d.e.c;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

class g {

    /* renamed from: a  reason: collision with root package name */
    private Thread f2389a;
    private l b;
    private XmlPullParser c;
    private boolean d;

    protected g(l lVar) {
        this.b = lVar;
        a();
    }

    private void a(d dVar) {
        if (dVar != null) {
            for (a.C0058a a2 : this.b.e.values()) {
                a2.a(dVar);
            }
        }
    }

    private void e() {
        this.c = XmlPullParserFactory.newInstance().newPullParser();
        this.c.setFeature("http://xmlpull.org/v1/doc/features.html#process-namespaces", true);
        this.c.setInput(this.b.h);
    }

    /* access modifiers changed from: private */
    public void f() {
        try {
            e();
            int eventType = this.c.getEventType();
            String str = "";
            do {
                this.b.o();
                if (eventType == 2) {
                    String name = this.c.getName();
                    if (this.c.getName().equals(b.EVENT_MESSAGE)) {
                        a(c.a(this.c));
                        str = name;
                    } else if (this.c.getName().equals("iq")) {
                        a(c.a(this.c, this.b));
                        str = name;
                    } else if (this.c.getName().equals("presence")) {
                        a(c.b(this.c));
                        str = name;
                    } else if (this.c.getName().equals("stream")) {
                        String str2 = "";
                        for (int i = 0; i < this.c.getAttributeCount(); i++) {
                            if (this.c.getAttributeName(i).equals("from")) {
                                this.b.l.a(this.c.getAttributeValue(i));
                            } else if (this.c.getAttributeName(i).equals("challenge")) {
                                str2 = this.c.getAttributeValue(i);
                            } else if (Constants.KEYS.PLACEMENTS.equals(this.c.getAttributeName(i))) {
                                String attributeValue = this.c.getAttributeValue(i);
                                com.xiaomi.d.c.b bVar = new com.xiaomi.d.c.b();
                                bVar.l("0");
                                bVar.k("0");
                                bVar.a(Constants.KEYS.PLACEMENTS, attributeValue);
                                bVar.a(b.a.b);
                                a(bVar);
                            }
                        }
                        this.b.a(str2);
                        str = name;
                    } else if (this.c.getName().equals(SocketMessage.MSG_ERROR_KEY)) {
                        throw new p(c.d(this.c));
                    } else {
                        if (this.c.getName().equals("warning")) {
                            this.c.next();
                            if (this.c.getName().equals("multi-login")) {
                                a(6, null);
                                str = name;
                            }
                        } else if (this.c.getName().equals("bind")) {
                            a(c.c(this.c));
                            str = name;
                        }
                        str = name;
                    }
                } else if (eventType == 3 && this.c.getName().equals("stream")) {
                    a(13, null);
                }
                eventType = this.c.next();
                if (this.d) {
                    break;
                }
            } while (eventType != 1);
            if (eventType == 1) {
                throw new Exception("SMACK: server close the connection or timeout happened, last element name=" + str + " host=" + this.b.c());
            }
        } catch (Exception e) {
            com.xiaomi.a.a.c.c.a(e);
            if (!this.d) {
                a(9, e);
            } else {
                com.xiaomi.a.a.c.c.c("reader is shutdown, ignore the exception.");
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.d = false;
        this.f2389a = new h(this, "Smack Packet Reader (" + this.b.k + ")");
    }

    /* access modifiers changed from: package-private */
    public void a(int i, Exception exc) {
        this.d = true;
        this.b.a(i, exc);
    }

    public void b() {
        this.f2389a.start();
    }

    public void c() {
        this.d = true;
    }

    /* access modifiers changed from: package-private */
    public void d() {
        this.b.e.clear();
    }
}
