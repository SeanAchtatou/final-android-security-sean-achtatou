package com.xiaomi.d;

import android.os.SystemClock;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.ViewCompat;
import com.xiaomi.a.a.c.c;
import com.xiaomi.a.a.e.d;
import com.xiaomi.d.b.a;
import com.xiaomi.d.c.f;
import com.xiaomi.d.e.i;
import com.xiaomi.e.g;
import com.xiaomi.network.b;
import com.xiaomi.network.f;
import com.xiaomi.push.service.XMPushService;
import com.xiaomi.push.service.ao;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import org.apache.mina.proxy.handlers.socks.SocksProxyConstants;

public class l extends a {
    private int A;
    public Exception n = null;
    protected Socket o;
    String p = null;
    i q;
    g r;
    private String s = null;
    private String t = "";
    private String u;
    /* access modifiers changed from: private */
    public XMPushService v;
    private volatile long w = 0;
    private volatile long x = 0;
    private final String y = "<pf><p>t:%1$d</p></pf>";
    private volatile long z = 0;

    public l(XMPushService xMPushService, b bVar) {
        super(xMPushService, bVar);
        this.v = xMPushService;
    }

    private void a(b bVar) {
        a(bVar.f(), bVar.e());
    }

    private void a(Exception exc) {
        if (SystemClock.elapsedRealtime() - this.z >= 300000) {
            this.A = 0;
        } else if (d.d(this.v)) {
            this.A++;
            if (this.A >= 2) {
                String c = c();
                c.a("max short conn time reached, sink down current host:" + c);
                a(c, 0, exc);
                this.A = 0;
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0224, code lost:
        if (android.text.TextUtils.equals(r10, com.xiaomi.a.a.e.d.f(r0.v)) != false) goto L_0x0226;
     */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0104  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0112 A[SYNTHETIC, Splitter:B:25:0x0112] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0161  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x017e A[SYNTHETIC, Splitter:B:36:0x017e] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x01cd  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x01f5  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x0211  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x0227 A[RETURN] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(java.lang.String r17, int r18) {
        /*
            r16 = this;
            r4 = 0
            r2 = 0
            r0 = r16
            r0.n = r2
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r5 = "get bucket for host : "
            java.lang.StringBuilder r2 = r2.append(r5)
            r0 = r17
            java.lang.StringBuilder r2 = r2.append(r0)
            java.lang.String r2 = r2.toString()
            java.lang.Integer r2 = com.xiaomi.a.a.c.c.e(r2)
            int r5 = r2.intValue()
            com.xiaomi.network.b r2 = r16.c(r17)
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)
            com.xiaomi.a.a.c.c.a(r5)
            if (r2 == 0) goto L_0x003a
            r3 = 1
            java.util.ArrayList r3 = r2.a(r3)
        L_0x003a:
            boolean r5 = r3.isEmpty()
            if (r5 == 0) goto L_0x0045
            r0 = r17
            r3.add(r0)
        L_0x0045:
            r6 = 0
            r0 = r16
            r0.z = r6
            r0 = r16
            com.xiaomi.push.service.XMPushService r5 = r0.v
            java.lang.String r10 = com.xiaomi.a.a.e.d.f(r5)
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            java.util.Iterator r12 = r3.iterator()
        L_0x005c:
            boolean r3 = r12.hasNext()
            if (r3 == 0) goto L_0x0237
            java.lang.Object r3 = r12.next()
            java.lang.String r3 = (java.lang.String) r3
            long r14 = java.lang.System.currentTimeMillis()
            r0 = r16
            int r5 = r0.b
            int r5 = r5 + 1
            r0 = r16
            r0.b = r5
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x010e, p -> 0x017a, Throwable -> 0x01e4 }
            r5.<init>()     // Catch:{ IOException -> 0x010e, p -> 0x017a, Throwable -> 0x01e4 }
            java.lang.String r6 = "begin to connect to "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ IOException -> 0x010e, p -> 0x017a, Throwable -> 0x01e4 }
            java.lang.StringBuilder r5 = r5.append(r3)     // Catch:{ IOException -> 0x010e, p -> 0x017a, Throwable -> 0x01e4 }
            java.lang.String r5 = r5.toString()     // Catch:{ IOException -> 0x010e, p -> 0x017a, Throwable -> 0x01e4 }
            com.xiaomi.a.a.c.c.a(r5)     // Catch:{ IOException -> 0x010e, p -> 0x017a, Throwable -> 0x01e4 }
            java.net.Socket r5 = r16.u()     // Catch:{ IOException -> 0x010e, p -> 0x017a, Throwable -> 0x01e4 }
            r0 = r16
            r0.o = r5     // Catch:{ IOException -> 0x010e, p -> 0x017a, Throwable -> 0x01e4 }
            r0 = r16
            java.net.Socket r5 = r0.o     // Catch:{ IOException -> 0x010e, p -> 0x017a, Throwable -> 0x01e4 }
            r6 = 0
            r5.bind(r6)     // Catch:{ IOException -> 0x010e, p -> 0x017a, Throwable -> 0x01e4 }
            r0 = r18
            java.net.InetSocketAddress r5 = com.xiaomi.network.d.b(r3, r0)     // Catch:{ IOException -> 0x010e, p -> 0x017a, Throwable -> 0x01e4 }
            r0 = r16
            java.net.Socket r6 = r0.o     // Catch:{ IOException -> 0x010e, p -> 0x017a, Throwable -> 0x01e4 }
            r7 = 5000(0x1388, float:7.006E-42)
            r6.connect(r5, r7)     // Catch:{ IOException -> 0x010e, p -> 0x017a, Throwable -> 0x01e4 }
            r0 = r16
            java.net.Socket r5 = r0.o     // Catch:{ IOException -> 0x010e, p -> 0x017a, Throwable -> 0x01e4 }
            r6 = 1
            r5.setTcpNoDelay(r6)     // Catch:{ IOException -> 0x010e, p -> 0x017a, Throwable -> 0x01e4 }
            r0 = r16
            r0.u = r3     // Catch:{ IOException -> 0x010e, p -> 0x017a, Throwable -> 0x01e4 }
            r16.x()     // Catch:{ IOException -> 0x010e, p -> 0x017a, Throwable -> 0x01e4 }
            r9 = 1
            long r4 = java.lang.System.currentTimeMillis()     // Catch:{ IOException -> 0x0231, p -> 0x022e, Throwable -> 0x022a }
            long r4 = r4 - r14
            r0 = r16
            r0.c = r4     // Catch:{ IOException -> 0x0231, p -> 0x022e, Throwable -> 0x022a }
            if (r2 == 0) goto L_0x00cf
            r0 = r16
            long r4 = r0.c     // Catch:{ IOException -> 0x0231, p -> 0x022e, Throwable -> 0x022a }
            r6 = 0
            r2.b(r3, r4, r6)     // Catch:{ IOException -> 0x0231, p -> 0x022e, Throwable -> 0x022a }
        L_0x00cf:
            long r4 = android.os.SystemClock.elapsedRealtime()     // Catch:{ IOException -> 0x0231, p -> 0x022e, Throwable -> 0x022a }
            r0 = r16
            r0.z = r4     // Catch:{ IOException -> 0x0231, p -> 0x022e, Throwable -> 0x022a }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0231, p -> 0x022e, Throwable -> 0x022a }
            r4.<init>()     // Catch:{ IOException -> 0x0231, p -> 0x022e, Throwable -> 0x022a }
            java.lang.String r5 = "connected to "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ IOException -> 0x0231, p -> 0x022e, Throwable -> 0x022a }
            java.lang.StringBuilder r4 = r4.append(r3)     // Catch:{ IOException -> 0x0231, p -> 0x022e, Throwable -> 0x022a }
            java.lang.String r5 = " in "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ IOException -> 0x0231, p -> 0x022e, Throwable -> 0x022a }
            r0 = r16
            long r6 = r0.c     // Catch:{ IOException -> 0x0231, p -> 0x022e, Throwable -> 0x022a }
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ IOException -> 0x0231, p -> 0x022e, Throwable -> 0x022a }
            java.lang.String r4 = r4.toString()     // Catch:{ IOException -> 0x0231, p -> 0x022e, Throwable -> 0x022a }
            com.xiaomi.a.a.c.c.a(r4)     // Catch:{ IOException -> 0x0231, p -> 0x022e, Throwable -> 0x022a }
        L_0x00fb:
            com.xiaomi.network.f r2 = com.xiaomi.network.f.a()
            r2.h()
            if (r9 != 0) goto L_0x0227
            com.xiaomi.d.p r2 = new com.xiaomi.d.p
            java.lang.String r3 = r11.toString()
            r2.<init>(r3)
            throw r2
        L_0x010e:
            r8 = move-exception
            r9 = r4
        L_0x0110:
            if (r2 == 0) goto L_0x011c
            long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0228 }
            long r4 = r4 - r14
            r6 = 0
            r2.b(r3, r4, r6, r8)     // Catch:{ all -> 0x0228 }
        L_0x011c:
            r0 = r16
            r0.n = r8     // Catch:{ all -> 0x0228 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0228 }
            r4.<init>()     // Catch:{ all -> 0x0228 }
            java.lang.String r5 = "SMACK: Could not connect to:"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0228 }
            java.lang.StringBuilder r4 = r4.append(r3)     // Catch:{ all -> 0x0228 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x0228 }
            com.xiaomi.a.a.c.c.d(r4)     // Catch:{ all -> 0x0228 }
            java.lang.String r4 = "SMACK: Could not connect to "
            java.lang.StringBuilder r4 = r11.append(r4)     // Catch:{ all -> 0x0228 }
            java.lang.StringBuilder r4 = r4.append(r3)     // Catch:{ all -> 0x0228 }
            java.lang.String r5 = " port:"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0228 }
            r0 = r18
            java.lang.StringBuilder r4 = r4.append(r0)     // Catch:{ all -> 0x0228 }
            java.lang.String r5 = " "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0228 }
            java.lang.String r5 = r8.getMessage()     // Catch:{ all -> 0x0228 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0228 }
            java.lang.String r5 = "\n"
            r4.append(r5)     // Catch:{ all -> 0x0228 }
            if (r9 != 0) goto L_0x0176
            r0 = r16
            java.lang.Exception r4 = r0.n
            com.xiaomi.e.g.a(r3, r4)
            r0 = r16
            com.xiaomi.push.service.XMPushService r3 = r0.v
            java.lang.String r3 = com.xiaomi.a.a.e.d.f(r3)
            boolean r3 = android.text.TextUtils.equals(r10, r3)
            if (r3 == 0) goto L_0x00fb
        L_0x0176:
            r3 = r9
        L_0x0177:
            r4 = r3
            goto L_0x005c
        L_0x017a:
            r8 = move-exception
            r9 = r4
        L_0x017c:
            if (r2 == 0) goto L_0x0188
            long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0228 }
            long r4 = r4 - r14
            r6 = 0
            r2.b(r3, r4, r6, r8)     // Catch:{ all -> 0x0228 }
        L_0x0188:
            r0 = r16
            r0.n = r8     // Catch:{ all -> 0x0228 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0228 }
            r4.<init>()     // Catch:{ all -> 0x0228 }
            java.lang.String r5 = "SMACK: Could not connect to:"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0228 }
            java.lang.StringBuilder r4 = r4.append(r3)     // Catch:{ all -> 0x0228 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x0228 }
            com.xiaomi.a.a.c.c.d(r4)     // Catch:{ all -> 0x0228 }
            java.lang.String r4 = "SMACK: Could not connect to "
            java.lang.StringBuilder r4 = r11.append(r4)     // Catch:{ all -> 0x0228 }
            java.lang.StringBuilder r4 = r4.append(r3)     // Catch:{ all -> 0x0228 }
            java.lang.String r5 = " port:"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0228 }
            r0 = r18
            java.lang.StringBuilder r4 = r4.append(r0)     // Catch:{ all -> 0x0228 }
            java.lang.String r5 = " "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0228 }
            java.lang.String r5 = r8.getMessage()     // Catch:{ all -> 0x0228 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0228 }
            java.lang.String r5 = "\n"
            r4.append(r5)     // Catch:{ all -> 0x0228 }
            if (r9 != 0) goto L_0x0176
            r0 = r16
            java.lang.Exception r4 = r0.n
            com.xiaomi.e.g.a(r3, r4)
            r0 = r16
            com.xiaomi.push.service.XMPushService r3 = r0.v
            java.lang.String r3 = com.xiaomi.a.a.e.d.f(r3)
            boolean r3 = android.text.TextUtils.equals(r10, r3)
            if (r3 != 0) goto L_0x0176
            goto L_0x00fb
        L_0x01e4:
            r5 = move-exception
        L_0x01e5:
            java.lang.Exception r6 = new java.lang.Exception     // Catch:{ all -> 0x020d }
            java.lang.String r7 = "abnormal exception"
            r6.<init>(r7, r5)     // Catch:{ all -> 0x020d }
            r0 = r16
            r0.n = r6     // Catch:{ all -> 0x020d }
            com.xiaomi.a.a.c.c.a(r5)     // Catch:{ all -> 0x020d }
            if (r4 != 0) goto L_0x0234
            r0 = r16
            java.lang.Exception r5 = r0.n
            com.xiaomi.e.g.a(r3, r5)
            r0 = r16
            com.xiaomi.push.service.XMPushService r3 = r0.v
            java.lang.String r3 = com.xiaomi.a.a.e.d.f(r3)
            boolean r3 = android.text.TextUtils.equals(r10, r3)
            if (r3 != 0) goto L_0x0234
            r9 = r4
            goto L_0x00fb
        L_0x020d:
            r2 = move-exception
            r9 = r4
        L_0x020f:
            if (r9 != 0) goto L_0x0226
            r0 = r16
            java.lang.Exception r4 = r0.n
            com.xiaomi.e.g.a(r3, r4)
            r0 = r16
            com.xiaomi.push.service.XMPushService r3 = r0.v
            java.lang.String r3 = com.xiaomi.a.a.e.d.f(r3)
            boolean r3 = android.text.TextUtils.equals(r10, r3)
            if (r3 == 0) goto L_0x00fb
        L_0x0226:
            throw r2
        L_0x0227:
            return
        L_0x0228:
            r2 = move-exception
            goto L_0x020f
        L_0x022a:
            r4 = move-exception
            r5 = r4
            r4 = r9
            goto L_0x01e5
        L_0x022e:
            r8 = move-exception
            goto L_0x017c
        L_0x0231:
            r8 = move-exception
            goto L_0x0110
        L_0x0234:
            r3 = r4
            goto L_0x0177
        L_0x0237:
            r9 = r4
            goto L_0x00fb
        */
        throw new UnsupportedOperationException("Method not decompiled: com.xiaomi.d.l.a(java.lang.String, int):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.xiaomi.network.f.a(java.lang.String, boolean):com.xiaomi.network.b
     arg types: [java.lang.String, int]
     candidates:
      com.xiaomi.network.f.a(java.util.Collection, java.lang.String):java.lang.String
      com.xiaomi.network.f.a(java.lang.String[], java.lang.String):java.lang.String
      com.xiaomi.network.f.a(java.lang.String, java.lang.String):void
      com.xiaomi.network.f.a(java.lang.String, com.xiaomi.network.b):void
      com.xiaomi.network.f.a(java.lang.String, boolean):com.xiaomi.network.b */
    private void a(String str, long j, Exception exc) {
        b a2 = f.a().a(b.b(), false);
        if (a2 != null) {
            a2.b(str, j, 0, exc);
            f.a().h();
        }
    }

    private synchronized void x() {
        y();
        this.q = new i(this);
        this.r = new g(this);
        if (this.l.g()) {
            a(this.g.c(), (a) null);
            if (this.g.d() != null) {
                b(this.g.d(), null);
            }
        }
        this.q.c();
        this.r.b();
    }

    private void y() {
        try {
            this.h = new BufferedReader(new InputStreamReader(this.o.getInputStream(), "UTF-8"), 4096);
            this.i = new BufferedWriter(new OutputStreamWriter(this.o.getOutputStream(), "UTF-8"));
            if (this.h != null && this.i != null) {
                f();
            }
        } catch (Exception e) {
            throw new p("Error to init reader and writer", e);
        }
    }

    public void a(int i, Exception exc) {
        this.v.a(new n(this, 2, i, exc));
    }

    public void a(com.xiaomi.d.c.d dVar) {
        if (this.q != null) {
            this.q.a(dVar);
            return;
        }
        throw new p("the writer is null.");
    }

    public void a(com.xiaomi.d.c.f fVar, int i, Exception exc) {
        b(fVar, i, exc);
        if (exc != null && this.z != 0) {
            a(exc);
        }
    }

    public synchronized void a(ao.b bVar) {
        new k().a(bVar, r(), this);
    }

    public synchronized void a(String str, String str2) {
        com.xiaomi.d.c.f fVar = new com.xiaomi.d.c.f(f.b.unavailable);
        fVar.l(str);
        fVar.n(str2);
        if (this.q != null) {
            this.q.a(fVar);
        }
    }

    public void a(com.xiaomi.d.c.d[] dVarArr) {
        for (com.xiaomi.d.c.d a2 : dVarArr) {
            a(a2);
        }
    }

    /* access modifiers changed from: protected */
    public synchronized void b(com.xiaomi.d.c.f fVar, int i, Exception exc) {
        if (n() != 2) {
            a(2, i, exc);
            this.j = "";
            if (this.r != null) {
                this.r.c();
                this.r.d();
                this.r = null;
            }
            if (this.q != null) {
                try {
                    this.q.b();
                } catch (IOException e) {
                    c.a(e);
                }
                this.q.a();
                this.q = null;
            }
            try {
                this.o.close();
            } catch (Throwable th) {
            }
            if (this.h != null) {
                try {
                    this.h.close();
                } catch (Throwable th2) {
                }
                this.h = null;
            }
            if (this.i != null) {
                try {
                    this.i.close();
                } catch (Throwable th3) {
                }
                this.i = null;
            }
            this.w = 0;
            this.x = 0;
        }
        return;
    }

    public void b(String str) {
        this.t = str;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.xiaomi.network.f.a(java.lang.String, boolean):com.xiaomi.network.b
     arg types: [java.lang.String, int]
     candidates:
      com.xiaomi.network.f.a(java.util.Collection, java.lang.String):java.lang.String
      com.xiaomi.network.f.a(java.lang.String[], java.lang.String):java.lang.String
      com.xiaomi.network.f.a(java.lang.String, java.lang.String):void
      com.xiaomi.network.f.a(java.lang.String, com.xiaomi.network.b):void
      com.xiaomi.network.f.a(java.lang.String, boolean):com.xiaomi.network.b */
    /* access modifiers changed from: package-private */
    public b c(String str) {
        b a2 = com.xiaomi.network.f.a().a(str, false);
        if (!a2.b()) {
            i.a(new o(this, str));
        }
        this.d = 0;
        try {
            byte[] address = InetAddress.getByName(a2.f).getAddress();
            this.d = address[0] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD;
            this.d |= (address[1] << 8) & MotionEventCompat.ACTION_POINTER_INDEX_MASK;
            this.d |= (address[2] << 16) & 16711680;
            this.d = ((address[3] << 24) & ViewCompat.MEASURED_STATE_MASK) | this.d;
        } catch (UnknownHostException e) {
        }
        return a2;
    }

    public String c() {
        return this.u;
    }

    public void m() {
        if (this.q != null) {
            this.q.d();
            this.v.a(new m(this, 13, System.currentTimeMillis()), 15000);
            return;
        }
        throw new p("the packetwriter is null.");
    }

    public String r() {
        return this.j;
    }

    public synchronized void s() {
        try {
            if (h() || g()) {
                c.a("WARNING: current xmpp has connected");
            } else {
                a(0, 0, (Exception) null);
                a(this.l);
            }
        } catch (IOException e) {
            throw new p(e);
        }
    }

    public String t() {
        String format = (this.x == 0 || this.w == 0) ? "" : String.format("<pf><p>t:%1$d</p></pf>", Long.valueOf(this.x - this.w));
        String c = g.c();
        return String.format(this.t, format, c != null ? "<q>" + c + "</q>" : "");
    }

    public Socket u() {
        return new Socket();
    }

    public void v() {
        this.w = SystemClock.uptimeMillis();
    }

    public void w() {
        this.x = SystemClock.uptimeMillis();
    }
}
