package com.xiaomi.d;

import android.os.Build;
import android.text.TextUtils;
import android.util.Base64;
import com.xiaomi.d.c.d;
import com.xiaomi.d.e.g;
import com.xiaomi.d.e.h;
import com.xiaomi.d.e.k;
import java.io.IOException;
import java.io.Writer;
import java.util.Locale;
import org.apache.mina.proxy.handlers.http.HttpProxyConstants;

class i {

    /* renamed from: a  reason: collision with root package name */
    private Writer f2391a;
    private l b;

    protected i(l lVar) {
        this.b = lVar;
        this.f2391a = lVar.i;
    }

    private void b(d dVar) {
        synchronized (this.f2391a) {
            try {
                String a2 = dVar.a();
                this.f2391a.write(a2 + HttpProxyConstants.CRLF);
                this.f2391a.flush();
                String o = dVar.o();
                if (!TextUtils.isEmpty(o)) {
                    k.a(this.b.m, o, (long) k.a(a2), false, System.currentTimeMillis());
                }
            } catch (IOException e) {
                throw new p(e);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.b.f.clear();
    }

    public void a(d dVar) {
        b(dVar);
        this.b.b(dVar);
    }

    public void b() {
        synchronized (this.f2391a) {
            this.f2391a.write("</stream:stream>");
            this.f2391a.flush();
        }
    }

    /* access modifiers changed from: package-private */
    public void c() {
        StringBuilder sb = new StringBuilder();
        sb.append("<stream:stream");
        sb.append(" xmlns=\"xm\"");
        sb.append(" xmlns:stream=\"xm\"");
        sb.append(" to=\"").append(this.b.b()).append("\"");
        sb.append(" version=\"105\"");
        sb.append(" model=\"").append(g.a(Build.MODEL)).append("\"");
        sb.append(" os=\"").append(g.a(Build.VERSION.INCREMENTAL)).append("\"");
        String b2 = h.b();
        if (b2 != null) {
            sb.append(" uid=\"").append(b2).append("\"");
        }
        sb.append(" sdk=\"").append(8).append("\"");
        sb.append(" connpt=\"").append(g.a(this.b.d())).append("\"");
        sb.append(" host=\"").append(this.b.c()).append("\"");
        sb.append(" locale=\"").append(g.a(Locale.getDefault().toString())).append("\"");
        byte[] a2 = this.b.a().a();
        if (a2 != null) {
            sb.append(" ps=\"").append(Base64.encodeToString(a2, 10)).append("\"");
        }
        sb.append(">");
        this.f2391a.write(sb.toString());
        this.f2391a.flush();
    }

    public void d() {
        synchronized (this.f2391a) {
            try {
                this.f2391a.write(this.b.t() + HttpProxyConstants.CRLF);
                this.f2391a.flush();
                this.b.v();
            } catch (IOException e) {
                throw new p(e);
            }
        }
    }
}
