package android.support.multidex;

import java.io.File;
import java.io.FileFilter;

/* compiled from: MultiDexExtractor */
final class c implements FileFilter {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f115a;

    c(String str) {
        this.f115a = str;
    }

    public boolean accept(File file) {
        return !file.getName().startsWith(this.f115a);
    }
}
