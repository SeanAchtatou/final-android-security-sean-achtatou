package android.support.v4.media.session;

import android.graphics.Bitmap;
import android.media.RemoteControlClient;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.media.session.MediaSessionCompatApi14;
import cn.banshenggua.aichang.utils.Constants;

public class MediaSessionCompatApi19 {
    private static final String METADATA_KEY_ALBUM_ART = "android.media.metadata.ALBUM_ART";
    private static final String METADATA_KEY_ART = "android.media.metadata.ART";
    private static final String METADATA_KEY_RATING = "android.media.metadata.RATING";
    private static final String METADATA_KEY_USER_RATING = "android.media.metadata.USER_RATING";

    public static Object createMetadataUpdateListener(MediaSessionCompatApi14.Callback callback) {
        return new OnMetadataUpdateListener(callback);
    }

    public static void setMetadata(Object obj, Bundle bundle, boolean z) {
        RemoteControlClient.MetadataEditor editMetadata = ((RemoteControlClient) obj).editMetadata(true);
        MediaSessionCompatApi14.buildOldMetadata(bundle, editMetadata);
        addNewMetadata(bundle, editMetadata);
        if (z && Build.VERSION.SDK_INT > 19) {
            editMetadata.addEditableKey(268435457);
        }
        editMetadata.apply();
    }

    public static void setOnMetadataUpdateListener(Object obj, Object obj2) {
        ((RemoteControlClient) obj).setMetadataUpdateListener((RemoteControlClient.OnMetadataUpdateListener) obj2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.media.RemoteControlClient.MetadataEditor.putObject(int, java.lang.Object):android.media.MediaMetadataEditor throws java.lang.IllegalArgumentException}
     arg types: [?, android.os.Parcelable]
     candidates:
      ClspMth{android.media.RemoteControlClient.MetadataEditor.putObject(int, java.lang.Object):android.media.RemoteControlClient$MetadataEditor throws java.lang.IllegalArgumentException}
      ClspMth{android.media.RemoteControlClient.MetadataEditor.putObject(int, java.lang.Object):android.media.MediaMetadataEditor throws java.lang.IllegalArgumentException} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.media.RemoteControlClient.MetadataEditor.putObject(int, java.lang.Object):android.media.MediaMetadataEditor throws java.lang.IllegalArgumentException}
     arg types: [int, android.os.Parcelable]
     candidates:
      ClspMth{android.media.RemoteControlClient.MetadataEditor.putObject(int, java.lang.Object):android.media.RemoteControlClient$MetadataEditor throws java.lang.IllegalArgumentException}
      ClspMth{android.media.RemoteControlClient.MetadataEditor.putObject(int, java.lang.Object):android.media.MediaMetadataEditor throws java.lang.IllegalArgumentException} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.media.RemoteControlClient.MetadataEditor.putBitmap(int, android.graphics.Bitmap):android.media.RemoteControlClient$MetadataEditor throws java.lang.IllegalArgumentException}
     arg types: [int, android.graphics.Bitmap]
     candidates:
      ClspMth{android.media.RemoteControlClient.MetadataEditor.putBitmap(int, android.graphics.Bitmap):android.media.MediaMetadataEditor throws java.lang.IllegalArgumentException}
      ClspMth{android.media.MediaMetadataEditor.putBitmap(int, android.graphics.Bitmap):android.media.MediaMetadataEditor throws java.lang.IllegalArgumentException}
      ClspMth{android.media.RemoteControlClient.MetadataEditor.putBitmap(int, android.graphics.Bitmap):android.media.RemoteControlClient$MetadataEditor throws java.lang.IllegalArgumentException} */
    static void addNewMetadata(Bundle bundle, RemoteControlClient.MetadataEditor metadataEditor) {
        if (bundle.containsKey("android.media.metadata.RATING")) {
            metadataEditor.putObject((int) Constants.RESULT_OK, (Object) bundle.getParcelable("android.media.metadata.RATING"));
        }
        if (bundle.containsKey("android.media.metadata.USER_RATING")) {
            metadataEditor.putObject(268435457, (Object) bundle.getParcelable("android.media.metadata.USER_RATING"));
        }
        if (bundle.containsKey("android.media.metadata.ART")) {
            metadataEditor.putBitmap(100, (Bitmap) bundle.getParcelable("android.media.metadata.ART"));
        } else if (bundle.containsKey("android.media.metadata.ALBUM_ART")) {
            metadataEditor.putBitmap(100, (Bitmap) bundle.getParcelable("android.media.metadata.ALBUM_ART"));
        }
    }

    static class OnMetadataUpdateListener<T extends MediaSessionCompatApi14.Callback> implements RemoteControlClient.OnMetadataUpdateListener {
        protected final T mCallback;

        public OnMetadataUpdateListener(T t) {
            this.mCallback = t;
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public void onMetadataUpdate(int r2, java.lang.Object r3) {
            /*
                r1 = this;
                r0 = 268435457(0x10000001, float:2.5243552E-29)
                if (r2 != r0) goto L_0x000e
                boolean r0 = r3 instanceof android.media.Rating
                if (r0 == 0) goto L_0x000e
                T r0 = r1.mCallback
                r0.onSetRating(r3)
            L_0x000e:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: android.support.v4.media.session.MediaSessionCompatApi19.OnMetadataUpdateListener.onMetadataUpdate(int, java.lang.Object):void");
        }
    }
}
