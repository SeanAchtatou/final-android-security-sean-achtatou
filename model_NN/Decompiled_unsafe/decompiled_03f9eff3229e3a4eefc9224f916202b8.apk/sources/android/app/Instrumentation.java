package android.app;

import android.annotation.TargetApi;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.IBinder;
import android.view.KeyEvent;
import android.view.MotionEvent;

public class Instrumentation {

    public static class ActivityMonitor {
    }

    public static final class ActivityResult {
    }

    public ActivityResult execStartActivity(Context context, IBinder iBinder, IBinder iBinder2, Activity activity, Intent intent, int i) {
        return null;
    }

    @TargetApi(16)
    public ActivityResult execStartActivity(Context context, IBinder iBinder, IBinder iBinder2, Activity activity, Intent intent, int i, Bundle bundle) {
        return null;
    }

    @TargetApi(14)
    public ActivityResult execStartActivity(Context context, IBinder iBinder, IBinder iBinder2, Fragment fragment, Intent intent, int i) {
        return null;
    }

    @TargetApi(16)
    public ActivityResult execStartActivity(Context context, IBinder iBinder, IBinder iBinder2, Fragment fragment, Intent intent, int i, Bundle bundle) {
        return null;
    }

    public Activity newActivity(Class<?> cls, Context context, IBinder iBinder, Application application, Intent intent, ActivityInfo activityInfo, CharSequence charSequence, Activity activity, String str, Object obj) throws InstantiationException, IllegalAccessException {
        return null;
    }

    public Activity newActivity(ClassLoader classLoader, String str, Intent intent) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
        return null;
    }

    public void callActivityOnCreate(Activity activity, Bundle bundle) {
    }

    @TargetApi(18)
    public UiAutomation getUiAutomation() {
        return null;
    }

    public void onCreate(Bundle bundle) {
    }

    public void start() {
    }

    public void onStart() {
    }

    public boolean onException(Object obj, Throwable th) {
        return false;
    }

    public void sendStatus(int i, Bundle bundle) {
    }

    public void finish(int i, Bundle bundle) {
    }

    public void setAutomaticPerformanceSnapshots() {
    }

    public void startPerformanceSnapshot() {
    }

    public void endPerformanceSnapshot() {
    }

    public void onDestroy() {
    }

    public Context getContext() {
        return null;
    }

    public ComponentName getComponentName() {
        return null;
    }

    public Context getTargetContext() {
        return null;
    }

    public boolean isProfiling() {
        return false;
    }

    public void startProfiling() {
    }

    public void stopProfiling() {
    }

    public void setInTouchMode(boolean z) {
    }

    public void waitForIdle(Runnable runnable) {
    }

    public void waitForIdleSync() {
    }

    public void runOnMainSync(Runnable runnable) {
    }

    public Activity startActivitySync(Intent intent) {
        return null;
    }

    public void addMonitor(ActivityMonitor activityMonitor) {
    }

    public ActivityMonitor addMonitor(IntentFilter intentFilter, ActivityResult activityResult, boolean z) {
        return null;
    }

    public ActivityMonitor addMonitor(String str, ActivityResult activityResult, boolean z) {
        return null;
    }

    public boolean checkMonitorHit(ActivityMonitor activityMonitor, int i) {
        return false;
    }

    public Activity waitForMonitor(ActivityMonitor activityMonitor) {
        return null;
    }

    public Activity waitForMonitorWithTimeout(ActivityMonitor activityMonitor, long j) {
        return null;
    }

    public void removeMonitor(ActivityMonitor activityMonitor) {
    }

    public boolean invokeMenuActionSync(Activity activity, int i, int i2) {
        return false;
    }

    public boolean invokeContextMenuAction(Activity activity, int i, int i2) {
        return false;
    }

    public void sendStringSync(String str) {
    }

    public void sendKeySync(KeyEvent keyEvent) {
    }

    public void sendKeyDownUpSync(int i) {
    }

    public void sendCharacterSync(int i) {
    }

    public void sendPointerSync(MotionEvent motionEvent) {
    }

    public void sendTrackballEventSync(MotionEvent motionEvent) {
    }

    public Application newApplication(ClassLoader classLoader, String str, Context context) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
        return null;
    }

    public void callApplicationOnCreate(Application application) {
    }

    public void callActivityOnDestroy(Activity activity) {
    }

    public void callActivityOnRestoreInstanceState(Activity activity, Bundle bundle) {
    }

    public void callActivityOnPostCreate(Activity activity, Bundle bundle) {
    }

    public void callActivityOnNewIntent(Activity activity, Intent intent) {
    }

    public void callActivityOnStart(Activity activity) {
    }

    public void callActivityOnRestart(Activity activity) {
    }

    public void callActivityOnResume(Activity activity) {
    }

    public void callActivityOnStop(Activity activity) {
    }

    public void callActivityOnSaveInstanceState(Activity activity, Bundle bundle) {
    }

    public void callActivityOnPause(Activity activity) {
    }

    public void callActivityOnUserLeaving(Activity activity) {
    }

    public void startAllocCounting() {
    }

    public void stopAllocCounting() {
    }

    public Bundle getAllocCounts() {
        return null;
    }

    public Bundle getBinderCounts() {
        return null;
    }
}
