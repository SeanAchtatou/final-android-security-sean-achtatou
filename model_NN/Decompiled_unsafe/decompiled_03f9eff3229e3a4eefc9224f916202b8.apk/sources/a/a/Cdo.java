package a.a;

import android.content.Context;
import com.umeng.analytics.a.b;
import com.umeng.analytics.a.d;
import com.umeng.analytics.k;

/* renamed from: a.a.do  reason: invalid class name */
/* compiled from: CacheService */
public final class Cdo implements du {
    private static Cdo c;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public du f84a = new dm(this.b);
    private Context b;

    private Cdo(Context context) {
        this.b = context.getApplicationContext();
    }

    public static synchronized Cdo a(Context context) {
        Cdo doVar;
        synchronized (Cdo.class) {
            if (c == null && context != null) {
                c = new Cdo(context);
            }
            doVar = c;
        }
        return doVar;
    }

    public void a(b bVar) {
        if (bVar != null && this.f84a != null) {
            bVar.a((d) this.f84a);
        }
    }

    public void a(dv dvVar) {
        k.b(new dp(this, dvVar));
    }

    public void b(dv dvVar) {
        this.f84a.b(dvVar);
    }

    public void b() {
        k.b(new dq(this));
    }
}
