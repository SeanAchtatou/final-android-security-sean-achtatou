package a.a;

import a.a.cb;
import a.a.cd;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

/* compiled from: TUnion */
public abstract class cd<T extends cd<?, ?>, F extends cb> implements bw<T, F> {

    /* renamed from: a  reason: collision with root package name */
    private static final Map<Class<? extends cy>, cz> f53a = new HashMap();
    protected Object b = null;
    protected F c = null;

    /* access modifiers changed from: protected */
    public abstract co a(cb cbVar);

    /* access modifiers changed from: protected */
    public abstract cw a();

    /* access modifiers changed from: protected */
    public abstract Object a(cr crVar, co coVar) throws ca;

    /* access modifiers changed from: protected */
    public abstract Object a(cr crVar, short s) throws ca;

    /* access modifiers changed from: protected */
    public abstract F b(short s);

    /* access modifiers changed from: protected */
    public abstract void c(cr crVar) throws ca;

    /* access modifiers changed from: protected */
    public abstract void d(cr crVar) throws ca;

    protected cd() {
    }

    static {
        f53a.put(da.class, new b());
        f53a.put(db.class, new d());
    }

    public F b() {
        return this.c;
    }

    public Object c() {
        return this.b;
    }

    public boolean d() {
        return this.c != null;
    }

    public void a(cr crVar) throws ca {
        f53a.get(crVar.y()).b().b(crVar, this);
    }

    public void b(cr crVar) throws ca {
        f53a.get(crVar.y()).b().a(crVar, this);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("<");
        sb.append(getClass().getSimpleName());
        sb.append(" ");
        if (b() != null) {
            Object c2 = c();
            sb.append(a(b()).f66a);
            sb.append(":");
            if (c2 instanceof ByteBuffer) {
                bx.a((ByteBuffer) c2, sb);
            } else {
                sb.append(c2.toString());
            }
        }
        sb.append(">");
        return sb.toString();
    }

    /* compiled from: TUnion */
    private static class b implements cz {
        private b() {
        }

        /* renamed from: a */
        public a b() {
            return new a();
        }
    }

    /* compiled from: TUnion */
    private static class a extends da<cd> {
        private a() {
        }

        /* renamed from: a */
        public void b(cr crVar, cd cdVar) throws ca {
            cdVar.c = null;
            cdVar.b = null;
            crVar.f();
            co h = crVar.h();
            cdVar.b = cdVar.a(crVar, h);
            if (cdVar.b != null) {
                cdVar.c = cdVar.b(h.c);
            }
            crVar.i();
            crVar.h();
            crVar.g();
        }

        /* renamed from: b */
        public void a(cr crVar, cd cdVar) throws ca {
            if (cdVar.b() == null || cdVar.c() == null) {
                throw new cs("Cannot write a TUnion with no set value!");
            }
            crVar.a(cdVar.a());
            crVar.a(cdVar.a((cb) cdVar.c));
            cdVar.c(crVar);
            crVar.b();
            crVar.c();
            crVar.a();
        }
    }

    /* compiled from: TUnion */
    private static class d implements cz {
        private d() {
        }

        /* renamed from: a */
        public c b() {
            return new c();
        }
    }

    /* compiled from: TUnion */
    private static class c extends db<cd> {
        private c() {
        }

        /* renamed from: a */
        public void b(cr crVar, cd cdVar) throws ca {
            cdVar.c = null;
            cdVar.b = null;
            short r = crVar.r();
            cdVar.b = cdVar.a(crVar, r);
            if (cdVar.b != null) {
                cdVar.c = cdVar.b(r);
            }
        }

        /* renamed from: b */
        public void a(cr crVar, cd cdVar) throws ca {
            if (cdVar.b() == null || cdVar.c() == null) {
                throw new cs("Cannot write a TUnion with no set value!");
            }
            crVar.a(cdVar.c.a());
            cdVar.d(crVar);
        }
    }
}
