package a.a;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Iterator;

/* compiled from: UError */
public class g extends x implements dv {
    public g() {
        a(System.currentTimeMillis());
        a(z.LEGIT);
    }

    public g(String str) {
        this();
        a(str);
    }

    public g(Throwable th) {
        this();
        a(a(th));
    }

    public g a(boolean z) {
        a(z ? z.LEGIT : z.ALIEN);
        return this;
    }

    private String a(Throwable th) {
        if (th == null) {
            return null;
        }
        try {
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            th.printStackTrace(printWriter);
            for (Throwable cause = th.getCause(); cause != null; cause = cause.getCause()) {
                cause.printStackTrace(printWriter);
            }
            String obj = stringWriter.toString();
            printWriter.close();
            stringWriter.close();
            return obj;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void a(bi biVar, String str) {
        an anVar;
        if (biVar.b() > 0) {
            Iterator<an> it = biVar.c().iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                anVar = it.next();
                if (str.equals(anVar.a())) {
                    break;
                }
            }
        }
        anVar = null;
        if (anVar == null) {
            anVar = new an();
            anVar.a(str);
            biVar.a(anVar);
        }
        anVar.a(this);
    }
}
