package a.a;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* compiled from: UALogEntry */
public class bi implements bw<bi, e>, Serializable, Cloneable {
    public static final Map<e, cg> l;
    /* access modifiers changed from: private */
    public static final cw m = new cw("UALogEntry");
    /* access modifiers changed from: private */
    public static final co n = new co("client_stats", (byte) 12, 1);
    /* access modifiers changed from: private */
    public static final co o = new co("app_info", (byte) 12, 2);
    /* access modifiers changed from: private */
    public static final co p = new co("device_info", (byte) 12, 3);
    /* access modifiers changed from: private */
    public static final co q = new co("misc_info", (byte) 12, 4);
    /* access modifiers changed from: private */
    public static final co r = new co("activate_msg", (byte) 12, 5);
    /* access modifiers changed from: private */
    public static final co s = new co("instant_msgs", (byte) 15, 6);
    /* access modifiers changed from: private */
    public static final co t = new co("sessions", (byte) 15, 7);
    /* access modifiers changed from: private */
    public static final co u = new co("imprint", (byte) 12, 8);
    /* access modifiers changed from: private */
    public static final co v = new co("id_tracking", (byte) 12, 9);
    /* access modifiers changed from: private */
    public static final co w = new co("active_user", (byte) 12, 10);
    /* access modifiers changed from: private */
    public static final co x = new co("control_policy", (byte) 12, 11);
    private static final Map<Class<? extends cy>, cz> y = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public r f38a;
    public p b;
    public v c;
    public at d;
    public l e;
    public List<an> f;
    public List<be> g;
    public aj h;
    public ag i;
    public n j;
    public t k;
    private e[] z = {e.ACTIVATE_MSG, e.INSTANT_MSGS, e.SESSIONS, e.IMPRINT, e.ID_TRACKING, e.ACTIVE_USER, e.CONTROL_POLICY};

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [a.a.bi$e, a.a.cg]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        y.put(da.class, new b());
        y.put(db.class, new d());
        EnumMap enumMap = new EnumMap(e.class);
        enumMap.put((Object) e.CLIENT_STATS, (Object) new cg("client_stats", (byte) 1, new ck((byte) 12, r.class)));
        enumMap.put((Object) e.APP_INFO, (Object) new cg("app_info", (byte) 1, new ck((byte) 12, p.class)));
        enumMap.put((Object) e.DEVICE_INFO, (Object) new cg("device_info", (byte) 1, new ck((byte) 12, v.class)));
        enumMap.put((Object) e.MISC_INFO, (Object) new cg("misc_info", (byte) 1, new ck((byte) 12, at.class)));
        enumMap.put((Object) e.ACTIVATE_MSG, (Object) new cg("activate_msg", (byte) 2, new ck((byte) 12, l.class)));
        enumMap.put((Object) e.INSTANT_MSGS, (Object) new cg("instant_msgs", (byte) 2, new ci((byte) 15, new ck((byte) 12, an.class))));
        enumMap.put((Object) e.SESSIONS, (Object) new cg("sessions", (byte) 2, new ci((byte) 15, new ck((byte) 12, be.class))));
        enumMap.put((Object) e.IMPRINT, (Object) new cg("imprint", (byte) 2, new ck((byte) 12, aj.class)));
        enumMap.put((Object) e.ID_TRACKING, (Object) new cg("id_tracking", (byte) 2, new ck((byte) 12, ag.class)));
        enumMap.put((Object) e.ACTIVE_USER, (Object) new cg("active_user", (byte) 2, new ck((byte) 12, n.class)));
        enumMap.put((Object) e.CONTROL_POLICY, (Object) new cg("control_policy", (byte) 2, new ck((byte) 12, t.class)));
        l = Collections.unmodifiableMap(enumMap);
        cg.a(bi.class, l);
    }

    /* compiled from: UALogEntry */
    public enum e implements cb {
        CLIENT_STATS(1, "client_stats"),
        APP_INFO(2, "app_info"),
        DEVICE_INFO(3, "device_info"),
        MISC_INFO(4, "misc_info"),
        ACTIVATE_MSG(5, "activate_msg"),
        INSTANT_MSGS(6, "instant_msgs"),
        SESSIONS(7, "sessions"),
        IMPRINT(8, "imprint"),
        ID_TRACKING(9, "id_tracking"),
        ACTIVE_USER(10, "active_user"),
        CONTROL_POLICY(11, "control_policy");
        
        private static final Map<String, e> l = new HashMap();
        private final short m;
        private final String n;

        static {
            Iterator it = EnumSet.allOf(e.class).iterator();
            while (it.hasNext()) {
                e eVar = (e) it.next();
                l.put(eVar.b(), eVar);
            }
        }

        private e(short s, String str) {
            this.m = s;
            this.n = str;
        }

        public short a() {
            return this.m;
        }

        public String b() {
            return this.n;
        }
    }

    public bi a(r rVar) {
        this.f38a = rVar;
        return this;
    }

    public void a(boolean z2) {
        if (!z2) {
            this.f38a = null;
        }
    }

    public bi a(p pVar) {
        this.b = pVar;
        return this;
    }

    public void b(boolean z2) {
        if (!z2) {
            this.b = null;
        }
    }

    public bi a(v vVar) {
        this.c = vVar;
        return this;
    }

    public void c(boolean z2) {
        if (!z2) {
            this.c = null;
        }
    }

    public bi a(at atVar) {
        this.d = atVar;
        return this;
    }

    public void d(boolean z2) {
        if (!z2) {
            this.d = null;
        }
    }

    public bi a(l lVar) {
        this.e = lVar;
        return this;
    }

    public boolean a() {
        return this.e != null;
    }

    public void e(boolean z2) {
        if (!z2) {
            this.e = null;
        }
    }

    public int b() {
        if (this.f == null) {
            return 0;
        }
        return this.f.size();
    }

    public void a(an anVar) {
        if (this.f == null) {
            this.f = new ArrayList();
        }
        this.f.add(anVar);
    }

    public List<an> c() {
        return this.f;
    }

    public bi a(List<an> list) {
        this.f = list;
        return this;
    }

    public boolean d() {
        return this.f != null;
    }

    public void f(boolean z2) {
        if (!z2) {
            this.f = null;
        }
    }

    public void a(be beVar) {
        if (this.g == null) {
            this.g = new ArrayList();
        }
        this.g.add(beVar);
    }

    public List<be> e() {
        return this.g;
    }

    public bi b(List<be> list) {
        this.g = list;
        return this;
    }

    public boolean f() {
        return this.g != null;
    }

    public void g(boolean z2) {
        if (!z2) {
            this.g = null;
        }
    }

    public bi a(aj ajVar) {
        this.h = ajVar;
        return this;
    }

    public boolean g() {
        return this.h != null;
    }

    public void h(boolean z2) {
        if (!z2) {
            this.h = null;
        }
    }

    public bi a(ag agVar) {
        this.i = agVar;
        return this;
    }

    public boolean h() {
        return this.i != null;
    }

    public void i(boolean z2) {
        if (!z2) {
            this.i = null;
        }
    }

    public bi a(n nVar) {
        this.j = nVar;
        return this;
    }

    public boolean i() {
        return this.j != null;
    }

    public void j(boolean z2) {
        if (!z2) {
            this.j = null;
        }
    }

    public bi a(t tVar) {
        this.k = tVar;
        return this;
    }

    public boolean j() {
        return this.k != null;
    }

    public void k(boolean z2) {
        if (!z2) {
            this.k = null;
        }
    }

    public void a(cr crVar) throws ca {
        y.get(crVar.y()).b().b(crVar, this);
    }

    public void b(cr crVar) throws ca {
        y.get(crVar.y()).b().a(crVar, this);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("UALogEntry(");
        sb.append("client_stats:");
        if (this.f38a == null) {
            sb.append("null");
        } else {
            sb.append(this.f38a);
        }
        sb.append(", ");
        sb.append("app_info:");
        if (this.b == null) {
            sb.append("null");
        } else {
            sb.append(this.b);
        }
        sb.append(", ");
        sb.append("device_info:");
        if (this.c == null) {
            sb.append("null");
        } else {
            sb.append(this.c);
        }
        sb.append(", ");
        sb.append("misc_info:");
        if (this.d == null) {
            sb.append("null");
        } else {
            sb.append(this.d);
        }
        if (a()) {
            sb.append(", ");
            sb.append("activate_msg:");
            if (this.e == null) {
                sb.append("null");
            } else {
                sb.append(this.e);
            }
        }
        if (d()) {
            sb.append(", ");
            sb.append("instant_msgs:");
            if (this.f == null) {
                sb.append("null");
            } else {
                sb.append(this.f);
            }
        }
        if (f()) {
            sb.append(", ");
            sb.append("sessions:");
            if (this.g == null) {
                sb.append("null");
            } else {
                sb.append(this.g);
            }
        }
        if (g()) {
            sb.append(", ");
            sb.append("imprint:");
            if (this.h == null) {
                sb.append("null");
            } else {
                sb.append(this.h);
            }
        }
        if (h()) {
            sb.append(", ");
            sb.append("id_tracking:");
            if (this.i == null) {
                sb.append("null");
            } else {
                sb.append(this.i);
            }
        }
        if (i()) {
            sb.append(", ");
            sb.append("active_user:");
            if (this.j == null) {
                sb.append("null");
            } else {
                sb.append(this.j);
            }
        }
        if (j()) {
            sb.append(", ");
            sb.append("control_policy:");
            if (this.k == null) {
                sb.append("null");
            } else {
                sb.append(this.k);
            }
        }
        sb.append(")");
        return sb.toString();
    }

    public void k() throws ca {
        if (this.f38a == null) {
            throw new cs("Required field 'client_stats' was not present! Struct: " + toString());
        } else if (this.b == null) {
            throw new cs("Required field 'app_info' was not present! Struct: " + toString());
        } else if (this.c == null) {
            throw new cs("Required field 'device_info' was not present! Struct: " + toString());
        } else if (this.d == null) {
            throw new cs("Required field 'misc_info' was not present! Struct: " + toString());
        } else {
            if (this.f38a != null) {
                this.f38a.d();
            }
            if (this.b != null) {
                this.b.g();
            }
            if (this.c != null) {
                this.c.r();
            }
            if (this.d != null) {
                this.d.k();
            }
            if (this.e != null) {
                this.e.b();
            }
            if (this.h != null) {
                this.h.f();
            }
            if (this.i != null) {
                this.i.e();
            }
            if (this.j != null) {
                this.j.a();
            }
            if (this.k != null) {
                this.k.b();
            }
        }
    }

    /* compiled from: UALogEntry */
    private static class b implements cz {
        private b() {
        }

        /* renamed from: a */
        public a b() {
            return new a();
        }
    }

    /* compiled from: UALogEntry */
    private static class a extends da<bi> {
        private a() {
        }

        /* renamed from: a */
        public void b(cr crVar, bi biVar) throws ca {
            crVar.f();
            while (true) {
                co h = crVar.h();
                if (h.b == 0) {
                    crVar.g();
                    biVar.k();
                    return;
                }
                switch (h.c) {
                    case 1:
                        if (h.b != 12) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            biVar.f38a = new r();
                            biVar.f38a.a(crVar);
                            biVar.a(true);
                            break;
                        }
                    case 2:
                        if (h.b != 12) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            biVar.b = new p();
                            biVar.b.a(crVar);
                            biVar.b(true);
                            break;
                        }
                    case 3:
                        if (h.b != 12) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            biVar.c = new v();
                            biVar.c.a(crVar);
                            biVar.c(true);
                            break;
                        }
                    case 4:
                        if (h.b != 12) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            biVar.d = new at();
                            biVar.d.a(crVar);
                            biVar.d(true);
                            break;
                        }
                    case 5:
                        if (h.b != 12) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            biVar.e = new l();
                            biVar.e.a(crVar);
                            biVar.e(true);
                            break;
                        }
                    case 6:
                        if (h.b != 15) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            cp l = crVar.l();
                            biVar.f = new ArrayList(l.b);
                            for (int i = 0; i < l.b; i++) {
                                an anVar = new an();
                                anVar.a(crVar);
                                biVar.f.add(anVar);
                            }
                            crVar.m();
                            biVar.f(true);
                            break;
                        }
                    case 7:
                        if (h.b != 15) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            cp l2 = crVar.l();
                            biVar.g = new ArrayList(l2.b);
                            for (int i2 = 0; i2 < l2.b; i2++) {
                                be beVar = new be();
                                beVar.a(crVar);
                                biVar.g.add(beVar);
                            }
                            crVar.m();
                            biVar.g(true);
                            break;
                        }
                    case 8:
                        if (h.b != 12) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            biVar.h = new aj();
                            biVar.h.a(crVar);
                            biVar.h(true);
                            break;
                        }
                    case 9:
                        if (h.b != 12) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            biVar.i = new ag();
                            biVar.i.a(crVar);
                            biVar.i(true);
                            break;
                        }
                    case 10:
                        if (h.b != 12) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            biVar.j = new n();
                            biVar.j.a(crVar);
                            biVar.j(true);
                            break;
                        }
                    case 11:
                        if (h.b != 12) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            biVar.k = new t();
                            biVar.k.a(crVar);
                            biVar.k(true);
                            break;
                        }
                    default:
                        cu.a(crVar, h.b);
                        break;
                }
                crVar.i();
            }
        }

        /* renamed from: b */
        public void a(cr crVar, bi biVar) throws ca {
            biVar.k();
            crVar.a(bi.m);
            if (biVar.f38a != null) {
                crVar.a(bi.n);
                biVar.f38a.b(crVar);
                crVar.b();
            }
            if (biVar.b != null) {
                crVar.a(bi.o);
                biVar.b.b(crVar);
                crVar.b();
            }
            if (biVar.c != null) {
                crVar.a(bi.p);
                biVar.c.b(crVar);
                crVar.b();
            }
            if (biVar.d != null) {
                crVar.a(bi.q);
                biVar.d.b(crVar);
                crVar.b();
            }
            if (biVar.e != null && biVar.a()) {
                crVar.a(bi.r);
                biVar.e.b(crVar);
                crVar.b();
            }
            if (biVar.f != null && biVar.d()) {
                crVar.a(bi.s);
                crVar.a(new cp((byte) 12, biVar.f.size()));
                for (an b : biVar.f) {
                    b.b(crVar);
                }
                crVar.e();
                crVar.b();
            }
            if (biVar.g != null && biVar.f()) {
                crVar.a(bi.t);
                crVar.a(new cp((byte) 12, biVar.g.size()));
                for (be b2 : biVar.g) {
                    b2.b(crVar);
                }
                crVar.e();
                crVar.b();
            }
            if (biVar.h != null && biVar.g()) {
                crVar.a(bi.u);
                biVar.h.b(crVar);
                crVar.b();
            }
            if (biVar.i != null && biVar.h()) {
                crVar.a(bi.v);
                biVar.i.b(crVar);
                crVar.b();
            }
            if (biVar.j != null && biVar.i()) {
                crVar.a(bi.w);
                biVar.j.b(crVar);
                crVar.b();
            }
            if (biVar.k != null && biVar.j()) {
                crVar.a(bi.x);
                biVar.k.b(crVar);
                crVar.b();
            }
            crVar.c();
            crVar.a();
        }
    }

    /* compiled from: UALogEntry */
    private static class d implements cz {
        private d() {
        }

        /* renamed from: a */
        public c b() {
            return new c();
        }
    }

    /* compiled from: UALogEntry */
    private static class c extends db<bi> {
        private c() {
        }

        public void a(cr crVar, bi biVar) throws ca {
            cx cxVar = (cx) crVar;
            biVar.f38a.b(cxVar);
            biVar.b.b(cxVar);
            biVar.c.b(cxVar);
            biVar.d.b(cxVar);
            BitSet bitSet = new BitSet();
            if (biVar.a()) {
                bitSet.set(0);
            }
            if (biVar.d()) {
                bitSet.set(1);
            }
            if (biVar.f()) {
                bitSet.set(2);
            }
            if (biVar.g()) {
                bitSet.set(3);
            }
            if (biVar.h()) {
                bitSet.set(4);
            }
            if (biVar.i()) {
                bitSet.set(5);
            }
            if (biVar.j()) {
                bitSet.set(6);
            }
            cxVar.a(bitSet, 7);
            if (biVar.a()) {
                biVar.e.b(cxVar);
            }
            if (biVar.d()) {
                cxVar.a(biVar.f.size());
                for (an b : biVar.f) {
                    b.b(cxVar);
                }
            }
            if (biVar.f()) {
                cxVar.a(biVar.g.size());
                for (be b2 : biVar.g) {
                    b2.b(cxVar);
                }
            }
            if (biVar.g()) {
                biVar.h.b(cxVar);
            }
            if (biVar.h()) {
                biVar.i.b(cxVar);
            }
            if (biVar.i()) {
                biVar.j.b(cxVar);
            }
            if (biVar.j()) {
                biVar.k.b(cxVar);
            }
        }

        public void b(cr crVar, bi biVar) throws ca {
            cx cxVar = (cx) crVar;
            biVar.f38a = new r();
            biVar.f38a.a(cxVar);
            biVar.a(true);
            biVar.b = new p();
            biVar.b.a(cxVar);
            biVar.b(true);
            biVar.c = new v();
            biVar.c.a(cxVar);
            biVar.c(true);
            biVar.d = new at();
            biVar.d.a(cxVar);
            biVar.d(true);
            BitSet b = cxVar.b(7);
            if (b.get(0)) {
                biVar.e = new l();
                biVar.e.a(cxVar);
                biVar.e(true);
            }
            if (b.get(1)) {
                cp cpVar = new cp((byte) 12, cxVar.s());
                biVar.f = new ArrayList(cpVar.b);
                for (int i = 0; i < cpVar.b; i++) {
                    an anVar = new an();
                    anVar.a(cxVar);
                    biVar.f.add(anVar);
                }
                biVar.f(true);
            }
            if (b.get(2)) {
                cp cpVar2 = new cp((byte) 12, cxVar.s());
                biVar.g = new ArrayList(cpVar2.b);
                for (int i2 = 0; i2 < cpVar2.b; i2++) {
                    be beVar = new be();
                    beVar.a(cxVar);
                    biVar.g.add(beVar);
                }
                biVar.g(true);
            }
            if (b.get(3)) {
                biVar.h = new aj();
                biVar.h.a(cxVar);
                biVar.h(true);
            }
            if (b.get(4)) {
                biVar.i = new ag();
                biVar.i.a(cxVar);
                biVar.i(true);
            }
            if (b.get(5)) {
                biVar.j = new n();
                biVar.j.a(cxVar);
                biVar.j(true);
            }
            if (b.get(6)) {
                biVar.k = new t();
                biVar.k.a(cxVar);
                biVar.k(true);
            }
        }
    }
}
