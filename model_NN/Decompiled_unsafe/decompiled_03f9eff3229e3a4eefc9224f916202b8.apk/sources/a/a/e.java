package a.a;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* compiled from: ViewPageTracker */
public class e {

    /* renamed from: a  reason: collision with root package name */
    private final Map<String, Long> f92a = new HashMap();
    private final ArrayList<c> b = new ArrayList<>();

    public void a(String str) {
        if (!TextUtils.isEmpty(str)) {
            synchronized (this.f92a) {
                this.f92a.put(str, Long.valueOf(System.currentTimeMillis()));
            }
        }
    }

    public void b(String str) {
        Long remove;
        if (!TextUtils.isEmpty(str)) {
            synchronized (this.f92a) {
                remove = this.f92a.remove(str);
            }
            if (remove == null) {
                bn.d("MobclickAgent", String.format("please call 'onPageStart(%s)' before onPageEnd", str));
                return;
            }
            long currentTimeMillis = System.currentTimeMillis() - remove.longValue();
            synchronized (this.b) {
                this.b.add(new c(str, currentTimeMillis));
            }
        }
    }

    public void a() {
        long j;
        String str;
        String str2 = null;
        long j2 = 0;
        synchronized (this.f92a) {
            for (Map.Entry next : this.f92a.entrySet()) {
                if (((Long) next.getValue()).longValue() > j2) {
                    long longValue = ((Long) next.getValue()).longValue();
                    str = (String) next.getKey();
                    j = longValue;
                } else {
                    j = j2;
                    str = str2;
                }
                str2 = str;
                j2 = j;
            }
        }
        if (str2 != null) {
            b(str2);
        }
    }

    public void a(Context context) {
        SharedPreferences a2 = eb.a(context);
        SharedPreferences.Editor edit = a2.edit();
        if (this.b.size() > 0) {
            String string = a2.getString("activities", "");
            StringBuilder sb = new StringBuilder();
            if (!TextUtils.isEmpty(string)) {
                sb.append(string);
                sb.append(";");
            }
            synchronized (this.b) {
                Iterator<c> it = this.b.iterator();
                while (it.hasNext()) {
                    c next = it.next();
                    sb.append(String.format("[\"%s\",%d]", next.f51a, Long.valueOf(next.b)));
                    sb.append(";");
                }
                this.b.clear();
            }
            sb.deleteCharAt(sb.length() - 1);
            edit.remove("activities");
            edit.putString("activities", sb.toString());
        }
        edit.commit();
    }

    public static List<av> a(SharedPreferences sharedPreferences) {
        String string = sharedPreferences.getString("activities", "");
        if ("".equals(string)) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        try {
            String[] split = string.split(";");
            for (String str : split) {
                if (!TextUtils.isEmpty(str)) {
                    arrayList.add(new h(str));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (arrayList.size() > 0) {
            return arrayList;
        }
        return null;
    }
}
