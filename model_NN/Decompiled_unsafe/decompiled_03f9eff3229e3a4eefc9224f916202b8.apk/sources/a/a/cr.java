package a.a;

import java.nio.ByteBuffer;

/* compiled from: TProtocol */
public abstract class cr {
    protected de e;

    public abstract void a() throws ca;

    public abstract void a(double d) throws ca;

    public abstract void a(int i) throws ca;

    public abstract void a(long j) throws ca;

    public abstract void a(co coVar) throws ca;

    public abstract void a(cp cpVar) throws ca;

    public abstract void a(cq cqVar) throws ca;

    public abstract void a(cw cwVar) throws ca;

    public abstract void a(String str) throws ca;

    public abstract void a(ByteBuffer byteBuffer) throws ca;

    public abstract void a(short s) throws ca;

    public abstract void a(boolean z) throws ca;

    public abstract void b() throws ca;

    public abstract void c() throws ca;

    public abstract void d() throws ca;

    public abstract void e() throws ca;

    public abstract cw f() throws ca;

    public abstract void g() throws ca;

    public abstract co h() throws ca;

    public abstract void i() throws ca;

    public abstract cq j() throws ca;

    public abstract void k() throws ca;

    public abstract cp l() throws ca;

    public abstract void m() throws ca;

    public abstract cv n() throws ca;

    public abstract void o() throws ca;

    public abstract boolean p() throws ca;

    public abstract byte q() throws ca;

    public abstract short r() throws ca;

    public abstract int s() throws ca;

    public abstract long t() throws ca;

    public abstract double u() throws ca;

    public abstract String v() throws ca;

    public abstract ByteBuffer w() throws ca;

    protected cr(de deVar) {
        this.e = deVar;
    }

    public void x() {
    }

    public Class<? extends cy> y() {
        return da.class;
    }
}
