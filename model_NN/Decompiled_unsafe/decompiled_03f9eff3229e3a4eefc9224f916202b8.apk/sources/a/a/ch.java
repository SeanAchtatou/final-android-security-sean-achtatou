package a.a;

import java.io.Serializable;

/* compiled from: FieldValueMetaData */
public class ch implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private final boolean f56a;
    public final byte b;
    private final String c;
    private final boolean d;

    public ch(byte b2, boolean z) {
        this.b = b2;
        this.f56a = false;
        this.c = null;
        this.d = z;
    }

    public ch(byte b2) {
        this(b2, false);
    }
}
