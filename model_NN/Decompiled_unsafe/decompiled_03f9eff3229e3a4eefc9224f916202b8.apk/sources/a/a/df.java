package a.a;

/* compiled from: TTransportException */
public class df extends ca {

    /* renamed from: a  reason: collision with root package name */
    protected int f75a = 0;

    public df() {
    }

    public df(int i) {
        this.f75a = i;
    }

    public df(int i, String str) {
        super(str);
        this.f75a = i;
    }

    public df(String str) {
        super(str);
    }

    public df(int i, Throwable th) {
        super(th);
        this.f75a = i;
    }
}
