package a.a;

import android.content.Context;
import android.content.SharedPreferences;
import com.umeng.analytics.a;
import java.util.Arrays;
import java.util.List;

/* compiled from: SessionTracker */
public class ee {
    private static String c = null;

    /* renamed from: a  reason: collision with root package name */
    private final String f95a = "a_start_time";
    private final String b = "a_end_time";

    public i a(Context context) {
        SharedPreferences a2 = eb.a(context);
        String string = a2.getString("session_id", null);
        if (string == null) {
            return null;
        }
        long j = a2.getLong("session_start_time", 0);
        long j2 = a2.getLong("session_end_time", 0);
        long j3 = 0;
        if (j2 != 0) {
            j3 = j2 - j;
            if (Math.abs(j3) > LogBuilder.MAX_INTERVAL) {
                j3 = 0;
            }
        }
        i iVar = new i();
        iVar.a(string);
        iVar.a(j);
        iVar.b(j2);
        iVar.c(j3);
        double[] b2 = a.b();
        if (b2 != null) {
            ar arVar = new ar(b2[0], b2[1], System.currentTimeMillis());
            if (iVar.f()) {
                iVar.a(arVar);
            } else {
                iVar.b(Arrays.asList(arVar));
            }
        }
        bg a3 = d.a(context);
        if (a3 != null) {
            iVar.a(a3);
        }
        List<av> a4 = e.a(a2);
        if (a4 != null && a4.size() > 0) {
            iVar.a(a4);
        }
        a(a2);
        return iVar;
    }

    private void a(SharedPreferences sharedPreferences) {
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.remove("session_start_time");
        edit.remove("session_end_time");
        edit.remove("a_start_time");
        edit.remove("a_end_time");
        edit.putString("activities", "");
        edit.commit();
    }

    public String b(Context context) {
        String c2 = bm.c(context);
        String a2 = a.a(context);
        long currentTimeMillis = System.currentTimeMillis();
        if (a2 == null) {
            throw new RuntimeException("Appkey is null or empty, Please check AndroidManifest.xml");
        }
        StringBuilder sb = new StringBuilder();
        sb.append(currentTimeMillis).append(a2).append(c2);
        c = bt.a(sb.toString());
        return c;
    }

    public void c(Context context) {
        SharedPreferences a2 = eb.a(context);
        if (a2 != null) {
            if (b(a2)) {
                bn.a("MobclickAgent", "Start new session: " + a(context, a2));
                return;
            }
            String string = a2.getString("session_id", null);
            SharedPreferences.Editor edit = a2.edit();
            edit.putLong("a_start_time", System.currentTimeMillis());
            edit.putLong("a_end_time", 0);
            edit.commit();
            bn.a("MobclickAgent", "Extend current session: " + string);
        }
    }

    public void d(Context context) {
        SharedPreferences a2 = eb.a(context);
        if (a2 != null) {
            if (a2.getLong("a_start_time", 0) != 0 || !a.h) {
                long currentTimeMillis = System.currentTimeMillis();
                SharedPreferences.Editor edit = a2.edit();
                edit.putLong("a_start_time", 0);
                edit.putLong("a_end_time", currentTimeMillis);
                edit.putLong("session_end_time", currentTimeMillis);
                edit.commit();
                return;
            }
            bn.b("MobclickAgent", "onPause called before onResume");
        }
    }

    private boolean b(SharedPreferences sharedPreferences) {
        long j = sharedPreferences.getLong("a_start_time", 0);
        long j2 = sharedPreferences.getLong("a_end_time", 0);
        long currentTimeMillis = System.currentTimeMillis();
        if (j != 0 && currentTimeMillis - j < a.l) {
            bn.b("MobclickAgent", "onResume called before onPause");
            return false;
        } else if (currentTimeMillis - j2 > a.l) {
            return true;
        } else {
            return false;
        }
    }

    private String a(Context context, SharedPreferences sharedPreferences) {
        Cdo a2 = Cdo.a(context);
        String b2 = b(context);
        i a3 = a(context);
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString("session_id", b2);
        edit.putLong("session_start_time", System.currentTimeMillis());
        edit.putLong("session_end_time", 0);
        edit.putLong("a_start_time", System.currentTimeMillis());
        edit.putLong("a_end_time", 0);
        edit.commit();
        if (a3 != null) {
            a2.a(a3);
        } else {
            a2.a((dv) null);
        }
        return b2;
    }

    public static String e(Context context) {
        if (c == null) {
            c = eb.a(context).getString("session_id", null);
        }
        return c;
    }
}
