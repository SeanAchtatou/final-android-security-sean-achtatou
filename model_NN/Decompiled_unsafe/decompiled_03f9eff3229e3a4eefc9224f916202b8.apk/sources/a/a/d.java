package a.a;

import android.content.Context;
import android.content.SharedPreferences;
import java.lang.reflect.Method;

/* compiled from: TrafficTracker */
public class d {
    public static bg a(Context context) {
        try {
            bg bgVar = new bg();
            long[] b = b(context);
            if (b[0] <= 0 || b[1] <= 0) {
                return null;
            }
            SharedPreferences a2 = eb.a(context);
            long j = a2.getLong("uptr", -1);
            long j2 = a2.getLong("dntr", -1);
            a2.edit().putLong("uptr", b[1]).putLong("dntr", b[0]).commit();
            if (j <= 0 || j2 <= 0) {
                return null;
            }
            b[0] = b[0] - j2;
            b[1] = b[1] - j;
            if (b[0] <= 0 || b[1] <= 0) {
                return null;
            }
            bgVar.b((int) b[0]);
            bgVar.a((int) b[1]);
            return bgVar;
        } catch (Exception e) {
            bn.d("MobclickAgent", "sdk less than 2.2 has get no traffic");
            return null;
        }
    }

    private static long[] b(Context context) throws Exception {
        Class<?> cls = Class.forName("android.net.TrafficStats");
        Method method = cls.getMethod("getUidRxBytes", Integer.TYPE);
        Method method2 = cls.getMethod("getUidTxBytes", Integer.TYPE);
        int i = context.getApplicationInfo().uid;
        if (i == -1) {
            return null;
        }
        return new long[]{((Long) method.invoke(null, Integer.valueOf(i))).longValue(), ((Long) method2.invoke(null, Integer.valueOf(i))).longValue()};
    }
}
