package a.a;

/* compiled from: TProtocolUtil */
public class cu {

    /* renamed from: a  reason: collision with root package name */
    private static int f70a = Integer.MAX_VALUE;

    public static void a(cr crVar, byte b) throws ca {
        a(crVar, b, f70a);
    }

    public static void a(cr crVar, byte b, int i) throws ca {
        int i2 = 0;
        if (i <= 0) {
            throw new ca("Maximum skip depth exceeded");
        }
        switch (b) {
            case 2:
                crVar.p();
                return;
            case 3:
                crVar.q();
                return;
            case 4:
                crVar.u();
                return;
            case 5:
            case 7:
            case 9:
            default:
                return;
            case 6:
                crVar.r();
                return;
            case 8:
                crVar.s();
                return;
            case 10:
                crVar.t();
                return;
            case 11:
                crVar.w();
                return;
            case 12:
                crVar.f();
                while (true) {
                    co h = crVar.h();
                    if (h.b == 0) {
                        crVar.g();
                        return;
                    } else {
                        a(crVar, h.b, i - 1);
                        crVar.i();
                    }
                }
            case 13:
                cq j = crVar.j();
                while (i2 < j.c) {
                    a(crVar, j.f68a, i - 1);
                    a(crVar, j.b, i - 1);
                    i2++;
                }
                crVar.k();
                return;
            case 14:
                cv n = crVar.n();
                while (i2 < n.b) {
                    a(crVar, n.f71a, i - 1);
                    i2++;
                }
                crVar.o();
                return;
            case 15:
                cp l = crVar.l();
                while (i2 < l.b) {
                    a(crVar, l.f67a, i - 1);
                    i2++;
                }
                crVar.m();
                return;
        }
    }
}
