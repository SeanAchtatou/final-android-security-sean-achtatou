package a.a;

import android.content.Context;
import android.content.SharedPreferences;
import com.umeng.analytics.m;

/* compiled from: StatTracer */
public class b implements dw {

    /* renamed from: a  reason: collision with root package name */
    public int f30a;
    public int b;
    public long c;
    private final int d = 3600000;
    private int e;
    private long f = 0;
    private long g = 0;
    private Context h;

    public b(Context context) {
        b(context);
    }

    private void b(Context context) {
        this.h = context.getApplicationContext();
        SharedPreferences a2 = eb.a(context);
        this.f30a = a2.getInt("successful_request", 0);
        this.b = a2.getInt("failed_requests ", 0);
        this.e = a2.getInt("last_request_spent_ms", 0);
        this.c = a2.getLong("last_request_time", 0);
        this.f = a2.getLong("last_req", 0);
    }

    public boolean a() {
        boolean z;
        boolean z2;
        if (this.c == 0) {
            z = true;
        } else {
            z = false;
        }
        if (!m.a(this.h).g()) {
            z2 = true;
        } else {
            z2 = false;
        }
        if (!z || !z2) {
            return false;
        }
        return true;
    }

    public void b() {
        this.f30a++;
        this.c = this.f;
    }

    public void c() {
        this.b++;
    }

    public void d() {
        this.f = System.currentTimeMillis();
    }

    public void e() {
        this.e = (int) (System.currentTimeMillis() - this.f);
    }

    public void f() {
        eb.a(this.h).edit().putInt("successful_request", this.f30a).putInt("failed_requests ", this.b).putInt("last_request_spent_ms", this.e).putLong("last_request_time", this.c).putLong("last_req", this.f).commit();
    }

    public void g() {
        eb.a(this.h).edit().putLong("first_activate_time", System.currentTimeMillis()).commit();
    }

    public boolean h() {
        if (this.g == 0) {
            this.g = eb.a(this.h).getLong("first_activate_time", 0);
        }
        return this.g == 0;
    }

    public long i() {
        return h() ? System.currentTimeMillis() : this.g;
    }

    public long j() {
        return this.f;
    }

    public static r a(Context context) {
        SharedPreferences a2 = eb.a(context);
        r rVar = new r();
        rVar.b(a2.getInt("failed_requests ", 0));
        rVar.c(a2.getInt("last_request_spent_ms", 0));
        rVar.a(a2.getInt("successful_request", 0));
        return rVar;
    }

    public void k() {
        d();
    }

    public void l() {
        e();
    }

    public void m() {
        b();
    }

    public void n() {
        c();
    }
}
