package a.a;

import com.tencent.stat.DeviceInfo;
import java.io.Serializable;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* compiled from: IdJournal */
public class ac implements bw<ac, e>, Serializable, Cloneable {
    public static final Map<e, cg> e;
    /* access modifiers changed from: private */
    public static final cw f = new cw("IdJournal");
    /* access modifiers changed from: private */
    public static final co g = new co("domain", (byte) 11, 1);
    /* access modifiers changed from: private */
    public static final co h = new co("old_id", (byte) 11, 2);
    /* access modifiers changed from: private */
    public static final co i = new co("new_id", (byte) 11, 3);
    /* access modifiers changed from: private */
    public static final co j = new co(DeviceInfo.TAG_TIMESTAMPS, (byte) 10, 4);
    private static final Map<Class<? extends cy>, cz> k = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public String f4a;
    public String b;
    public String c;
    public long d;
    private byte l = 0;
    private e[] m = {e.OLD_ID};

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [a.a.ac$e, a.a.cg]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        k.put(da.class, new b());
        k.put(db.class, new d());
        EnumMap enumMap = new EnumMap(e.class);
        enumMap.put((Object) e.DOMAIN, (Object) new cg("domain", (byte) 1, new ch((byte) 11)));
        enumMap.put((Object) e.OLD_ID, (Object) new cg("old_id", (byte) 2, new ch((byte) 11)));
        enumMap.put((Object) e.NEW_ID, (Object) new cg("new_id", (byte) 1, new ch((byte) 11)));
        enumMap.put((Object) e.TS, (Object) new cg(DeviceInfo.TAG_TIMESTAMPS, (byte) 1, new ch((byte) 10)));
        e = Collections.unmodifiableMap(enumMap);
        cg.a(ac.class, e);
    }

    /* compiled from: IdJournal */
    public enum e implements cb {
        DOMAIN(1, "domain"),
        OLD_ID(2, "old_id"),
        NEW_ID(3, "new_id"),
        TS(4, DeviceInfo.TAG_TIMESTAMPS);
        
        private static final Map<String, e> e = new HashMap();
        private final short f;
        private final String g;

        static {
            Iterator it = EnumSet.allOf(e.class).iterator();
            while (it.hasNext()) {
                e eVar = (e) it.next();
                e.put(eVar.b(), eVar);
            }
        }

        private e(short s, String str) {
            this.f = s;
            this.g = str;
        }

        public short a() {
            return this.f;
        }

        public String b() {
            return this.g;
        }
    }

    public ac a(String str) {
        this.f4a = str;
        return this;
    }

    public void a(boolean z) {
        if (!z) {
            this.f4a = null;
        }
    }

    public ac b(String str) {
        this.b = str;
        return this;
    }

    public boolean a() {
        return this.b != null;
    }

    public void b(boolean z) {
        if (!z) {
            this.b = null;
        }
    }

    public ac c(String str) {
        this.c = str;
        return this;
    }

    public void c(boolean z) {
        if (!z) {
            this.c = null;
        }
    }

    public ac a(long j2) {
        this.d = j2;
        d(true);
        return this;
    }

    public boolean b() {
        return bu.a(this.l, 0);
    }

    public void d(boolean z) {
        this.l = bu.a(this.l, 0, z);
    }

    public void a(cr crVar) throws ca {
        k.get(crVar.y()).b().b(crVar, this);
    }

    public void b(cr crVar) throws ca {
        k.get(crVar.y()).b().a(crVar, this);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("IdJournal(");
        sb.append("domain:");
        if (this.f4a == null) {
            sb.append("null");
        } else {
            sb.append(this.f4a);
        }
        if (a()) {
            sb.append(", ");
            sb.append("old_id:");
            if (this.b == null) {
                sb.append("null");
            } else {
                sb.append(this.b);
            }
        }
        sb.append(", ");
        sb.append("new_id:");
        if (this.c == null) {
            sb.append("null");
        } else {
            sb.append(this.c);
        }
        sb.append(", ");
        sb.append("ts:");
        sb.append(this.d);
        sb.append(")");
        return sb.toString();
    }

    public void c() throws ca {
        if (this.f4a == null) {
            throw new cs("Required field 'domain' was not present! Struct: " + toString());
        } else if (this.c == null) {
            throw new cs("Required field 'new_id' was not present! Struct: " + toString());
        }
    }

    /* compiled from: IdJournal */
    private static class b implements cz {
        private b() {
        }

        /* renamed from: a */
        public a b() {
            return new a();
        }
    }

    /* compiled from: IdJournal */
    private static class a extends da<ac> {
        private a() {
        }

        /* renamed from: a */
        public void b(cr crVar, ac acVar) throws ca {
            crVar.f();
            while (true) {
                co h = crVar.h();
                if (h.b == 0) {
                    crVar.g();
                    if (!acVar.b()) {
                        throw new cs("Required field 'ts' was not found in serialized data! Struct: " + toString());
                    }
                    acVar.c();
                    return;
                }
                switch (h.c) {
                    case 1:
                        if (h.b != 11) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            acVar.f4a = crVar.v();
                            acVar.a(true);
                            break;
                        }
                    case 2:
                        if (h.b != 11) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            acVar.b = crVar.v();
                            acVar.b(true);
                            break;
                        }
                    case 3:
                        if (h.b != 11) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            acVar.c = crVar.v();
                            acVar.c(true);
                            break;
                        }
                    case 4:
                        if (h.b != 10) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            acVar.d = crVar.t();
                            acVar.d(true);
                            break;
                        }
                    default:
                        cu.a(crVar, h.b);
                        break;
                }
                crVar.i();
            }
        }

        /* renamed from: b */
        public void a(cr crVar, ac acVar) throws ca {
            acVar.c();
            crVar.a(ac.f);
            if (acVar.f4a != null) {
                crVar.a(ac.g);
                crVar.a(acVar.f4a);
                crVar.b();
            }
            if (acVar.b != null && acVar.a()) {
                crVar.a(ac.h);
                crVar.a(acVar.b);
                crVar.b();
            }
            if (acVar.c != null) {
                crVar.a(ac.i);
                crVar.a(acVar.c);
                crVar.b();
            }
            crVar.a(ac.j);
            crVar.a(acVar.d);
            crVar.b();
            crVar.c();
            crVar.a();
        }
    }

    /* compiled from: IdJournal */
    private static class d implements cz {
        private d() {
        }

        /* renamed from: a */
        public c b() {
            return new c();
        }
    }

    /* compiled from: IdJournal */
    private static class c extends db<ac> {
        private c() {
        }

        public void a(cr crVar, ac acVar) throws ca {
            cx cxVar = (cx) crVar;
            cxVar.a(acVar.f4a);
            cxVar.a(acVar.c);
            cxVar.a(acVar.d);
            BitSet bitSet = new BitSet();
            if (acVar.a()) {
                bitSet.set(0);
            }
            cxVar.a(bitSet, 1);
            if (acVar.a()) {
                cxVar.a(acVar.b);
            }
        }

        public void b(cr crVar, ac acVar) throws ca {
            cx cxVar = (cx) crVar;
            acVar.f4a = cxVar.v();
            acVar.a(true);
            acVar.c = cxVar.v();
            acVar.c(true);
            acVar.d = cxVar.t();
            acVar.d(true);
            if (cxVar.b(1).get(0)) {
                acVar.b = cxVar.v();
                acVar.b(true);
            }
        }
    }
}
