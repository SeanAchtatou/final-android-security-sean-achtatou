package a.a;

import java.io.Serializable;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* compiled from: Imprint */
public class aj implements bw<aj, e>, Serializable, Cloneable {
    public static final Map<e, cg> d;
    /* access modifiers changed from: private */
    public static final cw e = new cw("Imprint");
    /* access modifiers changed from: private */
    public static final co f = new co("property", (byte) 13, 1);
    /* access modifiers changed from: private */
    public static final co g = new co("version", (byte) 8, 2);
    /* access modifiers changed from: private */
    public static final co h = new co("checksum", (byte) 11, 3);
    private static final Map<Class<? extends cy>, cz> i = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public Map<String, al> f11a;
    public int b;
    public String c;
    private byte j = 0;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [a.a.aj$e, a.a.cg]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        i.put(da.class, new b());
        i.put(db.class, new d());
        EnumMap enumMap = new EnumMap(e.class);
        enumMap.put((Object) e.PROPERTY, (Object) new cg("property", (byte) 1, new cj((byte) 13, new ch((byte) 11), new ck((byte) 12, al.class))));
        enumMap.put((Object) e.VERSION, (Object) new cg("version", (byte) 1, new ch((byte) 8)));
        enumMap.put((Object) e.CHECKSUM, (Object) new cg("checksum", (byte) 1, new ch((byte) 11)));
        d = Collections.unmodifiableMap(enumMap);
        cg.a(aj.class, d);
    }

    /* compiled from: Imprint */
    public enum e implements cb {
        PROPERTY(1, "property"),
        VERSION(2, "version"),
        CHECKSUM(3, "checksum");
        
        private static final Map<String, e> d = new HashMap();
        private final short e;
        private final String f;

        static {
            Iterator it = EnumSet.allOf(e.class).iterator();
            while (it.hasNext()) {
                e eVar = (e) it.next();
                d.put(eVar.b(), eVar);
            }
        }

        private e(short s, String str) {
            this.e = s;
            this.f = str;
        }

        public short a() {
            return this.e;
        }

        public String b() {
            return this.f;
        }
    }

    public Map<String, al> a() {
        return this.f11a;
    }

    public boolean b() {
        return this.f11a != null;
    }

    public void a(boolean z) {
        if (!z) {
            this.f11a = null;
        }
    }

    public int c() {
        return this.b;
    }

    public aj a(int i2) {
        this.b = i2;
        b(true);
        return this;
    }

    public boolean d() {
        return bu.a(this.j, 0);
    }

    public void b(boolean z) {
        this.j = bu.a(this.j, 0, z);
    }

    public String e() {
        return this.c;
    }

    public aj a(String str) {
        this.c = str;
        return this;
    }

    public void c(boolean z) {
        if (!z) {
            this.c = null;
        }
    }

    public void a(cr crVar) throws ca {
        i.get(crVar.y()).b().b(crVar, this);
    }

    public void b(cr crVar) throws ca {
        i.get(crVar.y()).b().a(crVar, this);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("Imprint(");
        sb.append("property:");
        if (this.f11a == null) {
            sb.append("null");
        } else {
            sb.append(this.f11a);
        }
        sb.append(", ");
        sb.append("version:");
        sb.append(this.b);
        sb.append(", ");
        sb.append("checksum:");
        if (this.c == null) {
            sb.append("null");
        } else {
            sb.append(this.c);
        }
        sb.append(")");
        return sb.toString();
    }

    public void f() throws ca {
        if (this.f11a == null) {
            throw new cs("Required field 'property' was not present! Struct: " + toString());
        } else if (this.c == null) {
            throw new cs("Required field 'checksum' was not present! Struct: " + toString());
        }
    }

    /* compiled from: Imprint */
    private static class b implements cz {
        private b() {
        }

        /* renamed from: a */
        public a b() {
            return new a();
        }
    }

    /* compiled from: Imprint */
    private static class a extends da<aj> {
        private a() {
        }

        /* renamed from: a */
        public void b(cr crVar, aj ajVar) throws ca {
            crVar.f();
            while (true) {
                co h = crVar.h();
                if (h.b == 0) {
                    crVar.g();
                    if (!ajVar.d()) {
                        throw new cs("Required field 'version' was not found in serialized data! Struct: " + toString());
                    }
                    ajVar.f();
                    return;
                }
                switch (h.c) {
                    case 1:
                        if (h.b != 13) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            cq j = crVar.j();
                            ajVar.f11a = new HashMap(j.c * 2);
                            for (int i = 0; i < j.c; i++) {
                                String v = crVar.v();
                                al alVar = new al();
                                alVar.a(crVar);
                                ajVar.f11a.put(v, alVar);
                            }
                            crVar.k();
                            ajVar.a(true);
                            break;
                        }
                    case 2:
                        if (h.b != 8) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            ajVar.b = crVar.s();
                            ajVar.b(true);
                            break;
                        }
                    case 3:
                        if (h.b != 11) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            ajVar.c = crVar.v();
                            ajVar.c(true);
                            break;
                        }
                    default:
                        cu.a(crVar, h.b);
                        break;
                }
                crVar.i();
            }
        }

        /* renamed from: b */
        public void a(cr crVar, aj ajVar) throws ca {
            ajVar.f();
            crVar.a(aj.e);
            if (ajVar.f11a != null) {
                crVar.a(aj.f);
                crVar.a(new cq((byte) 11, (byte) 12, ajVar.f11a.size()));
                for (Map.Entry next : ajVar.f11a.entrySet()) {
                    crVar.a((String) next.getKey());
                    ((al) next.getValue()).b(crVar);
                }
                crVar.d();
                crVar.b();
            }
            crVar.a(aj.g);
            crVar.a(ajVar.b);
            crVar.b();
            if (ajVar.c != null) {
                crVar.a(aj.h);
                crVar.a(ajVar.c);
                crVar.b();
            }
            crVar.c();
            crVar.a();
        }
    }

    /* compiled from: Imprint */
    private static class d implements cz {
        private d() {
        }

        /* renamed from: a */
        public c b() {
            return new c();
        }
    }

    /* compiled from: Imprint */
    private static class c extends db<aj> {
        private c() {
        }

        public void a(cr crVar, aj ajVar) throws ca {
            cx cxVar = (cx) crVar;
            cxVar.a(ajVar.f11a.size());
            for (Map.Entry next : ajVar.f11a.entrySet()) {
                cxVar.a((String) next.getKey());
                ((al) next.getValue()).b(cxVar);
            }
            cxVar.a(ajVar.b);
            cxVar.a(ajVar.c);
        }

        public void b(cr crVar, aj ajVar) throws ca {
            cx cxVar = (cx) crVar;
            cq cqVar = new cq((byte) 11, (byte) 12, cxVar.s());
            ajVar.f11a = new HashMap(cqVar.c * 2);
            for (int i = 0; i < cqVar.c; i++) {
                String v = cxVar.v();
                al alVar = new al();
                alVar.a(cxVar);
                ajVar.f11a.put(v, alVar);
            }
            ajVar.a(true);
            ajVar.b = cxVar.s();
            ajVar.b(true);
            ajVar.c = cxVar.v();
            ajVar.c(true);
        }
    }
}
