package a.a;

import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import com.umeng.analytics.a;
import com.umeng.analytics.j;
import java.util.ArrayList;
import java.util.List;

/* compiled from: MemoCache */
public class dx {

    /* renamed from: a  reason: collision with root package name */
    private List<dv> f90a = new ArrayList();
    private l b = null;
    private p c = null;
    private v d = null;
    private at e = null;
    private Context f = null;

    public dx(Context context) {
        this.f = context;
    }

    public synchronized int a() {
        int size;
        size = this.f90a.size();
        if (this.b != null) {
            size++;
        }
        return size;
    }

    public synchronized void a(dv dvVar) {
        this.f90a.add(dvVar);
    }

    public void a(bi biVar) {
        String e2 = ee.e(this.f);
        if (e2 != null) {
            synchronized (this) {
                if (this.b != null && new b(this.f).a()) {
                    biVar.a(this.b);
                    this.b = null;
                }
                for (dv a2 : this.f90a) {
                    a2.a(biVar, e2);
                }
                this.f90a.clear();
            }
            biVar.a(b());
            biVar.a(c());
            biVar.a(d());
            biVar.a(g());
            biVar.a(e());
            biVar.a(f());
            biVar.a(h());
        }
    }

    public synchronized void a(l lVar) {
        this.b = lVar;
    }

    public synchronized p b() {
        if (this.c == null) {
            this.c = new p();
            a(this.f);
        }
        return this.c;
    }

    public synchronized v c() {
        if (this.d == null) {
            this.d = new v();
            b(this.f);
        }
        return this.d;
    }

    public synchronized at d() {
        if (this.e == null) {
            this.e = new at();
            c(this.f);
        }
        return this.e;
    }

    public aj e() {
        try {
            return dh.a(this.f).a();
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public ag f() {
        try {
            return cm.a(this.f).b();
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public r g() {
        try {
            return b.a(this.f);
        } catch (Exception e2) {
            e2.printStackTrace();
            return new r();
        }
    }

    public n h() {
        String[] a2 = j.a(this.f);
        if (a2 == null || TextUtils.isEmpty(a2[0]) || TextUtils.isEmpty(a2[1])) {
            return null;
        }
        return new n(a2[0], a2[1]);
    }

    private void a(Context context) {
        try {
            this.c.a(a.a(context));
            this.c.e(a.b(context));
            if (!(a.f2136a == null || a.b == null)) {
                this.c.f(a.f2136a);
                this.c.g(a.b);
            }
            this.c.c(bm.o(context));
            this.c.a(bd.ANDROID);
            this.c.d("5.5.3");
            this.c.b(bm.b(context));
            this.c.a(Integer.parseInt(bm.a(context)));
            this.c.b(a.c);
            this.c.d(a.a());
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private void b(Context context) {
        try {
            this.d.e(bm.a());
            this.d.a(bm.c(context));
            this.d.b(bm.d(context));
            this.d.c(bm.k(context));
            this.d.d(Build.MODEL);
            this.d.f("Android");
            this.d.g(Build.VERSION.RELEASE);
            int[] l = bm.l(context);
            if (l != null) {
                this.d.a(new az(l[1], l[0]));
            }
            if (a.e == null || a.d != null) {
            }
            this.d.h(Build.BOARD);
            this.d.i(Build.BRAND);
            this.d.a(Build.TIME);
            this.d.j(Build.MANUFACTURER);
            this.d.k(Build.ID);
            this.d.l(Build.DEVICE);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private void c(Context context) {
        try {
            String[] e2 = bm.e(context);
            if ("Wi-Fi".equals(e2[0])) {
                this.e.a(k.ACCESS_TYPE_WIFI);
            } else if ("2G/3G".equals(e2[0])) {
                this.e.a(k.ACCESS_TYPE_2G_3G);
            } else {
                this.e.a(k.ACCESS_TYPE_UNKNOWN);
            }
            if (!"".equals(e2[1])) {
                this.e.d(e2[1]);
            }
            this.e.c(bm.m(context));
            String[] i = bm.i(context);
            this.e.b(i[0]);
            this.e.a(i[1]);
            this.e.a(bm.h(context));
        } catch (Exception e3) {
            e3.printStackTrace();
        }
    }
}
