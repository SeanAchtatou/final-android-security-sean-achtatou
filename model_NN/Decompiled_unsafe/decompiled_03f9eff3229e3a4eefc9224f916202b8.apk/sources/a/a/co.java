package a.a;

/* compiled from: TField */
public class co {

    /* renamed from: a  reason: collision with root package name */
    public final String f66a;
    public final byte b;
    public final short c;

    public co() {
        this("", (byte) 0, 0);
    }

    public co(String str, byte b2, short s) {
        this.f66a = str;
        this.b = b2;
        this.c = s;
    }

    public String toString() {
        return "<TField name:'" + this.f66a + "' type:" + ((int) this.b) + " field-id:" + ((int) this.c) + ">";
    }
}
