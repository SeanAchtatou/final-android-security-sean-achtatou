package a.a;

import java.io.Serializable;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* compiled from: DeviceInfo */
public class v implements bw<v, e>, Serializable, Cloneable {
    /* access modifiers changed from: private */
    public static final co A = new co("os_version", (byte) 11, 8);
    /* access modifiers changed from: private */
    public static final co B = new co("resolution", (byte) 12, 9);
    /* access modifiers changed from: private */
    public static final co C = new co("is_jailbroken", (byte) 2, 10);
    /* access modifiers changed from: private */
    public static final co D = new co("is_pirated", (byte) 2, 11);
    /* access modifiers changed from: private */
    public static final co E = new co("device_board", (byte) 11, 12);
    /* access modifiers changed from: private */
    public static final co F = new co("device_brand", (byte) 11, 13);
    /* access modifiers changed from: private */
    public static final co G = new co("device_manutime", (byte) 10, 14);
    /* access modifiers changed from: private */
    public static final co H = new co("device_manufacturer", (byte) 11, 15);
    /* access modifiers changed from: private */
    public static final co I = new co("device_manuid", (byte) 11, 16);
    /* access modifiers changed from: private */
    public static final co J = new co("device_name", (byte) 11, 17);
    private static final Map<Class<? extends cy>, cz> K = new HashMap();
    public static final Map<e, cg> r;
    /* access modifiers changed from: private */
    public static final cw s = new cw("DeviceInfo");
    /* access modifiers changed from: private */
    public static final co t = new co("device_id", (byte) 11, 1);
    /* access modifiers changed from: private */
    public static final co u = new co("idmd5", (byte) 11, 2);
    /* access modifiers changed from: private */
    public static final co v = new co("mac_address", (byte) 11, 3);
    /* access modifiers changed from: private */
    public static final co w = new co("open_udid", (byte) 11, 4);
    /* access modifiers changed from: private */
    public static final co x = new co("model", (byte) 11, 5);
    /* access modifiers changed from: private */
    public static final co y = new co("cpu", (byte) 11, 6);
    /* access modifiers changed from: private */
    public static final co z = new co("os", (byte) 11, 7);
    private byte L = 0;
    private e[] M = {e.DEVICE_ID, e.IDMD5, e.MAC_ADDRESS, e.OPEN_UDID, e.MODEL, e.CPU, e.OS, e.OS_VERSION, e.RESOLUTION, e.IS_JAILBROKEN, e.IS_PIRATED, e.DEVICE_BOARD, e.DEVICE_BRAND, e.DEVICE_MANUTIME, e.DEVICE_MANUFACTURER, e.DEVICE_MANUID, e.DEVICE_NAME};

    /* renamed from: a  reason: collision with root package name */
    public String f108a;
    public String b;
    public String c;
    public String d;
    public String e;
    public String f;
    public String g;
    public String h;
    public az i;
    public boolean j;
    public boolean k;
    public String l;
    public String m;
    public long n;
    public String o;
    public String p;
    public String q;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [a.a.v$e, a.a.cg]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        K.put(da.class, new b());
        K.put(db.class, new d());
        EnumMap enumMap = new EnumMap(e.class);
        enumMap.put((Object) e.DEVICE_ID, (Object) new cg("device_id", (byte) 2, new ch((byte) 11)));
        enumMap.put((Object) e.IDMD5, (Object) new cg("idmd5", (byte) 2, new ch((byte) 11)));
        enumMap.put((Object) e.MAC_ADDRESS, (Object) new cg("mac_address", (byte) 2, new ch((byte) 11)));
        enumMap.put((Object) e.OPEN_UDID, (Object) new cg("open_udid", (byte) 2, new ch((byte) 11)));
        enumMap.put((Object) e.MODEL, (Object) new cg("model", (byte) 2, new ch((byte) 11)));
        enumMap.put((Object) e.CPU, (Object) new cg("cpu", (byte) 2, new ch((byte) 11)));
        enumMap.put((Object) e.OS, (Object) new cg("os", (byte) 2, new ch((byte) 11)));
        enumMap.put((Object) e.OS_VERSION, (Object) new cg("os_version", (byte) 2, new ch((byte) 11)));
        enumMap.put((Object) e.RESOLUTION, (Object) new cg("resolution", (byte) 2, new ck((byte) 12, az.class)));
        enumMap.put((Object) e.IS_JAILBROKEN, (Object) new cg("is_jailbroken", (byte) 2, new ch((byte) 2)));
        enumMap.put((Object) e.IS_PIRATED, (Object) new cg("is_pirated", (byte) 2, new ch((byte) 2)));
        enumMap.put((Object) e.DEVICE_BOARD, (Object) new cg("device_board", (byte) 2, new ch((byte) 11)));
        enumMap.put((Object) e.DEVICE_BRAND, (Object) new cg("device_brand", (byte) 2, new ch((byte) 11)));
        enumMap.put((Object) e.DEVICE_MANUTIME, (Object) new cg("device_manutime", (byte) 2, new ch((byte) 10)));
        enumMap.put((Object) e.DEVICE_MANUFACTURER, (Object) new cg("device_manufacturer", (byte) 2, new ch((byte) 11)));
        enumMap.put((Object) e.DEVICE_MANUID, (Object) new cg("device_manuid", (byte) 2, new ch((byte) 11)));
        enumMap.put((Object) e.DEVICE_NAME, (Object) new cg("device_name", (byte) 2, new ch((byte) 11)));
        r = Collections.unmodifiableMap(enumMap);
        cg.a(v.class, r);
    }

    /* compiled from: DeviceInfo */
    public enum e implements cb {
        DEVICE_ID(1, "device_id"),
        IDMD5(2, "idmd5"),
        MAC_ADDRESS(3, "mac_address"),
        OPEN_UDID(4, "open_udid"),
        MODEL(5, "model"),
        CPU(6, "cpu"),
        OS(7, "os"),
        OS_VERSION(8, "os_version"),
        RESOLUTION(9, "resolution"),
        IS_JAILBROKEN(10, "is_jailbroken"),
        IS_PIRATED(11, "is_pirated"),
        DEVICE_BOARD(12, "device_board"),
        DEVICE_BRAND(13, "device_brand"),
        DEVICE_MANUTIME(14, "device_manutime"),
        DEVICE_MANUFACTURER(15, "device_manufacturer"),
        DEVICE_MANUID(16, "device_manuid"),
        DEVICE_NAME(17, "device_name");
        
        private static final Map<String, e> r = new HashMap();
        private final short s;
        private final String t;

        static {
            Iterator it = EnumSet.allOf(e.class).iterator();
            while (it.hasNext()) {
                e eVar = (e) it.next();
                r.put(eVar.b(), eVar);
            }
        }

        private e(short s2, String str) {
            this.s = s2;
            this.t = str;
        }

        public short a() {
            return this.s;
        }

        public String b() {
            return this.t;
        }
    }

    public v a(String str) {
        this.f108a = str;
        return this;
    }

    public boolean a() {
        return this.f108a != null;
    }

    public void a(boolean z2) {
        if (!z2) {
            this.f108a = null;
        }
    }

    public v b(String str) {
        this.b = str;
        return this;
    }

    public boolean b() {
        return this.b != null;
    }

    public void b(boolean z2) {
        if (!z2) {
            this.b = null;
        }
    }

    public v c(String str) {
        this.c = str;
        return this;
    }

    public boolean c() {
        return this.c != null;
    }

    public void c(boolean z2) {
        if (!z2) {
            this.c = null;
        }
    }

    public boolean d() {
        return this.d != null;
    }

    public void d(boolean z2) {
        if (!z2) {
            this.d = null;
        }
    }

    public v d(String str) {
        this.e = str;
        return this;
    }

    public boolean e() {
        return this.e != null;
    }

    public void e(boolean z2) {
        if (!z2) {
            this.e = null;
        }
    }

    public v e(String str) {
        this.f = str;
        return this;
    }

    public boolean f() {
        return this.f != null;
    }

    public void f(boolean z2) {
        if (!z2) {
            this.f = null;
        }
    }

    public v f(String str) {
        this.g = str;
        return this;
    }

    public boolean g() {
        return this.g != null;
    }

    public void g(boolean z2) {
        if (!z2) {
            this.g = null;
        }
    }

    public v g(String str) {
        this.h = str;
        return this;
    }

    public boolean h() {
        return this.h != null;
    }

    public void h(boolean z2) {
        if (!z2) {
            this.h = null;
        }
    }

    public v a(az azVar) {
        this.i = azVar;
        return this;
    }

    public boolean i() {
        return this.i != null;
    }

    public void i(boolean z2) {
        if (!z2) {
            this.i = null;
        }
    }

    public boolean j() {
        return bu.a(this.L, 0);
    }

    public void j(boolean z2) {
        this.L = bu.a(this.L, 0, z2);
    }

    public boolean k() {
        return bu.a(this.L, 1);
    }

    public void k(boolean z2) {
        this.L = bu.a(this.L, 1, z2);
    }

    public v h(String str) {
        this.l = str;
        return this;
    }

    public boolean l() {
        return this.l != null;
    }

    public void l(boolean z2) {
        if (!z2) {
            this.l = null;
        }
    }

    public v i(String str) {
        this.m = str;
        return this;
    }

    public boolean m() {
        return this.m != null;
    }

    public void m(boolean z2) {
        if (!z2) {
            this.m = null;
        }
    }

    public v a(long j2) {
        this.n = j2;
        n(true);
        return this;
    }

    public boolean n() {
        return bu.a(this.L, 2);
    }

    public void n(boolean z2) {
        this.L = bu.a(this.L, 2, z2);
    }

    public v j(String str) {
        this.o = str;
        return this;
    }

    public boolean o() {
        return this.o != null;
    }

    public void o(boolean z2) {
        if (!z2) {
            this.o = null;
        }
    }

    public v k(String str) {
        this.p = str;
        return this;
    }

    public boolean p() {
        return this.p != null;
    }

    public void p(boolean z2) {
        if (!z2) {
            this.p = null;
        }
    }

    public v l(String str) {
        this.q = str;
        return this;
    }

    public boolean q() {
        return this.q != null;
    }

    public void q(boolean z2) {
        if (!z2) {
            this.q = null;
        }
    }

    public void a(cr crVar) throws ca {
        K.get(crVar.y()).b().b(crVar, this);
    }

    public void b(cr crVar) throws ca {
        K.get(crVar.y()).b().a(crVar, this);
    }

    public String toString() {
        boolean z2 = false;
        StringBuilder sb = new StringBuilder("DeviceInfo(");
        boolean z3 = true;
        if (a()) {
            sb.append("device_id:");
            if (this.f108a == null) {
                sb.append("null");
            } else {
                sb.append(this.f108a);
            }
            z3 = false;
        }
        if (b()) {
            if (!z3) {
                sb.append(", ");
            }
            sb.append("idmd5:");
            if (this.b == null) {
                sb.append("null");
            } else {
                sb.append(this.b);
            }
            z3 = false;
        }
        if (c()) {
            if (!z3) {
                sb.append(", ");
            }
            sb.append("mac_address:");
            if (this.c == null) {
                sb.append("null");
            } else {
                sb.append(this.c);
            }
            z3 = false;
        }
        if (d()) {
            if (!z3) {
                sb.append(", ");
            }
            sb.append("open_udid:");
            if (this.d == null) {
                sb.append("null");
            } else {
                sb.append(this.d);
            }
            z3 = false;
        }
        if (e()) {
            if (!z3) {
                sb.append(", ");
            }
            sb.append("model:");
            if (this.e == null) {
                sb.append("null");
            } else {
                sb.append(this.e);
            }
            z3 = false;
        }
        if (f()) {
            if (!z3) {
                sb.append(", ");
            }
            sb.append("cpu:");
            if (this.f == null) {
                sb.append("null");
            } else {
                sb.append(this.f);
            }
            z3 = false;
        }
        if (g()) {
            if (!z3) {
                sb.append(", ");
            }
            sb.append("os:");
            if (this.g == null) {
                sb.append("null");
            } else {
                sb.append(this.g);
            }
            z3 = false;
        }
        if (h()) {
            if (!z3) {
                sb.append(", ");
            }
            sb.append("os_version:");
            if (this.h == null) {
                sb.append("null");
            } else {
                sb.append(this.h);
            }
            z3 = false;
        }
        if (i()) {
            if (!z3) {
                sb.append(", ");
            }
            sb.append("resolution:");
            if (this.i == null) {
                sb.append("null");
            } else {
                sb.append(this.i);
            }
            z3 = false;
        }
        if (j()) {
            if (!z3) {
                sb.append(", ");
            }
            sb.append("is_jailbroken:");
            sb.append(this.j);
            z3 = false;
        }
        if (k()) {
            if (!z3) {
                sb.append(", ");
            }
            sb.append("is_pirated:");
            sb.append(this.k);
            z3 = false;
        }
        if (l()) {
            if (!z3) {
                sb.append(", ");
            }
            sb.append("device_board:");
            if (this.l == null) {
                sb.append("null");
            } else {
                sb.append(this.l);
            }
            z3 = false;
        }
        if (m()) {
            if (!z3) {
                sb.append(", ");
            }
            sb.append("device_brand:");
            if (this.m == null) {
                sb.append("null");
            } else {
                sb.append(this.m);
            }
            z3 = false;
        }
        if (n()) {
            if (!z3) {
                sb.append(", ");
            }
            sb.append("device_manutime:");
            sb.append(this.n);
            z3 = false;
        }
        if (o()) {
            if (!z3) {
                sb.append(", ");
            }
            sb.append("device_manufacturer:");
            if (this.o == null) {
                sb.append("null");
            } else {
                sb.append(this.o);
            }
            z3 = false;
        }
        if (p()) {
            if (!z3) {
                sb.append(", ");
            }
            sb.append("device_manuid:");
            if (this.p == null) {
                sb.append("null");
            } else {
                sb.append(this.p);
            }
        } else {
            z2 = z3;
        }
        if (q()) {
            if (!z2) {
                sb.append(", ");
            }
            sb.append("device_name:");
            if (this.q == null) {
                sb.append("null");
            } else {
                sb.append(this.q);
            }
        }
        sb.append(")");
        return sb.toString();
    }

    public void r() throws ca {
        if (this.i != null) {
            this.i.c();
        }
    }

    /* compiled from: DeviceInfo */
    private static class b implements cz {
        private b() {
        }

        /* renamed from: a */
        public a b() {
            return new a();
        }
    }

    /* compiled from: DeviceInfo */
    private static class a extends da<v> {
        private a() {
        }

        /* renamed from: a */
        public void b(cr crVar, v vVar) throws ca {
            crVar.f();
            while (true) {
                co h = crVar.h();
                if (h.b == 0) {
                    crVar.g();
                    vVar.r();
                    return;
                }
                switch (h.c) {
                    case 1:
                        if (h.b != 11) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            vVar.f108a = crVar.v();
                            vVar.a(true);
                            break;
                        }
                    case 2:
                        if (h.b != 11) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            vVar.b = crVar.v();
                            vVar.b(true);
                            break;
                        }
                    case 3:
                        if (h.b != 11) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            vVar.c = crVar.v();
                            vVar.c(true);
                            break;
                        }
                    case 4:
                        if (h.b != 11) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            vVar.d = crVar.v();
                            vVar.d(true);
                            break;
                        }
                    case 5:
                        if (h.b != 11) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            vVar.e = crVar.v();
                            vVar.e(true);
                            break;
                        }
                    case 6:
                        if (h.b != 11) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            vVar.f = crVar.v();
                            vVar.f(true);
                            break;
                        }
                    case 7:
                        if (h.b != 11) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            vVar.g = crVar.v();
                            vVar.g(true);
                            break;
                        }
                    case 8:
                        if (h.b != 11) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            vVar.h = crVar.v();
                            vVar.h(true);
                            break;
                        }
                    case 9:
                        if (h.b != 12) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            vVar.i = new az();
                            vVar.i.a(crVar);
                            vVar.i(true);
                            break;
                        }
                    case 10:
                        if (h.b != 2) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            vVar.j = crVar.p();
                            vVar.j(true);
                            break;
                        }
                    case 11:
                        if (h.b != 2) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            vVar.k = crVar.p();
                            vVar.k(true);
                            break;
                        }
                    case 12:
                        if (h.b != 11) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            vVar.l = crVar.v();
                            vVar.l(true);
                            break;
                        }
                    case 13:
                        if (h.b != 11) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            vVar.m = crVar.v();
                            vVar.m(true);
                            break;
                        }
                    case 14:
                        if (h.b != 10) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            vVar.n = crVar.t();
                            vVar.n(true);
                            break;
                        }
                    case 15:
                        if (h.b != 11) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            vVar.o = crVar.v();
                            vVar.o(true);
                            break;
                        }
                    case 16:
                        if (h.b != 11) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            vVar.p = crVar.v();
                            vVar.p(true);
                            break;
                        }
                    case 17:
                        if (h.b != 11) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            vVar.q = crVar.v();
                            vVar.q(true);
                            break;
                        }
                    default:
                        cu.a(crVar, h.b);
                        break;
                }
                crVar.i();
            }
        }

        /* renamed from: b */
        public void a(cr crVar, v vVar) throws ca {
            vVar.r();
            crVar.a(v.s);
            if (vVar.f108a != null && vVar.a()) {
                crVar.a(v.t);
                crVar.a(vVar.f108a);
                crVar.b();
            }
            if (vVar.b != null && vVar.b()) {
                crVar.a(v.u);
                crVar.a(vVar.b);
                crVar.b();
            }
            if (vVar.c != null && vVar.c()) {
                crVar.a(v.v);
                crVar.a(vVar.c);
                crVar.b();
            }
            if (vVar.d != null && vVar.d()) {
                crVar.a(v.w);
                crVar.a(vVar.d);
                crVar.b();
            }
            if (vVar.e != null && vVar.e()) {
                crVar.a(v.x);
                crVar.a(vVar.e);
                crVar.b();
            }
            if (vVar.f != null && vVar.f()) {
                crVar.a(v.y);
                crVar.a(vVar.f);
                crVar.b();
            }
            if (vVar.g != null && vVar.g()) {
                crVar.a(v.z);
                crVar.a(vVar.g);
                crVar.b();
            }
            if (vVar.h != null && vVar.h()) {
                crVar.a(v.A);
                crVar.a(vVar.h);
                crVar.b();
            }
            if (vVar.i != null && vVar.i()) {
                crVar.a(v.B);
                vVar.i.b(crVar);
                crVar.b();
            }
            if (vVar.j()) {
                crVar.a(v.C);
                crVar.a(vVar.j);
                crVar.b();
            }
            if (vVar.k()) {
                crVar.a(v.D);
                crVar.a(vVar.k);
                crVar.b();
            }
            if (vVar.l != null && vVar.l()) {
                crVar.a(v.E);
                crVar.a(vVar.l);
                crVar.b();
            }
            if (vVar.m != null && vVar.m()) {
                crVar.a(v.F);
                crVar.a(vVar.m);
                crVar.b();
            }
            if (vVar.n()) {
                crVar.a(v.G);
                crVar.a(vVar.n);
                crVar.b();
            }
            if (vVar.o != null && vVar.o()) {
                crVar.a(v.H);
                crVar.a(vVar.o);
                crVar.b();
            }
            if (vVar.p != null && vVar.p()) {
                crVar.a(v.I);
                crVar.a(vVar.p);
                crVar.b();
            }
            if (vVar.q != null && vVar.q()) {
                crVar.a(v.J);
                crVar.a(vVar.q);
                crVar.b();
            }
            crVar.c();
            crVar.a();
        }
    }

    /* compiled from: DeviceInfo */
    private static class d implements cz {
        private d() {
        }

        /* renamed from: a */
        public c b() {
            return new c();
        }
    }

    /* compiled from: DeviceInfo */
    private static class c extends db<v> {
        private c() {
        }

        public void a(cr crVar, v vVar) throws ca {
            cx cxVar = (cx) crVar;
            BitSet bitSet = new BitSet();
            if (vVar.a()) {
                bitSet.set(0);
            }
            if (vVar.b()) {
                bitSet.set(1);
            }
            if (vVar.c()) {
                bitSet.set(2);
            }
            if (vVar.d()) {
                bitSet.set(3);
            }
            if (vVar.e()) {
                bitSet.set(4);
            }
            if (vVar.f()) {
                bitSet.set(5);
            }
            if (vVar.g()) {
                bitSet.set(6);
            }
            if (vVar.h()) {
                bitSet.set(7);
            }
            if (vVar.i()) {
                bitSet.set(8);
            }
            if (vVar.j()) {
                bitSet.set(9);
            }
            if (vVar.k()) {
                bitSet.set(10);
            }
            if (vVar.l()) {
                bitSet.set(11);
            }
            if (vVar.m()) {
                bitSet.set(12);
            }
            if (vVar.n()) {
                bitSet.set(13);
            }
            if (vVar.o()) {
                bitSet.set(14);
            }
            if (vVar.p()) {
                bitSet.set(15);
            }
            if (vVar.q()) {
                bitSet.set(16);
            }
            cxVar.a(bitSet, 17);
            if (vVar.a()) {
                cxVar.a(vVar.f108a);
            }
            if (vVar.b()) {
                cxVar.a(vVar.b);
            }
            if (vVar.c()) {
                cxVar.a(vVar.c);
            }
            if (vVar.d()) {
                cxVar.a(vVar.d);
            }
            if (vVar.e()) {
                cxVar.a(vVar.e);
            }
            if (vVar.f()) {
                cxVar.a(vVar.f);
            }
            if (vVar.g()) {
                cxVar.a(vVar.g);
            }
            if (vVar.h()) {
                cxVar.a(vVar.h);
            }
            if (vVar.i()) {
                vVar.i.b(cxVar);
            }
            if (vVar.j()) {
                cxVar.a(vVar.j);
            }
            if (vVar.k()) {
                cxVar.a(vVar.k);
            }
            if (vVar.l()) {
                cxVar.a(vVar.l);
            }
            if (vVar.m()) {
                cxVar.a(vVar.m);
            }
            if (vVar.n()) {
                cxVar.a(vVar.n);
            }
            if (vVar.o()) {
                cxVar.a(vVar.o);
            }
            if (vVar.p()) {
                cxVar.a(vVar.p);
            }
            if (vVar.q()) {
                cxVar.a(vVar.q);
            }
        }

        public void b(cr crVar, v vVar) throws ca {
            cx cxVar = (cx) crVar;
            BitSet b = cxVar.b(17);
            if (b.get(0)) {
                vVar.f108a = cxVar.v();
                vVar.a(true);
            }
            if (b.get(1)) {
                vVar.b = cxVar.v();
                vVar.b(true);
            }
            if (b.get(2)) {
                vVar.c = cxVar.v();
                vVar.c(true);
            }
            if (b.get(3)) {
                vVar.d = cxVar.v();
                vVar.d(true);
            }
            if (b.get(4)) {
                vVar.e = cxVar.v();
                vVar.e(true);
            }
            if (b.get(5)) {
                vVar.f = cxVar.v();
                vVar.f(true);
            }
            if (b.get(6)) {
                vVar.g = cxVar.v();
                vVar.g(true);
            }
            if (b.get(7)) {
                vVar.h = cxVar.v();
                vVar.h(true);
            }
            if (b.get(8)) {
                vVar.i = new az();
                vVar.i.a(cxVar);
                vVar.i(true);
            }
            if (b.get(9)) {
                vVar.j = cxVar.p();
                vVar.j(true);
            }
            if (b.get(10)) {
                vVar.k = cxVar.p();
                vVar.k(true);
            }
            if (b.get(11)) {
                vVar.l = cxVar.v();
                vVar.l(true);
            }
            if (b.get(12)) {
                vVar.m = cxVar.v();
                vVar.m(true);
            }
            if (b.get(13)) {
                vVar.n = cxVar.t();
                vVar.n(true);
            }
            if (b.get(14)) {
                vVar.o = cxVar.v();
                vVar.o(true);
            }
            if (b.get(15)) {
                vVar.p = cxVar.v();
                vVar.p(true);
            }
            if (b.get(16)) {
                vVar.q = cxVar.v();
                vVar.q(true);
            }
        }
    }
}
