package a.a;

import java.io.Serializable;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* compiled from: Resolution */
public class az implements bw<az, e>, Serializable, Cloneable {
    public static final Map<e, cg> c;
    /* access modifiers changed from: private */
    public static final cw d = new cw("Resolution");
    /* access modifiers changed from: private */
    public static final co e = new co("height", (byte) 8, 1);
    /* access modifiers changed from: private */
    public static final co f = new co("width", (byte) 8, 2);
    private static final Map<Class<? extends cy>, cz> g = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public int f28a;
    public int b;
    private byte h;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [a.a.az$e, a.a.cg]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        g.put(da.class, new b());
        g.put(db.class, new d());
        EnumMap enumMap = new EnumMap(e.class);
        enumMap.put((Object) e.HEIGHT, (Object) new cg("height", (byte) 1, new ch((byte) 8)));
        enumMap.put((Object) e.WIDTH, (Object) new cg("width", (byte) 1, new ch((byte) 8)));
        c = Collections.unmodifiableMap(enumMap);
        cg.a(az.class, c);
    }

    /* compiled from: Resolution */
    public enum e implements cb {
        HEIGHT(1, "height"),
        WIDTH(2, "width");
        
        private static final Map<String, e> c = new HashMap();
        private final short d;
        private final String e;

        static {
            Iterator it = EnumSet.allOf(e.class).iterator();
            while (it.hasNext()) {
                e eVar = (e) it.next();
                c.put(eVar.b(), eVar);
            }
        }

        private e(short s, String str) {
            this.d = s;
            this.e = str;
        }

        public short a() {
            return this.d;
        }

        public String b() {
            return this.e;
        }
    }

    public az() {
        this.h = 0;
    }

    public az(int i, int i2) {
        this();
        this.f28a = i;
        a(true);
        this.b = i2;
        b(true);
    }

    public boolean a() {
        return bu.a(this.h, 0);
    }

    public void a(boolean z) {
        this.h = bu.a(this.h, 0, z);
    }

    public boolean b() {
        return bu.a(this.h, 1);
    }

    public void b(boolean z) {
        this.h = bu.a(this.h, 1, z);
    }

    public void a(cr crVar) throws ca {
        g.get(crVar.y()).b().b(crVar, this);
    }

    public void b(cr crVar) throws ca {
        g.get(crVar.y()).b().a(crVar, this);
    }

    public String toString() {
        return "Resolution(" + "height:" + this.f28a + ", " + "width:" + this.b + ")";
    }

    public void c() throws ca {
    }

    /* compiled from: Resolution */
    private static class b implements cz {
        private b() {
        }

        /* renamed from: a */
        public a b() {
            return new a();
        }
    }

    /* compiled from: Resolution */
    private static class a extends da<az> {
        private a() {
        }

        /* renamed from: a */
        public void b(cr crVar, az azVar) throws ca {
            crVar.f();
            while (true) {
                co h = crVar.h();
                if (h.b == 0) {
                    crVar.g();
                    if (!azVar.a()) {
                        throw new cs("Required field 'height' was not found in serialized data! Struct: " + toString());
                    } else if (!azVar.b()) {
                        throw new cs("Required field 'width' was not found in serialized data! Struct: " + toString());
                    } else {
                        azVar.c();
                        return;
                    }
                } else {
                    switch (h.c) {
                        case 1:
                            if (h.b != 8) {
                                cu.a(crVar, h.b);
                                break;
                            } else {
                                azVar.f28a = crVar.s();
                                azVar.a(true);
                                break;
                            }
                        case 2:
                            if (h.b != 8) {
                                cu.a(crVar, h.b);
                                break;
                            } else {
                                azVar.b = crVar.s();
                                azVar.b(true);
                                break;
                            }
                        default:
                            cu.a(crVar, h.b);
                            break;
                    }
                    crVar.i();
                }
            }
        }

        /* renamed from: b */
        public void a(cr crVar, az azVar) throws ca {
            azVar.c();
            crVar.a(az.d);
            crVar.a(az.e);
            crVar.a(azVar.f28a);
            crVar.b();
            crVar.a(az.f);
            crVar.a(azVar.b);
            crVar.b();
            crVar.c();
            crVar.a();
        }
    }

    /* compiled from: Resolution */
    private static class d implements cz {
        private d() {
        }

        /* renamed from: a */
        public c b() {
            return new c();
        }
    }

    /* compiled from: Resolution */
    private static class c extends db<az> {
        private c() {
        }

        public void a(cr crVar, az azVar) throws ca {
            cx cxVar = (cx) crVar;
            cxVar.a(azVar.f28a);
            cxVar.a(azVar.b);
        }

        public void b(cr crVar, az azVar) throws ca {
            cx cxVar = (cx) crVar;
            azVar.f28a = cxVar.s();
            azVar.a(true);
            azVar.b = cxVar.s();
            azVar.b(true);
        }
    }
}
