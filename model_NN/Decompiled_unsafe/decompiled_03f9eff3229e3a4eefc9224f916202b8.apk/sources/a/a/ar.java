package a.a;

import com.tencent.stat.DeviceInfo;
import java.io.Serializable;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* compiled from: Location */
public class ar implements bw<ar, e>, Serializable, Cloneable {
    public static final Map<e, cg> d;
    /* access modifiers changed from: private */
    public static final cw e = new cw("Location");
    /* access modifiers changed from: private */
    public static final co f = new co("lat", (byte) 4, 1);
    /* access modifiers changed from: private */
    public static final co g = new co("lng", (byte) 4, 2);
    /* access modifiers changed from: private */
    public static final co h = new co(DeviceInfo.TAG_TIMESTAMPS, (byte) 10, 3);
    private static final Map<Class<? extends cy>, cz> i = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public double f19a;
    public double b;
    public long c;
    private byte j;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [a.a.ar$e, a.a.cg]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        i.put(da.class, new b());
        i.put(db.class, new d());
        EnumMap enumMap = new EnumMap(e.class);
        enumMap.put((Object) e.LAT, (Object) new cg("lat", (byte) 1, new ch((byte) 4)));
        enumMap.put((Object) e.LNG, (Object) new cg("lng", (byte) 1, new ch((byte) 4)));
        enumMap.put((Object) e.TS, (Object) new cg(DeviceInfo.TAG_TIMESTAMPS, (byte) 1, new ch((byte) 10)));
        d = Collections.unmodifiableMap(enumMap);
        cg.a(ar.class, d);
    }

    /* compiled from: Location */
    public enum e implements cb {
        LAT(1, "lat"),
        LNG(2, "lng"),
        TS(3, DeviceInfo.TAG_TIMESTAMPS);
        
        private static final Map<String, e> d = new HashMap();
        private final short e;
        private final String f;

        static {
            Iterator it = EnumSet.allOf(e.class).iterator();
            while (it.hasNext()) {
                e eVar = (e) it.next();
                d.put(eVar.b(), eVar);
            }
        }

        private e(short s, String str) {
            this.e = s;
            this.f = str;
        }

        public short a() {
            return this.e;
        }

        public String b() {
            return this.f;
        }
    }

    public ar() {
        this.j = 0;
    }

    public ar(double d2, double d3, long j2) {
        this();
        this.f19a = d2;
        a(true);
        this.b = d3;
        b(true);
        this.c = j2;
        c(true);
    }

    public boolean a() {
        return bu.a(this.j, 0);
    }

    public void a(boolean z) {
        this.j = bu.a(this.j, 0, z);
    }

    public boolean b() {
        return bu.a(this.j, 1);
    }

    public void b(boolean z) {
        this.j = bu.a(this.j, 1, z);
    }

    public boolean c() {
        return bu.a(this.j, 2);
    }

    public void c(boolean z) {
        this.j = bu.a(this.j, 2, z);
    }

    public void a(cr crVar) throws ca {
        i.get(crVar.y()).b().b(crVar, this);
    }

    public void b(cr crVar) throws ca {
        i.get(crVar.y()).b().a(crVar, this);
    }

    public String toString() {
        return "Location(" + "lat:" + this.f19a + ", " + "lng:" + this.b + ", " + "ts:" + this.c + ")";
    }

    public void d() throws ca {
    }

    /* compiled from: Location */
    private static class b implements cz {
        private b() {
        }

        /* renamed from: a */
        public a b() {
            return new a();
        }
    }

    /* compiled from: Location */
    private static class a extends da<ar> {
        private a() {
        }

        /* renamed from: a */
        public void b(cr crVar, ar arVar) throws ca {
            crVar.f();
            while (true) {
                co h = crVar.h();
                if (h.b == 0) {
                    crVar.g();
                    if (!arVar.a()) {
                        throw new cs("Required field 'lat' was not found in serialized data! Struct: " + toString());
                    } else if (!arVar.b()) {
                        throw new cs("Required field 'lng' was not found in serialized data! Struct: " + toString());
                    } else if (!arVar.c()) {
                        throw new cs("Required field 'ts' was not found in serialized data! Struct: " + toString());
                    } else {
                        arVar.d();
                        return;
                    }
                } else {
                    switch (h.c) {
                        case 1:
                            if (h.b != 4) {
                                cu.a(crVar, h.b);
                                break;
                            } else {
                                arVar.f19a = crVar.u();
                                arVar.a(true);
                                break;
                            }
                        case 2:
                            if (h.b != 4) {
                                cu.a(crVar, h.b);
                                break;
                            } else {
                                arVar.b = crVar.u();
                                arVar.b(true);
                                break;
                            }
                        case 3:
                            if (h.b != 10) {
                                cu.a(crVar, h.b);
                                break;
                            } else {
                                arVar.c = crVar.t();
                                arVar.c(true);
                                break;
                            }
                        default:
                            cu.a(crVar, h.b);
                            break;
                    }
                    crVar.i();
                }
            }
        }

        /* renamed from: b */
        public void a(cr crVar, ar arVar) throws ca {
            arVar.d();
            crVar.a(ar.e);
            crVar.a(ar.f);
            crVar.a(arVar.f19a);
            crVar.b();
            crVar.a(ar.g);
            crVar.a(arVar.b);
            crVar.b();
            crVar.a(ar.h);
            crVar.a(arVar.c);
            crVar.b();
            crVar.c();
            crVar.a();
        }
    }

    /* compiled from: Location */
    private static class d implements cz {
        private d() {
        }

        /* renamed from: a */
        public c b() {
            return new c();
        }
    }

    /* compiled from: Location */
    private static class c extends db<ar> {
        private c() {
        }

        public void a(cr crVar, ar arVar) throws ca {
            cx cxVar = (cx) crVar;
            cxVar.a(arVar.f19a);
            cxVar.a(arVar.b);
            cxVar.a(arVar.c);
        }

        public void b(cr crVar, ar arVar) throws ca {
            cx cxVar = (cx) crVar;
            arVar.f19a = cxVar.u();
            arVar.a(true);
            arVar.b = cxVar.u();
            arVar.b(true);
            arVar.c = cxVar.t();
            arVar.c(true);
        }
    }
}
