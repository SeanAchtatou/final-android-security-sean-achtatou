package a.a;

import android.os.Build;

/* compiled from: SerialTracker */
public class dj extends a {
    public dj() {
        super("serial");
    }

    public String f() {
        if (Build.VERSION.SDK_INT >= 9) {
            return Build.SERIAL;
        }
        return null;
    }
}
