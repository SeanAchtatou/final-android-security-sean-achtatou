package a.a;

import android.content.Context;
import android.text.TextUtils;
import cn.banshenggua.aichang.utils.Constants;
import com.umeng.analytics.e;
import com.umeng.analytics.m;
import java.io.File;
import java.nio.ByteBuffer;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

/* compiled from: ImprintHandler */
public class dh {

    /* renamed from: a  reason: collision with root package name */
    private static final byte[] f77a = "pbl0".getBytes();
    private static dh d;
    private ea b;
    private aj c = null;
    private Context e;

    dh(Context context) {
        this.e = context;
    }

    public static synchronized dh a(Context context) {
        dh dhVar;
        synchronized (dh.class) {
            if (d == null) {
                d = new dh(context);
                d.b();
            }
            dhVar = d;
        }
        return dhVar;
    }

    public void a(ea eaVar) {
        this.b = eaVar;
    }

    public String a(aj ajVar) {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry entry : new TreeMap(ajVar.a()).entrySet()) {
            sb.append((String) entry.getKey());
            sb.append(((al) entry.getValue()).a());
            sb.append(((al) entry.getValue()).c());
            sb.append(((al) entry.getValue()).e());
        }
        sb.append(ajVar.b);
        return bt.a(sb.toString()).toLowerCase(Locale.US);
    }

    private boolean c(aj ajVar) {
        if (!ajVar.e().equals(a(ajVar))) {
            return false;
        }
        for (al next : ajVar.a().values()) {
            byte[] a2 = e.a(next.e());
            byte[] a3 = a(next);
            int i = 0;
            while (true) {
                if (i < 4) {
                    if (a2[i] != a3[i]) {
                        return false;
                    }
                    i++;
                }
            }
        }
        return true;
    }

    public byte[] a(al alVar) {
        ByteBuffer allocate = ByteBuffer.allocate(8);
        allocate.order(null);
        allocate.putLong(alVar.c());
        byte[] array = allocate.array();
        byte[] bArr = f77a;
        byte[] bArr2 = new byte[4];
        for (int i = 0; i < 4; i++) {
            bArr2[i] = (byte) (array[i] ^ bArr[i]);
        }
        return bArr2;
    }

    public void b(aj ajVar) {
        if (ajVar != null && c(ajVar)) {
            synchronized (this) {
                aj ajVar2 = this.c;
                if (ajVar2 != null) {
                    ajVar = a(ajVar2, ajVar);
                }
                this.c = ajVar;
            }
            if (this.c != null) {
                d();
            }
        }
    }

    private aj a(aj ajVar, aj ajVar2) {
        if (ajVar2 != null) {
            Map<String, al> a2 = ajVar.a();
            for (Map.Entry next : ajVar2.a().entrySet()) {
                if (((al) next.getValue()).b()) {
                    a2.put(next.getKey(), next.getValue());
                } else {
                    a2.remove(next.getKey());
                }
            }
            ajVar.a(ajVar2.c());
            ajVar.a(a(ajVar));
        }
        return ajVar;
    }

    private void d() {
        if (this.b != null) {
            int a2 = a("defcon");
            if (a2 != -1) {
                this.b.a(a2);
                m.a(this.e).a(a2);
            }
            int a3 = a("latent");
            if (a3 != -1) {
                int i = a3 * Constants.CLEARIMGED;
                this.b.b(i);
                m.a(this.e).b(i);
            }
            int a4 = a("codex");
            if (a4 == 0 || a4 == 1 || a4 == -1) {
                this.b.c(a4);
                m.a(this.e).c(a4);
            }
        }
    }

    private int a(String str) {
        aj ajVar = this.c;
        if (ajVar == null || !ajVar.b()) {
            return -1;
        }
        al alVar = ajVar.a().get(str);
        if (alVar == null || TextUtils.isEmpty(alVar.a())) {
            return -1;
        }
        try {
            return Integer.parseInt(alVar.a().trim());
        } catch (Exception e2) {
            return -1;
        }
    }

    public synchronized aj a() {
        return this.c;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r2v0 */
    /* JADX WARN: Type inference failed for: r2v1, types: [java.io.InputStream] */
    /* JADX WARN: Type inference failed for: r2v3 */
    /* JADX WARN: Type inference failed for: r2v4, types: [byte[]] */
    /* JADX WARN: Type inference failed for: r2v7 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:24:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0026 A[SYNTHETIC, Splitter:B:8:0x0026] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void b() {
        /*
            r4 = this;
            r2 = 0
            java.io.File r0 = new java.io.File
            android.content.Context r1 = r4.e
            java.io.File r1 = r1.getFilesDir()
            java.lang.String r3 = ".imprint"
            r0.<init>(r1, r3)
            boolean r0 = r0.exists()
            if (r0 != 0) goto L_0x0015
        L_0x0014:
            return
        L_0x0015:
            android.content.Context r0 = r4.e     // Catch:{ Exception -> 0x003b, all -> 0x0044 }
            java.lang.String r1 = ".imprint"
            java.io.FileInputStream r1 = r0.openFileInput(r1)     // Catch:{ Exception -> 0x003b, all -> 0x0044 }
            byte[] r2 = a.a.bt.b(r1)     // Catch:{ Exception -> 0x004c }
            a.a.bt.c(r1)
        L_0x0024:
            if (r2 == 0) goto L_0x0014
            a.a.aj r0 = new a.a.aj     // Catch:{ Exception -> 0x0036 }
            r0.<init>()     // Catch:{ Exception -> 0x0036 }
            a.a.bz r1 = new a.a.bz     // Catch:{ Exception -> 0x0036 }
            r1.<init>()     // Catch:{ Exception -> 0x0036 }
            r1.a(r0, r2)     // Catch:{ Exception -> 0x0036 }
            r4.c = r0     // Catch:{ Exception -> 0x0036 }
            goto L_0x0014
        L_0x0036:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0014
        L_0x003b:
            r0 = move-exception
            r1 = r2
        L_0x003d:
            r0.printStackTrace()     // Catch:{ all -> 0x0049 }
            a.a.bt.c(r1)
            goto L_0x0024
        L_0x0044:
            r0 = move-exception
        L_0x0045:
            a.a.bt.c(r2)
            throw r0
        L_0x0049:
            r0 = move-exception
            r2 = r1
            goto L_0x0045
        L_0x004c:
            r0 = move-exception
            goto L_0x003d
        */
        throw new UnsupportedOperationException("Method not decompiled: a.a.dh.b():void");
    }

    public void c() {
        if (this.c != null) {
            try {
                bt.a(new File(this.e.getFilesDir(), ".imprint"), new cc().a(this.c));
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }
}
