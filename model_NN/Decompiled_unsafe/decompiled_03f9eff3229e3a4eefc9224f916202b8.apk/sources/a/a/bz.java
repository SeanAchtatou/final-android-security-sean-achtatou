package a.a;

import a.a.cn;

/* compiled from: TDeserializer */
public class bz {

    /* renamed from: a  reason: collision with root package name */
    private final cr f50a;
    private final dd b;

    public bz() {
        this(new cn.a());
    }

    public bz(ct ctVar) {
        this.b = new dd();
        this.f50a = ctVar.a(this.b);
    }

    public void a(bw bwVar, byte[] bArr) throws ca {
        try {
            this.b.a(bArr);
            bwVar.a(this.f50a);
        } finally {
            this.b.a();
            this.f50a.x();
        }
    }
}
