package a.a;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* compiled from: UEKV */
public class f extends aa implements dv {
    public f(String str, Map<String, Object> map, long j, int i) {
        a(str);
        b(System.currentTimeMillis());
        if (map.size() > 0) {
            a(b(map));
        }
        a(i <= 0 ? 1 : i);
        if (j > 0) {
            a(j);
        }
    }

    private HashMap<String, ax> b(Map<String, Object> map) {
        Iterator<Map.Entry<String, Object>> it = map.entrySet().iterator();
        HashMap<String, ax> hashMap = new HashMap<>();
        int i = 0;
        while (i < 10 && it.hasNext()) {
            Map.Entry next = it.next();
            ax axVar = new ax();
            Object value = next.getValue();
            if (value instanceof String) {
                axVar.a((String) value);
            } else if (value instanceof Long) {
                axVar.a(((Long) value).longValue());
            } else if (value instanceof Integer) {
                axVar.a(((Integer) value).longValue());
            } else if (value instanceof Float) {
                axVar.a(((Float) value).longValue());
            } else if (value instanceof Double) {
                axVar.a(((Double) value).longValue());
            }
            if (axVar.d()) {
                hashMap.put(next.getKey(), axVar);
                i++;
            }
        }
        return hashMap;
    }

    public void a(bi biVar, String str) {
        an anVar;
        if (biVar.b() > 0) {
            Iterator<an> it = biVar.c().iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                anVar = it.next();
                if (str.equals(anVar.a())) {
                    break;
                }
            }
        }
        anVar = null;
        if (anVar == null) {
            anVar = new an();
            anVar.a(str);
            biVar.a(anVar);
        }
        anVar.a(this);
    }
}
