package a.a;

import java.io.Serializable;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* compiled from: ClientStats */
public class r implements bw<r, e>, Serializable, Cloneable {
    public static final Map<e, cg> d;
    /* access modifiers changed from: private */
    public static final cw e = new cw("ClientStats");
    /* access modifiers changed from: private */
    public static final co f = new co("successful_requests", (byte) 8, 1);
    /* access modifiers changed from: private */
    public static final co g = new co("failed_requests", (byte) 8, 2);
    /* access modifiers changed from: private */
    public static final co h = new co("last_request_spent_ms", (byte) 8, 3);
    private static final Map<Class<? extends cy>, cz> i = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public int f104a = 0;
    public int b = 0;
    public int c;
    private byte j = 0;
    private e[] k = {e.LAST_REQUEST_SPENT_MS};

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [a.a.r$e, a.a.cg]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        i.put(da.class, new b());
        i.put(db.class, new d());
        EnumMap enumMap = new EnumMap(e.class);
        enumMap.put((Object) e.SUCCESSFUL_REQUESTS, (Object) new cg("successful_requests", (byte) 1, new ch((byte) 8)));
        enumMap.put((Object) e.FAILED_REQUESTS, (Object) new cg("failed_requests", (byte) 1, new ch((byte) 8)));
        enumMap.put((Object) e.LAST_REQUEST_SPENT_MS, (Object) new cg("last_request_spent_ms", (byte) 2, new ch((byte) 8)));
        d = Collections.unmodifiableMap(enumMap);
        cg.a(r.class, d);
    }

    /* compiled from: ClientStats */
    public enum e implements cb {
        SUCCESSFUL_REQUESTS(1, "successful_requests"),
        FAILED_REQUESTS(2, "failed_requests"),
        LAST_REQUEST_SPENT_MS(3, "last_request_spent_ms");
        
        private static final Map<String, e> d = new HashMap();
        private final short e;
        private final String f;

        static {
            Iterator it = EnumSet.allOf(e.class).iterator();
            while (it.hasNext()) {
                e eVar = (e) it.next();
                d.put(eVar.b(), eVar);
            }
        }

        private e(short s, String str) {
            this.e = s;
            this.f = str;
        }

        public short a() {
            return this.e;
        }

        public String b() {
            return this.f;
        }
    }

    public r a(int i2) {
        this.f104a = i2;
        a(true);
        return this;
    }

    public boolean a() {
        return bu.a(this.j, 0);
    }

    public void a(boolean z) {
        this.j = bu.a(this.j, 0, z);
    }

    public r b(int i2) {
        this.b = i2;
        b(true);
        return this;
    }

    public boolean b() {
        return bu.a(this.j, 1);
    }

    public void b(boolean z) {
        this.j = bu.a(this.j, 1, z);
    }

    public r c(int i2) {
        this.c = i2;
        c(true);
        return this;
    }

    public boolean c() {
        return bu.a(this.j, 2);
    }

    public void c(boolean z) {
        this.j = bu.a(this.j, 2, z);
    }

    public void a(cr crVar) throws ca {
        i.get(crVar.y()).b().b(crVar, this);
    }

    public void b(cr crVar) throws ca {
        i.get(crVar.y()).b().a(crVar, this);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("ClientStats(");
        sb.append("successful_requests:");
        sb.append(this.f104a);
        sb.append(", ");
        sb.append("failed_requests:");
        sb.append(this.b);
        if (c()) {
            sb.append(", ");
            sb.append("last_request_spent_ms:");
            sb.append(this.c);
        }
        sb.append(")");
        return sb.toString();
    }

    public void d() throws ca {
    }

    /* compiled from: ClientStats */
    private static class b implements cz {
        private b() {
        }

        /* renamed from: a */
        public a b() {
            return new a();
        }
    }

    /* compiled from: ClientStats */
    private static class a extends da<r> {
        private a() {
        }

        /* renamed from: a */
        public void b(cr crVar, r rVar) throws ca {
            crVar.f();
            while (true) {
                co h = crVar.h();
                if (h.b == 0) {
                    crVar.g();
                    if (!rVar.a()) {
                        throw new cs("Required field 'successful_requests' was not found in serialized data! Struct: " + toString());
                    } else if (!rVar.b()) {
                        throw new cs("Required field 'failed_requests' was not found in serialized data! Struct: " + toString());
                    } else {
                        rVar.d();
                        return;
                    }
                } else {
                    switch (h.c) {
                        case 1:
                            if (h.b != 8) {
                                cu.a(crVar, h.b);
                                break;
                            } else {
                                rVar.f104a = crVar.s();
                                rVar.a(true);
                                break;
                            }
                        case 2:
                            if (h.b != 8) {
                                cu.a(crVar, h.b);
                                break;
                            } else {
                                rVar.b = crVar.s();
                                rVar.b(true);
                                break;
                            }
                        case 3:
                            if (h.b != 8) {
                                cu.a(crVar, h.b);
                                break;
                            } else {
                                rVar.c = crVar.s();
                                rVar.c(true);
                                break;
                            }
                        default:
                            cu.a(crVar, h.b);
                            break;
                    }
                    crVar.i();
                }
            }
        }

        /* renamed from: b */
        public void a(cr crVar, r rVar) throws ca {
            rVar.d();
            crVar.a(r.e);
            crVar.a(r.f);
            crVar.a(rVar.f104a);
            crVar.b();
            crVar.a(r.g);
            crVar.a(rVar.b);
            crVar.b();
            if (rVar.c()) {
                crVar.a(r.h);
                crVar.a(rVar.c);
                crVar.b();
            }
            crVar.c();
            crVar.a();
        }
    }

    /* compiled from: ClientStats */
    private static class d implements cz {
        private d() {
        }

        /* renamed from: a */
        public c b() {
            return new c();
        }
    }

    /* compiled from: ClientStats */
    private static class c extends db<r> {
        private c() {
        }

        public void a(cr crVar, r rVar) throws ca {
            cx cxVar = (cx) crVar;
            cxVar.a(rVar.f104a);
            cxVar.a(rVar.b);
            BitSet bitSet = new BitSet();
            if (rVar.c()) {
                bitSet.set(0);
            }
            cxVar.a(bitSet, 1);
            if (rVar.c()) {
                cxVar.a(rVar.c);
            }
        }

        public void b(cr crVar, r rVar) throws ca {
            cx cxVar = (cx) crVar;
            rVar.f104a = cxVar.s();
            rVar.a(true);
            rVar.b = cxVar.s();
            rVar.b(true);
            if (cxVar.b(1).get(0)) {
                rVar.c = cxVar.s();
                rVar.c(true);
            }
        }
    }
}
