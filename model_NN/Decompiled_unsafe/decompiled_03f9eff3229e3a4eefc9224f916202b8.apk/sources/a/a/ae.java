package a.a;

import com.tencent.stat.DeviceInfo;
import java.io.Serializable;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* compiled from: IdSnapshot */
public class ae implements bw<ae, e>, Serializable, Cloneable {
    public static final Map<e, cg> d;
    /* access modifiers changed from: private */
    public static final cw e = new cw("IdSnapshot");
    /* access modifiers changed from: private */
    public static final co f = new co("identity", (byte) 11, 1);
    /* access modifiers changed from: private */
    public static final co g = new co(DeviceInfo.TAG_TIMESTAMPS, (byte) 10, 2);
    /* access modifiers changed from: private */
    public static final co h = new co("version", (byte) 8, 3);
    private static final Map<Class<? extends cy>, cz> i = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public String f6a;
    public long b;
    public int c;
    private byte j = 0;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [a.a.ae$e, a.a.cg]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        i.put(da.class, new b());
        i.put(db.class, new d());
        EnumMap enumMap = new EnumMap(e.class);
        enumMap.put((Object) e.IDENTITY, (Object) new cg("identity", (byte) 1, new ch((byte) 11)));
        enumMap.put((Object) e.TS, (Object) new cg(DeviceInfo.TAG_TIMESTAMPS, (byte) 1, new ch((byte) 10)));
        enumMap.put((Object) e.VERSION, (Object) new cg("version", (byte) 1, new ch((byte) 8)));
        d = Collections.unmodifiableMap(enumMap);
        cg.a(ae.class, d);
    }

    /* compiled from: IdSnapshot */
    public enum e implements cb {
        IDENTITY(1, "identity"),
        TS(2, DeviceInfo.TAG_TIMESTAMPS),
        VERSION(3, "version");
        
        private static final Map<String, e> d = new HashMap();
        private final short e;
        private final String f;

        static {
            Iterator it = EnumSet.allOf(e.class).iterator();
            while (it.hasNext()) {
                e eVar = (e) it.next();
                d.put(eVar.b(), eVar);
            }
        }

        private e(short s, String str) {
            this.e = s;
            this.f = str;
        }

        public short a() {
            return this.e;
        }

        public String b() {
            return this.f;
        }
    }

    public String a() {
        return this.f6a;
    }

    public ae a(String str) {
        this.f6a = str;
        return this;
    }

    public void a(boolean z) {
        if (!z) {
            this.f6a = null;
        }
    }

    public long b() {
        return this.b;
    }

    public ae a(long j2) {
        this.b = j2;
        b(true);
        return this;
    }

    public boolean c() {
        return bu.a(this.j, 0);
    }

    public void b(boolean z) {
        this.j = bu.a(this.j, 0, z);
    }

    public int d() {
        return this.c;
    }

    public ae a(int i2) {
        this.c = i2;
        c(true);
        return this;
    }

    public boolean e() {
        return bu.a(this.j, 1);
    }

    public void c(boolean z) {
        this.j = bu.a(this.j, 1, z);
    }

    public void a(cr crVar) throws ca {
        i.get(crVar.y()).b().b(crVar, this);
    }

    public void b(cr crVar) throws ca {
        i.get(crVar.y()).b().a(crVar, this);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("IdSnapshot(");
        sb.append("identity:");
        if (this.f6a == null) {
            sb.append("null");
        } else {
            sb.append(this.f6a);
        }
        sb.append(", ");
        sb.append("ts:");
        sb.append(this.b);
        sb.append(", ");
        sb.append("version:");
        sb.append(this.c);
        sb.append(")");
        return sb.toString();
    }

    public void f() throws ca {
        if (this.f6a == null) {
            throw new cs("Required field 'identity' was not present! Struct: " + toString());
        }
    }

    /* compiled from: IdSnapshot */
    private static class b implements cz {
        private b() {
        }

        /* renamed from: a */
        public a b() {
            return new a();
        }
    }

    /* compiled from: IdSnapshot */
    private static class a extends da<ae> {
        private a() {
        }

        /* renamed from: a */
        public void b(cr crVar, ae aeVar) throws ca {
            crVar.f();
            while (true) {
                co h = crVar.h();
                if (h.b == 0) {
                    crVar.g();
                    if (!aeVar.c()) {
                        throw new cs("Required field 'ts' was not found in serialized data! Struct: " + toString());
                    } else if (!aeVar.e()) {
                        throw new cs("Required field 'version' was not found in serialized data! Struct: " + toString());
                    } else {
                        aeVar.f();
                        return;
                    }
                } else {
                    switch (h.c) {
                        case 1:
                            if (h.b != 11) {
                                cu.a(crVar, h.b);
                                break;
                            } else {
                                aeVar.f6a = crVar.v();
                                aeVar.a(true);
                                break;
                            }
                        case 2:
                            if (h.b != 10) {
                                cu.a(crVar, h.b);
                                break;
                            } else {
                                aeVar.b = crVar.t();
                                aeVar.b(true);
                                break;
                            }
                        case 3:
                            if (h.b != 8) {
                                cu.a(crVar, h.b);
                                break;
                            } else {
                                aeVar.c = crVar.s();
                                aeVar.c(true);
                                break;
                            }
                        default:
                            cu.a(crVar, h.b);
                            break;
                    }
                    crVar.i();
                }
            }
        }

        /* renamed from: b */
        public void a(cr crVar, ae aeVar) throws ca {
            aeVar.f();
            crVar.a(ae.e);
            if (aeVar.f6a != null) {
                crVar.a(ae.f);
                crVar.a(aeVar.f6a);
                crVar.b();
            }
            crVar.a(ae.g);
            crVar.a(aeVar.b);
            crVar.b();
            crVar.a(ae.h);
            crVar.a(aeVar.c);
            crVar.b();
            crVar.c();
            crVar.a();
        }
    }

    /* compiled from: IdSnapshot */
    private static class d implements cz {
        private d() {
        }

        /* renamed from: a */
        public c b() {
            return new c();
        }
    }

    /* compiled from: IdSnapshot */
    private static class c extends db<ae> {
        private c() {
        }

        public void a(cr crVar, ae aeVar) throws ca {
            cx cxVar = (cx) crVar;
            cxVar.a(aeVar.f6a);
            cxVar.a(aeVar.b);
            cxVar.a(aeVar.c);
        }

        public void b(cr crVar, ae aeVar) throws ca {
            cx cxVar = (cx) crVar;
            aeVar.f6a = cxVar.v();
            aeVar.a(true);
            aeVar.b = cxVar.t();
            aeVar.b(true);
            aeVar.c = cxVar.s();
            aeVar.c(true);
        }
    }
}
