package a.a;

/* compiled from: EncodingUtils */
public class bu {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.bu.a(int, int):boolean
     arg types: [byte, int]
     candidates:
      a.a.bu.a(byte, int):boolean
      a.a.bu.a(int, int):boolean */
    public static final boolean a(byte b, int i) {
        return a((int) b, i);
    }

    public static final boolean a(int i, int i2) {
        return ((1 << i2) & i) != 0;
    }

    public static final int b(int i, int i2) {
        return ((1 << i2) ^ -1) & i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.bu.a(int, int, boolean):int
     arg types: [byte, int, boolean]
     candidates:
      a.a.bu.a(byte, int, boolean):byte
      a.a.bu.a(int, int, boolean):int */
    public static final byte a(byte b, int i, boolean z) {
        return (byte) a((int) b, i, z);
    }

    public static final int a(int i, int i2, boolean z) {
        if (z) {
            return (1 << i2) | i;
        }
        return b(i, i2);
    }
}
