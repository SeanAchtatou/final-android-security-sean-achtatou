package a.a;

import android.content.Context;
import java.util.Arrays;
import java.util.List;

/* compiled from: Defcon */
public class j {

    /* renamed from: a  reason: collision with root package name */
    private int f96a = 0;
    private final long b = 60000;

    public bi a(Context context, bi biVar) {
        if (biVar == null) {
            return null;
        }
        if (this.f96a == 1) {
            biVar.a((List<an>) null);
            return biVar;
        } else if (this.f96a == 2) {
            biVar.b(Arrays.asList(a(context)));
            biVar.a((List<an>) null);
            return biVar;
        } else if (this.f96a != 3) {
            return biVar;
        } else {
            biVar.b((List<be>) null);
            biVar.a((List<an>) null);
            return biVar;
        }
    }

    public be a(Context context) {
        long currentTimeMillis = System.currentTimeMillis();
        be beVar = new be();
        beVar.a(ee.e(context));
        beVar.a(currentTimeMillis);
        beVar.b(currentTimeMillis + 60000);
        beVar.c(60000);
        return beVar;
    }

    public long a() {
        switch (this.f96a) {
            case 1:
                return 14400000;
            case 2:
                return 28800000;
            case 3:
                return LogBuilder.MAX_INTERVAL;
            default:
                return 0;
        }
    }

    public void a(int i) {
        if (i >= 0 && i <= 3) {
            this.f96a = i;
        }
    }

    public boolean b() {
        return this.f96a != 0;
    }
}
