package a.a;

import java.io.Serializable;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* compiled from: ControlPolicy */
public class t implements bw<t, e>, Serializable, Cloneable {
    public static final Map<e, cg> b;
    /* access modifiers changed from: private */
    public static final cw c = new cw("ControlPolicy");
    /* access modifiers changed from: private */
    public static final co d = new co("latent", (byte) 12, 1);
    private static final Map<Class<? extends cy>, cz> e = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public ap f106a;
    private e[] f = {e.LATENT};

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [a.a.t$e, a.a.cg]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        e.put(da.class, new b());
        e.put(db.class, new d());
        EnumMap enumMap = new EnumMap(e.class);
        enumMap.put((Object) e.LATENT, (Object) new cg("latent", (byte) 2, new ck((byte) 12, ap.class)));
        b = Collections.unmodifiableMap(enumMap);
        cg.a(t.class, b);
    }

    /* compiled from: ControlPolicy */
    public enum e implements cb {
        LATENT(1, "latent");
        
        private static final Map<String, e> b = new HashMap();
        private final short c;
        private final String d;

        static {
            Iterator it = EnumSet.allOf(e.class).iterator();
            while (it.hasNext()) {
                e eVar = (e) it.next();
                b.put(eVar.b(), eVar);
            }
        }

        private e(short s, String str) {
            this.c = s;
            this.d = str;
        }

        public short a() {
            return this.c;
        }

        public String b() {
            return this.d;
        }
    }

    public t a(ap apVar) {
        this.f106a = apVar;
        return this;
    }

    public boolean a() {
        return this.f106a != null;
    }

    public void a(boolean z) {
        if (!z) {
            this.f106a = null;
        }
    }

    public void a(cr crVar) throws ca {
        e.get(crVar.y()).b().b(crVar, this);
    }

    public void b(cr crVar) throws ca {
        e.get(crVar.y()).b().a(crVar, this);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("ControlPolicy(");
        if (a()) {
            sb.append("latent:");
            if (this.f106a == null) {
                sb.append("null");
            } else {
                sb.append(this.f106a);
            }
        }
        sb.append(")");
        return sb.toString();
    }

    public void b() throws ca {
        if (this.f106a != null) {
            this.f106a.c();
        }
    }

    /* compiled from: ControlPolicy */
    private static class b implements cz {
        private b() {
        }

        /* renamed from: a */
        public a b() {
            return new a();
        }
    }

    /* compiled from: ControlPolicy */
    private static class a extends da<t> {
        private a() {
        }

        /* renamed from: a */
        public void b(cr crVar, t tVar) throws ca {
            crVar.f();
            while (true) {
                co h = crVar.h();
                if (h.b == 0) {
                    crVar.g();
                    tVar.b();
                    return;
                }
                switch (h.c) {
                    case 1:
                        if (h.b != 12) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            tVar.f106a = new ap();
                            tVar.f106a.a(crVar);
                            tVar.a(true);
                            break;
                        }
                    default:
                        cu.a(crVar, h.b);
                        break;
                }
                crVar.i();
            }
        }

        /* renamed from: b */
        public void a(cr crVar, t tVar) throws ca {
            tVar.b();
            crVar.a(t.c);
            if (tVar.f106a != null && tVar.a()) {
                crVar.a(t.d);
                tVar.f106a.b(crVar);
                crVar.b();
            }
            crVar.c();
            crVar.a();
        }
    }

    /* compiled from: ControlPolicy */
    private static class d implements cz {
        private d() {
        }

        /* renamed from: a */
        public c b() {
            return new c();
        }
    }

    /* compiled from: ControlPolicy */
    private static class c extends db<t> {
        private c() {
        }

        public void a(cr crVar, t tVar) throws ca {
            cx cxVar = (cx) crVar;
            BitSet bitSet = new BitSet();
            if (tVar.a()) {
                bitSet.set(0);
            }
            cxVar.a(bitSet, 1);
            if (tVar.a()) {
                tVar.f106a.b(cxVar);
            }
        }

        public void b(cr crVar, t tVar) throws ca {
            cx cxVar = (cx) crVar;
            if (cxVar.b(1).get(0)) {
                tVar.f106a = new ap();
                tVar.f106a.a(cxVar);
                tVar.a(true);
            }
        }
    }
}
