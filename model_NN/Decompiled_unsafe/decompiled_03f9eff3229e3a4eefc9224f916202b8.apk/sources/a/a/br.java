package a.a;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.view.MotionEventCompat;
import com.tencent.open.GameAppOperation;
import com.umeng.analytics.e;

/* compiled from: Envelope */
public class br {

    /* renamed from: a  reason: collision with root package name */
    private final byte[] f45a = {0, 0, 0, 0, 0, 0, 0, 0};
    private final int b = 1;
    private final int c = 0;
    private String d = "1.0";
    private String e = null;
    private byte[] f = null;
    private byte[] g = null;
    private byte[] h = null;
    private int i = 0;
    private int j = 0;
    private int k = 0;
    private byte[] l = null;
    private byte[] m = null;
    private boolean n = false;

    private br(byte[] bArr, String str, byte[] bArr2) throws Exception {
        if (bArr == null || bArr.length == 0) {
            throw new Exception("entity is null or empty");
        }
        this.e = str;
        this.k = bArr.length;
        this.l = bs.a(bArr);
        this.j = (int) (System.currentTimeMillis() / 1000);
        this.m = bArr2;
    }

    public static String a(Context context) {
        SharedPreferences a2 = eb.a(context);
        if (a2 == null) {
            return null;
        }
        return a2.getString(GameAppOperation.GAME_SIGNATURE, null);
    }

    public void a(String str) {
        this.f = e.a(str);
    }

    public String a() {
        return e.a(this.f);
    }

    public void a(int i2) {
        this.i = i2;
    }

    public void a(boolean z) {
        this.n = z;
    }

    public static br a(Context context, String str, byte[] bArr) {
        try {
            String k2 = bm.k(context);
            String c2 = bm.c(context);
            SharedPreferences a2 = eb.a(context);
            String string = a2.getString(GameAppOperation.GAME_SIGNATURE, null);
            int i2 = a2.getInt("serial", 1);
            br brVar = new br(bArr, str, (c2 + k2).getBytes());
            brVar.a(string);
            brVar.a(i2);
            brVar.b();
            a2.edit().putInt("serial", i2 + 1).putString(GameAppOperation.GAME_SIGNATURE, brVar.a()).commit();
            return brVar;
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public static br b(Context context, String str, byte[] bArr) {
        try {
            String k2 = bm.k(context);
            String c2 = bm.c(context);
            SharedPreferences a2 = eb.a(context);
            String string = a2.getString(GameAppOperation.GAME_SIGNATURE, null);
            int i2 = a2.getInt("serial", 1);
            br brVar = new br(bArr, str, (c2 + k2).getBytes());
            brVar.a(true);
            brVar.a(string);
            brVar.a(i2);
            brVar.b();
            a2.edit().putInt("serial", i2 + 1).putString(GameAppOperation.GAME_SIGNATURE, brVar.a()).commit();
            return brVar;
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public void b() {
        if (this.f == null) {
            this.f = d();
        }
        if (this.n) {
            byte[] bArr = new byte[16];
            try {
                System.arraycopy(this.f, 1, bArr, 0, 16);
                this.l = e.a(this.l, bArr);
            } catch (Exception e2) {
            }
        }
        this.g = a(this.f, this.j);
        this.h = e();
    }

    private byte[] a(byte[] bArr, int i2) {
        byte[] b2 = e.b(this.m);
        byte[] b3 = e.b(this.l);
        int length = b2.length;
        byte[] bArr2 = new byte[(length * 2)];
        for (int i3 = 0; i3 < length; i3++) {
            bArr2[i3 * 2] = b3[i3];
            bArr2[(i3 * 2) + 1] = b2[i3];
        }
        for (int i4 = 0; i4 < 2; i4++) {
            bArr2[i4] = bArr[i4];
            bArr2[(bArr2.length - i4) - 1] = bArr[(bArr.length - i4) - 1];
        }
        byte[] bArr3 = {(byte) (i2 & MotionEventCompat.ACTION_MASK), (byte) ((i2 >> 8) & MotionEventCompat.ACTION_MASK), (byte) ((i2 >> 16) & MotionEventCompat.ACTION_MASK), (byte) (i2 >>> 24)};
        for (int i5 = 0; i5 < bArr2.length; i5++) {
            bArr2[i5] = (byte) (bArr2[i5] ^ bArr3[i5 % 4]);
        }
        return bArr2;
    }

    private byte[] d() {
        return a(this.f45a, (int) (System.currentTimeMillis() / 1000));
    }

    private byte[] e() {
        return e.b((e.a(this.f) + this.i + this.j + this.k + e.a(this.g)).getBytes());
    }

    public byte[] c() {
        bk bkVar = new bk();
        bkVar.a(this.d);
        bkVar.b(this.e);
        bkVar.c(e.a(this.f));
        bkVar.a(this.i);
        bkVar.b(this.j);
        bkVar.c(this.k);
        bkVar.a(this.l);
        bkVar.d(this.n ? 1 : 0);
        bkVar.d(e.a(this.g));
        bkVar.e(e.a(this.h));
        try {
            return new cc().a(bkVar);
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public String toString() {
        int i2 = 1;
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("version : %s\n", this.d));
        sb.append(String.format("address : %s\n", this.e));
        sb.append(String.format("signature : %s\n", e.a(this.f)));
        sb.append(String.format("serial : %s\n", Integer.valueOf(this.i)));
        sb.append(String.format("timestamp : %d\n", Integer.valueOf(this.j)));
        sb.append(String.format("length : %d\n", Integer.valueOf(this.k)));
        sb.append(String.format("guid : %s\n", e.a(this.g)));
        sb.append(String.format("checksum : %s ", e.a(this.h)));
        Object[] objArr = new Object[1];
        if (!this.n) {
            i2 = 0;
        }
        objArr[0] = Integer.valueOf(i2);
        sb.append(String.format("codex : %d", objArr));
        return sb.toString();
    }
}
