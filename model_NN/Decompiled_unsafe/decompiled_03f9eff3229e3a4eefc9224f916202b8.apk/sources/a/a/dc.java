package a.a;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/* compiled from: TIOStreamTransport */
public class dc extends de {

    /* renamed from: a  reason: collision with root package name */
    protected InputStream f73a = null;
    protected OutputStream b = null;

    protected dc() {
    }

    public dc(OutputStream outputStream) {
        this.b = outputStream;
    }

    public int a(byte[] bArr, int i, int i2) throws df {
        if (this.f73a == null) {
            throw new df(1, "Cannot read from null inputStream");
        }
        try {
            int read = this.f73a.read(bArr, i, i2);
            if (read >= 0) {
                return read;
            }
            throw new df(4);
        } catch (IOException e) {
            throw new df(0, e);
        }
    }

    public void b(byte[] bArr, int i, int i2) throws df {
        if (this.b == null) {
            throw new df(1, "Cannot write to null outputStream");
        }
        try {
            this.b.write(bArr, i, i2);
        } catch (IOException e) {
            throw new df(0, e);
        }
    }
}
