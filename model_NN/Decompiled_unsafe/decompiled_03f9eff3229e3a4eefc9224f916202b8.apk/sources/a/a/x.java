package a.a;

import com.tencent.stat.DeviceInfo;
import java.io.Serializable;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* compiled from: Error */
public class x implements bw<x, e>, Serializable, Cloneable {
    public static final Map<e, cg> d;
    /* access modifiers changed from: private */
    public static final cw e = new cw("Error");
    /* access modifiers changed from: private */
    public static final co f = new co(DeviceInfo.TAG_TIMESTAMPS, (byte) 10, 1);
    /* access modifiers changed from: private */
    public static final co g = new co("context", (byte) 11, 2);
    /* access modifiers changed from: private */
    public static final co h = new co("source", (byte) 8, 3);
    private static final Map<Class<? extends cy>, cz> i = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public long f110a;
    public String b;
    public z c;
    private byte j = 0;
    private e[] k = {e.SOURCE};

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [a.a.x$e, a.a.cg]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        i.put(da.class, new b());
        i.put(db.class, new d());
        EnumMap enumMap = new EnumMap(e.class);
        enumMap.put((Object) e.TS, (Object) new cg(DeviceInfo.TAG_TIMESTAMPS, (byte) 1, new ch((byte) 10)));
        enumMap.put((Object) e.CONTEXT, (Object) new cg("context", (byte) 1, new ch((byte) 11)));
        enumMap.put((Object) e.SOURCE, (Object) new cg("source", (byte) 2, new cf((byte) 16, z.class)));
        d = Collections.unmodifiableMap(enumMap);
        cg.a(x.class, d);
    }

    /* compiled from: Error */
    public enum e implements cb {
        TS(1, DeviceInfo.TAG_TIMESTAMPS),
        CONTEXT(2, "context"),
        SOURCE(3, "source");
        
        private static final Map<String, e> d = new HashMap();
        private final short e;
        private final String f;

        static {
            Iterator it = EnumSet.allOf(e.class).iterator();
            while (it.hasNext()) {
                e eVar = (e) it.next();
                d.put(eVar.b(), eVar);
            }
        }

        private e(short s, String str) {
            this.e = s;
            this.f = str;
        }

        public short a() {
            return this.e;
        }

        public String b() {
            return this.f;
        }
    }

    public x a(long j2) {
        this.f110a = j2;
        b(true);
        return this;
    }

    public boolean a() {
        return bu.a(this.j, 0);
    }

    public void b(boolean z) {
        this.j = bu.a(this.j, 0, z);
    }

    public x a(String str) {
        this.b = str;
        return this;
    }

    public void c(boolean z) {
        if (!z) {
            this.b = null;
        }
    }

    public x a(z zVar) {
        this.c = zVar;
        return this;
    }

    public boolean b() {
        return this.c != null;
    }

    public void d(boolean z) {
        if (!z) {
            this.c = null;
        }
    }

    public void a(cr crVar) throws ca {
        i.get(crVar.y()).b().b(crVar, this);
    }

    public void b(cr crVar) throws ca {
        i.get(crVar.y()).b().a(crVar, this);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("Error(");
        sb.append("ts:");
        sb.append(this.f110a);
        sb.append(", ");
        sb.append("context:");
        if (this.b == null) {
            sb.append("null");
        } else {
            sb.append(this.b);
        }
        if (b()) {
            sb.append(", ");
            sb.append("source:");
            if (this.c == null) {
                sb.append("null");
            } else {
                sb.append(this.c);
            }
        }
        sb.append(")");
        return sb.toString();
    }

    public void c() throws ca {
        if (this.b == null) {
            throw new cs("Required field 'context' was not present! Struct: " + toString());
        }
    }

    /* compiled from: Error */
    private static class b implements cz {
        private b() {
        }

        /* renamed from: a */
        public a b() {
            return new a();
        }
    }

    /* compiled from: Error */
    private static class a extends da<x> {
        private a() {
        }

        /* renamed from: a */
        public void b(cr crVar, x xVar) throws ca {
            crVar.f();
            while (true) {
                co h = crVar.h();
                if (h.b == 0) {
                    crVar.g();
                    if (!xVar.a()) {
                        throw new cs("Required field 'ts' was not found in serialized data! Struct: " + toString());
                    }
                    xVar.c();
                    return;
                }
                switch (h.c) {
                    case 1:
                        if (h.b != 10) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            xVar.f110a = crVar.t();
                            xVar.b(true);
                            break;
                        }
                    case 2:
                        if (h.b != 11) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            xVar.b = crVar.v();
                            xVar.c(true);
                            break;
                        }
                    case 3:
                        if (h.b != 8) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            xVar.c = z.a(crVar.s());
                            xVar.d(true);
                            break;
                        }
                    default:
                        cu.a(crVar, h.b);
                        break;
                }
                crVar.i();
            }
        }

        /* renamed from: b */
        public void a(cr crVar, x xVar) throws ca {
            xVar.c();
            crVar.a(x.e);
            crVar.a(x.f);
            crVar.a(xVar.f110a);
            crVar.b();
            if (xVar.b != null) {
                crVar.a(x.g);
                crVar.a(xVar.b);
                crVar.b();
            }
            if (xVar.c != null && xVar.b()) {
                crVar.a(x.h);
                crVar.a(xVar.c.a());
                crVar.b();
            }
            crVar.c();
            crVar.a();
        }
    }

    /* compiled from: Error */
    private static class d implements cz {
        private d() {
        }

        /* renamed from: a */
        public c b() {
            return new c();
        }
    }

    /* compiled from: Error */
    private static class c extends db<x> {
        private c() {
        }

        public void a(cr crVar, x xVar) throws ca {
            cx cxVar = (cx) crVar;
            cxVar.a(xVar.f110a);
            cxVar.a(xVar.b);
            BitSet bitSet = new BitSet();
            if (xVar.b()) {
                bitSet.set(0);
            }
            cxVar.a(bitSet, 1);
            if (xVar.b()) {
                cxVar.a(xVar.c.a());
            }
        }

        public void b(cr crVar, x xVar) throws ca {
            cx cxVar = (cx) crVar;
            xVar.f110a = cxVar.t();
            xVar.b(true);
            xVar.b = cxVar.v();
            xVar.c(true);
            if (cxVar.b(1).get(0)) {
                xVar.c = z.a(cxVar.s());
                xVar.d(true);
            }
        }
    }
}
