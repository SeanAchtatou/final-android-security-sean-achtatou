package a.a;

import android.content.Context;
import android.provider.Settings;

/* compiled from: AndroidIdTracker */
public class ai extends a {

    /* renamed from: a  reason: collision with root package name */
    private Context f10a;

    public ai(Context context) {
        super("android_id");
        this.f10a = context;
    }

    public String f() {
        try {
            return Settings.Secure.getString(this.f10a.getContentResolver(), "android_id");
        } catch (Exception e) {
            return null;
        }
    }
}
