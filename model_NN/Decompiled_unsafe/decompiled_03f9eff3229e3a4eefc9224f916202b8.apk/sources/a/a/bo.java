package a.a;

import cn.banshenggua.aichang.utils.Constants;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Map;
import java.util.Random;
import java.util.zip.GZIPInputStream;
import java.util.zip.InflaterInputStream;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: UClient */
public class bo {

    /* renamed from: a  reason: collision with root package name */
    private static final String f44a = bo.class.getName();
    private Map<String, String> b;

    public <T extends bq> T a(bp bpVar, Class<T> cls) {
        JSONObject jSONObject;
        String trim = bpVar.c().trim();
        b(trim);
        if (bp.c.equals(trim)) {
            jSONObject = a(bpVar.b());
        } else if (bp.b.equals(trim)) {
            jSONObject = a(bpVar.d, bpVar.a());
        } else {
            jSONObject = null;
        }
        if (jSONObject == null) {
            return null;
        }
        try {
            return (bq) cls.getConstructor(JSONObject.class).newInstance(jSONObject);
        } catch (SecurityException e) {
            bn.b(f44a, "SecurityException", e);
        } catch (NoSuchMethodException e2) {
            bn.b(f44a, "NoSuchMethodException", e2);
        } catch (IllegalArgumentException e3) {
            bn.b(f44a, "IllegalArgumentException", e3);
        } catch (InstantiationException e4) {
            bn.b(f44a, "InstantiationException", e4);
        } catch (IllegalAccessException e5) {
            bn.b(f44a, "IllegalAccessException", e5);
        } catch (InvocationTargetException e6) {
            bn.b(f44a, "InvocationTargetException", e6);
        }
        return null;
    }

    private JSONObject a(String str, JSONObject jSONObject) {
        InputStream inputStream;
        boolean z = true;
        String jSONObject2 = jSONObject.toString();
        int nextInt = new Random().nextInt(Constants.CLEARIMGED);
        bn.c(f44a, String.valueOf(nextInt) + ":\trequest: " + str + bt.f47a + jSONObject2);
        HttpPost httpPost = new HttpPost(str);
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient(b());
        try {
            if (a()) {
                byte[] a2 = bs.a(jSONObject2, Charset.defaultCharset().name());
                httpPost.addHeader("Content-Encoding", "deflate");
                httpPost.setEntity(new InputStreamEntity(new ByteArrayInputStream(a2), (long) a2.length));
            } else {
                ArrayList arrayList = new ArrayList(1);
                arrayList.add(new BasicNameValuePair("content", jSONObject2));
                httpPost.setEntity(new UrlEncodedFormEntity(arrayList, "UTF-8"));
            }
            HttpResponse execute = defaultHttpClient.execute(httpPost);
            Header firstHeader = execute.getFirstHeader("Content-Type");
            if (execute.getStatusLine().getStatusCode() != 200) {
                z = false;
            }
            boolean a3 = bt.a(firstHeader, "application/json");
            if (!z || !a3) {
                bn.c(f44a, String.valueOf(nextInt) + ":\tFailed to send message. StatusCode = " + execute.getStatusLine().getStatusCode() + bt.f47a + str);
                return null;
            }
            HttpEntity entity = execute.getEntity();
            if (entity == null) {
                return null;
            }
            InputStream content = entity.getContent();
            Header firstHeader2 = execute.getFirstHeader("Content-Encoding");
            if (firstHeader2 == null || !firstHeader2.getValue().equalsIgnoreCase("deflate")) {
                inputStream = content;
            } else {
                inputStream = new InflaterInputStream(content);
            }
            String a4 = a(inputStream);
            bn.a(f44a, String.valueOf(nextInt) + ":\tresponse: " + bt.f47a + a4);
            if (a4 == null) {
                return null;
            }
            return new JSONObject(a4);
        } catch (ClientProtocolException e) {
            bn.c(f44a, String.valueOf(nextInt) + ":\tClientProtocolException,Failed to send message." + str, e);
            return null;
        } catch (IOException e2) {
            bn.c(f44a, String.valueOf(nextInt) + ":\tIOException,Failed to send message." + str, e2);
            return null;
        } catch (JSONException e3) {
            bn.c(f44a, String.valueOf(nextInt) + ":\tIOException,Failed to send message." + str, e3);
            return null;
        }
    }

    public boolean a() {
        return false;
    }

    private static String a(InputStream inputStream) {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream), 8192);
        StringBuilder sb = new StringBuilder();
        while (true) {
            try {
                String readLine = bufferedReader.readLine();
                if (readLine == null) {
                    try {
                        inputStream.close();
                        return sb.toString();
                    } catch (IOException e) {
                        bn.b(f44a, "Caught IOException in convertStreamToString()", e);
                        return null;
                    }
                } else {
                    sb.append(String.valueOf(readLine) + "\n");
                }
            } catch (IOException e2) {
                bn.b(f44a, "Caught IOException in convertStreamToString()", e2);
                try {
                    inputStream.close();
                    return null;
                } catch (IOException e3) {
                    bn.b(f44a, "Caught IOException in convertStreamToString()", e3);
                    return null;
                }
            } catch (Throwable th) {
                try {
                    inputStream.close();
                    throw th;
                } catch (IOException e4) {
                    bn.b(f44a, "Caught IOException in convertStreamToString()", e4);
                    return null;
                }
            }
        }
    }

    private JSONObject a(String str) {
        InputStream inputStream;
        int nextInt = new Random().nextInt(Constants.CLEARIMGED);
        try {
            String property = System.getProperty("line.separator");
            if (str.length() <= 1) {
                bn.b(f44a, String.valueOf(nextInt) + ":\tInvalid baseUrl.");
                return null;
            }
            bn.a(f44a, String.valueOf(nextInt) + ":\tget: " + str);
            HttpGet httpGet = new HttpGet(str);
            if (this.b != null && this.b.size() > 0) {
                for (String next : this.b.keySet()) {
                    httpGet.addHeader(next, this.b.get(next));
                }
            }
            HttpResponse execute = new DefaultHttpClient(b()).execute(httpGet);
            if (execute.getStatusLine().getStatusCode() == 200) {
                HttpEntity entity = execute.getEntity();
                if (entity != null) {
                    InputStream content = entity.getContent();
                    Header firstHeader = execute.getFirstHeader("Content-Encoding");
                    if (firstHeader == null || !firstHeader.getValue().equalsIgnoreCase("gzip")) {
                        if (firstHeader != null) {
                            if (firstHeader.getValue().equalsIgnoreCase("deflate")) {
                                bn.a(f44a, String.valueOf(nextInt) + "  Use InflaterInputStream get data....");
                                inputStream = new InflaterInputStream(content);
                            }
                        }
                        inputStream = content;
                    } else {
                        bn.a(f44a, String.valueOf(nextInt) + "  Use GZIPInputStream get data....");
                        inputStream = new GZIPInputStream(content);
                    }
                    String a2 = a(inputStream);
                    bn.a(f44a, String.valueOf(nextInt) + ":\tresponse: " + property + a2);
                    if (a2 == null) {
                        return null;
                    }
                    return new JSONObject(a2);
                }
            } else {
                bn.c(f44a, String.valueOf(nextInt) + ":\tFailed to send message. StatusCode = " + execute.getStatusLine().getStatusCode() + bt.f47a + str);
            }
            return null;
        } catch (ClientProtocolException e) {
            bn.c(f44a, String.valueOf(nextInt) + ":\tClientProtocolException,Failed to send message." + str, e);
            return null;
        } catch (Exception e2) {
            bn.c(f44a, String.valueOf(nextInt) + ":\tIOException,Failed to send message." + str, e2);
            return null;
        }
    }

    private HttpParams b() {
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(basicHttpParams, 10000);
        HttpConnectionParams.setSoTimeout(basicHttpParams, 20000);
        HttpProtocolParams.setUserAgent(basicHttpParams, System.getProperty("http.agent"));
        return basicHttpParams;
    }

    private void b(String str) {
        if (bt.c(str) || !(bp.c.equals(str.trim()) ^ bp.b.equals(str.trim()))) {
            throw new RuntimeException("验证请求方式失败[" + str + "]");
        }
    }
}
