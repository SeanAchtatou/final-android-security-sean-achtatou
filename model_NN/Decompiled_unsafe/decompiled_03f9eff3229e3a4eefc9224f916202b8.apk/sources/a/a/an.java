package a.a;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* compiled from: InstantMsg */
public class an implements bw<an, e>, Serializable, Cloneable {
    public static final Map<e, cg> e;
    /* access modifiers changed from: private */
    public static final cw f = new cw("InstantMsg");
    /* access modifiers changed from: private */
    public static final co g = new co("id", (byte) 11, 1);
    /* access modifiers changed from: private */
    public static final co h = new co("errors", (byte) 15, 2);
    /* access modifiers changed from: private */
    public static final co i = new co("events", (byte) 15, 3);
    /* access modifiers changed from: private */
    public static final co j = new co("game_events", (byte) 15, 4);
    private static final Map<Class<? extends cy>, cz> k = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public String f15a;
    public List<x> b;
    public List<aa> c;
    public List<aa> d;
    private e[] l = {e.ERRORS, e.EVENTS, e.GAME_EVENTS};

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [a.a.an$e, a.a.cg]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        k.put(da.class, new b());
        k.put(db.class, new d());
        EnumMap enumMap = new EnumMap(e.class);
        enumMap.put((Object) e.ID, (Object) new cg("id", (byte) 1, new ch((byte) 11)));
        enumMap.put((Object) e.ERRORS, (Object) new cg("errors", (byte) 2, new ci((byte) 15, new ck((byte) 12, x.class))));
        enumMap.put((Object) e.EVENTS, (Object) new cg("events", (byte) 2, new ci((byte) 15, new ck((byte) 12, aa.class))));
        enumMap.put((Object) e.GAME_EVENTS, (Object) new cg("game_events", (byte) 2, new ci((byte) 15, new ck((byte) 12, aa.class))));
        e = Collections.unmodifiableMap(enumMap);
        cg.a(an.class, e);
    }

    /* compiled from: InstantMsg */
    public enum e implements cb {
        ID(1, "id"),
        ERRORS(2, "errors"),
        EVENTS(3, "events"),
        GAME_EVENTS(4, "game_events");
        
        private static final Map<String, e> e = new HashMap();
        private final short f;
        private final String g;

        static {
            Iterator it = EnumSet.allOf(e.class).iterator();
            while (it.hasNext()) {
                e eVar = (e) it.next();
                e.put(eVar.b(), eVar);
            }
        }

        private e(short s, String str) {
            this.f = s;
            this.g = str;
        }

        public short a() {
            return this.f;
        }

        public String b() {
            return this.g;
        }
    }

    public String a() {
        return this.f15a;
    }

    public an a(String str) {
        this.f15a = str;
        return this;
    }

    public void a(boolean z) {
        if (!z) {
            this.f15a = null;
        }
    }

    public void a(x xVar) {
        if (this.b == null) {
            this.b = new ArrayList();
        }
        this.b.add(xVar);
    }

    public boolean b() {
        return this.b != null;
    }

    public void b(boolean z) {
        if (!z) {
            this.b = null;
        }
    }

    public void a(aa aaVar) {
        if (this.c == null) {
            this.c = new ArrayList();
        }
        this.c.add(aaVar);
    }

    public boolean c() {
        return this.c != null;
    }

    public void c(boolean z) {
        if (!z) {
            this.c = null;
        }
    }

    public boolean d() {
        return this.d != null;
    }

    public void d(boolean z) {
        if (!z) {
            this.d = null;
        }
    }

    public void a(cr crVar) throws ca {
        k.get(crVar.y()).b().b(crVar, this);
    }

    public void b(cr crVar) throws ca {
        k.get(crVar.y()).b().a(crVar, this);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("InstantMsg(");
        sb.append("id:");
        if (this.f15a == null) {
            sb.append("null");
        } else {
            sb.append(this.f15a);
        }
        if (b()) {
            sb.append(", ");
            sb.append("errors:");
            if (this.b == null) {
                sb.append("null");
            } else {
                sb.append(this.b);
            }
        }
        if (c()) {
            sb.append(", ");
            sb.append("events:");
            if (this.c == null) {
                sb.append("null");
            } else {
                sb.append(this.c);
            }
        }
        if (d()) {
            sb.append(", ");
            sb.append("game_events:");
            if (this.d == null) {
                sb.append("null");
            } else {
                sb.append(this.d);
            }
        }
        sb.append(")");
        return sb.toString();
    }

    public void e() throws ca {
        if (this.f15a == null) {
            throw new cs("Required field 'id' was not present! Struct: " + toString());
        }
    }

    /* compiled from: InstantMsg */
    private static class b implements cz {
        private b() {
        }

        /* renamed from: a */
        public a b() {
            return new a();
        }
    }

    /* compiled from: InstantMsg */
    private static class a extends da<an> {
        private a() {
        }

        /* renamed from: a */
        public void b(cr crVar, an anVar) throws ca {
            crVar.f();
            while (true) {
                co h = crVar.h();
                if (h.b == 0) {
                    crVar.g();
                    anVar.e();
                    return;
                }
                switch (h.c) {
                    case 1:
                        if (h.b != 11) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            anVar.f15a = crVar.v();
                            anVar.a(true);
                            break;
                        }
                    case 2:
                        if (h.b != 15) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            cp l = crVar.l();
                            anVar.b = new ArrayList(l.b);
                            for (int i = 0; i < l.b; i++) {
                                x xVar = new x();
                                xVar.a(crVar);
                                anVar.b.add(xVar);
                            }
                            crVar.m();
                            anVar.b(true);
                            break;
                        }
                    case 3:
                        if (h.b != 15) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            cp l2 = crVar.l();
                            anVar.c = new ArrayList(l2.b);
                            for (int i2 = 0; i2 < l2.b; i2++) {
                                aa aaVar = new aa();
                                aaVar.a(crVar);
                                anVar.c.add(aaVar);
                            }
                            crVar.m();
                            anVar.c(true);
                            break;
                        }
                    case 4:
                        if (h.b != 15) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            cp l3 = crVar.l();
                            anVar.d = new ArrayList(l3.b);
                            for (int i3 = 0; i3 < l3.b; i3++) {
                                aa aaVar2 = new aa();
                                aaVar2.a(crVar);
                                anVar.d.add(aaVar2);
                            }
                            crVar.m();
                            anVar.d(true);
                            break;
                        }
                    default:
                        cu.a(crVar, h.b);
                        break;
                }
                crVar.i();
            }
        }

        /* renamed from: b */
        public void a(cr crVar, an anVar) throws ca {
            anVar.e();
            crVar.a(an.f);
            if (anVar.f15a != null) {
                crVar.a(an.g);
                crVar.a(anVar.f15a);
                crVar.b();
            }
            if (anVar.b != null && anVar.b()) {
                crVar.a(an.h);
                crVar.a(new cp((byte) 12, anVar.b.size()));
                for (x b : anVar.b) {
                    b.b(crVar);
                }
                crVar.e();
                crVar.b();
            }
            if (anVar.c != null && anVar.c()) {
                crVar.a(an.i);
                crVar.a(new cp((byte) 12, anVar.c.size()));
                for (aa b2 : anVar.c) {
                    b2.b(crVar);
                }
                crVar.e();
                crVar.b();
            }
            if (anVar.d != null && anVar.d()) {
                crVar.a(an.j);
                crVar.a(new cp((byte) 12, anVar.d.size()));
                for (aa b3 : anVar.d) {
                    b3.b(crVar);
                }
                crVar.e();
                crVar.b();
            }
            crVar.c();
            crVar.a();
        }
    }

    /* compiled from: InstantMsg */
    private static class d implements cz {
        private d() {
        }

        /* renamed from: a */
        public c b() {
            return new c();
        }
    }

    /* compiled from: InstantMsg */
    private static class c extends db<an> {
        private c() {
        }

        public void a(cr crVar, an anVar) throws ca {
            cx cxVar = (cx) crVar;
            cxVar.a(anVar.f15a);
            BitSet bitSet = new BitSet();
            if (anVar.b()) {
                bitSet.set(0);
            }
            if (anVar.c()) {
                bitSet.set(1);
            }
            if (anVar.d()) {
                bitSet.set(2);
            }
            cxVar.a(bitSet, 3);
            if (anVar.b()) {
                cxVar.a(anVar.b.size());
                for (x b : anVar.b) {
                    b.b(cxVar);
                }
            }
            if (anVar.c()) {
                cxVar.a(anVar.c.size());
                for (aa b2 : anVar.c) {
                    b2.b(cxVar);
                }
            }
            if (anVar.d()) {
                cxVar.a(anVar.d.size());
                for (aa b3 : anVar.d) {
                    b3.b(cxVar);
                }
            }
        }

        public void b(cr crVar, an anVar) throws ca {
            cx cxVar = (cx) crVar;
            anVar.f15a = cxVar.v();
            anVar.a(true);
            BitSet b = cxVar.b(3);
            if (b.get(0)) {
                cp cpVar = new cp((byte) 12, cxVar.s());
                anVar.b = new ArrayList(cpVar.b);
                for (int i = 0; i < cpVar.b; i++) {
                    x xVar = new x();
                    xVar.a(cxVar);
                    anVar.b.add(xVar);
                }
                anVar.b(true);
            }
            if (b.get(1)) {
                cp cpVar2 = new cp((byte) 12, cxVar.s());
                anVar.c = new ArrayList(cpVar2.b);
                for (int i2 = 0; i2 < cpVar2.b; i2++) {
                    aa aaVar = new aa();
                    aaVar.a(cxVar);
                    anVar.c.add(aaVar);
                }
                anVar.c(true);
            }
            if (b.get(2)) {
                cp cpVar3 = new cp((byte) 12, cxVar.s());
                anVar.d = new ArrayList(cpVar3.b);
                for (int i3 = 0; i3 < cpVar3.b; i3++) {
                    aa aaVar2 = new aa();
                    aaVar2.a(cxVar);
                    anVar.d.add(aaVar2);
                }
                anVar.d(true);
            }
        }
    }
}
