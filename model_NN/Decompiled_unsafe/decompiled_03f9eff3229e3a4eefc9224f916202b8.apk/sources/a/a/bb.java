package a.a;

import java.io.Serializable;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* compiled from: Response */
public class bb implements bw<bb, e>, Serializable, Cloneable {
    public static final Map<e, cg> d;
    /* access modifiers changed from: private */
    public static final cw e = new cw("Response");
    /* access modifiers changed from: private */
    public static final co f = new co("resp_code", (byte) 8, 1);
    /* access modifiers changed from: private */
    public static final co g = new co("msg", (byte) 11, 2);
    /* access modifiers changed from: private */
    public static final co h = new co("imprint", (byte) 12, 3);
    private static final Map<Class<? extends cy>, cz> i = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public int f31a;
    public String b;
    public aj c;
    private byte j = 0;
    private e[] k = {e.MSG, e.IMPRINT};

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [a.a.bb$e, a.a.cg]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        i.put(da.class, new b());
        i.put(db.class, new d());
        EnumMap enumMap = new EnumMap(e.class);
        enumMap.put((Object) e.RESP_CODE, (Object) new cg("resp_code", (byte) 1, new ch((byte) 8)));
        enumMap.put((Object) e.MSG, (Object) new cg("msg", (byte) 2, new ch((byte) 11)));
        enumMap.put((Object) e.IMPRINT, (Object) new cg("imprint", (byte) 2, new ck((byte) 12, aj.class)));
        d = Collections.unmodifiableMap(enumMap);
        cg.a(bb.class, d);
    }

    /* compiled from: Response */
    public enum e implements cb {
        RESP_CODE(1, "resp_code"),
        MSG(2, "msg"),
        IMPRINT(3, "imprint");
        
        private static final Map<String, e> d = new HashMap();
        private final short e;
        private final String f;

        static {
            Iterator it = EnumSet.allOf(e.class).iterator();
            while (it.hasNext()) {
                e eVar = (e) it.next();
                d.put(eVar.b(), eVar);
            }
        }

        private e(short s, String str) {
            this.e = s;
            this.f = str;
        }

        public short a() {
            return this.e;
        }

        public String b() {
            return this.f;
        }
    }

    public boolean a() {
        return bu.a(this.j, 0);
    }

    public void a(boolean z) {
        this.j = bu.a(this.j, 0, z);
    }

    public String b() {
        return this.b;
    }

    public boolean c() {
        return this.b != null;
    }

    public void b(boolean z) {
        if (!z) {
            this.b = null;
        }
    }

    public aj d() {
        return this.c;
    }

    public boolean e() {
        return this.c != null;
    }

    public void c(boolean z) {
        if (!z) {
            this.c = null;
        }
    }

    public void a(cr crVar) throws ca {
        i.get(crVar.y()).b().b(crVar, this);
    }

    public void b(cr crVar) throws ca {
        i.get(crVar.y()).b().a(crVar, this);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("Response(");
        sb.append("resp_code:");
        sb.append(this.f31a);
        if (c()) {
            sb.append(", ");
            sb.append("msg:");
            if (this.b == null) {
                sb.append("null");
            } else {
                sb.append(this.b);
            }
        }
        if (e()) {
            sb.append(", ");
            sb.append("imprint:");
            if (this.c == null) {
                sb.append("null");
            } else {
                sb.append(this.c);
            }
        }
        sb.append(")");
        return sb.toString();
    }

    public void f() throws ca {
        if (this.c != null) {
            this.c.f();
        }
    }

    /* compiled from: Response */
    private static class b implements cz {
        private b() {
        }

        /* renamed from: a */
        public a b() {
            return new a();
        }
    }

    /* compiled from: Response */
    private static class a extends da<bb> {
        private a() {
        }

        /* renamed from: a */
        public void b(cr crVar, bb bbVar) throws ca {
            crVar.f();
            while (true) {
                co h = crVar.h();
                if (h.b == 0) {
                    crVar.g();
                    if (!bbVar.a()) {
                        throw new cs("Required field 'resp_code' was not found in serialized data! Struct: " + toString());
                    }
                    bbVar.f();
                    return;
                }
                switch (h.c) {
                    case 1:
                        if (h.b != 8) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            bbVar.f31a = crVar.s();
                            bbVar.a(true);
                            break;
                        }
                    case 2:
                        if (h.b != 11) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            bbVar.b = crVar.v();
                            bbVar.b(true);
                            break;
                        }
                    case 3:
                        if (h.b != 12) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            bbVar.c = new aj();
                            bbVar.c.a(crVar);
                            bbVar.c(true);
                            break;
                        }
                    default:
                        cu.a(crVar, h.b);
                        break;
                }
                crVar.i();
            }
        }

        /* renamed from: b */
        public void a(cr crVar, bb bbVar) throws ca {
            bbVar.f();
            crVar.a(bb.e);
            crVar.a(bb.f);
            crVar.a(bbVar.f31a);
            crVar.b();
            if (bbVar.b != null && bbVar.c()) {
                crVar.a(bb.g);
                crVar.a(bbVar.b);
                crVar.b();
            }
            if (bbVar.c != null && bbVar.e()) {
                crVar.a(bb.h);
                bbVar.c.b(crVar);
                crVar.b();
            }
            crVar.c();
            crVar.a();
        }
    }

    /* compiled from: Response */
    private static class d implements cz {
        private d() {
        }

        /* renamed from: a */
        public c b() {
            return new c();
        }
    }

    /* compiled from: Response */
    private static class c extends db<bb> {
        private c() {
        }

        public void a(cr crVar, bb bbVar) throws ca {
            cx cxVar = (cx) crVar;
            cxVar.a(bbVar.f31a);
            BitSet bitSet = new BitSet();
            if (bbVar.c()) {
                bitSet.set(0);
            }
            if (bbVar.e()) {
                bitSet.set(1);
            }
            cxVar.a(bitSet, 2);
            if (bbVar.c()) {
                cxVar.a(bbVar.b);
            }
            if (bbVar.e()) {
                bbVar.c.b(cxVar);
            }
        }

        public void b(cr crVar, bb bbVar) throws ca {
            cx cxVar = (cx) crVar;
            bbVar.f31a = cxVar.s();
            bbVar.a(true);
            BitSet b = cxVar.b(2);
            if (b.get(0)) {
                bbVar.b = cxVar.v();
                bbVar.b(true);
            }
            if (b.get(1)) {
                bbVar.c = new aj();
                bbVar.c.a(cxVar);
                bbVar.c(true);
            }
        }
    }
}
