package a.a;

import com.sina.weibo.sdk.constant.WBPageConstants;
import java.io.Serializable;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* compiled from: MiscInfo */
public class at implements bw<at, e>, Serializable, Cloneable {
    public static final Map<e, cg> k;
    /* access modifiers changed from: private */
    public static final cw l = new cw("MiscInfo");
    /* access modifiers changed from: private */
    public static final co m = new co("time_zone", (byte) 8, 1);
    /* access modifiers changed from: private */
    public static final co n = new co("language", (byte) 11, 2);
    /* access modifiers changed from: private */
    public static final co o = new co("country", (byte) 11, 3);
    /* access modifiers changed from: private */
    public static final co p = new co(WBPageConstants.ParamKey.LATITUDE, (byte) 4, 4);
    /* access modifiers changed from: private */
    public static final co q = new co(WBPageConstants.ParamKey.LONGITUDE, (byte) 4, 5);
    /* access modifiers changed from: private */
    public static final co r = new co("carrier", (byte) 11, 6);
    /* access modifiers changed from: private */
    public static final co s = new co("latency", (byte) 8, 7);
    /* access modifiers changed from: private */
    public static final co t = new co("display_name", (byte) 11, 8);
    /* access modifiers changed from: private */
    public static final co u = new co("access_type", (byte) 8, 9);
    /* access modifiers changed from: private */
    public static final co v = new co("access_subtype", (byte) 11, 10);
    private static final Map<Class<? extends cy>, cz> w = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public int f21a;
    public String b;
    public String c;
    public double d;
    public double e;
    public String f;
    public int g;
    public String h;
    public k i;
    public String j;
    private byte x = 0;
    private e[] y = {e.TIME_ZONE, e.LANGUAGE, e.COUNTRY, e.LATITUDE, e.LONGITUDE, e.CARRIER, e.LATENCY, e.DISPLAY_NAME, e.ACCESS_TYPE, e.ACCESS_SUBTYPE};

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [a.a.at$e, a.a.cg]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        w.put(da.class, new b());
        w.put(db.class, new d());
        EnumMap enumMap = new EnumMap(e.class);
        enumMap.put((Object) e.TIME_ZONE, (Object) new cg("time_zone", (byte) 2, new ch((byte) 8)));
        enumMap.put((Object) e.LANGUAGE, (Object) new cg("language", (byte) 2, new ch((byte) 11)));
        enumMap.put((Object) e.COUNTRY, (Object) new cg("country", (byte) 2, new ch((byte) 11)));
        enumMap.put((Object) e.LATITUDE, (Object) new cg(WBPageConstants.ParamKey.LATITUDE, (byte) 2, new ch((byte) 4)));
        enumMap.put((Object) e.LONGITUDE, (Object) new cg(WBPageConstants.ParamKey.LONGITUDE, (byte) 2, new ch((byte) 4)));
        enumMap.put((Object) e.CARRIER, (Object) new cg("carrier", (byte) 2, new ch((byte) 11)));
        enumMap.put((Object) e.LATENCY, (Object) new cg("latency", (byte) 2, new ch((byte) 8)));
        enumMap.put((Object) e.DISPLAY_NAME, (Object) new cg("display_name", (byte) 2, new ch((byte) 11)));
        enumMap.put((Object) e.ACCESS_TYPE, (Object) new cg("access_type", (byte) 2, new cf((byte) 16, k.class)));
        enumMap.put((Object) e.ACCESS_SUBTYPE, (Object) new cg("access_subtype", (byte) 2, new ch((byte) 11)));
        k = Collections.unmodifiableMap(enumMap);
        cg.a(at.class, k);
    }

    /* compiled from: MiscInfo */
    public enum e implements cb {
        TIME_ZONE(1, "time_zone"),
        LANGUAGE(2, "language"),
        COUNTRY(3, "country"),
        LATITUDE(4, WBPageConstants.ParamKey.LATITUDE),
        LONGITUDE(5, WBPageConstants.ParamKey.LONGITUDE),
        CARRIER(6, "carrier"),
        LATENCY(7, "latency"),
        DISPLAY_NAME(8, "display_name"),
        ACCESS_TYPE(9, "access_type"),
        ACCESS_SUBTYPE(10, "access_subtype");
        
        private static final Map<String, e> k = new HashMap();
        private final short l;
        private final String m;

        static {
            Iterator it = EnumSet.allOf(e.class).iterator();
            while (it.hasNext()) {
                e eVar = (e) it.next();
                k.put(eVar.b(), eVar);
            }
        }

        private e(short s, String str) {
            this.l = s;
            this.m = str;
        }

        public short a() {
            return this.l;
        }

        public String b() {
            return this.m;
        }
    }

    public at a(int i2) {
        this.f21a = i2;
        a(true);
        return this;
    }

    public boolean a() {
        return bu.a(this.x, 0);
    }

    public void a(boolean z) {
        this.x = bu.a(this.x, 0, z);
    }

    public at a(String str) {
        this.b = str;
        return this;
    }

    public boolean b() {
        return this.b != null;
    }

    public void b(boolean z) {
        if (!z) {
            this.b = null;
        }
    }

    public at b(String str) {
        this.c = str;
        return this;
    }

    public boolean c() {
        return this.c != null;
    }

    public void c(boolean z) {
        if (!z) {
            this.c = null;
        }
    }

    public boolean d() {
        return bu.a(this.x, 1);
    }

    public void d(boolean z) {
        this.x = bu.a(this.x, 1, z);
    }

    public boolean e() {
        return bu.a(this.x, 2);
    }

    public void e(boolean z) {
        this.x = bu.a(this.x, 2, z);
    }

    public at c(String str) {
        this.f = str;
        return this;
    }

    public boolean f() {
        return this.f != null;
    }

    public void f(boolean z) {
        if (!z) {
            this.f = null;
        }
    }

    public boolean g() {
        return bu.a(this.x, 3);
    }

    public void g(boolean z) {
        this.x = bu.a(this.x, 3, z);
    }

    public boolean h() {
        return this.h != null;
    }

    public void h(boolean z) {
        if (!z) {
            this.h = null;
        }
    }

    public at a(k kVar) {
        this.i = kVar;
        return this;
    }

    public boolean i() {
        return this.i != null;
    }

    public void i(boolean z) {
        if (!z) {
            this.i = null;
        }
    }

    public at d(String str) {
        this.j = str;
        return this;
    }

    public boolean j() {
        return this.j != null;
    }

    public void j(boolean z) {
        if (!z) {
            this.j = null;
        }
    }

    public void a(cr crVar) throws ca {
        w.get(crVar.y()).b().b(crVar, this);
    }

    public void b(cr crVar) throws ca {
        w.get(crVar.y()).b().a(crVar, this);
    }

    public String toString() {
        boolean z = false;
        StringBuilder sb = new StringBuilder("MiscInfo(");
        boolean z2 = true;
        if (a()) {
            sb.append("time_zone:");
            sb.append(this.f21a);
            z2 = false;
        }
        if (b()) {
            if (!z2) {
                sb.append(", ");
            }
            sb.append("language:");
            if (this.b == null) {
                sb.append("null");
            } else {
                sb.append(this.b);
            }
            z2 = false;
        }
        if (c()) {
            if (!z2) {
                sb.append(", ");
            }
            sb.append("country:");
            if (this.c == null) {
                sb.append("null");
            } else {
                sb.append(this.c);
            }
            z2 = false;
        }
        if (d()) {
            if (!z2) {
                sb.append(", ");
            }
            sb.append("latitude:");
            sb.append(this.d);
            z2 = false;
        }
        if (e()) {
            if (!z2) {
                sb.append(", ");
            }
            sb.append("longitude:");
            sb.append(this.e);
            z2 = false;
        }
        if (f()) {
            if (!z2) {
                sb.append(", ");
            }
            sb.append("carrier:");
            if (this.f == null) {
                sb.append("null");
            } else {
                sb.append(this.f);
            }
            z2 = false;
        }
        if (g()) {
            if (!z2) {
                sb.append(", ");
            }
            sb.append("latency:");
            sb.append(this.g);
            z2 = false;
        }
        if (h()) {
            if (!z2) {
                sb.append(", ");
            }
            sb.append("display_name:");
            if (this.h == null) {
                sb.append("null");
            } else {
                sb.append(this.h);
            }
            z2 = false;
        }
        if (i()) {
            if (!z2) {
                sb.append(", ");
            }
            sb.append("access_type:");
            if (this.i == null) {
                sb.append("null");
            } else {
                sb.append(this.i);
            }
        } else {
            z = z2;
        }
        if (j()) {
            if (!z) {
                sb.append(", ");
            }
            sb.append("access_subtype:");
            if (this.j == null) {
                sb.append("null");
            } else {
                sb.append(this.j);
            }
        }
        sb.append(")");
        return sb.toString();
    }

    public void k() throws ca {
    }

    /* compiled from: MiscInfo */
    private static class b implements cz {
        private b() {
        }

        /* renamed from: a */
        public a b() {
            return new a();
        }
    }

    /* compiled from: MiscInfo */
    private static class a extends da<at> {
        private a() {
        }

        /* renamed from: a */
        public void b(cr crVar, at atVar) throws ca {
            crVar.f();
            while (true) {
                co h = crVar.h();
                if (h.b == 0) {
                    crVar.g();
                    atVar.k();
                    return;
                }
                switch (h.c) {
                    case 1:
                        if (h.b != 8) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            atVar.f21a = crVar.s();
                            atVar.a(true);
                            break;
                        }
                    case 2:
                        if (h.b != 11) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            atVar.b = crVar.v();
                            atVar.b(true);
                            break;
                        }
                    case 3:
                        if (h.b != 11) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            atVar.c = crVar.v();
                            atVar.c(true);
                            break;
                        }
                    case 4:
                        if (h.b != 4) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            atVar.d = crVar.u();
                            atVar.d(true);
                            break;
                        }
                    case 5:
                        if (h.b != 4) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            atVar.e = crVar.u();
                            atVar.e(true);
                            break;
                        }
                    case 6:
                        if (h.b != 11) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            atVar.f = crVar.v();
                            atVar.f(true);
                            break;
                        }
                    case 7:
                        if (h.b != 8) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            atVar.g = crVar.s();
                            atVar.g(true);
                            break;
                        }
                    case 8:
                        if (h.b != 11) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            atVar.h = crVar.v();
                            atVar.h(true);
                            break;
                        }
                    case 9:
                        if (h.b != 8) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            atVar.i = k.a(crVar.s());
                            atVar.i(true);
                            break;
                        }
                    case 10:
                        if (h.b != 11) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            atVar.j = crVar.v();
                            atVar.j(true);
                            break;
                        }
                    default:
                        cu.a(crVar, h.b);
                        break;
                }
                crVar.i();
            }
        }

        /* renamed from: b */
        public void a(cr crVar, at atVar) throws ca {
            atVar.k();
            crVar.a(at.l);
            if (atVar.a()) {
                crVar.a(at.m);
                crVar.a(atVar.f21a);
                crVar.b();
            }
            if (atVar.b != null && atVar.b()) {
                crVar.a(at.n);
                crVar.a(atVar.b);
                crVar.b();
            }
            if (atVar.c != null && atVar.c()) {
                crVar.a(at.o);
                crVar.a(atVar.c);
                crVar.b();
            }
            if (atVar.d()) {
                crVar.a(at.p);
                crVar.a(atVar.d);
                crVar.b();
            }
            if (atVar.e()) {
                crVar.a(at.q);
                crVar.a(atVar.e);
                crVar.b();
            }
            if (atVar.f != null && atVar.f()) {
                crVar.a(at.r);
                crVar.a(atVar.f);
                crVar.b();
            }
            if (atVar.g()) {
                crVar.a(at.s);
                crVar.a(atVar.g);
                crVar.b();
            }
            if (atVar.h != null && atVar.h()) {
                crVar.a(at.t);
                crVar.a(atVar.h);
                crVar.b();
            }
            if (atVar.i != null && atVar.i()) {
                crVar.a(at.u);
                crVar.a(atVar.i.a());
                crVar.b();
            }
            if (atVar.j != null && atVar.j()) {
                crVar.a(at.v);
                crVar.a(atVar.j);
                crVar.b();
            }
            crVar.c();
            crVar.a();
        }
    }

    /* compiled from: MiscInfo */
    private static class d implements cz {
        private d() {
        }

        /* renamed from: a */
        public c b() {
            return new c();
        }
    }

    /* compiled from: MiscInfo */
    private static class c extends db<at> {
        private c() {
        }

        public void a(cr crVar, at atVar) throws ca {
            cx cxVar = (cx) crVar;
            BitSet bitSet = new BitSet();
            if (atVar.a()) {
                bitSet.set(0);
            }
            if (atVar.b()) {
                bitSet.set(1);
            }
            if (atVar.c()) {
                bitSet.set(2);
            }
            if (atVar.d()) {
                bitSet.set(3);
            }
            if (atVar.e()) {
                bitSet.set(4);
            }
            if (atVar.f()) {
                bitSet.set(5);
            }
            if (atVar.g()) {
                bitSet.set(6);
            }
            if (atVar.h()) {
                bitSet.set(7);
            }
            if (atVar.i()) {
                bitSet.set(8);
            }
            if (atVar.j()) {
                bitSet.set(9);
            }
            cxVar.a(bitSet, 10);
            if (atVar.a()) {
                cxVar.a(atVar.f21a);
            }
            if (atVar.b()) {
                cxVar.a(atVar.b);
            }
            if (atVar.c()) {
                cxVar.a(atVar.c);
            }
            if (atVar.d()) {
                cxVar.a(atVar.d);
            }
            if (atVar.e()) {
                cxVar.a(atVar.e);
            }
            if (atVar.f()) {
                cxVar.a(atVar.f);
            }
            if (atVar.g()) {
                cxVar.a(atVar.g);
            }
            if (atVar.h()) {
                cxVar.a(atVar.h);
            }
            if (atVar.i()) {
                cxVar.a(atVar.i.a());
            }
            if (atVar.j()) {
                cxVar.a(atVar.j);
            }
        }

        public void b(cr crVar, at atVar) throws ca {
            cx cxVar = (cx) crVar;
            BitSet b = cxVar.b(10);
            if (b.get(0)) {
                atVar.f21a = cxVar.s();
                atVar.a(true);
            }
            if (b.get(1)) {
                atVar.b = cxVar.v();
                atVar.b(true);
            }
            if (b.get(2)) {
                atVar.c = cxVar.v();
                atVar.c(true);
            }
            if (b.get(3)) {
                atVar.d = cxVar.u();
                atVar.d(true);
            }
            if (b.get(4)) {
                atVar.e = cxVar.u();
                atVar.e(true);
            }
            if (b.get(5)) {
                atVar.f = cxVar.v();
                atVar.f(true);
            }
            if (b.get(6)) {
                atVar.g = cxVar.s();
                atVar.g(true);
            }
            if (b.get(7)) {
                atVar.h = cxVar.v();
                atVar.h(true);
            }
            if (b.get(8)) {
                atVar.i = k.a(cxVar.s());
                atVar.i(true);
            }
            if (b.get(9)) {
                atVar.j = cxVar.v();
                atVar.j(true);
            }
        }
    }
}
