package a.a;

import java.io.Serializable;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* compiled from: Latent */
public class ap implements bw<ap, e>, Serializable, Cloneable {
    public static final Map<e, cg> c;
    /* access modifiers changed from: private */
    public static final cw d = new cw("Latent");
    /* access modifiers changed from: private */
    public static final co e = new co("latency", (byte) 8, 1);
    /* access modifiers changed from: private */
    public static final co f = new co("interval", (byte) 10, 2);
    private static final Map<Class<? extends cy>, cz> g = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public int f17a;
    public long b;
    private byte h;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [a.a.ap$e, a.a.cg]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        g.put(da.class, new b());
        g.put(db.class, new d());
        EnumMap enumMap = new EnumMap(e.class);
        enumMap.put((Object) e.LATENCY, (Object) new cg("latency", (byte) 1, new ch((byte) 8)));
        enumMap.put((Object) e.INTERVAL, (Object) new cg("interval", (byte) 1, new ch((byte) 10)));
        c = Collections.unmodifiableMap(enumMap);
        cg.a(ap.class, c);
    }

    /* compiled from: Latent */
    public enum e implements cb {
        LATENCY(1, "latency"),
        INTERVAL(2, "interval");
        
        private static final Map<String, e> c = new HashMap();
        private final short d;
        private final String e;

        static {
            Iterator it = EnumSet.allOf(e.class).iterator();
            while (it.hasNext()) {
                e eVar = (e) it.next();
                c.put(eVar.b(), eVar);
            }
        }

        private e(short s, String str) {
            this.d = s;
            this.e = str;
        }

        public short a() {
            return this.d;
        }

        public String b() {
            return this.e;
        }
    }

    public ap() {
        this.h = 0;
    }

    public ap(int i, long j) {
        this();
        this.f17a = i;
        a(true);
        this.b = j;
        b(true);
    }

    public boolean a() {
        return bu.a(this.h, 0);
    }

    public void a(boolean z) {
        this.h = bu.a(this.h, 0, z);
    }

    public boolean b() {
        return bu.a(this.h, 1);
    }

    public void b(boolean z) {
        this.h = bu.a(this.h, 1, z);
    }

    public void a(cr crVar) throws ca {
        g.get(crVar.y()).b().b(crVar, this);
    }

    public void b(cr crVar) throws ca {
        g.get(crVar.y()).b().a(crVar, this);
    }

    public String toString() {
        return "Latent(" + "latency:" + this.f17a + ", " + "interval:" + this.b + ")";
    }

    public void c() throws ca {
    }

    /* compiled from: Latent */
    private static class b implements cz {
        private b() {
        }

        /* renamed from: a */
        public a b() {
            return new a();
        }
    }

    /* compiled from: Latent */
    private static class a extends da<ap> {
        private a() {
        }

        /* renamed from: a */
        public void b(cr crVar, ap apVar) throws ca {
            crVar.f();
            while (true) {
                co h = crVar.h();
                if (h.b == 0) {
                    crVar.g();
                    if (!apVar.a()) {
                        throw new cs("Required field 'latency' was not found in serialized data! Struct: " + toString());
                    } else if (!apVar.b()) {
                        throw new cs("Required field 'interval' was not found in serialized data! Struct: " + toString());
                    } else {
                        apVar.c();
                        return;
                    }
                } else {
                    switch (h.c) {
                        case 1:
                            if (h.b != 8) {
                                cu.a(crVar, h.b);
                                break;
                            } else {
                                apVar.f17a = crVar.s();
                                apVar.a(true);
                                break;
                            }
                        case 2:
                            if (h.b != 10) {
                                cu.a(crVar, h.b);
                                break;
                            } else {
                                apVar.b = crVar.t();
                                apVar.b(true);
                                break;
                            }
                        default:
                            cu.a(crVar, h.b);
                            break;
                    }
                    crVar.i();
                }
            }
        }

        /* renamed from: b */
        public void a(cr crVar, ap apVar) throws ca {
            apVar.c();
            crVar.a(ap.d);
            crVar.a(ap.e);
            crVar.a(apVar.f17a);
            crVar.b();
            crVar.a(ap.f);
            crVar.a(apVar.b);
            crVar.b();
            crVar.c();
            crVar.a();
        }
    }

    /* compiled from: Latent */
    private static class d implements cz {
        private d() {
        }

        /* renamed from: a */
        public c b() {
            return new c();
        }
    }

    /* compiled from: Latent */
    private static class c extends db<ap> {
        private c() {
        }

        public void a(cr crVar, ap apVar) throws ca {
            cx cxVar = (cx) crVar;
            cxVar.a(apVar.f17a);
            cxVar.a(apVar.b);
        }

        public void b(cr crVar, ap apVar) throws ca {
            cx cxVar = (cx) crVar;
            apVar.f17a = cxVar.s();
            apVar.a(true);
            apVar.b = cxVar.t();
            apVar.b(true);
        }
    }
}
