package a.a;

/* compiled from: TSet */
public final class cv {

    /* renamed from: a  reason: collision with root package name */
    public final byte f71a;
    public final int b;

    public cv() {
        this((byte) 0, 0);
    }

    public cv(byte b2, int i) {
        this.f71a = b2;
        this.b = i;
    }

    public cv(cp cpVar) {
        this(cpVar.f67a, cpVar.b);
    }
}
