package a.a;

import com.tencent.open.GameAppOperation;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* compiled from: UMEnvelope */
public class bk implements bw<bk, e>, Serializable, Cloneable {
    public static final Map<e, cg> k;
    /* access modifiers changed from: private */
    public static final cw l = new cw("UMEnvelope");
    /* access modifiers changed from: private */
    public static final co m = new co("version", (byte) 11, 1);
    /* access modifiers changed from: private */
    public static final co n = new co("address", (byte) 11, 2);
    /* access modifiers changed from: private */
    public static final co o = new co(GameAppOperation.GAME_SIGNATURE, (byte) 11, 3);
    /* access modifiers changed from: private */
    public static final co p = new co("serial_num", (byte) 8, 4);
    /* access modifiers changed from: private */
    public static final co q = new co("ts_secs", (byte) 8, 5);
    /* access modifiers changed from: private */
    public static final co r = new co("length", (byte) 8, 6);
    /* access modifiers changed from: private */
    public static final co s = new co("entity", (byte) 11, 7);
    /* access modifiers changed from: private */
    public static final co t = new co("guid", (byte) 11, 8);
    /* access modifiers changed from: private */
    public static final co u = new co("checksum", (byte) 11, 9);
    /* access modifiers changed from: private */
    public static final co v = new co("codex", (byte) 8, 10);
    private static final Map<Class<? extends cy>, cz> w = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public String f40a;
    public String b;
    public String c;
    public int d;
    public int e;
    public int f;
    public ByteBuffer g;
    public String h;
    public String i;
    public int j;
    private byte x = 0;
    private e[] y = {e.CODEX};

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [a.a.bk$e, a.a.cg]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        w.put(da.class, new b());
        w.put(db.class, new d());
        EnumMap enumMap = new EnumMap(e.class);
        enumMap.put((Object) e.VERSION, (Object) new cg("version", (byte) 1, new ch((byte) 11)));
        enumMap.put((Object) e.ADDRESS, (Object) new cg("address", (byte) 1, new ch((byte) 11)));
        enumMap.put((Object) e.SIGNATURE, (Object) new cg(GameAppOperation.GAME_SIGNATURE, (byte) 1, new ch((byte) 11)));
        enumMap.put((Object) e.SERIAL_NUM, (Object) new cg("serial_num", (byte) 1, new ch((byte) 8)));
        enumMap.put((Object) e.TS_SECS, (Object) new cg("ts_secs", (byte) 1, new ch((byte) 8)));
        enumMap.put((Object) e.LENGTH, (Object) new cg("length", (byte) 1, new ch((byte) 8)));
        enumMap.put((Object) e.ENTITY, (Object) new cg("entity", (byte) 1, new ch((byte) 11, true)));
        enumMap.put((Object) e.GUID, (Object) new cg("guid", (byte) 1, new ch((byte) 11)));
        enumMap.put((Object) e.CHECKSUM, (Object) new cg("checksum", (byte) 1, new ch((byte) 11)));
        enumMap.put((Object) e.CODEX, (Object) new cg("codex", (byte) 2, new ch((byte) 8)));
        k = Collections.unmodifiableMap(enumMap);
        cg.a(bk.class, k);
    }

    /* compiled from: UMEnvelope */
    public enum e implements cb {
        VERSION(1, "version"),
        ADDRESS(2, "address"),
        SIGNATURE(3, GameAppOperation.GAME_SIGNATURE),
        SERIAL_NUM(4, "serial_num"),
        TS_SECS(5, "ts_secs"),
        LENGTH(6, "length"),
        ENTITY(7, "entity"),
        GUID(8, "guid"),
        CHECKSUM(9, "checksum"),
        CODEX(10, "codex");
        
        private static final Map<String, e> k = new HashMap();
        private final short l;
        private final String m;

        static {
            Iterator it = EnumSet.allOf(e.class).iterator();
            while (it.hasNext()) {
                e eVar = (e) it.next();
                k.put(eVar.b(), eVar);
            }
        }

        private e(short s, String str) {
            this.l = s;
            this.m = str;
        }

        public short a() {
            return this.l;
        }

        public String b() {
            return this.m;
        }
    }

    public bk a(String str) {
        this.f40a = str;
        return this;
    }

    public void a(boolean z) {
        if (!z) {
            this.f40a = null;
        }
    }

    public bk b(String str) {
        this.b = str;
        return this;
    }

    public void b(boolean z) {
        if (!z) {
            this.b = null;
        }
    }

    public bk c(String str) {
        this.c = str;
        return this;
    }

    public void c(boolean z) {
        if (!z) {
            this.c = null;
        }
    }

    public bk a(int i2) {
        this.d = i2;
        d(true);
        return this;
    }

    public boolean a() {
        return bu.a(this.x, 0);
    }

    public void d(boolean z) {
        this.x = bu.a(this.x, 0, z);
    }

    public bk b(int i2) {
        this.e = i2;
        e(true);
        return this;
    }

    public boolean b() {
        return bu.a(this.x, 1);
    }

    public void e(boolean z) {
        this.x = bu.a(this.x, 1, z);
    }

    public bk c(int i2) {
        this.f = i2;
        f(true);
        return this;
    }

    public boolean c() {
        return bu.a(this.x, 2);
    }

    public void f(boolean z) {
        this.x = bu.a(this.x, 2, z);
    }

    public bk a(byte[] bArr) {
        a(bArr == null ? null : ByteBuffer.wrap(bArr));
        return this;
    }

    public bk a(ByteBuffer byteBuffer) {
        this.g = byteBuffer;
        return this;
    }

    public void g(boolean z) {
        if (!z) {
            this.g = null;
        }
    }

    public bk d(String str) {
        this.h = str;
        return this;
    }

    public void h(boolean z) {
        if (!z) {
            this.h = null;
        }
    }

    public bk e(String str) {
        this.i = str;
        return this;
    }

    public void i(boolean z) {
        if (!z) {
            this.i = null;
        }
    }

    public bk d(int i2) {
        this.j = i2;
        j(true);
        return this;
    }

    public boolean d() {
        return bu.a(this.x, 3);
    }

    public void j(boolean z) {
        this.x = bu.a(this.x, 3, z);
    }

    public void a(cr crVar) throws ca {
        w.get(crVar.y()).b().b(crVar, this);
    }

    public void b(cr crVar) throws ca {
        w.get(crVar.y()).b().a(crVar, this);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("UMEnvelope(");
        sb.append("version:");
        if (this.f40a == null) {
            sb.append("null");
        } else {
            sb.append(this.f40a);
        }
        sb.append(", ");
        sb.append("address:");
        if (this.b == null) {
            sb.append("null");
        } else {
            sb.append(this.b);
        }
        sb.append(", ");
        sb.append("signature:");
        if (this.c == null) {
            sb.append("null");
        } else {
            sb.append(this.c);
        }
        sb.append(", ");
        sb.append("serial_num:");
        sb.append(this.d);
        sb.append(", ");
        sb.append("ts_secs:");
        sb.append(this.e);
        sb.append(", ");
        sb.append("length:");
        sb.append(this.f);
        sb.append(", ");
        sb.append("entity:");
        if (this.g == null) {
            sb.append("null");
        } else {
            bx.a(this.g, sb);
        }
        sb.append(", ");
        sb.append("guid:");
        if (this.h == null) {
            sb.append("null");
        } else {
            sb.append(this.h);
        }
        sb.append(", ");
        sb.append("checksum:");
        if (this.i == null) {
            sb.append("null");
        } else {
            sb.append(this.i);
        }
        if (d()) {
            sb.append(", ");
            sb.append("codex:");
            sb.append(this.j);
        }
        sb.append(")");
        return sb.toString();
    }

    public void e() throws ca {
        if (this.f40a == null) {
            throw new cs("Required field 'version' was not present! Struct: " + toString());
        } else if (this.b == null) {
            throw new cs("Required field 'address' was not present! Struct: " + toString());
        } else if (this.c == null) {
            throw new cs("Required field 'signature' was not present! Struct: " + toString());
        } else if (this.g == null) {
            throw new cs("Required field 'entity' was not present! Struct: " + toString());
        } else if (this.h == null) {
            throw new cs("Required field 'guid' was not present! Struct: " + toString());
        } else if (this.i == null) {
            throw new cs("Required field 'checksum' was not present! Struct: " + toString());
        }
    }

    /* compiled from: UMEnvelope */
    private static class b implements cz {
        private b() {
        }

        /* renamed from: a */
        public a b() {
            return new a();
        }
    }

    /* compiled from: UMEnvelope */
    private static class a extends da<bk> {
        private a() {
        }

        /* renamed from: a */
        public void b(cr crVar, bk bkVar) throws ca {
            crVar.f();
            while (true) {
                co h = crVar.h();
                if (h.b == 0) {
                    crVar.g();
                    if (!bkVar.a()) {
                        throw new cs("Required field 'serial_num' was not found in serialized data! Struct: " + toString());
                    } else if (!bkVar.b()) {
                        throw new cs("Required field 'ts_secs' was not found in serialized data! Struct: " + toString());
                    } else if (!bkVar.c()) {
                        throw new cs("Required field 'length' was not found in serialized data! Struct: " + toString());
                    } else {
                        bkVar.e();
                        return;
                    }
                } else {
                    switch (h.c) {
                        case 1:
                            if (h.b != 11) {
                                cu.a(crVar, h.b);
                                break;
                            } else {
                                bkVar.f40a = crVar.v();
                                bkVar.a(true);
                                break;
                            }
                        case 2:
                            if (h.b != 11) {
                                cu.a(crVar, h.b);
                                break;
                            } else {
                                bkVar.b = crVar.v();
                                bkVar.b(true);
                                break;
                            }
                        case 3:
                            if (h.b != 11) {
                                cu.a(crVar, h.b);
                                break;
                            } else {
                                bkVar.c = crVar.v();
                                bkVar.c(true);
                                break;
                            }
                        case 4:
                            if (h.b != 8) {
                                cu.a(crVar, h.b);
                                break;
                            } else {
                                bkVar.d = crVar.s();
                                bkVar.d(true);
                                break;
                            }
                        case 5:
                            if (h.b != 8) {
                                cu.a(crVar, h.b);
                                break;
                            } else {
                                bkVar.e = crVar.s();
                                bkVar.e(true);
                                break;
                            }
                        case 6:
                            if (h.b != 8) {
                                cu.a(crVar, h.b);
                                break;
                            } else {
                                bkVar.f = crVar.s();
                                bkVar.f(true);
                                break;
                            }
                        case 7:
                            if (h.b != 11) {
                                cu.a(crVar, h.b);
                                break;
                            } else {
                                bkVar.g = crVar.w();
                                bkVar.g(true);
                                break;
                            }
                        case 8:
                            if (h.b != 11) {
                                cu.a(crVar, h.b);
                                break;
                            } else {
                                bkVar.h = crVar.v();
                                bkVar.h(true);
                                break;
                            }
                        case 9:
                            if (h.b != 11) {
                                cu.a(crVar, h.b);
                                break;
                            } else {
                                bkVar.i = crVar.v();
                                bkVar.i(true);
                                break;
                            }
                        case 10:
                            if (h.b != 8) {
                                cu.a(crVar, h.b);
                                break;
                            } else {
                                bkVar.j = crVar.s();
                                bkVar.j(true);
                                break;
                            }
                        default:
                            cu.a(crVar, h.b);
                            break;
                    }
                    crVar.i();
                }
            }
        }

        /* renamed from: b */
        public void a(cr crVar, bk bkVar) throws ca {
            bkVar.e();
            crVar.a(bk.l);
            if (bkVar.f40a != null) {
                crVar.a(bk.m);
                crVar.a(bkVar.f40a);
                crVar.b();
            }
            if (bkVar.b != null) {
                crVar.a(bk.n);
                crVar.a(bkVar.b);
                crVar.b();
            }
            if (bkVar.c != null) {
                crVar.a(bk.o);
                crVar.a(bkVar.c);
                crVar.b();
            }
            crVar.a(bk.p);
            crVar.a(bkVar.d);
            crVar.b();
            crVar.a(bk.q);
            crVar.a(bkVar.e);
            crVar.b();
            crVar.a(bk.r);
            crVar.a(bkVar.f);
            crVar.b();
            if (bkVar.g != null) {
                crVar.a(bk.s);
                crVar.a(bkVar.g);
                crVar.b();
            }
            if (bkVar.h != null) {
                crVar.a(bk.t);
                crVar.a(bkVar.h);
                crVar.b();
            }
            if (bkVar.i != null) {
                crVar.a(bk.u);
                crVar.a(bkVar.i);
                crVar.b();
            }
            if (bkVar.d()) {
                crVar.a(bk.v);
                crVar.a(bkVar.j);
                crVar.b();
            }
            crVar.c();
            crVar.a();
        }
    }

    /* compiled from: UMEnvelope */
    private static class d implements cz {
        private d() {
        }

        /* renamed from: a */
        public c b() {
            return new c();
        }
    }

    /* compiled from: UMEnvelope */
    private static class c extends db<bk> {
        private c() {
        }

        public void a(cr crVar, bk bkVar) throws ca {
            cx cxVar = (cx) crVar;
            cxVar.a(bkVar.f40a);
            cxVar.a(bkVar.b);
            cxVar.a(bkVar.c);
            cxVar.a(bkVar.d);
            cxVar.a(bkVar.e);
            cxVar.a(bkVar.f);
            cxVar.a(bkVar.g);
            cxVar.a(bkVar.h);
            cxVar.a(bkVar.i);
            BitSet bitSet = new BitSet();
            if (bkVar.d()) {
                bitSet.set(0);
            }
            cxVar.a(bitSet, 1);
            if (bkVar.d()) {
                cxVar.a(bkVar.j);
            }
        }

        public void b(cr crVar, bk bkVar) throws ca {
            cx cxVar = (cx) crVar;
            bkVar.f40a = cxVar.v();
            bkVar.a(true);
            bkVar.b = cxVar.v();
            bkVar.b(true);
            bkVar.c = cxVar.v();
            bkVar.c(true);
            bkVar.d = cxVar.s();
            bkVar.d(true);
            bkVar.e = cxVar.s();
            bkVar.e(true);
            bkVar.f = cxVar.s();
            bkVar.f(true);
            bkVar.g = cxVar.w();
            bkVar.g(true);
            bkVar.h = cxVar.v();
            bkVar.h(true);
            bkVar.i = cxVar.v();
            bkVar.i(true);
            if (cxVar.b(1).get(0)) {
                bkVar.j = cxVar.s();
                bkVar.j(true);
            }
        }
    }
}
