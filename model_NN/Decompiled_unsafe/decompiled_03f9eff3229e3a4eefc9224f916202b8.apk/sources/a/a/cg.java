package a.a;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/* compiled from: FieldMetaData */
public class cg implements Serializable {
    private static Map<Class<? extends bw>, Map<? extends cb, cg>> d = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public final String f55a;
    public final byte b;
    public final ch c;

    public cg(String str, byte b2, ch chVar) {
        this.f55a = str;
        this.b = b2;
        this.c = chVar;
    }

    public static void a(Class<? extends bw> cls, Map<? extends cb, cg> map) {
        d.put(cls, map);
    }
}
