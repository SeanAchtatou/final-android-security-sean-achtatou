package a.a;

import com.sina.weibo.sdk.register.mobile.SelectCountryActivity;
import com.tencent.stat.DeviceInfo;
import java.io.Serializable;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* compiled from: Event */
public class aa implements bw<aa, e>, Serializable, Cloneable {
    public static final Map<e, cg> f;
    /* access modifiers changed from: private */
    public static final cw g = new cw("Event");
    /* access modifiers changed from: private */
    public static final co h = new co(SelectCountryActivity.EXTRA_COUNTRY_NAME, (byte) 11, 1);
    /* access modifiers changed from: private */
    public static final co i = new co("properties", (byte) 13, 2);
    /* access modifiers changed from: private */
    public static final co j = new co("duration", (byte) 10, 3);
    /* access modifiers changed from: private */
    public static final co k = new co("acc", (byte) 8, 4);
    /* access modifiers changed from: private */
    public static final co l = new co(DeviceInfo.TAG_TIMESTAMPS, (byte) 10, 5);
    private static final Map<Class<? extends cy>, cz> m = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public String f2a;
    public Map<String, ax> b;
    public long c;
    public int d;
    public long e;
    private byte n = 0;
    private e[] o = {e.DURATION, e.ACC};

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [a.a.aa$e, a.a.cg]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        m.put(da.class, new b());
        m.put(db.class, new d());
        EnumMap enumMap = new EnumMap(e.class);
        enumMap.put((Object) e.NAME, (Object) new cg(SelectCountryActivity.EXTRA_COUNTRY_NAME, (byte) 1, new ch((byte) 11)));
        enumMap.put((Object) e.PROPERTIES, (Object) new cg("properties", (byte) 1, new cj((byte) 13, new ch((byte) 11), new ck((byte) 12, ax.class))));
        enumMap.put((Object) e.DURATION, (Object) new cg("duration", (byte) 2, new ch((byte) 10)));
        enumMap.put((Object) e.ACC, (Object) new cg("acc", (byte) 2, new ch((byte) 8)));
        enumMap.put((Object) e.TS, (Object) new cg(DeviceInfo.TAG_TIMESTAMPS, (byte) 1, new ch((byte) 10)));
        f = Collections.unmodifiableMap(enumMap);
        cg.a(aa.class, f);
    }

    /* compiled from: Event */
    public enum e implements cb {
        NAME(1, SelectCountryActivity.EXTRA_COUNTRY_NAME),
        PROPERTIES(2, "properties"),
        DURATION(3, "duration"),
        ACC(4, "acc"),
        TS(5, DeviceInfo.TAG_TIMESTAMPS);
        
        private static final Map<String, e> f = new HashMap();
        private final short g;
        private final String h;

        static {
            Iterator it = EnumSet.allOf(e.class).iterator();
            while (it.hasNext()) {
                e eVar = (e) it.next();
                f.put(eVar.b(), eVar);
            }
        }

        private e(short s, String str) {
            this.g = s;
            this.h = str;
        }

        public short a() {
            return this.g;
        }

        public String b() {
            return this.h;
        }
    }

    public aa a(String str) {
        this.f2a = str;
        return this;
    }

    public void a(boolean z) {
        if (!z) {
            this.f2a = null;
        }
    }

    public aa a(Map<String, ax> map) {
        this.b = map;
        return this;
    }

    public void b(boolean z) {
        if (!z) {
            this.b = null;
        }
    }

    public aa a(long j2) {
        this.c = j2;
        c(true);
        return this;
    }

    public boolean a() {
        return bu.a(this.n, 0);
    }

    public void c(boolean z) {
        this.n = bu.a(this.n, 0, z);
    }

    public aa a(int i2) {
        this.d = i2;
        d(true);
        return this;
    }

    public boolean b() {
        return bu.a(this.n, 1);
    }

    public void d(boolean z) {
        this.n = bu.a(this.n, 1, z);
    }

    public aa b(long j2) {
        this.e = j2;
        e(true);
        return this;
    }

    public boolean c() {
        return bu.a(this.n, 2);
    }

    public void e(boolean z) {
        this.n = bu.a(this.n, 2, z);
    }

    public void a(cr crVar) throws ca {
        m.get(crVar.y()).b().b(crVar, this);
    }

    public void b(cr crVar) throws ca {
        m.get(crVar.y()).b().a(crVar, this);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("Event(");
        sb.append("name:");
        if (this.f2a == null) {
            sb.append("null");
        } else {
            sb.append(this.f2a);
        }
        sb.append(", ");
        sb.append("properties:");
        if (this.b == null) {
            sb.append("null");
        } else {
            sb.append(this.b);
        }
        if (a()) {
            sb.append(", ");
            sb.append("duration:");
            sb.append(this.c);
        }
        if (b()) {
            sb.append(", ");
            sb.append("acc:");
            sb.append(this.d);
        }
        sb.append(", ");
        sb.append("ts:");
        sb.append(this.e);
        sb.append(")");
        return sb.toString();
    }

    public void d() throws ca {
        if (this.f2a == null) {
            throw new cs("Required field 'name' was not present! Struct: " + toString());
        } else if (this.b == null) {
            throw new cs("Required field 'properties' was not present! Struct: " + toString());
        }
    }

    /* compiled from: Event */
    private static class b implements cz {
        private b() {
        }

        /* renamed from: a */
        public a b() {
            return new a();
        }
    }

    /* compiled from: Event */
    private static class a extends da<aa> {
        private a() {
        }

        /* renamed from: a */
        public void b(cr crVar, aa aaVar) throws ca {
            crVar.f();
            while (true) {
                co h = crVar.h();
                if (h.b == 0) {
                    crVar.g();
                    if (!aaVar.c()) {
                        throw new cs("Required field 'ts' was not found in serialized data! Struct: " + toString());
                    }
                    aaVar.d();
                    return;
                }
                switch (h.c) {
                    case 1:
                        if (h.b != 11) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            aaVar.f2a = crVar.v();
                            aaVar.a(true);
                            break;
                        }
                    case 2:
                        if (h.b != 13) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            cq j = crVar.j();
                            aaVar.b = new HashMap(j.c * 2);
                            for (int i = 0; i < j.c; i++) {
                                String v = crVar.v();
                                ax axVar = new ax();
                                axVar.a(crVar);
                                aaVar.b.put(v, axVar);
                            }
                            crVar.k();
                            aaVar.b(true);
                            break;
                        }
                    case 3:
                        if (h.b != 10) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            aaVar.c = crVar.t();
                            aaVar.c(true);
                            break;
                        }
                    case 4:
                        if (h.b != 8) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            aaVar.d = crVar.s();
                            aaVar.d(true);
                            break;
                        }
                    case 5:
                        if (h.b != 10) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            aaVar.e = crVar.t();
                            aaVar.e(true);
                            break;
                        }
                    default:
                        cu.a(crVar, h.b);
                        break;
                }
                crVar.i();
            }
        }

        /* renamed from: b */
        public void a(cr crVar, aa aaVar) throws ca {
            aaVar.d();
            crVar.a(aa.g);
            if (aaVar.f2a != null) {
                crVar.a(aa.h);
                crVar.a(aaVar.f2a);
                crVar.b();
            }
            if (aaVar.b != null) {
                crVar.a(aa.i);
                crVar.a(new cq((byte) 11, (byte) 12, aaVar.b.size()));
                for (Map.Entry next : aaVar.b.entrySet()) {
                    crVar.a((String) next.getKey());
                    ((ax) next.getValue()).b(crVar);
                }
                crVar.d();
                crVar.b();
            }
            if (aaVar.a()) {
                crVar.a(aa.j);
                crVar.a(aaVar.c);
                crVar.b();
            }
            if (aaVar.b()) {
                crVar.a(aa.k);
                crVar.a(aaVar.d);
                crVar.b();
            }
            crVar.a(aa.l);
            crVar.a(aaVar.e);
            crVar.b();
            crVar.c();
            crVar.a();
        }
    }

    /* compiled from: Event */
    private static class d implements cz {
        private d() {
        }

        /* renamed from: a */
        public c b() {
            return new c();
        }
    }

    /* compiled from: Event */
    private static class c extends db<aa> {
        private c() {
        }

        public void a(cr crVar, aa aaVar) throws ca {
            cx cxVar = (cx) crVar;
            cxVar.a(aaVar.f2a);
            cxVar.a(aaVar.b.size());
            for (Map.Entry next : aaVar.b.entrySet()) {
                cxVar.a((String) next.getKey());
                ((ax) next.getValue()).b(cxVar);
            }
            cxVar.a(aaVar.e);
            BitSet bitSet = new BitSet();
            if (aaVar.a()) {
                bitSet.set(0);
            }
            if (aaVar.b()) {
                bitSet.set(1);
            }
            cxVar.a(bitSet, 2);
            if (aaVar.a()) {
                cxVar.a(aaVar.c);
            }
            if (aaVar.b()) {
                cxVar.a(aaVar.d);
            }
        }

        public void b(cr crVar, aa aaVar) throws ca {
            cx cxVar = (cx) crVar;
            aaVar.f2a = cxVar.v();
            aaVar.a(true);
            cq cqVar = new cq((byte) 11, (byte) 12, cxVar.s());
            aaVar.b = new HashMap(cqVar.c * 2);
            for (int i = 0; i < cqVar.c; i++) {
                String v = cxVar.v();
                ax axVar = new ax();
                axVar.a(cxVar);
                aaVar.b.put(v, axVar);
            }
            aaVar.b(true);
            aaVar.e = cxVar.t();
            aaVar.e(true);
            BitSet b = cxVar.b(2);
            if (b.get(0)) {
                aaVar.c = cxVar.t();
                aaVar.c(true);
            }
            if (b.get(1)) {
                aaVar.d = cxVar.s();
                aaVar.d(true);
            }
        }
    }
}
