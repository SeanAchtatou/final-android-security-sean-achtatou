package a.a;

import java.io.Serializable;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* compiled from: ActiveUser */
public class n implements bw<n, e>, Serializable, Cloneable {
    public static final Map<e, cg> c;
    /* access modifiers changed from: private */
    public static final cw d = new cw("ActiveUser");
    /* access modifiers changed from: private */
    public static final co e = new co("provider", (byte) 11, 1);
    /* access modifiers changed from: private */
    public static final co f = new co("puid", (byte) 11, 2);
    private static final Map<Class<? extends cy>, cz> g = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public String f100a;
    public String b;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [a.a.n$e, a.a.cg]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        g.put(da.class, new b());
        g.put(db.class, new d());
        EnumMap enumMap = new EnumMap(e.class);
        enumMap.put((Object) e.PROVIDER, (Object) new cg("provider", (byte) 1, new ch((byte) 11)));
        enumMap.put((Object) e.PUID, (Object) new cg("puid", (byte) 1, new ch((byte) 11)));
        c = Collections.unmodifiableMap(enumMap);
        cg.a(n.class, c);
    }

    /* compiled from: ActiveUser */
    public enum e implements cb {
        PROVIDER(1, "provider"),
        PUID(2, "puid");
        
        private static final Map<String, e> c = new HashMap();
        private final short d;
        private final String e;

        static {
            Iterator it = EnumSet.allOf(e.class).iterator();
            while (it.hasNext()) {
                e eVar = (e) it.next();
                c.put(eVar.b(), eVar);
            }
        }

        private e(short s, String str) {
            this.d = s;
            this.e = str;
        }

        public short a() {
            return this.d;
        }

        public String b() {
            return this.e;
        }
    }

    public n() {
    }

    public n(String str, String str2) {
        this();
        this.f100a = str;
        this.b = str2;
    }

    public void a(boolean z) {
        if (!z) {
            this.f100a = null;
        }
    }

    public void b(boolean z) {
        if (!z) {
            this.b = null;
        }
    }

    public void a(cr crVar) throws ca {
        g.get(crVar.y()).b().b(crVar, this);
    }

    public void b(cr crVar) throws ca {
        g.get(crVar.y()).b().a(crVar, this);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("ActiveUser(");
        sb.append("provider:");
        if (this.f100a == null) {
            sb.append("null");
        } else {
            sb.append(this.f100a);
        }
        sb.append(", ");
        sb.append("puid:");
        if (this.b == null) {
            sb.append("null");
        } else {
            sb.append(this.b);
        }
        sb.append(")");
        return sb.toString();
    }

    public void a() throws ca {
        if (this.f100a == null) {
            throw new cs("Required field 'provider' was not present! Struct: " + toString());
        } else if (this.b == null) {
            throw new cs("Required field 'puid' was not present! Struct: " + toString());
        }
    }

    /* compiled from: ActiveUser */
    private static class b implements cz {
        private b() {
        }

        /* renamed from: a */
        public a b() {
            return new a();
        }
    }

    /* compiled from: ActiveUser */
    private static class a extends da<n> {
        private a() {
        }

        /* renamed from: a */
        public void b(cr crVar, n nVar) throws ca {
            crVar.f();
            while (true) {
                co h = crVar.h();
                if (h.b == 0) {
                    crVar.g();
                    nVar.a();
                    return;
                }
                switch (h.c) {
                    case 1:
                        if (h.b != 11) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            nVar.f100a = crVar.v();
                            nVar.a(true);
                            break;
                        }
                    case 2:
                        if (h.b != 11) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            nVar.b = crVar.v();
                            nVar.b(true);
                            break;
                        }
                    default:
                        cu.a(crVar, h.b);
                        break;
                }
                crVar.i();
            }
        }

        /* renamed from: b */
        public void a(cr crVar, n nVar) throws ca {
            nVar.a();
            crVar.a(n.d);
            if (nVar.f100a != null) {
                crVar.a(n.e);
                crVar.a(nVar.f100a);
                crVar.b();
            }
            if (nVar.b != null) {
                crVar.a(n.f);
                crVar.a(nVar.b);
                crVar.b();
            }
            crVar.c();
            crVar.a();
        }
    }

    /* compiled from: ActiveUser */
    private static class d implements cz {
        private d() {
        }

        /* renamed from: a */
        public c b() {
            return new c();
        }
    }

    /* compiled from: ActiveUser */
    private static class c extends db<n> {
        private c() {
        }

        public void a(cr crVar, n nVar) throws ca {
            cx cxVar = (cx) crVar;
            cxVar.a(nVar.f100a);
            cxVar.a(nVar.b);
        }

        public void b(cr crVar, n nVar) throws ca {
            cx cxVar = (cx) crVar;
            nVar.f100a = cxVar.v();
            nVar.a(true);
            nVar.b = cxVar.v();
            nVar.b(true);
        }
    }
}
