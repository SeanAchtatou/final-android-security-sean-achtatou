package a.a;

import java.io.Serializable;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* compiled from: Page */
public class av implements bw<av, e>, Serializable, Cloneable {
    public static final Map<e, cg> c;
    /* access modifiers changed from: private */
    public static final cw d = new cw("Page");
    /* access modifiers changed from: private */
    public static final co e = new co("page_name", (byte) 11, 1);
    /* access modifiers changed from: private */
    public static final co f = new co("duration", (byte) 10, 2);
    private static final Map<Class<? extends cy>, cz> g = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public String f23a;
    public long b;
    private byte h = 0;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [a.a.av$e, a.a.cg]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        g.put(da.class, new b());
        g.put(db.class, new d());
        EnumMap enumMap = new EnumMap(e.class);
        enumMap.put((Object) e.PAGE_NAME, (Object) new cg("page_name", (byte) 1, new ch((byte) 11)));
        enumMap.put((Object) e.DURATION, (Object) new cg("duration", (byte) 1, new ch((byte) 10)));
        c = Collections.unmodifiableMap(enumMap);
        cg.a(av.class, c);
    }

    /* compiled from: Page */
    public enum e implements cb {
        PAGE_NAME(1, "page_name"),
        DURATION(2, "duration");
        
        private static final Map<String, e> c = new HashMap();
        private final short d;
        private final String e;

        static {
            Iterator it = EnumSet.allOf(e.class).iterator();
            while (it.hasNext()) {
                e eVar = (e) it.next();
                c.put(eVar.b(), eVar);
            }
        }

        private e(short s, String str) {
            this.d = s;
            this.e = str;
        }

        public short a() {
            return this.d;
        }

        public String b() {
            return this.e;
        }
    }

    public av a(String str) {
        this.f23a = str;
        return this;
    }

    public void a(boolean z) {
        if (!z) {
            this.f23a = null;
        }
    }

    public av a(long j) {
        this.b = j;
        b(true);
        return this;
    }

    public boolean a() {
        return bu.a(this.h, 0);
    }

    public void b(boolean z) {
        this.h = bu.a(this.h, 0, z);
    }

    public void a(cr crVar) throws ca {
        g.get(crVar.y()).b().b(crVar, this);
    }

    public void b(cr crVar) throws ca {
        g.get(crVar.y()).b().a(crVar, this);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("Page(");
        sb.append("page_name:");
        if (this.f23a == null) {
            sb.append("null");
        } else {
            sb.append(this.f23a);
        }
        sb.append(", ");
        sb.append("duration:");
        sb.append(this.b);
        sb.append(")");
        return sb.toString();
    }

    public void b() throws ca {
        if (this.f23a == null) {
            throw new cs("Required field 'page_name' was not present! Struct: " + toString());
        }
    }

    /* compiled from: Page */
    private static class b implements cz {
        private b() {
        }

        /* renamed from: a */
        public a b() {
            return new a();
        }
    }

    /* compiled from: Page */
    private static class a extends da<av> {
        private a() {
        }

        /* renamed from: a */
        public void b(cr crVar, av avVar) throws ca {
            crVar.f();
            while (true) {
                co h = crVar.h();
                if (h.b == 0) {
                    crVar.g();
                    if (!avVar.a()) {
                        throw new cs("Required field 'duration' was not found in serialized data! Struct: " + toString());
                    }
                    avVar.b();
                    return;
                }
                switch (h.c) {
                    case 1:
                        if (h.b != 11) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            avVar.f23a = crVar.v();
                            avVar.a(true);
                            break;
                        }
                    case 2:
                        if (h.b != 10) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            avVar.b = crVar.t();
                            avVar.b(true);
                            break;
                        }
                    default:
                        cu.a(crVar, h.b);
                        break;
                }
                crVar.i();
            }
        }

        /* renamed from: b */
        public void a(cr crVar, av avVar) throws ca {
            avVar.b();
            crVar.a(av.d);
            if (avVar.f23a != null) {
                crVar.a(av.e);
                crVar.a(avVar.f23a);
                crVar.b();
            }
            crVar.a(av.f);
            crVar.a(avVar.b);
            crVar.b();
            crVar.c();
            crVar.a();
        }
    }

    /* compiled from: Page */
    private static class d implements cz {
        private d() {
        }

        /* renamed from: a */
        public c b() {
            return new c();
        }
    }

    /* compiled from: Page */
    private static class c extends db<av> {
        private c() {
        }

        public void a(cr crVar, av avVar) throws ca {
            cx cxVar = (cx) crVar;
            cxVar.a(avVar.f23a);
            cxVar.a(avVar.b);
        }

        public void b(cr crVar, av avVar) throws ca {
            cx cxVar = (cx) crVar;
            avVar.f23a = cxVar.v();
            avVar.a(true);
            avVar.b = cxVar.t();
            avVar.b(true);
        }
    }
}
