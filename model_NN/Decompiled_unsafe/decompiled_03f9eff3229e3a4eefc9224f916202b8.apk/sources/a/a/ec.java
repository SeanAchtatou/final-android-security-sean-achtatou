package a.a;

import a.a.cl;
import android.content.Context;
import com.umeng.analytics.a;
import com.umeng.analytics.m;

/* compiled from: Sender */
public class ec {

    /* renamed from: a  reason: collision with root package name */
    private cm f93a;
    private dh b;
    private final int c = 1;
    private Context d;
    /* access modifiers changed from: private */
    public b e;
    /* access modifiers changed from: private */
    public dy f;
    private bi g;
    private boolean h = false;
    /* access modifiers changed from: private */
    public boolean i;

    public ec(Context context, b bVar) {
        this.f93a = cm.a(context);
        this.b = dh.a(context);
        this.d = context;
        this.e = bVar;
        this.f = new dy(context);
        this.f.a(this.e);
    }

    public void a(bi biVar) {
        this.g = biVar;
    }

    public void a(boolean z) {
        this.h = z;
    }

    public void b(boolean z) {
        this.i = z;
    }

    public void a(ea eaVar) {
        this.b.a(eaVar);
    }

    public void a() {
        if (this.g != null) {
            c();
        } else {
            b();
        }
    }

    private void b() {
        m.a(this.d).h().a(new ed(this));
    }

    private void c() {
        br b2;
        int a2;
        this.f93a.a();
        bi biVar = this.g;
        biVar.a(this.f93a.b());
        byte[] b3 = b(biVar);
        if (b3 == null) {
            bn.d("MobclickAgent", "message is null");
            return;
        }
        if (!this.h) {
            b2 = br.a(this.d, a.a(this.d), b3);
        } else {
            b2 = br.b(this.d, a.a(this.d), b3);
        }
        byte[] c2 = b2.c();
        m.a(this.d).f();
        byte[] a3 = this.f.a(c2);
        if (a3 == null) {
            a2 = 1;
        } else {
            a2 = a(a3);
        }
        switch (a2) {
            case 1:
                if (!this.i) {
                    m.a(this.d).b(c2);
                }
                bn.b("MobclickAgent", "connection error");
                return;
            case 2:
                if (this.e.h()) {
                    this.e.g();
                }
                this.f93a.c();
                this.e.f();
                return;
            case 3:
                this.e.f();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public int a(byte[] bArr) {
        bb bbVar = new bb();
        try {
            new bz(new cl.a()).a(bbVar, bArr);
            if (bbVar.f31a == 1) {
                this.b.b(bbVar.d());
                this.b.c();
            }
            bn.a("MobclickAgent", "send log:" + bbVar.b());
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        if (bbVar.f31a == 1) {
            return 2;
        }
        return 3;
    }

    private byte[] b(bi biVar) {
        if (biVar == null) {
            return null;
        }
        try {
            byte[] a2 = new cc().a(biVar);
            if (bn.f43a) {
                bn.c("MobclickAgent", biVar.toString());
            }
            return a2;
        } catch (Exception e2) {
            bn.b("MobclickAgent", "Fail to serialize log ...", e2);
            return null;
        }
    }
}
