package a.a;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/* compiled from: AbstractIdTracker */
public abstract class a {

    /* renamed from: a  reason: collision with root package name */
    private final int f1a = 10;
    private final int b = 20;
    private final String c;
    private List<ac> d;
    private ae e;

    public abstract String f();

    public a(String str) {
        this.c = str;
    }

    public boolean a() {
        return g();
    }

    public String b() {
        return this.c;
    }

    public boolean c() {
        if (this.e == null || this.e.d() <= 20) {
            return true;
        }
        return false;
    }

    private boolean g() {
        ae aeVar = this.e;
        String a2 = aeVar == null ? null : aeVar.a();
        int d2 = aeVar == null ? 0 : aeVar.d();
        String a3 = a(f());
        if (a3 == null || a3.equals(a2)) {
            return false;
        }
        if (aeVar == null) {
            aeVar = new ae();
        }
        aeVar.a(a3);
        aeVar.a(System.currentTimeMillis());
        aeVar.a(d2 + 1);
        ac acVar = new ac();
        acVar.a(this.c);
        acVar.c(a3);
        acVar.b(a2);
        acVar.a(aeVar.b());
        if (this.d == null) {
            this.d = new ArrayList(2);
        }
        this.d.add(acVar);
        if (this.d.size() > 10) {
            this.d.remove(0);
        }
        this.e = aeVar;
        return true;
    }

    public ae d() {
        return this.e;
    }

    public List<ac> e() {
        return this.d;
    }

    public void a(List<ac> list) {
        this.d = list;
    }

    public String a(String str) {
        if (str == null) {
            return null;
        }
        String trim = str.trim();
        if (trim.length() == 0 || "0".equals(trim) || "unknown".equals(trim.toLowerCase(Locale.US))) {
            return null;
        }
        return trim;
    }

    public void a(ag agVar) {
        this.e = agVar.a().get(this.c);
        List<ac> b2 = agVar.b();
        if (b2 != null && b2.size() > 0) {
            if (this.d == null) {
                this.d = new ArrayList();
            }
            for (ac next : b2) {
                if (this.c.equals(next.f4a)) {
                    this.d.add(next);
                }
            }
        }
    }
}
