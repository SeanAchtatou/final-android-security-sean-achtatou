package a.a;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* compiled from: IdTracking */
public class ag implements bw<ag, e>, Serializable, Cloneable {
    public static final Map<e, cg> d;
    /* access modifiers changed from: private */
    public static final cw e = new cw("IdTracking");
    /* access modifiers changed from: private */
    public static final co f = new co("snapshots", (byte) 13, 1);
    /* access modifiers changed from: private */
    public static final co g = new co("journals", (byte) 15, 2);
    /* access modifiers changed from: private */
    public static final co h = new co("checksum", (byte) 11, 3);
    private static final Map<Class<? extends cy>, cz> i = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public Map<String, ae> f8a;
    public List<ac> b;
    public String c;
    private e[] j = {e.JOURNALS, e.CHECKSUM};

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [a.a.ag$e, a.a.cg]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        i.put(da.class, new b());
        i.put(db.class, new d());
        EnumMap enumMap = new EnumMap(e.class);
        enumMap.put((Object) e.SNAPSHOTS, (Object) new cg("snapshots", (byte) 1, new cj((byte) 13, new ch((byte) 11), new ck((byte) 12, ae.class))));
        enumMap.put((Object) e.JOURNALS, (Object) new cg("journals", (byte) 2, new ci((byte) 15, new ck((byte) 12, ac.class))));
        enumMap.put((Object) e.CHECKSUM, (Object) new cg("checksum", (byte) 2, new ch((byte) 11)));
        d = Collections.unmodifiableMap(enumMap);
        cg.a(ag.class, d);
    }

    /* compiled from: IdTracking */
    public enum e implements cb {
        SNAPSHOTS(1, "snapshots"),
        JOURNALS(2, "journals"),
        CHECKSUM(3, "checksum");
        
        private static final Map<String, e> d = new HashMap();
        private final short e;
        private final String f;

        static {
            Iterator it = EnumSet.allOf(e.class).iterator();
            while (it.hasNext()) {
                e eVar = (e) it.next();
                d.put(eVar.b(), eVar);
            }
        }

        private e(short s, String str) {
            this.e = s;
            this.f = str;
        }

        public short a() {
            return this.e;
        }

        public String b() {
            return this.f;
        }
    }

    public Map<String, ae> a() {
        return this.f8a;
    }

    public ag a(Map<String, ae> map) {
        this.f8a = map;
        return this;
    }

    public void a(boolean z) {
        if (!z) {
            this.f8a = null;
        }
    }

    public List<ac> b() {
        return this.b;
    }

    public ag a(List<ac> list) {
        this.b = list;
        return this;
    }

    public boolean c() {
        return this.b != null;
    }

    public void b(boolean z) {
        if (!z) {
            this.b = null;
        }
    }

    public boolean d() {
        return this.c != null;
    }

    public void c(boolean z) {
        if (!z) {
            this.c = null;
        }
    }

    public void a(cr crVar) throws ca {
        i.get(crVar.y()).b().b(crVar, this);
    }

    public void b(cr crVar) throws ca {
        i.get(crVar.y()).b().a(crVar, this);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("IdTracking(");
        sb.append("snapshots:");
        if (this.f8a == null) {
            sb.append("null");
        } else {
            sb.append(this.f8a);
        }
        if (c()) {
            sb.append(", ");
            sb.append("journals:");
            if (this.b == null) {
                sb.append("null");
            } else {
                sb.append(this.b);
            }
        }
        if (d()) {
            sb.append(", ");
            sb.append("checksum:");
            if (this.c == null) {
                sb.append("null");
            } else {
                sb.append(this.c);
            }
        }
        sb.append(")");
        return sb.toString();
    }

    public void e() throws ca {
        if (this.f8a == null) {
            throw new cs("Required field 'snapshots' was not present! Struct: " + toString());
        }
    }

    /* compiled from: IdTracking */
    private static class b implements cz {
        private b() {
        }

        /* renamed from: a */
        public a b() {
            return new a();
        }
    }

    /* compiled from: IdTracking */
    private static class a extends da<ag> {
        private a() {
        }

        /* renamed from: a */
        public void b(cr crVar, ag agVar) throws ca {
            crVar.f();
            while (true) {
                co h = crVar.h();
                if (h.b == 0) {
                    crVar.g();
                    agVar.e();
                    return;
                }
                switch (h.c) {
                    case 1:
                        if (h.b != 13) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            cq j = crVar.j();
                            agVar.f8a = new HashMap(j.c * 2);
                            for (int i = 0; i < j.c; i++) {
                                String v = crVar.v();
                                ae aeVar = new ae();
                                aeVar.a(crVar);
                                agVar.f8a.put(v, aeVar);
                            }
                            crVar.k();
                            agVar.a(true);
                            break;
                        }
                    case 2:
                        if (h.b != 15) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            cp l = crVar.l();
                            agVar.b = new ArrayList(l.b);
                            for (int i2 = 0; i2 < l.b; i2++) {
                                ac acVar = new ac();
                                acVar.a(crVar);
                                agVar.b.add(acVar);
                            }
                            crVar.m();
                            agVar.b(true);
                            break;
                        }
                    case 3:
                        if (h.b != 11) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            agVar.c = crVar.v();
                            agVar.c(true);
                            break;
                        }
                    default:
                        cu.a(crVar, h.b);
                        break;
                }
                crVar.i();
            }
        }

        /* renamed from: b */
        public void a(cr crVar, ag agVar) throws ca {
            agVar.e();
            crVar.a(ag.e);
            if (agVar.f8a != null) {
                crVar.a(ag.f);
                crVar.a(new cq((byte) 11, (byte) 12, agVar.f8a.size()));
                for (Map.Entry next : agVar.f8a.entrySet()) {
                    crVar.a((String) next.getKey());
                    ((ae) next.getValue()).b(crVar);
                }
                crVar.d();
                crVar.b();
            }
            if (agVar.b != null && agVar.c()) {
                crVar.a(ag.g);
                crVar.a(new cp((byte) 12, agVar.b.size()));
                for (ac b : agVar.b) {
                    b.b(crVar);
                }
                crVar.e();
                crVar.b();
            }
            if (agVar.c != null && agVar.d()) {
                crVar.a(ag.h);
                crVar.a(agVar.c);
                crVar.b();
            }
            crVar.c();
            crVar.a();
        }
    }

    /* compiled from: IdTracking */
    private static class d implements cz {
        private d() {
        }

        /* renamed from: a */
        public c b() {
            return new c();
        }
    }

    /* compiled from: IdTracking */
    private static class c extends db<ag> {
        private c() {
        }

        public void a(cr crVar, ag agVar) throws ca {
            cx cxVar = (cx) crVar;
            cxVar.a(agVar.f8a.size());
            for (Map.Entry next : agVar.f8a.entrySet()) {
                cxVar.a((String) next.getKey());
                ((ae) next.getValue()).b(cxVar);
            }
            BitSet bitSet = new BitSet();
            if (agVar.c()) {
                bitSet.set(0);
            }
            if (agVar.d()) {
                bitSet.set(1);
            }
            cxVar.a(bitSet, 2);
            if (agVar.c()) {
                cxVar.a(agVar.b.size());
                for (ac b : agVar.b) {
                    b.b(cxVar);
                }
            }
            if (agVar.d()) {
                cxVar.a(agVar.c);
            }
        }

        public void b(cr crVar, ag agVar) throws ca {
            cx cxVar = (cx) crVar;
            cq cqVar = new cq((byte) 11, (byte) 12, cxVar.s());
            agVar.f8a = new HashMap(cqVar.c * 2);
            for (int i = 0; i < cqVar.c; i++) {
                String v = cxVar.v();
                ae aeVar = new ae();
                aeVar.a(cxVar);
                agVar.f8a.put(v, aeVar);
            }
            agVar.a(true);
            BitSet b = cxVar.b(2);
            if (b.get(0)) {
                cp cpVar = new cp((byte) 12, cxVar.s());
                agVar.b = new ArrayList(cpVar.b);
                for (int i2 = 0; i2 < cpVar.b; i2++) {
                    ac acVar = new ac();
                    acVar.a(cxVar);
                    agVar.b.add(acVar);
                }
                agVar.b(true);
            }
            if (b.get(1)) {
                agVar.c = cxVar.v();
                agVar.c(true);
            }
        }
    }
}
