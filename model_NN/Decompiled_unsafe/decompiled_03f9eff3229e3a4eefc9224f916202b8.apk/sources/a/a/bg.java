package a.a;

import java.io.Serializable;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* compiled from: Traffic */
public class bg implements bw<bg, e>, Serializable, Cloneable {
    public static final Map<e, cg> c;
    /* access modifiers changed from: private */
    public static final cw d = new cw("Traffic");
    /* access modifiers changed from: private */
    public static final co e = new co("upload_traffic", (byte) 8, 1);
    /* access modifiers changed from: private */
    public static final co f = new co("download_traffic", (byte) 8, 2);
    private static final Map<Class<? extends cy>, cz> g = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public int f36a;
    public int b;
    private byte h = 0;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [a.a.bg$e, a.a.cg]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        g.put(da.class, new b());
        g.put(db.class, new d());
        EnumMap enumMap = new EnumMap(e.class);
        enumMap.put((Object) e.UPLOAD_TRAFFIC, (Object) new cg("upload_traffic", (byte) 1, new ch((byte) 8)));
        enumMap.put((Object) e.DOWNLOAD_TRAFFIC, (Object) new cg("download_traffic", (byte) 1, new ch((byte) 8)));
        c = Collections.unmodifiableMap(enumMap);
        cg.a(bg.class, c);
    }

    /* compiled from: Traffic */
    public enum e implements cb {
        UPLOAD_TRAFFIC(1, "upload_traffic"),
        DOWNLOAD_TRAFFIC(2, "download_traffic");
        
        private static final Map<String, e> c = new HashMap();
        private final short d;
        private final String e;

        static {
            Iterator it = EnumSet.allOf(e.class).iterator();
            while (it.hasNext()) {
                e eVar = (e) it.next();
                c.put(eVar.b(), eVar);
            }
        }

        private e(short s, String str) {
            this.d = s;
            this.e = str;
        }

        public short a() {
            return this.d;
        }

        public String b() {
            return this.e;
        }
    }

    public bg a(int i) {
        this.f36a = i;
        a(true);
        return this;
    }

    public boolean a() {
        return bu.a(this.h, 0);
    }

    public void a(boolean z) {
        this.h = bu.a(this.h, 0, z);
    }

    public bg b(int i) {
        this.b = i;
        b(true);
        return this;
    }

    public boolean b() {
        return bu.a(this.h, 1);
    }

    public void b(boolean z) {
        this.h = bu.a(this.h, 1, z);
    }

    public void a(cr crVar) throws ca {
        g.get(crVar.y()).b().b(crVar, this);
    }

    public void b(cr crVar) throws ca {
        g.get(crVar.y()).b().a(crVar, this);
    }

    public String toString() {
        return "Traffic(" + "upload_traffic:" + this.f36a + ", " + "download_traffic:" + this.b + ")";
    }

    public void c() throws ca {
    }

    /* compiled from: Traffic */
    private static class b implements cz {
        private b() {
        }

        /* renamed from: a */
        public a b() {
            return new a();
        }
    }

    /* compiled from: Traffic */
    private static class a extends da<bg> {
        private a() {
        }

        /* renamed from: a */
        public void b(cr crVar, bg bgVar) throws ca {
            crVar.f();
            while (true) {
                co h = crVar.h();
                if (h.b == 0) {
                    crVar.g();
                    if (!bgVar.a()) {
                        throw new cs("Required field 'upload_traffic' was not found in serialized data! Struct: " + toString());
                    } else if (!bgVar.b()) {
                        throw new cs("Required field 'download_traffic' was not found in serialized data! Struct: " + toString());
                    } else {
                        bgVar.c();
                        return;
                    }
                } else {
                    switch (h.c) {
                        case 1:
                            if (h.b != 8) {
                                cu.a(crVar, h.b);
                                break;
                            } else {
                                bgVar.f36a = crVar.s();
                                bgVar.a(true);
                                break;
                            }
                        case 2:
                            if (h.b != 8) {
                                cu.a(crVar, h.b);
                                break;
                            } else {
                                bgVar.b = crVar.s();
                                bgVar.b(true);
                                break;
                            }
                        default:
                            cu.a(crVar, h.b);
                            break;
                    }
                    crVar.i();
                }
            }
        }

        /* renamed from: b */
        public void a(cr crVar, bg bgVar) throws ca {
            bgVar.c();
            crVar.a(bg.d);
            crVar.a(bg.e);
            crVar.a(bgVar.f36a);
            crVar.b();
            crVar.a(bg.f);
            crVar.a(bgVar.b);
            crVar.b();
            crVar.c();
            crVar.a();
        }
    }

    /* compiled from: Traffic */
    private static class d implements cz {
        private d() {
        }

        /* renamed from: a */
        public c b() {
            return new c();
        }
    }

    /* compiled from: Traffic */
    private static class c extends db<bg> {
        private c() {
        }

        public void a(cr crVar, bg bgVar) throws ca {
            cx cxVar = (cx) crVar;
            cxVar.a(bgVar.f36a);
            cxVar.a(bgVar.b);
        }

        public void b(cr crVar, bg bgVar) throws ca {
            cx cxVar = (cx) crVar;
            bgVar.f36a = cxVar.s();
            bgVar.a(true);
            bgVar.b = cxVar.s();
            bgVar.b(true);
        }
    }
}
