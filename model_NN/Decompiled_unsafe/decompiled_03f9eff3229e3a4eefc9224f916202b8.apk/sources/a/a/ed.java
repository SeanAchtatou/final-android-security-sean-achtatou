package a.a;

import com.umeng.analytics.m;
import java.io.File;
import java.io.FileInputStream;

/* compiled from: Sender */
class ed implements m.b {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ec f94a;

    ed(ec ecVar) {
        this.f94a = ecVar;
    }

    public void a(File file) {
    }

    public boolean b(File file) {
        FileInputStream fileInputStream;
        int a2;
        try {
            fileInputStream = new FileInputStream(file);
            try {
                byte[] b = bt.b(fileInputStream);
                try {
                    bt.c(fileInputStream);
                    byte[] a3 = this.f94a.f.a(b);
                    if (a3 == null) {
                        a2 = 1;
                    } else {
                        a2 = this.f94a.a(a3);
                    }
                    if (a2 == 2 && this.f94a.e.h()) {
                        this.f94a.e.g();
                    }
                    if (!this.f94a.i && a2 == 1) {
                        return false;
                    }
                    return true;
                } catch (Exception e) {
                    return false;
                }
            } catch (Throwable th) {
                th = th;
            }
        } catch (Throwable th2) {
            th = th2;
            fileInputStream = null;
            bt.c(fileInputStream);
            throw th;
        }
    }

    public void c(File file) {
        this.f94a.e.f();
    }
}
