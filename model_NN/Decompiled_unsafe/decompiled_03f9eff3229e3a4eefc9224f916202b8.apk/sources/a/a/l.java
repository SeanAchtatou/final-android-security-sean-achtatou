package a.a;

import com.tencent.stat.DeviceInfo;
import java.io.Serializable;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* compiled from: ActivateMsg */
public class l implements bw<l, e>, Serializable, Cloneable {
    public static final Map<e, cg> b;
    /* access modifiers changed from: private */
    public static final cw c = new cw("ActivateMsg");
    /* access modifiers changed from: private */
    public static final co d = new co(DeviceInfo.TAG_TIMESTAMPS, (byte) 10, 1);
    private static final Map<Class<? extends cy>, cz> e = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public long f98a;
    private byte f;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [a.a.l$e, a.a.cg]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        e.put(da.class, new b());
        e.put(db.class, new d());
        EnumMap enumMap = new EnumMap(e.class);
        enumMap.put((Object) e.TS, (Object) new cg(DeviceInfo.TAG_TIMESTAMPS, (byte) 1, new ch((byte) 10)));
        b = Collections.unmodifiableMap(enumMap);
        cg.a(l.class, b);
    }

    /* compiled from: ActivateMsg */
    public enum e implements cb {
        TS(1, DeviceInfo.TAG_TIMESTAMPS);
        
        private static final Map<String, e> b = new HashMap();
        private final short c;
        private final String d;

        static {
            Iterator it = EnumSet.allOf(e.class).iterator();
            while (it.hasNext()) {
                e eVar = (e) it.next();
                b.put(eVar.b(), eVar);
            }
        }

        private e(short s, String str) {
            this.c = s;
            this.d = str;
        }

        public short a() {
            return this.c;
        }

        public String b() {
            return this.d;
        }
    }

    public l() {
        this.f = 0;
    }

    public l(long j) {
        this();
        this.f98a = j;
        a(true);
    }

    public boolean a() {
        return bu.a(this.f, 0);
    }

    public void a(boolean z) {
        this.f = bu.a(this.f, 0, z);
    }

    public void a(cr crVar) throws ca {
        e.get(crVar.y()).b().b(crVar, this);
    }

    public void b(cr crVar) throws ca {
        e.get(crVar.y()).b().a(crVar, this);
    }

    public String toString() {
        return "ActivateMsg(" + "ts:" + this.f98a + ")";
    }

    public void b() throws ca {
    }

    /* compiled from: ActivateMsg */
    private static class b implements cz {
        private b() {
        }

        /* renamed from: a */
        public a b() {
            return new a();
        }
    }

    /* compiled from: ActivateMsg */
    private static class a extends da<l> {
        private a() {
        }

        /* renamed from: a */
        public void b(cr crVar, l lVar) throws ca {
            crVar.f();
            while (true) {
                co h = crVar.h();
                if (h.b == 0) {
                    crVar.g();
                    if (!lVar.a()) {
                        throw new cs("Required field 'ts' was not found in serialized data! Struct: " + toString());
                    }
                    lVar.b();
                    return;
                }
                switch (h.c) {
                    case 1:
                        if (h.b != 10) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            lVar.f98a = crVar.t();
                            lVar.a(true);
                            break;
                        }
                    default:
                        cu.a(crVar, h.b);
                        break;
                }
                crVar.i();
            }
        }

        /* renamed from: b */
        public void a(cr crVar, l lVar) throws ca {
            lVar.b();
            crVar.a(l.c);
            crVar.a(l.d);
            crVar.a(lVar.f98a);
            crVar.b();
            crVar.c();
            crVar.a();
        }
    }

    /* compiled from: ActivateMsg */
    private static class d implements cz {
        private d() {
        }

        /* renamed from: a */
        public c b() {
            return new c();
        }
    }

    /* compiled from: ActivateMsg */
    private static class c extends db<l> {
        private c() {
        }

        public void a(cr crVar, l lVar) throws ca {
            ((cx) crVar).a(lVar.f98a);
        }

        public void b(cr crVar, l lVar) throws ca {
            lVar.f98a = ((cx) crVar).t();
            lVar.a(true);
        }
    }
}
