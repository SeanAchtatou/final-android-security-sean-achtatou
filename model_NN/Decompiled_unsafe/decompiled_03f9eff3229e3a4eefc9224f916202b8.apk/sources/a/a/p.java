package a.a;

import java.io.Serializable;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* compiled from: AppInfo */
public class p implements bw<p, e>, Serializable, Cloneable {
    public static final Map<e, cg> k;
    /* access modifiers changed from: private */
    public static final cw l = new cw("AppInfo");
    /* access modifiers changed from: private */
    public static final co m = new co("key", (byte) 11, 1);
    /* access modifiers changed from: private */
    public static final co n = new co("version", (byte) 11, 2);
    /* access modifiers changed from: private */
    public static final co o = new co("version_index", (byte) 8, 3);
    /* access modifiers changed from: private */
    public static final co p = new co("package_name", (byte) 11, 4);
    /* access modifiers changed from: private */
    public static final co q = new co("sdk_type", (byte) 8, 5);
    /* access modifiers changed from: private */
    public static final co r = new co("sdk_version", (byte) 11, 6);
    /* access modifiers changed from: private */
    public static final co s = new co(LogBuilder.KEY_CHANNEL, (byte) 11, 7);
    /* access modifiers changed from: private */
    public static final co t = new co("wrapper_type", (byte) 11, 8);
    /* access modifiers changed from: private */
    public static final co u = new co("wrapper_version", (byte) 11, 9);
    /* access modifiers changed from: private */
    public static final co v = new co("vertical_type", (byte) 8, 10);
    private static final Map<Class<? extends cy>, cz> w = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public String f102a;
    public String b;
    public int c;
    public String d;
    public bd e;
    public String f;
    public String g;
    public String h;
    public String i;
    public int j;
    private byte x = 0;
    private e[] y = {e.VERSION, e.VERSION_INDEX, e.PACKAGE_NAME, e.WRAPPER_TYPE, e.WRAPPER_VERSION, e.VERTICAL_TYPE};

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [a.a.p$e, a.a.cg]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        w.put(da.class, new b());
        w.put(db.class, new d());
        EnumMap enumMap = new EnumMap(e.class);
        enumMap.put((Object) e.KEY, (Object) new cg("key", (byte) 1, new ch((byte) 11)));
        enumMap.put((Object) e.VERSION, (Object) new cg("version", (byte) 2, new ch((byte) 11)));
        enumMap.put((Object) e.VERSION_INDEX, (Object) new cg("version_index", (byte) 2, new ch((byte) 8)));
        enumMap.put((Object) e.PACKAGE_NAME, (Object) new cg("package_name", (byte) 2, new ch((byte) 11)));
        enumMap.put((Object) e.SDK_TYPE, (Object) new cg("sdk_type", (byte) 1, new cf((byte) 16, bd.class)));
        enumMap.put((Object) e.SDK_VERSION, (Object) new cg("sdk_version", (byte) 1, new ch((byte) 11)));
        enumMap.put((Object) e.CHANNEL, (Object) new cg(LogBuilder.KEY_CHANNEL, (byte) 1, new ch((byte) 11)));
        enumMap.put((Object) e.WRAPPER_TYPE, (Object) new cg("wrapper_type", (byte) 2, new ch((byte) 11)));
        enumMap.put((Object) e.WRAPPER_VERSION, (Object) new cg("wrapper_version", (byte) 2, new ch((byte) 11)));
        enumMap.put((Object) e.VERTICAL_TYPE, (Object) new cg("vertical_type", (byte) 2, new ch((byte) 8)));
        k = Collections.unmodifiableMap(enumMap);
        cg.a(p.class, k);
    }

    /* compiled from: AppInfo */
    public enum e implements cb {
        KEY(1, "key"),
        VERSION(2, "version"),
        VERSION_INDEX(3, "version_index"),
        PACKAGE_NAME(4, "package_name"),
        SDK_TYPE(5, "sdk_type"),
        SDK_VERSION(6, "sdk_version"),
        CHANNEL(7, LogBuilder.KEY_CHANNEL),
        WRAPPER_TYPE(8, "wrapper_type"),
        WRAPPER_VERSION(9, "wrapper_version"),
        VERTICAL_TYPE(10, "vertical_type");
        
        private static final Map<String, e> k = new HashMap();
        private final short l;
        private final String m;

        static {
            Iterator it = EnumSet.allOf(e.class).iterator();
            while (it.hasNext()) {
                e eVar = (e) it.next();
                k.put(eVar.b(), eVar);
            }
        }

        private e(short s, String str) {
            this.l = s;
            this.m = str;
        }

        public short a() {
            return this.l;
        }

        public String b() {
            return this.m;
        }
    }

    public p a(String str) {
        this.f102a = str;
        return this;
    }

    public void a(boolean z) {
        if (!z) {
            this.f102a = null;
        }
    }

    public p b(String str) {
        this.b = str;
        return this;
    }

    public boolean a() {
        return this.b != null;
    }

    public void b(boolean z) {
        if (!z) {
            this.b = null;
        }
    }

    public p a(int i2) {
        this.c = i2;
        c(true);
        return this;
    }

    public boolean b() {
        return bu.a(this.x, 0);
    }

    public void c(boolean z) {
        this.x = bu.a(this.x, 0, z);
    }

    public p c(String str) {
        this.d = str;
        return this;
    }

    public boolean c() {
        return this.d != null;
    }

    public void d(boolean z) {
        if (!z) {
            this.d = null;
        }
    }

    public p a(bd bdVar) {
        this.e = bdVar;
        return this;
    }

    public void e(boolean z) {
        if (!z) {
            this.e = null;
        }
    }

    public p d(String str) {
        this.f = str;
        return this;
    }

    public void f(boolean z) {
        if (!z) {
            this.f = null;
        }
    }

    public p e(String str) {
        this.g = str;
        return this;
    }

    public void g(boolean z) {
        if (!z) {
            this.g = null;
        }
    }

    public p f(String str) {
        this.h = str;
        return this;
    }

    public boolean d() {
        return this.h != null;
    }

    public void h(boolean z) {
        if (!z) {
            this.h = null;
        }
    }

    public p g(String str) {
        this.i = str;
        return this;
    }

    public boolean e() {
        return this.i != null;
    }

    public void i(boolean z) {
        if (!z) {
            this.i = null;
        }
    }

    public p b(int i2) {
        this.j = i2;
        j(true);
        return this;
    }

    public boolean f() {
        return bu.a(this.x, 1);
    }

    public void j(boolean z) {
        this.x = bu.a(this.x, 1, z);
    }

    public void a(cr crVar) throws ca {
        w.get(crVar.y()).b().b(crVar, this);
    }

    public void b(cr crVar) throws ca {
        w.get(crVar.y()).b().a(crVar, this);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("AppInfo(");
        sb.append("key:");
        if (this.f102a == null) {
            sb.append("null");
        } else {
            sb.append(this.f102a);
        }
        if (a()) {
            sb.append(", ");
            sb.append("version:");
            if (this.b == null) {
                sb.append("null");
            } else {
                sb.append(this.b);
            }
        }
        if (b()) {
            sb.append(", ");
            sb.append("version_index:");
            sb.append(this.c);
        }
        if (c()) {
            sb.append(", ");
            sb.append("package_name:");
            if (this.d == null) {
                sb.append("null");
            } else {
                sb.append(this.d);
            }
        }
        sb.append(", ");
        sb.append("sdk_type:");
        if (this.e == null) {
            sb.append("null");
        } else {
            sb.append(this.e);
        }
        sb.append(", ");
        sb.append("sdk_version:");
        if (this.f == null) {
            sb.append("null");
        } else {
            sb.append(this.f);
        }
        sb.append(", ");
        sb.append("channel:");
        if (this.g == null) {
            sb.append("null");
        } else {
            sb.append(this.g);
        }
        if (d()) {
            sb.append(", ");
            sb.append("wrapper_type:");
            if (this.h == null) {
                sb.append("null");
            } else {
                sb.append(this.h);
            }
        }
        if (e()) {
            sb.append(", ");
            sb.append("wrapper_version:");
            if (this.i == null) {
                sb.append("null");
            } else {
                sb.append(this.i);
            }
        }
        if (f()) {
            sb.append(", ");
            sb.append("vertical_type:");
            sb.append(this.j);
        }
        sb.append(")");
        return sb.toString();
    }

    public void g() throws ca {
        if (this.f102a == null) {
            throw new cs("Required field 'key' was not present! Struct: " + toString());
        } else if (this.e == null) {
            throw new cs("Required field 'sdk_type' was not present! Struct: " + toString());
        } else if (this.f == null) {
            throw new cs("Required field 'sdk_version' was not present! Struct: " + toString());
        } else if (this.g == null) {
            throw new cs("Required field 'channel' was not present! Struct: " + toString());
        }
    }

    /* compiled from: AppInfo */
    private static class b implements cz {
        private b() {
        }

        /* renamed from: a */
        public a b() {
            return new a();
        }
    }

    /* compiled from: AppInfo */
    private static class a extends da<p> {
        private a() {
        }

        /* renamed from: a */
        public void b(cr crVar, p pVar) throws ca {
            crVar.f();
            while (true) {
                co h = crVar.h();
                if (h.b == 0) {
                    crVar.g();
                    pVar.g();
                    return;
                }
                switch (h.c) {
                    case 1:
                        if (h.b != 11) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            pVar.f102a = crVar.v();
                            pVar.a(true);
                            break;
                        }
                    case 2:
                        if (h.b != 11) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            pVar.b = crVar.v();
                            pVar.b(true);
                            break;
                        }
                    case 3:
                        if (h.b != 8) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            pVar.c = crVar.s();
                            pVar.c(true);
                            break;
                        }
                    case 4:
                        if (h.b != 11) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            pVar.d = crVar.v();
                            pVar.d(true);
                            break;
                        }
                    case 5:
                        if (h.b != 8) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            pVar.e = bd.a(crVar.s());
                            pVar.e(true);
                            break;
                        }
                    case 6:
                        if (h.b != 11) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            pVar.f = crVar.v();
                            pVar.f(true);
                            break;
                        }
                    case 7:
                        if (h.b != 11) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            pVar.g = crVar.v();
                            pVar.g(true);
                            break;
                        }
                    case 8:
                        if (h.b != 11) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            pVar.h = crVar.v();
                            pVar.h(true);
                            break;
                        }
                    case 9:
                        if (h.b != 11) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            pVar.i = crVar.v();
                            pVar.i(true);
                            break;
                        }
                    case 10:
                        if (h.b != 8) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            pVar.j = crVar.s();
                            pVar.j(true);
                            break;
                        }
                    default:
                        cu.a(crVar, h.b);
                        break;
                }
                crVar.i();
            }
        }

        /* renamed from: b */
        public void a(cr crVar, p pVar) throws ca {
            pVar.g();
            crVar.a(p.l);
            if (pVar.f102a != null) {
                crVar.a(p.m);
                crVar.a(pVar.f102a);
                crVar.b();
            }
            if (pVar.b != null && pVar.a()) {
                crVar.a(p.n);
                crVar.a(pVar.b);
                crVar.b();
            }
            if (pVar.b()) {
                crVar.a(p.o);
                crVar.a(pVar.c);
                crVar.b();
            }
            if (pVar.d != null && pVar.c()) {
                crVar.a(p.p);
                crVar.a(pVar.d);
                crVar.b();
            }
            if (pVar.e != null) {
                crVar.a(p.q);
                crVar.a(pVar.e.a());
                crVar.b();
            }
            if (pVar.f != null) {
                crVar.a(p.r);
                crVar.a(pVar.f);
                crVar.b();
            }
            if (pVar.g != null) {
                crVar.a(p.s);
                crVar.a(pVar.g);
                crVar.b();
            }
            if (pVar.h != null && pVar.d()) {
                crVar.a(p.t);
                crVar.a(pVar.h);
                crVar.b();
            }
            if (pVar.i != null && pVar.e()) {
                crVar.a(p.u);
                crVar.a(pVar.i);
                crVar.b();
            }
            if (pVar.f()) {
                crVar.a(p.v);
                crVar.a(pVar.j);
                crVar.b();
            }
            crVar.c();
            crVar.a();
        }
    }

    /* compiled from: AppInfo */
    private static class d implements cz {
        private d() {
        }

        /* renamed from: a */
        public c b() {
            return new c();
        }
    }

    /* compiled from: AppInfo */
    private static class c extends db<p> {
        private c() {
        }

        public void a(cr crVar, p pVar) throws ca {
            cx cxVar = (cx) crVar;
            cxVar.a(pVar.f102a);
            cxVar.a(pVar.e.a());
            cxVar.a(pVar.f);
            cxVar.a(pVar.g);
            BitSet bitSet = new BitSet();
            if (pVar.a()) {
                bitSet.set(0);
            }
            if (pVar.b()) {
                bitSet.set(1);
            }
            if (pVar.c()) {
                bitSet.set(2);
            }
            if (pVar.d()) {
                bitSet.set(3);
            }
            if (pVar.e()) {
                bitSet.set(4);
            }
            if (pVar.f()) {
                bitSet.set(5);
            }
            cxVar.a(bitSet, 6);
            if (pVar.a()) {
                cxVar.a(pVar.b);
            }
            if (pVar.b()) {
                cxVar.a(pVar.c);
            }
            if (pVar.c()) {
                cxVar.a(pVar.d);
            }
            if (pVar.d()) {
                cxVar.a(pVar.h);
            }
            if (pVar.e()) {
                cxVar.a(pVar.i);
            }
            if (pVar.f()) {
                cxVar.a(pVar.j);
            }
        }

        public void b(cr crVar, p pVar) throws ca {
            cx cxVar = (cx) crVar;
            pVar.f102a = cxVar.v();
            pVar.a(true);
            pVar.e = bd.a(cxVar.s());
            pVar.e(true);
            pVar.f = cxVar.v();
            pVar.f(true);
            pVar.g = cxVar.v();
            pVar.g(true);
            BitSet b = cxVar.b(6);
            if (b.get(0)) {
                pVar.b = cxVar.v();
                pVar.b(true);
            }
            if (b.get(1)) {
                pVar.c = cxVar.s();
                pVar.c(true);
            }
            if (b.get(2)) {
                pVar.d = cxVar.v();
                pVar.d(true);
            }
            if (b.get(3)) {
                pVar.h = cxVar.v();
                pVar.h(true);
            }
            if (b.get(4)) {
                pVar.i = cxVar.v();
                pVar.i(true);
            }
            if (b.get(5)) {
                pVar.j = cxVar.s();
                pVar.j(true);
            }
        }
    }
}
