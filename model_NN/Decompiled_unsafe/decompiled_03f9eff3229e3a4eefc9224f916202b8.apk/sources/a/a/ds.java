package a.a;

import com.umeng.analytics.a;
import java.lang.Thread;

/* compiled from: CrashHandler */
public class ds implements Thread.UncaughtExceptionHandler {

    /* renamed from: a  reason: collision with root package name */
    private Thread.UncaughtExceptionHandler f88a;
    private dz b;

    public ds() {
        if (Thread.getDefaultUncaughtExceptionHandler() != this) {
            this.f88a = Thread.getDefaultUncaughtExceptionHandler();
            Thread.setDefaultUncaughtExceptionHandler(this);
        }
    }

    public void a(dz dzVar) {
        this.b = dzVar;
    }

    public void uncaughtException(Thread thread, Throwable th) {
        a(th);
        if (this.f88a != null && this.f88a != Thread.getDefaultUncaughtExceptionHandler()) {
            this.f88a.uncaughtException(thread, th);
        }
    }

    private void a(Throwable th) {
        if (a.k) {
            this.b.a(th);
        } else {
            this.b.a(null);
        }
    }
}
