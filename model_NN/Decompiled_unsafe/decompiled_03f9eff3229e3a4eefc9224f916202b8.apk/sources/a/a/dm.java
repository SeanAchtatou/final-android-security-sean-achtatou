package a.a;

import android.content.Context;
import android.text.TextUtils;
import cn.banshenggua.aichang.utils.Constants;
import com.umeng.analytics.a.d;
import com.umeng.analytics.c;
import com.umeng.analytics.e;
import com.umeng.analytics.k;
import com.umeng.analytics.m;

/* compiled from: CacheImpl */
public final class dm implements du, ea, d {

    /* renamed from: a  reason: collision with root package name */
    private dx f81a = null;
    /* access modifiers changed from: private */
    public m b = null;
    /* access modifiers changed from: private */
    public b c = null;
    /* access modifiers changed from: private */
    public j d = new j();
    private a e = null;
    private int f = 10;
    private int g;
    /* access modifiers changed from: private */
    public Context h;

    public dm(Context context) {
        this.h = context;
        this.f81a = new dx(context);
        this.c = new b(context);
        this.b = m.a(context);
        this.d.a(this.b.c());
        this.e = new a();
        this.g = this.b.d(-1);
    }

    public void a() {
        if (bm.g(this.h)) {
            d();
        } else if (bn.f43a) {
            bn.c("MobclickAgent", "network is unavailable");
        }
    }

    public void a(dv dvVar) {
        if (dvVar != null) {
            this.f81a.a(dvVar);
        }
        a(dvVar instanceof be);
    }

    public void b(dv dvVar) {
        this.f81a.a(dvVar);
    }

    public void b() {
        if (this.f81a.a() > 0) {
            try {
                byte[] b2 = b(a(new int[0]));
                if (b2 != null) {
                    this.b.a(b2);
                }
            } catch (Throwable th) {
                if (th instanceof OutOfMemoryError) {
                    this.b.f();
                }
                if (th != null) {
                    th.printStackTrace();
                }
            }
        }
    }

    private void a(boolean z) {
        boolean a2 = this.c.a();
        if (a2) {
            this.f81a.a(new l(this.c.i()));
        }
        if (b(z)) {
            d();
        } else if (a2 || c()) {
            b();
        }
    }

    private void d(int i) {
        a(a(i, (int) (System.currentTimeMillis() - this.c.j())));
        k.a(new dn(this), (long) i);
    }

    private void a(bi biVar) {
        br a2;
        if (biVar != null) {
            cm a3 = cm.a(this.h);
            a3.a();
            biVar.a(a3.b());
            byte[] b2 = b(biVar);
            if (b2 != null) {
                if (e()) {
                    a2 = br.b(this.h, com.umeng.analytics.a.a(this.h), b2);
                } else {
                    a2 = br.a(this.h, com.umeng.analytics.a.a(this.h), b2);
                }
                byte[] c2 = a2.c();
                m a4 = m.a(this.h);
                a4.f();
                a4.b(c2);
                a3.c();
            }
        }
    }

    /* access modifiers changed from: protected */
    public bi a(int... iArr) {
        bi biVar;
        boolean z;
        boolean z2 = false;
        try {
            if (TextUtils.isEmpty(com.umeng.analytics.a.a(this.h))) {
                bn.b("MobclickAgent", "Appkey is missing ,Please check AndroidManifest.xml");
                return null;
            }
            byte[] e2 = m.a(this.h).e();
            bi a2 = e2 == null ? null : a(e2);
            if (a2 == null && this.f81a.a() == 0) {
                return null;
            }
            if (a2 == null) {
                biVar = new bi();
            } else {
                biVar = a2;
            }
            this.f81a.a(biVar);
            if (bn.f43a && biVar.f()) {
                for (be d2 : biVar.e()) {
                    if (d2.d() > 0) {
                        z = true;
                    } else {
                        z = z2;
                    }
                    z2 = z;
                }
                if (!z2) {
                    bn.d("MobclickAgent", "missing Activities or PageViews");
                }
            }
            bi a3 = this.d.a(this.h, biVar);
            if (iArr == null || iArr.length != 2) {
                return a3;
            }
            t tVar = new t();
            tVar.a(new ap(iArr[0] / Constants.CLEARIMGED, (long) iArr[1]));
            a3.a(tVar);
            return a3;
        } catch (Exception e3) {
            bn.b("MobclickAgent", "Fail to construct message ...", e3);
            m.a(this.h).f();
            return null;
        }
    }

    private bi a(byte[] bArr) {
        if (bArr == null) {
            return null;
        }
        try {
            bi biVar = new bi();
            new bz().a(biVar, bArr);
            return biVar;
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    private byte[] b(bi biVar) {
        if (biVar == null) {
            return null;
        }
        try {
            byte[] a2 = new cc().a(biVar);
            if (bn.f43a) {
                bn.c("MobclickAgent", biVar.toString());
            }
            return a2;
        } catch (Exception e2) {
            bn.b("MobclickAgent", "Fail to serialize log ...", e2);
            return null;
        }
    }

    private boolean b(boolean z) {
        if (!bm.g(this.h)) {
            if (bn.f43a) {
                bn.c("MobclickAgent", "network is unavailable");
            }
            return false;
        } else if (this.c.a()) {
            return true;
        } else {
            if (!bn.f43a || !bm.q(this.h)) {
                return this.e.c().a(z);
            }
            return true;
        }
    }

    private boolean c() {
        return this.f81a.a() > this.f;
    }

    private void d() {
        try {
            if (this.b.g()) {
                ec ecVar = new ec(this.h, this.c);
                ecVar.a(this);
                if (this.d.b()) {
                    ecVar.b(true);
                }
                ecVar.a();
                return;
            }
            bi a2 = a(new int[0]);
            if (a2 == null) {
                bn.a("MobclickAgent", "No data to report");
                return;
            }
            ec ecVar2 = new ec(this.h, this.c);
            ecVar2.a(this);
            if (this.d.b()) {
                ecVar2.b(true);
            }
            ecVar2.a(a2);
            ecVar2.a(e());
            ecVar2.a();
        } catch (Throwable th) {
            if (th instanceof OutOfMemoryError) {
            }
            if (th != null) {
                th.printStackTrace();
            }
        }
    }

    private boolean e() {
        switch (this.g) {
            case -1:
                return com.umeng.analytics.a.f;
            case 0:
            default:
                return false;
            case 1:
                return true;
        }
    }

    /* access modifiers changed from: private */
    public void e(int i) {
        d(i);
    }

    public void a(int i, long j) {
        this.e.a(i, (int) j);
    }

    public void a(int i) {
        if (i >= 0 && i <= 3) {
            this.d.a(i);
            this.e.b(i);
        }
    }

    public void b(int i) {
        if (i > 0) {
            this.e.a(i);
        }
    }

    public void c(int i) {
        this.g = i;
    }

    /* compiled from: CacheImpl */
    public class a {
        private final long b = 1296000000;
        private final int c = 1800000;
        private final int d = 10000;
        private c.f e;
        private int f;
        private int g;
        private int h;
        private int i;
        private boolean j = false;

        public a() {
            this.f = dm.this.b.c();
            int d2 = dm.this.b.d();
            if (d2 > 0) {
                this.g = c(d2);
            } else if (com.umeng.analytics.a.g > 0) {
                this.g = c(com.umeng.analytics.a.g);
            } else {
                this.g = 10000;
            }
            int[] b2 = dm.this.b.b();
            this.h = b2[0];
            this.i = b2[1];
        }

        /* access modifiers changed from: protected */
        public void a() {
            c.f aVar;
            boolean z = true;
            if (this.f > 0) {
                if (!(this.e instanceof c.a) || !this.e.a()) {
                    z = false;
                }
                if (z) {
                    aVar = this.e;
                } else {
                    aVar = new c.a(dm.this.c, dm.this.d);
                }
                this.e = aVar;
            } else {
                if (!(this.e instanceof c.b) || !this.e.a()) {
                    z = false;
                }
                if (!z) {
                    if (b()) {
                        int a2 = e.a(this.g, br.a(dm.this.h));
                        this.e = new c.b(a2);
                        dm.this.e(a2);
                    } else {
                        this.e = b(this.h, this.i);
                    }
                }
            }
            this.j = false;
        }

        /* access modifiers changed from: protected */
        public boolean b() {
            if (!dm.this.b.g() && !dm.this.c.a() && System.currentTimeMillis() - dm.this.c.j() > 1296000000) {
                return true;
            }
            return false;
        }

        public c.f c() {
            a();
            return this.e;
        }

        private c.f b(int i2, int i3) {
            switch (i2) {
                case 0:
                    return new c.f();
                case 1:
                    return new c.C0045c();
                case 2:
                case 3:
                default:
                    return new c.C0045c();
                case 4:
                    return new c.e(dm.this.c);
                case 5:
                    return new c.g(dm.this.h);
                case 6:
                    return new c.d(dm.this.c, (long) i3);
            }
        }

        private int c(int i2) {
            if (i2 > 1800000) {
                return 1800000;
            }
            return i2;
        }

        /* access modifiers changed from: protected */
        public void d() {
            this.j = true;
        }

        public void a(int i2) {
            this.g = c(i2);
            d();
        }

        public void b(int i2) {
            this.f = i2;
            d();
        }

        public void a(int i2, int i3) {
            this.h = i2;
            this.i = i3;
            d();
        }
    }
}
