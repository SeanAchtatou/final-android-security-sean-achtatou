package a.a;

import com.tencent.stat.DeviceInfo;
import java.io.Serializable;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* compiled from: ImprintValue */
public class al implements bw<al, e>, Serializable, Cloneable {
    public static final Map<e, cg> d;
    /* access modifiers changed from: private */
    public static final cw e = new cw("ImprintValue");
    /* access modifiers changed from: private */
    public static final co f = new co("value", (byte) 11, 1);
    /* access modifiers changed from: private */
    public static final co g = new co(DeviceInfo.TAG_TIMESTAMPS, (byte) 10, 2);
    /* access modifiers changed from: private */
    public static final co h = new co("guid", (byte) 11, 3);
    private static final Map<Class<? extends cy>, cz> i = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public String f13a;
    public long b;
    public String c;
    private byte j = 0;
    private e[] k = {e.VALUE};

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [a.a.al$e, a.a.cg]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        i.put(da.class, new b());
        i.put(db.class, new d());
        EnumMap enumMap = new EnumMap(e.class);
        enumMap.put((Object) e.VALUE, (Object) new cg("value", (byte) 2, new ch((byte) 11)));
        enumMap.put((Object) e.TS, (Object) new cg(DeviceInfo.TAG_TIMESTAMPS, (byte) 1, new ch((byte) 10)));
        enumMap.put((Object) e.GUID, (Object) new cg("guid", (byte) 1, new ch((byte) 11)));
        d = Collections.unmodifiableMap(enumMap);
        cg.a(al.class, d);
    }

    /* compiled from: ImprintValue */
    public enum e implements cb {
        VALUE(1, "value"),
        TS(2, DeviceInfo.TAG_TIMESTAMPS),
        GUID(3, "guid");
        
        private static final Map<String, e> d = new HashMap();
        private final short e;
        private final String f;

        static {
            Iterator it = EnumSet.allOf(e.class).iterator();
            while (it.hasNext()) {
                e eVar = (e) it.next();
                d.put(eVar.b(), eVar);
            }
        }

        private e(short s, String str) {
            this.e = s;
            this.f = str;
        }

        public short a() {
            return this.e;
        }

        public String b() {
            return this.f;
        }
    }

    public String a() {
        return this.f13a;
    }

    public boolean b() {
        return this.f13a != null;
    }

    public void a(boolean z) {
        if (!z) {
            this.f13a = null;
        }
    }

    public long c() {
        return this.b;
    }

    public boolean d() {
        return bu.a(this.j, 0);
    }

    public void b(boolean z) {
        this.j = bu.a(this.j, 0, z);
    }

    public String e() {
        return this.c;
    }

    public void c(boolean z) {
        if (!z) {
            this.c = null;
        }
    }

    public void a(cr crVar) throws ca {
        i.get(crVar.y()).b().b(crVar, this);
    }

    public void b(cr crVar) throws ca {
        i.get(crVar.y()).b().a(crVar, this);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("ImprintValue(");
        boolean z = true;
        if (b()) {
            sb.append("value:");
            if (this.f13a == null) {
                sb.append("null");
            } else {
                sb.append(this.f13a);
            }
            z = false;
        }
        if (!z) {
            sb.append(", ");
        }
        sb.append("ts:");
        sb.append(this.b);
        sb.append(", ");
        sb.append("guid:");
        if (this.c == null) {
            sb.append("null");
        } else {
            sb.append(this.c);
        }
        sb.append(")");
        return sb.toString();
    }

    public void f() throws ca {
        if (this.c == null) {
            throw new cs("Required field 'guid' was not present! Struct: " + toString());
        }
    }

    /* compiled from: ImprintValue */
    private static class b implements cz {
        private b() {
        }

        /* renamed from: a */
        public a b() {
            return new a();
        }
    }

    /* compiled from: ImprintValue */
    private static class a extends da<al> {
        private a() {
        }

        /* renamed from: a */
        public void b(cr crVar, al alVar) throws ca {
            crVar.f();
            while (true) {
                co h = crVar.h();
                if (h.b == 0) {
                    crVar.g();
                    if (!alVar.d()) {
                        throw new cs("Required field 'ts' was not found in serialized data! Struct: " + toString());
                    }
                    alVar.f();
                    return;
                }
                switch (h.c) {
                    case 1:
                        if (h.b != 11) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            alVar.f13a = crVar.v();
                            alVar.a(true);
                            break;
                        }
                    case 2:
                        if (h.b != 10) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            alVar.b = crVar.t();
                            alVar.b(true);
                            break;
                        }
                    case 3:
                        if (h.b != 11) {
                            cu.a(crVar, h.b);
                            break;
                        } else {
                            alVar.c = crVar.v();
                            alVar.c(true);
                            break;
                        }
                    default:
                        cu.a(crVar, h.b);
                        break;
                }
                crVar.i();
            }
        }

        /* renamed from: b */
        public void a(cr crVar, al alVar) throws ca {
            alVar.f();
            crVar.a(al.e);
            if (alVar.f13a != null && alVar.b()) {
                crVar.a(al.f);
                crVar.a(alVar.f13a);
                crVar.b();
            }
            crVar.a(al.g);
            crVar.a(alVar.b);
            crVar.b();
            if (alVar.c != null) {
                crVar.a(al.h);
                crVar.a(alVar.c);
                crVar.b();
            }
            crVar.c();
            crVar.a();
        }
    }

    /* compiled from: ImprintValue */
    private static class d implements cz {
        private d() {
        }

        /* renamed from: a */
        public c b() {
            return new c();
        }
    }

    /* compiled from: ImprintValue */
    private static class c extends db<al> {
        private c() {
        }

        public void a(cr crVar, al alVar) throws ca {
            cx cxVar = (cx) crVar;
            cxVar.a(alVar.b);
            cxVar.a(alVar.c);
            BitSet bitSet = new BitSet();
            if (alVar.b()) {
                bitSet.set(0);
            }
            cxVar.a(bitSet, 1);
            if (alVar.b()) {
                cxVar.a(alVar.f13a);
            }
        }

        public void b(cr crVar, al alVar) throws ca {
            cx cxVar = (cx) crVar;
            alVar.b = cxVar.t();
            alVar.b(true);
            alVar.c = cxVar.v();
            alVar.c(true);
            if (cxVar.b(1).get(0)) {
                alVar.f13a = cxVar.v();
                alVar.a(true);
            }
        }
    }
}
