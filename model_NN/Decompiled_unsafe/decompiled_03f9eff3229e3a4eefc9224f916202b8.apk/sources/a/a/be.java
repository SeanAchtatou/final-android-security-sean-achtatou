package a.a;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* compiled from: Session */
public class be implements bw<be, e>, Serializable, Cloneable {
    public static final Map<e, cg> h;
    /* access modifiers changed from: private */
    public static final cw i = new cw("Session");
    /* access modifiers changed from: private */
    public static final co j = new co("id", (byte) 11, 1);
    /* access modifiers changed from: private */
    public static final co k = new co("start_time", (byte) 10, 2);
    /* access modifiers changed from: private */
    public static final co l = new co("end_time", (byte) 10, 3);
    /* access modifiers changed from: private */
    public static final co m = new co("duration", (byte) 10, 4);
    /* access modifiers changed from: private */
    public static final co n = new co("pages", (byte) 15, 5);
    /* access modifiers changed from: private */
    public static final co o = new co("locations", (byte) 15, 6);
    /* access modifiers changed from: private */
    public static final co p = new co("traffic", (byte) 12, 7);
    private static final Map<Class<? extends cy>, cz> q = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public String f34a;
    public long b;
    public long c;
    public long d;
    public List<av> e;
    public List<ar> f;
    public bg g;
    private byte r = 0;
    private e[] s = {e.PAGES, e.LOCATIONS, e.TRAFFIC};

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [a.a.be$e, a.a.cg]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        q.put(da.class, new b());
        q.put(db.class, new d());
        EnumMap enumMap = new EnumMap(e.class);
        enumMap.put((Object) e.ID, (Object) new cg("id", (byte) 1, new ch((byte) 11)));
        enumMap.put((Object) e.START_TIME, (Object) new cg("start_time", (byte) 1, new ch((byte) 10)));
        enumMap.put((Object) e.END_TIME, (Object) new cg("end_time", (byte) 1, new ch((byte) 10)));
        enumMap.put((Object) e.DURATION, (Object) new cg("duration", (byte) 1, new ch((byte) 10)));
        enumMap.put((Object) e.PAGES, (Object) new cg("pages", (byte) 2, new ci((byte) 15, new ck((byte) 12, av.class))));
        enumMap.put((Object) e.LOCATIONS, (Object) new cg("locations", (byte) 2, new ci((byte) 15, new ck((byte) 12, ar.class))));
        enumMap.put((Object) e.TRAFFIC, (Object) new cg("traffic", (byte) 2, new ck((byte) 12, bg.class)));
        h = Collections.unmodifiableMap(enumMap);
        cg.a(be.class, h);
    }

    /* compiled from: Session */
    public enum e implements cb {
        ID(1, "id"),
        START_TIME(2, "start_time"),
        END_TIME(3, "end_time"),
        DURATION(4, "duration"),
        PAGES(5, "pages"),
        LOCATIONS(6, "locations"),
        TRAFFIC(7, "traffic");
        
        private static final Map<String, e> h = new HashMap();
        private final short i;
        private final String j;

        static {
            Iterator it = EnumSet.allOf(e.class).iterator();
            while (it.hasNext()) {
                e eVar = (e) it.next();
                h.put(eVar.b(), eVar);
            }
        }

        private e(short s, String str) {
            this.i = s;
            this.j = str;
        }

        public short a() {
            return this.i;
        }

        public String b() {
            return this.j;
        }
    }

    public be a(String str) {
        this.f34a = str;
        return this;
    }

    public void a(boolean z) {
        if (!z) {
            this.f34a = null;
        }
    }

    public be a(long j2) {
        this.b = j2;
        b(true);
        return this;
    }

    public boolean a() {
        return bu.a(this.r, 0);
    }

    public void b(boolean z) {
        this.r = bu.a(this.r, 0, z);
    }

    public be b(long j2) {
        this.c = j2;
        c(true);
        return this;
    }

    public boolean b() {
        return bu.a(this.r, 1);
    }

    public void c(boolean z) {
        this.r = bu.a(this.r, 1, z);
    }

    public be c(long j2) {
        this.d = j2;
        d(true);
        return this;
    }

    public boolean c() {
        return bu.a(this.r, 2);
    }

    public void d(boolean z) {
        this.r = bu.a(this.r, 2, z);
    }

    public int d() {
        if (this.e == null) {
            return 0;
        }
        return this.e.size();
    }

    public be a(List<av> list) {
        this.e = list;
        return this;
    }

    public boolean e() {
        return this.e != null;
    }

    public void e(boolean z) {
        if (!z) {
            this.e = null;
        }
    }

    public void a(ar arVar) {
        if (this.f == null) {
            this.f = new ArrayList();
        }
        this.f.add(arVar);
    }

    public be b(List<ar> list) {
        this.f = list;
        return this;
    }

    public boolean f() {
        return this.f != null;
    }

    public void f(boolean z) {
        if (!z) {
            this.f = null;
        }
    }

    public be a(bg bgVar) {
        this.g = bgVar;
        return this;
    }

    public boolean g() {
        return this.g != null;
    }

    public void g(boolean z) {
        if (!z) {
            this.g = null;
        }
    }

    public void a(cr crVar) throws ca {
        q.get(crVar.y()).b().b(crVar, this);
    }

    public void b(cr crVar) throws ca {
        q.get(crVar.y()).b().a(crVar, this);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("Session(");
        sb.append("id:");
        if (this.f34a == null) {
            sb.append("null");
        } else {
            sb.append(this.f34a);
        }
        sb.append(", ");
        sb.append("start_time:");
        sb.append(this.b);
        sb.append(", ");
        sb.append("end_time:");
        sb.append(this.c);
        sb.append(", ");
        sb.append("duration:");
        sb.append(this.d);
        if (e()) {
            sb.append(", ");
            sb.append("pages:");
            if (this.e == null) {
                sb.append("null");
            } else {
                sb.append(this.e);
            }
        }
        if (f()) {
            sb.append(", ");
            sb.append("locations:");
            if (this.f == null) {
                sb.append("null");
            } else {
                sb.append(this.f);
            }
        }
        if (g()) {
            sb.append(", ");
            sb.append("traffic:");
            if (this.g == null) {
                sb.append("null");
            } else {
                sb.append(this.g);
            }
        }
        sb.append(")");
        return sb.toString();
    }

    public void h() throws ca {
        if (this.f34a == null) {
            throw new cs("Required field 'id' was not present! Struct: " + toString());
        } else if (this.g != null) {
            this.g.c();
        }
    }

    /* compiled from: Session */
    private static class b implements cz {
        private b() {
        }

        /* renamed from: a */
        public a b() {
            return new a();
        }
    }

    /* compiled from: Session */
    private static class a extends da<be> {
        private a() {
        }

        /* renamed from: a */
        public void b(cr crVar, be beVar) throws ca {
            crVar.f();
            while (true) {
                co h = crVar.h();
                if (h.b == 0) {
                    crVar.g();
                    if (!beVar.a()) {
                        throw new cs("Required field 'start_time' was not found in serialized data! Struct: " + toString());
                    } else if (!beVar.b()) {
                        throw new cs("Required field 'end_time' was not found in serialized data! Struct: " + toString());
                    } else if (!beVar.c()) {
                        throw new cs("Required field 'duration' was not found in serialized data! Struct: " + toString());
                    } else {
                        beVar.h();
                        return;
                    }
                } else {
                    switch (h.c) {
                        case 1:
                            if (h.b != 11) {
                                cu.a(crVar, h.b);
                                break;
                            } else {
                                beVar.f34a = crVar.v();
                                beVar.a(true);
                                break;
                            }
                        case 2:
                            if (h.b != 10) {
                                cu.a(crVar, h.b);
                                break;
                            } else {
                                beVar.b = crVar.t();
                                beVar.b(true);
                                break;
                            }
                        case 3:
                            if (h.b != 10) {
                                cu.a(crVar, h.b);
                                break;
                            } else {
                                beVar.c = crVar.t();
                                beVar.c(true);
                                break;
                            }
                        case 4:
                            if (h.b != 10) {
                                cu.a(crVar, h.b);
                                break;
                            } else {
                                beVar.d = crVar.t();
                                beVar.d(true);
                                break;
                            }
                        case 5:
                            if (h.b != 15) {
                                cu.a(crVar, h.b);
                                break;
                            } else {
                                cp l = crVar.l();
                                beVar.e = new ArrayList(l.b);
                                for (int i = 0; i < l.b; i++) {
                                    av avVar = new av();
                                    avVar.a(crVar);
                                    beVar.e.add(avVar);
                                }
                                crVar.m();
                                beVar.e(true);
                                break;
                            }
                        case 6:
                            if (h.b != 15) {
                                cu.a(crVar, h.b);
                                break;
                            } else {
                                cp l2 = crVar.l();
                                beVar.f = new ArrayList(l2.b);
                                for (int i2 = 0; i2 < l2.b; i2++) {
                                    ar arVar = new ar();
                                    arVar.a(crVar);
                                    beVar.f.add(arVar);
                                }
                                crVar.m();
                                beVar.f(true);
                                break;
                            }
                        case 7:
                            if (h.b != 12) {
                                cu.a(crVar, h.b);
                                break;
                            } else {
                                beVar.g = new bg();
                                beVar.g.a(crVar);
                                beVar.g(true);
                                break;
                            }
                        default:
                            cu.a(crVar, h.b);
                            break;
                    }
                    crVar.i();
                }
            }
        }

        /* renamed from: b */
        public void a(cr crVar, be beVar) throws ca {
            beVar.h();
            crVar.a(be.i);
            if (beVar.f34a != null) {
                crVar.a(be.j);
                crVar.a(beVar.f34a);
                crVar.b();
            }
            crVar.a(be.k);
            crVar.a(beVar.b);
            crVar.b();
            crVar.a(be.l);
            crVar.a(beVar.c);
            crVar.b();
            crVar.a(be.m);
            crVar.a(beVar.d);
            crVar.b();
            if (beVar.e != null && beVar.e()) {
                crVar.a(be.n);
                crVar.a(new cp((byte) 12, beVar.e.size()));
                for (av b : beVar.e) {
                    b.b(crVar);
                }
                crVar.e();
                crVar.b();
            }
            if (beVar.f != null && beVar.f()) {
                crVar.a(be.o);
                crVar.a(new cp((byte) 12, beVar.f.size()));
                for (ar b2 : beVar.f) {
                    b2.b(crVar);
                }
                crVar.e();
                crVar.b();
            }
            if (beVar.g != null && beVar.g()) {
                crVar.a(be.p);
                beVar.g.b(crVar);
                crVar.b();
            }
            crVar.c();
            crVar.a();
        }
    }

    /* compiled from: Session */
    private static class d implements cz {
        private d() {
        }

        /* renamed from: a */
        public c b() {
            return new c();
        }
    }

    /* compiled from: Session */
    private static class c extends db<be> {
        private c() {
        }

        public void a(cr crVar, be beVar) throws ca {
            cx cxVar = (cx) crVar;
            cxVar.a(beVar.f34a);
            cxVar.a(beVar.b);
            cxVar.a(beVar.c);
            cxVar.a(beVar.d);
            BitSet bitSet = new BitSet();
            if (beVar.e()) {
                bitSet.set(0);
            }
            if (beVar.f()) {
                bitSet.set(1);
            }
            if (beVar.g()) {
                bitSet.set(2);
            }
            cxVar.a(bitSet, 3);
            if (beVar.e()) {
                cxVar.a(beVar.e.size());
                for (av b : beVar.e) {
                    b.b(cxVar);
                }
            }
            if (beVar.f()) {
                cxVar.a(beVar.f.size());
                for (ar b2 : beVar.f) {
                    b2.b(cxVar);
                }
            }
            if (beVar.g()) {
                beVar.g.b(cxVar);
            }
        }

        public void b(cr crVar, be beVar) throws ca {
            cx cxVar = (cx) crVar;
            beVar.f34a = cxVar.v();
            beVar.a(true);
            beVar.b = cxVar.t();
            beVar.b(true);
            beVar.c = cxVar.t();
            beVar.c(true);
            beVar.d = cxVar.t();
            beVar.d(true);
            BitSet b = cxVar.b(3);
            if (b.get(0)) {
                cp cpVar = new cp((byte) 12, cxVar.s());
                beVar.e = new ArrayList(cpVar.b);
                for (int i = 0; i < cpVar.b; i++) {
                    av avVar = new av();
                    avVar.a(cxVar);
                    beVar.e.add(avVar);
                }
                beVar.e(true);
            }
            if (b.get(1)) {
                cp cpVar2 = new cp((byte) 12, cxVar.s());
                beVar.f = new ArrayList(cpVar2.b);
                for (int i2 = 0; i2 < cpVar2.b; i2++) {
                    ar arVar = new ar();
                    arVar.a(cxVar);
                    beVar.f.add(arVar);
                }
                beVar.f(true);
            }
            if (b.get(2)) {
                beVar.g = new bg();
                beVar.g.a(cxVar);
                beVar.g(true);
            }
        }
    }
}
