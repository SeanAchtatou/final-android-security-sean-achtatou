package a.a;

/* compiled from: ShortStack */
public class bv {

    /* renamed from: a  reason: collision with root package name */
    private short[] f48a;
    private int b = -1;

    public bv(int i) {
        this.f48a = new short[i];
    }

    public short a() {
        short[] sArr = this.f48a;
        int i = this.b;
        this.b = i - 1;
        return sArr[i];
    }

    public void a(short s) {
        if (this.f48a.length == this.b + 1) {
            c();
        }
        short[] sArr = this.f48a;
        int i = this.b + 1;
        this.b = i;
        sArr[i] = s;
    }

    private void c() {
        short[] sArr = new short[(this.f48a.length * 2)];
        System.arraycopy(this.f48a, 0, sArr, 0, this.f48a.length);
        this.f48a = sArr;
    }

    public void b() {
        this.b = -1;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("<ShortStack vector:[");
        for (int i = 0; i < this.f48a.length; i++) {
            if (i != 0) {
                sb.append(" ");
            }
            if (i == this.b) {
                sb.append(">>");
            }
            sb.append((int) this.f48a[i]);
            if (i == this.b) {
                sb.append("<<");
            }
        }
        sb.append("]>");
        return sb.toString();
    }
}
