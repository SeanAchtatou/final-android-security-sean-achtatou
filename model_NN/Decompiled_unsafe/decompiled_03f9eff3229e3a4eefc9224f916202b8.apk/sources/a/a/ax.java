package a.a;

import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* compiled from: PropertyValue */
public class ax extends cd<ax, a> {

    /* renamed from: a  reason: collision with root package name */
    public static final Map<a, cg> f25a;
    private static final cw d = new cw("PropertyValue");
    private static final co e = new co("string_value", (byte) 11, 1);
    private static final co f = new co("long_value", (byte) 10, 2);

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [a.a.ax$a, a.a.cg]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        EnumMap enumMap = new EnumMap(a.class);
        enumMap.put((Object) a.STRING_VALUE, (Object) new cg("string_value", (byte) 3, new ch((byte) 11)));
        enumMap.put((Object) a.LONG_VALUE, (Object) new cg("long_value", (byte) 3, new ch((byte) 10)));
        f25a = Collections.unmodifiableMap(enumMap);
        cg.a(ax.class, f25a);
    }

    /* compiled from: PropertyValue */
    public enum a implements cb {
        STRING_VALUE(1, "string_value"),
        LONG_VALUE(2, "long_value");
        
        private static final Map<String, a> c = new HashMap();
        private final short d;
        private final String e;

        static {
            Iterator it = EnumSet.allOf(a.class).iterator();
            while (it.hasNext()) {
                a aVar = (a) it.next();
                c.put(aVar.b(), aVar);
            }
        }

        public static a a(int i) {
            switch (i) {
                case 1:
                    return STRING_VALUE;
                case 2:
                    return LONG_VALUE;
                default:
                    return null;
            }
        }

        public static a b(int i) {
            a a2 = a(i);
            if (a2 != null) {
                return a2;
            }
            throw new IllegalArgumentException("Field " + i + " doesn't exist!");
        }

        private a(short s, String str) {
            this.d = s;
            this.e = str;
        }

        public short a() {
            return this.d;
        }

        public String b() {
            return this.e;
        }
    }

    /* access modifiers changed from: protected */
    public Object a(cr crVar, co coVar) throws ca {
        a a2 = a.a(coVar.c);
        if (a2 == null) {
            return null;
        }
        switch (ay.f27a[a2.ordinal()]) {
            case 1:
                if (coVar.b == e.b) {
                    return crVar.v();
                }
                cu.a(crVar, coVar.b);
                return null;
            case 2:
                if (coVar.b == f.b) {
                    return Long.valueOf(crVar.t());
                }
                cu.a(crVar, coVar.b);
                return null;
            default:
                throw new IllegalStateException("setField wasn't null, but didn't match any of the case statements!");
        }
    }

    /* access modifiers changed from: protected */
    public void c(cr crVar) throws ca {
        switch (ay.f27a[((a) this.c).ordinal()]) {
            case 1:
                crVar.a((String) this.b);
                return;
            case 2:
                crVar.a(((Long) this.b).longValue());
                return;
            default:
                throw new IllegalStateException("Cannot write union with unknown field " + this.c);
        }
    }

    /* access modifiers changed from: protected */
    public Object a(cr crVar, short s) throws ca {
        a a2 = a.a(s);
        if (a2 != null) {
            switch (ay.f27a[a2.ordinal()]) {
                case 1:
                    return crVar.v();
                case 2:
                    return Long.valueOf(crVar.t());
                default:
                    throw new IllegalStateException("setField wasn't null, but didn't match any of the case statements!");
            }
        } else {
            throw new cs("Couldn't find a field with field id " + ((int) s));
        }
    }

    /* access modifiers changed from: protected */
    public void d(cr crVar) throws ca {
        switch (ay.f27a[((a) this.c).ordinal()]) {
            case 1:
                crVar.a((String) this.b);
                return;
            case 2:
                crVar.a(((Long) this.b).longValue());
                return;
            default:
                throw new IllegalStateException("Cannot write union with unknown field " + this.c);
        }
    }

    /* access modifiers changed from: protected */
    public co a(a aVar) {
        switch (ay.f27a[aVar.ordinal()]) {
            case 1:
                return e;
            case 2:
                return f;
            default:
                throw new IllegalArgumentException("Unknown field id " + aVar);
        }
    }

    /* access modifiers changed from: protected */
    public cw a() {
        return d;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public a b(short s) {
        return a.b(s);
    }

    public void a(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.c = a.STRING_VALUE;
        this.b = str;
    }

    public void a(long j) {
        this.c = a.LONG_VALUE;
        this.b = Long.valueOf(j);
    }

    public boolean equals(Object obj) {
        if (obj instanceof ax) {
            return a((ax) obj);
        }
        return false;
    }

    public boolean a(ax axVar) {
        return axVar != null && b() == axVar.b() && c().equals(axVar.c());
    }

    public int hashCode() {
        return 0;
    }
}
