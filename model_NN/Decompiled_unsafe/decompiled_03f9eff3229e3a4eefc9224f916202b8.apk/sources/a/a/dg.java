package a.a;

import android.content.Context;
import android.telephony.TelephonyManager;

/* compiled from: ImeiTracker */
public class dg extends a {

    /* renamed from: a  reason: collision with root package name */
    private Context f76a;

    public dg(Context context) {
        super("imei");
        this.f76a = context;
    }

    public String f() {
        TelephonyManager telephonyManager = (TelephonyManager) this.f76a.getSystemService("phone");
        if (telephonyManager == null) {
        }
        try {
            if (bm.a(this.f76a, "android.permission.READ_PHONE_STATE")) {
                return telephonyManager.getDeviceId();
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }
}
