package a.a;

import android.content.Context;
import android.text.TextUtils;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/* compiled from: IdTracker */
public class cm {

    /* renamed from: a  reason: collision with root package name */
    public static cm f62a;
    private final String b = "umeng_it.cache";
    private File c;
    private ag d = null;
    private long e;
    private long f;
    private Set<a> g = new HashSet();
    private a h = null;

    cm(Context context) {
        this.c = new File(context.getFilesDir(), "umeng_it.cache");
        this.f = LogBuilder.MAX_INTERVAL;
        this.h = new a(context);
        this.h.b();
    }

    public static synchronized cm a(Context context) {
        cm cmVar;
        synchronized (cm.class) {
            if (f62a == null) {
                f62a = new cm(context);
                f62a.a(new dg(context));
                f62a.a(new di(context));
                f62a.a(new ai(context));
                f62a.a(new dl(context));
                f62a.a(new dk(context));
                f62a.a(new dj());
                f62a.d();
            }
            cmVar = f62a;
        }
        return cmVar;
    }

    public boolean a(a aVar) {
        if (this.h.a(aVar.b())) {
            return this.g.add(aVar);
        }
        return false;
    }

    public void a() {
        long currentTimeMillis = System.currentTimeMillis();
        if (currentTimeMillis - this.e >= this.f) {
            boolean z = false;
            for (a next : this.g) {
                if (next.c()) {
                    if (next.a()) {
                        z = true;
                        if (!next.c()) {
                            this.h.b(next.b());
                        }
                    }
                    z = z;
                }
            }
            if (z) {
                f();
                this.h.a();
                e();
            }
            this.e = currentTimeMillis;
        }
    }

    public ag b() {
        return this.d;
    }

    private void f() {
        ag agVar = new ag();
        HashMap hashMap = new HashMap();
        ArrayList arrayList = new ArrayList();
        for (a next : this.g) {
            if (next.c()) {
                if (next.d() != null) {
                    hashMap.put(next.b(), next.d());
                }
                if (next.e() != null && !next.e().isEmpty()) {
                    arrayList.addAll(next.e());
                }
            }
        }
        agVar.a(arrayList);
        agVar.a(hashMap);
        synchronized (this) {
            this.d = agVar;
        }
    }

    public void c() {
        boolean z = false;
        for (a next : this.g) {
            if (next.c()) {
                if (next.e() != null && !next.e().isEmpty()) {
                    next.a((List<ac>) null);
                    z = true;
                }
                z = z;
            }
        }
        if (z) {
            this.d.b(false);
            e();
        }
    }

    public void d() {
        ag g2 = g();
        if (g2 != null) {
            ArrayList<a> arrayList = new ArrayList<>(this.g.size());
            synchronized (this) {
                this.d = g2;
                for (a next : this.g) {
                    next.a(this.d);
                    if (!next.c()) {
                        arrayList.add(next);
                    }
                }
                for (a remove : arrayList) {
                    this.g.remove(remove);
                }
            }
            f();
        }
    }

    public void e() {
        if (this.d != null) {
            a(this.d);
        }
    }

    private ag g() {
        FileInputStream fileInputStream;
        Throwable th;
        if (!this.c.exists()) {
            return null;
        }
        try {
            fileInputStream = new FileInputStream(this.c);
            try {
                byte[] b2 = bt.b(fileInputStream);
                ag agVar = new ag();
                new bz().a(agVar, b2);
                bt.c(fileInputStream);
                return agVar;
            } catch (Exception e2) {
                e = e2;
                try {
                    e.printStackTrace();
                    bt.c(fileInputStream);
                    return null;
                } catch (Throwable th2) {
                    th = th2;
                    bt.c(fileInputStream);
                    throw th;
                }
            }
        } catch (Exception e3) {
            e = e3;
            fileInputStream = null;
            e.printStackTrace();
            bt.c(fileInputStream);
            return null;
        } catch (Throwable th3) {
            fileInputStream = null;
            th = th3;
            bt.c(fileInputStream);
            throw th;
        }
    }

    private void a(ag agVar) {
        byte[] a2;
        if (agVar != null) {
            try {
                synchronized (this) {
                    a2 = new cc().a(agVar);
                }
                if (a2 != null) {
                    bt.a(this.c, a2);
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    /* compiled from: IdTracker */
    public static class a {

        /* renamed from: a  reason: collision with root package name */
        private Context f63a;
        private Set<String> b = new HashSet();

        public a(Context context) {
            this.f63a = context;
        }

        public boolean a(String str) {
            return !this.b.contains(str);
        }

        public void b(String str) {
            this.b.add(str);
        }

        public void a() {
            if (!this.b.isEmpty()) {
                StringBuilder sb = new StringBuilder();
                for (String append : this.b) {
                    sb.append(append);
                    sb.append(',');
                }
                sb.deleteCharAt(sb.length() - 1);
                eb.a(this.f63a).edit().putString("invld_id", sb.toString()).commit();
            }
        }

        public void b() {
            String[] split;
            String string = eb.a(this.f63a).getString("invld_id", null);
            if (!TextUtils.isEmpty(string) && (split = string.split(",")) != null) {
                for (String str : split) {
                    if (!TextUtils.isEmpty(str)) {
                        this.b.add(str);
                    }
                }
            }
        }
    }
}
