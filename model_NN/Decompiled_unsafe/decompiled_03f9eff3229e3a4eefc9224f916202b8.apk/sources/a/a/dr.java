package a.a;

import android.content.Context;
import android.content.SharedPreferences;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/* compiled from: Caretaker */
public class dr {

    /* renamed from: a  reason: collision with root package name */
    private final String f87a = "umeng_event_snapshot";
    private boolean b = false;
    private SharedPreferences c;
    private Map<String, ArrayList<Object>> d = new HashMap();

    public dr(Context context) {
        this.c = eb.a(context, "umeng_event_snapshot");
    }

    public void a(boolean z) {
        this.b = z;
    }
}
