package a.a;

import a.a.cn;
import java.io.ByteArrayOutputStream;

/* compiled from: TSerializer */
public class cc {

    /* renamed from: a  reason: collision with root package name */
    private final ByteArrayOutputStream f52a;
    private final dc b;
    private cr c;

    public cc() {
        this(new cn.a());
    }

    public cc(ct ctVar) {
        this.f52a = new ByteArrayOutputStream();
        this.b = new dc(this.f52a);
        this.c = ctVar.a(this.b);
    }

    public byte[] a(bw bwVar) throws ca {
        this.f52a.reset();
        bwVar.b(this.c);
        return this.f52a.toByteArray();
    }
}
