package cn.banshenggua.aichang.room;

import android.app.Activity;
import android.app.ActivityManager;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import cn.a.a.a;
import cn.banshenggua.aichang.app.Session;
import cn.banshenggua.aichang.room.message.User;
import cn.banshenggua.aichang.rtmpclient.AudioChannel;
import cn.banshenggua.aichang.rtmpclient.Channel;
import cn.banshenggua.aichang.rtmpclient.Client;
import cn.banshenggua.aichang.rtmpclient.GLVideoPlayView;
import cn.banshenggua.aichang.rtmpclient.NetStream;
import cn.banshenggua.aichang.rtmpclient.OnSubscriberListener;
import cn.banshenggua.aichang.rtmpclient.RtmpClientManager2;
import cn.banshenggua.aichang.rtmpclient.VideoPlayChannel;
import cn.banshenggua.aichang.rtmpclient.VideoPlayerInterface;
import cn.banshenggua.aichang.rtmpclient.VideoSize;
import cn.banshenggua.aichang.utils.Toaster;
import cn.banshenggua.aichang.utils.UIUtil;
import cn.banshenggua.aichang.utils.ULog;
import cn.banshenggua.aichang.utils.URLUtils;
import cn.banshenggua.aichang.widget.PlayImageView;
import java.util.ArrayList;
import java.util.List;

public class LivePlayer implements View.OnClickListener {
    private static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$room$LivePlayer$LIVE_TYPE = null;
    private static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$rtmpclient$RtmpClientManager2$RTMP_TYPE = null;
    private static final int BEGIN_PLAYING = 110;
    private static final int BEGIN_SHOW_PIC = 103;
    private static List<PlayClient> CurrentPlayClients = new ArrayList();
    private static final int HIDE_PROGRESS = 101;
    private static final int HIDE_VIDEO_PROGRESS = 108;
    private static final int RETRY_PLAYING = 106;
    private static final int SHOW_ERROR_PLAY_AUDIO = 109;
    private static final int SHOW_PROGRESS = 102;
    private static final int STOP_PLAYING = 107;
    private static final String TAG = "LivePlayer";
    private static final int VIDEO_LOADING = 104;
    private static final int VIDEO_PLAYING = 105;
    boolean isCheck = false;
    private boolean isRecording = false;
    boolean isSupport = false;
    private AudioChannel mAudioChannel = null;
    /* access modifiers changed from: private */
    public String mAudioUrl = null;
    private int mBufferTime = -1;
    /* access modifiers changed from: private */
    public ImageView mButton;
    private int mClientID0 = 0;
    private int mClientID1 = 1;
    private Activity mContext;
    /* access modifiers changed from: private */
    public Handler mHander = new Handler() {
        public void handleMessage(Message message) {
            super.handleMessage(message);
            switch (message.what) {
                case 101:
                    if (LivePlayer.this.mLoading != null) {
                        LivePlayer.this.mLoading.setVisibility(8);
                        return;
                    }
                    return;
                case 102:
                    if (LivePlayer.this.mLoading != null) {
                        LivePlayer.this.mLoading.setVisibility(0);
                        return;
                    }
                    return;
                case LivePlayer.BEGIN_SHOW_PIC /*103*/:
                    if (LivePlayer.this.mPicView != null) {
                        LivePlayer.this.mPicView.initData(new StringBuilder(String.valueOf(message.arg1)).toString());
                        return;
                    }
                    return;
                case LivePlayer.VIDEO_LOADING /*104*/:
                    if (LivePlayer.this.mButton != null) {
                        LivePlayer.this.mButton.setClickable(false);
                    }
                    if (LivePlayer.this.mViewGroup != null) {
                        LivePlayer.this.mViewGroup.setVisibility(4);
                    }
                    if (LivePlayer.this.mVideoLoading != null) {
                        LivePlayer.this.mVideoLoading.setVisibility(0);
                        return;
                    }
                    return;
                case LivePlayer.VIDEO_PLAYING /*105*/:
                    if (LivePlayer.this.mButton != null) {
                        LivePlayer.this.mButton.setClickable(true);
                    }
                    if (LivePlayer.this.mViewGroup != null) {
                        LivePlayer.this.mViewGroup.setVisibility(0);
                    }
                    if (LivePlayer.this.mVideoLoading != null) {
                        LivePlayer.this.mVideoLoading.setVisibility(8);
                        return;
                    }
                    return;
                case LivePlayer.RETRY_PLAYING /*106*/:
                    ULog.d("luoleixxxxxxxxx", "Retry_Playing");
                    LivePlayer.this.close();
                    if (!LivePlayer.this.isRecording()) {
                        ULog.d("luoleixxxxxxxxx", "Retry_Playing open: " + LivePlayer.this.mAudioUrl);
                        LivePlayer.this.open(LivePlayer.this.mAudioUrl, LivePlayer.this.mVideoUrl, LivePlayer.this.mIsMix);
                        return;
                    }
                    return;
                case LivePlayer.STOP_PLAYING /*107*/:
                    ULog.d("luoleixxxxxxxxx", "Stop_Playing");
                    LivePlayer.this.stopSubscribe();
                    return;
                case LivePlayer.HIDE_VIDEO_PROGRESS /*108*/:
                    if (LivePlayer.this.mVideoLoading != null) {
                        LivePlayer.this.mVideoLoading.setVisibility(8);
                        return;
                    }
                    return;
                case LivePlayer.SHOW_ERROR_PLAY_AUDIO /*109*/:
                    Toaster.showShortToast("无法初始化音频设备");
                    return;
                case LivePlayer.BEGIN_PLAYING /*110*/:
                    if (!LivePlayer.this.isRecording()) {
                        ULog.d("luoleixxxxxxxxx", "Begin_Playing");
                        LivePlayer.this.open(LivePlayer.this.mAudioUrl, LivePlayer.this.mVideoUrl, LivePlayer.this.mIsMix);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    };
    /* access modifiers changed from: private */
    public boolean mIsMix = false;
    private LIVE_TYPE mLiveType = LIVE_TYPE.AUDIO;
    /* access modifiers changed from: private */
    public ProgressBar mLoading;
    /* access modifiers changed from: private */
    public PlayImageView mPicView = null;
    private ViewGroup mRootView;
    private int mUseClientID0 = -1;
    private int mUseClientID1 = -1;
    private User mUser = null;
    private VideoPlayChannel mVideoChannel = null;
    /* access modifiers changed from: private */
    public ProgressBar mVideoLoading;
    private VideoSize mVideoSize = new VideoSize(320, 240);
    /* access modifiers changed from: private */
    public String mVideoUrl = null;
    private View mView;
    /* access modifiers changed from: private */
    public ViewGroup mViewGroup;
    private int mViewWidth = 0;

    enum LIVE_TYPE {
        AUDIO,
        VIDEO_CLOSE,
        VIDEO_OPEN
    }

    static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$room$LivePlayer$LIVE_TYPE() {
        int[] iArr = $SWITCH_TABLE$cn$banshenggua$aichang$room$LivePlayer$LIVE_TYPE;
        if (iArr == null) {
            iArr = new int[LIVE_TYPE.values().length];
            try {
                iArr[LIVE_TYPE.AUDIO.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[LIVE_TYPE.VIDEO_CLOSE.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[LIVE_TYPE.VIDEO_OPEN.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$cn$banshenggua$aichang$room$LivePlayer$LIVE_TYPE = iArr;
        }
        return iArr;
    }

    static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$rtmpclient$RtmpClientManager2$RTMP_TYPE() {
        int[] iArr = $SWITCH_TABLE$cn$banshenggua$aichang$rtmpclient$RtmpClientManager2$RTMP_TYPE;
        if (iArr == null) {
            iArr = new int[RtmpClientManager2.RTMP_TYPE.values().length];
            try {
                iArr[RtmpClientManager2.RTMP_TYPE.ERROR.ordinal()] = 4;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[RtmpClientManager2.RTMP_TYPE.ONE_CLIENT.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[RtmpClientManager2.RTMP_TYPE.ONE_STREAM.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[RtmpClientManager2.RTMP_TYPE.TWO_CLIENT.ordinal()] = 3;
            } catch (NoSuchFieldError e4) {
            }
            $SWITCH_TABLE$cn$banshenggua$aichang$rtmpclient$RtmpClientManager2$RTMP_TYPE = iArr;
        }
        return iArr;
    }

    private static class PlayClient {
        public String mAudioStreamId;
        public String mAudioStreamName;
        public String mClientId;
        public String mClientUrl;
        public String mVideoStreamId;
        public String mVideoStreamName;

        private PlayClient() {
        }
    }

    public void setBufferTime(int i) {
        this.mBufferTime = i;
        if (this.mAudioChannel != null) {
            this.mAudioChannel.setBuffer(i);
        }
    }

    public LivePlayer(Activity activity, ViewGroup viewGroup, VideoSize videoSize, int i, int i2, int i3, User user) {
        this.mRootView = viewGroup;
        this.mContext = activity;
        this.mVideoSize = videoSize;
        this.mViewWidth = i;
        this.mClientID0 = i2;
        this.mClientID1 = i3;
        initUser(user);
        initView();
    }

    private void initUser(User user) {
        this.mUser = user;
        if (this.mUser == null) {
            this.mUser = SimpleLiveRoomActivity.mMicUser;
        }
        if (this.mUser == null) {
            this.mUser = new User();
            this.mUser.mUid = Session.getCurrentAccount().b;
        }
    }

    private void initView() {
        this.mView = ViewGroup.inflate(this.mContext, a.g.view_live_player, null);
        this.mViewGroup = (ViewGroup) this.mView.findViewById(a.f.live_player_view_group);
        this.mLoading = (ProgressBar) this.mView.findViewById(a.f.live_player_loading);
        this.mLoading.setVisibility(8);
        this.mView.findViewById(a.f.live_player_video_button).setVisibility(8);
        this.mVideoLoading = (ProgressBar) this.mView.findViewById(a.f.live_player_video_loading);
        this.mVideoLoading.setVisibility(8);
        this.mRootView.removeAllViews();
        this.mRootView.addView(this.mView, new ViewGroup.LayoutParams(-1, -1));
    }

    public void setVideoButton(ImageView imageView) {
        this.mButton = imageView;
        setLiveType(this.mLiveType);
    }

    private class VideoPlayListener implements OnSubscriberListener {
        private boolean isLoading;

        private VideoPlayListener() {
            this.isLoading = false;
        }

        /* synthetic */ VideoPlayListener(LivePlayer livePlayer, VideoPlayListener videoPlayListener) {
            this();
        }

        public void OnLoading() {
            if (!this.isLoading && LivePlayer.this.mHander != null) {
                this.isLoading = true;
                LivePlayer.this.mHander.obtainMessage(LivePlayer.VIDEO_LOADING).sendToTarget();
            }
        }

        public void OnPlaying() {
            if (LivePlayer.this.mHander != null) {
                LivePlayer.this.mHander.obtainMessage(LivePlayer.VIDEO_PLAYING).sendToTarget();
            }
        }

        public void OnFinish() {
        }

        public void OnError(int i) {
        }
    }

    private class SubscribeListener implements OnSubscriberListener {
        private boolean mHasPlayed = false;
        private String mUid = "";

        public SubscribeListener(String str) {
            this.mUid = str;
        }

        public void OnFinish() {
            if (LivePlayer.this.mHander != null) {
                LivePlayer.this.mHander.obtainMessage(LivePlayer.STOP_PLAYING).sendToTarget();
            }
        }

        public void OnError(int i) {
            if (i == 100) {
                if (LivePlayer.this.mHander != null) {
                    LivePlayer.this.mHander.obtainMessage(LivePlayer.SHOW_ERROR_PLAY_AUDIO).sendToTarget();
                }
            } else if (LivePlayer.this.mHander != null) {
                ULog.d("luoleixxxxxxxxx", "onError handle retry");
                LivePlayer.this.mHander.obtainMessage(LivePlayer.RETRY_PLAYING).sendToTarget();
            }
        }

        public void OnLoading() {
            LivePlayer.this.showOrHideLoading(false);
        }

        public void OnPlaying() {
            LivePlayer.this.showOrHideLoading(true);
            if (!this.mHasPlayed) {
                this.mHasPlayed = true;
                if (LivePlayer.this.mPicView != null && LivePlayer.this.mHander != null) {
                    Message obtainMessage = LivePlayer.this.mHander.obtainMessage(LivePlayer.BEGIN_SHOW_PIC);
                    obtainMessage.what = LivePlayer.BEGIN_SHOW_PIC;
                    try {
                        obtainMessage.arg1 = Integer.parseInt(this.mUid);
                        LivePlayer.this.mHander.sendMessage(obtainMessage);
                    } catch (Exception e) {
                    }
                }
            }
        }
    }

    public void startSubscribeRtmpClient(String str, String str2, String str3, boolean z, boolean z2) {
        String[] UrlProgress;
        String[] strArr;
        VideoPlayChannel videoPlayChannel;
        Client client;
        VideoPlayChannel videoPlayChannel2;
        Client client2;
        NetStream netStream;
        if (!z || !z2) {
            String[] UrlProgress2 = URLUtils.UrlProgress(str2);
            UrlProgress = URLUtils.UrlProgress(str3);
            strArr = UrlProgress2;
        } else {
            ULog.d(TAG, "stream is isMix: " + z2);
            String[] UrlProgress3 = URLUtils.UrlProgress(str3);
            UrlProgress = URLUtils.UrlProgress(str3);
            strArr = UrlProgress3;
        }
        if (strArr != null && strArr.length >= 2) {
            AudioChannel audioChannel = new AudioChannel(strArr[1], null, 0, this.mClientID0);
            audioChannel.setBuffer(this.mBufferTime);
            VideoPlayChannel videoPlayChannel3 = null;
            Client client3 = null;
            RtmpClientManager2.RTMP_TYPE processURLS = RtmpClientManager2.processURLS(strArr, UrlProgress);
            switch ($SWITCH_TABLE$cn$banshenggua$aichang$rtmpclient$RtmpClientManager2$RTMP_TYPE()[processURLS.ordinal()]) {
                case 1:
                    if (z) {
                        videoPlayChannel3 = new VideoPlayChannel(UrlProgress[1], null, 0, this.mClientID0, getInstanceVideoView());
                    }
                    NetStream netStream2 = new NetStream(strArr[1], audioChannel, videoPlayChannel3, 0);
                    Client client4 = new Client(strArr[0], null, this.mClientID0);
                    client4.addStream(netStream2);
                    this.mUseClientID0 = this.mClientID0;
                    Client client5 = client4;
                    videoPlayChannel = videoPlayChannel3;
                    client = client5;
                    break;
                case 2:
                    if (z) {
                        VideoPlayChannel videoPlayChannel4 = new VideoPlayChannel(UrlProgress[1], null, 1, this.mClientID0, getInstanceVideoView());
                        videoPlayChannel = videoPlayChannel4;
                        netStream = new NetStream(UrlProgress[1], (Channel) null, videoPlayChannel4, 1);
                    } else {
                        videoPlayChannel = null;
                        netStream = null;
                    }
                    NetStream netStream3 = new NetStream(strArr[1], audioChannel, (Channel) null, 0);
                    Client client6 = new Client(strArr[0], null, this.mClientID0);
                    client6.addStream(netStream3);
                    client6.addStream(netStream);
                    this.mUseClientID0 = this.mClientID0;
                    client = client6;
                    break;
                case 3:
                    if (z) {
                        VideoPlayChannel videoPlayChannel5 = new VideoPlayChannel(UrlProgress[1], null, 0, this.mClientID1, getInstanceVideoView());
                        NetStream netStream4 = new NetStream(UrlProgress[1], (Channel) null, videoPlayChannel5, 0);
                        Client client7 = new Client(UrlProgress[0], null, this.mClientID1);
                        client7.addStream(netStream4);
                        this.mUseClientID1 = this.mClientID1;
                        Client client8 = client7;
                        videoPlayChannel2 = videoPlayChannel5;
                        client2 = client8;
                    } else {
                        videoPlayChannel2 = null;
                        client2 = null;
                    }
                    NetStream netStream5 = new NetStream(strArr[1], audioChannel, (Channel) null, 0);
                    Client client9 = new Client(strArr[0], null, this.mClientID0);
                    client9.addStream(netStream5);
                    this.mUseClientID0 = this.mClientID0;
                    client3 = client2;
                    client = client9;
                    break;
                case 4:
                    NetStream netStream6 = new NetStream(strArr[1], audioChannel, null);
                    netStream6.setSid(0);
                    Client client10 = new Client(strArr[0], null);
                    client10.setCid(this.mClientID0);
                    client10.addStream(netStream6);
                    this.mUseClientID0 = this.mClientID0;
                    Client client11 = client10;
                    videoPlayChannel = null;
                    client = client11;
                    break;
                default:
                    videoPlayChannel = null;
                    client = null;
                    break;
            }
            if (isRecording()) {
                stopSubscribe(true);
            }
            ULog.d(TAG, "stream type: " + processURLS);
            RtmpClientManager2 instance = RtmpClientManager2.getInstance();
            if (client != null) {
                ULog.d(TAG, "audio client cid: " + client.cid);
                ULog.d(TAG, "audio url 0: " + strArr[0]);
                ULog.d(TAG, "audio url 1: " + strArr[1]);
                instance.addClient(client);
            }
            if (client3 != null) {
                ULog.d(TAG, "video client cid: " + client3.cid);
                ULog.d(TAG, "video url 0: " + UrlProgress[0]);
                ULog.d(TAG, "video url 1: " + UrlProgress[1]);
                instance.addClient(client3);
            }
            if (audioChannel != null) {
                if (this.mAudioChannel != null) {
                    this.mAudioChannel.stopChannel();
                    this.mAudioChannel = null;
                }
                this.mAudioChannel = audioChannel;
                audioChannel.startChannel(true);
                this.mAudioChannel.setListener(new SubscribeListener(str));
            }
            if (videoPlayChannel != null) {
                if (this.mVideoChannel != null) {
                    this.mVideoChannel.stopChannel();
                    this.mVideoChannel = null;
                }
                this.mVideoChannel = videoPlayChannel;
                this.mVideoChannel.setAudioChannel(this.mAudioChannel);
                this.mVideoChannel.setListener(new VideoPlayListener(this, null));
                videoPlayChannel.startChannel();
            }
            instance.startManager();
        }
    }

    public boolean isRecording() {
        return this.isRecording;
    }

    public void setRecording(boolean z) {
        this.isRecording = z;
        stopSubscribe();
    }

    public void stopSubscribe() {
        stopSubscribe(true);
    }

    /* access modifiers changed from: private */
    public void showOrHideLoading(boolean z) {
        if (this.mHander != null) {
            if (z) {
                if (!this.mHander.hasMessages(101)) {
                    this.mHander.obtainMessage(101).sendToTarget();
                }
            } else if (!this.mHander.hasMessages(102)) {
                this.mHander.obtainMessage(102).sendToTarget();
            }
        }
    }

    public void stopSubscribe(boolean z) {
        if (z && this.mViewGroup != null) {
            this.mViewGroup.setVisibility(8);
            this.mViewGroup.removeAllViews();
        }
        ULog.d("luolei stopSubscribe", "cid0 = " + this.mUseClientID0 + "; cid1 = " + this.mUseClientID1);
        if (this.mUseClientID0 > -1) {
            RtmpClientManager2.getInstance().stopConnect(this.mUseClientID0);
        }
        if (this.mUseClientID1 > -1) {
            RtmpClientManager2.getInstance().stopConnect(this.mUseClientID1);
        }
        showOrHideLoading(true);
        if (this.mHander != null) {
            this.mHander.obtainMessage(HIDE_VIDEO_PROGRESS).sendToTarget();
        }
    }

    public boolean isPlayVideo() {
        return SimpleLiveRoomActivity.isVideoOpen();
    }

    private boolean isSupportGLView() {
        boolean z;
        if (this.isCheck) {
            return this.isSupport;
        }
        if (this.mContext == null) {
            return false;
        }
        if (((ActivityManager) this.mContext.getSystemService("activity")).getDeviceConfigurationInfo().reqGlEsVersion >= 131072) {
            z = true;
        } else {
            z = false;
        }
        this.isSupport = z;
        return this.isSupport;
    }

    private void setLiveType(LIVE_TYPE live_type) {
        this.mLiveType = live_type;
        if (this.mButton != null) {
            this.mButton.setClickable(true);
            this.mButton.setOnClickListener(this);
            switch ($SWITCH_TABLE$cn$banshenggua$aichang$room$LivePlayer$LIVE_TYPE()[this.mLiveType.ordinal()]) {
                case 1:
                    this.mButton.setVisibility(8);
                    return;
                case 2:
                    this.mButton.setVisibility(0);
                    this.mButton.setSelected(false);
                    return;
                case 3:
                    this.mButton.setVisibility(0);
                    this.mButton.setSelected(true);
                    return;
                default:
                    return;
            }
        }
    }

    private void closeVideo() {
        if (!TextUtils.isEmpty(this.mVideoUrl) && this.mLiveType == LIVE_TYPE.VIDEO_OPEN) {
            String[] UrlProgress = URLUtils.UrlProgress(this.mAudioUrl);
            String[] UrlProgress2 = URLUtils.UrlProgress(this.mVideoUrl);
            if (UrlProgress != null && UrlProgress.length >= 2) {
                RtmpClientManager2.RTMP_TYPE processURLS = RtmpClientManager2.processURLS(UrlProgress, UrlProgress2);
                RtmpClientManager2 instance = RtmpClientManager2.getInstance();
                if (instance.isRunning()) {
                    ULog.d("luolei", "closeVideo processURLS type: " + processURLS);
                    switch ($SWITCH_TABLE$cn$banshenggua$aichang$rtmpclient$RtmpClientManager2$RTMP_TYPE()[processURLS.ordinal()]) {
                        case 1:
                            ULog.e(TAG, "not support close video: " + this.mAudioUrl + "; " + this.mVideoUrl);
                            break;
                        case 2:
                            instance.stopStream(this.mClientID0, 1);
                            break;
                        case 3:
                            instance.stopConnect(this.mClientID1);
                            break;
                        case 4:
                            ULog.e(TAG, "not support close video: " + this.mAudioUrl + "; " + this.mVideoUrl);
                            break;
                    }
                    setLiveType(LIVE_TYPE.VIDEO_CLOSE);
                    if (this.mViewGroup != null) {
                        this.mViewGroup.removeAllViews();
                        this.mViewGroup.setVisibility(8);
                    }
                }
            }
        }
    }

    private void openPicInfo(String str) {
        this.mViewGroup.setVisibility(0);
        this.mViewGroup.removeAllViews();
        if (this.mPicView != null) {
            this.mPicView.cancelTimer();
            this.mPicView = null;
        }
        this.mPicView = new PlayImageView(this.mContext);
        this.mPicView.setScaleType(ImageView.ScaleType.MATRIX);
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(-1, -1);
        this.mViewGroup.addView(this.mPicView, layoutParams);
        this.mRootView.removeAllViews();
        this.mRootView.addView(this.mView, layoutParams);
    }

    private void closePicInfo(String str) {
        if (this.mPicView != null) {
            this.mPicView.cancelTimer();
            this.mViewGroup.removeAllViews();
            this.mPicView = null;
        }
    }

    private VideoPlayerInterface getInstanceVideoView() {
        this.mViewGroup.setVisibility(0);
        this.mViewGroup.removeAllViews();
        GLVideoPlayView gLVideoPlayView = new GLVideoPlayView(this.mContext);
        if (this.mViewWidth == 0) {
            this.mViewWidth = UIUtil.getInstance().getmScreenWidth();
        }
        int i = this.mViewWidth;
        int i2 = (this.mVideoSize.height * i) / this.mVideoSize.width;
        this.mViewGroup.addView(gLVideoPlayView, new ViewGroup.LayoutParams(-1, -1));
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(i, i2);
        this.mRootView.removeAllViews();
        this.mRootView.addView(this.mView, layoutParams);
        return gLVideoPlayView;
    }

    private void openVideo() {
        VideoPlayChannel videoPlayChannel;
        if (!TextUtils.isEmpty(this.mVideoUrl) && this.mLiveType == LIVE_TYPE.VIDEO_CLOSE) {
            String[] UrlProgress = URLUtils.UrlProgress(this.mAudioUrl);
            String[] UrlProgress2 = URLUtils.UrlProgress(this.mVideoUrl);
            if (UrlProgress != null && UrlProgress.length >= 2) {
                RtmpClientManager2 instance = RtmpClientManager2.getInstance();
                if (instance.isRunning()) {
                    setLiveType(LIVE_TYPE.VIDEO_OPEN);
                    RtmpClientManager2.RTMP_TYPE processURLS = RtmpClientManager2.processURLS(UrlProgress, UrlProgress2);
                    Log.d("luolei", "openVideo processURLS type: " + processURLS);
                    switch ($SWITCH_TABLE$cn$banshenggua$aichang$rtmpclient$RtmpClientManager2$RTMP_TYPE()[processURLS.ordinal()]) {
                        case 1:
                            ULog.e(TAG, "not support close video: " + this.mAudioUrl + "; " + this.mVideoUrl);
                            videoPlayChannel = null;
                            break;
                        case 2:
                            videoPlayChannel = new VideoPlayChannel(UrlProgress2[1], null);
                            videoPlayChannel.attachPlayView(getInstanceVideoView());
                            videoPlayChannel.setSid(1);
                            videoPlayChannel.setCid(this.mClientID0);
                            NetStream netStream = new NetStream(UrlProgress2[1], null, videoPlayChannel);
                            netStream.setSid(1);
                            instance.addStream(this.mClientID0, netStream);
                            this.mUseClientID0 = this.mClientID0;
                            break;
                        case 3:
                            videoPlayChannel = new VideoPlayChannel(UrlProgress2[1], null);
                            videoPlayChannel.attachPlayView(getInstanceVideoView());
                            videoPlayChannel.setSid(0);
                            videoPlayChannel.setCid(this.mClientID1);
                            NetStream netStream2 = new NetStream(UrlProgress2[1], null, videoPlayChannel);
                            Client client = new Client(UrlProgress2[0], null);
                            netStream2.setSid(0);
                            client.setCid(this.mClientID1);
                            client.addStream(netStream2);
                            instance.addClient(client);
                            this.mUseClientID1 = this.mClientID1;
                            break;
                        case 4:
                            Log.e(TAG, "not support close video: " + this.mAudioUrl + "; " + this.mVideoUrl);
                        default:
                            videoPlayChannel = null;
                            break;
                    }
                    if (videoPlayChannel != null) {
                        Log.d("luolei", "videochannel sid: " + videoPlayChannel.getSid() + "; cid: " + videoPlayChannel.getCid());
                        if (this.mVideoChannel != null) {
                            this.mVideoChannel.stopChannel();
                            this.mVideoChannel = null;
                        }
                        this.mVideoChannel = videoPlayChannel;
                        this.mVideoChannel.setAudioChannel(this.mAudioChannel);
                        this.mVideoChannel.setListener(new VideoPlayListener(this, null));
                        videoPlayChannel.startChannel();
                    }
                }
            }
        }
    }

    public void updateVideoSize(VideoSize videoSize, int i) {
        this.mVideoSize = videoSize;
        int i2 = (videoSize.height * i) / videoSize.width;
        if (isPlayVideo()) {
            ViewGroup.LayoutParams layoutParams = this.mView.getLayoutParams();
            layoutParams.height = i2;
            layoutParams.width = i;
            this.mView.setLayoutParams(layoutParams);
        }
        this.mViewWidth = i;
    }

    public void resetOpen(String str, String str2) {
        this.mAudioUrl = str;
        this.mVideoUrl = str2;
        this.mHander.obtainMessage(RETRY_PLAYING).sendToTarget();
        ULog.d(TAG, "resetOpen audio: " + str + "; video: " + str2);
    }

    public void open(String str, String str2, boolean z) {
        this.mAudioUrl = str;
        this.mVideoUrl = str2;
        this.mIsMix = z;
        boolean isPlayVideo = isPlayVideo();
        ULog.d(TAG, "choseMessageProcess open 001 + " + isPlayVideo);
        String[] UrlProgress = URLUtils.UrlProgress(str);
        String[] UrlProgress2 = URLUtils.UrlProgress(str2);
        if (UrlProgress2 == null || UrlProgress2.length < 2) {
            this.mVideoUrl = null;
        }
        if (UrlProgress == null || UrlProgress.length < 2) {
            this.mAudioUrl = null;
        }
        if (TextUtils.isEmpty(this.mAudioUrl)) {
            ULog.e(TAG, "audio url null");
            return;
        }
        ULog.d(TAG, "choseMessageProcess open 002; video: " + this.mVideoUrl);
        if (!isSupportGLView()) {
            Toaster.showLongAtCenter(this.mContext, "抱歉，您的设备暂时不支持视频");
            this.mVideoUrl = null;
        }
        if (TextUtils.isEmpty(this.mVideoUrl)) {
            this.mLiveType = LIVE_TYPE.AUDIO;
            openPicInfo(this.mUser.mUid);
        } else if (isPlayVideo) {
            this.mLiveType = LIVE_TYPE.VIDEO_OPEN;
            closePicInfo(this.mUser.mUid);
        } else {
            this.mLiveType = LIVE_TYPE.VIDEO_CLOSE;
            openPicInfo(this.mUser.mUid);
        }
        ULog.d(TAG, "choseMessageProcess open 004; livetype: " + this.mLiveType);
        setLiveType(this.mLiveType);
        startSubscribeRtmpClient(this.mUser.mUid, this.mAudioUrl, this.mVideoUrl, isPlayVideo, z);
        ULog.d(SocketRouter.TAG, "choseMessageProcess open 006");
    }

    public void close() {
        stopSubscribe();
    }

    public void CloseOrOpenVideo(boolean z) {
        if (this.mLiveType == LIVE_TYPE.VIDEO_CLOSE) {
            if (!z) {
                closePicInfo(this.mUser.mUid);
                openVideo();
            }
        } else if (this.mLiveType == LIVE_TYPE.VIDEO_OPEN && z) {
            closeVideo();
            openPicInfo(this.mUser.mUid);
            if (this.mPicView != null && !TextUtils.isEmpty(this.mUser.mUid)) {
                Message obtainMessage = this.mHander.obtainMessage(BEGIN_SHOW_PIC);
                obtainMessage.what = BEGIN_SHOW_PIC;
                try {
                    obtainMessage.arg1 = Integer.parseInt(this.mUser.mUid);
                    this.mHander.sendMessage(obtainMessage);
                } catch (Exception e) {
                }
            }
        }
    }

    public void onClick(View view) {
        if (view != null && view == this.mButton) {
            if (this.mLiveType == LIVE_TYPE.VIDEO_CLOSE) {
                closePicInfo(this.mUser.mUid);
                openVideo();
            } else if (this.mLiveType == LIVE_TYPE.VIDEO_OPEN) {
                closeVideo();
                openPicInfo(this.mUser.mUid);
                if (this.mPicView != null && !TextUtils.isEmpty(this.mUser.mUid)) {
                    Message obtainMessage = this.mHander.obtainMessage(BEGIN_SHOW_PIC);
                    obtainMessage.what = BEGIN_SHOW_PIC;
                    try {
                        obtainMessage.arg1 = Integer.parseInt(this.mUser.mUid);
                        this.mHander.sendMessage(obtainMessage);
                    } catch (Exception e) {
                    }
                }
            }
        }
    }
}
