package cn.banshenggua.aichang.room.message;

import cn.banshenggua.aichang.utils.Constants;

public enum MessageKey {
    Message_Null(""),
    Message_ALL("allmessage"),
    Message_Login("login"),
    Message_Logout("logout"),
    Message_Join("join"),
    Message_Leave("leave"),
    Message_Talk(Constants.TALK),
    Message_STalk("talkto"),
    Message_Gift("gift"),
    Message_GetUsers("getusers"),
    Message_GetMics("getmics"),
    Message_HeartBeat("heartbeat"),
    Message_ReqMic("reqmic"),
    Message_CancelMic("cancelmic"),
    Message_KickUser("kick"),
    Message_MuteUser("mute"),
    Message_OpenMic("openmic"),
    Message_CloseMic("closemic"),
    Message_ChangeMic("changemic"),
    Message_Media("media"),
    Message_MuteRoom("muteroom"),
    Message_ServerError(SocketMessage.MSG_ERROR_KEY),
    Message_ServerSys("system"),
    Message_AdjustMic("adjustmic"),
    Message_ChangeBanzou("banzou"),
    Message_RoomUpdate("roomupdate"),
    Message_Family("family"),
    Message_FAMILY_DISSOLVE("family_dissolve"),
    Message_RoomMod("roommodify"),
    Message_Toast("toast"),
    Message_RoomParam("roomparam"),
    MessageCenter_Private("private"),
    Message_ACK("ack"),
    Message_MultiReqMic("multi_reqmic"),
    Message_MultiDelMic("multi_delmic"),
    Message_MultiInviteMic("multi_invitemic"),
    Message_MultiCloseMic("multi_closemic"),
    Message_MultiConfirmMic("multi_confirmmic"),
    Message_MultiMedia("multi_media"),
    Message_MultiChangeMic("multi_changemic"),
    Message_MultiOpenMic("multi_openmic"),
    Message_RtmpProp("rtmpprop"),
    Message_MultiCancelMic("multi_cancelmic"),
    Message_GlobalGift("global_gift");
    
    private final String mKey;

    private MessageKey(String str) {
        this.mKey = str;
    }

    public String getKey() {
        return this.mKey;
    }
}
