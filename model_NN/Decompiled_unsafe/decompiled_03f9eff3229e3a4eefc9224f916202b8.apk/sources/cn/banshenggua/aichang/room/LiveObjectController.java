package cn.banshenggua.aichang.room;

import android.app.Activity;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import cn.banshenggua.aichang.room.LiveObject;
import cn.banshenggua.aichang.room.message.Rtmp;
import cn.banshenggua.aichang.room.message.User;
import cn.banshenggua.aichang.rtmpclient.VideoSize;
import cn.banshenggua.aichang.utils.UIUtil;
import cn.banshenggua.aichang.utils.ULog;

public class LiveObjectController {
    private static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$room$LiveObjectController$LiveObjectIndex = null;
    private static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$room$LiveObjectController$ViewSizeType = null;
    public static final int MAX_SUPPORT = 2;
    private Activity mContext;
    private int mControllerWidth = 0;
    private LiveObject[] mLiveObjects = new LiveObject[2];
    private FrameLayout mPrimaryView;
    private FrameLayout mSecondaryView;
    private ViewSizeType mViewSizeType = ViewSizeType.None;

    enum ViewSizeType {
        Primary,
        Secondary,
        All,
        None
    }

    static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$room$LiveObjectController$LiveObjectIndex() {
        int[] iArr = $SWITCH_TABLE$cn$banshenggua$aichang$room$LiveObjectController$LiveObjectIndex;
        if (iArr == null) {
            iArr = new int[LiveObjectIndex.values().length];
            try {
                iArr[LiveObjectIndex.Primary.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[LiveObjectIndex.Secondary.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            $SWITCH_TABLE$cn$banshenggua$aichang$room$LiveObjectController$LiveObjectIndex = iArr;
        }
        return iArr;
    }

    static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$room$LiveObjectController$ViewSizeType() {
        int[] iArr = $SWITCH_TABLE$cn$banshenggua$aichang$room$LiveObjectController$ViewSizeType;
        if (iArr == null) {
            iArr = new int[ViewSizeType.values().length];
            try {
                iArr[ViewSizeType.All.ordinal()] = 3;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[ViewSizeType.None.ordinal()] = 4;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[ViewSizeType.Primary.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[ViewSizeType.Secondary.ordinal()] = 2;
            } catch (NoSuchFieldError e4) {
            }
            $SWITCH_TABLE$cn$banshenggua$aichang$room$LiveObjectController$ViewSizeType = iArr;
        }
        return iArr;
    }

    public enum LiveObjectIndex {
        Primary(0),
        Secondary(1);
        
        private int index = 0;

        private LiveObjectIndex(int i) {
            this.index = i;
        }

        private int getIndex() {
            return this.index;
        }
    }

    public LiveObjectController(Activity activity, FrameLayout frameLayout, FrameLayout frameLayout2) {
        this.mContext = activity;
        this.mPrimaryView = frameLayout;
        this.mSecondaryView = frameLayout2;
        this.mControllerWidth = UIUtil.getInstance().getmScreenWidth();
    }

    private void Add(LiveObjectIndex liveObjectIndex, LiveObject liveObject) {
        if (liveObjectIndex != null && liveObject != null && LiveObjectIndex.access$2(liveObjectIndex) < 2 && LiveObjectIndex.access$2(liveObjectIndex) >= 0) {
            this.mLiveObjects[LiveObjectIndex.access$2(liveObjectIndex)] = liveObject;
        }
    }

    private LiveObject Get(LiveObjectIndex liveObjectIndex) {
        if (liveObjectIndex == null) {
            return null;
        }
        return this.mLiveObjects[LiveObjectIndex.access$2(liveObjectIndex)];
    }

    private void Del(LiveObjectIndex liveObjectIndex) {
        if (liveObjectIndex != null) {
            this.mLiveObjects[LiveObjectIndex.access$2(liveObjectIndex)] = null;
        }
    }

    private void resizeViewSize(ViewSizeType viewSizeType) {
        if (this.mPrimaryView != null && this.mSecondaryView != null) {
            this.mViewSizeType = viewSizeType;
            ULog.d(SocketRouter.TAG, "resizeViewSize type: " + viewSizeType);
            ViewGroup.LayoutParams layoutParams = this.mPrimaryView.getLayoutParams();
            ViewGroup.LayoutParams layoutParams2 = this.mSecondaryView.getLayoutParams();
            int i = this.mControllerWidth;
            switch ($SWITCH_TABLE$cn$banshenggua$aichang$room$LiveObjectController$ViewSizeType()[viewSizeType.ordinal()]) {
                case 1:
                    layoutParams.width = i;
                    layoutParams2.width = 0;
                    this.mPrimaryView.setVisibility(0);
                    this.mSecondaryView.setVisibility(8);
                    break;
                case 2:
                    layoutParams.width = 0;
                    layoutParams2.width = i;
                    this.mPrimaryView.setVisibility(8);
                    this.mSecondaryView.setVisibility(0);
                    break;
                case 3:
                    if (SimpleLiveRoomActivity.isVideoOpen()) {
                        layoutParams.width = i / 2;
                        layoutParams2.width = i / 2;
                    } else {
                        layoutParams.width = i;
                        layoutParams2.width = 0;
                    }
                    this.mPrimaryView.setVisibility(0);
                    this.mSecondaryView.setVisibility(0);
                    break;
                case 4:
                    this.mPrimaryView.setVisibility(8);
                    this.mSecondaryView.setVisibility(8);
                    break;
            }
            this.mPrimaryView.setLayoutParams(layoutParams);
            this.mSecondaryView.setLayoutParams(layoutParams2);
        }
    }

    public void startLiveObject(LiveObjectIndex liveObjectIndex) {
        LiveObject Get = Get(liveObjectIndex);
        if (Get != null) {
            Get.Start();
        }
    }

    public void addLiveObject(LiveObjectIndex liveObjectIndex, LiveObject.LiveObjectType liveObjectType, Rtmp.RtmpUrls rtmpUrls, User user) {
        LiveObject.ClientID clientID;
        VideoSize videoSize = new VideoSize(320, 240);
        VideoSize videoSize2 = new VideoSize(320, 240);
        FrameLayout frameLayout = this.mPrimaryView;
        LiveObject.ClientID clientID2 = new LiveObject.ClientID(0, 1);
        int i = UIUtil.getInstance().getmScreenWidth();
        int i2 = UIUtil.getInstance().getmScreenWidth();
        if (liveObjectIndex == LiveObjectIndex.Primary) {
            resizeViewSize(ViewSizeType.Primary);
        }
        if (liveObjectIndex == LiveObjectIndex.Secondary) {
            frameLayout = this.mSecondaryView;
            LiveObject Get = Get(LiveObjectIndex.Primary);
            if (Get == null || !Get.isRunning() || !LiveObject.LiveObjectType.isVideo(liveObjectType)) {
                i = i2;
                videoSize = videoSize2;
            } else if (Get.isVideo()) {
                VideoSize videoSize3 = new VideoSize(160, 240);
                videoSize = new VideoSize(160, 240);
                i = this.mControllerWidth / 2;
                resizeViewSize(ViewSizeType.All);
                ULog.d("luolei", "updateVideoSize 003 1 start: " + videoSize3);
                Get.UpdateSize(videoSize3, this.mControllerWidth / 2);
            } else {
                videoSize = new VideoSize(320, 240);
                i = this.mControllerWidth;
                resizeViewSize(ViewSizeType.Secondary);
            }
            if (Get != null && Get.isPlayer()) {
                clientID2 = new LiveObject.ClientID(2, 3);
            }
            clientID = clientID2;
        } else {
            clientID = clientID2;
        }
        LiveObject Get2 = Get(liveObjectIndex);
        if (Get2 != null) {
            ULog.d("luoleixxxxxxxxx", "addLiveObject");
            Get2.Stop();
        } else {
            Get2 = new LiveObject(this.mContext);
            Add(liveObjectIndex, Get2);
        }
        Get2.InitObject(liveObjectType, rtmpUrls, videoSize, frameLayout, clientID, i, user);
    }

    public void removeAll() {
        for (int i = 0; i < this.mLiveObjects.length; i++) {
            if (this.mLiveObjects[i] != null) {
                this.mLiveObjects[i].Stop();
            }
        }
        resizeViewSize(ViewSizeType.None);
    }

    public void removeLiveObject(LiveObjectIndex liveObjectIndex) {
        LiveObject Get = Get(liveObjectIndex);
        if (Get != null) {
            Get.Stop();
            Del(liveObjectIndex);
            ULog.d(SocketRouter.TAG, "removeLiveObject index: " + liveObjectIndex + "; obj: " + Get);
            switch ($SWITCH_TABLE$cn$banshenggua$aichang$room$LiveObjectController$LiveObjectIndex()[liveObjectIndex.ordinal()]) {
                case 1:
                    resizeViewSize(ViewSizeType.None);
                    return;
                case 2:
                    LiveObject Get2 = Get(LiveObjectIndex.Primary);
                    if (Get2 != null && Get2.isRunning()) {
                        ULog.d("luolei", "updateVideoSize 003 3 start: 320 x 240");
                        Get2.UpdateSize(new VideoSize(320, 240), this.mControllerWidth);
                        Get2.setPlayBufferTime(-1);
                    }
                    resizeViewSize(ViewSizeType.Primary);
                    return;
                default:
                    return;
            }
        }
    }

    public LiveObject getLiveObject(LiveObjectIndex liveObjectIndex) {
        return Get(liveObjectIndex);
    }

    public User getUser(LiveObjectIndex liveObjectIndex) {
        LiveObject Get = Get(liveObjectIndex);
        if (Get != null) {
            return Get.getUser();
        }
        return null;
    }

    public void setUser(LiveObjectIndex liveObjectIndex, User user) {
        LiveObject Get = Get(liveObjectIndex);
        ULog.d(SocketRouter.TAG, "setUser index: " + liveObjectIndex + "; user: " + user + "; obj: " + Get);
        if (Get != null) {
            Get.setUser(user);
        }
    }

    public void CloseOrOpenVideo(boolean z) {
        for (int i = 0; i < this.mLiveObjects.length; i++) {
            if (this.mLiveObjects[i] != null) {
                if (z) {
                    this.mLiveObjects[i].CloseVideo();
                } else {
                    this.mLiveObjects[i].OpenVideo();
                }
            }
        }
        if (this.mViewSizeType == ViewSizeType.All) {
            resizeViewSize(this.mViewSizeType);
        }
    }
}
