package cn.banshenggua.aichang.room;

import android.app.Activity;
import android.widget.FrameLayout;
import cn.banshenggua.aichang.room.message.Rtmp;
import cn.banshenggua.aichang.room.message.User;
import cn.banshenggua.aichang.rtmpclient.VideoSize;
import cn.banshenggua.aichang.utils.ULog;

public class LiveObject {
    private static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$room$LiveObject$LiveObjectType;
    private static final String TAG = LiveObject.class.getSimpleName();
    private ClientID mCID;
    private Activity mContext = null;
    private LivePlayer mPlayer = null;
    private Rtmp.RtmpUrls mRtmp = null;
    private LiveObjectStat mStat = null;
    private LiveObjectType mType = null;
    private User mUser = null;
    private VideoSize mVSize;
    private int mVWidth = 0;
    private FrameLayout mView = null;

    enum LiveObjectStat {
        Stop,
        Running,
        Inited,
        UnInit
    }

    static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$room$LiveObject$LiveObjectType() {
        int[] iArr = $SWITCH_TABLE$cn$banshenggua$aichang$room$LiveObject$LiveObjectType;
        if (iArr == null) {
            iArr = new int[LiveObjectType.values().length];
            try {
                iArr[LiveObjectType.AudioPlayer.ordinal()] = 2;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[LiveObjectType.VideoPlayer.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            $SWITCH_TABLE$cn$banshenggua$aichang$room$LiveObject$LiveObjectType = iArr;
        }
        return iArr;
    }

    public enum LiveObjectType {
        VideoPlayer,
        AudioPlayer;

        public static boolean isVideo(LiveObjectType liveObjectType) {
            if (liveObjectType != null && liveObjectType == VideoPlayer) {
                return true;
            }
            return false;
        }

        public static boolean isPlayer(LiveObjectType liveObjectType) {
            if (liveObjectType == null) {
                return false;
            }
            if (liveObjectType == VideoPlayer || liveObjectType == AudioPlayer) {
                return true;
            }
            return false;
        }

        public static boolean isRecorder(LiveObjectType liveObjectType) {
            return false;
        }
    }

    public static class ClientID {
        public int cid0;
        public int cid1;

        public ClientID(int i, int i2) {
            this.cid0 = i;
            this.cid1 = i2;
        }
    }

    public LiveObject(Activity activity) {
        this.mContext = activity;
        this.mStat = LiveObjectStat.UnInit;
    }

    public void InitObject(LiveObjectType liveObjectType, Rtmp.RtmpUrls rtmpUrls, VideoSize videoSize, FrameLayout frameLayout, ClientID clientID, int i, User user) {
        Stop();
        this.mType = liveObjectType;
        this.mRtmp = rtmpUrls;
        this.mVSize = videoSize;
        this.mView = frameLayout;
        this.mCID = clientID;
        this.mVWidth = i;
        this.mUser = user;
        initPlayerOrRecorder();
    }

    public void resetUrlObject(Rtmp.RtmpUrls rtmpUrls) {
        if (this.mPlayer != null && this.mStat == LiveObjectStat.Running) {
            String str = null;
            this.mRtmp = rtmpUrls;
            String str2 = this.mRtmp.urlAudio;
            if (this.mType == LiveObjectType.VideoPlayer) {
                str = this.mRtmp.urlVideo;
            }
            this.mPlayer.resetOpen(str2, str);
        }
    }

    public void setRtmp(Rtmp.RtmpUrls rtmpUrls) {
        if (rtmpUrls != null) {
            this.mRtmp = rtmpUrls;
        }
    }

    public Rtmp.RtmpUrls getRtmp() {
        return this.mRtmp;
    }

    public void setUser(User user) {
        this.mUser = user;
    }

    public User getUser() {
        return this.mUser;
    }

    public String toString() {
        return "type: " + this.mType + "; stat: " + this.mStat + "; size: " + this.mVSize + "; rtmp: " + this.mRtmp + "; super: " + super.toString();
    }

    private void initPlayerOrRecorder() {
        switch ($SWITCH_TABLE$cn$banshenggua$aichang$room$LiveObject$LiveObjectType()[this.mType.ordinal()]) {
            case 1:
            case 2:
                this.mPlayer = new LivePlayer(this.mContext, this.mView, this.mVSize, this.mVWidth, this.mCID.cid0, this.mCID.cid1, this.mUser);
                break;
        }
        this.mStat = LiveObjectStat.Inited;
    }

    private void OpenLiveMedia() {
        if (this.mStat == LiveObjectStat.Inited) {
            String str = null;
            String str2 = this.mRtmp.urlAudio;
            if (this.mType == LiveObjectType.VideoPlayer) {
                str = this.mRtmp.urlVideo;
            }
            if (this.mPlayer != null) {
                this.mPlayer.open(str2, str, this.mRtmp.isMixStream);
            }
        }
    }

    private void CloseLiveMedia() {
        if (this.mStat == LiveObjectStat.Running) {
            if (this.mPlayer != null) {
                this.mPlayer.close();
            }
            this.mPlayer = null;
        }
    }

    public void Start() {
        switch ($SWITCH_TABLE$cn$banshenggua$aichang$room$LiveObject$LiveObjectType()[this.mType.ordinal()]) {
            case 1:
            case 2:
                OpenLiveMedia();
                break;
        }
        this.mStat = LiveObjectStat.Running;
    }

    public void Stop() {
        if (this.mType != null) {
            switch ($SWITCH_TABLE$cn$banshenggua$aichang$room$LiveObject$LiveObjectType()[this.mType.ordinal()]) {
                case 1:
                case 2:
                    ULog.d("luoleixxxxxxxxx", "Stop Stop");
                    CloseLiveMedia();
                    break;
            }
        }
        this.mStat = LiveObjectStat.Stop;
    }

    public void CloseVideo() {
        if (this.mType == LiveObjectType.VideoPlayer && this.mStat == LiveObjectStat.Running && this.mPlayer != null) {
            this.mPlayer.CloseOrOpenVideo(true);
        }
    }

    public void OpenVideo() {
        if (this.mType == LiveObjectType.VideoPlayer && this.mStat == LiveObjectStat.Running && this.mPlayer != null) {
            this.mPlayer.CloseOrOpenVideo(false);
        }
    }

    public void UpdateSize(VideoSize videoSize, int i) {
        this.mVSize = videoSize;
        this.mVWidth = i;
        switch ($SWITCH_TABLE$cn$banshenggua$aichang$room$LiveObject$LiveObjectType()[this.mType.ordinal()]) {
            case 1:
                this.mPlayer.updateVideoSize(this.mVSize, this.mVWidth);
                return;
            case 2:
            default:
                return;
        }
    }

    public boolean isRunning() {
        return this.mStat == LiveObjectStat.Running;
    }

    public boolean isVideo() {
        return LiveObjectType.isVideo(this.mType);
    }

    public boolean isPlayer() {
        return LiveObjectType.isPlayer(this.mType);
    }

    public boolean isRecorder() {
        return LiveObjectType.isRecorder(this.mType);
    }

    public void setPlayBufferTime(int i) {
        if (this.mPlayer != null) {
            this.mPlayer.setBufferTime(i);
        }
    }
}
