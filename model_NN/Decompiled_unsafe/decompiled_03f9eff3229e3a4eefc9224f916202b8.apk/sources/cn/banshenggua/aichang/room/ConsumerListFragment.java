package cn.banshenggua.aichang.room;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import cn.a.a.a;
import cn.banshenggua.aichang.widget.PullToRefreshBase;
import cn.banshenggua.aichang.widget.PullToRefreshListView;
import com.android.volley.w;
import com.pocketmusic.kshare.requestobjs.a;
import com.pocketmusic.kshare.requestobjs.f;
import com.pocketmusic.kshare.requestobjs.o;
import com.pocketmusic.kshare.requestobjs.q;
import org.json.JSONObject;

@SuppressLint({"ValidFragment", "InflateParams"})
public class ConsumerListFragment extends Fragment implements View.OnClickListener {
    private a account;
    /* access modifiers changed from: private */
    public ConsumerAdapter consumerAdapter;
    /* access modifiers changed from: private */
    public f mGift;
    private ListView mListView;
    private PullToRefreshBase.OnRefreshListener2 mOnRefreshListener = new PullToRefreshBase.OnRefreshListener2() {
        public void onPullDownToRefresh() {
            if (ConsumerListFragment.this.mGift != null) {
                ConsumerListFragment.this.mGift.c();
            }
        }

        public void onPullUpToRefresh() {
            if (ConsumerListFragment.this.mGift != null) {
                ConsumerListFragment.this.mGift.b();
            }
        }
    };
    /* access modifiers changed from: private */
    public PullToRefreshListView mRefreshListView;
    private q mSimpleRequestListener = new q() {
        public void onErrorResponse(w wVar) {
            ConsumerListFragment.this.mRefreshListView.onRefreshComplete();
            ConsumerListFragment.this.isNoResult();
        }

        public void onResponse(JSONObject jSONObject) {
            ConsumerListFragment.this.mRefreshListView.onRefreshComplete();
            f fVar = new f(f.a.GiftList);
            fVar.a(jSONObject);
            if (fVar.i() != -1000) {
                ConsumerListFragment.this.isNoResult();
                return;
            }
            if (fVar.h) {
                ConsumerListFragment.this.consumerAdapter.addItem(fVar.f());
            } else {
                ConsumerListFragment.this.consumerAdapter.refreshUI(fVar.f());
            }
            if (!fVar.d()) {
                ConsumerListFragment.this.mRefreshListView.setMode(PullToRefreshBase.Mode.PULL_DOWN_TO_REFRESH);
            } else {
                ConsumerListFragment.this.mRefreshListView.setMode(PullToRefreshBase.Mode.BOTH);
            }
            ConsumerListFragment.this.isNoResult();
        }
    };
    private View no_result_layout;
    private o room;
    private FrameLayout searchResultLayout;

    public static ConsumerListFragment newInstance(a aVar) {
        return new ConsumerListFragment(aVar);
    }

    public static ConsumerListFragment newInstance(o oVar) {
        return new ConsumerListFragment(oVar);
    }

    public ConsumerListFragment(o oVar) {
        this.room = oVar;
    }

    public ConsumerListFragment(a aVar) {
        this.account = aVar;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ViewGroup viewGroup2 = (ViewGroup) layoutInflater.inflate(a.g.fragment_download_song_v3, (ViewGroup) null);
        initView(viewGroup2);
        this.no_result_layout = viewGroup2.findViewById(a.f.no_result_layout);
        return viewGroup2;
    }

    public void onDestroyView() {
        super.onDestroyView();
        if (this.consumerAdapter != null) {
            this.consumerAdapter.clearItem();
        }
    }

    /* access modifiers changed from: protected */
    public void initView(ViewGroup viewGroup) {
        this.mRefreshListView = (PullToRefreshListView) viewGroup.findViewById(a.f.public_items_listview);
        this.mRefreshListView.setOnRefreshListener(this.mOnRefreshListener);
        this.mListView = (ListView) this.mRefreshListView.getRefreshableView();
        this.consumerAdapter = new ConsumerAdapter(getActivity(), this.room);
        this.mListView.setAdapter((ListAdapter) this.consumerAdapter);
        initData();
    }

    public void onResume() {
        super.onResume();
        if (getActivity() == null) {
        }
    }

    /* access modifiers changed from: private */
    public void isNoResult() {
        if (this.consumerAdapter != null && this.no_result_layout != null) {
            if (this.consumerAdapter.getCount() == 0) {
                this.no_result_layout.setVisibility(0);
            } else {
                this.no_result_layout.setVisibility(8);
            }
        }
    }

    private void initData() {
        if (this.room != null) {
            this.mGift = new f(f.a.RoomGiftSpendTop);
            this.mGift.n = this.room.f626a;
            this.mListView.addHeaderView(LayoutInflater.from(getActivity()).inflate(a.g.head_spend_top, (ViewGroup) null));
        } else {
            this.mGift = new f(f.a.UserGiftListByUser);
            if (this.account != null) {
                this.mGift.i = this.account.b;
            }
        }
        this.mGift.a(this.mSimpleRequestListener);
        this.mGift.a();
    }

    public void onClick(View view) {
    }
}
