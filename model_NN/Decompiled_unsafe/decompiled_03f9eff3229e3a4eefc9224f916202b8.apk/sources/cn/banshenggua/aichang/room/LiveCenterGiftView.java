package cn.banshenggua.aichang.room;

import android.annotation.TargetApi;
import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import cn.a.a.a;
import cn.banshenggua.aichang.app.Session;
import cn.banshenggua.aichang.room.message.GiftMessage;
import cn.banshenggua.aichang.room.message.User;
import cn.banshenggua.aichang.utils.ImageLoaderUtil;
import cn.banshenggua.aichang.utils.ImageUtil;
import cn.banshenggua.aichang.utils.PreferencesUtils;
import cn.banshenggua.aichang.utils.SoundUtil;
import cn.banshenggua.aichang.utils.ULog;
import com.d.a.b.a.n;
import com.d.a.b.c;
import com.d.a.b.d;
import com.pocketmusic.kshare.requestobjs.e;
import com.pocketmusic.kshare.requestobjs.j;
import java.util.ArrayList;

@TargetApi(11)
public class LiveCenterGiftView {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$pocketmusic$kshare$requestobjs$Gift$GiftType = null;
    public static final String TAG = "LiveCenterGiftView";
    private Animation animationIn;
    private Animation animationOut;
    private c defOptions;
    /* access modifiers changed from: private */
    public ArrayList<e> gifts;
    private c levelOptions;
    private View mContentView;
    private Activity mContext;
    private ViewHolder mHanHuaViewHander = null;
    private SoundUtil mSoundUtil = null;
    private c options;
    /* access modifiers changed from: private */
    public PlayGiftRun playGiftRun;
    /* access modifiers changed from: private */
    public View playGiftView;
    private View playGiftViewShowGift;
    private View playGiftViewShowMessage;
    private c userIconOptions;

    static /* synthetic */ int[] $SWITCH_TABLE$com$pocketmusic$kshare$requestobjs$Gift$GiftType() {
        int[] iArr = $SWITCH_TABLE$com$pocketmusic$kshare$requestobjs$Gift$GiftType;
        if (iArr == null) {
            iArr = new int[e.c.values().length];
            try {
                iArr[e.c.GuiBin.ordinal()] = 4;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[e.c.HanHua.ordinal()] = 3;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[e.c.Normal.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[e.c.SaQian.ordinal()] = 2;
            } catch (NoSuchFieldError e4) {
            }
            $SWITCH_TABLE$com$pocketmusic$kshare$requestobjs$Gift$GiftType = iArr;
        }
        return iArr;
    }

    public LiveCenterGiftView(Activity activity) {
        this.mContext = activity;
        initView();
    }

    private void initView() {
        if (this.mContentView == null) {
            this.mContentView = ViewGroup.inflate(this.mContext, a.g.view_live_show_center_gift, null);
        }
        this.options = ImageUtil.getDefaultOptionForGift();
        this.levelOptions = ImageUtil.getDefaultLevelOption();
        this.userIconOptions = ImageUtil.getOvalDefaultOption();
        this.defOptions = ImageUtil.getDefaultOption();
        this.playGiftView = this.mContentView.findViewById(a.f.room_gift_view);
        this.playGiftViewShowGift = this.mContentView.findViewById(a.f.room_gift_view_showgift);
        this.playGiftViewShowMessage = this.mContentView.findViewById(a.f.room_gift_view_giftmessage);
    }

    public View getContentView() {
        return this.mContentView;
    }

    public class PlayGiftRun implements Runnable {
        public boolean running = false;

        public PlayGiftRun() {
        }

        public void run() {
            ULog.d(LiveCenterGiftView.TAG, "PlayGiftRun: running");
            if (LiveCenterGiftView.this.playGiftView != null) {
                if (LiveCenterGiftView.this.playGiftView.getVisibility() == 0) {
                    ULog.d(LiveCenterGiftView.TAG, "PlayGiftRun: beginStopGiftView");
                    LiveCenterGiftView.this.stopGiftView();
                    return;
                }
                this.running = true;
                ULog.d(LiveCenterGiftView.TAG, "PlayGiftRun, giftsize: " + LiveCenterGiftView.this.gifts.size());
                if (LiveCenterGiftView.this.gifts.size() == 0) {
                    this.running = false;
                } else {
                    LiveCenterGiftView.this.playGiftView();
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void stopGiftView() {
        ULog.d(TAG, "stopGiftView");
        if (this.animationOut == null) {
            this.animationOut = AnimationUtils.loadAnimation(this.mContext, a.C0001a.alpha_out);
        }
        this.animationOut.setAnimationListener(new SimpleAnimatorListener() {
            public void onAnimationStart(Animation animation) {
                ULog.d(LiveCenterGiftView.TAG, "onAnimationStart");
                super.onAnimationStart(animation);
            }

            public void onAnimationEnd(Animation animation) {
                ULog.d(LiveCenterGiftView.TAG, "onAnimationEnd");
            }
        });
        new Handler().postDelayed(this.playGiftRun, (long) 200);
        this.animationOut.reset();
        this.playGiftView.clearAnimation();
        this.playGiftView.setAnimation(this.animationOut);
        this.animationOut.start();
        this.playGiftView.setVisibility(8);
    }

    private void showPlayGiftView(boolean z, boolean z2) {
        if (z) {
            this.playGiftView.setVisibility(0);
            if (z2) {
                this.playGiftViewShowGift.setVisibility(8);
                this.playGiftViewShowMessage.setVisibility(0);
                return;
            }
            this.playGiftViewShowGift.setVisibility(0);
            this.playGiftViewShowMessage.setVisibility(8);
            return;
        }
        this.playGiftView.setVisibility(8);
    }

    private void doNormalGiftView(final e eVar, Bitmap bitmap) {
        ULog.d(TAG, "doNormalGiftView: " + eVar.d);
        if (bitmap == null) {
            d.a().a(eVar.q.e, (ImageView) this.playGiftView.findViewById(a.f.room_gift_view_gift_icon), this.defOptions);
        } else {
            ((ImageView) this.playGiftView.findViewById(a.f.room_gift_view_gift_icon)).setImageBitmap(bitmap);
        }
        this.playGiftView.findViewById(a.f.room_gift_view_gift_text).setVisibility(0);
        this.playGiftView.findViewById(a.f.room_gift_view_gift_text_2).setVisibility(0);
        this.playGiftView.findViewById(a.f.room_gift_view_gift_text_3).setVisibility(0);
        if (!"1".equalsIgnoreCase(eVar.I) || eVar.J == null || eVar.J.length <= 0) {
            ((TextView) this.playGiftView.findViewById(a.f.room_gift_view_gift_text)).setText(eVar.A);
            ((TextView) this.playGiftView.findViewById(a.f.room_gift_view_gift_text_2)).setText(String.format("%s   %s", "赠送", eVar.d));
            this.playGiftView.findViewById(a.f.room_gift_view_gift_text_3).setVisibility(8);
        } else {
            int length = eVar.J.length;
            if (length > 2) {
                ((TextView) this.playGiftView.findViewById(a.f.room_gift_view_gift_text)).setText(eVar.J[0]);
                ((TextView) this.playGiftView.findViewById(a.f.room_gift_view_gift_text_2)).setText(eVar.J[1]);
                ((TextView) this.playGiftView.findViewById(a.f.room_gift_view_gift_text_3)).setText(eVar.J[2]);
            } else if (length > 1) {
                ((TextView) this.playGiftView.findViewById(a.f.room_gift_view_gift_text)).setText(eVar.J[0]);
                ((TextView) this.playGiftView.findViewById(a.f.room_gift_view_gift_text_2)).setText(eVar.J[1]);
                this.playGiftView.findViewById(a.f.room_gift_view_gift_text_3).setVisibility(8);
            } else {
                ((TextView) this.playGiftView.findViewById(a.f.room_gift_view_gift_text)).setText(eVar.J[0]);
                this.playGiftView.findViewById(a.f.room_gift_view_gift_text_2).setVisibility(8);
                this.playGiftView.findViewById(a.f.room_gift_view_gift_text_3).setVisibility(8);
            }
        }
        if (this.animationIn == null) {
            this.animationIn = AnimationUtils.loadAnimation(this.mContext, a.C0001a.alpha_in);
        }
        this.animationIn.setAnimationListener(new SimpleAnimatorListener() {
            public void onAnimationEnd(Animation animation) {
                ULog.d(LiveCenterGiftView.TAG, "animationIn end");
                new Handler().postDelayed(LiveCenterGiftView.this.playGiftRun, (long) eVar.j);
            }
        });
        this.animationIn.reset();
        this.playGiftView.clearAnimation();
        this.playGiftView.setAnimation(this.animationIn);
        showPlayGiftView(true, false);
        this.animationIn.start();
        playGiftSound(eVar);
    }

    private ViewHolder createHolder(View view) {
        ViewHolder viewHolder = new ViewHolder(null);
        viewHolder.mUserHead = (ImageView) view.findViewById(a.f.room_giftmsg_img_usericon);
        viewHolder.mNickname = (TextView) view.findViewById(a.f.room_giftmsg_message_nickname);
        viewHolder.mNicknameRight = (TextView) view.findViewById(a.f.room_giftmsg_message_nickname_right);
        viewHolder.mTextMid = (TextView) view.findViewById(a.f.room_giftmsg_message_nickname_mid);
        viewHolder.mTextEnd = (TextView) view.findViewById(a.f.room_giftmsg_message_nickname_end);
        viewHolder.mMessage = (TextView) view.findViewById(a.f.room_giftmsg_message_message);
        viewHolder.mLevelImage = (ImageView) view.findViewById(a.f.room_giftmsg_message_img_level);
        viewHolder.authIcon = (ImageView) view.findViewById(a.f.room_giftmsg_message_img_auth);
        viewHolder.mUserLevelAnima = (ImageView) view.findViewById(a.f.room_giftmsg_level_animation);
        viewHolder.vipIcon = (ImageView) view.findViewById(a.f.room_giftmsg_vip_icon);
        return viewHolder;
    }

    private void initView(ViewHolder viewHolder) {
        if (viewHolder != null) {
            try {
                viewHolder.mUserHead.setImageResource(a.e.default_ovaled);
            } catch (OutOfMemoryError e) {
                System.gc();
            }
            viewHolder.mUserHead.setVisibility(0);
            viewHolder.mUserHead.setTag(null);
            viewHolder.mNickname.setText("");
            viewHolder.mNicknameRight.setVisibility(8);
            viewHolder.mNicknameRight.setText("");
            viewHolder.mTextMid.setVisibility(8);
            viewHolder.mTextMid.setText("");
            viewHolder.mTextEnd.setVisibility(8);
            viewHolder.mTextEnd.setText("");
            viewHolder.mMessage.setText("");
            viewHolder.mLevelImage.setVisibility(8);
            viewHolder.authIcon.setVisibility(8);
            viewHolder.vipIcon.setVisibility(8);
            viewHolder.mUserLevelAnima.setVisibility(8);
        }
    }

    private static class ViewHolder {
        public ImageView authIcon;
        public ImageView mLevelImage;
        public TextView mMessage;
        public TextView mNickname;
        public TextView mNicknameRight;
        public TextView mTextEnd;
        public TextView mTextMid;
        public ImageView mUserHead;
        public ImageView mUserLevelAnima;
        public ImageView vipIcon;

        private ViewHolder() {
        }

        /* synthetic */ ViewHolder(ViewHolder viewHolder) {
            this();
        }
    }

    private void showMessageView(e eVar, ViewHolder viewHolder) {
        String str = eVar.q.f615a;
        String str2 = "";
        if (eVar.q != null) {
            if (isMe(str)) {
                str = this.mContext.getResources().getString(a.h.room_me);
            } else {
                str = eVar.q.a();
            }
            ImageLoaderUtil.getInstance().a(eVar.q.a(User.FACE_SIZE.SIM), viewHolder.mUserHead, this.userIconOptions);
            str2 = "送给";
            if (eVar.r != null) {
                if (isMe(eVar.r.f615a)) {
                    str2 = String.format("%s %s", str2, this.mContext.getResources().getString(a.h.room_me));
                } else {
                    str2 = String.format("%s %s", str2, eVar.r.a());
                }
            }
            if (eVar != null) {
                switch ($SWITCH_TABLE$com$pocketmusic$kshare$requestobjs$Gift$GiftType()[eVar.O.ordinal()]) {
                    case 3:
                        viewHolder.mNicknameRight.setVisibility(0);
                        viewHolder.mNicknameRight.setText("发出一个喊话");
                        str2 = eVar.h;
                        break;
                    default:
                        showPlayGiftView(false, false);
                        break;
                }
            }
            setAuthIcon(viewHolder, eVar.q);
            setVipIcon(viewHolder, eVar.q);
        }
        viewHolder.mMessage.setText(str2);
        viewHolder.mNickname.setText(str);
        viewHolder.mUserHead.setTag(eVar.q);
    }

    private void setAuthIcon(ViewHolder viewHolder, e.C0014e eVar) {
        if (eVar == null) {
            return;
        }
        if (!TextUtils.isEmpty(eVar.j)) {
            ImageLoaderUtil.displayImageBg(viewHolder.authIcon, eVar.i, this.levelOptions);
            viewHolder.authIcon.setVisibility(0);
            return;
        }
        viewHolder.authIcon.setVisibility(8);
    }

    private void setVipIcon(ViewHolder viewHolder, e.C0014e eVar) {
        if (eVar == null) {
            return;
        }
        if (TextUtils.isEmpty(eVar.m)) {
            setVipIcon(viewHolder, eVar.f615a);
        } else if (eVar.m.equalsIgnoreCase("vip")) {
            viewHolder.vipIcon.setVisibility(0);
            viewHolder.vipIcon.setImageResource(a.e.vip_icon);
        }
    }

    private void setVipIcon(ViewHolder viewHolder, String str) {
        if (SimpleLiveRoomActivity.isVipUser(str)) {
            viewHolder.vipIcon.setImageResource(a.e.room_indentity_online);
            viewHolder.vipIcon.setVisibility(0);
            return;
        }
        viewHolder.vipIcon.setVisibility(8);
    }

    private boolean isMe(String str) {
        if (TextUtils.isEmpty(str) || !str.equalsIgnoreCase(Session.getCurrentAccount().b)) {
            return false;
        }
        return true;
    }

    private void doHanHuaGiftView(final e eVar) {
        if (this.mHanHuaViewHander == null) {
            this.mHanHuaViewHander = createHolder(this.playGiftViewShowMessage);
        }
        initView(this.mHanHuaViewHander);
        showMessageView(eVar, this.mHanHuaViewHander);
        if (this.animationIn == null) {
            this.animationIn = AnimationUtils.loadAnimation(this.mContext, a.C0001a.alpha_in);
        }
        this.animationIn.setAnimationListener(new SimpleAnimatorListener() {
            public void onAnimationEnd(Animation animation) {
                new Handler().postDelayed(LiveCenterGiftView.this.playGiftRun, (long) eVar.j);
            }
        });
        this.animationIn.reset();
        this.playGiftView.setAnimation(this.animationIn);
        showPlayGiftView(true, true);
        this.animationIn.start();
    }

    /* access modifiers changed from: private */
    public void playGiftView(e eVar, Bitmap bitmap) {
        switch ($SWITCH_TABLE$com$pocketmusic$kshare$requestobjs$Gift$GiftType()[eVar.O.ordinal()]) {
            case 3:
                doHanHuaGiftView(eVar);
                return;
            default:
                doNormalGiftView(eVar, bitmap);
                return;
        }
    }

    /* access modifiers changed from: private */
    public void playGiftView() {
        final e remove = this.gifts.remove(0);
        switch ($SWITCH_TABLE$com$pocketmusic$kshare$requestobjs$Gift$GiftType()[remove.O.ordinal()]) {
            case 3:
                playGiftView(remove, null);
                return;
            default:
                ULog.d(TAG, "imageLoader gift: " + remove);
                remove.e = String.valueOf(remove.e) + "bug";
                ImageLoaderUtil.getInstance().a(remove.e, this.options, new n() {
                    public void onLoadingComplete(String str, View view, Bitmap bitmap) {
                        ULog.d(LiveCenterGiftView.TAG, "playGiftView onLoadingComplete: " + remove.d);
                        if (bitmap != null) {
                            LiveCenterGiftView.this.playGiftView(remove, bitmap);
                        } else {
                            new Handler().postDelayed(LiveCenterGiftView.this.playGiftRun, 0);
                        }
                    }

                    public void onLoadingCancelled(String str, View view) {
                        ULog.d(LiveCenterGiftView.TAG, "playGiftView onLoadingCancelled");
                        LiveCenterGiftView.this.gifts.add(remove);
                        new Handler().postDelayed(LiveCenterGiftView.this.playGiftRun, 0);
                    }

                    public void onLoadingFailed(String str, View view, com.d.a.b.a.c cVar) {
                        ULog.d(LiveCenterGiftView.TAG, "playGiftView onLoadingFailed");
                        LiveCenterGiftView.this.gifts.add(remove);
                        new Handler().postDelayed(LiveCenterGiftView.this.playGiftRun, 0);
                    }
                });
                return;
        }
    }

    public void playGiftAnim(j jVar, e eVar, GiftMessage giftMessage) {
        if ("1".equalsIgnoreCase(jVar.e) || eVar.O == e.c.HanHua || "1".equalsIgnoreCase(eVar.I)) {
            ULog.d(TAG, "PlayGiftAnim");
            try {
                if (((long) Integer.valueOf(eVar.c).intValue()) >= jVar.h || eVar.O == e.c.HanHua || "1".equalsIgnoreCase(eVar.I)) {
                    e a2 = eVar.a();
                    a2.q = e.a(giftMessage.mFrom);
                    a2.A = a2.q.c;
                    if ("1".equalsIgnoreCase(jVar.e)) {
                        a2.j = jVar.g;
                    } else {
                        a2.j = eVar.j;
                    }
                    a2.h = giftMessage.mText;
                    a2.J = giftMessage.mCustoms;
                    if (this.gifts == null) {
                        this.gifts = new ArrayList<>();
                    }
                    this.gifts.add(a2);
                    if (this.playGiftRun == null) {
                        this.playGiftRun = new PlayGiftRun();
                    }
                    ULog.d(TAG, "PlayGiftAnim " + this.playGiftRun.running + "; playtime: " + a2.j);
                    if (!this.playGiftRun.running) {
                        this.playGiftRun.running = true;
                        new Handler().post(this.playGiftRun);
                    }
                }
            } catch (Exception e) {
                ULog.e(TAG, "PlayGiftAnim error", e);
            }
        } else {
            ULog.d(TAG, "play gift anim: " + eVar.e);
        }
    }

    private void playGiftSound(e eVar) {
        if (this.mSoundUtil == null) {
            this.mSoundUtil = new SoundUtil(this.mContext);
        }
        if (eVar == null || !eVar.K || TextUtils.isEmpty(eVar.x) || !PreferencesUtils.loadPrefBoolean(this.mContext, SimpleLiveRoomActivity.PRE_KEY_SHOW_VOICE_GIFT, true)) {
        }
    }

    public void cleanGifts() {
        if (this.gifts != null) {
            this.gifts.clear();
        }
    }
}
