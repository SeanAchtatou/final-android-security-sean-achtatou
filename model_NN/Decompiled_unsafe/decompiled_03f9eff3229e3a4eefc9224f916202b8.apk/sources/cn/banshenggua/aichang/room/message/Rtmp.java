package cn.banshenggua.aichang.room.message;

import android.text.TextUtils;
import cn.banshenggua.aichang.room.SocketRouter;
import cn.banshenggua.aichang.utils.ULog;
import com.baidu.mobads.interfaces.IXAdRequestInfo;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Rtmp implements Serializable {
    private static final long serialVersionUID = 5864034354439894629L;
    public String checksum = null;
    public String params = null;
    public String urlaud = null;
    public String urlpub = null;
    public List<RtmpUrls> urlsList = new ArrayList();

    public void parseRtmp(JSONObject jSONObject) {
        if (jSONObject != null) {
            try {
                this.urlaud = jSONObject.optString("urlaud", "");
                this.urlpub = jSONObject.optString("urlpub", "");
                this.params = jSONObject.optString("params", "");
                this.checksum = jSONObject.optString("checksum", "");
                JSONObject optJSONObject = jSONObject.optJSONObject("streams");
                JSONArray optJSONArray = jSONObject.optJSONArray("urls");
                if (optJSONArray != null && optJSONArray.length() > 0) {
                    this.urlsList.clear();
                    for (int i = 0; i < optJSONArray.length(); i++) {
                        if (!(optJSONArray.get(i) == null || optJSONArray.get(i) == JSONObject.NULL)) {
                            RtmpUrls rtmpUrls = new RtmpUrls();
                            rtmpUrls.parseUrls(optJSONArray.getJSONObject(i));
                            rtmpUrls.isMixStream = getIsMixStream(optJSONObject, i);
                            this.urlsList.add(rtmpUrls);
                        }
                    }
                    ULog.d(SocketRouter.TAG, "RtmpUrls=" + this.urlsList.size());
                    ULog.d(SocketRouter.TAG, "RtmpUrls urlAudio=" + this.urlsList.get(0).urlAudio);
                    ULog.d(SocketRouter.TAG, "RtmpUrls urlVideo=" + this.urlsList.get(0).urlVideo);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private boolean getIsMixStream(JSONObject jSONObject, int i) {
        if (jSONObject == null) {
            return false;
        }
        if (i == 0) {
            return "mix".equals(jSONObject.optString("m1"));
        }
        if (i == 1) {
            return "mix".equals(jSONObject.optString("m2"));
        }
        return false;
    }

    public class RtmpUrls {
        public String host = null;
        public boolean isMixStream = false;
        public String media = null;
        public String uid = null;
        public String urlAudio = null;
        public String urlVideo = null;

        public RtmpUrls() {
        }

        public void parseUrls(JSONObject jSONObject) {
            this.urlAudio = jSONObject.optString("a", "");
            this.urlVideo = jSONObject.optString(IXAdRequestInfo.V, "");
            this.media = jSONObject.optString("media", "audio");
            if (this.media.equalsIgnoreCase("V")) {
                this.media = "video";
            }
            this.uid = jSONObject.optString("uid", "");
            this.host = jSONObject.optString("host");
            if (TextUtils.isEmpty(this.host)) {
                this.host = "117.79.145.218:1936";
            }
        }

        public String toString() {
            return "media: " + this.media + "; uid: " + this.uid + "; a: " + this.urlAudio + "; v: " + this.urlVideo;
        }
    }
}
