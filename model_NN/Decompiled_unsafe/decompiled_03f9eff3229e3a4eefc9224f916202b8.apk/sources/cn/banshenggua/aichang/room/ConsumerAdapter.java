package cn.banshenggua.aichang.room;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import cn.a.a.a;
import cn.banshenggua.aichang.app.Session;
import cn.banshenggua.aichang.entry.ArrayListAdapter;
import cn.banshenggua.aichang.room.message.User;
import cn.banshenggua.aichang.utils.ImageLoaderUtil;
import cn.banshenggua.aichang.utils.ImageUtil;
import cn.banshenggua.aichang.utils.Toaster;
import com.d.a.b.c;
import com.d.a.b.d;
import com.pocketmusic.kshare.requestobjs.a;
import com.pocketmusic.kshare.requestobjs.e;
import com.pocketmusic.kshare.requestobjs.o;

@SuppressLint({"ResourceAsColor"})
public class ConsumerAdapter extends ArrayListAdapter<e> {
    private Activity context;
    private d imgLoader;
    View.OnClickListener itemImageClickListener = new View.OnClickListener() {
        public void onClick(View view) {
            new a();
            e.C0014e eVar = ((e) ConsumerAdapter.this.getItem(((Integer) view.getTag()).intValue())).q;
            if (eVar != null && ConsumerAdapter.this.mRoom != null) {
                ConsumerAdapter.this.showUserDialog(eVar);
            }
        }
    };
    public View.OnLongClickListener mOnItemLongClickListener = new View.OnLongClickListener() {
        public boolean onLongClick(View view) {
            return false;
        }
    };
    /* access modifiers changed from: private */
    public o mRoom;
    private c options = ImageUtil.getOvalDefaultOption();
    private c optionsLevel = ImageUtil.getDefaultLevelOption();

    public ConsumerAdapter(Activity activity, o oVar) {
        super(activity);
        this.mRoom = oVar;
        this.context = activity;
        this.imgLoader = d.a();
    }

    @SuppressLint({"ResourceAsColor"})
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder;
        int i2;
        e eVar = (e) getItem(i);
        if (view == null || view.getTag() == null) {
            view = LayoutInflater.from(this.mContext).inflate(a.g.item_gift_consumer_v3, (ViewGroup) null);
            viewHolder = createViewHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        viewHolder.right.setVisibility(8);
        if (eVar.q != null) {
            if (!TextUtils.isEmpty(eVar.q.e)) {
                this.imgLoader.a(eVar.q.e, viewHolder.left, this.options);
            }
            viewHolder.left.setTag(Integer.valueOf(i));
            viewHolder.left.setOnClickListener(this.itemImageClickListener);
            viewHolder.gift_item_layout.setTag(Integer.valueOf(i));
            viewHolder.gift_item_layout.setOnClickListener(this.itemImageClickListener);
            if (!TextUtils.isEmpty(eVar.q.a())) {
                this.imgLoader.a(eVar.q.e, viewHolder.left, this.options);
                viewHolder.name.setText(eVar.q.a());
            }
            viewHolder.position.setVisibility(0);
            viewHolder.position.setText(String.valueOf(i + 1) + ".");
            viewHolder.consumer.setText(String.format("消费了%s个爱币", eVar.p));
            if (!TextUtils.isEmpty(eVar.q.j)) {
                ImageLoaderUtil.displayImageBg(viewHolder.authIcon, eVar.q.i, this.optionsLevel);
                viewHolder.authIcon.setVisibility(0);
            } else {
                viewHolder.authIcon.setVisibility(8);
            }
            if (eVar.q.h <= 0 || this.mRoom == null) {
                viewHolder.mLevelImg.setVisibility(8);
            } else {
                viewHolder.mLevelImg.setVisibility(0);
                ImageLoaderUtil.getInstance().a(eVar.q.k, viewHolder.mLevelImg, this.optionsLevel);
            }
            if (SimpleLiveRoomActivity.isVipUser(eVar.q.f615a)) {
                ImageView imageView = viewHolder.identityImg;
                if (eVar.q.l) {
                    i2 = a.e.room_indentity_online;
                } else {
                    i2 = a.e.room_indentity_online;
                }
                imageView.setImageResource(i2);
                viewHolder.identityImg.setVisibility(0);
            } else {
                viewHolder.identityImg.setVisibility(8);
            }
            if ("vip".equalsIgnoreCase(eVar.q.m)) {
                viewHolder.identityImg.setVisibility(0);
                viewHolder.identityImg.setImageResource(a.e.vip_icon);
            }
        }
        return view;
    }

    /* access modifiers changed from: private */
    public void showUserDialog(e.C0014e eVar) {
        if (eVar.f615a == null || !eVar.f615a.equals(Session.getCurrentAccount().b)) {
            User user = new User();
            user.mUid = eVar.f615a;
            user.mNickname = eVar.c;
            user.mGender = eVar.d;
            user.mFace = eVar.e;
            user.setFullName(eVar.g);
            user.mLevel = eVar.h;
            user.authIcon = eVar.i;
            user.auth_info = eVar.j;
            user.mLevelImage = eVar.k;
            Toaster.showLongToast("更多精彩, 请下载爱唱");
        }
    }

    private ViewHolder createViewHolder(View view) {
        ViewHolder viewHolder = new ViewHolder();
        viewHolder.gift_item_layout = view.findViewById(a.f.gift_item_layout);
        viewHolder.left = (ImageView) view.findViewById(a.f.gift_item_image_left);
        viewHolder.right = (ImageView) view.findViewById(a.f.gift_item_image_right);
        viewHolder.name = (TextView) view.findViewById(a.f.gift_item_name);
        viewHolder.position = (TextView) view.findViewById(a.f.gift_item_postion);
        viewHolder.consumer = (TextView) view.findViewById(a.f.gift_item_text);
        viewHolder.authIcon = (ImageView) view.findViewById(a.f.gift_img_auth);
        viewHolder.mLevelImg = (ImageView) view.findViewById(a.f.gift_img_level);
        viewHolder.identityImg = (ImageView) view.findViewById(a.f.gift_img_identity);
        return viewHolder;
    }

    class ViewHolder {
        public ImageView authIcon;
        TextView consumer;
        View gift_item_layout;
        public ImageView identityImg;
        ImageView left;
        public ImageView mLevelImg;
        TextView name;
        TextView position;
        ImageView right;

        ViewHolder() {
        }
    }
}
