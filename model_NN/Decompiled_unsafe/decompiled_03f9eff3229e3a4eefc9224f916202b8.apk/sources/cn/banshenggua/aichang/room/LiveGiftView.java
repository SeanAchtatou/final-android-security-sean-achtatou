package cn.banshenggua.aichang.room;

import android.annotation.TargetApi;
import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import cn.a.a.a;
import cn.banshenggua.aichang.app.Session;
import cn.banshenggua.aichang.room.message.GiftMessage;
import cn.banshenggua.aichang.room.message.User;
import cn.banshenggua.aichang.utils.ImageLoaderUtil;
import cn.banshenggua.aichang.utils.ImageUtil;
import cn.banshenggua.aichang.utils.PreferencesUtils;
import cn.banshenggua.aichang.utils.SoundUtil;
import cn.banshenggua.aichang.utils.UIUtil;
import cn.banshenggua.aichang.utils.ULog;
import com.d.a.b.a.n;
import com.d.a.b.c;
import com.d.a.b.d;
import com.pocketmusic.kshare.requestobjs.e;
import com.pocketmusic.kshare.requestobjs.j;
import java.util.ArrayList;
import java.util.Random;

@TargetApi(11)
public class LiveGiftView {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$pocketmusic$kshare$requestobjs$Gift$GiftType = null;
    public static final String TAG = "LiveGiftView";
    public static int playAnimWidth;
    private Animation animationIn;
    private Animation animationOut;
    private c defOptions;
    private FlakeView flakeView;
    /* access modifiers changed from: private */
    public ArrayList<e> gifts;
    private c levelOptions;
    private View mContentView;
    private Activity mContext;
    private ViewHolder mHanHuaViewHander = null;
    private ViewGroup mRootView;
    private SoundUtil mSoundUtil = null;
    private c options;
    private int playAnimHeight;
    /* access modifiers changed from: private */
    public PlayGiftRun playGiftRun;
    /* access modifiers changed from: private */
    public View playGiftView;
    private View playGiftViewShowGift;
    private View playGiftViewShowMessage;
    private c userIconOptions;

    static /* synthetic */ int[] $SWITCH_TABLE$com$pocketmusic$kshare$requestobjs$Gift$GiftType() {
        int[] iArr = $SWITCH_TABLE$com$pocketmusic$kshare$requestobjs$Gift$GiftType;
        if (iArr == null) {
            iArr = new int[e.c.values().length];
            try {
                iArr[e.c.GuiBin.ordinal()] = 4;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[e.c.HanHua.ordinal()] = 3;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[e.c.Normal.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[e.c.SaQian.ordinal()] = 2;
            } catch (NoSuchFieldError e4) {
            }
            $SWITCH_TABLE$com$pocketmusic$kshare$requestobjs$Gift$GiftType = iArr;
        }
        return iArr;
    }

    public LiveGiftView(Activity activity, ViewGroup viewGroup) {
        this.mContext = activity;
        this.mRootView = viewGroup;
        initView();
    }

    private void initView() {
        if (this.mRootView != null) {
            this.mRootView.removeAllViews();
            if (this.mContentView == null) {
                this.mContentView = ViewGroup.inflate(this.mContext, a.g.view_live_show_gift, null);
            }
            this.mRootView.addView(this.mContentView, new ViewGroup.LayoutParams(-1, -1));
            if (this.flakeView == null) {
                this.flakeView = new FlakeView(this.mContext);
                if (Integer.parseInt(Build.VERSION.SDK) >= 11) {
                    this.flakeView.setLayerType(1, null);
                }
                ((ViewGroup) this.mContentView.findViewById(a.f.room_gift_layout)).addView(this.flakeView);
            }
            this.playAnimHeight = (UIUtil.getInstance().getmScreenWidth() * 3) / 4;
            playAnimWidth = UIUtil.getInstance().getmScreenWidth();
            this.options = ImageUtil.getDefaultOptionForGift();
            this.levelOptions = ImageUtil.getDefaultLevelOption();
            this.userIconOptions = ImageUtil.getOvalDefaultOption();
            this.defOptions = ImageUtil.getDefaultOption();
            this.playGiftView = this.mContentView.findViewById(a.f.room_gift_view);
            this.playGiftViewShowGift = this.mContentView.findViewById(a.f.room_gift_view_showgift);
            this.playGiftViewShowMessage = this.mContentView.findViewById(a.f.room_gift_view_giftmessage);
        }
    }

    public class PlayGiftRun implements Runnable {
        public boolean running = false;

        public PlayGiftRun() {
        }

        public void run() {
            if (LiveGiftView.this.playGiftView != null) {
                if (LiveGiftView.this.playGiftView.getVisibility() == 0) {
                    LiveGiftView.this.stopGiftView();
                    return;
                }
                this.running = true;
                if (LiveGiftView.this.gifts.size() == 0) {
                    this.running = false;
                } else {
                    LiveGiftView.this.playGiftView();
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void stopGiftView() {
        if (this.animationOut == null) {
            this.animationOut = AnimationUtils.loadAnimation(this.mContext, a.C0001a.alpha_out);
        }
        this.animationOut.setAnimationListener(new SimpleAnimatorListener() {
            public void onAnimationEnd(Animation animation) {
                new Handler().postDelayed(LiveGiftView.this.playGiftRun, 0);
            }
        });
        this.animationOut.reset();
        this.playGiftView.setAnimation(this.animationOut);
        this.animationOut.start();
        this.playGiftView.setVisibility(8);
    }

    private void showPlayGiftView(boolean z, boolean z2) {
        if (z) {
            this.playGiftView.setVisibility(0);
            if (z2) {
                this.playGiftViewShowGift.setVisibility(8);
                this.playGiftViewShowMessage.setVisibility(0);
                return;
            }
            this.playGiftViewShowGift.setVisibility(0);
            this.playGiftViewShowMessage.setVisibility(8);
            return;
        }
        this.playGiftView.setVisibility(8);
    }

    private void doNormalGiftView(final e eVar, Bitmap bitmap) {
        if (bitmap == null) {
            d.a().a(eVar.q.e, (ImageView) this.playGiftView.findViewById(a.f.room_gift_view_gift_icon), this.defOptions);
        } else {
            ((ImageView) this.playGiftView.findViewById(a.f.room_gift_view_gift_icon)).setImageBitmap(bitmap);
        }
        ((TextView) this.playGiftView.findViewById(a.f.room_gift_view_gift_text)).setText(eVar.A);
        if (this.animationIn == null) {
            this.animationIn = AnimationUtils.loadAnimation(this.mContext, a.C0001a.alpha_in);
        }
        this.animationIn.setAnimationListener(new SimpleAnimatorListener() {
            public void onAnimationEnd(Animation animation) {
                new Handler().postDelayed(LiveGiftView.this.playGiftRun, (long) eVar.j);
            }
        });
        this.animationIn.reset();
        this.playGiftView.setAnimation(this.animationIn);
        showPlayGiftView(true, false);
        this.animationIn.start();
        playGiftSound(eVar);
    }

    private ViewHolder createHolder(View view) {
        ViewHolder viewHolder = new ViewHolder(null);
        viewHolder.mUserHead = (ImageView) view.findViewById(a.f.room_giftmsg_img_usericon);
        viewHolder.mNickname = (TextView) view.findViewById(a.f.room_giftmsg_message_nickname);
        viewHolder.mNickname.setTextColor(this.mContext.getResources().getColor(a.c.white_ff));
        viewHolder.mNicknameRight = (TextView) view.findViewById(a.f.room_giftmsg_message_nickname_right);
        viewHolder.mNicknameRight.setTextColor(this.mContext.getResources().getColor(a.c.white_ff));
        viewHolder.mTextMid = (TextView) view.findViewById(a.f.room_giftmsg_message_nickname_mid);
        viewHolder.mTextMid.setTextColor(this.mContext.getResources().getColor(a.c.white_ff));
        viewHolder.mTextEnd = (TextView) view.findViewById(a.f.room_giftmsg_message_nickname_end);
        viewHolder.mTextEnd.setTextColor(this.mContext.getResources().getColor(a.c.white_ff));
        viewHolder.mMessage = (TextView) view.findViewById(a.f.room_giftmsg_message_message);
        viewHolder.mMessage.setTextColor(this.mContext.getResources().getColor(a.c.white_ff));
        viewHolder.mLevelImage = (ImageView) view.findViewById(a.f.room_giftmsg_message_img_level);
        viewHolder.authIcon = (ImageView) view.findViewById(a.f.room_giftmsg_message_img_auth);
        viewHolder.mUserLevelAnima = (ImageView) view.findViewById(a.f.room_giftmsg_level_animation);
        viewHolder.vipIcon = (ImageView) view.findViewById(a.f.room_giftmsg_vip_icon);
        return viewHolder;
    }

    private void initView(ViewHolder viewHolder) {
        if (viewHolder != null) {
            try {
                viewHolder.mUserHead.setImageResource(a.e.default_ovaled);
            } catch (OutOfMemoryError e) {
                System.gc();
            }
            viewHolder.mUserHead.setVisibility(0);
            viewHolder.mUserHead.setTag(null);
            viewHolder.mNickname.setText("");
            viewHolder.mNicknameRight.setVisibility(8);
            viewHolder.mNicknameRight.setText("");
            viewHolder.mTextMid.setVisibility(8);
            viewHolder.mTextMid.setText("");
            viewHolder.mTextEnd.setVisibility(8);
            viewHolder.mTextEnd.setText("");
            viewHolder.mMessage.setText("");
            viewHolder.mLevelImage.setVisibility(8);
            viewHolder.authIcon.setVisibility(8);
            viewHolder.vipIcon.setVisibility(8);
            viewHolder.mUserLevelAnima.setVisibility(8);
        }
    }

    private static class ViewHolder {
        public ImageView authIcon;
        public ImageView mLevelImage;
        public TextView mMessage;
        public TextView mNickname;
        public TextView mNicknameRight;
        public TextView mTextEnd;
        public TextView mTextMid;
        public ImageView mUserHead;
        public ImageView mUserLevelAnima;
        public ImageView vipIcon;

        private ViewHolder() {
        }

        /* synthetic */ ViewHolder(ViewHolder viewHolder) {
            this();
        }
    }

    private void showMessageView(e eVar, ViewHolder viewHolder) {
        String str = eVar.q.f615a;
        String str2 = "";
        if (eVar.q != null) {
            if (isMe(str)) {
                str = this.mContext.getResources().getString(a.h.room_me);
            } else {
                str = eVar.q.a();
            }
            ImageLoaderUtil.getInstance().a(eVar.q.a(User.FACE_SIZE.SIM), viewHolder.mUserHead, this.userIconOptions);
            str2 = "送给";
            if (eVar.r != null) {
                if (isMe(eVar.r.f615a)) {
                    str2 = String.format("%s %s", str2, this.mContext.getResources().getString(a.h.room_me));
                } else {
                    str2 = String.format("%s %s", str2, eVar.r.a());
                }
            }
            if (eVar != null) {
                switch ($SWITCH_TABLE$com$pocketmusic$kshare$requestobjs$Gift$GiftType()[eVar.O.ordinal()]) {
                    case 3:
                        viewHolder.mNicknameRight.setVisibility(0);
                        viewHolder.mNicknameRight.setText("发出一个喊话");
                        str2 = eVar.h;
                        break;
                    default:
                        showPlayGiftView(false, false);
                        break;
                }
            }
            setAuthIcon(viewHolder, eVar.q);
            setVipIcon(viewHolder, eVar.q);
        }
        viewHolder.mMessage.setText(str2);
        viewHolder.mNickname.setText(str);
        viewHolder.mUserHead.setTag(eVar.q);
    }

    private void setAuthIcon(ViewHolder viewHolder, e.C0014e eVar) {
        if (eVar == null) {
            return;
        }
        if (!TextUtils.isEmpty(eVar.j)) {
            ImageLoaderUtil.displayImageBg(viewHolder.authIcon, eVar.i, this.levelOptions);
            viewHolder.authIcon.setVisibility(0);
            return;
        }
        viewHolder.authIcon.setVisibility(8);
    }

    private void setVipIcon(ViewHolder viewHolder, e.C0014e eVar) {
        if (eVar == null) {
            return;
        }
        if (TextUtils.isEmpty(eVar.m)) {
            setVipIcon(viewHolder, eVar.f615a);
        } else if (eVar.m.equalsIgnoreCase("vip")) {
            viewHolder.vipIcon.setVisibility(0);
            viewHolder.vipIcon.setImageResource(a.e.vip_icon);
        }
    }

    private void setVipIcon(ViewHolder viewHolder, String str) {
        if (SimpleLiveRoomActivity.isVipUser(str)) {
            viewHolder.vipIcon.setImageResource(a.e.room_indentity_online);
            viewHolder.vipIcon.setVisibility(0);
            return;
        }
        viewHolder.vipIcon.setVisibility(8);
    }

    private boolean isMe(String str) {
        if (TextUtils.isEmpty(str) || !str.equalsIgnoreCase(Session.getCurrentAccount().b)) {
            return false;
        }
        return true;
    }

    private void doHanHuaGiftView(final e eVar) {
        if (this.mHanHuaViewHander == null) {
            this.mHanHuaViewHander = createHolder(this.playGiftViewShowMessage);
        }
        initView(this.mHanHuaViewHander);
        showMessageView(eVar, this.mHanHuaViewHander);
        if (this.animationIn == null) {
            this.animationIn = AnimationUtils.loadAnimation(this.mContext, a.C0001a.alpha_in);
        }
        this.animationIn.setAnimationListener(new SimpleAnimatorListener() {
            public void onAnimationEnd(Animation animation) {
                new Handler().postDelayed(LiveGiftView.this.playGiftRun, (long) eVar.j);
            }
        });
        this.animationIn.reset();
        this.playGiftView.setAnimation(this.animationIn);
        showPlayGiftView(true, true);
        this.animationIn.start();
    }

    /* access modifiers changed from: private */
    public void playGiftView(e eVar, Bitmap bitmap) {
        switch ($SWITCH_TABLE$com$pocketmusic$kshare$requestobjs$Gift$GiftType()[eVar.O.ordinal()]) {
            case 3:
                doHanHuaGiftView(eVar);
                return;
            default:
                doNormalGiftView(eVar, bitmap);
                return;
        }
    }

    /* access modifiers changed from: private */
    public void playGiftView() {
        final e remove = this.gifts.remove(0);
        switch ($SWITCH_TABLE$com$pocketmusic$kshare$requestobjs$Gift$GiftType()[remove.O.ordinal()]) {
            case 3:
                playGiftView(remove, null);
                return;
            default:
                ImageLoaderUtil.getInstance().a(remove.e, this.options, new n() {
                    public void onLoadingComplete(String str, View view, Bitmap bitmap) {
                        if (bitmap != null) {
                            LiveGiftView.this.playGiftView(remove, bitmap);
                        } else {
                            new Handler().postDelayed(LiveGiftView.this.playGiftRun, 0);
                        }
                    }

                    public void onLoadingCancelled(String str, View view) {
                        new Handler().postDelayed(LiveGiftView.this.playGiftRun, 0);
                    }

                    public void onLoadingFailed(String str, View view, com.d.a.b.a.c cVar) {
                        new Handler().postDelayed(LiveGiftView.this.playGiftRun, 0);
                    }
                });
                return;
        }
    }

    public void playGiftAnim(j jVar, final e eVar, final GiftMessage giftMessage) {
        if ("1".equalsIgnoreCase(jVar.e) || eVar.O == e.c.HanHua) {
            try {
                if (((long) Integer.valueOf(eVar.c).intValue()) >= jVar.h || eVar.O == e.c.HanHua) {
                    e a2 = eVar.a();
                    a2.q = e.a(giftMessage.mFrom);
                    a2.A = a2.q.c;
                    if ("1".equalsIgnoreCase(jVar.e)) {
                        a2.j = jVar.g;
                    } else {
                        a2.j = eVar.j;
                    }
                    a2.h = giftMessage.mText;
                    if (this.gifts == null) {
                        this.gifts = new ArrayList<>();
                    }
                    this.gifts.add(a2);
                    if (this.playGiftRun == null) {
                        this.playGiftRun = new PlayGiftRun();
                    }
                    if (!this.playGiftRun.running) {
                        this.playGiftRun.running = true;
                        new Handler().post(this.playGiftRun);
                    }
                }
            } catch (Exception e) {
            }
        } else {
            ULog.d(TAG, "play gift anim: " + eVar.e);
            ImageLoaderUtil.getInstance().a(eVar.e, this.options, new n() {
                public void onLoadingComplete(String str, View view, Bitmap bitmap) {
                    if (bitmap != null) {
                        ULog.d(LiveGiftView.TAG, "playGiftAnim: " + giftMessage.mFrom.getFullName());
                        LiveGiftView.this.playGiftAnim(eVar, bitmap, giftMessage.mFrom.getFullName());
                        LiveGiftView.this.playGiftSound(eVar);
                    }
                }
            });
        }
    }

    public void playGiftAnim(e eVar, Bitmap bitmap, String str) {
        int i;
        int i2;
        if (eVar.u == 1) {
            ViewGroup viewGroup = this.mRootView;
            playAnimWidth = this.mRootView.getWidth();
            this.playAnimHeight = this.mRootView.getHeight();
            this.flakeView.setBackgroundColor(this.mContext.getResources().getColor(a.c.black));
            this.flakeView.getBackground().setAlpha(150);
            int i3 = playAnimWidth;
            int height = (this.playAnimHeight - eVar.t) - (viewGroup.getHeight() / 4);
            if (i3 <= 0) {
                i3 = playAnimWidth;
            }
            if (height <= 0) {
                height = this.playAnimHeight;
            }
            int nextInt = new Random().nextInt(i3);
            i2 = new Random().nextInt(height);
            if (nextInt > playAnimWidth - eVar.s) {
                i = playAnimWidth - eVar.s;
            } else {
                i = nextInt;
            }
            this.flakeView.addToSecondLayerFlake(Flake.createFlake((float) i, (float) i2, eVar, bitmap, String.valueOf(str) + "赠送", (long) eVar.j));
        } else {
            ViewGroup viewGroup2 = this.mRootView;
            playAnimWidth = this.mRootView.getWidth();
            this.playAnimHeight = this.mRootView.getHeight();
            int nextInt2 = new Random().nextInt(playAnimWidth);
            int nextInt3 = new Random().nextInt((this.playAnimHeight * 3) / 4) + (viewGroup2.getHeight() / 4);
            if (nextInt2 > playAnimWidth - eVar.s) {
                nextInt2 = playAnimWidth - eVar.s;
            }
            if (nextInt3 > (this.playAnimHeight - eVar.t) - (viewGroup2.getHeight() / 4)) {
                i2 = (this.playAnimHeight - eVar.t) - (viewGroup2.getHeight() / 4);
            } else {
                i2 = nextInt3;
            }
            this.flakeView.addFlake(Flake.createFlake((float) i, (float) i2, eVar, bitmap, String.valueOf(str) + "赠送", (long) eVar.j));
        }
        ULog.d(TAG, "xRange = " + i + "yRange = " + i2);
    }

    /* access modifiers changed from: private */
    public void playGiftSound(e eVar) {
        if (this.mSoundUtil == null) {
            this.mSoundUtil = new SoundUtil(this.mContext);
        }
        if (eVar == null || TextUtils.isEmpty(eVar.x) || !PreferencesUtils.loadPrefBoolean(this.mContext, SimpleLiveRoomActivity.PRE_KEY_SHOW_VOICE_GIFT, true)) {
        }
    }

    public void cleanGifts() {
        if (this.gifts != null) {
            this.gifts.clear();
        }
    }
}
