package cn.banshenggua.aichang.room;

import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.view.View;
import java.util.ArrayList;

public class FlakeView extends View {
    private String TAG = FlakeView.class.getName();
    ValueAnimator animator = ValueAnimator.ofFloat(0.0f, 1.0f);
    ArrayList<Flake> flakes = new ArrayList<>();
    float fps = 0.0f;
    String fpsString = "";
    Matrix m = new Matrix();
    private int maxTextSize = 30;
    String numFlakesString = "";
    ArrayList<Flake> secondLayerflakes = new ArrayList<>();
    Paint textPaint = new Paint(1);

    public FlakeView(Context context) {
        super(context);
        this.textPaint.setColor(-1);
        this.textPaint.setTextSize(24.0f);
        this.animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                FlakeView.this.invalidate();
            }
        });
        this.animator.setRepeatCount(-1);
        this.animator.setDuration(3000L);
    }

    public void addFlake(Flake flake) {
        this.flakes.add(flake);
    }

    public void addToSecondLayerFlake(Flake flake) {
        this.secondLayerflakes.add(flake);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        this.flakes.clear();
        this.animator.cancel();
        this.animator.start();
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        long currentTimeMillis = System.currentTimeMillis();
        drawLayer(this.flakes, canvas, currentTimeMillis, false);
        drawLayer(this.secondLayerflakes, canvas, currentTimeMillis, true);
    }

    private void drawLayer(ArrayList<Flake> arrayList, Canvas canvas, long j, boolean z) {
        int i = 0;
        while (i < arrayList.size()) {
            Flake flake = arrayList.get(i);
            flake.scaleValue += flake.scaleSpeed;
            if (flake.scaleValue > flake.scaleMaxValue && flake.scaleSpeed > 0.0f) {
                flake.startNoScaleTime = j;
                flake.scaleSpeed = 0.0f;
                flake.scaleValue = flake.scaleMaxValue;
            }
            if (flake.startNoScaleTime > 0 && j - flake.startNoScaleTime >= flake.noScaleTotalTime) {
                flake.scaleSpeed = -flake.SCALE_SPEED_VALULE;
            }
            if (flake.scaleValue <= 0.0f) {
                flake.bitmap = null;
                arrayList.remove(i);
                if (z && arrayList.size() == 0) {
                    setBackgroundDrawable(null);
                }
            } else {
                i++;
                if (z) {
                    this.textPaint.setTextSize(((float) this.maxTextSize) * flake.scaleValue);
                    float measureText = this.textPaint.measureText(flake.title);
                    float height = (((float) (flake.bitmap.getHeight() / 2)) * Math.abs(flake.scaleValue)) + ((float) ((flake.bitmap.getHeight() / 2) + 20));
                    float width = (flake.x + ((float) (flake.bitmap.getWidth() / 2))) - (measureText / 2.0f);
                    if (width < 0.0f) {
                        width = 0.0f;
                    }
                    if (width > ((float) LiveGiftView.playAnimWidth) - measureText) {
                        width = ((float) LiveGiftView.playAnimWidth) - measureText;
                    }
                    canvas.drawText(flake.title, width, height + flake.y, this.textPaint);
                }
                this.m.setTranslate(flake.x, flake.y);
                this.m.preScale(flake.scaleValue, flake.scaleValue, (float) (flake.bitmap.getWidth() / 2), (float) (flake.bitmap.getHeight() / 2));
                canvas.drawBitmap(flake.bitmap, this.m, null);
            }
        }
    }

    public void pause() {
        this.animator.cancel();
    }

    public void resume() {
        this.animator.start();
    }
}
