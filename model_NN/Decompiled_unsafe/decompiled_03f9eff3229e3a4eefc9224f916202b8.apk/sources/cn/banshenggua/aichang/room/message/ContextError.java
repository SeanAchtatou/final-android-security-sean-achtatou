package cn.banshenggua.aichang.room.message;

import cn.banshenggua.aichang.utils.Constants;
import com.qq.e.comm.constants.ErrorCode;
import java.util.HashMap;
import java.util.Map;

public class ContextError {
    public static final int CHAT_FREQUENCY = 541;
    public static final int E_ALREADY_JOIN_MIC = 553;
    public static final int E_PARAM_MISSING = 550;
    public static final int E_USER_NOT_ON_MIC = 552;
    public static final int E_WRONG_ROOM_TYPE = 551;
    public static final int NoError = 0;
    public static final String REQ_MIC_TWICE_MSG = "不能重复排麦";
    public static final int REQ_MIC_Twice = 529;
    public static final int Room_MUTED = 533;
    public static final int Room_PassWord = 509;
    public static final int Server_Closed = 534;
    public static final int SocketClosed = 100404;
    public static final int SocketException = 100405;
    public static final int User_Join_Twice = 535;
    public static final int User_MUTED = 503;
    public static final Map<Integer, String> errorDict = new HashMap();

    static {
        errorDict.put(Integer.valueOf((int) SocketException), "网络异常");
        errorDict.put(Integer.valueOf((int) SocketClosed), "网络异常(关闭)");
        errorDict.put(500, "校验码错误");
        errorDict.put(501, "该房间不存在");
        errorDict.put(Integer.valueOf((int) ErrorCode.AdError.JSON_PARSE_ERROR), "该用户已被拉黑");
        errorDict.put(503, "管理员不让你玩了，一会儿再来吧。");
        errorDict.put(504, "进入房间失败");
        errorDict.put(Integer.valueOf((int) ErrorCode.AdError.RETRY_NO_FILL_ERROR), "该用户不存在");
        errorDict.put(Integer.valueOf((int) ErrorCode.AdError.RETRY_LOAD_SUCCESS), "没有权利进行此操作");
        errorDict.put(507, "用户不在指定的房间中");
        errorDict.put(508, "该用户不在线");
        errorDict.put(Integer.valueOf((int) Room_PassWord), "您输入的密码错误！");
        errorDict.put(510, "同一用户不能两次进入同一个房间");
        errorDict.put(511, "私聊或者送礼物的时候必须指定对象");
        errorDict.put(512, "内容不能为空");
        errorDict.put(513, "内容太多");
        errorDict.put(514, "房间已满");
        errorDict.put(515, "两次操作不是同一个用户");
        errorDict.put(516, "排麦列表已满");
        errorDict.put(517, "用户已不在排麦列表中");
        errorDict.put(518, "该伴奏信息有误，请重新下载");
        errorDict.put(519, "排麦列表为空");
        errorDict.put(520, "该礼物已经不存在了");
        errorDict.put(521, "爱币余额不足");
        errorDict.put(522, "写入礼物记录失败");
        errorDict.put(523, "不允许匿名用户使用");
        errorDict.put(524, "麦上用户不允许更改伴奏");
        errorDict.put(525, "管理员不允许排麦");
        errorDict.put(526, "管理员不在，不能点歌");
        errorDict.put(527, "礼物数不正确");
        errorDict.put(528, "管理员调整mic的位置不正确");
        errorDict.put(529, "唱完才能再点歌");
        errorDict.put(530, "系统错误");
        errorDict.put(531, "不认识的媒体类型");
        errorDict.put(532, "下麦异常（micid为空）");
        errorDict.put(Integer.valueOf((int) Room_MUTED), "房间被封，无法进入");
        errorDict.put(Integer.valueOf((int) Server_Closed), "后台服务繁忙，请稍后再试");
        errorDict.put(Integer.valueOf((int) User_Join_Twice), "您的账号在其他设备登录了");
        errorDict.put(Integer.valueOf((int) CHAT_FREQUENCY), "聊天频率太快，等等再发吧");
        errorDict.put(544, "用户等级太低不允许发言");
        errorDict.put(542, "未知的媒体类型，现在仅支持audio和video");
        errorDict.put(543, "不允许排视频麦");
        errorDict.put(Integer.valueOf((int) ErrorCode.OtherError.GET_PARAS_FROM_JS_ERROR), "我在TA的黑名单里面");
        errorDict.put(Integer.valueOf((int) ErrorCode.OtherError.GET_PARAS_FROM_NATIVE_ERROR), "TA在我的黑名单里面");
        errorDict.put(Integer.valueOf((int) E_PARAM_MISSING), "必填的参数没有");
        errorDict.put(Integer.valueOf((int) E_WRONG_ROOM_TYPE), "房间类型不正确");
        errorDict.put(Integer.valueOf((int) E_USER_NOT_ON_MIC), "用户不在麦上");
        errorDict.put(Integer.valueOf((int) E_ALREADY_JOIN_MIC), "连麦中，不允许再次连麦");
        errorDict.put(Integer.valueOf((int) E_PARAM_MISSING), "参数错误");
        errorDict.put(Integer.valueOf((int) E_WRONG_ROOM_TYPE), "房间类型不正确");
        errorDict.put(Integer.valueOf((int) E_USER_NOT_ON_MIC), "用户不在麦上");
        errorDict.put(Integer.valueOf((int) E_ALREADY_JOIN_MIC), "连麦中,不允许再次连麦");
        errorDict.put(599, "网络异常");
        errorDict.put(Integer.valueOf((int) Constants.CLEARIMGED), "内部错误");
    }

    public static String getErrorString(int i) {
        if (i == 0) {
            return "";
        }
        String str = errorDict.get(Integer.valueOf(i));
        if (str == null) {
            return "未知错误, " + i;
        }
        return str;
    }
}
