package cn.banshenggua.aichang.room.message;

import android.text.TextUtils;
import cn.banshenggua.aichang.room.gift.GiftUtils;
import com.pocketmusic.kshare.requestobjs.e;
import com.pocketmusic.kshare.requestobjs.o;
import com.sina.weibo.sdk.constant.WBPageConstants;
import org.json.JSONObject;

public class GiftMessage extends LiveMessage {
    private static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$room$message$GiftMessage$GiftMessageType;
    public String content = "";
    public String count = "";
    public String gift = "";
    public String[] mCustoms = null;
    public User mFrom = null;
    public e.c mGiftType = e.c.Normal;
    public String mGlobal = "0";
    public String mName = "";
    public String mText = "";
    public User mTo = null;
    public GiftMessageType mType = GiftMessageType.Message_Gift;
    public String toUid = "";
    public String uid = "";

    public enum GiftMessageType {
        Message_Gift,
        Message_GlobalGift
    }

    static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$room$message$GiftMessage$GiftMessageType() {
        int[] iArr = $SWITCH_TABLE$cn$banshenggua$aichang$room$message$GiftMessage$GiftMessageType;
        if (iArr == null) {
            iArr = new int[GiftMessageType.values().length];
            try {
                iArr[GiftMessageType.Message_Gift.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[GiftMessageType.Message_GlobalGift.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            $SWITCH_TABLE$cn$banshenggua$aichang$room$message$GiftMessage$GiftMessageType = iArr;
        }
        return iArr;
    }

    public GiftMessage(GiftMessageType giftMessageType, o oVar) {
        super(oVar);
        this.mType = giftMessageType;
        switch ($SWITCH_TABLE$cn$banshenggua$aichang$room$message$GiftMessage$GiftMessageType()[this.mType.ordinal()]) {
            case 1:
                this.mKey = MessageKey.Message_Gift;
                return;
            case 2:
                this.mKey = MessageKey.Message_GlobalGift;
                return;
            default:
                return;
        }
    }

    public SocketMessage getSocketMessage() {
        SocketMessage socketMessage = super.getSocketMessage();
        if (this.mUser != null) {
            switch ($SWITCH_TABLE$cn$banshenggua$aichang$room$message$GiftMessage$GiftMessageType()[this.mType.ordinal()]) {
                case 1:
                    if (!TextUtils.isEmpty(this.toUid)) {
                        socketMessage.pushCommend("touid", this.toUid);
                    }
                    if (!TextUtils.isEmpty(this.gift)) {
                        socketMessage.pushCommend("gift", this.gift);
                    }
                    if (!TextUtils.isEmpty(this.count)) {
                        socketMessage.pushCommend(WBPageConstants.ParamKey.COUNT, this.count);
                    }
                    if (!TextUtils.isEmpty(this.content)) {
                        socketMessage.pushCommend("content", this.content);
                        break;
                    }
                    break;
            }
        }
        return socketMessage;
    }

    public void parseOut(JSONObject jSONObject) {
        super.parseOut(jSONObject);
        if (jSONObject != null) {
            this.gift = jSONObject.optString("gid", "");
            this.uid = jSONObject.optString("uid", "");
            this.toUid = jSONObject.optString("touid", "");
            if (jSONObject.has("from")) {
                this.mFrom = new User();
                this.mFrom.parseUser(jSONObject.optJSONObject("from"));
            }
            if (jSONObject.has("to")) {
                this.mTo = new User();
                this.mTo.parseUser(jSONObject.optJSONObject("to"));
            }
            this.content = jSONObject.optString("content", "0");
            this.mText = jSONObject.optString("text", "");
            this.mGiftType = e.c.a(jSONObject.optString("subtype"));
            this.mName = jSONObject.optString("giftname");
            if (jSONObject.has("custom")) {
                this.mCustoms = jSONObject.optString("custom", "").split("\n");
            }
            this.mGlobal = jSONObject.optString("global", "0");
        }
    }

    public e getGift() {
        e gift2 = GiftUtils.getGift(this.gift);
        if (gift2 != null) {
            return gift2;
        }
        e eVar = new e();
        eVar.b = this.gift;
        eVar.O = this.mGiftType;
        eVar.d = this.mName;
        return eVar;
    }
}
