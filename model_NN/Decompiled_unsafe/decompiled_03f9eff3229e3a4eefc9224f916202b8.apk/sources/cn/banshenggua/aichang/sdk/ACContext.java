package cn.banshenggua.aichang.sdk;

import android.app.Application;
import android.support.v4.app.Fragment;
import cn.banshenggua.aichang.app.KShareApplication;
import cn.banshenggua.aichang.entry.RoomsEntryFragment;

public class ACContext {
    private static String APP_URL = "http://www.aichang.cn/download/aichang.apk";
    private static Application mInstanse;

    public static void initContext(Application application) throws ACException {
        mInstanse = application;
        KShareApplication.createInstance(mInstanse);
    }

    public static Fragment getRoomsEntryFragment() {
        return RoomsEntryFragment.newInstance();
    }

    public static void checkAndDownloadLib() throws ACException {
        KShareApplication.getInstance().checkLiveLib(null);
    }

    public static void setAppurl(String str) {
        APP_URL = str;
    }

    public static String getAppUrl() {
        return APP_URL;
    }
}
