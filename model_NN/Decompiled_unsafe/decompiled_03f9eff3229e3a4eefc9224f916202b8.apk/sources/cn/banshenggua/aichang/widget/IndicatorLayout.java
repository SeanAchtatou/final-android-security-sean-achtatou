package cn.banshenggua.aichang.widget;

import android.content.Context;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import cn.a.a.a;
import cn.banshenggua.aichang.widget.PullToRefreshBase;

public class IndicatorLayout extends FrameLayout implements Animation.AnimationListener {
    private static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$widget$PullToRefreshBase$Mode = null;
    static final int DEFAULT_ROTATION_ANIMATION_DURATION = 150;
    private ImageView mArrowImageView;
    private Animation mInAnim;
    private Animation mOutAnim;
    private final Animation mResetRotateAnimation;
    private final Animation mRotateAnimation;

    static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$widget$PullToRefreshBase$Mode() {
        int[] iArr = $SWITCH_TABLE$cn$banshenggua$aichang$widget$PullToRefreshBase$Mode;
        if (iArr == null) {
            iArr = new int[PullToRefreshBase.Mode.values().length];
            try {
                iArr[PullToRefreshBase.Mode.BOTH.ordinal()] = 3;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[PullToRefreshBase.Mode.NO_BOTH.ordinal()] = 4;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[PullToRefreshBase.Mode.PULL_DOWN_TO_REFRESH.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[PullToRefreshBase.Mode.PULL_UP_TO_REFRESH.ordinal()] = 2;
            } catch (NoSuchFieldError e4) {
            }
            $SWITCH_TABLE$cn$banshenggua$aichang$widget$PullToRefreshBase$Mode = iArr;
        }
        return iArr;
    }

    public IndicatorLayout(Context context, PullToRefreshBase.Mode mode) {
        super(context);
        int i;
        int i2;
        this.mArrowImageView = new ImageView(context);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-2, -2, 17);
        int dimensionPixelSize = getResources().getDimensionPixelSize(a.d.indicator_internal_padding);
        layoutParams.rightMargin = dimensionPixelSize;
        layoutParams.leftMargin = dimensionPixelSize;
        layoutParams.bottomMargin = dimensionPixelSize;
        layoutParams.topMargin = dimensionPixelSize;
        addView(this.mArrowImageView, layoutParams);
        switch ($SWITCH_TABLE$cn$banshenggua$aichang$widget$PullToRefreshBase$Mode()[mode.ordinal()]) {
            case 2:
                i = a.C0001a.slide_in_from_bottom;
                i2 = a.C0001a.slide_out_to_bottom;
                setBackgroundResource(a.e.indicator_bg_bottom);
                this.mArrowImageView.setImageResource(a.e.pulltorefresh_up_arrow);
                break;
            default:
                i = a.C0001a.slide_in_from_top;
                i2 = a.C0001a.slide_out_to_top;
                setBackgroundResource(a.e.indicator_bg_top);
                this.mArrowImageView.setImageResource(a.e.pulltorefresh_down_arrow);
                break;
        }
        this.mInAnim = AnimationUtils.loadAnimation(context, i);
        this.mInAnim.setAnimationListener(this);
        this.mOutAnim = AnimationUtils.loadAnimation(context, i2);
        this.mOutAnim.setAnimationListener(this);
        LinearInterpolator linearInterpolator = new LinearInterpolator();
        this.mRotateAnimation = new RotateAnimation(0.0f, -180.0f, 1, 0.5f, 1, 0.5f);
        this.mRotateAnimation.setInterpolator(linearInterpolator);
        this.mRotateAnimation.setDuration(150);
        this.mRotateAnimation.setFillAfter(true);
        this.mResetRotateAnimation = new RotateAnimation(-180.0f, 0.0f, 1, 0.5f, 1, 0.5f);
        this.mResetRotateAnimation.setInterpolator(linearInterpolator);
        this.mResetRotateAnimation.setDuration(150);
        this.mResetRotateAnimation.setFillAfter(true);
    }

    public final boolean isVisible() {
        Animation animation = getAnimation();
        if (animation != null) {
            if (this.mInAnim == animation) {
                return true;
            }
            return false;
        } else if (getVisibility() != 0) {
            return false;
        } else {
            return true;
        }
    }

    public void hide() {
        startAnimation(this.mOutAnim);
    }

    public void show() {
        startAnimation(this.mInAnim);
    }

    public void onAnimationEnd(Animation animation) {
        if (animation == this.mOutAnim) {
            this.mArrowImageView.clearAnimation();
            setVisibility(8);
        } else if (animation == this.mInAnim) {
            setVisibility(0);
        }
        clearAnimation();
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationStart(Animation animation) {
        setVisibility(0);
    }

    public void releaseToRefresh() {
        this.mArrowImageView.startAnimation(this.mRotateAnimation);
    }

    public void pullToRefresh() {
        this.mArrowImageView.startAnimation(this.mResetRotateAnimation);
    }
}
