package cn.banshenggua.aichang.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.v4.view.ViewCompat;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import cn.a.a.a;
import cn.banshenggua.aichang.widget.PullToRefreshBase;

public class LoadingLayout extends FrameLayout {
    private static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$widget$PullToRefreshBase$Mode = null;
    static final int DEFAULT_ROTATION_ANIMATION_DURATION = 100;
    private ProgressBar headerProgress;
    private final ImageView mHeaderImage;
    private final TextView mHeaderText;
    private String mPullLabel;
    private String mRefreshingLabel;
    private String mReleaseLabel;
    private final TextView mSubHeaderText;
    private final Animation resetRotateAnimation;
    private final Animation rotateAnimation = new RotateAnimation(0.0f, -180.0f, 1, 0.5f, 1, 0.5f);

    static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$widget$PullToRefreshBase$Mode() {
        int[] iArr = $SWITCH_TABLE$cn$banshenggua$aichang$widget$PullToRefreshBase$Mode;
        if (iArr == null) {
            iArr = new int[PullToRefreshBase.Mode.values().length];
            try {
                iArr[PullToRefreshBase.Mode.BOTH.ordinal()] = 3;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[PullToRefreshBase.Mode.NO_BOTH.ordinal()] = 4;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[PullToRefreshBase.Mode.PULL_DOWN_TO_REFRESH.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[PullToRefreshBase.Mode.PULL_UP_TO_REFRESH.ordinal()] = 2;
            } catch (NoSuchFieldError e4) {
            }
            $SWITCH_TABLE$cn$banshenggua$aichang$widget$PullToRefreshBase$Mode = iArr;
        }
        return iArr;
    }

    public LoadingLayout(Context context, PullToRefreshBase.Mode mode, TypedArray typedArray) {
        super(context);
        Drawable drawable;
        ViewGroup viewGroup = (ViewGroup) LayoutInflater.from(context).inflate(a.g.pull_to_refresh_header, this);
        this.mHeaderText = (TextView) viewGroup.findViewById(a.f.pull_to_refresh_text);
        this.mSubHeaderText = (TextView) viewGroup.findViewById(a.f.pull_to_refresh_sub_text);
        this.mHeaderImage = (ImageView) viewGroup.findViewById(a.f.pull_to_refresh_image);
        this.headerProgress = (ProgressBar) viewGroup.findViewById(a.f.pull_to_refresh_progress);
        LinearInterpolator linearInterpolator = new LinearInterpolator();
        this.rotateAnimation.setInterpolator(linearInterpolator);
        this.rotateAnimation.setDuration(100);
        this.rotateAnimation.setFillAfter(true);
        this.resetRotateAnimation = new RotateAnimation(-180.0f, 0.0f, 1, 0.5f, 1, 0.5f);
        this.resetRotateAnimation.setInterpolator(linearInterpolator);
        this.resetRotateAnimation.setDuration(100);
        this.resetRotateAnimation.setFillAfter(true);
        switch ($SWITCH_TABLE$cn$banshenggua$aichang$widget$PullToRefreshBase$Mode()[mode.ordinal()]) {
            case 2:
                this.mPullLabel = context.getString(a.h.pull_to_refresh_from_bottom_pull_label);
                this.mRefreshingLabel = context.getString(a.h.pull_to_refresh_from_bottom_refreshing_label);
                this.mReleaseLabel = context.getString(a.h.pull_to_refresh_from_bottom_release_label);
                this.mHeaderImage.setImageResource(a.e.pulltorefresh_up_arrow);
                break;
            default:
                this.mPullLabel = context.getString(a.h.pull_to_refresh_pull_label);
                this.mRefreshingLabel = context.getString(a.h.pull_to_refresh_refreshing_label);
                this.mReleaseLabel = context.getString(a.h.pull_to_refresh_release_label);
                this.mHeaderImage.setImageResource(a.e.pulltorefresh_down_arrow);
                break;
        }
        if (typedArray.hasValue(2)) {
            ColorStateList colorStateList = typedArray.getColorStateList(2);
            setTextColor(colorStateList == null ? ColorStateList.valueOf(ViewCompat.MEASURED_STATE_MASK) : colorStateList);
        }
        if (typedArray.hasValue(3)) {
            ColorStateList colorStateList2 = typedArray.getColorStateList(3);
            setSubTextColor(colorStateList2 == null ? ColorStateList.valueOf(ViewCompat.MEASURED_STATE_MASK) : colorStateList2);
        }
        if (typedArray.hasValue(1) && (drawable = typedArray.getDrawable(1)) != null) {
            setBackgroundDrawable(drawable);
        }
    }

    public void reset() {
        this.mHeaderText.setText(Html.fromHtml(this.mPullLabel));
        this.mHeaderImage.setVisibility(0);
        this.headerProgress.setVisibility(8);
        if (TextUtils.isEmpty(this.mSubHeaderText.getText())) {
            this.mSubHeaderText.setVisibility(8);
        } else {
            this.mSubHeaderText.setVisibility(0);
        }
    }

    public void releaseToRefresh() {
        this.mHeaderText.setText(Html.fromHtml(this.mReleaseLabel));
        this.mHeaderImage.clearAnimation();
        this.mHeaderImage.startAnimation(this.rotateAnimation);
    }

    public void setPullLabel(String str) {
        this.mPullLabel = str;
    }

    public void refreshing() {
        this.mHeaderText.setText(Html.fromHtml(this.mRefreshingLabel));
        this.mHeaderImage.clearAnimation();
        this.mHeaderImage.setVisibility(4);
        this.headerProgress.setVisibility(0);
    }

    public void setRefreshingLabel(String str) {
        this.mRefreshingLabel = str;
    }

    public void setReleaseLabel(String str) {
        this.mReleaseLabel = str;
    }

    public void pullToRefresh() {
        this.mHeaderText.setText(Html.fromHtml(this.mPullLabel));
        this.mHeaderImage.clearAnimation();
        this.mHeaderImage.startAnimation(this.resetRotateAnimation);
    }

    public void setTextColor(ColorStateList colorStateList) {
        this.mHeaderText.setTextColor(colorStateList);
        this.mSubHeaderText.setTextColor(colorStateList);
    }

    public void setSubTextColor(ColorStateList colorStateList) {
        this.mSubHeaderText.setTextColor(colorStateList);
    }

    public void setTextColor(int i) {
        setTextColor(ColorStateList.valueOf(i));
    }

    public void setSubTextColor(int i) {
        setSubTextColor(ColorStateList.valueOf(i));
    }

    public void setSubHeaderText(CharSequence charSequence) {
        if (TextUtils.isEmpty(charSequence)) {
            this.mSubHeaderText.setVisibility(8);
            return;
        }
        this.mSubHeaderText.setText(charSequence);
        this.mSubHeaderText.setVisibility(0);
    }
}
