package cn.banshenggua.aichang.widget;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.widget.ImageView;
import com.d.a.b.a.i;
import com.d.a.b.c.a;

public class OvaledBitmapDisplayer implements a {
    public void display(Bitmap bitmap, com.d.a.b.e.a aVar, i iVar) {
        ImageView imageView = (ImageView) aVar.d();
        try {
            bitmap = getOvaledCornerBitmap(bitmap);
        } catch (OutOfMemoryError e) {
        }
        imageView.setImageBitmap(bitmap);
    }

    private Bitmap getOvaledCornerBitmap(Bitmap bitmap) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        if (width > height) {
            width = height;
        } else {
            height = width;
        }
        Bitmap createBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        Paint paint = new Paint();
        Rect rect = new Rect(0, 0, width, height);
        RectF rectF = new RectF(rect);
        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(-1);
        canvas.drawOval(rectF, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        return createBitmap;
    }
}
