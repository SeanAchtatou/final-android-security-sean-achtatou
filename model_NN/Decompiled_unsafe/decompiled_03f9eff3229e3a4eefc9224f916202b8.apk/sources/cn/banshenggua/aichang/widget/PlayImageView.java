package cn.banshenggua.aichang.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.widget.ImageView;
import cn.banshenggua.aichang.utils.ImageUtil;
import cn.banshenggua.aichang.utils.NetWorkUtil;
import cn.banshenggua.aichang.utils.UIUtil;
import cn.banshenggua.aichang.utils.ULog;
import com.d.a.b.a.e;
import com.d.a.b.c;
import com.d.a.b.d;
import com.pocketmusic.kshare.requestobjs.a;
import com.pocketmusic.kshare.requestobjs.q;
import com.pocketmusic.kshare.requestobjs.v;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class PlayImageView extends ImageView {
    protected static final int ALBUM_FINISHED = 1;
    protected static final int PLAY_PHOTO = 0;
    protected static final String TAG = "PlayImageView";
    private q albumImageListener = new q() {
    };
    private q albumListener = new q() {
    };
    /* access modifiers changed from: private */
    public a albumsAccount;
    Handler hander = new Handler() {
        public void handleMessage(Message message) {
            int i = 0;
            switch (message.what) {
                case 0:
                    String access$0 = PlayImageView.this.getShowImageUrl(message.arg1);
                    if (PlayImageView.this.options == null) {
                        PlayImageView playImageView = PlayImageView.this;
                        if (PlayImageView.this.getWidth() >= PlayImageView.this.getHeight()) {
                            i = 1;
                        }
                        playImageView.options = ImageUtil.getNoDefaultPlayerImageAminOption(i);
                    }
                    PlayImageView.this.imageloader.a(access$0, PlayImageView.this, PlayImageView.this.options);
                    if (PlayImageView.this.isCanDownload()) {
                        PlayImageView.this.downloadPic(message.arg1 + 1);
                        return;
                    }
                    return;
                case 1:
                    if (PlayImageView.this.albumsAccount == null || PlayImageView.this.albumsAccount.u == null) {
                        ULog.d(PlayImageView.TAG, "albumsAccount albums null");
                        return;
                    }
                    PlayImageView.this.totalImageView = PlayImageView.this.albumsAccount.u.size();
                    PlayImageView.this.downloadPic(0);
                    PlayImageView.this.startTimer();
                    return;
                default:
                    return;
            }
        }
    };
    /* access modifiers changed from: private */
    public d imageloader = d.a();
    private List<String> list;
    private boolean mCanDownload = true;
    /* access modifiers changed from: private */
    public int mReadFileIndex = 0;
    /* access modifiers changed from: private */
    public c options;
    private Timer timer;
    /* access modifiers changed from: private */
    public int totalImageView = 0;

    public boolean isCanDownload() {
        return this.mCanDownload;
    }

    public void setCanDownload(boolean z) {
        this.mCanDownload = z;
    }

    public a getAlbumsAccount() {
        return this.albumsAccount;
    }

    public PlayImageView(Context context) {
        super(context);
    }

    public PlayImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public PlayImageView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    private void initImageLoader() {
        if (this.imageloader != null) {
            this.imageloader.a(ImageUtil.getScreenDipConfig());
        }
    }

    public void initData(String str) {
        cancelTimer();
        this.mCanDownload = true;
        this.albumsAccount = new a();
        this.albumsAccount.b = str;
        this.albumsAccount.a(this.albumListener);
        this.albumsAccount.b();
    }

    public void initData(a aVar) {
        cancelTimer();
        this.mCanDownload = true;
        setBackgroundDrawable(null);
        setImageBitmap(null);
        this.albumsAccount = aVar;
        this.albumsAccount.a(this.albumListener);
        this.albumsAccount.b();
    }

    /* access modifiers changed from: private */
    public String getShowImageUrl(int i) {
        int size;
        if (this.albumsAccount.u.size() == 0) {
            return null;
        }
        if (NetWorkUtil.networkType != 0 || this.albumsAccount.u.size() <= 3) {
            size = i % this.albumsAccount.u.size();
        } else {
            size = new Random().nextInt(3);
        }
        return this.albumsAccount.u.get(size).f634a;
    }

    /* access modifiers changed from: private */
    public void downloadPic(int i) {
        if (this.albumsAccount.u.size() != 0) {
            this.imageloader.a(this.albumsAccount.u.get(i % this.albumsAccount.u.size()).f634a, (e) null);
        }
    }

    private void getPlayPicByAPI(a aVar) {
        if (aVar.u.size() != 0) {
            for (v next : aVar.u) {
                next.a(this.albumImageListener);
                next.a();
            }
        }
    }

    /* access modifiers changed from: private */
    public void startTimer() {
        if (this.timer == null && this.totalImageView > 0) {
            this.timer = new Timer();
            this.timer.schedule(new TimerTask() {
                public void run() {
                    int i = 0;
                    try {
                        if (PlayImageView.this.hander != null) {
                            if (PlayImageView.this.albumsAccount.u == null || PlayImageView.this.albumsAccount.u.size() == 0) {
                                PlayImageView.this.mReadFileIndex = -1;
                                return;
                            }
                            PlayImageView playImageView = PlayImageView.this;
                            if (PlayImageView.this.mReadFileIndex < PlayImageView.this.albumsAccount.u.size() - 1) {
                                i = PlayImageView.this.mReadFileIndex + 1;
                            }
                            playImageView.mReadFileIndex = i;
                            Message obtainMessage = PlayImageView.this.hander.obtainMessage();
                            ULog.i(PlayImageView.TAG, "readFile:" + PlayImageView.this.mReadFileIndex);
                            obtainMessage.what = 0;
                            obtainMessage.arg1 = PlayImageView.this.mReadFileIndex;
                            PlayImageView.this.hander.sendMessage(obtainMessage);
                        }
                    } catch (Exception e) {
                    }
                }
            }, 0, 5000);
        }
    }

    private Bitmap getImageBitMap(List<String> list2, int i) {
        if (list2 != null && i < list2.size()) {
            return ImageUtil.decodeFile(list2.get(i), UIUtil.getInstance().getmScreenWidth(), UIUtil.getInstance().getmScreenWidth(), ImageUtil.ScalingLogic.CROP);
        }
        return null;
    }

    public void cancelTimer() {
        if (this.timer != null) {
            this.timer.cancel();
            this.timer = null;
        }
    }

    private void handleMessage(int i, float f) {
        Message obtainMessage = this.hander.obtainMessage();
        obtainMessage.what = i;
        obtainMessage.arg1 = (int) f;
        ULog.d(TAG, "downprogress" + obtainMessage.arg1 + "=" + f);
        this.hander.sendMessage(obtainMessage);
    }
}
