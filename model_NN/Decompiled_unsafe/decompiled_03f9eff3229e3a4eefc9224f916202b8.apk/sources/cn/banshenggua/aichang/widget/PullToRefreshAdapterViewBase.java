package cn.banshenggua.aichang.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.ContextMenu;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.AbsListView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import cn.a.a.a;
import cn.banshenggua.aichang.widget.PullToRefreshBase;

public abstract class PullToRefreshAdapterViewBase<T extends AbsListView> extends PullToRefreshBase<T> implements AbsListView.OnScrollListener {
    private static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$widget$PullToRefreshBase$Mode = null;
    static final boolean DEFAULT_SHOW_INDICATOR = true;
    private View mEmptyView;
    private IndicatorLayout mIndicatorIvBottom;
    private IndicatorLayout mIndicatorIvTop;
    private PullToRefreshBase.OnLastItemVisibleListener mOnLastItemVisibleListener;
    private AbsListView.OnScrollListener mOnScrollListener;
    private FrameLayout mRefreshableViewHolder;
    private int mSavedLastVisibleIndex = -1;
    private boolean mShowIndicator;

    public abstract ContextMenu.ContextMenuInfo getContextMenuInfo();

    static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$widget$PullToRefreshBase$Mode() {
        int[] iArr = $SWITCH_TABLE$cn$banshenggua$aichang$widget$PullToRefreshBase$Mode;
        if (iArr == null) {
            iArr = new int[PullToRefreshBase.Mode.values().length];
            try {
                iArr[PullToRefreshBase.Mode.BOTH.ordinal()] = 3;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[PullToRefreshBase.Mode.NO_BOTH.ordinal()] = 4;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[PullToRefreshBase.Mode.PULL_DOWN_TO_REFRESH.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[PullToRefreshBase.Mode.PULL_UP_TO_REFRESH.ordinal()] = 2;
            } catch (NoSuchFieldError e4) {
            }
            $SWITCH_TABLE$cn$banshenggua$aichang$widget$PullToRefreshBase$Mode = iArr;
        }
        return iArr;
    }

    public PullToRefreshAdapterViewBase(Context context) {
        super(context);
        ((AbsListView) this.mRefreshableView).setOnScrollListener(this);
    }

    public PullToRefreshAdapterViewBase(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        ((AbsListView) this.mRefreshableView).setOnScrollListener(this);
    }

    public PullToRefreshAdapterViewBase(Context context, PullToRefreshBase.Mode mode) {
        super(context, mode);
        ((AbsListView) this.mRefreshableView).setOnScrollListener(this);
    }

    public boolean getShowIndicator() {
        return this.mShowIndicator;
    }

    public final void onScroll(AbsListView absListView, int i, int i2, int i3) {
        if (this.mOnLastItemVisibleListener != null) {
            int i4 = i + i2;
            if (i2 > 0 && i4 + 1 == i3 && i4 != this.mSavedLastVisibleIndex) {
                this.mSavedLastVisibleIndex = i4;
                this.mOnLastItemVisibleListener.onLastItemVisible();
            }
        }
        if (getShowIndicatorInternal()) {
            updateIndicatorViewsVisibility();
        }
        if (this.mOnScrollListener != null) {
            this.mOnScrollListener.onScroll(absListView, i, i2, i3);
        }
    }

    public final void onScrollStateChanged(AbsListView absListView, int i) {
        if (this.mOnScrollListener != null) {
            this.mOnScrollListener.onScrollStateChanged(absListView, i);
        }
    }

    public final void setEmptyView(View view) {
        if (this.mEmptyView != null) {
            this.mRefreshableViewHolder.removeView(this.mEmptyView);
        }
        if (view != null) {
            view.setClickable(true);
            ViewParent parent = view.getParent();
            if (parent != null && (parent instanceof ViewGroup)) {
                ((ViewGroup) parent).removeView(view);
            }
            this.mRefreshableViewHolder.addView(view, -1, -1);
            if (this.mRefreshableView instanceof EmptyViewMethodAccessor) {
                ((EmptyViewMethodAccessor) this.mRefreshableView).setEmptyViewInternal(view);
            } else {
                ((AbsListView) this.mRefreshableView).setEmptyView(view);
            }
        }
    }

    public final void setOnLastItemVisibleListener(PullToRefreshBase.OnLastItemVisibleListener onLastItemVisibleListener) {
        this.mOnLastItemVisibleListener = onLastItemVisibleListener;
    }

    public final void setOnScrollListener(AbsListView.OnScrollListener onScrollListener) {
        this.mOnScrollListener = onScrollListener;
    }

    public void setShowIndicator(boolean z) {
        this.mShowIndicator = z;
        if (getShowIndicatorInternal()) {
            addIndicatorViews();
        } else {
            removeIndicatorViews();
        }
    }

    /* access modifiers changed from: protected */
    public void addRefreshableView(Context context, T t) {
        this.mRefreshableViewHolder = new FrameLayout(context);
        this.mRefreshableViewHolder.addView(t, -1, -1);
        addView(this.mRefreshableViewHolder, new LinearLayout.LayoutParams(-1, 0, 1.0f));
    }

    /* access modifiers changed from: protected */
    public int getNumberInternalFooterViews() {
        return 0;
    }

    /* access modifiers changed from: protected */
    public int getNumberInternalHeaderViews() {
        return 0;
    }

    /* access modifiers changed from: protected */
    public int getNumberInternalViews() {
        return getNumberInternalHeaderViews() + getNumberInternalFooterViews();
    }

    /* access modifiers changed from: protected */
    public void handleStyledAttributes(TypedArray typedArray) {
        this.mShowIndicator = typedArray.getBoolean(5, true);
    }

    /* access modifiers changed from: protected */
    public boolean isReadyForPullDown() {
        return isFirstItemVisible();
    }

    /* access modifiers changed from: protected */
    public boolean isReadyForPullUp() {
        return isLastItemVisible();
    }

    /* access modifiers changed from: protected */
    public void onPullToRefresh() {
        super.onPullToRefresh();
        if (getShowIndicatorInternal()) {
            switch ($SWITCH_TABLE$cn$banshenggua$aichang$widget$PullToRefreshBase$Mode()[getCurrentMode().ordinal()]) {
                case 1:
                    this.mIndicatorIvTop.pullToRefresh();
                    return;
                case 2:
                    this.mIndicatorIvBottom.pullToRefresh();
                    return;
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onReleaseToRefresh() {
        super.onReleaseToRefresh();
        if (getShowIndicatorInternal()) {
            switch ($SWITCH_TABLE$cn$banshenggua$aichang$widget$PullToRefreshBase$Mode()[getCurrentMode().ordinal()]) {
                case 1:
                    this.mIndicatorIvTop.releaseToRefresh();
                    return;
                case 2:
                    this.mIndicatorIvBottom.releaseToRefresh();
                    return;
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void resetHeader() {
        super.resetHeader();
        if (getShowIndicatorInternal()) {
            updateIndicatorViewsVisibility();
        }
    }

    /* access modifiers changed from: protected */
    public void setRefreshingInternal(boolean z) {
        super.setRefreshingInternal(z);
        if (getShowIndicatorInternal()) {
            updateIndicatorViewsVisibility();
        }
    }

    /* access modifiers changed from: protected */
    public void updateUIForMode() {
        super.updateUIForMode();
        if (getShowIndicatorInternal()) {
            addIndicatorViews();
        }
    }

    private void addIndicatorViews() {
        PullToRefreshBase.Mode mode = getMode();
        if (mode.canPullDown() && this.mIndicatorIvTop == null) {
            this.mIndicatorIvTop = new IndicatorLayout(getContext(), PullToRefreshBase.Mode.PULL_DOWN_TO_REFRESH);
            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-2, -2);
            layoutParams.rightMargin = getResources().getDimensionPixelSize(a.d.indicator_right_padding);
            layoutParams.gravity = 53;
            this.mRefreshableViewHolder.addView(this.mIndicatorIvTop, layoutParams);
        } else if (!mode.canPullDown() && this.mIndicatorIvTop != null) {
            this.mRefreshableViewHolder.removeView(this.mIndicatorIvTop);
            this.mIndicatorIvTop = null;
        }
        if (mode.canPullUp() && this.mIndicatorIvBottom == null) {
            this.mIndicatorIvBottom = new IndicatorLayout(getContext(), PullToRefreshBase.Mode.PULL_UP_TO_REFRESH);
            FrameLayout.LayoutParams layoutParams2 = new FrameLayout.LayoutParams(-2, -2);
            layoutParams2.rightMargin = getResources().getDimensionPixelSize(a.d.indicator_right_padding);
            layoutParams2.gravity = 85;
            this.mRefreshableViewHolder.addView(this.mIndicatorIvBottom, layoutParams2);
        } else if (!mode.canPullUp() && this.mIndicatorIvBottom != null) {
            this.mRefreshableViewHolder.removeView(this.mIndicatorIvBottom);
            this.mIndicatorIvBottom = null;
        }
    }

    private boolean getShowIndicatorInternal() {
        return this.mShowIndicator && isPullToRefreshEnabled();
    }

    private boolean isFirstItemVisible() {
        View childAt;
        if (((AbsListView) this.mRefreshableView).getCount() <= getNumberInternalViews()) {
            return true;
        }
        if (((AbsListView) this.mRefreshableView).getFirstVisiblePosition() != 0 || (childAt = ((AbsListView) this.mRefreshableView).getChildAt(0)) == null) {
            return false;
        }
        return childAt.getTop() >= ((AbsListView) this.mRefreshableView).getTop();
    }

    private boolean isLastItemVisible() {
        View childAt;
        int count = ((AbsListView) this.mRefreshableView).getCount();
        int lastVisiblePosition = ((AbsListView) this.mRefreshableView).getLastVisiblePosition();
        if (count <= getNumberInternalViews()) {
            return true;
        }
        if (lastVisiblePosition != count - 1 || (childAt = ((AbsListView) this.mRefreshableView).getChildAt(lastVisiblePosition - ((AbsListView) this.mRefreshableView).getFirstVisiblePosition())) == null) {
            return false;
        }
        return childAt.getBottom() <= ((AbsListView) this.mRefreshableView).getBottom();
    }

    private void removeIndicatorViews() {
        if (this.mIndicatorIvTop != null) {
            this.mRefreshableViewHolder.removeView(this.mIndicatorIvTop);
            this.mIndicatorIvTop = null;
        }
        if (this.mIndicatorIvBottom != null) {
            this.mRefreshableViewHolder.removeView(this.mIndicatorIvBottom);
            this.mIndicatorIvBottom = null;
        }
    }

    private void updateIndicatorViewsVisibility() {
        if (this.mIndicatorIvTop != null) {
            if (isRefreshing() || !isReadyForPullDown()) {
                if (this.mIndicatorIvTop.isVisible()) {
                    this.mIndicatorIvTop.hide();
                }
            } else if (!this.mIndicatorIvTop.isVisible()) {
                this.mIndicatorIvTop.show();
            }
        }
        if (this.mIndicatorIvBottom == null) {
            return;
        }
        if (isRefreshing() || !isReadyForPullUp()) {
            if (this.mIndicatorIvBottom.isVisible()) {
                this.mIndicatorIvBottom.hide();
            }
        } else if (!this.mIndicatorIvBottom.isVisible()) {
            this.mIndicatorIvBottom.show();
        }
    }
}
