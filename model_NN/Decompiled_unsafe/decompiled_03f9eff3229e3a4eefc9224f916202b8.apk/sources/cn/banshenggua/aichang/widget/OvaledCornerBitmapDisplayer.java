package cn.banshenggua.aichang.widget;

import android.graphics.Bitmap;
import android.widget.ImageView;
import cn.banshenggua.aichang.utils.ImageUtil;
import cn.banshenggua.aichang.utils.ULog;
import com.d.a.b.a.i;
import com.d.a.b.c.a;

public class OvaledCornerBitmapDisplayer implements a {
    int mLineWidth = 4;

    public OvaledCornerBitmapDisplayer() {
    }

    public OvaledCornerBitmapDisplayer(int i) {
        this.mLineWidth = i;
    }

    public void display(Bitmap bitmap, com.d.a.b.e.a aVar, i iVar) {
        ImageView imageView = (ImageView) aVar.d();
        try {
            ULog.d("ImageUtil", "imageView: width: " + imageView.getWidth() + "; height: " + imageView.getHeight());
            bitmap = getOvaledCornerBitmap(bitmap, imageView.getWidth(), imageView.getHeight());
        } catch (OutOfMemoryError e) {
        }
        imageView.setImageBitmap(bitmap);
    }

    private Bitmap getOvaledCornerBitmap(Bitmap bitmap, int i, int i2) {
        return ImageUtil.getOvaledCornerBitmap(bitmap, this.mLineWidth, i, i2);
    }
}
