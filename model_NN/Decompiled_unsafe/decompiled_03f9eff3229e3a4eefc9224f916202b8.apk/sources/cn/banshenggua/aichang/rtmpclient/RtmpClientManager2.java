package cn.banshenggua.aichang.rtmpclient;

import cn.banshenggua.aichang.app.KShareApplication;
import cn.banshenggua.aichang.sdk.ACException;
import cn.banshenggua.aichang.utils.FileUtils;
import cn.banshenggua.aichang.utils.Toaster;
import cn.banshenggua.aichang.utils.ULog;
import java.util.ArrayList;
import java.util.List;

public class RtmpClientManager2 implements Runnable {
    public static final int C_INPUT_0 = 2;
    public static final int C_INPUT_1 = 3;
    public static final int C_OUTPUT_0 = 0;
    public static final int C_OUTPUT_1 = 1;
    public static final int C_OUTPUT_2 = 2;
    public static final int C_OUTPUT_3 = 3;
    public static final int STREAM_0 = 0;
    public static final int STREAM_1 = 1;
    public static final int STREAM_2 = 2;
    public static final int STREAM_3 = 3;
    private static RtmpClientManager2 mInstanceManager;
    private boolean isInit = false;
    private boolean isRunning = false;
    List<Client> mClients = new ArrayList();
    private boolean mStop = false;
    Thread mThread = null;

    public enum RTMP_TYPE {
        ONE_STREAM,
        ONE_CLIENT,
        TWO_CLIENT,
        ERROR
    }

    private native void addInputStream(int i, int i2, String str, int i3, int i4, int i5, int i6, int i7, int i8);

    private native void addconnect(int i, String str);

    private native void addstream(int i, int i2, String str, boolean z, int i3);

    private native String getmessage();

    private native void init();

    private native void startconnect(int i);

    private native void stopconnect(int i);

    private native void stopstream(int i, int i2);

    public native void changeInputConfig(int i, int i2, boolean z, int i3, int i4, int i5, int i6, int i7, int i8);

    public native byte[] getdata(int i, int i2, boolean z, long j, VideoSize videoSize);

    public native boolean isConnected(int i, boolean z);

    public native void localpushdata(int i, boolean z, byte[] bArr, int i2, long j, boolean z2);

    public native int pulldata(int i, int i2, byte[] bArr, int i3, boolean z);

    public native DataSizeTime pulldatatime(int i, int i2, byte[] bArr, int i3, boolean z);

    public native void pushdata(int i, int i2, boolean z, byte[] bArr, int i3, long j, boolean z2);

    public native void rungetdata(int i, int i2, boolean z);

    public native void setBuffer(int i, int i2, boolean z, int i3);

    public native void startLocalFile(String str, int i, int i2, int i3, int i4);

    public native void stopLocalFile();

    public static RTMP_TYPE processURLS(String[] strArr, String[] strArr2) {
        if (strArr == null || strArr.length < 2) {
            return RTMP_TYPE.ERROR;
        }
        if (strArr2 == null || strArr2.length < 2) {
            return RTMP_TYPE.ERROR;
        }
        if (strArr[0].equalsIgnoreCase(strArr2[0]) && strArr[1].equalsIgnoreCase(strArr2[1])) {
            return RTMP_TYPE.ONE_STREAM;
        }
        if (!strArr[0].equals(strArr2[0]) || strArr[1].equals(strArr2[1])) {
            return RTMP_TYPE.TWO_CLIENT;
        }
        return RTMP_TYPE.ONE_CLIENT;
    }

    static {
        try {
            if (KShareApplication.getInstance().hasLiveLib()) {
                System.load(KShareApplication.getInstance().getLiveLibPath());
            } else if (FileUtils.copyToIfNotExist(KShareApplication.getInstance().getLiveLibPath(), KShareApplication.LIVE_LIB_PATH) == 0) {
                System.load(KShareApplication.getInstance().getLiveLibPath());
            } else {
                Toaster.showLongToast("直播库初始化失败。请重新启动应用。");
            }
        } catch (ACException e) {
            e.printStackTrace();
        }
    }

    public static RtmpClientManager2 getInstance() {
        if (mInstanceManager == null) {
            mInstanceManager = new RtmpClientManager2();
        }
        return mInstanceManager;
    }

    public void addClient(Client client) {
        if (client != null && findClient(client.cid) == null) {
            client.updateManager(this);
            this.mClients.add(client);
            if (this.isRunning) {
                addconnect(client.cid, client.mUrl);
                if (client.mStreams != null) {
                    int i = 0;
                    while (true) {
                        int i2 = i;
                        if (i2 >= client.mStreams.size()) {
                            break;
                        }
                        AddStream(client.cid, client.mStreams.get(i2));
                        i = i2 + 1;
                    }
                }
                startconnect(client.cid);
            }
        }
    }

    private void AddStream(int i, NetStream netStream) {
        if (netStream != null) {
            if (netStream.isInput && netStream.videoChannel != null && netStream.videoChannel.isSetInOutSize()) {
                addInputStream(i, netStream.getSid(), netStream.name, netStream.videoChannel.width, netStream.videoChannel.height, netStream.videoChannel.owidth, netStream.videoChannel.oheight, netStream.videoChannel.bitrate, netStream.videoChannel.quality);
            } else if (netStream.isInput || netStream.audioChannel == null) {
                addstream(i, netStream.getSid(), netStream.name, netStream.isInput, -1);
            } else {
                addstream(i, netStream.getSid(), netStream.name, netStream.isInput, netStream.audioChannel.mBufferTime);
            }
        }
    }

    public void removeClient(Client client) {
        if (client != null && this.mClients != null) {
            this.mClients.remove(client);
        }
    }

    public Client findClient(int i) {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= this.mClients.size()) {
                return null;
            }
            Client client = this.mClients.get(i3);
            if (client != null && client.cid == i) {
                return client;
            }
            i2 = i3 + 1;
        }
    }

    public NetStream findStream(int i, int i2) {
        int i3 = 0;
        while (true) {
            int i4 = i3;
            if (i4 >= this.mClients.size()) {
                return null;
            }
            Client client = this.mClients.get(i4);
            if (client != null && client.cid == i) {
                return client.findStream(i2);
            }
            i3 = i4 + 1;
        }
    }

    public void stopConnect(int i) {
        ULog.d("luoleixxxxxxxxx", "stop connect cid: " + i);
        if (i < 0) {
            ULog.d("luoleixxxxxxxxx", "mClients: " + this.mClients.size());
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 >= this.mClients.size()) {
                    this.mClients.clear();
                    return;
                }
                Client client = this.mClients.get(i3);
                if (client != null) {
                    ULog.d("luoleixxxxxxxxx", "stop connect client cid: " + client.cid + "; url: " + client.mUrl);
                    client.stop();
                    stopconnect(client.cid);
                }
                i2 = i3 + 1;
            }
        } else {
            Client findClient = findClient(i);
            ULog.d("luolei stopconnect", "cid: " + i + "; client: " + findClient);
            if (findClient != null) {
                findClient.stop();
                stopconnect(i);
                removeClient(findClient);
            }
        }
    }

    public boolean isConnected(int i) {
        return isConnected(i, false);
    }

    public void initManager() {
        this.isInit = true;
        init();
        for (int i = 0; i < this.mClients.size(); i++) {
            Client client = this.mClients.get(i);
            if (client != null) {
                ULog.d("luolei", "addclient : " + client.cid + "; url: " + client.mUrl);
                addconnect(client.cid, client.mUrl);
                if (client.mStreams != null) {
                    for (int i2 = 0; i2 < client.mStreams.size(); i2++) {
                        AddStream(client.cid, client.mStreams.get(i2));
                    }
                }
            }
        }
    }

    public void stopStream(int i, int i2) {
        Client findClient = findClient(i);
        if (findClient != null) {
            NetStream findStream = findClient.findStream(i2);
            if (findStream != null) {
                findStream.stopChannel();
            }
            findClient.removeStream(i2);
            stopstream(i, i2);
        }
    }

    public void addStream(int i, NetStream netStream) {
        Client findClient;
        if (netStream != null && findStream(i, netStream.sid) == null && (findClient = findClient(i)) != null) {
            findClient.addStream(netStream);
            netStream.updateManager(this);
            AddStream(i, netStream);
        }
    }

    public boolean isRunning() {
        return this.isRunning;
    }

    public void startManager() {
        if (!this.isRunning) {
            if (!this.isInit) {
                initManager();
            }
            this.mStop = false;
            this.mThread = new Thread(this);
            this.mThread.setPriority(10);
            this.mThread.start();
            this.isRunning = true;
        }
    }

    public void run() {
        ULog.d("luolei", "manager start run");
        startconnect(-1);
        while (!this.mStop) {
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
