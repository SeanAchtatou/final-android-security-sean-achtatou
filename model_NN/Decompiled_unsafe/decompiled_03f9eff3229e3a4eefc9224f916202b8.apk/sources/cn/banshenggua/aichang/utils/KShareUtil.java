package cn.banshenggua.aichang.utils;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import cn.a.a.a;
import cn.banshenggua.aichang.api.APIKey;
import cn.banshenggua.aichang.api.KURL;
import cn.banshenggua.aichang.app.DownloadService;
import cn.banshenggua.aichang.app.KShareApplication;
import cn.banshenggua.aichang.app.Session;
import cn.banshenggua.aichang.room.message.SocketMessage;
import cn.banshenggua.aichang.sdk.ACException;
import com.pocketmusic.kshare.a.a;
import com.pocketmusic.kshare.dialog.MyDialogFragment;
import com.pocketmusic.kshare.requestobjs.m;
import com.pocketmusic.kshare.requestobjs.s;
import eu.inmite.android.lib.dialogs.b;
import eu.inmite.android.lib.dialogs.c;
import org.apache.mina.proxy.handlers.http.ntlm.NTLMConstants;

public class KShareUtil {
    public static final String AC_APK = (String.valueOf(CommonUtil.getDownloadDir()) + "aichang.apk");

    public static void showToast(Context context, int i) {
        if (context != null) {
            Toast.makeText(context, i, 0).show();
        } else {
            ULog.d(SocketMessage.MSG_ERROR_KEY, "Toast IS ERROR");
        }
    }

    public static void showToast(Context context, String str) {
        if (TextUtils.isEmpty(str) || context == null) {
            ULog.d(SocketMessage.MSG_ERROR_KEY, "Toast IS ERROR");
        } else {
            Toast.makeText(context, str, 0).show();
        }
    }

    public static void showToastNetWorkInfo(Context context) {
    }

    public static void showToastJsonStatus(Context context, m mVar) {
        if (context != null && mVar != null && mVar.i() != -1000) {
            showToast(context, a.a(mVar.i(), mVar.j()));
        }
    }

    public static void showToastJsonStatus(Context context, m mVar, int i) {
        if (context != null) {
            if (mVar.i() != -1000) {
                showToast(context, a.a(mVar.i(), mVar.j()));
            } else {
                showToast(context, context.getString(i));
            }
        }
    }

    public static int sendMailByIntent(Context context, String str) {
        Intent intent = new Intent("android.intent.action.SEND");
        intent.setType("plain/text");
        intent.putExtra("android.intent.extra.EMAIL", new String[]{str});
        intent.putExtra("android.intent.extra.CC", "cc");
        intent.putExtra("android.intent.extra.TEXT", "android问题或建议：");
        context.startActivity(Intent.createChooser(intent, "感谢您的反馈"));
        return 1;
    }

    public static void showNewUpIcon(String str, Button button) {
        if (button != null) {
            if (TextUtils.isEmpty(str)) {
                button.setVisibility(8);
                return;
            }
            button.setVisibility(0);
            button.setText(str);
        }
    }

    public static void setNumUpIcon(int i, TextView textView) {
        if (textView != null) {
            if (i > 0) {
                textView.setVisibility(0);
                if (i < 100) {
                    textView.setText(new StringBuilder(String.valueOf(i)).toString());
                } else {
                    textView.setText("99+");
                }
            } else {
                textView.setVisibility(8);
            }
        }
    }

    public static void setNumUpIcon(int i, Button button) {
        if (button != null) {
            if (i > 0) {
                button.setVisibility(0);
                if (i < 100) {
                    button.setText(new StringBuilder(String.valueOf(i)).toString());
                } else {
                    button.setText("99+");
                }
            } else {
                button.setVisibility(8);
            }
        }
    }

    public static void setNewIcon(int i, TextView textView) {
        if (textView != null) {
            if (i > 0) {
                textView.setVisibility(0);
                textView.setText("new");
                return;
            }
            textView.setVisibility(8);
        }
    }

    public static void setNewIcon(int i, Button button) {
        if (button != null) {
            if (i > 0) {
                button.setVisibility(0);
                button.setText("new");
                return;
            }
            button.setVisibility(8);
        }
    }

    public static boolean processAnonymous(FragmentActivity fragmentActivity, String str, String str2) {
        if (Session.getCurrentAccount().d()) {
            Session.getSharedSession().updateAccount();
            if (Session.getCurrentAccount().d()) {
                tipInstallAC(fragmentActivity, str, str2);
                return true;
            }
        }
        return false;
    }

    public static void tipLoginDialog(FragmentActivity fragmentActivity) {
        tipInstallAC(fragmentActivity, null, null);
    }

    public static void installAC(FragmentActivity fragmentActivity) throws ACException {
        String urlEncode = KURL.urlEncode(s.a(APIKey.APIKey_GetAPK), null, true);
        ULog.d("luolei", "apkurl: " + urlEncode);
        Intent intent = new Intent(fragmentActivity, DownloadService.class);
        intent.setAction(DownloadService.ACTION_ADD_TO_DOWNLOAD);
        intent.putExtra(DownloadService.DOWNLOAD_URL, urlEncode);
        fragmentActivity.startService(intent);
    }

    public static void tipEntryAc(final FragmentActivity fragmentActivity, final String str, final String str2) {
        final MyDialogFragment myDialogFragment = (MyDialogFragment) MyDialogFragment.a(fragmentActivity, fragmentActivity.getSupportFragmentManager()).a(a.h.tip).b(a.h.tip_for_entry_ac).e(a.h.cancel).d(a.h.ok).d();
        myDialogFragment.a(new c() {
            public void onPositiveButtonClicked(int i) {
                KShareUtil.entryAc(FragmentActivity.this, str, str2);
                myDialogFragment.dismiss();
            }

            public void onNegativeButtonClicked(int i) {
                myDialogFragment.dismiss();
            }
        });
        myDialogFragment.a(new b() {
            public void onCancelled(int i) {
                MyDialogFragment.this.dismiss();
            }
        });
    }

    /* access modifiers changed from: private */
    public static void entryAc(Activity activity, String str, String str2) {
        Intent intent = new Intent();
        Uri uri = null;
        intent.addFlags(NTLMConstants.FLAG_UNIDENTIFIED_11);
        if (!TextUtils.isEmpty(str)) {
            uri = Uri.parse("aichang://webview?access_type=2&data_type=12&url=http%3A%2F%2Fweibo.aichang.cn%2Fapiv5%2Fuser%2Fuserinfo.php%3Fcmd%3Droominfo%26uid%3D" + str);
        }
        if (!TextUtils.isEmpty(str2)) {
            uri = Uri.parse("aichang://cn.banshenggua.aichang/roominfo?rid=" + str2);
        }
        intent.setComponent(new ComponentName("cn.banshenggua.aichang", "com.pocketmusic.kshare.WelcomeActivity"));
        if (uri != null) {
            intent.setData(uri);
        }
        activity.startActivity(intent);
        Toaster.showLongToast(a.h.entry_ac_toast);
        activity.finish();
    }

    public static void tipInstallAC(final FragmentActivity fragmentActivity, String str, String str2) {
        try {
            if (KShareApplication.getInstance().hasInstallAc()) {
                tipEntryAc(fragmentActivity, str, str2);
                return;
            }
        } catch (ACException e) {
            e.printStackTrace();
        }
        final MyDialogFragment myDialogFragment = (MyDialogFragment) MyDialogFragment.a(fragmentActivity, fragmentActivity.getSupportFragmentManager()).a(a.h.tip).b(a.h.tip_for_install_ac).e(a.h.cancel).d(a.h.ok).d();
        myDialogFragment.a(new c() {
            public void onPositiveButtonClicked(int i) {
                try {
                    KShareUtil.installAC(FragmentActivity.this);
                } catch (ACException e) {
                    Toaster.showLongToast("安装爱唱失败");
                }
                myDialogFragment.dismiss();
            }

            public void onNegativeButtonClicked(int i) {
                myDialogFragment.dismiss();
            }
        });
        myDialogFragment.a(new b() {
            public void onCancelled(int i) {
                MyDialogFragment.this.dismiss();
            }
        });
    }
}
