package cn.banshenggua.aichang.utils;

import android.text.TextUtils;
import cn.banshenggua.aichang.api.KURL;
import com.pocketmusic.kshare.requestobjs.g;
import com.sina.weibo.sdk.constant.WBConstants;
import java.net.URL;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

public class URLUtils {
    private static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$utils$URLUtils$ITEM_KEY = null;
    private static final String TAG = "URLUtils";

    static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$utils$URLUtils$ITEM_KEY() {
        int[] iArr = $SWITCH_TABLE$cn$banshenggua$aichang$utils$URLUtils$ITEM_KEY;
        if (iArr == null) {
            iArr = new int[ITEM_KEY.values().length];
            try {
                iArr[ITEM_KEY.ACCESS_TYPE.ordinal()] = 6;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[ITEM_KEY.CLOSE_VIEW.ordinal()] = 9;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[ITEM_KEY.CONTENTS.ordinal()] = 10;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[ITEM_KEY.DATA_TYPE.ordinal()] = 5;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[ITEM_KEY.IMAGE.ordinal()] = 2;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[ITEM_KEY.IMG.ordinal()] = 3;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[ITEM_KEY.KEYWORDS.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[ITEM_KEY.TITLE.ordinal()] = 1;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[ITEM_KEY.URL.ordinal()] = 4;
            } catch (NoSuchFieldError e9) {
            }
            try {
                iArr[ITEM_KEY.VIEW_TYPE.ordinal()] = 8;
            } catch (NoSuchFieldError e10) {
            }
            $SWITCH_TABLE$cn$banshenggua$aichang$utils$URLUtils$ITEM_KEY = iArr;
        }
        return iArr;
    }

    enum ITEM_KEY {
        TITLE("title", true),
        IMAGE(WBConstants.GAME_PARAMS_GAME_IMAGE_URL, true),
        IMG("img", true),
        URL("url", false),
        DATA_TYPE("data_type", false),
        ACCESS_TYPE("access_type", false),
        KEYWORDS("keywords", true),
        VIEW_TYPE("view_type", true),
        CLOSE_VIEW("close_view", true),
        CONTENTS("contents", true);
        
        private boolean canEmpty = false;
        private String value = "title";

        private ITEM_KEY(String str, boolean z) {
            this.value = str;
            this.canEmpty = z;
        }
    }

    public static g.a parseUrlToItem(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            Map<String, String> URLRequest = URLRequest(str);
            g.a aVar = new g.a();
            String str2 = URLRequest.get(ITEM_KEY.access$2(ITEM_KEY.CLOSE_VIEW));
            if (TextUtils.isEmpty(str2) || !str2.equalsIgnoreCase("1")) {
                for (ITEM_KEY item_key : ITEM_KEY.values()) {
                    if (item_key == null || TextUtils.isEmpty(ITEM_KEY.access$2(item_key))) {
                        return null;
                    }
                    String str3 = URLRequest.get(ITEM_KEY.access$2(item_key));
                    if (!TextUtils.isEmpty(str3)) {
                        ULog.d(TAG, str3);
                        switch ($SWITCH_TABLE$cn$banshenggua$aichang$utils$URLUtils$ITEM_KEY()[item_key.ordinal()]) {
                            case 1:
                                aVar.c = URLDecoder.decode(str3, "UTF-8");
                                continue;
                            case 2:
                                aVar.e = URLDecoder.decode(str3, "UTF-8");
                                continue;
                            case 3:
                                aVar.e = URLDecoder.decode(str3, "UTF-8");
                                continue;
                            case 4:
                                aVar.g = URLDecoder.decode(str3, "UTF-8");
                                continue;
                            case 5:
                                aVar.f618a = str3;
                                continue;
                            case 6:
                                aVar.b = str3;
                                continue;
                            case 7:
                                aVar.h = URLDecoder.decode(str3, "UTF-8");
                                continue;
                            case 8:
                                aVar.i = str3;
                                continue;
                            case 9:
                                if ("1".equalsIgnoreCase(str3)) {
                                    aVar.j = true;
                                    break;
                                } else {
                                    continue;
                                }
                            case 10:
                                aVar.l = URLDecoder.decode(str3, "UTF-8");
                                continue;
                        }
                    } else if (!ITEM_KEY.access$3(item_key)) {
                        return null;
                    }
                }
                if (aVar.a()) {
                    return aVar;
                }
                return null;
            }
            aVar.j = true;
            return aVar;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String UrlPage(String str) {
        String lowerCase = str.trim().toLowerCase();
        String[] split = lowerCase.split("[?]");
        if (lowerCase.length() <= 0 || split.length <= 1 || split[0] == null) {
            return null;
        }
        return split[0];
    }

    public static String[] UrlProgress(String str) {
        if (str == null || str.length() < 4) {
            return null;
        }
        int lastIndexOf = str.lastIndexOf("/");
        String[] strArr = new String[2];
        if (lastIndexOf <= 0 || lastIndexOf > str.length() - 2) {
            return null;
        }
        strArr[0] = str.substring(0, lastIndexOf);
        strArr[1] = str.substring(lastIndexOf + 1);
        System.out.println("ret_0: " + strArr[0]);
        System.out.println("ret_1: " + strArr[1]);
        return strArr;
    }

    public static String UrlGetHost(String str) {
        try {
            new URL(str.replaceFirst(str.substring(0, str.indexOf("://")), "http"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String UrlReplaceHost(String str, String str2) {
        try {
            String substring = str.substring(0, str.indexOf("://"));
            URL url = new URL(str.replaceFirst(substring, "http"));
            System.out.println("URL is " + url.toString());
            System.out.println("protocol is " + url.getProtocol());
            System.out.println("authority is " + url.getAuthority());
            System.out.println("file name is " + url.getFile());
            System.out.println("host is " + url.getHost());
            System.out.println("path is " + url.getPath());
            System.out.println("port is " + url.getPort());
            System.out.println("default port is " + url.getDefaultPort());
            System.out.println("query is " + url.getQuery());
            System.out.println("ref is " + url.getRef());
            return String.valueOf(substring) + "://" + str2 + url.getFile();
        } catch (Exception e) {
            e.printStackTrace();
            return str;
        }
    }

    private static String TruncateUrlPage(String str) {
        String lowerCase = str.trim().toLowerCase();
        String[] split = lowerCase.split("[?]");
        if (lowerCase.length() <= 1 || split.length <= 1 || split[1] == null) {
            return null;
        }
        return split[1];
    }

    public static Map<String, String> URLRequest(String str) {
        HashMap hashMap = new HashMap();
        String TruncateUrlPage = TruncateUrlPage(str);
        if (TruncateUrlPage != null) {
            for (String split : TruncateUrlPage.split("[&]")) {
                String[] split2 = split.split("[=]");
                if (split2.length > 1) {
                    hashMap.put(split2[0], split2[1]);
                } else if (split2[0] != "") {
                    hashMap.put(split2[0], "");
                }
            }
        }
        return hashMap;
    }

    public static String URLEncode(String str, Map<String, String> map) {
        KURL kurl = new KURL();
        kurl.baseURL = str;
        if (map != null) {
            kurl.getParameter.putAll(map);
        }
        return kurl.urlEncode();
    }
}
