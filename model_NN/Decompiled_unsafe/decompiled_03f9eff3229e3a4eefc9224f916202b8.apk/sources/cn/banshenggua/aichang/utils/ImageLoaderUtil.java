package cn.banshenggua.aichang.utils;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.view.View;
import cn.banshenggua.aichang.app.KShareApplication;
import cn.banshenggua.aichang.sdk.ACException;
import com.d.a.b.a.n;
import com.d.a.b.c;
import com.d.a.b.d;
import com.d.a.b.e;

public class ImageLoaderUtil {
    public static void initImageLoader() {
        if (!d.a().b()) {
            e screenDipConfig = ImageUtil.getScreenDipConfig();
            try {
                if (!NetWorkUtil.is3GNetwork(KShareApplication.getInstance().getApp())) {
                    screenDipConfig = ImageUtil.getOneDayLimitConfig();
                }
            } catch (ACException e) {
                e.printStackTrace();
            }
            d.a().a(screenDipConfig);
        }
    }

    public static d getInstance() {
        initImageLoader();
        return d.a();
    }

    public static void displayImageBg(final View view, String str, c cVar) {
        getInstance().a(str, cVar, new n() {
            public void onLoadingFailed(String str, View view, com.d.a.b.a.c cVar) {
            }

            public void onLoadingComplete(String str, View view, Bitmap bitmap) {
                view.setBackgroundDrawable(new BitmapDrawable(bitmap));
            }

            public void onLoadingCancelled(String str, View view) {
            }
        });
    }
}
