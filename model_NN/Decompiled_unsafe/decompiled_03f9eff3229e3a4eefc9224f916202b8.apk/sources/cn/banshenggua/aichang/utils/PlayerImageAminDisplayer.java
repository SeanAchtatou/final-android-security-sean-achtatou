package cn.banshenggua.aichang.utils;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import cn.a.a.a;
import com.d.a.b.a.i;
import com.d.a.b.c.a;
import java.util.Random;

public class PlayerImageAminDisplayer implements a {
    public static final int CROP_BITMAP_FROM_BOTTOM = 1;
    public static final int NO_CROP_BITMAP = 0;
    private static final String TAG = PlayerImageAminDisplayer.class.getName();
    private int[] aniIDs = {a.C0001a.playmusic_alpha_in, a.C0001a.playmusic_left_in, a.C0001a.push_bottom_in, a.C0001a.playmusic_slide_left_in};
    private int cropBitmapType = 0;
    private Matrix mMatrix;
    private Random random = new Random();

    public PlayerImageAminDisplayer(int i) {
        this.cropBitmapType = i;
        if (i == 1) {
            this.mMatrix = new Matrix();
        }
    }

    public void display(Bitmap bitmap, com.d.a.b.e.a aVar, i iVar) {
        float f;
        float f2;
        if (bitmap != null) {
            ImageView imageView = (ImageView) aVar.d();
            if (this.cropBitmapType == 1) {
                if (this.mMatrix == null) {
                    this.mMatrix = new Matrix();
                }
                int width = bitmap.getWidth();
                int height = bitmap.getHeight();
                int width2 = imageView.getWidth();
                int height2 = imageView.getHeight();
                if (width * height2 > width2 * height) {
                    f = ((float) height2) / ((float) height);
                    f2 = (((float) width2) - (((float) width) * f)) * 0.5f;
                } else {
                    f = ((float) width2) / ((float) width);
                    f2 = 0.0f;
                }
                this.mMatrix.setScale(f, f);
                this.mMatrix.postTranslate((float) ((int) (f2 + 0.5f)), (float) ((int) (0.0f + 0.5f)));
                imageView.setImageMatrix(this.mMatrix);
                ULog.d(TAG, "imageView.setImageBitmap=" + imageView.getId());
                imageView.setBackgroundDrawable(null);
                imageView.setImageBitmap(bitmap);
                return;
            }
            imageView.setImageBitmap(bitmap);
            try {
                imageView.startAnimation(AnimationUtils.loadAnimation(imageView.getContext(), this.aniIDs[this.random.nextInt(4)]));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
