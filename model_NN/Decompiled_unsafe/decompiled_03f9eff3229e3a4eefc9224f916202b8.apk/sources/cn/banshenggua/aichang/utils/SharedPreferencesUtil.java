package cn.banshenggua.aichang.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OptionalDataException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class SharedPreferencesUtil {
    public static final String GiftCacheFile = "gift_classic_file";
    private static String MachineLeverKey = "machine_lever";
    private static final String MachineRoomMonitor = "room_monitor";
    private static final String MachineRoomVideo = "room_support_video";
    private static final String MachineRoomVideoAdjust = "room_support_video_adjust";
    private static final String MachineRoomVideoCut = "room_support_video_cut";
    private static String MachineVideoLeverKey = "machine_lever_video";
    public static final int defalt_music_size = 60;
    public static final int defalt_music_type = 0;
    public static final int defalt_voice_size = 70;
    private static String tag = "SharedPreferencesUtil";

    public static void saveToningSetting(Activity activity, int i, int i2, int i3) {
        SharedPreferences.Editor edit = activity.getSharedPreferences("toning_prf", 0).edit();
        edit.putInt("voice_size", i);
        edit.putInt("music_size", i2);
        edit.putInt("music_type", i3);
        edit.commit();
    }

    public static Integer[] getToningSetting(Activity activity) {
        SharedPreferences sharedPreferences = activity.getSharedPreferences("toning_prf", 0);
        return new Integer[]{Integer.valueOf(sharedPreferences.getInt("voice_size", 70)), Integer.valueOf(sharedPreferences.getInt("music_size", 60)), Integer.valueOf(sharedPreferences.getInt("music_type", 0))};
    }

    public static void saveLastSaveToningSetting(Activity activity, int i, int i2, int i3) {
        SharedPreferences.Editor edit = activity.getSharedPreferences("toning_prf", 0).edit();
        edit.putInt("last_save_voice_size", i);
        edit.putInt("last_save_music_size", i2);
        edit.putInt("last_save_music_type", i3);
        edit.commit();
    }

    public static Integer[] getLastSaveToningSetting(Activity activity) {
        SharedPreferences sharedPreferences = activity.getSharedPreferences("toning_prf", 0);
        return new Integer[]{Integer.valueOf(sharedPreferences.getInt("last_save_voice_size", 70)), Integer.valueOf(sharedPreferences.getInt("last_save_music_size", 60)), Integer.valueOf(sharedPreferences.getInt("last_save_music_type", 0))};
    }

    public static void saveDeltaTime(long j, Context context) {
        SharedPreferences.Editor edit = context.getSharedPreferences("record_prf", 0).edit();
        edit.putLong("delta_time_ms", j);
        edit.commit();
    }

    public static long getServiceDeltaTime(Context context) {
        return context.getSharedPreferences("record_prf", 0).getLong("service_delta_time_ms", 0);
    }

    public static void saveServiceDeltaTime(long j, Context context) {
        SharedPreferences.Editor edit = context.getSharedPreferences("record_prf", 0).edit();
        edit.putLong("service_delta_time_ms", j);
        edit.commit();
    }

    public static void saveServiceVideoDeltaTime(long j, Context context) {
        PreferencesUtils.savePrefLongWriteable(context, MachineRoomVideoAdjust, j);
    }

    public static long getServiceVideoDeltaTime(Context context) {
        return PreferencesUtils.loadPrefLongWriteable(context, MachineRoomVideoAdjust, 0);
    }

    public static void saveServiceVideoCut(long j, Context context) {
        PreferencesUtils.savePrefLongWriteable(context, MachineRoomVideoCut, j);
    }

    public static long getServiceVideoCut(Context context) {
        return PreferencesUtils.loadPrefLongWriteable(context, MachineRoomVideoCut, 0);
    }

    public static boolean getServiceVideoSupport(Context context) {
        return PreferencesUtils.loadPrefBooleanWriteable(context, MachineRoomVideo, false);
    }

    public static void saveServiceVideoSupport(Context context, boolean z) {
        PreferencesUtils.savePrefBooleanWriteable(context, MachineRoomVideo, z);
    }

    public static boolean getServiceVideoMonitorSupport(Context context) {
        return PreferencesUtils.loadPrefBooleanWriteable(context, MachineRoomMonitor, false);
    }

    public static void saveServiceVideoMonitorSupport(Context context, boolean z) {
        PreferencesUtils.savePrefBooleanWriteable(context, MachineRoomMonitor, z);
    }

    public static long getDeltaTime(Context context) {
        return context.getSharedPreferences("record_prf", 0).getLong("delta_time_ms", 0);
    }

    public static void setNoFirstTongingTime(boolean z, Context context) {
        SharedPreferences.Editor edit = context.getSharedPreferences("record_prf", 0).edit();
        edit.putBoolean("is_first_tonging_time", z);
        edit.commit();
    }

    public static boolean isFirstTongingTime(Context context) {
        return context.getSharedPreferences("record_prf", 0).getBoolean("is_first_tonging_time", true);
    }

    public static Object saveObjectToFile(Context context, Object obj, String str) {
        try {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(context.openFileOutput(str, 0));
            objectOutputStream.writeObject(obj);
            objectOutputStream.close();
            return null;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public static List<Integer> getEmojiRentlyFromFile(Context context, String str) {
        ArrayList arrayList = new ArrayList();
        try {
            ObjectInputStream objectInputStream = new ObjectInputStream(context.openFileInput(str));
            arrayList.addAll((List) objectInputStream.readObject());
            objectInputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (OptionalDataException e2) {
            e2.printStackTrace();
        } catch (ClassNotFoundException e3) {
            e3.printStackTrace();
        } catch (IOException e4) {
            e4.printStackTrace();
        }
        return arrayList;
    }

    public static Object getObjectFromFile(Context context, String str) {
        IOException e;
        Object obj;
        ClassNotFoundException e2;
        OptionalDataException e3;
        FileNotFoundException e4;
        try {
            ObjectInputStream objectInputStream = new ObjectInputStream(context.openFileInput(str));
            obj = objectInputStream.readObject();
            try {
                objectInputStream.close();
            } catch (FileNotFoundException e5) {
                e4 = e5;
                e4.printStackTrace();
                return obj;
            } catch (OptionalDataException e6) {
                e3 = e6;
                e3.printStackTrace();
                return obj;
            } catch (ClassNotFoundException e7) {
                e2 = e7;
                e2.printStackTrace();
                return obj;
            } catch (IOException e8) {
                e = e8;
                e.printStackTrace();
                return obj;
            }
        } catch (FileNotFoundException e9) {
            FileNotFoundException fileNotFoundException = e9;
            obj = null;
            e4 = fileNotFoundException;
        } catch (OptionalDataException e10) {
            OptionalDataException optionalDataException = e10;
            obj = null;
            e3 = optionalDataException;
            e3.printStackTrace();
            return obj;
        } catch (ClassNotFoundException e11) {
            ClassNotFoundException classNotFoundException = e11;
            obj = null;
            e2 = classNotFoundException;
            e2.printStackTrace();
            return obj;
        } catch (IOException e12) {
            IOException iOException = e12;
            obj = null;
            e = iOException;
            e.printStackTrace();
            return obj;
        }
        return obj;
    }

    public static void saveRoomPassword(Activity activity, HashMap<String, String> hashMap) {
        String str;
        SharedPreferences.Editor edit = activity.getSharedPreferences("room_info", 0).edit();
        String str2 = null;
        Iterator<Map.Entry<String, String>> it = hashMap.entrySet().iterator();
        while (true) {
            str = str2;
            if (!it.hasNext()) {
                break;
            }
            Map.Entry next = it.next();
            if (!TextUtils.isEmpty(str)) {
                str2 = String.valueOf(str) + ";" + next.getKey() + "-" + next.getValue();
            } else {
                str2 = String.valueOf(str) + next.getKey() + "-" + next.getValue();
            }
        }
        if (str != null) {
            ULog.d(tag, "saveRoomPassword" + str);
            edit.putString("room_password", str);
            edit.commit();
        }
    }

    public static String getRoomPassword(Activity activity) {
        SharedPreferences sharedPreferences = activity.getSharedPreferences("room_info", 0);
        ULog.d(tag, "getRoomPassword" + sharedPreferences.getString("room_password", ""));
        return sharedPreferences.getString("room_password", "");
    }

    public static void addRoomPassword(Activity activity, String str, String str2) {
        String str3;
        SharedPreferences sharedPreferences = activity.getSharedPreferences("room_info", 0);
        String string = sharedPreferences.getString("room_password", "");
        if (!TextUtils.isEmpty(string)) {
            str3 = String.valueOf(string) + ";" + str + "-" + str2;
        } else {
            str3 = String.valueOf(string) + str + "-" + str2;
        }
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString("room_password", str3);
        ULog.d(tag, "addRoomPassword" + str3);
        edit.commit();
    }

    public static HashMap<String, String> getRoomPasswordMap(Activity activity) {
        HashMap<String, String> hashMap = new HashMap<>();
        String string = activity.getSharedPreferences("room_info", 0).getString("room_password", "");
        ULog.d(tag, "getRoomPasswordMap" + string);
        if (!TextUtils.isEmpty(string)) {
            String[] split = string.split(";");
            for (String split2 : split) {
                String[] split3 = split2.split("-");
                if (split3.length == 2) {
                    hashMap.put(split3[0], split3[1]);
                }
            }
        }
        return hashMap;
    }
}
