package cn.banshenggua.aichang.api;

import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import cn.banshenggua.aichang.app.KShareApplication;
import cn.banshenggua.aichang.utils.StringUtil;
import cn.banshenggua.aichang.utils.SystemDevice;
import cn.banshenggua.aichang.utils.ULog;
import com.tencent.stat.DeviceInfo;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class UrlUtill {
    public static Map<String, String> authParam = new HashMap();
    public static Map<String, String> baseParameter = new LinkedHashMap();

    public static void initAutheParameters() {
        authParam.put("__API__[charset]", StringUtil.Encoding);
        authParam.put("__API__[output]", "json");
        authParam.put("__API__[app_key]", "3856789339");
        authParam.put("__API__[app_secret]", "4a09feb097337960c380c8f4c2666a04");
    }

    public static void initBaseParameters() {
        if (baseParameter.size() == 0) {
            Bundle applicationMetaData = KShareApplication.getApplicationMetaData();
            String packInfo = KShareApplication.getPackInfo();
            baseParameter.put("base_version", packInfo);
            baseParameter.put(DeviceInfo.TAG_VERSION, packInfo);
            baseParameter.put("base_platform", "android");
            baseParameter.put("base_type", "ac_mini_sdk");
            baseParameter.put("base_channel", KShareApplication.getChannel());
            baseParameter.put("base_machine", SystemDevice.getInstance().getSystemMachine());
            if (applicationMetaData == null || applicationMetaData.getString("market") == null) {
                baseParameter.put("base_market", "no_market");
            } else {
                baseParameter.put("base_market", applicationMetaData.getString("market"));
            }
            if (!TextUtils.isEmpty("")) {
                baseParameter.put("first_market", "");
            }
            if (TextUtils.isEmpty(SystemDevice.getInstance().DEVICE_ID)) {
                baseParameter.put("device_id", "0");
            } else {
                baseParameter.put("device_id", SystemDevice.getInstance().DEVICE_ID);
            }
        }
    }

    public static String[] UrlProgress(String str) {
        if (str == null || str.length() < 4) {
            return null;
        }
        int lastIndexOf = str.lastIndexOf("/");
        String[] strArr = new String[2];
        if (lastIndexOf <= 0 || lastIndexOf > str.length() - 2) {
            return null;
        }
        strArr[0] = str.substring(0, lastIndexOf);
        strArr[1] = str.substring(lastIndexOf + 1);
        System.out.println("ret_0: " + strArr[0]);
        System.out.println("ret_1: " + strArr[1]);
        return strArr;
    }

    public static Map<String, String> getBaseParameter() {
        initBaseParameters();
        return baseParameter;
    }

    public static long getSystemtime() {
        return System.currentTimeMillis();
    }

    public static String createUrl(Uri.Builder builder) {
        builder.appendQueryParameter("time", new StringBuilder().append(getSystemtime()).toString());
        initBaseParameters();
        if (baseParameter != null && baseParameter.size() > 0) {
            for (Map.Entry next : baseParameter.entrySet()) {
                System.out.println("key= " + ((String) next.getKey()) + " and value= " + ((String) next.getValue()));
                builder.appendQueryParameter((String) next.getKey(), (String) next.getValue());
            }
        }
        return builder.build().toString();
    }

    public static Map<String, String> getParameter() {
        HashMap hashMap = new HashMap();
        initBaseParameters();
        hashMap.putAll(authParam);
        ULog.d("UrlUtil", "baseParameter: " + baseParameter + "; ver: " + baseParameter.get(DeviceInfo.TAG_VERSION));
        return hashMap;
    }

    public static Map<String, String> URLRequest(String str) {
        HashMap hashMap = new HashMap();
        String TruncateUrlPage = TruncateUrlPage(str);
        if (TruncateUrlPage != null) {
            for (String split : TruncateUrlPage.split("[&]")) {
                String[] split2 = split.split("[=]");
                if (split2.length > 1) {
                    hashMap.put(split2[0], split2[1]);
                } else if (split2[0] != "") {
                    hashMap.put(split2[0], "");
                }
            }
        }
        return hashMap;
    }

    private static String TruncateUrlPage(String str) {
        String lowerCase = str.trim().toLowerCase();
        String[] split = lowerCase.split("[?]");
        if (lowerCase.length() <= 1 || split.length <= 1 || split[1] == null) {
            return null;
        }
        return split[1];
    }
}
