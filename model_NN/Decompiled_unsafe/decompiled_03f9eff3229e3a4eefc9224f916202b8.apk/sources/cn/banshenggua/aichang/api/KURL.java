package cn.banshenggua.aichang.api;

import android.net.Uri;
import android.text.TextUtils;
import cn.banshenggua.aichang.utils.ULog;
import java.io.File;
import java.io.RandomAccessFile;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;

public class KURL {
    public static String Encoding = "UTF-8";
    public static Map<String, String> baseParameter = new LinkedHashMap();
    public String baseURL;
    public Map<String, KPostContent> binaryParameter = new HashMap();
    public boolean cacheWhenHttp = false;
    public Map<String, String> getParameter = new LinkedHashMap();
    public Map<String, String> postParameter = new LinkedHashMap();
    public KURLType type;

    public enum KURLType {
        KURL_Type_Default,
        KURL_Type_More,
        KURL_Type_String
    }

    public KURL() {
        initBaseParameters();
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0087  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x009a  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00b6  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void initBaseParameters() {
        /*
            java.util.Map<java.lang.String, java.lang.String> r0 = cn.banshenggua.aichang.api.KURL.baseParameter
            int r0 = r0.size()
            if (r0 != 0) goto L_0x00a3
            android.os.Bundle r0 = cn.banshenggua.aichang.app.KShareApplication.getApplicationMetaData()
            java.lang.String r1 = cn.banshenggua.aichang.app.KShareApplication.getPackInfo()
            java.util.Map<java.lang.String, java.lang.String> r2 = cn.banshenggua.aichang.api.KURL.baseParameter
            java.lang.String r3 = "base_version"
            r2.put(r3, r1)
            java.util.Map<java.lang.String, java.lang.String> r2 = cn.banshenggua.aichang.api.KURL.baseParameter
            java.lang.String r3 = "ver"
            r2.put(r3, r1)
            java.util.Map<java.lang.String, java.lang.String> r1 = cn.banshenggua.aichang.api.KURL.baseParameter
            java.lang.String r2 = "base_platform"
            java.lang.String r3 = "android"
            r1.put(r2, r3)
            java.util.Map<java.lang.String, java.lang.String> r1 = cn.banshenggua.aichang.api.KURL.baseParameter
            java.lang.String r2 = "base_type"
            java.lang.String r3 = "ac_sdk"
            r1.put(r2, r3)
            java.util.Map<java.lang.String, java.lang.String> r1 = cn.banshenggua.aichang.api.KURL.baseParameter
            java.lang.String r2 = "base_channel"
            java.lang.String r3 = cn.banshenggua.aichang.app.KShareApplication.getChannel()
            r1.put(r2, r3)
            java.util.Map<java.lang.String, java.lang.String> r1 = cn.banshenggua.aichang.api.KURL.baseParameter
            java.lang.String r2 = "base_machine"
            cn.banshenggua.aichang.utils.SystemDevice r3 = cn.banshenggua.aichang.utils.SystemDevice.getInstance()
            java.lang.String r3 = r3.getSystemMachine()
            r1.put(r2, r3)
            if (r0 == 0) goto L_0x00a4
            java.lang.String r1 = "market"
            java.lang.String r1 = r0.getString(r1)
            if (r1 == 0) goto L_0x00a4
            java.util.Map<java.lang.String, java.lang.String> r1 = cn.banshenggua.aichang.api.KURL.baseParameter
            java.lang.String r2 = "base_market"
            java.lang.String r3 = "market"
            java.lang.String r0 = r0.getString(r3)
            r1.put(r2, r0)
        L_0x0061:
            java.lang.String r1 = ""
            cn.banshenggua.aichang.app.KShareApplication r0 = cn.banshenggua.aichang.app.KShareApplication.getInstance()     // Catch:{ ACException -> 0x00ae }
            android.app.Application r0 = r0.getApp()     // Catch:{ ACException -> 0x00ae }
            java.lang.String r2 = "First_Market"
            r3 = 0
            java.lang.String r0 = cn.banshenggua.aichang.utils.PreferencesUtils.loadPrefString(r0, r2, r3)     // Catch:{ ACException -> 0x00ae }
            java.util.Map<java.lang.String, java.lang.String> r1 = cn.banshenggua.aichang.api.KURL.baseParameter     // Catch:{ ACException -> 0x00c4 }
            java.lang.String r2 = "package"
            cn.banshenggua.aichang.app.KShareApplication r3 = cn.banshenggua.aichang.app.KShareApplication.getInstance()     // Catch:{ ACException -> 0x00c4 }
            java.lang.String r3 = r3.getPackageName()     // Catch:{ ACException -> 0x00c4 }
            r1.put(r2, r3)     // Catch:{ ACException -> 0x00c4 }
        L_0x0081:
            boolean r1 = android.text.TextUtils.isEmpty(r0)
            if (r1 != 0) goto L_0x008e
            java.util.Map<java.lang.String, java.lang.String> r1 = cn.banshenggua.aichang.api.KURL.baseParameter
            java.lang.String r2 = "first_market"
            r1.put(r2, r0)
        L_0x008e:
            cn.banshenggua.aichang.utils.SystemDevice r0 = cn.banshenggua.aichang.utils.SystemDevice.getInstance()
            java.lang.String r0 = r0.DEVICE_ID
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 == 0) goto L_0x00b6
            java.util.Map<java.lang.String, java.lang.String> r0 = cn.banshenggua.aichang.api.KURL.baseParameter
            java.lang.String r1 = "device_id"
            java.lang.String r2 = "0"
            r0.put(r1, r2)
        L_0x00a3:
            return
        L_0x00a4:
            java.util.Map<java.lang.String, java.lang.String> r0 = cn.banshenggua.aichang.api.KURL.baseParameter
            java.lang.String r1 = "base_market"
            java.lang.String r2 = "no_market"
            r0.put(r1, r2)
            goto L_0x0061
        L_0x00ae:
            r0 = move-exception
            r4 = r0
            r0 = r1
            r1 = r4
        L_0x00b2:
            r1.printStackTrace()
            goto L_0x0081
        L_0x00b6:
            java.util.Map<java.lang.String, java.lang.String> r0 = cn.banshenggua.aichang.api.KURL.baseParameter
            java.lang.String r1 = "device_id"
            cn.banshenggua.aichang.utils.SystemDevice r2 = cn.banshenggua.aichang.utils.SystemDevice.getInstance()
            java.lang.String r2 = r2.DEVICE_ID
            r0.put(r1, r2)
            goto L_0x00a3
        L_0x00c4:
            r1 = move-exception
            goto L_0x00b2
        */
        throw new UnsupportedOperationException("Method not decompiled: cn.banshenggua.aichang.api.KURL.initBaseParameters():void");
    }

    public KURLType getType() {
        return this.type;
    }

    public void setType(KURLType kURLType) {
        this.type = kURLType;
    }

    public static class KPostContent {
        private boolean isSlice = false;
        private int mBegin = 0;
        private String mContentType = null;
        private byte[] mData = null;
        private String mFile = null;
        private Uri mFilePath = null;
        private String mFilename = "kfile";
        private int mSize = 0;

        public KPostContent(String str, String str2, Uri uri, byte[] bArr) {
            this.mContentType = str;
            this.mFilename = str2;
            this.mFilePath = uri;
            this.mData = bArr;
        }

        public KPostContent(String str, String str2, String str3, boolean z, int i, int i2) {
            this.mContentType = str;
            this.mFilename = str2;
            this.mFilePath = null;
            this.mFile = str3;
            this.mData = null;
            setSlice(z);
            this.mBegin = i;
            this.mSize = i2;
        }

        public String getContentType() {
            return this.mContentType;
        }

        public void setContentType(String str) {
            this.mContentType = str;
        }

        public String getFilename() {
            return this.mFilename;
        }

        public void setFilename(String str) {
            this.mFilename = str;
        }

        public Uri getFilePath() {
            return this.mFilePath;
        }

        public void setFilePath(Uri uri) {
            this.mFilePath = uri;
        }

        public byte[] getData() {
            if (isSlice()) {
                try {
                    RandomAccessFile randomAccessFile = new RandomAccessFile(new File(this.mFile), "r");
                    if (randomAccessFile.length() - ((long) this.mBegin) < ((long) this.mSize)) {
                        this.mSize = (int) (randomAccessFile.length() - ((long) this.mBegin));
                    }
                    byte[] bArr = new byte[this.mSize];
                    randomAccessFile.seek((long) this.mBegin);
                    int i = 0;
                    do {
                        i += randomAccessFile.read(bArr, i, this.mSize - i);
                    } while (i < this.mSize);
                    ULog.d("luolei", "kpostcontent getData begin: " + this.mBegin + "; size: " + this.mSize);
                    return bArr;
                } catch (Exception e) {
                    e.printStackTrace();
                    ULog.d("luolei", "kpostcontent getdata error", e);
                }
            }
            return this.mData;
        }

        public void setData(byte[] bArr) {
            this.mData = bArr;
        }

        public String toString() {
            return getClass() + "Content-Type:" + this.mContentType + "File-Name:" + this.mFilename + "File-Path:" + this.mFilePath + (this.mData == null ? "has data" : "has not data");
        }

        public boolean isSlice() {
            return this.isSlice;
        }

        public void setSlice(boolean z) {
            this.isSlice = z;
        }
    }

    public static List<BasicNameValuePair> getParamsList(Map<String, String> map) {
        LinkedList linkedList = new LinkedList();
        for (Map.Entry next : map.entrySet()) {
            if (next.getValue() != null) {
                linkedList.add(new BasicNameValuePair((String) next.getKey(), (String) next.getValue()));
            }
        }
        return linkedList;
    }

    public String urlEncode() {
        if (this.getParameter.size() == 0 && baseParameter.size() == 0) {
            return this.baseURL;
        }
        if (TextUtils.isEmpty(this.baseURL)) {
            return "";
        }
        if (this.type == KURLType.KURL_Type_String) {
            if (this.baseURL.indexOf("?") > -1) {
                return String.valueOf(this.baseURL) + "&" + URLEncodedUtils.format(getParamsList(this.getParameter), Encoding);
            }
            return String.valueOf(this.baseURL) + "?" + URLEncodedUtils.format(getParamsList(this.getParameter), Encoding);
        } else if (this.getParameter.size() == 0) {
            if (this.baseURL.indexOf("?") > -1) {
                return String.valueOf(this.baseURL) + "&" + URLEncodedUtils.format(getParamsList(baseParameter), Encoding);
            }
            return String.valueOf(this.baseURL) + "?" + URLEncodedUtils.format(getParamsList(baseParameter), Encoding);
        } else if (baseParameter.size() == 0) {
            if (this.baseURL.indexOf("?") > -1) {
                return String.valueOf(this.baseURL) + "&" + URLEncodedUtils.format(getParamsList(this.getParameter), Encoding);
            }
            return String.valueOf(this.baseURL) + "?" + URLEncodedUtils.format(getParamsList(this.getParameter), Encoding);
        } else if (this.baseURL.indexOf("?") > -1) {
            return String.valueOf(this.baseURL) + "&" + URLEncodedUtils.format(getParamsList(baseParameter), Encoding) + "&" + URLEncodedUtils.format(getParamsList(this.getParameter), Encoding);
        } else {
            return String.valueOf(this.baseURL) + "?" + URLEncodedUtils.format(getParamsList(baseParameter), Encoding) + "&" + URLEncodedUtils.format(getParamsList(this.getParameter), Encoding);
        }
    }

    public boolean hasPost() {
        return this.postParameter.size() > 0 || this.binaryParameter.size() > 0;
    }

    public String toString(boolean z) {
        StringBuilder sb = new StringBuilder();
        sb.append(this.baseURL);
        if (z && baseParameter.size() > 0) {
            if (sb.indexOf("?") > -1) {
                sb.append("&" + URLEncodedUtils.format(getParamsList(baseParameter), Encoding));
            } else {
                sb.append("?" + URLEncodedUtils.format(getParamsList(baseParameter), Encoding));
            }
        }
        if (this.getParameter.size() == 0 && this.postParameter.size() == 0) {
            return sb.toString();
        }
        if (this.getParameter.size() <= 0 || this.postParameter.size() != 0) {
            if (this.getParameter.size() == 0 && this.postParameter.size() > 0) {
                if (sb.indexOf("?") < 0) {
                    sb.append("?");
                } else {
                    sb.append("&");
                }
                for (Map.Entry next : this.postParameter.entrySet()) {
                    if (!TextUtils.isEmpty((CharSequence) next.getValue())) {
                        sb.append((String) next.getKey()).append("=").append(URLEncoder.encode((String) next.getValue())).append("&");
                    }
                }
                sb.deleteCharAt(sb.length() - 1);
            }
            if (this.getParameter.size() > 0 && this.postParameter.size() > 0) {
                if (sb.indexOf("?") < 0) {
                    sb.append("?");
                } else {
                    sb.append("&");
                }
                for (Map.Entry next2 : this.getParameter.entrySet()) {
                    if (!TextUtils.isEmpty((CharSequence) next2.getValue())) {
                        sb.append((String) next2.getKey()).append("=").append(URLEncoder.encode((String) next2.getValue())).append("&");
                    }
                }
                for (Map.Entry next3 : this.postParameter.entrySet()) {
                    if (!TextUtils.isEmpty((CharSequence) next3.getValue())) {
                        sb.append((String) next3.getKey()).append("=").append(URLEncoder.encode((String) next3.getValue())).append("&");
                    }
                }
                sb.deleteCharAt(sb.length() - 1);
            }
            return sb.toString();
        }
        if (sb.indexOf("?") < 0) {
            sb.append("?");
        } else {
            sb.append("&");
        }
        for (Map.Entry next4 : this.getParameter.entrySet()) {
            if (!TextUtils.isEmpty((CharSequence) next4.getValue())) {
                sb.append((String) next4.getKey()).append("=").append(URLEncoder.encode((String) next4.getValue())).append("&");
            }
        }
        sb.deleteCharAt(sb.length() - 1);
        return sb.toString();
    }

    public String toString() {
        return toString(true);
    }

    public String getHash() {
        ULog.d("KURL", "hash: " + toString());
        int hashCode = toString().hashCode();
        return String.valueOf(String.valueOf(Math.abs(hashCode) % 100)) + File.separator + String.valueOf(Math.abs(hashCode / 100) % 100) + File.separator + String.valueOf(hashCode);
    }

    public static String urlEncode(String str, String str2) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        if (str.indexOf("?") > -1) {
            return String.valueOf(str) + "&" + str2;
        }
        return String.valueOf(str) + "?" + str2;
    }

    public static String urlEncode(String str, Map<String, String> map, boolean z) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        HashMap hashMap = new HashMap();
        if (map != null) {
            hashMap.putAll(map);
        }
        if (z) {
            initBaseParameters();
            hashMap.putAll(baseParameter);
        }
        if (str.indexOf("?") > -1) {
            return String.valueOf(str) + "&" + URLEncodedUtils.format(getParamsList(hashMap), Encoding);
        }
        return String.valueOf(str) + "?" + URLEncodedUtils.format(getParamsList(hashMap), Encoding);
    }
}
