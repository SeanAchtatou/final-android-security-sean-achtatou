package cn.banshenggua.aichang.app;

import android.app.Service;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.IBinder;
import android.text.TextUtils;
import android.util.Log;
import cn.banshenggua.aichang.app.KShareApplication;
import cn.banshenggua.aichang.utils.CommonUtil;
import cn.banshenggua.aichang.utils.Toaster;
import cn.banshenggua.aichang.utils.ULog;
import com.pocketmusic.kshare.dialog.MyDialogFragment;
import java.io.File;
import org.apache.mina.proxy.handlers.http.ntlm.NTLMConstants;

public class DownloadService extends Service {
    public static final String ACTION_ADD_TO_DOWNLOAD = "com.pocketmusic.kshare.action.add_to_download";
    public static final String ACTION_DWNLOAD_PROCESS = "cn.banshenggua.aichang.downloadprocess";
    public static final String ACTION_START_DOWNLOAD = "cn.banshenggua.aichang.startdownload";
    public static final String ACTOIN_DWNLOAD_COMP = "cn.banshenggua.aichang.downloadcomplite";
    public static final String AC_APK = (String.valueOf(CommonUtil.getDownloadDir()) + "aichang.apk");
    public static final String DOWNLOAD_URL = "aichang_apk";
    private static final String DTAG = "ACDownload";
    /* access modifiers changed from: private */
    public boolean isDonwloading = false;
    private KShareApplication.DownloadListener mDefaultDownload = new KShareApplication.DownloadListener() {
        MyDialogFragment dialog = null;
        private int lastReportProcess = -1;

        public void onStart() {
            ULog.d(DownloadService.DTAG, "onStart");
            Toaster.showLongToast("开始下载安装包......");
            DownloadService.this.sendBroadcast(new Intent(DownloadService.ACTION_START_DOWNLOAD));
        }

        public void onLoading(int i) {
            if (i % 5 == 0 && this.lastReportProcess != i) {
                Intent intent = new Intent(DownloadService.ACTION_DWNLOAD_PROCESS);
                intent.putExtra(DownloadService.ACTION_DWNLOAD_PROCESS, i);
                DownloadService.this.sendBroadcast(intent);
                ULog.d(DownloadService.DTAG, "broadcast: " + i);
                this.lastReportProcess = i;
            }
        }

        public void onError(String str) {
            ULog.d(DownloadService.DTAG, "error: " + str);
        }

        public void onDownloaded(String str) {
            ULog.d(DownloadService.DTAG, "save file: " + str);
            DownloadService.this.sendBroadcast(new Intent(DownloadService.ACTOIN_DWNLOAD_COMP));
            DownloadService.this.isDonwloading = false;
            Toaster.showLongToast("下载安装包完成，请安装。");
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setFlags(NTLMConstants.FLAG_UNIDENTIFIED_11);
            intent.setDataAndType(Uri.fromFile(new File(DownloadService.AC_APK)), "application/vnd.android.package-archive");
            DownloadService.this.startActivity(intent);
        }
    };

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
        ULog.i(KShareApplication.TAG, "DownloadService.onCreate");
    }

    public void onStart(Intent intent, int i) {
        super.onStart(intent, i);
        if (intent != null) {
            String action = intent.getAction();
            Log.i(KShareApplication.TAG, "DownloadService.onStart - " + action);
            if (!TextUtils.isEmpty(action) && action.equals(ACTION_ADD_TO_DOWNLOAD)) {
                try {
                    downloadFile(intent.getStringExtra(DOWNLOAD_URL), AC_APK, null);
                } catch (Exception e) {
                }
            }
        }
    }

    public void onDestroy() {
        super.onDestroy();
        Log.i(KShareApplication.TAG, "DownloadService.onDestroy");
    }

    public void downloadFile(String str, String str2, KShareApplication.DownloadListener downloadListener) {
        KShareApplication.DownloadListener downloadListener2;
        if (this.isDonwloading) {
            Toaster.showLongToast("已经开始下载了......");
            return;
        }
        if (downloadListener == null) {
            downloadListener2 = this.mDefaultDownload;
        } else {
            downloadListener2 = downloadListener;
        }
        new DownloadTask(str2, str, downloadListener2, false).execute(new Void[0]);
        this.isDonwloading = true;
    }

    private class DownloadTask extends AsyncTask<Void, Integer, String> {
        private boolean isGzip = false;
        private KShareApplication.DownloadListener mListener;
        private String mSave;
        private String mUrl;
        private String mZipFile;

        public DownloadTask(String str, String str2, KShareApplication.DownloadListener downloadListener, boolean z) {
            this.mSave = str;
            this.mUrl = str2;
            this.mListener = downloadListener;
            this.isGzip = z;
            this.mZipFile = String.valueOf(str) + ".z";
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: cn.banshenggua.aichang.utils.FileUtils.streamToFile(java.lang.String, java.io.InputStream, boolean):int
         arg types: [java.lang.String, java.util.zip.GZIPInputStream, int]
         candidates:
          cn.banshenggua.aichang.utils.FileUtils.streamToFile(java.io.File, java.io.InputStream, boolean):int
          cn.banshenggua.aichang.utils.FileUtils.streamToFile(java.lang.String, java.io.InputStream, boolean):int */
        /* access modifiers changed from: protected */
        /* JADX WARNING: Removed duplicated region for block: B:59:0x00e3 A[SYNTHETIC, Splitter:B:59:0x00e3] */
        /* JADX WARNING: Removed duplicated region for block: B:62:0x00e8 A[Catch:{ IOException -> 0x010c }] */
        /* JADX WARNING: Removed duplicated region for block: B:64:0x00ed  */
        /* JADX WARNING: Removed duplicated region for block: B:72:0x00ff  */
        /* JADX WARNING: Removed duplicated region for block: B:87:? A[RETURN, SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.lang.String doInBackground(java.lang.Void... r16) {
            /*
                r15 = this;
                r3 = 0
                r2 = 0
                r1 = 0
                java.net.URL r0 = new java.net.URL     // Catch:{ Exception -> 0x010e }
                java.lang.String r4 = r15.mUrl     // Catch:{ Exception -> 0x010e }
                r0.<init>(r4)     // Catch:{ Exception -> 0x010e }
                java.net.URLConnection r0 = r0.openConnection()     // Catch:{ Exception -> 0x010e }
                java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x010e }
                r0.connect()     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                int r1 = r0.getResponseCode()     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                r4 = 200(0xc8, float:2.8E-43)
                if (r1 == r4) goto L_0x004d
                java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                java.lang.String r4 = "Server returned HTTP "
                r1.<init>(r4)     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                int r4 = r0.getResponseCode()     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                java.lang.String r4 = " "
                java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                java.lang.String r4 = r0.getResponseMessage()     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                if (r2 == 0) goto L_0x0041
                r2.close()     // Catch:{ IOException -> 0x0112 }
            L_0x0041:
                if (r3 == 0) goto L_0x0046
                r3.close()     // Catch:{ IOException -> 0x0112 }
            L_0x0046:
                if (r0 == 0) goto L_0x004b
                r0.disconnect()
            L_0x004b:
                r0 = r1
            L_0x004c:
                return r0
            L_0x004d:
                int r6 = r0.getContentLength()     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                java.io.InputStream r3 = r0.getInputStream()     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                boolean r1 = r15.isGzip     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                if (r1 == 0) goto L_0x009a
                java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                java.lang.String r4 = r15.mZipFile     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                r1.<init>(r4)     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                r2 = r1
            L_0x0061:
                r1 = 4096(0x1000, float:5.74E-42)
                byte[] r1 = new byte[r1]     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                r4 = 0
            L_0x0067:
                int r7 = r3.read(r1)     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                r8 = -1
                if (r7 != r8) goto L_0x00a3
                boolean r1 = r15.isGzip     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                if (r1 == 0) goto L_0x0089
                if (r2 == 0) goto L_0x0077
                r2.close()     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
            L_0x0077:
                java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                java.lang.String r4 = r15.mZipFile     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                r1.<init>(r4)     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                java.util.zip.GZIPInputStream r4 = new java.util.zip.GZIPInputStream     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                r4.<init>(r1)     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                java.lang.String r1 = r15.mSave     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                r5 = 0
                cn.banshenggua.aichang.utils.FileUtils.streamToFile(r1, r4, r5)     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
            L_0x0089:
                if (r2 == 0) goto L_0x008e
                r2.close()     // Catch:{ IOException -> 0x0103 }
            L_0x008e:
                if (r3 == 0) goto L_0x0093
                r3.close()     // Catch:{ IOException -> 0x0103 }
            L_0x0093:
                if (r0 == 0) goto L_0x0098
                r0.disconnect()
            L_0x0098:
                r0 = 0
                goto L_0x004c
            L_0x009a:
                java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                java.lang.String r4 = r15.mSave     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                r1.<init>(r4)     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                r2 = r1
                goto L_0x0061
            L_0x00a3:
                boolean r8 = r15.isCancelled()     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                if (r8 == 0) goto L_0x00bd
                r3.close()     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                if (r2 == 0) goto L_0x00b1
                r2.close()     // Catch:{ IOException -> 0x0110 }
            L_0x00b1:
                if (r3 == 0) goto L_0x00b6
                r3.close()     // Catch:{ IOException -> 0x0110 }
            L_0x00b6:
                if (r0 == 0) goto L_0x00bb
                r0.disconnect()
            L_0x00bb:
                r0 = 0
                goto L_0x004c
            L_0x00bd:
                long r8 = (long) r7
                long r4 = r4 + r8
                if (r6 <= 0) goto L_0x00d4
                r8 = 1
                java.lang.Integer[] r8 = new java.lang.Integer[r8]     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                r9 = 0
                r10 = 100
                long r10 = r10 * r4
                long r12 = (long) r6     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                long r10 = r10 / r12
                int r10 = (int) r10     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                java.lang.Integer r10 = java.lang.Integer.valueOf(r10)     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                r8[r9] = r10     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                r15.publishProgress(r8)     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
            L_0x00d4:
                r8 = 0
                r2.write(r1, r8, r7)     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                goto L_0x0067
            L_0x00d9:
                r1 = move-exception
                r14 = r1
                r1 = r0
                r0 = r14
            L_0x00dd:
                java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00f2 }
                if (r2 == 0) goto L_0x00e6
                r2.close()     // Catch:{ IOException -> 0x010c }
            L_0x00e6:
                if (r3 == 0) goto L_0x00eb
                r3.close()     // Catch:{ IOException -> 0x010c }
            L_0x00eb:
                if (r1 == 0) goto L_0x004c
                r1.disconnect()
                goto L_0x004c
            L_0x00f2:
                r0 = move-exception
            L_0x00f3:
                if (r2 == 0) goto L_0x00f8
                r2.close()     // Catch:{ IOException -> 0x0105 }
            L_0x00f8:
                if (r3 == 0) goto L_0x00fd
                r3.close()     // Catch:{ IOException -> 0x0105 }
            L_0x00fd:
                if (r1 == 0) goto L_0x0102
                r1.disconnect()
            L_0x0102:
                throw r0
            L_0x0103:
                r1 = move-exception
                goto L_0x0093
            L_0x0105:
                r2 = move-exception
                goto L_0x00fd
            L_0x0107:
                r1 = move-exception
                r14 = r1
                r1 = r0
                r0 = r14
                goto L_0x00f3
            L_0x010c:
                r2 = move-exception
                goto L_0x00eb
            L_0x010e:
                r0 = move-exception
                goto L_0x00dd
            L_0x0110:
                r1 = move-exception
                goto L_0x00b6
            L_0x0112:
                r2 = move-exception
                goto L_0x0046
            */
            throw new UnsupportedOperationException("Method not decompiled: cn.banshenggua.aichang.app.DownloadService.DownloadTask.doInBackground(java.lang.Void[]):java.lang.String");
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String str) {
            super.onPostExecute((Object) str);
            if (str == null && this.mListener != null) {
                this.mListener.onDownloaded(this.mSave);
            } else if (this.mListener != null) {
                this.mListener.onError(str);
            }
        }

        /* access modifiers changed from: protected */
        public void onProgressUpdate(Integer... numArr) {
            super.onProgressUpdate((Object[]) numArr);
            if (this.mListener != null) {
                this.mListener.onLoading(numArr[0].intValue());
            }
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
            if (this.mListener != null) {
                this.mListener.onStart();
            }
        }
    }
}
