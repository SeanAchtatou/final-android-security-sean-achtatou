package cn.banshenggua.aichang.app;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.SurfaceHolder;
import cn.banshenggua.aichang.player.PlayerEngine;
import cn.banshenggua.aichang.player.PlayerEngineListener;
import cn.banshenggua.aichang.player.PlayerService;
import cn.banshenggua.aichang.player.Playlist;
import cn.banshenggua.aichang.sdk.ACException;
import cn.banshenggua.aichang.utils.CommonUtil;
import cn.banshenggua.aichang.utils.ImageLoaderUtil;
import cn.banshenggua.aichang.utils.NetWorkUtil;
import cn.banshenggua.aichang.utils.ULog;
import com.android.volley.n;
import com.android.volley.p;
import com.pocketmusic.kshare.requestobjs.i;
import com.pocketmusic.kshare.requestobjs.q;
import com.pocketmusic.kshare.requestobjs.s;
import java.io.File;
import java.util.List;
import org.json.JSONObject;

public class KShareApplication {
    private static final String DTAG = "ACDownload";
    public static final String LIVE_LIB_PATH = (String.valueOf(CommonUtil.getDownloadDir()) + "livelib");
    public static final int PLAY_DOWNLOAD_LIST = 1;
    public static final int PLAY_ONLINE_LIST = 0;
    public static final String TAG = "KShareApplication";
    private static KShareApplication instance = null;
    private static p mRequestQueue;
    public boolean isInitData = false;
    /* access modifiers changed from: private */
    public Application mApp;
    private DownloadListener mDefaultDownload = new DownloadListener() {
        public void onStart() {
            ULog.d(KShareApplication.DTAG, "onStart");
        }

        public void onLoading(int i) {
            ULog.d(KShareApplication.DTAG, "process: " + i);
        }

        public void onError(String str) {
            ULog.d(KShareApplication.DTAG, "error: " + str);
        }

        public void onDownloaded(String str) {
            ULog.d(KShareApplication.DTAG, "save file: " + str);
        }
    };
    private PlayerEngine mIntentPlayerEngine;
    i mLibCheck = null;
    private Playlist mOnlinePlaylist;
    /* access modifiers changed from: private */
    public PlayerEngineListener mPlayerEngineListener;
    /* access modifiers changed from: private */
    public Playlist mPlaylist;
    public PlayerEngine mServicePlayerEngine;
    public SurfaceHolder mSurfaceHolder;
    public int playlist_mode = 0;
    public boolean selectWorkFragment = false;

    public interface DownloadListener {
        void onDownloaded(String str);

        void onError(String str);

        void onLoading(int i);

        void onStart();
    }

    public static KShareApplication getInstance() throws ACException {
        if (instance != null) {
            return instance;
        }
        throw new ACException(ACException.NOT_INIT_EXCEPTION);
    }

    public static void createInstance(Application application) throws ACException {
        if (instance != null) {
            return;
        }
        if (application == null) {
            throw new ACException(ACException.NOT_INIT_EXCEPTION);
        }
        instance = new KShareApplication(application);
    }

    private KShareApplication(Application application) {
        this.mApp = application;
    }

    public Application getApp() throws ACException {
        if (this.mApp != null) {
            return this.mApp;
        }
        throw new ACException(ACException.NOT_INIT_EXCEPTION);
    }

    public void initData() {
        try {
            s.a();
            ImageLoaderUtil.initImageLoader();
            CommonUtil.initKsharePath();
            Session.getSharedSession();
            NetWorkUtil.isNetworkState(this.mApp);
            this.isInitData = true;
            ULog.d(TAG, "initData");
        } catch (Exception e) {
        }
    }

    public static String getPackInfo() {
        return "6.9.5";
    }

    public String getPackageName() {
        if (this.mApp != null) {
            return this.mApp.getPackageName();
        }
        return "";
    }

    public static Bundle getApplicationMetaData() {
        try {
            ApplicationInfo applicationInfo = instance.mApp.getPackageManager().getApplicationInfo(instance.mApp.getPackageName(), 128);
            if (applicationInfo != null) {
                return applicationInfo.metaData;
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getChannel() {
        return "duoduo";
    }

    public p getRequestQueue() throws ACException {
        if (mRequestQueue == null) {
            mRequestQueue = com.android.volley.toolbox.s.a(instance.getApp());
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(n<T> nVar, String str) {
        if (TextUtils.isEmpty(str)) {
            str = TAG;
        }
        nVar.a((Object) str);
        try {
            getRequestQueue().a((n) nVar);
        } catch (ACException e) {
            e.printStackTrace();
        }
    }

    public void cancelPendingRequests(Object obj) {
        if (mRequestQueue != null) {
            mRequestQueue.a(obj);
        }
    }

    public void onDestroy() {
    }

    public void checkLiveLib(final DownloadListener downloadListener) {
        if (!this.isInitData) {
            initData();
        }
        if (!(this.mLibCheck == null || this.mLibCheck.b == 0)) {
            checkDownloadLib(downloadListener);
        }
        this.mLibCheck = new i();
        this.mLibCheck.a(new q() {
            public void onResponse(JSONObject jSONObject) {
                KShareApplication.this.mLibCheck.a(jSONObject);
                ULog.d(KShareApplication.DTAG, "response: " + jSONObject);
                if (KShareApplication.this.mLibCheck.b != 0) {
                    KShareApplication.this.checkDownloadLib(downloadListener);
                } else if (downloadListener != null) {
                    downloadListener.onError("未知错误");
                }
            }
        });
        this.mLibCheck.a();
    }

    public boolean hasLiveLib() {
        if (new File(getLiveLibPath()).exists()) {
            return true;
        }
        return false;
    }

    public boolean hasDownloadLib() {
        File file = new File(LIVE_LIB_PATH);
        if (!file.exists() || file.length() != this.mLibCheck.d) {
            return false;
        }
        return true;
    }

    public String getLiveLibPath() {
        if (this.mApp == null) {
            return null;
        }
        return new File(this.mApp.getDir("libs", 0), "librtmpclient_shared.so").getAbsolutePath();
    }

    /* access modifiers changed from: private */
    public void checkDownloadLib(DownloadListener downloadListener) {
        if (this.mLibCheck != null && this.mLibCheck.b != 0) {
            File file = new File(LIVE_LIB_PATH);
            if (downloadListener == null) {
                downloadListener = this.mDefaultDownload;
            }
            if (!file.exists() || file.length() != this.mLibCheck.d) {
                downloadLib(downloadListener);
                return;
            }
            downloadListener.onStart();
            downloadListener.onLoading(100);
            downloadListener.onDownloaded(LIVE_LIB_PATH);
        }
    }

    private void downloadLib(DownloadListener downloadListener) {
        DownloadListener downloadListener2;
        if (this.mLibCheck != null && this.mLibCheck.b != 0) {
            if (downloadListener == null) {
                downloadListener2 = this.mDefaultDownload;
            } else {
                downloadListener2 = downloadListener;
            }
            new DownloadTask(LIVE_LIB_PATH, this.mLibCheck.f622a, downloadListener2, this.mLibCheck.c).execute(new Void[0]);
        }
    }

    public void downloadFile(String str, String str2, DownloadListener downloadListener) {
        DownloadListener downloadListener2;
        if (downloadListener == null) {
            downloadListener2 = this.mDefaultDownload;
        } else {
            downloadListener2 = downloadListener;
        }
        new DownloadTask(str2, str, downloadListener2, false).execute(new Void[0]);
    }

    private class DownloadTask extends AsyncTask<Void, Integer, String> {
        private boolean isGzip = false;
        private DownloadListener mListener;
        private String mSave;
        private String mUrl;
        private String mZipFile;

        public DownloadTask(String str, String str2, DownloadListener downloadListener, boolean z) {
            this.mSave = str;
            this.mUrl = str2;
            this.mListener = downloadListener;
            this.isGzip = z;
            this.mZipFile = String.valueOf(str) + ".z";
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: cn.banshenggua.aichang.utils.FileUtils.streamToFile(java.lang.String, java.io.InputStream, boolean):int
         arg types: [java.lang.String, java.util.zip.GZIPInputStream, int]
         candidates:
          cn.banshenggua.aichang.utils.FileUtils.streamToFile(java.io.File, java.io.InputStream, boolean):int
          cn.banshenggua.aichang.utils.FileUtils.streamToFile(java.lang.String, java.io.InputStream, boolean):int */
        /* access modifiers changed from: protected */
        /* JADX WARNING: Removed duplicated region for block: B:59:0x00e3 A[SYNTHETIC, Splitter:B:59:0x00e3] */
        /* JADX WARNING: Removed duplicated region for block: B:62:0x00e8 A[Catch:{ IOException -> 0x010c }] */
        /* JADX WARNING: Removed duplicated region for block: B:64:0x00ed  */
        /* JADX WARNING: Removed duplicated region for block: B:72:0x00ff  */
        /* JADX WARNING: Removed duplicated region for block: B:87:? A[RETURN, SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.lang.String doInBackground(java.lang.Void... r16) {
            /*
                r15 = this;
                r3 = 0
                r2 = 0
                r1 = 0
                java.net.URL r0 = new java.net.URL     // Catch:{ Exception -> 0x010e }
                java.lang.String r4 = r15.mUrl     // Catch:{ Exception -> 0x010e }
                r0.<init>(r4)     // Catch:{ Exception -> 0x010e }
                java.net.URLConnection r0 = r0.openConnection()     // Catch:{ Exception -> 0x010e }
                java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x010e }
                r0.connect()     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                int r1 = r0.getResponseCode()     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                r4 = 200(0xc8, float:2.8E-43)
                if (r1 == r4) goto L_0x004d
                java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                java.lang.String r4 = "Server returned HTTP "
                r1.<init>(r4)     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                int r4 = r0.getResponseCode()     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                java.lang.String r4 = " "
                java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                java.lang.String r4 = r0.getResponseMessage()     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                if (r2 == 0) goto L_0x0041
                r2.close()     // Catch:{ IOException -> 0x0112 }
            L_0x0041:
                if (r3 == 0) goto L_0x0046
                r3.close()     // Catch:{ IOException -> 0x0112 }
            L_0x0046:
                if (r0 == 0) goto L_0x004b
                r0.disconnect()
            L_0x004b:
                r0 = r1
            L_0x004c:
                return r0
            L_0x004d:
                int r6 = r0.getContentLength()     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                java.io.InputStream r3 = r0.getInputStream()     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                boolean r1 = r15.isGzip     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                if (r1 == 0) goto L_0x009a
                java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                java.lang.String r4 = r15.mZipFile     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                r1.<init>(r4)     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                r2 = r1
            L_0x0061:
                r1 = 4096(0x1000, float:5.74E-42)
                byte[] r1 = new byte[r1]     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                r4 = 0
            L_0x0067:
                int r7 = r3.read(r1)     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                r8 = -1
                if (r7 != r8) goto L_0x00a3
                boolean r1 = r15.isGzip     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                if (r1 == 0) goto L_0x0089
                if (r2 == 0) goto L_0x0077
                r2.close()     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
            L_0x0077:
                java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                java.lang.String r4 = r15.mZipFile     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                r1.<init>(r4)     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                java.util.zip.GZIPInputStream r4 = new java.util.zip.GZIPInputStream     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                r4.<init>(r1)     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                java.lang.String r1 = r15.mSave     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                r5 = 0
                cn.banshenggua.aichang.utils.FileUtils.streamToFile(r1, r4, r5)     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
            L_0x0089:
                if (r2 == 0) goto L_0x008e
                r2.close()     // Catch:{ IOException -> 0x0103 }
            L_0x008e:
                if (r3 == 0) goto L_0x0093
                r3.close()     // Catch:{ IOException -> 0x0103 }
            L_0x0093:
                if (r0 == 0) goto L_0x0098
                r0.disconnect()
            L_0x0098:
                r0 = 0
                goto L_0x004c
            L_0x009a:
                java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                java.lang.String r4 = r15.mSave     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                r1.<init>(r4)     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                r2 = r1
                goto L_0x0061
            L_0x00a3:
                boolean r8 = r15.isCancelled()     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                if (r8 == 0) goto L_0x00bd
                r3.close()     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                if (r2 == 0) goto L_0x00b1
                r2.close()     // Catch:{ IOException -> 0x0110 }
            L_0x00b1:
                if (r3 == 0) goto L_0x00b6
                r3.close()     // Catch:{ IOException -> 0x0110 }
            L_0x00b6:
                if (r0 == 0) goto L_0x00bb
                r0.disconnect()
            L_0x00bb:
                r0 = 0
                goto L_0x004c
            L_0x00bd:
                long r8 = (long) r7
                long r4 = r4 + r8
                if (r6 <= 0) goto L_0x00d4
                r8 = 1
                java.lang.Integer[] r8 = new java.lang.Integer[r8]     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                r9 = 0
                r10 = 100
                long r10 = r10 * r4
                long r12 = (long) r6     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                long r10 = r10 / r12
                int r10 = (int) r10     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                java.lang.Integer r10 = java.lang.Integer.valueOf(r10)     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                r8[r9] = r10     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                r15.publishProgress(r8)     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
            L_0x00d4:
                r8 = 0
                r2.write(r1, r8, r7)     // Catch:{ Exception -> 0x00d9, all -> 0x0107 }
                goto L_0x0067
            L_0x00d9:
                r1 = move-exception
                r14 = r1
                r1 = r0
                r0 = r14
            L_0x00dd:
                java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00f2 }
                if (r2 == 0) goto L_0x00e6
                r2.close()     // Catch:{ IOException -> 0x010c }
            L_0x00e6:
                if (r3 == 0) goto L_0x00eb
                r3.close()     // Catch:{ IOException -> 0x010c }
            L_0x00eb:
                if (r1 == 0) goto L_0x004c
                r1.disconnect()
                goto L_0x004c
            L_0x00f2:
                r0 = move-exception
            L_0x00f3:
                if (r2 == 0) goto L_0x00f8
                r2.close()     // Catch:{ IOException -> 0x0105 }
            L_0x00f8:
                if (r3 == 0) goto L_0x00fd
                r3.close()     // Catch:{ IOException -> 0x0105 }
            L_0x00fd:
                if (r1 == 0) goto L_0x0102
                r1.disconnect()
            L_0x0102:
                throw r0
            L_0x0103:
                r1 = move-exception
                goto L_0x0093
            L_0x0105:
                r2 = move-exception
                goto L_0x00fd
            L_0x0107:
                r1 = move-exception
                r14 = r1
                r1 = r0
                r0 = r14
                goto L_0x00f3
            L_0x010c:
                r2 = move-exception
                goto L_0x00eb
            L_0x010e:
                r0 = move-exception
                goto L_0x00dd
            L_0x0110:
                r1 = move-exception
                goto L_0x00b6
            L_0x0112:
                r2 = move-exception
                goto L_0x0046
            */
            throw new UnsupportedOperationException("Method not decompiled: cn.banshenggua.aichang.app.KShareApplication.DownloadTask.doInBackground(java.lang.Void[]):java.lang.String");
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String str) {
            super.onPostExecute((Object) str);
            if (str == null && this.mListener != null) {
                this.mListener.onDownloaded(this.mSave);
            } else if (this.mListener != null) {
                this.mListener.onError(str);
            }
        }

        /* access modifiers changed from: protected */
        public void onProgressUpdate(Integer... numArr) {
            super.onProgressUpdate((Object[]) numArr);
            if (this.mListener != null) {
                this.mListener.onLoading(numArr[0].intValue());
            }
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
            if (this.mListener != null) {
                this.mListener.onStart();
            }
        }
    }

    public void setConcretePlayerEngine(PlayerEngine playerEngine) {
        this.mServicePlayerEngine = playerEngine;
    }

    public PlayerEngine getPlayerEngineInterface() {
        if (this.mIntentPlayerEngine == null) {
            this.mIntentPlayerEngine = new IntentPlayerEngine(this, null);
        }
        return this.mIntentPlayerEngine;
    }

    public void setPlayerEngineListener(PlayerEngineListener playerEngineListener) {
        getPlayerEngineInterface().setListener(playerEngineListener);
    }

    public void setPlayerSurfaceHolder(SurfaceHolder surfaceHolder) {
        getPlayerEngineInterface().setPlayerSurfaceHolder(surfaceHolder);
    }

    public PlayerEngineListener fetchPlayerEngineListener() {
        return this.mPlayerEngineListener;
    }

    public Playlist getPlaylist() {
        if (this.mPlaylist == null || this.mPlaylist.size() == 0) {
            this.mPlaylist = getOnlinePlaylist();
        }
        return this.mPlaylist;
    }

    public void setPlaylistMode(int i) {
    }

    public Playlist getOnlinePlaylist() {
        if (this.mOnlinePlaylist == null) {
            this.mOnlinePlaylist = new Playlist();
        }
        return this.mOnlinePlaylist;
    }

    public void setOnlinePlaylist(Playlist playlist) {
        this.mOnlinePlaylist = playlist;
    }

    private class IntentPlayerEngine implements PlayerEngine {
        private IntentPlayerEngine() {
        }

        /* synthetic */ IntentPlayerEngine(KShareApplication kShareApplication, IntentPlayerEngine intentPlayerEngine) {
            this();
        }

        public Playlist getPlaylist() {
            return KShareApplication.this.getPlaylist();
        }

        public boolean isPlaying() {
            if (KShareApplication.this.mServicePlayerEngine == null) {
                return false;
            }
            return KShareApplication.this.mServicePlayerEngine.isPlaying();
        }

        public void next() {
            if (KShareApplication.this.mServicePlayerEngine != null) {
                playlistCheck();
                KShareApplication.this.mServicePlayerEngine.next();
                return;
            }
            startAction(PlayerService.ACTION_NEXT);
        }

        public void openPlaylist(Playlist playlist) {
            KShareApplication.this.mPlaylist = playlist;
            if (KShareApplication.this.mServicePlayerEngine != null) {
                KShareApplication.this.mServicePlayerEngine.openPlaylist(playlist);
            }
        }

        public void pause() {
            if (KShareApplication.this.mServicePlayerEngine != null) {
                KShareApplication.this.mServicePlayerEngine.pause();
            }
        }

        public void play() {
            if (KShareApplication.this.mServicePlayerEngine != null) {
                playlistCheck();
                KShareApplication.this.mServicePlayerEngine.play();
                return;
            }
            startAction(PlayerService.ACTION_PLAY);
        }

        public void prev() {
            if (KShareApplication.this.mServicePlayerEngine != null) {
                playlistCheck();
                KShareApplication.this.mServicePlayerEngine.prev();
                return;
            }
            startAction(PlayerService.ACTION_PREV);
        }

        public void setListener(PlayerEngineListener playerEngineListener) {
            KShareApplication.this.mPlayerEngineListener = playerEngineListener;
            if (KShareApplication.this.mServicePlayerEngine != null || KShareApplication.this.mPlayerEngineListener != null) {
                startAction(PlayerService.ACTION_BIND_LISTENER);
            }
        }

        public void skipTo(int i) {
            if (KShareApplication.this.mServicePlayerEngine != null) {
                KShareApplication.this.mServicePlayerEngine.skipTo(i);
            }
        }

        public void stop() {
            startAction(PlayerService.ACTION_STOP);
        }

        private void startAction(String str) {
            if (KShareApplication.this.mApp != null) {
                Intent intent = new Intent(KShareApplication.this.mApp, PlayerService.class);
                intent.setAction(str);
                KShareApplication.this.mApp.startService(intent);
            }
        }

        private void playlistCheck() {
            if (KShareApplication.this.mServicePlayerEngine != null && KShareApplication.this.mServicePlayerEngine.getPlaylist() == null && KShareApplication.this.mPlaylist != null) {
                KShareApplication.this.mServicePlayerEngine.openPlaylist(KShareApplication.this.mPlaylist);
            }
        }

        public void setPlaybackMode(Playlist.PlaylistPlaybackMode playlistPlaybackMode) {
            KShareApplication.this.mPlaylist.setPlaylistPlaybackMode(playlistPlaybackMode);
        }

        public Playlist.PlaylistPlaybackMode getPlaybackMode() {
            return KShareApplication.this.mPlaylist.getPlaylistPlaybackMode();
        }

        public void forward(int i) {
            if (KShareApplication.this.mServicePlayerEngine != null) {
                KShareApplication.this.mServicePlayerEngine.forward(i);
            }
        }

        public void rewind(int i) {
            if (KShareApplication.this.mServicePlayerEngine != null) {
                KShareApplication.this.mServicePlayerEngine.rewind(i);
            }
        }

        public void prevList() {
            if (KShareApplication.this.mServicePlayerEngine != null) {
                KShareApplication.this.mServicePlayerEngine.prevList();
            }
        }

        public void seekToPosition(int i) {
            if (KShareApplication.this.mServicePlayerEngine != null) {
                KShareApplication.this.mServicePlayerEngine.seekToPosition(i);
            }
        }

        public int getDuration() {
            if (KShareApplication.this.mServicePlayerEngine != null) {
                return KShareApplication.this.mServicePlayerEngine.getDuration();
            }
            return 0;
        }

        public boolean isPausing() {
            if (KShareApplication.this.mServicePlayerEngine == null) {
                return false;
            }
            return KShareApplication.this.mServicePlayerEngine.isPausing();
        }

        public void setPlayerSurfaceHolder(SurfaceHolder surfaceHolder) {
            KShareApplication.this.mSurfaceHolder = surfaceHolder;
            if (KShareApplication.this.mServicePlayerEngine != null) {
                KShareApplication.this.mServicePlayerEngine.setPlayerSurfaceHolder(surfaceHolder);
            }
        }

        public MediaPlayer getMyCurrentMedia() {
            if (KShareApplication.this.mServicePlayerEngine != null) {
                return KShareApplication.this.mServicePlayerEngine.getMyCurrentMedia();
            }
            return null;
        }
    }

    public boolean hasInstallAc() {
        return isAvilible(this.mApp, "cn.banshenggua.aichang");
    }

    private boolean isAvilible(Context context, String str) {
        List<PackageInfo> installedPackages = context.getPackageManager().getInstalledPackages(0);
        for (int i = 0; i < installedPackages.size(); i++) {
            if (installedPackages.get(i).packageName.equalsIgnoreCase(str)) {
                return true;
            }
        }
        return false;
    }
}
