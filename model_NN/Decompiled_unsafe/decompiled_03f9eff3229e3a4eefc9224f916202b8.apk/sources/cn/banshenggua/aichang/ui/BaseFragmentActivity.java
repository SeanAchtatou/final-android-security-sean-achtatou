package cn.banshenggua.aichang.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.AttributeSet;
import android.view.View;
import cn.banshenggua.aichang.app.KShareApplication;
import cn.banshenggua.aichang.sdk.ACException;
import com.pocketmusic.kshare.requestobjs.m;
import com.umeng.analytics.b;

public abstract class BaseFragmentActivity extends FragmentActivity {
    public static final String FINISH_ACTIVITY_BROADCAST = "finish_activity_broadcast";
    public static final String UPDATE_DOWNLOAD_NOTIFY_BROADCAST = "update_download_notify_broadcast";
    public static final String UPDATE_NOTIFYMESSAGE_BROADCAST = "update_notifymessage_broadcast";
    public static final String UPDATE_NOTIFY_BROADCAST = "update_notify_broadcast";
    public String TAG = "BaseFragmentActivity";
    public boolean isRunningInBg;

    public /* bridge */ /* synthetic */ View onCreateView(View view, String str, Context context, AttributeSet attributeSet) {
        return super.onCreateView(view, str, context, attributeSet);
    }

    public /* bridge */ /* synthetic */ View onCreateView(String str, Context context, AttributeSet attributeSet) {
        return super.onCreateView(str, context, attributeSet);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.TAG = getClass().getSimpleName();
        try {
            if (!KShareApplication.getInstance().isInitData) {
                KShareApplication.getInstance().initData();
            }
        } catch (ACException e) {
            e.printStackTrace();
        }
    }

    public void onPause() {
        super.onPause();
        this.isRunningInBg = true;
        b.a(this);
    }

    public void onResume() {
        super.onResume();
        this.isRunningInBg = false;
        b.b(this);
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public void processToken(m mVar) {
    }

    public void showMessageNotify() {
    }

    public void showNotify() {
    }
}
