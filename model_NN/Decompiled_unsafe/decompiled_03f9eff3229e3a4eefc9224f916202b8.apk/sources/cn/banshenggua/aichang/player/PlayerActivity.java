package cn.banshenggua.aichang.player;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.TextView;
import cn.a.a.a;
import cn.banshenggua.aichang.app.KShareApplication;
import cn.banshenggua.aichang.room.gift.GiftUtils;
import cn.banshenggua.aichang.sdk.ACException;
import cn.banshenggua.aichang.ui.BaseFragmentActivity;
import cn.banshenggua.aichang.utils.Constants;
import cn.banshenggua.aichang.utils.DateUtil;
import cn.banshenggua.aichang.utils.ImageLoaderUtil;
import cn.banshenggua.aichang.utils.ImageUtil;
import cn.banshenggua.aichang.utils.KShareUtil;
import cn.banshenggua.aichang.utils.Toaster;
import cn.banshenggua.aichang.utils.UIUtil;
import cn.banshenggua.aichang.utils.ULog;
import com.d.a.b.c;
import com.d.a.b.d;
import com.pocketmusic.kshare.requestobjs.e;
import com.pocketmusic.kshare.requestobjs.f;
import com.pocketmusic.kshare.requestobjs.h;
import com.pocketmusic.kshare.requestobjs.q;
import com.pocketmusic.kshare.requestobjs.w;
import com.sina.weibo.sdk.exception.WeiboAuthException;
import org.json.JSONObject;

public class PlayerActivity extends BaseFragmentActivity {
    private static final String FINISH_PLAY_UI = "com.banshenggua.finish";
    private static final int MAX_HEAD_ALPHA = 178;
    public static int MID_HEIGHT = 0;
    /* access modifiers changed from: private */
    public static final String TAG = PlayerActivity.class.getName();
    public static boolean isNeedRefeshSuperFans = false;
    public static boolean isPlay = false;
    private Runnable dismissViewTask = new Runnable() {
        public void run() {
            PlayerActivity.this.dismissView();
        }
    };
    /* access modifiers changed from: private */
    public f giftList;
    /* access modifiers changed from: private */
    public ImageView headBgView;
    /* access modifiers changed from: private */
    public View headLayout;
    /* access modifiers changed from: private */
    public TextView headTitle;
    private TextView headTitle2;
    public w inputWeibo;
    protected boolean isRunningBg = false;
    /* access modifiers changed from: private */
    public PlayerFragmentAdapter mAdapter;
    protected TextView mCurrentTimeTextView;
    private FinishUiReceiver mFinishUiReceiver;
    private Button mForward;
    private Handler mHandler;
    private View.OnClickListener mHeadClick = new View.OnClickListener() {
        public void onClick(View view) {
            PlayerActivity.isPlay = true;
            if (KShareUtil.processAnonymous(PlayerActivity.this, PlayerActivity.this.weibo.b, null)) {
            }
        }
    };
    /* access modifiers changed from: private */
    public ProgressBar mLoadingDialog;
    /* access modifiers changed from: private */
    public View mMidPlayContent;
    private SeekBar.OnSeekBarChangeListener mOnSeekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {
        public void onStopTrackingTouch(SeekBar seekBar) {
        }

        public void onStartTrackingTouch(SeekBar seekBar) {
        }

        public void onProgressChanged(SeekBar seekBar, int i, boolean z) {
            if (z) {
                ULog.d(PlayerActivity.TAG, "onProgressChanged = " + i);
                PlayerActivity.this.getPlayerEngine().seekToPosition(i);
            }
        }
    };
    /* access modifiers changed from: private */
    public ViewPager mPager;
    private View.OnClickListener mPlayOnClickListener = new View.OnClickListener() {
        public void onClick(View view) {
            if (PlayerActivity.this.getPlayerEngine().isPlaying()) {
                PlayerActivity.this.setPlayerButton(true);
                PlayerActivity.this.getPlayerEngine().pause();
                return;
            }
            PlayerActivity.this.setPlayerButton(false);
            PlayerActivity.this.getPlayerEngine().play();
        }
    };
    /* access modifiers changed from: private */
    public PlayerEngineListener mPlayerEngineListener = new PlayerEngineListener() {
        public void onTrackChanged(w wVar) {
            if (PlayerActivity.this.inputWeibo != null) {
                ULog.d(PlayerActivity.TAG, "onTrackChanged");
                ULog.d(PlayerActivity.TAG, "getDuration = " + PlayerActivity.this.getPlayerEngine().getDuration());
                PlayerActivity.this.headTitle.setText(PlayerActivity.this.weibo.A);
                PlayerActivity.this.mCurrentTimeTextView.setText(UIUtil.getInstance().toTime(0));
                PlayerActivity.this.mTimeToSpareTextView.setText(UIUtil.getInstance().toTime((long) PlayerActivity.this.getPlayerEngine().getDuration()));
                PlayerActivity.this.setSeekbarMax();
            }
        }

        public void onTrackProgress(int i) {
            PlayerActivity.this.mCurrentTimeTextView.setText(UIUtil.getInstance().toTime((long) i));
            PlayerActivity.this.mTimeToSpareTextView.setText(UIUtil.getInstance().toTime((long) PlayerActivity.this.getPlayerEngine().getDuration()));
            if (PlayerActivity.this.getPlayerEngine().getDuration() > PlayerActivity.this.mSeekbar.getMax()) {
                int progress = (PlayerActivity.this.mSeekbar.getProgress() * PlayerActivity.this.getPlayerEngine().getDuration()) / PlayerActivity.this.mSeekbar.getMax();
                PlayerActivity.this.mSeekbar.setMax(PlayerActivity.this.getPlayerEngine().getDuration());
                PlayerActivity.this.mSeekbar.setProgress(progress);
            }
            PlayerActivity.this.mSeekbar.setProgress(i);
            if (i > 2 && PlayerActivity.this.mSurfaceView != null && PlayerActivity.this.showPicView.getVisibility() == 0 && PlayerActivity.this.getPlayerEngine().isPlaying()) {
                PlayerActivity.this.showPicView.setVisibility(8);
            }
            if (i > 0 && PlayerActivity.this.mLoadingDialog != null) {
                PlayerActivity.this.mLoadingDialog.setVisibility(8);
                PlayerActivity.this.mLoadingDialog = null;
            }
        }

        public void onTrackBuffering(int i) {
            if (!PlayerActivity.this.isFinishing() && PlayerActivity.this.mSeekbar != null) {
                PlayerActivity.this.mSeekbar.setSecondaryProgress((int) ((((float) i) / 100.0f) * ((float) PlayerActivity.this.mSeekbar.getMax())));
            }
        }

        public void onTrackStop() {
            PlayerActivity.this.setPlayerButton(false);
        }

        public boolean onTrackStart() {
            PlayerActivity.this.setPlayerButton(true);
            return true;
        }

        public void onTrackPause() {
            PlayerActivity.this.setPlayerButton(false);
        }

        public void onTrackStreamError() {
            if (PlayerActivity.this.repeatPlayNum < 2) {
                PlayerActivity.this.getPlayerEngine().play();
                PlayerActivity playerActivity = PlayerActivity.this;
                playerActivity.repeatPlayNum = playerActivity.repeatPlayNum + 1;
            }
        }

        public void onTrackPlay() {
            PlayerActivity.this.setSeekbarMax();
        }

        public void onVideoSizeChange(int i, int i2) {
            if (PlayerActivity.this.mSurfaceView != null) {
                ViewGroup.LayoutParams layoutParams = PlayerActivity.this.mSurfaceView.getLayoutParams();
                if (i > i2) {
                    layoutParams.height = (int) ((((double) i2) / (((double) i) * 1.0d)) * ((double) UIUtil.getInstance().getmScreenWidth()));
                } else if (i2 > i) {
                    layoutParams.width = (int) ((((double) i) / (((double) i2) * 1.0d)) * ((double) UIUtil.getInstance().getmScreenWidth()));
                }
                ULog.d(PlayerActivity.TAG, "onVideoSizeChange: " + layoutParams.width + "x" + layoutParams.height);
                PlayerActivity.this.mSurfaceView.setLayoutParams(layoutParams);
                PlayerActivity.this.mMidPlayContent.setBackgroundColor(PlayerActivity.this.getResources().getColor(a.c.black_ff));
            }
        }
    };
    int mReadFileIndex = 0;
    private Button mReply;
    /* access modifiers changed from: private */
    public ScrollView mScrollView;
    /* access modifiers changed from: private */
    public SeekBar mSeekbar;
    /* access modifiers changed from: private */
    public SurfaceView mSurfaceView;
    protected TextView mTimeToSpareTextView;
    protected boolean onClickFavorite;
    c options = ImageUtil.getOvalDefaultOption();
    private ImageView player_play;
    /* access modifiers changed from: private */
    public int repeatPlayNum = 0;
    /* access modifiers changed from: private */
    public int scrollHeight = 0;
    /* access modifiers changed from: private */
    public PlayImageView showPicView;
    boolean success = false;
    private q superFanslistener = new q() {
        public void onResponse(JSONObject jSONObject) {
            super.onResponse(jSONObject);
            PlayerActivity.this.giftList.a(jSONObject);
            if (PlayerActivity.this.giftList.i() == -1000) {
                PlayerActivity.this.adapteFansList();
            }
        }
    };
    int totalImageView = 0;
    public w weibo;
    private q weiboShowListen = new q() {
        public void onErrorResponse(com.android.volley.w wVar) {
            super.onErrorResponse(wVar);
        }

        public void onResponse(JSONObject jSONObject) {
            super.onResponse(jSONObject);
            PlayerActivity.this.weibo.a(jSONObject);
            if (PlayerActivity.this.weibo.ao != -1000) {
                KShareUtil.showToastJsonStatus(PlayerActivity.this, PlayerActivity.this.weibo);
            } else if (WeiboAuthException.DEFAULT_AUTH_ERROR_CODE.equalsIgnoreCase(PlayerActivity.this.weibo.y)) {
                Toaster.showLong(PlayerActivity.this, "此歌曲已被删除");
                PlayerActivity.this.finish();
            } else {
                PlayerActivity.this.showWeiboInfoView();
                ((Button) PlayerActivity.this.findViewById(a.f.btn_like)).setText(PlayerActivity.this.weibo.J);
                ((TextView) PlayerActivity.this.findViewById(a.f.weibo_listen_num)).setText("收听 " + PlayerActivity.this.weibo.I);
            }
        }
    };

    public /* bridge */ /* synthetic */ View onCreateView(View view, String str, Context context, AttributeSet attributeSet) {
        return super.onCreateView(view, str, context, attributeSet);
    }

    public /* bridge */ /* synthetic */ View onCreateView(String str, Context context, AttributeSet attributeSet) {
        return super.onCreateView(str, context, attributeSet);
    }

    private static class ShowListener extends q {
        private Context c;

        public ShowListener(Context context) {
            this.c = context;
        }
    }

    public static void launch(Context context, w wVar) {
        w f = wVar.f();
        if (f == null || f.P) {
            Toaster.showShortToast(a.h.default_fanchang_delete);
        } else if (wVar != null) {
            try {
                KShareApplication.getInstance().setPlaylistMode(0);
                if (wVar.T == w.c.InfoItem) {
                    wVar.a(new ShowListener(context));
                    wVar.m();
                    return;
                }
                entryPlayer(context, wVar);
            } catch (ACException e) {
                e.printStackTrace();
            }
        }
    }

    private static void entryPlayer(Context context, w wVar) {
        try {
            KShareApplication.getInstance().getPlayerEngineInterface().stop();
            context.sendBroadcast(new Intent(FINISH_PLAY_UI));
            Intent intent = new Intent(context, PlayerActivity.class);
            intent.putExtra(Constants.WEIBO, wVar);
            context.startActivity(intent);
        } catch (ACException e) {
            e.printStackTrace();
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (bundle != null) {
            if (this.weibo == null) {
                this.weibo = (w) bundle.get(Constants.WEIBO);
            }
            if (this.inputWeibo == null) {
                this.inputWeibo = (w) bundle.get(Constants.INPUT_WEIBO);
            }
        }
        if (Boolean.valueOf(getIntent().getBooleanExtra("launchByNotifaction", false)).booleanValue()) {
            sendBroadcast(new Intent(FINISH_PLAY_UI));
        }
        setContentView(a.g.activity_player);
        initWeiBo((w) getIntent().getSerializableExtra(Constants.WEIBO));
        if (this.inputWeibo != null) {
            this.mAdapter = new PlayerFragmentAdapter(getSupportFragmentManager(), this.inputWeibo);
            this.mAdapter.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    if (PlayerActivity.this.headLayout.getVisibility() == 0) {
                        PlayerActivity.this.dismissView();
                    } else {
                        PlayerActivity.this.showView();
                    }
                }
            });
            this.mPager = (ViewPager) findViewById(a.f.pager);
            initHeadView();
            initView();
            initFinishPlayerReceiver();
            this.mHandler = new Handler();
            this.mHandler.postDelayed(this.dismissViewTask, 5000);
            this.mHandler.postAtTime(new Runnable() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: cn.banshenggua.aichang.player.Playlist.addPlaylistEntry(com.pocketmusic.kshare.requestobjs.w, boolean):boolean
                 arg types: [com.pocketmusic.kshare.requestobjs.w, int]
                 candidates:
                  cn.banshenggua.aichang.player.Playlist.addPlaylistEntry(com.pocketmusic.kshare.requestobjs.w, int):void
                  cn.banshenggua.aichang.player.Playlist.addPlaylistEntry(com.pocketmusic.kshare.requestobjs.w, boolean):boolean */
                public void run() {
                    try {
                        PlayerActivity.this.mPager.setAdapter(PlayerActivity.this.mAdapter);
                        PlayerActivity.this.initData();
                        KShareApplication.getInstance().getOnlinePlaylist().addPlaylistEntry(PlayerActivity.this.weibo, true);
                        Playlist playlist = KShareApplication.getInstance().getPlaylist();
                        if (playlist != null && playlist.getSelectedTrack() != null) {
                            PlayerActivity.this.getPlayerEngine().openPlaylist(playlist);
                            if (PlayerActivity.this.getPlayerEngine().isPausing()) {
                                PlayerActivity.this.getPlayerEngine().pause();
                                if (PlayerActivity.this.mPlayerEngineListener != null) {
                                    PlayerActivity.this.mPlayerEngineListener.onTrackPause();
                                    return;
                                }
                                return;
                            }
                            PlayerActivity.this.getPlayerEngine().play();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }, 800);
        }
    }

    private void setSurfaceView() {
        this.mSurfaceView = (SurfaceView) findViewById(a.f.player_video_play);
        onDetachedFromWindow();
        getPlayerEngine().setPlayerSurfaceHolder(this.mSurfaceView.getHolder());
        this.mSurfaceView.getHolder().setType(3);
        this.mSurfaceView.getHolder().setKeepScreenOn(true);
        this.mSurfaceView.getHolder().addCallback(new SurfaceListener(this, null));
    }

    private class SurfaceListener implements SurfaceHolder.Callback {
        private SurfaceListener() {
        }

        /* synthetic */ SurfaceListener(PlayerActivity playerActivity, SurfaceListener surfaceListener) {
            this();
        }

        public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
        }

        public void surfaceCreated(SurfaceHolder surfaceHolder) {
            ULog.i(PlayerActivity.TAG, "surfaceCreated()");
            try {
                if (PlayerActivity.this.mSurfaceView != null && PlayerActivity.this.getPlayerEngine().getMyCurrentMedia() != null) {
                    PlayerActivity.this.getPlayerEngine().setPlayerSurfaceHolder(PlayerActivity.this.mSurfaceView.getHolder());
                    PlayerActivity.this.getPlayerEngine().getMyCurrentMedia().setDisplay(PlayerActivity.this.mSurfaceView.getHolder());
                    if (!PlayerActivity.this.getPlayerEngine().isPlaying()) {
                        PlayerActivity.this.showPicView.setVisibility(0);
                    }
                }
            } catch (Exception e) {
            }
        }

        public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
            ULog.i(PlayerActivity.TAG, "surfaceDestroyed()");
            if (PlayerActivity.this.mSurfaceView != null) {
                PlayerActivity.this.getPlayerEngine().setPlayerSurfaceHolder(null);
                try {
                    if (PlayerActivity.this.getPlayerEngine().getMyCurrentMedia() != null) {
                        PlayerActivity.this.getPlayerEngine().getMyCurrentMedia().setDisplay(null);
                    }
                    if (!PlayerActivity.isPlay) {
                        PlayerActivity.this.getPlayerEngine().pause();
                    }
                    ULog.d(PlayerActivity.TAG, "isPlay = " + PlayerActivity.isPlay);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void onSaveInstanceState(Bundle bundle) {
        if (this.inputWeibo != null) {
            bundle.putSerializable(Constants.INPUT_WEIBO, this.inputWeibo);
        }
        if (this.weibo != null) {
            bundle.putSerializable(Constants.WEIBO, this.weibo);
        }
        super.onSaveInstanceState(bundle);
    }

    public void onResume() {
        super.onResume();
        if (this.inputWeibo == null || this.showPicView == null) {
            finish();
            return;
        }
        this.mHandler.postAtTime(new Runnable() {
            public void run() {
                try {
                    KShareApplication.getInstance().setPlayerEngineListener(PlayerActivity.this.mPlayerEngineListener);
                    if (PlayerActivity.this.getPlayerEngine().getPlaylist().getSelectedTrack() != null) {
                        PlayerActivity.this.mPlayerEngineListener.onTrackChanged(PlayerActivity.this.getPlayerEngine().getPlaylist().getSelectedTrack());
                        if (!PlayerActivity.this.getPlayerEngine().isPlaying() && !PlayerActivity.this.getPlayerEngine().isPausing()) {
                            PlayerActivity.this.showPicView.setVisibility(0);
                            ULog.d(PlayerActivity.TAG, "call playerActivity play");
                            PlayerActivity.this.getPlayerEngine().play();
                        }
                        if (PlayerActivity.this.getPlayerEngine().isPausing()) {
                            PlayerActivity.this.setPlayerButton(false);
                        } else {
                            PlayerActivity.this.setPlayerButton(true);
                        }
                    }
                    if (PlayerActivity.isNeedRefeshSuperFans) {
                        PlayerActivity.this.refeshSuperGiftFans();
                    }
                } catch (ACException e) {
                    e.printStackTrace();
                }
            }
        }, 1000);
        this.isRunningBg = true;
        isPlay = false;
    }

    private void initHeadView() {
        findViewById(a.f.head_back).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                PlayerActivity.this.doFinish();
                PlayerActivity.this.finish();
            }
        });
        findViewById(a.f.head_more_btn).setOnClickListener(this.mHeadClick);
    }

    private void initMidAndPicLayout() {
        MID_HEIGHT = UIUtil.getInstance().getmScreenWidth();
        ViewGroup.LayoutParams layoutParams = this.mMidPlayContent.getLayoutParams();
        layoutParams.height = UIUtil.getInstance().getmScreenWidth();
        this.mMidPlayContent.setLayoutParams(layoutParams);
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"NewApi"})
    public void initView() {
        this.showPicView = (PlayImageView) findViewById(a.f.player_image_play);
        this.mMidPlayContent = findViewById(a.f.playmusic_mid_content);
        initMidAndPicLayout();
        this.mScrollView = (ScrollView) findViewById(a.f.player_scrollview);
        this.headBgView = (ImageView) findViewById(a.f.head_bg);
        this.headBgView.getDrawable().setAlpha(0);
        this.headLayout = findViewById(a.f.head_layout);
        this.scrollHeight = MID_HEIGHT / 3;
        final AnonymousClass12 r0 = new ViewTreeObserver.OnScrollChangedListener() {
            public void onScrollChanged() {
                int scrollY = PlayerActivity.this.mScrollView.getScrollY();
                if (scrollY > 0) {
                    PlayerActivity.this.showView();
                }
                if (scrollY < PlayerActivity.this.scrollHeight || scrollY > PlayerActivity.this.scrollHeight + PlayerActivity.MAX_HEAD_ALPHA) {
                    if (scrollY < PlayerActivity.this.scrollHeight) {
                        PlayerActivity.this.headBgView.getDrawable().setAlpha(0);
                        ULog.d(PlayerActivity.TAG, "setAlpha =0");
                    }
                    if (scrollY > PlayerActivity.this.scrollHeight + PlayerActivity.MAX_HEAD_ALPHA) {
                        PlayerActivity.this.headBgView.getDrawable().setAlpha(PlayerActivity.MAX_HEAD_ALPHA);
                        ULog.d(PlayerActivity.TAG, "setAlpha =178");
                        return;
                    }
                    return;
                }
                PlayerActivity.this.headBgView.getDrawable().setAlpha(scrollY - PlayerActivity.this.scrollHeight);
                ULog.d(PlayerActivity.TAG, "setAlpha =" + (scrollY - PlayerActivity.this.scrollHeight));
            }
        };
        this.mScrollView.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                PlayerActivity.this.mScrollView.getViewTreeObserver().addOnScrollChangedListener(r0);
                return false;
            }
        });
        this.headTitle = (TextView) findViewById(a.f.head_title);
        this.headTitle2 = (TextView) findViewById(a.f.head_title2);
        this.headTitle2.setVisibility(8);
        this.player_play = (ImageView) findViewById(a.f.player_play);
        this.player_play.setOnClickListener(this.mPlayOnClickListener);
        this.mSeekbar = (SeekBar) findViewById(a.f.player_seekbar_time);
        this.mSeekbar.setOnSeekBarChangeListener(this.mOnSeekBarChangeListener);
        this.mCurrentTimeTextView = (TextView) findViewById(a.f.player_tv_currenttime);
        this.mTimeToSpareTextView = (TextView) findViewById(a.f.player_tv_all_time);
        this.mReply = (Button) findViewById(a.f.playmusic_weibo_reply);
        this.mForward = (Button) findViewById(a.f.playmusic_weibo_forward);
        this.mReply.setOnClickListener(this.mHeadClick);
        this.mForward.setOnClickListener(this.mHeadClick);
        findViewById(a.f.follow_btn).setOnClickListener(this.mHeadClick);
        findViewById(a.f.btn_like).setOnClickListener(this.mHeadClick);
        this.mLoadingDialog = (ProgressBar) findViewById(a.f.dialog_progress);
    }

    private void showWeiBoInfo() {
        this.weibo.a(this.weiboShowListen);
        this.weibo.m();
    }

    /* access modifiers changed from: private */
    public void initData() {
        if (this.weibo.a() == w.b.Video) {
            setSurfaceView();
        } else {
            getPlayerEngine().setPlayerSurfaceHolder(null);
            findViewById(a.f.player_video_play).setVisibility(8);
            this.showPicView.setBackgroundDrawable(null);
            this.showPicView.initData(this.weibo);
        }
        this.headTitle2.setText(this.weibo.b());
        showWeiBoInfo();
        setSex((ImageView) findViewById(a.f.player_user_sex), this.weibo.w);
        ((TextView) findViewById(a.f.player_user_nickname)).setText(this.weibo.b());
        ((TextView) findViewById(a.f.player_weibo_signature)).setText(this.weibo.B);
        setForTime((TextView) findViewById(a.f.weibo_time), this.weibo.j);
        showWeiboInfoView();
        initGiftList(this.weibo);
        refeshSuperGiftFans();
    }

    /* access modifiers changed from: private */
    public void refeshSuperGiftFans() {
        isNeedRefeshSuperFans = false;
        this.giftList.a(this.superFanslistener);
        this.giftList.a();
    }

    /* access modifiers changed from: private */
    public void showWeiboInfoView() {
        this.mReply.setText(this.weibo.f);
        try {
            if (this.weibo.Z.equalsIgnoreCase("vip")) {
                findViewById(a.f.player_vip).setVisibility(0);
            }
            d.a().a(h.a(h.a.SIM, Integer.valueOf(this.weibo.p).intValue()), (ImageView) findViewById(a.f.player_user_level), ImageUtil.getDefaultLevelOption());
            if (!TextUtils.isEmpty(this.weibo.W)) {
                ImageLoaderUtil.displayImageBg(findViewById(a.f.player_user_auth), this.weibo.Y, ImageUtil.getDefaultLevelOption());
                findViewById(a.f.player_user_auth).setVisibility(0);
                TextView textView = (TextView) findViewById(a.f.player_user_level_text);
                textView.setText(this.weibo.W);
                textView.setTextColor(getResources().getColor(a.c.red_ff));
                return;
            }
            findViewById(a.f.player_user_auth).setVisibility(8);
            ((TextView) findViewById(a.f.player_user_level_text)).setText(String.valueOf(getResources().getString(a.h.zone_info_level, Integer.valueOf(this.weibo.p))) + "    " + this.weibo.q);
        } catch (Exception e) {
        }
    }

    private void setForTime(TextView textView, String str) {
        long j;
        try {
            j = Long.parseLong(str);
        } catch (Exception e) {
            j = 0;
        }
        textView.setText(DateUtil.converDayTime(j));
    }

    private void setSex(ImageView imageView, int i) {
        if (i == 1) {
            imageView.setImageResource(a.e.zone_image_boy);
        } else {
            imageView.setImageResource(a.e.zone_image_girl);
        }
    }

    private boolean isActivityFinishing() {
        if (this == null || isFinishing()) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: private */
    @SuppressLint({"InflateParams"})
    public void adapteFansList() {
        if (this.giftList != null) {
            LinearLayout linearLayout = (LinearLayout) findViewById(a.f.player_fans_list_layout);
            linearLayout.removeAllViews();
            for (e creatView : this.giftList.j) {
                View creatView2 = creatView(creatView);
                if (creatView2 != null) {
                    linearLayout.addView(creatView2);
                }
            }
            if (this.giftList.j.size() > 0) {
                TextView textView = (TextView) findViewById(a.f.player_all_fans_gift_btn);
                textView.setText(a.h.player_all_fans_gift_btn);
                textView.setOnClickListener(this.mHeadClick);
            }
        }
    }

    @SuppressLint({"InflateParams"})
    private View creatView(e eVar) {
        e.C0014e eVar2 = eVar.q;
        if (eVar2 == null) {
            return null;
        }
        View inflate = LayoutInflater.from(this).inflate(a.g.item_player_super_fans, (ViewGroup) null);
        TextView textView = (TextView) inflate.findViewById(a.f.player_fans_name);
        TextView textView2 = (TextView) inflate.findViewById(a.f.player_fans_gift_value);
        ImageView imageView = (ImageView) inflate.findViewById(a.f.player_fans_gender);
        ImageView imageView2 = (ImageView) inflate.findViewById(a.f.player_fans_face);
        imageView2.setTag(eVar.q.f615a);
        imageView2.setOnClickListener(this.mHeadClick);
        d.a().a(eVar.q.e, imageView2, this.options);
        e gift = GiftUtils.getGift(eVar.b);
        if (gift != null) {
            d.a().a(gift.e, (ImageView) inflate.findViewById(a.f.player_gift_img));
        } else {
            d.a().a(eVar.e, (ImageView) inflate.findViewById(a.f.player_gift_img));
        }
        d.a().a(h.a(h.a.SIM, eVar2.h), (ImageView) inflate.findViewById(a.f.player_fans_level));
        textView.setText(eVar2.a());
        textView2.setText("粉丝贡献:  " + eVar.p);
        setSex(imageView, eVar2.d);
        if ("vip".equalsIgnoreCase(eVar2.m)) {
            inflate.findViewById(a.f.player_fans_vip).setVisibility(0);
        } else if (!TextUtils.isEmpty(eVar2.i)) {
            d.a().a(eVar2.i, (ImageView) inflate.findViewById(a.f.player_fans_auth));
        }
        return inflate;
    }

    private void initFinishPlayerReceiver() {
        if (this.mFinishUiReceiver == null) {
            this.mFinishUiReceiver = new FinishUiReceiver(this, null);
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(FINISH_PLAY_UI);
            try {
                registerReceiver(this.mFinishUiReceiver, intentFilter);
            } catch (Exception e) {
            }
        }
    }

    private void unregisterReceiver() {
        try {
            if (this.mFinishUiReceiver != null) {
                unregisterReceiver(this.mFinishUiReceiver);
            }
        } catch (Exception e) {
        }
    }

    private class FinishUiReceiver extends BroadcastReceiver {
        private FinishUiReceiver() {
        }

        /* synthetic */ FinishUiReceiver(PlayerActivity playerActivity, FinishUiReceiver finishUiReceiver) {
            this();
        }

        public void onReceive(Context context, Intent intent) {
            if (PlayerActivity.this.isRunningBg) {
                PlayerActivity.this.finish();
            }
        }
    }

    public void onDestroy() {
        super.onDestroy();
        doFinish();
    }

    public void onBackPressed() {
        doFinish();
        super.onBackPressed();
    }

    public void doFinish() {
        this.mSurfaceView = null;
        getPlayerEngine().setPlayerSurfaceHolder(null);
        getPlayerEngine().stop();
        unregisterReceiver();
        if (this.showPicView != null) {
            this.showPicView.cancelTimer();
        }
        this.showPicView = null;
    }

    /* access modifiers changed from: private */
    public PlayerEngine getPlayerEngine() {
        try {
            return KShareApplication.getInstance().getPlayerEngineInterface();
        } catch (ACException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void onPause() {
        super.onPause();
        ULog.i(TAG, "PlayerActivity.onPause");
        try {
            KShareApplication.getInstance().setPlayerEngineListener(null);
        } catch (ACException e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void setPlayerButton(boolean z) {
        if (z) {
            this.player_play.setImageResource(a.e.player_video_playing_v3);
        } else {
            this.player_play.setImageResource(a.e.player_video_pause_v3);
        }
    }

    /* access modifiers changed from: private */
    public void setSeekbarMax() {
        ULog.d(TAG, "getDuration = " + getPlayerEngine().getDuration());
        if (getPlayerEngine().getDuration() > 0) {
            this.mSeekbar.setMax(getPlayerEngine().getDuration());
            this.mSeekbar.setProgress(0);
        }
    }

    private void initWeiBo(w wVar) {
        if (wVar == null) {
            wVar = getPlayerEngine().getPlaylist().getSelectedTrack();
        }
        if (wVar != null) {
            try {
                w wVar2 = (w) wVar.clone();
                this.inputWeibo = wVar2;
                if (wVar2.C == null) {
                    this.weibo = wVar2;
                } else {
                    this.weibo = wVar2.C;
                }
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
                finish();
            }
        }
    }

    private void initGiftList(w wVar) {
        this.giftList = new f(f.a.FanChangGiftTopUser);
        this.giftList.i = wVar.b;
        this.giftList.m = wVar.y;
    }

    public void showView() {
        removeCallbacks();
        if (this.headLayout.getVisibility() != 0) {
            this.headLayout.setAnimation(AnimationUtils.loadAnimation(this, a.C0001a.alpha_in));
            this.headLayout.setVisibility(0);
            View findViewById = findViewById(a.f.player_play_layout);
            findViewById.setAnimation(AnimationUtils.loadAnimation(this, a.C0001a.alpha_in));
            findViewById.setVisibility(0);
        }
    }

    private void removeCallbacks() {
        if (this.dismissViewTask != null) {
            this.mHandler.removeCallbacks(this.dismissViewTask);
            this.dismissViewTask = null;
        }
    }

    public void dismissView() {
        removeCallbacks();
        if (this.headLayout != null) {
            this.headLayout.setAnimation(AnimationUtils.loadAnimation(this, a.C0001a.alpha_out));
            this.headLayout.setVisibility(8);
        }
        View findViewById = findViewById(a.f.player_play_layout);
        findViewById.setAnimation(AnimationUtils.loadAnimation(this, a.C0001a.alpha_out));
        findViewById.setVisibility(8);
    }
}
