package cn.banshenggua.aichang.player;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.View;
import com.pocketmusic.kshare.requestobjs.w;

class PlayerFragmentAdapter extends FragmentPagerAdapter {
    private w inputWeibo;
    private int mCount = 1;
    private PlayerPhotoFragment mPlayerPhotoFragment;
    private View.OnClickListener userPhotoAreaOnClickListener;

    public PlayerFragmentAdapter(FragmentManager fragmentManager, w wVar) {
        super(fragmentManager);
        this.inputWeibo = wVar;
    }

    public Fragment getItem(int i) {
        if (this.mPlayerPhotoFragment != null) {
            return this.mPlayerPhotoFragment;
        }
        PlayerPhotoFragment newInstance = PlayerPhotoFragment.newInstance(this.inputWeibo, this.userPhotoAreaOnClickListener);
        this.mPlayerPhotoFragment = newInstance;
        return newInstance;
    }

    public int getCount() {
        return this.mCount;
    }

    public void resetData(w wVar) {
        this.inputWeibo = wVar;
        if (this.mPlayerPhotoFragment != null) {
            this.mPlayerPhotoFragment.resetData(wVar);
        }
    }

    public PlayerPhotoFragment getmPlayerPhotoFragment() {
        return this.mPlayerPhotoFragment;
    }

    public void setOnClickListener(View.OnClickListener onClickListener) {
        this.userPhotoAreaOnClickListener = onClickListener;
    }
}
