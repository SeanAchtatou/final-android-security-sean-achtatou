package cn.banshenggua.aichang.player;

import com.pocketmusic.kshare.requestobjs.w;

public interface PlayerEngineListener {
    void onTrackBuffering(int i);

    void onTrackChanged(w wVar);

    void onTrackPause();

    void onTrackPlay();

    void onTrackProgress(int i);

    boolean onTrackStart();

    void onTrackStop();

    void onTrackStreamError();

    void onVideoSizeChange(int i, int i2);
}
