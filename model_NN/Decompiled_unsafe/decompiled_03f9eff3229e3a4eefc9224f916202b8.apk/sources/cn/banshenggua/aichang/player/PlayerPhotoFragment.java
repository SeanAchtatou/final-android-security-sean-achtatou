package cn.banshenggua.aichang.player;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import cn.a.a.a;
import cn.banshenggua.aichang.utils.ImageUtil;
import cn.banshenggua.aichang.utils.KShareUtil;
import com.d.a.b.c;
import com.d.a.b.d;
import com.pocketmusic.kshare.requestobjs.Song;
import com.pocketmusic.kshare.requestobjs.w;
import com.qq.e.comm.constants.ErrorCode;

public final class PlayerPhotoFragment extends Fragment implements View.OnClickListener {
    private static final String KEY_CONTENT = "TestFragment:Content";
    private static final String KEY_CONTENT_INPUT = "TestFragment:ContentInput";
    protected final int DOWNLOAD_FINISH = ErrorCode.InitError.GET_INTERFACE_ERROR;
    protected final int DOWNLOAD_PROGRESS = ErrorCode.InitError.INIT_ADMANGER_ERROR;
    protected final int DOWNLOAD_START = ErrorCode.InitError.INIT_PLUGIN_ERROR;
    protected final String TAG = "PlayerPhotoFragment";
    protected int error_num = 0;
    private Button iWantSingBtn;
    private d imgLoader;
    private w inputWeiBo = null;
    private String lyricContent = null;
    private ImageView mInviteUserHead;
    private ImageView mUserHead;
    private boolean noDownloaded = false;
    c options = ImageUtil.getOvalDefaultOption();
    private Song song;
    private View.OnClickListener userPhotoAreaOnClickListener;
    private w weibo = null;

    public static PlayerPhotoFragment newInstance(w wVar, View.OnClickListener onClickListener) {
        PlayerPhotoFragment playerPhotoFragment = new PlayerPhotoFragment();
        playerPhotoFragment.initWeiBo(wVar);
        playerPhotoFragment.userPhotoAreaOnClickListener = onClickListener;
        return playerPhotoFragment;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (bundle != null) {
            if (bundle.containsKey(KEY_CONTENT)) {
                this.weibo = (w) bundle.getSerializable(KEY_CONTENT);
            }
            if (bundle.containsKey(KEY_CONTENT_INPUT)) {
                this.inputWeiBo = (w) bundle.getSerializable(KEY_CONTENT_INPUT);
            }
        }
    }

    @SuppressLint({"InflateParams"})
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        this.imgLoader = d.a();
        ViewGroup viewGroup2 = (ViewGroup) layoutInflater.inflate(a.g.player_photo_viewpager, (ViewGroup) null);
        this.mUserHead = (ImageView) getActivity().findViewById(a.f.player_user_photo);
        this.mUserHead.setOnClickListener(this);
        this.mInviteUserHead = (ImageView) getActivity().findViewById(a.f.player_user_photo_invite);
        this.mInviteUserHead.setOnClickListener(this);
        this.iWantSingBtn = (Button) getActivity().findViewById(a.f.btn_i_want_singing);
        this.iWantSingBtn.setOnClickListener(this);
        initData();
        if (this.userPhotoAreaOnClickListener != null) {
            viewGroup2.setOnClickListener(this.userPhotoAreaOnClickListener);
        }
        return viewGroup2;
    }

    public void onStart() {
        super.onStart();
        initData();
    }

    public void initData() throws OutOfMemoryError {
        w g = this.inputWeiBo.g();
        if (g == null) {
            getActivity().findViewById(a.f.user_info_invite).setVisibility(8);
        } else if (this.weibo.d() == w.a.INVITE || g.c()) {
            getActivity().findViewById(a.f.user_info_invite).setVisibility(8);
        } else {
            getActivity().findViewById(a.f.user_info_invite).setVisibility(0);
            this.imgLoader.a(g.n, this.mInviteUserHead, this.options);
            ((TextView) getActivity().findViewById(a.f.player_user_nickname_invite)).setText(g.b());
        }
        this.imgLoader.a(this.weibo.n, this.mUserHead, this.options);
    }

    public void initWeiBo(w wVar) {
        this.inputWeiBo = wVar;
        this.weibo = wVar;
        if (wVar.C != null) {
            this.weibo = wVar.C;
        }
    }

    public void resetData(w wVar) {
        this.lyricContent = null;
        initWeiBo(wVar);
        initData();
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putSerializable(KEY_CONTENT, this.weibo);
        bundle.putSerializable(KEY_CONTENT_INPUT, this.inputWeiBo);
    }

    private void launchUserZoneActivity() {
        w wVar = this.inputWeiBo.C != null ? this.inputWeiBo.C : this.inputWeiBo;
        new com.pocketmusic.kshare.requestobjs.a(wVar.c).b = wVar.b;
        KShareUtil.processAnonymous(getActivity(), wVar.b, null);
    }

    private void launchInviteUserZoneActivity() {
        w g = this.inputWeiBo.g();
        if (g != null && !g.c()) {
            KShareUtil.processAnonymous(getActivity(), g.b, null);
        }
    }

    public void onClick(View view) {
        KShareUtil.processAnonymous(getActivity(), this.inputWeiBo.b, null);
    }
}
