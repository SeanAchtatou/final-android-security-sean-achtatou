package cn.banshenggua.aichang.player;

import android.os.CountDownTimer;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

public class OnSeekToListenerImp implements View.OnTouchListener {
    private static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$player$SeekToMode = null;
    private static final int COUNT_DOWN_INTERVAL = 200;
    private static final int HOLD_BUTTON_THRESHOLD = 500;
    private static final int INIT_SEEK_TO_STEP = 500;
    private static final int MILIS_IN_FUTURE = 50000;
    long endTime = 0;
    PlayerActivity mPlayerActivity;
    PlayerEngine mPlayerEngine;
    double mSeekAccelaration;
    CountDownTimer mSeekTimer;
    SeekToMode mSeekToMode;
    long startTime = 0;
    int stepOfSeekTo;

    static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$player$SeekToMode() {
        int[] iArr = $SWITCH_TABLE$cn$banshenggua$aichang$player$SeekToMode;
        if (iArr == null) {
            iArr = new int[SeekToMode.values().length];
            try {
                iArr[SeekToMode.EForward.ordinal()] = 2;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[SeekToMode.ERewind.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            $SWITCH_TABLE$cn$banshenggua$aichang$player$SeekToMode = iArr;
        }
        return iArr;
    }

    public OnSeekToListenerImp(PlayerActivity playerActivity, PlayerEngine playerEngine, SeekToMode seekToMode) {
        this.mPlayerActivity = playerActivity;
        this.mPlayerEngine = playerEngine;
        this.mSeekToMode = seekToMode;
        this.mSeekTimer = new CountDownTimer(50000, 200) {
            private static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$player$SeekToMode;

            static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$player$SeekToMode() {
                int[] iArr = $SWITCH_TABLE$cn$banshenggua$aichang$player$SeekToMode;
                if (iArr == null) {
                    iArr = new int[SeekToMode.values().length];
                    try {
                        iArr[SeekToMode.EForward.ordinal()] = 2;
                    } catch (NoSuchFieldError e) {
                    }
                    try {
                        iArr[SeekToMode.ERewind.ordinal()] = 1;
                    } catch (NoSuchFieldError e2) {
                    }
                    $SWITCH_TABLE$cn$banshenggua$aichang$player$SeekToMode = iArr;
                }
                return iArr;
            }

            public void onTick(long j) {
                if (50000 - j > 500) {
                    switch ($SWITCH_TABLE$cn$banshenggua$aichang$player$SeekToMode()[OnSeekToListenerImp.this.mSeekToMode.ordinal()]) {
                        case 1:
                            OnSeekToListenerImp.this.mPlayerEngine.rewind(OnSeekToListenerImp.this.stepOfSeekTo);
                            break;
                        case 2:
                            OnSeekToListenerImp.this.mPlayerEngine.forward(OnSeekToListenerImp.this.stepOfSeekTo);
                            break;
                        default:
                            Log.e("Timer", "This shouldn't happen");
                            break;
                    }
                    if (OnSeekToListenerImp.this.stepOfSeekTo < 5000) {
                        OnSeekToListenerImp.this.stepOfSeekTo += 100;
                    }
                }
            }

            public void onFinish() {
            }
        };
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            this.startTime = System.currentTimeMillis();
            this.mSeekTimer.start();
            this.mPlayerEngine.pause();
            this.stepOfSeekTo = 500;
        } else if (motionEvent.getAction() == 1) {
            this.mSeekTimer.cancel();
            this.mPlayerEngine.pause();
            this.endTime = System.currentTimeMillis();
            if (this.endTime - this.startTime < 500) {
                switch ($SWITCH_TABLE$cn$banshenggua$aichang$player$SeekToMode()[this.mSeekToMode.ordinal()]) {
                    case 1:
                        this.mPlayerEngine.prev();
                        break;
                    case 2:
                        this.mPlayerEngine.next();
                        break;
                    default:
                        Log.e("Timer", "This shouldn't happen");
                        break;
                }
            } else {
                this.mPlayerEngine.play();
            }
        }
        return true;
    }
}
