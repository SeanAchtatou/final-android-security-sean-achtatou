package cn.banshenggua.aichang.player;

import android.util.Log;
import cn.banshenggua.aichang.utils.ULog;
import com.pocketmusic.kshare.requestobjs.w;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

public class Playlist implements Serializable {
    private static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$player$Playlist$PlaylistPlaybackMode = null;
    public static final int PLAYLIST_MAX_SIZE = 100;
    private static final String TAG = "Playlist";
    private static final long serialVersionUID = 1;
    private PlaylistPlaybackMode mPlaylistPlaybackMode = PlaylistPlaybackMode.NORMAL;
    protected ArrayList<w> playlist = null;
    protected int selected = -1;

    public enum PlaylistPlaybackMode {
        NORMAL,
        SHUFFLE,
        REPEAT,
        SHUFFLE_AND_REPEAT
    }

    static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$player$Playlist$PlaylistPlaybackMode() {
        int[] iArr = $SWITCH_TABLE$cn$banshenggua$aichang$player$Playlist$PlaylistPlaybackMode;
        if (iArr == null) {
            iArr = new int[PlaylistPlaybackMode.values().length];
            try {
                iArr[PlaylistPlaybackMode.NORMAL.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[PlaylistPlaybackMode.REPEAT.ordinal()] = 3;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[PlaylistPlaybackMode.SHUFFLE.ordinal()] = 2;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[PlaylistPlaybackMode.SHUFFLE_AND_REPEAT.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            $SWITCH_TABLE$cn$banshenggua$aichang$player$Playlist$PlaylistPlaybackMode = iArr;
        }
        return iArr;
    }

    public PlaylistPlaybackMode getPlaylistPlaybackMode() {
        return this.mPlaylistPlaybackMode;
    }

    public void setPlaylistPlaybackMode(PlaylistPlaybackMode playlistPlaybackMode) {
        if (Log.isLoggable(TAG, 3)) {
            Log.d(TAG, "(Set mode) selected = " + this.selected);
            Log.d(TAG, "Plyback mode set on: " + playlistPlaybackMode);
        }
        boolean z = false;
        switch ($SWITCH_TABLE$cn$banshenggua$aichang$player$Playlist$PlaylistPlaybackMode()[playlistPlaybackMode.ordinal()]) {
            case 1:
            case 3:
                if (this.mPlaylistPlaybackMode == PlaylistPlaybackMode.SHUFFLE || this.mPlaylistPlaybackMode == PlaylistPlaybackMode.SHUFFLE_AND_REPEAT) {
                    z = true;
                    break;
                }
            case 2:
            case 4:
                if (this.mPlaylistPlaybackMode == PlaylistPlaybackMode.NORMAL || this.mPlaylistPlaybackMode == PlaylistPlaybackMode.REPEAT) {
                    z = true;
                    break;
                }
        }
        this.mPlaylistPlaybackMode = playlistPlaybackMode;
        calculateOrder(z);
    }

    public ArrayList<w> getArraylist() {
        return this.playlist;
    }

    public void setArraylist(ArrayList<w> arrayList) {
        this.playlist = arrayList;
    }

    public Playlist() {
        if (Log.isLoggable(TAG, 3)) {
            Log.d(TAG, "Playlist constructor start");
        }
        this.playlist = new ArrayList<>();
        calculateOrder(true);
        if (Log.isLoggable(TAG, 3)) {
            Log.d(TAG, "Playlist constructor stop");
        }
    }

    public boolean isEmpty() {
        return this.playlist.size() == 0;
    }

    public void selectNext() {
        if (!isEmpty()) {
            this.selected++;
            this.selected %= this.playlist.size();
            ULog.d(TAG, "Current (next) selected = " + this.selected);
        }
    }

    public void selectPrev() {
        if (!isEmpty()) {
            this.selected--;
            if (this.selected < 0) {
                this.selected = this.playlist.size() - 1;
            }
        }
        ULog.d(TAG, "Current (prev) selected = " + this.selected);
    }

    public void select(int i) {
        if (!isEmpty() && i >= 0 && i < this.playlist.size()) {
            this.selected = i;
        }
    }

    public int getSelectedIndex() {
        if (isEmpty()) {
            this.selected = -1;
        }
        if (this.selected == -1 && !isEmpty()) {
            this.selected = 0;
        }
        if (this.selected >= this.playlist.size()) {
            this.selected -= this.playlist.size();
        }
        return this.selected;
    }

    public w getSelectedTrack() {
        ULog.d(TAG, "getSelectedTrack");
        int selectedIndex = getSelectedIndex();
        if (selectedIndex == -1) {
            return null;
        }
        w wVar = this.playlist.get(selectedIndex);
        ULog.d(TAG, "index = " + selectedIndex + "; song name = " + wVar.A + "-" + wVar.x);
        return wVar;
    }

    public boolean addPlaylistEntry(w wVar, boolean z) {
        if (wVar == null) {
            return false;
        }
        w hasInPlayList = hasInPlayList(wVar);
        if (hasInPlayList != null) {
            select(this.playlist.indexOf(hasInPlayList));
            return false;
        }
        this.playlist.clear();
        this.playlist.add(0, wVar);
        select(0);
        return true;
    }

    private w hasInPlayList(w wVar) {
        String str;
        String str2;
        if (wVar == null) {
            return null;
        }
        Iterator<w> it = this.playlist.iterator();
        while (it.hasNext()) {
            w next = it.next();
            if (wVar.C != null) {
                str = wVar.C.f635a;
            } else {
                str = wVar.f635a;
            }
            if (next.C != null) {
                str2 = next.C.f635a;
            } else {
                str2 = next.f635a;
            }
            if (str2.equals(str)) {
                return next;
            }
        }
        return null;
    }

    public void addPlaylistEntry(w wVar, int i) {
        if (wVar == null) {
            return;
        }
        if (this.playlist.contains(wVar)) {
            select(this.playlist.indexOf(wVar));
            return;
        }
        this.playlist.add(i, wVar);
        select(0);
    }

    public int size() {
        if (this.playlist == null) {
            return 0;
        }
        return this.playlist.size();
    }

    public w getWeiBo(int i) {
        return this.playlist.get(i);
    }

    public void remove(int i) {
        if (this.playlist != null && i < this.playlist.size() && i >= 0) {
            if (this.selected >= i) {
                this.selected--;
            }
            this.playlist.remove(i);
        }
    }

    public void remove(w wVar) {
        if (this.playlist.contains(wVar)) {
            remove(this.playlist.indexOf(wVar));
        }
    }

    public void removeAll() {
        this.playlist.clear();
    }

    public void insert(w wVar, int i) {
        if (this.playlist != null && i < this.playlist.size() + 1 && i >= 0) {
            if (this.selected >= i) {
                this.selected++;
            }
            this.playlist.add(i, wVar);
        }
    }

    private void calculateOrder(boolean z) {
        if (z) {
            if (this.mPlaylistPlaybackMode == null) {
                this.mPlaylistPlaybackMode = PlaylistPlaybackMode.NORMAL;
            }
            if (Log.isLoggable(TAG, 3)) {
                Log.d(TAG, "Playlist has been maped in " + this.mPlaylistPlaybackMode + " mode.");
            }
            switch ($SWITCH_TABLE$cn$banshenggua$aichang$player$Playlist$PlaylistPlaybackMode()[this.mPlaylistPlaybackMode.ordinal()]) {
                case 1:
                case 3:
                default:
                    return;
                case 2:
                case 4:
                    this.selected = new Random().nextInt(this.playlist.size());
                    return;
            }
        }
    }

    public boolean isLastWeiBoOnList() {
        if (this.selected == size() - 1) {
            return true;
        }
        return false;
    }

    public boolean limitPlaylistSize(Boolean bool) {
        if (this.playlist.size() <= 100) {
            return false;
        }
        this.playlist.remove(this.playlist.get(this.playlist.size() - 1));
        return true;
    }
}
