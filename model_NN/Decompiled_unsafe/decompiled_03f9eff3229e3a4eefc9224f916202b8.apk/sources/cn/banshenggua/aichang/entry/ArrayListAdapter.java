package cn.banshenggua.aichang.entry;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import java.util.ArrayList;
import java.util.List;

public abstract class ArrayListAdapter<T> extends BaseAdapter {
    private static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$entry$ArrayListAdapter$ScorllStatus = null;
    public static final int DEFAULT_MAX = 100;
    private boolean isLimit = false;
    private boolean keepHead = false;
    protected List<T> mBufferList;
    public Activity mContext;
    protected LayoutInflater mInflater;
    protected List<T> mList = new ArrayList();
    private int mMax = 100;
    private OnNotifyDataSetChangedListener mNotifyListener = null;
    private int mOffset = 1;
    private ScorllStatus mScorllStatus = ScorllStatus.Normal;
    private boolean useBufferForScorll = false;

    public interface OnNotifyDataSetChangedListener {
        void onNotifyDataSetChangedBefore();

        void onNotifyDataSetChangedEnd();
    }

    public enum ScorllStatus {
        Normal,
        Scorlling,
        ScorllStep
    }

    public abstract View getView(int i, View view, ViewGroup viewGroup);

    static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$entry$ArrayListAdapter$ScorllStatus() {
        int[] iArr = $SWITCH_TABLE$cn$banshenggua$aichang$entry$ArrayListAdapter$ScorllStatus;
        if (iArr == null) {
            iArr = new int[ScorllStatus.values().length];
            try {
                iArr[ScorllStatus.Normal.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[ScorllStatus.ScorllStep.ordinal()] = 3;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[ScorllStatus.Scorlling.ordinal()] = 2;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$cn$banshenggua$aichang$entry$ArrayListAdapter$ScorllStatus = iArr;
        }
        return iArr;
    }

    public ArrayListAdapter(Activity activity) {
        this.mContext = activity;
        this.mInflater = LayoutInflater.from(activity);
    }

    public void setLimit(int i, boolean z) {
        this.isLimit = true;
        this.mMax = i;
        this.keepHead = z;
    }

    public void setOnNotifyDataSetChangedListener(OnNotifyDataSetChangedListener onNotifyDataSetChangedListener) {
        this.mNotifyListener = onNotifyDataSetChangedListener;
    }

    public List<T> getList() {
        return this.mList;
    }

    public void clearItem() {
        this.mList.clear();
    }

    public void refreshUI(List<T> list) {
        if (list != null) {
            this.mList.clear();
            this.mList.addAll(list);
            processLimit();
            notifyDataSetChanged();
        }
    }

    private void processLimit() {
        if (this.isLimit && this.mList.size() > this.mMax) {
            if (this.keepHead) {
                this.mList.subList(this.mMax, this.mList.size()).clear();
            } else {
                this.mList.subList(0, this.mList.size() - this.mMax).clear();
            }
        }
    }

    public void addItem(T t, boolean z) {
        if (t != null) {
            if (!processScorllBuffer(null, t)) {
                this.mList.add(t);
            }
            processLimit();
        }
        if (z) {
            notifyDataSetChanged();
        }
    }

    public void removeItem(T t, boolean z) {
        if (t != null) {
            this.mList.remove(t);
        }
        if (z) {
            notifyDataSetChanged();
        }
    }

    public void addItem(List<T> list) {
        if (list != null) {
            if (list.size() > 0) {
                if (!processScorllBuffer(list, null)) {
                    this.mList.addAll(list);
                }
                processLimit();
            }
            notifyDataSetChanged();
        }
    }

    public void addItem(int i, List<T> list) {
        if (list != null) {
            if (list.size() > 0) {
                this.mList.addAll(i, list);
                processLimit();
            }
            notifyDataSetChanged();
        }
    }

    private boolean processScorllBuffer(List<T> list, T t) {
        if (!this.useBufferForScorll) {
            return false;
        }
        if (this.mBufferList == null) {
            this.mBufferList = new ArrayList();
        }
        switch ($SWITCH_TABLE$cn$banshenggua$aichang$entry$ArrayListAdapter$ScorllStatus()[this.mScorllStatus.ordinal()]) {
            case 1:
                if (this.mBufferList != null && this.mBufferList.size() > 0) {
                    this.mList.addAll(this.mBufferList);
                    this.mBufferList.clear();
                }
                if (list != null && list.size() > 0) {
                    this.mList.addAll(list);
                }
                if (t != null) {
                    this.mList.add(t);
                    break;
                }
                break;
            case 2:
            case 3:
                if (list != null && list.size() > 0) {
                    this.mBufferList.addAll(list);
                }
                if (t != null) {
                    this.mBufferList.add(t);
                    break;
                }
                break;
        }
        return true;
    }

    public int getItemNum() {
        if (this.mList != null) {
            return this.mList.size();
        }
        return 0;
    }

    public int getOffset() {
        return this.mOffset;
    }

    public void setOffset(int i) {
        this.mOffset = i;
    }

    public int getCount() {
        if (this.mList != null) {
            return this.mList.size();
        }
        return 0;
    }

    public Object getItem(int i) {
        if (this.mList == null) {
            return null;
        }
        return this.mList.get(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public void notifyDataSetChanged() {
        if (this.mNotifyListener != null) {
            this.mNotifyListener.onNotifyDataSetChangedBefore();
        }
        super.notifyDataSetChanged();
        if (this.mNotifyListener != null) {
            this.mNotifyListener.onNotifyDataSetChangedEnd();
        }
    }

    public void setUseBufferForScorll(boolean z) {
        this.useBufferForScorll = z;
    }

    public void setScorllStatus(ScorllStatus scorllStatus) {
        this.mScorllStatus = scorllStatus;
        processScorllBuffer(null, null);
        processLimit();
    }
}
