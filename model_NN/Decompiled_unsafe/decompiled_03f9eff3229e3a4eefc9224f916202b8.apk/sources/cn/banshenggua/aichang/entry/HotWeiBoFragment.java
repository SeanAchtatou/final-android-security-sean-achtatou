package cn.banshenggua.aichang.entry;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import cn.a.a.a;
import cn.banshenggua.aichang.room.message.LiveMessage;
import cn.banshenggua.aichang.utils.KShareUtil;
import cn.banshenggua.aichang.utils.Toaster;
import cn.banshenggua.aichang.utils.UIUtil;
import cn.banshenggua.aichang.widget.PullToRefreshBase;
import cn.banshenggua.aichang.widget.PullToRefreshListView;
import com.android.volley.w;
import com.pocketmusic.kshare.requestobjs.q;
import com.pocketmusic.kshare.requestobjs.x;
import java.util.List;
import org.json.JSONObject;

@SuppressLint({"InflateParams"})
public class HotWeiBoFragment extends BaseFragment implements PullToRefreshBase.OnRefreshListener2 {
    private static final String KEY_CONTENT = "type";
    private ViewGroup container;
    private View headView;
    boolean isPullDown = false;
    /* access modifiers changed from: private */
    public HotWeiboAdapter mAdapter;
    private ListView mHotListView;
    /* access modifiers changed from: private */
    public PullToRefreshListView mHotRefreshListView;
    private q mRequestListener = new q() {
        public void onErrorResponse(w wVar) {
            super.onErrorResponse(wVar);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: cn.banshenggua.aichang.entry.ArrayListAdapter.addItem(int, java.util.List<cn.banshenggua.aichang.room.message.LiveMessage>):void
         arg types: [int, java.util.List<com.pocketmusic.kshare.requestobjs.w>]
         candidates:
          cn.banshenggua.aichang.entry.ArrayListAdapter.addItem(cn.banshenggua.aichang.room.message.LiveMessage, boolean):void
          cn.banshenggua.aichang.entry.ArrayListAdapter.addItem(int, java.util.List<cn.banshenggua.aichang.room.message.LiveMessage>):void */
        public void onResponse(JSONObject jSONObject) {
            HotWeiBoFragment.this.mHotRefreshListView.onRefreshComplete();
            HotWeiBoFragment.this.mWeiBoList.a(jSONObject);
            if (HotWeiBoFragment.this.mWeiBoList.ao != -1000) {
                KShareUtil.showToastJsonStatus(HotWeiBoFragment.this.getActivity(), HotWeiBoFragment.this.mWeiBoList);
                return;
            }
            if (HotWeiBoFragment.this.mWeiBoList.i) {
                HotWeiBoFragment.this.mAdapter.addItem(HotWeiBoFragment.this.mWeiBoList.g());
            } else {
                HotWeiBoFragment.this.mAdapter.clearItem();
                HotWeiBoFragment.this.mAdapter.addItem(0, (List<LiveMessage>) HotWeiBoFragment.this.mWeiBoList.g());
            }
            if (HotWeiBoFragment.this.mAdapter.getList().size() > 0) {
                HotWeiBoFragment.this.adapterHeaderView((com.pocketmusic.kshare.requestobjs.w) HotWeiBoFragment.this.mAdapter.getList().get(0));
            } else {
                HotWeiBoFragment.this.adapterHeaderView(null);
            }
        }
    };
    /* access modifiers changed from: private */
    public x mWeiBoList;
    private x.b mWeiBoListType = x.b.TodaySelected;

    public static HotWeiBoFragment newInstance(x.b bVar) {
        HotWeiBoFragment hotWeiBoFragment = new HotWeiBoFragment();
        if (bVar != null) {
            hotWeiBoFragment.mWeiBoListType = bVar;
        }
        return hotWeiBoFragment;
    }

    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putSerializable("type", this.mWeiBoListType);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (bundle != null && bundle.containsKey("type")) {
            this.mWeiBoListType = (x.b) bundle.getSerializable("type");
        }
        this.container = (ViewGroup) getActivity().getLayoutInflater().inflate(a.g.public_pulllistview, (ViewGroup) null);
        initView(this.container);
        new Handler().postDelayed(new Runnable() {
            public void run() {
                HotWeiBoFragment.this.initData();
            }
        }, 500);
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return this.container;
    }

    public void onDestroyView() {
        super.onDestroyView();
        ViewGroup viewGroup = (ViewGroup) this.container.getParent();
        if (viewGroup != null) {
            viewGroup.removeAllViewsInLayout();
        }
    }

    /* access modifiers changed from: protected */
    public void initView(ViewGroup viewGroup) {
        this.mHotRefreshListView = (PullToRefreshListView) viewGroup.findViewById(a.f.public_items_listview);
        this.mHotRefreshListView.setOnRefreshListener(this);
        this.mHotListView = (ListView) this.mHotRefreshListView.getRefreshableView();
        this.mHotListView.setDividerHeight(0);
        this.mHotRefreshListView.setOnLastItemVisibleListener(new PullToRefreshBase.OnLastItemVisibleListener() {
            public void onLastItemVisible() {
                HotWeiBoFragment.this.onPullUpToRefresh();
            }
        });
        addHeaderView();
        this.mAdapter = new HotWeiboAdapter(getActivity());
        this.mHotListView.setAdapter((ListAdapter) this.mAdapter);
    }

    @SuppressLint({"InflateParams"})
    private void addHeaderView() {
        this.headView = LayoutInflater.from(getActivity()).inflate(a.g.item_hot_weibo, (ViewGroup) null);
        this.headView.setVisibility(8);
        this.headView.findViewById(a.f.weibo_item_02).setVisibility(8);
        ((ImageView) this.headView.findViewById(a.f.weibo_item_image_01)).setBackgroundResource(a.e.default_my);
        ((TextView) this.headView.findViewById(a.f.weibo_item_text_01)).setMaxWidth((UIUtil.getInstance().getmScreenWidth() * 3) / 4);
        View findViewById = this.headView.findViewById(a.f.weibo_item_layout_top);
        ViewGroup.LayoutParams layoutParams = findViewById.getLayoutParams();
        layoutParams.height = (UIUtil.getInstance().getmScreenWidth() * 5) / 8;
        findViewById.setLayoutParams(layoutParams);
        this.mHotListView.addHeaderView(this.headView);
    }

    /* access modifiers changed from: protected */
    public void initData() {
        if (this.mWeiBoList == null) {
            this.mWeiBoList = new x(this.mWeiBoListType, 20);
        }
        this.mWeiBoList.a(this.mRequestListener);
        if (this.mWeiBoList.g().size() > 0) {
            this.mAdapter.addItem(this.mWeiBoList.g());
        } else {
            this.mWeiBoList.d();
        }
    }

    /* access modifiers changed from: private */
    public void adapterHeaderView(com.pocketmusic.kshare.requestobjs.w wVar) {
        if (this.headView != null) {
            if (wVar != null) {
                this.headView.setVisibility(0);
                this.mAdapter.showWeibo(this.mAdapter.createHolder(this.headView, false), wVar);
                return;
            }
            this.headView.setVisibility(8);
        }
    }

    public void onPullDownToRefresh() {
        this.isPullDown = true;
        this.mWeiBoList.a();
    }

    public void onPullUpToRefresh() {
        if (this.mWeiBoList != null) {
            if (this.mWeiBoList.e()) {
                this.mWeiBoList.c();
                return;
            }
            Toaster.showShort(getActivity(), a.h.listview_no_more);
            this.mHotRefreshListView.onRefreshComplete();
        }
    }
}
