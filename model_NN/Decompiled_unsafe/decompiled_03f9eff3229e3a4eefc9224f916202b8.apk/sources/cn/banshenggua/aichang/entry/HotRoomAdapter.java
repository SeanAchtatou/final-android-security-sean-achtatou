package cn.banshenggua.aichang.entry;

import android.app.Activity;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import cn.a.a.a;
import cn.banshenggua.aichang.room.SimpleLiveRoomActivity;
import cn.banshenggua.aichang.utils.ImageLoaderUtil;
import cn.banshenggua.aichang.utils.ImageUtil;
import cn.banshenggua.aichang.utils.UIUtil;
import cn.banshenggua.aichang.utils.ULog;
import com.d.a.b.c;
import com.d.a.b.d;
import com.pocketmusic.kshare.requestobjs.o;

public class HotRoomAdapter extends ArrayListAdapter<o> implements View.OnClickListener {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$pocketmusic$kshare$requestobjs$Room$RoomOnMicMediaType = null;
    private static final int ITEMS = 2;
    private static final int ITEM_MARGE = 20;
    private static final String TAG = HotRoomAdapter.class.getName();
    private Activity activity;
    private d imgLoader;
    private int item_height = -1;
    c levelOptions = ImageUtil.getDefaultLevelOption();
    c options = ImageUtil.getDefaultPlayerImageAminOption(1);
    private boolean showback = false;

    static /* synthetic */ int[] $SWITCH_TABLE$com$pocketmusic$kshare$requestobjs$Room$RoomOnMicMediaType() {
        int[] iArr = $SWITCH_TABLE$com$pocketmusic$kshare$requestobjs$Room$RoomOnMicMediaType;
        if (iArr == null) {
            iArr = new int[o.b.values().length];
            try {
                iArr[o.b.Audio.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[o.b.Multi_Mic.ordinal()] = 3;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[o.b.Video.ordinal()] = 2;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$com$pocketmusic$kshare$requestobjs$Room$RoomOnMicMediaType = iArr;
        }
        return iArr;
    }

    public HotRoomAdapter(Activity activity2) {
        super(activity2);
        this.activity = activity2;
        this.imgLoader = d.a();
    }

    public HotRoomAdapter(Activity activity2, boolean z) {
        super(activity2);
        this.activity = activity2;
        this.imgLoader = d.a();
        this.showback = z;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder;
        if (view == null || view.getTag() == null) {
            view = this.mInflater.inflate(a.g.item_hot_room, (ViewGroup) null);
            viewHolder = createHolder(view, true);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        viewHolder.footer.setVisibility(8);
        if (i == getCount() - 1) {
            viewHolder.footer.setVisibility(0);
        }
        showRooms(viewHolder, i);
        return view;
    }

    public int getCount() {
        ULog.d(TAG, "mList.size() = " + this.mList.size());
        return getListSize() / 2;
    }

    public int getListSize() {
        if (this.mList.size() == 0) {
            return 0;
        }
        return this.mList.size() - 1;
    }

    public o getRoom(int i) {
        if (i + 1 < this.mList.size()) {
            return (o) this.mList.get(i + 1);
        }
        return null;
    }

    private void showRooms(ViewHolder viewHolder, int i) {
        View[] viewArr = {viewHolder.item1, viewHolder.item2};
        ImageView[] imageViewArr = {viewHolder.image01, viewHolder.image02};
        ImageView[] imageViewArr2 = {viewHolder.type01, viewHolder.type02};
        ImageView[] imageViewArr3 = {viewHolder.grade01, viewHolder.grade02};
        ImageView[] imageViewArr4 = {viewHolder.authIcon1, viewHolder.authIcon2};
        TextView[] textViewArr = {viewHolder.title01, viewHolder.title02};
        TextView[] textViewArr2 = {viewHolder.peopleNum1, viewHolder.peopleNum2};
        for (int i2 = 0; i2 < imageViewArr.length; i2++) {
            viewArr[i2].setVisibility(0);
            if (!this.showback || i != 0) {
                imageViewArr[i2].setImageBitmap(null);
                imageViewArr[i2].setBackgroundResource(a.e.default_my);
                textViewArr[i2].setVisibility(8);
                imageViewArr3[i2].setVisibility(8);
                imageViewArr2[i2].setVisibility(8);
            }
        }
        o[] oVarArr = new o[2];
        int listSize = getListSize();
        for (int i3 = 0; i3 < oVarArr.length; i3++) {
            if (i3 == 0 && this.showback && i == 0) {
                oVarArr[0] = null;
            } else if (this.showback) {
                if (((i * 2) + i3) - 1 < listSize) {
                    oVarArr[i3] = getRoom(((i * 2) + i3) - 1);
                } else {
                    oVarArr[i3] = null;
                }
            } else if ((i * 2) + i3 < listSize) {
                oVarArr[i3] = getRoom((i * 2) + i3);
            } else {
                oVarArr[i3] = null;
            }
        }
        for (int i4 = 0; i4 < oVarArr.length; i4++) {
            o oVar = oVarArr[i4];
            if (oVar == null) {
                viewArr[i4].setVisibility(4);
            } else {
                textViewArr[i4].setVisibility(0);
                imageViewArr3[i4].setVisibility(0);
                if (oVar.m != null) {
                    this.imgLoader.a(oVar.m.mLevelImage, imageViewArr3[i4], this.levelOptions);
                    if (!TextUtils.isEmpty(oVar.m.auth_info)) {
                        imageViewArr4[i4].setVisibility(0);
                        ImageLoaderUtil.displayImageBg(imageViewArr4[i4], oVar.m.authIcon, this.levelOptions);
                    } else {
                        imageViewArr4[i4].setVisibility(8);
                    }
                }
                this.imgLoader.a(oVar.f, imageViewArr[i4], this.options);
                imageViewArr[i4].setTag(oVar);
                imageViewArr3[i4].setTag(oVar);
                textViewArr[i4].setText(oVar.b);
                imageViewArr[i4].setOnClickListener(this);
                textViewArr2[i4].setText(String.valueOf(oVar.k) + "人");
                ULog.d(TAG, "room: ");
                showRoomType(oVar, imageViewArr2[i4]);
            }
        }
    }

    public void showRoom(ViewHolder viewHolder, o oVar) {
        if (oVar != null) {
            viewHolder.title01.setVisibility(0);
            if (oVar.m != null) {
                this.imgLoader.a(oVar.m.mLevelImage, viewHolder.grade01, this.levelOptions);
                if (!TextUtils.isEmpty(oVar.m.auth_info)) {
                    ImageLoaderUtil.displayImageBg(viewHolder.authIcon1, oVar.m.authIcon, this.levelOptions);
                    viewHolder.authIcon1.setVisibility(0);
                } else {
                    viewHolder.authIcon1.setVisibility(8);
                }
            }
            this.imgLoader.a(oVar.f, viewHolder.image01, this.options);
            viewHolder.image01.setTag(oVar);
            viewHolder.title01.setText(oVar.b);
            viewHolder.image01.setOnClickListener(this);
            viewHolder.peopleNum1.setText(String.valueOf(oVar.k) + "人");
            viewHolder.footer.setVisibility(8);
            showRoomType(oVar, viewHolder.type01);
        }
    }

    private void showRoomType(o oVar, ImageView imageView) {
        if (oVar != null && imageView != null) {
            imageView.setVisibility(0);
            switch ($SWITCH_TABLE$com$pocketmusic$kshare$requestobjs$Room$RoomOnMicMediaType()[oVar.S.ordinal()]) {
                case 1:
                    imageView.setVisibility(8);
                    return;
                case 2:
                    imageView.setImageResource(a.e.status_roomvideo);
                    return;
                case 3:
                    imageView.setImageResource(a.e.status_connect_mic);
                    return;
                default:
                    return;
            }
        }
    }

    private void initHolderHeight(View view) {
        if (this.item_height < 0) {
            this.item_height = (UIUtil.getInstance().getmScreenWidth() * 188) / 480;
        }
        View findViewById = view.findViewById(a.f.hot_item_layout_top);
        ViewGroup.LayoutParams layoutParams = findViewById.getLayoutParams();
        layoutParams.height = this.item_height;
        findViewById.setLayoutParams(layoutParams);
        ULog.d(TAG, "item_height: " + this.item_height);
    }

    public ViewHolder createHolder(View view, boolean z) {
        ViewHolder viewHolder = new ViewHolder();
        if (z) {
            initHolderHeight(view);
        }
        viewHolder.item1 = view.findViewById(a.f.hot_item_01);
        viewHolder.item2 = view.findViewById(a.f.hot_item_02);
        viewHolder.image01 = (ImageView) view.findViewById(a.f.hot_item_image_01);
        viewHolder.image02 = (ImageView) view.findViewById(a.f.hot_item_image_02);
        viewHolder.grade01 = (ImageView) view.findViewById(a.f.hot_item_grade_btn_01);
        viewHolder.grade02 = (ImageView) view.findViewById(a.f.hot_item_grade_btn_02);
        viewHolder.authIcon1 = (ImageView) view.findViewById(a.f.hot_item_auth_btn_01);
        viewHolder.authIcon2 = (ImageView) view.findViewById(a.f.hot_item_auth_btn_02);
        viewHolder.title01 = (TextView) view.findViewById(a.f.hot_item_text_01);
        viewHolder.title02 = (TextView) view.findViewById(a.f.hot_item_text_02);
        viewHolder.peopleNum1 = (TextView) view.findViewById(a.f.hot_item_num_01);
        viewHolder.peopleNum2 = (TextView) view.findViewById(a.f.hot_item_num_02);
        viewHolder.type01 = (ImageView) view.findViewById(a.f.hot_item_type_01);
        viewHolder.type02 = (ImageView) view.findViewById(a.f.hot_item_type_02);
        viewHolder.footer = view.findViewById(a.f.listview_item_footer);
        return viewHolder;
    }

    class ViewHolder {
        public ImageView authIcon1;
        public ImageView authIcon2;
        View footer;
        ImageView grade01;
        ImageView grade02;
        ImageView image01;
        ImageView image02;
        View item1;
        View item2;
        public TextView peopleNum1;
        public TextView peopleNum2;
        TextView title01;
        TextView title02;
        ImageView type01;
        ImageView type02;

        ViewHolder() {
        }
    }

    public void onClick(View view) {
        int id = view.getId();
        ULog.d(TAG, "onClick: " + id + "; hot_item_01: " + a.f.hot_item_image_01 + "; hot_item_02: " + a.f.hot_item_image_02);
        if (id == a.f.hot_item_image_01 || id == a.f.hot_item_image_02) {
            Object tag = view.getTag();
            if (tag instanceof o) {
                o oVar = (o) tag;
                SimpleLiveRoomActivity.launch(this.activity, (o) tag);
            }
        }
    }
}
