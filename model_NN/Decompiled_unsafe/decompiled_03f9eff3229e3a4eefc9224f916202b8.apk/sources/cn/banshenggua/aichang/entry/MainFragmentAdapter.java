package cn.banshenggua.aichang.entry;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import com.pocketmusic.kshare.requestobjs.x;

class MainFragmentAdapter extends FragmentPagerAdapter {
    private static final String[] CONTENT = {"直播房", "精选", "喊麦"};
    public static final String TAG = "DynamicFragment";
    private HotWeiBoFragment hmWeibosFragment;
    private HotRoomFragment roomsFragment;
    private HotWeiBoFragment weibosFragment;

    public MainFragmentAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    public Fragment getItem(int i) {
        switch (i) {
            case 0:
                if (this.roomsFragment == null) {
                    this.roomsFragment = HotRoomFragment.newInstance();
                }
                return this.roomsFragment;
            case 1:
                if (this.weibosFragment == null) {
                    this.weibosFragment = HotWeiBoFragment.newInstance(x.b.TodaySelected);
                }
                return this.weibosFragment;
            case 2:
                if (this.hmWeibosFragment == null) {
                    this.hmWeibosFragment = HotWeiBoFragment.newInstance(x.b.HMSelected);
                }
                return this.hmWeibosFragment;
            default:
                return null;
        }
    }

    public CharSequence getPageTitle(int i) {
        return CONTENT[i % CONTENT.length].toUpperCase();
    }

    public int getCount() {
        return CONTENT.length;
    }
}
