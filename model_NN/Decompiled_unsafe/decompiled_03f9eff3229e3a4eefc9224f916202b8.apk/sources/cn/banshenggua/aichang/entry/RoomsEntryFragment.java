package cn.banshenggua.aichang.entry;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import cn.a.a.a;

public class RoomsEntryFragment extends BaseFragment {
    private View container;
    private MainFragmentAdapter mAdapter;
    private ViewPager.OnPageChangeListener mOnPageChangeListener = new ViewPager.OnPageChangeListener() {
        public void onPageSelected(int i) {
            switch (i) {
                case 0:
                    RoomsEntryFragment.this.mRadioGroup.check(a.f.adbtn_1);
                    return;
                case 1:
                    RoomsEntryFragment.this.mRadioGroup.check(a.f.adbtn_2);
                    return;
                case 2:
                    RoomsEntryFragment.this.mRadioGroup.check(a.f.adbtn_3);
                    return;
                default:
                    return;
            }
        }

        public void onPageScrolled(int i, float f, int i2) {
        }

        public void onPageScrollStateChanged(int i) {
        }
    };
    /* access modifiers changed from: private */
    public ViewPager mPager;
    /* access modifiers changed from: private */
    public RadioGroup mRadioGroup = null;

    public static RoomsEntryFragment newInstance() {
        return new RoomsEntryFragment();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.mAdapter = new MainFragmentAdapter(getChildFragmentManager());
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        this.container = (ViewGroup) layoutInflater.inflate(a.g.fra_main_tab, (ViewGroup) null);
        this.mPager = (ViewPager) this.container.findViewById(a.f.pager);
        this.mPager.setOffscreenPageLimit(this.mAdapter.getCount());
        this.mPager.setAdapter(this.mAdapter);
        this.mPager.setOnPageChangeListener(this.mOnPageChangeListener);
        this.mRadioGroup = (RadioGroup) this.container.findViewById(a.f.radiogroup_ad_type);
        this.mRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i == a.f.adbtn_1) {
                    RoomsEntryFragment.this.mPager.setCurrentItem(0);
                }
                if (i == a.f.adbtn_2) {
                    RoomsEntryFragment.this.mPager.setCurrentItem(1);
                }
                if (i == a.f.adbtn_3) {
                    RoomsEntryFragment.this.mPager.setCurrentItem(2);
                }
            }
        });
        this.mRadioGroup.check(a.f.adbtn_1);
        return this.container;
    }

    public void onResume() {
        super.onResume();
    }
}
