package com.soft.android.appinstaller;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

public class FinishActivity extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.finish);
        GlobalConfig.getInstance().init(this);
        TextView textView = (TextView) findViewById(R.id.finishTextView);
        ActivityTexts texts = GlobalConfig.getInstance().getFinishTexts();
        if (texts != null) {
            textView.setText(texts.getText());
            textView.setMovementMethod(new ScrollingMovementMethod());
            setTitle(texts.getTitle());
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void onNextClicked(View v) {
        Log.v("log", "Next");
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse(GlobalConfig.getInstance().getValue("url"))));
    }

    public void onExitClicked(View v) {
        Log.v("log", "Exit");
        finish();
    }
}
