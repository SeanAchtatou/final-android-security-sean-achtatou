package com.baidu;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Collection;

class d implements FilenameFilter {
    final /* synthetic */ Collection a;
    final /* synthetic */ c b;

    d(c cVar, Collection collection) {
        this.b = cVar;
        this.a = collection;
    }

    public boolean accept(File file, String str) {
        return str.startsWith("__sdk_") && !this.a.contains(str);
    }
}
