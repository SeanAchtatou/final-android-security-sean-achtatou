package com.baidu;

import android.graphics.Bitmap;
import com.android.mmreader1361.mmabout;
import java.io.ByteArrayInputStream;
import java.io.InputStream;

class ba extends Thread {
    private Bitmap A;
    private bb B = null;
    private boolean C = false;
    private byte[] D = new byte[256];
    private int E = 0;
    private int F = 0;
    private int G = 0;
    private boolean H = false;
    private int I = 0;
    private int J;
    private short[] K;
    private byte[] L;
    private byte[] M;
    private byte[] N;
    private bb O;
    private int P;
    private az Q = null;
    private byte[] R = null;
    public int a;
    public int b;
    private InputStream c;
    private int d;
    private boolean e;
    private int f;
    private int g = 1;
    private int[] h;
    private int[] i;
    private int[] j;
    private int k;
    private int l;
    private int m;
    private int n;
    private boolean o;
    private boolean p;
    private int q;
    private int r;
    private int s;
    private int t;
    private int u;
    private int v;
    private int w;
    private int x;
    private int y;
    private Bitmap z;

    public ba(byte[] bArr, az azVar) {
        this.R = bArr;
        this.Q = azVar;
    }

    private int[] c(int i2) {
        int i3;
        int i4 = i2 * 3;
        byte[] bArr = new byte[i4];
        try {
            i3 = this.c.read(bArr);
        } catch (Exception e2) {
            e2.printStackTrace();
            i3 = 0;
        }
        if (i3 < i4) {
            this.d = 1;
            return null;
        }
        int[] iArr = new int[256];
        int i5 = 0;
        for (int i6 = 0; i6 < i2; i6++) {
            int i7 = i5 + 1;
            int i8 = i7 + 1;
            iArr[i6] = ((bArr[i5] & 255) << 16) | -16777216 | ((bArr[i7] & 255) << 8) | (bArr[i8] & 255);
            i5 = i8 + 1;
        }
        return iArr;
    }

    private void e() {
        int i2;
        int i3;
        int i4;
        int i5 = 0;
        int[] iArr = new int[(this.a * this.b)];
        if (this.G > 0) {
            if (this.G == 3) {
                int i6 = this.P - 2;
                if (i6 > 0) {
                    this.A = a(i6 - 1);
                } else {
                    this.A = null;
                }
            }
            if (this.A != null) {
                this.A.getPixels(iArr, 0, this.a, 0, 0, this.a, this.b);
                if (this.G == 2) {
                    int i7 = !this.H ? this.m : 0;
                    for (int i8 = 0; i8 < this.y; i8++) {
                        int i9 = ((this.w + i8) * this.a) + this.v;
                        int i10 = this.x + i9;
                        while (i9 < i10) {
                            iArr[i9] = i7;
                            i9++;
                        }
                    }
                }
            }
        }
        int i11 = 8;
        int i12 = 1;
        int i13 = 0;
        while (i13 < this.u) {
            if (this.p) {
                if (i5 >= this.u) {
                    i12++;
                    switch (i12) {
                        case 2:
                            i5 = 4;
                            break;
                        case 3:
                            i5 = 2;
                            i11 = 4;
                            break;
                        case 4:
                            i5 = 1;
                            i11 = 2;
                            break;
                    }
                }
                i2 = i12;
                i3 = i11;
                i4 = i5 + i11;
            } else {
                i2 = i12;
                i3 = i11;
                i4 = i5;
                i5 = i13;
            }
            int i14 = i5 + this.s;
            if (i14 < this.b) {
                int i15 = i14 * this.a;
                int i16 = this.r + i15;
                int i17 = this.t + i16;
                int i18 = this.a + i15 < i17 ? i15 + this.a : i17;
                int i19 = i16;
                int i20 = this.t * i13;
                while (i19 < i18) {
                    int i21 = i20 + 1;
                    int i22 = this.j[this.N[i20] & 255];
                    if (i22 != 0) {
                        iArr[i19] = i22;
                    }
                    i19++;
                    i20 = i21;
                }
            }
            i13++;
            i5 = i4;
            i11 = i3;
            i12 = i2;
        }
        this.z = Bitmap.createBitmap(iArr, this.a, this.b, Bitmap.Config.ARGB_4444);
    }

    private int f() {
        this.c = new ByteArrayInputStream(this.R);
        this.R = null;
        return g();
    }

    private int g() {
        j();
        if (this.c != null) {
            o();
            if (!i()) {
                m();
                if (this.P < 0) {
                    this.d = 1;
                    this.Q.a(false, -1);
                } else {
                    this.d = -1;
                    this.Q.a(true, -1);
                }
            }
            try {
                this.c.close();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        } else {
            this.d = 2;
            this.Q.a(false, -1);
        }
        return this.d;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r9v12, types: [int] */
    /* JADX WARN: Type inference failed for: r16v7, types: [int] */
    /* JADX WARN: Type inference failed for: r17v6, types: [int] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void h() {
        /*
            r24 = this;
            r2 = -1
            r0 = r24
            int r0 = r0.t
            r3 = r0
            r0 = r24
            int r0 = r0.u
            r4 = r0
            int r3 = r3 * r4
            r0 = r24
            byte[] r0 = r0.N
            r4 = r0
            if (r4 == 0) goto L_0x001b
            r0 = r24
            byte[] r0 = r0.N
            r4 = r0
            int r4 = r4.length
            if (r4 >= r3) goto L_0x0022
        L_0x001b:
            byte[] r4 = new byte[r3]
            r0 = r4
            r1 = r24
            r1.N = r0
        L_0x0022:
            r0 = r24
            short[] r0 = r0.K
            r4 = r0
            if (r4 != 0) goto L_0x0032
            r4 = 4096(0x1000, float:5.74E-42)
            short[] r4 = new short[r4]
            r0 = r4
            r1 = r24
            r1.K = r0
        L_0x0032:
            r0 = r24
            byte[] r0 = r0.L
            r4 = r0
            if (r4 != 0) goto L_0x0042
            r4 = 4096(0x1000, float:5.74E-42)
            byte[] r4 = new byte[r4]
            r0 = r4
            r1 = r24
            r1.L = r0
        L_0x0042:
            r0 = r24
            byte[] r0 = r0.M
            r4 = r0
            if (r4 != 0) goto L_0x0052
            r4 = 4097(0x1001, float:5.741E-42)
            byte[] r4 = new byte[r4]
            r0 = r4
            r1 = r24
            r1.M = r0
        L_0x0052:
            int r4 = r24.k()
            r5 = 1
            int r5 = r5 << r4
            int r6 = r5 + 1
            int r7 = r5 + 2
            int r8 = r4 + 1
            r9 = 1
            int r9 = r9 << r8
            r10 = 1
            int r9 = r9 - r10
            r10 = 0
        L_0x0063:
            if (r10 >= r5) goto L_0x0078
            r0 = r24
            short[] r0 = r0.K
            r11 = r0
            r12 = 0
            r11[r10] = r12
            r0 = r24
            byte[] r0 = r0.L
            r11 = r0
            byte r12 = (byte) r10
            r11[r10] = r12
            int r10 = r10 + 1
            goto L_0x0063
        L_0x0078:
            r10 = 0
            r11 = 0
            r12 = r11
            r13 = r10
            r14 = r10
            r15 = r2
            r16 = r8
            r17 = r9
            r18 = r7
            r11 = r10
            r7 = r10
            r8 = r10
            r9 = r10
        L_0x0088:
            if (r12 >= r3) goto L_0x0099
            if (r9 != 0) goto L_0x0184
            r0 = r14
            r1 = r16
            if (r0 >= r1) goto L_0x00c6
            if (r13 != 0) goto L_0x00ad
            int r8 = r24.l()
            if (r8 > 0) goto L_0x00a7
        L_0x0099:
            r2 = r7
        L_0x009a:
            if (r2 >= r3) goto L_0x019f
            r0 = r24
            byte[] r0 = r0.N
            r4 = r0
            r5 = 0
            r4[r2] = r5
            int r2 = r2 + 1
            goto L_0x009a
        L_0x00a7:
            r13 = 0
            r23 = r13
            r13 = r8
            r8 = r23
        L_0x00ad:
            r0 = r24
            byte[] r0 = r0.D
            r19 = r0
            byte r19 = r19[r8]
            r0 = r19
            r0 = r0 & 255(0xff, float:3.57E-43)
            r19 = r0
            int r19 = r19 << r14
            int r11 = r11 + r19
            int r14 = r14 + 8
            int r8 = r8 + 1
            int r13 = r13 + -1
            goto L_0x0088
        L_0x00c6:
            r19 = r11 & r17
            int r11 = r11 >> r16
            int r14 = r14 - r16
            r0 = r19
            r1 = r18
            if (r0 > r1) goto L_0x0099
            r0 = r19
            r1 = r6
            if (r0 == r1) goto L_0x0099
            r0 = r19
            r1 = r5
            if (r0 != r1) goto L_0x00f0
            int r15 = r4 + 1
            r16 = 1
            int r16 = r16 << r15
            r17 = 1
            int r16 = r16 - r17
            int r17 = r5 + 2
            r18 = r17
            r17 = r16
            r16 = r15
            r15 = r2
            goto L_0x0088
        L_0x00f0:
            if (r15 != r2) goto L_0x0109
            r0 = r24
            byte[] r0 = r0.M
            r10 = r0
            int r15 = r9 + 1
            r0 = r24
            byte[] r0 = r0.L
            r20 = r0
            byte r20 = r20[r19]
            r10[r9] = r20
            r9 = r15
            r10 = r19
            r15 = r19
            goto L_0x0088
        L_0x0109:
            r0 = r19
            r1 = r18
            if (r0 != r1) goto L_0x01a5
            r0 = r24
            byte[] r0 = r0.M
            r20 = r0
            int r21 = r9 + 1
            byte r10 = (byte) r10
            r20[r9] = r10
            r9 = r21
            r10 = r15
        L_0x011d:
            if (r10 <= r5) goto L_0x013c
            r0 = r24
            byte[] r0 = r0.M
            r20 = r0
            int r21 = r9 + 1
            r0 = r24
            byte[] r0 = r0.L
            r22 = r0
            byte r22 = r22[r10]
            r20[r9] = r22
            r0 = r24
            short[] r0 = r0.K
            r9 = r0
            short r9 = r9[r10]
            r10 = r9
            r9 = r21
            goto L_0x011d
        L_0x013c:
            r0 = r24
            byte[] r0 = r0.L
            r20 = r0
            byte r10 = r20[r10]
            r10 = r10 & 255(0xff, float:3.57E-43)
            r20 = 4096(0x1000, float:5.74E-42)
            r0 = r18
            r1 = r20
            if (r0 >= r1) goto L_0x0099
            r0 = r24
            byte[] r0 = r0.M
            r20 = r0
            int r21 = r9 + 1
            r0 = r10
            byte r0 = (byte) r0
            r22 = r0
            r20[r9] = r22
            r0 = r24
            short[] r0 = r0.K
            r9 = r0
            short r15 = (short) r15
            r9[r18] = r15
            r0 = r24
            byte[] r0 = r0.L
            r9 = r0
            byte r15 = (byte) r10
            r9[r18] = r15
            int r9 = r18 + 1
            r15 = r9 & r17
            if (r15 != 0) goto L_0x01a0
            r15 = 4096(0x1000, float:5.74E-42)
            if (r9 >= r15) goto L_0x01a0
            int r15 = r16 + 1
            int r16 = r17 + r9
        L_0x017a:
            r17 = r16
            r18 = r9
            r16 = r15
            r9 = r21
            r15 = r19
        L_0x0184:
            int r9 = r9 + -1
            r0 = r24
            byte[] r0 = r0.N
            r19 = r0
            int r20 = r7 + 1
            r0 = r24
            byte[] r0 = r0.M
            r21 = r0
            byte r21 = r21[r9]
            r19[r7] = r21
            int r7 = r12 + 1
            r12 = r7
            r7 = r20
            goto L_0x0088
        L_0x019f:
            return
        L_0x01a0:
            r15 = r16
            r16 = r17
            goto L_0x017a
        L_0x01a5:
            r10 = r19
            goto L_0x011d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.ba.h():void");
    }

    private boolean i() {
        return this.d != 0;
    }

    private void j() {
        this.d = 0;
        this.P = 0;
        this.O = null;
        this.h = null;
        this.i = null;
    }

    private int k() {
        try {
            return this.c.read();
        } catch (Exception e2) {
            this.d = 1;
            return 0;
        }
    }

    private int l() {
        this.E = k();
        int i2 = 0;
        if (this.E > 0) {
            while (i2 < this.E) {
                try {
                    int read = this.c.read(this.D, i2, this.E - i2);
                    if (read == -1) {
                        break;
                    }
                    i2 += read;
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
            if (i2 < this.E) {
                this.d = 1;
            }
        }
        return i2;
    }

    private void m() {
        boolean z2 = false;
        while (!z2 && !i()) {
            switch (k()) {
                case 0:
                    break;
                case mmabout.MORE_ID:
                    switch (k()) {
                        case 249:
                            n();
                            continue;
                        case 255:
                            l();
                            String str = "";
                            for (int i2 = 0; i2 < 11; i2++) {
                                str = str + ((char) this.D[i2]);
                            }
                            if (!str.equals("NETSCAPE2.0")) {
                                u();
                                break;
                            } else {
                                r();
                                continue;
                            }
                        default:
                            u();
                            continue;
                    }
                case 44:
                    p();
                    break;
                case 59:
                    z2 = true;
                    break;
                default:
                    this.d = 1;
                    break;
            }
        }
    }

    private void n() {
        k();
        int k2 = k();
        this.F = (k2 & 28) >> 2;
        if (this.F == 0) {
            this.F = 1;
        }
        this.H = (k2 & 1) != 0;
        this.I = s() * 10;
        this.J = k();
        k();
    }

    private void o() {
        String str = "";
        for (int i2 = 0; i2 < 6; i2++) {
            str = str + ((char) k());
        }
        if (!str.startsWith("GIF")) {
            this.d = 1;
            return;
        }
        q();
        if (this.e && !i()) {
            this.h = c(this.f);
            this.l = this.h[this.k];
        }
    }

    private void p() {
        int i2;
        this.r = s();
        this.s = s();
        this.t = s();
        this.u = s();
        int k2 = k();
        this.o = (k2 & 128) != 0;
        this.p = (k2 & 64) != 0;
        this.q = 2 << (k2 & 7);
        if (this.o) {
            this.i = c(this.q);
            this.j = this.i;
        } else {
            this.j = this.h;
            if (this.k == this.J) {
                this.l = 0;
            }
        }
        if (this.H) {
            i2 = this.j[this.J];
            this.j[this.J] = 0;
        } else {
            i2 = 0;
        }
        if (this.j == null) {
            this.d = 1;
        }
        if (!i()) {
            h();
            u();
            if (!i()) {
                this.P++;
                this.z = Bitmap.createBitmap(this.a, this.b, Bitmap.Config.ARGB_4444);
                e();
                if (this.O == null) {
                    this.O = new bb(this.z, this.I);
                    this.B = this.O;
                } else {
                    bb bbVar = this.O;
                    while (bbVar.c != null) {
                        bbVar = bbVar.c;
                    }
                    bbVar.c = new bb(this.z, this.I);
                }
                if (this.H) {
                    this.j[this.J] = i2;
                }
                t();
                this.Q.a(true, this.P);
            }
        }
    }

    private void q() {
        this.a = s();
        this.b = s();
        int k2 = k();
        this.e = (k2 & 128) != 0;
        this.f = 2 << (k2 & 7);
        this.k = k();
        this.n = k();
        bk.a(this.n + "");
    }

    private void r() {
        do {
            l();
            if (this.D[0] == 1) {
                this.g = (this.D[1] & 255) | ((this.D[2] & 255) << 8);
            }
            if (this.E <= 0) {
                return;
            }
        } while (!i());
    }

    private int s() {
        return k() | (k() << 8);
    }

    private void t() {
        this.G = this.F;
        this.v = this.r;
        this.w = this.s;
        this.x = this.t;
        this.y = this.u;
        this.A = this.z;
        this.m = this.l;
        this.F = 0;
        this.H = false;
        this.I = 0;
        this.i = null;
    }

    private void u() {
        do {
            l();
            if (this.E <= 0) {
                return;
            }
        } while (!i());
    }

    public Bitmap a(int i2) {
        bb b2 = b(i2);
        if (b2 == null) {
            return null;
        }
        return b2.a;
    }

    public void a() {
        bb bbVar = this.O;
        while (bbVar != null) {
            bbVar.a = null;
            this.O = this.O.c;
            bbVar = this.O;
        }
        if (this.c != null) {
            try {
                this.c.close();
            } catch (Exception e2) {
            }
            this.c = null;
        }
        this.R = null;
    }

    public int b() {
        return this.P;
    }

    public bb b(int i2) {
        bb bbVar = this.O;
        int i3 = 0;
        while (bbVar != null) {
            if (i3 == i2) {
                return bbVar;
            }
            bbVar = bbVar.c;
            i3++;
        }
        return null;
    }

    public Bitmap c() {
        return a(0);
    }

    public bb d() {
        if (!this.C) {
            this.C = true;
            return this.O;
        }
        if (this.d != 0) {
            this.B = this.B.c;
            if (this.B == null) {
                this.B = this.O;
            }
        } else if (this.B.c != null) {
            this.B = this.B.c;
        }
        return this.B;
    }

    public void run() {
        if (this.c != null) {
            g();
        } else if (this.R != null) {
            f();
        }
    }
}
