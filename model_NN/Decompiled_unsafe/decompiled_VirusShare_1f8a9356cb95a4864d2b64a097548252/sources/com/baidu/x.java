package com.baidu;

class x implements s {
    final /* synthetic */ AdView a;

    x(AdView adView) {
        this.a = adView;
    }

    public void a() {
        bk.b("adRequestStart");
    }

    public void a(Exception exc, FailReason failReason) {
        bk.b("adRequestException");
        this.a.c().onReceiveFail(failReason);
    }

    public void a(boolean z) {
        bk.b("adRequestSuccess refreshNow", z + "");
        if (z) {
            this.a.f();
        }
        this.a.c().onReceiveSuccess();
    }
}
