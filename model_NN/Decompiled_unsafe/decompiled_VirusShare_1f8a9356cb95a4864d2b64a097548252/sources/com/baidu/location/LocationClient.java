package com.baidu.location;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import com.tencent.lbsapi.core.e;
import java.util.ArrayList;
import java.util.Iterator;
import org.apache.http.HttpResponse;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

public class LocationClient {
    private d a = null;
    private ArrayList b = null;

    /* renamed from: byte  reason: not valid java name */
    private boolean f0byte = false;
    private b c = null;
    /* access modifiers changed from: private */

    /* renamed from: case  reason: not valid java name */
    public String f1case = null;

    /* renamed from: char  reason: not valid java name */
    private boolean f2char = true;
    private c d = null;
    /* access modifiers changed from: private */

    /* renamed from: do  reason: not valid java name */
    public a f3do = null;
    /* access modifiers changed from: private */
    public ArrayList e = new ArrayList();

    /* renamed from: else  reason: not valid java name */
    private final int f4else = 2;
    private LocServiceMode f = LocServiceMode.Background;
    /* access modifiers changed from: private */

    /* renamed from: for  reason: not valid java name */
    public e f5for = null;
    private Handler g = null;
    /* access modifiers changed from: private */

    /* renamed from: goto  reason: not valid java name */
    public String f6goto = null;
    /* access modifiers changed from: private */
    public String h = null;
    private long i;
    /* access modifiers changed from: private */

    /* renamed from: if  reason: not valid java name */
    public LocServiceMode f7if = LocServiceMode.Background;

    /* renamed from: int  reason: not valid java name */
    private final long f8int = 2000;

    /* renamed from: long  reason: not valid java name */
    private Context f9long;
    /* access modifiers changed from: private */

    /* renamed from: new  reason: not valid java name */
    public ArrayList f10new = null;
    /* access modifiers changed from: private */

    /* renamed from: try  reason: not valid java name */
    public boolean f11try = false;

    /* renamed from: void  reason: not valid java name */
    private final int f12void = 1;

    private class a extends Handler {
        private a() {
        }

        /* synthetic */ a(LocationClient locationClient, a aVar) {
            this();
        }

        public void handleMessage(Message message) {
            super.handleMessage(message);
            switch (message.arg1) {
                case 1:
                    LocationClient.this.m7if(LocationClient.this.f1case);
                    return;
                case 2:
                    LocationClient.this.m7if("InternetException");
                    return;
                default:
                    return;
            }
        }
    }

    private class b implements a {
        private b() {
        }

        /* synthetic */ b(LocationClient locationClient, b bVar) {
            this();
        }

        public void a(String str) {
            if (LocationClient.this.f5for.m34case() && LocationClient.this.e != null && LocationClient.this.e.size() < 30) {
                LocationClient.this.e.add(str);
            }
            if (LocationClient.this.f7if == LocServiceMode.Background && LocationClient.this.f10new != null && LocationClient.this.f10new.size() > 0) {
                Iterator it = LocationClient.this.f10new.iterator();
                while (it.hasNext()) {
                    ((LocationChangedListener) it.next()).onLocationChanged();
                }
            }
        }
    }

    private class c implements Runnable {
        private c() {
        }

        /* synthetic */ c(LocationClient locationClient, c cVar) {
            this();
        }

        public void run() {
            String str = LocationClient.this.f5for.m44try();
            if (str == null || str.equals(LocationClient.this.h)) {
                LocationClient.this.m7if((String) null);
            } else {
                LocationClient.this.a(str);
            }
        }
    }

    private class d implements ReceiveListener {
        private d() {
        }

        /* synthetic */ d(LocationClient locationClient, d dVar) {
            this();
        }

        public void onReceive(String str) {
            if (str == null || str.equals(LocationClient.this.h)) {
                LocationClient.this.m7if((String) null);
            } else {
                LocationClient.this.a(str);
            }
        }
    }

    public LocationClient(Context context) {
        this.f9long = context;
        this.f5for = new e(this.f9long);
    }

    /* access modifiers changed from: private */
    public void a(final String str) {
        if (this.f0byte) {
            final String str2 = null;
            if (this.e.size() >= 1) {
                str2 = (String) this.e.get(0);
                this.e.remove(0);
            }
            new Thread() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.baidu.location.LocationClient.a(com.baidu.location.LocationClient, boolean):void
                 arg types: [com.baidu.location.LocationClient, int]
                 candidates:
                  com.baidu.location.LocationClient.a(com.baidu.location.LocationClient, java.lang.String):void
                  com.baidu.location.LocationClient.a(com.baidu.location.LocationClient, boolean):void */
                public void run() {
                    if (!LocationClient.this.f11try) {
                        LocationClient.this.f11try = true;
                        HttpPost httpPost = new HttpPost("http://loc.map.baidu.com/sdk.php");
                        ArrayList arrayList = new ArrayList();
                        arrayList.add(new BasicNameValuePair("bloc", str));
                        if (str2 != null) {
                            arrayList.add(new BasicNameValuePair("up", str2));
                        }
                        try {
                            httpPost.setEntity(new UrlEncodedFormEntity(arrayList, e.e));
                            DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
                            defaultHttpClient.getParams().setParameter("http.connection.timeout", 15000);
                            defaultHttpClient.getParams().setParameter("http.socket.timeout", 15000);
                            HttpResponse execute = defaultHttpClient.execute(httpPost);
                            if (execute.getStatusLine().getStatusCode() == 200) {
                                LocationClient.this.h = str;
                                LocationClient.this.f1case = EntityUtils.toString(execute.getEntity(), e.e);
                                LocationClient.this.f6goto = LocationClient.this.f1case;
                                Message obtain = Message.obtain();
                                obtain.arg1 = 1;
                                LocationClient.this.f3do.sendMessage(obtain);
                            } else {
                                LocationClient.this.f1case = null;
                            }
                        } catch (Exception e) {
                            Message obtain2 = Message.obtain();
                            obtain2.arg1 = 2;
                            LocationClient.this.f3do.sendMessage(obtain2);
                        } finally {
                            LocationClient.this.f11try = false;
                        }
                    }
                }
            }.start();
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: if  reason: not valid java name */
    public void m7if(String str) {
        String str2 = str == null ? this.f6goto : str;
        if (this.b != null && this.b.size() > 0) {
            Iterator it = this.b.iterator();
            while (it.hasNext()) {
                ((ReceiveListener) it.next()).onReceive(str2);
            }
        }
    }

    public void addLocationChangedlistener(LocationChangedListener locationChangedListener) {
        if (this.f7if != LocServiceMode.Immediat) {
            if (this.f10new == null) {
                this.f10new = new ArrayList();
            }
            if (!this.f10new.contains(locationChangedListener)) {
                this.f10new.add(locationChangedListener);
            }
            if (this.c == null) {
                this.c = new b(this, null);
                this.f5for.a(this.c);
            }
        }
    }

    public void addRecerveListener(ReceiveListener receiveListener) {
        if (this.b == null) {
            this.b = new ArrayList();
        }
        if (!this.b.contains(receiveListener)) {
            this.b.add(receiveListener);
        }
        if (this.a == null) {
            this.a = new d(this, null);
            this.f5for.a(this.a);
        }
    }

    public void closeGPS() {
        if (!this.f0byte) {
            this.f2char = false;
            this.f5for.a();
        }
    }

    public int getLocation() {
        if (!this.f0byte) {
            return 1;
        }
        if (System.currentTimeMillis() - this.i <= 2000) {
            return 6;
        }
        this.i = System.currentTimeMillis();
        if (this.b == null || this.b.size() < 1) {
            return 2;
        }
        if (this.f == LocServiceMode.Immediat) {
            if (this.a != null) {
                return this.f5for.m33byte() ? 0 : 3;
            }
            Log.e("locSDK", "immediat scan but no callbacks ...");
            return 5;
        } else if (this.f != LocServiceMode.Background) {
            return 4;
        } else {
            if (this.g == null) {
                return 7;
            }
            if (this.d == null) {
                this.d = new c(this, null);
            }
            this.g.postDelayed(this.d, 10);
            return 0;
        }
    }

    public void openGPS() {
        if (!this.f0byte) {
            this.f2char = true;
            this.f5for.b();
        }
    }

    public void removeLocationChangedLiteners() {
        if (this.c != null) {
            this.f5for.m45void();
            this.c = null;
        }
        if (this.f10new != null) {
            this.f10new.clear();
            this.f10new = null;
        }
    }

    public void removeReceiveListeners() {
        if (this.a != null) {
            this.f5for.m39goto();
            this.a = null;
        }
        if (this.b != null) {
            this.b.clear();
            this.b = null;
        }
    }

    public void setAddrType(String str) {
        if (!this.f0byte) {
            this.f5for.a(str);
        }
    }

    public void setCoorType(String str) {
        if (!this.f0byte) {
            this.f5for.m41if(str);
        }
    }

    public void setProdName(String str) {
        if (!this.f0byte) {
            this.f5for.m36do(str);
        }
    }

    public void setServiceMode(LocServiceMode locServiceMode) {
        if (!this.f0byte) {
            this.f7if = locServiceMode;
        }
    }

    public void setTimeSpan(int i2) {
        if (!this.f0byte) {
            this.f5for.a(i2);
        }
    }

    public void start() {
        if (!this.f0byte) {
            this.f0byte = true;
            if (this.f7if != LocServiceMode.Immediat || this.f2char) {
                this.f = LocServiceMode.Background;
            } else {
                this.f = LocServiceMode.Immediat;
            }
            this.f5for.a(this.f);
            if (this.f == LocServiceMode.Background) {
                if (this.c == null) {
                    this.c = new b(this, null);
                    this.f5for.a(this.c);
                }
                this.g = new Handler();
            }
            this.f3do = new a(this, null);
            this.f5for.f();
        }
    }

    public void stop() {
        if (this.f0byte) {
            this.f0byte = false;
            this.f5for.c();
            this.f1case = null;
            this.f6goto = null;
            this.h = null;
            if (this.f10new != null) {
                this.f10new.clear();
                this.f10new = null;
            }
            this.c = null;
            if (this.b != null) {
                this.b.clear();
                this.b = null;
            }
            this.a = null;
            this.g = null;
            this.d = null;
        }
    }
}
