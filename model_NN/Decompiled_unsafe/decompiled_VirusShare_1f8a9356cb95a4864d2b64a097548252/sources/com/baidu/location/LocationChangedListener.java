package com.baidu.location;

public interface LocationChangedListener {
    void onLocationChanged();
}
