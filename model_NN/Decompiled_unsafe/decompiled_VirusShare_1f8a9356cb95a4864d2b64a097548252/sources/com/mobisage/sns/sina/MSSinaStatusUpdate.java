package com.mobisage.sns.sina;

public class MSSinaStatusUpdate extends MSSinaWeiboMessage {
    public MSSinaStatusUpdate(String appKey, String accessToken) {
        super(appKey, accessToken);
        this.urlPath = "https://api.weibo.com/2/statuses/update.json";
        this.httpMethod = "POST";
    }
}
