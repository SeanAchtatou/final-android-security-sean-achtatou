package com.mobisage.android;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import com.mobisage.android.MobiSageEnviroment;
import java.util.UUID;

public final class MobiSageManager {
    private static MobiSageManager a = new MobiSageManager();
    private boolean b = false;
    private Handler c;
    private HandlerThread d = new HandlerThread(UUID.randomUUID().toString(), 10);
    private a e;

    static /* synthetic */ void a(MobiSageManager mobiSageManager) {
        H.a();
        R.a();
        C0005e.a();
        Y.a();
        C0023w.a();
        D.a();
        C0026z zVar = new C0026z();
        zVar.c = 5;
        zVar.e = 86400;
        R.a().a(zVar);
        C0020t tVar = new C0020t();
        tVar.c = 5;
        tVar.e = 36000000;
        tVar.d = true;
        R.a().a(tVar);
    }

    public static MobiSageManager getInstance() {
        return a;
    }

    private MobiSageManager() {
        this.d.start();
        this.e = new a(this, (byte) 0);
        this.c = new Handler(this.d.getLooper(), this.e);
    }

    /* access modifiers changed from: protected */
    public final void finalize() throws Throwable {
        super.finalize();
        this.d.quit();
    }

    class a implements Handler.Callback {
        private a() {
        }

        /* synthetic */ a(MobiSageManager mobiSageManager, byte b) {
            this();
        }

        public final boolean handleMessage(Message message) {
            switch (message.what) {
                case 0:
                    MobiSageManager.a(MobiSageManager.this);
                    return true;
                default:
                    return false;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(Context context) {
        if (!this.b) {
            C0018r.a(context);
            C0025y.a(context);
            this.c.obtainMessage(0).sendToTarget();
            this.b = true;
        }
    }

    public final void trackSystemEvent(Activity activity, int eventID) {
        a(activity);
        if (eventID == 1 || eventID == 2) {
            C0002b bVar = new C0002b();
            bVar.c.putString("Network", C0025y.a(C0025y.c(activity)));
            bVar.c.putString("Carrier", C0025y.d(activity));
            bVar.c.putString("SystemEvent", String.valueOf(eventID));
            if (eventID == 1) {
                bVar.c.putString("EventID", MobiSageEnviroment.SystemEvent.App_Launching);
            } else {
                bVar.c.putString("EventID", MobiSageEnviroment.SystemEvent.App_Terminating);
            }
            bVar.c.putString("EventObject", C0018r.c);
            bVar.c.putString("AppVersion", C0018r.d);
            Y.a().a(u.k, bVar);
        }
    }

    public final void trackCustomEvent(Activity activity, String name, String data) {
        a(activity);
        C0002b bVar = new C0002b();
        bVar.c.putString("Network", C0025y.a(C0025y.c(activity)));
        bVar.c.putString("Carrier", C0025y.d(activity));
        bVar.c.putString("SystemEvent", String.valueOf(0));
        bVar.c.putString("EventID", name);
        bVar.c.putString("EventObject", data);
        bVar.c.putString("AppVersion", C0018r.d);
        Y.a().a(u.k, bVar);
    }

    public final void trackStreamEvent(Activity activity, String streamEvent) {
        a(activity);
        C0002b bVar = new C0002b();
        bVar.c.putString("TrackData", streamEvent);
        Y.a().a(u.m, bVar);
    }

    public final void pushMobiSageMessage(MobiSageMessage msg) {
        H.a().a(msg);
    }

    public final void cancelMobiSageMessage(MobiSageMessage msg) {
        H.a().b(msg);
    }
}
