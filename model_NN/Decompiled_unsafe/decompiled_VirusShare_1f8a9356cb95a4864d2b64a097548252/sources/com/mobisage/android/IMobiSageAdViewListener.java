package com.mobisage.android;

public interface IMobiSageAdViewListener {
    void onMobiSageAdViewHide(Object obj);

    void onMobiSageAdViewShow(Object obj);

    void onMobiSageAdViewUpdate(Object obj);
}
