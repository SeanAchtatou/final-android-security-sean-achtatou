package com.mobisage.android;

import MobWin.cnst.PROTOCOL_ENCODING;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.util.EntityUtils;

final class J extends N {
    public J(MobiSageMessage mobiSageMessage) {
        super(mobiSageMessage);
    }

    /* JADX INFO: finally extract failed */
    public final void run() {
        try {
            SchemeRegistry schemeRegistry = new SchemeRegistry();
            schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            schemeRegistry.register(new Scheme("https", new SNSSSLSocketFactory(), 443));
            BasicHttpParams basicHttpParams = new BasicHttpParams();
            basicHttpParams.setParameter("http.connection.timeout", 5000);
            basicHttpParams.setParameter("http.socket.timeout", 5000);
            HttpProtocolParams.setVersion(basicHttpParams, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(basicHttpParams, PROTOCOL_ENCODING.value);
            HttpResponse execute = new DefaultHttpClient(new SingleClientConnManager(basicHttpParams, schemeRegistry), basicHttpParams).execute(this.a.createHttpRequest());
            Header[] headers = execute.getHeaders("Content-Length");
            if (headers.length == 0) {
                byte[] byteArray = EntityUtils.toByteArray(execute.getEntity());
                this.a.result.putInt("StatusCode", execute.getStatusLine().getStatusCode());
                this.a.result.putByteArray("ResponseBody", byteArray);
            } else {
                int parseInt = Integer.parseInt(headers[0].getValue());
                byte[] byteArray2 = EntityUtils.toByteArray(execute.getEntity());
                if (byteArray2.length != parseInt) {
                    this.a.result.putInt("StatusCode", 400);
                    this.a.result.putString("ErrorText", "Bad Response Size");
                } else {
                    this.a.result.putInt("StatusCode", execute.getStatusLine().getStatusCode());
                    this.a.result.putByteArray("ResponseBody", byteArray2);
                }
            }
            if (this.a.callback != null) {
                this.a.callback.onMobiSageMessageFinish(this.a);
            }
        } catch (Exception e) {
            this.a.result.remove("StatusCode");
            this.a.result.putString("ErrorText", e.getLocalizedMessage());
            if (this.a.callback != null) {
                this.a.callback.onMobiSageMessageFinish(this.a);
            }
        } catch (Throwable th) {
            if (this.a.callback != null) {
                this.a.callback.onMobiSageMessageFinish(this.a);
            }
            throw th;
        }
        super.run();
    }
}
