package com.mobisage.android;

import android.os.Build;
import android.os.Handler;
import android.text.format.Time;
import com.mobisage.android.SNSSSLSocketFactory;
import java.io.File;
import java.net.URLEncoder;
import java.util.Locale;
import java.util.UUID;

final class ac extends aa {
    ac(Handler handler) {
        super(handler);
        this.c = u.k;
    }

    /* access modifiers changed from: protected */
    public final void f(C0002b bVar) {
        StringBuilder sb = new StringBuilder();
        sb.append("http://trc.adsage.com/trc/sdk/x.gif?");
        sb.append("ver=M3_02");
        sb.append("&md=" + URLEncoder.encode(Build.MODEL));
        sb.append("&pid=" + C0018r.a);
        sb.append("&muid=" + C0025y.a);
        sb.append("&ns=" + bVar.c.getString("Network"));
        sb.append("&nsr=" + bVar.c.getString("Carrier"));
        sb.append("&av=2.3.0");
        sb.append("&eid=" + bVar.c.getString("EventID"));
        sb.append("&eb=" + URLEncoder.encode(bVar.c.getString("EventObject")));
        sb.append("&se=" + bVar.c.getString("SystemEvent"));
        Time time = new Time();
        time.setToNow();
        if (C0018r.g == 0) {
            sb.append("&st=0");
            C0018r.g = time.toMillis(true);
        } else {
            sb.append("&st=" + String.valueOf((time.toMillis(true) - C0018r.g) / 1000));
            C0018r.g = 0;
        }
        sb.append("&et=" + time.format("%Y-%m-%d"));
        sb.append("&scr=" + C0025y.d);
        sb.append("&ol=1");
        sb.append("&sv=" + Build.VERSION.RELEASE);
        sb.append("&sl=" + Locale.getDefault().getLanguage());
        sb.append("&cv=" + bVar.c.getString("AppVersion"));
        sb.append("&ib=1");
        sb.append("&tm=" + time.format("%H:%M:%S"));
        sb.append("&tz=" + Time.getCurrentTimezone());
        sb.append("&sid=" + String.valueOf(C0018r.g / 1000));
        sb.append("&ich=" + URLEncoder.encode(C0018r.b));
        sb.append("&mac=" + C0025y.b(C0018r.h));
        sb.append("&ct=" + String.valueOf(1));
        sb.append("&loc=" + URLEncoder.encode(D.a().b()));
        sb.append("&oid=" + String.valueOf(0));
        String str = C0018r.f + "/Track" + "/" + String.valueOf(time.toMillis(true) / 1000) + "_" + UUID.randomUUID().toString() + ".dat";
        File file = new File(str);
        file.mkdirs();
        SNSSSLSocketFactory.a.a(file, sb.toString());
        this.e.put(bVar.b, bVar);
        X x = new X();
        x.a = str;
        x.callback = this.g;
        bVar.f.add(x);
        this.f.put(x.c, bVar.b);
        H.a().a(x);
    }
}
