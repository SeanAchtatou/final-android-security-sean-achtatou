package com.mobisage.android;

import android.os.Handler;
import android.text.format.Time;
import com.mobisage.android.SNSSSLSocketFactory;
import java.io.File;
import java.net.URLEncoder;
import java.util.UUID;

final class S extends aa {
    S(Handler handler) {
        super(handler);
        this.c = u.o;
    }

    /* access modifiers changed from: protected */
    public final void f(C0002b bVar) {
        StringBuilder sb = new StringBuilder();
        sb.append("http://trc.adsage.com/trc/sdk/x.gif?");
        sb.append("ver=M2_03");
        sb.append("&event=" + bVar.c.getString("EventType"));
        sb.append("&token=" + bVar.c.getString("Token"));
        sb.append("&adgroupid=" + bVar.c.getString("AdGroupID"));
        sb.append("&adid=" + bVar.c.getString("AdID"));
        sb.append("&pid=" + bVar.c.getString("PublisherID"));
        Time time = new Time();
        time.switchTimezone("GMT +0000");
        time.setToNow();
        sb.append("&time=" + time.format("%Y%m%d%H%M%S"));
        sb.append("&uid=" + C0025y.a);
        if (bVar.c.containsKey("Tag")) {
            sb.append("&tag=" + URLEncoder.encode(bVar.c.getString("Tag")));
        } else {
            sb.append("&tag=");
        }
        sb.append("&sv=2.3.0");
        if (bVar.c.containsKey("CustomData")) {
            sb.append("&cdata=" + URLEncoder.encode(bVar.c.getString("CustomData")));
        } else {
            sb.append("&cdata=");
        }
        sb.append("&udid=" + C0025y.g);
        sb.append("&loc=" + URLEncoder.encode(D.a().b()));
        sb.append("&oid=" + String.valueOf(0));
        sb.append("&ich=" + URLEncoder.encode(C0018r.b));
        String str = C0018r.f + "/Track" + "/" + String.valueOf(time.toMillis(true) / 1000) + "_" + UUID.randomUUID().toString() + ".dat";
        File file = new File(str);
        file.mkdirs();
        SNSSSLSocketFactory.a.a(file, sb.toString());
        this.e.put(bVar.b, bVar);
        X x = new X();
        x.a = str;
        x.callback = this.g;
        bVar.f.add(x);
        this.f.put(x.c, bVar.b);
        H.a().a(x);
    }
}
