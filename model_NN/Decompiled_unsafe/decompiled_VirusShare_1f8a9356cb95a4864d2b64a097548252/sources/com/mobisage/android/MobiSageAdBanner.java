package com.mobisage.android;

import android.content.Context;
import android.util.AttributeSet;
import android.view.animation.Animation;

public final class MobiSageAdBanner extends C0007g {
    private int a = 1;
    private int b = 1;

    public final /* bridge */ /* synthetic */ Integer getAdRefreshInterval() {
        return super.getAdRefreshInterval();
    }

    public final /* bridge */ /* synthetic */ String getCustomData() {
        return super.getCustomData();
    }

    public final /* bridge */ /* synthetic */ String getKeyword() {
        return super.getKeyword();
    }

    public final /* bridge */ /* synthetic */ void setAdRefreshInterval(Integer x0) {
        super.setAdRefreshInterval(x0);
    }

    public final /* bridge */ /* synthetic */ void setCustomData(String x0) {
        super.setCustomData(x0);
    }

    public final /* bridge */ /* synthetic */ void setKeyword(String x0) {
        super.setKeyword(x0);
    }

    public final /* bridge */ /* synthetic */ void setMobiSageAdViewListener(IMobiSageAdViewListener x0) {
        super.setMobiSageAdViewListener(x0);
    }

    public MobiSageAdBanner(Context context, String publisherID, String keyword, String customData) {
        super(context, publisherID, keyword, customData);
    }

    public MobiSageAdBanner(Context context, int adSize, String publisherID, String keyword, String customData) {
        super(context, adSize, publisherID, keyword, customData);
    }

    public MobiSageAdBanner(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public final int getAnimeType() {
        return this.a;
    }

    public final void setAnimeType(int animeType) {
        this.a = animeType;
    }

    /* access modifiers changed from: protected */
    public final void onLoadAdFinish() {
        P a2;
        int intValue = ((Integer) C0023w.a().a("adanimation")).intValue();
        if (intValue == 0) {
            intValue = this.a;
        }
        if (intValue == 1) {
            intValue = ((int) (Math.random() * 5.0d)) + 65;
        }
        if (this.b != intValue) {
            this.b = intValue;
            switch (intValue) {
                case 65:
                    a2 = new M();
                    break;
                case MobiSageAnimeType.Anime_TopToBottom /*66*/:
                    a2 = new ae();
                    break;
                case MobiSageAnimeType.Anime_LeftToRight /*67*/:
                    a2 = new ad();
                    break;
                case MobiSageAnimeType.Anime_Fade /*68*/:
                    a2 = new B();
                    break;
                case MobiSageAnimeType.Anime_Eyes /*69*/:
                    a2 = new A();
                    break;
                default:
                    a2 = new B();
                    break;
            }
            this.viewSwitcher.setInAnimation(a2.a(this.adSize));
            Animation b2 = a2.b(this.adSize);
            b2.setAnimationListener(new a(this, (byte) 0));
            this.viewSwitcher.setOutAnimation(b2);
        }
        super.onLoadAdFinish();
    }

    /* access modifiers changed from: protected */
    public final void switchAdView() {
        this.adViewState = 0;
    }

    class a implements Animation.AnimationListener {
        private a() {
        }

        /* synthetic */ a(MobiSageAdBanner mobiSageAdBanner, byte b) {
            this();
        }

        public final void onAnimationStart(Animation animation) {
        }

        public final void onAnimationEnd(Animation animation) {
            C0014n nVar = MobiSageAdBanner.this.frontWebView;
            MobiSageAdBanner.this.frontWebView = MobiSageAdBanner.this.backWebView;
            MobiSageAdBanner.this.backWebView = nVar;
            MobiSageAdBanner.this.backWebView.clearCache(true);
            MobiSageAdBanner.this.backWebView.destroyDrawingCache();
            MobiSageAdBanner.this.backWebView.clearView();
        }

        public final void onAnimationRepeat(Animation animation) {
        }
    }
}
