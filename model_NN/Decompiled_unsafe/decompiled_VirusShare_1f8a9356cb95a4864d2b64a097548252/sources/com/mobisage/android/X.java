package com.mobisage.android;

import com.mobisage.android.SNSSSLSocketFactory;
import java.io.File;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;

final class X extends MobiSageMessage {
    public String a;

    public X() {
        this.b = 3;
    }

    public final HttpRequestBase createHttpRequest() {
        HttpGet httpGet = new HttpGet(SNSSSLSocketFactory.a.a(new File(this.a)));
        httpGet.setHeader("Connection", "close");
        return httpGet;
    }

    public final Runnable createMessageRunnable() {
        return new Z(this);
    }
}
