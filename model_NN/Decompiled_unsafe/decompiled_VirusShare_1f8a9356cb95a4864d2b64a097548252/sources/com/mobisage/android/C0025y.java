package com.mobisage.android;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import java.util.UUID;

/* renamed from: com.mobisage.android.y  reason: case insensitive filesystem */
final class C0025y {
    public static String a = "";
    public static int b;
    public static int c;
    public static String d = "";
    public static String e = "";
    public static String f = "";
    public static String g = "";
    private static boolean h = false;
    private static String i = null;

    public static void a(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        if (telephonyManager != null) {
            f += telephonyManager.getDeviceId();
            g += telephonyManager.getSimSerialNumber();
        }
        e += Settings.Secure.getString(context.getContentResolver(), "android_id");
        a = new UUID((long) e.hashCode(), (((long) f.hashCode()) << 32) | ((long) g.hashCode())).toString();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
        float f2 = displayMetrics.density;
        c = displayMetrics.densityDpi;
        int i2 = displayMetrics.widthPixels;
        int i3 = displayMetrics.heightPixels;
        if (i2 > i3) {
            d = i2 + "x" + i3;
            b = i3;
        } else {
            d = i3 + "x" + i2;
            b = i2;
        }
        h = false;
    }

    static final String b(Context context) {
        return ((WifiManager) context.getSystemService("wifi")).getConnectionInfo().getMacAddress();
    }

    public static final int c(Context context) {
        NetworkInfo[] allNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getAllNetworkInfo();
        for (int i2 = 0; i2 < allNetworkInfo.length; i2++) {
            if ((allNetworkInfo[i2].getType() == 1 && allNetworkInfo[i2].getState() == NetworkInfo.State.CONNECTING) || allNetworkInfo[i2].getState() == NetworkInfo.State.CONNECTED) {
                return 1;
            }
            if ((allNetworkInfo[i2].getType() == 0 && allNetworkInfo[i2].getState() == NetworkInfo.State.CONNECTING) || allNetworkInfo[i2].getState() == NetworkInfo.State.CONNECTED) {
                return 2;
            }
        }
        return 0;
    }

    public static String a(int i2) {
        switch (i2) {
            case 0:
                return "NS_NA";
            case 1:
                return "WIFI";
            case 2:
                return "WWAN";
            default:
                return "NS_NA";
        }
    }

    public static final String d(Context context) {
        String str;
        if (i != null) {
            return i;
        }
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        if (telephonyManager != null) {
            if (telephonyManager.getPhoneType() == 2 && telephonyManager.getSimState() == 5) {
                i = telephonyManager.getNetworkOperator();
            } else if (telephonyManager.getSimState() == 5) {
                i = telephonyManager.getSimOperator();
            }
            if (i == null) {
                str = "n/a";
            } else {
                int intValue = Integer.valueOf(i.substring(0, 3)).intValue();
                int intValue2 = Integer.valueOf(i.substring(3)).intValue();
                if (intValue == 460) {
                    if (intValue2 == 0 || intValue2 == 2 || intValue2 == 7) {
                        i = "中国移动";
                    } else if (intValue2 == 1 || intValue2 == 6) {
                        i = "中国联通";
                    } else if (intValue2 == 3 || intValue2 == 5) {
                        i = "中国电信";
                    } else if (intValue2 == 4) {
                        i = "中国卫星全球网络";
                    }
                }
                str = i;
            }
            i = str;
        } else {
            i = "n/a";
        }
        return i;
    }
}
