package com.mobisage.android;

/* renamed from: com.mobisage.android.e  reason: case insensitive filesystem */
final class C0005e extends G {
    private static final C0005e c = new C0005e();

    public static C0005e a() {
        return c;
    }

    private C0005e() {
        C0009i iVar = new C0009i(this.a);
        this.b.put(Integer.valueOf(iVar.c), iVar);
        C0010j jVar = new C0010j(this.a);
        this.b.put(Integer.valueOf(jVar.c), jVar);
        C0003c cVar = new C0003c(this.a);
        this.b.put(Integer.valueOf(cVar.c), cVar);
        C0004d dVar = new C0004d(this.a);
        this.b.put(Integer.valueOf(dVar.c), dVar);
    }
}
