package com.mobisage.android;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import java.io.IOException;

public final class MobiSageBaiduBar extends LinearLayout {
    private Context a;
    /* access modifiers changed from: private */
    public EditText b;
    private ImageButton c;
    /* access modifiers changed from: private */
    public ImageButton d;
    private c e;
    private b f;
    private a g;

    public MobiSageBaiduBar(Context context) {
        super(context);
        setOrientation(0);
        this.a = context;
        setBackgroundColor(-1);
        this.c = new ImageButton(context);
        try {
            this.c.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            this.c.setBackgroundDrawable(Drawable.createFromStream(context.getAssets().open("mobisage/baidu.png"), null));
        } catch (IOException e2) {
            this.c.setBackgroundDrawable(null);
            this.c.setImageResource(17301600);
        }
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
        layoutParams.setMargins(20, 0, 0, 0);
        layoutParams.gravity = 16;
        this.c.setLayoutParams(layoutParams);
        addView(this.c);
        this.b = new EditText(context);
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-1, -2, 1.0f);
        this.b.setBackgroundDrawable(null);
        this.b.setLayoutParams(layoutParams2);
        this.b.setSingleLine(true);
        this.b.setHint("百度一下");
        addView(this.b);
        this.e = new c(this, (byte) 0);
        this.b.addTextChangedListener(this.e);
        this.f = new b(this, (byte) 0);
        this.b.setOnKeyListener(this.f);
        this.d = new ImageButton(context);
        try {
            this.d.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            this.d.setBackgroundDrawable(Drawable.createFromStream(context.getAssets().open("mobisage/clear.png"), null));
        } catch (IOException e3) {
            this.d.setBackgroundDrawable(null);
            this.d.setImageResource(17301533);
        }
        LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(-2, -2);
        layoutParams3.setMargins(0, 0, 20, 0);
        layoutParams3.gravity = 16;
        this.d.setLayoutParams(layoutParams3);
        this.d.setVisibility(8);
        this.g = new a(this, (byte) 0);
        this.d.setOnClickListener(this.g);
        addView(this.d);
    }

    public MobiSageBaiduBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        setOrientation(0);
        this.a = context;
        setBackgroundResource(17301526);
        this.c = new ImageButton(context);
        try {
            this.c.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            this.c.setBackgroundDrawable(Drawable.createFromStream(context.getAssets().open("mobisage/baidu.png"), null));
        } catch (IOException e2) {
            this.c.setBackgroundDrawable(null);
            this.c.setImageResource(17301600);
        }
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
        layoutParams.gravity = 16;
        this.c.setLayoutParams(layoutParams);
        addView(this.c);
        this.b = new EditText(context);
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-1, -1, 1.0f);
        this.b.setBackgroundDrawable(null);
        this.b.setLayoutParams(layoutParams2);
        this.b.setSingleLine(true);
        this.b.setHint("百度一下");
        addView(this.b);
        this.e = new c(this, (byte) 0);
        this.b.addTextChangedListener(this.e);
        this.f = new b(this, (byte) 0);
        this.b.setOnKeyListener(this.f);
        this.d = new ImageButton(context);
        try {
            this.d.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            this.d.setBackgroundDrawable(Drawable.createFromStream(context.getAssets().open("mobisage/clear.png"), null));
        } catch (IOException e3) {
            this.d.setBackgroundDrawable(null);
            this.d.setImageResource(17301533);
        }
        LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(-2, -2);
        layoutParams3.gravity = 16;
        this.d.setLayoutParams(layoutParams3);
        this.d.setVisibility(8);
        this.g = new a(this, (byte) 0);
        this.d.setOnClickListener(this.g);
        addView(this.d);
    }

    /* access modifiers changed from: protected */
    public final void onStartBaiduSearch() {
        String obj = this.b.getText().toString();
        Intent intent = new Intent(this.a, MobiSageActivity.class);
        intent.putExtra("type", "baidu");
        intent.putExtra("keyword", obj);
        this.a.startActivity(intent);
    }

    class c implements TextWatcher {
        private c() {
        }

        /* synthetic */ c(MobiSageBaiduBar mobiSageBaiduBar, byte b) {
            this();
        }

        public final void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        public final void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        public final void afterTextChanged(Editable editable) {
            if (MobiSageBaiduBar.this.b.length() == 0) {
                MobiSageBaiduBar.this.d.setVisibility(8);
            } else {
                MobiSageBaiduBar.this.d.setVisibility(0);
            }
        }
    }

    class b implements View.OnKeyListener {
        private b() {
        }

        /* synthetic */ b(MobiSageBaiduBar mobiSageBaiduBar, byte b) {
            this();
        }

        public final boolean onKey(View view, int i, KeyEvent keyEvent) {
            if (i != 66 || keyEvent.getAction() != 1) {
                return false;
            }
            MobiSageBaiduBar.this.onStartBaiduSearch();
            return true;
        }
    }

    class a implements View.OnClickListener {
        private a() {
        }

        /* synthetic */ a(MobiSageBaiduBar mobiSageBaiduBar, byte b) {
            this();
        }

        public final void onClick(View view) {
            MobiSageBaiduBar.this.b.setText("");
        }
    }
}
