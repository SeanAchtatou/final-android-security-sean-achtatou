package com.mobisage.android;

import android.os.Bundle;
import java.util.UUID;
import java.util.concurrent.LinkedBlockingQueue;

/* renamed from: com.mobisage.android.b  reason: case insensitive filesystem */
final class C0002b {
    public UUID a = null;
    public UUID b = UUID.randomUUID();
    public Bundle c = new Bundle();
    public Bundle d = new Bundle();
    public LinkedBlockingQueue<C0002b> e = new LinkedBlockingQueue<>();
    public LinkedBlockingQueue<MobiSageMessage> f = new LinkedBlockingQueue<>();
    public C0001a g;

    /* access modifiers changed from: protected */
    public final void finalize() throws Throwable {
        super.finalize();
        this.g = null;
        this.c.clear();
        this.d.clear();
        this.e.clear();
        this.f.clear();
    }

    public final boolean a() {
        if (this.f.size() == 0 && this.e.size() == 0) {
            return true;
        }
        return false;
    }
}
