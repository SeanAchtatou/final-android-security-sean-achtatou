package com.mobisage.android;

import android.net.http.AndroidHttpClient;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;

/* renamed from: com.mobisage.android.x  reason: case insensitive filesystem */
final class C0024x extends N {
    C0024x(C0022v vVar) {
        super(vVar);
    }

    public final void run() {
        AndroidHttpClient newInstance = AndroidHttpClient.newInstance("MobiSageSDK");
        try {
            HttpResponse execute = newInstance.execute(this.a.createHttpRequest());
            this.a.result.putInt("StatusCode", execute.getStatusLine().getStatusCode());
            this.a.result.putByteArray("ResponseBody", EntityUtils.toByteArray(execute.getEntity()));
            newInstance.close();
            if (this.a.callback != null) {
                this.a.callback.onMobiSageMessageFinish(this.a);
            }
        } catch (Exception e) {
            newInstance.close();
            if (this.a.callback != null) {
                this.a.callback.onMobiSageMessageFinish(this.a);
            }
        } catch (Throwable th) {
            newInstance.close();
            if (this.a.callback != null) {
                this.a.callback.onMobiSageMessageFinish(this.a);
            }
            throw th;
        }
        super.run();
    }
}
