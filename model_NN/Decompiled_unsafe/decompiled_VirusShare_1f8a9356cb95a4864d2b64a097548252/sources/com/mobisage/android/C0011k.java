package com.mobisage.android;

import android.os.Handler;
import android.os.Message;

/* renamed from: com.mobisage.android.k  reason: case insensitive filesystem */
final class C0011k extends Q {
    public Handler a;

    C0011k(Handler handler) {
        this.a = handler;
        this.d = false;
        this.e = 0;
    }

    /* access modifiers changed from: protected */
    public final void finalize() throws Throwable {
        super.finalize();
    }

    public final void run() {
        if (this.a != null) {
            Message obtainMessage = this.a.obtainMessage();
            obtainMessage.what = 1004;
            obtainMessage.sendToTarget();
        }
    }
}
