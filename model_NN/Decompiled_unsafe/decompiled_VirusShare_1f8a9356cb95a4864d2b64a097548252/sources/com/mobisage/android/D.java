package com.mobisage.android;

import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;

final class D {
    private static D a = new D();
    private String b;
    private E c;

    public static D a() {
        return a;
    }

    private D() {
        if (((LocationManager) C0018r.h.getSystemService("location")) != null) {
            this.c = new E();
            this.c.e = 3600;
            R.a().a(this.c);
        }
        this.b = "0 0";
    }

    /* access modifiers changed from: protected */
    public final void finalize() throws Throwable {
        R.a().b(this.c);
        super.finalize();
    }

    public final String b() {
        return this.b;
    }

    public final void c() {
        LocationManager locationManager;
        String bestProvider;
        Location lastKnownLocation;
        if (((Boolean) C0023w.a().a("enablelocation")).booleanValue() && (bestProvider = (locationManager = (LocationManager) C0018r.h.getSystemService("location")).getBestProvider(new Criteria(), true)) != null && (lastKnownLocation = locationManager.getLastKnownLocation(bestProvider)) != null) {
            this.b = String.valueOf(lastKnownLocation.getLongitude()) + " " + String.valueOf(lastKnownLocation.getLatitude());
        }
    }
}
