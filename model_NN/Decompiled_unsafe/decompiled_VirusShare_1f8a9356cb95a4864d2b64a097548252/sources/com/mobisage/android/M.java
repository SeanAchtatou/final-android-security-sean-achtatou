package com.mobisage.android;

import android.view.animation.Animation;
import android.view.animation.RotateAnimation;

final class M extends P {
    M() {
    }

    public final Animation a(int i) {
        RotateAnimation rotateAnimation = new RotateAnimation(0.0f, 360.0f, 1, 0.5f, 1, 0.5f);
        rotateAnimation.setDuration(1000);
        return rotateAnimation;
    }

    public final Animation b(int i) {
        RotateAnimation rotateAnimation = new RotateAnimation(-360.0f, 0.0f, 1, 0.5f, 1, 0.5f);
        rotateAnimation.setDuration(1000);
        return rotateAnimation;
    }
}
