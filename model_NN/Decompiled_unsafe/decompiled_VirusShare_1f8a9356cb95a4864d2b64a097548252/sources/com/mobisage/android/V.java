package com.mobisage.android;

import android.os.Build;
import android.os.Handler;
import android.text.format.Time;
import com.mobisage.android.SNSSSLSocketFactory;
import java.io.File;
import java.net.URLEncoder;
import java.util.UUID;

final class V extends aa {
    V(Handler handler) {
        super(handler);
        this.c = u.q;
    }

    /* access modifiers changed from: protected */
    public final void f(C0002b bVar) {
        StringBuilder sb = new StringBuilder();
        sb.append("http://trc.adsage.com/trc/sdk/x.gif?");
        sb.append("ver=M4_01");
        sb.append("&md=" + URLEncoder.encode(Build.MODEL));
        sb.append("&pf=2");
        sb.append("&ib=1");
        sb.append("&uid=" + C0025y.a);
        sb.append("&mac=" + C0025y.b(C0018r.h));
        sb.append("&ssn=" + C0025y.g);
        sb.append("&aid=" + C0025y.e);
        if (Build.VERSION.SDK_INT >= 9) {
            sb.append("&srn=" + Build.SERIAL);
        } else {
            sb.append("&srn=");
        }
        sb.append("&ie=" + C0025y.f);
        Time time = new Time();
        time.setToNow();
        String str = C0018r.f + "/Track" + "/" + String.valueOf(time.toMillis(true) / 1000) + "_" + UUID.randomUUID().toString() + ".dat";
        File file = new File(str);
        file.mkdirs();
        SNSSSLSocketFactory.a.a(file, sb.toString());
        this.e.put(bVar.b, bVar);
        X x = new X();
        x.a = str;
        x.callback = this.g;
        bVar.f.add(x);
        this.f.put(x.c, bVar.b);
        H.a().a(x);
    }
}
