package com.mobisage.android;

import android.content.Context;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;

/* renamed from: com.mobisage.android.r  reason: case insensitive filesystem */
final class C0018r {
    public static String a = "";
    public static String b = "";
    public static String c = "";
    public static String d = "";
    public static String e = null;
    public static String f = null;
    public static long g = 0;
    public static Context h = null;
    private static boolean i = false;

    public static final boolean a(Context context) {
        if (i) {
            return i;
        }
        h = context.getApplicationContext();
        PackageManager packageManager = context.getPackageManager();
        try {
            Bundle bundle = packageManager.getApplicationInfo(context.getPackageName(), 128).metaData;
            String string = bundle.getString("Mobisage_publishID");
            a = string;
            if (string == null) {
                a = "";
            }
            a = a.replace(" ", "").toLowerCase();
            String string2 = bundle.getString("Mobisage_channel");
            b = string2;
            if (string2 == null) {
                b = "";
            }
            context.getPackageName();
            d = packageManager.getPackageInfo(context.getPackageName(), 0).versionName;
            c = context.getApplicationInfo().loadLabel(packageManager).toString();
        } catch (PackageManager.NameNotFoundException e2) {
        }
        e = context.getCacheDir().getAbsolutePath();
        f = context.getFilesDir().getAbsolutePath();
        if (Build.VERSION.SDK_INT >= 9) {
            h.registerReceiver(new C0016p(), new IntentFilter("android.intent.action.DOWNLOAD_COMPLETE"));
        }
        i = true;
        return true;
    }
}
