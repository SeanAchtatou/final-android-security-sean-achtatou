package com.mobisage.android;

import android.app.DownloadManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.util.Log;
import java.io.File;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;

public final class MobiSageApkService extends Service {
    private static ConcurrentHashMap<String, C0015o> a = new ConcurrentHashMap<>();
    private static LinkedBlockingQueue<C0015o> b = new LinkedBlockingQueue<>();
    private static C0017q c;

    public final IBinder onBind(Intent intent) {
        return null;
    }

    public final int onStartCommand(Intent intent, int flags, int startId) {
        Bundle bundleExtra = intent.getBundleExtra("ExtraData");
        switch (bundleExtra.getInt("action")) {
            case 0:
                File externalStorageDirectory = Environment.getExternalStorageDirectory();
                new File(externalStorageDirectory.getPath() + "/" + Environment.DIRECTORY_DOWNLOADS + "/").mkdirs();
                String string = bundleExtra.getString("name");
                String str = externalStorageDirectory.getPath() + "/" + Environment.DIRECTORY_DOWNLOADS + "/" + string;
                String substring = string.substring(string.lastIndexOf("."));
                File file = new File(str);
                Integer num = 1;
                while (file.exists()) {
                    file = new File(str.replace(substring, "(" + num.toString() + ")" + substring));
                    num = Integer.valueOf(num.intValue() + 1);
                }
                if (Build.VERSION.SDK_INT < 9) {
                    C0015o oVar = new C0015o();
                    oVar.sourceURL = bundleExtra.getString("lpg");
                    oVar.a = bundleExtra.getString("name");
                    oVar.targetURL = file.getPath();
                    oVar.tempURL = str + ".tp";
                    a.put(oVar.c.toString(), oVar);
                    b.add(oVar);
                    Context context = C0018r.h;
                    Intent intent2 = new Intent(context, MobiSageApkService.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("did", oVar.c.toString());
                    bundle.putInt("action", 1);
                    intent2.putExtra("ExtraData", bundle);
                    PendingIntent service = PendingIntent.getService(context, 0, intent2, 134217728);
                    Notification notification = new Notification();
                    notification.flags = 16;
                    notification.tickerText = oVar.a;
                    notification.icon = 17301633;
                    notification.setLatestEventInfo(context, oVar.a, "下载准备中，点击取消下载", service);
                    ((NotificationManager) context.getSystemService("notification")).notify(oVar.c.hashCode(), notification);
                    a();
                    break;
                } else {
                    DownloadManager.Request request = new DownloadManager.Request(Uri.parse(bundleExtra.getString("lpg")));
                    request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, file.getName());
                    request.setAllowedNetworkTypes(3);
                    request.setAllowedOverRoaming(true);
                    request.setMimeType("application/vnd.android.package-archive");
                    ((DownloadManager) getSystemService("download")).enqueue(request);
                    break;
                }
            case 1:
                Log.d("ApkService", bundleExtra.getString("did"));
                String string2 = bundleExtra.getString("did");
                if (a.containsKey(string2)) {
                    C0015o oVar2 = a.get(string2);
                    a.remove(string2);
                    if (b.contains(oVar2)) {
                        b.remove(oVar2);
                    } else {
                        if (c != null) {
                            c.a();
                            c = null;
                        }
                        new File(oVar2.tempURL).delete();
                    }
                    ((NotificationManager) getApplicationContext().getSystemService("notification")).cancel(oVar2.c.hashCode());
                    a();
                    break;
                }
                break;
            case 2:
                if (a.containsKey(bundleExtra.getString("did"))) {
                    C0015o oVar3 = a.get(bundleExtra.getString("did"));
                    oVar3.result = new Bundle();
                    b.add(oVar3);
                    a();
                    break;
                }
                break;
            case 3:
                if (a.containsKey(bundleExtra.getString("did"))) {
                    C0015o oVar4 = a.get(bundleExtra.getString("did"));
                    if (bundleExtra.containsKey("ErrorText") || bundleExtra.getInt("StatusCode") >= 400) {
                        Context context2 = C0018r.h;
                        Intent intent3 = new Intent(context2, MobiSageApkService.class);
                        Bundle bundle2 = new Bundle();
                        bundle2.putString("did", oVar4.c.toString());
                        bundle2.putInt("action", 2);
                        intent3.putExtra("ExtraData", bundle2);
                        PendingIntent service2 = PendingIntent.getService(context2, 0, intent3, 134217728);
                        Notification notification2 = new Notification();
                        notification2.flags = 16;
                        notification2.tickerText = oVar4.a;
                        notification2.icon = 17301624;
                        notification2.setLatestEventInfo(context2, oVar4.a, "下载失败，点击重试", service2);
                        ((NotificationManager) context2.getSystemService("notification")).notify(oVar4.c.hashCode(), notification2);
                    } else {
                        a.remove(oVar4.c.toString());
                        Context context3 = C0018r.h;
                        Intent intent4 = new Intent();
                        intent4.setAction("android.intent.action.VIEW");
                        intent4.setDataAndType(Uri.parse("file://" + oVar4.targetURL), "application/vnd.android.package-archive");
                        intent4.setFlags(268435456);
                        PendingIntent activity = PendingIntent.getActivity(context3, 0, intent4, 134217728);
                        Notification notification3 = new Notification();
                        notification3.tickerText = oVar4.a;
                        notification3.icon = 17301634;
                        notification3.flags = 16;
                        notification3.setLatestEventInfo(context3, oVar4.a, "下载完成。点击进行安装", activity);
                        ((NotificationManager) context3.getSystemService("notification")).notify(oVar4.c.hashCode(), notification3);
                    }
                    c = null;
                    a();
                    break;
                } else {
                    ((NotificationManager) getApplicationContext().getSystemService("notification")).cancel(bundleExtra.getString("did").hashCode());
                    break;
                }
                break;
            case 4:
                DownloadManager.Query query = new DownloadManager.Query();
                query.setFilterById(bundleExtra.getLong("did"));
                Cursor query2 = ((DownloadManager) getSystemService("download")).query(query);
                if (query2.getCount() != 0) {
                    query2.moveToFirst();
                    if (query2.getInt(query2.getColumnIndex("status")) == 8) {
                        String string3 = query2.getString(query2.getColumnIndex("title"));
                        String string4 = query2.getString(query2.getColumnIndex("local_uri"));
                        Context context4 = C0018r.h;
                        Intent intent5 = new Intent();
                        intent5.setAction("android.intent.action.VIEW");
                        intent5.setDataAndType(Uri.parse("file://" + string4), "application/vnd.android.package-archive");
                        intent5.setFlags(268435456);
                        PendingIntent activity2 = PendingIntent.getActivity(context4, 0, intent5, 134217728);
                        Notification notification4 = new Notification();
                        notification4.tickerText = string3;
                        notification4.icon = 17301634;
                        notification4.flags = 16;
                        notification4.setLatestEventInfo(context4, string3, "下载完成。点击进行安装", activity2);
                        ((NotificationManager) context4.getSystemService("notification")).notify(string4.hashCode(), notification4);
                        break;
                    }
                }
                break;
        }
        return super.onStartCommand(intent, flags, startId);
    }

    private static void a() {
        if (c == null && b.size() != 0) {
            c = (C0017q) b.poll().createMessageRunnable();
            new Thread(c).start();
        }
    }
}
