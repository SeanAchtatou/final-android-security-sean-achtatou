package com.imocha;

import MobWin.cnst.PROTOCOL_ENCODING;
import android.util.Log;
import java.util.ArrayList;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

final class ac implements Runnable {
    private String a;
    private ShareActivity b;
    private /* synthetic */ ShareActivity c;

    ac(ShareActivity shareActivity, ShareActivity shareActivity2, String str) {
        this.c = shareActivity;
        this.b = shareActivity2;
        this.a = str;
    }

    public final void run() {
        try {
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
            String trim = this.b.d.getText().toString().trim();
            String trim2 = this.b.e.getText().toString().trim();
            String trim3 = this.b.f.getText().toString().trim();
            String str = this.b.a.c.b;
            String substring = trim2.length() > 20 ? trim2.substring(0, 20) : trim2;
            String replace = ((String) this.c.c.get("head")).replace("{USERNAME}", trim3).replace("{USERPHONE}", substring);
            String replace2 = ((String) this.c.c.get("end")).replace("{USERNAME}", trim3).replace("{USERPHONE}", substring);
            HttpPost httpPost = new HttpPost(String.valueOf(this.a) + "t=sms" + "&guid=" + af.a().d(this.b));
            ArrayList arrayList = new ArrayList();
            arrayList.add(new BasicNameValuePair("to", trim));
            arrayList.add(new BasicNameValuePair("from", substring));
            arrayList.add(new BasicNameValuePair("sender", trim3));
            arrayList.add(new BasicNameValuePair("subject", str));
            arrayList.add(new BasicNameValuePair("content", String.valueOf(replace) + "\n" + ((String) this.c.c.get("content")) + "\n" + replace2));
            httpPost.setEntity(new UrlEncodedFormEntity(arrayList, PROTOCOL_ENCODING.value));
            Log.v("iMocha SDK", "分享" + defaultHttpClient.execute(httpPost).getStatusLine());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
