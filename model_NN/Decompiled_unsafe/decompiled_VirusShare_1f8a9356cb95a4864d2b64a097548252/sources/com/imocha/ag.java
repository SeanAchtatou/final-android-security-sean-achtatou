package com.imocha;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;
import java.util.ArrayList;

final class ag extends BaseExpandableListAdapter {
    private /* synthetic */ v a;
    private final /* synthetic */ String b;

    ag(v vVar, String str) {
        this.a = vVar;
        this.b = str;
    }

    public final Object getChild(int i, int i2) {
        return ((e) this.a.a.k.get(this.a.a.j.get(i))).a.get(i2);
    }

    public final long getChildId(int i, int i2) {
        return (long) i2;
    }

    public final View getChildView(int i, int i2, boolean z, View view, ViewGroup viewGroup) {
        ArrayList arrayList = (ArrayList) getChild(i, i2);
        TextView textView = new TextView(this.a.a);
        textView.setText(String.valueOf((String) arrayList.get(0)) + ":" + ((String) arrayList.get(1)));
        if (this.b.startsWith("1.")) {
            textView.setPadding(30, 10, 10, 10);
        } else {
            textView.setPadding(60, 10, 10, 10);
        }
        return textView;
    }

    public final int getChildrenCount(int i) {
        return ((e) this.a.a.k.get(this.a.a.j.get(i))).a.size();
    }

    public final Object getGroup(int i) {
        return ((e) this.a.a.k.get((String) this.a.a.j.get(i))).b;
    }

    public final int getGroupCount() {
        return this.a.a.j.size();
    }

    public final long getGroupId(int i) {
        return (long) i;
    }

    public final View getGroupView(int i, boolean z, View view, ViewGroup viewGroup) {
        TextView textView = new TextView(this.a.a);
        textView.setText(((e) this.a.a.k.get(this.a.a.j.get(i))).b);
        if (this.b.startsWith("1.")) {
            textView.setPadding(10, 10, 10, 10);
        } else {
            textView.setPadding(60, 10, 10, 10);
        }
        return textView;
    }

    public final boolean hasStableIds() {
        return true;
    }

    public final boolean isChildSelectable(int i, int i2) {
        return true;
    }
}
