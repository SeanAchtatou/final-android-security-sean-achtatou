package com.imocha;

import java.lang.ref.WeakReference;

final class av implements Runnable {
    private WeakReference a = null;

    av(IMochaAdView iMochaAdView) {
        this.a = new WeakReference(iMochaAdView);
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0059  */
    /* JADX WARNING: Removed duplicated region for block: B:50:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r4 = this;
            java.lang.ref.WeakReference r0 = r4.a
            java.lang.Object r4 = r0.get()
            com.imocha.IMochaAdView r4 = (com.imocha.IMochaAdView) r4
            if (r4 != 0) goto L_0x000b
        L_0x000a:
            return
        L_0x000b:
            com.imocha.ai r0 = r4.g
            if (r0 != 0) goto L_0x0016
            com.imocha.ai r0 = new com.imocha.ai
            r0.<init>(r4)
            r4.g = r0
        L_0x0016:
            boolean r0 = r4.r     // Catch:{ Exception -> 0x006a }
            if (r0 != 0) goto L_0x0036
            boolean r0 = r4.u     // Catch:{ Exception -> 0x006a }
            if (r0 != 0) goto L_0x0036
            r4.q = false     // Catch:{ Exception -> 0x006a }
            com.imocha.IMochaAdType r0 = r4.j
            com.imocha.IMochaAdType r1 = com.imocha.IMochaAdType.BANNER
            if (r0 != r1) goto L_0x000a
            com.imocha.av r0 = new com.imocha.av
            r0.<init>(r4)
            long r1 = r4.d
            r4.refurbishAd(r0, r1)
            goto L_0x000a
        L_0x0036:
            r4.u = false     // Catch:{ Exception -> 0x006a }
            java.lang.String r0 = "iMocha"
            java.lang.String r1 = "start download Ad"
            android.util.Log.v(r0, r1)     // Catch:{ Exception -> 0x006a }
            com.imocha.IMochaEventCode r0 = com.imocha.IMochaEventCode.startDownloadAd     // Catch:{ Exception -> 0x006a }
            com.imocha.IMochaAdView.onEvent(r4, r0)     // Catch:{ Exception -> 0x006a }
            com.imocha.ai r0 = r4.g     // Catch:{ Exception -> 0x006a }
            int r0 = r0.a()     // Catch:{ Exception -> 0x006a }
            switch(r0) {
                case 0: goto L_0x0064;
                case 1: goto L_0x0084;
                case 2: goto L_0x004e;
                case 3: goto L_0x009c;
                case 4: goto L_0x004e;
                case 5: goto L_0x00a2;
                case 6: goto L_0x00ae;
                case 7: goto L_0x004e;
                case 8: goto L_0x00a8;
                default: goto L_0x004e;
            }     // Catch:{ Exception -> 0x006a }
        L_0x004e:
            com.imocha.IMochaEventCode r0 = com.imocha.IMochaEventCode.downloadError2     // Catch:{ Exception -> 0x006a }
            com.imocha.IMochaAdView.onEvent(r4, r0)     // Catch:{ Exception -> 0x006a }
        L_0x0053:
            com.imocha.IMochaAdType r0 = r4.j
            com.imocha.IMochaAdType r1 = com.imocha.IMochaAdType.BANNER
            if (r0 != r1) goto L_0x000a
            com.imocha.av r0 = new com.imocha.av
            r0.<init>(r4)
            long r1 = r4.d
            r4.refurbishAd(r0, r1)
            goto L_0x000a
        L_0x0064:
            com.imocha.IMochaEventCode r0 = com.imocha.IMochaEventCode.downloadError0     // Catch:{ Exception -> 0x006a }
            com.imocha.IMochaAdView.onEvent(r4, r0)     // Catch:{ Exception -> 0x006a }
            goto L_0x0053
        L_0x006a:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x008a }
            com.imocha.IMochaEventCode r0 = com.imocha.IMochaEventCode.downloadError2     // Catch:{ all -> 0x008a }
            com.imocha.IMochaAdView.onEvent(r4, r0)     // Catch:{ all -> 0x008a }
            com.imocha.IMochaAdType r0 = r4.j
            com.imocha.IMochaAdType r1 = com.imocha.IMochaAdType.BANNER
            if (r0 != r1) goto L_0x000a
            com.imocha.av r0 = new com.imocha.av
            r0.<init>(r4)
            long r1 = r4.d
            r4.refurbishAd(r0, r1)
            goto L_0x000a
        L_0x0084:
            com.imocha.IMochaEventCode r0 = com.imocha.IMochaEventCode.downloadError1     // Catch:{ Exception -> 0x006a }
            com.imocha.IMochaAdView.onEvent(r4, r0)     // Catch:{ Exception -> 0x006a }
            goto L_0x0053
        L_0x008a:
            r0 = move-exception
            com.imocha.IMochaAdType r1 = r4.j
            com.imocha.IMochaAdType r2 = com.imocha.IMochaAdType.BANNER
            if (r1 != r2) goto L_0x009b
            com.imocha.av r1 = new com.imocha.av
            r1.<init>(r4)
            long r2 = r4.d
            r4.refurbishAd(r1, r2)
        L_0x009b:
            throw r0
        L_0x009c:
            com.imocha.IMochaEventCode r0 = com.imocha.IMochaEventCode.downloadError3     // Catch:{ Exception -> 0x006a }
            com.imocha.IMochaAdView.onEvent(r4, r0)     // Catch:{ Exception -> 0x006a }
            goto L_0x0053
        L_0x00a2:
            com.imocha.IMochaEventCode r0 = com.imocha.IMochaEventCode.downloadError5     // Catch:{ Exception -> 0x006a }
            com.imocha.IMochaAdView.onEvent(r4, r0)     // Catch:{ Exception -> 0x006a }
            goto L_0x0053
        L_0x00a8:
            com.imocha.IMochaEventCode r0 = com.imocha.IMochaEventCode.downloadError8     // Catch:{ Exception -> 0x006a }
            com.imocha.IMochaAdView.onEvent(r4, r0)     // Catch:{ Exception -> 0x006a }
            goto L_0x0053
        L_0x00ae:
            java.util.HashMap r0 = com.imocha.IMochaAdView.f     // Catch:{ Exception -> 0x006a }
            java.lang.String r1 = r4.e     // Catch:{ Exception -> 0x006a }
            java.lang.Object r0 = r0.get(r1)     // Catch:{ Exception -> 0x006a }
            if (r0 == 0) goto L_0x00cf
            com.imocha.IMochaAdType r0 = r4.j     // Catch:{ Exception -> 0x006a }
            com.imocha.IMochaAdType r1 = com.imocha.IMochaAdType.BANNER     // Catch:{ Exception -> 0x006a }
            if (r0 != r1) goto L_0x00c9
            android.os.Handler r0 = r4.c     // Catch:{ Exception -> 0x006a }
            com.imocha.h r1 = new com.imocha.h     // Catch:{ Exception -> 0x006a }
            r1.<init>(r4)     // Catch:{ Exception -> 0x006a }
            r0.post(r1)     // Catch:{ Exception -> 0x006a }
            goto L_0x0053
        L_0x00c9:
            com.imocha.IMochaEventCode r0 = com.imocha.IMochaEventCode.adDownloadFinish     // Catch:{ Exception -> 0x006a }
            com.imocha.IMochaAdView.onEvent(r4, r0)     // Catch:{ Exception -> 0x006a }
            goto L_0x0053
        L_0x00cf:
            com.imocha.IMochaEventCode r0 = com.imocha.IMochaEventCode.downloadError2     // Catch:{ Exception -> 0x006a }
            com.imocha.IMochaAdView.onEvent(r4, r0)     // Catch:{ Exception -> 0x006a }
            goto L_0x0053
        */
        throw new UnsupportedOperationException("Method not decompiled: com.imocha.av.run():void");
    }
}
