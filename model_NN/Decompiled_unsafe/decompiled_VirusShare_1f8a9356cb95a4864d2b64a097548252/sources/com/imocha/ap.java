package com.imocha;

import android.content.Context;
import android.view.MotionEvent;
import android.widget.ImageView;

final class ap extends ImageView {
    private /* synthetic */ FullScreenAdActivity a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    ap(FullScreenAdActivity fullScreenAdActivity, Context context) {
        super(context);
        this.a = fullScreenAdActivity;
    }

    public final boolean onTouchEvent(MotionEvent motionEvent) {
        return this.a.onTouchEvent(motionEvent);
    }
}
