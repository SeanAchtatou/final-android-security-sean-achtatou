package com.imocha;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.os.Handler;
import android.telephony.CellLocation;
import android.telephony.TelephonyManager;
import android.util.Log;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public final class aj {
    double a = 0.0d;
    double b = 0.0d;
    Handler c = new Handler();
    private LocationManager d;
    private s e;
    private ScheduledExecutorService f;
    private ScheduledFuture g;
    private ScheduledFuture h;
    /* access modifiers changed from: private */
    public Location i;
    private w j;
    private Context k = null;
    private long l = 0;

    public aj(Context context, Handler handler) {
        this.k = context;
        this.c = handler;
    }

    static /* synthetic */ void a(aj ajVar, Location location) {
        ajVar.a("lat:" + location.getLatitude() + " long:" + location.getLongitude());
        ajVar.a(String.valueOf(location.getLongitude()) + "," + location.getLatitude());
    }

    private void a(s sVar) {
        if (this.f == null) {
            this.f = Executors.newScheduledThreadPool(2);
        }
        a("startTimer runnable " + sVar.a);
        this.g = this.f.schedule(new aa(this, sVar), 10, TimeUnit.SECONDS);
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        Log.v("LocationTest", str);
        if (this.j != null) {
            Date date = new Date(System.currentTimeMillis());
            String.valueOf(date.getHours()) + ":" + date.getMinutes() + ":" + date.getSeconds() + " " + str + "\n";
        }
    }

    /* access modifiers changed from: private */
    public boolean a(boolean z) {
        a("cancelTimer ");
        if (this.g != null) {
            return this.g.cancel(z);
        }
        return false;
    }

    /* access modifiers changed from: private */
    public void d() {
        if (this.e.a.equals("gps")) {
            f();
        } else if (this.e.a.equals("network")) {
            Log.v("LocationTest", "into requstCellLocation");
            this.e = new s(this, "cell");
            TelephonyManager telephonyManager = (TelephonyManager) this.k.getSystemService("phone");
            if (telephonyManager == null) {
                d();
            } else {
                int phoneType = telephonyManager.getPhoneType();
                if (phoneType == 1) {
                    if (this.f == null) {
                        this.f = Executors.newScheduledThreadPool(2);
                    }
                    this.h = this.f.schedule(new ab(this, telephonyManager), 0, TimeUnit.SECONDS);
                } else if (phoneType == 2) {
                    CellLocation cellLocation = telephonyManager.getCellLocation();
                    Method[] methods = cellLocation.getClass().getMethods();
                    if (methods != null) {
                        for (Method method : methods) {
                            if (method != null) {
                                try {
                                    if (method.getName().equals("getBaseStationLatitude")) {
                                        this.a = (double) ((Integer) method.invoke(cellLocation, new Object[0])).intValue();
                                    } else if (method.getName().equals("getBaseStationLongitude")) {
                                        this.b = (double) ((Integer) method.invoke(cellLocation, new Object[0])).intValue();
                                    }
                                } catch (Exception e2) {
                                    e2.printStackTrace();
                                }
                            }
                        }
                        if (!(this.a == 0.0d && this.b == 0.0d)) {
                            this.a /= 14400.0d;
                            this.b /= 14400.0d;
                        }
                    }
                }
            }
        } else {
            e();
        }
        a(this.e);
    }

    private void e() {
        if (this.d == null) {
            this.d = (LocationManager) this.k.getSystemService("location");
        }
        if (!(this.d.isProviderEnabled("gps"))) {
            f();
            return;
        }
        try {
            this.e = new s(this, "gps");
            this.d.requestLocationUpdates("gps", 10000, 200.0f, this.e);
        } catch (Exception e2) {
        }
    }

    private void f() {
        this.e = new s(this, "network");
        try {
            this.d.requestLocationUpdates("network", 10000, 200.0f, this.e);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public final void a() {
        if (System.currentTimeMillis() - this.l >= 30000) {
            this.l = System.currentTimeMillis();
            a(false);
            b();
            e();
            a(this.e);
        }
    }

    public final void b() {
        if (this.e != null) {
            if (this.e.a.equals("cell")) {
                a("removeUpdates " + this.e.a);
                if (this.h != null) {
                    this.h.cancel(false);
                    this.h = null;
                    return;
                }
                return;
            }
            a("removeUpdates " + this.e.a);
            this.d.removeUpdates(this.e);
        }
    }

    public final Location c() {
        return this.i;
    }
}
