package com.imocha;

import MobWin.cnst.PROTOCOL_ENCODING;
import android.util.Log;
import java.util.ArrayList;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

final class f implements Runnable {
    private String a;
    private y b;
    private /* synthetic */ y c;

    f(y yVar, y yVar2, String str) {
        this.c = yVar;
        this.b = yVar2;
        this.a = str;
    }

    public final void run() {
        try {
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
            String str = this.b.a.a;
            String trim = this.b.b.getText().toString().trim();
            String trim2 = this.b.c.getText().toString().trim();
            String str2 = this.b.a.b;
            String str3 = this.b.a.c;
            HttpPost httpPost = new HttpPost(String.valueOf(this.a) + "t=email&guid=" + af.a().d(this.b.getContext()));
            ArrayList arrayList = new ArrayList();
            arrayList.add(new BasicNameValuePair("to", str));
            arrayList.add(new BasicNameValuePair("from", trim));
            arrayList.add(new BasicNameValuePair("sender", trim2));
            arrayList.add(new BasicNameValuePair("subject", str2));
            arrayList.add(new BasicNameValuePair("content", str3));
            httpPost.setEntity(new UrlEncodedFormEntity(arrayList, PROTOCOL_ENCODING.value));
            Log.v("iMocha SDK", defaultHttpClient.execute(httpPost).getStatusLine().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
