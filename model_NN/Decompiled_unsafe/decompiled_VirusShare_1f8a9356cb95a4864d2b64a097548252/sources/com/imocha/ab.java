package com.imocha;

import android.location.Location;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

final class ab implements Runnable {
    private /* synthetic */ aj a;
    private final /* synthetic */ TelephonyManager b;

    ab(aj ajVar, TelephonyManager telephonyManager) {
        this.a = ajVar;
        this.b = telephonyManager;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException} */
    public final void run() {
        GsmCellLocation gsmCellLocation = (GsmCellLocation) this.b.getCellLocation();
        int cid = gsmCellLocation.getCid();
        int lac = gsmCellLocation.getLac();
        int intValue = Integer.valueOf(this.b.getNetworkOperator().substring(0, 3)).intValue();
        int intValue2 = Integer.valueOf(this.b.getNetworkOperator().substring(3, 5)).intValue();
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("version", "1.1.0");
            jSONObject.put("host", "maps.google.com");
            jSONObject.put("address_language", "zh_CN");
            jSONObject.put("radio_type", "gsm");
            jSONObject.put("request_address", true);
            JSONArray jSONArray = new JSONArray();
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("cell_id", cid);
            jSONObject2.put("location_area_code", lac);
            jSONObject2.put("mobile_country_code", intValue);
            jSONObject2.put("mobile_network_code", intValue2);
            jSONObject2.put("timing_advance", 0);
            jSONArray.put(jSONObject2);
            jSONObject.put("cell_towers", jSONArray);
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost("http://www.google.com/loc/json");
            httpPost.setEntity(new StringEntity(jSONObject.toString()));
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(defaultHttpClient.execute(httpPost).getEntity().getContent()));
            StringBuffer stringBuffer = new StringBuffer();
            for (String readLine = bufferedReader.readLine(); readLine != null; readLine = bufferedReader.readLine()) {
                stringBuffer.append(String.valueOf(readLine) + "\n");
            }
            JSONObject jSONObject3 = (JSONObject) new JSONObject(stringBuffer.toString()).get("location");
            this.a.a = jSONObject3.getDouble("latitude");
            this.a.b = jSONObject3.getDouble("longitude");
            if (this.a.a == 0.0d && this.a.b == 0.0d) {
                boolean unused = this.a.a(true);
                this.a.b();
                return;
            }
            this.a.i = new Location("network");
            this.a.i.setLatitude(this.a.a);
            this.a.i.setLongitude(this.a.b);
        } catch (Exception e) {
            e.printStackTrace();
            boolean unused2 = this.a.a(true);
            this.a.b();
            this.a.d();
        }
    }
}
