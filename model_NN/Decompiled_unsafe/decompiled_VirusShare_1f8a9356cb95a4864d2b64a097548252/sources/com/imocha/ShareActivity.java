package com.imocha;

import android.app.Activity;
import android.app.Dialog;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Contacts;
import android.text.InputFilter;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

public class ShareActivity extends Activity implements View.OnClickListener {
    private static Uri p = Uri.withAppendedPath(Uri.withAppendedPath(Uri.parse("content://com.android.contacts"), "data"), "phones");
    am a;
    ImageView b;
    HashMap c;
    /* access modifiers changed from: private */
    public EditText d;
    /* access modifiers changed from: private */
    public EditText e;
    /* access modifiers changed from: private */
    public EditText f;
    private TextView g;
    private boolean h = true;
    private Dialog i;
    /* access modifiers changed from: private */
    public ArrayList j = new ArrayList();
    /* access modifiers changed from: private */
    public HashMap k = new HashMap();
    private String l = "display_name";
    private String m = "_id";
    private String n = "data1";
    private String o = "contact_id";

    private View a(String str, int i2) {
        TextView textView = new TextView(this);
        textView.setText(str);
        if (i2 != -1) {
            textView.setTextColor(i2);
        }
        return textView;
    }

    private EditText a(EditText editText) {
        if (editText != null) {
            return editText;
        }
        EditText editText2 = new EditText(this);
        editText2.setSingleLine(true);
        return editText2;
    }

    private e a(String str) {
        e eVar = (e) this.k.get(str);
        if (eVar != null) {
            return eVar;
        }
        e eVar2 = new e(this);
        this.j.add(str);
        this.k.put(str, eVar2);
        return eVar2;
    }

    private void a() {
        Cursor cursor;
        Log.i("a", "getList");
        try {
            cursor = getContentResolver().query(Contacts.Phones.CONTENT_URI, new String[]{"name", "number", "person", "type"}, null, null, null);
        } catch (Exception e2) {
            e2.printStackTrace();
            cursor = null;
        }
        while (cursor.moveToNext()) {
            String string = cursor.getString(0);
            String string2 = cursor.getString(1);
            String string3 = cursor.getString(2);
            String string4 = cursor.getString(3);
            e a2 = a(string3);
            if (string.length() <= 0) {
                a2.b = string2;
            } else {
                a2.b = string;
            }
            ArrayList arrayList = new ArrayList();
            if (string4 == null) {
                arrayList.add("手机号码");
            } else if (string4.equals("1")) {
                arrayList.add("住宅电话");
            } else if (string4.equals("2")) {
                arrayList.add("手机号码");
            } else if (string4.equals("3")) {
                arrayList.add("单位电话");
            } else if (string4.equals("4")) {
                arrayList.add("单位传真");
            } else if (string4.equals("5")) {
                arrayList.add("住宅传真");
            } else if (string4.equals("6")) {
                arrayList.add("寻呼机号");
            } else {
                arrayList.add("其他电话");
            }
            arrayList.add(string2);
            a2.a.add(arrayList);
        }
        Cursor query = getContentResolver().query(Contacts.ContactMethods.CONTENT_URI, new String[]{"name", "data", "person", "kind", "type"}, null, null, null);
        while (query.moveToNext()) {
            String string5 = query.getString(0);
            String string6 = query.getString(1);
            String string7 = query.getString(2);
            String string8 = query.getString(3);
            String string9 = query.getString(4);
            if (string8.equals("1")) {
                e a3 = a(string7);
                if (string5.length() <= 0) {
                    a3.b = string6;
                } else {
                    a3.b = string5;
                }
                ArrayList arrayList2 = new ArrayList();
                if (string9 == null) {
                    arrayList2.add("电子邮箱");
                } else if (string9.equals("1")) {
                    arrayList2.add("主要邮箱");
                } else if (string9.equals("2")) {
                    arrayList2.add("单位邮箱");
                } else {
                    arrayList2.add("其他邮箱");
                }
                arrayList2.add(string6);
                a3.a.add(arrayList2);
            }
        }
        cursor.close();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    private boolean a(int i2) {
        String replace = this.d.getText().toString().trim().replace(65292, ',');
        if (replace.endsWith(",")) {
            replace = replace.substring(0, replace.length() - 1);
        }
        String[] split = replace.split(",");
        ArrayList arrayList = new ArrayList();
        for (int i3 = 0; i3 < split.length; i3++) {
            if (split[i3] != null && split[i3].trim().length() > 0) {
                arrayList.add(split[i3]);
            }
        }
        return arrayList.size() < i2;
    }

    private void b() {
        Cursor c2 = c();
        if (c2 != null) {
            while (c2.moveToNext()) {
                a(c2.getString(c2.getColumnIndex(this.m))).b = c2.getString(c2.getColumnIndex(this.l));
            }
            Cursor query = getContentResolver().query(Uri.withAppendedPath(Uri.withAppendedPath(Uri.parse("content://com.android.contacts"), "data"), "phones"), new String[]{this.n, this.o, "data2", this.l}, null, null, null);
            while (query.moveToNext()) {
                String[] columnNames = query.getColumnNames();
                for (String str : columnNames) {
                    Log.v("a", String.valueOf(str) + ":" + query.getString(query.getColumnIndex(str)));
                }
                Log.v("a", "----------------------------------------------------------------");
                String string = query.getString(0);
                String string2 = query.getString(1);
                String string3 = query.getString(2);
                String string4 = query.getString(3);
                e a2 = a(string2);
                if (string4.length() <= 0) {
                    a2.b = string;
                } else {
                    a2.b = string4;
                }
                ArrayList arrayList = new ArrayList();
                if (string3 == null) {
                    arrayList.add("手机号码");
                } else if (string3.equals("1")) {
                    arrayList.add("住宅电话");
                } else if (string3.equals("2")) {
                    arrayList.add("手机号码");
                } else if (string3.equals("3")) {
                    arrayList.add("单位电话");
                } else if (string3.equals("4")) {
                    arrayList.add("单位传真");
                } else if (string3.equals("5")) {
                    arrayList.add("住宅传真");
                } else if (string3.equals("6")) {
                    arrayList.add("寻呼机号");
                } else {
                    arrayList.add("其他电话");
                }
                arrayList.add(string);
                a2.a.add(arrayList);
            }
            Cursor query2 = getContentResolver().query(Uri.withAppendedPath(Uri.withAppendedPath(Uri.parse("content://com.android.contacts"), "data"), "emails"), new String[]{this.n, this.o, "data2", this.l}, null, null, null);
            while (query2.moveToNext()) {
                String string5 = query2.getString(0);
                String string6 = query2.getString(1);
                String string7 = query2.getString(2);
                String string8 = query2.getString(3);
                e a3 = a(string6);
                if (string8.length() <= 0) {
                    string8 = string5;
                }
                a3.b = string8;
                ArrayList arrayList2 = new ArrayList();
                if (string7 == null) {
                    arrayList2.add("电子邮箱");
                } else if (string7.equals("1")) {
                    arrayList2.add("主要邮箱");
                } else if (string7.equals("2")) {
                    arrayList2.add("单位邮箱");
                } else {
                    arrayList2.add("其他邮箱");
                }
                arrayList2.add(string5);
                a3.a.add(arrayList2);
            }
        }
    }

    private Cursor c() {
        try {
            return managedQuery(Uri.withAppendedPath(Uri.parse("content://com.android.contacts"), "contacts"), new String[]{this.m, this.l}, null, null, null);
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public void onClick(View view) {
        if (view.getId() != 255) {
            String trim = this.d.getText().toString().trim();
            String trim2 = this.f.getText().toString().trim();
            String trim3 = this.e.getText().toString().trim();
            if (trim.length() == 0) {
                Toast.makeText(getApplicationContext(), "收件人不能为空", 0).show();
            } else if (trim2.length() <= 0) {
                Toast.makeText(getApplicationContext(), "签名不能为空", 0).show();
            } else if (trim3.length() > 0 && trim3.length() > 50) {
                Toast.makeText(getApplicationContext(), "发件人 长度不能超过50个字符", 0).show();
            } else if (!a(6)) {
                Toast.makeText(getApplicationContext(), "仅限5人分享", 0).show();
            } else {
                IMochaAdView.b.schedule(new ac(this, this, this.a.a.a), 0, TimeUnit.SECONDS);
                finish();
            }
        } else if (!a(5)) {
            Toast.makeText(getApplicationContext(), "仅限5人分享", 0).show();
        } else {
            if (this.i == null) {
                this.i = new v(this, this);
            }
            this.b.setClickable(false);
            this.i.show();
        }
    }

    public void onCreate(Bundle bundle) {
        double d2;
        double d3;
        super.onCreate(bundle);
        requestWindowFeature(1);
        this.a = (am) IMochaAdView.f.get(getIntent().getStringExtra("SPACE_ID"));
        if (this.a == null) {
            finish();
            return;
        }
        ScrollView scrollView = new ScrollView(this);
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setPadding(5, 5, 5, 5);
        linearLayout.setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
        linearLayout.setOrientation(1);
        scrollView.addView(linearLayout);
        TableRow tableRow = new TableRow(this);
        TableRow tableRow2 = new TableRow(this);
        TableRow tableRow3 = new TableRow(this);
        TableLayout tableLayout = new TableLayout(this);
        tableLayout.setColumnShrinkable(1, true);
        tableLayout.setColumnStretchable(1, true);
        tableLayout.addView(tableRow);
        tableLayout.addView(tableRow2);
        tableLayout.addView(tableRow3);
        linearLayout.addView(tableLayout);
        tableRow.addView(a("收件人：", -1));
        this.d = a(this.d);
        tableRow.addView(this.d);
        if (Build.VERSION.RELEASE.startsWith("1.")) {
            a();
        } else {
            b();
        }
        if (this.j.size() == 0) {
            this.h = false;
        }
        if (this.h) {
            try {
                Bitmap decodeStream = BitmapFactory.decodeStream(af.b(this, "add.png"));
                this.b = new ImageView(this);
                this.b.setImageBitmap(decodeStream);
                this.b.setOnClickListener(this);
                this.b.setId(255);
                long e2 = (long) (af.a().e(this) * af.a().f(this));
                if (e2 < 112000) {
                    d2 = 24.0d;
                    d3 = 24.0d;
                } else if (e2 < 230400) {
                    d2 = 36.0d;
                    d3 = 36.0d;
                } else if (e2 < 344400) {
                    d2 = 40.0d;
                    d3 = 40.0d;
                } else {
                    d2 = 50.0d;
                    d3 = 50.0d;
                }
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams((int) d2, (int) d3);
                FrameLayout frameLayout = new FrameLayout(this);
                frameLayout.addView(this.b, layoutParams);
                TableRow.LayoutParams layoutParams2 = new TableRow.LayoutParams();
                layoutParams2.gravity = 16;
                tableRow.addView(frameLayout, layoutParams2);
            } catch (Exception e3) {
                e3.printStackTrace();
            }
        }
        tableRow.addView(a("*", -65536));
        tableRow2.addView(a("发件人：", -1));
        this.e = a(this.e);
        this.e.setFilters(new InputFilter[]{new InputFilter.LengthFilter(50)});
        tableRow2.addView(this.e);
        tableRow3.addView(a("签　名：", -1));
        this.f = a(this.f);
        this.f.setFilters(new InputFilter[]{new InputFilter.LengthFilter(12)});
        tableRow3.addView(this.f);
        if (this.h) {
            tableRow3.addView(new TextView(this));
        }
        tableRow3.addView(a("*", -65536));
        this.g = new TextView(this);
        String str = this.a.c.c;
        HashMap hashMap = new HashMap();
        str.replace("\r\n\r\n", "\n\n");
        int lastIndexOf = str.lastIndexOf("\n\n");
        int indexOf = str.indexOf("\n\n");
        if (indexOf != -1) {
            hashMap.put("head", str.substring(0, indexOf));
            if (lastIndexOf == -1) {
                hashMap.put("end", "");
                hashMap.put("content", str.substring(indexOf + 2));
            } else if (indexOf == lastIndexOf) {
                hashMap.put("head", str.substring(0, indexOf));
                hashMap.put("content", str.substring(indexOf + 2));
                hashMap.put("end", "");
            } else {
                hashMap.put("end", str.substring(lastIndexOf + 2));
                hashMap.put("content", str.substring(indexOf + 2, lastIndexOf));
            }
        } else {
            hashMap.put("head", "");
            hashMap.put("end", "");
            hashMap.put("content", str);
        }
        this.c = hashMap;
        this.g.setText((CharSequence) this.c.get("content"));
        this.g.setPadding(0, 15, 0, 15);
        LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(-1, -2);
        layoutParams3.weight = 1.0f;
        linearLayout.addView(this.g, layoutParams3);
        Button button = new Button(this);
        button.setText("发送分享");
        button.setOnClickListener(this);
        linearLayout.addView(button, new LinearLayout.LayoutParams(-1, -1));
        setContentView(scrollView, new RelativeLayout.LayoutParams(-1, -1));
    }
}
