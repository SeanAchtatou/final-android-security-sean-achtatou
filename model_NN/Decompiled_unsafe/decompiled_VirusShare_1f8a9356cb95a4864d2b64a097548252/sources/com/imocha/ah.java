package com.imocha;

import android.text.Editable;
import android.text.Selection;
import android.view.View;
import android.widget.ExpandableListView;
import java.util.ArrayList;

final class ah implements ExpandableListView.OnChildClickListener {
    private /* synthetic */ v a;

    ah(v vVar) {
        this.a = vVar;
    }

    public final boolean onChildClick(ExpandableListView expandableListView, View view, int i, int i2, long j) {
        String trim = this.a.a.d.getText().toString().trim();
        if (trim.length() > 0 && !trim.endsWith(",") && !trim.endsWith("，")) {
            trim = String.valueOf(trim) + ",";
        }
        this.a.a.d.setText(String.valueOf(trim) + ((String) ((ArrayList) ((e) this.a.a.k.get(this.a.a.j.get(i))).a.get(i2)).get(1)));
        Editable text = this.a.a.d.getText();
        Selection.setSelection(text, text.length());
        this.a.cancel();
        return false;
    }
}
