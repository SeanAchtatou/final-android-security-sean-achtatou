package com.tencent.mobwin.core.a;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

class i implements LocationListener {
    private final /* synthetic */ LocationManager a;

    i(LocationManager locationManager) {
        this.a = locationManager;
    }

    public void onLocationChanged(Location location) {
        f.C = location;
        f.D = System.currentTimeMillis();
        this.a.removeUpdates(this);
    }

    public void onProviderDisabled(String str) {
    }

    public void onProviderEnabled(String str) {
    }

    public void onStatusChanged(String str, int i, Bundle bundle) {
    }
}
