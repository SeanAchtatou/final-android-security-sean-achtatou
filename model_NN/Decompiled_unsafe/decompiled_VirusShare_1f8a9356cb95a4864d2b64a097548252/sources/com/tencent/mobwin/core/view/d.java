package com.tencent.mobwin.core.view;

import android.graphics.Bitmap;
import com.android.mmreader1361.mmabout;
import java.io.ByteArrayInputStream;
import java.io.InputStream;

public class d implements Runnable {
    private static final int O = 4096;
    public static final int a = 0;
    public static final int b = 1;
    public static final int c = 2;
    public static final int d = -1;
    private int A;
    private int B;
    private int C;
    private Bitmap D;
    private Bitmap E;
    private c F = null;
    private boolean G = false;
    private byte[] H = new byte[256];
    private int I = 0;
    private int J = 0;
    private int K = 0;
    private boolean L = false;
    private int M = 0;
    private int N;
    private short[] P;
    private byte[] Q;
    private byte[] R;
    private byte[] S;
    private c T;
    private int U;
    private a V = null;
    private byte[] W = null;
    public int e;
    public int f;
    private InputStream g;
    private int h;
    private boolean i;
    private int j;
    private int k = 1;
    private int[] l;
    private int[] m;
    private int[] n;
    private int o;
    private int p;
    private int q;
    private int r;
    private boolean s;
    private boolean t;
    private int u;
    private int v;
    private int w;
    private int x;
    private int y;
    private int z;

    public d(InputStream inputStream, a aVar) {
        this.g = inputStream;
        this.V = aVar;
    }

    public d(byte[] bArr, a aVar) {
        this.W = bArr;
        this.V = aVar;
    }

    private void A() {
        do {
            r();
            if (this.I <= 0) {
                return;
            }
        } while (!o());
    }

    private int[] d(int i2) {
        int i3;
        int i4 = i2 * 3;
        int[] iArr = null;
        byte[] bArr = new byte[i4];
        try {
            i3 = this.g.read(bArr);
        } catch (Exception e2) {
            e2.printStackTrace();
            i3 = 0;
        }
        if (i3 < i4) {
            this.h = 1;
        } else {
            iArr = new int[256];
            int i5 = 0;
            for (int i6 = 0; i6 < i2; i6++) {
                int i7 = i5 + 1;
                int i8 = i7 + 1;
                iArr[i6] = ((bArr[i5] & 255) << 16) | -16777216 | ((bArr[i7] & 255) << 8) | (bArr[i8] & 255);
                i5 = i8 + 1;
            }
        }
        return iArr;
    }

    private void k() {
        int i2;
        int i3;
        int i4;
        int i5 = 0;
        int[] iArr = new int[(this.e * this.f)];
        if (this.K > 0) {
            if (this.K == 3) {
                int i6 = this.U - 2;
                if (i6 > 0) {
                    this.E = b(i6 - 1);
                } else {
                    this.E = null;
                }
            }
            if (this.E != null) {
                this.E.getPixels(iArr, 0, this.e, 0, 0, this.e, this.f);
                if (this.K == 2) {
                    int i7 = !this.L ? this.q : 0;
                    for (int i8 = 0; i8 < this.C; i8++) {
                        int i9 = ((this.A + i8) * this.e) + this.z;
                        int i10 = this.B + i9;
                        while (i9 < i10) {
                            iArr[i9] = i7;
                            i9++;
                        }
                    }
                }
            }
        }
        int i11 = 8;
        int i12 = 1;
        int i13 = 0;
        while (i13 < this.y) {
            if (this.t) {
                if (i5 >= this.y) {
                    i12++;
                    switch (i12) {
                        case 2:
                            i5 = 4;
                            break;
                        case 3:
                            i5 = 2;
                            i11 = 4;
                            break;
                        case 4:
                            i5 = 1;
                            i11 = 2;
                            break;
                    }
                }
                i2 = i12;
                i3 = i11;
                i4 = i5 + i11;
            } else {
                i2 = i12;
                i3 = i11;
                i4 = i5;
                i5 = i13;
            }
            int i14 = i5 + this.w;
            if (i14 < this.f) {
                int i15 = i14 * this.e;
                int i16 = this.v + i15;
                int i17 = this.x + i16;
                int i18 = this.e + i15 < i17 ? i15 + this.e : i17;
                int i19 = i16;
                int i20 = this.x * i13;
                while (i19 < i18) {
                    int i21 = i20 + 1;
                    int i22 = this.n[this.S[i20] & 255];
                    if (i22 != 0) {
                        iArr[i19] = i22;
                    }
                    i19++;
                    i20 = i21;
                }
            }
            i13++;
            i5 = i4;
            i11 = i3;
            i12 = i2;
        }
        this.D = Bitmap.createBitmap(iArr, this.e, this.f, Bitmap.Config.ARGB_4444);
    }

    private int l() {
        this.g = new ByteArrayInputStream(this.W);
        this.W = null;
        return m();
    }

    private int m() {
        p();
        if (this.g != null) {
            u();
            if (!o()) {
                s();
                if (this.U < 0) {
                    this.h = 1;
                    this.V.a(false, -1);
                } else {
                    this.h = -1;
                    this.V.a(true, -1);
                }
            }
            try {
                this.g.close();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        } else {
            this.h = 2;
            this.V.a(false, -1);
        }
        return this.h;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r9v12, types: [int] */
    /* JADX WARN: Type inference failed for: r16v7, types: [int] */
    /* JADX WARN: Type inference failed for: r17v6, types: [int] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void n() {
        /*
            r24 = this;
            r2 = -1
            r0 = r24
            int r0 = r0.x
            r3 = r0
            r0 = r24
            int r0 = r0.y
            r4 = r0
            int r3 = r3 * r4
            r0 = r24
            byte[] r0 = r0.S
            r4 = r0
            if (r4 == 0) goto L_0x001b
            r0 = r24
            byte[] r0 = r0.S
            r4 = r0
            int r4 = r4.length
            if (r4 >= r3) goto L_0x0022
        L_0x001b:
            byte[] r4 = new byte[r3]
            r0 = r4
            r1 = r24
            r1.S = r0
        L_0x0022:
            r0 = r24
            short[] r0 = r0.P
            r4 = r0
            if (r4 != 0) goto L_0x0032
            r4 = 4096(0x1000, float:5.74E-42)
            short[] r4 = new short[r4]
            r0 = r4
            r1 = r24
            r1.P = r0
        L_0x0032:
            r0 = r24
            byte[] r0 = r0.Q
            r4 = r0
            if (r4 != 0) goto L_0x0042
            r4 = 4096(0x1000, float:5.74E-42)
            byte[] r4 = new byte[r4]
            r0 = r4
            r1 = r24
            r1.Q = r0
        L_0x0042:
            r0 = r24
            byte[] r0 = r0.R
            r4 = r0
            if (r4 != 0) goto L_0x0052
            r4 = 4097(0x1001, float:5.741E-42)
            byte[] r4 = new byte[r4]
            r0 = r4
            r1 = r24
            r1.R = r0
        L_0x0052:
            int r4 = r24.q()
            r5 = 1
            int r5 = r5 << r4
            int r6 = r5 + 1
            int r7 = r5 + 2
            int r8 = r4 + 1
            r9 = 1
            int r9 = r9 << r8
            r10 = 1
            int r9 = r9 - r10
            r10 = 0
        L_0x0063:
            if (r10 < r5) goto L_0x007b
            r10 = 0
            r11 = 0
            r12 = r11
            r13 = r10
            r14 = r10
            r15 = r2
            r16 = r8
            r17 = r9
            r18 = r7
            r11 = r10
            r7 = r10
            r8 = r10
            r9 = r10
        L_0x0075:
            if (r12 < r3) goto L_0x008e
        L_0x0077:
            r2 = r7
        L_0x0078:
            if (r2 < r3) goto L_0x0197
            return
        L_0x007b:
            r0 = r24
            short[] r0 = r0.P
            r11 = r0
            r12 = 0
            r11[r10] = r12
            r0 = r24
            byte[] r0 = r0.Q
            r11 = r0
            byte r12 = (byte) r10
            r11[r10] = r12
            int r10 = r10 + 1
            goto L_0x0063
        L_0x008e:
            if (r9 != 0) goto L_0x015e
            r0 = r14
            r1 = r16
            if (r0 >= r1) goto L_0x00bc
            if (r13 != 0) goto L_0x00a3
            int r8 = r24.r()
            if (r8 <= 0) goto L_0x0077
            r13 = 0
            r23 = r13
            r13 = r8
            r8 = r23
        L_0x00a3:
            r0 = r24
            byte[] r0 = r0.H
            r19 = r0
            byte r19 = r19[r8]
            r0 = r19
            r0 = r0 & 255(0xff, float:3.57E-43)
            r19 = r0
            int r19 = r19 << r14
            int r11 = r11 + r19
            int r14 = r14 + 8
            int r8 = r8 + 1
            int r13 = r13 + -1
            goto L_0x0075
        L_0x00bc:
            r19 = r11 & r17
            int r11 = r11 >> r16
            int r14 = r14 - r16
            r0 = r19
            r1 = r18
            if (r0 > r1) goto L_0x0077
            r0 = r19
            r1 = r6
            if (r0 == r1) goto L_0x0077
            r0 = r19
            r1 = r5
            if (r0 != r1) goto L_0x00e6
            int r15 = r4 + 1
            r16 = 1
            int r16 = r16 << r15
            r17 = 1
            int r16 = r16 - r17
            int r17 = r5 + 2
            r18 = r17
            r17 = r16
            r16 = r15
            r15 = r2
            goto L_0x0075
        L_0x00e6:
            if (r15 != r2) goto L_0x0100
            r0 = r24
            byte[] r0 = r0.R
            r10 = r0
            int r15 = r9 + 1
            r0 = r24
            byte[] r0 = r0.Q
            r20 = r0
            byte r20 = r20[r19]
            r10[r9] = r20
            r9 = r15
            r10 = r19
            r15 = r19
            goto L_0x0075
        L_0x0100:
            r0 = r19
            r1 = r18
            if (r0 != r1) goto L_0x01a8
            r0 = r24
            byte[] r0 = r0.R
            r20 = r0
            int r21 = r9 + 1
            byte r10 = (byte) r10
            r20[r9] = r10
            r9 = r21
            r10 = r15
        L_0x0114:
            if (r10 > r5) goto L_0x0179
            r0 = r24
            byte[] r0 = r0.Q
            r20 = r0
            byte r10 = r20[r10]
            r10 = r10 & 255(0xff, float:3.57E-43)
            r20 = 4096(0x1000, float:5.74E-42)
            r0 = r18
            r1 = r20
            if (r0 >= r1) goto L_0x0077
            r0 = r24
            byte[] r0 = r0.R
            r20 = r0
            int r21 = r9 + 1
            r0 = r10
            byte r0 = (byte) r0
            r22 = r0
            r20[r9] = r22
            r0 = r24
            short[] r0 = r0.P
            r9 = r0
            short r15 = (short) r15
            r9[r18] = r15
            r0 = r24
            byte[] r0 = r0.Q
            r9 = r0
            byte r15 = (byte) r10
            r9[r18] = r15
            int r9 = r18 + 1
            r15 = r9 & r17
            if (r15 != 0) goto L_0x01a3
            r15 = 4096(0x1000, float:5.74E-42)
            if (r9 >= r15) goto L_0x01a3
            int r15 = r16 + 1
            int r16 = r17 + r9
        L_0x0154:
            r17 = r16
            r18 = r9
            r16 = r15
            r9 = r21
            r15 = r19
        L_0x015e:
            int r9 = r9 + -1
            r0 = r24
            byte[] r0 = r0.S
            r19 = r0
            int r20 = r7 + 1
            r0 = r24
            byte[] r0 = r0.R
            r21 = r0
            byte r21 = r21[r9]
            r19[r7] = r21
            int r7 = r12 + 1
            r12 = r7
            r7 = r20
            goto L_0x0075
        L_0x0179:
            r0 = r24
            byte[] r0 = r0.R
            r20 = r0
            int r21 = r9 + 1
            r0 = r24
            byte[] r0 = r0.Q
            r22 = r0
            byte r22 = r22[r10]
            r20[r9] = r22
            r0 = r24
            short[] r0 = r0.P
            r9 = r0
            short r9 = r9[r10]
            r10 = r9
            r9 = r21
            goto L_0x0114
        L_0x0197:
            r0 = r24
            byte[] r0 = r0.S
            r4 = r0
            r5 = 0
            r4[r2] = r5
            int r2 = r2 + 1
            goto L_0x0078
        L_0x01a3:
            r15 = r16
            r16 = r17
            goto L_0x0154
        L_0x01a8:
            r10 = r19
            goto L_0x0114
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.mobwin.core.view.d.n():void");
    }

    private boolean o() {
        return this.h != 0;
    }

    private void p() {
        this.h = 0;
        this.U = 0;
        this.T = null;
        this.l = null;
        this.m = null;
    }

    private int q() {
        try {
            return this.g.read();
        } catch (Exception e2) {
            this.h = 1;
            return 0;
        }
    }

    private int r() {
        this.I = q();
        int i2 = 0;
        if (this.I > 0) {
            while (i2 < this.I) {
                try {
                    int read = this.g.read(this.H, i2, this.I - i2);
                    if (read == -1) {
                        break;
                    }
                    i2 += read;
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
            if (i2 < this.I) {
                this.h = 1;
            }
        }
        return i2;
    }

    private void s() {
        boolean z2 = false;
        while (!z2 && !o()) {
            switch (q()) {
                case 0:
                    break;
                case mmabout.MORE_ID:
                    switch (q()) {
                        case 249:
                            t();
                            continue;
                        case 255:
                            r();
                            String str = "";
                            for (int i2 = 0; i2 < 11; i2++) {
                                str = String.valueOf(str) + ((char) this.H[i2]);
                            }
                            if (!str.equals("NETSCAPE2.0")) {
                                A();
                                break;
                            } else {
                                x();
                                continue;
                            }
                        default:
                            A();
                            continue;
                    }
                case 44:
                    v();
                    break;
                case 59:
                    z2 = true;
                    break;
                default:
                    this.h = 1;
                    break;
            }
        }
    }

    private void t() {
        q();
        int q2 = q();
        this.J = (q2 & 28) >> 2;
        if (this.J == 0) {
            this.J = 1;
        }
        this.L = (q2 & 1) != 0;
        this.M = y() * 10;
        this.N = q();
        q();
    }

    private void u() {
        String str = "";
        for (int i2 = 0; i2 < 6; i2++) {
            str = String.valueOf(str) + ((char) q());
        }
        if (!str.startsWith("GIF")) {
            this.h = 1;
            return;
        }
        w();
        if (this.i && !o()) {
            this.l = d(this.j);
            this.p = this.l[this.o];
        }
    }

    private void v() {
        int i2;
        this.v = y();
        this.w = y();
        this.x = y();
        this.y = y();
        int q2 = q();
        this.s = (q2 & 128) != 0;
        this.t = (q2 & 64) != 0;
        this.u = 2 << (q2 & 7);
        if (this.s) {
            this.m = d(this.u);
            this.n = this.m;
        } else {
            this.n = this.l;
            if (this.o == this.N) {
                this.p = 0;
            }
        }
        if (this.L) {
            i2 = this.n[this.N];
            this.n[this.N] = 0;
        } else {
            i2 = 0;
        }
        if (this.n == null) {
            this.h = 1;
        }
        if (!o()) {
            n();
            A();
            if (!o()) {
                this.U++;
                this.D = Bitmap.createBitmap(this.e, this.f, Bitmap.Config.ARGB_4444);
                k();
                if (this.T == null) {
                    this.T = new c(this.D, this.M);
                    this.F = this.T;
                } else {
                    c cVar = this.T;
                    while (cVar.c != null) {
                        cVar = cVar.c;
                    }
                    cVar.c = new c(this.D, this.M);
                }
                if (this.L) {
                    this.n[this.N] = i2;
                }
                z();
                this.V.a(true, this.U);
            }
        }
    }

    private void w() {
        this.e = y();
        this.f = y();
        int q2 = q();
        this.i = (q2 & 128) != 0;
        this.j = 2 << (q2 & 7);
        this.o = q();
        this.r = q();
    }

    private void x() {
        do {
            r();
            if (this.H[0] == 1) {
                this.k = (this.H[1] & 255) | ((this.H[2] & 255) << 8);
            }
            if (this.I <= 0) {
                return;
            }
        } while (!o());
    }

    private int y() {
        return q() | (q() << 8);
    }

    private void z() {
        this.K = this.J;
        this.z = this.v;
        this.A = this.w;
        this.B = this.x;
        this.C = this.y;
        this.E = this.D;
        this.q = this.p;
        this.J = 0;
        this.L = false;
        this.M = 0;
        this.m = null;
    }

    public int a(int i2) {
        c c2;
        this.M = -1;
        if (i2 >= 0 && i2 < this.U && (c2 = c(i2)) != null) {
            this.M = c2.b;
        }
        return this.M;
    }

    public void a() {
        c cVar = this.T;
        while (cVar != null) {
            cVar.a = null;
            this.T = this.T.c;
            cVar = this.T;
        }
        if (this.g != null) {
            try {
                this.g.close();
            } catch (Exception e2) {
            }
            this.g = null;
        }
        this.W = null;
    }

    public int b() {
        return this.h;
    }

    public Bitmap b(int i2) {
        c c2 = c(i2);
        if (c2 == null) {
            return null;
        }
        return c2.a;
    }

    public c c(int i2) {
        c cVar = this.T;
        int i3 = 0;
        while (cVar != null) {
            if (i3 == i2) {
                return cVar;
            }
            cVar = cVar.c;
            i3++;
        }
        return null;
    }

    public boolean c() {
        return this.h == -1;
    }

    public int[] d() {
        c cVar = this.T;
        int[] iArr = new int[this.U];
        c cVar2 = cVar;
        int i2 = 0;
        while (cVar2 != null && i2 < this.U) {
            iArr[i2] = cVar2.b;
            cVar2 = cVar2.c;
            i2++;
        }
        return iArr;
    }

    public int e() {
        return this.U;
    }

    public Bitmap f() {
        return b(0);
    }

    public int g() {
        return this.k;
    }

    public c h() {
        return this.F;
    }

    public void i() {
        this.F = this.T;
    }

    public c j() {
        if (!this.G) {
            this.G = true;
            return this.T;
        }
        if (this.h != 0) {
            this.F = this.F.c;
            if (this.F == null) {
                this.F = this.T;
            }
        } else if (this.F.c != null) {
            this.F = this.F.c;
        }
        return this.F;
    }

    public void run() {
        if (this.g != null) {
            m();
        } else if (this.W != null) {
            l();
        }
    }
}
