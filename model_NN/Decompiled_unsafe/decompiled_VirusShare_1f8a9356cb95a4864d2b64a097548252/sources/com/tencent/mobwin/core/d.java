package com.tencent.mobwin.core;

import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationSet;

class d implements View.OnTouchListener {
    final /* synthetic */ w a;

    d(w wVar) {
        this.a = wVar;
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        if (action == 0) {
            AnimationSet animationSet = new AnimationSet(false);
            AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.6f);
            alphaAnimation.setDuration(100);
            alphaAnimation.setFillAfter(true);
            animationSet.addAnimation(alphaAnimation);
            animationSet.addAnimation(m.a(view, null));
            animationSet.setFillAfter(true);
            view.startAnimation(animationSet);
        } else if (action == 1) {
            AnimationSet animationSet2 = new AnimationSet(true);
            AlphaAnimation alphaAnimation2 = new AlphaAnimation(0.6f, 1.0f);
            alphaAnimation2.setDuration(100);
            animationSet2.setFillAfter(true);
            animationSet2.addAnimation(alphaAnimation2);
            view.startAnimation(animationSet2);
        }
        return false;
    }
}
