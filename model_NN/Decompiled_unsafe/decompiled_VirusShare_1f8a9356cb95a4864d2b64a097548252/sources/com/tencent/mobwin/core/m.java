package com.tencent.mobwin.core;

import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.CycleInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import com.tencent.mobwin.core.view.b;
import java.util.Random;

public class m {
    public static final int a = 500;
    public static final int b = 1000;

    public static Animation a() {
        TranslateAnimation translateAnimation = new TranslateAnimation(2, 1.0f, 2, 0.0f, 2, 0.0f, 2, 0.0f);
        translateAnimation.setDuration(500);
        translateAnimation.setInterpolator(new AccelerateInterpolator());
        return translateAnimation;
    }

    public static Animation a(View view, Animation.AnimationListener animationListener) {
        TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 10.0f, 0.0f, 0.0f);
        translateAnimation.setAnimationListener(animationListener);
        translateAnimation.setDuration(300);
        translateAnimation.setInterpolator(new CycleInterpolator(2.0f));
        translateAnimation.setFillAfter(false);
        return translateAnimation;
    }

    public static Animation[] a(View view, Animation.AnimationListener animationListener, y yVar) {
        Animation[] animationArr = new Animation[2];
        if (yVar != y.SwitchText) {
            switch (new Random().nextInt(4)) {
                case 0:
                    animationArr[0] = c(view, animationListener, yVar);
                    animationArr[1] = b(view, null, yVar);
                    break;
                case 1:
                    animationArr[0] = d(view, animationListener, yVar);
                    animationArr[1] = e(view, null, yVar);
                    break;
                case 2:
                    animationArr[0] = f(view, animationListener, yVar);
                    animationArr[1] = g(view, null, yVar);
                    break;
                case 3:
                    animationArr[0] = i(view, animationListener, yVar);
                    animationArr[1] = h(view, null, yVar);
                    break;
                default:
                    animationArr[0] = c(view, animationListener, yVar);
                    animationArr[1] = b(view, null, yVar);
                    break;
            }
        } else {
            switch (new Random().nextInt(2)) {
                case 0:
                    animationArr[0] = c(view, animationListener, yVar);
                    animationArr[1] = b(view, null, yVar);
                    break;
                case 1:
                    animationArr[0] = d(view, animationListener, yVar);
                    animationArr[1] = e(view, null, yVar);
                    break;
                case 2:
                    animationArr[0] = f(view, animationListener, yVar);
                    animationArr[1] = g(view, null, yVar);
                    break;
                default:
                    animationArr[0] = c(view, animationListener, yVar);
                    animationArr[1] = b(view, null, yVar);
                    break;
            }
        }
        return animationArr;
    }

    public static Animation b() {
        TranslateAnimation translateAnimation = new TranslateAnimation(2, 0.0f, 2, -1.0f, 2, 0.0f, 2, 0.0f);
        translateAnimation.setDuration(500);
        translateAnimation.setInterpolator(new AccelerateInterpolator());
        return translateAnimation;
    }

    private static Animation b(View view, Animation.AnimationListener animationListener, y yVar) {
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
        alphaAnimation.setInterpolator(new DecelerateInterpolator());
        alphaAnimation.setFillEnabled(false);
        alphaAnimation.setFillAfter(false);
        if (yVar == y.SwitchAd) {
            alphaAnimation.setDuration(500);
        } else {
            alphaAnimation.setDuration(1000);
        }
        alphaAnimation.setAnimationListener(animationListener);
        return alphaAnimation;
    }

    public static Animation c() {
        TranslateAnimation translateAnimation = new TranslateAnimation(2, -1.0f, 2, 0.0f, 2, 0.0f, 2, 0.0f);
        translateAnimation.setDuration(500);
        translateAnimation.setInterpolator(new AccelerateInterpolator());
        return translateAnimation;
    }

    private static Animation c(View view, Animation.AnimationListener animationListener, y yVar) {
        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        alphaAnimation.setInterpolator(new AccelerateInterpolator());
        alphaAnimation.setFillEnabled(false);
        alphaAnimation.setFillAfter(false);
        if (yVar == y.SwitchAd) {
            alphaAnimation.setDuration(500);
        } else {
            alphaAnimation.setDuration(1000);
        }
        alphaAnimation.setAnimationListener(animationListener);
        return alphaAnimation;
    }

    public static Animation d() {
        TranslateAnimation translateAnimation = new TranslateAnimation(2, 0.0f, 2, 1.0f, 2, 0.0f, 2, 0.0f);
        translateAnimation.setDuration(500);
        translateAnimation.setInterpolator(new AccelerateInterpolator());
        return translateAnimation;
    }

    private static Animation d(View view, Animation.AnimationListener animationListener, y yVar) {
        TranslateAnimation translateAnimation;
        if (yVar == y.SwitchAd) {
            translateAnimation = new TranslateAnimation(0.0f, 0.0f, 0.0f, (float) (-view.getHeight()));
            translateAnimation.setDuration(500);
        } else {
            translateAnimation = new TranslateAnimation(0.0f, 0.0f, 0.0f, -50.0f);
            translateAnimation.setDuration(1000);
        }
        translateAnimation.setFillAfter(false);
        translateAnimation.setFillEnabled(false);
        translateAnimation.setAnimationListener(animationListener);
        return translateAnimation;
    }

    private static Animation e(View view, Animation.AnimationListener animationListener, y yVar) {
        TranslateAnimation translateAnimation;
        if (yVar == y.SwitchAd) {
            translateAnimation = new TranslateAnimation(0.0f, 0.0f, (float) view.getHeight(), 0.0f);
            translateAnimation.setDuration(500);
        } else {
            translateAnimation = new TranslateAnimation(0.0f, 0.0f, 50.0f, 0.0f);
            translateAnimation.setDuration(1000);
        }
        translateAnimation.setFillAfter(false);
        translateAnimation.setFillEnabled(false);
        translateAnimation.setAnimationListener(animationListener);
        return translateAnimation;
    }

    private static Animation f(View view, Animation.AnimationListener animationListener, y yVar) {
        b bVar = new b(0.0f, 90.0f, ((float) com.tencent.mobwin.utils.b.d(view.getContext())) / 2.0f, ((float) com.tencent.mobwin.utils.b.e(view.getContext())) / 2.0f, 0.0f, true);
        if (yVar == y.SwitchAd) {
            bVar.setDuration(500);
        } else {
            bVar.setDuration(1000);
        }
        bVar.setFillAfter(false);
        bVar.setInterpolator(new AccelerateInterpolator());
        bVar.setAnimationListener(animationListener);
        return bVar;
    }

    private static Animation g(View view, Animation.AnimationListener animationListener, y yVar) {
        b bVar = new b(90.0f, 0.0f, ((float) com.tencent.mobwin.utils.b.d(view.getContext())) / 2.0f, ((float) com.tencent.mobwin.utils.b.e(view.getContext())) / 2.0f, 0.0f, false);
        if (yVar == y.SwitchAd) {
            bVar.setDuration(500);
        } else {
            bVar.setDuration(1000);
        }
        bVar.setFillAfter(false);
        bVar.setInterpolator(new AccelerateInterpolator());
        bVar.setAnimationListener(animationListener);
        return bVar;
    }

    private static Animation h(View view, Animation.AnimationListener animationListener, y yVar) {
        ScaleAnimation scaleAnimation = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, ((float) com.tencent.mobwin.utils.b.d(view.getContext())) / 2.0f, ((float) com.tencent.mobwin.utils.b.e(view.getContext())) / 2.0f);
        if (yVar == y.SwitchAd) {
            scaleAnimation.setDuration(500);
        } else {
            scaleAnimation.setDuration(1000);
        }
        scaleAnimation.setFillAfter(false);
        scaleAnimation.setInterpolator(new AccelerateInterpolator());
        scaleAnimation.setAnimationListener(animationListener);
        return scaleAnimation;
    }

    private static Animation i(View view, Animation.AnimationListener animationListener, y yVar) {
        ScaleAnimation scaleAnimation = new ScaleAnimation(1.0f, 0.0f, 1.0f, 0.0f, ((float) com.tencent.mobwin.utils.b.d(view.getContext())) / 2.0f, ((float) com.tencent.mobwin.utils.b.e(view.getContext())) / 2.0f);
        if (yVar == y.SwitchAd) {
            scaleAnimation.setDuration(500);
        } else {
            scaleAnimation.setDuration(1000);
        }
        scaleAnimation.setFillAfter(false);
        scaleAnimation.setInterpolator(new AccelerateInterpolator());
        scaleAnimation.setAnimationListener(animationListener);
        return scaleAnimation;
    }
}
