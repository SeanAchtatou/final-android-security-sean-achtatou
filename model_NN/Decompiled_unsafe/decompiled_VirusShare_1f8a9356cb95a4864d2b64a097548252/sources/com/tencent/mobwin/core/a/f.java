package com.tencent.mobwin.core.a;

import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import com.tencent.mobwin.core.o;
import com.tencent.mobwin.utils.b;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public class f {
    public static String A = "-";
    private static final String B = "TargetData";
    /* access modifiers changed from: private */
    public static Location C = null;
    /* access modifiers changed from: private */
    public static long D = 0;
    private static final long E = 900000;
    private static LocationListener F = new g();
    /* access modifiers changed from: private */
    public static LocationManager G = null;
    public static final String a = "sim";
    public static final String b = "tel";
    public static final String c = "tel_support";
    public static final String d = "wifi_support";
    public static final String e = "lwp_support";
    public static final String f = "imei";
    public static final String g = "meid";
    public static final String h = "imsi";
    public static final String i = "mcc";
    public static final String j = "mnc";
    public static final String k = "cid";
    public static final String l = "lac";
    public static final String m = "brand";
    public static final String n = "manufacturer";
    public static final String o = "model";
    public static final String p = "sdk";
    public static final String q = "release";
    public static final String r = "mac";
    public static final String s = "lang";
    public static final String t = "FEATURE_WIFI";
    public static final String u = "FEATURE_LIVE_WALLPAPER";
    public static final String v = "FEATURE_TELEPHONY";
    public static int w = -1;
    public static int x = -1;
    public static int y = -1;
    public static int z = -1;

    private static Object a(Context context, PackageManager packageManager, String str) {
        try {
            Class<?> cls = packageManager.getClass();
            Method method = cls.getMethod("hasSystemFeature", String.class);
            Field field = cls.getField(str);
            method.setAccessible(true);
            if (field != null) {
                Object obj = field.get(String.class);
                o.b("IORY", "localfield:" + field.getName() + "---" + field.toString() + "---" + obj.toString());
                if (obj != null) {
                    return method.invoke(packageManager, obj.toString());
                }
            }
            return null;
        } catch (Exception e2) {
            e2.printStackTrace();
            o.b("reflect", "can't get reflect method");
            return null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:8:0x000f A[Catch:{ SocketException -> 0x003a }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String a() {
        /*
            java.lang.String r0 = "-"
            java.util.Enumeration r1 = java.net.NetworkInterface.getNetworkInterfaces()     // Catch:{ SocketException -> 0x0031 }
            r2 = r0
        L_0x0007:
            boolean r0 = r1.hasMoreElements()     // Catch:{ SocketException -> 0x003a }
            if (r0 != 0) goto L_0x000f
            r0 = r2
        L_0x000e:
            return r0
        L_0x000f:
            java.lang.Object r0 = r1.nextElement()     // Catch:{ SocketException -> 0x003a }
            java.net.NetworkInterface r0 = (java.net.NetworkInterface) r0     // Catch:{ SocketException -> 0x003a }
            java.util.Enumeration r3 = r0.getInetAddresses()     // Catch:{ SocketException -> 0x003a }
        L_0x0019:
            boolean r0 = r3.hasMoreElements()     // Catch:{ SocketException -> 0x003a }
            if (r0 == 0) goto L_0x0007
            java.lang.Object r0 = r3.nextElement()     // Catch:{ SocketException -> 0x003a }
            java.net.InetAddress r0 = (java.net.InetAddress) r0     // Catch:{ SocketException -> 0x003a }
            boolean r4 = r0.isLoopbackAddress()     // Catch:{ SocketException -> 0x003a }
            if (r4 != 0) goto L_0x0019
            java.lang.String r0 = r0.getHostAddress()     // Catch:{ SocketException -> 0x003a }
            r2 = r0
            goto L_0x0019
        L_0x0031:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x0035:
            r0.printStackTrace()
            r0 = r1
            goto L_0x000e
        L_0x003a:
            r0 = move-exception
            r1 = r2
            goto L_0x0035
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.mobwin.core.a.f.a():java.lang.String");
    }

    public static Map a(Context context) {
        String str;
        String str2;
        String str3;
        HashMap hashMap = new HashMap();
        if (context.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE") != -1) {
            try {
                TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
                int phoneType = telephonyManager.getPhoneType();
                if (phoneType == 1) {
                    String deviceId = telephonyManager.getDeviceId();
                    if (deviceId == null) {
                        deviceId = "-";
                    }
                    hashMap.put(f, deviceId);
                    hashMap.put(g, "-");
                } else if (phoneType == 2) {
                    String deviceId2 = telephonyManager.getDeviceId();
                    if (deviceId2 == null) {
                        deviceId2 = "-";
                    }
                    hashMap.put(g, deviceId2);
                    hashMap.put(f, "-");
                } else {
                    hashMap.put(g, "-");
                    hashMap.put(f, "-");
                }
                Object a2 = a(context, context.getPackageManager(), t);
                if (a2 != null) {
                    o.b("IORY", "WIFI支持：" + a2.toString());
                    hashMap.put(d, a2.toString());
                } else {
                    hashMap.put(d, "-");
                }
                Object a3 = a(context, context.getPackageManager(), u);
                if (a3 != null) {
                    o.b("IORY", "LWP支持：" + a3.toString());
                    hashMap.put(e, a3.toString());
                } else {
                    hashMap.put(e, "-");
                }
                Object a4 = a(context, context.getPackageManager(), v);
                if (a4 != null) {
                    hashMap.put(c, a4.toString());
                    o.b("IORY", "TELEPHONE支持：" + a4.toString());
                } else {
                    hashMap.put(c, "-");
                }
                String line1Number = telephonyManager.getLine1Number();
                if (line1Number == null) {
                    line1Number = "-1";
                }
                hashMap.put(b, line1Number);
                String simSerialNumber = telephonyManager.getSimSerialNumber();
                if (simSerialNumber == null) {
                    simSerialNumber = "-";
                }
                hashMap.put(a, simSerialNumber);
                String subscriberId = telephonyManager.getSubscriberId();
                if (subscriberId == null) {
                    subscriberId = "-";
                }
                hashMap.put(h, subscriberId);
                if (telephonyManager.getSimState() == 5) {
                    String simOperator = telephonyManager.getSimOperator();
                    str = simOperator.substring(0, 3);
                    str2 = simOperator.substring(3);
                } else {
                    str = "";
                    str2 = "";
                }
                hashMap.put(i, str);
                hashMap.put(j, str2);
                String str4 = "";
                GsmCellLocation gsmCellLocation = (GsmCellLocation) telephonyManager.getCellLocation();
                if (gsmCellLocation != null) {
                    str4 = Integer.toString(gsmCellLocation.getCid());
                    str3 = Integer.toString(gsmCellLocation.getLac());
                } else {
                    str3 = "";
                }
                hashMap.put(k, str4);
                hashMap.put(l, str3);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        if (context.checkCallingOrSelfPermission("android.permission.ACCESS_WIFI_STATE") != -1) {
            try {
                String macAddress = ((WifiManager) context.getSystemService("wifi")).getConnectionInfo().getMacAddress();
                o.a(B, "mac:" + macAddress);
                hashMap.put(r, macAddress);
            } catch (Exception e3) {
                e3.printStackTrace();
                hashMap.put(r, "-");
            }
        }
        hashMap.put(s, context.getResources().getConfiguration().locale.getDisplayLanguage());
        return hashMap;
    }

    public static String b(Context context) {
        try {
            return b.a(context.getPackageManager().getPackageInfo(context.getPackageName(), 64).signatures[0].toCharsString());
        } catch (PackageManager.NameNotFoundException e2) {
            e2.printStackTrace();
            return "-";
        }
    }

    public static Map b() {
        HashMap hashMap = new HashMap();
        hashMap.put(m, Build.BRAND);
        hashMap.put(n, Build.MANUFACTURER);
        hashMap.put(o, Build.MODEL);
        hashMap.put(p, Build.VERSION.SDK);
        hashMap.put(q, Build.VERSION.RELEASE);
        if (Build.BRAND.toLowerCase().indexOf("generic") == -1 || Build.MODEL.toLowerCase().indexOf(p) != -1) {
        }
        return hashMap;
    }

    public static String c(Context context) {
        try {
            String str = context.getPackageManager().getPackageInfo(context.getPackageName(), 1).versionName;
            return str == null ? "-" : str;
        } catch (PackageManager.NameNotFoundException e2) {
            e2.printStackTrace();
            return "-";
        }
    }

    public static Location d(Context context) {
        String str;
        LocationManager locationManager;
        boolean z2;
        if (context != null) {
            try {
                if (C == null || System.currentTimeMillis() > D + E) {
                    synchronized (context) {
                        if (C == null || System.currentTimeMillis() > D + E) {
                            D = System.currentTimeMillis();
                            if (context.checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION") == 0) {
                                locationManager = (LocationManager) context.getSystemService("location");
                                if (locationManager != null) {
                                    Criteria criteria = new Criteria();
                                    criteria.setAccuracy(2);
                                    criteria.setCostAllowed(false);
                                    str = locationManager.getBestProvider(criteria, true);
                                    z2 = true;
                                } else {
                                    str = null;
                                    z2 = true;
                                }
                            } else {
                                str = null;
                                locationManager = null;
                                z2 = false;
                            }
                            if (str == null && context.checkCallingOrSelfPermission("android.permission.ACCESS_FINE_LOCATION") == 0) {
                                locationManager = (LocationManager) context.getSystemService("location");
                                if (locationManager != null) {
                                    Criteria criteria2 = new Criteria();
                                    criteria2.setAccuracy(1);
                                    criteria2.setCostAllowed(false);
                                    str = locationManager.getBestProvider(criteria2, true);
                                    z2 = true;
                                } else {
                                    z2 = true;
                                }
                            }
                            if (z2 && str != null) {
                                locationManager.requestLocationUpdates(str, 0, 0.0f, new i(locationManager), context.getMainLooper());
                            }
                        }
                    }
                }
            } catch (Exception e2) {
                e2.printStackTrace();
                return null;
            }
        }
        return C;
    }

    public static void e(Context context) {
        try {
            C = d(context);
            if (C != null) {
                w = (int) (C.getLongitude() * 1000000.0d);
                x = (int) (C.getLatitude() * 1000000.0d);
                y = (int) (C.getAltitude() * 1000000.0d);
                String.valueOf(C.getLatitude()) + "," + C.getLongitude();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public static void f(Context context) {
        NetworkInfo activeNetworkInfo;
        if (context.checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE") != -1 && (activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo()) != null) {
            if (activeNetworkInfo.getType() == 1) {
                z = 1;
                return;
            }
            String extraInfo = activeNetworkInfo.getExtraInfo();
            if (extraInfo != null) {
                String lowerCase = extraInfo.toLowerCase();
                if (lowerCase.equals("cmnet")) {
                    z = 6;
                } else if (lowerCase.equals("cmwap")) {
                    z = 7;
                } else if (lowerCase.equals("3gnet")) {
                    z = 2;
                } else if (lowerCase.equals("3gwap")) {
                    z = 3;
                } else if (lowerCase.equals("uninet")) {
                    z = 4;
                } else if (lowerCase.equals("uniwap")) {
                    z = 5;
                } else if (lowerCase.equals("ctnet")) {
                    z = 8;
                } else if (lowerCase.equals("ctwap")) {
                    z = 9;
                } else {
                    z = 0;
                }
            }
        }
    }
}
