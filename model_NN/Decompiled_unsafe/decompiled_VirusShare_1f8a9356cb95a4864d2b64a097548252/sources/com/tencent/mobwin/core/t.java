package com.tencent.mobwin.core;

import MobWin.ADClickInfo;
import MobWin.BannerInfo;
import MobWin.ReqActivateApp;
import MobWin.ReqAppLaunch;
import MobWin.ReqClickAD;
import MobWin.ReqGetAD;
import MobWin.ReqReportAdPlayInfo;
import MobWin.ResActivateApp;
import MobWin.ResAppLaunch;
import MobWin.ResClickAD;
import MobWin.ResGetAD;
import MobWin.ResReportAdPlayInfo;
import MobWin.SettingVersions;
import MobWin.cnst.FUNCTION_ACTIVATE_APP;
import MobWin.cnst.FUNCTION_APP_LAUNCH;
import MobWin.cnst.FUNCTION_CLICK;
import MobWin.cnst.FUNCTION_GETAD;
import MobWin.cnst.FUNCTION_REPORT_AD_PLAY_INFO;
import MobWin.cnst.MOBILEWIN_SERVANT;
import MobWin.cnst.PROTOCOL_ENCODING;
import MobWin.cnst.REQUEST_KEY;
import MobWin.cnst.RESPONSE_KEY;
import android.content.Context;
import android.os.Handler;
import com.qq.jce.wup.UniPacket;
import com.qq.taf.jce.JceStruct;
import com.tencent.mobwin.core.a.c;
import com.tencent.mobwin.core.a.e;
import com.tencent.mobwin.core.b.a;
import com.tencent.mobwin.core.b.b;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

final class t {
    static ResGetAD A = new ResGetAD();
    static ResReportAdPlayInfo B = new ResReportAdPlayInfo();
    static ResClickAD C = new ResClickAD();
    public static final int D = 1;
    public static final int E = 2;
    public static final int F = 3;
    public static final int G = 4;
    public static final int H = 5;
    public static final int I = 6;
    private static final String K = "LinkData";
    private static int M = 1;
    public static final int a = 1;
    public static final int b = 2;
    public static final int c = 3;
    public static final int d = 4;
    public static final int e = 5;
    public static final int f = 6;
    public static final int g = 7;
    public static final int h = 8;
    public static final int i = 9;
    public static final int j = 10;
    public static final int k = 11;
    public static final int l = 12;
    public static final int m = 13;
    public static final int n = 14;
    public static final int o = 15;
    public static final int p = 16;
    public static final int q = 17;
    public static final int r = 18;
    public static final int s = 0;
    public static final int t = 1;
    public static final int u = 0;
    public static final int v = 1;
    public static final int w = 2;
    public static final int x = 3;
    static ResAppLaunch y = new ResAppLaunch();
    static ResActivateApp z = new ResActivateApp();
    a J = new s(this);
    private c L;

    private void a(Context context, UniPacket uniPacket) {
        if (this.L == null) {
            this.L = new c(context);
        }
        uniPacket.setEncodeName(PROTOCOL_ENCODING.value);
        int i2 = M;
        M = i2 + 1;
        uniPacket.setRequestId(i2);
        uniPacket.setServantName(MOBILEWIN_SERVANT.a);
    }

    /* access modifiers changed from: private */
    public static JceStruct b(byte[] bArr, int i2) {
        if (bArr == null) {
            return null;
        }
        UniPacket uniPacket = new UniPacket();
        uniPacket.setEncodeName(PROTOCOL_ENCODING.value);
        uniPacket.decode(bArr);
        switch (i2) {
            case 1:
                return (JceStruct) uniPacket.getByClass(RESPONSE_KEY.value, y);
            case 2:
                return (JceStruct) uniPacket.getByClass(RESPONSE_KEY.value, A);
            case 3:
                return (JceStruct) uniPacket.getByClass(RESPONSE_KEY.value, B);
            case 4:
                return (JceStruct) uniPacket.getByClass(RESPONSE_KEY.value, C);
            case 5:
            default:
                return null;
            case 6:
                return (JceStruct) uniPacket.getByClass(RESPONSE_KEY.value, z);
        }
    }

    /* access modifiers changed from: private */
    public static byte[] b(InputStream inputStream) {
        if (inputStream == null) {
            return null;
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        while (true) {
            try {
                int read = inputStream.read();
                if (read == -1) {
                    break;
                }
                byteArrayOutputStream.write(read);
            } catch (Exception e2) {
                e2.printStackTrace();
                try {
                    byteArrayOutputStream.close();
                } catch (IOException e3) {
                    e3.printStackTrace();
                }
                return null;
            } catch (Throwable th) {
                try {
                    byteArrayOutputStream.close();
                } catch (IOException e4) {
                    e4.printStackTrace();
                }
                throw th;
            }
        }
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        try {
            byteArrayOutputStream.close();
        } catch (IOException e5) {
            e5.printStackTrace();
        }
        return byteArray;
    }

    public ResGetAD a(Context context, String str, BannerInfo bannerInfo, SettingVersions settingVersions, Handler handler) {
        UniPacket uniPacket = new UniPacket();
        a(context, uniPacket);
        uniPacket.setFuncName(FUNCTION_GETAD.a);
        ReqGetAD reqGetAD = new ReqGetAD();
        reqGetAD.a(this.L.a(context));
        reqGetAD.a(this.L.b(context));
        reqGetAD.a(settingVersions);
        reqGetAD.a(2);
        reqGetAD.b(1);
        reqGetAD.a(e.c(context));
        reqGetAD.b(e.d(context));
        reqGetAD.a(this.L.c(context));
        reqGetAD.i = bannerInfo;
        uniPacket.put(REQUEST_KEY.value, reqGetAD);
        o.a("IORY", "开始编码时间：" + System.currentTimeMillis());
        byte[] encode = uniPacket.encode();
        o.a("IORY", "编码结束时间：" + System.currentTimeMillis());
        b.a(new b(String.valueOf(str) + FUNCTION_GETAD.a, true, 2, encode, this.J, handler));
        return null;
    }

    public void a() {
        z.recyle();
        y.recyle();
        A.recyle();
        B.recyle();
        C.recyle();
    }

    public void a(Context context, String str, int i2, int i3, String str2, BannerInfo bannerInfo, String str3, Handler handler) {
        if (!w.f()) {
            UniPacket uniPacket = new UniPacket();
            a(context, uniPacket);
            uniPacket.setFuncName(FUNCTION_CLICK.a);
            ReqClickAD reqClickAD = new ReqClickAD();
            reqClickAD.a(this.L.a(context));
            reqClickAD.a(this.L.b(context));
            ADClickInfo aDClickInfo = new ADClickInfo();
            aDClickInfo.setAd_id(i2);
            aDClickInfo.setVri_key(str2);
            aDClickInfo.setActivated(i3);
            reqClickAD.a(str3);
            reqClickAD.a(aDClickInfo);
            reqClickAD.e = bannerInfo;
            reqClickAD.a(this.L.c(context));
            o.a(K, "getAdClickData 2:" + reqClickAD.toString());
            uniPacket.put(REQUEST_KEY.value, reqClickAD);
            b.a(new b(String.valueOf(str) + FUNCTION_CLICK.a, true, 4, uniPacket.encode(), this.J, handler));
        }
    }

    public void a(Context context, String str, long j2, long j3, Handler handler) {
        UniPacket uniPacket = new UniPacket();
        a(context, uniPacket);
        uniPacket.setFuncName(FUNCTION_APP_LAUNCH.a);
        ReqAppLaunch reqAppLaunch = new ReqAppLaunch();
        reqAppLaunch.setUser_info(this.L.a(context));
        reqAppLaunch.setApp_info(this.L.b(context));
        reqAppLaunch.settingVerions = new SettingVersions(j2, j3);
        uniPacket.put(REQUEST_KEY.value, reqAppLaunch);
        b.a(new b(String.valueOf(str) + FUNCTION_APP_LAUNCH.a, true, 1, uniPacket.encode(), this.J, handler));
    }

    public void a(Context context, String str, Handler handler) {
        UniPacket uniPacket = new UniPacket();
        a(context, uniPacket);
        uniPacket.setFuncName(FUNCTION_ACTIVATE_APP.a);
        ReqActivateApp reqActivateApp = new ReqActivateApp();
        reqActivateApp.setUser_info(this.L.a(context));
        reqActivateApp.setApp_info(this.L.b(context));
        o.a(K, "getAdActivateData 2:" + reqActivateApp.toString());
        uniPacket.put(REQUEST_KEY.value, reqActivateApp);
        b.a(new b(String.valueOf(str) + FUNCTION_ACTIVATE_APP.a, true, 6, uniPacket.encode(), this.J, handler));
    }

    public void a(Context context, String str, Handler handler, int i2) {
        b.a(new b(str, false, 5, null, new r(this, context, i2, handler), handler));
    }

    public void a(Context context, String str, com.tencent.mobwin.core.a.b bVar, int i2, BannerInfo bannerInfo, Handler handler) {
        if (!w.f()) {
            if (bVar.a != 0 || i2 != 0) {
                UniPacket uniPacket = new UniPacket();
                a(context, uniPacket);
                uniPacket.setFuncName(FUNCTION_REPORT_AD_PLAY_INFO.a);
                ReqReportAdPlayInfo reqReportAdPlayInfo = new ReqReportAdPlayInfo();
                reqReportAdPlayInfo.a(this.L.a(context));
                reqReportAdPlayInfo.a(this.L.b(context));
                reqReportAdPlayInfo.d = i2;
                reqReportAdPlayInfo.e = (int) (bVar.d / 1000);
                reqReportAdPlayInfo.g = bVar.e;
                reqReportAdPlayInfo.c = bVar.a;
                reqReportAdPlayInfo.j = bannerInfo;
                reqReportAdPlayInfo.h = bVar.f;
                reqReportAdPlayInfo.k = bVar.h;
                reqReportAdPlayInfo.l = bVar.i;
                reqReportAdPlayInfo.i = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(System.currentTimeMillis()));
                uniPacket.put(REQUEST_KEY.value, reqReportAdPlayInfo);
                b.a(new b(String.valueOf(str) + FUNCTION_REPORT_AD_PLAY_INFO.a, true, 3, uniPacket.encode(), this.J, handler));
            }
        }
    }
}
