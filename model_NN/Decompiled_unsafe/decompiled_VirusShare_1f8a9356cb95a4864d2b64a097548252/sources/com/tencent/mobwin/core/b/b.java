package com.tencent.mobwin.core.b;

import android.os.Handler;
import com.tencent.mobwin.core.a.f;
import com.tencent.mobwin.core.o;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import org.apache.http.HttpHost;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;

public class b {
    public static final int a = 30000;
    public static final int b = 30000;
    public static final int c = 4096;
    public static final int d = 3;
    private static final String k = "HttpTask";
    public boolean e = false;
    public String f = "";
    public int g = 0;
    public Handler h = null;
    HttpPost i = null;
    HttpGet j = null;
    private a l = null;
    private int m = 0;
    private boolean n = true;
    private byte[] o = null;
    private Map p = new HashMap();
    private URI q = null;

    public b(String str, boolean z, int i2, byte[] bArr, a aVar, Handler handler) {
        this.f = str;
        this.n = z;
        this.g = i2;
        this.o = bArr;
        this.l = aVar;
        this.h = handler;
        try {
            this.q = new URI(this.f);
        } catch (URISyntaxException e2) {
            e2.printStackTrace();
        }
    }

    public static void a(b bVar) {
        if (bVar != null) {
            new c(bVar).start();
        }
    }

    private boolean a(URI uri) {
        return uri.getScheme() != null && uri.getScheme().startsWith("http");
    }

    private HttpClient c() {
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(basicHttpParams, 30000);
        HttpConnectionParams.setSoTimeout(basicHttpParams, 30000);
        HttpConnectionParams.setSocketBufferSize(basicHttpParams, c);
        HttpClientParams.setRedirecting(basicHttpParams, true);
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient(basicHttpParams);
        o.a("Network", "Network" + f.z);
        if (f.z == 7 || f.z == 3 || f.z == 5) {
            defaultHttpClient.getParams().setParameter("http.route.default-proxy", new HttpHost("10.0.0.172", 80, "http"));
        }
        return defaultHttpClient;
    }

    public void a() {
        this.e = true;
    }

    public void a(String str, String str2) {
        if (str != null && str.length() > 0) {
            this.p.put(str, str2);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:121:0x0217  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x012a  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x0134  */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x014b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void b() {
        /*
            r9 = this;
            r7 = 2
            r6 = 0
            boolean r0 = r9.e
            if (r0 == 0) goto L_0x0007
        L_0x0006:
            return
        L_0x0007:
            java.net.URI r0 = r9.q
            boolean r0 = r9.a(r0)
            if (r0 != 0) goto L_0x021d
            com.tencent.mobwin.core.b.a r0 = r9.l
            r1 = 520224(0x7f020, float:7.28989E-40)
            r0.a(r1, r9)
            goto L_0x0006
        L_0x0018:
            org.apache.http.client.HttpClient r3 = r9.c()     // Catch:{ z -> 0x020b, Throwable -> 0x0204, all -> 0x01f6 }
            boolean r0 = r9.e     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
            if (r0 == 0) goto L_0x002c
            if (r3 == 0) goto L_0x0029
            org.apache.http.conn.ClientConnectionManager r0 = r3.getConnectionManager()
            r0.shutdown()
        L_0x0029:
            if (r2 == 0) goto L_0x0006
            goto L_0x0006
        L_0x002c:
            boolean r0 = r9.n     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
            if (r0 == 0) goto L_0x00b7
            org.apache.http.client.methods.HttpPost r0 = new org.apache.http.client.methods.HttpPost     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
            r0.<init>()     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
            r9.i = r0     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
            org.apache.http.client.methods.HttpPost r0 = r9.i     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
            java.net.URI r1 = r9.q     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
            r0.setURI(r1)     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
            byte[] r0 = r9.o     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
            if (r0 == 0) goto L_0x0053
            byte[] r0 = r9.o     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
            int r0 = r0.length     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
            if (r0 <= 0) goto L_0x0053
            org.apache.http.client.methods.HttpPost r0 = r9.i     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
            org.apache.http.entity.ByteArrayEntity r1 = new org.apache.http.entity.ByteArrayEntity     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
            byte[] r4 = r9.o     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
            r1.<init>(r4)     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
            r0.setEntity(r1)     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
        L_0x0053:
            java.util.Map r0 = r9.p     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
            int r0 = r0.size()     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
            if (r0 <= 0) goto L_0x006b
            java.util.Map r0 = r9.p     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
            java.util.Set r0 = r0.keySet()     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
            java.util.Iterator r4 = r0.iterator()     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
        L_0x0065:
            boolean r0 = r4.hasNext()     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
            if (r0 != 0) goto L_0x00f9
        L_0x006b:
            org.apache.http.protocol.BasicHttpContext r0 = new org.apache.http.protocol.BasicHttpContext     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
            r0.<init>()     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
            boolean r1 = r9.n     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
            if (r1 == 0) goto L_0x0155
            java.lang.String r1 = "HttpTask"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
            java.lang.String r5 = "httpSend "
            r4.<init>(r5)     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
            java.lang.Thread r5 = java.lang.Thread.currentThread()     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
            java.lang.String r5 = r5.getName()     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
            java.lang.String r5 = "URL:"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
            org.apache.http.client.methods.HttpPost r5 = r9.i     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
            java.net.URI r5 = r5.getURI()     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
            java.lang.String r4 = r4.toString()     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
            com.tencent.mobwin.core.o.b(r1, r4)     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
            org.apache.http.client.methods.HttpPost r1 = r9.i     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
            org.apache.http.HttpResponse r0 = r3.execute(r1, r0)     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
        L_0x00a6:
            boolean r1 = r9.e     // Catch:{ z -> 0x01be, Throwable -> 0x01fd, all -> 0x01ec }
            if (r1 == 0) goto L_0x0189
            if (r3 == 0) goto L_0x00b3
            org.apache.http.conn.ClientConnectionManager r1 = r3.getConnectionManager()
            r1.shutdown()
        L_0x00b3:
            if (r0 == 0) goto L_0x0006
            goto L_0x0006
        L_0x00b7:
            org.apache.http.client.methods.HttpGet r0 = new org.apache.http.client.methods.HttpGet     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
            r0.<init>()     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
            r9.j = r0     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
            org.apache.http.client.methods.HttpGet r0 = r9.j     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
            java.net.URI r1 = r9.q     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
            r0.setURI(r1)     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
            goto L_0x0053
        L_0x00c6:
            r0 = move-exception
            r1 = r2
            r2 = r3
        L_0x00c9:
            r0.printStackTrace()     // Catch:{ all -> 0x01f3 }
            com.tencent.mobwin.core.b.a r0 = r9.l     // Catch:{ all -> 0x01f3 }
            if (r0 == 0) goto L_0x00dc
            int r0 = r9.m     // Catch:{ all -> 0x01f3 }
            if (r0 < r7) goto L_0x00dc
            com.tencent.mobwin.core.b.a r0 = r9.l     // Catch:{ all -> 0x01f3 }
            r3 = 520240(0x7f030, float:7.29012E-40)
            r0.a(r3, r9)     // Catch:{ all -> 0x01f3 }
        L_0x00dc:
            if (r2 == 0) goto L_0x021a
            org.apache.http.conn.ClientConnectionManager r0 = r2.getConnectionManager()
            r0.shutdown()
            r0 = r6
        L_0x00e6:
            if (r1 == 0) goto L_0x0212
            r1 = r0
            r0 = r6
        L_0x00ea:
            int r2 = r9.m
            int r2 = r2 + 1
            r9.m = r2
            r2 = r0
            r0 = r1
        L_0x00f2:
            int r1 = r9.m
            r3 = 3
            if (r1 < r3) goto L_0x0018
            goto L_0x0006
        L_0x00f9:
            java.lang.Object r0 = r4.next()     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
            boolean r1 = r9.n     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
            if (r1 == 0) goto L_0x0137
            org.apache.http.client.methods.HttpPost r5 = r9.i     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
            java.util.Map r1 = r9.p     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
            java.lang.Object r1 = r1.get(r0)     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
            r5.addHeader(r0, r1)     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
            goto L_0x0065
        L_0x0112:
            r0 = move-exception
            r1 = r2
            r2 = r3
        L_0x0115:
            r0.printStackTrace()     // Catch:{ all -> 0x01f3 }
            com.tencent.mobwin.core.b.a r0 = r9.l     // Catch:{ all -> 0x01f3 }
            if (r0 == 0) goto L_0x0128
            int r0 = r9.m     // Catch:{ all -> 0x01f3 }
            if (r0 < r7) goto L_0x0128
            com.tencent.mobwin.core.b.a r0 = r9.l     // Catch:{ all -> 0x01f3 }
            r3 = 520208(0x7f010, float:7.28967E-40)
            r0.a(r3, r9)     // Catch:{ all -> 0x01f3 }
        L_0x0128:
            if (r2 == 0) goto L_0x0217
            org.apache.http.conn.ClientConnectionManager r0 = r2.getConnectionManager()
            r0.shutdown()
            r0 = r6
        L_0x0132:
            if (r1 == 0) goto L_0x0212
            r1 = r0
            r0 = r6
            goto L_0x00ea
        L_0x0137:
            org.apache.http.client.methods.HttpGet r5 = r9.j     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
            java.util.Map r1 = r9.p     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
            java.lang.Object r1 = r1.get(r0)     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
            r5.addHeader(r0, r1)     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
            goto L_0x0065
        L_0x0146:
            r0 = move-exception
            r1 = r2
            r2 = r3
        L_0x0149:
            if (r2 == 0) goto L_0x0152
            org.apache.http.conn.ClientConnectionManager r2 = r2.getConnectionManager()
            r2.shutdown()
        L_0x0152:
            if (r1 == 0) goto L_0x0154
        L_0x0154:
            throw r0
        L_0x0155:
            java.lang.String r1 = "HttpTask"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
            java.lang.String r5 = "httpSend "
            r4.<init>(r5)     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
            java.lang.Thread r5 = java.lang.Thread.currentThread()     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
            java.lang.String r5 = r5.getName()     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
            java.lang.String r5 = "URL:"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
            org.apache.http.client.methods.HttpGet r5 = r9.j     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
            java.net.URI r5 = r5.getURI()     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
            java.lang.String r4 = r4.toString()     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
            com.tencent.mobwin.core.o.b(r1, r4)     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
            org.apache.http.client.methods.HttpGet r1 = r9.j     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
            org.apache.http.HttpResponse r0 = r3.execute(r1, r0)     // Catch:{ z -> 0x00c6, Throwable -> 0x0112, all -> 0x0146 }
            goto L_0x00a6
        L_0x0189:
            org.apache.http.StatusLine r1 = r0.getStatusLine()     // Catch:{ z -> 0x01be, Throwable -> 0x01fd, all -> 0x01ec }
            int r1 = r1.getStatusCode()     // Catch:{ z -> 0x01be, Throwable -> 0x01fd, all -> 0x01ec }
            r2 = 200(0xc8, float:2.8E-43)
            if (r1 == r2) goto L_0x01c5
            r2 = 206(0xce, float:2.89E-43)
            if (r1 == r2) goto L_0x01c5
            boolean r2 = r9.n     // Catch:{ z -> 0x01be, Throwable -> 0x01fd, all -> 0x01ec }
            if (r2 == 0) goto L_0x01b8
            org.apache.http.client.methods.HttpPost r2 = r9.i     // Catch:{ z -> 0x01be, Throwable -> 0x01fd, all -> 0x01ec }
            r2.abort()     // Catch:{ z -> 0x01be, Throwable -> 0x01fd, all -> 0x01ec }
        L_0x01a2:
            com.tencent.mobwin.core.b.a r2 = r9.l     // Catch:{ z -> 0x01be, Throwable -> 0x01fd, all -> 0x01ec }
            if (r2 == 0) goto L_0x01ab
            com.tencent.mobwin.core.b.a r2 = r9.l     // Catch:{ z -> 0x01be, Throwable -> 0x01fd, all -> 0x01ec }
            r2.a(r1, r9)     // Catch:{ z -> 0x01be, Throwable -> 0x01fd, all -> 0x01ec }
        L_0x01ab:
            if (r3 == 0) goto L_0x01b4
            org.apache.http.conn.ClientConnectionManager r1 = r3.getConnectionManager()
            r1.shutdown()
        L_0x01b4:
            if (r0 == 0) goto L_0x0006
            goto L_0x0006
        L_0x01b8:
            org.apache.http.client.methods.HttpGet r2 = r9.j     // Catch:{ z -> 0x01be, Throwable -> 0x01fd, all -> 0x01ec }
            r2.abort()     // Catch:{ z -> 0x01be, Throwable -> 0x01fd, all -> 0x01ec }
            goto L_0x01a2
        L_0x01be:
            r1 = move-exception
            r2 = r3
            r8 = r0
            r0 = r1
            r1 = r8
            goto L_0x00c9
        L_0x01c5:
            boolean r1 = r9.e     // Catch:{ z -> 0x01be, Throwable -> 0x01fd, all -> 0x01ec }
            if (r1 == 0) goto L_0x01d6
            if (r3 == 0) goto L_0x01d2
            org.apache.http.conn.ClientConnectionManager r1 = r3.getConnectionManager()
            r1.shutdown()
        L_0x01d2:
            if (r0 == 0) goto L_0x0006
            goto L_0x0006
        L_0x01d6:
            com.tencent.mobwin.core.b.a r1 = r9.l     // Catch:{ z -> 0x01be, Throwable -> 0x01fd, all -> 0x01ec }
            if (r1 == 0) goto L_0x01df
            com.tencent.mobwin.core.b.a r1 = r9.l     // Catch:{ z -> 0x01be, Throwable -> 0x01fd, all -> 0x01ec }
            r1.a(r0, r9)     // Catch:{ z -> 0x01be, Throwable -> 0x01fd, all -> 0x01ec }
        L_0x01df:
            if (r3 == 0) goto L_0x01e8
            org.apache.http.conn.ClientConnectionManager r1 = r3.getConnectionManager()
            r1.shutdown()
        L_0x01e8:
            if (r0 == 0) goto L_0x0006
            goto L_0x0006
        L_0x01ec:
            r1 = move-exception
            r2 = r3
            r8 = r0
            r0 = r1
            r1 = r8
            goto L_0x0149
        L_0x01f3:
            r0 = move-exception
            goto L_0x0149
        L_0x01f6:
            r1 = move-exception
            r8 = r1
            r1 = r2
            r2 = r0
            r0 = r8
            goto L_0x0149
        L_0x01fd:
            r1 = move-exception
            r2 = r3
            r8 = r0
            r0 = r1
            r1 = r8
            goto L_0x0115
        L_0x0204:
            r1 = move-exception
            r8 = r1
            r1 = r2
            r2 = r0
            r0 = r8
            goto L_0x0115
        L_0x020b:
            r1 = move-exception
            r8 = r1
            r1 = r2
            r2 = r0
            r0 = r8
            goto L_0x00c9
        L_0x0212:
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x00ea
        L_0x0217:
            r0 = r2
            goto L_0x0132
        L_0x021a:
            r0 = r2
            goto L_0x00e6
        L_0x021d:
            r2 = r6
            r0 = r6
            goto L_0x00f2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.mobwin.core.b.b.b():void");
    }
}
