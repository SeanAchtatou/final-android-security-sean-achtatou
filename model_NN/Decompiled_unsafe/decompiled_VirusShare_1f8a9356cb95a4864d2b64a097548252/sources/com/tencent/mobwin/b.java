package com.tencent.mobwin;

import android.content.Context;
import android.os.Message;
import android.view.MotionEvent;
import android.webkit.WebView;
import com.tencent.mobwin.core.A;
import com.tencent.mobwin.core.m;
import com.tencent.mobwin.core.o;
import com.tencent.mobwin.core.view.f;
import com.tencent.mobwin.utils.a;

class b extends WebView {
    Context a;
    float b;
    long c;
    /* access modifiers changed from: package-private */
    public final /* synthetic */ MobinWINBrowserActivity d;
    private float e;
    private float f;
    private boolean g = false;
    private f h;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public b(MobinWINBrowserActivity mobinWINBrowserActivity, Context context) {
        super(context);
        this.d = mobinWINBrowserActivity;
        this.a = context;
        this.h = new f(mobinWINBrowserActivity);
        this.h.setImageBitmap(A.a().a("process_bar.png", 0));
        this.h.setVisibility(8);
    }

    private boolean a(MotionEvent motionEvent) {
        return this.g || ((double) Math.abs(motionEvent.getX() - this.e)) > 10.0d || ((double) Math.abs(motionEvent.getY() - this.f)) > 10.0d;
    }

    public f a() {
        return this.h;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        boolean onTouchEvent = super.onTouchEvent(motionEvent);
        if (isClickable()) {
            switch (motionEvent.getAction()) {
                case 0:
                    this.e = motionEvent.getX();
                    this.f = motionEvent.getY();
                    this.b = motionEvent.getX();
                    this.c = motionEvent.getEventTime();
                    this.g = false;
                    if (!MobinWINBrowserActivity.e && MobinWINBrowserActivity.f) {
                        Message obtain = Message.obtain();
                        obtain.what = 11;
                        this.d.H.sendMessage(obtain);
                        MobinWINBrowserActivity.e = true;
                        MobinWINBrowserActivity.f = false;
                        o.b("CloseMessage", "SHOW");
                        break;
                    }
                case 1:
                    float x = motionEvent.getX();
                    long eventTime = motionEvent.getEventTime();
                    float abs = Math.abs(this.b - x);
                    long j = eventTime - this.c;
                    o.a("Touch Event:", "Distance: " + abs + "px Time: " + j + "ms");
                    if (((double) Math.abs(motionEvent.getY() - this.f)) < 100.0d) {
                        if (this.b < x && j < 400 && abs > 50.0f) {
                            this.d.B.setInAnimation(m.c());
                            this.d.B.setOutAnimation(m.d());
                            this.d.B.showPrevious();
                            if (this.d.F == 0) {
                                this.d.F = this.d.G - 1;
                            } else {
                                MobinWINBrowserActivity mobinWINBrowserActivity = this.d;
                                mobinWINBrowserActivity.F = mobinWINBrowserActivity.F - 1;
                            }
                            this.d.C.setImageBitmap(a.a(75, 12, this.d.G, this.d.F, this.d));
                            a().refreshDrawableState();
                        }
                        if (this.b > x && j < 400 && abs > 50.0f) {
                            this.d.B.setInAnimation(m.a());
                            this.d.B.setOutAnimation(m.b());
                            this.d.B.showNext();
                            if (this.d.F == this.d.G - 1) {
                                this.d.F = 0;
                            } else {
                                MobinWINBrowserActivity mobinWINBrowserActivity2 = this.d;
                                mobinWINBrowserActivity2.F = mobinWINBrowserActivity2.F + 1;
                            }
                            this.d.C.setImageBitmap(a.a(75, 12, this.d.G, this.d.F, this.d));
                            a().refreshDrawableState();
                        }
                    }
                    if (MobinWINBrowserActivity.e && !MobinWINBrowserActivity.f) {
                        MobinWINBrowserActivity.e = false;
                        new l(this).start();
                        break;
                    }
                case 2:
                    this.g = a(motionEvent);
                    break;
            }
        }
        return onTouchEvent || isClickable();
    }
}
