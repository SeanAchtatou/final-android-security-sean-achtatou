package com.tencent.mobwin;

import android.graphics.Bitmap;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.tencent.mobwin.core.o;

class d extends WebViewClient {
    final /* synthetic */ MobinWINBrowserActivity a;

    private d(MobinWINBrowserActivity mobinWINBrowserActivity) {
        this.a = mobinWINBrowserActivity;
    }

    /* synthetic */ d(MobinWINBrowserActivity mobinWINBrowserActivity, d dVar) {
        this(mobinWINBrowserActivity);
    }

    public void onPageFinished(WebView webView, String str) {
        super.onPageFinished(webView, str);
        ((b) webView).a().setVisibility(8);
        o.a("SDK2", "onPageFinished:" + str);
    }

    public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        super.onPageStarted(webView, str, bitmap);
        ((b) webView).a().setVisibility(0);
        o.a("SDK2", "onPageStarted:" + str);
    }
}
