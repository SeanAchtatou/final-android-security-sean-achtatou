package com.tencent.mobwin;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.DownloadListener;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ViewFlipper;
import com.tencent.mobwin.core.A;
import com.tencent.mobwin.core.o;
import com.tencent.mobwin.core.view.f;
import com.tencent.mobwin.core.view.g;
import com.tencent.mobwin.utils.a;
import com.tencent.mobwin.utils.b;
import java.util.ArrayList;

public class MobinWINBrowserActivity extends Activity {
    public static final String a = "com.android.browser";
    public static final String b = "com.android.browser.BrowserActivity";
    static boolean e = false;
    static boolean f = true;
    public static final int g = 11;
    public static final int h = 12;
    public static final int i = 17;
    private ArrayList A = null;
    /* access modifiers changed from: private */
    public ViewFlipper B;
    /* access modifiers changed from: private */
    public ImageView C;
    /* access modifiers changed from: private */
    public g D;
    private ArrayList E;
    /* access modifiers changed from: private */
    public int F = 0;
    /* access modifiers changed from: private */
    public int G = 0;
    /* access modifiers changed from: private */
    public m H = null;
    private View.OnClickListener I = new j(this);
    private View.OnClickListener J = new k(this);
    private View.OnClickListener K = new f(this);
    private View.OnClickListener L = new g(this);
    private View.OnClickListener M = new h(this);
    private DownloadListener N = new i(this);
    final int c = 65;
    RelativeLayout d;
    /* access modifiers changed from: private */
    public WebView j;
    private final int k = 0;
    private final int l = 1;
    private final int m = 2;
    private final int n = 21;
    private String[] o = {"back.png", "back_pressed.png", "back_disable.png"};
    private String[] p = {"forward.png", "forward_pressed.png", "forward_disable.png"};
    private String[] q = {"jumpout.png", "jumpout_pressed.png"};
    private String[] r = {"quit.png", "quit_pressed.png"};
    private String[] s = {"refresh.png", "refresh_pressed.png"};
    /* access modifiers changed from: private */
    public g t;
    /* access modifiers changed from: private */
    public g u;
    private g v;
    private g w;
    private g x;
    /* access modifiers changed from: private */
    public f y;
    /* access modifiers changed from: private */
    public String z = null;

    private void a() {
        this.j = new WebView(this);
        this.j.setWebViewClient(new e(this, null));
        this.j.setWebChromeClient(new a(this, null));
        this.j.setDownloadListener(this.N);
        this.j.getSettings().setJavaScriptEnabled(true);
        WebSettings settings = this.j.getSettings();
        settings.setBuiltInZoomControls(true);
        settings.setSupportZoom(true);
        settings.setJavaScriptEnabled(true);
    }

    public static void a(String str, Context context) {
        o.a("SDK", "download" + str);
        if (str != null && context != null) {
            try {
                if (context.getPackageManager().getPackageInfo("com.android.browser", 0) != null) {
                    Intent intent = new Intent();
                    intent.setAction("android.intent.action.VIEW");
                    intent.setData(Uri.parse(str));
                    intent.setClassName("com.android.browser", "com.android.browser.BrowserActivity");
                    context.startActivity(intent);
                    return;
                }
                c(str, context);
            } catch (Exception e2) {
                if (0 != 0) {
                    Intent intent2 = new Intent();
                    intent2.setAction("android.intent.action.VIEW");
                    intent2.setData(Uri.parse(str));
                    intent2.setClassName("com.android.browser", "com.android.browser.BrowserActivity");
                    context.startActivity(intent2);
                    return;
                }
                c(str, context);
            } catch (Throwable th) {
                if (0 != 0) {
                    Intent intent3 = new Intent();
                    intent3.setAction("android.intent.action.VIEW");
                    intent3.setData(Uri.parse(str));
                    intent3.setClassName("com.android.browser", "com.android.browser.BrowserActivity");
                    context.startActivity(intent3);
                } else {
                    c(str, context);
                }
                throw th;
            }
        }
    }

    private void b() {
        this.B = new ViewFlipper(this);
        this.E = new ArrayList();
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < this.G) {
                RelativeLayout relativeLayout = new RelativeLayout(this);
                RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, -1);
                b bVar = new b(this, this);
                bVar.setInitialScale(100);
                bVar.setWebViewClient(new d(this, null));
                bVar.getSettings().setJavaScriptEnabled(true);
                bVar.setBackgroundColor(-16777216);
                bVar.setWebChromeClient(new c(this, null));
                bVar.getSettings().setBuiltInZoomControls(true);
                bVar.getSettings().setSupportZoom(true);
                bVar.loadUrl((String) this.A.get(i3));
                relativeLayout.addView(bVar, layoutParams2);
                RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(b.a(65, this), b.a(65, this));
                layoutParams3.addRule(13);
                bVar.a().setVisibility(8);
                this.E.add(bVar);
                relativeLayout.addView(bVar.a(), layoutParams3);
                this.B.addView(relativeLayout, i3, layoutParams);
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }

    private void c() {
        this.d = new RelativeLayout(this);
        this.d.addView(this.B, new RelativeLayout.LayoutParams(-1, -1));
        Bitmap a2 = a.a(75, 12, this.G, this.F, this);
        this.C = new ImageView(this);
        this.C.setImageBitmap(a2);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(12);
        layoutParams.addRule(14);
        if (b.a(this) < b.b(this)) {
            layoutParams.setMargins(0, 0, 0, b.a(90, this));
        } else {
            layoutParams.setMargins(0, 0, 0, b.a(40, this));
        }
        this.d.addView(this.C, layoutParams);
        this.D = new g(this);
        this.D.b(A.a().a("close_album.png", 0), A.a().a("close_album_pressed.png", 0), null);
        this.D.setOnClickListener(this.M);
        this.D.setPadding(b.a(10, this), b.a(10, this), b.a(10, this), b.a(10, this));
        setContentView(this.d);
    }

    /* access modifiers changed from: private */
    public static void c(String str, Context context) {
        Intent createChooser;
        if (str != null && context != null && (createChooser = Intent.createChooser(new Intent("android.intent.action.VIEW", Uri.parse(str)), "请选择浏览器")) != null) {
            context.startActivity(createChooser);
        }
    }

    private void d() {
        LinearLayout linearLayout = new LinearLayout(this);
        new LinearLayout.LayoutParams(-1, -1);
        linearLayout.setBackgroundColor(-1);
        linearLayout.setOrientation(1);
        RelativeLayout relativeLayout = new RelativeLayout(this);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -1);
        layoutParams.weight = 1.0f;
        relativeLayout.setBackgroundColor(-10066330);
        linearLayout.addView(relativeLayout, layoutParams);
        relativeLayout.addView(this.j, new RelativeLayout.LayoutParams(-1, -1));
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(b.a(65, this), b.a(65, this));
        layoutParams2.addRule(13);
        this.y = new f(this);
        this.y.setImageBitmap(A.a().a("process_bar.png", 0));
        this.y.setVisibility(8);
        relativeLayout.addView(this.y, layoutParams2);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-1, b.a(12, this));
        LinearLayout linearLayout2 = new LinearLayout(this);
        Bitmap a2 = A.a().a("toolbar_header.png", 0);
        layoutParams3.addRule(12);
        layoutParams3.addRule(14);
        linearLayout2.setBackgroundDrawable(new BitmapDrawable(a2));
        relativeLayout.addView(linearLayout2, layoutParams3);
        LinearLayout.LayoutParams layoutParams4 = new LinearLayout.LayoutParams(-1, b.a(80, this));
        LinearLayout linearLayout3 = new LinearLayout(this);
        linearLayout3.setOrientation(0);
        linearLayout3.setGravity(16);
        linearLayout3.setBackgroundDrawable(new BitmapDrawable(A.a().a("toolbar_body.png", 0)));
        linearLayout.addView(linearLayout3, layoutParams4);
        LinearLayout.LayoutParams layoutParams5 = new LinearLayout.LayoutParams(-1, -1);
        layoutParams5.weight = 1.0f;
        this.t = new g(this);
        Bitmap a3 = A.a().a(this.o[1], 0);
        this.t.a(A.a().a(this.o[0], 0), a3, A.a().a(this.o[2], 0));
        this.t.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        this.t.setOnClickListener(this.I);
        linearLayout3.addView(this.t, layoutParams5);
        Bitmap a4 = A.a().a("divideline.png", 0);
        LinearLayout.LayoutParams layoutParams6 = new LinearLayout.LayoutParams(2, b.a(76, this));
        ImageView imageView = new ImageView(this);
        imageView.setBackgroundDrawable(new BitmapDrawable(a4));
        linearLayout3.addView(imageView, layoutParams6);
        LinearLayout.LayoutParams layoutParams7 = new LinearLayout.LayoutParams(-1, -1);
        layoutParams7.weight = 1.0f;
        this.u = new g(this);
        Bitmap a5 = A.a().a(this.p[1], 0);
        this.u.a(A.a().a(this.p[0], 0), a5, A.a().a(this.p[2], 0));
        this.u.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        this.u.setOnClickListener(this.J);
        linearLayout3.addView(this.u, layoutParams7);
        LinearLayout.LayoutParams layoutParams8 = new LinearLayout.LayoutParams(2, b.a(76, this));
        ImageView imageView2 = new ImageView(this);
        imageView2.setBackgroundDrawable(new BitmapDrawable(a4));
        linearLayout3.addView(imageView2, layoutParams8);
        LinearLayout.LayoutParams layoutParams9 = new LinearLayout.LayoutParams(-1, -1);
        layoutParams9.weight = 1.0f;
        this.v = new g(this);
        Bitmap a6 = A.a().a(this.s[1], 0);
        this.v.a(A.a().a(this.s[0], 0), a6, null);
        this.v.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        this.v.setOnClickListener(this.K);
        linearLayout3.addView(this.v, layoutParams9);
        LinearLayout.LayoutParams layoutParams10 = new LinearLayout.LayoutParams(2, b.a(76, this));
        ImageView imageView3 = new ImageView(this);
        imageView3.setBackgroundDrawable(new BitmapDrawable(a4));
        linearLayout3.addView(imageView3, layoutParams10);
        LinearLayout.LayoutParams layoutParams11 = new LinearLayout.LayoutParams(-1, -1);
        layoutParams11.weight = 1.0f;
        this.w = new g(this);
        Bitmap a7 = A.a().a(this.q[1], 0);
        this.w.a(A.a().a(this.q[0], 0), a7, null);
        this.w.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        this.w.setOnClickListener(this.L);
        linearLayout3.addView(this.w, layoutParams11);
        LinearLayout.LayoutParams layoutParams12 = new LinearLayout.LayoutParams(2, b.a(76, this));
        ImageView imageView4 = new ImageView(this);
        imageView4.setBackgroundDrawable(new BitmapDrawable(a4));
        linearLayout3.addView(imageView4, layoutParams12);
        LinearLayout.LayoutParams layoutParams13 = new LinearLayout.LayoutParams(-1, -1);
        layoutParams13.weight = 1.0f;
        this.x = new g(this);
        Bitmap a8 = A.a().a(this.r[1], 0);
        this.x.a(A.a().a(this.r[0], 0), a8, null);
        this.x.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        linearLayout3.addView(this.x, layoutParams13);
        this.x.setOnClickListener(this.M);
        setContentView(linearLayout);
    }

    private void e() {
        if (this.j != null) {
            this.j.stopLoading();
            this.j.clearCache(true);
            this.j.destroy();
            this.j = null;
        }
    }

    public void a(Bundle bundle) {
        if (bundle == null) {
            this.z = getIntent().getStringExtra("URL");
        } else {
            this.z = bundle.getString("URL");
        }
        if (this.z == null || this.j == null) {
            finish();
        } else {
            this.j.loadUrl(this.z);
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (this.H == null) {
            this.H = new m(this);
        }
        getWindow().setFlags(4, 4);
        getWindow().addFlags(512);
        A.b(this, this.H);
        String stringExtra = getIntent().getStringExtra("EFFECTIVE_TYPE");
        if (stringExtra == null || !stringExtra.equals("_ALBUM")) {
            a();
            d();
            a(bundle);
            return;
        }
        if (this.A == null) {
            this.A = getIntent().getStringArrayListExtra("URL");
        }
        if (this.A != null) {
            this.G = this.A.size();
        }
        b();
        c();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        e();
        this.t = null;
        this.u = null;
        this.v = null;
        this.w = null;
        this.x = null;
    }

    public boolean onKeyUp(int i2, KeyEvent keyEvent) {
        if (i2 != 4 || this.j == null || !this.j.canGoBack()) {
            return super.onKeyUp(i2, keyEvent);
        }
        this.j.goBack();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        bundle.putString("URL", this.z);
        super.onSaveInstanceState(bundle);
    }
}
