package com.tencent.lbsapi;

import android.content.Context;
import com.tencent.lbsapi.core.f;

public class QLBSService {
    private f a = null;

    public QLBSService(Context context, QLBSNotification qLBSNotification, String str, String str2, String str3) {
        this.a = new f(context, qLBSNotification, str, str2, str3);
    }

    public int getCarrierId() {
        return this.a.e();
    }

    public byte[] getDeviceData() {
        return this.a.c();
    }

    public boolean isGpsEnabled() {
        return this.a.d();
    }

    public void release() {
        this.a.a();
        this.a = null;
    }

    public void setGpsEnabled(boolean z) {
        this.a.a(z);
    }

    public void startLocation(int i) {
        this.a.a(i);
    }

    public void stopLocation() {
        this.a.b();
    }
}
