package com.tencent.lbsapi.core;

import LBSAPIProtocol.Cell;
import LBSAPIProtocol.DeviceData;
import LBSAPIProtocol.GPS;
import LBSAPIProtocol.Measure;
import android.content.Context;
import com.qq.jce.wup.UniPacket;
import com.tencent.lbsapi.QLBSNotification;
import java.util.ArrayList;
import java.util.Iterator;

public class f {
    private QLBSEngine a = null;
    private QLBSJNI b = null;
    private String c = null;
    private String d = null;
    private String e = null;

    public f(Context context, QLBSNotification qLBSNotification, String str, String str2, String str3) {
        this.a = new QLBSEngine(context, qLBSNotification);
        this.b = new QLBSJNI();
        this.c = str == null ? "" : str;
        this.d = str2 == null ? "" : str2;
        this.e = str3 == null ? "" : str3;
    }

    public void a() {
        if (this.a != null) {
            this.a.a();
            this.a.e();
            this.a = null;
        }
    }

    public void a(int i) {
        this.a.a(i);
    }

    public void a(boolean z) {
        this.a.a(z);
    }

    public void b() {
        this.a.a();
    }

    public byte[] c() {
        try {
            DeviceData deviceData = new DeviceData();
            deviceData.setStCurMeasure(new Measure());
            deviceData.getStCurMeasure().setVCells(new ArrayList());
            deviceData.getStCurMeasure().getVCells().add(new Cell((short) this.a.f(), (short) this.a.g(), this.a.h(), this.a.i()));
            Iterator it = this.a.m().iterator();
            while (it.hasNext()) {
                deviceData.getStCurMeasure().getVCells().add(new Cell((short) this.a.f(), (short) this.a.g(), this.a.h(), ((Integer) it.next()).intValue()));
            }
            deviceData.getStCurMeasure().setStGps(new GPS(this.a.j(), this.a.k(), -1, 0));
            deviceData.getStCurMeasure().setVMacs(this.a.l());
            deviceData.getStCurMeasure().setStrExtraInfo(this.a.o());
            ArrayList q = this.a.q();
            if (q == null) {
                q = new ArrayList();
            }
            deviceData.setVMeasures(q);
            deviceData.setStrImei(this.a.n());
            deviceData.setStrAppUA(this.e);
            deviceData.setEDeviceType(1);
            UniPacket uniPacket = new UniPacket();
            uniPacket.setRequestId(0);
            uniPacket.setEncodeName(e.e);
            uniPacket.setServantName(e.f);
            uniPacket.setFuncName(e.h);
            uniPacket.put(e.g, this.c);
            UniPacket uniPacket2 = new UniPacket();
            uniPacket2.setEncodeName(e.e);
            uniPacket2.setServantName(e.f);
            uniPacket2.setFuncName(e.h);
            uniPacket2.put(e.h, deviceData);
            byte[] encode = this.b.encode(uniPacket2.encode(), this.c, this.d);
            if (encode == null) {
                return null;
            }
            uniPacket.put(e.i, encode);
            return uniPacket.encode();
        } catch (Exception e2) {
            return null;
        }
    }

    public boolean d() {
        if (this.a != null) {
            return this.a.b();
        }
        return false;
    }

    public int e() {
        if (this.a != null) {
            return this.a.c();
        }
        return -1;
    }
}
