package com.qq.taf.cnst;

public class Const {
    public static final int INVALID_GRID_CODE = -1;
    public static final int INVALID_HASH_CODE = -1;
    public static final byte PACKET_TYPE_JCENORMAL = 0;
    public static final byte PACKET_TYPE_JCEONEWAY = 1;
    public static final byte PACKET_TYPE_WUP = 2;
    public static final String STATUS_DYED_KEY = "STATUS_DYED_KEY";
    public static final String STATUS_GRID_CODE = "STATUS_GRID_CODE";
    public static final String STATUS_GRID_KEY = "STATUS_GRID_KEY";
    public static final String STATUS_RESULT_CODE = "STATUS_RESULT_CODE";
    public static final String STATUS_RESULT_DESC = "STATUS_RESULT_DESC";
    public static final String STATUS_SAMPLE_KEY = "STATUS_SAMPLE_KEY";
}
