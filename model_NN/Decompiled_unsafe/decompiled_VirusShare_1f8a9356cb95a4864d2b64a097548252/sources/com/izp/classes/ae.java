package com.izp.classes;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.GradientDrawable;
import android.widget.RelativeLayout;

public class ae extends RelativeLayout {
    public boolean a = false;
    public int b = -16777216;

    public ae(Context context) {
        super(context);
    }

    private static void a(Canvas canvas, Rect rect, int i, int i2) {
        Paint paint = new Paint();
        paint.setColor(i);
        paint.setAntiAlias(true);
        canvas.drawRect(rect, paint);
        GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{Color.argb(127, Color.red(i2), Color.green(i2), Color.blue(i2)), i2});
        int height = ((int) (((double) rect.height()) * 0.4375d)) + rect.top;
        gradientDrawable.setBounds(rect.left, rect.top, rect.right, height);
        gradientDrawable.draw(canvas);
        Rect rect2 = new Rect(rect.left, height, rect.right, rect.bottom);
        Paint paint2 = new Paint();
        paint2.setColor(i2);
        canvas.drawRect(rect2, paint2);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        if (this.a) {
            a(canvas, new Rect(0, 0, getWidth(), getHeight()), -1, this.b);
        }
    }
}
