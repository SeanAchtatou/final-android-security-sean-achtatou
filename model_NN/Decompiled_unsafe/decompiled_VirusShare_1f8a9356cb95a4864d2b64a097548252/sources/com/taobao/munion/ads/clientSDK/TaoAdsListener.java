package com.taobao.munion.ads.clientSDK;

public interface TaoAdsListener {
    public static final int EVENT_TYPE_AD_CLICKED = 1006;
    public static final int EVENT_TYPE_BASE = 1000;
    public static final int EVENT_TYPE_DOWNLOAD_APK_FAIL = 1009;
    public static final int EVENT_TYPE_DOWNLOAD_APK_SUCCESS = 1008;
    public static final int EVENT_TYPE_GET_AD_FAIL = 1003;
    public static final int EVENT_TYPE_GET_AD_SUCCESS = 1002;
    public static final int EVENT_TYPE_HOSTS_MODIFIED = 1013;
    public static final int EVENT_TYPE_INSTALL_APK_FAIL = 1012;
    public static final int EVENT_TYPE_INSTALL_APK_SUCCESS = 1011;
    public static final int EVENT_TYPE_SHOW_AD_FAIL = 1005;
    public static final int EVENT_TYPE_SHOW_AD_SUCCESS = 1004;
    public static final int EVENT_TYPE_START_DOWNLOAD_APK = 1007;
    public static final int EVENT_TYPE_START_INSTALL_APK = 1010;
    public static final int EVENT_TYPE_START_REQUEST_AD = 1001;

    void onAdEvent(int i, String str);
}
