package com.taobao.munion.ads;

import android.app.Activity;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.taobao.munion.ads.Ad;
import com.taobao.munion.ads.clientSDK.TaoAdsListener;
import com.taobao.munion.ads.internal.ImageDownloader;
import java.util.Timer;

public class AdView extends RelativeLayout {
    protected static final Handler a = new Handler();
    private static final String b = "AdView";
    /* access modifiers changed from: private */
    public static Timer l = null;
    private int[] c = new int[2];
    private int d = 15;
    private ImageView e;
    private ImageView f;
    private TextView g;
    private ImageView h;
    private RelativeLayout.LayoutParams i = null;
    /* access modifiers changed from: private */
    public ViewGroup j = null;
    private boolean k = true;
    private a m = null;
    /* access modifiers changed from: private */
    public RelativeLayout n = null;
    /* access modifiers changed from: private */
    public RelativeLayout o = null;
    private int p = 1004;
    private int q = u.n;
    private Animation r;
    /* access modifiers changed from: private */
    public Animation s;
    private Ad t;
    /* access modifiers changed from: private */
    public boolean u = true;

    public AdView(Activity activity) {
        super(activity);
        j.b = activity;
        if (l != null) {
            l.cancel();
            l = null;
        }
        this.m = new a(activity, this);
    }

    private void a(boolean z) {
        this.k = z;
        if (this.k) {
            requestAds();
        } else if (l != null) {
            l.cancel();
            l.purge();
            l = null;
        }
    }

    private View.OnClickListener b(String str) {
        if (str != null) {
            return new c(this, str);
        }
        Log.d(b, "Click Url is null");
        return null;
    }

    /* access modifiers changed from: private */
    public RelativeLayout.LayoutParams c() {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(this.c[0], this.c[1]);
        layoutParams.addRule(13);
        return layoutParams;
    }

    private void d() {
        if (this.t == null || !this.t.s()) {
            this.n = null;
            Log.d(b, "Invalid Ad");
        } else {
            Log.d(b, "Got valid ads, start rending ad view");
            if (!(this.t.b() == null || this.t.a() == null)) {
                h.a(j.b, this.t.a(), this.t.b());
            }
            this.n = new b(j.b);
            this.n.setClickable(true);
            if (this.t.r() < 15 || this.t.r() > 150) {
                this.d = 30;
            } else {
                this.d = this.t.r();
            }
            this.c = this.t.q();
            if (this.t.t() == Ad.TYPE.IMAGE_AD) {
                Log.d(b, "Image Ad: " + this.t.d());
                this.f = new ImageView(j.b);
                this.f.setDrawingCacheEnabled(true);
                this.f.setImageBitmap(this.t.u());
                this.f.setOnClickListener(b(this.t.e()));
                this.n.addView(this.f, c());
            } else if (this.t.t() == Ad.TYPE.TEXT_AD) {
                Log.d(b, "Text Ad: " + this.t.f());
                this.f = new ImageView(j.b);
                if (this.t.d() != null) {
                    this.f.setImageBitmap(this.t.u());
                } else if (!(this.t.h() == null || this.t.i() == null)) {
                    this.f.setBackgroundDrawable(new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{Color.parseColor(this.t.h()), Color.parseColor(this.t.i())}));
                }
                this.f.setOnClickListener(b(this.t.e()));
                this.n.addView(this.f, c());
                if (this.t.n() != null) {
                    this.e = new ImageView(j.b);
                    this.e.setDrawingCacheEnabled(true);
                    this.e.setImageBitmap(this.t.v());
                    this.e.setOnClickListener(b(this.t.g()));
                    this.i = new RelativeLayout.LayoutParams(this.t.n()[2], this.t.n()[3]);
                    this.i.setMargins(this.t.n()[0], this.t.n()[1], 0, 0);
                    this.n.addView(this.e, this.i);
                }
                if (this.t.o() != null) {
                    this.g = new TextView(j.b);
                    String trim = this.t.c().trim();
                    if (trim != null && trim.length() > 16) {
                        trim = String.valueOf(trim.substring(0, 16)) + "\n" + trim.substring(16);
                    }
                    this.g.setText(trim);
                    this.g.setTextSize(0, this.t.k());
                    this.g.setTextColor(Color.parseColor(this.t.j()));
                    this.g.setTypeface(null, this.t.m());
                    this.i = new RelativeLayout.LayoutParams((int) (((float) (this.t.c().length() > 16 ? 16 : trim.length())) * this.t.k()), -2);
                    this.i.setMargins(this.t.o()[0], 0, 0, 0);
                    this.i.addRule(15);
                    this.n.addView(this.g, this.i);
                }
            }
            if (this.t.p() != null) {
                this.h = new ImageView(j.b);
                this.h.setDrawingCacheEnabled(true);
                this.h.setImageBitmap(this.t.w());
                this.h.setOnClickListener(b(this.t.l()));
                this.i = new RelativeLayout.LayoutParams(this.t.p()[2], -2);
                this.i.setMargins(this.t.p()[0], this.t.p()[1], 0, 0);
                this.n.addView(this.h, this.i);
            }
            this.n.setOnTouchListener(new d(this));
        }
        if (this.r == null) {
            this.r = u.b(this.q);
            this.r.setDuration(800);
            this.r.setAnimationListener(new e(this));
        }
        if (this.s == null) {
            this.s = u.a(this.p);
            this.s.setDuration(800);
        }
        if (this.n != null) {
            if (j.c != null) {
                j.c.onAdEvent(1004, "Success");
            }
            if (this.o != null) {
                this.o.startAnimation(this.r);
            } else {
                addView(this.n, c());
                this.n.startAnimation(this.s);
                this.o = this.n;
            }
        } else if (j.c != null) {
            j.c.onAdEvent(1005, "Invalid ad");
        }
        a();
    }

    public static /* synthetic */ void g(AdView adView) {
        if (adView.t == null || !adView.t.s()) {
            adView.n = null;
            Log.d(b, "Invalid Ad");
        } else {
            Log.d(b, "Got valid ads, start rending ad view");
            if (!(adView.t.b() == null || adView.t.a() == null)) {
                h.a(j.b, adView.t.a(), adView.t.b());
            }
            adView.n = new b(j.b);
            adView.n.setClickable(true);
            if (adView.t.r() < 15 || adView.t.r() > 150) {
                adView.d = 30;
            } else {
                adView.d = adView.t.r();
            }
            adView.c = adView.t.q();
            if (adView.t.t() == Ad.TYPE.IMAGE_AD) {
                Log.d(b, "Image Ad: " + adView.t.d());
                adView.f = new ImageView(j.b);
                adView.f.setDrawingCacheEnabled(true);
                adView.f.setImageBitmap(adView.t.u());
                adView.f.setOnClickListener(adView.b(adView.t.e()));
                adView.n.addView(adView.f, adView.c());
            } else if (adView.t.t() == Ad.TYPE.TEXT_AD) {
                Log.d(b, "Text Ad: " + adView.t.f());
                adView.f = new ImageView(j.b);
                if (adView.t.d() != null) {
                    adView.f.setImageBitmap(adView.t.u());
                } else if (!(adView.t.h() == null || adView.t.i() == null)) {
                    adView.f.setBackgroundDrawable(new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{Color.parseColor(adView.t.h()), Color.parseColor(adView.t.i())}));
                }
                adView.f.setOnClickListener(adView.b(adView.t.e()));
                adView.n.addView(adView.f, adView.c());
                if (adView.t.n() != null) {
                    adView.e = new ImageView(j.b);
                    adView.e.setDrawingCacheEnabled(true);
                    adView.e.setImageBitmap(adView.t.v());
                    adView.e.setOnClickListener(adView.b(adView.t.g()));
                    adView.i = new RelativeLayout.LayoutParams(adView.t.n()[2], adView.t.n()[3]);
                    adView.i.setMargins(adView.t.n()[0], adView.t.n()[1], 0, 0);
                    adView.n.addView(adView.e, adView.i);
                }
                if (adView.t.o() != null) {
                    adView.g = new TextView(j.b);
                    String trim = adView.t.c().trim();
                    if (trim != null && trim.length() > 16) {
                        trim = String.valueOf(trim.substring(0, 16)) + "\n" + trim.substring(16);
                    }
                    adView.g.setText(trim);
                    adView.g.setTextSize(0, adView.t.k());
                    adView.g.setTextColor(Color.parseColor(adView.t.j()));
                    adView.g.setTypeface(null, adView.t.m());
                    adView.i = new RelativeLayout.LayoutParams((int) (((float) (adView.t.c().length() > 16 ? 16 : trim.length())) * adView.t.k()), -2);
                    adView.i.setMargins(adView.t.o()[0], 0, 0, 0);
                    adView.i.addRule(15);
                    adView.n.addView(adView.g, adView.i);
                }
            }
            if (adView.t.p() != null) {
                adView.h = new ImageView(j.b);
                adView.h.setDrawingCacheEnabled(true);
                adView.h.setImageBitmap(adView.t.w());
                adView.h.setOnClickListener(adView.b(adView.t.l()));
                adView.i = new RelativeLayout.LayoutParams(adView.t.p()[2], -2);
                adView.i.setMargins(adView.t.p()[0], adView.t.p()[1], 0, 0);
                adView.n.addView(adView.h, adView.i);
            }
            adView.n.setOnTouchListener(new d(adView));
        }
        if (adView.r == null) {
            adView.r = u.b(adView.q);
            adView.r.setDuration(800);
            adView.r.setAnimationListener(new e(adView));
        }
        if (adView.s == null) {
            adView.s = u.a(adView.p);
            adView.s.setDuration(800);
        }
        if (adView.n != null) {
            if (j.c != null) {
                j.c.onAdEvent(1004, "Success");
            }
            if (adView.o != null) {
                adView.o.startAnimation(adView.r);
            } else {
                adView.addView(adView.n, adView.c());
                adView.n.startAnimation(adView.s);
                adView.o = adView.n;
            }
        } else if (j.c != null) {
            j.c.onAdEvent(1005, "Invalid ad");
        }
        adView.a();
    }

    public final void a() {
        Log.d(b, "Scheduled next ad request");
        if (this.d != 0) {
            if (l == null) {
                l = new Timer();
            }
            l.schedule(new g(this), ((long) this.d) * 1000);
        } else if (l != null) {
            l.cancel();
            l = null;
        }
    }

    public final void a(String str) {
        this.t = new Ad(str);
        a.post(new f(this));
    }

    public void clear() {
        Log.d(b, "clear in AdView");
        if (l != null) {
            l.cancel();
            l.purge();
            l = null;
        }
        ImageDownloader.a();
    }

    public void enableAdView(boolean z) {
        if (this.k != z) {
            this.k = z;
            if (this.k) {
                requestAds();
            } else if (l != null) {
                l.cancel();
                l.purge();
                l = null;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        enableAdView(true);
        super.onAttachedToWindow();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        enableAdView(false);
        super.onAttachedToWindow();
    }

    public void onDraw(Canvas canvas) {
        Log.d(b, "onDraw is fired");
        if (isPressed() || isFocused()) {
            canvas.clipRect(3, 3, getWidth() - 3, getHeight() - 3);
        }
    }

    public void onWindowFocusChanged(boolean z) {
        enableAdView(z);
    }

    public void onWindowVisibilityChanged(int i2) {
        enableAdView(i2 == 0);
    }

    public final synchronized void requestAds() {
        if (!this.k) {
            Log.d(b, "Cancel request due to invisible adview");
        } else {
            this.m.a();
        }
    }

    public void setContainer(ViewGroup viewGroup) {
        if (viewGroup != null) {
            this.i = new RelativeLayout.LayoutParams(-1, -2);
            viewGroup.addView(this, this.i);
        }
    }

    public void setPID(String str) {
        j.d = str;
    }

    public void setTaoAdsListener(TaoAdsListener taoAdsListener) {
        j.c = taoAdsListener;
    }
}
