package LBSAPIProtocol;

import com.qq.taf.jce.JceDisplayer;
import com.qq.taf.jce.JceInputStream;
import com.qq.taf.jce.JceOutputStream;
import com.qq.taf.jce.JceStruct;
import com.qq.taf.jce.JceUtil;

public final class GPS extends JceStruct {
    static final /* synthetic */ boolean $assertionsDisabled = (!GPS.class.desiredAssertionStatus());
    static int cache_eType;
    public int eType = 0;
    public int iAlt = -1;
    public int iLat = 900000000;
    public int iLon = 900000000;

    public GPS() {
        setILat(this.iLat);
        setILon(this.iLon);
        setIAlt(this.iAlt);
        setEType(this.eType);
    }

    public GPS(int i, int i2, int i3, int i4) {
        setILat(i);
        setILon(i2);
        setIAlt(i3);
        setEType(i4);
    }

    public String className() {
        return "LBSAPIProtocol.GPS";
    }

    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            if ($assertionsDisabled) {
                return null;
            }
            throw new AssertionError();
        }
    }

    public void display(StringBuilder sb, int i) {
        JceDisplayer jceDisplayer = new JceDisplayer(sb, i);
        jceDisplayer.display(this.iLat, "iLat");
        jceDisplayer.display(this.iLon, "iLon");
        jceDisplayer.display(this.iAlt, "iAlt");
        jceDisplayer.display(this.eType, "eType");
    }

    public boolean equals(Object obj) {
        GPS gps = (GPS) obj;
        return JceUtil.equals(this.iLat, gps.iLat) && JceUtil.equals(this.iLon, gps.iLon) && JceUtil.equals(this.iAlt, gps.iAlt) && JceUtil.equals(this.eType, gps.eType);
    }

    public String fullClassName() {
        return "LBSAPIProtocol.GPS";
    }

    public int getEType() {
        return this.eType;
    }

    public int getIAlt() {
        return this.iAlt;
    }

    public int getILat() {
        return this.iLat;
    }

    public int getILon() {
        return this.iLon;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int */
    public void readFrom(JceInputStream jceInputStream) {
        setILat(jceInputStream.read(this.iLat, 0, true));
        setILon(jceInputStream.read(this.iLon, 1, true));
        setIAlt(jceInputStream.read(this.iAlt, 2, true));
        setEType(jceInputStream.read(this.eType, 3, true));
    }

    public void setEType(int i) {
        this.eType = i;
    }

    public void setIAlt(int i) {
        this.iAlt = i;
    }

    public void setILat(int i) {
        this.iLat = i;
    }

    public void setILon(int i) {
        this.iLon = i;
    }

    public void writeTo(JceOutputStream jceOutputStream) {
        jceOutputStream.write(this.iLat, 0);
        jceOutputStream.write(this.iLon, 1);
        jceOutputStream.write(this.iAlt, 2);
        jceOutputStream.write(this.eType, 3);
    }
}
