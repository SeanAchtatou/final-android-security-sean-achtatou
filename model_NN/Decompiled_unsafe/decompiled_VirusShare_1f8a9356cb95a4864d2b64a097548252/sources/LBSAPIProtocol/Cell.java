package LBSAPIProtocol;

import com.qq.taf.jce.JceDisplayer;
import com.qq.taf.jce.JceInputStream;
import com.qq.taf.jce.JceOutputStream;
import com.qq.taf.jce.JceStruct;
import com.qq.taf.jce.JceUtil;

public final class Cell extends JceStruct {
    static final /* synthetic */ boolean $assertionsDisabled = (!Cell.class.desiredAssertionStatus());
    public int iCellId = -1;
    public int iLac = -1;
    public short shMcc = -1;
    public short shMnc = -1;

    public Cell() {
        setShMcc(this.shMcc);
        setShMnc(this.shMnc);
        setILac(this.iLac);
        setICellId(this.iCellId);
    }

    public Cell(short s, short s2, int i, int i2) {
        setShMcc(s);
        setShMnc(s2);
        setILac(i);
        setICellId(i2);
    }

    public String className() {
        return "LBSAPIProtocol.Cell";
    }

    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            if ($assertionsDisabled) {
                return null;
            }
            throw new AssertionError();
        }
    }

    public void display(StringBuilder sb, int i) {
        JceDisplayer jceDisplayer = new JceDisplayer(sb, i);
        jceDisplayer.display(this.shMcc, "shMcc");
        jceDisplayer.display(this.shMnc, "shMnc");
        jceDisplayer.display(this.iLac, "iLac");
        jceDisplayer.display(this.iCellId, "iCellId");
    }

    public boolean equals(Object obj) {
        Cell cell = (Cell) obj;
        return JceUtil.equals(this.shMcc, cell.shMcc) && JceUtil.equals(this.shMnc, cell.shMnc) && JceUtil.equals(this.iLac, cell.iLac) && JceUtil.equals(this.iCellId, cell.iCellId);
    }

    public String fullClassName() {
        return "LBSAPIProtocol.Cell";
    }

    public int getICellId() {
        return this.iCellId;
    }

    public int getILac() {
        return this.iLac;
    }

    public short getShMcc() {
        return this.shMcc;
    }

    public short getShMnc() {
        return this.shMnc;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
     arg types: [short, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int */
    public void readFrom(JceInputStream jceInputStream) {
        setShMcc(jceInputStream.read(this.shMcc, 0, true));
        setShMnc(jceInputStream.read(this.shMnc, 1, true));
        setILac(jceInputStream.read(this.iLac, 2, true));
        setICellId(jceInputStream.read(this.iCellId, 3, true));
    }

    public void setICellId(int i) {
        this.iCellId = i;
    }

    public void setILac(int i) {
        this.iLac = i;
    }

    public void setShMcc(short s) {
        this.shMcc = s;
    }

    public void setShMnc(short s) {
        this.shMnc = s;
    }

    public void writeTo(JceOutputStream jceOutputStream) {
        jceOutputStream.write(this.shMcc, 0);
        jceOutputStream.write(this.shMnc, 1);
        jceOutputStream.write(this.iLac, 2);
        jceOutputStream.write(this.iCellId, 3);
    }
}
