package defpackage;

import java.io.FilterInputStream;
import java.io.InputStream;

/* renamed from: l  reason: default package */
public final class l extends FilterInputStream {
    public l(InputStream inputStream) {
        super(inputStream);
    }

    public final long skip(long j) {
        long j2 = 0;
        while (j2 < j) {
            long skip = this.in.skip(j - j2);
            if (skip == 0) {
                if (read() < 0) {
                    break;
                }
                skip = 1;
            }
            j2 += skip;
        }
        return j2;
    }
}
