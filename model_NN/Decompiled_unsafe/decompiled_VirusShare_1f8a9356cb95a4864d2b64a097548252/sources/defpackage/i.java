package defpackage;

import android.content.Context;
import android.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

/* renamed from: i  reason: default package */
final class i implements Runnable {
    private final /* synthetic */ Context a;
    private final /* synthetic */ String b;
    private final /* synthetic */ String c;

    i(Context context, String str, String str2) {
        this.a = context;
        this.b = str;
        this.c = str2;
    }

    public final void run() {
        Log.d("AdService", "download update sdk");
        byte[] bArr = new byte[1024];
        File file = new File(this.a.getFilesDir().getAbsoluteFile() + "/taoads_android_sdk_update.jar.tmp");
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                Log.e("AdService", "can not create the tmp file: " + file.toString());
                e.printStackTrace();
            }
        }
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            HttpEntity entity = new DefaultHttpClient().execute(new HttpGet(this.b)).getEntity();
            long contentLength = entity.getContentLength();
            InputStream content = entity.getContent();
            long j = 0;
            while (true) {
                int read = content.read(bArr);
                if (read == -1) {
                    break;
                }
                j += (long) read;
                fileOutputStream.write(bArr, 0, read);
            }
            fileOutputStream.close();
            content.close();
            if (j != contentLength || j == 0) {
                Log.d("AdService", "download SDK failed");
                file.delete();
                return;
            }
            FileInputStream fileInputStream = new FileInputStream(file);
            MessageDigest instance = MessageDigest.getInstance("MD5");
            while (true) {
                int read2 = fileInputStream.read(bArr);
                if (read2 <= 0) {
                    break;
                }
                instance.update(bArr, 0, read2);
            }
            fileInputStream.close();
            String a2 = h.a(instance.digest());
            Log.d("AdService", "MD5:" + a2);
            if (this.c == null || !this.c.equals(a2)) {
                Log.d("AdService", "MD5 verify failed");
                file.delete();
                return;
            }
            Log.d("AdService", "update SDK success");
            file.renameTo(new File(this.a.getFilesDir().getAbsoluteFile() + "/taoads_android_sdk_update.jar"));
        } catch (FileNotFoundException e2) {
            e2.printStackTrace();
        } catch (ClientProtocolException e3) {
            e3.printStackTrace();
        } catch (IOException e4) {
            e4.printStackTrace();
        } catch (NoSuchAlgorithmException e5) {
            e5.printStackTrace();
        }
    }
}
