package defpackage;

import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import com.taobao.munion.ads.AdView;

/* renamed from: c  reason: default package */
public final class c implements View.OnClickListener {
    final /* synthetic */ AdView a;
    private final /* synthetic */ String b;

    public c(AdView adView, String str) {
        this.a = adView;
        this.b = str;
    }

    public final void onClick(View view) {
        if (j.c != null) {
            j.c.onAdEvent(1006, "Ad clicked");
        }
        if (this.a.u) {
            if (this.a.j == null) {
                this.a.j = (ViewGroup) j.b.getWindow().findViewById(16908290);
            }
            this.a.enableAdView(false);
            m mVar = new m(j.b, this.a);
            mVar.setFocusable(true);
            mVar.setFocusableInTouchMode(true);
            mVar.requestFocus();
            mVar.a(this.b);
            if (this.a.j != null) {
                this.a.j.addView(mVar);
            }
            mVar.startAnimation(mVar.a);
            return;
        }
        j.b.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(this.b)));
    }
}
