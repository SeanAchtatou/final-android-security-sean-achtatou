package MobWin;

public final class GPSTYPE {
    static final /* synthetic */ boolean $assertionsDisabled = (!GPSTYPE.class.desiredAssertionStatus());
    public static final GPSTYPE GPS_MARS = new GPSTYPE(1, 1, "GPS_MARS");
    public static final GPSTYPE GPS_WGS84 = new GPSTYPE(0, 0, "GPS_WGS84");
    public static final int _GPS_MARS = 1;
    public static final int _GPS_WGS84 = 0;
    private static GPSTYPE[] __values = new GPSTYPE[2];
    private String __T = new String();
    private int __value;

    public static GPSTYPE convert(int val) {
        for (int __i = 0; __i < __values.length; __i++) {
            if (__values[__i].value() == val) {
                return __values[__i];
            }
        }
        if ($assertionsDisabled) {
            return null;
        }
        throw new AssertionError();
    }

    public static GPSTYPE convert(String val) {
        for (int __i = 0; __i < __values.length; __i++) {
            if (__values[__i].toString().equals(val)) {
                return __values[__i];
            }
        }
        if ($assertionsDisabled) {
            return null;
        }
        throw new AssertionError();
    }

    public int value() {
        return this.__value;
    }

    public String toString() {
        return this.__T;
    }

    private GPSTYPE(int index, int val, String s) {
        this.__T = s;
        this.__value = val;
        __values[index] = this;
    }
}
