package MobWin;

import com.qq.taf.jce.JceDisplayer;
import com.qq.taf.jce.JceInputStream;
import com.qq.taf.jce.JceOutputStream;
import com.qq.taf.jce.JceStruct;
import com.qq.taf.jce.JceUtil;

public final class AppColorsSetting extends JceStruct {
    static final /* synthetic */ boolean f = (!AppColorsSetting.class.desiredAssertionStatus());
    public boolean a = true;
    public String b = "";
    public String c = "";
    public String d = "";
    public String e = "";

    public AppColorsSetting() {
        a(this.a);
        a(this.b);
        b(this.c);
        c(this.d);
        d(this.e);
    }

    public AppColorsSetting(boolean z, String str, String str2, String str3, String str4) {
        a(z);
        a(str);
        b(str2);
        c(str3);
        d(str4);
    }

    public String a() {
        return "MobWin.AppColorsSetting";
    }

    public void a(String str) {
        this.b = str;
    }

    public void a(boolean z) {
        this.a = z;
    }

    public void b(String str) {
        this.c = str;
    }

    public boolean b() {
        return this.a;
    }

    public String c() {
        return this.b;
    }

    public void c(String str) {
        this.d = str;
    }

    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e2) {
            if (f) {
                return null;
            }
            throw new AssertionError();
        }
    }

    public String d() {
        return this.c;
    }

    public void d(String str) {
        this.e = str;
    }

    public void display(StringBuilder sb, int i) {
        JceDisplayer jceDisplayer = new JceDisplayer(sb, i);
        jceDisplayer.display(this.a, "isUseSDKDefault");
        jceDisplayer.display(this.b, "titleColor");
        jceDisplayer.display(this.c, "wordsColor");
        jceDisplayer.display(this.d, "bannerBgColor");
        jceDisplayer.display(this.e, "bannerBgOpacity");
    }

    public String e() {
        return this.d;
    }

    public boolean equals(Object obj) {
        AppColorsSetting appColorsSetting = (AppColorsSetting) obj;
        return JceUtil.equals(this.a, appColorsSetting.a) && JceUtil.equals(this.b, appColorsSetting.b) && JceUtil.equals(this.c, appColorsSetting.c) && JceUtil.equals(this.d, appColorsSetting.d) && JceUtil.equals(this.e, appColorsSetting.e);
    }

    public String f() {
        return this.e;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
     arg types: [boolean, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean */
    public void readFrom(JceInputStream jceInputStream) {
        a(jceInputStream.read(this.a, 1, true));
        a(jceInputStream.readString(2, false));
        b(jceInputStream.readString(3, false));
        c(jceInputStream.readString(4, false));
        d(jceInputStream.readString(5, false));
    }

    public void writeTo(JceOutputStream jceOutputStream) {
        jceOutputStream.write(this.a, 1);
        if (this.b != null) {
            jceOutputStream.write(this.b, 2);
        }
        if (this.c != null) {
            jceOutputStream.write(this.c, 3);
        }
        if (this.d != null) {
            jceOutputStream.write(this.d, 4);
        }
        if (this.e != null) {
            jceOutputStream.write(this.e, 5);
        }
    }
}
