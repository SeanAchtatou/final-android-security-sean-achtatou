package MobWin;

public final class NET_TYPE {
    static final /* synthetic */ boolean $assertionsDisabled = (!NET_TYPE.class.desiredAssertionStatus());
    public static final NET_TYPE CMNET = new NET_TYPE(6, 6, "CMNET");
    public static final NET_TYPE CMWAP = new NET_TYPE(7, 7, "CMWAP");
    public static final NET_TYPE CTNET = new NET_TYPE(8, 8, "CTNET");
    public static final NET_TYPE CTWAP = new NET_TYPE(9, 9, "CTWAP");
    public static final NET_TYPE GPRS = new NET_TYPE(0, 0, "GPRS");
    public static final NET_TYPE UNINET = new NET_TYPE(4, 4, "UNINET");
    public static final NET_TYPE UNIWAP = new NET_TYPE(5, 5, "UNIWAP");
    public static final NET_TYPE WIFI = new NET_TYPE(1, 1, "WIFI");
    public static final NET_TYPE _3GNET = new NET_TYPE(2, 2, "_3GNET");
    public static final NET_TYPE _3GWAP = new NET_TYPE(3, 3, "_3GWAP");
    public static final int _CMNET = 6;
    public static final int _CMWAP = 7;
    public static final int _CTNET = 8;
    public static final int _CTWAP = 9;
    public static final int _GPRS = 0;
    public static final int _UNINET = 4;
    public static final int _UNIWAP = 5;
    public static final int _WIFI = 1;
    public static final int __3GNET = 2;
    public static final int __3GWAP = 3;
    private static NET_TYPE[] __values = new NET_TYPE[10];
    private String __T = new String();
    private int __value;

    public static NET_TYPE convert(int val) {
        for (int __i = 0; __i < __values.length; __i++) {
            if (__values[__i].value() == val) {
                return __values[__i];
            }
        }
        if ($assertionsDisabled) {
            return null;
        }
        throw new AssertionError();
    }

    public static NET_TYPE convert(String val) {
        for (int __i = 0; __i < __values.length; __i++) {
            if (__values[__i].toString().equals(val)) {
                return __values[__i];
            }
        }
        if ($assertionsDisabled) {
            return null;
        }
        throw new AssertionError();
    }

    public int value() {
        return this.__value;
    }

    public String toString() {
        return this.__T;
    }

    private NET_TYPE(int index, int val, String s) {
        this.__T = s;
        this.__value = val;
        __values[index] = this;
    }
}
