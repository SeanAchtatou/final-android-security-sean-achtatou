package com.adwo.adsdk;

import android.content.Context;
import android.content.IntentFilter;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.widget.RelativeLayout;
import com.ju6.mms.pdu.CharacterSets;
import java.util.Timer;

public final class AdwoAdView extends RelativeLayout {
    private static int d;
    /* access modifiers changed from: private */
    public static Handler k = new Handler();
    private static int l;
    private static int m;
    /* access modifiers changed from: private */
    public static byte n = 0;
    protected boolean a;
    protected volatile boolean b;
    /* access modifiers changed from: private */
    public C0002b c;
    private Timer e;
    private int f;
    private int g;
    private boolean h;
    /* access modifiers changed from: private */
    public AdListener i;
    private boolean j;
    private D o;
    private Context p;

    public final void finalize() {
        try {
            super.finalize();
        } catch (Throwable th) {
        }
        if (this.p != null) {
            try {
                if (this.o != null) {
                    this.p.unregisterReceiver(this.o);
                }
                this.p = null;
                if (this.i != null) {
                    this.i = null;
                }
            } catch (Throwable th2) {
            }
        }
    }

    protected static int a() {
        return l;
    }

    protected static int b() {
        return m;
    }

    public AdwoAdView(Context context, String str, int i2, int i3, boolean z, int i4) {
        super(context, null, 0);
        this.b = true;
        this.o = new D(this);
        this.p = null;
        this.p = context;
        this.j = false;
        setFocusable(true);
        setDescendantFocusability(262144);
        setClickable(true);
        if (z) {
            C0009i.a(z);
        }
        b(i3);
        setBackgroundColor(i2);
        a(i4);
        this.h = true;
        C0009i.b(str);
        a(context);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adwo.adsdk.i.a(int, int, boolean):byte
     arg types: [int, int, int]
     candidates:
      com.adwo.adsdk.i.a(android.content.Context, byte, byte):com.adwo.adsdk.f
      com.adwo.adsdk.i.a(int, int, boolean):byte */
    private void a(Context context) {
        new DisplayMetrics();
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        Log.i("Adwo SDK", "Version 2.2 width: " + displayMetrics.widthPixels + " height:" + displayMetrics.heightPixels + " density:" + displayMetrics.density);
        C0009i.b(context);
        byte a2 = C0009i.a((int) ((float) displayMetrics.widthPixels), (int) ((float) displayMetrics.heightPixels), false);
        n = a2;
        if (a2 < 5 || n > 9) {
            l = 320;
            m = 50;
        } else {
            l = O.j[n - 5];
            m = O.k[n - 5];
        }
        IntentFilter intentFilter = new IntentFilter("android.intent.action.SCREEN_ON");
        intentFilter.addAction("android.intent.action.SCREEN_OFF");
        context.registerReceiver(this.o, intentFilter);
    }

    public AdwoAdView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        this.p = context;
    }

    public AdwoAdView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        int i3;
        int i4;
        this.b = true;
        this.o = new D(this);
        this.p = null;
        this.p = context;
        this.j = false;
        setFocusable(true);
        setDescendantFocusability(262144);
        setClickable(true);
        if (attributeSet != null) {
            String str = "http://schemas.android.com/apk/res/" + context.getPackageName();
            boolean attributeBooleanValue = attributeSet.getAttributeBooleanValue(str, "testing", false);
            if (attributeBooleanValue) {
                C0009i.a(attributeBooleanValue);
            }
            i4 = attributeSet.getAttributeUnsignedIntValue(str, "textColor", -1);
            int attributeUnsignedIntValue = attributeSet.getAttributeUnsignedIntValue(str, "backgroundColor", 0);
            a(attributeSet.getAttributeIntValue(str, "refreshInterval", 30));
            i3 = attributeUnsignedIntValue;
        } else {
            i3 = 0;
            i4 = -1;
        }
        this.h = true;
        b(i4);
        setBackgroundColor(i3);
        C0009i.b(C0009i.a(context));
        a(context);
    }

    /* access modifiers changed from: protected */
    public final void c() {
        if (this.b) {
            if (super.getVisibility() != 0) {
                Log.w("Adwo SDK", "You have set ads view invisible.  You must call ads view.setVisibility(View.VISIBLE).");
            } else {
                new z(this).start();
            }
        }
    }

    private static void a(int i2) {
        int i3;
        if (i2 <= 0) {
            i3 = 0;
        } else if (i2 < 20) {
            C0009i.a("Fresh ads Interval(" + i2 + ") seconds must be >= " + 20);
            i3 = i2;
        } else {
            if (i2 > 600) {
                C0009i.a("Fresh ads Interval(" + i2 + ") seconds must be <= " + 600);
            }
            i3 = i2;
        }
        d = i3 * CharacterSets.UCS2;
    }

    private void a(boolean z) {
        synchronized (this) {
            if (z) {
                if (d > 0) {
                    if (this.e == null) {
                        this.e = new Timer();
                        this.e.schedule(new B(this), 0, (long) d);
                    }
                }
            }
            if (!z || d == 0) {
                if (this.e != null) {
                    this.e.cancel();
                    this.e = null;
                }
            } else if (super.getVisibility() == 4 && this.e != null) {
                this.e.cancel();
                this.e = null;
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void onWindowVisibilityChanged(int i2) {
        super.onWindowVisibilityChanged(i2);
        if (i2 == 8) {
            a(false);
        } else if (i2 == 0) {
            a(true);
        } else if (i2 == 4) {
            a(false);
        }
    }

    private void b(int i2) {
        this.g = -16777216 | i2;
        if (this.c != null) {
            this.c.a(i2);
        }
        invalidate();
    }

    /* access modifiers changed from: protected */
    public final int d() {
        return this.g;
    }

    public final void setBackgroundColor(int i2) {
        this.f = -16777216 | i2;
        if (this.c != null) {
            this.c.setBackgroundColor(i2);
        }
        invalidate();
    }

    /* access modifiers changed from: protected */
    public final int e() {
        return this.f;
    }

    public final void setVisibility(int i2) {
        if (super.getVisibility() != i2) {
            synchronized (this) {
                int childCount = getChildCount();
                for (int i3 = 0; i3 < childCount; i3++) {
                    getChildAt(i3).setVisibility(i2);
                }
                super.setVisibility(i2);
                if (i2 == 4 && this.c != null) {
                    this.c.c();
                    removeView(this.c);
                    this.c.a();
                    this.c = null;
                }
            }
        }
    }

    public final int getVisibility() {
        if (this.h) {
            if (!(this.c != null)) {
                return 8;
            }
        }
        return super.getVisibility();
    }

    public final void setListener(AdListener adListener) {
        synchronized (this) {
            this.i = adListener;
        }
    }

    /* access modifiers changed from: protected */
    public final void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        setMeasuredDimension(getMeasuredWidth(), m);
    }

    /* access modifiers changed from: protected */
    public final void onAttachedToWindow() {
        this.j = true;
        super.onAttachedToWindow();
    }

    /* access modifiers changed from: protected */
    public final void onDetachedFromWindow() {
        this.j = false;
        a(false);
        super.onDetachedFromWindow();
    }

    static /* synthetic */ void b(AdwoAdView adwoAdView, C0002b bVar) {
        if (adwoAdView.c != null) {
            adwoAdView.c.c();
        }
        adwoAdView.c = bVar;
        if (adwoAdView.j) {
            AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
            alphaAnimation.setDuration(166);
            alphaAnimation.startNow();
            alphaAnimation.setFillAfter(true);
            alphaAnimation.setInterpolator(new AccelerateInterpolator());
            adwoAdView.startAnimation(alphaAnimation);
        }
    }

    static /* synthetic */ void c(AdwoAdView adwoAdView, C0002b bVar) {
        bVar.setVisibility(8);
        float width = ((float) adwoAdView.getWidth()) / 2.0f;
        float height = ((float) adwoAdView.getHeight()) / 2.0f;
        float width2 = -0.4f * ((float) adwoAdView.getWidth());
        M m2 = M.values()[(int) (2.0d * Math.random())];
        S s = new S(0.0f, -90.0f, width, height, 0.0f, 0.0f, width2, true, m2);
        s.setDuration(500);
        s.setFillAfter(true);
        s.setInterpolator(new AccelerateInterpolator());
        s.setAnimationListener(new C(adwoAdView, bVar, m2));
        adwoAdView.startAnimation(s);
    }
}
