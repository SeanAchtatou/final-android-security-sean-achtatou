package com.izp.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import com.safesys.myvpn.Constants;

public class IZPView extends RelativeLayout {
    public String adType;
    private b adm = new b(this);
    public IZPDelegate delegate;
    public boolean isDev = true;
    public String productID;
    private boolean started;

    public IZPView(Context context) {
        super(context);
        setBackgroundColor(0);
    }

    public IZPView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setBackgroundColor(0);
    }

    public IZPView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        setBackgroundColor(0);
    }

    public void startAdExchange() {
        if (this.started) {
            return;
        }
        if (this.productID == null) {
            if (this.delegate != null) {
                this.delegate.errorReport(this, 100, "product id is null !");
            }
        } else if (this.adType != null) {
            this.adm.a(this.productID);
            this.adm.b(this.adType);
            this.adm.a(this.isDev);
            this.adm.a(this.delegate);
            this.adm.b();
            this.started = true;
        } else if (this.delegate != null) {
            this.delegate.errorReport(this, Constants.VPN_ERROR_CONNECTION_FAILED, "adType is null !");
        }
    }

    public void stopAdExchange() {
        if (this.started) {
            this.adm.c();
            this.started = false;
        }
    }
}
