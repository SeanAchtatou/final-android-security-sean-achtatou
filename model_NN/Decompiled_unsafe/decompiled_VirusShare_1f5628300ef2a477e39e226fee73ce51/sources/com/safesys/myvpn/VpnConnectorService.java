package com.safesys.myvpn;

import android.app.PendingIntent;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.Toast;
import com.safesys.myvpn.wrapper.VpnState;

public class VpnConnectorService extends Service {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$safesys$myvpn$wrapper$VpnState;
    /* access modifiers changed from: private */
    public static final String TAG = VpnConnectorService.class.getName();
    private static final ComponentName THIS_APPWIDGET = new ComponentName("com.safesys.myvpn", "com.safesys.myvpn.VpnAppWidgetProvider");
    private VpnActor actor;
    private Context context;
    private VpnStateReceiver receiver;
    private VpnState state = VpnState.IDLE;

    static /* synthetic */ int[] $SWITCH_TABLE$com$safesys$myvpn$wrapper$VpnState() {
        int[] iArr = $SWITCH_TABLE$com$safesys$myvpn$wrapper$VpnState;
        if (iArr == null) {
            iArr = new int[VpnState.values().length];
            try {
                iArr[VpnState.CANCELLED.ordinal()] = 3;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[VpnState.CONNECTED.ordinal()] = 4;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[VpnState.CONNECTING.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[VpnState.DISCONNECTING.ordinal()] = 2;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[VpnState.IDLE.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[VpnState.UNKNOWN.ordinal()] = 7;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[VpnState.UNUSABLE.ordinal()] = 6;
            } catch (NoSuchFieldError e7) {
            }
            $SWITCH_TABLE$com$safesys$myvpn$wrapper$VpnState = iArr;
        }
        return iArr;
    }

    private final class VpnStateReceiver extends BroadcastReceiver {
        private VpnStateReceiver() {
        }

        /* synthetic */ VpnStateReceiver(VpnConnectorService vpnConnectorService, VpnStateReceiver vpnStateReceiver) {
            this();
        }

        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (Constants.ACT_TOGGLE_VPN_CONN.equals(action)) {
                VpnConnectorService.this.toggleVpnState(intent);
            } else if (Constants.ACTION_VPN_CONNECTIVITY.equals(action)) {
                VpnConnectorService.this.onStateChanged(intent);
            } else {
                Log.w(VpnConnectorService.TAG, "VpnStateReceiver ignores unknown intent:" + intent);
            }
        }
    }

    public void onCreate() {
        super.onCreate();
        Log.i(TAG, "VpnConnectorService created");
        this.context = getApplicationContext();
        this.actor = new VpnActor(this.context);
        updateViews();
        registerReceivers();
        this.actor.checkStatus();
    }

    private void registerReceivers() {
        this.receiver = new VpnStateReceiver(this, null);
        IntentFilter filter = new IntentFilter();
        filter.addAction(Constants.ACTION_VPN_CONNECTIVITY);
        filter.addAction(Constants.ACT_TOGGLE_VPN_CONN);
        registerReceiver(this.receiver, filter);
    }

    public void onDestroy() {
        Log.i(TAG, "VpnConnectorService destroyed");
        unregisterReceivers();
        super.onDestroy();
    }

    private void unregisterReceivers() {
        if (this.receiver != null) {
            unregisterReceiver(this.receiver);
        }
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void toggleVpnState(Intent intent) {
        switch ($SWITCH_TABLE$com$safesys$myvpn$wrapper$VpnState()[this.state.ordinal()]) {
            case 4:
                this.actor.disconnect();
                return;
            case 5:
                connect();
                return;
            default:
                Log.i(TAG, "toggleVpnState intent not handled, currentState=" + this.state + ", intent=" + intent);
                return;
        }
    }

    private void connect() {
        try {
            this.actor.connect();
        } catch (NoActiveVpnException e) {
            Toast.makeText(this, (int) R.string.err_no_active_vpn, 0).show();
            Log.e(TAG, "connect failed, no active vpn");
        }
    }

    public void onStateChanged(Intent intent) {
        String profileName = intent.getStringExtra(Constants.BROADCAST_PROFILE_NAME);
        VpnState newState = Utils.extractVpnState(intent);
        Log.d(TAG, String.valueOf(profileName) + " stateChanged: " + this.state + "->" + newState + ", errCode=" + intent.getIntExtra(Constants.BROADCAST_ERROR_CODE, 0));
        updateViews(profileName, newState);
    }

    private void updateViews(String profileName, VpnState newState) {
        if (!profileName.equals(Utils.getActvieProfileName(this.context))) {
            Log.d(TAG, "updateViews, ignores non-active profile event for " + profileName);
            return;
        }
        this.state = newState;
        updateViews();
    }

    private void updateViews() {
        RemoteViews views = new RemoteViews(this.context.getPackageName(), getViewId());
        updateButtons(views);
        AppWidgetManager.getInstance(this.context).updateAppWidget(THIS_APPWIDGET, views);
    }

    private int getViewId() {
        switch ($SWITCH_TABLE$com$safesys$myvpn$wrapper$VpnState()[this.state.ordinal()]) {
            case 1:
            case 2:
                return R.layout.vpn_widget_transition;
            case 3:
            default:
                return R.layout.vpn_widget_disconnected;
            case 4:
                return R.layout.vpn_widget_connected;
        }
    }

    private void updateButtons(RemoteViews views) {
        Intent intent = new Intent(this.context, ToggleVpn.class);
        intent.putExtra(Constants.KEY_VPN_STATE, this.state);
        views.setOnClickPendingIntent(R.id.frmToggleVpnStatue, PendingIntent.getActivity(this.context, 2, intent, 1));
    }
}
