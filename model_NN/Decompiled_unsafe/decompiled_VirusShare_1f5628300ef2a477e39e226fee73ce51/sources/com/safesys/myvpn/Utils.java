package com.safesys.myvpn;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import com.safesys.myvpn.wrapper.VpnProfile;
import com.safesys.myvpn.wrapper.VpnState;
import java.io.File;
import java.io.Serializable;

public final class Utils {
    public static void showErrMessage(Activity ctx, AppException e) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setCancelable(true).setMessage(ctx.getString(e.getMessageCode(), e.getMessageArgs()));
        AlertDialog dlg = builder.create();
        dlg.setOwnerActivity(ctx);
        dlg.show();
    }

    public static void ensureDir(File dir) {
        String state = Environment.getExternalStorageState();
        if (!"mounted".equals(state)) {
            throw new AppException("no writable storage found, state=" + state, R.string.err_no_writable_storage, new Object[0]);
        }
        if (!dir.exists()) {
            dir.mkdirs();
        }
        if (!dir.exists()) {
            throw new AppException("failed to mkdir: " + dir, R.string.err_exp_write_storage_failed, new Object[0]);
        }
    }

    public static String getActvieProfileName(Context ctx) {
        VpnProfile activeProfile = VpnProfileRepository.getInstance(ctx).getActiveProfile();
        if (activeProfile != null) {
            return activeProfile.getName();
        }
        return null;
    }

    public static VpnState extractVpnState(Intent intent) {
        Serializable obj = intent.getSerializableExtra(Constants.BROADCAST_CONNECTION_STATE);
        VpnState state = VpnState.IDLE;
        if (obj != null) {
            return VpnState.valueOf(obj.toString());
        }
        return state;
    }

    public static boolean isInStableState(VpnProfile p) {
        VpnState state = p.getState();
        return state == VpnState.CONNECTED || state == VpnState.IDLE;
    }

    private Utils() {
    }
}
