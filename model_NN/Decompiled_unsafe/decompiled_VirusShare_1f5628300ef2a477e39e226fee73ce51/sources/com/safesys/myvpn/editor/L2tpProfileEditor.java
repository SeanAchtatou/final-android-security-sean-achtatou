package com.safesys.myvpn.editor;

import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import com.safesys.myvpn.Constants;
import com.safesys.myvpn.R;
import com.safesys.myvpn.wrapper.L2tpProfile;
import com.safesys.myvpn.wrapper.VpnProfile;
import com.safesys.myvpn.wrapper.VpnType;

public class L2tpProfileEditor extends VpnProfileEditor {
    private CheckBox chkSecretEnabled;
    /* access modifiers changed from: private */
    public TextView lblSecret;
    /* access modifiers changed from: private */
    public EditText txtSecret;

    /* access modifiers changed from: protected */
    public void initSpecificWidgets(ViewGroup content) {
        this.chkSecretEnabled = new CheckBox(this);
        this.chkSecretEnabled.setText(getString(R.string.l2tp_secret_enabled));
        content.addView(this.chkSecretEnabled);
        this.lblSecret = new TextView(this);
        this.lblSecret.setText(getString(R.string.l2tp_secret));
        content.addView(this.lblSecret);
        this.txtSecret = new EditText(this);
        this.txtSecret.setImeOptions(5);
        this.txtSecret.setTransformationMethod(new PasswordTransformationMethod());
        content.addView(this.txtSecret);
        this.lblSecret.setEnabled(false);
        this.txtSecret.setEnabled(false);
        this.chkSecretEnabled.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                L2tpProfileEditor.this.lblSecret.setEnabled(isChecked);
                L2tpProfileEditor.this.txtSecret.setEnabled(isChecked);
            }
        });
        checkPL2TP();
    }

    private void checkPL2TP() {
        if (((VpnType) getIntent().getExtras().get(Constants.KEY_VPN_TYPE)) == VpnType.PL2TP) {
            Log.d("XXX", "That's OK!");
            this.chkSecretEnabled.setVisibility(8);
            this.chkSecretEnabled.setChecked(false);
            this.txtSecret.setVisibility(8);
            this.lblSecret.setVisibility(8);
        }
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            super.onRestoreInstanceState(savedInstanceState);
            this.chkSecretEnabled.setChecked(savedInstanceState.getBoolean("secretEnabled"));
            this.txtSecret.setText(savedInstanceState.getCharSequence("secret"));
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("secretEnabled", this.chkSecretEnabled.isChecked());
        outState.putCharSequence("secret", this.txtSecret.getText());
    }

    /* access modifiers changed from: protected */
    public VpnProfile createProfile() {
        return new L2tpProfile(getApplicationContext());
    }

    /* access modifiers changed from: protected */
    public void doPopulateProfile() {
        L2tpProfile p = (L2tpProfile) getProfile();
        boolean secretEnabled = this.chkSecretEnabled.isChecked();
        p.setSecretEnabled(secretEnabled);
        p.setSecretString(secretEnabled ? this.txtSecret.getText().toString().trim() : "");
    }

    /* access modifiers changed from: protected */
    public void doBindToViews() {
        L2tpProfile p = (L2tpProfile) getProfile();
        this.chkSecretEnabled.setChecked(p.isSecretEnabled());
        if (p.isSecretEnabled()) {
            this.txtSecret.setText(p.getSecretString());
        }
    }
}
