package com.safesys.myvpn.editor;

import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.CheckBox;
import com.safesys.myvpn.R;
import com.safesys.myvpn.wrapper.PptpProfile;
import com.safesys.myvpn.wrapper.VpnProfile;

public class PptpProfileEditor extends VpnProfileEditor {
    private CheckBox chkEncrypt;

    /* access modifiers changed from: protected */
    public void initSpecificWidgets(ViewGroup content) {
        this.chkEncrypt = new CheckBox(this);
        this.chkEncrypt.setText(getString(R.string.encrypt_enabled));
        content.addView(this.chkEncrypt);
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            super.onRestoreInstanceState(savedInstanceState);
            this.chkEncrypt.setChecked(savedInstanceState.getBoolean("encrypt"));
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("encrypt", this.chkEncrypt.isChecked());
    }

    /* access modifiers changed from: protected */
    public VpnProfile createProfile() {
        return new PptpProfile(getApplicationContext());
    }

    /* access modifiers changed from: protected */
    public void doPopulateProfile() {
        ((PptpProfile) getProfile()).setEncryptionEnabled(this.chkEncrypt.isChecked());
    }

    /* access modifiers changed from: protected */
    public void doBindToViews() {
        this.chkEncrypt.setChecked(((PptpProfile) getProfile()).isEncryptionEnabled());
    }
}
