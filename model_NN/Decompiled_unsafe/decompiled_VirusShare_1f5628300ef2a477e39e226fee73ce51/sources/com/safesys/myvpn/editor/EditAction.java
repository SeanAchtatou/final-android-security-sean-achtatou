package com.safesys.myvpn.editor;

public enum EditAction {
    CREATE,
    EDIT,
    DELETE
}
