package com.safesys.myvpn.editor;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.ju6.mms.pdu.PduPersister;
import com.safesys.myvpn.AppException;
import com.safesys.myvpn.Constants;
import com.safesys.myvpn.R;
import com.safesys.myvpn.VpnProfileRepository;
import com.safesys.myvpn.wrapper.InvalidProfileException;
import com.safesys.myvpn.wrapper.KeyStore;
import com.safesys.myvpn.wrapper.VpnProfile;
import com.safesys.myvpn.wrapper.VpnState;
import com.safesys.myvpn.wrapper.VpnType;

public abstract class VpnProfileEditor extends Activity {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$safesys$myvpn$editor$EditAction = null;
    private static final int DESC_FONT_SIZE = 12;
    private static final int GRAY = -6710887;
    private EditAction editAction;
    private Object[] errMsgArgs;
    private KeyStore keyStore;
    private TextView lblDnsSuffices;
    private TextView lblDnsSufficesDesc;
    private TextView lblServer;
    private TextView lblVpnName;
    private VpnProfile profile;
    private VpnProfileRepository repository;
    private Runnable resumeAction;
    private EditText txtDnsSuffices;
    private EditText txtPassword;
    private EditText txtServer;
    private EditText txtUserName;
    private EditText txtVpnName;

    /* access modifiers changed from: protected */
    public abstract VpnProfile createProfile();

    /* access modifiers changed from: protected */
    public abstract void doBindToViews();

    /* access modifiers changed from: protected */
    public abstract void doPopulateProfile();

    /* access modifiers changed from: protected */
    public abstract void initSpecificWidgets(ViewGroup viewGroup);

    static /* synthetic */ int[] $SWITCH_TABLE$com$safesys$myvpn$editor$EditAction() {
        int[] iArr = $SWITCH_TABLE$com$safesys$myvpn$editor$EditAction;
        if (iArr == null) {
            iArr = new int[EditAction.values().length];
            try {
                iArr[EditAction.CREATE.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[EditAction.DELETE.ordinal()] = 3;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[EditAction.EDIT.ordinal()] = 2;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$com$safesys$myvpn$editor$EditAction = iArr;
        }
        return iArr;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.vpn_profile_editor);
        this.repository = VpnProfileRepository.getInstance(getApplicationContext());
        this.keyStore = new KeyStore(getApplicationContext());
        LinearLayout contentView = new LinearLayout(this);
        contentView.setOrientation(1);
        initWidgets(contentView);
        ((ScrollView) findViewById(R.id.editorScrollView)).addView(contentView);
        init(getIntent());
    }

    private void initWidgets(ViewGroup content) {
        this.lblVpnName = new TextView(this);
        this.lblVpnName.setText(getString(R.string.vpnname));
        content.addView(this.lblVpnName);
        this.txtVpnName = new EditText(this);
        this.txtVpnName.setImeOptions(5);
        content.addView(this.txtVpnName);
        this.lblServer = new TextView(this);
        this.lblServer.setText(getString(R.string.server));
        content.addView(this.lblServer);
        this.txtServer = new EditText(this);
        this.txtServer.setImeOptions(5);
        content.addView(this.txtServer);
        initSpecificWidgets(content);
        this.lblDnsSuffices = new TextView(this);
        this.lblDnsSuffices.setText(getString(R.string.dns_suffices));
        content.addView(this.lblDnsSuffices);
        this.lblDnsSufficesDesc = new TextView(this);
        this.lblDnsSufficesDesc.setText(getString(R.string.comma_sep));
        this.lblDnsSufficesDesc.setTextColor((int) GRAY);
        this.lblDnsSufficesDesc.setTextSize(1, 12.0f);
        content.addView(this.lblDnsSufficesDesc);
        this.txtDnsSuffices = new EditText(this);
        this.txtDnsSuffices.setImeOptions(5);
        content.addView(this.txtDnsSuffices);
        TextView lblUserName = new TextView(this);
        lblUserName.setText(getString(R.string.username));
        content.addView(lblUserName);
        this.txtUserName = new EditText(this);
        this.txtUserName.setImeOptions(5);
        content.addView(this.txtUserName);
        TextView lblPassword = new TextView(this);
        lblPassword.setText(getString(R.string.password));
        content.addView(lblPassword);
        this.txtPassword = new EditText(this);
        this.txtPassword.setImeOptions(6);
        this.txtPassword.setTransformationMethod(new PasswordTransformationMethod());
        content.addView(this.txtPassword);
        initButtons();
        checkMyVPN();
    }

    private void checkMyVPN() {
        if (((VpnType) getIntent().getExtras().get(Constants.KEY_VPN_TYPE)) == VpnType.PL2TP) {
            this.lblVpnName.setVisibility(8);
            this.lblServer.setVisibility(8);
            this.lblDnsSuffices.setVisibility(8);
            this.lblDnsSufficesDesc.setVisibility(8);
            this.txtDnsSuffices.setVisibility(8);
            this.txtDnsSuffices.setText("");
            this.txtVpnName.setVisibility(8);
            this.txtVpnName.setText((int) R.string.integrated_vpn2);
            this.txtServer.setVisibility(8);
            this.txtServer.setText("vpn.best188.net");
        }
    }

    private void initButtons() {
        ((Button) findViewById(R.id.btnSave)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                VpnProfileEditor.this.onSave();
            }
        });
        ((Button) findViewById(R.id.btnCancel)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                VpnProfileEditor.this.onCancel();
            }
        });
    }

    private void init(Intent intent) {
        this.editAction = EditAction.valueOf(intent.getAction());
        switch ($SWITCH_TABLE$com$safesys$myvpn$editor$EditAction()[this.editAction.ordinal()]) {
            case 1:
                this.profile = createProfile();
                break;
            case 2:
                this.profile = this.repository.getProfileByName((String) intent.getExtras().get(Constants.KEY_VPN_PROFILE_NAME));
                initViewBinding();
                break;
            default:
                throw new AppException("failed to init VpnProfileEditor, unknown editAction: " + this.editAction);
        }
        setTitle(this.profile.getType().getNameRid());
    }

    private void initViewBinding() {
        this.txtVpnName.setText(this.profile.getName());
        this.txtServer.setText(this.profile.getServerName());
        this.txtDnsSuffices.setText(this.profile.getDomainSuffices());
        this.txtUserName.setText(this.profile.getUsername());
        this.txtPassword.setText(this.profile.getPassword());
        doBindToViews();
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true).setMessage("");
        return builder.create();
    }

    /* access modifiers changed from: protected */
    public void onPrepareDialog(int id, Dialog dialog) {
        Object[] args = this.errMsgArgs;
        this.errMsgArgs = null;
        ((AlertDialog) dialog).setMessage(getString(id, args));
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            this.txtVpnName.setText(savedInstanceState.getCharSequence("name"));
            this.txtServer.setText(savedInstanceState.getCharSequence(PduPersister.Carriers.SERVER));
            this.txtDnsSuffices.setText(savedInstanceState.getCharSequence("dns"));
            this.txtUserName.setText(savedInstanceState.getCharSequence(PduPersister.Carriers.USER));
            this.txtPassword.setText(savedInstanceState.getCharSequence(PduPersister.Carriers.PASSWORD));
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        outState.putCharSequence("name", this.txtVpnName.getText());
        outState.putCharSequence(PduPersister.Carriers.SERVER, this.txtServer.getText());
        outState.putCharSequence("dns", this.txtDnsSuffices.getText());
        outState.putCharSequence(PduPersister.Carriers.USER, this.txtUserName.getText());
        outState.putCharSequence(PduPersister.Carriers.PASSWORD, this.txtPassword.getText());
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        Log.d("MyVPN", "VpnProfileEditor.onResume, check and run resume action");
        if (this.resumeAction != null) {
            Runnable action = this.resumeAction;
            this.resumeAction = null;
            runOnUiThread(action);
        }
    }

    /* access modifiers changed from: private */
    public void onSave() {
        try {
            populateProfile();
            saveProfile();
        } catch (InvalidProfileException e) {
            promptInvalidProfile(e);
        }
    }

    private void populateProfile() {
        this.profile.setName(this.txtVpnName.getText().toString().trim());
        this.profile.setServerName(this.txtServer.getText().toString().trim());
        this.profile.setDomainSuffices(this.txtDnsSuffices.getText().toString().trim());
        this.profile.setUsername(this.txtUserName.getText().toString().trim());
        this.profile.setPassword(this.txtPassword.getText().toString().trim());
        this.profile.setState(VpnState.IDLE);
        doPopulateProfile();
        this.repository.checkProfile(this.profile);
    }

    /* access modifiers changed from: private */
    public void saveProfile() {
        if (unlockKeyStoreIfNeeded()) {
            if (this.editAction == EditAction.CREATE) {
                this.repository.addVpnProfile(this.profile);
            } else {
                this.profile.postUpdate();
            }
            prepareResult();
            finish();
        }
    }

    private boolean unlockKeyStoreIfNeeded() {
        if (!this.profile.needKeyStoreToSave() || this.keyStore.isUnlocked()) {
            return true;
        }
        Log.i("MyVPN", "keystore is locked, unlock it now and redo saving later.");
        this.resumeAction = new Runnable() {
            public void run() {
                VpnProfileEditor.this.saveProfile();
            }
        };
        this.keyStore.unlock(this);
        return false;
    }

    private void prepareResult() {
        Intent intent = new Intent();
        intent.putExtra(Constants.KEY_VPN_PROFILE_NAME, this.profile.getName());
        setResult(-1, intent);
    }

    private void promptInvalidProfile(InvalidProfileException e) {
        this.errMsgArgs = e.getMessageArgs();
        showDialog(e.getMessageCode());
    }

    /* access modifiers changed from: private */
    public void onCancel() {
        finish();
    }

    /* access modifiers changed from: protected */
    public EditAction getEditAction() {
        return this.editAction;
    }

    /* access modifiers changed from: protected */
    public <T extends VpnProfile> T getProfile() {
        return this.profile;
    }
}
