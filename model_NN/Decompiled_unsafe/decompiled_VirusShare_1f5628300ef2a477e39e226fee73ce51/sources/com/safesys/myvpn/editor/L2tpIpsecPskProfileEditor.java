package com.safesys.myvpn.editor;

import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import com.safesys.myvpn.R;
import com.safesys.myvpn.wrapper.L2tpIpsecPskProfile;
import com.safesys.myvpn.wrapper.VpnProfile;

public class L2tpIpsecPskProfileEditor extends L2tpProfileEditor {
    private EditText txtKey;

    /* access modifiers changed from: protected */
    public void initSpecificWidgets(ViewGroup content) {
        TextView lblKey = new TextView(this);
        lblKey.setText(getString(R.string.psk));
        content.addView(lblKey);
        this.txtKey = new EditText(this);
        this.txtKey.setImeOptions(5);
        this.txtKey.setTransformationMethod(new PasswordTransformationMethod());
        content.addView(this.txtKey);
        super.initSpecificWidgets(content);
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            super.onRestoreInstanceState(savedInstanceState);
            this.txtKey.setText(savedInstanceState.getCharSequence("psk"));
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putCharSequence("psk", this.txtKey.getText());
    }

    /* access modifiers changed from: protected */
    public VpnProfile createProfile() {
        return new L2tpIpsecPskProfile(getApplicationContext());
    }

    /* access modifiers changed from: protected */
    public void doPopulateProfile() {
        ((L2tpIpsecPskProfile) getProfile()).setPresharedKey(this.txtKey.getText().toString().trim());
        super.doPopulateProfile();
    }

    /* access modifiers changed from: protected */
    public void doBindToViews() {
        this.txtKey.setText(((L2tpIpsecPskProfile) getProfile()).getPresharedKey());
        super.doBindToViews();
    }
}
