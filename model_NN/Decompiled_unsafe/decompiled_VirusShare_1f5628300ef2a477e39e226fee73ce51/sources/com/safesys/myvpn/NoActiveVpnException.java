package com.safesys.myvpn;

public class NoActiveVpnException extends AppException {
    private static final long serialVersionUID = 1;

    public NoActiveVpnException(String detailMessage) {
        super(detailMessage, R.string.err_no_active_vpn, new Object[0]);
    }

    public NoActiveVpnException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable, R.string.err_no_active_vpn, new Object[0]);
    }
}
