package com.safesys.myvpn;

public class AppException extends RuntimeException {
    private static final long serialVersionUID = 1;
    private Object[] messageArgs;
    private int messageCode;

    public AppException(String detailMessage) {
        super(detailMessage);
    }

    public AppException(String message, int msgCode, Object... msgArgs) {
        super(message);
        this.messageCode = msgCode;
        this.messageArgs = msgArgs;
    }

    public AppException(String message, Throwable throwable, int msgCode, Object... msgArgs) {
        super(message, throwable);
        this.messageCode = msgCode;
        this.messageArgs = msgArgs;
    }

    public AppException(String message, Throwable cause) {
        super(message, cause);
    }

    public int getMessageCode() {
        return this.messageCode;
    }

    public Object[] getMessageArgs() {
        return this.messageArgs;
    }
}
