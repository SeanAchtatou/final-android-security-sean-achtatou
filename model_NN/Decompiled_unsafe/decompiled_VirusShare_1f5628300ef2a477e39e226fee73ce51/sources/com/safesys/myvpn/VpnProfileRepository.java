package com.safesys.myvpn;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import com.safesys.myvpn.wrapper.InvalidProfileException;
import com.safesys.myvpn.wrapper.VpnProfile;
import com.safesys.myvpn.wrapper.VpnType;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public final class VpnProfileRepository {
    private static final String FILE_ACT_ID = "active_profile_id";
    private static final String FILE_PROFILES = "profiles";
    private static final String TAG = "MyVPN";
    private static VpnProfileRepository instance;
    private String activeProfileId;
    private Context context;
    private List<VpnProfile> profiles = new ArrayList();

    private VpnProfileRepository(Context ctx) {
        this.context = ctx;
    }

    public static VpnProfileRepository getInstance(Context ctx) {
        if (instance == null) {
            instance = new VpnProfileRepository(ctx);
            instance.load();
        }
        return instance;
    }

    public void save() {
        Log.d(TAG, "save, activeId=" + this.activeProfileId + ", profiles=" + this.profiles);
        try {
            saveActiveProfileId();
            saveProfiles();
        } catch (IOException e) {
            Log.e(TAG, "save profiles failed", e);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:9:0x001a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void saveActiveProfileId() throws java.io.IOException {
        /*
            r3 = this;
            r0 = 0
            java.io.ObjectOutputStream r1 = new java.io.ObjectOutputStream     // Catch:{ all -> 0x0017 }
            java.lang.String r2 = "active_profile_id"
            java.io.FileOutputStream r2 = r3.openPrivateFileOutput(r2)     // Catch:{ all -> 0x0017 }
            r1.<init>(r2)     // Catch:{ all -> 0x0017 }
            java.lang.String r2 = r3.activeProfileId     // Catch:{ all -> 0x001e }
            r1.writeObject(r2)     // Catch:{ all -> 0x001e }
            if (r1 == 0) goto L_0x0016
            r1.close()
        L_0x0016:
            return
        L_0x0017:
            r2 = move-exception
        L_0x0018:
            if (r0 == 0) goto L_0x001d
            r0.close()
        L_0x001d:
            throw r2
        L_0x001e:
            r2 = move-exception
            r0 = r1
            goto L_0x0018
        */
        throw new UnsupportedOperationException("Method not decompiled: com.safesys.myvpn.VpnProfileRepository.saveActiveProfileId():void");
    }

    private void saveProfiles() throws IOException {
        ObjectOutputStream os = null;
        try {
            ObjectOutputStream os2 = new ObjectOutputStream(openPrivateFileOutput(FILE_PROFILES));
            try {
                for (VpnProfile p : this.profiles) {
                    p.write(os2);
                }
                if (os2 != null) {
                    os2.close();
                }
            } catch (Throwable th) {
                th = th;
                os = os2;
            }
        } catch (Throwable th2) {
            th = th2;
            if (os != null) {
                os.close();
            }
            throw th;
        }
    }

    private FileOutputStream openPrivateFileOutput(String fileName) throws FileNotFoundException {
        return this.context.openFileOutput(fileName, 0);
    }

    private void load() {
        try {
            loadActiveProfileId();
            loadProfiles();
            Log.d(TAG, "loaded, activeId=" + this.activeProfileId + ", profiles=" + this.profiles);
        } catch (Exception e) {
            Log.e(TAG, "load profiles failed", e);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:9:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void loadActiveProfileId() throws java.io.IOException, java.lang.ClassNotFoundException {
        /*
            r4 = this;
            r0 = 0
            java.io.ObjectInputStream r1 = new java.io.ObjectInputStream     // Catch:{ all -> 0x001c }
            android.content.Context r2 = r4.context     // Catch:{ all -> 0x001c }
            java.lang.String r3 = "active_profile_id"
            java.io.FileInputStream r2 = r2.openFileInput(r3)     // Catch:{ all -> 0x001c }
            r1.<init>(r2)     // Catch:{ all -> 0x001c }
            java.lang.Object r2 = r1.readObject()     // Catch:{ all -> 0x0023 }
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ all -> 0x0023 }
            r4.activeProfileId = r2     // Catch:{ all -> 0x0023 }
            if (r1 == 0) goto L_0x001b
            r1.close()
        L_0x001b:
            return
        L_0x001c:
            r2 = move-exception
        L_0x001d:
            if (r0 == 0) goto L_0x0022
            r0.close()
        L_0x0022:
            throw r2
        L_0x0023:
            r2 = move-exception
            r0 = r1
            goto L_0x001d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.safesys.myvpn.VpnProfileRepository.loadActiveProfileId():void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:9:0x001a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void loadProfiles() throws java.lang.Exception {
        /*
            r4 = this;
            r0 = 0
            java.io.ObjectInputStream r1 = new java.io.ObjectInputStream     // Catch:{ all -> 0x0017 }
            android.content.Context r2 = r4.context     // Catch:{ all -> 0x0017 }
            java.lang.String r3 = "profiles"
            java.io.FileInputStream r2 = r2.openFileInput(r3)     // Catch:{ all -> 0x0017 }
            r1.<init>(r2)     // Catch:{ all -> 0x0017 }
            r4.loadProfilesFrom(r1)     // Catch:{ all -> 0x001e }
            if (r1 == 0) goto L_0x0016
            r1.close()
        L_0x0016:
            return
        L_0x0017:
            r2 = move-exception
        L_0x0018:
            if (r0 == 0) goto L_0x001d
            r0.close()
        L_0x001d:
            throw r2
        L_0x001e:
            r2 = move-exception
            r0 = r1
            goto L_0x0018
        */
        throw new UnsupportedOperationException("Method not decompiled: com.safesys.myvpn.VpnProfileRepository.loadProfiles():void");
    }

    private void loadProfilesFrom(ObjectInputStream is) throws Exception {
        while (true) {
            try {
                loadProfileObject((VpnType) is.readObject(), is.readObject(), is);
            } catch (EOFException e) {
                Log.d(TAG, "reach the end of profiles file");
                return;
            }
        }
    }

    private void loadProfileObject(VpnType type, Object obj, ObjectInputStream is) throws Exception {
        if (obj != null) {
            VpnProfile p = VpnProfile.newInstance(type, this.context);
            if (p.isCompatible(obj)) {
                p.read(obj, is);
                this.profiles.add(p);
                return;
            }
            Log.e(TAG, "saved profile '" + obj + "' is NOT compatible with " + type);
        }
    }

    public void setActiveProfile(VpnProfile profile) {
        Log.i(TAG, "active vpn set to: " + profile);
        this.activeProfileId = profile.getId();
    }

    public String getActiveProfileId() {
        return this.activeProfileId;
    }

    public VpnProfile getActiveProfile() {
        if (this.activeProfileId == null) {
            return null;
        }
        return getProfileById(this.activeProfileId);
    }

    private VpnProfile getProfileById(String id) {
        for (VpnProfile p : this.profiles) {
            if (p.getId().equals(id)) {
                return p;
            }
        }
        return null;
    }

    public VpnProfile getProfileByName(String name) {
        for (VpnProfile p : this.profiles) {
            if (p.getName().equals(name)) {
                return p;
            }
        }
        return null;
    }

    public List<VpnProfile> getAllVpnProfiles() {
        return Collections.unmodifiableList(this.profiles);
    }

    public void addVpnProfile(VpnProfile p) {
        p.postConstruct();
        this.profiles.add(p);
    }

    public void checkProfile(VpnProfile newProfile) {
        String newName = newProfile.getName();
        if (TextUtils.isEmpty(newName)) {
            throw new InvalidProfileException("profile name is empty.", R.string.err_empty_name, new Object[0]);
        }
        for (VpnProfile p : this.profiles) {
            if (newProfile != p && newName.equals(p.getName())) {
                throw new InvalidProfileException("duplicated profile name '" + newName + "'.", R.string.err_duplicated_profile_name, newName);
            }
        }
        newProfile.validate();
    }

    public void deleteVpnProfile(VpnProfile profile) {
        String id = profile.getId();
        Log.d(TAG, "delete vpn: " + profile + ", removed=" + this.profiles.remove(profile));
        if (id.equals(this.activeProfileId)) {
            this.activeProfileId = null;
            Log.d(TAG, "deactivate vpn: " + profile);
        }
    }

    public void backup(String path) {
        if (this.profiles.isEmpty()) {
            Log.i(TAG, "profile list is empty, will not export");
            return;
        }
        save();
        File dir = ensureDir(path);
        try {
            doBackup(dir, FILE_ACT_ID);
            doBackup(dir, FILE_PROFILES);
        } catch (Exception e) {
            throw new AppException("backup failed", e, R.string.err_exp_failed, new Object[0]);
        }
    }

    private File ensureDir(String path) {
        File dir = new File(path);
        Utils.ensureDir(dir);
        return dir;
    }

    private void doBackup(File dir, String name) throws Exception {
        StreamCrypto.encrypt(this.context.openFileInput(name), new FileOutputStream(new File(dir, name)));
    }

    public void restore(String dir) {
        checkExternalData(dir);
        try {
            doRestore(dir, FILE_ACT_ID);
            doRestore(dir, FILE_PROFILES);
            clean();
            load();
        } catch (Exception e) {
            throw new AppException("restore failed", e, R.string.err_imp_failed, new Object[0]);
        }
    }

    private void clean() {
        this.activeProfileId = null;
        this.profiles.clear();
    }

    private void doRestore(String dir, String name) throws Exception {
        StreamCrypto.decrypt(new FileInputStream(new File(dir, name)), openPrivateFileOutput(name));
    }

    private void checkExternalData(String path) {
        File id = new File(path, FILE_ACT_ID);
        File profiles2 = new File(path, FILE_PROFILES);
        if (!verifyDataFile(id) || !verifyDataFile(profiles2)) {
            throw new AppException("no valid data found in: " + path, R.string.err_imp_nodata, new Object[0]);
        }
    }

    private boolean verifyDataFile(File file) {
        return file.exists() && file.isFile() && file.length() > 0;
    }

    public Date checkLastBackup(String path) {
        File id = new File(path, FILE_ACT_ID);
        if (!verifyDataFile(id)) {
            return null;
        }
        return new Date(id.lastModified());
    }
}
