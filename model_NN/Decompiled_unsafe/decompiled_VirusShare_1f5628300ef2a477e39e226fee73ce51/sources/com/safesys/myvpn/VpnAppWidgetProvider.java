package com.safesys.myvpn;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class VpnAppWidgetProvider extends AppWidgetProvider {
    private static final String TAG = "MyVPN";

    public void onEnabled(Context context) {
        super.onEnabled(context);
        Log.d(TAG, "VpnAppWidgetProvider enabled");
        context.startService(new Intent(context, VpnConnectorService.class));
    }

    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        Log.d(TAG, "VpnAppWidgetProvider onUpdate");
        context.startService(new Intent(context, VpnConnectorService.class));
    }

    public void onDisabled(Context context) {
        Log.d(TAG, "VpnAppWidgetProvider onDisabled");
        context.stopService(new Intent(context, VpnConnectorService.class));
        super.onDisabled(context);
    }
}
