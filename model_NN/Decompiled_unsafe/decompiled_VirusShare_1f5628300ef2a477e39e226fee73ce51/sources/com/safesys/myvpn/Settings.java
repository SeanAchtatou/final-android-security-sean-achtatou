package com.safesys.myvpn;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.PreferenceActivity;

public class Settings extends PreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener {
    private ListPreference periodList;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.pref);
        this.periodList = (ListPreference) getPreferenceScreen().findPreference(KeepAlive.PREF_HEARTBEAT_PERIOD);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        CharSequence period = this.periodList.getEntry();
        this.periodList.setSummary(getString(R.string.keepalive_period_sum, new Object[]{period}));
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }

    public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
        if (key.equals(KeepAlive.PREF_HEARTBEAT_PERIOD)) {
            CharSequence period = this.periodList.getEntry();
            this.periodList.setSummary(getString(R.string.keepalive_period_sum, new Object[]{period}));
        }
    }
}
