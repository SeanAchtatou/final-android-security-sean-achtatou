package com.safesys.myvpn;

public final class Constants {
    public static final String ACTION_VPN_CONNECTIVITY = "vpn.connectivity";
    public static final String ACT_ADD_VPN = "myvpn.addVpnAction";
    public static final String ACT_TOGGLE_VPN_CONN = "myvpn.toggleVpnConnectionAction";
    public static final String ACT_VPN_SETTINGS = "android.net.vpn.SETTINGS";
    public static final String BROADCAST_CONNECTION_STATE = "connection_state";
    public static final String BROADCAST_ERROR_CODE = "err";
    public static final String BROADCAST_PROFILE_NAME = "profile_name";
    public static final String CAT_DEFAULT = "android.intent.category.DEFAULT";
    public static final int DLG_ABOUT = 2;
    public static final int DLG_BACKUP = 3;
    public static final int DLG_HACK = 5;
    public static final int DLG_RESTORE = 4;
    public static final int DLG_SU = 6;
    public static final int DLG_VPN_PROFILE_ALERT = 1;
    public static final String EXP_DIR_REGEX = "\\d{6}\\-\\d{6}";
    public static final String KEY_VPN_PROFILE_ID = "vpnProfileId";
    public static final String KEY_VPN_PROFILE_NAME = "vpnProfileName";
    public static final String KEY_VPN_STATE = "activeVpnState";
    public static final String KEY_VPN_TYPE = "vpnType";
    public static final int REQ_ADD_VPN = 2;
    public static final int REQ_EDIT_VPN = 3;
    public static final int REQ_SELECT_VPN_TYPE = 1;
    public static final int VPN_ERROR_AUTH = 51;
    public static final int VPN_ERROR_CHALLENGE = 5;
    public static final int VPN_ERROR_CONNECTION_FAILED = 101;
    public static final int VPN_ERROR_CONNECTION_LOST = 103;
    public static final int VPN_ERROR_LARGEST = 200;
    public static final int VPN_ERROR_NO_ERROR = 0;
    public static final int VPN_ERROR_PPP_NEGOTIATION_FAILED = 42;
    public static final int VPN_ERROR_REMOTE_HUNG_UP = 7;
    public static final int VPN_ERROR_REMOTE_PPP_HUNG_UP = 48;
    public static final int VPN_ERROR_UNKNOWN_SERVER = 102;

    private Constants() {
    }
}
