package com.safesys.myvpn;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.ConditionVariable;
import android.os.IBinder;
import android.util.Log;
import com.safesys.myvpn.wrapper.VpnManager;
import com.safesys.myvpn.wrapper.VpnProfile;
import com.safesys.myvpn.wrapper.VpnService;
import com.safesys.myvpn.wrapper.VpnState;

public class VpnActor {
    private static final int ONE_SEC = 1000;
    private static final String TAG = "MyVPN";
    /* access modifiers changed from: private */
    public Context context;
    private VpnProfileRepository repository;
    private VpnManager vpnMgr;
    private VpnService vpnSrv;

    public VpnActor(Context ctx) {
        this.context = ctx;
    }

    public void connect() {
        VpnProfile p = getRepository().getActiveProfile();
        if (p == null) {
            throw new NoActiveVpnException("connect failed, no active vpn");
        }
        connect(p);
    }

    public void connect(VpnProfile p) {
        Log.i(TAG, "connect to: " + p);
        p.preConnect();
        final VpnProfile cp = p.dulicateToConnect();
        getVpnMgr().startVpnService();
        if (!getVpnMgr().bindVpnService(new ServiceConnection() {
            public void onServiceConnected(ComponentName className, IBinder service) {
                try {
                    if (!VpnActor.this.getVpnSrv().connect(service, cp)) {
                        Log.d(VpnActor.TAG, "~~~~~~ connect() failed!");
                        VpnActor.this.broadcastConnectivity(cp.getName(), VpnState.IDLE, Constants.VPN_ERROR_CONNECTION_FAILED);
                    } else {
                        Log.d(VpnActor.TAG, "~~~~~~ connect() succeeded!");
                    }
                } catch (Throwable th) {
                    Log.e(VpnActor.TAG, "connect()", th);
                    VpnActor.this.broadcastConnectivity(cp.getName(), VpnState.IDLE, Constants.VPN_ERROR_CONNECTION_FAILED);
                } finally {
                    VpnActor.this.context.unbindService(this);
                }
            }

            public void onServiceDisconnected(ComponentName className) {
                Log.e(VpnActor.TAG, "onServiceDisconnected");
                VpnActor.this.checkStatus();
            }
        })) {
            Log.e(TAG, "bind service failed");
            broadcastConnectivity(cp.getName(), VpnState.IDLE, Constants.VPN_ERROR_CONNECTION_FAILED);
        }
    }

    public void disconnect() {
        Log.i(TAG, "disconnect active vpn");
        if (!getVpnMgr().bindVpnService(new ServiceConnection() {
            public void onServiceConnected(ComponentName className, IBinder service) {
                try {
                    VpnActor.this.getVpnSrv().disconnect(service);
                } catch (Exception e) {
                    Log.e(VpnActor.TAG, "disconnect()", e);
                    VpnActor.this.checkStatus();
                } finally {
                    VpnActor.this.context.unbindService(this);
                }
            }

            public void onServiceDisconnected(ComponentName className) {
                Log.e(VpnActor.TAG, "onServiceDisconnected");
                VpnActor.this.checkStatus();
            }
        })) {
            Log.e(TAG, "bind service failed");
            checkStatus();
        }
    }

    public void checkStatus() {
        VpnProfile p = getRepository().getActiveProfile();
        if (p != null) {
            checkStatus(p);
        }
    }

    private void checkStatus(final VpnProfile p) {
        Log.i(TAG, "check status of vpn: " + p);
        final ConditionVariable cv = new ConditionVariable();
        cv.close();
        if (getVpnMgr().bindVpnService(new ServiceConnection() {
            public synchronized void onServiceConnected(ComponentName className, IBinder service) {
                cv.open();
                try {
                    VpnActor.this.getVpnSrv().checkStatus(service, p);
                    VpnActor.this.context.unbindService(this);
                } catch (Exception e) {
                    Log.e(VpnActor.TAG, "checkStatus()", e);
                    VpnActor.this.broadcastConnectivity(p.getName(), VpnState.IDLE, 0);
                    VpnActor.this.context.unbindService(this);
                } catch (Throwable th) {
                    VpnActor.this.context.unbindService(this);
                    throw th;
                }
                return;
            }

            public void onServiceDisconnected(ComponentName className) {
                cv.open();
                VpnActor.this.broadcastConnectivity(p.getName(), VpnState.IDLE, 0);
                VpnActor.this.context.unbindService(this);
            }
        }) && !cv.block(1000)) {
            broadcastConnectivity(p.getName(), VpnState.IDLE, 0);
        }
    }

    public void checkAllStatus() {
        for (VpnProfile p : getRepository().getAllVpnProfiles()) {
            checkStatus(p);
        }
    }

    private VpnProfileRepository getRepository() {
        if (this.repository == null) {
            this.repository = VpnProfileRepository.getInstance(this.context);
        }
        return this.repository;
    }

    private VpnManager getVpnMgr() {
        if (this.vpnMgr == null) {
            this.vpnMgr = new VpnManager(this.context);
        }
        return this.vpnMgr;
    }

    /* access modifiers changed from: private */
    public VpnService getVpnSrv() {
        if (this.vpnSrv == null) {
            this.vpnSrv = new VpnService(this.context);
        }
        return this.vpnSrv;
    }

    public void activate(VpnProfile p) {
        getRepository().setActiveProfile(p);
        broadcastConnectivity(p.getName(), p.getState(), 0);
    }

    public void broadcastConnectivity(String profileName, VpnState s, int error) {
        Intent intent = new Intent(Constants.ACTION_VPN_CONNECTIVITY);
        intent.putExtra(Constants.BROADCAST_PROFILE_NAME, profileName);
        intent.putExtra(Constants.BROADCAST_CONNECTION_STATE, s);
        if (error != 0) {
            intent.putExtra(Constants.BROADCAST_ERROR_CODE, error);
        }
        this.context.sendBroadcast(intent);
    }
}
