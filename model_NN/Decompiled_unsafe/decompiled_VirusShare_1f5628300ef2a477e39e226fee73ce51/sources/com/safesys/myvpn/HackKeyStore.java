package com.safesys.myvpn;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.Toast;
import com.safesys.myvpn.wrapper.KeyStore;

public class HackKeyStore {
    private static final String PREF_IGNORE_HACK = "com.safesys.myvpn.pref.ignoreHack";
    private static final String TAG = "MyVPN.HackKeyStore";
    private Activity activity;
    private KeyStore keyStore;
    private SharedPreferences pref;

    public HackKeyStore(Activity activity2) {
        this.activity = activity2;
        this.pref = activity2.getPreferences(0);
        this.keyStore = new KeyStore(activity2);
    }

    public void check(boolean force) {
        boolean ignoreHack = this.pref.getBoolean(PREF_IGNORE_HACK, false);
        if (force || !ignoreHack) {
            boolean hacked = this.keyStore.isHacked();
            Log.d(TAG, String.format("check keytore, hacked=%1$s, ignore=%2$s", Boolean.valueOf(hacked), Boolean.valueOf(ignoreHack)));
            if (hacked && force) {
                Toast.makeText(this.activity, (int) R.string.hacked, 0).show();
            }
            if (!hacked) {
                promoptHack(ignoreHack);
            }
        }
    }

    private void promoptHack(boolean ignoreHack) {
        View layout = this.activity.getLayoutInflater().inflate((int) R.layout.hack, (ViewGroup) this.activity.findViewById(R.id.hackRoot));
        AlertDialog.Builder builder = new AlertDialog.Builder(this.activity);
        builder.setView(layout).setTitle(this.activity.getString(R.string.hack_keystore)).setIcon(17301659);
        builder.setCancelable(true);
        builder.setPositiveButton(17039370, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        final CheckBox chkIgnore = (CheckBox) layout.findViewById(R.id.chkIgnoreHack);
        chkIgnore.setChecked(ignoreHack);
        AlertDialog dlg = builder.create();
        dlg.setOwnerActivity(this.activity);
        dlg.setOnDismissListener(new DialogInterface.OnDismissListener() {
            public void onDismiss(DialogInterface dialog) {
                HackKeyStore.this.savePref(chkIgnore.isChecked());
            }
        });
        dlg.show();
    }

    /* access modifiers changed from: private */
    public void savePref(boolean isChecked) {
        Log.d(TAG, "com.safesys.myvpn.pref.ignoreHack->" + isChecked);
        SharedPreferences.Editor editor = this.pref.edit();
        editor.putBoolean(PREF_IGNORE_HACK, isChecked);
        editor.commit();
    }
}
