package com.safesys.myvpn.wrapper;

import com.safesys.myvpn.AppException;

public class InvalidProfileException extends AppException {
    private static final long serialVersionUID = 1;

    public InvalidProfileException(String detailMessage, int messageResourceId, Object... messageArgs) {
        super(detailMessage, messageResourceId, messageArgs);
    }

    public InvalidProfileException(String detailMessage, Throwable throwable, int messageResourceId, Object... messageArgs) {
        super(detailMessage, throwable, messageResourceId, messageArgs);
    }
}
