package com.safesys.myvpn.wrapper;

import com.safesys.myvpn.AppException;

public class WrapperException extends AppException {
    private static final long serialVersionUID = 1;

    public WrapperException(String detailMessage) {
        super(detailMessage, 0, new Object[0]);
    }

    public WrapperException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }
}
