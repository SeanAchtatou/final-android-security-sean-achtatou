package com.safesys.myvpn.wrapper;

import android.content.Context;
import android.content.ServiceConnection;
import com.safesys.myvpn.wrapper.AbstractWrapper;

public class VpnManager extends AbstractWrapper {
    public static final String METHOD_BIND_VPN_SERVICE = "bindVpnService";
    public static final String METHOD_START_VPN_SERVICE = "startVpnService";
    public static final String METHOD_STOP_VPN_SERVICE = "stopVpnService";

    public VpnManager(Context ctx) {
        super(ctx, "android.net.vpn.VpnManager", new AbstractWrapper.StubInstanceCreator() {
            /* access modifiers changed from: protected */
            public Object newStubInstance(Class<?> stubClass, Context ctx) throws Exception {
                return stubClass.getConstructor(Context.class).newInstance(ctx);
            }
        });
    }

    public void startVpnService() {
        invokeStubMethod(METHOD_START_VPN_SERVICE, new Object[0]);
    }

    public void stopVpnService() {
        invokeStubMethod(METHOD_STOP_VPN_SERVICE, new Object[0]);
    }

    public boolean bindVpnService(ServiceConnection c) {
        try {
            return ((Boolean) getStubClass().getMethod(METHOD_BIND_VPN_SERVICE, ServiceConnection.class).invoke(getStub(), c)).booleanValue();
        } catch (Throwable th) {
            throw new WrapperException("bindVpnService failed", th);
        }
    }
}
