package com.safesys.myvpn.wrapper;

public enum VpnState {
    CONNECTING,
    DISCONNECTING,
    CANCELLED,
    CONNECTED,
    IDLE,
    UNUSABLE,
    UNKNOWN
}
