package com.safesys.myvpn.wrapper;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import com.safesys.myvpn.AppException;
import com.safesys.myvpn.R;

public class L2tpProfile extends VpnProfile {
    public static final String KEY_PREFIX_L2TP_SECRET = "VPN_l";
    private KeyStore keyStore;

    protected L2tpProfile(Context ctx, String stubClass) {
        super(ctx, stubClass);
    }

    public L2tpProfile(Context ctx) {
        super(ctx, "android.net.vpn.L2tpProfile");
    }

    public VpnType getType() {
        return VpnType.L2TP;
    }

    public void setSecretEnabled(boolean enabled) {
        try {
            getStubClass().getMethod("setSecretEnabled", Boolean.TYPE).invoke(getStub(), Boolean.valueOf(enabled));
        } catch (Throwable th) {
            throw new AppException("setSecretEnabled failed", th);
        }
    }

    public boolean isSecretEnabled() {
        return ((Boolean) invokeStubMethod("isSecretEnabled", new Object[0])).booleanValue();
    }

    public void setSecretString(String secret) {
        invokeStubMethod("setSecretString", secret);
    }

    public String getSecretString() {
        return (String) invokeStubMethod("getSecretString", new Object[0]);
    }

    public void validate() {
        super.validate();
        if (isSecretEnabled() && TextUtils.isEmpty(getSecretString())) {
            throw new InvalidProfileException("secret is empty", R.string.err_empty_secret, new Object[0]);
        }
    }

    public void postConstruct() {
        super.postConstruct();
        processSecret();
    }

    public void postUpdate() {
        super.postUpdate();
        processSecret();
    }

    public void preConnect() {
        super.preConnect();
        processSecret();
    }

    /* access modifiers changed from: protected */
    public void processSecret() {
        String key = makeKey();
        if (isSecretEnabled()) {
            if (!getKeyStore().put(key, getSecretString())) {
                Log.e("MyVPN", "keystore write failed: key=" + key);
                return;
            }
            return;
        }
        getKeyStore().delete(key);
    }

    private String makeKey() {
        return KEY_PREFIX_L2TP_SECRET + getId();
    }

    public boolean needKeyStoreToSave() {
        return isSecretEnabled() && !TextUtils.isEmpty(getSecretString());
    }

    public boolean needKeyStoreToConnect() {
        return isSecretEnabled();
    }

    /* access modifiers changed from: protected */
    public KeyStore getKeyStore() {
        if (this.keyStore == null) {
            this.keyStore = new KeyStore(getContext());
        }
        return this.keyStore;
    }

    public L2tpProfile dulicateToConnect() {
        L2tpProfile p = (L2tpProfile) super.dulicateToConnect();
        boolean secretEnabled = isSecretEnabled();
        p.setSecretEnabled(secretEnabled);
        if (secretEnabled) {
            p.setSecretString(makeKey());
        }
        return p;
    }
}
