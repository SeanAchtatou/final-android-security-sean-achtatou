package com.safesys.myvpn.wrapper;

import android.content.Context;
import android.content.pm.PackageManager;
import dalvik.system.PathClassLoader;
import java.lang.reflect.Method;

public abstract class AbstractWrapper implements Cloneable {
    private static final String STUB_PACK = "com.android.settings";
    private static PathClassLoader stubClassLoader;
    private transient Context context;
    private Object stub;
    private Class<?> stubClass;
    private String stubClassName;
    private StubInstanceCreator stubInstanceCreator;

    protected static class StubInstanceCreator {
        protected StubInstanceCreator() {
        }

        /* access modifiers changed from: protected */
        public Object newStubInstance(Class<?> stubClass, Context context) throws Exception {
            return stubClass.newInstance();
        }
    }

    protected AbstractWrapper(Context ctx, String stubClass2) {
        this.context = ctx;
        this.stubClassName = stubClass2;
        this.stubInstanceCreator = new StubInstanceCreator();
        init();
    }

    protected AbstractWrapper(Context ctx, String stubClass2, StubInstanceCreator stubCreator) {
        this.context = ctx;
        this.stubClassName = stubClass2;
        this.stubInstanceCreator = stubCreator;
        init();
    }

    public Context getContext() {
        return this.context;
    }

    public String getStubClassName() {
        return this.stubClassName;
    }

    public Class<?> getStubClass() {
        return this.stubClass;
    }

    public void setStub(Object stub2) {
        this.stub = stub2;
    }

    public Object getStub() {
        return this.stub;
    }

    private void init() {
        try {
            initClassLoader(this.context);
            this.stubClass = loadClass(this.stubClassName);
            this.stub = this.stubInstanceCreator.newStubInstance(this.stubClass, this.context);
        } catch (Exception e) {
            throw new WrapperException("init classloader failed", e);
        }
    }

    private static void initClassLoader(Context ctx) throws PackageManager.NameNotFoundException {
        if (stubClassLoader == null) {
            stubClassLoader = new PathClassLoader(ctx.getPackageManager().getApplicationInfo(STUB_PACK, 0).sourceDir, ClassLoader.getSystemClassLoader());
        }
    }

    protected static final Class<?> loadClass(String qname) throws ClassNotFoundException {
        return Class.forName(qname, true, stubClassLoader);
    }

    /* access modifiers changed from: protected */
    public <T> T invokeStubMethod(String methodName, Object... args) {
        try {
            return findStubMethod(methodName, args).invoke(this.stub, args);
        } catch (Exception e) {
            throw new WrapperException("failed to invoke mehod '" + methodName + "' on stub", e);
        }
    }

    /* access modifiers changed from: protected */
    public Method findStubMethod(String methodName, Object... args) throws NoSuchMethodException {
        return findMethod(this.stubClass, methodName, args);
    }

    /* access modifiers changed from: protected */
    public Method findMethod(Class<?> clazz, String methodName, Object... args) throws NoSuchMethodException {
        Class[] argTypes = new Class[args.length];
        int length = args.length;
        int i = 0;
        int i2 = 0;
        while (i < length) {
            argTypes[i2] = args[i].getClass();
            i++;
            i2++;
        }
        Method method = clazz.getMethod(methodName, argTypes);
        method.setAccessible(true);
        return method;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v2, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: com.safesys.myvpn.wrapper.AbstractWrapper} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.safesys.myvpn.wrapper.AbstractWrapper clone() {
        /*
            r5 = this;
            r1 = 0
            java.lang.Object r3 = super.clone()     // Catch:{ CloneNotSupportedException -> 0x000d }
            r0 = r3
            com.safesys.myvpn.wrapper.AbstractWrapper r0 = (com.safesys.myvpn.wrapper.AbstractWrapper) r0     // Catch:{ CloneNotSupportedException -> 0x000d }
            r1 = r0
            r1.init()     // Catch:{ CloneNotSupportedException -> 0x000d }
        L_0x000c:
            return r1
        L_0x000d:
            r3 = move-exception
            r2 = r3
            java.lang.String r3 = "MyVPN"
            java.lang.String r4 = "clone failed"
            android.util.Log.e(r3, r4, r2)
            goto L_0x000c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.safesys.myvpn.wrapper.AbstractWrapper.clone():com.safesys.myvpn.wrapper.AbstractWrapper");
    }
}
