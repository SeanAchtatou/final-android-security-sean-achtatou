package com.safesys.myvpn.wrapper;

import com.safesys.myvpn.R;
import com.safesys.myvpn.editor.L2tpIpsecPskProfileEditor;
import com.safesys.myvpn.editor.L2tpProfileEditor;
import com.safesys.myvpn.editor.PptpProfileEditor;
import com.safesys.myvpn.editor.VpnProfileEditor;

public enum VpnType {
    PL2TP("L2TP", R.string.vpn_myvpn, R.string.vpn_myvpn_info, L2tpProfile.class, L2tpProfileEditor.class),
    PPTP("PPTP", R.string.vpn_pptp, R.string.vpn_pptp_info, PptpProfile.class, PptpProfileEditor.class),
    L2TP("L2TP", R.string.vpn_l2tp, R.string.vpn_l2tp_info, L2tpProfile.class, L2tpProfileEditor.class),
    L2TP_IPSEC_PSK("L2TP/IPSec PSK", R.string.vpn_l2tp_psk, R.string.vpn_l2tp_psk_info, L2tpIpsecPskProfile.class, L2tpIpsecPskProfileEditor.class);
    
    private boolean active;
    private Class<? extends VpnProfile> clazz;
    private int descRid;
    private Class<? extends VpnProfileEditor> editorClass;
    private String name;
    private int nameRid;

    private VpnType(String name2, int nameRid2, int descRid2, Class<? extends VpnProfile> clazz2, Class<? extends VpnProfileEditor> editorClass2) {
        this.name = name2;
        this.nameRid = nameRid2;
        this.descRid = descRid2;
        this.clazz = clazz2;
        this.editorClass = editorClass2;
    }

    public String getName() {
        return this.name;
    }

    public Class<? extends VpnProfile> getProfileClass() {
        return this.clazz;
    }

    public boolean isActive() {
        return this.active;
    }

    public void setActive(boolean a) {
        this.active = a;
    }

    public int getNameRid() {
        return this.nameRid;
    }

    public int getDescRid() {
        return this.descRid;
    }

    public Class<? extends VpnProfileEditor> getEditorClass() {
        return this.editorClass;
    }
}
