package com.safesys.myvpn.wrapper;

import android.content.Context;
import android.os.IBinder;
import com.safesys.myvpn.wrapper.AbstractWrapper;

public class VpnService extends AbstractWrapper {
    public VpnService(Context ctx) {
        super(ctx, "android.net.vpn.IVpnService$Stub", new AbstractWrapper.StubInstanceCreator() {
            /* access modifiers changed from: protected */
            public Object newStubInstance(Class<?> cls, Context ctx) throws Exception {
                return null;
            }
        });
    }

    public boolean connect(IBinder service, VpnProfile profile) throws Exception {
        asInterface(service);
        return ((Boolean) getStubClass().getMethod("connect", profile.getGenericProfileClass(), String.class, String.class).invoke(getStub(), profile.getStub(), profile.getUsername(), profile.getPassword())).booleanValue();
    }

    public void disconnect(IBinder service) throws Exception {
        asInterface(service);
        invokeStubMethod("disconnect", new Object[0]);
    }

    public void checkStatus(IBinder service, VpnProfile p) throws Exception {
        asInterface(service);
        getStubClass().getMethod("checkStatus", p.getGenericProfileClass()).invoke(getStub(), p.getStub());
    }

    private void asInterface(IBinder service) throws Exception {
        setStub(getStubClass().getMethod("asInterface", IBinder.class).invoke(null, service));
    }
}
