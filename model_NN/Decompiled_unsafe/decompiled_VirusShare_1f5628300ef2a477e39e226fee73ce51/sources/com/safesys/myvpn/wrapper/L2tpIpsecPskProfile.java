package com.safesys.myvpn.wrapper;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import com.safesys.myvpn.R;

public class L2tpIpsecPskProfile extends L2tpProfile {
    public static final String KEY_PREFIX_IPSEC_PSK = "VPN_i";

    public L2tpIpsecPskProfile(Context ctx) {
        super(ctx, "android.net.vpn.L2tpIpsecPskProfile");
    }

    public VpnType getType() {
        return VpnType.L2TP_IPSEC_PSK;
    }

    public void setPresharedKey(String key) {
        invokeStubMethod("setPresharedKey", key);
    }

    public String getPresharedKey() {
        return (String) invokeStubMethod("getPresharedKey", new Object[0]);
    }

    public void validate() {
        super.validate();
        if (TextUtils.isEmpty(getPresharedKey())) {
            throw new InvalidProfileException("presharedKey is empty", R.string.err_empty_psk, new Object[0]);
        }
    }

    /* access modifiers changed from: protected */
    public void processSecret() {
        super.processSecret();
        String psk = getPresharedKey();
        String key = makeKey();
        if (!getKeyStore().put(key, psk)) {
            Log.e("MyVPN", "keystore write failed: key=" + key);
        }
    }

    private String makeKey() {
        return KEY_PREFIX_IPSEC_PSK + getId();
    }

    public boolean needKeyStoreToSave() {
        return super.needKeyStoreToSave() || !TextUtils.isEmpty(getPresharedKey());
    }

    public boolean needKeyStoreToConnect() {
        return true;
    }

    public L2tpIpsecPskProfile dulicateToConnect() {
        L2tpIpsecPskProfile p = (L2tpIpsecPskProfile) super.dulicateToConnect();
        p.setPresharedKey(makeKey());
        return p;
    }
}
