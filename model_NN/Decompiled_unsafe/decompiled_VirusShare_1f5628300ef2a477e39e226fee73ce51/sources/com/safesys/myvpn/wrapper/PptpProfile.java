package com.safesys.myvpn.wrapper;

import android.content.Context;
import com.safesys.myvpn.AppException;

public class PptpProfile extends VpnProfile {
    public PptpProfile(Context ctx) {
        super(ctx, "android.net.vpn.PptpProfile");
    }

    public VpnType getType() {
        return VpnType.PPTP;
    }

    public void setEncryptionEnabled(boolean enabled) {
        try {
            getStubClass().getMethod("setEncryptionEnabled", Boolean.TYPE).invoke(getStub(), Boolean.valueOf(enabled));
        } catch (Throwable th) {
            throw new AppException("setEncryptionEnabled failed", th);
        }
    }

    public boolean isEncryptionEnabled() {
        return ((Boolean) invokeStubMethod("isEncryptionEnabled", new Object[0])).booleanValue();
    }
}
