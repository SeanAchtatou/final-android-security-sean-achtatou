package com.safesys.myvpn.wrapper;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.safesys.myvpn.AppException;
import com.safesys.myvpn.wrapper.AbstractWrapper;
import java.lang.reflect.Method;

public class KeyStore extends AbstractWrapper {
    private static final int NO_ERROR = 1;
    public static final String UNLOCK_ACTION = "android.credentials.UNLOCK";

    public KeyStore(Context ctx) {
        super(ctx, "android.security.KeyStore", new AbstractWrapper.StubInstanceCreator() {
            /* access modifiers changed from: protected */
            public Object newStubInstance(Class<?> stubClass, Context ctx) throws Exception {
                return stubClass.getMethod("getInstance", new Class[0]).invoke(null, new Object[0]);
            }
        });
    }

    public boolean put(String key, String value) {
        return ((Boolean) invokeStubMethod("put", key, value)).booleanValue();
    }

    public boolean contains(VpnProfile p) {
        return ((Boolean) invokeStubMethod("contains", ((String) L2tpIpsecPskProfile.KEY_PREFIX_IPSEC_PSK) + p.getId())).booleanValue();
    }

    public boolean delete(String key) {
        return ((Boolean) invokeStubMethod("delete", key)).booleanValue();
    }

    public void unlock(Activity ctx) {
        try {
            ctx.startActivity(new Intent(UNLOCK_ACTION));
        } catch (ActivityNotFoundException e) {
            Log.e("MyVPN", "unlock credentials failed", e);
        }
    }

    public boolean isUnlocked() {
        int err = ((Integer) invokeStubMethod("test", new Object[0])).intValue();
        Log.d("MyVPN", "KeyStore.test result is: " + err);
        return err == 1;
    }

    /* Debug info: failed to restart local var, previous not found, register: 10 */
    public boolean isHacked() {
        try {
            Method m = getStubClass().getDeclaredMethod("execute", Integer.TYPE, byte[][].class);
            m.setAccessible(true);
            m.invoke(getStub(), 'x', new byte[0][]);
            int err = ((Integer) invokeStubMethod("getLastError", new Object[0])).intValue();
            Log.d("MyVPN", "cmd 'x' result=" + err);
            if (err == 1) {
                return true;
            }
            return false;
        } catch (Throwable th) {
            throw new AppException("verify failed", th);
        }
    }
}
