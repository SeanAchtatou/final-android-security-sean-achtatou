package com.safesys.myvpn.wrapper;

import android.content.Context;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.UUID;

public abstract class VpnProfile extends AbstractWrapper {
    private String password;
    private VpnState state = VpnState.UNKNOWN;
    private String username;

    public abstract VpnType getType();

    protected VpnProfile(Context ctx, String stubClass) {
        super(ctx, stubClass);
    }

    public static VpnProfile newInstance(VpnType vpnType, Context ctx) {
        Class<? extends VpnProfile> profileClass = vpnType.getProfileClass();
        if (profileClass == null) {
            throw new IllegalArgumentException("profile class is null for " + vpnType);
        }
        try {
            return (VpnProfile) profileClass.getConstructor(Context.class).newInstance(ctx);
        } catch (Exception e) {
            throw new WrapperException("failed to create instance for " + vpnType, e);
        }
    }

    public void postConstruct() {
        setId(UUID.randomUUID().toString());
    }

    public void postUpdate() {
    }

    public void validate() {
    }

    public void preConnect() {
    }

    public void setId(String id) {
        invokeStubMethod("setId", id);
    }

    public String getId() {
        return (String) invokeStubMethod("getId", new Object[0]);
    }

    public void setName(String name) {
        invokeStubMethod("setName", name);
    }

    public String getName() {
        return (String) invokeStubMethod("getName", new Object[0]);
    }

    public void setServerName(String name) {
        invokeStubMethod("setServerName", name);
    }

    public String getServerName() {
        return (String) invokeStubMethod("getServerName", new Object[0]);
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username2) {
        this.username = username2;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password2) {
        this.password = password2;
    }

    public void setDomainSuffices(String entries) {
        invokeStubMethod("setDomainSuffices", entries);
    }

    public String getDomainSuffices() {
        return (String) invokeStubMethod("getDomainSuffices", new Object[0]);
    }

    public void setRouteList(String entries) {
        invokeStubMethod("setRouterList", entries);
    }

    public String getRouteList() {
        return (String) invokeStubMethod("getRouteList", new Object[0]);
    }

    public Class<?> getGenericProfileClass() {
        try {
            return loadClass("android.net.vpn.VpnProfile");
        } catch (ClassNotFoundException e) {
            throw new WrapperException("load class failed", e);
        }
    }

    public VpnState getState() {
        return this.state;
    }

    public void setState(VpnState state2) {
        this.state = state2;
    }

    public void write(ObjectOutputStream os) throws IOException {
        os.writeObject(getType());
        os.writeObject(getStub());
        os.writeObject(this.username);
        os.writeObject(this.password);
    }

    public void read(Object obj, ObjectInputStream is) throws Exception {
        setStub(obj);
        this.username = (String) is.readObject();
        this.password = (String) is.readObject();
    }

    public boolean isCompatible(Object obj) {
        return getStubClass().equals(obj.getClass());
    }

    public String toString() {
        return String.valueOf(getId()) + "#" + getName();
    }

    public boolean needKeyStoreToSave() {
        return false;
    }

    public boolean needKeyStoreToConnect() {
        return false;
    }

    public VpnProfile clone() {
        VpnProfile c = (VpnProfile) super.clone();
        c.setId(getId());
        c.setName(getName());
        c.setServerName(getServerName());
        c.setDomainSuffices(getDomainSuffices());
        return c;
    }

    public VpnProfile dulicateToConnect() {
        return clone();
    }
}
