package com.safesys.myvpn;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.http.AndroidHttpClient;
import android.preference.PreferenceManager;
import android.util.Log;
import com.safesys.myvpn.wrapper.VpnState;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;
import org.apache.http.client.methods.HttpHead;

public final class KeepAlive extends BroadcastReceiver {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$safesys$myvpn$wrapper$VpnState = null;
    public static final String PREF_ENABLED = "com.safesys.myvpn.pref.keepAlive";
    public static final String PREF_HEARTBEAT_PERIOD = "com.safesys.myvpn.pref.keepAlive.period";
    /* access modifiers changed from: private */
    public static final String TAG = KeepAlive.class.getName();
    private static Heartbeat heartbeat;
    private static Timer timer = new Timer("com.safesys.myvpn.HeartbeatTimer", true);

    static /* synthetic */ int[] $SWITCH_TABLE$com$safesys$myvpn$wrapper$VpnState() {
        int[] iArr = $SWITCH_TABLE$com$safesys$myvpn$wrapper$VpnState;
        if (iArr == null) {
            iArr = new int[VpnState.values().length];
            try {
                iArr[VpnState.CANCELLED.ordinal()] = 3;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[VpnState.CONNECTED.ordinal()] = 4;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[VpnState.CONNECTING.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[VpnState.DISCONNECTING.ordinal()] = 2;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[VpnState.IDLE.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[VpnState.UNKNOWN.ordinal()] = 7;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[VpnState.UNUSABLE.ordinal()] = 6;
            } catch (NoSuchFieldError e7) {
            }
            $SWITCH_TABLE$com$safesys$myvpn$wrapper$VpnState = iArr;
        }
        return iArr;
    }

    public void onReceive(Context context, Intent intent) {
        if (Constants.ACTION_VPN_CONNECTIVITY.equals(intent.getAction())) {
            String profileName = intent.getStringExtra(Constants.BROADCAST_PROFILE_NAME);
            if (!profileName.equals(Utils.getActvieProfileName(context))) {
                Log.d(TAG, "ignores non-active profile event: " + profileName);
                return;
            }
            stateChanged(profileName, Utils.extractVpnState(intent), PreferenceManager.getDefaultSharedPreferences(context));
        }
    }

    private void stateChanged(String profileName, VpnState newState, SharedPreferences prefs) {
        Log.d(TAG, String.valueOf(profileName) + " state ==> " + newState);
        switch ($SWITCH_TABLE$com$safesys$myvpn$wrapper$VpnState()[newState.ordinal()]) {
            case 4:
                startHeartbeat(prefs);
                return;
            case 5:
                stopHeartbeat();
                return;
            default:
                return;
        }
    }

    private static void startHeartbeat(SharedPreferences prefs) {
        if (prefs.getBoolean(PREF_ENABLED, true) && heartbeat == null) {
            int period = getPeriodFromPrefs(prefs);
            Log.d(TAG, "start heartbeat every (ms)" + period);
            heartbeat = new Heartbeat(null);
            timer.schedule(heartbeat, (long) period, (long) period);
        }
    }

    private static int getPeriodFromPrefs(SharedPreferences prefs) {
        return Period.access$2(Period.valueOf(prefs.getString(PREF_HEARTBEAT_PERIOD, Period.TEN_MIN.toString())));
    }

    private static synchronized void stopHeartbeat() {
        synchronized (KeepAlive.class) {
            if (heartbeat != null) {
                heartbeat.cancel();
                Log.d(TAG, "removed heartbeat timerTask: " + timer.purge());
            }
        }
    }

    private static class Heartbeat extends TimerTask {
        private static final String[] TARGETS = {"http://www.google.com", "http://www.android.com", "http://www.bing.com", "http://www.yahoo.com"};
        private static int index;

        private Heartbeat() {
        }

        /* synthetic */ Heartbeat(Heartbeat heartbeat) {
            this();
        }

        public void run() {
            String url = nextUrl();
            Log.i(KeepAlive.TAG, "start heartbeat, target=" + url);
            AndroidHttpClient client = AndroidHttpClient.newInstance("MyVPN");
            try {
                Log.i(KeepAlive.TAG, "heartbeat resp: " + client.execute(new HttpHead(url)).getStatusLine());
            } catch (IOException e) {
                Log.e(KeepAlive.TAG, "heartdbeat error", e);
            } finally {
                client.close();
            }
        }

        private static String nextUrl() {
            return TARGETS[nextIndex()];
        }

        private static synchronized int nextIndex() {
            int i;
            synchronized (Heartbeat.class) {
                index = index == TARGETS.length ? 0 : index;
                i = index;
                index = i + 1;
            }
            return i;
        }
    }

    private enum Period {
        FIVE_MIN(300000),
        TEN_MIN(600000),
        FIFTEEN_MIN(900000),
        THIRTY_MIN(1800000),
        TEST_5_SEC(5000);
        
        private int value;

        private Period(int v) {
            this.value = v;
        }
    }
}
