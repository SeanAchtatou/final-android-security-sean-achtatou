package com.safesys.myvpn;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.GeneralSecurityException;
import javax.crypto.Cipher;
import javax.crypto.CipherOutputStream;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class StreamCrypto {
    private static final int BUF_SIZE = 128;
    private static final byte[] RAWKEY = {65, 98, 95, 45, 54, 89, 53, 33, 41, 103, 43, 68, 85, 72, 110, 115};

    public static void encrypt(InputStream in, OutputStream out) throws IOException, GeneralSecurityException {
        process(1, in, out);
    }

    public static void decrypt(InputStream in, OutputStream out) throws IOException, GeneralSecurityException {
        process(2, in, out);
    }

    private static void process(int mode, InputStream in, OutputStream out) throws IOException, GeneralSecurityException {
        CipherOutputStream co = null;
        try {
            co = doProcess(mode, in, out);
        } finally {
            in.close();
            if (co != null) {
                co.close();
            }
        }
    }

    private static CipherOutputStream doProcess(int mode, InputStream in, OutputStream out) throws IOException, GeneralSecurityException {
        CipherOutputStream co = new CipherOutputStream(out, getCipher(mode));
        byte[] buf = new byte[128];
        while (true) {
            int len = in.read(buf);
            if (len <= 0) {
                return co;
            }
            co.write(buf, 0, len);
        }
    }

    private static Cipher getCipher(int mode) throws GeneralSecurityException {
        SecretKeySpec skeySpec = new SecretKeySpec(RAWKEY, "AES");
        IvParameterSpec ivps = new IvParameterSpec(RAWKEY);
        Cipher c = Cipher.getInstance("AES/CBC/PKCS5Padding");
        c.init(mode, skeySpec, ivps);
        return c;
    }

    private StreamCrypto() {
    }
}
