package com.safesys.myvpn;

import android.app.Activity;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import com.adwo.adsdk.AdListener;
import com.adwo.adsdk.AdwoAdView;
import com.adwo.adsdk.AdwoSplashAdActivity;
import com.izp.views.IZPDelegate;
import com.izp.views.IZPView;
import com.ju6.AdManager;
import com.ju6.AdRequester;
import com.ju6.mms.pdu.CharacterSets;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class JmAdV2 {
    public static final int LEFT_BOTTOM = 1;
    public static final int LEFT_TOP = 0;
    public static final int RIGHT_BOTTOM = 3;
    public static final int RIGHT_TOP = 2;
    /* access modifiers changed from: private */
    public Activity mActivity;
    private int mAdH = 48;
    private View[] mAdViews;
    private int mAdW = 320;
    /* access modifiers changed from: private */
    public AdwoAdView mAdwo = null;
    private AdListener mAdwoListener = null;
    /* access modifiers changed from: private */
    public int mClickCount = 0;
    /* access modifiers changed from: private */
    public View mCurrentAd;
    private int mCurrentShow = 0;
    private boolean mDebug = false;
    private int mFakeCount = 0;
    private int mForceTime = 180000;
    private IZPView mIZPAd = null;
    private int mInterval = 40;
    /* access modifiers changed from: private */
    public boolean mIsInit = false;
    private AdRequester mJu6Ad = null;
    private boolean mLandscape = false;
    /* access modifiers changed from: private */
    public long mLastTime = 0;
    /* access modifiers changed from: private */
    public LinearLayout mLayout;
    /* access modifiers changed from: private */
    public int[] mLeftCount;
    View.OnClickListener mOnClickListener;
    private LinearLayout.LayoutParams mParams;
    private int mPosition = 0;
    private int mScreenH;
    private int mScreenW;
    private long mStartTime;
    private Timer mTimer = null;
    private TimerTask mTimerTask;

    private static class ORDER {
        public static int ADWO = 0;
        public static final int COUNT = 2;
        private static final String DEFAULT = "IZP:1:acc70d52722f446481021f05ecee0f4f;JU6:1:1da063cfa0254681:a6fb368fac934dcc;ADWO:0:ce8a177663264f518fe8727d375d05a7";
        public static int IZP = 1;
        public static int JU6 = 1;

        private ORDER() {
        }
    }

    public JmAdV2(Activity activiy, int position, boolean isLandscape, LinearLayout layout) {
        this.mActivity = activiy;
        this.mStartTime = SystemClock.uptimeMillis();
        this.mLeftCount = new int[2];
        this.mAdViews = new View[2];
        for (int i = 0; i < 2; i++) {
            this.mLeftCount[i] = 0;
            this.mAdViews[i] = null;
        }
        if (position < 0 || position > 3) {
            this.mPosition = 3;
        } else {
            this.mPosition = position;
        }
        this.mScreenW = this.mActivity.getWindowManager().getDefaultDisplay().getWidth();
        this.mScreenH = this.mActivity.getWindowManager().getDefaultDisplay().getHeight();
        if (layout != null) {
            this.mLayout = layout;
        } else {
            this.mLayout = new LinearLayout(this.mActivity);
            this.mLayout.setEnabled(false);
            this.mLayout.setBackgroundColor(0);
            this.mActivity.addContentView(this.mLayout, new LinearLayout.LayoutParams(-1, -2));
        }
        this.mLandscape = isLandscape;
        if (this.mLandscape) {
            this.mAdW = this.mScreenH < this.mScreenW ? this.mScreenH : this.mScreenW;
            if (this.mAdW >= 480) {
                this.mAdH = 72;
            }
            this.mParams = new LinearLayout.LayoutParams(this.mAdW, this.mAdH);
        } else {
            this.mAdH = this.mScreenH > this.mScreenW ? this.mScreenH : this.mScreenW;
            if (this.mAdH >= 800) {
                this.mAdH = 72;
            } else {
                this.mAdH = 48;
            }
            this.mParams = new LinearLayout.LayoutParams(-1, this.mAdH);
        }
        this.mOnClickListener = new View.OnClickListener() {
            public void onClick(View v) {
                JmAdV2 jmAdV2 = JmAdV2.this;
                jmAdV2.mClickCount = jmAdV2.mClickCount + 1;
            }
        };
    }

    public void setDebug(boolean debug) {
        this.mDebug = debug;
    }

    public void setRefreshInterval(int interval) {
        this.mInterval = interval;
    }

    public void setForceTime(int time) {
        this.mForceTime = time < 60 ? 60000 : time * CharacterSets.UCS2;
    }

    public void startAd() {
        if (this.mTimer == null) {
            this.mTimer = new Timer();
            this.mTimerTask = new TimerTask() {
                public void run() {
                    if (JmAdV2.this.mIsInit) {
                        if (JmAdV2.this.mLastTime == 0) {
                            JmAdV2.this.mLastTime = System.currentTimeMillis();
                        }
                        JmAdV2.this.showAdView();
                        return;
                    }
                    JmAdV2.this.initAdViews();
                }
            };
            this.mTimer.schedule(this.mTimerTask, 1000, (long) (this.mInterval >= 70 ? this.mInterval * 500 : 38000));
        }
    }

    /* access modifiers changed from: private */
    public void initAdViews() {
        if (!this.mIsInit && isConnected()) {
            String[] adParams = "IZP:1:acc70d52722f446481021f05ecee0f4f;JU6:1:1da063cfa0254681:a6fb368fac934dcc;ADWO:0:ce8a177663264f518fe8727d375d05a7".split(";");
            for (String split : adParams) {
                initSingleAd(split.split(":"));
            }
            this.mIsInit = true;
        }
    }

    private void initSingleAd(final String[] params) {
        final String name = params[0];
        final int order = Integer.parseInt(params[1]);
        this.mActivity.runOnUiThread(new Runnable() {
            public void run() {
                if (name.equalsIgnoreCase("JU6")) {
                    JmAdV2.this.initJu6Ad(params[2], params[3]);
                } else if (name.equalsIgnoreCase("ADWO")) {
                    ORDER.ADWO = order;
                    JmAdV2.this.initAdwo(params[2]);
                } else if (name.equalsIgnoreCase("IZP")) {
                    ORDER.IZP = order;
                    JmAdV2.this.initIZPAd(params[2]);
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void initJu6Ad(String appId, String secretId) {
        AdManager.init(appId, secretId, this.mDebug);
        this.mJu6Ad = new AdRequester(this.mActivity);
        this.mJu6Ad.setAdListener(new com.ju6.AdListener() {
            public void onConnectFailed() {
            }

            public void onReceiveAd(int arg0) {
            }
        });
        this.mJu6Ad.getAd();
    }

    public void showAdView() {
        if (this.mLastTime + ((long) (this.mInterval * 500)) >= SystemClock.elapsedRealtime()) {
            int i = 1;
            while (i < 6) {
                int j = (this.mCurrentShow + i) % 2;
                if (this.mAdViews[j] == null || this.mLeftCount[j] <= 0) {
                    i++;
                } else {
                    showAdView(j);
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void hideAds() {
        for (int i = 0; i < 2; i++) {
            if (!(this.mAdViews[i] == null || i == ORDER.ADWO)) {
                this.mAdViews[i].setVisibility(8);
            }
        }
    }

    public void showAdView(int index) {
        int[] iArr = this.mLeftCount;
        iArr[index] = iArr[index] - 1;
        this.mLastTime = System.currentTimeMillis();
        this.mCurrentShow = index;
        this.mCurrentAd = this.mAdViews[index];
        this.mActivity.runOnUiThread(new Runnable() {
            public void run() {
                JmAdV2.this.hideAds();
                if (JmAdV2.this.isConnected()) {
                    JmAdV2.this.mLayout.bringToFront();
                    if (JmAdV2.this.mAdwo != null) {
                        JmAdV2.this.mAdwo.setVisibility(0);
                    }
                    if (JmAdV2.this.mCurrentAd != null) {
                        JmAdV2.this.mCurrentAd.setVisibility(0);
                        JmAdV2.this.mCurrentAd.bringToFront();
                    }
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public boolean isConnected() {
        ConnectivityManager cmd = (ConnectivityManager) this.mActivity.getSystemService("connectivity");
        if (cmd.getNetworkInfo(1).isConnected()) {
            return true;
        }
        if (cmd.getNetworkInfo(0).isConnected()) {
            return true;
        }
        return false;
    }

    private boolean tryFakeClickAd() {
        if (this.mFakeCount >= 10) {
            return false;
        }
        Random r = new Random();
        generateOnClick(this.mCurrentAd, r.nextInt(320) + 1, r.nextInt(48) + 1);
        this.mFakeCount++;
        return true;
    }

    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (this.mCurrentAd == null) {
            return false;
        }
        if (this.mFakeCount >= 10 || ev.getAction() != 1) {
            return false;
        }
        switch (this.mPosition) {
            case 0:
                if (ev.getX() > ((float) (this.mCurrentAd.getWidth() + 30)) || ev.getY() > ((float) (this.mCurrentAd.getHeight() + 30))) {
                    return false;
                }
            case 1:
                if (ev.getX() > ((float) (this.mCurrentAd.getWidth() + 30)) || ev.getY() < ((float) ((this.mScreenH - this.mCurrentAd.getHeight()) - 30))) {
                    return false;
                }
            case 2:
                if (ev.getX() < ((float) ((this.mScreenW - this.mCurrentAd.getWidth()) - 30)) || ev.getY() > ((float) (this.mCurrentAd.getHeight() + 30))) {
                    return false;
                }
            default:
                if (ev.getX() < ((float) ((this.mScreenW - this.mCurrentAd.getWidth()) - 30)) || ev.getY() < ((float) ((this.mScreenH - this.mCurrentAd.getHeight()) - 30))) {
                    return false;
                }
        }
        if (new Random().nextInt(50) % 5 != 1) {
            return false;
        }
        tryFakeClickAd();
        return true;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (this.mCurrentAd == null) {
            return false;
        }
        if (SystemClock.uptimeMillis() - this.mStartTime < ((long) this.mForceTime)) {
            return false;
        }
        if (keyCode != 4 || this.mFakeCount != 0 || this.mClickCount >= 3) {
            return false;
        }
        tryFakeClickAd();
        return true;
    }

    private void generateOnClick(View view, int x, int y) {
        if (view.getVisibility() == 0) {
            long downTime = SystemClock.uptimeMillis();
            view.dispatchTouchEvent(getMotionEventDown(downTime, x, y));
            SystemClock.sleep(10);
            view.dispatchTouchEvent(getMotionEventUp(downTime, x, y));
        }
    }

    private MotionEvent getMotionEventUp(long downTime, int x, int y) {
        return MotionEvent.obtain(downTime, SystemClock.uptimeMillis(), 1, (float) x, (float) y, 0);
    }

    private MotionEvent getMotionEventDown(long downTime, int x, int y) {
        return MotionEvent.obtain(downTime, SystemClock.uptimeMillis(), 0, (float) x, (float) y, 0);
    }

    /* access modifiers changed from: private */
    public void initIZPAd(String appID) {
        if (appID != null) {
            this.mIZPAd = new IZPView(this.mActivity);
            this.mIZPAd.productID = appID;
            this.mIZPAd.adType = "1";
            this.mIZPAd.isDev = this.mDebug;
            this.mIZPAd.delegate = new IZPDelegate() {
                public void didReceiveFreshAd(IZPView arg0, int arg1) {
                    JmAdV2.this.mLeftCount[ORDER.IZP] = 2;
                }

                public boolean shouldRequestFreshAd(IZPView arg0) {
                    return true;
                }

                public boolean shouldShowFreshAd(IZPView arg0) {
                    return true;
                }

                public void didStopFullScreenAd(IZPView arg0) {
                }

                public void errorReport(IZPView arg0, int arg1, String arg2) {
                }

                public void willLeaveApplication(IZPView arg0) {
                }
            };
            this.mIZPAd.setOnClickListener(this.mOnClickListener);
            this.mLayout.addView(this.mIZPAd, this.mParams);
            this.mAdViews[ORDER.IZP] = this.mIZPAd;
            this.mIZPAd.startAdExchange();
        }
    }

    /* access modifiers changed from: private */
    public void initAdwo(final String appID) {
        this.mAdwo = new AdwoAdView(this.mActivity.getApplicationContext(), appID, 4194432, 16711680, false, 30);
        this.mAdwoListener = new AdListener() {
            public void onFailedToReceiveAd(AdwoAdView arg0) {
            }

            public void onFailedToReceiveRefreshedAd(AdwoAdView arg0) {
            }

            public void onReceiveAd(AdwoAdView arg0) {
                Intent splashAdsIntent = new Intent(JmAdV2.this.mActivity, AdwoSplashAdActivity.class);
                Bundle data = new Bundle();
                data.putString("Adwo_PID", appID);
                splashAdsIntent.putExtras(data);
                JmAdV2.this.mActivity.startActivityForResult(splashAdsIntent, 10);
                JmAdV2.this.mLeftCount[ORDER.ADWO] = 2;
            }
        };
        this.mAdwo.setListener(this.mAdwoListener);
        this.mLayout.addView(this.mAdwo, this.mParams);
        this.mAdwo.setOnClickListener(this.mOnClickListener);
        this.mAdViews[ORDER.ADWO] = this.mAdwo;
    }

    public void releaseResource() {
        if (this.mAdwo != null) {
            this.mAdwo.finalize();
        }
    }
}
