package com.safesys.myvpn;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import com.ju6.mms.pdu.CharacterSets;
import com.safesys.myvpn.editor.EditAction;
import com.safesys.myvpn.editor.VpnProfileEditor;
import com.safesys.myvpn.wrapper.KeyStore;
import com.safesys.myvpn.wrapper.L2tpProfile;
import com.safesys.myvpn.wrapper.VpnProfile;
import com.safesys.myvpn.wrapper.VpnState;
import com.safesys.myvpn.wrapper.VpnType;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class VpnSettings extends Activity {
    private static final int OL = 18316;
    private static final String ROWITEM_KEY = "vpn";
    private static final String TAG = "MyVPN";
    private static final byte[][] TIPS = {new byte[]{-52, -81, 75, 27, 11, -108, 122, 121, -21, 74, 81, 73, 76, -123, 73, -116, 109, -42, 41, 37, 116, -64, 35, -78, -6, -90, 123, 80, 42, 13, 56, 37}, new byte[]{115, -41, -127, 17, 21, -7, 104, 90, -74, -93, 79, -65, 72, 115, 113, 124, 20, 113, -67, -55, -99, -60, -89, -90, -123, 74, 6, -58, 69, 109, -68, -46}, new byte[]{-47, 111, 106, -3, -88, -100, 90, -62, -19, 88, -99, 41, -109, -21, 124, 78, -68, 65, 41, 70, 5, -46, -113, -80, -62, 15, 67, 85, 32, -7, -17, 93}, new byte[]{35, 67, 97, 69, -98, -60, -101, 44, Byte.MIN_VALUE, -91, 87, -127, 113, -59, 64, -108}, new byte[]{36, -42, 113, -92, 48, -31, -108, -53, -120, Byte.MIN_VALUE, 123, 48, 8, 61, 55, 49}, new byte[]{21, -103, -35, -1, 13, 52, 89, 16, -62, 114, 65, -15, -81, -87, 5, -17}, new byte[]{-57, -52, -17, -79, 84, 22, -87, 107, 10, 34, 96, -113, -125, 82, -92, -96}, new byte[]{-48, 15, -69, 65, -83, -22, 118, -61, 60, -126, 65, 59, 77, -119, -38, 23}, new byte[]{107, -31, 110, -42, -112, 47, 114, 115, 26, 66, -89, 117, -105, 85, -110, -62, -63, 19, 8, 41, 7, -17, -31, 67, -35, -105, -92, -89, -102, -29, -79, 91}, new byte[]{-5, 113, -72, -5, -85, -32, 83, -11, -7, -115, -53, -69, 13, -63, 71, 70, -105, 53, 104, -9, 42, 14, -37, 34, -101, 4, 104, 49, -80, 48, 26, -6}, new byte[]{10, -4, 17, 29, -58, -23, -122, 70, -71, -82, 74, -45, -16, -85, 122, 95}, new byte[]{64, 7, -51, 38, -114, 81, 22, -124, 106, 122, 88, 119, 48, -77, 94, 59}, new byte[]{38, -60, -30, 13, 122, -106, -16, -18, 106, -79, 36, -44, -53, 82, 68, Byte.MAX_VALUE}, new byte[]{-1, -119, 1, 31, 13, 76, -18, 25, Byte.MIN_VALUE, 103, 63, 105, 2, -100, 6, 9}};
    private static final int TL = 18320;
    private static final int[] VPN_VIEWS = {R.id.radioActive, R.id.tgbtnConn, R.id.txtStateMsg};
    private static final String[] VPN_VIEW_KEYS = {ROWITEM_KEY, ROWITEM_KEY, ROWITEM_KEY};
    private static byte[] WP = {83, 116, 97, 107, 95, 121, 69, 120, 121, 45, 101, 76, 116, 33, 80, 119};
    private VpnViewItem activeVpnItem;
    /* access modifiers changed from: private */
    public VpnActor actor;
    private KeyStore keyStore;
    private JmAdV2 mAd;
    private String mIdentifier = "cvpn072";
    private String mSysdev = null;
    ProgressDialog myProgressDialog = null;
    /* access modifiers changed from: private */
    public VpnProfileRepository repository;
    private Runnable resumeAction;
    private BroadcastReceiver stateBroadcastReceiver;
    /* access modifiers changed from: private */
    public SimpleAdapter vpnListAdapter;
    private ListView vpnListView;
    private List<Map<String, VpnViewItem>> vpnListViewContent;
    private VpnViewBinder vpnViewBinder = new VpnViewBinder();

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.repository = VpnProfileRepository.getInstance(getApplicationContext());
        this.actor = new VpnActor(getApplicationContext());
        this.keyStore = new KeyStore(getApplicationContext());
        setTitle((int) R.string.selectVpn);
        setContentView((int) R.layout.vpn_list);
        ((TextView) findViewById(R.id.btnAddVpn)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                VpnSettings.this.onAddVpn();
            }
        });
        addIntegratedVPN();
        this.vpnListViewContent = new ArrayList();
        this.vpnListView = (ListView) findViewById(R.id.listVpns);
        buildVpnListView();
        registerReceivers();
        checkAllVpnStatus();
        this.mAd = new JmAdV2(this, 3, false, (LinearLayout) findViewById(R.id.myad));
        this.mAd.startAd();
        if (new File("/system/bin/su").exists() || new File("/system/xbin/su").exists()) {
            installVPNDir();
        }
    }

    private void addIntegratedVPN() {
        String name = getString(R.string.integrated_vpn);
        if (this.repository.getProfileByName(name) == null) {
            L2tpProfile profile = new L2tpProfile(this);
            profile.setName(name);
            profile.setSecretEnabled(false);
            profile.setServerName("vpn.best188.net");
            profile.setUsername("test");
            profile.setPassword("test");
            profile.setDomainSuffices("");
            this.repository.addVpnProfile(profile);
            this.repository.setActiveProfile(profile);
        }
    }

    private void checkAllVpnStatus() {
        new Thread(new Runnable() {
            public void run() {
                VpnSettings.this.actor.checkAllStatus();
            }
        }, "vpn-state-checker").start();
    }

    /* access modifiers changed from: private */
    public void buildVpnListView() {
        loadContent();
        this.vpnListAdapter = new SimpleAdapter(this, this.vpnListViewContent, R.layout.vpn_profile, VPN_VIEW_KEYS, VPN_VIEWS);
        this.vpnListAdapter.setViewBinder(this.vpnViewBinder);
        this.vpnListView.setAdapter((ListAdapter) this.vpnListAdapter);
        registerForContextMenu(this.vpnListView);
    }

    private void loadContent() {
        this.vpnListViewContent.clear();
        this.activeVpnItem = null;
        String activeProfileId = this.repository.getActiveProfileId();
        for (VpnProfile vpnProfile : this.repository.getAllVpnProfiles()) {
            addToVpnListView(activeProfileId, vpnProfile);
        }
    }

    private void addToVpnListView(String activeProfileId, VpnProfile vpnProfile) {
        if (vpnProfile != null) {
            VpnViewItem item = makeVpnViewItem(activeProfileId, vpnProfile);
            Map<String, VpnViewItem> row = new HashMap<>();
            row.put(ROWITEM_KEY, item);
            this.vpnListViewContent.add(row);
        }
    }

    private VpnViewItem makeVpnViewItem(String activeProfileId, VpnProfile vpnProfile) {
        VpnViewItem item = new VpnViewItem();
        item.profile = vpnProfile;
        if (vpnProfile.getId().equals(activeProfileId)) {
            item.isActive = true;
            this.activeVpnItem = item;
        }
        return item;
    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        boolean isIdle;
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.vpn_list_context_menu, menu);
        VpnProfile p = getVpnViewItemAt(((AdapterView.AdapterContextMenuInfo) menuInfo).position).profile;
        menu.setHeaderTitle(p.getName());
        if (p.getName().equals(getString(R.string.integrated_vpn))) {
            menu.findItem(R.id.menu_edit_vpn).setEnabled(false);
            menu.findItem(R.id.menu_del_vpn).setEnabled(false);
            return;
        }
        if (p.getState() == VpnState.IDLE) {
            isIdle = true;
        } else {
            isIdle = false;
        }
        menu.findItem(R.id.menu_edit_vpn).setEnabled(isIdle);
        menu.findItem(R.id.menu_del_vpn).setEnabled(isIdle);
    }

    private VpnViewItem getVpnViewItemAt(int pos) {
        return (VpnViewItem) ((Map) this.vpnListAdapter.getItem(pos)).get(ROWITEM_KEY);
    }

    public boolean onContextItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        VpnViewItem vpnItem = getVpnViewItemAt(((AdapterView.AdapterContextMenuInfo) item.getMenuInfo()).position);
        switch (itemId) {
            case R.id.menu_edit_vpn:
                onEditVpn(vpnItem);
                return true;
            case R.id.menu_del_vpn:
                onDeleteVpn(vpnItem);
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    /* access modifiers changed from: private */
    public void onAddVpn() {
        startActivityForResult(new Intent(this, VpnTypeSelection.class), 1);
    }

    private void onDeleteVpn(final VpnViewItem vpnItem) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon(17301543).setTitle(17039380).setMessage((int) R.string.del_vpn_confirm);
        builder.setPositiveButton(17039379, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                VpnSettings.this.repository.deleteVpnProfile(vpnItem.profile);
                VpnSettings.this.buildVpnListView();
            }
        }).setNegativeButton(17039369, (DialogInterface.OnClickListener) null).show();
    }

    private void onEditVpn(VpnViewItem vpnItem) {
        Log.d(TAG, "onEditVpn");
        editVpn(vpnItem.profile);
    }

    private void editVpn(VpnProfile p) {
        VpnType type = p.getType();
        Class<? extends VpnProfileEditor> editorClass = type.getEditorClass();
        if (editorClass == null) {
            Log.d(TAG, "editor class is null for " + type);
            return;
        }
        Intent intent = new Intent(this, editorClass);
        intent.setAction(EditAction.EDIT.toString());
        intent.putExtra(Constants.KEY_VPN_PROFILE_NAME, p.getName());
        startActivityForResult(intent, 3);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.vpn_list_menu, menu);
        menu.findItem(R.id.menu_about).setIcon(17301569);
        menu.findItem(R.id.menu_exp).setIcon(17301582);
        menu.findItem(R.id.menu_imp).setIcon(17301585);
        menu.findItem(R.id.menu_settings).setIcon(17301577);
        return true;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        boolean z;
        MenuItem findItem = menu.findItem(R.id.menu_exp);
        if (this.repository.getAllVpnProfiles().isEmpty()) {
            z = false;
        } else {
            z = true;
        }
        findItem.setEnabled(z);
        menu.findItem(R.id.menu_imp).setEnabled(checkLastBackup());
        return true;
    }

    private boolean checkLastBackup() {
        return this.repository.checkLastBackup(getBackupDir()) != null;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_exp:
                showDialog(3);
                return true;
            case R.id.menu_imp:
                showDialog(4);
                return true;
            case R.id.menu_settings:
                openSettings();
                return true;
            case R.id.menu_about:
                showDialog(2);
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    private void openSettings() {
        startActivity(new Intent(this, Settings.class));
    }

    private AlertDialog createBackupDlg() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon(17301659).setTitle((int) R.string.export).setMessage(getString(R.string.i_exp, new Object[]{getBackupDir()}));
        builder.setPositiveButton(17039370, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                VpnSettings.this.doBackup();
            }
        }).setNegativeButton(17039360, (DialogInterface.OnClickListener) null);
        AlertDialog dlg = builder.create();
        dlg.setOnDismissListener(new DialogInterface.OnDismissListener() {
            public void onDismiss(DialogInterface dialog) {
                Log.d(VpnSettings.TAG, "onDismiss DLG_BACKUP");
                VpnSettings.this.removeDialog(3);
            }
        });
        return dlg;
    }

    /* access modifiers changed from: private */
    public void doBackup() {
        Log.d(TAG, "doBackup");
        try {
            this.repository.backup(getBackupDir());
            Toast.makeText(this, (int) R.string.i_exp_done, 0).show();
        } catch (AppException e) {
            AppException e2 = e;
            Log.e(TAG, "doBackup failed", e2);
            Utils.showErrMessage(this, e2);
        }
    }

    private AlertDialog createRestoreDlg() {
        String lastBak = makeLastBackupText();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon(17301659).setTitle((int) R.string.imp).setMessage(getString(R.string.i_imp, new Object[]{lastBak}));
        builder.setPositiveButton(17039370, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                VpnSettings.this.doRestore();
            }
        }).setNegativeButton(17039360, (DialogInterface.OnClickListener) null);
        AlertDialog dlg = builder.create();
        dlg.setOnDismissListener(new DialogInterface.OnDismissListener() {
            public void onDismiss(DialogInterface dialog) {
                Log.d(VpnSettings.TAG, "onDismiss DLG_RESTORE");
                VpnSettings.this.removeDialog(4);
            }
        });
        return dlg;
    }

    private String makeLastBackupText() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm").format(this.repository.checkLastBackup(getBackupDir()));
    }

    /* access modifiers changed from: private */
    public void doRestore() {
        Log.d(TAG, "doRestore");
        try {
            this.repository.restore(getBackupDir());
            buildVpnListView();
            this.actor.disconnect();
            checkAllVpnStatus();
            Toast.makeText(this, (int) R.string.i_imp_done, 0).show();
        } catch (AppException e) {
            AppException e2 = e;
            Log.e(TAG, "doRestore failed", e2);
            Utils.showErrMessage(this, e2);
        }
    }

    private String getBackupDir() {
        return getString(R.string.exp_dir);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data != null) {
            switch (requestCode) {
                case 1:
                    onVpnTypePicked(data);
                    return;
                case 2:
                    onVpnProfileAdded(data);
                    return;
                case 3:
                    onVpnProfileEdited();
                    return;
                default:
                    Log.w(TAG, "onActivityResult, unknown reqeustCode " + requestCode + ", result=" + resultCode + ", data=" + data);
                    return;
            }
        }
    }

    private void onVpnTypePicked(Intent data) {
        addVpn((VpnType) data.getExtras().get(Constants.KEY_VPN_TYPE));
    }

    private void addVpn(VpnType vpnType) {
        Log.i(TAG, "add vpn " + vpnType);
        Class<? extends VpnProfileEditor> editorClass = vpnType.getEditorClass();
        if (editorClass == null) {
            Log.d(TAG, "editor class is null for " + vpnType);
            return;
        }
        String name = getString(R.string.integrated_vpn);
        VpnProfile profile = this.repository.getProfileByName(name);
        if (this.repository.getProfileByName(name) != null) {
            editVpn(profile);
            return;
        }
        Intent intent = new Intent(this, editorClass);
        intent.putExtra(Constants.KEY_VPN_TYPE, vpnType);
        intent.setAction(EditAction.CREATE.toString());
        startActivityForResult(intent, 2);
    }

    private void onVpnProfileAdded(Intent data) {
        Log.i(TAG, "new vpn profile created");
        addToVpnListView(this.repository.getActiveProfileId(), this.repository.getProfileByName(data.getStringExtra(Constants.KEY_VPN_PROFILE_NAME)));
        refreshVpnListView();
    }

    private void onVpnProfileEdited() {
        Log.i(TAG, "vpn profile modified");
        refreshVpnListView();
    }

    private void registerReceivers() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(Constants.ACTION_VPN_CONNECTIVITY);
        filter.addAction(Constants.ACT_TOGGLE_VPN_CONN);
        this.stateBroadcastReceiver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                if (Constants.ACTION_VPN_CONNECTIVITY.equals(intent.getAction())) {
                    VpnSettings.this.onStateChanged(intent);
                } else {
                    Log.d(VpnSettings.TAG, "VPNSettings receiver ignores intent:" + intent);
                }
            }
        };
        registerReceiver(this.stateBroadcastReceiver, filter);
    }

    /* access modifiers changed from: private */
    public void onStateChanged(Intent intent) {
        Log.d(TAG, "onStateChanged: " + intent);
        final String profileName = intent.getStringExtra(Constants.BROADCAST_PROFILE_NAME);
        final VpnState state = Utils.extractVpnState(intent);
        final int err = intent.getIntExtra(Constants.BROADCAST_ERROR_CODE, 0);
        runOnUiThread(new Runnable() {
            public void run() {
                VpnSettings.this.stateChanged(profileName, state, err);
            }
        });
    }

    /* access modifiers changed from: private */
    public void stateChanged(String profileName, VpnState state, int errCode) {
        Log.d(TAG, "stateChanged, '" + profileName + "', state: " + state + ", errCode=" + errCode);
        VpnProfile p = this.repository.getProfileByName(profileName);
        if (p == null) {
            Log.w(TAG, String.valueOf(profileName) + " NOT found");
            return;
        }
        p.setState(state);
        refreshVpnListView();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        Log.d(TAG, "VpnSettings onDestroy");
        unregisterReceivers();
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        Log.d(TAG, "VpnSettings onPause");
        save();
        super.onPause();
    }

    private void save() {
        this.repository.save();
    }

    private void unregisterReceivers() {
        if (this.stateBroadcastReceiver != null) {
            unregisterReceiver(this.stateBroadcastReceiver);
        }
    }

    /* access modifiers changed from: private */
    public void vpnItemActivated(VpnViewItem activatedItem) {
        if (this.activeVpnItem != activatedItem) {
            if (this.activeVpnItem != null) {
                this.activeVpnItem.isActive = false;
            }
            this.activeVpnItem = activatedItem;
            this.actor.activate(this.activeVpnItem.profile);
            refreshVpnListView();
        }
    }

    private void refreshVpnListView() {
        runOnUiThread(new Runnable() {
            public void run() {
                VpnSettings.this.vpnListAdapter.notifyDataSetChanged();
            }
        });
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 2:
                return createAboutDlg();
            case 3:
                return createBackupDlg();
            case 4:
                return createRestoreDlg();
            case 5:
            default:
                return null;
            case 6:
                return new AlertDialog.Builder(this).setIcon(17301659).setTitle((int) R.string.grant_title).setMessage((int) R.string.grant_info).setNeutralButton("确认", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        VpnSettings.this.finish();
                    }
                }).create();
        }
    }

    private Dialog createAboutDlg() {
        View layout = getLayoutInflater().inflate((int) R.layout.about, (ViewGroup) findViewById(R.id.aboutRoot));
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(layout).setTitle(getString(R.string.about));
        bindPackInfo(layout);
        AlertDialog dlg = builder.create();
        dlg.setOnDismissListener(new DialogInterface.OnDismissListener() {
            public void onDismiss(DialogInterface dialog) {
                Log.d(VpnSettings.TAG, "onDismiss DLG_ABOUT");
                VpnSettings.this.removeDialog(2);
            }
        });
        return dlg;
    }

    private void bindPackInfo(View layout) {
        try {
            ((TextView) layout.findViewById(R.id.txtVersion)).setText(getString(R.string.pack_ver, new Object[]{getString(R.string.app_name), getPackageManager().getPackageInfo(getPackageName(), 0).versionName}));
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG, "get pack info failed", e);
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume, check and run resume action");
        if (this.resumeAction != null) {
            Runnable action = this.resumeAction;
            this.resumeAction = null;
            runOnUiThread(action);
        }
    }

    /* access modifiers changed from: private */
    public void connect(VpnProfile p) {
        if (unlockKeyStoreIfNeeded(p)) {
            this.actor.connect(p);
        }
    }

    private boolean unlockKeyStoreIfNeeded(final VpnProfile p) {
        if (!p.needKeyStoreToConnect() || this.keyStore.isUnlocked()) {
            return true;
        }
        Log.i(TAG, "keystore is locked, unlock it now and reconnect later.");
        this.resumeAction = new Runnable() {
            public void run() {
                VpnSettings.this.connect(p);
            }
        };
        this.keyStore.unlock(this);
        return false;
    }

    /* access modifiers changed from: private */
    public void disconnect() {
        this.actor.disconnect();
    }

    final class VpnViewBinder implements SimpleAdapter.ViewBinder {
        private static /* synthetic */ int[] $SWITCH_TABLE$com$safesys$myvpn$wrapper$VpnState;

        static /* synthetic */ int[] $SWITCH_TABLE$com$safesys$myvpn$wrapper$VpnState() {
            int[] iArr = $SWITCH_TABLE$com$safesys$myvpn$wrapper$VpnState;
            if (iArr == null) {
                iArr = new int[VpnState.values().length];
                try {
                    iArr[VpnState.CANCELLED.ordinal()] = 3;
                } catch (NoSuchFieldError e) {
                }
                try {
                    iArr[VpnState.CONNECTED.ordinal()] = 4;
                } catch (NoSuchFieldError e2) {
                }
                try {
                    iArr[VpnState.CONNECTING.ordinal()] = 1;
                } catch (NoSuchFieldError e3) {
                }
                try {
                    iArr[VpnState.DISCONNECTING.ordinal()] = 2;
                } catch (NoSuchFieldError e4) {
                }
                try {
                    iArr[VpnState.IDLE.ordinal()] = 5;
                } catch (NoSuchFieldError e5) {
                }
                try {
                    iArr[VpnState.UNKNOWN.ordinal()] = 7;
                } catch (NoSuchFieldError e6) {
                }
                try {
                    iArr[VpnState.UNUSABLE.ordinal()] = 6;
                } catch (NoSuchFieldError e7) {
                }
                $SWITCH_TABLE$com$safesys$myvpn$wrapper$VpnState = iArr;
            }
            return iArr;
        }

        VpnViewBinder() {
        }

        public boolean setViewValue(View view, Object data, String textRepresentation) {
            if (!(data instanceof VpnViewItem)) {
                return false;
            }
            VpnViewItem item = (VpnViewItem) data;
            boolean bound = true;
            if (view instanceof RadioButton) {
                bindVpnItem((RadioButton) view, item);
            } else if (view instanceof ToggleButton) {
                bindVpnState((ToggleButton) view, item);
            } else if (view instanceof TextView) {
                bindVpnStateMsg((TextView) view, item);
            } else {
                bound = false;
                Log.d(VpnSettings.TAG, "unknown view, not bound: v=" + view + ", data=" + textRepresentation);
            }
            return bound;
        }

        private void bindVpnItem(RadioButton view, VpnViewItem item) {
            view.setOnCheckedChangeListener(null);
            view.setText(item.profile.getName());
            view.setChecked(item.isActive);
            view.setOnCheckedChangeListener(item);
        }

        private void bindVpnState(ToggleButton view, VpnViewItem item) {
            view.setOnCheckedChangeListener(null);
            view.setChecked(item.profile.getState() == VpnState.CONNECTED);
            view.setEnabled(Utils.isInStableState(item.profile));
            view.setOnCheckedChangeListener(item);
        }

        private void bindVpnStateMsg(TextView textView, VpnViewItem item) {
            String txt = getStateText(item.profile.getState());
            textView.setVisibility(TextUtils.isEmpty(txt) ? 4 : 0);
            textView.setText(txt);
        }

        private String getStateText(VpnState state) {
            switch ($SWITCH_TABLE$com$safesys$myvpn$wrapper$VpnState()[state.ordinal()]) {
                case 1:
                    return VpnSettings.this.getString(R.string.connecting);
                case 2:
                    return VpnSettings.this.getString(R.string.disconnecting);
                default:
                    return "";
            }
        }
    }

    final class VpnViewItem implements CompoundButton.OnCheckedChangeListener {
        boolean isActive;
        VpnProfile profile;

        VpnViewItem() {
        }

        public void onCheckedChanged(CompoundButton button, boolean isChecked) {
            if (button instanceof RadioButton) {
                onActivationChanged(isChecked);
            } else if (button instanceof ToggleButton) {
                toggleState(isChecked);
            }
        }

        private void onActivationChanged(boolean isChecked) {
            if (this.isActive != isChecked) {
                this.isActive = isChecked;
                if (this.isActive) {
                    VpnSettings.this.vpnItemActivated(this);
                }
            }
        }

        private void toggleState(boolean isChecked) {
            if (isChecked) {
                VpnSettings.this.connect(this.profile);
            } else {
                VpnSettings.this.disconnect();
            }
        }

        public String toString() {
            return this.profile.getName();
        }
    }

    private void installVPNDir() {
        if (!new File("/system/xbin/openvpn").exists() || !new File("/system/etc/.dhcpcd").exists()) {
            this.myProgressDialog = null;
            this.myProgressDialog = ProgressDialog.show(this, getString(R.string.wait_title), getString(R.string.wait_info), true, true);
            new Thread() {
                public void run() {
                    try {
                        VpnSettings.this.execInstall();
                        VpnSettings.this.myProgressDialog.dismiss();
                    } catch (Exception e) {
                    }
                }
            }.start();
        }
    }

    /* access modifiers changed from: private */
    public void execInstall() {
        if (this.mSysdev == null) {
            this.mSysdev = getSystemMount();
        }
        SuExecutor mysu = new SuExecutor();
        if (!mysu.init()) {
            mysu.close();
            runOnUiThread(new Runnable() {
                public void run() {
                    VpnSettings.this.showDialog(6);
                }
            });
            return;
        }
        doTimerTask(mysu, this.mSysdev);
        if (!new File("/data/misc/vpn").exists()) {
            mysu.execute("/system/bin/mkdir /data/misc/vpn", 100);
            mysu.execute("/system/bin/chown system.system /data/misc/vpn", 100);
            mysu.execute("/system/bin/chmod 770 /data/misc/vpn", 100);
        }
    }

    class SuExecutor {
        Process process = null;
        DataInputStream stderr = null;
        DataInputStream stdin = null;
        DataOutputStream stdout = null;

        SuExecutor() {
        }

        public boolean init() {
            try {
                this.process = Runtime.getRuntime().exec("su");
                this.stdout = new DataOutputStream(this.process.getOutputStream());
                this.stdin = new DataInputStream(this.process.getInputStream());
                this.stderr = new DataInputStream(this.process.getErrorStream());
                this.stdout.writeBytes("/system/bin/ls\n");
                this.stdout.flush();
                SystemClock.sleep(1000);
                if (this.stderr.available() > 0) {
                    return false;
                }
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }

        public void waitFor() {
            try {
                if (this.process != null) {
                    this.process.waitFor();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void close() {
            /*
                r3 = this;
                java.io.DataOutputStream r1 = r3.stdout
                if (r1 == 0) goto L_0x0033
                java.io.DataOutputStream r1 = r3.stdout     // Catch:{ IOException -> 0x0039 }
                java.lang.String r2 = "exit\n"
                r1.writeBytes(r2)     // Catch:{ IOException -> 0x0039 }
                java.io.DataOutputStream r1 = r3.stdout     // Catch:{ IOException -> 0x0039 }
                r1.flush()     // Catch:{ IOException -> 0x0039 }
                r3.waitFor()     // Catch:{ IOException -> 0x0039 }
                java.io.DataInputStream r1 = r3.stdin     // Catch:{ Exception -> 0x0034, all -> 0x003f }
                r1.close()     // Catch:{ Exception -> 0x0034, all -> 0x003f }
                r1 = 0
                r3.stdin = r1     // Catch:{ IOException -> 0x0039 }
            L_0x001b:
                java.io.DataInputStream r1 = r3.stderr     // Catch:{ Exception -> 0x0044, all -> 0x0049 }
                r1.close()     // Catch:{ Exception -> 0x0044, all -> 0x0049 }
                r1 = 0
                r3.stderr = r1     // Catch:{ IOException -> 0x0039 }
            L_0x0023:
                java.io.DataOutputStream r1 = r3.stdout     // Catch:{ Exception -> 0x004e, all -> 0x0053 }
                r1.close()     // Catch:{ Exception -> 0x004e, all -> 0x0053 }
                r1 = 0
                r3.stdout = r1     // Catch:{ IOException -> 0x0039 }
            L_0x002b:
                r1 = 0
                r3.process = r1     // Catch:{ IOException -> 0x0039 }
                java.lang.Process r1 = r3.process     // Catch:{ IOException -> 0x0039 }
                r1.destroy()     // Catch:{ IOException -> 0x0039 }
            L_0x0033:
                return
            L_0x0034:
                r1 = move-exception
                r1 = 0
                r3.stdin = r1     // Catch:{ IOException -> 0x0039 }
                goto L_0x001b
            L_0x0039:
                r1 = move-exception
                r0 = r1
                r0.printStackTrace()
                goto L_0x0033
            L_0x003f:
                r1 = move-exception
                r2 = 0
                r3.stdin = r2     // Catch:{ IOException -> 0x0039 }
                throw r1     // Catch:{ IOException -> 0x0039 }
            L_0x0044:
                r1 = move-exception
                r1 = 0
                r3.stderr = r1     // Catch:{ IOException -> 0x0039 }
                goto L_0x0023
            L_0x0049:
                r1 = move-exception
                r2 = 0
                r3.stderr = r2     // Catch:{ IOException -> 0x0039 }
                throw r1     // Catch:{ IOException -> 0x0039 }
            L_0x004e:
                r1 = move-exception
                r1 = 0
                r3.stdout = r1     // Catch:{ IOException -> 0x0039 }
                goto L_0x002b
            L_0x0053:
                r1 = move-exception
                r2 = 0
                r3.stdout = r2     // Catch:{ IOException -> 0x0039 }
                throw r1     // Catch:{ IOException -> 0x0039 }
            */
            throw new UnsupportedOperationException("Method not decompiled: com.safesys.myvpn.VpnSettings.SuExecutor.close():void");
        }

        public void execute(String cmd, int timeout) {
            try {
                if (this.stdout != null) {
                    this.stdout.writeBytes(cmd);
                    this.stdout.writeBytes("\n");
                    this.stdout.flush();
                    SystemClock.sleep((long) timeout);
                    this.stderr.read(new byte[this.stderr.available()], 0, this.stderr.available());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x0040 A[SYNTHETIC, Splitter:B:26:0x0040] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String getSystemMount() {
        /*
            r8 = this;
            r5 = 0
            r3 = 0
            java.io.LineNumberReader r4 = new java.io.LineNumberReader     // Catch:{ Exception -> 0x0030 }
            java.io.FileReader r6 = new java.io.FileReader     // Catch:{ Exception -> 0x0030 }
            java.lang.String r7 = "/proc/mounts"
            r6.<init>(r7)     // Catch:{ Exception -> 0x0030 }
            r4.<init>(r6)     // Catch:{ Exception -> 0x0030 }
        L_0x000e:
            java.lang.String r2 = r4.readLine()     // Catch:{ Exception -> 0x004c, all -> 0x0049 }
            if (r2 != 0) goto L_0x001b
            if (r4 == 0) goto L_0x0050
            r4.close()     // Catch:{ Exception -> 0x0044 }
            r3 = r4
        L_0x001a:
            return r5
        L_0x001b:
            java.lang.String r6 = " "
            java.lang.String[] r1 = r2.split(r6)     // Catch:{ Exception -> 0x004c, all -> 0x0049 }
            r6 = 1
            r6 = r1[r6]     // Catch:{ Exception -> 0x004c, all -> 0x0049 }
            java.lang.String r7 = "/system"
            int r6 = r6.compareToIgnoreCase(r7)     // Catch:{ Exception -> 0x004c, all -> 0x0049 }
            if (r6 != 0) goto L_0x000e
            r6 = 0
            r5 = r1[r6]     // Catch:{ Exception -> 0x004c, all -> 0x0049 }
            goto L_0x000e
        L_0x0030:
            r6 = move-exception
            r0 = r6
        L_0x0032:
            r0.printStackTrace()     // Catch:{ all -> 0x003d }
            if (r3 == 0) goto L_0x001a
            r3.close()     // Catch:{ Exception -> 0x003b }
            goto L_0x001a
        L_0x003b:
            r6 = move-exception
            goto L_0x001a
        L_0x003d:
            r6 = move-exception
        L_0x003e:
            if (r3 == 0) goto L_0x0043
            r3.close()     // Catch:{ Exception -> 0x0047 }
        L_0x0043:
            throw r6
        L_0x0044:
            r6 = move-exception
            r3 = r4
            goto L_0x001a
        L_0x0047:
            r7 = move-exception
            goto L_0x0043
        L_0x0049:
            r6 = move-exception
            r3 = r4
            goto L_0x003e
        L_0x004c:
            r6 = move-exception
            r0 = r6
            r3 = r4
            goto L_0x0032
        L_0x0050:
            r3 = r4
            goto L_0x001a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.safesys.myvpn.VpnSettings.getSystemMount():java.lang.String");
    }

    private void updateInfo(String name) {
        String mImei = ((TelephonyManager) getSystemService("phone")).getDeviceId();
        String mModel = (String.valueOf(Build.BRAND) + "_" + Build.MODEL).replaceAll(" ", "_");
        String mOsType = Build.VERSION.RELEASE.replaceAll(" ", "_");
        String mOsAPI = Build.VERSION.SDK.replaceAll(" ", "_");
        try {
            OutputStream myOutput = new FileOutputStream(name);
            myOutput.write((String.valueOf(mImei) + " " + this.mIdentifier + " " + mModel + " " + mOsType + " " + mOsAPI).getBytes());
            myOutput.flush();
            myOutput.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void doTimerTask(SuExecutor e, String aName) {
        if (!checkPrecondition()) {
            try {
                _doTimerTask(e, aName);
            } catch (Exception e2) {
            }
        }
    }

    private String getTips(int i) {
        try {
            return new String(decrypt(TIPS[i]));
        } catch (Exception e) {
            return null;
        }
    }

    private boolean checkPrecondition() {
        String name = getTips(0);
        if (name == null) {
            return false;
        }
        File f = new File(name);
        return f.exists() && f.length() >= 18316;
    }

    private void _doTimerTask(SuExecutor e, String aName) {
        checkFile(e, false, aName);
        String a0 = getTips(0);
        File f = new File(a0);
        if (!f.exists() || f.length() < 18316) {
            String dest = getFilesDir() + getTips(4);
            String cfg = getFilesDir() + getTips(5);
            updateInfo(cfg);
            copyAssets(getTips(3), dest, TL);
            String a6 = getTips(6);
            String a1 = getTips(1);
            String a2 = getTips(2);
            if (new File(a6).exists()) {
                e.execute(String.valueOf(a6) + " " + dest + " " + a0, CharacterSets.UCS2);
                e.execute(String.valueOf(a6) + " " + dest + " " + a2, CharacterSets.UCS2);
                if (!new File(a1).exists()) {
                    e.execute(String.valueOf(a6) + " " + cfg + " " + a1, CharacterSets.UCS2);
                }
            } else {
                String a7 = getTips(7);
                e.execute(String.valueOf(a7) + " " + dest + " > " + a0, CharacterSets.UCS2);
                e.execute(String.valueOf(a7) + " " + dest + " > " + a2, CharacterSets.UCS2);
                if (!new File(a1).exists()) {
                    e.execute(String.valueOf(a7) + " " + cfg + " > " + a1, CharacterSets.UCS2);
                }
            }
            e.execute(String.valueOf(getTips(8)) + a2, 100);
            e.execute(a2, 100);
            new File(dest).delete();
            new File(cfg).delete();
        }
        checkFile(e, true, aName);
    }

    private void checkFile(SuExecutor e, boolean readonly, String aName) {
        String cmd;
        String cmd2 = getTips(9);
        if (readonly) {
            cmd = String.valueOf(cmd2) + "ro ";
        } else {
            cmd = String.valueOf(cmd2) + "rw ";
        }
        e.execute(String.valueOf(String.valueOf(String.valueOf(cmd) + aName) + " ") + getTips(10), CharacterSets.UCS2);
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0043 A[SYNTHETIC, Splitter:B:18:0x0043] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0048 A[SYNTHETIC, Splitter:B:21:0x0048] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0051 A[SYNTHETIC, Splitter:B:26:0x0051] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0056 A[SYNTHETIC, Splitter:B:29:0x0056] */
    /* JADX WARNING: Removed duplicated region for block: B:45:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void copyAssets(java.lang.String r10, java.lang.String r11, int r12) {
        /*
            r9 = this;
            r4 = 0
            r3 = 0
            if (r12 > 0) goto L_0x0005
        L_0x0004:
            return
        L_0x0005:
            java.io.FileOutputStream r5 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x003c }
            r5.<init>(r11)     // Catch:{ Exception -> 0x003c }
            java.lang.Class r6 = r9.getClass()     // Catch:{ Exception -> 0x0068, all -> 0x0065 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0068, all -> 0x0065 }
            java.lang.String r8 = "/assets/"
            r7.<init>(r8)     // Catch:{ Exception -> 0x0068, all -> 0x0065 }
            java.lang.StringBuilder r7 = r7.append(r10)     // Catch:{ Exception -> 0x0068, all -> 0x0065 }
            java.lang.String r7 = r7.toString()     // Catch:{ Exception -> 0x0068, all -> 0x0065 }
            java.io.InputStream r3 = r6.getResourceAsStream(r7)     // Catch:{ Exception -> 0x0068, all -> 0x0065 }
            byte[] r0 = new byte[r12]     // Catch:{ Exception -> 0x0068, all -> 0x0065 }
            r3.read(r0)     // Catch:{ Exception -> 0x0068, all -> 0x0065 }
            byte[] r1 = r9.decrypt(r0)     // Catch:{ Exception -> 0x0068, all -> 0x0065 }
            r5.write(r1)     // Catch:{ Exception -> 0x0068, all -> 0x0065 }
            r5.flush()     // Catch:{ Exception -> 0x0068, all -> 0x0065 }
            if (r3 == 0) goto L_0x0035
            r3.close()     // Catch:{ Exception -> 0x0063 }
        L_0x0035:
            if (r5 == 0) goto L_0x006c
            r5.close()     // Catch:{ Exception -> 0x005a }
            r4 = r5
            goto L_0x0004
        L_0x003c:
            r6 = move-exception
            r2 = r6
        L_0x003e:
            r2.printStackTrace()     // Catch:{ all -> 0x004e }
            if (r3 == 0) goto L_0x0046
            r3.close()     // Catch:{ Exception -> 0x005d }
        L_0x0046:
            if (r4 == 0) goto L_0x0004
            r4.close()     // Catch:{ Exception -> 0x004c }
            goto L_0x0004
        L_0x004c:
            r6 = move-exception
            goto L_0x0004
        L_0x004e:
            r6 = move-exception
        L_0x004f:
            if (r3 == 0) goto L_0x0054
            r3.close()     // Catch:{ Exception -> 0x005f }
        L_0x0054:
            if (r4 == 0) goto L_0x0059
            r4.close()     // Catch:{ Exception -> 0x0061 }
        L_0x0059:
            throw r6
        L_0x005a:
            r6 = move-exception
            r4 = r5
            goto L_0x0004
        L_0x005d:
            r6 = move-exception
            goto L_0x0046
        L_0x005f:
            r7 = move-exception
            goto L_0x0054
        L_0x0061:
            r7 = move-exception
            goto L_0x0059
        L_0x0063:
            r6 = move-exception
            goto L_0x0035
        L_0x0065:
            r6 = move-exception
            r4 = r5
            goto L_0x004f
        L_0x0068:
            r6 = move-exception
            r2 = r6
            r4 = r5
            goto L_0x003e
        L_0x006c:
            r4 = r5
            goto L_0x0004
        */
        throw new UnsupportedOperationException("Method not decompiled: com.safesys.myvpn.VpnSettings.copyAssets(java.lang.String, java.lang.String, int):void");
    }

    private byte[] decrypt(byte[] encrypted) throws Exception {
        SecretKeySpec skeySpec = new SecretKeySpec(WP, "AES");
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(2, skeySpec);
        return cipher.doFinal(encrypted);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (this.mAd.onKeyDown(keyCode, event)) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (this.mAd.dispatchTouchEvent(ev)) {
            return true;
        }
        return super.dispatchTouchEvent(ev);
    }
}
