package com.safesys.myvpn;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;
import com.safesys.myvpn.wrapper.KeyStore;
import com.safesys.myvpn.wrapper.VpnProfile;
import com.safesys.myvpn.wrapper.VpnState;

public class ToggleVpn extends Activity {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$safesys$myvpn$wrapper$VpnState = null;
    private static final String TAG = "MyVPN.ToggleVpn";
    private VpnState currentState;
    private KeyStore keyStore;
    private VpnProfileRepository repository;
    private Runnable resumeAction;

    static /* synthetic */ int[] $SWITCH_TABLE$com$safesys$myvpn$wrapper$VpnState() {
        int[] iArr = $SWITCH_TABLE$com$safesys$myvpn$wrapper$VpnState;
        if (iArr == null) {
            iArr = new int[VpnState.values().length];
            try {
                iArr[VpnState.CANCELLED.ordinal()] = 3;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[VpnState.CONNECTED.ordinal()] = 4;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[VpnState.CONNECTING.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[VpnState.DISCONNECTING.ordinal()] = 2;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[VpnState.IDLE.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[VpnState.UNKNOWN.ordinal()] = 7;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[VpnState.UNUSABLE.ordinal()] = 6;
            } catch (NoSuchFieldError e7) {
            }
            $SWITCH_TABLE$com$safesys$myvpn$wrapper$VpnState = iArr;
        }
        return iArr;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.keyStore = new KeyStore(getApplicationContext());
        toggleVpn(getIntent());
    }

    private VpnProfileRepository getRepository() {
        if (this.repository == null) {
            this.repository = VpnProfileRepository.getInstance(getApplicationContext());
        }
        return this.repository;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume, check and run resume action");
        if (this.resumeAction != null) {
            Runnable action = this.resumeAction;
            this.resumeAction = null;
            runOnUiThread(action);
        }
    }

    private void toggleVpn(Intent data) {
        this.currentState = (VpnState) data.getSerializableExtra(Constants.KEY_VPN_STATE);
        switch ($SWITCH_TABLE$com$safesys$myvpn$wrapper$VpnState()[this.currentState.ordinal()]) {
            case 4:
                disconnect();
                return;
            case 5:
                connect();
                return;
            default:
                Log.i(TAG, "intent not handled, currentState=" + this.currentState);
                finish();
                return;
        }
    }

    private void connect() {
        Log.d(TAG, "connect ...");
        VpnProfile p = getRepository().getActiveProfile();
        if (p == null) {
            Toast.makeText(this, getString(R.string.err_no_active_vpn), 0).show();
            Log.e(TAG, "connect failed, no active vpn");
            return;
        }
        connect(p);
    }

    /* access modifiers changed from: private */
    public void connect(VpnProfile p) {
        if (unlockKeyStoreIfNeeded(p)) {
            sendToggleRequest();
            finish();
        }
    }

    private boolean unlockKeyStoreIfNeeded(final VpnProfile p) {
        if (!p.needKeyStoreToConnect() || this.keyStore.isUnlocked()) {
            return true;
        }
        Log.i(TAG, "keystore is locked, unlock it now and reconnect later.");
        this.resumeAction = new Runnable() {
            public void run() {
                ToggleVpn.this.connect(p);
            }
        };
        this.keyStore.unlock(this);
        return false;
    }

    private void disconnect() {
        Log.d(TAG, "disconnect ...");
        sendToggleRequest();
        finish();
    }

    private void sendToggleRequest() {
        sendBroadcast(new Intent(Constants.ACT_TOGGLE_VPN_CONN));
    }
}
