package com.safesys.myvpn;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import com.safesys.myvpn.wrapper.VpnType;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class VpnTypeSelection extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.vpn_type_list);
        ListView listVpnTypes = (ListView) findViewById(R.id.listVpnTypes);
        List<Map<String, ?>> content = new ArrayList<>();
        for (VpnType vpnType : VpnType.values()) {
            Map<String, String> row = new HashMap<>();
            row.put("name", String.valueOf(getString(R.string.add)) + " " + getString(vpnType.getNameRid()));
            row.put("desc", getString(vpnType.getDescRid()));
            content.add(row);
        }
        listVpnTypes.setAdapter((ListAdapter) new SimpleAdapter(this, content, R.layout.vpn_type, new String[]{"name", "desc"}, new int[]{R.id.txtVpnType, R.id.txtVpnTypeDesc}));
        listVpnTypes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                VpnType vpnType = VpnType.values()[position];
                Log.i("MyVPN", vpnType + " picked");
                Intent data = new Intent(VpnTypeSelection.this, VpnSettings.class);
                data.putExtra(Constants.KEY_VPN_TYPE, vpnType);
                VpnTypeSelection.this.setResult(-1, data);
                VpnTypeSelection.this.finish();
            }
        });
    }
}
