package com.ju6;

import java.util.GregorianCalendar;

public class AdManager {
    private static boolean a = false;
    private static long b;
    private static String c = "";
    private static String d = "";
    private static boolean e = false;
    private static Gender f;
    private static GregorianCalendar g;

    public enum Gender {
        MALE,
        FEMALE
    }

    public static void init(String appid, String password, boolean testmode) {
        c = appid;
        d = password;
        e = testmode;
    }

    public static boolean isAdFinish() {
        return a;
    }

    public static void setAdFinish() {
        a = true;
    }

    public static String getAppid() {
        return c;
    }

    public static String getPwd() {
        return d;
    }

    public static boolean isTestmode() {
        return e;
    }

    public static long getAdTimestamp() {
        return b;
    }

    public static void setAdTimestamp() {
        b = System.currentTimeMillis();
    }

    public static Gender getGender() {
        return f;
    }

    public static String getGenderAsString() {
        if (f == Gender.MALE) {
            return "m";
        }
        if (f == Gender.FEMALE) {
            return "f";
        }
        return null;
    }

    public static void setGender(Gender gender) {
        f = gender;
    }

    public static GregorianCalendar getBirthday() {
        return g;
    }

    public static String getBirthdayAsString() {
        GregorianCalendar birthday = getBirthday();
        if (birthday == null) {
            return null;
        }
        return String.format("%04d%02d%02d", Integer.valueOf(birthday.get(1)), Integer.valueOf(birthday.get(2) + 1), Integer.valueOf(birthday.get(5)));
    }

    public static void setBirthday(GregorianCalendar birthday) {
        g = birthday;
    }

    public static void setBirthday(int year, int month, int day) {
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.set(year, month - 1, day);
        setBirthday(gregorianCalendar);
    }
}
