package com.ju6;

import android.content.Context;

public class AdRequester {
    private Context a;
    private AdListener b;

    public AdRequester(Context context) {
        this.a = context;
    }

    public void setAdListener(AdListener paramAdListener) {
        try {
            this.b = paramAdListener;
        } catch (Exception e) {
        }
    }

    public void getAd() {
        new c(this.a, this.b).start();
    }
}
