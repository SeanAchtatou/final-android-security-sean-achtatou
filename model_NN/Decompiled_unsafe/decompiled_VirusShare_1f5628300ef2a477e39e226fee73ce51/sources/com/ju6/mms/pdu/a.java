package com.ju6.mms.pdu;

import com.ju6.mms.InvalidHeaderValueException;

class a extends GenericPdu {
    private PduBody b;

    public a() {
    }

    public a(PduHeaders pduHeaders, PduBody pduBody) {
        super(pduHeaders);
        this.b = pduBody;
    }

    a(PduHeaders pduHeaders) {
        super(pduHeaders);
    }

    public PduBody getBody() {
        return this.b;
    }

    public void setBody(PduBody body) {
        this.b = body;
    }

    public EncodedStringValue getSubject() {
        return this.a.getEncodedStringValue(150);
    }

    public void setSubject(EncodedStringValue value) {
        this.a.setEncodedStringValue(value, 150);
    }

    public EncodedStringValue[] getTo() {
        return this.a.getEncodedStringValues(151);
    }

    public void addTo(EncodedStringValue value) {
        this.a.appendEncodedStringValue(value, 151);
    }

    public int getPriority() {
        return this.a.getOctet(143);
    }

    public void setPriority(int value) throws InvalidHeaderValueException {
        this.a.setOctet(value, 143);
    }

    public long getDate() {
        return this.a.getLongInteger(133);
    }

    public void setDate(long value) {
        this.a.setLongInteger(value, 133);
    }
}
