package com.ju6.mms.pdu;

import android.util.Log;
import com.ju6.mms.ContentType;
import com.ju6.mms.InvalidHeaderValueException;

public class MMS extends a {
    public /* bridge */ /* synthetic */ void addTo(EncodedStringValue encodedStringValue) {
        super.addTo(encodedStringValue);
    }

    public /* bridge */ /* synthetic */ PduBody getBody() {
        return super.getBody();
    }

    public /* bridge */ /* synthetic */ long getDate() {
        return super.getDate();
    }

    public /* bridge */ /* synthetic */ int getPriority() {
        return super.getPriority();
    }

    public /* bridge */ /* synthetic */ EncodedStringValue getSubject() {
        return super.getSubject();
    }

    public /* bridge */ /* synthetic */ EncodedStringValue[] getTo() {
        return super.getTo();
    }

    public /* bridge */ /* synthetic */ void setBody(PduBody pduBody) {
        super.setBody(pduBody);
    }

    public /* bridge */ /* synthetic */ void setDate(long j) {
        super.setDate(j);
    }

    public /* bridge */ /* synthetic */ void setPriority(int i) throws InvalidHeaderValueException {
        super.setPriority(i);
    }

    public /* bridge */ /* synthetic */ void setSubject(EncodedStringValue encodedStringValue) {
        super.setSubject(encodedStringValue);
    }

    public MMS() {
        try {
            setMessageType(132);
            setMmsVersion(18);
            setContentType(ContentType.MULTIPART_RELATED.getBytes());
            setFrom(new EncodedStringValue(PduHeaders.FROM_INSERT_ADDRESS_TOKEN_STR.getBytes()));
            setTransactionId(("T" + Long.toHexString(System.currentTimeMillis())).getBytes());
        } catch (InvalidHeaderValueException e) {
            Log.e("MMS", "Unexpected InvalidHeaderValueException.", e);
        }
    }

    public MMS(byte[] contentType, EncodedStringValue from, int mmsVersion, byte[] transactionId) throws InvalidHeaderValueException {
        setMessageType(128);
        setContentType(contentType);
        setFrom(from);
        setMmsVersion(mmsVersion);
        setTransactionId(transactionId);
    }

    public EncodedStringValue[] getBcc() {
        return this.a.getEncodedStringValues(129);
    }

    public void addBcc(EncodedStringValue value) {
        this.a.appendEncodedStringValue(value, 129);
    }

    public void setBcc(EncodedStringValue[] value) {
        this.a.setEncodedStringValues(value, 129);
    }

    public EncodedStringValue[] getCc() {
        return this.a.getEncodedStringValues(130);
    }

    public void addCc(EncodedStringValue value) {
        this.a.appendEncodedStringValue(value, 130);
    }

    public void setCc(EncodedStringValue[] value) {
        this.a.setEncodedStringValues(value, 130);
    }

    public byte[] getContentType() {
        return this.a.getTextString(132);
    }

    public void setContentType(byte[] value) {
        this.a.setTextString(value, 132);
    }

    public int getDeliveryReport() {
        return this.a.getOctet(134);
    }

    public void setDeliveryReport(int value) throws InvalidHeaderValueException {
        this.a.setOctet(value, 134);
    }

    public long getExpiry() {
        return this.a.getLongInteger(136);
    }

    public void setExpiry(long value) {
        this.a.setLongInteger(value, 136);
    }

    public long getMessageSize() {
        return this.a.getLongInteger(142);
    }

    public void setMessageSize(long value) {
        this.a.setLongInteger(value, 142);
    }

    public byte[] getMessageClass() {
        return this.a.getTextString(138);
    }

    public void setMessageClass(byte[] value) {
        this.a.setTextString(value, 138);
    }

    public int getReadReport() {
        return this.a.getOctet(144);
    }

    public void setReadReport(int value) throws InvalidHeaderValueException {
        this.a.setOctet(value, 144);
    }

    public void setTo(EncodedStringValue[] value) {
        this.a.setEncodedStringValues(value, 151);
    }

    public byte[] getTransactionId() {
        return this.a.getTextString(152);
    }

    public void setTransactionId(byte[] value) {
        this.a.setTextString(value, 152);
    }
}
