package com.ju6.mms.pdu;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;
import com.ju6.mms.ContentType;
import com.ju6.mms.MmsException;
import com.ju6.mms.util.PduCache;
import com.ju6.mms.util.SqliteWrapper;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PduPersister {
    public static final int PROC_STATUS_COMPLETED = 3;
    public static final int PROC_STATUS_PERMANENTLY_FAILURE = 2;
    public static final int PROC_STATUS_TRANSIENT_FAILURE = 1;
    public static final String TEMPORARY_DRM_OBJECT_URI = "content://mms/9223372036854775807/part";
    private static PduPersister a;
    private static final PduCache b = PduCache.getInstance();
    private static final int[] c = {129, 130, 137, 151};
    private static final HashMap<Uri, Integer> d;
    private static final HashMap<Integer, Integer> e;
    private static final HashMap<Integer, Integer> f;
    private static final HashMap<Integer, Integer> g;
    private static final HashMap<Integer, Integer> h;
    private static final HashMap<Integer, Integer> i;
    private static final HashMap<Integer, String> j;
    private static final HashMap<Integer, String> k;
    private static final HashMap<Integer, String> l;
    private static final HashMap<Integer, String> m;
    private static final HashMap<Integer, String> n;
    private final Context o;
    private final ContentResolver p;

    public interface BaseMmsColumns {
        public static final String ADAPTATION_ALLOWED = "adp_a";
        public static final String APPLIC_ID = "apl_id";
        public static final String AUX_APPLIC_ID = "aux_apl_id";
        public static final String BCC = "bcc";
        public static final String CANCEL_ID = "cl_id";
        public static final String CANCEL_STATUS = "cl_st";
        public static final String CC = "cc";
        public static final String CONTENT_CLASS = "ct_cls";
        public static final String CONTENT_LOCATION = "ct_l";
        public static final String CONTENT_TYPE = "ct_t";
        public static final String DATE = "date";
        public static final String DELIVERY_REPORT = "d_rpt";
        public static final String DELIVERY_TIME = "d_tm";
        public static final String DELIVERY_TIME_TOKEN = "d_tm_tok";
        public static final String DISTRIBUTION_INDICATOR = "d_ind";
        public static final String DRM_CONTENT = "drm_c";
        public static final String ELEMENT_DESCRIPTOR = "e_des";
        public static final String EXPIRY = "exp";
        public static final String FROM = "from";
        public static final String LIMIT = "limit";
        public static final String LOCKED = "locked";
        public static final String MBOX_QUOTAS = "mb_qt";
        public static final String MBOX_QUOTAS_TOKEN = "mb_qt_tok";
        public static final String MBOX_TOTALS = "mb_t";
        public static final String MBOX_TOTALS_TOKEN = "mb_t_tok";
        public static final String MESSAGE_BOX = "msg_box";
        public static final int MESSAGE_BOX_ALL = 0;
        public static final int MESSAGE_BOX_DRAFTS = 3;
        public static final int MESSAGE_BOX_INBOX = 1;
        public static final int MESSAGE_BOX_OUTBOX = 4;
        public static final int MESSAGE_BOX_SENT = 2;
        public static final String MESSAGE_CLASS = "m_cls";
        public static final String MESSAGE_COUNT = "m_cnt";
        public static final String MESSAGE_ID = "m_id";
        public static final String MESSAGE_SIZE = "m_size";
        public static final String MESSAGE_TYPE = "m_type";
        public static final String META_DATA = "meta_data";
        public static final String MMS_VERSION = "v";
        public static final String MM_FLAGS = "mm_flg";
        public static final String MM_FLAGS_TOKEN = "mm_flg_tok";
        public static final String MM_STATE = "mm_st";
        public static final String PREVIOUSLY_SENT_BY = "p_s_by";
        public static final String PREVIOUSLY_SENT_DATE = "p_s_d";
        public static final String PRIORITY = "pri";
        public static final String QUOTAS = "qt";
        public static final String READ = "read";
        public static final String READ_REPORT = "rr";
        public static final String READ_STATUS = "read_status";
        public static final String RECOMMENDED_RETRIEVAL_MODE = "r_r_mod";
        public static final String RECOMMENDED_RETRIEVAL_MODE_TEXT = "r_r_mod_txt";
        public static final String REPLACE_ID = "repl_id";
        public static final String REPLY_APPLIC_ID = "r_apl_id";
        public static final String REPLY_CHARGING = "r_chg";
        public static final String REPLY_CHARGING_DEADLINE = "r_chg_dl";
        public static final String REPLY_CHARGING_DEADLINE_TOKEN = "r_chg_dl_tok";
        public static final String REPLY_CHARGING_ID = "r_chg_id";
        public static final String REPLY_CHARGING_SIZE = "r_chg_sz";
        public static final String REPORT_ALLOWED = "rpt_a";
        public static final String RESPONSE_STATUS = "resp_st";
        public static final String RESPONSE_TEXT = "resp_txt";
        public static final String RETRIEVE_STATUS = "retr_st";
        public static final String RETRIEVE_TEXT = "retr_txt";
        public static final String RETRIEVE_TEXT_CHARSET = "retr_txt_cs";
        public static final String SEEN = "seen";
        public static final String SENDER_VISIBILITY = "s_vis";
        public static final String START = "start";
        public static final String STATUS = "st";
        public static final String STATUS_TEXT = "st_txt";
        public static final String STORE = "store";
        public static final String STORED = "stored";
        public static final String STORE_STATUS = "store_st";
        public static final String STORE_STATUS_TEXT = "store_st_txt";
        public static final String SUBJECT = "sub";
        public static final String SUBJECT_CHARSET = "sub_cs";
        public static final String THREAD_ID = "thread_id";
        public static final String TO = "to";
        public static final String TOTALS = "totals";
        public static final String TRANSACTION_ID = "tr_id";
    }

    public interface CanonicalAddressesColumns {
        public static final String ADDRESS = "address";
    }

    public static final class Carriers {
        public static final String APN = "apn";
        public static final String AUTH_TYPE = "authtype";
        public static final Uri CONTENT_URI = Uri.parse("content://telephony/carriers");
        public static final String CURRENT = "current";
        public static final String DEFAULT_SORT_ORDER = "name ASC";
        public static final String MCC = "mcc";
        public static final String MMSC = "mmsc";
        public static final String MMSPORT = "mmsport";
        public static final String MMSPROXY = "mmsproxy";
        public static final String MNC = "mnc";
        public static final String NAME = "name";
        public static final String NUMERIC = "numeric";
        public static final String PASSWORD = "password";
        public static final String PORT = "port";
        public static final String PROXY = "proxy";
        public static final String SERVER = "server";
        public static final String TYPE = "type";
        public static final String USER = "user";
    }

    public static final class MmsSms {
        public static final Uri CONTENT_CONVERSATIONS_URI = Uri.parse("content://mms-sms/conversations");
        public static final Uri CONTENT_DRAFT_URI = Uri.parse("content://mms-sms/draft");
        public static final Uri CONTENT_FILTER_BYPHONE_URI = Uri.parse("content://mms-sms/messages/byphone");
        public static final Uri CONTENT_LOCKED_URI = Uri.parse("content://mms-sms/locked");
        public static final Uri CONTENT_UNDELIVERED_URI = Uri.parse("content://mms-sms/undelivered");
        public static final Uri CONTENT_URI = Uri.parse("content://mms-sms/");
        public static final int ERR_TYPE_GENERIC = 1;
        public static final int ERR_TYPE_GENERIC_PERMANENT = 10;
        public static final int ERR_TYPE_MMS_PROTO_PERMANENT = 12;
        public static final int ERR_TYPE_MMS_PROTO_TRANSIENT = 3;
        public static final int ERR_TYPE_SMS_PROTO_PERMANENT = 11;
        public static final int ERR_TYPE_SMS_PROTO_TRANSIENT = 2;
        public static final int ERR_TYPE_TRANSPORT_FAILURE = 4;
        public static final int MMS_PROTO = 1;
        public static final int NO_ERROR = 0;
        public static final Uri SEARCH_URI = Uri.parse("content://mms-sms/search");
        public static final int SMS_PROTO = 0;
        public static final String TYPE_DISCRIMINATOR_COLUMN = "transport_type";

        public static final class PendingMessages {
            public static final Uri CONTENT_URI = Uri.withAppendedPath(MmsSms.CONTENT_URI, "pending");
            public static final String DUE_TIME = "due_time";
            public static final String ERROR_CODE = "err_code";
            public static final String ERROR_TYPE = "err_type";
            public static final String LAST_TRY = "last_try";
            public static final String MSG_ID = "msg_id";
            public static final String MSG_TYPE = "msg_type";
            public static final String PROTO_TYPE = "proto_type";
            public static final String RETRY_INDEX = "retry_index";
        }

        public static final class WordsTable {
            public static final String ID = "_id";
            public static final String INDEXED_TEXT = "index_text";
            public static final String SOURCE_ROW_ID = "source_id";
            public static final String TABLE_ID = "table_to_use";
        }
    }

    public interface TextBasedSmsColumns {
        public static final String ADDRESS = "address";
        public static final String BODY = "body";
        public static final String DATE = "date";
        public static final String ERROR_CODE = "error_code";
        public static final String LOCKED = "locked";
        public static final int MESSAGE_TYPE_ALL = 0;
        public static final int MESSAGE_TYPE_DRAFT = 3;
        public static final int MESSAGE_TYPE_FAILED = 5;
        public static final int MESSAGE_TYPE_INBOX = 1;
        public static final int MESSAGE_TYPE_OUTBOX = 4;
        public static final int MESSAGE_TYPE_QUEUED = 6;
        public static final int MESSAGE_TYPE_SENT = 2;
        public static final String META_DATA = "meta_data";
        public static final String PERSON = "person";
        public static final String PERSON_ID = "person";
        public static final String PROTOCOL = "protocol";
        public static final String READ = "read";
        public static final String REPLY_PATH_PRESENT = "reply_path_present";
        public static final String SEEN = "seen";
        public static final String SERVICE_CENTER = "service_center";
        public static final String STATUS = "status";
        public static final int STATUS_COMPLETE = 0;
        public static final int STATUS_FAILED = 64;
        public static final int STATUS_NONE = -1;
        public static final int STATUS_PENDING = 32;
        public static final String SUBJECT = "subject";
        public static final String THREAD_ID = "thread_id";
        public static final String TYPE = "type";
    }

    public interface ThreadsColumns {
        public static final String DATE = "date";
        public static final String ERROR = "error";
        public static final String HAS_ATTACHMENT = "has_attachment";
        public static final String MESSAGE_COUNT = "message_count";
        public static final String READ = "read";
        public static final String RECIPIENT_IDS = "recipient_ids";
        public static final String SNIPPET = "snippet";
        public static final String SNIPPET_CHARSET = "snippet_cs";
        public static final String TYPE = "type";
    }

    static {
        String[] strArr = {MmsSms.WordsTable.ID, BaseMmsColumns.MESSAGE_BOX, "thread_id", BaseMmsColumns.RETRIEVE_TEXT, BaseMmsColumns.SUBJECT, BaseMmsColumns.CONTENT_LOCATION, BaseMmsColumns.CONTENT_TYPE, BaseMmsColumns.MESSAGE_CLASS, BaseMmsColumns.MESSAGE_ID, BaseMmsColumns.RESPONSE_TEXT, BaseMmsColumns.TRANSACTION_ID, BaseMmsColumns.CONTENT_CLASS, BaseMmsColumns.DELIVERY_REPORT, BaseMmsColumns.MESSAGE_TYPE, BaseMmsColumns.MMS_VERSION, BaseMmsColumns.PRIORITY, BaseMmsColumns.READ_REPORT, BaseMmsColumns.READ_STATUS, BaseMmsColumns.REPORT_ALLOWED, BaseMmsColumns.RETRIEVE_STATUS, BaseMmsColumns.STATUS, "date", BaseMmsColumns.DELIVERY_TIME, BaseMmsColumns.EXPIRY, BaseMmsColumns.MESSAGE_SIZE, BaseMmsColumns.SUBJECT_CHARSET, BaseMmsColumns.RETRIEVE_TEXT_CHARSET};
        String[] strArr2 = {MmsSms.WordsTable.ID, Mms.Part.CHARSET, Mms.Part.CONTENT_DISPOSITION, Mms.Part.CONTENT_ID, Mms.Part.CONTENT_LOCATION, Mms.Part.CONTENT_TYPE, Mms.Part.FILENAME, "name", Mms.Part.TEXT};
        HashMap<Uri, Integer> hashMap = new HashMap<>();
        d = hashMap;
        hashMap.put(Mms.Inbox.CONTENT_URI, 1);
        d.put(Mms.Sent.CONTENT_URI, 2);
        d.put(Mms.Draft.CONTENT_URI, 3);
        d.put(Mms.Outbox.CONTENT_URI, 4);
        HashMap<Integer, Integer> hashMap2 = new HashMap<>();
        e = hashMap2;
        hashMap2.put(150, 25);
        e.put(154, 26);
        HashMap<Integer, String> hashMap3 = new HashMap<>();
        j = hashMap3;
        hashMap3.put(150, BaseMmsColumns.SUBJECT_CHARSET);
        j.put(154, BaseMmsColumns.RETRIEVE_TEXT_CHARSET);
        HashMap<Integer, Integer> hashMap4 = new HashMap<>();
        f = hashMap4;
        hashMap4.put(154, 3);
        f.put(150, 4);
        HashMap<Integer, String> hashMap5 = new HashMap<>();
        k = hashMap5;
        hashMap5.put(154, BaseMmsColumns.RETRIEVE_TEXT);
        k.put(150, BaseMmsColumns.SUBJECT);
        HashMap<Integer, Integer> hashMap6 = new HashMap<>();
        g = hashMap6;
        hashMap6.put(131, 5);
        g.put(132, 6);
        g.put(138, 7);
        g.put(139, 8);
        g.put(147, 9);
        g.put(152, 10);
        HashMap<Integer, String> hashMap7 = new HashMap<>();
        l = hashMap7;
        hashMap7.put(131, BaseMmsColumns.CONTENT_LOCATION);
        l.put(132, BaseMmsColumns.CONTENT_TYPE);
        l.put(138, BaseMmsColumns.MESSAGE_CLASS);
        l.put(139, BaseMmsColumns.MESSAGE_ID);
        l.put(147, BaseMmsColumns.RESPONSE_TEXT);
        l.put(152, BaseMmsColumns.TRANSACTION_ID);
        HashMap<Integer, Integer> hashMap8 = new HashMap<>();
        h = hashMap8;
        hashMap8.put(Integer.valueOf((int) PduHeaders.CONTENT_CLASS), 11);
        h.put(134, 12);
        h.put(140, 13);
        h.put(141, 14);
        h.put(143, 15);
        h.put(144, 16);
        h.put(155, 17);
        h.put(145, 18);
        h.put(153, 19);
        h.put(149, 20);
        HashMap<Integer, String> hashMap9 = new HashMap<>();
        m = hashMap9;
        hashMap9.put(Integer.valueOf((int) PduHeaders.CONTENT_CLASS), BaseMmsColumns.CONTENT_CLASS);
        m.put(134, BaseMmsColumns.DELIVERY_REPORT);
        m.put(140, BaseMmsColumns.MESSAGE_TYPE);
        m.put(141, BaseMmsColumns.MMS_VERSION);
        m.put(143, BaseMmsColumns.PRIORITY);
        m.put(144, BaseMmsColumns.READ_REPORT);
        m.put(155, BaseMmsColumns.READ_STATUS);
        m.put(145, BaseMmsColumns.REPORT_ALLOWED);
        m.put(153, BaseMmsColumns.RETRIEVE_STATUS);
        m.put(149, BaseMmsColumns.STATUS);
        HashMap<Integer, Integer> hashMap10 = new HashMap<>();
        i = hashMap10;
        hashMap10.put(133, 21);
        i.put(135, 22);
        i.put(136, 23);
        i.put(142, 24);
        HashMap<Integer, String> hashMap11 = new HashMap<>();
        n = hashMap11;
        hashMap11.put(133, "date");
        n.put(135, BaseMmsColumns.DELIVERY_TIME);
        n.put(136, BaseMmsColumns.EXPIRY);
        n.put(142, BaseMmsColumns.MESSAGE_SIZE);
    }

    private PduPersister(Context context) {
        this.o = context;
        this.p = context.getContentResolver();
    }

    public static PduPersister getPduPersister(Context context) {
        if (a == null || !context.equals(a.o)) {
            a = new PduPersister(context);
        }
        return a;
    }

    private void a(long j2, int i2, EncodedStringValue[] encodedStringValueArr) {
        ContentValues contentValues = new ContentValues(3);
        for (EncodedStringValue encodedStringValue : encodedStringValueArr) {
            contentValues.clear();
            contentValues.put("address", toIsoString(encodedStringValue.getTextString()));
            contentValues.put(Mms.Addr.CHARSET, Integer.valueOf(encodedStringValue.getCharacterSet()));
            contentValues.put("type", Integer.valueOf(i2));
            SqliteWrapper.insert(this.o, this.p, Uri.parse("content://mms/" + j2 + "/addr"), contentValues);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public Uri persistPart(PduPart part, long msgId) throws MmsException {
        Uri parse = Uri.parse("content://mms/" + msgId + "/part");
        ContentValues contentValues = new ContentValues(8);
        int charset = part.getCharset();
        if (charset != 0) {
            contentValues.put(Mms.Part.CHARSET, Integer.valueOf(charset));
        }
        if (part.getContentType() != null) {
            String isoString = toIsoString(part.getContentType());
            contentValues.put(Mms.Part.CONTENT_TYPE, isoString);
            if (ContentType.APP_SMIL.equals(isoString)) {
                contentValues.put(Mms.Part.SEQ, (Integer) -1);
            }
            if (part.getFilename() != null) {
                contentValues.put(Mms.Part.FILENAME, new String(part.getFilename()));
            }
            if (part.getName() != null) {
                contentValues.put("name", new String(part.getName()));
            }
            if (part.getContentDisposition() != null) {
                contentValues.put(Mms.Part.CONTENT_DISPOSITION, toIsoString(part.getContentDisposition()));
            }
            if (part.getContentId() != null) {
                contentValues.put(Mms.Part.CONTENT_ID, toIsoString(part.getContentId()));
            }
            if (part.getContentLocation() != null) {
                contentValues.put(Mms.Part.CONTENT_LOCATION, toIsoString(part.getContentLocation()));
            }
            Uri insert = SqliteWrapper.insert(this.o, this.p, parse, contentValues);
            if (insert == null) {
                throw new MmsException("Failed to persist part, return null.");
            }
            a(part, insert, isoString);
            part.setDataUri(insert);
            return insert;
        }
        throw new MmsException("MIME type of the part must be set.");
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x006d A[SYNTHETIC, Splitter:B:22:0x006d] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0072 A[SYNTHETIC, Splitter:B:25:0x0072] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(com.ju6.mms.pdu.PduPart r9, android.net.Uri r10, java.lang.String r11) throws com.ju6.mms.MmsException {
        /*
            r8 = this;
            r4 = 0
            byte[] r0 = r9.getData()     // Catch:{ FileNotFoundException -> 0x005a, IOException -> 0x00ec, all -> 0x0140 }
            java.lang.String r1 = "text/plain"
            boolean r1 = r1.equals(r11)     // Catch:{ FileNotFoundException -> 0x005a, IOException -> 0x00ec, all -> 0x0140 }
            if (r1 != 0) goto L_0x001d
            java.lang.String r1 = "application/smil"
            boolean r1 = r1.equals(r11)     // Catch:{ FileNotFoundException -> 0x005a, IOException -> 0x00ec, all -> 0x0140 }
            if (r1 != 0) goto L_0x001d
            java.lang.String r1 = "text/html"
            boolean r1 = r1.equals(r11)     // Catch:{ FileNotFoundException -> 0x005a, IOException -> 0x00ec, all -> 0x0140 }
            if (r1 == 0) goto L_0x0076
        L_0x001d:
            boolean r1 = r8.a()     // Catch:{ FileNotFoundException -> 0x005a, IOException -> 0x00ec, all -> 0x0140 }
            if (r1 == 0) goto L_0x0076
            android.content.ContentValues r1 = new android.content.ContentValues     // Catch:{ FileNotFoundException -> 0x005a, IOException -> 0x00ec, all -> 0x0140 }
            r1.<init>()     // Catch:{ FileNotFoundException -> 0x005a, IOException -> 0x00ec, all -> 0x0140 }
            java.lang.String r2 = "text"
            com.ju6.mms.pdu.EncodedStringValue r3 = new com.ju6.mms.pdu.EncodedStringValue     // Catch:{ FileNotFoundException -> 0x005a, IOException -> 0x00ec, all -> 0x0140 }
            r3.<init>(r0)     // Catch:{ FileNotFoundException -> 0x005a, IOException -> 0x00ec, all -> 0x0140 }
            java.lang.String r0 = r3.getString()     // Catch:{ FileNotFoundException -> 0x005a, IOException -> 0x00ec, all -> 0x0140 }
            r1.put(r2, r0)     // Catch:{ FileNotFoundException -> 0x005a, IOException -> 0x00ec, all -> 0x0140 }
            android.content.ContentResolver r0 = r8.p     // Catch:{ FileNotFoundException -> 0x005a, IOException -> 0x00ec, all -> 0x0140 }
            r2 = 0
            r3 = 0
            int r0 = r0.update(r10, r1, r2, r3)     // Catch:{ FileNotFoundException -> 0x005a, IOException -> 0x00ec, all -> 0x0140 }
            r1 = 1
            if (r0 == r1) goto L_0x0160
            com.ju6.mms.MmsException r0 = new com.ju6.mms.MmsException     // Catch:{ FileNotFoundException -> 0x005a, IOException -> 0x00ec, all -> 0x0140 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x005a, IOException -> 0x00ec, all -> 0x0140 }
            java.lang.String r2 = "unable to update "
            r1.<init>(r2)     // Catch:{ FileNotFoundException -> 0x005a, IOException -> 0x00ec, all -> 0x0140 }
            java.lang.String r2 = r10.toString()     // Catch:{ FileNotFoundException -> 0x005a, IOException -> 0x00ec, all -> 0x0140 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ FileNotFoundException -> 0x005a, IOException -> 0x00ec, all -> 0x0140 }
            java.lang.String r1 = r1.toString()     // Catch:{ FileNotFoundException -> 0x005a, IOException -> 0x00ec, all -> 0x0140 }
            r0.<init>(r1)     // Catch:{ FileNotFoundException -> 0x005a, IOException -> 0x00ec, all -> 0x0140 }
            throw r0     // Catch:{ FileNotFoundException -> 0x005a, IOException -> 0x00ec, all -> 0x0140 }
        L_0x005a:
            r0 = move-exception
            r1 = r4
            r2 = r4
        L_0x005d:
            java.lang.String r3 = "PduPersister"
            java.lang.String r4 = "Failed to open Input/Output stream."
            android.util.Log.e(r3, r4, r0)     // Catch:{ all -> 0x006a }
            com.ju6.mms.MmsException r3 = new com.ju6.mms.MmsException     // Catch:{ all -> 0x006a }
            r3.<init>(r0)     // Catch:{ all -> 0x006a }
            throw r3     // Catch:{ all -> 0x006a }
        L_0x006a:
            r0 = move-exception
        L_0x006b:
            if (r2 == 0) goto L_0x0070
            r2.close()     // Catch:{ IOException -> 0x00fc }
        L_0x0070:
            if (r1 == 0) goto L_0x0075
            r1.close()     // Catch:{ IOException -> 0x0113 }
        L_0x0075:
            throw r0
        L_0x0076:
            android.content.ContentResolver r1 = r8.p     // Catch:{ FileNotFoundException -> 0x005a, IOException -> 0x00ec, all -> 0x0140 }
            java.io.OutputStream r1 = r1.openOutputStream(r10)     // Catch:{ FileNotFoundException -> 0x005a, IOException -> 0x00ec, all -> 0x0140 }
            if (r0 != 0) goto L_0x00e7
            android.net.Uri r0 = r9.getDataUri()     // Catch:{ FileNotFoundException -> 0x015b, IOException -> 0x0151, all -> 0x0145 }
            if (r0 == 0) goto L_0x0086
            if (r0 != r10) goto L_0x00a9
        L_0x0086:
            java.lang.String r0 = "PduPersister"
            java.lang.String r2 = "Can't find data for this part."
            android.util.Log.w(r0, r2)     // Catch:{ FileNotFoundException -> 0x015b, IOException -> 0x0151, all -> 0x0145 }
            if (r1 == 0) goto L_0x0092
            r1.close()     // Catch:{ IOException -> 0x0093 }
        L_0x0092:
            return
        L_0x0093:
            r0 = move-exception
            java.lang.String r2 = "PduPersister"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "IOException while closing: "
            r3.<init>(r4)
            java.lang.StringBuilder r1 = r3.append(r1)
            java.lang.String r1 = r1.toString()
            android.util.Log.e(r2, r1, r0)
            goto L_0x0092
        L_0x00a9:
            android.content.ContentResolver r2 = r8.p     // Catch:{ FileNotFoundException -> 0x015b, IOException -> 0x0151, all -> 0x0145 }
            java.io.InputStream r0 = r2.openInputStream(r0)     // Catch:{ FileNotFoundException -> 0x015b, IOException -> 0x0151, all -> 0x0145 }
            r2 = 256(0x100, float:3.59E-43)
            byte[] r2 = new byte[r2]     // Catch:{ FileNotFoundException -> 0x00e0, IOException -> 0x0155, all -> 0x014a }
        L_0x00b3:
            int r3 = r0.read(r2)     // Catch:{ FileNotFoundException -> 0x00e0, IOException -> 0x0155, all -> 0x014a }
            r4 = -1
            if (r3 != r4) goto L_0x00db
        L_0x00ba:
            if (r1 == 0) goto L_0x00bf
            r1.close()     // Catch:{ IOException -> 0x012a }
        L_0x00bf:
            if (r0 == 0) goto L_0x0092
            r0.close()     // Catch:{ IOException -> 0x00c5 }
            goto L_0x0092
        L_0x00c5:
            r1 = move-exception
            java.lang.String r2 = "PduPersister"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "IOException while closing: "
            r3.<init>(r4)
            java.lang.StringBuilder r0 = r3.append(r0)
            java.lang.String r0 = r0.toString()
            android.util.Log.e(r2, r0, r1)
            goto L_0x0092
        L_0x00db:
            r4 = 0
            r1.write(r2, r4, r3)     // Catch:{ FileNotFoundException -> 0x00e0, IOException -> 0x0155, all -> 0x014a }
            goto L_0x00b3
        L_0x00e0:
            r2 = move-exception
            r7 = r2
            r2 = r1
            r1 = r0
            r0 = r7
            goto L_0x005d
        L_0x00e7:
            r1.write(r0)     // Catch:{ FileNotFoundException -> 0x015b, IOException -> 0x0151, all -> 0x0145 }
            r0 = r4
            goto L_0x00ba
        L_0x00ec:
            r0 = move-exception
            r1 = r4
            r2 = r4
        L_0x00ef:
            java.lang.String r3 = "PduPersister"
            java.lang.String r4 = "Failed to read/write data."
            android.util.Log.e(r3, r4, r0)     // Catch:{ all -> 0x006a }
            com.ju6.mms.MmsException r3 = new com.ju6.mms.MmsException     // Catch:{ all -> 0x006a }
            r3.<init>(r0)     // Catch:{ all -> 0x006a }
            throw r3     // Catch:{ all -> 0x006a }
        L_0x00fc:
            r3 = move-exception
            java.lang.String r4 = "PduPersister"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            java.lang.String r6 = "IOException while closing: "
            r5.<init>(r6)
            java.lang.StringBuilder r2 = r5.append(r2)
            java.lang.String r2 = r2.toString()
            android.util.Log.e(r4, r2, r3)
            goto L_0x0070
        L_0x0113:
            r2 = move-exception
            java.lang.String r3 = "PduPersister"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r5 = "IOException while closing: "
            r4.<init>(r5)
            java.lang.StringBuilder r1 = r4.append(r1)
            java.lang.String r1 = r1.toString()
            android.util.Log.e(r3, r1, r2)
            goto L_0x0075
        L_0x012a:
            r2 = move-exception
            java.lang.String r3 = "PduPersister"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r5 = "IOException while closing: "
            r4.<init>(r5)
            java.lang.StringBuilder r1 = r4.append(r1)
            java.lang.String r1 = r1.toString()
            android.util.Log.e(r3, r1, r2)
            goto L_0x00bf
        L_0x0140:
            r0 = move-exception
            r1 = r4
            r2 = r4
            goto L_0x006b
        L_0x0145:
            r0 = move-exception
            r2 = r1
            r1 = r4
            goto L_0x006b
        L_0x014a:
            r2 = move-exception
            r7 = r2
            r2 = r1
            r1 = r0
            r0 = r7
            goto L_0x006b
        L_0x0151:
            r0 = move-exception
            r2 = r1
            r1 = r4
            goto L_0x00ef
        L_0x0155:
            r2 = move-exception
            r7 = r2
            r2 = r1
            r1 = r0
            r0 = r7
            goto L_0x00ef
        L_0x015b:
            r0 = move-exception
            r2 = r1
            r1 = r4
            goto L_0x005d
        L_0x0160:
            r0 = r4
            r1 = r4
            goto L_0x00ba
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ju6.mms.pdu.PduPersister.a(com.ju6.mms.pdu.PduPart, android.net.Uri, java.lang.String):void");
    }

    /* JADX INFO: finally extract failed */
    private boolean a() {
        boolean z;
        Cursor query = SqliteWrapper.query(this.o, this.o.getContentResolver(), Uri.parse("content://mms/"), new String[]{" sql from sqlite_master WHERE tbl_name ='part' and type = 'table' -- "}, null, null, null);
        if (query == null) {
            return false;
        }
        try {
            if (query.moveToNext()) {
                String string = query.getString(0);
                if (!TextUtils.isEmpty(string)) {
                    String[] split = string.split(",");
                    int length = split.length;
                    int i2 = 0;
                    while (true) {
                        if (i2 >= length) {
                            z = false;
                            break;
                        } else if (split[i2].trim().startsWith(Mms.Part.TEXT)) {
                            z = true;
                            break;
                        } else {
                            i2++;
                        }
                    }
                    query.close();
                    return z;
                }
            }
            z = false;
            query.close();
            return z;
        } catch (Throwable th) {
            query.close();
            throw th;
        }
    }

    public void updateHeaders(Uri uri, MMS sendReq) {
        b.purge(uri);
        ContentValues contentValues = new ContentValues(10);
        byte[] contentType = sendReq.getContentType();
        if (contentType != null) {
            contentValues.put(BaseMmsColumns.CONTENT_TYPE, toIsoString(contentType));
        }
        long date = sendReq.getDate();
        if (date != -1) {
            contentValues.put("date", Long.valueOf(date));
        }
        int deliveryReport = sendReq.getDeliveryReport();
        if (deliveryReport != 0) {
            contentValues.put(BaseMmsColumns.DELIVERY_REPORT, Integer.valueOf(deliveryReport));
        }
        long expiry = sendReq.getExpiry();
        if (expiry != -1) {
            contentValues.put(BaseMmsColumns.EXPIRY, Long.valueOf(expiry));
        }
        byte[] messageClass = sendReq.getMessageClass();
        if (messageClass != null) {
            contentValues.put(BaseMmsColumns.MESSAGE_CLASS, toIsoString(messageClass));
        }
        int priority = sendReq.getPriority();
        if (priority != 0) {
            contentValues.put(BaseMmsColumns.PRIORITY, Integer.valueOf(priority));
        }
        int readReport = sendReq.getReadReport();
        if (readReport != 0) {
            contentValues.put(BaseMmsColumns.READ_REPORT, Integer.valueOf(readReport));
        }
        byte[] transactionId = sendReq.getTransactionId();
        if (transactionId != null) {
            contentValues.put(BaseMmsColumns.TRANSACTION_ID, toIsoString(transactionId));
        }
        EncodedStringValue subject = sendReq.getSubject();
        if (subject != null) {
            contentValues.put(BaseMmsColumns.SUBJECT, toIsoString(subject.getTextString()));
            contentValues.put(BaseMmsColumns.SUBJECT_CHARSET, Integer.valueOf(subject.getCharacterSet()));
        } else {
            contentValues.put(BaseMmsColumns.SUBJECT, "");
        }
        long messageSize = sendReq.getMessageSize();
        if (messageSize > 0) {
            contentValues.put(BaseMmsColumns.MESSAGE_SIZE, Long.valueOf(messageSize));
        }
        PduHeaders a2 = sendReq.a();
        HashSet hashSet = new HashSet();
        for (int i2 : c) {
            EncodedStringValue[] encodedStringValueArr = null;
            if (i2 == 137) {
                EncodedStringValue encodedStringValue = a2.getEncodedStringValue(i2);
                if (encodedStringValue != null) {
                    encodedStringValueArr = new EncodedStringValue[]{encodedStringValue};
                }
            } else {
                encodedStringValueArr = a2.getEncodedStringValues(i2);
            }
            if (encodedStringValueArr != null) {
                long parseId = ContentUris.parseId(uri);
                SqliteWrapper.delete(this.o, this.p, Uri.parse("content://mms/" + parseId + "/addr"), "type=" + i2, null);
                a(parseId, i2, encodedStringValueArr);
                if (i2 == 151) {
                    for (EncodedStringValue encodedStringValue2 : encodedStringValueArr) {
                        if (encodedStringValue2 != null) {
                            hashSet.add(encodedStringValue2.getString());
                        }
                    }
                }
            }
        }
        contentValues.put("thread_id", Long.valueOf(Threads.getOrCreateThreadId(this.o, hashSet)));
        SqliteWrapper.update(this.o, this.p, uri, contentValues, null, null);
    }

    public Uri persist(GenericPdu pdu, Uri uri) throws MmsException {
        long j2;
        PduBody body;
        EncodedStringValue[] encodedStringValueArr;
        EncodedStringValue[] encodedStringValues;
        if (uri == null) {
            throw new MmsException("Uri may not be null.");
        } else if (d.get(uri) == null) {
            throw new MmsException("Bad destination, must be one of content://mms/inbox, content://mms/sent, content://mms/drafts, content://mms/outbox, content://mms/temp.");
        } else {
            b.purge(uri);
            PduHeaders a2 = pdu.a();
            ContentValues contentValues = new ContentValues();
            for (Map.Entry next : k.entrySet()) {
                int intValue = ((Integer) next.getKey()).intValue();
                EncodedStringValue encodedStringValue = a2.getEncodedStringValue(intValue);
                if (encodedStringValue != null) {
                    contentValues.put((String) next.getValue(), toIsoString(encodedStringValue.getTextString()));
                    contentValues.put(j.get(Integer.valueOf(intValue)), Integer.valueOf(encodedStringValue.getCharacterSet()));
                }
            }
            for (Map.Entry next2 : l.entrySet()) {
                byte[] textString = a2.getTextString(((Integer) next2.getKey()).intValue());
                if (textString != null) {
                    contentValues.put((String) next2.getValue(), toIsoString(textString));
                }
            }
            for (Map.Entry next3 : m.entrySet()) {
                int octet = a2.getOctet(((Integer) next3.getKey()).intValue());
                if (octet != 0) {
                    contentValues.put((String) next3.getValue(), Integer.valueOf(octet));
                }
            }
            for (Map.Entry next4 : n.entrySet()) {
                long longInteger = a2.getLongInteger(((Integer) next4.getKey()).intValue());
                if (longInteger != -1) {
                    contentValues.put((String) next4.getValue(), Long.valueOf(longInteger));
                }
            }
            HashMap hashMap = new HashMap(c.length);
            for (int i2 : c) {
                if (i2 == 137) {
                    EncodedStringValue encodedStringValue2 = a2.getEncodedStringValue(i2);
                    encodedStringValues = encodedStringValue2 != null ? new EncodedStringValue[]{encodedStringValue2} : null;
                } else {
                    encodedStringValues = a2.getEncodedStringValues(i2);
                }
                hashMap.put(Integer.valueOf(i2), encodedStringValues);
            }
            HashSet hashSet = new HashSet();
            int messageType = pdu.getMessageType();
            if (messageType == 130 || messageType == 132 || messageType == 128) {
                switch (messageType) {
                    case 128:
                        encodedStringValueArr = (EncodedStringValue[]) hashMap.get(151);
                        break;
                    case 129:
                    case 131:
                    default:
                        encodedStringValueArr = null;
                        break;
                    case 130:
                    case 132:
                        encodedStringValueArr = (EncodedStringValue[]) hashMap.get(137);
                        break;
                }
                if (encodedStringValueArr != null) {
                    for (EncodedStringValue encodedStringValue3 : encodedStringValueArr) {
                        if (encodedStringValue3 != null) {
                            hashSet.add(encodedStringValue3.getString());
                        }
                    }
                }
                j2 = Threads.getOrCreateThreadId(this.o, hashSet);
            } else {
                j2 = Long.MAX_VALUE;
            }
            contentValues.put("thread_id", Long.valueOf(j2));
            long currentTimeMillis = System.currentTimeMillis();
            if ((pdu instanceof a) && (body = ((a) pdu).getBody()) != null) {
                int partsNum = body.getPartsNum();
                for (int i3 = 0; i3 < partsNum; i3++) {
                    persistPart(body.getPart(i3), currentTimeMillis);
                }
            }
            Uri insert = SqliteWrapper.insert(this.o, this.p, uri, contentValues);
            if (insert == null) {
                throw new MmsException("persist() failed: return null.");
            }
            long parseId = ContentUris.parseId(insert);
            ContentValues contentValues2 = new ContentValues(1);
            contentValues2.put(Mms.Part.MSG_ID, Long.valueOf(parseId));
            SqliteWrapper.update(this.o, this.p, Uri.parse("content://mms/" + currentTimeMillis + "/part"), contentValues2, null, null);
            Uri parse = Uri.parse(uri + "/" + parseId);
            for (int i4 : c) {
                EncodedStringValue[] encodedStringValueArr2 = (EncodedStringValue[]) hashMap.get(Integer.valueOf(i4));
                if (encodedStringValueArr2 != null) {
                    a(parseId, i4, encodedStringValueArr2);
                }
            }
            return parse;
        }
    }

    public Uri move(Uri from, Uri to) throws MmsException {
        long parseId = ContentUris.parseId(from);
        if (parseId == -1) {
            throw new MmsException("Error! ID of the message: -1.");
        }
        Integer num = d.get(to);
        if (num == null) {
            throw new MmsException("Bad destination, must be one of content://mms/inbox, content://mms/sent, content://mms/drafts, content://mms/outbox, content://mms/temp.");
        }
        ContentValues contentValues = new ContentValues(1);
        contentValues.put(BaseMmsColumns.MESSAGE_BOX, num);
        SqliteWrapper.update(this.o, this.p, from, contentValues, null, null);
        return ContentUris.withAppendedId(to, parseId);
    }

    public static String toIsoString(byte[] bytes) {
        try {
            return new String(bytes, CharacterSets.MIMENAME_ISO_8859_1);
        } catch (UnsupportedEncodingException e2) {
            Log.e("PduPersister", "ISO_8859_1 must be supported!", e2);
            return "";
        }
    }

    public static byte[] getBytes(String data) {
        try {
            return data.getBytes(CharacterSets.MIMENAME_ISO_8859_1);
        } catch (UnsupportedEncodingException e2) {
            Log.e("PduPersister", "ISO_8859_1 must be supported!", e2);
            return new byte[0];
        }
    }

    public void release() {
        SqliteWrapper.delete(this.o, this.p, Uri.parse(TEMPORARY_DRM_OBJECT_URI), null, null);
    }

    public Cursor getPendingMessages(long dueTime) {
        Uri.Builder buildUpon = MmsSms.PendingMessages.CONTENT_URI.buildUpon();
        buildUpon.appendQueryParameter(TextBasedSmsColumns.PROTOCOL, "mms");
        return SqliteWrapper.query(this.o, this.p, buildUpon.build(), null, "err_type < ? AND due_time <= ?", new String[]{String.valueOf(10), String.valueOf(dueTime)}, MmsSms.PendingMessages.DUE_TIME);
    }

    public static final class Threads implements ThreadsColumns {
        public static final int BROADCAST_THREAD = 1;
        public static final int COMMON_THREAD = 0;
        public static final Uri CONTENT_URI;
        public static final Uri OBSOLETE_THREADS_URI;
        private static final String[] a = {MmsSms.WordsTable.ID};
        private static final Uri b = Uri.parse("content://mms-sms/threadID");

        static {
            Uri withAppendedPath = Uri.withAppendedPath(MmsSms.CONTENT_URI, "conversations");
            CONTENT_URI = withAppendedPath;
            OBSOLETE_THREADS_URI = Uri.withAppendedPath(withAppendedPath, "obsolete");
        }

        private Threads() {
        }

        public static long getOrCreateThreadId(Context context, String recipient) {
            HashSet hashSet = new HashSet();
            hashSet.add(recipient);
            return getOrCreateThreadId(context, hashSet);
        }

        /* JADX INFO: finally extract failed */
        public static long getOrCreateThreadId(Context context, Set<String> recipients) {
            Uri.Builder buildUpon = b.buildUpon();
            for (String next : recipients) {
                if (Mms.isEmailAddress(next)) {
                    next = Mms.extractAddrSpec(next);
                }
                buildUpon.appendQueryParameter("recipient", next);
            }
            Uri build = buildUpon.build();
            Cursor query = SqliteWrapper.query(context, context.getContentResolver(), build, a, null, null, null);
            if (query != null) {
                try {
                    if (query.moveToFirst()) {
                        long j = query.getLong(0);
                        query.close();
                        return j;
                    }
                    Log.e("PduPersister", "getOrCreateThreadId returned no rows!");
                    query.close();
                } catch (Throwable th) {
                    query.close();
                    throw th;
                }
            }
            Log.e("PduPersister", "getOrCreateThreadId failed with uri " + build.toString());
            return Long.MAX_VALUE;
        }
    }

    public static final class Mms implements BaseMmsColumns {
        public static final Uri CONTENT_URI;
        public static final String DEFAULT_SORT_ORDER = "date DESC";
        public static final Pattern EMAIL_ADDRESS = Pattern.compile("[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}\\@[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}(\\.[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25})+");
        public static final Pattern NAME_ADDR_EMAIL_PATTERN = Pattern.compile("\\s*(\"[^\"]*\"|[^<>\"]+)\\s*<([^<>]+)>\\s*");
        public static final Pattern PHONE = Pattern.compile("(\\+[0-9]+[\\- \\.]*)?(\\([0-9]+\\)[\\- \\.]*)?([0-9][0-9\\- \\.][0-9\\- \\.]+[0-9])");
        public static final Pattern QUOTED_STRING_PATTERN = Pattern.compile("\\s*\"([^\"]*)\"\\s*");
        public static final Uri REPORT_REQUEST_URI;
        public static final Uri REPORT_STATUS_URI = Uri.withAppendedPath(CONTENT_URI, "report-status");

        public static final class Addr {
            public static final String ADDRESS = "address";
            public static final String CHARSET = "charset";
            public static final String CONTACT_ID = "contact_id";
            public static final String MSG_ID = "msg_id";
            public static final String TYPE = "type";
        }

        public static final class Draft implements BaseMmsColumns {
            public static final Uri CONTENT_URI = Uri.parse("content://mms/drafts");
            public static final String DEFAULT_SORT_ORDER = "date DESC";
        }

        public static final class Inbox implements BaseMmsColumns {
            public static final Uri CONTENT_URI = Uri.parse("content://mms/inbox");
            public static final String DEFAULT_SORT_ORDER = "date DESC";
        }

        public static final class Outbox implements BaseMmsColumns {
            public static final Uri CONTENT_URI = Uri.parse("content://mms/outbox");
            public static final String DEFAULT_SORT_ORDER = "date DESC";
        }

        public static final class Part {
            public static final String CHARSET = "chset";
            public static final String CONTENT_DISPOSITION = "cd";
            public static final String CONTENT_ID = "cid";
            public static final String CONTENT_LOCATION = "cl";
            public static final String CONTENT_TYPE = "ct";
            public static final String CT_START = "ctt_s";
            public static final String CT_TYPE = "ctt_t";
            public static final String FILENAME = "fn";
            public static final String MSG_ID = "mid";
            public static final String NAME = "name";
            public static final String SEQ = "seq";
            public static final String TEXT = "text";
            public static final String _DATA = "_data";
        }

        public static final class Rate {
            public static final Uri CONTENT_URI = Uri.withAppendedPath(Mms.CONTENT_URI, "rate");
            public static final String SENT_TIME = "sent_time";
        }

        public static final class ScrapSpace {
            public static final Uri CONTENT_URI = Uri.parse("content://mms/scrapSpace");
            public static final String SCRAP_FILE_PATH = (String.valueOf(Environment.getExternalStorageDirectory().getPath()) + "/mms/scrapSpace/.temp.jpg");
        }

        public static final class Sent implements BaseMmsColumns {
            public static final Uri CONTENT_URI = Uri.parse("content://mms/sent");
            public static final String DEFAULT_SORT_ORDER = "date DESC";
        }

        static {
            Uri parse = Uri.parse("content://mms");
            CONTENT_URI = parse;
            REPORT_REQUEST_URI = Uri.withAppendedPath(parse, "report-request");
        }

        public static final Cursor query(ContentResolver cr, String[] projection) {
            return cr.query(CONTENT_URI, projection, null, null, "date DESC");
        }

        public static final Cursor query(ContentResolver cr, String[] projection, String where, String orderBy) {
            String str;
            Uri uri = CONTENT_URI;
            if (orderBy == null) {
                str = "date DESC";
            } else {
                str = orderBy;
            }
            return cr.query(uri, projection, where, null, str);
        }

        public static final String getMessageBoxName(int msgBox) {
            switch (msgBox) {
                case 0:
                    return "all";
                case 1:
                    return "inbox";
                case 2:
                    return "sent";
                case 3:
                    return "drafts";
                case 4:
                    return "outbox";
                default:
                    return null;
            }
        }

        public static String extractAddrSpec(String address) {
            Matcher matcher = NAME_ADDR_EMAIL_PATTERN.matcher(address);
            if (matcher.matches()) {
                return matcher.group(2);
            }
            return address;
        }

        public static boolean isEmailAddress(String address) {
            if (TextUtils.isEmpty(address)) {
                return false;
            }
            return EMAIL_ADDRESS.matcher(extractAddrSpec(address)).matches();
        }

        public static boolean isPhoneNumber(String number) {
            if (TextUtils.isEmpty(number)) {
                return false;
            }
            return PHONE.matcher(number).matches();
        }

        public static final class Intents {
            public static final String CONTENT_CHANGED_ACTION = "android.intent.action.CONTENT_CHANGED";
            public static final String DELETED_CONTENTS = "deleted_contents";
            public static final String EXTRA_BCC = "bcc";
            public static final String EXTRA_CC = "cc";
            public static final String EXTRA_CONTENTS = "contents";
            public static final String EXTRA_SUBJECT = "subject";
            public static final String EXTRA_TYPES = "types";

            private Intents() {
            }
        }
    }

    public static final class Intents {
        public static final String EXTRA_PLMN = "plmn";
        public static final String EXTRA_SHOW_PLMN = "showPlmn";
        public static final String EXTRA_SHOW_SPN = "showSpn";
        public static final String EXTRA_SPN = "spn";
        public static final String SECRET_CODE_ACTION = "android.provider.Telephony.SECRET_CODE";
        public static final String SPN_STRINGS_UPDATED_ACTION = "android.provider.Telephony.SPN_STRINGS_UPDATED";

        private Intents() {
        }
    }
}
