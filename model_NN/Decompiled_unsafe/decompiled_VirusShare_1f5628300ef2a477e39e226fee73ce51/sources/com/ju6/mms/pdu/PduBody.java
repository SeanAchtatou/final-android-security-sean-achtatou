package com.ju6.mms.pdu;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

public class PduBody {
    private Vector<PduPart> a;
    private Map<String, PduPart> b;
    private Map<String, PduPart> c;
    private Map<String, PduPart> d;
    private Map<String, PduPart> e;

    public PduBody() {
        this.a = null;
        this.b = null;
        this.c = null;
        this.d = null;
        this.e = null;
        this.a = new Vector<>();
        this.b = new HashMap();
        this.c = new HashMap();
        this.d = new HashMap();
        this.e = new HashMap();
    }

    private void a(PduPart pduPart) {
        byte[] contentId = pduPart.getContentId();
        if (contentId != null) {
            this.b.put(new String(contentId), pduPart);
        }
        byte[] contentLocation = pduPart.getContentLocation();
        if (contentLocation != null) {
            this.c.put(new String(contentLocation), pduPart);
        }
        byte[] name = pduPart.getName();
        if (name != null) {
            this.d.put(new String(name), pduPart);
        }
        byte[] filename = pduPart.getFilename();
        if (filename != null) {
            this.e.put(new String(filename), pduPart);
        }
    }

    public boolean addPart(PduPart part) {
        if (part == null) {
            return false;
        }
        a(part);
        return this.a.add(part);
    }

    public void addPart(int index, PduPart part) {
        if (part != null) {
            a(part);
            this.a.add(index, part);
        }
    }

    public PduPart removePart(int index) {
        return this.a.remove(index);
    }

    public void removeAll() {
        this.a.clear();
    }

    public PduPart getPart(int index) {
        return this.a.get(index);
    }

    public int getPartIndex(PduPart part) {
        return this.a.indexOf(part);
    }

    public int getPartsNum() {
        return this.a.size();
    }

    public PduPart getPartByContentId(String cid) {
        return this.b.get(cid);
    }

    public PduPart getPartByContentLocation(String contentLocation) {
        return this.c.get(contentLocation);
    }

    public PduPart getPartByName(String name) {
        return this.d.get(name);
    }

    public PduPart getPartByFileName(String filename) {
        return this.e.get(filename);
    }
}
