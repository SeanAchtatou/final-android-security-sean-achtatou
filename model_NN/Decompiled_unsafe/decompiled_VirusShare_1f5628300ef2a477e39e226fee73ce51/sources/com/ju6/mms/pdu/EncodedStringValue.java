package com.ju6.mms.pdu;

import android.util.Log;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

public class EncodedStringValue implements Cloneable {
    private int a;
    private byte[] b;

    public EncodedStringValue(int charset, byte[] data) {
        if (data != null) {
            this.a = charset;
            this.b = new byte[data.length];
            System.arraycopy(data, 0, this.b, 0, data.length);
        }
    }

    public EncodedStringValue(byte[] data) {
        this(106, data);
    }

    public EncodedStringValue(String data) {
        try {
            this.b = data.getBytes("utf-8");
            this.a = 106;
        } catch (UnsupportedEncodingException e) {
            Log.e("EncodedStringValue", "Default encoding must be supported.", e);
        }
    }

    public int getCharacterSet() {
        return this.a;
    }

    public void setCharacterSet(int charset) {
        this.a = charset;
    }

    public byte[] getTextString() {
        byte[] bArr = new byte[this.b.length];
        System.arraycopy(this.b, 0, bArr, 0, this.b.length);
        return bArr;
    }

    public void setTextString(byte[] textString) {
        if (textString != null) {
            this.b = new byte[textString.length];
            System.arraycopy(textString, 0, this.b, 0, textString.length);
        }
    }

    public String getString() {
        if (this.a == 0) {
            return new String(this.b);
        }
        try {
            return new String(this.b, CharacterSets.getMimeName(this.a));
        } catch (UnsupportedEncodingException e) {
            try {
                return new String(this.b, CharacterSets.MIMENAME_ISO_8859_1);
            } catch (UnsupportedEncodingException e2) {
                return new String(this.b);
            }
        }
    }

    public void appendTextString(byte[] textString) {
        if (textString != null) {
            if (this.b == null) {
                this.b = new byte[textString.length];
                System.arraycopy(textString, 0, this.b, 0, textString.length);
                return;
            }
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            try {
                byteArrayOutputStream.write(this.b);
                byteArrayOutputStream.write(textString);
            } catch (IOException e) {
                e.printStackTrace();
            }
            this.b = byteArrayOutputStream.toByteArray();
        }
    }

    public Object clone() throws CloneNotSupportedException {
        super.clone();
        int length = this.b.length;
        byte[] bArr = new byte[length];
        System.arraycopy(this.b, 0, bArr, 0, length);
        try {
            return new EncodedStringValue(this.a, bArr);
        } catch (Exception e) {
            Log.e("EncodedStringValue", "failed to clone an EncodedStringValue: " + this);
            e.printStackTrace();
            throw new CloneNotSupportedException(e.getMessage());
        }
    }

    public EncodedStringValue[] split(String pattern) {
        String[] split = getString().split(pattern);
        EncodedStringValue[] encodedStringValueArr = new EncodedStringValue[split.length];
        int i = 0;
        while (i < encodedStringValueArr.length) {
            try {
                encodedStringValueArr[i] = new EncodedStringValue(this.a, split[i].getBytes());
                i++;
            } catch (NullPointerException e) {
                return null;
            }
        }
        return encodedStringValueArr;
    }

    public static EncodedStringValue[] extract(String src) {
        String[] split = src.split(";");
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < split.length; i++) {
            if (split[i].length() > 0) {
                arrayList.add(new EncodedStringValue(split[i]));
            }
        }
        int size = arrayList.size();
        if (size > 0) {
            return (EncodedStringValue[]) arrayList.toArray(new EncodedStringValue[size]);
        }
        return null;
    }

    public static String concat(EncodedStringValue[] addr) {
        StringBuilder sb = new StringBuilder();
        int length = addr.length - 1;
        for (int i = 0; i <= length; i++) {
            sb.append(addr[i].getString());
            if (i < length) {
                sb.append(";");
            }
        }
        return sb.toString();
    }

    public static EncodedStringValue copy(EncodedStringValue value) {
        if (value == null) {
            return null;
        }
        return new EncodedStringValue(value.a, value.b);
    }

    public static EncodedStringValue[] encodeStrings(String[] array) {
        int length = array.length;
        if (length <= 0) {
            return null;
        }
        EncodedStringValue[] encodedStringValueArr = new EncodedStringValue[length];
        for (int i = 0; i < length; i++) {
            encodedStringValueArr[i] = new EncodedStringValue(array[i]);
        }
        return encodedStringValueArr;
    }
}
