package com.ju6.mms.pdu;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;

public class CharacterSets {
    public static final int ANY_CHARSET = 0;
    public static final int BIG5 = 2026;
    public static final int DEFAULT_CHARSET = 106;
    public static final String DEFAULT_CHARSET_NAME = "utf-8";
    public static final int ISO_8859_1 = 4;
    public static final int ISO_8859_2 = 5;
    public static final int ISO_8859_3 = 6;
    public static final int ISO_8859_4 = 7;
    public static final int ISO_8859_5 = 8;
    public static final int ISO_8859_6 = 9;
    public static final int ISO_8859_7 = 10;
    public static final int ISO_8859_8 = 11;
    public static final int ISO_8859_9 = 12;
    public static final String MIMENAME_ANY_CHARSET = "*";
    public static final String MIMENAME_BIG5 = "big5";
    public static final String MIMENAME_ISO_8859_1 = "iso-8859-1";
    public static final String MIMENAME_ISO_8859_2 = "iso-8859-2";
    public static final String MIMENAME_ISO_8859_3 = "iso-8859-3";
    public static final String MIMENAME_ISO_8859_4 = "iso-8859-4";
    public static final String MIMENAME_ISO_8859_5 = "iso-8859-5";
    public static final String MIMENAME_ISO_8859_6 = "iso-8859-6";
    public static final String MIMENAME_ISO_8859_7 = "iso-8859-7";
    public static final String MIMENAME_ISO_8859_8 = "iso-8859-8";
    public static final String MIMENAME_ISO_8859_9 = "iso-8859-9";
    public static final String MIMENAME_SHIFT_JIS = "shift_JIS";
    public static final String MIMENAME_UCS2 = "iso-10646-ucs-2";
    public static final String MIMENAME_US_ASCII = "us-ascii";
    public static final String MIMENAME_UTF_16 = "utf-16";
    public static final String MIMENAME_UTF_8 = "utf-8";
    public static final int SHIFT_JIS = 17;
    public static final int UCS2 = 1000;
    public static final int US_ASCII = 3;
    public static final int UTF_16 = 1015;
    public static final int UTF_8 = 106;
    private static final int[] a;
    private static final String[] b = {MIMENAME_ANY_CHARSET, MIMENAME_US_ASCII, MIMENAME_ISO_8859_1, MIMENAME_ISO_8859_2, MIMENAME_ISO_8859_3, MIMENAME_ISO_8859_4, MIMENAME_ISO_8859_5, MIMENAME_ISO_8859_6, MIMENAME_ISO_8859_7, MIMENAME_ISO_8859_8, MIMENAME_ISO_8859_9, MIMENAME_SHIFT_JIS, "utf-8", MIMENAME_BIG5, MIMENAME_UCS2, MIMENAME_UTF_16};
    private static final HashMap<Integer, String> c = new HashMap<>();
    private static final HashMap<String, Integer> d = new HashMap<>();
    private static /* synthetic */ boolean e;

    static {
        boolean z;
        if (!CharacterSets.class.desiredAssertionStatus()) {
            z = true;
        } else {
            z = false;
        }
        e = z;
        int[] iArr = new int[16];
        iArr[1] = 3;
        iArr[2] = 4;
        iArr[3] = 5;
        iArr[4] = 6;
        iArr[5] = 7;
        iArr[6] = 8;
        iArr[7] = 9;
        iArr[8] = 10;
        iArr[9] = 11;
        iArr[10] = 12;
        iArr[11] = 17;
        iArr[12] = 106;
        iArr[13] = 2026;
        iArr[14] = 1000;
        iArr[15] = 1015;
        a = iArr;
        if (e || a.length == b.length) {
            int length = a.length - 1;
            for (int i = 0; i <= length; i++) {
                c.put(Integer.valueOf(a[i]), b[i]);
                d.put(b[i], Integer.valueOf(a[i]));
            }
            return;
        }
        throw new AssertionError();
    }

    private CharacterSets() {
    }

    public static String getMimeName(int mibEnumValue) throws UnsupportedEncodingException {
        String str = c.get(Integer.valueOf(mibEnumValue));
        if (str != null) {
            return str;
        }
        throw new UnsupportedEncodingException();
    }

    public static int getMibEnumValue(String mimeName) throws UnsupportedEncodingException {
        if (mimeName == null) {
            return -1;
        }
        Integer num = d.get(mimeName);
        if (num != null) {
            return num.intValue();
        }
        throw new UnsupportedEncodingException();
    }
}
