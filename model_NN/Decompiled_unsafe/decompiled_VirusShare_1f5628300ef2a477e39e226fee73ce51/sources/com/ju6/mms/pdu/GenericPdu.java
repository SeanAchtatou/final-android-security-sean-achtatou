package com.ju6.mms.pdu;

import com.ju6.mms.InvalidHeaderValueException;

public class GenericPdu {
    PduHeaders a;

    public GenericPdu() {
        this.a = null;
        this.a = new PduHeaders();
    }

    GenericPdu(PduHeaders headers) {
        this.a = null;
        this.a = headers;
    }

    /* access modifiers changed from: package-private */
    public final PduHeaders a() {
        return this.a;
    }

    public int getMessageType() {
        return this.a.getOctet(140);
    }

    public void setMessageType(int value) throws InvalidHeaderValueException {
        this.a.setOctet(value, 140);
    }

    public int getMmsVersion() {
        return this.a.getOctet(141);
    }

    public void setMmsVersion(int value) throws InvalidHeaderValueException {
        this.a.setOctet(value, 141);
    }

    public EncodedStringValue getFrom() {
        return this.a.getEncodedStringValue(137);
    }

    public void setFrom(EncodedStringValue value) {
        this.a.setEncodedStringValue(value, 137);
    }
}
