package com.ju6.mms.util;

import java.util.HashMap;

abstract class a<K, V> {
    private final HashMap<K, C0000a<V>> a = new HashMap<>();

    protected a() {
    }

    public boolean put(Object obj, Object obj2) {
        if (this.a.size() >= 500) {
            return false;
        }
        if (obj == null) {
            return false;
        }
        C0000a aVar = new C0000a();
        aVar.b = obj2;
        this.a.put(obj, aVar);
        return true;
    }

    public V get(K key) {
        C0000a aVar;
        if (key == null || (aVar = this.a.get(key)) == null) {
            return null;
        }
        aVar.a++;
        return aVar.b;
    }

    public V purge(Object obj) {
        C0000a remove = this.a.remove(obj);
        if (remove != null) {
            return remove.b;
        }
        return null;
    }

    public void purgeAll() {
        this.a.clear();
    }

    public int size() {
        return this.a.size();
    }

    /* renamed from: com.ju6.mms.util.a$a  reason: collision with other inner class name */
    private static class C0000a<V> {
        int a;
        V b;

        /* synthetic */ C0000a() {
            this((byte) 0);
        }

        private C0000a(byte b2) {
        }
    }
}
