package com.ju6.mms.util;

import com.ju6.mms.pdu.GenericPdu;

public final class PduCacheEntry {
    private final GenericPdu a;
    private final int b;
    private final long c;

    public PduCacheEntry(GenericPdu pdu, int msgBox, long threadId) {
        this.a = pdu;
        this.b = msgBox;
        this.c = threadId;
    }

    public final GenericPdu getPdu() {
        return this.a;
    }

    public final int getMessageBox() {
        return this.b;
    }

    public final long getThreadId() {
        return this.c;
    }
}
