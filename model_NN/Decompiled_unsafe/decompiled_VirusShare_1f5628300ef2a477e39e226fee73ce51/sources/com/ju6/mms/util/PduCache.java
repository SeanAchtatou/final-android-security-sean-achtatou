package com.ju6.mms.util;

import android.content.ContentUris;
import android.content.UriMatcher;
import android.net.Uri;
import com.ju6.mms.pdu.CharacterSets;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

public final class PduCache extends a<Uri, PduCacheEntry> {
    public static final int MESSAGE_BOX_ALL = 0;
    public static final int MESSAGE_BOX_DRAFTS = 3;
    public static final int MESSAGE_BOX_INBOX = 1;
    public static final int MESSAGE_BOX_OUTBOX = 4;
    public static final int MESSAGE_BOX_SENT = 2;
    private static final UriMatcher a;
    private static final HashMap<Integer, Integer> b;
    private static PduCache c;
    private final HashMap<Integer, HashSet<Uri>> d = new HashMap<>();
    private final HashMap<Long, HashSet<Uri>> e = new HashMap<>();

    public final /* bridge */ /* synthetic */ Object get(Object obj) {
        return super.get(obj);
    }

    public final synchronized /* bridge */ /* synthetic */ Object purge(Object obj) {
        return purge((Uri) obj);
    }

    public final synchronized /* bridge */ /* synthetic */ boolean put(Object obj, Object obj2) {
        return put((Uri) obj, (PduCacheEntry) obj2);
    }

    public final /* bridge */ /* synthetic */ int size() {
        return super.size();
    }

    static {
        UriMatcher uriMatcher = new UriMatcher(-1);
        a = uriMatcher;
        uriMatcher.addURI("mms", null, 0);
        a.addURI("mms", "#", 1);
        a.addURI("mms", "inbox", 2);
        a.addURI("mms", "inbox/#", 3);
        a.addURI("mms", "sent", 4);
        a.addURI("mms", "sent/#", 5);
        a.addURI("mms", "drafts", 6);
        a.addURI("mms", "drafts/#", 7);
        a.addURI("mms", "outbox", 8);
        a.addURI("mms", "outbox/#", 9);
        a.addURI("mms-sms", "conversations", 10);
        a.addURI("mms-sms", "conversations/#", 11);
        HashMap<Integer, Integer> hashMap = new HashMap<>();
        b = hashMap;
        hashMap.put(2, 1);
        b.put(4, 2);
        b.put(6, 3);
        b.put(8, 4);
    }

    private PduCache() {
    }

    public static final synchronized PduCache getInstance() {
        PduCache pduCache;
        synchronized (PduCache.class) {
            if (c == null) {
                c = new PduCache();
            }
            pduCache = c;
        }
        return pduCache;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.ju6.mms.util.a.put(java.lang.Object, java.lang.Object):boolean
     arg types: [android.net.Uri, com.ju6.mms.util.PduCacheEntry]
     candidates:
      com.ju6.mms.util.PduCache.put(android.net.Uri, com.ju6.mms.util.PduCacheEntry):boolean
      com.ju6.mms.util.a.put(java.lang.Object, java.lang.Object):boolean */
    public final synchronized boolean put(Uri uri, PduCacheEntry entry) {
        HashSet hashSet;
        Uri withAppendedPath;
        boolean put;
        int messageBox = entry.getMessageBox();
        HashSet hashSet2 = this.d.get(Integer.valueOf(messageBox));
        if (hashSet2 == null) {
            HashSet hashSet3 = new HashSet();
            this.d.put(Integer.valueOf(messageBox), hashSet3);
            hashSet = hashSet3;
        } else {
            hashSet = hashSet2;
        }
        long threadId = entry.getThreadId();
        HashSet hashSet4 = this.e.get(Long.valueOf(threadId));
        if (hashSet4 == null) {
            hashSet4 = new HashSet();
            this.e.put(Long.valueOf(threadId), hashSet4);
        }
        switch (a.match(uri)) {
            case 1:
                withAppendedPath = uri;
                break;
            case 2:
            case 4:
            case 6:
            case CharacterSets.ISO_8859_5:
            default:
                withAppendedPath = null;
                break;
            case 3:
            case 5:
            case 7:
            case CharacterSets.ISO_8859_6:
                withAppendedPath = Uri.withAppendedPath(Uri.parse("content://mms"), uri.getLastPathSegment());
                break;
        }
        put = super.put((Object) withAppendedPath, (Object) entry);
        if (put) {
            hashSet.add(withAppendedPath);
            hashSet4.add(withAppendedPath);
        }
        return put;
    }

    public final synchronized PduCacheEntry purge(Uri uri) {
        PduCacheEntry pduCacheEntry;
        HashSet remove;
        int match = a.match(uri);
        switch (match) {
            case 0:
            case 10:
                purgeAll();
                pduCacheEntry = null;
                break;
            case 1:
                pduCacheEntry = a(uri);
                break;
            case 2:
            case 4:
            case 6:
            case CharacterSets.ISO_8859_5:
                Integer num = b.get(Integer.valueOf(match));
                if (!(num == null || (remove = this.d.remove(num)) == null)) {
                    Iterator it = remove.iterator();
                    while (it.hasNext()) {
                        Uri uri2 = (Uri) it.next();
                        PduCacheEntry pduCacheEntry2 = (PduCacheEntry) super.purge((Object) uri2);
                        if (pduCacheEntry2 != null) {
                            a(uri2, pduCacheEntry2);
                        }
                    }
                }
                pduCacheEntry = null;
                break;
            case 3:
            case 5:
            case 7:
            case CharacterSets.ISO_8859_6:
                pduCacheEntry = a(Uri.withAppendedPath(Uri.parse("content://mms"), uri.getLastPathSegment()));
                break;
            case 11:
                HashSet remove2 = this.e.remove(Long.valueOf(ContentUris.parseId(uri)));
                if (remove2 != null) {
                    Iterator it2 = remove2.iterator();
                    while (it2.hasNext()) {
                        Uri uri3 = (Uri) it2.next();
                        PduCacheEntry pduCacheEntry3 = (PduCacheEntry) super.purge((Object) uri3);
                        if (pduCacheEntry3 != null) {
                            b(uri3, pduCacheEntry3);
                        }
                    }
                }
                pduCacheEntry = null;
                break;
            default:
                pduCacheEntry = null;
                break;
        }
        return pduCacheEntry;
    }

    private PduCacheEntry a(Uri uri) {
        PduCacheEntry pduCacheEntry = (PduCacheEntry) super.purge((Object) uri);
        if (pduCacheEntry == null) {
            return null;
        }
        a(uri, pduCacheEntry);
        b(uri, pduCacheEntry);
        return pduCacheEntry;
    }

    public final synchronized void purgeAll() {
        super.purgeAll();
        this.d.clear();
        this.e.clear();
    }

    private void a(Uri uri, PduCacheEntry pduCacheEntry) {
        HashSet hashSet = this.e.get(Long.valueOf(pduCacheEntry.getThreadId()));
        if (hashSet != null) {
            hashSet.remove(uri);
        }
    }

    private void b(Uri uri, PduCacheEntry pduCacheEntry) {
        HashSet hashSet = this.e.get(Integer.valueOf(pduCacheEntry.getMessageBox()));
        if (hashSet != null) {
            hashSet.remove(uri);
        }
    }
}
