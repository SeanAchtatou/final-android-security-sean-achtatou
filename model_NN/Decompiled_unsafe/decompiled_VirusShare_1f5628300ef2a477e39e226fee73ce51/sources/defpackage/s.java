package defpackage;

import java.io.InputStream;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.SAXException;

/* renamed from: s  reason: default package */
public class s extends n {
    b c;
    StringBuffer d = new StringBuffer();
    l e = new l();
    SAXParserFactory f = SAXParserFactory.newInstance();
    SAXParser g;
    u h;

    public s() {
        try {
            this.g = this.f.newSAXParser();
        } catch (ParserConfigurationException e2) {
            e2.printStackTrace();
        } catch (SAXException e3) {
            e3.printStackTrace();
        }
        this.h = new u(this);
    }

    public void a(int i) {
        this.c.a(this.c.M + 1, "can not establish connection!");
    }

    public void a(b bVar) {
        this.c = bVar;
        this.d.setLength(0);
        this.d.append("ret=2");
        this.d.append("&rqt=2");
        this.d.append("&pid=" + bVar.c);
        this.d.append("&tid=" + bVar.d);
        this.d.append("&ty=" + bVar.f);
        this.d.append("&u=" + bVar.g);
        this.d.append("&o=" + bVar.i);
        this.d.append("&m=" + bVar.h);
        this.d.append("&ip=" + bVar.j);
        this.d.append("&ay=" + bVar.e);
        this.d.append("&ray=" + bVar.k);
        this.d.append("&w=" + bVar.o);
        this.d.append("&h=" + bVar.p);
        this.d.append("&tt=" + bVar.l);
        this.d.append("&v=" + bVar.q);
        this.d.append("&st=" + bVar.n);
        this.d.append("&shid=" + ((Object) bVar.a.c));
        this.d.append("&ac=" + b.r);
        this.d.append("&ca=" + bVar.s);
        this.e.a(a, this.d.toString(), this);
    }

    public void a(InputStream inputStream) {
        try {
            this.g.parse(inputStream, this.h);
        } catch (Exception e2) {
            e2.printStackTrace();
            this.c.a(this.c.M + 2, "parse ad error!");
        }
    }
}
