package defpackage;

/* renamed from: x  reason: default package */
class x extends w {
    static final /* synthetic */ boolean g = (!v.class.desiredAssertionStatus());
    private static final byte[] h = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 43, 47};
    private static final byte[] i = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 45, 95};
    int c;
    public final boolean d;
    public final boolean e;
    public final boolean f;
    private final byte[] j;
    private int k;
    private final byte[] l;

    public x(int i2, byte[] bArr) {
        this.a = bArr;
        this.d = (i2 & 1) == 0;
        this.e = (i2 & 2) == 0;
        this.f = (i2 & 4) != 0;
        this.l = (i2 & 8) == 0 ? h : i;
        this.j = new byte[2];
        this.c = 0;
        this.k = this.e ? 19 : -1;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0057  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00f7  */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x020f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(byte[] r12, int r13, int r14, boolean r15) {
        /*
            r11 = this;
            byte[] r0 = r11.l
            byte[] r1 = r11.a
            r2 = 0
            int r3 = r11.k
            int r4 = r14 + r13
            r5 = -1
            int r6 = r11.c
            switch(r6) {
                case 0: goto L_0x00ac;
                case 1: goto L_0x00af;
                case 2: goto L_0x00d2;
                default: goto L_0x000f;
            }
        L_0x000f:
            r6 = r13
        L_0x0010:
            r7 = -1
            if (r5 == r7) goto L_0x0255
            int r7 = r2 + 1
            int r8 = r5 >> 18
            r8 = r8 & 63
            byte r8 = r0[r8]
            r1[r2] = r8
            int r2 = r7 + 1
            int r8 = r5 >> 12
            r8 = r8 & 63
            byte r8 = r0[r8]
            r1[r7] = r8
            int r7 = r2 + 1
            int r8 = r5 >> 6
            r8 = r8 & 63
            byte r8 = r0[r8]
            r1[r2] = r8
            int r2 = r7 + 1
            r5 = r5 & 63
            byte r5 = r0[r5]
            r1[r7] = r5
            int r3 = r3 + -1
            if (r3 != 0) goto L_0x0255
            boolean r3 = r11.f
            if (r3 == 0) goto L_0x0048
            int r3 = r2 + 1
            r5 = 13
            r1[r2] = r5
            r2 = r3
        L_0x0048:
            int r3 = r2 + 1
            r5 = 10
            r1[r2] = r5
            r2 = 19
            r5 = r3
            r3 = r2
            r2 = r6
        L_0x0053:
            int r6 = r2 + 3
            if (r6 > r4) goto L_0x00f5
            byte r6 = r12[r2]
            r6 = r6 & 255(0xff, float:3.57E-43)
            int r6 = r6 << 16
            int r7 = r2 + 1
            byte r7 = r12[r7]
            r7 = r7 & 255(0xff, float:3.57E-43)
            int r7 = r7 << 8
            r6 = r6 | r7
            int r7 = r2 + 2
            byte r7 = r12[r7]
            r7 = r7 & 255(0xff, float:3.57E-43)
            r6 = r6 | r7
            int r7 = r6 >> 18
            r7 = r7 & 63
            byte r7 = r0[r7]
            r1[r5] = r7
            int r7 = r5 + 1
            int r8 = r6 >> 12
            r8 = r8 & 63
            byte r8 = r0[r8]
            r1[r7] = r8
            int r7 = r5 + 2
            int r8 = r6 >> 6
            r8 = r8 & 63
            byte r8 = r0[r8]
            r1[r7] = r8
            int r7 = r5 + 3
            r6 = r6 & 63
            byte r6 = r0[r6]
            r1[r7] = r6
            int r2 = r2 + 3
            int r5 = r5 + 4
            int r3 = r3 + -1
            if (r3 != 0) goto L_0x0053
            boolean r3 = r11.f
            if (r3 == 0) goto L_0x0252
            int r3 = r5 + 1
            r6 = 13
            r1[r5] = r6
        L_0x00a3:
            int r5 = r3 + 1
            r6 = 10
            r1[r3] = r6
            r3 = 19
            goto L_0x0053
        L_0x00ac:
            r6 = r13
            goto L_0x0010
        L_0x00af:
            int r6 = r13 + 2
            if (r6 > r4) goto L_0x000f
            byte[] r5 = r11.j
            r6 = 0
            byte r5 = r5[r6]
            r5 = r5 & 255(0xff, float:3.57E-43)
            int r5 = r5 << 16
            int r6 = r13 + 1
            byte r7 = r12[r13]
            r7 = r7 & 255(0xff, float:3.57E-43)
            int r7 = r7 << 8
            r5 = r5 | r7
            int r7 = r6 + 1
            byte r6 = r12[r6]
            r6 = r6 & 255(0xff, float:3.57E-43)
            r5 = r5 | r6
            r6 = 0
            r11.c = r6
            r6 = r7
            goto L_0x0010
        L_0x00d2:
            int r6 = r13 + 1
            if (r6 > r4) goto L_0x000f
            byte[] r5 = r11.j
            r6 = 0
            byte r5 = r5[r6]
            r5 = r5 & 255(0xff, float:3.57E-43)
            int r5 = r5 << 16
            byte[] r6 = r11.j
            r7 = 1
            byte r6 = r6[r7]
            r6 = r6 & 255(0xff, float:3.57E-43)
            int r6 = r6 << 8
            r5 = r5 | r6
            int r6 = r13 + 1
            byte r7 = r12[r13]
            r7 = r7 & 255(0xff, float:3.57E-43)
            r5 = r5 | r7
            r7 = 0
            r11.c = r7
            goto L_0x0010
        L_0x00f5:
            if (r15 == 0) goto L_0x020f
            int r6 = r11.c
            int r6 = r2 - r6
            r7 = 1
            int r7 = r4 - r7
            if (r6 != r7) goto L_0x0166
            r6 = 0
            int r7 = r11.c
            if (r7 <= 0) goto L_0x0161
            byte[] r7 = r11.j
            int r8 = r6 + 1
            byte r6 = r7[r6]
            r7 = r2
            r2 = r6
            r6 = r8
        L_0x010e:
            r2 = r2 & 255(0xff, float:3.57E-43)
            int r2 = r2 << 4
            int r8 = r11.c
            int r6 = r8 - r6
            r11.c = r6
            int r6 = r5 + 1
            int r8 = r2 >> 6
            r8 = r8 & 63
            byte r8 = r0[r8]
            r1[r5] = r8
            int r5 = r6 + 1
            r2 = r2 & 63
            byte r0 = r0[r2]
            r1[r6] = r0
            boolean r0 = r11.d
            if (r0 == 0) goto L_0x024f
            int r0 = r5 + 1
            r2 = 61
            r1[r5] = r2
            int r2 = r0 + 1
            r5 = 61
            r1[r0] = r5
            r0 = r2
        L_0x013b:
            boolean r2 = r11.e
            if (r2 == 0) goto L_0x0151
            boolean r2 = r11.f
            if (r2 == 0) goto L_0x014a
            int r2 = r0 + 1
            r5 = 13
            r1[r0] = r5
            r0 = r2
        L_0x014a:
            int r2 = r0 + 1
            r5 = 10
            r1[r0] = r5
            r0 = r2
        L_0x0151:
            r1 = r0
            r0 = r7
        L_0x0153:
            boolean r2 = defpackage.x.g
            if (r2 != 0) goto L_0x0203
            int r2 = r11.c
            if (r2 == 0) goto L_0x0203
            java.lang.AssertionError r0 = new java.lang.AssertionError
            r0.<init>()
            throw r0
        L_0x0161:
            int r7 = r2 + 1
            byte r2 = r12[r2]
            goto L_0x010e
        L_0x0166:
            int r6 = r11.c
            int r6 = r2 - r6
            r7 = 2
            int r7 = r4 - r7
            if (r6 != r7) goto L_0x01e5
            r6 = 0
            int r7 = r11.c
            r8 = 1
            if (r7 <= r8) goto L_0x01d8
            byte[] r7 = r11.j
            int r8 = r6 + 1
            byte r6 = r7[r6]
            r7 = r2
            r2 = r6
            r6 = r8
        L_0x017e:
            r2 = r2 & 255(0xff, float:3.57E-43)
            int r2 = r2 << 10
            int r8 = r11.c
            if (r8 <= 0) goto L_0x01dd
            byte[] r8 = r11.j
            int r9 = r6 + 1
            byte r6 = r8[r6]
            r8 = r7
            r7 = r9
        L_0x018e:
            r6 = r6 & 255(0xff, float:3.57E-43)
            int r6 = r6 << 2
            r2 = r2 | r6
            int r6 = r11.c
            int r6 = r6 - r7
            r11.c = r6
            int r6 = r5 + 1
            int r7 = r2 >> 12
            r7 = r7 & 63
            byte r7 = r0[r7]
            r1[r5] = r7
            int r5 = r6 + 1
            int r7 = r2 >> 6
            r7 = r7 & 63
            byte r7 = r0[r7]
            r1[r6] = r7
            int r6 = r5 + 1
            r2 = r2 & 63
            byte r0 = r0[r2]
            r1[r5] = r0
            boolean r0 = r11.d
            if (r0 == 0) goto L_0x024c
            int r0 = r6 + 1
            r2 = 61
            r1[r6] = r2
        L_0x01be:
            boolean r2 = r11.e
            if (r2 == 0) goto L_0x01d4
            boolean r2 = r11.f
            if (r2 == 0) goto L_0x01cd
            int r2 = r0 + 1
            r5 = 13
            r1[r0] = r5
            r0 = r2
        L_0x01cd:
            int r2 = r0 + 1
            r5 = 10
            r1[r0] = r5
            r0 = r2
        L_0x01d4:
            r1 = r0
            r0 = r8
            goto L_0x0153
        L_0x01d8:
            int r7 = r2 + 1
            byte r2 = r12[r2]
            goto L_0x017e
        L_0x01dd:
            int r8 = r7 + 1
            byte r7 = r12[r7]
            r10 = r7
            r7 = r6
            r6 = r10
            goto L_0x018e
        L_0x01e5:
            boolean r0 = r11.e
            if (r0 == 0) goto L_0x01ff
            if (r5 <= 0) goto L_0x01ff
            r0 = 19
            if (r3 == r0) goto L_0x01ff
            boolean r0 = r11.f
            if (r0 == 0) goto L_0x024a
            int r0 = r5 + 1
            r6 = 13
            r1[r5] = r6
        L_0x01f9:
            int r5 = r0 + 1
            r6 = 10
            r1[r0] = r6
        L_0x01ff:
            r0 = r2
            r1 = r5
            goto L_0x0153
        L_0x0203:
            boolean r2 = defpackage.x.g
            if (r2 != 0) goto L_0x0248
            if (r0 == r4) goto L_0x0248
            java.lang.AssertionError r0 = new java.lang.AssertionError
            r0.<init>()
            throw r0
        L_0x020f:
            r0 = 1
            int r0 = r4 - r0
            if (r2 != r0) goto L_0x0227
            byte[] r0 = r11.j
            int r1 = r11.c
            int r4 = r1 + 1
            r11.c = r4
            byte r2 = r12[r2]
            r0[r1] = r2
            r0 = r5
        L_0x0221:
            r11.b = r0
            r11.k = r3
            r0 = 1
            return r0
        L_0x0227:
            r0 = 2
            int r0 = r4 - r0
            if (r2 != r0) goto L_0x0246
            byte[] r0 = r11.j
            int r1 = r11.c
            int r4 = r1 + 1
            r11.c = r4
            byte r4 = r12[r2]
            r0[r1] = r4
            byte[] r0 = r11.j
            int r1 = r11.c
            int r4 = r1 + 1
            r11.c = r4
            int r2 = r2 + 1
            byte r2 = r12[r2]
            r0[r1] = r2
        L_0x0246:
            r0 = r5
            goto L_0x0221
        L_0x0248:
            r0 = r1
            goto L_0x0221
        L_0x024a:
            r0 = r5
            goto L_0x01f9
        L_0x024c:
            r0 = r6
            goto L_0x01be
        L_0x024f:
            r0 = r5
            goto L_0x013b
        L_0x0252:
            r3 = r5
            goto L_0x00a3
        L_0x0255:
            r5 = r2
            r2 = r6
            goto L_0x0053
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.x.a(byte[], int, int, boolean):boolean");
    }
}
