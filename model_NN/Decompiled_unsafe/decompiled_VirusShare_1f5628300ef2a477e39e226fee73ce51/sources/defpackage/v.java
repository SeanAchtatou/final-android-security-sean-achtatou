package defpackage;

import java.io.UnsupportedEncodingException;

/* renamed from: v  reason: default package */
public class v {
    static final /* synthetic */ boolean a = (!v.class.desiredAssertionStatus());

    private v() {
    }

    public static String a(byte[] bArr, int i) {
        try {
            return new String(b(bArr, i), "US-ASCII");
        } catch (UnsupportedEncodingException e) {
            throw new AssertionError(e);
        }
    }

    public static byte[] a(byte[] bArr, int i, int i2, int i3) {
        x xVar = new x(i3, null);
        int i4 = (i2 / 3) * 4;
        if (!xVar.d) {
            switch (i2 % 3) {
                case 1:
                    i4 += 2;
                    break;
                case 2:
                    i4 += 3;
                    break;
            }
        } else if (i2 % 3 > 0) {
            i4 += 4;
        }
        if (xVar.e && i2 > 0) {
            i4 += (((i2 - 1) / 57) + 1) * (xVar.f ? 2 : 1);
        }
        xVar.a = new byte[i4];
        xVar.a(bArr, i, i2, true);
        if (a || xVar.b == i4) {
            return xVar.a;
        }
        throw new AssertionError();
    }

    public static byte[] b(byte[] bArr, int i) {
        return a(bArr, 0, bArr.length, i);
    }
}
