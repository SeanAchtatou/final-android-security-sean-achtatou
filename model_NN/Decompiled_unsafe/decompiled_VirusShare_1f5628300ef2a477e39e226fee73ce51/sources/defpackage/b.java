package defpackage;

import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Message;
import android.provider.Browser;
import android.telephony.TelephonyManager;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.izp.views.IZPDelegate;
import com.izp.views.IZPView;
import com.ju6.mms.pdu.CharacterSets;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Timer;

/* renamed from: b  reason: default package */
public class b implements View.OnTouchListener, Animation.AnimationListener {
    public static String r = "";
    public ImageView A;
    public TextView B;
    public y C;
    public ProgressBar D;
    public boolean E;
    public boolean F;
    public Timer G;
    public Timer H;
    public String I = "-1";
    public IZPView J;
    public IZPDelegate K;
    e L;
    public int M;
    public boolean N = false;
    public ArrayList O = new ArrayList();
    public boolean P = false;
    Bitmap Q = null;
    final int R = 0;
    final int S = 1;
    /* access modifiers changed from: private */
    public b T = this;
    public a a;
    public j b;
    public String c;
    public String d;
    public String e;
    public String f;
    public String g;
    public String h;
    public String i;
    public String j;
    public String k;
    public String l;
    public StringBuffer m;
    public String n = "2";
    public int o;
    public int p;
    public String q = "1.0.1";
    public String s = "";
    public p t;
    public s u;
    public o v;
    public r w;
    public q x;
    public z y;
    public TextView z;

    public b(IZPView iZPView) {
        this.J = iZPView;
        this.e = "2";
        this.f = "1";
        this.m = new StringBuffer();
        this.m.append("false");
        this.u = new s();
        this.t = new p();
        this.v = new o();
        this.w = new r();
        this.x = new q();
        this.a = new a();
        this.b = new j();
        this.D = new ProgressBar(this.J.getContext(), null, 16842873);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(13);
        this.D.setLayoutParams(layoutParams);
        this.y = new z(iZPView.getContext());
        this.y.setBackgroundColor(0);
        this.y.setOnTouchListener(this);
        this.J.addView(this.y);
        this.y.addView(this.D);
        this.A = new ImageView(iZPView.getContext());
        this.A.setVisibility(4);
        this.y.addView(this.A);
        this.z = new TextView(iZPView.getContext());
        this.z.setVisibility(4);
        this.z.setBackgroundColor(17170445);
        this.y.addView(this.z);
        this.B = new TextView(iZPView.getContext());
        this.B.setVisibility(4);
        this.B.setBackgroundColor(17170445);
        this.y.addView(this.B);
        this.C = new y(iZPView.getContext());
        this.C.b = this;
        this.C.setVisibility(4);
        this.y.addView(this.C);
        this.L = new e(this);
        this.H = new Timer();
        this.F = false;
        this.E = false;
        d();
    }

    public void a() {
        if (this.O.size() != 0 && !this.P) {
            String str = (String) this.O.get(0);
            if (str.equals("REQUEST_XML")) {
                if (this.K == null || this.K.shouldRequestFreshAd(this.J)) {
                    this.u.a(this.T);
                } else {
                    return;
                }
            } else if (str.equals("REQUEST_IMG")) {
                if (this.K != null && !this.K.shouldRequestFreshAd(this.J)) {
                    return;
                }
                if (this.a.e.toString().compareTo("1") == 0 || this.a.e.toString().compareTo("5") == 0) {
                    this.t.a(this, this.a.f.toString());
                } else if (this.a.e.toString().compareTo("2") == 0) {
                    this.t.a(this, this.a.p.toString());
                } else {
                    n();
                }
            } else if (str.equals("SWITCH_AD")) {
                if (this.K == null || this.K.shouldShowFreshAd(this.J)) {
                    n();
                } else {
                    return;
                }
            }
            this.O.remove(0);
        }
    }

    public void a(int i2) {
        this.H.schedule(new f(this), (long) i2);
    }

    public void a(int i2, String str) {
        if (this.K != null) {
            this.K.errorReport(this.J, i2, str);
        }
        a(10000);
    }

    public void a(IZPDelegate iZPDelegate) {
        this.K = iZPDelegate;
    }

    public void a(String str) {
        this.c = str;
    }

    public void a(boolean z2) {
        if (z2) {
            this.h = "true";
        } else {
            this.h = "false";
        }
    }

    public void b() {
        if (!this.F) {
            this.O.add("REQUEST_XML");
            this.G = new Timer();
            this.G.scheduleAtFixedRate(new d(this), 10, 800);
            this.D.setVisibility(0);
            this.F = true;
            return;
        }
        this.G = new Timer();
        this.G.scheduleAtFixedRate(new d(this), 10, 800);
    }

    public void b(int i2) {
        this.H.schedule(new g(this), (long) i2);
    }

    public void b(String str) {
        this.k = str;
    }

    public void c() {
        this.G.cancel();
        this.G.purge();
    }

    public void d() {
        String m2;
        TelephonyManager telephonyManager = (TelephonyManager) this.J.getContext().getSystemService("phone");
        this.s = telephonyManager.getNetworkOperatorName();
        this.d = v.a(telephonyManager.getDeviceId().getBytes(), 0).trim();
        this.g = v.a(Build.MANUFACTURER.getBytes(), 0).trim();
        this.l = v.a(Build.MODEL.getBytes(), 0).trim();
        this.i = v.a(("Android" + Build.VERSION.RELEASE).getBytes(), 0).trim();
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) this.J.getContext().getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo != null && activeNetworkInfo.isAvailable()) {
            if (activeNetworkInfo.getType() == 0) {
                String extraInfo = activeNetworkInfo.getExtraInfo();
                if (extraInfo != null) {
                    r = extraInfo.toLowerCase();
                } else {
                    r = "unknown";
                }
            } else {
                r = "wifi";
            }
        }
        WifiManager wifiManager = (WifiManager) this.J.getContext().getSystemService("wifi");
        if (wifiManager.isWifiEnabled()) {
            int ipAddress = wifiManager.getConnectionInfo().getIpAddress();
            m2 = String.format("%d.%d.%d.%d", Integer.valueOf(ipAddress & 255), Integer.valueOf((ipAddress >> 8) & 255), Integer.valueOf((ipAddress >> 16) & 255), Integer.valueOf((ipAddress >> 24) & 255));
        } else {
            m2 = m();
        }
        this.j = m2;
    }

    public void e() {
        this.o = this.J.getWidth();
        this.p = this.J.getHeight();
    }

    public void f() {
        this.b.a = this.a.a.toString();
        this.b.b = this.a.b.toString();
        this.b.c = this.a.c.toString();
        this.b.d = this.a.d.toString();
        this.b.e = this.a.e.toString();
        this.b.f = this.a.f.toString();
        this.b.g = this.a.g.toString();
        this.b.h = this.a.h.toString();
        this.b.i = this.a.i.toString();
        this.b.j = this.a.j.toString();
        this.b.k = this.a.k.toString();
        this.b.l = this.a.l.toString();
        this.b.m = this.a.m.toString();
        this.b.n = this.a.n.toString();
        this.b.o = this.a.o.toString();
        this.b.p = this.a.p.toString();
        this.b.q = this.a.q.toString();
        this.Q = this.b.r;
        this.b.r = this.a.r;
        this.b.s++;
    }

    public synchronized void g() {
        if (this.D.getVisibility() == 0) {
            this.D.setVisibility(4);
        }
        this.E = false;
        f();
        if (this.K != null) {
            this.K.didReceiveFreshAd(this.J, this.b.s);
        }
        if (this.b.s == 1) {
            j();
            this.E = true;
        } else {
            l();
        }
        a(Integer.valueOf(this.b.a).intValue() * CharacterSets.UCS2);
        b(Integer.valueOf(this.b.b).intValue() * CharacterSets.UCS2);
        if (this.m.toString().equals("true") && !this.N) {
            this.H.schedule(new i(this), 1000);
            this.N = true;
        }
    }

    public void h() {
        if (this.I.compareTo(this.b.e) != 0) {
            if (this.k.toString().compareTo("1") == 0) {
                if (this.b.e.compareTo("1") == 0) {
                    RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(this.J.getWidth(), this.J.getHeight());
                    layoutParams.addRule(14);
                    this.y.setLayoutParams(layoutParams);
                    this.y.a = false;
                    RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(this.y.getWidth(), this.y.getHeight());
                    layoutParams2.addRule(13);
                    this.A.setLayoutParams(layoutParams2);
                    this.A.setVisibility(0);
                    this.z.setVisibility(4);
                    this.B.setVisibility(4);
                    this.C.setVisibility(4);
                } else if (this.b.e.compareTo("2") == 0) {
                    RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(this.J.getWidth(), this.J.getHeight());
                    layoutParams3.addRule(14);
                    this.y.setLayoutParams(layoutParams3);
                    this.y.a = true;
                    RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(this.b.r.getWidth(), this.b.r.getHeight());
                    layoutParams4.addRule(15);
                    layoutParams4.setMargins(5, (this.J.getHeight() - this.b.r.getHeight()) / 2, 0, 0);
                    this.A.setLayoutParams(layoutParams4);
                    this.A.setVisibility(0);
                    this.A.setId(18);
                    this.A.setMinimumHeight(this.b.r.getHeight());
                    RelativeLayout.LayoutParams layoutParams5 = new RelativeLayout.LayoutParams((this.y.getWidth() - 38) - 5, this.y.getHeight());
                    layoutParams5.addRule(1, 18);
                    this.z.setLayoutParams(layoutParams5);
                    this.z.setVisibility(0);
                    RelativeLayout.LayoutParams layoutParams6 = new RelativeLayout.LayoutParams(65, 12);
                    layoutParams6.addRule(11);
                    layoutParams6.addRule(12, -1);
                    this.B.setLayoutParams(layoutParams6);
                    this.B.setVisibility(0);
                    this.C.setVisibility(4);
                } else if (this.b.e.compareTo("3") == 0) {
                    RelativeLayout.LayoutParams layoutParams7 = new RelativeLayout.LayoutParams(this.J.getWidth(), this.J.getHeight());
                    layoutParams7.addRule(14);
                    this.y.setLayoutParams(layoutParams7);
                    this.y.a = true;
                    RelativeLayout.LayoutParams layoutParams8 = new RelativeLayout.LayoutParams(this.y.getWidth(), this.y.getHeight());
                    layoutParams8.addRule(13);
                    this.z.setLayoutParams(layoutParams8);
                    this.z.setVisibility(0);
                    RelativeLayout.LayoutParams layoutParams9 = new RelativeLayout.LayoutParams(65, 12);
                    layoutParams9.addRule(11);
                    layoutParams9.addRule(12, -1);
                    this.B.setLayoutParams(layoutParams9);
                    this.B.setVisibility(0);
                    this.A.setVisibility(4);
                    this.C.setVisibility(4);
                }
            } else if (this.k.toString().compareTo("2") == 0 && this.b.e.compareTo("5") == 0) {
                RelativeLayout.LayoutParams layoutParams10 = new RelativeLayout.LayoutParams(this.J.getWidth(), this.J.getHeight());
                layoutParams10.addRule(13);
                this.y.setLayoutParams(layoutParams10);
                this.y.a = false;
                RelativeLayout.LayoutParams layoutParams11 = new RelativeLayout.LayoutParams(this.J.getWidth(), this.J.getHeight());
                layoutParams11.addRule(13);
                this.A.setLayoutParams(layoutParams11);
                this.A.setScaleType(ImageView.ScaleType.MATRIX);
                this.A.setVisibility(0);
                if (this.J.getWidth() < this.J.getHeight()) {
                    Matrix matrix = new Matrix();
                    matrix.preScale((((float) this.J.getWidth()) * 1.0f) / (((float) this.a.r.getHeight()) * 1.0f), (((float) this.J.getHeight()) * 1.0f) / (((float) this.a.r.getWidth()) * 1.0f));
                    matrix.preRotate(90.0f);
                    matrix.postTranslate((float) this.J.getWidth(), 0.0f);
                    this.A.setImageMatrix(matrix);
                } else {
                    Matrix matrix2 = new Matrix();
                    matrix2.preScale((float) (this.J.getWidth() / this.a.r.getWidth()), (float) (this.J.getHeight() / this.a.r.getHeight()));
                    this.A.setImageMatrix(matrix2);
                }
                RelativeLayout.LayoutParams layoutParams12 = new RelativeLayout.LayoutParams(60, 60);
                layoutParams12.addRule(11);
                layoutParams12.addRule(12, -1);
                layoutParams12.setMargins(0, 0, 20, 20);
                this.C.setLayoutParams(layoutParams12);
                this.C.setVisibility(0);
                this.z.setVisibility(4);
                this.B.setVisibility(4);
            }
            this.I = this.b.e;
        }
    }

    public void i() {
        if (this.b.e.compareTo("1") == 0) {
            this.A.setImageBitmap(this.b.r);
        } else if (this.b.e.compareTo("2") == 0) {
            this.A.setImageBitmap(this.b.r);
            k();
        } else if (this.b.e.compareTo("3") == 0) {
            k();
        } else if (this.b.e.compareTo("5") == 0) {
            this.A.setImageBitmap(this.b.r);
        }
        if (this.Q != null && !this.Q.isRecycled()) {
            this.Q.recycle();
            this.Q = null;
        }
    }

    public void j() {
        h();
        i();
    }

    public void k() {
        if (this.b.q.compareTo("1") == 0) {
            this.z.setGravity(3);
        } else if (this.b.q.compareTo("2") == 0) {
            this.z.setGravity(17);
        } else if (this.b.q.compareTo("3") == 0) {
            this.z.setGravity(5);
        }
        this.z.setTextColor(Long.decode(this.b.j).intValue());
        this.B.setTextColor(Long.decode(this.b.j).intValue());
        this.B.setText("Powered by IZP");
        this.B.setTextSize(6.0f);
        if (this.b.l.compareTo("1") == 0) {
            this.z.setTypeface(null, 1);
        } else if (this.b.l.compareTo("2") == 0) {
            this.z.setTypeface(null, 2);
        } else {
            this.z.setTypeface(null, 0);
        }
        this.z.setTextSize((float) Long.decode(this.b.m).intValue());
        this.z.setText(this.b.q);
    }

    public void l() {
        k kVar = new k(0, -((this.J.getWidth() / 2) + (this.y.getWidth() / 2)), 0, 0);
        kVar.a = 1;
        kVar.setDuration(1000);
        kVar.setInterpolator(new LinearInterpolator());
        kVar.setAnimationListener(this);
        this.y.startAnimation(kVar);
    }

    public String m() {
        try {
            Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
            while (networkInterfaces.hasMoreElements()) {
                Enumeration<InetAddress> inetAddresses = networkInterfaces.nextElement().getInetAddresses();
                while (true) {
                    if (inetAddresses.hasMoreElements()) {
                        InetAddress nextElement = inetAddresses.nextElement();
                        if (!nextElement.isLoopbackAddress()) {
                            return nextElement.getHostAddress().toString();
                        }
                    }
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return null;
    }

    public void n() {
        Message message = new Message();
        message.what = 0;
        this.L.sendMessage(message);
    }

    public void o() {
        this.H.schedule(new h(this), 10);
    }

    public void onAnimationEnd(Animation animation) {
        if (((k) animation).a == 1) {
            j();
            k kVar = new k(this.J.getWidth(), 0, 0, 0);
            kVar.a = 2;
            animation.setInterpolator(new LinearInterpolator());
            kVar.setDuration(1000);
            kVar.setAnimationListener(this);
            this.y.startAnimation(kVar);
        } else if (((k) animation).a == 2) {
            this.E = true;
        }
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationStart(Animation animation) {
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0 && this.E) {
            if (!this.P) {
                this.P = true;
                o();
            }
            p();
        }
        return true;
    }

    public synchronized void p() {
        if (this.K != null) {
            this.K.willLeaveApplication(this.J);
        }
        String trim = this.b.g.trim();
        if (this.b.h.compareTo("1") == 0) {
            this.J.getContext().startActivity(new Intent("android.intent.action.VIEW", Uri.parse(trim)));
        } else if (this.b.h.compareTo("3") == 0) {
            try {
                this.J.getContext().startActivity(new Intent("android.intent.action.CALL", Uri.parse("tel:" + trim)));
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        } else if (this.b.h.compareTo("4") == 0) {
            try {
                int indexOf = trim.indexOf("@");
                String substring = trim.substring(0, indexOf);
                Intent intent = new Intent("android.intent.action.SENDTO", Uri.parse("smsto:" + trim.substring(indexOf + 1, trim.length() - 1)));
                intent.putExtra("sms_body", substring);
                this.J.getContext().startActivity(intent);
            } catch (Exception e3) {
                e3.printStackTrace();
            }
        }
        return;
    }

    public String q() {
        StringBuffer stringBuffer = new StringBuffer();
        PackageManager packageManager = this.J.getContext().getPackageManager();
        for (ApplicationInfo next : packageManager.getInstalledApplications(128)) {
            if ((next.flags & 1) == 0) {
                stringBuffer.append(packageManager.getApplicationLabel(next));
                stringBuffer.append(",");
            }
        }
        try {
            return v.a(stringBuffer.toString().getBytes(), 2);
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public String r() {
        StringBuffer stringBuffer = new StringBuffer();
        Cursor allVisitedUrls = Browser.getAllVisitedUrls(this.J.getContext().getContentResolver());
        if (allVisitedUrls.moveToFirst() && allVisitedUrls.getCount() > 0) {
            while (!allVisitedUrls.isAfterLast()) {
                String string = allVisitedUrls.getString(allVisitedUrls.getColumnIndex("url"));
                if (string.indexOf("http://") != -1) {
                    string = string.substring(7);
                }
                int i2 = 0;
                int i3 = 0;
                while (i3 < 4) {
                    i2 = string.indexOf("/", i2 + 1);
                    if (i2 == -1) {
                        break;
                    }
                    i3++;
                }
                if (i3 == 4 && i2 != -1) {
                    string = string.substring(0, i2);
                }
                int indexOf = string.indexOf("?");
                if (indexOf != -1) {
                    string = string.substring(0, indexOf);
                }
                if (string.length() > 60) {
                    string = string.substring(0, 60);
                }
                stringBuffer.append(string + ",");
                allVisitedUrls.moveToNext();
            }
        }
        if (stringBuffer.toString().lastIndexOf(",") == stringBuffer.length() - 1) {
            stringBuffer.delete(stringBuffer.length() - 1, stringBuffer.length());
        }
        try {
            return v.a(stringBuffer.toString().getBytes(), 2);
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }
}
