package defpackage;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.view.MotionEvent;
import android.view.View;
import com.ju6.mms.pdu.PduHeaders;

/* renamed from: y  reason: default package */
public class y extends View {
    Paint a = new Paint();
    public b b;

    public y(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        this.a.setColor(-7829368);
        this.a.setStyle(Paint.Style.FILL);
        this.a.setAlpha(PduHeaders.RECOMMENDED_RETRIEVAL_MODE);
        canvas.drawRoundRect(new RectF(0.0f, 0.0f, (float) getWidth(), (float) getHeight()), 5.0f, 5.0f, this.a);
        this.a.setColor(-1);
        this.a.setAlpha(255);
        this.a.setStrokeWidth(2.0f);
        canvas.drawLine(5.0f, 5.0f, (float) (getWidth() - 5), (float) (getHeight() - 5), this.a);
        canvas.drawLine((float) (getWidth() - 5), 5.0f, 5.0f, (float) (getHeight() - 5), this.a);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() != 0 || this.b.K == null) {
            return true;
        }
        this.b.K.didStopFullScreenAd(this.b.J);
        this.b.c();
        return true;
    }
}
