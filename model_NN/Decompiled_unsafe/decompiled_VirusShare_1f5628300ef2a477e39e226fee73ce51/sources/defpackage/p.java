package defpackage;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import java.io.IOException;
import java.io.InputStream;

/* renamed from: p  reason: default package */
public class p extends n {
    b c;
    l d = new l();

    public void a(int i) {
        this.c.a(this.c.M + 5, "can not get ad resource!");
    }

    public void a(b bVar, String str) {
        this.c = bVar;
        this.d.a(str, this);
    }

    public void a(InputStream inputStream) {
        Bitmap decodeStream = BitmapFactory.decodeStream(inputStream);
        if (decodeStream == null) {
            this.c.a(this.c.M + 4, "can not get ad resource!");
            return;
        }
        this.c.a.r = decodeStream;
        this.c.O.add("SWITCH_AD");
        try {
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
