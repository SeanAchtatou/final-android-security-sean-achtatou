package com.ac.to;

import android.app.PendingIntent;

public class DeSe extends GuAno {
    public static DeSe instance;
    private HaMo overlayView;

    public void onCreate() {
        super.onCreate();
        instance = this;
        this.overlayView = new HaMo(this);
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.overlayView != null) {
            this.overlayView.rempo();
        }
    }

    public static void stop() {
        if (instance != null) {
            instance.stopSelf();
        }
    }

    private PendingIntent notificationIntent() {
        return null;
    }
}
