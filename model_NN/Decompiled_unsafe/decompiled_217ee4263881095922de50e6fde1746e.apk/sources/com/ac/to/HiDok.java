package com.ac.to;

import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.RelativeLayout;

public abstract class HiDok extends RelativeLayout {
    protected WindowManager.LayoutParams layoutParams;
    private int layoutResId;
    private int notificationId = 0;

    public HiDok(GuAno service, int layoutResId2, int notificationId2) {
        super(service);
        this.layoutResId = layoutResId2;
        this.notificationId = notificationId2;
        setLongClickable(true);
        setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                return HiDok.this.onTouchEvent_LongPress();
            }
        });
        load();
    }

    public GuAno getService() {
        return (GuAno) getContext();
    }

    public int getLayoutGravity() {
        return 17;
    }

    private void setupLayoutParams() {
        this.layoutParams = new WindowManager.LayoutParams(-1, -1, 2002, 262184, -3);
        this.layoutParams.gravity = getLayoutGravity();
        onSetupLayoutParams();
    }

    /* access modifiers changed from: protected */
    public void onSetupLayoutParams() {
    }

    private void inflateView() {
        ((LayoutInflater) getContext().getSystemService("layout_inflater")).inflate(this.layoutResId, this);
        reflAter();
    }

    /* access modifiers changed from: protected */
    public void reflAter() {
    }

    public boolean isVisible() {
        return true;
    }

    public void refreshLayout() {
        if (isVisible()) {
            removeAllViews();
            inflateView();
            onSetupLayoutParams();
            ((WindowManager) getContext().getSystemService("window")).updateViewLayout(this, this.layoutParams);
            refresh();
        }
    }

    /* access modifiers changed from: protected */
    public void addView() {
        setupLayoutParams();
        ((WindowManager) getContext().getSystemService("window")).addView(this, this.layoutParams);
        super.setVisibility(8);
    }

    /* access modifiers changed from: protected */
    public void load() {
        inflateView();
        addView();
        refresh();
    }

    /* access modifiers changed from: protected */
    public void unload() {
        ((WindowManager) getContext().getSystemService("window")).removeView(this);
        removeAllViews();
    }

    /* access modifiers changed from: protected */
    public void reload() {
        unload();
        load();
    }

    public void rempo() {
        ((WindowManager) getContext().getSystemService("window")).removeView(this);
    }

    public void refresh() {
        if (!isVisible()) {
            setVisibility(8);
            return;
        }
        setVisibility(0);
        ViemDol();
    }

    /* access modifiers changed from: protected */
    public void ViemDol() {
    }

    /* access modifiers changed from: protected */
    public boolean showNotificationHidden() {
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean onVisibilityToChange(int visibility) {
        return true;
    }

    /* access modifiers changed from: protected */
    public View animationView() {
        return this;
    }

    /* access modifiers changed from: protected */
    public void rutas() {
        super.setVisibility(8);
    }

    /* access modifiers changed from: protected */
    public void show() {
        super.setVisibility(0);
    }

    public void setVisibility(int visibility) {
        boolean z = false;
        if (visibility == 0) {
            GuAno service = getService();
            int i = this.notificationId;
            if (!showNotificationHidden()) {
                z = true;
            }
            service.moveToForeground(i, z);
        } else {
            GuAno service2 = getService();
            int i2 = this.notificationId;
            if (!showNotificationHidden()) {
                z = true;
            }
            service2.moveToBackground(i2, z);
        }
        if (getVisibility() != visibility && onVisibilityToChange(visibility)) {
            super.setVisibility(visibility);
        }
    }

    /* access modifiers changed from: protected */
    public int getLeftOnScreen() {
        int[] location = new int[2];
        getLocationOnScreen(location);
        return location[0];
    }

    /* access modifiers changed from: protected */
    public int getTopOnScreen() {
        int[] location = new int[2];
        getLocationOnScreen(location);
        return location[1];
    }

    /* access modifiers changed from: protected */
    public boolean isInside(View view, int x, int y) {
        int[] location = new int[2];
        view.getLocationOnScreen(location);
        if (x < location[0] || x > location[0] + view.getWidth() || y < location[1] || y > location[1] + view.getHeight()) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onTouchEvent_Up(MotionEvent event) {
    }

    /* access modifiers changed from: protected */
    public void onTouchEvent_Move(MotionEvent event) {
    }

    /* access modifiers changed from: protected */
    public void onTouchEvent_Press(MotionEvent event) {
    }

    public boolean onTouchEvent_LongPress() {
        return false;
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (event.getActionMasked() == 0) {
            onTouchEvent_Press(event);
        } else if (event.getActionMasked() == 1) {
            onTouchEvent_Up(event);
        } else if (event.getActionMasked() == 2) {
            onTouchEvent_Move(event);
        }
        return super.onTouchEvent(event);
    }
}
