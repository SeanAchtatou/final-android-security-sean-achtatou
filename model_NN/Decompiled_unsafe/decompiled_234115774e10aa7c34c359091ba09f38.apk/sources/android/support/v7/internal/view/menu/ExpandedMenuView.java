package android.support.v7.internal.view.menu;

import android.content.Context;
import android.support.v7.internal.view.menu.MenuBuilder;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public final class ExpandedMenuView extends ListView implements MenuBuilder.ItemInvoker, MenuView, AdapterView.OnItemClickListener {
    private int mAnimations;
    private MenuBuilder mMenu;

    public ExpandedMenuView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setOnItemClickListener(this);
    }

    public void initialize(MenuBuilder menu) {
        this.mMenu = menu;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        setChildrenDrawingCacheEnabled(false);
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [android.support.v7.internal.view.menu.MenuItemImpl, android.view.MenuItem] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean invokeItem(android.support.v7.internal.view.menu.MenuItemImpl r3) {
        /*
            r2 = this;
            android.support.v7.internal.view.menu.MenuBuilder r0 = r2.mMenu
            r1 = 0
            boolean r0 = r0.performItemAction(r3, r1)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.internal.view.menu.ExpandedMenuView.invokeItem(android.support.v7.internal.view.menu.MenuItemImpl):boolean");
    }

    public void onItemClick(AdapterView parent, View v, int position, long id) {
        invokeItem((MenuItemImpl) getAdapter().getItem(position));
    }

    public int getWindowAnimations() {
        return this.mAnimations;
    }
}
