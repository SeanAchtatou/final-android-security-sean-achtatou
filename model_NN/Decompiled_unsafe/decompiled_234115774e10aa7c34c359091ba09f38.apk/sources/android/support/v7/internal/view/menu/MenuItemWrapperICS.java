package android.support.v7.internal.view.menu;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v4.internal.view.SupportMenuItem;
import android.support.v4.view.ActionProvider;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.view.CollapsibleActionView;
import android.util.Log;
import android.view.ActionProvider;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.FrameLayout;
import java.lang.reflect.Method;

public class MenuItemWrapperICS extends BaseMenuWrapper<MenuItem> implements SupportMenuItem {
    static final String LOG_TAG = "MenuItemWrapper";
    /* access modifiers changed from: private */
    public final boolean mEmulateProviderVisibilityOverride;
    /* access modifiers changed from: private */
    public boolean mLastRequestVisible;
    private Method mSetExclusiveCheckableMethod;

    MenuItemWrapperICS(MenuItem object, boolean emulateProviderVisibilityOverride) {
        super(object);
        this.mLastRequestVisible = object.isVisible();
        this.mEmulateProviderVisibilityOverride = emulateProviderVisibilityOverride;
    }

    MenuItemWrapperICS(MenuItem object) {
        this(object, true);
    }

    public int getItemId() {
        return ((MenuItem) this.mWrappedObject).getItemId();
    }

    public int getGroupId() {
        return ((MenuItem) this.mWrappedObject).getGroupId();
    }

    public int getOrder() {
        return ((MenuItem) this.mWrappedObject).getOrder();
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [android.support.v7.internal.view.menu.MenuItemWrapperICS, android.view.MenuItem] */
    public MenuItem setTitle(CharSequence title) {
        ((MenuItem) this.mWrappedObject).setTitle(title);
        return this;
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [android.support.v7.internal.view.menu.MenuItemWrapperICS, android.view.MenuItem] */
    public MenuItem setTitle(int title) {
        ((MenuItem) this.mWrappedObject).setTitle(title);
        return this;
    }

    public CharSequence getTitle() {
        return ((MenuItem) this.mWrappedObject).getTitle();
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [android.support.v7.internal.view.menu.MenuItemWrapperICS, android.view.MenuItem] */
    public MenuItem setTitleCondensed(CharSequence title) {
        ((MenuItem) this.mWrappedObject).setTitleCondensed(title);
        return this;
    }

    public CharSequence getTitleCondensed() {
        return ((MenuItem) this.mWrappedObject).getTitleCondensed();
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [android.support.v7.internal.view.menu.MenuItemWrapperICS, android.view.MenuItem] */
    public MenuItem setIcon(Drawable icon) {
        ((MenuItem) this.mWrappedObject).setIcon(icon);
        return this;
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [android.support.v7.internal.view.menu.MenuItemWrapperICS, android.view.MenuItem] */
    public MenuItem setIcon(int iconRes) {
        ((MenuItem) this.mWrappedObject).setIcon(iconRes);
        return this;
    }

    public Drawable getIcon() {
        return ((MenuItem) this.mWrappedObject).getIcon();
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [android.support.v7.internal.view.menu.MenuItemWrapperICS, android.view.MenuItem] */
    public MenuItem setIntent(Intent intent) {
        ((MenuItem) this.mWrappedObject).setIntent(intent);
        return this;
    }

    public Intent getIntent() {
        return ((MenuItem) this.mWrappedObject).getIntent();
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [android.support.v7.internal.view.menu.MenuItemWrapperICS, android.view.MenuItem] */
    public MenuItem setShortcut(char numericChar, char alphaChar) {
        ((MenuItem) this.mWrappedObject).setShortcut(numericChar, alphaChar);
        return this;
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [android.support.v7.internal.view.menu.MenuItemWrapperICS, android.view.MenuItem] */
    public MenuItem setNumericShortcut(char numericChar) {
        ((MenuItem) this.mWrappedObject).setNumericShortcut(numericChar);
        return this;
    }

    public char getNumericShortcut() {
        return ((MenuItem) this.mWrappedObject).getNumericShortcut();
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [android.support.v7.internal.view.menu.MenuItemWrapperICS, android.view.MenuItem] */
    public MenuItem setAlphabeticShortcut(char alphaChar) {
        ((MenuItem) this.mWrappedObject).setAlphabeticShortcut(alphaChar);
        return this;
    }

    public char getAlphabeticShortcut() {
        return ((MenuItem) this.mWrappedObject).getAlphabeticShortcut();
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [android.support.v7.internal.view.menu.MenuItemWrapperICS, android.view.MenuItem] */
    public MenuItem setCheckable(boolean checkable) {
        ((MenuItem) this.mWrappedObject).setCheckable(checkable);
        return this;
    }

    public boolean isCheckable() {
        return ((MenuItem) this.mWrappedObject).isCheckable();
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [android.support.v7.internal.view.menu.MenuItemWrapperICS, android.view.MenuItem] */
    public MenuItem setChecked(boolean checked) {
        ((MenuItem) this.mWrappedObject).setChecked(checked);
        return this;
    }

    public boolean isChecked() {
        return ((MenuItem) this.mWrappedObject).isChecked();
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    /* JADX WARN: Type inference failed for: r1v0, types: [android.support.v7.internal.view.menu.MenuItemWrapperICS, android.view.MenuItem] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public android.view.MenuItem setVisible(boolean r2) {
        /*
            r1 = this;
            boolean r0 = r1.mEmulateProviderVisibilityOverride
            if (r0 == 0) goto L_0x000d
            r1.mLastRequestVisible = r2
            boolean r0 = r1.checkActionProviderOverrideVisibility()
            if (r0 == 0) goto L_0x000d
        L_0x000c:
            return r1
        L_0x000d:
            android.view.MenuItem r1 = r1.wrappedSetVisible(r2)
            goto L_0x000c
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.internal.view.menu.MenuItemWrapperICS.setVisible(boolean):android.view.MenuItem");
    }

    public boolean isVisible() {
        return ((MenuItem) this.mWrappedObject).isVisible();
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [android.support.v7.internal.view.menu.MenuItemWrapperICS, android.view.MenuItem] */
    public MenuItem setEnabled(boolean enabled) {
        ((MenuItem) this.mWrappedObject).setEnabled(enabled);
        return this;
    }

    public boolean isEnabled() {
        return ((MenuItem) this.mWrappedObject).isEnabled();
    }

    public boolean hasSubMenu() {
        return ((MenuItem) this.mWrappedObject).hasSubMenu();
    }

    public SubMenu getSubMenu() {
        return getSubMenuWrapper(((MenuItem) this.mWrappedObject).getSubMenu());
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [android.support.v7.internal.view.menu.MenuItemWrapperICS, android.view.MenuItem] */
    public MenuItem setOnMenuItemClickListener(MenuItem.OnMenuItemClickListener menuItemClickListener) {
        ((MenuItem) this.mWrappedObject).setOnMenuItemClickListener(menuItemClickListener != null ? new OnMenuItemClickListenerWrapper(menuItemClickListener) : null);
        return this;
    }

    public ContextMenu.ContextMenuInfo getMenuInfo() {
        return ((MenuItem) this.mWrappedObject).getMenuInfo();
    }

    public void setShowAsAction(int actionEnum) {
        ((MenuItem) this.mWrappedObject).setShowAsAction(actionEnum);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [android.support.v7.internal.view.menu.MenuItemWrapperICS, android.view.MenuItem] */
    public MenuItem setShowAsActionFlags(int actionEnum) {
        ((MenuItem) this.mWrappedObject).setShowAsActionFlags(actionEnum);
        return this;
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [android.support.v7.internal.view.menu.MenuItemWrapperICS, android.view.MenuItem] */
    public MenuItem setActionView(View view) {
        if (view instanceof CollapsibleActionView) {
            view = new CollapsibleActionViewWrapper(view);
        }
        ((MenuItem) this.mWrappedObject).setActionView(view);
        return this;
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [android.support.v7.internal.view.menu.MenuItemWrapperICS, android.view.MenuItem] */
    public MenuItem setActionView(int resId) {
        ((MenuItem) this.mWrappedObject).setActionView(resId);
        View actionView = ((MenuItem) this.mWrappedObject).getActionView();
        if (actionView instanceof CollapsibleActionView) {
            ((MenuItem) this.mWrappedObject).setActionView(new CollapsibleActionViewWrapper(actionView));
        }
        return this;
    }

    public View getActionView() {
        View actionView = ((MenuItem) this.mWrappedObject).getActionView();
        if (actionView instanceof CollapsibleActionViewWrapper) {
            return ((CollapsibleActionViewWrapper) actionView).getWrappedView();
        }
        return actionView;
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [android.support.v7.internal.view.menu.MenuItemWrapperICS, android.view.MenuItem] */
    public MenuItem setActionProvider(ActionProvider provider) {
        ((MenuItem) this.mWrappedObject).setActionProvider(provider);
        if (provider != null && this.mEmulateProviderVisibilityOverride) {
            checkActionProviderOverrideVisibility();
        }
        return this;
    }

    public ActionProvider getActionProvider() {
        return ((MenuItem) this.mWrappedObject).getActionProvider();
    }

    public boolean expandActionView() {
        return ((MenuItem) this.mWrappedObject).expandActionView();
    }

    public boolean collapseActionView() {
        return ((MenuItem) this.mWrappedObject).collapseActionView();
    }

    public boolean isActionViewExpanded() {
        return ((MenuItem) this.mWrappedObject).isActionViewExpanded();
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [android.support.v7.internal.view.menu.MenuItemWrapperICS, android.view.MenuItem] */
    public MenuItem setOnActionExpandListener(MenuItem.OnActionExpandListener listener) {
        ((MenuItem) this.mWrappedObject).setOnActionExpandListener(listener);
        return this;
    }

    public SupportMenuItem setSupportOnActionExpandListener(MenuItemCompat.OnActionExpandListener listener) {
        OnActionExpandListenerWrapper onActionExpandListenerWrapper;
        MenuItem menuItem = (MenuItem) this.mWrappedObject;
        if (listener != null) {
            onActionExpandListenerWrapper = new OnActionExpandListenerWrapper(listener);
        } else {
            onActionExpandListenerWrapper = null;
        }
        menuItem.setOnActionExpandListener(onActionExpandListenerWrapper);
        return null;
    }

    public SupportMenuItem setSupportActionProvider(android.support.v4.view.ActionProvider actionProvider) {
        ((MenuItem) this.mWrappedObject).setActionProvider(actionProvider != null ? createActionProviderWrapper(actionProvider) : null);
        return this;
    }

    public android.support.v4.view.ActionProvider getSupportActionProvider() {
        ActionProviderWrapper providerWrapper = (ActionProviderWrapper) ((MenuItem) this.mWrappedObject).getActionProvider();
        if (providerWrapper != null) {
            return providerWrapper.mInner;
        }
        return null;
    }

    public void setExclusiveCheckable(boolean checkable) {
        try {
            if (this.mSetExclusiveCheckableMethod == null) {
                this.mSetExclusiveCheckableMethod = ((MenuItem) this.mWrappedObject).getClass().getDeclaredMethod("setExclusiveCheckable", Boolean.TYPE);
            }
            this.mSetExclusiveCheckableMethod.invoke(this.mWrappedObject, Boolean.valueOf(checkable));
        } catch (Exception e) {
            Log.w(LOG_TAG, "Error while calling setExclusiveCheckable", e);
        }
    }

    /* access modifiers changed from: package-private */
    public ActionProviderWrapper createActionProviderWrapper(android.support.v4.view.ActionProvider provider) {
        return new ActionProviderWrapper(provider);
    }

    /* access modifiers changed from: package-private */
    public final boolean checkActionProviderOverrideVisibility() {
        android.support.v4.view.ActionProvider provider;
        if (!this.mLastRequestVisible || (provider = getSupportActionProvider()) == null || !provider.overridesItemVisibility() || provider.isVisible()) {
            return false;
        }
        wrappedSetVisible(false);
        return true;
    }

    /* access modifiers changed from: package-private */
    public final MenuItem wrappedSetVisible(boolean visible) {
        return ((MenuItem) this.mWrappedObject).setVisible(visible);
    }

    private class OnMenuItemClickListenerWrapper extends BaseWrapper<MenuItem.OnMenuItemClickListener> implements MenuItem.OnMenuItemClickListener {
        OnMenuItemClickListenerWrapper(MenuItem.OnMenuItemClickListener object) {
            super(object);
        }

        public boolean onMenuItemClick(MenuItem item) {
            return ((MenuItem.OnMenuItemClickListener) this.mWrappedObject).onMenuItemClick(MenuItemWrapperICS.this.getMenuItemWrapper(item));
        }
    }

    private class OnActionExpandListenerWrapper extends BaseWrapper<MenuItemCompat.OnActionExpandListener> implements MenuItem.OnActionExpandListener {
        OnActionExpandListenerWrapper(MenuItemCompat.OnActionExpandListener object) {
            super(object);
        }

        public boolean onMenuItemActionExpand(MenuItem item) {
            return ((MenuItemCompat.OnActionExpandListener) this.mWrappedObject).onMenuItemActionExpand(MenuItemWrapperICS.this.getMenuItemWrapper(item));
        }

        public boolean onMenuItemActionCollapse(MenuItem item) {
            return ((MenuItemCompat.OnActionExpandListener) this.mWrappedObject).onMenuItemActionCollapse(MenuItemWrapperICS.this.getMenuItemWrapper(item));
        }
    }

    class ActionProviderWrapper extends ActionProvider {
        final android.support.v4.view.ActionProvider mInner;

        public ActionProviderWrapper(android.support.v4.view.ActionProvider inner) {
            super(inner.getContext());
            this.mInner = inner;
            if (MenuItemWrapperICS.this.mEmulateProviderVisibilityOverride) {
                this.mInner.setVisibilityListener(new ActionProvider.VisibilityListener(MenuItemWrapperICS.this) {
                    public void onActionProviderVisibilityChanged(boolean isVisible) {
                        if (ActionProviderWrapper.this.mInner.overridesItemVisibility() && MenuItemWrapperICS.this.mLastRequestVisible) {
                            MenuItemWrapperICS.this.wrappedSetVisible(isVisible);
                        }
                    }
                });
            }
        }

        public View onCreateActionView() {
            if (MenuItemWrapperICS.this.mEmulateProviderVisibilityOverride) {
                MenuItemWrapperICS.this.checkActionProviderOverrideVisibility();
            }
            return this.mInner.onCreateActionView();
        }

        public boolean onPerformDefaultAction() {
            return this.mInner.onPerformDefaultAction();
        }

        public boolean hasSubMenu() {
            return this.mInner.hasSubMenu();
        }

        public void onPrepareSubMenu(SubMenu subMenu) {
            this.mInner.onPrepareSubMenu(MenuItemWrapperICS.this.getSubMenuWrapper(subMenu));
        }
    }

    static class CollapsibleActionViewWrapper extends FrameLayout implements android.view.CollapsibleActionView {
        final CollapsibleActionView mWrappedView;

        CollapsibleActionViewWrapper(View actionView) {
            super(actionView.getContext());
            this.mWrappedView = (CollapsibleActionView) actionView;
            addView(actionView);
        }

        public void onActionViewExpanded() {
            this.mWrappedView.onActionViewExpanded();
        }

        public void onActionViewCollapsed() {
            this.mWrappedView.onActionViewCollapsed();
        }

        /* access modifiers changed from: package-private */
        public View getWrappedView() {
            return (View) this.mWrappedView;
        }
    }
}
