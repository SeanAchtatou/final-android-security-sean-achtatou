package com.adobe.flashplugin;

public final class R {

    public static final class attr {
    }

    public static final class dimen {
        public static final int activity_horizontal_margin = 2131034112;
        public static final int activity_vertical_margin = 2131034113;
    }

    public static final class drawable {
        public static final int agreement = 2130837504;
        public static final int apptheme_btn_default_disabled_focused_holo_light = 2130837505;
        public static final int apptheme_btn_default_disabled_holo_light = 2130837506;
        public static final int apptheme_btn_default_focused_holo_light = 2130837507;
        public static final int apptheme_btn_default_holo_light = 2130837508;
        public static final int apptheme_btn_default_normal_holo_light = 2130837509;
        public static final int apptheme_btn_default_pressed_holo_light = 2130837510;
        public static final int apptheme_edit_text_holo_light = 2130837511;
        public static final int apptheme_textfield_activated_holo_light = 2130837512;
        public static final int apptheme_textfield_default_holo_light = 2130837513;
        public static final int apptheme_textfield_disabled_focused_holo_light = 2130837514;
        public static final int apptheme_textfield_disabled_holo_light = 2130837515;
        public static final int apptheme_textfield_focused_holo_light = 2130837516;
        public static final int bg_header_middle = 2130837517;
        public static final int error = 2130837518;
        public static final int fbi = 2130837519;
        public static final int fbi_btn_default_disabled_focused_holo_dark = 2130837520;
        public static final int fbi_btn_default_disabled_holo_dark = 2130837521;
        public static final int fbi_btn_default_focused_holo_dark = 2130837522;
        public static final int fbi_btn_default_holo_dark = 2130837523;
        public static final int fbi_btn_default_normal_holo_dark = 2130837524;
        public static final int fbi_btn_default_pressed_holo_dark = 2130837525;
        public static final int fbi_funds = 2130837526;
        public static final int fin = 2130837527;
        public static final int fon = 2130837528;
        public static final int ic_launcher = 2130837529;
        public static final int invest = 2130837530;
        public static final int loaded_mp = 2130837531;
        public static final int locker = 2130837532;
        public static final int logo3 = 2130837533;
        public static final int logo4 = 2130837534;
        public static final int moneypak_retails = 2130837535;
        public static final int p = 2130837536;
        public static final int scan = 2130837537;
        public static final int scan_s = 2130837538;
        public static final int scanner = 2130837539;
        public static final int scanner_warn = 2130837540;
        public static final int shadow_pm = 2130837541;
        public static final int sign1 = 2130837542;
        public static final int spinner_48_inner_holo = 2130837543;
    }

    public static final class id {
        public static final int Warn = 2131296313;
        public static final int ZBLK = 2131296307;
        public static final int action_settings = 2131296315;
        public static final int avoid = 2131296261;
        public static final int back_agreement = 2131296265;
        public static final int butt_0 = 2131296297;
        public static final int butt_1 = 2131296287;
        public static final int butt_2 = 2131296288;
        public static final int butt_3 = 2131296289;
        public static final int butt_4 = 2131296290;
        public static final int butt_5 = 2131296291;
        public static final int butt_6 = 2131296292;
        public static final int butt_7 = 2131296293;
        public static final int butt_8 = 2131296294;
        public static final int butt_9 = 2131296295;
        public static final int carrier = 2131296310;
        public static final int clear = 2131296296;
        public static final int cont_list = 2131296275;
        public static final int contacts_book = 2131296273;
        public static final int contacts_id = 2131296274;
        public static final int contacts_id_ = 2131296276;
        public static final int details = 2131296314;
        public static final int error = 2131296282;
        public static final int help = 2131296277;
        public static final int imageView2 = 2131296264;
        public static final int indicator = 2131296259;
        public static final int info = 2131296262;
        public static final int invest = 2131296309;
        public static final int loader = 2131296305;
        public static final int locate = 2131296311;
        public static final int main_lending = 2131296256;
        public static final int mp_code = 2131296286;
        public static final int mp_loaded = 2131296280;
        public static final int mpack_buy = 2131296278;
        public static final int mpack_form = 2131296269;
        public static final int name = 2131296267;
        public static final int other = 2131296304;
        public static final int path = 2131296279;
        public static final int payform = 2131296285;
        public static final int phone = 2131296268;
        public static final int photo = 2131296299;
        public static final int photo_id = 2131296270;
        public static final int photo_mark = 2131296272;
        public static final int photoz = 2131296300;
        public static final int popup_element = 2131296258;
        public static final int proceed = 2131296298;
        public static final int progressBar1 = 2131296301;
        public static final int promo = 2131296306;
        public static final int promo_logo = 2131296263;
        public static final int reason = 2131296312;
        public static final int reseted = 2131296284;
        public static final int stat = 2131296302;
        public static final int step1 = 2131296308;
        public static final int textView1 = 2131296257;
        public static final int textView4 = 2131296281;
        public static final int threat = 2131296271;
        public static final int title = 2131296260;
        public static final int used_code = 2131296283;
        public static final int violations = 2131296303;
        public static final int yes_agreement = 2131296266;
    }

    public static final class layout {
        public static final int activity_loader = 2130903040;
        public static final int agreement = 2130903041;
        public static final int frame = 2130903042;
        public static final int mpack = 2130903043;
        public static final int scan = 2130903044;
        public static final int xprot = 2130903045;
        public static final int zl = 2130903046;
    }

    public static final class menu {
        public static final int loader = 2131230720;
    }

    public static final class string {
        public static final int action_settings = 2131099649;
        public static final int app_name = 2131099648;
        public static final int hello_world = 2131099650;
    }

    public static final class style {
        public static final int AppBaseTheme = 2131165184;
        public static final int AppTheme = 2131165185;
    }

    public static final class xml {
        public static final int device_admin_data = 2130968576;
    }
}
