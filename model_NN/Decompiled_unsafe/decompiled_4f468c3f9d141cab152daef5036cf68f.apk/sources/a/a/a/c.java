package a.a.a;

import android.view.View;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import org.a.a;

public class c {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final org.a.c f1a = a.a(c.class);
    private static Class b;
    private static Class c;
    private static Method d;
    private static Method e;
    private static Method f;
    private static Method g;
    private Object h;

    static {
        try {
            Class<?> cls = Class.forName("android.widget.ZoomButtonsController");
            b = cls;
            for (Class<?> cls2 : cls.getDeclaredClasses()) {
                if ("OnZoomListener".equals(cls2.getSimpleName())) {
                    c = cls2;
                }
            }
            d = b.getMethod("setOnZoomListener", c);
            e = b.getMethod("setVisible", Boolean.TYPE);
            f = b.getMethod("setZoomInEnabled", Boolean.TYPE);
            g = b.getMethod("setZoomOutEnabled", Boolean.TYPE);
        } catch (Exception e2) {
            f1a.b("no zoom buttons: " + e2);
        }
    }

    public c(View view) {
        if (b != null) {
            try {
                this.h = b.getConstructor(View.class).newInstance(view);
            } catch (Exception e2) {
                f1a.d("exception instantiating: " + e2);
            }
        }
    }

    public final void a(b bVar) {
        if (this.h != null) {
            try {
                a aVar = new a(this, bVar);
                Object newProxyInstance = Proxy.newProxyInstance(c.getClassLoader(), new Class[]{c}, aVar);
                d.invoke(this.h, newProxyInstance);
            } catch (Exception e2) {
                f1a.d("setOnZoomListener exception: " + e2);
            }
        }
    }

    public final void a(boolean z) {
        if (this.h != null) {
            try {
                e.invoke(this.h, Boolean.valueOf(z));
            } catch (Exception e2) {
                f1a.d("setVisible exception: " + e2);
            }
        }
    }

    public final void b(boolean z) {
        if (this.h != null) {
            try {
                f.invoke(this.h, Boolean.valueOf(z));
            } catch (Exception e2) {
                f1a.d("setZoomInEnabled exception: " + e2);
            }
        }
    }

    public final void c(boolean z) {
        if (this.h != null) {
            try {
                g.invoke(this.h, Boolean.valueOf(z));
            } catch (Exception e2) {
                f1a.d("setZoomOutEnabled exception: " + e2);
            }
        }
    }
}
