package org.a.a;

public final class g extends a {
    public static final g b = new g();

    protected g() {
    }

    public final String a() {
        return "NOP";
    }

    public final void a(String str) {
    }

    public final void a(String str, Object obj, Object obj2) {
    }

    public final void a(String str, Throwable th) {
    }

    public final void b(String str) {
    }

    public final void b(String str, Throwable th) {
    }

    public final void c(String str) {
    }

    public final void c(String str, Throwable th) {
    }

    public final void d(String str) {
    }
}
