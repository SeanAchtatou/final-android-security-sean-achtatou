package org.a.a;

public final class f {
    public static final void a(String str) {
        System.err.println("SLF4J: " + str);
    }

    public static final void a(String str, Throwable th) {
        System.err.println(str);
        System.err.println("Reported exception:");
        th.printStackTrace();
    }
}
