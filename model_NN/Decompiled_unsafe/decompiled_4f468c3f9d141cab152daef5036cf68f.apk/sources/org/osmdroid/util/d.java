package org.osmdroid.util;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public final class d {

    /* renamed from: a  reason: collision with root package name */
    private String f367a;
    private List b;
    private List c;
    private List d;
    private HashMap e;
    private boolean f;
    private int g;

    public d(File file) {
        this(file.getAbsolutePath());
    }

    private d(String str) {
        this.b = new ArrayList();
        this.c = new ArrayList();
        this.d = new ArrayList();
        this.e = new HashMap();
        this.f = false;
        this.g = 0;
        this.f367a = str;
        b();
        c();
    }

    private void b() {
        this.b.add(new RandomAccessFile(new File(this.f367a), "r"));
        int i = 0;
        while (true) {
            i++;
            File file = new File(this.f367a + "-" + i);
            if (file.exists()) {
                this.b.add(new RandomAccessFile(file, "r"));
            } else {
                return;
            }
        }
    }

    private void c() {
        RandomAccessFile randomAccessFile = (RandomAccessFile) this.b.get(0);
        for (RandomAccessFile length : this.b) {
            this.d.add(Long.valueOf(length.length()));
        }
        int readInt = randomAccessFile.readInt();
        if (readInt != 4) {
            throw new IOException("Bad file version: " + readInt);
        }
        int readInt2 = randomAccessFile.readInt();
        if (readInt2 != 256) {
            throw new IOException("Bad tile size: " + readInt2);
        }
        int readInt3 = randomAccessFile.readInt();
        for (int i = 0; i < readInt3; i++) {
            int readInt4 = randomAccessFile.readInt();
            int readInt5 = randomAccessFile.readInt();
            byte[] bArr = new byte[readInt5];
            randomAccessFile.read(bArr, 0, readInt5);
            this.e.put(new Integer(readInt4), new String(bArr));
        }
        int readInt6 = randomAccessFile.readInt();
        for (int i2 = 0; i2 < readInt6; i2++) {
            a aVar = new a(this);
            aVar.f366a = Integer.valueOf(randomAccessFile.readInt());
            aVar.b = Integer.valueOf(randomAccessFile.readInt());
            aVar.c = Integer.valueOf(randomAccessFile.readInt());
            aVar.d = Integer.valueOf(randomAccessFile.readInt());
            aVar.e = Integer.valueOf(randomAccessFile.readInt());
            aVar.f = Integer.valueOf(randomAccessFile.readInt());
            aVar.g = Long.valueOf(randomAccessFile.readLong());
            this.c.add(aVar);
        }
    }

    public final InputStream a(int i, int i2, int i3) {
        a aVar;
        RandomAccessFile randomAccessFile;
        long j;
        Iterator it = this.c.iterator();
        while (true) {
            if (!it.hasNext()) {
                aVar = null;
                break;
            }
            aVar = (a) it.next();
            if (i3 == aVar.f366a.intValue() && i >= aVar.b.intValue() && i <= aVar.c.intValue() && i2 >= aVar.d.intValue() && i2 <= aVar.e.intValue()) {
                if (!this.f || aVar.f.intValue() == this.g) {
                    break;
                }
            }
        }
        if (aVar == null) {
            return null;
        }
        try {
            int intValue = ((aVar.e.intValue() + 1) - aVar.d.intValue()) * (i - aVar.b.intValue());
            RandomAccessFile randomAccessFile2 = (RandomAccessFile) this.b.get(0);
            randomAccessFile2.seek((((long) (intValue + (i2 - aVar.d.intValue()))) * 12) + aVar.g.longValue());
            long readLong = randomAccessFile2.readLong();
            int readInt = randomAccessFile2.readInt();
            RandomAccessFile randomAccessFile3 = (RandomAccessFile) this.b.get(0);
            if (readLong > ((Long) this.d.get(0)).longValue()) {
                int size = this.d.size();
                int i4 = 0;
                while (i4 < size - 1 && readLong > ((Long) this.d.get(i4)).longValue()) {
                    readLong -= ((Long) this.d.get(i4)).longValue();
                    i4++;
                }
                j = readLong;
                randomAccessFile = (RandomAccessFile) this.b.get(i4);
            } else {
                long j2 = readLong;
                randomAccessFile = randomAccessFile3;
                j = j2;
            }
            byte[] bArr = new byte[readInt];
            randomAccessFile.seek(j);
            for (int read = randomAccessFile.read(bArr, 0, readInt); read < readInt; read += randomAccessFile.read(bArr, read, readInt)) {
            }
            return new ByteArrayInputStream(bArr, 0, readInt);
        } catch (IOException e2) {
            return null;
        }
    }

    public final String a() {
        return this.f367a;
    }
}
