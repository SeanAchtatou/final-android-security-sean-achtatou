package org.osmdroid.a.c;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import org.a.a;

public class c {

    /* renamed from: a  reason: collision with root package name */
    private static final org.a.c f339a = a.a(c.class);

    private c() {
    }

    public static long a(InputStream inputStream, OutputStream outputStream) {
        long j = 0;
        byte[] bArr = new byte[8192];
        while (true) {
            int read = inputStream.read(bArr);
            if (read == -1) {
                return j;
            }
            outputStream.write(bArr, 0, read);
            j += (long) read;
        }
    }

    public static void a(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
                f339a.a("IO", "Could not close stream", e);
            }
        }
    }
}
