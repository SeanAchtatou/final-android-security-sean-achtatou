package org.osmdroid.a.b;

import java.io.File;
import java.io.InputStream;
import org.osmdroid.util.d;

public final class f implements m {

    /* renamed from: a  reason: collision with root package name */
    private final d f328a;

    private f(File file) {
        this.f328a = new d(file);
    }

    public static f a(File file) {
        return new f(file);
    }

    public final InputStream a(org.osmdroid.a.a.f fVar, org.osmdroid.a.f fVar2) {
        return this.f328a.a(fVar2.b(), fVar2.c(), fVar2.a());
    }

    public final String toString() {
        return "GEMFFileArchive [mGEMFFile=" + this.f328a.a() + "]";
    }
}
