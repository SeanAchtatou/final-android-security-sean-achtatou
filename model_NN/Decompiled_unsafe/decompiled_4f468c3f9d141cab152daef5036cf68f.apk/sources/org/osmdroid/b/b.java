package org.osmdroid.b;

import android.graphics.Point;
import org.osmdroid.d.a;
import org.osmdroid.util.BoundingBoxE6;

public final class b {

    /* renamed from: a  reason: collision with root package name */
    private final f f353a;

    public b(f fVar) {
        this.f353a = fVar;
    }

    public final void a(int i, int i2) {
        if (i > 0 && i2 > 0) {
            BoundingBoxE6 d = this.f353a.d();
            int f = this.f353a.f();
            float max = Math.max(((float) i) / ((float) d.c()), ((float) i2) / ((float) d.d()));
            if (max > 1.0f) {
                this.f353a.b(f - org.osmdroid.b.b.b.a(max));
            } else if (((double) max) < 0.5d) {
                this.f353a.b((org.osmdroid.b.b.b.a(1.0f / max) + f) - 1);
            }
        }
    }

    public final void a(a aVar) {
        Point a2 = org.osmdroid.b.b.a.a(((double) aVar.a()) * 1.0E-6d, ((double) aVar.b()) * 1.0E-6d, this.f353a.k());
        int j = this.f353a.j() / 2;
        this.f353a.scrollTo(a2.x - j, a2.y - j);
    }

    public final void a(BoundingBoxE6 boundingBoxE6) {
        a(boundingBoxE6.c(), boundingBoxE6.d());
    }

    public final boolean a() {
        return this.f353a.g();
    }

    public final boolean b() {
        return this.f353a.h();
    }
}
