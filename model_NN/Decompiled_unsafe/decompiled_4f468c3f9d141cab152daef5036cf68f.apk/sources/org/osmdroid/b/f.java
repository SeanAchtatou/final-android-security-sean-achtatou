package org.osmdroid.b;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Point;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.ScaleAnimation;
import android.widget.Scroller;
import java.util.LinkedList;
import java.util.List;
import org.a.a;
import org.a.c;
import org.b.a.a.b;
import org.b.a.a.d;
import org.osmdroid.a.e;
import org.osmdroid.b.a.i;
import org.osmdroid.util.BoundingBoxE6;
import org.osmdroid.util.GeoPoint;

public class f extends View implements b {

    /* renamed from: a  reason: collision with root package name */
    private static final c f357a = a.a(f.class);
    private static final double b = (1.0d / Math.log(1.5384615384615383d));
    /* access modifiers changed from: private */
    public int c;
    /* access modifiers changed from: private */
    public int d;
    /* access modifiers changed from: private */
    public final LinkedList e;
    private a f;
    private final i g;
    private final GestureDetector h;
    /* access modifiers changed from: private */
    public final Scroller i;
    private final ScaleAnimation j;
    private final ScaleAnimation k;
    private final d l;
    private final b m;
    /* access modifiers changed from: private */
    public final a.a.a.c n;
    /* access modifiers changed from: private */
    public boolean o;
    private org.osmdroid.b p;
    private d q;
    private float r;
    private org.osmdroid.c.a s;
    private final Matrix t;
    private final org.osmdroid.a.i u;
    private final Handler v;

    public f(Context context) {
        this(context, null);
    }

    private f(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.c = 0;
        this.d = 0;
        this.e = new LinkedList();
        this.l = new d(this);
        this.o = false;
        this.r = 1.0f;
        this.t = new Matrix();
        this.p = new org.osmdroid.c(context);
        this.m = new b(this);
        this.i = new Scroller(context);
        this.d = 256;
        e eVar = new e(context, a(attributeSet));
        this.v = new org.osmdroid.a.c.d(this);
        this.u = eVar;
        this.u.a(this.v);
        this.g = new i(this.u, this.p);
        this.e.add(this.g);
        this.n = new a.a.a.c(this);
        this.n.a(new c(this));
        this.j = new ScaleAnimation(1.0f, 2.0f, 1.0f, 2.0f, 1, 0.5f, 1, 0.5f);
        this.k = new ScaleAnimation(1.0f, 0.5f, 1.0f, 0.5f, 1, 0.5f, 1, 0.5f);
        this.j.setDuration(500);
        this.k.setDuration(500);
        this.j.setAnimationListener(this.l);
        this.k.setAnimationListener(this.l);
        this.h = new GestureDetector(context, new g(this));
        this.h.setOnDoubleTapListener(new e(this));
    }

    public static int a(int i2) {
        int i3 = 0;
        if (i2 <= 0) {
            return 0;
        }
        int i4 = i2;
        while (i4 != 0) {
            i4 >>= 1;
            i3++;
        }
        return i3 - 1;
    }

    static /* synthetic */ Point a(f fVar, Point point, int i2) {
        Point point2 = new Point();
        int i3 = 1 << (fVar.c - 1);
        point2.set(((point.x - i3) * i2) - (i2 / 2), ((point.y - i3) * i2) - (i2 / 2));
        return point2;
    }

    /* JADX WARN: Type inference failed for: r2v12, types: [org.osmdroid.a.a.f, java.lang.Object] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static org.osmdroid.a.a.f a(android.util.AttributeSet r7) {
        /*
            r6 = 0
            org.osmdroid.a.a.d r1 = org.osmdroid.a.a.b.f322a
            if (r7 == 0) goto L_0x002a
            java.lang.String r2 = "tilesource"
            java.lang.String r2 = r7.getAttributeValue(r6, r2)
            if (r2 == 0) goto L_0x002a
            org.osmdroid.a.a.f r2 = org.osmdroid.a.a.b.a(r2)     // Catch:{ IllegalArgumentException -> 0x0060 }
            org.a.c r3 = org.osmdroid.b.f.f357a     // Catch:{ IllegalArgumentException -> 0x0060 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ IllegalArgumentException -> 0x0060 }
            r4.<init>()     // Catch:{ IllegalArgumentException -> 0x0060 }
            java.lang.String r5 = "Using tile source specified in layout attributes: "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ IllegalArgumentException -> 0x0060 }
            java.lang.StringBuilder r4 = r4.append(r2)     // Catch:{ IllegalArgumentException -> 0x0060 }
            java.lang.String r4 = r4.toString()     // Catch:{ IllegalArgumentException -> 0x0060 }
            r3.b(r4)     // Catch:{ IllegalArgumentException -> 0x0060 }
            r1 = r2
        L_0x002a:
            if (r7 == 0) goto L_0x0047
            boolean r2 = r1 instanceof org.osmdroid.a.a.g
            if (r2 == 0) goto L_0x0047
            java.lang.String r2 = "style"
            java.lang.String r2 = r7.getAttributeValue(r6, r2)
            if (r2 != 0) goto L_0x003e
            java.lang.String r2 = "cloudmadeStyle"
            java.lang.String r2 = r7.getAttributeValue(r6, r2)
        L_0x003e:
            if (r2 != 0) goto L_0x007a
            org.a.c r2 = org.osmdroid.b.f.f357a
            java.lang.String r3 = "Using default style: 1"
            r2.b(r3)
        L_0x0047:
            org.a.c r2 = org.osmdroid.b.f.f357a
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Using tile source: "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r3 = r3.append(r1)
            java.lang.String r3 = r3.toString()
            r2.b(r3)
            return r1
        L_0x0060:
            r2 = move-exception
            org.a.c r2 = org.osmdroid.b.f.f357a
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Invalid tile souce specified in layout attributes: "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r3 = r3.append(r1)
            java.lang.String r3 = r3.toString()
            r2.c(r3)
            goto L_0x002a
        L_0x007a:
            org.a.c r3 = org.osmdroid.b.f.f357a
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "Using style specified in layout attributes: "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r4 = r4.append(r2)
            java.lang.String r4 = r4.toString()
            r3.b(r4)
            r0 = r1
            org.osmdroid.a.a.g r0 = (org.osmdroid.a.a.g) r0
            r7 = r0
            r7.a(r2)
            goto L_0x0047
        */
        throw new UnsupportedOperationException("Method not decompiled: org.osmdroid.b.f.a(android.util.AttributeSet):org.osmdroid.a.a.f");
    }

    private boolean l() {
        int d2 = this.g.d();
        if (this.c >= d2) {
            return false;
        }
        return !this.l.b || this.l.f355a < d2;
    }

    private boolean m() {
        int c2 = this.g.c();
        if (this.c <= c2) {
            return false;
        }
        return !this.l.b || this.l.f355a > c2;
    }

    public final Object a() {
        return this;
    }

    public final void a(MotionEvent motionEvent) {
        int size = this.e.size() - 1;
        while (size >= 0 && !((org.osmdroid.b.a.e) this.e.get(size)).b(motionEvent, this)) {
            size--;
        }
    }

    public final void a(Object obj) {
        if (obj == null && this.r != 1.0f) {
            b(Math.round((float) (Math.log((double) this.r) * b)) + this.c);
        }
        this.r = 1.0f;
    }

    public final void a(org.b.a.a.a aVar) {
        aVar.a(this.r);
    }

    /* access modifiers changed from: package-private */
    public final boolean a(GeoPoint geoPoint) {
        Point a2 = org.osmdroid.b.b.a.a(geoPoint.a(), geoPoint.b(), k());
        int k2 = (1 << k()) / 2;
        if (getAnimation() == null || getAnimation().hasEnded()) {
            f357a.a("StartScroll");
            this.i.startScroll(getScrollX(), getScrollY(), (a2.x - k2) - getScrollX(), (a2.y - k2) - getScrollY(), 500);
            postInvalidate();
        }
        return g();
    }

    /* access modifiers changed from: package-private */
    public final int b(int i2) {
        int max = Math.max(this.g.c(), Math.min(this.g.d(), i2));
        int i3 = this.c;
        this.c = max;
        this.n.b(l());
        this.n.c(m());
        if (max > i3) {
            scrollTo(getScrollX() << (max - i3), getScrollY() << (max - i3));
        } else if (max < i3) {
            scrollTo(getScrollX() >> (i3 - max), getScrollY() >> (i3 - max));
        }
        Point point = new Point();
        this.f = new a(this);
        for (int size = this.e.size() - 1; size >= 0; size--) {
            if (this.e.get(size) instanceof org.osmdroid.b.a.f) {
                getScrollX();
                getScrollY();
                if (((org.osmdroid.b.a.f) this.e.get(size)).a()) {
                    scrollTo(point.x, point.y);
                }
            }
        }
        if (!(max == i3 || this.s == null)) {
            new org.osmdroid.c.b(this, max);
        }
        return this.c;
    }

    public final b b() {
        return this.m;
    }

    public final boolean b(MotionEvent motionEvent) {
        for (int size = this.e.size() - 1; size >= 0; size--) {
            if (((org.osmdroid.b.a.e) this.e.get(size)).a(motionEvent, this)) {
                postInvalidate();
                return true;
            }
        }
        return false;
    }

    public final boolean b(org.b.a.a.a aVar) {
        this.r = aVar.a();
        invalidate();
        return true;
    }

    public final List c() {
        return this.e;
    }

    public void computeScroll() {
        if (this.i.computeScrollOffset()) {
            if (this.i.isFinished()) {
                b(this.c);
            } else {
                scrollTo(this.i.getCurrX(), this.i.getCurrY());
            }
            postInvalidate();
        }
    }

    public final BoundingBoxE6 d() {
        getWidth();
        getHeight();
        int a2 = a(this.d);
        int i2 = 1 << ((this.c + a2) - 1);
        int scrollY = (getScrollY() + i2) - (getHeight() / 2);
        int scrollY2 = getScrollY() + i2 + (getHeight() / 2);
        int scrollX = (getScrollX() + i2) - (getWidth() / 2);
        int width = (getWidth() / 2) + i2 + getScrollX();
        int i3 = this.c + a2;
        return new BoundingBoxE6(org.osmdroid.b.b.a.b(scrollY, i3), org.osmdroid.b.b.a.a(width, i3), org.osmdroid.b.b.a.b(scrollY2, i3), org.osmdroid.b.b.a.a(scrollX, i3));
    }

    public final a e() {
        if (this.f == null) {
            this.f = new a(this);
        }
        return this.f;
    }

    public final int f() {
        return this.l.b ? this.l.f355a : this.c;
    }

    /* access modifiers changed from: package-private */
    public final boolean g() {
        if (!l()) {
            return false;
        }
        if (this.l.b) {
            return false;
        }
        int unused = this.l.f355a = this.c + 1;
        boolean unused2 = this.l.b = true;
        startAnimation(this.j);
        return true;
    }

    /* access modifiers changed from: package-private */
    public final boolean h() {
        if (!m()) {
            return false;
        }
        if (this.l.b) {
            return false;
        }
        int unused = this.l.f355a = this.c - 1;
        boolean unused2 = this.l.b = true;
        startAnimation(this.k);
        return true;
    }

    public final boolean i() {
        return this.l.b;
    }

    /* access modifiers changed from: package-private */
    public final int j() {
        return 1 << k();
    }

    /* access modifiers changed from: package-private */
    public final int k() {
        return this.c + a(this.d);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        this.n.a(false);
        for (int size = this.e.size() - 1; size >= 0; size--) {
            ((org.osmdroid.b.a.e) this.e.get(size)).b();
        }
        super.onDetachedFromWindow();
    }

    public void onDraw(Canvas canvas) {
        System.currentTimeMillis();
        this.f = new a(this);
        if (this.r == 1.0f) {
            canvas.translate((float) (getWidth() / 2), (float) (getHeight() / 2));
        } else {
            canvas.getMatrix(this.t);
            this.t.postTranslate((float) (getWidth() / 2), (float) (getHeight() / 2));
            this.t.preScale(this.r, this.r, (float) getScrollX(), (float) getScrollY());
            canvas.setMatrix(this.t);
        }
        canvas.drawColor(-3355444);
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < this.e.size()) {
                ((org.osmdroid.b.a.e) this.e.get(i3)).b(canvas, this);
                i2 = i3 + 1;
            } else {
                System.currentTimeMillis();
                return;
            }
        }
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        for (int size = this.e.size() - 1; size >= 0; size--) {
            this.e.get(size);
        }
        return super.onKeyDown(i2, keyEvent);
    }

    public boolean onKeyUp(int i2, KeyEvent keyEvent) {
        for (int size = this.e.size() - 1; size >= 0; size--) {
            this.e.get(size);
        }
        return super.onKeyUp(i2, keyEvent);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        for (int size = this.e.size() - 1; size >= 0; size--) {
            this.e.get(size);
        }
        if (this.q != null && this.q.a(motionEvent)) {
            return true;
        }
        if (this.h.onTouchEvent(motionEvent)) {
            return true;
        }
        return super.onTouchEvent(motionEvent);
    }

    public boolean onTrackballEvent(MotionEvent motionEvent) {
        for (int size = this.e.size() - 1; size >= 0; size--) {
            this.e.get(size);
        }
        scrollBy((int) (motionEvent.getX() * 25.0f), (int) (motionEvent.getY() * 25.0f));
        return super.onTrackballEvent(motionEvent);
    }

    public void scrollTo(int i2, int i3) {
        int k2 = 1 << k();
        int i4 = i2 % k2;
        int i5 = i3 % k2;
        super.scrollTo(i4, i5);
        if (this.s != null) {
            new org.osmdroid.c.c(this, i4, i5);
        }
    }
}
