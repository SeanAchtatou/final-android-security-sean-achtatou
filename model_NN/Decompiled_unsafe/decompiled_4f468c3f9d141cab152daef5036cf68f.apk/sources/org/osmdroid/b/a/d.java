package org.osmdroid.b.a;

final /* synthetic */ class d {

    /* renamed from: a  reason: collision with root package name */
    static final /* synthetic */ int[] f347a = new int[b.values().length];

    static {
        try {
            f347a[b.CUSTOM.ordinal()] = 1;
        } catch (NoSuchFieldError e) {
        }
        try {
            f347a[b.CENTER.ordinal()] = 2;
        } catch (NoSuchFieldError e2) {
        }
        try {
            f347a[b.BOTTOM_CENTER.ordinal()] = 3;
        } catch (NoSuchFieldError e3) {
        }
        try {
            f347a[b.TOP_CENTER.ordinal()] = 4;
        } catch (NoSuchFieldError e4) {
        }
        try {
            f347a[b.RIGHT_CENTER.ordinal()] = 5;
        } catch (NoSuchFieldError e5) {
        }
        try {
            f347a[b.LEFT_CENTER.ordinal()] = 6;
        } catch (NoSuchFieldError e6) {
        }
        try {
            f347a[b.UPPER_RIGHT_CORNER.ordinal()] = 7;
        } catch (NoSuchFieldError e7) {
        }
        try {
            f347a[b.LOWER_RIGHT_CORNER.ordinal()] = 8;
        } catch (NoSuchFieldError e8) {
        }
        try {
            f347a[b.UPPER_LEFT_CORNER.ordinal()] = 9;
        } catch (NoSuchFieldError e9) {
        }
        try {
            f347a[b.LOWER_LEFT_CORNER.ordinal()] = 10;
        } catch (NoSuchFieldError e10) {
        }
    }
}
