package com.agilebinary.mobilemonitor.client.android.ui;

import android.app.Dialog;
import android.view.View;

final class bx implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ Dialog f262a;
    private /* synthetic */ MapActivity_GOOGLE b;

    bx(MapActivity_GOOGLE mapActivity_GOOGLE, Dialog dialog) {
        this.b = mapActivity_GOOGLE;
        this.f262a = dialog;
    }

    public final void onClick(View view) {
        this.b.e.a(!this.b.e.a());
        this.b.b.invalidate();
        this.b.i.b("MAP_LAYER_DIRECTIONS__BOOL", this.b.e.a());
        this.f262a.dismiss();
    }
}
