package com.agilebinary.mobilemonitor.client.android.ui;

import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import com.agilebinary.mobilemonitor.client.a.b.f;
import com.agilebinary.mobilemonitor.client.a.b.o;
import com.agilebinary.mobilemonitor.client.a.b.t;
import com.agilebinary.mobilemonitor.client.android.c.a;
import com.agilebinary.mobilemonitor.client.android.c.c;
import com.agilebinary.phonebeagle.client.R;

public class EventDetailsActivity_MMS extends EventDetailsActivity_base {

    /* renamed from: a  reason: collision with root package name */
    private TableLayout f196a;
    private TextView b;
    private TextView c;
    private TextView d;
    private TableRow e;
    private TableRow f;
    private TableRow h;
    private LayoutInflater i;
    private ViewGroup k;

    private int a(int i2, int i3, String str) {
        TableRow tableRow = (TableRow) this.i.inflate((int) R.layout.eventdetails_row, (ViewGroup) null);
        ((TextView) tableRow.getChildAt(0)).setText(i3);
        ((TextView) tableRow.getChildAt(1)).setText(str);
        this.f196a.addView(tableRow, i2);
        return 1;
    }

    private int a(int i2, int i3, String str, String str2) {
        return a(i2, i3, a.a(str, str2));
    }

    private int a(int i2, int i3, String[] strArr, String[] strArr2) {
        for (int i4 = 0; i4 < strArr2.length; i4++) {
            a(i2 + i4, i3, strArr[i4], strArr2[i4]);
        }
        return strArr.length;
    }

    private int a(int i2, t tVar) {
        boolean z;
        boolean z2;
        TableRow tableRow = new TableRow(this);
        tableRow.setBackgroundColor(i2 % 2 == 0 ? R.color.background_mms_part_even : R.color.background_mms_part_odd);
        tableRow.setLayoutParams(new TableLayout.LayoutParams(-2, -2));
        if (tVar.d()) {
            TextView textView = (TextView) this.i.inflate((int) R.layout.eventdetails_mms_textpart, (ViewGroup) null);
            TableRow.LayoutParams layoutParams = new TableRow.LayoutParams(-1, -2);
            layoutParams.span = 2;
            textView.setLayoutParams(layoutParams);
            tableRow.addView(textView);
            textView.setText(tVar.f());
            z = true;
        } else {
            z = false;
        }
        if (!z && tVar.c() != null) {
            try {
                LinearLayout linearLayout = (LinearLayout) this.i.inflate((int) R.layout.eventdetails_mms_imagepart, (ViewGroup) null);
                TableRow.LayoutParams layoutParams2 = new TableRow.LayoutParams(-1, -2);
                layoutParams2.span = 2;
                linearLayout.setLayoutParams(layoutParams2);
                tableRow.addView(linearLayout);
                ImageView imageView = (ImageView) linearLayout.getChildAt(0);
                imageView.setAdjustViewBounds(true);
                imageView.setImageBitmap(BitmapFactory.decodeByteArray(tVar.c(), 0, tVar.c().length));
                z2 = true;
            } catch (OutOfMemoryError e2) {
                e2.printStackTrace();
                z2 = z;
            }
            if (!z2) {
                TextView textView2 = (TextView) this.i.inflate((int) R.layout.eventdetails_mms_textpart, (ViewGroup) null);
                TableRow.LayoutParams layoutParams3 = new TableRow.LayoutParams(-1, -2);
                layoutParams3.span = 2;
                textView2.setLayoutParams(layoutParams3);
                tableRow.addView(textView2);
                textView2.setText(getString(R.string.label_event_mms_part_cantshow, new Object[]{tVar.a(), tVar.b(), Integer.valueOf(tVar.e())}));
            }
        }
        this.f196a.addView(tableRow, i2);
        return 1;
    }

    private static int a(ViewGroup viewGroup, View view) {
        int childCount = viewGroup.getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            if (viewGroup.getChildAt(i2) == view) {
                return i2;
            }
        }
        return -1;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: protected */
    public final void a(ViewGroup viewGroup) {
        this.i = (LayoutInflater) getSystemService("layout_inflater");
        this.i.inflate((int) R.layout.eventdetails_mms, viewGroup, true);
        this.f196a = (TableLayout) findViewById(R.id.eventdetails_mms_table);
        this.b = (TextView) findViewById(R.id.eventdetails_mms_kind);
        this.c = (TextView) findViewById(R.id.eventdetails_mms_time);
        this.d = (TextView) findViewById(R.id.eventdetails_mms_speed);
        this.e = (TableRow) findViewById(R.id.eventdetails_mms_row_kind);
        this.f = (TableRow) findViewById(R.id.eventdetails_mms_row_time);
        this.h = (TableRow) findViewById(R.id.eventdetails_mms_row_speed);
        this.k = viewGroup;
    }

    /* access modifiers changed from: protected */
    public final void a(o oVar) {
        super.a(oVar);
        int a2 = a(this.f196a, this.f);
        int a3 = (a(this.f196a, this.h) - a2) - 1;
        for (int i2 = 0; i2 < a3; i2++) {
            this.f196a.removeViewAt(a2 + 1);
        }
        f fVar = (f) oVar;
        this.b.setText(getResources().getString(fVar.x() == 1 ? R.string.label_event_mms_incoming : R.string.label_event_mms_outgoing));
        this.c.setText(c.a().c(fVar.x() == 1 ? fVar.w() : fVar.v()));
        this.d.setText(a.a(this, fVar));
        if (fVar.x() != 2 || !a.a(fVar)) {
            this.d.setTextColor(getResources().getColor(R.color.text));
        } else {
            this.d.setTextColor(getResources().getColor(R.color.warning));
        }
        int a4 = a(this.f196a, this.f) + 1;
        if (fVar.x() == 1) {
            a4 += a(a4, (int) R.string.label_event_mms_header_from, fVar.b(), fVar.c());
        }
        int a5 = a4 + a(a4, (int) R.string.label_event_mms_header_to, fVar.d(), fVar.e());
        int a6 = a5 + a(a5, (int) R.string.label_event_mms_header_cc, fVar.f(), fVar.g());
        int a7 = a6 + a(a6, (int) R.string.label_event_mms_header_bcc, fVar.h(), fVar.i());
        if (!a.a(fVar.a())) {
            a7 += a(a7, R.string.label_event_mms_header_subject, fVar.a());
        }
        int i3 = a7;
        for (t a8 : fVar.j()) {
            i3 += a(i3, a8);
        }
    }
}
