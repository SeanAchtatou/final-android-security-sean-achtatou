package com.agilebinary.mobilemonitor.client.android;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import com.agilebinary.mobilemonitor.a.a.a.a;
import com.agilebinary.mobilemonitor.client.a.b;
import com.agilebinary.mobilemonitor.client.a.e;
import com.agilebinary.mobilemonitor.client.android.a.a.f;
import com.agilebinary.mobilemonitor.client.android.a.c.c;
import com.agilebinary.mobilemonitor.client.android.a.d;
import com.agilebinary.mobilemonitor.client.android.a.g;
import com.agilebinary.mobilemonitor.client.android.a.m;
import com.agilebinary.mobilemonitor.client.android.b.j;
import com.agilebinary.mobilemonitor.client.android.ui.BaseActivity;
import com.agilebinary.mobilemonitor.client.android.ui.LoginActivity;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class MyApplication extends Application {

    /* renamed from: a  reason: collision with root package name */
    public static b f141a = new e();
    private static final String b = com.agilebinary.mobilemonitor.client.android.c.b.a();
    private static e k;
    private d c;
    private m d;
    private boolean e = true;
    private d f;
    private i g;
    private j h;
    private int i = 0;
    private Map j = new HashMap();

    public static e g() {
        return k;
    }

    private void i() {
        this.g = new i(this);
        k = this.e ? new f(this) : new g(this);
    }

    private void j() {
        try {
            if (this.c != null) {
                FileOutputStream openFileOutput = openFileOutput("account", 0);
                ObjectOutputStream objectOutputStream = new ObjectOutputStream(openFileOutput);
                objectOutputStream.writeBoolean(this.e);
                objectOutputStream.writeObject(this.c.a());
                objectOutputStream.close();
                openFileOutput.close();
                return;
            }
            k();
        } catch (Exception e2) {
            a.e(e2);
        }
    }

    private void k() {
        try {
            deleteFile("account");
        } catch (Exception e2) {
            a.e(e2);
        }
    }

    public final d a() {
        return this.c;
    }

    public final void a(byte b2, int i2) {
        this.j.put(Byte.valueOf(b2), Integer.valueOf(i2));
    }

    public final void a(Context context) {
        BaseActivity.k();
        e().b();
        f();
        k();
        this.c = null;
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(67108864);
        context.startActivity(intent);
    }

    public final void a(String str) {
        this.c.a(str);
        j();
    }

    public final boolean a(byte b2) {
        Integer num = (Integer) this.j.get(Byte.valueOf(b2));
        return num != null && num.intValue() > 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.client.android.d.<init>(com.agilebinary.mobilemonitor.client.android.a.a.f, boolean):void
     arg types: [com.agilebinary.mobilemonitor.client.android.a.a.f, int]
     candidates:
      com.agilebinary.mobilemonitor.client.android.d.<init>(com.agilebinary.mobilemonitor.client.android.a.a.g, boolean):void
      com.agilebinary.mobilemonitor.client.android.d.<init>(com.agilebinary.mobilemonitor.client.android.a.a.f, boolean):void */
    public final boolean a(String str, String str2, g gVar) {
        if (str == null) {
            this.d = new c();
            this.c = new d(new f("DEMO", "DEMO", System.currentTimeMillis(), "DEMO", "demo@phonebeagle.com", "", "01234567890ABCDEF", true, System.currentTimeMillis(), true, System.currentTimeMillis(), "applicationCategoryString", "applicationIdString", "applicationPlatformString", "applicationVersionString", "activationHistoryString"), true);
            this.e = true;
            i();
            j();
            return true;
        }
        this.d = new com.agilebinary.mobilemonitor.client.android.a.b.f();
        this.f = this.d.a();
        try {
            this.c = new d(this.f.a(str, com.agilebinary.mobilemonitor.client.android.c.e.a(str2), gVar), false);
            this.e = false;
            j();
            i();
            return true;
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
            this.f = null;
            return false;
        }
    }

    public final m b() {
        return this.d;
    }

    public final Integer b(byte b2) {
        return (Integer) this.j.get(Byte.valueOf(b2));
    }

    public final void b(String str) {
        this.c.b(str);
        j();
    }

    public final boolean c() {
        return this.c != null;
    }

    public final boolean c(byte b2) {
        return ((Integer) this.j.get(Byte.valueOf(b2))) == null;
    }

    public final boolean d() {
        return this.e;
    }

    public final synchronized j e() {
        if (this.h == null) {
            this.h = new j(this, this.g);
        }
        this.i++;
        "mEventStoreAllocationCount=" + this.i;
        return this.h;
    }

    public final synchronized void f() {
        this.i--;
        "mEventStoreAllocationCount=" + this.i;
        if (this.i == 0) {
            this.h.a();
            this.h = null;
        }
    }

    public final void h() {
        for (byte valueOf : j.f174a) {
            this.j.remove(Byte.valueOf(valueOf));
        }
    }

    public void onCreate() {
        super.onCreate();
        a.f121a = new com.agilebinary.mobilemonitor.client.android.d.a();
        try {
            FileInputStream openFileInput = openFileInput("account");
            ObjectInputStream objectInputStream = new ObjectInputStream(openFileInput);
            this.e = objectInputStream.readBoolean();
            this.c = new d((f) objectInputStream.readObject(), this.e);
            objectInputStream.close();
            openFileInput.close();
        } catch (Exception e2) {
            try {
                FileInputStream openFileInput2 = openFileInput("account");
                ObjectInputStream objectInputStream2 = new ObjectInputStream(openFileInput2);
                this.e = objectInputStream2.readBoolean();
                this.c = new d((com.agilebinary.mobilemonitor.client.android.a.a.g) objectInputStream2.readObject(), this.e);
                objectInputStream2.close();
                openFileInput2.close();
            } catch (Exception e3) {
            }
        }
        if (this.c != null) {
            this.d = this.e ? new c() : new com.agilebinary.mobilemonitor.client.android.a.b.f();
            i();
        }
    }

    public void onLowMemory() {
        super.onLowMemory();
        if (this.h != null) {
            j.e();
        }
    }
}
