package com.agilebinary.mobilemonitor.client.android.ui.map;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import com.agilebinary.mobilemonitor.client.a.b.n;
import com.agilebinary.mobilemonitor.client.a.b.s;
import com.agilebinary.mobilemonitor.client.a.d;
import com.agilebinary.phonebeagle.client.R;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.Projection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public final class m extends Overlay {

    /* renamed from: a  reason: collision with root package name */
    private Paint f288a = new Paint();
    private Paint b;
    private Paint c;
    private Paint d;
    private List e;
    private Set f;
    private int g = -1;
    private boolean h;

    public m(Context context) {
        this.f288a.setColor(context.getResources().getColor(R.color.accuraccy_coords_fill));
        this.f288a.setStyle(Paint.Style.FILL);
        this.f288a.setAntiAlias(true);
        this.b = new Paint();
        this.b.setColor(context.getResources().getColor(R.color.accuraccy_coords_border));
        this.b.setStyle(Paint.Style.STROKE);
        this.b.setStrokeWidth(1.0f);
        this.b.setAntiAlias(true);
        this.c = new Paint();
        this.c.setColor(context.getResources().getColor(R.color.accuraccy_cell_fill));
        this.c.setStyle(Paint.Style.FILL);
        this.c.setAntiAlias(true);
        this.d = new Paint();
        this.d.setColor(context.getResources().getColor(R.color.accuraccy_cell_border));
        this.d.setStyle(Paint.Style.STROKE);
        this.d.setStrokeWidth(1.0f);
        this.d.setAntiAlias(true);
    }

    public final void a(List list) {
        this.e = list;
    }

    public final void a(boolean z) {
        this.h = z;
    }

    public final boolean a() {
        return this.h;
    }

    public final void draw(Canvas canvas, MapView mapView, boolean z) {
        m.super.draw(canvas, mapView, z);
        if (this.h && this.e != null && this.e.size() != 0) {
            Projection projection = mapView.getProjection();
            this.f = new HashSet(this.e.size());
            Point point = new Point();
            for (s sVar : this.e) {
                d dVar = (d) sVar;
                projection.toPixels(dVar.i(), point);
                this.f.add(new c(this, point.x, point.y, projection.metersToEquatorPixels((float) dVar.g()), dVar instanceof n ? this.c : this.f288a, dVar instanceof n ? this.d : this.b));
            }
            for (c cVar : this.f) {
                canvas.drawCircle((float) cVar.f279a, (float) cVar.b, cVar.c, cVar.d);
                canvas.drawCircle((float) cVar.f279a, (float) cVar.b, cVar.c, cVar.e);
            }
        }
    }
}
