package com.agilebinary.mobilemonitor.client.android.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import com.agilebinary.mobilemonitor.client.android.c.b;
import com.agilebinary.mobilemonitor.client.android.ui.a.c;
import com.agilebinary.mobilemonitor.client.android.ui.a.f;
import com.agilebinary.phonebeagle.client.R;

public class ResetPasswordActivity extends BaseActivity {

    /* renamed from: a  reason: collision with root package name */
    private static final String f206a = b.a();
    /* access modifiers changed from: private */
    public EditText b;
    private Button c;
    private Button d;

    public static void a(Activity activity) {
        activity.startActivityForResult(new Intent(activity, ResetPasswordActivity.class), 2);
    }

    public static void a(Context context, c cVar) {
        Intent intent = new Intent(context, ResetPasswordActivity.class);
        intent.putExtra("EXTRA_SERVICE_RESULT", cVar);
        intent.setFlags(536870912);
        context.startActivity(intent);
    }

    static /* synthetic */ void a(ResetPasswordActivity resetPasswordActivity, String str) {
        resetPasswordActivity.a((int) R.string.msg_progress_communicating);
        f fVar = new f(resetPasswordActivity);
        f.f213a = fVar;
        fVar.execute(str);
    }

    /* access modifiers changed from: protected */
    public final int a() {
        return R.layout.reset_password;
    }

    /* access modifiers changed from: protected */
    public final void b() {
        if (f.f213a != null) {
            try {
                f.f213a.a();
            } catch (Exception e) {
            }
        }
    }

    public void finish() {
        super.finish();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.b = (EditText) findViewById(R.id.resetpassword_key);
        this.c = (Button) findViewById(R.id.resetpassword_cancel);
        this.d = (Button) findViewById(R.id.resetpassword_ok);
        this.d.setOnClickListener(new aq(this));
        this.c.setOnClickListener(new ap(this));
        if (bundle != null) {
            this.b.setText(bundle.getString("EXTRA_KEY"));
        }
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        this.b.setText("");
        c cVar = (c) intent.getSerializableExtra("EXTRA_SERVICE_RESULT");
        b(false);
        if (cVar == null) {
            return;
        }
        if (cVar.b() != null) {
            if (cVar.b().c()) {
                b((int) R.string.error_service_account_general);
            } else if (cVar.b().b()) {
                b((int) R.string.error_service_account_general);
            } else if (cVar.b().d()) {
                b((int) R.string.error_password_reset_missing_email);
            } else {
                b((int) R.string.error_service_account_general);
            }
        } else if (cVar.a()) {
            setResult(-1);
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        bundle.putString("EXTRA_KEY", this.b.getText().toString());
        super.onSaveInstanceState(bundle);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }
}
