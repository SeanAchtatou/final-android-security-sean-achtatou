package com.agilebinary.mobilemonitor.client.android.ui;

import android.view.View;
import com.agilebinary.phonebeagle.client.R;

final class bc implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ AccountInfoActivity f243a;

    bc(AccountInfoActivity accountInfoActivity) {
        this.f243a = accountInfoActivity;
    }

    public final void onClick(View view) {
        ChangePasswordActivity.a(this.f243a, R.string.label_changepassword_reason_change, false);
    }
}
