package com.agilebinary.mobilemonitor.client.a.b;

import android.content.Context;
import com.agilebinary.mobilemonitor.client.a.a.c;
import com.agilebinary.mobilemonitor.client.a.e;

public abstract class s extends o {

    /* renamed from: a  reason: collision with root package name */
    private long f137a;

    public s(String str, long j, long j2, c cVar, e eVar) {
        super(str, j, j2, cVar);
        this.f137a = eVar.a(cVar.d());
    }

    public abstract String b(Context context);

    public abstract String c(Context context);

    public abstract Double f();
}
