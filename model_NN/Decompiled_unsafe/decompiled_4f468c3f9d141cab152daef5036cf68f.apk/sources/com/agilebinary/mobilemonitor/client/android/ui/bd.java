package com.agilebinary.mobilemonitor.client.android.ui;

import android.view.View;
import com.agilebinary.phonebeagle.client.R;

final class bd implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ AccountInfoActivity f244a;

    bd(AccountInfoActivity accountInfoActivity) {
        this.f244a = accountInfoActivity;
    }

    public final void onClick(View view) {
        ChangeEmailActivity.a(this.f244a, R.string.label_changeemail_text_normal, false);
    }
}
