package com.agilebinary.mobilemonitor.client.a.b;

import android.content.Context;
import com.agilebinary.mobilemonitor.client.a.a.c;
import com.agilebinary.mobilemonitor.client.a.e;
import com.agilebinary.phonebeagle.client.R;

public final class d extends i {

    /* renamed from: a  reason: collision with root package name */
    private long f128a;
    private int b;

    public d(String str, long j, long j2, c cVar, e eVar) {
        super(str, j, j2, cVar, eVar);
        this.f128a = eVar.a(cVar.d());
        this.b = cVar.c();
    }

    public final long a() {
        return this.f128a;
    }

    public final String a(Context context) {
        int i = 0;
        switch (this.b) {
            case 1:
                i = R.string.label_event_system_power_off;
                break;
            case 2:
                i = R.string.label_event_system_power_on;
                break;
            case 3:
                i = R.string.label_event_system_power_reboot;
                break;
            case 4:
                i = R.string.label_event_system_airplanemode_off;
                break;
            case 5:
                i = R.string.label_event_system_airplanemode_on;
                break;
            case 6:
                i = R.string.label_event_system_battery_low;
                break;
            case 7:
                i = R.string.label_event_system_battery_ok;
                break;
        }
        return context.getString(i);
    }

    public final byte k() {
        return 10;
    }
}
