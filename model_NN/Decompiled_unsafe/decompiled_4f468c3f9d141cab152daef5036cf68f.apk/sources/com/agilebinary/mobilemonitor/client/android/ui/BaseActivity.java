package com.agilebinary.mobilemonitor.client.android.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import com.agilebinary.mobilemonitor.client.android.MyApplication;
import com.agilebinary.mobilemonitor.client.android.a.t;
import com.agilebinary.mobilemonitor.client.android.c.b;
import com.agilebinary.phonebeagle.client.R;
import java.util.Stack;

public abstract class BaseActivity extends Activity {

    /* renamed from: a  reason: collision with root package name */
    private static final String f191a = b.a();
    private static Stack b = new Stack();
    private ProgressDialog c;
    private boolean d;
    private AlertDialog e;
    private int f;
    public MyApplication g;
    private int h;

    public static void k() {
        while (b.size() > 0) {
            "" + b.peek();
            ((BaseActivity) b.pop()).finish();
        }
    }

    /* access modifiers changed from: protected */
    public abstract int a();

    /* access modifiers changed from: protected */
    public void a(int i) {
        this.h = i;
        this.c.setMessage(getString(i));
        this.c.show();
    }

    /* access modifiers changed from: protected */
    public void b() {
    }

    /* access modifiers changed from: protected */
    public final void b(int i) {
        this.f = i;
        this.e.setMessage(getString(i));
        this.e.show();
    }

    /* access modifiers changed from: protected */
    public final void b(boolean z) {
        if (!this.c.isShowing()) {
            return;
        }
        if (z) {
            this.c.cancel();
        } else {
            this.c.dismiss();
        }
    }

    /* access modifiers changed from: protected */
    public void c() {
    }

    /* access modifiers changed from: protected */
    public CharSequence d() {
        return getString(R.string.app_title);
    }

    public void finish() {
        b(true);
        if (this.e.isShowing()) {
            this.e.dismiss();
        }
        super.finish();
    }

    /* access modifiers changed from: protected */
    public final void i() {
        this.e.dismiss();
    }

    /* access modifiers changed from: protected */
    public boolean j() {
        return requestWindowFeature(7);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        b.push(this);
        super.onCreate(bundle);
        this.g = (MyApplication) getApplication();
        this.d = j();
        setContentView(a());
        if (this.d) {
            getWindow().setFeatureInt(7, R.layout.titlebar);
            TextView textView = (TextView) findViewById(R.id.titlebar_text);
            if (textView != null) {
                textView.setText(d());
            }
        }
        this.c = new ProgressDialog(this);
        this.c.setIndeterminate(true);
        this.c.setCancelable(true);
        this.c.setTitle((int) R.string.label_dialog_title);
        this.c.setOnCancelListener(new d(this));
        this.c.setOnDismissListener(new f(this));
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true).setTitle((int) R.string.label_dialog_title).setNeutralButton((int) R.string.label_ok, new e(this));
        this.e = builder.create();
        if (bundle != null) {
            int i = bundle.getInt("EXTRA_PROGRESS_MSG");
            if (i != 0) {
                a(i);
                return;
            }
            int i2 = bundle.getInt("EXTRA_ERROR_MSG");
            if (i2 != 0) {
                b(i2);
            }
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.base, menu);
        MenuItem findItem = menu.findItem(R.id.menu_base_help);
        t.a();
        findItem.setVisible("http://www.phonebeagle.com/help_ac.html".trim().length() > 0);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        b.remove(this);
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.menu_base_help /*2131296408*/:
                c();
                t.a();
                try {
                    startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://www.phonebeagle.com/help_ac.html")));
                } catch (ActivityNotFoundException e2) {
                }
                return true;
            case R.id.menu_base_about /*2131296409*/:
                c();
                startActivity(new Intent(this, AboutActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        bundle.putInt("EXTRA_PROGRESS_MSG", this.c.isShowing() ? this.h : 0);
        bundle.putInt("EXTRA_ERROR_MSG", this.e.isShowing() ? this.f : 0);
        super.onSaveInstanceState(bundle);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        b(false);
        if (this.e.isShowing()) {
            this.e.dismiss();
        }
        super.onStop();
    }
}
