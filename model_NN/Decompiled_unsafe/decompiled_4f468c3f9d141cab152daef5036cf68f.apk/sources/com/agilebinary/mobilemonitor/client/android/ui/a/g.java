package com.agilebinary.mobilemonitor.client.android.ui.a;

import android.os.AsyncTask;
import com.agilebinary.mobilemonitor.client.android.MyApplication;
import com.agilebinary.mobilemonitor.client.android.c.b;
import com.agilebinary.mobilemonitor.client.android.ui.BaseActivity;
import com.agilebinary.mobilemonitor.client.android.ui.MainActivity;

public final class g extends AsyncTask implements com.agilebinary.mobilemonitor.client.android.a.g {

    /* renamed from: a  reason: collision with root package name */
    public static g f214a;
    private static String b = b.a();
    private final BaseActivity c;
    private final MyApplication d;
    private com.agilebinary.a.a.a.d.c.b e;

    public g(BaseActivity baseActivity) {
        this.c = baseActivity;
        this.d = baseActivity.g;
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x00f5 A[Catch:{ q -> 0x010e, all -> 0x0116 }, LOOP:0: B:3:0x000c->B:29:0x00f5, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0086 A[EDGE_INSN: B:48:0x0086->B:13:0x0086 ?: BREAK  , SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.Void b() {
        /*
            r13 = this;
            r11 = 0
            r10 = 0
            com.agilebinary.mobilemonitor.client.android.MyApplication r0 = r13.d
            com.agilebinary.mobilemonitor.client.android.b.j r6 = r0.e()
            byte[] r7 = com.agilebinary.mobilemonitor.client.android.b.j.f174a     // Catch:{ all -> 0x0116 }
            int r8 = r7.length     // Catch:{ all -> 0x0116 }
            r9 = r10
        L_0x000c:
            if (r9 >= r8) goto L_0x0086
            byte r2 = r7[r9]     // Catch:{ all -> 0x0116 }
            boolean r0 = r13.isCancelled()     // Catch:{ all -> 0x0116 }
            if (r0 != 0) goto L_0x0086
            long r0 = r6.b(r2)     // Catch:{ all -> 0x0116 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0116 }
            r3.<init>()     // Catch:{ all -> 0x0116 }
            java.lang.String r4 = "TimeOfLastssenEvent...for event type "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0116 }
            java.lang.StringBuilder r3 = r3.append(r2)     // Catch:{ all -> 0x0116 }
            java.lang.String r4 = " = "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0116 }
            java.lang.StringBuilder r3 = r3.append(r0)     // Catch:{ all -> 0x0116 }
            r3.toString()     // Catch:{ all -> 0x0116 }
            boolean r3 = r13.isCancelled()     // Catch:{ all -> 0x0116 }
            if (r3 != 0) goto L_0x0086
            r3 = 0
            int r3 = (r0 > r3 ? 1 : (r0 == r3 ? 0 : -1))
            if (r3 >= 0) goto L_0x0097
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ q -> 0x010e }
            r3.<init>()     // Catch:{ q -> 0x010e }
            java.lang.String r4 = ""
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ q -> 0x010e }
            java.lang.StringBuilder r3 = r3.append(r2)     // Catch:{ q -> 0x010e }
            r3.toString()     // Catch:{ q -> 0x010e }
            com.agilebinary.mobilemonitor.client.android.MyApplication r3 = r13.d     // Catch:{ q -> 0x010e }
            com.agilebinary.mobilemonitor.client.android.a.m r3 = r3.b()     // Catch:{ q -> 0x010e }
            com.agilebinary.mobilemonitor.client.android.a.a r3 = r3.f()     // Catch:{ q -> 0x010e }
            com.agilebinary.mobilemonitor.client.android.MyApplication r4 = r13.d     // Catch:{ q -> 0x010e }
            com.agilebinary.mobilemonitor.client.android.d r4 = r4.a()     // Catch:{ q -> 0x010e }
            long r0 = r3.a(r4, r2, r13)     // Catch:{ q -> 0x010e }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ q -> 0x010e }
            r3.<init>()     // Catch:{ q -> 0x010e }
            java.lang.String r4 = "Now, TimeOfLastssenEvent...for event type "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ q -> 0x010e }
            java.lang.StringBuilder r3 = r3.append(r2)     // Catch:{ q -> 0x010e }
            java.lang.String r4 = " = +ref"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ q -> 0x010e }
            r3.toString()     // Catch:{ q -> 0x010e }
            boolean r3 = r13.isCancelled()     // Catch:{ q -> 0x010e }
            if (r3 == 0) goto L_0x008e
        L_0x0086:
            com.agilebinary.mobilemonitor.client.android.MyApplication r0 = r13.d
            r0.f()
            com.agilebinary.mobilemonitor.client.android.ui.a.g.f214a = r11
            return r11
        L_0x008e:
            r6.b(r2, r0)     // Catch:{ q -> 0x010e }
            boolean r3 = r13.isCancelled()     // Catch:{ q -> 0x010e }
            if (r3 != 0) goto L_0x0086
        L_0x0097:
            r3 = r0
        L_0x0098:
            boolean r0 = r13.isCancelled()     // Catch:{ q -> 0x011f }
            if (r0 != 0) goto L_0x0086
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ q -> 0x011f }
            r0.<init>()     // Catch:{ q -> 0x011f }
            java.lang.String r1 = ""
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ q -> 0x011f }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ q -> 0x011f }
            r0.toString()     // Catch:{ q -> 0x011f }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ q -> 0x011f }
            r0.<init>()     // Catch:{ q -> 0x011f }
            java.lang.String r1 = " since "
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ q -> 0x011f }
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ q -> 0x011f }
            r0.toString()     // Catch:{ q -> 0x011f }
            com.agilebinary.mobilemonitor.client.android.MyApplication r0 = r13.d     // Catch:{ q -> 0x011f }
            com.agilebinary.mobilemonitor.client.android.a.m r0 = r0.b()     // Catch:{ q -> 0x011f }
            com.agilebinary.mobilemonitor.client.android.a.c r0 = r0.c()     // Catch:{ q -> 0x011f }
            com.agilebinary.mobilemonitor.client.android.MyApplication r1 = r13.d     // Catch:{ q -> 0x011f }
            com.agilebinary.mobilemonitor.client.android.d r1 = r1.a()     // Catch:{ q -> 0x011f }
            r5 = r13
            int r0 = r0.a(r1, r2, r3, r5)     // Catch:{ q -> 0x011f }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ q -> 0x0126 }
            r1.<init>()     // Catch:{ q -> 0x0126 }
            java.lang.String r3 = ""
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ q -> 0x0126 }
            java.lang.StringBuilder r1 = r1.append(r0)     // Catch:{ q -> 0x0126 }
            r1.toString()     // Catch:{ q -> 0x0126 }
            boolean r1 = r13.isCancelled()     // Catch:{ q -> 0x0126 }
            if (r1 != 0) goto L_0x0086
        L_0x00ef:
            boolean r1 = r13.isCancelled()     // Catch:{ all -> 0x0116 }
            if (r1 != 0) goto L_0x0086
            r1 = 2
            java.lang.Integer[] r1 = new java.lang.Integer[r1]     // Catch:{ all -> 0x0116 }
            r3 = 0
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ all -> 0x0116 }
            r1[r3] = r2     // Catch:{ all -> 0x0116 }
            r2 = 1
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ all -> 0x0116 }
            r1[r2] = r0     // Catch:{ all -> 0x0116 }
            r13.publishProgress(r1)     // Catch:{ all -> 0x0116 }
            int r0 = r9 + 1
            r9 = r0
            goto L_0x000c
        L_0x010e:
            r3 = move-exception
            r12 = r3
            r3 = r0
            r0 = r12
            r0.printStackTrace()     // Catch:{ all -> 0x0116 }
            goto L_0x0098
        L_0x0116:
            r0 = move-exception
            com.agilebinary.mobilemonitor.client.android.MyApplication r1 = r13.d
            r1.f()
            com.agilebinary.mobilemonitor.client.android.ui.a.g.f214a = r11
            throw r0
        L_0x011f:
            r0 = move-exception
            r1 = r10
        L_0x0121:
            r0.printStackTrace()     // Catch:{ all -> 0x0116 }
            r0 = r1
            goto L_0x00ef
        L_0x0126:
            r1 = move-exception
            r12 = r1
            r1 = r0
            r0 = r12
            goto L_0x0121
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.client.android.ui.a.g.b():java.lang.Void");
    }

    public final void a() {
        cancel(false);
        if (this.e != null) {
            try {
                this.e.d();
            } catch (Exception e2) {
            }
        }
    }

    public final void a(com.agilebinary.a.a.a.d.c.b bVar) {
        this.e = bVar;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object doInBackground(Object[] objArr) {
        return b();
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onProgressUpdate(Object[] objArr) {
        Integer[] numArr = (Integer[]) objArr;
        MainActivity.a(this.c, (byte) numArr[0].intValue(), numArr[1].intValue());
    }
}
