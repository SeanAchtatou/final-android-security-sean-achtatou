package com.agilebinary.mobilemonitor.client.a.b;

import android.content.Context;
import com.agilebinary.mobilemonitor.client.a.c;
import com.agilebinary.mobilemonitor.client.a.e;
import com.agilebinary.mobilemonitor.client.android.a.a.b;
import com.agilebinary.phonebeagle.client.R;

public final class h extends n implements c {
    private int c = -1;
    private int d = -1;
    private int e = -1;
    private int f = -1;

    public h(String str, long j, long j2, com.agilebinary.mobilemonitor.client.a.a.c cVar, e eVar) {
        super(str, j, j2, cVar, eVar);
        this.c = cVar.c();
        this.d = cVar.c();
        this.e = cVar.c();
        this.f = cVar.c();
    }

    public final void a(b bVar) {
        this.b = bVar;
    }

    public final boolean a() {
        return this.b == null;
    }

    public final int b() {
        return this.c;
    }

    public final String b(Context context) {
        return context.getString(R.string.label_event_location_type_cell_gsm);
    }

    public final int c() {
        return this.d;
    }

    public final String c(Context context) {
        return this.f134a;
    }

    public final int d() {
        return this.e;
    }

    public final int e() {
        return this.f;
    }

    public final byte k() {
        return 7;
    }
}
