package com.agilebinary.mobilemonitor.client.a.b;

import com.agilebinary.mobilemonitor.client.a.a.c;
import com.agilebinary.mobilemonitor.client.a.e;

public final class u extends i {

    /* renamed from: a  reason: collision with root package name */
    private long f139a;
    private long b;
    private long c;
    private byte d;
    private String e;
    private String f;

    public u(String str, long j, long j2, c cVar, e eVar) {
        super(str, j, j2, cVar, eVar);
        this.f139a = eVar.a(cVar.d());
        this.b = eVar.a(cVar.d());
        this.c = eVar.a(cVar.d());
        this.d = cVar.b();
        this.e = cVar.g();
        this.f = cVar.g();
    }

    public final long a() {
        return this.f139a;
    }

    public final long b() {
        return this.b;
    }

    public final long c() {
        return this.c;
    }

    public final byte d() {
        return this.d;
    }

    public final String e() {
        return this.e;
    }

    public final String f() {
        return this.f;
    }

    public final byte k() {
        return 1;
    }
}
