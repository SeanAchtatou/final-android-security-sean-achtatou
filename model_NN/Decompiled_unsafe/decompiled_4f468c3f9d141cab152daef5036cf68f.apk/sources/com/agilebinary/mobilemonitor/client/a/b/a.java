package com.agilebinary.mobilemonitor.client.a.b;

import android.content.Context;
import com.agilebinary.mobilemonitor.client.a.a.c;
import com.agilebinary.mobilemonitor.client.a.d;
import com.agilebinary.mobilemonitor.client.a.e;
import org.osmdroid.util.GeoPoint;

public abstract class a extends s implements d {

    /* renamed from: a  reason: collision with root package name */
    private double f125a;
    private double b;
    private double c;
    private long d;
    private boolean e;
    private boolean f;
    private String g;
    private boolean h;
    private double i;
    private boolean j;
    private double k;

    public a(String str, long j2, long j3, c cVar, e eVar) {
        super(str, j2, j3, cVar, eVar);
        this.f125a = cVar.e();
        this.b = cVar.e();
        this.c = cVar.e();
        this.d = eVar.a(cVar.d());
        this.e = cVar.a();
        this.f = cVar.a();
        this.g = cVar.g();
        this.h = cVar.a();
        if (this.h) {
            this.i = cVar.e();
        }
        this.j = cVar.a();
        if (this.j) {
            this.k = cVar.e();
        }
    }

    public final double a() {
        return this.f125a;
    }

    public final CharSequence a(Context context) {
        return com.agilebinary.mobilemonitor.client.android.c.a.a(context, Double.valueOf(this.i));
    }

    public final double b() {
        return this.b;
    }

    public final long c() {
        return this.d;
    }

    public final boolean d() {
        return this.e;
    }

    public final boolean e() {
        return this.f;
    }

    public final Double f() {
        return Double.valueOf(this.c);
    }

    public final double g() {
        return this.c;
    }

    public final GeoPoint h() {
        return new GeoPoint(this.f125a, this.b);
    }

    public final com.google.android.maps.GeoPoint i() {
        GeoPoint h2 = h();
        if (h2 == null) {
            return null;
        }
        return new com.google.android.maps.GeoPoint(h2.a(), h2.b());
    }

    public final boolean j() {
        return true;
    }
}
