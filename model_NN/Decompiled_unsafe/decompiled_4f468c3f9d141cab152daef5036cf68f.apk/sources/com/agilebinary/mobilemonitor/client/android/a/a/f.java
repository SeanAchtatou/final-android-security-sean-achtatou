package com.agilebinary.mobilemonitor.client.android.a.a;

import java.io.Serializable;
import java.util.List;

public final class f implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private String f148a;
    private String b;
    private String c;
    private boolean d;
    private boolean e;
    private long f;
    private String g;
    private String h;
    private String i;
    private long j;
    private long k;
    private String l;
    private String m;
    private String n;
    private String o;
    private String p;
    private String q;
    private boolean r;
    private boolean s;
    private boolean t;
    private boolean u;
    private boolean v;
    private boolean w;
    private List x;

    public f() {
    }

    public f(String str, String str2, long j2, String str3, String str4, String str5, String str6, boolean z, long j3, boolean z2, long j4, String str7, String str8, String str9, String str10, String str11) {
        this(str, str2, j2, str3, str4, str5, str6, z, j3, z2, j4, str7, str8, str9, str10, str11, (byte) 0);
    }

    private f(String str, String str2, long j2, String str3, String str4, String str5, String str6, boolean z, long j3, boolean z2, long j4, String str7, String str8, String str9, String str10, String str11, byte b2) {
        this.f148a = str;
        this.b = str2;
        this.f = j2;
        this.g = str3;
        this.h = str4;
        this.i = str5;
        this.c = str6;
        this.d = z;
        this.j = j3;
        this.e = z2;
        this.k = j4;
        this.l = str7;
        this.m = str8;
        this.n = str9;
        this.o = str10;
        this.o = str10;
        this.p = str11;
        this.x = null;
        this.r = true;
        this.s = true;
        this.t = true;
        this.u = true;
        this.v = true;
        this.w = true;
    }

    public final String a() {
        return this.f148a;
    }

    public final void a(long j2) {
        this.f = j2;
    }

    public final void a(String str) {
        this.f148a = str;
    }

    public final void a(boolean z) {
        this.d = z;
    }

    public final String b() {
        return this.b;
    }

    public final void b(long j2) {
        this.j = j2;
    }

    public final void b(String str) {
        this.b = str;
    }

    public final void b(boolean z) {
        this.e = z;
    }

    public final String c() {
        return this.g;
    }

    public final void c(long j2) {
        this.k = j2;
    }

    public final void c(String str) {
        this.g = str;
    }

    public final void c(boolean z) {
        this.r = z;
    }

    public final String d() {
        return this.h;
    }

    public final void d(String str) {
        this.h = str;
    }

    public final void d(boolean z) {
        this.s = z;
    }

    public final void e(String str) {
        this.i = str;
    }

    public final void e(boolean z) {
        this.t = z;
    }

    public final boolean e() {
        return this.e;
    }

    public final String f() {
        return this.l;
    }

    public final void f(String str) {
        this.c = str;
    }

    public final void f(boolean z) {
        this.u = z;
    }

    public final String g() {
        return this.m;
    }

    public final void g(String str) {
        this.l = str;
    }

    public final void g(boolean z) {
        this.v = z;
    }

    public final String h() {
        return this.n;
    }

    public final void h(String str) {
        this.m = str;
    }

    public final void h(boolean z) {
        this.w = z;
    }

    public final String i() {
        return this.o;
    }

    public final void i(String str) {
        this.n = str;
    }

    public final void j(String str) {
        this.o = str;
    }

    public final boolean j() {
        return this.r;
    }

    public final void k(String str) {
        this.p = str;
    }

    public final boolean k() {
        return this.s;
    }

    public final void l(String str) {
        this.q = str;
    }

    public final boolean l() {
        return this.t;
    }

    public final boolean m() {
        return this.u;
    }

    public final boolean n() {
        return this.v;
    }

    public final boolean o() {
        return this.w;
    }
}
