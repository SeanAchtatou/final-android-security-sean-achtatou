package com.agilebinary.mobilemonitor.client.a.b;

import android.graphics.Bitmap;
import com.agilebinary.mobilemonitor.client.a.a;
import com.agilebinary.mobilemonitor.client.a.a.c;
import com.agilebinary.mobilemonitor.client.a.b;
import java.io.ByteArrayOutputStream;
import java.io.Serializable;

public final class t implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private String f138a;
    private String b;
    private String c;
    private byte[] d;
    private int e = -1;
    private String f;
    private boolean g;
    private /* synthetic */ f h;

    public t(f fVar, c cVar, b bVar, ByteArrayOutputStream byteArrayOutputStream) {
        this.h = fVar;
        this.f138a = cVar.g();
        this.b = cVar.g();
        this.c = cVar.g();
        this.g = cVar.a();
        if (this.g) {
            this.f = cVar.g();
            return;
        }
        this.e = cVar.c();
        if (bVar.a(this.f138a, this.e) == a.BITMAP) {
            byte[] bArr = new byte[this.e];
            "readfully " + this.e + " bytes...";
            cVar.a(bArr);
            Bitmap[] a2 = f.b(bArr);
            byteArrayOutputStream.reset();
            a2[1].compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream);
            this.d = byteArrayOutputStream.toByteArray();
            if (fVar.o == null) {
                byteArrayOutputStream.reset();
                a2[0].compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                byte[] unused = fVar.o = byteArrayOutputStream.toByteArray();
                return;
            }
            return;
        }
        "skipping " + this.e + " bytes...";
        f.a(cVar, this.e);
    }

    public final String a() {
        return this.f138a;
    }

    public final String b() {
        return this.c;
    }

    public final byte[] c() {
        return this.d;
    }

    public final boolean d() {
        return this.g;
    }

    public final int e() {
        if (!this.g) {
            return this.e;
        }
        if (this.f == null) {
            return 0;
        }
        return this.f.length();
    }

    public final String f() {
        return this.f;
    }
}
