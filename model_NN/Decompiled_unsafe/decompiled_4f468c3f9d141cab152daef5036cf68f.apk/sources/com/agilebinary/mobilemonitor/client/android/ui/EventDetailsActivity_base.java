package com.agilebinary.mobilemonitor.client.android.ui;

import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import com.agilebinary.mobilemonitor.client.a.b.o;
import com.agilebinary.mobilemonitor.client.android.c.a;
import com.agilebinary.mobilemonitor.client.android.c.c;
import com.agilebinary.phonebeagle.client.R;

public abstract class EventDetailsActivity_base extends BaseLoggedInActivity {

    /* renamed from: a  reason: collision with root package name */
    private TextView f200a;
    private TextView b;
    private ImageButton c;
    private ImageButton d;
    private ImageButton e;
    private ImageButton f;
    private o h;
    private TextView i;
    private TextView k;
    private long l;
    private byte m;

    /* access modifiers changed from: protected */
    public final int a() {
        return R.layout.eventdetails_base;
    }

    /* access modifiers changed from: protected */
    public abstract void a(ViewGroup viewGroup);

    /* access modifiers changed from: protected */
    public void a(o oVar) {
        this.h = oVar;
        this.f200a.setText(oVar.r());
        this.b.setText(c.a().c(oVar.t()));
        this.d.setEnabled(this.j.e(this.m, oVar.u()));
        this.c.setEnabled(this.j.c(this.m, oVar.u()));
        this.e.setEnabled(this.c.isEnabled());
        this.f.setEnabled(this.d.isEnabled());
        this.k.setText(c.a().d(oVar.u()));
        this.i.setText(a.a(this, this.h.s()));
    }

    /* access modifiers changed from: protected */
    public final void e() {
        o f2 = this.j.f(this.m, this.h.u());
        if (f2 != null) {
            a(f2);
        }
    }

    /* access modifiers changed from: protected */
    public final void f() {
        o d2 = this.j.d(this.m, this.h.u());
        if (d2 != null) {
            a(d2);
        }
    }

    /* access modifiers changed from: protected */
    public final void g() {
        o f2 = this.j.f(this.m);
        if (f2 != null) {
            a(f2);
        }
    }

    /* access modifiers changed from: protected */
    public final void h() {
        o g = this.j.g(this.m);
        if (g != null) {
            a(g);
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        a((ViewGroup) findViewById(R.id.eventdetails_typespecific));
        this.l = 0;
        this.m = -1;
        if (bundle != null) {
            this.l = bundle.getLong("EXTRA_EVENT_ID");
            this.m = bundle.getByte("EXTRA_EVENT_TYPE");
        } else {
            this.l = getIntent().getLongExtra("EXTRA_EVENT_ID", 0);
            this.m = getIntent().getByteExtra("EXTRA_EVENT_TYPE", (byte) -1);
        }
        this.f200a = (TextView) findViewById(R.id.eventdetails_common_phone_identifier);
        this.b = (TextView) findViewById(R.id.eventdetails_common_servertime);
        this.k = (TextView) findViewById(R.id.eventdetails_navigation_text);
        this.i = (TextView) findViewById(R.id.eventdetails_common_country);
        this.c = (ImageButton) findViewById(R.id.eventdetails_navigation_prev);
        this.d = (ImageButton) findViewById(R.id.eventdetails_navigation_next);
        this.e = (ImageButton) findViewById(R.id.eventdetails_navigation_first);
        this.f = (ImageButton) findViewById(R.id.eventdetails_navigation_last);
        this.c.setEnabled(false);
        this.d.setEnabled(false);
        this.e.setEnabled(false);
        this.f.setEnabled(false);
        this.c.setOnClickListener(new an(this));
        this.d.setOnClickListener(new ao(this));
        this.e.setOnClickListener(new al(this));
        this.f.setOnClickListener(new am(this));
        a(this.j.g(this.m, this.l));
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        bundle.putLong("EXTRA_EVENT_ID", this.h.u());
        bundle.putByte("EXTRA_EVENT_TYPE", this.m);
        super.onSaveInstanceState(bundle);
    }
}
