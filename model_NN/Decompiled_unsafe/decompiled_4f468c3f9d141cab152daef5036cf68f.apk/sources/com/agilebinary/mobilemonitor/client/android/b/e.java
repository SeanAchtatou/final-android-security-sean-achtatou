package com.agilebinary.mobilemonitor.client.android.b;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

final class e extends SQLiteOpenHelper {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ j f170a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public e(j jVar, Context context, String str) {
        super(context, str, (SQLiteDatabase.CursorFactory) null, 10);
        this.f170a = jVar;
    }

    public final void onCreate(SQLiteDatabase sQLiteDatabase) {
        String format = String.format("CREATE TABLE %1$s (%2$s INTEGER PRIMAY KEY, %3$s INTEGER, %4$s BLOB, %5$s STRING, %6$s STRING, %7$s DOUBLE,%8$s DOUBLE, %9$s DOUBLE, %10$s INTEGER, %11$s INTEGER, %12$s INTEGER )", "location", f.b, f.c, f.d, "line1", "line2", "accuracy", "lat", "lon", "contenttype", "powersave", "valloc");
        String.format("Executing SQL: %1$s", format);
        sQLiteDatabase.execSQL(format);
        String format2 = String.format("CREATE TABLE %1$s (%2$s INTEGER PRIMAY KEY, %3$s INTEGER, %4$s BLOB, %5$s STRING, %6$s STRING )", "call", c.b, c.c, c.d, "line1", "line2");
        String.format("Executing SQL: %1$s", format2);
        sQLiteDatabase.execSQL(format2);
        String format3 = String.format("CREATE TABLE %1$s (%2$s INTEGER PRIMAY KEY, %3$s INTEGER, %4$s BLOB, %5$s STRING, %6$s STRING )", "sms", a.b, a.c, a.d, "dir", "text");
        String.format("Executing SQL: %1$s", format3);
        sQLiteDatabase.execSQL(format3);
        String format4 = String.format("CREATE TABLE %1$s (%2$s INTEGER PRIMAY KEY, %3$s INTEGER, %4$s BLOB, %5$s STRING, %6$s STRING, %7$s BLOB )", "mms", d.b, d.c, d.d, "dir", "text", "thumbnail");
        String.format("Executing SQL: %1$s", format4);
        sQLiteDatabase.execSQL(format4);
        String format5 = String.format("CREATE TABLE %1$s (%2$s INTEGER PRIMAY KEY, %3$s INTEGER, %4$s BLOB, %5$s STRING, %6$s STRING )", "web", k.b, k.c, k.d, "line1", "line2");
        String.format("Executing SQL: %1$s", format5);
        sQLiteDatabase.execSQL(format5);
        String format6 = String.format("CREATE TABLE %1$s (%2$s INTEGER PRIMAY KEY, %3$s INTEGER, %4$s BLOB, %5$s STRING)", "syst_m", l.b, l.c, l.d, "line1");
        String.format("Executing SQL: %1$s", format6);
        sQLiteDatabase.execSQL(format6);
        String format7 = String.format("CREATE TABLE %1$s (%2$s INTEGER PRIMAY KEY, %3$s INTEGER, %4$s BLOB, %5$s STRING, %6$s STRING )", i.f173a, i.b, i.c, i.d, i.f, i.g);
        String.format("Executing SQL: %1$s", format7);
        sQLiteDatabase.execSQL(format7);
        String format8 = String.format("CREATE TABLE %1$s (%2$s INTEGER PRIMAY KEY , %3$s INTEGER,  UNIQUE (%2$s) ON CONFLICT REPLACE ) ", "curday", "eventtype", "day");
        String.format("Executing SQL: %1$s", format8);
        sQLiteDatabase.execSQL(format8);
        String format9 = String.format("CREATE TABLE %1$s (%2$s INTEGER PRIMAY KEY , %3$s INTEGER,  UNIQUE (%2$s) ON CONFLICT REPLACE ) ", "oldestevent", "eventtype", "oldest");
        String.format("Executing SQL: %1$s", format9);
        sQLiteDatabase.execSQL(format9);
        String format10 = String.format("CREATE TABLE %1$s (%2$s INTEGER PRIMAY KEY , %3$s INTEGER,  UNIQUE (%2$s) ON CONFLICT REPLACE ) ", "lastseen", "eventtype", "lastid");
        String.format("Executing SQL: %1$s", format10);
        sQLiteDatabase.execSQL(format10);
    }

    public final void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        String.format("Upgrading database from version %1$s to %2$s, destroying all existing data", Integer.valueOf(i), Integer.valueOf(i2));
        sQLiteDatabase.execSQL(String.format("DROP TABLE IF EXISTS %1$s", "location"));
        sQLiteDatabase.execSQL(String.format("DROP TABLE IF EXISTS %1$s", "call"));
        sQLiteDatabase.execSQL(String.format("DROP TABLE IF EXISTS %1$s", "sms"));
        sQLiteDatabase.execSQL(String.format("DROP TABLE IF EXISTS %1$s", "mms"));
        sQLiteDatabase.execSQL(String.format("DROP TABLE IF EXISTS %1$s", "web"));
        sQLiteDatabase.execSQL(String.format("DROP TABLE IF EXISTS %1$s", "syst_m"));
        sQLiteDatabase.execSQL(String.format("DROP TABLE IF EXISTS %1$s", i.f173a));
        sQLiteDatabase.execSQL(String.format("DROP TABLE IF EXISTS %1$s", "curday"));
        sQLiteDatabase.execSQL(String.format("DROP TABLE IF EXISTS %1$s", "oldestevent"));
        sQLiteDatabase.execSQL(String.format("DROP TABLE IF EXISTS %1$s", "lastseen"));
        onCreate(sQLiteDatabase);
    }
}
