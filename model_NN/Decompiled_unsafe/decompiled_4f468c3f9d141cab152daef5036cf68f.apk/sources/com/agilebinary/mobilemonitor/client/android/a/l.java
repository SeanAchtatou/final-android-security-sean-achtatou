package com.agilebinary.mobilemonitor.client.android.a;

import com.agilebinary.a.a.a.c.d.i;
import com.agilebinary.a.a.a.e.e;
import com.agilebinary.a.a.a.h.b.g;
import com.agilebinary.a.a.a.h.b.h;
import com.agilebinary.a.a.a.j;
import com.agilebinary.mobilemonitor.a.a.a.a;
import com.agilebinary.mobilemonitor.client.android.c.b;
import java.util.Timer;
import java.util.concurrent.TimeUnit;

public final class l {

    /* renamed from: a  reason: collision with root package name */
    private static final String f159a = b.a();
    private static l b;
    private Timer c = new Timer("HttpTimeoutTimer");
    private i d;

    private l(e eVar) {
        com.agilebinary.a.a.a.c.b.a.b bVar = new com.agilebinary.a.a.a.c.b.a.b(new h(), TimeUnit.SECONDS);
        bVar.c();
        bVar.b();
        this.d = new i(bVar, eVar);
        try {
            t.a();
            b bVar2 = new b((byte) 0);
            this.d.r().a().a(new g("http", com.agilebinary.a.a.a.h.b.e.b(), 80));
            this.d.r().a().a(new g("https", bVar2, 443));
        } catch (Exception e) {
            a.e(e);
        }
        this.d.a(new i(this));
        this.d.a(new h(this));
    }

    public static l a() {
        if (b == null) {
            com.agilebinary.a.a.a.e.a aVar = new com.agilebinary.a.a.a.e.a();
            aVar.b("http.connection.timeout", 20000);
            aVar.b("http.socket.timeout", 20000);
            aVar.b("http.socket.linger", 20);
            t.a();
            aVar.a("http.useragent", "PhoneBeagle Android Client" + "1.22");
            b = new l(aVar);
        }
        return b;
    }

    public final j a(com.agilebinary.a.a.a.d.c.b bVar) {
        return this.d.a(bVar);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
        r1.d();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x003e, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
        r1.d();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x004d, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x004e, code lost:
        com.agilebinary.mobilemonitor.a.a.a.a.e(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0056, code lost:
        throw new com.agilebinary.mobilemonitor.client.android.a.q(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0018, code lost:
        r0 = e;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x001e A[SYNTHETIC, Splitter:B:12:0x001e] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0044 A[SYNTHETIC, Splitter:B:28:0x0044] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x004d A[Catch:{ InterruptedIOException -> 0x0061, IOException -> 0x005c, n -> 0x004d }, ExcHandler: n (r0v3 'e' com.agilebinary.a.a.a.c.b.n A[CUSTOM_DECLARE, Catch:{  }]), Splitter:B:1:0x0001] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:14:0x0021=Splitter:B:14:0x0021, B:30:0x0047=Splitter:B:30:0x0047, B:9:0x0019=Splitter:B:9:0x0019, B:25:0x003f=Splitter:B:25:0x003f} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.agilebinary.mobilemonitor.client.android.a.s a(java.lang.String r6, com.agilebinary.mobilemonitor.client.android.a.g r7) {
        /*
            r5 = this;
            r0 = 0
            com.agilebinary.a.a.a.d.c.a r1 = new com.agilebinary.a.a.a.d.c.a     // Catch:{ InterruptedIOException -> 0x0061, IOException -> 0x005c, n -> 0x004d }
            r1.<init>(r6)     // Catch:{ InterruptedIOException -> 0x0061, IOException -> 0x005c, n -> 0x004d }
            r7.a(r1)     // Catch:{ InterruptedIOException -> 0x0018, IOException -> 0x003e, n -> 0x004d }
            r2 = 300000(0x493e0, double:1.482197E-318)
            com.agilebinary.a.a.a.j r0 = com.agilebinary.mobilemonitor.client.android.a.n.a(r5, r1, r2)     // Catch:{ InterruptedIOException -> 0x0018, IOException -> 0x003e, n -> 0x004d }
            if (r0 != 0) goto L_0x0029
            com.agilebinary.mobilemonitor.client.android.a.q r0 = new com.agilebinary.mobilemonitor.client.android.a.q     // Catch:{ InterruptedIOException -> 0x0018, IOException -> 0x003e, n -> 0x004d }
            r0.<init>()     // Catch:{ InterruptedIOException -> 0x0018, IOException -> 0x003e, n -> 0x004d }
            throw r0     // Catch:{ InterruptedIOException -> 0x0018, IOException -> 0x003e, n -> 0x004d }
        L_0x0018:
            r0 = move-exception
        L_0x0019:
            com.agilebinary.mobilemonitor.a.a.a.a.e(r0)     // Catch:{ all -> 0x0027 }
            if (r1 == 0) goto L_0x0021
            r1.d()     // Catch:{ Exception -> 0x0057 }
        L_0x0021:
            com.agilebinary.mobilemonitor.client.android.a.q r0 = new com.agilebinary.mobilemonitor.client.android.a.q     // Catch:{ all -> 0x0027 }
            r0.<init>()     // Catch:{ all -> 0x0027 }
            throw r0     // Catch:{ all -> 0x0027 }
        L_0x0027:
            r0 = move-exception
            throw r0
        L_0x0029:
            com.agilebinary.mobilemonitor.client.android.a.s r2 = new com.agilebinary.mobilemonitor.client.android.a.s     // Catch:{ InterruptedIOException -> 0x0018, IOException -> 0x003e, n -> 0x004d }
            r2.<init>(r0)     // Catch:{ InterruptedIOException -> 0x0018, IOException -> 0x003e, n -> 0x004d }
            boolean r0 = r2.c()     // Catch:{ InterruptedIOException -> 0x0018, IOException -> 0x003e, n -> 0x004d }
            if (r0 == 0) goto L_0x005b
            com.agilebinary.mobilemonitor.client.android.a.q r0 = new com.agilebinary.mobilemonitor.client.android.a.q     // Catch:{ InterruptedIOException -> 0x0018, IOException -> 0x003e, n -> 0x004d }
            int r2 = r2.b()     // Catch:{ InterruptedIOException -> 0x0018, IOException -> 0x003e, n -> 0x004d }
            r0.<init>(r2)     // Catch:{ InterruptedIOException -> 0x0018, IOException -> 0x003e, n -> 0x004d }
            throw r0     // Catch:{ InterruptedIOException -> 0x0018, IOException -> 0x003e, n -> 0x004d }
        L_0x003e:
            r0 = move-exception
        L_0x003f:
            com.agilebinary.mobilemonitor.a.a.a.a.e(r0)     // Catch:{ all -> 0x0027 }
            if (r1 == 0) goto L_0x0047
            r1.d()     // Catch:{ Exception -> 0x0059 }
        L_0x0047:
            com.agilebinary.mobilemonitor.client.android.a.q r1 = new com.agilebinary.mobilemonitor.client.android.a.q     // Catch:{ all -> 0x0027 }
            r1.<init>(r0)     // Catch:{ all -> 0x0027 }
            throw r1     // Catch:{ all -> 0x0027 }
        L_0x004d:
            r0 = move-exception
            com.agilebinary.mobilemonitor.a.a.a.a.e(r0)     // Catch:{ all -> 0x0027 }
            com.agilebinary.mobilemonitor.client.android.a.q r1 = new com.agilebinary.mobilemonitor.client.android.a.q     // Catch:{ all -> 0x0027 }
            r1.<init>(r0)     // Catch:{ all -> 0x0027 }
            throw r1     // Catch:{ all -> 0x0027 }
        L_0x0057:
            r0 = move-exception
            goto L_0x0021
        L_0x0059:
            r1 = move-exception
            goto L_0x0047
        L_0x005b:
            return r2
        L_0x005c:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x003f
        L_0x0061:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x0019
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.client.android.a.l.a(java.lang.String, com.agilebinary.mobilemonitor.client.android.a.g):com.agilebinary.mobilemonitor.client.android.a.s");
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:14:0x0028=Splitter:B:14:0x0028, B:29:0x004c=Splitter:B:29:0x004c} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.agilebinary.mobilemonitor.client.android.a.s a(java.lang.String r4, byte[] r5, com.agilebinary.mobilemonitor.client.android.a.g r6) {
        /*
            r3 = this;
            com.agilebinary.a.a.a.d.c.c r0 = new com.agilebinary.a.a.a.d.c.c
            r0.<init>(r4)
            r6.a(r0)
            if (r5 == 0) goto L_0x0012
            com.agilebinary.a.a.a.g.d r1 = new com.agilebinary.a.a.a.g.d
            r1.<init>(r5)
            r0.a(r1)
        L_0x0012:
            r1 = 300000(0x493e0, double:1.482197E-318)
            com.agilebinary.a.a.a.j r1 = com.agilebinary.mobilemonitor.client.android.a.n.a(r3, r0, r1)     // Catch:{ InterruptedIOException -> 0x0021, IOException -> 0x0045, n -> 0x0052 }
            if (r1 != 0) goto L_0x0030
            com.agilebinary.mobilemonitor.client.android.a.q r1 = new com.agilebinary.mobilemonitor.client.android.a.q     // Catch:{ InterruptedIOException -> 0x0021, IOException -> 0x0045, n -> 0x0052 }
            r1.<init>()     // Catch:{ InterruptedIOException -> 0x0021, IOException -> 0x0045, n -> 0x0052 }
            throw r1     // Catch:{ InterruptedIOException -> 0x0021, IOException -> 0x0045, n -> 0x0052 }
        L_0x0021:
            r1 = move-exception
            com.agilebinary.mobilemonitor.a.a.a.a.e(r1)     // Catch:{ all -> 0x002e }
            r0.d()     // Catch:{ Exception -> 0x005c }
        L_0x0028:
            com.agilebinary.mobilemonitor.client.android.a.q r0 = new com.agilebinary.mobilemonitor.client.android.a.q     // Catch:{ all -> 0x002e }
            r0.<init>()     // Catch:{ all -> 0x002e }
            throw r0     // Catch:{ all -> 0x002e }
        L_0x002e:
            r0 = move-exception
            throw r0
        L_0x0030:
            com.agilebinary.mobilemonitor.client.android.a.s r2 = new com.agilebinary.mobilemonitor.client.android.a.s     // Catch:{ InterruptedIOException -> 0x0021, IOException -> 0x0045, n -> 0x0052 }
            r2.<init>(r1)     // Catch:{ InterruptedIOException -> 0x0021, IOException -> 0x0045, n -> 0x0052 }
            boolean r1 = r2.c()     // Catch:{ InterruptedIOException -> 0x0021, IOException -> 0x0045, n -> 0x0052 }
            if (r1 == 0) goto L_0x0060
            com.agilebinary.mobilemonitor.client.android.a.q r1 = new com.agilebinary.mobilemonitor.client.android.a.q     // Catch:{ InterruptedIOException -> 0x0021, IOException -> 0x0045, n -> 0x0052 }
            int r2 = r2.b()     // Catch:{ InterruptedIOException -> 0x0021, IOException -> 0x0045, n -> 0x0052 }
            r1.<init>(r2)     // Catch:{ InterruptedIOException -> 0x0021, IOException -> 0x0045, n -> 0x0052 }
            throw r1     // Catch:{ InterruptedIOException -> 0x0021, IOException -> 0x0045, n -> 0x0052 }
        L_0x0045:
            r1 = move-exception
            com.agilebinary.mobilemonitor.a.a.a.a.e(r1)     // Catch:{ all -> 0x002e }
            r0.d()     // Catch:{ Exception -> 0x005e }
        L_0x004c:
            com.agilebinary.mobilemonitor.client.android.a.q r0 = new com.agilebinary.mobilemonitor.client.android.a.q     // Catch:{ all -> 0x002e }
            r0.<init>(r1)     // Catch:{ all -> 0x002e }
            throw r0     // Catch:{ all -> 0x002e }
        L_0x0052:
            r0 = move-exception
            com.agilebinary.mobilemonitor.a.a.a.a.e(r0)     // Catch:{ all -> 0x002e }
            com.agilebinary.mobilemonitor.client.android.a.q r1 = new com.agilebinary.mobilemonitor.client.android.a.q     // Catch:{ all -> 0x002e }
            r1.<init>(r0)     // Catch:{ all -> 0x002e }
            throw r1     // Catch:{ all -> 0x002e }
        L_0x005c:
            r0 = move-exception
            goto L_0x0028
        L_0x005e:
            r0 = move-exception
            goto L_0x004c
        L_0x0060:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.client.android.a.l.a(java.lang.String, byte[], com.agilebinary.mobilemonitor.client.android.a.g):com.agilebinary.mobilemonitor.client.android.a.s");
    }

    public final void b() {
        this.d.r().a(1000, TimeUnit.MILLISECONDS);
    }

    public final Timer c() {
        return this.c;
    }
}
