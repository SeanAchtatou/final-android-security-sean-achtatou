package com.agilebinary.mobilemonitor.client.android.ui.map;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.agilebinary.mobilemonitor.client.a.b.s;
import com.agilebinary.phonebeagle.client.R;
import java.util.List;

public final class d extends Dialog implements DialogInterface.OnCancelListener, AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    private a f280a;
    private ListView b = ((ListView) findViewById(R.id.map_marker_select_dialog_list));
    private k c = new k(getContext());

    public d(Context context, a aVar) {
        super(context);
        this.f280a = aVar;
        setTitle((int) R.string.label_map_markerselectdialog_title);
        setOnCancelListener(this);
        setContentView((int) R.layout.map_marker_select_dialog);
        this.b.setAdapter((ListAdapter) this.c);
        this.b.setOnItemClickListener(this);
    }

    public final void a(List list) {
        this.c.a(list);
    }

    public final void onCancel(DialogInterface dialogInterface) {
        dismiss();
        this.f280a.a(null);
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        dismiss();
        this.f280a.a((s) adapterView.getItemAtPosition(i));
    }
}
