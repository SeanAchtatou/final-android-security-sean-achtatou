package com.agilebinary.mobilemonitor.client.android.ui;

import android.view.View;
import com.agilebinary.phonebeagle.client.R;
import java.util.regex.Pattern;

final class ai implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ChangeEmailActivity f226a;

    ai(ChangeEmailActivity changeEmailActivity) {
        this.f226a = changeEmailActivity;
    }

    public final void onClick(View view) {
        String obj = this.f226a.c.getText().toString();
        if (!obj.equals(this.f226a.d.getText().toString())) {
            this.f226a.b((int) R.string.error_email_match);
        } else if (!Pattern.compile(".+@.+\\.[a-z]+").matcher(obj).matches()) {
            this.f226a.b((int) R.string.error_email_invalid);
        } else {
            ChangeEmailActivity.a(this.f226a, obj);
        }
    }
}
