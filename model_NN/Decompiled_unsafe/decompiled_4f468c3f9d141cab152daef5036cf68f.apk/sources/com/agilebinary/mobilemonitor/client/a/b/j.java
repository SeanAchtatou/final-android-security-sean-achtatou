package com.agilebinary.mobilemonitor.client.a.b;

import android.content.Context;
import com.agilebinary.mobilemonitor.client.a.a.c;
import com.agilebinary.mobilemonitor.client.a.e;
import com.agilebinary.mobilemonitor.client.android.c.a;
import com.agilebinary.phonebeagle.client.R;

public final class j extends q {

    /* renamed from: a  reason: collision with root package name */
    private String f133a;
    private String b;
    private String c;

    public j(String str, long j, long j2, c cVar, e eVar) {
        super(str, j, j2, cVar, eVar);
        this.f133a = cVar.g();
        this.b = cVar.g();
        this.c = cVar.g();
    }

    public final String a() {
        return this.f133a;
    }

    public final String a(Context context) {
        return context.getString(x() == 1 ? R.string.label_event_sms_from_fmt : R.string.label_event_sms_to_fmt, a.b(this.b, this.c));
    }

    public final String b() {
        return this.b;
    }

    public final String c() {
        return this.c;
    }

    public final String d() {
        return this.f133a;
    }

    public final byte k() {
        return 2;
    }
}
