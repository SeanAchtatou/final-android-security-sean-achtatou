package com.agilebinary.mobilemonitor.client.android.a;

import com.agilebinary.mobilemonitor.a.a.a.a;
import com.agilebinary.mobilemonitor.client.android.c.f;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PublicKey;
import java.security.SignatureException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.security.spec.X509EncodedKeySpec;
import java.util.HashSet;
import javax.net.ssl.X509TrustManager;

public final class r implements X509TrustManager {

    /* renamed from: a  reason: collision with root package name */
    private PublicKey f162a;
    private boolean b;
    private HashSet c = new HashSet();
    private /* synthetic */ b d;

    r(b bVar) {
        this.d = bVar;
        if ("30820122300d06092a864886f70d01010105000382010f003082010a028201010091bf33fa0f93d502ba077d9d5a1d1cf6661c34f6ed9c58f8da92cd00c10e0a409ab0eab5413133eaeaaff781e0087440c94e57cf51373fab6b22ad8b183d39674b328d19fded40d12aafe2f6f6773093174392bd5f006313053e362c9414891559d9380134602aad941dbd01a6a3bbdfc05bc8b853e771473a9c1a4b0edaa7f3db7af9571ae57721fd0ba4a0297c7bff92ab2eba67d602a56b972252d18e2e685a0ce350a7911363a121d9a84b657f0eda24d26ac4306aae3944371716301c9148bf3512c2febdfd4b5b6f21a4cb37c4551f25f6f4b1352f8865f6a3cbfe3a6072ce26a6acce908acc3acdd571ba23fdc69b1b644cc09e66c41e48cf6c2504830203010001".trim().length() > 0) {
            try {
                this.f162a = KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(f.a("30820122300d06092a864886f70d01010105000382010f003082010a028201010091bf33fa0f93d502ba077d9d5a1d1cf6661c34f6ed9c58f8da92cd00c10e0a409ab0eab5413133eaeaaff781e0087440c94e57cf51373fab6b22ad8b183d39674b328d19fded40d12aafe2f6f6773093174392bd5f006313053e362c9414891559d9380134602aad941dbd01a6a3bbdfc05bc8b853e771473a9c1a4b0edaa7f3db7af9571ae57721fd0ba4a0297c7bff92ab2eba67d602a56b972252d18e2e685a0ce350a7911363a121d9a84b657f0eda24d26ac4306aae3944371716301c9148bf3512c2febdfd4b5b6f21a4cb37c4551f25f6f4b1352f8865f6a3cbfe3a6072ce26a6acce908acc3acdd571ba23fdc69b1b644cc09e66c41e48cf6c2504830203010001".toCharArray())));
            } catch (Exception e) {
                this.b = true;
            }
        }
    }

    public final void checkClientTrusted(X509Certificate[] x509CertificateArr, String str) {
    }

    public final void checkServerTrusted(X509Certificate[] x509CertificateArr, String str) {
        if (x509CertificateArr.length == 0) {
            throw new CertificateException("no certificate in chain");
        } else if (this.b) {
            throw new CertificateException("certificate invalid because couldn't decode configured public key");
        } else if (this.f162a != null) {
            X509Certificate x509Certificate = x509CertificateArr[0];
            if (!this.c.contains(x509Certificate)) {
                try {
                    x509Certificate.verify(this.f162a);
                    this.c.add(x509Certificate);
                } catch (InvalidKeyException e) {
                    a.e(e);
                    throw new CertificateException(e);
                } catch (NoSuchAlgorithmException e2) {
                    a.e(e2);
                    throw new CertificateException(e2);
                } catch (NoSuchProviderException e3) {
                    a.e(e3);
                    throw new CertificateException(e3);
                } catch (SignatureException e4) {
                    a.e(e4);
                    throw new CertificateException(e4);
                }
            }
        }
    }

    public final X509Certificate[] getAcceptedIssuers() {
        return null;
    }
}
