package com.agilebinary.mobilemonitor.client.android.ui;

import android.database.Cursor;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.agilebinary.mobilemonitor.client.android.c.c;
import com.agilebinary.phonebeagle.client.R;

public class k extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    protected TextView f274a;
    protected ImageView b;
    private EventListActivity_base c;

    public k(EventListActivity_base eventListActivity_base) {
        super(eventListActivity_base, null);
        this.c = eventListActivity_base;
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.f274a = (TextView) findViewById(R.id.eventlist_rowview_base_time);
        this.b = (ImageView) findViewById(R.id.eventlist_rowview_base_star);
    }

    public void a(Cursor cursor) {
        long j = cursor.getLong(0);
        TextView textView = this.f274a;
        getContext();
        textView.setText(c.a().a(j));
        this.b.setEnabled(this.c.a(j));
    }
}
