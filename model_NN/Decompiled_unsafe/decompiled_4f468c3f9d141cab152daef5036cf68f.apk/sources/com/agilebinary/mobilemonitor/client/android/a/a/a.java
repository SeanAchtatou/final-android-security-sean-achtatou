package com.agilebinary.mobilemonitor.client.android.a.a;

import java.io.Serializable;

public final class a extends c implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private long f143a;
    private int b;
    private b c;

    public final String a() {
        return "{\"create_date\":\"" + this.f143a + "\", \"geocoder_source_id\":\"" + this.b + "\", \"" + "mobile_country_code" + "\":\"" + super.c() + "\", \"" + "mobile_network_code" + "\":\"" + super.d() + "\", \"" + "cell_id" + "\":\"" + super.e() + "\", \"" + "location_area_code" + "\":\"" + super.f() + "\", \"" + "latitude" + "\":\"" + this.c.f144a + "\", \"" + "longitude" + "\":\"" + this.c.b + "\", \"" + "accuracy" + "\":\"" + this.c.c + "\", \"" + "country" + "\":\"" + this.c.d + "\", \"" + "country_code" + "\":\"" + this.c.e + "\", \"" + "region" + "\":\"" + this.c.f + "\", \"" + "city" + "\":\"" + this.c.g + "\", \"" + "street" + "\":\"" + this.c.h + "\"}";
    }

    public final void a(int i) {
        this.b = i;
    }

    public final void a(long j) {
        this.f143a = j;
    }

    public final void a(b bVar) {
        this.c = bVar;
    }

    public final b b() {
        return this.c;
    }

    public final String toString() {
        return "GeocodeGsmCellLocation: " + a();
    }
}
