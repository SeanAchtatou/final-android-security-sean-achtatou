package com.agilebinary.mobilemonitor.client.android.ui;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import java.util.ArrayList;
import java.util.List;

public final class g extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    private List f271a = new ArrayList();
    private /* synthetic */ MainActivity b;

    public g(MainActivity mainActivity) {
        this.b = mainActivity;
    }

    public final void a(int i, int i2, byte b2) {
        this.f271a.add(new az(this.b, i, i2, b2));
    }

    public final int getCount() {
        return this.f271a.size();
    }

    public final Object getItem(int i) {
        return this.f271a.get(i);
    }

    public final long getItemId(int i) {
        return (long) ((az) this.f271a.get(i)).c;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        az azVar = (az) getItem(i);
        h hVar = view == null ? new h(this.b, this.b) : (h) view;
        hVar.a(azVar);
        return hVar;
    }
}
