package com.agilebinary.mobilemonitor.client.android.ui;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.agilebinary.mobilemonitor.client.android.a.t;
import com.agilebinary.mobilemonitor.client.android.c.b;
import com.agilebinary.mobilemonitor.client.android.ui.a.g;
import com.agilebinary.phonebeagle.client.R;

public class MainActivity extends BaseLoggedInActivity implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    private static String f203a = b.a();
    private ListView b;
    private g c;
    /* access modifiers changed from: private */
    public Animation d;

    public MainActivity() {
        "" + this;
    }

    public static void a(Context context, byte b2, int i) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra("EXTRA_PROGRESS_EVENTTYPE", b2);
        intent.putExtra("EXTRA_PROGRESS_NUM", i);
        intent.setFlags(536870912);
        context.startActivity(intent);
    }

    private void e() {
        if (g.f214a == null) {
            this.g.h();
            this.b.invalidateViews();
            g gVar = new g(this);
            g.f214a = gVar;
            gVar.execute(new Void[0]);
        }
    }

    private static void f() {
        if (g.f214a != null) {
            try {
                g.f214a.a();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: protected */
    public final int a() {
        return R.layout.main;
    }

    /* access modifiers changed from: protected */
    public final void c() {
        f();
        super.c();
    }

    public void finish() {
        f();
        super.finish();
    }

    public void onCreate(Bundle bundle) {
        "" + this;
        super.onCreate(bundle);
        this.b = (ListView) findViewById(R.id.main_list);
        this.c = new g(this);
        this.b.setAdapter((ListAdapter) this.c);
        this.b.setOnItemClickListener(this);
        if (!this.g.d() && this.g.a().n()) {
            this.c.a(R.drawable.shoppingcart, R.string.label_main_upgrade, (byte) -3);
        }
        if (this.g.a().h()) {
            this.c.a(R.drawable.sms, R.string.label_event_sms, (byte) 3);
        }
        if (this.g.a().i()) {
            this.c.a(R.drawable.call, R.string.label_event_call, (byte) 2);
        }
        if (this.g.a().j()) {
            this.c.a(R.drawable.location, R.string.label_event_location, (byte) 1);
        }
        if (this.g.a().k()) {
            this.c.a(R.drawable.mms, R.string.label_event_mms, (byte) 4);
        }
        if (this.g.a().l()) {
            this.c.a(R.drawable.web, R.string.label_event_web, (byte) 6);
        }
        if (this.g.a().m()) {
            this.c.a(R.drawable.application, R.string.label_event_application, (byte) 9);
        }
        this.c.a(R.drawable.system, R.string.label_event_system, (byte) 8);
        this.c.a(R.drawable.control, R.string.label_main_controlpanel, (byte) -2);
        this.d = AnimationUtils.loadAnimation(this, R.anim.updating_star);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    public void onItemClick(AdapterView adapterView, View view, int i, long j) {
        az azVar = (az) this.c.getItem(i);
        if (azVar.c == -2) {
            f();
            startActivity(new Intent(this, ControlPanelActivity.class));
        } else if (azVar.c == -3) {
            f();
            t.a();
            try {
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse("N/A" + this.g.a().b())));
            } catch (ActivityNotFoundException e) {
            }
        } else {
            f();
            EventListActivity_base.a(this, azVar.c);
        }
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        this.g.a(intent.getByteExtra("EXTRA_PROGRESS_EVENTTYPE", (byte) 0), intent.getIntExtra("EXTRA_PROGRESS_NUM", 0));
        this.b.invalidateViews();
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.menu_main_refresh /*2131296413*/:
                e();
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        e();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }
}
