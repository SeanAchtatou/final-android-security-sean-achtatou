package com.agilebinary.mobilemonitor.client.a.b;

import android.content.Context;
import com.agilebinary.mobilemonitor.client.a.a.c;
import com.agilebinary.phonebeagle.client.R;

public final class e extends i {

    /* renamed from: a  reason: collision with root package name */
    private long f129a;
    private String b;

    public e(String str, long j, long j2, c cVar, com.agilebinary.mobilemonitor.client.a.e eVar) {
        super(str, j, j2, cVar, eVar);
        this.f129a = eVar.a(cVar.d());
        this.b = cVar.g();
    }

    public final long a() {
        return this.f129a;
    }

    public final String a(Context context) {
        return context.getString(R.string.label_event_web_search, this.b);
    }

    public final String b() {
        return this.b;
    }

    public final byte k() {
        return 13;
    }
}
