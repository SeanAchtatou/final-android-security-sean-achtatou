package com.agilebinary.a.a.a.c.b;

import com.agilebinary.a.a.a.b;
import com.agilebinary.a.a.a.f;
import com.agilebinary.a.a.a.h.a.a;
import com.agilebinary.a.a.a.h.c.c;
import com.agilebinary.a.a.a.h.c.h;
import java.net.InetAddress;

public final class g implements h {

    /* renamed from: a  reason: collision with root package name */
    private com.agilebinary.a.a.a.h.b.h f46a;

    public g(com.agilebinary.a.a.a.h.b.h hVar) {
        if (hVar == null) {
            throw new IllegalArgumentException("SchemeRegistry must not be null.");
        }
        this.f46a = hVar;
    }

    public final c a(b bVar, f fVar) {
        if (fVar == null) {
            throw new IllegalStateException("Request must not be null.");
        }
        c b = a.b(fVar.g());
        if (b != null) {
            return b;
        }
        if (bVar == null) {
            throw new IllegalStateException("Target host must not be null.");
        }
        InetAddress c = a.c(fVar.g());
        b a2 = a.a(fVar.g());
        boolean d = this.f46a.a(bVar.c()).d();
        return a2 == null ? new c(bVar, c, d) : new c(bVar, c, a2, d);
    }
}
