package com.agilebinary.a.a.a.c.a;

import com.agilebinary.a.a.a.k.b;
import com.agilebinary.a.a.a.k.c;
import com.agilebinary.a.a.a.k.e;
import com.agilebinary.a.a.a.k.f;
import com.agilebinary.a.a.a.k.i;
import com.agilebinary.a.a.a.k.k;

public class r implements k {
    public final void a(b bVar, String str) {
        if (bVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        } else if (str == null) {
            throw new e("Missing value for domain attribute");
        } else if (str.trim().length() == 0) {
            throw new e("Blank value for domain attribute");
        } else {
            bVar.b(str);
        }
    }

    public void a(c cVar, f fVar) {
        if (cVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        } else if (fVar == null) {
            throw new IllegalArgumentException("Cookie origin may not be null");
        } else {
            String a2 = fVar.a();
            String c = cVar.c();
            if (c == null) {
                throw new i("Cookie domain may not be null");
            } else if (a2.contains(".")) {
                if (!a2.endsWith(c)) {
                    if (c.startsWith(".")) {
                        c = c.substring(1, c.length());
                    }
                    if (!a2.equals(c)) {
                        throw new i("Illegal domain attribute \"" + c + "\". Domain of origin: \"" + a2 + "\"");
                    }
                }
            } else if (!a2.equals(c)) {
                throw new i("Illegal domain attribute \"" + c + "\". Domain of origin: \"" + a2 + "\"");
            }
        }
    }

    public boolean b(c cVar, f fVar) {
        if (cVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        } else if (fVar == null) {
            throw new IllegalArgumentException("Cookie origin may not be null");
        } else {
            String a2 = fVar.a();
            String c = cVar.c();
            if (c == null) {
                return false;
            }
            if (a2.equals(c)) {
                return true;
            }
            if (!c.startsWith(".")) {
                c = '.' + c;
            }
            return a2.endsWith(c) || a2.equals(c.substring(1));
        }
    }
}
