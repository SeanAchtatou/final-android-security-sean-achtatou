package com.agilebinary.a.a.a.i;

public final class d {

    /* renamed from: a  reason: collision with root package name */
    private final String f116a;
    private final String b;
    private final String c;
    private final String d;
    private final String e;

    private d(String str, String str2, String str3, String str4, String str5) {
        if (str == null) {
            throw new IllegalArgumentException("Package identifier must not be null.");
        }
        this.f116a = str;
        this.b = str2 != null ? str2 : "UNAVAILABLE";
        this.c = str3 != null ? str3 : "UNAVAILABLE";
        this.d = str4 != null ? str4 : "UNAVAILABLE";
        this.e = str5 != null ? str5 : "UNAVAILABLE";
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x004a  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x00c3  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final com.agilebinary.a.a.a.i.d a(java.lang.String r6, java.lang.ClassLoader r7) {
        /*
            r5 = 0
            if (r6 != 0) goto L_0x000b
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "Package identifier must not be null."
            r0.<init>(r1)
            throw r0
        L_0x000b:
            if (r7 != 0) goto L_0x00c5
            java.lang.Thread r0 = java.lang.Thread.currentThread()
            java.lang.ClassLoader r0 = r0.getContextClassLoader()
            r1 = r0
        L_0x0016:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0059 }
            r0.<init>()     // Catch:{ IOException -> 0x0059 }
            r2 = 46
            r3 = 47
            java.lang.String r2 = r6.replace(r2, r3)     // Catch:{ IOException -> 0x0059 }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ IOException -> 0x0059 }
            java.lang.String r2 = "/"
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ IOException -> 0x0059 }
            java.lang.String r2 = "version.properties"
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ IOException -> 0x0059 }
            java.lang.String r0 = r0.toString()     // Catch:{ IOException -> 0x0059 }
            java.io.InputStream r0 = r1.getResourceAsStream(r0)     // Catch:{ IOException -> 0x0059 }
            if (r0 == 0) goto L_0x005d
            java.util.Properties r2 = new java.util.Properties     // Catch:{ all -> 0x0054 }
            r2.<init>()     // Catch:{ all -> 0x0054 }
            r2.load(r0)     // Catch:{ all -> 0x0054 }
            r0.close()     // Catch:{ IOException -> 0x00b4 }
        L_0x0048:
            if (r2 == 0) goto L_0x00c3
            if (r6 != 0) goto L_0x005f
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "Package identifier must not be null."
            r0.<init>(r1)
            throw r0
        L_0x0054:
            r2 = move-exception
            r0.close()     // Catch:{ IOException -> 0x0059 }
            throw r2     // Catch:{ IOException -> 0x0059 }
        L_0x0059:
            r0 = move-exception
            r0 = r5
        L_0x005b:
            r2 = r0
            goto L_0x0048
        L_0x005d:
            r2 = r5
            goto L_0x0048
        L_0x005f:
            if (r2 == 0) goto L_0x00bf
            java.lang.String r0 = "info.module"
            java.lang.Object r0 = r2.get(r0)
            java.lang.String r0 = (java.lang.String) r0
            if (r0 == 0) goto L_0x00bd
            int r3 = r0.length()
            if (r3 > 0) goto L_0x00bd
            r3 = r5
        L_0x0072:
            java.lang.String r0 = "info.release"
            java.lang.Object r0 = r2.get(r0)
            java.lang.String r0 = (java.lang.String) r0
            if (r0 == 0) goto L_0x00bb
            int r4 = r0.length()
            if (r4 <= 0) goto L_0x008a
            java.lang.String r4 = "${pom.version}"
            boolean r4 = r0.equals(r4)
            if (r4 == 0) goto L_0x00bb
        L_0x008a:
            r4 = r5
        L_0x008b:
            java.lang.String r0 = "info.timestamp"
            java.lang.Object r0 = r2.get(r0)
            java.lang.String r0 = (java.lang.String) r0
            if (r0 == 0) goto L_0x00b7
            int r2 = r0.length()
            if (r2 <= 0) goto L_0x00a3
            java.lang.String r2 = "${mvn.timestamp}"
            boolean r2 = r0.equals(r2)
            if (r2 == 0) goto L_0x00b7
        L_0x00a3:
            r2 = r3
            r3 = r4
            r4 = r5
        L_0x00a6:
            if (r1 == 0) goto L_0x00ad
            java.lang.String r0 = r1.toString()
            r5 = r0
        L_0x00ad:
            com.agilebinary.a.a.a.i.d r0 = new com.agilebinary.a.a.a.i.d
            r1 = r6
            r0.<init>(r1, r2, r3, r4, r5)
        L_0x00b3:
            return r0
        L_0x00b4:
            r0 = move-exception
            r0 = r2
            goto L_0x005b
        L_0x00b7:
            r2 = r3
            r3 = r4
            r4 = r0
            goto L_0x00a6
        L_0x00bb:
            r4 = r0
            goto L_0x008b
        L_0x00bd:
            r3 = r0
            goto L_0x0072
        L_0x00bf:
            r4 = r5
            r3 = r5
            r2 = r5
            goto L_0x00a6
        L_0x00c3:
            r0 = r5
            goto L_0x00b3
        L_0x00c5:
            r1 = r7
            goto L_0x0016
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.a.a.a.i.d.a(java.lang.String, java.lang.ClassLoader):com.agilebinary.a.a.a.i.d");
    }

    public final String a() {
        return this.c;
    }

    public final String toString() {
        StringBuffer stringBuffer = new StringBuffer(this.f116a.length() + 20 + this.b.length() + this.c.length() + this.d.length() + this.e.length());
        stringBuffer.append("VersionInfo(").append(this.f116a).append(':').append(this.b);
        if (!"UNAVAILABLE".equals(this.c)) {
            stringBuffer.append(':').append(this.c);
        }
        if (!"UNAVAILABLE".equals(this.d)) {
            stringBuffer.append(':').append(this.d);
        }
        stringBuffer.append(')');
        if (!"UNAVAILABLE".equals(this.e)) {
            stringBuffer.append('@').append(this.e);
        }
        return stringBuffer.toString();
    }
}
