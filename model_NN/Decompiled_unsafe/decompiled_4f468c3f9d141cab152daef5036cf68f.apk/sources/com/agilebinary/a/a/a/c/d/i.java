package com.agilebinary.a.a.a.c.d;

import com.agilebinary.a.a.a.a.a;
import com.agilebinary.a.a.a.aa;
import com.agilebinary.a.a.a.c.a.s;
import com.agilebinary.a.a.a.c.a.t;
import com.agilebinary.a.a.a.c.a.v;
import com.agilebinary.a.a.a.d.b.c;
import com.agilebinary.a.a.a.d.b.h;
import com.agilebinary.a.a.a.e.e;
import com.agilebinary.a.a.a.e.f;
import com.agilebinary.a.a.a.f.b;
import com.agilebinary.a.a.a.f.g;
import com.agilebinary.a.a.a.f.j;
import com.agilebinary.a.a.a.h.k;
import com.agilebinary.a.a.a.h.n;
import com.agilebinary.a.a.a.i.d;

public final class i extends d {
    public i() {
        super(null, null);
    }

    public i(k kVar, e eVar) {
        super(kVar, eVar);
        a(new p());
    }

    /* access modifiers changed from: protected */
    public final e a() {
        f fVar = new f();
        fVar.a("http.protocol.version", aa.d);
        fVar.a("http.protocol.content-charset", "ISO-8859-1");
        fVar.b("http.tcp.nodelay");
        fVar.b("http.socket.buffer-size", 8192);
        d a2 = d.a("org.apache.http.client", getClass().getClassLoader());
        fVar.a("http.useragent", "Apache-HttpClient/" + (a2 != null ? a2.a() : "UNAVAILABLE") + " (java 1.5)");
        return fVar;
    }

    /* access modifiers changed from: protected */
    public final com.agilebinary.a.a.a.f.k b() {
        g gVar = new g((byte) 0);
        gVar.a("http.scheme-registry", r().a());
        gVar.a("http.authscheme-registry", s());
        gVar.a("http.cookiespec-registry", t());
        gVar.a("http.cookie-store", u());
        gVar.a("http.auth.credentials-provider", v());
        return gVar;
    }

    /* access modifiers changed from: protected */
    public final b c() {
        return new b();
    }

    /* JADX WARN: Type inference failed for: r1v14, types: [java.lang.Object] */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.agilebinary.a.a.a.h.k d() {
        /*
            r6 = this;
            com.agilebinary.a.a.a.h.b.h r2 = new com.agilebinary.a.a.a.h.b.h
            r2.<init>()
            com.agilebinary.a.a.a.h.b.g r1 = new com.agilebinary.a.a.a.h.b.g
            java.lang.String r3 = "http"
            r4 = 80
            com.agilebinary.a.a.a.h.b.e r5 = com.agilebinary.a.a.a.h.b.e.b()
            r1.<init>(r3, r4, r5)
            r2.a(r1)
            com.agilebinary.a.a.a.h.b.g r1 = new com.agilebinary.a.a.a.h.b.g
            java.lang.String r3 = "https"
            r4 = 443(0x1bb, float:6.21E-43)
            com.agilebinary.a.a.a.h.d.f r5 = com.agilebinary.a.a.a.h.d.f.b()
            r1.<init>(r3, r4, r5)
            r2.a(r1)
            com.agilebinary.a.a.a.e.e r1 = r6.q()
            r3 = 0
            java.lang.String r4 = "http.connection-manager.factory-class-name"
            java.lang.Object r6 = r1.a(r4)
            java.lang.String r6 = (java.lang.String) r6
            if (r6 == 0) goto L_0x007e
            java.lang.Class r1 = java.lang.Class.forName(r6)     // Catch:{ ClassNotFoundException -> 0x0048, IllegalAccessException -> 0x0062, InstantiationException -> 0x006d }
            java.lang.Object r1 = r1.newInstance()     // Catch:{ ClassNotFoundException -> 0x0048, IllegalAccessException -> 0x0062, InstantiationException -> 0x006d }
            r0 = r1
            com.agilebinary.a.a.a.h.i r0 = (com.agilebinary.a.a.a.h.i) r0     // Catch:{ ClassNotFoundException -> 0x0048, IllegalAccessException -> 0x0062, InstantiationException -> 0x006d }
            r6 = r0
            r1 = r6
        L_0x0041:
            if (r1 == 0) goto L_0x0078
            com.agilebinary.a.a.a.h.k r1 = r1.a()
        L_0x0047:
            return r1
        L_0x0048:
            r1 = move-exception
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Invalid class name: "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r6)
            java.lang.String r2 = r2.toString()
            r1.<init>(r2)
            throw r1
        L_0x0062:
            r1 = move-exception
            java.lang.IllegalAccessError r2 = new java.lang.IllegalAccessError
            java.lang.String r1 = r1.getMessage()
            r2.<init>(r1)
            throw r2
        L_0x006d:
            r1 = move-exception
            java.lang.InstantiationError r2 = new java.lang.InstantiationError
            java.lang.String r1 = r1.getMessage()
            r2.<init>(r1)
            throw r2
        L_0x0078:
            com.agilebinary.a.a.a.c.b.i r1 = new com.agilebinary.a.a.a.c.b.i
            r1.<init>(r2)
            goto L_0x0047
        L_0x007e:
            r1 = r3
            goto L_0x0041
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.a.a.a.c.d.i.d():com.agilebinary.a.a.a.h.k");
    }

    /* access modifiers changed from: protected */
    public final a e() {
        return new a();
    }

    /* access modifiers changed from: protected */
    public final com.agilebinary.a.a.a.k.d f() {
        com.agilebinary.a.a.a.k.d dVar = new com.agilebinary.a.a.a.k.d();
        dVar.a("best-match", new t());
        dVar.a("compatibility", new v());
        dVar.a("netscape", new s());
        dVar.a("rfc2109", new com.agilebinary.a.a.a.c.a.e());
        dVar.a("rfc2965", new com.agilebinary.a.a.a.c.a.i());
        return dVar;
    }

    /* access modifiers changed from: protected */
    public final com.agilebinary.a.a.a.s g() {
        return new com.agilebinary.a.a.a.c.d();
    }

    /* access modifiers changed from: protected */
    public final n h() {
        return new e();
    }

    /* access modifiers changed from: protected */
    public final j i() {
        j jVar = new j();
        jVar.a(new h());
        jVar.a(new com.agilebinary.a.a.a.f.e());
        jVar.a(new com.agilebinary.a.a.a.f.i());
        jVar.a(new com.agilebinary.a.a.a.d.b.e());
        jVar.a(new com.agilebinary.a.a.a.f.h());
        jVar.a(new com.agilebinary.a.a.a.f.a());
        jVar.a(new com.agilebinary.a.a.a.d.b.d());
        jVar.a(new c());
        jVar.a(new com.agilebinary.a.a.a.d.b.g());
        jVar.a(new com.agilebinary.a.a.a.d.b.b());
        jVar.a(new com.agilebinary.a.a.a.d.b.a());
        jVar.a(new com.agilebinary.a.a.a.d.b.f());
        return jVar;
    }

    /* access modifiers changed from: protected */
    public final com.agilebinary.a.a.a.d.k j() {
        return new a();
    }

    /* access modifiers changed from: protected */
    public final com.agilebinary.a.a.a.d.b k() {
        return new c();
    }

    /* access modifiers changed from: protected */
    public final com.agilebinary.a.a.a.d.b l() {
        return new b();
    }

    /* access modifiers changed from: protected */
    public final com.agilebinary.a.a.a.d.d m() {
        return new o();
    }

    /* access modifiers changed from: protected */
    public final com.agilebinary.a.a.a.d.e n() {
        return new n();
    }

    /* access modifiers changed from: protected */
    public final com.agilebinary.a.a.a.h.c.h o() {
        return new com.agilebinary.a.a.a.c.b.g(r().a());
    }

    /* access modifiers changed from: protected */
    public final com.agilebinary.a.a.a.d.f p() {
        return new f();
    }
}
