package com.agilebinary.a.a.a.a;

import java.util.Locale;
import java.util.concurrent.ConcurrentHashMap;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private final ConcurrentHashMap f3a = new ConcurrentHashMap();

    public final h a(String str) {
        if (str == null) {
            throw new IllegalArgumentException("Name may not be null");
        }
        f fVar = (f) this.f3a.get(str.toLowerCase(Locale.ENGLISH));
        if (fVar != null) {
            return fVar.a();
        }
        throw new IllegalStateException("Unsupported authentication scheme: " + str);
    }
}
