package com.agilebinary.a.a.a.f;

import java.util.HashMap;
import java.util.Map;

public final class g implements k {

    /* renamed from: a  reason: collision with root package name */
    private final k f92a;
    private Map b;

    private g() {
        this.b = null;
        this.f92a = null;
    }

    public g(byte b2) {
        this();
    }

    public final Object a(String str) {
        if (str == null) {
            throw new IllegalArgumentException("Id may not be null");
        }
        Object obj = null;
        if (this.b != null) {
            obj = this.b.get(str);
        }
        return (obj != null || this.f92a == null) ? obj : this.f92a.a(str);
    }

    public final void a(String str, Object obj) {
        if (str == null) {
            throw new IllegalArgumentException("Id may not be null");
        }
        if (this.b == null) {
            this.b = new HashMap();
        }
        this.b.put(str, obj);
    }
}
