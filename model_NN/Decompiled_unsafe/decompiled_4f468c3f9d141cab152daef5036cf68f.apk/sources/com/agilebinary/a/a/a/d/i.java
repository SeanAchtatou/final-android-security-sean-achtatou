package com.agilebinary.a.a.a.d;

import java.io.IOException;

public final class i extends IOException {
    public i() {
    }

    public i(String str) {
        super(str);
    }

    public i(Throwable th) {
        initCause(th);
    }
}
