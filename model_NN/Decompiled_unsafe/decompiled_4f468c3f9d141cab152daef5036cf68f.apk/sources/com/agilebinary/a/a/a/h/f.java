package com.agilebinary.a.a.a.h;

import com.agilebinary.a.a.a.b;
import java.net.ConnectException;

public final class f extends ConnectException {

    /* renamed from: a  reason: collision with root package name */
    private final b f113a;

    public f(b bVar, ConnectException connectException) {
        super("Connection to " + bVar + " refused");
        this.f113a = bVar;
        initCause(connectException);
    }
}
