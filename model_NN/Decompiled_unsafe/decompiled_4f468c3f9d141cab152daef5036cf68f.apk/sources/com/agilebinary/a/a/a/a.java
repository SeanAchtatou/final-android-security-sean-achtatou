package com.agilebinary.a.a.a;

import com.agilebinary.a.a.a.i.c;
import java.io.Serializable;

public class a implements Serializable, Cloneable {

    /* renamed from: a  reason: collision with root package name */
    protected final int f2a;
    protected final int b;
    private String c;

    public a(String str, int i, int i2) {
        if (str == null) {
            throw new IllegalArgumentException("Protocol name must not be null.");
        } else if (i < 0) {
            throw new IllegalArgumentException("Protocol major version number must not be negative.");
        } else if (i2 < 0) {
            throw new IllegalArgumentException("Protocol minor version number may not be negative");
        } else {
            this.c = str;
            this.f2a = i;
            this.b = i2;
        }
    }

    public a a(int i, int i2) {
        return (i == this.f2a && i2 == this.b) ? this : new a(this.c, i, i2);
    }

    public final String a() {
        return this.c;
    }

    public final boolean a(a aVar) {
        if (aVar != null && this.c.equals(aVar.c)) {
            if (aVar == null) {
                throw new IllegalArgumentException("Protocol version must not be null.");
            } else if (!this.c.equals(aVar.c)) {
                throw new IllegalArgumentException("Versions for different protocols cannot be compared. " + this + " " + aVar);
            } else {
                int i = this.f2a - aVar.f2a;
                if (i == 0) {
                    i = this.b - aVar.b;
                }
                if (i <= 0) {
                    return true;
                }
            }
        }
        return false;
    }

    public final int b() {
        return this.f2a;
    }

    public final int c() {
        return this.b;
    }

    public Object clone() {
        return super.clone();
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof a)) {
            return false;
        }
        a aVar = (a) obj;
        return this.c.equals(aVar.c) && this.f2a == aVar.f2a && this.b == aVar.b;
    }

    public final int hashCode() {
        return (this.c.hashCode() ^ (this.f2a * 100000)) ^ this.b;
    }

    public String toString() {
        c cVar = new c(16);
        cVar.a(this.c);
        cVar.a('/');
        cVar.a(Integer.toString(this.f2a));
        cVar.a('.');
        cVar.a(Integer.toString(this.b));
        return cVar.toString();
    }
}
