package com.agilebinary.a.a.a.c.b;

import com.agilebinary.a.a.a.e.e;
import com.agilebinary.a.a.a.h.c.c;
import com.agilebinary.a.a.a.h.g;
import com.agilebinary.a.a.a.h.k;

public abstract class m extends l {

    /* renamed from: a  reason: collision with root package name */
    protected volatile a f52a;

    protected m(k kVar, a aVar) {
        super(kVar, aVar.f35a);
        this.f52a = aVar;
    }

    private void a(a aVar) {
        if (o() || aVar == null) {
            throw new n();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.a.a.a.c.b.a.a(boolean, com.agilebinary.a.a.a.e.e):void
     arg types: [int, com.agilebinary.a.a.a.e.e]
     candidates:
      com.agilebinary.a.a.a.c.b.a.a(com.agilebinary.a.a.a.h.c.c, com.agilebinary.a.a.a.e.e):void
      com.agilebinary.a.a.a.c.b.a.a(boolean, com.agilebinary.a.a.a.e.e):void */
    public final void a(e eVar) {
        a i = i();
        a(i);
        i.a(false, eVar);
    }

    public final void a(com.agilebinary.a.a.a.f.k kVar, e eVar) {
        a i = i();
        a(i);
        i.a(eVar);
    }

    public final void a(c cVar, com.agilebinary.a.a.a.f.k kVar, e eVar) {
        a i = i();
        a(i);
        i.a(cVar, eVar);
    }

    public final void a(Object obj) {
        a i = i();
        a(i);
        i.a(obj);
    }

    public final c c_() {
        a i = i();
        a(i);
        if (i.c == null) {
            return null;
        }
        return i.c.h();
    }

    /* access modifiers changed from: protected */
    public a i() {
        return this.f52a;
    }

    /* access modifiers changed from: protected */
    public synchronized void j() {
        this.f52a = null;
        super.j();
    }

    public final void k() {
        a i = i();
        if (i != null) {
            i.b();
        }
        g n = n();
        if (n != null) {
            n.k();
        }
    }

    public final void m() {
        a i = i();
        if (i != null) {
            i.b();
        }
        g n = n();
        if (n != null) {
            n.m();
        }
    }
}
