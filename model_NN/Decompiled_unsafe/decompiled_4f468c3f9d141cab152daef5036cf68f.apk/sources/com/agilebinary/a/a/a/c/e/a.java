package com.agilebinary.a.a.a.c.e;

import com.agilebinary.a.a.a.c;
import com.agilebinary.a.a.a.c.c.e;
import com.agilebinary.a.a.a.c.c.m;
import com.agilebinary.a.a.a.c.c.o;
import com.agilebinary.a.a.a.t;
import com.agilebinary.a.a.a.x;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private final com.agilebinary.a.a.a.g.a f81a;

    public a(com.agilebinary.a.a.a.g.a aVar) {
        if (aVar == null) {
            throw new IllegalArgumentException("Content length strategy may not be null");
        }
        this.f81a = aVar;
    }

    public final c a(com.agilebinary.a.a.a.j.a aVar, x xVar) {
        if (aVar == null) {
            throw new IllegalArgumentException("Session input buffer may not be null");
        } else if (xVar == null) {
            throw new IllegalArgumentException("HTTP message may not be null");
        } else {
            com.agilebinary.a.a.a.g.c cVar = new com.agilebinary.a.a.a.g.c();
            long a2 = this.f81a.a(xVar);
            if (a2 == -2) {
                cVar.a(true);
                cVar.a(-1);
                cVar.a(new e(aVar));
            } else if (a2 == -1) {
                cVar.a(false);
                cVar.a(-1);
                cVar.a(new o(aVar));
            } else {
                cVar.a(false);
                cVar.a(a2);
                cVar.a(new m(aVar, a2));
            }
            t c = xVar.c("Content-Type");
            if (c != null) {
                cVar.a(c);
            }
            t c2 = xVar.c("Content-Encoding");
            if (c2 != null) {
                cVar.b(c2);
            }
            return cVar;
        }
    }
}
