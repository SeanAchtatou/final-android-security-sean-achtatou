package com.agilebinary.a.a.a.d.c;

import com.agilebinary.a.a.a.a;
import com.agilebinary.a.a.a.b.l;
import com.agilebinary.a.a.a.b.q;
import com.agilebinary.a.a.a.e.e;
import com.agilebinary.a.a.a.h.b;
import com.agilebinary.a.a.a.h.d;
import java.io.IOException;
import java.net.URI;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public abstract class f extends l implements b, g, Cloneable {
    private Lock c = new ReentrantLock();
    private boolean d;
    private URI e;
    private b f;
    private d g;

    public final com.agilebinary.a.a.a.d a() {
        String b = b();
        a b2 = com.agilebinary.a.a.a.e.b.b(g());
        URI uri = this.e;
        String aSCIIString = uri != null ? uri.toASCIIString() : null;
        if (aSCIIString == null || aSCIIString.length() == 0) {
            aSCIIString = "/";
        }
        return new q(b, aSCIIString, b2);
    }

    public final void a(b bVar) {
        this.c.lock();
        try {
            if (this.d) {
                throw new IOException("Request already aborted");
            }
            this.g = null;
            this.f = bVar;
        } finally {
            this.c.unlock();
        }
    }

    public final void a(d dVar) {
        this.c.lock();
        try {
            if (this.d) {
                throw new IOException("Request already aborted");
            }
            this.f = null;
            this.g = dVar;
        } finally {
            this.c.unlock();
        }
    }

    public final void a(URI uri) {
        this.e = uri;
    }

    public abstract String b();

    public final a c() {
        return com.agilebinary.a.a.a.e.b.b(g());
    }

    public Object clone() {
        f fVar = (f) super.clone();
        fVar.c = new ReentrantLock();
        fVar.d = false;
        fVar.g = null;
        fVar.f = null;
        fVar.f15a = (com.agilebinary.a.a.a.b.b) com.agilebinary.a.a.a.d.a.a.a(this.f15a);
        fVar.b = (e) com.agilebinary.a.a.a.d.a.a.a(this.b);
        return fVar;
    }

    public final void d() {
        this.c.lock();
        try {
            if (!this.d) {
                this.d = true;
                b bVar = this.f;
                d dVar = this.g;
                this.c.unlock();
                if (bVar != null) {
                    bVar.a();
                }
                if (dVar != null) {
                    try {
                        dVar.b();
                    } catch (IOException e2) {
                    }
                }
            }
        } finally {
            this.c.unlock();
        }
    }

    public final URI e_() {
        return this.e;
    }
}
