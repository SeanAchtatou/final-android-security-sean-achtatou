package com.agilebinary.a.a.a.c.a;

import com.agilebinary.a.a.a.b.e;
import com.agilebinary.a.a.a.i.c;
import com.agilebinary.a.a.a.k.f;
import com.agilebinary.a.a.a.k.i;
import com.agilebinary.a.a.a.k.l;
import com.agilebinary.a.a.a.k.m;
import com.agilebinary.a.a.a.t;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class ag extends ah {

    /* renamed from: a  reason: collision with root package name */
    private static final l f26a = new l();
    private static final String[] b = {"EEE, dd MMM yyyy HH:mm:ss zzz", "EEEE, dd-MMM-yy HH:mm:ss zzz", "EEE MMM d HH:mm:ss yyyy"};
    private final String[] c;
    private final boolean d;

    public ag() {
        this(null, false);
    }

    public ag(String[] strArr, boolean z) {
        if (strArr != null) {
            this.c = (String[]) strArr.clone();
        } else {
            this.c = b;
        }
        this.d = z;
        a("version", new f());
        a("path", new m());
        a("domain", new c());
        a("max-age", new ac());
        a("secure", new h());
        a("comment", new o());
        a("expires", new j(this.c));
    }

    private static void a(c cVar, String str, String str2, int i) {
        cVar.a(str);
        cVar.a("=");
        if (str2 == null) {
            return;
        }
        if (i > 0) {
            cVar.a('\"');
            cVar.a(str2);
            cVar.a('\"');
            return;
        }
        cVar.a(str2);
    }

    private List b(List list) {
        int i;
        int i2 = Integer.MAX_VALUE;
        Iterator it = list.iterator();
        while (true) {
            i = i2;
            if (!it.hasNext()) {
                break;
            }
            com.agilebinary.a.a.a.k.c cVar = (com.agilebinary.a.a.a.k.c) it.next();
            i2 = cVar.h() < i ? cVar.h() : i;
        }
        c cVar2 = new c(list.size() * 40);
        cVar2.a("Cookie");
        cVar2.a(": ");
        cVar2.a("$Version=");
        cVar2.a(Integer.toString(i));
        Iterator it2 = list.iterator();
        while (it2.hasNext()) {
            cVar2.a("; ");
            a(cVar2, (com.agilebinary.a.a.a.k.c) it2.next(), i);
        }
        ArrayList arrayList = new ArrayList(1);
        arrayList.add(new e(cVar2));
        return arrayList;
    }

    private List c(List list) {
        ArrayList arrayList = new ArrayList(list.size());
        Iterator it = list.iterator();
        while (it.hasNext()) {
            com.agilebinary.a.a.a.k.c cVar = (com.agilebinary.a.a.a.k.c) it.next();
            int h = cVar.h();
            c cVar2 = new c(40);
            cVar2.a("Cookie: ");
            cVar2.a("$Version=");
            cVar2.a(Integer.toString(h));
            cVar2.a("; ");
            a(cVar2, cVar, h);
            arrayList.add(new e(cVar2));
        }
        return arrayList;
    }

    public int a() {
        return 1;
    }

    public List a(t tVar, f fVar) {
        if (tVar == null) {
            throw new IllegalArgumentException("Header may not be null");
        } else if (fVar == null) {
            throw new IllegalArgumentException("Cookie origin may not be null");
        } else if (tVar.a().equalsIgnoreCase("Set-Cookie")) {
            return a(tVar.c(), fVar);
        } else {
            throw new com.agilebinary.a.a.a.k.e("Unrecognized cookie header '" + tVar.toString() + "'");
        }
    }

    public final List a(List list) {
        List list2;
        if (list == null) {
            throw new IllegalArgumentException("List of cookies may not be null");
        } else if (list.isEmpty()) {
            throw new IllegalArgumentException("List of cookies may not be empty");
        } else {
            if (list.size() > 1) {
                list2 = new ArrayList(list);
                Collections.sort(list2, f26a);
            } else {
                list2 = list;
            }
            return this.d ? b(list2) : c(list2);
        }
    }

    /* access modifiers changed from: protected */
    public void a(c cVar, com.agilebinary.a.a.a.k.c cVar2, int i) {
        a(cVar, cVar2.a(), cVar2.b(), i);
        if (cVar2.d() != null && (cVar2 instanceof m) && ((m) cVar2).e("path")) {
            cVar.a("; ");
            a(cVar, "$Path", cVar2.d(), i);
        }
        if (cVar2.c() != null && (cVar2 instanceof m) && ((m) cVar2).e("domain")) {
            cVar.a("; ");
            a(cVar, "$Domain", cVar2.c(), i);
        }
    }

    public void a(com.agilebinary.a.a.a.k.c cVar, f fVar) {
        if (cVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        }
        String a2 = cVar.a();
        if (a2.indexOf(32) != -1) {
            throw new i("Cookie name may not contain blanks");
        } else if (a2.startsWith("$")) {
            throw new i("Cookie name may not start with $");
        } else {
            super.a(cVar, fVar);
        }
    }

    public t b() {
        return null;
    }

    public String toString() {
        return "rfc2109";
    }
}
