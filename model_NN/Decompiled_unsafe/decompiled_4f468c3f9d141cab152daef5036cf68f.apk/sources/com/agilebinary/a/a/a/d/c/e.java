package com.agilebinary.a.a.a.d.c;

import com.agilebinary.a.a.a.c;
import com.agilebinary.a.a.a.d.a.a;
import com.agilebinary.a.a.a.p;
import com.agilebinary.a.a.a.t;

public abstract class e extends f implements p {
    private c c;

    public final void a(c cVar) {
        this.c = cVar;
    }

    public Object clone() {
        e eVar = (e) super.clone();
        if (this.c != null) {
            eVar.c = (c) a.a(this.c);
        }
        return eVar;
    }

    public final boolean g_() {
        t c2 = c("Expect");
        return c2 != null && "100-continue".equalsIgnoreCase(c2.b());
    }

    public final c h() {
        return this.c;
    }
}
