package com.agilebinary.a.a.a.i;

import com.agilebinary.a.a.a.f.d;
import java.io.Serializable;

public final class c implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private char[] f115a;
    private int b;

    public c(int i) {
        if (i < 0) {
            throw new IllegalArgumentException("Buffer capacity may not be negative");
        }
        this.f115a = new char[i];
    }

    private void d(int i) {
        char[] cArr = new char[Math.max(this.f115a.length << 1, i)];
        System.arraycopy(this.f115a, 0, cArr, 0, this.b);
        this.f115a = cArr;
    }

    public final char a(int i) {
        return this.f115a[i];
    }

    public final int a(int i, int i2, int i3) {
        int i4 = i2 < 0 ? 0 : i2;
        int i5 = i3 > this.b ? this.b : i3;
        if (i4 > i5) {
            return -1;
        }
        while (i4 < i5) {
            if (this.f115a[i4] == i) {
                return i4;
            }
            i4++;
        }
        return -1;
    }

    public final String a(int i, int i2) {
        return new String(this.f115a, i, i2 - i);
    }

    public final void a() {
        this.b = 0;
    }

    public final void a(char c) {
        int i = this.b + 1;
        if (i > this.f115a.length) {
            d(i);
        }
        this.f115a[this.b] = c;
        this.b = i;
    }

    public final void a(c cVar, int i, int i2) {
        if (cVar != null) {
            a(cVar.f115a, i, i2);
        }
    }

    public final void a(String str) {
        String str2 = str == null ? "null" : str;
        int length = str2.length();
        int i = this.b + length;
        if (i > this.f115a.length) {
            d(i);
        }
        str2.getChars(0, length, this.f115a, this.b);
        this.b = i;
    }

    public final void a(byte[] bArr, int i, int i2) {
        if (bArr != null) {
            if (i < 0 || i > bArr.length || i2 < 0 || i + i2 < 0 || i + i2 > bArr.length) {
                throw new IndexOutOfBoundsException("off: " + i + " len: " + i2 + " b.length: " + bArr.length);
            } else if (i2 != 0) {
                int i3 = this.b;
                int i4 = i3 + i2;
                if (i4 > this.f115a.length) {
                    d(i4);
                }
                int i5 = i;
                while (i3 < i4) {
                    this.f115a[i3] = (char) (bArr[i5] & 255);
                    i5++;
                    i3++;
                }
                this.b = i4;
            }
        }
    }

    public final void a(char[] cArr, int i, int i2) {
        if (cArr != null) {
            if (i < 0 || i > cArr.length || i2 < 0 || i + i2 < 0 || i + i2 > cArr.length) {
                throw new IndexOutOfBoundsException("off: " + i + " len: " + i2 + " b.length: " + cArr.length);
            } else if (i2 != 0) {
                int i3 = this.b + i2;
                if (i3 > this.f115a.length) {
                    d(i3);
                }
                System.arraycopy(cArr, i, this.f115a, this.b, i2);
                this.b = i3;
            }
        }
    }

    public final String b(int i, int i2) {
        if (i < 0) {
            throw new IndexOutOfBoundsException("Negative beginIndex: " + i);
        } else if (i2 > this.b) {
            throw new IndexOutOfBoundsException("endIndex: " + i2 + " > length: " + this.b);
        } else if (i > i2) {
            throw new IndexOutOfBoundsException("beginIndex: " + i + " > endIndex: " + i2);
        } else {
            int i3 = i;
            while (i3 < i2 && d.a(this.f115a[i3])) {
                i3++;
            }
            int i4 = i2;
            while (i4 > i3 && d.a(this.f115a[i4 - 1])) {
                i4--;
            }
            return new String(this.f115a, i3, i4 - i3);
        }
    }

    public final void b(int i) {
        if (i > 0 && i > this.f115a.length - this.b) {
            d(this.b + i);
        }
    }

    public final char[] b() {
        return this.f115a;
    }

    public final int c() {
        return this.b;
    }

    public final int c(int i) {
        return a(i, 0, this.b);
    }

    public final boolean d() {
        return this.b == 0;
    }

    public final String toString() {
        return new String(this.f115a, 0, this.b);
    }
}
