package com.agilebinary.a.a.a.d.b;

import com.agilebinary.a.a.a.ab;
import com.agilebinary.a.a.a.f;
import com.agilebinary.a.a.a.f.k;
import com.agilebinary.a.a.a.h.a;
import com.agilebinary.a.a.a.h.c.c;

public final class e implements ab {
    public final void a(f fVar, k kVar) {
        if (fVar == null) {
            throw new IllegalArgumentException("HTTP request may not be null");
        } else if (fVar.a().a().equalsIgnoreCase("CONNECT")) {
            fVar.b("Proxy-Connection", "Keep-Alive");
        } else {
            a aVar = (a) kVar.a("http.connection");
            if (aVar == null) {
                throw new IllegalStateException("Client connection not specified in HTTP context");
            }
            c c_ = aVar.c_();
            if ((c_.c() == 1 || c_.d()) && !fVar.a("Connection")) {
                fVar.a("Connection", "Keep-Alive");
            }
            if (c_.c() == 2 && !c_.d() && !fVar.a("Proxy-Connection")) {
                fVar.a("Proxy-Connection", "Keep-Alive");
            }
        }
    }
}
