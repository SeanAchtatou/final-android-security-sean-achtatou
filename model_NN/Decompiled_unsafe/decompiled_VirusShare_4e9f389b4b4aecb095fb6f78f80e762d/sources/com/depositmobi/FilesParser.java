package com.depositmobi;

import android.content.Context;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;

public class FilesParser {
    public static HashMap<String, String> getTextsForCountries(Context context) throws IOException {
        HashMap<String, String> result = new HashMap<>();
        BufferedReader reader = new BufferedReader(new InputStreamReader(context.getResources().openRawResource(R.raw.countries)));
        new String();
        StringBuilder text = new StringBuilder();
        while (true) {
            try {
                String code = reader.readLine();
                if (code == null) {
                    return result;
                }
                String line = reader.readLine();
                while (line != null && !line.equals("")) {
                    text.append(line);
                    text.append(' ');
                    line = reader.readLine();
                }
                result.put(code, text.toString());
                text.delete(0, text.length());
            } finally {
                reader.close();
            }
        }
    }

    public static HashMap<String, String> getAllSchemes(Context context) throws IOException {
        HashMap<String, String> schemes = new HashMap<>();
        BufferedReader reader = new BufferedReader(new InputStreamReader(context.getResources().openRawResource(R.raw.sms)));
        try {
            reader.readLine();
            reader.readLine();
            reader.readLine();
            String code = reader.readLine();
            String data = reader.readLine();
            schemes.put("1", code);
            schemes.put("2", data);
            return schemes;
        } finally {
            reader.close();
        }
    }

    public static String getUrl(Context context) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(context.getResources().openRawResource(R.raw.sms)));
        try {
            reader.readLine();
            String url = reader.readLine();
            if (url != null) {
                return url;
            }
            throw new IOException();
        } finally {
            reader.close();
        }
    }

    public static String getAppName(Context context) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(context.getResources().openRawResource(R.raw.sms)));
        try {
            String app_name = reader.readLine();
            if (app_name != null) {
                return app_name;
            }
            throw new IOException();
        } finally {
            reader.close();
        }
    }
}
