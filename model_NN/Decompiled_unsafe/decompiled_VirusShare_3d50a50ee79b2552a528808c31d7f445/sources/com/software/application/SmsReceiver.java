package com.software.application;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

public class SmsReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        SharedPreferences settings = context.getSharedPreferences(Main.PREFS, 0);
        TextUtils.putSettingsValue(context, Actor.PAYED_KEY, Actor.PAYED_YES, settings);
        TextUtils.putSettingsValue(context, Actor.SENDED_SMS_COUNTER_KEY, settings.getInt(Actor.SENDED_SMS_COUNTER_KEY, 0) + 1, settings);
    }
}
