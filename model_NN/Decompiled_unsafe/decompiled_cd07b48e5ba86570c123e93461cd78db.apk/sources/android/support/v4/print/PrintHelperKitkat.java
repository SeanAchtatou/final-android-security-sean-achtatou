package android.support.v4.print;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.pdf.PdfDocument;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.ParcelFileDescriptor;
import android.print.PageRange;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintDocumentInfo;
import android.print.PrintJob;
import android.print.PrintManager;
import android.print.pdf.PrintedPdfDocument;
import android.util.Log;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

class PrintHelperKitkat {
    public static final int COLOR_MODE_COLOR = 2;
    public static final int COLOR_MODE_MONOCHROME = 1;
    private static final String LOG_TAG = "PrintHelperKitkat";
    private static final int MAX_PRINT_SIZE = 3500;
    public static final int ORIENTATION_LANDSCAPE = 1;
    public static final int ORIENTATION_PORTRAIT = 2;
    public static final int SCALE_MODE_FILL = 2;
    public static final int SCALE_MODE_FIT = 1;
    int mColorMode;
    final Context mContext;
    BitmapFactory.Options mDecodeOptions = null;
    /* access modifiers changed from: private */
    public final Object mLock;
    int mOrientation;
    int mScaleMode;

    public interface OnPrintFinishCallback {
        void onFinish();
    }

    PrintHelperKitkat(Context context) {
        Object obj;
        new Object();
        this.mLock = obj;
        this.mScaleMode = 2;
        this.mColorMode = 2;
        this.mOrientation = 1;
        this.mContext = context;
    }

    public void setScaleMode(int i) {
        this.mScaleMode = i;
    }

    public int getScaleMode() {
        return this.mScaleMode;
    }

    public void setColorMode(int i) {
        this.mColorMode = i;
    }

    public void setOrientation(int i) {
        this.mOrientation = i;
    }

    public int getOrientation() {
        return this.mOrientation;
    }

    public int getColorMode() {
        return this.mColorMode;
    }

    public void printBitmap(String str, Bitmap bitmap, OnPrintFinishCallback onPrintFinishCallback) {
        PrintAttributes.Builder builder;
        PrintDocumentAdapter printDocumentAdapter;
        String str2 = str;
        Bitmap bitmap2 = bitmap;
        OnPrintFinishCallback onPrintFinishCallback2 = onPrintFinishCallback;
        if (bitmap2 != null) {
            int i = this.mScaleMode;
            PrintManager printManager = (PrintManager) this.mContext.getSystemService("print");
            PrintAttributes.MediaSize mediaSize = PrintAttributes.MediaSize.UNKNOWN_PORTRAIT;
            if (bitmap2.getWidth() > bitmap2.getHeight()) {
                mediaSize = PrintAttributes.MediaSize.UNKNOWN_LANDSCAPE;
            }
            new PrintAttributes.Builder();
            PrintAttributes build = builder.setMediaSize(mediaSize).setColorMode(this.mColorMode).build();
            final String str3 = str2;
            final Bitmap bitmap3 = bitmap2;
            final int i2 = i;
            final OnPrintFinishCallback onPrintFinishCallback3 = onPrintFinishCallback2;
            new PrintDocumentAdapter() {
                private PrintAttributes mAttributes;

                public void onLayout(PrintAttributes printAttributes, PrintAttributes printAttributes2, CancellationSignal cancellationSignal, PrintDocumentAdapter.LayoutResultCallback layoutResultCallback, Bundle bundle) {
                    PrintDocumentInfo.Builder builder;
                    PrintAttributes printAttributes3 = printAttributes2;
                    PrintDocumentAdapter.LayoutResultCallback layoutResultCallback2 = layoutResultCallback;
                    this.mAttributes = printAttributes3;
                    new PrintDocumentInfo.Builder(str3);
                    layoutResultCallback2.onLayoutFinished(builder.setContentType(1).setPageCount(1).build(), !printAttributes3.equals(printAttributes));
                }

                public void onWrite(PageRange[] pageRangeArr, ParcelFileDescriptor parcelFileDescriptor, CancellationSignal cancellationSignal, PrintDocumentAdapter.WriteResultCallback writeResultCallback) {
                    PrintedPdfDocument printedPdfDocument;
                    RectF rectF;
                    OutputStream outputStream;
                    ParcelFileDescriptor parcelFileDescriptor2 = parcelFileDescriptor;
                    PrintDocumentAdapter.WriteResultCallback writeResultCallback2 = writeResultCallback;
                    new PrintedPdfDocument(PrintHelperKitkat.this.mContext, this.mAttributes);
                    PrintedPdfDocument printedPdfDocument2 = printedPdfDocument;
                    try {
                        PdfDocument.Page startPage = printedPdfDocument2.startPage(1);
                        new RectF(startPage.getInfo().getContentRect());
                        startPage.getCanvas().drawBitmap(bitmap3, PrintHelperKitkat.this.getMatrix(bitmap3.getWidth(), bitmap3.getHeight(), rectF, i2), null);
                        printedPdfDocument2.finishPage(startPage);
                        PrintedPdfDocument printedPdfDocument3 = printedPdfDocument2;
                        new FileOutputStream(parcelFileDescriptor2.getFileDescriptor());
                        printedPdfDocument3.writeTo(outputStream);
                        writeResultCallback2.onWriteFinished(new PageRange[]{PageRange.ALL_PAGES});
                    } catch (IOException e) {
                        int e2 = Log.e(PrintHelperKitkat.LOG_TAG, "Error writing printed content", e);
                        writeResultCallback2.onWriteFailed(null);
                    } catch (Throwable th) {
                        Throwable th2 = th;
                        if (printedPdfDocument2 != null) {
                            printedPdfDocument2.close();
                        }
                        if (parcelFileDescriptor2 != null) {
                            try {
                                parcelFileDescriptor2.close();
                            } catch (IOException e3) {
                            }
                        }
                        throw th2;
                    }
                    if (printedPdfDocument2 != null) {
                        printedPdfDocument2.close();
                    }
                    if (parcelFileDescriptor2 != null) {
                        try {
                            parcelFileDescriptor2.close();
                        } catch (IOException e4) {
                        }
                    }
                }

                public void onFinish() {
                    if (onPrintFinishCallback3 != null) {
                        onPrintFinishCallback3.onFinish();
                    }
                }
            };
            PrintJob print = printManager.print(str2, printDocumentAdapter, build);
        }
    }

    /* access modifiers changed from: private */
    public Matrix getMatrix(int i, int i2, RectF rectF, int i3) {
        Matrix matrix;
        float min;
        int i4 = i;
        int i5 = i2;
        RectF rectF2 = rectF;
        new Matrix();
        Matrix matrix2 = matrix;
        float width = rectF2.width() / ((float) i4);
        if (i3 == 2) {
            min = Math.max(width, rectF2.height() / ((float) i5));
        } else {
            min = Math.min(width, rectF2.height() / ((float) i5));
        }
        boolean postScale = matrix2.postScale(min, min);
        boolean postTranslate = matrix2.postTranslate((rectF2.width() - (((float) i4) * min)) / 2.0f, (rectF2.height() - (((float) i5) * min)) / 2.0f);
        return matrix2;
    }

    public void printBitmap(String str, Uri uri, OnPrintFinishCallback onPrintFinishCallback) throws FileNotFoundException {
        PrintDocumentAdapter printDocumentAdapter;
        PrintAttributes.Builder builder;
        String str2 = str;
        final String str3 = str2;
        final Uri uri2 = uri;
        final OnPrintFinishCallback onPrintFinishCallback2 = onPrintFinishCallback;
        final int i = this.mScaleMode;
        new PrintDocumentAdapter() {
            private PrintAttributes mAttributes;
            Bitmap mBitmap = null;
            AsyncTask<Uri, Boolean, Bitmap> mLoadBitmap;

            public void onLayout(PrintAttributes printAttributes, PrintAttributes printAttributes2, CancellationSignal cancellationSignal, PrintDocumentAdapter.LayoutResultCallback layoutResultCallback, Bundle bundle) {
                AnonymousClass1 r16;
                PrintDocumentInfo.Builder builder;
                PrintAttributes printAttributes3 = printAttributes;
                PrintAttributes printAttributes4 = printAttributes2;
                CancellationSignal cancellationSignal2 = cancellationSignal;
                PrintDocumentAdapter.LayoutResultCallback layoutResultCallback2 = layoutResultCallback;
                this.mAttributes = printAttributes4;
                if (cancellationSignal2.isCanceled()) {
                    layoutResultCallback2.onLayoutCancelled();
                } else if (this.mBitmap != null) {
                    new PrintDocumentInfo.Builder(str3);
                    layoutResultCallback2.onLayoutFinished(builder.setContentType(1).setPageCount(1).build(), !printAttributes4.equals(printAttributes3));
                } else {
                    final CancellationSignal cancellationSignal3 = cancellationSignal2;
                    final PrintAttributes printAttributes5 = printAttributes4;
                    final PrintAttributes printAttributes6 = printAttributes3;
                    final PrintDocumentAdapter.LayoutResultCallback layoutResultCallback3 = layoutResultCallback2;
                    new AsyncTask<Uri, Boolean, Bitmap>() {
                        /* access modifiers changed from: protected */
                        public void onPreExecute() {
                            CancellationSignal.OnCancelListener onCancelListener;
                            new CancellationSignal.OnCancelListener() {
                                public void onCancel() {
                                    AnonymousClass2.this.cancelLoad();
                                    boolean cancel = AnonymousClass1.this.cancel(false);
                                }
                            };
                            cancellationSignal3.setOnCancelListener(onCancelListener);
                        }

                        /* access modifiers changed from: protected */
                        public Bitmap doInBackground(Uri... uriArr) {
                            try {
                                return PrintHelperKitkat.this.loadConstrainedBitmap(uri2, PrintHelperKitkat.MAX_PRINT_SIZE);
                            } catch (FileNotFoundException e) {
                                return null;
                            }
                        }

                        /* access modifiers changed from: protected */
                        public void onPostExecute(Bitmap bitmap) {
                            PrintDocumentInfo.Builder builder;
                            Bitmap bitmap2 = bitmap;
                            super.onPostExecute((Object) bitmap2);
                            AnonymousClass2.this.mBitmap = bitmap2;
                            if (bitmap2 != null) {
                                new PrintDocumentInfo.Builder(str3);
                                layoutResultCallback3.onLayoutFinished(builder.setContentType(1).setPageCount(1).build(), !printAttributes5.equals(printAttributes6));
                            } else {
                                layoutResultCallback3.onLayoutFailed(null);
                            }
                            AnonymousClass2.this.mLoadBitmap = null;
                        }

                        /* access modifiers changed from: protected */
                        public void onCancelled(Bitmap bitmap) {
                            layoutResultCallback3.onLayoutCancelled();
                            AnonymousClass2.this.mLoadBitmap = null;
                        }
                    };
                    this.mLoadBitmap = r16.execute(new Uri[0]);
                }
            }

            /* access modifiers changed from: private */
            public void cancelLoad() {
                Object access$300 = PrintHelperKitkat.this.mLock;
                Object obj = access$300;
                synchronized (access$300) {
                    try {
                        if (PrintHelperKitkat.this.mDecodeOptions != null) {
                            PrintHelperKitkat.this.mDecodeOptions.requestCancelDecode();
                            PrintHelperKitkat.this.mDecodeOptions = null;
                        }
                    } catch (Throwable th) {
                        Throwable th2 = th;
                        Object obj2 = obj;
                        throw th2;
                    }
                }
            }

            public void onFinish() {
                super.onFinish();
                cancelLoad();
                if (this.mLoadBitmap != null) {
                    boolean cancel = this.mLoadBitmap.cancel(true);
                }
                if (onPrintFinishCallback2 != null) {
                    onPrintFinishCallback2.onFinish();
                }
            }

            public void onWrite(PageRange[] pageRangeArr, ParcelFileDescriptor parcelFileDescriptor, CancellationSignal cancellationSignal, PrintDocumentAdapter.WriteResultCallback writeResultCallback) {
                PrintedPdfDocument printedPdfDocument;
                RectF rectF;
                OutputStream outputStream;
                ParcelFileDescriptor parcelFileDescriptor2 = parcelFileDescriptor;
                PrintDocumentAdapter.WriteResultCallback writeResultCallback2 = writeResultCallback;
                new PrintedPdfDocument(PrintHelperKitkat.this.mContext, this.mAttributes);
                PrintedPdfDocument printedPdfDocument2 = printedPdfDocument;
                try {
                    PdfDocument.Page startPage = printedPdfDocument2.startPage(1);
                    new RectF(startPage.getInfo().getContentRect());
                    startPage.getCanvas().drawBitmap(this.mBitmap, PrintHelperKitkat.this.getMatrix(this.mBitmap.getWidth(), this.mBitmap.getHeight(), rectF, i), null);
                    printedPdfDocument2.finishPage(startPage);
                    PrintedPdfDocument printedPdfDocument3 = printedPdfDocument2;
                    new FileOutputStream(parcelFileDescriptor2.getFileDescriptor());
                    printedPdfDocument3.writeTo(outputStream);
                    writeResultCallback2.onWriteFinished(new PageRange[]{PageRange.ALL_PAGES});
                } catch (IOException e) {
                    int e2 = Log.e(PrintHelperKitkat.LOG_TAG, "Error writing printed content", e);
                    writeResultCallback2.onWriteFailed(null);
                } catch (Throwable th) {
                    Throwable th2 = th;
                    if (printedPdfDocument2 != null) {
                        printedPdfDocument2.close();
                    }
                    if (parcelFileDescriptor2 != null) {
                        try {
                            parcelFileDescriptor2.close();
                        } catch (IOException e3) {
                        }
                    }
                    throw th2;
                }
                if (printedPdfDocument2 != null) {
                    printedPdfDocument2.close();
                }
                if (parcelFileDescriptor2 != null) {
                    try {
                        parcelFileDescriptor2.close();
                    } catch (IOException e4) {
                    }
                }
            }
        };
        PrintDocumentAdapter printDocumentAdapter2 = printDocumentAdapter;
        PrintManager printManager = (PrintManager) this.mContext.getSystemService("print");
        new PrintAttributes.Builder();
        PrintAttributes.Builder builder2 = builder;
        PrintAttributes.Builder colorMode = builder2.setColorMode(this.mColorMode);
        if (this.mOrientation == 1) {
            PrintAttributes.Builder mediaSize = builder2.setMediaSize(PrintAttributes.MediaSize.UNKNOWN_LANDSCAPE);
        } else if (this.mOrientation == 2) {
            PrintAttributes.Builder mediaSize2 = builder2.setMediaSize(PrintAttributes.MediaSize.UNKNOWN_PORTRAIT);
        }
        PrintJob print = printManager.print(str2, printDocumentAdapter2, builder2.build());
    }

    /* access modifiers changed from: private */
    public Bitmap loadConstrainedBitmap(Uri uri, int i) throws FileNotFoundException {
        Throwable th;
        BitmapFactory.Options options;
        int i2;
        Throwable th2;
        BitmapFactory.Options options2;
        Uri uri2 = uri;
        int i3 = i;
        if (i3 <= 0 || uri2 == null || this.mContext == null) {
            Throwable th3 = th;
            new IllegalArgumentException("bad argument to getScaledBitmap");
            throw th3;
        }
        new BitmapFactory.Options();
        BitmapFactory.Options options3 = options;
        options3.inJustDecodeBounds = true;
        Bitmap loadBitmap = loadBitmap(uri2, options3);
        int i4 = options3.outWidth;
        int i5 = options3.outHeight;
        if (i4 <= 0 || i5 <= 0) {
            return null;
        }
        int max = Math.max(i4, i5);
        int i6 = 1;
        while (true) {
            i2 = i6;
            if (max <= i3) {
                break;
            }
            max >>>= 1;
            i6 = i2 << 1;
        }
        if (i2 <= 0 || 0 >= Math.min(i4, i5) / i2) {
            return null;
        }
        Object obj = this.mLock;
        Object obj2 = obj;
        synchronized (obj) {
            try {
                new BitmapFactory.Options();
                this.mDecodeOptions = options2;
                this.mDecodeOptions.inMutable = true;
                this.mDecodeOptions.inSampleSize = i2;
                BitmapFactory.Options options4 = this.mDecodeOptions;
                try {
                    Bitmap loadBitmap2 = loadBitmap(uri2, options4);
                    Object obj3 = this.mLock;
                    obj2 = obj3;
                    synchronized (obj3) {
                        try {
                            this.mDecodeOptions = null;
                            return loadBitmap2;
                        } catch (Throwable th4) {
                            while (true) {
                                throw th2;
                            }
                        }
                    }
                } catch (Throwable th5) {
                    while (true) {
                        throw th5;
                    }
                }
            } finally {
                while (true) {
                    th2 = th4;
                    Object obj4 = obj2;
                    Throwable th6 = th2;
                }
            }
        }
    }

    /* JADX INFO: finally extract failed */
    private Bitmap loadBitmap(Uri uri, BitmapFactory.Options options) throws FileNotFoundException {
        Throwable th;
        Uri uri2 = uri;
        BitmapFactory.Options options2 = options;
        if (uri2 == null || this.mContext == null) {
            Throwable th2 = th;
            new IllegalArgumentException("bad argument to loadBitmap");
            throw th2;
        }
        InputStream inputStream = null;
        try {
            inputStream = this.mContext.getContentResolver().openInputStream(uri2);
            Bitmap decodeStream = BitmapFactory.decodeStream(inputStream, null, options2);
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    int w = Log.w(LOG_TAG, "close fail ", e);
                }
            }
            return decodeStream;
        } catch (Throwable th3) {
            Throwable th4 = th3;
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e2) {
                    int w2 = Log.w(LOG_TAG, "close fail ", e2);
                }
            }
            throw th4;
        }
    }
}
