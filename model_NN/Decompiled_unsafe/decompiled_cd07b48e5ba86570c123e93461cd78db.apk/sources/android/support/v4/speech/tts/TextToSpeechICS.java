package android.support.v4.speech.tts;

import android.content.Context;
import android.os.Build;
import android.speech.tts.TextToSpeech;
import android.util.Log;

class TextToSpeechICS {
    private static final String TAG = "android.support.v4.speech.tts";

    TextToSpeechICS() {
    }

    static TextToSpeech construct(Context context, TextToSpeech.OnInitListener onInitListener, String str) {
        TextToSpeech textToSpeech;
        TextToSpeech textToSpeech2;
        TextToSpeech textToSpeech3;
        Context context2 = context;
        TextToSpeech.OnInitListener onInitListener2 = onInitListener;
        String str2 = str;
        if (Build.VERSION.SDK_INT >= 14) {
            new TextToSpeech(context2, onInitListener2, str2);
            return textToSpeech;
        } else if (str2 == null) {
            new TextToSpeech(context2, onInitListener2);
            return textToSpeech3;
        } else {
            int w = Log.w(TAG, "Can't specify tts engine on this device");
            new TextToSpeech(context2, onInitListener2);
            return textToSpeech2;
        }
    }
}
