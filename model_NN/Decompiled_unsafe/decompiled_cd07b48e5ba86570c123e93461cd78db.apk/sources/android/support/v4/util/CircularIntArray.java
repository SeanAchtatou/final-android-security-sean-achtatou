package android.support.v4.util;

public final class CircularIntArray {
    private int mCapacityBitmask;
    private int[] mElements;
    private int mHead;
    private int mTail;

    private void doubleCapacity() {
        Throwable th;
        int length = this.mElements.length;
        int i = length - this.mHead;
        int i2 = length << 1;
        if (i2 < 0) {
            Throwable th2 = th;
            new RuntimeException("Max array capacity exceeded");
            throw th2;
        }
        int[] iArr = new int[i2];
        System.arraycopy(this.mElements, this.mHead, iArr, 0, i);
        System.arraycopy(this.mElements, 0, iArr, i, this.mHead);
        this.mElements = iArr;
        this.mHead = 0;
        this.mTail = length;
        this.mCapacityBitmask = i2 - 1;
    }

    public CircularIntArray() {
        this(8);
    }

    public CircularIntArray(int i) {
        Throwable th;
        int i2 = i;
        if (i2 <= 0) {
            Throwable th2 = th;
            new IllegalArgumentException("capacity must be positive");
            throw th2;
        }
        int highestOneBit = Integer.bitCount(i2) != 1 ? 1 << (Integer.highestOneBit(i2) + 1) : i2;
        this.mCapacityBitmask = highestOneBit - 1;
        this.mElements = new int[highestOneBit];
    }

    public void addFirst(int i) {
        this.mHead = (this.mHead - 1) & this.mCapacityBitmask;
        this.mElements[this.mHead] = i;
        if (this.mHead == this.mTail) {
            doubleCapacity();
        }
    }

    public void addLast(int i) {
        this.mElements[this.mTail] = i;
        this.mTail = (this.mTail + 1) & this.mCapacityBitmask;
        if (this.mTail == this.mHead) {
            doubleCapacity();
        }
    }

    public int popFirst() {
        Throwable th;
        if (this.mHead == this.mTail) {
            Throwable th2 = th;
            new ArrayIndexOutOfBoundsException();
            throw th2;
        }
        this.mHead = (this.mHead + 1) & this.mCapacityBitmask;
        return this.mElements[this.mHead];
    }

    public int popLast() {
        Throwable th;
        if (this.mHead == this.mTail) {
            Throwable th2 = th;
            new ArrayIndexOutOfBoundsException();
            throw th2;
        }
        int i = (this.mTail - 1) & this.mCapacityBitmask;
        this.mTail = i;
        return this.mElements[i];
    }

    public void clear() {
        this.mTail = this.mHead;
    }

    public void removeFromStart(int i) {
        Throwable th;
        int i2 = i;
        if (i2 > 0) {
            if (i2 > size()) {
                Throwable th2 = th;
                new ArrayIndexOutOfBoundsException();
                throw th2;
            }
            this.mHead = (this.mHead + i2) & this.mCapacityBitmask;
        }
    }

    public void removeFromEnd(int i) {
        Throwable th;
        int i2 = i;
        if (i2 > 0) {
            if (i2 > size()) {
                Throwable th2 = th;
                new ArrayIndexOutOfBoundsException();
                throw th2;
            }
            this.mTail = (this.mTail - i2) & this.mCapacityBitmask;
        }
    }

    public int getFirst() {
        Throwable th;
        if (this.mHead != this.mTail) {
            return this.mElements[this.mHead];
        }
        Throwable th2 = th;
        new ArrayIndexOutOfBoundsException();
        throw th2;
    }

    public int getLast() {
        Throwable th;
        if (this.mHead != this.mTail) {
            return this.mElements[(this.mTail - 1) & this.mCapacityBitmask];
        }
        Throwable th2 = th;
        new ArrayIndexOutOfBoundsException();
        throw th2;
    }

    public int get(int i) {
        Throwable th;
        int i2 = i;
        if (i2 >= 0 && i2 < size()) {
            return this.mElements[(this.mHead + i2) & this.mCapacityBitmask];
        }
        Throwable th2 = th;
        new ArrayIndexOutOfBoundsException();
        throw th2;
    }

    public int size() {
        return (this.mTail - this.mHead) & this.mCapacityBitmask;
    }

    public boolean isEmpty() {
        return this.mHead == this.mTail;
    }
}
