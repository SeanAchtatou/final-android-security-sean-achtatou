package android.support.v4.util;

import java.util.LinkedHashMap;
import java.util.Map;

public class LruCache<K, V> {
    private int createCount;
    private int evictionCount;
    private int hitCount;
    private final LinkedHashMap<K, V> map;
    private int maxSize;
    private int missCount;
    private int putCount;
    private int size;

    public LruCache(int i) {
        LinkedHashMap<K, V> linkedHashMap;
        Throwable th;
        int i2 = i;
        if (i2 <= 0) {
            Throwable th2 = th;
            new IllegalArgumentException("maxSize <= 0");
            throw th2;
        }
        this.maxSize = i2;
        new LinkedHashMap<>(0, 0.75f, true);
        this.map = linkedHashMap;
    }

    /* JADX INFO: finally extract failed */
    public void resize(int i) {
        Throwable th;
        int i2 = i;
        if (i2 <= 0) {
            Throwable th2 = th;
            new IllegalArgumentException("maxSize <= 0");
            throw th2;
        }
        synchronized (this) {
            try {
                this.maxSize = i2;
                trimToSize(i2);
            } catch (Throwable th3) {
                while (true) {
                    throw th3;
                }
            }
        }
    }

    /* JADX INFO: finally extract failed */
    public final V get(K k) {
        Throwable th;
        K k2 = k;
        if (k2 == null) {
            Throwable th2 = th;
            new NullPointerException("key == null");
            throw th2;
        }
        synchronized (this) {
            try {
                V v = this.map.get(k2);
                if (v != null) {
                    this.hitCount = this.hitCount + 1;
                    V v2 = v;
                    return v2;
                }
                this.missCount = this.missCount + 1;
                V create = create(k2);
                if (create == null) {
                    return null;
                }
                synchronized (this) {
                    try {
                        this.createCount = this.createCount + 1;
                        V put = this.map.put(k2, create);
                        if (put != null) {
                            V put2 = this.map.put(k2, put);
                        } else {
                            this.size = this.size + safeSizeOf(k2, create);
                        }
                        if (put != null) {
                            entryRemoved(false, k2, create, put);
                            return put;
                        }
                        trimToSize(this.maxSize);
                        return create;
                    } catch (Throwable th3) {
                        throw th3;
                    }
                }
            } catch (Throwable th4) {
                while (true) {
                    throw th4;
                }
            }
        }
    }

    /* JADX INFO: finally extract failed */
    public final V put(K k, V v) {
        Throwable th;
        K k2 = k;
        V v2 = v;
        if (k2 == null || v2 == null) {
            Throwable th2 = th;
            new NullPointerException("key == null || value == null");
            throw th2;
        }
        synchronized (this) {
            try {
                this.putCount = this.putCount + 1;
                this.size = this.size + safeSizeOf(k2, v2);
                V put = this.map.put(k2, v2);
                if (put != null) {
                    this.size = this.size - safeSizeOf(k2, put);
                }
                if (put != null) {
                    entryRemoved(false, k2, put, v2);
                }
                trimToSize(this.maxSize);
                return put;
            } catch (Throwable th3) {
                while (true) {
                    throw th3;
                }
            }
        }
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0059, code lost:
        monitor-exit(r7);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x005a, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void trimToSize(int r14) {
        /*
            r13 = this;
            r0 = r13
            r1 = r14
        L_0x0002:
            r7 = r0
            r12 = r7
            r7 = r12
            r8 = r12
            r4 = r8
            monitor-enter(r7)
            r7 = r0
            int r7 = r7.size     // Catch:{ all -> 0x0043 }
            if (r7 < 0) goto L_0x001b
            r7 = r0
            java.util.LinkedHashMap<K, V> r7 = r7.map     // Catch:{ all -> 0x0043 }
            boolean r7 = r7.isEmpty()     // Catch:{ all -> 0x0043 }
            if (r7 == 0) goto L_0x0049
            r7 = r0
            int r7 = r7.size     // Catch:{ all -> 0x0043 }
            if (r7 == 0) goto L_0x0049
        L_0x001b:
            java.lang.IllegalStateException r7 = new java.lang.IllegalStateException     // Catch:{ all -> 0x0043 }
            r12 = r7
            r7 = r12
            r8 = r12
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ all -> 0x0043 }
            r12 = r9
            r9 = r12
            r10 = r12
            r10.<init>()     // Catch:{ all -> 0x0043 }
            r10 = r0
            java.lang.Class r10 = r10.getClass()     // Catch:{ all -> 0x0043 }
            java.lang.String r10 = r10.getName()     // Catch:{ all -> 0x0043 }
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ all -> 0x0043 }
            java.lang.String r10 = ".sizeOf() is reporting inconsistent results!"
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ all -> 0x0043 }
            java.lang.String r9 = r9.toString()     // Catch:{ all -> 0x0043 }
            r8.<init>(r9)     // Catch:{ all -> 0x0043 }
            throw r7     // Catch:{ all -> 0x0043 }
        L_0x0043:
            r7 = move-exception
            r6 = r7
            r7 = r4
            monitor-exit(r7)     // Catch:{ all -> 0x0043 }
            r7 = r6
            throw r7
        L_0x0049:
            r7 = r0
            int r7 = r7.size     // Catch:{ all -> 0x0043 }
            r8 = r1
            if (r7 <= r8) goto L_0x0058
            r7 = r0
            java.util.LinkedHashMap<K, V> r7 = r7.map     // Catch:{ all -> 0x0043 }
            boolean r7 = r7.isEmpty()     // Catch:{ all -> 0x0043 }
            if (r7 == 0) goto L_0x005b
        L_0x0058:
            r7 = r4
            monitor-exit(r7)     // Catch:{ all -> 0x0043 }
            return
        L_0x005b:
            r7 = r0
            java.util.LinkedHashMap<K, V> r7 = r7.map     // Catch:{ all -> 0x0043 }
            java.util.Set r7 = r7.entrySet()     // Catch:{ all -> 0x0043 }
            java.util.Iterator r7 = r7.iterator()     // Catch:{ all -> 0x0043 }
            java.lang.Object r7 = r7.next()     // Catch:{ all -> 0x0043 }
            java.util.Map$Entry r7 = (java.util.Map.Entry) r7     // Catch:{ all -> 0x0043 }
            r5 = r7
            r7 = r5
            java.lang.Object r7 = r7.getKey()     // Catch:{ all -> 0x0043 }
            r2 = r7
            r7 = r5
            java.lang.Object r7 = r7.getValue()     // Catch:{ all -> 0x0043 }
            r3 = r7
            r7 = r0
            java.util.LinkedHashMap<K, V> r7 = r7.map     // Catch:{ all -> 0x0043 }
            r8 = r2
            java.lang.Object r7 = r7.remove(r8)     // Catch:{ all -> 0x0043 }
            r7 = r0
            r12 = r7
            r7 = r12
            r8 = r12
            int r8 = r8.size     // Catch:{ all -> 0x0043 }
            r9 = r0
            r10 = r2
            r11 = r3
            int r9 = r9.safeSizeOf(r10, r11)     // Catch:{ all -> 0x0043 }
            int r8 = r8 - r9
            r7.size = r8     // Catch:{ all -> 0x0043 }
            r7 = r0
            r12 = r7
            r7 = r12
            r8 = r12
            int r8 = r8.evictionCount     // Catch:{ all -> 0x0043 }
            r9 = 1
            int r8 = r8 + 1
            r7.evictionCount = r8     // Catch:{ all -> 0x0043 }
            r7 = r4
            monitor-exit(r7)     // Catch:{ all -> 0x0043 }
            r7 = r0
            r8 = 1
            r9 = r2
            r10 = r3
            r11 = 0
            r7.entryRemoved(r8, r9, r10, r11)
            goto L_0x0002
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.util.LruCache.trimToSize(int):void");
    }

    /* JADX INFO: finally extract failed */
    public final V remove(K k) {
        Throwable th;
        K k2 = k;
        if (k2 == null) {
            Throwable th2 = th;
            new NullPointerException("key == null");
            throw th2;
        }
        synchronized (this) {
            try {
                V remove = this.map.remove(k2);
                if (remove != null) {
                    this.size = this.size - safeSizeOf(k2, remove);
                }
                if (remove != null) {
                    entryRemoved(false, k2, remove, null);
                }
                return remove;
            } catch (Throwable th3) {
                while (true) {
                    throw th3;
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void entryRemoved(boolean z, K k, V v, V v2) {
    }

    /* access modifiers changed from: protected */
    public V create(K k) {
        return null;
    }

    private int safeSizeOf(K k, V v) {
        Throwable th;
        StringBuilder sb;
        K k2 = k;
        V v2 = v;
        int sizeOf = sizeOf(k2, v2);
        if (sizeOf >= 0) {
            return sizeOf;
        }
        Throwable th2 = th;
        new StringBuilder();
        new IllegalStateException(sb.append("Negative size: ").append((Object) k2).append("=").append((Object) v2).toString());
        throw th2;
    }

    /* access modifiers changed from: protected */
    public int sizeOf(K k, V v) {
        return 1;
    }

    public final void evictAll() {
        trimToSize(-1);
    }

    public final synchronized int size() {
        return this.size;
    }

    public final synchronized int maxSize() {
        return this.maxSize;
    }

    public final synchronized int hitCount() {
        return this.hitCount;
    }

    public final synchronized int missCount() {
        return this.missCount;
    }

    public final synchronized int createCount() {
        return this.createCount;
    }

    public final synchronized int putCount() {
        return this.putCount;
    }

    public final synchronized int evictionCount() {
        return this.evictionCount;
    }

    public final synchronized Map<K, V> snapshot() {
        Map<K, V> map2;
        Map<K, V> map3;
        map3 = map2;
        new LinkedHashMap(this.map);
        return map3;
    }

    public final synchronized String toString() {
        Object[] objArr;
        int i = this.hitCount + this.missCount;
        int i2 = i != 0 ? (100 * this.hitCount) / i : 0;
        Object[] objArr2 = new Object[4];
        objArr2[0] = Integer.valueOf(this.maxSize);
        Object[] objArr3 = objArr2;
        objArr3[1] = Integer.valueOf(this.hitCount);
        Object[] objArr4 = objArr3;
        objArr4[2] = Integer.valueOf(this.missCount);
        objArr = objArr4;
        objArr[3] = Integer.valueOf(i2);
        return String.format("LruCache[maxSize=%d,hits=%d,misses=%d,hitRate=%d%%]", objArr);
    }
}
