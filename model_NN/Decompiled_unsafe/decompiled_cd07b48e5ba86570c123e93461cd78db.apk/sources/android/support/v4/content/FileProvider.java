package android.support.v4.content;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.pm.ProviderInfo;
import android.content.res.XmlResourceParser;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.text.TextUtils;
import android.webkit.MimeTypeMap;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.xmlpull.v1.XmlPullParserException;

public class FileProvider extends ContentProvider {
    private static final String ATTR_NAME = "name";
    private static final String ATTR_PATH = "path";
    private static final String[] COLUMNS;
    private static final File DEVICE_ROOT;
    private static final String META_DATA_FILE_PROVIDER_PATHS = "android.support.FILE_PROVIDER_PATHS";
    private static final String TAG_CACHE_PATH = "cache-path";
    private static final String TAG_EXTERNAL = "external-path";
    private static final String TAG_FILES_PATH = "files-path";
    private static final String TAG_ROOT_PATH = "root-path";
    private static HashMap<String, PathStrategy> sCache;
    private PathStrategy mStrategy;

    interface PathStrategy {
        File getFileForUri(Uri uri);

        Uri getUriForFile(File file);
    }

    static {
        File file;
        HashMap<String, PathStrategy> hashMap;
        String[] strArr = new String[2];
        strArr[0] = "_display_name";
        String[] strArr2 = strArr;
        strArr2[1] = "_size";
        COLUMNS = strArr2;
        new File("/");
        DEVICE_ROOT = file;
        new HashMap<>();
        sCache = hashMap;
    }

    public boolean onCreate() {
        return true;
    }

    public void attachInfo(Context context, ProviderInfo providerInfo) {
        Throwable th;
        Throwable th2;
        Context context2 = context;
        ProviderInfo providerInfo2 = providerInfo;
        super.attachInfo(context2, providerInfo2);
        if (providerInfo2.exported) {
            Throwable th3 = th2;
            new SecurityException("Provider must not be exported");
            throw th3;
        } else if (!providerInfo2.grantUriPermissions) {
            Throwable th4 = th;
            new SecurityException("Provider must grant uri permissions");
            throw th4;
        } else {
            this.mStrategy = getPathStrategy(context2, providerInfo2.authority);
        }
    }

    public static Uri getUriForFile(Context context, String str, File file) {
        return getPathStrategy(context, str).getUriForFile(file);
    }

    public Cursor query(Uri uri, String[] strArr, String str, String[] strArr2, String str2) {
        MatrixCursor matrixCursor;
        String[] strArr3 = strArr;
        File fileForUri = this.mStrategy.getFileForUri(uri);
        if (strArr3 == null) {
            strArr3 = COLUMNS;
        }
        String[] strArr4 = new String[strArr3.length];
        Object[] objArr = new Object[strArr3.length];
        int i = 0;
        String[] strArr5 = strArr3;
        int length = strArr5.length;
        for (int i2 = 0; i2 < length; i2++) {
            String str3 = strArr5[i2];
            if ("_display_name".equals(str3)) {
                strArr4[i] = "_display_name";
                int i3 = i;
                i++;
                objArr[i3] = fileForUri.getName();
            } else if ("_size".equals(str3)) {
                strArr4[i] = "_size";
                int i4 = i;
                i++;
                objArr[i4] = Long.valueOf(fileForUri.length());
            }
        }
        String[] copyOf = copyOf(strArr4, i);
        Object[] copyOf2 = copyOf(objArr, i);
        new MatrixCursor(copyOf, 1);
        MatrixCursor matrixCursor2 = matrixCursor;
        matrixCursor2.addRow(copyOf2);
        return matrixCursor2;
    }

    public String getType(Uri uri) {
        String mimeTypeFromExtension;
        File fileForUri = this.mStrategy.getFileForUri(uri);
        int lastIndexOf = fileForUri.getName().lastIndexOf(46);
        if (lastIndexOf < 0 || (mimeTypeFromExtension = MimeTypeMap.getSingleton().getMimeTypeFromExtension(fileForUri.getName().substring(lastIndexOf + 1))) == null) {
            return "application/octet-stream";
        }
        return mimeTypeFromExtension;
    }

    public Uri insert(Uri uri, ContentValues contentValues) {
        Throwable th;
        Throwable th2 = th;
        new UnsupportedOperationException("No external inserts");
        throw th2;
    }

    public int update(Uri uri, ContentValues contentValues, String str, String[] strArr) {
        Throwable th;
        Throwable th2 = th;
        new UnsupportedOperationException("No external updates");
        throw th2;
    }

    public int delete(Uri uri, String str, String[] strArr) {
        return this.mStrategy.getFileForUri(uri).delete() ? 1 : 0;
    }

    public ParcelFileDescriptor openFile(Uri uri, String str) throws FileNotFoundException {
        return ParcelFileDescriptor.open(this.mStrategy.getFileForUri(uri), modeToMode(str));
    }

    private static PathStrategy getPathStrategy(Context context, String str) {
        Throwable th;
        Throwable th2;
        Context context2 = context;
        String str2 = str;
        HashMap<String, PathStrategy> hashMap = sCache;
        HashMap<String, PathStrategy> hashMap2 = hashMap;
        synchronized (hashMap) {
            try {
                PathStrategy pathStrategy = sCache.get(str2);
                if (pathStrategy == null) {
                    pathStrategy = parsePathStrategy(context2, str2);
                    PathStrategy put = sCache.put(str2, pathStrategy);
                }
                return pathStrategy;
            } catch (IOException e) {
                IOException iOException = e;
                Throwable th3 = th2;
                new IllegalArgumentException("Failed to parse android.support.FILE_PROVIDER_PATHS meta-data", iOException);
                throw th3;
            } catch (XmlPullParserException e2) {
                XmlPullParserException xmlPullParserException = e2;
                Throwable th4 = th;
                new IllegalArgumentException("Failed to parse android.support.FILE_PROVIDER_PATHS meta-data", xmlPullParserException);
                throw th4;
            } catch (Throwable th5) {
                Throwable th6 = th5;
                HashMap<String, PathStrategy> hashMap3 = hashMap2;
                throw th6;
            }
        }
    }

    private static PathStrategy parsePathStrategy(Context context, String str) throws IOException, XmlPullParserException {
        SimplePathStrategy simplePathStrategy;
        Throwable th;
        Context context2 = context;
        String str2 = str;
        new SimplePathStrategy(str2);
        SimplePathStrategy simplePathStrategy2 = simplePathStrategy;
        XmlResourceParser loadXmlMetaData = context2.getPackageManager().resolveContentProvider(str2, 128).loadXmlMetaData(context2.getPackageManager(), META_DATA_FILE_PROVIDER_PATHS);
        if (loadXmlMetaData == null) {
            Throwable th2 = th;
            new IllegalArgumentException("Missing android.support.FILE_PROVIDER_PATHS meta-data");
            throw th2;
        }
        while (true) {
            int next = loadXmlMetaData.next();
            int i = next;
            if (next == 1) {
                return simplePathStrategy2;
            }
            if (i == 2) {
                String name = loadXmlMetaData.getName();
                String attributeValue = loadXmlMetaData.getAttributeValue(null, ATTR_NAME);
                String attributeValue2 = loadXmlMetaData.getAttributeValue(null, ATTR_PATH);
                File file = null;
                if (TAG_ROOT_PATH.equals(name)) {
                    file = buildPath(DEVICE_ROOT, attributeValue2);
                } else if (TAG_FILES_PATH.equals(name)) {
                    file = buildPath(context2.getFilesDir(), attributeValue2);
                } else if (TAG_CACHE_PATH.equals(name)) {
                    file = buildPath(context2.getCacheDir(), attributeValue2);
                } else if (TAG_EXTERNAL.equals(name)) {
                    file = buildPath(Environment.getExternalStorageDirectory(), attributeValue2);
                }
                if (file != null) {
                    simplePathStrategy2.addRoot(attributeValue, file);
                }
            }
        }
    }

    static class SimplePathStrategy implements PathStrategy {
        private final String mAuthority;
        private final HashMap<String, File> mRoots;

        public SimplePathStrategy(String str) {
            HashMap<String, File> hashMap;
            new HashMap<>();
            this.mRoots = hashMap;
            this.mAuthority = str;
        }

        public void addRoot(String str, File file) {
            Throwable th;
            StringBuilder sb;
            Throwable th2;
            String str2 = str;
            File file2 = file;
            if (TextUtils.isEmpty(str2)) {
                Throwable th3 = th2;
                new IllegalArgumentException("Name must not be empty");
                throw th3;
            }
            try {
                File put = this.mRoots.put(str2, file2.getCanonicalFile());
            } catch (IOException e) {
                IOException iOException = e;
                Throwable th4 = th;
                new StringBuilder();
                new IllegalArgumentException(sb.append("Failed to resolve canonical path for ").append(file2).toString(), iOException);
                throw th4;
            }
        }

        public Uri getUriForFile(File file) {
            Throwable th;
            StringBuilder sb;
            String substring;
            StringBuilder sb2;
            Uri.Builder builder;
            Throwable th2;
            StringBuilder sb3;
            File file2 = file;
            try {
                String canonicalPath = file2.getCanonicalPath();
                Map.Entry entry = null;
                for (Map.Entry next : this.mRoots.entrySet()) {
                    String path = ((File) next.getValue()).getPath();
                    if (canonicalPath.startsWith(path) && (entry == null || path.length() > ((File) entry.getValue()).getPath().length())) {
                        entry = next;
                    }
                }
                if (entry == null) {
                    Throwable th3 = th2;
                    new StringBuilder();
                    new IllegalArgumentException(sb3.append("Failed to find configured root that contains ").append(canonicalPath).toString());
                    throw th3;
                }
                String path2 = ((File) entry.getValue()).getPath();
                if (path2.endsWith("/")) {
                    substring = canonicalPath.substring(path2.length());
                } else {
                    substring = canonicalPath.substring(path2.length() + 1);
                }
                new StringBuilder();
                String sb4 = sb2.append(Uri.encode((String) entry.getKey())).append('/').append(Uri.encode(substring, "/")).toString();
                new Uri.Builder();
                return builder.scheme("content").authority(this.mAuthority).encodedPath(sb4).build();
            } catch (IOException e) {
                Throwable th4 = th;
                new StringBuilder();
                new IllegalArgumentException(sb.append("Failed to resolve canonical path for ").append(file2).toString());
                throw th4;
            }
        }

        public File getFileForUri(Uri uri) {
            File file;
            Throwable th;
            StringBuilder sb;
            Throwable th2;
            Throwable th3;
            StringBuilder sb2;
            Uri uri2 = uri;
            String encodedPath = uri2.getEncodedPath();
            int indexOf = encodedPath.indexOf(47, 1);
            String decode = Uri.decode(encodedPath.substring(1, indexOf));
            String decode2 = Uri.decode(encodedPath.substring(indexOf + 1));
            File file2 = this.mRoots.get(decode);
            if (file2 == null) {
                Throwable th4 = th3;
                new StringBuilder();
                new IllegalArgumentException(sb2.append("Unable to find configured root for ").append(uri2).toString());
                throw th4;
            }
            new File(file2, decode2);
            File file3 = file;
            try {
                File canonicalFile = file3.getCanonicalFile();
                if (canonicalFile.getPath().startsWith(file2.getPath())) {
                    return canonicalFile;
                }
                Throwable th5 = th2;
                new SecurityException("Resolved path jumped beyond configured root");
                throw th5;
            } catch (IOException e) {
                Throwable th6 = th;
                new StringBuilder();
                new IllegalArgumentException(sb.append("Failed to resolve canonical path for ").append(file3).toString());
                throw th6;
            }
        }
    }

    private static int modeToMode(String str) {
        int i;
        Throwable th;
        StringBuilder sb;
        String str2 = str;
        if ("r".equals(str2)) {
            i = 268435456;
        } else if ("w".equals(str2) || "wt".equals(str2)) {
            i = 738197504;
        } else if ("wa".equals(str2)) {
            i = 704643072;
        } else if ("rw".equals(str2)) {
            i = 939524096;
        } else if ("rwt".equals(str2)) {
            i = 1006632960;
        } else {
            Throwable th2 = th;
            new StringBuilder();
            new IllegalArgumentException(sb.append("Invalid mode: ").append(str2).toString());
            throw th2;
        }
        return i;
    }

    private static File buildPath(File file, String... strArr) {
        File file2;
        File file3 = file;
        String[] strArr2 = strArr;
        int length = strArr2.length;
        for (int i = 0; i < length; i++) {
            String str = strArr2[i];
            if (str != null) {
                new File(file3, str);
                file3 = file2;
            }
        }
        return file3;
    }

    private static String[] copyOf(String[] strArr, int i) {
        int i2 = i;
        String[] strArr2 = new String[i2];
        System.arraycopy(strArr, 0, strArr2, 0, i2);
        return strArr2;
    }

    private static Object[] copyOf(Object[] objArr, int i) {
        int i2 = i;
        Object[] objArr2 = new Object[i2];
        System.arraycopy(objArr, 0, objArr2, 0, i2);
        return objArr2;
    }
}
