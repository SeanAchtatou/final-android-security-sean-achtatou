package android.support.v4.content;

import android.content.Context;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.util.TimeUtils;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.concurrent.CountDownLatch;

public abstract class AsyncTaskLoader<D> extends Loader<D> {
    static final boolean DEBUG = false;
    static final String TAG = "AsyncTaskLoader";
    volatile AsyncTaskLoader<D>.LoadTask mCancellingTask;
    Handler mHandler;
    long mLastLoadCompleteTime = -10000;
    volatile AsyncTaskLoader<D>.LoadTask mTask;
    long mUpdateThrottle;

    public abstract D loadInBackground();

    final class LoadTask extends ModernAsyncTask<Void, Void, D> implements Runnable {
        /* access modifiers changed from: private */
        public CountDownLatch done;
        D result;
        boolean waiting;

        LoadTask() {
            CountDownLatch countDownLatch;
            new CountDownLatch(1);
            this.done = countDownLatch;
        }

        /* access modifiers changed from: protected */
        public D doInBackground(Void... voidArr) {
            this.result = AsyncTaskLoader.this.onLoadInBackground();
            return this.result;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(D d) {
            try {
                AsyncTaskLoader.this.dispatchOnLoadComplete(this, d);
                this.done.countDown();
            } catch (Throwable th) {
                Throwable th2 = th;
                this.done.countDown();
                throw th2;
            }
        }

        /* access modifiers changed from: protected */
        public void onCancelled() {
            try {
                AsyncTaskLoader.this.dispatchOnCancelled(this, this.result);
                this.done.countDown();
            } catch (Throwable th) {
                Throwable th2 = th;
                this.done.countDown();
                throw th2;
            }
        }

        public void run() {
            this.waiting = AsyncTaskLoader.DEBUG;
            AsyncTaskLoader.this.executePendingTask();
        }
    }

    public AsyncTaskLoader(Context context) {
        super(context);
    }

    public void setUpdateThrottle(long j) {
        Handler handler;
        long j2 = j;
        this.mUpdateThrottle = j2;
        if (j2 != 0) {
            new Handler();
            this.mHandler = handler;
        }
    }

    /* access modifiers changed from: protected */
    public void onForceLoad() {
        AsyncTaskLoader<D>.LoadTask loadTask;
        super.onForceLoad();
        boolean cancelLoad = cancelLoad();
        new LoadTask();
        this.mTask = loadTask;
        executePendingTask();
    }

    public boolean cancelLoad() {
        if (this.mTask == null) {
            return DEBUG;
        }
        if (this.mCancellingTask != null) {
            if (this.mTask.waiting) {
                this.mTask.waiting = DEBUG;
                this.mHandler.removeCallbacks(this.mTask);
            }
            this.mTask = null;
            return DEBUG;
        } else if (this.mTask.waiting) {
            this.mTask.waiting = DEBUG;
            this.mHandler.removeCallbacks(this.mTask);
            this.mTask = null;
            return DEBUG;
        } else {
            boolean cancel = this.mTask.cancel(DEBUG);
            if (cancel) {
                this.mCancellingTask = this.mTask;
            }
            this.mTask = null;
            return cancel;
        }
    }

    public void onCanceled(D d) {
    }

    /* access modifiers changed from: package-private */
    public void executePendingTask() {
        if (this.mCancellingTask == null && this.mTask != null) {
            if (this.mTask.waiting) {
                this.mTask.waiting = DEBUG;
                this.mHandler.removeCallbacks(this.mTask);
            }
            if (this.mUpdateThrottle <= 0 || SystemClock.uptimeMillis() >= this.mLastLoadCompleteTime + this.mUpdateThrottle) {
                ModernAsyncTask executeOnExecutor = this.mTask.executeOnExecutor(ModernAsyncTask.THREAD_POOL_EXECUTOR, null);
                return;
            }
            this.mTask.waiting = true;
            boolean postAtTime = this.mHandler.postAtTime(this.mTask, this.mLastLoadCompleteTime + this.mUpdateThrottle);
        }
    }

    /* access modifiers changed from: package-private */
    public void dispatchOnCancelled(AsyncTaskLoader<D>.LoadTask loadTask, D d) {
        onCanceled(d);
        if (this.mCancellingTask == loadTask) {
            rollbackContentChanged();
            this.mLastLoadCompleteTime = SystemClock.uptimeMillis();
            this.mCancellingTask = null;
            executePendingTask();
        }
    }

    /* access modifiers changed from: package-private */
    public void dispatchOnLoadComplete(AsyncTaskLoader<D>.LoadTask loadTask, D d) {
        AsyncTaskLoader<D>.LoadTask loadTask2 = loadTask;
        D d2 = d;
        if (this.mTask != loadTask2) {
            dispatchOnCancelled(loadTask2, d2);
        } else if (isAbandoned()) {
            onCanceled(d2);
        } else {
            commitContentChanged();
            this.mLastLoadCompleteTime = SystemClock.uptimeMillis();
            this.mTask = null;
            deliverResult(d2);
        }
    }

    /* access modifiers changed from: protected */
    public D onLoadInBackground() {
        return loadInBackground();
    }

    public void waitForLoader() {
        AsyncTaskLoader<D>.LoadTask loadTask = this.mTask;
        if (loadTask != null) {
            try {
                loadTask.done.await();
            } catch (InterruptedException e) {
            }
        }
    }

    public void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        String str2 = str;
        PrintWriter printWriter2 = printWriter;
        super.dump(str2, fileDescriptor, printWriter2, strArr);
        if (this.mTask != null) {
            printWriter2.print(str2);
            printWriter2.print("mTask=");
            printWriter2.print(this.mTask);
            printWriter2.print(" waiting=");
            printWriter2.println(this.mTask.waiting);
        }
        if (this.mCancellingTask != null) {
            printWriter2.print(str2);
            printWriter2.print("mCancellingTask=");
            printWriter2.print(this.mCancellingTask);
            printWriter2.print(" waiting=");
            printWriter2.println(this.mCancellingTask.waiting);
        }
        if (this.mUpdateThrottle != 0) {
            printWriter2.print(str2);
            printWriter2.print("mUpdateThrottle=");
            TimeUtils.formatDuration(this.mUpdateThrottle, printWriter2);
            printWriter2.print(" mLastLoadCompleteTime=");
            TimeUtils.formatDuration(this.mLastLoadCompleteTime, SystemClock.uptimeMillis(), printWriter2);
            printWriter2.println();
        }
    }
}
