package android.support.v4.content.res;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build;

public class ResourcesCompat {
    public static Drawable getDrawable(Resources resources, int i, Resources.Theme theme) throws Resources.NotFoundException {
        Resources resources2 = resources;
        int i2 = i;
        Resources.Theme theme2 = theme;
        if (Build.VERSION.SDK_INT >= 21) {
            return ResourcesCompatApi21.getDrawable(resources2, i2, theme2);
        }
        return resources2.getDrawable(i2);
    }

    public static Drawable getDrawableForDensity(Resources resources, int i, int i2, Resources.Theme theme) throws Resources.NotFoundException {
        Resources resources2 = resources;
        int i3 = i;
        int i4 = i2;
        Resources.Theme theme2 = theme;
        int i5 = Build.VERSION.SDK_INT;
        if (i5 >= 21) {
            return ResourcesCompatApi21.getDrawableForDensity(resources2, i3, i4, theme2);
        }
        if (i5 >= 15) {
            return ResourcesCompatIcsMr1.getDrawableForDensity(resources2, i3, i4);
        }
        return resources2.getDrawable(i3);
    }
}
