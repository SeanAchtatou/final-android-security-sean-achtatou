package android.support.v4.widget;

import android.database.Cursor;
import android.widget.Filter;

class CursorFilter extends Filter {
    CursorFilterClient mClient;

    interface CursorFilterClient {
        void changeCursor(Cursor cursor);

        CharSequence convertToString(Cursor cursor);

        Cursor getCursor();

        Cursor runQueryOnBackgroundThread(CharSequence charSequence);
    }

    CursorFilter(CursorFilterClient cursorFilterClient) {
        this.mClient = cursorFilterClient;
    }

    public CharSequence convertResultToString(Object obj) {
        return this.mClient.convertToString((Cursor) obj);
    }

    /* access modifiers changed from: protected */
    public Filter.FilterResults performFiltering(CharSequence charSequence) {
        Filter.FilterResults filterResults;
        Cursor runQueryOnBackgroundThread = this.mClient.runQueryOnBackgroundThread(charSequence);
        new Filter.FilterResults();
        Filter.FilterResults filterResults2 = filterResults;
        if (runQueryOnBackgroundThread != null) {
            filterResults2.count = runQueryOnBackgroundThread.getCount();
            filterResults2.values = runQueryOnBackgroundThread;
        } else {
            filterResults2.count = 0;
            filterResults2.values = null;
        }
        return filterResults2;
    }

    /* access modifiers changed from: protected */
    public void publishResults(CharSequence charSequence, Filter.FilterResults filterResults) {
        Filter.FilterResults filterResults2 = filterResults;
        Cursor cursor = this.mClient.getCursor();
        if (filterResults2.values != null && filterResults2.values != cursor) {
            this.mClient.changeCursor((Cursor) filterResults2.values);
        }
    }
}
