package android.support.v4.widget;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public abstract class ResourceCursorAdapter extends CursorAdapter {
    private int mDropDownLayout;
    private LayoutInflater mInflater;
    private int mLayout;

    /* JADX WARNING: Illegal instructions before constructor call */
    @java.lang.Deprecated
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public ResourceCursorAdapter(android.content.Context r11, int r12, android.database.Cursor r13) {
        /*
            r10 = this;
            r0 = r10
            r1 = r11
            r2 = r12
            r3 = r13
            r4 = r0
            r5 = r1
            r6 = r3
            r4.<init>(r5, r6)
            r4 = r0
            r5 = r0
            r6 = r2
            r8 = r5
            r9 = r6
            r5 = r9
            r6 = r8
            r7 = r9
            r6.mDropDownLayout = r7
            r4.mLayout = r5
            r4 = r0
            r5 = r1
            java.lang.String r6 = "layout_inflater"
            java.lang.Object r5 = r5.getSystemService(r6)
            android.view.LayoutInflater r5 = (android.view.LayoutInflater) r5
            r4.mInflater = r5
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.widget.ResourceCursorAdapter.<init>(android.content.Context, int, android.database.Cursor):void");
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public ResourceCursorAdapter(android.content.Context r12, int r13, android.database.Cursor r14, boolean r15) {
        /*
            r11 = this;
            r0 = r11
            r1 = r12
            r2 = r13
            r3 = r14
            r4 = r15
            r5 = r0
            r6 = r1
            r7 = r3
            r8 = r4
            r5.<init>(r6, r7, r8)
            r5 = r0
            r6 = r0
            r7 = r2
            r9 = r6
            r10 = r7
            r6 = r10
            r7 = r9
            r8 = r10
            r7.mDropDownLayout = r8
            r5.mLayout = r6
            r5 = r0
            r6 = r1
            java.lang.String r7 = "layout_inflater"
            java.lang.Object r6 = r6.getSystemService(r7)
            android.view.LayoutInflater r6 = (android.view.LayoutInflater) r6
            r5.mInflater = r6
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.widget.ResourceCursorAdapter.<init>(android.content.Context, int, android.database.Cursor, boolean):void");
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public ResourceCursorAdapter(android.content.Context r12, int r13, android.database.Cursor r14, int r15) {
        /*
            r11 = this;
            r0 = r11
            r1 = r12
            r2 = r13
            r3 = r14
            r4 = r15
            r5 = r0
            r6 = r1
            r7 = r3
            r8 = r4
            r5.<init>(r6, r7, r8)
            r5 = r0
            r6 = r0
            r7 = r2
            r9 = r6
            r10 = r7
            r6 = r10
            r7 = r9
            r8 = r10
            r7.mDropDownLayout = r8
            r5.mLayout = r6
            r5 = r0
            r6 = r1
            java.lang.String r7 = "layout_inflater"
            java.lang.Object r6 = r6.getSystemService(r7)
            android.view.LayoutInflater r6 = (android.view.LayoutInflater) r6
            r5.mInflater = r6
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.widget.ResourceCursorAdapter.<init>(android.content.Context, int, android.database.Cursor, int):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        return this.mInflater.inflate(this.mLayout, viewGroup, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View newDropDownView(Context context, Cursor cursor, ViewGroup viewGroup) {
        return this.mInflater.inflate(this.mDropDownLayout, viewGroup, false);
    }

    public void setViewResource(int i) {
        this.mLayout = i;
    }

    public void setDropDownViewResource(int i) {
        this.mDropDownLayout = i;
    }
}
