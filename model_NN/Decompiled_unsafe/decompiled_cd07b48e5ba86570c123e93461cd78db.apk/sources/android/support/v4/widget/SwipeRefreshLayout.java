package android.support.v4.widget;

import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.ViewCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Transformation;
import android.widget.AbsListView;

public class SwipeRefreshLayout extends ViewGroup {
    private static final int ALPHA_ANIMATION_DURATION = 300;
    private static final int ANIMATE_TO_START_DURATION = 200;
    private static final int ANIMATE_TO_TRIGGER_DURATION = 200;
    private static final int CIRCLE_BG_LIGHT = -328966;
    private static final int CIRCLE_DIAMETER = 40;
    private static final int CIRCLE_DIAMETER_LARGE = 56;
    private static final float DECELERATE_INTERPOLATION_FACTOR = 2.0f;
    public static final int DEFAULT = 1;
    private static final int DEFAULT_CIRCLE_TARGET = 64;
    private static final float DRAG_RATE = 0.5f;
    private static final int INVALID_POINTER = -1;
    public static final int LARGE = 0;
    private static final int[] LAYOUT_ATTRS = {16842766};
    private static final String LOG_TAG = SwipeRefreshLayout.class.getSimpleName();
    private static final int MAX_ALPHA = 255;
    private static final float MAX_PROGRESS_ANGLE = 0.8f;
    private static final int SCALE_DOWN_DURATION = 150;
    private static final int STARTING_PROGRESS_ALPHA = 76;
    private int mActivePointerId;
    private Animation mAlphaMaxAnimation;
    private Animation mAlphaStartAnimation;
    private final Animation mAnimateToCorrectPosition;
    private final Animation mAnimateToStartPosition;
    private int mCircleHeight;
    /* access modifiers changed from: private */
    public CircleImageView mCircleView;
    private int mCircleViewIndex;
    private int mCircleWidth;
    /* access modifiers changed from: private */
    public int mCurrentTargetOffsetTop;
    private final DecelerateInterpolator mDecelerateInterpolator;
    protected int mFrom;
    private float mInitialDownY;
    private float mInitialMotionY;
    private boolean mIsBeingDragged;
    /* access modifiers changed from: private */
    public OnRefreshListener mListener;
    private int mMediumAnimationDuration;
    /* access modifiers changed from: private */
    public boolean mNotify;
    private boolean mOriginalOffsetCalculated;
    protected int mOriginalOffsetTop;
    /* access modifiers changed from: private */
    public MaterialProgressDrawable mProgress;
    private Animation.AnimationListener mRefreshListener;
    /* access modifiers changed from: private */
    public boolean mRefreshing;
    private boolean mReturningToStart;
    /* access modifiers changed from: private */
    public boolean mScale;
    private Animation mScaleAnimation;
    private Animation mScaleDownAnimation;
    private Animation mScaleDownToStartAnimation;
    /* access modifiers changed from: private */
    public float mSpinnerFinalOffset;
    /* access modifiers changed from: private */
    public float mStartingScale;
    private View mTarget;
    private float mTotalDragDistance;
    private int mTouchSlop;
    /* access modifiers changed from: private */
    public boolean mUsingCustomStart;

    public interface OnRefreshListener {
        void onRefresh();
    }

    static /* synthetic */ int access$802(SwipeRefreshLayout swipeRefreshLayout, int i) {
        int i2 = i;
        int i3 = i2;
        swipeRefreshLayout.mCurrentTargetOffsetTop = i3;
        return i2;
    }

    /* access modifiers changed from: private */
    public void setColorViewAlpha(int i) {
        int i2 = i;
        this.mCircleView.getBackground().setAlpha(i2);
        this.mProgress.setAlpha(i2);
    }

    public void setProgressViewOffset(boolean z, int i, int i2) {
        this.mScale = z;
        this.mCircleView.setVisibility(8);
        int i3 = i;
        this.mCurrentTargetOffsetTop = i3;
        this.mOriginalOffsetTop = i3;
        this.mSpinnerFinalOffset = (float) i2;
        this.mUsingCustomStart = true;
        this.mCircleView.invalidate();
    }

    public void setProgressViewEndTarget(boolean z, int i) {
        this.mSpinnerFinalOffset = (float) i;
        this.mScale = z;
        this.mCircleView.invalidate();
    }

    public void setSize(int i) {
        int i2 = i;
        if (i2 == 0 || i2 == 1) {
            DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
            if (i2 == 0) {
                int i3 = (int) (56.0f * displayMetrics.density);
                this.mCircleWidth = i3;
                this.mCircleHeight = i3;
            } else {
                int i4 = (int) (40.0f * displayMetrics.density);
                this.mCircleWidth = i4;
                this.mCircleHeight = i4;
            }
            this.mCircleView.setImageDrawable(null);
            this.mProgress.updateSizes(i2);
            this.mCircleView.setImageDrawable(this.mProgress);
        }
    }

    public SwipeRefreshLayout(Context context) {
        this(context, null);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public SwipeRefreshLayout(android.content.Context r11, android.util.AttributeSet r12) {
        /*
            r10 = this;
            r0 = r10
            r1 = r11
            r2 = r12
            r5 = r0
            r6 = r1
            r7 = r2
            r5.<init>(r6, r7)
            r5 = r0
            r6 = 0
            r5.mRefreshing = r6
            r5 = r0
            r6 = -1082130432(0xffffffffbf800000, float:-1.0)
            r5.mTotalDragDistance = r6
            r5 = r0
            r6 = 0
            r5.mOriginalOffsetCalculated = r6
            r5 = r0
            r6 = -1
            r5.mActivePointerId = r6
            r5 = r0
            r6 = -1
            r5.mCircleViewIndex = r6
            r5 = r0
            android.support.v4.widget.SwipeRefreshLayout$1 r6 = new android.support.v4.widget.SwipeRefreshLayout$1
            r9 = r6
            r6 = r9
            r7 = r9
            r8 = r0
            r7.<init>()
            r5.mRefreshListener = r6
            r5 = r0
            android.support.v4.widget.SwipeRefreshLayout$6 r6 = new android.support.v4.widget.SwipeRefreshLayout$6
            r9 = r6
            r6 = r9
            r7 = r9
            r8 = r0
            r7.<init>()
            r5.mAnimateToCorrectPosition = r6
            r5 = r0
            android.support.v4.widget.SwipeRefreshLayout$7 r6 = new android.support.v4.widget.SwipeRefreshLayout$7
            r9 = r6
            r6 = r9
            r7 = r9
            r8 = r0
            r7.<init>()
            r5.mAnimateToStartPosition = r6
            r5 = r0
            r6 = r1
            android.view.ViewConfiguration r6 = android.view.ViewConfiguration.get(r6)
            int r6 = r6.getScaledTouchSlop()
            r5.mTouchSlop = r6
            r5 = r0
            r6 = r0
            android.content.res.Resources r6 = r6.getResources()
            r7 = 17694721(0x10e0001, float:2.6081284E-38)
            int r6 = r6.getInteger(r7)
            r5.mMediumAnimationDuration = r6
            r5 = r0
            r6 = 0
            r5.setWillNotDraw(r6)
            r5 = r0
            android.view.animation.DecelerateInterpolator r6 = new android.view.animation.DecelerateInterpolator
            r9 = r6
            r6 = r9
            r7 = r9
            r8 = 1073741824(0x40000000, float:2.0)
            r7.<init>(r8)
            r5.mDecelerateInterpolator = r6
            r5 = r1
            r6 = r2
            int[] r7 = android.support.v4.widget.SwipeRefreshLayout.LAYOUT_ATTRS
            android.content.res.TypedArray r5 = r5.obtainStyledAttributes(r6, r7)
            r3 = r5
            r5 = r0
            r6 = r3
            r7 = 0
            r8 = 1
            boolean r6 = r6.getBoolean(r7, r8)
            r5.setEnabled(r6)
            r5 = r3
            r5.recycle()
            r5 = r0
            android.content.res.Resources r5 = r5.getResources()
            android.util.DisplayMetrics r5 = r5.getDisplayMetrics()
            r4 = r5
            r5 = r0
            r6 = 1109393408(0x42200000, float:40.0)
            r7 = r4
            float r7 = r7.density
            float r6 = r6 * r7
            int r6 = (int) r6
            r5.mCircleWidth = r6
            r5 = r0
            r6 = 1109393408(0x42200000, float:40.0)
            r7 = r4
            float r7 = r7.density
            float r6 = r6 * r7
            int r6 = (int) r6
            r5.mCircleHeight = r6
            r5 = r0
            r5.createProgressView()
            r5 = r0
            r6 = 1
            android.support.v4.view.ViewCompat.setChildrenDrawingOrderEnabled(r5, r6)
            r5 = r0
            r6 = 1115684864(0x42800000, float:64.0)
            r7 = r4
            float r7 = r7.density
            float r6 = r6 * r7
            r5.mSpinnerFinalOffset = r6
            r5 = r0
            r6 = r0
            float r6 = r6.mSpinnerFinalOffset
            r5.mTotalDragDistance = r6
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.widget.SwipeRefreshLayout.<init>(android.content.Context, android.util.AttributeSet):void");
    }

    /* access modifiers changed from: protected */
    public int getChildDrawingOrder(int i, int i2) {
        int i3 = i;
        int i4 = i2;
        if (this.mCircleViewIndex < 0) {
            return i4;
        }
        if (i4 == i3 - 1) {
            return this.mCircleViewIndex;
        }
        if (i4 >= this.mCircleViewIndex) {
            return i4 + 1;
        }
        return i4;
    }

    private void createProgressView() {
        CircleImageView circleImageView;
        MaterialProgressDrawable materialProgressDrawable;
        new CircleImageView(getContext(), CIRCLE_BG_LIGHT, 20.0f);
        this.mCircleView = circleImageView;
        new MaterialProgressDrawable(getContext(), this);
        this.mProgress = materialProgressDrawable;
        this.mProgress.setBackgroundColor(CIRCLE_BG_LIGHT);
        this.mCircleView.setImageDrawable(this.mProgress);
        this.mCircleView.setVisibility(8);
        addView(this.mCircleView);
    }

    public void setOnRefreshListener(OnRefreshListener onRefreshListener) {
        this.mListener = onRefreshListener;
    }

    private boolean isAlphaUsedForScale() {
        return Build.VERSION.SDK_INT < 11;
    }

    public void setRefreshing(boolean z) {
        int i;
        boolean z2 = z;
        if (!z2 || this.mRefreshing == z2) {
            setRefreshing(z2, false);
            return;
        }
        this.mRefreshing = z2;
        if (!this.mUsingCustomStart) {
            i = (int) (this.mSpinnerFinalOffset + ((float) this.mOriginalOffsetTop));
        } else {
            i = (int) this.mSpinnerFinalOffset;
        }
        setTargetOffsetTopAndBottom(i - this.mCurrentTargetOffsetTop, true);
        this.mNotify = false;
        startScaleUpAnimation(this.mRefreshListener);
    }

    private void startScaleUpAnimation(Animation.AnimationListener animationListener) {
        Animation animation;
        Animation.AnimationListener animationListener2 = animationListener;
        this.mCircleView.setVisibility(0);
        if (Build.VERSION.SDK_INT >= 11) {
            this.mProgress.setAlpha(255);
        }
        new Animation() {
            public void applyTransformation(float f, Transformation transformation) {
                SwipeRefreshLayout.this.setAnimationProgress(f);
            }
        };
        this.mScaleAnimation = animation;
        this.mScaleAnimation.setDuration((long) this.mMediumAnimationDuration);
        if (animationListener2 != null) {
            this.mCircleView.setAnimationListener(animationListener2);
        }
        this.mCircleView.clearAnimation();
        this.mCircleView.startAnimation(this.mScaleAnimation);
    }

    /* access modifiers changed from: private */
    public void setAnimationProgress(float f) {
        float f2 = f;
        if (isAlphaUsedForScale()) {
            setColorViewAlpha((int) (f2 * 255.0f));
            return;
        }
        ViewCompat.setScaleX(this.mCircleView, f2);
        ViewCompat.setScaleY(this.mCircleView, f2);
    }

    private void setRefreshing(boolean z, boolean z2) {
        boolean z3 = z;
        boolean z4 = z2;
        if (this.mRefreshing != z3) {
            this.mNotify = z4;
            ensureTarget();
            this.mRefreshing = z3;
            if (this.mRefreshing) {
                animateOffsetToCorrectPosition(this.mCurrentTargetOffsetTop, this.mRefreshListener);
            } else {
                startScaleDownAnimation(this.mRefreshListener);
            }
        }
    }

    /* access modifiers changed from: private */
    public void startScaleDownAnimation(Animation.AnimationListener animationListener) {
        Animation animation;
        new Animation() {
            public void applyTransformation(float f, Transformation transformation) {
                SwipeRefreshLayout.this.setAnimationProgress(1.0f - f);
            }
        };
        this.mScaleDownAnimation = animation;
        this.mScaleDownAnimation.setDuration(150);
        this.mCircleView.setAnimationListener(animationListener);
        this.mCircleView.clearAnimation();
        this.mCircleView.startAnimation(this.mScaleDownAnimation);
    }

    private void startProgressAlphaStartAnimation() {
        this.mAlphaStartAnimation = startAlphaAnimation(this.mProgress.getAlpha(), STARTING_PROGRESS_ALPHA);
    }

    private void startProgressAlphaMaxAnimation() {
        this.mAlphaMaxAnimation = startAlphaAnimation(this.mProgress.getAlpha(), 255);
    }

    private Animation startAlphaAnimation(int i, int i2) {
        Animation animation;
        int i3 = i;
        int i4 = i2;
        if (this.mScale && isAlphaUsedForScale()) {
            return null;
        }
        final int i5 = i3;
        final int i6 = i4;
        new Animation() {
            public void applyTransformation(float f, Transformation transformation) {
                SwipeRefreshLayout.this.mProgress.setAlpha((int) (((float) i5) + (((float) (i6 - i5)) * f)));
            }
        };
        Animation animation2 = animation;
        animation2.setDuration(300);
        this.mCircleView.setAnimationListener(null);
        this.mCircleView.clearAnimation();
        this.mCircleView.startAnimation(animation2);
        return animation2;
    }

    @Deprecated
    public void setProgressBackgroundColor(int i) {
        setProgressBackgroundColorSchemeResource(i);
    }

    public void setProgressBackgroundColorSchemeResource(int i) {
        setProgressBackgroundColorSchemeColor(getResources().getColor(i));
    }

    public void setProgressBackgroundColorSchemeColor(int i) {
        int i2 = i;
        this.mCircleView.setBackgroundColor(i2);
        this.mProgress.setBackgroundColor(i2);
    }

    @Deprecated
    public void setColorScheme(int... iArr) {
        setColorSchemeResources(iArr);
    }

    public void setColorSchemeResources(int... iArr) {
        int[] iArr2 = iArr;
        Resources resources = getResources();
        int[] iArr3 = new int[iArr2.length];
        for (int i = 0; i < iArr2.length; i++) {
            iArr3[i] = resources.getColor(iArr2[i]);
        }
        setColorSchemeColors(iArr3);
    }

    public void setColorSchemeColors(int... iArr) {
        ensureTarget();
        this.mProgress.setColorSchemeColors(iArr);
    }

    public boolean isRefreshing() {
        return this.mRefreshing;
    }

    private void ensureTarget() {
        if (this.mTarget == null) {
            for (int i = 0; i < getChildCount(); i++) {
                View childAt = getChildAt(i);
                if (!childAt.equals(this.mCircleView)) {
                    this.mTarget = childAt;
                    return;
                }
            }
        }
    }

    public void setDistanceToTriggerSync(int i) {
        this.mTotalDragDistance = (float) i;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int measuredWidth = getMeasuredWidth();
        int measuredHeight = getMeasuredHeight();
        if (getChildCount() != 0) {
            if (this.mTarget == null) {
                ensureTarget();
            }
            if (this.mTarget != null) {
                View view = this.mTarget;
                int paddingLeft = getPaddingLeft();
                int paddingTop = getPaddingTop();
                view.layout(paddingLeft, paddingTop, paddingLeft + ((measuredWidth - getPaddingLeft()) - getPaddingRight()), paddingTop + ((measuredHeight - getPaddingTop()) - getPaddingBottom()));
                int measuredWidth2 = this.mCircleView.getMeasuredWidth();
                this.mCircleView.layout((measuredWidth / 2) - (measuredWidth2 / 2), this.mCurrentTargetOffsetTop, (measuredWidth / 2) + (measuredWidth2 / 2), this.mCurrentTargetOffsetTop + this.mCircleView.getMeasuredHeight());
            }
        }
    }

    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        if (this.mTarget == null) {
            ensureTarget();
        }
        if (this.mTarget != null) {
            this.mTarget.measure(View.MeasureSpec.makeMeasureSpec((getMeasuredWidth() - getPaddingLeft()) - getPaddingRight(), 1073741824), View.MeasureSpec.makeMeasureSpec((getMeasuredHeight() - getPaddingTop()) - getPaddingBottom(), 1073741824));
            this.mCircleView.measure(View.MeasureSpec.makeMeasureSpec(this.mCircleWidth, 1073741824), View.MeasureSpec.makeMeasureSpec(this.mCircleHeight, 1073741824));
            if (!this.mUsingCustomStart && !this.mOriginalOffsetCalculated) {
                this.mOriginalOffsetCalculated = true;
                int i3 = -this.mCircleView.getMeasuredHeight();
                this.mOriginalOffsetTop = i3;
                this.mCurrentTargetOffsetTop = i3;
            }
            this.mCircleViewIndex = -1;
            for (int i4 = 0; i4 < getChildCount(); i4++) {
                if (getChildAt(i4) == this.mCircleView) {
                    this.mCircleViewIndex = i4;
                    return;
                }
            }
        }
    }

    public int getProgressCircleDiameter() {
        return this.mCircleView != null ? this.mCircleView.getMeasuredHeight() : 0;
    }

    public boolean canChildScrollUp() {
        if (Build.VERSION.SDK_INT >= 14) {
            return ViewCompat.canScrollVertically(this.mTarget, -1);
        }
        if (this.mTarget instanceof AbsListView) {
            AbsListView absListView = (AbsListView) this.mTarget;
            return absListView.getChildCount() > 0 && (absListView.getFirstVisiblePosition() > 0 || absListView.getChildAt(0).getTop() < absListView.getPaddingTop());
        }
        return ViewCompat.canScrollVertically(this.mTarget, -1) || this.mTarget.getScrollY() > 0;
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        MotionEvent motionEvent2 = motionEvent;
        ensureTarget();
        int actionMasked = MotionEventCompat.getActionMasked(motionEvent2);
        if (this.mReturningToStart && actionMasked == 0) {
            this.mReturningToStart = false;
        }
        if (!isEnabled() || this.mReturningToStart || canChildScrollUp() || this.mRefreshing) {
            return false;
        }
        switch (actionMasked) {
            case 0:
                setTargetOffsetTopAndBottom(this.mOriginalOffsetTop - this.mCircleView.getTop(), true);
                this.mActivePointerId = MotionEventCompat.getPointerId(motionEvent2, 0);
                this.mIsBeingDragged = false;
                float motionEventY = getMotionEventY(motionEvent2, this.mActivePointerId);
                if (motionEventY != -1.0f) {
                    this.mInitialDownY = motionEventY;
                    break;
                } else {
                    return false;
                }
            case 1:
            case 3:
                this.mIsBeingDragged = false;
                this.mActivePointerId = -1;
                break;
            case 2:
                if (this.mActivePointerId != -1) {
                    float motionEventY2 = getMotionEventY(motionEvent2, this.mActivePointerId);
                    if (motionEventY2 != -1.0f) {
                        if (motionEventY2 - this.mInitialDownY > ((float) this.mTouchSlop) && !this.mIsBeingDragged) {
                            this.mInitialMotionY = this.mInitialDownY + ((float) this.mTouchSlop);
                            this.mIsBeingDragged = true;
                            this.mProgress.setAlpha(STARTING_PROGRESS_ALPHA);
                            break;
                        }
                    } else {
                        return false;
                    }
                } else {
                    int e = Log.e(LOG_TAG, "Got ACTION_MOVE event but don't have an active pointer id.");
                    return false;
                }
            case 6:
                onSecondaryPointerUp(motionEvent2);
                break;
        }
        return this.mIsBeingDragged;
    }

    private float getMotionEventY(MotionEvent motionEvent, int i) {
        MotionEvent motionEvent2 = motionEvent;
        int findPointerIndex = MotionEventCompat.findPointerIndex(motionEvent2, i);
        if (findPointerIndex < 0) {
            return -1.0f;
        }
        return MotionEventCompat.getY(motionEvent2, findPointerIndex);
    }

    public void requestDisallowInterceptTouchEvent(boolean z) {
    }

    private boolean isAnimationRunning(Animation animation) {
        Animation animation2 = animation;
        return animation2 != null && animation2.hasStarted() && !animation2.hasEnded();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    public boolean onTouchEvent(MotionEvent motionEvent) {
        Animation.AnimationListener animationListener;
        float f;
        MotionEvent motionEvent2 = motionEvent;
        int actionMasked = MotionEventCompat.getActionMasked(motionEvent2);
        if (this.mReturningToStart && actionMasked == 0) {
            this.mReturningToStart = false;
        }
        if (!isEnabled() || this.mReturningToStart || canChildScrollUp()) {
            return false;
        }
        switch (actionMasked) {
            case 0:
                this.mActivePointerId = MotionEventCompat.getPointerId(motionEvent2, 0);
                this.mIsBeingDragged = false;
                break;
            case 1:
            case 3:
                if (this.mActivePointerId == -1) {
                    if (actionMasked == 1) {
                        int e = Log.e(LOG_TAG, "Got ACTION_UP event but don't have an active pointer id.");
                    }
                    return false;
                }
                int findPointerIndex = MotionEventCompat.findPointerIndex(motionEvent2, this.mActivePointerId);
                this.mIsBeingDragged = false;
                if ((MotionEventCompat.getY(motionEvent2, findPointerIndex) - this.mInitialMotionY) * DRAG_RATE > this.mTotalDragDistance) {
                    setRefreshing(true, true);
                } else {
                    this.mRefreshing = false;
                    this.mProgress.setStartEndTrim(0.0f, 0.0f);
                    Animation.AnimationListener animationListener2 = null;
                    if (!this.mScale) {
                        new Animation.AnimationListener() {
                            public void onAnimationStart(Animation animation) {
                            }

                            public void onAnimationEnd(Animation animation) {
                                if (!SwipeRefreshLayout.this.mScale) {
                                    SwipeRefreshLayout.this.startScaleDownAnimation(null);
                                }
                            }

                            public void onAnimationRepeat(Animation animation) {
                            }
                        };
                        animationListener2 = animationListener;
                    }
                    animateOffsetToStartPosition(this.mCurrentTargetOffsetTop, animationListener2);
                    this.mProgress.showArrow(false);
                }
                this.mActivePointerId = -1;
                return false;
            case 2:
                int findPointerIndex2 = MotionEventCompat.findPointerIndex(motionEvent2, this.mActivePointerId);
                if (findPointerIndex2 >= 0) {
                    float y = (MotionEventCompat.getY(motionEvent2, findPointerIndex2) - this.mInitialMotionY) * DRAG_RATE;
                    if (this.mIsBeingDragged) {
                        this.mProgress.showArrow(true);
                        float f2 = y / this.mTotalDragDistance;
                        if (f2 >= 0.0f) {
                            float min = Math.min(1.0f, Math.abs(f2));
                            float max = (((float) Math.max(((double) min) - 0.4d, 0.0d)) * 5.0f) / 3.0f;
                            float abs = Math.abs(y) - this.mTotalDragDistance;
                            if (this.mUsingCustomStart) {
                                f = this.mSpinnerFinalOffset - ((float) this.mOriginalOffsetTop);
                            } else {
                                f = this.mSpinnerFinalOffset;
                            }
                            float f3 = f;
                            float max2 = Math.max(0.0f, Math.min(abs, f3 * DECELERATE_INTERPOLATION_FACTOR) / f3);
                            float pow = ((float) (((double) (max2 / 4.0f)) - Math.pow((double) (max2 / 4.0f), 2.0d))) * DECELERATE_INTERPOLATION_FACTOR;
                            int i = this.mOriginalOffsetTop + ((int) ((f3 * min) + (f3 * pow * DECELERATE_INTERPOLATION_FACTOR)));
                            if (this.mCircleView.getVisibility() != 0) {
                                this.mCircleView.setVisibility(0);
                            }
                            if (!this.mScale) {
                                ViewCompat.setScaleX(this.mCircleView, 1.0f);
                                ViewCompat.setScaleY(this.mCircleView, 1.0f);
                            }
                            if (y < this.mTotalDragDistance) {
                                if (this.mScale) {
                                    setAnimationProgress(y / this.mTotalDragDistance);
                                }
                                if (this.mProgress.getAlpha() > STARTING_PROGRESS_ALPHA && !isAnimationRunning(this.mAlphaStartAnimation)) {
                                    startProgressAlphaStartAnimation();
                                }
                                this.mProgress.setStartEndTrim(0.0f, Math.min((float) MAX_PROGRESS_ANGLE, max * MAX_PROGRESS_ANGLE));
                                this.mProgress.setArrowScale(Math.min(1.0f, max));
                            } else if (this.mProgress.getAlpha() < 255 && !isAnimationRunning(this.mAlphaMaxAnimation)) {
                                startProgressAlphaMaxAnimation();
                            }
                            this.mProgress.setProgressRotation((-0.25f + (0.4f * max) + (pow * DECELERATE_INTERPOLATION_FACTOR)) * DRAG_RATE);
                            setTargetOffsetTopAndBottom(i - this.mCurrentTargetOffsetTop, true);
                            break;
                        } else {
                            return false;
                        }
                    }
                } else {
                    int e2 = Log.e(LOG_TAG, "Got ACTION_MOVE event but have an invalid active pointer id.");
                    return false;
                }
                break;
            case 5:
                this.mActivePointerId = MotionEventCompat.getPointerId(motionEvent2, MotionEventCompat.getActionIndex(motionEvent2));
                break;
            case 6:
                onSecondaryPointerUp(motionEvent2);
                break;
        }
        return true;
    }

    private void animateOffsetToCorrectPosition(int i, Animation.AnimationListener animationListener) {
        Animation.AnimationListener animationListener2 = animationListener;
        this.mFrom = i;
        this.mAnimateToCorrectPosition.reset();
        this.mAnimateToCorrectPosition.setDuration(200);
        this.mAnimateToCorrectPosition.setInterpolator(this.mDecelerateInterpolator);
        if (animationListener2 != null) {
            this.mCircleView.setAnimationListener(animationListener2);
        }
        this.mCircleView.clearAnimation();
        this.mCircleView.startAnimation(this.mAnimateToCorrectPosition);
    }

    private void animateOffsetToStartPosition(int i, Animation.AnimationListener animationListener) {
        int i2 = i;
        Animation.AnimationListener animationListener2 = animationListener;
        if (this.mScale) {
            startScaleDownReturnToStartAnimation(i2, animationListener2);
            return;
        }
        this.mFrom = i2;
        this.mAnimateToStartPosition.reset();
        this.mAnimateToStartPosition.setDuration(200);
        this.mAnimateToStartPosition.setInterpolator(this.mDecelerateInterpolator);
        if (animationListener2 != null) {
            this.mCircleView.setAnimationListener(animationListener2);
        }
        this.mCircleView.clearAnimation();
        this.mCircleView.startAnimation(this.mAnimateToStartPosition);
    }

    /* access modifiers changed from: private */
    public void moveToStart(float f) {
        setTargetOffsetTopAndBottom((this.mFrom + ((int) (((float) (this.mOriginalOffsetTop - this.mFrom)) * f))) - this.mCircleView.getTop(), false);
    }

    private void startScaleDownReturnToStartAnimation(int i, Animation.AnimationListener animationListener) {
        Animation animation;
        Animation.AnimationListener animationListener2 = animationListener;
        this.mFrom = i;
        if (isAlphaUsedForScale()) {
            this.mStartingScale = (float) this.mProgress.getAlpha();
        } else {
            this.mStartingScale = ViewCompat.getScaleX(this.mCircleView);
        }
        new Animation() {
            public void applyTransformation(float f, Transformation transformation) {
                float f2 = f;
                SwipeRefreshLayout.this.setAnimationProgress(SwipeRefreshLayout.this.mStartingScale + ((-SwipeRefreshLayout.this.mStartingScale) * f2));
                SwipeRefreshLayout.this.moveToStart(f2);
            }
        };
        this.mScaleDownToStartAnimation = animation;
        this.mScaleDownToStartAnimation.setDuration(150);
        if (animationListener2 != null) {
            this.mCircleView.setAnimationListener(animationListener2);
        }
        this.mCircleView.clearAnimation();
        this.mCircleView.startAnimation(this.mScaleDownToStartAnimation);
    }

    /* access modifiers changed from: private */
    public void setTargetOffsetTopAndBottom(int i, boolean z) {
        this.mCircleView.bringToFront();
        this.mCircleView.offsetTopAndBottom(i);
        this.mCurrentTargetOffsetTop = this.mCircleView.getTop();
        if (z && Build.VERSION.SDK_INT < 11) {
            invalidate();
        }
    }

    private void onSecondaryPointerUp(MotionEvent motionEvent) {
        MotionEvent motionEvent2 = motionEvent;
        int actionIndex = MotionEventCompat.getActionIndex(motionEvent2);
        if (MotionEventCompat.getPointerId(motionEvent2, actionIndex) == this.mActivePointerId) {
            this.mActivePointerId = MotionEventCompat.getPointerId(motionEvent2, actionIndex == 0 ? 1 : 0);
        }
    }
}
