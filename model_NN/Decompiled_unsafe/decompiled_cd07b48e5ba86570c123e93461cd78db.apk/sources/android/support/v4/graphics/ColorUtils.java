package android.support.v4.graphics;

import android.graphics.Color;
import android.support.v4.view.ViewCompat;

public class ColorUtils {
    private static final int MIN_ALPHA_SEARCH_MAX_ITERATIONS = 10;
    private static final int MIN_ALPHA_SEARCH_PRECISION = 10;

    private ColorUtils() {
    }

    public static int compositeColors(int i, int i2) {
        int i3 = i;
        int i4 = i2;
        int alpha = Color.alpha(i4);
        int alpha2 = Color.alpha(i3);
        int compositeAlpha = compositeAlpha(alpha2, alpha);
        return Color.argb(compositeAlpha, compositeComponent(Color.red(i3), alpha2, Color.red(i4), alpha, compositeAlpha), compositeComponent(Color.green(i3), alpha2, Color.green(i4), alpha, compositeAlpha), compositeComponent(Color.blue(i3), alpha2, Color.blue(i4), alpha, compositeAlpha));
    }

    private static int compositeAlpha(int i, int i2) {
        return 255 - (((255 - i2) * (255 - i)) / 255);
    }

    private static int compositeComponent(int i, int i2, int i3, int i4, int i5) {
        int i6 = i;
        int i7 = i2;
        int i8 = i3;
        int i9 = i4;
        int i10 = i5;
        if (i10 == 0) {
            return 0;
        }
        return (((255 * i6) * i7) + ((i8 * i9) * (255 - i7))) / (i10 * 255);
    }

    public static double calculateLuminance(int i) {
        int i2 = i;
        double red = ((double) Color.red(i2)) / 255.0d;
        double pow = red < 0.03928d ? red / 12.92d : Math.pow((red + 0.055d) / 1.055d, 2.4d);
        double green = ((double) Color.green(i2)) / 255.0d;
        double pow2 = green < 0.03928d ? green / 12.92d : Math.pow((green + 0.055d) / 1.055d, 2.4d);
        double blue = ((double) Color.blue(i2)) / 255.0d;
        return (0.2126d * pow) + (0.7152d * pow2) + (0.0722d * (blue < 0.03928d ? blue / 12.92d : Math.pow((blue + 0.055d) / 1.055d, 2.4d)));
    }

    public static double calculateContrast(int i, int i2) {
        Throwable th;
        int i3 = i;
        int i4 = i2;
        if (Color.alpha(i4) != 255) {
            Throwable th2 = th;
            new IllegalArgumentException("background can not be translucent");
            throw th2;
        }
        if (Color.alpha(i3) < 255) {
            i3 = compositeColors(i3, i4);
        }
        double calculateLuminance = calculateLuminance(i3) + 0.05d;
        double calculateLuminance2 = calculateLuminance(i4) + 0.05d;
        return Math.max(calculateLuminance, calculateLuminance2) / Math.min(calculateLuminance, calculateLuminance2);
    }

    public static int calculateMinimumAlpha(int i, int i2, float f) {
        Throwable th;
        int i3 = i;
        int i4 = i2;
        float f2 = f;
        if (Color.alpha(i4) != 255) {
            Throwable th2 = th;
            new IllegalArgumentException("background can not be translucent");
            throw th2;
        } else if (calculateContrast(setAlphaComponent(i3, 255), i4) < ((double) f2)) {
            return -1;
        } else {
            int i5 = 0;
            int i6 = 255;
            for (int i7 = 0; i7 <= 10 && i6 - i5 > 10; i7++) {
                int i8 = (i5 + i6) / 2;
                if (calculateContrast(setAlphaComponent(i3, i8), i4) < ((double) f2)) {
                    i5 = i8;
                } else {
                    i6 = i8;
                }
            }
            return i6;
        }
    }

    public static void RGBToHSL(int i, int i2, int i3, float[] fArr) {
        float f;
        float abs;
        float[] fArr2 = fArr;
        float f2 = ((float) i) / 255.0f;
        float f3 = ((float) i2) / 255.0f;
        float f4 = ((float) i3) / 255.0f;
        float max = Math.max(f2, Math.max(f3, f4));
        float min = Math.min(f2, Math.min(f3, f4));
        float f5 = max - min;
        float f6 = (max + min) / 2.0f;
        if (max == min) {
            abs = 0.0f;
            f = 0.0f;
        } else {
            if (max == f2) {
                f = ((f3 - f4) / f5) % 6.0f;
            } else if (max == f3) {
                f = ((f4 - f2) / f5) + 2.0f;
            } else {
                f = ((f2 - f3) / f5) + 4.0f;
            }
            abs = f5 / (1.0f - Math.abs((2.0f * f6) - 1.0f));
        }
        fArr2[0] = (f * 60.0f) % 360.0f;
        fArr2[1] = abs;
        fArr2[2] = f6;
    }

    public static void colorToHSL(int i, float[] fArr) {
        int i2 = i;
        RGBToHSL(Color.red(i2), Color.green(i2), Color.blue(i2), fArr);
    }

    public static int HSLToColor(float[] fArr) {
        float[] fArr2 = fArr;
        float f = fArr2[0];
        float f2 = fArr2[1];
        float f3 = fArr2[2];
        float abs = (1.0f - Math.abs((2.0f * f3) - 1.0f)) * f2;
        float f4 = f3 - (0.5f * abs);
        float abs2 = abs * (1.0f - Math.abs(((f / 60.0f) % 2.0f) - 1.0f));
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        switch (((int) f) / 60) {
            case 0:
                i = Math.round(255.0f * (abs + f4));
                i2 = Math.round(255.0f * (abs2 + f4));
                i3 = Math.round(255.0f * f4);
                break;
            case 1:
                i = Math.round(255.0f * (abs2 + f4));
                i2 = Math.round(255.0f * (abs + f4));
                i3 = Math.round(255.0f * f4);
                break;
            case 2:
                i = Math.round(255.0f * f4);
                i2 = Math.round(255.0f * (abs + f4));
                i3 = Math.round(255.0f * (abs2 + f4));
                break;
            case 3:
                i = Math.round(255.0f * f4);
                i2 = Math.round(255.0f * (abs2 + f4));
                i3 = Math.round(255.0f * (abs + f4));
                break;
            case 4:
                i = Math.round(255.0f * (abs2 + f4));
                i2 = Math.round(255.0f * f4);
                i3 = Math.round(255.0f * (abs + f4));
                break;
            case 5:
            case 6:
                i = Math.round(255.0f * (abs + f4));
                i2 = Math.round(255.0f * f4);
                i3 = Math.round(255.0f * (abs2 + f4));
                break;
        }
        return Color.rgb(Math.max(0, Math.min(255, i)), Math.max(0, Math.min(255, i2)), Math.max(0, Math.min(255, i3)));
    }

    public static int setAlphaComponent(int i, int i2) {
        Throwable th;
        int i3 = i;
        int i4 = i2;
        if (i4 >= 0 && i4 <= 255) {
            return (i3 & ViewCompat.MEASURED_SIZE_MASK) | (i4 << 24);
        }
        Throwable th2 = th;
        new IllegalArgumentException("alpha must be between 0 and 255.");
        throw th2;
    }
}
