package android.support.v4.graphics.drawable;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;

class DrawableCompatLollipop {
    DrawableCompatLollipop() {
    }

    public static void setHotspot(Drawable drawable, float f, float f2) {
        drawable.setHotspot(f, f2);
    }

    public static void setHotspotBounds(Drawable drawable, int i, int i2, int i3, int i4) {
        drawable.setHotspotBounds(i, i2, i3, i4);
    }

    public static void setTint(Drawable drawable, int i) {
        Drawable drawable2 = drawable;
        int i2 = i;
        if (drawable2 instanceof DrawableWrapperLollipop) {
            DrawableCompatBase.setTint(drawable2, i2);
        } else {
            drawable2.setTint(i2);
        }
    }

    public static void setTintList(Drawable drawable, ColorStateList colorStateList) {
        Drawable drawable2 = drawable;
        ColorStateList colorStateList2 = colorStateList;
        if (drawable2 instanceof DrawableWrapperLollipop) {
            DrawableCompatBase.setTintList(drawable2, colorStateList2);
        } else {
            drawable2.setTintList(colorStateList2);
        }
    }

    public static void setTintMode(Drawable drawable, PorterDuff.Mode mode) {
        Drawable drawable2 = drawable;
        PorterDuff.Mode mode2 = mode;
        if (drawable2 instanceof DrawableWrapperLollipop) {
            DrawableCompatBase.setTintMode(drawable2, mode2);
        } else {
            drawable2.setTintMode(mode2);
        }
    }

    public static Drawable wrapForTinting(Drawable drawable) {
        Drawable drawable2;
        Drawable drawable3 = drawable;
        if (!(drawable3 instanceof GradientDrawable)) {
            return drawable3;
        }
        new DrawableWrapperLollipop(drawable3);
        return drawable2;
    }
}
