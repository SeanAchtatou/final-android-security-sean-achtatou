package android.support.v4.graphics.drawable;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.os.Build;
import android.support.v4.graphics.BitmapCompat;
import android.support.v4.view.GravityCompat;
import android.util.Log;
import java.io.InputStream;

public class RoundedBitmapDrawableFactory {
    private static final String TAG = "RoundedBitmapDrawableFactory";

    private static class DefaultRoundedBitmapDrawable extends RoundedBitmapDrawable {
        DefaultRoundedBitmapDrawable(Resources resources, Bitmap bitmap) {
            super(resources, bitmap);
        }

        public void setMipMap(boolean z) {
            boolean z2 = z;
            if (this.mBitmap != null) {
                BitmapCompat.setHasMipMap(this.mBitmap, z2);
                invalidateSelf();
            }
        }

        public boolean hasMipMap() {
            return this.mBitmap != null && BitmapCompat.hasMipMap(this.mBitmap);
        }

        /* access modifiers changed from: package-private */
        public void gravityCompatApply(int i, int i2, int i3, Rect rect, Rect rect2) {
            GravityCompat.apply(i, i2, i3, rect, rect2, 0);
        }
    }

    public static RoundedBitmapDrawable create(Resources resources, Bitmap bitmap) {
        RoundedBitmapDrawable roundedBitmapDrawable;
        RoundedBitmapDrawable roundedBitmapDrawable2;
        Resources resources2 = resources;
        Bitmap bitmap2 = bitmap;
        if (Build.VERSION.SDK_INT >= 21) {
            new RoundedBitmapDrawable21(resources2, bitmap2);
            return roundedBitmapDrawable2;
        }
        new DefaultRoundedBitmapDrawable(resources2, bitmap2);
        return roundedBitmapDrawable;
    }

    public static RoundedBitmapDrawable create(Resources resources, String str) {
        StringBuilder sb;
        String str2 = str;
        RoundedBitmapDrawable create = create(resources, BitmapFactory.decodeFile(str2));
        if (create.getBitmap() == null) {
            new StringBuilder();
            int w = Log.w(TAG, sb.append("BitmapDrawable cannot decode ").append(str2).toString());
        }
        return create;
    }

    public static RoundedBitmapDrawable create(Resources resources, InputStream inputStream) {
        StringBuilder sb;
        InputStream inputStream2 = inputStream;
        RoundedBitmapDrawable create = create(resources, BitmapFactory.decodeStream(inputStream2));
        if (create.getBitmap() == null) {
            new StringBuilder();
            int w = Log.w(TAG, sb.append("BitmapDrawable cannot decode ").append(inputStream2).toString());
        }
        return create;
    }
}
