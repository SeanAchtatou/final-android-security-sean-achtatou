package android.support.v4.app;

import android.app.RemoteInput;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.RemoteInputCompatBase;

class RemoteInputCompatApi20 {
    RemoteInputCompatApi20() {
    }

    static RemoteInputCompatBase.RemoteInput[] toCompat(RemoteInput[] remoteInputArr, RemoteInputCompatBase.RemoteInput.Factory factory) {
        RemoteInput[] remoteInputArr2 = remoteInputArr;
        RemoteInputCompatBase.RemoteInput.Factory factory2 = factory;
        if (remoteInputArr2 == null) {
            return null;
        }
        RemoteInputCompatBase.RemoteInput[] newArray = factory2.newArray(remoteInputArr2.length);
        for (int i = 0; i < remoteInputArr2.length; i++) {
            RemoteInput remoteInput = remoteInputArr2[i];
            newArray[i] = factory2.build(remoteInput.getResultKey(), remoteInput.getLabel(), remoteInput.getChoices(), remoteInput.getAllowFreeFormInput(), remoteInput.getExtras());
        }
        return newArray;
    }

    static RemoteInput[] fromCompat(RemoteInputCompatBase.RemoteInput[] remoteInputArr) {
        RemoteInput.Builder builder;
        RemoteInputCompatBase.RemoteInput[] remoteInputArr2 = remoteInputArr;
        if (remoteInputArr2 == null) {
            return null;
        }
        RemoteInput[] remoteInputArr3 = new RemoteInput[remoteInputArr2.length];
        for (int i = 0; i < remoteInputArr2.length; i++) {
            RemoteInputCompatBase.RemoteInput remoteInput = remoteInputArr2[i];
            new RemoteInput.Builder(remoteInput.getResultKey());
            remoteInputArr3[i] = builder.setLabel(remoteInput.getLabel()).setChoices(remoteInput.getChoices()).setAllowFreeFormInput(remoteInput.getAllowFreeFormInput()).addExtras(remoteInput.getExtras()).build();
        }
        return remoteInputArr3;
    }

    static Bundle getResultsFromIntent(Intent intent) {
        return RemoteInput.getResultsFromIntent(intent);
    }

    static void addResultsToIntent(RemoteInputCompatBase.RemoteInput[] remoteInputArr, Intent intent, Bundle bundle) {
        RemoteInput.addResultsToIntent(fromCompat(remoteInputArr), intent, bundle);
    }
}
