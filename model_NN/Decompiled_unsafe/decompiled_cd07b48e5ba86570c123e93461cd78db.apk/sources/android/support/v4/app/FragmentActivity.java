package android.support.v4.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.internal.view.SupportMenu;
import android.support.v4.util.SimpleArrayMap;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;

public class FragmentActivity extends Activity {
    static final String FRAGMENTS_TAG = "android:support:fragments";
    private static final int HONEYCOMB = 11;
    static final int MSG_REALLY_STOPPED = 1;
    static final int MSG_RESUME_PENDING = 2;
    private static final String TAG = "FragmentActivity";
    SimpleArrayMap<String, LoaderManagerImpl> mAllLoaderManagers;
    boolean mCheckedForLoaderManager;
    final FragmentContainer mContainer;
    boolean mCreated;
    final FragmentManagerImpl mFragments;
    final Handler mHandler;
    LoaderManagerImpl mLoaderManager;
    boolean mLoadersStarted;
    boolean mOptionsMenuInvalidated;
    boolean mReallyStopped;
    boolean mResumed;
    boolean mRetaining;
    boolean mStopped;

    public FragmentActivity() {
        Handler handler;
        FragmentManagerImpl fragmentManagerImpl;
        FragmentContainer fragmentContainer;
        new Handler() {
            public void handleMessage(Message message) {
                Message message2 = message;
                switch (message2.what) {
                    case 1:
                        if (FragmentActivity.this.mStopped) {
                            FragmentActivity.this.doReallyStop(false);
                            return;
                        }
                        return;
                    case 2:
                        FragmentActivity.this.onResumeFragments();
                        boolean execPendingActions = FragmentActivity.this.mFragments.execPendingActions();
                        return;
                    default:
                        super.handleMessage(message2);
                        return;
                }
            }
        };
        this.mHandler = handler;
        new FragmentManagerImpl();
        this.mFragments = fragmentManagerImpl;
        new FragmentContainer() {
            @Nullable
            public View findViewById(int i) {
                return FragmentActivity.this.findViewById(i);
            }

            public boolean hasView() {
                Window window = FragmentActivity.this.getWindow();
                return (window == null || window.peekDecorView() == null) ? false : true;
            }
        };
        this.mContainer = fragmentContainer;
    }

    static final class NonConfigurationInstances {
        Object activity;
        SimpleArrayMap<String, Object> children;
        Object custom;
        ArrayList<Fragment> fragments;
        SimpleArrayMap<String, LoaderManagerImpl> loaders;

        NonConfigurationInstances() {
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        StringBuilder sb;
        StringBuilder sb2;
        int i3 = i;
        int i4 = i2;
        Intent intent2 = intent;
        this.mFragments.noteStateNotSaved();
        int i5 = i3 >> 16;
        if (i5 != 0) {
            int i6 = i5 - 1;
            if (this.mFragments.mActive == null || i6 < 0 || i6 >= this.mFragments.mActive.size()) {
                new StringBuilder();
                int w = Log.w(TAG, sb.append("Activity result fragment index out of range: 0x").append(Integer.toHexString(i3)).toString());
                return;
            }
            Fragment fragment = this.mFragments.mActive.get(i6);
            if (fragment == null) {
                new StringBuilder();
                int w2 = Log.w(TAG, sb2.append("Activity result no fragment exists for index: 0x").append(Integer.toHexString(i3)).toString());
                return;
            }
            fragment.onActivityResult(i3 & SupportMenu.USER_MASK, i4, intent2);
            return;
        }
        super.onActivityResult(i3, i4, intent2);
    }

    public void onBackPressed() {
        if (!this.mFragments.popBackStackImmediate()) {
            supportFinishAfterTransition();
        }
    }

    public void supportFinishAfterTransition() {
        ActivityCompat.finishAfterTransition(this);
    }

    public void setEnterSharedElementCallback(SharedElementCallback sharedElementCallback) {
        ActivityCompat.setEnterSharedElementCallback(this, sharedElementCallback);
    }

    public void setExitSharedElementCallback(SharedElementCallback sharedElementCallback) {
        ActivityCompat.setExitSharedElementCallback(this, sharedElementCallback);
    }

    public void supportPostponeEnterTransition() {
        ActivityCompat.postponeEnterTransition(this);
    }

    public void supportStartPostponedEnterTransition() {
        ActivityCompat.startPostponedEnterTransition(this);
    }

    public void onConfigurationChanged(Configuration configuration) {
        Configuration configuration2 = configuration;
        super.onConfigurationChanged(configuration2);
        this.mFragments.dispatchConfigurationChanged(configuration2);
    }

    /* access modifiers changed from: protected */
    public void onCreate(@Nullable Bundle bundle) {
        Bundle bundle2 = bundle;
        this.mFragments.attachActivity(this, this.mContainer, null);
        if (getLayoutInflater().getFactory() == null) {
            getLayoutInflater().setFactory(this);
        }
        super.onCreate(bundle2);
        NonConfigurationInstances nonConfigurationInstances = (NonConfigurationInstances) getLastNonConfigurationInstance();
        if (nonConfigurationInstances != null) {
            this.mAllLoaderManagers = nonConfigurationInstances.loaders;
        }
        if (bundle2 != null) {
            this.mFragments.restoreAllState(bundle2.getParcelable(FRAGMENTS_TAG), nonConfigurationInstances != null ? nonConfigurationInstances.fragments : null);
        }
        this.mFragments.dispatchCreate();
    }

    public boolean onCreatePanelMenu(int i, Menu menu) {
        int i2 = i;
        Menu menu2 = menu;
        if (i2 != 0) {
            return super.onCreatePanelMenu(i2, menu2);
        }
        boolean onCreatePanelMenu = super.onCreatePanelMenu(i2, menu2) | this.mFragments.dispatchCreateOptionsMenu(menu2, getMenuInflater());
        if (Build.VERSION.SDK_INT >= 11) {
            return onCreatePanelMenu;
        }
        return true;
    }

    @Nullable
    public View onCreateView(String str, @NonNull Context context, @NonNull AttributeSet attributeSet) {
        String str2 = str;
        Context context2 = context;
        AttributeSet attributeSet2 = attributeSet;
        if (!"fragment".equals(str2)) {
            return super.onCreateView(str2, context2, attributeSet2);
        }
        View onCreateView = this.mFragments.onCreateView(null, str2, context2, attributeSet2);
        if (onCreateView == null) {
            return super.onCreateView(str2, context2, attributeSet2);
        }
        return onCreateView;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        doReallyStop(false);
        this.mFragments.dispatchDestroy();
        if (this.mLoaderManager != null) {
            this.mLoaderManager.doDestroy();
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        int i2 = i;
        KeyEvent keyEvent2 = keyEvent;
        if (Build.VERSION.SDK_INT >= 5 || i2 != 4 || keyEvent2.getRepeatCount() != 0) {
            return super.onKeyDown(i2, keyEvent2);
        }
        onBackPressed();
        return true;
    }

    public void onLowMemory() {
        super.onLowMemory();
        this.mFragments.dispatchLowMemory();
    }

    public boolean onMenuItemSelected(int i, MenuItem menuItem) {
        int i2 = i;
        MenuItem menuItem2 = menuItem;
        if (super.onMenuItemSelected(i2, menuItem2)) {
            return true;
        }
        switch (i2) {
            case 0:
                return this.mFragments.dispatchOptionsItemSelected(menuItem2);
            case 6:
                return this.mFragments.dispatchContextItemSelected(menuItem2);
            default:
                return false;
        }
    }

    public void onPanelClosed(int i, Menu menu) {
        int i2 = i;
        Menu menu2 = menu;
        switch (i2) {
            case 0:
                this.mFragments.dispatchOptionsMenuClosed(menu2);
                break;
        }
        super.onPanelClosed(i2, menu2);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.mResumed = false;
        if (this.mHandler.hasMessages(2)) {
            this.mHandler.removeMessages(2);
            onResumeFragments();
        }
        this.mFragments.dispatchPause();
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        this.mFragments.noteStateNotSaved();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        boolean sendEmptyMessage = this.mHandler.sendEmptyMessage(2);
        this.mResumed = true;
        boolean execPendingActions = this.mFragments.execPendingActions();
    }

    /* access modifiers changed from: protected */
    public void onPostResume() {
        super.onPostResume();
        this.mHandler.removeMessages(2);
        onResumeFragments();
        boolean execPendingActions = this.mFragments.execPendingActions();
    }

    /* access modifiers changed from: protected */
    public void onResumeFragments() {
        this.mFragments.dispatchResume();
    }

    public boolean onPreparePanel(int i, View view, Menu menu) {
        int i2 = i;
        View view2 = view;
        Menu menu2 = menu;
        if (i2 != 0 || menu2 == null) {
            return super.onPreparePanel(i2, view2, menu2);
        }
        if (this.mOptionsMenuInvalidated) {
            this.mOptionsMenuInvalidated = false;
            menu2.clear();
            boolean onCreatePanelMenu = onCreatePanelMenu(i2, menu2);
        }
        return onPrepareOptionsPanel(view2, menu2) | this.mFragments.dispatchPrepareOptionsMenu(menu2);
    }

    /* access modifiers changed from: protected */
    public boolean onPrepareOptionsPanel(View view, Menu menu) {
        return super.onPreparePanel(0, view, menu);
    }

    public final Object onRetainNonConfigurationInstance() {
        NonConfigurationInstances nonConfigurationInstances;
        if (this.mStopped) {
            doReallyStop(true);
        }
        Object onRetainCustomNonConfigurationInstance = onRetainCustomNonConfigurationInstance();
        ArrayList<Fragment> retainNonConfig = this.mFragments.retainNonConfig();
        boolean z = false;
        if (this.mAllLoaderManagers != null) {
            int size = this.mAllLoaderManagers.size();
            LoaderManagerImpl[] loaderManagerImplArr = new LoaderManagerImpl[size];
            for (int i = size - 1; i >= 0; i--) {
                loaderManagerImplArr[i] = this.mAllLoaderManagers.valueAt(i);
            }
            for (int i2 = 0; i2 < size; i2++) {
                LoaderManagerImpl loaderManagerImpl = loaderManagerImplArr[i2];
                if (loaderManagerImpl.mRetaining) {
                    z = true;
                } else {
                    loaderManagerImpl.doDestroy();
                    LoaderManagerImpl remove = this.mAllLoaderManagers.remove(loaderManagerImpl.mWho);
                }
            }
        }
        if (retainNonConfig == null && !z && onRetainCustomNonConfigurationInstance == null) {
            return null;
        }
        new NonConfigurationInstances();
        NonConfigurationInstances nonConfigurationInstances2 = nonConfigurationInstances;
        nonConfigurationInstances2.activity = null;
        nonConfigurationInstances2.custom = onRetainCustomNonConfigurationInstance;
        nonConfigurationInstances2.children = null;
        nonConfigurationInstances2.fragments = retainNonConfig;
        nonConfigurationInstances2.loaders = this.mAllLoaderManagers;
        return nonConfigurationInstances2;
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        Bundle bundle2 = bundle;
        super.onSaveInstanceState(bundle2);
        Parcelable saveAllState = this.mFragments.saveAllState();
        if (saveAllState != null) {
            bundle2.putParcelable(FRAGMENTS_TAG, saveAllState);
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        this.mStopped = false;
        this.mReallyStopped = false;
        this.mHandler.removeMessages(1);
        if (!this.mCreated) {
            this.mCreated = true;
            this.mFragments.dispatchActivityCreated();
        }
        this.mFragments.noteStateNotSaved();
        boolean execPendingActions = this.mFragments.execPendingActions();
        if (!this.mLoadersStarted) {
            this.mLoadersStarted = true;
            if (this.mLoaderManager != null) {
                this.mLoaderManager.doStart();
            } else if (!this.mCheckedForLoaderManager) {
                this.mLoaderManager = getLoaderManager("(root)", this.mLoadersStarted, false);
                if (this.mLoaderManager != null && !this.mLoaderManager.mStarted) {
                    this.mLoaderManager.doStart();
                }
            }
            this.mCheckedForLoaderManager = true;
        }
        this.mFragments.dispatchStart();
        if (this.mAllLoaderManagers != null) {
            int size = this.mAllLoaderManagers.size();
            LoaderManagerImpl[] loaderManagerImplArr = new LoaderManagerImpl[size];
            for (int i = size - 1; i >= 0; i--) {
                loaderManagerImplArr[i] = this.mAllLoaderManagers.valueAt(i);
            }
            for (int i2 = 0; i2 < size; i2++) {
                LoaderManagerImpl loaderManagerImpl = loaderManagerImplArr[i2];
                loaderManagerImpl.finishRetain();
                loaderManagerImpl.doReportStart();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        this.mStopped = true;
        boolean sendEmptyMessage = this.mHandler.sendEmptyMessage(1);
        this.mFragments.dispatchStop();
    }

    public Object onRetainCustomNonConfigurationInstance() {
        return null;
    }

    public Object getLastCustomNonConfigurationInstance() {
        NonConfigurationInstances nonConfigurationInstances = (NonConfigurationInstances) getLastNonConfigurationInstance();
        return nonConfigurationInstances != null ? nonConfigurationInstances.custom : null;
    }

    public void supportInvalidateOptionsMenu() {
        if (Build.VERSION.SDK_INT >= 11) {
            ActivityCompatHoneycomb.invalidateOptionsMenu(this);
        } else {
            this.mOptionsMenuInvalidated = true;
        }
    }

    public void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        StringBuilder sb;
        StringBuilder sb2;
        StringBuilder sb3;
        String str2 = str;
        FileDescriptor fileDescriptor2 = fileDescriptor;
        PrintWriter printWriter2 = printWriter;
        String[] strArr2 = strArr;
        if (Build.VERSION.SDK_INT >= 11) {
        }
        printWriter2.print(str2);
        printWriter2.print("Local FragmentActivity ");
        printWriter2.print(Integer.toHexString(System.identityHashCode(this)));
        printWriter2.println(" State:");
        new StringBuilder();
        String sb4 = sb.append(str2).append("  ").toString();
        printWriter2.print(sb4);
        printWriter2.print("mCreated=");
        printWriter2.print(this.mCreated);
        printWriter2.print("mResumed=");
        printWriter2.print(this.mResumed);
        printWriter2.print(" mStopped=");
        printWriter2.print(this.mStopped);
        printWriter2.print(" mReallyStopped=");
        printWriter2.println(this.mReallyStopped);
        printWriter2.print(sb4);
        printWriter2.print("mLoadersStarted=");
        printWriter2.println(this.mLoadersStarted);
        if (this.mLoaderManager != null) {
            printWriter2.print(str2);
            printWriter2.print("Loader Manager ");
            printWriter2.print(Integer.toHexString(System.identityHashCode(this.mLoaderManager)));
            printWriter2.println(":");
            LoaderManagerImpl loaderManagerImpl = this.mLoaderManager;
            new StringBuilder();
            loaderManagerImpl.dump(sb3.append(str2).append("  ").toString(), fileDescriptor2, printWriter2, strArr2);
        }
        this.mFragments.dump(str2, fileDescriptor2, printWriter2, strArr2);
        printWriter2.print(str2);
        printWriter2.println("View Hierarchy:");
        new StringBuilder();
        dumpViewHierarchy(sb2.append(str2).append("  ").toString(), printWriter2, getWindow().getDecorView());
    }

    private static String viewToString(View view) {
        StringBuilder sb;
        String str;
        View view2 = view;
        new StringBuilder(128);
        StringBuilder sb2 = sb;
        StringBuilder append = sb2.append(view2.getClass().getName());
        StringBuilder append2 = sb2.append('{');
        StringBuilder append3 = sb2.append(Integer.toHexString(System.identityHashCode(view2)));
        StringBuilder append4 = sb2.append(' ');
        switch (view2.getVisibility()) {
            case 0:
                StringBuilder append5 = sb2.append('V');
                break;
            case 4:
                StringBuilder append6 = sb2.append('I');
                break;
            case 8:
                StringBuilder append7 = sb2.append('G');
                break;
            default:
                StringBuilder append8 = sb2.append('.');
                break;
        }
        StringBuilder append9 = sb2.append(view2.isFocusable() ? 'F' : '.');
        StringBuilder append10 = sb2.append(view2.isEnabled() ? 'E' : '.');
        StringBuilder append11 = sb2.append(view2.willNotDraw() ? '.' : 'D');
        StringBuilder append12 = sb2.append(view2.isHorizontalScrollBarEnabled() ? 'H' : '.');
        StringBuilder append13 = sb2.append(view2.isVerticalScrollBarEnabled() ? 'V' : '.');
        StringBuilder append14 = sb2.append(view2.isClickable() ? 'C' : '.');
        StringBuilder append15 = sb2.append(view2.isLongClickable() ? 'L' : '.');
        StringBuilder append16 = sb2.append(' ');
        StringBuilder append17 = sb2.append(view2.isFocused() ? 'F' : '.');
        StringBuilder append18 = sb2.append(view2.isSelected() ? 'S' : '.');
        StringBuilder append19 = sb2.append(view2.isPressed() ? 'P' : '.');
        StringBuilder append20 = sb2.append(' ');
        StringBuilder append21 = sb2.append(view2.getLeft());
        StringBuilder append22 = sb2.append(',');
        StringBuilder append23 = sb2.append(view2.getTop());
        StringBuilder append24 = sb2.append('-');
        StringBuilder append25 = sb2.append(view2.getRight());
        StringBuilder append26 = sb2.append(',');
        StringBuilder append27 = sb2.append(view2.getBottom());
        int id = view2.getId();
        if (id != -1) {
            StringBuilder append28 = sb2.append(" #");
            StringBuilder append29 = sb2.append(Integer.toHexString(id));
            Resources resources = view2.getResources();
            if (!(id == 0 || resources == null)) {
                switch (id & ViewCompat.MEASURED_STATE_MASK) {
                    case ViewCompat.MEASURED_STATE_TOO_SMALL /*16777216*/:
                        str = "android";
                        String resourceTypeName = resources.getResourceTypeName(id);
                        String resourceEntryName = resources.getResourceEntryName(id);
                        StringBuilder append30 = sb2.append(" ");
                        StringBuilder append31 = sb2.append(str);
                        StringBuilder append32 = sb2.append(":");
                        StringBuilder append33 = sb2.append(resourceTypeName);
                        StringBuilder append34 = sb2.append("/");
                        StringBuilder append35 = sb2.append(resourceEntryName);
                        break;
                    case 2130706432:
                        str = "app";
                        String resourceTypeName2 = resources.getResourceTypeName(id);
                        String resourceEntryName2 = resources.getResourceEntryName(id);
                        StringBuilder append302 = sb2.append(" ");
                        StringBuilder append312 = sb2.append(str);
                        StringBuilder append322 = sb2.append(":");
                        StringBuilder append332 = sb2.append(resourceTypeName2);
                        StringBuilder append342 = sb2.append("/");
                        StringBuilder append352 = sb2.append(resourceEntryName2);
                        break;
                    default:
                        try {
                            str = resources.getResourcePackageName(id);
                            String resourceTypeName22 = resources.getResourceTypeName(id);
                            String resourceEntryName22 = resources.getResourceEntryName(id);
                            StringBuilder append3022 = sb2.append(" ");
                            StringBuilder append3122 = sb2.append(str);
                            StringBuilder append3222 = sb2.append(":");
                            StringBuilder append3322 = sb2.append(resourceTypeName22);
                            StringBuilder append3422 = sb2.append("/");
                            StringBuilder append3522 = sb2.append(resourceEntryName22);
                            break;
                        } catch (Resources.NotFoundException e) {
                            break;
                        }
                }
            }
        }
        StringBuilder append36 = sb2.append("}");
        return sb2.toString();
    }

    private void dumpViewHierarchy(String str, PrintWriter printWriter, View view) {
        ViewGroup viewGroup;
        int childCount;
        StringBuilder sb;
        String str2 = str;
        PrintWriter printWriter2 = printWriter;
        View view2 = view;
        printWriter2.print(str2);
        if (view2 == null) {
            printWriter2.println("null");
            return;
        }
        printWriter2.println(viewToString(view2));
        if ((view2 instanceof ViewGroup) && (childCount = (viewGroup = (ViewGroup) view2).getChildCount()) > 0) {
            new StringBuilder();
            String sb2 = sb.append(str2).append("  ").toString();
            for (int i = 0; i < childCount; i++) {
                dumpViewHierarchy(sb2, printWriter2, viewGroup.getChildAt(i));
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void doReallyStop(boolean z) {
        boolean z2 = z;
        if (!this.mReallyStopped) {
            this.mReallyStopped = true;
            this.mRetaining = z2;
            this.mHandler.removeMessages(1);
            onReallyStop();
        }
    }

    /* access modifiers changed from: package-private */
    public void onReallyStop() {
        if (this.mLoadersStarted) {
            this.mLoadersStarted = false;
            if (this.mLoaderManager != null) {
                if (!this.mRetaining) {
                    this.mLoaderManager.doStop();
                } else {
                    this.mLoaderManager.doRetain();
                }
            }
        }
        this.mFragments.dispatchReallyStop();
    }

    public void onAttachFragment(Fragment fragment) {
    }

    public FragmentManager getSupportFragmentManager() {
        return this.mFragments;
    }

    public void startActivityForResult(Intent intent, int i) {
        Throwable th;
        Intent intent2 = intent;
        int i2 = i;
        if (i2 == -1 || (i2 & SupportMenu.CATEGORY_MASK) == 0) {
            super.startActivityForResult(intent2, i2);
            return;
        }
        Throwable th2 = th;
        new IllegalArgumentException("Can only use lower 16 bits for requestCode");
        throw th2;
    }

    public void startActivityFromFragment(Fragment fragment, Intent intent, int i) {
        Throwable th;
        Fragment fragment2 = fragment;
        Intent intent2 = intent;
        int i2 = i;
        if (i2 == -1) {
            super.startActivityForResult(intent2, -1);
        } else if ((i2 & SupportMenu.CATEGORY_MASK) != 0) {
            Throwable th2 = th;
            new IllegalArgumentException("Can only use lower 16 bits for requestCode");
            throw th2;
        } else {
            super.startActivityForResult(intent2, ((fragment2.mIndex + 1) << 16) + (i2 & SupportMenu.USER_MASK));
        }
    }

    /* access modifiers changed from: package-private */
    public void invalidateSupportFragment(String str) {
        LoaderManagerImpl loaderManagerImpl;
        String str2 = str;
        if (this.mAllLoaderManagers != null && (loaderManagerImpl = this.mAllLoaderManagers.get(str2)) != null && !loaderManagerImpl.mRetaining) {
            loaderManagerImpl.doDestroy();
            LoaderManagerImpl remove = this.mAllLoaderManagers.remove(str2);
        }
    }

    public LoaderManager getSupportLoaderManager() {
        if (this.mLoaderManager != null) {
            return this.mLoaderManager;
        }
        this.mCheckedForLoaderManager = true;
        this.mLoaderManager = getLoaderManager("(root)", this.mLoadersStarted, true);
        return this.mLoaderManager;
    }

    /* access modifiers changed from: package-private */
    public LoaderManagerImpl getLoaderManager(String str, boolean z, boolean z2) {
        LoaderManagerImpl loaderManagerImpl;
        SimpleArrayMap<String, LoaderManagerImpl> simpleArrayMap;
        String str2 = str;
        boolean z3 = z;
        boolean z4 = z2;
        if (this.mAllLoaderManagers == null) {
            new SimpleArrayMap<>();
            this.mAllLoaderManagers = simpleArrayMap;
        }
        LoaderManagerImpl loaderManagerImpl2 = this.mAllLoaderManagers.get(str2);
        if (loaderManagerImpl2 != null) {
            loaderManagerImpl2.updateActivity(this);
        } else if (z4) {
            new LoaderManagerImpl(str2, this, z3);
            loaderManagerImpl2 = loaderManagerImpl;
            LoaderManagerImpl put = this.mAllLoaderManagers.put(str2, loaderManagerImpl2);
        }
        return loaderManagerImpl2;
    }
}
