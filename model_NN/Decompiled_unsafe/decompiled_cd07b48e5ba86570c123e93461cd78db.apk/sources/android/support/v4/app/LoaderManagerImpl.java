package android.support.v4.app;

import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.util.DebugUtils;
import android.support.v4.util.SparseArrayCompat;
import android.util.Log;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.lang.reflect.Modifier;

/* compiled from: LoaderManager */
class LoaderManagerImpl extends LoaderManager {
    static boolean DEBUG = false;
    static final String TAG = "LoaderManager";
    FragmentActivity mActivity;
    boolean mCreatingLoader;
    final SparseArrayCompat<LoaderInfo> mInactiveLoaders;
    final SparseArrayCompat<LoaderInfo> mLoaders;
    boolean mRetaining;
    boolean mRetainingStarted;
    boolean mStarted;
    final String mWho;

    /* compiled from: LoaderManager */
    final class LoaderInfo implements Loader.OnLoadCompleteListener<Object> {
        final Bundle mArgs;
        LoaderManager.LoaderCallbacks<Object> mCallbacks;
        Object mData;
        boolean mDeliveredData;
        boolean mDestroyed;
        boolean mHaveData;
        final int mId;
        boolean mListenerRegistered;
        Loader<Object> mLoader;
        LoaderInfo mPendingLoader;
        boolean mReportNextStart;
        boolean mRetaining;
        boolean mRetainingStarted;
        boolean mStarted;

        public LoaderInfo(int i, Bundle bundle, LoaderManager.LoaderCallbacks<Object> loaderCallbacks) {
            this.mId = i;
            this.mArgs = bundle;
            this.mCallbacks = loaderCallbacks;
        }

        /* access modifiers changed from: package-private */
        public void start() {
            Throwable th;
            StringBuilder sb;
            StringBuilder sb2;
            if (this.mRetaining && this.mRetainingStarted) {
                this.mStarted = true;
            } else if (!this.mStarted) {
                this.mStarted = true;
                if (LoaderManagerImpl.DEBUG) {
                    new StringBuilder();
                    int v = Log.v(LoaderManagerImpl.TAG, sb2.append("  Starting: ").append(this).toString());
                }
                if (this.mLoader == null && this.mCallbacks != null) {
                    this.mLoader = this.mCallbacks.onCreateLoader(this.mId, this.mArgs);
                }
                if (this.mLoader == null) {
                    return;
                }
                if (!this.mLoader.getClass().isMemberClass() || Modifier.isStatic(this.mLoader.getClass().getModifiers())) {
                    if (!this.mListenerRegistered) {
                        this.mLoader.registerListener(this.mId, this);
                        this.mListenerRegistered = true;
                    }
                    this.mLoader.startLoading();
                    return;
                }
                Throwable th2 = th;
                new StringBuilder();
                new IllegalArgumentException(sb.append("Object returned from onCreateLoader must not be a non-static inner member class: ").append(this.mLoader).toString());
                throw th2;
            }
        }

        /* access modifiers changed from: package-private */
        public void retain() {
            StringBuilder sb;
            if (LoaderManagerImpl.DEBUG) {
                new StringBuilder();
                int v = Log.v(LoaderManagerImpl.TAG, sb.append("  Retaining: ").append(this).toString());
            }
            this.mRetaining = true;
            this.mRetainingStarted = this.mStarted;
            this.mStarted = false;
            this.mCallbacks = null;
        }

        /* access modifiers changed from: package-private */
        public void finishRetain() {
            StringBuilder sb;
            if (this.mRetaining) {
                if (LoaderManagerImpl.DEBUG) {
                    new StringBuilder();
                    int v = Log.v(LoaderManagerImpl.TAG, sb.append("  Finished Retaining: ").append(this).toString());
                }
                this.mRetaining = false;
                if (this.mStarted != this.mRetainingStarted && !this.mStarted) {
                    stop();
                }
            }
            if (this.mStarted && this.mHaveData && !this.mReportNextStart) {
                callOnLoadFinished(this.mLoader, this.mData);
            }
        }

        /* access modifiers changed from: package-private */
        public void reportStart() {
            if (this.mStarted && this.mReportNextStart) {
                this.mReportNextStart = false;
                if (this.mHaveData) {
                    callOnLoadFinished(this.mLoader, this.mData);
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void stop() {
            StringBuilder sb;
            if (LoaderManagerImpl.DEBUG) {
                new StringBuilder();
                int v = Log.v(LoaderManagerImpl.TAG, sb.append("  Stopping: ").append(this).toString());
            }
            this.mStarted = false;
            if (!this.mRetaining && this.mLoader != null && this.mListenerRegistered) {
                this.mListenerRegistered = false;
                this.mLoader.unregisterListener(this);
                this.mLoader.stopLoading();
            }
        }

        /* access modifiers changed from: package-private */
        public void destroy() {
            StringBuilder sb;
            StringBuilder sb2;
            if (LoaderManagerImpl.DEBUG) {
                new StringBuilder();
                int v = Log.v(LoaderManagerImpl.TAG, sb2.append("  Destroying: ").append(this).toString());
            }
            this.mDestroyed = true;
            boolean z = this.mDeliveredData;
            this.mDeliveredData = false;
            if (this.mCallbacks != null && this.mLoader != null && this.mHaveData && z) {
                if (LoaderManagerImpl.DEBUG) {
                    new StringBuilder();
                    int v2 = Log.v(LoaderManagerImpl.TAG, sb.append("  Reseting: ").append(this).toString());
                }
                String str = null;
                if (LoaderManagerImpl.this.mActivity != null) {
                    str = LoaderManagerImpl.this.mActivity.mFragments.mNoTransactionsBecause;
                    LoaderManagerImpl.this.mActivity.mFragments.mNoTransactionsBecause = "onLoaderReset";
                }
                try {
                    this.mCallbacks.onLoaderReset(this.mLoader);
                    if (LoaderManagerImpl.this.mActivity != null) {
                        LoaderManagerImpl.this.mActivity.mFragments.mNoTransactionsBecause = str;
                    }
                } catch (Throwable th) {
                    Throwable th2 = th;
                    if (LoaderManagerImpl.this.mActivity != null) {
                        LoaderManagerImpl.this.mActivity.mFragments.mNoTransactionsBecause = str;
                    }
                    throw th2;
                }
            }
            this.mCallbacks = null;
            this.mData = null;
            this.mHaveData = false;
            if (this.mLoader != null) {
                if (this.mListenerRegistered) {
                    this.mListenerRegistered = false;
                    this.mLoader.unregisterListener(this);
                }
                this.mLoader.reset();
            }
            if (this.mPendingLoader != null) {
                this.mPendingLoader.destroy();
            }
        }

        public void onLoadComplete(Loader<Object> loader, Object obj) {
            StringBuilder sb;
            StringBuilder sb2;
            Loader<Object> loader2 = loader;
            Object obj2 = obj;
            if (LoaderManagerImpl.DEBUG) {
                new StringBuilder();
                int v = Log.v(LoaderManagerImpl.TAG, sb2.append("onLoadComplete: ").append(this).toString());
            }
            if (this.mDestroyed) {
                if (LoaderManagerImpl.DEBUG) {
                    int v2 = Log.v(LoaderManagerImpl.TAG, "  Ignoring load complete -- destroyed");
                }
            } else if (LoaderManagerImpl.this.mLoaders.get(this.mId) == this) {
                LoaderInfo loaderInfo = this.mPendingLoader;
                if (loaderInfo != null) {
                    if (LoaderManagerImpl.DEBUG) {
                        new StringBuilder();
                        int v3 = Log.v(LoaderManagerImpl.TAG, sb.append("  Switching to pending loader: ").append(loaderInfo).toString());
                    }
                    this.mPendingLoader = null;
                    LoaderManagerImpl.this.mLoaders.put(this.mId, null);
                    destroy();
                    LoaderManagerImpl.this.installLoader(loaderInfo);
                    return;
                }
                if (this.mData != obj2 || !this.mHaveData) {
                    this.mData = obj2;
                    this.mHaveData = true;
                    if (this.mStarted) {
                        callOnLoadFinished(loader2, obj2);
                    }
                }
                LoaderInfo loaderInfo2 = LoaderManagerImpl.this.mInactiveLoaders.get(this.mId);
                if (!(loaderInfo2 == null || loaderInfo2 == this)) {
                    loaderInfo2.mDeliveredData = false;
                    loaderInfo2.destroy();
                    LoaderManagerImpl.this.mInactiveLoaders.remove(this.mId);
                }
                if (LoaderManagerImpl.this.mActivity != null && !LoaderManagerImpl.this.hasRunningLoaders()) {
                    LoaderManagerImpl.this.mActivity.mFragments.startPendingDeferredFragments();
                }
            } else if (LoaderManagerImpl.DEBUG) {
                int v4 = Log.v(LoaderManagerImpl.TAG, "  Ignoring load complete -- not active");
            }
        }

        /* access modifiers changed from: package-private */
        public void callOnLoadFinished(Loader<Object> loader, Object obj) {
            StringBuilder sb;
            Loader<Object> loader2 = loader;
            Object obj2 = obj;
            if (this.mCallbacks != null) {
                String str = null;
                if (LoaderManagerImpl.this.mActivity != null) {
                    str = LoaderManagerImpl.this.mActivity.mFragments.mNoTransactionsBecause;
                    LoaderManagerImpl.this.mActivity.mFragments.mNoTransactionsBecause = "onLoadFinished";
                }
                try {
                    if (LoaderManagerImpl.DEBUG) {
                        new StringBuilder();
                        int v = Log.v(LoaderManagerImpl.TAG, sb.append("  onLoadFinished in ").append(loader2).append(": ").append(loader2.dataToString(obj2)).toString());
                    }
                    this.mCallbacks.onLoadFinished(loader2, obj2);
                    if (LoaderManagerImpl.this.mActivity != null) {
                        LoaderManagerImpl.this.mActivity.mFragments.mNoTransactionsBecause = str;
                    }
                    this.mDeliveredData = true;
                } catch (Throwable th) {
                    Throwable th2 = th;
                    if (LoaderManagerImpl.this.mActivity != null) {
                        LoaderManagerImpl.this.mActivity.mFragments.mNoTransactionsBecause = str;
                    }
                    throw th2;
                }
            }
        }

        public String toString() {
            StringBuilder sb;
            new StringBuilder(64);
            StringBuilder sb2 = sb;
            StringBuilder append = sb2.append("LoaderInfo{");
            StringBuilder append2 = sb2.append(Integer.toHexString(System.identityHashCode(this)));
            StringBuilder append3 = sb2.append(" #");
            StringBuilder append4 = sb2.append(this.mId);
            StringBuilder append5 = sb2.append(" : ");
            DebugUtils.buildShortClassTag(this.mLoader, sb2);
            StringBuilder append6 = sb2.append("}}");
            return sb2.toString();
        }

        public void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
            StringBuilder sb;
            StringBuilder sb2;
            String str2 = str;
            FileDescriptor fileDescriptor2 = fileDescriptor;
            PrintWriter printWriter2 = printWriter;
            String[] strArr2 = strArr;
            printWriter2.print(str2);
            printWriter2.print("mId=");
            printWriter2.print(this.mId);
            printWriter2.print(" mArgs=");
            printWriter2.println(this.mArgs);
            printWriter2.print(str2);
            printWriter2.print("mCallbacks=");
            printWriter2.println(this.mCallbacks);
            printWriter2.print(str2);
            printWriter2.print("mLoader=");
            printWriter2.println(this.mLoader);
            if (this.mLoader != null) {
                Loader<Object> loader = this.mLoader;
                new StringBuilder();
                loader.dump(sb2.append(str2).append("  ").toString(), fileDescriptor2, printWriter2, strArr2);
            }
            if (this.mHaveData || this.mDeliveredData) {
                printWriter2.print(str2);
                printWriter2.print("mHaveData=");
                printWriter2.print(this.mHaveData);
                printWriter2.print("  mDeliveredData=");
                printWriter2.println(this.mDeliveredData);
                printWriter2.print(str2);
                printWriter2.print("mData=");
                printWriter2.println(this.mData);
            }
            printWriter2.print(str2);
            printWriter2.print("mStarted=");
            printWriter2.print(this.mStarted);
            printWriter2.print(" mReportNextStart=");
            printWriter2.print(this.mReportNextStart);
            printWriter2.print(" mDestroyed=");
            printWriter2.println(this.mDestroyed);
            printWriter2.print(str2);
            printWriter2.print("mRetaining=");
            printWriter2.print(this.mRetaining);
            printWriter2.print(" mRetainingStarted=");
            printWriter2.print(this.mRetainingStarted);
            printWriter2.print(" mListenerRegistered=");
            printWriter2.println(this.mListenerRegistered);
            if (this.mPendingLoader != null) {
                printWriter2.print(str2);
                printWriter2.println("Pending Loader ");
                printWriter2.print(this.mPendingLoader);
                printWriter2.println(":");
                LoaderInfo loaderInfo = this.mPendingLoader;
                new StringBuilder();
                loaderInfo.dump(sb.append(str2).append("  ").toString(), fileDescriptor2, printWriter2, strArr2);
            }
        }
    }

    LoaderManagerImpl(String str, FragmentActivity fragmentActivity, boolean z) {
        SparseArrayCompat<LoaderInfo> sparseArrayCompat;
        SparseArrayCompat<LoaderInfo> sparseArrayCompat2;
        new SparseArrayCompat<>();
        this.mLoaders = sparseArrayCompat;
        new SparseArrayCompat<>();
        this.mInactiveLoaders = sparseArrayCompat2;
        this.mWho = str;
        this.mActivity = fragmentActivity;
        this.mStarted = z;
    }

    /* access modifiers changed from: package-private */
    public void updateActivity(FragmentActivity fragmentActivity) {
        this.mActivity = fragmentActivity;
    }

    private LoaderInfo createLoader(int i, Bundle bundle, LoaderManager.LoaderCallbacks<Object> loaderCallbacks) {
        LoaderInfo loaderInfo;
        int i2 = i;
        Bundle bundle2 = bundle;
        LoaderManager.LoaderCallbacks<Object> loaderCallbacks2 = loaderCallbacks;
        new LoaderInfo(i2, bundle2, loaderCallbacks2);
        LoaderInfo loaderInfo2 = loaderInfo;
        loaderInfo2.mLoader = loaderCallbacks2.onCreateLoader(i2, bundle2);
        return loaderInfo2;
    }

    /* JADX INFO: finally extract failed */
    private LoaderInfo createAndInstallLoader(int i, Bundle bundle, LoaderManager.LoaderCallbacks<Object> loaderCallbacks) {
        int i2 = i;
        Bundle bundle2 = bundle;
        LoaderManager.LoaderCallbacks<Object> loaderCallbacks2 = loaderCallbacks;
        try {
            this.mCreatingLoader = true;
            LoaderInfo createLoader = createLoader(i2, bundle2, loaderCallbacks2);
            installLoader(createLoader);
            this.mCreatingLoader = false;
            return createLoader;
        } catch (Throwable th) {
            this.mCreatingLoader = false;
            throw th;
        }
    }

    /* access modifiers changed from: package-private */
    public void installLoader(LoaderInfo loaderInfo) {
        LoaderInfo loaderInfo2 = loaderInfo;
        this.mLoaders.put(loaderInfo2.mId, loaderInfo2);
        if (this.mStarted) {
            loaderInfo2.start();
        }
    }

    public <D> Loader<D> initLoader(int i, Bundle bundle, LoaderManager.LoaderCallbacks<D> loaderCallbacks) {
        StringBuilder sb;
        StringBuilder sb2;
        StringBuilder sb3;
        Throwable th;
        int i2 = i;
        Bundle bundle2 = bundle;
        LoaderManager.LoaderCallbacks<D> loaderCallbacks2 = loaderCallbacks;
        if (this.mCreatingLoader) {
            Throwable th2 = th;
            new IllegalStateException("Called while creating a loader");
            throw th2;
        }
        LoaderInfo loaderInfo = this.mLoaders.get(i2);
        if (DEBUG) {
            new StringBuilder();
            int v = Log.v(TAG, sb3.append("initLoader in ").append(this).append(": args=").append(bundle2).toString());
        }
        if (loaderInfo == null) {
            loaderInfo = createAndInstallLoader(i2, bundle2, loaderCallbacks2);
            if (DEBUG) {
                new StringBuilder();
                int v2 = Log.v(TAG, sb2.append("  Created new loader ").append(loaderInfo).toString());
            }
        } else {
            if (DEBUG) {
                new StringBuilder();
                int v3 = Log.v(TAG, sb.append("  Re-using existing loader ").append(loaderInfo).toString());
            }
            loaderInfo.mCallbacks = loaderCallbacks2;
        }
        if (loaderInfo.mHaveData && this.mStarted) {
            loaderInfo.callOnLoadFinished(loaderInfo.mLoader, loaderInfo.mData);
        }
        return loaderInfo.mLoader;
    }

    public <D> Loader<D> restartLoader(int i, Bundle bundle, LoaderManager.LoaderCallbacks<D> loaderCallbacks) {
        StringBuilder sb;
        StringBuilder sb2;
        StringBuilder sb3;
        StringBuilder sb4;
        Throwable th;
        int i2 = i;
        Bundle bundle2 = bundle;
        LoaderManager.LoaderCallbacks<D> loaderCallbacks2 = loaderCallbacks;
        if (this.mCreatingLoader) {
            Throwable th2 = th;
            new IllegalStateException("Called while creating a loader");
            throw th2;
        }
        LoaderInfo loaderInfo = this.mLoaders.get(i2);
        if (DEBUG) {
            new StringBuilder();
            int v = Log.v(TAG, sb4.append("restartLoader in ").append(this).append(": args=").append(bundle2).toString());
        }
        if (loaderInfo != null) {
            LoaderInfo loaderInfo2 = this.mInactiveLoaders.get(i2);
            if (loaderInfo2 == null) {
                if (DEBUG) {
                    new StringBuilder();
                    int v2 = Log.v(TAG, sb.append("  Making last loader inactive: ").append(loaderInfo).toString());
                }
                loaderInfo.mLoader.abandon();
                this.mInactiveLoaders.put(i2, loaderInfo);
            } else if (loaderInfo.mHaveData) {
                if (DEBUG) {
                    new StringBuilder();
                    int v3 = Log.v(TAG, sb3.append("  Removing last inactive loader: ").append(loaderInfo).toString());
                }
                loaderInfo2.mDeliveredData = false;
                loaderInfo2.destroy();
                loaderInfo.mLoader.abandon();
                this.mInactiveLoaders.put(i2, loaderInfo);
            } else if (!loaderInfo.mStarted) {
                if (DEBUG) {
                    int v4 = Log.v(TAG, "  Current loader is stopped; replacing");
                }
                this.mLoaders.put(i2, null);
                loaderInfo.destroy();
            } else {
                if (loaderInfo.mPendingLoader != null) {
                    if (DEBUG) {
                        new StringBuilder();
                        int v5 = Log.v(TAG, sb2.append("  Removing pending loader: ").append(loaderInfo.mPendingLoader).toString());
                    }
                    loaderInfo.mPendingLoader.destroy();
                    loaderInfo.mPendingLoader = null;
                }
                if (DEBUG) {
                    int v6 = Log.v(TAG, "  Enqueuing as new pending loader");
                }
                loaderInfo.mPendingLoader = createLoader(i2, bundle2, loaderCallbacks2);
                return loaderInfo.mPendingLoader.mLoader;
            }
        }
        return createAndInstallLoader(i2, bundle2, loaderCallbacks2).mLoader;
    }

    public void destroyLoader(int i) {
        StringBuilder sb;
        Throwable th;
        int i2 = i;
        if (this.mCreatingLoader) {
            Throwable th2 = th;
            new IllegalStateException("Called while creating a loader");
            throw th2;
        }
        if (DEBUG) {
            new StringBuilder();
            int v = Log.v(TAG, sb.append("destroyLoader in ").append(this).append(" of ").append(i2).toString());
        }
        int indexOfKey = this.mLoaders.indexOfKey(i2);
        if (indexOfKey >= 0) {
            LoaderInfo valueAt = this.mLoaders.valueAt(indexOfKey);
            this.mLoaders.removeAt(indexOfKey);
            valueAt.destroy();
        }
        int indexOfKey2 = this.mInactiveLoaders.indexOfKey(i2);
        if (indexOfKey2 >= 0) {
            LoaderInfo valueAt2 = this.mInactiveLoaders.valueAt(indexOfKey2);
            this.mInactiveLoaders.removeAt(indexOfKey2);
            valueAt2.destroy();
        }
        if (this.mActivity != null && !hasRunningLoaders()) {
            this.mActivity.mFragments.startPendingDeferredFragments();
        }
    }

    public <D> Loader<D> getLoader(int i) {
        Throwable th;
        int i2 = i;
        if (this.mCreatingLoader) {
            Throwable th2 = th;
            new IllegalStateException("Called while creating a loader");
            throw th2;
        }
        LoaderInfo loaderInfo = this.mLoaders.get(i2);
        if (loaderInfo == null) {
            return null;
        }
        if (loaderInfo.mPendingLoader != null) {
            return loaderInfo.mPendingLoader.mLoader;
        }
        return loaderInfo.mLoader;
    }

    /* access modifiers changed from: package-private */
    public void doStart() {
        RuntimeException runtimeException;
        StringBuilder sb;
        StringBuilder sb2;
        if (DEBUG) {
            new StringBuilder();
            int v = Log.v(TAG, sb2.append("Starting in ").append(this).toString());
        }
        if (this.mStarted) {
            new RuntimeException("here");
            RuntimeException runtimeException2 = runtimeException;
            Throwable fillInStackTrace = runtimeException2.fillInStackTrace();
            new StringBuilder();
            int w = Log.w(TAG, sb.append("Called doStart when already started: ").append(this).toString(), runtimeException2);
            return;
        }
        this.mStarted = true;
        for (int size = this.mLoaders.size() - 1; size >= 0; size--) {
            this.mLoaders.valueAt(size).start();
        }
    }

    /* access modifiers changed from: package-private */
    public void doStop() {
        RuntimeException runtimeException;
        StringBuilder sb;
        StringBuilder sb2;
        if (DEBUG) {
            new StringBuilder();
            int v = Log.v(TAG, sb2.append("Stopping in ").append(this).toString());
        }
        if (!this.mStarted) {
            new RuntimeException("here");
            RuntimeException runtimeException2 = runtimeException;
            Throwable fillInStackTrace = runtimeException2.fillInStackTrace();
            new StringBuilder();
            int w = Log.w(TAG, sb.append("Called doStop when not started: ").append(this).toString(), runtimeException2);
            return;
        }
        for (int size = this.mLoaders.size() - 1; size >= 0; size--) {
            this.mLoaders.valueAt(size).stop();
        }
        this.mStarted = false;
    }

    /* access modifiers changed from: package-private */
    public void doRetain() {
        RuntimeException runtimeException;
        StringBuilder sb;
        StringBuilder sb2;
        if (DEBUG) {
            new StringBuilder();
            int v = Log.v(TAG, sb2.append("Retaining in ").append(this).toString());
        }
        if (!this.mStarted) {
            new RuntimeException("here");
            RuntimeException runtimeException2 = runtimeException;
            Throwable fillInStackTrace = runtimeException2.fillInStackTrace();
            new StringBuilder();
            int w = Log.w(TAG, sb.append("Called doRetain when not started: ").append(this).toString(), runtimeException2);
            return;
        }
        this.mRetaining = true;
        this.mStarted = false;
        for (int size = this.mLoaders.size() - 1; size >= 0; size--) {
            this.mLoaders.valueAt(size).retain();
        }
    }

    /* access modifiers changed from: package-private */
    public void finishRetain() {
        StringBuilder sb;
        if (this.mRetaining) {
            if (DEBUG) {
                new StringBuilder();
                int v = Log.v(TAG, sb.append("Finished Retaining in ").append(this).toString());
            }
            this.mRetaining = false;
            for (int size = this.mLoaders.size() - 1; size >= 0; size--) {
                this.mLoaders.valueAt(size).finishRetain();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void doReportNextStart() {
        for (int size = this.mLoaders.size() - 1; size >= 0; size--) {
            this.mLoaders.valueAt(size).mReportNextStart = true;
        }
    }

    /* access modifiers changed from: package-private */
    public void doReportStart() {
        for (int size = this.mLoaders.size() - 1; size >= 0; size--) {
            this.mLoaders.valueAt(size).reportStart();
        }
    }

    /* access modifiers changed from: package-private */
    public void doDestroy() {
        StringBuilder sb;
        StringBuilder sb2;
        if (!this.mRetaining) {
            if (DEBUG) {
                new StringBuilder();
                int v = Log.v(TAG, sb2.append("Destroying Active in ").append(this).toString());
            }
            for (int size = this.mLoaders.size() - 1; size >= 0; size--) {
                this.mLoaders.valueAt(size).destroy();
            }
            this.mLoaders.clear();
        }
        if (DEBUG) {
            new StringBuilder();
            int v2 = Log.v(TAG, sb.append("Destroying Inactive in ").append(this).toString());
        }
        for (int size2 = this.mInactiveLoaders.size() - 1; size2 >= 0; size2--) {
            this.mInactiveLoaders.valueAt(size2).destroy();
        }
        this.mInactiveLoaders.clear();
    }

    public String toString() {
        StringBuilder sb;
        new StringBuilder(128);
        StringBuilder sb2 = sb;
        StringBuilder append = sb2.append("LoaderManager{");
        StringBuilder append2 = sb2.append(Integer.toHexString(System.identityHashCode(this)));
        StringBuilder append3 = sb2.append(" in ");
        DebugUtils.buildShortClassTag(this.mActivity, sb2);
        StringBuilder append4 = sb2.append("}}");
        return sb2.toString();
    }

    public void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        StringBuilder sb;
        StringBuilder sb2;
        String str2 = str;
        FileDescriptor fileDescriptor2 = fileDescriptor;
        PrintWriter printWriter2 = printWriter;
        String[] strArr2 = strArr;
        if (this.mLoaders.size() > 0) {
            printWriter2.print(str2);
            printWriter2.println("Active Loaders:");
            new StringBuilder();
            String sb3 = sb2.append(str2).append("    ").toString();
            for (int i = 0; i < this.mLoaders.size(); i++) {
                LoaderInfo valueAt = this.mLoaders.valueAt(i);
                printWriter2.print(str2);
                printWriter2.print("  #");
                printWriter2.print(this.mLoaders.keyAt(i));
                printWriter2.print(": ");
                printWriter2.println(valueAt.toString());
                valueAt.dump(sb3, fileDescriptor2, printWriter2, strArr2);
            }
        }
        if (this.mInactiveLoaders.size() > 0) {
            printWriter2.print(str2);
            printWriter2.println("Inactive Loaders:");
            new StringBuilder();
            String sb4 = sb.append(str2).append("    ").toString();
            for (int i2 = 0; i2 < this.mInactiveLoaders.size(); i2++) {
                LoaderInfo valueAt2 = this.mInactiveLoaders.valueAt(i2);
                printWriter2.print(str2);
                printWriter2.print("  #");
                printWriter2.print(this.mInactiveLoaders.keyAt(i2));
                printWriter2.print(": ");
                printWriter2.println(valueAt2.toString());
                valueAt2.dump(sb4, fileDescriptor2, printWriter2, strArr2);
            }
        }
    }

    public boolean hasRunningLoaders() {
        boolean z = false;
        int size = this.mLoaders.size();
        for (int i = 0; i < size; i++) {
            LoaderInfo valueAt = this.mLoaders.valueAt(i);
            z |= valueAt.mStarted && !valueAt.mDeliveredData;
        }
        return z;
    }
}
