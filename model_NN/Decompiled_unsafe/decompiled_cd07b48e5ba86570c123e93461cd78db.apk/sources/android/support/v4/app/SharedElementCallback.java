package android.support.v4.app;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import android.view.View;
import android.widget.ImageView;
import java.util.List;
import java.util.Map;

public abstract class SharedElementCallback {
    private static final String BUNDLE_SNAPSHOT_BITMAP = "sharedElement:snapshot:bitmap";
    private static final String BUNDLE_SNAPSHOT_IMAGE_MATRIX = "sharedElement:snapshot:imageMatrix";
    private static final String BUNDLE_SNAPSHOT_IMAGE_SCALETYPE = "sharedElement:snapshot:imageScaleType";
    private static int MAX_IMAGE_SIZE = AccessibilityEventCompat.TYPE_TOUCH_INTERACTION_START;
    private Matrix mTempMatrix;

    public void onSharedElementStart(List<String> list, List<View> list2, List<View> list3) {
    }

    public void onSharedElementEnd(List<String> list, List<View> list2, List<View> list3) {
    }

    public void onRejectSharedElements(List<View> list) {
    }

    public void onMapSharedElements(List<String> list, Map<String, View> map) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    public Parcelable onCaptureSharedElementSnapshot(View view, Matrix matrix, RectF rectF) {
        Canvas canvas;
        Matrix matrix2;
        Bitmap createDrawableBitmap;
        Bundle bundle;
        View view2 = view;
        Matrix matrix3 = matrix;
        RectF rectF2 = rectF;
        if (view2 instanceof ImageView) {
            ImageView imageView = (ImageView) view2;
            Drawable drawable = imageView.getDrawable();
            Drawable background = imageView.getBackground();
            if (!(drawable == null || background != null || (createDrawableBitmap = createDrawableBitmap(drawable)) == null)) {
                new Bundle();
                Bundle bundle2 = bundle;
                bundle2.putParcelable(BUNDLE_SNAPSHOT_BITMAP, createDrawableBitmap);
                bundle2.putString(BUNDLE_SNAPSHOT_IMAGE_SCALETYPE, imageView.getScaleType().toString());
                if (imageView.getScaleType() == ImageView.ScaleType.MATRIX) {
                    float[] fArr = new float[9];
                    imageView.getImageMatrix().getValues(fArr);
                    bundle2.putFloatArray(BUNDLE_SNAPSHOT_IMAGE_MATRIX, fArr);
                }
                return bundle2;
            }
        }
        int round = Math.round(rectF2.width());
        int round2 = Math.round(rectF2.height());
        Bitmap bitmap = null;
        if (round > 0 && round2 > 0) {
            float min = Math.min(1.0f, ((float) MAX_IMAGE_SIZE) / ((float) (round * round2)));
            int i = (int) (((float) round) * min);
            int i2 = (int) (((float) round2) * min);
            if (this.mTempMatrix == null) {
                new Matrix();
                this.mTempMatrix = matrix2;
            }
            this.mTempMatrix.set(matrix3);
            boolean postTranslate = this.mTempMatrix.postTranslate(-rectF2.left, -rectF2.top);
            boolean postScale = this.mTempMatrix.postScale(min, min);
            bitmap = Bitmap.createBitmap(i, i2, Bitmap.Config.ARGB_8888);
            new Canvas(bitmap);
            Canvas canvas2 = canvas;
            canvas2.concat(this.mTempMatrix);
            view2.draw(canvas2);
        }
        return bitmap;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    private static Bitmap createDrawableBitmap(Drawable drawable) {
        Canvas canvas;
        Drawable drawable2 = drawable;
        int intrinsicWidth = drawable2.getIntrinsicWidth();
        int intrinsicHeight = drawable2.getIntrinsicHeight();
        if (intrinsicWidth <= 0 || intrinsicHeight <= 0) {
            return null;
        }
        float min = Math.min(1.0f, ((float) MAX_IMAGE_SIZE) / ((float) (intrinsicWidth * intrinsicHeight)));
        if ((drawable2 instanceof BitmapDrawable) && min == 1.0f) {
            return ((BitmapDrawable) drawable2).getBitmap();
        }
        int i = (int) (((float) intrinsicWidth) * min);
        int i2 = (int) (((float) intrinsicHeight) * min);
        Bitmap createBitmap = Bitmap.createBitmap(i, i2, Bitmap.Config.ARGB_8888);
        new Canvas(createBitmap);
        Rect bounds = drawable2.getBounds();
        int i3 = bounds.left;
        int i4 = bounds.top;
        int i5 = bounds.right;
        int i6 = bounds.bottom;
        drawable2.setBounds(0, 0, i, i2);
        drawable2.draw(canvas);
        drawable2.setBounds(i3, i4, i5, i6);
        return createBitmap;
    }

    public View onCreateSnapshotView(Context context, Parcelable parcelable) {
        ImageView imageView;
        ImageView imageView2;
        Matrix matrix;
        Context context2 = context;
        Parcelable parcelable2 = parcelable;
        ImageView imageView3 = null;
        if (parcelable2 instanceof Bundle) {
            Bundle bundle = (Bundle) parcelable2;
            Bitmap bitmap = (Bitmap) bundle.getParcelable(BUNDLE_SNAPSHOT_BITMAP);
            if (bitmap == null) {
                return null;
            }
            new ImageView(context2);
            ImageView imageView4 = imageView2;
            imageView3 = imageView4;
            imageView4.setImageBitmap(bitmap);
            imageView4.setScaleType(ImageView.ScaleType.valueOf(bundle.getString(BUNDLE_SNAPSHOT_IMAGE_SCALETYPE)));
            if (imageView4.getScaleType() == ImageView.ScaleType.MATRIX) {
                float[] floatArray = bundle.getFloatArray(BUNDLE_SNAPSHOT_IMAGE_MATRIX);
                new Matrix();
                Matrix matrix2 = matrix;
                matrix2.setValues(floatArray);
                imageView4.setImageMatrix(matrix2);
            }
        } else if (parcelable2 instanceof Bitmap) {
            new ImageView(context2);
            imageView3 = imageView;
            imageView3.setImageBitmap((Bitmap) parcelable2);
        }
        return imageView3;
    }
}
