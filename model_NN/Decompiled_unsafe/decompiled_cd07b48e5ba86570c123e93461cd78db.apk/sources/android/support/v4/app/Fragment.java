package android.support.v4.app;

import android.app.Activity;
import android.content.ComponentCallbacks;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.util.DebugUtils;
import android.support.v4.util.SimpleArrayMap;
import android.support.v4.view.LayoutInflaterCompat;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import java.io.FileDescriptor;
import java.io.PrintWriter;

public class Fragment implements ComponentCallbacks, View.OnCreateContextMenuListener {
    static final int ACTIVITY_CREATED = 2;
    static final int CREATED = 1;
    static final int INITIALIZING = 0;
    static final int RESUMED = 5;
    static final int STARTED = 4;
    static final int STOPPED = 3;
    static final Object USE_DEFAULT_TRANSITION;
    private static final SimpleArrayMap<String, Class<?>> sClassMap;
    FragmentActivity mActivity;
    boolean mAdded;
    Boolean mAllowEnterTransitionOverlap;
    Boolean mAllowReturnTransitionOverlap;
    View mAnimatingAway;
    Bundle mArguments;
    int mBackStackNesting;
    boolean mCalled;
    boolean mCheckedForLoaderManager;
    FragmentManagerImpl mChildFragmentManager;
    ViewGroup mContainer;
    int mContainerId;
    boolean mDeferStart;
    boolean mDetached;
    Object mEnterTransition = null;
    SharedElementCallback mEnterTransitionCallback = null;
    Object mExitTransition = null;
    SharedElementCallback mExitTransitionCallback = null;
    int mFragmentId;
    FragmentManagerImpl mFragmentManager;
    boolean mFromLayout;
    boolean mHasMenu;
    boolean mHidden;
    boolean mInLayout;
    int mIndex = -1;
    View mInnerView;
    LoaderManagerImpl mLoaderManager;
    boolean mLoadersStarted;
    boolean mMenuVisible = true;
    int mNextAnim;
    Fragment mParentFragment;
    Object mReenterTransition = USE_DEFAULT_TRANSITION;
    boolean mRemoving;
    boolean mRestored;
    boolean mResumed;
    boolean mRetainInstance;
    boolean mRetaining;
    Object mReturnTransition = USE_DEFAULT_TRANSITION;
    Bundle mSavedFragmentState;
    SparseArray<Parcelable> mSavedViewState;
    Object mSharedElementEnterTransition = null;
    Object mSharedElementReturnTransition = USE_DEFAULT_TRANSITION;
    int mState = 0;
    int mStateAfterAnimating;
    String mTag;
    Fragment mTarget;
    int mTargetIndex = -1;
    int mTargetRequestCode;
    boolean mUserVisibleHint = true;
    View mView;
    String mWho;

    static {
        SimpleArrayMap<String, Class<?>> simpleArrayMap;
        Object obj;
        new SimpleArrayMap<>();
        sClassMap = simpleArrayMap;
        new Object();
        USE_DEFAULT_TRANSITION = obj;
    }

    public static class SavedState implements Parcelable {
        public static final Parcelable.Creator<SavedState> CREATOR;
        final Bundle mState;

        SavedState(Bundle bundle) {
            this.mState = bundle;
        }

        SavedState(Parcel parcel, ClassLoader classLoader) {
            ClassLoader classLoader2 = classLoader;
            this.mState = parcel.readBundle();
            if (classLoader2 != null && this.mState != null) {
                this.mState.setClassLoader(classLoader2);
            }
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeBundle(this.mState);
        }

        static {
            Parcelable.Creator<SavedState> creator;
            new Parcelable.Creator<SavedState>() {
                public SavedState createFromParcel(Parcel parcel) {
                    SavedState savedState;
                    new SavedState(parcel, null);
                    return savedState;
                }

                public SavedState[] newArray(int i) {
                    return new SavedState[i];
                }
            };
            CREATOR = creator;
        }
    }

    public static class InstantiationException extends RuntimeException {
        public InstantiationException(String str, Exception exc) {
            super(str, exc);
        }
    }

    public static Fragment instantiate(Context context, String str) {
        return instantiate(context, str, null);
    }

    public static Fragment instantiate(Context context, String str, @Nullable Bundle bundle) {
        Throwable th;
        StringBuilder sb;
        Throwable th2;
        StringBuilder sb2;
        Throwable th3;
        StringBuilder sb3;
        Context context2 = context;
        String str2 = str;
        Bundle bundle2 = bundle;
        try {
            Class<?> cls = sClassMap.get(str2);
            if (cls == null) {
                cls = context2.getClassLoader().loadClass(str2);
                Class<?> put = sClassMap.put(str2, cls);
            }
            Fragment fragment = (Fragment) cls.newInstance();
            if (bundle2 != null) {
                bundle2.setClassLoader(fragment.getClass().getClassLoader());
                fragment.mArguments = bundle2;
            }
            return fragment;
        } catch (ClassNotFoundException e) {
            ClassNotFoundException classNotFoundException = e;
            Throwable th4 = th3;
            new StringBuilder();
            new InstantiationException(sb3.append("Unable to instantiate fragment ").append(str2).append(": make sure class name exists, is public, and has an").append(" empty constructor that is public").toString(), classNotFoundException);
            throw th4;
        } catch (InstantiationException e2) {
            InstantiationException instantiationException = e2;
            Throwable th5 = th2;
            new StringBuilder();
            new InstantiationException(sb2.append("Unable to instantiate fragment ").append(str2).append(": make sure class name exists, is public, and has an").append(" empty constructor that is public").toString(), instantiationException);
            throw th5;
        } catch (IllegalAccessException e3) {
            IllegalAccessException illegalAccessException = e3;
            Throwable th6 = th;
            new StringBuilder();
            new InstantiationException(sb.append("Unable to instantiate fragment ").append(str2).append(": make sure class name exists, is public, and has an").append(" empty constructor that is public").toString(), illegalAccessException);
            throw th6;
        }
    }

    static boolean isSupportFragmentClass(Context context, String str) {
        Context context2 = context;
        String str2 = str;
        try {
            Class<?> cls = sClassMap.get(str2);
            if (cls == null) {
                cls = context2.getClassLoader().loadClass(str2);
                Class<?> put = sClassMap.put(str2, cls);
            }
            return Fragment.class.isAssignableFrom(cls);
        } catch (ClassNotFoundException e) {
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public final void restoreViewState(Bundle bundle) {
        Throwable th;
        StringBuilder sb;
        Bundle bundle2 = bundle;
        if (this.mSavedViewState != null) {
            this.mInnerView.restoreHierarchyState(this.mSavedViewState);
            this.mSavedViewState = null;
        }
        this.mCalled = false;
        onViewStateRestored(bundle2);
        if (!this.mCalled) {
            Throwable th2 = th;
            new StringBuilder();
            new SuperNotCalledException(sb.append("Fragment ").append(this).append(" did not call through to super.onViewStateRestored()").toString());
            throw th2;
        }
    }

    /* access modifiers changed from: package-private */
    public final void setIndex(int i, Fragment fragment) {
        StringBuilder sb;
        StringBuilder sb2;
        Fragment fragment2 = fragment;
        this.mIndex = i;
        if (fragment2 != null) {
            new StringBuilder();
            this.mWho = sb2.append(fragment2.mWho).append(":").append(this.mIndex).toString();
            return;
        }
        new StringBuilder();
        this.mWho = sb.append("android:fragment:").append(this.mIndex).toString();
    }

    /* access modifiers changed from: package-private */
    public final boolean isInBackStack() {
        return this.mBackStackNesting > 0;
    }

    public final boolean equals(Object obj) {
        return super.equals(obj);
    }

    public final int hashCode() {
        return super.hashCode();
    }

    public String toString() {
        StringBuilder sb;
        new StringBuilder(128);
        StringBuilder sb2 = sb;
        DebugUtils.buildShortClassTag(this, sb2);
        if (this.mIndex >= 0) {
            StringBuilder append = sb2.append(" #");
            StringBuilder append2 = sb2.append(this.mIndex);
        }
        if (this.mFragmentId != 0) {
            StringBuilder append3 = sb2.append(" id=0x");
            StringBuilder append4 = sb2.append(Integer.toHexString(this.mFragmentId));
        }
        if (this.mTag != null) {
            StringBuilder append5 = sb2.append(" ");
            StringBuilder append6 = sb2.append(this.mTag);
        }
        StringBuilder append7 = sb2.append('}');
        return sb2.toString();
    }

    public final int getId() {
        return this.mFragmentId;
    }

    public final String getTag() {
        return this.mTag;
    }

    public void setArguments(Bundle bundle) {
        Throwable th;
        Bundle bundle2 = bundle;
        if (this.mIndex >= 0) {
            Throwable th2 = th;
            new IllegalStateException("Fragment already active");
            throw th2;
        }
        this.mArguments = bundle2;
    }

    public final Bundle getArguments() {
        return this.mArguments;
    }

    public void setInitialSavedState(SavedState savedState) {
        Throwable th;
        SavedState savedState2 = savedState;
        if (this.mIndex >= 0) {
            Throwable th2 = th;
            new IllegalStateException("Fragment already active");
            throw th2;
        }
        this.mSavedFragmentState = (savedState2 == null || savedState2.mState == null) ? null : savedState2.mState;
    }

    public void setTargetFragment(Fragment fragment, int i) {
        this.mTarget = fragment;
        this.mTargetRequestCode = i;
    }

    public final Fragment getTargetFragment() {
        return this.mTarget;
    }

    public final int getTargetRequestCode() {
        return this.mTargetRequestCode;
    }

    public final FragmentActivity getActivity() {
        return this.mActivity;
    }

    public final Resources getResources() {
        Throwable th;
        StringBuilder sb;
        if (this.mActivity != null) {
            return this.mActivity.getResources();
        }
        Throwable th2 = th;
        new StringBuilder();
        new IllegalStateException(sb.append("Fragment ").append(this).append(" not attached to Activity").toString());
        throw th2;
    }

    public final CharSequence getText(int i) {
        return getResources().getText(i);
    }

    public final String getString(int i) {
        return getResources().getString(i);
    }

    public final String getString(int i, Object... objArr) {
        return getResources().getString(i, objArr);
    }

    public final FragmentManager getFragmentManager() {
        return this.mFragmentManager;
    }

    public final FragmentManager getChildFragmentManager() {
        if (this.mChildFragmentManager == null) {
            instantiateChildFragmentManager();
            if (this.mState >= 5) {
                this.mChildFragmentManager.dispatchResume();
            } else if (this.mState >= 4) {
                this.mChildFragmentManager.dispatchStart();
            } else if (this.mState >= 2) {
                this.mChildFragmentManager.dispatchActivityCreated();
            } else if (this.mState >= 1) {
                this.mChildFragmentManager.dispatchCreate();
            }
        }
        return this.mChildFragmentManager;
    }

    public final Fragment getParentFragment() {
        return this.mParentFragment;
    }

    public final boolean isAdded() {
        return this.mActivity != null && this.mAdded;
    }

    public final boolean isDetached() {
        return this.mDetached;
    }

    public final boolean isRemoving() {
        return this.mRemoving;
    }

    public final boolean isInLayout() {
        return this.mInLayout;
    }

    public final boolean isResumed() {
        return this.mResumed;
    }

    public final boolean isVisible() {
        return isAdded() && !isHidden() && this.mView != null && this.mView.getWindowToken() != null && this.mView.getVisibility() == 0;
    }

    public final boolean isHidden() {
        return this.mHidden;
    }

    public final boolean hasOptionsMenu() {
        return this.mHasMenu;
    }

    public final boolean isMenuVisible() {
        return this.mMenuVisible;
    }

    public void onHiddenChanged(boolean z) {
    }

    public void setRetainInstance(boolean z) {
        Throwable th;
        boolean z2 = z;
        if (!z2 || this.mParentFragment == null) {
            this.mRetainInstance = z2;
            return;
        }
        Throwable th2 = th;
        new IllegalStateException("Can't retain fragements that are nested in other fragments");
        throw th2;
    }

    public final boolean getRetainInstance() {
        return this.mRetainInstance;
    }

    public void setHasOptionsMenu(boolean z) {
        boolean z2 = z;
        if (this.mHasMenu != z2) {
            this.mHasMenu = z2;
            if (isAdded() && !isHidden()) {
                this.mActivity.supportInvalidateOptionsMenu();
            }
        }
    }

    public void setMenuVisibility(boolean z) {
        boolean z2 = z;
        if (this.mMenuVisible != z2) {
            this.mMenuVisible = z2;
            if (this.mHasMenu && isAdded() && !isHidden()) {
                this.mActivity.supportInvalidateOptionsMenu();
            }
        }
    }

    public void setUserVisibleHint(boolean z) {
        boolean z2 = z;
        if (!this.mUserVisibleHint && z2 && this.mState < 4) {
            this.mFragmentManager.performPendingDeferredStart(this);
        }
        this.mUserVisibleHint = z2;
        this.mDeferStart = !z2;
    }

    public boolean getUserVisibleHint() {
        return this.mUserVisibleHint;
    }

    public LoaderManager getLoaderManager() {
        Throwable th;
        StringBuilder sb;
        if (this.mLoaderManager != null) {
            return this.mLoaderManager;
        }
        if (this.mActivity == null) {
            Throwable th2 = th;
            new StringBuilder();
            new IllegalStateException(sb.append("Fragment ").append(this).append(" not attached to Activity").toString());
            throw th2;
        }
        this.mCheckedForLoaderManager = true;
        this.mLoaderManager = this.mActivity.getLoaderManager(this.mWho, this.mLoadersStarted, true);
        return this.mLoaderManager;
    }

    public void startActivity(Intent intent) {
        Throwable th;
        StringBuilder sb;
        Intent intent2 = intent;
        if (this.mActivity == null) {
            Throwable th2 = th;
            new StringBuilder();
            new IllegalStateException(sb.append("Fragment ").append(this).append(" not attached to Activity").toString());
            throw th2;
        }
        this.mActivity.startActivityFromFragment(this, intent2, -1);
    }

    public void startActivityForResult(Intent intent, int i) {
        Throwable th;
        StringBuilder sb;
        Intent intent2 = intent;
        int i2 = i;
        if (this.mActivity == null) {
            Throwable th2 = th;
            new StringBuilder();
            new IllegalStateException(sb.append("Fragment ").append(this).append(" not attached to Activity").toString());
            throw th2;
        }
        this.mActivity.startActivityFromFragment(this, intent2, i2);
    }

    public void onActivityResult(int i, int i2, Intent intent) {
    }

    public LayoutInflater getLayoutInflater(Bundle bundle) {
        LayoutInflater cloneInContext = this.mActivity.getLayoutInflater().cloneInContext(this.mActivity);
        FragmentManager childFragmentManager = getChildFragmentManager();
        LayoutInflaterCompat.setFactory(cloneInContext, this.mChildFragmentManager.getLayoutInflaterFactory());
        return cloneInContext;
    }

    public void onInflate(Activity activity, AttributeSet attributeSet, Bundle bundle) {
        this.mCalled = true;
    }

    public void onAttach(Activity activity) {
        this.mCalled = true;
    }

    public Animation onCreateAnimation(int i, boolean z, int i2) {
        return null;
    }

    public void onCreate(@Nullable Bundle bundle) {
        this.mCalled = true;
    }

    @Nullable
    public View onCreateView(LayoutInflater layoutInflater, @Nullable ViewGroup viewGroup, @Nullable Bundle bundle) {
        return null;
    }

    public void onViewCreated(View view, @Nullable Bundle bundle) {
    }

    @Nullable
    public View getView() {
        return this.mView;
    }

    public void onActivityCreated(@Nullable Bundle bundle) {
        this.mCalled = true;
    }

    public void onViewStateRestored(@Nullable Bundle bundle) {
        this.mCalled = true;
    }

    public void onStart() {
        this.mCalled = true;
        if (!this.mLoadersStarted) {
            this.mLoadersStarted = true;
            if (!this.mCheckedForLoaderManager) {
                this.mCheckedForLoaderManager = true;
                this.mLoaderManager = this.mActivity.getLoaderManager(this.mWho, this.mLoadersStarted, false);
            }
            if (this.mLoaderManager != null) {
                this.mLoaderManager.doStart();
            }
        }
    }

    public void onResume() {
        this.mCalled = true;
    }

    public void onSaveInstanceState(Bundle bundle) {
    }

    public void onConfigurationChanged(Configuration configuration) {
        this.mCalled = true;
    }

    public void onPause() {
        this.mCalled = true;
    }

    public void onStop() {
        this.mCalled = true;
    }

    public void onLowMemory() {
        this.mCalled = true;
    }

    public void onDestroyView() {
        this.mCalled = true;
    }

    public void onDestroy() {
        this.mCalled = true;
        if (!this.mCheckedForLoaderManager) {
            this.mCheckedForLoaderManager = true;
            this.mLoaderManager = this.mActivity.getLoaderManager(this.mWho, this.mLoadersStarted, false);
        }
        if (this.mLoaderManager != null) {
            this.mLoaderManager.doDestroy();
        }
    }

    /* access modifiers changed from: package-private */
    public void initState() {
        this.mIndex = -1;
        this.mWho = null;
        this.mAdded = false;
        this.mRemoving = false;
        this.mResumed = false;
        this.mFromLayout = false;
        this.mInLayout = false;
        this.mRestored = false;
        this.mBackStackNesting = 0;
        this.mFragmentManager = null;
        this.mChildFragmentManager = null;
        this.mActivity = null;
        this.mFragmentId = 0;
        this.mContainerId = 0;
        this.mTag = null;
        this.mHidden = false;
        this.mDetached = false;
        this.mRetaining = false;
        this.mLoaderManager = null;
        this.mLoadersStarted = false;
        this.mCheckedForLoaderManager = false;
    }

    public void onDetach() {
        this.mCalled = true;
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
    }

    public void onPrepareOptionsMenu(Menu menu) {
    }

    public void onDestroyOptionsMenu() {
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        return false;
    }

    public void onOptionsMenuClosed(Menu menu) {
    }

    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        getActivity().onCreateContextMenu(contextMenu, view, contextMenuInfo);
    }

    public void registerForContextMenu(View view) {
        view.setOnCreateContextMenuListener(this);
    }

    public void unregisterForContextMenu(View view) {
        view.setOnCreateContextMenuListener(null);
    }

    public boolean onContextItemSelected(MenuItem menuItem) {
        return false;
    }

    public void setEnterSharedElementCallback(SharedElementCallback sharedElementCallback) {
        this.mEnterTransitionCallback = sharedElementCallback;
    }

    public void setExitSharedElementCallback(SharedElementCallback sharedElementCallback) {
        this.mExitTransitionCallback = sharedElementCallback;
    }

    public void setEnterTransition(Object obj) {
        this.mEnterTransition = obj;
    }

    public Object getEnterTransition() {
        return this.mEnterTransition;
    }

    public void setReturnTransition(Object obj) {
        this.mReturnTransition = obj;
    }

    public Object getReturnTransition() {
        return this.mReturnTransition == USE_DEFAULT_TRANSITION ? getEnterTransition() : this.mReturnTransition;
    }

    public void setExitTransition(Object obj) {
        this.mExitTransition = obj;
    }

    public Object getExitTransition() {
        return this.mExitTransition;
    }

    public void setReenterTransition(Object obj) {
        this.mReenterTransition = obj;
    }

    public Object getReenterTransition() {
        return this.mReenterTransition == USE_DEFAULT_TRANSITION ? getExitTransition() : this.mReenterTransition;
    }

    public void setSharedElementEnterTransition(Object obj) {
        this.mSharedElementEnterTransition = obj;
    }

    public Object getSharedElementEnterTransition() {
        return this.mSharedElementEnterTransition;
    }

    public void setSharedElementReturnTransition(Object obj) {
        this.mSharedElementReturnTransition = obj;
    }

    public Object getSharedElementReturnTransition() {
        return this.mSharedElementReturnTransition == USE_DEFAULT_TRANSITION ? getSharedElementEnterTransition() : this.mSharedElementReturnTransition;
    }

    public void setAllowEnterTransitionOverlap(boolean z) {
        this.mAllowEnterTransitionOverlap = Boolean.valueOf(z);
    }

    public boolean getAllowEnterTransitionOverlap() {
        return this.mAllowEnterTransitionOverlap == null ? true : this.mAllowEnterTransitionOverlap.booleanValue();
    }

    public void setAllowReturnTransitionOverlap(boolean z) {
        this.mAllowReturnTransitionOverlap = Boolean.valueOf(z);
    }

    public boolean getAllowReturnTransitionOverlap() {
        return this.mAllowReturnTransitionOverlap == null ? true : this.mAllowReturnTransitionOverlap.booleanValue();
    }

    public void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        StringBuilder sb;
        StringBuilder sb2;
        StringBuilder sb3;
        String str2 = str;
        FileDescriptor fileDescriptor2 = fileDescriptor;
        PrintWriter printWriter2 = printWriter;
        String[] strArr2 = strArr;
        printWriter2.print(str2);
        printWriter2.print("mFragmentId=#");
        printWriter2.print(Integer.toHexString(this.mFragmentId));
        printWriter2.print(" mContainerId=#");
        printWriter2.print(Integer.toHexString(this.mContainerId));
        printWriter2.print(" mTag=");
        printWriter2.println(this.mTag);
        printWriter2.print(str2);
        printWriter2.print("mState=");
        printWriter2.print(this.mState);
        printWriter2.print(" mIndex=");
        printWriter2.print(this.mIndex);
        printWriter2.print(" mWho=");
        printWriter2.print(this.mWho);
        printWriter2.print(" mBackStackNesting=");
        printWriter2.println(this.mBackStackNesting);
        printWriter2.print(str2);
        printWriter2.print("mAdded=");
        printWriter2.print(this.mAdded);
        printWriter2.print(" mRemoving=");
        printWriter2.print(this.mRemoving);
        printWriter2.print(" mResumed=");
        printWriter2.print(this.mResumed);
        printWriter2.print(" mFromLayout=");
        printWriter2.print(this.mFromLayout);
        printWriter2.print(" mInLayout=");
        printWriter2.println(this.mInLayout);
        printWriter2.print(str2);
        printWriter2.print("mHidden=");
        printWriter2.print(this.mHidden);
        printWriter2.print(" mDetached=");
        printWriter2.print(this.mDetached);
        printWriter2.print(" mMenuVisible=");
        printWriter2.print(this.mMenuVisible);
        printWriter2.print(" mHasMenu=");
        printWriter2.println(this.mHasMenu);
        printWriter2.print(str2);
        printWriter2.print("mRetainInstance=");
        printWriter2.print(this.mRetainInstance);
        printWriter2.print(" mRetaining=");
        printWriter2.print(this.mRetaining);
        printWriter2.print(" mUserVisibleHint=");
        printWriter2.println(this.mUserVisibleHint);
        if (this.mFragmentManager != null) {
            printWriter2.print(str2);
            printWriter2.print("mFragmentManager=");
            printWriter2.println(this.mFragmentManager);
        }
        if (this.mActivity != null) {
            printWriter2.print(str2);
            printWriter2.print("mActivity=");
            printWriter2.println(this.mActivity);
        }
        if (this.mParentFragment != null) {
            printWriter2.print(str2);
            printWriter2.print("mParentFragment=");
            printWriter2.println(this.mParentFragment);
        }
        if (this.mArguments != null) {
            printWriter2.print(str2);
            printWriter2.print("mArguments=");
            printWriter2.println(this.mArguments);
        }
        if (this.mSavedFragmentState != null) {
            printWriter2.print(str2);
            printWriter2.print("mSavedFragmentState=");
            printWriter2.println(this.mSavedFragmentState);
        }
        if (this.mSavedViewState != null) {
            printWriter2.print(str2);
            printWriter2.print("mSavedViewState=");
            printWriter2.println(this.mSavedViewState);
        }
        if (this.mTarget != null) {
            printWriter2.print(str2);
            printWriter2.print("mTarget=");
            printWriter2.print(this.mTarget);
            printWriter2.print(" mTargetRequestCode=");
            printWriter2.println(this.mTargetRequestCode);
        }
        if (this.mNextAnim != 0) {
            printWriter2.print(str2);
            printWriter2.print("mNextAnim=");
            printWriter2.println(this.mNextAnim);
        }
        if (this.mContainer != null) {
            printWriter2.print(str2);
            printWriter2.print("mContainer=");
            printWriter2.println(this.mContainer);
        }
        if (this.mView != null) {
            printWriter2.print(str2);
            printWriter2.print("mView=");
            printWriter2.println(this.mView);
        }
        if (this.mInnerView != null) {
            printWriter2.print(str2);
            printWriter2.print("mInnerView=");
            printWriter2.println(this.mView);
        }
        if (this.mAnimatingAway != null) {
            printWriter2.print(str2);
            printWriter2.print("mAnimatingAway=");
            printWriter2.println(this.mAnimatingAway);
            printWriter2.print(str2);
            printWriter2.print("mStateAfterAnimating=");
            printWriter2.println(this.mStateAfterAnimating);
        }
        if (this.mLoaderManager != null) {
            printWriter2.print(str2);
            printWriter2.println("Loader Manager:");
            LoaderManagerImpl loaderManagerImpl = this.mLoaderManager;
            new StringBuilder();
            loaderManagerImpl.dump(sb3.append(str2).append("  ").toString(), fileDescriptor2, printWriter2, strArr2);
        }
        if (this.mChildFragmentManager != null) {
            printWriter2.print(str2);
            new StringBuilder();
            printWriter2.println(sb.append("Child ").append(this.mChildFragmentManager).append(":").toString());
            FragmentManagerImpl fragmentManagerImpl = this.mChildFragmentManager;
            new StringBuilder();
            fragmentManagerImpl.dump(sb2.append(str2).append("  ").toString(), fileDescriptor2, printWriter2, strArr2);
        }
    }

    /* access modifiers changed from: package-private */
    public Fragment findFragmentByWho(String str) {
        String str2 = str;
        if (str2.equals(this.mWho)) {
            return this;
        }
        if (this.mChildFragmentManager != null) {
            return this.mChildFragmentManager.findFragmentByWho(str2);
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public void instantiateChildFragmentManager() {
        FragmentManagerImpl fragmentManagerImpl;
        FragmentContainer fragmentContainer;
        new FragmentManagerImpl();
        this.mChildFragmentManager = fragmentManagerImpl;
        new FragmentContainer() {
            @Nullable
            public View findViewById(int i) {
                Throwable th;
                int i2 = i;
                if (Fragment.this.mView != null) {
                    return Fragment.this.mView.findViewById(i2);
                }
                Throwable th2 = th;
                new IllegalStateException("Fragment does not have a view");
                throw th2;
            }

            public boolean hasView() {
                return Fragment.this.mView != null;
            }
        };
        this.mChildFragmentManager.attachActivity(this.mActivity, fragmentContainer, this);
    }

    /* access modifiers changed from: package-private */
    public void performCreate(Bundle bundle) {
        Parcelable parcelable;
        Throwable th;
        StringBuilder sb;
        Bundle bundle2 = bundle;
        if (this.mChildFragmentManager != null) {
            this.mChildFragmentManager.noteStateNotSaved();
        }
        this.mCalled = false;
        onCreate(bundle2);
        if (!this.mCalled) {
            Throwable th2 = th;
            new StringBuilder();
            new SuperNotCalledException(sb.append("Fragment ").append(this).append(" did not call through to super.onCreate()").toString());
            throw th2;
        } else if (bundle2 != null && (parcelable = bundle2.getParcelable("android:support:fragments")) != null) {
            if (this.mChildFragmentManager == null) {
                instantiateChildFragmentManager();
            }
            this.mChildFragmentManager.restoreAllState(parcelable, null);
            this.mChildFragmentManager.dispatchCreate();
        }
    }

    /* access modifiers changed from: package-private */
    public View performCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        LayoutInflater layoutInflater2 = layoutInflater;
        ViewGroup viewGroup2 = viewGroup;
        Bundle bundle2 = bundle;
        if (this.mChildFragmentManager != null) {
            this.mChildFragmentManager.noteStateNotSaved();
        }
        return onCreateView(layoutInflater2, viewGroup2, bundle2);
    }

    /* access modifiers changed from: package-private */
    public void performActivityCreated(Bundle bundle) {
        Throwable th;
        StringBuilder sb;
        Bundle bundle2 = bundle;
        if (this.mChildFragmentManager != null) {
            this.mChildFragmentManager.noteStateNotSaved();
        }
        this.mCalled = false;
        onActivityCreated(bundle2);
        if (!this.mCalled) {
            Throwable th2 = th;
            new StringBuilder();
            new SuperNotCalledException(sb.append("Fragment ").append(this).append(" did not call through to super.onActivityCreated()").toString());
            throw th2;
        } else if (this.mChildFragmentManager != null) {
            this.mChildFragmentManager.dispatchActivityCreated();
        }
    }

    /* access modifiers changed from: package-private */
    public void performStart() {
        Throwable th;
        StringBuilder sb;
        if (this.mChildFragmentManager != null) {
            this.mChildFragmentManager.noteStateNotSaved();
            boolean execPendingActions = this.mChildFragmentManager.execPendingActions();
        }
        this.mCalled = false;
        onStart();
        if (!this.mCalled) {
            Throwable th2 = th;
            new StringBuilder();
            new SuperNotCalledException(sb.append("Fragment ").append(this).append(" did not call through to super.onStart()").toString());
            throw th2;
        }
        if (this.mChildFragmentManager != null) {
            this.mChildFragmentManager.dispatchStart();
        }
        if (this.mLoaderManager != null) {
            this.mLoaderManager.doReportStart();
        }
    }

    /* access modifiers changed from: package-private */
    public void performResume() {
        Throwable th;
        StringBuilder sb;
        if (this.mChildFragmentManager != null) {
            this.mChildFragmentManager.noteStateNotSaved();
            boolean execPendingActions = this.mChildFragmentManager.execPendingActions();
        }
        this.mCalled = false;
        onResume();
        if (!this.mCalled) {
            Throwable th2 = th;
            new StringBuilder();
            new SuperNotCalledException(sb.append("Fragment ").append(this).append(" did not call through to super.onResume()").toString());
            throw th2;
        } else if (this.mChildFragmentManager != null) {
            this.mChildFragmentManager.dispatchResume();
            boolean execPendingActions2 = this.mChildFragmentManager.execPendingActions();
        }
    }

    /* access modifiers changed from: package-private */
    public void performConfigurationChanged(Configuration configuration) {
        Configuration configuration2 = configuration;
        onConfigurationChanged(configuration2);
        if (this.mChildFragmentManager != null) {
            this.mChildFragmentManager.dispatchConfigurationChanged(configuration2);
        }
    }

    /* access modifiers changed from: package-private */
    public void performLowMemory() {
        onLowMemory();
        if (this.mChildFragmentManager != null) {
            this.mChildFragmentManager.dispatchLowMemory();
        }
    }

    /* access modifiers changed from: package-private */
    public boolean performCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        Menu menu2 = menu;
        MenuInflater menuInflater2 = menuInflater;
        boolean z = false;
        if (!this.mHidden) {
            if (this.mHasMenu && this.mMenuVisible) {
                z = true;
                onCreateOptionsMenu(menu2, menuInflater2);
            }
            if (this.mChildFragmentManager != null) {
                z |= this.mChildFragmentManager.dispatchCreateOptionsMenu(menu2, menuInflater2);
            }
        }
        return z;
    }

    /* access modifiers changed from: package-private */
    public boolean performPrepareOptionsMenu(Menu menu) {
        Menu menu2 = menu;
        boolean z = false;
        if (!this.mHidden) {
            if (this.mHasMenu && this.mMenuVisible) {
                z = true;
                onPrepareOptionsMenu(menu2);
            }
            if (this.mChildFragmentManager != null) {
                z |= this.mChildFragmentManager.dispatchPrepareOptionsMenu(menu2);
            }
        }
        return z;
    }

    /* access modifiers changed from: package-private */
    public boolean performOptionsItemSelected(MenuItem menuItem) {
        MenuItem menuItem2 = menuItem;
        if (!this.mHidden) {
            if (this.mHasMenu && this.mMenuVisible && onOptionsItemSelected(menuItem2)) {
                return true;
            }
            if (this.mChildFragmentManager != null && this.mChildFragmentManager.dispatchOptionsItemSelected(menuItem2)) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean performContextItemSelected(MenuItem menuItem) {
        MenuItem menuItem2 = menuItem;
        if (!this.mHidden) {
            if (onContextItemSelected(menuItem2)) {
                return true;
            }
            if (this.mChildFragmentManager != null && this.mChildFragmentManager.dispatchContextItemSelected(menuItem2)) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public void performOptionsMenuClosed(Menu menu) {
        Menu menu2 = menu;
        if (!this.mHidden) {
            if (this.mHasMenu && this.mMenuVisible) {
                onOptionsMenuClosed(menu2);
            }
            if (this.mChildFragmentManager != null) {
                this.mChildFragmentManager.dispatchOptionsMenuClosed(menu2);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void performSaveInstanceState(Bundle bundle) {
        Parcelable saveAllState;
        Bundle bundle2 = bundle;
        onSaveInstanceState(bundle2);
        if (this.mChildFragmentManager != null && (saveAllState = this.mChildFragmentManager.saveAllState()) != null) {
            bundle2.putParcelable("android:support:fragments", saveAllState);
        }
    }

    /* access modifiers changed from: package-private */
    public void performPause() {
        Throwable th;
        StringBuilder sb;
        if (this.mChildFragmentManager != null) {
            this.mChildFragmentManager.dispatchPause();
        }
        this.mCalled = false;
        onPause();
        if (!this.mCalled) {
            Throwable th2 = th;
            new StringBuilder();
            new SuperNotCalledException(sb.append("Fragment ").append(this).append(" did not call through to super.onPause()").toString());
            throw th2;
        }
    }

    /* access modifiers changed from: package-private */
    public void performStop() {
        Throwable th;
        StringBuilder sb;
        if (this.mChildFragmentManager != null) {
            this.mChildFragmentManager.dispatchStop();
        }
        this.mCalled = false;
        onStop();
        if (!this.mCalled) {
            Throwable th2 = th;
            new StringBuilder();
            new SuperNotCalledException(sb.append("Fragment ").append(this).append(" did not call through to super.onStop()").toString());
            throw th2;
        }
    }

    /* access modifiers changed from: package-private */
    public void performReallyStop() {
        if (this.mChildFragmentManager != null) {
            this.mChildFragmentManager.dispatchReallyStop();
        }
        if (this.mLoadersStarted) {
            this.mLoadersStarted = false;
            if (!this.mCheckedForLoaderManager) {
                this.mCheckedForLoaderManager = true;
                this.mLoaderManager = this.mActivity.getLoaderManager(this.mWho, this.mLoadersStarted, false);
            }
            if (this.mLoaderManager == null) {
                return;
            }
            if (!this.mActivity.mRetaining) {
                this.mLoaderManager.doStop();
            } else {
                this.mLoaderManager.doRetain();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void performDestroyView() {
        Throwable th;
        StringBuilder sb;
        if (this.mChildFragmentManager != null) {
            this.mChildFragmentManager.dispatchDestroyView();
        }
        this.mCalled = false;
        onDestroyView();
        if (!this.mCalled) {
            Throwable th2 = th;
            new StringBuilder();
            new SuperNotCalledException(sb.append("Fragment ").append(this).append(" did not call through to super.onDestroyView()").toString());
            throw th2;
        } else if (this.mLoaderManager != null) {
            this.mLoaderManager.doReportNextStart();
        }
    }

    /* access modifiers changed from: package-private */
    public void performDestroy() {
        Throwable th;
        StringBuilder sb;
        if (this.mChildFragmentManager != null) {
            this.mChildFragmentManager.dispatchDestroy();
        }
        this.mCalled = false;
        onDestroy();
        if (!this.mCalled) {
            Throwable th2 = th;
            new StringBuilder();
            new SuperNotCalledException(sb.append("Fragment ").append(this).append(" did not call through to super.onDestroy()").toString());
            throw th2;
        }
    }
}
