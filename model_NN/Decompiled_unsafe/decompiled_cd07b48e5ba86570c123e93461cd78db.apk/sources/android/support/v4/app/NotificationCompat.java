package android.support.v4.app;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.NotificationCompatApi20;
import android.support.v4.app.NotificationCompatApi21;
import android.support.v4.app.NotificationCompatBase;
import android.support.v4.app.NotificationCompatJellybean;
import android.support.v4.app.NotificationCompatKitKat;
import android.support.v4.app.RemoteInputCompatBase;
import android.widget.RemoteViews;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class NotificationCompat {
    public static final String CATEGORY_ALARM = "alarm";
    public static final String CATEGORY_CALL = "call";
    public static final String CATEGORY_EMAIL = "email";
    public static final String CATEGORY_ERROR = "err";
    public static final String CATEGORY_EVENT = "event";
    public static final String CATEGORY_MESSAGE = "msg";
    public static final String CATEGORY_PROGRESS = "progress";
    public static final String CATEGORY_PROMO = "promo";
    public static final String CATEGORY_RECOMMENDATION = "recommendation";
    public static final String CATEGORY_SERVICE = "service";
    public static final String CATEGORY_SOCIAL = "social";
    public static final String CATEGORY_STATUS = "status";
    public static final String CATEGORY_SYSTEM = "sys";
    public static final String CATEGORY_TRANSPORT = "transport";
    public static final int COLOR_DEFAULT = 0;
    public static final int DEFAULT_ALL = -1;
    public static final int DEFAULT_LIGHTS = 4;
    public static final int DEFAULT_SOUND = 1;
    public static final int DEFAULT_VIBRATE = 2;
    public static final String EXTRA_BACKGROUND_IMAGE_URI = "android.backgroundImageUri";
    public static final String EXTRA_BIG_TEXT = "android.bigText";
    public static final String EXTRA_COMPACT_ACTIONS = "android.compactActions";
    public static final String EXTRA_INFO_TEXT = "android.infoText";
    public static final String EXTRA_LARGE_ICON = "android.largeIcon";
    public static final String EXTRA_LARGE_ICON_BIG = "android.largeIcon.big";
    public static final String EXTRA_MEDIA_SESSION = "android.mediaSession";
    public static final String EXTRA_PEOPLE = "android.people";
    public static final String EXTRA_PICTURE = "android.picture";
    public static final String EXTRA_PROGRESS = "android.progress";
    public static final String EXTRA_PROGRESS_INDETERMINATE = "android.progressIndeterminate";
    public static final String EXTRA_PROGRESS_MAX = "android.progressMax";
    public static final String EXTRA_SHOW_CHRONOMETER = "android.showChronometer";
    public static final String EXTRA_SHOW_WHEN = "android.showWhen";
    public static final String EXTRA_SMALL_ICON = "android.icon";
    public static final String EXTRA_SUB_TEXT = "android.subText";
    public static final String EXTRA_SUMMARY_TEXT = "android.summaryText";
    public static final String EXTRA_TEMPLATE = "android.template";
    public static final String EXTRA_TEXT = "android.text";
    public static final String EXTRA_TEXT_LINES = "android.textLines";
    public static final String EXTRA_TITLE = "android.title";
    public static final String EXTRA_TITLE_BIG = "android.title.big";
    public static final int FLAG_AUTO_CANCEL = 16;
    public static final int FLAG_FOREGROUND_SERVICE = 64;
    public static final int FLAG_GROUP_SUMMARY = 512;
    public static final int FLAG_HIGH_PRIORITY = 128;
    public static final int FLAG_INSISTENT = 4;
    public static final int FLAG_LOCAL_ONLY = 256;
    public static final int FLAG_NO_CLEAR = 32;
    public static final int FLAG_ONGOING_EVENT = 2;
    public static final int FLAG_ONLY_ALERT_ONCE = 8;
    public static final int FLAG_SHOW_LIGHTS = 1;
    /* access modifiers changed from: private */
    public static final NotificationCompatImpl IMPL;
    public static final int PRIORITY_DEFAULT = 0;
    public static final int PRIORITY_HIGH = 1;
    public static final int PRIORITY_LOW = -1;
    public static final int PRIORITY_MAX = 2;
    public static final int PRIORITY_MIN = -2;
    public static final int STREAM_DEFAULT = -1;
    public static final int VISIBILITY_PRIVATE = 0;
    public static final int VISIBILITY_PUBLIC = 1;
    public static final int VISIBILITY_SECRET = -1;

    public interface Extender {
        Builder extend(Builder builder);
    }

    interface NotificationCompatImpl {
        Notification build(Builder builder);

        Action getAction(Notification notification, int i);

        int getActionCount(Notification notification);

        Action[] getActionsFromParcelableArrayList(ArrayList<Parcelable> arrayList);

        Bundle getBundleForUnreadConversation(NotificationCompatBase.UnreadConversation unreadConversation);

        String getCategory(Notification notification);

        Bundle getExtras(Notification notification);

        String getGroup(Notification notification);

        boolean getLocalOnly(Notification notification);

        ArrayList<Parcelable> getParcelableArrayListForActions(Action[] actionArr);

        String getSortKey(Notification notification);

        NotificationCompatBase.UnreadConversation getUnreadConversationFromBundle(Bundle bundle, NotificationCompatBase.UnreadConversation.Factory factory, RemoteInputCompatBase.RemoteInput.Factory factory2);

        boolean isGroupSummary(Notification notification);
    }

    static class NotificationCompatImplBase implements NotificationCompatImpl {
        NotificationCompatImplBase() {
        }

        public Notification build(Builder builder) {
            Builder builder2 = builder;
            Notification notification = builder2.mNotification;
            notification.setLatestEventInfo(builder2.mContext, builder2.mContentTitle, builder2.mContentText, builder2.mContentIntent);
            if (builder2.mPriority > 0) {
                notification.flags |= 128;
            }
            return notification;
        }

        public Bundle getExtras(Notification notification) {
            return null;
        }

        public int getActionCount(Notification notification) {
            return 0;
        }

        public Action getAction(Notification notification, int i) {
            return null;
        }

        public Action[] getActionsFromParcelableArrayList(ArrayList<Parcelable> arrayList) {
            return null;
        }

        public ArrayList<Parcelable> getParcelableArrayListForActions(Action[] actionArr) {
            return null;
        }

        public String getCategory(Notification notification) {
            return null;
        }

        public boolean getLocalOnly(Notification notification) {
            return false;
        }

        public String getGroup(Notification notification) {
            return null;
        }

        public boolean isGroupSummary(Notification notification) {
            return false;
        }

        public String getSortKey(Notification notification) {
            return null;
        }

        public Bundle getBundleForUnreadConversation(NotificationCompatBase.UnreadConversation unreadConversation) {
            return null;
        }

        public NotificationCompatBase.UnreadConversation getUnreadConversationFromBundle(Bundle bundle, NotificationCompatBase.UnreadConversation.Factory factory, RemoteInputCompatBase.RemoteInput.Factory factory2) {
            return null;
        }
    }

    static class NotificationCompatImplGingerbread extends NotificationCompatImplBase {
        NotificationCompatImplGingerbread() {
        }

        public Notification build(Builder builder) {
            Builder builder2 = builder;
            Notification notification = builder2.mNotification;
            notification.setLatestEventInfo(builder2.mContext, builder2.mContentTitle, builder2.mContentText, builder2.mContentIntent);
            Notification add = NotificationCompatGingerbread.add(notification, builder2.mContext, builder2.mContentTitle, builder2.mContentText, builder2.mContentIntent, builder2.mFullScreenIntent);
            if (builder2.mPriority > 0) {
                add.flags |= 128;
            }
            return add;
        }
    }

    static class NotificationCompatImplHoneycomb extends NotificationCompatImplBase {
        NotificationCompatImplHoneycomb() {
        }

        public Notification build(Builder builder) {
            Builder builder2 = builder;
            return NotificationCompatHoneycomb.add(builder2.mContext, builder2.mNotification, builder2.mContentTitle, builder2.mContentText, builder2.mContentInfo, builder2.mTickerView, builder2.mNumber, builder2.mContentIntent, builder2.mFullScreenIntent, builder2.mLargeIcon);
        }
    }

    static class NotificationCompatImplIceCreamSandwich extends NotificationCompatImplBase {
        NotificationCompatImplIceCreamSandwich() {
        }

        public Notification build(Builder builder) {
            Builder builder2 = builder;
            return NotificationCompatIceCreamSandwich.add(builder2.mContext, builder2.mNotification, builder2.mContentTitle, builder2.mContentText, builder2.mContentInfo, builder2.mTickerView, builder2.mNumber, builder2.mContentIntent, builder2.mFullScreenIntent, builder2.mLargeIcon, builder2.mProgressMax, builder2.mProgress, builder2.mProgressIndeterminate);
        }
    }

    static class NotificationCompatImplJellybean extends NotificationCompatImplBase {
        NotificationCompatImplJellybean() {
        }

        public Notification build(Builder builder) {
            NotificationCompatJellybean.Builder builder2;
            Builder builder3 = builder;
            new NotificationCompatJellybean.Builder(builder3.mContext, builder3.mNotification, builder3.mContentTitle, builder3.mContentText, builder3.mContentInfo, builder3.mTickerView, builder3.mNumber, builder3.mContentIntent, builder3.mFullScreenIntent, builder3.mLargeIcon, builder3.mProgressMax, builder3.mProgress, builder3.mProgressIndeterminate, builder3.mUseChronometer, builder3.mPriority, builder3.mSubText, builder3.mLocalOnly, builder3.mExtras, builder3.mGroupKey, builder3.mGroupSummary, builder3.mSortKey);
            NotificationCompatJellybean.Builder builder4 = builder2;
            NotificationCompat.addActionsToBuilder(builder4, builder3.mActions);
            NotificationCompat.addStyleToBuilderJellybean(builder4, builder3.mStyle);
            return builder4.build();
        }

        public Bundle getExtras(Notification notification) {
            return NotificationCompatJellybean.getExtras(notification);
        }

        public int getActionCount(Notification notification) {
            return NotificationCompatJellybean.getActionCount(notification);
        }

        public Action getAction(Notification notification, int i) {
            return (Action) NotificationCompatJellybean.getAction(notification, i, Action.FACTORY, RemoteInput.FACTORY);
        }

        public Action[] getActionsFromParcelableArrayList(ArrayList<Parcelable> arrayList) {
            return (Action[]) NotificationCompatJellybean.getActionsFromParcelableArrayList(arrayList, Action.FACTORY, RemoteInput.FACTORY);
        }

        public ArrayList<Parcelable> getParcelableArrayListForActions(Action[] actionArr) {
            return NotificationCompatJellybean.getParcelableArrayListForActions(actionArr);
        }

        public boolean getLocalOnly(Notification notification) {
            return NotificationCompatJellybean.getLocalOnly(notification);
        }

        public String getGroup(Notification notification) {
            return NotificationCompatJellybean.getGroup(notification);
        }

        public boolean isGroupSummary(Notification notification) {
            return NotificationCompatJellybean.isGroupSummary(notification);
        }

        public String getSortKey(Notification notification) {
            return NotificationCompatJellybean.getSortKey(notification);
        }
    }

    static class NotificationCompatImplKitKat extends NotificationCompatImplJellybean {
        NotificationCompatImplKitKat() {
        }

        public Notification build(Builder builder) {
            NotificationCompatKitKat.Builder builder2;
            Builder builder3 = builder;
            new NotificationCompatKitKat.Builder(builder3.mContext, builder3.mNotification, builder3.mContentTitle, builder3.mContentText, builder3.mContentInfo, builder3.mTickerView, builder3.mNumber, builder3.mContentIntent, builder3.mFullScreenIntent, builder3.mLargeIcon, builder3.mProgressMax, builder3.mProgress, builder3.mProgressIndeterminate, builder3.mShowWhen, builder3.mUseChronometer, builder3.mPriority, builder3.mSubText, builder3.mLocalOnly, builder3.mPeople, builder3.mExtras, builder3.mGroupKey, builder3.mGroupSummary, builder3.mSortKey);
            NotificationCompatKitKat.Builder builder4 = builder2;
            NotificationCompat.addActionsToBuilder(builder4, builder3.mActions);
            NotificationCompat.addStyleToBuilderJellybean(builder4, builder3.mStyle);
            return builder4.build();
        }

        public Bundle getExtras(Notification notification) {
            return NotificationCompatKitKat.getExtras(notification);
        }

        public int getActionCount(Notification notification) {
            return NotificationCompatKitKat.getActionCount(notification);
        }

        public Action getAction(Notification notification, int i) {
            return (Action) NotificationCompatKitKat.getAction(notification, i, Action.FACTORY, RemoteInput.FACTORY);
        }

        public boolean getLocalOnly(Notification notification) {
            return NotificationCompatKitKat.getLocalOnly(notification);
        }

        public String getGroup(Notification notification) {
            return NotificationCompatKitKat.getGroup(notification);
        }

        public boolean isGroupSummary(Notification notification) {
            return NotificationCompatKitKat.isGroupSummary(notification);
        }

        public String getSortKey(Notification notification) {
            return NotificationCompatKitKat.getSortKey(notification);
        }
    }

    static class NotificationCompatImplApi20 extends NotificationCompatImplKitKat {
        NotificationCompatImplApi20() {
        }

        public Notification build(Builder builder) {
            NotificationCompatApi20.Builder builder2;
            Builder builder3 = builder;
            new NotificationCompatApi20.Builder(builder3.mContext, builder3.mNotification, builder3.mContentTitle, builder3.mContentText, builder3.mContentInfo, builder3.mTickerView, builder3.mNumber, builder3.mContentIntent, builder3.mFullScreenIntent, builder3.mLargeIcon, builder3.mProgressMax, builder3.mProgress, builder3.mProgressIndeterminate, builder3.mShowWhen, builder3.mUseChronometer, builder3.mPriority, builder3.mSubText, builder3.mLocalOnly, builder3.mPeople, builder3.mExtras, builder3.mGroupKey, builder3.mGroupSummary, builder3.mSortKey);
            NotificationCompatApi20.Builder builder4 = builder2;
            NotificationCompat.addActionsToBuilder(builder4, builder3.mActions);
            NotificationCompat.addStyleToBuilderJellybean(builder4, builder3.mStyle);
            return builder4.build();
        }

        public Action getAction(Notification notification, int i) {
            return (Action) NotificationCompatApi20.getAction(notification, i, Action.FACTORY, RemoteInput.FACTORY);
        }

        public Action[] getActionsFromParcelableArrayList(ArrayList<Parcelable> arrayList) {
            return (Action[]) NotificationCompatApi20.getActionsFromParcelableArrayList(arrayList, Action.FACTORY, RemoteInput.FACTORY);
        }

        public ArrayList<Parcelable> getParcelableArrayListForActions(Action[] actionArr) {
            return NotificationCompatApi20.getParcelableArrayListForActions(actionArr);
        }

        public boolean getLocalOnly(Notification notification) {
            return NotificationCompatApi20.getLocalOnly(notification);
        }

        public String getGroup(Notification notification) {
            return NotificationCompatApi20.getGroup(notification);
        }

        public boolean isGroupSummary(Notification notification) {
            return NotificationCompatApi20.isGroupSummary(notification);
        }

        public String getSortKey(Notification notification) {
            return NotificationCompatApi20.getSortKey(notification);
        }
    }

    static class NotificationCompatImplApi21 extends NotificationCompatImplApi20 {
        NotificationCompatImplApi21() {
        }

        public Notification build(Builder builder) {
            NotificationCompatApi21.Builder builder2;
            Builder builder3 = builder;
            new NotificationCompatApi21.Builder(builder3.mContext, builder3.mNotification, builder3.mContentTitle, builder3.mContentText, builder3.mContentInfo, builder3.mTickerView, builder3.mNumber, builder3.mContentIntent, builder3.mFullScreenIntent, builder3.mLargeIcon, builder3.mProgressMax, builder3.mProgress, builder3.mProgressIndeterminate, builder3.mShowWhen, builder3.mUseChronometer, builder3.mPriority, builder3.mSubText, builder3.mLocalOnly, builder3.mCategory, builder3.mPeople, builder3.mExtras, builder3.mColor, builder3.mVisibility, builder3.mPublicVersion, builder3.mGroupKey, builder3.mGroupSummary, builder3.mSortKey);
            NotificationCompatApi21.Builder builder4 = builder2;
            NotificationCompat.addActionsToBuilder(builder4, builder3.mActions);
            NotificationCompat.addStyleToBuilderJellybean(builder4, builder3.mStyle);
            return builder4.build();
        }

        public String getCategory(Notification notification) {
            return NotificationCompatApi21.getCategory(notification);
        }

        public Bundle getBundleForUnreadConversation(NotificationCompatBase.UnreadConversation unreadConversation) {
            return NotificationCompatApi21.getBundleForUnreadConversation(unreadConversation);
        }

        public NotificationCompatBase.UnreadConversation getUnreadConversationFromBundle(Bundle bundle, NotificationCompatBase.UnreadConversation.Factory factory, RemoteInputCompatBase.RemoteInput.Factory factory2) {
            return NotificationCompatApi21.getUnreadConversationFromBundle(bundle, factory, factory2);
        }
    }

    /* access modifiers changed from: private */
    public static void addActionsToBuilder(NotificationBuilderWithActions notificationBuilderWithActions, ArrayList<Action> arrayList) {
        NotificationBuilderWithActions notificationBuilderWithActions2 = notificationBuilderWithActions;
        Iterator<Action> it = arrayList.iterator();
        while (it.hasNext()) {
            notificationBuilderWithActions2.addAction(it.next());
        }
    }

    /* access modifiers changed from: private */
    public static void addStyleToBuilderJellybean(NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor, Style style) {
        NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor2 = notificationBuilderWithBuilderAccessor;
        Style style2 = style;
        if (style2 == null) {
            return;
        }
        if (style2 instanceof BigTextStyle) {
            BigTextStyle bigTextStyle = (BigTextStyle) style2;
            NotificationCompatJellybean.addBigTextStyle(notificationBuilderWithBuilderAccessor2, bigTextStyle.mBigContentTitle, bigTextStyle.mSummaryTextSet, bigTextStyle.mSummaryText, bigTextStyle.mBigText);
        } else if (style2 instanceof InboxStyle) {
            InboxStyle inboxStyle = (InboxStyle) style2;
            NotificationCompatJellybean.addInboxStyle(notificationBuilderWithBuilderAccessor2, inboxStyle.mBigContentTitle, inboxStyle.mSummaryTextSet, inboxStyle.mSummaryText, inboxStyle.mTexts);
        } else if (style2 instanceof BigPictureStyle) {
            BigPictureStyle bigPictureStyle = (BigPictureStyle) style2;
            NotificationCompatJellybean.addBigPictureStyle(notificationBuilderWithBuilderAccessor2, bigPictureStyle.mBigContentTitle, bigPictureStyle.mSummaryTextSet, bigPictureStyle.mSummaryText, bigPictureStyle.mPicture, bigPictureStyle.mBigLargeIcon, bigPictureStyle.mBigLargeIconSet);
        }
    }

    static {
        NotificationCompatImpl notificationCompatImpl;
        NotificationCompatImpl notificationCompatImpl2;
        NotificationCompatImpl notificationCompatImpl3;
        NotificationCompatImpl notificationCompatImpl4;
        NotificationCompatImpl notificationCompatImpl5;
        NotificationCompatImpl notificationCompatImpl6;
        NotificationCompatImpl notificationCompatImpl7;
        NotificationCompatImpl notificationCompatImpl8;
        if (Build.VERSION.SDK_INT >= 21) {
            new NotificationCompatImplApi21();
            IMPL = notificationCompatImpl8;
        } else if (Build.VERSION.SDK_INT >= 20) {
            new NotificationCompatImplApi20();
            IMPL = notificationCompatImpl7;
        } else if (Build.VERSION.SDK_INT >= 19) {
            new NotificationCompatImplKitKat();
            IMPL = notificationCompatImpl6;
        } else if (Build.VERSION.SDK_INT >= 16) {
            new NotificationCompatImplJellybean();
            IMPL = notificationCompatImpl5;
        } else if (Build.VERSION.SDK_INT >= 14) {
            new NotificationCompatImplIceCreamSandwich();
            IMPL = notificationCompatImpl4;
        } else if (Build.VERSION.SDK_INT >= 11) {
            new NotificationCompatImplHoneycomb();
            IMPL = notificationCompatImpl3;
        } else if (Build.VERSION.SDK_INT >= 9) {
            new NotificationCompatImplGingerbread();
            IMPL = notificationCompatImpl2;
        } else {
            new NotificationCompatImplBase();
            IMPL = notificationCompatImpl;
        }
    }

    public static class Builder {
        private static final int MAX_CHARSEQUENCE_LENGTH = 5120;
        ArrayList<Action> mActions;
        String mCategory;
        int mColor;
        CharSequence mContentInfo;
        PendingIntent mContentIntent;
        CharSequence mContentText;
        CharSequence mContentTitle;
        Context mContext;
        Bundle mExtras;
        PendingIntent mFullScreenIntent;
        String mGroupKey;
        boolean mGroupSummary;
        Bitmap mLargeIcon;
        boolean mLocalOnly;
        Notification mNotification;
        int mNumber;
        public ArrayList<String> mPeople;
        int mPriority;
        int mProgress;
        boolean mProgressIndeterminate;
        int mProgressMax;
        Notification mPublicVersion;
        boolean mShowWhen = true;
        String mSortKey;
        Style mStyle;
        CharSequence mSubText;
        RemoteViews mTickerView;
        boolean mUseChronometer;
        int mVisibility;

        public Builder(Context context) {
            ArrayList<Action> arrayList;
            Notification notification;
            ArrayList<String> arrayList2;
            new ArrayList<>();
            this.mActions = arrayList;
            this.mLocalOnly = false;
            this.mColor = 0;
            this.mVisibility = 0;
            new Notification();
            this.mNotification = notification;
            this.mContext = context;
            this.mNotification.when = System.currentTimeMillis();
            this.mNotification.audioStreamType = -1;
            this.mPriority = 0;
            new ArrayList<>();
            this.mPeople = arrayList2;
        }

        public Builder setWhen(long j) {
            this.mNotification.when = j;
            return this;
        }

        public Builder setShowWhen(boolean z) {
            this.mShowWhen = z;
            return this;
        }

        public Builder setUsesChronometer(boolean z) {
            this.mUseChronometer = z;
            return this;
        }

        public Builder setSmallIcon(int i) {
            this.mNotification.icon = i;
            return this;
        }

        public Builder setSmallIcon(int i, int i2) {
            this.mNotification.icon = i;
            this.mNotification.iconLevel = i2;
            return this;
        }

        public Builder setContentTitle(CharSequence charSequence) {
            this.mContentTitle = limitCharSequenceLength(charSequence);
            return this;
        }

        public Builder setContentText(CharSequence charSequence) {
            this.mContentText = limitCharSequenceLength(charSequence);
            return this;
        }

        public Builder setSubText(CharSequence charSequence) {
            this.mSubText = limitCharSequenceLength(charSequence);
            return this;
        }

        public Builder setNumber(int i) {
            this.mNumber = i;
            return this;
        }

        public Builder setContentInfo(CharSequence charSequence) {
            this.mContentInfo = limitCharSequenceLength(charSequence);
            return this;
        }

        public Builder setProgress(int i, int i2, boolean z) {
            this.mProgressMax = i;
            this.mProgress = i2;
            this.mProgressIndeterminate = z;
            return this;
        }

        public Builder setContent(RemoteViews remoteViews) {
            this.mNotification.contentView = remoteViews;
            return this;
        }

        public Builder setContentIntent(PendingIntent pendingIntent) {
            this.mContentIntent = pendingIntent;
            return this;
        }

        public Builder setDeleteIntent(PendingIntent pendingIntent) {
            this.mNotification.deleteIntent = pendingIntent;
            return this;
        }

        public Builder setFullScreenIntent(PendingIntent pendingIntent, boolean z) {
            this.mFullScreenIntent = pendingIntent;
            setFlag(128, z);
            return this;
        }

        public Builder setTicker(CharSequence charSequence) {
            this.mNotification.tickerText = limitCharSequenceLength(charSequence);
            return this;
        }

        public Builder setTicker(CharSequence charSequence, RemoteViews remoteViews) {
            this.mNotification.tickerText = limitCharSequenceLength(charSequence);
            this.mTickerView = remoteViews;
            return this;
        }

        public Builder setLargeIcon(Bitmap bitmap) {
            this.mLargeIcon = bitmap;
            return this;
        }

        public Builder setSound(Uri uri) {
            this.mNotification.sound = uri;
            this.mNotification.audioStreamType = -1;
            return this;
        }

        public Builder setSound(Uri uri, int i) {
            this.mNotification.sound = uri;
            this.mNotification.audioStreamType = i;
            return this;
        }

        public Builder setVibrate(long[] jArr) {
            this.mNotification.vibrate = jArr;
            return this;
        }

        public Builder setLights(int i, int i2, int i3) {
            this.mNotification.ledARGB = i;
            this.mNotification.ledOnMS = i2;
            this.mNotification.ledOffMS = i3;
            this.mNotification.flags = (this.mNotification.flags & -2) | (this.mNotification.ledOnMS != 0 && this.mNotification.ledOffMS != 0 ? 1 : 0);
            return this;
        }

        public Builder setOngoing(boolean z) {
            setFlag(2, z);
            return this;
        }

        public Builder setOnlyAlertOnce(boolean z) {
            setFlag(8, z);
            return this;
        }

        public Builder setAutoCancel(boolean z) {
            setFlag(16, z);
            return this;
        }

        public Builder setLocalOnly(boolean z) {
            this.mLocalOnly = z;
            return this;
        }

        public Builder setCategory(String str) {
            this.mCategory = str;
            return this;
        }

        public Builder setDefaults(int i) {
            int i2 = i;
            this.mNotification.defaults = i2;
            if ((i2 & 4) != 0) {
                this.mNotification.flags |= 1;
            }
            return this;
        }

        private void setFlag(int i, boolean z) {
            int i2 = i;
            if (z) {
                this.mNotification.flags |= i2;
                return;
            }
            this.mNotification.flags &= i2 ^ -1;
        }

        public Builder setPriority(int i) {
            this.mPriority = i;
            return this;
        }

        public Builder addPerson(String str) {
            boolean add = this.mPeople.add(str);
            return this;
        }

        public Builder setGroup(String str) {
            this.mGroupKey = str;
            return this;
        }

        public Builder setGroupSummary(boolean z) {
            this.mGroupSummary = z;
            return this;
        }

        public Builder setSortKey(String str) {
            this.mSortKey = str;
            return this;
        }

        public Builder addExtras(Bundle bundle) {
            Bundle bundle2;
            Bundle bundle3 = bundle;
            if (bundle3 != null) {
                if (this.mExtras == null) {
                    new Bundle(bundle3);
                    this.mExtras = bundle2;
                } else {
                    this.mExtras.putAll(bundle3);
                }
            }
            return this;
        }

        public Builder setExtras(Bundle bundle) {
            this.mExtras = bundle;
            return this;
        }

        public Bundle getExtras() {
            Bundle bundle;
            if (this.mExtras == null) {
                new Bundle();
                this.mExtras = bundle;
            }
            return this.mExtras;
        }

        public Builder addAction(int i, CharSequence charSequence, PendingIntent pendingIntent) {
            Object obj;
            new Action(i, charSequence, pendingIntent);
            boolean add = this.mActions.add(obj);
            return this;
        }

        public Builder addAction(Action action) {
            boolean add = this.mActions.add(action);
            return this;
        }

        public Builder setStyle(Style style) {
            Style style2 = style;
            if (this.mStyle != style2) {
                this.mStyle = style2;
                if (this.mStyle != null) {
                    this.mStyle.setBuilder(this);
                }
            }
            return this;
        }

        public Builder setColor(int i) {
            this.mColor = i;
            return this;
        }

        public Builder setVisibility(int i) {
            this.mVisibility = i;
            return this;
        }

        public Builder setPublicVersion(Notification notification) {
            this.mPublicVersion = notification;
            return this;
        }

        public Builder extend(Extender extender) {
            Builder extend = extender.extend(this);
            return this;
        }

        @Deprecated
        public Notification getNotification() {
            return NotificationCompat.IMPL.build(this);
        }

        public Notification build() {
            return NotificationCompat.IMPL.build(this);
        }

        protected static CharSequence limitCharSequenceLength(CharSequence charSequence) {
            CharSequence charSequence2 = charSequence;
            if (charSequence2 == null) {
                return charSequence2;
            }
            if (charSequence2.length() > MAX_CHARSEQUENCE_LENGTH) {
                charSequence2 = charSequence2.subSequence(0, MAX_CHARSEQUENCE_LENGTH);
            }
            return charSequence2;
        }
    }

    public static abstract class Style {
        CharSequence mBigContentTitle;
        Builder mBuilder;
        CharSequence mSummaryText;
        boolean mSummaryTextSet = false;

        public void setBuilder(Builder builder) {
            Builder builder2 = builder;
            if (this.mBuilder != builder2) {
                this.mBuilder = builder2;
                if (this.mBuilder != null) {
                    Builder style = this.mBuilder.setStyle(this);
                }
            }
        }

        public Notification build() {
            Notification notification = null;
            if (this.mBuilder != null) {
                notification = this.mBuilder.build();
            }
            return notification;
        }
    }

    public static class BigPictureStyle extends Style {
        Bitmap mBigLargeIcon;
        boolean mBigLargeIconSet;
        Bitmap mPicture;

        public BigPictureStyle() {
        }

        public BigPictureStyle(Builder builder) {
            setBuilder(builder);
        }

        public BigPictureStyle setBigContentTitle(CharSequence charSequence) {
            this.mBigContentTitle = Builder.limitCharSequenceLength(charSequence);
            return this;
        }

        public BigPictureStyle setSummaryText(CharSequence charSequence) {
            this.mSummaryText = Builder.limitCharSequenceLength(charSequence);
            this.mSummaryTextSet = true;
            return this;
        }

        public BigPictureStyle bigPicture(Bitmap bitmap) {
            this.mPicture = bitmap;
            return this;
        }

        public BigPictureStyle bigLargeIcon(Bitmap bitmap) {
            this.mBigLargeIcon = bitmap;
            this.mBigLargeIconSet = true;
            return this;
        }
    }

    public static class BigTextStyle extends Style {
        CharSequence mBigText;

        public BigTextStyle() {
        }

        public BigTextStyle(Builder builder) {
            setBuilder(builder);
        }

        public BigTextStyle setBigContentTitle(CharSequence charSequence) {
            this.mBigContentTitle = Builder.limitCharSequenceLength(charSequence);
            return this;
        }

        public BigTextStyle setSummaryText(CharSequence charSequence) {
            this.mSummaryText = Builder.limitCharSequenceLength(charSequence);
            this.mSummaryTextSet = true;
            return this;
        }

        public BigTextStyle bigText(CharSequence charSequence) {
            this.mBigText = Builder.limitCharSequenceLength(charSequence);
            return this;
        }
    }

    public static class InboxStyle extends Style {
        ArrayList<CharSequence> mTexts;

        public InboxStyle() {
            ArrayList<CharSequence> arrayList;
            new ArrayList<>();
            this.mTexts = arrayList;
        }

        public InboxStyle(Builder builder) {
            ArrayList<CharSequence> arrayList;
            new ArrayList<>();
            this.mTexts = arrayList;
            setBuilder(builder);
        }

        public InboxStyle setBigContentTitle(CharSequence charSequence) {
            this.mBigContentTitle = Builder.limitCharSequenceLength(charSequence);
            return this;
        }

        public InboxStyle setSummaryText(CharSequence charSequence) {
            this.mSummaryText = Builder.limitCharSequenceLength(charSequence);
            this.mSummaryTextSet = true;
            return this;
        }

        public InboxStyle addLine(CharSequence charSequence) {
            boolean add = this.mTexts.add(Builder.limitCharSequenceLength(charSequence));
            return this;
        }
    }

    public static class Action extends NotificationCompatBase.Action {
        public static final NotificationCompatBase.Action.Factory FACTORY;
        public PendingIntent actionIntent;
        public int icon;
        /* access modifiers changed from: private */
        public final Bundle mExtras;
        private final RemoteInput[] mRemoteInputs;
        public CharSequence title;

        public interface Extender {
            Builder extend(Builder builder);
        }

        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public Action(int r12, java.lang.CharSequence r13, android.app.PendingIntent r14) {
            /*
                r11 = this;
                r0 = r11
                r1 = r12
                r2 = r13
                r3 = r14
                r4 = r0
                r5 = r1
                r6 = r2
                r7 = r3
                android.os.Bundle r8 = new android.os.Bundle
                r10 = r8
                r8 = r10
                r9 = r10
                r9.<init>()
                r9 = 0
                r4.<init>(r5, r6, r7, r8, r9)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: android.support.v4.app.NotificationCompat.Action.<init>(int, java.lang.CharSequence, android.app.PendingIntent):void");
        }

        private Action(int i, CharSequence charSequence, PendingIntent pendingIntent, Bundle bundle, RemoteInput[] remoteInputArr) {
            Bundle bundle2;
            Bundle bundle3;
            Bundle bundle4 = bundle;
            RemoteInput[] remoteInputArr2 = remoteInputArr;
            this.icon = i;
            this.title = Builder.limitCharSequenceLength(charSequence);
            this.actionIntent = pendingIntent;
            if (bundle4 != null) {
                bundle3 = bundle4;
            } else {
                bundle3 = bundle2;
                new Bundle();
            }
            this.mExtras = bundle3;
            this.mRemoteInputs = remoteInputArr2;
        }

        /* access modifiers changed from: protected */
        public int getIcon() {
            return this.icon;
        }

        /* access modifiers changed from: protected */
        public CharSequence getTitle() {
            return this.title;
        }

        /* access modifiers changed from: protected */
        public PendingIntent getActionIntent() {
            return this.actionIntent;
        }

        public Bundle getExtras() {
            return this.mExtras;
        }

        public RemoteInput[] getRemoteInputs() {
            return this.mRemoteInputs;
        }

        public static final class Builder {
            private final Bundle mExtras;
            private final int mIcon;
            private final PendingIntent mIntent;
            private ArrayList<RemoteInput> mRemoteInputs;
            private final CharSequence mTitle;

            /* JADX WARNING: Illegal instructions before constructor call */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public Builder(int r12, java.lang.CharSequence r13, android.app.PendingIntent r14) {
                /*
                    r11 = this;
                    r0 = r11
                    r1 = r12
                    r2 = r13
                    r3 = r14
                    r4 = r0
                    r5 = r1
                    r6 = r2
                    r7 = r3
                    android.os.Bundle r8 = new android.os.Bundle
                    r10 = r8
                    r8 = r10
                    r9 = r10
                    r9.<init>()
                    r4.<init>(r5, r6, r7, r8)
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: android.support.v4.app.NotificationCompat.Action.Builder.<init>(int, java.lang.CharSequence, android.app.PendingIntent):void");
            }

            /* JADX WARNING: Illegal instructions before constructor call */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public Builder(android.support.v4.app.NotificationCompat.Action r11) {
                /*
                    r10 = this;
                    r0 = r10
                    r1 = r11
                    r2 = r0
                    r3 = r1
                    int r3 = r3.icon
                    r4 = r1
                    java.lang.CharSequence r4 = r4.title
                    r5 = r1
                    android.app.PendingIntent r5 = r5.actionIntent
                    android.os.Bundle r6 = new android.os.Bundle
                    r9 = r6
                    r6 = r9
                    r7 = r9
                    r8 = r1
                    android.os.Bundle r8 = r8.mExtras
                    r7.<init>(r8)
                    r2.<init>(r3, r4, r5, r6)
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: android.support.v4.app.NotificationCompat.Action.Builder.<init>(android.support.v4.app.NotificationCompat$Action):void");
            }

            private Builder(int i, CharSequence charSequence, PendingIntent pendingIntent, Bundle bundle) {
                this.mIcon = i;
                this.mTitle = Builder.limitCharSequenceLength(charSequence);
                this.mIntent = pendingIntent;
                this.mExtras = bundle;
            }

            public Builder addExtras(Bundle bundle) {
                Bundle bundle2 = bundle;
                if (bundle2 != null) {
                    this.mExtras.putAll(bundle2);
                }
                return this;
            }

            public Bundle getExtras() {
                return this.mExtras;
            }

            public Builder addRemoteInput(RemoteInput remoteInput) {
                ArrayList<RemoteInput> arrayList;
                RemoteInput remoteInput2 = remoteInput;
                if (this.mRemoteInputs == null) {
                    new ArrayList<>();
                    this.mRemoteInputs = arrayList;
                }
                boolean add = this.mRemoteInputs.add(remoteInput2);
                return this;
            }

            public Builder extend(Extender extender) {
                Builder extend = extender.extend(this);
                return this;
            }

            public Action build() {
                Action action;
                new Action(this.mIcon, this.mTitle, this.mIntent, this.mExtras, this.mRemoteInputs != null ? (RemoteInput[]) this.mRemoteInputs.toArray(new RemoteInput[this.mRemoteInputs.size()]) : null);
                return action;
            }
        }

        public static final class WearableExtender implements Extender {
            private static final int DEFAULT_FLAGS = 1;
            private static final String EXTRA_WEARABLE_EXTENSIONS = "android.wearable.EXTENSIONS";
            private static final int FLAG_AVAILABLE_OFFLINE = 1;
            private static final String KEY_CANCEL_LABEL = "cancelLabel";
            private static final String KEY_CONFIRM_LABEL = "confirmLabel";
            private static final String KEY_FLAGS = "flags";
            private static final String KEY_IN_PROGRESS_LABEL = "inProgressLabel";
            private CharSequence mCancelLabel;
            private CharSequence mConfirmLabel;
            private int mFlags = 1;
            private CharSequence mInProgressLabel;

            public WearableExtender() {
            }

            public WearableExtender(Action action) {
                Bundle bundle = action.getExtras().getBundle(EXTRA_WEARABLE_EXTENSIONS);
                if (bundle != null) {
                    this.mFlags = bundle.getInt(KEY_FLAGS, 1);
                    this.mInProgressLabel = bundle.getCharSequence(KEY_IN_PROGRESS_LABEL);
                    this.mConfirmLabel = bundle.getCharSequence(KEY_CONFIRM_LABEL);
                    this.mCancelLabel = bundle.getCharSequence(KEY_CANCEL_LABEL);
                }
            }

            public Builder extend(Builder builder) {
                Bundle bundle;
                Builder builder2 = builder;
                new Bundle();
                Bundle bundle2 = bundle;
                if (this.mFlags != 1) {
                    bundle2.putInt(KEY_FLAGS, this.mFlags);
                }
                if (this.mInProgressLabel != null) {
                    bundle2.putCharSequence(KEY_IN_PROGRESS_LABEL, this.mInProgressLabel);
                }
                if (this.mConfirmLabel != null) {
                    bundle2.putCharSequence(KEY_CONFIRM_LABEL, this.mConfirmLabel);
                }
                if (this.mCancelLabel != null) {
                    bundle2.putCharSequence(KEY_CANCEL_LABEL, this.mCancelLabel);
                }
                builder2.getExtras().putBundle(EXTRA_WEARABLE_EXTENSIONS, bundle2);
                return builder2;
            }

            public WearableExtender clone() {
                WearableExtender wearableExtender;
                new WearableExtender();
                WearableExtender wearableExtender2 = wearableExtender;
                wearableExtender2.mFlags = this.mFlags;
                wearableExtender2.mInProgressLabel = this.mInProgressLabel;
                wearableExtender2.mConfirmLabel = this.mConfirmLabel;
                wearableExtender2.mCancelLabel = this.mCancelLabel;
                return wearableExtender2;
            }

            public WearableExtender setAvailableOffline(boolean z) {
                setFlag(1, z);
                return this;
            }

            public boolean isAvailableOffline() {
                return (this.mFlags & 1) != 0;
            }

            private void setFlag(int i, boolean z) {
                int i2 = i;
                if (z) {
                    this.mFlags = this.mFlags | i2;
                } else {
                    this.mFlags = this.mFlags & (i2 ^ -1);
                }
            }

            public WearableExtender setInProgressLabel(CharSequence charSequence) {
                this.mInProgressLabel = charSequence;
                return this;
            }

            public CharSequence getInProgressLabel() {
                return this.mInProgressLabel;
            }

            public WearableExtender setConfirmLabel(CharSequence charSequence) {
                this.mConfirmLabel = charSequence;
                return this;
            }

            public CharSequence getConfirmLabel() {
                return this.mConfirmLabel;
            }

            public WearableExtender setCancelLabel(CharSequence charSequence) {
                this.mCancelLabel = charSequence;
                return this;
            }

            public CharSequence getCancelLabel() {
                return this.mCancelLabel;
            }
        }

        static {
            NotificationCompatBase.Action.Factory factory;
            new NotificationCompatBase.Action.Factory() {
                public Action build(int i, CharSequence charSequence, PendingIntent pendingIntent, Bundle bundle, RemoteInputCompatBase.RemoteInput[] remoteInputArr) {
                    Action action;
                    new Action(i, charSequence, pendingIntent, bundle, (RemoteInput[]) remoteInputArr);
                    return action;
                }

                public Action[] newArray(int i) {
                    return new Action[i];
                }
            };
            FACTORY = factory;
        }
    }

    public static final class WearableExtender implements Extender {
        private static final int DEFAULT_CONTENT_ICON_GRAVITY = 8388613;
        private static final int DEFAULT_FLAGS = 1;
        private static final int DEFAULT_GRAVITY = 80;
        private static final String EXTRA_WEARABLE_EXTENSIONS = "android.wearable.EXTENSIONS";
        private static final int FLAG_CONTENT_INTENT_AVAILABLE_OFFLINE = 1;
        private static final int FLAG_HINT_AVOID_BACKGROUND_CLIPPING = 16;
        private static final int FLAG_HINT_HIDE_ICON = 2;
        private static final int FLAG_HINT_SHOW_BACKGROUND_ONLY = 4;
        private static final int FLAG_START_SCROLL_BOTTOM = 8;
        private static final String KEY_ACTIONS = "actions";
        private static final String KEY_BACKGROUND = "background";
        private static final String KEY_CONTENT_ACTION_INDEX = "contentActionIndex";
        private static final String KEY_CONTENT_ICON = "contentIcon";
        private static final String KEY_CONTENT_ICON_GRAVITY = "contentIconGravity";
        private static final String KEY_CUSTOM_CONTENT_HEIGHT = "customContentHeight";
        private static final String KEY_CUSTOM_SIZE_PRESET = "customSizePreset";
        private static final String KEY_DISPLAY_INTENT = "displayIntent";
        private static final String KEY_FLAGS = "flags";
        private static final String KEY_GRAVITY = "gravity";
        private static final String KEY_HINT_SCREEN_TIMEOUT = "hintScreenTimeout";
        private static final String KEY_PAGES = "pages";
        public static final int SCREEN_TIMEOUT_LONG = -1;
        public static final int SCREEN_TIMEOUT_SHORT = 0;
        public static final int SIZE_DEFAULT = 0;
        public static final int SIZE_FULL_SCREEN = 5;
        public static final int SIZE_LARGE = 4;
        public static final int SIZE_MEDIUM = 3;
        public static final int SIZE_SMALL = 2;
        public static final int SIZE_XSMALL = 1;
        public static final int UNSET_ACTION_INDEX = -1;
        private ArrayList<Action> mActions;
        private Bitmap mBackground;
        private int mContentActionIndex;
        private int mContentIcon;
        private int mContentIconGravity;
        private int mCustomContentHeight;
        private int mCustomSizePreset;
        private PendingIntent mDisplayIntent;
        private int mFlags = 1;
        private int mGravity;
        private int mHintScreenTimeout;
        private ArrayList<Notification> mPages;

        public WearableExtender() {
            ArrayList<Action> arrayList;
            ArrayList<Notification> arrayList2;
            new ArrayList<>();
            this.mActions = arrayList;
            new ArrayList<>();
            this.mPages = arrayList2;
            this.mContentIconGravity = 8388613;
            this.mContentActionIndex = -1;
            this.mCustomSizePreset = 0;
            this.mGravity = DEFAULT_GRAVITY;
        }

        public WearableExtender(Notification notification) {
            ArrayList<Action> arrayList;
            ArrayList<Notification> arrayList2;
            new ArrayList<>();
            this.mActions = arrayList;
            new ArrayList<>();
            this.mPages = arrayList2;
            this.mContentIconGravity = 8388613;
            this.mContentActionIndex = -1;
            this.mCustomSizePreset = 0;
            this.mGravity = DEFAULT_GRAVITY;
            Bundle extras = NotificationCompat.getExtras(notification);
            Bundle bundle = extras != null ? extras.getBundle(EXTRA_WEARABLE_EXTENSIONS) : null;
            if (bundle != null) {
                Action[] actionsFromParcelableArrayList = NotificationCompat.IMPL.getActionsFromParcelableArrayList(bundle.getParcelableArrayList(KEY_ACTIONS));
                if (actionsFromParcelableArrayList != null) {
                    boolean addAll = Collections.addAll(this.mActions, actionsFromParcelableArrayList);
                }
                this.mFlags = bundle.getInt(KEY_FLAGS, 1);
                this.mDisplayIntent = (PendingIntent) bundle.getParcelable(KEY_DISPLAY_INTENT);
                Notification[] access$500 = NotificationCompat.getNotificationArrayFromBundle(bundle, KEY_PAGES);
                if (access$500 != null) {
                    boolean addAll2 = Collections.addAll(this.mPages, access$500);
                }
                this.mBackground = (Bitmap) bundle.getParcelable(KEY_BACKGROUND);
                this.mContentIcon = bundle.getInt(KEY_CONTENT_ICON);
                this.mContentIconGravity = bundle.getInt(KEY_CONTENT_ICON_GRAVITY, 8388613);
                this.mContentActionIndex = bundle.getInt(KEY_CONTENT_ACTION_INDEX, -1);
                this.mCustomSizePreset = bundle.getInt(KEY_CUSTOM_SIZE_PRESET, 0);
                this.mCustomContentHeight = bundle.getInt(KEY_CUSTOM_CONTENT_HEIGHT);
                this.mGravity = bundle.getInt(KEY_GRAVITY, DEFAULT_GRAVITY);
                this.mHintScreenTimeout = bundle.getInt(KEY_HINT_SCREEN_TIMEOUT);
            }
        }

        public Builder extend(Builder builder) {
            Bundle bundle;
            Builder builder2 = builder;
            new Bundle();
            Bundle bundle2 = bundle;
            if (!this.mActions.isEmpty()) {
                bundle2.putParcelableArrayList(KEY_ACTIONS, NotificationCompat.IMPL.getParcelableArrayListForActions((Action[]) this.mActions.toArray(new Action[this.mActions.size()])));
            }
            if (this.mFlags != 1) {
                bundle2.putInt(KEY_FLAGS, this.mFlags);
            }
            if (this.mDisplayIntent != null) {
                bundle2.putParcelable(KEY_DISPLAY_INTENT, this.mDisplayIntent);
            }
            if (!this.mPages.isEmpty()) {
                bundle2.putParcelableArray(KEY_PAGES, (Parcelable[]) this.mPages.toArray(new Notification[this.mPages.size()]));
            }
            if (this.mBackground != null) {
                bundle2.putParcelable(KEY_BACKGROUND, this.mBackground);
            }
            if (this.mContentIcon != 0) {
                bundle2.putInt(KEY_CONTENT_ICON, this.mContentIcon);
            }
            if (this.mContentIconGravity != 8388613) {
                bundle2.putInt(KEY_CONTENT_ICON_GRAVITY, this.mContentIconGravity);
            }
            if (this.mContentActionIndex != -1) {
                bundle2.putInt(KEY_CONTENT_ACTION_INDEX, this.mContentActionIndex);
            }
            if (this.mCustomSizePreset != 0) {
                bundle2.putInt(KEY_CUSTOM_SIZE_PRESET, this.mCustomSizePreset);
            }
            if (this.mCustomContentHeight != 0) {
                bundle2.putInt(KEY_CUSTOM_CONTENT_HEIGHT, this.mCustomContentHeight);
            }
            if (this.mGravity != DEFAULT_GRAVITY) {
                bundle2.putInt(KEY_GRAVITY, this.mGravity);
            }
            if (this.mHintScreenTimeout != 0) {
                bundle2.putInt(KEY_HINT_SCREEN_TIMEOUT, this.mHintScreenTimeout);
            }
            builder2.getExtras().putBundle(EXTRA_WEARABLE_EXTENSIONS, bundle2);
            return builder2;
        }

        public WearableExtender clone() {
            WearableExtender wearableExtender;
            ArrayList<Action> arrayList;
            ArrayList<Notification> arrayList2;
            new WearableExtender();
            WearableExtender wearableExtender2 = wearableExtender;
            new ArrayList<>(this.mActions);
            wearableExtender2.mActions = arrayList;
            wearableExtender2.mFlags = this.mFlags;
            wearableExtender2.mDisplayIntent = this.mDisplayIntent;
            new ArrayList<>(this.mPages);
            wearableExtender2.mPages = arrayList2;
            wearableExtender2.mBackground = this.mBackground;
            wearableExtender2.mContentIcon = this.mContentIcon;
            wearableExtender2.mContentIconGravity = this.mContentIconGravity;
            wearableExtender2.mContentActionIndex = this.mContentActionIndex;
            wearableExtender2.mCustomSizePreset = this.mCustomSizePreset;
            wearableExtender2.mCustomContentHeight = this.mCustomContentHeight;
            wearableExtender2.mGravity = this.mGravity;
            wearableExtender2.mHintScreenTimeout = this.mHintScreenTimeout;
            return wearableExtender2;
        }

        public WearableExtender addAction(Action action) {
            boolean add = this.mActions.add(action);
            return this;
        }

        public WearableExtender addActions(List<Action> list) {
            boolean addAll = this.mActions.addAll(list);
            return this;
        }

        public WearableExtender clearActions() {
            this.mActions.clear();
            return this;
        }

        public List<Action> getActions() {
            return this.mActions;
        }

        public WearableExtender setDisplayIntent(PendingIntent pendingIntent) {
            this.mDisplayIntent = pendingIntent;
            return this;
        }

        public PendingIntent getDisplayIntent() {
            return this.mDisplayIntent;
        }

        public WearableExtender addPage(Notification notification) {
            boolean add = this.mPages.add(notification);
            return this;
        }

        public WearableExtender addPages(List<Notification> list) {
            boolean addAll = this.mPages.addAll(list);
            return this;
        }

        public WearableExtender clearPages() {
            this.mPages.clear();
            return this;
        }

        public List<Notification> getPages() {
            return this.mPages;
        }

        public WearableExtender setBackground(Bitmap bitmap) {
            this.mBackground = bitmap;
            return this;
        }

        public Bitmap getBackground() {
            return this.mBackground;
        }

        public WearableExtender setContentIcon(int i) {
            this.mContentIcon = i;
            return this;
        }

        public int getContentIcon() {
            return this.mContentIcon;
        }

        public WearableExtender setContentIconGravity(int i) {
            this.mContentIconGravity = i;
            return this;
        }

        public int getContentIconGravity() {
            return this.mContentIconGravity;
        }

        public WearableExtender setContentAction(int i) {
            this.mContentActionIndex = i;
            return this;
        }

        public int getContentAction() {
            return this.mContentActionIndex;
        }

        public WearableExtender setGravity(int i) {
            this.mGravity = i;
            return this;
        }

        public int getGravity() {
            return this.mGravity;
        }

        public WearableExtender setCustomSizePreset(int i) {
            this.mCustomSizePreset = i;
            return this;
        }

        public int getCustomSizePreset() {
            return this.mCustomSizePreset;
        }

        public WearableExtender setCustomContentHeight(int i) {
            this.mCustomContentHeight = i;
            return this;
        }

        public int getCustomContentHeight() {
            return this.mCustomContentHeight;
        }

        public WearableExtender setStartScrollBottom(boolean z) {
            setFlag(8, z);
            return this;
        }

        public boolean getStartScrollBottom() {
            return (this.mFlags & 8) != 0;
        }

        public WearableExtender setContentIntentAvailableOffline(boolean z) {
            setFlag(1, z);
            return this;
        }

        public boolean getContentIntentAvailableOffline() {
            return (this.mFlags & 1) != 0;
        }

        public WearableExtender setHintHideIcon(boolean z) {
            setFlag(2, z);
            return this;
        }

        public boolean getHintHideIcon() {
            return (this.mFlags & 2) != 0;
        }

        public WearableExtender setHintShowBackgroundOnly(boolean z) {
            setFlag(4, z);
            return this;
        }

        public boolean getHintShowBackgroundOnly() {
            return (this.mFlags & 4) != 0;
        }

        public WearableExtender setHintAvoidBackgroundClipping(boolean z) {
            setFlag(16, z);
            return this;
        }

        public boolean getHintAvoidBackgroundClipping() {
            return (this.mFlags & 16) != 0;
        }

        public WearableExtender setHintScreenTimeout(int i) {
            this.mHintScreenTimeout = i;
            return this;
        }

        public int getHintScreenTimeout() {
            return this.mHintScreenTimeout;
        }

        private void setFlag(int i, boolean z) {
            int i2 = i;
            if (z) {
                this.mFlags = this.mFlags | i2;
            } else {
                this.mFlags = this.mFlags & (i2 ^ -1);
            }
        }
    }

    public static final class CarExtender implements Extender {
        private static final String EXTRA_CAR_EXTENDER = "android.car.EXTENSIONS";
        private static final String EXTRA_COLOR = "app_color";
        private static final String EXTRA_CONVERSATION = "car_conversation";
        private static final String EXTRA_LARGE_ICON = "large_icon";
        private static final String TAG = "CarExtender";
        private int mColor = 0;
        private Bitmap mLargeIcon;
        private UnreadConversation mUnreadConversation;

        public CarExtender() {
        }

        public CarExtender(Notification notification) {
            Notification notification2 = notification;
            if (Build.VERSION.SDK_INT >= 21) {
                Bundle bundle = NotificationCompat.getExtras(notification2) == null ? null : NotificationCompat.getExtras(notification2).getBundle(EXTRA_CAR_EXTENDER);
                if (bundle != null) {
                    this.mLargeIcon = (Bitmap) bundle.getParcelable(EXTRA_LARGE_ICON);
                    this.mColor = bundle.getInt(EXTRA_COLOR, 0);
                    this.mUnreadConversation = (UnreadConversation) NotificationCompat.IMPL.getUnreadConversationFromBundle(bundle.getBundle(EXTRA_CONVERSATION), UnreadConversation.FACTORY, RemoteInput.FACTORY);
                }
            }
        }

        public Builder extend(Builder builder) {
            Bundle bundle;
            Builder builder2 = builder;
            if (Build.VERSION.SDK_INT < 21) {
                return builder2;
            }
            new Bundle();
            Bundle bundle2 = bundle;
            if (this.mLargeIcon != null) {
                bundle2.putParcelable(EXTRA_LARGE_ICON, this.mLargeIcon);
            }
            if (this.mColor != 0) {
                bundle2.putInt(EXTRA_COLOR, this.mColor);
            }
            if (this.mUnreadConversation != null) {
                bundle2.putBundle(EXTRA_CONVERSATION, NotificationCompat.IMPL.getBundleForUnreadConversation(this.mUnreadConversation));
            }
            builder2.getExtras().putBundle(EXTRA_CAR_EXTENDER, bundle2);
            return builder2;
        }

        public CarExtender setColor(int i) {
            this.mColor = i;
            return this;
        }

        public int getColor() {
            return this.mColor;
        }

        public CarExtender setLargeIcon(Bitmap bitmap) {
            this.mLargeIcon = bitmap;
            return this;
        }

        public Bitmap getLargeIcon() {
            return this.mLargeIcon;
        }

        public CarExtender setUnreadConversation(UnreadConversation unreadConversation) {
            this.mUnreadConversation = unreadConversation;
            return this;
        }

        public UnreadConversation getUnreadConversation() {
            return this.mUnreadConversation;
        }

        public static class UnreadConversation extends NotificationCompatBase.UnreadConversation {
            static final NotificationCompatBase.UnreadConversation.Factory FACTORY;
            private final long mLatestTimestamp;
            private final String[] mMessages;
            private final String[] mParticipants;
            private final PendingIntent mReadPendingIntent;
            private final RemoteInput mRemoteInput;
            private final PendingIntent mReplyPendingIntent;

            UnreadConversation(String[] strArr, RemoteInput remoteInput, PendingIntent pendingIntent, PendingIntent pendingIntent2, String[] strArr2, long j) {
                this.mMessages = strArr;
                this.mRemoteInput = remoteInput;
                this.mReadPendingIntent = pendingIntent2;
                this.mReplyPendingIntent = pendingIntent;
                this.mParticipants = strArr2;
                this.mLatestTimestamp = j;
            }

            public String[] getMessages() {
                return this.mMessages;
            }

            public RemoteInput getRemoteInput() {
                return this.mRemoteInput;
            }

            public PendingIntent getReplyPendingIntent() {
                return this.mReplyPendingIntent;
            }

            public PendingIntent getReadPendingIntent() {
                return this.mReadPendingIntent;
            }

            public String[] getParticipants() {
                return this.mParticipants;
            }

            public String getParticipant() {
                return this.mParticipants.length > 0 ? this.mParticipants[0] : null;
            }

            public long getLatestTimestamp() {
                return this.mLatestTimestamp;
            }

            static {
                NotificationCompatBase.UnreadConversation.Factory factory;
                new NotificationCompatBase.UnreadConversation.Factory() {
                    public UnreadConversation build(String[] strArr, RemoteInputCompatBase.RemoteInput remoteInput, PendingIntent pendingIntent, PendingIntent pendingIntent2, String[] strArr2, long j) {
                        UnreadConversation unreadConversation;
                        new UnreadConversation(strArr, (RemoteInput) remoteInput, pendingIntent, pendingIntent2, strArr2, j);
                        return unreadConversation;
                    }
                };
                FACTORY = factory;
            }

            public static class Builder {
                private long mLatestTimestamp;
                private final List<String> mMessages;
                private final String mParticipant;
                private PendingIntent mReadPendingIntent;
                private RemoteInput mRemoteInput;
                private PendingIntent mReplyPendingIntent;

                public Builder(String str) {
                    List<String> list;
                    new ArrayList();
                    this.mMessages = list;
                    this.mParticipant = str;
                }

                public Builder addMessage(String str) {
                    boolean add = this.mMessages.add(str);
                    return this;
                }

                public Builder setReplyAction(PendingIntent pendingIntent, RemoteInput remoteInput) {
                    this.mRemoteInput = remoteInput;
                    this.mReplyPendingIntent = pendingIntent;
                    return this;
                }

                public Builder setReadPendingIntent(PendingIntent pendingIntent) {
                    this.mReadPendingIntent = pendingIntent;
                    return this;
                }

                public Builder setLatestTimestamp(long j) {
                    this.mLatestTimestamp = j;
                    return this;
                }

                public UnreadConversation build() {
                    UnreadConversation unreadConversation;
                    new UnreadConversation((String[]) this.mMessages.toArray(new String[this.mMessages.size()]), this.mRemoteInput, this.mReplyPendingIntent, this.mReadPendingIntent, new String[]{this.mParticipant}, this.mLatestTimestamp);
                    return unreadConversation;
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public static Notification[] getNotificationArrayFromBundle(Bundle bundle, String str) {
        Bundle bundle2 = bundle;
        String str2 = str;
        Parcelable[] parcelableArray = bundle2.getParcelableArray(str2);
        if ((parcelableArray instanceof Notification[]) || parcelableArray == null) {
            return (Notification[]) parcelableArray;
        }
        Notification[] notificationArr = new Notification[parcelableArray.length];
        for (int i = 0; i < parcelableArray.length; i++) {
            notificationArr[i] = (Notification) parcelableArray[i];
        }
        bundle2.putParcelableArray(str2, notificationArr);
        return notificationArr;
    }

    public static Bundle getExtras(Notification notification) {
        return IMPL.getExtras(notification);
    }

    public static int getActionCount(Notification notification) {
        return IMPL.getActionCount(notification);
    }

    public static Action getAction(Notification notification, int i) {
        return IMPL.getAction(notification, i);
    }

    public static String getCategory(Notification notification) {
        return IMPL.getCategory(notification);
    }

    public static boolean getLocalOnly(Notification notification) {
        return IMPL.getLocalOnly(notification);
    }

    public static String getGroup(Notification notification) {
        return IMPL.getGroup(notification);
    }

    public static boolean isGroupSummary(Notification notification) {
        return IMPL.isGroupSummary(notification);
    }

    public static String getSortKey(Notification notification) {
        return IMPL.getSortKey(notification);
    }
}
