package android.support.v4.app;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.v4.content.IntentCompat;
import android.util.Log;

public class NavUtils {
    private static final NavUtilsImpl IMPL;
    public static final String PARENT_ACTIVITY = "android.support.PARENT_ACTIVITY";
    private static final String TAG = "NavUtils";

    interface NavUtilsImpl {
        Intent getParentActivityIntent(Activity activity);

        String getParentActivityName(Context context, ActivityInfo activityInfo);

        void navigateUpTo(Activity activity, Intent intent);

        boolean shouldUpRecreateTask(Activity activity, Intent intent);
    }

    static class NavUtilsImplBase implements NavUtilsImpl {
        NavUtilsImplBase() {
        }

        public Intent getParentActivityIntent(Activity activity) {
            ComponentName componentName;
            StringBuilder sb;
            Intent intent;
            Intent component;
            Activity activity2 = activity;
            String parentActivityName = NavUtils.getParentActivityName(activity2);
            if (parentActivityName == null) {
                return null;
            }
            new ComponentName(activity2, parentActivityName);
            ComponentName componentName2 = componentName;
            try {
                if (NavUtils.getParentActivityName(activity2, componentName2) == null) {
                    component = IntentCompat.makeMainActivity(componentName2);
                } else {
                    new Intent();
                    component = intent.setComponent(componentName2);
                }
                return component;
            } catch (PackageManager.NameNotFoundException e) {
                new StringBuilder();
                int e2 = Log.e(NavUtils.TAG, sb.append("getParentActivityIntent: bad parentActivityName '").append(parentActivityName).append("' in manifest").toString());
                return null;
            }
        }

        public boolean shouldUpRecreateTask(Activity activity, Intent intent) {
            String action = activity.getIntent().getAction();
            return action != null && !action.equals("android.intent.action.MAIN");
        }

        public void navigateUpTo(Activity activity, Intent intent) {
            Activity activity2 = activity;
            Intent intent2 = intent;
            Intent addFlags = intent2.addFlags(67108864);
            activity2.startActivity(intent2);
            activity2.finish();
        }

        public String getParentActivityName(Context context, ActivityInfo activityInfo) {
            StringBuilder sb;
            Context context2 = context;
            ActivityInfo activityInfo2 = activityInfo;
            if (activityInfo2.metaData == null) {
                return null;
            }
            String string = activityInfo2.metaData.getString(NavUtils.PARENT_ACTIVITY);
            if (string == null) {
                return null;
            }
            if (string.charAt(0) == '.') {
                new StringBuilder();
                string = sb.append(context2.getPackageName()).append(string).toString();
            }
            return string;
        }
    }

    static class NavUtilsImplJB extends NavUtilsImplBase {
        NavUtilsImplJB() {
        }

        public Intent getParentActivityIntent(Activity activity) {
            Activity activity2 = activity;
            Intent parentActivityIntent = NavUtilsJB.getParentActivityIntent(activity2);
            if (parentActivityIntent == null) {
                parentActivityIntent = superGetParentActivityIntent(activity2);
            }
            return parentActivityIntent;
        }

        /* access modifiers changed from: package-private */
        public Intent superGetParentActivityIntent(Activity activity) {
            return super.getParentActivityIntent(activity);
        }

        public boolean shouldUpRecreateTask(Activity activity, Intent intent) {
            return NavUtilsJB.shouldUpRecreateTask(activity, intent);
        }

        public void navigateUpTo(Activity activity, Intent intent) {
            NavUtilsJB.navigateUpTo(activity, intent);
        }

        public String getParentActivityName(Context context, ActivityInfo activityInfo) {
            Context context2 = context;
            ActivityInfo activityInfo2 = activityInfo;
            String parentActivityName = NavUtilsJB.getParentActivityName(activityInfo2);
            if (parentActivityName == null) {
                parentActivityName = super.getParentActivityName(context2, activityInfo2);
            }
            return parentActivityName;
        }
    }

    static {
        NavUtilsImpl navUtilsImpl;
        NavUtilsImpl navUtilsImpl2;
        if (Build.VERSION.SDK_INT >= 16) {
            new NavUtilsImplJB();
            IMPL = navUtilsImpl2;
            return;
        }
        new NavUtilsImplBase();
        IMPL = navUtilsImpl;
    }

    public static boolean shouldUpRecreateTask(Activity activity, Intent intent) {
        return IMPL.shouldUpRecreateTask(activity, intent);
    }

    public static void navigateUpFromSameTask(Activity activity) {
        Throwable th;
        StringBuilder sb;
        Activity activity2 = activity;
        Intent parentActivityIntent = getParentActivityIntent(activity2);
        if (parentActivityIntent == null) {
            Throwable th2 = th;
            new StringBuilder();
            new IllegalArgumentException(sb.append("Activity ").append(activity2.getClass().getSimpleName()).append(" does not have a parent activity name specified.").append(" (Did you forget to add the android.support.PARENT_ACTIVITY <meta-data> ").append(" element in your manifest?)").toString());
            throw th2;
        }
        navigateUpTo(activity2, parentActivityIntent);
    }

    public static void navigateUpTo(Activity activity, Intent intent) {
        IMPL.navigateUpTo(activity, intent);
    }

    public static Intent getParentActivityIntent(Activity activity) {
        return IMPL.getParentActivityIntent(activity);
    }

    public static Intent getParentActivityIntent(Context context, Class<?> cls) throws PackageManager.NameNotFoundException {
        ComponentName componentName;
        ComponentName componentName2;
        Intent intent;
        Intent component;
        Context context2 = context;
        new ComponentName(context2, cls);
        String parentActivityName = getParentActivityName(context2, componentName);
        if (parentActivityName == null) {
            return null;
        }
        new ComponentName(context2, parentActivityName);
        ComponentName componentName3 = componentName2;
        if (getParentActivityName(context2, componentName3) == null) {
            component = IntentCompat.makeMainActivity(componentName3);
        } else {
            new Intent();
            component = intent.setComponent(componentName3);
        }
        return component;
    }

    public static Intent getParentActivityIntent(Context context, ComponentName componentName) throws PackageManager.NameNotFoundException {
        ComponentName componentName2;
        Intent intent;
        Intent component;
        Context context2 = context;
        ComponentName componentName3 = componentName;
        String parentActivityName = getParentActivityName(context2, componentName3);
        if (parentActivityName == null) {
            return null;
        }
        new ComponentName(componentName3.getPackageName(), parentActivityName);
        ComponentName componentName4 = componentName2;
        if (getParentActivityName(context2, componentName4) == null) {
            component = IntentCompat.makeMainActivity(componentName4);
        } else {
            new Intent();
            component = intent.setComponent(componentName4);
        }
        return component;
    }

    @Nullable
    public static String getParentActivityName(Activity activity) {
        Throwable th;
        Activity activity2 = activity;
        try {
            return getParentActivityName(activity2, activity2.getComponentName());
        } catch (PackageManager.NameNotFoundException e) {
            PackageManager.NameNotFoundException nameNotFoundException = e;
            Throwable th2 = th;
            new IllegalArgumentException(nameNotFoundException);
            throw th2;
        }
    }

    @Nullable
    public static String getParentActivityName(Context context, ComponentName componentName) throws PackageManager.NameNotFoundException {
        Context context2 = context;
        return IMPL.getParentActivityName(context2, context2.getPackageManager().getActivityInfo(componentName, 128));
    }

    private NavUtils() {
    }
}
