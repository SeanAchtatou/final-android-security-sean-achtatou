package android.support.v4.media;

import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

public final class RatingCompat implements Parcelable {
    public static final Parcelable.Creator<RatingCompat> CREATOR;
    public static final int RATING_3_STARS = 3;
    public static final int RATING_4_STARS = 4;
    public static final int RATING_5_STARS = 5;
    public static final int RATING_HEART = 1;
    public static final int RATING_NONE = 0;
    private static final float RATING_NOT_RATED = -1.0f;
    public static final int RATING_PERCENTAGE = 6;
    public static final int RATING_THUMB_UP_DOWN = 2;
    private static final String TAG = "Rating";
    private Object mRatingObj;
    private final int mRatingStyle;
    private final float mRatingValue;

    private RatingCompat(int i, float f) {
        this.mRatingStyle = i;
        this.mRatingValue = f;
    }

    public String toString() {
        StringBuilder sb;
        new StringBuilder();
        return sb.append("Rating:style=").append(this.mRatingStyle).append(" rating=").append(this.mRatingValue < 0.0f ? "unrated" : String.valueOf(this.mRatingValue)).toString();
    }

    public int describeContents() {
        return this.mRatingStyle;
    }

    public void writeToParcel(Parcel parcel, int i) {
        Parcel parcel2 = parcel;
        parcel2.writeInt(this.mRatingStyle);
        parcel2.writeFloat(this.mRatingValue);
    }

    static {
        Parcelable.Creator<RatingCompat> creator;
        new Parcelable.Creator<RatingCompat>() {
            public RatingCompat createFromParcel(Parcel parcel) {
                RatingCompat ratingCompat;
                Parcel parcel2 = parcel;
                new RatingCompat(parcel2.readInt(), parcel2.readFloat());
                return ratingCompat;
            }

            public RatingCompat[] newArray(int i) {
                return new RatingCompat[i];
            }
        };
        CREATOR = creator;
    }

    public static RatingCompat newUnratedRating(int i) {
        RatingCompat ratingCompat;
        int i2 = i;
        switch (i2) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
                new RatingCompat(i2, RATING_NOT_RATED);
                return ratingCompat;
            default:
                return null;
        }
    }

    public static RatingCompat newHeartRating(boolean z) {
        RatingCompat ratingCompat;
        RatingCompat ratingCompat2 = ratingCompat;
        new RatingCompat(1, z ? 1.0f : 0.0f);
        return ratingCompat2;
    }

    public static RatingCompat newThumbRating(boolean z) {
        RatingCompat ratingCompat;
        RatingCompat ratingCompat2 = ratingCompat;
        new RatingCompat(2, z ? 1.0f : 0.0f);
        return ratingCompat2;
    }

    public static RatingCompat newStarRating(int i, float f) {
        float f2;
        RatingCompat ratingCompat;
        StringBuilder sb;
        int i2 = i;
        float f3 = f;
        switch (i2) {
            case 3:
                f2 = 3.0f;
                break;
            case 4:
                f2 = 4.0f;
                break;
            case 5:
                f2 = 5.0f;
                break;
            default:
                new StringBuilder();
                int e = Log.e(TAG, sb.append("Invalid rating style (").append(i2).append(") for a star rating").toString());
                return null;
        }
        if (f3 < 0.0f || f3 > f2) {
            int e2 = Log.e(TAG, "Trying to set out of range star-based rating");
            return null;
        }
        new RatingCompat(i2, f3);
        return ratingCompat;
    }

    public static RatingCompat newPercentageRating(float f) {
        RatingCompat ratingCompat;
        float f2 = f;
        if (f2 < 0.0f || f2 > 100.0f) {
            int e = Log.e(TAG, "Invalid percentage-based rating value");
            return null;
        }
        new RatingCompat(6, f2);
        return ratingCompat;
    }

    public boolean isRated() {
        return this.mRatingValue >= 0.0f;
    }

    public int getRatingStyle() {
        return this.mRatingStyle;
    }

    public boolean hasHeart() {
        if (this.mRatingStyle != 1) {
            return false;
        }
        return this.mRatingValue == 1.0f;
    }

    public boolean isThumbUp() {
        if (this.mRatingStyle != 2) {
            return false;
        }
        return this.mRatingValue == 1.0f;
    }

    public float getStarRating() {
        switch (this.mRatingStyle) {
            case 3:
            case 4:
            case 5:
                if (isRated()) {
                    return this.mRatingValue;
                }
                break;
        }
        return RATING_NOT_RATED;
    }

    public float getPercentRating() {
        if (this.mRatingStyle != 6 || !isRated()) {
            return RATING_NOT_RATED;
        }
        return this.mRatingValue;
    }

    public static RatingCompat fromRating(Object obj) {
        RatingCompat newUnratedRating;
        Object obj2 = obj;
        if (obj2 == null || Build.VERSION.SDK_INT < 21) {
            return null;
        }
        int ratingStyle = RatingCompatApi21.getRatingStyle(obj2);
        if (RatingCompatApi21.isRated(obj2)) {
            switch (ratingStyle) {
                case 1:
                    newUnratedRating = newHeartRating(RatingCompatApi21.hasHeart(obj2));
                    break;
                case 2:
                    newUnratedRating = newThumbRating(RatingCompatApi21.isThumbUp(obj2));
                    break;
                case 3:
                case 4:
                case 5:
                    newUnratedRating = newStarRating(ratingStyle, RatingCompatApi21.getStarRating(obj2));
                    break;
                case 6:
                    newUnratedRating = newPercentageRating(RatingCompatApi21.getPercentRating(obj2));
                    break;
                default:
                    return null;
            }
        } else {
            newUnratedRating = newUnratedRating(ratingStyle);
        }
        newUnratedRating.mRatingObj = obj2;
        return newUnratedRating;
    }

    public Object getRating() {
        if (this.mRatingObj != null || Build.VERSION.SDK_INT < 21) {
            return this.mRatingObj;
        }
        if (isRated()) {
            switch (this.mRatingStyle) {
                case 1:
                    this.mRatingObj = RatingCompatApi21.newHeartRating(hasHeart());
                    break;
                case 2:
                    this.mRatingObj = RatingCompatApi21.newThumbRating(isThumbUp());
                    break;
                case 3:
                case 4:
                case 5:
                    this.mRatingObj = RatingCompatApi21.newStarRating(this.mRatingStyle, getStarRating());
                    break;
                case 6:
                    this.mRatingObj = RatingCompatApi21.newPercentageRating(getPercentRating());
                    return null;
                default:
                    return null;
            }
        } else {
            this.mRatingObj = RatingCompatApi21.newUnratedRating(this.mRatingStyle);
        }
        return this.mRatingObj;
    }
}
