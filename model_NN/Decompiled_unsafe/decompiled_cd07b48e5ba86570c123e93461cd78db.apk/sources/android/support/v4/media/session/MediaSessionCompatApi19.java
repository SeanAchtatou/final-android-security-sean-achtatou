package android.support.v4.media.session;

import android.media.MediaMetadataEditor;
import android.media.RemoteControlClient;
import android.os.Bundle;
import android.support.v4.media.session.MediaSessionCompatApi14;

public class MediaSessionCompatApi19 {
    private static final long ACTION_SET_RATING = 128;
    private static final String METADATA_KEY_RATING = "android.media.metadata.RATING";
    private static final String METADATA_KEY_USER_RATING = "android.media.metadata.USER_RATING";
    private static final String METADATA_KEY_YEAR = "android.media.metadata.YEAR";

    public static void setTransportControlFlags(Object obj, long j) {
        ((RemoteControlClient) obj).setTransportControlFlags(getRccTransportControlFlagsFromActions(j));
    }

    public static Object createMetadataUpdateListener(MediaSessionCompatApi14.Callback callback) {
        Object obj;
        new OnMetadataUpdateListener(callback);
        return obj;
    }

    public static void setMetadata(Object obj, Bundle bundle, long j) {
        Bundle bundle2 = bundle;
        RemoteControlClient.MetadataEditor editMetadata = ((RemoteControlClient) obj).editMetadata(true);
        MediaSessionCompatApi14.buildOldMetadata(bundle2, editMetadata);
        addNewMetadata(bundle2, editMetadata);
        if ((j & 128) != 0) {
            editMetadata.addEditableKey(268435457);
        }
        editMetadata.apply();
    }

    public static void setOnMetadataUpdateListener(Object obj, Object obj2) {
        ((RemoteControlClient) obj).setMetadataUpdateListener((RemoteControlClient.OnMetadataUpdateListener) obj2);
    }

    static int getRccTransportControlFlagsFromActions(long j) {
        long j2 = j;
        int rccTransportControlFlagsFromActions = MediaSessionCompatApi18.getRccTransportControlFlagsFromActions(j2);
        if ((j2 & 128) != 0) {
            rccTransportControlFlagsFromActions |= 512;
        }
        return rccTransportControlFlagsFromActions;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.media.RemoteControlClient.MetadataEditor.putLong(int, long):android.media.RemoteControlClient$MetadataEditor throws java.lang.IllegalArgumentException}
     arg types: [int, long]
     candidates:
      ClspMth{android.media.RemoteControlClient.MetadataEditor.putLong(int, long):android.media.MediaMetadataEditor throws java.lang.IllegalArgumentException}
      ClspMth{android.media.MediaMetadataEditor.putLong(int, long):android.media.MediaMetadataEditor throws java.lang.IllegalArgumentException}
      ClspMth{android.media.RemoteControlClient.MetadataEditor.putLong(int, long):android.media.RemoteControlClient$MetadataEditor throws java.lang.IllegalArgumentException} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.media.RemoteControlClient.MetadataEditor.putObject(int, java.lang.Object):android.media.MediaMetadataEditor throws java.lang.IllegalArgumentException}
     arg types: [int, android.os.Parcelable]
     candidates:
      ClspMth{android.media.RemoteControlClient.MetadataEditor.putObject(int, java.lang.Object):android.media.RemoteControlClient$MetadataEditor throws java.lang.IllegalArgumentException}
      ClspMth{android.media.RemoteControlClient.MetadataEditor.putObject(int, java.lang.Object):android.media.MediaMetadataEditor throws java.lang.IllegalArgumentException} */
    static void addNewMetadata(Bundle bundle, RemoteControlClient.MetadataEditor metadataEditor) {
        Bundle bundle2 = bundle;
        RemoteControlClient.MetadataEditor metadataEditor2 = metadataEditor;
        if (bundle2 != null) {
            if (bundle2.containsKey("android.media.metadata.YEAR")) {
                RemoteControlClient.MetadataEditor putLong = metadataEditor2.putLong(8, bundle2.getLong("android.media.metadata.YEAR"));
            }
            if (bundle2.containsKey("android.media.metadata.RATING")) {
                MediaMetadataEditor putObject = metadataEditor2.putObject(101, (Object) bundle2.getParcelable("android.media.metadata.RATING"));
            }
            if (bundle2.containsKey("android.media.metadata.USER_RATING")) {
                MediaMetadataEditor putObject2 = metadataEditor2.putObject(268435457, (Object) bundle2.getParcelable("android.media.metadata.USER_RATING"));
            }
        }
    }

    static class OnMetadataUpdateListener<T extends MediaSessionCompatApi14.Callback> implements RemoteControlClient.OnMetadataUpdateListener {
        protected final T mCallback;

        public OnMetadataUpdateListener(T t) {
            this.mCallback = t;
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public void onMetadataUpdate(int r6, java.lang.Object r7) {
            /*
                r5 = this;
                r0 = r5
                r1 = r6
                r2 = r7
                r3 = r1
                r4 = 268435457(0x10000001, float:2.5243552E-29)
                if (r3 != r4) goto L_0x0015
                r3 = r2
                boolean r3 = r3 instanceof android.media.Rating
                if (r3 == 0) goto L_0x0015
                r3 = r0
                T r3 = r3.mCallback
                r4 = r2
                r3.onSetRating(r4)
            L_0x0015:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: android.support.v4.media.session.MediaSessionCompatApi19.OnMetadataUpdateListener.onMetadataUpdate(int, java.lang.Object):void");
        }
    }
}
