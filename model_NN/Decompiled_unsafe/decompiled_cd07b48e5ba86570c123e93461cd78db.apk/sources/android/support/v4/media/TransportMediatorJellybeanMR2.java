package android.support.v4.media;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.RemoteControlClient;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewTreeObserver;

class TransportMediatorJellybeanMR2 implements RemoteControlClient.OnGetPlaybackPositionListener, RemoteControlClient.OnPlaybackPositionUpdateListener {
    AudioManager.OnAudioFocusChangeListener mAudioFocusChangeListener;
    boolean mAudioFocused;
    final AudioManager mAudioManager;
    final Context mContext;
    boolean mFocused;
    final Intent mIntent;
    final BroadcastReceiver mMediaButtonReceiver;
    PendingIntent mPendingIntent;
    int mPlayState = 0;
    final String mReceiverAction;
    final IntentFilter mReceiverFilter;
    RemoteControlClient mRemoteControl;
    final View mTargetView;
    final TransportMediatorCallback mTransportCallback;
    final ViewTreeObserver.OnWindowAttachListener mWindowAttachListener;
    final ViewTreeObserver.OnWindowFocusChangeListener mWindowFocusListener;

    public TransportMediatorJellybeanMR2(Context context, AudioManager audioManager, View view, TransportMediatorCallback transportMediatorCallback) {
        ViewTreeObserver.OnWindowAttachListener onWindowAttachListener;
        ViewTreeObserver.OnWindowFocusChangeListener onWindowFocusChangeListener;
        BroadcastReceiver broadcastReceiver;
        AudioManager.OnAudioFocusChangeListener onAudioFocusChangeListener;
        StringBuilder sb;
        Intent intent;
        IntentFilter intentFilter;
        Context context2 = context;
        new ViewTreeObserver.OnWindowAttachListener() {
            public void onWindowAttached() {
                TransportMediatorJellybeanMR2.this.windowAttached();
            }

            public void onWindowDetached() {
                TransportMediatorJellybeanMR2.this.windowDetached();
            }
        };
        this.mWindowAttachListener = onWindowAttachListener;
        new ViewTreeObserver.OnWindowFocusChangeListener() {
            public void onWindowFocusChanged(boolean z) {
                if (z) {
                    TransportMediatorJellybeanMR2.this.gainFocus();
                } else {
                    TransportMediatorJellybeanMR2.this.loseFocus();
                }
            }
        };
        this.mWindowFocusListener = onWindowFocusChangeListener;
        new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                try {
                    TransportMediatorJellybeanMR2.this.mTransportCallback.handleKey((KeyEvent) intent.getParcelableExtra("android.intent.extra.KEY_EVENT"));
                } catch (ClassCastException e) {
                    int w = Log.w("TransportController", e);
                }
            }
        };
        this.mMediaButtonReceiver = broadcastReceiver;
        new AudioManager.OnAudioFocusChangeListener() {
            public void onAudioFocusChange(int i) {
                TransportMediatorJellybeanMR2.this.mTransportCallback.handleAudioFocusChange(i);
            }
        };
        this.mAudioFocusChangeListener = onAudioFocusChangeListener;
        this.mContext = context2;
        this.mAudioManager = audioManager;
        this.mTargetView = view;
        this.mTransportCallback = transportMediatorCallback;
        new StringBuilder();
        this.mReceiverAction = sb.append(context2.getPackageName()).append(":transport:").append(System.identityHashCode(this)).toString();
        new Intent(this.mReceiverAction);
        this.mIntent = intent;
        Intent intent2 = this.mIntent.setPackage(context2.getPackageName());
        new IntentFilter();
        this.mReceiverFilter = intentFilter;
        this.mReceiverFilter.addAction(this.mReceiverAction);
        this.mTargetView.getViewTreeObserver().addOnWindowAttachListener(this.mWindowAttachListener);
        this.mTargetView.getViewTreeObserver().addOnWindowFocusChangeListener(this.mWindowFocusListener);
    }

    public Object getRemoteControlClient() {
        return this.mRemoteControl;
    }

    public void destroy() {
        windowDetached();
        this.mTargetView.getViewTreeObserver().removeOnWindowAttachListener(this.mWindowAttachListener);
        this.mTargetView.getViewTreeObserver().removeOnWindowFocusChangeListener(this.mWindowFocusListener);
    }

    /* access modifiers changed from: package-private */
    public void windowAttached() {
        RemoteControlClient remoteControlClient;
        Intent registerReceiver = this.mContext.registerReceiver(this.mMediaButtonReceiver, this.mReceiverFilter);
        this.mPendingIntent = PendingIntent.getBroadcast(this.mContext, 0, this.mIntent, 268435456);
        new RemoteControlClient(this.mPendingIntent);
        this.mRemoteControl = remoteControlClient;
        this.mRemoteControl.setOnGetPlaybackPositionListener(this);
        this.mRemoteControl.setPlaybackPositionUpdateListener(this);
    }

    /* access modifiers changed from: package-private */
    public void gainFocus() {
        if (!this.mFocused) {
            this.mFocused = true;
            this.mAudioManager.registerMediaButtonEventReceiver(this.mPendingIntent);
            this.mAudioManager.registerRemoteControlClient(this.mRemoteControl);
            if (this.mPlayState == 3) {
                takeAudioFocus();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void takeAudioFocus() {
        if (!this.mAudioFocused) {
            this.mAudioFocused = true;
            int requestAudioFocus = this.mAudioManager.requestAudioFocus(this.mAudioFocusChangeListener, 3, 1);
        }
    }

    public void startPlaying() {
        if (this.mPlayState != 3) {
            this.mPlayState = 3;
            this.mRemoteControl.setPlaybackState(3);
        }
        if (this.mFocused) {
            takeAudioFocus();
        }
    }

    public long onGetPlaybackPosition() {
        return this.mTransportCallback.getPlaybackPosition();
    }

    public void onPlaybackPositionUpdate(long j) {
        this.mTransportCallback.playbackPositionUpdate(j);
    }

    public void refreshState(boolean z, long j, int i) {
        boolean z2 = z;
        long j2 = j;
        int i2 = i;
        if (this.mRemoteControl != null) {
            this.mRemoteControl.setPlaybackState(z2 ? 3 : 1, j2, z2 ? 1.0f : 0.0f);
            this.mRemoteControl.setTransportControlFlags(i2);
        }
    }

    public void pausePlaying() {
        if (this.mPlayState == 3) {
            this.mPlayState = 2;
            this.mRemoteControl.setPlaybackState(2);
        }
        dropAudioFocus();
    }

    public void stopPlaying() {
        if (this.mPlayState != 1) {
            this.mPlayState = 1;
            this.mRemoteControl.setPlaybackState(1);
        }
        dropAudioFocus();
    }

    /* access modifiers changed from: package-private */
    public void dropAudioFocus() {
        if (this.mAudioFocused) {
            this.mAudioFocused = false;
            int abandonAudioFocus = this.mAudioManager.abandonAudioFocus(this.mAudioFocusChangeListener);
        }
    }

    /* access modifiers changed from: package-private */
    public void loseFocus() {
        dropAudioFocus();
        if (this.mFocused) {
            this.mFocused = false;
            this.mAudioManager.unregisterRemoteControlClient(this.mRemoteControl);
            this.mAudioManager.unregisterMediaButtonEventReceiver(this.mPendingIntent);
        }
    }

    /* access modifiers changed from: package-private */
    public void windowDetached() {
        loseFocus();
        if (this.mPendingIntent != null) {
            this.mContext.unregisterReceiver(this.mMediaButtonReceiver);
            this.mPendingIntent.cancel();
            this.mPendingIntent = null;
            this.mRemoteControl = null;
        }
    }
}
