package android.support.v4.media;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.media.MediaDescriptionCompat;
import android.support.v4.media.MediaMetadataCompatApi21;
import android.support.v4.util.ArrayMap;
import android.text.TextUtils;
import android.util.Log;
import java.util.Set;

public final class MediaMetadataCompat implements Parcelable {
    public static final Parcelable.Creator<MediaMetadataCompat> CREATOR;
    /* access modifiers changed from: private */
    public static final ArrayMap<String, Integer> METADATA_KEYS_TYPE;
    public static final String METADATA_KEY_ALBUM = "android.media.metadata.ALBUM";
    public static final String METADATA_KEY_ALBUM_ART = "android.media.metadata.ALBUM_ART";
    public static final String METADATA_KEY_ALBUM_ARTIST = "android.media.metadata.ALBUM_ARTIST";
    public static final String METADATA_KEY_ALBUM_ART_URI = "android.media.metadata.ALBUM_ART_URI";
    public static final String METADATA_KEY_ART = "android.media.metadata.ART";
    public static final String METADATA_KEY_ARTIST = "android.media.metadata.ARTIST";
    public static final String METADATA_KEY_ART_URI = "android.media.metadata.ART_URI";
    public static final String METADATA_KEY_AUTHOR = "android.media.metadata.AUTHOR";
    public static final String METADATA_KEY_COMPILATION = "android.media.metadata.COMPILATION";
    public static final String METADATA_KEY_COMPOSER = "android.media.metadata.COMPOSER";
    public static final String METADATA_KEY_DATE = "android.media.metadata.DATE";
    public static final String METADATA_KEY_DISC_NUMBER = "android.media.metadata.DISC_NUMBER";
    public static final String METADATA_KEY_DISPLAY_DESCRIPTION = "android.media.metadata.DISPLAY_DESCRIPTION";
    public static final String METADATA_KEY_DISPLAY_ICON = "android.media.metadata.DISPLAY_ICON";
    public static final String METADATA_KEY_DISPLAY_ICON_URI = "android.media.metadata.DISPLAY_ICON_URI";
    public static final String METADATA_KEY_DISPLAY_SUBTITLE = "android.media.metadata.DISPLAY_SUBTITLE";
    public static final String METADATA_KEY_DISPLAY_TITLE = "android.media.metadata.DISPLAY_TITLE";
    public static final String METADATA_KEY_DURATION = "android.media.metadata.DURATION";
    public static final String METADATA_KEY_GENRE = "android.media.metadata.GENRE";
    public static final String METADATA_KEY_MEDIA_ID = "android.media.metadata.MEDIA_ID";
    public static final String METADATA_KEY_NUM_TRACKS = "android.media.metadata.NUM_TRACKS";
    public static final String METADATA_KEY_RATING = "android.media.metadata.RATING";
    public static final String METADATA_KEY_TITLE = "android.media.metadata.TITLE";
    public static final String METADATA_KEY_TRACK_NUMBER = "android.media.metadata.TRACK_NUMBER";
    public static final String METADATA_KEY_USER_RATING = "android.media.metadata.USER_RATING";
    public static final String METADATA_KEY_WRITER = "android.media.metadata.WRITER";
    public static final String METADATA_KEY_YEAR = "android.media.metadata.YEAR";
    private static final int METADATA_TYPE_BITMAP = 2;
    private static final int METADATA_TYPE_LONG = 0;
    private static final int METADATA_TYPE_RATING = 3;
    private static final int METADATA_TYPE_TEXT = 1;
    private static final String[] PREFERRED_BITMAP_ORDER;
    private static final String[] PREFERRED_DESCRIPTION_ORDER;
    private static final String[] PREFERRED_URI_ORDER;
    private static final String TAG = "MediaMetadata";
    /* access modifiers changed from: private */
    public final Bundle mBundle;
    private MediaDescriptionCompat mDescription;
    private Object mMetadataObj;

    static {
        ArrayMap<String, Integer> arrayMap;
        Parcelable.Creator<MediaMetadataCompat> creator;
        new ArrayMap<>();
        METADATA_KEYS_TYPE = arrayMap;
        Integer put = METADATA_KEYS_TYPE.put(METADATA_KEY_TITLE, 1);
        Integer put2 = METADATA_KEYS_TYPE.put(METADATA_KEY_ARTIST, 1);
        Integer put3 = METADATA_KEYS_TYPE.put(METADATA_KEY_DURATION, 0);
        Integer put4 = METADATA_KEYS_TYPE.put(METADATA_KEY_ALBUM, 1);
        Integer put5 = METADATA_KEYS_TYPE.put(METADATA_KEY_AUTHOR, 1);
        Integer put6 = METADATA_KEYS_TYPE.put(METADATA_KEY_WRITER, 1);
        Integer put7 = METADATA_KEYS_TYPE.put(METADATA_KEY_COMPOSER, 1);
        Integer put8 = METADATA_KEYS_TYPE.put(METADATA_KEY_COMPILATION, 1);
        Integer put9 = METADATA_KEYS_TYPE.put(METADATA_KEY_DATE, 1);
        Integer put10 = METADATA_KEYS_TYPE.put(METADATA_KEY_YEAR, 0);
        Integer put11 = METADATA_KEYS_TYPE.put(METADATA_KEY_GENRE, 1);
        Integer put12 = METADATA_KEYS_TYPE.put(METADATA_KEY_TRACK_NUMBER, 0);
        Integer put13 = METADATA_KEYS_TYPE.put(METADATA_KEY_NUM_TRACKS, 0);
        Integer put14 = METADATA_KEYS_TYPE.put(METADATA_KEY_DISC_NUMBER, 0);
        Integer put15 = METADATA_KEYS_TYPE.put(METADATA_KEY_ALBUM_ARTIST, 1);
        Integer put16 = METADATA_KEYS_TYPE.put(METADATA_KEY_ART, 2);
        Integer put17 = METADATA_KEYS_TYPE.put(METADATA_KEY_ART_URI, 1);
        Integer put18 = METADATA_KEYS_TYPE.put(METADATA_KEY_ALBUM_ART, 2);
        Integer put19 = METADATA_KEYS_TYPE.put(METADATA_KEY_ALBUM_ART_URI, 1);
        Integer put20 = METADATA_KEYS_TYPE.put(METADATA_KEY_USER_RATING, 3);
        Integer put21 = METADATA_KEYS_TYPE.put(METADATA_KEY_RATING, 3);
        Integer put22 = METADATA_KEYS_TYPE.put(METADATA_KEY_DISPLAY_TITLE, 1);
        Integer put23 = METADATA_KEYS_TYPE.put(METADATA_KEY_DISPLAY_SUBTITLE, 1);
        Integer put24 = METADATA_KEYS_TYPE.put(METADATA_KEY_DISPLAY_DESCRIPTION, 1);
        Integer put25 = METADATA_KEYS_TYPE.put(METADATA_KEY_DISPLAY_ICON, 2);
        Integer put26 = METADATA_KEYS_TYPE.put(METADATA_KEY_DISPLAY_ICON_URI, 1);
        Integer put27 = METADATA_KEYS_TYPE.put(METADATA_KEY_MEDIA_ID, 1);
        String[] strArr = new String[7];
        strArr[0] = METADATA_KEY_TITLE;
        String[] strArr2 = strArr;
        strArr2[1] = METADATA_KEY_ARTIST;
        String[] strArr3 = strArr2;
        strArr3[2] = METADATA_KEY_ALBUM;
        String[] strArr4 = strArr3;
        strArr4[3] = METADATA_KEY_ALBUM_ARTIST;
        String[] strArr5 = strArr4;
        strArr5[4] = METADATA_KEY_WRITER;
        String[] strArr6 = strArr5;
        strArr6[5] = METADATA_KEY_AUTHOR;
        String[] strArr7 = strArr6;
        strArr7[6] = METADATA_KEY_COMPOSER;
        PREFERRED_DESCRIPTION_ORDER = strArr7;
        String[] strArr8 = new String[3];
        strArr8[0] = METADATA_KEY_DISPLAY_ICON;
        String[] strArr9 = strArr8;
        strArr9[1] = METADATA_KEY_ART;
        String[] strArr10 = strArr9;
        strArr10[2] = METADATA_KEY_ALBUM_ART;
        PREFERRED_BITMAP_ORDER = strArr10;
        String[] strArr11 = new String[3];
        strArr11[0] = METADATA_KEY_DISPLAY_ICON_URI;
        String[] strArr12 = strArr11;
        strArr12[1] = METADATA_KEY_ART_URI;
        String[] strArr13 = strArr12;
        strArr13[2] = METADATA_KEY_ALBUM_ART_URI;
        PREFERRED_URI_ORDER = strArr13;
        new Parcelable.Creator<MediaMetadataCompat>() {
            public MediaMetadataCompat createFromParcel(Parcel parcel) {
                MediaMetadataCompat mediaMetadataCompat;
                new MediaMetadataCompat(parcel);
                return mediaMetadataCompat;
            }

            public MediaMetadataCompat[] newArray(int i) {
                return new MediaMetadataCompat[i];
            }
        };
        CREATOR = creator;
    }

    private MediaMetadataCompat(Bundle bundle) {
        Bundle bundle2;
        new Bundle(bundle);
        this.mBundle = bundle2;
    }

    private MediaMetadataCompat(Parcel parcel) {
        this.mBundle = parcel.readBundle();
    }

    public boolean containsKey(String str) {
        return this.mBundle.containsKey(str);
    }

    public CharSequence getText(String str) {
        return this.mBundle.getCharSequence(str);
    }

    public String getString(String str) {
        CharSequence charSequence = this.mBundle.getCharSequence(str);
        if (charSequence != null) {
            return charSequence.toString();
        }
        return null;
    }

    public long getLong(String str) {
        return this.mBundle.getLong(str, 0);
    }

    public RatingCompat getRating(String str) {
        RatingCompat ratingCompat = null;
        try {
            ratingCompat = (RatingCompat) this.mBundle.getParcelable(str);
        } catch (Exception e) {
            int w = Log.w(TAG, "Failed to retrieve a key as Rating.", e);
        }
        return ratingCompat;
    }

    public Bitmap getBitmap(String str) {
        Bitmap bitmap = null;
        try {
            bitmap = (Bitmap) this.mBundle.getParcelable(str);
        } catch (Exception e) {
            int w = Log.w(TAG, "Failed to retrieve a key as Bitmap.", e);
        }
        return bitmap;
    }

    public MediaDescriptionCompat getDescription() {
        MediaDescriptionCompat.Builder builder;
        if (this.mDescription != null) {
            return this.mDescription;
        }
        String string = getString(METADATA_KEY_MEDIA_ID);
        CharSequence[] charSequenceArr = new CharSequence[3];
        Bitmap bitmap = null;
        Uri uri = null;
        CharSequence text = getText(METADATA_KEY_DISPLAY_TITLE);
        if (!TextUtils.isEmpty(text)) {
            charSequenceArr[0] = text;
            charSequenceArr[1] = getText(METADATA_KEY_DISPLAY_SUBTITLE);
            charSequenceArr[2] = getText(METADATA_KEY_DISPLAY_DESCRIPTION);
        } else {
            int i = 0;
            int i2 = 0;
            while (i < charSequenceArr.length && i2 < PREFERRED_DESCRIPTION_ORDER.length) {
                int i3 = i2;
                i2++;
                CharSequence text2 = getText(PREFERRED_DESCRIPTION_ORDER[i3]);
                if (!TextUtils.isEmpty(text2)) {
                    int i4 = i;
                    i++;
                    charSequenceArr[i4] = text2;
                }
            }
        }
        int i5 = 0;
        while (true) {
            if (i5 >= PREFERRED_BITMAP_ORDER.length) {
                break;
            }
            Bitmap bitmap2 = getBitmap(PREFERRED_BITMAP_ORDER[i5]);
            if (bitmap2 != null) {
                bitmap = bitmap2;
                break;
            }
            i5++;
        }
        int i6 = 0;
        while (true) {
            if (i6 >= PREFERRED_URI_ORDER.length) {
                break;
            }
            String string2 = getString(PREFERRED_URI_ORDER[i6]);
            if (!TextUtils.isEmpty(string2)) {
                uri = Uri.parse(string2);
                break;
            }
            i6++;
        }
        new MediaDescriptionCompat.Builder();
        MediaDescriptionCompat.Builder builder2 = builder;
        MediaDescriptionCompat.Builder mediaId = builder2.setMediaId(string);
        MediaDescriptionCompat.Builder title = builder2.setTitle(charSequenceArr[0]);
        MediaDescriptionCompat.Builder subtitle = builder2.setSubtitle(charSequenceArr[1]);
        MediaDescriptionCompat.Builder description = builder2.setDescription(charSequenceArr[2]);
        MediaDescriptionCompat.Builder iconBitmap = builder2.setIconBitmap(bitmap);
        MediaDescriptionCompat.Builder iconUri = builder2.setIconUri(uri);
        this.mDescription = builder2.build();
        return this.mDescription;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeBundle(this.mBundle);
    }

    public int size() {
        return this.mBundle.size();
    }

    public Set<String> keySet() {
        return this.mBundle.keySet();
    }

    public Bundle getBundle() {
        return this.mBundle;
    }

    public static MediaMetadataCompat fromMediaMetadata(Object obj) {
        Builder builder;
        Object obj2 = obj;
        if (obj2 == null || Build.VERSION.SDK_INT < 21) {
            return null;
        }
        new Builder();
        Builder builder2 = builder;
        for (String next : MediaMetadataCompatApi21.keySet(obj2)) {
            Integer num = METADATA_KEYS_TYPE.get(next);
            if (num != null) {
                switch (num.intValue()) {
                    case 0:
                        Builder putLong = builder2.putLong(next, MediaMetadataCompatApi21.getLong(obj2, next));
                        continue;
                    case 1:
                        Builder putText = builder2.putText(next, MediaMetadataCompatApi21.getText(obj2, next));
                        continue;
                    case 2:
                        Builder putBitmap = builder2.putBitmap(next, MediaMetadataCompatApi21.getBitmap(obj2, next));
                        continue;
                    case 3:
                        Builder putRating = builder2.putRating(next, RatingCompat.fromRating(MediaMetadataCompatApi21.getRating(obj2, next)));
                        continue;
                }
            }
        }
        MediaMetadataCompat build = builder2.build();
        build.mMetadataObj = obj2;
        return build;
    }

    public Object getMediaMetadata() {
        if (this.mMetadataObj != null || Build.VERSION.SDK_INT < 21) {
            return this.mMetadataObj;
        }
        Object newInstance = MediaMetadataCompatApi21.Builder.newInstance();
        for (String next : keySet()) {
            Integer num = METADATA_KEYS_TYPE.get(next);
            if (num != null) {
                switch (num.intValue()) {
                    case 0:
                        MediaMetadataCompatApi21.Builder.putLong(newInstance, next, getLong(next));
                        continue;
                    case 1:
                        MediaMetadataCompatApi21.Builder.putText(newInstance, next, getText(next));
                        continue;
                    case 2:
                        MediaMetadataCompatApi21.Builder.putBitmap(newInstance, next, getBitmap(next));
                        continue;
                    case 3:
                        MediaMetadataCompatApi21.Builder.putRating(newInstance, next, getRating(next).getRating());
                        continue;
                }
            }
        }
        this.mMetadataObj = MediaMetadataCompatApi21.Builder.build(newInstance);
        return this.mMetadataObj;
    }

    public static final class Builder {
        private final Bundle mBundle;

        public Builder() {
            Bundle bundle;
            new Bundle();
            this.mBundle = bundle;
        }

        public Builder(MediaMetadataCompat mediaMetadataCompat) {
            Bundle bundle;
            new Bundle(mediaMetadataCompat.mBundle);
            this.mBundle = bundle;
        }

        public Builder putText(String str, CharSequence charSequence) {
            Throwable th;
            StringBuilder sb;
            String str2 = str;
            CharSequence charSequence2 = charSequence;
            if (!MediaMetadataCompat.METADATA_KEYS_TYPE.containsKey(str2) || ((Integer) MediaMetadataCompat.METADATA_KEYS_TYPE.get(str2)).intValue() == 1) {
                this.mBundle.putCharSequence(str2, charSequence2);
                return this;
            }
            Throwable th2 = th;
            new StringBuilder();
            new IllegalArgumentException(sb.append("The ").append(str2).append(" key cannot be used to put a CharSequence").toString());
            throw th2;
        }

        public Builder putString(String str, String str2) {
            Throwable th;
            StringBuilder sb;
            String str3 = str;
            String str4 = str2;
            if (!MediaMetadataCompat.METADATA_KEYS_TYPE.containsKey(str3) || ((Integer) MediaMetadataCompat.METADATA_KEYS_TYPE.get(str3)).intValue() == 1) {
                this.mBundle.putCharSequence(str3, str4);
                return this;
            }
            Throwable th2 = th;
            new StringBuilder();
            new IllegalArgumentException(sb.append("The ").append(str3).append(" key cannot be used to put a String").toString());
            throw th2;
        }

        public Builder putLong(String str, long j) {
            Throwable th;
            StringBuilder sb;
            String str2 = str;
            long j2 = j;
            if (!MediaMetadataCompat.METADATA_KEYS_TYPE.containsKey(str2) || ((Integer) MediaMetadataCompat.METADATA_KEYS_TYPE.get(str2)).intValue() == 0) {
                this.mBundle.putLong(str2, j2);
                return this;
            }
            Throwable th2 = th;
            new StringBuilder();
            new IllegalArgumentException(sb.append("The ").append(str2).append(" key cannot be used to put a long").toString());
            throw th2;
        }

        public Builder putRating(String str, RatingCompat ratingCompat) {
            Throwable th;
            StringBuilder sb;
            String str2 = str;
            RatingCompat ratingCompat2 = ratingCompat;
            if (!MediaMetadataCompat.METADATA_KEYS_TYPE.containsKey(str2) || ((Integer) MediaMetadataCompat.METADATA_KEYS_TYPE.get(str2)).intValue() == 3) {
                this.mBundle.putParcelable(str2, ratingCompat2);
                return this;
            }
            Throwable th2 = th;
            new StringBuilder();
            new IllegalArgumentException(sb.append("The ").append(str2).append(" key cannot be used to put a Rating").toString());
            throw th2;
        }

        public Builder putBitmap(String str, Bitmap bitmap) {
            Throwable th;
            StringBuilder sb;
            String str2 = str;
            Bitmap bitmap2 = bitmap;
            if (!MediaMetadataCompat.METADATA_KEYS_TYPE.containsKey(str2) || ((Integer) MediaMetadataCompat.METADATA_KEYS_TYPE.get(str2)).intValue() == 2) {
                this.mBundle.putParcelable(str2, bitmap2);
                return this;
            }
            Throwable th2 = th;
            new StringBuilder();
            new IllegalArgumentException(sb.append("The ").append(str2).append(" key cannot be used to put a Bitmap").toString());
            throw th2;
        }

        public MediaMetadataCompat build() {
            MediaMetadataCompat mediaMetadataCompat;
            new MediaMetadataCompat(this.mBundle);
            return mediaMetadataCompat;
        }
    }
}
