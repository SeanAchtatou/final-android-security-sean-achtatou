package android.support.v4.media.session;

import android.app.PendingIntent;
import android.content.Context;
import android.media.AudioManager;
import android.media.RemoteControlClient;
import android.os.SystemClock;
import android.support.v4.media.session.MediaSessionCompatApi14;

public class MediaSessionCompatApi18 {
    private static final long ACTION_SEEK_TO = 256;

    public static Object createPlaybackPositionUpdateListener(MediaSessionCompatApi14.Callback callback) {
        Object obj;
        new OnPlaybackPositionUpdateListener(callback);
        return obj;
    }

    public static void registerMediaButtonEventReceiver(Context context, PendingIntent pendingIntent) {
        ((AudioManager) context.getSystemService("audio")).registerMediaButtonEventReceiver(pendingIntent);
    }

    public static void unregisterMediaButtonEventReceiver(Context context, PendingIntent pendingIntent) {
        ((AudioManager) context.getSystemService("audio")).unregisterMediaButtonEventReceiver(pendingIntent);
    }

    public static void setState(Object obj, int i, long j, float f, long j2) {
        Object obj2 = obj;
        int i2 = i;
        long j3 = j;
        float f2 = f;
        long j4 = j2;
        long elapsedRealtime = SystemClock.elapsedRealtime();
        if (i2 == 3 && j3 > 0) {
            long j5 = 0;
            if (j4 > 0) {
                j5 = elapsedRealtime - j4;
                if (f2 > 0.0f && f2 != 1.0f) {
                    j5 = (long) (((float) j5) * f2);
                }
            }
            j3 += j5;
        }
        ((RemoteControlClient) obj2).setPlaybackState(MediaSessionCompatApi14.getRccStateFromState(i2), j3, f2);
    }

    public static void setTransportControlFlags(Object obj, long j) {
        ((RemoteControlClient) obj).setTransportControlFlags(getRccTransportControlFlagsFromActions(j));
    }

    public static void setOnPlaybackPositionUpdateListener(Object obj, Object obj2) {
        ((RemoteControlClient) obj).setPlaybackPositionUpdateListener((RemoteControlClient.OnPlaybackPositionUpdateListener) obj2);
    }

    static int getRccTransportControlFlagsFromActions(long j) {
        long j2 = j;
        int rccTransportControlFlagsFromActions = MediaSessionCompatApi14.getRccTransportControlFlagsFromActions(j2);
        if ((j2 & 256) != 0) {
            rccTransportControlFlagsFromActions |= 256;
        }
        return rccTransportControlFlagsFromActions;
    }

    static class OnPlaybackPositionUpdateListener<T extends MediaSessionCompatApi14.Callback> implements RemoteControlClient.OnPlaybackPositionUpdateListener {
        protected final T mCallback;

        public OnPlaybackPositionUpdateListener(T t) {
            this.mCallback = t;
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public void onPlaybackPositionUpdate(long r7) {
            /*
                r6 = this;
                r0 = r6
                r1 = r7
                r3 = r0
                T r3 = r3.mCallback
                r4 = r1
                r3.onSeekTo(r4)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: android.support.v4.media.session.MediaSessionCompatApi18.OnPlaybackPositionUpdateListener.onPlaybackPositionUpdate(long):void");
        }
    }
}
