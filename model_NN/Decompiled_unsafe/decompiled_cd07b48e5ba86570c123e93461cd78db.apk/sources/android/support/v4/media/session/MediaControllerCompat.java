package android.support.v4.media.session;

import android.app.PendingIntent;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.os.ResultReceiver;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.RatingCompat;
import android.support.v4.media.session.IMediaControllerCallback;
import android.support.v4.media.session.IMediaSession;
import android.support.v4.media.session.MediaControllerCompatApi21;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import java.util.ArrayList;
import java.util.List;

public final class MediaControllerCompat {
    private static final String TAG = "MediaControllerCompat";
    private final MediaControllerImpl mImpl;
    private final MediaSessionCompat.Token mToken;

    interface MediaControllerImpl {
        void adjustVolume(int i, int i2);

        boolean dispatchMediaButtonEvent(KeyEvent keyEvent);

        Bundle getExtras();

        long getFlags();

        Object getMediaController();

        MediaMetadataCompat getMetadata();

        String getPackageName();

        PlaybackInfo getPlaybackInfo();

        PlaybackStateCompat getPlaybackState();

        List<MediaSessionCompat.QueueItem> getQueue();

        CharSequence getQueueTitle();

        int getRatingType();

        PendingIntent getSessionActivity();

        TransportControls getTransportControls();

        void registerCallback(Callback callback, Handler handler);

        void sendCommand(String str, Bundle bundle, ResultReceiver resultReceiver);

        void setVolumeTo(int i, int i2);

        void unregisterCallback(Callback callback);
    }

    public MediaControllerCompat(Context context, MediaSessionCompat mediaSessionCompat) {
        MediaControllerImpl mediaControllerImpl;
        MediaControllerImpl mediaControllerImpl2;
        Throwable th;
        Context context2 = context;
        MediaSessionCompat mediaSessionCompat2 = mediaSessionCompat;
        if (mediaSessionCompat2 == null) {
            Throwable th2 = th;
            new IllegalArgumentException("session must not be null");
            throw th2;
        }
        this.mToken = mediaSessionCompat2.getSessionToken();
        if (Build.VERSION.SDK_INT >= 21) {
            new MediaControllerImplApi21(context2, mediaSessionCompat2);
            this.mImpl = mediaControllerImpl2;
            return;
        }
        new MediaControllerImplBase(this.mToken);
        this.mImpl = mediaControllerImpl;
    }

    public MediaControllerCompat(Context context, MediaSessionCompat.Token token) throws RemoteException {
        MediaControllerImpl mediaControllerImpl;
        MediaControllerImpl mediaControllerImpl2;
        Throwable th;
        Context context2 = context;
        MediaSessionCompat.Token token2 = token;
        if (token2 == null) {
            Throwable th2 = th;
            new IllegalArgumentException("sessionToken must not be null");
            throw th2;
        }
        this.mToken = token2;
        if (Build.VERSION.SDK_INT >= 21) {
            new MediaControllerImplApi21(context2, token2);
            this.mImpl = mediaControllerImpl2;
            return;
        }
        new MediaControllerImplBase(this.mToken);
        this.mImpl = mediaControllerImpl;
    }

    public TransportControls getTransportControls() {
        return this.mImpl.getTransportControls();
    }

    public boolean dispatchMediaButtonEvent(KeyEvent keyEvent) {
        Throwable th;
        KeyEvent keyEvent2 = keyEvent;
        if (keyEvent2 != null) {
            return this.mImpl.dispatchMediaButtonEvent(keyEvent2);
        }
        Throwable th2 = th;
        new IllegalArgumentException("KeyEvent may not be null");
        throw th2;
    }

    public PlaybackStateCompat getPlaybackState() {
        return this.mImpl.getPlaybackState();
    }

    public MediaMetadataCompat getMetadata() {
        return this.mImpl.getMetadata();
    }

    public List<MediaSessionCompat.QueueItem> getQueue() {
        return this.mImpl.getQueue();
    }

    public CharSequence getQueueTitle() {
        return this.mImpl.getQueueTitle();
    }

    public Bundle getExtras() {
        return this.mImpl.getExtras();
    }

    public int getRatingType() {
        return this.mImpl.getRatingType();
    }

    public long getFlags() {
        return this.mImpl.getFlags();
    }

    public PlaybackInfo getPlaybackInfo() {
        return this.mImpl.getPlaybackInfo();
    }

    public PendingIntent getSessionActivity() {
        return this.mImpl.getSessionActivity();
    }

    public MediaSessionCompat.Token getSessionToken() {
        return this.mToken;
    }

    public void setVolumeTo(int i, int i2) {
        this.mImpl.setVolumeTo(i, i2);
    }

    public void adjustVolume(int i, int i2) {
        this.mImpl.adjustVolume(i, i2);
    }

    public void registerCallback(Callback callback) {
        registerCallback(callback, null);
    }

    public void registerCallback(Callback callback, Handler handler) {
        Handler handler2;
        Throwable th;
        Callback callback2 = callback;
        Handler handler3 = handler;
        if (callback2 == null) {
            Throwable th2 = th;
            new IllegalArgumentException("callback cannot be null");
            throw th2;
        }
        if (handler3 == null) {
            new Handler();
            handler3 = handler2;
        }
        this.mImpl.registerCallback(callback2, handler3);
    }

    public void unregisterCallback(Callback callback) {
        Throwable th;
        Callback callback2 = callback;
        if (callback2 == null) {
            Throwable th2 = th;
            new IllegalArgumentException("callback cannot be null");
            throw th2;
        }
        this.mImpl.unregisterCallback(callback2);
    }

    public void sendCommand(String str, Bundle bundle, ResultReceiver resultReceiver) {
        Throwable th;
        String str2 = str;
        Bundle bundle2 = bundle;
        ResultReceiver resultReceiver2 = resultReceiver;
        if (TextUtils.isEmpty(str2)) {
            Throwable th2 = th;
            new IllegalArgumentException("command cannot be null or empty");
            throw th2;
        }
        this.mImpl.sendCommand(str2, bundle2, resultReceiver2);
    }

    public String getPackageName() {
        return this.mImpl.getPackageName();
    }

    public Object getMediaController() {
        return this.mImpl.getMediaController();
    }

    public static abstract class Callback implements IBinder.DeathRecipient {
        /* access modifiers changed from: private */
        public final Object mCallbackObj;
        /* access modifiers changed from: private */
        public MessageHandler mHandler;
        /* access modifiers changed from: private */
        public boolean mRegistered = false;

        static /* synthetic */ boolean access$302(Callback callback, boolean z) {
            boolean z2 = z;
            boolean z3 = z2;
            callback.mRegistered = z3;
            return z2;
        }

        public Callback() {
            Object obj;
            MediaControllerCompatApi21.Callback callback;
            if (Build.VERSION.SDK_INT >= 21) {
                new StubApi21();
                this.mCallbackObj = MediaControllerCompatApi21.createCallback(callback);
                return;
            }
            new StubCompat();
            this.mCallbackObj = obj;
        }

        public void onSessionDestroyed() {
        }

        public void onSessionEvent(String str, Bundle bundle) {
        }

        public void onPlaybackStateChanged(PlaybackStateCompat playbackStateCompat) {
        }

        public void onMetadataChanged(MediaMetadataCompat mediaMetadataCompat) {
        }

        public void onQueueChanged(List<MediaSessionCompat.QueueItem> list) {
        }

        public void onQueueTitleChanged(CharSequence charSequence) {
        }

        public void onExtrasChanged(Bundle bundle) {
        }

        public void onAudioInfoChanged(PlaybackInfo playbackInfo) {
        }

        public void binderDied() {
            onSessionDestroyed();
        }

        /* access modifiers changed from: private */
        public void setHandler(Handler handler) {
            MessageHandler messageHandler;
            new MessageHandler(handler.getLooper());
            this.mHandler = messageHandler;
        }

        private class StubApi21 implements MediaControllerCompatApi21.Callback {
            private StubApi21() {
            }

            public void onSessionDestroyed() {
                Callback.this.onSessionDestroyed();
            }

            public void onSessionEvent(String str, Bundle bundle) {
                Callback.this.onSessionEvent(str, bundle);
            }

            public void onPlaybackStateChanged(Object obj) {
                Callback.this.onPlaybackStateChanged(PlaybackStateCompat.fromPlaybackState(obj));
            }

            public void onMetadataChanged(Object obj) {
                Callback.this.onMetadataChanged(MediaMetadataCompat.fromMediaMetadata(obj));
            }
        }

        private class StubCompat extends IMediaControllerCallback.Stub {
            private StubCompat() {
            }

            public void onEvent(String str, Bundle bundle) throws RemoteException {
                Callback.this.mHandler.post(1, str, bundle);
            }

            public void onSessionDestroyed() throws RemoteException {
                Callback.this.mHandler.post(8, null, null);
            }

            public void onPlaybackStateChanged(PlaybackStateCompat playbackStateCompat) throws RemoteException {
                Callback.this.mHandler.post(2, playbackStateCompat, null);
            }

            public void onMetadataChanged(MediaMetadataCompat mediaMetadataCompat) throws RemoteException {
                Callback.this.mHandler.post(3, mediaMetadataCompat, null);
            }

            public void onQueueChanged(List<MediaSessionCompat.QueueItem> list) throws RemoteException {
                Callback.this.mHandler.post(5, list, null);
            }

            public void onQueueTitleChanged(CharSequence charSequence) throws RemoteException {
                Callback.this.mHandler.post(6, charSequence, null);
            }

            public void onExtrasChanged(Bundle bundle) throws RemoteException {
                Callback.this.mHandler.post(7, bundle, null);
            }

            public void onVolumeInfoChanged(ParcelableVolumeInfo parcelableVolumeInfo) throws RemoteException {
                Object obj;
                ParcelableVolumeInfo parcelableVolumeInfo2 = parcelableVolumeInfo;
                Object obj2 = null;
                if (parcelableVolumeInfo2 != null) {
                    new PlaybackInfo(parcelableVolumeInfo2.volumeType, parcelableVolumeInfo2.audioStream, parcelableVolumeInfo2.controlType, parcelableVolumeInfo2.maxVolume, parcelableVolumeInfo2.currentVolume);
                    obj2 = obj;
                }
                Callback.this.mHandler.post(4, obj2, null);
            }
        }

        private class MessageHandler extends Handler {
            private static final int MSG_DESTROYED = 8;
            private static final int MSG_EVENT = 1;
            private static final int MSG_UPDATE_EXTRAS = 7;
            private static final int MSG_UPDATE_METADATA = 3;
            private static final int MSG_UPDATE_PLAYBACK_STATE = 2;
            private static final int MSG_UPDATE_QUEUE = 5;
            private static final int MSG_UPDATE_QUEUE_TITLE = 6;
            private static final int MSG_UPDATE_VOLUME = 4;

            public MessageHandler(Looper looper) {
                super(looper);
            }

            public void handleMessage(Message message) {
                Message message2 = message;
                if (Callback.this.mRegistered) {
                    switch (message2.what) {
                        case 1:
                            Callback.this.onSessionEvent((String) message2.obj, message2.getData());
                            return;
                        case 2:
                            Callback.this.onPlaybackStateChanged((PlaybackStateCompat) message2.obj);
                            return;
                        case 3:
                            Callback.this.onMetadataChanged((MediaMetadataCompat) message2.obj);
                            return;
                        case 4:
                            Callback.this.onAudioInfoChanged((PlaybackInfo) message2.obj);
                            return;
                        case 5:
                            Callback.this.onQueueChanged((List) message2.obj);
                            return;
                        case 6:
                            Callback.this.onQueueTitleChanged((CharSequence) message2.obj);
                            return;
                        case 7:
                            Callback.this.onExtrasChanged((Bundle) message2.obj);
                            return;
                        case 8:
                            Callback.this.onSessionDestroyed();
                            return;
                        default:
                            return;
                    }
                }
            }

            public void post(int i, Object obj, Bundle bundle) {
                obtainMessage(i, obj).sendToTarget();
            }
        }
    }

    public static abstract class TransportControls {
        public abstract void fastForward();

        public abstract void pause();

        public abstract void play();

        public abstract void playFromMediaId(String str, Bundle bundle);

        public abstract void playFromSearch(String str, Bundle bundle);

        public abstract void rewind();

        public abstract void seekTo(long j);

        public abstract void sendCustomAction(PlaybackStateCompat.CustomAction customAction, Bundle bundle);

        public abstract void sendCustomAction(String str, Bundle bundle);

        public abstract void setRating(RatingCompat ratingCompat);

        public abstract void skipToNext();

        public abstract void skipToPrevious();

        public abstract void skipToQueueItem(long j);

        public abstract void stop();

        TransportControls() {
        }
    }

    public static final class PlaybackInfo {
        public static final int PLAYBACK_TYPE_LOCAL = 1;
        public static final int PLAYBACK_TYPE_REMOTE = 2;
        private final int mAudioStream;
        private final int mCurrentVolume;
        private final int mMaxVolume;
        private final int mPlaybackType;
        private final int mVolumeControl;

        PlaybackInfo(int i, int i2, int i3, int i4, int i5) {
            this.mPlaybackType = i;
            this.mAudioStream = i2;
            this.mVolumeControl = i3;
            this.mMaxVolume = i4;
            this.mCurrentVolume = i5;
        }

        public int getPlaybackType() {
            return this.mPlaybackType;
        }

        public int getAudioStream() {
            return this.mAudioStream;
        }

        public int getVolumeControl() {
            return this.mVolumeControl;
        }

        public int getMaxVolume() {
            return this.mMaxVolume;
        }

        public int getCurrentVolume() {
            return this.mCurrentVolume;
        }
    }

    static class MediaControllerImplBase implements MediaControllerImpl {
        private IMediaSession mBinder;
        private MediaSessionCompat.Token mToken;
        private TransportControls mTransportControls;

        public MediaControllerImplBase(MediaSessionCompat.Token token) {
            MediaSessionCompat.Token token2 = token;
            this.mToken = token2;
            this.mBinder = IMediaSession.Stub.asInterface((IBinder) token2.getToken());
        }

        public void registerCallback(Callback callback, Handler handler) {
            StringBuilder sb;
            Throwable th;
            Callback callback2 = callback;
            Handler handler2 = handler;
            if (callback2 == null) {
                Throwable th2 = th;
                new IllegalArgumentException("callback may not be null.");
                throw th2;
            }
            try {
                this.mBinder.asBinder().linkToDeath(callback2, 0);
                this.mBinder.registerCallbackListener((IMediaControllerCallback) callback2.mCallbackObj);
                callback2.setHandler(handler2);
                boolean access$302 = Callback.access$302(callback2, true);
            } catch (RemoteException e) {
                new StringBuilder();
                int e2 = Log.e(MediaControllerCompat.TAG, sb.append("Dead object in registerCallback. ").append(e).toString());
                callback2.onSessionDestroyed();
            }
        }

        public void unregisterCallback(Callback callback) {
            StringBuilder sb;
            Throwable th;
            Callback callback2 = callback;
            if (callback2 == null) {
                Throwable th2 = th;
                new IllegalArgumentException("callback may not be null.");
                throw th2;
            }
            try {
                this.mBinder.unregisterCallbackListener((IMediaControllerCallback) callback2.mCallbackObj);
                boolean unlinkToDeath = this.mBinder.asBinder().unlinkToDeath(callback2, 0);
                boolean access$302 = Callback.access$302(callback2, false);
            } catch (RemoteException e) {
                new StringBuilder();
                int e2 = Log.e(MediaControllerCompat.TAG, sb.append("Dead object in unregisterCallback. ").append(e).toString());
            }
        }

        public boolean dispatchMediaButtonEvent(KeyEvent keyEvent) {
            StringBuilder sb;
            Throwable th;
            KeyEvent keyEvent2 = keyEvent;
            if (keyEvent2 == null) {
                Throwable th2 = th;
                new IllegalArgumentException("event may not be null.");
                throw th2;
            }
            try {
                boolean sendMediaButton = this.mBinder.sendMediaButton(keyEvent2);
            } catch (RemoteException e) {
                new StringBuilder();
                int e2 = Log.e(MediaControllerCompat.TAG, sb.append("Dead object in dispatchMediaButtonEvent. ").append(e).toString());
            }
            return false;
        }

        public TransportControls getTransportControls() {
            TransportControls transportControls;
            if (this.mTransportControls == null) {
                new TransportControlsBase(this.mBinder);
                this.mTransportControls = transportControls;
            }
            return this.mTransportControls;
        }

        public PlaybackStateCompat getPlaybackState() {
            StringBuilder sb;
            try {
                return this.mBinder.getPlaybackState();
            } catch (RemoteException e) {
                new StringBuilder();
                int e2 = Log.e(MediaControllerCompat.TAG, sb.append("Dead object in getPlaybackState. ").append(e).toString());
                return null;
            }
        }

        public MediaMetadataCompat getMetadata() {
            StringBuilder sb;
            try {
                return this.mBinder.getMetadata();
            } catch (RemoteException e) {
                new StringBuilder();
                int e2 = Log.e(MediaControllerCompat.TAG, sb.append("Dead object in getMetadata. ").append(e).toString());
                return null;
            }
        }

        public List<MediaSessionCompat.QueueItem> getQueue() {
            StringBuilder sb;
            try {
                return this.mBinder.getQueue();
            } catch (RemoteException e) {
                new StringBuilder();
                int e2 = Log.e(MediaControllerCompat.TAG, sb.append("Dead object in getQueue. ").append(e).toString());
                return null;
            }
        }

        public CharSequence getQueueTitle() {
            StringBuilder sb;
            try {
                return this.mBinder.getQueueTitle();
            } catch (RemoteException e) {
                new StringBuilder();
                int e2 = Log.e(MediaControllerCompat.TAG, sb.append("Dead object in getQueueTitle. ").append(e).toString());
                return null;
            }
        }

        public Bundle getExtras() {
            StringBuilder sb;
            try {
                return this.mBinder.getExtras();
            } catch (RemoteException e) {
                new StringBuilder();
                int e2 = Log.e(MediaControllerCompat.TAG, sb.append("Dead object in getExtras. ").append(e).toString());
                return null;
            }
        }

        public int getRatingType() {
            StringBuilder sb;
            try {
                return this.mBinder.getRatingType();
            } catch (RemoteException e) {
                new StringBuilder();
                int e2 = Log.e(MediaControllerCompat.TAG, sb.append("Dead object in getRatingType. ").append(e).toString());
                return 0;
            }
        }

        public long getFlags() {
            StringBuilder sb;
            try {
                return this.mBinder.getFlags();
            } catch (RemoteException e) {
                new StringBuilder();
                int e2 = Log.e(MediaControllerCompat.TAG, sb.append("Dead object in getFlags. ").append(e).toString());
                return 0;
            }
        }

        public PlaybackInfo getPlaybackInfo() {
            StringBuilder sb;
            PlaybackInfo playbackInfo;
            try {
                ParcelableVolumeInfo volumeAttributes = this.mBinder.getVolumeAttributes();
                PlaybackInfo playbackInfo2 = playbackInfo;
                new PlaybackInfo(volumeAttributes.volumeType, volumeAttributes.audioStream, volumeAttributes.controlType, volumeAttributes.maxVolume, volumeAttributes.currentVolume);
                return playbackInfo2;
            } catch (RemoteException e) {
                new StringBuilder();
                int e2 = Log.e(MediaControllerCompat.TAG, sb.append("Dead object in getPlaybackInfo. ").append(e).toString());
                return null;
            }
        }

        public PendingIntent getSessionActivity() {
            StringBuilder sb;
            try {
                return this.mBinder.getLaunchPendingIntent();
            } catch (RemoteException e) {
                new StringBuilder();
                int e2 = Log.e(MediaControllerCompat.TAG, sb.append("Dead object in getSessionActivity. ").append(e).toString());
                return null;
            }
        }

        public void setVolumeTo(int i, int i2) {
            StringBuilder sb;
            try {
                this.mBinder.setVolumeTo(i, i2, null);
            } catch (RemoteException e) {
                new StringBuilder();
                int e2 = Log.e(MediaControllerCompat.TAG, sb.append("Dead object in setVolumeTo. ").append(e).toString());
            }
        }

        public void adjustVolume(int i, int i2) {
            StringBuilder sb;
            try {
                this.mBinder.adjustVolume(i, i2, null);
            } catch (RemoteException e) {
                new StringBuilder();
                int e2 = Log.e(MediaControllerCompat.TAG, sb.append("Dead object in adjustVolume. ").append(e).toString());
            }
        }

        public void sendCommand(String str, Bundle bundle, ResultReceiver resultReceiver) {
            StringBuilder sb;
            MediaSessionCompat.ResultReceiverWrapper resultReceiverWrapper;
            String str2 = str;
            Bundle bundle2 = bundle;
            try {
                new MediaSessionCompat.ResultReceiverWrapper(resultReceiver);
                this.mBinder.sendCommand(str2, bundle2, resultReceiverWrapper);
            } catch (RemoteException e) {
                new StringBuilder();
                int e2 = Log.e(MediaControllerCompat.TAG, sb.append("Dead object in sendCommand. ").append(e).toString());
            }
        }

        public String getPackageName() {
            StringBuilder sb;
            try {
                return this.mBinder.getPackageName();
            } catch (RemoteException e) {
                new StringBuilder();
                int e2 = Log.e(MediaControllerCompat.TAG, sb.append("Dead object in getPackageName. ").append(e).toString());
                return null;
            }
        }

        public Object getMediaController() {
            return null;
        }
    }

    static class TransportControlsBase extends TransportControls {
        private IMediaSession mBinder;

        public TransportControlsBase(IMediaSession iMediaSession) {
            this.mBinder = iMediaSession;
        }

        public void play() {
            StringBuilder sb;
            try {
                this.mBinder.play();
            } catch (RemoteException e) {
                new StringBuilder();
                int e2 = Log.e(MediaControllerCompat.TAG, sb.append("Dead object in play. ").append(e).toString());
            }
        }

        public void playFromMediaId(String str, Bundle bundle) {
            StringBuilder sb;
            try {
                this.mBinder.playFromMediaId(str, bundle);
            } catch (RemoteException e) {
                new StringBuilder();
                int e2 = Log.e(MediaControllerCompat.TAG, sb.append("Dead object in playFromMediaId. ").append(e).toString());
            }
        }

        public void playFromSearch(String str, Bundle bundle) {
            StringBuilder sb;
            try {
                this.mBinder.playFromSearch(str, bundle);
            } catch (RemoteException e) {
                new StringBuilder();
                int e2 = Log.e(MediaControllerCompat.TAG, sb.append("Dead object in playFromSearch. ").append(e).toString());
            }
        }

        public void skipToQueueItem(long j) {
            StringBuilder sb;
            try {
                this.mBinder.skipToQueueItem(j);
            } catch (RemoteException e) {
                new StringBuilder();
                int e2 = Log.e(MediaControllerCompat.TAG, sb.append("Dead object in skipToQueueItem. ").append(e).toString());
            }
        }

        public void pause() {
            StringBuilder sb;
            try {
                this.mBinder.pause();
            } catch (RemoteException e) {
                new StringBuilder();
                int e2 = Log.e(MediaControllerCompat.TAG, sb.append("Dead object in pause. ").append(e).toString());
            }
        }

        public void stop() {
            StringBuilder sb;
            try {
                this.mBinder.stop();
            } catch (RemoteException e) {
                new StringBuilder();
                int e2 = Log.e(MediaControllerCompat.TAG, sb.append("Dead object in stop. ").append(e).toString());
            }
        }

        public void seekTo(long j) {
            StringBuilder sb;
            try {
                this.mBinder.seekTo(j);
            } catch (RemoteException e) {
                new StringBuilder();
                int e2 = Log.e(MediaControllerCompat.TAG, sb.append("Dead object in seekTo. ").append(e).toString());
            }
        }

        public void fastForward() {
            StringBuilder sb;
            try {
                this.mBinder.fastForward();
            } catch (RemoteException e) {
                new StringBuilder();
                int e2 = Log.e(MediaControllerCompat.TAG, sb.append("Dead object in fastForward. ").append(e).toString());
            }
        }

        public void skipToNext() {
            StringBuilder sb;
            try {
                this.mBinder.next();
            } catch (RemoteException e) {
                new StringBuilder();
                int e2 = Log.e(MediaControllerCompat.TAG, sb.append("Dead object in skipToNext. ").append(e).toString());
            }
        }

        public void rewind() {
            StringBuilder sb;
            try {
                this.mBinder.rewind();
            } catch (RemoteException e) {
                new StringBuilder();
                int e2 = Log.e(MediaControllerCompat.TAG, sb.append("Dead object in rewind. ").append(e).toString());
            }
        }

        public void skipToPrevious() {
            StringBuilder sb;
            try {
                this.mBinder.previous();
            } catch (RemoteException e) {
                new StringBuilder();
                int e2 = Log.e(MediaControllerCompat.TAG, sb.append("Dead object in skipToPrevious. ").append(e).toString());
            }
        }

        public void setRating(RatingCompat ratingCompat) {
            StringBuilder sb;
            try {
                this.mBinder.rate(ratingCompat);
            } catch (RemoteException e) {
                new StringBuilder();
                int e2 = Log.e(MediaControllerCompat.TAG, sb.append("Dead object in setRating. ").append(e).toString());
            }
        }

        public void sendCustomAction(PlaybackStateCompat.CustomAction customAction, Bundle bundle) {
            sendCustomAction(customAction.getAction(), bundle);
        }

        public void sendCustomAction(String str, Bundle bundle) {
            StringBuilder sb;
            try {
                this.mBinder.sendCustomAction(str, bundle);
            } catch (RemoteException e) {
                new StringBuilder();
                int e2 = Log.e(MediaControllerCompat.TAG, sb.append("Dead object in sendCustomAction. ").append(e).toString());
            }
        }
    }

    static class MediaControllerImplApi21 implements MediaControllerImpl {
        private final Object mControllerObj;

        public MediaControllerImplApi21(Context context, MediaSessionCompat mediaSessionCompat) {
            this.mControllerObj = MediaControllerCompatApi21.fromToken(context, mediaSessionCompat.getSessionToken().getToken());
        }

        public MediaControllerImplApi21(Context context, MediaSessionCompat.Token token) throws RemoteException {
            Throwable th;
            this.mControllerObj = MediaControllerCompatApi21.fromToken(context, token.getToken());
            if (this.mControllerObj == null) {
                Throwable th2 = th;
                new RemoteException();
                throw th2;
            }
        }

        public void registerCallback(Callback callback, Handler handler) {
            MediaControllerCompatApi21.registerCallback(this.mControllerObj, callback.mCallbackObj, handler);
        }

        public void unregisterCallback(Callback callback) {
            MediaControllerCompatApi21.unregisterCallback(this.mControllerObj, callback.mCallbackObj);
        }

        public boolean dispatchMediaButtonEvent(KeyEvent keyEvent) {
            return MediaControllerCompatApi21.dispatchMediaButtonEvent(this.mControllerObj, keyEvent);
        }

        public TransportControls getTransportControls() {
            TransportControls transportControls;
            TransportControls transportControls2;
            Object transportControls3 = MediaControllerCompatApi21.getTransportControls(this.mControllerObj);
            if (transportControls3 != null) {
                transportControls = transportControls2;
                new TransportControlsApi21(transportControls3);
            } else {
                transportControls = null;
            }
            return transportControls;
        }

        public PlaybackStateCompat getPlaybackState() {
            Object playbackState = MediaControllerCompatApi21.getPlaybackState(this.mControllerObj);
            return playbackState != null ? PlaybackStateCompat.fromPlaybackState(playbackState) : null;
        }

        public MediaMetadataCompat getMetadata() {
            Object metadata = MediaControllerCompatApi21.getMetadata(this.mControllerObj);
            return metadata != null ? MediaMetadataCompat.fromMediaMetadata(metadata) : null;
        }

        public List<MediaSessionCompat.QueueItem> getQueue() {
            List<MediaSessionCompat.QueueItem> list;
            List<Object> queue = MediaControllerCompatApi21.getQueue(this.mControllerObj);
            if (queue == null) {
                return null;
            }
            new ArrayList();
            List<MediaSessionCompat.QueueItem> list2 = list;
            for (Object obtain : queue) {
                boolean add = list2.add(MediaSessionCompat.QueueItem.obtain(obtain));
            }
            return list2;
        }

        public CharSequence getQueueTitle() {
            return MediaControllerCompatApi21.getQueueTitle(this.mControllerObj);
        }

        public Bundle getExtras() {
            return MediaControllerCompatApi21.getExtras(this.mControllerObj);
        }

        public int getRatingType() {
            return MediaControllerCompatApi21.getRatingType(this.mControllerObj);
        }

        public long getFlags() {
            return MediaControllerCompatApi21.getFlags(this.mControllerObj);
        }

        public PlaybackInfo getPlaybackInfo() {
            PlaybackInfo playbackInfo;
            PlaybackInfo playbackInfo2;
            Object playbackInfo3 = MediaControllerCompatApi21.getPlaybackInfo(this.mControllerObj);
            if (playbackInfo3 != null) {
                playbackInfo = playbackInfo2;
                new PlaybackInfo(MediaControllerCompatApi21.PlaybackInfo.getPlaybackType(playbackInfo3), MediaControllerCompatApi21.PlaybackInfo.getLegacyAudioStream(playbackInfo3), MediaControllerCompatApi21.PlaybackInfo.getVolumeControl(playbackInfo3), MediaControllerCompatApi21.PlaybackInfo.getMaxVolume(playbackInfo3), MediaControllerCompatApi21.PlaybackInfo.getCurrentVolume(playbackInfo3));
            } else {
                playbackInfo = null;
            }
            return playbackInfo;
        }

        public PendingIntent getSessionActivity() {
            return MediaControllerCompatApi21.getSessionActivity(this.mControllerObj);
        }

        public void setVolumeTo(int i, int i2) {
            MediaControllerCompatApi21.setVolumeTo(this.mControllerObj, i, i2);
        }

        public void adjustVolume(int i, int i2) {
            MediaControllerCompatApi21.adjustVolume(this.mControllerObj, i, i2);
        }

        public void sendCommand(String str, Bundle bundle, ResultReceiver resultReceiver) {
            MediaControllerCompatApi21.sendCommand(this.mControllerObj, str, bundle, resultReceiver);
        }

        public String getPackageName() {
            return MediaControllerCompatApi21.getPackageName(this.mControllerObj);
        }

        public Object getMediaController() {
            return this.mControllerObj;
        }
    }

    static class TransportControlsApi21 extends TransportControls {
        private final Object mControlsObj;

        public TransportControlsApi21(Object obj) {
            this.mControlsObj = obj;
        }

        public void play() {
            MediaControllerCompatApi21.TransportControls.play(this.mControlsObj);
        }

        public void pause() {
            MediaControllerCompatApi21.TransportControls.pause(this.mControlsObj);
        }

        public void stop() {
            MediaControllerCompatApi21.TransportControls.stop(this.mControlsObj);
        }

        public void seekTo(long j) {
            MediaControllerCompatApi21.TransportControls.seekTo(this.mControlsObj, j);
        }

        public void fastForward() {
            MediaControllerCompatApi21.TransportControls.fastForward(this.mControlsObj);
        }

        public void rewind() {
            MediaControllerCompatApi21.TransportControls.rewind(this.mControlsObj);
        }

        public void skipToNext() {
            MediaControllerCompatApi21.TransportControls.skipToNext(this.mControlsObj);
        }

        public void skipToPrevious() {
            MediaControllerCompatApi21.TransportControls.skipToPrevious(this.mControlsObj);
        }

        public void setRating(RatingCompat ratingCompat) {
            RatingCompat ratingCompat2 = ratingCompat;
            MediaControllerCompatApi21.TransportControls.setRating(this.mControlsObj, ratingCompat2 != null ? ratingCompat2.getRating() : null);
        }

        public void playFromMediaId(String str, Bundle bundle) {
            MediaControllerCompatApi21.TransportControls.playFromMediaId(this.mControlsObj, str, bundle);
        }

        public void playFromSearch(String str, Bundle bundle) {
            MediaControllerCompatApi21.TransportControls.playFromSearch(this.mControlsObj, str, bundle);
        }

        public void skipToQueueItem(long j) {
            MediaControllerCompatApi21.TransportControls.skipToQueueItem(this.mControlsObj, j);
        }

        public void sendCustomAction(PlaybackStateCompat.CustomAction customAction, Bundle bundle) {
            MediaControllerCompatApi21.TransportControls.sendCustomAction(this.mControlsObj, customAction.getAction(), bundle);
        }

        public void sendCustomAction(String str, Bundle bundle) {
            MediaControllerCompatApi21.TransportControls.sendCustomAction(this.mControlsObj, str, bundle);
        }
    }
}
