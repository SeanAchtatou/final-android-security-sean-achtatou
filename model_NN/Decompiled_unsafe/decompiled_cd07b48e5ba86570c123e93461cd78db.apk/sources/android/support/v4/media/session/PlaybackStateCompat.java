package android.support.v4.media.session;

import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;
import android.text.TextUtils;

public final class PlaybackStateCompat implements Parcelable {
    public static final long ACTION_FAST_FORWARD = 64;
    public static final long ACTION_PAUSE = 2;
    public static final long ACTION_PLAY = 4;
    public static final long ACTION_PLAY_FROM_MEDIA_ID = 1024;
    public static final long ACTION_PLAY_FROM_SEARCH = 2048;
    public static final long ACTION_PLAY_PAUSE = 512;
    public static final long ACTION_REWIND = 8;
    public static final long ACTION_SEEK_TO = 256;
    public static final long ACTION_SET_RATING = 128;
    public static final long ACTION_SKIP_TO_NEXT = 32;
    public static final long ACTION_SKIP_TO_PREVIOUS = 16;
    public static final long ACTION_SKIP_TO_QUEUE_ITEM = 4096;
    public static final long ACTION_STOP = 1;
    public static final Parcelable.Creator<PlaybackStateCompat> CREATOR;
    public static final long PLAYBACK_POSITION_UNKNOWN = -1;
    public static final int STATE_BUFFERING = 6;
    public static final int STATE_CONNECTING = 8;
    public static final int STATE_ERROR = 7;
    public static final int STATE_FAST_FORWARDING = 4;
    public static final int STATE_NONE = 0;
    public static final int STATE_PAUSED = 2;
    public static final int STATE_PLAYING = 3;
    public static final int STATE_REWINDING = 5;
    public static final int STATE_SKIPPING_TO_NEXT = 10;
    public static final int STATE_SKIPPING_TO_PREVIOUS = 9;
    public static final int STATE_STOPPED = 1;
    /* access modifiers changed from: private */
    public final long mActions;
    /* access modifiers changed from: private */
    public final long mBufferedPosition;
    /* access modifiers changed from: private */
    public final CharSequence mErrorMessage;
    /* access modifiers changed from: private */
    public final long mPosition;
    /* access modifiers changed from: private */
    public final float mSpeed;
    /* access modifiers changed from: private */
    public final int mState;
    private Object mStateObj;
    /* access modifiers changed from: private */
    public final long mUpdateTime;

    private PlaybackStateCompat(int i, long j, long j2, float f, long j3, CharSequence charSequence, long j4) {
        this.mState = i;
        this.mPosition = j;
        this.mBufferedPosition = j2;
        this.mSpeed = f;
        this.mActions = j3;
        this.mErrorMessage = charSequence;
        this.mUpdateTime = j4;
    }

    private PlaybackStateCompat(Parcel parcel) {
        Parcel parcel2 = parcel;
        this.mState = parcel2.readInt();
        this.mPosition = parcel2.readLong();
        this.mSpeed = parcel2.readFloat();
        this.mUpdateTime = parcel2.readLong();
        this.mBufferedPosition = parcel2.readLong();
        this.mActions = parcel2.readLong();
        this.mErrorMessage = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel2);
    }

    public String toString() {
        StringBuilder sb;
        new StringBuilder("PlaybackState {");
        StringBuilder sb2 = sb;
        StringBuilder append = sb2.append("state=").append(this.mState);
        StringBuilder append2 = sb2.append(", position=").append(this.mPosition);
        StringBuilder append3 = sb2.append(", buffered position=").append(this.mBufferedPosition);
        StringBuilder append4 = sb2.append(", speed=").append(this.mSpeed);
        StringBuilder append5 = sb2.append(", updated=").append(this.mUpdateTime);
        StringBuilder append6 = sb2.append(", actions=").append(this.mActions);
        StringBuilder append7 = sb2.append(", error=").append(this.mErrorMessage);
        StringBuilder append8 = sb2.append("}");
        return sb2.toString();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        Parcel parcel2 = parcel;
        parcel2.writeInt(this.mState);
        parcel2.writeLong(this.mPosition);
        parcel2.writeFloat(this.mSpeed);
        parcel2.writeLong(this.mUpdateTime);
        parcel2.writeLong(this.mBufferedPosition);
        parcel2.writeLong(this.mActions);
        TextUtils.writeToParcel(this.mErrorMessage, parcel2, i);
    }

    public int getState() {
        return this.mState;
    }

    public long getPosition() {
        return this.mPosition;
    }

    public long getBufferedPosition() {
        return this.mBufferedPosition;
    }

    public float getPlaybackSpeed() {
        return this.mSpeed;
    }

    public long getActions() {
        return this.mActions;
    }

    public CharSequence getErrorMessage() {
        return this.mErrorMessage;
    }

    public long getLastPositionUpdateTime() {
        return this.mUpdateTime;
    }

    public static PlaybackStateCompat fromPlaybackState(Object obj) {
        PlaybackStateCompat playbackStateCompat;
        Object obj2 = obj;
        if (obj2 == null || Build.VERSION.SDK_INT < 21) {
            return null;
        }
        new PlaybackStateCompat(PlaybackStateCompatApi21.getState(obj2), PlaybackStateCompatApi21.getPosition(obj2), PlaybackStateCompatApi21.getBufferedPosition(obj2), PlaybackStateCompatApi21.getPlaybackSpeed(obj2), PlaybackStateCompatApi21.getActions(obj2), PlaybackStateCompatApi21.getErrorMessage(obj2), PlaybackStateCompatApi21.getLastPositionUpdateTime(obj2));
        PlaybackStateCompat playbackStateCompat2 = playbackStateCompat;
        playbackStateCompat2.mStateObj = obj2;
        return playbackStateCompat2;
    }

    public Object getPlaybackState() {
        if (this.mStateObj != null || Build.VERSION.SDK_INT < 21) {
            return this.mStateObj;
        }
        this.mStateObj = PlaybackStateCompatApi21.newInstance(this.mState, this.mPosition, this.mBufferedPosition, this.mSpeed, this.mActions, this.mErrorMessage, this.mUpdateTime);
        return this.mStateObj;
    }

    static {
        Parcelable.Creator<PlaybackStateCompat> creator;
        new Parcelable.Creator<PlaybackStateCompat>() {
            public PlaybackStateCompat createFromParcel(Parcel parcel) {
                PlaybackStateCompat playbackStateCompat;
                new PlaybackStateCompat(parcel);
                return playbackStateCompat;
            }

            public PlaybackStateCompat[] newArray(int i) {
                return new PlaybackStateCompat[i];
            }
        };
        CREATOR = creator;
    }

    public static final class CustomAction implements Parcelable {
        public static final Parcelable.Creator<CustomAction> CREATOR;
        private final String mAction;
        private final Bundle mExtras;
        private final int mIcon;
        private final CharSequence mName;

        private CustomAction(String str, CharSequence charSequence, int i, Bundle bundle) {
            this.mAction = str;
            this.mName = charSequence;
            this.mIcon = i;
            this.mExtras = bundle;
        }

        private CustomAction(Parcel parcel) {
            Parcel parcel2 = parcel;
            this.mAction = parcel2.readString();
            this.mName = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel2);
            this.mIcon = parcel2.readInt();
            this.mExtras = parcel2.readBundle();
        }

        public void writeToParcel(Parcel parcel, int i) {
            Parcel parcel2 = parcel;
            parcel2.writeString(this.mAction);
            TextUtils.writeToParcel(this.mName, parcel2, i);
            parcel2.writeInt(this.mIcon);
            parcel2.writeBundle(this.mExtras);
        }

        public int describeContents() {
            return 0;
        }

        static {
            Parcelable.Creator<CustomAction> creator;
            new Parcelable.Creator<CustomAction>() {
                public CustomAction createFromParcel(Parcel parcel) {
                    CustomAction customAction;
                    new CustomAction(parcel);
                    return customAction;
                }

                public CustomAction[] newArray(int i) {
                    return new CustomAction[i];
                }
            };
            CREATOR = creator;
        }

        public String getAction() {
            return this.mAction;
        }

        public CharSequence getName() {
            return this.mName;
        }

        public int getIcon() {
            return this.mIcon;
        }

        public Bundle getExtras() {
            return this.mExtras;
        }

        public String toString() {
            StringBuilder sb;
            new StringBuilder();
            return sb.append("Action:mName='").append((Object) this.mName).append(", mIcon=").append(this.mIcon).append(", mExtras=").append(this.mExtras).toString();
        }

        public static final class Builder {
            private final String mAction;
            private Bundle mExtras;
            private final int mIcon;
            private final CharSequence mName;

            public Builder(String str, CharSequence charSequence, int i) {
                Throwable th;
                Throwable th2;
                Throwable th3;
                String str2 = str;
                CharSequence charSequence2 = charSequence;
                int i2 = i;
                if (TextUtils.isEmpty(str2)) {
                    Throwable th4 = th3;
                    new IllegalArgumentException("You must specify an action to build a CustomAction.");
                    throw th4;
                } else if (TextUtils.isEmpty(charSequence2)) {
                    Throwable th5 = th2;
                    new IllegalArgumentException("You must specify a name to build a CustomAction.");
                    throw th5;
                } else if (i2 == 0) {
                    Throwable th6 = th;
                    new IllegalArgumentException("You must specify an icon resource id to build a CustomAction.");
                    throw th6;
                } else {
                    this.mAction = str2;
                    this.mName = charSequence2;
                    this.mIcon = i2;
                }
            }

            public Builder setExtras(Bundle bundle) {
                this.mExtras = bundle;
                return this;
            }

            public CustomAction build() {
                CustomAction customAction;
                new CustomAction(this.mAction, this.mName, this.mIcon, this.mExtras);
                return customAction;
            }
        }
    }

    public static final class Builder {
        private long mActions;
        private long mBufferedPosition;
        private CharSequence mErrorMessage;
        private long mPosition;
        private float mRate;
        private int mState;
        private long mUpdateTime;

        public Builder() {
        }

        public Builder(PlaybackStateCompat playbackStateCompat) {
            PlaybackStateCompat playbackStateCompat2 = playbackStateCompat;
            this.mState = playbackStateCompat2.mState;
            this.mPosition = playbackStateCompat2.mPosition;
            this.mRate = playbackStateCompat2.mSpeed;
            this.mUpdateTime = playbackStateCompat2.mUpdateTime;
            this.mBufferedPosition = playbackStateCompat2.mBufferedPosition;
            this.mActions = playbackStateCompat2.mActions;
            this.mErrorMessage = playbackStateCompat2.mErrorMessage;
        }

        public Builder setState(int i, long j, float f) {
            return setState(i, j, f, SystemClock.elapsedRealtime());
        }

        public Builder setState(int i, long j, float f, long j2) {
            this.mState = i;
            this.mPosition = j;
            this.mUpdateTime = j2;
            this.mRate = f;
            return this;
        }

        public Builder setBufferedPosition(long j) {
            this.mBufferedPosition = j;
            return this;
        }

        public Builder setActions(long j) {
            this.mActions = j;
            return this;
        }

        public Builder setErrorMessage(CharSequence charSequence) {
            this.mErrorMessage = charSequence;
            return this;
        }

        public PlaybackStateCompat build() {
            PlaybackStateCompat playbackStateCompat;
            new PlaybackStateCompat(this.mState, this.mPosition, this.mBufferedPosition, this.mRate, this.mActions, this.mErrorMessage, this.mUpdateTime);
            return playbackStateCompat;
        }
    }
}
