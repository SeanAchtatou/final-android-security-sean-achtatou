package android.support.v4.media.session;

import android.media.session.PlaybackState;

class PlaybackStateCompatApi21 {
    PlaybackStateCompatApi21() {
    }

    public static int getState(Object obj) {
        return ((PlaybackState) obj).getState();
    }

    public static long getPosition(Object obj) {
        return ((PlaybackState) obj).getPosition();
    }

    public static long getBufferedPosition(Object obj) {
        return ((PlaybackState) obj).getBufferedPosition();
    }

    public static float getPlaybackSpeed(Object obj) {
        return ((PlaybackState) obj).getPlaybackSpeed();
    }

    public static long getActions(Object obj) {
        return ((PlaybackState) obj).getActions();
    }

    public static CharSequence getErrorMessage(Object obj) {
        return ((PlaybackState) obj).getErrorMessage();
    }

    public static long getLastPositionUpdateTime(Object obj) {
        return ((PlaybackState) obj).getLastPositionUpdateTime();
    }

    public static Object newInstance(int i, long j, long j2, float f, long j3, CharSequence charSequence, long j4) {
        PlaybackState.Builder builder;
        new PlaybackState.Builder();
        PlaybackState.Builder builder2 = builder;
        PlaybackState.Builder state = builder2.setState(i, j, f, j4);
        PlaybackState.Builder bufferedPosition = builder2.setBufferedPosition(j2);
        PlaybackState.Builder actions = builder2.setActions(j3);
        PlaybackState.Builder errorMessage = builder2.setErrorMessage(charSequence);
        return builder2.build();
    }
}
