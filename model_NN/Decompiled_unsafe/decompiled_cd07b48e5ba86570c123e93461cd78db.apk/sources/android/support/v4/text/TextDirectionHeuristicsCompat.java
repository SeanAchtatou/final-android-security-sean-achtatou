package android.support.v4.text;

import android.support.v4.view.MotionEventCompat;
import java.nio.CharBuffer;
import java.util.Locale;

public class TextDirectionHeuristicsCompat {
    public static final TextDirectionHeuristicCompat ANYRTL_LTR;
    public static final TextDirectionHeuristicCompat FIRSTSTRONG_LTR;
    public static final TextDirectionHeuristicCompat FIRSTSTRONG_RTL;
    public static final TextDirectionHeuristicCompat LOCALE = TextDirectionHeuristicLocale.INSTANCE;
    public static final TextDirectionHeuristicCompat LTR;
    public static final TextDirectionHeuristicCompat RTL;
    private static final int STATE_FALSE = 1;
    private static final int STATE_TRUE = 0;
    private static final int STATE_UNKNOWN = 2;

    private interface TextDirectionAlgorithm {
        int checkRtl(CharSequence charSequence, int i, int i2);
    }

    static {
        TextDirectionHeuristicCompat textDirectionHeuristicCompat;
        TextDirectionHeuristicCompat textDirectionHeuristicCompat2;
        TextDirectionHeuristicCompat textDirectionHeuristicCompat3;
        TextDirectionHeuristicCompat textDirectionHeuristicCompat4;
        TextDirectionHeuristicCompat textDirectionHeuristicCompat5;
        new TextDirectionHeuristicInternal(null, false);
        LTR = textDirectionHeuristicCompat;
        new TextDirectionHeuristicInternal(null, true);
        RTL = textDirectionHeuristicCompat2;
        new TextDirectionHeuristicInternal(FirstStrong.INSTANCE, false);
        FIRSTSTRONG_LTR = textDirectionHeuristicCompat3;
        new TextDirectionHeuristicInternal(FirstStrong.INSTANCE, true);
        FIRSTSTRONG_RTL = textDirectionHeuristicCompat4;
        new TextDirectionHeuristicInternal(AnyStrong.INSTANCE_RTL, false);
        ANYRTL_LTR = textDirectionHeuristicCompat5;
    }

    /* access modifiers changed from: private */
    public static int isRtlText(int i) {
        switch (i) {
            case 0:
                return 1;
            case 1:
            case 2:
                return 0;
            default:
                return 2;
        }
    }

    /* access modifiers changed from: private */
    public static int isRtlTextOrFormat(int i) {
        switch (i) {
            case 0:
            case MotionEventCompat.AXIS_RZ /*14*/:
            case 15:
                return 1;
            case 1:
            case 2:
            case 16:
            case MotionEventCompat.AXIS_LTRIGGER /*17*/:
                return 0;
            default:
                return 2;
        }
    }

    private static abstract class TextDirectionHeuristicImpl implements TextDirectionHeuristicCompat {
        private final TextDirectionAlgorithm mAlgorithm;

        /* access modifiers changed from: protected */
        public abstract boolean defaultIsRtl();

        public TextDirectionHeuristicImpl(TextDirectionAlgorithm textDirectionAlgorithm) {
            this.mAlgorithm = textDirectionAlgorithm;
        }

        public boolean isRtl(char[] cArr, int i, int i2) {
            return isRtl(CharBuffer.wrap(cArr), i, i2);
        }

        public boolean isRtl(CharSequence charSequence, int i, int i2) {
            Throwable th;
            CharSequence charSequence2 = charSequence;
            int i3 = i;
            int i4 = i2;
            if (charSequence2 == null || i3 < 0 || i4 < 0 || charSequence2.length() - i4 < i3) {
                Throwable th2 = th;
                new IllegalArgumentException();
                throw th2;
            } else if (this.mAlgorithm == null) {
                return defaultIsRtl();
            } else {
                return doCheck(charSequence2, i3, i4);
            }
        }

        private boolean doCheck(CharSequence charSequence, int i, int i2) {
            switch (this.mAlgorithm.checkRtl(charSequence, i, i2)) {
                case 0:
                    return true;
                case 1:
                    return false;
                default:
                    return defaultIsRtl();
            }
        }
    }

    private static class TextDirectionHeuristicInternal extends TextDirectionHeuristicImpl {
        private final boolean mDefaultIsRtl;

        private TextDirectionHeuristicInternal(TextDirectionAlgorithm textDirectionAlgorithm, boolean z) {
            super(textDirectionAlgorithm);
            this.mDefaultIsRtl = z;
        }

        /* access modifiers changed from: protected */
        public boolean defaultIsRtl() {
            return this.mDefaultIsRtl;
        }
    }

    private static class FirstStrong implements TextDirectionAlgorithm {
        public static final FirstStrong INSTANCE;

        public int checkRtl(CharSequence charSequence, int i, int i2) {
            CharSequence charSequence2 = charSequence;
            int i3 = i;
            int i4 = 2;
            int i5 = i3 + i2;
            for (int i6 = i3; i6 < i5 && i4 == 2; i6++) {
                i4 = TextDirectionHeuristicsCompat.isRtlTextOrFormat(Character.getDirectionality(charSequence2.charAt(i6)));
            }
            return i4;
        }

        private FirstStrong() {
        }

        static {
            FirstStrong firstStrong;
            new FirstStrong();
            INSTANCE = firstStrong;
        }
    }

    private static class AnyStrong implements TextDirectionAlgorithm {
        public static final AnyStrong INSTANCE_LTR;
        public static final AnyStrong INSTANCE_RTL;
        private final boolean mLookForRtl;

        public int checkRtl(CharSequence charSequence, int i, int i2) {
            CharSequence charSequence2 = charSequence;
            int i3 = i;
            boolean z = false;
            int i4 = i3 + i2;
            for (int i5 = i3; i5 < i4; i5++) {
                switch (TextDirectionHeuristicsCompat.isRtlText(Character.getDirectionality(charSequence2.charAt(i5)))) {
                    case 0:
                        if (!this.mLookForRtl) {
                            z = true;
                            break;
                        } else {
                            return 0;
                        }
                    case 1:
                        if (this.mLookForRtl) {
                            z = true;
                            break;
                        } else {
                            return 1;
                        }
                }
            }
            if (!z) {
                return 2;
            }
            return this.mLookForRtl ? 1 : 0;
        }

        private AnyStrong(boolean z) {
            this.mLookForRtl = z;
        }

        static {
            AnyStrong anyStrong;
            AnyStrong anyStrong2;
            new AnyStrong(true);
            INSTANCE_RTL = anyStrong;
            new AnyStrong(false);
            INSTANCE_LTR = anyStrong2;
        }
    }

    private static class TextDirectionHeuristicLocale extends TextDirectionHeuristicImpl {
        public static final TextDirectionHeuristicLocale INSTANCE;

        public TextDirectionHeuristicLocale() {
            super(null);
        }

        /* access modifiers changed from: protected */
        public boolean defaultIsRtl() {
            return TextUtilsCompat.getLayoutDirectionFromLocale(Locale.getDefault()) == 1;
        }

        static {
            TextDirectionHeuristicLocale textDirectionHeuristicLocale;
            new TextDirectionHeuristicLocale();
            INSTANCE = textDirectionHeuristicLocale;
        }
    }
}
