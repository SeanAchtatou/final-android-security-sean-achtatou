package android.support.v4.text;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.MotionEventCompat;
import java.util.Locale;

public class TextUtilsCompat {
    private static String ARAB_SCRIPT_SUBTAG = "Arab";
    private static String HEBR_SCRIPT_SUBTAG = "Hebr";
    public static final Locale ROOT;

    @NonNull
    public static String htmlEncode(@NonNull String str) {
        StringBuilder sb;
        String str2 = str;
        new StringBuilder();
        StringBuilder sb2 = sb;
        for (int i = 0; i < str2.length(); i++) {
            char charAt = str2.charAt(i);
            switch (charAt) {
                case MotionEventCompat.AXIS_GENERIC_3 /*34*/:
                    StringBuilder append = sb2.append("&quot;");
                    break;
                case MotionEventCompat.AXIS_GENERIC_7 /*38*/:
                    StringBuilder append2 = sb2.append("&amp;");
                    break;
                case MotionEventCompat.AXIS_GENERIC_8 /*39*/:
                    StringBuilder append3 = sb2.append("&#39;");
                    break;
                case '<':
                    StringBuilder append4 = sb2.append("&lt;");
                    break;
                case '>':
                    StringBuilder append5 = sb2.append("&gt;");
                    break;
                default:
                    StringBuilder append6 = sb2.append(charAt);
                    break;
            }
        }
        return sb2.toString();
    }

    public static int getLayoutDirectionFromLocale(@Nullable Locale locale) {
        Locale locale2 = locale;
        if (locale2 != null && !locale2.equals(ROOT)) {
            String script = ICUCompat.getScript(ICUCompat.addLikelySubtags(locale2.toString()));
            if (script == null) {
                return getLayoutDirectionFromFirstChar(locale2);
            }
            if (script.equalsIgnoreCase(ARAB_SCRIPT_SUBTAG) || script.equalsIgnoreCase(HEBR_SCRIPT_SUBTAG)) {
                return 1;
            }
        }
        return 0;
    }

    private static int getLayoutDirectionFromFirstChar(Locale locale) {
        Locale locale2 = locale;
        switch (Character.getDirectionality(locale2.getDisplayName(locale2).charAt(0))) {
            case 1:
            case 2:
                return 1;
            default:
                return 0;
        }
    }

    static {
        Locale locale;
        new Locale("", "");
        ROOT = locale;
    }
}
