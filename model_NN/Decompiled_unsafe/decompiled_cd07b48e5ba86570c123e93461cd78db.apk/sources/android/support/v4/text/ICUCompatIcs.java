package android.support.v4.text;

import android.util.Log;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

class ICUCompatIcs {
    private static final String TAG = "ICUCompatIcs";
    private static Method sAddLikelySubtagsMethod;
    private static Method sGetScriptMethod;

    ICUCompatIcs() {
    }

    static {
        try {
            Class<?> cls = Class.forName("libcore.icu.ICU");
            if (cls != null) {
                sGetScriptMethod = cls.getMethod("getScript", String.class);
                sAddLikelySubtagsMethod = cls.getMethod("addLikelySubtags", String.class);
            }
        } catch (Exception e) {
            int w = Log.w(TAG, e);
        }
    }

    public static String getScript(String str) {
        String str2 = str;
        try {
            if (sGetScriptMethod != null) {
                return (String) sGetScriptMethod.invoke(null, str2);
            }
        } catch (IllegalAccessException e) {
            int w = Log.w(TAG, e);
        } catch (InvocationTargetException e2) {
            int w2 = Log.w(TAG, e2);
        }
        return null;
    }

    public static String addLikelySubtags(String str) {
        String str2 = str;
        try {
            if (sAddLikelySubtagsMethod != null) {
                return (String) sAddLikelySubtagsMethod.invoke(null, str2);
            }
        } catch (IllegalAccessException e) {
            int w = Log.w(TAG, e);
        } catch (InvocationTargetException e2) {
            int w2 = Log.w(TAG, e2);
        }
        return str2;
    }
}
