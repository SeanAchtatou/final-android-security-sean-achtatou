package android.support.v4.hardware.display;

import android.content.Context;
import android.os.Build;
import android.view.Display;
import android.view.WindowManager;
import java.util.WeakHashMap;

public abstract class DisplayManagerCompat {
    public static final String DISPLAY_CATEGORY_PRESENTATION = "android.hardware.display.category.PRESENTATION";
    private static final WeakHashMap<Context, DisplayManagerCompat> sInstances;

    public abstract Display getDisplay(int i);

    public abstract Display[] getDisplays();

    public abstract Display[] getDisplays(String str);

    static {
        WeakHashMap<Context, DisplayManagerCompat> weakHashMap;
        new WeakHashMap<>();
        sInstances = weakHashMap;
    }

    DisplayManagerCompat() {
    }

    /* JADX INFO: finally extract failed */
    public static DisplayManagerCompat getInstance(Context context) {
        DisplayManagerCompat displayManagerCompat;
        DisplayManagerCompat displayManagerCompat2;
        Context context2 = context;
        WeakHashMap<Context, DisplayManagerCompat> weakHashMap = sInstances;
        WeakHashMap<Context, DisplayManagerCompat> weakHashMap2 = weakHashMap;
        synchronized (weakHashMap) {
            try {
                DisplayManagerCompat displayManagerCompat3 = sInstances.get(context2);
                if (displayManagerCompat3 == null) {
                    if (Build.VERSION.SDK_INT >= 17) {
                        new JellybeanMr1Impl(context2);
                        displayManagerCompat3 = displayManagerCompat2;
                    } else {
                        new LegacyImpl(context2);
                        displayManagerCompat3 = displayManagerCompat;
                    }
                    DisplayManagerCompat put = sInstances.put(context2, displayManagerCompat3);
                }
                DisplayManagerCompat displayManagerCompat4 = displayManagerCompat3;
                return displayManagerCompat4;
            } catch (Throwable th) {
                Throwable th2 = th;
                WeakHashMap<Context, DisplayManagerCompat> weakHashMap3 = weakHashMap2;
                throw th2;
            }
        }
    }

    private static class LegacyImpl extends DisplayManagerCompat {
        private final WindowManager mWindowManager;

        public LegacyImpl(Context context) {
            this.mWindowManager = (WindowManager) context.getSystemService("window");
        }

        public Display getDisplay(int i) {
            Display defaultDisplay = this.mWindowManager.getDefaultDisplay();
            if (defaultDisplay.getDisplayId() == i) {
                return defaultDisplay;
            }
            return null;
        }

        public Display[] getDisplays() {
            return new Display[]{this.mWindowManager.getDefaultDisplay()};
        }

        public Display[] getDisplays(String str) {
            return str == null ? getDisplays() : new Display[0];
        }
    }

    private static class JellybeanMr1Impl extends DisplayManagerCompat {
        private final Object mDisplayManagerObj;

        public JellybeanMr1Impl(Context context) {
            this.mDisplayManagerObj = DisplayManagerJellybeanMr1.getDisplayManager(context);
        }

        public Display getDisplay(int i) {
            return DisplayManagerJellybeanMr1.getDisplay(this.mDisplayManagerObj, i);
        }

        public Display[] getDisplays() {
            return DisplayManagerJellybeanMr1.getDisplays(this.mDisplayManagerObj);
        }

        public Display[] getDisplays(String str) {
            return DisplayManagerJellybeanMr1.getDisplays(this.mDisplayManagerObj, str);
        }
    }
}
