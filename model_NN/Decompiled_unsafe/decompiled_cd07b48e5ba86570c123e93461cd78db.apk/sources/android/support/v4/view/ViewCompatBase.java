package android.support.v4.view;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.view.View;

class ViewCompatBase {
    ViewCompatBase() {
    }

    static ColorStateList getBackgroundTintList(View view) {
        View view2 = view;
        return view2 instanceof TintableBackgroundView ? ((TintableBackgroundView) view2).getSupportBackgroundTintList() : null;
    }

    static void setBackgroundTintList(View view, ColorStateList colorStateList) {
        View view2 = view;
        ColorStateList colorStateList2 = colorStateList;
        if (view2 instanceof TintableBackgroundView) {
            ((TintableBackgroundView) view2).setSupportBackgroundTintList(colorStateList2);
        }
    }

    static PorterDuff.Mode getBackgroundTintMode(View view) {
        View view2 = view;
        return view2 instanceof TintableBackgroundView ? ((TintableBackgroundView) view2).getSupportBackgroundTintMode() : null;
    }

    static void setBackgroundTintMode(View view, PorterDuff.Mode mode) {
        View view2 = view;
        PorterDuff.Mode mode2 = mode;
        if (view2 instanceof TintableBackgroundView) {
            ((TintableBackgroundView) view2).setSupportBackgroundTintMode(mode2);
        }
    }

    static boolean isLaidOut(View view) {
        View view2 = view;
        return view2.getWidth() > 0 && view2.getHeight() > 0;
    }
}
