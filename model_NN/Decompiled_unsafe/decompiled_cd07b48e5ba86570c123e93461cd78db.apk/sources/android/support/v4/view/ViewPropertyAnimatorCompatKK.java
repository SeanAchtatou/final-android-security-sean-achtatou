package android.support.v4.view;

import android.animation.ValueAnimator;
import android.view.View;
import android.view.ViewPropertyAnimator;

class ViewPropertyAnimatorCompatKK {
    ViewPropertyAnimatorCompatKK() {
    }

    public static void setUpdateListener(View view, ViewPropertyAnimatorUpdateListener viewPropertyAnimatorUpdateListener) {
        ValueAnimator.AnimatorUpdateListener animatorUpdateListener;
        View view2 = view;
        final ViewPropertyAnimatorUpdateListener viewPropertyAnimatorUpdateListener2 = viewPropertyAnimatorUpdateListener;
        final View view3 = view2;
        new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                viewPropertyAnimatorUpdateListener2.onAnimationUpdate(view3);
            }
        };
        ViewPropertyAnimator updateListener = view2.animate().setUpdateListener(animatorUpdateListener);
    }
}
