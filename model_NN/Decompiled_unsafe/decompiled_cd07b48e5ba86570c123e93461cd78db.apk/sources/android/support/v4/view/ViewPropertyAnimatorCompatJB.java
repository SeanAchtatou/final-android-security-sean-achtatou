package android.support.v4.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.view.View;
import android.view.ViewPropertyAnimator;

class ViewPropertyAnimatorCompatJB {
    ViewPropertyAnimatorCompatJB() {
    }

    public static void withStartAction(View view, Runnable runnable) {
        ViewPropertyAnimator withStartAction = view.animate().withStartAction(runnable);
    }

    public static void withEndAction(View view, Runnable runnable) {
        ViewPropertyAnimator withEndAction = view.animate().withEndAction(runnable);
    }

    public static void withLayer(View view) {
        ViewPropertyAnimator withLayer = view.animate().withLayer();
    }

    public static void setListener(View view, ViewPropertyAnimatorListener viewPropertyAnimatorListener) {
        Animator.AnimatorListener animatorListener;
        View view2 = view;
        ViewPropertyAnimatorListener viewPropertyAnimatorListener2 = viewPropertyAnimatorListener;
        if (viewPropertyAnimatorListener2 != null) {
            final ViewPropertyAnimatorListener viewPropertyAnimatorListener3 = viewPropertyAnimatorListener2;
            final View view3 = view2;
            new AnimatorListenerAdapter() {
                public void onAnimationCancel(Animator animator) {
                    viewPropertyAnimatorListener3.onAnimationCancel(view3);
                }

                public void onAnimationEnd(Animator animator) {
                    viewPropertyAnimatorListener3.onAnimationEnd(view3);
                }

                public void onAnimationStart(Animator animator) {
                    viewPropertyAnimatorListener3.onAnimationStart(view3);
                }
            };
            ViewPropertyAnimator listener = view2.animate().setListener(animatorListener);
            return;
        }
        ViewPropertyAnimator listener2 = view2.animate().setListener(null);
    }
}
