package android.support.v4.provider;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.DocumentsContract;
import android.util.Log;
import java.util.ArrayList;

class DocumentsContractApi21 {
    private static final String TAG = "DocumentFile";

    DocumentsContractApi21() {
    }

    public static Uri createFile(Context context, Uri uri, String str, String str2) {
        return DocumentsContract.createDocument(context.getContentResolver(), uri, str, str2);
    }

    public static Uri createDirectory(Context context, Uri uri, String str) {
        return createFile(context, uri, "vnd.android.document/directory", str);
    }

    public static Uri prepareTreeUri(Uri uri) {
        Uri uri2 = uri;
        return DocumentsContract.buildDocumentUriUsingTree(uri2, DocumentsContract.getTreeDocumentId(uri2));
    }

    public static Uri[] listFiles(Context context, Uri uri) {
        ArrayList arrayList;
        StringBuilder sb;
        Uri uri2 = uri;
        ContentResolver contentResolver = context.getContentResolver();
        Uri buildChildDocumentsUriUsingTree = DocumentsContract.buildChildDocumentsUriUsingTree(uri2, DocumentsContract.getDocumentId(uri2));
        new ArrayList();
        ArrayList arrayList2 = arrayList;
        try {
            Cursor query = contentResolver.query(buildChildDocumentsUriUsingTree, new String[]{"document_id"}, null, null, null);
            while (query.moveToNext()) {
                boolean add = arrayList2.add(DocumentsContract.buildDocumentUriUsingTree(uri2, query.getString(0)));
            }
            closeQuietly(query);
        } catch (Exception e) {
            Exception exc = e;
            new StringBuilder();
            int w = Log.w(TAG, sb.append("Failed query: ").append(exc).toString());
            closeQuietly(null);
        } catch (Throwable th) {
            closeQuietly(null);
            throw th;
        }
        return (Uri[]) arrayList2.toArray(new Uri[arrayList2.size()]);
    }

    public static Uri renameTo(Context context, Uri uri, String str) {
        return DocumentsContract.renameDocument(context.getContentResolver(), uri, str);
    }

    private static void closeQuietly(AutoCloseable autoCloseable) {
        AutoCloseable autoCloseable2 = autoCloseable;
        if (autoCloseable2 != null) {
            try {
                autoCloseable2.close();
            } catch (RuntimeException e) {
                throw e;
            } catch (Exception e2) {
            }
        }
    }
}
