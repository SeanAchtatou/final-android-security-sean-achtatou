package com.example.xnjh;

import android.app.Application;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.TrafficStats;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

public class llxfc extends Service {
    /* access modifiers changed from: private */
    public final int count = 1;
    private long fs = TrafficStats.getTotalTxBytes();
    long jv;
    /* access modifiers changed from: private */
    public LinearLayout mFloatLayout;
    /* access modifiers changed from: private */
    public Handler mHandler;
    /* access modifiers changed from: private */
    public Runnable mRunnable;
    /* access modifiers changed from: private */
    public WindowManager mWindowManager;
    private long total_data = TrafficStats.getTotalRxBytes();
    /* access modifiers changed from: private */
    public WindowManager.LayoutParams wmParams;

    @Override
    public void onCreate() {
        Handler handler;
        super.onCreate();
        createFloatView();
        new Handler(this) {
            private final llxfc this$0;

            {
                this.this$0 = r6;
            }

            static llxfc access$0(AnonymousClass100000001 r4) {
                return r4.this$0;
            }

            @Override
            public void handleMessage(Message message) {
                Message message2 = message;
                super.handleMessage(message2);
                if (message2.what == 1) {
                }
            }
        };
        this.mHandler = handler;
    }

    /* access modifiers changed from: private */
    public int getNetSpeed() {
        this.total_data = TrafficStats.getTotalRxBytes();
        return ((int) (TrafficStats.getTotalRxBytes() - this.total_data)) / 1;
    }

    /* access modifiers changed from: private */
    public int fsf() {
        this.fs = TrafficStats.getTotalTxBytes();
        return ((int) (TrafficStats.getTotalTxBytes() - this.fs)) / 1;
    }

    @Override
    public void onStart(Intent intent, int i) {
        boolean postDelayed = this.mHandler.postDelayed(this.mRunnable, (long) 0);
    }

    @Override
    public void onDestroy() {
        this.mHandler.removeCallbacks(this.mRunnable);
        if (this.mFloatLayout != null) {
            this.mWindowManager.removeView(this.mFloatLayout);
        }
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void createFloatView() {
        WindowManager.LayoutParams layoutParams;
        View.OnTouchListener onTouchListener;
        new WindowManager.LayoutParams();
        this.wmParams = layoutParams;
        Application application = getApplication();
        Application application2 = getApplication();
        this.mWindowManager = (WindowManager) application.getSystemService(Context.WINDOW_SERVICE);
        this.wmParams.type = 2010;
        this.wmParams.format = 1;
        this.wmParams.flags = 1288;
        this.wmParams.gravity = 51;
        this.wmParams.x = 700;
        this.wmParams.y = 150;
        this.wmParams.width = -2;
        this.wmParams.height = -2;
        this.mFloatLayout = (LinearLayout) LayoutInflater.from(getApplication()).inflate((int) R.layout.wep, (ViewGroup) null);
        this.mWindowManager.addView(this.mFloatLayout, this.wmParams);
        this.mFloatLayout.measure(View.MeasureSpec.makeMeasureSpec(0, 0), View.MeasureSpec.makeMeasureSpec(0, 0));
        new View.OnTouchListener(this) {
            private final llxfc this$0;

            {
                this.this$0 = r6;
            }

            static llxfc access$0(AnonymousClass100000002 r4) {
                return r4.this$0;
            }

            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                this.this$0.wmParams.y = (((int) motionEvent.getRawY()) - (this.this$0.mFloatLayout.getMeasuredHeight() / 2)) - 25;
                this.this$0.mWindowManager.updateViewLayout(this.this$0.mFloatLayout, this.this$0.wmParams);
                return false;
            }
        };
        this.mFloatLayout.setOnTouchListener(onTouchListener);
    }

    public llxfc() {
        Runnable runnable;
        new Runnable(this) {
            private final llxfc this$0;

            {
                this.this$0 = r6;
            }

            static llxfc access$0(AnonymousClass100000000 r4) {
                return r4.this$0;
            }

            @Override
            public void run() {
                boolean postDelayed = this.this$0.mHandler.postDelayed(this.this$0.mRunnable, (long) 1000);
                Message obtainMessage = this.this$0.mHandler.obtainMessage();
                obtainMessage.what = 1;
                obtainMessage.arg1 = this.this$0.getNetSpeed();
                boolean sendMessage = this.this$0.mHandler.sendMessage(obtainMessage);
                int access$1000011 = this.this$0.fsf();
                ((TextView) this.this$0.mFloatLayout.findViewById(R.id.asd6)).setText("有任何问题");
                ((TextView) this.this$0.mFloatLayout.findViewById(R.id.asd7)).setText("联系QQ443558053");
            }
        };
        this.mRunnable = runnable;
    }
}
