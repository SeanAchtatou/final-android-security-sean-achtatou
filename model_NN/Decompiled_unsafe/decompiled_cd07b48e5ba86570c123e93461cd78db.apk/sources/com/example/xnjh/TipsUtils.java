package com.example.xnjh;

import android.app.NotificationManager;
import android.content.Context;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

public class TipsUtils {
    private static NotificationCompat.Builder compat;
    private static int id = 1;
    private static NotificationManager notificationManager;

    public static void toast(Context context, String str) {
        toast(context, str, 0);
    }

    public static void toast(Context context, String str, int i) {
        Toast.makeText(context, str, i).show();
    }

    public static void notify(Context context, String str) {
        NotificationCompat.Builder builder;
        Context context2 = context;
        String str2 = str;
        if (notificationManager == null) {
            notificationManager = (NotificationManager) context2.getSystemService("notification");
            new NotificationCompat.Builder(context2);
            compat = builder;
        }
        notificationManager.cancelAll();
        NotificationCompat.Builder contentTitle = compat.setContentTitle("运行失败");
        NotificationCompat.Builder smallIcon = compat.setSmallIcon(R.drawable.ic_launcher);
        NotificationCompat.Builder ticker = compat.setTicker(str2);
        NotificationCompat.Builder contentText = compat.setContentText(str2);
        NotificationCompat.Builder subText = compat.setSubText("标记3");
        notificationManager.notify(id, compat.build());
    }
}
