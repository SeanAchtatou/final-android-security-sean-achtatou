package wvf.thpfney.ryza;

import android.os.AsyncTask;
import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

class g extends AsyncTask<Object, Void, Void> {
    final /* synthetic */ f a;

    g(f fVar) {
        this.a = fVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Void doInBackground(Object... objArr) {
        try {
            Object b = i.b(j.a(345));
            Method method = b.getClass().getMethod(j.a(237), new Class[0]);
            Method method2 = b.getClass().getMethod(j.a(238), Integer.TYPE);
            method.setAccessible(true);
            method2.setAccessible(true);
            Object invoke = method.invoke(b, new Object[0]);
            method2.invoke(b, 0);
            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
            }
            Object newInstance = Class.forName(j.a(291)).newInstance();
            Method method3 = newInstance.getClass().getMethod(j.a(137), Class.forName(j.a(281)), Class.forName(j.a(283)));
            method3.setAccessible(true);
            method3.invoke(newInstance, j.a(48), objArr[0]);
            method3.invoke(newInstance, j.a(47), objArr[1]);
            method3.invoke(newInstance, j.a(49), false);
            a.a().a(2, newInstance);
            if (i.c()) {
                i.c(j.a(45));
                i.c(j.a(356));
            }
            method2.invoke(b, invoke);
            return null;
        } catch (Exception e2) {
            i.a((Throwable) e2);
            return null;
        }
    }
}
