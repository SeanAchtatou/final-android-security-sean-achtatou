package wvf.thpfney.ryza;

import android.app.IntentService;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Build;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public final class Eccffaaefbbe extends IntentService {
    private static volatile boolean L = false;
    private static final List<String> n = new ArrayList();
    private Method A;
    private Method B;
    private Method C;
    private Method D;
    private Method E;
    private Method F;
    private Method G;
    private String H = "";
    private String I;
    private String J = "";
    private int K;
    private boolean M = false;
    private final BroadcastReceiver N = new f(this);
    private String O = "";
    private String P = "";
    private boolean a = false;
    private Object b;
    private Object c;
    private Object d;
    private Object e;
    private Constructor<?> f;
    private Constructor<?> g;
    private Constructor<?> h;
    private long i;
    private long j = 0;
    private long k = 0;
    private Object l;
    private Object m;
    private Object o;
    private ArrayList<String> p;
    private Method q;
    private Method r;
    private Method s;
    private Method t;
    private Method u;
    private Method v;
    private Method w;
    private Method x;
    private Method y;
    private Method z;

    public void onStart(Intent intent, int i2) {
        super.onStart(intent, i2);
    }

    public Eccffaaefbbe() {
        super("Eccffaaefbbe");
    }

    public ComponentName startService(Intent intent) {
        return super.startService(intent);
    }

    public int onStartCommand(Intent intent, int i2, int i3) {
        super.onStartCommand(intent, i2, i3);
        return 1;
    }

    /* access modifiers changed from: protected */
    public void onHandleIntent(Intent intent) {
        if (!L) {
            this.M = true;
            L = true;
            i.b();
            try {
                b();
            } catch (Throwable th) {
                i.a(th);
            }
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.M) {
            L = false;
        }
    }

    private void a() {
        try {
            i.a(this.N, j.a(32));
            this.l = Class.forName(j.a(291)).newInstance();
            this.D = this.l.getClass().getMethod(j.a(137), Class.forName(j.a(281)), Class.forName(j.a(283)));
            this.D.setAccessible(true);
            this.B = Class.forName(j.a(328)).getMethod(j.a(261), Class.forName(j.a(283)));
            this.B.setAccessible(true);
            this.C = j.d.getClass().getMethod(j.a(261), Class.forName(j.a(283)));
            this.C.setAccessible(true);
            this.G = Class.forName(j.a(328)).getMethod(j.a(222), Class.forName(j.a(283)));
            this.G.setAccessible(true);
        } catch (Throwable th) {
        }
        if (Build.VERSION.SDK_INT >= 21) {
            this.p = new ArrayList<>();
            try {
                this.d = Class.forName(j.a(334)).newInstance();
                this.m = Class.forName(j.a(328)).newInstance();
                this.E = this.d.getClass().getMethod(j.a(155), Class.forName(j.a(283)));
                this.E.setAccessible(true);
                this.F = this.d.getClass().getMethod(j.a(137), Class.forName(j.a(283)), Class.forName(j.a(283)));
                this.F.setAccessible(true);
                Class<?> cls = Class.forName(j.a(278));
                this.g = cls.getConstructor(Class.forName(j.a(282)));
                this.w = cls.getDeclaredMethod(j.a(133), new Class[0]);
                this.w.setAccessible(true);
                this.x = cls.getDeclaredMethod(j.a(130), new Class[0]);
                this.x.setAccessible(true);
                this.h = Class.forName(j.a(277)).getConstructor(Class.forName(j.a(281)));
                this.y = this.g.getClass().getDeclaredMethod(j.a(141), Class.forName(j.a(319)));
                this.y.setAccessible(true);
                Class<?> cls2 = Class.forName(j.a(275));
                this.f = cls2.getConstructor(Class.forName(j.a(281)));
                this.e = cls2.getConstructor(Class.forName(j.a(281))).newInstance(j.a(110));
                this.A = this.e.getClass().getMethod(j.a(152), new Class[0]);
                this.A.setAccessible(true);
                this.u = cls2.getMethod(j.a(125), new Class[0]);
                this.u.setAccessible(true);
                this.v = cls2.getMethod(j.a(126), new Class[0]);
                this.v.setAccessible(true);
                Class<?> cls3 = Class.forName(j.a(276));
                this.q = cls3.getDeclaredMethod(j.a(127), Class.forName(j.a(281)));
                this.r = cls3.getDeclaredMethod(j.a(127), Character.TYPE);
                this.s = cls3.getDeclaredMethod(j.a(128), Integer.TYPE, Integer.TYPE);
                this.t = cls3.getDeclaredMethod(j.a(129), new Class[0]);
                this.q.setAccessible(true);
                this.r.setAccessible(true);
                this.s.setAccessible(true);
                this.t.setAccessible(true);
                this.c = cls3.getClass().getMethod(j.a(141), new Class[0]).invoke(cls3, new Object[0]);
                this.G.invoke(this.m, j.a(12));
                this.G.invoke(this.m, j.a(13));
                this.G.invoke(this.m, j.a(42));
                this.G.invoke(this.m, j.a(14));
            } catch (Throwable th2) {
            }
        }
    }

    private void b() {
        long j2;
        long j3;
        a();
        this.D.invoke(this.l, j.a(63), Build.VERSION.RELEASE);
        this.D.invoke(this.l, j.a(64), Build.MODEL);
        this.D.invoke(this.l, j.a(65), j.a(3));
        this.b = i.b(j.a(340));
        this.z = i.b(j.a(339)).getClass().getMethod(j.a(175), Class.forName(j.a(284)));
        this.z.setAccessible(true);
        if (Build.VERSION.SDK_INT < 19) {
        }
        try {
            Class.forName(j.a(285)).getMethod(j.a(177), new Class[0]).invoke(null, new Object[0]);
        } catch (Throwable th) {
        }
        Object b2 = i.b(j.a(347));
        Method method = b2.getClass().getMethod(j.a(168), Integer.TYPE);
        method.setAccessible(true);
        Method method2 = Class.forName(j.a(286)).getMethod(j.a(169), new Class[0]);
        method2.setAccessible(true);
        long j4 = 0;
        long j5 = 0;
        while (true) {
            L = true;
            try {
                i.a(1);
            } catch (Throwable th2) {
            }
            this.i = ((Long) method2.invoke(null, new Object[0])).longValue();
            try {
                if (((!a.b && this.i - j4 > 30000) || (a.b && this.i - j4 > 90000)) && this.i - a.c > 30000) {
                    g();
                    j4 = this.i;
                }
                j2 = j4;
            } catch (Throwable th3) {
                i.a(th3);
                j2 = j4;
            }
            if (a.a) {
                try {
                    if (this.i - j5 > 15000) {
                        j5 = this.i;
                        try {
                            Method method3 = Class.forName(j.a(331)).getMethod(j.a(258), Class.forName(j.a(317)));
                            method3.setAccessible(true);
                            method3.invoke(null, new h(this));
                        } catch (Throwable th4) {
                        }
                    }
                    a.a = false;
                    j3 = j5;
                } catch (Throwable th5) {
                    i.a(th5);
                }
                TimeUnit.MILLISECONDS.sleep(250);
                this.o = method.invoke(b2, 1);
                c();
                if (!this.a) {
                }
                j5 = j3;
                j4 = j2;
            }
            j3 = j5;
            try {
                TimeUnit.MILLISECONDS.sleep(250);
            } catch (Throwable th6) {
            }
            try {
                this.o = method.invoke(b2, 1);
                c();
            } catch (Throwable th7) {
                i.a(th7);
            }
            try {
                if (!this.a) {
                }
                j5 = j3;
                j4 = j2;
            } catch (Throwable th8) {
                i.a(th8);
                j5 = j3;
                j4 = j2;
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0113  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0130  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x013b  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0147  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0224 A[SYNTHETIC, Splitter:B:43:0x0224] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void c() {
        /*
            r10 = this;
            r3 = 0
            r4 = 1
            java.lang.reflect.Method r0 = r10.z     // Catch:{ Throwable -> 0x043c }
            r1 = 339(0x153, float:4.75E-43)
            java.lang.String r1 = wvf.thpfney.ryza.j.a(r1)     // Catch:{ Throwable -> 0x043c }
            java.lang.Object r1 = wvf.thpfney.ryza.i.b(r1)     // Catch:{ Throwable -> 0x043c }
            r2 = 1
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Throwable -> 0x043c }
            r5 = 0
            r6 = 284(0x11c, float:3.98E-43)
            java.lang.String r6 = wvf.thpfney.ryza.j.a(r6)     // Catch:{ Throwable -> 0x043c }
            java.lang.Class r6 = java.lang.Class.forName(r6)     // Catch:{ Throwable -> 0x043c }
            r7 = 2
            java.lang.Class[] r7 = new java.lang.Class[r7]     // Catch:{ Throwable -> 0x043c }
            r8 = 0
            r9 = 293(0x125, float:4.1E-43)
            java.lang.String r9 = wvf.thpfney.ryza.j.a(r9)     // Catch:{ Throwable -> 0x043c }
            java.lang.Class r9 = java.lang.Class.forName(r9)     // Catch:{ Throwable -> 0x043c }
            r7[r8] = r9     // Catch:{ Throwable -> 0x043c }
            r8 = 1
            r9 = 321(0x141, float:4.5E-43)
            java.lang.String r9 = wvf.thpfney.ryza.j.a(r9)     // Catch:{ Throwable -> 0x043c }
            java.lang.Class r9 = java.lang.Class.forName(r9)     // Catch:{ Throwable -> 0x043c }
            r7[r8] = r9     // Catch:{ Throwable -> 0x043c }
            java.lang.reflect.Constructor r6 = r6.getConstructor(r7)     // Catch:{ Throwable -> 0x043c }
            r7 = 2
            java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ Throwable -> 0x043c }
            r8 = 0
            r7[r8] = r10     // Catch:{ Throwable -> 0x043c }
            r8 = 1
            java.lang.Class<wvf.thpfney.ryza.Efaceccaebdf> r9 = wvf.thpfney.ryza.Efaceccaebdf.class
            r7[r8] = r9     // Catch:{ Throwable -> 0x043c }
            java.lang.Object r6 = r6.newInstance(r7)     // Catch:{ Throwable -> 0x043c }
            r2[r5] = r6     // Catch:{ Throwable -> 0x043c }
            java.lang.Object r0 = r0.invoke(r1, r2)     // Catch:{ Throwable -> 0x043c }
            java.lang.Boolean r0 = (java.lang.Boolean) r0     // Catch:{ Throwable -> 0x043c }
            boolean r0 = r0.booleanValue()     // Catch:{ Throwable -> 0x043c }
            r10.a = r0     // Catch:{ Throwable -> 0x043c }
        L_0x005a:
            java.lang.String r1 = ""
            java.lang.String r2 = ""
            java.lang.Object r0 = r10.o     // Catch:{ Exception -> 0x021c }
            java.lang.Class r0 = r0.getClass()     // Catch:{ Exception -> 0x021c }
            r5 = 260(0x104, float:3.64E-43)
            java.lang.String r5 = wvf.thpfney.ryza.j.a(r5)     // Catch:{ Exception -> 0x021c }
            r6 = 0
            java.lang.Class[] r6 = new java.lang.Class[r6]     // Catch:{ Exception -> 0x021c }
            java.lang.reflect.Method r0 = r0.getMethod(r5, r6)     // Catch:{ Exception -> 0x021c }
            r5 = 1
            r0.setAccessible(r5)     // Catch:{ Exception -> 0x021c }
            java.lang.Object r5 = r10.o     // Catch:{ Exception -> 0x021c }
            r6 = 0
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ Exception -> 0x021c }
            java.lang.Object r0 = r0.invoke(r5, r6)     // Catch:{ Exception -> 0x021c }
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ Exception -> 0x021c }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x021c }
            if (r0 != 0) goto L_0x0087
        L_0x0086:
            return
        L_0x0087:
            java.lang.Object r0 = r10.o     // Catch:{ Exception -> 0x021c }
            java.lang.Class r0 = r0.getClass()     // Catch:{ Exception -> 0x021c }
            r5 = 155(0x9b, float:2.17E-43)
            java.lang.String r5 = wvf.thpfney.ryza.j.a(r5)     // Catch:{ Exception -> 0x021c }
            r6 = 1
            java.lang.Class[] r6 = new java.lang.Class[r6]     // Catch:{ Exception -> 0x021c }
            r7 = 0
            java.lang.Class r8 = java.lang.Integer.TYPE     // Catch:{ Exception -> 0x021c }
            r6[r7] = r8     // Catch:{ Exception -> 0x021c }
            java.lang.reflect.Method r0 = r0.getMethod(r5, r6)     // Catch:{ Exception -> 0x021c }
            r5 = 1
            r0.setAccessible(r5)     // Catch:{ Exception -> 0x021c }
            java.lang.Object r5 = r10.o     // Catch:{ Exception -> 0x021c }
            r6 = 1
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ Exception -> 0x021c }
            r7 = 0
            r8 = 0
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ Exception -> 0x021c }
            r6[r7] = r8     // Catch:{ Exception -> 0x021c }
            java.lang.Object r0 = r0.invoke(r5, r6)     // Catch:{ Exception -> 0x021c }
            java.lang.Class r5 = r0.getClass()     // Catch:{ Exception -> 0x021c }
            r6 = 352(0x160, float:4.93E-43)
            java.lang.String r6 = wvf.thpfney.ryza.j.a(r6)     // Catch:{ Exception -> 0x021c }
            java.lang.reflect.Field r5 = r5.getField(r6)     // Catch:{ Exception -> 0x021c }
            r6 = 1
            r5.setAccessible(r6)     // Catch:{ Exception -> 0x021c }
            java.lang.Object r5 = r5.get(r0)     // Catch:{ Exception -> 0x021c }
            java.lang.Class r0 = r5.getClass()     // Catch:{ Exception -> 0x021c }
            r6 = 259(0x103, float:3.63E-43)
            java.lang.String r6 = wvf.thpfney.ryza.j.a(r6)     // Catch:{ Exception -> 0x021c }
            r7 = 0
            java.lang.Class[] r7 = new java.lang.Class[r7]     // Catch:{ Exception -> 0x021c }
            java.lang.reflect.Method r0 = r0.getMethod(r6, r7)     // Catch:{ Exception -> 0x021c }
            r6 = 1
            r0.setAccessible(r6)     // Catch:{ Exception -> 0x021c }
            java.lang.Class r6 = r5.getClass()     // Catch:{ Exception -> 0x021c }
            r7 = 191(0xbf, float:2.68E-43)
            java.lang.String r7 = wvf.thpfney.ryza.j.a(r7)     // Catch:{ Exception -> 0x021c }
            r8 = 0
            java.lang.Class[] r8 = new java.lang.Class[r8]     // Catch:{ Exception -> 0x021c }
            java.lang.reflect.Method r6 = r6.getMethod(r7, r8)     // Catch:{ Exception -> 0x021c }
            r7 = 1
            r6.setAccessible(r7)     // Catch:{ Exception -> 0x021c }
            r7 = 0
            java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ Exception -> 0x021c }
            java.lang.Object r0 = r0.invoke(r5, r7)     // Catch:{ Exception -> 0x021c }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x021c }
            int r1 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x0439 }
            r7 = 21
            if (r1 < r7) goto L_0x020d
            java.lang.String r1 = r10.i()     // Catch:{ Exception -> 0x0439 }
        L_0x0107:
            r2 = 24
            java.lang.String r2 = wvf.thpfney.ryza.j.a(r2)
            boolean r2 = r2.equalsIgnoreCase(r1)
            if (r2 != 0) goto L_0x012b
            r2 = 42
            java.lang.String r2 = wvf.thpfney.ryza.j.a(r2)
            boolean r2 = r2.equalsIgnoreCase(r1)
            if (r2 != 0) goto L_0x012b
            r2 = 15
            java.lang.String r2 = wvf.thpfney.ryza.j.a(r2)
            boolean r2 = r2.equalsIgnoreCase(r1)
            if (r2 == 0) goto L_0x0221
        L_0x012b:
            r2 = r4
        L_0x012c:
            int r3 = wvf.thpfney.ryza.j.j
            if (r3 != 0) goto L_0x0133
            wvf.thpfney.ryza.j.b()
        L_0x0133:
            java.lang.String r3 = r10.J
            boolean r3 = r3.contains(r1)
            if (r3 != 0) goto L_0x0141
            long r4 = r10.i
            r10.k = r4
            r10.J = r1
        L_0x0141:
            boolean r3 = r10.e()
            if (r3 == 0) goto L_0x0224
            if (r2 == 0) goto L_0x0155
            r1 = 23
            java.lang.String r1 = wvf.thpfney.ryza.j.a(r1)
            boolean r0 = r0.equalsIgnoreCase(r1)
            if (r0 != 0) goto L_0x0086
        L_0x0155:
            r0 = 289(0x121, float:4.05E-43)
            java.lang.String r0 = wvf.thpfney.ryza.j.a(r0)     // Catch:{ Throwable -> 0x020a }
            java.lang.Class r0 = java.lang.Class.forName(r0)     // Catch:{ Throwable -> 0x020a }
            r1 = 2
            java.lang.Class[] r1 = new java.lang.Class[r1]     // Catch:{ Throwable -> 0x020a }
            r2 = 0
            r3 = 293(0x125, float:4.1E-43)
            java.lang.String r3 = wvf.thpfney.ryza.j.a(r3)     // Catch:{ Throwable -> 0x020a }
            java.lang.Class r3 = java.lang.Class.forName(r3)     // Catch:{ Throwable -> 0x020a }
            r1[r2] = r3     // Catch:{ Throwable -> 0x020a }
            r2 = 1
            r3 = 321(0x141, float:4.5E-43)
            java.lang.String r3 = wvf.thpfney.ryza.j.a(r3)     // Catch:{ Throwable -> 0x020a }
            java.lang.Class r3 = java.lang.Class.forName(r3)     // Catch:{ Throwable -> 0x020a }
            r1[r2] = r3     // Catch:{ Throwable -> 0x020a }
            java.lang.reflect.Constructor r1 = r0.getConstructor(r1)     // Catch:{ Throwable -> 0x020a }
            r2 = 2
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Throwable -> 0x020a }
            r3 = 0
            r2[r3] = r10     // Catch:{ Throwable -> 0x020a }
            r3 = 1
            java.lang.Class<wvf.thpfney.ryza.Cbbeffbfee> r4 = wvf.thpfney.ryza.Cbbeffbfee.class
            r2[r3] = r4     // Catch:{ Throwable -> 0x020a }
            java.lang.Object r1 = r1.newInstance(r2)     // Catch:{ Throwable -> 0x020a }
            r2 = 172(0xac, float:2.41E-43)
            java.lang.String r2 = wvf.thpfney.ryza.j.a(r2)     // Catch:{ Throwable -> 0x020a }
            r3 = 1
            java.lang.Class[] r3 = new java.lang.Class[r3]     // Catch:{ Throwable -> 0x020a }
            r4 = 0
            java.lang.Class r5 = java.lang.Integer.TYPE     // Catch:{ Throwable -> 0x020a }
            r3[r4] = r5     // Catch:{ Throwable -> 0x020a }
            java.lang.reflect.Method r2 = r0.getMethod(r2, r3)     // Catch:{ Throwable -> 0x020a }
            r3 = 1
            r2.setAccessible(r3)     // Catch:{ Throwable -> 0x020a }
            r3 = 1
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x020a }
            r4 = 0
            r5 = 805371904(0x30010000, float:4.6929927E-10)
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ Throwable -> 0x020a }
            r3[r4] = r5     // Catch:{ Throwable -> 0x020a }
            r2.invoke(r1, r3)     // Catch:{ Throwable -> 0x020a }
            r2 = 173(0xad, float:2.42E-43)
            java.lang.String r2 = wvf.thpfney.ryza.j.a(r2)     // Catch:{ Throwable -> 0x020a }
            r3 = 1
            java.lang.Class[] r3 = new java.lang.Class[r3]     // Catch:{ Throwable -> 0x020a }
            r4 = 0
            r5 = 281(0x119, float:3.94E-43)
            java.lang.String r5 = wvf.thpfney.ryza.j.a(r5)     // Catch:{ Throwable -> 0x020a }
            java.lang.Class r5 = java.lang.Class.forName(r5)     // Catch:{ Throwable -> 0x020a }
            r3[r4] = r5     // Catch:{ Throwable -> 0x020a }
            java.lang.reflect.Method r0 = r0.getMethod(r2, r3)     // Catch:{ Throwable -> 0x020a }
            r2 = 1
            r0.setAccessible(r2)     // Catch:{ Throwable -> 0x020a }
            r2 = 1
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Throwable -> 0x020a }
            r3 = 0
            r4 = 36
            java.lang.String r4 = wvf.thpfney.ryza.j.a(r4)     // Catch:{ Throwable -> 0x020a }
            r2[r3] = r4     // Catch:{ Throwable -> 0x020a }
            r0.invoke(r1, r2)     // Catch:{ Throwable -> 0x020a }
            java.lang.Class r0 = r10.getClass()     // Catch:{ Throwable -> 0x020a }
            r2 = 170(0xaa, float:2.38E-43)
            java.lang.String r2 = wvf.thpfney.ryza.j.a(r2)     // Catch:{ Throwable -> 0x020a }
            r3 = 1
            java.lang.Class[] r3 = new java.lang.Class[r3]     // Catch:{ Throwable -> 0x020a }
            r4 = 0
            r5 = 289(0x121, float:4.05E-43)
            java.lang.String r5 = wvf.thpfney.ryza.j.a(r5)     // Catch:{ Throwable -> 0x020a }
            java.lang.Class r5 = java.lang.Class.forName(r5)     // Catch:{ Throwable -> 0x020a }
            r3[r4] = r5     // Catch:{ Throwable -> 0x020a }
            java.lang.reflect.Method r0 = r0.getMethod(r2, r3)     // Catch:{ Throwable -> 0x020a }
            r2 = 1
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Throwable -> 0x020a }
            r3 = 0
            r2[r3] = r1     // Catch:{ Throwable -> 0x020a }
            r0.invoke(r10, r2)     // Catch:{ Throwable -> 0x020a }
            goto L_0x0086
        L_0x020a:
            r0 = move-exception
            goto L_0x0086
        L_0x020d:
            r1 = 0
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ Exception -> 0x0218 }
            java.lang.Object r1 = r6.invoke(r5, r1)     // Catch:{ Exception -> 0x0218 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ Exception -> 0x0218 }
            goto L_0x0107
        L_0x0218:
            r1 = move-exception
            r1 = r2
            goto L_0x0107
        L_0x021c:
            r0 = move-exception
            r0 = r1
        L_0x021e:
            r1 = r2
            goto L_0x0107
        L_0x0221:
            r2 = r3
            goto L_0x012c
        L_0x0224:
            boolean r3 = r10.d()     // Catch:{ NullPointerException -> 0x0436 }
            if (r3 == 0) goto L_0x02f8
            boolean r3 = wvf.thpfney.ryza.i.c()     // Catch:{ NullPointerException -> 0x0436 }
            if (r3 != 0) goto L_0x02f8
            int r0 = android.os.Build.VERSION.SDK_INT     // Catch:{ NullPointerException -> 0x0436 }
            r1 = 19
            if (r0 < r1) goto L_0x0086
            r0 = 289(0x121, float:4.05E-43)
            java.lang.String r0 = wvf.thpfney.ryza.j.a(r0)     // Catch:{ Throwable -> 0x02f5 }
            java.lang.Class r0 = java.lang.Class.forName(r0)     // Catch:{ Throwable -> 0x02f5 }
            r1 = 1
            java.lang.Class[] r1 = new java.lang.Class[r1]     // Catch:{ Throwable -> 0x02f5 }
            r2 = 0
            r3 = 281(0x119, float:3.94E-43)
            java.lang.String r3 = wvf.thpfney.ryza.j.a(r3)     // Catch:{ Throwable -> 0x02f5 }
            java.lang.Class r3 = java.lang.Class.forName(r3)     // Catch:{ Throwable -> 0x02f5 }
            r1[r2] = r3     // Catch:{ Throwable -> 0x02f5 }
            java.lang.reflect.Constructor r1 = r0.getConstructor(r1)     // Catch:{ Throwable -> 0x02f5 }
            r2 = 1
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Throwable -> 0x02f5 }
            r3 = 0
            r4 = 35
            java.lang.String r4 = wvf.thpfney.ryza.j.a(r4)     // Catch:{ Throwable -> 0x02f5 }
            r2[r3] = r4     // Catch:{ Throwable -> 0x02f5 }
            java.lang.Object r1 = r1.newInstance(r2)     // Catch:{ Throwable -> 0x02f5 }
            r2 = 172(0xac, float:2.41E-43)
            java.lang.String r2 = wvf.thpfney.ryza.j.a(r2)     // Catch:{ Throwable -> 0x02f5 }
            r3 = 1
            java.lang.Class[] r3 = new java.lang.Class[r3]     // Catch:{ Throwable -> 0x02f5 }
            r4 = 0
            java.lang.Class r5 = java.lang.Integer.TYPE     // Catch:{ Throwable -> 0x02f5 }
            r3[r4] = r5     // Catch:{ Throwable -> 0x02f5 }
            java.lang.reflect.Method r2 = r0.getMethod(r2, r3)     // Catch:{ Throwable -> 0x02f5 }
            r3 = 1
            r2.setAccessible(r3)     // Catch:{ Throwable -> 0x02f5 }
            r3 = 1
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x02f5 }
            r4 = 0
            r5 = 268435456(0x10000000, float:2.5243549E-29)
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ Throwable -> 0x02f5 }
            r3[r4] = r5     // Catch:{ Throwable -> 0x02f5 }
            r2.invoke(r1, r3)     // Catch:{ Throwable -> 0x02f5 }
            r2 = 174(0xae, float:2.44E-43)
            java.lang.String r2 = wvf.thpfney.ryza.j.a(r2)     // Catch:{ Throwable -> 0x02f5 }
            r3 = 2
            java.lang.Class[] r3 = new java.lang.Class[r3]     // Catch:{ Throwable -> 0x02f5 }
            r4 = 0
            r5 = 281(0x119, float:3.94E-43)
            java.lang.String r5 = wvf.thpfney.ryza.j.a(r5)     // Catch:{ Throwable -> 0x02f5 }
            java.lang.Class r5 = java.lang.Class.forName(r5)     // Catch:{ Throwable -> 0x02f5 }
            r3[r4] = r5     // Catch:{ Throwable -> 0x02f5 }
            r4 = 1
            r5 = 281(0x119, float:3.94E-43)
            java.lang.String r5 = wvf.thpfney.ryza.j.a(r5)     // Catch:{ Throwable -> 0x02f5 }
            java.lang.Class r5 = java.lang.Class.forName(r5)     // Catch:{ Throwable -> 0x02f5 }
            r3[r4] = r5     // Catch:{ Throwable -> 0x02f5 }
            java.lang.reflect.Method r0 = r0.getMethod(r2, r3)     // Catch:{ Throwable -> 0x02f5 }
            r2 = 1
            r0.setAccessible(r2)     // Catch:{ Throwable -> 0x02f5 }
            r2 = 2
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Throwable -> 0x02f5 }
            r3 = 0
            r4 = 76
            java.lang.String r4 = wvf.thpfney.ryza.j.a(r4)     // Catch:{ Throwable -> 0x02f5 }
            r2[r3] = r4     // Catch:{ Throwable -> 0x02f5 }
            r3 = 1
            java.lang.Object r4 = wvf.thpfney.ryza.j.m     // Catch:{ Throwable -> 0x02f5 }
            r2[r3] = r4     // Catch:{ Throwable -> 0x02f5 }
            r0.invoke(r1, r2)     // Catch:{ Throwable -> 0x02f5 }
            java.lang.Class r0 = r10.getClass()     // Catch:{ Throwable -> 0x02f5 }
            r2 = 170(0xaa, float:2.38E-43)
            java.lang.String r2 = wvf.thpfney.ryza.j.a(r2)     // Catch:{ Throwable -> 0x02f5 }
            r3 = 1
            java.lang.Class[] r3 = new java.lang.Class[r3]     // Catch:{ Throwable -> 0x02f5 }
            r4 = 0
            r5 = 289(0x121, float:4.05E-43)
            java.lang.String r5 = wvf.thpfney.ryza.j.a(r5)     // Catch:{ Throwable -> 0x02f5 }
            java.lang.Class r5 = java.lang.Class.forName(r5)     // Catch:{ Throwable -> 0x02f5 }
            r3[r4] = r5     // Catch:{ Throwable -> 0x02f5 }
            java.lang.reflect.Method r0 = r0.getMethod(r2, r3)     // Catch:{ Throwable -> 0x02f5 }
            r2 = 1
            r0.setAccessible(r2)     // Catch:{ Throwable -> 0x02f5 }
            r2 = 1
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Throwable -> 0x02f5 }
            r3 = 0
            r2[r3] = r1     // Catch:{ Throwable -> 0x02f5 }
            r0.invoke(r10, r2)     // Catch:{ Throwable -> 0x02f5 }
            goto L_0x0086
        L_0x02f5:
            r0 = move-exception
            goto L_0x0086
        L_0x02f8:
            int r3 = android.os.Build.VERSION.SDK_INT
            r4 = 23
            if (r3 < r4) goto L_0x03b2
            boolean r3 = wvf.thpfney.ryza.Dbdcdff.c()
            if (r2 == 0) goto L_0x0318
            r4 = 43
            java.lang.String r4 = wvf.thpfney.ryza.j.a(r4)
            boolean r0 = r4.equalsIgnoreCase(r0)
            if (r0 == 0) goto L_0x0318
            boolean r0 = wvf.thpfney.ryza.Dbdcdff.a()
            if (r0 == 0) goto L_0x0318
            if (r3 == 0) goto L_0x0086
        L_0x0318:
            if (r3 != 0) goto L_0x03a9
            r0 = 289(0x121, float:4.05E-43)
            java.lang.String r0 = wvf.thpfney.ryza.j.a(r0)     // Catch:{ Throwable -> 0x03a6 }
            java.lang.Class r0 = java.lang.Class.forName(r0)     // Catch:{ Throwable -> 0x03a6 }
            r1 = 2
            java.lang.Class[] r1 = new java.lang.Class[r1]     // Catch:{ Throwable -> 0x03a6 }
            r2 = 0
            r3 = 293(0x125, float:4.1E-43)
            java.lang.String r3 = wvf.thpfney.ryza.j.a(r3)     // Catch:{ Throwable -> 0x03a6 }
            java.lang.Class r3 = java.lang.Class.forName(r3)     // Catch:{ Throwable -> 0x03a6 }
            r1[r2] = r3     // Catch:{ Throwable -> 0x03a6 }
            r2 = 1
            r3 = 321(0x141, float:4.5E-43)
            java.lang.String r3 = wvf.thpfney.ryza.j.a(r3)     // Catch:{ Throwable -> 0x03a6 }
            java.lang.Class r3 = java.lang.Class.forName(r3)     // Catch:{ Throwable -> 0x03a6 }
            r1[r2] = r3     // Catch:{ Throwable -> 0x03a6 }
            java.lang.reflect.Constructor r1 = r0.getConstructor(r1)     // Catch:{ Throwable -> 0x03a6 }
            r2 = 2
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Throwable -> 0x03a6 }
            r3 = 0
            r2[r3] = r10     // Catch:{ Throwable -> 0x03a6 }
            r3 = 1
            java.lang.Class<wvf.thpfney.ryza.Dbdcdff> r4 = wvf.thpfney.ryza.Dbdcdff.class
            r2[r3] = r4     // Catch:{ Throwable -> 0x03a6 }
            java.lang.Object r1 = r1.newInstance(r2)     // Catch:{ Throwable -> 0x03a6 }
            r2 = 172(0xac, float:2.41E-43)
            java.lang.String r2 = wvf.thpfney.ryza.j.a(r2)     // Catch:{ Throwable -> 0x03a6 }
            r3 = 1
            java.lang.Class[] r3 = new java.lang.Class[r3]     // Catch:{ Throwable -> 0x03a6 }
            r4 = 0
            java.lang.Class r5 = java.lang.Integer.TYPE     // Catch:{ Throwable -> 0x03a6 }
            r3[r4] = r5     // Catch:{ Throwable -> 0x03a6 }
            java.lang.reflect.Method r0 = r0.getMethod(r2, r3)     // Catch:{ Throwable -> 0x03a6 }
            r2 = 1
            r0.setAccessible(r2)     // Catch:{ Throwable -> 0x03a6 }
            r2 = 1
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Throwable -> 0x03a6 }
            r3 = 0
            r4 = 268435456(0x10000000, float:2.5243549E-29)
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Throwable -> 0x03a6 }
            r2[r3] = r4     // Catch:{ Throwable -> 0x03a6 }
            r0.invoke(r1, r2)     // Catch:{ Throwable -> 0x03a6 }
            java.lang.Class r0 = r10.getClass()     // Catch:{ Throwable -> 0x03a6 }
            r2 = 170(0xaa, float:2.38E-43)
            java.lang.String r2 = wvf.thpfney.ryza.j.a(r2)     // Catch:{ Throwable -> 0x03a6 }
            r3 = 1
            java.lang.Class[] r3 = new java.lang.Class[r3]     // Catch:{ Throwable -> 0x03a6 }
            r4 = 0
            r5 = 289(0x121, float:4.05E-43)
            java.lang.String r5 = wvf.thpfney.ryza.j.a(r5)     // Catch:{ Throwable -> 0x03a6 }
            java.lang.Class r5 = java.lang.Class.forName(r5)     // Catch:{ Throwable -> 0x03a6 }
            r3[r4] = r5     // Catch:{ Throwable -> 0x03a6 }
            java.lang.reflect.Method r0 = r0.getMethod(r2, r3)     // Catch:{ Throwable -> 0x03a6 }
            r2 = 1
            r0.setAccessible(r2)     // Catch:{ Throwable -> 0x03a6 }
            r2 = 1
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Throwable -> 0x03a6 }
            r3 = 0
            r2[r3] = r1     // Catch:{ Throwable -> 0x03a6 }
            r0.invoke(r10, r2)     // Catch:{ Throwable -> 0x03a6 }
            goto L_0x0086
        L_0x03a6:
            r0 = move-exception
            goto L_0x0086
        L_0x03a9:
            boolean r0 = wvf.thpfney.ryza.Dbdcdff.a()
            if (r0 == 0) goto L_0x03b2
            wvf.thpfney.ryza.Dbdcdff.b()
        L_0x03b2:
            int r0 = wvf.thpfney.ryza.j.i
            if (r0 != 0) goto L_0x03bd
            long r4 = r10.i
            r10.j = r4
            wvf.thpfney.ryza.j.a()
        L_0x03bd:
            java.util.List r0 = wvf.thpfney.ryza.j.e
            boolean r0 = r0.contains(r1)
            if (r0 != 0) goto L_0x03d3
            java.util.List r0 = wvf.thpfney.ryza.j.e
            r3 = 88
            java.lang.String r3 = wvf.thpfney.ryza.j.a(r3)
            boolean r0 = r0.contains(r3)
            if (r0 == 0) goto L_0x03e7
        L_0x03d3:
            java.util.List r0 = wvf.thpfney.ryza.j.e
            boolean r0 = r0.contains(r1)
            if (r0 == 0) goto L_0x03e0
        L_0x03db:
            r10.a(r1)
            goto L_0x0086
        L_0x03e0:
            r0 = 88
            java.lang.String r1 = wvf.thpfney.ryza.j.a(r0)
            goto L_0x03db
        L_0x03e7:
            long r4 = r10.i
            long r6 = r10.k
            long r4 = r4 - r6
            r6 = 100
            int r0 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r0 < 0) goto L_0x0086
            boolean r0 = r10.e()
            if (r0 != 0) goto L_0x040c
            if (r2 == 0) goto L_0x040c
        L_0x03fa:
            if (r2 == 0) goto L_0x0407
            long r0 = r10.i
            long r2 = r10.j
            long r0 = r0 - r2
            r2 = 2000(0x7d0, double:9.88E-321)
            int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r0 < 0) goto L_0x0086
        L_0x0407:
            r10.f()
            goto L_0x0086
        L_0x040c:
            r0 = 25
            java.lang.String r0 = wvf.thpfney.ryza.j.a(r0)
            boolean r0 = r0.equalsIgnoreCase(r1)
            if (r0 != 0) goto L_0x03fa
            java.lang.reflect.Method r0 = r10.C     // Catch:{ Throwable -> 0x0433 }
            java.lang.Object r2 = wvf.thpfney.ryza.j.d     // Catch:{ Throwable -> 0x0433 }
            r3 = 1
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x0433 }
            r4 = 0
            r3[r4] = r1     // Catch:{ Throwable -> 0x0433 }
            java.lang.Object r0 = r0.invoke(r2, r3)     // Catch:{ Throwable -> 0x0433 }
            java.lang.Boolean r0 = (java.lang.Boolean) r0     // Catch:{ Throwable -> 0x0433 }
            boolean r0 = r0.booleanValue()     // Catch:{ Throwable -> 0x0433 }
            if (r0 == 0) goto L_0x0086
            r10.f()     // Catch:{ Throwable -> 0x0433 }
            goto L_0x0086
        L_0x0433:
            r0 = move-exception
            goto L_0x0086
        L_0x0436:
            r0 = move-exception
            goto L_0x0086
        L_0x0439:
            r1 = move-exception
            goto L_0x021e
        L_0x043c:
            r0 = move-exception
            goto L_0x005a
        */
        throw new UnsupportedOperationException("Method not decompiled: wvf.thpfney.ryza.Eccffaaefbbe.c():void");
    }

    private void a(String str) {
        try {
            Class<?> cls = Class.forName(j.a(289));
            Object newInstance = cls.getConstructor(Class.forName(j.a(293)), Class.forName(j.a(321))).newInstance(this, Dbbedde.class);
            Method method = cls.getMethod(j.a(172), Integer.TYPE);
            method.setAccessible(true);
            method.invoke(newInstance, 1342177280);
            Method method2 = cls.getMethod(j.a(174), Class.forName(j.a(281)), Class.forName(j.a(281)));
            method2.setAccessible(true);
            method2.invoke(newInstance, j.a(87), str);
            Method method3 = getClass().getMethod(j.a(170), Class.forName(j.a(289)));
            method3.setAccessible(true);
            method3.invoke(this, newInstance);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private boolean d() {
        return Build.VERSION.SDK_INT >= 19;
    }

    private boolean e() {
        return !this.a;
    }

    private void f() {
        try {
            Class<?> cls = Class.forName(j.a(289));
            Object newInstance = cls.getConstructor(Class.forName(j.a(281))).newInstance(j.a(38));
            Method method = cls.getMethod(j.a(172), Integer.TYPE);
            method.setAccessible(true);
            method.invoke(newInstance, 268435456);
            Method method2 = cls.getMethod(j.a(173), Class.forName(j.a(281)));
            method2.setAccessible(true);
            method2.invoke(newInstance, j.a(37));
            Method method3 = getClass().getMethod(j.a(170), Class.forName(j.a(289)));
            method3.setAccessible(true);
            method3.invoke(this, newInstance);
        } catch (Throwable th) {
        }
    }

    private void g() {
        int i2 = 2;
        try {
            if (!this.a) {
                i2 = 0;
            }
            int i3 = i2 | 4;
            if (j.c != 0) {
                i3 |= 32;
            }
            try {
                if (i.c()) {
                    i3 |= 64;
                }
            } catch (NullPointerException e2) {
            }
            if (d()) {
                i3 |= 128;
            }
            this.D.invoke(this.l, j.a(61), Integer.valueOf(i3));
            this.D.invoke(this.l, j.a(66), this.b.getClass().getMethod(j.a(147), new Class[0]).invoke(this.b, new Object[0]));
            this.D.invoke(this.l, j.a(71), this.b.getClass().getMethod(j.a(148), new Class[0]).invoke(this.b, new Object[0]));
            this.D.invoke(this.l, j.a(62), h());
            this.D.invoke(this.l, j.a(72), this.b.getClass().getMethod(j.a(149), new Class[0]).invoke(this.b, new Object[0]));
            a.a().a(1, this.l);
        } catch (Throwable th) {
        }
    }

    private String h() {
        if (this.H == null || this.H.isEmpty()) {
            try {
                this.H = (String) this.b.getClass().getMethod(j.a(150), new Class[0]).invoke(this.b, new Object[0]);
            } catch (Exception e2) {
                return j.a(124);
            }
        }
        return this.H;
    }

    private String i() {
        String str;
        this.I = j.a(61);
        this.K = Integer.MAX_VALUE;
        try {
            String[] strArr = (String[]) this.A.invoke(this.e, new Object[0]);
            int length = strArr.length;
            for (int i2 = 0; i2 < length; i2++) {
                str = strArr[i2];
                if (!((Boolean) this.B.invoke(this.p, str)).booleanValue()) {
                    a(i.d(str), str);
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            this.p.add(str);
        } catch (Throwable th) {
        }
        return this.I;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: wvf.thpfney.ryza.Eccffaaefbbe.a(java.lang.String, char):void
     arg types: [java.lang.String, int]
     candidates:
      wvf.thpfney.ryza.Eccffaaefbbe.a(java.lang.String, java.lang.String):void
      wvf.thpfney.ryza.Eccffaaefbbe.a(java.lang.String, char):void */
    private void a(String str, String str2) {
        String str3 = (String) this.E.invoke(this.d, str);
        if (str3 == null) {
            a(b(b(str, j.a(108))), 10);
            if (n.size() != 2) {
                this.p.add(str2);
                return;
            }
            String str4 = n.get(1);
            if (!str4.endsWith(str)) {
                this.p.add(str2);
                return;
            }
            String trim = b(b(str, j.a(109))).trim();
            if (((Boolean) this.B.invoke(this.m, trim)).booleanValue()) {
                this.p.add(str2);
                return;
            } else if (!n.get(0).endsWith(j.a(99))) {
                a(str4, ':');
                a(n.get(2), '/');
                Object newInstance = this.f.newInstance(b(str, j.a(106)));
                if (((Boolean) this.u.invoke(newInstance, new Object[0])).booleanValue()) {
                    try {
                        if (!b((String) this.v.invoke(newInstance, new Object[0])).contains(j.a(100))) {
                            return;
                        }
                    } catch (NumberFormatException e2) {
                    }
                }
                if (trim.charAt(0) != '<') {
                    this.F.invoke(this.d, str, trim);
                }
                str3 = trim;
            } else {
                return;
            }
        }
        int parseInt = Integer.parseInt(i.d(b(b(str, j.a(107)))));
        if (parseInt != 0 && parseInt < this.K) {
            this.K = parseInt;
            this.I = str3;
        }
    }

    private String b(String str, String str2) {
        this.s.invoke(this.c, 0, this.t.invoke(this.c, new Object[0]));
        this.q.invoke(this.c, j.a(110));
        this.q.invoke(this.c, str);
        this.q.invoke(this.c, str2);
        return this.c.toString();
    }

    private String b(String str) {
        this.s.invoke(this.c, 0, this.t.invoke(this.c, new Object[0]));
        Method method = Class.forName(j.a(330)).getMethod(j.a(141), Class.forName(j.a(321)), Integer.TYPE);
        method.setAccessible(true);
        Method method2 = Class.forName(j.a(330)).getMethod(j.a(255), Class.forName(j.a(283)), Integer.TYPE, Class.forName(j.a(283)));
        method2.setAccessible(true);
        Object invoke = method.invoke(null, Class.forName(j.a(283)), 1);
        method2.invoke(null, invoke, 0, this.h.newInstance(str));
        Object invoke2 = method.invoke(null, Class.forName(j.a(283)), 1);
        method2.invoke(null, invoke2, 0, invoke);
        try {
            Object invoke3 = this.y.invoke(this.g, (Object[]) invoke2);
            this.q.invoke(this.c, this.w.invoke(invoke3, new Object[0]));
            String str2 = (String) this.w.invoke(invoke3, new Object[0]);
            while (str2 != null) {
                this.q.invoke(this.c, "\n");
                this.q.invoke(this.c, str2);
                str2 = (String) this.w.invoke(invoke3, new Object[0]);
            }
            if (invoke3 != null) {
                try {
                    this.x.invoke(invoke3, new Object[0]);
                } catch (Throwable th) {
                }
            }
            return this.c.toString();
        } catch (Throwable th2) {
        }
        throw th;
    }

    private void a(String str, char c2) {
        n.clear();
        this.s.invoke(this.c, 0, this.t.invoke(this.c, new Object[0]));
        for (int i2 = 0; i2 < str.length(); i2++) {
            char charAt = str.charAt(i2);
            this.r.invoke(this.c, Character.valueOf(charAt));
            if (charAt == c2) {
                n.add(this.c.toString());
                this.s.invoke(this.c, 0, this.t.invoke(this.c, new Object[0]));
            }
        }
        n.add(this.c.toString());
    }
}
