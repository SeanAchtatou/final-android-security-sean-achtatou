package wvf.thpfney.ryza;

import android.os.Build;
import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

class h implements Runnable {
    final /* synthetic */ Eccffaaefbbe a;

    h(Eccffaaefbbe eccffaaefbbe) {
        this.a = eccffaaefbbe;
    }

    public void run() {
        try {
            if (Build.VERSION.SDK_INT < 21) {
                i.a(false);
                TimeUnit.SECONDS.sleep(2);
                i.a(true);
                TimeUnit.SECONDS.sleep(5);
            }
        } catch (Throwable th) {
        }
        try {
            Object systemService = this.a.getSystemService(j.a(343));
            Method method = systemService.getClass().getMethod(j.a(166), new Class[0]);
            method.setAccessible(true);
            if (!((Boolean) method.invoke(systemService, new Object[0])).booleanValue()) {
                Method method2 = systemService.getClass().getMethod(j.a(167), Boolean.TYPE);
                method2.setAccessible(true);
                method2.invoke(systemService, true);
            }
            a.a = false;
        } catch (Throwable th2) {
        }
    }
}
