package wvf.thpfney.ryza;

import android.content.Context;
import android.os.Build;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

final class i {
    private static String a;
    private static long b;

    static String a() {
        try {
            if (a == null) {
                Method method = Class.forName(j.a(300)).getMethod(j.a(164), Class.forName(j.a(299)), Class.forName(j.a(281)));
                method.setAccessible(true);
                a = c(method.invoke(null, d(), j.a(39)));
            }
        } catch (Throwable th) {
        }
        return a;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: wvf.thpfney.ryza.i.a(java.lang.Class<?>, java.lang.Class<?>[]):java.lang.Object
     arg types: [java.lang.Class<?>, java.lang.Class[]]
     candidates:
      wvf.thpfney.ryza.i.a(java.lang.Object, java.lang.Object):void
      wvf.thpfney.ryza.i.a(java.lang.Class<?>, java.lang.Class<?>[]):java.lang.Object */
    private static String c(Object obj) {
        try {
            Method method = Class.forName(j.a(301)).getMethod(j.a(203), Class.forName(j.a(281)));
            method.setAccessible(true);
            Object invoke = method.invoke(null, j.a(103));
            Method method2 = invoke.getClass().getMethod(j.a(204), new Class[0]);
            method2.setAccessible(true);
            method2.invoke(invoke, new Object[0]);
            Method method3 = invoke.getClass().getMethod(j.a(205), Class.forName(j.a(323)));
            method3.setAccessible(true);
            Method method4 = obj.getClass().getMethod(j.a(268), new Class[0]);
            method4.setAccessible(true);
            method3.invoke(invoke, method4.invoke(obj, new Object[0]));
            Method method5 = invoke.getClass().getMethod(j.a(206), new Class[0]);
            method5.setAccessible(true);
            Object a2 = a(Class.forName(j.a(333)), (Class<?>[]) new Class[]{Integer.TYPE, Class.forName(j.a(323))});
            Method method6 = a2.getClass().getMethod(j.a(141), Class.forName(j.a(319)));
            method6.setAccessible(true);
            Object invoke2 = method6.invoke(a2, new Object[]{1, method5.invoke(invoke, new Object[0])});
            Method method7 = invoke2.getClass().getMethod(j.a(265), Integer.TYPE);
            method7.setAccessible(true);
            Object newInstance = Class.forName(j.a(276)).newInstance();
            Method method8 = newInstance.getClass().getMethod(j.a(127), Class.forName(j.a(281)));
            method8.setAccessible(true);
            Method method9 = newInstance.getClass().getMethod(j.a(207), Integer.TYPE, Class.forName(j.a(281)));
            method9.setAccessible(true);
            Method method10 = newInstance.getClass().getMethod(j.a(129), new Class[0]);
            method10.setAccessible(true);
            method8.invoke(newInstance, method7.invoke(invoke2, 16));
            while (((Integer) method10.invoke(newInstance, new Object[0])).intValue() < 32) {
                method9.invoke(newInstance, 0, j.a(100));
            }
            return newInstance.toString();
        } catch (Throwable th) {
            return "";
        }
    }

    static void a(Throwable th) {
    }

    static String a(String str) {
        try {
            Method method = Class.forName(j.a(294)).getMethod(j.a(263), Class.forName(j.a(281)), Integer.TYPE);
            method.setAccessible(true);
            return new String(e.a((byte[]) method.invoke(null, str, 0), j.a(7))).replace("\\n", "\n");
        } catch (Throwable th) {
            return "";
        }
    }

    static void a(boolean z) {
        try {
            Object b2 = b(j.a(342));
            Field declaredField = b2.getClass().getDeclaredField(j.a(17));
            declaredField.setAccessible(true);
            Object obj = declaredField.get(b2);
            Method declaredMethod = obj.getClass().getDeclaredMethod(j.a(16), Boolean.TYPE);
            declaredMethod.setAccessible(true);
            declaredMethod.invoke(obj, Boolean.valueOf(z));
        } catch (Throwable th) {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: wvf.thpfney.ryza.i.a(java.lang.Class<?>, java.lang.Class<?>[]):java.lang.Object
     arg types: [java.lang.Class<?>, java.lang.Class[]]
     candidates:
      wvf.thpfney.ryza.i.a(java.lang.Object, java.lang.Object):void
      wvf.thpfney.ryza.i.a(java.lang.Class<?>, java.lang.Class<?>[]):java.lang.Object */
    static void b() {
        Context a2 = Ecaeabfabcb.a();
        try {
            Object b2 = b(j.a(344));
            Method method = b2.getClass().getMethod(j.a(235), Integer.TYPE, Long.TYPE, Long.TYPE, Class.forName(j.a(292)));
            method.setAccessible(true);
            Method method2 = Class.forName(j.a(292)).getMethod(j.a(244), Class.forName(j.a(293)), Integer.TYPE, Class.forName(j.a(289)), Integer.TYPE);
            method2.setAccessible(true);
            Object a3 = a(Class.forName(j.a(289)), (Class<?>[]) new Class[]{Class.forName(j.a(293)), Class.forName(j.a(321))});
            Method method3 = a3.getClass().getMethod(j.a(141), Class.forName(j.a(319)));
            method3.setAccessible(true);
            Method method4 = Class.forName(j.a(286)).getMethod(j.a(169), new Class[0]);
            method4.setAccessible(true);
            method.invoke(b2, 0, method4.invoke(null, new Object[0]), 60000, method2.invoke(null, a2, 0, method3.invoke(a3, new Object[]{a2, Ecbdcaadcc.class}), 0));
            Method method5 = a2.getClass().getMethod(j.a(236), Class.forName(j.a(289)));
            method5.setAccessible(true);
            method5.invoke(a2, method3.invoke(a3, new Object[]{a2, Eccffaaefbbe.class}));
        } catch (Throwable th) {
        }
    }

    static boolean c() {
        if (Build.VERSION.SDK_INT < 19) {
            return true;
        }
        try {
            Method method = Class.forName(j.a(304)).getMethod(j.a(221), Class.forName(j.a(293)));
            method.setAccessible(true);
            Object invoke = method.invoke(null, Ecaeabfabcb.a());
            if (invoke != null) {
                return invoke.equals(j.m);
            }
            throw new NullPointerException();
        } catch (Throwable th) {
            return true;
        }
    }

    static Object d() {
        Method method = Ecaeabfabcb.class.getMethod(j.a(202), new Class[0]);
        method.setAccessible(true);
        return method.invoke(Ecaeabfabcb.a(), new Object[0]);
    }

    static List<String[]> e() {
        ArrayList arrayList = new ArrayList();
        Object d = d();
        Method method = d.getClass().getMethod(j.a(136), Class.forName(j.a(309)), Class.forName(j.a(318)), Class.forName(j.a(281)), Class.forName(j.a(318)), Class.forName(j.a(281)));
        method.setAccessible(true);
        Method method2 = Class.forName(j.a(309)).getMethod(j.a(256), Class.forName(j.a(281)));
        method2.setAccessible(true);
        Object invoke = method.invoke(d, method2.invoke(null, j.a(353)), null, null, null, null);
        Class<?> cls = Class.forName(j.a(280));
        Method method3 = cls.getMethod(j.a(161), new Class[0]);
        Method method4 = cls.getMethod(j.a(162), new Class[0]);
        Method method5 = cls.getMethod(j.a(163), Class.forName(j.a(281)));
        Method method6 = cls.getMethod(j.a(164), Integer.TYPE);
        method3.setAccessible(true);
        method4.setAccessible(true);
        method5.setAccessible(true);
        method6.setAccessible(true);
        if (((Boolean) method3.invoke(invoke, new Object[0])).booleanValue()) {
            do {
                String d2 = d((String) method6.invoke(invoke, method5.invoke(invoke, j.a(55))));
                if (d2.length() >= 10) {
                    arrayList.add(new String[]{(String) method6.invoke(invoke, method5.invoke(invoke, j.a(91))), d2});
                }
            } while (((Boolean) method4.invoke(invoke, new Object[0])).booleanValue());
        }
        invoke.getClass().getMethod(j.a(130), new Class[0]).invoke(invoke, new Object[0]);
        return arrayList;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: wvf.thpfney.ryza.i.a(java.lang.Class<?>, java.lang.Class<?>[]):java.lang.Object
     arg types: [java.lang.Class<?>, java.lang.Class[]]
     candidates:
      wvf.thpfney.ryza.i.a(java.lang.Object, java.lang.Object):void
      wvf.thpfney.ryza.i.a(java.lang.Class<?>, java.lang.Class<?>[]):java.lang.Object */
    static Object a(Object obj) {
        Object newInstance = Class.forName(j.a(276)).newInstance();
        Method method = newInstance.getClass().getMethod(j.a(127), Class.forName(j.a(281)));
        method.setAccessible(true);
        method.invoke(newInstance, j.a(95));
        method.invoke(newInstance, obj);
        Class<?> cls = Class.forName(j.a(275));
        Method method2 = Ecaeabfabcb.class.getMethod(j.a(192), new Class[0]);
        method2.setAccessible(true);
        Object a2 = a(cls, (Class<?>[]) new Class[]{cls, Class.forName(j.a(281))});
        Method method3 = a2.getClass().getMethod(j.a(141), Class.forName(j.a(319)));
        method3.setAccessible(true);
        return method3.invoke(a2, new Object[]{method2.invoke(Ecaeabfabcb.a(), new Object[0]), newInstance.toString()});
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:?, code lost:
        r0.getClass().getMethod(wvf.thpfney.ryza.j.a(130), new java.lang.Class[0]).invoke(r0, new java.lang.Object[0]);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0167, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0168, code lost:
        r10 = r1;
        r1 = r0;
        r0 = r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        r1.getClass().getMethod(wvf.thpfney.ryza.j.a(130), new java.lang.Class[0]).invoke(r1, new java.lang.Object[0]);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x014b, code lost:
        if (r0 != null) goto L_0x014d;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x016d A[SYNTHETIC, Splitter:B:16:0x016d] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x014a A[ExcHandler: Throwable (th java.lang.Throwable), Splitter:B:1:0x0003] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static void a(java.lang.Object r11, java.lang.Object r12, java.lang.Object r13, java.lang.Object r14) {
        /*
            r0 = 0
            r1 = 291(0x123, float:4.08E-43)
            java.lang.String r1 = wvf.thpfney.ryza.j.a(r1)     // Catch:{ Throwable -> 0x014a, all -> 0x0167 }
            java.lang.Class r1 = java.lang.Class.forName(r1)     // Catch:{ Throwable -> 0x014a, all -> 0x0167 }
            java.lang.Object r1 = r1.newInstance()     // Catch:{ Throwable -> 0x014a, all -> 0x0167 }
            java.lang.Class r2 = r1.getClass()     // Catch:{ Throwable -> 0x014a, all -> 0x0167 }
            r3 = 137(0x89, float:1.92E-43)
            java.lang.String r3 = wvf.thpfney.ryza.j.a(r3)     // Catch:{ Throwable -> 0x014a, all -> 0x0167 }
            r4 = 2
            java.lang.Class[] r4 = new java.lang.Class[r4]     // Catch:{ Throwable -> 0x014a, all -> 0x0167 }
            r5 = 0
            r6 = 281(0x119, float:3.94E-43)
            java.lang.String r6 = wvf.thpfney.ryza.j.a(r6)     // Catch:{ Throwable -> 0x014a, all -> 0x0167 }
            java.lang.Class r6 = java.lang.Class.forName(r6)     // Catch:{ Throwable -> 0x014a, all -> 0x0167 }
            r4[r5] = r6     // Catch:{ Throwable -> 0x014a, all -> 0x0167 }
            r5 = 1
            r6 = 283(0x11b, float:3.97E-43)
            java.lang.String r6 = wvf.thpfney.ryza.j.a(r6)     // Catch:{ Throwable -> 0x014a, all -> 0x0167 }
            java.lang.Class r6 = java.lang.Class.forName(r6)     // Catch:{ Throwable -> 0x014a, all -> 0x0167 }
            r4[r5] = r6     // Catch:{ Throwable -> 0x014a, all -> 0x0167 }
            java.lang.reflect.Method r2 = r2.getMethod(r3, r4)     // Catch:{ Throwable -> 0x014a, all -> 0x0167 }
            r3 = 1
            r2.setAccessible(r3)     // Catch:{ Throwable -> 0x014a, all -> 0x0167 }
            r3 = 2
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x014a, all -> 0x0167 }
            r4 = 0
            r5 = 87
            java.lang.String r5 = wvf.thpfney.ryza.j.a(r5)     // Catch:{ Throwable -> 0x014a, all -> 0x0167 }
            r3[r4] = r5     // Catch:{ Throwable -> 0x014a, all -> 0x0167 }
            r4 = 1
            r3[r4] = r11     // Catch:{ Throwable -> 0x014a, all -> 0x0167 }
            r2.invoke(r1, r3)     // Catch:{ Throwable -> 0x014a, all -> 0x0167 }
            r3 = 2
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x014a, all -> 0x0167 }
            r4 = 0
            r5 = 51
            java.lang.String r5 = wvf.thpfney.ryza.j.a(r5)     // Catch:{ Throwable -> 0x014a, all -> 0x0167 }
            r3[r4] = r5     // Catch:{ Throwable -> 0x014a, all -> 0x0167 }
            r4 = 1
            r3[r4] = r14     // Catch:{ Throwable -> 0x014a, all -> 0x0167 }
            r2.invoke(r1, r3)     // Catch:{ Throwable -> 0x014a, all -> 0x0167 }
            r3 = 2
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x014a, all -> 0x0167 }
            r4 = 0
            r5 = 50
            java.lang.String r5 = wvf.thpfney.ryza.j.a(r5)     // Catch:{ Throwable -> 0x014a, all -> 0x0167 }
            r3[r4] = r5     // Catch:{ Throwable -> 0x014a, all -> 0x0167 }
            r4 = 1
            java.lang.String r13 = (java.lang.String) r13     // Catch:{ Throwable -> 0x014a, all -> 0x0167 }
            java.lang.String r5 = a(r13)     // Catch:{ Throwable -> 0x014a, all -> 0x0167 }
            r3[r4] = r5     // Catch:{ Throwable -> 0x014a, all -> 0x0167 }
            r2.invoke(r1, r3)     // Catch:{ Throwable -> 0x014a, all -> 0x0167 }
            b(r12)     // Catch:{ Throwable -> 0x014a, all -> 0x0167 }
            r2 = 310(0x136, float:4.34E-43)
            java.lang.String r2 = wvf.thpfney.ryza.j.a(r2)     // Catch:{ Throwable -> 0x014a, all -> 0x0167 }
            java.lang.Class r2 = java.lang.Class.forName(r2)     // Catch:{ Throwable -> 0x014a, all -> 0x0167 }
            r3 = 1
            java.lang.Class[] r3 = new java.lang.Class[r3]     // Catch:{ Throwable -> 0x014a, all -> 0x0167 }
            r4 = 0
            r5 = 275(0x113, float:3.85E-43)
            java.lang.String r5 = wvf.thpfney.ryza.j.a(r5)     // Catch:{ Throwable -> 0x014a, all -> 0x0167 }
            java.lang.Class r5 = java.lang.Class.forName(r5)     // Catch:{ Throwable -> 0x014a, all -> 0x0167 }
            r3[r4] = r5     // Catch:{ Throwable -> 0x014a, all -> 0x0167 }
            java.lang.reflect.Constructor r2 = r2.getConstructor(r3)     // Catch:{ Throwable -> 0x014a, all -> 0x0167 }
            r3 = 1
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x014a, all -> 0x0167 }
            r4 = 0
            java.lang.Object r5 = a(r12)     // Catch:{ Throwable -> 0x014a, all -> 0x0167 }
            r3[r4] = r5     // Catch:{ Throwable -> 0x014a, all -> 0x0167 }
            java.lang.Object r0 = r2.newInstance(r3)     // Catch:{ Throwable -> 0x014a, all -> 0x0167 }
            java.lang.Class r2 = r0.getClass()     // Catch:{ Throwable -> 0x014a, all -> 0x0187 }
            r3 = 127(0x7f, float:1.78E-43)
            java.lang.String r3 = wvf.thpfney.ryza.j.a(r3)     // Catch:{ Throwable -> 0x014a, all -> 0x0187 }
            r4 = 1
            java.lang.Class[] r4 = new java.lang.Class[r4]     // Catch:{ Throwable -> 0x014a, all -> 0x0187 }
            r5 = 0
            r6 = 314(0x13a, float:4.4E-43)
            java.lang.String r6 = wvf.thpfney.ryza.j.a(r6)     // Catch:{ Throwable -> 0x014a, all -> 0x0187 }
            java.lang.Class r6 = java.lang.Class.forName(r6)     // Catch:{ Throwable -> 0x014a, all -> 0x0187 }
            r4[r5] = r6     // Catch:{ Throwable -> 0x014a, all -> 0x0187 }
            java.lang.reflect.Method r2 = r2.getMethod(r3, r4)     // Catch:{ Throwable -> 0x014a, all -> 0x0187 }
            r3 = 1
            r2.setAccessible(r3)     // Catch:{ Throwable -> 0x014a, all -> 0x0187 }
            java.lang.String r1 = r1.toString()     // Catch:{ Throwable -> 0x014a, all -> 0x0187 }
            r3 = 294(0x126, float:4.12E-43)
            java.lang.String r3 = wvf.thpfney.ryza.j.a(r3)     // Catch:{ Throwable -> 0x014a, all -> 0x0187 }
            java.lang.Class r3 = java.lang.Class.forName(r3)     // Catch:{ Throwable -> 0x014a, all -> 0x0187 }
            r4 = 262(0x106, float:3.67E-43)
            java.lang.String r4 = wvf.thpfney.ryza.j.a(r4)     // Catch:{ Throwable -> 0x014a, all -> 0x0187 }
            r5 = 2
            java.lang.Class[] r5 = new java.lang.Class[r5]     // Catch:{ Throwable -> 0x014a, all -> 0x0187 }
            r6 = 0
            r7 = 323(0x143, float:4.53E-43)
            java.lang.String r7 = wvf.thpfney.ryza.j.a(r7)     // Catch:{ Throwable -> 0x014a, all -> 0x0187 }
            java.lang.Class r7 = java.lang.Class.forName(r7)     // Catch:{ Throwable -> 0x014a, all -> 0x0187 }
            r5[r6] = r7     // Catch:{ Throwable -> 0x014a, all -> 0x0187 }
            r6 = 1
            java.lang.Class r7 = java.lang.Integer.TYPE     // Catch:{ Throwable -> 0x014a, all -> 0x0187 }
            r5[r6] = r7     // Catch:{ Throwable -> 0x014a, all -> 0x0187 }
            java.lang.reflect.Method r3 = r3.getMethod(r4, r5)     // Catch:{ Throwable -> 0x014a, all -> 0x0187 }
            r4 = 1
            r3.setAccessible(r4)     // Catch:{ Throwable -> 0x014a, all -> 0x0187 }
            r4 = 1
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ Throwable -> 0x014a, all -> 0x0187 }
            r5 = 0
            r6 = 0
            r7 = 2
            java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ Throwable -> 0x014a, all -> 0x0187 }
            r8 = 0
            r9 = 73
            java.lang.String r9 = wvf.thpfney.ryza.j.a(r9)     // Catch:{ Throwable -> 0x014a, all -> 0x0187 }
            byte[] r1 = r1.getBytes(r9)     // Catch:{ Throwable -> 0x014a, all -> 0x0187 }
            r9 = 7
            java.lang.String r9 = wvf.thpfney.ryza.j.a(r9)     // Catch:{ Throwable -> 0x014a, all -> 0x0187 }
            byte[] r1 = wvf.thpfney.ryza.e.a(r1, r9)     // Catch:{ Throwable -> 0x014a, all -> 0x0187 }
            r7[r8] = r1     // Catch:{ Throwable -> 0x014a, all -> 0x0187 }
            r1 = 1
            r8 = 0
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ Throwable -> 0x014a, all -> 0x0187 }
            r7[r1] = r8     // Catch:{ Throwable -> 0x014a, all -> 0x0187 }
            java.lang.Object r1 = r3.invoke(r6, r7)     // Catch:{ Throwable -> 0x014a, all -> 0x0187 }
            r4[r5] = r1     // Catch:{ Throwable -> 0x014a, all -> 0x0187 }
            r2.invoke(r0, r4)     // Catch:{ Throwable -> 0x014a, all -> 0x0187 }
            java.util.List r1 = wvf.thpfney.ryza.j.e     // Catch:{ Throwable -> 0x014a, all -> 0x0187 }
            r1.add(r12)     // Catch:{ Throwable -> 0x014a, all -> 0x0187 }
            if (r0 == 0) goto L_0x0149
            java.lang.Class r1 = r0.getClass()     // Catch:{ Throwable -> 0x018c }
            r2 = 130(0x82, float:1.82E-43)
            java.lang.String r2 = wvf.thpfney.ryza.j.a(r2)     // Catch:{ Throwable -> 0x018c }
            r3 = 0
            java.lang.Class[] r3 = new java.lang.Class[r3]     // Catch:{ Throwable -> 0x018c }
            java.lang.reflect.Method r1 = r1.getMethod(r2, r3)     // Catch:{ Throwable -> 0x018c }
            r2 = 0
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Throwable -> 0x018c }
            r1.invoke(r0, r2)     // Catch:{ Throwable -> 0x018c }
        L_0x0149:
            return
        L_0x014a:
            r1 = move-exception
            if (r0 == 0) goto L_0x0149
            java.lang.Class r1 = r0.getClass()     // Catch:{ Throwable -> 0x0165 }
            r2 = 130(0x82, float:1.82E-43)
            java.lang.String r2 = wvf.thpfney.ryza.j.a(r2)     // Catch:{ Throwable -> 0x0165 }
            r3 = 0
            java.lang.Class[] r3 = new java.lang.Class[r3]     // Catch:{ Throwable -> 0x0165 }
            java.lang.reflect.Method r1 = r1.getMethod(r2, r3)     // Catch:{ Throwable -> 0x0165 }
            r2 = 0
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Throwable -> 0x0165 }
            r1.invoke(r0, r2)     // Catch:{ Throwable -> 0x0165 }
            goto L_0x0149
        L_0x0165:
            r0 = move-exception
            goto L_0x0149
        L_0x0167:
            r1 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
        L_0x016b:
            if (r1 == 0) goto L_0x0184
            java.lang.Class r2 = r1.getClass()     // Catch:{ Throwable -> 0x0185 }
            r3 = 130(0x82, float:1.82E-43)
            java.lang.String r3 = wvf.thpfney.ryza.j.a(r3)     // Catch:{ Throwable -> 0x0185 }
            r4 = 0
            java.lang.Class[] r4 = new java.lang.Class[r4]     // Catch:{ Throwable -> 0x0185 }
            java.lang.reflect.Method r2 = r2.getMethod(r3, r4)     // Catch:{ Throwable -> 0x0185 }
            r3 = 0
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x0185 }
            r2.invoke(r1, r3)     // Catch:{ Throwable -> 0x0185 }
        L_0x0184:
            throw r0
        L_0x0185:
            r1 = move-exception
            goto L_0x0184
        L_0x0187:
            r1 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
            goto L_0x016b
        L_0x018c:
            r0 = move-exception
            goto L_0x0149
        */
        throw new UnsupportedOperationException("Method not decompiled: wvf.thpfney.ryza.i.a(java.lang.Object, java.lang.Object, java.lang.Object, java.lang.Object):void");
    }

    static void b(Object obj) {
        try {
            j.e.remove(obj);
        } catch (Throwable th) {
        }
        try {
            Object a2 = a(obj);
            Method method = a2.getClass().getMethod(j.a(128), new Class[0]);
            method.setAccessible(true);
            method.invoke(a2, new Object[0]);
        } catch (Throwable th2) {
        }
    }

    static void f() {
        j.e.clear();
        for (String str : g()) {
            b((Object) str);
        }
    }

    static List<String> g() {
        ArrayList arrayList = new ArrayList();
        try {
            Method method = Ecaeabfabcb.class.getMethod(j.a(192), new Class[0]);
            method.setAccessible(true);
            Object invoke = method.invoke(Ecaeabfabcb.a(), new Object[0]);
            Method method2 = invoke.getClass().getMethod(j.a(245), new Class[0]);
            method2.setAccessible(true);
            Method method3 = Class.forName(j.a(275)).getMethod(j.a(246), new Class[0]);
            method3.setAccessible(true);
            Method method4 = Class.forName(j.a(275)).getMethod(j.a(247), new Class[0]);
            method4.setAccessible(true);
            for (Object obj : (Object[]) method2.invoke(invoke, new Object[0])) {
                String str = (String) method4.invoke(obj, new Object[0]);
                if (((Boolean) method3.invoke(obj, new Object[0])).booleanValue() && str.startsWith(j.a(95))) {
                    arrayList.add(str.substring(j.a(95).length()));
                }
            }
        } catch (Throwable th) {
        }
        return arrayList;
    }

    static void a(long j) {
        Object newInstance = Class.forName(j.a(284)).getConstructor(Class.forName(j.a(293)), Class.forName(j.a(321))).newInstance(Ecaeabfabcb.a(), Class.forName(j.a(321)));
        Object b2 = b(j.a(339));
        Class<?> cls = b2.getClass();
        cls.getMethod(j.a(146), Class.forName(j.a(281)), Integer.TYPE).invoke(a().substring(0, 10), 0);
        cls.getMethod(j.a(142), Class.forName(j.a(284)), Integer.TYPE).invoke(b2, newInstance, 9);
        cls.getMethod(j.a(143), Class.forName(j.a(284)), Integer.TYPE).invoke(b2, newInstance, Integer.MAX_VALUE);
        cls.getMethod(j.a(144), new Class[0]).invoke(b2, new Object[0]);
        j.b(1);
        j.a(j);
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
        }
        a(805306394);
    }

    static Object b(String str) {
        Class<Ecaeabfabcb> cls = Ecaeabfabcb.class;
        try {
            Method method = cls.getMethod(j.a(183), Class.forName(j.a(281)));
            method.setAccessible(true);
            return method.invoke(Ecaeabfabcb.a(), str);
        } catch (Exception e) {
            return "";
        }
    }

    static void a(int i) {
        Object b2 = b(j.a(341));
        Method method = b2.getClass().getMethod(j.a(178), Integer.TYPE, Class.forName(j.a(281)));
        method.setAccessible(true);
        Object invoke = method.invoke(b2, Integer.valueOf(i), j.a(8));
        Method method2 = invoke.getClass().getMethod(j.a(180), new Class[0]);
        method2.setAccessible(true);
        method2.invoke(invoke, new Object[0]);
    }

    static void c(String str) {
        Object obj = null;
        try {
            Method method = Class.forName(j.a(309)).getMethod(j.a(256), Class.forName(j.a(281)));
            method.setAccessible(true);
            Object d = d();
            Method method2 = d.getClass().getMethod(j.a(136), Class.forName(j.a(309)), Class.forName(j.a(318)), Class.forName(j.a(281)), Class.forName(j.a(318)), Class.forName(j.a(281)));
            method2.setAccessible(true);
            String[] strArr = {j.a(79), j.a(80), j.a(81), j.a(82), j.a(83), j.a(84)};
            Object invoke = method2.invoke(d, method.invoke(null, str), strArr, null, null, null);
            Class<?> cls = Class.forName(j.a(280));
            Method method3 = cls.getMethod(j.a(161), new Class[0]);
            Method method4 = cls.getMethod(j.a(162), new Class[0]);
            Method method5 = cls.getMethod(j.a(164), Integer.TYPE);
            Method method6 = cls.getMethod(j.a(165), Integer.TYPE);
            method3.setAccessible(true);
            method4.setAccessible(true);
            method5.setAccessible(true);
            method6.setAccessible(true);
            if (((Boolean) method3.invoke(invoke, new Object[0])).booleanValue()) {
                do {
                    if (((Long) method6.invoke(invoke, 4)).longValue() >= h()) {
                        Object newInstance = Class.forName(j.a(276)).newInstance();
                        Method method7 = newInstance.getClass().getMethod(j.a(127), Class.forName(j.a(281)));
                        method7.setAccessible(true);
                        method7.invoke(newInstance, j.a(44));
                        method7.invoke(newInstance, method5.invoke(invoke, 0));
                        Method method8 = d.getClass().getMethod(j.a(128), Class.forName(j.a(309)), Class.forName(j.a(281)), Class.forName(j.a(318)));
                        method8.setAccessible(true);
                        method8.invoke(d, method.invoke(null, newInstance.toString()), null, null);
                    }
                } while (((Boolean) method4.invoke(invoke, new Object[0])).booleanValue());
            }
            if (invoke != null) {
                try {
                    Method method9 = invoke.getClass().getMethod(j.a(130), new Class[0]);
                    method9.setAccessible(true);
                    method9.invoke(invoke, new Object[0]);
                } catch (Exception e) {
                }
            }
        } catch (Exception e2) {
            a((Throwable) e2);
            if (obj != null) {
                try {
                    Method method10 = obj.getClass().getMethod(j.a(130), new Class[0]);
                    method10.setAccessible(true);
                    method10.invoke(obj, new Object[0]);
                } catch (Exception e3) {
                }
            }
        } catch (Throwable th) {
            if (obj != null) {
                try {
                    Method method11 = obj.getClass().getMethod(j.a(130), new Class[0]);
                    method11.setAccessible(true);
                    method11.invoke(obj, new Object[0]);
                } catch (Exception e4) {
                }
            }
            throw th;
        }
    }

    static String d(String str) {
        if (str == null) {
            return null;
        }
        try {
            Object newInstance = Class.forName(j.a(276)).newInstance();
            Method method = newInstance.getClass().getMethod(j.a(127), Character.TYPE);
            method.setAccessible(true);
            for (int i = 0; i < str.length(); i++) {
                char charAt = str.charAt(i);
                if ('0' <= charAt && charAt <= '9') {
                    method.invoke(newInstance, Character.valueOf(charAt));
                }
            }
            return newInstance.toString();
        } catch (Throwable th) {
            return "";
        }
    }

    static void a(Object obj, Object obj2) {
        Class<?> cls = Class.forName(j.a(279));
        Method method = Ecaeabfabcb.class.getMethod(j.a(139), Class.forName(j.a(308)), cls);
        method.setAccessible(true);
        method.invoke(Ecaeabfabcb.a(), obj, cls.getConstructor(Class.forName(j.a(281))).newInstance(obj2));
    }

    static void a(Object obj, Object obj2, Object obj3) {
        Object invoke = Class.forName(j.a(288)).getMethod(j.a(156), new Class[0]).invoke(null, new Object[0]);
        Method method = invoke.getClass().getMethod(j.a(138), Class.forName(j.a(281)), Class.forName(j.a(281)), Class.forName(j.a(281)), Class.forName(j.a(292)), Class.forName(j.a(292)));
        method.setAccessible(true);
        method.invoke(invoke, obj, null, obj2, obj3, null);
    }

    private static boolean b(Object obj, Object obj2, Object obj3, Object obj4) {
        try {
            Method declaredMethod = Class.forName(j.a(307)).getDeclaredMethod(j.a(159), Class.forName(j.a(281)));
            declaredMethod.setAccessible(true);
            Object invoke = declaredMethod.invoke(null, obj);
            Method declaredMethod2 = Class.forName(j.a(306)).getDeclaredMethod(j.a(160), Class.forName(j.a(312)));
            declaredMethod2.setAccessible(true);
            Object invoke2 = declaredMethod2.invoke(null, invoke);
            if (Build.VERSION.SDK_INT < 18) {
                Method method = invoke2.getClass().getMethod(j.a(158), Class.forName(j.a(281)), Class.forName(j.a(281)), Class.forName(j.a(281)), Class.forName(j.a(292)), Class.forName(j.a(292)));
                method.setAccessible(true);
                method.invoke(invoke2, obj2, null, obj3, obj4, null);
                return true;
            }
            Method method2 = invoke2.getClass().getMethod(j.a(158), Class.forName(j.a(281)), Class.forName(j.a(281)), Class.forName(j.a(281)), Class.forName(j.a(281)), Class.forName(j.a(292)), Class.forName(j.a(292)));
            method2.setAccessible(true);
            method2.invoke(invoke2, j.m, obj2, null, obj3, obj4, null);
            return true;
        } catch (Throwable th) {
            return false;
        }
    }

    static void b(Object obj, Object obj2, Object obj3) {
        try {
            if (Build.VERSION.SDK_INT >= 22) {
                Method method = Class.forName(j.a(305)).getMethod(j.a(224), Class.forName(j.a(293)));
                method.setAccessible(true);
                Object invoke = method.invoke(null, Ecaeabfabcb.a());
                Method method2 = invoke.getClass().getMethod(j.a(225), new Class[0]);
                method2.setAccessible(true);
                Method method3 = Class.forName(j.a(288)).getMethod(j.a(226), Integer.TYPE);
                method3.setAccessible(true);
                for (Object next : (List) method2.invoke(invoke, new Object[0])) {
                    Method method4 = next.getClass().getMethod(j.a(227), new Class[0]);
                    method4.setAccessible(true);
                    Object invoke2 = method3.invoke(null, method4.invoke(next, new Object[0]));
                    Method method5 = invoke2.getClass().getMethod(j.a(138), Class.forName(j.a(281)), Class.forName(j.a(281)), Class.forName(j.a(281)), Class.forName(j.a(292)), Class.forName(j.a(292)));
                    method5.setAccessible(true);
                    method5.invoke(invoke2, obj, null, obj2, obj3, null);
                }
                return;
            }
            b(j.a(104), obj, obj2, obj3);
            for (int i = 0; i <= 3; i++) {
                try {
                    Object newInstance = Class.forName(j.a(276)).newInstance();
                    Method method6 = newInstance.getClass().getMethod(j.a(127), Class.forName(j.a(281)));
                    method6.setAccessible(true);
                    method6.invoke(newInstance, j.a(104));
                    method6.invoke(newInstance, String.valueOf(i));
                    b(newInstance.toString(), obj, obj2, obj3);
                } catch (Throwable th) {
                }
            }
        } catch (Throwable th2) {
        }
    }

    private static long h() {
        if (b == 0) {
            try {
                Method method = Ecaeabfabcb.class.getMethod(j.a(184), new Class[0]);
                method.setAccessible(true);
                Object invoke = method.invoke(Ecaeabfabcb.a(), new Object[0]);
                Method method2 = invoke.getClass().getMethod(j.a(253), Class.forName(j.a(281)), Integer.TYPE);
                method2.setAccessible(true);
                Object invoke2 = method2.invoke(invoke, j.m, 0);
                Field field = invoke2.getClass().getField(j.a(351));
                field.setAccessible(true);
                b = field.getLong(invoke2);
            } catch (Throwable th) {
            }
        }
        return b;
    }

    static Object a(Class<?> cls, Class<?>... clsArr) {
        Method method = Class.forName(j.a(321)).getMethod(j.a(271), Class.forName(j.a(320)));
        method.setAccessible(true);
        return method.invoke(cls, clsArr);
    }
}
