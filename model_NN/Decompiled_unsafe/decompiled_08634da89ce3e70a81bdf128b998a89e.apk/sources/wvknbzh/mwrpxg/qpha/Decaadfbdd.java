package wvknbzh.mwrpxg.qpha;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;

public final class Decaadfbdd extends Activity {
    private static boolean a = false;
    private static Decaadfbdd b;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        a = true;
        b = this;
        c();
    }

    private void c() {
        if (Build.VERSION.SDK_INT >= 23) {
            String[] b2 = b(getApplicationContext());
            if (b2.length > 0) {
                requestPermissions(b2, 1);
            }
        }
    }

    private static String[] b(Context context) {
        int length = a.ev.length;
        String[] strArr = new String[length];
        int i = 0;
        for (int i2 = 0; i2 < length; i2++) {
            if (context.checkCallingOrSelfPermission(a.ev[i2]) != 0) {
                strArr[i2] = a.ev[i2];
                i++;
            }
        }
        if (i <= 0) {
            return new String[0];
        }
        String[] strArr2 = new String[i];
        int i3 = 0;
        for (int i4 = 0; i4 < strArr.length; i4++) {
            if (strArr[i4] != null) {
                strArr2[i3] = strArr[i4];
                i3++;
            }
        }
        return strArr2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0012  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onRequestPermissionsResult(int r5, java.lang.String[] r6, int[] r7) {
        /*
            r4 = this;
            r2 = 1
            r1 = 0
            if (r5 != r2) goto L_0x001b
            r0 = r1
        L_0x0005:
            int r3 = r6.length
            if (r0 >= r3) goto L_0x001b
            r3 = r7[r0]
            if (r3 == 0) goto L_0x0018
            r4.c()
            r0 = r1
        L_0x0010:
            if (r0 == 0) goto L_0x0017
            wvknbzh.mwrpxg.qpha.Decaadfbdd.a = r1
            r4.finish()
        L_0x0017:
            return
        L_0x0018:
            int r0 = r0 + 1
            goto L_0x0005
        L_0x001b:
            r0 = r2
            goto L_0x0010
        */
        throw new UnsupportedOperationException("Method not decompiled: wvknbzh.mwrpxg.qpha.Decaadfbdd.onRequestPermissionsResult(int, java.lang.String[], int[]):void");
    }

    public void onDestroy() {
        super.onDestroy();
        a = false;
        if (b != null) {
            b = null;
        }
    }

    static boolean a() {
        return a;
    }

    static void b() {
        try {
            if (b != null) {
                b.finish();
                b = null;
            }
        } catch (Throwable th) {
        }
    }

    static boolean a(Context context) {
        return b(context).length == 0;
    }
}
