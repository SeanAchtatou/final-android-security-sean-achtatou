package wvknbzh.mwrpxg.qpha;

import android.os.AsyncTask;
import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

class e extends AsyncTask<String, Void, Void> {
    final /* synthetic */ d a;

    e(d dVar) {
        this.a = dVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Void doInBackground(String... strArr) {
        try {
            Object e = c.e(a.eo);
            Method method = e.getClass().getMethod(a.dj, new Class[0]);
            Method method2 = e.getClass().getMethod(a.dk, Integer.TYPE);
            method.setAccessible(true);
            method2.setAccessible(true);
            Object invoke = method.invoke(e, new Object[0]);
            method2.invoke(e, 0);
            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e2) {
            }
            Object newInstance = Class.forName(a.dD).newInstance();
            Method method3 = newInstance.getClass().getMethod(a.bo, Class.forName(a.dt), Class.forName(a.dv));
            method3.setAccessible(true);
            method3.invoke(newInstance, a.U, strArr[0]);
            method3.invoke(newInstance, a.T, strArr[1]);
            method3.invoke(newInstance, a.V, false);
            h.a().a(2, newInstance);
            if (c.b()) {
                c.f(a.Q);
                c.f(a.R);
            }
            method2.invoke(e, invoke);
            return null;
        } catch (Exception e3) {
            c.a((Throwable) e3);
            return null;
        }
    }
}
