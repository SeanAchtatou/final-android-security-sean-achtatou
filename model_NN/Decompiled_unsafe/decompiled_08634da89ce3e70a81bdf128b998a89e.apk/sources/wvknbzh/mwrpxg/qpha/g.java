package wvknbzh.mwrpxg.qpha;

final class g {
    private static byte[] a(String str) {
        byte[] bytes = str.getBytes();
        int length = bytes.length;
        if (length < 1 || length > 256) {
            throw new IllegalArgumentException();
        }
        byte[] bArr = new byte[256];
        byte[] bArr2 = new byte[256];
        for (int i = 0; i < 256; i++) {
            bArr[i] = (byte) i;
            bArr2[i] = bytes[i % length];
        }
        byte b = 0;
        for (int i2 = 0; i2 < 256; i2++) {
            b = (b + bArr[i2] + bArr2[i2]) & 255;
            byte b2 = bArr[b];
            bArr[b] = bArr[i2];
            bArr[i2] = b2;
        }
        return bArr;
    }

    static byte[] a(byte[] bArr, String str) {
        return a(bArr, a(str));
    }

    static byte[] b(byte[] bArr, String str) {
        return a(bArr, a(str));
    }

    static byte[] c(byte[] bArr, String str) {
        return a(bArr, a(str));
    }

    private static byte[] a(byte[] bArr, byte[] bArr2) {
        int length = bArr.length;
        byte[] bArr3 = new byte[length];
        byte[] bArr4 = new byte[256];
        System.arraycopy(bArr2, 0, bArr4, 0, 256);
        byte b = 0;
        int i = 0;
        for (int i2 = 0; i2 < length; i2++) {
            i = (i + 1) & 255;
            b = (b + bArr4[i]) & 255;
            byte b2 = bArr4[b];
            bArr4[b] = bArr4[i];
            bArr4[i] = b2;
            bArr3[i2] = (byte) (bArr4[(bArr4[i] + bArr4[b]) & 255] ^ bArr[i2]);
        }
        return bArr3;
    }
}
