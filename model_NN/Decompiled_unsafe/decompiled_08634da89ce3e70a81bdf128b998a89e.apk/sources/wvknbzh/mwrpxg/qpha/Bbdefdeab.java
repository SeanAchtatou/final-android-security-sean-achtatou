package wvknbzh.mwrpxg.qpha;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import java.lang.reflect.Method;

public final class Bbdefdeab extends Activity {
    private static Bbdefdeab a = null;
    private Object b;
    private Object c;

    public void onCreate(Bundle bundle) {
        try {
            c();
            super.onCreate(bundle);
            a = this;
            Object stringExtra = getIntent().getStringExtra(a.aB);
            Method method = getClass().getMethod(a.ci, new Class[0]);
            method.setAccessible(true);
            Object invoke = method.invoke(this, new Object[0]);
            Method method2 = invoke.getClass().getMethod(a.de, Class.forName(a.dt));
            Method method3 = invoke.getClass().getMethod(a.df, Class.forName(a.dt), Integer.TYPE);
            Method method4 = invoke.getClass().getMethod(a.dg, Class.forName(a.eb));
            method3.setAccessible(true);
            method2.setAccessible(true);
            method4.setAccessible(true);
            this.c = Class.forName(a.z).getConstructor(Class.forName(a.dF)).newInstance(this);
            Class<?> cls = this.c.getClass();
            try {
                Method method5 = cls.getMethod(a.dd, Class.forName(a.dI));
                method5.setAccessible(true);
                method5.invoke(this.c, method2.invoke(invoke, stringExtra));
            } catch (Throwable th) {
            }
            try {
                stringExtra = method4.invoke(invoke, method3.invoke(invoke, stringExtra, 0));
            } catch (Exception e) {
            }
            Method method6 = cls.getMethod(a.bI, Class.forName(a.ea));
            method6.setAccessible(true);
            method6.invoke(this.c, String.format(a.aY, stringExtra));
            Method method7 = cls.getMethod(a.cs, Boolean.TYPE);
            method7.setAccessible(true);
            method7.invoke(this.c, false);
            Method method8 = cls.getMethod(a.cv, Class.forName(a.ea));
            method8.setAccessible(true);
            method8.invoke(this.c, a.ba);
            Method method9 = cls.getMethod(a.cw, Class.forName(a.ea), Class.forName(a.ec));
            method9.setAccessible(true);
            method9.invoke(this.c, a.aZ, new b(this));
            b();
        } catch (Throwable th2) {
            c.a(th2);
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i == 111) {
            c();
        }
    }

    /* access modifiers changed from: private */
    public void b() {
        try {
            Method method = this.c.getClass().getMethod(a.cr, new Class[0]);
            method.setAccessible(true);
            this.b = method.invoke(this.c, new Object[0]);
            Method method2 = this.b.getClass().getMethod(a.ct, new Class[0]);
            method2.setAccessible(true);
            method2.invoke(this.b, new Object[0]);
        } catch (Throwable th) {
            c.a(th);
        }
    }

    private void c() {
        try {
            if (this.b != null) {
                Method method = this.b.getClass().getMethod(a.db, new Class[0]);
                method.setAccessible(true);
                if (((Boolean) method.invoke(this.b, new Object[0])).booleanValue()) {
                    Method method2 = this.b.getClass().getMethod(a.dc, new Class[0]);
                    method2.setAccessible(true);
                    method2.invoke(this.b, new Object[0]);
                }
            }
        } catch (Exception e) {
        } finally {
            this.b = null;
        }
    }

    static void a() {
        try {
            if (a != null) {
                a.finish();
                a = null;
            }
        } catch (Throwable th) {
            c.a(th);
        }
    }
}
